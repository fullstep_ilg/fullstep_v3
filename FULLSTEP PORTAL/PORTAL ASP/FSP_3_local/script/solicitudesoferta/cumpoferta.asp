﻿<!DOCTYPE HTML PUBLIC >

<%@  language="VBScript" %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/fsal_1.asp"-->

<script src="../common/menu.asp"></script>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <title><%=title%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script language="JavaScript" src="slider.js"></script>
    <script src="../common/formatos.js"></script>
    <script language="JavaScript" src="../common/ajax.js"></script>
    <script>var RutaSegura="<%=application("RUTASEGURA")%>"</script>
    <script language="JavaScript" src="cumpOferta.js"></script>
    <script src="../solicitudesoferta/js/escalado.asp"></script>
    <!--#include file="../common/fsal_3.asp"-->
    <!--#include file="../common/fsal_5.asp"-->

    <style type="text/css">
        TD.importesParciales {
            text-align: right;
        }
    </style>

    <%
''' <summary>
''' Permite al proveedor realizar ofertas  subastas, bajarse a excel ofertas e imprimir ofertas.
''' </summary>    
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>

oldTimeOut = server.ScriptTimeout
server.ScriptTimeout = 30000

anyoproceso = cint(Request("Anyo"))
gmn1proceso = Request("Gmn1")
codproceso = clng(Request("Cod"))

dim IsSecure
IsSecure = left(Application ("RUTASEGURA"),8)
if IsSecure = "https://" then
    IsSecure = "; secure"
else
    IsSecure =""
end if

Response.AddHeader "Set-Cookie","PROCE_ANYO=" &  anyoproceso & "; path=/; HttpOnly" + IsSecure
Response.AddHeader "Set-Cookie","PROCE_GMN1=" &  gmn1proceso & "; path=/; HttpOnly" + IsSecure
Response.AddHeader "Set-Cookie","PROCE_COD=" &  codproceso & "; path=/; HttpOnly" + IsSecure

set oRaiz=validarUsuario(Idioma,true,false,0)

utcservidor= oraiz.UTCServidor	

CiaComp = clng(oRaiz.Sesion.CiaComp)

    %>
    <script>
dFechaInicialServer = UTCtoClient(<%=JSHora(oRaiz.ObtenerSQLUTCDate(CiaComp))%>)
dFechaInicialClient = new Date()
iIrAApartado="<%=Request("IrA")%>"
bSinSync=true
PuedesClicarItems=1
    </script>
    <link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/reset.css">
    <link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
    <script src="../js/arbol.js"></script>
    <script src="../common/calendar.asp"></script>
</head>
<body topmargin="0" leftmargin="0" onload="init()" onresize="resize()">
    <script>dibujaMenu(3)</script>
    <%


dim den

Idioma = Request.Cookies("USU_IDIOMA")
den=devolverTextos(Idioma,54 )

'Mensaje a mostrar cuando se viene de cargar excel y hay cambios en GS
men=Request("Mensaje")
if men<>"" and men<>"0" then
    mensaje=den(145) & vbcrlf 
    select Case men
    case "1" 'Los costes y descuentos del proceso han sido modificados por el comprador mientras usted completaba su oferta
        mensaje=mensaje & den(147) 
    case "2" 'El comprador ha a�ñadido nuevos ítems de compra mientras usted completaba su oferta
        mensaje=mensaje & den(148) 
    case "3" 'El comprador ha eliminado algunos ítems de compra mientras usted completaba su oferta
        mensaje=mensaje & den(149) 
    case "4" 'El comprador ha modificado la cantidad de algunos ítems de compra mientras usted completaba su oferta
        mensaje=mensaje & den(150) 
    case "5" 'El comprador ha eliminado algún grupo de compra mientras usted completaba su oferta
        mensaje=mensaje & den(151) 
    case "6" 'El comprador ha añadido algún nuevo grupo de compra mientras usted completaba su oferta
        mensaje=mensaje & den(152) 
    end select
    mensaje=mensaje & vbcrlf & den(146) 'Por favor, revise la oferta
    %>
    <script>
        alert("<%=JSAlertText(mensaje)%>")    
    </script>
    <%

end if 
''''

decimalfmt = Request.Cookies ("USU_DECIMALFMT")
thousanfmt = Request.Cookies ("USU_THOUSANFMT") 
precisionfmt = CINT(Request.Cookies ("USU_PRECISIONFMT") )
datefmt = Request.Cookies ("USU_DATEFMT") 
	
CodProve = oRaiz.Sesion.CiaCodGs
CiaComp = cint(Request("Cia"))

Set oProceso = oRaiz.Generar_CProceso

oProceso.cargarConfiguracion CiaComp, anyoproceso, gmn1proceso, codproceso, CodProve
oProceso.cargarProceso CiaComp, Idioma
oProceso.CargarComentarioProveedor CiaComp , CodProve
%>
<script>
//Dato Objetivo proceso: Si la moneda es la de proceso y la oferta, NO se ha de usar cambio leido de bbdd, sino q se ha de usar el Cambio del proceso
MonedaBBDD= '<%=oProceso.MonCod%>'
MonedaBBDDCambio=<%=JSNum(oProceso.Cambio)%>
</script>
<%
'DPD -> Se anota en la Base de datos el acceso del proveedor para que pueda ser monitorizado desde el panel del GS
oProceso.AnotarEventoEntrada CiaComp,CodProve
' Carga de los datos de Costes / Descuentos
oProceso.CargarCostesDescuentos CiaComp



IF oProceso.Subasta and oProceso.subastaActiva = false then
	set oProceso = nothing
    
	set oRaiz = nothing
    %>
    <script>
    
	window.open ("<%=application("RUTASEGURA")%>script/solicitudesoferta/pujacerrada.asp?Idioma=<%=Idioma%>&Anyo=<%=anyoproceso%>&Gmn1=<%=gmn1proceso%>&Proce=<%=codproceso%>&CiaComp=<%=CiaComp%>","_self")
    </script>
    <%
	server.ScriptTimeout = oldTimeOut
	Response.End 
END IF

if oProceso.estado = 12 or oProceso.estado = 13 or oProceso.estado = 20 then
	set oProceso = nothing
	
	set oRaiz = nothing
    %>
    <script>
	    window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/procesocerrado.asp?Idioma=<%=Idioma%>&Anyo=<%=anyoproceso%>&Gmn1=<%=gmn1proceso%>&Proc=<%=codproceso%>&CiaComp=<%=CiaComp%>", "_self")
    </script>
    <%
	server.ScriptTimeout = oldTimeOut
	Response.End 
end if


oProceso.cargarEspecificaciones CiaComp

'JORGE cargo los atributos de especificaciones del proceso
oProceso.CargarAEspecificacionesP CiaComp

set oOferta = oProceso.CargarOfertaProveedor(Idioma,ciacomp,codprove)
oOferta.LeerFechaAct(CiaComp)
    %>
    <script>
dFecActOferta= <%=JSHora(oOferta.fechaact)%>
    </script>
    <%

'Carga de los datos de la oferta de los grupos
if not oProceso.Grupos is nothing then
    for each oGrupo in oProceso.Grupos
        oGrupo.cargarDatosOferta Idioma, CiaComp, oGrupo.ID, anyoproceso, gmn1proceso, codproceso, CodProve, oOferta.num        
		oGrupo.cargarGrupo Idioma,ciaComp, oGrupo.ID, ,,,codprove,oOferta.num,oOferta.codMon,oProceso.SolicitarCantMax
    next
end if

oOferta.CargarImportesParcialesGrupo CiaComp


'equiv: Va a contener la equivalencia de la moneda de la oferta respecto a la moneda central. 
'Si moneda central el euro, moneda proceso centimo de euro, moneda oferta centimo de euro -> Equiv = 100



if oProceso.CambiarMon and not oProceso.AdminPublica then
	if trim(oOferta.codmon)<>"" then
		codMon = oOferta.codmon
		denMon = oOferta.monden
		equiv = oProceso.cambio * oOferta.cambio
	else
        set oProve = oraiz.DevolverProveedorEnCiaComp(Idioma, oRaiz.Sesion.CiaId, CiaComp)
        sCodMon = "EUR"
        if not oProve is nothing then
            with oProve
		        if isnull(.CodMon) then
			        set oRS = oRaiz.ObtenerMonedaCentral(ciacomp)
			        if not oRS is nothing then
				        sCodMon = oRS.fields("MON_GS").value
				        oRS.close
				        set oRs = nothing				        
			        end if
		        else
			        sCodMon = .CodMon
		        end if            
            end with
        end if
        set oProve = nothing 

    	set oMonedas = oRaiz.Generar_CMonedas()
		oMonedas.CargarTodasLasMonedasDesde Idioma,CiaComp , 1, sCodMon, , , true, , true

		codMon = sCodMon
		denMon = oMonedas.Item(sCodMon).den
		equiv=Numero(replace(oMonedas.item(sCodMon).equiv,thousanfmt,decimalfmt))
        set oMonedas = nothing
	end if
	
else
	codMon = oProceso.MonCod
	denMon = oProceso.monDen
	if trim(oOferta.codmon)<>"" then
		equiv = oProceso.cambio * oOferta.cambio
	else
		equiv = oProceso.cambio
	end if
end if

dim bConsulta
if iif(oProceso.MaximoNumeroOfertas<>0,oProceso.MaximoNumeroOfertas<> oOferta.TotEnviadas,true) then
	bConsulta = false
else
	bConsulta = true
end if
    %>



    <%if oProceso.AdminPublica then%>
    <script src="js/configadmin.asp"></script>
    <script src="js/sobre.asp"></script>
    <%else%>
    <script src="js/config.asp"></script>
    <%end if%>

    <script src="js/proceso.asp?consulta=<%=bConsulta%>"></script>
    <script src="js/grupo.asp?consulta=<%=bConsulta%>"></script>
    <script src="js/item.asp"></script>
    <script src="js/adjunto.asp"></script>
    <script src="js/atributo.asp"></script>
    <script src="js/auxiliares.asp"></script>
    <script src="js/especificacion.asp"></script>
    <script src="js/AtributoEspecificacion.asp"></script>
    <script src="js/costedescgrofert.asp"></script>
    <script src="js/costedescrecalculo.asp"></script>
    <script>

vdecimalfmt ="<%=decimalfmt%>"
vthousanfmt ="<%=thousanfmt%>"
vprecisionfmt = <%=precisionfmt%>
vdatefmt = "<%=datefmt%>"
imgAbrir = new Image()
imgAbrir.src = "images/abrir.GIF"
imgEliminar = new Image()
imgEliminar.src = "images/elimadjunto.GIF"

    </script>
    <script>
var den=new Array();

<%
	dim iden
	for iden=0 to ubound(den)-1
		%>
		den[ <%=iden%>] = '<%=JStext(den(iden))%>'
		<%
	next
%>




<%
'En el objeto proceso almacenamos un objeto moneda. En principio tendr la moneda con la que se ha ofertado y la equivalencia respecto a la moneda central
with oProceso%>

//Introducimos un valor m�s en la creaci�n del objeto proceso, como �ltimo par�metro el modo del proyecto (proceso/grupo/�tem)
proceso = new Proceso (<%=.anyo%>,'<%=.gmn1Cod%>','<%=.Cod%>','<%=JSText(.Den)%>',<%if .DefDestino=1 then%>new Destino('<%=JSText(.destCod)%>','<%=JSText(.Destino.Den)%>')<%else%>null<%end if%>,'<%=JSText(.pagCod & " - " & .pagDen)%>',<%=JSDate(.FechaInicioSuministro)%>,<%=JSDate(.FechaFinSuministro)%>,'<%=JSText(.esp)%>','<%=JSText(codprove)%>',<%=oOferta.Num%>, <%=JSText(oOferta.EstUltOfe)%>, <%=JSDate(oOferta.FechaHasta)%>,new Moneda('<%=JSText(codMon)%>','<%=JSText(denMon)%>', <%=JSNum(equiv)%>), <%=JSNum(oOferta.Cambio)%>, '<%=JSText(oOferta.obs)%>',<%=JSNum(oOferta.TotEnviadas)%>,<%=JSDate(oOferta.FechaUltOfe)%>,<%=JSNum(oOferta.totalBytes)%>,new CfgProceso(<%=JSBool(.DefDestino=1)%>,<%=JSBool(.DefFormaPago=1)%>,<%=JSBool(.DefFechasSum=1)%>,<%=JSBool(.DefEspecificaciones)%>,<%=JSBool(.SolicitarCantMax)%>,<%=JSBool(.PedirAlterPrecios)%>,<%=JSBool(.DefAdjunOferta)%>,<%=JSBool(.DefAdjunGrupo)%>,<%=JSBool(.DefAdjunItem)%>,<%=JSBool(.Subasta)%>,<%=JSBool(.VerProveedor)%>,<%=JSBool(.VerDetallePujas)%>,<%=JSBool(.cambiarMon)%>,<%=JSBool(.MostrarFinSum)%>,<%=JSBool(.HayCostesDescuentos)%>),<%=JSNum(oOferta.ImporteTotal)%>, "<%=JSText(oOferta.obsAdjuntos)%>", <%=JSNum(oOferta.IdPortal)%>, <%=JSNum(oOferta.posicionPuja)%>,<%=JSNum(oOferta.ImporteBruto)%>,<%=.modo%>)

<%end with%>

<%for each oEsp in oProceso.Especificaciones
with oEsp%>
	esp = new Especificacion(<%=.id%>,'<%=JSText(.nombre)%>','<%=JSText(.Comentario)%>',<%=JSNum(.datasize)%>,proceso)
	proceso.anyaEspec(esp)
<%
end with
next%>

// carga de los valores de los costes / descuentos en el �mbito del proceso
<%


set oAtribOfe = nothing
oOferta.CargarAtributosOfertados CiaComp, codMon
set oAtribOfe = oOferta.AtribsOfertados
vImporteAnterior = 0
 %>

<%if not (oProceso.CostesDescTotalOferta is nothing) then %>

<%for each oCosDes in oProceso.CostesDescTotalOferta

with oCosDes%>   
	costedesc = new Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>, <%=JSNum(.valor)%>, <%=JSBool(.obligatorio)%>, <%=JSNum(.valorMin)%>, <%=JSNum(.valorMax)%>, null, proceso,'<%=JSText(.operacion)%>',null,<%=.ambito %>,null<%
        if not .Grupos is nothing then
            str=",new Array("
        for each oGrupo in .Grupos
	        if str<>",new Array(" then
		        str = str & ","
	        end if      
            str = str & "'" & JSText(oGrupo.Grupo) & "'"
        next
        str = str & ")"
    end if
    response.write str & ",'" & JSText(.Alcance) & "')"
    
    %>
	proceso.anyaCosteDescuentoTotalOferta(costedesc)

    <%if oCosDes.Ambito=1 then%>
	    costedescvalor = new Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>,<%
        if not oAtribOfe is nothing then
        if not oAtribOfe.Item(cstr(.PAID)) is nothing then
            if (.operacion = "+" or  .operacion = "-") then
	            vValor = oAtribOfe.Item(cstr(.PAID)).valor 
            else
	            vValor = oAtribOfe.Item(cstr(.PAID)).valor 
            end if
    //        If vImporteAnterior = 0 then
    //            vImporte = oOferta.importeBruto
    //        else
    //            vImporte = vImporteAnterior
    //        end if
    //        vImporteAnterior = oAtribOfe.Item(cstr(.PAID)).ImporteParcial
    
            vImporte = oAtribOfe.Item(cstr(.PAID)).ImporteParcial
        else
            vValor = null
            vImporte = null
        end if
        else
            vValor = null
            vImporte = null
        end if
        response.write  JSNum(vValor) 
        response.write  "," + JSBool(.obligatorio)
        response.write "," & JSNum(.valorMin) &  "," & JSNum(.valorMax) 
        if .intro = 1 then
	        str=",["
	        for each oValor in .Valores
		        if str<>",[" then
			        str = str & ","
		        end if
                if (.operacion = "+" or  .operacion = "-") then
			        vValor = oValor.Valor
		        else
			        vValor = oValor.Valor 
                end if
                str = str & JSNum(vValor)
            next
            response.write str & "],proceso,'" & JSText(.operacion) & "'," & JSNum(.aplicar) & ", null"
            response.write ", " & JSNum(vImporte) & ")"
	    else
		    response.write ",null,proceso,'" & JSText(.operacion) & "',"  & JSNum(.aplicar) & ", null"
            response.write ", " &  JSNum(vImporte) & ")"
	    end if

         %>
	    proceso.anyaCosteDescuento(costedescvalor)
<%    end if
end with
next%>
<%end if %>

<%if not (oProceso.CostesDescTotalGrupo is nothing) then %>
<%for each oCosDes in oProceso.CostesDescTotalGrupo
with oCosDes%>
	costedesc = new Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>, <%=JSNum(.valor)%>, <%=JSBool(.obligatorio)%>, <%=JSNum(.valorMin)%>, <%=JSNum(.valorMax)%>, null, proceso,'<%=JSText(.operacion)%>',null,<%=.ambito %>,null<%
        if not .Grupos is nothing then
        str=",new Array("
        for each oGrupo in .Grupos
	        if str<>",new Array(" then
		        str = str & ","
	        end if      
            str = str & "'" & JSText(oGrupo.Grupo) & "'"
        next
        str = str & ")"
    end if
    response.write str & ",'" & JSText(.Alcance) & "')"

    %>
	proceso.anyaCosteDescuentoTotalGrupo(costedesc)

    <%if oCosDes.Ambito=1 then%>
	    costedescvalor = new Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>,<%
        if not oAtribOfe is nothing then
        if not oAtribOfe.Item(cstr(.PAID)) is nothing then
            if (.operacion = "+" or  .operacion = "-") then
	            vValor = oAtribOfe.Item(cstr(.PAID)).valor
            else
	            vValor = oAtribOfe.Item(cstr(.PAID)).valor 
            end if
    //        If vImporteAnterior = 0 then
    //            vImporte = oOferta.importeBruto
    //        else
    //            vImporte = vImporteAnterior
    //        end if
    //        vImporteAnterior = oAtribOfe.Item(cstr(.PAID)).ImporteParcial
            vImporte = oAtribOfe.Item(cstr(.PAID)).ImporteParcial
        else
            vValor = null
            vImporte = null
        end if
        else
            vValor = null
            vImporte = null
        end if
        response.write  JSNum(vValor) 
        response.write  "," + JSBool(.obligatorio)
        response.write "," & JSNum(.valorMin) &  "," & JSNum(.valorMax) 
        if .intro = 1 then
	        str=",["
	        for each oValor in .Valores
		        if str<>",[" then
			        str = str & ","
		        end if
                if (.operacion = "+" or  .operacion = "-") then
			        vValor = oValor.Valor
		        else
			        vValor = oValor.Valor 
                end if
                str = str & JSNum(vValor)
            next
            response.write str & "],proceso,'" & JSText(.operacion) & "'," & JSNum(.aplicar) & ", null"
            response.write ", " &  JSNum(vImporte) & ")"
	    else
		    response.write ",null,proceso,'" & JSText(.operacion) & "',"  & JSNum(.aplicar) & ", null"
            response.write ", " &  JSNum(vImporte) & ")"
	    end if
         %>
	    proceso.anyaCosteDescuento(costedescvalor)
<%end if
end with
next%>
<%end if %>

<%if not (oProceso.CostesDescTotalItem is nothing) then %>
<%for each oCosDes in oProceso.CostesDescTotalItem
with oCosDes%>
	costedesc = new Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>, <%=JSNum(.valor)%>, <%=JSBool(.obligatorio)%>, <%=JSNum(.valorMin)%>, <%=JSNum(.valorMax)%>, null, proceso,'<%=JSText(.operacion)%>',null,<%=.ambito %>,null<%
        if not .Grupos is nothing then
        str=",new Array("
        for each oGrupo in .Grupos
	        if str<>",new Array(" then
		        str = str & ","
	        end if      
            str = str & "'" & JSText(oGrupo.Grupo) & "'"
        next
        str = str & ")"
    end if
    response.write str & ",'" & JSText(.Alcance) & "')"
    
    %>
	proceso.anyaCosteDescuentoTotalItem(costedesc)

    <%if oCosDes.Ambito=1 then%>
	    costedescvalor = new Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>,<%
        if not oAtribOfe is nothing then
        if not oAtribOfe.Item(cstr(.PAID)) is nothing then
            if (.operacion = "+" or  .operacion = "-") then
	            vValor = oAtribOfe.Item(cstr(.PAID)).valor
            else
	            vValor = oAtribOfe.Item(cstr(.PAID)).valor 
            end if
    //        If vImporteAnterior = 0 then
    //            vImporte = oOferta.importeBruto
    //        else
    //            vImporte = vImporteAnterior
    //        end if
    //        vImporteAnterior = oAtribOfe.Item(cstr(.PAID)).ImporteParcial          
            vImporte = oAtribOfe.Item(cstr(.PAID)).ImporteParcial
        else
            vValor = null
            vImporte = null
        end if
        else
            vValor = null
            vImporte = null
        end if
        response.write  JSNum(vValor) 
        response.write  "," + JSBool(.obligatorio)
        response.write "," & JSNum(.valorMin) &  "," & JSNum(.valorMax) 
        if .intro = 1 then
	        str=",["
	        for each oValor in .Valores
		        if str<>",[" then
			        str = str & ","
		        end if
                if (.operacion = "+" or  .operacion = "-") then
			        vValor = oValor.Valor
		        else
			        vValor = oValor.Valor 
                end if
                str = str & JSNum(vValor)
            next
            response.write str & "],proceso,'" & JSText(.operacion) & "'," & JSNum(.aplicar) & ", null"
            response.write ", " &  JSNum(vImporte) & ")"
	    else
		    response.write ",null,proceso,'" & JSText(.operacion) & "',"  & JSNum(.aplicar) & ", null"
            response.write ", " &  JSNum(vImporte) & ")"
	    end if

         %>
	    proceso.anyaCosteDescuento(costedescvalor)
<%end if
end with
next%>
<%end if %>

<%if not (oProceso.CostesDescPrecItem is nothing) then %>
<%for each oCosDes in oProceso.CostesDescPrecItem
with oCosDes%>
	costedesc = new Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>, <%=JSNum(.valor)%>, <%=JSBool(.obligatorio)%>, <%=JSNum(.valorMin)%>, <%=JSNum(.valorMax)%>, null, proceso,'<%=JSText(.operacion)%>',null,<%=.ambito %>,null<%
        if not .Grupos is nothing then
        str=",new Array("
        for each oGrupo in .Grupos
	        if str<>",new Array(" then
		        str = str & ","
	        end if      
            str = str & "'" & JSText(oGrupo.Grupo) & "'"
        next
        str = str & ")"
    end if
    response.write str & ",'" & JSText(.Alcance) & "')"
    
    %>
	proceso.anyaCosteDescuentoPrecItem(costedesc)

    <%if oCosDes.Ambito=1 then%>
	    costedescvalor = new Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>,<%
        if not oAtribOfe is nothing then
        if not oAtribOfe.Item(cstr(.PAID)) is nothing then
            if (.operacion = "+" or  .operacion = "-") then
	            vValor = oAtribOfe.Item(cstr(.PAID)).valor
            else
	            vValor = oAtribOfe.Item(cstr(.PAID)).valor 
            end if
    //        If vImporteAnterior = 0 then
    //            vImporte = oOferta.importeBruto
    //        else
    //            vImporte = vImporteAnterior
    //        end if
    //        vImporteAnterior = oAtribOfe.Item(cstr(.PAID)).ImporteParcial
            vImporte = oAtribOfe.Item(cstr(.PAID)).ImporteParcial
        else
            vValor = null
            vImporte = null
        end if
        else
            vValor = null
            vImporte = null
        end if
        response.write  JSNum(vValor) 
        response.write  "," + JSBool(.obligatorio)
        response.write "," & JSNum(.valorMin) &  "," & JSNum(.valorMax) 
        if .intro = 1 then
	        str=",["
	        for each oValor in .Valores
		        if str<>",[" then
			        str = str & ","
		        end if
                if (.operacion = "+" or  .operacion = "-") then
			        vValor = oValor.Valor
		        else
			        vValor = oValor.Valor 
                end if
                str = str & JSNum(vValor)
            next
            response.write str & "],proceso,'" & JSText(.operacion) & "'," & JSNum(.aplicar) & ", null"
            response.write ", " &  JSNum(vImporte) & ")"
	    else
		    response.write ",null,proceso,'" & JSText(.operacion) & "',"  & JSNum(.aplicar) & ", null"
            response.write ", " &  JSNum(vImporte) & ")"
	    end if
         %>
	    proceso.anyaCosteDescuento(costedescvalor)
<%end if

end with
next%>
<%end if %>

//Carga de los importes parciales de cada coste/descuento para los grupos
<%if not (oProceso.ImportesParcialesGrupo is nothing) then %>
<%for each oImpPar in oProceso.ImportesParcialesGrupo
with oImpPar%>   
	imporParc = new CosteDescGrOfert('<%=RTrim(JSText(.Grupo))%>', <%=JSNum(.CosteDescuento)%>, <%=JSNum(.ImporteParcial)%>)
	proceso.anyaImportesParcialesGrupo(imporParc)
<%
end with
next%>
<%end if %>


//JORGE cargo los atributos de espeficaciones del proceso
<%
for each oAespP in oProceso.AEspecificacionesP
with oAespP%>
	AespP = new AtributoEspecificacion(0,
	<%=JSNum(.id)%>,	
	'<%=JSText(.cod)%>',	
	'<%=JSText(.descr)%>',	
	'<%=JSText(.Desc)%>',
	<%=JSNum(.Atributo)%>,
	<%=JSNum(.Interno)%>,
	<%=JSNum(.Pedido)%>,
	<%=JSNum(.Orden)%>,
	<%=JSNum(.Valor_num)%>,'<%=JSText(.Valor_text)%>',
	<%=JSDate(.Valor_fec)%>,
	<%=JSBool(.Valor_bool)%>,
	proceso)
		
	proceso.anyaAEspecP(AespP)
<%
end with
next%>


var gCodMon="<%=codmon%>"
var gDenMon="<%=denmon%>"

var vFechaLimite
var iEstadoSubasta
<%if  not isnull(oProceso.FechaMinimoLimOfertas) then%>
	vFechaLimite = UTCtoClient(<%=JSHora(oProceso.FechaMinimoLimOfertas)%>)        
    iEstadoSubasta =  <%=JSnum(oProceso.EstadoSubasta)%>
<%end if%>

<%if oProceso.subasta then%>
	var sMensajeError = "<%=JSText(den(50))%>"
	var vFechaApertura
    var vFechaReinicio
    vFechaReinicio = ""
	<%if  not isnull(oProceso.FechaApertura) then%>
		vFechaApertura = UTCtoClient(<%=JSHora(oProceso.FechaApertura)%>)
	<%end if%>
    <%if oProceso.EstadoSubasta=3 then %>
        <%if not isnull(oProceso.FechaReini) then %>
            vFechaReinicio = UTCtoClient(<%=JSHora(oProceso.FechaReini)%>)    
        <%end if%>
    <%end if%>
	var vFechaAperturaSobre
	<%if oProceso.tipo = 1 then%> //Tipo Sobre cerrado
        <%if  not isnull(oProceso.FecApeSobre) then%>
		    vFechaAperturaSobre = UTCtoClient(<%=JSHora(oProceso.FecApeSobre)%>)
	    <%end if%>
    <%end if%>


<%end if%>

<%
if oProceso.AdminPublica then
	for each oSobre in oProceso.Sobres
	with oSobre
	%>
		sobre = new Sobre (proceso,<%=.cod%>,"<%=JSText(.est)%>",UTCtoClient(<%=JSHora(.fechaapertura)%>),'<%=JSText(.obs)%>')
		proceso.anyaSobre(sobre)

	<%
		if not .Grupos is nothing then
			for each oGrupo in .grupos
            with oGrupo
			%>
				oCfg = new CfgGrupo (<%=JSBool(oGrupo.DefDestino)%>,<%=JSBool(oGrupo.DefFormaPago)%>,<%=JSBool(oGrupo.DefFechasSum)%>,<%=JSBool(oGrupo.DefEspecificaciones)%>,<%=JSBool(oGrupo.HayAtributos)%>,<%=JSBool(oGrupo.HayCostesDescuentos)%>)
                // En la creaci�n del objeto Grupo, pasamos cvalor del campo Posici�n, que antes se pasaba como null
		        grupo = new Grupo (proceso,'<%=JSText(oGrupo.Codigo)%>','<%=JSText(oGrupo.den)%>','<%=JSText(oGrupo.descripcion)%>',<%if .DefDestino then%>new Destino('<%=JSText(.DestCod)%>','<%=JSText(.Destino.Den)%>')<%else%>null<%end if%>,'<%=JSText(oGrupo.pagCod & " - " & oGrupo.pagDen)%>',<%=JSDate(oGrupo.FechaInicioSuministro)%>,<%=JSDate(oGrupo.FechaFinSuministro)%>,'<%=JSText(oGrupo.Esp)%>',<%=JSNum(oGrupo.NumItems)%>,oCfg,sobre,<%=JSNum(oGrupo.importeTotal * equiv)%>,"<%=JSText(oGrupo.ObsAdjuntos)%>",<%=JSNum(oGrupo.posicionpuja)%>,'<%=JSText(oGrupo.ID)%>',<%=JSNum(oGrupo.importeBruto * equiv)%>, <%=JSNum(oGrupo.Bloqueado)%>)
				sobre.anyaGrupo(grupo)
				proceso.anyaGrupo(grupo)
			<%
            end with
			next            
		end if
	end with
	next
else
	if not oProceso.grupos is nothing then        
		for each oGrupo in oProceso.Grupos
		with oGrupo
		%>
			oCfg = new CfgGrupo (<%=JSBool(oGrupo.DefDestino)%>,<%=JSBool(oGrupo.DefFormaPago)%>,<%=JSBool(oGrupo.DefFechasSum)%>,<%=JSBool(oGrupo.DefEspecificaciones)%>,<%=JSBool(oGrupo.HayAtributos)%>,<%=JSBool(oGrupo.HayCostesDescuentos)%>)
			oGrupo = new Grupo (proceso,'<%=JSText(.Codigo)%>','<%=JSText(.den)%>','<%=JSText(.descripcion)%>',<%if .DefDestino then%>new Destino('<%=JSText(.destCod)%>','<%=JSText(.Destino.Den)%>')<%else%>null<%end if%>,'<%=JSText(.pagCod & " - " & .pagDen)%>',<%=JSDate(.FechaInicioSuministro)%>,<%=JSDate(.FechaFinSuministro)%>,'<%=JSText(.Esp)%>',<%=JSNum(.NumItems)%>,oCfg,null,<%=JSNum(.importeTotal)%>,"<%=JSText(oGrupo.ObsAdjuntos)%>",<%=JSNum(.posicionPuja)%>,<%=JSNum(.ID)%>,<%=JSNum(.importeBruto)%>,<%=JSBool(.HayEscalados)%>, <%=JSNum(oGrupo.Bloqueado)%>)
			proceso.anyaGrupo(oGrupo)
		<%
            if .hayEscalados then    
                .cargarEscalados(CiaComp)        
	            for each oEscalado in .Escalados
	            %>
		            //Parametros de  Escalado(id, inicio, fin, precio,precioValido,valorNum,precioGS,precioOferta,cantidad)
		            oEsc = new Escalado (<%=oEscalado.id%>,<%=JSNUM(oEscalado.inicial)%>,<%=JSNUM(oEscalado.final)%>,null,null,null,null,null,null)
		            oGrupo.anyaEscGrupo(oEsc)		
	            <%
	            next
            end if
		end with
		next
	end if
end if%>



// carga de los valores de los costes / descuentos de ambito grupo para todos los grupos
<%

set oAtribOfe = nothing
set	oOferta.OfertaGrupos = oRaiz.Generar_COfertaGrupos
if not oProceso.Grupos is nothing then
    for each oGrupo in oProceso.Grupos
	    set oOfeGrupo= oOferta.OfertaGrupos.Add (oOferta,oGrupo)
	    oOfeGrupo.CargarAtributosOfertados CiaComp, codMon
	    set oAtribOfe = oOfeGrupo.AtribsOfertados %>

        for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != '<%=oGrupo.Codigo%>';i++)
		        {
		        }
	    vObj = proceso.grupos[i]

        <%if not (oProceso.CostesDescTotalOferta is nothing) then %>
        <%for each oCosDes in oProceso.CostesDescTotalOferta
        if oCosDes.Ambito=2 then
        with oCosDes%>   
        
	        costedescvalor = new Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>,<%
            if not oAtribOfe is nothing then
            if not oAtribOfe.Item(cstr(.PAID)) is nothing then
                if (.operacion = "+" or  .operacion = "-") then
	                vValor = oAtribOfe.Item(cstr(.PAID)).valor
                else
	                vValor = oAtribOfe.Item(cstr(.PAID)).valor 
                end if
                vImporte = oAtribOfe.Item(cstr(.PAID)).ImporteParcial
            else
                vValor = null
                vImporte = null
            end if
            else
                vValor = null
                vImporte = null
            end if
            response.write  JSNum(vValor) 
            response.write  "," + JSBool(.obligatorio)
            response.write "," & JSNum(.valorMin) &  "," & JSNum(.valorMax) 
            if .intro = 1 then
	            str=",["
	            for each oValor in .Valores
		            if str<>",[" then
			            str = str & ","
		            end if
                    if (.operacion = "+" or  .operacion = "-") then
			            vValor = oValor.Valor
		            else
			            vValor = oValor.Valor 
                    end if
                    str = str & JSNum(vValor)
                next
                response.write str & "],proceso,'" & JSText(.operacion) & "'," & JSNum(.aplicar) & ", null"
                response.write ", " & JSNum(vImporte) & ")"
	        else
		        response.write ",null,proceso,'" & JSText(.operacion) & "',"  & JSNum(.aplicar) & ", null"
                response.write ", " &  JSNum(vImporte) & ")"
	        end if

             %>
	        vObj.anyaCosteDescuento(costedescvalor)
        <%
        end with
        end if
        next%>
        <%end if %>

        <%if not (oProceso.CostesDescTotalGrupo is nothing) then %>

        <%for each oCosDes in oProceso.CostesDescTotalGrupo
        with oCosDes
            if .Ambito=2 then%>
	            costedescvalor = new Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>,<%
                if not oAtribOfe is nothing then
                if not oAtribOfe.Item(cstr(.PAID)) is nothing then
                    if (.operacion = "+" or  .operacion = "-") then
	                    vValor = oAtribOfe.Item(cstr(.PAID)).valor
                    else
	                    vValor = oAtribOfe.Item(cstr(.PAID)).valor 
                    end if
                    vImporte = oAtribOfe.Item(cstr(.PAID)).ImporteParcial
                else
                    vValor = null
                    vImporte = null
                end if
                else
                    vValor = null
                    vImporte = null
                end if

                response.write  JSNum(vValor) 
                response.write  "," + JSBool(.obligatorio)
                response.write "," & JSNum(.valorMin) &  "," & JSNum(.valorMax) 
                if .intro = 1 then
	                str=",["
	                for each oValor in .Valores
		                if str<>",[" then
			                str = str & ","
		                end if
                        if (.operacion = "+" or  .operacion = "-") then
			                vValor = oValor.Valor
		                else
			                vValor = oValor.Valor 
                        end if
                        str = str & JSNum(vValor)
                    next
                    response.write str & "],proceso,'" & JSText(.operacion) & "'," & JSNum(.aplicar) & ", null"
                    response.write ", " &  JSNum(vImporte) & ")"
	            else
		            response.write ",null,proceso,'" & JSText(.operacion) & "',"  & JSNum(.aplicar) & ", null"
                    response.write ", " &  JSNum(vImporte) & ")"
	            end if
                 %>
	            vObj.anyaCosteDescuento(costedescvalor)
        <%end if
        end with
        next%>
        <%end if %>

        <%if not (oProceso.CostesDescTotalItem is nothing) then %>
        <%for each oCosDes in oProceso.CostesDescTotalItem
        with oCosDes
            if .Ambito=2 then%>
	            costedescvalor = new Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>,<%
                if not oAtribOfe is nothing then
                if not oAtribOfe.Item(cstr(.PAID)) is nothing then
                    if (.operacion = "+" or  .operacion = "-") then
	                    vValor = oAtribOfe.Item(cstr(.PAID)).valor
                    else
	                    vValor = oAtribOfe.Item(cstr(.PAID)).valor 
                    end if
                    vImporte = oAtribOfe.Item(cstr(.PAID)).ImporteParcial
                else
                    vValor = null
                    vImporte = null
                end if
                else
                    vValor = null
                    vImporte = null
                end if
                response.write  JSNum(vValor) 
                response.write  "," + JSBool(.obligatorio)
                response.write "," & JSNum(.valorMin) &  "," & JSNum(.valorMax) 
                if .intro = 1 then
	                str=",["
	                for each oValor in .Valores
		                if str<>",[" then
			                str = str & ","
		                end if
                        if (.operacion = "+" or  .operacion = "-") then
			                vValor = oValor.Valor
		                else
			                vValor = oValor.Valor 
                        end if
                        str = str & JSNum(vValor)
                    next
                    response.write str & "],proceso,'" & JSText(.operacion) & "'," & JSNum(.aplicar) & ", null"
                    response.write ", " &  JSNum(vImporte) & ")"
	            else
		            response.write ",null,proceso,'" & JSText(.operacion) & "',"  & JSNum(.aplicar) & ", null"
                    response.write ", " &  JSNum(vImporte) & ")"
	            end if

                 %>
	            vObj.anyaCosteDescuento(costedescvalor)
        <%end if
        end with
        next%>
        <%end if %>

        <%if not (oProceso.CostesDescPrecItem is nothing) then %>
        <%for each oCosDes in oProceso.CostesDescPrecItem
        with oCosDes
            if .Ambito=2 then%>
	            costedescvalor = new Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>,<%
                if not oAtribOfe is nothing then
                if not oAtribOfe.Item(cstr(.PAID)) is nothing then
                    if (.operacion = "+" or  .operacion = "-") then
	                    vValor = oAtribOfe.Item(cstr(.PAID)).valor
                    else
	                    vValor = oAtribOfe.Item(cstr(.PAID)).valor 
                    end if
            //        If vImporteAnterior = 0 then
            //            vImporte = oOferta.importeBruto
            //        else
            //            vImporte = vImporteAnterior
            //        end if
            //        vImporteAnterior = oAtribOfe.Item(cstr(.PAID)).ImporteParcial
                    vImporte = oAtribOfe.Item(cstr(.PAID)).ImporteParcial
                else
                    vValor = null
                    vImporte = null
                end if
                else
                    vValor = null
                    vImporte = null
                end if
                response.write  JSNum(vValor) 
                response.write  "," + JSBool(.obligatorio)
                response.write "," & JSNum(.valorMin) &  "," & JSNum(.valorMax) 
                if .intro = 1 then
	                str=",["
	                for each oValor in .Valores
		                if str<>",[" then
			                str = str & ","
		                end if
                        if (.operacion = "+" or  .operacion = "-") then
			                vValor = oValor.Valor
		                else
			                vValor = oValor.Valor 
                        end if
                        str = str & JSNum(vValor)
                    next
                    response.write str & "],proceso,'" & JSText(.operacion) & "'," & JSNum(.aplicar) & ", null"
                    response.write ", " &  JSNum(vImporte) & ")"
	            else
		            response.write ",null,proceso,'" & JSText(.operacion) & "',"  & JSNum(.aplicar) & ", null"
                    response.write ", " &  JSNum(vImporte) & ")"
	            end if
                 %>
	            vObj.anyaCosteDescuento(costedescvalor)
        <%end if
        end with
        next%>
        <%end if 
    next
end if



%>
//Fin carga de valores de costes/desc de ambito grupo para todos los grupo


    </script>




    <script>

var m
var t
var l
var la
var tb, tb2, tb3, tbvar
var wt
var wto
var ha
var hSwitch
m=7
t=42 //Ancho del men� � Top de contenidos
l=5 //Margen izquierdo
la = 15
tb1=28 //Ancho de la barra de botones (1 fila)
tb2=54 //Ancho de la barra de botones (2 filas)
tb3=78 //Ancho de la barra de botones (3 filas)

<%if oProceso.subasta then %>
    <%if oProceso.verPrecioGanador then %>
        tbvar= tb2
    <%else %>
        tb3 = tb2
        //tb2 = tb1
        tbvar= tb1
        tb = tb1        
    <%end if %>
<%else%>
    tb = tb1
    tbvar= tb1
<%end if %>


wt=240
wto=250
if (proceso.estado==1) //Alto del t�tulo de cada apartado
	ha=52 //26
else
	ha=52
hSwitch = 20

var esSubasta = <%=JSBool(oProceso.subasta)%>
var pedirAlterPrecios = <%=JSBool(oProceso.PedirAlterPrecios)%>
var solicitarCantMax = <%=JSBool(oProceso.SolicitarCantMax)%>

    </script>
    <%
'******************************************************************************
'**** FUNCION QUE MUESTRA LOS DATOS GENERALES DEL PROCESO *********************
'******************************************************************************
    %>
    <script>

<%if oProceso.subasta then %>

<%if not isnull (oProceso.FechaMinimoLimOfertas) or not isnull(oProceso.FechaApertura)then%>

oIntervalRestante = setInterval("actualizarTiempoRestante()",1000)



var bGuardando = true

var oInterval = null
var verDesdePrimeraPuja
var hayOfertaProveedor

verDesdePrimeraPuja = <%=lcase(oProceso.verDesdePrimeraPuja)%>
hayOfertaProveedor = <%=lcase(oProceso.hayOfertaProveedor(CiaComp,CodProve) )%>



    </script>
    <script>
/*
''' <summary>
''' Cada segundo se actualiza el contador de la subasta. Inicializa el timer para cargar la oferta minima. Cuando llega 
'''	a 0 el plazo de la subasta ha finalizado y cierra la subasta. 
''' Lo de llegar a 0 no siempre significa q el plazo de la subasta ha finalizado, pq si otro proveeedor ha pujado a
'''	falta de pocos segundos, y esta configurado q el plazo se amplia, realmente no se ha acabado pero este explorer aun
'''	no se ha enterado. Se entera con la carga de la oferta minima (timer 10 segundos). Por esto cuando se llega a 0
''' se va a bbdd a mirar la fecha hora de plazo.
''' </summary>     
''' <returns>Nada</returns>
''' <remarks>Llamada desde: timer sistema; Tiempo m�ximo:1sg al llegar la cuenta a 0 e ir a bbdd</remarks>
*/

var cambiaDeEstado
var seAbreSobre = false
var bAbierta
var bSobreCerrado = false
var alertaAperturaSubasta = false
var primeraCarga = true
var sinPujas

if (verDesdePrimeraPuja==false || (verDesdePrimeraPuja == true && hayOfertaProveedor))
    sinPujas = false
else
    sinPujas = true 


var tipoProceso
tipoProceso = <%=JSnum(oProceso.tipo)%>


/*
''' <summary>
''' actualizar Tiempo Restante de subasta
''' </summary>     
''' <remarks>Llamada desde: js\cargarsubasta.asp    function cambiarFechaLimite; Tiempo m�áximo: 0</remarks>*/
function actualizarTiempoRestante()
{
var dAhora

dAhora = new Date()

var dAhoraReal = new Date(dFechaInicialServer.getFullYear(),dFechaInicialServer.getMonth(),dFechaInicialServer.getDate(),dFechaInicialServer.getHours(), dFechaInicialServer.getMinutes(), dFechaInicialServer.getSeconds(),dFechaInicialServer.getMilliseconds() + (dAhora - dFechaInicialClient))

if (tipoProceso==1)
{
	if (vFechaAperturaSobre > dAhoraReal )
		{
		bSobreCerrado = true
		}
	else
		bSobreCerrado= false
}
else
{
    bSobreCerrado= false
}


if (vFechaApertura > dAhoraReal )
	{
	//todava no est� abierta la subasta
    
	bAbierta = false
	if (oInterval==null)
		{		
		oInterval = setInterval("cargarOfertasMinimas()",10000)
		}
    document.getElementById("imgEnviar").style.visibility="hidden"


	ret = restaFecha(dAhoraReal, vFechaApertura)

	if (!ret)
		{
		}
	else
		{
		if (document.getElementById("divTiempoRestante"))
			document.getElementById("divTiempoRestante").innerHTML = ret 
		if (document.getElementById("divTiempoRestantePuja"))
			{
			document.getElementById("divTiempoRestantePuja").innerHTML = ret 
			}
		}
	}
else
	{

    if (!bAbierta && !primeraCarga) 
    {
        bAbierta = true
        mostrarCronometros()
        actualizarSubasta()
    }
	bAbierta = true
	if (oInterval==null)
		{		
		oInterval = setInterval("cargarOfertasMinimas()",10000)
        mostrarCronometros()
		document.getElementById("imgEnviar").style.visibility="visible"
		}

        if (iEstadoSubasta == 2) document.getElementById("imgEnviar").style.visibility="visible"
        else document.getElementById("imgEnviar").style.visibility="hidden"



    switch (iEstadoSubasta)
    {
    case 2: //en proceso
	    ret = restaFecha(dAhoraReal, vFechaLimite)
        break;
    case 3:
        if (vFechaReinicio != "")  
            if (dAhoraReal > vFechaReinicio)  
                ret = den[137]
            else 
            ret = restaFecha(dAhoraReal, vFechaReinicio)            
        else 
            ret = den[136]
        break;
    case 4:
	    ret = restaFecha(dAhoraReal, dAhoraReal)
        break;
    }

	if (!ret)
		{
            if (!ComprobarFechaFin())
            {
			window.clearInterval(oIntervalRestante)
            window.clearInterval(oInterval)        	
            oIntervalRestante=null		
            oInterval=null
            
            document.location.href="<%=application("RUTASEGURA")%>script/solicitudesoferta/pujacerrada.asp?Idioma=<%=Idioma%>&Anyo=<%=anyoproceso%>&Gmn1=<%=gmn1proceso%>&Proce=<%=codproceso%>&CiaComp=<%=CiaComp%>"
            }
		}
	else
		{
		if (document.getElementById("divTiempoRestante"))
			document.getElementById("divTiempoRestante").innerHTML = ret
		if (document.getElementById("divTiempoRestantePuja"))
			{
			document.getElementById("divTiempoRestantePuja").innerHTML = ret
			}
		}
	}

    //Comprobar Apertura de sobre
<%if oProceso.tipo = 1 then%>
        if (vFechaAperturaSobre > dAhoraReal )
            {
	        bSobreCerrado = true
            }
        else
	        bSobreCerrado= false
<%else%>
    bSobreCerrado= false
<%end if %>

        if (!bSobreCerrado)
        {

            if (seAbreSobre) 
            {
                mostrarCronometros()
                actualizarSubasta()
            }
        }    
        seAbreSobre = bSobreCerrado
        primeraCarga = false



}



<%end if%>
<%end if%>


/*
''' <summary>
''' Cada vez q se cambia el plazo se avisa a los diferentes proveeedores
''' </summary>
''' <param name="newFec">Nueva fecha hora de plazo</param>  
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cargarofemin.asp (en el fondo el timer de 10sg) ; Tiempo m�ximo:0</remarks>
*/
function cambiarFechaLimite(newFec)
{

var dAhora
var dAhoraReal 

if (newFec-vFechaLimite != 0)
	{

		dAhora = new Date()

		dAhoraReal = new Date(dFechaInicialServer.getFullYear(),dFechaInicialServer.getMonth(),dFechaInicialServer.getDate(),dFechaInicialServer.getHours(), dFechaInicialServer.getMinutes(), dFechaInicialServer.getSeconds(),dFechaInicialServer.getMilliseconds() + (dAhora - dFechaInicialClient))
		ret = restaFecha(dAhoraReal, newFec)

		vFechaLimite=newFec
	    if (dAhoraReal > newFec) // Se acab� la subasta
            {
            alert("<%=JSAlertText(den(44))%>\n<%=JSAlertText(den(138))%>")
            
            window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/pujacerrada.asp?anyo=<%=anyoproceso%>&gmn1=<%=gmn1proceso%>&proce=<%=codproceso%>", "default_main")
            }
        else
            {
		    alert("<%=JSAlertText(den(44))%>\n<%=JSAlertText(den(31))%>:" + ret)
            }
		if(oIntervalRestante==null)
			{
				oIntervalRestante = setInterval("actualizarTiempoRestante()",1000)
			}
	}
else
	{
		if(oIntervalRestante==null)
		{
			bGuardando = true
			window.clearInterval(oIntervalRestante)
		    dAhora = new Date()
    
    		dAhoraReal = new Date(dFechaInicialServer.getFullYear(),dFechaInicialServer.getMonth(),dFechaInicialServer.getDate(),dFechaInicialServer.getHours(), dFechaInicialServer.getMinutes(), dFechaInicialServer.getSeconds(),dFechaInicialServer.getMilliseconds() + (dAhora - dFechaInicialClient))
            
            if (dAhoraReal > vFechaLimite)
                
			    window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/pujacerrada.asp?anyo=<%=anyoproceso%>&gmn1=<%=gmn1proceso%>&proce=<%=codproceso%>", "default_main")
		}
	}
	

}

var bEstamosEnProceso
if (esSubasta){
	bEstamosEnProceso = false
	bEstamos="0"	
}
else{
	bEstamosEnProceso = true
	bEstamos="1"	
}


var importeBrutoOferta
var hayCostesDescuentosItems=false
var noScroll = false

/*
''' <summary>
''' Mostrar por pantalla la subasta
''' </summary>
''' <param name="vProc">Objeto proceso q contiene toda la oferta</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: datosProceso ; Tiempo m�ximo: 0</remarks>
winEspera.asp
*/
function mostrarProceso(vProc){
    var str
    str = ""

    if (esSubasta){
	    str+= "	<table width=100%>"
	    str+= "		<tr>"
	    str+= "			<td><div id=divCronometro></div>" 
	    str+= "			</td>"
	    str+= "		</tr>" 
	    str+= " </table>"
    }

    str+= "	<table width=100%>"
    str+= "		<tr>"
    str+= "			<td>" 
    str+= "				<table>"
    str+= "					<tr>"	
    <%if oProceso.MaximoNumeroOfertas<>1 then%>
    str+= "						<td>" + cajetin('<%=JSText(den(5))%>',date2str(vProc.fecUltimaOfe,vdatefmt)) + "</td>"
    str+= "						<td>" + cajetin('<%=JSText(den(6))%>',num2str(vProc.totOfertas,vdecimalfmt,vthousanfmt,vprecisionfmt)) + "</td>"
    <%end if%>
    str+= "					</tr>"
    str+= "				</table>"
    str+= "			</td>"
    str+= "		</tr>" 
    str+= "		<tr>"
    str+= "			<td>" 
    str+= "				<table>"
    str+= "					<tr>"

    <%if  not isnull(oProceso.FechaMinimoLimOfertas) then%>
	    var FechaLocal

	    FechaLocal = new Date()
        //vFechaLimite ya est� en zona horaria de cliente
        FechaLocal = new Date(vFechaLimite.getFullYear(), vFechaLimite.getMonth(), vFechaLimite.getDate(), vFechaLimite.getHours(), vFechaLimite.getMinutes())
	    str+= "						<td>" + cajetin("<%=JSText(den(63))%>","<span class='numero negrita fgrande'>&nbsp;</span>" + date2str(FechaLocal,vdatefmt,-1)  ) + "</td>"

    <%else%>
        str+= "						<td>" + cajetin("<%=JSText(den(63))%>","<span class='numero negrita fgrande'>&nbsp;</span>" + date2str(vFechaLimite,vdatefmt,-1)  ) + "</td>"
    <%end if%>

    str+= "					</tr>"
    str+= "				</table>"
    str+= "			</td>"
    str+= "		</tr>"

    if (vProc.cfg.dest){
        str+= "		<tr>"
        str+= "			<td>" 
        str+= "				<table>"
        str+= "					<tr>"
        str+= "						<td>" + cajetin('<%=JSText(den(9))%>',vProc.dest.cod + ' - ' + vProc.dest.den,null,'detalleDestino',vProc.dest.cod) + "</td>"
        str+= "					</tr>"
        str+= "				</table>"
        str+= "			</td>"
        str+= "		</tr>"
    }

    if (vProc.cfg.pag){
        str+= "		<tr>"
        str+= "			<td>" 
        str+= "				<table>"
        str+= "					<tr>"
        str+= "						<td colspan = 3>" + cajetin('<%=JSText(den(8))%>',vProc.pag) + "</td>"
        str+= "					</tr>"
        str+= "				</table>"
        str+= "			</td>"
        str+= "		</tr>"
    }

    if (vProc.cfg.fecSum){
        str+= "		<tr>"
        str+= "			<td>" 
        str+= "				<table>"
        str+= "					<tr>"
        str+= "						<td>" + cajetin('<%=JSText(den(10))%>',date2str(proceso.finisum,vdatefmt)) + "</td>"
        if (vProc.cfg.mostrarFinSum)
	        str+= "						<td>" + cajetin('<%=JSText(den(11))%>',date2str(proceso.ffinsum,vdatefmt)) + "</td>"
        str+= "					</tr>"
        str+= "				</table>"
        str+= "			</td>"
        str+= "		</tr>"
    }

    if (vProc.cfg.especs){
	    if (vProc.esp || vProc.especs.length>0 || vProc.Aespecs.length > 0){
		    str+= "		<tr>"
		    str+= "			<td>" 
		    str+= "				<table width=100%>"
		    str+= "					<tr>"
		    str+= "						<td>" + cajetin('<%=JSText(den(12))%>',mostrarEspecificaciones(vProc),"width:100%;") + "</td>"
		    str+= "					</tr>"
		    str+= "				</table>"
		    str+= "			</td>"
		    str+= "		</tr>"
		}


        if (vProc.cfg.costesDescuentos){
            importeBrutoOferta = 0
            if(bHayCostesDescuentosGrupo(vProc)){
		        str+= "		<tr>"
		        str+= "			<td>" 
		        str+= "				<table width=100%>"
		        str+= "					<tr>"
		        str+= "						<td>" + cajetin('<%=JSText(den(111))%>',mostrarCostesDescuentosGrupo(vProc),"width:100%;") + "</td>" //JVS Idioma Costes/Descuentos de grupo
		        str+= "					</tr>"
		        str+= "				</table>"
		        str+= "			</td>"
		        str+= "		</tr>"
            }

            if(bHayCostesDescuentosOferta(vProc)){
		        str+= "		<tr>"
		        str+= "			<td>" 
		        str+= "				<table width=100%>"
		        str+= "					<tr>"
		        str+= "					    <td>" + cajetin('<%=JSText(den(112))%>',mostrarCostesDescuentos(vProc),"width:100%;") + "</td>" //JVS Idioma  Costes/Descuentos de la oferta
		        str+= "					</tr>"
		        str+= "				</table>"
		        str+= "			</td>"
		        str+= "		</tr>"
            }
        }
    }

    if (vProc.Aespecs.length > 0)    {
	    str+= "		<tr>"
	    str+= "			<td>" 
	    str+= "				<table width=100%>"
	    str+= "					<tr>"
	    str+= "						<td>" + cajetin('<%=JSText(den(83))%>',mostrarAtributosEspecificacion(vProc,0),"width:100%;") + "</td>"
	    str+= "					</tr>"
	    str+= "				</table>"
	    str+= "			</td>"
	    str+= "		</tr>"

    }

    str+= "	</table>"

    document.getElementById("divApartadoS").innerHTML=str

	if (esSubasta)	
		mostrarCronometros()	
}

</script>

<script language="JavaScript" type="text/JavaScript">
    var oMonedas
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo m�ximo:0</remarks>*/
    function init(){
        if (<%=application("FSAL")%> == 1){
            Ajax_FSALActualizar3(); //registro de acceso
        }
        document.getElementById('tablemenu').style.display = 'block';
        document.images["imgIzquierda"].style.visibility = "visible"
        document.getElementById("divBotones").style.visibility="visible"
        mostrarConfigProceso(proceso) //Dibujar�a el �rbol (Esta funci�n est� en config.asp)
        if (esSubasta){
            modoSubasta()
        }
        else{
            datosProceso()
            resize()
            document.getElementById("divBotones").style.visibility="visible"
            document.getElementById("divArbol").style.visibility="visible"
            document.getElementById("divOcultarArbol").style.visibility="visible"
            document.images["imgIzquierda"].style.visibility="visible"
            if (iIrAApartado != "")
                actualizarApartado(iIrAApartado)	
        }
    }
</script>


    <%
if oProceso.subasta then
'******************************************************************************
'**** MODO SUBASTA    *****************
'******************************************************************************
    %>
    <script>

function ponerFecha(vFec)
{
proceso.fecValidez = str2date(vFec,vdatefmt)
prepararOfertaAEnviar(3, (linkRecalculo == 1 ? 1 : 0))
}

var bPujando = false
function noPujando()
{
    bPujando = false
    OcultarDivCostesDescItem()
}
function modoSubasta(e) {

 

    ocultaArbol()
if (document.getElementById("divApartado").aptdoAnterior)
	eval (document.getElementById("divApartado").aptdoAnterior)

bPujando=true
dibujarApartado("")
resize()
if (proceso.subastaCargada==false)
	dibujarBarra("<%=den(28)%>")

document.getElementById("divApartado").aptdoAnterior = "noPujando()"
proceso.cargarSubasta("fraOfertaServer")

}


function mostrarSubasta()
{


if (document.getElementById("divPuja"))
	{
	hAptdo = document.getElementById("divApartadoS").style.height
	hCabecera = (hCFijo + wDesviacion)

	document.getElementById("divPuja").style.overflow="hidden"
	document.getElementById("divPuja").style.top = document.getElementById("divApartado").style.top
	document.getElementById("divPuja").style.left = document.getElementById("divApartado").style.left
	document.getElementById("divPuja").style.height = document.getElementById("divApartado").style.height
	document.getElementById("divPuja").style.width = document.getElementById("divApartado").style.width
	document.getElementById("divPuja").style.clip = document.getElementById("divApartadoS").style.clip
	document.getElementById("divPuja").style.visibility="visible"
	document.getElementById("divApartadoS").style.visibility = "hidden"
	document.getElementById("divApartado").style.visibility = "hidden"

	//muestra los importes netos de los items, una vez aplicados los costes/descuentos que correspondan
    for (i=0;i<itemsPuja.length;i++)
	    {
	        strDetOferta = "<b>" + num2str(itemsPuja[i].importeNeto, vdecimalfmt, vthousanfmt, vprecisionfmt) + "</b>"
	        document.getElementById("divTotalLinea4" + "_" + itemsPuja[i].id).innerHTML = strDetOferta
            nombre = "txtPujaPU_" + itemsPuja[i].id
            cargarPrecioItem(itemsPuja[i].precioNeto, nombre)
	    } 
	
	}
for (i=0;i<proceso.grupos.length;i++)
	{
	document.images["imgGrupoA" +  proceso.grupos[i].cod ].style.visibility=parseFloat(document.getElementById("divArticuloPuja" + proceso.grupos[i].cod).style.height) == hCabeceraGrupo ? "hidden" : "visible"
	document.images["imgGrupoB" +  proceso.grupos[i].cod ].style.visibility=parseFloat(document.getElementById("divArticuloPuja" + proceso.grupos[i].cod).style.height) != hCabeceraGrupo ? "hidden" : "visible"
	}

}

function cargarPrecioItem(precio,nombre)
{
    document.forms["frmOfertaAEnviar"].elements[nombre].value = num2str(precio, vdecimalfmt, vthousanfmt, vprecisionfmt)
    

}
//''' <summary>
//''' Cada vez q scrolas los items de una puja, hay q scrolar las pujas de todos los items.
//''' </summary>
//''' <remarks>Llamada desde: cumpoferta.js (function resize())     js\actualizarsubasta.asp (onScroll)     js\cargarsubasta.asp (onScroll)
//'''   js\cargarresultadopuja.asp (onScroll) ; Tiempo m�ximo: 0,2</remarks>
function scrollFijasPuja()
{
    document.getElementById("divArticuloPujaC").style.left = -parseFloat(document.getElementById("divArticuloPujaScroll").scrollLeft) + 'px';
}

function guardarSubasta()
{

}

//''' <summary>
//''' Oculta la puja del grupo
//''' </summary>
//''' <remarks>Llamada desde: cargarresultadopuja.asp (2 llamadas onclick)  cumpoferta.asp (4 llamadas onclick)     js\actualizarsubasta.asp (1 llamada onclick)
//''    js\cargarsubasta.asp (1 llamada onclick) ; Tiempo m�ximo: 0,2</remarks>
function ocultarPujaGrupo(grupo) {
var i
var calcPx
for (i=0;i<proceso.grupos.length;i++)
	{
	if (proceso.grupos[i].cod == grupo)
		{
		if (parseFloat(document.getElementById("divArticuloPuja" + proceso.grupos[i].cod).style.height) != (hCabeceraGrupo+ wBorde + hDesviacion+5)) {
		    calcPx = hCabeceraGrupo + wBorde + hDesviacion + 5
		    document.getElementById("divArticuloPuja" + proceso.grupos[i].cod).style.height = calcPx + 'px';
			document.images["imgGrupoA" + proceso.grupos[i].cod].style.visibility="hidden" 
			document.images["imgGrupoB" + proceso.grupos[i].cod].style.visibility="visible" 
			}
		else {
		    calcPx = proceso.grupos[i].numItems * (hCFijo + wBorde + hDesviacion) + hCabeceraGrupo + hCabeceraGrupoDetalle + wBorde + hDesviacion + 10
		    document.getElementById("divArticuloPuja" + proceso.grupos[i].cod).style.height = calcPx + 'px';
			document.images["imgGrupoA" + proceso.grupos[i].cod].style.visibility="visible" 
			document.images["imgGrupoB" + proceso.grupos[i].cod].style.visibility="hidden" 
			}

       
		document.getElementById("divArticuloPuja" + proceso.grupos[i].cod).style.clip = "rect(0px,auto," + parseFloat(document.getElementById("divArticuloPuja" + proceso.grupos[i].cod).style.height) + "px,0px)"
		}
	else
		{
		    if (proceso.grupos[i - 1]) {
		        calcPx = parseFloat(document.getElementById("divArticuloPuja" + proceso.grupos[i - 1].cod).style.top) + parseFloat(document.getElementById("divArticuloPuja" + proceso.grupos[i - 1].cod).style.height)
		        document.getElementById("divArticuloPuja" + proceso.grupos[i].cod).style.top = calcPx + 'px';
		    }
		}
	}
}
    </script>


    <%
else
    %>
    <script>
var bPedirFecha=true
function ponerFecha(vFec)
{
proceso.fecValidez = str2date(vFec,vdatefmt)
bPedirFecha=false
prepararOfertaAEnviar(2, (linkRecalculo == 1 ? 1 : 0))
}
    </script>
    <%
end if
    %>


    <div style="visibility: hidden; position: absolute; top: 15px; height: 90%; width: 90%; left: 0px; clip: rect(0,100%,100%,0); z-index: 1000;" name="divBloqueo" id="divBloqueo">
        <img src="images/transparente.gif" width="90%" height="90%">
    </div>


    <div name="divBotones" id="divBotones" style="visibility: hidden">
        <table width="10%" border="0">
            <tr>
                <%if iif(oProceso.MaximoNumeroOfertas<>0,oProceso.MaximoNumeroOfertas<> oOferta.TotEnviadas,true) then%>
                <td class="todoCentro">
                    <a title="<%=den(1)%>" href="javascript:void(null)" onclick="prepararOfertaAEnviar(1,(linkRecalculo==1?1:0))">
                        <img border="0" name="imgGuardar" id="imgGuardar" src="images/guardar_off.gif" width="23" height="22"></a>
                </td>
                <td class="todoCentro">
                    <a title="<%=den(2)%>" href="javascript:void(null)">
                        <img border="0" name="imgExportar" id="imgExportar" src="images/Excel_off.gif" onclick="descargarExcel(true)" width="23" height="22"></a>
                </td>
                <%end if%>
                <td class="todoCentro">
                    <a title="<%=den(3)%>" href="javascript:void(null)" onclick="imprimirOferta(true)">
                        <img border="0" name="imgimprimir" id="imgimprimir" src="images/imprimir_off.gif" width="23" height="22"></a>
                </td>
                <td class="todoCentro">
                    <a title="<%=den(62)%>" href="javascript:void(null)" onclick="descargarEspecific()">
                        <img border="0" name="imgdescargar" id="imgdescargar" src="images/DescargarEspecif_off.gif" width="23" height="22"></a>
                </td>
                <td class="todoCentro">&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
                <%

        if iif(oProceso.MaximoNumeroOfertas<>0,oProceso.MaximoNumeroOfertas> oOferta.TotEnviadas,true) then%>
                <td class="todoCentro">
                    <div name="imgEnviar" id="imgEnviar" style="visibility: <%if not oProceso.subasta then%>visible<%else%>hidden<%end if%>">
                        <a title="<%=den(4)%>" href="javascript:void(null)" onclick="prepararOfertaAEnviar(2,(linkRecalculo==1?1:0))">
                            <img border="0" src="images/enviar_off.gif" width="40" height="22"></a>
                    </div>
                </td>
                <%end if%>
                <%If not oProceso.Ofertado then%>
                <td class="todoCentro">
                    <div name="divNoOfertar" id="divNoOfertar">
                        <%if oProceso.NoOfe then%>
                        <a title="<%=den(77)%>" href="javascript:void(null)" onclick="noOfertar(<%=JSNum(oproceso.anyo)%>,'<%=JSText(oproceso.gmn1cod)%>',<%=JSNum(oproceso.cod)%>,0)">
                            <img border="0" name="imgNoofertar" id="imgNoofertar" src="images/nooferta_on.gif" width="16" height="16"></a>
                        <%else%>
                        <a title="<%=den(77)%>" href="javascript:void(null)" onmouseover="document.images['imgNoofertar'].src='images/nooferta_on.gif'" onmouseout="document.images['imgNoofertar'].src='images/nooferta_off.gif'" onclick="noOfertar(<%=JSNum(oproceso.anyo)%>,'<%=JSText(oproceso.gmn1cod)%>',<%=JSNum(oproceso.cod)%>,0)">
                            <img border="0" name="imgNoofertar" id="imgNoofertar" src="images/nooferta_off.gif" width="16" height="16"></a>
                        <%end if%>
                    </div>
                </td>
                <%end if%>
            </tr>

            <%if oproceso.subasta then %>
            <tr>
                <td class="todoCentro" valign="top">
                    <div id="Ganadora" style="display: none; margin-bottom: 5pt">
                        <a title="<%=JSText(den(124))%>" href="javascript:void(null)" onclick="CargarOfertaGanadora()">
                            <img border="0" src="images/igualar-ganadora.png" width="16" height="16">
                        </a>
                    </div>
                    <div id="Restaurar" style="display: none">
                        <a title="<%=JSText(den(125))%>" href="javascript:void(null)" onclick="document.location.reload()">
                            <img border="0" src="images/cargar-datos.png" width="16" height="16">
                        </a>
                    </div>
                </td>
            </tr>
            <%end if %>
        </table>
    </div>

    <div name="divOcultarArbol" id="divOcultarArbol" style="visibility: hidden;">
        <table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="<%=cMOARBOL%>">
            <tr>
                <td>
                    <a href="javascript:void(null)" onclick="ocultaArbol()">
                        <img name="imgIzquierda" id="imgIzquierda" style="visibility: hidden;" border="0" src="images/izquierda2.gif" width="19" height="12">
                    </a>
                </td>
                <td>
                    <a href="javascript:void(null)" onclick="ocultaArbol()" class="lnkMOArbol negrita">
                        <nobr><%=den(75)%></nobr>
                    </a>
                </td>
                <td width="100%">&nbsp;
                </td>
            </tr>
        </table>
    </div>

    <div name="divArbol" id="divArbol">
    </div>


    <div name="divMostrarArbol" id="divMostrarArbol" style="visibility: hidden; overflow: hidden;">
        <table id="tablaMostrarArbol" width="100%" bgcolor="<%=cMOARBOL%>">
            <tr>
                <td height="10px" valign="top">
                    <a href="javascript:void(null)" onclick="muestraArbol()">
                        <img name="imgDerecha" id="imgDerecha" style="visibility: hidden;" border="0" src="images/derecha2.gif" width="19" height="12">
                    </a>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <a href="javascript:void(null)" onclick="muestraArbol()" class="lnkMOArbol negrita">
                        <img border="0" src="images/<%=Idioma%>/mostrararbol.gif">
                    </a>
                </td>
            </tr>
            <tr>
                <td height="100%">&nbsp;
                </td>
            </tr>
        </table>
    </div>




    <div name="divAlerta" id="divAlerta">
    </div>

    <script>
function ocultaArbol() {

wt = 0
tb = tbvar
wto=25
hSwitch = 150
document.getElementById("divArbol").style.visibility="hidden"
document.getElementById("divMostrarArbol").style.visibility="visible"
document.getElementById("divOcultarArbol").style.visibility="hidden"
document.images["imgDerecha"].style.visibility="visible"
document.images["imgIzquierda"].style.visibility="hidden"
resize()
}

function muestraArbol() {

m=7
l=5 //Margen izquierdo
la = 15
tb=tb1 //Ancho de la barra de botones
wt=240
wto=250
if (proceso.estado==1) //Alto del t�tulo de cada apartado
	ha=52 //26
else
	ha=52
hSwitch = 20

document.getElementById("divArbol").style.visibility="visible"
document.getElementById("divMostrarArbol").style.visibility="hidden"
document.getElementById("divOcultarArbol").style.visibility="visible"
document.images["imgDerecha"].style.visibility="hidden"
document.images["imgIzquierda"].style.visibility="visible"
resize()
}

    </script>

    <div name="divApartado" id="divApartado">
    </div>

    <form name="frmOfertaAEnviar" id="frmOfertaAEnviar" action="guardarOferta.asp" target="fraOfertaServer" method="post">
        <input type="hidden" name="NumOBJ" id="NumOBJ" value="<%=request("NumOBJ")%>">
        <input type="hidden" name="NumOFE" id="NumOFE" value="<%=oOferta.num%>">
        <%if Application("FSAL")=1 then%>
        <input type="hidden" name="fechaIniFSAL7">
        <input type="hidden" name="sIdRegistroFSAL7">
        <%end if%>

        <div name="divItems" id="divItems">
        </div>


        <div name="divOfertaAEnviar" id="divOfertaAEnviar">
        </div>

        <div name="paPruebas" id="paPruebas"></div>

    </form>


    <div id="divCostesDescItem" name="divCostesDescItem" style="visibility: hidden; background-color: white; position: absolute; top: 100px; left: 220px; width: 460px; min-height: 225px; overflow-y: none; color: '#000000'; padding-right: 10px; z-index: 301">
    </div>




</body>


<script>

var nombreGrupo

// <summary>
// acceso a la pantalla de datos de grupo desde el men� lateral
// </summary>
// <param name="e">identificador de la pantalla</param>
// <returns></returns>
// <remarks>Llamada desde: config.asp/mostrarConfigProceso; Tiempo m�ximo:0,1</remarks>
// <revision>JVS 16/11/2011</revision>
function datosGrupo(e)
{
if (document.getElementById("divApartado").aptdoAnterior)
	eval (document.getElementById("divApartado").aptdoAnterior)

var vRama
vRama = eval("rama" + e)
dibujarApartado(vRama.text)
resize()

var vGrp

var i

for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != vRama.cod ;i++)
	{
	}

vGrp = proceso.grupos[i]
document.getElementById("divApartado").aptdoAnterior=null
document.getElementById("divApartado").aptdoActual="mostrarGrupoCod('" + vGrp.cod + "')"
// Pone a false los campos booleanos de control de la pantalla de retorno tras rec�lculo
cambiarFlag()
// Pone a true el campo booleano de datosGrupo de control de la pantalla de retorno tras rec�lculo
bEstamosEnGrupo = true
bEstamos="3_" + vGrp.cod
//AQUI CARGA EL GRUPO
  vGrp.cargarDatos("fraOfertaServer")
}


function downloadEspec(espec, grupo, item)
{

if (item ==null) 
	{
	if (grupo == null)
		{
		proceso.especs[espec].download("fraOfertaServer")
		}
	else
		{
		var i

		for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != grupo ;i++)
			{
			}

		proceso.grupos[i].especs[espec].download("fraOfertaServer")
		}
	}
else
	{
	for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != grupo ;i++)
		{
		}
	
	for (j=0;j<proceso.grupos[i].items.length && proceso.grupos[i].items[j].id !=item ;j++)
		{
		}
	
	vObj = proceso.grupos[i].items[j]
	vObj.especs[espec].download("fraOfertaServer")
	}
}


function cargarEspecificaciones(cod,id)
{
	var i


	for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != cod ;i++)
		{
		}
	
	for (j=0;j<proceso.grupos[i].items.length && proceso.grupos[i].items[j].id !=id ;j++)
		{
		}
	
	vObj = proceso.grupos[i].items[j]
	
	vObj.cargarEspecificaciones("fraOfertaServer")
	

}
var wEspecsItem
var str_wEspecsItem
/*''' <summary>
''' Muestra una especificacion de  item
''' </summary>
''' <param name="vObj">Especificacion</param>
''' <remarks>Llamada desde: solicitudesoferta\js\item.asp\funcion cargarEspecificacionesItem ; Tiempo máximo: 0</remarks>*/
function mostrarEspecificacionesItem(vObj)
{

    wEspecsItem = window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/especificacionesItem.asp?titulo=<%=den(12)%>","_blank","top=100,left=150,width=600,height=250,location=no,menubar=no,resizable=yes,scrollbars=yes,toolbar=no")

    str_wEspecsItem = ""
    str_wEspecsItem+= "				<table width=100%>"
    str_wEspecsItem+= "					<tr>"
    str_wEspecsItem+= "						<td>" + cajetin('<%=JSText(den(12))%>',mostrarEspecificaciones(vObj),"width:100%;") + "</td>"
    str_wEspecsItem+= "					</tr>"
    str_wEspecsItem+= "				</table>"
    str_wEspecsItem+= "<br>"
    str_wEspecsItem+= "<p align=center><input class=button type=button name=cmdCerrar id=cmdCerrar value='<%=den(27)%>' onclick='window.close()'></P>"
    
    setTimeout(function(){BucleMostrarEspecificacionesItem(wEspecsItem)},10);

}

/*''' <summary>
''' Muestra una especificacion de  item
''' </summary>
''' <param name="wEspecsItem">Pantalla especificaciones</param>
''' <remarks>Llamada desde: funcion mostrarEspecificacionesItem ; Tiempo máximo: 0</remarks>*/
function BucleMostrarEspecificacionesItem(wEspecsItem){
    if(wEspecsItem.document.getElementById("divEspecificacionesItem")==null) 
        setTimeout(function(){BucleMostrarEspecificacionesItem(wEspecsItem)},10);
    else 
        wEspecsItem.document.getElementById("divEspecificacionesItem").innerHTML =str_wEspecsItem;
};



function mostrarEspecificaciones(vObj)
{
var strEspec
strEspec = ""
strEspec = JS2HTML(vObj.esp)

if (vObj.especs.length>0)
	{
	strEspec+="<br>"
	strEspec+="	<table style='width:100%;'>"
	strEspec+="		<tr>"
	strEspec+="			<td class='cabecera'>"
	strEspec+="				<nobr><%=JSText(den(13))%></nobr>"
	strEspec+="			</td>"
	strEspec+="			<td class='cabecera'>"
	strEspec+="				<nobr><%=JSText(den(14))%></nobr>"
	strEspec+="			</td>"
	strEspec+="			<td class='cabecera'>"
	strEspec+="				<nobr><%=JSText(den(15))%></nobr>"
	strEspec+="			</td>"
	strEspec+="			<td class='cabecera'>"
	strEspec+="				&nbsp;"
	strEspec+="			</td>"
	strEspec+="		</tr>"
	var i
	var vClase
	vClase = "filaImpar"
	for (i=0;i<vObj.especs.length;i++)
		{
		strEspec+="		<tr>"
		strEspec+="			<td class='" + vClase + "'>"
		if(vObj.proceso)
			{
			strEspec+="				<a href='javascript:void(null)' onclick='downloadEspec(" + i + ",\"" + vObj.cod + "\")'><nobr>" + vObj.especs[i].nombre + "</nobr></a>"
			}
		else 
			if (vObj.grupo)
				{
				strEspec+="				<a href='javascript:void(null)' onclick='downloadEspec(" + i + ",\"" + vObj.grupo.cod + "\"," +  vObj.id + ")'><nobr>" + vObj.especs[i].nombre + "</nobr></a>"
				}
			else
				{
				strEspec+="				<a href='javascript:void(null)' onclick='downloadEspec(" + i + ")'><nobr>" + vObj.especs[i].nombre + "</nobr></a>"
				}
		strEspec+="			</td>"
		strEspec+="			<td class='" + vClase + " numero'>"
		dSize = num2str(vObj.especs[i].size / 1024,vdecimalfmt,vthousanfmt,0) 
		if (dSize == "0") 
			dSize = "1"
		strEspec+="				<nobr>" + dSize + "&nbsp;Kb.</nobr>"
		strEspec+="			</td>"
		strEspec+="			<td class='" + vClase + "'>"
		strEspec+="				" + JS2HTML(vObj.especs[i].comentario) + ""
		strEspec+="			</td>"
		strEspec+="			<td class='" + vClase + " centro'>"
		if(vObj.proceso)
			{
			strEspec+="				<a href='javascript:void(null)' onclick='downloadEspec(" + i + ",\"" + vObj.cod + "\")'><img border=0 SRC=" + imgAbrir.src +"></a>"
			}
		else 
			if (vObj.grupo)
				{
				strEspec+="				<a href='javascript:void(null)' onclick='downloadEspec(" + i + ",\"" + vObj.grupo.cod + "\"," +  vObj.id + ")'><img border=0 SRC=" + imgAbrir.src +"></a>"
				}
			else
				{
				strEspec+="				<a href='javascript:void(null)' onclick='downloadEspec(" + i + ")'><img border=0 SRC=" + imgAbrir.src +"></a>"
				}
		strEspec+="			</td>"
		strEspec+="		</tr>"
		if (vClase == "filaImpar")
			vClase = "filaPar"
		else
			vClase = "filaImpar"
		}
		
	strEspec+="	</table>"
	}

	//jpa Tarea 540
	if (vObj.Aespecs.length>0)
	{	

		
		if (vObj.proceso==undefined && vObj.grupo==undefined)  
		{ 
			strEspec+="<br>"
			strEspec+="	<table style='width:100%;'>"
			strEspec+= "	<tr>"
			strEspec+= "	<td>" + cajetin('<%=JSText(den(83))%>',mostrarAtributosEspecificacion(vObj,1),"width:100%;") + "</td>"
			strEspec+= "	</tr>"		
			strEspec+="	</table>"		
		}
		
		if (vObj.proceso!=undefined && vObj.grupo==undefined)  
		{ 
			strEspec+="<br>"
			strEspec+="	<table style='width:100%;'>"
			strEspec+= "	<tr>"
			strEspec+= "	<td>" + cajetin('<%=JSText(den(83))%>',mostrarAtributosEspecificacion(vObj,2),"width:100%;") + "</td>"
			strEspec+= "	</tr>"		
			strEspec+="	</table>"		
		}

		if (vObj.proceso==undefined && vObj.grupo!=undefined)  
		{ 
			strEspec+="<br>"
			strEspec+="	<table style='width:100%;'>"
			strEspec+= "	<tr>"
			strEspec+= "	<td>" + cajetin('<%=JSText(den(83))%>',mostrarAtributosEspecificacion(vObj,3),"width:100%;") + "</td>"
			strEspec+= "	</tr>"		
			strEspec+="	</table>"		
		}

	
	}
	//fin jpa Tarea 540	
return (strEspec)
}	

/*
''' <summary>
''' Muestra un atributo
''' </summary>
''' <param name="atrib">id de atributo</param>
''' <param name="ambito">ambito de atributo</param>        
''' <remarks>Llamada desde:   N inputs q se meten en esta pantalla y js\cargaritems.asp y solicitudesoferta\cumpoferta.js; Tiempo máximo: 0</remarks>*/
function linkAtrib(atrib,ambito)
{
var str
if (ambito==1)
 str="window.open('<%=application("RUTASEGURA")%>script/common/atributo.asp?anyo=" + atrib.parent.anyo + "&gmn1=" + atrib.parent.gmn1 + "&proce=" + atrib.parent.cod + "&ambito=1&atrib=" + atrib.id + "', '_blank','top=100,left=150,width=500,height=350,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no')"
else
 str="window.open('<%=application("RUTASEGURA")%>script/common/atributo.asp?anyo=" + atrib.parent.proceso.anyo + "&gmn1=" + atrib.parent.proceso.gmn1 + "&proce=" + atrib.parent.proceso.cod + "&grupo=" + atrib.parent.id + "&ambito=" + ambito + "&atrib=" + atrib.id + "',  '_blank','top=100,left=150,width=500,height=350,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no')"
return (str)
}

/*
''' <summary>
''' link a la descripcin del coste/descuento
''' </summary>
<param name=atrib>coste/descuento</param>
<param name=ambito>�mbito del coste/descuento</param>
<param name=grupo>grupo al que pertenece el coste/descuento</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Tiempo m�ximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
*/
function linkCosteDesc(atrib,ambito,grupo)
{
var str
if (ambito==1)
 str="window.open('<%=application("RUTASEGURA")%>script/common/atributo.asp?anyo=" + atrib.parent.anyo + "&gmn1=" + atrib.parent.gmn1 + "&proce=" + atrib.parent.cod + "&ambito=1&atrib=" + atrib.id + "', '_blank','top=100,left=150,width=500,height=350,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no')"
else
 str="window.open('<%=application("RUTASEGURA")%>script/common/atributo.asp?anyo=" + atrib.parent.anyo + "&gmn1=" + atrib.parent.gmn1 + "&proce=" + atrib.parent.cod + "&grupo=" + grupo + "&ambito=" + ambito + "&atrib=" + atrib.id + "',  '_blank','top=100,left=150,width=500,height=350,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no')"
return (str)
}

/*
''' <summary>
''' el popup para los atributos de especificacion es distinto
''' </summary>
''' <param name="atrib">id de atributo</param>
''' <param name="ambito">ambito de atributo</param>        
''' <remarks>Llamada desde:   N inputs q se meten en esta pantalla; Tiempo m�áximo: 0</remarks>*/
function linkAtribEspe(atrib,ambito)
{
var str

switch (ambito) { 
    case 1: 
       str="window.open('<%=application("RUTASEGURA")%>script/common/atributoespecificacion.asp?anyo=" + atrib.parent.anyo + "&gmn1=" + atrib.parent.gmn1 + "&proce=" + atrib.parent.cod + "&ambito=1&atrib=" + atrib.Atrib + "', '_blank','top=100,left=150,width=500,height=350,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no')"
       break 
    case 2: 
       str="window.open('<%=application("RUTASEGURA")%>script/common/atributoespecificacion.asp?anyo=" + atrib.parent.proceso.anyo + "&gmn1=" + atrib.parent.proceso.gmn1 + "&proce=" + atrib.parent.proceso.cod + "&grupo=" + atrib.parent.cod + "&ambito=" + ambito + "&atrib=" + atrib.Atrib + "',  '_blank','top=100,left=150,width=500,height=350,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no')"
       break        
    case 3: 
       str="window.open('<%=application("RUTASEGURA")%>script/common/atributoespecificacion.asp?anyo=" + atrib.parent.proceso.anyo + "&gmn1=" + atrib.parent.proceso.gmn1 + "&proce=" + atrib.parent.proceso.cod  + "&ambito=" + ambito + "&atrib=" + atrib.id + "&item=" + atrib.item + "', '_blank','top=100,left=150,width=500,height=350,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no')"       
       break 
} 

return (str)
}

/*
''' <summary>
''' el popup para los atributos de especificacion es distinto
''' </summary>
''' <param name="texto">especificacion</param>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.asp\funcion mostrarAtributosEspecificacion ; Tiempo mximo: 0</remarks>*/
function linkTextoLargo(texto)
{
var str
     str="window.open('<%=application("RUTASEGURA")%>script/common/atriespeTextoLargo.asp?texto=" + encodeURI(texto) + "', '_blank','top=100,left=150,width=500,height=350,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no')"

return (str)
}

var wCAtributoT1
var wCAtributoT2
var wCAtributoT3
var wCAtributoT4

var wCAtributo


wCAtributo=240

wCAtributoT1=240
wCAtributoT2=240
wCAtributoT3=240
wCAtributoT4=100

var wCCosteDescuentoT1
var wCCosteDescuentoT2
var wCCosteDescuentoT3
var wCCosteDescuentoT4
var wCCosteDescuentoT5

var wCCosteDescuento


wCCosteDescuento=240

wCCosteDescuentoT1=240
wCCosteDescuentoT2=120
wCCosteDescuentoT3=120
wCCosteDescuentoT4=100
wCCosteDescuentoT5=60


function mostrarAtributos(vObj,sFrm, vGrupo)
{
var strAtrib
var i
var j
strAtrib = ""
strAtrib+="	<table style='width:100%;'>"
strAtrib+="		<tr><td colspan=3><%=den(52)%></td></tr>"

var vClase



vClase = "filaImpar"
var iAmbito

iAmbito = vObj.proceso?2:1
for (i=0;i<vObj.atribs.length;i++)
	{
	strAtrib+="		<tr>"
	strAtrib+="			<td width=70% class='" + vClase + "'>"
	strAtrib+="				<input type=hidden name=pAtribCod id=pAtribCod value=" + vObj.atribs[i].cod + ">"
    strAtrib+="					<table><tr><td><nobr><a class='popUp" + " " + (vObj.atribs[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkAtrib(vObj.atribs[i],iAmbito) + "\">" + (vObj.atribs[i].obligatorio?"(*)":"") + vObj.atribs[i].den + "</a></td><td><a class='popUp" + " " + (vObj.atribs[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkAtrib(vObj.atribs[i],iAmbito) + "\"><IMG border = 0 SRC='../images/masinform.gif'></a></td></tr></table></nobr>"
    
	strAtrib+="			</td>"
	strAtrib+="			<td class='" + vClase + "' style='width=" + wCAtributo + "'>"
	strAtrib += mostrarAtributo (vObj.atribs[i],null,sFrm,0,vGrupo)
	strAtrib+="			</td>"
	strAtrib+="		</tr>"
	if (vClase == "filaImpar")
		vClase = "filaPar"
	else
		vClase = "filaImpar"
	}
strAtrib+="	</table>"

return (strAtrib)
}

/*''' <summary>
''' Muestra los atributos de especificacion
''' </summary>
''' <param name="vObj">Objeto de item</param>
''' <param name="iIem">Ambito de los atributos</param>
''' <remarks>Llamada desde:  Tiempo m�ximo:0,1</remarks>
*/
function mostrarAtributosEspecificacion(vObj,iIem)
{
var strAtrib
var i
var j
strAtrib = ""
strAtrib+="	<table style='width:100%;'>"

var vClase
var iAmbito
var dia, mes,  anio, cadenaFecha
var vEsTexto = 0
var vEsFecha = 0
var vEsGrande = 0
var miString = ""
var valor
var valorCadena  = ""
var Minumero


iAmbito= iIem

for (i=0;i<vObj.Aespecs.length;i++)
	{
	vEsTexto = 0
	vEsFecha = 0
	vEsGrande = 0	
	strAtrib+="		<tr>"
	strAtrib+="			<td width=70% class='filaImpar'>"
	strAtrib+="				<input type=hidden name=pAtribCod id=pAtribCod value=" + vObj.Aespecs[i].Atrib + ">"
	strAtrib+="					<table><tr><td><nobr><a class='popUp" + " "  +   "' href='javascript:void(null)' onclick=\"" + linkAtribEspe(vObj.Aespecs[i],iAmbito) + "\">" + vObj.Aespecs[i].Desc + "</a></td><td><a class='popUp" + " " +   "' href='javascript:void(null)' onclick=\"" + linkAtribEspe(vObj.Aespecs[i],iAmbito) + "\"><IMG border = 0 SRC='../images/masinform.gif'></a></td></tr></table></nobr>"
    
	strAtrib+="			</td>"
	
						vClase = "filaPar"
				
						//Si Valor_num no es null es num y dejo de mirar
						//SiNo Si Valor_text no es null es text y dejo de mirar
						//SiNo Si Valor_fec no es null es fec y dejo de mirar
						//SiNo es Valor_bool				
						
						if (vObj.Aespecs[i].Valor_num != null)
							{valor = vObj.Aespecs[i].Valor_num
							 vEsTexto =0
							 }
						else
						    {			
								if (vObj.Aespecs[i].Valor_text != null)
									{
										valor = vObj.Aespecs[i].Valor_text									 
										if (typeof(valor) == 'string')		
										{									 
										    vEsTexto =1
										    //miro si es fecha -----------------------------
											if (vObj.Aespecs[i].Valor_fec != null)
												{valor = vObj.Aespecs[i].Valor_fec
												 vEsTexto =0
												 vEsFecha =1
												 dia=valor.getDate() 
												 mes=valor.getMonth()
												 mes++
												 anio= valor.getFullYear()
												 cadenaFecha = dia
												 cadenaFecha += "/" + mes
												 cadenaFecha += "/" + anio
												 }
										    //fin miro si es fecha -------------------------
										    //miro si es boolean -----------------------------								    
											if (valor.length == 0)
												{
												 vEsBooelan =1
												 if (vObj.Aespecs[i].Valor_bool == null)
												 	{valor = ''}												 
												 if (vObj.Aespecs[i].Valor_bool ==0)
												 	{valor = '<%=den(35)%>'}
												 if (vObj.Aespecs[i].Valor_bool ==1)
												    {valor = '<%=den(34)%>'}												 
												 vEsTexto =0
												 }
										    //fin miro si es boolean -------------------------    
										    if (vEsTexto ==1)
										    {
												if (valor.length > 20)
												   {vEsGrande = 1}
												else
												   {
													vEsGrande = 0
													//Se hace el repace para el caso especial de que haya ' en la cadena													
													valor = valor.replace("&#92;&#39;","'")
												   }
										    }
										}
									 }
							}
							
	strAtrib+="			<td align=right class='" + vClase + "'>"
	
	if (vEsTexto ==1)
		{
			if (vEsGrande == 1)		
				{
				 strAtrib+="" + "..." + ""
				 strAtrib+= "&nbsp;&nbsp;" + "<input type=button  id=button1 name=button1 class=button value='...' onclick=\"" + linkTextoLargo(valor) + "\">"
				}
			else
				{strAtrib+="" + valor + ""
				}
		}
	else
		{
		 if (vEsFecha==1)
			{strAtrib+="" + cadenaFecha + ""}
		 else
			{strAtrib+="" + valor + ""}
		}
		
	strAtrib+="			</td>"			
	strAtrib+="		</tr>"
	}
	
strAtrib+="	</table>"

return (strAtrib)
}
//JORGE  FIJARME

function actualizarEstado(tipo,elemento,vdecimalfmt,vthousanfmt,vprec,minimo,maximo,errMsg)
{
ha=52

if (tipo==2)
{
var ret
ret = validarNumero(elemento,vdecimalfmt,vthousanfmt,vprec,minimo,maximo,errMsg,"<%=JSText(den(56))%>" + (minimo!=null?"\n<%=JSText(den(37))%> " + num2str(minimo,vdecimalfmt,vthousanfmt,vprecisionfmt):"") + (maximo!=null?"\n<%=JSText(den(38))%> " + num2str(maximo,vdecimalfmt,vthousanfmt,vprecisionfmt):""))
if (!ret)
	{
	return false
	}
}	
proceso.estado=4
noScroll = true
document.getElementById("divCabApartado").innerHTML = generaCabecera(tituloActual)

return true
}

/*
''' <summary>
''' validaci�n cuando se actualiza el valor de un campo de coste/descuento
''' </summary>
<param name=tipo>tipo del coste/descuento (debe ser 2 = num�rico)</param>
<param name=iAmbito>�mbito del coste/descuento</param>
<param name=elemento>campo de coste/descuento</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Tiempo m�ximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
*/

function actualizarEstadoCosteDesc(tipo,iAmbito,elemento,vdecimalfmt,vthousanfmt,vprec,minimo,maximo,errMsg)
{
var sNameOrdenAplic
var form

ha=52
if (tipo==2)
{
var ret
ret = validarNumero(elemento,vdecimalfmt,vthousanfmt,vprec,minimo,maximo,errMsg,"<%=JSText(den(56))%>" + (minimo!=null?"\n<%=JSText(den(37))%> " + num2str(minimo,vdecimalfmt,vthousanfmt,vprecisionfmt):"") + (maximo!=null?"\n<%=JSText(den(38))%> " + num2str(maximo,vdecimalfmt,vthousanfmt,vprecisionfmt):""))
if (!ret)
	{
	return false
	}
}

if(!nombreGrupo)
    guardaValoresCostesDescuentos(elemento.name,iAmbito)
else
    guardaValoresCostesDescuentos(elemento.name,iAmbito,nombreGrupo)


proceso.estado=4

if(iAmbito==3)
{
	//if(linkRecalculoItem == 0 && proceso.importe)
	// DPD quito la condici�n de proceso.importe
	// El proceso puede no tener importe y hay que recalcular costesdescuentos
    if(linkRecalculoItem == 0 )
    {
        linkRecalculoItem = 1
         if(linkRecalculoPrevio == 2)
            linkRecalculoPrevio = linkRecalculo

        paidCD = (elemento.name.substr(elemento.name.indexOf("_") + 1)).substr(0, elemento.name.substr(elemento.name.indexOf("_") + 1).indexOf("_"))
        valorAnterior = elemento.defaultValue

        if(elemento.getAttribute("pu")==4)
            linkRecalcularItem("txtPujaPU_", null, paidCD, valorAnterior, null, elemento.divModificable)
        else if(precioNum=="")
            linkRecalcularItem("txtPU_", null, paidCD, valorAnterior, null, elemento.divModificable)
        else if(precioNum==2)
            linkRecalcularItem("txtPU2_", null, paidCD, valorAnterior, null, elemento.divModificable)
        else if(precioNum==3)
            linkRecalcularItem("txtPU3_", null, paidCD, valorAnterior, null, elemento.divModificable)
    }
}
else
{
    //if(linkRecalculo == 0 && proceso.importe)
	// DPD quito la condici�n de proceso.importe
	// El proceso puede no tener importe y hay que recalcular costesdescuentos
    if(linkRecalculo == 0 )
    {
        linkRecalculo = 1
        linkRecalculoPendiente = 1
        document.getElementById("divCabApartado").innerHTML = generaCabecera(tituloActual)
    }
    else if(linkRecalculo == 0)
    {
        linkRecalculoPendiente = 0
    }

}



for (i=0;i<proceso.grupos.length;i++)
{    
	sEstado = estadoOferta(proceso.estado)
	strT = sEstado + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  (linkRecalculo==0?num2str(proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt):"-") + "&nbsp;" + proceso.mon.cod + "&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(null)' onclick='verAyuda(" + proceso.estado + ")'><IMG border=0 SRC=../images/masinform.gif  WIDTH=11 HEIGHT=14 name=imgMasInform id=imgMasInform></a>"
    
    if(linkRecalculo==1)
        strT+="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(null)' onclick=\"linkRecalculoOferta()\"><img src=\"images/RecOferTot1-25x25.png\"  border=0 width=19 height=19 / ><%=JSText(den(128))%></a>" //Recalcular totales de oferta
	sClase = ""
    
	switch (proceso.estado)
		{
		case 1:
			sClase = "OFENUEVA"
			break;
		case 2:
			sClase = "OFESINENVIAR"
			break;
		case 3:
		case 5:
			sClase = "OFEENVIADA"
			break;
		case 4:
			sClase = "OFESINGUARDAR"
			break
		default:
			sClase = ""
			break;
		}
        tb= tbvar //DPD
        resize()


	if (document.getElementById("divTotalOferta_" + proceso.grupos[i].cod) && sClase!="")
		{				      
		document.getElementById("divTotalOferta_" + proceso.grupos[i].cod).innerHTML  = strT
		document.getElementById("divTotalOferta_" + proceso.grupos[i].cod).parentNode.className=sClase
		}
}
if (esSubasta)
{			
	cadena = str
	//Se pasa con timeOut ya que sino al cambiar la cantidad y pulsar directamente en el boton de pujar
	//no coge el evento onclick!!!!!!!
    
    setTimeout ("MostrarCabeceraOferta()",700)

    generaCabecerasGruposSubasta()
}

}


/*
''' <summary>
''' guardado del valor de un campo de coste/descuento
''' </summary>
<param name=sNameCosteDescuento>nombre del campo de coste/descuento</param>
<param name=iAmbito>�mbito del coste/descuento</param>
<param name=grupo>grupo al que pertenece el coste/descuento (si aplica)</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Tiempo m�ximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
*/
function guardaValoresCostesDescuentos(sNameCosteDescuento,iAmbito,grupo)
{
var vObj=proceso
var indiceIt
if(iAmbito==1)
    form = "frmCostesDescuentos"
else if(iAmbito==2)
    form = "frmCostesDescuentosGrupo"
else //if(iAmbito==3)
    form = "frmCostesDescuentosItem"

var sNameOrdenAplic = "hOrdenAplica" + sNameCosteDescuento


eval("var numOrden = document.getElementById(\"" + form + "\")." + sNameOrdenAplic + ".value")
eval("var valor = document.getElementById(\"" + form + "\")." + sNameCosteDescuento + ".value")
eval("var importeParcial = document.getElementById(\"" + form + "\").hImpo" + sNameCosteDescuento + ".value")
eval("var pu = document.getElementById(\"" + form + "\")." + sNameCosteDescuento + ".pu")


//poner el valor modificado en rojo
//if(proceso.importe && form != "frmCostesDescuentosItem")	
// DPD quito la condici�n de proceso.importe
// El proceso puede no tener importe y hay que recalcular costesdescuentos
if(form != "frmCostesDescuentosItem")
    eval("document.getElementById(\"" + form + "\")." + sNameCosteDescuento + ".style.color =  '#ff0000'")


if (valor == "") 
    valor=null
else
    valor = str2num(valor,vdecimalfmt,vthousanfmt,vprecisionfmt)

eval("var idCosteDescuento = document.getElementById(\"" + form + "\").hId" + sNameCosteDescuento + ".value")
eval("var aplica = document.getElementById(\"" + form + "\").hAplica" + sNameCosteDescuento + ".value")


if(iAmbito==1)  //tratamiento para los valores de �mbito oferta
{
    if (proceso.costeDescRecalculo)
        var numOrdenGuardado = proceso.costeDescRecalculo.numOrden
    else
        var numOrdenGuardado = 999

    if(numOrden<numOrdenGuardado)
    {
        var costeDescModif = new CosteDescRecalculo(numOrden, sNameCosteDescuento, valor, idCosteDescuento, importeParcial, aplica, iAmbito, grupo)
        proceso.costeDescRecalculo = costeDescModif
    }   
}

if(iAmbito==2)  //tratamiento para los valores de �mbito grupo
{
    for(i=0;i<proceso.grupos.length;i++)
    {
        if(proceso.grupos[i].cod == grupo)
        {
            if (proceso.grupos[i].costeDescRecalculo)
                var numOrdenGuardado = proceso.grupos[i].costeDescRecalculo.numOrden
            else
                var numOrdenGuardado = 999

            if(numOrden<numOrdenGuardado)
            {
                var costeDescModif = new CosteDescRecalculo(numOrden, sNameCosteDescuento, valor, idCosteDescuento, importeParcial, aplica, iAmbito, grupo)
                proceso.grupos[i].costeDescRecalculo = costeDescModif
            }
        }
    }
}

//aplica=0 --> aplica a Total Oferta
//aplica=1 --> aplica a Total Grupo
//aplica=2 --> aplica a Total Item
//aplica=3 --> aplica a Descuento Item
var vGru
var vGrupo
var iAmbitoOferta = 1
var iAmbitoGrupo = 2
var iAmbitoItem = 3


if(grupo)
{
 for (i=0;i<vObj.grupos.length;i++)
    {
        if (vObj.grupos[i].cod==grupo)
        {
            vGrupo = vObj.grupos[i]
            break
        }
    }
}

if(iAmbito==3)
{
        
var idItem = document.getElementById("frmCostesDescuentosItem").item.value
var iesc = document.getElementById("frmCostesDescuentosItem").esc.value

if(pu==4)
{
    for(indiceIt=0;indiceIt<itemsPuja.length;indiceIt++)
        if(itemsPuja[indiceIt].id == idItem)
        {
            var oItemPuja = itemsPuja[indiceIt]
            break
        }
    if(vGrupo.items.length)
    {
        for(indiceIt=0;indiceIt<vGrupo.items.length;indiceIt++)
            if(vGrupo.items[indiceIt].id == oItemPuja.id)
            {
                var oItem = vGrupo.items[indiceIt]
                break
            }
    }
}
else
{
    for(indiceIt=0;indiceIt<vGrupo.items.length;indiceIt++)
        if(vGrupo.items[indiceIt].id == idItem)
        {
            var oItem = vGrupo.items[indiceIt]
            break
        }
    if (esSubasta)
	{
        for(indiceIt=0;indiceIt<itemsPuja.length;indiceIt++)
            if(itemsPuja[indiceIt].id == oItem.id)
            {
                var oItemPuja = itemsPuja[indiceIt]
                break
            }
    }

}

}

if(iesc>=0) idEsc = oItem.escalados[iesc].id
       
if (aplica==3)
{

    for (i=0;i<proceso.CostesDescPrecItem.length;i++)
    {
        if (proceso.CostesDescPrecItem[i].paid==idCosteDescuento)
        {
            if (proceso.CostesDescPrecItem[i].ambito == iAmbitoOferta)
            {
                 costeDescValor = obtenerCosteDescValor(proceso.costesDescuentos,proceso.CostesDescPrecItem[i])
                 costeDescValor.valor = valor
            }
            else if (proceso.CostesDescPrecItem[i].ambito == iAmbitoGrupo)
            {
                 costeDescValor = obtenerCosteDescValor(vGrupo.costesDescuentos,proceso.CostesDescPrecItem[i])
                 costeDescValor.valor = valor
            }
            else  //if (vGrupo.costesDescuentosItems[i].ambito == iAmbitoItem)
            {
                if (oItem)
                {
                    costeDescValor = obtenerCosteDescValor(oItem.costesDescuentosOfertados,proceso.CostesDescPrecItem[i])
					costeDescValor.valor = valor
					if(iesc>=0) 						
					{
						if (costeDescValor.escalados.length>0) 
							costeDescValor.escalados[iesc].valorNum = valor						
						
					}
					else
					{
					costeDescValor.valor = valor
					}
                }
                if (oItemPuja)
                {
                    costeDescValor = obtenerCosteDescValor(oItemPuja.costesDescuentosOfertados,proceso.CostesDescPrecItem[i])
                    costeDescValor.valor = valor
                }
            }

            break
        }
    }

}


if (aplica==2)
{

    for (i=0;i<proceso.CostesDescTotalItem.length;i++)
    {
        if (proceso.CostesDescTotalItem[i].paid==idCosteDescuento)
        {
            if (proceso.CostesDescTotalItem[i].ambito == iAmbitoOferta)
            {
                 costeDescValor = obtenerCosteDescValor(proceso.costesDescuentos,proceso.CostesDescTotalItem[i])
                 costeDescValor.valor = valor
            }
            else if (proceso.CostesDescTotalItem[i].ambito == iAmbitoGrupo)
            {
                 costeDescValor = obtenerCosteDescValor(vGrupo.costesDescuentos,proceso.CostesDescTotalItem[i])
                 costeDescValor.valor = valor
            }
            else  //if (vGrupo.costesDescuentosItems[i].ambito == iAmbitoItem)
            {
                if (oItem)
                {
                    costeDescValor = obtenerCosteDescValor(oItem.costesDescuentosOfertados,proceso.CostesDescTotalItem[i])
                    
					costeDescValor.valor = valor
					if(iesc>=0) 						
					{
						if (costeDescValor.escalados) 
							costeDescValor.escalados[iesc].valorNum = valor													
						
					}
					else
					{
					costeDescValor.valor = valor
					}
					
                }
                if (oItemPuja)
                {
                    costeDescValor = obtenerCosteDescValor(oItemPuja.costesDescuentosOfertados,proceso.CostesDescTotalItem[i])
                    costeDescValor.valor = valor
                }
            }
            break
        }
    }

}



if (aplica==1) 
{
    for (i=0;i<vObj.CostesDescTotalGrupo.length;i++)
    {
        if (vObj.CostesDescTotalGrupo[i].paid==idCosteDescuento)
        {        
            if(vObj.CostesDescTotalGrupo[i].ambito==1)
                costeDescValor = obtenerCosteDescValor(vObj.costesDescuentos,vObj.CostesDescTotalGrupo[i])
            else
                costeDescValor = obtenerCosteDescValor(vGrupo.costesDescuentos,vObj.CostesDescTotalGrupo[i])
            costeDescValor.valor=valor
            break
        }
    }

} //fin aplica==1

if(aplica==0)
{
    
    for (i=0;i<vObj.CostesDescTotalOferta.length;i++)
    {
        if (vObj.CostesDescTotalOferta[i].paid==idCosteDescuento)
        {
            if(vObj.CostesDescTotalOferta[i].ambito==1)
                costeDescValor = obtenerCosteDescValor(vObj.costesDescuentos,vObj.CostesDescTotalOferta[i])
            else
                costeDescValor = obtenerCosteDescValor(vGrupo.costesDescuentos,vObj.CostesDescTotalOferta[i])
            costeDescValor.valor=valor
            break
        }
    }

} //Fin aplica==0
}




function validarNumeroB(mirror,vmirror, elemento,vdecimalfmt,vthousanfmt,vprec,minimo,maximo,errMsg)
/*'************************************************************
'*** rue 01/04/2009
'*** Descripci�n: Funcion que recalcula los valores de la oferta al cambiar los campos que influyen en los sumatorios de la oferta         *****
'*** Par�metros de entrada: *****
'*** mirror			nombre input sobre el que se realiza la accion
'*** vmirror		input sobre el que se realiza la accion
'*** elemento		item sobre el que se hace la modificacion
'*** vdecimalfmt	formato decimal del usuario
'*** vthousanfmt	formato miles del usuario
'*** vprec			numero de decimales que se muestran
'*** minimo			valor minimo aldmitido para ese campo
'*** maximo			valor maximo aldmitido para ese campo
'*** errMsg			error que se ha producido
'*** Par�metros de entrada: atributoOfertado: el atributo          *****
'*** Par�metros de entrada: sFrm: Formulario, lo utilizan los input de fechas          *****
'*** Par�metros de salida:     *****                    
'***                                               *****                    
'*** Llamada desde: el onchange de los inputs de tipo numero  						*****                    
'** Tiempo m�ximo:   						*****                    
'************************************************************
*/
{
var ret
var i
var oItem
ret = validarNumero(elemento,vdecimalfmt,vthousanfmt,vprec,minimo,maximo,errMsg,"<%=JSText(den(56))%>" + (minimo!=null?"\n<%=JSText(den(37))%> " + num2str(minimo,vdecimalfmt,vthousanfmt,vprecisionfmt):"") + (maximo!=null?"\n<%=JSText(den(38))%> " + num2str(maximo,vdecimalfmt,vthousanfmt,vprecisionfmt):""))
var sGrpCod
nombre = Mid(elemento.name, 0, "txtPU_".length + 1)

if(linkRecalculoPrevio == 2)
    linkRecalculoPrevio = linkRecalculo

if(linkRecalculoPendiente == 1)
    linkRecalculo = 1
if (!ret)
	{
	return false
	}
else
	{
	proceso.estado=4
	if (mirror!="null" && mirror!=null && mirror!="" && document.forms["frmOfertaAEnviar"].elements[mirror])
		document.forms["frmOfertaAEnviar"].elements[mirror].value=elemento.value
	if (vmirror!="undefined" && vmirror!="null" && vmirror!=null)
		{
		//eval("oItem = " + vmirror)
		oItem =  vmirror
		if (oItem.mirror)
			if (oItem.mirror.length>0)
				{
				for (i=0;i<oItem.mirror.length;i++)
					eval(oItem.mirror[i] + ".precio =" + str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt))
				}
		}
	}	

sEscId=""	
if (elemento.getAttribute("cant"))	
	{	
	switch(elemento.getAttribute("pu"))
		{
		case "1":
		case "2":
		case "3":
			codigos = elemento.name.split("_")
			sGrpCod = codigos[1]
			sItemId = codigos[2]
			if (codigos.length>=4) sEscId="_" + codigos[3]
			break;
		case "4":
			codigos = elemento.name.split("_")
			sGrpCod = ""
			sItemId = codigos[1]
			if (codigos.length>=3) sEscId="_" + codigos[2]
			break;
		}
	if (sGrpCod!="")		
		{
		for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != sGrpCod;i++)
			{
			}
		oObj = proceso.grupos[i]
					
		for (i=0;i<oObj.items.length && oObj.items[i].id != sItemId;i++)
			{
			}
		oObj = oObj.items[i]
		}
	else
		{
		
		for (i=0;i<itemsPuja.length && itemsPuja[i].id != sItemId;i++)
			{
			}
		oObj = itemsPuja[i]
		}

	switch(elemento.getAttribute("pu"))
		{
		case "1":
		case "4":
				oObj.precio = str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt)			
			break
		case "2":
			oObj.precio2 = str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt)
			break
		case "3":
			oObj.precio3 = str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt)
			break
		}
	
	strDetOferta = ""
	if (solicitarCantMax)
	{
		sNamePU = elemento.id	
		

		sTmp1 = sNamePU.replace("txtPU_","txtCantMax_")	
		sTmp2 = sNamePU.replace("txtPU2_","txtCantMax_")	
		sTmp3 = sNamePU.replace("txtPU3_","txtCantMax_")	
        sTmp4 = sNamePU.replace("txtPUI_","txtCantMax_")
			
		if (sTmp1 != sNamePU )
		{		
			elemento2 = document.forms["frmOfertaAEnviar"].elements[sTmp1]
		}
		else
		{
			if (sTmp2 != sNamePU )
				elemento2 = document.forms["frmOfertaAEnviar"].elements[sTmp2]
			else
                {
                    if (sTmp3 != sNamePU )
			            elemento2 = document.forms["frmOfertaAEnviar"].elements[sTmp3]
                    else
                        elemento2 = document.forms["frmOfertaAEnviar"].elements[sTmp4]
		        }						
		}	
		if ((elemento2.value != "" ) && (str2num(elemento2.value,vdecimalfmt,vthousanfmt,vprecisionfmt) <= elemento.getAttribute("cant")) && (elemento2.value != null) && (str2num(elemento2.value,vdecimalfmt,vthousanfmt,vprecisionfmt) > 0) )				
			strDetOfertaAux = "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * str2num(elemento2.value,vdecimalfmt,vthousanfmt,vprecisionfmt),vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"						
		else
			strDetOfertaAux = "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * elemento.getAttribute("cant"),vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"		

	    // dpd incidencia 18612
	}
	else
	{
		if (elemento.value != "")
		{
			strDetOfertaAux = "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * elemento.getAttribute("cant"),vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"	
		}
	}
    var precioUsar = oItem.usar // precio al que se pueden modificar sus c/d
    if(elemento.getAttribute("pu")==1 && (precioUsar==1 || precioUsar==null))
        divModificable = true
    else if(elemento.getAttribute("pu")==2 && precioUsar==2)
        divModificable = true
    else if(elemento.getAttribute("pu")==3 && precioUsar==3)
        divModificable = true
    else if(elemento.getAttribute("pu")==4)
        divModificable = true
    else
        divModificable = false

	
	
    if (elemento.value == "") strDetOfertaAux = ""
	if (divModificable && oItem.importeNeto!=null)
        strDetOfertaAux = "<b>" + num2str(oItem.importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
     
     	
	var vGr	
	if (sEscId!="")	
	{
	
		for (i=0;proceso.grupos[i].cod!=elemento.getAttribute("grupo");i++){}
		vGr = proceso.grupos[i]
		var vItem
		for (i=0;vGr.items[i].id!=elemento.getAttribute("itemid");i++){}
		vItem = vGr.items[i]	
	
		for (iesc=0;"_" + vItem.escalados[iesc].id!=sEscId;iesc++){}
		if (vItem.escalados[iesc].cant==null) strDetOfertaAux = ""
	}
	else
	{
		iesc = -1
	}
	
	strDetOferta += strDetOfertaAux
	if(document.getElementById("divTotalLinea" + elemento.getAttribute("pu") + "_" + elemento.getAttribute("itemid") + sEscId)) document.getElementById("divTotalLinea" + elemento.getAttribute("pu") + "_" + elemento.getAttribute("itemid") + sEscId).innerHTML = strDetOferta
	
	if (elemento.getAttribute("pu")=="1" || elemento.getAttribute("pu")=="2" || elemento.getAttribute("pu")=="3" || elemento.getAttribute("pu")=="4" )
		{
		
		if (elemento.getAttribute("pu")=="4")
			if (document.getElementById("divTotalLinea1_" + elemento.itemid + sEscId))  
				document.getElementById("divTotalLinea1_" + elemento.itemid + sEscId).innerHTML = strDetOferta
            else if (document.getElementById("divTotalLinea2_" + elemento.itemid + sEscId))
				document.getElementById("divTotalLinea2_" + elemento.itemid + sEscId).innerHTML = strDetOferta
            else if (document.getElementById("divTotalLinea3_" + elemento.itemid + sEscId))
				document.getElementById("divTotalLinea3_" + elemento.itemid + sEscId).innerHTML = strDetOferta
		if (elemento.getAttribute("pu")=="1")
			if (document.getElementById("divTotalLinea4_" + elemento.itemid + sEscId))
				document.getElementById("divTotalLinea4_" + elemento.itemid + sEscId).innerHTML = strDetOferta

	
		for (i=0;i<proceso.grupos.length;i++)
			{
			if (!document.getElementById("divTotalOferta_" + proceso.grupos[i].cod) && document.getElementById("tbl_" + proceso.grupos[i].cod))
				{
				ha=52
				resize()
				}
			sEstado = estadoOferta(proceso.estado)
            strT = sEstado + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  num2str(proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;" + proceso.mon.cod + "&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(null)' onclick='verAyuda(" + proceso.estado + ")'><IMG border=0 SRC=../images/masinform.gif  WIDTH=11 HEIGHT=14 name=imgMasInform id=imgMasInform></a>"
            
			sClase = ""
            
			switch (proceso.estado)
				{
				case 1:
					sClase = "OFENUEVA"
					break;
				case 2:
					sClase = "OFESINENVIAR"
					break;
				case 3:
				case 5:
					sClase = "OFEENVIADA"
					break;
				case 4:
					sClase = "OFESINGUARDAR"
					break
				default:
					sClase = ""
					break;
				}
            tb= tbvar //DPD
            resize()
                
			if (document.getElementById("divTotalOferta_" + proceso.grupos[i].cod) && sClase!="")
				{				
				document.getElementById("divTotalOferta_" + proceso.grupos[i].cod).innerHTML  = strT
				document.getElementById("divTotalOferta_" + proceso.grupos[i].cod).parentNode.className=sClase
				}
			}

		<%if oProceso.subasta then%>
			
				cadena = strT
				//Se pasa con timeOut ya que sino al cambiar la cantidad y pulsar directamente en el boton de pujar
				//no coge el evento onclick!!!!!!!
                
                setTimeout ("MostrarCabeceraOferta()",700)
                generaCabecerasGruposSubasta()
		

		for (i=0;i<proceso.grupos.length;i++)
			{
			if (document.getElementById("divCabeceraPreciosSubasta_" + proceso.grupos[i].cod))
				{
              strT ="<table width='100%'  height='"+ (hCabeceraGrupo+5) +"' border=0><tr>"
              strT += "<td class='cabecera' width='25%'>"
              strT += "<table border='0' cellspacing='0' cellpadding='0'><tr><td>"
              strT += "<a class=ayuda href='javascript:void(null)' onclick='ocultarPujaGrupo(\"" + proceso.grupos[i].cod + "\")' title = '" + proceso.grupos[i].den + "'>"
              strT += "<IMG style='position:absolute;top:5px;left:5px;visibility:visible;' name=imgGrupoA" + proceso.grupos[i].cod + " id=imgGrupoA" + proceso.grupos[i].cod + " border=0 SRC=images/arribab.gif>"
              strT += "<IMG style='position:absolute;top:5px;left:5px;visibility:hidden;' name=imgGrupoB" + proceso.grupos[i].cod + " id=imgGrupoB" + proceso.grupos[i].cod + " border=0 SRC=images/abajob.gif>"
              strT += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>"
              strT += "</td><td class='cabecera'>"
              strT += "<div style='overflow:hidden;height=12' title='" + "<%=den(57) %> " + proceso.grupos[i].cod  + " - " + proceso.grupos[i].den  + "'>"                           
              strT += "<%=den(57) %> " + proceso.grupos[i].cod  + " - " + proceso.grupos[i].den + "</div>"
              strT += "</td></tr></table>"
              strT += "</td>"              
                if (proceso.grupos[i].importe ==null)
                {
				    strT +="<td class='cabecera' width='25%'> &nbsp;" 
                }
                else
                {
				    strT +="<td class='cabecera' width='25%'> <%=den(86) %>:&nbsp;" + num2str(proceso.grupos[i].importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;" + proceso.mon.cod
                }

				if (proceso.grupos[i].cod != elemento.grupo) {
					//posicion Puja GRUPO VALIDAR
					if (proceso.grupos[i].importe > 0) {
						
						strT += "</td><td width='20%'>"
						if (document.getElementById("divPosicionPujaGrupo_" + proceso.grupos[i].cod)) {
							strT2 = document.getElementById("divPosicionPujaGrupo_" + proceso.grupos[i].cod).innerHTML
							strT += "<div name=divPosicionPujaGrupo_"+proceso.grupos[i].cod + " id=divPosicionPujaGrupo_"+proceso.grupos[i].cod + ">"
							strT += strT2
							strT+= "</div>"	
						}

			
					}else 
						strT += "</td><td width='20%'>"	
				}else {
					strT += "</td><td width='20%'>"
				}
                strT += "</td>"
                if (document.getElementById("divGanadorPujaGrupo_" + proceso.grupos[i].cod)) {
                    strT += "<td width=30% align=center class='cabecera' nowrap>"
                    strT += "<div name=divGanadorPujaGrupo_"+proceso.grupos[i].cod + " id=divGanadorPujaGrupo_"+proceso.grupos[i].cod + ">"
                    strT += document.getElementById("divGanadorPujaGrupo_" + proceso.grupos[i].cod).innerHTML
                    strT += "</div>"
                    strT += "</td>"
                }
				strT += "</tr></table>"
				document.getElementById("divCabeceraPreciosSubasta_" + proceso.grupos[i].cod).innerHTML  = strT
				}
			}

		<%end if%>

		elemento.oldValue = str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt)

		}
        if(hayCostesDescuentosItems && linkRecalculoItem == 0)
        {
            linkRecalculoItem = 1
            if(linkRecalculoPrevio == 2)
                linkRecalculoPrevio = linkRecalculo
                if(elemento.getAttribute("pu")==4)
                    linkRecalcularItem("txtPujaPU_", null, null, "new")
                else
                    linkRecalcularItem("txtPU_", null, null, "new")
        }

	}

	
//Totales
 //recalcularTotales()
 	if (sGrpCod!="")		
		{
		for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != sGrpCod;i++)
			{
			}
		objGrupo = proceso.grupos[i]
        vGr = objGrupo
					
		for (i=0;i<objGrupo.items.length && objGrupo.items[i].id != sItemId;i++)
			{
			}
		objItem = objGrupo.items[i]
		}
	else
		{
		
		for (i=0;i<itemsPuja.length && itemsPuja[i].id != sItemId;i++)
			{
			}
		objItem = itemsPuja[i]
		for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != objItem.grupo;i++)
			{
			}
		objGrupo = proceso.grupos[i]
        vGr = objGrupo

		}
 var oItemSecundario = null

         if(elemento.getAttribute("pu")==4)
        {
            if(vGr.items.length)
            {
                for(indiceIt=0;indiceIt<vGr.items.length;indiceIt++)
                if(vGr.items[indiceIt].id == objItem.id)
                {
                    oItemSecundario = vGr.items[indiceIt]
                    break
                }
            }
        }
        else
        {
            <%if oProceso.subasta then%>
                for(indiceIt=0;indiceIt<itemsPuja.length;indiceIt++)
                    if(itemsPuja[indiceIt].id == objItem.id)
                    {
                        oItemSecundario = itemsPuja[indiceIt]
                        break
                    }
            <%end if %>

        }

        
        var bRecalcularCostesDescItem = false
        if(elemento.getAttribute("pu")==4)
        {
            if (Mid(elemento.name, 0, "txtPujaPU_".length + 1) == "txtPujaPU_")
            {
                var elementoPU = "txtPujaPUBruto_" + Mid(elemento.name, "txtPujaPU_".length + 1, 0)
                bRecalcularCostesDescItem = true
            }
        }
        else
        {
			txtPUlen = ("txtPU" +  precioNum + "_").length
			if (Mid(elemento.name, 0, txtPUlen + 1) == "txtPU_")
			{
				var elementoPU = "txtPUBruto_" + Mid(elemento.name, txtPUlen + 1, 0)
				bRecalcularCostesDescItem = true
			}
            else if (Mid(elemento.name, 0, txtPUlen + 1) == "txtPU2_")
           {
				var elementoPU = "txtPU2Bruto_" + Mid(elemento.name, txtPUlen + 1, 0)
				bRecalcularCostesDescItem = true
			}
             else if (Mid(elemento.name, 0, txtPUlen + 1) == "txtPU3_")
           {
				var elementoPU = "txtPU3Bruto_" + Mid(elemento.name, txtPUlen + 1, 0)
				bRecalcularCostesDescItem = true
			}
        }

        if(bRecalcularCostesDescItem)
            {                
                eval("document.getElementById(\"frmOfertaAEnviar\")." + elementoPU + ".value=elemento.value")
                var precioUnitarioInicial = str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt)
                var precioUnitarioInicialAnterior = str2num(elemento.defaultValue,vdecimalfmt,vthousanfmt,vprecisionfmt)
                var precioUsar = objItem.usar // precio al que se pueden modificar sus c/d
                nombre = Mid(elemento.name, 0, "txtPU_".length + 1)
                if(nombre=="txtPU_" && (precioUsar==1 || precioUsar==null))
                    divModificable = true
                else if(nombre=="txtPU2" && precioUsar==2)
                    divModificable = true
                else if(nombre=="txtPU3" && precioUsar==3)
                    divModificable = true
                else if(nombre=="txtPuj")
                    divModificable = true
                else
                    divModificable = false
                recalcularCostesDescuentosItem(vGr,objItem,precioUnitarioInicial,(elemento.getAttribute("pu")==4?1:0), oItemSecundario, precioUnitarioInicialAnterior,0,0,divModificable, iesc,0)
                linkRecalculo = 0
                linkRecalculoPendiente = 0
                generaCabeceraItems()
            }

return ret	
}


function validarPrecioUnitario(mirror,vmirror, elemento,vdecimalfmt,vthousanfmt,vprec,minimo,maximo,errMsg)
/*'************************************************************
'*** rue 01/04/2009
'*** Descripci�n: Funcion que recalcula los valores de la oferta al cambiar los campos que influyen en los sumatorios de la oferta         *****
'*** Par�metros de entrada: *****
'*** mirror			input sobre el que se realiza la accion
'*** vmirror		longitud del mirror
'*** elemento		item sobre el que se hace la modificacion
'*** vdecimalfmt	formato decimal del usuario
'*** vthousanfmt	formato miles del usuario
'*** vprec			
'*** minimo			valor minimo aldmitido para ese campo
'*** maximo			valor maximo aldmitido para ese campo
'*** errMsg			error que se ha producido
'*** Par�metros de entrada: atributoOfertado: el atributo          *****
'*** Par�metros de entrada: sFrm: Formulario, lo utilizan los input de fechas          *****
'*** Par�metros de salida:     *****                    
'***                                               *****                    
'*** Llamada desde: el onchange de los inputs de tipo numero  						*****                    
'** Tiempo m�ximo:   						*****                    
'************************************************************
*/
{
var ret
var i
ret = validarNumero(elemento,vdecimalfmt,vthousanfmt,vprec,minimo,maximo,errMsg,"<%=JSText(den(56))%>" + (minimo!=null?"\n<%=JSText(den(37))%> " + num2str(minimo,vdecimalfmt,vthousanfmt,vprecisionfmt):"") + (maximo!=null?"\n<%=JSText(den(38))%> " + num2str(maximo,vdecimalfmt,vthousanfmt,vprecisionfmt):""))

var precioUsar = oItem.usar // precio al que se pueden modificar sus c/d
if(elemento.getAttribute("pu")==1 && (precioUsar==1 || precioUsar==null))
    divModificable = true
else if(elemento.getAttribute("pu")==2 && precioUsar==2)
    divModificable = true
else if(elemento.getAttribute("pu")==3 && precioUsar==3)
    divModificable = true
else if(elemento.getAttribute("pu")==4)
    divModificable = true
else
    divModificable = false

if(divModificable)
{
    if(linkRecalculoPrevio == 2)
        linkRecalculoPrevio = linkRecalculo
    //linkRecalculo = 1
    if(linkRecalculoPendiente == 1)
        linkRecalculo = 1
}

if (!ret)
	{
	return false
	}
else
	{
	proceso.estado=4
	if (mirror!="null" && mirror!=null && mirror!="" && document.forms["frmOfertaAEnviar"].elements[mirror])
		document.forms["frmOfertaAEnviar"].elements[mirror].value=elemento.value
	if (vmirror!="undefined" && vmirror!="null" && vmirror!=null)
		{
		oItem = eval(vmirror)
		if (oItem.mirror)
			if (oItem.mirror.length>0)
				{
				for (i=0;i<oItem.mirror.length;i++)
					eval(oItem.mirror[i] + ".precio =" + str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt))
				}
		}
	}	
sEscId =""
if (elemento.getAttribute("cant"))	
	{
	
	switch(elemento.getAttribute("pu"))
		{
		case "1":
		case "2":
		case "3":
			codigos = elemento.name.split("_")
			sGrpCod = codigos[1]
			sItemId = codigos[2]
			if (codigos.length>=4) sEscId="_" + codigos[3]

			break;
		case "4":
			codigos = elemento.name.split("_")
			sGrpCod = ""
			sItemId = codigos[1]
			if (codigos.length>=3) sEscId="_" + codigos[2]

			break;
		}
	if (sGrpCod!="")		
		{
		for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != sGrpCod;i++)
			{
			}
		oObj = proceso.grupos[i]
					
		for (i=0;i<oObj.items.length && oObj.items[i].id != sItemId;i++)
			{
			}
		oObj = oObj.items[i]
		}
	else
		{
		
		for (i=0;i<itemsPuja.length && itemsPuja[i].id != sItemId;i++)
			{
			}
		oObj = itemsPuja[i]
		}

var iesc = -1;
var i
if(sEscId!="")
{
	for (i=0; ((i<oObj.escalados.length) && ('_' +oObj.escalados[i].id != sEscId));i++) {}
	if ('_' + oObj.escalados[i].id == sEscId) iesc= i
}

		
		
		
	switch(elemento.getAttribute("pu"))
		{
		case "1":
		case "4":
            precioUnitarioInicialAnterior = oObj.precio
			if (iesc>=0)
			{
				oObj.escalados[iesc].precio = str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt)
				if (oObj.cantidad)
				{
					oObj.precio = str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt)
				}				
			}
			else
			{
				oObj.precio = str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt)
			}
			break
		case "2":
            precioUnitarioInicialAnterior = oObj.precio2
			oObj.precio2 = str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt)
			break
		case "3":
            precioUnitarioInicialAnterior = oObj.precio3
			oObj.precio3 = str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt)
			break
		}
	
	strDetOferta = ""
	if (solicitarCantMax)
	{
		sNamePU = elemento.id	
		

		sTmp1 = sNamePU.replace("txtPU_","txtCantMax_")	
		sTmp2 = sNamePU.replace("txtPU2_","txtCantMax_")	
		sTmp3 = sNamePU.replace("txtPU3_","txtCantMax_")	
        sTmp4 = sNamePU.replace("txtPUI_","txtCantMax_")
			
		if (sTmp1 != sNamePU )
		{		
			elemento2 = document.forms["frmOfertaAEnviar"].elements[sTmp1]
		}
		else
		{
			if (sTmp2 != sNamePU )
				elemento2 = document.forms["frmOfertaAEnviar"].elements[sTmp2]
			else				
                {
                    if (sTmp3 != sNamePU )
			            elemento2 = document.forms["frmOfertaAEnviar"].elements[sTmp3]
                    else
                        elemento2 = document.forms["frmOfertaAEnviar"].elements[sTmp4]
		        }			
			
		}	
			
		if ((elemento2.value != "" ) && (str2num(elemento2.value,vdecimalfmt,vthousanfmt,vprecisionfmt) <= elemento.getAttribute("cant")) && (elemento2.value != null) && (str2num(elemento2.value,vdecimalfmt,vthousanfmt,vprecisionfmt) > 0) )				
			strDetOfertaAux = "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * str2num(elemento2.value,vdecimalfmt,vthousanfmt,vprecisionfmt),vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"						
		else
			strDetOfertaAux = "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * elemento.getAttribute("cant"),vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"		
	}		
	else
	{
		strDetOfertaAux = "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * elemento.getAttribute("cant"),vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"	
	}
	//dpd incidencia 18612
    if(divModificable)
	    if(oItem.importeNeto!=null) strDetOfertaAux = "<b>" + num2str(oItem.importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"  

    if(elemento.value == "") strDetOfertaAux = ""
	
	if (iesc>=0)
	{
		if (!oObj.escalados[iesc].usar)  strDetOfertaAux = ""		
	}
     strDetOferta += strDetOfertaAux
	if (document.getElementById("divTotalLinea" + elemento.getAttribute("pu") + "_" + elemento.getAttribute("itemid") + sEscId)) document.getElementById("divTotalLinea" + elemento.getAttribute("pu") + "_" + elemento.getAttribute("itemid") + sEscId).innerHTML = strDetOferta
	
	if (elemento.getAttribute("pu")=="1" ||elemento.getAttribute("pu")=="2" ||elemento.getAttribute("pu")=="3" || elemento.getAttribute("pu")=="4" )
		{
		
		if (elemento.getAttribute("pu")=="4")
			if (document.getElementById("divTotalLinea1_" + elemento.itemid + sEscId)) 
				document.getElementById("divTotalLinea1_" + elemento.itemid + sEscId).innerHTML = strDetOferta
            else if (document.getElementById("divTotalLinea2_" + elemento.itemid + sEscId))
				document.getElementById("divTotalLinea2_" + elemento.itemid + sEscId).innerHTML = strDetOferta
            else if (document.getElementById("divTotalLinea3_" + elemento.itemid + sEscId))
				document.getElementById("divTotalLinea3_" + elemento.itemid + sEscId).innerHTML = strDetOferta
		if (elemento.getAttribute("pu")=="1")
			if (document.getElementById("divTotalLinea4_" + elemento.itemid + sEscId))
				document.getElementById("divTotalLinea4_" + elemento.itemid + sEscId).innerHTML = strDetOferta
    
    generaCabeceraItems()

	<%if oProceso.subasta then%>
			  
				cadena = strT
				//Se pasa con timeOut ya que sino al cambiar la cantidad y pulsar directamente en el boton de pujar
				//no coge el evento onclick!!!!!!!                
                
                setTimeout ("MostrarCabeceraOferta()",700)
                generaCabecerasGruposSubasta()
		

/*		DPD 
        La cabecera de grupo ya no se escribe aqu�.
        Es necesaria la informaci�n de posici�n y de proveedor ganador as� que se escrbir� una vez aceptado el importe
        En la funci�n generaCabecerasGruposSubasta()
*/

		<%end if%>
		elemento.oldValue = str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt)

        
        if(pu==4)
            var elementoPU = "txtPujaPUBruto_" + Mid(elemento.name, "txtPujaPUI_".length + 1, 0)
        else if(pu==1)
            var elementoPU = "txtPUBruto_" + Mid(elemento.name, "txtPUI_".length + 1, 0)
        else if(pu==2)
            var elementoPU = "txtPU2Bruto_" + Mid(elemento.name, "txtPUI_".length + 1, 0)
        else if(pu==3)
            var elementoPU = "txtPU3Bruto_" + Mid(elemento.name, "txtPUI_".length + 1, 0)
        eval("document.getElementById(\"frmOfertaAEnviar\")." + elementoPU + ".value=elemento.value")
        
		}
	}

if (sGrpCod!="")		
{
	for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != sGrpCod;i++)
		{
		}
	objGrupo = proceso.grupos[i]
					
	for (i=0;i<objGrupo.items.length && objGrupo.items[i].id != sItemId;i++)
		{
		}
	objItem = objGrupo.items[i]
    
     <%if oProceso.subasta then%>
    for(indiceIt=0;indiceIt<itemsPuja.length;indiceIt++)
            if(itemsPuja[indiceIt].id == objItem.id)
            {
                itemsPuja[indiceIt].precio = eval("objItem.precio" + precioNum)
                break
            }
    <%end if %>
}
else
{
		
	for (i=0;i<itemsPuja.length && itemsPuja[i].id != sItemId;i++)
		{
		}
	objItem = itemsPuja[i]

    if(vGr.items.length)
    {
        for(indiceIt=0;indiceIt<vGr.items.length;indiceIt++)
            if(vGr.items[indiceIt].id == objItem.id)
            {
                
                eval("vGr.items[indiceIt].precio" + precioNum) = objItem.precio
                break
            }
    }

}


//Totales

if(linkRecalculoItem == 0)
{
    linkRecalculoItem = 1
    if(linkRecalculoPrevio == 2)
                linkRecalculoPrevio = linkRecalculo
    if(pu==4)
        linkRecalcularItem("txtPujaPU_", precioUnitarioInicialAnterior, null, null, null, divModificable)
    else if(pu==1)
        linkRecalcularItem("txtPU_", precioUnitarioInicialAnterior, null, null, null, divModificable)
    else if(pu==2)
        linkRecalcularItem("txtPU2_", precioUnitarioInicialAnterior, null, null, null, divModificable)
    else if(pu==3)
        linkRecalcularItem("txtPU3_", precioUnitarioInicialAnterior, null, null, null, divModificable)

}

return ret
}


/*
''' <summary>
''' muestra la cabecera en el formulario de los �tems
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Tiempo m�ximo: 0</remarks>
''' <author>JVS</author>
*/
function generaCabeceraItems()
{
    
    var ind
        if (proceso.estado==1)
        {

        }
	
		for (ind=0;ind<proceso.grupos.length;ind++)
			{
			if (!document.getElementById("divTotalOferta_" + proceso.grupos[ind].cod) && document.getElementById("tbl_" + proceso.grupos[ind].cod))
				{
				ha=52
				resize()
				}
			sEstado = estadoOferta(proceso.estado)
            str = sEstado + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  (linkRecalculo==0?num2str(proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt):"-") + "&nbsp;" + proceso.mon.cod + "&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(null)' onclick='verAyuda(" + proceso.estado + ")'><IMG border=0 SRC=../images/masinform.gif  WIDTH=11 HEIGHT=14 name=imgMasInform id=imgMasInform></a>"
            
            if(linkRecalculo==1)
                str+="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class='popUp' href='javascript:void(null)' onclick=\"linkRecalculoOferta()\"><img src=\"images/RecOferTot1-25x25.png\"  border=0 width=19 height=19 / ><%=JSText(den(128))%></a>"  //Recalcular totales de oferta
			sClase = ""
            
			switch (proceso.estado)
				{
				case 1:
					sClase = "OFENUEVA"
					break;
				case 2:
					sClase = "OFESINENVIAR"
					break;
				case 3:
				case 5:
					sClase = "OFEENVIADA"
					break;
				case 4:
					sClase = "OFESINGUARDAR"
					break
				default:
					sClase = ""
					break;
				}
            tb= tbvar //DPD
            if (proceso.estado==1)
            {
                 if (document.getElementById("divTotalOferta_" + proceso.grupos[ind].cod))
	            {	
                    document.getElementById("divTotalOferta_" + proceso.grupos[ind].cod).innerHTML  = ""
                }
            }
            else
            {
                if (document.getElementById("divTotalOferta_" + proceso.grupos[ind].cod) && sClase!="")
	            {				
	                document.getElementById("divTotalOferta_" + proceso.grupos[ind].cod).innerHTML  = str
	                document.getElementById("divTotalOferta_" + proceso.grupos[ind].cod).parentNode.className=sClase
	            }
            }
                
        }

        	<%if oProceso.subasta then%>
			
				cadena = str
				//Se pasa con timeOut ya que sino al cambiar la cantidad y pulsar directamente en el boton de pujar
				//no coge el evento onclick!!!!!!!                
                setTimeout ("MostrarCabeceraOferta()",700)

                generaCabecerasGruposSubasta()
            <%end if %>
}


/*
''' <summary>
''' muestra la cabecera de grupos en caso de subasta
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Tiempo m�ximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
*/
function generaCabecerasGruposSubasta()
{



for (i=0;i<proceso.grupos.length;i++)
	{
	if (document.getElementById("divCabeceraPreciosSubasta_" + proceso.grupos[i].cod))
		{
              strT ="<table width='100%'  height='"+ (hCabeceraGrupo+5) +"' border=0><tr>"
              strT += "<td class='cabecera' width='25%'>"
              strT += "<table border='0' cellspacing='0' cellpadding='0'><tr><td>"
              strT += "<a class=ayuda href='javascript:void(null)' onclick='ocultarPujaGrupo(\"" + proceso.grupos[i].cod + "\")' title = '" + proceso.grupos[i].den + "'>"
              strT += "<IMG style='position:absolute;top:5px;left:5px;visibility:hidden;' name=imgGrupoA" + proceso.grupos[i].cod + " id=imgGrupoA" + proceso.grupos[i].cod + " border=0 SRC=images/arribab.gif>"
              strT += "<IMG style='position:absolute;top:5px;left:5px;visibility:hidden;' name=imgGrupoB" + proceso.grupos[i].cod + " id=imgGrupoB" + proceso.grupos[i].cod + " border=0 SRC=images/abajob.gif>"
              strT += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>"
              strT += "</td><td class='cabecera'>"
              strT += "<div style='overflow:hidden;height=12' title='" + "<%=den(57) %> " + proceso.grupos[i].cod  + " - " + proceso.grupos[i].den  + "'>"                           
              strT += "<%=den(57) %> " + proceso.grupos[i].cod  + " - " + proceso.grupos[i].den + "</div>"
              strT += "</td></tr></table>"
              strT += "</td>"              

                if (proceso.grupos[i].importe ==null)
                {
				    strT +="<td class='cabecera' width='25%'> &nbsp;" 
                }
                else
                {
				    strT +="<td class='cabecera' width='25%'> <%=den(86) %>:&nbsp;" + num2str(proceso.grupos[i].importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;" + proceso.mon.cod
                }
		if (proceso.grupos[i].importe > 0) {
			strT += "</td><td width='20%'>"
			if (document.getElementById("divPosicionPujaGrupo_" + proceso.grupos[i].cod)) {
				strT2 = document.getElementById("divPosicionPujaGrupo_" + proceso.grupos[i].cod).innerHTML
				strT += "<div name=divPosicionPujaGrupo_"+proceso.grupos[i].cod + " id=divPosicionPujaGrupo_"+proceso.grupos[i].cod + ">"
				strT += strT2
				strT+= "</div>"	
			}
			
		}else {
			strT += "</td><td width='20%'>"
		}
				
        strT += "</td>"
        if (document.getElementById("divGanadorPujaGrupo_" + proceso.grupos[i].cod)) {
            strT += "<td width=30% align=center class='cabecera' nowrap>"
            strT += "<div name=divGanadorPujaGrupo_"+proceso.grupos[i].cod + " id=divGanadorPujaGrupo_"+proceso.grupos[i].cod + ">"
            strT += document.getElementById("divGanadorPujaGrupo_" + proceso.grupos[i].cod).innerHTML
            strT += "</div>"
            strT += "</td>"
        }
		strT += "</tr></table>"



		document.getElementById("divCabeceraPreciosSubasta_" + proceso.grupos[i].cod).innerHTML  = strT
		}
	}
}

/*
''' <summary>
''' Devuelve una cadena desde la posici�n n, con c caracteres
''' </summary>
<param name=s>Cadena a partir</param>
<param name=n>Posici�n de inicio</param>
<param name=c>N�mero de caracteres a partir</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Tiempo m�ximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
*/

function Mid(s, n, c){
	// Devuelve una cadena desde la posici�n n, con c caracteres
	// Si c = 0 devolver toda la cadena desde la posici�n n
	
	var numargs=Mid.arguments.length;
	
	// Si s�lo se pasan los dos primeros argumentos
	if(numargs<3)
		c=s.length-n+1;
		
	if(c<1)
		c=s.length-n+1;
	if(n+c >s.length)
		c=s.length-n+1;
	if(n>s.length)
		return "";
		
	return s.substring(n-1,n+c-1);
}









function MostrarPosicionPuja()
{

MostrarCabeceraOferta()


for (i=0;i<proceso.grupos.length;i++)
       {
       if (document.getElementById("divCabeceraPreciosSubasta_" + proceso.grupos[i].cod))
              {

              //Primera carga del grupo
              strT ="<table width='100%'  height='"+ (hCabeceraGrupo+5) +"' border=0><tr>"
              strT += "<td class='cabecera' width='25%'>"
              strT += "<table border='0' cellspacing='0' cellpadding='0'><tr><td>"
              strT += "<a class=ayuda href='javascript:void(null)' onclick='ocultarPujaGrupo(\"" + proceso.grupos[i].cod + "\")' title = '" + proceso.grupos[i].den + "'>"
              strT += "<IMG style='position:absolute;top:5px;left:5px;visibility:visible;' name=imgGrupoA" + proceso.grupos[i].cod + " id=imgGrupoA" + proceso.grupos[i].cod + " border=0 SRC=images/arribab.gif>"
              strT += "<IMG style='position:absolute;top:5px;left:5px;visibility:hidden;' name=imgGrupoB" + proceso.grupos[i].cod + " id=imgGrupoB" + proceso.grupos[i].cod + " border=0 SRC=images/abajob.gif>"
              strT += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>"
              strT += "</td><td class='cabecera'>"
              strT += "<div style='overflow:hidden;height=12' title='" + "<%=den(57) %> " + proceso.grupos[i].cod  + " - " + proceso.grupos[i].den  + "'>"                           
              strT += "<%=den(57) %> " + proceso.grupos[i].cod  + " - " + proceso.grupos[i].den + "</div>"
              strT += "</td></tr></table>"
              strT += "</td>"              
                if (proceso.grupos[i].importe ==null)
                {
				    strT +="<td class='cabecera' width='25%'> &nbsp;" 
                }
                else
                {
				    strT +="<td class='cabecera' width='25%'> <%=den(86) %>:&nbsp; " + num2str(proceso.grupos[i].importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;" + proceso.mon.cod + "</td>"
                }

                     
              //posicion Puja GRUPO Primera Carga
            
            if (bAbierta && !bSobreCerrado && !sinPujas)
            {
            if (proceso.grupos[i].posicionPuja > 0) {
                     strT += "<td width='20%'>"
                     strT += "<div name=divPosicionPujaGrupo_"+proceso.grupos[i].cod + " id=divPosicionPujaGrupo_"+proceso.grupos[i].cod + ">"
              
                     if ((proceso.modo == 2) && (bAbierta && !bSobreCerrado && !sinPujas))
                     {
                         strT += "<table cellpadding='0' cellspacing='0' width='100%' height='20' class='POSICIONPUJAGRUPO'><tr>"
                     }
                     else
                     {
                        strT += "<table cellpadding='0' cellspacing='0' width='100%' height='20'><tr>"
                     }
                     
                     strT += "<td align=center class='cabecera'>"
                     if (bAbierta && !bSobreCerrado && !sinPujas)
                     {                     
                         strT+="<%=den(84) %>:" 
                         strT+="<b>"   
                         strT+=proceso.grupos[i].posicionPuja 
                         strT+="</b>"  
                     }
                     strT+= "</td></tr></table>" 
                     strT+= "</div>"      
                     
            }else {
                   strT += "<td width='20%'>"
                   strT += "<div name=divPosicionPujaGrupo_"+proceso.grupos[i].cod + " id=divPosicionPujaGrupo_"+proceso.grupos[i].cod + ">"
                     
                   strT+= "</div>"      
            }
              strT += "</td>"
            }
            <%if oProceso.modo = 2  then        'Modo Grupo     %>
                <% if oProceso.verproveganador or oProceso.verprecioganador  then %>

                    if (bAbierta && !bSobreCerrado && !sinPujas) 
                    {
                      strT += "<td width=30% align=center class='cabecera' nowrap>"
                      strT += "<div name=divGanadorPujaGrupo_"+proceso.grupos[i].cod + " id=divGanadorPujaGrupo_"+proceso.grupos[i].cod + ">"
                      strT += "divGanadorPujaGrupo_"+proceso.grupos[i].cod                
                      strT +="</div>"
                      strT += "</td>"
                    }
                <%end if %>
            <%end if %>
              strT += "</tr></table>"
              document.getElementById("divCabeceraPreciosSubasta_" + proceso.grupos[i].cod).innerHTML  = strT
              }
       }
}

//''' <summary>
//''' Establece los valores y el estilo que se muestran en la cabecera
//''' </summary>
//''' <remarks>Llamada desde: cumpoferta.asp (6 llamadas)      cumpoferta.js (2 llamadas); Tiempo m�ximo: 0,2</remarks>
function MostrarCabeceraOferta()
{   
    if (proceso.estado > 1)
    {

	    if (!document.getElementById("divTotalOfertaSubasta") && document.getElementById("tblSubasta"))
				    {
				    //Se pasa con timeOut ya que sino al cambiar la cantidad y pulsar directamente en el boton de pujar
				    //no coge el evento onclick!!!!!!!	
	    oTR = document.getElementById("tblSubasta").insertRow(0)
	    oTR.height= 15
	    oTD = oTR.insertCell(-1)
	    ha=52
	    strDivs = "<div name=divTotalOfertaSubasta id=divTotalOfertaSubasta style='overflow:hidden'></div>" 
        oTD.innerHTML += strDivs
	    document.getElementById("divApartadoSPuja").style.top = '50px'

	    resize()
	
	    }
    }



       strT ="<table cellpadding='0' cellspacing='0' border=0>"
       strT +="<tr>"
       strT +="<td class='" +sClase +"'>"
       strT += sEstado + "&nbsp;&nbsp;" +  num2str(proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;" + proceso.mon.cod + "&nbsp;&nbsp;<a href='javascript:void(null)' onclick='verAyuda(" + proceso.estado + ")'><IMG border=0 SRC=../images/masinform.gif  WIDTH=11 HEIGHT=14 name=imgMasInform id=imgMasInform></a>"
       
       strT += "</td>"
       strT += "<td  class='" +sClase +"'>&nbsp;&nbsp;&nbsp;&nbsp;</td>"

       if (verDesdePrimeraPuja==false || (verDesdePrimeraPuja == true && hayOfertaProveedor))
       {
		   if ( proceso.posicionPuja > 0 ){
				  //Posicion Puja PROCESO
				  strT += "<td >"
				  if (proceso.modo == 1)
				  {              
					strT += "<div id='MostrarPosicionPujaProceso' class='POSICIONPUJAPROCESO' "
				  }
				  else
				  {
					//document.getElementById("divTotalOfertaSubasta").style.visibility = "visible"  
					strT += "<div id='MostrarPosicionPujaProceso' "
				  }
				  if (bAbierta && !bSobreCerrado && !sinPujas) strT += " style='visibility:visible'>"
				  else strT += "style='visibility:hidden'>"
				  strT += "<table border=0 cellspacing=0 cellpadding=0><tr>"
				  strT += "<td class='" +sClase +"' >&nbsp;&nbsp;&nbsp;&nbsp; <%=den(84) %>:</td>"
				  strT += "<td class='" +sClase +"' >"
				  strT += "<div name=divPosicionPujaProceso id=divPosicionPujaProceso>"
				  strT +="<b>&nbsp;" + proceso.posicionPuja  +"</b>"
				  strT +="</div>"
				  strT +="</td><td  class='" +sClase +"'>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>"
				  strT +="</div>"
				  strT +="</td>"
				}
			else {
				  
				  //Posicion Puja PROCESO
				  strT += "<td class='" +sClase +"'>"
				  if (proceso.modo == 1)
				  {              
					strT += "<div id='MostrarPosicionPujaProceso' class='POSICIONPUJAPROCESO' "
				  }
				  else
				  {
					strT += "<div id='MostrarPosicionPujaProceso' "
				  }
				  strT += "style='visibility:hidden'>"
				  strT += "<table border=0 cellspacing=0 cellpadding=0><tr>"
				  strT += "<td class='" +sClase +"' >&nbsp;&nbsp;&nbsp;&nbsp; <%=den(84) %>:</td>"
				  strT += "<td class='" +sClase +"' >"


				  strT += "<div name=divPosicionPujaProceso id=divPosicionPujaProceso>"              
				  strT +="</div>"
				  strT +="</td>"
			}
        }
        else
        {
                strT += "<td><div id='MostrarPosicionPujaProceso' style='visibility:hidden'></div></td> "                
        }
              strT += "<td  class='" +sClase +"'>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>"
    
       if (document.getElementById("divTotalOfertaSubasta"))
              {
              document.getElementById("divTotalOfertaSubasta").innerHTML  = strT
              document.getElementById("divTotalOfertaSubasta").style.backgroundcolor="#00FF00"
              document.getElementById("divTotalOfertaSubasta").parentNode.className=sClase
              }
}



function recalcularTotales()
{
var sNameCantMaxl
proceso.importe = 0
for (i=0;i<proceso.grupos.length;i++)
	{
	if (proceso.grupos[i].items.length>0)
		{
		proceso.grupos[i].importe = 0	
		for (j=0;j<proceso.grupos[i].items.length;j++)
			{

			sNamePU = "txtPU_" + proceso.grupos[i].cod + "_" + proceso.grupos[i].items[j].id
			sNameCantMaxl = "txtCantMax_" + proceso.grupos[i].cod + "_" + proceso.grupos[i].items[j].id
			if (solicitarCantMax)
			{
				if ( ( str2num(document.forms["frmOfertaAEnviar"].elements[sNameCantMaxl].value,vdecimalfmt,vthousanfmt,vprecisionfmt) <= proceso.grupos[i].items[j].cant ) && (document.forms["frmOfertaAEnviar"].elements[sNameCantMaxl].value != "") && (str2num(document.forms["frmOfertaAEnviar"].elements[sNameCantMaxl].value,vdecimalfmt,vthousanfmt,vprecisionfmt) > 0) )								
				{
					proceso.grupos[i].importe += (str2num(document.forms["frmOfertaAEnviar"].elements[sNamePU].value,vdecimalfmt,vthousanfmt,vprecisionfmt) * str2num(document.forms["frmOfertaAEnviar"].elements[sNameCantMaxl].value,vdecimalfmt,vthousanfmt,vprecisionfmt) )					
				}	
				else
				{
					proceso.grupos[i].importe += (str2num(document.forms["frmOfertaAEnviar"].elements[sNamePU].value,vdecimalfmt,vthousanfmt,vprecisionfmt) * proceso.grupos[i].items[j].cant)					
				}
			}
			else
			{
				proceso.grupos[i].importe += (str2num(document.forms["frmOfertaAEnviar"].elements[sNamePU].value,vdecimalfmt,vthousanfmt,vprecisionfmt) * proceso.grupos[i].items[j].cant)
			}
			elemento = document.forms["frmOfertaAEnviar"].elements[sNamePU]
			elemento2 = document.forms["frmOfertaAEnviar"].elements[sNameCantMaxl]
			
			if (elemento.getAttribute("cant") && elemento.value!="")	
				{					
					if (solicitarCantMax)
					{
						strDetOferta = ""							
						if ( str2num(elemento2.value,vdecimalfmt,vthousanfmt,vprecisionfmt)  <= str2num(elemento.getAttribute("cant"),vdecimalfmt,vthousanfmt,vprecisionfmt) && (elemento2.value != "") && (str2num(elemento2.value,vdecimalfmt,vthousanfmt,vprecisionfmt) > 0) )
						{
							strDetOferta += "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * str2num(elemento2.value,vdecimalfmt,vthousanfmt,vprecisionfmt),vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"
						}
						else
						{	
							strDetOferta += "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * elemento.getAttribute("cant"),vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"							
						}
						 //@@DPD Escalados 4
						document.getElementById("divTotalLinea" + elemento.getAttribute("pu") + "_" + elemento.getAttribute("itemid")).innerHTML = strDetOferta
					}
					else
					{
						 //@@DPD Escalados 5
						strDetOferta = ""
						strDetOferta += "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * elemento.getAttribute("cant"),vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"
						document.getElementById("divTotalLinea" + elemento.getAttribute("pu") + "_" + elemento.getAttribute("itemid")).innerHTML = strDetOferta					
					}
				}
			}
		}
	if (document.getElementById("divCabeceraPrecios_" + proceso.grupos[i].cod))
		document.getElementById("divCabeceraPrecios_" + proceso.grupos[i].cod).innerHTML ="<%=den(57)%> " + proceso.grupos[i].cod + "&nbsp;<%=den(58)%>" + num2str(proceso.grupos[i].importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;" + proceso.mon.cod
	proceso.importe += proceso.grupos[i].importe	
	}


sEstado = estadoOferta(proceso.estado)
strT = sEstado + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  (linkRecalculo==0?num2str(proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt):"-") + "&nbsp;" + proceso.mon.cod + "&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(null)' onclick='verAyuda(" + proceso.estado + ")'><IMG border=0 SRC=../images/masinform.gif  WIDTH=11 HEIGHT=14 name=imgMasInform id=imgMasInform></a>"

if(linkRecalculo==1)
    strT+="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(null)' onclick=\"linkRecalculoOferta()\"><img src=\"images/RecOferTot1-25x25.png\"  border=0 width=19 height=19 / > <%=JSText(den(128))%></a>"  //Recalcular totales de oferta
sClase = ""

switch (proceso.estado)
	{
	case 1:
        sClase = "OFENUEVA"
		break;
	case 2:
		sClase = "OFESINENVIAR"
		break;
	case 3:
	case 5:
		sClase = "OFEENVIADA"
		break;
	case 4:
		sClase = "OFESINGUARDAR"
		break
	default:
		sClase = ""
		break;
	}
    tb= tbvar //DPD
    resize()
    
				
for (i=0;i<proceso.grupos.length;i++)
	{
	if (document.getElementById("divTotalOferta_" + proceso.grupos[i].cod) && sClase!="")
		{
		document.getElementById("divTotalOferta_" + proceso.grupos[i].cod).innerHTML  = strT
		document.getElementById("divTotalOferta_" + proceso.grupos[i].cod).parentNode.className=sClase
		}
	}
<%if oProceso.subasta then%>
proceso.importe = 0
for (j=0;j<proceso.grupos.length;j++)
	proceso.grupos[j].importe =0
for (i=0;i<itemsPuja.length;i++)
	{
		
	sNamePU = "txtPujaPU_" + itemsPuja[i].id
		
	proceso.importe += str2num(document.forms["frmOfertaAEnviar"].elements[sNamePU].value,vdecimalfmt,vthousanfmt,vprecisionfmt) * itemsPuja[i].cant
	elemento = document.forms["frmOfertaAEnviar"].elements[sNamePU]
	
	if (elemento.getAttribute("cant") && elemento.value!="")	
		{
		strDetOferta = ""
		strDetOferta += "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * elemento.getAttribute("cant"),vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"
		  //@@DPD Escalados 6
		document.getElementById("divTotalLinea" + elemento.getAttribute("pu") + "_" + elemento.getAttribute("itemid")).innerHTML = strDetOferta
		}
	for (j=0;j<proceso.grupos.length;j++)
		if (proceso.grupos[j].cod==itemsPuja[i].grupo)
			proceso.grupos[j].importe += str2num(document.forms["frmOfertaAEnviar"].elements[sNamePU].value,vdecimalfmt,vthousanfmt,vprecisionfmt) * itemsPuja[i].cant

	}
		

	strT ="<table cellpadding='0' cellspacing='0'><tr><td class='" +sClase +"'>"
    strT += sEstado + "&nbsp;&nbsp;" +  num2str(proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;" + proceso.mon.cod + "&nbsp;&nbsp;<a href='javascript:void(null)' onclick='verAyuda(" + proceso.estado + ")'><IMG border=0 SRC=../images/masinform.gif  WIDTH=11 HEIGHT=14 name=imgMasInform id=imgMasInform></a>"
    
	if (proceso.estado != 2) {
        if (bAbierta && !bSobreCerrado && !sinPujas)
        {
		strT += "&nbsp;&nbsp;&nbsp;&nbsp; <%=den(84) %>: </td>"
		//Posicion Puja PROCESO
		strT += "<td class='" +sClase +"'>"
		strT += "<div name=divPosicionPujaProceso id=divPosicionPujaProceso>"
		strT +="<b>&nbsp;" + proceso.posicionPuja  +"</b>"
		strT +="</div>"
		strT +="</<td>"

		strT += "</tr></table>"
        }
	}
    
	if (document.getElementById("divTotalOfertaSubasta"))
		{
		document.getElementById("divTotalOfertaSubasta").innerHTML  = strT
		document.getElementById("divTotalOfertaSubasta").parentNode.className=sClase
		}
for (i=0;i<proceso.grupos.length;i++)
	{
	if (document.getElementById("divCabeceraPreciosSubasta_" + proceso.grupos[i].cod))
		{

		//Primera carga del grupo
        strT ="<table width='100%'  height='"+ (hCabeceraGrupo+5) +"' border=0><tr>"
        strT += "<td class='cabecera' width='25%'>"
        strT += "<table border='0' cellspacing='0' cellpadding='0'><tr><td>"
        strT += "<a class=ayuda href='javascript:void(null)' onclick='ocultarPujaGrupo(\"" + proceso.grupos[i].cod + "\")' title = '" + proceso.grupos[i].den + "'>"
        strT += "<IMG style='position:absolute;top:5px;left:5px;visibility:visible;' name=imgGrupoA" + proceso.grupos[i].cod + " id=imgGrupoA" + proceso.grupos[i].cod + " border=0 SRC=images/arribab.gif>"
        strT += "<IMG style='position:absolute;top:5px;left:5px;visibility:hidden;' name=imgGrupoB" + proceso.grupos[i].cod + " id=imgGrupoB" + proceso.grupos[i].cod + " border=0 SRC=images/abajob.gif>"
        strT += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>"
        strT += "</td><td class='cabecera'>"
        strT += "<div style='overflow:hidden;height=12' title='" + "<%=den(57) %> " + proceso.grupos[i].cod  + " - " + proceso.grupos[i].den  + "'>"                           
        strT += "<%=den(57) %> " + proceso.grupos[i].cod  + " - " + proceso.grupos[i].den + "</div>"
        strT += "</td></tr></table>"
        strT += "</td>"              
        if (proceso.grupos[i].importe ==null)
        {
			strT +="<td class='cabecera' width='25%'> &nbsp;" 
        }
        else
        {
			strT +="<td class='cabecera' width='25%'> <%=den(86) %>:&nbsp; " + num2str(proceso.grupos[i].importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;" + proceso.mon.cod + "</td>"
        }

		//posicion Puja GRUPO Primera Carga
		if (proceso.estado != 2) {
		if (proceso.grupos[i].posicionPuja > 0) {
			strT += "<td width='20%'>"
			strT += "<div name=divPosicionPujaGrupo_"+proceso.grupos[i].cod + " id=divPosicionPujaGrupo_"+proceso.grupos[i].cod + ">"
			
			strT += "<table cellpadding='0' cellspacing='0' width='100%' height='20'><tr>"
			strT += "<td class='cabecera'>"
            if (bAbierta && !bSobreCerrado && !sinPujas)
            {
                strT += "<%=den(84) %>: "
			    strT+="<b>" +  proceso.grupos[i].posicionPuja+ "</b>"	
            }
			strT+= "</td></tr></table>"	
			strT+= "</div>"	
			
		}else {
			strT += "<td width='20%'>"
			strT += "<div name=divPosicionPujaGrupo_"+proceso.grupos[i].cod + " id=divPosicionPujaGrupo_"+proceso.grupos[i].cod + ">"
			
			strT+= "</div>"	
		
		}
		}
        strT += "</td>"
        if (document.getElementById("divGanadorPujaGrupo_" + proceso.grupos[i].cod)) {
            strT += "<td width=30% align=center class='cabecera' nowrap>"
            strT += "<div name=divGanadorPujaGrupo_"+proceso.grupos[i].cod + " id=divGanadorPujaGrupo_"+proceso.grupos[i].cod + ">"
            strT += document.getElementById("divGanadorPujaGrupo_" + proceso.grupos[i].cod).innerHTML
            strT += "</div>"
            strT += "</td>"
        }
		strT += "</tr></table>"
			
	
		document.getElementById("divCabeceraPreciosSubasta_" + proceso.grupos[i].cod).innerHTML  = strT
		}
	}
<%end if%>
}



function dibujarCabeceraSubasta()
{

sEstado = estadoOferta(proceso.estado)
strT = sEstado + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  (linkRecalculo==0?num2str(proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt):"-") + "&nbsp;" + proceso.mon.cod + "&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(null)' onclick='verAyuda(" + proceso.estado + ")'><IMG border=0 SRC=../images/masinform.gif  WIDTH=11 HEIGHT=14 name=imgMasInform id=imgMasInform></a>"

if(linkRecalculo==1)
    strT+="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(null)' onclick=\"linkRecalculoOferta()\"><img src=\"images/RecOferTot1-25x25.png\"  border=0 width=19 height=19 / > <%=JSText(den(128))%></a>"  //Recalcular totales de oferta
sClase = ""

switch (proceso.estado)
	{
	case 1:
        sClase = "OFENUEVA" 
		break;
	case 2:
		sClase = "OFESINENVIAR"
		break;
	case 3:
	case 5:
		sClase = "OFEENVIADA"
		break;
	case 4:
		sClase = "OFESINGUARDAR"
		break
	default:
		sClase = ""
		break;
	}
    tb= tbvar //DPD
    resize()

    
				
strT ="<table cellpadding='0' cellspacing='0'><tr><td class='" +sClase +"'>"
strT += sEstado + "&nbsp;&nbsp;" +  num2str(proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;" + proceso.mon.cod + "&nbsp;&nbsp;<a href='javascript:void(null)' onclick='verAyuda(" + proceso.estado + ")'><IMG border=0 SRC=../images/masinform.gif  WIDTH=11 HEIGHT=14 name=imgMasInform id=imgMasInform></a>"

if (proceso.estado != 2) {
    if (bAbierta && !bSobreCerrado && !sinPujas)
    {
	strT += "&nbsp;&nbsp;&nbsp;&nbsp; <%=den(84) %>: </td>"
	//Posicion Puja PROCESO
	strT += "<td class='" +sClase +"'>"
	strT += "<div name=divPosicionPujaProceso id=divPosicionPujaProceso>"
	strT +="<b>&nbsp;" + proceso.posicionPuja  +"</b>"
	strT +="</div>"
	strT +="</<td>"

	strT += "</tr></table>"
    }
}
    
if (document.getElementById("divTotalOfertaSubasta"))
	{
	document.getElementById("divTotalOfertaSubasta").innerHTML  = strT
	document.getElementById("divTotalOfertaSubasta").parentNode.className=sClase
	}
}



function validarFechaB(e,vdatefmt,minimo,maximo)
{
var ret

ret = validarFecha(e,vdatefmt,minimo,maximo)

if (!ret)
	alert("<%=JSAlertText(den(36))%>" + vdatefmt + ((minimo!=null && minimo!="")?"\n<%=JSAlertText(den(37))%> " + minimo :"")  + ((maximo!=null && maximo!="")?"\n<%=JSText(den(38))%> " + maximo:""))

return ret
}

/*'************************************************************
'*** Descripci�n: dibuja en patalla un atributo       *****
'*** Par�metros de entrada: atributo: el atributo          *****
'*** Par�metros de entrada: atributoOfertado: el atributo          *****
'*** Par�metros de entrada: sFrm: Formulario, lo utilizan los input de fechas          *****
'*** Par�metros de salida:     *****                    
'***                                               *****                    
'*** Llamada desde: cargaritems.asp  						*****                    
'** Tiempo m�ximo:   						*****                    
'************************************************************
*/
//JORGE FIJARME BUSCANDO CAJETIN
function mostrarAtributo(atributo,atributoOfertado,sFrm,iesc, vGrupo)
{


var j
var strAtrib
strAtrib = ""
var sNameAtrib
var iBloqueado

iBloqueado=0
if (vGrupo != null)
{
    if (vGrupo.Bloqueado == 1)
    {    
        iBloqueado = 1
    }
}

if (atributoOfertado) //es atributo de un item
	{	
	sNameAtrib = "itemAtrib_" + atributo.parent.cod + "_" + atributoOfertado.item.id + "_" + atributo.paid
	
	idEsc = ""
	
	atributo.valor = atributoOfertado.valor
	
	if (iesc>=0) 
	{
		idEsc  = atributoOfertado.item.escalados[iesc].id
		sNameAtrib += "_" + idEsc		
	}			

	
	}
	
else
	if (atributo.parent.proceso) //es el atributo de un grupo
		{
		sNameAtrib = "grupoAtrib_" + atributo.parent.cod + "_" + atributo.paid
		}
	else //es atributo de proceso
		{
		sNameAtrib = "procAtrib_" + atributo.paid
		}
switch (atributo.tipo)
	{
	case 1: //Entrada de texto
		if (atributo.intro==0)
        {
			<%if bConsulta then%>
				strAtrib += (atributo.valor==null?"":atributo.valor)
			<%else%>
                if (iBloqueado == 0)
                {
                    strAtrib += inputTexto(sNameAtrib,wCAtributoT1,32471,atributo.valor,"actualizarEstado(1)")
                }
                if (iBloqueado == 1)
                {
                    strAtrib += (atributo.valor==null?"":atributo.valor)
                }
			<%end if%>
        }
		else
			{
			<%if bConsulta then%>
				strAtrib += atributo.valor
			<%else%>
                if (iBloqueado == 0)
                {
				    var j
				    var maxChars=0
				    var wCAtribTexto
                    if (atributo.lista)
				        for (j=0;j<atributo.lista.length;j++)
					        maxChars=atributo.lista[j].length>maxChars?atributo.lista[j].length:maxChars
					
				    wCAtribTexto= maxChars*8  + 25
				
				    if (wCAtribTexto < 240)
					    wCAtribTexto = 240

				    strAtrib += "<select  style='width:" + wCAtribTexto + "' name=" + sNameAtrib + " id =" + sNameAtrib + " onchange='actualizarEstado(1)'>"
								
				    strAtrib += "<option value=''></option>"
                    if (atributo.lista)
				        for (j=0;j<atributo.lista.length;j++)
					    {
					        if (atributo.lista[j]==atributo.valor)
						        strAtrib += "<option selected value='" + atributo.lista[j]  + "'>" + atributo.lista[j] + "</option>"
					        else
						        strAtrib += "<option value='" + atributo.lista[j]  + "'>" + atributo.lista[j] + "</option>"
					    }
						
				    strAtrib += "</select>"
                }
                if (iBloqueado == 1)
                {
                    strAtrib += atributo.valor
                }
			<%end if%>
			}
		break
	case 2: //Entrada de n�meros
		if (atributo.intro==0)
        {
			<%if bConsulta then%>
				strAtrib += num2str(atributo.valor,vdecimalfmt,vthousanfmt,vprecisionfmt)
			<%else%>
                if (iBloqueado == 0)
                {
				    strAtrib += inputNumero(sNameAtrib,wCAtributoT2,30,atributo.valor,atributo.valorMin,atributo.valorMax,null,"actualizarEstado(2,","<%=JSText(den(71))%>\\n<%=JSText(den(72))%> " + vdecimalfmt + "\\n<%=JSText(den(73))%> " + vthousanfmt,"<%=JSText(den(56))%>\\n<%=JSText(den(37))%> " + atributo.valorMin + "\\n<%=JSText(den(38))%> " + atributo.valorMax)
                }
                if (iBloqueado == 1)
                {
                    strAtrib += num2str(atributo.valor,vdecimalfmt,vthousanfmt,vprecisionfmt)
                }
			<%end if%>
		}
        else
			{
			<%if bConsulta then%>
				strAtrib += num2str(atributo.valor,vdecimalfmt,vthousanfmt,vprecisionfmt)
			<%else%>
                if (iBloqueado == 0)
                {
				    strAtrib += "<select  style='width:" + wCAtributoT2 + "'  name=" + sNameAtrib + " id =" + sNameAtrib + " onchange='actualizarEstado(1)'>"
				    strAtrib += "<option value=''></option>"
                    if (atributo.lista)
				        for (j=0;j<atributo.lista.length;j++)
					    {
					        if (num2str(atributo.lista[j],vdecimalfmt,vthousanfmt,10)==num2str(atributo.valor,vdecimalfmt,vthousanfmt,10))
						        strAtrib += "<option selected value='" + num2str(atributo.lista[j],vdecimalfmt,vthousanfmt,10)  + "'>" + num2str(atributo.lista[j],vdecimalfmt,vthousanfmt,vprecisionfmt) + "</option>"
					        else
						        strAtrib += "<option value='" + num2str(atributo.lista[j],vdecimalfmt,vthousanfmt,10)  + "'>" + num2str(atributo.lista[j],vdecimalfmt,vthousanfmt,vprecisionfmt) + "</option>"
					    }
				    strAtrib += "</select>"
                }
                if (iBloqueado == 1)
                {
                    strAtrib += num2str(atributo.valor,vdecimalfmt,vthousanfmt,vprecisionfmt)
                }
			<%end if%>
			}
		break
	case 3: //Entrada de fechas
		if (atributo.intro==0)
		{
			<%if bConsulta then%>
				strAtrib += date2str(atributo.valor,vdatefmt)
			<%else%>
                if (iBloqueado == 0)
                {
				    strAtrib += inputFecha(sNameAtrib,wCAtributoT3,atributo.valor,atributo.valorMin, atributo.valorMax,sFrm,"actualizarEstado","","<%=JSText(den(68))%>" + vdatefmt + "\\n<%=JSText(den(69))%>","<%=JSText(den(70))%>")
                }
                if (iBloqueado == 1)
                {
                    strAtrib += date2str(atributo.valor,vdatefmt)
                }
			<%end if%>
		}
		else
		{
			<%if bConsulta then%>
				strAtrib += date2str(atributo.valor,vdatefmt)
			<%else%>
                if (iBloqueado == 0)
                {
				    strAtrib += "<select  style='width:" + wCAtributoT3 + "'  name=" + sNameAtrib + " id =" + sNameAtrib + "  onchange='actualizarEstado(1)'>"
				    strAtrib += "<option value=''></option>"
                    if (atributo.lista)
				        for (j=0;j<atributo.lista.length;j++)
					    {

					        if (date2str(atributo.lista[j],vdatefmt)==date2str(atributo.valor,vdatefmt))
						        strAtrib += "<option selected value='" + date2str(atributo.lista[j],vdatefmt) + "'>" + date2str(atributo.lista[j],vdatefmt) + "</option>"
					        else
						        strAtrib += "<option value='" + date2str(atributo.lista[j],vdatefmt)  + "'>" + date2str(atributo.lista[j],vdatefmt) + "</option>"
					        }
						
				    strAtrib += "</select>"
                }
                if (iBloqueado == 1)
                {
                    strAtrib += date2str(atributo.valor,vdatefmt)
                }
			<%end if%>
		}
		break
	case 4: //Chekbox
			<%if bConsulta then%>
				switch(atributo.valor)
					{
					case null:
						strAtrib += ""
						break;
					case true:
						strAtrib += "<%=den(34)%>"
						break;
					case false:
						strAtrib += "<%=den(35)%>"
						break;
					}
			<%else%>
                if (iBloqueado == 0)
                {
				    strAtrib += "<select  style='width:" + wCAtributoT4 + "'  name=" + sNameAtrib + " id =" + sNameAtrib + " onchange='actualizarEstado(1)'>"
				    strAtrib += "<option value='' " + (atributo.valor==null?" selected ":"")  + "></option>"
				    strAtrib += "<option value=1 " + (atributo.valor==true?" selected ":"")  + "><%=den(34)%></option>"
				    strAtrib += "<option value=0 "  + (atributo.valor==false?" selected ":"")  + "><%=den(35)%></option>"
				    strAtrib += "</select>"
                }
                if (iBloqueado == 1)
                {
                    switch(atributo.valor)
					{
					case null:
						strAtrib += ""
						break;
					case true:
						strAtrib += "<%=den(34)%>"
						break;
					case false:
						strAtrib += "<%=den(35)%>"
						break;
					}
                }
			<%end if%>

		break
	}

return (strAtrib)
}

/*
''' <summary>
''' Muestra el coste/descuento como una textbox donde se puede insertar su valor
''' </summary>
<param name=costeDescuento>coste/descuento</param>
<param name=costeDescuentoValor>valor del coste/descuento</param>
<param name=aplica>A qu� aplica el coste/descuento</param>
<param name=grupo>Grupo del coste/descuento</param>
<param name=soloLectura>si es de s�lo lectura</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Tiempo m�ximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
*/

function mostrarCosteDescuento(costeDescuento,costeDescuentoValor, aplica, grupo, soloLectura)
{
var j
var strCosteDescuento
strCosteDescuento = ""

nombreGrupo = grupo

var sNameCosteDescuento

    sNameCosteDescuento = "procCosteDesc_" + costeDescuento.paid
    if(grupo)
	    sNameCosteDescuento +=  "_" + grupo
    if (costeDescuentoValor) //hay valor
    	costeDescuento.valor = costeDescuentoValor.valor
	if (costeDescuento.intro==0) //Coste / descuento input normal
		<%if bConsulta then%>
			strCosteDescuento += num2str(costeDescuento.valor,vdecimalfmt,vthousanfmt,vprecisionfmt)
		<%else%>
            if(soloLectura)
                strCosteDescuento += inputNumeroRO(sNameCosteDescuento,wCCosteDescuentoT5,costeDescuento.valor)
            else
                strCosteDescuento += inputNumero(sNameCosteDescuento,wCCosteDescuentoT5,30,costeDescuento.valor,costeDescuento.valorMin,costeDescuento.valorMax,null,"actualizarEstadoCosteDesc(2,"+costeDescuento.ambito+",","<%=JSText(den(71))%>\\n<%=JSText(den(72))%> " + vdecimalfmt + "\\n<%=JSText(den(73))%> " + vthousanfmt,"<%=JSText(den(56))%>\\n<%=JSText(den(37))%> " + costeDescuento.valorMin + "\\n<%=JSText(den(38))%> " + costeDescuento.valorMax)
		<%end if%>
	else   //Coste / descuento lista desplegable
		{
		<%if bConsulta then%>
			strCosteDescuento += num2str(costeDescuento.valor,vdecimalfmt,vthousanfmt,vprecisionfmt)
		<%else%>
            if(soloLectura)
                strCosteDescuento += inputNumeroRO(sNameCosteDescuento,wCCosteDescuentoT5,costeDescuento.valor)
            else
            {
                strCosteDescuento += "<select  style='width:" + wCCosteDescuentoT5 + "'  name=" + sNameCosteDescuento + " id =" + sNameCosteDescuento + " onchange='actualizarEstadoCosteDesc(1,"+costeDescuento.ambito+"," + sNameCosteDescuento + ")'>"
			    strCosteDescuento += "<option value=''></option>"
                if (costeDescuentoValor.lista)
			        for (j=0;j<costeDescuentoValor.lista.length;j++)
				    {
				        if (num2str(costeDescuentoValor.lista[j],vdecimalfmt,vthousanfmt,10)==num2str(costeDescuento.valor,vdecimalfmt,vthousanfmt,10))
					        strCosteDescuento += "<option selected value='" + num2str(costeDescuentoValor.lista[j],vdecimalfmt,vthousanfmt,10)  + "'>" + num2str(costeDescuentoValor.lista[j],vdecimalfmt,vthousanfmt,vprecisionfmt) + "</option>"
				        else
					        strCosteDescuento += "<option value='" + num2str(costeDescuentoValor.lista[j],vdecimalfmt,vthousanfmt,10)  + "'>" + num2str(costeDescuentoValor.lista[j],vdecimalfmt,vthousanfmt,vprecisionfmt) + "</option>"
				    }
					
			    strCosteDescuento += "</select>"
            }
		<%end if%>
		}

    if(costeDescuentoValor)
        strCosteDescuento+="				<input type=hidden name=hImpo" + sNameCosteDescuento + " id=hImpo" + sNameCosteDescuento + " value=" + costeDescuentoValor.importeParcial + ">"
    else
        strCosteDescuento+="				<input type=hidden name=hImpo" + sNameCosteDescuento + " id=hImpo" + sNameCosteDescuento + " value=0>"
    strCosteDescuento+="				<input type=hidden name=hId" + sNameCosteDescuento + " id=hId" + sNameCosteDescuento + " value=" + costeDescuento.paid + ">"
    strCosteDescuento+="				<input type=hidden name=hAplica" + sNameCosteDescuento + " id=hAplica" + sNameCosteDescuento + " value=" + aplica + ">"
    //Campo para controlar el orden en que se va a aplicar el coste / descuento a la hora de calcular los importes
    strCosteDescuento+="				<input type=hidden name=hOrdenAplica" + sNameCosteDescuento + " id=hOrdenAplica" + sNameCosteDescuento + " value=" + numOrdenAplic + ">"
return (strCosteDescuento)
}


function mostrarCosteDescuentoItem(costeDescuento,costeDescuentoValor, aplica, grupo, soloLectura, subasta,iesc)
{
var j,pu
var strCosteDescuento
strCosteDescuento = ""

nombreGrupo = grupo

var sNameCosteDescuento

if(subasta)
    pu=4
else
    pu=1

if (costeDescuentoValor) //hay valor
{
    sNameCosteDescuento = "procCosteDesc_" + costeDescuento.paid
    if(grupo)     sNameCosteDescuento +=  "_" + grupo
	
	costeDescuento.valor = costeDescuentoValor.valor
	
	//DPD Escalados	
	if ((costeDescuento.ambito== 3) )
	{
		if (iesc>=0) 
		{					
			costeDescuento.escalados = costeDescuentoValor.escalados
		}		
	}
}
if(costeDescuentoValor) //hay valor
    strCosteDescuento+="				<input type=hidden name=hImpo" + sNameCosteDescuento + " value=" + costeDescuentoValor.importeParcial + ">"
else
    strCosteDescuento+="				<input type=hidden name=hImpo" + sNameCosteDescuento + " value=0>"
strCosteDescuento+="				<input type=hidden name=hId" + sNameCosteDescuento + " value=" + costeDescuento.paid + ">"
strCosteDescuento+="				<input type=hidden name=hAplica" + sNameCosteDescuento + " value=" + aplica + ">"
//Campo para controlar el orden en que se va a aplicar el coste / descuento a la hora de calcular los importes
strCosteDescuento+="				<input type=hidden name=hOrdenAplica" + sNameCosteDescuento + " value=" + numOrdenAplic + ">"

	if ((costeDescuento.ambito==3))
	{			
		if (costeDescuento.escalados[iesc]) //@@DPD No deber�a ser necesaria la comprobaci�n
			if(iesc>=0) costeDescuento.valor = costeDescuento.escalados[iesc].valorNum
	}


	if (costeDescuento.intro==0) //Coste / descuento input normal
		<%if bConsulta then%>
			strCosteDescuento += num2str(costeDescuento.valor,vdecimalfmt,vthousanfmt,vprecisionfmt)
		<%else%>
            if(soloLectura)
                strCosteDescuento += inputNumeroRO(sNameCosteDescuento,wCCosteDescuentoT5,costeDescuento.valor)
            else
                strCosteDescuento += inputNumeroCosteDescuentoItem(sNameCosteDescuento,wCCosteDescuentoT5,30,costeDescuento.valor,costeDescuento.valorMin,costeDescuento.valorMax,null,"actualizarEstadoCosteDesc(2,"+costeDescuento.ambito+",","<%=JSText(den(71))%>\\n<%=JSText(den(72))%> " + vdecimalfmt + "\\n<%=JSText(den(73))%> " + vthousanfmt,"<%=JSText(den(56))%>\\n<%=JSText(den(37))%> " + costeDescuento.valorMin + "\\n<%=JSText(den(38))%> " + costeDescuento.valorMax, pu)
		<%end if%>
	else   //Coste / descuento lista desplegable
		{
		<%if bConsulta then%>
			strCosteDescuento += num2str(costeDescuento.valor,vdecimalfmt,vthousanfmt,vprecisionfmt)
		<%else%>
            if(soloLectura)
                strCosteDescuento += inputNumeroRO(sNameCosteDescuento,wCCosteDescuentoT5,costeDescuento.valor)
            else
            {
                strCosteDescuento += "<select  style='width:" + wCCosteDescuentoT5 + "'  name=" + sNameCosteDescuento + " id =" + sNameCosteDescuento + " onchange='actualizarEstadoCosteDesc(1,"+costeDescuento.ambito+"," + sNameCosteDescuento + ")' pu=" + pu + ">"
			    strCosteDescuento += "<option value=''></option>"
			    if (costeDescuentoValor.lista)
                    for (j=0;j<costeDescuentoValor.lista.length;j++)
				    {
				        if (num2str(costeDescuentoValor.lista[j],vdecimalfmt,vthousanfmt,10)==num2str(costeDescuento.valor,vdecimalfmt,vthousanfmt,10))
					        strCosteDescuento += "<option selected value='" + num2str(costeDescuentoValor.lista[j],vdecimalfmt,vthousanfmt,10)  + "'>" + num2str(costeDescuentoValor.lista[j],vdecimalfmt,vthousanfmt,vprecisionfmt) + "</option>"
				        else
					        strCosteDescuento += "<option value='" + num2str(costeDescuentoValor.lista[j],vdecimalfmt,vthousanfmt,10)  + "'>" + num2str(costeDescuentoValor.lista[j],vdecimalfmt,vthousanfmt,vprecisionfmt) + "</option>"
				    }
					
			    strCosteDescuento += "</select>"
            }
		<%end if%>
		}

return (strCosteDescuento)
}


function inputNumeroRO (sName, lSize,  dValue, mas, errMsg, errLimites)
{
var s
s = "<span class='numero' style='width:" + lSize + ";'>"  + num2str(dValue,vdecimalfmt,vthousanfmt,100) + "</span>"
return s
}

function inputNumeroCosteDescuentoItem (sName, lSize, lLenMax, dValue, valorMin, valorMax, mas, sFuncValidacion, errMsg, errLimites, pu)
{
var s
if (!sFuncValidacion)
 sFuncValidacion = "validarNumero("
s = "<input " + (mas?mas:"") + " language='javascript' class='numero' style='width:" + lSize + "' type=text name=" + sName + " id =" + sName + " maxlength=" + lLenMax + " onkeypress='return mascaraNumero(this,vdecimalfmt,vthousanfmt,event);' onchange='return " + sFuncValidacion + "this,vdecimalfmt,vthousanfmt,100," + valorMin + "," + valorMax + ",\"" + errMsg + "\",\"" + errLimites + "\")'  value='" + num2str(dValue,vdecimalfmt,vthousanfmt,100) + "', pu=" + pu + " >"
return s
}


</script>


<%
if oProceso.AdminPublica then
'******************************************************************************
'**** FUNCION QUE MUESTRA LOS DATOS DEL SOBRE    ******************************
'******************************************************************************
%>
<script>

    // <summary>
    // acceso a la pantalla de datos de sobre desde el men� lateral
    // </summary>
    // <param name="e">identificador de la pantalla</param>
    // <returns></returns>
    // <remarks>Llamada desde: configadmin.asp/mostrarConfigProceso; Tiempo m�ximo:0,1</remarks>
    // <revision>JVS 16/11/2011</revision>
function datosSobre(e)
{
if (document.getElementById("divApartado").aptdoAnterior)
	eval (document.getElementById("divApartado").aptdoAnterior)

var vRama
vRama = eval("rama" + e)
dibujarApartado(vRama.text)
resize()

// Pone a false los campos booleanos de control de la pantalla de retorno tras rec�lculo
cambiarFlag()
// Pone a true el campo booleano de datosSobre de control de la pantalla de retorno tras rec�lculo
bEstamosEnSobre = true
bEstamos = "4_" + vRama.cod

var vSbr

var i

for (i=0;i<proceso.sobres.length && proceso.sobres[i].cod != vRama.cod ;i++)
	{
	}

vSbr = proceso.sobres[i]

str = ""
str+= "	<table width=100%>"
str+= "		<tr>"
str+= "			<td>" 
str+= "				<table >"
str+= "					<tr>"	
str+= "						<td>" + cajetin('<%=JSText(den(53))%>',date2str(vSbr.fechaApertura,vdatefmt,true),null) + "</td>"
str+= "					</tr>"
str+= "				</table>"
str+= "			</td>"
str+= "		</tr>" 
str+= "		<tr>"
str+= "			<td>" 
str+= "				<table width=100%>"
str+= "					<tr>"
str+= "						<td>" + cajetin('<%=JSText(den(20))%>',vSbr.obs,"width:100%;") + "</td>"
str+= "					</tr>"
str+= "				</table>"
str+= "			</td>"
str+= "		</tr>"
str+= "	</table>"


document.getElementById("divApartadoS").innerHTML=str

desbloquear()
}


</script>
<%end if%>

<%
'******************************************************************************
'**** FUNCION QUE MUESTRA LOS DATOS GENERALES DEL GRUPO   *********************
'******************************************************************************
%>
<script>

    /*
    ''' <summary>
    ''' Obtiene el grupo e invoca a mostrarGrupo
    ''' </summary>
    ''' <param name="codGrp">C�digo de grupo</param>
    ''' <returns></returns>
    ''' <remarks>Llamada desde: cumpoferta.asp/datosGrupo; Tiempo m�ximo:0,1</remarks>
    ''' <revision>JVS 16/11/2011</revision>
    */
    function mostrarGrupoCod(codGrp) {
        var i
        var vGrp

        for (i = 0; i < proceso.grupos.length && proceso.grupos[i].cod != codGrp; i++) {
        }
        vGrp = proceso.grupos[i]
        mostrarGrupo(vGrp)
    }

    /*
    ''' <summary>
    ''' Muestra la pantalla de grupo
    ''' </summary>
    ''' <param name="vGrp">objeto grupo</param>
    ''' <returns>Nada</returns>
    ''' <remarks>Llamada desde: cumpoferta.asp/mostrarGrupoCod; Tiempo m�ximo:0,1</remarks>
    ''' <revision>JVS 16/11/2011</revision>
    */

function mostrarGrupo(vGrp) {
var str
str = ""
str += "<form name=\"frmGrupo\" id=\"frmGrupo\">"
str += "  <input type=\"hidden\" name=\"grupo\" id=\"grupo\" value='" + vGrp.cod + "'>"
str += "</form>"
str+= "	<table width=50%>"
str+= "		<tr>"
str+= "			<td colspan=2>" 
str+= "				<table width=100%>"
str+= "					<tr>"	
str+= "						<td>" + cajetin('<%=JSText(den(16))%>',vGrp.desc,"width:100%;") + "</td>"
str+= "					</tr>"
str+= "				</table>"
str+= "			</td>"
str += "		</tr>"

var bEncontradoGrupo = false

if (proceso.CostesDescTotalGrupo.length) {
    for (i = 0; i < proceso.CostesDescTotalGrupo.length; i++) {
        alcance = proceso.CostesDescTotalGrupo[i].alcance
        if (alcance == '' || alcance == vGrp.cod) {
            bEncontradoGrupo = true
            break
        }
    }
}


if (bEncontradoGrupo) {
    str += "		<tr>"
    str += "			<td>"
    str += "				<table>"
    str += "					<tr>"
    str += "	            		<td>"
    str += cajetin('<%=JSText(den(100))%>', num2str(vGrp.importeBruto, vdecimalfmt, vthousanfmt, vprecisionfmt) + "&nbsp" + proceso.mon.cod, null, null, null, null, "numero negrita fgrande") //JVS Idioma Importe total bruto del grupo
    str += "			            </td>"
    str += "	            		<td>"
    str += cajetin('<%=JSText(den(101))%>', num2str(vGrp.importe, vdecimalfmt, vthousanfmt, vprecisionfmt) + "&nbsp" + proceso.mon.cod, null, null, null, null, "numero negrita fgrande") //JVS Idioma Importe total neto del grupo
    str += "			            </td>"
    str += "					</tr>"
    str += "				</table>"
    str += "			</td>"
    str += "		</tr>"
}
else {
    str += "		<tr>"
    str += "			<td colspan=2>"
    str += "				<table>"
    str += "					<tr>"
    str += "						<td>" + cajetin('<%=JSText(den(61))%>', num2str(vGrp.importeBruto, vdecimalfmt, vthousanfmt, vprecisionfmt) + "&nbsp" + proceso.mon.cod, null, null, null, null, "numero negrita fgrande") + "</td>"
    str += "					</tr>"
    str += "				</table>"
    str += "			</td>"
}

if (vGrp.cfg.dest) {
str+= "		<tr>"
str+= "			<td>" 
str+= "				<table>"
str+= "					<tr>"
str+= "						<td>" + cajetin('<%=JSText(den(9))%>',vGrp.dest.cod + ' - ' + vGrp.dest.den,null,'detalleDestino',vGrp.dest.cod) + "</td>"
str+= "					</tr>"
str+= "				</table>"
str+= "			</td>"
str+= "		</tr>"
}
if (vGrp.cfg.pag)
{
str+= "		<tr>"
str+= "			<td>" 
str+= "				<table>"
str+= "					<tr>"
str+= "						<td colspan = 3>" + cajetin('<%=JSText(den(8))%>',vGrp.pag) + "</td>"
str+= "					</tr>"
str+= "				</table>"
str+= "			</td>"
str+= "		</tr>"
}
if (vGrp.cfg.fecSum)
{
str+= "		<tr>"
str+= "			<td>" 
str+= "				<table>"
str+= "					<tr>"
str+= "						<td>" + cajetin('<%=JSText(den(10))%>',date2str(vGrp.finisum,vdatefmt)) + "</td>"
if (proceso.cfg.mostrarFinSum)
	str+= "						<td>" + cajetin('<%=JSText(den(11))%>',date2str(vGrp.ffinsum,vdatefmt)) + "</td>"
str+= "					</tr>"
str+= "				</table>"
str+= "			</td>"
str+= "		</tr>"
}


if (vGrp.cfg.especs || vGrp.Aespecs)
{
	if (vGrp.esp || vGrp.especs.length>0 || vGrp.Aespecs.length > 0)
		{
		str+= "		<tr>"
		str+= "			<td>" 
		str+= "				<table width=100%>"
		str+= "					<tr>"
		str+= "						<td>" + cajetin('<%=JSText(den(12))%>',mostrarEspecificaciones(vGrp),"width:100%;") + "</td>"
		str+= "					</tr>"
		str+= "				</table>"
		str+= "			</td>"
		str+= "		</tr>"
		}
}


str+= "	</table>"

document.getElementById("divApartadoS").innerHTML=str

}

</script>
<%
'******************************************************************************
'**** FUNCION QUE MUESTRA LOS DATOS GENERALES DE LA OFERTA    *****************
'******************************************************************************

%>
<script>
var importeAnteriorAmbProc = 0 
var importeAnteriorAmbGrupo = 0


var oMonedas
oMonedas = new Array()
var equivActual
/*
''' <summary>
''' No ofertar un proceso
''' </summary>
''' <param name="anyo">anyo de proceso</param>
''' <param name="gmn1">gmn1 de proceso</param>        
''' <param name="cod">cod de proceso</param>
''' <param name="i">identificador de q linea no se oferta para cambiarle mas tarde el aspecto</param>        
''' <remarks>Llamada desde: los a href de divNoOfertar de la pantalla ; Tiempo m�áximo: 0</remarks>*/
function noOfertar(anyo, gmn1, cod, i)
{
window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/noofertar.asp?i=" + i + "&anyo=" + anyo + "&gmn1=" + gmn1 + "&cod=" + cod, "_blank", "width=600,height=325,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no,top=0,left=0")
}

function rellenarMonedas(lista,mon)
{
var i

if (oMonedas.length==0)
	oMonedas = cargarMonedas()

for (i=0;i<oMonedas.length;i++)
	{
	x = lista.options.length
	lista.options[x]=new Option(oMonedas[i].den,oMonedas[i].cod)
	lista.options[x].equiv = oMonedas[i].equiv
	if (oMonedas[i].cod==mon)
		{
		lista.selectedIndex=i
		equivActual=oMonedas[i].equiv
		}
	}
}

function guardarOferta()
{
<%if not bConsulta then%>

	var vOfe
	var i

	vOfe = proceso
	f=document.forms["frmDatosGenerales"]
	vOfe.fecValidez=str2date(f.txtFechaValidez.value,vdatefmt)
	if (vOfe.cfg.cambiarMon==true)
		{
		vOfe.mon.cod = f.lstMonOfe.options[f.lstMonOfe.selectedIndex].value
		vOfe.mon.equiv = f.lstMonOfe.options[f.lstMonOfe.selectedIndex].equiv
		}

	vOfe.obs = f.txtObsGenerales.value

	for (i=0;i<proceso.atribs.length;i++)
		{
		sNameAtrib = "procAtrib_" + proceso.atribs[i].paid
		switch (proceso.atribs[i].tipo)
			{
			case 1: //Entrada de texto
				proceso.atribs[i].valor = eval("f." + sNameAtrib + ".value")
				break
			case 2: //Entrada de nmeros
				proceso.atribs[i].valor = str2num(eval("f." + sNameAtrib + ".value"),vdecimalfmt,vthousanfmt,vprecisionfmt)
				break
			case 3: //Entrada de fechas
				proceso.atribs[i].valor = str2date(eval("f." + sNameAtrib + ".value"),vdatefmt)
				break
			case 4: //Chekbox
				proceso.atribs[i].valor = f.elements[sNameAtrib].value==""?null:f.elements[sNameAtrib].value==1
					
				break
			}
		}

<%end if%>
}

function preCambiarMoneda(bSinConf,bHacerComprobaciones){      
    ClickItemsAccesible(0);
          
    if (bHacerComprobaciones){
        //Comprobar si hay alguna adjudicación anterior para el proveedor
        <%if oProceso.MonedaAdjudicacionesPreviasProve(CiaComp,oOferta.Prove)<>"" then%>
            alert("<%=JSText(den(154))%>");
            for (i=0;i<document.forms["frmDatosGenerales"].elements["lstMonOfe"].options.length;i++){
	            if (document.forms["frmDatosGenerales"].elements["lstMonOfe"].options[i].value == "<%=oOferta.CodMon%>"){
		            document.forms["frmDatosGenerales"].elements["lstMonOfe"].selectedIndex = i
		            break;
		        }
	        }
            ClickItemsAccesible(1);
            return false;
        <%end if%>

        if(!bSinConf){
	        if (confirm("<%=JSText(den(66))%>")==false){
		        for (i=0;i<document.forms["frmDatosGenerales"].elements["lstMonOfe"].options.length;i++){
			        if (document.forms["frmDatosGenerales"].elements["lstMonOfe"].options[i].value == proceso.mon.cod){
				        document.forms["frmDatosGenerales"].elements["lstMonOfe"].selectedIndex = i
				        break;
			        }
		        }
                ClickItemsAccesible(1);
		        return false;
	        }
	    }
    }       

    //Si hay al menos un grupo sin items cargados llama a la carga de ítems y se sale sin hacer el cambio de moneda
    //Desde cargaritems.asp se volverá a llamar a esta función una vez terminado la carga y se volverá a comprobar si los grupos tienen los items cargados
    //Sólo después de tener todos los grupos los ítems cargados se llama al cambio de moneda
    for (k=0;k<proceso.grupos.length;k++){         
        if (!proceso.grupos[k].itemsCargados){   
            if (!wProgreso) dibujarBarra("<%=den(28)%>");    
                               
            proceso.grupos[k].cargarItems(true,"fraOfertaServer","preCambiarMoneda(" + bSinConf + ",false)"); 
            
            if (wProgreso) progreso(((k+1)/proceso.grupos.length)*100);
               
            return;       
        }
    }

    cambiarMoneda(bSinConf);

    ClickItemsAccesible(1);
}

function ClickItemsAccesible (Valor){
    PuedesClicarItems = Valor
}

function cambiarMoneda(bSinConf){
    var i
    var j
    var vGrp
    var oItem
    var sNamePU
    var sNamePUSubasta
    var sNamePU2
    var sNamePU3
    var f
    var equivAnterior	        

    if (document.forms["frmDatosGenerales"]){
	    equivAnterior = equivActual        
      
	    equivActual = oMonedas[document.forms["frmDatosGenerales"].elements["lstMonOfe"].selectedIndex].equiv              
	    proceso.mon.cod = oMonedas[document.forms["frmDatosGenerales"].elements["lstMonOfe"].selectedIndex].cod
	    proceso.mon.equiv = equivActual 
	    cambio = 1/equivAnterior * equivActual
    }
    else
	    cambio = 1
   
    f=document.forms["frmOfertaAEnviar"]

    <%if oProceso.subasta then%>
    for (i=0;i<itemsPuja.length;i++){
	    oItem = itemsPuja[i]
	    sNamePUSubasta = "txtPujaPU_" + oItem.id
	    if (f.elements[sNamePUSubasta])
		    {
		    if (oItem.precio!=null)
			    {
			    oItem.precio = oItem.precio * cambio
			    f.elements[sNamePUSubasta].value = num2str(oItem.precio,vdecimalfmt,vthousanfmt,100)
			    f.elements[sNamePUSubasta].oldValue = str2num(f.elements[sNamePUSubasta].value,vdecimalfmt,vthousanfmt)
			    elemento = f.elements[sNamePUSubasta]
			    strDetOferta = ""
			    strDetOferta += "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * elemento.getAttribute("cant"),vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"
			     //@@DPD Escalados 7
			    document.getElementById("divTotalLinea" + elemento.getAttribute("pu") + "_" + elemento.getAttribute("itemid")).innerHTML = strDetOferta
			    }
		    else
			    f.elements[sNamePUSubasta].value = ""
		    if (cambio!=1)
			    {
			    oItem.ofertaMinima = oItem.ofertaMinima * cambio
			    <%if oProceso.verDetallePujas then%>
				    if(document.getElementById("divPujaOfeMin" + oItem.id))
                    {
					    sSuperada = oItem.proveCod==proceso.prove?"":" superada"
					    document.getElementById("divPujaOfeMin" + oItem.id).innerHTML = "<a class='negrita' href='javascript:void(null)' modo='3'  sGrupo = '" + oItem.grupo + "' lItem = " + oItem.id + " sArt= '" +  oItem.cod + "' sDescr = '" + oItem.descr + "' onclick='verDetallePujas(this)'><b>" + num2str(oItem.ofertaMinima,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b></a>"
				    }
			    <%else%>
				    if(document.getElementById("divPujaOfeMin" + oItem.id))
                    {
					    document.getElementById("divPujaOfeMin" + oItem.id).innerHTML = 	num2str(oItem.ofertaMinima,vdecimalfmt, vthousanfmt, vprecisionfmt) 
				    }
			    <%end if%>
			    }
		    }

	    }
    <%end if%>

    for (i=0;i<proceso.grupos.length;i++){ 
	    vGrp = proceso.grupos[i]
	    if (document.getElementById("divPrecios1_" + vGrp.cod))
		    document.getElementById("divPrecios1_" + vGrp.cod).innerHTML = "<nobr><%=JSText(replace(den(58),":",""))%>&nbsp;" + proceso.mon.cod + "</nobr>"
	    if (document.getElementById("divPrecios2_" + vGrp.cod))
		    document.getElementById("divPrecios2_" + vGrp.cod).innerHTML = "<nobr><%=JSText(replace(den(58),":",""))%>&nbsp;" + proceso.mon.cod + "</nobr>"
	    if (document.getElementById("divPrecios3_" + vGrp.cod))
		    document.getElementById("divPrecios3_" + vGrp.cod).innerHTML = "<nobr><%=JSText(replace(den(58),":",""))%>&nbsp;" + proceso.mon.cod + "</nobr>"
    
        //aplicar el cambio a los costes descuentos de grupo
        aplicarCambioCostesDescuentosGrupo(vGrp,cambio)
        //hacer conversi�n de moneda al importe bruto y al importe neto del grupo
	    vGrp.importe =  vGrp.importe  * cambio
        vGrp.importeBruto =  vGrp.importeBruto  * cambio
       <%if oProceso.subasta then%>
    	    for (j=0;j<itemsPuja.length;j++)
    	    {
		        oItem = itemsPuja[j]
		        if (oItem.precio!=null)
                   //aplicar el cambio a los costes descuentos de item para subasta
                   aplicarCambioCostesDescuentosItem(vGrp,oItem,oItem.precio,cambio)
            }
        <%end if%>
        
	    for (j=0;j<vGrp.items.length;j++){
            //cambios en objetos
		    oItem = vGrp.items[j];		
            if (oItem.precio!=null){
                //aplicar el cambio a los costes descuentos de item
                aplicarCambioCostesDescuentosItem(vGrp,oItem,oItem.precio,cambio);
            }
            if (cambio!=1){
			    if (oItem.oferta!=null) oItem.oferta = oItem.oferta * cambio;

                //Dato Objetivo proceso: Si la moneda es la de proceso y la oferta, NO se ha de usar cambio leido de bbdd, sino q se ha de usar MonedaBBDDCambio
                if (MonedaBBDD==proceso.mon.cod){
                    if (oItem.obj!=null){ 
                        cambioObj = 1/equivAnterior * MonedaBBDDCambio
                        oItem.obj = oItem.obj * cambioObj;
                    }
                } else {
    			    if (oItem.obj!=null) oItem.obj = oItem.obj * cambio;
                }
                if (oItem.precio2!=null) oItem.precio2 = oItem.precio2 * cambio;
                if (oItem.precio3!=null) oItem.precio3 = oItem.precio3 * cambio;			
		    }
            <%if oProceso.subasta then%>            
			    if (cambio!=1) oItem.ofertaMinima = oItem.ofertaMinima * cambio
		    <%end if%>
        
            sNamePU = "txtPU_" + vGrp.cod + "_" + oItem.id
		    sNameCantMaxl = "txtCantMax_" + vGrp.cod + "_" + oItem.id

            //cambios en presentación
		    if (f.elements[sNamePU]){
			    if (f.elements[sNamePU].value!=""){
				    if (oItem.precio!=null){                   
					    f.elements[sNamePU].value = num2str(oItem.precio,vdecimalfmt,vthousanfmt,100)
					    elemento = f.elements[sNamePU]
					    elemento2 = f.elements[sNameCantMaxl]
					    strDetOferta = "<b>" + num2str(oItem.importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
					     // @@DPD Escalados 8
					    document.getElementById("divTotalLinea" + elemento.getAttribute("pu") + "_" + elemento.getAttribute("itemid")).innerHTML = strDetOferta
			        }
				    else
					    f.elements[sNamePU].value = ""
			    }
			    if (cambio!=1){
				    if (oItem.oferta!=null) document.getElementById("divOfertaActual_" + oItem.id).innerText = num2str(oItem.oferta,vdecimalfmt,vthousanfmt,vprecisionfmt)				
				    if (oItem.obj!=null) { 
                        document.getElementById("divObjetivo_" + oItem.id).innerText = num2str(oItem.obj,vdecimalfmt,vthousanfmt,10)				
                    }
			    }
		    }
            if (cambio!=1){
                if (oItem.obj!=null){                        
                    //Objetivo: Previamente lo has cambiado para q al hacer "xx / oferta.cambio * equivobj" te de lo mismo q gs. 
                    //Esto hay q deshacerlo, para q si cambias de moneda, la cifra de partida sea la "esperada" en la moneda "anterior". "Esperada" pq se supone q el cambio es uno y has usado otro. 
                    //Ejemplo: bbdd: 10,6182662494868        Moneda: Libra           Cambio al crear proceso libra: 0.70633         Cambio actual Libra: 1.5 ---> El objetivo es 5 Libras. Aplicar un cambio de 1.5 no da 5, se ha aplicado 0.70633.                
                    if (MonedaBBDD==proceso.mon.cod){    
                        oItem.obj = oItem.obj / cambioObj;
                        oItem.obj = oItem.obj * cambio;
                    }
                }
            }
		    <%if oProceso.subasta then%>            
			    if (cambio!=1){                
				    if (document.getElementById("divOfeMin" + oItem.id)){					
					    <%if oProceso.verDetallePujas then%>
						    sSuperada = oItem.proveCod==proceso.prove?"":" superada"                        
						    document.getElementById("divOfeMin" + oItem.id).innerHTML = "<a class='popUp " + sSuperada + "'  href='javascript:void(null)' modo='3'  sGrupo = '" + oItem.grupo + "' lItem = " + oItem.id + " sArt= '" +  oItem.cod + "' sDescr = '" + oItem.descr + "' onclick='verDetallePujas(this)'>" + num2str(oItem.ofertaMinima,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</a>"
					    <%else%>
						    document.getElementById("divOfeMin" + oItem.id).innerHTML = 	num2str(oItem.ofertaMinima,vdecimalfmt, vthousanfmt, vprecisionfmt) 
					    <%end if%>
				    }
			    }
		    <%end if%>
		    if (cambio!=1){
			    sNamePU2 = "txtPU2_" + vGrp.cod + "_" + oItem.id
			    sNameCantMaxl = "txtCantMax_" + vGrp.cod + "_" + oItem.id
			    if (f.elements[sNamePU2]){				
				    f.elements[sNamePU2].value = num2str(oItem.precio2,vdecimalfmt,vthousanfmt,100)
				    elemento = f.elements[sNamePU2]
				    elemento2 = f.elements[sNameCantMaxl]
				    strDetOferta = ""
				    if (solicitarCantMax){
					    if (str2num(elemento2.value,vdecimalfmt,vthousanfmt,vprecisionfmt)  <= str2num(elemento.getAttribute("cant"),vdecimalfmt,vthousanfmt,vprecisionfmt) && (elemento2.value != "") && (str2num(elemento2.value,vdecimalfmt,vthousanfmt,vprecisionfmt) > 0) )
						    strDetOferta += "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * elemento2.value,vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"
					    else
						    strDetOferta += "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * elemento.getAttribute("cant"),vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"
				    }
				    else{				
					    strDetOferta += "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * elemento.getAttribute("cant"),vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"
				    }
				     //@@DPD Escalados 9
				    document.getElementById("divTotalLinea" + elemento.getAttribute("pu") + "_" + elemento.getAttribute("itemid")).innerHTML = strDetOferta
			    }
			    sNamePU3 = "txtPU3_" + vGrp.cod + "_" + oItem.id
			    sNameCantMaxl = "txtCantMax_" + vGrp.cod + "_" + oItem.id
			    if (f.elements[sNamePU3]){				
				    f.elements[sNamePU3].value = num2str(oItem.precio3,vdecimalfmt,vthousanfmt,100)
				    elemento = f.elements[sNamePU3]
				    elemento2 = f.elements[sNameCantMaxl]
				    strDetOferta = ""
				    if (solicitarCantMax){
					    if (str2num(elemento2.value,vdecimalfmt,vthousanfmt,vprecisionfmt)  <= str2num(elemento.getAttribute("cant"),vdecimalfmt,vthousanfmt,vprecisionfmt) && (elemento2.value != "") && (str2num(elemento2.value,vdecimalfmt,vthousanfmt,vprecisionfmt) > 0) )
						    strDetOferta += "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * elemento2.value,vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"
					    else
						    strDetOferta += "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * elemento.getAttribute("cant"),vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"												
			        }
				    else{
					    strDetOferta += "<b>" + num2str(str2num(elemento.value,vdecimalfmt,vthousanfmt,vprecisionfmt) * elemento.getAttribute("cant"),vdecimalfmt,vthousanfmt,vprecisionfmt)  + "</b>"
				    }
				     //@@DPD Escalados 10 
				    document.getElementById("divTotalLinea" + elemento.getAttribute("pu") + "_" + elemento.getAttribute("itemid")).innerHTML = strDetOferta
			    }
		    }            
	    }                
    }

    //aplicar conversi�n de moneda a los costes/descuentos del proceso
    aplicarCambioCostesDescuentos(cambio)
    //hacer conversi�n de moneda al importe bruto y al importe neto del proceso   
    proceso.importe =  proceso.importe  * cambio
    proceso.importeBruto =  proceso.importeBruto  * cambio

    //Total de oferta para cada grupo
    for (i=0;i<proceso.grupos.length;i++){
        if (document.getElementById("divTotalOferta_" + proceso.grupos[i].cod)){            
            //CargarItems.asp mira estado y si es 1  pone string vacio
            if (proceso.estado>1){
                var sDiv = estadoOferta(proceso.estado) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + (linkRecalculo == 0 ? num2str(proceso.importe, vdecimalfmt, vthousanfmt, vprecisionfmt) : "-") + "&nbsp;" + proceso.mon.cod +
                        "&nbsp;&nbsp;&nbsp;&nbsp;<IMG SRC=../images/masinform.gif  WIDTH=11 HEIGHT=14 name=imgMasInform>";
                if (linkRecalculo == 1)
                    sDiv += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(null)' onclick=\"linkRecalculoOferta()\"><%=JSText(den(34))%></a>";

                document.getElementById("divTotalOferta_" + proceso.grupos[i].cod).innerHTML =sDiv
            }
        }
    }

    //aplicar cambio a los atributos numericos que se aplican al precio.

    f  = document.forms["frmDatosGenerales"];    

    for (i=0;i<proceso.atribs.length;i++){
	    sNameAtrib = "procAtrib_" + proceso.atribs[i].paid
	
	    proceso.atribs[i].valor = str2num(f.elements[sNameAtrib].value,vdecimalfmt,vthousanfmt)
	    if (proceso.atribs[i].operacion=="+" || proceso.atribs[i].operacion=="-"){
		    if (f.elements[sNameAtrib].options)
			    for (m=0;m<f.elements[sNameAtrib].options.length;m++){
				    if (f.elements[sNameAtrib].options[m].value!=""){
					    f.elements[sNameAtrib].options[m].value = parseFloat(f.elements[sNameAtrib].options[m].value) * cambio
					    f.elements[sNameAtrib].options[m].innerText = num2str(f.elements[sNameAtrib].options[m].value,vdecimalfmt,vthousanfmt,vprecisionfmt)
				    }
			    }

		    if (proceso.atribs[i].lista)
			    for (m=0;m<proceso.atribs[i].lista.length;m++)
				    proceso.atribs[i].lista[m] = proceso.atribs[i].lista[m] * cambio

		    if (proceso.atribs[i].valor!=null){
			    proceso.atribs[i].valor = proceso.atribs[i].valor * cambio;
		        f.elements[sNameAtrib].value = parseFloat(proceso.atribs[i].valor);
                f.elements[sNameAtrib].innerText = num2str(f.elements[sNameAtrib].value,vdecimalfmt,vthousanfmt,vprecisionfmt);
		    }						
	    }
    }		

    f=document.forms["frmOfertaAEnviar"]

    for (j =0;j < proceso.grupos.length;j++){
	    for (k=0;k<proceso.grupos[j].atribs.length;k++)
		    if (proceso.grupos[j].atribs[k].operacion=="+" || proceso.grupos[j].atribs[k].operacion=="-"){
			    if (proceso.grupos[j].atribs[k].lista)
				    for (m=0;m<proceso.grupos[j].atribs[k].lista.length;m++)
					    proceso.grupos[j].atribs[k].lista[m] = proceso.grupos[j].atribs[k].lista[m] * cambio
			
			    if (proceso.grupos[j].atribs[k].valor!=null)
				    proceso.grupos[j].atribs[k].valor = proceso.grupos[j].atribs[k].valor * cambio
		    }

	    for (k=0;k<proceso.grupos[j].atribsItems.length;k++)
		    if (proceso.grupos[j].atribsItems[k].operacion=="+" || proceso.grupos[j].atribsItems[k].operacion=="-")
			    for(l=0;l<proceso.grupos[j].items.length;l++){
				    sNameAtrib = "itemAtrib_" + proceso.grupos[j].cod + "_" + proceso.grupos[j].items[l].id + "_" + proceso.grupos[j].atribsItems[k].paid
				    if (f.elements[sNameAtrib].options)
					    for (m=0;m<f.elements[sNameAtrib].options.length;m++){
						    if (f.elements[sNameAtrib].options[m].value!=""){
							    f.elements[sNameAtrib].options[m].value = parseFloat(f.elements[sNameAtrib].options[m].value) * cambio 
							    f.elements[sNameAtrib].options[m].innerText = num2str(f.elements[sNameAtrib].options[m].value,vdecimalfmt,vthousanfmt,vprecisionfmt)
						    }
					    }
				    if (f.elements[sNameAtrib].value!="")
					    f.elements[sNameAtrib].value = num2str(str2num(f.elements[sNameAtrib].value,vdecimalfmt,vthousanfmt) * cambio,vdecimalfmt,vthousanfmt,vprecisionfmt)
			    }
    }

    if(document.forms["frmDatosGenerales"]){
	    gCodMon = oMonedas[document.forms["frmDatosGenerales"].elements["lstMonOfe"].selectedIndex].cod
	    gDenMon = oMonedas[document.forms["frmDatosGenerales"].elements["lstMonOfe"].selectedIndex].den
    }	
	
    proceso.estado=4
      // @@DPD Y si el proceso no tiene importe...?
    if(proceso.importe){
	    document.getElementById("divCabApartado").innerHTML = generaCabecera(tituloActual)
	    <%if oProceso.subasta then%>
        MostrarCabeceraOferta()
        <%end if%>
    }
}


function comprobarFechaCaducada(e,valor){
    hoy = new Date()
    intro=str2date(valor,vdatefmt)
    intro.setHours(hoy.getHours(),hoy.getMinutes(),hoy.getSeconds(),hoy.getMilliseconds())

    if (intro<hoy)	    {
	    alert("<%=JSText(den(82))%> " + date2str(hoy,vdatefmt) )
	    return false
	}

    actualizarEstado(1)	
    return true
}

function mostrarOferta(vOfe)
{
var str
var sInputFechaValidez
var sSelectMonedas
var sObservaciones



<%if bConsulta then%>
	sInputFechaValidez = date2str(vOfe.fecValidez,vdatefmt)
	sSelectMonedas = vOfe.mon.cod
	sObservaciones = vOfe.obs
<%else%>
	sInputFechaValidez = inputFecha("txtFechaValidez",120,vOfe.fecValidez,null,null,"frmDatosGenerales","comprobarFechaCaducada","","<%=JSText(den(68))%>" + vdatefmt + "\\n<%=JSText(den(69))%>")
	sSelectMonedas = "<SELECT name=lstMonOfe id=lstMonOfe onchange='return preCambiarMoneda(false,true)'></select><IMG name=imgAux id=imgAux border=0 SRC='images/shim.gif' width=1 height=16>"
	sObservaciones = "<TEXTAREA cols=80 rows=8 name=txtObsGenerales id=txtObsGenerales onchange='actualizarEstado(1)'>" + vOfe.obs + "</TEXTAREA>"
<%end if%>

str = ""
str+= "<form name=frmDatosGenerales id=frmDatosGenerales>"
str+= "	<table width=100%>"
str+= "		<tr>"
str+= "			<td >" 
str+= "				<table>"
str+= "					<tr>"	
str+= "						<td >" + cajetin('<%=JSText(den(18))%>',sInputFechaValidez,"width:5px;") + "</td>"

if (proceso.cfg.cambiarMon==true)
str+= "						<td>" + cajetin('<%=JSText(den(19))%>',sSelectMonedas) + "</td>"
else
str+= "						<td>" + cajetin('<%=JSText(den(19))%>',proceso.mon.cod + " - " + proceso.mon.den + "<IMG name=imgAux id=imgAux border=0 SRC='images/shim.gif' width=1 height=16>") + "</td>"

str+= "					</tr>"
str+= "				</table>"
str+= "			</td>"
str+= "		</tr>"
str+= "		<tr>"
str+= "			<td>" 
str+= "				<table>"
str+= "					<tr>"	
str+= "						<td>" + cajetin('<%=JSText(den(20))%>',sObservaciones,"width:100%") + "</td>"
str+= "					</tr>"
str+= "				</table>"
str+= "			</td>"
str+= "		</tr>"
if (proceso.sinAtributos==false)
	{
	str+= "		<tr>"
	str+= "			<td>" 
	str+= "				<table width=100%>"
	str+= "					<tr>"
	str+= "						<td>" + cajetin('<%=JSText(den(21))%>',mostrarAtributos(proceso,"frmDatosGenerales",null),"width:100%;") + "</td>"
	str+= "					</tr>"
	str+= "				</table>"
	str+= "			</td>"
	str+= "		</tr>"
	}
str+= "	<table>"
str+= "</form>"


document.getElementById("divApartadoS").innerHTML=str

<%if not bConsulta then%>
if (proceso.cfg.cambiarMon==true)
	rellenarMonedas(document.forms["frmDatosGenerales"].lstMonOfe,vOfe.mon.cod)
<%end if%>
}







/*
''' <summary>
''' Mostrar por pantalla los Costes / Descuentos de �mbito proceso aplicados a precio item y total item
''' </summary>
''' <param name="vObj">Objeto que contiene toda el proceso</param>
''' <param name="iAmbito">�mbito</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:; Tiempo m�ximo: 0</remarks>
cumpoferta.asp
''' <autor>JVS</autor>
*/
function mostrarCostesDescuentosAmbitoProcesoItem(vObj, iAmbito)
{

var strCosteDesc
var i
var j
strCosteDesc = ""
var vClase




var costeDescValor
var operador
var unidad
var importeAnterior = 0
var primeraVez = true

if (vObj.CostesDescPrecItem)
{
    for (i=0;i<vObj.CostesDescPrecItem.length;i++)
	{
        if(vObj.CostesDescPrecItem[i].ambito == iAmbito)
        {
            if (primeraVez)
            {
                    strCosteDesc+="	<table style='width:100%;'>"

                    vClase = "filaImpar"

                    strCosteDesc+="		<tr>"
                    strCosteDesc+="			<td class=sombreado>"
                    strCosteDesc+="			   <%=JSText(den(110))%>" //JVS Idioma Costes/Descuentos de item
                    strCosteDesc+="			</td>"
                    strCosteDesc+="			<td class=sombreado>"
                    strCosteDesc+="			   <%=JSText(den(93))%>" //JVS Idioma �mbito
                    strCosteDesc+="			</td>"
                    strCosteDesc+="			<td class=sombreado>"
                    strCosteDesc+="			   <%=JSText(den(94))%>" //JVS Idioma Importe oferta
                    strCosteDesc+="			</td>"
                    strCosteDesc+="		</tr>"
                    primeraVez = false
            }
                numOrdenAplic+=1
                costeDescValor = obtenerCosteDescValor(vObj.costesDescuentos,vObj.CostesDescPrecItem[i])
                operador = "<b>" + obtenerOperador(vObj.CostesDescPrecItem[i].operacion) + "</b>"
                unidad = "<b>" + obtenerUnidad(vObj.CostesDescPrecItem[i].operacion, gCodMon) + "</b>"
	            strCosteDesc+="		<tr>"
	            strCosteDesc+="			<td width=70% class='" + vClase + "'>"
                strCosteDesc+="		        <table width=100%>"
                strCosteDesc+="			        <td width=70% class='" + vClase + "'>"
	            strCosteDesc+="				    <input type=hidden name=pAtribCod id=pAtribCod value=" + vObj.CostesDescPrecItem[i].cod + ">"
                strCosteDesc+="					<table><tr><td><nobr><a class='popUp" + " " + (vObj.CostesDescPrecItem[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkAtrib(vObj.CostesDescPrecItem[i],iAmbito) + "\">" + (vObj.CostesDescPrecItem[i].obligatorio?"(*)":"") + vObj.CostesDescPrecItem[i].den + "</a></td><td><a class='popUp" + " " + (vObj.CostesDescPrecItem[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkAtrib(vObj.CostesDescPrecItem[i],iAmbito) + "\"><IMG border = 0 SRC='../images/masinform.gif'></a><%=JSText(den(92))%></td></tr></table></nobr>"  //JVS Idioma (Aplica al precio unitario)
                
                strCosteDesc+="			        </td>"
                strCosteDesc+="			        <td class='" + vClase + "'>"
                strCosteDesc += operador + " " + mostrarCosteDescuento (vObj.CostesDescPrecItem[i],obtenerCosteDescValor(vObj.costesDescuentos,vObj.CostesDescPrecItem[i]), 3) + " " + unidad
                strCosteDesc+="			        </td>"
                strCosteDesc+="			    </table>"
                strCosteDesc+="			 </td>"
                strCosteDesc+="			<td class='" + vClase + "'>"
                if(vObj.CostesDescPrecItem[i].alcance=='')
                    strCosteDesc+="			    <%=JSText(den(95))%>"  //JVS idioma Todos los items
                else
                    strCosteDesc+="			    <%=JSText(den(96))%> " + vObj.CostesDescPrecItem[i].alcance  //JVS idioma Grupo
                strCosteDesc+="			</td>"
                strCosteDesc+="			<td class='" + vClase + " importesParciales' style='width=" + wCAtributo + "'>"
                if (costeDescValor.valor && proceso.importe)
                    strCosteDesc+= "   " +             num2str(costeDescValor.importeParcial,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
	            strCosteDesc+="			</td>"
	            strCosteDesc+="		</tr>"
	            if (vClase == "filaImpar")
		            vClase = "filaPar"
	            else
		            vClase = "filaImpar"
        }
    }
}




if (vObj.CostesDescTotalItem)
{
    
    for (i=0;i<vObj.CostesDescTotalItem.length;i++)
	{
        if(vObj.CostesDescTotalItem[i].ambito == iAmbito)
        {
            if (primeraVez)
            {
                    strCosteDesc+="	<table style='width:100%;'>"

                    vClase = "filaImpar"

                    strCosteDesc+="		<tr>"
                    strCosteDesc+="			<td class=sombreado>"
                    strCosteDesc+="			   <%=JSText(den(110))%>" //JVS Idioma Costes/Descuentos de item
                    strCosteDesc+="			</td>"
                    strCosteDesc+="			<td class=sombreado>"
                    strCosteDesc+="			   <%=JSText(den(93))%>" //JVS Idioma �mbito
                    strCosteDesc+="			</td>"
                    strCosteDesc+="			<td class=sombreado>"
                    strCosteDesc+="			   <%=JSText(den(94))%>" //JVS Idioma Importe oferta
                    strCosteDesc+="			</td>"
                    strCosteDesc+="		</tr>"
                    primeraVez = false
            }

                numOrdenAplic+=1
                costeDescValor = obtenerCosteDescValor(vObj.costesDescuentos,vObj.CostesDescTotalItem[i])
                operador = "<b>" + obtenerOperador(vObj.CostesDescTotalItem[i].operacion) + "</b>"
                unidad = "<b>" + obtenerUnidad(vObj.CostesDescTotalItem[i].operacion, gCodMon) + "</b>"

	            strCosteDesc+="		<tr>"
	            strCosteDesc+="			<td width=70% class='" + vClase + "'>"

                strCosteDesc+="		        <table width=100%>"
                strCosteDesc+="			        <td width=70% class='" + vClase + "'>"
	            strCosteDesc+="				    <input type=hidden name=pAtribCod id=pAtribCod value=" + vObj.CostesDescTotalItem[i].cod + ">"
                strCosteDesc+="					<table><tr><td><nobr><a class='popUp" + " " + (vObj.CostesDescTotalItem[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkAtrib(vObj.CostesDescTotalItem[i],iAmbito) + "\">" + (vObj.CostesDescTotalItem[i].obligatorio?"(*)":"") + vObj.CostesDescTotalItem[i].den + "</a></td><td><a class='popUp" + " " + (vObj.CostesDescTotalItem[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkAtrib(vObj.CostesDescTotalItem[i],iAmbito) + "\"><IMG border = 0 SRC='../images/masinform.gif'></a><%=JSText(den(91))%></td></tr></table></nobr>"  //JVS Idioma (Aplica al total de item)
                
                strCosteDesc+="			        </td>"
                strCosteDesc+="			        <td class='" + vClase + "'>"
                strCosteDesc += operador + " " + mostrarCosteDescuento (vObj.CostesDescTotalItem[i],obtenerCosteDescValor(vObj.costesDescuentos,vObj.CostesDescTotalItem[i]), 2) + unidad    
                strCosteDesc+="			        </td>"
                strCosteDesc+="			    </table>"
                strCosteDesc+="			</td>"
                strCosteDesc+="			<td class='" + vClase + "' style='width=" + wCAtributo + "'>"
                if(vObj.CostesDescTotalItem[i].alcance=='')
                    strCosteDesc+="			    <%=JSText(den(95))%>" // Todos los items
                else
                    strCosteDesc+="			    <%=JSText(den(96))%> " + vObj.CostesDescTotalItem[i].alcance // Grupo
                strCosteDesc+="			</td>"
	            strCosteDesc+="			<td class='" + vClase + " importesParciales' style='width=" + wCAtributo + "'>"
                if (costeDescValor.valor && proceso.importe)
                    strCosteDesc+= "   " +             num2str(costeDescValor.importeParcial,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
	            strCosteDesc+="			</td>"
	            strCosteDesc+="		</tr>"
	            if (vClase == "filaImpar")
		            vClase = "filaPar"
	            else
		            vClase = "filaImpar"
                if(costeDescValor.importeParcial)
                    importeAnteriorAmbProc = costeDescValor.importeParcial
        }
    }
    strCosteDesc+="	</table>"
}
return (strCosteDesc)
}



/*
''' <summary>
''' Mostrar por pantalla los Costes / Descuentos de �mbito proceso aplicados a total de grupo
''' </summary>
''' <param name="vObj">Objeto que contiene toda la oferta</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:; Tiempo m�ximo: 0</remarks>
cumpoferta.asp
''' <autor>JVS</autor>
*/
function mostrarCostesDescuentosAmbitoProcesoGrupo(vObj, iAmbito)
{

var strCosteDesc
var i
var j
strCosteDesc = ""
var vClase
var costeDescValor
var operador
var unidad
var primeraVez = true

if (vObj.CostesDescTotalGrupo)
{
    for (i=0;i<vObj.CostesDescTotalGrupo.length;i++)
	{
        if(vObj.CostesDescTotalGrupo[i].ambito == iAmbito)
        {
                if(primeraVez)
                {
                        strCosteDesc+="	<table style='width:100%;'>"

                        vClase = "filaImpar"

                        strCosteDesc+="		<tr>"
                        strCosteDesc+="			<td class=sombreado>"
                        strCosteDesc+="			   <%=JSText(den(111))%>" //JVS Idioma Costes/Descuentos de grupo
                        strCosteDesc+="			</td>"
                        strCosteDesc+="		</tr>"
                        primeraVez = false
                }
                numOrdenAplic+=1
                costeDescValor = obtenerCosteDescValor(vObj.costesDescuentos,vObj.CostesDescTotalGrupo[i])
                operador = "<b>" + obtenerOperador(vObj.CostesDescTotalGrupo[i].operacion) + "</b>"
                unidad = "<b>" + obtenerUnidad(vObj.CostesDescTotalGrupo[i].operacion, gCodMon) + "</b>"

	            strCosteDesc+="		<tr>"
	            strCosteDesc+="			<td width=100% class='" + vClase + "' border=0>"
                strCosteDesc+="		        <table width=100%>"
                strCosteDesc+="			        <td width=70% class='" + vClase + "'>"
                strCosteDesc+="		            <table width=100%>"
                strCosteDesc+="			            <td width=70% class='" + vClase + "'>"
	            strCosteDesc+="				        <input type=hidden name=pAtribCod id=pAtribCod value=" + vObj.CostesDescTotalGrupo[i].cod + ">"
                strCosteDesc+="					    <table><tr><td><nobr><a class='popUp" + " " + (vObj.CostesDescTotalGrupo[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkAtrib(vObj.CostesDescTotalGrupo[i],iAmbito) + "\">" + (vObj.CostesDescTotalGrupo[i].obligatorio?"(*)":"") + vObj.CostesDescTotalGrupo[i].den + "</a></td><td><a class='popUp" + " " + (vObj.CostesDescTotalGrupo[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkAtrib(vObj.CostesDescTotalGrupo[i],iAmbito) + "\"><IMG border = 0 SRC='../images/masinform.gif'></a></td></tr></table></nobr>"
                
                strCosteDesc+="			            </td>"
                strCosteDesc+="			            <td class='" + vClase + "'>"
	            strCosteDesc += operador + " " + mostrarCosteDescuento (vObj.CostesDescTotalGrupo[i],obtenerCosteDescValor(vObj.costesDescuentos,vObj.CostesDescTotalGrupo[i]), 1) + " " + unidad    
                strCosteDesc+="			            </td>"
                strCosteDesc+="		            </table>"
                strCosteDesc+="			        </td>"
                strCosteDesc+="		            <td class='" + vClase + " importesParciales'>"
                if (costeDescValor.valor && proceso.importe && costeDescValor.importeParcial!=null)
                    strCosteDesc+= "   " +             num2str(costeDescValor.importeParcial,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
                strCosteDesc+="		            </td>"    
                strCosteDesc+="			    </table>"
                strCosteDesc+="		   </td>"       
	            strCosteDesc+="		</tr>"
	            if (vClase == "filaImpar")
		            vClase = "filaPar"
	            else
		            vClase = "filaImpar"
        }
    }
    strCosteDesc+="	</table>"
}
return (strCosteDesc)
}


/*
''' <summary>
''' Mostrar por pantalla los Costes / Descuentos de �mbito proceso
''' </summary>
''' <param name="vOfe">Objeto que contiene toda la oferta</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:; Tiempo m�ximo: 0</remarks>
cumpoferta.asp
''' <autor>JVS</autor>
*/
function mostrarCostesDescuentosAmbitoProceso(vObj, iAmbito)
{

var strCosteDesc
var i
var j
strCosteDesc = ""
var vClase
var primeraVez = true



var costeDescValor
var operador
var unidad

importeAnterior = vObj.importeBruto
if (vObj.CostesDescTotalOferta)
{

    for (i=0;i<vObj.CostesDescTotalOferta.length;i++)
	{
        if(vObj.CostesDescTotalOferta[i].ambito == iAmbito)
        {
               if(primeraVez)
                {
                    strCosteDesc+="	<table style='width:100%;'>"

                    vClase = "filaImpar"

                    strCosteDesc+="		<tr>"
                    strCosteDesc+="			<td class=sombreado colspan=2>"
                    strCosteDesc+="			   <%=JSText(den(112))%>" //JVS Idioma Costes/Descuentos de la oferta
                    strCosteDesc+="			</td>"
                    strCosteDesc+="		</tr>"
                    primeraVez = false
                }
                numOrdenAplic+=1
                costeDescValor = obtenerCosteDescValor(vObj.costesDescuentos,vObj.CostesDescTotalOferta[i])
                operador = "<b>" + obtenerOperador(vObj.CostesDescTotalOferta[i].operacion) + "</b>"
                unidad = "<b>" + obtenerUnidad(vObj.CostesDescTotalOferta[i].operacion, gCodMon) + "</b>"   

	            strCosteDesc+="		<tr>"
	            strCosteDesc+="			<td width=70% class='" + vClase + "'>"
                strCosteDesc+="		      <table width=100%>"
                strCosteDesc+="			    <td class='" + vClase + "' width='70%'>"
	            strCosteDesc+="				        <input type=hidden name=pAtribCod id=pAtribCod value=" + vObj.CostesDescTotalOferta[i].cod + ">"
                strCosteDesc+="					    <table><tr><td><nobr><a class='popUp" + " " + (vObj.CostesDescTotalOferta[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkAtrib(vObj.CostesDescTotalOferta[i],iAmbito) + "\">" + (vObj.CostesDescTotalOferta[i].obligatorio?"(*)":"") + vObj.CostesDescTotalOferta[i].den + "</a></td><td><a class='popUp" + " " + (vObj.CostesDescTotalOferta[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkAtrib(vObj.CostesDescTotalOferta[i],iAmbito) + "\"><IMG border = 0 SRC='../images/masinform.gif'></a></td></tr></table></nobr>"
                
	            strCosteDesc+="			    </td>"
	            strCosteDesc+="			    <td class='" + vClase + "'>"
                strCosteDesc += operador + " " + mostrarCosteDescuento (vObj.CostesDescTotalOferta[i],obtenerCosteDescValor(vObj.costesDescuentos,vObj.CostesDescTotalOferta[i]), 0) + " " + unidad    
	            strCosteDesc+="			    </td>"
                strCosteDesc+="			  </table>"
                strCosteDesc+="			</td>"
                strCosteDesc+="			<td class='" + vClase + " importesParciales'>"
                if (costeDescValor.valor && proceso.importe)
                    strCosteDesc+=              num2str(importeAnterior,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon + " " + obtenerOperador(vObj.CostesDescTotalOferta[i].operacion) + " " + num2str(costeDescValor.valor,vdecimalfmt,vthousanfmt,vprecisionfmt)  + " " + obtenerUnidad(vObj.CostesDescTotalOferta[i].operacion, gCodMon) + " = " + num2str(costeDescValor.importeParcial,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
	            strCosteDesc+="			</td>"
	            strCosteDesc+="		</tr>"
	            if (vClase == "filaImpar")
		            vClase = "filaPar"
	            else
		            vClase = "filaImpar"
                if(costeDescValor.importeParcial)
                    importeAnterior = costeDescValor.importeParcial
        }
    }
    strCosteDesc+="	</table>"
}

return (strCosteDesc)

    
}







/*
''' <summary>
''' Mostrar por pantalla los Costes / Descuentos de �mbito grupo
''' </summary>
''' <param name="vObj">Objeto que contiene el grupo</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:; Tiempo m�ximo: 0</remarks>
cumpoferta.asp
''' <autor>JVS</autor>
*/
function mostrarCostesDescuentosItemAmbitoGrupo(vObj, iAmbito)
{
var strCosteDesc
var i
var j
strCosteDesc = ""
var vClase

var costeDescValor
var operador
var unidad
var importeAnterior = 0
var iAmbitoOferta = 1
var bEncontradoGrupo
var pintaCabecera

    pintaCabecera = true
    strCosteDesc+="	<table style='width:100%;'>"
    if (proceso.CostesDescPrecItem)
    {
        if (proceso.CostesDescPrecItem.length)
        {
            for (i=0;i<proceso.CostesDescPrecItem.length;i++)
	        {
                if(proceso.CostesDescPrecItem[i].ambito == iAmbito || proceso.CostesDescPrecItem[i].ambito == iAmbitoOferta)
                {
                     alcance = proceso.CostesDescPrecItem[i].alcance
                     if (alcance == '' || alcance == vObj.cod)
                            bEncontradoGrupo = true
                      else
                            bEncontradoGrupo = false

                    if (bEncontradoGrupo)
                    {
                        if (pintaCabecera)
                        {
                            vClase = "filaImpar"

                            strCosteDesc+="		<tr>"
                            strCosteDesc+="			<td class=sombreado colspan=2>"
                            strCosteDesc+="			   <%=JSText(den(110))%>" //JVS Idioma Costes/Descuentos de item
                            strCosteDesc+="			</td>"
                            strCosteDesc+="		</tr>"
                            pintaCabecera = false
                        }

                        numOrdenAplic+=1
                        if (proceso.CostesDescPrecItem[i].ambito == iAmbitoOferta)
                            costeDescValor = obtenerCosteDescValor(proceso.costesDescuentos,proceso.CostesDescPrecItem[i])
                        else
                            costeDescValor = obtenerCosteDescValor(vObj.costesDescuentos,proceso.CostesDescPrecItem[i])
                        operador = "<b>" + obtenerOperador(proceso.CostesDescPrecItem[i].operacion) + "</b>"
                        unidad = "<b>" + obtenerUnidad(proceso.CostesDescPrecItem[i].operacion, gCodMon) + "</b>"

	                    strCosteDesc+="		<tr>"
	                    strCosteDesc+="			<td width=70% class='" + vClase + "'>"
                        strCosteDesc+="		        <table width=100%>"
                        strCosteDesc+="			        <td width=70% class='" + vClase + "'>"
	                    strCosteDesc+="				<input type=hidden name=pAtribCod id=pAtribCod value=" + proceso.CostesDescPrecItem[i].cod + ">"
                        strCosteDesc+="					<table><tr><td><nobr><a class='popUp" + " " + (proceso.CostesDescPrecItem[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkCosteDesc(proceso.CostesDescPrecItem[i],proceso.CostesDescPrecItem[i].ambito,vObj.id) + "\">" + (proceso.CostesDescPrecItem[i].obligatorio?"(*)":"") + proceso.CostesDescPrecItem[i].den + "</a></td><td><a class='popUp" + " " + (proceso.CostesDescPrecItem[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkCosteDesc(proceso.CostesDescPrecItem[i],proceso.CostesDescPrecItem[i].ambito,vObj.id) + "\"><IMG border = 0 SRC='../images/masinform.gif'></a><%=JSText(den(92))%></td></tr></table></nobr>" //JVS Idioma (Aplica al precio unitario)
	                    strCosteDesc+="			        </td>"
	                    strCosteDesc+="			        <td class='" + vClase + "'>"
                        strCosteDesc += operador + " " + mostrarCosteDescuento (proceso.CostesDescPrecItem[i],costeDescValor, 3, vObj.cod, (proceso.CostesDescPrecItem[i].ambito == iAmbito?false:true)) + " " + unidad    
                        strCosteDesc+="			        </td>"
                        strCosteDesc+="			    </table>"
                        strCosteDesc+="		    </td>"
                        strCosteDesc+="			<td class='" + vClase + " importesParciales'>"
                        if (costeDescValor)
                        {
                            if (costeDescValor.valor && costeDescValor.importeParcial)

                            {
                                strCosteDesc+= "   " +             num2str(costeDescValor.importeParcial,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
                            }
                        }    
	                    strCosteDesc+="			</td>"
	                    strCosteDesc+="		</tr>"
	                    if (vClase == "filaImpar")
		                    vClase = "filaPar"
	                    else
		                    vClase = "filaImpar"
                        if(costeDescValor)
                            importeAnteriorAmbGrupo = costeDescValor.importeParcial



                    }
                }
            }
        }
    }

    if (proceso.CostesDescTotalItem)
    {

        
        for (i=0;i<proceso.CostesDescTotalItem.length;i++)
	    {
            if(proceso.CostesDescTotalItem[i].ambito == iAmbito || proceso.CostesDescTotalItem[i].ambito == iAmbitoOferta)
            {
                 alcance = proceso.CostesDescTotalItem[i].alcance
                 if (alcance == '' || alcance == vObj.cod)
                        bEncontradoGrupo = true
                  else
                        bEncontradoGrupo = false

                if (bEncontradoGrupo)
                {
                        if (pintaCabecera)
                        {
                            vClase = "filaImpar"

                            strCosteDesc+="		<tr>"
                            strCosteDesc+="			<td class=sombreado colspan=2>"
                            strCosteDesc+="			   <%=JSText(den(110))%>" //JVS Idioma Costes/Descuentos de item
                            strCosteDesc+="			</td>"
                            strCosteDesc+="		</tr>"
                            pintaCabecera = false
                        }

                    numOrdenAplic+=1
                    if (proceso.CostesDescTotalItem[i].ambito == iAmbitoOferta)
                        costeDescValor = obtenerCosteDescValor(proceso.costesDescuentos,proceso.CostesDescTotalItem[i])
                    else
                        costeDescValor = obtenerCosteDescValor(vObj.costesDescuentos,proceso.CostesDescTotalItem[i])
                    operador = "<b>" + obtenerOperador(proceso.CostesDescTotalItem[i].operacion) + "</b>"
                    unidad = "<b>" + obtenerUnidad(proceso.CostesDescTotalItem[i].operacion, gCodMon) + "</b>"

	                strCosteDesc+="		<tr>"
	                strCosteDesc+="			<td width=70% class='" + vClase + "'>"
                    strCosteDesc+="		        <table width=100%>"
                    strCosteDesc+="			        <td width=70% class='" + vClase + "'>"
	                strCosteDesc+="				<input type=hidden name=pAtribCod id=pAtribCod value=" + proceso.CostesDescTotalItem[i].cod + ">"
                    strCosteDesc+="					<table><tr><td><nobr><a class='popUp" + " " + (proceso.CostesDescTotalItem[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkCosteDesc(proceso.CostesDescTotalItem[i],proceso.CostesDescTotalItem[i].ambito,vObj.id) + "\">" + (proceso.CostesDescTotalItem[i].obligatorio?"(*)":"") + proceso.CostesDescTotalItem[i].den + "</a></td><td><a class='popUp" + " " + (proceso.CostesDescTotalItem[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkCosteDesc(proceso.CostesDescTotalItem[i],proceso.CostesDescTotalItem[i].ambito,vObj.id) + "\"><IMG border = 0 SRC='../images/masinform.gif'></a><%=JSText(den(91))%></td></tr></table></nobr>" //JVS Idioma (Aplica al total del item)

	                strCosteDesc+="			        </td>"
	                strCosteDesc+="			        <td class='" + vClase + "'>"
                    strCosteDesc += operador + " " + mostrarCosteDescuento (proceso.CostesDescTotalItem[i],costeDescValor, 2, vObj.cod, (proceso.CostesDescTotalItem[i].ambito == iAmbito?false:true)) + " " + unidad    
                    strCosteDesc+="			        </td>"
                    strCosteDesc+="			    </table>"
                strCosteDesc+="			    </td>"

                    strCosteDesc+="			<td class='" + vClase + " importesParciales'>"
                    if (costeDescValor.valor && costeDescValor.importeParcial)
                    {
                           strCosteDesc+= "   " +             num2str(costeDescValor.importeParcial,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
                     }
                    strCosteDesc+="			</td>"
    	                strCosteDesc+="		</tr>"
	                if (vClase == "filaImpar")
		                vClase = "filaPar"
	                else
		                vClase = "filaImpar"
                    if(costeDescValor.importeParcial)
                        importeAnteriorAmbGrupo = costeDescValor.importeParcial
                }
            }
        }
    }
    strCosteDesc+="	</table>"



return (strCosteDesc)
}



/*
''' <summary>
''' Mostrar por pantalla los Costes / Descuentos de �mbito grupo
''' </summary>
''' <param name="vObj">Objeto que contiene el grupo</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:; Tiempo m�ximo: 0</remarks>
cumpoferta.asp
''' <autor>JVS</autor>
*/
function mostrarCostesDescuentosGrupoAmbitoGrupo(vObj, iAmbito)
{

var strCosteDesc
var i
var j
strCosteDesc = ""
var vClase
var pintaCabecera

pintaCabecera = true

var costeDescValor
var operador
var unidad
var importeAnterior = 0
var iAmbitoOferta = 1
var bEncontradoGrupo


if (proceso.CostesDescTotalGrupo)
{
    if (proceso.CostesDescTotalGrupo.length)
    {
        importeAnterior = vObj.importeBruto
        for (i=0;i<proceso.CostesDescTotalGrupo.length;i++)
	    {
            if(proceso.CostesDescTotalGrupo[i].ambito == iAmbito || proceso.CostesDescTotalGrupo[i].ambito == iAmbitoOferta)
                {
                    alcance = proceso.CostesDescTotalGrupo[i].alcance
                     if (alcance == '' || alcance == vObj.cod)
                            bEncontradoGrupo = true
                      else
                            bEncontradoGrupo = false

                if (bEncontradoGrupo)
                {
                    if (pintaCabecera)
                    {
                        strCosteDesc+="	<table style='width:100%;'>"

                        vClase = "filaImpar"

                        strCosteDesc+="		<tr>"
                        strCosteDesc+="			<td class=sombreado colspan=2>"
                        strCosteDesc+="			   <%=JSText(den(111))%>" //JVS Idioma Costes/Descuentos de grupo
                        strCosteDesc+="			</td>"
                        strCosteDesc+="		</tr>"
                        pintaCabecera = false
                    }
                    numOrdenAplic+=1
                    if (proceso.CostesDescTotalGrupo[i].ambito == iAmbitoOferta)
                        costeDescValor = obtenerCosteDescValor(proceso.costesDescuentos,proceso.CostesDescTotalGrupo[i])
                    else
                        costeDescValor = obtenerCosteDescValor(vObj.costesDescuentos,proceso.CostesDescTotalGrupo[i])
                    operador = "<b>" + obtenerOperador(proceso.CostesDescTotalGrupo[i].operacion) + "</b>"
                    unidad = "<b>" + obtenerUnidad(proceso.CostesDescTotalGrupo[i].operacion, gCodMon) + "</b>"

	                strCosteDesc+="		<tr>"
	                strCosteDesc+="			<td width=70% class='" + vClase + "'>"
                    strCosteDesc+="		        <table width=100%>"
                     strCosteDesc+="	            <td width=70% class='" + vClase + "'>"
	                strCosteDesc+="			    	<input type=hidden name=pAtribCod id=pAtribCod value=" + proceso.CostesDescTotalGrupo[i].cod + ">"
                    strCosteDesc+="				    	<table><tr><td><nobr><a class='popUp" + " " + (proceso.CostesDescTotalGrupo[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkCosteDesc(proceso.CostesDescTotalGrupo[i],proceso.CostesDescTotalGrupo[i].ambito,vObj.id) + "\">" + (proceso.CostesDescTotalGrupo[i].obligatorio?"(*)":"") + proceso.CostesDescTotalGrupo[i].den + "</a></td><td><a class='popUp" + " " + (proceso.CostesDescTotalGrupo[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkCosteDesc(proceso.CostesDescTotalGrupo[i],proceso.CostesDescTotalGrupo[i].ambito,vObj.id) + "\"><IMG border = 0 SRC='../images/masinform.gif'></a></td></tr></table></nobr>"
	                strCosteDesc+="			        </td>"
	                strCosteDesc+="			        <td class='" + vClase + "'>"
                    strCosteDesc += operador + " " + mostrarCosteDescuento (proceso.CostesDescTotalGrupo[i],costeDescValor, 1, vObj.cod, (proceso.CostesDescTotalGrupo[i].ambito == iAmbito?false:true)) + " " + unidad    
                    strCosteDesc+="			        </td>"
                    strCosteDesc+="			    </table>"
                    strCosteDesc+="			</td>"
                    strCosteDesc+="			<td class='" + vClase + " importesParciales'>"
                    for (k=0;k<proceso.ImportesParcialesGrupo.length;k++)
                    {
                        if (proceso.ImportesParcialesGrupo[k].grupo == vObj.cod && proceso.CostesDescTotalGrupo[i].paid == proceso.ImportesParcialesGrupo[k].costeDesc)
                        {
                            importeParcialCosteDesc = proceso.ImportesParcialesGrupo[k].importeParcial
                            break
                        }
                    }

                    //if (costeDescValor.valor && proceso.importe)
					// DPD Quito la condic�n de proceso.importe
					if (costeDescValor.valor)
                    {
                        if(proceso.CostesDescTotalGrupo[i].ambito == iAmbitoOferta)
                        {
                            if(importeAnterior)
							{
								if(importeParcialCosteDesc)
									strCosteDesc+=              num2str(importeAnterior,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon + " " + obtenerOperador(proceso.CostesDescTotalGrupo[i].operacion) + " " + num2str(costeDescValor.valor,vdecimalfmt,vthousanfmt,vprecisionfmt)  + " " + obtenerUnidad(proceso.CostesDescTotalGrupo[i].operacion, gCodMon) + " = " + num2str(importeParcialCosteDesc,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
							}
                            else
							{
                                if(importeParcialCosteDesc)
                                    strCosteDesc+= "   " +             num2str(importeParcialCosteDesc,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon

							}
                        }
                        else

						{
                            if(importeAnterior)
							{
								if(costeDescValor.importeParcial)
									strCosteDesc+=              num2str(importeAnterior,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon + " " + obtenerOperador(proceso.CostesDescTotalGrupo[i].operacion) + " " + num2str(costeDescValor.valor,vdecimalfmt,vthousanfmt,vprecisionfmt)  + " " + obtenerUnidad(proceso.CostesDescTotalGrupo[i].operacion, gCodMon) + " = " + num2str(costeDescValor.importeParcial,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
                            }
							else
							{
                                if(costeDescValor.importeParcial)
                                    strCosteDesc+= "   " +             num2str(costeDescValor.importeParcial,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
							}
                        }

                    }    
	                strCosteDesc+="			</td>"
	                strCosteDesc+="		</tr>"
	                if (vClase == "filaImpar")
		                vClase = "filaPar"
	                else
		                vClase = "filaImpar"
                importeAnterior = costeDescValor.importeParcial

                }
            }
        }
        strCosteDesc+="	</table>"
    }
}
return (strCosteDesc)
}


function obtenerImpParcialAnterior(codGrupo,costeDescId)
{
    var i, impAnterior
    for(i=0;i<proceso.ImportesParcialesGrupo.length;i++)
        if(proceso.ImportesParcialesGrupo[i].costeDesc == costeDescId && proceso.ImportesParcialesGrupo[i].grupo == codGrupo)
        {
            impAnterior = proceso.ImportesParcialesGrupo[i].importeParcial
            break
        }
    return impAnterior
}



</script>



<%
'******************************************************************************
'**** FUNCION QUE MUESTRA LOS ATRIBUTOS DE UN GRUPO     *****************
'******************************************************************************

%>
<script>

// <summary>
// acceso a la pantalla de datos de atributos desde el men� lateral
// </summary>
// <param name="e">identificador de la pantalla</param>
// <returns></returns>
// <remarks>Llamada desde: config.asp/mostrarConfigProceso; Tiempo m�ximo:0,1</remarks>
// <revision>JVS 16/11/2011</revision>
function introducirAtributos(e)
{
var vRama
if (document.getElementById("divApartado").aptdoAnterior)
	eval (document.getElementById("divApartado").aptdoAnterior)

vRama = eval("rama" + e)

dibujarApartado(vRama.text)
resize()

// Pone a false los campos booleanos de control de la pantalla de retorno tras rec�lculo
cambiarFlag()
// Pone a true el campo booleano de introducirAtributos de control de la pantalla de retorno tras rec�lculo
bEstamosEnAtributos = true
bEstamos="5_" & vRama.cod

var vGrp

var i
for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != vRama.cod;i++)
	{
	}
vGrp = proceso.grupos[i]

document.getElementById("divApartado").aptdoAnterior="guardarAtributosGrupo('" + vGrp.cod + "')"
document.getElementById("divApartado").aptdoActual = "mostrarAtributosGrupoCod('" + vGrp.cod + "')"
vGrp.cargarAtribs("fraOfertaServer")


}

function guardarAtributosGrupo(cod)
{
<%if not bConsulta then%>
	var vGrp

	var i
	for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != cod;i++)
		{
		}
	vGrp = proceso.grupos[i]
    if (vGrp.Bloqueado == 0)
    {
	f = document.forms["frmAtributosGrupo" + vGrp.cod]
	for (i=0;i<vGrp.atribs.length;i++)
		{
		    sNameAtrib = "grupoAtrib_" + vGrp.cod + "_" + vGrp.atribs[i].paid
		
		    switch (vGrp.atribs[i].tipo)
			    {
			    case 1: //Entrada de texto
				    vGrp.atribs[i].valor = eval("f." + sNameAtrib + ".value")
				    break
			    case 2: //Entrada de n�meros
				    vGrp.atribs[i].valor = str2num(eval("f." + sNameAtrib + ".value"),vdecimalfmt,vthousanfmt,vprecisionfmt)
				    break
			    case 3: //Entrada de fechas
				    vGrp.atribs[i].valor = str2date(eval("f." + sNameAtrib + ".value"),vdatefmt)
				    break
			    case 4: //Chekbox
			        vGrp.atribs[i].valor = f.elements[sNameAtrib].value == "" ? null : f.elements[sNameAtrib].value == 1
				    break
			    }
		

		    }
        }
<%end if%>

}

/*
''' <summary>
''' Obtiene el grupo e invoca a mostrarAtributosGrupo
''' </summary>
''' <param name="codGrp">C�digo de grupo</param>
''' <returns></returns>
''' <remarks>Llamada desde: cumpoferta.asp/introducirAtributos; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 16/11/2011</revision>
*/
function mostrarAtributosGrupoCod(codGrp)
{
    var i
    var vGrp

    for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != codGrp;i++)
		{
		}
    vGrp = proceso.grupos[i]
    mostrarAtributosGrupo(vGrp)
}

/*
''' <summary>
''' Muestra la pantalla de atributos de grupo
''' </summary>
''' <param name="vGrp">objeto grupo</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cumpoferta.asp/mostrarAtributosGrupoCod; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 16/11/2011</revision>
*/
function mostrarAtributosGrupo(vGrp)
{
var str
var scroll

scroll = document.getElementById("divScroll" + vGrp.cod).scrollLeft

str = ""
str+= "<form name=frmAtributosGrupo" + vGrp.cod + " id=frmAtributosGrupo" + vGrp.cod + ">"
str+= "  <input type=\"hidden\" name=\"grupo\" id=\"grupo\" value='" + vGrp.cod + "'>"
str+= "	<table width=100%>"
str+= "		<tr>"
str+= "			<td width=100%>" 
str+= "			" + mostrarAtributos(vGrp,"frmAtributosGrupo" + vGrp.cod, vGrp)
str+= "			</td>"
str+= "		</tr>"
str+= "	<table>"
str+= "</form>"


document.getElementById("divApartadoS").innerHTML=str
document.getElementById("divScroll" + vGrp.cod).scrollLeft = scroll
}



</script>


<%
'************************************************************************
'**** FUNCION QUE MUESTRA LOS COSTES / DESCUENTOS DE �MBITO PROCESO *****
'************************************************************************

%>
<script>

var linkRecalculo = 0
var linkRecalculoItem = 0
var linkRecalculoPrevio = 2
var linkRecalculoPendiente = 0
var numOrdenAplic = 0
var recalcularTodosGrupos = 0
var coordx, coordy
/*
''' <summary>
''' Permite introducir los Costes / Descuentos de �mbito proceso
''' </summary>
''' <param name="e"></param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:; Tiempo m�ximo: 0</remarks>
config.asp
''' <autor>JVS</autor>
''' <revision>JVS 16/11/2011</revision>
*/

function introducirCostesDesc(e)
{

var vRama

if (document.getElementById("divApartado").aptdoAnterior)
	eval (document.getElementById("divApartado").aptdoAnterior)

vRama = eval("rama" + e)



dibujarApartado(vRama.text)
resize()

// Pone a false los campos booleanos de control de la pantalla de retorno tras rec�lculo
cambiarFlag()
// Pone a true el campo booleano de introducirCostesDesc de control de la pantalla de retorno tras rec�lculo
bEstamosEnCostesDesc = true
bEstamos="6"

document.getElementById("divApartado").aptdoAnterior="guardarCostesDescProceso()"
document.getElementById("divApartado").aptdoActual = "mostrarFormCostesDescuentosAmbitoProceso(proceso,1)"

proceso.mostrarCostesDesc("fraOfertaServer")
}


/*
''' <summary>
''' Permite introducir los Costes / Descuentos de �mbito grupo
''' </summary>
''' <param name="e"></param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:; Tiempo m�ximo: 0</remarks>
config.asp
''' <autor>JVS</autor>
''' <revision>JVS 16/11/2011</revision>
*/

function introducirCostesDescGrupo(e)
{
var vRama

if (document.getElementById("divApartado").aptdoAnterior)
	eval (document.getElementById("divApartado").aptdoAnterior)

vRama = eval("rama" + e)

dibujarApartado(vRama.text)
resize()

// Pone a false los campos booleanos de control de la pantalla de retorno tras rec�lculo
cambiarFlag()
// Pone a true el campo booleano de introducirCostesDescGrupo de control de la pantalla de retorno tras rec�lculo
bEstamosEnCostesDescGrupo = true
bEstamos="7_" + vRama.cod

var vGrp

var i
for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != vRama.cod;i++)
	{
	}
vGrp = proceso.grupos[i]

document.getElementById("divApartado").aptdoAnterior="guardarCostesDescGrupo('" + vGrp.cod + "')"
document.getElementById("divApartado").aptdoActual = "mostrarFormCostesDescuentosAmbitoGrupo('" + vGrp.cod + "',2)"
vGrp.mostrarCostesDesc("fraOfertaServer")
}

function mostrarWCostesDescuentos(sNombreCampo)
{
    var campo = document.forms["frmOfertaAEnviar"].elements[sNombreCampo].name
	winCostesDesc = window.open ("detalleCostesDescItem.asp?nombreCampo=" + campo, "Costes/Descuentos de Item", "top=100,left=150,width=400,height=300,location=no,menubar=no,resizable=yes,scrollbars=no,toolbar=no")
	
}


/*
''' <summary>
''' Permite guardar los Costes / Descuentos de �mbito proceso desde la pantalla
''' </summary>
''' <param name=""></param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:introducirCostesDesc; Tiempo m�ximo: 0</remarks>
cumpoferta.asp
''' <autor>JVS</autor>
*/


function guardarCostesDescProceso()
{
<%if not bConsulta then%>

	var i
    var sNameCosteDesc
	var costeDescValor
    var f

    var iAmbito

    iAmbito = proceso.proceso?2:1

    f = document.forms["frmCostesDescuentos"]

    if (proceso.CostesDescTotalOferta.length > 0)
    {
	    for (i=0;i<proceso.CostesDescTotalOferta.length;i++)
	    {
            if(proceso.CostesDescTotalOferta[i].ambito == iAmbito)
	        {
                costeDescValor = obtenerCosteDescValor(proceso.costesDescuentos,proceso.CostesDescTotalOferta[i])

		        sNameCosteDesc = "procCosteDesc_" + proceso.CostesDescTotalOferta[i].paid
		
		        costeDescValor.valor = str2num(eval("f." + sNameCosteDesc + ".value"),vdecimalfmt,vthousanfmt,vprecisionfmt)
                costeDescValor.ImporteParcial = str2num(eval("f.hImpo" + sNameCosteDesc + ".value"),vdecimalfmt,vthousanfmt,vprecisionfmt)
	        }
        }
    }
<%end if%>

}


/*
''' <summary>
''' Permite guardar los Costes / Descuentos de �mbito grupo desde la pantalla
''' </summary>
''' <param name=""></param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:introducirCostesDescGrupo; Tiempo m�ximo: 0</remarks>
cumpoferta.asp
''' <autor>JVS</autor>
*/


function guardarCostesDescGrupo(grupo)
{
<%if not bConsulta then%>

	var i
    var sNameCosteDesc
	var costeDescValor
    var f

    var iAmbito

    iAmbito = 2

    var vGrp

    for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != grupo;i++)
	    {
	    }
    vGrp = proceso.grupos[i]

    f = document.forms["frmCostesDescuentosGrupo"]

    if (proceso.CostesDescPrecItem.length > 0)
    {
	    for (i=0;i<proceso.CostesDescPrecItem.length;i++)
	    {
            alcance = proceso.CostesDescPrecItem[i].alcance
            if (alcance == '' || alcance == grupo)
                bEncontradoGrupo = true
            else
                bEncontradoGrupo = false

            if (bEncontradoGrupo)
            {
                if(proceso.CostesDescPrecItem[i].ambito == iAmbito)
	            {
                    costeDescValor = obtenerCosteDescValor(vGrp.costesDescuentos,proceso.CostesDescPrecItem[i])
                    if(grupo)
		                sNameCosteDesc = "procCosteDesc_" + proceso.CostesDescPrecItem[i].paid + "_" + grupo
               
		            costeDescValor.valor = str2num(eval("f." + sNameCosteDesc + ".value"),vdecimalfmt,vthousanfmt,vprecisionfmt)
                    costeDescValor.ImporteParcial = str2num(eval("f.hImpo" + sNameCosteDesc + ".value"),vdecimalfmt,vthousanfmt,vprecisionfmt)
	            }
            }
        }
    }


    if (proceso.CostesDescTotalItem.length > 0)
    {
	    for (i=0;i<proceso.CostesDescTotalItem.length;i++)
	    {
            alcance = proceso.CostesDescTotalItem[i].alcance
            if (alcance == '' || alcance == grupo)
                bEncontradoGrupo = true
            else
                bEncontradoGrupo = false

            if (bEncontradoGrupo)
            {

                if(proceso.CostesDescTotalItem[i].ambito == iAmbito)
	            {
                    costeDescValor = obtenerCosteDescValor(vGrp.costesDescuentos,proceso.CostesDescTotalItem[i])
                
                    if(grupo)
		                sNameCosteDesc = "procCosteDesc_" + proceso.CostesDescTotalItem[i].paid + "_" + grupo
               
		            costeDescValor.valor = str2num(eval("f." + sNameCosteDesc + ".value"),vdecimalfmt,vthousanfmt,vprecisionfmt)
                    costeDescValor.ImporteParcial = str2num(eval("f.hImpo" + sNameCosteDesc + ".value"),vdecimalfmt,vthousanfmt,vprecisionfmt)
	            }
            }
        }
    }



    
    if (proceso.CostesDescTotalGrupo.length > 0)
    {
	    for (i=0;i<proceso.CostesDescTotalGrupo.length;i++)
	    {
            alcance = proceso.CostesDescTotalGrupo[i].alcance
            if (alcance == '' || alcance == grupo)
                bEncontradoGrupo = true
            else
                bEncontradoGrupo = false

            if (bEncontradoGrupo)
            {

                if(proceso.CostesDescTotalGrupo[i].ambito == iAmbito)
	            {
                    costeDescValor = obtenerCosteDescValor(vGrp.costesDescuentos,proceso.CostesDescTotalGrupo[i])

                    if(grupo)
		                sNameCosteDesc = "procCosteDesc_" + proceso.CostesDescTotalGrupo[i].paid + "_" + grupo
               
		            costeDescValor.valor = str2num(eval("f." + sNameCosteDesc + ".value"),vdecimalfmt,vthousanfmt,vprecisionfmt)
                    costeDescValor.ImporteParcial = str2num(eval("f.hImpo" + sNameCosteDesc + ".value"),vdecimalfmt,vthousanfmt,vprecisionfmt)
	            }
            }
        }
    }
<%end if%>

}




/*
''' <summary>
''' Permite mostrar la pantalla de los Costes / Descuentos de �mbito proceso
''' </summary>
''' <param name="vOfe">Objeto que contiene toda la oferta</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:introducirCostesDesc; Tiempo m�ximo: 0</remarks>
cumpoferta.asp
''' <autor>JVS</autor>
*/

function mostrarFormCostesDescuentosAmbitoProceso(vOfe,iAmbito)
{
var str
str = ""
str+= "<form name=frmCostesDescuentos id=frmCostesDescuentos>"
if (proceso.sinCostesDescuentos==false)
{
    str+= "<table width=100%>"
    str += "	<tr height=24px>"
    str += "		<td>"
    str += "<span style='text-align:left'><%=JSText(den(97))%>:</span>" //JVS Idioma Introduzca los valores de costes/descuentos que le indicamos a continuaci�n
    str += "		</td>"
    str += "	</tr>"
	str+= "		<tr>"
	str+= "			<td>" 
	str+= "				<table width=100%>"
	str+= "					<tr>"
    str+= "						    <td>" + mostrarCostesDescuentosAmbitoProcesoItem(vOfe,iAmbito) + "</td>"
	str+= "					</tr>"
	str+= "				</table>"
	str+= "			</td>"
	str+= "		</tr>"

	str+= "		<tr>"
	str+= "			<td>" 
	str+= "				<table width=100%>"
	str+= "					<tr>"
    str+= "						    <td>" + mostrarCostesDescuentosAmbitoProcesoGrupo(vOfe,iAmbito) + "</td>"
	str+= "					</tr>"
	str+= "				</table>"
	str+= "			</td>"
	str+= "		</tr>"
    if (iAmbito==1)
    {
  	    str+= "		<tr>"   
	    str+= "			<td>" 
	    str+= "				<table width=100%>"
	    str+= "					<tr>"
        str+= "						    <td>" + mostrarCostesDescuentosAmbitoProceso(vOfe,iAmbito) + "</td>"
	    str+= "					</tr>"
	    str+= "				</table>"
	    str+= "			</td>"
	    str+= "		</tr>"
    }
    str+= "	<table>"
}

str+= "</form>"

numOrdenAplic=0

document.getElementById("divApartadoS").innerHTML=str

}

/*
''' <summary>
''' Permite mostrar la pantalla de los Costes / Descuentos de �mbito grupo
''' </summary>
''' <param name="codGr">C�digo de grupo</param>
''' <param name="iAmbito">�mbito</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:introducirCostesDescGrupo; Tiempo m�ximo: 0</remarks>
cumpoferta.asp
''' <autor>JVS</autor>
''' <revision>JVS 16/11/2011</revision>
*/
function mostrarFormCostesDescuentosAmbitoGrupo(codGr,iAmbito)
{

var str

var i
var vGr

for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != codGr;i++)
	{
	}
vGr = proceso.grupos[i]
str = ""
str+= "<form name=frmCostesDescuentosGrupo id=frmCostesDescuentosGrupo>"
str+= "  <input type=\"hidden\" name=\"grupo\" id=\"grupo\" value='" + vGr.cod + "'>"

    {
    str += "		<td>"
    str += "<span style='text-align:left'><%=JSText(den(97))%>:</span>"  //JVS Idioma Introduzca los valores de costes/descuentos que le indicamos a continuaci�n
    str += "		</td>"

	str+= "		<tr>"
	str+= "			<td>" 
    str+="<div style=\"BORDER:1px solid #000000;FONT-WEIGHT: bold;WIDTH:155px;PADDING:5px\">	<%=JSText(den(102))%>: " + num2str(vGr.importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon + "</div>" //JVS Idioma Importe grupo
	str+= "				<table width=100%>"
	str+= "					<tr>"
	str+= "						    <td>" + mostrarCostesDescuentosItemAmbitoGrupo(vGr,iAmbito) + "</td>"
	str+= "					</tr>"
	str+= "				</table>"
	str+= "			</td>"
	str+= "		</tr>"
    str+= "		<tr>"
	str+= "			<td>" 
	str+= "				<table width=100%>"
	str+= "					<tr>"
    str+= "						    <td>" + mostrarCostesDescuentosGrupoAmbitoGrupo(vGr,iAmbito) + "</td>"
	str+= "					</tr>"
	str+= "				</table>"
	str+= "			</td>"
	str+= "		</tr>"
    }
str+= "	<table>"
str+= "</form>"

numOrdenAplic=0
document.getElementById("divApartadoS").innerHTML=str

}



/*
''' <summary>
''' Mostrar por pantalla los Costes / Descuentos aplicados a item (precio y total)
''' </summary>
''' <param name="vGr">Objeto que contiene el grupo</param>
''' <param name="vIt">Objeto que contiene el item</param>
''' <param name="nombre">Nombre del campo de precio unitario</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:; Tiempo m�ximo: 0</remarks>
cumpoferta.asp
''' <author>JVS</author>
*/

function mostrarFormCostesDescuentosItem(vGr, vIt, nombre,divModificable,iesc,idEsc)
{
var str
str = ""
str+= "<form name=frmCostesDescuentosItem id=frmCostesDescuentosItem>"
str+= "  <input type=\"hidden\" name=\"grupo\" id=\"grupo\" value='" + vGr + "'>"
str+= "  <input type=\"hidden\" name=\"item\" id=\"item\" value=" + vIt + ">"
str+= "  <input type=\"hidden\" name=\"esc\" id=\"esc\" value=" + iesc + ">"
str+=" <h2><table><tr><td><img alt=\"Logo\" src=\"images/CostesDescuentos.png\"/ ></td>"
str+= "<td><h2><%=den(88)%></h2></td></tr></table></h2>"
str+= mostrarCostesDescuentosItem(vGr, vIt, nombre,divModificable,iesc)
str+= "</form>"

numOrdenAplic=0
document.getElementById("divCostesDescItem").style.padding="10px"
document.getElementById("divCostesDescItem").style.border="1px solid #cccccc"
document.getElementById("divCostesDescItem").style.left=coordx
document.getElementById("divCostesDescItem").innerHTML=str


// DPD Ajustamos la posici�n vertical de la capa
var wd = getWindowData()
var margen = 10
document.getElementById("divCostesDescItem").style.top=coordy
if (parseInt(document.getElementById("divCostesDescItem").offsetHeight) + coordy > (wd[1] + wd[3]))  //se sale por la parte inferior de la pantalla --> Scroll	
{	
document.body.scrollTop = ((parseInt(document.getElementById("divCostesDescItem").offsetHeight) + parseInt(document.getElementById("divCostesDescItem").style.top)) - wd[1] )
}

//Pasar el foco al precio unitario del formulario de costes/descuentos
if(pu==4)
    var sNamePUI = "txtPujaPUI_" + vIt
else
    var sNamePUI = "txtPUI_" + vGr + "_" + vIt   

	
sNamePUI += idEsc // Identificador de escalado
	
eval("document.getElementById(\"frmCostesDescuentosItem\")." + sNamePUI + ".focus()")

}

/*
''' <summary>
''' Redibuja la pantalla de los Costes / Descuentos aplicados a item (precio y total)
''' </summary>
''' <param name="vGr">Objeto que contiene el grupo</param>
''' <param name="vIt">Objeto que contiene el item</param>
''' <param name="nombre">Nombre del campo de precio unitario</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:; Tiempo m�ximo: 0</remarks>
cumpoferta.asp
''' <author>JVS</author>
''' <revision>JVS 26/05/2011</revision>
*/
function redibujarFormCostesDescuentosItem(vGr, vIt, nombre, divModificable,iesc)
{

redibujarCostesDescuentosItem(vGr, vIt, nombre, divModificable,iesc)

}


/*
''' <summary>
''' Mostrar por pantalla los Costes / Descuentos aplicados a item (precio y total)
''' </summary>
''' <param name="vGr">Objeto que contiene el grupo</param>
''' <param name="oItem">Objeto que contiene el item</param>
''' <param name="nombre">Nombre del campo de precio unitario</param>
''' <param name="divModificable"Indica si los costes/descuentos se pueden modificar o no</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:; Tiempo m�ximo: 0</remarks>
cumpoferta.asp
''' <author>JVS</author>
''' <revision>LTG 30/03/2012</revision>
*/
function mostrarCostesDescuentosItem(codGr, idItem, nombre,divModificable,iesc)
{
var strCosteDesc
var strCosteDesc2
var i
var j
var vClase
var cant
var objCosteDescValor
var operador
var unidad
var importeAnterior = null
var bEncontradoPrimeroAplicTotalItem = false
var bEncontradoPrimeroAplicPrecioItem = false
var iAmbitoOferta = 1
var iAmbitoGrupo = 2
var iAmbitoItem = 3
var iGrupo

strCosteDesc = ""
strCosteDesc2= ""

if(nombre=="txtPujaPU_")
    pu=4
else if(nombre=="txtPU2_")
    pu=2
else if(nombre=="txtPU3_")
    pu=3
else
    pu=1
for (i=0;i<proceso.grupos.length;i++)
{
    if(proceso.grupos[i].cod == codGr)
    {
	    vGr = proceso.grupos[i]
        break
    }
}

iGrupo = i

if (pu==4)
{
    for (i=0;i<itemsPuja.length;i++)
    {
        if(itemsPuja[i].id == idItem)
        {
	        oItem = itemsPuja[i]
            break
        }
    }
}
 
else
{
   for (i=0;i<vGr.items.length;i++)
    {
        if(vGr.items[i].id == idItem)
        {
	        oItem = vGr.items[i]
            break
        }
    }
}

cant = oItem.cant


if (iesc>=0) 
{
	idEsc = "_" + oItem.escalados[iesc].id
	cant = oItem.escalados[iesc].cant
	//if (cant!=null) {if((cant<=oItem.escalados[iesc].inicio) || (cant>oItem.escalados[iesc].fin)) cant = null}

}
else
	idEsc = ""



var precioUnitario = eval("oItem.precio" + precioNum)

if (iesc>=0) precioUnitario= oItem.escalados[iesc].precio

   if (vGr.costesDescuentosItems)
    {
        strCosteDesc+= "<div><b>" + oItem.cod + " - " + oItem.descr + "</b></div>"

        vClase = "filaImpar"
        if(pu==4)
            strCosteDesc+= generaCabeceraCostesDescItemSubastas()
        else
            strCosteDesc+= generaCabeceraCostesDescItem()
        importeAnterior = precioUnitario
        strCosteDesc+= "<div name=divCabCostesDescItem id=divCabCostesDescItem style='text-align:right'>&nbsp;"
        strCosteDesc+= "</div>"
        if(pu==4)
        {
            sNamePU = "txtPujaPUI_" + oItem.id
	        sNamePUNormal = "txtPUI_" + vGr.cod +  "_" + oItem.id
			sNamePUNormal += idEsc

        }
        else
        {
            sNamePU = "txtPUI_" + vGr.cod + "_" + oItem.id            
        }
		
		sNamePU += idEsc 
        
        // Guardar precio para Undo
        undoPrecio = precioUnitario
        if(divModificable)
        {
            undoCostesDesc = new Array()
            undoEstado = proceso.estado
        }                         

        //Primero la parte de los costes descuentos para calcular el importe neto del escalado y poder mostrarlo en la parte superior del div
        for (i=0;i<vGr.costesDescuentosItems.length;i++)
	    {
            operador = obtenerOperador(vGr.costesDescuentosItems[i].operacion)
            unidad = obtenerUnidad(vGr.costesDescuentosItems[i].operacion, gCodMon)

            if (vGr.costesDescuentosItems[i].ambito == iAmbitoOferta)
                 objCosteDescValor = obtenerCosteDescValor(proceso.costesDescuentos,vGr.costesDescuentosItems[i])
            else if (vGr.costesDescuentosItems[i].ambito == iAmbitoGrupo)
                 objCosteDescValor = obtenerCosteDescValor(vGr.costesDescuentos,vGr.costesDescuentosItems[i])
            else  //if (vGr.costesDescuentosItems[i].ambito == iAmbitoItem)
                 objCosteDescValor = obtenerCosteDescValor(oItem.costesDescuentosOfertados,vGr.costesDescuentosItems[i])
            
            if(divModificable)
			    undoCostesDesc[i] = objCosteDescValor.valor

			objCosteDescImporteParcial = obtenerCosteDescValor(costesDescuentosImpParcial, vGr.costesDescuentosItems[i])

            //El primero que aplica al precio del item
            if(vGr.costesDescuentosItems[i].aplicar==3 && !bEncontradoPrimeroAplicPrecioItem)
            {
                strCosteDesc2+= "<br/>"
                strCosteDesc2+= "<fieldset style='padding:3px;'>"
                strCosteDesc2+= "<legend style='color:#000;font-weight:bold'><%=JSText(den(108))%>&nbsp;</legend>" //JVS Idioma Costes/descuentos a aplicar al precio unitario
                strCosteDesc2+= "	<table width=100%>"
                bEncontradoPrimeroAplicPrecioItem = true
            }            

            //El primero que aplica al total del item
            if(vGr.costesDescuentosItems[i].aplicar==2 && !bEncontradoPrimeroAplicTotalItem)
            {
            //Como es el primer coste/descuento que aplica al total del item, antes de mostrarlo sacamos los parciales en funci�n del precio
                if(bEncontradoPrimeroAplicPrecioItem)
                {
                    strCosteDesc2+= "	</table>"
                    strCosteDesc2+= "</fieldset>"
                    strCosteDesc2+= "<br/>"
                }
                strCosteDesc2+= "<fieldset style='padding:3px;'>"
                strCosteDesc2+= "<legend style='color:#000;font-weight:bold'><%=JSText(den(109))%>&nbsp;</legend>" //JVS Idioma Costes/descuentos a aplicar al importe total del �tem
                sNamePUCD = "txtPUF_" + vGr.cod + "_" + oItem.id
                importeParcial = precioNetoCalculado * cant
                strCosteDesc2+="<div id=divParcialImporteItem>	 <%=JSText(den(106))%>: " + num2str(cant,vdecimalfmt,vthousanfmt,vprecisionfmt)  + "&nbsp;" + oItem.uni.cod  + "&nbsp;&nbsp;&nbsp;" + "<%=JSText(den(107))%> " + num2str(importeParcial,vdecimalfmt,vthousanfmt,vprecisionfmt)  + "&nbsp;" + gCodMon + "</div>" //JVS Idioma Cantidad *** Importe total del �tem
                
                importeAnterior = importeParcial
                bEncontradoPrimeroAplicTotalItem = true
                strCosteDesc2+= "	<table width=100%>"
                vClase=="filaImpar"
            }
            strCosteDesc2+="		<tr>"
	        strCosteDesc2+="			<td width=20 class='" + vClase + "'>"
	        strCosteDesc2+="				<input type=hidden name=pAtribCod id=pAtribCod value=" + vGr.costesDescuentosItems[i].cod + ">"
	        strCosteDesc2+="					<table><tr><td width=20><nobr><a class='popUp" + " " + (vGr.costesDescuentosItems[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkCosteDesc(vGr.costesDescuentosItems[i],vGr.costesDescuentosItems[i].ambito,vGr.id) + "\">" + (vGr.costesDescuentosItems[i].obligatorio?"(*)":"") + recortar(vGr.costesDescuentosItems[i].den,15) + "</a></td><td><a class='popUp" + " " + (vGr.costesDescuentosItems[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkCosteDesc(vGr.costesDescuentosItems[i],vGr.costesDescuentosItems[i].ambito,vGr.id) + "\"><IMG border = 0 SRC='../images/masinform.gif'></a></td></tr></table></nobr>"
	        strCosteDesc2+="			</td>"
            strCosteDesc2+="			<td class='" + vClase + "' width=30%>"

            if(operador=="/" && !objCosteDescValor.valor)
                strCosteDesc2+= operador
            else    
            {        
                if(divModificable)
				    strCosteDesc2 += operador + " " + mostrarCosteDescuentoItem (vGr.costesDescuentosItems[i],objCosteDescValor, vGr.costesDescuentosItems[i].aplicar, vGr.cod, (vGr.costesDescuentosItems[i].ambito == iAmbitoItem?false:true), (pu==4?true:false),iesc) + " " + unidad
			    else
				    strCosteDesc2 += operador + " " + mostrarCosteDescuentoItem (vGr.costesDescuentosItems[i],objCosteDescValor, vGr.costesDescuentosItems[i].aplicar, vGr.cod, true, (pu==4?true:false),iesc) + " " + unidad
            }
            strCosteDesc2+="			</td>"
            strCosteDesc2+="			<td class='" + vClase + " importesParciales'>"
            strCosteDesc2+="			    <div id=divCosteDesc_" + objCosteDescImporteParcial.paid + ">"
			//if (objCosteDescValor && proceso.importe)
			//DPD Quito la condici�n de proceso.importe
            if (objCosteDescValor )
            {
				if(objCosteDescValor.aplicar!=2 || cant!=null)
				{
					if (objCosteDescImporteParcial.importeParcial && importeAnterior != null && objCosteDescValor.valor)
					{   
							strCosteDesc2+= "   " + num2str(importeAnterior,vdecimalfmt,vthousanfmt,vprecisionfmt) + "  " + operador + " "  + num2str(objCosteDescValor.valor,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + unidad + " = " +   num2str(objCosteDescImporteParcial.importeParcial,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
							importeAnterior = objCosteDescImporteParcial.importeParcial
					}
				}
                strCosteDesc2+="			    </div>"
       	        strCosteDesc2+="			</td>"
                strCosteDesc2+="		</tr>"

                if (vClase=="filaImpar")
		            vClase="filaPar"
	            else
		            vClase="filaImpar"
            }

        }           

        // Ahora la parte superior del div
        if(pu==4)
            strCosteDesc+= "<span>	<%=JSText(den(103))%>: " + inputNumero (sNamePU,wCPU,null,precioUnitario,null,null,"cant=" + cant + " pu=" + pu + " grupo='" + vGr.cod + "' itemid=" + oItem.id + " divModificable=" + divModificable + " oldValue=" + oItem.precio,"validarPrecioUnitario(\"" + sNamePUNormal + "\",itemsPuja[" + itemsPuja.length + "],", "<%=JSText(den(14))%>\\n<%=JSText(den(15))%> " + vdecimalfmt + "\\n<%=JSText(den(16))%> " + vthousanfmt) + "&nbsp;" + gCodMon + "&nbsp;&nbsp;&nbsp;" //JVS Idioma Precio unitario bruto
        else
            strCosteDesc+="<span>	<%=JSText(den(103))%>: " + inputNumero (sNamePU,wCPU,null,precioUnitario,null,null,"cant=" + cant + " uniden='" + oItem.uni.den + "' unicod='" + oItem.uni.cod + "' pu=" + pu + " grupo='" + vGr.cod + "' itemid=" + oItem.id + " oldValue=" + eval("oItem.precio" + precioNum),"validarPrecioUnitario(null,new Array(\"proceso.grupos[" + iGrupo + "].items[" + vGr.items.length + "]\"),", "<%=JSText(den(31))%>\\n<%=JSText(den(32))%> " + vdecimalfmt + "\\n<%=JSText(den(33))%> " + vthousanfmt) + "&nbsp;" + gCodMon + "</span>" //JVS Idioma Precio unitario bruto
        //strCosteDesc+=" <span id=divPrecioNeto><span  style=\"BORDER:1px solid #000000;FONT-WEIGHT: bold;WIDTH:200px;PADDING:2px\"> <%=JSText(den(104))%>: " + num2str(oItem.precioNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;" + gCodMon +"</span></span>" //JVS Idioma Precio unitario neto              
        strCosteDesc+=" <span id=divPrecioNeto><span  style=\"BORDER:1px solid #000000;FONT-WEIGHT: bold;WIDTH:200px;PADDING:2px\"> <%=JSText(den(104))%>: " + num2str(objCosteDescImporteParcial.importeParcial,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;" + gCodMon +"</span></span>" //JVS Idioma Precio unitario neto              

        //Parte superior + costes/descuentos
        strCosteDesc+=strCosteDesc2

        //Si no se ha encontrado ning�n coste/descuento que aplique al total del item
        //hay que sacar los datos parciales
        if(!bEncontradoPrimeroAplicTotalItem)
        {
            sNamePU = "txtPUF_" + vGr.cod + "_" + oItem.id
            importeParcial = precioNetoCalculado  * cant
            strCosteDesc+="<div>	 <%=JSText(den(106))%>: " + num2str(cant,vdecimalfmt,vthousanfmt,vprecisionfmt)  + "&nbsp;" + oItem.uni.cod + "</div>" //JVS Idioma Cantidad
            strCosteDesc+= "	</table>"
            strCosteDesc+= "</fieldset>"
            strCosteDesc+= "<br/>"
                
            importeAnterior = importeParcial
        }
        else
        {
            importeAnterior = objCosteDescImporteParcial.importeParcial
            strCosteDesc+= "	</table>"
            strCosteDesc+= "</fieldset>"
            strCosteDesc+= "<br/>"
        }

        if (objCosteDescValor && cant!=null)
        {
            if(objCosteDescImporteParcial.importeParcial)
            {
            
                sNameIF = "txtImpF_" + vGrp.cod + "_" + oItem.id
                strCosteDesc+="<div id=divImporteTotalItem><div style=\"BORDER:1px solid #000000;FONT-WEIGHT: bold;WIDTH:100%;PADDING:5px\">	<%=JSText(den(107))%>: " + num2str(importeAnterior,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon + "</div> </div>" //JVS Idioma Importe total del �tem
            }
            else
                strCosteDesc+="<div id=divImporteTotalItem><div style=\"BORDER:0px solid #000000;FONT-WEIGHT: bold;WIDTH:100%;PADDING:6px\"></div></div>"
        }
        strCosteDesc+= "<div style=\"TEXT-ALIGN:center;TOP-MARGIN:3px\"><input type=button class=button value='<%=JSText(den(105))%>' onclick='setTimeout(function() { CerrarDivCostesDescItem(" + idItem + ",\"" + codGr + "\",\"" + nombre + "\",1," + divModificable + "," + iesc + ",0)}, 700)'>" //JVS Idioma Aceptar
        strCosteDesc+= "<input type=button class=button value='<%=JSText(den(142))%>' onclick='setTimeout(function() { CerrarDivCostesDescItem(" + idItem + ",\"" + codGr + "\",\"" + nombre + "\",0," + divModificable + "," + iesc + ",0)}, 700)'></div>" //JVS Idioma Cancelar
        
     }
    return strCosteDesc
}

/*
''' <summary>
''' Redibujar la pantalla de los Costes / Descuentos aplicados a item (precio y total)
''' </summary>
''' <param name="vGr">Objeto que contiene el grupo</param>
''' <param name="oItem">Objeto que contiene el item</param>
''' <param name="nombre">Nombre del campo de precio unitario</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:; Tiempo m�ximo: 0</remarks>
cumpoferta.asp
''' <author>JVS</author>    
*/

function redibujarCostesDescuentosItem(codGr, idItem, nombre, divModificable,iesc)
{

var strCosteDesc
var i
var j
strCosteDesc = ""
var vClase


var objCosteDescValor
var operador
var unidad
var importeAnterior = null
var bEncontradoPrimeroAplicTotalItem = false
var bEncontradoPrimeroAplicPrecioItem = false
var iAmbitoOferta = 1
var iAmbitoGrupo = 2
var iAmbitoItem = 3
var iGrupo

var cant



if(divModificable==null)
    divModificable=true

if(nombre=="txtPujaPU_")
    pu=4
else if(nombre=="txtPU_")
    pu=1
else if(nombre=="txtPU2_")
    pu=2
else if(nombre=="txtPU3_")
    pu=3

for (i=0;i<proceso.grupos.length;i++)
{
    if(proceso.grupos[i].cod == codGr)
    {
	    vGr = proceso.grupos[i]
        break
    }
}

iGrupo = i

if (pu==4)
{
    for (i=0;i<itemsPuja.length;i++)
    {
        if(itemsPuja[i].id == idItem)
        {
	        oItem = itemsPuja[i]
            break
        }
    }
}
 
else
{
   for (i=0;i<vGr.items.length;i++)
    {
        if(vGr.items[i].id == idItem)
        {
	        oItem = vGr.items[i]
            break
        }
    }
}

cant = oItem.cant
if (iesc>=0)
{ 	
	cant = oItem.escalados[iesc].cant
	//if (cant!=null) {if((cant<=oItem.escalados[iesc].inicio) || (cant>oItem.escalados[iesc].fin)) cant = null}
}
   if (vGr.costesDescuentosItems)
    {

        importeAnterior = eval("oItem.precio" + precioNum)
		
		if(iesc>=0) importeAnterior = oItem.escalados[iesc].precio
		
		
        if(pu==4)
        {
            sNamePU = "txtPujaPUI_" + oItem.id
	        sNamePUNormal = "txtPUI_" + vGr.cod +  "_" + oItem.id
        }
        else
        {
            sNamePU = "txtPUI_" + vGr.cod + "_" + oItem.id            
        }

        str=""
        str=" <span  style=\"BORDER:1px solid #000000;FONT-WEIGHT: bold;WIDTH:200px;PADDING:2px\"> <%=JSText(den(104))%>: " + num2str(precioNetoCalculado,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;" + gCodMon +"</span>" //JVS Idioma Precio unitario neto
        document.getElementById("divPrecioNeto").innerHTML=str
        
        for (i=0;i<vGr.costesDescuentosItems.length;i++)
	    {
            operador = obtenerOperador(vGr.costesDescuentosItems[i].operacion)
            unidad = obtenerUnidad(vGr.costesDescuentosItems[i].operacion, gCodMon)

            if (vGr.costesDescuentosItems[i].ambito == iAmbitoOferta)
                 objCosteDescValor = obtenerCosteDescValor(proceso.costesDescuentos,vGr.costesDescuentosItems[i])
            else if (vGr.costesDescuentosItems[i].ambito == iAmbitoGrupo)
                 objCosteDescValor = obtenerCosteDescValor(vGr.costesDescuentos,vGr.costesDescuentosItems[i])
            else  //if (vGr.costesDescuentosItems[i].ambito == iAmbitoItem)
                 objCosteDescValor = obtenerCosteDescValor(oItem.costesDescuentosOfertados,vGr.costesDescuentosItems[i])

			objCosteDescImporteParcial = obtenerCosteDescValor(costesDescuentosImpParcial, vGr.costesDescuentosItems[i])
            //El primero que aplica al precio del item
            if(vGr.costesDescuentosItems[i].aplicar==3 && !bEncontradoPrimeroAplicPrecioItem)
            {
                bEncontradoPrimeroAplicPrecioItem = true
            }

			if(vGr.costesDescuentosItems[i].aplicar==2 && cant==null) objCosteDescImporteParcial.importeParcial = null
			
            //El primero que aplica al total del item
            if(vGr.costesDescuentosItems[i].aplicar==2 && !bEncontradoPrimeroAplicTotalItem)
            {				            
                		
            //Como es el primer coste/descuento que aplica al total del item, antes de mostrarlo sacamos los parciales en funci�n del precio
                if(bEncontradoPrimeroAplicPrecioItem)
                {
                }
				if (cant!=null)
				{
					sNamePU = "txtPUF_" + vGr.cod + "_" + oItem.id
					importeParcial = precioNetoCalculado * cant
					str=""
					str+="	 <%=JSText(den(106))%>: " + num2str(cant,vdecimalfmt,vthousanfmt,vprecisionfmt)  + "&nbsp;" + oItem.uni.cod  + "&nbsp;&nbsp;&nbsp;" + "<%=JSText(den(107))%> " + num2str(importeParcial,vdecimalfmt,vthousanfmt,vprecisionfmt)  + "&nbsp;" + gCodMon //JVS Idioma Cantidad *** Importe total del �tem
					document.getElementById("divParcialImporteItem").innerHTML=str
				}
				else
				{
					importeParcial = null
					objCosteDescImporteParcial.importeParcial = null
				}
				importeAnterior = importeParcial
                bEncontradoPrimeroAplicTotalItem = true
                vClase=="filaImpar"
            }
            str=""
            divCosteDesc = "divCosteDesc_" + objCosteDescImporteParcial.paid
			//if (objCosteDescValor && proceso.importe)
			//DPD Con los escalados existe la posibilidad de que haya precios unitarios pero no cantidades
			//por lo que es posible que tampoco haya importe. Pero aun as� hay que mostar los costes descuentos (sobre precio unitario)
			if (objCosteDescValor)
            {
                if (objCosteDescImporteParcial.importeParcial && importeAnterior != null && objCosteDescValor.valor)
                {   
                        str+= "   " + num2str(importeAnterior,vdecimalfmt,vthousanfmt,vprecisionfmt) + "  " + operador + " "  + num2str(objCosteDescValor.valor,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + unidad + " = " +   num2str(objCosteDescImporteParcial.importeParcial,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
                }
				document.getElementById(divCosteDesc).innerHTML=str
				importeAnterior = objCosteDescImporteParcial.importeParcial
				
            }

        }   
        //Si no se ha encontrado ning�n coste/descuento que aplique al total del item
        //hay que sacar los datos parciales
        if(!bEncontradoPrimeroAplicTotalItem)
        {
            sNamePU = "txtPUF_" + vGr.cod + "_" + oItem.id
            importeParcial = precioNetoCalculado * cant

            importeAnterior = importeParcial
        }
        else
        {
            importeAnterior = objCosteDescImporteParcial.importeParcial
        }

        if (objCosteDescValor && cant!=null)
        {
            if(objCosteDescImporteParcial.importeParcial)
            {            
                sNameIF = "txtImpF_" + vGrp.cod + "_" + oItem.id
                str=""
                str+="<div style=\"BORDER:1px solid #000000;FONT-WEIGHT: bold;WIDTH:100%;PADDING:5px\">	<%=JSText(den(107))%>: " + num2str(importeAnterior,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon + "</div>"
                document.getElementById("divImporteTotalItem").innerHTML=str
            }
            else
            {
                str=""
                str+="<div style=\"BORDER:0px solid #000000;FONT-WEIGHT: bold;WIDTH:100%;PADDING:6px\"></div>"
                document.getElementById("divImporteTotalItem").innerHTML=str
            }
        }
        
     }
}



/*
''' <summary>
''' Mostrar por pantalla la capa que se superpone para mostrar los Costes / Descuentos aplicados a item (precio y total)
''' </summary>
''' <param name="oItem">Objeto que contiene el item</param>
''' <param name="codGr">Objeto que contiene el grupo</param>
''' <param name="nombre">Nombre del campo de precio unitario</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:; Tiempo m�ximo: 0</remarks>
cumpoferta.asp
''' <author>JVS</author>
''' <revision>JVS 26/05/2011</revision>
*/

function MostrarDivCostesDescItem(idItem, codGr, nombre, iesc){

    var indiceGr
    var indiceIt
    var oItemSecundario = null
	costesDescuentosImpParcial = new Array()

    if(nombre=="txtPU_")
        precioNum = ""
    else if(nombre=="txtPU2_")
        precioNum = 2
    else if(nombre=="txtPU3_")
        precioNum = 3
		
	
    for(indiceGr=0;indiceGr<proceso.grupos.length;indiceGr++)
    if(proceso.grupos[indiceGr].cod == codGr)
    {
        vGr = proceso.grupos[indiceGr]
        break
    }
    if (vGr.costesDescuentosItems.length)
        hayCostesDescuentosItems = true
    else
        hayCostesDescuentosItems = false

    if (hayCostesDescuentosItems)
    {
	    document.getElementById("divCostesDescItem").style.visibility = "visible"
        
        if(nombre=="txtPujaPU_")
        {
            for(indiceIt=0;indiceIt<itemsPuja.length;indiceIt++)
                if(itemsPuja[indiceIt].id == idItem)
                {
                    oItem = itemsPuja[indiceIt]
                    break
                }
            if(vGr.items.length)
            {
                for(indiceIt=0;indiceIt<vGr.items.length;indiceIt++)
                if(vGr.items[indiceIt].id == oItem.id)
                {
                    oItemSecundario = vGr.items[indiceIt]
                    break
                }
            }
        }
        else
        {
            for(indiceIt=0;indiceIt<vGr.items.length;indiceIt++)
                if(vGr.items[indiceIt].id == idItem)
                {
                    oItem = vGr.items[indiceIt]
                    break
                }
            <%if oProceso.subasta then%>
                for(indiceIt=0;indiceIt<itemsPuja.length;indiceIt++)
                    if(itemsPuja[indiceIt].id == oItem.id)
                    {
                        oItemSecundario = itemsPuja[indiceIt]
                        break
                    }
            <%end if %>

        }

		if (iesc>=0) 
			idEsc = "_" + oItem.escalados[iesc].id
		else
			idEsc = ""


		
        var precioUnitarioInicial = eval("oItem.precio" + precioNum)
		
		
		//DPD  SI HAY ESCALADOS, CARGAR EL PRECIO DEL ESCALADO
		if (iesc>=0)  precioUnitarioInicial = oItem.escalados[iesc].precio
		
        var precioUsar = oItem.usar // precio al que se pueden modificar sus c/d
        if(nombre=="txtPU_" && (precioUsar==1 || precioUsar==null))
            divModificable = true
        else if(nombre=="txtPU2_" && precioUsar==2)
            divModificable = true
        else if(nombre=="txtPU3_" && precioUsar==3)
            divModificable = true
        else if(nombre=="txtPujaPU_")
            divModificable = true
        else
            divModificable = false		
		recalcularCostesDescuentosItem(vGr,oItem,precioUnitarioInicial,(nombre=="txtPujaPU_"?1:0), oItemSecundario, null, null, null, divModificable,iesc,0)
        //el precio neto nuevo se usa para actualizar el campo de precio unitario
        if(nombre=="txtPujaPU_")
            var sNamePU = nombre + idItem
        else
            var sNamePU = nombre + codGr + "_" + idItem		
		
		/* 
        // se guarda el precio en el campo de precio unitario: si estamos en el precio aplicable se guarda el neto y si no el bruto
        if(divModificable)
            eval("document.getElementById(\"frmOfertaAEnviar\")." + sNamePU + idEsc + ".value=((oItem.precioNeto!=null)?num2str(oItem.precioNeto,vdecimalfmt,vthousanfmt,1000):'')")
        else
            eval("document.getElementById(\"frmOfertaAEnviar\")." + sNamePU + ".value=((precioNetoCalculado!=null)?num2str(precioNetoCalculado,vdecimalfmt,vthousanfmt,1000):'')")
        */

        linkRecalculoItem = 0
    
       //elemento en relaci�n al cual se va a posicionar la capa de Costes/Descuentos de �tem
        var elm = eval("document.getElementById(\"frmOfertaAEnviar\")." + sNamePU + idEsc)
        if( typeof( elm.offsetParent ) != 'undefined' ) {
        var originalElement = elm
        for(coordx = 0, coordy = 0; elm; elm = elm.offsetParent ) {
          coordx += elm.offsetLeft
          coordy += elm.offsetTop
          //Para el caso de scroll
          if( elm != originalElement && elm != document.body && elm != document.documentElement ) {
            coordx -= elm.scrollLeft
            coordy -= elm.scrollTop
          }
        }
      } else {
        coordx = elm.x
        coordy = elm.y
      }

        coordx -= 350
        coordy += 20

        mostrarFormCostesDescuentosItem(codGr, idItem,nombre,divModificable,iesc,idEsc)		
    }	
}

/*
''' <summary>
''' Oculta la capa flotante de costes/descuentos para �tems
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:; Tiempo m�ximo: 0</remarks>
cumpoferta.asp
''' <author>JVS</author>
*/
function OcultarDivCostesDescItem(){
	document.getElementById("divCostesDescItem").style.visibility = "hidden"
}

/*
''' <summary>
''' Cierra la capa flotante de costes/descuentos para �tems y recalcula los costes/descuentos para ese �tem
''' </summary>
''' <param name="idItem">identificador del item</param>
''' <param name="codGr">c�digo del grupo</param>
''' <param name="nombre">Nombre del campo de precio unitario</param>
''' <param name="bAceptCancel">Se pulso aceptar en los c/d o no</param>
''' <param name="divModificable">Se pueden modificar c/d (depende de q precio usar y q precio estes cerrando)</param>
''' <param name="iesc">escalados</param>
''' <param name="bRecalcCD">Tras un cambio en los c/d de grupo/proceso los importes/precios del item pueden cambiar</param>
''' <remarks>Llamada desde:function mostrarCostesDescuentosItem     function AplicarReducirPrecios   function introducirPrecios     js\cargarGanadora.asp; Tiempo m�áximo: 0</remarks>
''' <author>JVS</author>
''' <revision>LTG 30/03/2012</revision>
*/
function CerrarDivCostesDescItem(idItem, codGr, nombre, bAceptCancel,divModificable,iesc, bRecalcCD)
{

    var indiceGr
    var indiceIt
    var oItemSecundario = null

    var iAmbitoOferta = 1
    var iAmbitoGrupo = 2
    var iAmbitoItem = 3
    var objCosteDescValor
    var undoRecalculo
	
	if (iesc>=0) {
        if (bRecalcCD == 1) {    
            if(nombre=="txtPujaPU_"){
                for(indiceIt=0;indiceIt<itemsPuja.length;indiceIt++){
                    if(itemsPuja[indiceIt].id == idItem)
                    {
                        oItem = itemsPuja[indiceIt]
                        break
                     }
                }
            }
            else{
                for(indiceGr=0;indiceGr<proceso.grupos.length;indiceGr++){
                    if(proceso.grupos[indiceGr].cod == codGr){
                        vGr = proceso.grupos[indiceGr]
                        for(indiceIt=0;indiceIt<vGr.items.length;indiceIt++){
                            if(vGr.items[indiceIt].id == idItem)
                            {
                                oItem = vGr.items[indiceIt]
                                break
                            }
                        }
                        break
                    }
                }
            }
        }
		idEsc = "_" + oItem.escalados[iesc].id
    }
	else
		idEsc = ""		
		
    if(nombre=="txtPujaPU_")
        var sNamePU = nombre + idItem
    else
        var sNamePU = nombre + codGr + "_" + idItem
		
	sNamePU+=idEsc
    
    var precioUnitarioInicial = eval("document.getElementById(\"frmOfertaAEnviar\")." + sNamePU + ".value")
    for(indiceGr=0;indiceGr<proceso.grupos.length;indiceGr++)
        if(proceso.grupos[indiceGr].cod == codGr)
        {
            vGr = proceso.grupos[indiceGr]
            break
        }

    if(nombre=="txtPujaPU_")
    {
        for(indiceIt=0;indiceIt<itemsPuja.length;indiceIt++)
            if(itemsPuja[indiceIt].id == idItem)
            {
                oItem = itemsPuja[indiceIt]
                
                if (bRecalcCD == 1) {
                    if (oItem.precioNeto == null) {
                        return false;
                    }
                }
                
                break
            }
        if(vGr.items.length)
        {
            for(indiceIt=0;indiceIt<vGr.items.length;indiceIt++)
                if(vGr.items[indiceIt].id == oItem.id)
                {
                    oItemSecundario = vGr.items[indiceIt]
                    break
                }
       }
    }
    else
    {
        for(indiceIt=0;indiceIt<vGr.items.length;indiceIt++)
            if(vGr.items[indiceIt].id == idItem)
            {
                oItem = vGr.items[indiceIt]
                 if (bRecalcCD == 1) {
                    if (oItem.precioNeto == null) {
                        return false;
                    }
                }
                break
            }
        <%if oProceso.Subasta then %>
            for(indiceIt=0;indiceIt<itemsPuja.length;indiceIt++)
                if(itemsPuja[indiceIt].id == oItem.id)
                {
                    oItemSecundario = itemsPuja[indiceIt]
                    break
                }
        <%end if %>        
    }


    if(bAceptCancel==0)  // si se cancela, recuperamos el antiguo precio y el estado,  y despus recalculamos
    {
        if(divModificable)
            proceso.estado = undoEstado
        if(precioNum=="")
		{
            oItem.precio = undoPrecio
			if (iesc>=0) oItem.escalados[iesc].precio = undoPrecio
		}
        if(precioNum==2)
            oItem.precio2 = undoPrecio
        if(precioNum==3)
            oItem.precio3 = undoPrecio
    }
    var precioUnitarioInicial = eval("oItem.precio" + precioNum)
	//DPD  SI HAY ESCALADOS, CARGAR EL PRECIO DEL ESCALADO
	if (iesc>=0)  precioUnitarioInicial = oItem.escalados[iesc].precio

	

    if(bAceptCancel==0)  // si se cancela, recuperamos los antiguos valores de los costes/descuentos y despu�s recalculamos
    for (i=0;i<vGr.costesDescuentosItems.length;i++)
    {
            if (vGr.costesDescuentosItems[i].ambito == iAmbitoOferta)
                 objCosteDescValor = obtenerCosteDescValor(proceso.costesDescuentos,vGr.costesDescuentosItems[i])
            else if (vGr.costesDescuentosItems[i].ambito == iAmbitoGrupo)
                 objCosteDescValor = obtenerCosteDescValor(vGr.costesDescuentos,vGr.costesDescuentosItems[i])
            else  //if (vGr.costesDescuentosItems[i].ambito == iAmbitoItem)
                 objCosteDescValor = obtenerCosteDescValor(oItem.costesDescuentosOfertados,vGr.costesDescuentosItems[i])
            if(divModificable)
			{
                objCosteDescValor.valor = undoCostesDesc[i]
				if(vGr.costesDescuentosItems[i].ambito==iAmbitoItem && iesc>=0) 
				{	
					if (objCosteDescValor.escalados[iesc]) //@@DPD No deber�a ser necesaria la comprobaci�n
					{
						objCosteDescValor.escalados[iesc].valorNum =undoCostesDesc[i]
					}
				}
			}
    }   
    if (document.getElementById("frmCostesDescuentosItem")){ 
 	    if (document.getElementById("frmCostesDescuentosItem").esc) 
	    {
		    iesc=document.getElementById("frmCostesDescuentosItem").esc.value
		    if(iesc>=0) 
			    idEsc = "_" + oItem.escalados[iesc].id
		    else
			    idEsc = ""
	    }
	    else
	    {
		    iesc="-1"   
		    idEsc = ""
	    }
    }
	recalcularCostesDescuentosItem(vGr,oItem,precioUnitarioInicial,(nombre=="txtPujaPU_"?1:0), oItemSecundario, null, null, null, divModificable, iesc,bRecalcCD)

   //el precio neto nuevo se usa para actualizar el campo de precio unitario
    if(nombre=="txtPujaPU_")
        var sNamePU = nombre + idItem
    else
        var sNamePU = nombre + codGr + "_" + idItem

	sNamePU+=idEsc		
    eval("document.getElementById(\"frmOfertaAEnviar\")." + sNamePU + ".value=(precioNetoCalculado?num2str(precioNetoCalculado,vdecimalfmt,vthousanfmt,1000):'')")


    linkRecalculoItem = 0

if(bAceptCancel==0)
{
    if(divModificable)
    {
       //muestra el importe del item neto, una vez aplicados los costes/descuentos que correspondan
	   
	   var mostrarLinea = true
	   
	   if (iesc>=0)
	   {
	   
			//if (oItem.escalados[iesc].usar)	   
			//{
				//se ignoran los escalados que no correspnden con la cantidad
				mostrarLinea = oItem.escalados[iesc].usar
			//}
	   }
	   
		if (mostrarLinea)
		{
			strDetOferta = "<b>" + num2str(oItem.importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
		}
         <%if oProceso.Subasta then %>
            document.getElementById("divTotalLinea4" + "_" + oItem.id + idEsc).innerHTML = strDetOferta
        <% else%>
            if(precioNum=="")
                if(document.getElementById("divTotalLinea1" + "_" + oItem.id + idEsc)) document.getElementById("divTotalLinea1" + "_" + oItem.id + idEsc).innerHTML = strDetOferta
            else if(precioNum==2)
                if(document.getElementById("divTotalLinea2" + "_" + oItem.id + idEsc)) document.getElementById("divTotalLinea2" + "_" + oItem.id + idEsc).innerHTML = strDetOferta
            else if(precioNum==3)
                if(document.getElementById("divTotalLinea3" + "_" + oItem.id + idEsc)) document.getElementById("divTotalLinea3" + "_" + oItem.id + idEsc).innerHTML = strDetOferta
        <%end if %>
    }
}

        if((linkRecalculo == 0) || (bRecalcCD == 0))
            generaCabeceraItems()
			
			
	if (iesc >=0)
	{
		//Asignar al item los costes-descuentos correctos seg�n los escalados
		for (i=0;i<oItem.costesDescuentosOfertados.length;i++)
		{
			oItem.costesDescuentosOfertados[i].valor = null
			for (j=0;j<oItem.costesDescuentosOfertados[i].escalados.length;j++)
			{
				if(oItem.escalados[j].usar)
				{
					oItem.costesDescuentosOfertados[i].valor = oItem.costesDescuentosOfertados[i].escalados[j].valorNum
				}
			}
		
		}
	}			
    OcultarDivCostesDescItem()
}   



</script>




<%
'******************************************************************************
'**** FUNCION QUE MUESTRA LOS ATRIBUTOS DE UN GRUPO     *****************
'******************************************************************************

%>
<script>

// <summary>
// acceso a la pantalla de datos de atributos desde el men� lateral
// </summary>
// <param name="e">identificador de la pantalla</param>
// <returns></returns>
// <remarks>Llamada desde: config.asp/mostrarConfigProceso; Tiempo m�ximo:0,1</remarks>
// <revision>JVS 16/11/2011</revision>

function introducirAtributos(e)
{
var vRama

if (document.getElementById("divApartado").aptdoAnterior)
	eval (document.getElementById("divApartado").aptdoAnterior)

vRama = eval("rama" + e)

dibujarApartado(vRama.text)
resize()

// Pone a false los campos booleanos de control de la pantalla de retorno tras rec�lculo
cambiarFlag()
// Pone a true el campo booleano de introducirAtributos de control de la pantalla de retorno tras rec�lculo
bEstamosEnAtributos = true
bEstamos="5_" + vRama.cod


var vGrp

var i
for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != vRama.cod;i++)
	{
	}
vGrp = proceso.grupos[i]

document.getElementById("divApartado").aptdoAnterior="guardarAtributosGrupo('" + vGrp.cod + "')"
vGrp.cargarAtribs("fraOfertaServer")


}

function guardarAtributosGrupo(cod)
{
<%if not bConsulta then%>
	var vGrp

	var i
	for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != cod;i++)
		{
		}
	vGrp = proceso.grupos[i]
    if (vGrp.Bloqueado == 0)
    {
	    f = document.forms["frmAtributosGrupo" + vGrp.cod]
	    for (i=0;i<vGrp.atribs.length;i++)
		    {
		    sNameAtrib = "grupoAtrib_" + vGrp.cod + "_" + vGrp.atribs[i].paid
		
		    switch (vGrp.atribs[i].tipo)
			    {
			    case 1: //Entrada de texto
				    vGrp.atribs[i].valor = eval("f." + sNameAtrib + ".value")
				    break
			    case 2: //Entrada de n�meros
				    vGrp.atribs[i].valor = str2num(eval("f." + sNameAtrib + ".value"),vdecimalfmt,vthousanfmt,vprecisionfmt)
				    break
			    case 3: //Entrada de fechas
				    vGrp.atribs[i].valor = str2date(eval("f." + sNameAtrib + ".value"),vdatefmt)
				    break
			    case 4: //Chekbox
			        vGrp.atribs[i].valor = f.elements[sNameAtrib].value == "" ? null : f.elements[sNameAtrib].value == 1
				    break
			    }
		
            }
		}
<%end if%>

}

function mostrarAtributosGrupo(vGrp){
    var str

    str = ""
    str+= "<form name=frmAtributosGrupo" + vGrp.cod + " id=frmAtributosGrupo" + vGrp.cod + ">"
    str+= "	<table width=100%>"
    str+= "		<tr>"
    str+= "			<td width=100%>" 
    str+= "			" + mostrarAtributos(vGrp,"frmAtributosGrupo" + vGrp.cod, vGrp)
    str+= "			</td>"
    str+= "		</tr>"
    str+= "	<table>"
    str+= "</form>"

    document.getElementById("divApartadoS").innerHTML=str
}

</script>

<%
'******************************************************************************
'**** FUNCION QUE MUESTRA LOS ITEMS DE UN GRUPO    *****************
'******************************************************************************
%>
<script>
var wCEsp
var wCCodArt
var wCDenArt
var wCDestino
var wCCantidad
var wCUnidad 
var wCDenAnyoImputacion

var wCOfertaMinima
var wCPosicionPuja
var wCProveedor
var wCOfertaActual 
var wCObjetivo
var wCFecIni
var wCFecFin
var wCFormaPago
var wCPU
var wCTotalLinea
var wCComentario
var wCCantMax
var wCAdjunItem 
var wCPU2
var wCPU3
var hCFijo
var hCabecera 
var wCRango
var wDesviacion


// <summary>acceso a la pantalla de datos de precios desde el men� lateral</summary>
// <param name="e">identificador de la pantalla</param>
// <remarks>Llamada desde: config.asp/mostrarConfigProceso; Tiempo m�ximo:0,1</remarks>
// <revision>JVS 16/11/2011</revision>
function introducirPrecios(e){   
    var vRama
    if (document.getElementById("divApartado").aptdoAnterior)
	    eval (document.getElementById("divApartado").aptdoAnterior)

    vRama = eval("rama" + e)

    dibujarApartado(vRama.text)
    resize()

    // Pone a false los campos booleanos de control de la pantalla de retorno tras rec�lculo
    cambiarFlag()
    // Pone a true el campo booleano de introducirPrecios de control de la pantalla de retorno tras rec�lculo
    bEstamosEnPrecios = true
    bEstamos="8_" + vRama.cod

    var vGrp

    var i
    for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != vRama.cod;i++){}
    vGrp = proceso.grupos[i]    
    if (vGrp.items.length==0)
	    dibujarBarra("<%=den(28)%>")

    document.getElementById("divApartado").aptdoAnterior="OcultarDivCostesDescItem()"
    vGrp.cargarItems(false,"fraOfertaServer","");    
    if ((vGrp.costesDescuentosItems.length) || (vGrp.costesDescuentos.length)){
        if(linkRecalculo == 0 ){
            //Recalc todo en funcion de los c/d de grupo/proceso q se apliquen al item y aun no han sido aplicados 

            var nEscalados = 1
            var iesc
            var bAplica = true

            if (vGrp.hayEscalados) nEscalados = vGrp.escalados.length

            for (i = 0; i <= vGrp.items.length - 1; i++) {
                for (iesc=0; iesc<nEscalados; iesc++){
                    if (vGrp.hayEscalados){
                        if (vGrp.items[i].cant >= vGrp.escalados[iesc].inicio){
                            if (vGrp.escalados[iesc].fin){
                                if (vGrp.items[i].cant <= vGrp.escalados[iesc].fin){
                                    escalado = iesc
                                    bAplica = true
                                }
                                else{
                                    bAplica = false 
                                }
                            }
                            else{
                                if (vGrp.items[i].cant == vGrp.escalados[iesc].inicio){
                                    escalado = iesc
                                    bAplica = true
                                }
                                else{
                                    bAplica = false 
                                }                            
                            }
                        }
                        else{
                            bAplica = false 
                        }
                    }
                    else
                        escalado = -1  
    
                    if (bAplica){
                        if (proceso.cfg.subasta){
                            CerrarDivCostesDescItem(vGrp.items[i].id, vGrp.cod, "txtPujaPU_", "1",true,escalado,1)
                        }
                        else{
                            var precioUsar = vGrp.items[i].usar // precio al que se pueden modificar sus c/d
                            if(precioUsar==1 || precioUsar==null){
                                CerrarDivCostesDescItem(vGrp.items[i].id, vGrp.cod, "txtPU_", "1",true,escalado,1)
                            }
                            else if(precioUsar==2){
                                CerrarDivCostesDescItem(vGrp.items[i].id, vGrp.cod, "txtPU2_", "1",true,escalado,1)
                            }
                            else if(precioUsar==3){
                                CerrarDivCostesDescItem(vGrp.items[i].id, vGrp.cod, "txtPU3_", "1",true,escalado,1)
                            }
                            }
                        }
                    }
            }
        }
    }    
}


//''' <summary>
//''' Mostrar los Items del Grupo
//''' </summary>
//''' <param name="vGrp">Grupo</param>
//''' <remarks>Llamada desde: js\grupo.asp ; Tiempo m�ximo: 0,2</remarks>
function mostrarItemsGrupo(vGrp)
{
ocultaArbol()

if (document.getElementById("div" + vGrp.cod))
	{
	hAptdo = document.getElementById("divApartadoS").style.height
	hCabecera = 2*(hCFijo + wDesviacion)

	document.getElementById("div" + vGrp.cod).style.overflow="hidden"
	document.getElementById("div" + vGrp.cod).style.top = document.getElementById("divApartado").style.top
	document.getElementById("div" + vGrp.cod).style.left = document.getElementById("divApartado").style.left
	document.getElementById("div" + vGrp.cod).style.height = document.getElementById("divApartado").style.height
	document.getElementById("div" + vGrp.cod).style.width = document.getElementById("divApartado").style.width
	document.getElementById("div" + vGrp.cod).style.clip = document.getElementById("divApartadoS").style.clip
    
    var calcPx

    calcPx = parseFloat(document.getElementById("divApartadoS").style.height) - hCabecera
	document.getElementById("divScroll" + vGrp.cod ).style.height= calcPx + 'px';
    calcPx = parseFloat(document.getElementById("div" + vGrp.cod).style.width)- vGrp.wApArticulo
	document.getElementById("divScroll" + vGrp.cod ).style.width=  calcPx + 'px';

	document.getElementById("divArticulo" + vGrp.cod ).style.width = vGrp.wApArticulo + 'px';
	document.getElementById("divDatosItem" + vGrp.cod ).style.width = vGrp.wApDatosItem + 'px';
	<%if oProceso.subasta then%>
        if(document.getElementById("divSubasta" + vGrp.cod)) document.getElementById("divSubasta" + vGrp.cod ).style.width = vGrp.wApSubasta + 'px';	
	<%end if%>
	document.getElementById("divOferta" + vGrp.cod ).style.width = vGrp.wApOferta + 'px';
	<%if oProceso.PedirAlterPrecios then%>
		document.getElementById("divMasPrecios" + vGrp.cod ).style.width = vGrp.wApMasPrecios + 'px';
	<%end if%>
	if (vGrp.atribsItems.length>0)
		{
		document.getElementById("divAtributos" + vGrp.cod ).style.width = vGrp.wApAtributos + 'px';
		}


	document.getElementById("div" + vGrp.cod).style.visibility="visible"
	document.getElementById("divApartadoS").style.visibility="hidden"
	document.getElementById("divApartado").style.visibility="hidden"
	
	
	}


document.images["imgDatosItemD_" +  vGrp.cod ].style.visibility=vGrp.wApDatosItem != (wCSeparacion + wDesviacion + wBorde) ? "hidden" : "visible"
document.images["imgDatosItemI_" +  vGrp.cod ].style.visibility=vGrp.wApDatosItem == (wCSeparacion + wDesviacion + wBorde) ? "hidden" : "visible"
<%if oProceso.subasta then%>
        // DPD Incidencia 18899
        // cuando en la subasta a�n no se ha abierto el sobre, estas dos im�genes no existen.
		if (document.images["imgSubastaD_" +  vGrp.cod ]) document.images["imgSubastaD_" +  vGrp.cod ].style.visibility=vGrp.wApSubasta != (wCSeparacion + wDesviacion + wBorde) ? "hidden" : "visible"
		if (document.images["imgSubastaI_" +  vGrp.cod ]) document.images["imgSubastaI_" +  vGrp.cod ].style.visibility=vGrp.wApSubasta == (wCSeparacion + wDesviacion + wBorde) ? "hidden" : "visible"
<%end if%>

document.images["imgOfertaD_" +  vGrp.cod ].style.visibility=vGrp.wApOferta != (wCSeparacion + wDesviacion + wBorde) ? "hidden" : "visible"
document.images["imgOfertaI_" +  vGrp.cod].style.visibility=vGrp.wApOferta == (wCSeparacion + wDesviacion + wBorde) ? "hidden" : "visible"

<%if oProceso.PedirAlterPrecios then%>
	document.images["imgMasPreciosD_" +  vGrp.cod ].style.visibility=vGrp.wApMasPrecios != (wCSeparacion + wDesviacion + wBorde) ? "hidden" : "visible"
	document.images["imgMasPreciosI_" +  vGrp.cod ].style.visibility=vGrp.wApMasPrecios == (wCSeparacion + wDesviacion + wBorde) ? "hidden" : "visible"
<%end if%>
if (vGrp.atribsItems.length>0)
	{
	document.images["imgAtributosD_" +  vGrp.cod ].style.visibility=vGrp.wApAtributos != (wCSeparacion + wDesviacion + wBorde) ? "hidden" : "visible"
	document.images["imgAtributosI_" +   vGrp.cod ].style.visibility=vGrp.wApAtributos == (wCSeparacion + wDesviacion + wBorde) ? "hidden" : "visible"
	}

}



/*
''' <summary>
''' Cada vez q scrolas los items, hay q scrolar la cabecera de los items.
''' </summary>
''' <param name="cod">grupo a scrollar</param>
''' <remarks>Llamada desde: cumpoferta.asp(5 llamadas)  cumpoferta.js(1 llamada)  js\cargaritems.asp (2 onscroll); Tiempo m�ximo: 0</remarks>*/
function scrollFijas(cod)
{

document.getElementById("divArticulo" + cod).style.top=  - parseFloat(document.getElementById("divScroll" + cod).scrollTop) + 'px';
<%if oProceso.PedirAlterPrecios then%>

<%end if%>
 if (document.images["imgAtributos_" + cod ])
    document.getElementById("divCabecera" + cod).style.left=  - parseFloat(document.getElementById("divScroll" + cod).scrollLeft) + 'px';
 if (document.images["imgSubasta_" + cod ])
    document.getElementById("divCabecera" + cod).style.left=  - parseFloat(document.getElementById("divScroll" + cod).scrollLeft) + 'px';
 if (document.images["imgDatosItem_" + cod ])
    document.getElementById("divCabecera" + cod).style.left=  - parseFloat(document.getElementById("divScroll" + cod).scrollLeft) + 'px';
}

//''' <summary>
//''' Muestra/oculta el grupo de oferta (no subasta)
//''' </summary>
//''' <param name="cod">Grupo</param>
//''' <remarks>Llamada desde: js\cargaritems.asp (2 onclick); Tiempo m�ximo: 0,2</remarks>
function omDatosItem(cod)
{
var vGrp

var i
for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != cod;i++)
	{
	}
vGrp = proceso.grupos[i]

if (vGrp.wApDatosItem == wCSeparacion + wDesviacion + wBorde + CIE)
	{
	vGrp.wApDatosItem = wCSeparacion + wCDestino + wCCantidad + wCUnidad + wCOfertaActual + wCObjetivo + wCFecIni + wCFecFin + wCFormaPago + (vGrp.colspanDatosItem +1)*(wDesviacion + wBorde + CIE)
	if (vGrp.hayEscalados)
	{ // las columnas de �ltimo precio y objetivo no est�n en el �rea de �tem sino en la de oferta
	vGrp.wApDatosItem = vGrp.wApDatosItem - wCOfertaActual - wCObjetivo
	}
	document.images["imgDatosItemD_" + cod ].style.visibility="hidden"
	document.images["imgDatosItemI_" + cod ].style.visibility="visible"
	}
else
	{
	vGrp.wApDatosItem = wCSeparacion + wDesviacion + wBorde + CIE
	document.images["imgDatosItemI_" + cod ].style.visibility="hidden"
	document.images["imgDatosItemD_" + cod ].style.visibility="visible"
	}

vGrp.wApOferta = parseFloat(document.getElementById("divOferta" + cod ).style.width)
if (isNaN(vGrp.wApOferta)){
    //copiado de omPrecios
	vGrp.wApOferta = wCSeparacion + wCPU +  wCTotalLinea + wCComentario + wCCantMax + wCAdjunItem + (vGrp.colspanPrecios +1) * (wDesviacion + wBorde + CIE)
	if (vGrp.hayEscalados)
	{   // las columnas de �ltimo precio y objetivo no est�n en el �rea de �tem sino en la de oferta
		// adem�s aparece la columna nueva de Rangos
	    vGrp.wApOferta = vGrp.wApOferta + wCOfertaActual + wCObjetivo + wCRango
	}
}

<%if oProceso.subasta then%>
	if(document.getElementById("divSubasta" + cod )) {
        vGrp.wApSubasta = parseFloat(document.getElementById("divSubasta" + cod ).style.width)
        if (isNaN(vGrp.wApSubasta)){
            //copiado de omSubasta
            vGrp.wApSubasta = wCSeparacion + wCProveedor + wCOfertaMinima + (vGrp.colspanSubasta +1) * (wDesviacion + wBorde)
        }
    }
<%end if%>
<%if oProceso.PedirAlterPrecios then%>
	vGrp.wApMasPrecios = parseFloat(document.getElementById("divMasPrecios" + cod ).style.width)
    if (isNaN(vGrp.wApMasPrecios)){
        //copiado de omMasPrecios
        vGrp.wApMasPrecios = wCSeparacion + wCPU2 + wCTotalLinea2 + wCComentario + wCPU3 + wCTotalLinea3 + wCComentario + (vGrp.colspanMasPrecios + 1)* (wDesviacion + wBorde + CIE)
    }
<%else%>
	vGrp.wApMasPrecios = 0
<%end if%>

if (vGrp.atribsItems.length>0)
	{
	vGrp.wApAtributos = parseFloat(document.getElementById("divAtributos" + cod ).style.width)
	}


document.getElementById("divDatosItem" + cod ).style.width = vGrp.wApDatosItem + 'px';
document.getElementById("divDatosItem" + cod ).style.clip = "rect(0px," + vGrp.wApDatosItem + "px,auto,0px)"
document.getElementById("divDatosItemC" + cod ).style.width = vGrp.wApDatosItem + 'px';
document.getElementById("divDatosItemC" + cod ).style.clip = "rect(0px," + vGrp.wApDatosItem + "px,auto,0px)"

<%if oProceso.Subasta then%>
	if (document.getElementById("divSubasta" + cod )) document.getElementById("divSubasta" + cod ).style.left = vGrp.wApDatosItem + 'px';
	if (document.getElementById("divSubasta" + cod )) document.getElementById("divSubastaC" + cod ).style.left = vGrp.wApDatosItem + 'px';
<%end if%>

document.getElementById("divOferta" + cod ).style.left = vGrp.wApDatosItem + vGrp.wApSubasta + 'px';
document.getElementById("divOfertaC" + cod ).style.left = vGrp.wApDatosItem + vGrp.wApSubasta + 'px';

var calcPx
<%if oProceso.PedirAlterPrecios then%>
    calcPx = vGrp.wApDatosItem  + vGrp.wApSubasta + vGrp.wApOferta
	document.getElementById("divMasPrecios" + cod ).style.left  = calcPx + 'px';
	document.getElementById("divMasPreciosC" + cod ).style.left  = calcPx + 'px';
<%end if%>
if (vGrp.atribsItems.length>0)
	{
    calcPx = vGrp.wApDatosItem + vGrp.wApSubasta + vGrp.wApOferta + vGrp.wApMasPrecios
	document.getElementById("divAtributos" + cod ).style.left =  calcPx + 'px';
	document.getElementById("divAtributosC" + cod ).style.left = calcPx + 'px';
	}
scrollFijas(cod)
}



<%if oProceso.subasta then%>
	function verDetallePujas(e)
	{
		var vGrp
		var i
		var equiv
		var codMon
		var lstMon
		var vItem
		
		if (oMonedas.length==0)
			oMonedas = cargarMonedas()
		var lstMon

		if (document.forms["frmOfertaAEnviar"].lstMonOfe)
			{
			lstMon = document.forms["frmOfertaAEnviar"].lstMonOfe
			codMon = lstMon[lstMon.selectedIndex].value
			equiv = lstMon[lstMon.selectedIndex].equiv
			}
		else
			{
			codMon=gCodMon
			for (i=0;i < oMonedas.length && oMonedas[i].cod!=codMon ;i++)
				{
				}
			equiv = oMonedas[i].equiv
			}

        switch(e.modo)        
        {
            case "1": // Proceso
		        window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/detallepujas.asp?codmon=" + codMon + "&equiv=" + equiv + "&modo=" + e.modo + "&descripcion=" + e.sDescr, "_blank", "top=100,left=150,width=400,height=350,location=no,menubar=no,resizable=yes,scrollbars=no,toolbar=no")
                break;
            case "2":  // Grupo
		        window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/detallepujas.asp?codmon=" + codMon + "&equiv=" + equiv + "&modo=" + e.modo + "&idgrupo=" + e.idgrupo + "&codgrupo=" + e.codgrupo + "&descripcion=" + e.sDescr, "_blank", "top=100,left=150,width=400,height=350,location=no,menubar=no,resizable=yes,scrollbars=no,toolbar=no")
                break;
            case "3": // Item
		        window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/detallepujas.asp?codmon=" + codMon + "&equiv=" + equiv + "&modo=" + e.modo + "&item=" + e.lItem + "&articulo=" + e.sArt + "&descripcion=" + e.sDescr, "_blank", "top=100,left=150,width=400,height=350,location=no,menubar=no,resizable=yes,scrollbars=no,toolbar=no")
        }
	}

    //''' <summary>
    //''' Muestra/oculta el grupo de oferta (subasta)
    //''' </summary>
    //''' <param name="cod">Grupo</param>
    //''' <remarks>Llamada desde: js\cargaritems.asp (2 onclick); Tiempo m�ximo: 0,2</remarks>
    function omSubasta(cod)
	{
	var vGrp
    
	var i
	for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != cod;i++)
		{
		}
	vGrp = proceso.grupos[i]

	if (vGrp.wApSubasta == wCSeparacion + wDesviacion + wBorde)
		{
		vGrp.wApSubasta = wCSeparacion + wCProveedor + wCOfertaMinima + (vGrp.colspanSubasta +1) * (wDesviacion + wBorde)
		document.images["imgSubastaD_" + cod ].style.visibility="hidden"
		document.images["imgSubastaI_" + cod ].style.visibility="visible"
		}
	else
		{
		vGrp.wApSubasta = wCSeparacion + wDesviacion + wBorde
		document.images["imgSubastaD_" + cod ].style.visibility="visible"
		document.images["imgSubastaI_" + cod ].style.visibility="hidden"
		}


	vGrp.wApOferta = parseFloat(document.getElementById("divOferta" + cod ).style.width)
    if (isNaN(vGrp.wApOferta)){
        //copiado de omPrecios
	    vGrp.wApOferta = wCSeparacion + wCPU +  wCTotalLinea + wCComentario + wCCantMax + wCAdjunItem + (vGrp.colspanPrecios +1) * (wDesviacion + wBorde + CIE)
	    if (vGrp.hayEscalados)
	    {   // las columnas de �ltimo precio y objetivo no est�n en el �rea de �tem sino en la de oferta
		    // adem�s aparece la columna nueva de Rangos
	        vGrp.wApOferta = vGrp.wApOferta + wCOfertaActual + wCObjetivo + wCRango
	    }
    }

	vGrp.wApDatosItem = parseFloat(document.getElementById("divDatosItem" + cod ).style.width)
    if (isNaN(vGrp.wApDatosItem)){
        //copiado de omDatosItem
	    vGrp.wApDatosItem = wCSeparacion + wCDestino + wCCantidad + wCUnidad + wCOfertaActual + wCObjetivo + wCFecIni + wCFecFin + wCFormaPago + (vGrp.colspanDatosItem +1)*(wDesviacion + wBorde + CIE)
	    if (vGrp.hayEscalados)
	    { // las columnas de �ltimo precio y objetivo no est�n en el �rea de �tem sino en la de oferta
	        vGrp.wApDatosItem = vGrp.wApDatosItem - wCOfertaActual - wCObjetivo
	    }
    }

	<%if oProceso.PedirAlterPrecios then%>
		vGrp.wApMasPrecios = parseFloat(document.getElementById("divMasPrecios" + cod ).style.width)
        if (isNaN(vGrp.wApMasPrecios)){
            //copiado de omMasPrecios
            vGrp.wApMasPrecios = wCSeparacion + wCPU2 + wCTotalLinea2 + wCComentario + wCPU3 + wCTotalLinea3 + wCComentario + (vGrp.colspanMasPrecios + 1)* (wDesviacion + wBorde + CIE)
        }
	<%else%>
		vGrp.wApMasPrecios = 0
	<%end if%>
	if (vGrp.atribsItems.length>0)
		{
		vGrp.wApAtributos = parseFloat(document.getElementById("divAtributos" + cod ).style.width)
		}

    if (document.getElementById("divSubasta" + cod )){
	    document.getElementById("divSubasta" + cod ).style.width = vGrp.wApSubasta + 'px';
        document.getElementById("divSubasta" + cod ).style.clip = "rect(0px," + vGrp.wApSubasta + "px,auto,0px)"
	    document.getElementById("divSubastaC" + cod ).style.width = vGrp.wApSubasta + 'px';
        document.getElementById("divSubastaC" + cod ).style.clip = "rect(0px," + vGrp.wApSubasta + "px,auto,0px)"
    }

    var calcPx
    calcPx = vGrp.wApDatosItem + vGrp.wApSubasta
	document.getElementById("divOferta" + cod ).style.left = calcPx + 'px'; 
	document.getElementById("divOfertaC" + cod ).style.left = calcPx + 'px';
	<%if oProceso.PedirAlterPrecios then%>
        calcPx = vGrp.wApDatosItem  + vGrp.wApSubasta + vGrp.wApOferta
		document.getElementById("divMasPrecios" + cod ).style.left  = calcPx + 'px'; 
		document.getElementById("divMasPreciosC" + cod ).style.left  = calcPx + 'px';
	<%end if%>
	if (vGrp.atribsItems.length>0)
		{
        calcPx = vGrp.wApDatosItem + vGrp.wApSubasta + vGrp.wApOferta + vGrp.wApMasPrecios
		document.getElementById("divAtributos" + cod ).style.left  = calcPx + 'px'; 
		document.getElementById("divAtributosC" + cod ).style.left  = calcPx + 'px';
		}
	scrollFijas(cod)
	}


<%end if%>



//''' <summary>
//''' Muestra/oculta los precios del grupo de oferta
//''' </summary>
//''' <param name="cod">Grupo</param>
//''' <remarks>Llamada desde: js\cargaritems.asp (2 onclick); Tiempo m�ximo: 0,2</remarks>
function omPrecios(cod)
{
var vGrp
var i
for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != cod;i++)
	{
	}
vGrp = proceso.grupos[i]

if (vGrp.wApOferta == wCSeparacion + wDesviacion + wBorde + CIE)
	{
	vGrp.wApOferta = wCSeparacion + wCPU +  wCTotalLinea + wCComentario + wCCantMax + wCAdjunItem + (vGrp.colspanPrecios +1) * (wDesviacion + wBorde + CIE)
	if (vGrp.hayEscalados)
	{ // las columnas de �ltimo precio y objetivo no est�n en el �rea de �tem sino en la de oferta
		// adem�s aparece la columna nueva de Rangos
	vGrp.wApOferta = vGrp.wApOferta + wCOfertaActual + wCObjetivo + wCRango
	}
	document.images["imgOfertaD_" + cod ].style.visibility="hidden"
	document.images["imgOfertaI_" + cod ].style.visibility="visible"
	}
else
	{
	vGrp.wApOferta = wCSeparacion + wDesviacion + wBorde + CIE
	document.images["imgOfertaD_" + cod ].style.visibility="visible"
	document.images["imgOfertaI_" + cod ].style.visibility="hidden"
	}


<%if oProceso.Subasta then%>
    if (document.getElementById("divSubasta" + cod )){
        vGrp.wApSubasta = parseFloat(document.getElementById("divSubasta" + cod ).style.width)
        if (isNaN(vGrp.wApSubasta)){
            //copiado de omSubasta
            vGrp.wApSubasta = wCSeparacion + wCProveedor + wCOfertaMinima + (vGrp.colspanSubasta +1) * (wDesviacion + wBorde)
        }
    }
<%end if%>

vGrp.wApDatosItem = parseFloat(document.getElementById("divDatosItem" + cod ).style.width)
if (isNaN(vGrp.wApDatosItem)){
    //copiado de omDatosItem
	vGrp.wApDatosItem = wCSeparacion + wCDestino + wCCantidad + wCUnidad + wCOfertaActual + wCObjetivo + wCFecIni + wCFecFin + wCFormaPago + (vGrp.colspanDatosItem +1)*(wDesviacion + wBorde + CIE)
	if (vGrp.hayEscalados)
	{ // las columnas de �ltimo precio y objetivo no est�n en el �rea de �tem sino en la de oferta
	    vGrp.wApDatosItem = vGrp.wApDatosItem - wCOfertaActual - wCObjetivo
	}
}

<%if oProceso.PedirAlterPrecios then%>
	vGrp.wApMasPrecios = parseFloat(document.getElementById("divMasPrecios" + cod ).style.width)
    if (isNaN(vGrp.wApMasPrecios)){
        //copiado de omMasPrecios
        vGrp.wApMasPrecios = wCSeparacion + wCPU2 + wCTotalLinea2 + wCComentario + wCPU3 + wCTotalLinea3 + wCComentario + (vGrp.colspanMasPrecios + 1)* (wDesviacion + wBorde + CIE)
    }
<%else%>
	vGrp.wApMasPrecios = 0
<%end if%>
if (vGrp.atribsItems.length>0)
	{
	vGrp.wApAtributos = parseFloat(document.getElementById("divAtributos" + cod ).style.width)
	}

document.getElementById("divOferta" + cod ).style.width = vGrp.wApOferta + 'px';
document.getElementById("divOferta" + cod ).style.clip = "rect(0px," + vGrp.wApOferta + "px,auto,0px)"
document.getElementById("divOfertaC" + cod ).style.width = vGrp.wApOferta + 'px';
document.getElementById("divOfertaC" + cod ).style.clip = "rect(0px," + vGrp.wApOferta + "px,auto,0px)"

<%if oProceso.Subasta then%>
if (document.getElementById("divSubasta" + cod )) document.getElementById("divSubasta" + cod ).style.left = vGrp.wApDatosItem + 'px';
if (document.getElementById("divSubastaC" + cod )) document.getElementById("divSubastaC" + cod ).style.left = vGrp.wApDatosItem + 'px';
<%end if%>

var calcPx
calcPx = vGrp.wApDatosItem + vGrp.wApSubasta
document.getElementById("divOferta" + cod ).style.left = calcPx + 'px';
document.getElementById("divOfertaC" + cod ).style.left = calcPx + 'px';
<%if oProceso.PedirAlterPrecios then%>
    calcPx = vGrp.wApDatosItem  + vGrp.wApSubasta + vGrp.wApOferta
	document.getElementById("divMasPrecios" + cod ).style.left  =  calcPx + 'px';
	document.getElementById("divMasPreciosC" + cod ).style.left  = calcPx + 'px';
<%end if%>
if (vGrp.atribsItems.length>0)
	{
    calcPx = vGrp.wApDatosItem + vGrp.wApSubasta + vGrp.wApOferta + vGrp.wApMasPrecios
	document.getElementById("divAtributos" + cod ).style.left  = calcPx + 'px'; 
	document.getElementById("divAtributosC" + cod ).style.left  = calcPx + 'px';
	}
scrollFijas(cod)
}

<%if oProceso.PedirAlterPrecios then%>
    //''' <summary>
    //''' Muestra/oculta los precios del grupo de oferta
    //''' </summary>
    //''' <param name="cod">Grupo</param>
    //''' <remarks>Llamada desde: js\cargaritems.asp (2 onclick); Tiempo m�ximo: 0,2</remarks>
	function omMasPrecios(cod)
	{
	var vGrp    
	var i
	for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != cod;i++)
		{
		}
	vGrp = proceso.grupos[i]
	if (vGrp.wApMasPrecios == wCSeparacion + wDesviacion + wBorde + CIE)
		{
		vGrp.wApMasPrecios = wCSeparacion + wCPU2 + wCTotalLinea2 + wCComentario + wCPU3 + wCTotalLinea3 + wCComentario + (vGrp.colspanMasPrecios + 1)* (wDesviacion + wBorde + CIE)
		document.images["imgMasPreciosD_" + cod ].style.visibility="hidden"
		document.images["imgMasPreciosI_" + cod ].style.visibility="visible"
		}
	else
		{
		vGrp.wApMasPrecios = wCSeparacion + wDesviacion + wBorde + CIE
		document.images["imgMasPreciosD_" + cod ].style.visibility="visible"
		document.images["imgMasPreciosI_" + cod ].style.visibility="hidden"
		}

	vGrp.wApOferta = parseFloat(document.getElementById("divOferta" + cod ).style.width)
    if (isNaN(vGrp.wApOferta)){
        //copiado de omPrecios
	    vGrp.wApOferta = wCSeparacion + wCPU +  wCTotalLinea + wCComentario + wCCantMax + wCAdjunItem + (vGrp.colspanPrecios +1) * (wDesviacion + wBorde + CIE)
	    if (vGrp.hayEscalados)
	    {   // las columnas de �ltimo precio y objetivo no est�n en el �rea de �tem sino en la de oferta
		    // adem�s aparece la columna nueva de Rangos
	        vGrp.wApOferta = vGrp.wApOferta + wCOfertaActual + wCObjetivo + wCRango
	    }
    }

	<%if oProceso.Subasta then%>
		if (document.getElementById("divSubasta" + cod )) { 
            vGrp.wApSubasta = parseFloat(document.getElementById("divSubasta" + cod ).style.width)
            if (isNaN(vGrp.wApSubasta)){
                //copiado de omSubasta
                vGrp.wApSubasta = wCSeparacion + wCProveedor + wCOfertaMinima + (vGrp.colspanSubasta +1) * (wDesviacion + wBorde)
            }
        }
	<%end if%>

	vGrp.wApDatosItem = parseFloat(document.getElementById("divDatosItem" + cod ).style.width)
    if (isNaN(vGrp.wApDatosItem)){
        //copiado de omDatosItem
	    vGrp.wApDatosItem = wCSeparacion + wCDestino + wCCantidad + wCUnidad + wCOfertaActual + wCObjetivo + wCFecIni + wCFecFin + wCFormaPago + (vGrp.colspanDatosItem +1)*(wDesviacion + wBorde + CIE)
	    if (vGrp.hayEscalados)
	    { // las columnas de �ltimo precio y objetivo no est�n en el �rea de �tem sino en la de oferta
	        vGrp.wApDatosItem = vGrp.wApDatosItem - wCOfertaActual - wCObjetivo
	    }
    }

	if (vGrp.atribsItems.length>0)
		{
		vGrp.wApAtributos = parseFloat(document.getElementById("divAtributos" + cod ).style.width)
		}

	document.getElementById("divMasPrecios" + cod ).style.width = vGrp.wApMasPrecios + 'px';
    document.getElementById("divMasPrecios" + cod ).style.clip = "rect(0px," + vGrp.wApMasPrecios + "px,auto,0px)"
	document.getElementById("divMasPreciosC" + cod ).style.width = vGrp.wApMasPrecios + 'px';
    document.getElementById("divMasPreciosC" + cod ).style.clip = "rect(0px," + vGrp.wApMasPrecios + "px,auto,0px)"

	<%if oProceso.Subasta then%>
		if (document.getElementById("divSubasta" + cod )) document.getElementById("divSubasta" + cod ).style.left = vGrp.wApDatosItem + 'px';
		if (document.getElementById("divSubastaC" + cod )) document.getElementById("divSubastaC" + cod ).style.left = vGrp.wApDatosItem + 'px';
	<%end if%>

    var calcPx
    calcPx = vGrp.wApDatosItem + vGrp.wApSubasta
	document.getElementById("divOferta" + cod ).style.left = calcPx + 'px'; 
	document.getElementById("divOfertaC" + cod ).style.left = calcPx + 'px';

    calcPx = vGrp.wApDatosItem + vGrp.wApSubasta + vGrp.wApOferta
	document.getElementById("divMasPrecios" + cod ).style.left  = calcPx + 'px'; 
	document.getElementById("divMasPreciosC" + cod ).style.left  = calcPx + 'px';
	if (vGrp.atribsItems.length>0)
		{
        calcPx = vGrp.wApDatosItem + vGrp.wApSubasta + vGrp.wApOferta + vGrp.wApMasPrecios
		document.getElementById("divAtributos" + cod ).style.left  = calcPx + 'px'; 
		document.getElementById("divAtributosC" + cod ).style.left  = calcPx + 'px';
		}

scrollFijas(cod)
	}
<%end if%>


/*'************************************************************
'*** Descripci�n: Obtener el ancho de un determinado atributo       *****
'*** Par�metros de entrada: atrib: el atributo          *****
'*** Par�metros de salida:     *****                    
'***                                               *****                    
'*** Llamada desde: cumpoferta.asp y cargaritems.asp  						*****                    
'** Tiempo m�ximo:   						*****                    
'************************************************************
*/

function obtenAnchoAtributo(atrib)
	{
	var wCAtributo
	
	if (atrib.intro==0)
		switch (atrib.tipo)			
			{
			case 1:
				wCAtributo= wCAtributoT1
				break;
			case 2:
				wCAtributo= wCAtributoT2
				break;
			case 3:
				wCAtributo= wCAtributoT3
				break;
			case 4:
				wCAtributo= wCAtributoT4
				break;
			}
	else
		switch (atrib.tipo)			
			{
			case 1:
				var j
				var maxChars=0
                if (atrib.lista)
				    for (j=0;j<atrib.lista.length;j++)
					    maxChars=atrib.lista[j].length>maxChars?atrib.lista[j].length:maxChars
				wCAtributo= maxChars*8  + 25
				if (wCAtributo < 240)
					wCAtributo = 240

				break;
			case 2:
				wCAtributo= wCAtributoT2
				break;
			case 3:
				wCAtributo= wCAtributoT3
				break;
			case 4:
				wCAtributo= wCAtributoT4
				break;
			}
	
	return(wCAtributo)
	}

//''' <summary>
//''' Muestra/oculta los atributos del grupo de oferta
//''' </summary>
//''' <param name="cod">Grupo</param>
//''' <remarks>Llamada desde: js\cargaritems.asp (2 onclick); Tiempo m�ximo: 0,2</remarks>
function omAtributos(cod)
{
var vGrp

var i
for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != cod;i++)
	{
	}
vGrp = proceso.grupos[i]
if (vGrp.wApAtributos == wCSeparacion + wDesviacion + wBorde + CIE)
	{
	vGrp.wApAtributos = wCSeparacion + (vGrp.colspanAtributos + 1) * (wDesviacion + wBorde + CIE)
	for (k=0;k<vGrp.atribsItems.length;k++)
		{
		vGrp.wApAtributos += obtenAnchoAtributo(vGrp.atribsItems[k])
		}
	
	document.images["imgAtributosD_" + cod ].style.visibility="hidden"
	document.images["imgAtributosI_" +  cod ].style.visibility="visible"
	}
else
	{
	vGrp.wApAtributos = wCSeparacion + wDesviacion + wBorde + CIE
	document.images["imgAtributosD_" + cod ].style.visibility="visible"
	document.images["imgAtributosI_" + cod ].style.visibility="hidden"
	}

<%if oProceso.subasta then%>
	if (document.getElementById("divSubasta" + cod )) {
        vGrp.wApSubasta = parseFloat(document.getElementById("divSubasta" + cod ).style.width)
        if (isNaN(vGrp.wApSubasta)){
            //copiado de omSubasta
            vGrp.wApSubasta = wCSeparacion + wCProveedor + wCOfertaMinima + (vGrp.colspanSubasta +1) * (wDesviacion + wBorde)
        }
    }
<%end if%>

vGrp.wApOferta = parseFloat(document.getElementById("divOferta" + cod ).style.width)
if (isNaN(vGrp.wApOferta)){
    //copiado de omPrecios
	vGrp.wApOferta = wCSeparacion + wCPU +  wCTotalLinea + wCComentario + wCCantMax + wCAdjunItem + (vGrp.colspanPrecios +1) * (wDesviacion + wBorde + CIE)
	if (vGrp.hayEscalados)
	{   // las columnas de �ltimo precio y objetivo no est�n en el �rea de �tem sino en la de oferta
		// adem�s aparece la columna nueva de Rangos
	    vGrp.wApOferta = vGrp.wApOferta + wCOfertaActual + wCObjetivo + wCRango
	}
}

vGrp.wApDatosItem = parseFloat(document.getElementById("divDatosItem" + cod ).style.width)
if (isNaN(vGrp.wApDatosItem)){
    //copiado de omDatosItem
	vGrp.wApDatosItem = wCSeparacion + wCDestino + wCCantidad + wCUnidad + wCOfertaActual + wCObjetivo + wCFecIni + wCFecFin + wCFormaPago + (vGrp.colspanDatosItem +1)*(wDesviacion + wBorde + CIE)
	if (vGrp.hayEscalados)
	{ // las columnas de �ltimo precio y objetivo no est�n en el �rea de �tem sino en la de oferta
	    vGrp.wApDatosItem = vGrp.wApDatosItem - wCOfertaActual - wCObjetivo
	}
}

<%if oProceso.PedirAlterPrecios then%>
	vGrp.wApMasPrecios = parseFloat(document.getElementById("divMasPrecios" + cod ).style.width)
    if (isNaN(vGrp.wApMasPrecios)){
        //copiado de omMasPrecios
        vGrp.wApMasPrecios = wCSeparacion + wCPU2 + wCTotalLinea2 + wCComentario + wCPU3 + wCTotalLinea3 + wCComentario + (vGrp.colspanMasPrecios + 1)* (wDesviacion + wBorde + CIE)
    }
<%else%>
	vGrp.wApMasPrecios = 0
<%end if%>

if (vGrp.atribsItems.length>0)
	{
	document.getElementById("divAtributos" + cod ).style.width = vGrp.wApAtributos + 'px';
    document.getElementById("divAtributos" + cod ).style.clip = "rect(0px," + vGrp.wApAtributos + "px,auto,0px)"
	document.getElementById("divAtributosC" + cod ).style.width = vGrp.wApAtributos + 'px';
    document.getElementById("divAtributosC" + cod ).style.clip = "rect(0px," + vGrp.wApAtributos + "px,auto,0px)"
	}

<%if oProceso.subasta then%>
	if (document.getElementById("divSubasta" + cod )) document.getElementById("divSubasta" + cod ).style.left = vGrp.wApDatosItem + 'px';
	if (document.getElementById("divSubastaC" + cod )) document.getElementById("divSubastaC" + cod ).style.left = vGrp.wApDatosItem + 'px';
<%end if%>

var calcPx
calcPx = vGrp.wApDatosItem + vGrp.wApSubasta
document.getElementById("divOferta" + cod ).style.left = calcPx + 'px'; 
document.getElementById("divOfertaC" + cod ).style.left = calcPx + 'px'; 
<%if oProceso.PedirAlterPrecios then%>
    calcPx = vGrp.wApDatosItem + vGrp.wApOferta + vGrp.wApSubasta
    document.getElementById("divMasPrecios" + cod ).style.left  = calcPx + 'px'; 
    document.getElementById("divMasPreciosC" + cod ).style.left  = calcPx + 'px'; 
<%end if%>
if (vGrp.atribsItems.length>0)
	{
    calcPx =vGrp.wApDatosItem + vGrp.wApOferta + vGrp.wApMasPrecios + vGrp.wApSubasta
	document.getElementById("divAtributos" + cod ).style.left  = calcPx + 'px';
	document.getElementById("divAtributosC" + cod ).style.left  = calcPx + 'px'; 
	}
scrollFijas(cod)
}
/*
''' <summary>
''' Muestra/modifica el comentario
''' </summary>
''' <param name="sInput">input html</param>     
''' <remarks>Llamada desde: js\cargaritems.asp ; Tiempo m�áximo: 0</remarks>*/
function introComentario(sInput,iBloqueado)
	{
    
	window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/comentario.asp?Target=" + sInput+"&Bloqueado="+ iBloqueado,"_blank","top=100,left=150,width=200,height=150,location=no,menubar=no,resizable=yes,scrollbars=no,toolbar=no")
	}


function guardarComentario(donde, elque)
{
imgname = "img" + donde
if (elque=="")
	document.images[imgname].src = "images/comentariovacio.gif"
else
	document.images[imgname].src = "images/comentariolleno.gif"

document.forms["frmOfertaAEnviar"].elements[donde].value = elque
actualizarEstado(1)

}


</script>

<%
'******************************************************************************
'**** PROGRESO 
'******************************************************************************

%>


<script>
var wProgreso

/*
''' <summary>
''' Muestra la barra de progreso mientras se procesa 
''' </summary>
''' <param name="titulo">titulo para la pantalla progreso</param>
''' <remarks>Llamada desde: function introducirPrecios       function modoSubasta; Tiempo máximo: 0</remarks>*/
function dibujarBarra(titulo)
{
	wProgreso = null
	wProgreso = window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/progreso.asp?titulo=<%=server.urlEncode(den(28))%>", "_blank", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
	var acceso = false
    progreso(0);
	// DPD cambios realizados para compatibilidad con IE9
	// el primer acceso a wProgreso.document.getElementById("divProgreso") genera un error de acceso si la ventana an no se ha cargado
    // por eso se hace un segundo bucle controlando el error
	//while (acceso == false) {
	//    try {
	//        while (!wProgreso.document.getElementById("divProgreso")) {
	//        }
	//        acceso = true
	//    }
	//    catch (e) {
    //        acceso =false
	//    }
	//}	

}

function progreso(lPorcent){
	var sTabla
	var i
	var lValor
	var numBlocks
	numBlocks=40

	if (wProgreso==null)
		return
	lValor = lPorcent * numBlocks / 100
	wProgreso.focus()
	lValor = parseInt(lValor.toString())

	sTabla = ""
	sTabla += "<table align = left width = 100% cellspacing=1 cellpadding = 1>"
	sTabla += "	<tr>"
	sTabla += "		<td>"
	sTabla += "			<table width = 100% bgcolor=white>"
	sTabla += "				<tr>"
	for (i=0;i<numBlocks;i++){
		if (i<=lValor)
			sTabla += "					<td bgcolor = <%=cBLOQUEPROGRESO%>>"
		else	
			sTabla += "					<td bgcolor = white>"
		sTabla +="&nbsp;</td>"
    }
	sTabla += "				</tr>"
	sTabla += "			</table>"
	sTabla += "		</td>"
	sTabla += "	</tr>"
	sTabla += "</table>"

	if (lValor<numBlocks && wProgreso.document.getElementById("divProgreso"))
		wProgreso.document.getElementById("divProgreso").innerHTML=sTabla

	if (lValor==numBlocks){
		wProgreso.close()
		bGuardando=false
		wProgreso =null
	}	
}

</script>




<%
'******************************************************************************
'**** ARCHIVOS ADJUNTOS    *****************
'******************************************************************************

dim strFECHAHORA
strFECHAHORA=cstr(year(now())) & cstr(month(now())) & cstr(day(now())) & cstr(hour(now())) & cstr(minute(now())) & cstr(second(now()))

%>
<script>


function resetAdjuntos(){
    var vGrp

    proceso.adjuntos = new Array()
    proceso.adjuntosCargados = false
    proceso.sinAdjuntos=false

    for (j = 0;j<proceso.adjuntos.length;j++){
	    proceso.adjuntos[j].modified = true
	}

    for (i=0;i<proceso.grupos.length;i++){
	    proceso.grupos[i].adjuntosCargados = false
	    proceso.grupos[i].adjuntos = new Array()
	    proceso.grupos[i].sinAdjuntos = false
	}

    for (j= 0;j<arrAdjuntosItem.length;j++){
	    arrAdjuntosItem[j].oItem.adjuntos = new Array()
	    arrAdjuntosItem[j].oItem.adjuntosCargados = false
	    arrAdjuntosItem[j].oItem.sinAdjuntos = false
	}
	
    arrAdjuntosItem = new Array()

    ramaRoot.select()
}

function adjuntosItem(cod,item)
{
var vGrp

var i
for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != cod;i++)
	{
	}
vGrp = proceso.grupos[i]


var vItem

vItem = vGrp.items[item]


vItem.cargarAdjuntos("fraOfertaServer")
}


function guardarAdjuntosGrupo(cod)
{
<%if not bConsulta then%>

	var vGrp

	var i
	for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != cod;i++)
		{
		}
	vGrp = proceso.grupos[i]

	var i


	f = document.forms["frmAdjuntosGrupo" + cod]

	vGrp.obsAdjun = f.txtObsAdjunOfe.value
	for (i = 0;i<vGrp.adjuntos.length;i++)
		{
		if (vGrp.adjuntos[i].deleted==false)
			{
			sNameAdjun = "grupoAdjun_" + cod + "_" + i
			vGrp.adjuntos[i].nombre = eval("f." + sNameAdjun + ".value")
			vGrp.adjuntos[i].comentario = eval("f.com_" + sNameAdjun + ".value")
			}
		}
<%end if%>
}

// <summary>
// acceso a la pantalla de datos de adjuntos desde el men� lateral
// </summary>
// <param name="e">identificador de la pantalla</param>
// <returns></returns>
// <remarks>Llamada desde: config.asp/mostrarConfigProceso; Tiempo m�ximo:0,1</remarks>
// <revision>JVS 16/11/2011</revision>
function introducirAdjuntos(e)
{
var vRama
if (document.getElementById("divApartado").aptdoAnterior)
	eval (document.getElementById("divApartado").aptdoAnterior)

vRama = eval("rama" + e)



dibujarApartado(vRama.text)
resize()

// Pone a false los campos booleanos de control de la pantalla de retorno tras rec�lculo
cambiarFlag()
// Pone a true el campo booleano de introducirAdjuntos de control de la pantalla de retorno tras rec�lculo
bEstamosEnAdjuntos = true
bEstamos="9_" + vRama.cod

var vGrp

var i
for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != vRama.cod;i++)
	{
	}
vGrp = proceso.grupos[i]

document.getElementById("divApartado").aptdoAnterior="guardarAdjuntosGrupo('" + vGrp.cod + "')"
document.getElementById("divApartado").aptdoActual = "mostrarAdjuntosGrupoCod('" + vGrp.cod + "')"

vGrp.cargarAdjuntos("fraOfertaServer")

}


function guardarAdjuntosOferta()
{
<%if not bConsulta then%>

	var i

	f = document.forms["frmAdjuntosProceso"]

	proceso.obsAdjun = f.txtObsAdjunOfe.value

	for (i = 0;i<proceso.adjuntos.length;i++)
		{
		if (proceso.adjuntos[i].deleted==false)
			{
			sNameAdjun = "procAdjun_" + i
			proceso.adjuntos[i].nombre = eval("f." + sNameAdjun + ".value")
			proceso.adjuntos[i].comentario = eval("f.com_" + sNameAdjun + ".value")
			}
		}
<%end if%>
}

// <summary>
// acceso a la pantalla de datos de adjuntos de la oferta desde el men� lateral
// </summary>
// <param name="e">identificador de la pantalla</param>
// <returns></returns>
// <remarks>Llamada desde: config.asp/mostrarConfigProceso; Tiempo m�ximo:0,1</remarks>
// <revision>JVS 16/11/2011</revision>
function adjuntosOferta(e)
{
var vRama
if (document.getElementById("divApartado").aptdoAnterior)
	eval (document.getElementById("divApartado").aptdoAnterior)
vRama = eval("rama" + e)



dibujarApartado(vRama.text)
resize()


// Pone a false los campos booleanos de control de la pantalla de retorno tras rec�lculo
cambiarFlag()
// Pone a true el campo booleano de adjuntosOferta de control de la pantalla de retorno tras rec�lculo
bEstamosEnAdjuntosOferta = true
bEstamos="10"

document.getElementById("divApartado").aptdoAnterior="guardarAdjuntosOferta()"
document.getElementById("divApartado").aptdoActual="mostrarAdjuntos(proceso)"

proceso.cargarAdjuntos("fraOfertaServer")
}



function insertarCelda (TR,sClase,sHTML,vAlign, iRowSpan)
	{
	var oTD
	
	oTD = TR.insertCell(-1)
	oTD.className=sClase

	if (vAlign)
		oTD.style.verticalAlign=vAlign
	
	if (iRowSpan)
		oTD.rowSpan = iRowSpan

    oTD.innerHTML += sHTML

	}



function mostrarAdjuntos(vObj, oTbl)
{
var vClase = "filaImpar"
var oTR
var sNameAdjun
for (i=0;i<vObj.adjuntos.length;i++)
	{
	if (vObj.adjuntos[i].deleted==false)
		{
		if (vObj.proceso) //es el adjunto de un grupo
			{
			sNameAdjun = "grupoAdjun_" + vObj.cod + "_" + i
			}
		else //es adjunto de proceso
			if (vObj.grupo)
				{
				sNameAdjun = "itemAdjun_" + vObj.id + "_" + i
				}
			else
			{
			sNameAdjun = "procAdjun_" + i
			}

		oTR = oTbl.insertRow(-1)
		strAdjun=""
		if (vObj.grupo)
			{
			strAdjun+="<input type=hidden name=index_" + sNameAdjun + " id=index_" + sNameAdjun + " value=" + i + ">"
			<%if bConsulta then%>
				strAdjun+="				" + vObj.adjuntos[i].nombre
			<%else%>
				strAdjun+="<input type=hidden name=" + sNameAdjun + " id=" + sNameAdjun + " value='" + validarText(vObj.adjuntos[i].nombre) + "'>"
                strAdjun+="<label>" + vObj.adjuntos[i].nombre + "</label>"
			<%end if%>
			}
		else
        {
			<%if bConsulta then%>
				strAdjun+="				" + vObj.adjuntos[i].nombre
			<%else%>
                strAdjun+="<input type=hidden name=" + sNameAdjun + " id=" + sNameAdjun + " value='" + validarText(vObj.adjuntos[i].nombre) + "'>"
                strAdjun+="<label>" + vObj.adjuntos[i].nombre + "</label>"
			<%end if%>
		}

		insertarCelda(oTR,vClase,strAdjun,null,null)
				
		strAdjun=""
		dSize = num2str(vObj.adjuntos[i].size / 1024,vdecimalfmt,vthousanfmt,0)
		if (dSize == "0") 
			dSize = "1"
		
		strAdjun+="				<nobr>" + dSize + "&nbsp;Kb.</nobr>"

		insertarCelda(oTR,vClase + " numero",strAdjun,null,null)

		strAdjun=""
		if (vObj.grupo)
			<%if bConsulta then%>
				strAdjun+=vObj.adjuntos[i].comentario
			<%else%>
				strAdjun+="				" + inputTexto("com_" + sNameAdjun,"100%",500,vObj.adjuntos[i].comentario,"modificarAdjuntoItem(\"" + sNameAdjun + "\")")
			<%end if%>
		else
			<%if bConsulta then%>
				strAdjun+=vObj.adjuntos[i].comentario
			<%else%>
				strAdjun+="				" + inputTexto("com_" + sNameAdjun,"100%",500,vObj.adjuntos[i].comentario,"actualizarEstado(1)")
			<%end if%>
				
		insertarCelda(oTR,vClase,strAdjun,null,null)
		strAdjun=""

		if(vObj.proceso)
			{
			strAdjun+="				<a href='javascript:void(null)' onclick='downloadAdjunto(" + i + ",\"" + vObj.cod + "\")'><img name=imgAbrir_" + i + " id=imgAbrir_" + i + "  border=0 SRC=images/abrir.GIF></a>"
			}
		else 
			if (vObj.grupo)
				{
				strAdjun+="				<a href='javascript:void(null)' onclick='downloadAdjunto(" + i + ",\"" + vObj.grupo.cod + "\"," + vObj.id + ")'><img name=imgAbrir_" + i + " id=imgAbrir_" + i + "   border=0 SRC=images/abrir.GIF></a>"
				}
			else
				{
				strAdjun+="				<a href='javascript:void(null)' onclick='downloadAdjunto(" + i + ")'><img name=imgAbrir_" + i + " id=imgAbrir_" + i + "   border=0 SRC=images/abrir.GIF></a>"
				}

		insertarCelda(oTR,vClase,strAdjun,null,null)
		
		if (vObj.grupo)
			wAdjunItem.document.images["imgAbrir_" + i].src = imgAbrir.src		
		else
			document.images["imgAbrir_" + i].src = imgAbrir.src		

		strAdjun=""

		if(vObj.proceso)
			{
			strAdjun+="				<a href='javascript:void(null)' onclick='eliminaAdjunto(" + i + ",\"" + vObj.cod + "\")'><img  name=imgEliminar_" + i + " id=imgEliminar_" + i + " border=0 SRC=images/elimadjunto.GIF></a>"
			}
		else 
			if (vObj.grupo)
				{
				strAdjun+="				<a href='javascript:void(null)' onclick='eliminaAdjunto(" + i + ",\"" + vObj.grupo.cod + "\"," + vObj.id + ")'><img  name=imgEliminar_" + i + " id=imgEliminar_" + i + " border=0 SRC=images/elimadjunto.GIF></a>"
				}
			else
				{
				strAdjun+="				<a href='javascript:void(null)' onclick='eliminaAdjunto(" + i + ")'><img  name=imgEliminar_" + i + " id=imgEliminar_" + i + " border=0 SRC=images/elimadjunto.GIF></a>"
				}

				

		insertarCelda(oTR,vClase,strAdjun,null,null)


		if (vObj.grupo)
			wAdjunItem.document.images["imgEliminar_" + i].src = imgEliminar.src
		else
			document.images["imgEliminar_" + i].src = imgEliminar.src
				
		if (vClase == "filaImpar")
			vClase = "filaPar"
		else
			vClase = "filaImpar"
		}
	}

}

function mostrarTablaAdjuntos(vObj)
{
var strAdjun
var i
var j

if (vObj.proceso) //es el adjunto de un grupo
	{
	sNameTable = "tblAdjun_" + vObj.cod
	}
else //es adjunto de proceso
	if (vObj.grupo)
		{
		sNameTable = "tblAdjun_" + vObj.id
		}
	else
	{
	sNameTable = "tblAdjun"
	}

vObj.adjunTableName = sNameTable

strAdjun = "<form name=frmAdjuntos id=frmAdjuntos>"
strAdjun+="	<table name=" + sNameTable + " id=" + sNameTable + "   style='width:100%;'>"
strAdjun+="		<tr>"
strAdjun+="			<td class='cabecera'>"
strAdjun+="				<nobr><%=JSText(den(22))%></nobr>"
strAdjun+="			</td>"
strAdjun+="			<td class='cabecera'>"
strAdjun+="				<nobr><%=JSText(den(23))%></nobr>"
strAdjun+="			</td>"
strAdjun+="			<td class='cabecera'>"
strAdjun+="				<nobr><%=JSText(den(24))%></nobr>"
strAdjun+="			</td>"
strAdjun+="			<td class='cabecera'>"
strAdjun+="				<nobr>&nbsp;</nobr>"
strAdjun+="			</td>"
strAdjun+="			<td class='cabecera'>"
strAdjun+="				<nobr>&nbsp;</nobr>"
strAdjun+="			</td>"
strAdjun+="		</tr>"
strAdjun+="	</table></form>"
return (strAdjun)
}


function mostrarAdjuntosProceso()
{
var str
var sTabla
objAdjuntando = proceso
sObservaciones = "<TEXTAREA cols=80 rows=8 name=txtObsAdjunOfe id=txtObsAdjunOfe onchange='modificarObsAdjun(this.value)'>" + (proceso.obsAdjun==null?"":proceso.obsAdjun) + "</TEXTAREA>"
str = "<form name=frmAdjuntosProceso id=frmAdjuntosProceso>"
str+= "	<table width=100%>"
str+= "		<tr>"
str+= "			<td>" 
str+= "				<table>"
str+= "					<tr>"	
str+= "						<td>" + cajetin('<%=JSText(den(20))%>',sObservaciones,"width:100%") + "</td>"
str+= "					</tr>"
str+= "				</table>"
str+= "			</td>"
str+= "		</tr>"
str+= "		<tr>"
str+= "			<td>" 
str+= "				<table width=100%>"
str+= "					<tr>"	
str+= "						<td>" + cajetin('<%=JSText(den(25))%>',mostrarTablaAdjuntos(proceso),"width:100%") + "</td>"
str+= "					</tr>"
str+= "				</table>"
str+= "			</td>"
str+= "		</tr>"
str+= "		<tr>"
str+= "			<td align = center>" 
str+= "<input type=button class=button value='<%=JSText(den(26))%>' onclick='anyadirAdjunto()'>"
str+= "			</td>"
str+= "		</tr>"
str+= "	</table>"
str+= "	</form>"
document.getElementById("divApartadoS").innerHTML = str

oTabla = document.getElementById(proceso.adjunTableName)

mostrarAdjuntos(proceso,oTabla)


}

var vTmpComentario 
var vTmpNombre 

var objAdjuntando
/*
''' <summary>
''' Adjunta un fichero
''' </summary>    
''' <remarks>Llamada desde: function anyadirAdjunto ; Tiempo m�áximo: 0</remarks>*/
function adjuntar()
	{
	var winAdjun		
	kbActual=0
	winAdjun=window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/upload.asp?Idioma=<%=Idioma%>&TargetURL=<%=Server.URLEncode(strFECHAHORA)%>&kbACTUAL=" + proceso.totalBytes ,"_blank","top=100,left=150,width=410,height=250,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
	winAdjun.focus()
	}


function anyadirAdjunto(cod,item)
{
if (!cod)
	{
	objAdjuntando = proceso
	}
else
	{
	var i
	for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != cod;i++)
		{
		}
	if (!item)
		{
		objAdjuntando = proceso.grupos[i]
		}
	else
		{
		var j
		for (j=0;j<proceso.grupos[i].items.length && proceso.grupos[i].items[j].id != item;j++)
			{
			}
		objAdjuntando = proceso.grupos[i].items[j]
		}
	}
adjuntar()
}

var arrAdjuntosItem 

arrAdjuntosItem = new Array()


function uploadRealizado(iSize,idPortal)
{
var adjun
if (vTmpNombre=="")
	return 
	
adjun = new Adjunto(null,vTmpNombre,vTmpComentario,iSize,idPortal,true)
objAdjuntando.anyaAdjunto(adjun)
proceso.totalBytes += iSize
vTmpNombre=""
vTmpComentario=""
objAdjuntando.cargarAdjuntos()
if (objAdjuntando.grupo)
	{
	actualizarNumAdjuntos (objAdjuntando)
    actualizarTextoAdjuntos (objAdjuntando)
	}
actualizarEstado(1)
}



function modificarObsAdjun(obs)
{
objAdjuntando.obsAdjun=obs
if (objAdjuntando.grupo)
	{
	document.forms["frmOfertaAEnviar"].elements["txtObsAdjunItem_" + objAdjuntando.id].value = obs
	}
actualizarEstado(1)	
}


function modificarAdjuntoItem(nombre, comentario, adjun)

{
objAdjuntando.adjuntos[adjun].nombre=nombre
objAdjuntando.adjuntos[adjun].comentario=comentario
actualizarEstado(1)
}

/*
''' <summary>
''' Obtiene el grupo e invoca a mostrarAdjuntosGrupo
''' </summary>
''' <param name="codGrp">Cdigo de grupo</param>
''' <returns></returns>
''' <remarks>Llamada desde: cumpoferta.asp/introducirAdjuntos; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 16/11/2011</revision>
*/
function mostrarAdjuntosGrupoCod(codGrp)
{
    var i
    var vGrp

    for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != codGrp;i++)
		{
		}
    vGrp = proceso.grupos[i]
    mostrarAdjuntosGrupo(vGrp)
}

/*
''' <summary>
''' Muestra la pantalla de adjuntos de grupo
''' </summary>
''' <param name="vGrp">objeto grupo</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cumpoferta.asp/mostrarAdjuntosGrupoCod; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 16/11/2011</revision>
*/
function mostrarAdjuntosGrupo(vGrp)
{

var str
var sTabla
objAdjuntando=vGrp
if (vGrp.Bloqueado != 1)
{
    sObservaciones = "<TEXTAREA cols=80 rows=8 name=txtObsAdjunOfe id=txtObsAdjunOfe onchange='modificarObsAdjun(this.value)'>" +(vGrp.obsAdjun==null?"":vGrp.obsAdjun) + "</TEXTAREA>"
}
else
{
    sObservaciones = "<TEXTAREA readonly cols=80 rows=8 name=txtObsAdjunOfe id=txtObsAdjunOfe onchange='modificarObsAdjun(this.value)'>" +(vGrp.obsAdjun==null?"":vGrp.obsAdjun) + "</TEXTAREA>"
}
str = "<form name=frmAdjuntosGrupo" + vGrp.cod + " id=frmAdjuntosGrupo" + vGrp.cod + ">"
str+= "  <input type=\"hidden\" name=\"grupo\" id=\"grupo\" value='" + vGrp.cod + "'>"
str+= "	<table width=100%>"
str+= "		<tr>"
str+= "			<td>" 
str+= "				<table>"
str+= "					<tr>"	
str+= "						<td>" + cajetin('<%=JSText(den(20))%>',sObservaciones,"width:100%") + "</td>"
str+= "					</tr>"
str+= "				</table>"
str+= "			</td>"
str+= "		</tr>"
str+= "		<tr>"
str+= "			<td>" 
str+= "				<table width=100%>"
str+= "					<tr>"	
str+= "						<td>" + cajetin('<%=JSText(den(25))%>',mostrarTablaAdjuntos(vGrp),"width:100%") + "</td>"
str+= "					</tr>"
str+= "				</table>"
str+= "			</td>"
str+= "		</tr>"
str+= "		<tr>"
str+= "			<td align = center>" 
                    if (vGrp.Bloqueado != 1)
                    {
str+= "<input type=button class=button value='<%=JSText(den(26))%>' onclick='anyadirAdjunto(\"" + vGrp.cod +"\")' >"
                    }
str+= "			</td>"
str+= "		</tr>"
str+= "	</table>"
str+= "	</form>"

document.getElementById("divApartadoS").innerHTML = str

oTabla = document.getElementById(vGrp.adjunTableName)

mostrarAdjuntos(vGrp,oTabla)


}
var wAdjunItem

function mostrarAdjuntosItem(vItem)
{
    if (!wAdjunItem)
	{
	    wAdjunItem = window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/adjuntosItem.asp","_blank","top=100,left=150,width=600,height=350,location=no,menubar=no,resizable=yes,scrollbars=yes,toolbar=no")
        //Para que le de tiempo a cargar del todo la página le pongo un pequeño timeout (Chrome y Safari no la cargaban a tiempo)
        setTimeout(function(){RellenarAdjuntosItem(wAdjunItem ,ComunAdjuntosItemRellenar(vItem),vItem)},500)
    }
}

function DesdeAdjuntosItemRellenar(vItem)
{
    RellenarAdjuntosItem(wAdjunItem ,ComunAdjuntosItemRellenar(vItem),vItem)
}

function ComunAdjuntosItemRellenar(vItem)
{
    var str
    var sTabla

    if (vItem.grupo.Bloqueado != 1)
    {
        sObservaciones = "<TEXTAREA cols=80 rows=8 name=txtObsAdjunOfe id=txtObsAdjunOfe onchange='modificarObsAdjun(this.value)'>" + (vItem.obsAdjun==null?"":vItem.obsAdjun) + "</TEXTAREA>"
    }
    else
    {
        sObservaciones = "<TEXTAREA readonly cols=80 rows=8 name=txtObsAdjunOfe id=txtObsAdjunOfe onchange='modificarObsAdjun(this.value)'>" + (vItem.obsAdjun==null?"":vItem.obsAdjun) + "</TEXTAREA>"
    }
    str = ""
    str+= "	<table width=100%>"
    str+= "		<tr>"
    str+= "			<td colspan =2>" 
    str+= "				<table>"
    str+= "					<tr>"	
    str+= "						<td>" + cajetin('<%=JSText(den(20))%>',sObservaciones,"width:100%") + "</td>"
    str+= "					</tr>"
    str+= "				</table>"
    str+= "			</td>"
    str+= "		</tr>"
    str+= "		<tr>"
    str+= "			<td colspan =2>" 
    str+= "				<table width=100%>"
    str+= "					<tr>"	
    str+= "						<td>" + cajetin('<%=JSText(den(25))%>',mostrarTablaAdjuntos(vItem),"width:100%") + "</td>"
    str+= "					</tr>"
    str+= "				</table>"
    str+= "			</td>"
    str+= "		</tr>"
    str+= "		<tr>"
    str+= "			<td align = center>" 
    if (vItem.grupo.Bloqueado != 1)
    {
        str+= "<input type=button class=button value='<%=JSText(den(26))%>' onclick='p.anyadirAdjunto(\"" + vItem.grupo.cod +"\"," + vItem.id + ")' >"
    }
    str+= "			</td>"
    str+= "			<td align = center>" 
    str+= "<input type=button class=button value='<%=JSText(den(27))%>' onclick='window.close()' >"
    str+= "			</td>"
    str+= "		</tr>"
    str+= "	</table>"

    return str
}


function RellenarAdjuntosItem(wAdjunItem,str,vItem){
    wAdjunItem.document.getElementById("divAdjuntosItem").innerHTML =str

    oTabla = wAdjunItem.document.getElementById(vItem.adjunTableName)

    mostrarAdjuntos(vItem,oTabla)
    wAdjunItem.focus()
}

var sPosibleEliminar
function downloadAdjunto(adjun, grupo, item)
{
sPosibleEliminar = "eliminaAdjunto(" + adjun + "," + (grupo!=null?"'" + grupo + "'":"null") + "," + item + ")"
if (item ==null) 
	{
	if (grupo == null)
		{

		proceso.adjuntos[adjun].download("fraOfertaServer")
		}
	else
		{
		var i

		for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != grupo ;i++)
			{
			}

		proceso.grupos[i].adjuntos[adjun].download("fraOfertaServer")
		}
	}
else
	{
	var i

	for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != grupo ;i++)
		{
		}
	var j
	for (j=0;i<proceso.grupos[i].items.length && proceso.grupos[i].items[j].id != item ;j++)
		{
		}
	proceso.grupos[i].items[j].adjuntos[adjun].download("fraOfertaServer")
	
	//para el item	
	}


}

function eliminaAdjunto(adjun, grupo, item)
{
if (confirm("<%=JSText(den(33))%>")==false)
	return

if (item ==null) 
	{
	if (grupo == null)
		{

		proceso.adjuntos[adjun].deleted=true
		proceso.totalBytes -= proceso.adjuntos[adjun].size
		proceso.cargarAdjuntos()

		}
	else
		{
		var i

		for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != grupo ;i++)
			{
			}

		proceso.totalBytes -= proceso.grupos[i].adjuntos[adjun].size
		proceso.grupos[i].adjuntos[adjun].deleted=true
		proceso.grupos[i].cargarAdjuntos()
		}
	}
else
	{
	//para el item	
	var i

	for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != grupo ;i++)
		{
		}
	var j
	for (j=0;j<proceso.grupos[i].items.length && proceso.grupos[i].items[j].id != item ;j++)
		{
		}

	var k

	proceso.totalBytes -= proceso.grupos[i].items[j].adjuntos[adjun].size
	proceso.grupos[i].items[j].adjuntos[adjun].deleted=true
	proceso.grupos[i].items[j].numAdjuntos--
	actualizarNumAdjuntos (proceso.grupos[i].items[j])
	proceso.grupos[i].items[j].cargarAdjuntos()
	l=arrAdjuntosItem.length
	arrAdjuntosItem[l] = new ItemAdjunto(proceso.grupos[i].items[j],proceso.grupos[i].items[j].adjuntos[adjun])
	wAdjunItem.focus()
	}

actualizarEstado(1)
}

function actualizarNumAdjuntos(item)
{
var str
str = ""
var j

for (j=0;item.id!=item.grupo.items[j].id;j++)
{
}
var obs = document.forms["frmOfertaAEnviar"].elements["txtObsAdjunItem_" + item.id].value
str += "<input type=hidden name=txtObsAdjunItem_" + item.id + " id=txtObsAdjunItem_" + item.id + ">"
str += "<a href='javascript:void(null)' onclick='adjuntosItem(\"" + item.grupo.cod + "\"," + j +")'>"
str += (item.numAdjuntos>0 ? item.numAdjuntos : "&nbsp;") + "&nbsp;"
str += "<IMG border=0 SRC=images/clip.gif>"
str += "</a>"

document.getElementById("divItemAdjun_" + item.id).innerHTML = str
document.forms["frmOfertaAEnviar"].elements["txtObsAdjunItem_" + item.id].value = obs

}

function actualizarTextoAdjuntos(item)
{
    wAdjunItem.DesdeAdjuntosItemRellenar(item)
}


</script>



<%

'******************************************************************************
'**** ALMACENAR OFERTA   *****************
'******************************************************************************


%>


<script>


/*
''' <summary>
''' Almacenar Oferta enviandola o solo guardandola. Exige que la subasta tenga fecha de validez y precios en todos los 
'''	items. Si no es subasta deja guardar sin fecha validez pero no enviar.
'''	Si en una subasta quedan 20 segundos y retrasas el reloj del equipo 30 segundos el contador dice q te quedan 50sg
'''	lo q no es verdad, eso se controla cada vez q pujas, NO traga aunque saque la pagina de espera. De momento as pq
''' esto va a cambiar.
''' </summary>
''' <param name="modoEnvio">Indica que tipo de grabaci�n es: 1-guardar, 2-enviar, 3-puja?,4-guardaryrecalcular, 5- guardar+recalcular+enviar</param>  
''' <param name="recalc">Indica si hay que recalcular despu�s de guardar</param>  
''' <returns>Nada</returns>
''' <remarks>Llamada desde: btnPujar imgGuardar; Tiempo m�ximo:0</remarks>
*/
function prepararOfertaAEnviar(modoEnvio, recalc)
{
if (<%=application("FSAL")%> == 1)
{
fechaIniGuardarOferta=devolverUTCFechaFSAL();  //se registra la fecha FICLIPREPAR al inicio de la ejecucion
sIdRegistroGuardarOferta=devolverIdRegistroFSAL(); 
}
var j 
j=document.forms["frmOfertaAEnviar"]
if (<%=application("FSAL")%> == 1)
{
j.fechaIniFSAL7.value = fechaIniGuardarOferta; //pasar fecha inicio
j.sIdRegistroFSAL7.value = sIdRegistroGuardarOferta; //pasar idregistro inicio
}

bGuardando=true
if (document.getElementById("divApartado").aptdoAnterior && !bEstamosEnProceso) {
	eval (document.getElementById("divApartado").aptdoAnterior)
}

if(!recalc)
    recalc=0



if(modoEnvio>1)
	{
	
	
<%if oProceso.Subasta then%>
	//Comprobar que no ha tocado el reloj de su puesto para q no llege a 0
	


<% select case oProceso.modo%>	
<%case 1  %> //proceso completo
	//Comprobar de que no se introducen items sin precios en la puja
	for (i=0;i<itemsPuja.length;i++)
	{
		if (itemsPuja[i].precioNeto == null) {
			alert("<%=JSAlertText(den(87))%>")
			return false
		}
	}
<%case 2 %> // por grupos
    // comprobar que se puja por grupos completos
        var Matriz=new Array(proceso.grupos.length);
        for(i=0;i<=proceso.grupos.length-1;i++)
        {
        Matriz[i]=new Array(3);
        Matriz[i][0] = proceso.grupos[i].cod
        Matriz[i][1] = proceso.grupos[i].numItems
        Matriz[i][2] = 0
        }
    var grupo
	for (i=0;i<itemsPuja.length;i++)
	{

        for (g=0;g<=Matriz.length-1;g++)
        {
            if(Matriz[g][0] == itemsPuja[i].grupo) grupo = g
        }

		if (itemsPuja[i].precioNeto != null) {
            Matriz[grupo][2]++
		}
	}

    var algunGrupoIncompleto = false
    var algunGrupoCompleto = false

        for (g=0;g<=Matriz.length-1;g++)
        {
            if (Matriz[g][1] != Matriz[g][2])
            {
                
                if (Matriz[g][2]!=0) algunGrupoIncompleto = true
            }
            else
            {
                algunGrupoCompleto = true
            }
        }


    if (algunGrupoIncompleto)
    {
        // 
     alert("<%=JSAlertText(den(139))%>")
     return false
    }

    if (!algunGrupoCompleto)
    {
     alert("<%=JSAlertText(den(140))%>")
     return false
    }

    

<%case 3 %> // por items
    // Comprobar que al menos se puje por uno de los items
    var ningunapuja = true
	for (i=0;i<itemsPuja.length;i++)
	{
		if (itemsPuja[i].precioNeto != null) {
            ningunapuja = false
		}
	}
    if (ningunapuja)
    {
	    alert("<%=JSAlertText(den(141))%>")
	    return false
    }



<%end select%>

<%end if%>
	
	
	
	<%if not oProceso.adminpublica then%>
		
		if (proceso.fecValidez==null)
			{
			if (modoEnvio==3)
				{
				window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/fecha.asp","_blank","top=100,left=150,width=300,height=100,location=no,menubar=no,resizable=yes,scrollbars=yes,toolbar=no")
				return
				}
			
			alert("<%=JSAlertText(den(42))%>\n<%=JSAlertText(den(43))%>")
			return false
			}
	<%else%>
		if (bPedirFecha)
			{
			window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/fecha.asp?fecha=" + date2str(proceso.fecValidez,vdatefmt),"_blank","top=100,left=150,width=300,height=100,location=no,menubar=no,resizable=yes,scrollbars=yes,toolbar=no")
			return
			}
		bPedirFecha=true
	<%end if%>



	hoy = new Date()
	intro=proceso.fecValidez
	intro.setHours(hoy.getHours(),hoy.getMinutes(),hoy.getSeconds(),hoy.getMilliseconds())
	if (intro<hoy)
		{
		alert("<%=JSText(den(82))%> " + date2str(hoy,vdatefmt) )
		return false
		}

	}
dFechaA=datehour2str(dFecActOferta,vdatefmt)	

document.getElementById("divOfertaAEnviar").innerHTML = ""
f = document.forms["frmOfertaAEnviar"]
str="<INPUT type = hidden name=modoEnvio id=modoEnvio value=" + modoEnvio.toString() + ">"
str+="<INPUT type = hidden name=recalc id=recalc value=" + recalc.toString() + ">"
str+="<INPUT type = hidden name=fecact id=fecact value='" + dFechaA.toString() + "'>"
str+="<INPUT type = hidden name=estamos id=estamos value=" + bEstamos.toString() + ">"
document.getElementById("divOfertaAEnviar").innerHTML += str

<%if not oProceso.adminpublica then%>
if (modoEnvio>1)
	{
	if (modoEnvio==2)
		winEspera = window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/winEspera.asp?msg=<%=server.urlEncode(den(64))%>&continua=continuarPrepararOferta", "winEspera", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=yes,scrollbars=no,toolbar=no")
	else
		winEspera = window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/winEspera.asp?msg=<%=server.urlEncode(den(30))%>&msg2=<%=den(65)%>&continua=continuarPrepararOferta", "winEspera", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=yes,scrollbars=no,toolbar=no")
	while (winEspera.document.images["imgReloj"])
		{
		}
	}
else
<%end if%>
	{

	winEspera = window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/winEspera.asp?msg=<%=server.urlEncode(den(29))%>&continua=continuarPrepararOferta", "winEspera", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=yes,scrollbars=no,toolbar=no")
	while (winEspera.document.images["imgReloj"])
		{
		}
	}

winEspera.resizeTo (400,120)
winEspera.focus()

}

function continuarPrepararOferta()
{
var t
var vprecisionfmt // Variable local para la funci�n que crea los inputs mediante los que se env�a la oferta.
vprecisionfmt = 1000

sInputFechaValidez = "<INPUT type=hidden name=ofeFechaValidez id=ofeFechaValidez>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputFechaValidez
f.ofeFechaValidez.value = date2str(proceso.fecValidez,vdatefmt)
	
sInputMonedaOferta = "<INPUT type=hidden name=ofeMoneda id=ofeMoneda>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputMonedaOferta
f.ofeMoneda.value=proceso.mon.cod

sInputCambioOferta = "<INPUT type=hidden name=ofeCambio id=ofeCambio>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputCambioOferta
if (proceso.mon.cod=="<%=JSText(oProceso.MonCod)%>")
	f.ofeCambio.value=1
else
	f.ofeCambio.value=proceso.mon.equiv/<%=JSNum(oProceso.cambio)%>

sInputObservaciones = "<INPUT type=hidden name=ofeObs id=ofeObs>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputObservaciones
f.ofeObs.value=proceso.obs

sInputEstadoOferta = "<INPUT type=hidden name=ofeEstado id=ofeEstado>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputEstadoOferta
f.ofeEstado.value=proceso.estado


var i
var j
var numAtribs
numAtribs =0
if (proceso.atribs.length>0)
	{
	sInputModifAtribsOferta = "<INPUT type=hidden name=ofeModifAtribs id=ofeModifAtribs value = 1>"
    document.getElementById("divOfertaAEnviar").innerHTML += sInputModifAtribsOferta
	for (i=0;i<proceso.atribs.length;i++)
		{
		if(proceso.atribs[i].tipo==4)
			bEntra = proceso.atribs[i].valor==null ? false  : true
		else
			bEntra=true			

		if (bEntra)
			{		
			sNameAtrib = "ofeprocAtrib_" + proceso.atribs[i].paid
			sInputAtribX = "<INPUT type=hidden name=ofeProcInputAtrib" + i + " id=ofeProcInputAtrib" + i + " value='" + sNameAtrib + "'>"
			sInputAtribX += "<INPUT type=hidden name=tipo" + sNameAtrib + " id=tipo" + sNameAtrib + ">"
			sInputAtribX += "<INPUT type=hidden name=" + sNameAtrib + " id=" + sNameAtrib + ">"
            document.getElementById("divOfertaAEnviar").innerHTML += sInputAtribX
		
			t = eval("f." +  sNameAtrib )
			switch (proceso.atribs[i].tipo)
				{
				case 1:
					t.value = proceso.atribs[i].valor
					break
				case 2:
					t.value = num2str(proceso.atribs[i].valor,vdecimalfmt,vthousanfmt,1000)
					break
				case 3:
					t.value = date2str(proceso.atribs[i].valor,vdatefmt)
					break
				case 4:
					if (proceso.atribs[i].valor)
						t.value = "1"
					else 
						if (proceso.atribs[i].valor==false)
							t.value = "0"
						else
							t.value = "null"
					break
				}
			t = eval("f.tipo" +  sNameAtrib )
			t.value = proceso.atribs[i].tipo
			numAtribs++
			}
		}	
	}

sInputNumProceAtribs = "<INPUT type=hidden name=ofeProcNumAtribs id=ofeProcNumAtribs>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputNumProceAtribs
f.ofeProcNumAtribs.value = proceso.atribs.length

//JVS guardar estructuras de costes / descuentos (precio item, total item, total grupo, total oferta) para comparar con GS
var indiceCostesDesc
numCostesDescuentos = 0
for (indiceCostesDesc=0;indiceCostesDesc<proceso.CostesDescTotalOferta.length;indiceCostesDesc++)
{
            vCostesDesc = proceso.CostesDescTotalOferta[indiceCostesDesc]
            sNameCosteDesc = "ofeprocCostesDescTotOferta_" + vCostesDesc.paid
			sInputCosteDescX = "<INPUT type=hidden name=ofeProcInputCosteDescTotOferta" + indiceCostesDesc + " id=ofeProcInputCosteDescTotOferta" + indiceCostesDesc + " value='" + sNameCosteDesc + "'>"
			sInputCosteDescX += "<INPUT type=hidden name=id" + sNameCosteDesc + " id=id" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=paid" + sNameCosteDesc + " id=paid" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=cod" + sNameCosteDesc + " id=cod" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=den" + sNameCosteDesc + " id=den" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=intro" + sNameCosteDesc + " id=intro" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=tipo" + sNameCosteDesc + " id=tipo" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=valor" + sNameCosteDesc + " id=valor" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=obligatorio" + sNameCosteDesc + " id=obligatorio" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=valorMin" + sNameCosteDesc + " id=valorMin" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=valorMax" + sNameCosteDesc + " id=valorMax" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=lista" + sNameCosteDesc + " id=lista" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=operacion" + sNameCosteDesc + " id=operacion" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=aplicar" + sNameCosteDesc + " id=aplicar" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=ambito" + sNameCosteDesc + " id=ambito" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=importeParcial" + sNameCosteDesc + " id=importeParcial" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=alcance" + sNameCosteDesc + " id=alcance" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=numGrupos" + sNameCosteDesc + " id=numGrupos" + sNameCosteDesc + ">"
            document.getElementById("divOfertaAEnviar").innerHTML += sInputCosteDescX
		
            t = eval("f.id" +  sNameCosteDesc)
			t.value = vCostesDesc.id
            t = eval("f.paid" +  sNameCosteDesc)
			t.value = vCostesDesc.paid
            t = eval("f.cod" +  sNameCosteDesc)
			t.value = vCostesDesc.cod
            t = eval("f.den" +  sNameCosteDesc)
			t.value = vCostesDesc.den
			t = eval("f.intro" +  sNameCosteDesc)
			t.value = vCostesDesc.intro
			t = eval("f.tipo" +  sNameCosteDesc)
			t.value = vCostesDesc.tipo
            t = eval("f.valor" +  sNameCosteDesc)
			t.value = vCostesDesc.valor
            t = eval("f.obligatorio" +  sNameCosteDesc )
			t.value = vCostesDesc.obligatorio
            t = eval("f.valorMin" +  sNameCosteDesc)
			t.value = vCostesDesc.valorMin
            t = eval("f.valorMax" +  sNameCosteDesc)
			t.value = vCostesDesc.valorMax
            t = eval("f.lista" +  sNameCosteDesc)
			t.value = vCostesDesc.lista
            t = eval("f.operacion" +  sNameCosteDesc)
			t.value = vCostesDesc.operacion
            t = eval("f.aplicar" +  sNameCosteDesc)
			t.value = vCostesDesc.aplicar
            t = eval("f.ambito" +  sNameCosteDesc)
			t.value = vCostesDesc.ambito
            t = eval("f.importeParcial" +  sNameCosteDesc)
			t.value = vCostesDesc.importeParcial
            t = eval("f.alcance" +  sNameCosteDesc)
			t.value = vCostesDesc.alcance
			numCostesDescuentos++
}

sInputNumProceCostesDescTotOferta = "<INPUT type=hidden name=ofeProcNumCostesDescTotOferta id=ofeProcNumCostesDescTotOferta>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputNumProceCostesDescTotOferta
f.ofeProcNumCostesDescTotOferta.value = numCostesDescuentos

numCostesDescuentos = 0
var numGrupos = 0

for (indiceCostesDesc=0;indiceCostesDesc<proceso.CostesDescTotalGrupo.length;indiceCostesDesc++)
{
            vCostesDesc = proceso.CostesDescTotalGrupo[indiceCostesDesc]
            sNameCosteDesc = "ofeprocCostesDescTotGrupo_" + vCostesDesc.paid
			sInputCosteDescX = "<INPUT type=hidden name=ofeProcInputCosteDescTotGrupo" + indiceCostesDesc + " id=ofeProcInputCosteDescTotGrupo" + indiceCostesDesc + " value='" + sNameCosteDesc + "'>"
			sInputCosteDescX += "<INPUT type=hidden name=id" + sNameCosteDesc + " id=id" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=paid" + sNameCosteDesc + " id=paid" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=cod" + sNameCosteDesc + " id=cod" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=den" + sNameCosteDesc + " id=den" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=intro" + sNameCosteDesc + " id=intro" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=tipo" + sNameCosteDesc + " id=tipo" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=valor" + sNameCosteDesc + " id=valor" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=obligatorio" + sNameCosteDesc + " id=obligatorio" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=valorMin" + sNameCosteDesc + " id=valorMin" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=valorMax" + sNameCosteDesc + " id=valorMax" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=lista" + sNameCosteDesc + " id=lista" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=operacion" + sNameCosteDesc + " id=operacion" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=aplicar" + sNameCosteDesc + " id=aplicar" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=ambito" + sNameCosteDesc + " id=ambito" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=importeParcial" + sNameCosteDesc + " id=importeParcial" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=alcance" + sNameCosteDesc + " id=alcance" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=numGrupos" + sNameCosteDesc + " id=numGrupos" + sNameCosteDesc + ">"
            for(indiceGrupos=0;indiceGrupos<vCostesDesc.grupos.length;indiceGrupos++)
            {
                sInputCosteDescX += "<INPUT type=hidden name=codGrupo" + indiceGrupos + sNameCosteDesc + " id=codGrupo" + indiceGrupos + sNameCosteDesc + ">"
                numGrupos++
            }
            document.getElementById("divOfertaAEnviar").innerHTML += sInputCosteDescX

		
            t = eval("f.id" +  sNameCosteDesc)
			t.value = vCostesDesc.id
            t = eval("f.paid" +  sNameCosteDesc)
			t.value = vCostesDesc.paid
            t = eval("f.cod" +  sNameCosteDesc)
			t.value = vCostesDesc.cod
            t = eval("f.den" +  sNameCosteDesc)
			t.value = vCostesDesc.den
			t = eval("f.intro" +  sNameCosteDesc)
			t.value = vCostesDesc.intro
			t = eval("f.tipo" +  sNameCosteDesc)
			t.value = vCostesDesc.tipo
            t = eval("f.valor" +  sNameCosteDesc)
			t.value = vCostesDesc.valor
            t = eval("f.obligatorio" +  sNameCosteDesc )
			t.value = vCostesDesc.obligatorio
            t = eval("f.valorMin" +  sNameCosteDesc)
			t.value = vCostesDesc.valorMin
            t = eval("f.valorMax" +  sNameCosteDesc)
			t.value = vCostesDesc.valorMax
            t = eval("f.lista" +  sNameCosteDesc)
			t.value = vCostesDesc.lista
            t = eval("f.operacion" +  sNameCosteDesc)
			t.value = vCostesDesc.operacion
            t = eval("f.aplicar" +  sNameCosteDesc)
			t.value = vCostesDesc.aplicar
            t = eval("f.ambito" +  sNameCosteDesc)
			t.value = vCostesDesc.ambito
            t = eval("f.importeParcial" +  sNameCosteDesc)
			t.value = vCostesDesc.importeParcial
            t = eval("f.alcance" +  sNameCosteDesc)
			t.value = vCostesDesc.alcance        
            t = eval("f.numGrupos" +  sNameCosteDesc)
			t.value = numGrupos
            for(indiceGrupos=0;indiceGrupos<vCostesDesc.grupos.length;indiceGrupos++)
            {
                t = eval("f.codGrupo" + indiceGrupos +  sNameCosteDesc)
			    t.value = vCostesDesc.grupos[indiceGrupos]
            }
            numGrupos=0

			numCostesDescuentos++
}

sInputNumProceCostesDescTotGrupo = "<INPUT type=hidden name=ofeProcNumCostesDescTotGrupo id=ofeProcNumCostesDescTotGrupo>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputNumProceCostesDescTotGrupo
f.ofeProcNumCostesDescTotGrupo.value = numCostesDescuentos

numCostesDescuentos = 0

for (indiceCostesDesc=0;indiceCostesDesc<proceso.CostesDescTotalItem.length;indiceCostesDesc++)
{

            vCostesDesc = proceso.CostesDescTotalItem[indiceCostesDesc]
            sNameCosteDesc = "ofeprocCostesDescTotItem_" + vCostesDesc.paid
			sInputCosteDescX = "<INPUT type=hidden name=ofeProcInputCosteDescTotItem" + indiceCostesDesc + " id=ofeProcInputCosteDescTotItem" + indiceCostesDesc + " value='" + sNameCosteDesc + "'>"
			sInputCosteDescX += "<INPUT type=hidden name=id" + sNameCosteDesc + " id=id" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=paid" + sNameCosteDesc + " id=paid" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=cod" + sNameCosteDesc + " id=cod" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=den" + sNameCosteDesc + " id=den" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=intro" + sNameCosteDesc + " id=intro" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=tipo" + sNameCosteDesc + " id=tipo" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=valor" + sNameCosteDesc + " id=valor" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=obligatorio" + sNameCosteDesc + " name=obligatorio" + sNameCosteDesc + "=obligatorio" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=valorMin" + sNameCosteDesc + " id=valorMin" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=valorMax" + sNameCosteDesc + " id=valorMax" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=lista" + sNameCosteDesc + " id=lista" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=operacion" + sNameCosteDesc + " id=operacion" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=aplicar" + sNameCosteDesc + " id=aplicar" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=ambito" + sNameCosteDesc + " id=ambito" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=importeParcial" + sNameCosteDesc + " id=importeParcial" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=alcance" + sNameCosteDesc + " id=alcance" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=numGrupos" + sNameCosteDesc + " id=numGrupos" + sNameCosteDesc + ">"
            document.getElementById("divOfertaAEnviar").innerHTML += sInputCosteDescX
		
            t = eval("f.id" +  sNameCosteDesc)
			t.value = vCostesDesc.id
            t = eval("f.paid" +  sNameCosteDesc)
			t.value = vCostesDesc.paid
            t = eval("f.cod" +  sNameCosteDesc)
			t.value = vCostesDesc.cod
            t = eval("f.den" +  sNameCosteDesc)
			t.value = vCostesDesc.den
			t = eval("f.intro" +  sNameCosteDesc)
			t.value = vCostesDesc.intro
			t = eval("f.tipo" +  sNameCosteDesc)
			t.value = vCostesDesc.tipo
            t = eval("f.valor" +  sNameCosteDesc)
			t.value = vCostesDesc.valor
            t = eval("f.obligatorio" +  sNameCosteDesc )
			t.value = vCostesDesc.obligatorio
            t = eval("f.valorMin" +  sNameCosteDesc)
			t.value = vCostesDesc.valorMin
            t = eval("f.valorMax" +  sNameCosteDesc)
			t.value = vCostesDesc.valorMax
            t = eval("f.lista" +  sNameCosteDesc)
			t.value = vCostesDesc.lista
            t = eval("f.operacion" +  sNameCosteDesc)
			t.value = vCostesDesc.operacion
            t = eval("f.aplicar" +  sNameCosteDesc)
			t.value = vCostesDesc.aplicar
            t = eval("f.ambito" +  sNameCosteDesc)
			t.value = vCostesDesc.ambito
            t = eval("f.importeParcial" +  sNameCosteDesc)
			t.value = vCostesDesc.importeParcial
            t = eval("f.alcance" +  sNameCosteDesc)
			t.value = vCostesDesc.alcance
			numCostesDescuentos++
}

sInputNumProceCostesDescTotItem = "<INPUT type=hidden name=ofeProcNumCostesDescTotItem id=ofeProcNumCostesDescTotItem>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputNumProceCostesDescTotItem
f.ofeProcNumCostesDescTotItem.value = numCostesDescuentos

numCostesDescuentos = 0

for (indiceCostesDesc=0;indiceCostesDesc<proceso.CostesDescPrecItem.length;indiceCostesDesc++)
{
            vCostesDesc = proceso.CostesDescPrecItem[indiceCostesDesc]
            sNameCosteDesc = "ofeprocCostesDescPreItem_" + vCostesDesc.paid
			sInputCosteDescX = "<INPUT type=hidden name=ofeProcInputCosteDescPreItem" + indiceCostesDesc + " id=ofeProcInputCosteDescPreItem" + indiceCostesDesc + " value='" + sNameCosteDesc + "'>"
			sInputCosteDescX += "<INPUT type=hidden name=id" + sNameCosteDesc + " id=id" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=paid" + sNameCosteDesc + " id=paid" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=cod" + sNameCosteDesc + " id=cod" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=den" + sNameCosteDesc + " id=den" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=intro" + sNameCosteDesc + " id=intro" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=tipo" + sNameCosteDesc + " id=tipo" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=valor" + sNameCosteDesc + " id=valor" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=obligatorio" + sNameCosteDesc + " id=obligatorio" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=valorMin" + sNameCosteDesc + " id=valorMin" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=valorMax" + sNameCosteDesc + " id=valorMax" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=lista" + sNameCosteDesc + " id=lista" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=operacion" + sNameCosteDesc + " id=operacion" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=aplicar" + sNameCosteDesc + " id=aplicar" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=ambito" + sNameCosteDesc + " id=ambito" + sNameCosteDesc + ">"
			sInputCosteDescX += "<INPUT type=hidden name=importeParcial" + sNameCosteDesc + " id=importeParcial" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=alcance" + sNameCosteDesc + " id=alcance" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=numGrupos" + sNameCosteDesc + " id=numGrupos" + sNameCosteDesc + ">"
            document.getElementById("divOfertaAEnviar").innerHTML += sInputCosteDescX
		
            t = eval("f.id" +  sNameCosteDesc)
			t.value = vCostesDesc.id
            t = eval("f.paid" +  sNameCosteDesc)
			t.value = vCostesDesc.paid
            t = eval("f.cod" +  sNameCosteDesc)
			t.value = vCostesDesc.cod
            t = eval("f.den" +  sNameCosteDesc)
			t.value = vCostesDesc.den
			t = eval("f.intro" +  sNameCosteDesc)
			t.value = vCostesDesc.intro
			t = eval("f.tipo" +  sNameCosteDesc)
			t.value = vCostesDesc.tipo
            t = eval("f.valor" +  sNameCosteDesc)
			t.value = vCostesDesc.valor
            t = eval("f.obligatorio" +  sNameCosteDesc )
			t.value = vCostesDesc.obligatorio
            t = eval("f.valorMin" +  sNameCosteDesc)
			t.value = vCostesDesc.valorMin
            t = eval("f.valorMax" +  sNameCosteDesc)
			t.value = vCostesDesc.valorMax
            t = eval("f.lista" +  sNameCosteDesc)
			t.value = vCostesDesc.lista
            t = eval("f.operacion" +  sNameCosteDesc)
			t.value = vCostesDesc.operacion
            t = eval("f.aplicar" +  sNameCosteDesc)
			t.value = vCostesDesc.aplicar
            t = eval("f.ambito" +  sNameCosteDesc)
			t.value = vCostesDesc.ambito
            t = eval("f.importeParcial" +  sNameCosteDesc)
			t.value = vCostesDesc.importeParcial
            t = eval("f.alcance" +  sNameCosteDesc)
			t.value = vCostesDesc.alcance
			numCostesDescuentos++
}

sInputNumProceCostesDescPreItem = "<INPUT type=hidden name=ofeProcNumCostesDescPreItem id=ofeProcNumCostesDescPreItem>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputNumProceCostesDescPreItem
f.ofeProcNumCostesDescPreItem.value = numCostesDescuentos


//JVS costes / descuentos de la oferta

var i
var j
var numCostesDescuentos
var numImpoParcGr=0
var objCosteDescValor
var vGru

vGru = proceso.grupos

numCostesDescuentos =0
if (proceso.costesDescuentos.length>0)
	{
	sInputModifCosteDescOferta = "<INPUT type=hidden name=ofeModifCostesDescOferta id=ofeModifCostesDescOferta value = 1>"
    document.getElementById("divOfertaAEnviar").innerHTML += sInputModifCosteDescOferta
	for (i=0;i<proceso.costesDescuentos.length;i++)
		{

			sNameCosteDesc = "ofeprocCostesDescOferta_" + proceso.costesDescuentos[i].paid
			sInputCosteDescX = "<INPUT type=hidden name=ofeProcInputCosteDescOferta" + i + " id=ofeProcInputCosteDescOferta" + i + " value='" + sNameCosteDesc + "'>"
			sInputCosteDescX += "<INPUT type=hidden name=" + sNameCosteDesc + " id=" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=impo" + sNameCosteDesc + " id=impo" + sNameCosteDesc + ">"
            document.getElementById("divOfertaAEnviar").innerHTML += sInputCosteDescX
		
			t = eval("f." +  sNameCosteDesc )
			t.value = num2str(proceso.costesDescuentos[i].valor,vdecimalfmt,vthousanfmt,1000)
			t = eval("f.impo" +  sNameCosteDesc )
			t.value = num2str(proceso.costesDescuentos[i].importeParcial,vdecimalfmt,vthousanfmt,1000)
			numCostesDescuentos++
		}	
	}


sInputImporteBruto  = "<INPUT type=hidden name=ofeProcImporteBruto id=ofeProcImporteBruto>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputImporteBruto
f.ofeProcImporteBruto.value = num2str(proceso.importeBruto,vdecimalfmt,vthousanfmt,1000)

sInputImporte  = "<INPUT type=hidden name=ofeProcImporte id=ofeProcImporte>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputImporte
f.ofeProcImporte.value = num2str(proceso.importe,vdecimalfmt,vthousanfmt,1000)

sInputNumProceCostesDescOferta = "<INPUT type=hidden name=ofeProcNumCostesDescOferta id=ofeProcNumCostesDescOferta>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputNumProceCostesDescOferta
f.ofeProcNumCostesDescOferta.value = numCostesDescuentos

//Obtenci�n del importe parcial de cada grupo
for (indiceGr=0;indiceGr<vGru.length;indiceGr++)
{
    campo = "gruProcImporte_" + vGru[indiceGr].cod
    sInputImporte  = "<INPUT type=hidden name=" + campo + " id=" + campo + ">"
    document.getElementById("divOfertaAEnviar").innerHTML += sInputImporte
    eval("f." + campo + ".value=num2str(vGru[indiceGr].importe,vdecimalfmt,vthousanfmt,1000)")
}

//Importes parciales de grupos para atributos que son de �mbito Oferta pero aplican al total de grupo

for (k=0;k<proceso.ImportesParcialesGrupo.length;k++)
{
    sNameCosteDesc = "ofeprocImpoParcGrOferta_" + proceso.ImportesParcialesGrupo[k].costeDesc + "_"  + proceso.ImportesParcialesGrupo[k].grupo
	sInputCosteDescX = "<INPUT type=hidden name=ofeprocImpoParcGrOferta" + k + " id=ofeprocImpoParcGrOferta" + k + " value='" + sNameCosteDesc + "'>"
	sInputCosteDescX += "<INPUT type=hidden name=" + sNameCosteDesc + " id=" + sNameCosteDesc + ">"
    sInputCosteDescX += "<INPUT type=hidden name=impo" + sNameCosteDesc + " id=impo" + sNameCosteDesc + ">"
    sInputCosteDescX += "<INPUT type=hidden name=grupo" + sNameCosteDesc + " id=grupo" + sNameCosteDesc + ">"
    document.getElementById("divOfertaAEnviar").innerHTML += sInputCosteDescX
		
	t = eval("f." +  sNameCosteDesc )
	t.value = proceso.ImportesParcialesGrupo[k].costeDesc
	t = eval("f.impo" +  sNameCosteDesc )
	t.value = num2str(proceso.ImportesParcialesGrupo[k].importeParcial,vdecimalfmt,vthousanfmt,1000)
    t = eval("f.grupo" +  sNameCosteDesc )
	t.value = proceso.ImportesParcialesGrupo[k].grupo
    numImpoParcGr++
} 

sInputNumProceImpoParcGrOferta = "<INPUT type=hidden name=ofeProcNumImpoParcGrOferta id=ofeProcNumImpoParcGrOferta>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputNumProceImpoParcGrOferta
f.ofeProcNumImpoParcGrOferta.value = numImpoParcGr

//JVS

//JVS costes / descuentos de los grupos
var i
var j
var numCostesDescuentos
var objCosteDescValor
var vGrupo

numCostesDescuentos =0

 for (i=0;i<proceso.grupos.length;i++)
{
    vGrupo = proceso.grupos[i]
    if (proceso.grupos[i].costesDescuentos.length>0)
	{
	sInputModifCosteDescGrupo = "<INPUT type=hidden name=ofeModifCostesDescGrupo" + "_" + vGrupo.cod + " id=ofeModifCostesDescGrupo" + "_" + vGrupo.cod + " value = 1>"
    document.getElementById("divOfertaAEnviar").innerHTML += sInputModifCosteDescGrupo
	for (j=0;j<vGrupo.costesDescuentos.length;j++)
		{

			sNameCosteDesc = "ofeprocCostesDescGrupo_" + vGrupo.cod + "_" + vGrupo.costesDescuentos[j].paid
			sInputCosteDescX = "<INPUT type=hidden name=ofeProcInputCosteDescGrupo" + j + "_" + vGrupo.cod + " id=ofeProcInputCosteDescGrupo" + j + "_" + vGrupo.cod + " value='" + sNameCosteDesc + "'>"
			sInputCosteDescX += "<INPUT type=hidden name=" + sNameCosteDesc + " id=" + sNameCosteDesc + ">"
            sInputCosteDescX += "<INPUT type=hidden name=impo" + sNameCosteDesc + " id=impo" + sNameCosteDesc + ">"
            document.getElementById("divOfertaAEnviar").innerHTML += sInputCosteDescX
		
			t = eval("f." +  sNameCosteDesc )
			t.value = num2str(vGrupo.costesDescuentos[j].valor,vdecimalfmt,vthousanfmt,1000)
			t = eval("f.impo" +  sNameCosteDesc )
			t.value = num2str(vGrupo.costesDescuentos[j].importeParcial,vdecimalfmt,vthousanfmt,1000)
			numCostesDescuentos++
		}	
    }
 
    sInputImporteBruto  = "<INPUT type=hidden name=ofeProcImporteBruto" + "_" + vGrupo.cod + " id=ofeProcImporteBruto" + "_" + vGrupo.cod + ">"
    document.getElementById("divOfertaAEnviar").innerHTML += sInputImporteBruto
    eval("f.ofeProcImporteBruto" + "_" + vGrupo.cod + ".value = num2str(vGrupo.importeBruto,vdecimalfmt,vthousanfmt,1000)")

    sInputImporte  = "<INPUT type=hidden name=ofeProcImporte" + "_" + vGrupo.cod + " id=ofeProcImporte" + "_" + vGrupo.cod + ">"
    document.getElementById("divOfertaAEnviar").innerHTML += sInputImporte
    eval("f.ofeProcImporte" + "_" + vGrupo.cod + ".value = num2str(vGrupo.importe,vdecimalfmt,vthousanfmt,1000)")

    sInputNumProceCostesDescGrupo = "<INPUT type=hidden name=ofeProcNumCostesDescGrupo" + "_" + vGrupo.cod + " id=ofeProcNumCostesDescGrupo" + "_" + vGrupo.cod + ">"
    document.getElementById("divOfertaAEnviar").innerHTML += sInputNumProceCostesDescGrupo
    eval("f.ofeProcNumCostesDescGrupo" + "_" + vGrupo.cod + ".value = numCostesDescuentos")
}
//JVS



//JVS costes / descuentos de los �tems
var i, j, k
var numCostesDescuentos
var objCosteDescValor
var vGrupo, vItem, vCosteDescOfe


if(proceso.subastaCargada)
{
    for(i=0;i<itemsPuja.length;i++)
    {
        vItem = itemsPuja[i]
        if (vItem.costesDescuentosOfertados.length>0)
	    {
            numCostesDescuentos=0			
	        sInputModifCosteDescItem = "<INPUT type=hidden name=ofeModifCostesDescItem" + "_" + vItem.id + " id=ofeModifCostesDescItem" + "_" + vItem.id + " value = 1>"
            document.getElementById("divOfertaAEnviar").innerHTML += sInputModifCosteDescItem
	        for (k=0;k<vItem.costesDescuentosOfertados.length;k++)
		    {
                vCosteDescOfe = vItem.costesDescuentosOfertados[k] 

			    sNameCosteDesc = "ofeprocCostesDescItem_" + vItem.id + "_" + vCosteDescOfe.paid
			    sInputCosteDescX = "<INPUT type=hidden name=ofeProcInputCosteDescItem" + k + "_" + vItem.id + " id=ofeProcInputCosteDescItem" + k + "_" + vItem.id + " value='" + sNameCosteDesc + "'>"
			    sInputCosteDescX += "<INPUT type=hidden name=" + sNameCosteDesc + " id=" + sNameCosteDesc + ">"
                sInputCosteDescX += "<INPUT type=hidden name=impo" + sNameCosteDesc + " id=impo" + sNameCosteDesc + ">"
                document.getElementById("divOfertaAEnviar").innerHTML += sInputCosteDescX
		
			    t = eval("f." +  sNameCosteDesc )
			    t.value = num2str(vCosteDescOfe.valor,vdecimalfmt,vthousanfmt,1000)
			    t = eval("f.impo" +  sNameCosteDesc )
			    t.value = num2str(vCosteDescOfe.importeParcial,vdecimalfmt,vthousanfmt,1000)
			    numCostesDescuentos++
		    }	

             sInputNumProceCostesDescItem = "<INPUT type=hidden name=ofeProcNumCostesDescItem" + "_" + vItem.id + " id=ofeProcNumCostesDescItem" + "_" + vItem.id + ">"
            document.getElementById("divOfertaAEnviar").innerHTML += sInputNumProceCostesDescItem
            eval("f.ofeProcNumCostesDescItem" + "_" + vItem.id + ".value = numCostesDescuentos")

        }
        sInputImporteNeto  = "<INPUT type=hidden name=ofeProcImporteNeto" + "_" + vItem.id + " id=ofeProcImporteNeto" + "_" + vItem.id + ">"
        document.getElementById("divOfertaAEnviar").innerHTML += sInputImporteNeto
		
        if(vItem.importeNeto!=null)
            eval("f.ofeProcImporteNeto" + "_" + vItem.id + ".value = num2str(vItem.importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt)")
        else
            eval("f.ofeProcImporteNeto" + "_" + vItem.id + ".value =  \"\"")

        sInputPrecioNeto  = "<INPUT type=hidden name=ofeProcPrecioNeto" + "_" + vItem.id + " id=ofeProcPrecioNeto" + "_" + vItem.id + ">"
        document.getElementById("divOfertaAEnviar").innerHTML += sInputPrecioNeto
        if(vItem.importeNeto!=null)
            eval("f.ofeProcPrecioNeto" + "_" + vItem.id + ".value = num2str(vItem.precioNeto,vdecimalfmt,vthousanfmt,vprecisionfmt)")
        else
            eval("f.ofeProcPrecioNeto" + "_" + vItem.id + ".value = \"\"")

        sNamePrecio = "txtPUBruto_" + vGrupo.cod + "_" + vItem.id
        if (f.elements[sNamePrecio]){
            if(vItem.importeNeto!=null)
                eval("f.txtPUBruto" + "_" + vGrupo.cod + "_" + vItem.id + ".value = num2str(vItem.precio,vdecimalfmt,vthousanfmt,vprecisionfmt)")
            else
                eval("f.txtPUBruto" + "_" + vGrupo.cod + "_" + vItem.id + ".value = \"\"")
        }

        sNamePrecio2 = "txtPU2Bruto_" + vGrupo.cod + "_" + vItem.id
        if (f.elements[sNamePrecio2]){
            if(vItem.importeNeto!=null)
                eval("f.txtPU2Bruto_" + vGrupo.cod + "_" + vItem.id + ".value = num2str(vItem.precio2,vdecimalfmt,vthousanfmt,vprecisionfmt)")
            else
                eval("f.txtPU2Bruto_" + vGrupo.cod + "_" + vItem.id + ".value = \"\"")            
        }
        sNamePrecio3 = "txtPU3Bruto_" + vGrupo.cod + "_" + vItem.id
        if (f.elements[sNamePrecio3]){
            if(vItem.importeNeto!=null)
                eval("f.txtPU3Bruto_" + vGrupo.cod + "_" + vItem.id + ".value = num2str(vItem.precio3,vdecimalfmt,vthousanfmt,vprecisionfmt)")
            else
                eval("f.txtPU3Bruto_" + vGrupo.cod + "_" + vItem.id + ".value = \"\"")
        }
    }
}
else
{
    for (i=0;i<proceso.grupos.length;i++)
    {	
		if (vGrupo.hayEscalados)
		{
			sInputModifCosteDescItemEsc = "<INPUT type=hidden name=NumEscaladosGrupo" + "_" + vGrupo.cod + " id=NumEscaladosGrupo" + "_" + vGrupo.cod + " value = "  + vGrupo.escalados.length + ">"
			document.getElementById("divOfertaAEnviar").innerHTML += sInputModifCosteDescItemEsc
			for (iesc=0;iesc<vGrupo.escalados.length;iesc++)
			{			
			    sInputEscalado = "<INPUT type=hidden name=EscaladoGrupo" + iesc + "_" + vGrupo.cod + " id=EscaladoGrupo" + iesc + "_" + vGrupo.cod + " value='" + vGrupo.escalados[iesc].id + "'>"						
				document.getElementById("divOfertaAEnviar").innerHTML += sInputEscalado
				
			    sInputEscalado = "<INPUT type=hidden name=EscaladoGrupo_" + vGrupo.escalados[iesc].id + "_ini id=EscaladoGrupo" + vGrupo.escalados[iesc].id + "_ini value='" + num2str(vGrupo.escalados[iesc].inicio,vdecimalfmt, vthousanfmt,vprecisionfmt) + "'>"						
				document.getElementById("divOfertaAEnviar").innerHTML += sInputEscalado
				
			    sInputEscalado = "<INPUT type=hidden name=EscaladoGrupo_" + vGrupo.escalados[iesc].id + "_fin id=EscaladoGrupo" + vGrupo.escalados[iesc].id + "_fin value='" + num2str(vGrupo.escalados[iesc].fin,vdecimalfmt, vthousanfmt,vprecisionfmt) + "'>"						
				document.getElementById("divOfertaAEnviar").innerHTML += sInputEscalado
				
			}			
		}
		else
		{
			sInputModifCosteDescItemEsc = "<INPUT type=hidden name=NumEscaladosGrupo" + "_" + vGrupo.cod + " id=NumEscaladosGrupo" + "_" + vGrupo.cod + " value = "  + vGrupo.escalados.length + ">"
			document.getElementById("divOfertaAEnviar").innerHTML += sInputModifCosteDescItemEsc		
		}

	
        vGrupo = proceso.grupos[i]
        for(j=0;j<vGrupo.items.length;j++)
        {
            vItem = vGrupo.items[j]
            if (vItem.costesDescuentosOfertados.length>0)
	        {
	            sInputModifCosteDescItem = "<INPUT type=hidden name=ofeModifCostesDescItem" + "_" + vGrupo.cod + "_" + vItem.id + " id=ofeModifCostesDescItem" + "_" + vGrupo.cod + "_" + vItem.id + " value = 1>"
                document.getElementById("divOfertaAEnviar").innerHTML += sInputModifCosteDescItem				
				if (vGrupo.hayEscalados)
				{				
					for (iesc=0;iesc<vGrupo.escalados.length;iesc++)
					{
						sInputModifCosteDescItemEsc = "<INPUT type=hidden name=ofeModifCostesDescItemEsc" + "_" + vGrupo.cod + "_" + vItem.id + "_" + vGrupo.escalados[iesc].id + " id=ofeModifCostesDescItem" + "_" + vGrupo.cod + "_" + vItem.id + "_" + vGrupo.escalados[iesc].id + " value = 1>"
						document.getElementById("divOfertaAEnviar").innerHTML += sInputModifCosteDescItemEsc
					}
				}
	            for (k=0;k<vItem.costesDescuentosOfertados.length;k++)
		        {
                    vCosteDescOfe = vItem.costesDescuentosOfertados[k] 

			        sNameCosteDesc = "ofeprocCostesDescItem_" + vGrupo.cod + "_" + vItem.id + "_" + vCosteDescOfe.paid
			        sInputCosteDescX = "<INPUT type=hidden name=ofeProcInputCosteDescItem" + k + "_" + vGrupo.cod + "_" + vItem.id + " id=ofeProcInputCosteDescItem" + k + "_" + vGrupo.cod + "_" + vItem.id + " value='" + sNameCosteDesc + "'>"
			        sInputCosteDescX += "<INPUT type=hidden name=" + sNameCosteDesc + " id=" + sNameCosteDesc + ">"
                    sInputCosteDescX += "<INPUT type=hidden name=impo" + sNameCosteDesc + " id=impo" + sNameCosteDesc + ">"
                    document.getElementById("divOfertaAEnviar").innerHTML += sInputCosteDescX
		
			        t = eval("f." +  sNameCosteDesc )
			        t.value = num2str(vCosteDescOfe.valor,vdecimalfmt,vthousanfmt,1000)
			        t = eval("f.impo" +  sNameCosteDesc )
			        t.value = num2str(vCosteDescOfe.importeParcial,vdecimalfmt,vthousanfmt,1000)
					
					if (vGrupo.hayEscalados)
					{
						for (iesc=0;iesc<vGrupo.escalados.length;iesc++)
						{
							sNameCosteDesc = "ofeprocCostesDescItem_" + vGrupo.cod + "_" + vItem.id + "_" + vCosteDescOfe.paid + "_" + vGrupo.escalados[iesc].id
							sInputCosteDescX = "<INPUT type=hidden name=ofeProcInputCosteDescItem" + k + "_" + vGrupo.cod + "_" + vItem.id + "_" + vGrupo.escalados[iesc].id + " id=ofeProcInputCosteDescItem" + k + "_" + vGrupo.cod + "_" + vItem.id + " value='" + sNameCosteDesc + "'>"
							sInputCosteDescX += "<INPUT type=hidden name=" + sNameCosteDesc + " id=" + sNameCosteDesc + ">"
							document.getElementById("divOfertaAEnviar").innerHTML += sInputCosteDescX
				
							t = eval("f." +  sNameCosteDesc )							
							if(vCosteDescOfe.escalados[iesc]) //DPD No deber�a ser necesaria la comprobaci�n
								t.value = num2str(vCosteDescOfe.escalados[iesc].valorNum,vdecimalfmt,vthousanfmt,1000)						
						}
					}
					
			        numCostesDescuentos++
		        }	
                sInputNumProceCostesDescItem = "<INPUT type=hidden name=ofeProcNumCostesDescItem" + "_" + vGrupo.cod + "_" + vItem.id + " id=ofeProcNumCostesDescItem" + "_" + vGrupo.cod + "_" + vItem.id + ">"
                document.getElementById("divOfertaAEnviar").innerHTML += sInputNumProceCostesDescItem
                eval("f.ofeProcNumCostesDescItem" + "_" + vGrupo.cod + "_" + vItem.id + ".value = numCostesDescuentos")

            }

            sInputImporteNeto  = "<INPUT type=hidden name=ofeProcImporteNeto" + "_" + vGrupo.cod + "_" + vItem.id + " id=ofeProcImporteNeto" + "_" + vGrupo.cod + "_" + vItem.id + ">"
            document.getElementById("divOfertaAEnviar").innerHTML += sInputImporteNeto
			
            if(vItem.importeNeto!=null)
                eval("f.ofeProcImporteNeto" + "_" + vGrupo.cod + "_" + vItem.id + ".value = num2str(vItem.importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt)")
            else
                //eval("f.ofeProcImporteNeto" + "_" + vGrupo.cod + "_" + vItem.id + ".value = 0")
				eval("f.ofeProcImporteNeto" + "_" + vGrupo.cod + "_" + vItem.id + ".value = \"\"")								

            sInputPrecioNeto  = "<INPUT type=hidden name=ofeProcPrecioNeto" + "_" + vGrupo.cod + "_" + vItem.id + " id=ofeProcPrecioNeto" + "_" + vGrupo.cod + "_" + vItem.id + ">"
            document.getElementById("divOfertaAEnviar").innerHTML += sInputPrecioNeto
            if(vItem.importeNeto!=null)
                eval("f.ofeProcPrecioNeto" + "_" + vGrupo.cod + "_" + vItem.id + ".value = num2str(vItem.precioNeto,vdecimalfmt,vthousanfmt,vprecisionfmt)")
            else
				eval("f.ofeProcPrecioNeto" + "_" + vGrupo.cod + "_" + vItem.id + ".value = \"\"")								

            if (!vGrupo.hayEscalados) 
            {
                if(vItem.importeNeto!=null)
                    eval("f.txtPUBruto_" + vGrupo.cod + "_" + vItem.id + ".value = num2str(vItem.precio,vdecimalfmt,vthousanfmt,vprecisionfmt)")
                else
                    eval("f.txtPUBruto_" + vGrupo.cod + "_" + vItem.id + ".value = \"\"")
            
                sNamePrecio2 = "txtPU2Bruto_" + vGrupo.cod + "_" + vItem.id
                if (f.elements[sNamePrecio2]){
                    if(vItem.importeNeto!=null)
                        eval("f.txtPU2Bruto_" + vGrupo.cod + "_" + vItem.id + ".value = num2str(vItem.precio2,vdecimalfmt,vthousanfmt,vprecisionfmt)")
                    else
                        eval("f.txtPU2Bruto_" + vGrupo.cod + "_" + vItem.id + ".value = \"\"")            
                }
                sNamePrecio3 = "txtPU3Bruto_" + vGrupo.cod + "_" + vItem.id
                if (f.elements[sNamePrecio3]){
                    if(vItem.importeNeto!=null)
                        eval("f.txtPU3Bruto_" + vGrupo.cod + "_" + vItem.id + ".value = num2str(vItem.precio3,vdecimalfmt,vthousanfmt,vprecisionfmt)")
                    else
                        eval("f.txtPU3Bruto_" + vGrupo.cod + "_" + vItem.id + ".value = \"\"")
                }
            }
        }
    }
}
//JVS





sInputObsAdjun  = "<INPUT type=hidden name=ofeObsAdjun id=ofeObsAdjun>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputObsAdjun
f.ofeObsAdjun.value = (proceso.obsAdjun==null?"":proceso.obsAdjun)


if (proceso.adjuntosCargados)
	{
	sInputObsAdjun  = "<INPUT type=hidden name=ofeAdjuntosCargados id=ofeAdjuntosCargados value=1>"
    document.getElementById("divOfertaAEnviar").innerHTML += sInputObsAdjun
	sInputNumAdjuntos  = "<INPUT type=hidden name=ofeNumAdjuntos id=ofeNumAdjuntos>"
    document.getElementById("divOfertaAEnviar").innerHTML += sInputNumAdjuntos
	f.ofeNumAdjuntos.value = proceso.adjuntos.length


	for (i=0;i<proceso.adjuntos.length;i++) 
		{
		sNameAdjun = "ofeprocAdjun_" + i
		sInputAdjunX = "<INPUT type=hidden name=ID_" + sNameAdjun + " id=ID_" + sNameAdjun + " >"
		sInputAdjunX += "<INPUT type=hidden name=IDPortal_" + sNameAdjun + " id=IDPortal_" + sNameAdjun + " >"
		sInputAdjunX += "<INPUT type=hidden name=NOM_" + sNameAdjun + " id=NOM_" + sNameAdjun + " >"
		sInputAdjunX += "<INPUT type=hidden name=COM_" + sNameAdjun + " id=COM_" + sNameAdjun + " >"
		sInputAdjunX += "<INPUT type=hidden name=DEL_" + sNameAdjun + " id=DEL_" + sNameAdjun + " >"
		sInputAdjunX += "<INPUT type=hidden name=MOD_" + sNameAdjun + " id=MOD_" + sNameAdjun + " >"
		sInputAdjunX += "<INPUT type=hidden name=SIZE_" + sNameAdjun + " id=SIZE_" + sNameAdjun + " >"
		
		
        document.getElementById("divOfertaAEnviar").innerHTML += sInputAdjunX
		
		t = eval("f.ID_" +  sNameAdjun )
		t.value = proceso.adjuntos[i].ID==null?"":proceso.adjuntos[i].ID
		
		
		t = eval("f.IDPortal_" +  sNameAdjun )
		t.value = proceso.adjuntos[i].idPortal==null?"":proceso.adjuntos[i].idPortal

		t = eval("f.NOM_" +  sNameAdjun )
		t.value = proceso.adjuntos[i].nombre

		
		t = eval("f.COM_" +  sNameAdjun )
		t.value = proceso.adjuntos[i].comentario

		t = eval("f.DEL_" +  sNameAdjun )
		t.value = proceso.adjuntos[i].deleted ? 1 : 0
		t = eval("f.MOD_" +  sNameAdjun )
		t.value = proceso.adjuntos[i].modified ? 1 : 0

		t = eval("f.SIZE_" +  sNameAdjun )
		t.value = proceso.adjuntos[i].size
		}	
	}
else
	{
	sInputObsAdjun  = "<INPUT type=hidden name=ofeAdjuntosCargados id=ofeAdjuntosCargados value=0>"
    document.getElementById("divOfertaAEnviar").innerHTML += sInputObsAdjun
	}
	

sInputNumGrupos  = "<INPUT type=hidden  name=ofeNumGrupos id=ofeNumGrupos>"
document.getElementById("divOfertaAEnviar").innerHTML +=sInputNumGrupos
f.ofeNumGrupos.value = proceso.grupos.length


for (i=0;i<proceso.grupos.length;i++)
	{

	sInputNumItems = "<INPUT type=hidden name=numItemsGrupoSubasta" + proceso.grupos[i].cod + " id=numItemsGrupoSubasta" + proceso.grupos[i].cod + ">"
    document.getElementById("divOfertaAEnviar").innerHTML +=sInputNumItems
	t=eval("f.numItemsGrupoSubasta" + proceso.grupos[i].cod)
	t.value = proceso.grupos[i].numItems

	sInputCodigosGrupos = "<INPUT type=hidden name=ofeGrupoCodGrupo" + i + " id=ofeGrupoCodGrupo" + i + ">"
    document.getElementById("divOfertaAEnviar").innerHTML +=sInputCodigosGrupos
	t=eval("f.ofeGrupoCodGrupo" + i)
	t.value = proceso.grupos[i].cod
	
	sInputIDsGrupos = "<INPUT type=hidden name=ofeGrupoIdGrupo" + i + " id=ofeGrupoIdGrupo" + i + ">"
    document.getElementById("divOfertaAEnviar").innerHTML += sInputIDsGrupos
	t=eval("f.ofeGrupoIdGrupo" + i)
	t.value = proceso.grupos[i].id
	
	
	if (proceso.grupos[i].atribs.length>0)
		{
		sInputModifAtribsGrupo = "<INPUT type=hidden name=ofeGrupoModifAtribs_" + proceso.grupos[i].cod + " id=ofeGrupoModifAtribs_" + proceso.grupos[i].cod + " value = 1>"
        document.getElementById("divOfertaAEnviar").innerHTML += sInputModifAtribsGrupo
			
		numAtribs =0
		for (j=0;j<proceso.grupos[i].atribs.length;j++)
			{
			if(proceso.grupos[i].atribs[j].tipo==4)
				bEntra = proceso.grupos[i].atribs[j].valor==null ? false  : true
			else
				bEntra=true			
			if (bEntra)
				{		
				sNameAtrib = "ofegrupoAtrib_" + proceso.grupos[i].cod + "_" + proceso.grupos[i].atribs[j].paid
				sInputAtribX = "<INPUT type=hidden name=ofeGrupoInputAtrib_" + proceso.grupos[i].cod + "_" + j + " id=ofeGrupoInputAtrib_" + proceso.grupos[i].cod + "_" + j + " value='" + sNameAtrib + "'>"
				sInputAtribX += "<INPUT type=hidden name=tipo" + sNameAtrib + " id=tipo" + sNameAtrib + ">"
				sInputAtribX += "<INPUT type=hidden name=" + sNameAtrib + " id=" + sNameAtrib + ">"
                document.getElementById("divOfertaAEnviar").innerHTML += sInputAtribX
				t = eval("f.tipo" +  sNameAtrib )
								
				t.value = proceso.grupos[i].atribs[j].tipo
				t = eval("f." +  sNameAtrib )

				switch (proceso.grupos[i].atribs[j].tipo)
					{
					case 1:
						t.value = proceso.grupos[i].atribs[j].valor
						break
					case 2:
						t.value = num2str(proceso.grupos[i].atribs[j].valor,vdecimalfmt,vthousanfmt,1000)
						break
					case 3:
						t.value = date2str(proceso.grupos[i].atribs[j].valor,vdatefmt)
						break
					case 4:
						if (proceso.grupos[i].atribs[j].valor)
							t.value = "1"
						else 
							if (proceso.grupos[i].atribs[j].valor==false)
								t.value = "0"
							else
								t.value = "null"
						break
					}
				numAtribs++

				}
			}
		sInputNumGrupoAtribs = "<INPUT type=hidden name=ofeGrupoNumAtribs" + i + " id=ofeGrupoNumAtribs" + i + ">"
        document.getElementById("divOfertaAEnviar").innerHTML += sInputNumGrupoAtribs
		t= eval("f.ofeGrupoNumAtribs" + i )
	
		t.value = proceso.grupos[i].atribs.length

	
		}

	
	sInputObsAdjun  = "<INPUT type=hidden name=ofeGrupoObsAdjun" + i + " id=ofeGrupoObsAdjun" + i + ">"
    document.getElementById("divOfertaAEnviar").innerHTML += sInputObsAdjun
	f.elements["ofeGrupoObsAdjun" + i].value = (proceso.grupos[i].obsAdjun==null?"":proceso.grupos[i].obsAdjun)

	if (proceso.grupos[i].adjuntosCargados)
		{
		sInputObsAdjun  = "<INPUT type=hidden name=ofeGrupoAdjuntosCargados" + i + "  id=ofeGrupoAdjuntosCargados" + i + " value=1>"
        document.getElementById("divOfertaAEnviar").innerHTML += sInputObsAdjun
		sInputNumAdjuntos  = "<INPUT type=hidden name=ofeGrupoNumAdjuntos" + i + " id=ofeGrupoNumAdjuntos" + i + ">"

        document.getElementById("divOfertaAEnviar").innerHTML += sInputNumAdjuntos
		t= eval("f.ofeGrupoNumAdjuntos" + i )
		t.value = proceso.grupos[i].adjuntos.length

		for (j=0;j<proceso.grupos[i].adjuntos.length;j++)
			{
			sNameAdjun = "ofeprocAdjun_" + proceso.grupos[i].cod + "_" +  j
			sInputAdjunX = "<INPUT type=hidden name=ID_" + sNameAdjun + " id=ID_" + sNameAdjun + " >"
			sInputAdjunX += "<INPUT type=hidden name=IDPortal_" + sNameAdjun + " id=IDPortal_" + sNameAdjun + " >"
			sInputAdjunX += "<INPUT type=hidden name=NOM_" + sNameAdjun + " id=NOM_" + sNameAdjun + " >"
			sInputAdjunX += "<INPUT type=hidden name=COM_" + sNameAdjun + " id=COM_" + sNameAdjun + " >"
			sInputAdjunX += "<INPUT type=hidden name=DEL_" + sNameAdjun + " id=DEL_" + sNameAdjun + " >"
			sInputAdjunX += "<INPUT type=hidden name=MOD_" + sNameAdjun + " id=MOD_" + sNameAdjun + " >"
			sInputAdjunX += "<INPUT type=hidden name=SIZE_" + sNameAdjun + " id=SIZE_" + sNameAdjun + " >"
            document.getElementById("divOfertaAEnviar").innerHTML +=sInputAdjunX
			t = eval("f.ID_" +  sNameAdjun )
			t.value = proceso.grupos[i].adjuntos[j].ID==null?"":proceso.grupos[i].adjuntos[j].ID
			t = eval("f.IDPortal_" +  sNameAdjun )
			t.value = proceso.grupos[i].adjuntos[j].idPortal==null?"":proceso.grupos[i].adjuntos[j].idPortal
			t = eval("f.NOM_" +  sNameAdjun )
			t.value = proceso.grupos[i].adjuntos[j].nombre
			t = eval("f.COM_" +  sNameAdjun )
			t.value = proceso.grupos[i].adjuntos[j].comentario
			t = eval("f.DEL_" +  sNameAdjun )
			t.value = proceso.grupos[i].adjuntos[j].deleted ? 1 : 0
			t = eval("f.MOD_" +  sNameAdjun )
			t.value = proceso.grupos[i].adjuntos[j].modified ? 1 : 0
			t = eval("f.SIZE_" +  sNameAdjun )
			t.value = proceso.grupos[i].adjuntos[j].size
	
			}	
		}
	else
		{
		sInputObsAdjun  = "<INPUT type=hidden name=ofeGrupoAdjuntosCargados" + i + " id=ofeGrupoAdjuntosCargados" + i + " value=0>"
        document.getElementById("divOfertaAEnviar").innerHTML += sInputObsAdjun
		}
		

	}

	
j=0
for (i=0;i<arrAdjuntosItem.length;i++)
	{
	vAdjun=arrAdjuntosItem[i].oAdjunto
	
	vItem =arrAdjuntosItem[i].oItem

	if (vItem.adjuntosCargados)
		{	
		f.elements["ItemAdjuntosCargados_" + vItem.id].value = 1
		sNameInput = "numAdjunItem" + vItem.id
		if (eval("f." + sNameInput))
			{
			t =eval("f." + sNameInput)
			t.value = parseInt(t.value) + 1
			vItem.numAdjuntosPosteados ++
			}
		else
			{
			sInputNumAdjunItem = "<input type = hidden name =" + sNameInput + ">"
            document.getElementById("divOfertaAEnviar").innerHTML += sInputNumAdjunItem
			t =eval("f." + sNameInput)
			t.value = 1
			vItem.numAdjuntosPosteados = 1
			}


		sNameAdjun = "ofeitemAdjun_" + vItem.id + "_" +  vItem.numAdjuntosPosteados
		sInputAdjunX = "<INPUT type=hidden name=ID_" + sNameAdjun + " id=ID_" + sNameAdjun + " >"
		sInputAdjunX += "<INPUT type=hidden name=IDPortal_" + sNameAdjun + " id=IDPortal_" + sNameAdjun + " >"
		sInputAdjunX += "<INPUT type=hidden name=NOM_" + sNameAdjun + " id=NOM_" + sNameAdjun + " >"
		sInputAdjunX += "<INPUT type=hidden name=COM_" + sNameAdjun + " id=COM_" + sNameAdjun + " >"
		sInputAdjunX += "<INPUT type=hidden name=DEL_" + sNameAdjun + " id=DEL_" + sNameAdjun + " >"
		sInputAdjunX += "<INPUT type=hidden name=MOD_" + sNameAdjun + " id=MOD_" + sNameAdjun + " >"
		sInputAdjunX += "<INPUT type=hidden name=SIZE_" + sNameAdjun + " id=SIZE_" + sNameAdjun + " >"
        document.getElementById("divOfertaAEnviar").innerHTML += sInputAdjunX
		t = eval("f.ID_" +  sNameAdjun )
		t.value = vAdjun.ID==null?"":vAdjun.ID
		t = eval("f.IDPortal_" +  sNameAdjun )
		t.value = vAdjun.idPortal==null?"":vAdjun.idPortal
		t = eval("f.NOM_" +  sNameAdjun )
		t.value = vAdjun.nombre
		t = eval("f.COM_" +  sNameAdjun )
		t.value = vAdjun.comentario
		t = eval("f.DEL_" +  sNameAdjun )
		t.value = vAdjun.deleted ? 1 : 0
		t = eval("f.MOD_" +  sNameAdjun )
		t.value = vAdjun.modified ? 1 : 0
		t = eval("f.SIZE_" +  sNameAdjun )
		t.value = vAdjun.size
		}
	}

sInputNumAdjuntosItem  = "<INPUT type=hidden name=ofeNumAdjuntosItem name=ofeNumAdjuntosItem>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputNumAdjuntosItem
f.ofeNumAdjuntosItem.value = j

sInputModoSubasta  = "<INPUT type=hidden name=txtModoSubasta name=txtModoSubasta>"
document.getElementById("divOfertaAEnviar").innerHTML += sInputModoSubasta
f.txtModoSubasta.value = proceso.subastaCargada?"1":"0"
document.forms["frmOfertaAEnviar"].submit()
if (<%=application("FSAL")%> == 1)
{
Ajax_FSALRegistrar5(fechaIniGuardarOferta,"<%=sIDPagina%>".replace("cumpOferta.asp","guardaroferta.asp"),sIdRegistroGuardarOferta); //FFCLIPREPAR al final
}
}
/*
''' <summary>
''' Descarga el proceso en un excel
''' </summary>
''' <param name="bConfirm">true</param>
''' <remarks>Llamada desde: imgExportar/onclick ; Tiempo m�áximo: 0</remarks>*/
function descargarExcel(bConfirm)
{
window.open('<%=application("RUTASEGURA")%>script/solicitudesoferta/excel.asp','_blank','top=100,left=150,width=400,height=200,location=no,menubar=no,toolbar=no,resizable=no,addressbar=no')
}
/*
''' <summary>
''' imprimir la Oferta
''' </summary>
''' <param name="bConfirm">false->solicitudesoferta\avisoguardar.asp  true->solicitudesoferta\cumpoferta.asp(</param>
''' <remarks>Llamada desde: solicitudesoferta\avisoguardar.asp  solicitudesoferta\cumpoferta.asp(; Tiempo máximo: 0</remarks>*/
function imprimirOferta(bConfirm)
{
    if (bConfirm)
	{
	window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/avisoguardar.asp?target=imprimir","_blank","top=60,left=20,width=550,height=330,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no")
	return
	}
	if (<%=application("FSAL")%> == 1)
	{
    fechaImpOferta=devolverUTCFechaFSAL();  //se registra la fecha FICLIPREPAR al inicio de la ejecucion
    sIdRegistroImpOferta=devolverIdRegistroFSAL();
	}
	var lstMon
	var codMon
	
	if (document.forms["frmOfertaAEnviar"].lstMonOfe)
		{
		lstMon = document.forms["frmOfertaAEnviar"].lstMonOfe
		codMon = lstMon[lstMon.selectedIndex].value
		denMon = oMonedas[lstMon.selectedIndex].den 
		equiv = lstMon[lstMon.selectedIndex].equiv
		}
	else
		{
		denMon = gDenMon
		codMon=gCodMon
		equiv = ""
		}

    winEspera = window.open("<%=application("RUTASEGURA")%>script/common/winEspera.asp?msg=<%=server.urlEncode(den(64))%>", "_blank", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
    if (winEspera != null){
        while (winEspera.document.images["imgReloj"])
	    {
	    }
    }
    if (<%=application("FSAL")%> == 1)
    {
    Ajax_FSALRegistrar5(fechaImpOferta,"<%=sIDPagina%>".replace("cumpOferta.asp","imprimirOferta.asp"),sIdRegistroImpOferta); //FFCLIPREPAR al final
    window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/imprimirOferta.asp?codMon=" + codMon + "&fechaIniFSAL7=" + fechaImpOferta + "&sIdRegistroFSAL7=" + sIdRegistroImpOferta + "&denMon=" + denMon + "&equiv=" + equiv + "&NumOfe=<%=oOferta.num%>&IdPortal=<%=oOferta.IdPortal%>","_blank","top=100,left=150,width=400,height=100,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no,scrollbars=yes")
    }
    else
    {
    window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/imprimirOferta.asp?codMon=" + codMon + "&denMon=" + denMon + "&equiv=" + equiv + "&NumOfe=<%=oOferta.num%>&IdPortal=<%=oOferta.IdPortal%>","_blank","top=100,left=150,width=400,height=100,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no,scrollbars=yes")
    }
    if (winEspera != null){
        winEspera.focus()
    }
}

/*
''' <summary>
''' Descarga las especificaciones de todo el proceso
''' </summary>
''' <remarks>Llamada desde: imgdescargar/onclick ; Tiempo máximo: 0</remarks>*/
function descargarEspecific()
{
  winEspec = window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/descargaespecific.asp?denproceso=<%=server.urlEncode(oproceso.den)%>","_blank","top=60,left=20,width=550,height=240,location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars=no,titlebar=no")
}

/*
''' <summary>
''' Lanzar la descarga de un excel con la oferta
''' </summary>      
''' <remarks>Llamada desde: Nadie?? ; Tiempo máximo: 0</remarks>*/
function descargar()
{
winEspera = window.open("<%=application("RUTASEGURA")%>script/common/winEspera.asp?continua=continuarDescarga", "_blank", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
while (winEspera.document.images["imgReloj"])
	{
	}
}

/*
''' <summary>
''' Lanzar la descarga de un excel con la oferta
''' </summary>   
''' <returns>Nada</returns>
''' <remarks>Llamada desde:winEspera.asp ; Tiempo mximo:0</remarks>
*/
function continuarDescarga()
{
if (<%=application("FSAL")%> == 1)
{
fechadownOferExcel=devolverUTCFechaFSAL();  //se registra la fecha FICLIPREPAR al inicio de la ejecucion
sIdRegistrodownOferExcel=devolverIdRegistroFSAL();
}
var FechaLocal
var otz
var tzoaux
FechaLocal = new Date()
otz = FechaLocal.getTimezoneOffset()
tzoaux=<%=utcservidor%>

if (<%=application("FSAL")%> == 1)
{ 
Ajax_FSALRegistrar5(fechadownOferExcel,"<%=sIDPagina%>".replace("cumpOferta.asp","downloadofertaexcel.asp"),sIdRegistrodownOferExcel); //FFCLIPREPAR al final
window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/downloadofertaexcel.asp?download=1&fechaIniFSAL7=" + fechadownOferExcel + "&sIdRegistroFSAL7=" + sIdRegistrodownOferExcel + "&DifUtc=" + tzoaux + "&DifServ=" + otz,"fraOfertaServer")
}
else
{
window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/downloadofertaexcel.asp?download=1&DifUtc=" + tzoaux + "&DifServ=" + otz,"fraOfertaServer")
}

}




function escribirCabecera(linea) {

	if (!document.getElementById("divTotalOfertaSubasta") && document.getElementById("tblSubasta"))
				{
				//Se pasa con timeOut ya que sino al cambiar la cantidad y pulsar directamente en el boton de pujar
				//no coge el evento onclick!!!!!!!	
	oTR = document.getElementById("tblSubasta").insertRow(0)
	oTR.height= 15
	oTD = oTR.insertCell(-1)
	ha=52
	strDivs = "<div name=divTotalOfertaSubasta id=divTotalOfertaSubasta style='overflow:hidden;'></div>" 
    oTD.innerHTML +=strDivs
	document.getElementById("divApartadoSPuja").style.top = 50

	resize()
	
	}
    
	if (document.getElementById("divTotalOfertaSubasta"))
		{
		document.getElementById("divTotalOfertaSubasta").innerHTML  = linea
		document.getElementById("divTotalOfertaSubasta").parentNode.className=sClase
		}

}


function DenominacionUni(uni)
{
/*'************************************************************
'*** rue 01/04/2009
'*** Descripci�n: Abre la pantalla de detalles de la unidad
'*** Par�metros de entrada: *****
'*** uni			unidad a mostrar
'***                                               *****                    
'*** Llamada desde: campos de tipo unidad de los items de la oferta  						*****                    
'** Tiempo m�ximo:   						*****                    
'************************************************************
*/

	winUP=window.open("<%=application("RUTASEGURA")%>script/common/DenominacionUP.asp?Idioma=<%=Idioma%>&Cod=" + Var2Param(uni) + "&Oferta=1" ,"_blank","width=400,height=130,location=no,menubar=no,toolbar=no,resizable=no,addressbar=no scrollbars=yes")
	winUP.focus()
}




function MostrarGanadorProceso (prove,precio) 
{
// Actualiza el proveedor y/o la oferta en modo proceso
// Parametro prove: Nombre del proveedor que va ganando la subasta
// Parametro precio: Precio de la oferta que va ganando la subasta
// Establece los datos en os DIVs correspondientes seg�n las condiciones de visualizaci�n (verdetallePujas, verProveganador...)
    var strDivs = ""
    var title = ""
    <%if oProceso.modo = 1  then 'Modo Proceso %>
        <% if oProceso.verproveganador or oProceso.verprecioganador  then %>
            if (bAbierta && !bSobreCerrado && !sinPujas)
            {
                if ((prove != "") && (precio != 0 ))
                {
                    <% if oProceso.verDetallePujas AND oProceso.verprecioganador then%>
    			        strDivs += "<div style='cursor:pointer' modo='1' sDescr='" + proceso.den + "' onclick='verDetallePujas(this)'>"
                    <%end if%>
                    title = "("
                    strDivs += "("
                    <% if oProceso.verproveganador then %>
                            if (prove != "") {
                                strDivs += "<%=den(121)%> " + prove
                                title += "<%=den(121)%> " + prove
                            }
                    <%end if%>
                    <% if oProceso.verproveganador and oProceso.verprecioganador  then %>
                        strDivs += "&nbsp;&nbsp;"
                        title += "&nbsp;&nbsp;"
                    <%end if%>
                    <% if oProceso.verprecioganador then %>
                        <% if oProceso.verproveganador then  %>
                            if (precio != 0) {
                                strDivs += "<%=den(58)%> " + num2str(precio,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
                                title += "<%=den(58)%> " + num2str(precio,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
                            }
                        <%else %>
                            if (precio != 0){
                                 strDivs += "<%=den(116)%> " + num2str(precio,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
                                 title += "<%=den(116)%> " + num2str(precio,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
                            }
                        <%end if%>
                    <%end if%>
                    strDivs += ")"
                    title += ")"
                    <% if oProceso.verDetallePujas AND oProceso.verprecioganador then %>
    			        strDivs += "</div>"
                    <%end if%>

                }
            }
        <%end if%>
    <%end if%>

    if (document.getElementById("DivGanadorProceso")) {
	    document.getElementById("DivGanadorProceso").innerHTML = strDivs
        document.getElementById("DivGanadorProceso").title = title
    }
}


function MostrarGanadorGrupo (grupo,prove,precio) 
{
// Establece los datos en los DIVs correspondientes al grupo indicado seg�n las condiciones de visualizaci�n
// parametro grupo: identificador del grupo
// Parametro prove: Nombre del proveedor que va ganando la subasta
// Parametro precio: Precio de la oferta que va ganando la subasta

    var idgrupo
    var g

    for (g=0;g<proceso.grupos.length && proceso.grupos[g].cod != grupo;g++)
	    {
	    }

    if (proceso.grupos[g].cod==grupo ) idgrupo = proceso.grupos[g].id

    var strDivs = ""
    <%if oProceso.modo = 2  then 'Modo Grupo %>
        <% if oProceso.verproveganador or oProceso.verprecioganador  then %>
            if (bAbierta && !bSobreCerrado && !sinPujas)
            {
                <% if oProceso.verDetallePujas AND oProceso.verprecioganador then%>
    			    strDivs += "<div style='cursor:pointer' modo='2' idgrupo='" + idgrupo + "' codgrupo='" + grupo + "'  sDescr='" + grupo + " - " + proceso.grupos[g].den + "' onclick='verDetallePujas(this)'>"
                <%end if%>
                strDivs += "("
                <% if oProceso.verproveganador then %>
                    if (prove!="") strDivs += " <%=den(121)%>  " + prove
                <%end if%>
                <% if oProceso.verproveganador and oProceso.verprecioganador  then %>
                    strDivs += "&nbsp;&nbsp;"
                <%end if%>
                <% if oProceso.verprecioganador then %>
                    <% if oProceso.verproveganador then  %>
                        if (precio != 0) strDivs += "<%=den(58)%> " + num2str(precio,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
                    <%else %>
                        if (precio != 0) strDivs += "<%=den(116)%> " + num2str(precio,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
                    <%end if%>
                    
                <%end if%>
                strDivs += ")"
                <% if oProceso.verDetallePujas AND oProceso.verprecioganador then %>
    			    strDivs += "</div>"
                <%end if%>

            }
        <%end if%>
    <%end if%>

    if ((precio==0) && (prove=="")) strDivs=""

    if (document.getElementById("divGanadorPujaGrupo_" + grupo)) {
	    document.getElementById("divGanadorPujaGrupo_" + grupo).innerHTML = strDivs
    }
}




function CargarOfertaGanadora()
{
// realiza la llamada al objeto proceso m�todo igualarOfertasMinimas para que se conecte al portal y obtenga la oferta m�nima existente para el proceso

    document.body.style.cursor = "wait"   
	proceso.igualaOfertasMinimas("fraIgualarPujas")
    document.body.style.cursor = "auto"
}



function ImporteNeto(Item,vGr,precioUnitarioInicial,Cantidad)
// hace un c�lculo del importe neto de un item respecto a su precio unitario inicial
// aplicando en caso de que proceda los costes-descuentos correspondientes sobre el precio unitario y el total.
{

    var vObj=proceso
    var Entrar=0
    var PrimerRegistro=0
    var importeneto
    var aplicaTotalItem   = 2
    var aplicaPrecioItem  = 3

    var importeOfertabruto = 0
    var impAnterior
    var impCalculado
    var vGru
    var bGrupoTratar=0
    var bRecalculaServidor=0
    var vGrupo
    var iAmbitoOferta = 1
    var iAmbitoGrupo = 2
    var iAmbitoItem = 3

    var importeBrutoGrupo = 0
    var importeNetoGrupo = 0
    var gruposRecalc = new Array()

    PrimerRegistro=0
    precioUnitarioFinal = 0

    //si no hay precio unitario inicial no recalcula
    if (!precioUnitarioInicial)
        precioUnitarioInicial = 0
    //el importe parcial es el precio unitario inicial
    impParcial = precioUnitarioInicial

    //el precio neto es el precio unitario inicial (salvo que haya costes / descuentos aplicados al precio, en cuayo caso se recalcular�)
    precioNeto = precioUnitarioInicial

    
    for(i=0;i<vGr.costesDescuentosItems.length;i++)
    {
            
        //En el momento en que el coste/descuento aplique al total del item por vez primera, calculamos el PU final y el importe
        // este importe se convierte en el nuevo importe a partir del cual hay que hacer los c�lculos
        if(vGr.costesDescuentosItems[i].aplicar==aplicaTotalItem && PrimerRegistro==0)
        {
            //el precio unitario final est� guardado en la variable impParcial
            precioUnitarioFinal = impParcial

            //el nuevo importe parcial es el precio unitario final por la cantidad
                impParcial *= Cantidad

            //el nuevo precio neto (tras aplicar los costes/descuentos ser� este precio unitario final) y se guarda en el item
            precioNeto = precioUnitarioFinal

            PrimerRegistro=1
        }
        // obtenemos el objeto que contiene el valor del coste/descuento y los importes parciales
        if (vGr.costesDescuentosItems[i].ambito == iAmbitoOferta)
                objCosteDescValor = obtenerCosteDescValor(proceso.costesDescuentos,vGr.costesDescuentosItems[i])
        else if (vGr.costesDescuentosItems[i].ambito == iAmbitoGrupo)
                objCosteDescValor = obtenerCosteDescValor(vGr.costesDescuentos,vGr.costesDescuentosItems[i])
        else  //if (vGr.costesDescuentosItems[i].ambito == iAmbitoItem)
                objCosteDescValor = obtenerCosteDescValor(Item.costesDescuentosOfertados,vGr.costesDescuentosItems[i])
        // Se calcula el nuevo importe parcial
            if(objCosteDescValor.valor)
                impParcial=obtenerImporteParcial(vGr.costesDescuentosItems[i].operacion,objCosteDescValor.valor,impParcial) 
            
        //El nuevo importe parcial del coste/descuento ser� el calculado en el paso anterior
        objCosteDescValor.importeParcial = impParcial   
    }


    if(PrimerRegistro==1)   //si hay costes/desc que se aplican al total de item, se mantiene el importe el importe neto final del item es el �ltimo calculado
        importeNeto = impParcial
    else  //si no hay costes/desc que se aplican al total de item, el importe neto del item es el precioUnitarioFinal * cantidad    
       importeNeto = impParcial * Cantidad

    return importeNeto
}



var reducirPreciosGrupo = ""  
// Se utiliza en las funciones ReducirPrecios y AplicarReducirPrecios para determinar a los �tems de qu� grupo se aplica el descuento. 
// Si es vac�o se aplicar� a todos los items del proceso
// Se establece en la carga del DIV que contiene el Slider




function ReducirPrecios(descuento)
{
    // Realiza un c�lculo del importe total de la oferta resultante de aplicar el descuento indicado a un cojunto de items.
    // Y devuelve el importe total de la oferta resultante
    // param: descuento (float)

    if (descuento != 0)
    {
        var grupoOriginal; grupoOriginal = new Array()
        var grupoDiff ;grupoDiff = new Array()
        var grupoFinal ; grupoFinal = new Array()
        var g
        // almacenamos los importes de antes y de despu�s de aplicar el descuento, adem�s de la diferencias (descuento) de cada grupo
        for (g=0;g<proceso.grupos.length;g++)
        {
            grupoOriginal[g] = proceso.grupos[g].importe
            grupoDiff[g] = 0
            grupoFinal[g] = 0
        }
        // procesamos los descuentos de los �tems
        var i
        for (i=0;i<itemsPuja.length;i++)
        {
            // En caso de no indicar grupo, se aplicar� el descuento a todos los �tems del proceso,
            // En caso contrario, se aplicar� �nicamente a los �tems del grupo indicado.
            if ((reducirPreciosGrupo=="") || (reducirPreciosGrupo==itemsPuja[i].grupo))
            {
               //localizamos el grupo g en el Array
            for (g=0;g<proceso.grupos.length && proceso.grupos[g].cod != itemsPuja[i].grupo;g++)
	            {
	            }
        
                pBruto = (itemsPuja[i].precio==null?0:itemsPuja[i].precio)
                npBruto  = pBruto * (1- (descuento/100))
                iNeto = (itemsPuja[i].importeNeto==null?0:itemsPuja[i].importeNeto)
                // calculamos el importe neto para el nuevo precio unitario bruto.
                niNeto =  ImporteNeto(itemsPuja[i],proceso.grupos[g],npBruto,itemsPuja[i].cant)
                // anotamos el nuevo importe bruto para el grupo
                grupoDiff[g] += (iNeto - niNeto)
                grupoFinal[g] += niNeto
            }
        }



        // actualizamos los importes netos de los grupos en base a los descuentos realizados en los items
        var sumagrupos = 0
        for (g=0;g<proceso.grupos.length;g++)
        {
            if (grupoDiff[g] != 0)
            {
                for (iCostes=0;iCostes<proceso.CostesDescTotalGrupo.length;iCostes++)
                {
                    idCosteDescuento = proceso.CostesDescTotalGrupo[iCostes].paid
                    if(proceso.CostesDescTotalGrupo[iCostes].ambito==1)
                        objCosteDescValor = obtenerCosteDescValor(proceso.costesDescuentos,proceso.CostesDescTotalGrupo[iCostes])
                    else
                        objCosteDescValor = obtenerCosteDescValor(proceso.grupos[g].costesDescuentos,proceso.CostesDescTotalGrupo[iCostes])
                    valor = (objCosteDescValor.valor==null?0:objCosteDescValor.valor)
                    impCalculado=obtenerImporteParcial(proceso.CostesDescTotalGrupo[iCostes].operacion,valor,grupoFinal[g]) 
                    grupoFinal[g] = impCalculado
                } 
            }
            else
            {
                grupoFinal[g] = grupoOriginal[g]
            }

            sumagrupos+= grupoFinal[g]
        }

        if(reducirPreciosGrupo!="")
        {
            for (g=0;g<proceso.grupos.length && proceso.grupos[g].cod != reducirPreciosGrupo;g++){}
            document.getElementById("aplicandodescuentogrupo").innerHTML= num2str(grupoFinal[g],vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
            
        }



        // aplicar los costes-descuentos de ambito proceso sobre el total de la oferta
        for (iCostes=0;iCostes<proceso.CostesDescTotalOferta.length;iCostes++)
        {
            idCosteDescuento = proceso.CostesDescTotalOferta[iCostes].paid
            objCosteDescValor = obtenerCosteDescValor(proceso.costesDescuentos,proceso.CostesDescTotalOferta[iCostes])
            valor = (objCosteDescValor.valor==null?0:objCosteDescValor.valor)
            impCalculado=obtenerImporteParcial(proceso.CostesDescTotalOferta[iCostes].operacion,valor,sumagrupos) 
            sumagrupos = impCalculado
        } 
        return num2str(sumagrupos,vdecimalfmt,vthousanfmt,vprecisionfmt)
    }
    else
    // descuento 0
    {
        if(reducirPreciosGrupo!="")
        {
    
            for (g=0;g<proceso.grupos.length && proceso.grupos[g].cod != reducirPreciosGrupo;g++){}
            document.getElementById("aplicandodescuentogrupo").innerHTML= num2str(proceso.grupos[g].importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
        }
        return num2str(proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt)
    }
}



//''' <summary>
//''' Aplica la reducci�ón porcentual de los importes a los items del grupo/proceso
//''' </summary>
//''' <param name="descuento">% de rebaja</param>
//''' <remarks>Llamada desde: Capa con el control Slider, bt Aplicar ; Tiempo máximo: 0</remarks>
function AplicarReducirPrecios(descuento)
{
var vprecisionfmt //variable local para establecer los inputs con total precisin
vprecisionfmt = 1000

        var fDescuento
        fDescuento = str2num(descuento,vdecimalfmt,vthousanfmt,vprecisionfmt)
        if (fDescuento >0)
        {
            var grupoOriginal; grupoOriginal = new Array()
            var grupoDiff ;grupoDiff = new Array()
            var grupoFinal ; grupoFinal = new Array()
            var g
            for (g=0;g<proceso.grupos.length;g++)
            {
                grupoOriginal[g] = proceso.grupos[g].importe
                grupoDiff[g] = 0
                grupoFinal[g] = 0
            }


            var i
            for (i=0;i<itemsPuja.length;i++)
            {
                if ((reducirPreciosGrupo=="") || (reducirPreciosGrupo==itemsPuja[i].grupo))
                {
                   //localizamos el grupo g en el Array
                    for (g=0;g<proceso.grupos.length && proceso.grupos[g].cod != itemsPuja[i].grupo;g++)
                    {
                    }
        
                    //pBruto = (itemsPuja[i].precio==null?0:itemsPuja[i].precio)
                    if (itemsPuja[i].precio!=null) {
                    pBruto = itemsPuja[i].precio;
					if (String(pBruto).indexOf(".") > 0)
						var ndec = String(pBruto).substr(String(pBruto).indexOf(".")+1).length  // n de decimales que tiene el valor de origen
					else
						ndec = 0
					if (ndec<vprecisionfmt) ndec=vprecisionfmt
					if (ndec>20) ndec=20
                    npBruto  = pBruto * (1- (fDescuento/100))
					npBruto = npBruto.toFixed(ndec)
                    iNeto = (itemsPuja[i].importeNeto==null?0:itemsPuja[i].importeNeto)

                    vIDItem = itemsPuja[i].id
                    vGrupo = proceso.grupos[g].cod

                    vGr = proceso.grupos[g]

                    if (vGr.costesDescuentosItems.length)
                    {
                        MostrarDivCostesDescItem(vIDItem, vGrupo, "txtPujaPU_")    
                        var nombreinput = "txtPujaPUI_" + vIDItem
                        itemsPuja[i].precio = npBruto
                        document.getElementById(nombreinput).value =  num2str(npBruto,vdecimalfmt, vthousanfmt, vprecisionfmt)
                        document.getElementById("txtPujaPUBruto_" + vIDItem).value =  num2str(npBruto,vdecimalfmt, vthousanfmt, vprecisionfmt)
                        var objinput = document.getElementById(nombreinput)
                        linkRecalcularItem("txtPujaPU_")
                        proceso.estado = 4						
                        CerrarDivCostesDescItem(vIDItem, vGrupo, "txtPujaPU_", 1,true,-1,0)
                    }
                    else
                    {
                        var vPrec
                        var nombreinput = "txtPujaPU_"  + vIDItem
                        itemsPuja[i].precio = npBruto
                        document.getElementById(nombreinput).value =  num2str(npBruto,vdecimalfmt, vthousanfmt, vprecisionfmt)
                        document.getElementById("txtPujaPUBruto_" + vIDItem).value =  num2str(npBruto,vdecimalfmt, vthousanfmt, vprecisionfmt)
                        var objinput = document.getElementById(nombreinput)
                        validarNumeroB("txtPujaPU_" + vGrupo + "_"  + vIDItem, itemsPuja[i], objinput,vdecimalfmt, vthousanfmt,vprecisionfmt)        
                        proceso.estado = 4

                    }
                }
            }
        }
    }
}



//''' <summary>
//''' Muestra la capa con el control deslizante para poder aplicar descuentos
//''' </summary>
//''' <param name="grupo">grupo</param>
//''' <remarks>Llamada desde: js\actualizarsubasta.asp       js\cargarsubasta.asp; Tiempo m�ximo: 0,2</remarks>
function mostrarSlider(grupo)
{  
    
    cerrarSlider()
    // Se establece el grupo al que se aplicar�n los descuentos. Si es vac�o se aplicar� a todo el proceso
    reducirPreciosGrupo=grupo

    // Se calcula la posici�n absoluta para mostrar la capa
    var elm = eval("document.getElementById(\"BajarPrecios" + grupo + "\")" )
    if( typeof( elm.offsetParent ) != 'undefined' ) 
    {
        var originalElement = elm
        for(coordx = 0, coordy = 0; elm; elm = elm.offsetParent ) 
        {
            coordx += elm.offsetLeft
            coordy += elm.offsetTop
            //Para el caso de scroll
            if( elm != originalElement && elm != document.body && elm != document.documentElement ) 
            {
                coordx -= elm.scrollLeft
                coordy -= elm.scrollTop
            }
        }
    } 
    else 
    {
        coordx = elm.x
        coordy = elm.y
    }
    var calcPx
    calcPx = coordx - 120
    document.getElementById("DivDescuento").style.left= calcPx + 'px';
    calcPx = coordy + 20
    document.getElementById("DivDescuento").style.top= calcPx + 'px';

    // Se muestra la capa y el control Slider
    document.getElementById("DivDescuento").style.visibility='visible'
    document.getElementById('sl0base').style.visibility='visible'
    document.getElementById('sl0slider').style.visibility='visible'

    if (grupo!="")
    {
	    for (g=0;g<proceso.grupos.length && proceso.grupos[g].cod!=grupo;g++){}	    
        document.getElementById("aplicandodescuentogrupo").innerHTML= proceso.grupos[g].importe + " " + gCodMon
        document.getElementById("aplicandodescuentogrupo").style.visibility="visible"
        document.getElementById("aplicodescuentogrupo").style.visibility="visible"
    }
    else
    {
        document.getElementById("aplicodescuentogrupo").style.visibility="hidden"
    }
}

function cerrarSlider()
{
    // Oculta la capa con el control deslizante de descuento.
    
    document.getElementById("DivDescuento").style.visibility='hidden'
    document.getElementById('sl0base').style.visibility='hidden'
    document.getElementById('sl0slider').style.visibility='hidden'
    document.getElementById('aplicodescuentogrupo').style.visibility='hidden'
    document.getElementById('aplicandodescuentogrupo').style.visibility='hidden'

    // Se inicializa a 0 para la pr�xima vez que se muestre
    sl.f_setValue(0)
    document.getElementById("aplicandodescuento").innerHTML = num2str(proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
}

var ultimoDescuento = 0
// almaceno el �ltimo descuento introducido para en el onkeyup, realizar el descuento s�lo cuando haya cambiado el valor.


function slideKeyPress(element,event)
// Se pulsa una tecla en el cuadro de texto del control deslizante
{
    var mN
    mN = mascaraNumero(element,vdecimalfmt,vthousanfmt,event)     
    return mN
}

function slideKeyUp(element)
// Se trata la pulsaci�n de una tecla en el cuadro de texto del control deslizante
{
var valor
    valor = (str2num(element.value,vdecimalfmt,vthousanfmt)==null?0:str2num(element.value,vdecimalfmt,vthousanfmt))

    if (valor <0)
    {
        element.value = 0
        valor = 0
    }
    if (valor > 100)
    {
        element.value = 100
        valor = 100
    }

    if (valor!=ultimoDescuento)
    {        
        document.getElementById('aplicandodescuento').innerHTML = ReducirPrecios(valor) + " " + gCodMon
        sl.f_setValue(valor,'',true);
    } 
    ultimoDescuento=valor

}



</script>


<!--Capa con el control Slider para aplicar descuentos  -->
<div id="DivDescuento" style="position: absolute; visibility: hidden; z-index: 1000; top: 10px; left: 10px; width: 240px; background-color: #EEEEEE; border: solid 1px #AAAAAA;">
    <form action="slider" method="get" name="sliderForm" id="sliderForm">
        <table border="0" cellspacing="0" cellpadding="5" align="center" width="240">
            <tr>
                <td align="right" valign="top" width="12" class="filaPar">&nbsp;</td>
                <td align="right" width="50%" class="filaPar"><%=den(122)%></td>
                <td align="left" width="50%" class="filaPar">
                    <input type="text" name="porcentajedescuento" id="porcentajedescuento" size="5" style="text-align: right;" onkeypress="return slideKeyPress(this,event)" onkeyup="slideKeyUp(this)">
                    %</td>
                <td align="right" valign="top" width="12" class="filaPar"><a href="javascript:cerrarSlider();" onmouseover="document.getElementById('sliderx').src='images/xb.gif';" onmouseout="document.getElementById('sliderx').src='images/x.gif';">
                    <img id="sliderx" src="images/x.gif" border="0" /></a> </td>
            </tr>
            <tr>
                <td colspan="4" align="center" class="filaPar">
                    <table border="0" cellspacing="0" cellpadding="0" width="120">
                        <tr>

                            <td>
                                <!--Control Slider -->
                                <script language="JavaScript">
                                    var sl
                                    var A_TPL = {
                                        'b_vertical': false,
                                        'b_watch': true,
                                        'n_controlWidth': 120,
                                        'n_controlHeight': 16,
                                        'n_sliderWidth': 16,
                                        'n_sliderHeight': 15,
                                        'n_pathLeft': 1,
                                        'n_pathTop': 1,
                                        'n_pathLength': 103,
                                        's_imgControl': 'images/sldr2h_bg.gif',
                                        's_imgSlider': 'images/sldr2h_sl.gif',
                                        'n_zIndex': 1
                                    }

                                    var A_INIT1 = {
                                        's_form': 'sliderForm',
                                        's_name': 'porcentajedescuento',
                                        'n_minValue': 0,
                                        'n_maxValue': 100,
                                        'n_value': 0,
                                        'n_step': 1
                                    }
                                    sl = new slider(A_INIT1, A_TPL);
                                </script>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>



            <tr>
                <td align="right" valign="top" width="12" class="filaPar">&nbsp;</td>
                <td align="right" width="50%" class="filaPar">
                    <div id="aplicodescuentogrupo" style="visibility: hidden">
                        <%=den(123)%>
                    </div>
                    <%=den(113)%>
                </td>
                <td align="left" width="50%" class="filaPar">
                    <div id="aplicandodescuentogrupo" style="visibility: visible">&nbsp;</div>
                    <div id="aplicandodescuento">
                        <script>document.write(num2str(proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt));</script>
                    </div>
                </td>
                <td align="right" valign="top" width="12" class="filaPar">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4" align="center" class="filaPar">
                    <input type="button" value="<%=den(114)%>" onclick="AplicarReducirPrecios(document.getElementById('porcentajedescuento').value);cerrarSlider();" />
                </td>
            </tr>
        </table>

    </form>
</div>

<script>

function recortar(cadena, lng)
{
    var cadenaDevuelta
    if(lng > cadena.length + 3)
        cadenaDevuelta = cadena
    else
        cadenaDevuelta = cadena.substring(0,lng) + '...'
    return cadenaDevuelta
}

function actualizarSubasta() 
{
//Recarga la informaci�n de grupos e items de la oferta

    for (g=0;g<proceso.grupos.length;g++)
    {
        document.getElementById("divArticuloPuja" + proceso.grupos[g].cod ).innerHTML = ""
    }
    window.open("<%=Application("RUTASEGURA")%>script/solicitudesoferta/js/actualizarsubasta.asp?anyo=" + proceso.anyo + "&gmn1=" + proceso.gmn1 + "&proce=" + proceso.cod + "&ofe=" + proceso.num + "&mon=" + proceso.mon.cod + "&eq=" + proceso.mon.equiv, "fraOfertaServer", "")
}


function botonRestaurar() 
// Establece la visisbilidad del bot�n restaurar seg�n las condicion del estado del proceso
{
    if (proceso.estado == 4) 
    {
        if (document.getElementById("Restaurar")) document.getElementById("Restaurar").style.display = "block"
    }
    else 
    {
        if (document.getElementById("Restaurar")) document.getElementById("Restaurar").style.display = "none"
    }  
        
}

function botonGanadora() 
// Establece la visisbilidad del bot�n ganadora seg�n las condiciones visualizaci�n y de comienzo de la subasta
{
    if (document.getElementById("Ganadora")) document.getElementById("Ganadora").style.display = "none"
    <%
     if oProceso.verprecioganador  then %>
            if (bAbierta && !bSobreCerrado && !sinPujas) 
            {
                if (document.getElementById("Ganadora")) document.getElementById("Ganadora").style.display = "block"
            }
    <%end if%> 
}



function ReglasSubasta()
// Descarga la documentaci�n de las reglas de la subasta
// Se hace una llamada a una p�gina de PM ya que la documentaci�n est� guardada en SQL-filestream y para obtener los ficheros se utiliza .NET
{

	window.open("<%=Application("RUTASEGURA")%>script/PMPortal/script/_common/fileStream.aspx?anyo=" + proceso.anyo + "&gmn1=" + proceso.gmn1 + "&proce=" + proceso.cod + "&lCiaComp=<%=CiaComp%>&op=reglasSubasta", "fraOfertaServer","")


}


//Funci�n que comprueba la fecha de f�n de la subasta y devuelve cierto en caso de que haya cambiado
// se llama justo en el momento de fin de subasta para comprobar que no se ha cambiado la fecha en los �ltimos 10 segundos.
function ComprobarFechaFin() {
    crearObjetoXmlHttpRequest()
    AjaxSync("js/xmlfechafin.asp?anyo=" + proceso.anyo + "&gmn1=" + proceso.gmn1 + "&proce=" + proceso.cod)
    var XML = xmlobj.responseXML.getElementsByTagName("fullstep");
    var xmlfeclimofe = XML[0].getElementsByTagName("feclimofe")[0].firstChild.nodeValue;
    var feclimofe = UTCtoClient(eval(xmlfeclimofe))
    return (feclimofe - vFechaLimite != 0)
}



</script>

<!--
<div style="top:500;left:500; z-index:1000; position:absolute;">
    <a href="#" onclick="actualizarSubasta();return false;">actualizarSubasta </a>
</div>
-->

<%
server.ScriptTimeout = oldTimeOut
set oOferta = nothing
set oProceso = nothing
set oRaiz = nothing
%>
<!--#include file="../common/fsal_2.asp"-->
</html>
