﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->
<%

''' <summary>
''' Muestra el resultado de las pujas realizadas. 
'''	Si pargen_interno.mostrar_popupsubasta=1 lanza otra pantalla para mostrar el resultado
'''	Si pargen_interno.mostrar_popupsubasta=0 muestra el resultado en esta pantalla y las fechashora apertura/fin deben ir en hora local.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cumpoferta.asp; Tiempo máximo: 0</remarks>

Anyo=cint(Request ("Anyo"))
Gmn1=Request("GMN1")
Proce=clng(Request("Proce"))

dim oRaiz

Idioma = Request.Cookies("USU_IDIOMA")
set oRaiz=validarUsuario(Idioma,true,true,0)

set paramgen = oRaiz.Sesion.Parametros


%>

<html>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/menu.asp"></script>
<script src="../common/formatos.js"></script>


<head>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%if request("noabierta") = "1" or paramgen.NoMostrarPopUp then%>
<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
    document.getElementById('tablemenu').style.display = 'block';
}
</script>
<%else%>

<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
    document.getElementById('tablemenu').style.display = 'block';

    window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/cargarresultadopuja.asp?anyo=<%=anyo%>&gmn1=<%=gmn1%>&proce=<%=proce%>","_blank","top=50,left=50,width=670,height=500,location=no,scrollbars=yes,menubar=no,toolbar=no,resizable=yes,addressbar=no")
}
</script>
<%end if%>

</head>
<body topmargin="0" leftmargin="0" onload="init()">

<script>dibujaMenu(3)</script>


<%


CiaComp = oRaiz.Sesion.CiaComp
CiaCod = oRaiz.Sesion.CiaCod

if paramgen.NoMostrarPopUp then
	Set oProceso = oRaiz.Generar_CProceso
	oProceso.cargarProceso CiaComp, Idioma, Anyo, Gmn1, Proce
	
	DenProce=oProceso.Den
	
	datefmt=Request.Cookies("USU_DATEFMT")
	
	FechaI=oProceso.FechaApertura
	yFechaI = Year(FechaI)
	mFechaI = Month(FechaI)-1
	dFechaI = Day(FechaI)
	hFechaI = Hour(FechaI)
	miFechaI = Minute(FechaI)
	
	FechaF=oProceso.FechaMinimoLimOfertas
	yFechaF = Year(FechaF)
	mFechaF = Month(FechaF)-1
	dFechaF = Day(FechaF)
	hFechaF = Hour(FechaF)
	miFechaF = Minute(FechaF)	
	
	utcservidor= oraiz.UTCServidor	
	
	CiaProv=oRaiz.Sesion.CiaCodGs
	
	Pujas= oProceso.NumeroPujas (CiaComp,CiaProv)
	
	set oProceso = nothing
end if

dim den

den = devolverTextos(Idioma,90)	

Titulo = den(1)
Texto1 = den(2)
Texto2 = den(3)

if paramgen.NoMostrarPopUp then
	Texto1 = den(12)
	Texto3 = Texto2
	Texto2 = "&larr;&nbsp;" & Texto2	
end if

if not (request("noabierta") = "1") and not (paramgen.NoMostrarPopUp) then %>
	<h1>
	<%= Titulo%>&nbsp;<span class="negro"><%=Anyo & "/" & Gmn1 & "/" & Proce %></span>
	</h1>
<%end if%>


<br>
<%
if request("noabierta") = "1" then%>

	<p class="error"><%=den(4)%></p>

	<p class="error"><%=den(5)%></p>

<%elseif paramgen.NoMostrarPopUp then%>
	<table width="100%">
		<tr>
			<td colspan="3">
				<h1>
				<%=den(6) & " " & Anyo & "/" & Gmn1 & "/" & Proce & " " & DenProce%>
				</h1>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td> 
			<td width="48%">				
				<table width="100%" height="100%">
				<tr>
					<h1><%=Texto1%></h1>
				</tr>
				<tr>
					<td align="left" valign="left" width="10%">
						<img SRC="<%=application("RUTASEGURA")%>script/images/subastacerrada.jpg">
					</td>
					<td>
						<table>
							<tr>
									<td class="fmedia" width="42%" nowrap><%=den(7)%></td>
									<td class="fmedia"><div id="divI"></div></td>
							</tr>
							<tr>													
									<td class="fmedia" width="42%" nowrap><%=den(8)%></td>
									<td class="fmedia"><div id="divF"></div></td>								
							</tr>
							<tr>								
									<td colspan="2" class="fmedia"><%=den(9) & " " & Pujas%></td>
							</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
			<td width="29%">&nbsp;</td> 
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>			
		<tr>
			<td colspan="3">
				<h3><%=den(10)%></h3>			
			</td>
		</tr>		
		<tr>
			<td>&nbsp;</td>
			<td>				
				<table width="100%">
				<tr>
					<td width="77px">&nbsp;</td>
					<td>
						<h3><%=den(11)%></h3>
					</td>
				</tr>
				</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
				<p class="centro">
				<a LANGUAGE="javascript" onmousemove="window.status='<%=Texto3%>'" onmouseout="window.status=''" HREF="<%=application("RUTASEGURA")%>script/solicitudesoferta/solicitudesoferta11.asp?Idioma=<%=Idioma%>&amp;CiaComp=<%=CiaComp%>&amp;CiaCod=<%=CiaCod%>" target="default_main"> 
				<%=Texto2%>
				</a></p>
			</td>			
		</tr>
	</table>
<%else%>
	<p class="error"><%=Texto1%></p>
<%end if%>

<%if (not paramgen.NoMostrarPopUp) then%>
	<br>
	<p class="centro">
	<a LANGUAGE="javascript" onmousemove="window.status='<%=Texto2%>'" onmouseout="window.status=''" HREF="<%=application("RUTASEGURA")%>script/solicitudesoferta/solicitudesoferta11.asp?Idioma=<%=Idioma%>&amp;CiaComp=<%=CiaComp%>&amp;CiaCod=<%=CiaCod%>" target="default_main"> 
	<%=Texto2%>
	</a></p>
<%end if%>

<%if (paramgen.NoMostrarPopUp) then%>
<script>

    function UTCtoClient(FechaUTC) {
    //Convierte una fecha en formato UTC a la fecha en la franja horaria del navegador
        miFecha = new Date()
        tzo = miFecha.getTimezoneOffset()
        var fechaCliente
        fechaCliente = new Date(FechaUTC.getFullYear(), FechaUTC.getMonth(), FechaUTC.getDate(), FechaUTC.getHours(), FechaUTC.getMinutes() - tzo, FechaUTC.getSeconds())
        return fechaCliente
    }



	var diferencia
	var Inicio 
	Inicio = new Date()
	
	var tzo
	tzo= Inicio.getTimezoneOffset()

	var tzoaux
	var FecIni = new Date()
	var FecFin

	tzoaux=<%=utcservidor%>+tzo	
	tzo= parseInt(tzo/60)
	
	if (tzo == 0) 
	{
		diferencia =""
	}
	else 
	{
		diferencia= tzo.toString()
	}	

	var vDateFmt='<%=datefmt%>'
	
	FecIni=UTCtoClient(new Date (<%=yFechaI%>,<%=mFechaI%>,<%=dFechaI%>,<%=hFechaI%>,<%=miFechaI%>-tzoaux))
	FecFin=UTCtoClient(new Date(<%=yFechaF%>,<%=mFechaF%>,<%=dFechaF%>,<%=hFechaF%>,<%=miFechaF%>-tzoaux))	
	
	divI.innerText= date2str(FecIni,vDateFmt,true) 
	divF.innerText= date2str(FecFin,vDateFmt,true) 
	
</script>
<%end if%>

</body>

</html>
<%
set oRaiz = nothing

set paramgen = nothing
%>