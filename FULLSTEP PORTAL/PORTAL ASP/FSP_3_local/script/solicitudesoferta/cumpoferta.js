﻿/////////////////////
// V A R I A B L E S
/////////////////////
var dFechaInicialServer 
var dFechaInicialClient 
var sClase
var sEstado
var precioNum = ""
var precioNetoCalculado = 0
var bSinSync
var vdecimalfmt 
var vthousanfmt 
var vprecisionfmt
var vdatefmt
var imgAbrir
var imgEliminar
var proceso 
var tituloActual
var costesDescuentosImpParcial //estructura de costes/descuentos creada para almacenar los importes parciales de los de ámbito proceso o grupo que aplican a precio de ítem o total de ítem
var costesDescuentosImpParcialAnterior //estructura de costes/descuentos creada para almacenar (y poder recuperar posteriormente, para el cálculo posterior) los importes parciales de los de ámbito proceso o grupo que aplican a precio de ítem o total de ítem 
var undoPrecio //guarda el precio unitario para la función de Cancelar de ítem
var undoCostesDesc //guarda los costes/descuentos para la función de Cancelar de ítem
var undoEstado //guarda el estado para la función de Cancelar de ítem


/////////////////////
// F U N C I O N E S 
/////////////////////

//Convierte una fecha en formato UTC a la fecha en la franja horaria del navegador
function UTCtoClient(FechaUTC) {    
    miFecha=FechaUTC
	tzo = miFecha.getTimezoneOffset()
	var fechaCliente
	fechaCliente = new Date(FechaUTC.getFullYear(), FechaUTC.getMonth(), FechaUTC.getDate(), FechaUTC.getHours(), FechaUTC.getMinutes() - tzo, FechaUTC.getSeconds() )
	return fechaCliente
}

//Sincronizar Relojes Server-Cliente
function syncRelojes(dfecserver,dfecclient)
{

dFechaInicialServer = new Date(dfecserver)
dFechaInicialClient = new Date(dfecclient)

}

// Devuelve un array con las propiedades Width(sin scroll),Height(sin Scroll),Scrollx, ScrollY,Width(con Scroll),Heigth(con Scroll)
// Se utilizará para hacer los cálculos de posiciones de objetos en pantalla.
// Es compatible con Firefox
function getWindowData(){ 
    var widthViewport,heightViewport,xScroll,yScroll,widthTotal,heightTotal; 
    if (typeof window.innerWidth != 'undefined'){ 
        widthViewport= window.innerWidth-17; 
        heightViewport= window.innerHeight-17; 
    }else if(typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth !='undefined' && document.documentElement.clientWidth != 0){ 
        widthViewport=document.documentElement.clientWidth; 
        heightViewport=document.documentElement.clientHeight; 
    }else{ 
        widthViewport= document.getElementsByTagName('body')[0].clientWidth; 
        heightViewport=document.getElementsByTagName('body')[0].clientHeight; 
    } 
    xScroll=self.pageXOffset || (document.documentElement.scrollLeft+document.body.scrollLeft);
    yScroll=self.pageYOffset || (document.documentElement.scrollTop+document.body.scrollTop);
    widthTotal=Math.max(document.documentElement.scrollWidth,document.body.scrollWidth,widthViewport);
    heightTotal=Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,heightViewport);
    return [widthViewport,heightViewport,xScroll,yScroll,widthTotal,heightTotal];
} 

//''' <summary>
//''' Establece la visibilidad/ tamaño/ posicion de los diferentes divs
//''' </summary>
//''' <remarks>Llamada desde: cumpoferta.asp (onresize y 21 function)  cumpoferta.js (4 function)    js\cargarsubasta.asp (1 function)    js\actualizarsubasta.asp (1 function); Tiempo máximo: 0,2</remarks>
function resize()
{
    // Establece la visisbilidad del botón restaurar según las condicion del estado del proceso
    botonRestaurar()
    // Establece la visisbilidad del botón ganadora según las condiciones visualización y de comienzo de la subasta
    botonGanadora()
	var w
	var h
	var wa
	var wd = getWindowData()
	//h=document.body.offsetHeight - t
	h=wd[1] - t
	if (h<0)
		return
	//w=document.body.offsetWidth 
	w=wd[4]
	wa = w - wto -30 //- 20          // DPD Cambios para evitar Scrol horizontal en caso de que aparezca la barra de scrol vertical.
	tb = tb1
	// cálculo del alto de la barra de botones
	if (document.getElementById("Ganadora")){if (document.getElementById("Ganadora").style.display == "block"){tb = tb + (tb2-tb1)}}
	if (document.getElementById("Restaurar")){if (document.getElementById("Restaurar").style.display == "block"){tb = tb + (tb2-tb1)}}

	if (document.getElementById("divMostrarArbol").style.visibility == "hidden") tb = tb1

	var calcPx

	calcPx = t + tb + m * 2 + hSwitch
	document.getElementById("divArbol").style.top = calcPx + 'px';
	document.getElementById("divArbol").style.left = l + 'px';
	document.getElementById("divArbol").style.width = wto + 'px';
	calcPx = (h - tb - m * 3 - hSwitch)
	if (calcPx > 0) document.getElementById("divArbol").style.height = calcPx + 'px';

	calcPx = t + tb + m
	document.getElementById("divMostrarArbol").style.top = calcPx + 'px';
	document.getElementById("divMostrarArbol").style.left = l + 'px';
    document.getElementById("divMostrarArbol").style.width = wto + 'px';
	if (((h - tb - m * 2) + 4) > 0) {
	    calcPx = (h - tb - m * 2) + 4
	    document.getElementById("divMostrarArbol").style.height = calcPx + 'px';
	    document.getElementById("tablaMostrarArbol").style.height = calcPx + 'px';
	}

	calcPx = t + tb + m
	document.getElementById("divOcultarArbol").style.top = '15px';
	document.getElementById("divOcultarArbol").style.left = l + 'px';
    document.getElementById("divOcultarArbol").style.width = wto + 'px';
    document.getElementById("divOcultarArbol").style.height = hSwitch + 'px';

    document.getElementById("divBotones").style.top ='10px';
	document.getElementById("divBotones").style.left = l + 'px';
	document.getElementById("divBotones").style.height = tb + 'px';
    document.getElementById("divBotones").style.width = wto + 'px';

    
    document.getElementById("divApartado").style.top = t + 'px';
    calcPx = l + wto + m
    document.getElementById("divApartado").style.left = calcPx + 'px';
    document.getElementById("divApartado").style.width = wa + 'px';
    calcPx = h - m + 4
    if (h - m > 0) document.getElementById("divApartado").style.height = calcPx + 'px';
	var vGrp

	var i

	if (document.getElementById("divApartadoS"))
	{
	    document.getElementById("divApartadoS").style.top = ha + 'px';
	    calcPx = wa - 2
	    document.getElementById("divApartadoS").style.width = calcPx + 'px';
	    calcPx = h - m - ha - 2
	    if (h - m - ha - 2 > 0) document.getElementById("divApartadoS").style.height = calcPx + 'px';
		for (i=0;i<proceso.grupos.length  ;i++)
		{
		vGrp = proceso.grupos[i]
		
		if (document.getElementById("divScroll" + vGrp.cod)) {		    
		    calcPx = parseFloat(document.getElementById("divApartadoS").style.height) - hCabecera
			document.getElementById("divScroll" + vGrp.cod).style.height = calcPx + 'px'; 
		    
            calcPx = parseFloat(document.getElementById("divApartadoS").style.width)- vGrp.wApArticulo
		    document.getElementById("divScroll" + vGrp.cod).style.width = calcPx + 'px';

			if(!noScroll) document.getElementById("divScroll" + vGrp.cod).scrollLeft=0
			scrollFijas(vGrp.cod)
			}

		if (document.getElementById("div" + vGrp.cod))
			{
			document.getElementById("div" + vGrp.cod).style.top = document.getElementById("divApartado").style.top
			document.getElementById("div" + vGrp.cod).style.left = document.getElementById("divApartado").style.left
			document.getElementById("div" + vGrp.cod).style.width = document.getElementById("divApartado").style.width
			document.getElementById("div" + vGrp.cod).style.height =document.getElementById("divApartado").style.height
			}			
		}
		 noScroll = false
	}
	if (esSubasta)
	{
		if (document.getElementById("divPuja"))
		{
		document.getElementById("divPuja").style.top = document.getElementById("divApartado").style.top
		document.getElementById("divPuja").style.left = document.getElementById("divApartado").style.left
		document.getElementById("divPuja").style.width = document.getElementById("divApartado").style.width
		document.getElementById("divPuja").style.height =document.getElementById("divApartado").style.height
		}
		if (document.getElementById("divArticuloPujaScroll")) {
		    calcPx = parseFloat(document.getElementById("divApartadoS").style.height) - hCabecera + hCabeceraGrupoDetalle - 30
		    if (calcPx > 0) document.getElementById("divArticuloPujaScroll").style.height = calcPx + 'px';

			document.getElementById("divArticuloPujaScroll").style.width=document.getElementById("divApartadoS").style.width
			if(!noScroll) document.getElementById("divArticuloPujaScroll").scrollLeft=0
			noScroll = false
			scrollFijasPuja()
		}
	}
}

//''' <summary>
//''' Dibuja el Apartado de datos (los divs de la derecha) de la oferta/subasta
//''' </summary>
//''' <param name="titulo">titulo del apartado</param> 
//''' <remarks>Llamada desde: cumpoferta.asp (10 functions)  cumpoferta.js (2 functions); Tiempo máximo: 0,2</remarks>
function dibujarApartado(titulo)
{
	tituloActual = titulo
	document.getElementById("divBloqueo").style.visibility="visible"
	var str
	var i
	for (i=0;i<proceso.grupos.length  ;i++)
	{
		vGrp = proceso.grupos[i]
		if (document.getElementById("div" + vGrp.cod))
		{
			document.getElementById("div" + vGrp.cod).style.visibility="hidden"
			if (vGrp.atribsItems.length>0)
			{
				document.images["imgAtributosD_" +  vGrp.cod ].style.visibility="hidden"
				document.images["imgAtributosI_" +   vGrp.cod ].style.visibility="hidden"
			}
			if (esSubasta)
			{    
				if (document.images["imgSubastaD_" +  vGrp.cod ]) document.images["imgSubastaD_" +  vGrp.cod ].style.visibility="hidden"
				if (document.images["imgSubastaI_" +  vGrp.cod ]) document.images["imgSubastaI_" +  vGrp.cod ].style.visibility="hidden"
			}
			if (pedirAlterPrecios)
			{
				document.images["imgMasPreciosD_" +  vGrp.cod ].style.visibility="hidden"
				document.images["imgMasPreciosI_" +  vGrp.cod ].style.visibility="hidden"
			}
			document.images["imgOfertaD_" +  vGrp.cod ].style.visibility="hidden"
			document.images["imgOfertaI_" +  vGrp.cod].style.visibility="hidden"
			document.images["imgDatosItemI_" +  vGrp.cod ].style.visibility="hidden"
			document.images["imgDatosItemD_" +  vGrp.cod ].style.visibility="hidden"
		}			
	}

	if (esSubasta)
	{
		if (document.getElementById("divPuja"))
		{
			document.getElementById("divPuja").style.visibility="hidden"
			for (i=0;i<proceso.grupos.length  ;i++)
			{
				document.images["imgGrupoA" + proceso.grupos[i].cod].style.visibility="hidden" 
				document.images["imgGrupoB" + proceso.grupos[i].cod].style.visibility="hidden" 
			}
		}
	}
	str  = "<div name=divCabApartado id=divCabApartado>"
	str += generaCabecera(titulo)
	str += "</div>"
	str += "<div name=divApartadoS id=divApartadoS style='position:absolute;left:0px;top:90px;overflow:scroll;'></div>"
	document.getElementById("divApartado").innerHTML=str
	document.getElementById("divApartado").style.visibility="visible"
}


function generaCabecera(titulo)
{
var str
str ="<table width=100%>"
if (proceso.estado>1)
	{
	str += "	<tr height=24px>"
    
	switch (proceso.estado)
		{
		case 1:
            sClase = "OFENUEVA"
			break;
		case 2:
			sClase = "OFESINENVIAR"
			break;
		case 3:
		case 5:
			sClase = "OFEENVIADA"
			break;
		case 4:
			sClase = "OFESINGUARDAR"
			break
		default:
			sClase = ""
			break;
		}    
    resize()

	str += "		<td class=" + sClase +">"
	sEstado = estadoOferta(proceso.estado)
    str += sEstado + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + (linkRecalculo==0?num2str(proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt):"-") + "&nbsp;" + proceso.mon.cod + "&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(null)' onclick='verAyuda(" + proceso.estado + ")'><IMG border=0 SRC=../images/masinform.gif  WIDTH=11 HEIGHT=14 name=imgMasInform id=imgMasInform></a>"
    
    if(linkRecalculo==1)
		str+="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(null)' onclick=\"linkRecalculoOferta()\"><img src=\"images/RecOferTot1-25x25.png\" border=0 width=19 height=19/ >" +  den[128] + "</a>" //Recalcular Totales de Oferta
	str += "		</td>"
	str += "	</tr>"
	}
else
	{
	str += "	<tr height=24px>"
	str += "		<td >"
	str += "&nbsp;"
	str += "		</td>"
	str += "	</tr>"
	}	
str += "	<tr height=24px>"
str += "		<td class=cabApartado>"

str += "                        <div id='denproceso' style='overflow:hidden;height=14 pt' title='" + titulo.replace('<nobr>','').replace('</nobr>','') + "'>"

str += titulo

str += "                        </div>"


str += "		</td>"
str += "	</tr>"
str += "</table>"
return str
}


/*
''' <summary>
''' Se ejecuta cuando el usuario pulsa en el link de recálculo de la oferta
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: generaCabecera; Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
*/
function linkRecalculoOferta()
{
/*var nombreGuardado = ''
var ambitoGuardado = ''
var grupoGuardado = ''*/
var vCosteDescRec
var vGrupo
//var numGruposTratados=0
var gruposRecalc = new Array()
    var indiceGr
var indiceGrRec=0
var ordenDesde=9999
var ordenAux
var recalculoCorrecto = true
var bHayPrecios = false

    //Comprobar que hay precios en alguno de los Ã­tems de la oferta

    for(indiceGr=0;indiceGr<proceso.grupos.length;indiceGr++)
    {
        if (proceso.grupos[indiceGr].items.length)
        {
            for (i=0;i<=proceso.grupos[indiceGr].items.length-1;i++)
            {
                if(proceso.grupos[indiceGr].items[i].precio !=null || proceso.grupos[indiceGr].items[i].precio2 !=null || proceso.grupos[indiceGr].items[i].precio3 !=null)
                {
                    bHayPrecios = true
                    break
                }
            }
        }
        else
        {
            if(HayPrecio(proceso.grupos[indiceGr].cod, proceso.idPortal))
             {
                bHayPrecios = true
                break
             }
        }
    }

    if (!bHayPrecios)
    {
        //alert("No existen datos para realizar el recálculo de la oferta. Introduzca los precios de los ítems.")
        alert(den[143])
        return
    }

    //recálculo de los costes / desc de los grupos
    // obtenemos el total de grupos que han sido modificados y que hay que recalcular, y lo guardamos en un array de grupos
    //recorremos los grupos del proceso
    for(indiceGr=0;indiceGr<proceso.grupos.length;indiceGr++)

            //si el grupo está pendiente de recálculo
            if (proceso.grupos[indiceGr].costeDescRecalculo)
            {
                //si el proceso está pendiente de recálculo
                if (proceso.costeDescRecalculo)
                {
                    //obtenemos el orden del coste/descuento para obtener el de mayor rango de entre los recalculados
                    ordenAux=obtenerOrdenCosteDesc(proceso.grupos[indiceGr].costeDescRecalculo.idCosteDescuento,proceso.grupos[indiceGr].costeDescRecalculo.aplica)
                    if (ordenAux<ordenDesde) 
                        {
                        ordenDesde=ordenAux
                        vCosteDescRec=proceso.grupos[indiceGr].costeDescRecalculo
                        }
                }
                else
                {
                //si el proceso no está pendiente de recálculo no es necesario que obtengamos el coste/descuento 
                //de mayor rango de entre los recalculados, ya que vamos a gestionar todos los grupos en la función de recálculo
                gruposRecalc[indiceGrRec] = proceso.grupos[indiceGr]
                indiceGrRec++
                }
            }

    //si el proceso está pendiente de recálculo obtenemos el orden del coste/descuento para obtener el 
    //de mayor rango de entre los recalculados
    if (proceso.costeDescRecalculo)
        {
            if (!vCosteDescRec)
                 vCosteDescRec = proceso.costeDescRecalculo
            else
                {
                ordenAux=obtenerOrdenCosteDesc(proceso.costeDescRecalculo.idCosteDescuento,proceso.costeDescRecalculo.aplica)
                if (ordenAux<ordenDesde) 
                    {
                    ordenDesde=ordenAux
                    vCosteDescRec=proceso.costeDescRecalculo
                    }
                }
            //recalculamos a partir del coste/descuento de mayor rango
            recalculoCorrecto = recalcularCostesDescuentos(false,null,vCosteDescRec.nombreCosteDesc, vCosteDescRec.valor, vCosteDescRec.idCosteDescuento, vCosteDescRec.importeParcial, vCosteDescRec.aplica, vCosteDescRec.ambito)
            if (recalculoCorrecto)
                proceso.costeDescRecalculo = null
        }
    else
        //si el proceso no está pendiente de recálculo, recalculamos con los datos de los grupos que hay que recalcular
       if(gruposRecalc[0]!=null)
            recalculoCorrecto = recalcularCostesDescuentos(false,gruposRecalc)
    if (recalculoCorrecto) {
        for (indiceGr = 0; indiceGr < proceso.grupos.length; indiceGr++)
            if (proceso.grupos[indiceGr].costeDescRecalculo)
                proceso.grupos[indiceGr].costeDescRecalculo = null
    }

    if (recalculoCorrecto) 
    {
        linkRecalculo = 0
        linkRecalculoPendiente = 0
    }
    if (document.getElementById("divApartado").aptdoActual)
       eval(document.getElementById("divApartado").aptdoActual)
    document.getElementById("divCabApartado").innerHTML = generaCabecera(tituloActual)

    for (i=0;i<proceso.grupos.length;i++)
	{
	    sEstado = estadoOferta(proceso.estado)
	    strT = sEstado + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  (linkRecalculo==0?num2str(proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt):"-") + "&nbsp;" + proceso.mon.cod + "&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(null)' onclick='verAyuda(" + proceso.estado + ")'><IMG border=0 SRC=../images/masinform.gif  WIDTH=11 HEIGHT=14 name=imgMasInform id=imgMasInform></a>"
        
        if(linkRecalculo==1)
            strT+="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(null)' onclick=\"linkRecalculoOferta()\"><img src=\"images/RecOferTot1-25x25.png\" border=0 width=19 height=19/ >" + den[128] + "</a>"
	    sClase = ""
        
	    switch (proceso.estado)
		    {
		    case 1:
			    sClase = "OFENUEVA"
			    break;
		    case 2:
			    sClase = "OFESINENVIAR"
                tbvar= tb2
			    break;
		    case 3:
		    case 5:
			    sClase = "OFEENVIADA"
			    break;
		    case 4:
			    sClase = "OFESINGUARDAR"
			    break
		    default:
			    sClase = ""
			    break;
		    }
        tb= tbvar //DPD
        resize()
	    if (document.getElementById("divTotalOferta_" + proceso.grupos[i].cod) && sClase!="")
		    {				
		    document.getElementById("divTotalOferta_" + proceso.grupos[i].cod).innerHTML  = strT
		    document.getElementById("divTotalOferta_" + proceso.grupos[i].cod).parentNode.className=sClase
		    }
    }

    if (esSubasta)
	{		
		cadena = strT
		//Se pasa con timeOut ya que sino al cambiar la cantidad y pulsar directamente en el boton de pujar
		//no coge el evento onclick!!!!!!!        
		// DPD setTimeout ("escribirCabecera(cadena)",700);
        setTimeout ("MostrarCabeceraOferta()",700);
        generaCabecerasGruposSubasta()
    }
    OcultarDivCostesDescItem()
}



//Función que comprueba que haya precios para alguno de los Ítems de un grupo y devuelve cierto en caso de que los haya
function HayPrecio(codGrupo, idPortal) {
    crearObjetoXmlHttpRequest()
    AjaxSync("js/xmlpreciosgrupo.asp?anyo=" + proceso.anyo + "&gmn1=" + proceso.gmn1 + "&proce=" + proceso.cod + "&grupo=" + codGrupo + "&idPortal=" + idPortal)
    var XML = xmlobj.responseXML.getElementsByTagName("fullstep");
    var hayPrec = XML[0].getElementsByTagName("hayPrecio")[0].firstChild.nodeValue;
    return (hayPrec != 0)
}

function obtenerOrdenCosteDesc(idCosteDescuento,aplica)
{
var indiceCostesDesc
var ordenParcial=1
var bContinuar=1

if(aplica==3)
{
    for (indiceCostesDesc=0;indiceCostesDesc<proceso.CostesDescPrecItem.length;indiceCostesDesc++)
    {
        if (proceso.CostesDescPrecItem[indiceCostesDesc].paid==idCosteDescuento)
            break
        ordenParcial++
     }
     bContinuar = 0
}

if(bContinuar)
{
    ordenParcial += proceso.CostesDescPrecItem.length
}

if(aplica==2)
{
    for (indiceCostesDesc=0;indiceCostesDesc<proceso.CostesDescTotalItem.length;indiceCostesDesc++)
    {
        if (proceso.CostesDescTotalItem[indiceCostesDesc].paid==idCosteDescuento)
            break
        ordenParcial++
     }
     bContinuar = 0
}

if(bContinuar)
{
    ordenParcial += proceso.CostesDescTotalItem.length
}

if(aplica==1)
{
    for (indiceCostesDesc=0;indiceCostesDesc<proceso.CostesDescTotalGrupo.length;indiceCostesDesc++)
    {
        if (proceso.CostesDescTotalGrupo[indiceCostesDesc].paid==idCosteDescuento)
            break
        ordenParcial++
     }
     bContinuar = 0
}

if(bContinuar)
{
    ordenParcial += proceso.CostesDescTotalGrupo.length
}


if(aplica==0)
{
    for (indiceCostesDesc=0;indiceCostesDesc<proceso.CostesDescTotalOferta.length;indiceCostesDesc++)
    {
        if (proceso.CostesDescTotalOferta[indiceCostesDesc].paid==idCosteDescuento)
            break
        ordenParcial++
     }
     bContinuar = 0
}

return ordenParcial
}



function generaCabeceraCostesDescItem()
{
	var str
	str=""
	if(linkRecalculoItem==1)
		str+="<a href='javascript:void(null)' onclick='linkRecalcularItem(\"txtPU_\")'>" + den[98] + "</a>" //JVS Idioma Recalcular totales de ítem
	return str
}

function generaCabeceraCostesDescItemSubastas()
{
	var str
	str=""
	if(linkRecalculoItem==1)
		str+="<a href='javascript:void(null)' onclick='linkRecalcularItem(\"txtPujaPU_\")'>" + den[98] + "</a>" //JVS Idioma Recalcular totales de ítem
	return str
}


/*
''' <summary>
''' Recalcula el item tras un cambio en los costes/descuentos. Y recalcula totales grupo y oferta.
''' </summary>
''' <param name="nombre">nombre del control ejemplo: txtPujaPU_ / txtPU_ / ... </param>
''' <param name="precioUnitarioInicialAnterior">precio actual</param>
''' <param name="paidCD">Id del coste/descuento</param>
''' <param name="valorAnterior">valor Anterior del coste/descuento</param>
''' <param name="primeravez">si es la primera vez q entra en el precio</param>
''' <param name="divModificable">si puede o no editar los costes/descuentos</param>
''' <remarks>Llamada desde:cumpoferta.asp     cumpoferta.js         js\cargarGanadora.asp; Tiempo máximo:0,1</remarks>
*/
function linkRecalcularItem(nombre, precioUnitarioInicialAnterior, paidCD, valorAnterior, primeravez, divModificable)
{
var indiceGr
var indiceIt
var oItemSecundario = null

var ind
var codGrupo = document.getElementById("frmCostesDescuentosItem").grupo.value
var idItem = document.getElementById("frmCostesDescuentosItem").item.value
var iesc = document.getElementById("frmCostesDescuentosItem").esc.value
if (divModificable==null)
    divModificable=true
  
for(indiceGr=0;indiceGr<proceso.grupos.length;indiceGr++)
    if(proceso.grupos[indiceGr].cod == codGrupo)
    {
        vGr = proceso.grupos[indiceGr]
        break
    }

if(nombre=="txtPujaPU_")
{
    for(indiceIt=0;indiceIt<itemsPuja.length;indiceIt++)
        if(itemsPuja[indiceIt].id == idItem)
        {
            oItem = itemsPuja[indiceIt]
            break
        }
    if(vGr.items.length)
    {
        for(indiceIt=0;indiceIt<vGr.items.length;indiceIt++)
            if(vGr.items[indiceIt].id == oItem.id)
            {
                oItemSecundario = vGr.items[indiceIt]
                break
            }
    }
}
else
{
    for(indiceIt=0;indiceIt<vGr.items.length;indiceIt++)
        if(vGr.items[indiceIt].id == idItem)
        {
            oItem = vGr.items[indiceIt]
            break
        }
    if (esSubasta) 
    {
		for(indiceIt=0;indiceIt<itemsPuja.length;indiceIt++)
            if(itemsPuja[indiceIt].id == oItem.id)
            {
                oItemSecundario = itemsPuja[indiceIt]
                break
			}
    }
}
if(nombre=="txtPujaPU_")
    var sNamePU = "txtPujaPUI_" + idItem
else
    var sNamePU = "txtPUI_" + codGrupo + "_" + idItem    

if (iesc>=0) 
	idEsc = "_" + oItem.escalados[iesc].id
else
	idEsc = ""

	
sNamePU += idEsc	
//var precioUnitarioInicial = eval("document.getElementById(\"frmCostesDescuentosItem\")." + sNamePU + ".value")

var precioUnitarioInicial = eval("oItem.precio" + precioNum)

if(iesc>=0) precioUnitarioInicial = eval("oItem.escalados[iesc].precio")



//precioUnitarioInicialAnterior => si se ha modificado el precio
//paidCD,valorAnterior => si se ha modificado un coste/descuento de ámbito ítem

recalcularCostesDescuentosItem(vGr,oItem,precioUnitarioInicial,(nombre=="txtPujaPU_"?1:0), oItemSecundario,precioUnitarioInicialAnterior,paidCD,valorAnterior,divModificable,iesc,0)

//el precio neto nuevo se usa para actualizar el campo de precio unitario
if(nombre=="txtPujaPU_")
    var sNamePU = nombre + idItem
else
    var sNamePU = nombre + codGrupo + "_" + idItem

sNamePU += idEsc	
	
//eval("document.getElementById(\"frmOfertaAEnviar\")." + sNamePU + ".value=((oItem.precioNeto!=null)?num2str(oItem.precioNeto,vdecimalfmt,vthousanfmt,1000):'')")
eval("document.getElementById(\"frmOfertaAEnviar\")." + sNamePU + ".value=((precioNetoCalculado!=null)?num2str(precioNetoCalculado,vdecimalfmt,vthousanfmt,1000):'')")


/*linkRecalculo = 0
generaCabeceraItems()
*/
        if(linkRecalculo == 0)
            generaCabeceraItems()
/*
  if(linkRecalculoPrevio == 0)    
 {
        linkRecalculo = 0
            generaCabeceraItems()
 }
 linkRecalculoPrevio = 2
 */

linkRecalculoItem = 0
if (primeravez=="new")mostrarFormCostesDescuentosItem(vGr.cod, oItem.id,nombre,iesc,idEsc)
else redibujarFormCostesDescuentosItem(vGr.cod, oItem.id,nombre,divModificable,iesc)


}



/*
''' <summary>
''' Muestra la ayuda
''' </summary>
''' <param name="est">estado del proceso</param>
''' <remarks>Llamada desde: cumpoferta.js; Tiempo mÃ¡ximo:0,1</remarks>
*/
function verAyuda(est)
{
if (est==5)
	h = 380
else
	h = 360
	if (esSubasta)
	{
		h = 150
		window.open(RutaSegura + "script/solicitudesoferta/ayudaestadopuja.asp?estado=" + est, "_blank", "top=100,left=150,width=650,height=" + h + ",location=no,menubar=no,resizable=yes,scrollbars=yes,toolbar=no")
	}
	else
	{
	    window.open(RutaSegura + "script/solicitudesoferta/ayudaestado.asp?estado=" + est, "_blank", "top=100,left=150,width=650,height=" + h + ",location=no,menubar=no,resizable=yes,scrollbars=yes,toolbar=no")
	}
}
function cajetin(titulo,valor,estilo,func,param,horiz,estiloValor)
{
var str

str  = "<table class='cajetin' style='" + estilo + "'><tr><td class=sombreado>"
str += "<nobr>" + titulo + "</nobr>"
if (horiz)

str += "</td><td" + (func ? " class='muesca'":"")  + (estiloValor?" class='" + estiloValor + "'":"") + ">"	
else
str += "</td></tr><tr><td" + (estiloValor?" class='" + estiloValor + "'":"") + ">"	

if (valor== null || valor=="")
	str += "&nbsp"
else
	{
	if (func)
        str += "<table width=100%><tr><td width=100%><a class=popUp href='javascript:void(null)'  onclick='" + func + "(\"" + param + "\")'>"
	str += JS2HTML(valor)
	if (func)
		str += "</a></td><td><a class=popUp href='javascript:void(null)'  onclick='" + func + "(\"" + param + "\")'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table>"
        
	}
str += "</td></tr></table>"

return (str)
}

// <summary>
// acceso a la pantalla de datos de proceso desde el menú lateral
// </summary>
// <param name="e">identificador de la pantalla</param>
// <returns></returns>
// <remarks>Llamada desde: config.asp/mostrarConfigProceso; Tiempo máximo:0,1</remarks>
// <revision>JVS 16/11/2011</revision>
function  datosProceso(e)   
{
if (document.getElementById("divApartado").aptdoAnterior) 
	eval (document.getElementById("divApartado").aptdoAnterior)

dibujarApartado(proceso.den)
resize()

document.getElementById("divApartado").aptdoAnterior="cambiarFlag()"
document.getElementById("divApartado").aptdoActual="mostrarProceso(proceso)"
mostrarProceso(proceso)

// Pone a false los campos booleanos de control de la pantalla de retorno tras recálculo
cambiarFlag()
// Pone a true el campo booleano de datosProceso de control de la pantalla de retorno tras recálculo
bEstamosEnProceso = true
bEstamos="1"
desbloquear()
}

function desbloquear()
{
document.getElementById("divBloqueo").style.visibility="hidden"
}

/*
''' <summary>
''' Pone a false los campos booleanos de control de la pantalla de retorno tras recálculo
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cumpoferta.asp; Tiempo máximo:0,1</remarks>
''' <revision>JVS 16/11/2011</revision>
*/

function cambiarFlag()
{
    bEstamosEnProceso = false //1
    bEstamosEnOferta = false  //2
    bEstamosEnGrupo = false   //3
    bEstamosEnSobre = false   //4
    bEstamosEnAtributos = false  //5
    bEstamosEnCostesDesc = false //6
    bEstamosEnCostesDescGrupo = false  //7
    bEstamosEnPrecios = false  //8
    bEstamosEnAdjuntos = false  //9
    bEstamosEnAdjuntosOferta = false //10
    bEstamos="0"
}


// <summary>
// acceso a la pantalla de datos de oferta desde el menú lateral
// </summary>
// <param name="e">identificador de la pantalla</param>
// <returns></returns>
// <remarks>Llamada desde: config.asp/mostrarConfigProceso; Tiempo máximo:0,1</remarks>
// <revision>JVS 16/11/2011</revision>
function  datosOferta(e)
{
if (document.getElementById("divApartado").aptdoAnterior)
	eval (document.getElementById("divApartado").aptdoAnterior)

dibujarApartado(den[17])
resize()

// Pone a false los campos booleanos de control de la pantalla de retorno tras recálculo
cambiarFlag()
// Pone a true el campo booleano de datosOferta de control de la pantalla de retorno tras recálculo
bEstamosEnOferta = true
bEstamos = "2"
document.getElementById("divApartado").aptdoAnterior = "guardarOferta()"
document.getElementById("divApartado").aptdoActual = "mostrarOferta(proceso)"

proceso.cargarAtribs("fraOfertaServer")

}



function restaFecha(f1, f2)
{

lDif = f2 - f1


lDias = parseInt(lDif / 86400000)

lResto = lDif % 86400000

lHoras = parseInt(lResto / 3600000)

lResto = lDif % 3600000

lMinutos = parseInt(lResto / 60000)

lResto = lResto % 60000

lSegundos = parseInt(lResto / 1000)

if(lSegundos<0 || lMinutos<0 || lHoras <0 )
	return false

sRet = lDias + " " + den[32] + "  " + "00".substr(0,2-lHoras.toString().length) + lHoras.toString() + ":" + "00".substr(0,2-lMinutos.toString().length) + lMinutos.toString() + ":" + "00".substr(0,2-lSegundos.toString().length) + lSegundos.toString()


return (sRet)

}


function cargarOfertasMinimas()
{

//if (!bGuardando && bPujando && !bSinSync)
// A pesar de no estar en el modo de subasta, se llama al proceso que abre cargarOfeMin.asp con el bPujando como parámetro adicional
// esta página Modifica el estado de los eventos y tal y como hasta ahora, sólo si bPujando es cierto cargará las ofertas mínimas.


if (!bGuardando && !bSinSync)
	proceso.cargarOfertasMinimas("fraOfertaPujas",bPujando)
}





/*
''' <summary>
''' Cada vez q se reanuda la subasta se avisa a los distintos proveedores
''' </summary>
''' <param name="newFec"></param>  
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cargarofemin.asp (en el fondo el timer de 10sg) ; Tiempo máximo:0</remarks>
*/
function cambiarFechaReinicio(newFec)
{
    vFechaReinicio = newFec 
}




/*
''' <summary>
''' Cada vez q se cambia el plazo se avisa a los diferentes proveeedores
''' </summary>
''' <param name="newFec">Nueva fecha hora de plazo</param>  
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cargarofemin.asp (en el fondo el timer de 10sg) ; Tiempo máximo:0</remarks>
*/
function cambiarFechaApertura(newFec)
{

if (newFec-vFechaApertura != 0) // Cambia la fecha de apertura
	{
    // aquí puede haber dos opciones, que se mantenga cerrada o que el cambio implique la apertura de la subasta.
    var dAhoraReal = new Date(dFechaInicialServer.getFullYear(),dFechaInicialServer.getMonth(),dFechaInicialServer.getDate(),dFechaInicialServer.getHours(), dFechaInicialServer.getMinutes(), dFechaInicialServer.getSeconds(),dFechaInicialServer.getMilliseconds() + (dAhora - dFechaInicialClient))

    if (newFec < dAhoraReal)
    {   // La subasta se abre
        alert(den[126])
    }
    else
    {
        alert(den[127] + date2str(newFec,vdatefmt,true))               
        bAbierta = false        
    }
    
    vFechaApertura = newFec
    mostrarCronometros()
    actualizarSubasta()
    
	}
}



/*
''' <summary>
''' Cada vez q se cambia la fecha de apertura, hay que comprobar que la visualización de las columnas es la correcta
''' </summary>
''' <param name="newFec">Nueva fecha de apertura de sobre</param>  
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cargarofemin.asp (en el fondo el timer de 10sg) ; Tiempo máximo:0</remarks>
*/
function cambiarFechaApeSobre(newFec)
{
if (newFec-vFechaAperturaSobre != 0) // Cambia la fecha de apertura
	{
    // aquí puede haber dos opciones, que haya cambio de estado o no
    var dAhoraReal = new Date(dFechaInicialServer.getFullYear(),dFechaInicialServer.getMonth(),dFechaInicialServer.getDate(),dFechaInicialServer.getHours(), dFechaInicialServer.getMinutes(), dFechaInicialServer.getSeconds(),dFechaInicialServer.getMilliseconds() + (dAhora - dFechaInicialClient))
    

    if (((vFechaAperturaSobre > dAhoraReal) && (newFec <= dAhoraReal)) || ((vFechaAperturaSobre <= dAhoraReal) && (newFec > dAhoraReal)))
    //Si el cambio de la apertura de sobre afecta a la subasta, es decirq ue pasa de sobre cerrado a abierto o viceversa
    {        
        mostrarCronometros()
        actualizarSubasta()
    }
    vFechaAperturaSobre = newFec
	}
}



/*
''' <summary>
''' Cada vez q se cambia el estado de la subasta actualiza la cabecera
''' </summary>
''' <param name="newEstado">Nuevo Estado de la subastas</param>  
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cargarofemin.asp (en el fondo el timer de 10sg) ; Tiempo máximo:0</remarks>
*/
function cambiarEstadoSubasta(newEstado)
{
    //el estado de la subasta ha cambiado
    if (newEstado != iEstadoSubasta)
    {

	switch (newEstado)
		{
		case 1:
            
			break
		case 2:
	        switch (iEstadoSubasta)
		        {
		        case 1:
                    // la subasa comienza de forma normal
			        break
		        case 3:
                    // la subasta se reanuda tras estar pausada                    
                    alert (den[129] + "\n" + den[133] + date2str(vFechaLimite,vdatefmt,true))
			        break
                }
            break
		case 3:
            var mensaje
            if (vFechaReinicio != '')
            {
                mensaje = den[130] + "\n" + den[134] + date2str(vFechaReinicio,vdatefmt,true)
            }
            else
            {
                mensaje = den[130] + "\n" + den[131]
            }
                alert(mensaje)
			break
		case 4:
            alert(den[132])			  
            break
		case 5:
            // La subasta ha finalizado 
            break
        }


        iEstadoSubasta = newEstado
        mostrarCronometros()
        actualizarSubasta()               
    }
}



/*
''' <summary>
''' Cada segundo se actualiza el cronometro de la subasta, esta función "pinta" el div cronometro
''' </summary>    
''' <remarks>Llamada desde: cumpoferta.asp  cumpoferta.js   guardaroferta.asp    js\actualizarsubasta.asp   js\cargarsubasta.asp; Tiempo máximo: 0</remarks>*/
function mostrarCronometros()
{


var str

str = ""
dAhora = new Date()

var dAhoraReal = new Date(dFechaInicialServer.getFullYear(),dFechaInicialServer.getMonth(),dFechaInicialServer.getDate(),dFechaInicialServer.getHours(), dFechaInicialServer.getMinutes(), dFechaInicialServer.getSeconds(),dFechaInicialServer.getMilliseconds() + (dAhora - dFechaInicialClient))

 
if (vFechaApertura > dAhoraReal )
	bAbierta = false
else
	bAbierta = true


var vFecha 

if (bAbierta )
	{
	ret = restaFecha(dAhora, vFechaLimite)
	vFecha = vFechaLimite
	}
else
	{
	ret = restaFecha(dAhora, vFechaApertura)
	vFecha = vFechaApertura
	}



if (document.getElementById("divCronometro") && vFecha)
	{
	str = ""
	str+= "				<table>"
	str+= "					<tr>"	
	if (bAbierta)
		str+= "						<td >" + cajetin(den[31],"<div id=divTiempoRestante name=divTiempoRestante></div>","width:300px;",null,null,true) + "</td>"
	else
		str+= "						<td >" + cajetin(den[49],"<div id=divTiempoRestante name=divTiempoRestante></div>","width:300px;",null,null,true) + "</td>"
	str+= "					</tr>"
	str+= "				</table>"

	document.getElementById("divCronometro").innerHTML = str
	}


if (document.getElementById("divCronometroPuja"))
	{
	str = ""
	str+= "				<table>"
	str+= "					<tr>"	
	if (vFecha)
		if (bAbierta)
            switch (iEstadoSubasta)
            {
            case 2: //en proceso
    			    str+= "						<td>" + cajetin(den[31],"<div id=divTiempoRestantePuja name=divTiempoRestantePuja></div>","width:300px;",null,null,true) + "</td>"
                    break;
            case 3: // pausada
    			    str+= "						<td>" + cajetin(den[135],"<div id=divTiempoRestantePuja name=divTiempoRestantePuja></div>","width:300px;",null,null,true) + "</td>"
                    break;
            case 4: // detenida
    			    str+= "						<td>" + cajetin(den[31],"<div id=divTiempoRestantePuja name=divTiempoRestantePuja></div>","width:300px;",null,null,true) + "</td>"
                    break;
            }
		else
			str+= "						<td>" + cajetin(den[49],"<div id=divTiempoRestantePuja name=divTiempoRestantePuja></div>","width:300px;",null,null,true) + "</td>"

	if (bAbierta)
	{

        if (cambiaDeEstado == false) 
        {
            actualizarSubasta()
        }

        switch (iEstadoSubasta)
        {
        case 1: //No Iniciada

            // puede darse el caso de que esté en estado 1 porque aún no se ha actualizado en la BD, pero si llega a este punto es que cumple
            // la restricción horaria y se pondrá a estado 2 en el siguiente acceso a BD
            // por lo tanto se trata el estado 1 como si fuera el 2

		        str+= "						<td>" 
		        str+= ""							 //SUBASTA NO INICIADA 
		        str+= "							&nbsp;&nbsp;<input type=button class=button name=btnPujar id=btnPujar value='" + den[59] + "' onclick = 'prepararOfertaAEnviar(3,(linkRecalculo==1?1:0))' style='width:80px;'>"
		        str+= "						</td>"
                break;
        case 2: // En Proceso
		        str+= "						<td>"
		        str+= "							&nbsp;&nbsp;<input type=button class=button name=btnPujar id=btnPujar value='" + den[51] + "' onclick = 'prepararOfertaAEnviar(3,(linkRecalculo==1?1:0))' style='width:80px;'>"
		        str+= "						</td>"
                break;
        case 3: // Pausada
		        str+= "						<td>" 
		        str+= "							" + den[118] //SUBASTA PAUSADA
		        str+= "						</td>"
                break;
        case 4: // Detenida
		        str+= "						<td>" 
		        str+= "							" + den[119] //SUBASTA DETENIDA
		        str+= "						</td>"
                break;
        case 5: // Finalizada
		        str+= "						<td>" 
		        str+= "							" + den[120]  //SUBASTA FINALIZADA
		        str+= "						</td>"
                break;
        }

	}
	str+= "					</tr>"
	str+= "				</table>"
	document.getElementById("divCronometroPuja").innerHTML = str
	}
    cambiaDeEstado = bAbierta 



}


/*
''' <summary>
''' Nos lleva al apartado que corresponde despues de un guardado
''' </summary>
''' <param name="IrA">string que nos dice a que apartado hay que ir y de quÃ© grupo</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cumpoferta.asp; Tiempo mÃ¡ximo:0,1</remarks>
''' <revision>EPB 06/02/2013</revision>
*/
function actualizarApartado(IrA)

{
 var str;
 var apartado;
 var grupo;
 
     if (IrA.indexOf("_")!=-1)
     {   str=IrA.split("_")
         apartado= str[0]
         grupo=str[1]

         for (i = 0; i < proceso.grupos.length && proceso.grupos[i].cod != grupo; i++)
         { }
         vGrp = proceso.grupos[i]
     }
     else
         apartado = IrA
         
    cambiarFlag()

	switch (apartado) {
	case "1":
	    bEstamosEnProceso=true;
	    bEstamos="1";
	    break;
	case "2":
	    datosOferta('1');
	    bEstamosEnOferta=true;
	    bEstamos="2";
	    break;
    case "3":
        datosGrupo("GR_" + grupo);
        bEstamosEnGrupo=true;
        bEstamos="3_" + grupo;
        break;
    case "4":
        datosSobre("SOBRE_" + grupo);
        bEstamosEnSobre=true;
        bEstamos="4_" + grupo;
        break;
    case "5":
        introducirAtributos("GR_" + grupo + "_2");
        bEstamosEnAtributos=true;
        bEstamos="5_" + grupo;
        break;
    case "6":
        introducirCostesDesc('4');
        bEstamosEnCostesDesc=true;
        bEstamos="6";
        break;
    case "7":
        introducirCostesDescGrupo("GR_" + grupo + "_4");
        bEstamosEnCostesDescGrupo=true;
        bEstamos="7_" + grupo;
        break;
    case "8":
        introducirPrecios("GR_" + grupo + "_1");
        bEstamosEnPrecios=true;
        bEstamos="8_" + grupo;
        break;
    case "9":
        introducirAdjuntos("GR_" + grupo + "_3");
        bEstamosEnAdjuntos=true;
        bEstamos="9_" + grupo;
        break;
    case "10":
        adjuntosOferta('2');   
        bEstamosEnAdjuntosOferta =true;
        bEstamos="10";

        break;    
    }
}


/*
''' <summary>
''' Determina si hay Costes / Descuentos con valores para alguno de los grupos de la oferta
''' </summary>
''' <param name="vObj">Objeto proceso q contiene toda la oferta</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: mostrarProceso; Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
*/
function bHayCostesDescuentosGrupo(vObj)
{

var costeDescValor
var bEncontradoGrupo
var bEncontradoValor = false
var vGrupo
var i, j, k

for (i=0;i<vObj.grupos.length;i++)
{
    bEncontradoGrupo = false
    vGrupo = vObj.grupos[i]
    importeNetoGrupo = vGrupo.importe
    if(importeNetoGrupo)
    {
        for (j=0;j<vObj.CostesDescTotalGrupo.length;j++)
	    {
            for (k=0;k<vObj.CostesDescTotalGrupo[j].grupos.length;k++)
            {
                if (vObj.CostesDescTotalGrupo[j].grupos[k] == vGrupo.id)
                {
                    bEncontradoGrupo = true                	
                    break
                }
            }
            if(bEncontradoGrupo)
            {

                if(vObj.CostesDescTotalGrupo[j].ambito==1)
                    costeDescValor = obtenerCosteDescValor(vObj.costesDescuentos,vObj.CostesDescTotalGrupo[j])
                else
                    costeDescValor = obtenerCosteDescValor(vGrupo.costesDescuentos,vObj.CostesDescTotalGrupo[j])
                if (costeDescValor.valor)
                {
                    bEncontradoValor = true
                    break
                }
            }
        }
    }
}

return (bEncontradoValor)
}


/*
''' <summary>
''' Determina si hay Costes / Descuentos con valores para la oferta
''' </summary>
''' <param name="vObj">Objeto proceso q contiene toda la oferta</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: mostrarProceso; Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <autor>JVS</autor>
*/
function bHayCostesDescuentosOferta(vObj)
{
var i, j

var costeDescValor
var bEncontradoValor = false

for (i=0;i<vObj.CostesDescTotalOferta.length;i++)
	{
    costeDescValor = obtenerCosteDescValor(vObj.costesDescuentos,vObj.CostesDescTotalOferta[i])
    if (costeDescValor.valor && costeDescValor.importeParcial)
    {
        bEncontradoValor = true
        break            
	}
}    


return (bEncontradoValor)
}



/*
''' <summary>
''' Mostrar por pantalla el resumen de los Costes / Descuentos de la oferta
''' </summary>
''' <param name="vObj">Objeto proceso q contiene toda la oferta</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: mostrarProceso; Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <autor>JVS</autor>
*/
function mostrarCostesDescuentos(vObj)
{
var strAtrib
var i
var j
strAtrib = ""
strAtrib+="	<table style='width:100%;'>"
var vClase

vClase = "filaImpar"
var iAmbito
var costeDescValor
var importeAnterior
iAmbito = vObj.proceso?2:1

importeAnterior = vObj.importeBruto
    
for (i=0;i<vObj.CostesDescTotalOferta.length;i++)
	{
    costeDescValor = obtenerCosteDescValor(vObj.costesDescuentos,vObj.CostesDescTotalOferta[i])
	// if (costeDescValor.valor && proceso.importe)
	// DPD quito la condicíon de proceso.importe
    if (costeDescValor.valor)
    {
	strAtrib+="		<tr>"
	strAtrib+="			<td width=70% class='" + vClase + "'>"
	strAtrib+="				<input type=hidden id=pAtribCod name=pAtribCod value=" + vObj.CostesDescTotalOferta[i].cod + ">"
	strAtrib+="					<table><tr><td><nobr><a class='popUp" + " " + (vObj.CostesDescTotalOferta[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkAtrib(vObj.CostesDescTotalOferta[i],iAmbito) + "\">" + (vObj.CostesDescTotalOferta[i].obligatorio?"(*)":"") + vObj.CostesDescTotalOferta[i].den + "</a></td><td><a class='popUp" + " " + (vObj.CostesDescTotalOferta[i].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkAtrib(vObj.CostesDescTotalOferta[i],iAmbito) + "\"><IMG border = 0 SRC='../images/masinform.gif'></a></td></tr></table></nobr>"
    
	strAtrib+="			</td>"
	strAtrib+="			<td class='" + vClase + " importesParciales' style='width=" + wCAtributo + "'>"
    strAtrib+=              num2str(importeAnterior,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon + " " + obtenerOperador(vObj.CostesDescTotalOferta[i].operacion) + " " + num2str(costeDescValor.valor,vdecimalfmt,vthousanfmt,vprecisionfmt)  + " " + obtenerUnidad(vObj.CostesDescTotalOferta[i].operacion, gCodMon) + " = " + num2str(costeDescValor.importeParcial,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
	strAtrib+="			</td>"
	strAtrib+="		</tr>"
	if (vClase == "filaImpar")
		vClase = "filaPar"
	else
		vClase = "filaImpar"
	}
    if(costeDescValor.importeParcial)
        importeAnterior = costeDescValor.importeParcial
    }
strAtrib+="	</table>"


return (strAtrib)
}




/*
''' <summary>
''' Mostrar por pantalla los Costes / Descuentos aplicados a total de grupo
''' </summary>
''' <param name="vObj">Objeto proceso q contiene toda la oferta</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: mostrarProceso; Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
*/


function mostrarCostesDescuentosGrupo(vObj)
{
var strAtrib
var strAtribAux
var i
var j
strAtrib = ""
strAtribAux = ""
strAtrib+="	<table style='width:100%;'>"
var vClase

vClase = "filaImpar"
var iAmbito
var costeDescValor
var importeAnterior
var importeParcialCosteDesc
var importeBrutoGrupo
var importeNetoGrupo
var importeAnterior
var vGrupo
var i
var j
var k

iAmbito = vObj.proceso?2:1
for (i=0;i<vObj.grupos.length;i++)
{
    bEncontradoGrupo = false
    vGrupo = vObj.grupos[i]
    importeBrutoGrupo = vGrupo.importeBruto
    importeNetoGrupo = vGrupo.importe
    importeAnterior = importeBrutoGrupo
    importeParcialCosteDesc = 0
	//if(importeNetoGrupo && proceso.importe)
	// Quito la condición de proceso.importe
    if(importeNetoGrupo)
    {
        strAtrib+="		<tr>"
	    strAtrib+="			<td width=70% class='" + vClase + "'>"
        strAtrib+="	Grupo   " + vGrupo.den
	    strAtrib+="			</td>"
	    strAtrib+="			<td class='" + vClase + " importesParciales' style='width=" + wCAtributo + "'>"
        strAtrib+=              num2str(importeNetoGrupo,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
	    strAtrib+="			</td>"
	    strAtrib+="		</tr>"
        if (vClase == "filaImpar")
		    vClase = "filaPar"
	    else
		    vClase = "filaImpar"

        for (j=0;j<vObj.CostesDescTotalGrupo.length;j++)
	    {
            for (k=0;k<vObj.CostesDescTotalGrupo[j].grupos.length;k++)
            {
                if (vObj.CostesDescTotalGrupo[j].grupos[k] == vGrupo.id)
                {
                    bEncontradoGrupo = true                	
                    break
                }
            }
            if(bEncontradoGrupo)
            {

                if(vObj.CostesDescTotalGrupo[j].ambito==1)
                    costeDescValor = obtenerCosteDescValor(vObj.costesDescuentos,vObj.CostesDescTotalGrupo[j])
                else
                    costeDescValor = obtenerCosteDescValor(vGrupo.costesDescuentos,vObj.CostesDescTotalGrupo[j])
				//if (costeDescValor.valor && proceso.importe)
				// DPP quito la condición de proceso.importe
                if (costeDescValor.valor)
                {
                    for (k=0;k<vObj.ImportesParcialesGrupo.length;k++)
                    {
                        if (vObj.ImportesParcialesGrupo[k].grupo == vGrupo.cod && vObj.CostesDescTotalGrupo[j].paid == vObj.ImportesParcialesGrupo[k].costeDesc)
                        {
                            importeParcialCosteDesc = vObj.ImportesParcialesGrupo[k].importeParcial
                            break
                        }
                    }   

	                strAtrib+="		<tr>"
	                strAtrib+="			<td width=70% class='" + vClase + "'>"
	                strAtrib+="				<input type=hidden id=pAtribCod name=pAtribCod value=" + vObj.CostesDescTotalGrupo[j].cod + ">"
	                strAtrib+="					<table><tr><td><nobr><a class='popUp" + " " + (vObj.CostesDescTotalGrupo[j].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkAtrib(vObj.CostesDescTotalGrupo[j],iAmbito) + "\">" + (vObj.CostesDescTotalGrupo[j].obligatorio?"(*)":"") + vObj.CostesDescTotalGrupo[j].den + "</a></td><td><a class='popUp" + " " + (vObj.CostesDescTotalGrupo[j].obligatorio?" negrita ":"") +   "' href='javascript:void(null)' onclick=\"" + linkAtrib(vObj.CostesDescTotalGrupo[j],iAmbito) + "\"><IMG border = 0 SRC='../images/masinform.gif'></a></td></tr></table></nobr>"
                    
	                strAtrib+="			</td>"
	                strAtrib+="			<td class='" + vClase + " importesParciales' style='width=" + wCAtributo + "'>"
                    strAtrib+=              num2str(importeAnterior,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon + " " + obtenerOperador(vObj.CostesDescTotalGrupo[j].operacion) + " " + num2str(costeDescValor.valor,vdecimalfmt,vthousanfmt,vprecisionfmt)  + " " + obtenerUnidad(vObj.CostesDescTotalGrupo[j].operacion, gCodMon) + " = " + num2str(importeParcialCosteDesc,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + gCodMon
	                strAtrib+="			</td>"
	                strAtrib+="		</tr>"
	                if (vClase == "filaImpar")
		                vClase = "filaPar"
	                else
		                vClase = "filaImpar"
                }
                if(importeParcialCosteDesc)
                    importeAnterior = importeParcialCosteDesc
            }
        }
    }
}

strAtrib+="	</table>"


return (strAtrib)
}



/*
''' <summary>
''' Recalcula todos los importes en función de las modificaciones efectuadas en los valores de los costes/descuentos
''' </summary>
<param name=calcularEnItem>Indica si el cálculo se va a hacer desde el recálculo de Item</param>
<param name=grupos>Lista de grupos afectados por el recálculo</param>
<param name=sNameCosteDescuento>Nombre del campo de coste/descuento modificado</param>
<param name=valor>Nuevo valor del campo de coste/descuento modificado</param>
<param name=idCosteDescuento>Identificador del coste/descuento modificado</param>
<param name=importeParcial>Importe parcial del coste/descuento modificado</param>
<param name=aplica>A qué aplica el coste/descuento modificado, si a Total de la oferta, a Total del grupo, a Total del ítem o a Precio del ítem</param>
<param name=iAmbito>Ámbito del coste/descuento modificado, si a </param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: linkRecalculoOferta; Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
*/

function recalcularCostesDescuentos(calcularEnItem, grupos, sNameCosteDescuento, valor, idCosteDescuento, importeParcial, aplica, iAmbito)
{
var vObj=proceso
var Entrar=0
var PrimerRegistro=0

//aplica=0 --> aplica a Total Oferta
//aplica=1 --> aplica a Total Grupo
//aplica=2 --> aplica a Total Item
//aplica=3 --> aplica a Precio Item
var aplicaTotalOferta = 0
var aplicaTotalGrupo  = 1
var aplicaTotalItem   = 2
var aplicaPrecioItem  = 3


var importeOfertabruto = 0
var impAnterior
var impCalculado
var vGru
var bGrupoTratar=0
var bRecalculaServidor=0
var vGrupo

if(grupos && !calcularEnItem)
{
 for (i=0;i<grupos.length;i++)
    {
        if (grupos[i].costeDescRecalculo.aplica==aplicaTotalItem || grupos[i].costeDescRecalculo.aplica==aplicaPrecioItem)
        {
            bRecalculaServidor = 1
            break
        }
    }
}
if (aplica && bRecalculaServidor ==0)
{
    if (aplica==aplicaTotalItem || aplica==aplicaPrecioItem)
        bRecalculaServidor = 1
}
       
if (bRecalculaServidor)
{
    if(!confirm(den[99]))  //JVS Idioma La oferta será almacenada. ¿Desea continuar?"
        return false
    else
    {
        prepararOfertaAEnviar(1,1)
        return true
    }
   //mostrar mensaje que se va a guardar la oferta si-No
   //Si responde no nos vamos
   //Si responde sí llamamos a PrepararOferta(1) pasandole param de recalculo
   //En guardaroferta.asp si se guarda con recalculo hay que recargar la pagina cumpoferta.asp e ir a la parte donde estaba el usuario
    
}
    
    NumGrupo=0
    for (j=0;j<proceso.grupos.length;j++)
    {
        
        NumGrupo=NumGrupo+1
        if (grupos)
        {
          for (indiceGr=0;indiceGr<grupos.length;indiceGr++)
              {
                bGrupoTratar = 0
              if (proceso.grupos[j].cod==grupos[indiceGr].cod) {               
                vObj.importeBruto=vObj.importeBruto - proceso.grupos[j].importe
                if (!calcularEnItem)
                {
                    sNameCosteDescuento = grupos[indiceGr].costeDescRecalculo.nombreCosteDesc
                    valor = grupos[indiceGr].costeDescRecalculo.valor
                    idCosteDescuento = grupos[indiceGr].costeDescRecalculo.idCosteDescuento
                    importeParcial = grupos[indiceGr].costeDescRecalculo.importeParcial
                    aplica = grupos[indiceGr].costeDescRecalculo.aplica
                    iAmbito = grupos[indiceGr].costeDescRecalculo.ambito
                    if(aplica == 1)
                        bGrupoTratar=1
                }
                else
                {
                    for (iCostes=0;iCostes<proceso.CostesDescTotalGrupo.length;iCostes++)
                    {
                        for (iGrupos=0;iGrupos<proceso.CostesDescTotalGrupo[iCostes].grupos.length;iGrupos++)
                        {
                            if (proceso.CostesDescTotalGrupo[iCostes].grupos[iGrupos]==grupos[0].id)
                            {
                                idCosteDescuento = proceso.CostesDescTotalGrupo[iCostes].paid
                                if(proceso.CostesDescTotalGrupo[iCostes].ambito==1)
                                    objCosteDescValor = obtenerCosteDescValor(proceso.costesDescuentos,proceso.CostesDescTotalGrupo[iCostes])
                                else
                                    objCosteDescValor = obtenerCosteDescValor(grupos[0].costesDescuentos,proceso.CostesDescTotalGrupo[iCostes])
                                valor = objCosteDescValor.valor 
                                aplica = 1
                                bGrupoTratar=1
                                break
                            }
                        }
                        if(bGrupoTratar)
                            break
                    } 
                    if(!bGrupoTratar)    
                    {
                        aplica = 1
                        bGrupoTratar=1
                    }
                }
              }
            }
        }
        if (!grupos && NumGrupo==1)
        {
            if(aplica == 1)
            {
                 vObj.importeBruto=0
                 bGrupoTratar=1
            }
        }

        if(bGrupoTratar==1)
        {
            vGru=proceso.grupos[j]
        
            impAnterior=vGru.importeBruto 
            PrimerRegistro=0
            Entrar=0
            for (i=0;i<proceso.CostesDescTotalGrupo.length;i++)
	        {
               if (proceso.CostesDescTotalGrupo[i].paid==idCosteDescuento)
                {
                PrimerRegistro=1
                Entrar=1
                valorCosteDesc = valor

                }
               else
                {
                 if (Entrar==0)
                 {
                    /* Si se trata de un Coste/Desc de ámbito oferta y no estamos tratando un grupo, recuperamos el importe
                    anterior que tenemos guardado en la oferta */
                        /* Si se trata de un Coste/Desc de ámbito grupo, recuperamos el importe anterior que tenemos guardado 
                        en el grupo */
                        if(proceso.CostesDescTotalGrupo[i].ambito==2)
                            costeanterior=obtenerCosteDescValor(vGru.costesDescuentos,proceso.CostesDescTotalGrupo[i])
                    /* Si se trata de un Coste/Desc de ámbito oferta y estamos tratando un grupo, recuperamos el importe 
                    anterior que tenemos guardado en la estructura ImportesParcialesGrupo asociada al grupo que estamos tratando */
                    if(proceso.CostesDescTotalGrupo[i].ambito==1)
                    {
                        impAux=obtenerImpParcialAnterior(vGru.cod,proceso.CostesDescTotalGrupo[i].paid)
                        if (impAux)
                            impAnterior = impAux
                    }
                    else
                        if (costeanterior.importeParcial)
                          impAnterior=costeanterior.importeParcial    
                 }
                }
                if (Entrar==1)
                {
                //si el atributo afecta al grupo j calculo la operacion
                  bAplicaAlGrupo=0
                  for (k=0;k<proceso.CostesDescTotalGrupo[i].grupos.length;k++)
                  {
                   if (proceso.CostesDescTotalGrupo[i].grupos[k]==vGru.id)
                   {
                     bAplicaAlGrupo=1
                     break
                     }
                  }

                  if(proceso.CostesDescTotalGrupo[i].ambito==1)
                    {
                        costeDescValor = obtenerCosteDescValor(vObj.costesDescuentos,proceso.CostesDescTotalGrupo[i])
                        impAux=obtenerImpParcialAnterior(vGru.cod,proceso.CostesDescTotalGrupo[i].paid)
                        impParcialPrevio = impAux
                        if (!costeDescValor.importeParcial)
                               costeDescValor.importeParcial =0
                        if (!grupos)
                           {
                           if (NumGrupo==1)
                              costeDescValor.importeParcial=0
                           }
                        else
                           if (impAux && costeDescValor.importeParcial)
                               costeDescValor.importeParcial =costeDescValor.importeParcial - impAux
                    }
                    else
                    {
                        costeDescValor = obtenerCosteDescValor(vGru.costesDescuentos,proceso.CostesDescTotalGrupo[i])
                        impParcialPrevio = obtenerImpParcialAnterior(vGru.cod,proceso.CostesDescTotalGrupo[i].paid)
                    }

                  if (bAplicaAlGrupo==1)
                  {

                    // Si es el registro modificado hay que guardar el nuevo valor en la estructura, si no recuperamos el valor desde la estructura
                    if (PrimerRegistro==0)
                        valorCosteDesc=costeDescValor.valor
                    else
                        
                        costeDescValor.valor=valorCosteDesc
                    // Se calcula el nuevo importe parcial
                    
                    impCalculado=obtenerImporteParcial(proceso.CostesDescTotalGrupo[i].operacion,valorCosteDesc,impAnterior) 
                    if(!impCalculado) impCalculado = 0
                    if(proceso.CostesDescTotalGrupo[i].ambito==1)
                        costeDescValor.importeParcial=costeDescValor.importeParcial+impCalculado
                    else
                        costeDescValor.importeParcial=impCalculado
                    if (impCalculado)
                      impAnterior=impCalculado
                    PrimerRegistro=0
                    //break
                    impParcialActual = impAnterior
                    guardarImpParcialGrupo(vGru.cod,costeDescValor.paid,impParcialPrevio,impParcialActual)
                   }
                   else
                    {
                    if(proceso.CostesDescTotalGrupo[i].ambito==1)
                            costeDescValor.importeParcial=costeDescValor.importeParcial+impAnterior
                    }
                  } //Entrar
	            }//for costes/desc
            //}//grupo tratar
            //guardar en la estrucutura correspondiente el importe calculado del ultimo atributo como importe neto del grupo
            if(aplica==1)
            {
                vGru.importe=impAnterior
                //importeofertabruto=importeofertabruto+el importe calculado del ultimo atributo 

                
            if (esSubasta)
			{
                if (vGru.importe == 0)
                {
                    //dpd  En el caso de subasta, Si el importe es 0, hay que comprobar si realmente tiene que ser null verificando todos los ítems
                    // suponemos que es null, salvo que encontremos items con valor distinto de null
                    vGru.importe = null
                    //recorrer todos lo items y localizar los que sean del grupo en curso
                    for (i=0;i<=itemsPuja.length-1;i++)
                    {
                        if (itemsPuja[i].grupo == vGru.cod)
                        {
                            if(itemsPuja[i].precio !=null)
                            {
                                vGru.importe = 0
                            }
                        }
                    }
                }
			}
			else
            {
                if (vGru.importe == 0)
                {
                    vGru.importe = null
                    //recorrer todos lo items y localizar los que sean del grupo en curso
                    for (i=0;i<=vGru.items.length-1;i++)
                    {
                        if(vGru.items[i].precio !=null)
                        {
                            vGru.importe = 0
                        }
                    }
                }
             }
                vObj.importeBruto=vObj.importeBruto + vGru.importe
            }
        } //fin if bGrupoTratar

    } //fin bucle grupos



    
    impAnterior=vObj.importeBruto
    if (Entrar==0 && calcularEnItem) 
        Entrar=1
    for (i=0;i<proceso.CostesDescTotalOferta.length;i++)
    {
        if (Entrar==0)
        {  
           if (proceso.CostesDescTotalOferta[i].paid==idCosteDescuento)
           {
            PrimerRegistro=1
            Entrar=1
            valorCosteDesc = valor
           }
           else
           {
                    costeanterior=obtenerCosteDescValor(vObj.costesDescuentos,proceso.CostesDescTotalOferta[i])
                if (costeanterior.importeParcial)
                  impAnterior=costeanterior.importeParcial
           }
        }
        if (Entrar==1)
        {
                costeDescValor = obtenerCosteDescValor(vObj.costesDescuentos,proceso.CostesDescTotalOferta[i])
            // Si es el registro modificado hay que guardar el nuevo valor en la estructura, si no recuperamos el valor desde la estructura
            if (PrimerRegistro==0)
                valorCosteDesc=costeDescValor.valor
            else                
                costeDescValor.valor=valorCosteDesc
                // Se calcula el nuevo importe parcial
            
            if (impAnterior)
                impCalculado=obtenerImporteParcial(proceso.CostesDescTotalOferta[i].operacion,valorCosteDesc,impAnterior) 
            else
                impCalculado = 0
            costeDescValor.importeParcial=impCalculado
            if (impCalculado)
               impAnterior=impCalculado
            PrimerRegistro=0
        }
    }

    vObj.importe=impAnterior

return true

}






/*
''' <summary>
''' Recalcula todos los importes en función de las modificaciones efectuadas en los valores de los costes/descuentos aplicados a un ítem
''' </summary>
<param name=vGr>Grupo del ítem</param>
<param name=oItem>Objeto que contienen al ítem</param>
<param name=valor>Nuevo valor del campo de coste/descuento modificado</param>
<param name=precioUnitarioInicial>Precio Unitario del ítem antes de aplicar los costes/descuentos</param>
<param name=subasta>Indica si se trata de una subasta</param>
<param name=precioUnitarioInicialAnterior>Precio bruto antes de cambiarlo el usuario</param>
<param name=paidCD>Identificador del coste/descuento modificado y que provoca el recálculo</param>
<param name=valorAnterior>Valor previo del coste/descuento modificado</param>
<param name=bRecalcCD>Cuando se abre items desde el menu de la izquierda a recalc por si se han cambiado c/d de grupo /proceso q influyan</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: MostrarDivCostesDescItem;CerrarDivCostesDescItem;linkRecalcularItem Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
*/

function recalcularCostesDescuentosItem(vGr,oItem,precioUnitarioInicial,subasta,oItemSecundario, precioUnitarioInicialAnterior, paidCD, valorAnterior,divModificable, iesc, bRecalcCD) {
var vObj=proceso
var Entrar=0
var PrimerRegistro=0

var aplicaTotalItem   = 2
var aplicaPrecioItem  = 3

var importeOfertabruto = null
var impAnterior
var impCalculado
var vGru
var bGrupoTratar=0
var bRecalculaServidor=0
var vGrupo
var iAmbitoOferta = 1
var iAmbitoGrupo = 2
var iAmbitoItem = 3
var importeNetoNuevo = null
var importeBrutoGrupo = null
var importeNetoGrupo = null
var importeBrutoOferta = null
var gruposRecalc = new Array()
var precioNetoAnterior
var cant = oItem.cant


var objCosteDescImporteParcialAnterior

if (iesc>=0)
{
	sEscId = '_' + oItem.escalados[iesc].id

	cant = oItem.escalados[iesc].cant
		
}
else
{
	sEscId = ""
}

if(divModificable==null)
    divModificable=true
costesDescuentosImpParcialAnterior = new Array()
costesDescuentosImpParcialAnterior = costesDescuentosImpParcial

costesDescuentosImpParcial = new Array()

PrimerRegistro=0
precioUnitarioFinal = null
if(divModificable)
    precioNetoAnterior = oItem.precioNeto
//si no hay precio unitario inicial no recalcula
//dpd Incidencia 18612
//if (!precioUnitarioInicial)
//    precioUnitarioInicial = 0
    //return false
//el importe parcial es el precio unitario inicial
impParcial = precioUnitarioInicial

//jvs Incidencia 18873
//calculamos el importe parcial antes del recálculo a partir del precio unitario que había antes de que el usuario lo modificara
if (precioUnitarioInicialAnterior==null)
    impParcialAnterior = precioUnitarioInicial
else
    impParcialAnterior = precioUnitarioInicialAnterior

//el precio neto es el precio unitario inicial (salvo que haya costes / descuentos aplicados al precio, en cuayo caso se recalculará)
if(divModificable)
    oItem.precioNeto = precioUnitarioInicial

if(divModificable)
    importeNetoItem = oItem.importeNeto
else
    importeNetoItem = null
precioNetoCalculado = precioUnitarioInicial
if(vGr.importe)
    importeNetoGrupo = vGr.importe
if(vGr.importeBruto)
    importeBrutoGrupo = vGr.importeBruto

//al importe bruto de la oferta le restamos el importe neto del grupo
if (proceso.importeBruto)
    importeBrutoOferta = proceso.importeBruto - importeNetoGrupo

//al importe bruto del grupo le restamos el importe neto del item
//if (bRecalcCD == 0) {
    if (vGr.hayEscalados == false) {
        if (importeNetoItem != null) importeBrutoGrupo -= (importeNetoItem ? importeNetoItem : 0)
    }
    else {
        if (oItem.escalados[iesc].usar) {
            if (importeNetoItem != null) importeBrutoGrupo -= (importeNetoItem ? importeNetoItem : 0)
        }
    }
//}
//else {
    //Pruebas 31900.6 / 2012 / 415: El grupo esta YA recalculado con los c/d pero no los items
//    importeBrutoGrupo = 0
//}
for(i=0;i<vGr.costesDescuentosItems.length;i++)
{
            
    //En el momento en que el coste/descuento aplique al total del item por vez primera, calculamos el PU final y el importe
    // este importe se convierte en el nuevo importe a partir del cual hay que hacer los cálculos
    if(vGr.costesDescuentosItems[i].aplicar==aplicaTotalItem && PrimerRegistro==0)
    {
        //el precio unitario final está guardado en la variable impParcial
        precioUnitarioFinal = impParcial

		if(cant!=null)
		{
		
			//el nuevo importe parcial es el precio unitario final por la cantidad
			if (solicitarCantMax)
			{
			    if (oItem.cantmax != null && (oItem.cantmax <= cant)) 
                {
                    if (impParcial != null) 
                        impParcial *= oItem.cantmax
			    }
			    else
			        if (impParcial != null) {
			            if (oItem.cant != null)
			                impParcial *= cant
			            else
			                impParcial = null
			        }	
			}
			else
			{
				if (impParcial!=null)
				{
					if (cant!=null) 
						impParcial *= cant
					else
						imParcial = null
				}
			}


			//el importe parcial anterior también se calcula como el producto del precio por la cantidad
			if (solicitarCantMax)
			{
			    if (oItem.cantmax != null && (oItem.cantmax <= cant)) 
                {
                    if (impParcialAnterior != null) 
                        impParcialAnterior *= oItem.cantmax
			    }
			    else
			        if (impParcialAnterior != null) {
			            if (cant != null)
			                impParcialAnterior *= cant
			            else
			                impParcialAnterior = null
			        }
			}
			else
			{
				if (impParcialAnterior!=null)
				{
					if (cant!=null) 
						impParcialAnterior *= cant
					else
						impParcialAnterior = null
				}
			}
		}

        //el nuevo precio neto (tras aplicar los costes/descuentos será este precio unitario final) y se guarda en el item
         if(divModificable)
            oItem.precioNeto = precioUnitarioFinal
			precioNetoCalculado = precioUnitarioFinal
        PrimerRegistro=1
    }
	
    // obtenemos el objeto que contiene el valor del coste/descuento y los importes parciales
    if (vGr.costesDescuentosItems[i].ambito == iAmbitoOferta)
            objCosteDescValor = obtenerCosteDescValor(proceso.costesDescuentos,vGr.costesDescuentosItems[i])
    else if (vGr.costesDescuentosItems[i].ambito == iAmbitoGrupo)
            objCosteDescValor = obtenerCosteDescValor(vGr.costesDescuentos,vGr.costesDescuentosItems[i])
    else  //if (vGr.costesDescuentosItems[i].ambito == iAmbitoItem)
         {
		 objCosteDescValor = obtenerCosteDescValor(oItem.costesDescuentosOfertados,vGr.costesDescuentosItems[i])
		 if (iesc>=0) 
			{		
				if(objCosteDescValor.escalados[iesc]) // @@DPD No debería ser necesaria la comprobación
					objCosteDescValor.valor = objCosteDescValor.escalados[iesc].valorNum
			}
		}

	if (vGr.costesDescuentosItems[i].aplicar!=aplicaTotalItem || cant!=null)
	{
	// Se calcula el nuevo importe parcial
        if(objCosteDescValor.valor && impParcial!=null)
			
            impParcial=obtenerImporteParcial(vGr.costesDescuentosItems[i].operacion,objCosteDescValor.valor,impParcial) 

    // Se calcula el importe parcial Anterior
    // Si se ha modificado el coste/descuento el recálculo del importe anterior ha de hacerse usando como base el valor antes de la modificación
        if (paidCD == vGr.costesDescuentosItems[i].paid)
        {
            if(valorAnterior && impParcialAnterior)
                impParcialAnterior=obtenerImporteParcial(vGr.costesDescuentosItems[i].operacion,valorAnterior,impParcialAnterior) 
        }
        else
        {
            if(objCosteDescValor.valor && impParcialAnterior)
                impParcialAnterior=obtenerImporteParcial(vGr.costesDescuentosItems[i].operacion,objCosteDescValor.valor,impParcialAnterior) 
        }
    if (costesDescuentosImpParcialAnterior != null)        
        objCosteDescImporteParcialAnterior = obtenerCosteDescValor(costesDescuentosImpParcialAnterior, vGr.costesDescuentosItems[i])
    else
        objCosteDescImporteParcialAnterior = null

		
	}	
		
    //El nuevo importe parcial del coste/descuento será el calculado en el paso anterior    

    var impParcialActual = null
    var impParcialPrevio = null
    //Recálculo del importe parcial del c/d partiendo del importe calculado para ese ítem y del que tenía antes de que se modificara el precio unitario o el c/d
    if(vGr.costesDescuentosItems[i].aplicar==aplicaTotalItem)
    {
        if (objCosteDescImporteParcialAnterior != null)
        {
            if(divModificable)
                objCosteDescValor.importeParcial = objCosteDescValor.importeParcial - objCosteDescImporteParcialAnterior.importeParcial + impParcial
            impParcialActual = impParcial
            impParcialPrevio = objCosteDescImporteParcialAnterior.importeParcial
        }
    }
    else
    {
        if (objCosteDescImporteParcialAnterior != null)
        {
            if(divModificable)
			{
                
				if(cant!=null)
				{
					objCosteDescValor.importeParcial = objCosteDescValor.importeParcial - objCosteDescImporteParcialAnterior.importeParcial * cant + impParcial * cant
					impParcialActual = impParcial * cant
					impParcialPrevio = objCosteDescImporteParcialAnterior.importeParcial * cant
				}
				else
				{
					impParcialActual = null
					objCosteDescValor.importeParcial = null
					impParcialPrevio = null
				}
			}
        }
    }
    guardarImpParcialGrupo(vGr.cod,objCosteDescValor.paid,impParcialPrevio, impParcialActual)
    // se guarda el importe parcial del coste/descuento respecto a ese ítem
    anyaCosteDescuentoImpParcial(costesDescuentosImpParcial, i, objCosteDescValor)
    costesDescuentosImpParcial[i].importeParcial = impParcial
	
}
if (PrimerRegistro==1)
	{   //si hay costes/desc que se aplican al total de item, se mantiene el importe el importe neto final del item es el último calculado
		if (cant!=null) importeNetoNuevo = impParcial
	}
else  //si no hay costes/desc que se aplican al total de item, el importe neto del item es el precioUnitarioFinal * cantidad
{
    //el precio unitario final está guardado en la variable impParcial
    precioUnitarioFinal = impParcial
    if (solicitarCantMax)
	{
		if (oItem.cantmax!=null && (oItem.cantmax <= cant))
			importeNetoNuevo = impParcial * oItem.cantmax
		else
			if(oItem.cant!=null) importeNetoNuevo = impParcial * cant
	}
    else
	{
		if(cant!=null) importeNetoNuevo = impParcial * cant
    }
    //el nuevo precio neto (tras aplicar los costes/descuentos será este precio unitario final) y se guarda en el item
	//dpd incidencia 18612
    if(divModificable)
    {
        if (precioUnitarioInicial == null)
            oItem.precioNeto = null
        else 
            oItem.precioNeto = precioUnitarioFinal
	}
    if (precioUnitarioInicial == null)
        precioNetoCalculado = null
    else 
        precioNetoCalculado = precioUnitarioFinal

}

//al importe bruto del grupo le sumamos el nuevo importe neto del item
//if (oItem.importeNeto!=null) importeBrutoGrupo += oItem.importeNeto
if (importeNetoNuevo != null) {
    if (iesc >= 0) {
        if (oItem.escalados[iesc].usar) {
            importeBrutoGrupo += importeNetoNuevo
        }
    } 
    else     
        importeBrutoGrupo += importeNetoNuevo
}

if (divModificable)
    if (cant != null) {
        if (iesc >= 0) {
            if (oItem.escalados[iesc].usar) {
                oItem.importeNeto = importeNetoNuevo
            }
        }
        else
            oItem.importeNeto = importeNetoNuevo
    }        

if (importeBrutoGrupo == 0)
{
    if (esSubasta)
	{
		//dpd  En el caso de subasta, Si el importe es 0, hay que comprobar si realmente tiene que ser null verificando todos los ítems
		// suponemos que es null, salvo que encontremos items con valor distinto de null
		importeBrutoGrupo = null
		//recorrer todos lo items y localizar los que sean del grupo en curso
		for (i=0;i<=itemsPuja.length-1;i++)
		{
			if (itemsPuja[i].grupo == vGr.cod)
			{
				if(itemsPuja[i].precio !=null)
				{
					importeBrutoGrupo = 0
				}
			}
		}
	}
	else
    {
		importeBrutoGrupo = null
		//recorrer todos lo items y localizar los que sean del grupo en curso
		for (i=0;i<=vGr.items.length-1;i++)
		{
			if(vGr.items[i].precio !=null)
			{
				importeBrutoGrupo = 0
			}
		}
	}    
	
}


if(divModificable) {
	if (cant!=null)
		vGr.importeBruto = importeBrutoGrupo

    //recalcular grupo
        gruposRecalc[0]=vGr
        recalcularCostesDescuentos(true,gruposRecalc)

    //al importe neto del grupo le sumo el importe bruto de la oferta, y obtengo el nuevo bruto de la oferta
    proceso.importeBruto = importeBrutoOferta + vGr.importe

    //recálculo de la oferta
    recalcularCostesDescuentos(true)
    proceso.costeDescRecalculo = null
}

strDetOferta = ""
if(divModificable)
{
    //muestra el importe del item neto, una vez aplicados los costes/descuentos que correspondan
    if(importeNetoNuevo!=null) {
        if (vGr.hayEscalados == false) {
            strDetOferta = "<b>" + num2str(oItem.importeNeto, vdecimalfmt, vthousanfmt, vprecisionfmt) + "</b>"
        }
        else {
            if (oItem.escalados[iesc].usar) {
                strDetOferta = "<b>" + num2str(oItem.importeNeto, vdecimalfmt, vthousanfmt, vprecisionfmt) + "</b>"
            }
            else {
                strDetOferta = "<b></b>"
            }
        }
        if(subasta)
        {			
             document.getElementById("divTotalLinea4" + "_" + oItem.id).innerHTML = strDetOferta
        }
        else
        {						
            if(precioNum=="")			
                 if(document.getElementById("divTotalLinea1" + "_" + oItem.id + sEscId)) document.getElementById("divTotalLinea1" + "_" + oItem.id + sEscId).innerHTML = strDetOferta
            else if(precioNum==2)
                 if(document.getElementById("divTotalLinea2" + "_" + oItem.id + sEscId)) document.getElementById("divTotalLinea2" + "_" + oItem.id + sEscId).innerHTML = strDetOferta
            else if(precioNum==3)
                 if(document.getElementById("divTotalLinea3" + "_" + oItem.id + sEscId)) document.getElementById("divTotalLinea3" + "_" + oItem.id + sEscId).innerHTML = strDetOferta
        }
    }
    importeNetoItem = oItem.importeNeto

    //muestra el importe del grupo neto, una vez aplicados los costes/descuentos que correspondan
    if ( document.getElementById("divCabeceraPrecios_" + vGr.cod))
	     document.getElementById("divCabeceraPrecios_" + vGr.cod).innerHTML = den[57] + " " + vGr.cod + "&nbsp;" + den[58] + num2str(vGr.importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;" + proceso.mon.cod



	if(oItemSecundario)
	{
		eval("oItemSecundario.precio" + precioNum + " = oItem.precio" + precioNum)
		oItemSecundario.precioNeto = oItem.precioNeto
		oItemSecundario.importeBruto = oItem.importeBruto
		oItemSecundario.importeNeto = oItem.importeNeto

	}
}
else
{
	// sólo para cprecios alternativos ---->  No hay escalados.
    if(importeNetoNuevo!=null)
    {
        strDetOfert = "<b>" + num2str(importeNetoNuevo,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"		 
        if(precioNum=="")
             document.getElementById("divTotalLinea1" + "_" + oItem.id).innerHTML = strDetOfert
        else if(precioNum==2)
             document.getElementById("divTotalLinea2" + "_" + oItem.id).innerHTML = strDetOfert
        else if(precioNum==3)
             document.getElementById("divTotalLinea3" + "_" + oItem.id).innerHTML = strDetOfert
    }
}

if (iesc>=0)
{
	if (!oItem.escalados[iesc].usar)
	{
		oItem.precioNeto = precioNetoAnterior
	}		
}

return true

}



/*
''' <summary>
''' añade un elemento a la estructura de costes/descuentos creada para almacenar los importes parciales de los de ámbito proceso que aplican a precio de ítem o total de ítem 
''' </summary>
<param name=costesDescuentosImpParcial>estructura de costes/descuentos</param>
<param name=i>posición en que se guarda el elemento</param>
<param name=costeDescValor>coste/descuento que queremos almacenar</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
''' <revision>JVS 09/05/2011</revision>
*/

function anyaCosteDescuentoImpParcial(costesDescuentosImpParcial, i, costeDescValor)
{
    costesDescuentosImpParcial[i]=new Atributo(costeDescValor.id, costeDescValor.paid, costeDescValor.cod, costeDescValor.den, costeDescValor.intro, costeDescValor.tipo, costeDescValor.valor, costeDescValor.obligatorio, costeDescValor.valorMin, costeDescValor.valorMax, costeDescValor.lista, costeDescValor.obj, costeDescValor.operacion, costeDescValor.aplicar, costeDescValor.ambito, costeDescValor.importeParcial, costeDescValor.grupos, costeDescValor.alcance)
}


/*
''' <summary>
''' Obtiene los valores de los costes/descuentos del proceso y de sus importes parciales cuando se produce un cambio de moneda
''' </summary>
<param name=cambio>Cambio a aplicar</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cambiarMoneda; Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
*/

function aplicarCambioCostesDescuentos(cambio)
{
var iAmbitoOferta = 1
   for(i=0;i<proceso.costesDescuentos.length;i++)
    {
            
        // obtenemos el objeto que contiene el valor del coste/descuento y los importes parciales
        procesoCosteDescValor = proceso.costesDescuentos[i]
        // se calcula el valor una vez aplicado el cambio (si corresponde)
        procesoCosteDescValor.valor = obtenerValorCambioMoneda(procesoCosteDescValor.operacion,procesoCosteDescValor.valor,cambio)
            
        //El nuevo importe parcial del coste/descuento será el que tenga una vez aplicado el cambio
        procesoCosteDescValor.importeParcial = procesoCosteDescValor.importeParcial * cambio
    }


    //actualizar importes parciales de atributos de grupo
    for (i=0;i<proceso.ImportesParcialesGrupo.length;i++)
        proceso.ImportesParcialesGrupo[i].importeParcial *= cambio

return true
}


/*
''' <summary>
''' Obtiene los valores de los costes/descuentos del grupo y de sus importes parciales cuando se produce un cambio de moneda
''' </summary>
<param name=cambio>Cambio a aplicar</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cambiarMoneda; Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
*/


function aplicarCambioCostesDescuentosGrupo(vGr,cambio)
{

var iAmbitoOferta = 1
var iAmbitoGrupo = 2

for(i=0;i<vGr.costesDescuentos.length;i++)
{
    procesoCosteDescValor = vGr.costesDescuentos[i]
    // se calcula el valor una vez aplicado el cambio (si corresponde)
    procesoCosteDescValor.valor = obtenerValorCambioMoneda(procesoCosteDescValor.operacion,procesoCosteDescValor.valor,cambio)
            
    //El nuevo importe parcial del coste/descuento será el que tenga una vez aplicado el cambio
    procesoCosteDescValor.importeParcial = procesoCosteDescValor.importeParcial * cambio
}
   
return true
}

/*
''' <summary>
''' Obtiene los valores de los costes/descuentos del ítem y de sus importes parciales cuando se produce un cambio de moneda
''' </summary>
<param name=cambio>Cambio a aplicar</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cambiarMoneda; Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
*/

function aplicarCambioCostesDescuentosItem(vGr,oItem,precioUnitarioInicial,cambio)
{

var iAmbitoOferta = 1
var iAmbitoGrupo = 2
var iAmbitoItem = 3


//si no hay precio unitario inicial no aplica el cambio
if (precioUnitarioInicial)
{
    for(i=0;i<oItem.costesDescuentosOfertados.length;i++)
    {
            
        // obtenemos el objeto que contiene el valor del coste/descuento y los importes parciales
        procesoCosteDescValor = oItem.costesDescuentosOfertados[i]
        // se calcula el valor una vez aplicado el cambio (si corresponde)
        procesoCosteDescValor.valor = obtenerValorCambioMoneda(procesoCosteDescValor.operacion,procesoCosteDescValor.valor,cambio)
            
        //El nuevo importe parcial del coste/descuento será el que tenga una vez aplicado el cambio
        procesoCosteDescValor.importeParcial = procesoCosteDescValor.importeParcial * cambio
    }
    //el nuevo precio se guarda en el item tras aplicar el cambio
    oItem.precio=oItem.precio * cambio
    //el nuevo precio neto (tras aplicar los costes/descuentos será el precio unitario final) se guarda en el item tras aplicar el cambio
    oItem.precioNeto = oItem.precioNeto * cambio
    //el nuevo importe neto del item 
    oItem.importeNeto = oItem.importeNeto * cambio
    //el nuevo importe bruto del item 
    oItem.importeBruto = oItem.importeBruto * cambio
}
    
return true

}


/*
''' <summary>
''' Guardar el importe parcial por grupo de un coste/descuento
''' </summary>
''' <param name="codGrupo">Código de grupo</param>
''' <param name="costeDescId">Identificador del coste/descuento</param>
''' <param name="impAnterior">Importe parcial del coste/descuento dentro del grupo</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: recalcularCostesDescuentos; Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <autor>JVS</autor>
*/

function guardarImpParcialGrupo(codGrupo,costeDescId,impParcialAnterior,impParcialActual)
{
    var indiceImpParcGr
    var encontrado=0
    for(indiceImpParcGr=0;indiceImpParcGr<proceso.ImportesParcialesGrupo.length;indiceImpParcGr++)
        if(proceso.ImportesParcialesGrupo[indiceImpParcGr].costeDesc == costeDescId && proceso.ImportesParcialesGrupo[indiceImpParcGr].grupo == codGrupo)
        {
            proceso.ImportesParcialesGrupo[indiceImpParcGr].importeParcial = proceso.ImportesParcialesGrupo[indiceImpParcGr].importeParcial - impParcialAnterior + impParcialActual
            encontrado = 1
            break
        }
    if(encontrado==0)
    {
	    imporParc = new CosteDescGrOfert(codGrupo, costeDescId, impParcialActual)
	    proceso.anyaImportesParcialesGrupo(imporParc)
    }
}


/*
''' <summary>
''' Obtiene el objeto donde se halla el valor del coste/descuento
''' </summary>
<param name=costeDescValor>estructura que contiene los valores de los costes/descuentos</param>
<param name=atributo>coste/descuento del que queremos recuperar el elemento de la estructura que contiene su valor</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
*/
function obtenerCosteDescValor(costeDescValor,atributo)
    {
        var indice
        var paid = atributo.paid
            
        for(indice=0;indice< costeDescValor.length;indice++) 
        {
		    if(paid == costeDescValor[indice].paid){
			    return costeDescValor[indice]
		    } 
	    }

    }

/*
''' <summary>
''' Obtener operador del Coste / Descuento
''' </summary>
''' <param name="costeDescOperacion">Operación</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: mostrarCostesDescuentos; Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <autor>JVS</autor>
*/

 function obtenerOperador(costeDescOperacion)
    {
        if(costeDescOperacion == '+' || costeDescOperacion == '+%')
			return '+'
        if(costeDescOperacion == '-' || costeDescOperacion == '-%')
			return '-'
        if(costeDescOperacion == '*')
			return '*'        
        if(costeDescOperacion == '/')
			return '/'        
    }


/*
''' <summary>
''' Unidad (% / moneda) del Coste / Descuento
''' </summary>
''' <param name="costeDescOperacion">Operación</param>
''' <param name="mon">Moneda</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: mostrarCostesDescuentos; Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <autor>JVS</autor>
*/

 function obtenerUnidad(costeDescOperacion, mon)
    {
        if(costeDescOperacion == '+' || costeDescOperacion == '-')
			return mon
        if (costeDescOperacion == '*'|| costeDescOperacion == '/')
            return ''
        if(costeDescOperacion == '+%' || costeDescOperacion == '-%')
			return '%'        
    }



  /*
''' <summary>
''' Importe parcial del Coste / Descuento
''' </summary>
''' <param name="costeDescOperacion">Operación</param>
''' <param name="mon">Moneda</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: mostrarCostesDescuentos; Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <autor>JVS</autor>
*/

 function  obtenerImporteParcial(costeDescOperacion, costeDescvalor,importeParcial)
 {
        if(costeDescOperacion == '+')
            return parseFloat(importeParcial) + parseFloat(costeDescvalor)
        if( costeDescOperacion == '+%')
			return parseFloat(importeParcial) + parseFloat(importeParcial) * parseFloat(costeDescvalor) * 0.01
        if(costeDescOperacion == '-')
            return parseFloat(importeParcial) - parseFloat(costeDescvalor)
        if(costeDescOperacion == '-%')
			return parseFloat(importeParcial) - parseFloat(importeParcial) * parseFloat(costeDescvalor) * 0.01
        if(costeDescOperacion == '*')
            return parseFloat(importeParcial) * parseFloat(costeDescvalor)
        if(costeDescOperacion == '/')
            return parseFloat(importeParcial) / parseFloat(costeDescvalor)
 }

/*
''' <summary>
''' Obtiene el valor del coste/descuento en función del cambio de moneda
''' </summary>
<param name=costeDescOperacion>operación que aplica el coste/descuento</param>
<param name=costeDescvalor>valor del coste/descuento</param>
<param name=cambio>cambio a aplicar</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: aplicarCambioCostesDescuentos; Tiempo máximo: 0</remarks>
cumpOferta.asp
''' <author>JVS</author>
*/

 function obtenerValorCambioMoneda(costeDescOperacion, costeDescvalor,cambio)
 {
        if (!cambio)
            cambio=1
        if(costeDescOperacion == '+')
            return parseFloat(costeDescvalor) * parseFloat(cambio)
        else if(costeDescOperacion == '-')
            return parseFloat(costeDescvalor) * parseFloat(cambio)
        else
            return parseFloat(costeDescvalor)        
 }



 

function estadoOferta(codest)
	{
    
	switch(codest)
		{
		case 1:
			return(den[46])
			break;
		case 2:
			return(den[78])
			break;
			
		case 3:
			return(den[79])
			break;
		case 4:
			return(den[80])
			break;
		case 5:
			return("<nobr>" + den[79] + "&nbsp;" + "<span class=fpeque>" + den[81] + "</span></nobr>")
			break;
			
		}
}	


/*
''' <summary>
''' Actualiza datos de las colecciones
''' </summary>
''' <param name="totOfertas">Ofertas</param>
''' <param name="fecUltimaOfe">Fecha de ÃƒÂºltima oferta</param>
''' <param name="estado">Estado</param>
''' <param name="importeNeto">Importe neto</param>
''' <param name="importeBruto">Importe bruto</param>
''' <param name="posicionPujaProceso">PosiciÃƒÂ³n de la puja</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: guardaroferta.asp; Tiempo mÃƒÂ¡ximo:0,1</remarks>
''' <revision>JVS 16/11/2011</revision>
*/

function actualizarDatosGenerales(totOfertas, fecUltimaOfe, estado, importeNeto, importeBruto, fecact) {

    proceso.totOfertas = totOfertas
    proceso.estado = estado
    proceso.importe = importeNeto
    proceso.importeBruto = importeBruto
    dFecActOferta = new Date(fecact.getFullYear(), fecact.getMonth(), fecact.getDate(), fecact.getHours(), fecact.getMinutes() , fecact.getSeconds())
    bGuardando = false
//    if (posicionPujaProceso)
//        proceso.posicionPuja = posicionPujaProceso


    document.getElementById("divCabApartado").innerHTML = generaCabecera(tituloActual)
    if (esSubasta) {
        MostrarCabeceraOferta()
        //DPD sÃƒÂ³lo hay que cambiar el estado    dibujarCabeceraSubasta()
    }

    for (i = 0; i < proceso.grupos.length; i++) {
        sEstado = estadoOferta(proceso.estado)
        strT = sEstado + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + (linkRecalculo == 0 ? num2str(proceso.importe, vdecimalfmt, vthousanfmt, vprecisionfmt) : "-") + "&nbsp;" + proceso.mon.cod + "&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(null)' onclick='verAyuda(" + proceso.estado + ")'><IMG border=0 SRC=../images/masinform.gif  WIDTH=11 HEIGHT=14 name=imgMasInform id=imgMasInform></a>"
        if (linkRecalculo == 1)
            strT += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(null)' onclick=\"linkRecalculoOferta()\"><img src=\"images/RecOferTot1-25x25.png\" border=0 width=19 height=19 / >" + den[128] + "</a>"
        sClase = ""
        switch (proceso.estado) {
            case 1:
                sClase = ""
                break;
            case 2:
                sClase = "OFESINENVIAR"


                break;
            case 3:
            case 5:
                sClase = "OFEENVIADA"

                break;
            case 4:
                sClase = "OFESINGUARDAR"
                break
            default:
                sClase = ""
                break;
        }

        if (document.getElementById("divTotalOferta_" + proceso.grupos[i].cod) && sClase != "") {
            document.getElementById("divTotalOferta_" + proceso.grupos[i].cod).innerHTML = strT
            document.getElementById("divTotalOferta_" + proceso.grupos[i].cod).parentNode.className = sClase
        }
    }
}

 