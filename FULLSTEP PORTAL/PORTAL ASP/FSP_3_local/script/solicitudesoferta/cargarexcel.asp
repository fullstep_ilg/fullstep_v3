﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/fsal_1.asp"-->

<script language="JavaScript" src="../common/ajax.js"></script>
<script src="../common/formatos.js"></script>
<!--#include file="../common/fsal_3.asp"-->

<%
''' <summary>
''' Exporta a excel la oferta
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\excelserver.asp ; Tiempo máximo: 0,2</remarks>


dim oRaiz 

OldTimeOut = server.ScriptTimeout 

server.ScriptTimeout=1800
Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,true,false,0)

CiaComp = clng(oRaiz.Sesion.CiaComp)

anyo = request.Cookies ("PROCE_ANYO")
gmn1 = request.Cookies ("PROCE_GMN1")
proce = request.Cookies ("PROCE_COD")

dim proveCia
proveCia = oRaiz.Sesion.CiaCodGs
if Application("PYME") <> "" then
	sPyme = Application("PYME") 
	If InStr(1, proveCia, Application("PYME")) = 1 Then
		ProveCod = Replace(proveCia, Application("PYME"), "", 1, 1)
	else
		ProveCod = proveCia
	End If
else
	ProveCod = proveCia
end if
CiaProve = oRaiz.Sesion.CiaId

sUsu = oRaiz.Sesion.UsuCod

path=request("path")
SET oXLS = oRaiz.generar_CExcel()
Mensaje="0"
set oError = oXLS.ObtenerDatosDeXLS( ciacomp, anyo, gmn1, proce, ProveCod, path,CiaProve, sUsu,Idioma,sPyme)
if oError.numError<>0 then
	set oXLS = nothing
	set oRaiz = nothing	
	
    'Este error es cuando han cambiado los costes y descuentos, cantidad de Ã­tems, items o grupos y se recarga toda la pÃ¡gina
    if  oError.numError <> 46 then      	
        porFecha=0
        if oError.NumError =51 OR oError.NumError =52 OR oError.NumError= 53 OR oError.NumError= 54 OR oError.NumError= 55 then
            select case oError.NumError
		        case 51
		            porFecha=3
		        case 52
		            porFecha=2
		        case 53
		            porFecha=1
		        case 54
		            porFecha=4
                case 55
                    porFecha=5
	        end select
        end if
  
        server.ScriptTimeout=OldTimeOut%>
        <script>
        top.winEspera.close()	
        window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/errorexcel.asp?PorFecha=<%=porFecha%>","_blank", "top=100,left=150,width=400,height=200,location=no,menubar=no,toolbar=no,resizable=no,addressbar=no")
        </script>
        <%
        Response.End
    else
        Mensaje=oError.Arg1
    end if						
end if
		

set oXLS = nothing


%>

<html>
<title><%=title%></title>
<script type="text/JavaScript"></script>
<script>
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
	{
	if (<%=application("FSAL")%> == 1)
    {
	    Ajax_FSALActualizar3(); //registro de acceso
	}
	top.winEspera.close()	
	window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/solicitudesoferta21.asp?Idioma=<%=Idioma%>&Cia=<%=Server.URLEncode(CiaComp)%>&Anyo=<%=Server.URLEncode(anyo)%>&Gmn1=<%=Server.URLEncode(gmn1)%>&Cod=<%=Server.URLEncode(proce)%>&Mensaje=<%=Server.URLEncode(Mensaje)%>","default_main")	
	}
</script>

<body onload="init()">
</body>
<!--#include file="../common/fsal_2.asp"-->
</html>

<%
server.ScriptTimeout=OldTimeOut
set oRaiz = nothing
 %>