﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->

<script language="javascript" runat="SERVER">
/*
''' <summary>
''' Crear variables javascript a partir de la fechas del servidor
''' Fechas: Servidor /UTC segun reloj local /UTC segun reloj servidor
''' </summary>
''' <param name="pre">Prefijo de las variables</param>
''' <param name="anyo">año de fecha</param>
''' <param name="mes">mes de fecha</param>        
''' <param name="dia">dia de fecha</param>        
''' <param name="hora">hora de fecha</param>        
''' <param name="minuto">minuto de fecha</param>        
''' <param name="utcservidor">Diferencia en minutos entre fecha servidor y fecha utc</param>                
''' <returns>Variables fecha creadas</returns>
''' <remarks>Llamada desde: sistema; Tiempo máximo: 0</remarks>
*/
function scriptFecha(pre, anyo, mes, dia, hora, minuto, utcservidor) {
var miFecha
var tzo
miFecha = new Date(anyo, mes-1, dia, hora, minuto)
tzo = miFecha.getTimezoneOffset()

miFecha = new Date(anyo, mes - 1, dia, hora, (minuto - tzo))

var fechaServidor
var fechaUTC
var fechaCliente

fechaServidor = new Date(miFecha.getFullYear(), miFecha.getMonth(), miFecha.getDate(), miFecha.getHours(), miFecha.getMinutes() + tzo + utcservidor, miFecha.getSeconds())
fechaUTC = new Date(miFecha.getFullYear(), miFecha.getMonth(), miFecha.getDate(), miFecha.getHours(), miFecha.getMinutes() + tzo, miFecha.getSeconds())
fechaCliente = new Date(miFecha.getFullYear(), miFecha.getMonth(), miFecha.getDate(), miFecha.getHours(), miFecha.getMinutes(), miFecha.getSeconds())

str="var " + pre + "vTzoServer \n"
str+="var " + pre + "vFechaServer \n"
str+="var " + pre + "vFechaUTC \n"
str+="var " + pre + "vFechaUTCGMT1 \n"
str+=" " + pre + "vTzoServer = " + tzo + "\n"
str += "  " + pre + "vFechaServer=new Date(" + fechaServidor.getFullYear() + "," + fechaServidor.getMonth() + "," + fechaServidor.getDate() + "," + fechaServidor.getHours() + "," + fechaServidor.getMinutes() + ")\n"
str += "  " + pre + "vFechaUTC=new Date(" + fechaUTC.getFullYear() + "," + fechaUTC.getMonth() + "," + fechaUTC.getDate() + "," + fechaUTC.getHours() + "," + fechaUTC.getMinutes() + ")\n"
str += "  " + pre + "vFechaUTCGMT1=new Date(" + fechaCliente.getFullYear() + "," + fechaCliente.getMonth() + "," + fechaCliente.getDate() + "," + fechaCliente.getHours() + "," + fechaCliente.getMinutes() + ")"

return(str)
}					

					
</script>


<%

''' <summary>
''' Muestra un detalle de las pujas realizadas. Las fechas deben ir en hora local.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cumpoferta.asp; Tiempo máximo: 0</remarks>

decimalfmt = Request.Cookies ("USU_DECIMALFMT")
thousanfmt = Request.Cookies ("USU_THOUSANFMT") 
precisionfmt = CINT(Request.Cookies ("USU_PRECISIONFMT") )
datefmt = Request.Cookies ("USU_DATEFMT") 


Idioma = Request.Cookies("USU_IDIOMA")
Idioma = trim(Idioma)

set oRaiz=validarUsuario(Idioma,true,false,0)
	
utcservidor= oraiz.UTCServidor	
	
CiaComp = clng(oRaiz.Sesion.CiaComp)
CodProve = oRaiz.Sesion.CiaCodGs

anyo = request.Cookies ("PROCE_ANYO")
gmn1 = request.Cookies ("PROCE_GMN1")
proce = request.Cookies ("PROCE_COD") 


''' Cambio desde la versión 31900_3
''' Hay un parámetro que indica si se mostrarán las pujas de Item, Grupo o Proceso

modo = Request("Modo")

select case modo
case "1" ' Modo proceso
case "2" ' Modo grupo
    grupo = Request("idgrupo")
    codgrupo = Request("codgrupo")
case "3" ' Modo ítem
    item = Request("Item")
end select

set oProceso = oRaiz.generar_CProceso()

if  not oProceso.cargarProceso(CiaComp,Idioma,anyo,gmn1,proce) then
    oProceso.anyo = anyo
    oProceso.gmn1cod = gmn1
    oProceso.cod = proce
end if
oProceso.CargarConfiguracion Ciacomp,,,,CodProve
codmon= Request("codmon")
equiv = numero(replace(request("equiv"),".",decimalfmt))

Dim Den
den=devolverTextos(Idioma,57)

dim sTitulo 
dim ador


select case modo
case "1" ' Modo proceso
	set ador = oProceso.DevolverDetallesPujas(CiaComp,CodProve)      
    sTitulo= request("descripcion")
    sTitulo =  den(14) & "<br><br>" & stitulo
case "2" ' Modo grupo    
    set oGrupo = oProceso.Grupos.item(codgrupo)
    set ador = oGrupo.DevolverDetallesPujas(CiaComp,CodProve)      
    sTitulo= request("grupo") & " "  & request("descripcion")
    sTitulo =  den(13) & "<br><br>" & stitulo
case "3" ' Modo ítem
    set oProceso.Grupos = oRaiz.Generar_CGrupos()
    codgrupo = "AUX"
    set oGrupo = oProceso.Grupos.add(oProceso, codgrupo)
    set oGrupo.Items = oRaiz.Generar_CItems()
    'crea un objeto de tipo cItem
    set oItem = oGrupo.Items.add(oProceso,  oGrupo , item )
	set ador = oItem.DevolverDetallesPujas(CiaComp,CodProve)      
    sTitulo= request("articulo") & " "  & request("descripcion")
    sTitulo =  den(1) & "<br><br>" & stitulo
end select




%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><%=title%></title>
<script src="../common/formatos.js"></script>

<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script>

var vdecimalfmt 
vdecimalfmt ="<%=decimalfmt%>"
var vthousanfmt 
vthousanfmt ="<%=thousanfmt%>"
var vprecisionfmt
vprecisionfmt = <%=precisionfmt%>
var vdatefmt
vdatefmt = "<%=datefmt%>"

</script>
<script LANGUAGE=javascript>
function Cerrar() {
	window.close(); 
}

var vdatefmt
vdatefmt = "<%=datefmt%>"

function showPopUp(e, fServer, fUTC, fLocal)
{
str="<%=den(8)%>" + date2str(fServer,vdatefmt,true) + "\n"
str+="<%=den(9)%>" + date2str(fUTC,vdatefmt,true) + "\n"
str+="<%=den(10)%>" + date2str(fLocal,vdatefmt,true) 

e.title=str

}

</script>

</head>
<BODY onload="focus()" >
<center>


<table borcer="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td>
            <img src="images/puja-detalle.png" />
        </td>
        <td>
            <h2><%=sTitulo%></h2>
        </td>
    </tr>
</table>



<h1>

</h1>
	

<%		

	''' Obtiene las pujas para el item



	
	if ador is nothing then	%>
		<h3>	
		<%=Den (7)%>
		</h3>
		
	<%elseif ador.eof then%>
		<h3>	
		<%=Den (7)%>
		</h3>
	<%else%>
	

<TABLE border=1 width="95%" cellspacing=1 cellpadding=3>
	<TR>
		<TD class=cabecera>
			<%=Den (11)%>
		</TD>
		
		<TD class=filaPar>
			<script>
			document.write (num2str(<%=JSNum(ador("PRECIO").value *equiv)%>,vdecimalfmt,vthousanfmt,vprecisionfmt) + " <%=codMon%>")
			</script>
		</TD>
	</TR>
	
<%if oProceso.verproveedor AND oProceso.verProveGanador then%>
	<TR>
		<TD class=cabecera>
			<%=Den (3)%>
		</TD>
		<TD class=filaPar>
			<%=ador("PROVE").value%>
		</TD>
	</TR>
<%end if%>

</TABLE>
<BR>
<TABLE  border=0 width="95%" cellspacing=1 cellpadding=3>

	<TR align=Center>
		<TD class=cabecera>
			<%=Den (4)%>
		</TD>
		
	
<%if oProceso.verproveedor AND oProceso.verProveGanador then%>
		<TD class=cabecera>
			<%= Den (3)%>
		</TD>
<%end if%>	
		<TD  class=cabecera align=center>
			<%=Den (12)%>
		</TD>
	
	</TR>
	
<%
mclass="filaImpar"
WHILE NOT ador.EOF%>
	<TR >
		<script>
		<%=scriptFecha("v" & i, year(ador("FECHA").value), month(ador("FECHA").value), day(ador("FECHA").value),Hour(ador("FECHA").value), minute(ador("FECHA").value),utcservidor)%>
		var v<%=i%>FechaLocal
		v<%=i%>FechaLocal = new Date()
		otz = v<%=i%>FechaLocal.getTimezoneOffset()
		tzoaux=<%=utcservidor%>+otz

		v<%=i%>FechaLocal = new Date(v<%=i%>vFechaServer.getFullYear(), v<%=i%>vFechaServer.getMonth(), v<%=i%>vFechaServer.getDate(), v<%=i%>vFechaServer.getHours(), v<%=i%>vFechaServer.getMinutes() - tzoaux)
		p = date2str(v<%=i%>FechaLocal,vdatefmt,true)
		</script>
		<TD class="<%=mclass%>" align=Center nowrap="">
			<a href="javascript:void(null)" onmouseover="showPopUp(this,v<%=i%>vFechaServer,v<%=i%>vFechaUTC,v<%=i%>FechaLocal)" onclick="alert(this.title)"><script>document.write(p)</script></a><br>			
		</td>
		</TD>
		<%i=i+1%>
<%if oProceso.verproveedor AND oProceso.verProveganador then%>
		<TD  class="<%=mclass%>" align=left>
			<%=ador("PROVE")%>
		</TD>
<%end if%>
		<TD  class="<%=mclass%>" align=right  nowrap="">
			<script>
			document.write (num2str(<%=JSNum(ador("PRECIO").value *equiv)%>,vdecimalfmt,vthousanfmt,vprecisionfmt) + " <%=codMon%>")
			</script>
		</TD>
	</TR>
	<%ador.movenext
	if mclass="filaImpar" then
		mclass="filaPar"
	else
		mclass="filaImpar" 
	end if
wend%>

</TABLE>

<%end if%>
<BR><BR>


<center>
<A ID=Cerrar LANGUAGE=javascript href="javascript:void(null)" onmousemove="window.status='<%=Den(6)%>'" onclick="Cerrar()">
<%=Den(6)%>
</A>
</center>


<%
set oPujas = nothing 
set oItem= nothing

%>
</center>
</BODY></HTML>
