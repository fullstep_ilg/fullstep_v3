﻿
<%@ Language=VBScript %>
<%
oldTimeOut = server.ScriptTimeout
server.ScriptTimeout =40000

 
%>

<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->
<HTML>
<HEAD>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script src="../common/formatos.js"></script>
<script SRC="js/item.asp"></script>
<script SRC="js/auxiliares.asp"></script>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
</HEAD>
<script>

function init()
{

}
</script>



<%
''' <summary>
''' Muestra el resultado de las pujas realizadas.
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\pujacerrada.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
if Idioma="" then
	Idioma = "SPA"
end if

dim den54: den54 = devolvertextos(Idioma,54)
dim den91: den91 = devolvertextos(Idioma,91)
dim den94: den94 = devolvertextos(Idioma,94)

set oRaiz=validarUsuario(Idioma,true,false,0)

decimalfmt = Request.Cookies ("USU_DECIMALFMT")

CiaComp = clng(oRaiz.Sesion.CiaComp)
anyo = request("anyo")
gmn1 = request("gmn1")
proce = request("proce")

utcservidor= oraiz.UTCServidor	





prove = oRaiz.Sesion.CiaCodGs


decimalfmt = Request.Cookies ("USU_DECIMALFMT")
thousanfmt = Request.Cookies ("USU_THOUSANFMT") 
precisionfmt = CINT(Request.Cookies ("USU_PRECISIONFMT") )
datefmt = Request.Cookies ("USU_DATEFMT") 


Set oProceso = oRaiz.Generar_CProceso

oProceso.cargarConfiguracion CiaComp, anyo, gmn1, proce, prove
oProceso.cargarProceso CiaComp, Idioma
set oOferta = oProceso.CargarOfertaProveedor(Idioma,ciacomp,prove)



ofe = oOferta.num

'equiv: Va a contener la equivalencia de la moneda de la oferta respecto a la moneda central. 
'Si moneda central el euro, moneda proceso centimo de euro, moneda oferta centimo de euro -> Equiv = 100



if oProceso.CambiarMon and not oProceso.AdminPublica then
	if trim(oOferta.codmon)<>"" then
		codMon = oOferta.codmon
		denMon = oOferta.monden
		equiv = oProceso.cambio * oOferta.cambio
	else
        set oProve = oraiz.DevolverProveedorEnCiaComp(Idioma, oRaiz.Sesion.CiaId, CiaComp)
        sCodMon = "EUR"
        if not oProve is nothing then
            with oProve
		        if isnull(.CodMon) then
			        set oRS = oRaiz.ObtenerMonedaCentral(ciacomp)
			        if not oRS is nothing then
				        sCodMon = oRS.fields("MON_GS").value
				        oRS.close
				        set oRs = nothing				        
			        end if
		        else
			        sCodMon = .CodMon
		        end if            
            end with
        end if
        set oProve = nothing 

    	set oMonedas = oRaiz.Generar_CMonedas()
		oMonedas.CargarTodasLasMonedasDesde Idioma,CiaComp , 1, sCodMon, , , true, , true

		codMon = sCodMon
		denMon = oMonedas.Item(sCodMon).den
		equiv=Numero(replace(oMonedas.item(sCodMon).equiv,thousanfmt,decimalfmt))
        set oMonedas = nothing
	end if
	
else
	codMon = oProceso.MonCod
	denMon = oProceso.monDen
	if trim(oOferta.codmon)<>"" then
		equiv = oProceso.cambio * oOferta.cambio
	else
		equiv = oProceso.cambio
	end if
end if


mon = codmon


function TextoMultiIdioma(id,idioma)

    
end function



dim bConsulta

bConsulta = true



%>


<BODY onload="Imprimir()" onclick="return window_onclick()">


<table  WIDTH="630" style="margin-left:-3pt" border=0>
	<tr>
		<td>
			<img src="images/puja-resumen.png" />
		</td>
		<td>
            <p class=error>
                <%=den91(2)%><br /><br />

                <div id="DivTextoGanador" style="visibility:hidden">
                    <%=oRaiz.TextoMultiIdioma(ciaComp,oProceso.textofin,Idioma)%>
                </div>
            </p>
		</td>
	</tr>
    <tr>
        <td colspan="2" align="left">
            <p class="error" style="margin-left:0pt">
                <br/><%=den91(26)%>
            </p>
        </td>
    </tr>
</table>













<div id="divApartadoS" >
<div id="divItems" >
</div>
</div>








<%if oProceso.AdminPublica then%>
	<script SRC="js/configadmin.asp"></script>
	<script SRC="js/sobre.asp"></script>
<%else%>
	<script SRC="js/config.asp"></script>
<%end if%>





<script SRC="js/proceso.asp?consulta=<%=bConsulta%>"></script>
<script SRC="js/grupo.asp?consulta=<%=bConsulta%>"></script>
<script SRC="js/item.asp"></script>
<script SRC="js/adjunto.asp"></script>
<script SRC="js/atributo.asp"></script>
<script SRC="js/auxiliares.asp"></script>
<script SRC="js/especificacion.asp"></script>
<script SRC="js/AtributoEspecificacion.asp"></script>
<script SRC="js/costedescgrofert.asp"></script>
<script SRC="js/costedescrecalculo.asp"></script>
<script>




function estadoOferta(codest)
	{
    
	switch(codest)
		{
		case 1:
			return("<%=den54(46)%>")
			break;
		case 2:
			return("<%=den54(78)%>")
			break;
			
		case 3:
			return("<%=den54(79)%>")
			break;
		case 4:
			return("<%=den54(80)%>")
			break;
		case 5:
			return("<nobr><%=den54(79)%>" + "&nbsp;" + "<span class=fpeque><%=den54(81)%></span></nobr>")
			break;
			
		}
}	



var sClase
var sEstado
var vdecimalfmt 
vdecimalfmt ="<%=decimalfmt%>"
var vthousanfmt 
vthousanfmt ="<%=thousanfmt%>"
var vprecisionfmt
vprecisionfmt = <%=precisionfmt%>
var vdatefmt
vdatefmt = "<%=datefmt%>"
var gCodMon="<%=codmon%>"
var gDenMon="<%=denmon%>"
var ofeCambio=<%=oOferta.cambio%>



var proceso 

<%
'En el objeto proceso almacenamos un objeto moneda. En principio tendrá la moneda con la que se ha ofertado y la equivalencia respecto a la moneda central
with oProceso%>

proceso = new Proceso (<%=.anyo%>,'<%=.gmn1Cod%>','<%=.Cod%>','<%=JSText(.Den)%>',<%if .DefDestino=1 then%>new Destino('<%=JSText(.destCod)%>','<%=JSText(.Destino.Den)%>')<%else%>null<%end if%>,'<%=JSText(.pagCod & " - " & .pagDen)%>',<%=JSDate(.FechaInicioSuministro)%>,<%=JSDate(.FechaFinSuministro)%>,'<%=JSText(.esp)%>','<%=JSText(codprove)%>',<%=oOferta.Num%>, <%=JSText(oOferta.EstUltOfe)%>, <%=JSDate(oOferta.FechaHasta)%>,new Moneda('<%=JSText(codMon)%>','<%=JSText(denMon)%>', <%=JSNum(equiv)%>), <%=JSNum(oOferta.Cambio)%>, '<%=JSText(oOferta.obs)%>',<%=JSNum(oOferta.TotEnviadas)%>,<%=JSDate(oOferta.FechaUltOfe)%>,<%=JSNum(oOferta.totalBytes)%>,new CfgProceso(<%=JSBool(.DefDestino=1)%>,<%=JSBool(.DefFormaPago=1)%>,<%=JSBool(.DefFechasSum=1)%>,<%=JSBool(.DefEspecificaciones)%>,<%=JSBool(.SolicitarCantMax)%>,<%=JSBool(.PedirAlterPrecios)%>,<%=JSBool(.DefAdjunOferta)%>,<%=JSBool(.DefAdjunGrupo)%>,<%=JSBool(.DefAdjunItem)%>,<%=JSBool(.Subasta)%>,<%=JSBool(.VerProveedor)%>,<%=JSBool(.VerDetallePujas)%>,<%=JSBool(.cambiarMon)%>,<%=JSBool(.MostrarFinSum)%>,<%=JSBool(.HayCostesDescuentos)%>),<%=JSNum(oOferta.ImporteTotal)%>, "<%=JSText(oOferta.obsAdjuntos)%>", <%=JSNum(oOferta.IdPortal)%>, <%=JSNum(oOferta.posicionPuja)%>,<%=JSNum(oOferta.ImporteBruto)%>,<%=.modo%>)

<%end with%>


<%


	if not oProceso.grupos is nothing then
        
		for each oGrupo in oProceso.Grupos
		with oGrupo
            oGrupo.cargarGrupo Idioma,ciaComp,, anyo, gmn1, proce,prove,ofe,mon,oProceso.SolicitarCantMax
        
		%>
			oCfg = new CfgGrupo (<%=JSBool(oGrupo.DefDestino)%>,<%=JSBool(oGrupo.DefFormaPago)%>,<%=JSBool(oGrupo.DefFechasSum)%>,<%=JSBool(oGrupo.DefEspecificaciones)%>,<%=JSBool(oGrupo.HayAtributos)%>,<%=JSBool(oGrupo.HayCostesDescuentos)%>)
			proceso.anyaGrupo(new Grupo (proceso,'<%=JSText(.Codigo)%>','<%=JSText(.den)%>','<%=JSText(.descripcion)%>',null,'<%=JSText(.pagCod & " - " & .pagDen)%>',<%=JSDate(.FechaInicioSuministro)%>,<%=JSDate(.FechaFinSuministro)%>,'<%=JSText(.Esp)%>',<%=JSNum(.NumItems)%>,oCfg,null,<%=JSNum(.importeTotal)%>,"<%=JSText(oGrupo.ObsAdjuntos)%>",<%=JSNum(.posicionPuja)%>,<%=JSNum(.ID)%>,<%=JSNum(.importeBruto)%>))
		<%
		end with
		next
	end if

%>



proc = proceso

itemsPuja = new Array()

function anyaItemPuja(oItem)
{
var l
l = itemsPuja.length

itemsPuja[l]= oItem

}

var i

wCCodArt = 100
wCDenArt = 180
wCCantidad = 45
wCUnidad = 45
wCPU = 70
wCOfertaMinima = 80
wCPosicionPuja = 60

</script>
<script>

<%if oProceso.verProveedor then%>
    <%if oProceso.verDesdePrimeraPuja = false OR ((oProceso.verDesdePrimeraPuja = true) AND (oProceso.hayOfertaProveedor(CiaComp,Prove)=true)) then %>
	    colspanGrupo = 9
	    wCProveedor = 160
    <%else %>
	    colspanGrupo = 8
	    wCDenArt = 250
	    wCProveedor = 0
    <%end if%>
<%else%>
	colspanGrupo = 8
	wCDenArt = 250
	wCProveedor = 0
<%end if%>
wCTotalLinea = 80
hCabeceraGrupo=16
hCabeceraGrupoDetalle=16
</script>

<script>

hCFijo=12 //Alto de cada fila
wDesviacion = 4 // Cada columna es cuatro puntos más ancha que la wC correspondiente...
hDesviacion = 2
wBorde = 2 //Ancho de las separaciones de las columnas
wCSeparacion = 10 //Ancho de las columnas de separación

function prepararDivsPuja()
{


hAptdo = document.getElementById("divApartadoS").style.height
hCabecera = hCFijo + wDesviacion

if (!(document.getElementById("divPuja")))
{

proceso.wApArticulo = wCCodArt + wCDenArt + wCProveedor + wCOfertaMinima + wCPosicionPuja + wCPU + wCTotalLinea +  colspanGrupo * (wDesviacion + wBorde)
<%
'
'divArticuloC        
'+-----------------+ 
'¦                 ¦ 
'¦                 ¦ 
'¦                 ¦ 
'¦                 ¦ 
'¦                 ¦ 
'¦                 ¦ 
'¦                 ¦ 
'+-----------------+ 
'divArticuloScroll   
'+-----------------+ 
'¦divArticulo      ¦ 
'¦+---------------+¦ 
'¦¦               ¦¦ 
'¦¦               ¦¦ 
'¦¦               ¦¦ 
'¦¦               ¦¦ 
'¦¦               ¦¦ 
'¦¦               ¦¦ 
'¦¦               ¦¦ 
'¦+---------------+¦ 
'+-----------------+ 
                   
%>
strDivs  = ""
strDivs += "<div name=divPuja id=divPuja style='visibility:visible;BORDER:<%=cBORDEBLOQUE%> 1px solid ;position:absolute; top:160;left:5'>"
strDivs += "<table name=tblSubasta id=tblSubasta width='630'  border=0 >"

if (proceso.estado>1)
	{
	strDivs += "	<tr height=16 >"
	switch (proceso.estado)
		{
		case 1:
			break;
		case 2:
			sClase = "OFESINENVIAR"
			break;
		case 3:
		case 5:
			sClase = "OFEENVIADA"
			break;
		case 4:
			sClase = "OFESINGUARDAR"
			break
		}
		

	strDivs += "<td class=" + sClase +">"
	strDivs += "<div name=divTotalOfertaSubasta id=divTotalOfertaSubasta style='overflow:hidden;' >" 
	sEstado = "<%=JSText(den91(27))%>"
	strDivs += sEstado + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  num2str(proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;" + proceso.mon.cod 
	strDivs += "</div>"
	strDivs += "</td>"
	strDivs += "</tr>"
	}

strDivs += "	</tr>"
strDivs += "</table>"

strDivs+= "	<table width=100% border=0>"
strDivs+= "		<tr>"
strDivs+= "			<td width=35% ><div name=divCronometroPuja id=divCronometroPuja></div>" 
strDivs+= "			</td>"
strDivs+= "		</tr>" 
strDivs+= " </table>"

strDivs += "<div name=divApartadoSPuja id=divApartadoSPuja>"

		strDivs += "<DIV NAME=divArticuloPujaC id=divArticuloPujaC ></div>"

		strDivs += "<DIV NAME=divArticuloPujaScroll id=divArticuloPujaScroll onScroll='scrollFijasPuja()' >"


		for (i=0;i<proceso.grupos.length;i++)
			{
			if (i==0)
				vTop=0
			else
				vTop+=proceso.grupos[i-1].numItems*(hCFijo + wBorde + hDesviacion) + hCabeceraGrupo + hCabeceraGrupoDetalle + wBorde + hDesviacion + 10

            if (!document.getElementById("divArticuloPuja" + proceso.grupos[i].cod)) strDivs += "<DIV NAME=divArticuloPuja" + proceso.grupos[i].cod + " id=divArticuloPuja" + proceso.grupos[i].cod + " ></div>"
			}

		strDivs += "</div>"
	strDivs += "</div>"
strDivs += "</div>"
document.getElementById("divItems").innerHTML +=strDivs





}

}


function prepararCabeceraPuja(codMonedaGrupo,codgrupo)
{

var strCabArticulo

strCabArticulo = ""
strCabArticulo += "		<TD class='cabecera'>"
strCabArticulo += "			<div style='width:" + wCCodArt + ";height:" + hCFijo + ";overflow:hidden;'><%=JSText(den94(1))%></div>"
strCabArticulo += "		</TD>"
strCabArticulo += "		<TD class='cabecera'>"
strCabArticulo += "			<div style='width:" + wCDenArt + ";height:" + hCFijo + ";overflow:hidden;'><%=JSText(den94(2))%></div>"
strCabArticulo += "		</TD>"

strCabArticulo += "		<TD class='cabecera'>"
strCabArticulo += "			<div style='width:" + wCCantidad + ";height:" + hCFijo + ";overflow:hidden;'><nobr><%=JSText(den91(28))%></nobr></div>"
strCabArticulo += "		</TD>"
strCabArticulo += "		<TD class='cabecera'>"
strCabArticulo += "			<div style='width:" + wCUnidad + ";height:" + hCFijo + ";overflow:hidden;'><nobr><%=JSText(den91(29))%></nobr></div>"
strCabArticulo += "		</TD>"


    if (proceso.modo==3)
    {
       strCabArticulo += "		<TD class='POSICIONPUJAITEM'>"
    }
    else
    {
        strCabArticulo += "		<TD class='cabecera'>"
    }

strCabArticulo += "			<div style='width:" + wCPosicionPuja + ";height:" + hCFijo + ";overflow:hidden;'><nobr><%=JSText(den91(30))%></nobr></div>"
strCabArticulo += "		</TD>"

strCabArticulo += "		<TD class='cabecera' align=right>"
strCabArticulo += "			<div style='width:" + wCPU + ";height:" + hCFijo + ";overflow:hidden;'><table border=0 cellspacing=0 cellpadding=0 width=100% ><tr><td class='cabecera'><nobr><%=JSText(den94(3))%></nobr></td><td align=left>"

strCabArticulo += "			</td></tr></table></div>"
strCabArticulo += "		</TD>"
strCabArticulo += "		<TD class='cabecera' textAlign='rigth'>"
strCabArticulo += "			<div style='width:" + wCTotalLinea + ";height:" + hCFijo + ";overflow:hidden;'><table heigth='100%' cellspacing='0' cellpading='0'><tr><td class='cabecera' vAlign='top'><%=JSText(den94(22))%></td><td class='cabecera' vAlign='bottom'>" + codMonedaGrupo + "</td></tr></table></div>"
strCabArticulo += "		</TD>"

strArticuloC="<TABLE cellspacing='2' cellpading='2' width='100%' ><TR>" + strCabArticulo + "</TR></TABLE>"

return strArticuloC
}



<%

oProceso.cargarItems CiaComp,mon,prove,ofe,Idioma
oProceso.oferta.cargarPrecios CiaComp



                    Dim MostrarMensajeGanador
                    MostrarMensajeGanador = false
                select case  oproceso.modo
                case 1  'Modo Proceso
                    if  oProceso.cargarPosicionProceso(CiaComp, prove) = 1 then
                        MostrarMensajeGanador = true
                    end if                
                case 2 ' Modo grupo
                    for each oGrupo in oProceso.grupos                         
                        if split(oGrupo.proveGanador(CiaComp,true ),"||")(0) = prove then
                            MostrarMensajeGanador = true
                        end if
                    next
                case 3 ' Modo items
                    for each oItem in oProceso.items                         
                        if oItem.minProveCod = prove then
                            MostrarMensajeGanador = true
                        end if
                    next
                end select

                if MostrarMensajeGanador then
                %>
                        document.getElementById("DivTextoGanador").style.visibility="visible"
                <%
                end if









%>
</script>
<%'response.write "<p>Items cargados:" & now() & "</P>"%>
<script>




function insertarCelda (TR,sClase,sHTML,vAlign, iRowSpan, tAlign)
	{
    
	var oTD
	
	oTD = TR.insertCell(-1)
	oTD.className=sClase

	if (vAlign)
		oTD.style.verticalAlign=vAlign

	if (tAlign)
		oTD.style.textAlign=tAlign

	

	if (iRowSpan)
		oTD.rowSpan = iRowSpan

	oTD.innerHTML+=sHTML

	}



function contador(grupo)
{
var j

for (j=0;j<proceso.grupos.length && proceso.grupos[j].cod!=grupo;j++)
	{
	
	}
proceso.grupos[j].cont++
return (proceso.grupos[j].cont)
}




prepararDivsPuja()

//prepararCabeceraPuja()




var strDetArticulo




strDetArticulo = ""

var x

var vGrp
fila='filaImpar'

for (i=0;i<proceso.grupos.length;i++)
	{
	vGrp = proceso.grupos[i]
	strTablaGrupo = "<TABLE name='tblArticuloPuja" + proceso.grupos[i].cod + "' id='tblArticuloPuja" + proceso.grupos[i].cod + "' border=0 width=100%  >"
	strTablaGrupo += "<tr>"
	strTablaGrupo += "<td class=cabecera colspan=" + colspanGrupo + " width='100%'>"
	strTablaGrupo += "			<div name=divCabeceraPreciosSubasta_" + vGrp.cod + " id=divCabeceraPreciosSubasta_" + vGrp.cod + " style='width:100%';height:" + hCabeceraGrupo + ";overflow:visible;'>"
	strTablaGrupo += "<a class=ayuda href='javascript:void(null)' onfocus='return OcultarDivCostesDescItem()' onclick='ocultarPujaGrupo(\"" + proceso.grupos[i].cod + "\")' title = '" + proceso.grupos[i].den + "'>"
	
	strTablaGrupo += proceso.grupos[i].cod + "&nbsp;&nbsp;<%=den94(13)%> " + num2str(vGrp.importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;<%=mon%>"

	strTablaGrupo += "</a>"
	strTablaGrupo += "</div>"
	strTablaGrupo += "</td>"
	strTablaGrupo += "</tr>"
	
	//Nueva Cabecera Detalle (Codigo, denominacion...
	strTablaGrupo += "<tr>"
	strTablaGrupo += "<td class=cabecera colspan=" + colspanGrupo + " width='100%'>"
	strTablaGrupo += "<DIV NAME=divArticuloPujaC_" + vGrp.cod + " id=divArticuloPujaC_" + vGrp.cod + " style='width:'100%';height:" + hCabeceraGrupoDetalle + ";overflow:hidden;'>"
	
	strTablaGrupo += prepararCabeceraPuja ('<%=mon%>', proceso.grupos[i].cod)
	strTablaGrupo += "</div>"
	strTablaGrupo += "</td>"
	strTablaGrupo += "</tr>"
	
	strTablaGrupo += "</TABLE>"
	document.getElementById("divArticuloPuja" + proceso.grupos[i].cod).innerHTML +=strTablaGrupo
	}

j=0


/*
''' <summary>
''' Mostrar por pantalla la caja de precio unitario para el item
''' </summary>
''' <param name="sName">Nombre del elelmento</param>
''' <param name="lSize">Tamaño</param>
''' <param name="lLenMax">Longitud máxima</param>
''' <param name="dValue">valor</param>
''' <param name="valorMin">valor mínimo</param>
''' <param name="valorMax">valor máximo</param>
''' <param name="mas"></param>
''' <param name="sFuncValidacion">Función de validación</param>
''' <param name="sFuncFoco">Función de Foco</param>
''' <param name="errMsg"></param>
''' <param name="errLimites"></param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cargaritems.asp; Tiempo máximo: 0</remarks>
config.asp
''' <autor>JVS</autor>
*/
function inputNumeroItem (sName, lSize, lLenMax, dValue, valorMin, valorMax, mas, sFuncValidacion, sFuncFoco, errMsg, errLimites)
{

var s
if (!sFuncValidacion)
 sFuncValidacion = "validarNumero("
s = "<input " + (mas?mas:"") + " language='javascript' class='numero' style='width:" + lSize + "' type=text name=" + sName + " id =" + sName + " maxlength=" + lLenMax + " onkeypress='return mascaraNumero(this,vdecimalfmt,vthousanfmt,event);' onchange='return " + sFuncValidacion + "this,vdecimalfmt,vthousanfmt,100," + valorMin + "," + valorMax + ",\"" + errMsg + "\",\"" + errLimites + "\")' onfocus='return " + sFuncFoco + ")' value='" + num2str(dValue,vdecimalfmt,vthousanfmt,100) + "' >"
return s
}


/*
''' <summary>
''' funcion que dibuja cada item de la subasta
''' </summary>
''' <param name="vGrupo">Grupo del proceso</param>
''' <param name="vIDItem">Id del item</param>  
''' <param name="vArt">Articulo</param>  
''' <param name="vDescr">Descripcion del articulo</param>  
''' <param name="vCant">Cantidad del item</param>
''' <param name="vUni">Unidad del articulo</param>
''' <param name="vUniDen">Denominacion de la unidad del articulo</param>
''' <param name="vPrec">Precio</param>                
''' <param name="vMin">Valor minimo</param>    
''' <param name="vPrec">Precio</param>
''' <param name="vProve">Proveedor</param>                
''' <param name="vProveDen">Denominacion del proveedor</param>                
''' <param name="vFilaS">Estilo de la fila</param>
''' <param name="vPosicionPuja">Posicion de la puja</param>                                                                
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cargaritems,cargarsubasta; Tiempo máximo:0</remarks>
*/ 

function dibujarItem(vGrupo, vIDItem, vArt, vDescr, vCant, vUni, vUniDen, vPrec, vMin, vProve, vProveDen, vFilaS,vPosicionPuja,importeNeto,precioNeto)

{
	
	oItem = new Item (vGrupo, vIDItem,vArt,vDescr,vCant,new Unidad(vUni,vUniDen),null,null,null,null,null,null,null,null,vPrec,null,null,null,null,vMin,vProve,vProveDen)	
	
	oTR = document.getElementById("tblArticuloPuja" + vGrupo).insertRow(-1)

	strDetArticulo = "			<input type=hidden name=txtPujaIDItem_" + vGrupo + "_" + contador(vGrupo) + " value='" + vIDItem + "'>"
	strDetArticulo += "			<input type=hidden name=txtPujaClaseFila_" + vIDItem + "  value='" +  fila + "'>"
	
	strDetArticulo += "			<div style='width:" + wCCodArt + ";height:" + hCFijo + ";overflow:hidden;'>"
	strDetArticulo += oItem.cod
	strDetArticulo += "			</div>"

	insertarCelda(oTR,fila,strDetArticulo)


	strDetArticulo = "			<div style='width:" + wCDenArt + ";height:" + hCFijo + ";overflow:hidden;'>"
	strDetArticulo += oItem.descr
	strDetArticulo += "			</div>"
	
	oTD = oTR.insertCell(-1)
	oTD.className = fila
    oTD.title = oItem.descr
	oTD.innerHTML +=strDetArticulo

	strDetArticulo = "			<div style='width:" + wCCantidad + ";height:" + hCFijo + ";overflow:hidden;'>"
	strDetArticulo += num2str(oItem.cant,vdecimalfmt,vthousanfmt,vprecisionfmt)
	strDetArticulo += "			<input type=hidden name=txtPujaCantidad_" + vGrupo + "_" + oItem.id + " value='" + num2str(oItem.cant,vdecimalfmt,vthousanfmt,vprecisionfmt) + "'>"
	strDetArticulo += "			</div>"
	oTD = oTR.insertCell(-1)
	oTD.className=fila
	oTD.innerHTML +=strDetArticulo


	strDetArticulo = "			<div style='width:" + wCUnidad + ";height:" + hCFijo + ";overflow:hidden;'>"
	strDetArticulo += "<table width=100% style='margin-top:-3px'><tr><td width=100%><a class=popUp href='javascript:void(null)' title='" + oItem.uni.den + "' onfocus='return OcultarDivCostesDescItem()' onclick='DenominacionUni(\"" + oItem.uni.cod + "\")'>" +  oItem.uni.cod +  "</A></td>"
	//strDetArticulo += "<td><a class=popUp href='javascript:void(null)' title='" + oItem.uni.den + "' onfocus='return OcultarDivCostesDescItem()' onclick='DenominacionUni(\"" + oItem.uni.cod + "\")'><IMG border=0 SRC='../images/masinform.gif'></a></td>"
    strDetArticulo += "</tr></table>"		
	strDetArticulo += "			</div>"

	oTD = oTR.insertCell(-1)
	oTD.className=fila
	oTD.innerHTML +=strDetArticulo

	    //Posicion de la puja
	    strDetArticulo = "<div class='" + vFilaS + "' name=divPujaPosicion" + vIDItem + " id=divPujaPosicion" + vIDItem + " style='width:" + wCPosicionPuja  + ";height:" + hCFijo + ";overflow:hidden;align:center'>"
	    strDetArticulo +="<b>"
	    if (vPosicionPuja != null)
        {
    	        strDetArticulo += vPosicionPuja
        }
	    strDetArticulo += "</b></div>"	


	    oTD = oTR.insertCell(-1)
	    oTD.name = 'tdPujaPosicion_' + vIDItem
	    oTD.id = 'tdPujaPosicion_' + vIDItem
	    oTD.style.textAlign='center'
	    oTD.style.verticalAlign='middle'
	    oTD.className=fila
	    oTD.innerHTML += strDetArticulo


	strDetArticulo = "			<div style='width:" + wCPU + ";height:" + hCFijo + ";overflow:hidden;z-index:1000;'>"
		strDetArticulo += "<b>" + num2str(precioNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b"
	strDetArticulo += "			</div>"

	insertarCelda(oTR,fila,strDetArticulo,"","","right")
	strDetOferta = "			<div name=divTotalLinea4_" + oItem.id + " id=divTotalLinea4_" + oItem.id + " style='width:" + wCTotalLinea + ";height:" + hCFijo + ";overflow:hidden;z-index:1000;'>"

	if (oItem.precio!=null)
		{
//		strDetOferta += num2str(oItem.cant,vdecimalfmt,vthousanfmt,vprecisionfmt) 
//		strDetOferta += "&nbsp;<a class=popUp href='javascript:void(null)' title='" + oItem.uni.den + "'>" +  oItem.uni.cod +  "</A>"
//		strDetOferta += "<br>"
//		strDetOferta += "<b>" + num2str(oItem.precio * oItem.cant,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
		strDetOferta += "<b>" + num2str(importeNeto,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</b>"
		}
	strDetOferta += "			</div>"

	insertarCelda(oTR,fila + " numero",strDetOferta)
	
	if (fila=="filaImpar")
		fila="filaPar"
	else
		fila="filaImpar"
	j++
	anyaItemPuja(oItem)
}



<%
cont = 0
for each oItem in oProceso.items

	if cont=10 then
		%>
		</script>
		<script>
		<%
		cont = 0
	else
		cont = cont + 1
	end if
	%>


	filaS = ""


	//dibujarItem("<%=oItem.grupo.codigo%>", <%=oItem.id%>,"<%=JSText(oItem.ArticuloCod)%>", "<%=JSText(oItem.Descr)%>", <%=JSNum(oItem.cantidad)%>,"<%=JSText(oItem.uniCod)%>", "<%=JSText(oItem.uniDen)%>", <%=JSNum(oItem.precioItem.PrecioOferta / oOferta.cambio * equiv)%>, <%=JSNum(oItem.minprecio / oOferta.cambio * equiv)%>, <%if oProceso.verProveedor then%>"<%=JSText(oItem.MinProveCod)%>","<%=JSText(oItem.MinProveDen)%>"<%else%>null,null<%end if%>, filaS,<%=JSNum(oItem.PosicionPuja)%>, <%=JSNum(oItem.PrecioItem.PrecioNeto)%>, <%=JSNum(oItem.PrecioItem.ImporteBruto / oOferta.cambio * equiv)%>, <%=JSNum(oItem.PrecioItem.ImporteNeto / oOferta.cambio * equiv)%>)
	<%

 next


oProceso.CargarCostesDescuentos CiaComp

if not oProceso.grupos is nothing then

    for each oGrupo in oProceso.grupos
        oGrupo.cargarGrupo Idioma,ciaComp,, anyo, gmn1, proce,prove,ofe,mon,oProceso.SolicitarCantMax
        oGrupo.cargarItems Idioma,CiaComp,mon,prove,ofe,false
        oGrupo.cargarAtributos CiaComp,3
    
        set oOfeGrupo = oRaiz.Generar_COfertaGrupo()
        set oOfeGrupo.Oferta = oOferta
        set oOfeGrupo.Grupo = oGrupo
        oOfeGrupo.cargarPrecios ciacomp,mon
        %>

    
           for (i=0;i<proceso.grupos.length && proceso.grupos[i].cod != '<%=oGrupo.Codigo%>';i++)
		        {
		        }
	    oObj = proceso.grupos[i]

        <%

        cont = 0
        for each oItem in oGrupo.items



            %>
            dibujarItem("<%=oItem.grupo.codigo%>", <%=oItem.id%>,"<%=JSText(oItem.ArticuloCod)%>", "<%=JSText(oItem.Descr)%>", <%=JSNum(oItem.cantidad)%>,"<%=JSText(oItem.uniCod)%>", "<%=JSText(oItem.uniDen)%>", <%=JSNum(oItem.precioItem.PrecioOferta / oOferta.cambio * equiv)%>, <%=JSNum(oItem.minprecio / oOferta.cambio * equiv)%>, <%if oProceso.verDesdePrimeraPuja = false OR ((oProceso.verDesdePrimeraPuja = true) AND (oProceso.hayOfertaProveedor(CiaComp,Prove)=true)) then %><%if oProceso.verProveedor then%>"<%=JSText(oItem.MinProveCod)%>","<%=JSText(oItem.MinProveDen)%>"<%else%>null,null<%end if%><%else%>null,null<%end if%>, filaS,<%=JSNum(oItem.PosicionPuja)%>, <%=JSNum(oItem.PrecioItem.ImporteNeto / oOferta.cambio * equiv)%>, <%=JSNum(oItem.PrecioItem.PrecioNeto)%>)
            for (i=0;i<itemsPuja.length && itemsPuja[i].id != '<%=oItem.id%>';i++)
            {
            }
            vItem = itemsPuja[i]
                   <%
            'JVS carga de los items
       	    if cont=10 then
		        %>
		        </script>
		        <script>
		        <%
		        cont = 0
	        else
		        cont = cont + 1
	        end if
	        %>


	        filaS = ""


            vItem.precioNeto = <%=JSNum(oItem.PrecioItem.PrecioNeto)%>
            vItem.importeBruto = <%=JSNum(oItem.PrecioItem.ImporteBruto / oOferta.cambio * equiv)%>
            vItem.importeNeto = <%=JSNum(oItem.PrecioItem.ImporteNeto / oOferta.cambio * equiv)%>

            vItem.costesDescuentosOfertados.length = 0

	        <% 
        
            'JVS carga de los valores de los costes / descuentos de ámbito item
             if not (oGrupo.CostesDescItem is nothing) then
                for each  oCosDes in oGrupo.CostesDescItem
                with oCosDes
                     set oAtribOfe = oItem.PrecioItem.CostesDescOfertados
                    if .Ambito=3 then%>
                         costedescvalor = new Atributo(<%=.id%>, <%=.paid%>, '<%=JSText(.cod)%>', '<%=JSText(.den)%>', <%=.intro%>, <%=.tipo%>,<%
                        if not oAtribOfe is nothing then
                            if not oAtribOfe.Item(cstr(.PAID)) is nothing then
                                if (.operacion = "+" or  .operacion = "-") then
        	                        vValor = oAtribOfe.Item(cstr(.PAID)).valor / oOferta.cambio * equiv
                                else
	                                vValor = oAtribOfe.Item(cstr(.PAID)).valor 
                                end if
                                vImporte = oAtribOfe.Item(cstr(.PAID)).ImporteParcial
                            else
                                vValor = null
                                vImporte = null
                            end if
                        else
                            vValor = null
                            vImporte = null
                        end if
                        response.write  JSNum(vValor) 
                        response.write  "," + JSBool(.obligatorio)
                        response.write "," & JSNum(.valorMin) &  "," & JSNum(.valorMax) 
                        if .intro = 1 then
        	                str=",["
	                        for each oValor in .Valores
    		                    if str<>",[" then
        			                str = str & ","
            	                end if
                                if (.operacion = "+" or  .operacion = "-") then
			                        vValor = oValor.Valor / oProceso.Cambio * equiv
        		                else
		        	                vValor = oValor.Valor 
                                end if
                                str = str & JSNum(vValor)
                            next
                            response.write str & "],proceso,'" & JSText(.operacion) & "'," & JSNum(.aplicar) & ", null"
                            response.write ", " &  JSNum(vImporte) & ")"
	                    else
    		                response.write ",null,proceso,'" & JSText(.operacion) & "',"  & JSNum(.aplicar) & ", null"
                            response.write ", " &  JSNum(vImporte) & ")"
            	        end if
                        %>                    
    	                vItem.anyaCosteDescuentoOfertado(costedescvalor)
                        <%
                    end if
                end with
                next
            end if
        next
    next
end if


%>




<%if oProceso.modo = 1  then 'Modo Proceso %>
MostrarGanadorProceso('<%=oProceso.proveGanador(CiaComp)%>',<%=oProceso.importeGanador(CiaComp)%>)
<%end if %>






proceso.subastaCargada=true
MostrarPosicionPuja()
//recalcularTotales()


bGuardando = false

     if (document.getElementById("Ganadora")) document.getElementById("Ganadora").style.display = "block"

function window_onclick() {

}


function MostrarGanadorProceso (prove,precio) 
{
// Actualiza el proveedor y/o la oferta en modo proceso
// Establece los datos en os DIVs correspondientes según las condiciones de visualización
    var strDivs = ""

    <%if oProceso.modo = 1  then 'Modo Proceso %>
        <% if oProceso.verproveganador or oProceso.verprecioganador  then %>
            <%if oProceso.verDesdePrimeraPuja = false OR ((oProceso.verDesdePrimeraPuja = true) AND (oProceso.hayOfertaProveedor(CiaComp,CodProve)=true)) then %>                       
                if ((prove != "") && (precio != 0 ))
                {
                    <% if oProceso.verDetallePujas AND oProceso.verprecioganador then%>
    			        strDivs += "<div style='cursor:pointer' modo='1' sDescr='" + proceso.den + "' onclick='verDetallePujas(this)'>"
                    <%end if%>

                    strDivs += "("
                    <% if oProceso.verproveganador then %>
                            if (prove != "") strDivs += "<%=den54(121)%> " + prove
                    <%end if%>
                    <% if oProceso.verproveganador and oProceso.verprecioganador  then %>
                        strDivs += "&nbsp;&nbsp;"
                    <%end if%>
                    <% if oProceso.verprecioganador then %>
                        <% if oProceso.verproveganador then  %>
                            if (precio != 0) strDivs += "<%=den54(58)%> " + (precio * ofeCambio) + " " + gCodMon
                        <%else %>
                            if (precio != 0) strDivs += "<%=den54(116)%> " + (precio * ofeCambio) + " " + gCodMon
                        <%end if%>
                    <%end if%>
                    strDivs += ")"
                    <% if oProceso.verDetallePujas AND oProceso.verprecioganador then %>
    			        strDivs += "</div>"
                    <%end if%>

                }
            <%end if%>
        <%end if%>
    <%end if%>

    if (document.getElementById("DivGanadorProceso")) {
	    document.getElementById("DivGanadorProceso").innerHTML = strDivs
    }
}





function MostrarPosicionPuja()
{

MostrarCabeceraOferta()


for (i=0;i<proceso.grupos.length;i++)
       {
       if (document.getElementById("divCabeceraPreciosSubasta_" + proceso.grupos[i].cod))
              {

              //Primera carga del grupo
              strT ="<table width='100%'  height='"+ (hCabeceraGrupo+5) +"' border=0><tr>"
              strT += "<td class='cabecera' width='25%'>"
                           
              strT += "<a class=ayuda href='javascript:void(null)' onclick='ocultarPujaGrupo(\"" + proceso.grupos[i].cod + "\")' title = '" + proceso.grupos[i].den + "'>"
              strT += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>"
              strT += "<%=den54(57) %> " + proceso.grupos[i].cod  + " - " + proceso.grupos[i].den + "</td>"              
                           
              strT +="<td class='cabecera' width='25%'><%=den54(86) %>:&nbsp;" + num2str(proceso.grupos[i].importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;" + proceso.mon.cod + "</td>"
                     
              //posicion Puja GRUPO Primera Carga
            
              
            if (proceso.grupos[i].posicionPuja > 0) {
                     strT += "<td width='20%'>"
                     strT += "<div name=divPosicionPujaGrupo_"+proceso.grupos[i].cod + " id=divPosicionPujaGrupo_"+proceso.grupos[i].cod + ">"
              <% if oProceso.modo = 2 then %>
                         strT += "<table cellpadding='0' cellspacing='0' width='100%' height='20' class='POSICIONPUJAGRUPO'><tr>"
               <%else %>                     
                    strT += "<table cellpadding='0' cellspacing='0' width='100%' height='20' ><tr>"
               <%end if %>
                     strT += "<td align=center class='cabecera'>"
                         strT+="<%=den54(84) %>:" 
                         strT+="<b>"   
                         strT+=proceso.grupos[i].posicionPuja 
                         strT+="</b>"  
                     strT+= "</td></tr></table>" 
                     strT+= "</div>"      
                     
              //strT += "<td width='20%'>&nbsp;"
              strT += "</td>"
            }
              strT += "</tr></table>"
              document.getElementById("divCabeceraPreciosSubasta_" + proceso.grupos[i].cod).innerHTML  = strT
              }
       }
}

function MostrarCabeceraOferta()
{

    if (proceso.estado > 1)
    {

	    if (!document.getElementById("divTotalOfertaSubasta") && document.getElementById("tblSubasta"))
				    {
	    oTR = document.getElementById("tblSubasta").insertRow(0)
	    oTR.height= 15
	    oTD = oTR.insertCell(-1)
	    ha=52
	    strDivs = "<div name=divTotalOfertaSubasta id=divTotalOfertaSubasta style='overflow:hidden;'></div>" 
	    oTD.innerHTML +=strDivs
	    document.getElementById("divApartadoSPuja").style.top = 50

	    resize()
	
	    }
    }



       strT ="<table cellpadding='0' cellspacing='0' border=0>"
       strT +="<tr>"
       strT +="<td class='" +sClase +"'>"
       strT += sEstado + "&nbsp;&nbsp;" +  num2str(proceso.importe,vdecimalfmt,vthousanfmt,vprecisionfmt) + "&nbsp;" + proceso.mon.cod 
       
       strT += "</td>"
       strT += "<td  class='" +sClase +"'>&nbsp;&nbsp;&nbsp;&nbsp;</td>"
       if ( proceso.posicionPuja > 0 ){
              //Posicion Puja PROCESO
              strT += "<td >"
              if (proceso.modo == 1)
              {              
                strT += "<div id='MostrarPosicionPujaProceso' class='POSICIONPUJAPROCESO' "
              }
              else
              {
                document.getElementById("divTotalOfertaSubasta").style.visibility = "visible"  
                strT += "<div id='MostrarPosicionPujaProceso' "
              }
              strT += " style='visibility:visible'>"
              strT += "<table border=0 cellspacing=0 cellpadding=0><tr>"
              strT += "<td class='" +sClase +"' >&nbsp;&nbsp;&nbsp;&nbsp; <%=den54(84) %>:</td>"
              strT += "<td class='" +sClase +"' >"
              strT += "<div name=divPosicionPujaProceso id=divPosicionPujaProceso>"
              strT +="<b>&nbsp;" + proceso.posicionPuja  +"</b>"
              strT +="</div>"
              strT +="</td><td  class='" +sClase +"'>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>"
              strT +="</div>"
              strT +="</td>"
            }
        else {
              
              //Posicion Puja PROCESO
              strT += "<td class='" +sClase +"'>"
              strT += "<div name=divPosicionPujaProceso id=divPosicionPujaProceso>"              
              strT +="</div>"
              strT +="</td>"
        }


              strT += "</tr></table>"
    
       if (document.getElementById("divTotalOfertaSubasta"))
              {
              document.getElementById("divTotalOfertaSubasta").innerHTML  = strT
              document.getElementById("divTotalOfertaSubasta").parentNode.className=sClase
              }


}






            </script>

<script language="javascript" runat="SERVER">
    /*
    ''' <summary>
    ''' Crear variables javascript a partir de la fechas del servidor
    ''' Fechas: Servidor /UTC segun reloj local /UTC segun reloj servidor
    ''' </summary>
    ''' <param name="pre">Prefijo de las variables</param>
    ''' <param name="anyo">año de fecha</param>
    ''' <param name="mes">mes de fecha</param>        
    ''' <param name="dia">dia de fecha</param>        
    ''' <param name="hora">hora de fecha</param>        
    ''' <param name="minuto">minuto de fecha</param>        
    ''' <param name="utcservidor">Diferencia en minutos entre fecha servidor y fecha utc</param>                
    ''' <returns>Variables fecha creadas</returns>
    ''' <remarks>Llamada desde: sistema; Tiempo máximo: 0</remarks>
    */
    function scriptFecha(pre, anyo, mes, dia, hora, minuto, utcservidor) {
        var miFecha
        var tzo
        miFecha = new Date(anyo, mes - 1, dia, hora, minuto)
        tzo = miFecha.getTimezoneOffset()

        miFecha = new Date(anyo, mes - 1, dia, hora, (minuto - tzo))

        var fechaServidor
        var fechaUTC
        var fechaCliente

        fechaServidor = new Date(miFecha.getFullYear(), miFecha.getMonth(), miFecha.getDate(), miFecha.getHours(), miFecha.getMinutes() + tzo + utcservidor, miFecha.getSeconds())
        fechaUTC = new Date(miFecha.getFullYear(), miFecha.getMonth(), miFecha.getDate(), miFecha.getHours(), miFecha.getMinutes() + tzo, miFecha.getSeconds())
        fechaCliente = new Date(miFecha.getFullYear(), miFecha.getMonth(), miFecha.getDate(), miFecha.getHours(), miFecha.getMinutes(), miFecha.getSeconds())

        str = "var " + pre + "vTzoServer \n"
        str += "var " + pre + "vFechaServer \n"
        str += "var " + pre + "vFechaUTC \n"
        str += "var " + pre + "vFechaUTCGMT1 \n"
        str += " " + pre + "vTzoServer = " + tzo + "\n"
        str += "  " + pre + "vFechaServer=new Date(" + fechaServidor.getFullYear() + "," + fechaServidor.getMonth() + "," + fechaServidor.getDate() + "," + fechaServidor.getHours() + "," + fechaServidor.getMinutes() + ")\n"
        str += "  " + pre + "vFechaUTC=new Date(" + fechaUTC.getFullYear() + "," + fechaUTC.getMonth() + "," + fechaUTC.getDate() + "," + fechaUTC.getHours() + "," + fechaUTC.getMinutes() + ")\n"
        str += "  " + pre + "vFechaUTCGMT1=new Date(" + fechaCliente.getFullYear() + "," + fechaCliente.getMonth() + "," + fechaCliente.getDate() + "," + fechaCliente.getHours() + "," + fechaCliente.getMinutes() + ")"

        return (str)
    }					


					
					
</script>








<script>

    var vdatefmt
    vdatefmt = "<%=datefmt%>"
    function showPopUp(e, fServer, fUTC, fLocal) {
        str = "<%=den91(4)%> " + date2str(fServer, vdatefmt, true) + "\n"
        str += "<%=den91(5)%> " + date2str(fUTC, vdatefmt, true) + "\n"
        str += "<%=den91(6)%> " + date2str(fLocal, vdatefmt, true)

        e.title = str

    }

</script>



<table ALIGN="left" WIDTH="630" style="margin-left:-3pt">
	<tr>
		<td colspan="4" class="cabecera">
			<%=den91(7)%>: <%=oProceso.anyo & "/" & oProceso.gmn1cod & "/" & oProceso.cod & " " & oProceso.den%>
		</td>
	</tr>
	<tr>
		<td class="cabecera">
			<%=den91(8)%>
		</td>
		<td class="filaimpar"> 
			<script>
			<%=scriptFecha("vfa" & i, year(oProceso.FechaApertura), month(oProceso.FechaApertura), day(oProceso.FechaApertura),Hour(oProceso.FechaApertura), minute(oProceso.FechaApertura), utcservidor)%>

			var vfa<%=i%>FechaLocal

			vfa<%=i%>FechaLocal = new Date()
	
			otz = vfa<%=i%>FechaLocal.getTimezoneOffset()
			tzoaux=<%=utcservidor%>+otz
			
			vfa<%=i%>FechaLocal = new Date(vfa<%=i%>vFechaServer.getFullYear(), vfa<%=i%>vFechaServer.getMonth(), vfa<%=i%>vFechaServer.getDate(), vfa<%=i%>vFechaServer.getHours(), vfa<%=i%>vFechaServer.getMinutes() - tzoaux)

			p = date2str(vfa<%=i%>FechaLocal,vdatefmt,true)

				
			</script>
			<a href="javascript:void(null)" onmouseover="showPopUp(this,vfa<%=i%>vFechaServer,vfa<%=i%>vFechaUTC,vfa<%=i%>vFechaUTCGMT1)" onclick="alert(this.title)">
            <script>document.write(p)</script></a><br>
		</td>
		<td class="cabecera">
			<%=den91(9)%>
		</td>
		<td class="filaimpar">
			<script>
			<%=scriptFecha("v" & i, year(oProceso.FechaMinimoLimOfertas), month(oProceso.FechaMinimoLimOfertas), day(oProceso.FechaMinimoLimOfertas),Hour(oProceso.FechaMinimoLimOfertas), minute(oProceso.FechaMinimoLimOfertas), utcservidor)%>
			
			var v<%=i%>FechaLocal
			
			v<%=i%>FechaLocal = new Date()
			
			otz = v<%=i%>FechaLocal.getTimezoneOffset()
			tzoaux=<%=utcservidor%>+otz

			v<%=i%>FechaLocal = new Date(v<%=i%>vFechaServer.getFullYear(), v<%=i%>vFechaServer.getMonth(), v<%=i%>vFechaServer.getDate(), v<%=i%>vFechaServer.getHours(), v<%=i%>vFechaServer.getMinutes() - tzoaux)
			
			p = date2str(v<%=i%>FechaLocal,vdatefmt,true)			
			</script>
			<a href="javascript:void(null)" onmouseover="showPopUp(this,v<%=i%>vFechaServer,v<%=i%>vFechaUTC,v<%=i%>vFechaUTCGMT1)" onclick="alert(this.title)">
            <script>document.write(p)</script></a><br>
		</td>
	</tr>
</table>		


<script>
    function Imprimir() {
        window.print()
    }

</script>







<%


set oProceso = nothing
set oRaiz = nothing

server.ScriptTimeout =oldTimeOut

%>

</BODY>
</HTML>
