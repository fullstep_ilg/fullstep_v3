﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/fsal_7.asp"-->

<%
''' <summary>
''' Imprimir la oferta. Ahora muestra fecha fin subasta en el equipo local.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cumpoferta.asp imprimir.asp; Tiempo máximo:0</remarks>

OldTimeOut = server.ScriptTimeout 
server.ScriptTimeout =30000
dim den
Idioma = Request.Cookies("USU_IDIOMA")
den = devolverTextos(Idioma,101)

Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,true,false,0)


utcservidor= oraiz.UTCServidor	
	
'Obtiene los datos de la oferta
CiaComp = clng(oRaiz.Sesion.CiaComp)
CodProve = oRaiz.Sesion.CiaCodGs
anyoproceso=Request.Cookies ("PROCE_ANYO") 
gmn1proceso=Request.Cookies ("PROCE_GMN1") 
codproceso=Request.Cookies ("PROCE_COD") 
NumOfe = request("NumOfe")

codMon=Request("codMon")
denMon=Request("denMon")

'obtiene los formatos del usuario
decimalfmt = Request.Cookies ("USU_DECIMALFMT")
thousanfmt = Request.Cookies ("USU_THOUSANFMT") 
precisionfmt = CINT(Request.Cookies ("USU_PRECISIONFMT") )
datefmt = Request.Cookies ("USU_DATEFMT") 


%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><%=title%></title>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script src="../common/ajax.js"></script>
<script src="../common/formatos.js"></script>
<!--#include file="../common/fsal_3.asp"-->
</head>

<%
	

set oDestinos = oRaiz.Generar_CDestinos()

	

Set oProceso = oRaiz.Generar_CProceso


oProceso.cargarConfiguracion CiaComp, anyoproceso, gmn1proceso, codproceso, CodProve
oProceso.cargarProceso CiaComp, Idioma
%>

<style>

.rellenado {background-color:#dcdcdc;}

</style>
<script>
function resize()
	{
	p = window.opener 
	
	p.winEspera.close()
	window.moveTo(60,20)
	window.resizeTo (700,550)
	}
	
function init()
{
    if (<%=Application("FSAL")%> == 1)
    {
        Ajax_FSALActualizar3(); //registro de acceso
    }
}

</script>
<body onload="resize(),init()" topmargin=0>
<table width=100%>
	<tr >
		<td align=right>
			<a href="javascript:window.print()"><img border="0" name="imgimprimir" src="images/imprimir_off.gif" onmouseover="this.src='images/imprimir_on.gif'" onmouseout="this.src='images/imprimir_off.gif'" WIDTH="23" HEIGHT="22"></a>
		</td>
			
	</tr>
</table>
<hr>
<table width=100%>
	<tr >
		<td width=50% valign=top class="negrita fgrande">
			<%=oProceso.anyo%>/<%=oProceso.gmn1Cod%>/<%=oProceso.cod%>&nbsp;<%=oProceso.den%>
		</td>
		<td width=30% valign=top class="negrita fmedia">
			<div name =divTotalOferta id =divTotalOferta>
			</div>
		</td>
		<td width=20% valign=top>
			<span class=rellenado ><%=den(2)%></span>
		</td>
			
	</tr>
</table>

<%

If oProceso.DefEspecificaciones And oProceso.EspAdj Then
    oProceso.CargarEspecificaciones CiaComp
End If%>

<script>
	var vFechaLimite
	vFechaLimite = <%=JSHora(oProceso.FechaMinimoLimOfertas)%>
	
	var vdatefmt
	vdatefmt = "<%=datefmt%>"
</script>

<%set oOferta = oProceso.CargarOfertaProveedor(Idioma,ciacomp,codprove,NumOfe)
equiv = oProceso.cambio * oOferta.cambio
if request("IdPortal")="null" or request("IdPortal") = "" then
	if request.Cookies ("PROCE_IDPORTAL") <> "" then
		oOferta.IDPortal = request.Cookies ("PROCE_IDPORTAL")
	else
		oOferta.IDPortal = null
	end if
else
	oOferta.IDPortal = numero(request("IdPortal"))
end if

IF oOferta.codmon = "" then
    set oProve = oraiz.DevolverProveedorEnCiaComp(Idioma, oRaiz.Sesion.CiaId, CiaComp)
    sCodMon = "EUR"
    if not oProve is nothing then
        with oProve
		    if isnull(.CodMon) then
			    set oRS = oRaiz.ObtenerMonedaCentral(ciacomp)
			    if not oRS is nothing then
				    sCodMon = oRS.fields("MON_GS").value
				    oRS.close
				    set oRs = nothing				        
			    end if
		    else
			    sCodMon = .CodMon
		    end if            
        end with
    end if
    set oProve = nothing 

    set oMonedas = oRaiz.Generar_CMonedas()
	oMonedas.CargarTodasLasMonedasDesde Idioma,CiaComp , 1, sCodMon, , , true, , true

	ooferta.codMon = sCodMon
	oOferta.monden = oMonedas.Item(sCodMon).den
    set oMonedas = nothing
end if


' Carga de los datos de Costes / Descuentos
oProceso.CargarCostesDescuentos CiaComp

oProceso.CargarAtributos CiaComp
oProceso.Oferta.CargarAtributosOfertados CiaComp, oOferta.codmon
oOferta.cargarAdjuntos CiaComp
	
	
Set oProceso.Oferta.OfertaGrupos = oRaiz.Generar_COfertaGrupos()
Dim oGrupo
if not oProceso.Grupos is nothing then
	For Each oGrupo In oProceso.Grupos
	    oGrupo.cargarGrupo Idioma,CiaComp,,,,,codprove,numofe,oOferta.codmon,oProceso.SolicitarCantMax
	    oGrupo.CargarEspecificaciones CiaComp
	    oGrupo.CargarAtributos CiaComp, 2
		    
	    Set oOfeGrupo = oProceso.Oferta.OfertaGrupos.Add(oProceso.Oferta, oGrupo)
		oOfeGrupo.cargarAdjuntos CiaComp
	    oGrupo.CargarAtributos CiaComp, 3
	Next
end if


%>
<table width=100%>
	<tr>
		<td class=negrita>
			<%=den(3)%>
		</td>
		<td class=negrita>
			<%=den(4)%>
		</td>
		<td class=negrita>
			<%=den(5)%>
		</td>
		<td class=negrita>
			<%=den(9)%>
		</td>
	</tr>
	<tr>
		<td >
			<%=visualizacionfecha(oOferta.FechaUltOfe,datefmt)%>
		</td>
		<td >
			<%=oOferta.totEnviadas%>
		</td>
		<td >
			<%select case oOferta.EstUltOfe
				case 1:
					Response.Write den(6)
				case 2:
					Response.Write den(8)
				case 3:
					Response.Write den(7)
			end select%>
		</td>
		<td >
           <% if not isnull(oProceso.FechaMinimoLimOfertas) then%>
			<script>
			var FechaLocal

			FechaLocal = new Date()
	
			otz = FechaLocal.getTimezoneOffset()
			tzoaux=<%=utcservidor%>+otz		
			FechaLocal = new Date(vFechaLimite.getFullYear(), vFechaLimite.getMonth(), vFechaLimite.getDate(), vFechaLimite.getHours(), vFechaLimite.getMinutes() - tzoaux)
			p =date2str(FechaLocal,vdatefmt,-1)
			document.write(p)
			</script>
            <%end if %>
			&nbsp;
		</td>
	</tr>
</table>
<hr>
<%
if oProceso.defDestino = 1 or oProceso.DefFormaPago = 1 or oProceso.DefFechasSum = 1 or oProceso.DefEspecificaciones = 1 then
%>
<table>
	<tr>
		<td class="fmedia negrita">
			<%=den(10)%>
		</td>
	</tr>
</table>
<hr>
	<%if oProceso.defDestino = 1 then%>
	<table width=100%>
		<tr>
			<td width=10% class=negrita><%=den(11)%></td>
			<td width=15%><%=oProceso.destino.cod%>&nbsp;<%=oProceso.destino.den%></td>
			<td width=75%><%=oProceso.destino.dir%>&nbsp;<%=oProceso.destino.cp%>&nbsp;<%=oProceso.destino.pob%>&nbsp;<%=oProceso.destino.provi%>&nbsp;<%=oProceso.destino.pai%>&nbsp;</td>
		</tr>
	</table>
	<%end if%>
	<table width=100%>
		<tr>
		<%if oProceso.DefFormaPago = 1 then%>
			<td width=15% class=negrita><%=den(12)%></td>
			<td width=20% ><%=oProceso.pagCod%>&nbsp;<%=oProceso.pagDen%></td>
		<%end if

		if oProceso.DefFechasSum = 1 then%>
			<td width=15% class=negrita><%=den(13)%></td>
			<td width=10% ><%=visualizacionfecha(oProceso.FechaInicioSuministro,datefmt)%></td>
			<%if oProceso.MostrarFinSum then%>
				<td width=15% class=negrita><%=den(14)%></td>
				<td width=10% ><%=visualizacionfecha(oProceso.FechaFinSuministro,datefmt)%></td>
			<%else%>
				<td width=15% class=negrita>&nbsp;</td>
				<td width=10% >&nbsp;</td>
			<%end if%>

		<%end if%>
		</tr>
	</table>

	<%
	if oProceso.DefEspecificaciones  And oProceso.EspAdj  then
		if not isnull(oProceso.Esp) then%>
			<table width=100%>
				<tr>
					<td class=negrita><%=den(15)%></td>
				<tr>
				</tr>
					<td><%=VB2HTML(oProceso.esp)%></td>
				</tr>
			</table>
		<%end if%>
		<%if oProceso.especificaciones.count>0 then%>
			<table width=100%>
				<tr>
					<td colspan =2 class=negrita>
						<%=den(16)%>
					</td>
				</tr>
			<%for each oEsp in oProceso.especificaciones%>
				<tr>
					<td width=25% valign=top>
						<%=oEsp.nombre%>
					</td>
					<td width=75% valign=top>
						<%=VB2HTML(oEsp.comentario)%>
					</td>
				</tr>
			<%next%>
			</table>
		<%end if
	end if
%>
<hr>

<%end if%>
<%if not oProceso.adminpublica then%>
	<table>
		<tr>
			<td class="fmedia negrita">
				<%=den(17)%>
			</td>
		</tr>
	</table>
	<hr>

	<table width=80%>
		<tr>
			<td width=20% class=negrita><%=den(18)%></td>
			<td width=10% > <span class=rellenado ><%=visualizacionFecha(oOferta.FechaHasta,datefmt)%></span> </td>
			<td width=20%>&nbsp;</td>
			<td width=25% class=negrita><%=den(19)%></td>
			<td width=25%><span class=rellenado ><%=oOferta.codmon & " - " & oOferta.monden%></span></td>
		</tr>
	</table>


	<%if not isnull(oOferta.obs) and oOferta.obs <>"" then%>
		
	<table width=100%>
		<tr>
			<td class=negrita><%=den(20)%></td>
		</tr>
		<tr>
			<td>
				<span class=rellenado><%=VB2HTML(oOferta.obs)%></span>
			</td>
		</tr>
	</table>
	<%end if%>

	<%if oProceso.DefAdjunOferta then
		if not isnull(oOferta.ObsAdjuntos) and oOferta.obsAdjuntos<>"" then%>
			
		<table width=100%>
			<tr>
				<td class=negrita><%=den(21)%></td>
			</tr>
			<tr>
				<td>
					<span class=rellenado><%=VB2HTML(oOferta.ObsAdjuntos)%></span>
				</td>
			</tr>
		</table>
		<%end if%>
		<%if oOferta.adjuntos.count>0 then%>
			<table width=100%>
				<tr>
					<td colspan =2 class=negrita>
						<%=den(22)%>
					</td>
				</tr>
				
			<%for each oAdjun in oOferta.adjuntos%>
				<tr>
					<td width=25% valign=top>
						<span class=rellenado><%=oAdjun.nombre%></span>
					</td>
					<td width=75% valign=top>
						<span class=rellenado><%=VB2HTML(oAdjun.comentario)%></span>
					</td>
				</tr>
			<%next%>
			</table>
		<%end if%>
			 
	<%end if%>


	<%
	noAtributos = false
	if oProceso.atributos is nothing then
		noAtributos = true
	else
		if oProceso.atributos.count=0 then
			noAtributos = true
		end if
	end if



	if not noAtributos then%>
	<table width=100%>
		<tr>
			<td class=negrita><%=den(23)%></td>
		</tr>
	</table>
	<table width=100%>
	<%for each oAtrib in oProceso.atributos%>
		<tr>
			<td width=5%><%=oAtrib.Cod%></td>
			<td width=30%><%=oAtrib.den%></td>
			<td width=65%
			<%
			if not oOferta.AtribsOfertados is nothing then
				if not oOferta.AtribsOfertados.Item(cstr(oAtrib.PAID)) is nothing then
					select case oAtrib.Tipo
						case 1
							response.write "><span class=rellenado>" & VB2HTML(oOferta.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor)
						case 2
							'no aplicamos la condición de si es precio por que el cambio es 1 siempre al estar los atributos generales siempre en moneda de la oferta
							vValor = oOferta.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor
							response.write "><span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
						case 3
							response.write "><span class=rellenado>" &  visualizacionFecha(oOferta.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor,datefmt)
						case 4
							if oOferta.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor then
								Response.write  "><span class=rellenado>" & den(27)
							else
								Response.write  "><span class=rellenado>" & den(28)
							end if
					end select
				else
					Response.Write ">&nbsp;"
				end if
			else
				Response.Write ">&nbsp;"
			end if
			%>
			</span></td>
		</tr>
	<%next%>
	</table>
	<hr>
	<%end if%>
<%end if%>


<%
    '''''''''Tratamiento para los costes/descuentos'''''''''

   'Comprobamos si hay costes/descuentos que tratar
	noCostesDescuentosOferta = false
	if  oProceso.CostesDescTotalOferta is nothing then
		noCostesDescuentosOferta = true
	else
        if  oProceso.CostesDescTotalOferta.count=0 then
			noCostesDescuentosOferta = true
		end if
	end if

    escritaCabecera = false

    ' Si hay costes/descuentos de oferta se sacan 
   	if not noCostesDescuentosOferta then%>
	<table width=100%>
		<tr>
			<td class=negrita><%=den(50)%></td> 
		</tr>
	</table>
    <%escritaCabecera = true%>
	<table width=100%>

		<%for each oCosteDesc in oProceso.CostesDescTotalOferta%>
			<tr>
				<%if oCosteDesc.obligatorio then%>
					<td width=5% class=negrita>(*)&nbsp;<%=oCosteDesc.Cod%></td>
					<td width=30% class=negrita><%=oCosteDesc.den%></td>
				<%else%>
					<td width=5%><%=oCosteDesc.Cod%></td>
					<td width=30%><%=oCosteDesc.den%></td>
				<%end if%>
				<td width=65%
				<%
				if not oOferta.AtribsOfertados is nothing then
					if not oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)) is nothing then
						if oCosteDesc.obligatorio and isnull(oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor) then
							Response.Write "><span class=error>****"
							bFalta = true
						else
							vValor = oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor
							response.write "><span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
						end if
					else
						if oCosteDesc.obligatorio  then
							Response.Write "><span class=error>****"
							bFalta = true
						else					
							Response.Write "><span>&nbsp;"
						end if
					end if
				else
					if oCosteDesc.obligatorio then
						Response.Write "><span class=error>****"
						bFalta = true
					else					
						Response.Write "><span>&nbsp;"
					end if
				end if
				%>
				</span></td>
			</tr>
		<%next%>
	<%end if



    '''''''''Tratamiento para los costes/descuentos de Item ámbito oferta'''''''''
    dim noCostesDescuentosTotItemOfe, noCostesDescuentosPreItemOfe
   'Comprobamos si hay costes/descuentos de Item que tratar
	noCostesDescuentosTotItemOfe = false
	if  oProceso.CostesDescTotalItem is nothing then
		noCostesDescuentosTotItemOfe = true
	else
        if  oProceso.CostesDescTotalItem.count=0 then
            noCostesDescuentosTotItemOfe = true
		end if
	end if

   'Comprobamos si hay costes/descuentos de Item que tratar
	noCostesDescuentosPreItemOfe = false
	if  oProceso.CostesDescPrecItem is nothing then
		noCostesDescuentosPreItemOfe = true
	else
        if  oProceso.CostesDescPrecItem.count=0 then
            noCostesDescuentosPreItemOfe = true
		end if
	end if


    ' Si hay costes/descuentos de item para la oferta se sacan 
   	if not noCostesDescuentosTotItemOfe then%>
		<%for each oCosteDesc in oProceso.CostesDescTotalItem%>
            <%if oCosteDesc.ambito = 1 then%>
                <%if not escritaCabecera then %>
            	    <table width=100%>
		                <tr>
			                <td class=negrita><%=den(50)%></td> <!-- Costes/descuentos adicionales de la oferta-->
		                </tr>
	                </table>
                    <table width=100%>
                    <%escritaCabecera = true%>
                <%end if%>
			    <tr>
				    <%if oCosteDesc.obligatorio then%>
					    <td width=5% class=negrita>(*)&nbsp;<%=oCosteDesc.Cod%></td>
					    <td width=30% class=negrita><%=oCosteDesc.den%></td>
                        <td width=30% class=negrita><%=den(54)%></td>
				    <%else%>
					    <td width=5%><%=oCosteDesc.Cod%></td>
					    <td width=30%><%=oCosteDesc.den%></td>
                        <td width=30%><%=den(54)%></td>
				    <%end if%>
				    <td width=65%
				    <%
				    if not oOferta.AtribsOfertados is nothing then
					    if not oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)) is nothing then
						    if oCosteDesc.obligatorio and isnull(oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor) then
							    Response.Write "><span class=error>****"
							    bFalta = true
						    else
							    vValor = oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor
							    response.write "><span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
						    end if
					    else
						    if oCosteDesc.obligatorio  then
							    Response.Write "><span class=error>****"
							    bFalta = true
						    else					
							    Response.Write "><span>&nbsp;"
						    end if
					    end if
				    else
					    if oCosteDesc.obligatorio then
						    Response.Write "><span class=error>****"
						    bFalta = true
					    else					
						    Response.Write "><span>&nbsp;"
					    end if
				    end if
				    %>
				    </span></td>
			    </tr>
            <%end if%>
		<%next
        end if
        if not noCostesDescuentosPreItemOfe then%>
		    <%for each oCosteDesc in oProceso.CostesDescPrecItem%>
                <%if oCosteDesc.ambito = 1 then%>
                    <%if not escritaCabecera then %>
            	        <table width=100%>
		                    <tr>
			                    <td class=negrita><%=den(50)%></td> <!-- Costes/descuentos adicionales de la oferta-->
		                    </tr>
	                    </table>
                        <table width=100%>
                        <%escritaCabecera = true%>
                    <%end if%>
			        <tr>
				        <%if oCosteDesc.obligatorio then%>
					        <td width=5% class=negrita>(*)&nbsp;<%=oCosteDesc.Cod%></td>
					        <td width=30% class=negrita><%=oCosteDesc.den%></td>
                            <td width=30% class=negrita><%=den(55)%></td>
				        <%else%>
					        <td width=5%><%=oCosteDesc.Cod%></td>
					        <td width=30%><%=oCosteDesc.den%></td>
                            <td width=30%><%=den(55)%></td>
				        <%end if%>
				        <td width=65%
				        <%
				        if not oOferta.AtribsOfertados is nothing then
					        if not oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)) is nothing then
						        if oCosteDesc.obligatorio and isnull(oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor) then
							        Response.Write "><span class=error>****"
							        bFalta = true
						        else
							        vValor = oOferta.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor
							        response.write "><span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
						        end if
					        else
						        if oCosteDesc.obligatorio  then
							        Response.Write "><span class=error>****"
							        bFalta = true
						        else					
							        Response.Write "><span>&nbsp;"
						        end if
					        end if
				        else
					        if oCosteDesc.obligatorio then
						        Response.Write "><span class=error>****"
						        bFalta = true
					        else					
						        Response.Write "><span>&nbsp;"
					        end if
				        end if
				        %>
				        </span></td>
			        </tr>
                <%end if%>
		     <%next%>
        <%end if%>     

    <%if escritaCabecera then %>
    </table>
	<hr>
	<%end if%>
   

<%
'oOferta.ImporteTotal = 0
if not oProceso.Grupos is nothing then
for each oGrupo in oProceso.grupos
'oOferta.ImporteTotal = oOferta.ImporteTotal + oGrupo.importeTotal 
'oOferta.ImporteTotal = oOferta.ImporteTotal + oGrupo.importeTotal * equiv
'Quito el *equiv por que no tiene sentido. El importe total del grupo viene en la moneda QUE SE LE PASA QUE EN ESTE CASO ES LA MONEDA de la oferta
%>

<table width=100%>
	<tr >
		<td width=70% class="negrita fmedia">
			<%=den(24)%>&nbsp;<%=oGrupo.codigo%>&nbsp;-&nbsp;<%=oGrupo.den%><%if oProceso.adminpublica then%>&nbsp;(<%=den(25)%><%=oGrupo.sobre%>:<%=den(29)%><%=visualizacionfecha(oProceso.sobres.item(oGrupo.sobre).FechaApertura,datefmt)%>)<%end if%>
		</td>
		<td width=30% class="negrita fmedia numero">
			<nobr>
				<%=den(1)%>&nbsp;<%=visualizacionNumero(oGrupo.importeTotal,decimalfmt,thousandfmt,precisionfmt)%>&nbsp<%=oOferta.codmon%>
			</nobr>
		</td>
	</tr>
</table>
<hr>

<%if not isnull(oGrupo.descripcion) and oGrupo.descripcion<>"" then%>
<table width=100%>
	<tr>
		<td class=negrita><%=den(26)%></td>
	<tr>
	</tr>
		<td><%=VB2HTML(oGrupo.descripcion)%></td>
	</tr>
</table>
<%end if%>


<%
if oProceso.defDestino = 2 or oProceso.DefFormaPago = 2 or oProceso.DefFechasSum = 2 or oProceso.DefEspecificaciones = 2 then
%>
	<%if oProceso.defDestino = 2 and not oGrupo.destino is nothing then%>
	<table width=100%>
		<tr>
			<td width=10% class=negrita><%=den(11)%></td>
			<td width=15%><%=oGrupo.destino.cod%>&nbsp;<%=oGrupo.destino.den%></td>
			<td width=75%><%=oGrupo.destino.dir%>&nbsp;<%=oGrupo.destino.cp%>&nbsp;<%=oGrupo.destino.pob%>&nbsp;<%=oGrupo.destino.provi%>&nbsp;<%=oGrupo.destino.pai%>&nbsp;</td>
		</tr>
	</table>
	<%end if%>
	<table width=100%>
		<tr>
		<%if oProceso.DefFormaPago = 2 then%>
			<td width=15% class=negrita><%=den(12)%></td>
			<td width=20% ><%=oGrupo.pagCod%>&nbsp;<%=oGrupo.pagDen%></td>
		<%end if

		if oProceso.DefFechasSum = 2 then%>
			<td width=15% class=negrita><%=den(13)%></td>
			<td width=10% ><%=visualizacionfecha(oGrupo.FechaInicioSuministro,datefmt)%></td>

			<%if oProceso.MostrarFinSum then%>
				<td width=15% class=negrita><%=den(14)%></td>
				<td width=10% ><%=visualizacionfecha(oGrupo.FechaFinSuministro,datefmt)%></td>
			<%else%>
				<td width=15% class=negrita>&nbsp;</td>
				<td width=10% >&nbsp;</td>
			<%end if%>
		<%end if%>
		</tr>
	</table>

	<%
	if oGrupo.DefEspecificaciones And oGrupo.EspAdj  then
		if not isnull(oGrupo.Esp) then%>
			<table width=100%>
				<tr>
					<td class=negrita><%=den(15)%></td>
				<tr>
				</tr>
					<td><%=VB2HTML(oGrupo.esp)%></td>
				</tr>
			</table>
		<%end if%>
		<%if oGrupo.especificaciones.count>0 then%>
			<table width=100%>
				<tr>
					<td colspan =2 class=negrita>
						<%=den(16)%>
					</td>
				</tr>
			<%for each oEsp in oGrupo.especificaciones%>
				<tr>
					<td width=25% valign=top>
						<%=oEsp.nombre%>
					</td>
					<td width=75% valign=top>
						<%=VB2HTML(oEsp.comentario)%>
					</td>
				</tr>
			<%next%>
			</table>
		<%end if
	end if
end if
%>

<%

set oOfeGrupo = oOferta.OfertaGrupos.item(oGrupo.codigo)
oOfeGrupo.CargarAtributosOfertados CiaComp,oOferta.codmon
oGrupo.cargarItems Idioma,CiaComp,codmon,codprove,oOferta.num,true
oOfeGrupo.cargarPrecios ciacomp,oOferta.codmon

oOfeGrupo.CargarTodosAdjuntos CiaComp
if oProceso.DefAdjunGrupo then
	if not isnull(oGrupo.ObsAdjuntos) and oGrupo.obsAdjuntos<>"" then%>
		
	<table width=100%>
		<tr>
			<td class=negrita><%=den(21)%></td>
		</tr>
		<tr>
			<td>
				<span class=rellenado><%=VB2HTML(oGrupo.ObsAdjuntos)%></span>
			</td>
		</tr>
	</table>
	<%end if%>
	<%if oOfeGrupo.adjuntos.count>0 then%>
		<table width=100%>
			<tr>
				<td colspan =2 class=negrita>
					<%=den(22)%>
				</td>
			</tr>
			
		<%for each oAdjun in oOfeGrupo.adjuntos%>
			<tr>
				<td width=25% valign=top>
					<span class=rellenado><%=oAdjun.nombre%></span>
				</td>
				<td width=75% valign=top>
					<span class=rellenado><%=VB2HTML(oAdjun.comentario)%></span>
				</td>
			</tr>
		<%next%>
		</table>
	<%end if%>
		 
<%end if%>

<%
noAtributos = false
if oGrupo.atributos is nothing then
	noAtributos = true
else
	if oGrupo.atributos.count=0 then
		noAtributos = true
	end if
end if



if not noAtributos then%>
<table width=100%>
	<tr>
		<td class=negrita><%=den(23)%></td>
	</tr>
</table>
<table width=100%>
<%for each oAtrib in oGrupo.atributos%>
	<tr>
		<td width=5%><%=oAtrib.Cod%></td>
		<td width=30%><%=oAtrib.den%></td>
		<td width=65%
		<% if not oOfeGrupo.AtribsOfertados is nothing then
				if not oOfeGrupo.AtribsOfertados.Item(cstr(oAtrib.PAID)) is nothing then
					select case oAtrib.Tipo
						case 1
							response.write "><span class=rellenado>" & VB2HTML(oOfeGrupo.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor)
						case 2
							if (oAtrib.operacion = "+" or  oAtrib.operacion = "-") then
								vValor = oOfeGrupo.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor  / oOfeGrupo.cambio * equiv
							else
								vValor = oOfeGrupo.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor
							end if
							response.write "><span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
						case 3
							response.write "><span class=rellenado>" &  visualizacionFecha(oOfeGrupo.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor,datefmt)
						case 4
							if oOfeGrupo.AtribsOfertados.Item(cstr(oAtrib.PAID)).valor then
								Response.write  "><span class=rellenado>" & den(27)
							else
								Response.write  "><span class=rellenado>" & den(28)
							end if
					end select
				else
					Response.Write ">&nbsp;"
				end if
			else
				Response.Write ">&nbsp;"
			end if
		%>
		</span></td>
	</tr>
<%next%>
</table>
<%end if%>

<%
'''''''''Tratamiento para los costes/descuentos de Total Grupo'''''''''

'Comprobamos si hay costes/descuentos de Total Grupo que tratar
noCostesDescuentosGrupo = false
if  oProceso.CostesDescTotalGrupo is nothing then
	noCostesDescuentosGrupo = true
else
    if  oProceso.CostesDescTotalGrupo.count=0 then
		noCostesDescuentosGrupo = true
	end if
end if





if not noCostesDescuentosGrupo then%>
<table width=100%>
	<tr>
		<td class=negrita><%=den(51)%></td>
	</tr>
</table>
<table width=100%>
<%for each oCosteDesc in oProceso.CostesDescTotalGrupo%>
	<tr>
		<%if oCosteDesc.obligatorio then%>
			<td width=5% class=negrita>(*)&nbsp;<%=oCosteDesc.Cod%></td>
			<td width=30% class=negrita><%=oCosteDesc.den%></td>
		<%else%>
			<td width=5%><%=oCosteDesc.Cod%></td>
			<td width=30%><%=oCosteDesc.den%></td>
		<%end if%>
		<td width=65%
		<%
			if not oOfeGrupo.AtribsOfertados is nothing then
				if not oOfeGrupo.AtribsOfertados.Item(cstr(oCosteDesc.PAID)) is nothing then
                    if oCosteDesc.obligatorio and isnull(oOfeGrupo.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor) then
						Response.Write "><span class=error>****"
						bFalta = true
					else
					    if (oCosteDesc.operacion = "+" or  oCosteDesc.operacion = "-") then
						    vValor = oOfeGrupo.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor  / oOfeGrupo.cambio * equiv
					    else
						    vValor = oOfeGrupo.AtribsOfertados.Item(cstr(oCosteDesc.PAID)).valor
					    end if
					    response.write "><span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
                    end if
				else
					if oCosteDesc.obligatorio then
						Response.Write "><span class=error>****"
						bFalta = true
					else					
						Response.Write "><span>&nbsp;"
					end if
                end if
			else
				if oCosteDesc.obligatorio then
					Response.Write "><span class=error>****"
					bFalta = true
				else					
					Response.Write "><span>&nbsp;"
				end if
			end if
		%>
		</span></td>
	</tr>
	<%next%>
</table>
<%end if%>



<%

if oGrupo.items.count >0 then
	for each oItem in oGrupo.items

	if oProceso.defDestino=3 then
		if  oDestinos.item(oItem.DestCod) is nothing then
			set adorDest = oDestinos.CargarDatosDestino(Idioma,Ciacomp, oItem.DestCod)
			oDestinos.Add adorDest("COD").Value, adorDest("DEN").Value, false, adorDest("DIR"), adorDest("POB"), adorDest("CP"), adorDest("PROVI"), adorDest("PAI")
			adorDest.close
			set adorDest = nothing
		end if
	end if



	%>

	<table width=100%>
		<tr>
			<td style="font-size:1px;border-bottom:1 dashed black">
				&nbsp;
			</td>
		</tr>
	</table>
				
	<table width=100%>
		<tr >
			<td width=25% valign=top>
				<span class="negrita">
				(<%=oGrupo.codigo%>)&nbsp;
				<%
				if isnull(oItem.ArticuloCod) then
					Response.Write VB2HTML(oItem.Descr)
				else
					Response.Write oItem.ArticuloCod & " - " & oItem.Descr
				end if
				%>
				</span>
				<BR>
				<BR>
				<nobr>
					<b><%=den(31)%></b>&nbsp;<%=visualizacionNumero(oItem.Cantidad,decimalfmt,thousandfmt,precisionfmt)%>
				</nobr>
				<BR>
				<nobr>
					<b><%=den(49)%></b>&nbsp;<%= oItem.uniCod & " " &  oItem.uniDen%>
				</nobr>
			</td>

			<td align=right>
				<table width = 100%>
					<%if not isnull(oItem.precioItem.PrecioOferta) then%>
					<tr>
						<td width="40%" >
							<nobr>
							<b><%=den(30)%></b>&nbsp;<span class=rellenado><%=visualizacionNumero(oItem.precioItem.PrecioOferta / oOferta.cambio * equiv,decimalfmt,thousandfmt,-1)%></span>
							</nobr>
						</td>
						<%if not isnull(oItem.Objetivo) and oProceso.objetivosPublicados then%>
							<td width=30%>
								<nobr>
								<b><%=den(36)%></b>&nbsp;<%=visualizacionNumero(oItem.Objetivo,decimalfmt,thousandfmt,precisionfmt)%>
								</nobr>
							</td>
						<%end if%>
						<td class=negrita>
							<nobr>
								<%=den(1)%>&nbsp;<span class=rellenado><%=visualizacionNumero(oItem.precioItem.ImporteNeto,decimalfmt,thousandfmt,precisionfmt)%>&nbsp<%=oOferta.codmon%></span>
							</nobr>
						</td>
					</tr>
					<%else%>
					<tr>
						<td >
							<span class="error" style="font-weight:normal;font-size:10px;"><%=den(35)%></span>
						</td>
					</tr>
					<%end if%>
				</table>
				<hr>
				<%if oProceso.PedirAlterPrecios then%>
				<%if not isnull(oItem.precioItem.Precio2) then%>
				<table width = 100%>
					<tr>
						<td width="40%">
							<nobr>
							<b><%=den(33)%></b>&nbsp;<span class=rellenado><%=visualizacionNumero(oItem.PrecioItem.Precio2 / oOferta.cambio * equiv,decimalfmt,thousandfmt,precisionfmt)%></span>
							</nobr>
						</td>
						<%if not isnull(oItem.Objetivo) and oProceso.objetivosPublicados then%>
							<td width="30%" ><nobr>&nbsp;</nobr></td>
						<%end if%>
						<td class=negrita>
							<nobr>
								<%=den(1)%>&nbsp;<span class=rellenado><%=visualizacionNumero(oItem.Cantidad * (oItem.PrecioItem.Precio2 / oOferta.cambio * equiv),decimalfmt,thousandfmt,precisionfmt)%>&nbsp<%=oOferta.codmon%></span>
							</nobr>
						</td>
					</tr>
				</table>
				<hr>
				<%end if
				if not isnull(oItem.precioItem.Precio3) then%>
				<table width = 100%>
					<tr>
						<td width="40%">
							<nobr>
							<b><%=den(34)%></b>&nbsp;<span class=rellenado><%=visualizacionNumero(oItem.PrecioItem.Precio3 / oOferta.cambio * equiv,decimalfmt,thousandfmt,precisionfmt)%></span>
							</nobr>
						</td>
						<%if not isnull(oItem.Objetivo) and oProceso.objetivosPublicados then%>
							<td width="30%">&nbsp;</td>
						<%end if%>
						<td class=negrita>
							<nobr>
								<%=den(1)%>&nbsp;<span class=rellenado><%=visualizacionNumero(oItem.Cantidad * (oItem.PrecioItem.Precio3 / oOferta.cambio * equiv),decimalfmt,thousandfmt,precisionfmt)%>&nbsp<%=oOferta.codmon%></span>
							</nobr>
						</td>
					</tr>
				</table>
				<hr>
				<%end if%>
				<%end if%>
			</td>
			
		</tr>
	</table>

	<table>
	<tr>  
				<td width="20%">&nbsp;</td>
				<td >
				<table>
				<%if not isnull(oItem.PrecioItem.Comentario1) and oItem.PrecioItem.Comentario1<>"" then%>
				
					<tr>
						<td class=negrita>
							<%=den(32)%>
						</td>
						<td>
							<span class=rellenado>
								<%=VB2HTML(oItem.PrecioItem.Comentario1)%>
							</span>
						</td>
					</tr>
				
				<%end if%>
				<%if not isnull(oItem.PrecioItem.Comentario2) and oItem.PrecioItem.Comentario2<>"" then%>
				
					<tr>
						<td class=negrita>
							<%=den(32)%>2
						</td>
					
						<td>
							<span class=rellenado>
								<%=VB2HTML(oItem.PrecioItem.Comentario2)%>
							</span>
						</td>
					</tr>
				
				<%end if%>
				<%if not isnull(oItem.PrecioItem.Comentario3) and oItem.PrecioItem.Comentario3<>"" then%>
				
					<tr>
						<td class=negrita>
							<%=den(32)%>3
						</td>
					
						<td>
							<span class=rellenado>
								<%=VB2HTML(oItem.PrecioItem.Comentario3)%>
							</span>
						</td>
					</tr>
				
				<%end if%>
			</table>
		</tr>	
	</table>

	<%
	if oProceso.defDestino = 3 or oProceso.DefFormaPago = 3 or oProceso.DefFechasSum = 3 or oGrupo.defEspecificacionesItem  then
	%>
		<%if oProceso.defDestino = 3 then%>
		<table width=100%>
			<tr>
				<td width=10% class=negrita><%=den(11)%></td>
				<td width=15%><%=oDestinos.item(oItem.destCod).cod%>&nbsp;<%=oDestinos.item(oItem.destCod).den%></td>
				<td width=75%>
				<%=oDestinos.item(oItem.destCod).dir%>&nbsp;<%=oDestinos.item(oItem.destCod).cp%>&nbsp;<%=oDestinos.item(oItem.destCod).pob%>&nbsp;<%=oDestinos.item(oItem.destCod).provi%>&nbsp;<%=oDestinos.item(oItem.destCod).pai%>&nbsp;
				</td>
			</tr>
		</table>
		<%end if%>
		<table width=100%>
			<tr>
			<%if oProceso.DefFormaPago = 3 then%>
				<td width=15% class=negrita><%=den(12)%></td>
				<td width=20% ><%=oItem.PagCod & " - " & oItem.PagDen%></td>
			<%end if

			if oProceso.DefFechasSum = 3 then%>
				<td width=15% class=negrita><%=den(13)%></td>
				<td width=10% ><%=visualizacionFecha(oItem.FechaInicioSuministro,datefmt)%></td>
				<%if oProceso.MostrarFinSum then%>
					<td width=15% class=negrita><%=den(14)%></td>
					<td width=10% ><%=visualizacionFecha(oItem.FechaFinSuministro,datefmt)%></td>
				<%else%>
					<td width=15% class=negrita>&nbsp;</td>
					<td width=10% >&nbsp;</td>
				
				<%end if%>
			<%end if%>
			</tr>
		</table>
		<%
		if oGrupo.defEspecificacionesItem then
			if not isnull(oItem.Esp) then%>
				<table width=100%>
					<tr>
						<td class=negrita><%=den(15)%></td>
					<tr>
					</tr>
						<td><%=VB2HTML(oItem.Esp)%></td>
					</tr>
				</table>
			<%end if%>
			<%

'			set oItem = oRaiz.Generar_Citem()
			set oItem.conexion = oProceso.conexion
			oItem.cargarEspecificaciones ciaComp, anyoproceso, gmn1proceso, codproceso, oItem.id
			if oItem.especificaciones.count>0 then%>
				<table width=100%>
					<tr>
						<td colspan =2 class=negrita>
							<%=den(16)%>
						</td>
					</tr>
				<%for each oEsp in oItem.especificaciones%>
					<tr>
						<td width=25% valign=top>
							<%=oEsp.nombre%>
						</td>
						<td width=75% valign=top>
							<%=VB2HTML(oEsp.comentario)%>
						</td>
					</tr>
				<%next%>
				</table>
			<%end if
			
		end if
	end if
	%>

	<%if oProceso.defAdjunItem then
		if not isnull(oItem.precioItem.ObsAdjuntos) and oItem.precioItem.ObsAdjuntos<>"" then%>
			
		<table width=100%>
			<tr>
				<td class=negrita><%=den(21)%></td>
			</tr>
			<tr>
				<td>
					<span class=rellenado><%=VB2HTML(oItem.precioItem.ObsAdjuntos)%></span>
				</td>
			</tr>
		</table>
		<%end if
		set oPrecioItem = oItem.PrecioItem
		if  not oPrecioItem.adjuntos is nothing then
		if oPrecioItem.adjuntos.count>0 then%>
			<table width=100%>
				<tr>
					<td colspan =2 class=negrita>
						<%=den(22)%>
					</td>
				</tr>
				
			<%for each oAdjun in oPrecioItem.adjuntos%>
				<tr>
					<td width=25% valign=top>
						<span class=rellenado><%=oAdjun.nombre%></span>
					</td>
					<td width=75% valign=top>
						<span class=rellenado><%=VB2HTML(oAdjun.comentario)%></span>
					</td>
				</tr>
			<%next%>
			</table>
		<%end if%>
		<%end if%>

	<%end if%>

	<%
	noAtributos = false
	if oGrupo.AtributosItem is nothing then
		noAtributos = true
	else
		if oGrupo.AtributosItem.count=0 then
			noAtributos = true
		end if
	end if

	if not noAtributos then%>
	<table width=100%>
		<tr>
			<td class=negrita><%=den(23)%></td>
		</tr>
	</table>
	<table width=100%>
	<%for each oAtrib in oGrupo.AtributosItem
		Set oAtribOfertado = oItem.PrecioItem.AtributosOfertado.Item(CStr(oAtrib.PAID))
	%>
		<tr>
			<%if oAtrib.obligatorio then%>
				<td width=5% class=negrita>(*)&nbsp;<%=oAtrib.Cod%></td>
				<td width=30% class=negrita><%=oAtrib.den%></td>
			<%else%>
				<td width=5%><%=oAtrib.Cod%></td>
				<td width=30%><%=oAtrib.den%></td>
			<%end if%>
			<td width=65%
			<%
				if oAtrib.obligatorio and isnull(oAtribOfertado.valor) then
					Response.Write "><span class=error>****"
					bFalta = true
				else
					select case oAtrib.Tipo
						case 1
							response.write "><span class=rellenado>" & VB2HTML(oAtribOfertado.Valor)
						case 2
							if (oAtrib.operacion = "+" or  oAtrib.operacion = "-") then
								vValor = oAtribOfertado.valor / oOferta.cambio * equiv
							else
								vValor = oAtribOfertado.valor
							end if
							response.write "><span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
						case 3
							response.write "><span class=rellenado>" &  visualizacionFecha(oAtribOfertado.Valor,datefmt)
						case 4
							if oAtribOfertado.Valor then
								Response.write  "><span class=rellenado>" & den(27)
							else
								Response.write  "><span class=rellenado>" & den(28)
							end if
					end select
				end if
			%>
			</span></td>
		</tr>
	<%next%>
	</table>
	<%end if%>

<%
'''''''''Tratamiento para los costes/descuentos de Item: Total Item y Precio Item'''''''''

'Comprobamos si hay costes/descuentos de Item que tratar
noCostesDescuentosItem = false
if  oItem.PrecioItem.CostesDescOfertados is nothing then
	noCostesDescuentosItem = true
else
    if  oItem.PrecioItem.CostesDescOfertados.count=0 then
		noCostesDescuentosItem = true
	end if
end if

noCostesDescuentosTotalItem = false
if  oProceso.CostesDescTotalItem is nothing then
	noCostesDescuentosTotalItem = true
else
    if  oProceso.CostesDescTotalItem.count=0 then
		noCostesDescuentosTotalItem = true
	end if
end if

noCostesDescuentosPrecItem = false
if oProceso.CostesDescPrecItem is nothing then
	noCostesDescuentosPrecItem = true
else
    if  oProceso.CostesDescPrecItem.count=0 then
		noCostesDescuentosPrecItem = true
	end if
end if

PrimeraVez=True

if not noCostesDescuentosItem then%>

<% if not noCostesDescuentosTotalItem then%>
<%for each oCosteDescOfertado in oItem.PrecioItem.CostesDescOfertados
	Set oCosteDesc = oProceso.CostesDescTotalItem.Item(CStr(oCosteDescOfertado.Atributo.PAID))
    If Not oCosteDesc Is Nothing Then
        If PrimeraVez Then%>
        <table width=100%>
	        <tr>
		        <td class=negrita><%=den(52)%></td>
	        </tr>
        </table>
        <table width=100%>
        <%PrimeraVez = False
          End If %>
	<tr>
		<%if oCosteDesc.obligatorio then%>
			<td width=5% class=negrita>(*)&nbsp;<%=oCosteDesc.Cod%></td>
			<td width=30% class=negrita><%=oCosteDesc.den%></td>
		<%else%>
			<td width=5%><%=oCosteDesc.Cod%></td>
			<td width=30%><%=oCosteDesc.den%></td>
		<%end if%>
		<td width=65%
		<%
			if oCosteDesc.obligatorio and isnull(oCosteDescOfertado.valor) then
				Response.Write "><span class=error>****"
				bFalta = true
			else
				if (oCosteDesc.operacion = "+" or  oCosteDesc.operacion = "-") then
					vValor = oCosteDescOfertado.valor  / oOfeGrupo.cambio * equiv
				else
					vValor = oCosteDescOfertado.valor
				end if
				response.write "><span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
			end if
				
		%>
		</span></td>
	</tr>
    <%End If %>
<%next%>
<%end if %>
<%If Not PrimeraVez Then %>
</table>
<%End If %>

<% PrimeraVez=True%>
<% if not noCostesDescuentosPrecItem then%>
<%for each oCosteDescOfertado in oItem.PrecioItem.CostesDescOfertados
	Set oCosteDesc = oProceso.CostesDescPrecItem.Item(CStr(oCosteDescOfertado.Atributo.PAID))
    If Not oCosteDesc Is Nothing Then
        If PrimeraVez Then%>
        <table width=100%>
	    <tr>
		    <td class=negrita><%=den(53)%></td>
	    </tr>
        </table>
        <table width=100%>
        <%PrimeraVez=false
        End If %>
	<tr>
		<%if oCosteDesc.obligatorio then%>
			<td width=5% class=negrita>(*)&nbsp;<%=oCosteDesc.Cod%></td>
			<td width=30% class=negrita><%=oCosteDesc.den%></td>
		<%else%>
			<td width=5%><%=oCosteDesc.Cod%></td>
			<td width=30%><%=oCosteDesc.den%></td>
		<%end if%>
		<td width=65%
		<%
			if oCosteDesc.obligatorio and isnull(oCosteDescOfertado.valor) then
				Response.Write "><span class=error>****"
				bFalta = true
			else
				if (oCosteDesc.operacion = "+" or  oCosteDesc.operacion = "-") then
					vValor = oCosteDescOfertado.valor  / oOfeGrupo.cambio * equiv
				else
					vValor = oCosteDescOfertado.valor
				end if
				response.write "><span class=rellenado>" & visualizacionNumero(vValor,decimalfmt,thousandfmt,precisionfmt)
			end if
				
		%>
		</span></td>
	</tr>
    <%End If %>
<%next%>
<%end if %>
<%If Not PrimeraVez Then %>
</table>
<%End If %>

<%end if%>



	<%next
	%>

	<table width=100%>
		<tr>
			<td style="font-size:1px;border-bottom:1 dashed black">
				&nbsp;
			</td>
		</tr>
	</table>
	<%
end if
%>




<%next
end if
%>

	
<script>

s="				<nobr>"
s+="					<%=den(1)%>&nbsp;<%=visualizacionNumero(oOferta.ImporteTotal,decimalfmt,thousandfmt,precisionfmt)%>&nbsp<%=oOferta.codmon%>"
s+="				</nobr>"

document.getElementById("divTotalOferta").innerHTML = s

</script>
</body>
<%

server.ScriptTimeout =OldTimeOut
	
set oProceso.grupos = nothing
set oProceso.atributos = nothing
set oProceso.adjuntos = nothing
set oProceso = nothing
set oOferta = nothing

set oRaiz = nothing

%>
<!--#include file="../common/fsal_8.asp"-->
</html>