﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/fsal_1.asp"-->

<%
''' <summary>
''' Realiza la grabacion q indica q una compania no desea acudir a una oferta/subasta.
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.asp        solicitudesoferta\solicitudesoferta11.asp ; Tiempo máximo: 0,2</remarks>

dim den
Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,true,false,0)

CiaComp = clng(oRaiz.Sesion.CiaComp)
Prove = oRaiz.Sesion.CiaCodGs



anyo = request("anyo")
gmn1 = request("gmn1")
proce = request("cod")
i = request("i")

den = devolverTextos(Idioma,115)

Set oProceso = oRaiz.Generar_CProceso

oProceso.cargarProceso CiaComp, Idioma, anyo, gmn1, proce

anyo= server.HTMLEncode(anyo)
gmn1= server.HTMLEncode(gmn1)
proce= server.HTMLEncode(proce)
i= server.HTMLEncode(i)

if Request.ServerVariables("CONTENT_LENGTH")>0 then
	Motivo = request("txtMotivo")
	oProceso.anyo = anyo
	oProceso.gmn1cod = gmn1
	oproceso.cod = proce
	UsuId = oRaiz.Sesion.UsuId
	CiaId = oRaiz.Sesion.CiaId
	CodPortal = cstr(Application ("NOMPORTAL"))
	set oError = oProceso.MarcarProveedorNoOferta(CiaComp,Prove,Motivo,CiaId,UsuId, Application("PYME"), CodPortal)
	if oError.numError<>0 then
		%>
	<html>
    <head>
        <script src="../common/formatos.js"></script>
        <script language="JavaScript" src="../common/ajax.js"></script>
        <!--#include file="../common/fsal_3.asp"-->
        <title><%=title%></title>
		<script>
		    alert("Ha ocurrido un error")
            if (<%=application("FSAL")%> == 1)
            {
                Ajax_FSALActualizar3(); //registro de acceso
            }
		</script>
		<!--#include file="../common/fsal_2.asp"-->
		<%
		set oProceso=nothing
		set oRaiz=nothing
        
		response.end
	else
		%>
        <script src="../common/formatos.js"></script>
        <script language="JavaScript" src="../common/ajax.js"></script>
        <!--#include file="../common/fsal_3.asp"-->
		<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
		<script>
			window.resizeTo (600,170)
			p = window.opener
			<%if i<>0 then%>
			s = "<a href='javascript:void(null)' onclick='noOfertar(<%=JSNum(anyo)%>,\"<%=JSText(gmn1)%>\",<%=JSNum(proce)%>,<%=i%>)'>"
			s += "<img border=0 id='imgNoofertar_<%=i%>' name='imgNoofertar_<%=i%>' src='images/nooferta_on.gif' WIDTH=16 HEIGHT=16>"
			s += "</a>"
			p.document.getElementById("divNoOfertar_<%=i%>").innerHTML = s
			<%else%>
			s = "<a href='javascript:void(null)' onclick='noOfertar(<%=JSNum(anyo)%>,\"<%=JSText(gmn1)%>\",<%=JSNum(proce)%>,<%=i%>)'>"
			s += "<img border=0 id='imgNoofertar' name='imgNoofertar' src='images/nooferta_on.gif' WIDTH=16 HEIGHT=16>"
			s += "</a>"
			p.document.getElementById("divNoOfertar").innerHTML = s
			<%end if%>
            function init()
            {
                if (<%=application("FSAL")%> == 1)
                {
                    Ajax_FSALActualizar3(); //registro de acceso
                }
            }
		</script>
		</head>
		<body onload="init()" >
		
<table width=100% class=cabeceraNOOFE>
	<tr>
		<td >
<P><b><u><%=den(1)%>:&nbsp;<%=anyo & "/" & gmn1 & "/" & proce & "&nbsp;" & oProceso.den%></u></b></p>

<P class=fgrande><%=den(8)%></P>
		</td>
	</tr>
</table>
<br>
	<br>
	
		<center>
			<input type=button class=button name=cmdCerrar value="<%=den(9)%>" onclick="window.close()"> 
		</center>
		</body>
        <!--#include file="../common/fsal_2.asp"-->
		</html>
		<%	
		set oProceso=nothing
		set oRaiz=nothing
        
		response.end
	end if
end if




oProceso.CargarComentarioProveedor CiaComp , Prove

%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><%=title%></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script src="../common/formatos.js"></script>
<script language="JavaScript" src="../common/ajax.js"></script>
<!--#include file="../common/fsal_3.asp"-->
<script>
function Validar()
{
if (document.forms["frmMotivo"].elements["txtMotivo"].value=="")
	{
	alert("<%=JSText(den(7))%>")
	return false
	}
if (document.forms["frmMotivo"].elements["txtMotivoOld"].value!=document.forms["frmMotivo"].elements["txtMotivo"].value)
	return true
else
	{
	window.close()
	return false
	}
}
function init()
{
    if (<%=application("FSAL")%> == 1)
    {
        Ajax_FSALActualizar3(); //registro de acceso
    }
    document.forms["frmMotivo"].elements["txtMotivoOld"].value=document.forms["frmMotivo"].elements["txtMotivo"].value
}
</script>
</HEAD>

<BODY onload="init()" >
<form name=frmMotivo id=frmMotivo method=post onsubmit="return Validar()">
<input type=hidden name=anyo id=anyo value="<%=anyo%>">
<input type=hidden name=gmn1 id=gmn1 value="<%=gmn1%>">
<input type=hidden name=proce id=proce value="<%=proce%>">
<input type=hidden name=txtMotivoOld id=txtMotivoOld value="">
<table width=100% class=cabeceraNOOFE>
	<tr>
		<td >
<P><b><u><%=den(1)%>:&nbsp;<%=anyo & "/" & gmn1 & "/" & proce & "&nbsp;" & oProceso.den%></u></b></p>

<P><%=den(2)%></P>
		</td>
	</tr>
</table>
<br>

<table width=100% height=80% class=fondoNOOFE>
	<tr>
		<td align=center colspan=3>
			<script>
				document.write(textArea("txtMotivo", "95%", 170,4000,"<%=JSText(den(6))%>","<%=JSText(oProceso.motivoNoOfe)%>",null))
			</script>
		</td>
	</tr>

	<tr>
		<td WIDTH=50% align=CENTER>
			<input style="width:180;" type=submit class=button name=cmdAceptar id=cmdAceptar width=5 value="<%=den(3)%>">
		</td>
		<td  WIDTH=50% align=CENTER>
			<input style="width:180;" type=button class=button name=cmdCancelar id=cmdCancelar value="<%=den(4)%>" onclick="window.close()">
		</td>
	</tr>
</table>
</form>
</BODY>
<%
set oRaiz= nothing	
%>
<!--#include file="../common/fsal_2.asp"-->
</HTML>
