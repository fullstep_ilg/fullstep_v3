﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->



<script language="javascript" runat="SERVER">
/*
''' <summary>
''' Crear variables javascript a partir de la fechas del servidor
''' Fechas: Servidor /UTC segun reloj local /UTC segun reloj servidor
''' </summary>
''' <param name="pre">Prefijo de las variables</param>
''' <param name="anyo">año de fecha</param>
''' <param name="mes">mes de fecha</param>        
''' <param name="dia">dia de fecha</param>        
''' <param name="hora">hora de fecha</param>        
''' <param name="minuto">minuto de fecha</param>        
''' <param name="utcservidor">Diferencia en minutos entre fecha servidor y fecha utc</param>                
''' <returns>Variables fecha creadas</returns>
''' <remarks>Llamada desde: sistema; Tiempo máximo: 0</remarks>
*/
function scriptFecha(pre, anyo, mes, dia, hora, minuto,utcservidor)
{
var miFecha
var tzo
miFecha = new Date(anyo, mes-1, dia, hora, minuto)

tzo= miFecha.getTimezoneOffset ()

miUTCFecha = new Date(miFecha.getUTCFullYear() ,miFecha.getUTCMonth() ,miFecha.getUTCDate(), miFecha.getUTCHours(), miFecha.getMinutes())
miUTCFechaGMT1 = new Date(miFecha.getFullYear() ,miFecha.getMonth() ,miFecha.getDate(), miFecha.getHours(), miFecha.getMinutes() - utcservidor)

str="var " + pre + "vTzoServer \n"
str+="var " + pre + "vFechaServer \n"
str+="var " + pre + "vFechaUTC \n"
str+="var " + pre + "vFechaUTCGMT1 \n"
str+=" " + pre + "vTzoServer = " + tzo + "\n"
str+="  " + pre + "vFechaServer=new Date(" + miFecha.getFullYear() + "," + miFecha.getMonth() + "," + miFecha.getDate() + "," + miFecha.getHours() + "," + miFecha.getMinutes() + ")\n"
str+="  " + pre + "vFechaUTC=new Date(" + miUTCFecha.getFullYear() + "," + miUTCFecha.getMonth() + "," + miUTCFecha.getDate() + "," + miUTCFecha.getHours() + "," + miUTCFecha.getMinutes() + ")\n"
str+="  " + pre + "vFechaUTCGMT1=new Date(" + miUTCFechaGMT1.getFullYear() + "," + miUTCFechaGMT1.getMonth() + "," + miUTCFechaGMT1.getDate() + "," + miUTCFechaGMT1.getHours() + "," + miUTCFechaGMT1.getMinutes() + ")"

return(str)
}					
					
					
</script>

<html>
<head>
    <title><%=title%></title>
</head>
<%
''' <summary>
''' Muestra el resultado de las pujas realizadas. Las fechas deben ir en hora local.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: pujacerrada.asp; Tiempo máximo: 0</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
dim den

den = devolverTextos(Idioma,91)	

%>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script src="../common/formatos.js"></script>

<body topmargin="0" leftmargin="0" onload="Imprimir()">

<div name="divEspera" id="divEspera" style="position:absolute;top:15;left:0;visibility:visible;width:90%;height:90%;">
<table width="100%" height="100%">
	<tr>
		<td align="center" valign="center">
			<table>
				<tr>
					<td>
						<img SRC="<%=application("RUTASEGURA")%>script/images/cursor-espera_grande.gif">
					</td>
					<td>
						<span class="fmgrande"><%=den(25)%></span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>


<%


dim oRaiz


set oRaiz=validarUsuario(Idioma,true,false,0)

utcservidor= oraiz.UTCServidor	

Anyo=cint(Request("Anyo"))
Gmn1=Request("GMN1")
Proce=clng(Request("Proce"))
CiaComp = oRaiz.Sesion.CiaComp
CiaCod = oRaiz.Sesion.CiaCod
CodProve = oRaiz.Sesion.CiaCodGs

set oProceso = oRaiz.Generar_CProceso()
set oDestinos=oRaiz.Generar_cDestinos()	

oProceso.CargarProceso ciacomp, Idioma, anyo, gmn1, proce


set oOferta = oProceso.CargarOfertaProveedor(Idioma,ciacomp,codprove)
codMon = oOferta.codmon

decimalfmt = Request.Cookies ("USU_DECIMALFMT")
thousanfmt = Request.Cookies ("USU_THOUSANFMT") 
precisionfmt = CINT(Request.Cookies ("USU_PRECISIONFMT") )
datefmt = Request.Cookies ("USU_DATEFMT") 



Titulo = den(1)
Texto1 = den(2)
Texto2 = den(3)


%>


<script>
var vdatefmt
vdatefmt = "<%=datefmt%>"
function showPopUp(e, fServer, fUTC, fLocal)
{
str="<%=den(4)%> " + date2str(fServer,vdatefmt,true) + "\n"
str+="<%=den(5)%> " + date2str(fUTC,vdatefmt,true) + "\n"
str+="<%=den(6)%> " + date2str(fLocal,vdatefmt,true) 

e.title=str

}

function Imprimir()
{	
	document.getElementById("divEspera").style.visibility="hidden"
	window.print()
}

</script>

<br>
<br>
<br>
<table ALIGN="CENTER" WIDTH="90%">
	<tr>
		<td colspan="4" class="cabecera">
			<%=den(7)%>: <%=oProceso.anyo & "/" & oProceso.gmn1cod & "/" & oProceso.cod & " " & oProceso.den%>
		</td>
	</tr>
	<tr>
		<td class="cabecera">
			<%=den(8)%>
		</td>
		<td class="filaimpar"> 
			<script>
			<%=scriptFecha("vfa" & i, year(oProceso.FechaApertura), month(oProceso.FechaApertura), day(oProceso.FechaApertura),Hour(oProceso.FechaApertura), minute(oProceso.FechaApertura), utcservidor)%>

			var vfa<%=i%>FechaLocal

			vfa<%=i%>FechaLocal = new Date()
	
			otz = vfa<%=i%>FechaLocal.getTimezoneOffset()
			tzoaux=<%=utcservidor%>+otz
			
			vfa<%=i%>FechaLocal = new Date(vfa<%=i%>vFechaServer.getFullYear(), vfa<%=i%>vFechaServer.getMonth(), vfa<%=i%>vFechaServer.getDate(), vfa<%=i%>vFechaServer.getHours(), vfa<%=i%>vFechaServer.getMinutes() - tzoaux)

			p = date2str(vfa<%=i%>FechaLocal,vdatefmt,true)

				
			</script>
			<a href="javascript:void(null)" onmouseover="showPopUp(this,vfa<%=i%>vFechaServer,vfa<%=i%>vFechaUTCGMT1,vfa<%=i%>FechaLocal)" onclick="alert(this.title)"><script>document.write(p)</script></a><br>
		</td>
		<td class="cabecera">
			<%=den(9)%>
		</td>
		<td class="filaimpar">
			<script>
			<%=scriptFecha("v" & i, year(oProceso.FechaMinimoLimOfertas), month(oProceso.FechaMinimoLimOfertas), day(oProceso.FechaMinimoLimOfertas),Hour(oProceso.FechaMinimoLimOfertas), minute(oProceso.FechaMinimoLimOfertas), utcservidor)%>
			
			var v<%=i%>FechaLocal
			
			v<%=i%>FechaLocal = new Date()
			
			otz = v<%=i%>FechaLocal.getTimezoneOffset()
			tzoaux=<%=utcservidor%>+otz

			v<%=i%>FechaLocal = new Date(v<%=i%>vFechaServer.getFullYear(), v<%=i%>vFechaServer.getMonth(), v<%=i%>vFechaServer.getDate(), v<%=i%>vFechaServer.getHours(), v<%=i%>vFechaServer.getMinutes() - tzoaux)
			
			p = date2str(v<%=i%>FechaLocal,vdatefmt,true)			
			</script>
			<a href="javascript:void(null)" onmouseover="showPopUp(this,v<%=i%>vFechaServer,v<%=i%>vFechaUTCGMT1,v<%=i%>FechaLocal)" onclick="alert(this.title)"><script>document.write(p)</script></a><br>
		</td>
	</tr>
</table>		


<br>

<br>
<%
if oProceso.verProveedor then
	set ador = oProceso.CargarResultadoPuja(Idioma, ciacomp, codMon)
else
	set ador = oProceso.CargarResultadoPuja(Idioma, ciacomp, codMon, codprove)
end if
%>
<table align="center" width="90%">
<%


provenew = ador("prove").value
proveold = ""
fila = "filaPar"
while not ador.eof
	provenew = ador("prove").value
	if provenew <> proveold then%>
		<tr>
			<td colspan="6" class="<%if codProve=provenew then%>&quot;adjudicada&quot;<%else%>&quot;noadjudicada&quot;<%end if%>">
				<%if provenew = "#NOPROVE#" then%>
					<%=den(15)%>
				<%elseif provenew = "#NOADJ#" then%>
					<%=den(24)%>
				<%else%>
					<%=ador("proveden").value%>
				<%end if%>
			</td>
		</tr>
		<tr>
			<td class="cabecera">
				&nbsp;
			</td>
			<td class="cabecera">
				<%=den(10)%>
			</td>
			<td class="cabecera">
				<%=den(11)%>
			</td>
			<td class="cabecera">
				<%=den(12)%>
			</td>
			<td class="cabecera">
				<%=den(13)%>
			</td>
			<td class="cabecera">
				<%=den(14)%>
			</td>
		</tr>
	<%end if%>
		<tr>
			<td class="<%=fila%>">
				&nbsp;
			</td>
			<td class="<%=fila%>">
				<%=ador("ART").value%> - <%=ador("ARTDESCR").value%>
			</td>
			<td class="<%=fila%>" align="right">
				<%=visualizacionNumero(ador("CANT").value,decimalfmt,thousanfmt,precisionfmt)%>
			</td>
			<td class="<%=fila%>">
				<%=ador("UNI").value%> - <%=ador("UNIDEN").value%>
			</td>
			<td class="<%=fila%>">
				<%=ador("DESTINO").value%>
			</td>
			<td class="<%=fila%>" align="right">
				<%=visualizacionNumero(ador("PRECIO").value,decimalfmt,thousanfmt,precisionfmt)%> (<%=codMon%>)
			</td>
		</tr>
		


<%
	if oDestinos.item(ador("DESTINO").value) is nothing then
		set adorDest = oDestinos.CargarDatosDestino(Idioma,Ciacomp, ador("DESTINO").value)
		oDestinos.Add adorDest("COD").Value, adorDest("DEN").Value, false, adorDest("DIR"), adorDest("POB"), adorDest("CP"), adorDest("PROVI"), adorDest("PAI")
		adorDest.close
		set adorDest = nothing
	end if
	if fila = "filapar" then
		fila = "filaImpar"
	else
		fila = "filaPar"
	end if
	proveold=provenew
	ador.movenext
wend
ador.close
set ador = nothing

%>

</table>
<%

if oDestinos.count>0 then%>
<br>
<br>
<br>
<br>
	<table width="90%" align="center">
		<tr>
			<td colspan="7" class="cabecera"><%=den(23)%></td>
		</tr>
		<tr>
			<td class="cabecera"><%=den(16)%></td>
			<td class="cabecera"><%=den(17)%></td>
			<td class="cabecera"><%=den(18)%></td>
			<td class="cabecera"><%=den(19)%></td>
			<td class="cabecera"><%=den(20)%></td>
			<td class="cabecera"><%=den(21)%></td>
			<td class="cabecera"><%=den(22)%></td>
		</tr>

<%
fila = "filaImpar"
for each oDest in oDestinos	%>

		<tr>
			<td class="<%=fila%>"><%=oDest.cod%></td>
			<td class="<%=fila%>"><%=HTMLEncode(oDest.den)%></td>
			<td class="<%=fila%>"><%=HTMLEncode(oDest.dir)%></td>
			<td class="<%=fila%>"><%=oDest.cp%></td>
			<td class="<%=fila%>"><%=HTMLEncode(oDest.pob)%></td>
			<td class="<%=fila%>"><%=oDest.provi%></td>
			<td class="<%=fila%>"><%=oDest.pai%></td>
	</tr>

<%

next
end if
%>

</body>

</html>
<%
set oRaiz = nothing

%>