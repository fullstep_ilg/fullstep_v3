﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/fsal_1.asp"-->

<script language="javascript" runat="SERVER">

    /*
    ''' <summary>
    ''' Crear variables javascript a partir de la fechas del servidor
    ''' Fechas: Servidor /UTC segun reloj local /UTC segun reloj servidor
    ''' </summary>
    ''' <param name="pre">Prefijo de las variables</param>
    ''' <param name="anyo">año de fecha</param>
    ''' <param name="mes">mes de fecha</param>        
    ''' <param name="dia">dia de fecha</param>        
    ''' <param name="hora">hora de fecha</param>        
    ''' <param name="minuto">minuto de fecha</param>        
    ''' <param name="utcservidor">Diferencia en minutos entre fecha servidor y fecha utc</param>                
    ''' <returns>Variables fecha creadas</returns>
    ''' <remarks>Llamada desde: sistema; Tiempo máximo: 0</remarks>
    */
function scriptFecha(pre, anyo, mes, dia, hora, minuto, utcservidor) {
var miFecha
var tzo
miFecha = new Date(anyo, mes-1, dia, hora, minuto)
tzo = miFecha.getTimezoneOffset()

miFecha = new Date(anyo, mes - 1, dia, hora, (minuto - tzo))

var fechaServidor
var fechaUTC
var fechaCliente

fechaServidor = new Date(miFecha.getFullYear(), miFecha.getMonth(), miFecha.getDate(), miFecha.getHours(), miFecha.getMinutes() + tzo + utcservidor, miFecha.getSeconds())
fechaUTC = new Date(miFecha.getFullYear(), miFecha.getMonth(), miFecha.getDate(), miFecha.getHours(), miFecha.getMinutes() + tzo, miFecha.getSeconds())
fechaCliente = new Date(miFecha.getFullYear(), miFecha.getMonth(), miFecha.getDate(), miFecha.getHours(), miFecha.getMinutes(), miFecha.getSeconds())

str="var " + pre + "vTzoServer \n"
str+="var " + pre + "vFechaServer \n"
str+="var " + pre + "vFechaUTC \n"
str+="var " + pre + "vFechaUTCGMT1 \n"
str+=" " + pre + "vTzoServer = " + tzo + "\n"
str += "  " + pre + "vFechaServer=new Date(" + fechaServidor.getFullYear() + "," + fechaServidor.getMonth() + "," + fechaServidor.getDate() + "," + fechaServidor.getHours() + "," + fechaServidor.getMinutes() + ")\n"
str += "  " + pre + "vFechaUTC=new Date(" + fechaUTC.getFullYear() + "," + fechaUTC.getMonth() + "," + fechaUTC.getDate() + "," + fechaUTC.getHours() + "," + fechaUTC.getMinutes() + ")\n"
str += "  " + pre + "vFechaUTCGMT1=new Date(" + fechaCliente.getFullYear() + "," + fechaCliente.getMonth() + "," + fechaCliente.getDate() + "," + fechaCliente.getHours() + "," + fechaCliente.getMinutes() + ")"

return(str)
}					
					
					
</script>

<%

Idioma=Request.Cookies ("USU_IDIOMA")

''' <summary>
''' Muestra todas la ofertas/subastas a las que tiene acceso el proveedor. Las fechas hora de apertura/fin deben ir en hora local.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema; Tiempo máximo: 0</remarks>

Idioma=Request.Cookies ("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,true,true,0)

utcservidor= oraiz.UTCServidor	

CiaComp = Request("CiaComp")
CiaCod = Request("CiaCod")
CiaDen = Request("CiaDen")

Prove = clng(oRaiz.Sesion.CiaId)
	
set oProve = oraiz.DevolverProveedorEnCiaComp(Idioma,Prove,CiaComp)

CiaComp=server.HTMLEncode(CiaComp)
CiaCod=server.HTMLEncode(CiaCod)
CiaDen=server.HTMLEncode(CiaDen)
	
if not oProve is nothing then
	with oProve
		if isnull(.CodMon) then
			set oRS = oRaiz.ObtenerMonedaCentral(ciacomp)
			if not oRS is nothing then
				sCodMon = oRS.fields("MON_GS").value
				oRS.close
				set oRs = nothing
			else
				sCodMon = "EUR"
			end if
		else
			sCodMon = .CodMon
		end if
	end with
end if
	
set oProve = nothing 

%>
<script SRC="../common/menu.asp"></script>
<html>
<head>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta HTTP-EQUIV="REFRESH" CONTENT="60">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script src="../common/formatos.js"></script>
<script language="JavaScript" src="../common/ajax.js"></script>
<!--#include file="../common/fsal_3.asp"-->

<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{   
    if (<%=application("FSAL")%> == 1)
    {
        Ajax_FSALActualizar3(); //registro de acceso
    }
    document.getElementById('tablemenu').style.display = 'block';
}
</script>
		

</head>

<script>    dibujaMenu(3)</script>



<body bgColor="#ffffff" topmargin="0" leftmargin="0" onload="init()">




<%

datefmt=Request.Cookies("USU_DATEFMT")
	

CiaComp = clng(oRaiz.Sesion.CiaComp)
ProveCod = oRaiz.Sesion.CiaCodGs


	
dim den

den = devolverTextos(Idioma,53)	
%>

<script>
var vdatefmt
vdatefmt = "<%=datefmt%>"
function showPopUp(e, fServer, fUTC, fLocal)
{
str="<%=den(25)%> " + date2str(fServer,vdatefmt,true) + "\n"
str+="<%=den(26)%> " + date2str(fUTC,vdatefmt,true) + "\n"
str+="<%=den(27)%> " + date2str(fLocal,vdatefmt,true) 

e.title=str

}

function helpWindow(theUrl)
{

winHelp=window.open("<%=application("RUTASEGURA")%>custom/help/" + theUrl + "?Idioma=<%=Idioma%>" ,"popup","width=500,height=125,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no,top=0,left=0")

return true
}
/*
''' <summary>
''' No ofertar un proceso
''' </summary>
''' <param name="anyo">anyo de proceso</param>
''' <param name="gmn1">gmn1 de proceso</param>        
''' <param name="cod">cod de proceso</param>
''' <param name="i">identificador de q linea no se oferta para cambierle mas tarde el aaspecto</param>        
''' <remarks>Llamada desde: cada divnoOfertar_i de la pantalla ; Tiempo máximo: 0</remarks>*/
function noOfertar(anyo, gmn1, cod, i)
{
window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/noofertar.asp?i=" + i + "&anyo=" + anyo + "&gmn1=" + gmn1 + "&cod=" + cod, "_blank", "width=600,height=325,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no,top=0,left=0")
}

function detallePersona(cod)
{
window.open("<%=Application("RUTASEGURA")%>script/common/detallepersona.asp?Per=" + Var2Param(cod),"_blank", "top=150, left=60, width=350, height=160, resizable=yes,status=no,toolbar=no,menubar=no,location=no")
}



</script>


<h1><%=den(14) & "&nbsp;<font color=black>" & Request("CiaDen")%></font></h1>
<h3><%=Den (1)%></h3>
<h3><%=Den (28)%>&nbsp;<%=Den (32)%>&nbsp;<%=Den (31)%></h3>
<%
''' Listar los procesos
	set oProcesos = oraiz.DevolverProcesosPublicadosProveedor(CiaComp,ProveCod,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"))   	
	if oprocesos is nothing then%>
			
		<h3>
		<%=Den (2)%>
		</h3>
<p>&nbsp;</p>
		<%
		Response.End
		
	else
		if oProcesos.Count=0 then%>
			<h2>
			<%=Den (2)%>
			</h2>
<p>&nbsp;</p>
			<%Response.End
		End if
	end if
%>




<table class="principal" border="0" width="95%" cellspacing="2" cellpadding="1">

<%IF oProcesos.Normales > 0 THEN%>
	<tr>
		<td class="cabecera" colspan="7" align="center">
			<%=Den (10)%>
		</td>
	</tr>
	
	<tr>
		<td width="10%" class="cabecera">
			<a class="ayuda" href="javascript:void(null)" onclick="helpWindow('procesoc.asp')" title="<%=Den (15)%>"><%=Den (3)%></a>
		</td>
		
		<td colspan="2" width="40%" class="cabecera">
			<a class="ayuda" href="javascript:void(null)" onclick="helpWindow('denominacionc.asp')" title="<%=Den (16)%>"><%=Den (4)%></a>
		</td>

		<td width="10%" class="cabecera">
			<a class="ayuda" href="javascript:void(null)" onclick="helpWindow('contactoc.asp')" title="<%=Den (29)%>"><%=Den (29)%></a>
		</td>
		
		<td width="15%" class="cabecera">
			<a class="ayuda" href="javascript:void(null)" onclick="helpWindow('limitec.asp')" title="<%=Den (17)%>"><%=Den (5)%></a>
		</td>
		
		<td width="10%" class="cabecera">
			<a class="ayuda" href="javascript:void(null)" onclick="helpWindow('ofertadoc.asp')" title="<%=Den (18)%>"><%=Den (6)%></a>
		</td>
		
		<td width="10%" class="cabecera">
			<a class="ayuda" href="javascript:void(null)" onclick="helpWindow('objetivosc.asp')" title="<%=Den (19)%>"><%=Den (7)%></a>
		</td>

	</tr>
<%	
	''' Contenido
	Dim i 
	mclass="filaImpar"
	For i=1 to oProcesos.Normales

		set oproceso=oProcesos.Item(i)
		%>

		<tr>
			<td class="<%=mclass%>">
			    <a ID="option0" LANGUAGE="javascript" onmousemove="window.status='<%=oProceso.Anyo & "/" & oProceso.GMN1Cod & "/" & oProceso.Cod%>'" onmouseout="window.status=''" HREF="<%=application("RUTASEGURA")%>script/solicitudesoferta/solicitudesoferta21.asp?Idioma=<%=Idioma%>&amp;Cia=<%=Server.URLEncode(CiaComp)%>&amp;Anyo=<%=Server.URLEncode(oproceso.anyo)%>&amp;Gmn1=<%=Server.URLEncode(oproceso.gmn1cod)%>&amp;Cod=<%=Server.URLEncode(oproceso.cod)%>&amp;NumOBJ=<%=Server.URLEncode(oproceso.objetivosCont)%>"><%=oProceso.Anyo & "/" & oProceso.GMN1Cod & "/" & oProceso.Cod%></a>
			</td>
			<td colspan="2" class="<%=mclass%>">
				<a ID="option0" LANGUAGE="javascript" onmousemove="window.status='<%=oProceso.Anyo & "/" & oProceso.GMN1Cod & "/" & oProceso.Cod%>'" onmouseout="window.status=''" HREF="<%=application("RUTASEGURA")%>script/solicitudesoferta/solicitudesoferta21.asp?Idioma=<%=Idioma%>&amp;Cia=<%=Server.URLEncode(CiaComp)%>&amp;Anyo=<%=Server.URLEncode(oproceso.anyo)%>&amp;Gmn1=<%=Server.URLEncode(oproceso.gmn1cod)%>&amp;Cod=<%=Server.URLEncode(oproceso.cod)%>&amp;NumOBJ=<%=Server.URLEncode(oproceso.objetivosCont)%>"> <%=oProceso.Den%></a>			
			</td>
			<td class="<%=mclass%>">
				<%if not oProceso.com is nothing then%>
					<table width="100%">
						<tr>
							<td width="100%">
								<a href="javascript:void(null)" onclick="detallePersona(&quot;<%=oProceso.Com.Cod%>&quot;)">
									<%=HTMLEncode(oProceso.Com.Nombre)%>&nbsp;<%=HTMLEncode(oProceso.Com.Apellidos)%>
								</a>
							</td>
							<td>
								<a href="javascript:void(null)" onclick="detallePersona(&quot;<%=oProceso.Com.Cod%>&quot;)"><img border="0" SRC="../images/masinform.gif" WIDTH="11" HEIGHT="14"></a>
							</td>
						</tr>
					</table>
				<%else%>
					&nbsp;
				<%end if%>
			</td>
			<td class="<%=mclass%>" align="left">
				<%if not isnull(oProceso.FechaMinimoLimOfertas) then%>
					<script>
					<%=scriptFecha("vo" & i, year(oProceso.FechaMinimoLimOfertas), month(oProceso.FechaMinimoLimOfertas), day(oProceso.FechaMinimoLimOfertas),Hour(oProceso.FechaMinimoLimOfertas), minute(oProceso.FechaMinimoLimOfertas),utcservidor)%>
					var FechaLocal
					FechaLocal = new Date()
					otz = FechaLocal.getTimezoneOffset()
					//tzoaux=<%=utcservidor%>+otz				
					tzoaux=otz				
					FechaLocal = new Date(vo<%=i%>vFechaUTC.getFullYear(), vo<%=i%>vFechaUTC.getMonth(), vo<%=i%>vFechaUTC.getDate(), vo<%=i%>vFechaUTC.getHours(), vo<%=i%>vFechaUTC.getMinutes() - tzoaux)
								
					document.write(date2str(FechaLocal,vdatefmt,true))
                    

					</script>
				<%else
					Response.Write "&nbsp;"
				end if%>
			</td>
			
			<td class="<%=mclass%>" align="Center">
				<%If oProceso.Ofertado then%>
					<img border="0" id="imgNoofertar_<%=i%>" name="imgNoofertar_<%=i%>" src="images/tick.gif" WIDTH="17" HEIGHT="16">
				<%else%>
					<div name="divNoOfertar_<%=i%>" id="divNoOfertar_<%=i%>">
					<%if oProceso.NoOfe then%>
						<a href="javascript:void(null)" onclick="noOfertar(<%=JSNum(oproceso.anyo)%>,'<%=JSText(oproceso.gmn1cod)%>',<%=JSNum(oproceso.cod)%>,<%=i%>)">
							<img border="0" id="imgNoofertar_<%=i%>" name="imgNoofertar_<%=i%>" src="images/nooferta_on.gif" WIDTH="16" HEIGHT="16">
						</a>
					<%else%>
						<a href="javascript:void(null)" onclick="noOfertar(<%=JSNum(oproceso.anyo)%>,'<%=JSText(oproceso.gmn1cod)%>',<%=JSNum(oproceso.cod)%>,<%=i%>)">
							<%=Den (30)%>
						</a>
					<%end if%>
					</div>
				<%end if%>
			</td>
   	        
			<td class="<%=mclass%>" align="Center">
                <%
                if oproceso.objetivosPublicados then
                    IF oproceso.ObjetivosNuevos then
					    Response.Write Den (8)
				    elseif not oproceso.ObjetivosNuevos then
					    Response.Write Den (9)
				    end if
                end if
                %>
			</td>
		
		</tr><%
		
			contador = contador + 1
			if mclass="filaImpar" then
				mclass="filaPar"
			else
				mclass="filaImpar" 
			end if
		
	Next
END IF

'Si hay procesos en modo subasta añade la tabla para los procesos de subasta
if oProcesos.Subasta > 0 then%>
	<tr>
		<td class="cabecera" colspan="7" align="center">
			<%=Den (11)%>
		</td>
	</tr>
	<tr>
		<td class="cabecera">
			<a class="ayuda" href="javascript:void(null)" onclick="helpWindow('procesos.asp')" title="<%=Den (15)%>"><%=Den (3)%></a>
		</td>
	
		<td width="25%" class="cabecera">
			<a class="ayuda" href="javascript:void(null)" onclick="helpWindow('denominacions.asp')" title="<%=Den (16)%>"><%=Den (4)%></a>
		</td>

		<td width="10%" class="cabecera">
			<a class="ayuda" href="javascript:void(null)" onclick="helpWindow('contactoc.asp')" title="<%=Den (29)%>"><%=Den (29)%></a>
		</td>
		
		<td width="15%" class="cabecera">
			<a class="ayuda" href="javascript:void(null)" onclick="helpWindow('inicios.asp')" title="<%=Den (33)%>"><%=Den (21)%></a>
		</td>
		<td class="cabecera">
			<a class="ayuda" href="javascript:void(null)" onclick="helpWindow('limites.asp')" title="<%=Den (17)%>"><%=Den (22)%></a>
		</td>
	
		<td class="cabecera">
			<a class="ayuda" href="javascript:void(null)" onclick="helpWindow('ofertados.asp')" title="<%=Den (18)%>"><%=Den (6)%></a>
		</td>
	
		<td class="cabecera">
			<a class="ayuda" href="javascript:void(null)" onclick="helpWindow('psuperada.asp')" title="<%=Den (20)%>"><%=Den (13)%></a>
		</td>
	</tr>

<%
	mclass="filaImpar"
    dim bo
    dim onProceso
	For i = oProcesos.Normales + 1 to oProcesos.Count
	set oproceso=oProcesos.Item(i)
%>
	<tr>
		<td class="<%=mclass%>">
			<a ID="option0" LANGUAGE="javascript" onmousemove="window.status='<%=oProceso.Anyo & "/" & oProceso.GMN1Cod & "/" & oProceso.Cod%>'" onmouseout="window.status=''" HREF="solicitudesoferta21.asp?Idioma=<%=Idioma%>&amp;Cia=<%=Server.URLEncode(CiaComp)%>&amp;Anyo=<%=Server.URLEncode(oproceso.anyo)%>&amp;Gmn1=<%=Server.URLEncode(oproceso.gmn1cod)%>&amp;Cod=<%=Server.URLEncode(oproceso.cod)%>&amp;NumOBJ=<%=Server.URLEncode(oproceso.objetivosCont)%>"><%=oProceso.Anyo & "/" & oProceso.GMN1Cod & "/" & oProceso.Cod%></a>
		</td>
		
		<td class="<%=mclass%>">
			<a ID="option0" LANGUAGE="javascript" onmousemove="window.status='<%=oProceso.Anyo & "/" & oProceso.GMN1Cod & "/" & oProceso.Cod%>'" onmouseout="window.status=''" HREF="solicitudesoferta21.asp?Idioma=<%=Idioma%>&amp;Cia=<%=Server.URLEncode(CiaComp)%>&amp;Anyo=<%=Server.URLEncode(oproceso.anyo)%>&amp;Gmn1=<%=Server.URLEncode(oproceso.gmn1cod)%>&amp;Cod=<%=Server.URLEncode(oproceso.cod)%>&amp;NumOBJ=<%=Server.URLEncode(oproceso.objetivosCont)%>"><%=oProceso.Den%></a>		
		</td>
		<td class="<%=mclass%>">
			<%if not oProceso.com is nothing then%>
				<table width="100%">
					<tr>
						<td width="100%">
							<a href="javascript:void(null)" onclick="detallePersona(&quot;<%=oProceso.Com.Cod%>&quot;)">
								<%=oProceso.Com.Nombre%>&nbsp;<%=oProceso.Com.Apellidos%>
							</a>
						</td>
						<td>
							<a href="javascript:void(null)" onclick="detallePersona(&quot;<%=oProceso.Com.Cod%>&quot;)"><img border="0" SRC="../images/masinform.gif" WIDTH="11" HEIGHT="14"></a>
						</td>
					</tr>
				</table>
			<%else%>
				&nbsp;
			<%end if%>
		</td>
		
		<%
        if not isnull(oProceso.FechaApertura) then%>
			<script>
			<%=scriptFecha("vfa" & i, year(oProceso.FechaApertura), month(oProceso.FechaApertura), day(oProceso.FechaApertura),Hour(oProceso.FechaApertura), minute(oProceso.FechaApertura),utcservidor)%>

			var vfa<%=i%>FechaLocal

			vfa<%=i%>FechaLocal = new Date()
	
			otz = vfa<%=i%>FechaLocal.getTimezoneOffset()
			//tzoaux=<%=utcservidor%>+otz
            tzoaux=otz
			vfa<%=i%>FechaLocal = new Date(vfa<%=i%>vFechaUTC.getFullYear(), vfa<%=i%>vFechaUTC.getMonth(), vfa<%=i%>vFechaUTC.getDate(), vfa<%=i%>vFechaUTC.getHours(), vfa<%=i%>vFechaUTC.getMinutes() - tzoaux)

			p = date2str(vfa<%=i%>FechaLocal,vdatefmt,true)
			</script>
			<%
            
            
            miNow = oRaiz.ObtenerSQLUTCDate(CiaComp)
			if oProceso.FechaApertura<=miNow and iif(isnull(oProceso.FechaMinimoLimOfertas),miNow,oProceso.FechaMinimoLimOfertas)>=miNow then
                
                Set onProceso = oRaiz.Generar_CProceso
                bo = onProceso.cargarConfiguracion (CiaComp, oProceso.anyo, oProceso.gmn1COD, oProceso.cod)
                
                if onProceso.estadosubasta = 4 then  ' Detenida %>
				    <td class="<%=mclass%>" align="left">
					<a href="javascript:void(null)" onmouseover="showPopUp(this,vfa<%=i%>vFechaServer,vfa<%=i%>vFechaUTC,vfa<%=i%>FechaLocal)" onclick="alert(this.title)"><script>					                                                                                                                                                           document.write(p)</script></a><br>
					<%=den(34)%>

                <%else %>
				<td class="subAbierta" align="left">
					<a href="javascript:void(null)" onmouseover="showPopUp(this,vfa<%=i%>vFechaServer,vfa<%=i%>vFechaUTC,vfa<%=i%>FechaLocal)" onclick="alert(this.title)"><script>document.write(p)</script></a><br>
					<%=den(23)%>
                <%end if %>

			<%else%>
				<td class="<%=mclass%>" align="left">
					<a href="javascript:void(null)" onmouseover="showPopUp(this,vfa<%=i%>vFechaServer,vfa<%=i%>vFechaUTC,vfa<%=i%>FechaLocal)" onclick="alert(this.title)"><script>document.write(p)</script></a><br>
			<%end if%>				
			</td>
		<%else%>
			<td class="subAbierta" align="left">
				<%=den(23)%>
			</td>
				
		<%end if%>

		<%if not isnull(oProceso.FechaMinimoLimOfertas) then%>
			<script>
			<%=scriptFecha("v" & i, year(oProceso.FechaMinimoLimOfertas), month(oProceso.FechaMinimoLimOfertas), day(oProceso.FechaMinimoLimOfertas),Hour(oProceso.FechaMinimoLimOfertas), minute(oProceso.FechaMinimoLimOfertas),utcservidor)%>
			var v<%=i%>FechaLocal
			v<%=i%>FechaLocal = new Date()
			otz = v<%=i%>FechaLocal.getTimezoneOffset()
			//tzoaux=<%=utcservidor%>+otz
            tzoaux=otz
			v<%=i%>FechaLocal = new Date(v<%=i%>vFechaUTC.getFullYear(), v<%=i%>vFechaUTC.getMonth(), v<%=i%>vFechaUTC.getDate(), v<%=i%>vFechaUTC.getHours(), v<%=i%>vFechaUTC.getMinutes() - tzoaux)
			p = date2str(v<%=i%>FechaLocal,vdatefmt,true)

			</script>
			<%if oProceso.FechaMinimoLimOfertas>miNow then%>
				<td class="<%=mclass%>" align="left">
					<a href="javascript:void(null)" onmouseover="showPopUp(this,v<%=i%>vFechaServer,v<%=i%>vFechaUTC,v<%=i%>FechaLocal)" onclick="alert(this.title)"><script>document.write(p)</script></a><br>
			<%else%>
				<td class="subCerrada" align="left">
					<a href="javascript:void(null)" onmouseover="showPopUp(this,v<%=i%>vFechaServer,v<%=i%>vFechaUTC,v<%=i%>FechaLocal)" onclick="alert(this.title)"><script>document.write(p)</script></a><br>
					<%=den(24)%>
				<%end if%>
			</td>
		<%else%>
			<td class="<%=mclass%>" align="left">
				&nbsp;
			</td>
		<%end if%>
		
		<td class="<%=mclass%>" align="Center">
			<%If oProceso.Ofertado then%>
				<img border="0" id="imgNoofertar_<%=i%>" name="imgNoofertar_<%=i%>" src="images/tick.gif" WIDTH="17" HEIGHT="16">
			<%else
				if oProceso.NoOfe then%>
					<a href="javascript:void(null)" onclick="noOfertar(<%=JSNum(oproceso.anyo)%>,'<%=JSText(oproceso.gmn1cod)%>',<%=JSNum(oproceso.cod)%>,<%=i%>)">
						<img border="0" id="imgNoofertar_<%=i%>" name="imgNoofertar_<%=i%>" src="images/nooferta_on.gif" WIDTH="16" HEIGHT="16">
					</a>
				<%else%>
				
					<a href="javascript:void(null)" onclick="noOfertar(<%=JSNum(oproceso.anyo)%>,'<%=JSText(oproceso.gmn1cod)%>',<%=JSNum(oproceso.cod)%>,<%=i%>)">
						<%=Den (30)%>
					</a>
				<%end if%>
			<%end if%>
		</td>
		<td class="<%=mclass%>" align="Center">
			<%if oproceso.Superada then
				Response.Write Den (8)
			else
				Response.Write Den (9)
			end if%>
		</td>
			
	</tr>
	
	<%contador = contador + 1
	if mclass="filaImpar" then
		mclass="filaPar"
	else
		mclass="filaImpar" 
	end if
next
	
end if%>

</table>

<%
set oprocesos = nothing 
set ousuario = nothing 
set oraiz = nothing 
 %>
</body>
<!--#include file="../common/fsal_2.asp"-->
</html>
