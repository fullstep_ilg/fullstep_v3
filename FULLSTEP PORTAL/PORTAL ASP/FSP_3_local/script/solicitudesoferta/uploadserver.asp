﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<%
''' <summary>
''' Usando un objeto Persits.Upload graba en bbdd el archivo indicado
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\upload.asp ; Tiempo máximo: 0,2</remarks>
dim oRaiz

oldTimeOut = server.ScriptTimeout 
server.ScriptTimeout =10000

Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,true,false,0)

Set mySmartUpload = Server.CreateObject("Persits.Upload")
mySmartUpload.ProgressID = Request.QueryString("PID")

CiaComp = clng(oRaiz.Sesion.CiaComp)
	
set oError=oraiz.devolverMAXADJUN(cint(ciaComp),MAXADJUNGS,MAXADJUNP)

on error resume next

sExtensionIncorrecta = oRaiz.VerificarExtensionUpload_SinGrabar(Request.QueryString("NombreArchivo"))

if (sExtensionIncorrecta = "") then  
    mySmartUpload.Save Application ("CARPETAUPLOADS")

    sExtensionIncorrecta = oRaiz.VerificarExtensionUpload (mySmartUpload.form.item("NombreFichero"),Application ("CARPETAUPLOADS") )
end if

bTipoArchivoDistinto = false

if (not sExtensionIncorrecta = "") then    
    sExtensionIncorrectaAux = replace(sExtensionIncorrecta, "@@", "")

    if not (sExtensionIncorrectaAux = sExtensionIncorrecta) then
        bTipoArchivoDistinto = true    
        sExtensionIncorrecta=sExtensionIncorrectaAux    
    end if
else
    maxADJUN=MAXADJUNGS
    if MAXADJUNP<maxADJUN then
	    maxADJUN=MAXADJUNP
    end if

    kbActual = mySmartUpload.form.item("kbActual")
    kbActual = Numero(replace(kbActual,".",decimalfmt))

    MAXIMO = maxADJUN- kbActual
    if MAXIMO <=0 then
	    MAXIMO = 1
    END IF
	
    if err = 0 then
	    tehaspasao=false
	    grupo =  mySmartUpload.Form("Grupo")
	    item = mySmartUpload.Form("Item")


	    tehaspasao=true
	    For each file In mySmartUpload.Files
	    '  Only if the file exist
	    '  **********************
	           If Not file Is Nothing Then
			    iFileSize = file.originalSize
			    if maxADJUN-kbACTUAL >= ifilesize then
				    set oAdjun = oRaiz.Generar_CAdjunto
				    set oError = oAdjun.guardarAdjuntoPortal (CiaComp,  file.path)
				    idAdjun = oAdjun.AdjunIDPortal
				    ifilesize = oAdjun.datasize
				    set oAdjun = nothing
				    tehaspasao = false
			    else
				    tehaspasao=true
			    end if
		    end if

	    next
    else
	    ifilesize=mySmartUpload.TotalBytes 
	    tehaspasao = true
    end if

end if

server.ScriptTimeout =oldTimeOut

%>
<script>
p=window.opener
function quit()
	{
	var e
	try
	{
	p=window.opener
	p.winEspec = null
	}
	catch(e)
	{
	return true 
	}
	}	
</script>

<%
dim Cia

dim CiaUsu


Dim Den

den=devolverTextos(Idioma,59)

if (bTipoArchivoDistinto) then

%>

<html>
<title><%=title%></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<body  onunload="quit()">

<center>
    <SPAN class="fgrande error negrita">
        <%=Den (1)%>
    </span>
    <P>
    <span class ="fmedia negrita">
    <BR>
        <%=replace(Den (13), "###", sExtensionIncorrecta)%>
    </span>
</center>

<form id=form4 name=form1><center><input class=button type=button value=<%=den(3)%> onclick="window.close()"  id=button1 name=button0>
</body>

</html>

<%

elseif (not sExtensionIncorrecta = "") then

%>

<html>
<title><%=title%></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<body  onunload="quit()">

<center>
    <SPAN class="fgrande error negrita">
        <%=Den (1)%>
    </span>
    <P>
    <span class ="fmedia negrita">
    <BR>
        <%=replace(Den (12), "###", sExtensionIncorrecta)%>
    </span>
</center>

<form id=form3 name=form1><center><input class=button type=button value=<%=den(3)%> onclick="window.close()"  id=button0 name=button0>
</body>

</html>

<%

elseif tehaspasao then

%>


<html>
<title><%=title%></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<body  onunload="quit()">

<CENTER>
<SPAN class="fgrande error negrita">
<%=Den (1)%>
</span>
<P>
<span class ="fmedia negrita">
<BR>
<%=Den (2)%>
</span>
</center>
<BR>
<%=Den (6)%>&nbsp;<%=VisualizacionNumero(maxADJUN/1024,decimalfmt,thousandfmt,0)%>&nbsp Kb.
<BR>
<%=Den (7)%>&nbsp;<%=VisualizacionNumero(kbACTUAL/1024,decimalfmt,thousandfmt,0)%>&nbsp Kb.
<BR>
<%=Den (8)%>&nbsp;<%=VisualizacionNumero(iFileSize/1024,decimalfmt,thousandfmt,0)%>&nbsp Kb.

<form id=form2 name=form2><center><input class=button type=button value=<%=den(3)%> onclick="window.close()" >
</body>

</html>

<%
elseif ifilesize=0 then
	 

%>


<html>
<title><%=title%></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<style>
.tINPUT  {font-family:tahoma;font-weight:normal;font-size:10;width=50px;font-weight:bold;}

</style>
<body  onunload="quit()">

<CENTER>
<SPAN class="fgrande error negrita">
<%=Den (1)%>
</span>
<P>
<span class ="fmedia negrita">
<BR>
<%=Den (4)%>
</span>
<form id=form1 name=form1><center><input class=button type=button value=<%=den(3)%> onclick="window.close()" >
</body>

</html>

<%
	

else
%>


<html>
<script>
function init()
	{

	p.uploadRealizado(<%=iFileSize%>,<%=idAdjun%>)
	window.close()
	}
</script>

<body onload="init()" onunload="quit()">
</body>

</html>
<%end if%>


<%
set oRaiz = nothing

%>