﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->

<%
''' <summary>
''' Usando un objeto Persits.UploadProgress muestra el progreso de la grabación en bbdd del archivo indicado
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.asp ; Tiempo máximo: 0,2</remarks>

Idioma=Request("Idioma")

set oRaiz=validarUsuario(Idioma,false,false,0)

Dim Den

den=devolverTextos(Idioma,58)

strRepostURL = application("RUTASEGURA") & "script/solicitudesoferta/uploadserver.asp" 

%>

<%
Set UploadProgress = Server.CreateObject("Persits.UploadProgress")
PID = "PID=" & UploadProgress.CreateProgressID()
barref = Application("RUTASEGURA") & "script/solicitudesoferta/framebar.asp?to=10&" & PID
%> 


<HTML>
<title><%=title%></title>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script src="../common/formatos.js"></script>
<SCRIPT LANGUAGE="JavaScript">

//''' <summary>
//''' Mostrar la barra de progreso mientras se graba el adjunto
//''' </summary>
//''' <remarks>Llamada desde: validar() ; Tiempo máximo: 0,2</remarks>
function ShowProgress()
{
    strAppVersion = navigator.appVersion;
    if (document.frmUpload.txtAdjunto.value != "") {


        if (strAppVersion.indexOf('MSIE') != -1 && strAppVersion.substr(strAppVersion.indexOf('MSIE')+5,1) > 4)
        {
            winstyle = "dialogWidth=375px; dialogHeight:130px; center:yes";
            window.showModelessDialog('<% = barref %>&b=IE&NombreArchivo=' + Var2Param(document.forms["frmUpload"].NombreFichero.value), null, winstyle);
        }
        else
        {
            window.open('<% = barref %>&b=NN&NombreArchivo=' + Var2Param(document.forms["frmUpload"].NombreFichero.value), '', 'width=370,height=115', true);
        }

    }
    return true;
}
</SCRIPT> 

<script>
function validarKeyPress(e,l)
{
if (e.value.length>=l)
	return false
else
	return true
}

function validarLength(e,l)
{

if (e.value.length>l)
	{
	alert("<%=den(6)%>")
	return false
	}
else
	return true

}

//''' <summary>
//''' Comprueba q hayas indicado un archivo
//''' </summary>
//''' <remarks>Llamada desde: onsubmit ; Tiempo máximo: 0,2</remarks>
function validar()
{
if (document.forms["frmUpload"].txtAdjunto.value!="")
	{
	var sFile
	var sTFile
	p=window.opener
	sFile = document.forms["frmUpload"].txtAdjunto.value
	sTFile = sFile.split("\\")
	p.vTmpComentario = document.forms["frmUpload"].txtComentario.value
	p.vTmpNombre = sTFile[sTFile.length - 1]

	document.forms["frmUpload"].NombreFichero.value = sTFile[sTFile.length - 1]

	document.forms["frmUpload"].action = document.forms["frmUpload"].Ruta.value + '?' + document.forms["frmUpload"].MiPID.value + '&NombreArchivo=' + Var2Param(document.forms["frmUpload"].NombreFichero.value)


	ret = ShowProgress()
	return (ret)
	}
return false

}


</script>

<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">

</HEAD>
<BODY topmargin=10 >

<FORM id=frmUpload name=frmUpload enctype="multipart/form-data" method="POST" onsubmit="return validar()">
<input type=hidden name=kbActual value=<%=request("kbActual")%>>
<input type=hidden name=MaxAdjun value=<%=request("MaxAdjun")%>>
<input type="hidden" name="NombreFichero" value="" />
<input type="hidden" name="Ruta" value='<%=application("RUTASEGURA")%>script/solicitudesoferta/uploadserver.asp' /> 
<input type="hidden" name="MiPID" value='<%=PID%>' />

<table align=center width=80% cellspacing=1>
	<TR>
		<TD>
			<table width=100% >
				<tr>
					<td class="cabecera fgrande" colspan=2>
			            <%=den(1)%>
					</td>
				</tr>
				<tr>
					<td colspan=2 >
						<input type=file name=txtAdjunto size=48>
					</td>
				</tr>
				<tr>
					<td class=cabecera colspan=2 align=left>
			            <%=den(2)%>
					</td>
				</tr>
				<tr>
					<td colspan=2 >
						<TEXTAREA  cols=60 id=txtComentario name=txtComentario rows=4 width="100%" onkeypress="return validarKeyPress(this,500)" onchange="return validarLength(this,500)"></TEXTAREA>
					</td>
				</tr>
				<tr>
					<td class="centro">
						<input class=button type=submit value="<%=den(4)%>">
					</td>
					<td class="centro">
						<input class=button type=button value="<%=den(3)%>" onclick="window.close()">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</form>


</BODY>
</HTML>
