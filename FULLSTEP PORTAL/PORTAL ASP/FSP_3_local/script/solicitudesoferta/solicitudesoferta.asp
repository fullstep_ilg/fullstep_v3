﻿<%@ Language=VBScript %>
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/acceso.asp"-->
<%   
''' <summary>
''' Muestra las ofertas/subastas a las q tenga acceso el usuario
''' </summary>
''' <remarks>Llamada desde: common\menu.asp     common\introformatos.asp ; Tiempo máximo: 0,2</remarks>


dim mostrarFMT

Idioma = Request("Idioma")
Idioma = trim(Idioma)

set oRaiz=validarUsuario(Idioma,true,true,0)
	
	
DelProveedor = clng(oRaiz.Sesion.CiaId)
lCiaComp = clng(oRaiz.Sesion.CiaComp)

set paramgen = oRaiz.Sesion.Parametros

set oCias = oraiz.DevolverCompaniasCompradoras (Idioma,DelProveedor,cint(Application("PORTAL")),lCiaComp,Application("PYME"))
	
	
dim den

den = devolverTextos(Idioma,52)	

if paramGen.unaCompradora and not oCias is nothing then
	set oCia = oCias.item(1)
	ciaComp=ocia.id
	ciaCod = oCia.cod
	ciaDen = ocia.den
	set ocia = nothing
	set oCias = nothing
	set paramgen = nothing
	set oRaiz = nothing
    
	Response.Redirect Application ("RUTASEGURA") & "script/solicitudesoferta/solicitudesoferta11.asp?Idioma=" & Idioma & "&CiaComp=" & Server.URLEncode(ciaComp) & "&CiaCod=" & Server.URLEncode(ciaCod) & "&CiaDen=" & Server.URLEncode(ciaDen)
	Response.End 
end if
set paramgen =nothing
	 %>
<html>

<head>

<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta NAME="GENERATOR" Content="Microsoft FrontPage 4.0">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/menu.asp"></script>
<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
    document.getElementById('tablemenu').style.display = 'block';
}
</script>
		
</head>
<body bgcolor="#ffffff" topMargin=0 leftmargin=0 onload="init()">
<script>
dibujaMenu(3)
</script>
<h1><%=Den (2)%></h1>
<%if oCias is nothing then%>
<p class=Error><%= den(9) %></p>
<p>&nbsp;</p>

<%
else




%>



	<h3><% = Den(3) %></h3>
	<h3><% = Den (4) %></h3>
	<div align="center">
	<table border="0" width="70%" cellspacing="2" cellpadding="1">
	  <tr>
	    <th width="40%" class="cabecera"><% = Den (5) %></th>
	    <th width="20%" class="cabecera"><% = Den (6) %></th>
	    <th width="20%" class="cabecera"><% = Den (7) %></th>
	    <th width="20%" class="cabecera"><% = Den (8) %></th>
	  </tr>
	  <%
		mclass="filaImpar"
		if not oCias is nothing then              
			for each oCia in oCias
				
				if oCia.NumProceActivos > 0 then%>
				
					<tr>  
					<td class=<%=mclass%>>
						<A ID=option0 LANGUAGE=javascript onmousemove="window.status='<%=oCia.Den%>'" onmouseout="window.status=''" href="<%=application("RUTASEGURA")%>script/solicitudesoferta/solicitudesoferta11.asp?Idioma=<%=Idioma%>&CiaComp=<%=Server.URLEncode(ocia.id)%>&CiaCod=<%=Server.URLEncode(ocia.Cod)%>&CiaDen=<%=Server.URLEncode(ocia.den)%>">
						<%=oCia.den%>
						</A>
					</td>
					<td class=<%=mclass%> align="right">
						<%=ocia.numproceactivos%>
					</td>
					<td class=<%=mclass%> align="right">
						<%=ocia.numprocenuevos%>
					</td>
					<td class=<%=mclass%> align="right">
						<%=ocia.numprocearevisar%>
					</td>
				</tr>
					
				<%
				if mclass="filaImpar" then
					mclass="filaPar"
				else
					mclass="filaImpar" 
				end if
				end if
			next 
		end if
	  %>

	</table>
	</div>
<%end if%>
</body>
</html>
