﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/idioma.asp"-->
<HTML>
<HEAD>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script src="../common/formatos.js"></script>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<%
''' <summary>
''' Muestra el comentario del item completamente y lo deja modificar
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.asp ; Tiempo máximo: 0,2</remarks>

	Idioma = request.cookies("USU_IDIOMA")
	Indice=Request("Indice")
	Bloqueado = Request("Bloqueado")
	set oRaiz=validarUsuario(Idioma,false,false,0)
	

	dim den
	
	den = devolvertextos(idioma,86)

	sName=request("Target")
%>
</HEAD>
<BODY>

</HEAD>

<script>


function cerrar()
{
	var p
	var vi
		
	p=window.opener 


	p.guardarComentario("<%=sName%>",document.forms["frmComentario"].txtObserv.value)

	window.close()
}

	function cancelar()
		{
		window.close()
		}
		
	function init()
		{
			//Carga en el textarea el valor de las observaciones para ese pedido
			var p
			var f
			
			p=window.opener 
			
			f =p.document.forms["frmOfertaAEnviar"]
	
			document.forms["frmComentario"].txtObserv.value =f.elements["<%=sName%>"].value
		}
</script>
		
<BODY  onload="init()" rightmargin=2>

<form name=frmComentario>


<table width=100%>
	<tr>
		<td>
			<script>
				    document.write(textArea("txtObserv", "95%", 90,2000,"<%=JSText(den(4))%>",null,null,<%=Bloqueado%>))
			</script>
		</td>
	</tr>
	<tr>
		<td>
            <div style="text-align:center;">            
            <%if Bloqueado = 0 then%>
            	<input type="button" class="button" id="btnCerrar" value="<%=Den(2)%>" onclick="cerrar()"/>            
            <%end if%>		
			<input type="button" class="button" style="margin-left:1em;" id="btnCancelar" value="<%=Den(3)%>" onclick="cancelar()"/>
            </div>
		</td>
	</tr>
</table>


</form>
</BODY>

</HTML>
