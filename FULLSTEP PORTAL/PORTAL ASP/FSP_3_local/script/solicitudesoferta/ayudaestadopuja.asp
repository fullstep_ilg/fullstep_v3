﻿<%@ Language=VBScript %>
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/acceso.asp"-->
<%
''' <summary>
''' Muestra la ayuda de una subasta
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.js ; Tiempo máximo: 0,2</remarks> 

estado=request("estado")

dim den

Idioma = Request.Cookies("USU_IDIOMA")
den=devolverTextos(Idioma,117 )




%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><%=title%></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
</head>
<body>

<%
select case estado
	case 2:
%>
<p class="OFESINENVIAR"><%=den(2)%>
<hr></p>
<p><%=den(6)%></p>
<%
	case 3,5:
%>
<p class="OFEENVIADA"><%=den(3)%><%if estado=5 then%><span class="fmedia"><%=den(5)%></span><%end if%>
<hr></p>
<%if estado = 3 then%>
	<p><%=den(11)%></p>
<%else%>
	<p><%=den(14)%></p>
<%end if%>
<%
	case 4:
%>
<p class="OFESINGUARDAR"><%=den(4)%><hr></p>
<p><%=den(13)%></p>
<%
end select
%>


</body>
</html>
