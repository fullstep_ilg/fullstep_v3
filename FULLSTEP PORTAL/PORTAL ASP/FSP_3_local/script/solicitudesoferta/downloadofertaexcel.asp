﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/fsal_7.asp"-->
<HTML>
<script language="JavaScript" src="../common/ajax.js"></script>
<script src="../common/formatos.js"></script>
<!--#include file="../common/fsal_3.asp"-->
<script>
function init()
{
    if (<%=Application("FSAL")%> == 1)
    {
        Ajax_FSALActualizar3(); //registro de acceso
    }
}
</script>
<BODY onload="init()">

<%

''' <summary>
''' descarga un excel con la oferta
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.as ; Tiempo máximo: 0,2</remarks>
Public Function ValidFilename(ByVal sFileName ) 
Dim s 

s = sFileName

s = Replace(s, "\", "_")
s = Replace(s, "/", "_")
s = Replace(s, ":", "_")
s = Replace(s, "*", "_")
s = Replace(s, "?", "_")
s = Replace(s, """", "_")
s = Replace(s, ">", "_")
s = Replace(s, "<", "_")
s = Replace(s, "|", "_")
s = Replace(s, "@", "_")
s = Replace(s, "#", "_")

ValidFilename = s


End Function


Idioma = Request.Cookies ("USU_IDIOMA")


set oRaiz=validarUsuario(Idioma,true,false,0)

lCiaComp = clng(oRaiz.Sesion.CiaComp)

anyo = request.Cookies ("PROCE_ANYO")
gmn1 = request.Cookies ("PROCE_GMN1")
proce = request.Cookies ("PROCE_COD")

dim proveCia
proveCia = oRaiz.Sesion.CiaCodGs
if Application("PYME") <> "" then
	If InStr(1, proveCia, Application("PYME")) = 1 Then
		prove = Replace(proveCia, Application("PYME"), "", 1, 1)
	else
		prove = proveCia
	End If
else
	prove = proveCia
end if


		
OldTimeout=Server.ScriptTimeout	

Server.ScriptTimeout=3600


strFECHAHORA=cstr(year(now())) & cstr(month(now())) & cstr(day(now())) & cstr(hour(now())) & cstr(minute(now())) & cstr(second(now()))

miFilename = ValidFilename(anyo & "_" & gmn1 & "_" & proce  & "_" & prove & ".xls")

s=""
lowerbound=65
upperbound=90
randomize
for i = 1 to 10
	s= s & chr(Int((upperbound - lowerbound + 1) * Rnd + lowerbound))
next	


sPath = Application ("CARPETAUPLOADS") & "\" & s & "\"
sPyme = Application ("PYME")

''' <summary>
''' GenerarOfertaExcel ahora va con las diferencias horarias entre servidor y utc y entre local y utc
''' </summary>      
''' <returns>nada</returns>
''' <remarks>Llamada desde: sistema; Tiempo máximo:0</remarks>

oRaiz.GenerarOfertaExcel lCiaComp, anyo,gmn1,proce,prove,sPath & mifilename,idioma,sPyme,request("DifUtc"),request("DifServ")

datasize = oRaiz.DataSizeFile(spath & mifilename)

%>
<title><%=title%></title>
<!--#include file="../common/fsal_8.asp"-->
<script>

var p

p = window.parent.frames["fraOfertaClient"]
p.winEspera.close()



window.open("<%=application("RUTASEGURA")%>script/common/download.asp?path=<%=s%>&nombre=<%=server.urlencode(mifilename)%>&datasize=<%=server.urlencode(datasize)%>","_blank","top=100,left=150,width=630,height=300,location=no,menubar=yes,resizable=yes,scrollbars=no,toolbar=no")

</script>
</BODY>
</HTML>
<%
set oRaiz = nothing
Response.End
%>
