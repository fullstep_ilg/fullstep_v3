﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/fsal_1.asp"-->

<%
''' <summary>
''' Controlar los obligatorios antes de confirmar oferta. 
''' Si todo esta bien se da opción a confirmar sino todo esta bien no da opción a confirmar oferta. En ambos casos muestra por pantalla los datos 
''' introducidos, bien los q se van a confirmar o bien los q faltan.
''' </summary>     
''' <remarks>Llamada desde: GuardarOferta.asp ; Tiempo máximo: 0</remarks>
OldTimeOut = server.ScriptTimeout
server.ScriptTimeout = 3600
'Recogida de las rutas de los ficheros
rutHtml=Request.QueryString("RutHtml")
rutPdf=Request.QueryString("RutPdf")
Set oFsoLocal = Server.CreateObject("Scripting.FileSystemObject")
existeHtml = true
existeHtml = oFsoLocal.FileExists(Replace(rutHtml,"\","\\"))
'Si existe el fichero html hay que esperar. Se hace una espera pasiva mediante recarga de la página cada cierto
'tiempo. No es ideal, pero por ahora es la única forma que se ha encontrado, ya que parece que aquí no se pueden
'construir (ni obtener) ni el objeto WScript ni sus descendientes.
If existeHtml Then
	%>
	<html>
	<head>
        <script src="../common/formatos.js"></script>
        <script type="text/javascript" src="<%=application("RUTASEGURA")%>script/js/jquery.min.js"></script>
        <script language="JavaScript" src="../common/ajax.js"></script>
        <!--#include file="../common/fsal_3.asp"-->
    </head>
	<body>
	<script>
		window.setTimeout(funB,3000)
		function funB()
		{
			window.open("confirmoferta2.asp?codMon=<%=Request.QueryString("codMon")%>&NumOfe=<%=Request.QueryString("NumOfe")%>&IDPortal=<%=Request.QueryString("IDPortal")%>&NumObj=<%=Request.QueryString("NumObj")%>&RutHtml=<%=Replace(Request.QueryString("RutHtml"),"\","\\")%>&RutPdf=<%=Replace(Request.QueryString("RutPdf"),"\","\\")%>","fraCOConfirmacion")
		}
	</script>

    <script>
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
    function init()
	    {
	    if (<%=application("FSAL")%> == 1)
        {
	        Ajax_FSALActualizar3(); //registro de acceso
	    }
	    top.winEspera.close()	
	    window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/solicitudesoferta21.asp?Idioma=<%=Idioma%>&Cia=<%=Server.URLEncode(CiaComp)%>&Anyo=<%=Server.URLEncode(anyo)%>&Gmn1=<%=Server.URLEncode(gmn1)%>&Cod=<%=Server.URLEncode(proce)%>&Mensaje=<%=Server.URLEncode(Mensaje)%>","default_main")	
	    }
    </script>

	</body>
	</html>
	<%
	Response.End
End if
existePdf = true
existePdf = oFsoLocal.FileExists(Replace(rutPdf,"\","\\"))
anyoproceso=Request.Cookies ("PROCE_ANYO")
gmn1proceso=Request.Cookies ("PROCE_GMN1") 
codproceso=Request.Cookies ("PROCE_COD") 
Idioma = Request.cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,true,false,0)		

'Obtiene los datos de la oferta
CiaComp = clng(oRaiz.Sesion.CiaComp)
CodProve = oRaiz.Sesion.CiaCodGs

Set oProceso = oRaiz.Generar_CProceso
oProceso.cargarConfiguracion CiaComp, anyoproceso, gmn1proceso, codproceso, CodProve
oProceso.cargarProceso CiaComp, Idioma, anyoproceso, gmn1proceso, codproceso

porFecha=0

NumOfe = request("NumOfe")

set oOferta = oProceso.CargarOfertaProveedor(Idioma,ciacomp,codprove,NumOfe)
set oOferta.proceso = oProceso
oOferta.IDPortal = Request.Cookies("PROCE_IDPORTAL")
'Cargar ruta en oOferta
oOferta.RutaAdjunFirmaDig = rutPdf

'Si no existe el fichero PDF se debe guardar la oferta pero no enviarla, así que se navega a la página
'ofertaenviada con el valor de error que muestre que se ha guardado la oferta pero no se ha enviado.
'existePdf=false
If not existePdf Then
		%><script>
			window.parent.opener.open("ofertaenviada.asp?error=1000&NumObj=<%=request("NumObj")%>","default_main")
			window.parent.close()
		</script><%
		Response.End
End if
CodPortal = cstr(Application ("NOMPORTAL"))
set TESError = oOferta.Enviar(Idioma,ciaComp, Application("PYME"), false, CodPortal)

if oProceso.EnvioAsincrono=0 then
	sePudoEnviar = oOferta.ErrorEnvio
	if oOferta.errorEnvio = 0 and TESError.NumError<>0 then
		sePudoEnviar = TESError.NumError
	end if

	If TESError.NumError <> 0 or oOferta.errorEnvio>=100 Then
		set oProceso = nothing
		set oOferta = nothing	
            
		set oRaiz = nothing
		%><script>
			window.parent.opener.open("ofertaenviada.asp?error=<%=sePudoEnviar%>&NumObj=<%=request("NumObj")%>","default_main")
			window.parent.close()
		</script><%
		Response.End
	End If

else
	
	sePudoEnviar = 50
end if
	
set oOferta = nothing
set oProceso = nothing
set oRaiz = nothing
    
	%>
	<script>
		window.parent.opener.open("ofertaenviada.asp?error=<%=sePudoEnviar%>&NumObj=<%=request("NumObj")%>","default_main")
		window.parent.close()
	</script>
<!--#include file="../common/fsal_2.asp"-->
	<%
Response.End
%>
<title><%=title%></title>