﻿<%@ Language=VBScript %>
<!--#include file="../common/colores.asp"-->
<%
''' <summary>
''' Dibuja la barra de progreso
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.asp ; Tiempo máximo: 0,2</remarks> 
%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><%=title%></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
</HEAD>
<BODY topmargin=0 leftmargin=0 bottommargin=0 rightmargin=0>
<div name=divProgreso id=divProgreso style='position:absolute;top=35;left=5;border:1 solid <%=cBORDEPROGRESO%>;'>

</div>
</BODY>
</HTML>
