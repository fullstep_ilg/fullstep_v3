﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<%
''' <summary>
''' Descarga una especificacion
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\downloadespec.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request("Idioma")
Idioma = trim(Idioma)


set oRaiz=validarUsuario(Idioma,true,false,0)

		
lCiaComp = clng(oRaiz.Sesion.CiaComp)
CodProve = oRaiz.Sesion.CiaCodGs

anyo = Request("Anyo")
gmn1 = Request("Gmn1")
proce = Request("Proce")
grupo = request("Grupo")
item = request("Item")
espec = Request("espec")
PathDeNombre = Request("Path")
		
OldTimeout=Server.ScriptTimeout	

Server.ScriptTimeout=3600

if item<>"" then		
	Set oEspecificacion = oRaiz.DevolverEspecificacion(lCiaComp,Anyo,Gmn1,Proce,espec,grupo,item)
	sPath = Application ("CARPETAUPLOADS") & "\" & PathDeNombre & "\" & anyo & "_" & gmn1 & "_" & proce & "_" & grupo & "_" & item & "\"
	svPath = PathDeNombre & "/" & anyo & "_" & gmn1 & "_" & proce & "_" & grupo & "_" & item
else
	if grupo <>"" then
		Set oEspecificacion = oRaiz.DevolverEspecificacion(lCiaComp,Anyo,Gmn1,Proce,espec,grupo)
		sPath = Application ("CARPETAUPLOADS") & "\" & PathDeNombre & "\" & anyo & "_" & gmn1 & "_" & proce & "_" & grupo & "\"
		svPath = PathDeNombre & "/" & anyo & "_" & gmn1 & "_" & proce & "_" & grupo
	else
		Set oEspecificacion = oRaiz.DevolverEspecificacion(lCiaComp,Anyo,Gmn1,Proce,espec)
		sPath = Application ("CARPETAUPLOADS") & "\" & PathDeNombre & "\" & anyo & "_" & gmn1 & "_" & proce & "\"
		svPath = PathDeNombre & "/" & anyo & "_" & gmn1 & "_" & proce
	end if
end if
			
if oespecificacion is nothing then	 
	set oraiz = nothing
    
	Response.End 
end if

set oError = oEspecificacion.escribirADisco(lCiaComp,sPath, oEspecificacion.Nombre)

%>
<script>
    window.open("<%=application("RUTASEGURA")%>script/common/download.asp?path=<%=server.urlencode(svPath)%>&nombre=<%=server.urlencode(oERror.arg1)%>&datasize=<%=server.urlencode(oEspecificacion.datasize)%>", "_blank", "top=100,left=150,width=630,height=300,location=no,menubar=yes,resizable=yes,scrollbars=no,toolbar=no")
</script>

<html>
<head>
<title><%=title%></title>
<script>
function Init()
	{
	top.winEspera.close()
    }
</script>
</head>
<body onload="Init()">
</body>
</html>

<%
Response.End
%>