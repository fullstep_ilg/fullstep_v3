﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">

<%
''' <summary>
''' Descarga todas las especificaciones de la oferta
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\descargaespecific.asp ; Tiempo máximo: 0,2</remarks> 

dim den
Idioma = Request.Cookies("USU_IDIOMA")
den = devolverTextos(Idioma,103)

Idioma = Request.Cookies("USU_IDIOMA")

denproceso=Request("denproceso")
anyoproceso=Request.Cookies ("PROCE_ANYO") 
gmn1proceso=Request.Cookies ("PROCE_GMN1") 
codproceso=Request.Cookies ("PROCE_COD") 

%>

<title><%=title%></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script src="../common/formatos.js"></script>
</HEAD>

<script>
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/descargaEspecificServer.asp","fraEspecServer")
 
}

function cerrar()
{
window.parent.close()
}

</script>

<BODY onload = init()>

<div name=divLiteral id=divLiteral>
  <table width=100% height=90%>
	<tr><td class="cabApartado"><%=den(3) & "&nbsp;" & anyoproceso & "/" & gmn1proceso & "/" & codproceso & "&nbsp;" & denproceso%></td></tr>
	<tr height="20%"><td><%=den(1)%></td></tr>
	<tr height="40%"><td><%=den(2)%></td></tr>
	<tr height="60%"><td align="center">
		<img name="imgDerecha" border="0" SRC="images/Book-09.GIF" WIDTH="80" HEIGHT="80"></td>
	</tr>
	<tr height="25%"><td> </td></tr>
  </table>
</div>


<div name=divArchivo id=divArchivo>
</div>


</body>

</HTML>
