﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<%
''' <summary>
''' Descarga una especificacion
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\js\especificacion.asp ; Tiempo máximo: 0,2</remarks>

dim den

Idioma = Request.Cookies("USU_IDIOMA")
den=devolverTextos(Idioma,54 )

set oRaiz=validarUsuario(Idioma,true,false,0)

		
lCiaComp = clng(oRaiz.Sesion.CiaComp)
CodProve = oRaiz.Sesion.CiaCodGs

anyo = Request("Anyo")
gmn1 = Request("Gmn1")
proce = Request("Proce")
grupo = request("Grupo")
item = request("Item")
espec = Request("espec")

s=""
lowerbound=65
upperbound=90
randomize
for i = 1 to 10
	s= s & chr(Int((upperbound - lowerbound + 1) * Rnd + lowerbound))
next	


if item<>"" then		
	Set oEspecificacion = oRaiz.DevolverEspecificacion(lCiaComp,Anyo,Gmn1,Proce,espec,grupo,item)
	sPath = Application ("CARPETAUPLOADS") & "\" & s & "\" & anyo & "_" & gmn1 & "_" & proce & "_" & grupo & "_" & item & "\"
	svPath = s & "/" & anyo & "_" & gmn1 & "_" & proce & "_" & grupo & "_" & item
else
	if grupo <>"" then
		Set oEspecificacion = oRaiz.DevolverEspecificacion(lCiaComp,Anyo,Gmn1,Proce,espec,grupo)
		sPath = Application ("CARPETAUPLOADS") & "\" & s & "\" & anyo & "_" & gmn1 & "_" & proce & "_" & grupo & "\"
		svPath = s & "/" & anyo & "_" & gmn1 & "_" & proce & "_" & grupo
	else
		Set oEspecificacion = oRaiz.DevolverEspecificacion(lCiaComp,Anyo,Gmn1,Proce,espec)
		sPath = Application ("CARPETAUPLOADS") & "\" & s & "\" & anyo & "_" & gmn1 & "_" & proce & "\"
		svPath = s & "/" & anyo & "_" & gmn1 & "_" & proce
	end if
end if
			
if oespecificacion is nothing then
	 
	set oraiz = nothing
	Response.End 
end if

sQry= Request.QueryString ()
sQry = sQry + "&Path=" + s

%>
<title><%=title%></title>
<script>
    var winEspera
    /*
    ''' <summary>
    ''' Iniciar la carga
    ''' </summary>   
    ''' <remarks>Llamada desde: solicitudesoferta\downloadespec.asp ; Tiempo máximo: 0</remarks>*/
    function CargandoArchivo() {
        top.winEspera = window.open("<%=application("RUTASEGURA")%>script/common/winEspera.asp?msg=<%=server.urlEncode(den(144))%>&continua=continuarCargandoArchivo", "_blank", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
    }
    /*
    ''' <summary>
    ''' Continuar la carga asincronamente
    ''' </summary>    
    ''' <remarks>Llamada desde: solicitudesoferta\downloadespec.asp ; Tiempo máximo: 0</remarks>*/
    function continuarCargandoArchivo()
    {
        window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/downloadespec2.asp?<%=sQry%>", "fraOfertaServer")
        top.winEspera.focus()
    }  
    
    CargandoArchivo()  
</script>

<%
Response.End

set oRaiz = nothing
%>
