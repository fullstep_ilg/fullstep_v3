﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<% 
''' <summary>
''' Muestra el Historico de Aprobaciones
''' </summary>
''' <remarks>Llamada desde: registro\compania.asp ; Tiempo máximo: 0,2</remarks>

	Idioma = request.cookies("USU_IDIOMA")
	set oRaiz=validarUsuario(Idioma,true,false,0)
	dim Den
	Den=devolverTextos(idioma,16)
	SeparadorDecimal = Request.Cookies("USU_DECIMALFMT")
	SeparadorMiles = Request.Cookies ("USU_THOUSANFMT")
	Precision = Request.cookies("USU_PRECISIONFMT")
	FormatoFecha=Request.Cookies ("USU_DATEFMT")
	LineaId = request("LineaId")
	
    
	set oLinea = oRaiz.Generar_CLinea
	oLinea.Id = LineaId
	set ador = oLinea.DevolverHistoricoAprobaciones
	
	
	
	'visualizacionNumero(num,".",",",2)
	'visualizacionFecha(fecha,"mm/dd/yyyy"
	
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<title><%=title%></title>
</head>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">

<script>
</script>

<body>
<table>
<tr>
<td class="titulo negrita subrallado">
<%=Den(1)%>
</td></tr>
</table>

<% if ador is nothing then
				
	else%>

	<table cellSpacing="1" cellPadding="1" width="100%" border="0">
  
	<tr>
    <td class="cabecera"><%=Den(2)%></td>
    <td class="cabecera"><%=Den(3)%></td>
    <td class="cabecera"><%=Den(4)%></td>
    <td class="cabecera"><%=Den(5)%></td></tr>
 	<%	
		while not ador.EOF
			%><tr>
			<td ><%= visualizacionfecha(ador("FECHA").value,FormatoFecha)%></td>
			<td ><%
			select case ador("EST").value
			case 1,2
					sEst = DEn(6)
			case 20
					sEst = Den(7)
			end select
			Response.Write sEst
			%></td>
			<td ><%= ador("PERSONA").value %></td>
			<td ><%= ador("COMENT").value %></td></tr>
			<% ador.movenext
		wend
		ador.close
		set ador = nothing
			
	end if
%>
  </table></p>

</body>
</html>
