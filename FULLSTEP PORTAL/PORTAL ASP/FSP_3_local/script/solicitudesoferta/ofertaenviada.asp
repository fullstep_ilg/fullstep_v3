﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<html>
<head>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/menu.asp"></script>

<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
    document.getElementById('tablemenu').style.display = 'block';
}
</script>
</head>
<body topMargin="0" leftmargin="0" onload="init()">


<% 
''' <summary>
''' Informa de q se ha enviado correctamente la oferta
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\guardaroferta.asp        solicitudesoferta\confirmoferta.asp ; Tiempo máximo: 0,2</remarks>

	Idioma = Request.Cookies("USU_IDIOMA")

    set oRaiz=validarUsuario(Idioma,true,false,0)

	CiaComp = oRaiz.Sesion.CiaComp
	CiaCod = oRaiz.Sesion.CiaCod
	anyoproce = Cint(Request.Cookies ("PROCE_ANYO") )
	gmn1proce = Request.Cookies ("PROCE_GMN1")
	codproce = Request.Cookies ("PROCE_COD")
	
	error = cint(Request("Error"))
	NumObj = request("NumObj")

	Dim Den
	
	den = devolverTextos(Idioma,89)

%>

<%if request("popup")<>"1" then%>
<script>    dibujaMenu(3)</script>
<%end if%>

<%if error=0 then%>
<h1>
<%=Den (2) & "&nbsp" & "<font color=black>" & anyoproce & "/" & GMN1proce & "/" & codproce & "</font>"%>
</h1>

<h2><%=Den(1)%></h2>
<center>
<a HREF="<%=application("RUTASEGURA")%>script/solicitudesoferta/solicitudesoferta11.asp?Idioma=<%=Idioma%>&amp;Ciacod=<%=CiaCod%>&amp;CiaComp=<%=CiaComp%>"><%=Den (3)%></a>
</center>

<%elseif error=10 then%>

<h1>
<%=Den (2) & "&nbsp" & "<font color=black>" & anyoproce & "/" & GMN1proce & "/" & codproce & "</font>"%>
</h1>

<p><%=Den(8)%></p>
<p><nobr><img SRC="images/cursor-espera_grande.gif" WIDTH="31" HEIGHT="28"><%=Den(9)%></nobr></p>
<%elseif error=50 then%>

<h1>
<%=Den (2) & "&nbsp" & "<font color=black>" & anyoproce & "/" & GMN1proce & "/" & codproce & "</font>"%>
</h1>
<p ><%=Den(13)%></p>
<p ><%=Den(14)%></p>

<p ><%=Den(12)%> <a style="font-size:12px" href="mailto:<%=Application("MAILPORTAL")%>"><%=Application("MAILPORTAL")%></a></p>

<center>
<a HREF="<%=application("RUTASEGURA")%>script/solicitudesoferta/solicitudesoferta11.asp?Idioma=<%=Idioma%>&amp;Ciacod=<%=CiaCod%>&amp;CiaComp=<%=CiaComp%>"><%=Den (3)%></a>
</center>

<%elseif error=1000 then%>
<h1>
<%=Den (2) & "&nbsp" & "<font color=black>" & anyoproce & "/" & GMN1proce & "/" & codproce & "</font>"%>
</h1>
<h2><%=Den(4)%></h2>

<%elseif error>=100 then%>
<h1>
<%=Den (2) & "&nbsp" & "<font color=black>" & anyoproce & "/" & GMN1proce & "/" & codproce & "</font>"%>
</h1>

<p class="error"><%=Den(4)%></p>

<p ><%=Den(11)%></p>

<p ><%=Den(12)%> <a style="font-size:12px" href="mailto:<%=Application("MAILPORTAL")%>"><%=Application("MAILPORTAL")%></a></p>
<%elseif error=40 then%>
<h1>
<%=Den (2) & "&nbsp" & "<font color=black>" & anyoproce & "/" & GMN1proce & "/" & codproce & "</font>"%>
</h1>

<h2><%=Den(15)%></h2>
<center>
<a HREF="<%=application("RUTASEGURA")%>script/solicitudesoferta/solicitudesoferta11.asp?Idioma=<%=Idioma%>&amp;Ciacod=<%=CiaCod%>&amp;CiaComp=<%=CiaComp%>"><%=Den (3)%></a>
</center>

<%else%>

<h1>
<%=Den (2) & "&nbsp" & "<font color=black>" & anyoproce & "/" & GMN1proce & "/" & codproce & "</font>"%>
</h1>

<p class="error"><%=Den(4)%></p>
<p><%=Den(5)%></p>
<p><%=Den(6)%></p>


<center>

<%if request("popup")="1" then%>
	<a HREF="javascript:window.close()"><%=Den (10)%></a>
<%else%>
	<a HREF="<%=application("RUTASEGURA")%>script/solicitudesoferta/solicitudesoferta21.asp?Idioma=<%=Idioma%>&amp;Cia=<%=CiaComp%>&amp;Anyo=<%=anyoproce%>&amp;Gmn1=<%=GMN1Proce%>&amp;Cod=<%=codProce%>&amp;NumOBJ=<%=NumObj%>"><%=Den (7)%></a>
<%end if%>
</center>

<%end if%>

</body>
</html>
