﻿<%@ Language=VBScript %>
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/acceso.asp"-->
<%
''' <summary>
''' Muestra la ayuda de un proceso no de subasta
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.js ; Tiempo máximo: 0,2</remarks> 

estado=request("estado")

dim den

Idioma = Request.Cookies("USU_IDIOMA")
den=devolverTextos(Idioma,117 )




%>
<html>
<head>
    <title><%=title%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp"/>
</head>
<body>

<%
select case estado
	case 2:
%>
<p class="OFESINENVIAR"><%=den(2)%>
<hr></p>
<p><%=den(6)%></p>
<%
	case 3,5:
%>
<p class="OFEENVIADA"><%=den(3)%><%if estado=5 then%><span class="fmedia"><%=den(5)%></span><%end if%>
<hr></p>
<%if estado = 3 then%>
	<p><%=den(11)%></p>
<%else%>
	<p><%=den(14)%></p>
<%end if%>
<%
	case 4:
%>
<p class="OFESINGUARDAR"><%=den(4)%><hr></p>
<p><%=den(13)%></p>
<%
end select
%>

<%if estado = 3 or estado=5 then%>
	<p><%=den(12)%></p>
<%else%>
	<p><%=den(7)%></p>
<%end if%>
<p class="negrita"><%=den(8)%><br><br>
<img SRC="images/ayudaofeenviar.gif" WIDTH="162" HEIGHT="37"></p>


<%if estado = 4 then%>

<p><%=den(15)%></p>
<%else%>
<p><%=den(9)%></p>
<%end if%>
<p class="negrita"><%=den(10)%><br><br>
<img SRC="images/ayudaofeguardar.gif" WIDTH="165" HEIGHT="37"></p>



</body>
</html>
