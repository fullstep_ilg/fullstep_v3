﻿<%@ Language=VBScript %>
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/acceso.asp"-->

<%
''' <summary>
''' Para las subastas, el servidor puede estar en una franja horaria y el navegador del usuario en otra franja. Hay q sincronizar los relojes q cada vea lo hora correcta.
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\js\actualizarsubasta.asp  solicitudesoferta\js\cargarGanadora.asp     solicitudesoferta\js\cargarsubasta.asp; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,true,false,0)
	

CiaComp = clng(oRaiz.Sesion.CiaComp)

%>

<script src="../common/formatos.js"></script>
<script>


    function UTCtoClient(FechaUTC) {
    //Convierte una fecha en formato UTC a la fecha en la franja horaria del navegador
        miFecha = new Date()
        tzo = miFecha.getTimezoneOffset()
        var fechaCliente
        fechaCliente = new Date(FechaUTC.getFullYear(), FechaUTC.getMonth(), FechaUTC.getDate(), FechaUTC.getHours(), FechaUTC.getMinutes() - tzo, FechaUTC.getSeconds())
        return fechaCliente
    }


dFechaInicialServer = UTCtoClient(<%=JSHora(oRaiz.ObtenerSQLUTCDate(CiaComp))%>)
dFechaInicialClient = new Date()

function init()
{

p = parent.frames["fraOfertaClient"]
p.syncRelojes(dFechaInicialServer,dFechaInicialClient)
p.bSinSync=false
}
</script>


<HTML>
<HEAD>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</HEAD>


<BODY onload="init()">

<P>&nbsp;</P>

</BODY>
</HTML>
