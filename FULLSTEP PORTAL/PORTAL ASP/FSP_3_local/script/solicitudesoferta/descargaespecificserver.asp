﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->
<%
oldTimeOut = server.ScriptTimeout 
server.ScriptTimeout = 30000
%>
<title><%=title%></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">

<script>
var p

p = window.parent.frames["fraEspecClient"]

</script>

<%		
''' <summary>
''' Descarga todas las especificaciones de la oferta
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\descargaespecificacclient.asp ; Tiempo máximo: 0,2</remarks>

dim den
Idioma = Request.Cookies("USU_IDIOMA")
den = devolverTextos(Idioma,104)

set oRaiz=validarUsuario(Idioma,true,false,0)


'Obtiene los datos de la oferta
CiaComp = clng(oRaiz.Sesion.CiaComp)
anyoproceso=Request.Cookies ("PROCE_ANYO") 
gmn1proceso=Request.Cookies ("PROCE_GMN1") 
codproceso=Request.Cookies ("PROCE_COD") 
CodProve = oRaiz.Sesion.CiaCodGs

'carga todas las especificaciones del proceso
Set oProceso = oRaiz.Generar_CProceso
lNumEspec=oProceso.CargarTodasEspecificaciones (CiaComp,anyoproceso,gmn1proceso,codproceso,CodProve)

if lNumEspec=0 then  
	set oProceso=nothing
	set oRaiz=nothing	
	
%>
	<script>
		p.document.getElementById("divLiteral").innerHTML='<P class=error><%=den(7)%></P><center><P><input class=button type=button name=btnCerrar value="<%=Den(8)%>" onclick="cerrar()"></center></P>'
	</script>
<% 

else


	dim miCarpeta
	dim strPROVFECHAHORA
	dim sPath
	dim i 





	s=""
	lowerbound=65
	upperbound=90
	randomize
	for i = 1 to 10
		s= s & chr(Int((upperbound - lowerbound + 1) * Rnd + lowerbound))
	next	

	i=1
	
	sPath = Application ("CARPETAUPLOADS") & "\" & s & "\" & anyoproceso & "_" & gmn1proceso & "_" & codproceso & "\"'donde va a quedar el fichero autodescomprimible.
	
	strPROVFECHAHORA=cstr(CodProve) & cstr(year(now())) & cstr(month(now())) & cstr(day(now())) & cstr(hour(now())) & cstr(minute(now())) & cstr(second(now()))

	miCarpeta = Application ("CARPETAUPLOADS") & "\" & strPROVFECHAHORA  'donde se van a ir dejando las especificaciones para comprimir despues


	if not oProceso.Especificaciones is nothing then
		for each oEspec in oProceso.Especificaciones
			
%>			<script>
				p.document.getElementById("divArchivo").innerHTML="<table width=100%><tr height=20><td class=cabecera><%=den(1)%>&nbsp;<%=i%>&nbsp;<%=den(2)%>&nbsp;<%=lNumEspec%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=oespec.nombre%></td></tr></table>"
			</script>
<%			
			
			oespec.DescargarEspec CiaComp,den(3),miCarpeta

			i=i+1
		next
	end if
	
	if not oProceso.Grupos is nothing then
		for each oGrupo in oProceso.Grupos
			if not oGrupo.Especificaciones is nothing then
				for each oEspec in oGrupo.Especificaciones
%>			
					<script>
						p.document.getElementById("divArchivo").innerHTML="<table width=100%><tr height=20><td class=cabecera><%=den(1)%>&nbsp;<%=i%>&nbsp;<%=den(2)%>&nbsp;<%=lNumEspec%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=oespec.nombre%></td></tr></table>"
					</script>
<%			

					oespec.DescargarEspec CiaComp,den(4),miCarpeta

					i=i+1
				next
			end if
			
			if not oGrupo.Items is nothing then
				for each oItem in oGrupo.Items
					if not oItem.Especificaciones is nothing then	
						for each oEspec in oItem.Especificaciones
%>			
							<script>
								p.document.getElementById("divArchivo").innerHTML="<table width=100%><tr height=20><td class=cabecera><%=den(1)%>&nbsp;<%=i%>&nbsp;<%=den(2)%>&nbsp;<%=lNumEspec%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=oespec.nombre%></td></tr></table>"
							</script>
<%			

							oespec.DescargarEspec CiaComp,den(5),miCarpeta

							i=i+1
						next
					end if
				next
			end if
		next
	end if
	
%>
	<script>
		p.document.getElementById("divArchivo").innerHTML="<table width=100%><tr height=20><td class=cabecera><%=den(6)%></td></tr></table>"
	</script>

<%
	
	dim miFichero	
	miFichero=den(9) & ".exe"
	
	'Ahora comprime todos los ficheros en un zip auto-extraible y los elimina de carpetaUploads
	datasize= oProceso.ComprimirEspecificaciones(miCarpeta,sPath,miFichero)
	
	set oProceso=nothing
	set oRaiz=nothing	
    
%>
	<script>
		var re
		var re2
		re=/MSIE 5.5/
		re2=/Q299618/
	
		re3 = /SP1/

		p=null
		if (navigator.userAgent.search(re)!=-1 && navigator.appMinorVersion.search(re2)==-1 && navigator.appMinorVersion.search(re3)!=-1)
		{
			window.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/mspatch.asp","_blank","top=100,left=150,width=300,height=250,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
		}	
		else
		{
			window.open("<%=application("RUTASEGURA")%>script/common/download.asp?path=<%=server.urlencode(s & "/" & anyoproceso & "_" & gmn1proceso & "_" & codproceso)%>&nombre=<%=server.urlencode(miFichero)%>&datasize=<%=server.urlencode(datasize)%>","_blank","top=100,left=150,width=630,height=300,location=no,menubar=yes,resizable=yes,scrollbars=no,toolbar=no")
		}
		window.parent.close()
	</script>
	
<%	
end if

server.ScriptTimeout =oldTimeOut

%>