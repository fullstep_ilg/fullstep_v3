﻿<%@ Language=VBScript %>
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/acceso.asp"-->

<%
''' <summary>
''' Muestra un aviso q la accion q vas a hacer implica primera un guardado de la oferta
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.asp  solicitudesoferta\excel.asp ; Tiempo máximo: 0,2</remarks> 

dim den

Idioma = Request.Cookies("USU_IDIOMA")
den=devolverTextos(Idioma,105)


target = request("target")


%>
<html>
<title><%=title%></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</head>

<script>
function aceptar()
{
p = window.opener
<%if target = "excel" then%>
	p.descargar(false)
<%else%>
	p.imprimirOferta(false)
<%end if%>
window.close()
}

</script>
<body>
<table><tr><td>
<img SRC="images/information.gif" WIDTH="32" HEIGHT="32"></td>
<td><p class="negrita"><%=den(1)%></p></td></tr></table>


<p class="negrita">
<%if target = "excel" then
	Response.Write den(9)
else
	Response.Write den(8)
end if%>
</p>

<p class="negrita" style="margin-top:5px">
<%if target = "excel" then
	Response.Write den(3)
else
	Response.Write den(2)
end if%>
</p>

<p><%=den(10)%><img style="margin-bottom:-5px" SRC="images/guardar_off.gif" WIDTH="23" HEIGHT="22"></p>


<p><%=den(4)%></p>
<p><%=den(5)%>&nbsp;<img style="margin-bottom:-5px" WIDTH="40" HEIGHT="22" SRC="images/enviar_off.gif"></p>

<table width="80%" align="center">
	<tr>
		<td width="50%" align="center">
			<input type="button" class="button" name="cmdAceptar" onclick="aceptar()" value="<%=den(6)%>">
		</td>
		<td width="50%" align="center">
			<input type="button" class="button" name="cmdCancelar" onclick="window.close()" value="<%=den(7)%>">
		</td>
	</tr>
</table>

</body>
</html>
