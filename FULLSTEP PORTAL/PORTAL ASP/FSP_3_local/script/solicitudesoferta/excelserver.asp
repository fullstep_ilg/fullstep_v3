﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->

<%
''' <summary>
''' Exporta a excel la oferta
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\excel.asp ; Tiempo máximo: 0,2</remarks>

dim oRaiz 

OldTimeOut = server.ScriptTimeout 

server.ScriptTimeout=1800
Idioma = Request.Cookies("USU_IDIOMA")

dim den

den = devolverTextos(Idioma, 112)

set oRaiz=validarUsuario(Idioma,true,false,0)


Set mySmartUpload = Server.CreateObject("Persits.Upload")
mySmartUpload.ProgressID = Request.QueryString("PID")


CiaComp = clng(oRaiz.Sesion.CiaComp)

anyo = request.Cookies ("PROCE_ANYO")
gmn1 = request.Cookies ("PROCE_GMN1")
proce = request.Cookies ("PROCE_COD")

dim proveCia
proveCia = oRaiz.Sesion.CiaCodGs
if Application("PYME") <> "" then
	If InStr(1, proveCia, Application("PYME")) = 1 Then
		ProveCod = Replace(proveCia, Application("PYME"), "", 1, 1)
	else
		ProveCod = proveCia
	End If
else
	ProveCod = proveCia
end if


if Application("FSAL")=1 then

    arrUsuCodCiaCod=ObtenerUsuCodCiaCod(Request.Cookies("USU_SESIONID"),Request.ServerVariables("LOCAL_ADDR"),Request.Cookies("SESPP"),Request.Cookies("USU_CSRFTOKEN"))
    if UBound(arrUsuCodCiaCod) = 1 then
        sUsuCod=arrUsuCodCiaCod(0)
        sProveCod=arrUsuCodCiaCod(1)
    else
        sUsuCod="Registro"
        sProveCod="Registro"
    end if

    set arrUsuCodCiaCod=Nothing

    fechaIniFSAL7=Request("fechaIniFSAL7")
    sIdRegistroFSAL7=Request("sIdRegistroFSAL7")

    sIdRegistroFSAL1=sIdRegistroFSAL7   ''para que en fsal_3.asp coja valor la asignacion que hay
    sIDPagina="http://" & Request.ServerVariables("SERVER_NAME") & Request.ServerVariables("URL")
    sNavegador=Request.ServerVariables("HTTP_USER_AGENT")
    sQueryString=Request.ServerVariables("QUERY_STRING")
    fechaFinFSAL7 = GetFechaLog()

    if InStr(sNavegador,"Firefox/29.0") > 0 then
        sArgs="Tipo=5&Producto=" & Application("FSAL_Origen") & _
          "&fechapet=" & fechaIniFSAL7 & _
          "&fecha=" & fechaFinFSAL7 & _
          "&pagina=" & sIDPagina & _
          "&iPost=0" & _
          "&iP=" & Request.ServerVariables("REMOTE_ADDR") & _
          "&sUsuCod=" & sUsuCod & _
          "&sPaginaOrigen=" & ObtenerPaginaOrigen(sIDPagina,5) & _
          "&sNavegador=" & sNavegador & _
          "&IdRegistro=" & sIdRegistroFSAL7 & _
          "&sProveCod=" & sProveCod & _
          "&sQueryString=" & sQueryString & _
          "&sNormalizarFechas=0"
        CallWebServiceASP Application("URL_FSAL_RegistrarAccesos_Server"),"FSALRegistrarAccesos",sArgs
    end if

    fechaIniFSAL7=GetFechaLog()

end if


mySmartUpload.Save Application ("CARPETAUPLOADS")


set file = mysmartupload.Files(1)

path = file.path

set file = nothing
set mySmartUpload = nothing

%>
<html>
<title><%=title%></title>
<script src="../common/ajax.js"></script>
<script src="../common/formatos.js"></script>
<!--#include file="../common/fsal_3.asp"-->
<script>
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
	{   
        if (<%=application("FSAL")%> == 1)
	    {
	        Ajax_FSALActualizar3(); //registro de acceso
	    }
	    window.opener.top.winEspera = window.open("<%=application("RUTASEGURA")%>script/common/winEspera.asp?msg=<%=server.urlencode(den(1))%>&msg2=<%=server.urlencode(den(2))%>", "_blank", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
	    window.opener.top.winEspera.focus()
	    var acceso = false
	    // cambios realizados para compatibilidad con IE9
	    // el primer acceso a wEspecsItem.document.all("divEspecificacionesItem") genera un error de acceso si la ventana aÃºn no se ha cargado
	    // por eso se hace un segundo bucle controlando el error
	    while (acceso == false) {
	        try {
                while (typeof(window.opener.top.winEspera.document.images["imgReloj"])!= 'undefined') {
                }
	            acceso = true
	        }
	        catch (e) {
	            acceso = false;
	        }
	    }
		
	    window.close()
	    window.opener.open("<%=application("RUTASEGURA")%>script/solicitudesoferta/cargarexcel.asp?path=<%=Server.urlEncode(path)%>","fraOfertaServer")
	}
</script>

<body onload="init()">

</body>
<!--#include file="../common/fsal_8.asp"-->
</html>

<%
server.ScriptTimeout=OldTimeOut
set oRaiz = nothing
%>