﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %> 
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/fsal_1.asp"-->

<script src="../common/ajax.js"></script>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp"/>
<% 



''' <summary>
''' Mostrar la pantalla para modificar los datos del usuario
''' </summary>
''' <remarks>Llamada desde: menu.asp; Tiempo máximo: 0,1</remarks>
    Idioma = Request.Cookies("USU_IDIOMA")

    set oRaiz=validarUsuario(Idioma,true,true,1)

    dim IsSecure
    IsSecure = left(Application ("RUTASEGURA"),8)
    if IsSecure = "https://" then                       
        IsSecure = "; secure"
    else
        IsSecure =""
    end if
    Response.AddHeader "Set-Cookie","USU_CSRFTOKEN=" & server.URLEncode(oRaiz.Sesion.CSRFToken) & "; path=/; HttpOnly" + IsSecure

	idUsuario = oRaiz.Sesion.UsuId
	idCia = oRaiz.Sesion.CiaId
	idPortal = Application("PORTAL")

    
	set oUsuario = oRaiz.devolverdatosUsuario(idUsuario,idCia,idPortal)
	set oCia = oRaiz.devolverdatosCia(idCia,idPortal)

	''' Cargamos los literales según el idioma actual
		
	Dim Den

	den=devolverTextos(Idioma,29)
		
	''' Asignamos los literales a cada elemento
		
	lblTitulo= Den (1)
	lblRellene = Den(2)
	lblCod =Den (3)
	lblPWD = Den (4)
	lblConfirmPWD = Den(5)
	lblApe = Den (6)
    lblNif = Den (30)
	lblNom = Den (7)
	lblDepartamento = Den (8)
	lblCargo = Den (9)
	lbltfno = Den (10)
	lblFax = Den (12)
	lblEmail = Den (13)
	lblIdioma = Den (14)
	lblTfnoMv = Den(18)

    set paramgen = oRaiz.Sesion.Parametros

    CiaId = oRaiz.Sesion.CiaId
    set oCompania = oRaiz.devolverDatosCia (CiaId, Application("PORTAL"), false)
    set oPaises = oraiz.Generar_CPaises 			
    oPaises.CargarTodosLosPaises Idioma,,,,,true
    set oPais = oPaises.Item(ocompania.codpais)
%>  
<script type="text/javascript" src="../common/menu.asp"></script>
<script type="text/javascript" src="../common/formatos.js"></script>
<!--#include file="../common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init() {
    if (<%=application("FSAL")%> == 1)
    {
        Ajax_FSALActualizar3(); //registro de acceso
    }
    document.getElementById('tablemenu').style.display = 'block';
}
</script>
</head>
<body topMargin=0 leftmargin=0 onload="init()">
<script type="text/javascript">dibujaMenu(5);</script>
<script type="text/javascript">	
var validNif = <%=oPais.validNif %>;
function ValidarNIFCIF(){
    p= window.parent;
    f = document.forms["frmModificarUsuario"];
    switch(validNif)
	    {
	    case 0:
		    return true
		    break;
	    case 1:
		    if(f.txtNif.value=="") 
			    return true
		    if (VALIDAR_CIF(f.txtNif.value))
			    return true
			
		    if (VALIDAR_NIF(f.txtNif.value))
			    return true

		    return false
		    break;
	    default:
		    return true
	    }
}

function Validacion()
{
    f = document.forms["frmModificarUsuario"]
	f.txtCod.value = trim(f.txtCod.value);

	if (trim(f.txtCod.value) == "" || trim(f.txtApe.value) == "" || trim(f.txtNombre.value) == "" || trim(f.txtDep.value)=="" || trim(f.txtCargo.value) =="" 
    || trim(f.txtTfno.value) == "" || trim(f.txtemail.value) == "")	{
	    return("<%=JSText(Den(22))%>")
	}
	if (f.txtCod.value.length<6)
		{
		return ('<%=JSText(den(24))%>')
		}		
	if (!validarEmail(f.txtemail.value))
		{
			return ('<%=JSText(den(26))%>')
		}

    if (!ValidarNIFCIF()){
        f.txtNif.focus();
        return ('<%=JSAlertText(den(31))%>');
    }

    <%if paramgen.CompruebaTfno then%>
	    if (!ValidarTelefono(trim(f.txtTfno.value),'<%=JSText(oCia.DenPais)%>'))
		    {
			    return ('<%=JSText(den(27))%>')
		    }
    <%end if%>
    <%if paramgen.CompruebaTfno then%>
	    if (trim(f.txtTfno2.value) != "")
	    {
		    if (!ValidarTelefono(trim(f.txtTfno2.value),'<%=JSText(oCia.DenPais)%>')){
			    return ('<%=JSText(den(27))%>')
		    }
	    }
    <%end if%>
    <%if paramgen.CompruebaTfno then%>
	    if (trim(f.txtTfnoMv.value) != "")
	    {
		    if (!ValidarTelefono(trim(f.txtTfnoMv.value),'<%=JSText(oCia.DenPais)%>')){
			    return ('<%=JSText(den(28))%>')
		    }
	    }
    <%end if%>	
    <%if paramgen.CompruebaTfno then%>
	    if (trim(f.txtFax.value) != "")
	    {
		    if (!ValidarTelefono(trim(f.txtFax.value),'<%=JSText(oCia.DenPais)%>')){
			    return ('<%=JSText(den(29))%>')
		    }
	    }
    <%end if%>
    return("")
}

function ModificarUsuario_onsubmit(){   
    Dato = Validacion();   
    if (Dato != ""){
		alert (Dato);
		return false;
    }else{
		document.forms["frmModificarUsuario"].submit();
    }
};
</script>
<title>%=texto</title>
<h1><%=lblTitulo%></h1>
<h4><%=lblRellene%></h4>
    <form name="frmModificarUsuario" action="CodigoModificarUsuario.asp" method="post">
    <input type="hidden" name="Idioma" value="<% = trim(Idioma)%>" />
    <div align="center">
        <table align="center" border="0" bgcolor="white" cellpadding="1" cellspacing="1"
            width="70%">
            <tr>
                <td align="left" class="filaImpar negrita" width="150">
                    <%=lblCod%>
                </td>
                <td align="left" class="filaImpar" width="500">
                    <input type="text" id="txtCod" name="txtCod" size="20" maxlength="20" value="<%=HTMLEncode(oUsuario.Cod)%>" autocomplete="off"/>
                </td>
            </tr>
            <tr>
                <td align="left" class="filaPar negrita" width="150">
                    <%=lblNom%>
                </td>
                <td align="left" class="filaPar" width="500">
                    <input type="text" id="txtNombre" name="txtNombre" size="20" maxlength="20" value="<%=HTMLEncode(oUsuario.Nombre)%>">
                </td>
            </tr>
            <tr>
                <td align="left" class="filaImpar negrita" width="150">
                    <%=lblApe%>
                </td>
                <td align="left" class="filaImpar" width="500">
                    <input type="text" id="txtApe" name="txtApe" size="41" maxlength="100" value="<%=HTMLEncode(oUsuario.Apellidos)%>">
                </td>
            </tr> 
            <tr>
                <td align="left" class="filaPar negrita" width="150">
                    <%=lblNif%>
                </td>
                <td align="left" class="filaPar" width="500">
                    <input type="text" id="txtNif" name="txtNif" size="41" maxlength="100" value="<%=HTMLEncode(oUsuario.Nif)%>" />
                </td>
            </tr>          
            <tr>
                <td align="left" class="filaImpar negrita" width="150">
                    <%=lblDepartamento%>
                </td>
                <td align="left" class="filaImpar" width="500">
                    <input type="text" id="txtDep" name="txtDep" size="41" maxlength="100" value="<%=HTMLEncode(oUsuario.codDep)%>">
                </td>
            </tr>
            <tr>
                <td align="left" class="filaPar negrita" width="150">
                    <%=lblCargo%>
                </td>
                <td align="left" class="filaPar" width="500">
                    <input type="text" id="txtCargo" name="txtCargo" size="41" maxlength="100" value="<%=HTMLEncode(oUsuario.Cargo)%>">
                </td>
            </tr>
            <tr>
                <td align="left" class="filaImpar negrita" width="150">
                    <%=lbltfno%>
                </td>
                <td align="left" class="filaImpar" width="500">
                    <input type="text" id="txtTfno" name="txtTfno" size="19" maxlength="20" value="<%=HTMLEncode(oUsuario.Tfno)%>">
                </td>
            </tr>
            <tr>
                <td align="left" class="filaPar negrita" width="150">
                    &nbsp;
                </td>
                <td align="left" class="filaPar" width="500">
                    <input type="text" id="txtTfno2" name="txtTfno2" size="19" maxlength="20" value="<%=HTMLEncode(oUsuario.Tfno2)%>">
                </td>
            </tr>
            <tr>
                <td align="left" class="filaImpar negrita" width="150">
                    <%=lblTfnoMv%>
                </td>
                <td align="left" class="filaImpar" width="500">
                    <input type="text" id="txtTfnoMv" name="txtTfnoMv" size="19" maxlength="100" value="<%=HTMLEncode(oUsuario.TfnoMovil)%>">
                </td>
            </tr>
            <tr>
                <td align="left" class="filaPar negrita" width="150">
                    <%=lblFax%>
                </td>
                <td align="left" class="filaPar" width="500">
                    <input type="text" id="txtFax" name="txtFax" size="19" maxlength="100" value="<%=HTMLEncode(oUsuario.fax)%>">
                </td>
            </tr>
            <tr>
                <td align="left" class="filaImpar negrita" width="150">
                    <%=lblEmail%>
                </td>
                <td align="left" class="filaImpar" width="500">
                    <input type="text" id="txtemail" name="txtemail" size="40" maxlength="100" value="<%=HTMLEncode(oUsuario.Mail)%>">
                </td>
            </tr>
            <tr>
                <td align="left" class="filaPar negrita" width="150">
                    <%=lblIdioma%>
                </td>
                <td align="left" class="filaPar" width="500">
                    <select name="lstIdioma" id="lstIdioma" size="1">
                        <%set adorIdi = oRaiz.DevolverIdiomas
			    while not adorIdi.eof%>
                        <option value="<%=adorIdi.fields("COD").value%>" <%if ousuario.Idioma=adorIdi.fields("COD").value then%>
                            selected<%end if%>>
                            <%=adorIdi.fields("DEN").value%></option>
                        <%adorIdi.movenext
			    wend
			    adorIdi.close
			    set adorIdi = nothing%>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="left" class="filaImpar negrita" width="200">
                    <%=Den(19)%>
                </td>
                <td align="left" class="filaImpar" width="500">
                    <input type="radio" class="radio" id="optTipoEMail" name="optTipoEMail" value="1"
                        <%if oUsuario.TipoMail=1 then%> checked <%end if%> /><b><%=Den(21)%></b>
                    <input type="radio" class="radio" id="optTipoEMail" name="optTipoEMail" value="0"
                        <%if oUsuario.TipoMail=0 then%> checked <%end if%> /><b><%=Den(20)%></b>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <center>
        <input class="button" id="cmdGuardar" name="cmdGuardar" type="button" value="<%=den(17)%>"
            onclick="return ModificarUsuario_onsubmit()" /></center>
    </form>
</body>
<!--#include file="../common/fsal_2.asp"-->
</html>