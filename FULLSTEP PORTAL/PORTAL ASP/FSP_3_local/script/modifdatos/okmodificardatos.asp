﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/acceso.asp"-->
<% 
''' <summary>
''' Mostrar la pantalla para indicar modificacion correcta de los datos del usuario
''' </summary>
''' <remarks>Llamada desde: modifdatos\codigomodificarusuario.asp; Tiempo máximo: 0,1</remarks>
	Idioma = Request.Cookies("USU_IDIOMA")
	Dim Den
	den=devolverTextos(Idioma,30)

    '''cookie para comunicarnos con la la parte .NET y hacerle saber que la culture del usuario
    '''ha cambiado
    Response.Cookies("CultureChange")=1 
%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/menu.asp"></script>

<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
    function init() {
        document.getElementById('tablemenu').style.display = 'block';
        window.parent.document.title = '<%=title%>';
    }
</script>
</HEAD>
<BODY topMargin=0 leftmargin=0 onload = "init()">
<script>dibujaMenu(5)</script>
<h1><%=Den(1)%></h1>
<h3><%=Den(2)%></h3>
</BODY>
</HTML>

