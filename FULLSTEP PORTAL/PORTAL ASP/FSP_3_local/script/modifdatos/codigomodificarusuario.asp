﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/formatos.asp"-->
<% 

''' <summary>
''' modificar los datos del usuario
''' </summary>
''' <remarks>Llamada desde: modifdatos\modificardatos.asp ; Tiempo máximo: 0,2</remarks>
	Idioma = Request.Cookies("USU_IDIOMA")
    Idioma = trim(Idioma)
	
	set oRaiz=validarUsuario(Idioma,true,false,2)
		
	IdUsu = oRaiz.Sesion.UsuId
	CiaId = oRaiz.Sesion.CiaId
	PortalId = cint(Application("PORTAL"))
	usuCod = request("txtCOD")
	Ape = request("txtApe")
    Nif = request("txtNif")
	Nombre = request("txtNombre")
	Dep = request("txtDep")
	Cargo = request("txtCargo")
	Tfno = request("txtTfno")
	Tfno2 = request("txtTfno2")
	Fax = request("txtFax")
	email = request("txtemail")
	Idioma = request("lstIdioma")
	TfnoMv = request("txtTfnoMv")
	TipoMail=request("optTipoEMail")
		
	set oError = oRaiz.ModificarDatosUsuario (Request.Cookies("USU_SESIONID"),IdUsu,CiaId,PortalId,usuCod,Ape,Nif,Nombre,Dep,Cargo,Tfno,Tfno2,Fax,Email,Idioma,TfnoMv,TipoMail)
	numError = oError.numerror
	if numError = 0 then
        dim IsSecure
        IsSecure = left(Application ("RUTASEGURA"),8)
        if IsSecure = "https://" then                       
            IsSecure = "; secure"
        else
            IsSecure =""
        end if

        Response.AddHeader "Set-Cookie","USU_IDIOMA=" &  Idioma  & "; path=/; HttpOnly" + IsSecure
	end if


	set oError = nothing
	set oUsuario = nothing
	 
    
	set oraiz = nothing
	
	if numError <> 0 then
		Response.Redirect Application ("RUTASEGURA") & "script/modifdatos/ErrorAlModificarUsuario.asp"  
	else
		Response.Redirect Application ("RUTASEGURA") & "script/modifdatos/OKModificarDatos.asp"
	end if
	
	
	
%>
