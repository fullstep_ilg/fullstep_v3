﻿<%@ Language=VBScript %>
<!--#include file="./common/acceso.asp"-->
<!--#include file="./common/idioma.asp"-->

<%
''' <summary>
''' Tras un oRaiz.ObtenerMonedaCentral resulta q no hay moneda
''' </summary>
''' <remarks>Llamada desde: pedidos\buscarpedidos.asp   pedidos\cargarlineas.asp    pedidos\pedidos11dir.asp    seguimpedidos\buscarpedidos.asp
''' seguimpedidos\cargarlineas.asp; Tiempo máximo: 0,2</remarks>

Idioma = Request("Idioma")
if Idioma = "" then
	Idioma = "SPA"
end if
	
set oRaiz=validarUsuario(Idioma,false,false,0)




dim den


den = devolverTextos(Idioma, 74)


%>
<HTML>
<HEAD>
<% 
    
%>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
    function init() {
        document.getElementById('tablemenu').style.display = 'block';
    }
</script>
</HEAD>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="common/menu.asp"></script>

<BODY topmargin=0 leftmargin=0 onload="init()">
<script>dibujaMenu(4)</script>

<table class=principal>
	<tr>
		<td class = error>
			<%=den(1)%>
		</td>
	</tr>
	<tr>
		<td>
			<%=den(2)%>
		</td>
	</tr>
	<tr>
		<td>
			<%=den(3)%>
		</td>
	</tr>
	<tr>
		<td>
			<%=den(6)%>
		</td>
	</tr>
	<tr>
		<td>
			<a style="font-size:12px" href="mailto:<%=Application("MAILPORTAL")%>"><%=Application("MAILPORTAL")%></a>
		</td>
	</tr>
	<tr>
		<td>
			<%=den(5)%>
		</td>
	</tr>
</table>

</BODY>
</HTML>
