﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/menu.asp"></script>
<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
    document.getElementById('tablemenu').style.display = 'block';
}
</script>
</HEAD>
<BODY topmargin=0 leftmargin=0 onload="init()">


<%
''' <summary>
''' Mensaje q indica q el cambio se realizo correctamente
''' </summary>
''' <remarks>Llamada desde: usuppal\modifactividadesclient.asp      usuppal\guardarcompania.asp ; Tiempo máximo: 0,2</remarks>

Idioma = trim(Request.Cookies("USU_IDIOMA"))

set oRaiz=validarUsuario(Idioma,false,false,0)


Dim Den

den=devolverTextos(Idioma,70)
%>
<script>    dibujaMenu(6)</script>
<%if request("avisarConf")="yes" then%>
<h1><%=Den(4)%></h1>
<h3><%=Den(5)%></h3>
<BR>
<h3><%=Den(3)%></h3>
<%else%>
<h1><%=Den(1)%></h1>
<h3><%=Den(2)%></h3>
<%end if%>
<br>
<br>
<p>&nbsp;</p>
</BODY>
</HTML>
