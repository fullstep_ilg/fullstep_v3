﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->

<%
''' <summary>
''' Para modificar datos de la compania, esta es la parte q nos proporciona la utilidad del combo provincias.
''' </summary>
''' <remarks>Llamada desde: usuppal\modifcompaniaclient.asp ; Tiempo máximo: 0,2</remarks>
dim oRaiz
dim Pais

Pais = request("Pais")
Idioma=trim(Request.Cookies("USU_IDIOMA"))
set oRaiz=validarUsuario(Idioma,false,false,0)


''' <summary>
''' Obtiene las provincias de un pais
''' </summary>
''' <param name="Pais">Pais</param>
''' <remarks>Llamada desde: propia pantalla ; Tiempo máximo: 0,2</remarks>
function obtenerProvi (Pais)

	
	set opaises = oraiz.generar_CPaises
	oPaises.add  "","",,,,cint(Pais)
	set oPais = oPaises.item(1)
	
	opais.CargarTodasLasProvincias ,,,true

%>
<script>

/*
''' <summary>
''' obtener provincia
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: modifcompaniaserver.asp Tiempo m�ximo: 0</remarks>
''' <revision>JVS 16/06/2011</revision>
*/
function obtenerProvi()
{
var f
f=window.parent.frames["fraModifCompaniaClient"]

f.borrarProvi()

f.anyadirProvi('','')
<%
	for each oProvi in oPais.Provincias
%>	
	f.anyadirProvi('<%=oProvi.id%>','<%=oProvi.den%>')
<%
	next
%>
//se comprueba si es obligatorio o no rellenar el campo provincia en funci�n del pa�s
f.comprobarProviOblig()
}

</script>
<%
set oPaises=nothing
set oPais.Provincias=nothing
set oPais=nothing

end function



%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<%


obtenerprovi(Pais)
	
%>

<script>

function init()
	{
	    obtenerProvi()
	}

</script>



</head>
<body onload="init()">
<p>&nbsp;</p>
</body>
</html>

<%

set oRaiz = nothing	
%>
