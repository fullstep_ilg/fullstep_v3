﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<%
''' <summary>
''' Usando un objeto Persits.Upload graba en bbdd el archivo indicado
''' </summary>
''' <remarks>Llamada desde: usuppal\upload.asp ; Tiempo máximo: 0,2</remarks>
dim oRaiz 

oldTimeOut = server.ScriptTimeout 
server.ScriptTimeout =10000

Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,true,false,0)

CiaId = clng(oRaiz.Sesion.CiaId)

Set mySmartUpload = Server.CreateObject("Persits.Upload")
mySmartUpload.CodePage = 65001
mySmartUpload.ProgressID = Request.QueryString("PID")
	
CiaComp = clng(oRaiz.Sesion.CiaComp)
	
on error resume next

bErrorNombreArchivo = false

if (Request.QueryString("NombreArchivo") = "") then
    bErrorNombreArchivo = true
end if

if (not bErrorNombreArchivo) then

    sExtensionIncorrecta = oRaiz.VerificarExtensionUpload_SinGrabar(Request.QueryString("NombreArchivo"))

    if (sExtensionIncorrecta = "") then   
        mySmartUpload.Save Application ("CARPETAUPLOADS")

        if (not Request.QueryString("NombreArchivo") = mySmartUpload.form.item("NombreFichero")) then
            bErrorNombreArchivo = true

            oRaiz.VerificarExtensionUpload mySmartUpload.form.item("NombreFichero"),Application ("CARPETAUPLOADS"), true
        else             
            sExtensionIncorrecta = oRaiz.VerificarExtensionUpload (mySmartUpload.form.item("NombreFichero"),Application ("CARPETAUPLOADS"))
        end if
    end if

    if (not bErrorNombreArchivo) then

        bTipoArchivoDistinto = false

        if (not sExtensionIncorrecta = "") then    
            sExtensionIncorrectaAux = replace(sExtensionIncorrecta, "@@", "")

            if not (sExtensionIncorrectaAux = sExtensionIncorrecta) then
                bTipoArchivoDistinto = true    
                sExtensionIncorrecta=sExtensionIncorrectaAux    
            end if
        else

            kbActual = mySmartUpload.form.item("kbActual")
            kbActual = Numero(replace(kbActual,".",decimalfmt))

            maxADJUN = mySmartUpload.form.item("MaxAdjun")
            maxADJUN= Numero(replace(MaxAdjun,".",decimalfmt))

            MAXIMO = maxADJUN- kbActual
            if MAXIMO <=0 then
	            MAXIMO = 1
            END IF
	
            if err = 0 then
	            For each file In mySmartUpload.Files
	            '  Only if the file exist
	            '  **********************
		           If Not file Is Nothing Then
			            iFileSize = file.originalSize
			            if maxADJUN-kbACTUAL >= ifilesize then
				            sFile = file.path
				            sTFile = sPlit(sFile,"\")
				            sNombre = sTFile(ubound(sTFile))
				            set oEspec = oRaiz.Generar_CEspecificacion
				            oEspec.nombre=sNombre
				            oEspec.Comentario=mySmartUpload.form.item("txtComentario").value
				            set oError = oEspec.guardarEspecificacionCia (CiaId,file.path)
				            idEspec = oEspec.ID
				            ifilesize = oEspec.datasize
				            set oAdjun = nothing
				            tehaspasao = false
			            else
				            tehaspasao=true
			            end if
		            end if

	            next
            else
	            ifilesize=mySmartUpload.TotalBytes 
	            tehaspasao = true
            end if
        end if
    end if
end if
        
server.ScriptTimeout =oldTimeOut

%>
<script>
p=window.opener
function quit()
	{
	var e
	try
	{
	p=window.opener
	p.winEspec = null
	}
	catch(e)
	{
	return true 
	}
	}	
</script>

<%
dim Cia

dim CiaUsu


Dim Den

den=devolverTextos(Idioma,59)

if (bErrorNombreArchivo) then

%>

<html>
<html>
<title><%=title %></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<body  onunload="quit()">

<center>
    <SPAN class="fgrande error negrita">
        <%=Den (1)%>
    </span>
    <P>
    <span class ="fmedia negrita">
    <BR>
        <%=Den(4)%>
    </span>
</center>

<form id=form5 name=form1><center><input class=button type=button value=<%=den(3)%> onclick="window.close()"  id=button4 name=button0>
</body>

</html>

<%

elseif (bTipoArchivoDistinto) then

%>

<html>
<title><%=title %></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<body  onunload="quit()">

<center>
    <SPAN class="fgrande error negrita">
        <%=Den (1)%>
    </span>
    <P>
    <span class ="fmedia negrita">
    <BR>
        <%=replace(Den (13), "###", sExtensionIncorrecta)%>
    </span>
</center>

<form id=form4 name=form1><center><input class=button type=button value=<%=den(3)%> onclick="window.close()"  id=button3 name=button0>
</body>

</html>

<%

elseif (not sExtensionIncorrecta = "") then

%>

<html>
                

<title><%=title %></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<body  onunload="quit()">

<center>
    <SPAN class="fgrande error negrita">
        <%=Den (1)%>
    </span>
    <P>
    <span class ="fmedia negrita">
    <BR>
        <%=replace(Den (12), "###", sExtensionIncorrecta)%>
    </span>
</center>

<form id=form3 name=form1><center><input class=button type=button value=<%=den(3)%> onclick="window.close()"  id=button0 name=button0>
</body>

</html>

<%

elseif tehaspasao then

%>


<html>
<title></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<body  onunload="quit()">

<CENTER>
<SPAN class="fgrande error negrita">
<%=Den (1)%>
</span>
<P>
<span class ="fmedia negrita">
<BR>
<%=Den (9)%>
</span>
</center>
<BR>
<%=Den (11)%>&nbsp;<%=VisualizacionNumero(maxADJUN/1024,decimalfmt,thousandfmt,0)%>&nbsp Kb.
<BR>
<%=Den (10)%>&nbsp;<%=VisualizacionNumero(kbACTUAL/1024,decimalfmt,thousandfmt,0)%>&nbsp Kb.
<BR>
<%=Den (8)%>&nbsp;<%=VisualizacionNumero(iFileSize/1024,decimalfmt,thousandfmt,0)%>&nbsp Kb.

<form id=form2 name=form2><center><input class=button type=button value=<%=den(3)%> onclick="window.close()"  id=button1 name=button1>
</body>

</html>

<%
elseif ifilesize=0 then
	 

%>


<html>
<title></title>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<style>
.tINPUT  {font-family:tahoma;font-weight:normal;font-size:10;width=50px;font-weight:bold;}

</style>
<body  onunload="quit()">

<CENTER>
<SPAN class="fgrande error negrita">
<%=Den (1)%>
</span>
<P>
<span class ="fmedia negrita">
<BR>
<%=Den (4)%>
</span>
<form id=form1 name=form1><center><input class=button type=button value=<%=den(3)%> onclick="window.close()"  id=button2 name=button2>
</body>

</html>

<%
	

else
%>


<html>
<script>
function init()
	{

	p.uploadRealizado(<%=iFileSize%>,<%=idEspec%>)
	window.close()
	}
</script>

<body onload="init()" onunload="quit()">
</body>

</html>
<%end if%>


<%

set oRaiz = nothing
%>