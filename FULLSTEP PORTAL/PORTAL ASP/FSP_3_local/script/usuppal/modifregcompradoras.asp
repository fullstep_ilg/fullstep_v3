﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->

<% 
''' <summary>
''' Modificar las companias compradoras
''' </summary>
''' <remarks>Llamada desde: common\menu.asp ; Tiempo máximo: 0,2</remarks>

Idioma=trim(Request("Idioma"))
set oRaiz=validarUsuario(Idioma,true,true,0)

IdProve = oRaiz.Sesion.CiaId
Dim Den

lIdUsu = oRaiz.Sesion.UsuId 

if Request.ServerVariables("CONTENT_LENGTH")>0 then

	oRaiz.comenzarTransaccion
	set TESError = oRaiz.eliminarSolictitudesCompradoras(IdProve)
	if TESError.numerror <> 0 then
		if TESError.numerror = 200 then
			oRaiz.cancelarTransaccion
			
			set oRaiz = nothing
			Response.Redirect Application ("RUTASEGURA") & "script/usuppal/errormodifregcomp.asp?Idioma=" & Idioma
			response.end
		else
			
			set oRaiz = nothing
			Response.Redirect Application ("RUTASEGURA") & "script/usuppal/errormodifregcomp.asp?Idioma=" & Idioma
			response.end
		end if
	end if
			
	i = 1
	while request("txtCompradora" & trim(cstr(i)))<>"" 
		if request("chkSolicitada" & trim(cstr(i))) <> "" then
			IdCiaComp = clng(request("txtCompradora" & trim(cstr(i))))
			set TESError = oraiz.AsignarCompradoraALaCia (IdProve,IdCiaComp)
			if TESError.numerror <> 0 then
				if TESError.numerror = 200 then
					oRaiz.cancelarTransaccion
					
					set oRaiz = nothing
					Response.Redirect Application ("RUTASEGURA") & "script/usuppal/errormodifregcomp.asp?Idioma=" & Idioma
					response.end
				else
					
					set oRaiz = nothing
					Response.Redirect Application ("RUTASEGURA") & "script/usuppal/errormodifregcomp.asp?Idioma=" & Idioma
					response.end
				end if
			end if
		end if
		i = i + 1
	wend
	oRaiz.confirmarTransaccion
	
	set ParamGen = oRaiz.Sesion.Parametros
    CodPortal = cstr(Application ("NOMPORTAL"))
	if ParamGen.NotifSolicComp=1 then
		den=devolverTextos(Idioma,82)
		set oCia= oraiz.generar_CCompania()
		oCia.Id=IdProve
		oCia.PYME=Application("PYME")
		oCia.LanzarEmailAdmRegComp ParamGen.Email, ParamGen.TipoEmail, den(1), lIdUsu, CodPortal
	end if

	set ParamGen = nothing
	set oRaiz = nothing
    
	Response.Redirect Application ("RUTASEGURA") & "script/usuppal/okmodifregcomp.asp?Idioma=" & Idioma
	


end if


den=devolverTextos(Idioma,68)


%>


<HTML>
<HEAD>
<% 
    
%>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</HEAD>

<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
    document.getElementById('tablemenu').style.display = 'block';
}

</script>

<script>
/*
''' <summary>
''' Muestra el detalle de una compania
''' </summary>
''' <param name="id">id de compania</param>
''' <remarks>Llamada desde: a href de cada compania ; Tiempo máximo: 0</remarks>*/
function detalle(id)
{
window.open("<%=Application("RUTASEGURA")%>script/usuppal/detallecia.asp?CiaId=" + id + "&Idioma=<%=Idioma%>","newwin","width=500,height=500,location=no,menubar=no,resizable=no,scrollbars=yes,toolbar=no")
return
}

</script>

<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/menu.asp"></script>
<BODY topmargin=0 leftmargin=0 onload="init()">
<SCRIPT>dibujaMenu()</SCRIPT>
<h1><%=Den(1)%></h1>
<h3><%=den(2)%></h3>

<form name=frmCompradoras method=post>

<table width=65% align=center>
	<tr>
		<td width=45% class='cabecera' ><%=den(3)%></td>
		<td width=10% class='cabecera' ><%=den(4)%></td>
		<td width=15% class='cabecera' ><NOBR><%=den(5)%></nobr></td>
		<td width=20% class='cabecera' ><NOBR><%=den(6)%></nobr></td>
	</tr>


<%

set ador = oraiz.devolverRegistroEnCompradoras(IdProve)
i = 1
mClase="filaImpar"
while not ador.eof
%>

	<tr>
		<td class='<%=mClase%>' bgcolor=white>
			<input type = hidden name=txtCompradora<%=i%> value=<%=ador("IDCIA")%>>
			<%=ador("COD") & " - " & ador("DEN")%>
		</td>
		<td class='<%=mClase%>' align=center><%if ador("REGISTRADA") = 1 then%><%=den(7)%><%else%><%=den(8)%><%end if%></td>
		<td class='<%=mClase%>' align=center><input type = checkbox class=checkbox  name=chkSolicitada<%=i%> <%if ador("SOLICITADA") = 1 then%> CHECKED <%end if%> value=<%=ador("IDCIA")%>></td>
		<td class='<%=mClase%>' align=center><a href="javascript:void(null)" onclick="return detalle(<%=ador("IDCIA")%>)"><%=den(9)%></a></td>
	</tr>
<%
	if mClase="filaImpar" then
		mClase="filaPar"
	else
		mClase="filaImpar"
	end if
i = i + 1
ador.movenext

wend

ador.close
set ador = nothing
%>

</table>

<br><br>
<table align=center width=40%>
<tr>
<td align=center><input class=button type=submit value=<%=den(10)%> id=btnAceptar name=btnAceptar></td>
</tr>
</table>
</BODY>
</HTML>
