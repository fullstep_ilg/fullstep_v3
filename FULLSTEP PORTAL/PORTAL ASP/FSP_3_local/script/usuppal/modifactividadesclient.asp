﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<%
if Request.ServerVariables("CONTENT_LENGTH")=0 then
%>
<html>
<style>
#divActividades {position:absolute;visibility:hidden;top:0;left:0;}
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/menu.asp"></script>


<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
    document.getElementById('tablemenu').style.display = 'block';
}
</script>

</head>


<body LANGUAGE="javascript" topmargin=0 leftmargin=0 onload = "init()">



<% 
end if


''' <summary>
''' Cuando pulses guardar, guardara los cambios en activades de la empresa
''' </summary>
''' <remarks>Llamada desde: usuppal\modifactividades.asp ; Tiempo máximo: 0,2</remarks>

Idioma=trim (Request.Cookies("USU_IDIOMA"))
set oRaiz=validarUsuario(Idioma,true,true,0)
	

Dim Den

if Request.ServerVariables("CONTENT_LENGTH")>0 then
	ciaID=oRaiz.Sesion.CiaId
	NumActs=request("txtNumActs")
	
	lIdUsu = oRaiz.Sesion.UsuId 
	
	dim oActsN1
	dim oActsN2
	dim oActsN3
	dim oActsN4
	dim oActsN5
	
	set oActsN1 = oraiz.generar_cactividadesNivel1()
	set oActsN2 = oraiz.generar_cactividadesNivel2()
	set oActsN3 = oraiz.generar_cactividadesNivel3()
	set oActsN4 = oraiz.generar_cactividadesNivel4()
	set oActsN5 = oraiz.generar_cactividadesNivel5()
	
		
	for i = 1 to NumActs
		IdentAct=request("txtAct" & cstr(i))
		CodAct=request("txtCodAct" & cstr(i))
		Idents=Split(IdentAct,"_",-1,1)
		if ubound(Idents)= 0 then
			ACN1 = cint(Idents(0))
			oactsn1.add acn1,codact,"No Importa ahora"
		else
			if ubound(Idents)= 1 then
				ACN1 = cint(Idents(0))
				ACN2 = cint(Idents(1))
				oactsn2.add acn1,acn2,codact,"No Importa ahora"
			else
				if ubound(Idents)= 2 then
					ACN1 = cint(Idents(0))
					ACN2 = cint(Idents(1))
					ACN3 = cint(Idents(2))
					oactsn3.add acn1,acn2,acn3,codact,"No Importa ahora"
				else
					if ubound(Idents)= 3 then
						ACN1 = cint(Idents(0))
						ACN2 = cint(Idents(1))
						ACN3 = cint(Idents(2))
						ACN4 = cint(Idents(3))
						oactsn4.add acn1,acn2,acn3,acn4,codact,"No Importa ahora"
					else
						ACN1 = cint(Idents(0))
						ACN2 = cint(Idents(1))
						ACN3 = cint(Idents(2))
						ACN4 = cint(Idents(3))
						ACN5 = cint(Idents(4))
						oactsn5.add acn1,acn2,acn3,acn4,acn5,codact,"No Importa ahora"
					end if
				end if
			end if
		end if
	next

	set oCia = oraiz.generar_CCompania()
	oCia.Id = ciaid
	set oError = oCia.modificarEstructuraActividades(oactsN1 , oactsN2 , oactsN3 , oactsN4 , oactsN5 )
	

	set ParamGen = oRaiz.Sesion.Parametros

	
	if ParamGen.NotifSolicAct=1 THEN
	den=devolverTextos(Idioma,81)
	oCia.PYME=Application("PYME")
    CodPortal = cstr(Application ("NOMPORTAL"))
	oCia.LanzarEmailAdmActividad ParamGen.Email, ParamGen.TipoEmail, den(1),lIdUsu, CodPortal
	end if

	set ParamGen = nothing
			
	Response.Redirect Application ("RUTASEGURA") & "script/usuppal/okmodificardatos.asp?AvisarConf=yes" 

end if


den=devolverTextos(Idioma,66)
set paramgen = oraiz.devolverParametrosGenerales
dim numMaxActividades
numMaxActividades=paramgen.MaxActividades
%>
<script>    dibujaMenu(6)</script>
<script>

/*
''' <summary>
''' Antes de guardar la modificacion, hace comprobaciones
''' </summary>     
''' <remarks>Llamada desde: cmdGuardar/onclick ; Tiempo máximo: 0</remarks>*/
function guardar ()
{
window.frames["fraActividad"].obtenerActividadesSeleccionadas()
window.open("<%=Application("RUTASEGURA")%>script/blank.htm","fraModifActividadesServer")
if (document.forms["frmActividades"].txtNumActs.value==0)
{
    alert('<%=den(7)%>')
	return false
}
if ((<%=numMaxActividades%> > 0) && (document.forms["frmActividades"].txtNumActs.value > <%=numMaxActividades%>))
	{
	alert('<%=Replace(den(10),"#Num",numMaxActividades)%>')
	return false
	}
return true
}

</script>
<h1><%=den(1)%></h1>
<h3><%=den(2)%></h3>
<h3><%=den(3)%></h3>
<p class=principal>
<!--<font size=3 face=wingdings color=black><strong>ü</strong></font>:&nbsp;<%=den(8)%>-->
<img src="../images/check.gif" align="baseline">:&nbsp;<%=den(8)%>
</p>
<!-- no se saca la den(5) aunque no se borra de B.D (Despliegue las actividades hasta el 4º nivel para seleccionar las actividades 
de su compañía  -->
<h5><%=den(4)%></h5>



<table class=principal border="0" height="215" width="95%" bgcolor="white" cellPadding="1" cellSpacing="0">
	<tr>
		<td height="215px">
			<iframe name="fraActividad" id="fraActividad" src="actividades.asp" frameborder="1" width="100%" height="100%"></iframe>
		</td>
	</tr>
</table>
<h5><%=den(9)%></h5>
<form name="frmActividades" method="post" action="modifActividadesclient.asp" target="_self">
	<p align="center"><input class="button" type="submit" value="<%=den(6)%>" onclick="return guardar()" id="cmdGuardar" name="cmdGuardar"></p>
	<input type="hidden" name="Idioma" value="<%=trim (Request.Cookies("USU_IDIOMA"))%>">
	<input type="hidden" name="txtNumActs" value="0">

<div name="divActividades" id="divActividades">

</div>
</form>

</body>
</html>
