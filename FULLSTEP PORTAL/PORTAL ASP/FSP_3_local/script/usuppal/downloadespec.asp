﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<%
''' <summary>
''' Descarga el fichero
''' </summary>
''' <remarks>Llamada desde: usuppal\adjuntardoc.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")
Idioma = trim(Idioma)


set oRaiz=validarUsuario(Idioma,true,false,0)

		
lCiaComp = clng(oRaiz.Sesion.CiaComp)
CiaId = clng(oRaiz.Sesion.CiaId)

espec = Request("espec")
nombre=request("nombre")
dataSize = clng(request("datasize"))
		
OldTimeout=Server.ScriptTimeout	

Server.ScriptTimeout=3600
s=""
lowerbound=65
upperbound=90
randomize
for i = 1 to 10
	s= s & chr(Int((upperbound - lowerbound + 1) * Rnd + lowerbound))
next	


Set oEspecificacion = oRaiz.Generar_CEspecificacion
oEspecificacion.ID=espec
oEspecificacion.dataSize = datasize
oEspecificacion.nombre=nombre
set oEspecificacion.Cia=oraiz.Generar_CCompania
oEspecificacion.Cia.Id=CiaId

sPath = Application ("CARPETAUPLOADS") & "\" & s & "\" & CiaId & "\"
svPath = s & "/" & CiaId

if oespecificacion is nothing then
	set oraiz = nothing
    
	Response.End 
end if


set oError = oEspecificacion.escribirADisco(lCiaComp,sPath, oEspecificacion.Nombre)

%>
<script>
window.open("<%=Application("RUTASEGURA")%>script/common/download.asp?path=<%=server.urlencode(svPath)%>&nombre=<%=server.urlencode(oERror.arg1)%>&datasize=<%=server.urlencode(oEspecificacion.datasize)%>","_blank","top=100,left=150,width=630,height=300,location=no,menubar=yes,resizable=yes,scrollbars=no,toolbar=no")
</script>

<%
set oEspecificacion = nothing
set oRaiz = nothing

Response.End
%>
