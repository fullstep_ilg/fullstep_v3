﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script src="../js/arbol.js"></script>
</head>
<style>
A {font-family:Tahoma;font-size:12;text-decoration:none}
TD {font-family:Tahoma;font-size:12;}
.negro {font-family:Tahoma;font-size:12;color:black;text-decoration:none;}
.azul {font-family:Tahoma;font-size:12;color:blue;text-decoration:none;}
#divArbol {position:absolute;top:0;left:0;visibility:visible;z-index:1;}
#divEspera {position:absolute;top:0;left:0;width:400;clip:rect(0 400 15 0);visibility:hidden;z-index:200;}
</style>

<%
''' <summary>
''' Muestra las activadades de la empresa
''' </summary>
''' <remarks>Llamada desde: usuppal\modifactividadesclient.asp ; Tiempo máximo: 0,2</remarks>
dim CiaComp

Idioma = Request.Cookies("USU_IDIOMA")
Idioma = trim(Idioma)

set oRaiz=validarUsuario(Idioma,true,true,0)


CiaComp = clng(oRaiz.Sesion.CiaId)
    
set paramgen = oraiz.devolverParametrosGenerales
Dim Den

den=devolverTextos(Idioma,38)

dim numMaxActividades
numMaxActividades=paramgen.MaxActividades

txtEsperaCarga=den(11)
txtEspera=den(12)

%>


<script>

var recienCargada=true

//''' <summary>
//''' Carga los nodos hijos
//''' </summary>
//''' <remarks>Llamada desde: js\arbol.js/cargarHojasRama ; Tiempo máximo: 0,2</remarks>
function obtener(codigo)
{
var codigos
var l
var hayHojas
var ret

if (codigo=="Root")
	{
	return true
	}
	
codigos= codigo.split("_")

l=codigos.length
var imgcolor

recienCargada=false
ret=""
var h
switch(l)
	{
	case 1:
		h=window.open("<%=Application("RUTASEGURA")%>script/usuppal/ModifActividadesserver.asp?ACTN1=" + codigos[0],"fraModifActividadesServer") 
		break;
	case 2:
		window.open("<%=Application("RUTASEGURA")%>script/usuppal/ModifActividadesserver.asp?ACTN1=" + codigos[0] + "&ACTN2=" + codigos[1],"fraModifActividadesServer") 
		break;
	case 3:
		window.open("<%=Application("RUTASEGURA")%>script/usuppal/ModifActividadesserver.asp?ACTN1=" + codigos[0] + "&ACTN2=" + codigos[1] + "&ACTN3=" + codigos[2],"fraModifActividadesServer")
		break;
	case 4:
		window.open("<%=Application("RUTASEGURA")%>script/usuppal/ModifActividadesserver.asp?ACTN1=" + codigos[0] + "&ACTN2=" + codigos[1] + "&ACTN3=" + codigos[2] + "&ACTN4=" + codigos[3],"fraModifActividadesServer")
		break;
	default:
		return false
		break;
	}
document.getElementById("divEspera").style.top=document.body.scrollTop + 50
document.getElementById("divEspera").style.left=50
document.getElementById("divEspera").style.visibility="visible"
document.getElementById("divArbol").style.visibility="hidden"

return true
}


function crearRama(padre,hijo,cod,desc,nada,eshoja,nulo,color,chequeable,marca)
{
var rPadre
rPadre=eval("rama" + padre)

if (!rPadre.hojasCargadas)
	{
	eval("rama" + hijo + "=new Rama ('" + hijo + "',cod,desc,false,eshoja,null,color,chequeable,true)")
	eval("rama" + padre + ".add(rama" + hijo +")")
	}
}

function yaEsta(padre,esRama)
{
var rPadre
rPadre=eval("rama" + padre)
document.getElementById("divEspera").style.visibility="hidden"
document.getElementById("divArbol").style.visibility="visible"
if (!rPadre.hojasCargadas)
	{
	eval("rama" + padre + ".asyncWrite(esRama)")
	}
}

//''' <summary>
//''' Cargar el arbol de actividades
//''' </summary>
//''' <remarks>Llamada desde: init() ; Tiempo máximo: 0,2</remarks>
function cargarTodo()
{
<%

	Dim oACN1 
	Dim oACN2 
	Dim oACN3 
	Dim oACN4 
	Dim oACN5 

	dim Vacio
	dim Paramgen
	
	Vacio=true
	
	set paramgen = oRaiz.Sesion.Parametros
	set oCia = oraiz.generar_ccompania()
	ocia.id = ciaComp
	if Application("PYME") <> "" then
		set oActsN1 = oCia.DevolverEstructuraActividadesVisible ((Idioma), Application("PYME"))
	else
		set oActsN1 = oCia.DevolverEstructuraActividadesVisible (Idioma)
	end if
	
	if not oActsN1 is nothing then 
	
		For Each oACN1 In oActsN1
			Vacio=false
			%>
			rama<%=oACN1.id%>=new Rama ('<%=oACN1.id%>','<%=JSText(oACN1.Cod)%>','<%=JSText(oACN1.Den)%>',false,<%if oACN1.ExistenActividadesHijas then%>false<%else%>true<%end if%>,null,'DarkGoldenrod',<%if paramgen.NivelMinAct = 1 then%>true<%else%>false<%end if%>,true,true<%if oacn1.asignada then%>,'ü'<%end if%>)
			ramaRoot.add(rama<%=oACN1.Id%>)
			<%if oacn1.pendiente then%>
				rama<%=oACN1.Id%>.checked=true
			<%end if%>

			<%			
		    For Each oACN2 In oACN1.ActividadesNivel2

				%>
				rama<%=oACN1.Id%>_<%=oACN2.Id%>=new Rama ('<%=oACN1.Id%>_<%=oACN2.Id%>','<%=JSText(oACN2.Cod)%>','<%=JSText(oACN2.Den)%>',false,<%if oACN2.ExistenActividadesHijas then%>false<%else%>true<%end if%>,null,'green',<%if paramgen.NivelMinAct > 2 then%>false<%else%>true<%end if%>,true,true<%if oacn2.asignada or oacn1.asignada then%>,'ü'<%end if%>)
				rama<%=oACN1.Id%>.hojasCargadas=true
				rama<%=oACN1.Id%>.expanded = true
				rama<%=oACN1.Id%>.add(rama<%=oACN1.Id%>_<%=oACN2.Id%>)
				<%if oacn2.pendiente then%>
					rama<%=oACN1.Id%>_<%=oACN2.Id%>.checked=true
				<%end if%>

				<%			
				For Each oACN3 In oACN2.ActividadesNivel3
					%>
					rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>=new Rama ('<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>','<%=JSText(oACN3.Cod)%>','<%=JSText(oACN3.Den)%>',false,<%if oACN3.ExistenActividadesHijas then%>false<%else%>true<%end if%>,null,'blue',<%if paramgen.NivelMinAct > 3 then%>false<%else%>true<%end if%>,true,true<%if oacn3.asignada or oacn2.asignada or oacn1.asignada then%>,'ü'<%end if%>)
					rama<%=oACN1.Id%>_<%=oACN2.Id%>.hojasCargadas=true
					rama<%=oACN1.Id%>_<%=oACN2.Id%>.expanded=true
					rama<%=oACN1.Id%>_<%=oACN2.Id%>.add(rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>)
					<%if oacn3.pendiente then%>
						rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>.checked=true
					<%end if%>
					<%			
					For Each oACN4 In oACN3.ActividadesNivel4
						%>
						rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>=new Rama ('<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>','<%=JSText(oACN4.Cod)%>','<%=JSText(oACN4.Den)%>',false,<%if oACN4.ExistenActividadesHijas then%>false<%else%>true<%end if%>,null,'brown',<%if paramgen.NivelMinAct > 4 then%>false<%else%>true<%end if%>,true,true<%if oacn4.asignada or oacn3.asignada or oacn2.asignada or oacn1.asignada then%>,'ü'<%end if%>)
						rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>.hojasCargadas=true
						rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>.expanded=true
						rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>.add(rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>)
						<%if oacn4.pendiente then%>
							rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>.checked=true
						<%end if%>
						<%			
		                For Each oACN5 In oACN4.ActividadesNivel5
							%>
							rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>_<%=oACN5.Id%>=new Rama ('<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>_<%=oACN5.Id%>','<%=JSText(oACN5.Cod)%>','<%=JSText(oACN5.Den)%>',true,true,null,'gray',<%if paramgen.NivelMinAct > 5 then%>false<%else%>true<%end if%>,true,true<%if oacn5.asignada or oacn4.asignada or oacn3.asignada or oacn2.asignada or oacn1.asignada then%>,'ü'<%end if%>)
							rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>.hojasCargadas=true
							rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>.expanded = true
							rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>.add(rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>_<%=oACN5.Id%>)
							<%if oacn5.pendiente then%>
								rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>_<%=oACN5.Id%>.checked=true
							<%end if%>
							<%			
		                Next
		                set oACN4.ActividadesNivel5 = nothing
		            Next
		            set oACN3.ActividadesNivel4 = nothing
				Next
				set oACN2.ActividadesNivel3 = nothing
		    Next
		    set oACN1.ActividadesNivel2 = nothing
		Next
		set oActsN1 = nothing
    end if   
set oprove = nothing
set paramgen = nothing
set oraiz = nothing

%>
}


function init()
{

ramaRoot=new Rama ('Root','Root','<%=den(14)%>',false,false,null,'DarkGoldenrod',false,true)
cargarTodo()

ramaRoot.write(null)
ramaRoot.expcont()

}

var p
p=window.parent

function obtenerActividadesSeleccionadas()
{
p.document.forms["frmActividades"].txtNumActs.value=0
p.document.getElementById("divActividades").innerHTML=""
recorrerArbol(ramaRoot)
}

function recorrerArbol(rama)
{
var i
var Acts
if (rama.checked)
	{
	if (!rama.parent.checked)
		{
		p.document.forms["frmActividades"].txtNumActs.value = parseInt(p.document.forms["frmActividades"].txtNumActs.value) + 1
		Acts=p.document.forms["frmActividades"].txtNumActs.value
	
		str="<input type=hidden name=txtAct" + Acts + " value='" + rama.id + "'>"
		str+="<input type=hidden name=txtCodAct" + Acts + " value='" + rama.cod + "'>"
		p.document.getElementById("divActividades").innerHTML += str
		}
	}
for (i=0;i<rama.hojas.length;i++)
	{
	recorrerArbol(rama.hojas[i])
	}
}

	
var antSeleccionada
antSeleccionada=null

function desselParent(rama)
{
var chkDesel

if (rama.id!='Root' && rama.chequeable)
	{
	rama.checked=false
	expanded = dessel(rama)
	if (expanded)
		{
		chkDesel = eval("document.forms['frmArbol'].chkRama" + rama.id )
		chkDesel.checked=false
		}
	desselParent(rama.parent)
	}
return
}

function selHojas(rama,valor)
{
var i
var chkSel
var expanded

expanded = dessel(rama)
if (expanded)
	{
	chkSel = eval("document.forms['frmArbol'].chkRama" + rama.id )
	chkSel.checked=valor
	}
rama.checked=valor
for (i=0;i<rama.hojas.length;i++)
	{
	if (rama.hojas[i].chequeable)
		{
		selHojas(rama.hojas[i],valor)
		}
		
	}

}


function seleccionar(codigo)
{
obtenerActividadesSeleccionadas()
var rama
rama=eval("rama"+ codigo)

if ((<%=numMaxActividades%> == 0) || (<%=numMaxActividades%> >= p.document.forms["frmActividades"].txtNumActs.value))
{
    selHojas(rama,true) 
    return true
}
else
{
    alert('<%=JSAlertText(Replace(den(18),"#Num",numMaxActividades))%>')
    desseleccionar(codigo)
    return false
}    

}


function desseleccionar(codigo)
{
var rama
rama=eval("rama"+ codigo)
desselParent(rama)
selHojas(rama,false)
}


</script>


<body onload="init()" topmargin="1" leftmargin="1">
<form name="frmArbol">
<div name="divArbol" id="divArbol">
<font size="1" face="tahoma" color="darkblue"><%=txtEspera%></font>
</div>

<div name="divEspera" id="divEspera">
<font size="2" face="tahoma" color="darkblue"><%=txtEsperaCarga%></font>
</div>
</form>
</body>
</html>
