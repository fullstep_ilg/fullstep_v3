﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<%
''' <summary>
''' Borra la especificación
''' </summary>
''' <remarks>Llamada desde: usuppal\adjuntardoc.asp ; Tiempo máximo: 0,2</remarks>


Idioma = request.cookies("USU_IDIOMA")

dim oRaiz	
dim bEliminar 

set oRaiz=validarUsuario(Idioma,true,false,0)

CiaId = clng(oRaiz.Sesion.CiaId)

set oEspecificacion = oRaiz.Generar_CEspecificacion
oEspecificacion.id = Numero(request("espec"))

bEliminar=Request("bElim")

IF bEliminar=1 then
	set oError = oEspecificacion.EliminarCiaEsp(CiaId)
else
	oEspecificacion.nombre=Request("sNombre")
	oEspecificacion.comentario=Request("sComent")
	set oError = oEspecificacion.ModificarCiaEsp(CiaId)
end if

%>

<html>
<script>
function init()
	{
	<%if bEliminar=1 then%>
		var p
		p=window.parent.frames["fraEspecClient"]
		p.eliminaAdjun(<%=oEspecificacion.Id%>)
	<%end if%>
	window.close()
	}
</script>

<body onload="init()">

</body>
</html>

<%
set oEspecificacion = nothing
set oRaiz = nothing

%>