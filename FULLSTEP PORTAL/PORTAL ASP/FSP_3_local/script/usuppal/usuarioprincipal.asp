﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<%
''' <summary>
''' Cambia el usuario principal del portal
''' </summary>
''' <remarks>Llamada desde: common\menu.asp ; Tiempo máximo: 0,2</remarks>

if Request.ServerVariables("CONTENT_LENGTH")>0 then
	idUsu=clng(request("lstUsuarios"))
end if
%>
<HTML>
<HEAD>
<% 
    
%>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">

<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/menu.asp"></script>
<script>dibujaMenu(6)</script>
<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
    document.getElementById('tablemenu').style.display = 'block';
}
</script>
</HEAD>
<BODY TOPMARGIN=0 leftmargin=0 onload="init()">



<%
''' <summary>
''' Cambia el usuario principal del portal
''' </summary>
''' <remarks>Llamada desde: common\menu.asp ; Tiempo máximo: 0,2</remarks>

Idioma=trim(Request.Cookies("USU_IDIOMA"))

set oRaiz=validarUsuario(Idioma,true,false,0)

set oCompania = oRaiz.generar_CCompania()
oCompania.Id = clng(oRaiz.Sesion.CiaId)


if Request.ServerVariables("CONTENT_LENGTH")>0 then

	ocompania.EstablecerUsuarioPrincipal idusu,Request.Cookies("USU_SESIONID")

%>

<script>
    window.open("<%=Application("RUTASEGURA")%>script/usuppal/okguardarUsuario.asp?accion=Principal","_self")
</script>
<%
end if

oCompania.IdPortal = Application("PORTAL")
set oError = oCompania.cargarusuarioscia(true)


Dim Den

den=devolverTextos(Idioma,72)



%>


<%
for each oUsuario in oCompania.usuarios
	if clng(oRaiz.Sesion.UsuId)=oUsuario.id then 
		nombreActualPpal = oUsuario.Nombre & " " & oUsuario.apellidos
	end if
next
%>

<h1><%=den(1)%></h1>
<h3><%=den(2) & "&nbsp;<b>" & nombreActualPpal & "</b>"%></h3>
<h3><%=den(3)%></h3>
<h3><%=den(4)%></h3>
<form name=frmUsuarios method=post >
<input type=hidden name =Idioma value="<%=Idioma%>">
<table class=principal cellpadding=0 cellspacing=5 border="0" width="85%" align=center>
	<tr>
		<td align=center  height="58">
			<select size="1" name="lstUsuarios" width=100%>
				<%for each oUsuario in oCompania.usuarios%>
				<option value=<%=oUsuario.Id%>><%=oUsuario.Nombre & " " & oUsuario.apellidos %></option>
				<%next%>
			</select>
		</td>
	</tr>
	<tr>
		<td align=center >
			<INPUT class=button id=cmdAceptar name=cmdAceptar type=submit value="<%=den(5)%>">
		</td>
	</tr>
</table>
</form>

<%

set oCompania.usuarios = nothing
set oCompania = nothing

set oRaiz = nothing
%>
</BODY>
</HTML>
