﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<% 
''' <summary>
''' Mensaje q indica error en los cambios de datos de companias compradoras
''' </summary>
''' <remarks>Llamada desde: usuppal\modifregcompradoras.asp ; Tiempo máximo: 0,2</remarks>


Idioma=trim(Request.Cookies("USU_IDIOMA"))
set oRaiz=validarUsuario(Idioma,true,false,0)

IdProve = oRaiz.Sesion.CiaId
Dim Den

den=devolverTextos(Idioma,65)
%>

<html>
<head>
<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
    function init() {
        document.getElementById('tablemenu').style.display = 'block';
    }
</script>
</head>
<script SRC="../common/menu.asp"></script>
<body topmargin=0 leftmargin=0 onload="init()">
<%dibujarMenu%>
	<h1><%=Den(1)%></h1>
	<h3><%=Den(2)%></h3>
	<h3><%=Den(3)%></h3>
	<p class=principal>
	<a href="mailto:<%=Application("MAILPORTAL")%>"><%=Application("MAILPORTAL")%></a></p>
</body>
</html>
<%


%>