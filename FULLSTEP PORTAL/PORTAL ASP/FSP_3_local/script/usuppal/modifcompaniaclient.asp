﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<script SRC="../common/menu.asp"></script>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">

<script SRC="../common/formatos.js"></script>


</HEAD>
<BODY topmargin=0 leftmargin=0 onload="init()">


<%
''' <summary>
''' Para modificar datos de la compania, esta es la parte visible , en la q se ven los datos y puedes cambiarlos.
''' </summary>
''' <remarks>Llamada desde: usuppal\modifcompania.asp ; Tiempo máximo: 0,2</remarks>

Idioma = trim(Request.Cookies("USU_IDIOMA"))

set oRaiz=validarUsuario(Idioma,true,true,0)
codigo = Request("codigo")

CiaId = oRaiz.Sesion.CiaId
decimalfmt = Request.Cookies ("USU_DECIMALFMT")
thousanfmt = Request.Cookies ("USU_THOUSANFMT") 
precisionfmt = CINT(Request.Cookies ("USU_PRECISIONFMT") )

set oCompania = oRaiz.devolverDatosCia (CiaId, Application("PORTAL"), false)


Dim Den
Dim hayProvi

den=devolverTextos(Idioma,67)

set oPaises = oraiz.Generar_CPaises 
			
oPaises.CargarTodosLosPaises Idioma,,,,,true

set oPais = oPaises.Item(ocompania.codpais)
oPais.CargarTodasLasProvincias ,,,true

set oMonedas = oraiz.Generar_CMonedas
			
oMonedas.CargarTodasLasMonedasDelPortalDesde Idioma,1000					
set paramgen = oRaiz.Sesion.Parametros

'Recuperamos la denominación de la moneda central
denominacionMoneda = oRaiz.ObtenerDenMonedaCentral(Idioma)

if oPais.Provincias.Count = 0 then
    hayProvi = false
else
    hayProvi = true
end if


%>
<script>    dibujaMenu(6)</script>
<script>

var provinciaObligatoria

<%if hayProvi then %>

provinciaObligatoria = true

<%else %>

provinciaObligatoria = false

<%end if %>
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{    
if ("<%=Request("codigo")%>" == "errorExisteNIFCompania")
	{
		alert("<%=den(29)%>");
	}
else if ("<%=codigo%>" !== "")
	{
		alert("<%=codigo%>");
	}
document.getElementById('tablemenu').style.display = 'block';
}
</script>
<script language="Javascript1.2">

vdecimalfmt='<%=decimalfmt%>'
vthousanfmt='<%=thousanfmt%>'
/*
''' <summary>
''' Obtine la lsuta de provincias del pais seleccionado
''' </summary>    
''' <remarks>Llamada desde: lstPai/onchange ; Tiempo máximo: 0</remarks>*/
function obtenerProvi()
{
var Pais
Pais=document.forms["frmCompania"].lstPai[document.forms["frmCompania"].lstPai.selectedIndex].value

if (Pais=="")
	{
	borrarProvi()

	document.forms["frmCompania"].lstProvi.options[0]=new Option ("",-1)
	return
	}

window.open("<%=Application("RUTASEGURA")%>script/usuppal/modifcompaniaserver.asp?Pais=" + Pais,"fraModifCompaniaServer")

}

/*
''' <summary>
''' comprobar si es obligatorio seleccionar una provincia
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: modifcompaniaserver.asp Tiempo máximo: 0</remarks>
''' <revision>JVS 16/06/2011</revision>
*/
function comprobarProviOblig()
{
    if(document.forms["frmCompania"].lstProvi.length > 1)
    {
        provinciaObligatoria = true
        document.getElementById("prov").innerHTML = "<%=Den(30) %>"
    }
    else
    {
        provinciaObligatoria = false
        document.getElementById("prov").innerHTML    = "<%=Den(10) %>"
    }

}

function borrarProvi()
{
while(document.forms["frmCompania"].lstProvi.options.length>0)
	{
	document.forms["frmCompania"].lstProvi.options.remove(document.forms["frmCompania"].lstProvi.options.length - 1)
	}
}

function ValidarCP()
{
    f = document.forms["frmCompania"]
    var urlregex = new RegExp("^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$");
    return urlregex.test(f.txtCP.value);
}

function ValidarDirWebEmpresa()
{
    f = document.forms["frmCompania"]
    var urlregex = new RegExp("^(https?://)?(([\\w!~*'().&=+$%-]+: )?[\\w!~*'().&=+$%-]+@)?(([0-9]{1,3}\\.){3}[0-9]{1,3}|([\\w!~*'()-]+\\.)*([\\w^-][\\w-]{0,61})?[\\w]\\.[a-z]{2,6})(:[0-9]{1,4})?((/*)|(/+[\\w!~*'().;?:@&=+$,%#-]+)+/*)$");
                               
                            
    return urlregex.test(f.txtURLCia.value);
}


function anyadirProvi(Id,Den)
{
document.forms["frmCompania"].lstProvi.options[document.forms["frmCompania"].lstProvi.options.length]=new Option(Den,Id)
}

/*
''' <summary>
''' realiza las validaciones del formulario
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: modifcompaniaclient.asp Tiempo máximo: 0</remarks>
''' <revision>JVS 16/06/2011</revision>
*/
function validar()
{
var f
var errorProvincia = false
f = document.forms["frmCompania"]

//Si la provincia es obligatoria, se comprueba si en la lista de provincias se ha seleccionado alguna
if (provinciaObligatoria && (document.forms["frmCompania"].lstProvi.value == "" || document.forms["frmCompania"].lstProvi.value == "-1"))
    errorProvincia = true

if ( f.txtDen.value=="" || f.txtDir.value=="" || f.txtCP.value=="" || f.txtPob.value=="" || f.txtNIF.value=="" || errorProvincia 
    <%if UCase(application("NOMPORTAL"))="GES" then%> || trim(f.txtComent.value)==""<%end if%>) 
	{	
	alert("<%=den(20)%>");
	return false
	}
if (!ValidarNIFCIF())
	return false

if (f.txtURLCia.value !== "") {
    if (!ValidarDirWebEmpresa())
    {
        alert("<%=den(32)%>");
        return false;
    }
}

if (document.forms["frmCompania"].lstPai[document.forms["frmCompania"].lstPai.selectedIndex].value==198)
{
if (!ValidarCP())
    {
    alert("<%=den(31)%>");
    return false;
    }
}

    
	
return true;
}
    



<%		
if paramGen.CompruebaNif and not paramGen.BloqModifProve then
%>
function ValidarNIFCIF()

{
        
f = document.forms["frmCompania"]
switch(parseInt(f.lstPai.options[f.lstPai.selectedIndex].validNif))
	{
	case 0:
		return true
		break;
	case 1:
		if(f.txtNIF.value=="") 
			return true
		if (VALIDAR_CIF(f.txtNIF.value))
			return true
			
		if (VALIDAR_NIF(f.txtNIF.value))
			return true

		alert("<%=JSAlertText(den(25))%>")
		return false
		break;
	default:
		return true
	}
}
<%else%>
function ValidarNIFCIF()
{
return true
}
<%end if

%>




</script>


<h1><%=den(1)%></h1>
<h3><%=den(2)%></h3>
<h4><%=den(3)%></h3>

<form name=frmCompania method=post action="guardarcompania.asp"  language="javascript1.2" onsubmit="return validar()">

<input type=hidden name=Idioma value="<%=Idioma%>">
<input type=hidden name=txtPENDCONF value="<%=cint(ocompania.PendienteConfirm)%>">
<input type=hidden name=CodCia value="<%=oCompania.cod%>">

<TABLE class=principal bgcolor="white" border=0 cellPadding=3 cellSpacing=0 width="80%">
	<TR>
		<TD class="filaImpar negrita">
			<%=den(4)%>
		</TD>
		<TD class="filaImpar negrita">
			<%
				dim codCia
				if Application("PYME") <> "" then
					If InStr(1, oCompania.cod, Application("PYME")) = 1 Then
						codCia = Replace(oCompania.cod, Application("PYME"), "", 1, 1)
					else
						codCia = oCompania.cod
					End If
				else
					codCia = oCompania.cod
				end if
			%>
			<%=codCia%>
		</TD>
	</TR>
	<TR>
		<TD class="filaPar negrita">
			<%=den(5)%>
		</TD>
		<TD class=filaPar>			 
			<INPUT  type="text" id="txtDen"  name="txtDen" size=72 maxLength=100 value="<%=ocompania.den%>">			
		</TD>
	</TR>
	<TR>
		<TD class="filaImpar negrita">
			<%=den(6)%>
		</TD>
		<TD class=filaImpar>
			<INPUT  type="text" id="txtDir" name="txtDir" size=72 maxLength=255 value="<%=ocompania.direccion%>">
		</TD>
	</TR>
	<TR>
		<TD class="filaPar negrita">
			<%=den(7)%>
		</TD>
		<TD  class=filaPar>
			<INPUT  type="text" id="txtCP" name="txtCP" size=14 maxLength=20 value="<%=ocompania.cp%>">
		</TD>
	</TR>
	<TR>
		<TD class="filaImpar negrita">
			<%=den(8)%>
		</TD>
		<TD class=filaImpar>
			<INPUT  type="text" id="txtPob" name="txtPob" size=36 maxLength=100 value="<%=ocompania.poblacion%>">
		</TD>
	</TR>
	<TR>
		<TD class="filaPar negrita">
			<%=den(9)%>
		</TD>
		<TD  class=filaPar>
			<SELECT name=lstPai  size=1 onchange="obtenerProvi()">
				<%for each otPais in oPaises%>
				<option validNif=<%=otPais.ValidNif%> value=<%=otPais.id%> <%if otPais.cod=ocompania.codpais then%> SELECTED<%end if%>><%=otPais.den%></option>
				<%next%>
			
			</SELECT>
		</TD>
	</TR>
	<TR>
		<TD class="filaImpar negrita">
            <label id="prov" name="prov"><%if hayProvi then %><%=den(30)%><%else %><%=den(10)%><%end if %></label>
		</TD>
		<TD  class=filaImpar>
			<SELECT name=lstProvi  size=1>
				<option value=-1 ></option>
			<%
			for each oProvi in oPais.Provincias%>
				<option value=<%=oProvi.id%> <%if oprovi.cod = ocompania.codprovi then%> SELECTED<%end if%>><%=oProvi.den%></option>
			<%next
			
			set oPais.Provincias = nothing 
			set oPais = nothing
			set oPaises = nothing
			%>
			</SELECT>
		</TD>
	</TR>
	<TR>
		<TD class="filaPar negrita">
			<%=den(11)%>
		</TD>
		<TD class=filaPar>			
			<SELECT name=lstMon size=1>
			
			<%for each oMon in oMonedas%>
				<option value=<%=oMon.Id%> <%if omon.cod= ocompania.codmon then%> SELECTED<%end if%>><%=oMon.Den & " (" & oMon.Cod & ")"%></option>
			<%next
			set oMonedas = nothing
			%>
			</SELECT>			
		</TD>
	</TR>
	<TR>
		<TD class="filaImpar negrita">
			<%=den(12)%>
		</TD>
		<TD class=filaImpar>
			<SELECT name=lstIdi  size=1>
			<%set adorIdi = oRaiz.DevolverIdiomas
			while not adorIdi.eof%>
				<OPTION  value="<%=adorIdi.fields("COD").value%>"<%if ocompania.Idioma=adorIdi.fields("COD").value then%> selected<%end if%>><%=adorIdi.fields("DEN").value%></OPTION>
				<%adorIdi.movenext
			wend
			adorIdi.close
			set adorIdi = nothing%>
			</SELECT>
		</TD>
	</TR>
	<TR>
		<TD class="filaPar negrita">
			<%=den(13)%>
		</TD>
		<TD  class=filaPar>
			<%if paramgen.BloqModifProve then%>
				<input type=hidden  name=txtNIF value="<%=ocompania.nif%>">
				<%=ocompania.nif%>
			<%else%>
				<INPUT  type="text" id="txtNIF" name="txtNIF" size=20 maxLength=20 value="<%=ocompania.nif%>" onchange="return ValidarNIFCIF()">
			<%end if%>
		</TD>
	</TR>
	<TR>
		<TD class="filaImpar negrita">
			<%=den(14)%>
		</TD>
		<TD  class=filaImpar>
			<INPUT  type="text" id="txtVolFac" name="txtVolFac" size=20 maxLength=20 onchange="return validarNumero(this,'<%=decimalfmt%>','<%=thousanfmt%>','<%=precisionfmt%>');" onkeypress="return mascaraNumero(this,'<%=decimalfmt%>','<%=thousanfmt%>',event);"  value="<%=VisualizacionNumero(ocompania.volfac,decimalfmt,thousanfmt,precisionfmt)%>">
			<font size=1 face=tahoma>&nbsp;
				(x 1000 <%=denominacionMoneda%>)
			</font>
		</TD>
	</TR>
	<TR>
		<TD class="filaPar negrita">
			<%if application("NOMPORTAL") = "huf" then%>
				<%=den(26)%>
			<%else%>
				<%=den(15)%>
			<%end if%>
		</TD>
		<TD  class=filaPar>
			<INPUT  type="text" id="txtCliRef1" name="txtCliRef1" size=71 maxLength=50 value="<%=ocompania.cliref1%>">
		</TD>
	</TR>
	<TR>
		<TD  class="filaImpar negrita">&nbsp;</TD>
		<TD  class=filaImpar>
			<INPUT  type="text" id="txtCliRef2" name="txtCliRef2" size=71 maxLength=50 value="<%=ocompania.cliref2%>">
		</TD>
	</TR>
	<TR>
		<TD class="filaPar negrita" >&nbsp;</TD>
		<TD  class=filaPar>
			<INPUT  type="text" id="txtCliRef3" name="txtCliRef3" size=71 maxLength=50 value="<%=ocompania.cliref3%>">
		</TD>
	</TR>
	<TR>
		<TD class="filaImpar negrita">&nbsp;</TD>
		<TD class=filaImpar>
			<INPUT  type="text" id="txtCliRef4" name="txtCliRef4" size=71 maxLength=50 value="<%=ocompania.cliref4%>">
		</TD>
	</TR>
	<TR>
		<TD class="filaPar negrita">
				<%=den(16)%>
		</TD>
		<TD class=filaPar>
			<INPUT  type="text" id="txtHom1" name="txtHom1" size=20 maxLength=50 value="<%=ocompania.Hom1%>">
		</TD>
	</TR>
	<TR>
		<TD class="filaImpar negrita">&nbsp;</TD>
		<TD class=filaImpar>
			<INPUT  type="text" id="txtHom2" name="txtHom2" size=20 maxLength=50 value="<%=ocompania.Hom2%>">
		</TD>
	</TR>
	<TR>
		<TD class="filaPar negrita">&nbsp;</TD>
		<TD class=filaPar>
			<INPUT  type="text" id="txtHom3" name="txtHom3" size=20 maxLength=50 value="<%=ocompania.Hom3%>">
		</TD>
	</TR>
	<TR>
		<TD class="filaImpar negrita">
			<%=den(17)%>
		</TD>
		<TD class=filaImpar>
			<INPUT  type="text" id="txtURLCia" name="txtURLCia" size=51 maxLength=255 value="<%=ocompania.urlcia%>">
		</TD>
	</TR>
	<TR>
		<TD class="filaPar negrita" valign="top">
			<%if UCase(application("NOMPORTAL"))="GES" then%>
                <span><%=den(33)%><br><br>
				<%=den(34)%></span>
            <%else
			    if paramGen.ActivarRegistroActiv then%>
				    <span><%=den(27)%><br><br>
				    <%=den(28)%></span>
			    <%else%>
				    <span><%=den(18)%><br><br>
				    <%=den(24)%></span>
			    <%end if
            end if%>	
			
		</TD>
		<TD class=filaPar>
			<TEXTAREA  id="txtComent" name="txtComent" rows=3 cols=61><%=ocompania.coment%></TEXTAREA>
		</TD>
	</TR>
</TABLE>

<br>

<table class=principal width=95%>
	<tr>
		<td align = center>
			<INPUT class=button type=submit name="cmdGuardar" id="cmdGuardar" value="<%=den(19)%>" >
		</td>
	</tr>
</table>

<br>

</form>

</BODY>
<%
set ocompania = nothing

set oraiz = nothing

set paramgen = nothing
%>
</HTML>
