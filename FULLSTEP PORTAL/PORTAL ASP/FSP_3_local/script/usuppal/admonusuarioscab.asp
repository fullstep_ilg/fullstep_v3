﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/menu.asp"></script>

<SCRIPT>
function cambiar()
{
f=document.forms["frmUsuarios"]

if (f.lstUsuarios.options[f.lstUsuarios.selectedIndex].value=="-1")
	{
	return false
	}
f.submit()
return true
}
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
    function init() {
        document.getElementById('tablemenu').style.display = 'block';
    }
</SCRIPT>
</HEAD>
<BODY TOPMARGIN=0 leftmargin=0 onload="init()">


<%
''' <summary>
''' Muestra los usuarios de la compania y deja añadir nuevos usuarios.
''' </summary>
''' <remarks>Llamada desde: usuppal\admonusuarios.asp   usuppal\okguardarusuario.asp    usuppal\errorguardarusuario.asp; Tiempo máximo: 0,2</remarks>


Idioma=trim(Request.Cookies("USU_IDIOMA"))

set oRaiz=validarUsuario(Idioma,true,false,0)


set oCompania = oRaiz.generar_CCompania()

oCompania.Id = clng(oRaiz.Sesion.CiaId)
oCompania.IdPortal = Application("PORTAL")
set oError = oCompania.cargarusuarioscia(true)


Dim Den

den=devolverTextos(Idioma,61)



%>

<script>    dibujaMenu(6)</script>

<form name=frmUsuarios target=fraAdmonUsuariosDetalle method=post action=admonusuariosdet.asp>

<h1><%=den(1)%></h1>
<h3><%=den(2)%></h3>

<input type=hidden name =Idioma value="<%=Idioma%>">
<table class=principal cellpadding=0 cellspacing=0 border="0" width="85%" align=left>
	<tr>
		<td width="76%">
			<select onchange="cambiar()" size="1" name="lstUsuarios" width=100%>
				<option value=-1></option>
				<%for each oUsuario in oCompania.usuarios%>
				<option value=<%=oUsuario.Id%>><%=oUsuario.Nombre & " " & oUsuario.apellidos %></option>
				<%next%>

			</select>
		</td>
		<td width="50%">
			<INPUT class=button id=cmdNuevo name=cmdNuevo type=submit value="<%=den(3)%>">
		</td>
	</tr>
</table>
<p>&nbsp;</p>
</form>

<%

set oCompania.usuarios = nothing
set oCompania = nothing

set oRaiz = nothing
%>
</BODY>
</HTML>
