﻿<%@EnableSessionState=False%>
<!--#include file="../common/acceso.asp"-->
<% Response.Expires = -1 %>
<%
''' <summary>
''' Mostrar la barra de progreso mientras se graba el adjunto
''' </summary>
''' <remarks>Llamada desde: upload.asp ; Tiempo máximo: 0,2</remarks>
Idioma = Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,false,false,0)
sExtensionIncorrecta = oRaiz.VerificarExtensionUpload_SinGrabar(Request.QueryString("NombreArchivo"))
set oRaiz = nothing

if (not sExtensionIncorrecta = "") then%>
<HTML>
<script>
    function init() {
        window.setTimeout(window.close(), 20)
    }
</script>
<BODY onload="init()">

<P>&nbsp;</P>

</BODY>
</HTML>
<%else

dim den (500)

den(1) = "Uploading files"
den(2) = "Upload progress"

%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><%=title%></title>
<style type='text/css'>td {font-family:arial; font-size: 9pt }</style>
</HEAD>

<BODY BGCOLOR="#C0C0B0">
<IFRAME src="bar.asp?PID=<%= Request("PID") & "&to=" & Request("to") %>" title="<%=den(2)%>" noresize scrolling=no
frameborder=0 framespacing=10 width=369 height=65></IFRAME>

</BODY>

</HTML>
<%end if%>
