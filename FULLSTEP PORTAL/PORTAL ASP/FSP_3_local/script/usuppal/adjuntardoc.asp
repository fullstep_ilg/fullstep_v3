﻿<!DOCTYPE html PUBLIC  >
<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">

<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/formatos.js"></script>
<script SRC="../common/menu.asp"></script>
<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
    document.getElementById('tablemenu').style.display = 'block';
}
</script>
</HEAD>
<BODY TOPMARGIN=0 leftmargin=0 onload="init()">



<%
''' <summary>
''' Muestra los adjuntos permitiendo descargarlos, borrarlos y añadir nuevos.
''' </summary>
''' <remarks>Llamada desde: usuppal\especificaciones.asp ; Tiempo máximo: 0,2</remarks>

Idioma=trim(Request.Cookies("USU_IDIOMA"))

set oRaiz=validarUsuario(Idioma,true,false,0)

set oCompania = oRaiz.generar_CCompania()
CiaId = clng(oRaiz.Sesion.CiaId)
set oCompania = oRaiz.devolverDatosCia (CiaId, Application("PORTAL"), false)


Dim Den
dim EspacioConsumido
dim EspacioLibre

den=devolverTextos(Idioma,119)

set oError =oCompania.CargarTodasLasEspecificaciones

EspacioConsumido=0
if oCompania.Especificaciones.count>0 then
	for each oAdjun in oCompania.Especificaciones
		EspacioConsumido=EspacioConsumido + oAdjun.dataSize
		EspacioLibre=oCompania.MaxSizeAdjun-EspacioConsumido
	next
else
	EspacioLibre=oCompania.MaxSizeAdjun
end if
%>
<script>    dibujaMenu(6)</script>

<h1><%=den(1)%></h1>
<h3><%=den(2) & "&nbsp;" & oCompania.den %></h3>

<script>
var vTmpComentario 
var vTmpNombre 
var vdecimalfmt 
var vthousanfmt
var vEspacioConsumido
var vEspacioLibre

vEspacioConsumido=<%=EspacioConsumido%>
vEspacioLibre=<%=EspacioLibre%>

vdecimalfmt='<%=decimalfmt%>'
vthousanfmt='<%=thousandfmt%>'


function insertarCelda (TR,sClase,sHTML,vAlign, iRowSpan,vAlign2)
	{
	var oTD
	
	oTD = TR.insertCell(-1)
	oTD.className=sClase

	if (vAlign)
		oTD.style.verticalAlign=vAlign
	
	if (iRowSpan)
		oTD.rowSpan = iRowSpan

	if (vAlign2==true)
		{
		oTD.align="center"
		}
		
	oTD.innerHTML +=sHTML

	}
/*
''' <summary>
''' Descarga un adjunto
''' </summary>
''' <param name="id">id de input adjunto</param> 
''' <remarks>Llamada desde: N inputs de esta pantalla ; Tiempo máximo: 0</remarks>*/	
function downloadAdjunto(id)
{	
	f = document.forms["frmAdjuntos"]
	Adjunto = f.elements["txtAdjId_" + id].value
	nombre  = f.elements["txtAdjNombre_" + id].value
	datasize = f.elements["txtAdjTamanyo_" + id].value
	
	window.open("<%=Application("RUTASEGURA")%>script/usuppal/downloadespec.asp?download=1&espec=" + Adjunto + "&nombre=" + nombre + "&datasize=" + datasize ,"fraEspecServer")

}
/*
''' <summary>
''' Añade un adjunto
''' </summary>
''' <param name="id">id de input adjunto</param>    
''' <remarks>Llamada desde: N inputs de esta pantalla ; Tiempo máximo: 0</remarks>*/
function anyadirAdjunto()
{

var winAdjun		
winAdjun=window.open("<%=Application("RUTASEGURA")%>script/usuppal/upload.asp?TargetURL=<%=Server.URLEncode(strFECHAHORA)%>&kbACTUAL=" + vEspacioConsumido + "&maxADJUN=<%=oCompania.MaxSizeAdjun%>" ,"_blank","top=100,left=150,width=410,height=250,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
winAdjun.focus()
	
}
/*
''' <summary>
''' Modificar un adjunto
''' </summary>
''' <param name="id">id de input adjunto</param>
''' <remarks>Llamada desde: N inputs de esta pantalla ; Tiempo máximo: 0</remarks>*/
function modificarAdjunto(id)
{
var oTABLE = document.getElementById("tblAdjuntos")
lRows = oTABLE.rows.length
	for (var i=0;i<=lRows;i++)
	{
		fid = null
		if(document.forms["frmAdjuntos"].elements["txtAdjId_" + i])
			fid = document.forms["frmAdjuntos"].elements["txtAdjId_" + i].value
			
		if (id==fid && id!=null)
		  {
			window.open("<%=Application("RUTASEGURA")%>script/usuppal/eliminarespec.asp?espec=" + id + "&bElim=0&sNombre=" + document.forms["frmAdjuntos"].elements["txtAdjNombre_" + i].value + "&sComent=" + document.forms["frmAdjuntos"].elements["txtAdjComentario_" + i].value,"fraEspecServer")
			//Sale del bucle
			i=lRows
		  }
	}

}

function eliminarAdjunto(id)
{
if (confirm("<%=JSText(den(13))%>"))
	{	
	window.open("eliminarespec.asp?espec=" + id + "&bElim=1","fraEspecServer")
	}
}


function eliminaAdjun(id)
{
var iSize 

	var oTABLE = document.getElementById("tblAdjuntos")
	lRows = oTABLE.rows.length
	
	for (var i=0;i<=lRows;i++)
	{
		fid = null
		if(document.forms["frmAdjuntos"].elements["txtAdjId_" + i])
			fid = document.forms["frmAdjuntos"].elements["txtAdjId_" + i].value
			
		if (id==fid && id!=null)
			{
			//Elimina el adjunto seleccionado de la tabla:
			oDiv = document.forms["frmAdjuntos"].elements["txtAdjId_" + i].parentNode
			oTD = oDiv.parentNode
			oTR = oTD.parentNode
			iSize = document.forms["frmAdjuntos"].elements["txtAdjTamanyo_" + i].value
			oTABLE.deleteRow(oTR.rowIndex)
			
				
			//Actualiza los valores de espacio consumido y espacio libre:
			vEspacioConsumido-= iSize
			vEspacioLibre=<%=oCompania.MaxSizeAdjun%>-vEspacioConsumido

			oTABLE = document.getElementById("tblAdjun")
			Rows = oTABLE.rows.length
			oTR =oTABLE.rows[1]

			str="<td class=sombreado><nobr><%=den(4)%>&nbsp;"
			str+= num2str(vEspacioConsumido/1024,vdecimalfmt,vthousanfmt,0)
			str+="&nbsp;Kb.&nbsp;"
			str+="<%=den(5)%>&nbsp;"
			str+= num2str(vEspacioLibre/1024,vdecimalfmt,vthousanfmt,0)
			str+="&nbsp;Kb.&nbsp;<%=den(6)%></nobr></td>"

			oTR.deleteCell(0)
			insertarCelda(oTR,"sombreado",str)

			//Sale del bucle
			i=lRows
			}
	}

}

function uploadRealizado(iSize,id)
{
var adjun
if (vTmpNombre=="")
	return 

insertarAdjunto(id,vTmpNombre,vTmpComentario,iSize)

vTmpNombre=""
vTmpComentario=""
}

function insertarAdjunto(id, nombre, comentario, iSize)
{
oTABLE = document.getElementById("tblAdjuntos")
lRows = oTABLE.rows.length

var str

if (document.getElementById("tblAdjuntos").rows[lRows-1])
	vFila = document.getElementById("tblAdjuntos").rows[lRows-1].cells[0].className
else
	vFila = "filaPar"

	
if (vFila == "cabecera")
	vFila = "filaPar"

if (vFila == "filaPar")
	vFila="filaImpar"
else
	vFila="filaPar"


oTR = oTABLE.insertRow(lRows)

str = "<div>"
str+= "<input type=hidden name='txtAdjId_" + id + "' id='txtAdjId_" + id + "' value=" + id + ">"
str+= "<input type=hidden name='txtAdjTamanyo_" + id + "' id='txtAdjTamanyo_" + id + "' value=" + iSize + ">"
str+= "<input type=hidden name='txtAdjNombre_" + id + "' id='txtAdjNombre_" + id + "' value=" + nombre + ">"
str+= "<label>" + nombre + "</label>"
str+= "</div>"

insertarCelda(oTR,vFila,str)

str = "<div>"
str+= num2str(iSize/1024,vdecimalfmt,vthousanfmt,0)
str+="&nbsp;Kb."
str+= "</div>"

insertarCelda(oTR,vFila + " numero",str)

str = "<div >"
str+= inputTexto("txtAdjComentario_" + id ,"98%",500,comentario,"modificarAdjunto(" + id + ")")
str+= "</div>"

insertarCelda(oTR,vFila,str)

str = "<div>"
str+= "<a href='javascript:downloadAdjunto(\"" + id + "\")'><img border = 0 src= '../solicitudesoferta/images/abrir.gif'></A>"
str+= "</div>"

insertarCelda(oTR,vFila,str,null,null,true)

str = "<div>"
str+= "<a href='javascript:eliminarAdjunto(" + id + ")'><img border = 0 src= '../solicitudesoferta/images/elimadjunto.gif'></A>"
str+= "</div>"


insertarCelda(oTR,vFila,str,null,null,true)
	
//Actualiza los valores de espacio consumido y espacio libre:
vEspacioConsumido+= iSize
vEspacioLibre=<%=oCompania.MaxSizeAdjun%>-vEspacioConsumido

oTABLE = document.getElementById("tblAdjun")
Rows = oTABLE.rows.length
oTR =oTABLE.rows[1]

str="<td class=sombreado><nobr><%=den(4)%>&nbsp;"
str+= num2str(vEspacioConsumido/1024,vdecimalfmt,vthousanfmt,0)
str+="&nbsp;Kb.&nbsp;"
str+="<%=den(5)%>&nbsp;"
str+= num2str(vEspacioLibre/1024,vdecimalfmt,vthousanfmt,0)
str+="&nbsp;Kb.&nbsp;<%=den(6)%></nobr></td>"

oTR.deleteCell(0)
insertarCelda(oTR,"sombreado",str)

}


</script>

<form name=frmAdjuntos>

<%
fila = "filaImpar"
%>

<table name="tblAdjun" id="tblAdjun" cellpadding=0 cellspacing=5 style="width:97%" align=center border="0">
	<tr>
		<td class=sombreado><nobr><%=den(3)%></nobr></td>
	</tr>
	<tr>
		<td class=sombreado><nobr><%=den(4)%>&nbsp;<%=visualizacionnumero(EspacioConsumido/1024,decimalfmt,thousandfmt,0)%>&nbsp;Kb.&nbsp;<%=den(5)%>&nbsp;<%=visualizacionnumero(EspacioLibre/1024,decimalfmt,thousandfmt,0)%>&nbsp;Kb. <%=den(6)%></nobr></td>
	</tr>
	
	<tr>
	<td>
		<table name="tblAdjuntos" id="tblAdjuntos" style="width:100%">
		<tr>
		<DIV><td class="cabecera" width="30%"><%=den(7)%></td></DIV>
		<DIV><td class="cabecera" align=right width="10%"><%=den(8)%></td></DIV>
		<DIV><td class="cabecera" width="40%"><%=den(9)%></td></DIV>
		<DIV><td class="cabecera" align=center width="7%"><%=den(10)%></td></DIV>
		<DIV><td class="cabecera" align=center width="7%"><%=den(11)%></td></DIV>
		</tr>
		
	<%for each oAdjun in oCompania.Especificaciones%>
		<tr>
		<td class="<%=fila%>">
			<DIV>
			<input type="hidden" name="txtAdjId_<%=oAdjun.id%>" id="txtAdjId_<%=oAdjun.id%>" value="<%=oAdjun.id%>">
			<input type="hidden" name="txtAdjTamanyo_<%=oAdjun.id%>" id="txtAdjTamanyo_<%=oAdjun.id%>" value="<%=oAdjun.dataSize%>">
			<input type="hidden" name="txtAdjNombre_<%=oAdjun.id%>" id="txtAdjNombre_<%=oAdjun.id%>" value="<%=oAdjun.nombre%>">
            <label><%=HTMLEncode(oAdjun.nombre)%></label>
			</DIV>
		</td>
		
		<td class="<%=fila%> numero">
			<DIV>
			<%=visualizacionnumero(oAdjun.dataSize/1024,decimalfmt,thousandfmt,0)%>&nbsp;Kb.
			</DIV>
		</td>
		
		<td class="<%=fila%>">
			<DIV>
			<input type="text" maxlength=500 style=width:98% name="txtAdjComentario_"<%=oAdjun.id%> id="txtAdjComentario_<%=oAdjun.id%>" value="<%=HTMLEncode(oAdjun.Comentario)%>" onchange="modificarAdjunto(<%=oAdjun.id%>)">
			</DIV>
		</td>
		
		<td class="<%=fila%>"  align="center" >
			<DIV>
			<a href="javascript:downloadAdjunto(<%=oAdjun.id%>)"><img border="0" src="../solicitudesoferta/images/abrir.gif" WIDTH="15" HEIGHT="13"></a>
			</DIV>
		</td>
		
		<td class="<%=fila%>"  align="center" >
			<div>
			<a href=javascript:eliminarAdjunto(<%=oAdjun.id%>)><img border="0" src="../solicitudesoferta/images/elimadjunto.gif" WIDTH="15" HEIGHT="13"></a>
			</div>
		</td>
		
		
		<%if fila ="filaImpar" then
			fila = "filaPar"
		else
			fila = "filaImpar"
		end if
		%>
		</tr>
		
	<%next%> 

	</table>
	</td>
	</tr>
	
	<tr>
		<td align=left >
			<INPUT type=button class=button id=cmdAceptar name=cmdAceptar value="<%=den(12)%>" onclick="anyadirAdjunto()">
		</td>
	</tr>
</table>

</form>

<%
set oCompania = nothing
set oRaiz = nothing

%>


</BODY>
</HTML>
