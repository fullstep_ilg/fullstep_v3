﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/formatos.asp"-->

<%
''' <summary>
''' Guardar una modificacion en la compania
''' </summary>
''' <remarks>Llamada desde: usuppal\modifcompaniaclient.asp ; Tiempo máximo: 0,2</remarks>

Idioma =trim(Request.Cookies("USU_IDIOMA"))
set oRaiz=validarUsuario(Idioma,true,true,0)

set paramgen = oRaiz.Sesion.Parametros
if not paramgen.BloqModifProve then
    set TESError = oRaiz.ValidarModificacionNIF(Request.Form("CodCia"), Request.Form("txtNIF"), Application("PYME"))
    if tesError.numError = 2 then 'Si existe el NIF de la compania, vuelve atras
	    codigo = "errorExisteNIFCompania"
	    Response.Redirect Application ("RUTASEGURA") & "script/usuppal/modifcompaniaclient.asp?codigo=" & codigo
	    Response.End
    else
	    codigo = "noError"
    end if
end if

'Llama a las validaciones de la mapper:
Dim  NomMapper
Dim oValidacionMapper
dim oMapper
dim sDireccion 
 
Set oValidacionMapper=oRaiz.generar_CValidacionesMapper
NomMapper=""
NomMapper= oValidacionMapper.ReadMapper()

iNumError=0
if  NomMapper <> "" then 
      On Error Resume Next
      set oMapper = nothing
      set oMapper = CreateObject (NomMapper & ".CValidacionesPortal")
      If Not oMapper is Nothing Then
         sDireccion=Request.Form("txtDir")
         if oMapper.TratarValidacionPortal(sdireccion,iNumError)=false then 
            codigo= oValidacionMapper.DevolverTexto(Idioma,iNumError)
            Response.Redirect Application ("RUTASEGURA") & "script/usuppal/modifcompaniaclient.asp?codigo=" & codigo
            Response.End
         else
	        codigo = "noError"
         end if 
       end if 
end if
	
dlstMon= clng(request("lstMon"))
dlstIdi= request("lstIdi")
dtxtDen = request("txtDen") 
dtxtVolFac = Numero(request("txtVolFac"))
dtxtCliRef1 = request("txtCliRef1")
dtxtCliRef2 = request("txtCliRef2")
dtxtCliRef3 = request("txtCliRef3")
dtxtCliRef4 = request("txtCliRef4")
dtxtHom1 = request("txtHom1")
dtxtHom2 = request("txtHom2")
dtxtHom3 = request("txtHom3")
dtxtURLCia = request("txtURLCia")
dtxtComent = request("txtComent")
dtxtPendConf = request("txtPendConf")
dCodCia=Request.Form("CodCia")
dNif=Request.Form("txtNIF")
dPyme=Application("PYME")

set oCia = oraiz.generar_ccompania()
ocia.cod=dCodCia
ocia.nif=dNif
ocia.Pyme=dPyme
ocia.id = oRaiz.Sesion.CiaId
oCia.codmon = dlstMon
ocia.idioma = dlstidi
ocia.den = dtxtden
ocia.volfac = dtxtVolFac
ocia.cliref1 = dtxtcliref1
ocia.cliref2 = dtxtcliref2
ocia.cliref3 = dtxtcliref3
ocia.cliref4 = dtxtcliref4
ocia.hom1 = dtxthom1
ocia.hom2 = dtxthom2
ocia.hom3 = dtxthom3
ocia.urlcia = dtxturlcia
ocia.coment = dtxtcoment
ocia.direccion =  request("txtDir") 
ocia.CP = request("txtCP") 
ocia.Poblacion = request("txtPob") 
ocia.idPais = clng(request("lstPai"))
ocia.NIF = request("txtNIF")
ocia.PendienteConfirm = cint(dtxtPendConf)


Provi = request("lstprovi")
	
if Provi= "-1" or Provi= "" then
	Provi = null
else
	Provi = clng(Provi)
end if

ocia.idprovincia=Provi

ocia.modificardatoscompania

set ocia=nothing



set oraiz = nothing

Response.Redirect Application ("RUTASEGURA") & "script/usuppal/okmodificardatos.asp"

%>

