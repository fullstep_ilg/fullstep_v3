﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<%
''' <summary>
''' Muestra en detalle la compania
''' </summary>
''' <remarks>Llamada desde: usuppal\modifregcompradoras.asp ; Tiempo máximo: 0,2</remarks>


Idioma = trim(Request.Cookies("USU_IDIOMA"))

set oRaiz=validarUsuario(Idioma,false,false,0)
	

CiaId = Request("CiaId")

set oCompania = oRaiz.devolverDatosCia (CiaId, Application("PORTAL"), false)



Dim Den

den=devolverTextos(Idioma,41)

set oPaises = oraiz.Generar_CPaises 
			
oPaises.CargarTodosLosPaises Idioma,,oCompania.codpais,,true,true

set oPais = oPaises.Item(ocompania.codpais)

if not isnull(oCompania.codprovi) or oCompania.codprovi<>"" then
	oPais.CargarTodasLasProvincias oCompania.codprovi,,true
end if


set oActsN1 = ocompania.DevolverEstructuraActividades (Idioma)
	

%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script src="../js/arbol.js"></script>

<script>

function obtener(codigo)
{
return true
}
//''' <summary>
//''' Cargar el arbol de actividades
//''' </summary>
//''' <remarks>Llamada desde: init() ; Tiempo máximo: 0,2</remarks>
function cargarTodo()
{
<%

	Dim oACN1 
	Dim oACN2 
	Dim oACN3 
	Dim oACN4 
	Dim oACN5 

	dim Vacio
	
	Vacio=true
	
	
	if not oActsN1 is nothing then 
	
		For Each oACN1 In oActsN1
			Vacio=false
			%>
			rama<%=oACN1.id%>=new Rama ('<%=oACN1.id%>','<%=JSText(oACN1.Cod)%>','<%=JSText(oACN1.Den)%>',false,<%if oACN1.ExistenActividadesHijas then%>false<%else%>true<%end if%>,null,'DarkGoldenrod',false,true,true)
			ramaRoot.add(rama<%=oACN1.Id%>)
			<%			
		    For Each oACN2 In oACN1.ActividadesNivel2

				%>
				rama<%=oACN1.Id%>_<%=oACN2.Id%>=new Rama ('<%=oACN1.Id%>_<%=oACN2.Id%>','<%=JSText(oACN2.Cod)%>','<%=JSText(oACN2.Den)%>',false,<%if oACN2.ExistenActividadesHijas then%>false<%else%>true<%end if%>,null,'green',false,true,true)
				rama<%=oACN1.Id%>.hojasCargadas=true
				rama<%=oACN1.Id%>.expanded = true
				rama<%=oACN1.Id%>.add(rama<%=oACN1.Id%>_<%=oACN2.Id%>)
				<%			
				For Each oACN3 In oACN2.ActividadesNivel3
					%>
					rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>=new Rama ('<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>','<%=JSText(oACN3.Cod)%>','<%=JSText(oACN3.Den)%>',false,<%if oACN3.ExistenActividadesHijas then%>false<%else%>true<%end if%>,null,'blue',false,true,true)
					rama<%=oACN1.Id%>_<%=oACN2.Id%>.hojasCargadas=true
					rama<%=oACN1.Id%>_<%=oACN2.Id%>.expanded=true
					rama<%=oACN1.Id%>_<%=oACN2.Id%>.add(rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>)
					<%			
					For Each oACN4 In oACN3.ActividadesNivel4
						%>
						rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>=new Rama ('<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>','<%=JSText(oACN4.Cod)%>','<%=JSText(oACN4.Den)%>',false,<%if oACN4.ExistenActividadesHijas then%>false<%else%>true<%end if%>,null,'brown',false,true,true)
						rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>.hojasCargadas=true
						rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>.expanded=false
						rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>.add(rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>)
						<%			
		                For Each oACN5 In oACN4.ActividadesNivel5
							%>
							rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>_<%=oACN5.Id%>=new Rama ('<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>_<%=oACN5.Id%>','<%=JSText(oACN5.Cod)%>','<%=JSText(oACN5.Den)%>',false,true,null,'gray',false,true,true)
							rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>.hojasCargadas=true
							rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>.expanded = true
							rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>.add(rama<%=oACN1.Id%>_<%=oACN2.Id%>_<%=oACN3.Id%>_<%=oACN4.Id%>_<%=oACN5.Id%>)
							<%			
		                Next
		                set oACN4.ActividadesNivel5 = nothing
		            Next
		            set oACN3.ActividadesNivel4 = nothing
				Next
				set oACN2.ActividadesNivel3 = nothing
		    Next
		    set oACN1.ActividadesNivel2 = nothing
		Next
		set oActsN1 = nothing
    end if   
%>

<%if vacio then%>
	return false
<%else%>
    return true
<%end if%>

}


function init()
{



ramaRoot=new Rama ('Root','Root','<b><%=den(14)%></b>',false,false,null,'DarkGoldenrod',false,true)
if (cargarTodo())
{
ramaRoot.write(null)
ramaRoot.expcont()
}
}

</script>

<style>
#divArbol {position:relative;visibility:visible;z-index:1;}
</style>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
</HEAD>
<BODY onload="init()">


<h4>
<%=den(1)%>
</h4>

<TABLE bgcolor="white" border=0 cellPadding=3 cellSpacing=0 width="100%">
	<TR>
		<TD class=negrita>
			<%=den(2)%>
		</TD>
		<TD >
			<%=oCompania.cod%>
		</TD>
	</TR>
	<TR>
		<TD class=negrita>
			<%=den(3)%>
		</TD>
		<TD>
			<%=HTMLEncode(ocompania.den)%>
		</TD>
	</TR>
	<TR>
		<TD class=negrita>
			<%=den(4)%>
		</TD>
		<TD>
			<%=HTMLEncode(ocompania.direccion)%>
		</TD>
	</TR>
	<TR>
		<TD class=negrita>
			<%=den(5)%>:
		</TD>
		<TD>
			<%=ocompania.cp%>
		</TD>
	</TR>
	<TR>
		<TD class=negrita>
			<%=den(6)%>
		</TD>
		<TD>
			<%=HTMLEncode(ocompania.poblacion)%>
		</TD>
	</TR>
	<TR>
		<TD class=negrita>
			<%=den(7)%>
		</TD>
		<TD>
				<%for each otPais in oPaises%>
					<%=otPais.den%>
				<%next%>
			
		</TD>
	</TR>
	<TR>
		<TD class=negrita>
			<%=den(8)%>
		</TD>
		<TD>
			<%
			if not isnull(oCompania.codprovi) or oCompania.codprovi<>"" then
			for each oProvi in oPais.Provincias%>
				<%=oProvi.den%>
			<%next
			
			set oPais.Provincias = nothing 
			end if
			set oPais = nothing
			set oPaises = nothing
			%>
		</TD>
	</TR>
	<TR>
		<TD class=negrita>
			<%=den(9)%>
		</TD>
		<TD>
			<%=VisualizacionNumero(ocompania.volfac,",",".",2)%>&nbsp;<%=den(10)%>
		</TD>
	</TR>
	<TR>
		<TD class=negrita>
			<%=den(11)%>
		</TD>
		<TD>
			<%=ocompania.cliref1%>
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
		<TD>
			<%=ocompania.cliref2%>
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
		<TD>
			<%=ocompania.cliref3%>
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
		<TD>
			<%=ocompania.cliref4%>
		</TD>
	</TR>
	<TR>
		<TD class=negrita>
			<%=den(12)%>
		</TD>
		<TD>
			<%=ocompania.Hom1%>
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
		<TD>
			<%=ocompania.Hom2%>
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
		<TD>
			<%=ocompania.Hom3%>
		</TD>
	</TR>
	<TR>
		<TD class=negrita>
			<%=den(13)%>
		</TD>
		<TD>
			<%=ocompania.urlcia%>
		</TD>
	</TR>
	<TR>
		<TD colspan=2>&nbsp;
		</TD>
	</TR>
	<TR>
		<TD colspan=2>
			<div name="divArbol" id="divArbol">
			</div>
		</TD>
	</TR>
</TABLE>

  
<br>

<table width=95%>
	<tr>
		<td align = center>
			<INPUT class="button" type=button name="cmdVolver" id="cmdVolver" value="<%=den(15)%>" onclick="window.close()">
		</td>
	</tr>
</table>

<br>

</form>

</BODY>
<%
set ocompania = nothing

set oraiz = nothing
%>
</HTML>
