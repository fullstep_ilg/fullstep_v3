﻿<%@ Language=VBScript %>

<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<%
''' <summary>
''' Mensaje q indica q el cambio se realizo correctamente
''' </summary>
''' <remarks>Llamada desde: usuppal\usuarioprincipal.asp    usuppal\guardarusuario.asp; Tiempo máximo: 0,2</remarks>



Idioma=trim(Request.Cookies("USU_IDIOMA"))

set oRaiz=validarUsuario(Idioma,false,true,0)

Accion = request("Accion")

Dim Den

den=devolverTextos(Idioma,64)
%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">

<script language="JavaScript" type="text/JavaScript">
    /*''' <summary>
    ''' Iniciar la pagina.
    ''' </summary>     
    ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
    
<%    
	select case accion
		case "Nuevo", "Eliminar", "Modificar"
%>
		window.open("<%=Application("RUTASEGURA")%>script/usuppal/admonusuarioscab.asp","fraAdmonUsuariosCabecera")
		
<%
		case "Principal"
%>
			document.getElementById('tablemenu').style.display = 'block';
<%	end select
%>
}

</script>
</HEAD>
<BODY topmargin=0 leftmargin=0 onload="init()">
<%
	select case accion
		case "Nuevo"

		set paramgen = oRaiz.Sesion.Parametros
		
		if paramGen.AutorizacionAutomatica then
			%>
			<h1><%=Den(3)%></h1>
			<h3><%=Den(15)%></h3>
			<%	
		else		
			%>
			<h1><%=Den(3)%></h1>
			<h3><%=Den(6)%></h3>
			<%	
		end if	
		set paramgen = nothing
		
		case "Eliminar"
		%>
		<h1><%=Den(1)%></h1>
		<h3><%=Den(4)%></h3>
		<%		
		case "Modificar"
		%>
		<h1><%=Den(2)%></h1>
		<h3><%=Den(5)%></h3>
		<%		
		case "Principal"
		%>
		<script SRC="../common/menu.asp"></script>
		<script>dibujaMenu(6)</script>
		
		<h1><%=Den(11)%></h1>
		<h3><%=Den(12)%></h3>
		<p>&nbsp;</p>
		<%		
	end select
%>

</BODY>
</HTML>

