﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->

<HTML>
<HEAD>
<% 
    Idioma = Request.Cookies("USU_IDIOMA")
    
%>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</HEAD>
<%
''' <summary>
''' Muestra los usuarios de la compania y deja añadir nuevos usuarios.
''' </summary>
''' <remarks>Llamada desde: common/menu.asp; Tiempo máximo: 0,2</remarks>
%>
<frameset frameborder="0" rows="190,*" >
	<frame name="fraAdmonUsuariosCabecera"  scrolling="no" src="admonusuarioscab.asp">
	<frame name="fraAdmonUsuariosDetalle" src="../blank.htm" scrolling="auto">
	<noframes>
		<body>

			<p>This page uses frames, but your browser doesn't support them.</p>

		</body>
	</noframes>
</frameset>

</HTML>
