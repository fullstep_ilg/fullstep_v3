﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<%
''' <summary>
''' Guardar un usuario nuevo o una modificacion en el usuario
''' </summary>
''' <remarks>Llamada desde: usuppal\admonusuariosdet.asp ; Tiempo máximo: 0,2</remarks>
Idioma=Request.Cookies("USU_IDIOMA") 
Idioma=trim(Idioma)
set oRaiz=validarUsuario(Idioma,true,true,2)

if request("cmdNuevo")<>"" then
	accion = "Nuevo"
end if
if request("cmdGuardar")<>"" then
	accion = "Modificar"
end if
if request("cmdEliminar")<>"" then
	accion = "Eliminar"
end if

select case accion
	case "Nuevo"
		ciaId = oRaiz.Sesion.CiaId
		portalId = application("PORTAL")
		usuCod = request("txtCOD")
		Ape = request("txtApe")
        Nif = request("txtNif")
		Nombre = request("txtNombre")		
		Dep = request("txtDep")
		Cargo = request("txtCargo")
		Tfno = request("txtTfno")
		Tfno2 = request("txtTfno2")
		Fax = request("txtFax")
		email = request("txtemail")
		Idioma = request("lstIdioma")
		TfnoMv = request("txtTfnoMv")
		TipoMail = request("optTipoEMail")

		set paramgen = oRaiz.Sesion.Parametros		
		if paramGen.AutorizacionAutomatica then
			vFPEST = 3	
		else
			vFPEST = 1	
		end if		
		set paramgen = nothing
		'Se crea un usuario en portal. Desde la dll se llama al servicio de cambio de contraseña y se le crea una contraseña nueva.        
		oRaiz.comenzarTransaccion	
		set oError = oRaiz.RegistrarUsuarioEnPortal (PortalId,CiaId,usuCod,Ape,Nif,Nombre,Dep,Cargo,Tfno,Tfno2,Fax,Email,Idioma,Request.ServerVariables("REMOTE_ADDR") & " : " & Request.ServerVariables("REMOTE_HOST"),false,vFPEST,TfnoMv,TipoMail)		
		CodPortal = cstr(Application ("NOMPORTAL"))
        if oError.numError = 0 then
			oRaiz.confirmarTransaccion
            'El mail se enviara con un hash, código para saber que debe ingresar contraseña nueva.
			if vFPEST = 3 then
				oRaiz.EnviarEmailAutorizacion CiaId,usuCod, Application("PYME"), CodPortal
			end if
		else
			IF oError.numError = 200 then
			    oRaiz.cancelarTransaccion
			end if
			Error=oError.numError
		end if
	case "Eliminar"
		set oCompania = oraiz.Generar_CCompania
		
		oCompania.id = oRaiz.Sesion.CiaId
		dim idUsu
		idUsu = clng(request("txtID"))
		
		IF idUsu = clng(oRaiz.Sesion.UsuId) then
			nuevoPpal = clng(request("lstUsuarios"))
			if nuevoPpal > 0 then
				ocompania.EstablecerUsuarioPrincipal nuevoPpal,oRaiz.Sesion.id
			else
				ocompania.EstablecerUsuarioPrincipal -1,oRaiz.Sesion.id
			end if
			set oError = oCompania.EliminarUsuarioCia(idUsu)
			set oCompania=nothing
			
			set oRaiz=nothing%>
			<html>
			<script type="text/javascript">
				window.open("<%=APPLICATION("RUTANORMAL")%>","_top")
			</script>
			</html>
		<%Response.End
		end if			

		set oError = oCompania.EliminarUsuarioCia(idUsu)
		
		set oCompania= nothing
	case "Modificar"
		set oCompania = oraiz.Generar_CCompania
		oCompania.id = oRaiz.Sesion.CiaId
		
		idUsu = clng(request("txtID"))
		ciaId = oRaiz.Sesion.CiaId
		portalId = application("PORTAL")
		usuCod = request("txtCOD")
		Ape = request("txtApe")
        Nif = request("txtNif")
        Nombre = request("txtNombre")
		Dep = request("txtDep")
		Cargo = request("txtCargo")
		Tfno = request("txtTfno")
		Tfno2 = request("txtTfno2")
		Fax = request("txtFax")
		email = request("txtemail")
		Idioma = request("lstIdioma")
		TfnoMv = request("txtTfnoMv")
		TipoMail = request("optTipoEMail")
		set oError = oRaiz.ModificarDatosUsuario (Request.Cookies("USU_SESIONID"),IdUsu,CiaId,PortalId,usuCod,Ape,Nif,Nombre,Dep,Cargo,Tfno,Tfno2,Fax,Email,Idioma,TfnoMv,TipoMail)	

		nuevoPpal = clng(request("lstUsuarios"))
		if nuevoPpal > 0 then
			oCompania.EstablecerUsuarioPrincipal nuevoPpal,oRaiz.Sesion.id
		end if
		
		if idUsu<>nuevoPpal and nuevoPpal > 0 THEN
			oRaiz.Sesion.UsuPpal=0
		else 
			if nuevoPpal > 0 THEN
				oRaiz.Sesion.UsuPpal=1
			end if
		end if
		set oCompania= nothing	
end select

if oerror.numerror = 0 then
	set oerror = nothing
	if not oRaiz.Sesion.UsuPpal and Accion = "Modificar" then
        set oRaiz = nothing%>
		<script type="text/javascript">
		    window.open("<%=Application("RUTASEGURA")%>script/usuppal/okguardarUsuario.asp?accion=Principal","fraAdmonUsuariosCabecera")
		</script>
	<%else
        set oRaiz = nothing
		Response.Redirect Application ("RUTASEGURA") & "script/usuppal/okguardarUsuario.asp?accion=" & Accion
	end if
else
	set oerror = nothing
    set oRaiz = nothing
	Response.Redirect Application ("RUTASEGURA") & "script/usuppal/errorguardarUsuario.asp?accion=" & Accion & "&Error=" & Error
end if%>