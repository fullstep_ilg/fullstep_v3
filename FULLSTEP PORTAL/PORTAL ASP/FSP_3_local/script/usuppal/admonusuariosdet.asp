﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/idioma.asp"-->
<%
''' <summary>
''' Mostrar la pantalla para administrar los usuarios de la empresa. Tambien puedes dar de alta usuarios.
''' </summary>
''' <remarks>Llamada desde: admonusuarioscab.asp; Tiempo máximo: 0,1</remarks>
Dim Den
Idioma=trim(Request.Cookies("USU_IDIOMA"))

den=devolverTextos(Idioma,62)

set oRaiz=validarUsuario(Idioma,true,true,1)

dim IsSecure
IsSecure = left(Application ("RUTASEGURA"),8)
if IsSecure = "https://" then                       
    IsSecure = "; secure"
else
    IsSecure =""
end if

Response.AddHeader "Set-Cookie","USU_CSRFTOKEN=" & server.URLEncode(oRaiz.Sesion.CSRFToken) & "; path=/; HttpOnly" + IsSecure
CiaId = oRaiz.Sesion.CiaId
set oCompania = oRaiz.devolverDatosCia (CiaId, Application("PORTAL"), false)
set oPaises = oraiz.Generar_CPaises 			
oPaises.CargarTodosLosPaises Idioma,,,,,true
set oPais = oPaises.Item(ocompania.codpais)
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp"/>
<meta name="GENERATOR" content="Microsoft Visual Studio 6.0"/>
<script type="text/javascript" src="../common/formatos.js"></script>
<script type="text/javascript">
var validNif = <%=oPais.validNif %>;
function init()
{
    document.getElementById("divEspera").style.visibility = "hidden";
};
</script>
</head>
<body topmargin="0" leftmargin="0" onload="init()">
<div name="divEspera" id="divEspera" style="position:absolute;top:15;left:0;visibility:visible;width:90%;height:90%;">
<table width="100%" height="100%">
	<tr>
		<td align="center" valign="center">
			<table>
				<tr>
					<td>
						<img SRC="<%=application("RUTASEGURA")%>script/images/cursor-espera_grande.gif">
					</td>
					<td>
						<span class="fmgrande"><%=den(35)%></span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>
<%
idUsuario = request("lstUsuarios")
idCia = oRaiz.Sesion.CiaId
idPortal = Application("PORTAL")
dim nuevo
nuevo = false
if Request("cmdNuevo")<>"" then
	nuevo = true
end if

if nuevo then
	set oUsuario = oRaiz.generar_CUsuario
else
	set oUsuario = oRaiz.devolverdatosUsuario(idUsuario,idCia,idPortal)
    oUsuario.PWDEncriptada = "(-%@#$-)"
end if
set oCia = oRaiz.devolverdatosCia(idCia,idPortal)
set paramgen = oRaiz.Sesion.Parametros

%>
<script type="text/javascript">
function confirmar()
{
<%
if oUsuario.Principal then	
	set oCompania = oRaiz.generar_CCompania()
	oCompania.Id = clng(oRaiz.Sesion.CiaId)
	oCompania.IdPortal = Application("PORTAL")
	set oError = oCompania.cargarusuarioscia(true)
	%>
	if (confirm("<%=den(24)%>")==true)
		{
		<%if oCompania.usuarios.count=1 then%>
			if (confirm("<%=den(27)%>")==true)
				{
				//iiiiiiiiiiiiiiiiiiiiii
				return true
				}
		<%else%>
			f=document.forms["frmUsuario"]
			if (f.lstUsuarios.options[f.lstUsuarios.selectedIndex].value=="-1")
				{
				alert("<%=den(28)%>")
				return false
				}
			else
				{
				return true
				}
		<%end if%>
		}
<%else%>
	if (confirm("<%=den(24)%>")==true)
		{
		return true
		}
<%end if%>

return false
}

function ValidarNIFCIF(){
    p= window.parent
    f = document.forms["frmUsuario"]
    switch(validNif)
	    {
	    case 0:
		    return true
		    break;
	    case 1:
		    if(f.txtNif.value=="") 
			    return true
		    if (VALIDAR_CIF(f.txtNif.value))
			    return true
			
		    if (VALIDAR_NIF(f.txtNif.value))
			    return true

		    alert("<%=JSAlertText(den(42))%>")
		    return false
		    break;
	    default:
		    return true
	    }
}

function validar()
{       
	f=document.forms["frmUsuario"]
	
	f.txtCod.value = trim(f.txtCod.value)

	if (trim(f.txtCod.value) == "" || trim(f.txtApe.value) == "" || trim(f.txtNombre.value) == "" || trim(f.txtDep.value)=="" 
        || trim(f.txtCargo.value) =="" || trim(f.txtTfno.value) == "" || trim(f.txtemail.value) == "")
		{
		alert ('<%=JSText(den(20))%>')
		return false
		}
	if (f.txtCod.value.length<6)
		{
		alert('<%=JSText(den(22))%>')
		return false
		}	
	if (!validarEmail(f.txtemail.value))
		{
			alert ('<%=JSText(den(37))%>')
			return false
		}

    if (!ValidarNIFCIF()){
        f.txtNif.focus();
        return false;
    }

    <%if paramgen.CompruebaTfno then%>
	    if (!ValidarTelefono(trim(f.txtTfno.value),'<%=JSText(oCia.DenPais)%>'))
		    {
			    alert ('<%=JSText(den(38))%>')
			    return false
		    }
    <%end if%>
    <%if paramgen.CompruebaTfno then%>
	    if (trim(f.txtTfno2.value) != "")
	    {
		    if (!ValidarTelefono(trim(f.txtTfno2.value),'<%=JSText(oCia.DenPais)%>')){
			    alert ('<%=JSText(den(38))%>')
			    return false
		    }
	    }
    <%end if%>
    <%if paramgen.CompruebaTfno then%>
	    if (trim(f.txtTfnoMv.value) != "")
	    {
		    if (!ValidarTelefono(trim(f.txtTfnoMv.value),'<%=JSText(oCia.DenPais)%>')){
			    alert('<%=JSText(den(39))%>')
			    return false
		    }
	    }
    <%end if%>	
    <%if paramgen.CompruebaTfno then%>
	    if (trim(f.txtFax.value) != "")
	    {
		    if (!ValidarTelefono(trim(f.txtFax.value),'<%=JSText(oCia.DenPais)%>')){
			    alert('<%=JSText(den(40))%>')
			    return false
		    }
	    }
    <%end if%>
		
<%if nuevo then
	set paramgen = oRaiz.Sesion.Parametros

	if paramGen.AutorizacionAutomatica then%>
	if (confirm('<%=JSText(den(36))%>')==false)
		{
		return false
		}
	<%else%>
	if (confirm('<%=JSText(den(29))%>')==false)
		{
		return false
		}
	<%end if
	set paramgen = nothing
else%>
	if (confirm('<%=JSText(den(31))%>')==false)
		{
		return false
		}
<%end if%>
    

	return true;
}
</script>
<h1>
<%if nuevo then%>
<%=den(1)%>
<%else%>
<%=den(17)%>
<%end if%>
</h1>
<h3>
<%if nuevo then%>
<%=den(2)%>
<%else%>
<%=den(18)%>
<%end if%>
</h3>
<h4><%=den(3)%></h4>
<form name="frmUsuario" method="post" action="guardarUsuario.asp">
<input type="hidden" name="Idioma" value="<%=Idioma%>"/>
<input type="hidden" id="txtID" name="txtId" value="<%=oUsuario.ID%>"/>
<input type="hidden" id="txtFECPWD" name="txtFECPWD" value="<%=oUsuario.fecpwd%>"/>
<table align="center" class="principal" border="0" cellpadding="1" cellspacing="1" width="70%">
	<tr>
		<td class="filaImpar negrita" width="150">
			<%=den(4)%>
		</td>
		<td class="filaImpar" width="500">
			<input type="text" id="txtCod" name="txtCod" size="20" maxLength="20" value="<%=oUsuario.Cod%>" autocomplete="off"/>
		</td>
	</tr>
	<tr>
		<td class="filaPar negrita" width="150">
			<%=den(6)%>
		</td>
		<td class="filaPar" width="500">			
			<input type="text" id="txtNombre" name="txtNombre" size="20" maxLength="20" value="<%=HTMLEncode(oUsuario.Nombre)%>"/>
		</td>
	</tr>
	<tr>
		<td class="filaImpar negrita" width="150">
			<%=den(5)%>
		</td>
		<td class="filaImpar" width="500">
			<input type="text" id="txtApe" name="txtApe" size="41" maxLength="100" value="<%=HTMLEncode(oUsuario.Apellidos)%>"/>
		</td>
	</tr>
    <tr>
		<td class="filaPar negrita" width="150">
			<%=den(41)%>
		</td>
		<td class="filaPar" width="500">
			<input type="text" id="txtNif" name="txtNif" size="41" maxLength="100" value="<%=oUsuario.Nif%>" onchange="ValidarNIFCIF();"/>
		</td>
	</tr>
	<tr>
		<td class="filaImpar negrita" width="150">
			<%=den(9)%>
		</td>
		<td class="filaImpar" width="500">
			<input type="text" id="txtDep" name="txtDep" size="41" maxLength="100" value="<%=HTMLEncode(oUsuario.codDep)%>"/>
		</td>
	</tr>
	<tr>
		<td class="filaPar negrita" width="150">
			<%=den(10)%>
		</td>
		<td class="filaPar" width="500">
			<input type="text" id="txtCargo" name="txtCargo" size="41" maxLength="100" value="<%=HTMLEncode(oUsuario.Cargo)%>"/>
		</td>
	</tr>
	<tr>
		<td class="filaImpar negrita" width="150">
			<%=den(11)%>
		</td>
		<td class="filaImpar" width="500">
			<input type="text" id="txtTfno" name="txtTfno" size="19" maxLength="20" value="<%=HTMLEncode(oUsuario.Tfno)%>"/>
		</td>
	</tr>
		<tr>
		<td class="filaPar negrita" width="150">
			&nbsp;
		</td>
		<td class="filaPar" width="500">
			<input type="text" id="txtTfno2" name="txtTfno2" size="19" maxLength="20" value="<%=HTMLEncode(oUsuario.Tfno2)%>"/>
		</td>
	</tr>
	<tr>
		<td class="filaImpar negrita" width="150">
			<%=den(30)%>
		</td>
		<td class="filaImpar" width="500">
			<input type="text" id="txtTfnoMv" name="txtTfnoMv" size="19" maxLength="100" value="<%=HTMLEncode(oUsuario.TfnoMovil)%>"/>
		</td>
	</tr>
	<tr>
		<td class="filaPar negrita" width="150">
			<%=den(12)%>
		</td>
		<td class="filaPar" width="500">
			<input type="text" id="txtFax" name="txtFax" size="19" maxLength="100" value="<%=HTMLEncode(oUsuario.fax)%>"/>
		</td>
	</tr>
	<tr>
		<td class="filaImpar negrita" width="150">
			<%=den(13)%>
		</td>
		<td class="filaImpar" width="500">
			<input type="text" id="txtemail" name="txtemail" size="40" maxLength="100" value="<%=HTMLEncode(oUsuario.Mail)%>"/>
		</td>
	</tr>
	<tr>
		<td class="filaPar negrita" width="150">
			<%=den(14)%>
		</td>
		<td class="filaPar" width="500">
			<select name="lstIdioma" id="lstIdioma" size="1">
			<%set adorIdi = oRaiz.DevolverIdiomas
			while not adorIdi.eof%>
				<option value="<%=adorIdi.fields("COD").value%>" <%if ousuario.Idioma=adorIdi.fields("COD").value then%> selected<%end if%>><%=adorIdi.fields("DEN").value%></option>
				<%adorIdi.movenext
			wend
			adorIdi.close
			set adorIdi = nothing%>
			</select>
		</td>
	</tr>
	<tr>
		<td class="filaImpar negrita" width="200">
			<%=Den(32)%>
		</td>		
		<td class="filaImpar" width="500">
			<input type="radio" class="radio" id="optTipoEMail" name="optTipoEMail" value="1" <%if oUsuario.TipoMail=1 or nuevo then%> checked <%end if%>/><%=Den(34)%>
            <input type="radio" class="radio" id="optTipoEMail" name="optTipoEMail" value="0" <%if oUsuario.TipoMail=0 and not nuevo then%> checked <%end if%>/><%=Den(33)%>
		</td>		
	</tr>
<%if oUsuario.Principal then

if ocompania.usuarios.count>1 then
%>
	<tr>
		<td class="filaPar negrita" width="150">
			<%=den(25)%>
		</td>
		<td class="filaPar" width="500">
			<select size="1" name="lstUsuarios" width="100%">
				<option value="-1" SELECTED></option>
				<%for each oNuevoPpal in oCompania.usuarios%>
				<%if oNuevoPpal.id<>oUsuario.id then%>
				<option value="<%=oNuevoPpal.Id%>"><%=oNuevoPpal.Nombre & " " & oNuevoPpal.apellidos %></option>
				<%end if%>
				<%next%>
			</select>
		</td>
	</tr>

<%end if
end if
%>
	<tr>
		<td align="left" colspan="2">
			<br>
			<br>
				<table border="0" width="81%">
					<tr>
					<%if nuevo then%>
						<td width="50%" align="center">
							<input class="button" id="cmdNuevo" name="cmdNuevo" type="submit" onclick="return validar()" value="<%=den(19)%>"/>
						</td>
					<%else%>
						<td width="50%" align="center">
							<input class="button" id="cmdGuardar" name="cmdGuardar" type="submit" onclick="return validar()" value="<%=den(15)%>"/>
						</td>
						<td width="50%" align="center">
							<input class="button" id="cmdEliminar" name="cmdEliminar" type="submit" value="<%=den(16)%>" onclick="return confirmar()"/>
						</td>
					<%end if%>
					</tr>
				</table>
		</td>
	</tr>
</table>
<%
set ousuario=nothing
set oCia=nothing

set oraiz = nothing


%>
</form>
</body>
</html>
