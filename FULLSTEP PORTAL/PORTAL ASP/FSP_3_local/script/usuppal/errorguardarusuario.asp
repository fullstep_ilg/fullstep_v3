﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<%
''' <summary>
''' Mensaje q indica error en los cambios de datos de usuario
''' </summary>
''' <remarks>Llamada desde: usuppal\guardarusuario.asp ; Tiempo máximo: 0,2</remarks>

Idioma=trim(Request.Cookies("USU_IDIOMA"))

set oRaiz=validarUsuario(Idioma,false,false,0)

Accion = request("Accion")

Dim Den

den=devolverTextos(Idioma,64)
%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script>
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
	window.open("<%=Application("RUTASEGURA")%>script/usuppal/admonusuarioscab.asp","fraAdmonUsuariosCabecera")
}

</script>
</HEAD>

<BODY topmargin=0 leftmargin=0 onload=init()>

<p class=error><%=Den(7)%></p>
<%
	select case accion
		case "Nuevo"
			%>
			<h3><%=Den(8)%></h3>			
			<%if request("Error")=301 then%>
			<h3><%=Den (14)%></h3>
			<%end if%>
			<%		
		case "Eliminar"
			if request("Error")="ppal" then
			%>
			<h3><%=Den(13)%></h3>
			<%		
			else
			%>
			<h3><%=Den(10)%></h3>
			<%		
			end if
		case "Modificar"
			%>
			<h3><%=Den(9)%></h3>
			<%		
	end select
%>
</BODY>
</HTML>
