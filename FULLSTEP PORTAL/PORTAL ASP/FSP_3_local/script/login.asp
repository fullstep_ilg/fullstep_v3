﻿<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.Expires = -1 %> 
<% Response.Charset = "UTF-8"%>
<!--#include file="../common/XSS.asp"-->
<!--#include file="../common/idioma.asp"-->
<%		

''' <summary>
''' Cargar en cookies todo lo q necesita menu.asp y otras varias pantallas. Son datos sobre el usuario, si tiene 
'''	acceso a, por ejemplo, certificados, etc.
''' Al final llama a la pantalla de inicio si el usuario es valido.
'''	Las personalizaciones llaman a esta, por lo q se controla el XSS sin tocar las personalizaciones.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: default.asp (de todas las instalaciones) ; Tiempo máximo:0 </remarks>
Idioma = Request("Idioma")
Idioma = trim(Idioma)
if Idioma="" then
	Idioma = "SPA"
end if


%>

<script type="text/javascript" src="<%=application("RUTANORMAL")%>script/js/jquery.min.js"></script>

<script type="text/javascript" src="<%=application("RUTANORMAL")%>script/politicacookies/js/politicacookies.js"></script>

<%
IdOrden=Request("IdOrden") 
CiaComp=Request("CiaComp") 

IPDir=Request.ServerVariables("LOCAL_ADDR")

dim IsSecure
IsSecure = left(Application ("RUTASEGURA"),8)
if IsSecure = "https://" then                       
    IsSecure = "; secure"
else
    IsSecure =""
end if

if Request.ServerVariables("CONTENT_LENGTH")>0 then
    UsuSesionId=Request.Cookies("USU_SESIONID")
    PersistID=Request.Cookies("SESPP")

	set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
	i = oraiz.Conectar(Application("INSTANCIA"))
	if i = 1 then
		Response.Redirect application("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	else
		If IdOrden="" then
            set Sesion = oraiz.ValidarLogin (request("txtCIA"),request("txtUSU"),request("txtPWD"),cint(Application("PORTAL")),Application("PYME"),false, _
                                            Application("APROVISIONAMIENTO"),Application("SOLICITUDESPM"),Application("CERTIFICADOS"),Application("NOCONFORMIDADES"), _
                                            Application("CN"),IPDir,PersistID, UsuSesionId,Application("NOMPORTAL"))		
		else
            set Sesion = oraiz.ValidarLogin (request("txtCIA"),request("txtUSU"),request("txtPWD"),cint(Application("PORTAL")),Application("PYME"),false, _
                                            Application("APROVISIONAMIENTO"),Application("SOLICITUDESPM"),Application("CERTIFICADOS"),Application("NOCONFORMIDADES"), _
                                            Application("CN"),IPDir,PersistID,UsuSesionId,Application("NOMPORTAL"),CiaComp)		
		end if
        
		if Sesion.Id ="" then 
            if oraiz.UsuarioYaLogeado = 0 then
			    if IdOrden="" Then%>
				    <script>
                        //Usuario-contraseña incorrecta
				        window.open("<%=application("RUTASEGURA")%>script/usuarionovalido.asp?Idioma=<%=Idioma%>&ErrorSesion=0", "_blank", "top=100,left=150,width=450,height=240,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no")
				    </script>
			    <%
			    else
				    set oCompania = oRaiz.devolverDatosCia (CiaComp, Application("PORTAL"), false)
			    %>
				    <script>
					    window.open ("<%=application("RUTASEGURA")%>script/usuarionovalido.asp?Idioma=<%=Idioma%>&NomCiaComp=<%=oCompania.Den%>&ErrorSesion=0","_blank","top=100,left=150,width=400,height=1000,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no")
				    </script>
				    <%set oCompania=nothing%>
			    <%end if
            else 
                IF IdOrden="" Then%>
				    <script>
				        //Se ha borrado la cookie habiendo una sesión activa
					    window.open ("<%=application("RUTASEGURA")%>script/usuarionovalido.asp?Idioma=<%=Idioma%>&ErrorSesion=1","_blank","top=100,left=150,width=600,height=900,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no")
				    </script>                
			    <%
			    else
				    set oCompania = oRaiz.devolverDatosCia (CiaComp, Application("PORTAL"), false)
			    %>
				    <script>
				        window.open("<%=application("RUTASEGURA")%>script/usuarionovalido.asp?Idioma=<%=Idioma%>&NomCiaComp=<%=oCompania.Den%>&ErrorSesion=1", "_blank", "top=100,left=150,width=400,height=240,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no")
				    </script>
				    <%set oCompania=nothing%>
                <%end if%>			
            <%end if%>
		<%else

            'En la cookie USU se escribirá el ID de sesión generado y los datos actuales MOSTRARFMT, THOUSANFMT, DECIMALFMT, PRECISIONFMT, DATEFMT, IDIOMA, TIPOMAIL
            Response.AddHeader "Set-Cookie","USU_SESIONID=" &  server.URLEncode(Sesion.Id) & "; path=/; HttpOnly" + IsSecure

            Response.AddHeader "Set-Cookie","USU_MOSTRARFMT=" &  Sesion.MostrarFMT  & "; path=/; HttpOnly" + IsSecure

            Response.AddHeader "Set-Cookie","USU_DECIMALFMT=" & Sesion.DecimalFMT  & "; path=/; HttpOnly" + IsSecure
            Response.AddHeader "Set-Cookie","USU_THOUSANFMT=" & Sesion.ThousanFMT  & "; path=/; HttpOnly" + IsSecure
            Response.AddHeader "Set-Cookie","USU_DATEFMT=" & Sesion.DateFMT  & "; path=/; HttpOnly" + IsSecure
            Response.AddHeader "Set-Cookie","USU_PRECISIONFMT=" & Sesion.PrecisionFMT  & "; path=/; HttpOnly" + IsSecure

            Response.AddHeader "Set-Cookie","USU_TIPOMAIL=" & Sesion.TipoMail  & "; path=/; HttpOnly" + IsSecure
            Response.AddHeader "Set-Cookie","USU_IDIOMA=" &   Sesion.Idioma  & "; path=/; HttpOnly" + IsSecure

            %>
            <script type="text/javascript" >
            /****
            /*Rellena el título según idioma seleccionado por el usuario:
            /*En la página de login si no está logeado
            /*En el idioma seleccionado por el usuario en la aplicación si está logeado
            **/
            $('document').ready(function () {
                <% 
                    titulo="TITULOVENTANAS_" & Sesion.Idioma
                %>
                document.title = '<%=application(titulo) %>';
            });
            </script>
            <%
            Response.AddHeader "Set-Cookie","USU_CSRFTOKEN=""; path=/; HttpOnly" + IsSecure

            if Sesion.Parametros.unaCompradora then
				application("UNACOMPRADORA") = 1
				application("IDCIACOMPRADORA") = Sesion.Parametros.IdCiaCompradora
			else
				application("UNACOMPRADORA") = 0
            end if

            'El Id generado para la cookie persistente se almacenará en una cookie de nombre SESPP (sesión portal persistente) a la que pondremos una caducidad de 24 horas.
            Fecha=Date+1
            sDia=cstr(day(Fecha))
            sUrte = cstr(year(Fecha))

            sDia = left("00",2-len(sDia)) & sDia

            select case Weekday(Fecha)
            case 1:
                sDiaWeek = "Sun, "
            case 2:
                sDiaWeek = "Mon, "
            case 3: 
                sDiaWeek = "Tue, "
            case 4: 
                sDiaWeek = "Wed, "
            case 5: 
                sDiaWeek = "Thu, "
            case 6:
                sDiaWeek = "Fri, "
            case 7:
                sDiaWeek = "Sat, "
            end select 
                    
            select case month(Fecha)
            case 1:
                sMes= "Jan"
            case 2:
                sMes= "Feb"
            case 3:
                sMes= "Mar"
            case 4:
                sMes= "Apr"
            case 5:
                sMes= "May"
            case 6:
                sMes= "Jun"
            case 7:
                sMes= "Jul"
            case 8:
                sMes= "Aug"
            case 9:
                sMes= "Sep"
            case 10:
                sMes= "Oct"
            case 11:
                sMes= "Nov"
            case 12:
                sMes= "Dec"
            end select
                    
            VisualizacionFecha = sDiaWeek + sDia + "-" +  sMes + "-" + sUrte + " 00:00:00 GMT"    

            Response.AddHeader "Set-Cookie","SESPP=" & server.URLEncode(Sesion.PersistID)  & "; path=/; HttpOnly; Expires=" + VisualizacionFecha + IsSecure

                       
			path = application("RUTASEGURA")
			servidor = Request.servervariables("SERVER_NAME")
			path = replace(path,"http://" & servidor,"")
			path = replace(path,"https://" & servidor,"")
			'Ponemos esto porque lo otro no funciona debido al redireccionamiento al puerto 8080 de Ormazabal
			'http://purchasing.ormazabal.com/rfi/ a http://rfi.ormazabal.com:8080/
			'lo quito otra vez pq lo han cambiado al 80
			'en gestamp si que sale por otro puerto
			if Application("PUERTO80")=0 then
				server.transfer "/script/login/inicio.asp"
			else
				server.transfer path & "script/login/inicio.asp"
			end if

		end if
	end if
end if
if application("NOMPORTAL")<> "huf" then
%>

<script>

function Alta()
{
window.open("../../registro/registro.asp?Idioma=<%=Idioma%>","_blank","top=15,left=20,width=700,height=500,location=no,resizable=yes,scrollbars=yes,toolbar=yes")

}
</script>


<%end if%>





