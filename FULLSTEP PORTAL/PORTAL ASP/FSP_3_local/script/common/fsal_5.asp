﻿<%
if Application("FSAL")=1 then
%>
<script type='text/javascript'>
    function ObtenerPaginaOrigenJS(pagina,tipoFecha) {
        var sPaginaOrigen;

        if (tipoFecha == 1 || tipoFecha == 5) {
            if (pagina.toLowerCase().search(String('inicio.asp').toLowerCase()) > 0) {
                sPaginaOrigen=pagina.replace('inicio.asp','inicio.asp');
            } else if (pagina.toLowerCase().search(String('actividadesCia.asp').toLowerCase()) > 0) {
                sPaginaOrigen = pagina.replace('actividadesCia.asp', 'compania.asp');
            } else if (pagina.toLowerCase().search(String('actividades.asp').toLowerCase()) > 0) {
                sPaginaOrigen = pagina.replace('actividades.asp', 'compania.asp');
            } else if (pagina.toLowerCase().search(String('actividadesciaserver.asp').toLowerCase()) > 0) {
                sPaginaOrigen = pagina.replace('actividadesciaserver.asp', 'compania.asp');
            } else if (pagina.toLowerCase().search(String('usuario.asp').toLowerCase()) > 0) {
                sPaginaOrigen=pagina.replace('usuario.asp','actividadesCia.asp');
            } else if (pagina.toLowerCase().search(String('resumen.asp').toLowerCase()) > 0) {
                sPaginaOrigen = pagina.replace('resumen.asp', 'usuario.asp');
            } else if (pagina.toLowerCase().search(String('registrarproveedor.asp').toLowerCase()) > 0) {
                sPaginaOrigen = pagina.replace('registrarproveedor.asp', 'resumen.asp');
            } else if (pagina.toLowerCase().search(String('imprimirOferta.asp').toLowerCase()) > 0) {
                sPaginaOrigen=pagina.replace('imprimirOferta.asp','cumpoferta.asp');
            } else if (pagina.toLowerCase().search(String('downloadofertaexcel.asp').toLowerCase()) > 0) {
                sPaginaOrigen=pagina.replace('downloadofertaexcel.asp','cumpoferta.asp');
            } else if (pagina.toLowerCase().search(String('guardaroferta.asp').toLowerCase()) > 0) {
                sPaginaOrigen=pagina.replace('guardaroferta.asp','cumpoferta.asp');
            } else if (pagina.toLowerCase().search(String('excelserver.asp').toLowerCase()) > 0) {
                sPaginaOrigen=pagina.replace('excelserver.asp','excel.asp');
            } else {
                sPaginaOrigen='';
            }
        }

        return sPaginaOrigen;
    }

    function Ajax_FSALRegistrar5(fechaBegin,pagina,idRegistro) {
        var fechaEnd = devolverUTCFechaFSAL();
        Ajax('<%=Application("URL_FSAL_RegistrarAccesos_Client") & "/FSALRegistrarAccesos" %>?Tipo=5&Producto=<%=Application("FSAL_Origen") %>&fechapet=' + fechaBegin.toString() + '&fecha=' + fechaEnd.toString() + '&pagina=' + pagina + '&iPost=0&iP=<%=Request.ServerVariables("REMOTE_ADDR") %>&sUsuCod=<%=sUsuCod %>&sPaginaOrigen=' + ObtenerPaginaOrigenJS(pagina, 5) + '&sNavegador=<%=sNavegador %>&IdRegistro=' + idRegistro + '&sProveCod=<%=sProveCod %>&sQueryString=<%=sQueryString %>' + '&sNormalizarFechas=1', FunNull);
    }
</script>
<%
end if
%>
