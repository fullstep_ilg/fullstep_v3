<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<%
''' <summary>
''' Dependiendo de si el proveedor conectado es de una compania, digamos, unica (paramGen.unaCompradora) carga una pantalla
''	detallada de no conformidades. En caso de ser una compania, digamos, no unica (1 empresa fisica N empresas logicas)
''	muestra la lista de empresas con hipervinculos a pantalla detallada de las no conformidades de cada una.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: menu.asp ; Tiempo m�ximo:0</remarks>

	Idioma = Request("Idioma")
	Idioma = trim(Idioma)
    
    AccesoExterno=cbool(Application("ACCESO_SERVIDOR_EXTERNO"))       
    if AccesoExterno then    
       
        Cia=iif(request("txtCIA")="",request.cookies("USU_CIACOD"),request("txtCIA"))
	    Usu=iif(request("txtUSU")="",request.cookies("USU_USUCOD"),request("txtUSU"))
	    CodigoSesion=iif(request("CodigoSesion")="",request.cookies("USU_SESIONID_EXTERNO"),request("CodigoSesion"))     	
        set oRaiz=validarUsuarioAccesoExterno(Idioma,true,true,0,Cia,Usu,CodigoSesion)
        if Request("Pagina")=5 Or Request("Pagina")=15 then
            set oRsSubMenusExternos = oRaiz.DevolverSubMenusUrlsExternas(CStr(Idioma))
            Do While NOT oRsSubMenusExternos.EOF
                if Request("Pagina")=15 then%>
                    <script>TipoPopup=1;</script> 
                    <%if cint(oRsSubMenusExternos("ID"))=cInt(Request("ID"))  then%>

                        <script>url='<%=oRsSubMenusExternos("URL")%>';</script>
                        <script>pagCustom='<%=oRsSubMenusExternos("PAGINA_CUSTOM")%>';</script>  
                        <%if oRsSubMenusExternos("TIPO_LLAMADA")=1  then%>
                            <script>TipoOpen=1;</script> 
                        <%else%>
                            <script>TipoOpen=0;</script> 
                        <%end if %>


                        <%exit do
                    end if
                else
                    if oRsSubMenusExternos("TIPO_LLAMADA")=3 then%>
                        <script>url='<%=oRsSubMenusExternos("URL")%>';</script>
                        <script>paginaCustom='<%=oRsSubMenusExternos("PAGINA_CUSTOM")%>';</script>  
              
                    <%exit do
                    end if

                end if
                
                oRsSubMenusExternos.MoveNext

            Loop
        end if
    else
	    set oRaiz=validarUsuario(Idioma,true,true,0)
    end if            		
%>

<script>
    function init() { 
 
        $.when($.ajax({
            type: "POST",
            url: '<%=Application("RUTASEGURA")%>script/PMPortal/Consultas.asmx/LoginDesdeAsp',
            contentType: "application/json; charset=utf-8",            
            dataType: "json",
            async: false
        })).done(function (msg) {                          
            switch (<%=Request("Pagina")%>){
                case 1:
                    //Certificados
                    window.open('<%=Application("RUTASEGURA")%>script/PMPortal/script/certificados/extFrames2.aspx', ((<%=cint(AccesoExterno)%>==-1)?'_self':'default_main'));                      
                    break;
                case 2:
                    //Puntuaciones
                    window.open('<%=Application("RUTASEGURA")%>script/PMPortal/script/variablescalidad/PuntuacionesProveedor.aspx', ((<%=cint(AccesoExterno)%>==-1)?'_self':'default_main'));
                    break;
                case 3:
                    //Entregas
                    <%if oRaiz.sesion.MostrarFMT=0 then%>
		                window.open('<%=Application("RUTASEGURA")%>script/PMPortal/script/entregas/entregasPedidos.aspx', ((<%=cint(AccesoExterno)%>==-1)?'_self':'default_main'));
	                <%else%>
		                if (<%=oRaiz.sesion.MostrarFMT%>==1)
			                {
			                setTimeout('window.open("<%=Application("RUTASEGURA")%>script/PMPortal/script/entregas/entregasPedidos.aspx",((<%=cint(AccesoExterno)%>==-1)?'_self':'default_main'))',200)
			                winFormat = window.open('<%=Application("RUTASEGURA")%>script/common/introformatos.asp?Idioma=<%=Idioma%>&modificar=-1&func=pedidos', ((<%=cint(AccesoExterno)%>==-1)?'_self':'winFormat'), "width=600,height=400,location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no")
			                winFormat.focus()
			                }
		                else
			                window.open('<%=Application("RUTASEGURA")%>script/PMPortal/script/entregas/entregasPedidos.aspx',((<%=cint(AccesoExterno)%>==-1)?'_self':'default_main'))
	                <%end if%>
                    break;
                case 4:
                    //Solicitudes
                    window.open('<%=Application("RUTASEGURA")%>script/PMPortal/script/solicitudes/extFrames.aspx', ((<%=cint(AccesoExterno)%>==-1)?'_self':'default_main'))
                    break;    
                case 5: //JDEdwards
                    $(document).ready(function () {
                        $("#fraJDELogin").load(function () {                    
                            $("#fraJDELogin").attr("src", url + paginaCustom);                   
                        });
                    });
                    window.open(url + paginaCustom, "_blank","width=850,height=630, resizable=yes");
                    break; 
                case 6:	//Detalle de no conformidad                    
		            window.open('<%=Application("RUTASEGURA")%>script/PMPortal/script/noconformidad/extFrames.aspx?noconformidad=<%=server.URLEncode(request("noconformidad"))%>&instancia=<%=server.URLEncode(request("instancia"))%>', ((<%=cint(AccesoExterno)%>==-1)?'_self':'default_main'));            
		            break;
		        case 7:	//No conformidades cerradas  		    
		            window.open('<%=Application("RUTASEGURA")%>script/PMPortal/script/noconformidad/extFrames2.aspx', ((<%=cint(AccesoExterno)%>==-1)?'_self':'default_main'));	
		            break;   
                case 8:	//Visor facturas
		            window.open('<%=Application("RUTASEGURA")%>script/PMPortal/script/facturas/VisorFacturas.aspx', ((<%=cint(AccesoExterno)%>==-1)?'_self':'default_main'));	
		            break;                  
                case 9: //Seguimiento de facturas
                    window.open('<%=Application("RUTASEGURA")%>script/PMPortal/script/solicitudes/extFrames2.aspx?TipoVisor=5', ((<%=cint(AccesoExterno)%>==-1)?'_self':'default_main'));	
                    break;
                case 10: //Alta de facturas
                    window.open('<%=Application("RUTASEGURA")%>script/PMPortal/script/solicitudes/extFrames3.aspx?TipoSolicitud=13', ((<%=cint(AccesoExterno)%>==-1)?'_self':'default_main'));	
                    break;     
                case 11: //Solicitudes QA
                    window.open('<%=Application("RUTASEGURA")%>script/PMPortal/script/solicitudes/extFrames2.aspx?TipoVisor=7', ((<%=cint(AccesoExterno)%>==-1)?'_self':'default_main'));	
                    break;    
                case 12: //Encuestas
                    window.open('<%=Application("RUTASEGURA")%>script/PMPortal/script/solicitudes/extFrames2.aspx?TipoVisor=6', ((<%=cint(AccesoExterno)%>==-1)?'_self':'default_main'));	
                    break;
                case 13: //Alta encuestas
                    window.open('<%=Application("RUTASEGURA")%>script/PMPortal/script/solicitudes/extFrames3.aspx?TipoSolicitud=12', ((<%=cint(AccesoExterno)%>==-1)?'_self':'default_main'));
                    break;   
                case 14: //Alta solicitudes QA
                    window.open('<%=Application("RUTASEGURA")%>script/PMPortal/script/solicitudes/extFrames3.aspx?TipoSolicitud=14', ((<%=cint(AccesoExterno)%>==-1)?'_self':'default_main'));
                    break;  
                case 15: 
		             if (TipoOpen==1)
                       { 
                    window.open('<%=Application("RUTASEGURA")%>script/PMPortal/script/custom/' + pagCustom,"_blank","height=800,width=1000,resizable=yes,scrollbars=yes");}
                    else
                        
{window.open('<%=Application("RUTASEGURA")%>script/PMPortal/script/custom/' + pagCustom,"_self");}
                    break;                                                                                                                           
            
			}			
        });
    }    
</script>

<html> 
    <head>
        <script src="../common/formatos.js"></script>
        <script SRC="../common/menu.asp"></script>
        <script type="text/javascript" src="<%=application("RUTASEGURA")%>script/js/jquery.min.js"></script>
        <script language="JavaScript" src="../common/ajax.js"></script>
    </head>      
    <body onload="init()">
        <frameset rows="70,15,*,0,0"  frameborder="0" border="0" name=frSet id=frSet>        
        <frame name="fraJDELogin" id="fraJDELogin" src="">        
    </frameset> 
    </body>    
</html>

