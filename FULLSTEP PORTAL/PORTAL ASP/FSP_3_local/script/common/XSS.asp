﻿<%
''' <summary>
''' Controlar el cross-site scripting (XSS)
''' </summary>
''' <param name="Request">objeto Request de la pantalla a controlar</param>       
''' <returns>0 hay XSS / 1 no hay XSS</returns>
''' <remarks>Llamada desde: Toda pantalla en q queramos controlar el cross-site; Tiempo máximo:0,1</remarks>
Function ControlarXSS (Request)
	dim intRespuesta
	
	intRespuesta =1
	
	For Each elemento in Request.form 
		if CtrlPosible(Request.form(elemento))=0 then
			intRespuesta =  0
			exit for
		end if
	next
	For Each elemento in Request.querystring 
		if CtrlPosible(Request.querystring(elemento))=0 then
			intRespuesta = 0
			exit for
		end if
	next	

	ControlarXSS = intRespuesta
End Function

''' <summary>
''' Controlar el cross-site scripting (XSS) con una lista negra de codigo javascript
''' </summary>
''' <param name="strRequest">string a controlar</param>      
''' <returns>0 hay XSS / 1 no hay XSS</returns>
''' <remarks>Llamada desde:ControlarXSS; Tiempo máximo: 0,1</remarks>
Function CtrlPosible (strRequest)
	dim intRespuesta
	dim i
	dim ABuscar(128)
	dim j
	
	strRequest = QuitaEspacios(trim(Ucase(strRequest)))
	strRequest = QuitaAsciiHex(strRequest)

	intRespuesta=1
	
	if strRequest ="" then
	else		
		
		ABuscar(1) = "javascript"
		ABuscar(2) = "vbscript"
		ABuscar(3) = "expression"
		ABuscar(4) = "applet"
		ABuscar(5) = "meta"
		ABuscar(6) = "xml"
		ABuscar(7) = "blink"
		ABuscar(8) = "link"
		ABuscar(9) = "style"
		ABuscar(10) = "script"
		ABuscar(11) = "embed"
		ABuscar(12) = "object"
		ABuscar(13) = "iframe"
		ABuscar(14) = "frame"
		ABuscar(15) = "frameset"
		ABuscar(16) = "ilayer"
		ABuscar(17) = "layer"
		ABuscar(18) = "bgsound"
		ABuscar(19) = "title"
		ABuscar(20) = "base"
		ABuscar(21) = "onabort"
		ABuscar(22) = "onactivate"
		ABuscar(23) = "onafterprint"
		ABuscar(24) = "onafterupdate"
		ABuscar(25) = "onbeforeactivate"		
		ABuscar(26) = "onbeforecopy"
		ABuscar(27) = "onbeforecut"
		ABuscar(28) = "onbeforedeactivate"
		ABuscar(29) = "onbeforeeditfocus"
		ABuscar(30) = "onbeforepaste"
		ABuscar(31) = "onbeforeprint"		
		ABuscar(32) = "onbeforeunload"
		ABuscar(33) = "onbeforeupdate"
		ABuscar(34) = "onblur"
		ABuscar(35) = "onbounce"
		ABuscar(36) = "oncellchange"
		ABuscar(37) = "onchange"		
		ABuscar(38) = "onclick"
		ABuscar(39) = "oncontextmenu"
		ABuscar(40) = "oncontrolselect"
		ABuscar(41) = "oncopy"
		ABuscar(42) = "oncut"
		ABuscar(43) = "ondataavailable"		
		ABuscar(44) = "ondatasetchanged"		
		ABuscar(45) = "ondatasetcomplete"
		ABuscar(46) = "ondblclick"
		ABuscar(47) = "ondeactivate"		
		ABuscar(48) = "ondrag"
		ABuscar(49) = "ondragend"
		ABuscar(50) = "ondragenter"
		ABuscar(51) = "ondragleave"
		ABuscar(52) = "ondragover"
		ABuscar(53) = "ondragstart"		
		ABuscar(54) = "ondrop"	
		ABuscar(55) = "onerror"
		ABuscar(56) = "onerrorupdate"
		ABuscar(57) = "onfilterchange"		
		ABuscar(58) = "onfinish"
		ABuscar(59) = "onfocus"
		ABuscar(60) = "onfocusin"
		ABuscar(61) = "onfocusout"
		ABuscar(62) = "onhelp"
		ABuscar(63) = "onkeydown"		
		ABuscar(64) = "onkeypress"	
		ABuscar(65) = "onkeyup"
		ABuscar(66) = "onlayoutcomplete"
		ABuscar(67) = "onload"		
		ABuscar(68) = "onlosecapture"
		ABuscar(69) = "onmousedown"
		ABuscar(70) = "onmouseenter"
		ABuscar(71) = "onmouseleave"
		ABuscar(72) = "onmousemove"
		ABuscar(73) = "onmouseout"	
		ABuscar(74) = "onmouseover"	
		ABuscar(75) = "onmouseup"
		ABuscar(76) = "onmousewheel"
		ABuscar(77) = "onmove"		
		ABuscar(78) = "onmoveend"
		ABuscar(79) = "onmovestart"
		ABuscar(80) = "onpaste"
		ABuscar(81) = "onpropertychange"
		ABuscar(82) = "onreadystatechange"
		ABuscar(83) = "onreset"		
		ABuscar(84) = "onresize"
		ABuscar(85) = "onresizeend"	
		ABuscar(86) = "onresizestart"
		ABuscar(87) = "onrowenter"
		ABuscar(88) = "onrowexit"		
		ABuscar(89) = "onrowsdelete"
		ABuscar(90) = "onrowsinserted"
		ABuscar(91) = "onscroll"
		ABuscar(92) = "onselect"
		ABuscar(93) = "onselectionchange"
		ABuscar(94) = "onselectstart"		
		ABuscar(95) = "onstart"
		ABuscar(96) = "onstop"	
		ABuscar(97) = "onsubmit"
		ABuscar(98) = "onunload"
        ABuscar(99) = "alert"
        ABuscar(100) = "onbegin"
        ABuscar(101) = "ondragdrop"
        ABuscar(102) = "onend"
        ABuscar(103) = "onhashchange"
        ABuscar(104) = "oninput"
        ABuscar(105) = "onmediacomplete"
        ABuscar(106) = "onmediaerror"
        ABuscar(107) = "onmessage"
        ABuscar(108) = "onoffline"
        ABuscar(109) = "ononline"
        ABuscar(110) = "onoutofsync"
        ABuscar(111) = "onpause"
        ABuscar(112) = "onpopstate"
        ABuscar(113) = "onprogress"
        ABuscar(114) = "onredo"
        ABuscar(115) = "onrepeat"
        ABuscar(116) = "onresume"
        ABuscar(117) = "onreverse"
        ABuscar(118) = "onrowsenter"
        ABuscar(119) = "onrowinserted"
        ABuscar(120) = "onseek"
        ABuscar(121) = "onstorage"
        ABuscar(122) = "onsyncrestored"
        ABuscar(123) = "ontimeerror"
        ABuscar(124) = "ontrackchange"
        ABuscar(125) = "onundo"
        ABuscar(126) = "onurlflip"
        ABuscar(127) = "seeksegmenttime"

        ABuscar(128) = "ahref"
									
		for i = 1 to 20
			if CtrlPosibleEntrada (strRequest,Ucase("<" & ABuscar(i))) = 0 then
				intRespuesta =  0
				exit for
			end if
		next

        if intRespuesta =  1 then
			for i = 21 to 127
				if CtrlPosibleEntrada (strRequest,Ucase(ABuscar(i))) = 0 then
					intRespuesta =  0
					exit for
				end if	
			next
		end if		

        if intRespuesta =  1 then
            if CtrlPosibleEntrada (strRequest,Ucase("<" & ABuscar(128))) = 0 then
				intRespuesta =  0
			end if
        end if
	end if
	
	CtrlPosible  = intRespuesta
End Function

''' <summary>
''' Controlar el cross-site scripting (XSS) contra una entrada de la lista negra javascript
''' </summary>
''' <param name="strRequest">string a controlar</param>      
''' <param name="strABuscar">string lista negra</param>   
''' <returns>0 hay XSS / 1 no hay XSS</returns>
''' <remarks>Llamada desde: CtrlPosible; Tiempo máximo:0,1</remarks>
Function CtrlPosibleEntrada (strRequest,strABuscar)
	dim j
	dim k
	dim i
	
	dim car_ABuscar
	dim car_strRequest
	
	dim esBueno
    dim bEsEvento ' Me dice si el string de la lista negra es un evento
	
	bEsEvento = false
	
	car_ABuscar= mid(strABuscar, 1, 1)

    if car_ABuscar <> "<" then
	    bEsEvento = true
	end if
		
	for j = 1 to len(strRequest)
		car_strRequest= mid(strRequest, j, 1)
			
		if car_ABuscar = car_strRequest then
		
			i =j+1	
			esBueno = false
			
			for k = 2 to len(strABuscar)
				if mid(strRequest,i,1) = mid(strABuscar,k,1) then
					i = i + 1
				else
					esBueno = true
					exit for
				end if
			next
			
			if esBueno = false then
			    if bEsEvento then ' Si se trata de un evento se calcula el carácter siguiente al de la entrada y se da ésta por válida si el carácter es letra o número
			        carSiguiente_strRequest = mid(strRequest, i, 1)
			        if RegExpTest("[0-9A-ZÀ-Ü]", carSiguiente_strRequest) then
			            CtrlPosibleEntrada= 1
				        exit function
			        end if
			    end if
				CtrlPosibleEntrada= 0
				exit function
			end if
		end if
	next
	
	CtrlPosibleEntrada= 1
	
End Function

''' <summary>
''' Trasformar en strRequest los caracteres q me vinieran en decimal o hexadecimal en codigo ascii
''' </summary>
''' <param name="strRequest">string a controlar</param>          
''' <returns>strRequest con los caracteres convertidos a ascii</returns>
''' <remarks>Llamada desde:CtrlPosible; Tiempo máximo:0</remarks>
Function QuitaAsciiHex (strRequest)
	dim i 
	dim j
	dim k
	dim sAux
	dim car
	dim asci3
	dim asci4
	dim c_asci3
	dim c_asci4

	sAux =""
	NumCar =len(strRequest)
	i=1
	
	while i < NumCar + 1		
		car= mid(strRequest, i,1)
		asci3= mid(strRequest, i,3)
		asci4= mid(strRequest, i,4)
	
		if car= "%" or car ="\" then
			encontrado=false
			
			'mayusculas
			k= 41
						
			for j= 65 to 90
				c_asci3 = "%" & cstr(j)
				
				select case j
				case 74
					c_asci4 = "\X4A"			
				case 75
					c_asci4 = "\X4B"			
				case 76
					c_asci4 = "\X4C"			
				case 77
					c_asci4 = "\X4D"			
				case 78
					c_asci4 = "\X4E"			
				case 79
					c_asci4 = "\X4F"			
				case 90			
					c_asci4 = "\X5A"
				case else				
					c_asci4 = "\X" & cstr(k)
					k= k+1
				end select

				if asci3 = c_asci3 then 
					sAux= sAux & chr(j)
					
					i=i+2
					encontrado=true
					
					exit for
				elseif asci4 = c_asci4 then 
					sAux= sAux & chr(j)
					
					i=i+3
					encontrado=true
					
					exit for
				end if			
			next
			
			'minusculas
			k= 61
			for j= 97 to 122
				c_asci3 = "%" & cstr(j)
				
				select case j
				case 106
					c_asci4 = "\X6A"			
				case 107
					c_asci4 = "\X6B"			
				case 108
					c_asci4 = "\X6C"			
				case 109
					c_asci4 = "\X6D"			
				case 110
					c_asci4 = "\X6E"			
				case 111
					c_asci4 = "\X6F"			
				case 122			
					c_asci4 = "\X7A"
				case else				
					c_asci4 = "\X" & cstr(k)
					k= k+1
				end select

				if asci3 = c_asci3 then 
					sAux= sAux & Ucase(chr(j))
					
					i=i+2
					encontrado=true
					
					exit for
				elseif asci4 = c_asci4 then 
					sAux= sAux & Ucase(chr(j))
					
					i=i+3
					encontrado=true
					
					exit for
				end if			
			next
			
			if encontrado = false then
				sAux= sAux & car
			end if
		else
			sAux= sAux & car
		end if
		
		i=i+1				
	wend
	
	QuitaAsciiHex = sAux
End Function

''' <summary>
''' Trasformar el strRequest quitandole los espacios
''' </summary>
''' <param name="strRequest">string a controlar</param>          
''' <returns>strRequest sin espacios</returns>
''' <remarks>Llamada desde:CtrlPosible; Tiempo máximo:0</remarks>
Function QuitaEspacios (strRequest)
	dim sAux
	dim i
	dim NumCar
	dim car
	dim asci3
	dim asci4

	sAux =""
	NumCar =len(strRequest)
	i=1
	
	while i < NumCar + 1
		car= mid(strRequest, i,1)
		asci3= mid(strRequest, i,3)
		asci4= mid(strRequest, i,4)
	
		if car =" " then 
			i = i + 1
		elseif asci3 = "%32" then 
			i = i + 3
		elseif asci4 = "\X20" then
			i = i + 4
		else
			sAux = sAux & car
			
			i = i + 1
		end if		
	wend
	
	QuitaEspacios =  sAux
End Function

''' <summary>
''' Encuentra las coincidencias con el patrón de búsqueda
''' </summary>
''' <param name="strMatchPattern">patrón de búsqueda</param>          
''' <param name="strPhrase">cadena a comparar con el patrón de búsqueda</param>          
''' <returns>true o false</returns>
''' <remarks>Llamada desde:CtrlPosibleEntrada; Tiempo máximo:0</remarks>

Function RegExpTest(strMatchPattern, strPhrase)
    'crea variables
    Dim objRegEx, Match, Matches, StrReturnStr
    'crea instancia del objeto RegExp
    Set objRegEx = New RegExp 

    'encuentra todas las coincidencias
    objRegEx.Global = True
    'set case insensitive
    objRegEx.IgnoreCase = True
    'set the pattern
    objRegEx.Pattern = strMatchPattern 

    'crea la coleccion de coincidencias
    Set Matches = objRegEx.Execute(strPhrase) 

    RegExpTest = false
 
    For Each Match in Matches
        RegExpTest = true
    Next
End Function 

Function NoHtmlEnParametroExpuesto(strParam) 
    dim strAux

    if strParam ="" then
        strAux=strParam
    else
        'Se busca cualquier forma de <
        strAux = QuitaAsciiHex(strParam)
        if InStr(strAux,"<") >0 then
            strAux=mid(strAux,1,InStr(strAux,"<")-1)
        else
            strAux=strParam
        end if
    end if

    NoHtmlEnParametroExpuesto = strAux

End Function

function strip_tags(pageRequest)
    dim todoOK
    todoOK=true

    dim regEx 
    set regEx = new RegExp

    with regEx
        .Global = true
        .IgnoreCase = true
        .Pattern = "(\<(/?[^\>]+)\>)"
    end with
    
    For Each elemento in pageRequest.querystring        
        if Request.querystring(elemento) <> regEx.Replace(Request.querystring(elemento), "") then
            todoOK=false            
            exit for
        end if
	next	
    
    if todoOK then
        For Each elemento in pageRequest.form 
            if Request.form(elemento) <> regEx.Replace(Request.form(elemento), "") then
                todoOK=false                
                exit for
            end if		    
	    next
    end if
     
    set regEx = nothing

    strip_tags=todoOK
end function

Function HTMLEncode(ByVal sVal)
    sReturn = ""

    If ((TypeName(sVal)="String") And (Not IsNull(sVal)) And (sVal<>"")) Then    
        For jj = 1 To Len(sVal)        
            ch = Mid(sVal, jj, 1)

            Set oRE = New RegExp : oRE.Pattern = "[ a-zA-Z0-9ñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙâêîôûÂÊÎÔÛ]"

            If (Not oRE.Test(ch)) Then
                ch = "&#" & Asc(ch) & ";"
            End If

            sReturn = sReturn & ch            
            Set oRE = Nothing
        Next
    End If
    
    HTMLEncode = sReturn
End Function

''' <summary>
''' Controlar el cross-site scripting (XSS)
''' </summary> 
''' <returns>Si hay XSS te redirige a una pantalla de error</returns>
''' <remarks>Llamada desde: Toda pantalla en q queramos controlar el cross-site; Tiempo máximo:0,1</remarks> 
dim todoCorrecto
dim respuesta
todoCorrecto=true
respuesta=1

todoCorrecto=strip_tags(Request)
if todoCorrecto then
    respuesta = ControlarXSS(Request)
end if

if not todoCorrecto or respuesta=0 then
	Response.Redirect Application ("RUTANORMAL") & "/script/MensajeXSS.asp?Idioma=SPA" 
	Response.End  
end if

%>

