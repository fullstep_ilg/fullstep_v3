﻿<%
''' <summary>
''' Carga un div encima de la pantalla de inicio con hipervinculos a lo que tenga que prestar atención el proveedor.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:inicio.asp ; Tiempo máximo:0</remarks>
Idioma=Request.Cookies ("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,true,true,0)

dim den
den = devolverTextos(Idioma,124)

lCiaComp = clng(oRaiz.Sesion.CiaComp)
idCia = clng(oRaiz.Sesion.CiaId)
codprove = oRaiz.Sesion.CiaCod

codUsuario =oRaiz.Sesion.UsuCod

set oSolicitudes = oRaiz.Generar_CSolicitudes

sPyme = Application("PYME")

set ador = oSolicitudes.DevolverPendientes(lCiaComp,idCia,codUsuario,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"))
set adorSol = oSolicitudes.DevolverPendientesSolicitudes(lCiaComp,oRaiz.Sesion.CiaCodGs,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"))
set adorCert = oSolicitudes.DevolverPendientesCertificados(lCiaComp,Idioma,codprove)
set adorOfertas = oSolicitudes.DevolverPendientesOfertas(lCiaComp,codprove,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"))
set adorFact = oSolicitudes.DevolverPendientesFacturas(lCiaComp,codprove)

set oSolicitudes = nothing

if oRaiz.Sesion.Pedidos > 0 then
    iConPedidos = 1
else
    iConPedidos = 0
end if

iConSolicitudes = oRaiz.Sesion.PmSolicitudes
iConCertificados = oRaiz.Sesion.Certificados
iConNoConformidades = oRaiz.Sesion.NoConformidades
iConOfertas=1
iConFacturas=1

if (ador.EOF) then
	set ador= nothing
else
	if (adorOfertas("PROCE_NUE").Value=0) then iConOfertas =0
	if (ador("ORDEN_NUE").Value=0) then iConPedidos =0
	if (adorCert(0).value=0) then iConCertificados =0
	if (oRaiz.Sesion.NoConfNuevas=0) then iConNoConformidades =0
	if (adorSol(0).Value=0) then iConSolicitudes =0    
    if (adorFact(0).Value=0) then iConFacturas =0
    if (iConFacturas=1) then
        if not (oRaiz.EsContactoEnGS(oRaiz.Sesion.CiaComp,oRaiz.Sesion.UsuId,oRaiz.Sesion.CiaCodGs)) then
            iConFacturas =0
        end if
    end if
end if
iBajaLog=oRaiz.Sesion.BajaCalidad

if iBajaLog=0 then
else
	iConCertificados=0
	iConNoConformidades=0
end if

if (iConOfertas+iConPedidos+iConCertificados+iConNoConformidades+iConSolicitudes+iConFacturas=0) then
	set ador= nothing
end if

%>

<script language="javascript">
function MostrarNo() {
    divPendiente.style.visibility = "hidden"
    setCookie("MostrarNo", "1", new Date(2035, 11, 31))
}

function Ocultar(){
	divPendiente.style.visibility = "hidden"
}

function setCookie(name, value, expires) {
    document.cookie = name + '=' + value + '; expires=' + expires.toGMTString() 
}

function clearCookie(name){    
	expires = new Date();
    expires.setYear(expires.getYear() - 1);

    document.cookie = name + '=null' + '; expires=' + expires.toGMTString(); 		 
}
</script>

<%If Not ador is nothing Then%>
	<div id="divPendiente" name="divPendiente" style="BACKGROUND-COLOR:gray; position:absolute;top:100px;left:320px;width:348px;overflow-y:none;color:#000000;z-index:1000">
        <table width="98%" height="98%" align="center" valign="center" border="0" cellspacing="0px">
        <tr class="lineaverde" height="3px">
			<td></td>
			<td></td>
        </tr>        
        <tr class="titulop" height="30px">
			<td>FULLSTEP <%=den(10)%>&nbsp;<%=VisualizacionFecha(ador("DIA").Value, "dd/mm/yyyy")%></td>
			<td align="Right"><img onclick="Ocultar()" style="cursor:pointer" SRC="<%=application("RUTASEGURA")%>script/images/Cerrar.gif">&nbsp;&nbsp;</td>
        </tr>
        <tr class="subtitulo2p">
			<td><%=den(1)%>&nbsp;<%=HTMLEncode(ador("NOMBRE").Value)%></td>
			<td></td>
        </tr>
        <tr class="subtitulo1p">
			<td><%=den(2)%></td>
			<td></td>
        </tr>
        <tr class="lineap" height="12px">
			<td></td>
			<td></td>
        </tr>
        <%If iConOfertas=1 then%>
        <tr class="lineap">
			<td><a href="#" onclick="irAOfertas()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=den(3)%></a></td>
			<td onclick="irAOfertas()"><%=adorOfertas("PROCE_NUE").Value%></td>
		</tr>
		<%End If%>
		<%If iConPedidos=1 then%>
        <tr class="lineap">
			<td><a href="#" onclick="irAPedidosPendientes()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=den(4)%></a></td>
			<td onclick="irAPedidosPendientes()"><%=ador("ORDEN_NUE").Value%></td>
        </tr>
        <%End If%>
        <%If iConCertificados>0 then%>
        <tr class="lineap">
			<td><a href="#" onclick="irACertificados()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=den(5)%></a></td>
			<td onclick="irACertificados()"><%=adorCert(0).value%></td>
        </tr>
        <%End If%>
        <%If iConNoConformidades>0 then%>
        <tr class="lineap">
			<td><a href="#" onclick="irANoConformidades()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=den(6)%></a></td>
			<td onclick="irANoConformidades()"><%=oRaiz.Sesion.NoConfNuevas%></td>
        </tr>
        <%End If%>
        <%If iConSolicitudes>0 then%>
        <tr class="lineap">
			<td width="80%"><a href="#" onclick="irASolicitudes()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=den(7)%></a></td>
			<td width="20%" onclick="irASolicitudes()"><%=adorSol(0).Value%></td>
        </tr>
        <%End If%>
        <%if iConFacturas>0 then%>
		<tr class="lineap">
			<td width="80%"><a href="#" onclick="irAFacturas()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=den(9)%></a></td>
			<td width="20%" onclick="irAFacturas()"><%=adorFact(0).Value%></td>
        </tr>
        <%End If%>
        <tr class="lineap" height="12px">
			<td></td>
			<td></td>
        </tr>
        <tr class="lineacheck">
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="NoMuestra" name="NoMuestra" type="checkbox" onclick="MostrarNo()" class="checkbox"><%=den(8)%></td>
			<td align="center"></td>			
        </tr>
        <tr class="lineap" height="12px">
			<td></td>
			<td></td>
		<tr class="lineaverde" height="3px">
			<td></td>
			<td></td>
        </tr>  
        </tr>
        </table>
	</div>
<%End If%>

<script language="JavaScript" type="text/JavaScript">
function GetCookie (name) { 
	var arg = name + "=" 
	var alen = arg.length 
	var clen = document.cookie.length
	var cookies = document.cookie.split("&") 
	re = eval ("/" + name + "=/") 
	re2 = /=/ 
	var i = 0
	for (i = 0;i<=cookies.length-1;i ++) { 
		if (cookies[i].search(re)==0) { 
			ret = cookies[i].substr(cookies[i].search(re2)+ 1,1) 
			return(ret) 
		} 
	} 
}

if (GetCookie("MostrarNo")!=1)
{
	//document.getElementById("divContenedor").innerHTML="<DIV id=divPendiente name='divPendiente' style='BACKGROUND-COLOR: darkgreen; position:absolute;top:300px;left:420px;width:350px;height:125px;overflow-y:none;color:''#000000'';padding-right:10px;z-index:1000'>" + document.getElementById("divContenedor").innerHTML
}
else
{
//	document.getElementById("divPendiente").innerHTML=""
	document.getElementById("divPendiente").style.visibility="hidden"
}

</script>

<%

set oRaiz = nothing
%>

