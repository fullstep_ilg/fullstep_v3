﻿
<%

dim ie
dim ie2
dim ie3
dim ie4
dim ie5
dim ie55
dim ns
dim ns2
dim ns3
dim ns4
dim ie5M

VersionNavegador=Request.ServerVariables("HTTP_USER_AGENT")
ie=false
ie2=false
ie3=false
ie4=false
ie5=false
ie55=false
ns=false
ns2=false
ns3=false
ns4=false

If InStr(VersionNavegador,"MSIE") Then
	ie=true
	If InStr(VersionNavegador,"MSIE 2") Then ie2=true End If
	If InStr(VersionNavegador,"MSIE 3") Then ie3=true End If
	If InStr(VersionNavegador,"MSIE 4") Then ie4=true End If
	If InStr(VersionNavegador,"MSIE 5") Then ie5=true End If
	If InStr(VersionNavegador,"MSIE 5.5") Then ie55=true End If
	If InStr(VersionNavegador,"MSIE 6") Then ie6=true End If
	If InStr(VersionNavegador,"DigExt") Then 
		ie5 = true
		ie5M = true
	End If
else
	ns=true
	If InStr(VersionNavegador,"Mozilla/2")  Then ns2=true End If
	If InStr(VersionNavegador,"Mozilla/3.") Then ns3=true End If
	If InStr(VersionNavegador,"Mozilla/4.") Then ns4=true End If
end if 





function devolverTextos(Idioma, Modulo)
dim den()
dim i
dim numTextos
dim oIdi


set oIdi = CreateObject ("FSPIdiomas_" & Application ("NUM_VERSION") & ".CGestorIdiomasP")
oIdi.conectar application("INSTANCIA")


set ador = oIdi.DevolverTextoModulo (Modulo)


numTextos = ador.recordcount
redim den(numTextos)


for i= 1 to numTextos
	Den(i) = ador(Idioma).value
	ador.Movenext
next

ador.close
set ador=nothing
set oIdi = nothing

devolverTextos=den

end function

''' <summary>Devuelve un string que indica el navegador del cliente
'''         MSIE:explorer
'''         OPR:Opera
'''         CHR:Chrome
'''         MOZ:Mozilla
'''         SAF:Safari
'''</summary>
''' <remarks>Llamada desde: politicacookies.asp, usuarionovalido.asp</remarks>
Function getNavegador()
    dim nav
    nav=Request.ServerVariables("HTTP_USER_AGENT")
    getNavegador="IE"
    if instr(nav,"MSIE") then 
        getNavegador="IE"
    elseif instr(nav,"OPR") then
        getNavegador="OPR"
    elseif instr(nav,"Chrome") then 
        getNavegador="CHR"
    elseif instr(nav,"Mozilla") then 
        getNavegador="MOZ"
    elseif instr(nav,"Safari") then
        getNavegador="SAF"
    end if        
end function
'''Devuelve el nombre del navegador del cliente
''' <remarks>Llamada desde: politicacookies.asp</remarks>
function getNavegadorNombre()
    dim nav
    nav=Request.ServerVariables("HTTP_USER_AGENT")
    getNavegadorNombre=""
    if instr(nav,"MSIE") then 
        getNavegadorNombre="Internet Explorer"
    elseif instr(nav,"OPR") then
        getNavegadorNombre="Opera"
    elseif instr(nav,"Chrome") then 
        getNavegadorNombre="Google Chrome"
    elseif instr(nav,"Mozilla") then 
        getNavegadorNombre="Mozilla Firefox"
    elseif instr(nav,"Safari") then
        getNavegadorNombre="Safari"
    end if    
end function

%>


