﻿<%@ Language=VBScript %>
<!--#include file="acceso.asp"-->
<!--#include file="idioma.asp"-->
<!--#include file="colores.asp"-->
<!--#include file="formatos.asp"-->

<%
''' <summary>
''' Mostrar la descripción de un atributo de especificación
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.asp ; Tiempo máximo: 0,2</remarks>

	Idioma = Request.Cookies("USU_IDIOMA")
	atrib=Request("atrib")

	set oRaiz=validarUsuario(Idioma,true,false,0)

	CiaComp = oRaiz.Sesion.CiaComp

	dim den
	
	den = devolvertextos(Idioma,92)

    
	set oAtriEspecificaciones = oRaiz.Generar_CAtributosEspe	

	anyo = request("anyo")
	gmn1 = request("gmn1")
	proce = request("proce")
	grupo = request("grupo")
	atrib = request("atrib")	
	item = request("item")
	
	ambito = clng(request("ambito"))
	 
	'if ambito = 1 then
	'		oAtriEspecificaciones.DevolverTodosLosAtributosEspeDesde ciacomp, anyo, gmn1, proce, , , atrib,item
	'	else
	'		oAtriEspecificaciones.DevolverTodosLosAtributosEspeDesde ciacomp, anyo, gmn1, proce, grupo, ambito, atrib,item
	'	end if

	select case ambito
		case 1
			oAtriEspecificaciones.DevolverTodosLosAtributosEspeDesde ciacomp, anyo, gmn1, proce, , ambito, atrib
		case 2
			oAtriEspecificaciones.DevolverTodosLosAtributosEspeDesde ciacomp, anyo, gmn1, proce, grupo, ambito, atrib
		case 3
			oAtriEspecificaciones.DevolverTodosLosAtributosEspeDesde ciacomp, anyo, gmn1, proce, , ambito, atrib,item			
	end select


	set oAtriEspecificacion = oAtriEspecificaciones.item(atrib)

%>

<HTML>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">

<TITLE><%=title%></TITLE>

<SCRIPT>

function Cerrar()
{
	window.close()
}

</SCRIPT>

</HEAD>


<BODY topmargin="10" leftmargin="0">


<TABLE class="normal" align="center" WIDTH="98%" border="0" cellspacing="1" cellpadding="1">
	<TR>
		<TD width="5%" class="cabecera"><%=den(2)%></td>
		<TD class="filaPar" ><%=HTMLEncode(oAtriEspecificacion.cod)%></td>
	</TR>
	<TR>
		<TD width="35%" class="cabecera"><%=den(3)%></td>
		<TD class="filaPar"><%=HTMLEncode(oAtriEspecificacion.Desc)%></TD>
	</TR>
	<TR>
		<TD width="35%" class="cabecera"  valign=top><%=den(29)%></td>
		<TD class="filaPar"><%=VB2HTML(oAtriEspecificacion.descr)%></TD>
	</TR>

	<TR>
		<TD height=40px align=center COLSPAN=2>
			<INPUT TYPE="button" class="button" ID=Cerrar LANGUAGE=javascript class="Aprov" value="<%=Den(19)%>" onclick="Cerrar()">
		</TD>
	</TR>
	
</TABLE>	
			

</BODY>
</HTML>

<%
set oAtriEspecificaciones = nothing

set oRaiz = nothing
%>