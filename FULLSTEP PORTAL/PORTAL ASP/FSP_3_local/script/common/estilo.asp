﻿<link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet"> 
<%
''' <summary>
''' Cargar los estilos
''' </summary>     
''' <remarks>Llamada desde: Unos 186 aspes lo usan para coger estilo; Tiempo máximo: 0</remarks>%>

<%response.ContentType="text/css"%>

<!--#include file="colores.asp"-->
<%   
	Server.Execute("../../custom/" & application("NOMPORTAL") & "/public/cn_Styles.css")
	Server.Execute("../../custom/" & application("NOMPORTAL") & "/public/cn_Styles_plugin.css")
%>

@font-face {
  font-family: 'Gotham-Book';
  src: local('Gotham-Book'), url(<%=Application("RUTASEGURA")%>script/common/ttf/Gotham-Book.ttf);
}

<%if UCase(application("NOMPORTAL")) = UCase("FED") or UCase(application("NOMPORTAL")) = UCase("mai") then%>
BODY {font-family:"Arial";font-size:10px;background-color:#ffffff}
<%elseif UCase(application("NOMPORTAL")) = UCase("mai") then%>
BODY {font-family:"Arial";font-size:10px;background-color:#ffffff}
<%elseif UCase(application("NOMPORTAL")) = UCase("FIC") then%>
<%'BODY {font-family:"Arial";font-size:10px;background-color:#B9E3E7;background-repeat:no-repeat;background-image:url(../../custom/ficosa/images/fondo_ficosa.gif);}%>
BODY {font-family:"Arial";font-size:10px;background-color:#ffffff}
<%elseif UCase(application("NOMPORTAL"))=UCase("HUF") then%>
BODY {font-family:"Arial";font-size:12px;background-color:#ffffff}
<%elseif UCase(application("NOMPORTAL"))=UCase("cfg") then%>
BODY {font-family:"Arial";font-size:10px;background-color:#ffffff}
<%elseif UCase(application("NOMPORTAL"))=UCase("full") then%>
BODY {font-family:"Verdana";font-size:10px;background-color:#ffffff}
<%elseif UCase(application("NOMPORTAL"))=UCase("BSA") then%>
BODY {font-family:"Arial";font-size:10px;background-color:#ffffff}
<%elseif UCase(application("NOMPORTAL"))=UCase("BAN") then%>
BODY {font-family:"verdana";font-size:12px;background-color:#ffffff}
<%elseif UCase(application("NOMPORTAL"))=UCase("PPL") then%>
BODY {font-family:"Arial";font-size:12px;background-color:#ffffff}
<%elseif UCase(application("NOMPORTAL"))=UCase("ILU") then%>
BODY {font-family:"Arial";font-size:12px;background-color:#ffffff}
<%elseif UCase(application("NOMPORTAL"))=UCase("HRE") then%>
BODY {font-family:'Comfortaa', Verdana;font-size:12px;background-color:#ffffff}
<%elseif UCase(application("NOMPORTAL"))=UCase("BER") then%>
BODY {font-family:"Calibri";font-size:12px;background-color:#ffffff}
<%elseif UCase(application("NOMPORTAL"))=UCase("SBFE") then%>
BODY {font-family:Gotham-Book;font-size:12px;background-color:#ffffff}
<%elseif UCase(application("NOMPORTAL"))=UCase("PES") then%>
BODY {font-family:Gotham-Book;font-size:12px;background-color:#ffffff}
<%else%>
BODY {font-family:"Verdana";font-size:10px;background-color:#ffffff}
<%end if%>
<%if UCase(application("NOMPORTAL")) = UCase("BAN") then%>
SELECT {font-family:"Verdana";font-size:12px;}
OPTION  {font-family:"Verdana";font-weight:normal;font-size:11px;}
TD {font-family:"Verdana";font-size:11px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("HRE") then%>
SELECT {font-family:'Comfortaa', Verdana;font-size:12px;}
OPTION  {font-family:'Comfortaa', Verdana;font-weight:normal;font-size:11px;}
TD {font-family:'Comfortaa', Verdana;font-size:11px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("ILU") then%>
SELECT {font-family:"Arial";font-size:12px;}
OPTION  {font-family:"Arial";font-weight:normal;font-size:11px;}
TD {font-family:"Arial";font-size:14px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("BER") then%>
SELECT {font-family:"Calibri";font-size:12px;}
OPTION  {font-family:"Calibri";font-weight:normal;font-size:11px;}
TD {font-family:"Calibri";font-size:11px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("SBFE") then%>
SELECT {font-family:Gotham-Book;font-size:12px;}
OPTION  {font-family:Gotham-Book;font-weight:normal;font-size:11px;}
TD {font-family:Gotham-Book;font-size:11px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("PES") then%>
SELECT {font-family:Gotham-Book;font-size:12px;}
OPTION  {font-family:Gotham-Book;font-weight:normal;font-size:11px;}
TD {font-family:Gotham-Book;font-size:11px;}
<%else%>
SELECT {font-family:"Verdana";font-size:10px;}
OPTION  {font-family:"Verdana";font-weight:normal;font-size:10}
TD {font-family:"Verdana";font-size:10px;}
<%end if%>
<%if UCase(application("NOMPORTAL")) = UCase("FED") then%>
INPUT {font-family:"Arial";font-size:10px;BORDER: <%=cBORDEINPUT%> 1px solid;}
INPUT.button {font-family:"Arial";font-size:10px;font-weight:bold;min-width:80px;overflow:visible;padding-left:5px;padding-right:5px;border-style:outset;}
INPUT.radio {font-family:"Arial";font-size:10px;font-weight:bold;border-style:none;}
INPUT.checkbox {font-family:"Arial";font-size:10px;font-weight:bold;border-style:none;}
TEXTAREA {font-family:"Arial";font-size:10px;BORDER: <%=cBORDEINPUT%> 1px solid;}
<%elseif UCase(application("NOMPORTAL")) = UCase("pri") then%>
INPUT {font-family:"Verdana";font-size:10px;BORDER: <%=cBORDEINPUT%> 1px solid;}
INPUT.button {font-size:10px;font-weight:bold;min-width:80px;overflow:visible;padding-left:5px;padding-right:5px;border-style:outset;}
INPUT.radio {font-size:10px;font-weight:bold;border-style:none;}
INPUT.checkbox {font-size:10px;font-weight:bold;border-style:none;}
TEXTAREA {font-family:"Verdana";font-size:10px;BORDER: <%=cBORDEINPUT%> 1px solid;}
<%elseif UCase(application("NOMPORTAL")) = UCase("FIC") or UCase(application("NOMPORTAL")) = UCase("huf") then%>
INPUT {font-family:"Arial";font-size:11px;BORDER: <%=cBORDEINPUT%> 1px solid;}
INPUT.button {font-family:"Arial";font-size:10px;font-weight:bold;min-width:80px;overflow:visible;padding-left:5px;padding-right:5px;border-style:outset;}
INPUT.radio {font-family:"Arial";font-size:10px;font-weight:bold;border-style:none;}
INPUT.checkbox {font-family:"Arial";font-size:10px;font-weight:bold;border-style:none;}
TEXTAREA {font-family:"Arial";font-size:11px;BORDER: <%=cBORDEINPUT%> 1px solid;}
<%elseif UCase(application("NOMPORTAL")) = UCase("cfg") then%>
INPUT {font-family:"Arial";font-size:11px;BORDER: <%=cBORDEINPUT%> 1px solid;}
INPUT.button {font-family:"Arial";font-size:10px;font-weight:bold;min-width:80px;overflow:visible;padding-left:5px;padding-right:5px;border-style:outset;}
INPUT.radio {font-family:"Arial";font-size:10px;font-weight:bold;border-style:none;}
INPUT.checkbox {font-family:"Arial";font-size:10px;font-weight:bold;border-style:none;}
TEXTAREA {font-family:"Arial";font-size:11px;BORDER: <%=cBORDEINPUT%> 1px solid;}
<%elseif UCase(application("NOMPORTAL")) = UCase("BAN") then%>
INPUT {font-family:"Verdana";font-size:12px;BORDER: <%=cBORDEINPUT%> 1px solid;}
INPUT.button {font-family:"Verdana";font-size:12px;font-weight:bold;min-width:80px;overflow:visible;padding-left:5px;padding-right:5px;border-style:outset;}
INPUT.radio {font-family:"Verdana";font-size:12px;font-weight:bold;border-style:none;}
INPUT.checkbox {font-family:"Verdana";font-size:12px;font-weight:bold;border-style:none;}
TEXTAREA {font-family:"Verdana";font-size:11px;BORDER: <%=cBORDEINPUT%> 1px solid;}
<%elseif UCase(application("NOMPORTAL")) = UCase("OCA") then%>
INPUT {font-family:"Verdana";font-size:12px;BORDER: <%=cBORDEINPUT%> 1px solid;}
INPUT.button {font-family:"Verdana";font-size:12px;font-weight:bold;min-width:80px;overflow:visible;padding-left:5px;padding-right:5px;border-style:outset;}
INPUT.radio {font-family:"Verdana";font-size:12px;font-weight:bold;border-style:none;}
INPUT.checkbox {font-family:"Verdana";font-size:12px;font-weight:bold;border-style:none;}
TEXTAREA {font-family:"Verdana";font-size:11px;BORDER: <%=cBORDEINPUT%> 1px solid;}
<%elseif UCase(application("NOMPORTAL")) = UCase("ETE") then%>
INPUT {font-family:"Verdana";font-size:12px;BORDER: <%=cBORDEINPUT%> 1px solid;}
INPUT.button {font-family:"Verdana";font-size:12px;font-weight:bold;min-width:80px;overflow:visible;padding-left:5px;padding-right:5px;border-style:outset;}
INPUT.radio {font-family:"Verdana";font-size:12px;font-weight:bold;border-style:none;}
INPUT.checkbox {font-family:"Verdana";font-size:12px;font-weight:bold;border-style:none;}
TEXTAREA {font-family:"Verdana";font-size:11px;BORDER: <%=cBORDEINPUT%> 1px solid;}
<%elseif UCase(application("NOMPORTAL")) = UCase("SER") then%>
INPUT {font-family:"Verdana";font-size:12px;BORDER: <%=cBORDEINPUT%> 1px solid;}
INPUT.button {font-family:"Verdana";font-size:12px;font-weight:bold;min-width:80px;overflow:visible;padding-left:5px;padding-right:5px;border-style:outset;}
INPUT.radio {font-family:"Verdana";font-size:12px;font-weight:bold;border-style:none;}
INPUT.checkbox {font-family:"Verdana";font-size:12px;font-weight:bold;border-style:none;}
TEXTAREA {font-family:"Verdana";font-size:11px;BORDER: <%=cBORDEINPUT%> 1px solid;}
<%elseif UCase(application("NOMPORTAL")) = UCase("ILU") then%>
INPUT {font-family:"Arial";font-size:14px;BORDER: <%=cBORDEINPUT%> 1px solid;}
INPUT.button {font-family:"Arial";font-size:14px;font-weight:bold;min-width:80px;overflow:visible;padding-left:5px;padding-right:5px;border-style:outset;}
INPUT.radio {font-family:"Arial";font-size:14px;font-weight:bold;border-style:none;}
INPUT.checkbox {font-family:"Arial";font-size:14px;font-weight:bold;border-style:none;}
TEXTAREA {font-family:"Arial";font-size:11px;BORDER: <%=cBORDEINPUT%> 1px solid;}
<%elseif UCase(application("NOMPORTAL")) = UCase("HRE") then%>
INPUT {font-family:'Comfortaa', Verdana;font-size:12px;BORDER: <%=cBORDEINPUT%> 1px solid;}
INPUT.button {font-family:'Comfortaa', Verdana;font-size:12px;font-weight:bold;min-width:80px;overflow:visible;padding-left:8px;border-style:outset;}
INPUT.radio {font-family:'Comfortaa', Verdana;font-size:12px;font-weight:bold;border-style:none;}
INPUT.checkbox {font-family:'Comfortaa', Verdana;font-size:12px;font-weight:bold;border-style:none;}
TEXTAREA {font-family:'Comfortaa', Verdana;font-size:11px;BORDER: <%=cBORDEINPUT%> 1px solid;}
<%elseif UCase(application("NOMPORTAL")) = UCase("BER") then%>
INPUT {font-family:"Calibri";font-size:12px;BORDER: <%=cBORDEINPUT%> 1px solid;}
INPUT.button {font-family:"Calibri";font-size:12px;font-weight:bold;min-width:80px;overflow:visible;padding-left:8px;border-style:outset;}
INPUT.radio {font-family:"Calibri";font-size:12px;font-weight:bold;border-style:none;}
INPUT.checkbox {font-family:"Calibri";font-size:12px;font-weight:bold;border-style:none;}
TEXTAREA {font-family:"Calibri";font-size:11px;BORDER: <%=cBORDEINPUT%> 1px solid;}
<%elseif UCase(application("NOMPORTAL")) = UCase("SBFE") then%>
INPUT {font-family:Gotham-Book;font-size:12px;BORDER: <%=cBORDEINPUT%> 1px solid;}
INPUT.button {font-family:Gotham-Book;font-size:12px;font-weight:bold;min-width:80px;overflow:visible;padding-left:8px;border-style:outset;}
INPUT.radio {font-family:Gotham-Book;font-size:12px;font-weight:bold;border-style:none;}
INPUT.checkbox {font-family:Gotham-Book;font-size:12px;font-weight:bold;border-style:none;}
TEXTAREA {font-family:Gotham-Book;font-size:11px;BORDER: <%=cBORDEINPUT%> 1px solid;}
<%elseif UCase(application("NOMPORTAL")) = UCase("PES") then%>
INPUT {font-family:Gotham-Book;font-size:12px;BORDER: <%=cBORDEINPUT%> 1px solid;}
INPUT.button {font-family:Gotham-Book;font-size:12px;font-weight:bold;min-width:80px;overflow:visible;padding-left:8px;border-style:outset;}
INPUT.radio {font-family:Gotham-Book;font-size:12px;font-weight:bold;border-style:none;}
INPUT.checkbox {font-family:Gotham-Book;font-size:12px;font-weight:bold;border-style:none;}
TEXTAREA {font-family:Gotham-Book;font-size:11px;BORDER: <%=cBORDEINPUT%> 1px solid;}
<%else%>
INPUT {font-family:"Verdana";font-size:10px;}
INPUT.button {font-size:10px;font-weight:bold;min-width:80px;overflow:visible;padding-left:5px;padding-right:5px;}
INPUT.radio {font-size:10px;font-weight:bold;}
INPUT.checkbox {font-size:10px;font-weight:bold;border-style:none;}
TEXTAREA {font-family:"Verdana";font-size:10px;}
<%end if%>

<%if UCase(application("NOMPORTAL")) = UCase("FED") then%>
P {font-family:"Arial";margin-left:20px;font-size:12px;}
FORM {margin-top:0px;margin-bottom:0px;}
a {font-family:"Arial";}
<%elseif UCase(application("NOMPORTAL")) = UCase("HRE") then%>
P {font-family:'Comfortaa', Verdana;margin-left:20px;font-size:11px;}
FORM {margin-top:0px;margin-bottom:0px;}
a {font-family:'Comfortaa', Verdana;}
<%elseif UCase(application("NOMPORTAL")) = UCase("MSM") then%>
P {font-family:Gotham-Book;margin-left:20px;font-size:12px;}
FORM {margin-top:0px;margin-bottom:0px;}
a {font-family:Gotham-Book;}
<%elseif UCase(application("NOMPORTAL")) = UCase("ELE") then%>
P {font-family:'Gotham-Book', Verdana;margin-left:20px;font-size:11px;}
FORM {margin-top:0px;margin-bottom:0px;}
a {font-family:'Gotham-Book', Verdana;}
<%elseif UCase(application("NOMPORTAL")) = UCase("BER") then%>
P {font-family:"Calibri";margin-left:20px;font-size:11px;}
FORM {margin-top:0px;margin-bottom:0px;}
a {font-family:"Calibri";}
<%elseif UCase(application("NOMPORTAL")) = UCase("ILU") then%>
P {font-family:"Arial";margin-left:20px;font-size:11px;}
FORM {margin-top:0px;margin-bottom:0px;}
a {font-family:"Arial";}
<%elseif UCase(application("NOMPORTAL")) = UCase("IBC") then%>
P {font-family:Gotham-Book;margin-left:20px;font-size:12px;}
FORM {margin-top:0px;margin-bottom:0px;}
a {font-family:Gotham-Book;}
<%elseif UCase(application("NOMPORTAL")) = UCase("SAM") then%>
P {font-family:'Gotham-Book', Verdana;margin-left:20px;font-size:11px;}
FORM {margin-top:0px;margin-bottom:0px;}
a {font-family:'Gotham-Book', Verdana;}
<%elseif UCase(application("NOMPORTAL")) = UCase("SBFE") then%>
P {font-family:'Gotham-Book', Verdana;margin-left:20px;font-size:11px;}
FORM {margin-top:0px;margin-bottom:0px;}
a {font-family:'Gotham-Book', Verdana;}
<%elseif UCase(application("NOMPORTAL")) = UCase("PES") then%>
P {font-family:Gotham-Book;margin-left:20px;font-size:12px;}
FORM {margin-top:0px;margin-bottom:0px;}
a {font-family:Gotham-Book;}
<%elseif UCase(application("NOMPORTAL")) = UCase("RGA") then%>
P {font-family:Gotham-Book;margin-left:20px;font-size:12px;}
FORM {margin-top:0px;margin-bottom:0px;}
a {font-family:Gotham-Book;}
<%elseif UCase(application("NOMPORTAL")) = UCase("CIE") then%>
P {font-family:Gotham-Book;margin-left:20px;font-size:12px;}
FORM {margin-top:0px;margin-bottom:0px;}
a {font-family:Gotham-Book;}
<%elseif UCase(application("NOMPORTAL")) = UCase("JUV") then%>
P {font-family:Gotham-Book;margin-left:20px;font-size:12px;}
FORM {margin-top:0px;margin-bottom:0px;}
a {font-family:Gotham-Book;}
<%elseif UCase(application("NOMPORTAL")) = UCase("GRI") then%>
P {font-family:Gotham-Book;margin-left:20px;font-size:12px;}
FORM {margin-top:0px;margin-bottom:0px;}
a {font-family:Gotham-Book;}
<%elseif UCase(application("NOMPORTAL")) = UCase("DEO") then%>
P {font-family:Gotham-Book;margin-left:20px;font-size:12px;}
FORM {margin-top:0px;margin-bottom:0px;}
a {font-family:Gotham-Book;}
<%else%>
P {font-family:"Verdana";margin-left:20px;font-size:12px;}
FORM {margin-top:0px;margin-bottom:0px;}
a {font-family:"Verdana";}
<%end if%>

<%if UCase(application("NOMPORTAL")) = UCase("BSA") then%>
a.mnuPpal:link {font-family:"Arial";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:active {font-family:"Arial";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:visited {font-family:"Arial";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:hover {font-family:"Arial";font-size:12px;color:<%=cMENUHLINK%>;text-decoration:none}
<%elseif UCase(application("NOMPORTAL")) = UCase("OCA") then%>
a.mnuPpal:link {font-family:"Verdana";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:active {font-family:"Verdana";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:visited {font-family:"Verdana";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:hover {font-family:"Verdana";font-size:12px;color:<%=cMENUHLINK%>;text-decoration:underline}
<%elseif UCase(application("NOMPORTAL")) = UCase("ETE") then%>
a.mnuPpal:link {font-family:"Verdana";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:active {font-family:"Verdana";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:visited {font-family:"Verdana";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:hover {font-family:"Verdana";font-size:12px;color:<%=cMENUHLINK%>;text-decoration:underline}
<%elseif UCase(application("NOMPORTAL")) = UCase("SER") then%>
a.mnuPpal:link {font-family:"Verdana";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:active {font-family:"Verdana";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:visited {font-family:"Verdana";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:hover {font-family:"Verdana";font-size:12px;color:<%=cMENUHLINK%>;text-decoration:underline}
<%elseif UCase(application("NOMPORTAL")) = UCase("HRE") then%>
a.mnuPpal:link {font-family:'Comfortaa', Verdana;font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:active {font-family:'Comfortaa', Verdana;font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:visited {font-family:'Comfortaa', Verdana;font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:hover {font-family:'Comfortaa', Verdana;font-size:12px;color:<%=cMENUHLINK%>;text-decoration:underline}
<%elseif UCase(application("NOMPORTAL")) = UCase("PPL") then%>
a.mnuPpal:link {font-family:"Arial";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:active {font-family:"Arial";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:visited {font-family:"Arial";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:hover {font-family:"Arial";font-size:12px;color:<%=cMENUHLINK%>;text-decoration:underline}
<%elseif UCase(application("NOMPORTAL")) = UCase("ILU") then%>
a.mnuPpal:link {font-family:"Arial";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:active {font-family:"Arial";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:visited {font-family:"Arial";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:hover {font-family:"Arial";font-size:12px;color:<%=cMENUHLINK%>;text-decoration:underline}
<%elseif UCase(application("NOMPORTAL")) = UCase("BER") then%>
a.mnuPpal:link {font-family:"Calibri";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:active {font-family:"Calibri";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:visited {font-family:"Calibri";font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:hover {font-family:"Calibri";font-size:12px;color:<%=cMENUHLINK%>;text-decoration:underline}
<%elseif UCase(application("NOMPORTAL")) = UCase("SBFE") then%>
a.mnuPpal:link {font-family:Gotham-Book;font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:active {font-family:Gotham-Book;font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:visited {font-family:Gotham-Book;font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:hover {font-family:Gotham-Book;font-size:12px;color:<%=cMENUHLINK%>;text-decoration:underline}
<%elseif UCase(application("NOMPORTAL")) = UCase("PES") then%>
a.mnuPpal:link {font-family:Gotham-Book;font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:active {font-family:Gotham-Book;font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:visited {font-family:Gotham-Book;font-size:12px;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:hover {font-family:Gotham-Book;font-size:12px;color:<%=cMENUHLINK%>;text-decoration:underline}
<%else%>
a.mnuPpal:link {font-size:10px;font-weight:bold;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:active {font-size:10px;font-weight:bold;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:visited {font-size:10px;font-weight:bold;color:<%=cMENULINK%>;text-decoration:none}
a.mnuPpal:hover {font-size:10px;font-weight:bold;color:<%=cMENUHLINK%>;text-decoration:none}
<%end if%>

<%if UCase(application("NOMPORTAL")) = UCase("OCA") then%>
a.ayuda:link {color:<%=cHELPLINK%>;font-size:12;font-weight:bold;text-decoration:none;}
a.ayuda:active {color:<%=cHELPLINK%>;font-size:12;font-weight:bold;text-decoration:none}
a.ayuda:visited {color:<%=cHELPLINK%>;font-size:12;font-weight:bold;text-decoration:none}
a.ayuda:hover {color:<%=cHELPLINK%>;font-size:12;font-weight:bold;text-decoration:none}
<%elseif UCase(application("NOMPORTAL")) = UCase("ETE") then%>
a.ayuda:link {color:<%=cHELPLINK%>;font-size:12;font-weight:bold;text-decoration:none;}
a.ayuda:active {color:<%=cHELPLINK%>;font-size:12;font-weight:bold;text-decoration:none}
a.ayuda:visited {color:<%=cHELPLINK%>;font-size:12;font-weight:bold;text-decoration:none}
a.ayuda:hover {color:<%=cHELPLINK%>;font-size:12;font-weight:bold;text-decoration:none}
<%elseif UCase(application("NOMPORTAL")) = UCase("SER") then%>
a.ayuda:link {color:<%=cHELPLINK%>;font-size:12;font-weight:bold;text-decoration:none;}
a.ayuda:active {color:<%=cHELPLINK%>;font-size:12;font-weight:bold;text-decoration:none}
a.ayuda:visited {color:<%=cHELPLINK%>;font-size:12;font-weight:bold;text-decoration:none}
a.ayuda:hover {color:<%=cHELPLINK%>;font-size:12;font-weight:bold;text-decoration:none}
<%else%>
a.ayuda:link {color:<%=cHELPLINK%>;font-size:10;font-weight:bold;text-decoration:none;}
a.ayuda:active {color:<%=cHELPLINK%>;font-size:10;font-weight:bold;text-decoration:none}
a.ayuda:visited {color:<%=cHELPLINK%>;font-size:10;font-weight:bold;text-decoration:none}
a.ayuda:hover {color:<%=cHELPLINK%>;font-size:10;font-weight:bold;text-decoration:none}
<%end if%>

a.popUp:link {color:<%=cPOPUPLINK%>;font-size:10;font-weight:normal;text-decoration:none;}
a.popUp:active {color:<%=cPOPUPLINK%>;font-size:10;font-weight:normal;text-decoration:none}
a.popUp:visited {color:<%=cPOPUPLINK%>;font-size:10;font-weight:normal;text-decoration:none}
a.popUp:hover {color:<%=cPOPUPHLINK%>;font-size:10;font-weight:normal;text-decoration:none}

a.negrita:link {font-weight:bold;}
a.negrita:active {font-weight:bold;}
a.negrita:visited {font-weight:bold;}
a.negrita:hover {font-weight:bold;}

a.superada:link {color:<%=cTEXTOPUJASUPERADA%>;font-weight:bold;}
a.superada:active {color:<%=cTEXTOPUJASUPERADA%>;font-weight:bold;}
a.superada:visited {color:<%=cTEXTOPUJASUPERADA%>;font-weight:bold;}
a.superada:hover {color:<%=cTEXTOPUJASUPERADA%>;font-weight:bold;}

<%if UCase(application("NOMPORTAL")) = UCase("FIC") then%>

a:link {text-decoration:none;color:#666666}
a:visited {text-decoration:none;color:#666666}
a:active {text-decoration:none;color:#000000}
a:hover {  color: #000000; text-decoration: none}
<%elseif UCase(application("NOMPORTAL")) = UCase("OCA") then%>
a:link {font-size:12px;text-decoration:none;}
a:visited {font-size:12px;text-decoration:none;}
a:active {font-size:12px;text-decoration:none;}
a:hover {font-size:12px; text-decoration: underline}
<%elseif UCase(application("NOMPORTAL")) = UCase("ETE") then%>
a:link {font-size:12px;text-decoration:none;}
a:visited {font-size:12px;text-decoration:none;}
a:active {font-size:12px;text-decoration:none;}
a:hover {font-size:12px; text-decoration: underline}
<%elseif UCase(application("NOMPORTAL")) = UCase("SER") then%>
a:link {font-size:12px;text-decoration:none;}
a:visited {font-size:12px;text-decoration:none;}
a:active {font-size:12px;text-decoration:none;}
a:hover {font-size:12px; text-decoration: underline}
<%elseif UCase(application("NOMPORTAL")) = UCase("ILU") then%>
a:link {font-size:12px;text-decoration:none;}
a:visited {font-size:12px;text-decoration:none;}
a:active {font-size:12px;text-decoration:none;}
a:hover {font-size:12px; text-decoration: underline}
<%elseif UCase(application("NOMPORTAL")) = UCase("HRE") then%>
a:link {font-size:12px;text-decoration:none;color:#000000}
a:visited {font-size:12px;text-decoration:none;color:#FFFFFF}
a:active {font-size:12px;text-decoration:none;color:#FFFFFF}
a:hover { font-size:12px;color:#000000; text-decoration: none}
<%elseif UCase(application("NOMPORTAL")) = UCase("MSM") then%>
a:link {font-family:Gotham-Book !important;font-size:12px;text-decoration:none;color:#df0006}
<%elseif UCase(application("NOMPORTAL")) = UCase("IBC") then%>
a:link {font-family:Gotham-Book !important;font-size:12px;text-decoration:none;color:#0062ae}
<%else%>

a:link {color:<%=cLINK%>;font-size:10;font-weight:normal;text-decoration:none}
a:active {color:<%=cLINK%>;font-size:10;font-weight:normal;text-decoration:none}
a:visited {color:<%=cLINK%>;font-size:10;font-weight:normal;text-decoration:none}
a:hover {color:<%=cHLINK%>;font-size:10;font-weight:normal;text-decoration:none}
<%end if%>

TD.mnuPpal {font-weight:bold;font-size:10;}
TD.fuenteTxiki {font-size:2px;}
#divLogo {position:absolute;top:15;left:0;width:100%;height:auto;z-index:0;}
<%if UCase(application("NOMPORTAL")) = UCase("FED") then%>
H1 {font-family:"Arial";font-size:23px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:"Arial";font-size:14px;font-weight:bold;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:"Arial";font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:"Arial";font-size:10px;font-weight:bold;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:"Arial";font-size:10px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("FIC") then%>
H1 {font-family:"Arial";font-size:18px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:"Arial";font-size:13px;font-weight:bold;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:"Arial";font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:"Arial";font-size:10px;font-weight:bold;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:"Arial";font-size:10px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("huf") then%>
H1 {font-family:"Arial";font-size:14px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:"Arial";font-size:13px;font-weight:bold;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:"Arial";font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:"Arial";font-size:10px;font-weight:bold;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:"Arial";font-size:10px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("BSA") then%>
H1 {font-family:"Arial";font-size:14px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:"Arial";font-size:13px;font-weight:bold;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:"Arial";font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:"Arial";font-size:10px;font-weight:bold;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:"Arial";font-size:10px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("cfg") then%>
H1 {font-family:"Arial";font-size:18px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:"Arial";font-size:13px;font-weight:bold;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:"Arial";font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:"Arial";font-size:10px;font-weight:normal;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:"Arial";font-size:10px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("BAN") then%>
H1 {font-family:"Verdana";font-size:18px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:"Verdana";font-size:13px;font-weight:bold;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:"Verdana";font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:"Verdana";font-size:11px;font-weight:normal;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:"Verdana";font-size:11px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("OCA") then%>
H1 {font-family:"Verdana";font-size:18px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:"Verdana";font-size:16px;font-weight:normal;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:"Verdana";font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:"Verdana";font-size:11px;font-weight:normal;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:"Verdana";font-size:11px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("ETE") then%>
H1 {font-family:"Verdana";font-size:18px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:"Verdana";font-size:16px;font-weight:normal;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:"Verdana";font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:"Verdana";font-size:11px;font-weight:normal;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:"Verdana";font-size:11px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("SER") then%>
H1 {font-family:"Verdana";font-size:18px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:"Verdana";font-size:16px;font-weight:normal;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:"Verdana";font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:"Verdana";font-size:11px;font-weight:normal;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:"Verdana";font-size:11px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("HRE") then%>
H1 {font-family:'Comfortaa', Verdana;font-size:18px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:'Comfortaa', Verdana;font-size:16px;font-weight:normal;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:'Comfortaa', Verdana;font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:'Comfortaa', Verdana;font-size:11px;font-weight:normal;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:'Comfortaa', Verdana;font-size:11px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("PPL") then%>
H1 {font-family:"Arial";font-size:18px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:"Arial";font-size:13px;font-weight:bold;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:"Arial";font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:"Arial";font-size:10px;font-weight:normal;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:"Arial";font-size:10px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("MSM") then%>
H1 {font-family:"Gotham-Book";font-size:18px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:"Gotham-Book";font-size:13px;font-weight:bold;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:"Gotham-Book";font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:"Gotham-Book";font-size:10px;font-weight:normal;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:"Gotham-Book";font-size:10px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("IBC") then%>
H1 {font-family:"Gotham-Book";font-size:18px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:"Gotham-Book";font-size:13px;font-weight:bold;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:"Gotham-Book";font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:"Gotham-Book";font-size:10px;font-weight:normal;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:"Gotham-Book";font-size:10px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("ILU") then%>
H1 {font-family:"Arial";font-size:16px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:"Arial";font-size:14px;font-weight:bold;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:"Arial";font-size:13px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:"Arial";font-size:12px;font-weight:bold;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:"Arial";font-size:12px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("BER") then%>
H1 {font-family:"Calibri";font-size:18px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:"Calibri";font-size:13px;font-weight:bold;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:"Calibri";font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:"Calibri";font-size:10px;font-weight:normal;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:"Calibri";font-size:10px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("SBFE") then%>
H1 {font-family:Gotham-Book;font-size:18px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:Gotham-Book;font-size:13px;font-weight:bold;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:Gotham-Book;font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:Gotham-Book;font-size:10px;font-weight:normal;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:Gotham-Book;font-size:10px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%elseif UCase(application("NOMPORTAL")) = UCase("PES") then%>
H1 {font-family:Gotham-Book;font-size:18px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:Gotham-Book;font-size:13px;font-weight:bold;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:Gotham-Book;font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:Gotham-Book;font-size:10px;font-weight:normal;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:Gotham-Book;font-size:10px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%else%>
H1 {font-family:"Verdana";font-size:18px;font-weight:bold;color:<%=cH1%>;margin-left:20px;margin-top:10px;margin-botton:7px;}
H2 {font-family:"Verdana";font-size:13px;font-weight:bold;color:<%=cH2%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H3 {font-family:"Verdana";font-size:12px;font-weight:normal;color:<%=cH3%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H4 {font-family:"Verdana";font-size:10px;font-weight:bold;color:<%=cH4%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
H5 {font-family:"Verdana";font-size:10px;font-weight:normal;color:<%=cH5%>;margin-left:20px;margin-top:6px;margin-botton:7px;}
<%end if%>

TABLE.cabeceraNOOFE {background-color:<%=cFONDOCABNOOFERTAR%>;color:<%=cTEXTONOOFERTAR%>;}
TABLE.fondoNOOFE {background-color:<%=cFONDONOOFERTAR%>}

<%if UCase(application("NOMPORTAL")) = UCase("FED") then%>
TH.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Arial";font-size:10px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Arial";font-size:10px;font-weight:bold;color:<%=cTextoCabecera%>;}
.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Arial";font-size:10px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.filaPar {padding-left:3;background-color:<%=cFilaPar%>;font-family:"Arial";font-size:10px;color:<%=cTextoFilaPar%>;}
TD.filaImpar {padding-left:3;background-color:<%=cFilaImpar%>;font-family:"Arial";font-size:10px;color:<%=cTextoFilaImPar%>;}
TD.subrallado {padding-left: 3px; font-weight:bold;color:<%=cSubtitulo%>;border-bottom:<%=cSubtitulo%> 1px solid;}
<%elseif UCase(application("NOMPORTAL")) = UCase("cfg") then%>
TH.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Arial";font-size:11px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Arial";font-size:11px;font-weight:bold;color:<%=cTextoCabecera%>;}
.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Arial";font-size:11px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.filaPar {padding-left:3;background-color:<%=cFilaPar%>;font-family:"Arial";font-size:11px;color:<%=cTextoFilaPar%>;}
TD.filaImpar {padding-left:3;background-color:<%=cFilaImpar%>;font-family:"Arial";font-size:11px;color:<%=cTextoFilaImPar%>;}
TD.subrallado {padding-left: 3px; font-weight:bold;color:<%=cSubtitulo%>;border-bottom:<%=cSubtitulo%> 1px solid;}
TD.sombreado {background-color:<%=cSOMBRACAJETIN%>;font-weight:bold;color:<%=cTextoCabecera%>;}
<%elseif UCase(application("NOMPORTAL")) = UCase("FIC") or UCase(application("NOMPORTAL")) = UCase("huf") then%>
TH.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Verdana";font-size:10px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Verdana";font-size:10px;font-weight:bold;color:<%=cTextoCabecera%>;}
.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Verdana";font-size:10px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.filaPar {padding-left:3;background-color:<%=cFilaPar%>;font-family:"Verdana";font-size:10px;color:<%=cTextoFilaPar%>;}
TD.filaImpar {padding-left:3;background-color:<%=cFilaImpar%>;font-family:"Verdana";font-size:10px;color:<%=cTextoFilaImPar%>;}
TD.subrallado {padding-left: 3px; font-weight:bold;color:<%=cSubtitulo%>;border-bottom:<%=cSubtitulo%> 1px solid;}
<%elseif UCase(application("NOMPORTAL")) = UCase("BAN") then%>
TH.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Verdana";font-size:12px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Verdana";font-size:12px;font-weight:bold;color:<%=cTextoCabecera%>;}
.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Verdana";font-size:12px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.filaPar {padding-left:3;background-color:<%=cFilaPar%>;font-family:"Verdana";font-size:12px;color:<%=cTextoFilaPar%>;}
TD.filaImpar {padding-left:3;background-color:<%=cFilaImpar%>;font-family:"Verdana";font-size:12px;color:<%=cTextoFilaImPar%>;}
TD.subrallado {padding-left: 3px; font-weight:bold;color:<%=cSubtitulo%>;border-bottom:<%=cSubtitulo%> 1px solid;}
TD.sombreado {background-color:<%=cSOMBRACAJETIN%>;font-weight:bold;color:<%=cTextoCabecera%>;}
<%elseif UCase(application("NOMPORTAL")) = UCase("OCA") then%>
TH.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Verdana";font-size:12px;font-weight:normal;color:<%=cTextoCabecera%>;}
TD.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Verdana";font-size:12px;font-weight:normal;color:<%=cTextoCabecera%>;text-align:left;}
.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Verdana";font-size:12px;font-weight:normal;color:<%=cTextoCabecera%>;text-align:left;}
TD.filaPar {padding-left:3;background-color:<%=cFilaPar%>;font-family:"Verdana";font-size:12px;color:<%=cTextoFilaPar%>;}
TD.filaImpar {padding-left:3;background-color:<%=cFilaImpar%>;font-family:"Verdana";font-size:12px;color:<%=cTextoFilaImPar%>;}
TD.subrallado {padding-left: 3px; font-weight:bold;color:<%=cSubtitulo%>;border-bottom:<%=cSubtitulo%> 1px solid;}
TD.sombreado {background-color:<%=cSOMBRACAJETIN%>;font-weight:bold;color:<%=cTextoCabecera%>;}
<%elseif UCase(application("NOMPORTAL")) = UCase("ETE") then%>
TH.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Verdana";font-size:12px;font-weight:normal;color:<%=cTextoCabecera%>;}
TD.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Verdana";font-size:12px;font-weight:normal;color:<%=cTextoCabecera%>;text-align:left;}
.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Verdana";font-size:12px;font-weight:normal;color:<%=cTextoCabecera%>;text-align:left;}
TD.filaPar {padding-left:3;background-color:<%=cFilaPar%>;font-family:"Verdana";font-size:12px;color:<%=cTextoFilaPar%>;}
TD.filaImpar {padding-left:3;background-color:<%=cFilaImpar%>;font-family:"Verdana";font-size:12px;color:<%=cTextoFilaImPar%>;}
TD.subrallado {padding-left: 3px; font-weight:bold;color:<%=cSubtitulo%>;border-bottom:<%=cSubtitulo%> 1px solid;}
TD.sombreado {background-color:<%=cSOMBRACAJETIN%>;font-weight:bold;color:<%=cTextoCabecera%>;}
<%elseif UCase(application("NOMPORTAL")) = UCase("SER") then%>
TH.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Verdana";font-size:12px;font-weight:normal;color:<%=cTextoCabecera%>;}
TD.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Verdana";font-size:12px;font-weight:normal;color:<%=cTextoCabecera%>;text-align:left;}
.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Verdana";font-size:12px;font-weight:normal;color:<%=cTextoCabecera%>;text-align:left;}
TD.filaPar {padding-left:3;background-color:<%=cFilaPar%>;font-family:"Verdana";font-size:12px;color:<%=cTextoFilaPar%>;}
TD.filaImpar {padding-left:3;background-color:<%=cFilaImpar%>;font-family:"Verdana";font-size:12px;color:<%=cTextoFilaImPar%>;}
TD.subrallado {padding-left: 3px; font-weight:bold;color:<%=cSubtitulo%>;border-bottom:<%=cSubtitulo%> 1px solid;}
TD.sombreado {background-color:<%=cSOMBRACAJETIN%>;font-weight:bold;color:<%=cTextoCabecera%>;}
<%elseif UCase(application("NOMPORTAL")) = UCase("HRE") then%>
TH.cabecera {padding-left:3px;background-color:<%=cCabeceraTablas%>;font-family:'Comfortaa', Verdana;font-size:12px;font-weight:normal;color:<%=cTextoCabecera%>;}
TD.cabecera {padding:3px;background-color:<%=cCabeceraTablas%>;font-family:'Comfortaa', Verdana;font-size:12px;font-weight:normal;color:<%=cTextoCabecera%>;text-align:left;}
.cabecera {padding-left:3px;background-color:<%=cCabeceraTablas%>;font-family:'Comfortaa', Verdana;font-size:12px;font-weight:normal;color:<%=cTextoCabecera%>;text-align:left;}
TD.filaPar {padding:3px;background-color:<%=cFilaPar%>;font-family:'Comfortaa', Verdana;font-size:12px;color:<%=cTextoFilaPar%>;}
TD.filaImpar {padding:3px;background-color:<%=cFilaImpar%>;font-family:'Comfortaa', Verdana;font-size:12px;color:<%=cTextoFilaImPar%>;}
TD.subrallado {padding-left: 3px; font-weight:bold;color:<%=cSubtitulo%>;border-bottom:<%=cSubtitulo%> 1px solid;}
TD.sombreado {background-color:<%=cSOMBRACAJETIN%>;font-weight:bold;color:<%=cTextoCabecera%>;}
<%elseif UCase(application("NOMPORTAL")) = UCase("PPL") then%>
TH.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Arial";font-size:11px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Arial";font-size:11px;font-weight:bold;color:<%=cTextoCabecera%>;}
.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Arial";font-size:11px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.filaPar {padding-left:3;background-color:<%=cFilaPar%>;font-family:"Arial";font-size:11px;color:<%=cTextoFilaPar%>;}
TD.filaImpar {padding-left:3;background-color:<%=cFilaImpar%>;font-family:"Arial";font-size:11px;color:<%=cTextoFilaImPar%>;}
TD.subrallado {padding-left: 3px; font-weight:bold;color:<%=cSubtitulo%>;border-bottom:<%=cSubtitulo%> 1px solid;}
TD.sombreado {background-color:<%=cSOMBRACAJETIN%>;font-weight:bold;color:<%=cTextoCabecera%>;}
<%elseif UCase(application("NOMPORTAL")) = UCase("MSM") then%>
TH.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:Gotham-Book;font-size:12px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:Gotham-Book;font-size:12px;font-weight:bold;color:<%=cTextoCabecera%>;}
.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:Gotham-Book;font-size:12px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.filaPar {padding-left:3;background-color:<%=cFilaPar%>;font-family:Gotham-Book;font-size:11px;color:<%=cTextoFilaPar%>;}
TD.filaImpar {padding-left:3;background-color:<%=cFilaImpar%>;font-family:Gotham-Book;font-size:11px;color:<%=cTextoFilaImPar%>;}
TD.subrallado {padding-left: 3px; font-weight:bold;color:<%=cSubtitulo%>;border-bottom:<%=cSubtitulo%> 1px solid;}
TD.sombreado {background-color:<%=cSOMBRACAJETIN%>;font-weight:bold;color:<%=cTextoCabecera%>;}
<%elseif UCase(application("NOMPORTAL")) = UCase("IBC") then%>
TH.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:Gotham-Book;font-size:12px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:Gotham-Book;font-size:12px;font-weight:bold;color:<%=cTextoCabecera%>;}
.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:Gotham-Book;font-size:12px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.filaPar {padding-left:3;background-color:<%=cFilaPar%>;font-family:Gotham-Book;font-size:11px;color:<%=cTextoFilaPar%>;}
TD.filaImpar {padding-left:3;background-color:<%=cFilaImpar%>;font-family:Gotham-Book;font-size:11px;color:<%=cTextoFilaImPar%>;}
TD.subrallado {padding-left: 3px; font-weight:bold;color:<%=cSubtitulo%>;border-bottom:<%=cSubtitulo%> 1px solid;}
TD.sombreado {background-color:<%=cSOMBRACAJETIN%>;font-weight:bold;color:<%=cTextoCabecera%>;}
<%elseif UCase(application("NOMPORTAL")) = UCase("ILU") then%>
TH.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Arial";font-size:14px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Arial";font-size:14px;font-weight:bold;color:<%=cTextoCabecera%>;}
.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Arial";font-size:14px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.filaPar {padding-left:3;background-color:<%=cFilaPar%>;font-family:"Arial";font-size:14px;color:<%=cTextoFilaPar%>;}
TD.filaImpar {padding-left:3;background-color:<%=cFilaImpar%>;font-family:"Arial";font-size:14px;color:<%=cTextoFilaImPar%>;}
TD.subrallado {padding-left: 3px; font-weight:bold;color:<%=cSubtitulo%>;border-bottom:<%=cSubtitulo%> 1px solid;}
TD.sombreado {background-color:<%=cSOMBRACAJETIN%>;font-weight:bold;color:<%=cTextoCabecera%>;}
<%elseif UCase(application("NOMPORTAL")) = UCase("BER") then%>
TH.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Calibri";font-size:11px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Calibri";font-size:11px;font-weight:bold;color:<%=cTextoCabecera%>;}
.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:"Calibri";font-size:11px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.filaPar {padding-left:3;background-color:<%=cFilaPar%>;font-family:"Calibri";font-size:11px;color:<%=cTextoFilaPar%>;}
TD.filaImpar {padding-left:3;background-color:<%=cFilaImpar%>;font-family:"Calibri";font-size:11px;color:<%=cTextoFilaImPar%>;}
TD.subrallado {padding-left: 3px; font-weight:bold;color:<%=cSubtitulo%>;border-bottom:<%=cSubtitulo%> 1px solid;}
TD.sombreado {background-color:<%=cSOMBRACAJETIN%>;font-weight:bold;color:<%=cTextoCabecera%>;}
<%elseif UCase(application("NOMPORTAL")) = UCase("SBFE") then%>
TH.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:Gotham-Book;font-size:12px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:Gotham-Book;font-size:12px;font-weight:bold;color:<%=cTextoCabecera%>;}
.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:Gotham-Book;font-size:12px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.filaPar {padding-left:3;background-color:<%=cFilaPar%>;font-family:Gotham-Book;font-size:11px;color:<%=cTextoFilaPar%>;}
TD.filaImpar {padding-left:3;background-color:<%=cFilaImpar%>;font-family:Gotham-Book;font-size:11px;color:<%=cTextoFilaImPar%>;}
TD.subrallado {padding-left: 3px; font-weight:bold;color:<%=cSubtitulo%>;border-bottom:<%=cSubtitulo%> 1px solid;}
TD.sombreado {background-color:<%=cSOMBRACAJETIN%>;font-weight:bold;color:<%=cTextoCabecera%>;}
<%elseif UCase(application("NOMPORTAL")) = UCase("PES") then%>
TH.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:Gotham-Book;font-size:12px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:Gotham-Book;font-size:12px;font-weight:bold;color:<%=cTextoCabecera%>;}
.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-family:Gotham-Book;font-size:12px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.filaPar {padding-left:3;background-color:<%=cFilaPar%>;font-family:Gotham-Book;font-size:11px;color:<%=cTextoFilaPar%>;}
TD.filaImpar {padding-left:3;background-color:<%=cFilaImpar%>;font-family:Gotham-Book;font-size:11px;color:<%=cTextoFilaImPar%>;}
TD.subrallado {padding-left: 3px; font-weight:bold;color:<%=cSubtitulo%>;border-bottom:<%=cSubtitulo%> 1px solid;}
TD.sombreado {background-color:<%=cSOMBRACAJETIN%>;font-weight:bold;color:<%=cTextoCabecera%>;}
<%else%>
TH.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-size:10px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-size:10px;font-weight:bold;color:<%=cTextoCabecera%>;}
.cabecera {padding-left:3;background-color:<%=cCabeceraTablas%>;font-size:10px;font-weight:bold;color:<%=cTextoCabecera%>;}
TD.filaPar {padding-left:3;background-color:<%=cFilaPar%>;font-size:10px;color:<%=cTextoFilaPar%>;}
TD.filaImpar {padding-left:3;background-color:<%=cFilaImpar%>;font-size:10px;color:<%=cTextoFilaImPar%>;}
TD.subrallado {padding-left: 3px; font-weight:bold;color:<%=cSubtitulo%>;border-bottom:<%=cSubtitulo%> 1px solid;}

<%end if%>
.subrallado {border-bottom:<%=cSubtitulo%> 1px solid;}
.sobrerallado {border-top:<%=cSubtitulo%> 1px solid;}

TD.linea1 {border-bottom:<%=cColorLinea1Down%> 1px solid;font-size:1px}
TD.linea2 {border-bottom:<%=cColorLinea2Down%> 1px solid;font-size:1px}
.error {font-size:12px;color:<%=cError%>;font-weight:bold;}


TABLE.principal {margin-left:20px;margin-top:6px;margin-botton:7px;}
.principal {font-size:12px;margin-left:20px;margin-top:6px;margin-botton:7px;}

TABLE.filtro {border:1;}


<%if UCase(application("NOMPORTAL")) = UCase("HRE") then%>
.negrita {font-weight:normal;}
<%else%>
.negrita {font-weight:bold;}
<%end if%>

.centro {text-align:center;align:center;}
.izquierda {text-align:left;}
.derecha {text-align:right;}

.numero {text-align:right;}

.fpeque {font-size:10px;}
.fmedia {font-size:12px;line-height:25px}
.fgrande {font-size:14px;}
.fmgrande {font-size:16px;}

.negro {color:black;}

TD.sombreado {background-color:<%=cSOMBRACAJETIN%>;font-weight:bold;}
TABLE.cajetin {width:160px;border:<%=cBORDECAJETIN%> 1px solid;}


TD.superada {padding-left:3;background-color:<%=cFONDOPUJASUPERADA%>;color:<%=cTEXTOPUJASUPERADA%>;}
TD.adjudicada {padding-left:3;background-color:<%=cFONDOADJUDICADA%>;color:<%=cTEXTOADJUDICADA%>;font-weight:bold;}
TD.noadjudicada {padding-left:3;background-color:<%=cFONDONOADJUDICADA%>;color:<%=cTEXTONOADJUDICADA%>;font-weight:bold;}

.muesca
{
	BACKGROUND-POSITION: right top;
	BACKGROUND-IMAGE: url(<%=Application("RUTASEGURA")%>script/common/images/muesca.gif);
	BACKGROUND-REPEAT: no-repeat
}

.obligatorio
{
	BACKGROUND-POSITION: left top;
	BACKGROUND-IMAGE: url(<%=Application("RUTASEGURA")%>script/common/images/atributoobligatorio.gif);
	BACKGROUND-REPEAT: repeat-y
}

.tablasubrayadaright {BORDER-BOTTOM: black 1px solid; BORDER-RIGHT: black 1px solid;}
.tablasubrayadaleft {BORDER-BOTTOM: black 1px solid;}

<%
'SOLO PARA LAS OFERTAS
%>


TD.subAbierta {background-color:<%=cCELSUBASTAABIERTA%>;font-weight:bold;}
TD.subCerrada {background-color:<%=cCELSUBASTACERRADA%>;font-weight:bold;}

a.lnkMOArbol:link {font-size:10;font-weight:bold;color:<%=cLNKARBOL%>;text-decoration:none}
a.lnkMOArbol:active {font-size:10;font-weight:bold;color:<%=cLNKARBOL%>;text-decoration:none}
a.lnkMOArbol:visited {font-size:10;font-weight:bold;color:<%=cLNKARBOL%>;text-decoration:none}
a.lnkMOArbol:hover {font-size:10;font-weight:bold;color:<%=cLNKARBOL%>;text-decoration:none}


#divArbolOut {position:absolute;top:90px;left:5px;width:250px;border:<%=cBORDEBLOQUE%> 1px solid;padding-top:5px;visibility:hidden;}

#divBotones { position:relative;left:5px;width:250px;border:<%=cBORDEBLOQUE%> 1px solid;visibility:hidden;overflow:hidden;}

#divOcultarArbol {position:relative;top:65px; left:5px;width:250px;border:<%=cBORDEBLOQUE%> 1px solid;padding-top:0px;visibility:hidden;}
#divMostrarArbol {position:absolute; top:60px;left:5px;width:250px;border:<%=cBORDEBLOQUE%> 1px solid;padding-top:0px;visibility:hidden;}

#divArbol {position:absolute;top:90px;left:5px;width:250px;border:<%=cBORDEBLOQUE%> 1px solid;Overflow: scroll;padding-top:5px;visibility:hidden;}

#divApartado {position:absolute;top:89px;left:1px;Height:22px;Width:100px;Overflow:none;visibility:hidden;float:right;z-index:300;BORDER:<%=cBORDEBLOQUE%> 1px solid;}
#divPuja {position:absolute;top:89px;left:1px;Height:22px;Width:100px;Overflow:none;visibility:hidden;float:right;z-index:300;BORDER:<%=cBORDEBLOQUE%> 1px solid;}


.cabApartado {font-size:14px;font-weight:bold;color:<%=cFNTAPARTOFE%>;background-color:<%=cAPARTOFE%>;text-align:center;}



<%
'EL BOTÓN DE SALIR
%>
INPUT.botonLogOut {font-family:"verdana";font-size:10px;font-weight:bold;width:60px;overflow:visible;padding-left:5px;padding-right:5px;border-style:outset;background-color:<%=cFONDOLOGOUT%>;color:<%=cTEXTOLOGOUT%>;border-color:<%=cBORDELOGOUT%>}


<%
'Estilo del texto que informa del estado de la oferta

%>

.OFESINENVIAR {font-size:14px;font-weight:bold;color:<%=cFNTOFESINENVIAR%>;text-align:left;}
.OFESINGUARDAR {font-size:14px;font-weight:bold;color:<%=cFNTOFESINGUARDAR%>;text-align:left;}
.OFEENVIADA {font-size:14px;font-weight:bold;color:<%=cFNTENVIADA%>;text-align:left;}

<%
	'Estilo de la barra que separa el resumen del pedido de la politica de aceptación de pedidos
%>
HR.P_acep_pedidos {height:3px;COLOR: <%=cCabeceraTablas%>;}



<%if UCase(application("NOMPORTAL")) = UCase("HRE") then%>
.titulop
{
	FONT-WEIGHT: bold;
	FONT-SIZE: 16px;
	COLOR: <%=cTEXTOWHITE%>;
	FONT-STYLE: normal;
	FONT-FAMILY: comfortaaregular;
	BACKGROUND-COLOR: <%=cFONDOGRAY%>;
	TEXT-DECORATION: none
}
<%elseif UCase(application("NOMPORTAL")) = UCase("SAM") then%>
.titulop
{
	FONT-WEIGHT: bold;
	FONT-SIZE: 16px;
	COLOR: <%=cTEXTOWHITE%>;
	FONT-STYLE: normal;
	FONT-FAMILY: Gotham-Book;
	BACKGROUND-COLOR: <%=cFONDOGRAY%>;
	TEXT-DECORATION: none
}
<%else%>
.titulop
{
	FONT-WEIGHT: bold;
	FONT-SIZE: 22px;
	COLOR: <%=cTEXTOWHITE%>;
	FONT-STYLE: normal;
	FONT-FAMILY: Verdana;
	BACKGROUND-COLOR: <%=cFONDOGRAY%>;
	TEXT-DECORATION: none
}
<%end if%>
.subtitulo1p
{
	FONT-WEIGHT: bold;
	FONT-SIZE: 10px;
	COLOR: <%=cTEXTOBLACK%>;
	FONT-STYLE: normal;
	FONT-FAMILY: Verdana;
	BACKGROUND-COLOR: <%=cFONDOWHITE%>;
	TEXT-DECORATION: none
}
.subtitulo2p
{
	FONT-WEIGHT: normal;
	FONT-SIZE: 10px;
	COLOR: <%=cTEXTOBLACK%>;
	FONT-STYLE: normal;
	FONT-FAMILY: Verdana;
	BACKGROUND-COLOR: <%=cFONDOWHITE%>;
	TEXT-DECORATION: none
}
.lineap
{
	FONT-SIZE: 9px;
	COLOR: <%=cTEXTOBLUE%>;
	FONT-STYLE: normal;
	FONT-FAMILY: Verdana;
	BACKGROUND-COLOR: <%=cFONDOSILVER%>;
	TEXT-DECORATION: none
}
.lineacheck
{
	FONT-WEIGHT: bold;
	FONT-SIZE: 9px;
	COLOR: <%=cTEXTOBLUE%>;
	FONT-STYLE: normal;
	FONT-FAMILY: Verdana;
	BACKGROUND-COLOR: <%=cFONDOSILVER%>;
	TEXT-DECORATION: none
}
.lineaverde
{
	FONT-WEIGHT: normal;
	FONT-SIZE: 9px;
	COLOR: <%=cFONDOBLACK%>;
	FONT-STYLE: normal;
	FONT-FAMILY: Verdana;
	BACKGROUND-COLOR: <%=cFONDOGRAY%>;
	TEXT-DECORATION: none
}



<%
'Estilo que resalta la posición en la subasta en la modalidad del proceso
%>

TD.POSICIONPUJAITEM {padding-left:3;background-color:#E36200;font-size:10px;font-weight:bold;color:<%=cTextoCabecera%>;}    
.POSICIONPUJAPROCESO {border:solid 2pt #E36200;}
.POSICIONPUJAGRUPO {border:solid 2pt #E36200;}



