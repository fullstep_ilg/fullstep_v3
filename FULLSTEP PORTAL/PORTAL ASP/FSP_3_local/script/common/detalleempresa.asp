﻿<%@  language="VBScript" %>
<!--#include file="acceso.asp"-->
<!--#include file="idioma.asp"-->
<!--#include file="formatos.asp"-->
<!--#include file="fsal_1.asp"-->
<html>
<head>
    <title>
        <%=title%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
    <script src="formatos.js"></script>
    <script language="JavaScript" src="ajax.js"></script>
    <%
''' <summary>
''' Mostrar el detalle de una empresa
''' </summary>
''' <remarks>Llamada desde: pedidos11.asp	pedidos11dir.asp	seguimclt.asp; Tiempo máximo: 0</remarks>

	Idioma = Request.Cookies("USU_IDIOMA")
	Emp = request("Emp")
	
	set oRaiz=validarUsuario(Idioma,true,false,0)
    

	CiaComp = oRaiz.Sesion.CiaComp

	Den=devolverTextos(idioma,114)
	
	
	Set oEmpresas = oRaiz.Generar_CEmpresas()

	oEmpresas.CargarTodasLasEmpresasDesde CiaComp ,Idioma, Emp, Application("PYME")

   	If Not oEmpresas.Item(cstr(Emp)) Is Nothing Then
		set oEmpresa = oEmpresas.Item(cstr(Emp))
    End If
    set oEmpresas=nothing
    
    %>
    <script>
        function Cerrar() {
            //Cierra la ventana
            window.close()
        }

    </script>
    <script language="JavaScript" type="text/JavaScript">
        function init() {
            if (<%=Application("FSAL")%> == 1){
                Ajax_FSALActualizar3();
            }
        }
    </script>
    <!--#include file="fsal_3.asp"-->
</head>
<body topmargin="15" leftmargin="0" onload="init()">
    <table class="normal" align="center" width="98%" border="0" cellspacing="1" cellpadding="1">
        <tr>
            <td width="5%" class="cabecera">
                <%=den(1)%>
            </td>
            <td class="filaPar">
                <%=oEmpresa.NIF%>
            </td>
        </tr>
        <tr>
            <td width="35%" class="cabecera">
                <%=den(2)%>
            </td>
            <td class="filaPar">
                <%=HTMLEncode(oEmpresa.Den)%>
            </td>
        </tr>
        <tr>
            <td width="35%" class="cabecera">
                <%=den(3)%>
            </td>
            <td class="filaPar">
                <%=HTMLEncode(oEmpresa.Direccion)%>
            </td>
        </tr>
        <tr>
            <td width="35%" class="cabecera">
                <%=den(4)%>
            </td>
            <td class="filaPar">
                <%=HTMLEncode(oEmpresa.CP)%>
            </td>
        </tr>
        <tr>
            <td width="35%" class="cabecera">
                <%=den(5)%>
            </td>
            <td class="filaPar">
                <%=HTMLEncode(oEmpresa.Poblacion)%>
            </td>
        </tr>
        <tr>
            <td width="35%" class="cabecera">
                <%=den(6)%>
            </td>
            <td class="filaPar">
                <%=HTMLEncode(oEmpresa.DenProvi)%>
            </td>
        </tr>
        <tr>
            <td width="35%" class="cabecera">
                <%=den(7)%>
            </td>
            <td class="filaPar">
                <%=HTMLEncode(oEmpresa.DenPais)%>
            </td>
        </tr>
        <tr>
            <td colspan="2" height="40px" align="center">
                <input type="button" class="button" id="btnCerrar" value="<%=Den(9)%>" onclick="Cerrar()">
            </td>
        </tr>
    </table>
</body>
<!--#include file="fsal_2.asp"-->
</html>
<%
	set oEmpresa=nothing
	set oRaiz=nothing
%>