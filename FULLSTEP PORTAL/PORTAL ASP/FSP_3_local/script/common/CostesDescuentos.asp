﻿<%@ Language=VBScript %>
<!--#include file="acceso.asp"-->
<!--#include file="idioma.asp"-->
<!--#include file="formatos.asp"-->

<HTML>
<HEAD>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<style>
    html,body{margin:0; width:100%; height:100%}
</style>
<%
''' <summary>
''' Mostrar la descripción de una unidad
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos41.asp       
    Ambito = Request("Ambito")
	Idioma = Request("Idioma")
    Id= Request("Id")    
    Moneda= Request("Moneda") 
    Cambio= replace(Request("Cambio"),".",",")
    ImporteBruto= replace(Request("ImporteBruto"),".",",")
    ImporteCostes= replace(Request("ImporteCostes"),".",",")
    ImporteDescuentos= replace(Request("ImporteDescuentos"),".",",")
    ImporteNeto= replace(Request("ImporteNeto"),".",",")

    decimalfmt=request.Cookies ("USU_DECIMALFMT")
	thousandfmt=Request.Cookies ("USU_THOUSANFMT")
	precisionfmt=cint(Request.Cookies ("USU_PRECISIONFMT"))
	datefmt=Request.Cookies("USU_DATEFMT") 
	
	set oRaiz=validarUsuario(Idioma,true,false,0)    
	CiaComp = oRaiz.Sesion.CiaComp
	Den=devolverTextos(idioma,131)
	    
	set oOrden = oraiz.Generar_COrden
    if Ambito=0 then
        'CDs de cabecera
        oOrden.id = clng(Id)
	    set adorCDs = oOrden.DevolverCostesDescuentosCab(CiaComp)
    else
        'CDs de línea        
	    set adorCDs = oOrden.DevolverCostesDescuentosLinea(Id,CiaComp)
    end if
%>

<TITLE><%=title%></TITLE>

<SCRIPT>
function Cerrar()
{
	//Cierra la ventana
	window.close()
}

</SCRIPT>

</HEAD>


<BODY topmargin="0" leftmargin="0">

<div style="clear:both; float:left; width:100%; height:98%;  text-align:center">    
    <div style="clear:both; width:98%; height:85%; text-align:left; overflow:auto;">        
        <div>&nbsp;</div>
        <div style="width:100%">
            <table align="center" width="100%" border="0" cellspacing="2" cellpadding="1">
                <tr>
                    <td width="25%"><b><%=den(2)%></b></td> 
                    <td width="25%"><%=visualizacionNumero(ImporteBruto,decimalfmt,thousanfmt,precisionfmt)%>&nbsp;(<%=Moneda%>)</td>                       
                    <td width="25%"><b><%=den(3)%></b></td>                     
                    <td width="25%"><%=visualizacionNumero(ImporteNeto,decimalfmt,thousanfmt,precisionfmt)%>&nbsp;(<%=Moneda%>)</td>                                        
                </tr>
            </table>                            
        </div> 
        <div>&nbsp;</div>
        <%if not adorCDs is nothing then                                             
            adorCDs.filter="Tipo=0" 
            if adorCDs.recordcount>0 then%>  
                <div style="width:100%">                                                          
                    <table align="center" width="100%" border="0" cellspacing="2" cellpadding="1">
                        <tr class="cabecera"><td colspan="2"><%=den(4)%>=<%=visualizacionNumero(ImporteCostes,decimalfmt,thousanfmt,precisionfmt)%>&nbsp;(<%=Moneda%>)</td></tr>
                        <tr class="cabecera">
                            <td width="50%" style="font-weight:normal;"><%=den(5)%></td>
                            <td width="50%" style="font-weight:normal;"><%=den(6)%>&nbsp;(<%=Moneda%>)</td>
                        </tr>                    
                        <%adorCDs.movefirst
                        while not adorCDs.eof                                                        
                            sDescCD=VisualizacionCosteDesc(iif(isnull(adorCDs("DEN").value),"",adorCDs("DEN").value),adorCDs("TIPO").value,adorCDs("OPERACION").value,adorCDs("VALOR").value,Moneda,Cambio)%>
                            <tr>
                                <td class="filaImpar" width="50%"><%=sDescCD%></td>
                                <td class="filaImpar" width="50%" align="right"><%=visualizacionNumero((adorCDs("IMPORTE").value*Cambio),decimalfmt,thousanfmt,precisionfmt)%></td>
                            </tr>
                            <%adorCDs.movenext
                        wend%>                                              
                    </table>                                                 
                </div>
                <div>&nbsp;</div>
            <%end if          
        end if  
        if not adorCDs is nothing then                        
            adorCDs.filter="Tipo=1"
            if adorCDs.recordcount>0 then%>
                <div style="width:100%">            
                    <table align="center" width="100%" border="0" cellspacing="2" cellpadding="1">
                        <tr class="cabecera"><td colspan="2"><%=den(7)%>=<%=visualizacionNumero(ImporteDescuentos,decimalfmt,thousanfmt,precisionfmt)%>&nbsp;(<%=Moneda%>)</td></tr>
                        <tr class="cabecera">
                            <td width="50%" style="font-weight:normal;"><%=den(8)%></td>
                            <td width="50%" style="font-weight:normal;"><%=den(9)%>&nbsp;(<%=Moneda%>)</td> 
                        </tr>                   
                        <%adorCDs.movefirst
                        while not adorCDs.eof                            
                            sDescCD=VisualizacionCosteDesc(iif(isnull(adorCDs("DEN").value),"",adorCDs("DEN").value),adorCDs("TIPO").value,adorCDs("OPERACION").value,adorCDs("VALOR").value,Moneda,Cambio)%>
                            <tr>
                                <td class="filaImpar" width="50%"><%=sDescCD%></td>
                                <td class="filaImpar" width="50%" align="right"><%=visualizacionNumero((adorCDs("IMPORTE").value*Cambio),decimalfmt,thousanfmt,precisionfmt)%></td>
                            </tr>
                            <%adorCDs.movenext
                        wend%>                                                 
                    </table>                 
                </div>                
            <%end if     
        end if%>            
    </div>       
    <div style="clear:both; position:relative; width:100%; height:10%; text-align:center;">
        <div>&nbsp;</div>	                   
        <INPUT TYPE="button" class="button" ID=btnCerrar value="<%=Den(10)%>" onclick="Cerrar()">
        <div>&nbsp;</div>
    </div>        
</div>
</BODY>
</HTML>

