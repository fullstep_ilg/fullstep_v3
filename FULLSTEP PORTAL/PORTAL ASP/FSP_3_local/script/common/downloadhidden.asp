﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<%
''' <summary>
''' A los dos segundos de pulsar descargar, se procede a la descarga
''' </summary>
''' <remarks>Llamada desde: common\download.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies ("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,false,false,0)

den=devolverTextos(Idioma,113)

mpath = application("RUTASEGURA")
servidor = Request.servervariables("SERVER_NAME")
mpath = replace(mpath,"http://" & servidor,"")
mpath = replace(mpath,"https://" & servidor,"")

sPath = mPath & "download/" & request("path") & "/" & request("nombre")

%>
<HTML>
<script>
function init()
{
window.setTimeout(download,2000)
}
/*
''' <summary>
''' A los dos segundos de pulsar descargar, se procede a la descarga
''' </summary>     
''' <remarks>Llamada desde: init ; Tiempo máximo: 0</remarks>*/
function download()
{    
	window.open("<%=application("RUTASEGURA")%>/script/common/downloadfile.asp?download=1&src=<%=server.URLEncode(server.mappath(sPath))%>&fname=<%=server.urlEncode(request("nombre"))%>","_self")
}
</script>
<HEAD>
<title><%=Application("TITULOVENTANAS_" & Idioma)%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</HEAD>
<BODY onload="init()">

<P>&nbsp;</P>

</BODY>
</HTML>
