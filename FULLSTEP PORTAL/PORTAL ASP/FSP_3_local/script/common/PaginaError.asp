﻿<%@ language="VBScript" %>

<%
''' <summary>
''' Muestra la pantalla de error
''' </summary>
''' <remarks>Llamada desde: nadie ; Tiempo máximo: 0,2</remarks> 

  Option Explicit

  Const lngMaxFormBytes = 200

  Dim objASPError, blnErrorWritten, strServername, strServerIP, strRemoteIP
  Dim strMethod, lngPos, datNow, strQueryString, strURL

  If Response.Buffer Then
    Response.Clear
    Response.Status = "500 Internal Server Error"
    Response.ContentType = "text/html"
    Response.Expires = 0
  End If

  Set objASPError = Server.GetLastError
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">

<html dir=ltr>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<META NAME="ROBOTS" CONTENT="NOINDEX">
</head>

<body bgcolor="FFFFFF">
	

	<table class=principal>
	<tr>
		<td class = error>
			Error
		</td>
	</tr>
	<tr>
		<td class = error>
			&nbsp;
		</td>		
	</tr>
	<tr>
		<td>
			Please, contact the portal administrator and tell him about this.
		</td>
	</tr>
	<tr>
		<td>
			Contact data:
		</td>
	</tr>
	<tr>
		<td>
			<a style="font-size:12px" href="mailto:<%=Application("MAILPORTAL")%>"><%=Application("MAILPORTAL")%></a>
		</td>
	</tr>
	</table>

</body>
</html>
