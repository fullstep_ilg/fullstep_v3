﻿// Funciones para la utilización de llamadas javasript Asincronas mediante XMLHTTP
// DPD 12/04/2011

// objeto XMLHTTP
var xmlobj;

// instancia del objeto
function crearObjetoXmlHttpRequest(){
  if(window.XMLHttpRequest) {
    xmlobj = new XMLHttpRequest();
  }
  else  {
    try {
      xmlobj = new ActiveXObject("Microsoft.XMLHTTP");
    }
    catch (e) {
    }
  }
  return xmlobj;
}

// Llamada Asíncrona
function Ajax(URL,Fn)
{
  xmlobj = crearObjetoXmlHttpRequest();
  xmlobj.open("GET",URL,true);  
  xmlobj.onreadystatechange = Fn;
  xmlobj.send(null);
}

// Llamada Síncrona
function AjaxSync(URL)
{
  xmlobj = crearObjetoXmlHttpRequest();
  xmlobj.open("GET",URL,false);  
  xmlobj.send(null);
}

//Manejador de la respuesta (Para llamadas asíncronas)
function manejadorRespuesta(Fn){
  if(xmlobj.readyState ==4) {
      var respuesta = xmlobj.responseText;
      /*Código DOM para actualizar el documento XHTML*/
      eval(Fn);
      xmlobj=null;
  }
}

//función que no hace nada ya que no esperamos respuesta de la llamada asíncrona.
function FunNull() {
}

