﻿<%
if Application("FSAL")=1 then

    arrUsuCodCiaCod=ObtenerUsuCodCiaCod(Request.Cookies("USU_SESIONID"),Request.ServerVariables("LOCAL_ADDR"),Request.Cookies("SESPP"),Request.Cookies("USU_CSRFTOKEN"))
    if UBound(arrUsuCodCiaCod) = 1 then
        sUsuCod=arrUsuCodCiaCod(0)
        sProveCod=arrUsuCodCiaCod(1)
    else
        sUsuCod="Registro"
        sProveCod="Registro"
    end if

    set arrUsuCodCiaCod=Nothing

    if (Request.Form("fechaIniFSAL7") <> "") and (Request.Form("sIdRegistroFSAL7") <> "") then
        fechaIniFSAL7=Request.Form("fechaIniFSAL7")
        sIdRegistroFSAL7=Request.Form("sIdRegistroFSAL7")
    else
        fechaIniFSAL7=Request("fechaIniFSAL7")
        sIdRegistroFSAL7=Request("sIdRegistroFSAL7")
    end if
    sIdRegistroFSAL1=sIdRegistroFSAL7   ''para que en fsal_3.asp coja valor la asignacion que hay
    sIDPagina="http://" & Request.ServerVariables("SERVER_NAME") & Request.ServerVariables("URL")
    sNavegador=Request.ServerVariables("HTTP_USER_AGENT")
    sQueryString=Server.URLEncode(Request.ServerVariables("QUERY_STRING"))
    fechaFinFSAL7 = GetFechaLog()

    if InStr(sNavegador,"Firefox/29.0") > 0 then
        sArgs="Tipo=5&Producto=" & Application("FSAL_Origen") & _
          "&fechapet=" & fechaIniFSAL7 & _
          "&fecha=" & fechaFinFSAL7 & _
          "&pagina=" & sIDPagina & _
          "&iPost=0" & _
          "&iP=" & Request.ServerVariables("REMOTE_ADDR") & _
          "&sUsuCod=" & sUsuCod & _
          "&sPaginaOrigen=" & ObtenerPaginaOrigen(sIDPagina,5) & _
          "&sNavegador=" & sNavegador & _
          "&IdRegistro=" & sIdRegistroFSAL7 & _
          "&sProveCod=" & sProveCod & _
          "&sQueryString=" & sQueryString & _
          "&sNormalizarFechas=0"
        CallWebServiceASP Application("URL_FSAL_RegistrarAccesos_Server"),"FSALRegistrarAccesos",sArgs
    end if

    fechaIniFSAL7=GetFechaLog()

end if
%>