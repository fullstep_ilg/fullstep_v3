﻿
/*''' <summary>
''' Los query string pueden tener un problema con ' , con " , con chr(10) y con chr(13). Los reemplazados
''' por sus codigos unicode en formato decimal.
''' </summary>
''' <param name="Entrada">Texto donde reemplazar</param>
''' <returns>Texto reemplazado</returns>
''' <remarks>Llamada desde: noconformidad.aspx/uwgNoConform_CellClickHandler; Tiempo mÃ¡ximo:0</remarks>*/
function validarText(Entrada) {
    Entrada = replaceAll(Entrada, "'", "/X27;")//%39
    Entrada = replaceAll(Entrada, '"', "/X22;")//%34

    return Entrada
}


/* valida que la fecha (en realidad un input) que se le pasa esté en el formato adecuado

date_field: objeto input que debe contener la fecha a validar
vdatefmt:formato que debe cumplir la fecha

*/


function validarFecha(date_field,vdatefmt,minimo,maximo,fValid,errMsg,errLimites) {
	var lminimo
	var lmaximo
		lminimo=minimo
		lmaximo=maximo
		sepFecha = vdatefmt.substr(2,1)
		if (vdatefmt.substr(0,1)=="d")
			wMonth=1
		else
			wMonth=0
		if (vdatefmt.substr(0,1)=="m")
			wDay=1
		else
			wDay=0
        if (!date_field.value)  
                return true;
        var in_date = stripCharString(date_field.value," ");
        in_date = in_date.toUpperCase();
        var date_is_bad = 0;  
        if (!allowInString(in_date,sepFecha  + "0123456789"))
                date_is_bad = 1; // invalid characters in date
        if (!date_is_bad) {
                var date_pieces = new Array();
                date_pieces = in_date.split(sepFecha);
                if (date_pieces.length == 2) {
                        var d = new Date();
                        in_date = in_date + sepFecha + get_full_year(d);
                        date_pieces = in_date.split(sepFecha);
                }
                if (date_pieces.length != 3 || parseInt(date_pieces[wMonth],10) < 1 || parseInt(date_pieces[wMonth],10) > 12 
                                || parseInt(date_pieces[wDay],10) < 1 || parseInt(date_pieces[wDay],10) > 31 
                                || (date_pieces[2].length != 2 && date_pieces[2].length != 4)) {
                        date_is_bad = 6;  // date is not in format of m[m]/d[d]/yy[yy]
                }
        }
        if (date_is_bad) {
				if (errMsg)
					alert(errMsg)
                date_field.focus();
                return (false);
        }
			
        in_date = date_pieces[wMonth] + "/" + date_pieces[wDay] + "/" + date_pieces[2]
        
        var ms = Date.parse(in_date);
        var d = new Date();
        d.setTime(ms);
		var return_date = d.toLocaleString();
		var return_month = parseInt(d.getMonth() + 1).toString();
		return_month = (return_month.length==1 ? "0" : "") + return_month; 
		var return_date =  parseInt(d.getDate()).toString();
		return_date = (return_date.length==1 ? "0" : "") + return_date;
		if (vdatefmt.substr(1,1)=="d")
        return_date = return_date + sepFecha + return_month  + sepFecha + get_full_year(d);
		else
        return_date = return_month + sepFecha + return_date  + sepFecha + get_full_year(d);
        
        if (lminimo=="" ||lminimo ==null)
			lminimo = return_date
			
			
		
		if (lmaximo=="" ||lmaximo ==null)
			lmaximo = return_date
        
        if (str2date(lminimo,vdatefmt)>str2date(return_date,vdatefmt) || str2date(lmaximo,vdatefmt)<str2date(return_date,vdatefmt))
			{
			if (errLimites)
				alert(errLimites)
            date_field.focus();
			return false
			}
			
		
		x=true
        if (fValid)
			x = eval(fValid + "(date_field,return_date)")
		if (!x)
			{
			date_field.focus();
			return false
			}
		else
			{
		    date_field.value = return_date;

			return true
			}

 
			

}       // normalize the year to yyyy

function get_full_year(d) {
		var y = ""
		if (d.getFullYear() != null)
		{
			y = d.getFullYear();
			if (y < 1970) y+= 100;		
		} else
		{	
	        y = d.getYear();
	        if (y > 69  && y < 100) y += 1900;
	        if (y < 1000) y += 2000;
		}
        return y;
}
// The following functions were written by Gordon McComb
// More information can be found here: http://www.javaworld.com/javaworld/jw-02-1997/jw-02-javascript.html
function stripCharString (InString, CharString)  {
        var OutString="";
   for (var Count=0; Count < InString.length; Count++)  {
        var TempChar=InString.substring (Count, Count+1);
      var Strip = false;
      for (var Countx = 0; Countx < CharString.length; Countx++) {
        var StripThis = CharString.substring(Countx, Countx+1)
         if (TempChar == StripThis) {
                Strip = true;
            break;
         }
      }
      if (!Strip)
        OutString=OutString+TempChar;
   }
        return (OutString);
}
function allowInString (InString, RefString)  {
        if(InString.length==0) return (false);
        for (var Count=0; Count < InString.length; Count++)  {
        var TempChar= InString.substring (Count, Count+1);
      if (RefString.indexOf (TempChar, 0)==-1)  
        return (false);
   }
   return (true);
}


/*Comprueba que el valor de elemento que se le pasa sea un número 
y además lo formatea de forma que cumpla las reglas

elemento: objeto Input cuyo valor se va a validar/formatear
vdecimalfmt: separador decimal
vthousanfmt: separador de miles
vprecisionfmt: número de decimales que se muestran
*/


function validarNumero(elemento, vdecimalfmt, vthousanfmt, vprecisionfmt, minimo, maximo, errMsg, errLimites)
{
var lminimo
var lmaximo
lminimo=minimo
lmaximo=maximo
f=document.forms["frmOferta"]
valor=elemento.value
if (!elemento.value)  
	{
    return true;
    }

if (!allowInString(valor,vdecimalfmt + vthousanfmt + "0123456789+-"))
	{
	//"Número incorrecto.\nSeparador decimal: " + vdecimalfmt + "\nSeparador de miles: " + vthousanfmt
	if (errMsg)
		alert(errMsg)
	elemento.focus()
	return false
	}
rd = eval("/\\" + vdecimalfmt + "/")
if (valor.length==0)
	{
	return true
	}

if (valor.search(rd)==valor.length -1)
	{
	valor=valor.replace(rd,"")
	}

if (vthousanfmt=="") 
	{
	rr = /\X/i
	}
else
	{
	rr= eval("/\\" + vthousanfmt + "/i")
	}

while (valor.search(rr)!=-1)
	{
	valor=valor.replace(rr,"")
	}

var miValor

miValor =str2num(valor,vdecimalfmt, vthousanfmt)
if (lminimo==null)
	lminimo=miValor
if (lmaximo==null)
	lmaximo=miValor
if (lminimo>miValor || lmaximo<miValor)
	{
	if (errLimites)
		alert(errLimites)
	return false
	}
else
	{
	elemento.value=num2str(str2num(valor,vdecimalfmt,vthousanfmt),vdecimalfmt,vthousanfmt,vprecisionfmt)
	return true
	}

}

/*''' <summary>
''' evita el tecleo de caracteres incorrectos cuando se introduce un número se debe incluir en el onkeypress del input que se quiera limitar
''' </summary>
''' <param name="elemento">objeto donde se teclea</param>
''' <param name="vdecimalfmt">caracter para los decimales</param>        
''' <param name="vthousanfmt">caracter para los miles</param>
''' <param name="event">evento de sistema</param>       
''' <returns>True si se accepta la pulsación False si no se acepta</returns>
''' <remarks>Llamada desde: Todos los onkeypres de controles numericos; Tiempo máximo: 0,2</remarks>*/
function mascaraNumero(elemento,vdecimalfmt,vthousanfmt,event) {
    var Key
	//CALIDAD
    Key = ((document.all) ? event.keyCode : event.charCode)

    var FF
	//CALIDAD
    FF = ((document.all) ? 0 : 1)

    if (FF == 1) {
        if (Key == 0) {
            /*Propiedad keyCode: para las teclas normales, no definido. Para las teclas especiales, el código interno de la tecla.
              Propiedad charCode: para las teclas normales, el código del carácter de la tecla que se ha pulsado. Para las teclas especiales, 0.
            */
            Key = event.keyCode;
        }
    }

    //IE9 responde las siguientes teclas especiales
    //8 Delete
    //9 Tab
    //35 Fin
    //36 Inicio
    //37 flecha izq
    //39 flecha derecha
    //127 Suprimir   Funciona aun sin controlarlo. De hecho si haces un alert(Key) da 46, es decir, punto?
    //No responde a flecha arriba ni flecha abajo. Notar q donde si q responde es un .net, pero alli son webnumeric y no html.

    if (vthousanfmt=="")
    {
        if (Key == vdecimalfmt.charCodeAt(0) || (Key >= 48 && Key <= 57) || Key == 43 || Key == 45 || Key == 8 || Key == 9 || (Key >= 35 && Key <= 37) || Key == 39)
		return true
		else
		return false
	}
    else
	{
	    if (Key == vthousanfmt.charCodeAt(0) || Key == vdecimalfmt.charCodeAt(0) || (Key >= 48 && Key <= 57) || Key == 43 || Key == 45 || Key == 8 || Key == 9 || (Key >= 35 && Key <= 37) || Key == 39)
	    	return true
		else
		    return false
	}
}

/*''' <summary>
''' Redondear el numero dado al numero de decimales dado
''' </summary>
''' <param name="num">Numero a redondear</param>
''' <param name="dec">numero de decimales</param>        
''' <returns>numero redondeado</returns>
''' <remarks>Llamada desde: formatos.js/num2str; Tiempo máximo:0,01</remarks>*/
function redondea(num,dec)
{
var nTmp = num

var sNum=nTmp.toString()
var rd=/\./
var pDec = sNum.search(rd)
var rde=/\e/
var pDecE = sNum.search(rde)

if (pDec==-1)
	return(num)

sDec = sNum.substr(pDec + 1, sNum.length)

if (pDecE==-1)
{
	dec = (dec<sDec.length)?dec:sDec.length	
}
else
{ //Encontrado numero tal como 2.5e-8
	return(num)
}


for (var i=0;i<dec;i++)
	{
	nTmp = nTmp * 10
	}

nTmp = Math.round(nTmp)


for (var i=0;i<dec;i++)
	nTmp = nTmp / 10
	
return(nTmp)

}

//''' <summary>
//''' convierte un numero a una cadena aplicandole el formato correspondiente
//''' </summary>
//''' <param name="num">Numero a convertir</param>
//''' <param name="vdecimalfmt">separador de decimales</param>
//''' <param name="vthousanfmt">separador de miles</param>
//''' <param name="vprecisionfmt">numero de decimales a mostrar</param>                
//''' <returns>cadena con el formato correspondiente ó si es un numero muy pequeño el numero</returns>
//''' <remarks>Llamada desde: Toda pantalla q muestre numeros. Si haces un find salen 141 usos; Tiempo máximo: 0,1</remarks>
function num2str(num,vdecimalfmt,vthousanfmt,vprecisionfmt)
{
var j
if (num==0)
	{
	return ("0")
	}
if (num == null)
	{
	return ("")
	}
if (isNaN (num))
	{
	return ("")
	}

var sNum
var sDec
var sInt
var bNegativo
var i


bNegativo=false
var vNum = redondea(num,vprecisionfmt)
sNum=vNum.toString()

var rde=/\e/
var pDecE = sNum.search(rde)

if (pDecE != -1)
{
	sInt = sNum.substr(0, pDecE)
	sDec = (sNum.substr(pDecE + 2, sNum.length))-1
	
	sNum= "0" + vdecimalfmt 
	for (var i=0;i<sDec;i++)
	{
		sNum = sNum + "0"
	}
	
	for (var j=0;j<sInt.length;j++)
	{
		if (isNaN (sInt.charAt(j)))
		{
			sNum = sNum
			
			if (sInt.substr(j+1,3) == "000")
			{
				j=sInt.length
			}
			if (sInt.substr(j+1,3) == "999")
			{
				sNum = sNum.substr(0,sNum.length -1)
				if (sInt.substr(j-1,1)=="9")
				{
					sNum = sNum.substr(0,sNum.length -1)
					sNum = sNum + "1"					
				}
				else
				{
					num = parseFloat(sInt.charAt(j-1)) + 1
					sNum = sNum + num
				}				
				j=sInt.length
			}			
		}
		else
		{
			if (sInt.substr(j+1,3) == "000")
			{
				sNum = sNum + sInt.charAt(j)
				
				j=sInt.length
			}
			else
			{
				if (sInt.substr(j+1,3) == "999")
				{						
					
					if (sInt.substr(j,1)=="9")				
					{
						sNum = sNum.substr(0,sNum.length -1)
						sNum = sNum + "1"
					}
					else
					{
						num = parseFloat(sInt.charAt(j)) + 1
						sNum = sNum + num
					}
										
					j=sInt.length
				}			
				else
				{
					sNum = sNum + sInt.charAt(j)
				}
			}
		}
	}

	//--------------------------------------------------
	//Por una parte mirar si es Valido
	//Por otra parte mirar cifras significativas
	var MeVale=0
	var k =0
	
	for(k=0;k<vprecisionfmt+12;k++)
	{
		if (k>1)
		{
			if (sNum.charAt(k)!="0")//Por una parte mirar si es Valido	
			{						
				k=vprecisionfmt+12
				MeVale=1				
			}
		}
	}
	
	if (MeVale==0) 
	{
		sNum="0"		
	}
	else
	{//Por otra parte mirar cifras significativas

		if (sNum.length -2 > vprecisionfmt)
		{
			var sNumAux1
			var sNumAux2
			var iNumAux1
			var iNumAux2
			var Fin =0
			var PosValido = vprecisionfmt
			
			while (Fin==0)
			{
				sNumAux1=sNum.substr(2,PosValido)
				sNumAux2="0." + sNum.substr(PosValido+2)
				iNumAux1=parseFloat(sNumAux1)
				iNumAux2=Math.round(parseFloat(sNumAux2))				
								
				if (iNumAux1!=0)
				{
					Fin=1
					
					if (iNumAux2==0)
					{
						sNum=sNum.substr(0,PosValido+2)
					}
					else
					{//Ya se q +1
						num = parseFloat(sNum.charAt(PosValido+1)) + 1
						if (num==10)
						{
							num = parseFloat(sNum.charAt(PosValido)) + 1
							if (num==10)
							{
								num = parseFloat(sNum.charAt(PosValido-1)) + 1	
								PosValido = PosValido -1
							}	
							sNum = sNum.substr(0,PosValido)
							sNum = sNum + num							
						}
						else
						{
							sNum = sNum.substr(0,PosValido+1)
							sNum = sNum + num
						}
					}
					
					if (PosValido > vprecisionfmt)
					{
						sNumAux1="0." + sNum.substr(PosValido+1)
						iNumAux1=Math.round(parseFloat(sNumAux1))	
						
						if (iNumAux1!=0)
						{
							sNum = sNum.substr(0,sNum.length-2) + "1"
						}
					}
				}
				else
				{
					PosValido = PosValido +1 
				}
			}			
		}
	}
	//--------------------------------------------------	
	
	return (sNum)
}

if (sNum.charAt(0)=="-")
	{
	bNegativo=true
	sNum=sNum.substr(1,sNum.length)
	}
rd=/\./
pDec = sNum.search(rd)

if (pDec==-1)
	{
	sInt = sNum
	sDec = ""
	}
else
	{
	sInt = sNum.substr(0, pDec)
	sDec= sNum.substr(pDec+1,20)
	if (sDec.substr(sDec.length-4,4)=="999" + sDec.substr(sDec.length-1,1))
		{
		var lDigito = 10 - parseFloat(sDec.substr(sDec.length-1,1))
		var sSum = "0."
		for (j=1;j<sDec.length;j++)
			sSum += "0"
		sSum += lDigito.toString()
		
		
		sNum = (parseFloat(sInt + "." + sDec) + parseFloat(sSum)).toString()
		pDec = sNum.search(rd)

		if (pDec==-1)
			{
			sInt = sNum
			sDec = ""
			}
		else
			{
			sInt = sNum.substr(0, pDec)
			sDec = sNum.substr(pDec+1,vprecisionfmt)
			}
		}
	}

if (sInt.length>3)
	{
	newValor=""
	s=0
	//los posicionamos correctamente
	for (i=sInt.length-1;i>=0;i--)
		{
		if (s==3)
			{
			newValor=vthousanfmt + newValor
			s=0
			}
		newValor=sInt.charAt(i) + newValor				
		s++
		}
	sInt=newValor
	}

var redondeo = sDec

sDec=sDec.substr(0,vprecisionfmt>=12?10:vprecisionfmt)

while(sDec.charAt(sDec.length-1)=="0")
	sDec=sDec.substr(0,sDec.length-1)

if (sDec!="" && sDec!="000000000000".substr(0,vprecisionfmt))
	sNum= sInt + vdecimalfmt + sDec
else
	sNum = sInt
if (bNegativo==true)
	sNum = "-" + sNum

var vprec = vprecisionfmt
while (sNum=="0" && num!=0)
	{
	vprec++
	sNum = num2str(num,vdecimalfmt,vthousanfmt,vprec)
	}
	

return (sNum)
}

//convierte una cadena en un número
function str2num(str,vdecimalfmt,vthousanfmt)
{
if (str=="")
	{
	return (null)
	}
if (vthousanfmt=="") 
	{
	rm = /\X/i
	}
else
	{
	rm= eval("/\\" + vthousanfmt + "/i")
	}

while (str.search(rm)!=-1)
	{
	str=str.replace(rm,"")
	}

rd=eval("/\\" + vdecimalfmt + "/")

str=str.replace(rd,".")

num = parseFloat(str)

return (num)

}


// <summary>
// Esta funcion convierte un objeto javascript Date en una cadena con una fecha y horas minutos y segundos
//en el formato especificado en vdatefmt
// </summary>
// <param name="fec">Fecha</param>
// <param name="datefmt">Formato</param>        
// <returns>Fecha</returns>
// <remarks>Llamada desde: cumpoferta.asp ; Tiempo máximo: 0,1</remarks>
// REvisada por EPB 21/02/2013
function datehour2str(fec, vdatefmt) {
    var vDia
    var vMes
    var str

    if (fec == null) {
        return ("")
    }
    re = /dd/i
    
    vDia = "00".substr(0,2-fec.getDate().toString().length) + fec.getDate().toString()
    str = vdatefmt.replace(re, vDia)

    re = /mm/i

    vMes = "00".substr(0,2-(fec.getMonth() + 1).toString().length) + (fec.getMonth() + 1).toString()
    str = str.replace(re, vMes)

    re = /yyyy/i

    str = str.replace(re, fec.getFullYear())
    str += (" " + "00".substr(0,2-(fec.getHours()).toString().length) + fec.getHours() + ":" + "00".substr(0,2-(fec.getMinutes()).toString().length) + fec.getMinutes() + ":" + "00".substr(0,2-(fec.getSeconds()).toString().length) + fec.getSeconds())  
    return (str)
}

//Esta funcion convierte un objeto javascript Date en una cadena con una fecha
//en el formato especificado en vdatefmt
function date2str(fec,vdatefmt,bConHora)
{
var vDia
var vMes
var str

if (fec==null)
	{
	return ("")
	}
re=/dd/i
vDia = "00".substr(0,2-fec.getDate().toString().length) + fec.getDate().toString()
str = vdatefmt.replace(re,vDia)

re=/mm/i

vMes = "00".substr(0,2-(fec.getMonth() + 1).toString().length) + (fec.getMonth() + 1).toString()
str = str.replace(re,vMes)

re=/yyyy/i

str = str.replace(re,fec.getFullYear())
if (bConHora)
	str += (" " + "00".substr(0,2-(fec.getHours()).toString().length) + fec.getHours() + ":" + "00".substr(0,2-(fec.getMinutes()).toString().length) + fec.getMinutes())
return(str)
}

//Esta funcion convierte una cadena con una fecha en el formato indicado en vdatefmt
//a un objeto Date
function str2date(str,vdatefmt)
{
if (str=="")
	{
	return null
	}
sepFecha = vdatefmt.substr(2,1)
if (vdatefmt.substr(0,1)=="d")
	wMonth=1
else
	wMonth=0
if (vdatefmt.substr(0,1)=="m")
	wDay=1
else
	wDay=0

var in_date = stripCharString(str," ");
in_date = in_date.toUpperCase();
var date_pieces = new Array();
date_pieces = in_date.split(sepFecha);
if (date_pieces.length == 2) 
	{
        var d = new Date();
        in_date = in_date + sepFecha + get_full_year(d);
        date_pieces = in_date.split(sepFecha);
	}
        
in_date = date_pieces[wMonth] + "/" + date_pieces[wDay] + "/" + date_pieces[2]
        
var ms = Date.parse(in_date);
var d = new Date();
d.setTime(ms);

return (d)

}
function devolverLocFecha(Urte,Mes,Dia,Hora,Minuto)
{
var vDate
var sDate
vDate=new Date(Urte,Mes-1,Dia,Hora,Minuto)
sDia=vDate.getDate().toString()
sDia ="00".substr(0,2 - sDia.length) + sDia

sMes = (vDate.getMonth() + 1).toString()
sMes ="00".substr(0,2 - sMes.length) + sMes

sHora = (vDate.getHours()).toString()
sHora = "00".substr(0,2 - sHora.length) + sHora

sMinu = (vDate.getMinutes()).toString()
sMinu = "00".substr(0,2 - sMinu.length) + sMinu

sDate=sDia + "/" + sMes + "/" + vDate.getFullYear() + " " + sHora + ":" + sMinu
return sDate
}
function devolverUTCFecha(Urte,Mes,Dia,Hora,Minuto)
{
var vDate
var sDate
vDate=new Date(Urte,Mes-1,Dia,Hora,Minuto)

sDia=vDate.getUTCDate().toString()
sDia ="00".substr(0,2 - sDia.length) + sDia

sMes = (vDate.getUTCMonth() + 1).toString()
sMes ="00".substr(0,2 - sMes.length) + sMes

sHora = (vDate.getUTCHours()).toString()
sHora = "00".substr(0,2 - sHora.length) + sHora

sMinu = (vDate.getUTCMinutes()).toString()
sMinu = "00".substr(0,2 - sMinu.length) + sMinu


sDate=sDia + "/" + sMes + "/" + vDate.getUTCFullYear() + " " + sHora + ":" + sMinu
return sDate
}



/*
''' <summary>
''' Crear un cajetin con valores de tipo fecha
''' </summary>
''' <param name="sName">Nombre del control</param>
''' <param name="lSize">Tamaño en pixels de control</param>    
''' <param name="dValue">Valor del control</param>
''' <param name="valorMin">Valor minimo del control</param>   
''' <param name="valorMax">Valor maximo del control</param>
''' <param name="sFrm">Nombre del formulario Calendario</param>   
''' <param name="fValid">Si en el formulario Calendario, la fecha es correcta q funcion lanzar</param>
''' <param name="mas">Javascript con eventos de cliente del control (eejmpo: onChange)</param>   
''' <param name="errMsg">Texto de error por no cumplir con fValid</param>
''' <param name="errLimites">Texto de error por haber introducido un texto demasiado largo</param>       
''' <remarks>Llamada desde: pedidos21.asp   seguimclt.asp   cumpoferta.asp; Tiempo máximo: 0</remarks>*/
function inputFecha (sName, lSize,dValue,valorMin,valorMax,sFrm,fValid,mas,errMsg,errLimites)
{
var s
var imgCalendarioOff
var imgCalendarioOn
var strEspec
imgCalendarioOff = new Image()
imgCalendarioOff.src = "../common/images/calendario.GIF"
imgCalendarioOn = new Image()
imgCalendarioOn.src = "../common/images/calendario.GIF"
s = "<table border=0 cellpadding=0 cellspacing=0><tr><td valign=top><input " + mas + " maxlength=10 style='width:" + (lSize - 25) + "px' type=text  name=" + sName + " id =" + sName + " onkeydown='if (event.keyCode== 13) return(false)'  onchange='return validarFecha(this, vdatefmt,\"" + date2str(valorMin,vdatefmt) + "\",\"" + date2str(valorMax,vdatefmt) + "\"," + (fValid?"\"" + fValid + "\"":"null") + ", \"" + errMsg + "\", \"" + errLimites + "\")'   value='" +  (dValue ? date2str(dValue,vdatefmt) : "") + "'>"
s += "</td><td><a href='javascript:void(null)' onclick='show_calendar(\"" + sFrm + "." + sName + "\",null,null,vdatefmt," + (fValid?"\"" + fValid + "\"":"null") + "," + (valorMin==null?"null":"\"" + date2str(valorMin,vdatefmt) + "\"" ) + "," + (valorMax==null?"null":"\"" + date2str(valorMax,vdatefmt) + "\"") + ")'><IMG name=imgCalendario onmouseover='this.src=\"" + imgCalendarioOn.src + "\";' onmouseout='this.src=\"" + imgCalendarioOff.src + "\";'  border=0 SRC='" + imgCalendarioOff.src + "'></a></td></tr></table>"
return s
}

function trim(sValor)
{
var re = / /

var i

var sTmp = sValor

while (sTmp.search(re)==0)
	sTmp = sTmp.replace(re,"")

var sRev = ""
for (i=0;i<sTmp.length;i++)
	sRev=sTmp.charAt(i) + sRev


while (sRev.search(re)==0)
	sRev = sRev.replace(re,"")
	
sTmp=""
for (i=0;i<sRev.length;i++)
	sTmp=sRev.charAt(i) + sTmp

return(sTmp)


}


function replaceAll(sOriginal, sBuscar, sSustituir)
{
if (sOriginal==null)
	return null

var re = eval("/" + sBuscar + "/")
var sTmp = ""

while (sOriginal.search(re)>=0)
	{
	sTmp += sOriginal.substr(0,sOriginal.search(re)) + sSustituir
	sOriginal = sOriginal.substr(sOriginal.search(re) + sBuscar.length,sOriginal.length)
	}
sTmp += sOriginal
return (sTmp)

}

function JS2HTML(s)
{

var sTmp = s
var re = /\<input/
if (sTmp.search(re)!=-1)
	return (s)
var re = /\<textarea/
if (sTmp.search(re)!=-1)
	return (s)
var re = /\<INPUT/
if (sTmp.search(re)!=-1)
	return (s)
var re = /\<TEXTAREA/
if (sTmp.search(re)!=-1)
	return (s)


var re = /\n/
var i = 0 
while (sTmp.search(re)!=-1)
	{
	sTmp = sTmp.replace(re,"<br>")
	}
return(sTmp)
}


/*''' <summary>
''' Validar un CIF o NIE. 
'''	Comprobar que tiene 1 letra seguido de 7 números y 1 numero o letra. Luego con los 7 numeros comprobar letra.
''' NIE 1 letra de entre (XYZ) + 7 numeros + 1 letra
''' </summary>
''' <param name="sCIF">Cif o Nie a validar</param>    
''' <returns>Si es valido o no</returns>
''' <remarks>Llamada desde: compania.asp	modifcompaniaclient.asp; Tiempo máximo:0,1</remarks>*/
function VALIDAR_CIF(sCIF)

{
	var sPRI 
	var sULT 
	var sRESTO 
	var i

	var iSUMA 
	var sIMPARES
	    
	sIMPARES = new Array()
	if (trim(sCIF).length != 9)
	    return false
	else
		{
		sPRI = sCIF.substr(0,1).toUpperCase()
		sULT = sCIF.substr(sCIF.length-1,1).toUpperCase()
		sRESTO = sCIF.substr(1,7)
		for (i=0;i<7;i++)
		{	
			if (isNaN (sRESTO.charAt(i)))
			{
				return false
			}
		}

		iSUMA = parseInt(sRESTO.substr(1,1)) + parseInt(sRESTO.substr(3, 1)) + parseInt(sRESTO.substr(5, 1))


		sIMPARES[1] = (2 * parseInt(sRESTO.substr(0, 1))).toString()
		sIMPARES[1] = "00".substr(0,2-sIMPARES[1].length) + sIMPARES[1]
		sIMPARES[2] = (2 * parseInt(sRESTO.substr(2, 1))).toString()
		sIMPARES[2] = "00".substr(0,2-sIMPARES[2].length) + sIMPARES[2]
		sIMPARES[3] = (2 * parseInt(sRESTO.substr(4, 1))).toString()
		sIMPARES[3] = "00".substr(0,2-sIMPARES[3].length) + sIMPARES[3]
		sIMPARES[4] = (2 * parseInt(sRESTO.substr(6, 1))).toString()
		sIMPARES[4] = "00".substr(0,2-sIMPARES[4].length) + sIMPARES[4]
		for (i = 1;i<=4;i++)
			{
			izq = parseInt(sIMPARES[i].substr(0, 1))
			der = parseInt(sIMPARES[i].substr(sIMPARES[i].length - 1, 1))
		    iSUMA = iSUMA + (izq + der)
			}
		iAux = parseInt(iSUMA.toString().substr(iSUMA.toString().length-1, 1))
		if (sPRI != "X" && sPRI != "Y" && sPRI != "Z" &&  sPRI != "P" && sPRI != "S" && sPRI != "Q" )
			{
			sULTC = (10 - iAux).toString()
			if (sULT ==  sULTC.substr(sULTC.length-1,1))
				rdo  = true
			else
			    switch (sULTC.substr(sULTC.length-1,1))
				{
                 case "1":
                    if (sULT == "A")
                        rdo = true
                    else
                        rdo = false
					break                        
                case "2":
                    if (sULT == "B")
                        rdo = true
                    else
                        rdo = false
					break                        
                case "3":
                    if (sULT == "C")
                        rdo = true
                    else
                        rdo = false
					break                        
                case "4":
                    if (sULT == "D")
						rdo = true
                    else
                        rdo = false
					break                        
                case "5":
                    if (sULT == "E")
                        rdo = true
                    else
                        rdo = false
					break                        
                 case "6":
                    if (sULT == "F")
                        rdo = true
                    else
                        rdo = false
					break
                 case "7":
                    if (sULT == "G")
                        rdo = true
                    else
                        rdo = false
					break                        
                 case "8":
                    if (sULT == "H")
                        rdo = true
                    else
                        rdo = false
					break
                 case "9":
                    if (sULT == "I")
                        rdo = true
                    else
                        rdo = false
                    break
	             case "0":
					if (sULT == "J")
                        rdo = true
					else
                        rdo = false
					break
				default:
					rdo = false
				}
			 }
		else 
			{
			if (sPRI == "X" || sPRI == "Y" || sPRI == "Z")
				{
					//Valida Nie
					if (sPRI == "X")
						rdo=  VALIDAR_NIF("0" + sRESTO + sULT)
					if (sPRI == "Y")
						rdo= VALIDAR_NIF("1" + sRESTO + sULT)
					if (sPRI == "Z")
						rdo= VALIDAR_NIF("2" + sRESTO + sULT)											
				}
			else
				{
					if (sULT == String.fromCharCode(64 + (10 - iAux)))
						rdo = true
					else
						rdo = false				
				}
			}
		}
return (rdo)
}

/*''' <summary>
''' Validar un NIF. 
'''	Comprobar que tiene 8 números y una letra (entre A-Z). Luego con los 8 numeros comprobar letra.
''' </summary>
''' <param name="sNif">Nif a validar</param>    
''' <returns>Si es valido o no</returns>
''' <remarks>Llamada desde: compania.asp	modifcompaniaclient.asp; Tiempo máximo:0,1</remarks>*/
function VALIDAR_NIF(sNif) 
{

    var lngVALOR 
    var intENTERO 
    var sLetra 
    
    if (trim(sNif).length != 9 )
        return false
    else
		{
			var dni = sNif.substring(0,8).toString();
			for (var i=0;i<8;i++)
			{	
				if (isNaN (dni.charAt(i)))
				{
					return false
				}
			}		
		
        sLetra = sNif.substr(sNif.length-1,1).toUpperCase()
        if (sLetra >= "A" && sLetra <= "Z")
			{
            lngVALOR = parseFloat(sNif)
            if (isNaN (lngVALOR))
				return false            
            intEntero = lngVALOR / 23
            intEntero = num2str(lngVALOR / 23,".","",10)
            rd=/\./
            var pDec = intEntero.search(rd)
			if (pDec==-1)
				intENTERO = 1
			else
				{
				intEntero = intEntero.substr(0,pDec)
				intENTERO = lngVALOR - (intEntero * 23) + 1
				}
            switch (intENTERO)
				{
                case 1:
                case 24:
                    if (sLetra == "T")
                        return true
                    else
                        return false
                    break
                case 2:
                    if (sLetra == "R")
                        return true
                    else
                        return false
                    break
                case 3:
                    if (sLetra == "W")
                        return true
                    else
                        return false
                    break
                case 4:
                    if (sLetra == "A")
                        return true
                    else
                        return false
                    break
                case 5:
                    if (sLetra == "G")
                        return true
                    else
                        return false
                    break
                 case 6:
                    if (sLetra == "M")
                        return true
                    else
                        return false
                    break
                 case 7:
                    if (sLetra == "Y")
                        return true
                    else
                        return false
                    break
                 case 8:
                    if (sLetra == "F")
                        return true
                    else
                        return false
                    break
                 case 9:
                    if (sLetra == "P")
                        return true
                    else
                        return false
                    break
                 case 10:
                    if (sLetra == "D")
                        return true
                    else
                        return false
                    break
                 case 11:
                    if (sLetra == "X")
                        return true
                    else
                        return false
                    break
                 case 12:
                    if (sLetra == "B")
                        return true
                    else
                        return false
                    break
                 case 13:
                    if (sLetra == "N")
                        return true
                    else
                        return false
                    break
                 case 14:
                    if (sLetra == "J")
                        return true
                    else
                        return false
                    break
                 case 15:
                    if (sLetra == "Z")
                        return true
                    else
                        return false
                    break
                 case 16:
                    if (sLetra == "S")
                        return true
                    else
                        return false
                    break
                 case 17:
                    if (sLetra == "Q")
                        return true
                    else
                        return false
                    break
                 case 18:
                    if (sLetra == "V")
                        return true
                    else
                        return false
                    break
                 case 19:
                    if (sLetra == "H")
                        return true
                    else
                        return false
                    break
                 case 20:
                    if (sLetra == "L")
                        return true
                    else
                        return false
                    break
                 case 21:
                    if (sLetra == "C")
                        return true
                    else
                        return false
                    break
                 case 22:
                    if (sLetra == "K")
                        return true
                    else
                        return false
                    break
                 case 23:
                    if (sLetra == "E")
                        return true
                    else
                        return false
                    break
                 default:
					return false                    
				}
			}
		else
            return(false)
		}
}


function inputTexto (sName,lSize,lMax,sValue,func)
{
var s

sValue = replaceAll(sValue,"'","&#39")
s = "<input style='width:" + lSize + "' type=text name=" + sName + " id =" + sName + " maxlength= " + lMax + " value='" +  (sValue ? sValue : "") + "' " + (func?" onchange='" + func + "' ":"") + ">"
return s
}

function inputNumero (sName, lSize, lLenMax, dValue, valorMin, valorMax, mas, sFuncValidacion, errMsg, errLimites)
{
var s
if (!sFuncValidacion)
 sFuncValidacion = "validarNumero("
s = "<input " + (mas?mas:"") + " language='javascript' class='numero' style='width:" + lSize + "' type=text name=" + sName + " id =" + sName + " maxlength=" + lLenMax + " onkeypress='return mascaraNumero(this,vdecimalfmt,vthousanfmt,event);' onchange='return " + sFuncValidacion + "this,vdecimalfmt,vthousanfmt,100," + valorMin + "," + valorMax + ",\"" + errMsg + "\",\"" + errLimites + "\")'  value='" + num2str(dValue,vdecimalfmt,vthousanfmt,100) + "' >"
return s
}






/*function JS2HTML(s)
{
var re = /\n/
var sTmp = s
var i = 0 

while (sTmp.search(re)!=-1)
	{
	sTmp = sTmp.replace(re,"<br>")
	}
return(sTmp)
}*/

function Var2Param(v)
{
var re = /%/
var sTmp = v


while (sTmp.search(re)!=-1)
	{
	
	sTmp = sTmp.replace(re,"###")
	}
var re = /###/


while (sTmp.search(re)!=-1)
	{
	sTmp = sTmp.replace(re,"%25")
	}
return(sTmp)
}


function validarKeyPress(e,l)
{
if (e.value.length>=l)
	return false
else
	return true
}

function validarLength(e,l,msg)
{

if (e.value.length>l)
	{
	if (msg!="null")
		alert(msg)
	return false
	}

if (e.func)
	{
	eval(e.func + "(e)")
	}
return true
}


function textArea (sName,lWidth, lHeight, lMax,smsg, sValue, func, Bloqueado)
{
    var s
if (!isNaN(lHeight-1))
    lHeight--

if (Bloqueado != 1) 
{
    s = "<TEXTAREA style='width:" + lWidth + ";height:" + lHeight + "' type=text name=" + sName + " id =" + sName + " onkeypress='return validarKeyPress(this," + lMax + ")' onchange='return validarLength(this," + lMax + ",\"" + smsg + "\")' " + (func ? " func='" + func + "' " : "") + ">" + (sValue ? sValue : "") + "</TEXTAREA>"
}
else
{
    s = "<TEXTAREA readonly style='width:" + lWidth + ";height:" + lHeight + "' type=text name=" + sName + " id =" + sName + " onkeypress='return validarKeyPress(this," + lMax + ")' onchange='return validarLength(this," + lMax + ",\"" + smsg + "\")' " + (func ? " func='" + func + "' " : "") + ">" + (sValue ? sValue : "") + "</TEXTAREA>"
}

return s
}

// Declaring required variables
var digits = "0123456789";
var phoneNumberDelimiters = "()- "; // non-digit characters which are allowed in phone numbers
var validWorldPhoneChars = phoneNumberDelimiters + "+"; // characters which are allowed in international phone numbers (a leading + is OK)
var minDigitsInIPhoneNumber = 9; // Minimum no of digits in an international phone no.

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}
function trim(s)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not a whitespace, append to returnString.
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (c != " ") returnString += c;
    }
    return returnString;
}
function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}
/*
''' <summary>
''' Valida el numero de telefono distinguiendo en la validación si el numero es de España o no
''' </summary>
''' <param name="strPhone">Numero de telefono a validar</param>
''' <param name="strPais">Descripción del pais del nº de telefono</param>
''' <returns>Si la validación es correcta se envia true, sino se envia false</returns>
''' <remarks>Llamada desde:modificardatos.asp, admonusuariosdet.asp, usuario.asp</remarks>
''' <remarks></remarks> */
function ValidarTelefono(strPhone, strPais)
{
	var bracket=3
	strPhone=trim(strPhone)
	if(strPhone.split("+").length>2) return false //Si hay más de un +  false
	if(strPhone.split("(").length>2) return false //Si hay más de un (  false
	if(strPhone.split(")").length>2) return false //Si hay más de un )  false
	if(strPhone.indexOf("-")!=-1)bracket=bracket+1 //En bracket + 1 si hay guiones
	
	//Si no hay ( y sí hay )  false
	if(strPhone.indexOf("(")==-1 && strPhone.indexOf(")")!=-1)return false
	if(strPhone.indexOf(")")==-1 && strPhone.indexOf("(")!=-1)return false
	s=stripCharsInBag(strPhone,validWorldPhoneChars);  //Deja sólo los numeros
	var pais=strPais.toUpperCase()
	if(pais.indexOf("ESPAÑA")!=-1 || pais.indexOf("SPAIN")!=-1)
	{
		var brchr=strPhone.indexOf("(")
		if(brchr!=-1 && strPhone.split("+").length>1 && strPhone.charAt(brchr+1)!="+") return false
		
		
		if(s.length>9 && s.length==11)
		{
			if(strPhone.indexOf("(")==-1 && strPhone.indexOf("+")!=-1 && strPhone.substring(0, 1)!= "+") return false
			if(strPhone.indexOf("(")!=-1 && strPhone.indexOf("+")!=-1 && strPhone.substring(1, 2)!= "+") return false
			var guion1=strPhone.indexOf("(")
			var guion2=strPhone.indexOf(")")
			if (guion1 !=-1 && guion2 !=-1)
			{
				var prefijo=strPhone.substring(guion1+1,guion2);
				if(strPhone.indexOf("+")!=-1 && prefijo!="+34") return false;
				if(strPhone.indexOf("+")==-1 && prefijo!="34") return false;
			}
			if(s.substring(0, 2)== "34")
			{
				return true
			} 
			else
			{
				return false
			}
			
		}else if(s.length==9)
			{
			if(strPhone.indexOf("(")!=-1) return false;
			if(strPhone.indexOf(")")!=-1) return false;
			if(strPhone.indexOf("+")!=-1) return false;
			 return true
			 }
			else 
			{
			return false
			}
	}
	//Si es entero y de longitud >= minimo numero de digitos  true
	return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}
/*
''' <summary>
''' Valida la dirección de e-mail
''' </summary>
''' <param name="email">E-mail a validar</param>
''' <returns>Si la validación es correcta se envia true, sino se envia false</returns>
''' <remarks>Llamada desde:modificardatos.asp, admonusuariosdet.asp,usuario.asp</remarks>
''' <remarks></remarks> */
function validarEmail(email)
{ 
    var RegExPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,18})+$/;

if(!email.match(RegExPattern)) 
  {
    return false
  } 
  else
  {
	return true
  }
}

/*
''' <summary>
''' Devuelve la fecha UTC del cliente en formato adecuado para FSAL
''' </summary>
''' <returns>Devuelve la fecha UTC del cliente en formato adecuado para FSAL</returns>
''' <remarks>Llamada desde:fsal_3.asp, fsal_5.asp, paginas con codigo para FSAL</remarks>
''' <remarks></remarks> */
function devolverUTCFechaFSAL() {
    var fecha = new Date();
    var yyyy, MM, dd, HH, mm, ss, ms;
    yyyy = fecha.getUTCFullYear();
    MM = addZero(fecha.getUTCMonth() + 1, 2);
    dd = addZero(fecha.getUTCDate(), 2);
    HH = addZero(fecha.getUTCHours(), 2);
    mm = addZero(fecha.getUTCMinutes(), 2);
    ss = addZero(fecha.getUTCSeconds(), 2);
    ms = addZero(fecha.getUTCMilliseconds(), 3);
    return yyyy + '-' + MM + '-' + dd + ' ' + HH + ':' + mm + ':' + ss + '.' + ms;
}
/*
''' <summary>
''' Devuelve una cadena con tantos 0 delante como se indique
''' </summary>
''' <param name="x">cadena a la que concatenar 0</param>
''' <param name="n">numero de 0 a concatenar</param>
''' <returns>Devuelve una cadena con tantos 0 delante como se indique</returns>
''' <remarks>Llamada desde: funcion devolverUTCFechaFSAL()</remarks>
''' <remarks></remarks> */
function addZero(x, n) {
    while (x.toString().length < n) {
        x = '0' + x;
    }
    return x;
}
/*
''' <summary>
''' Devuelve el IdRegistro para FSAL
''' </summary>
''' <returns>Devuelve el IDRegistro para FSAL</returns>
''' <remarks>Llamada desde: paginas con codigo para FSAL</remarks>
''' <remarks></remarks> */
function devolverIdRegistroFSAL() {
    dfecha = new Date();
    sIdRegistro = (dfecha.getTime() * 10000) + 621355968000000000;
    sIdRegistro = sIdRegistro.toString().substring(0, sIdRegistro.toString().length - 3) + addZero(dfecha.getUTCMilliseconds(), 3);
    sIdRegistro = sIdRegistro + addZero(Math.floor((Math.random() * 99999999) + 1).toString(), 8);
    return sIdRegistro;
}

function validarNIF_NIE(dni) {
    var numero, let, letra;
    var expresion_regular_dni = /^[XYZ]?\d{5,8}[A-Z]$/;

    dni = dni.toUpperCase();

    if (expresion_regular_dni.test(dni) === true) {
        numero = dni.substr(0, dni.length - 1);
        numero = numero.replace('X', 0);
        numero = numero.replace('Y', 1);
        numero = numero.replace('Z', 2);
        let = dni.substr(dni.length - 1, 1);
        numero = numero % 23;
        letra = 'TRWAGMYFPDXBNJZSQVHLCKET';
        letra = letra.substring(numero, numero + 1);
        if (letra != let) {
            //alert('Dni erroneo, la letra del NIF no se corresponde');
            return false;
        } else {
            //alert('Dni correcto');
            return true;
        }
    } else {
        //alert('Dni erroneo, formato no válido');
        return false;
    }
};