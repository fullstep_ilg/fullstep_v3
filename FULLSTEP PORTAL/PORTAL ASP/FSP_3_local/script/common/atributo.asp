﻿<%@ Language=VBScript %>
<!--#include file="acceso.asp"-->
<!--#include file="idioma.asp"-->
<!--#include file="colores.asp"-->
<!--#include file="formatos.asp"-->

<%
''' <summary>
''' Mostrar la descripción de un atributo
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.asp ; Tiempo máximo: 0,2</remarks>

	Idioma = Request.Cookies("USU_IDIOMA")

	atrib=Request("atrib")
	
	set oRaiz=validarUsuario(Idioma,true,false,0)

	CiaComp = oRaiz.Sesion.CiaComp

	dim den
	
	den = devolvertextos(Idioma,92)

    
	set oAtributos = oRaiz.Generar_CAtributos	
	

anyo = request("anyo")
gmn1 = request("gmn1")
proce = request("proce")
grupo = request("grupo")
atrib = request("atrib")
ambito = clng(request("ambito"))
	
if ambito = 1 then
	oAtributos.DevolverTodosLosAtributosDesde ciacomp, anyo, gmn1, proce, , , atrib
else
	oAtributos.DevolverTodosLosAtributosDesde ciacomp, anyo, gmn1, proce, grupo, ambito, atrib
end if

set oAtrib = oAtributos.item(atrib)

%>

<HTML>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<title><%=title%></title>

<SCRIPT>

function Cerrar()
{
	window.close()
}

</SCRIPT>

</HEAD>


<BODY topmargin="10" leftmargin="0">


<TABLE class="normal" align="center" WIDTH="98%" border="0" cellspacing="1" cellpadding="1">
	<TR>
		<TD width="5%" class="cabecera"><%=den(2)%></td>
		<TD class="filaPar" ><%=HTMLEncode(oAtrib.cod)%></td>
	</TR>
	<TR>
		<TD width="35%" class="cabecera"><%=den(3)%></td>
		<TD class="filaPar"><%=HTMLEncode(oAtrib.den)%></TD>
	</TR>
	<TR>
		<TD width="35%" class="cabecera"  valign=top><%=den(29)%></td>
		<TD class="filaPar"><%=VB2HTML(oAtrib.descripcion)%></TD>
	</TR>
	<TR>
		<TD width="35%" class="cabecera"><%=den(20)%></td>
		<%if oAtrib.obligatorio then%>
			<TD bgcolor="<%=cFONDOOBLIGATORIO%>" >
				<%=den(21)%>
		<%else%>
			<TD class="filaPar">
				<%=den(22)%>
		<%end if%>
		</TD>
	</TR>
	<TR>
		<TD width="5%" class="cabecera"><%=den(4)%></td>
		<TD class="filaPar">
		<%if oAtrib.intro=0 then%>
			<%=den(5)%>
		<%else%>
			<%=den(6)%>
		<%end if%>
		</TD>
	</TR>
<%if not isnull(oAtrib.operacion) then%>
	<TR>
		<TD width="5%" class="cabecera"><%=den(23)%></td>
		<TD class="filaPar">
			<%=oAtrib.operacion%>
		</TD>
	</TR>
	<TR>
		<TD width="5%" class="cabecera"><%=den(24)%></td>
		<TD class="filaPar">
			<%select case oAtrib.aplicar
				case 0:
					Response.write den(25)
				case 1:
					Response.write den(26)
				case 2:
					Response.write den(27)
				case 3:
					Response.write den(28)
			end select%>

		</TD>
	</TR>
<%end if%>
	<TR>
		<TD width="10%" class="cabecera"><%=den(7)%></td>
		<TD class="filaPar">
			<%select case oAtrib.tipo
				case 1:
					Response.Write den(8)
				case 2:
					Response.Write den(9)
				case 3:
					Response.Write den(10)
				case 4:
					Response.Write den(11)
			end select%>
		</TD>
	</TR>

<%if oAtrib.intro = 0 then%>
	<TR>
		<TD width="10%" class="cabecera"><%=den(12)%></td>
		<TD class="filaPar">
		
		<%
		if not isnull(oAtrib.valorMin) then

			select case oAtrib.tipo
				case 1:
					Response.Write oAtrib.valorMin
				case 2:
					Response.Write visualizacionNumero(oAtrib.valorMin,decimalfmt,thousandfmt,precisionfmt)
				case 3:
					Response.Write visualizacionFecha(oAtrib.valorMin,datefmt)
			end select
			
		else
			Response.Write "&nbsp;"	
		end if
		%>
		
		</TD>
	</TR>
	<TR>
		<TD width="10%" class="cabecera"><%=den(13)%></td>
		<TD class="filaPar">
		<%
		if not isnull(oAtrib.valorMax) then
			select case oAtrib.tipo
				case 1:
					Response.Write oAtrib.valorMax
				case 2:
					Response.Write visualizacionNumero(oAtrib.valorMax,decimalfmt,thousandfmt,precisionfmt)
				case 3:
					Response.Write visualizacionFecha(oAtrib.valorMax,datefmt)
			end select
		else
			Response.Write "&nbsp;"	
		end if
		%>
		
		</TD>
	</TR>
<%else%>
	<TR>
		<TD colspan=2 class="cabecera"><%=den(18)%></td>
		</tr>
		<TR>
		<TD COLSPAN = 2 class="filaPar">


		<%for each oValor in oAtrib.Valores
			select case oAtrib.tipo
				case 1:
					Response.Write "<LI>" & oValor.valor & "<br>"
				case 2:
					Response.Write "<LI>" & visualizacionNumero(oValor.valor,decimalfmt,thousandfmt,precisionfmt) & "<br>"
				case 3:
					Response.Write "<LI>" & visualizacionFecha(oValor.valor,datefmt) & "<br>"
			end select
		next%>
		</TD>
	</TR>
<%end if%>
	<TR>
		<TD height=40px align=center COLSPAN=2>
			<INPUT TYPE="button" class="button" ID=Cerrar LANGUAGE=javascript class="Aprov" value="<%=Den(19)%>" onclick="Cerrar()">
		</TD>
	</TR>
	
</TABLE>	
			

</BODY>
</HTML>

<%

set oAtrib = nothing

set oRaiz = nothing

%>