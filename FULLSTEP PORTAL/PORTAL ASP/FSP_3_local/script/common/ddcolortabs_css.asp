﻿<%Response.ContentType="text/css" %>

<!--#include file="colores.asp"-->

A{
text-decoration:none;
}

A:link
{
font-family:Verdana;
text-decoration:none;
/* color:black !important; */
}

.ddcolortabs{
padding: 0;
width: 100%;
background: transparent;
voice-family: "\"}\"";
voice-family: inherit;
font-family:Verdana;
font-size:10pt;

}

.ddcolortabs ul{
/* font: normal 11px Arial, Verdana, sans-serif; */
margin:0;
padding:0;
list-style:none;
font-family:Verdana;
font-size:10pt;

}

.ddcolortabs li{
display:inline;
margin:0 2px 0 0;
padding:0;
text-transform:uppercase;
font-family:Verdana;
font-size:10pt;

}


.ddcolortabs a{
float:left;
color: <%=cMenuText%>;
font-weight:bold;
background: <%=cMenuBg%> url(images/color_tabs_left.gif) no-repeat left top;
margin:0 2px 0 0;
padding:0 0 1px 3px;
text-decoration:none;
letter-spacing: 1px;
font-family:Verdana;
font-size:10pt;
font-weight: normal;

}

.ddcolortabs a span{
float:left;
display:block;
background: transparent url(images/color_tabs_right.gif) no-repeat right top;
padding: 6px 15px 6px 15px;
font-weight:bold;
color: <%=cMenuText%>;
font-family:Verdana;
font-size:10pt;
}


.ddcolortabs .selected a span{
float:left;
display:block;
background: <%=cMenuBgAct%> url(images/color_tabs_right.gif) no-repeat right top;  /*transparent*/
padding: 6px 15px 6px 15px;
font-weight:bold;
color: <%=cMenuTextAct%> !important;
font-family:Verdana;
font-size:10pt;
}


.ddcolortabs .hover a span{
text-decoration:none;
float:left;
display:block;
background: <%=cMenuBgHover%> url(images/color_tabs_right.gif) no-repeat right top;  /*transparent*/
padding: 6px 15px 6px 15px;
font-weight:bold;
color: <%=cMenuTextAct%> !important;
font-family:Verdana;
font-size:10pt;
}



.ddcolortabs a span{
float:none;
font-family:Verdana;
font-size:10pt;

}

.ddcolortabs a:hover{
text-decoration:none;
background-color: <%=cMenuBgHover%>;
color: <%=cMenuTextHover%> !important;
font-weight:bold;
font-family:Verdana;
font-size:10pt;
}

.ddcolortabs a:hover span{
text-decoration:none;
background-color: <%=cMenuBgHover%>;
color:<%=cMenuTextHover%> !important;
font-family:Verdana;
font-size:10pt;
}

.ddcolortabs .selected a, #ddcolortabs .selected a span{ /*currently selected tab*/
background-color: <%=cMenuBgAct%>;
color: <%=cMenuTextAct%> !important;
font-weight:bold;
font-family:Verdana;
font-size:10pt;
}

.ddcolortabs .selected a:hover, #ddcolortabs .selected a:hover span{ /*currently selected tab*/
text-decoration:none;
background-color: <%=cMenuBgHover%>;
color: <%=cMenuTextAct%> !important;
font-weight:bold;
font-family:Verdana;
font-size:10pt;
}





.ddcolortabs .hover a, #ddcolortabs .hover a span{ /*currently selected tab*/
text-decoration:none;
background-color: <%=cMenuBgHover%>;
color: <%=cMenuTextAct%> !important;
font-weight:bold;
font-family:Verdana;
font-size:10pt;
}



.ddcolortabsline{
clear: both;
padding: 0;
width: 100%;
height: 8px;
line-height: 8px;
background: <%=cMenuBg%>;
border-top: 1px solid #fff; /*Remove this to remove border between bar and tabs*/
}

/* ######### Style for Drop Down Menu ######### */

.dropmenudiv_a{
position:absolute;
top: 0;
border: 1px solid <%=cMenuBgAct%>; /*THEME CHANGE HERE*/
border-top-width: 2px; /*Top border width. Should match height of .ddcolortabsline above*/
border-bottom-width: 0;
font:normal 12px Arial;
line-height:18px;
z-index:900;
background-color: <%=cSubMenuBg%>;
min-width: 200px;
width: auto;
visibility: hidden;
}


.dropmenudiv_a a{
width: auto;
display: block;
text-indent: 5px;
border-top: 0 solid <%=cMenuBg%>;
border-bottom: 1px solid <%=cMenuBgAct%>; /*THEME CHANGE HERE*/
padding: 2px 0;
text-decoration: none;
color: <%=cSubMenuText%>;
font-size: 12px;
font-weight: normal;

}

* html .dropmenudiv_a a{ /*IE only hack*/
width: 100%;
}

.dropmenudiv_a a:hover{ /*THEME CHANGE HERE*/
text-decoration:none;
background-color: <%=cSubMenuBgHover%>; 
color: <%=cSubMenuTextHover%> !important;
font-size: 12px;
font-weight: normal;

}