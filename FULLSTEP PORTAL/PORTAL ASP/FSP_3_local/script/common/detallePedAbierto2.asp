﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<%

    
    decimalfmt=request.Cookies ("USU_DECIMALFMT")
	thousandfmt=Request.Cookies ("USU_THOUSANFMT")
	precisionfmt=cint(Request.Cookies ("USU_PRECISIONFMT"))
	datefmt=Request.Cookies("USU_DATEFMT")
    i=0
	Idioma = Request("Idioma")
	Idioma = trim(Idioma)
    IdOrden = Request("IdOrden")
    IdLinea = Request("LineaPedido")
    

	set oRaiz=validarUsuario(Idioma,true,false,0)
    CiaComp=oRaiz.Sesion.CiaComp
	Set oLinea = oRaiz.Generar_CLinea()
    oLinea.id=IdLinea   


	CiaComp = oRaiz.Sesion.CiaComp
	CiaCod = oRaiz.Sesion.CiaCod
	'Idiomas
	dim den
	den = devolverTextos(Idioma,136)	

    CiaDen = Request("CiaDen")

    set rs=oLinea.DevolverPedidoAbierto(CiaComp)

%>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><%=title%></title>
    <link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp"/>
    <script src="../common/formatos.js"></script>
    <style>
        th
        {
            width:130px;
            text-align:left;
        }
    </style>
</head>

<body>
    <table align="center" WIDTH="98%" border="0" cellspacing="1" cellpadding="1" style="text-align:left">
        <tr>
            <th class="cabecera">
                <%= den(1)%>
            </th>
            <td>
                <%=rs("ANYO").value & "/" & rs("PEDIDO").value & "/" & rs("ORDEN").value %>
            </td>
        </tr>
        <tr>
            <th class="cabecera">
                <%= den(2)%>
            </th>
            <td>
                <%=HTMLEncode(rs("NOM").value) & " " & HTMLEncode(rs("APE").value) %>
            </td>
        </tr>
        <tr>
            <th class="cabecera">
                <%= den(3)%>
            </th>
            <td>
                <%=visualizacionFecha(rs("FECHA").value,datefmt) %>
            </td>
        </tr>
        <tr>
            <th class="cabecera">
                <%= den(4)%>
            </th>
            <td>
                <%=visualizacionFecha(rs("PED_ABIERTO_FECINI").value,datefmt) %>
            </td>
        </tr>
        <tr>
            <th class="cabecera">
                <%= den(5)%>
            </th>
            <td>
                <%= visualizacionFecha(rs("PED_ABIERTO_FECFIN").value,datefmt) %>
            </td>
        </tr>
        <tr>
            <th class="cabecera">
                <%= den(6)%>
            </th>
            <td>
                <%         
                    
                select case rs("EST").value 
                    case 100 'No vigente
		                ordenEst=   den(13)
                    case 101 'Abierto
                        ordenEst=   den(14)
                    case 102 'Cerrado
                        ordenEst=   den(15)
                    case 103 'Anulado
                        ordenEst=   den(16)
                end select
                response.write ordenEst
                %>
            </td>
        </tr>
        <tr>
            <th class="cabecera">
                <%= den(7)%>
            </th>
            <td>
                <%=formatIntString(rs("NUMLIN").value,3)%>
            </td>
        </tr>
        <tr>
            <th class="cabecera">
                <%= den(8)%>
            </th>
            <td>
                <%
                    dim estado
                    if not isnull(rs("EST_LIN").value) then
                        estado= rs("EST_LIN").value
                    else
                        estado = rs("EST").value 
                    end if
                    select case estado
                        case 100 'No vigente
		                        Response.write(den(13))
                        case 101 'Abierto
                            Response.write(den(14))
                        case 102 'Cerrado
                            Response.write(den(15))
                        case 103 'Anulado
                            Response.write(den(16))
                    end select
                %>
            </td>
        </tr>
        <tr>
            <th class="cabecera">
                <%      if rs("PED_ABIERTO_TIPO").value=2 then
                            Response.Write(den(9))
                        else
                            Response.Write(den(17))
                        end if 
                %>
            </th>
            <td>
                <%
                        if rs("PED_ABIERTO_TIPO").value=2 then
                            Response.Write(visualizacionNumero(rs("IMPORTE_PED").value*rs("CAMBIO").value,decimalfmt,thousandfmt,precisionfmt))
                        else
                            Response.Write(visualizacionNumero(rs("CANT_PED").value,decimalfmt,thousandfmt,precisionfmt))
                        end if  
                %>
            </td>
        </tr>
        <tr>
            <th class="cabecera">
                <%
                        if rs("PED_ABIERTO_TIPO").value=2 then
                            Response.Write(den(10))
                        else
                            Response.Write(den(18))
                        end if 
                %>
            </th>
            <td>
                <%
                    if rs("PED_ABIERTO_TIPO").value=2 then
                        Response.Write(visualizacionNumero(rs("IMPORTE_PED_ABIERTO").value*rs("CAMBIO").value,decimalfmt,thousandfmt,precisionfmt))
                    else
                        Response.Write(visualizacionNumero(rs("CANTIDAD_PED_ABIERTO").value,decimalfmt,thousandfmt,precisionfmt))
                    end if        
                %>
            </td>
        </tr>
        <tr>
            <th class="cabecera">
                <%      
                    if rs("PED_ABIERTO_TIPO").value=2 then
                        Response.Write(den(11))
                    else
                        Response.Write(den(19))
                    end if 
                %>
            </th>
            <td>
                <%
                        if rs("PED_ABIERTO_TIPO").value=2 then
                            Response.Write(rs("MON").value)
                        else
                            Response.Write(rs("UP").value)
                        end if 
                %>
            </td>
        </tr>
        
    </table>
    <br />
    <center><input type="button" class="button Aprov" id="Cerrar" value="<%=Den(12)%>" onclick="javascript:window.close()"/></center>
</body>
</html>
<%

    
 %>