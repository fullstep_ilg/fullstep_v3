﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<%
''' <summary>
''' Muestra la pantalla de error pq la no conformidad a ver esta en proceso
''' </summary>
''' <remarks>Llamada desde: noconformidades\noconformidades11.asp ; Tiempo máximo: 0,2</remarks> 

Idioma=Request.Cookies ("USU_IDIOMA")

if Request("EsCertificado")="1" then
	modulo_idioma = 123
else
	modulo_idioma = 125
end if

dim den
den = devolverTextos(Idioma,modulo_idioma)
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><%=title%></title>
    <meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
    <link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
</head>
<body>

<table width="90%" align="center">
<tr><td class="cabecera negrita" width="40%"><%=den(4)%></td><td class="filaPar"><%=Request("ID")%></td></tr>
<tr><td class="cabecera negrita" width="40%"><%=den(5)%></td><td class="filaPar"><%=Request("Tipo")%></td></tr>
</table>
<p></p>
<table width="90%" align="center" cellpadding="3">
<tr><td width="10%"><img src="../images/Icono_Error_Amarillo_40x40.gif" WIDTH="40" HEIGHT="40"></td><td><p><%=den(12)%></p></td></tr>
</table>
<p></p>
<center><input type="button" class="button" name="cmdCerrar" onclick="window.close()" value="<%=den(11)%>"></center>
</body>
</html>
