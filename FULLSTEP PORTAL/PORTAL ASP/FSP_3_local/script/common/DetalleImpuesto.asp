﻿<%@ Language=VBScript %>
<!--#include file="acceso.asp"-->
<!--#include file="idioma.asp"-->
<!--#include file="formatos.asp"-->

<HTML>
<HEAD>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<style>
    html,body{margin:0; width:100%; height:100%}
</style>
<%
''' <summary>
''' Mostrar la descripci�n de una unidad
''' </summary>
''' <remarks>Llamada desde: seguimpedidos\seguimclt41.asp    
	Idioma = Request("Idioma")
    Id= Request("IdOrden")    
    Impuesto= Request("ImpDen") 
    Moneda= Request("ImpMon")
    mValor= replace(Request("ImpVal"),",",".")
    Valor=CDbl(mValor)
    cuota=Request("cuota")

    decimalfmt=request.Cookies ("USU_DECIMALFMT")
	thousandfmt=Request.Cookies ("USU_THOUSANFMT")
	precisionfmt=cint(Request.Cookies ("USU_PRECISIONFMT"))

	set oRaiz=validarUsuario(Idioma,true,false,0)    
	CiaComp = oRaiz.Sesion.CiaComp
	Den=devolverTextos(idioma,131)
    title=den(16)
	    
	set oOrden = oraiz.Generar_COrden
    'CDs de cabecera
    oOrden.id = clng(Id)
	set ador = oOrden.Devolverlineas(CiaComp,Idioma)
    oOrden.CargarLineasDePedido ador   
%>

<TITLE><%=title%></TITLE>

<SCRIPT>
    function Cerrar() {
        //Cierra la ventana
        window.close()
    }
</SCRIPT>

</HEAD>


<BODY topmargin="0" leftmargin="0">

<div style="clear:both; float:left; width:100%; height:98%;  text-align:center">    
    <div style="clear:both; width:98%; height:85%; text-align:left; overflow:auto;">        
        <div>&nbsp;</div>
        <div style="width:100%">                                                          
            <table align="center" width="100%" border="0" cellspacing="2" cellpadding="1">
                <tr class="cabecera">
                    <td colspan="2" style="font-weight:bold;"><%=Den(11)%>&nbsp;<%=Impuesto%>&nbsp;<%=Valor%>%</td>
                </tr>                 
                <% ImpTotal=0
                
                oOrden.CargarCostesDescuentos CiaComp 
                oOrden.cargarImpuestos Idioma, CiaComp
                ImpCoste=0
                ImpCoste = oOrden.getCosteByTipoImpuesto(Impuesto, Valor)
                ImpTotal = ImpTotal + ImpCoste   
                If ImpCoste>0 then
                %>
                <tr>
                    <td class="filaImpar"><%=Den(5)%></td>
                    <td class="filaImpar" align="right"><%=visualizacionNumero(ImpCoste,decimalfmt,thousandfmt,precisionfmt)%>&nbsp;<%=Moneda%></td>
                </tr>
                <%end if
                ImpBruto=0
                for each oLinea in oOrden.LineasPedido
                    ImpBruto = oLinea.getBrutoDescuentoByTipoImpuesto(Impuesto, Valor, oOrden, CiaComp, Idioma)
                    IdLinea = oLinea.Num
                    ImpTotal = ImpTotal + ImpBruto  
                    If ImpBruto>0 then
                    %>
                    <tr>
                        <td class="filaImpar"><%=Den(2)%>&nbsp;<%=Den(12)%>&nbsp;<%=IdLinea%>&nbsp;(<%=Den(13)%>)</td>
                        <td class="filaImpar" align="right"><%=visualizacionNumero(ImpBruto,decimalfmt,thousandfmt,precisionfmt)%>&nbsp;<%=Moneda%></td>
                    </tr>
                    <%end if
                next%>
                <% ImpCoste=0
                for each oLinea in oOrden.LineasPedido
                    
                    ImpCoste = oLinea.getCosteByTipoImpuesto(Impuesto, Valor)
                    IdLinea = oLinea.Num
                    ImpTotal = ImpTotal + ImpCoste                                             
                    If ImpCoste>0 then
                    %>
                    <tr>
                        <td class="filaImpar"><%=Den(5)%>&nbsp;<%=Den(12)%>&nbsp;<%=IdLinea%></td>
                        <td class="filaImpar" align="right"><%=visualizacionNumero(ImpCoste,decimalfmt,thousandfmt,precisionfmt)%>&nbsp;<%=Moneda%></td>
                    </tr>
                    <%end if
                next%>
                    <tr style="font-weight:bold;">
                        <td class="filaImpar"><%=Ucase(Den(14))%></td>
                        <td class="filaImpar" align="right"><%=visualizacionNumero(ImpTotal,decimalfmt,thousandfmt,precisionfmt)%>&nbsp;<%=Moneda%></td>
                    </tr> 
                    <tr style="color:red">
                        <td style="font-weight:bold;"><%=Den(15)%>&nbsp;<%=Impuesto%>&nbsp;<%=Valor%>%</td>
                        <td align="right"><%=cuota%>&nbsp;<%=Moneda%></td>
                    </tr>                                              
            </table>                                                 
        </div>
        <div>&nbsp;</div>      
    </div>       
    <div style="clear:both; position:relative; width:100%; height:10%; text-align:center;">
        <div>&nbsp;</div>	                   
        <INPUT TYPE="button" class="button" ID=btnCerrar value="<%=Den(10)%>" onclick="Cerrar()">
        <div>&nbsp;</div>
    </div>        
</div>
</BODY>
</HTML>

