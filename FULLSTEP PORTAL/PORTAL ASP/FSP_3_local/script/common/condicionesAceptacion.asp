﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">

<%@ language="VBScript" %>
<!--#include file="idioma.asp"--> 
<%    
    Idioma = Request("Idioma")
    Idioma = trim(Idioma)
    if Idioma="" then Idioma = "SPA"
    dim Textos
    Textos=devolverTextos(Idioma,1)

    'Obtener las últimas condiciones de aceptación
    dim oRaiz
    dim idCondAcep
    dim sCondAcep
    idCondAcep=0
    sCondAcep=""
    set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")    
    if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	    set oRaiz = nothing	    
	    Response.Redirect Application ("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	    Response.End 
	else
        set rsCond=oRaiz.DevolverUltVersionCondicionesAceptacion(request("txtCIA"),request("txtUSU"),Idioma)
        If Not rsCond Is Nothing Then
            If rsCond.RecordCount > 0 Then 
                idCondAcep=rsCond("ID")
                sCondAcep=rsCond("TEXTO")
            end if
        end if
    end if  
        
    if Request.QueryString("post") = "yes" then          
        oRaiz.ActualizarCondicionesAceptacionUsuario Session.Contents.Item("txtCIA"),Session.Contents.Item("txtUSU"),idCondAcep                
        Session.Contents.Item("DesdeCondiciones")=1 
        'Redirigir a la página llamante
        Response.Redirect Request.ServerVariables("HTTP_REFERER")   
    else
        'Meto los datos del login introducidos por el usuario en Session.Contetnts para que al aceptar las condiciones y redirigirlo a la página de login (login.asp) tenga estos datos
        Session.Contents.Item("txtCIA") = request("txtCIA") 
        Session.Contents.Item("txtUSU") = request("txtUSU") 
        Session.Contents.Item("txtPWD") = request("txtPWD") 
    end if    
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
    <style type="text/css">
        .condiciones
        {
            border-color:grey;
            border-style:solid; 
            border-width:thin;
            background-color: beige;            
        }
        .titulo{            
            font-weight:bold;
            font-size:xx-large;
            padding-top :1em;
            padding-bottom :1em;
            display:inline-block;
            color:black;            
        }
        .fuente{
            font-family: Arial, Helvetica, sans-serif;
            font-size:small; 
            color:black;           
        }        
        .fondo{
            background-color: whitesmoke;
        }
    </style>   
</head>
<body bgcolor="FFFFFF">    
    <form name="Condiciones" id="Condiciones" method="post" action="<%=Application("RUTASEGURA")%>script/common/condicionesAceptacion.asp?post=yes">
        <div style="width:60%; text-align:left; position:absolute; left:10%;" class="fuente fondo">
            <div id="divTitulo" style="text-align:center;">
                <span class="titulo"><%=Textos(13)%></span>
            </div>
            <div style="width:80%; display:table; margin:0 auto; padding-top:1em;" id="divCondicionesAceptacion" class="condiciones">
                <%response.write sCondAcep%>
	        </div>	
            <div id="divBoton" style="text-align:center; padding:2em;">
                <input type="submit" value="<%=Textos(14)%>"/>                
            </div>
        </div>        
    </form>
</body>
</html>