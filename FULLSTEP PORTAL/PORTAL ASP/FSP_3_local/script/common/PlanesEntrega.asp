﻿<%@  language="VBScript" %>
<!--#include file="acceso.asp"-->
<!--#include file="idioma.asp"-->
<!--#include file="formatos.asp"-->

<html>
<head>
    <title><%=title%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
    <style>
        html, body {
            margin: 0;
            width: 100%;
            height: 100%;
        }
    </style>
    <%
''' <summary>
''' Mostrar la descripción de una unidad
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos41.asp   

	Idioma = Request("Idioma")
    Anyo= Request("Anyo")
    NumPedido= Request("Pedido")
    NumOrden= Request("Orden")
	IdLinea=Request("IdLinea")
    CodArt= Request("CodArt")
    DenArt= Request("DenArt")
    Uni= Request("Uni")
    Mon=Request("Mon")
    TipoRecepcion=Request("TipoRecepcion")
    datefmt=Request.Cookies("USU_DATEFMT")
	
	set oRaiz=validarUsuario(Idioma,true,false,0)    
	CiaComp = oRaiz.Sesion.CiaComp
	Den=devolverTextos(idioma,130)
		
	set oLP = oraiz.Generar_CLinea
    oLP.id=IdLinea
	set ador = oLP.DevolverPlanesEntrega(CiaComp)
    %>

    <title><%=title%></title>

    <script>
function Cerrar()
{
	//Cierra la ventana
	window.close()
}

    </script>
</head>

<body topmargin="0" leftmargin="0">
    <div style="clear: both; float: left; width: 100%; height: 100%;">
        <div style="clear: both; width: 100%; height: 80%; overflow: auto;">
            <table class="normal" align="center" width="98%" border="0" cellspacing="1" cellpadding="1">
                <tr>
                    <td colspan="2">
                        <table width="100%" cellspacing="0">
                            <tr>
                                <td width="20%"><b><%=den(2)%>:</b></td>
                                <td width="80%"><%=Anyo%>/<%=NumPedido%>/<%=NumOrden%></td>
                            </tr>
                            <tr>
                                <td width="20%"><b><%=den(3)%>:</b></td>
                                <td width="80%"><%=CodArt%> - <%=DenArt%></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><b><%=den(1)%></b></td>
                </tr>
                <tr>
                    <td width="35%" class="cabecera"><%=den(4)%></td>
                    <%if TipoRecepcion=0 then %>
                    <td width="35%" class="cabecera"><%=den(5) & " " & Uni%></td>
                    <%else %>
                    <td width="35%" class="cabecera"><%=den(7) & " " & Mon%></td>
                    <%end if %>
                </tr>

                <%while not ador.eof%>
                <tr>
                    <td class="filaPar"><%=VisualizacionFecha(ador("FECHA").value,datefmt)%></td>
                    <%if TipoRecepcion=0 then %>
                    <td class="filaPar"><%=ador("CANTIDAD").value%></td>
                    <%else %>
                    <td class="filaPar"><%=ador("IMPORTE").value%></td>
                    <%end if %>
                </tr>
                <%ador.movenext%>
                <%wend%>
            </table>
        </div>
        <div style="clear: both; position: relative; width: 100%; height: 15%; text-align: center;">
            <div>&nbsp;</div>
            <input type="button" class="button" id="btnCerrar" value="<%=Den(6)%>" onclick="Cerrar()">
            <div>&nbsp;</div>
        </div>
    </div>
</body>
</html>