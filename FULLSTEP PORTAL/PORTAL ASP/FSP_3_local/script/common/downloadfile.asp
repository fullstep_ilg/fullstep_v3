﻿<%@ Language=VBScript %>

<!--#include file="../common/acceso.asp"-->
<%
''' <summary>
''' Descarga el fichero
''' </summary>
''' <remarks>Llamada desde: common\downloadhidden.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies ("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,true,false,0)

Response.ContentType="application/octet-stream"

Response.AddHeader "content-disposition", "attachment; filename=""" & request("fname")  & """"


Response.BinaryWrite(oRaiz.downloadFile(oRaiz.Sesion.CiaComp,request("src"),false))


set oraiz = nothing 

%>
<title><%=title%></title>

