﻿<%@ Language=VBScript %>
<!--#include file="acceso.asp"-->
<!--#include file="idioma.asp"-->
<!--#include file="colores.asp"-->
<!--#include file="formatos.asp"-->
<%
''' <summary>
''' Mostrar la descripción de un atributo q es texto largo
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\cumpoferta.asp ; Tiempo máximo: 0,2</remarks>

	Idioma = Request.Cookies("USU_IDIOMA")
	texto=Request("texto")
	dim den
	den = devolvertextos(Idioma,92)
%>

<HTML>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">

<TITLE><%=title%></TITLE>

<SCRIPT>

function Cerrar()
{
	window.close()
}

</SCRIPT>

</HEAD>
<BODY topmargin="10" leftmargin="0">
<TABLE class="normal" align="center" WIDTH="98%" border="0" cellspacing="1" cellpadding="1">
	<TR>
		<TD width="5%" class="cabecera"><%=den(2)%></td>
		<TD class="filaPar" ><TEXTAREA NAME="comments" COLS=80 ROWS=6><%=HTMLEncode(texto)%></TEXTAREA></td>
	</TR>
	<TR>
		<TD height=40px align=center COLSPAN=2>
			<INPUT TYPE="button" class="button" ID=Cerrar LANGUAGE=javascript class="Aprov" value="<%=Den(19)%>" onclick="Cerrar()">
		</TD>
	</TR>
	
</TABLE>	
</BODY>
</HTML>

