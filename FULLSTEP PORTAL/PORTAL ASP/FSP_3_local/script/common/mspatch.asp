﻿<%@ Language=VBScript %>
<%
''' <summary>
''' Muestra la pantalla de error pq "Esta versión no le permite descargarse archivos desde el portal"
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos11.asp pedidos\pedidos11dir.asp pedidos\pedidos41.asp seguimpedidos\seguimclt.asp  seguimpedidos\seguimclt41.asp
''' solicitudesoferta\descargaespecificserver.asp   solicitudesoferta\excel.asp     solicitudesoferta\js\adjunto.asp    solicitudesoferta\js\especificacion.asp
''' ; Tiempo máximo: 0,2</remarks> 
%>
<HTML>
<HEAD>
<% 
    Idioma = Request.Cookies("USU_IDIOMA")
    
%>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<title><%=title%></title>
</HEAD>
<BODY>
<%if Request.Cookies("USU_IDIOMA")="SPA" then%>
<p>
Su navegador es el Microsoft Internet Explorer 5.5 SP1 
</p>
<p>
Esta versión no le permite descargarse archivos desde el portal 
</p>
<P>
Haga click en el enlace para acceder a la actualización publicada por Microsoft.
</p>

<%else%>
<p>
Your current browser is Internet Explorer 5.5 SP1
</p>
<p>
This version does not allow to download files from the site
</p>
<P>
Click on the link if you wish to install the service pack published by Microsoft
</p>
<%end if%>
<P><a style="font-size:12px" href="http://www.microsoft.com/windows/ie/download/critical/q299618/default.asp " target="_blank">
Security Update, May 24, 2001</a></P>

<P align=center><a href="#" onclick="window.close()">
<%if Request.Cookies("USU_IDIOMA")="SPA" then%>
Cerrar
<%else%>
Close
<%end if%>
</a></P>

</BODY>
</HTML>
