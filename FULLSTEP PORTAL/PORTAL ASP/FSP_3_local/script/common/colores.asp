<%

leftLogout ="90%"
topLogout=15

leftNotifCN ="88%"
topNotifCN="15px"

cFONDOLOGOUT = "#FF0000"

cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "white"

cBORDEINPUT = "#b2b2b2"
cBORDECAJETIN = "#b2b2b2"

cColorLinea1Down="blue"
cColorLinea2Down="red"

cLNKARBOL = "black"
cMOARBOL = "#cccccc"

cFONDONOOFERTAR = "white"
cTEXTONOOFERTAR = "black"
cFONDOCABNOOFERTAR = "white"


cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#274f76"


fMENUVERTICAL = "verdana"
MENUFONTSIZE = 7

cFONDOVERDE="darkgreen"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="gray"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="blue"

IF UCase(APPLICATION("NOMPORTAL"))=UCase("uf") then

cFONDOLOGOUT = "yellow"

cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "black"
end if


IF UCase(APPLICATION("NOMPORTAL"))=UCase("pri") then

' BOTON SALIR
cFONDOLOGOUT = "#800000"

cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "white"

cTEXTOWHITE="#444444"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

'color de las filas pares
cFilaPar = "#d7dfed"
cFilaPar = "white"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#d1aaaa"
cFilaImpar = "#F7F7F7"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#8ba21c"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#8ba21c"
cH2="#000000"
cH3="#000000"
cH4="#000000"
cH5="#000000"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#8ba21c"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#000000"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES = "#e8e8e8"

'Color de fondo de las opciones del men� horizontal activada

cHACT = "#8ba21c"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES = "#8ba21c"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#000000"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#f2f2f2"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#989898"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#8ba21c"
cVTEXTOOVER = "#ffffff"


'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#C6C7C6"


'Color del borde de la barra de progreso
cBORDEPROGRESO="darkblue"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="blue"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = cH1


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#8ba21c"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#8ba21c"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#f2f2f2"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#8ba21c"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#f5f5f5"

cPESTANYADES = "#f5f5f5"
cPESTANYAACT = "#989898"


cCELSUBASTAABIERTA = "darkseagreen"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#e8e8e8"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#8ba21c"
cDAYCOLOR = "#8ba21c"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("full") then

cFONDOLOGOUT = "#6d93a2"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "white"

'color de las filas pares
cFilaPar = "#cccccc"
cTextoFilaPar = "#666666"

'color de la filas impares
cFilaImPar = "#999999"
cTextoFilaImPar = "#ffffff"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#6d93a2"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#274f76"
cH2="#000000"
cH3="#000000"
cH4="#000000"
cH5="#000000"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#d04b3c"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#6d93a2"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#999999"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#999999"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#6d93a2"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#6d93a2"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#990000"
cVTEXTOOVER = "#990000"


'Color del borde de la barra de progreso
cBORDEPROGRESO="darkblue"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="blue"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "blue"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#840000"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#999999"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#6d93a2"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"


elseif UCase(APPLICATION("NOMPORTAL"))=UCase("fic") then

' BOTON SALIR
cFONDOLOGOUT = "#b5a660"

cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "white"

'color de las filas pares
cFilaPar = "#ffffff"
cTextoFilaPar = "#5A5959"

'color de la filas impares
cFilaImPar = "#EAE8E8"
cTextoFilaImPar = "#5A5959"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#cccccc"

'color del texto de las cabeceras
cTextoCabecera = "#000000"

'color del t�tulo de la p�gina
'cH1="#B1B176"
ch1="#333333"
cH2="#333333"
cH3="#333333"
cH4="#333333"
cH5="#333333"

'color de los enlaces normales
cLINK = "#848284"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#838335"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "838335"

'color de los textos
cTextoNormal="#5A5959"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#000000"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#dcceb3"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#dcceb3"

'color de los links en el men� horizontal
cMENULINK = "#848284"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#000000"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#000000"

'Color de fondo de las opciones del men� vertical
cVDES="#dcceb3"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#F4E5C8"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#000000"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#333333"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#999999"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#999999"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#DCCEB3"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#DCCEB3"
cFNTAPARTOFE = "black"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#DCCEB3"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#DCCEB3"

cPESTANYADES = "#E5E5B5"
cPESTANYAACT = "#DCCEB3"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#DCCEB3"


cCELSUBASTAABIERTA = "#AFCFB1"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"


cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#DCCEB3"
cDAYCOLOR = "#848284"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"


elseif UCase(APPLICATION("NOMPORTAL"))=UCase("fulldemo") then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#AB0020"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#AB0020"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#AB0020"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#AB0020"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#cccccc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#AB0020"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#AB0020"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"


elseif UCase(APPLICATION("NOMPORTAL"))=UCase("ori")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#435c98"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#ffffff"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#435c98"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#435c98"
cH2="#435c98"
cH3="#666666"
cH4="#666666"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#435c98"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#ebebeb"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#435c98"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#435c98"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#435c98"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#435c98"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#ffffff"
cVTEXTOOVER = "#f39a06"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#435c98"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"


elseif UCase(APPLICATION("NOMPORTAL"))=UCase("fsn")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#808080"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#808080"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#808080"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#808080"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#FFFFFF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#FFFFFF"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#808080"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("com")then

' BOTON SALIR
cFONDOLOGOUT = "#b5a660"

cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "white"

'color de las filas pares
cFilaPar = "#ffffff"
cTextoFilaPar = "#5A5959"

'color de la filas impares
cFilaImPar = "#e0e0b6"
cTextoFilaImPar = "#5A5959"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#b5a660"

'color del texto de las cabeceras
cTextoCabecera = "#ffffff"

'color del t�tulo de la p�gina
'cH1="#B1B176"
ch1="#333333"
cH2="#333333"
cH3="#333333"
cH4="#333333"
cH5="#333333"

'color de los enlaces normales
cLINK = "#848484"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#838335"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "838335"

'color de los textos
cTextoNormal="#5A5959"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#000000"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#b5a660"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#b5a660"

'color de los links en el men� horizontal
cMENULINK = "#ffffff"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#000000"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#000000"

'Color de fondo de las opciones del men� vertical
cVDES="#cccc99"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#b5a660"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#000000"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#333333"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#999999"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#999999"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#DCCEB3"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#b5a660"
cFNTAPARTOFE = "black"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#b5a660"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#b5a660"

cPESTANYADES = "#E5E5B5"
cPESTANYAACT = "#b5a660"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#b5a660"


cCELSUBASTAABIERTA = "#AFCFB1"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"


cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#b5a660"
cDAYCOLOR = "#848284"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("sar")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#13878C"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#808080"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#808080"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
cLINK = "#000000"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#13878C"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#13878C"

'color de los links en el men� horizontal
cMENULINK = "#13878C"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#FFFFFF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#E6F5F6"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#13878C"
cVTEXTOOVER = "#13878C"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("aer")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#004253"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#808080"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#808080"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
cLINK = "#000000"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#005187"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#005187"

'color de los links en el men� horizontal
cMENULINK = "#005187"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#FFFFFF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#004573"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#005187"
cVTEXTOOVER = "#ffffff"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("sca")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#004253"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#808080"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#808080"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
cLINK = "#000000"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#009ddd"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#009ddd"

'color de los links en el men� horizontal
cMENULINK = "#009DDD"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#FFFFFF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#009ddd"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#1966B0"
cVTEXTOOVER = "#ffffff"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#1966B0"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("ibc")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#004253"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#808080"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#808080"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
cLINK = "#000000"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#0062AE"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#0062AE"

'color de los links en el men� horizontal
cMENULINK = "#0062AE"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#FFFFFF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#0062AE"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000"
cVTEXTOOVER = "#ffffff"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#1966B0"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"


elseif UCase(APPLICATION("NOMPORTAL"))=UCase("pla")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#808080"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#808080"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#808080"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#808080"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#FFFFFF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#FFFFFF"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#808080"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("ase")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#808080"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#808080"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#808080"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#808080"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#FFFFFF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#FFFFFF"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#808080"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("uti")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#808080"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#808080"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#AB0020"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#AB0020"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#CCCCCC"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#AB0020"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#666666"
cVTEXTOOVER = "#FFFFFF"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("ono")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#6c1f7f"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#ffffff"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#ffffff"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#6c1f7f"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#333333"
cH2="#333333"
cH3="#333333"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#6c1f7f"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#232323"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#525252"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#525252"

'color de los links en el men� horizontal
cMENULINK = "#white"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#FFFFFF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#FFFFFF"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#808080"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("fer")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#808080"

'color de las filas pares
cFilaPar = "#ffffff"
cTextoFilaPar = "#666666"

'color de la filas impares
cFilaImPar = "#f8f8f8"
cTextoFilaImPar = "#666666"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#ffe6a5"

'color del texto de las cabeceras
cTextoCabecera = "#666666"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#335588"
cLINK = "#335588"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#de7103"

'color de los enlaces a ayuda
cHELPLINK = "#666666"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "#666666"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#e3e2e2"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#fec10e"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#fec10e"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "black"

'Color de fondo de las opciones del men� vertical
cVDES="#ffe6a5"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#ffe6a5"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#808080"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "#444444"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#e3e2e2"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("ctr")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#808080"

'color de las filas pares
cFilaPar = "#ffffff"
cTextoFilaPar = "#666666"

'color de la filas impares
cFilaImPar = "#f8f8f8"
cTextoFilaImPar = "#666666"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#ffe6a5"

'color del texto de las cabeceras
cTextoCabecera = "#666666"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#000000"
cLINK = "#000000"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#de7103"

'color de los enlaces a ayuda
cHELPLINK = "#666666"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "#666666"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#e3e2e2"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#edd632"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#225542"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "black"

'Color de fondo de las opciones del men� vertical
cVDES="#225542"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#efdc57"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#ffffff"
cVTEXTOOVER = "#ffffff"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "#444444"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#e3e2e2"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("lcx")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#009ad8"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#white"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#ffffff"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#009ad8"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#009ad8"
cH2="#4C4C4C"
cH3="#4C4C4C"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
'cLINK = "#335588"
cLINK = "#009ad8"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#232323"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#009ad8"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#009ad8"

'color de los links en el men� horizontal
cMENULINK = "#ffffff"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#ffffff"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#ffffff"

'Color de fondo de las opciones del men� vertical
cVDES="#eeeeee"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#ffffff"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#808080"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#009ad8"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#009ad8"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#009ad8"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#009ad8"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#b8b8b8"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "4c4c4c"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("hre")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#009ad8"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#white"

'color de las filas pares
cFilaPar = "#ffffff"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#EDEBEB"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#0a94d6"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="##008ECE"
cH2="#4C4C4C"
cH3="#4C4C4C"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
'cLINK = "#335588"
cLINK = "#0a94d6"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#0A94D6"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#46BC98"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#46BC98"

'color de los links en el men� horizontal
cMENULINK = "#FFFFFF"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#000000"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#ffffff"

'Color de fondo de las opciones del men� vertical
cVDES="#0A94D6"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#46BC98"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#FFFFFF"
cVTEXTOOVER = "#FFFFFF"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#009ad8"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#009ad8"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#009ad8"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#009ad8"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#b8b8b8"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "4c4c4c"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("ilu")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#0077CB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#white"

'color de las filas pares
cFilaPar = "#ffffff"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#EDEBEB"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#0077CB"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="##008ECE"
cH2="#4C4C4C"
cH3="#4C4C4C"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
'cLINK = "#335588"
cLINK = "#0077CB"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#ffffff"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#0077CB"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#0077CB"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#000000"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#ffffff"

'Color de fondo de las opciones del men� vertical
cVDES="#ffffff"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#0077CB"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000"
cVTEXTOOVER = "#FFFFFF"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0077CB"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#0077CB"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#0077CB"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#0077CB"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#b8b8b8"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "4c4c4c"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#0077CB"

cPESTANYADES = "#1966B0"
cPESTANYAACT = "#0077CB"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#0077CB"
cDAYCOLOR = "#0077CB"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("mut")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#009ad8"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#white"

'color de las filas pares
cFilaPar = "#ffffff"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#EDEBEB"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#0a94d6"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="##008ECE"
cH2="#4C4C4C"
cH3="#4C4C4C"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
'cLINK = "#335588"
cLINK = "#0a94d6"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#3645b0"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#dd7009"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#dd7009"

'color de los links en el men� horizontal
cMENULINK = "#FFFFFF"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#ffffff"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#ffffff"

'Color de fondo de las opciones del men� vertical
cVDES="#3645b0"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#dd7009"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#FFFFFF"
cVTEXTOOVER = "#FFFFFF"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#009ad8"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#009ad8"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#009ad8"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#009ad8"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#b8b8b8"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "4c4c4c"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("sam")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#009ad8"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#white"

'color de las filas pares
cFilaPar = "#ffffff"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#EDEBEB"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#1428a0"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="##008ECE"
cH2="#4C4C4C"
cH3="#4C4C4C"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
'cLINK = "#335588"
cLINK = "#0a94d6"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#1428a0"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#1428a0"

'color de los links en el men� horizontal
cMENULINK = "#1428a0"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#ffffff"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#ffffff"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#1428a0"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#1428a0"
cVTEXTOOVER = "#FFFFFF"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#1428a0"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#1428a0"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#1428a0"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#1428a0"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#b8b8b8"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "4c4c4c"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#1428a0"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#1428a0"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("sod")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#009ad8"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#white"

'color de las filas pares
cFilaPar = "#ffffff"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#EDEBEB"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#035783"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="##008ECE"
cH2="#4C4C4C"
cH3="#4C4C4C"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
'cLINK = "#335588"
cLINK = "#035783"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#035783"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#436c81"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#436c81"

'color de los links en el men� horizontal
cMENULINK = "#FFFFFF"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#ffffff"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#ffffff"

'Color de fondo de las opciones del men� vertical
cVDES="#035783"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#436c81"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#FFFFFF"
cVTEXTOOVER = "#FFFFFF"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#035783"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#035783"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#035783"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#035783"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#b8b8b8"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "4c4c4c"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#035783"
cDAYCOLOR = "#035783"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("ber")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#009ad8"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#white"

'color de las filas pares
cFilaPar = "#ffffff"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#EDEBEB"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#10295e"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="##008ECE"
cH2="#4C4C4C"
cH3="#4C4C4C"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
'cLINK = "#335588"
cLINK = "#10295e"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#10295e"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#fd5653"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#10295e"

'color de los links en el men� horizontal
cMENULINK = "#FFFFFF"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#ffffff"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#ffffff"

'Color de fondo de las opciones del men� vertical
cVDES="#10295e"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#fd5653"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#FFFFFF"
cVTEXTOOVER = "#FFFFFF"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#035783"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#035783"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#035783"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#035783"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#b8b8b8"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "4c4c4c"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#035783"
cDAYCOLOR = "#035783"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("sbfe")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#009ad8"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#white"

'color de las filas pares
cFilaPar = "#ffffff"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#EDEBEB"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#10295e"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="##008ECE"
cH2="#4C4C4C"
cH3="#4C4C4C"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
'cLINK = "#335588"
cLINK = "#10295e"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#6db5cb"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#2a97bc"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#2a97bc"

'color de los links en el men� horizontal
cMENULINK = "#FFFFFF"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#ffffff"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#ffffff"

'Color de fondo de las opciones del men� vertical
cVDES="#6db5cb"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#2a97bc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#FFFFFF"
cVTEXTOOVER = "#FFFFFF"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#6db5cb"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#6db5cb"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#6db5cb"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#6db5cb"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#2a97bc"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#b8b8b8"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "4c4c4c"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#6db5cb"

cPESTANYADES = "#6db5cb"
cPESTANYAACT = "#2a97bc"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#6db5cb"
cDAYCOLOR = "#6db5cb"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("prored")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#009ad8"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#white"

'color de las filas pares
cFilaPar = "#ffffff"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#EDEBEB"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#10295e"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="##008ECE"
cH2="#4C4C4C"
cH3="#4C4C4C"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
'cLINK = "#335588"
cLINK = "#10295e"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#F4F4F4"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#c4c4c4"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#c4c4c4"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#ffffff"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#ffffff"

'Color de fondo de las opciones del men� vertical
cVDES="#F4F4F4"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#c4c4c4"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#ffffff"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#6db5cb"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#6db5cb"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#6db5cb"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#6db5cb"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#2a97bc"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#b8b8b8"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "4c4c4c"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#6db5cb"

cPESTANYADES = "#6db5cb"
cPESTANYAACT = "#2a97bc"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#6db5cb"
cDAYCOLOR = "#6db5cb"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("lop")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#009ad8"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#white"

'color de las filas pares
cFilaPar = "#ffffff"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#EDEBEB"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#10295e"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="##008ECE"
cH2="#4C4C4C"
cH3="#4C4C4C"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
'cLINK = "#335588"
cLINK = "#10295e"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#032950"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#182D4D"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#182D4D"

'color de los links en el men� horizontal
cMENULINK = "#ffffff"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#ffffff"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#ffffff"

'Color de fondo de las opciones del men� vertical
cVDES="#F4F4F4"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#c4c4c4"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#ffffff"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#6db5cb"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#6db5cb"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#6db5cb"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#6db5cb"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#2a97bc"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#b8b8b8"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "4c4c4c"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#6db5cb"

cPESTANYADES = "#6db5cb"
cPESTANYAACT = "#2a97bc"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#6db5cb"
cDAYCOLOR = "#6db5cb"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("pes")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#009ad8"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#white"

'color de las filas pares
cFilaPar = "#ffffff"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#EDEBEB"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#D6001C"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="##008ECE"
cH2="#4C4C4C"
cH3="#4C4C4C"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
'cLINK = "#D6001C"
cLINK = "#D6001C"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#D6001C"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#F5F5F5"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#D6001C"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#D6001C"

'color de los links en el men� horizontal
cMENULINK = "#666666"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#ffffff"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#ffffff"

'Color de fondo de las opciones del men� vertical
cVDES="#F5F5F5"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#D6001C"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#666666"
cVTEXTOOVER = "#FFFFFF"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#6db5cb"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#6db5cb"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#6db5cb"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#6db5cb"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#2a97bc"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#b8b8b8"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "4c4c4c"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#6db5cb"

cPESTANYADES = "#6db5cb"
cPESTANYAACT = "#2a97bc"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#6db5cb"
cDAYCOLOR = "#6db5cb"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"


elseif UCase(APPLICATION("NOMPORTAL"))=UCase("cac")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#fce160"
cBORDELOGOUT = "#fce160"
cTEXTOLOGOUT = "#512107"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#ffffff"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#fce160"

'color del texto de las cabeceras
cTextoCabecera = "#512107"

'color del t�tulo de la p�gina
cH1="#512107"
cH2="#4C4C4C"
cH3="#4C4C4C"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
'cLINK = "#335588"
cLINK = "#512107"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#512107"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#232323"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#512107"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#512107"

'color de los links en el men� horizontal
cMENULINK = "#ffffff"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#ffffff"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#ffffff"

'Color de fondo de las opciones del men� vertical
cVDES="#eeeeee"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#ffffff"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#808080"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#512107"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#512107"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#512107"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#512107"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#b8b8b8"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "4c4c4c"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"


elseif UCase(APPLICATION("NOMPORTAL"))=UCase("afi")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#808080"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#808080"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#808080"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#808080"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#FFFFFF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#FFFFFF"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#808080"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("ahi")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#808080"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#808080"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#808080"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#808080"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#FFFFFF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#FFFFFF"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#808080"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"



elseif UCase(APPLICATION("NOMPORTAL"))=UCase("lid")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#808080"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#808080"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#808080"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#808080"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#FFFFFF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#FFFFFF"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#808080"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"


elseif UCase(APPLICATION("NOMPORTAL"))=UCase("afe")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#808080"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#808080"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#808080"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#808080"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#FFFFFF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#FFFFFF"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#808080"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("avn")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#808080"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#808080"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#808080"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#808080"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#FFFFFF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#FFFFFF"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#808080"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("ppl")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#E10E49"

cFNTOFESINENVIAR = "#E10E49"
cFNTOFESINGUARDAR = "#E10E49"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#E10E49"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#E10E49"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#E10E49"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#E10E49"

'color de los textos
cTextoNormal="#675c53"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#d9d9d9"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#E10E49"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#E10E49"

'color de los links en el men� horizontal
cMENULINK = "#675C53"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#d9d9d9"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#d9d9d9"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#E10E49"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#E10E49"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#E10E49"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#E10E49"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#E10E49"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#E10E49"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

    '***************************************************************************************************
    '***************************************************************************************************
    '***************************************************************************************************
    
elseif UCase(APPLICATION("NOMPORTAL"))=UCase("ged")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#004a93"

cFNTOFESINENVIAR = "#004a93"
cFNTOFESINGUARDAR = "#004a93"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#004a93"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#004a93"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#004a93"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#004a93"

'color de los textos
cTextoNormal="#004a93"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#004a93"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#004a93"

'color de los links en el men� horizontal
cMENULINK = "#004a93"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#cccccc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#004a93"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#004a93"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#004a93"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#004a93"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#004a93"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#004a93"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"


    '***************************************************************************************************
    '***************************************************************************************************
elseif UCase(APPLICATION("NOMPORTAL"))=UCase("cor")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#AB0020"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#AB0020"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#AB0020"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#AB0020"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#cccccc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#AB0020"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#AB0020"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("DES")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#AB0020"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#AB0020"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#AB0020"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#AB0020"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#cccccc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#AB0020"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#AB0020"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("PYM")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#AB0020"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#AB0020"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#AB0020"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#AB0020"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#cccccc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#AB0020"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#AB0020"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("zgz")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#C0162D"

cFNTOFESINENVIAR = "#C0162D"
cFNTOFESINGUARDAR = "#C0162D"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#C0162D"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#C0162D"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#C0162D"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#C0162D"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#C0162D"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#C0162D"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#cccccc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#C0162D"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#C0162D"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#C0162D"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#C0162D"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#C0162D"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#C0162D"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("mor")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#C0162D"

cFNTOFESINENVIAR = "#1D3E6B"
cFNTOFESINGUARDAR = "#1D3E6B"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#1D3E6B"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#1D3E6B"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#1D3E6B"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#1D3E6B"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#1D3E6B"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#1D3E6B"

'color de los links en el men� horizontal
cMENULINK = "#1D3E6B"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#cccccc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#1D3E6B"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#1D3E6B"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#1D3E6B"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#1D3E6B"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#1D3E6B"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#1D3E6B"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("tgl")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#033443"

cFNTOFESINENVIAR = "#588E9E"
cFNTOFESINGUARDAR = "#588E9E"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#588E9E"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#588E9E"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "##033443"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#588E9E"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#588E9E"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#588E9E"

'color de los links en el men� horizontal
cMENULINK = "#1e1d1d"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#cccccc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#033443"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#588E9E"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#588E9E"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#588E9E"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#588E9E"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#588E9E"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("cos")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#033443"

cFNTOFESINENVIAR = "#606470"
cFNTOFESINGUARDAR = "#606470"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#606470"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#606470"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "##033443"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#606470"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#606470"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#606470"

'color de los links en el men� horizontal
cMENULINK = "#1e1d1d"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#cccccc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#033443"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#606470"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#606470"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#606470"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#606470"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#606470"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#606470"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("aje")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#AB0020"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#AB0020"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#AB0020"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#AB0020"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#cccccc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#AB0020"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#AB0020"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("des")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#AB0020"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#AB0020"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#AB0020"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#AB0020"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#cccccc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#AB0020"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#AB0020"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"


elseif UCase(APPLICATION("NOMPORTAL"))=UCase("cip")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#AB0020"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#AB0020"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#AB0020"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#AB0020"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#cccccc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#AB0020"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#AB0020"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("usdemo")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#AB0020"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#AB0020"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#AB0020"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#AB0020"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#AB0020"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#cccccc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#AB0020"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#AB0020"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"


elseif UCase(APPLICATION("NOMPORTAL"))=UCase("bga")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="#004A8F"
cTEXTOBLUE="#AB0020"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#97C74A"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#004A8F"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#ffffff"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#97C74A"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#97C74A"
cH2="##004A8F"
cH3="##004A8F"
cH4="##004A8F"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#004A8F"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#003769"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#97C74A"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#97C74A"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#E4E5E6"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#E4E5E6"


'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#004A8F"

'Color de fondo de las opciones del men� vertical
cVDES="#97C74A"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#97C74A"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#004A8F"
cVTEXTOOVER = "#004A8F"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#97C74A"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#97C74A"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#97C74A"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#97C74A"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#97C74A"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "#004A8F"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#97C74A"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#97C74A"
cDAYCOLOR = "#97C74A"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"



elseif UCase(APPLICATION("NOMPORTAL"))=UCase("huf")then

' BOTON SALIR
cFONDOLOGOUT = "#ff0000"

cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "white"

'color de las filas pares
cFilaPar = "#e5e5e5"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#bfbfbf"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#868483"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#000000"
cH2="#000000"
cH3="#000000"
cH4="#000000"
cH5="#000000"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#d04b3c"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#000000"
'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#e5e5e5"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#FF0000"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#FF0000"


'color de los links en el men� horizontal
cMENULINK = "#FF0000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#bfbfbf"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#bfbfbf"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#000000"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "red"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#e5e5e5"


'*
'******    FIN MENU ***********
'*
'Color del borde de la barra de progreso
cBORDEPROGRESO="#000066"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="blue"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#000066"

'*****
'***** OFERTAS
'*****
'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#840000"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#bfbfbf"
cFNTAPARTOFE = "black"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#e5e5e5"
'*****
'***** OFERTAS
'*****

'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "darkseagreen"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e5e5e5"
cBORDECALENDARIO = "#bfbfbf"
cDAYCOLOR = "#bfbfbf"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"



elseif UCase(APPLICATION("NOMPORTAL"))=UCase("cfg")then

'BOTON SALIR
cFONDOLOGOUT = "#2a6aa8"

cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "white"

'color de las filas pares
cFilaPar = "#CCCCCC"
cTextoFilaPar = "#05143F"

'color de la filas impares
cFilaImPar = "#E6E6E6"
cTextoFilaImPar = "#05143F"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#2a6aa8"

'color del texto de las cabeceras
cTextoCabecera = "#ffffff"

'color del t�tulo de la p�gina
cH1="#d71920"
cH2="#000000"
cH3="#000000"
cH4="#000000"
cH5="#000000"

'color de los enlaces normales
cLINK = "#2a6aa8"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#0356a6"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#000000"
'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#2a6aa8"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#88b330"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#88b330"


'color de los links en el men� horizontal
cMENULINK = "#FFFFFF"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#FFFFFF"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#ffffff"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#ffffff"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#2a6aa8"
cVTEXTOOVER = "#88b330"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "#FFFFFF"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#ffffff"


'*
'******    FIN MENU ***********
'*
'Color del borde de la barra de progreso
cBORDEPROGRESO="#1215C"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "1215C"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#1215C"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#1215C"

'*****
'***** OFERTAS
'*****
'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#1215C"

'Color de los t�tulos de cabecera de los apartados en la cumplimentaci�n de la oferta
cFNTAPARTOFE = "#ffffff"
cAPARTOFE = "#2a6aa8"

'Color de fondo de los cajetines en la cumplimentaci�n de la oferta
cSOMBRACAJETIN = "#2a6aa8"
'*****
'***** OFERTAS
'*****

'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "darkseagreen"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#FFDF99"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e5e5e5"
cBORDECALENDARIO = "#2a6aa8"
cDAYCOLOR = "#2a6aa8"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("fed")then

' BOTON SALIR
cFONDOLOGOUT = "#ff0000"

cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "white"

'color de las filas pares
cFilaPar = "#d8d8d8"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#CDCDCD"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#4f86ae"

'color del texto de las cabeceras
cTextoCabecera = "#ffffff"

'color del t�tulo de la p�gina
cH1="#8AB3C7"
cH2="#000000"
cH3="#000000"
cH4="#000000"
cH5="#000000"

'color de los enlaces normales
'cLINK = "#00438C"
cLINK = "#e18b50"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#ff6907"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#000000"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#8AB3C7"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#4f86ae"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#4f86ae"

'color de los links en el men� horizontal
cMENULINK = "white"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#b5d7e7"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#b5d7e7"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#000000"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#4f86ae"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#4f86ae"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#4f86ae"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#4f86ae"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#85ABCF"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
'cAPARTOFE = "#c0c0c0"
'cFNTAPARTOFE = "#166991"
cAPARTOFE = "#8AB3C7"
cFNTAPARTOFE = "#ffffff"


'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta
cSOMBRACAJETIN="#8AB3C7"

'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#166991"

cPESTANYADES = "#166991"
cPESTANYAACT = "#166991"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"


'Color de fondo de las opciones del men� vertical
cVNORMAL = "#8AB3C7"


cCELSUBASTAABIERTA = "#ADCFAF"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
'cFONDOOBLIGATORIO = "#fa8072"
cFONDOOBLIGATORIO = "#b09a85"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#d8d8d8"
cBORDECALENDARIO = "#8AB3C7"
cDAYCOLOR = "#8AB3C7"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("mai")then

' BOTON SALIR
cFONDOLOGOUT = "#97BBBA"

cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "white"

'color de las filas pares
cFilaPar = "D6D2BE"
cTextoFilaPar = "#05143F"

'color de la filas impares
cFilaImPar = "#97BBBA"
cTextoFilaImPar = "#ffffff"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#b70202"

'color del texto de las cabeceras
cTextoCabecera = "#ffffff"

'color del t�tulo de la p�gina
cH1="#05143F"
cH2="#274f76"
cH3="#000000"
cH4="#000000"
cH5="#000000"

'color de los enlaces normales
cLINK = "#F60036"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "red"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#274f76"
'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#b70202"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#05143F"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#05143F"

'color de los links en el men� horizontal
cMENULINK = "#FFFFFF"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#05143F"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#97BBBA"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="97BBBA"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#b70202"
cVTEXTOOVER = "#b70202"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "#FFFFFF"

'Color de fondo de las opciones del men� vertical
cVNORMAL ="D6D2BE"


'*
'******    FIN MENU ***********
'*
'Color del borde de la barra de progreso
cBORDEPROGRESO="#1215C"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "1215C"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#1215C"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#1215C"

'*****
'***** OFERTAS
'*****
'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#1215C"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cFNTAPARTOFE = "#b70202"
cAPARTOFE = "#CCCCCC"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta
cSOMBRACAJETIN = "#CCCCCC"
'*****
'***** OFERTAS
'*****

'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "darkseagreen"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#FFDF99"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e5e5e5"
cBORDECALENDARIO = "#000066"
cDAYCOLOR = "#000066"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("bsa")then

cFONDOLOGOUT = "#CEEFF7"
cBORDELOGOUT = "#0099cc"
cTEXTOLOGOUT = "black"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#0099cc"

fMENUVERTICAL = "arial"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "#CEEFF7"
cTextoFilaPar = "#000000"

'color de la filas impares
cFilaImPar = "white"
cTextoFilaImPar = "#000000"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#CEEFF7"

'color del texto de las cabeceras
cTextoCabecera = "#000000"

'color del t�tulo de la p�gina
cH1="#000000"
cH2="#000000"
cH3="#000000"
cH4="#000000"
cH5="#000000"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#666666"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#0099cc"

'color de los enlaces a ayuda
cHELPLINK = "black"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "black"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="white"

'Color de fondo de las opciones del men� horizontal activada
cHACT="black"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="black"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#ffffff"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#FFFFFF"

'Color de fondo de las opciones del men� vertical
cVDES="#0099CC"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#CEEFF7"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#ffffff"
cVTEXTOOVER = "#0099cc"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0099cc"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#0099cc"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#0099cc"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#0099cc"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#CEEFF7"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#0099cc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("fem")then

cFONDOLOGOUT = "#CEEFF7"
cBORDELOGOUT = "#2464ac"
cTEXTOLOGOUT = "black"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#2464ac"

fMENUVERTICAL = "arial"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "#CEEFF7"
cTextoFilaPar = "#000000"

'color de la filas impares
cFilaImPar = "white"
cTextoFilaImPar = "#000000"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#CEEFF7"

'color del texto de las cabeceras
cTextoCabecera = "#000000"

'color del t�tulo de la p�gina
cH1="#000000"
cH2="#000000"
cH3="#000000"
cH4="#000000"
cH5="#000000"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#666666"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#2464ac"

'color de los enlaces a ayuda
cHELPLINK = "black"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "black"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="white"

'Color de fondo de las opciones del men� horizontal activada
cHACT="black"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="black"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#ffffff"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#FFFFFF"

'Color de fondo de las opciones del men� vertical
cVDES="#2464ac"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#CEEFF7"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#ffffff"
cVTEXTOOVER = "#2464ac"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#2464ac"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#2464ac"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#2464ac"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#2464ac"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#CEEFF7"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#2464ac"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("orm")then

cFONDOLOGOUT = "#DBDBDB"
cBORDELOGOUT = "#0099cc"
cTEXTOLOGOUT = "#0051BA"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#0099cc"

fMENUVERTICAL = "arial"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "white"
cTextoFilaPar = "#000000"

'color de la filas impares
cFilaImPar = "#DBDBDB"
cTextoFilaImPar = "#000000"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#Cccccc"

'color del texto de las cabeceras
cTextoCabecera = "#000000"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#666666"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#0051BA"

'color de los enlaces a ayuda
cHELPLINK = "black"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "black"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#DBDBDB"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#0051BA"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#0051BA"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#000000"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#DBDBDB"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#DBDBDB"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#0051BA"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL ="#B0B0B0"
'*
'******    FIN MENU ***********


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0099cc"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#9EC1E7"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#9EC1E7"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "0071B8"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#9EC1E7"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"


elseif UCase(APPLICATION("NOMPORTAL"))=UCase("ges")then

cFONDOLOGOUT = "#DBDBDB"
cBORDELOGOUT = "#0099cc"
cTEXTOLOGOUT = "#0051BA"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#0099cc"

fMENUVERTICAL = "verdana"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "white"
cTextoFilaPar = "#000000"

'color de la filas impares
cFilaImPar = "#DBDBDB"
cTextoFilaImPar = "#000000"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#Cccccc"

'color del texto de las cabeceras
cTextoCabecera = "#000000"

'color del t�tulo de la p�gina
cH1="#2C4FA0"
cH2="#2C4FA0"
cH3="#2C4FA0"
cH4="#2C4FA0"
cH5="#2C4FA0"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#666666"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#0051BA"

'color de los enlaces a ayuda
cHELPLINK = "black"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "black"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#E8E8E8"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#2C4FA0"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#2C4FA0"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#000000"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "WHITE"

'Color de fondo de las opciones del men� vertical
cVDES="#DBDBDB"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#DBDBDB"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#0051BA"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL ="#E8E8E8"


'*
'******    FIN MENU ***********


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0099cc"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#0545A9"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#0545A9"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#2C4FA1"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("log")then

cFONDOLOGOUT = "#DBDBDB"
cBORDELOGOUT = "#0099cc"
cTEXTOLOGOUT = "#0051BA"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#0099cc"

fMENUVERTICAL = "verdana"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "white"
cTextoFilaPar = "#000000"

'color de la filas impares
cFilaImPar = "#DBDBDB"
cTextoFilaImPar = "#000000"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#Cccccc"

'color del texto de las cabeceras
cTextoCabecera = "#000000"

'color del t�tulo de la p�gina
cH1="#2C4FA0"
cH2="#2C4FA0"
cH3="#2C4FA0"
cH4="#2C4FA0"
cH5="#2C4FA0"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#666666"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#0051BA"

'color de los enlaces a ayuda
cHELPLINK = "black"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "black"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#E8E8E8"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#2C4FA0"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#2C4FA0"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#000000"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "WHITE"

'Color de fondo de las opciones del men� vertical
cVDES="#DBDBDB"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#DBDBDB"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#0051BA"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL ="#E8E8E8"


'*
'******    FIN MENU ***********


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0099cc"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#0545A9"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#0545A9"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#2C4FA1"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("oca")then

cFONDOLOGOUT = "#DBDBDB"
cBORDELOGOUT = "#294478"
cTEXTOLOGOUT = "#DB3705"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#0099cc"

fMENUVERTICAL = "verdana"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "white"
cTextoFilaPar = "#294478"

'color de la filas impares
cFilaImPar = "#E5E3E3"
cTextoFilaImPar = "#294478"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#203A74"

'color del texto de las cabeceras
cTextoCabecera = "#FFF"

'color del t�tulo de la p�gina
cH1="#294478"
cH2="#294478"
cH3="#333333"
cH4="#333333"
cH5="#333333"

'color de los enlaces normales
'cLINK = "#3a68be"
cLINK = "##3a68be"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#3a68be"

'color de los enlaces a ayuda
cHELPLINK = "#FFF"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "#FFF"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#EEE"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#4064AA"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#3E66B0"

'color de los links en el men� horizontal
cMENULINK = "#213793"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#FFF"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "WHITE"

'Color de fondo de las opciones del men� vertical
cVDES="#E9EFF4"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#E9EFF4"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#213793"
cVTEXTOOVER = "#213793"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL ="#E8E8E8"

'Tama�o del texto del men�
tMEMUTEXTO ="#E8E8E8"

'*
'******    FIN MENU ***********


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0099cc"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#0545A9"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#0545A9"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#2C4FA1"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("ser")then

cFONDOLOGOUT = "#DBDBDB"
cBORDELOGOUT = "#294478"
cTEXTOLOGOUT = "#DB3705"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#0099cc"

fMENUVERTICAL = "verdana"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "white"
cTextoFilaPar = "#294478"

'color de la filas impares
cFilaImPar = "#E5E3E3"
cTextoFilaImPar = "#294478"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#203A74"

'color del texto de las cabeceras
cTextoCabecera = "#FFF"

'color del t�tulo de la p�gina
cH1="#294478"
cH2="#294478"
cH3="#333333"
cH4="#333333"
cH5="#333333"

'color de los enlaces normales
'cLINK = "#3a68be"
cLINK = "##3a68be"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#3a68be"

'color de los enlaces a ayuda
cHELPLINK = "#FFF"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "#FFF"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#EEE"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#4064AA"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#3E66B0"

'color de los links en el men� horizontal
cMENULINK = "#213793"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#FFF"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "WHITE"

'Color de fondo de las opciones del men� vertical
cVDES="#E9EFF4"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#E9EFF4"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#213793"
cVTEXTOOVER = "#213793"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL ="#E8E8E8"

'Tama�o del texto del men�
tMEMUTEXTO ="#E8E8E8"

'*
'******    FIN MENU ***********


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0099cc"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#0545A9"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#0545A9"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#2C4FA1"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("ete")then

cFONDOLOGOUT = "#DBDBDB"
cBORDELOGOUT = "#294478"
cTEXTOLOGOUT = "#DB3705"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#0099cc"

fMENUVERTICAL = "verdana"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "white"
cTextoFilaPar = "#294478"

'color de la filas impares
cFilaImPar = "#E5E3E3"
cTextoFilaImPar = "#294478"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#203A74"

'color del texto de las cabeceras
cTextoCabecera = "#FFF"

'color del t�tulo de la p�gina
cH1="#294478"
cH2="#294478"
cH3="#333333"
cH4="#333333"
cH5="#333333"

'color de los enlaces normales
'cLINK = "#3a68be"
cLINK = "##3a68be"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#3a68be"

'color de los enlaces a ayuda
cHELPLINK = "#FFF"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "#FFF"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#EEE"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#4064AA"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#3E66B0"

'color de los links en el men� horizontal
cMENULINK = "#213793"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#FFF"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "WHITE"

'Color de fondo de las opciones del men� vertical
cVDES="#E9EFF4"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#E9EFF4"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#213793"
cVTEXTOOVER = "#213793"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL ="#E8E8E8"

'Tama�o del texto del men�
tMEMUTEXTO ="#E8E8E8"

'*
'******    FIN MENU ***********


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0099cc"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#0545A9"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#0545A9"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#2C4FA1"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"




elseif UCase(APPLICATION("NOMPORTAL"))=UCase("ree")then

cFONDOLOGOUT = "#DBDBDB"
cBORDELOGOUT = "#0099cc"
cTEXTOLOGOUT = "#0051BA"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#0099cc"

fMENUVERTICAL = "verdana"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "white"
cTextoFilaPar = "#000000"

'color de la filas impares
cFilaImPar = "#DBDBDB"
cTextoFilaImPar = "#000000"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#Cccccc"

'color del texto de las cabeceras
cTextoCabecera = "#000000"

'color del t�tulo de la p�gina
cH1="#2C4FA0"
cH2="#2C4FA0"
cH3="#2C4FA0"
cH4="#2C4FA0"
cH5="#2C4FA0"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#666666"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#0051BA"

'color de los enlaces a ayuda
cHELPLINK = "black"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "black"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#E8E8E8"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#2C4FA0"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#2C4FA0"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#000000"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "WHITE"

'Color de fondo de las opciones del men� vertical
cVDES="#DBDBDB"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#DBDBDB"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#0051BA"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL ="#E8E8E8"


'*
'******    FIN MENU ***********


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0099cc"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#0545A9"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#0545A9"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#2C4FA1"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("cas")then

cFONDOLOGOUT = "#DBDBDB"
cBORDELOGOUT = "#F0272D"
cTEXTOLOGOUT = "#F0272D"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#0099cc"

fMENUVERTICAL = "verdana"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "white"
cTextoFilaPar = "#000000"

'color de la filas impares
cFilaImPar = "#DBDBDB"
cTextoFilaImPar = "#000000"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#Cccccc"

'color del texto de las cabeceras
cTextoCabecera = "#000000"

'color del t�tulo de la p�gina
cH1="#F0272D"
cH2="#000000"
cH3="#000000"
cH4="#000000"
cH5="#000000"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#666666"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#F0272D"

'color de los enlaces a ayuda
cHELPLINK = "black"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "black"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#F2F2F2"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#F0272D"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#F0272D"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#000000"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "WHITE"

'Color de fondo de las opciones del men� vertical
cVDES="#DBDBDB"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#DBDBDB"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#F0272D"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL ="#E8E8E8"


'*
'******    FIN MENU ***********


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0099cc"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#0545A9"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#0545A9"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "F0272D"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("rea")then

cFONDOLOGOUT = "#DBDBDB"
cBORDELOGOUT = "#0c4c92"
cTEXTOLOGOUT = "#0c4c92"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#0099cc"

fMENUVERTICAL = "verdana"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "white"
cTextoFilaPar = "#000000"

'color de la filas impares
cFilaImPar = "#DBDBDB"
cTextoFilaImPar = "#000000"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#Cccccc"

'color del texto de las cabeceras
cTextoCabecera = "#000000"

'color del t�tulo de la p�gina
cH1="#0c4c92"
cH2="#000000"
cH3="#000000"
cH4="#000000"
cH5="#000000"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#666666"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#0c4c92"

'color de los enlaces a ayuda
cHELPLINK = "black"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "black"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"
'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#F2F2F2"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#1B3D76"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#1B3D76"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#000000"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "WHITE"

'Color de fondo de las opciones del men� vertical
cVDES="#DBDBDB"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#DBDBDB"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#0c4c92"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL ="#E8E8E8"


'*
'******    FIN MENU ***********


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0099cc"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#0545A9"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#000000"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "0c4c92"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("map")then

cFONDOLOGOUT = "#BD0000"
cBORDELOGOUT = "#cecece"
cTEXTOLOGOUT = "#ffffff"

cFNTOFESINENVIAR = "#d00000"
cFNTOFESINGUARDAR = "#d00000"
cFNTENVIADA= "#D00000"

fMENUVERTICAL = "verdana"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "white"
cTextoFilaPar = "#000000"

'color de la filas impares
cFilaImPar = "#DBDBDB"
cTextoFilaImPar = "#000000"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#Cccccc"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#D00000"
cH2="#D00000"
cH3="#D00000"
cH4="#D00000"
cH5="#D00000"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#666666"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#D00000"

'color de los enlaces a ayuda
cHELPLINK = "black"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "black"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#D00000"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#D00000"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#D00000"

'color de los links en el men� horizontal
cMENULINK = "#FFFFFF"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#FFFFFF"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "WHITE"

'Color de fondo de las opciones del men� vertical
cVDES="#DBDBDB"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#DBDBDB"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#D00000"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "WHITE"

'Color de fondo de las opciones del men� vertical
cVNORMAL ="#E8E8E8"


'*
'******    FIN MENU ***********


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0099cc"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#0545A9"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#0545A9"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "D00000"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#EFEFED"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("msm")then

cFONDOLOGOUT = "#BD0000"
cBORDELOGOUT = "#cecece"
cTEXTOLOGOUT = "#ffffff"

cFNTOFESINENVIAR = "#d00000"
cFNTOFESINGUARDAR = "#d00000"
cFNTENVIADA= "#D00000"

fMENUVERTICAL = "verdana"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "white"
cTextoFilaPar = "#000000"

'color de la filas impares
cFilaImPar = "#DBDBDB"
cTextoFilaImPar = "#000000"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#Cccccc"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#D00000"
cH2="#D00000"
cH3="#D00000"
cH4="#D00000"
cH5="#D00000"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#666666"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#D00000"

'color de los enlaces a ayuda
cHELPLINK = "black"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "black"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#D00000"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#D00000"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#D00000"

'color de los links en el men� horizontal
cMENULINK = "#FFFFFF"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#FFFFFF"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "WHITE"

'Color de fondo de las opciones del men� vertical
cVDES="#DBDBDB"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#DBDBDB"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#D00000"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "WHITE"

'Color de fondo de las opciones del men� vertical
cVNORMAL ="#E8E8E8"


'*
'******    FIN MENU ***********


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0099cc"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#0545A9"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#0545A9"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "D00000"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#EFEFED"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"


elseif UCase(APPLICATION("NOMPORTAL"))=UCase("bkt")then

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#FE7C00"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#FE7C00"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#FE7C00"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#FE7C00"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#FE7C00"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#cccccc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#FE7C00"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#FE7C00"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#FE7C00"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#FE7C00"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#FE7C00"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#FE7C00"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#CCCCCC"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("SCP") then

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#FE7C00"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#cccccc"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#FE7C00"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#666666"
cH2="#666666"
cH3="#666666"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#FE7C00"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#FE7C00"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#FE7C00"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#cccccc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#FE7C00"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#FE7C00"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#FE7C00"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#FE7C00"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#FE7C00"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#FE7C00"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#CCCCCC"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("yel")then

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#FFFF00"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#000000"

'color de las filas pares
cFilaPar = "#FFFEB3"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#FFFFFF"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#E0DFDF"

'color del texto de las cabeceras
cTextoCabecera = "#000000"

'color del t�tulo de la p�gina
cH1="#000000"
cH2="#000000"
cH3="#000000"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
cLINK = "black"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#FF7E00"

'color de los enlaces a ayuda
cHELPLINK = "black"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "black"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#FFFF00"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#000000"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#000000"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "black"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#ffff00"

'Color de fondo de las opciones del men� vertical
cVDES="#cccccc"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#cccccc"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#FFFF00"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#FFFF00"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#FFFF00"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#FFFF00"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#FFFF00"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#FFFEB3"
cFNTAPARTOFE = "black"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#FFFEB3"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"


elseif UCase(APPLICATION("NOMPORTAL"))=UCase("cob")then

cFONDOLOGOUT = "#DBDBDB"
cBORDELOGOUT = "#F0272D"
cTEXTOLOGOUT = "#F0272D"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#0099cc"

fMENUVERTICAL = "verdana"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "white"
cTextoFilaPar = "#000000"

'color de la filas impares
cFilaImPar = "#DBDBDB"
cTextoFilaImPar = "#000000"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#Cccccc"

'color del texto de las cabeceras
cTextoCabecera = "#000000"

'color del t�tulo de la p�gina
cH1="#F0272D"
cH2="#000000"
cH3="#000000"
cH4="#000000"
cH5="#000000"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#666666"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#F0272D"

'color de los enlaces a ayuda
cHELPLINK = "black"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "black"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#F2F2F2"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#F0272D"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#F0272D"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#000000"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "WHITE"

'Color de fondo de las opciones del men� vertical
cVDES="#DBDBDB"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#DBDBDB"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#F0272D"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL ="#E8E8E8"


'*
'******    FIN MENU ***********


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0099cc"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#0545A9"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#0545A9"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "F0272D"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("clb")then

cFONDOLOGOUT = "#DBDBDB"
cBORDELOGOUT = "#d20825"
cTEXTOLOGOUT = "#d20825"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#0099cc"

fMENUVERTICAL = "verdana"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "white"
cTextoFilaPar = "#000000"

'color de la filas impares
cFilaImPar = "#DBDBDB"
cTextoFilaImPar = "#000000"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#Cccccc"

'color del texto de las cabeceras
cTextoCabecera = "#000000"

'color del t�tulo de la p�gina
cH1="#d20825"
cH2="#000000"
cH3="#000000"
cH4="#000000"
cH5="#000000"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#666666"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#F0272D"

'color de los enlaces a ayuda
cHELPLINK = "black"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "black"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#F2F2F2"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#d20825"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#d20825"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#000000"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "WHITE"

'Color de fondo de las opciones del men� vertical
cVDES="#DBDBDB"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#DBDBDB"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#d20825"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL ="#E8E8E8"


'*
'******    FIN MENU ***********


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0099cc"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#0545A9"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#0545A9"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "d20825"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"


elseif UCase(APPLICATION("NOMPORTAL"))=UCase("nor")then

cFONDOLOGOUT = "#DBDBDB"
cBORDELOGOUT = "#BE0004"
cTEXTOLOGOUT = "#BE0004"

cFNTOFESINENVIAR = "#BE0004"
cFNTOFESINGUARDAR = "#BE0004"
cFNTENVIADA= "#0099cc"

fMENUVERTICAL = "verdana"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "white"
cTextoFilaPar = "#000000"

'color de la filas impares
cFilaImPar = "#DBDBDB"
cTextoFilaImPar = "#000000"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#Cccccc"

'color del texto de las cabeceras
cTextoCabecera = "#000000"

'color del t�tulo de la p�gina
cH1="#BE0004"
cH2="#000000"
cH3="#000000"
cH4="#000000"
cH5="#000000"

'color de los enlaces normales
'cLINK = "#BE0004"
cLINK = "#666666"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#BE0004"

'color de los enlaces a ayuda
cHELPLINK = "black"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "black"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#F2F2F2"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#BE0004"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#BE0004"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#000000"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "WHITE"

'Color de fondo de las opciones del men� vertical
cVDES="#DBDBDB"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#DBDBDB"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#BE0004"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL ="#E8E8E8"


'*
'******    FIN MENU ***********


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0099cc"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#0545A9"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#0545A9"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "BE0004"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("beg")then

cFONDOLOGOUT = "#DBDBDB"
cBORDELOGOUT = "#BE0004"
cTEXTOLOGOUT = "#BE0004"

cFNTOFESINENVIAR = "#BE0004"
cFNTOFESINGUARDAR = "#BE0004"
cFNTENVIADA= "#0099cc"

fMENUVERTICAL = "verdana"
MENUFONTSIZE = 8

'color de las filas pares
cFilaPar = "white"
cTextoFilaPar = "#000000"

'color de la filas impares
cFilaImPar = "#DBDBDB"
cTextoFilaImPar = "#000000"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#Cccccc"

'color del texto de las cabeceras
cTextoCabecera = "#000000"

'color del t�tulo de la p�gina
cH1="#BE0004"
cH2="#000000"
cH3="#000000"
cH4="#000000"
cH5="#000000"

'color de los enlaces normales
'cLINK = "#BE0004"
cLINK = "#666666"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#BE0004"

'color de los enlaces a ayuda
cHELPLINK = "black"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "black"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#666666"

'*
'******    MENU ***********
'*
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#F2F2F2"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#BE0004"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#BE0004"

'color de los links en el men� horizontal
cMENULINK = "#000000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#000000"

'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "WHITE"

'Color de fondo de las opciones del men� vertical
cVDES="#DBDBDB"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#DBDBDB"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#BE0004"

'color del link en el men� horizontal cuando NO estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL ="#E8E8E8"


'*
'******    FIN MENU ***********


'Color del borde de la barra de progreso
cBORDEPROGRESO="#0099cc"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#0545A9"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#0545A9"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "BE0004"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("rsi")then
cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#77b031"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#77b031"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#ffffff"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#ffffff"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#77b031"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#333333"
cH2="#333333"
cH3="#333333"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#77b031"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#232323"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#77b031"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#77b031"

'color de los links en el men� horizontal
cMENULINK = "#white"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#FFFFFF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#FFFFFF"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#808080"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"



elseif UCase(APPLICATION("NOMPORTAL"))=UCase("ban")then
cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#77b031"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#F5F5F5"
cBORDELOGOUT = "#8A1616"
cTEXTOLOGOUT = "#35261A"

cBORDEINPUT = "#B2B0AB"
cBORDECAJETIN = "#B2B0AB"

'color de las filas pares
cFilaPar = "#ffffff"
cTextoFilaPar = "#35261A"

'color de la filas impares
cFilaImPar = "#ffffff"
cTextoFilaImPar = "#35261A"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#9f9892"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#35261A"
cH2="#35261A"
cH3="#35261A"
cH4="#35261A"
cH5="#35261A"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#35261A"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "#8F8171"

'color de los enlaces a ayuda
cHELPLINK = "#35261A"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "#8F8171"



'color del t�tulo en las p�ginas que informan de un error
cError = "#B50302"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#35261A"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#C4D600"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#645A4F"

'color de los links en el men� horizontal
cMENULINK = "#ffffff"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "#ffffff"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "#ffffff"

'Color de fondo de las opciones del men� vertical
cVDES="#FFFFFF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#645A4F"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#FFFFFF"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#B50302"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#B50302"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#B50302"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#B50302"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#9F9892"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#35261A"

cPESTANYADES = "#b2b0ab"
cPESTANYAACT = "#35261A"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("qed")then
cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#CCCCCC"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#77b031"

cFNTOFESINENVIAR = "#AB0020"
cFNTOFESINGUARDAR = "#AB0020"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#649600"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#ffffff"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#ffffff"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#649600"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#333333"
cH2="#333333"
cH3="#333333"
cH4="#666666"
cH5="#666666"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#649600"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "#AB0020"

'color de los textos
cTextoNormal="#666666"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#232323"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#649600"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#649600"

'color de los links en el men� horizontal
cMENULINK = "#white"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#FFFFFF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#FFFFFF"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#808080"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#AB0020"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#AB0020"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#AB0020"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#AB0020"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#808080"
cFNTAPARTOFE = "white"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "white"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("esg")then

cFONDOVERDE="#CCCCCC"
cFONDOSILVER="silver"
cFONDOWHITE="white"
cFONDOGRAY="#808080"
cFONDOGRAYDARK="DarkGray"

cTEXTOWHITE="white"
cTEXTOBLACK="black"
cTEXTOBLUE="#C82208"

cFNTOFESINENVIAR = "#C82208"
cFNTOFESINGUARDAR = "#C82208"
cFNTENVIADA= "#666666"

cFONDOLOGOUT = "#EDEBEB"
cBORDELOGOUT = "#CCCCCC"
cTEXTOLOGOUT = "#C82208"

'color de las filas pares
cFilaPar = "#EDEBEB"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#ffffff"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#cccccc"

'color del texto de las cabeceras
cTextoCabecera = "#000000"

'color del t�tulo de la p�gina
cH1="#000000"
cH2="#000000"
cH3="#000000"
cH4="#cccccc"
cH5="#cccccc"

'color de los enlaces normales
'cLINK = "black"
cLINK = "black"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "C82208"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "C82208"

'color de los textos
cTextoNormal="#000000"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#cccccc"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#C82208"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#C82208"

'color de los links en el men� horizontal
cMENULINK = "#990000"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "WHITE"

'Color de fondo de las opciones del men� vertical
cVDES="#C82208"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#C82208"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#444444"
cVTEXTOOVER = "#FFFFFF"


'Color del borde de la barra de progreso
cBORDEPROGRESO="#C82208"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "#C82208"

'Color de los bloques de progreso
cBLOQUEPROGRESO="#C82208"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "#C82208"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#999999"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#C82208"
cFNTAPARTOFE = "WHITE"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#cccccc"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"

'Color de fondo de las opciones del men� vertical
cVNORMAL = "#cccccc"


cCELSUBASTAABIERTA = "#d1aaaa"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"

cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#840000"
cDAYCOLOR = "#840000"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"
       

elseif UCase(APPLICATION("NOMPORTAL"))=UCase("deo")then


    cFONDOVERDE="#CCCCCC"
    cFONDOSILVER="silver"
    cFONDOWHITE="white"
    cFONDOGRAY="#CCCCCC"
    cFONDOGRAYDARK="DarkGray"

    cTEXTOWHITE="white"
    cTEXTOBLACK="black"
    cTEXTOBLUE="#AB0020"

    cFNTOFESINENVIAR = "#AB0020"
    cFNTOFESINGUARDAR = "#AB0020"
    cFNTENVIADA= "#666666"

    cFONDOLOGOUT = "#EDEBEB"
    cBORDELOGOUT = "#CCCCCC"
    cTEXTOLOGOUT = "#AB0020"

    'color de las filas pares
    cFilaPar = "#EDEBEB"
    cTextoFilaPar = "black"

    'color de la filas impares
    cFilaImPar = "#cccccc"
    cTextoFilaImPar = "black"

    'color del fondo de las cabeceras de las tablas
    cCabeceraTablas = "#adadad"

    'color del texto de las cabeceras
    cTextoCabecera = "#FFFFFF"

    'color del t�tulo de la p�gina
    cH1="#666666"
    cH2="#666666"
    cH3="#666666"
    cH4="#cccccc"
    cH5="#cccccc"

    'color de los enlaces normales
    'cLINK = "#FF6600"
    cLINK = "#adadad"
    'color de los enlaces normales cuando estas sobre el enlace
    cHLINK = "black"

    'color de los enlaces a ayuda
    cHELPLINK = "white"
    'color de los enlaces a ayuda cuando estas sobre el enlace
    cHELPHLINK = "white"



    'color del t�tulo en las p�ginas que informan de un error
    cError = "#adadad"

    'color de los textos
    cTextoNormal="#666666"

    'colores del men�
    'Color de fondo de las opciones del men� horizontal desactivada
    cHDES="#cccccc"

    'Color de fondo de las opciones del men� horizontal activada
    cHACT="#adadad"
    'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
    cHRES="#adadad"

    'color de los links en el men� horizontal
    cMENULINK = "#990000"
    'color de los links en el men� horizontal cuando est�s sobre el enlace
    cMENUHLINK = "white"
    'color del link en el men� horizontal cuando estamos en esa opcion
    cMENUACTUAL = "white"

    'Color de fondo de las opciones del men� vertical
    cVDES="#cccccc"
    'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
    cVACT="#cccccc"

    'Color del texto de las opciones del men� vertical
    cVTEXTO = "#444444"
    cVTEXTOOVER = "#adadad"


    'Color del borde de la barra de progreso
    cBORDEPROGRESO="#adadad"

    'Color del borde del bloque donde se muestra la acci�n en el progreso
    cBORDEACCIONPROGRESO= "#adadad"

    'Color de los bloques de progreso
    cBLOQUEPROGRESO="#adadad"

    'Color de los textos que reflejan las acciones en el progreso 
    cACCIONPROGRESO = "#adadad"


    'Color de los bordes de los bloques (envio de ofertas)
    cBORDEBLOQUE = "#999999"

    'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
    cAPARTOFE = "#adadad"
    cFNTAPARTOFE = "white"

    'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

    cSOMBRACAJETIN = "#cccccc"


    'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
    cSubtitulo = "#051F6A"

    'Color del borde de las pesta�as

    cBORDEPESTANYA = "#85ABCF"

    cPESTANYADES = "#A5C7FF"
    cPESTANYAACT = "#85ABCF"

    'color del link en el men� horizontal cuando no estamos en esa opci�n
    cMENUDESACT = "black"

    'Color de fondo de las opciones del men� vertical
    cVNORMAL = "#cccccc"


    cCELSUBASTAABIERTA = "#d1aaaa"
    cCELSUBASTACERRADA = "darkgray"

    'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
    cFONDOPUJASUPERADA = "#d3d3d3"
    cTEXTOPUJASUPERADA= "red"

    'Color del fondo del S� en el pop up de descripci�n de un atributo.
    cFONDOOBLIGATORIO = "#fa8072"

    cFONDOADJUDICADA="blue"
    cTEXTOADJUDICADA="white"

    cFONDONOADJUDICADA="red"
    cTEXTONOADJUDICADA="white"

    cWEEKENDCOLOR = "#e0e0e0"
    cBORDECALENDARIO = "#840000"
    cDAYCOLOR = "#840000"

    'Color de los enlaces a popups con el detalle de lo que se pincha
    cPOPUPLINK = "black"
    cPOPUPHLINK = "black"



else

'color de las filas pares
cFilaPar = "#85ABCF"
cTextoFilaPar = "black"

'color de la filas impares
cFilaImPar = "#A5C7FF"
cTextoFilaImPar = "black"

'color del fondo de las cabeceras de las tablas
cCabeceraTablas = "#094176"

'color del texto de las cabeceras
cTextoCabecera = "#FFFFFF"

'color del t�tulo de la p�gina
cH1="#051F6A"
cH2="#000000"
cH3="#000000"
cH4="#000000"
cH5="#000000"

'color de los enlaces normales
'cLINK = "#FF6600"
cLINK = "#ED1E24"
'color de los enlaces normales cuando estas sobre el enlace
cHLINK = "black"

'color de los enlaces a ayuda
cHELPLINK = "white"
'color de los enlaces a ayuda cuando estas sobre el enlace
cHELPHLINK = "white"



'color del t�tulo en las p�ginas que informan de un error
cError = "red"

'color de los textos
cTextoNormal="#000000"

'colores del men�
'Color de fondo de las opciones del men� horizontal desactivada
cHDES="#85ABCF"

'Color de fondo de las opciones del men� horizontal activada
cHACT="#094176"
'Color de fondo de las opciones del men� horizontal cuando se sit�a el rat�n encima
cHRES="#094176"

'color de los links en el men� horizontal
cMENULINK = "black"
'color de los links en el men� horizontal cuando est�s sobre el enlace
cMENUHLINK = "white"
'color del link en el men� horizontal cuando estamos en esa opcion
cMENUACTUAL = "white"

'Color de fondo de las opciones del men� vertical
cVDES="#A5C7FF"
'Color de fondo de las opciones del men� vertical cuando estas sobre la opci�n
cVACT="#A5C7FF"

'Color del texto de las opciones del men� vertical
cVTEXTO = "#000000"
cVTEXTOOVER = "#000000"


'Color del borde de la barra de progreso
cBORDEPROGRESO="darkblue"

'Color del borde del bloque donde se muestra la acci�n en el progreso
cBORDEACCIONPROGRESO= "black"

'Color de los bloques de progreso
cBLOQUEPROGRESO="blue"

'Color de los textos que reflejan las acciones en el progreso 
cACCIONPROGRESO = "blue"


'Color de los bordes de los bloques (envio de ofertas)
cBORDEBLOQUE = "#85ABCF"

'Color de la cabecera de los apartados en la cumplimentaci�n de la oferta
cAPARTOFE = "#85ABCF"
cFNTAPARTOFE = "black"

'Color de los t�tulos de los cajetines en la cumplimentaci�n de la oferta

cSOMBRACAJETIN = "#A5C7FF"


'Color de los subt�tulos (criterios de b�squeda, resultado...) en los pedidos (por ahora)
cSubtitulo = "#051F6A"

'Color del borde de las pesta�as

cBORDEPESTANYA = "#85ABCF"

cPESTANYADES = "#A5C7FF"
cPESTANYAACT = "#85ABCF"

'color del link en el men� horizontal cuando no estamos en esa opci�n
cMENUDESACT = "black"


'Color de fondo de las opciones del men� vertical
cVNORMAL = "#85ABCF"



cCELSUBASTAABIERTA = "darkseagreen"
cCELSUBASTACERRADA = "darkgray"

'Color del fondo/texto de las celdas donde viene el proveedor que tiene el mejor precio en las subastas
cFONDOPUJASUPERADA = "#d3d3d3"
cTEXTOPUJASUPERADA= "red"

'Color del fondo del S� en el pop up de descripci�n de un atributo.
cFONDOOBLIGATORIO = "#fa8072"

cFONDOADJUDICADA="blue"
cTEXTOADJUDICADA="white"

cFONDONOADJUDICADA="red"
cTEXTONOADJUDICADA="white"



cWEEKENDCOLOR = "#e0e0e0"
cBORDECALENDARIO = "#85ABCF"
cDAYCOLOR = "#85ABCF"

'Color de los enlaces a popups con el detalle de lo que se pincha
cPOPUPLINK = "black"
cPOPUPHLINK = "black"

end if



'MENU a partir de la Versi�n 319004

cMenuBg = cHDES						'Fondo del elemento del menu
cMenuText = cMENULINK				'Texto del elemento del men�
cMenuBgAct = cHACT					'Fondo del elemento del menu actual
cMenuTextAct = cMENUACTUAL 			'Texto del elemento del men� actual
cMenuBgHover = cHRES	 			'Fondo del elemento del menu resaltado (Hover)
cMenuTextHover = cMENUHLINK         'Texto del elemento del men� resaltado (Hover)
cSubMenuBg = cVDES					'Fondo del elemento del submenu
cSubMenuText = cVTEXTO				'Texto del elemento del submen�
cSubMenuBgHover = cVACT 			'Fondo del elemento del menu resaltado (Hover)
cSubMenuTextHover = cVTEXTOOVER		'Texto del elemento del men� resaltado (Hover)





%>