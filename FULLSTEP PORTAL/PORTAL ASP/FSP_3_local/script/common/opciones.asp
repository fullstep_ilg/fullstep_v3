﻿<%@  language="VBScript" %>
<!--#include file="acceso.asp"-->
<!--#include file="idioma.asp"-->
<!--#include file="formatos.asp"-->
<!--#include file="fsal_1.asp"-->
<%
''' <summary>
''' Muestra la pantalla para q el usuario pueda cambiar su configuración de formatos numericos, de fecha y tipo mail
''' </summary>
''' <remarks>Llamada desde: common\opciones.asp common\menu.asp ; Tiempo máximo: 0,2</remarks>

Idioma=Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,true,false,0)

CiaId=oRaiz.Sesion.CiaId
UsuId=oRaiz.Sesion.UsuId
decimalFmt=Request.Cookies("USU_DECIMALFMT")
precisionFmt=Request.Cookies("USU_PRECISIONFMT")
thousanFmt=Request.Cookies("USU_THOUSANFMT")
dateFmt=Request.Cookies("USU_DATEFMT")
mostrarFmt=Request.Cookies("USU_MOSTRARFMT")
tipomail=Request.Cookies("USU_TIPOMAIL")

dim IsSecure
IsSecure = left(Application ("RUTASEGURA"),8)
if IsSecure = "https://" then                       
    IsSecure = "; secure"
else
    IsSecure =""
end if

if isnull(decimalFmt) then
    Response.AddHeader "Set-Cookie","USU_DECIMALFMT=,; path=/; HttpOnly" + IsSecure
	decimalFmt=","
    Response.AddHeader "Set-Cookie","USU_THOUSANFMT=.; path=/; HttpOnly" + IsSecure
	thousanFmt="."
    Response.AddHeader "Set-Cookie","USU_DATEFMT=dd/mm/yyyy; path=/; HttpOnly" + IsSecure
	dateFmt="dd/mm/yyyy"
end if


if Request.ServerVariables("CONTENT_LENGTH")>0 then

	decimalfmt=request("lstdecimal")
	thousanfmt=request("lstMiles")
	datefmt=request("lstFecha")
	precisionfmt=request("lstPrecision")
	
	tipoMail=cint(request("lstMail"))

	if request("chkMostrar")=1 then
		mostrarfmt=1
	else
		mostrarfmt=0
	end if
	
	oraiz.guardarformatosusuario ciaid, UsuId, decimalfmt,thousanfmt,precisionfmt,datefmt,mostrarfmt,,tipoMail
	
    Response.AddHeader "Set-Cookie","USU_MOSTRARFMT=" &  mostrarfmt  & "; path=/; HttpOnly" + IsSecure
    Response.AddHeader "Set-Cookie","USU_DECIMALFMT=" & decimalfmt  & "; path=/; HttpOnly" + IsSecure
    Response.AddHeader "Set-Cookie","USU_THOUSANFMT=" & thousanfmt  & "; path=/; HttpOnly" + IsSecure    
    Response.AddHeader "Set-Cookie","USU_PRECISIONFMT=" & precisionfmt  & "; path=/; HttpOnly" + IsSecure
    Response.AddHeader "Set-Cookie","USU_DATEFMT=" & datefmt  & "; path=/; HttpOnly" + IsSecure
	Response.AddHeader "Set-Cookie","USU_TIPOMAIL=" & tipoMail  & "; path=/; HttpOnly" + IsSecure
    
	set oraiz=nothing

%>
<html>
<script src="formatos.js"></script>
<script language="JavaScript" src="ajax.js"></script>
<!--#include file="fsal_3.asp"-->
<script>	
	vdecimalfmt='<%=decimalfmt%>'
	vthousanfmt='<%=thousanfmt%>'
	vprecisionfmt=<%=precisionfmt%>
	vdatefmt='<%=datefmt%>'
	
	function init()
	{
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
		p=window.opener
        if (p.requiereRefresco)
			{
				p.aplicarFormatos(vdecimalfmt,vthousanfmt,vprecisionfmt,vdatefmt)
			}
		window.close()
	}
</script>
<body onload="init()">
</body>
<!--#include file="fsal_2.asp"-->
</html>
<%	Response.End

end if	


dim den
den = devolverTextos(Idioma,7)

titulo=Application("TITULOVENTANAS_" & Request("Idioma"))
	

%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <% 
    
    %>
    <title>
        <%=title%></title>
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
    <script src="formatos.js"></script>
    <script language="JavaScript" src="ajax.js"></script>
    <!--#include file="fsal_3.asp"-->
</head>
<script>

    function validar() {
        var f
        f = document.forms["frmFormatos"]

        if (f.lstDecimal.options[f.lstDecimal.selectedIndex].value == f.lstMiles.options[f.lstMiles.selectedIndex].value) {
            alert("<%=den(11)%>")
            return false
        }

        return true
    }

    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }

</script>
<body topmargin="0" leftmargin="0" onload="init()">
    <br>
    <br>
    <form name="frmFormatos" method="post" action="opciones.asp" onsubmit="return validar();">
    <input type="hidden" name="Idioma" value="<%=Idioma%>">
    <table align="center" cellspacing="0" cellpadding="0" border="0" width="450px">
        <tr>
            <td>
                <table border="0" cellspacing="2" width="100%">
                    <tr>
                        <td colspan="2" class="cabecera">
                            <%=den(2)%>
                        </td>
                    </tr>
                    <tr>
                        <td class="filaImpar">
                            <%=den(3)%>
                        </td>
                        <td class="filaImpar" width="20%">
                            <select name="lstDecimal">
                                <option value="." <%if decimalFmt="." then%>SELECTED<%end if%>>.</option>
                                <option value="," <%if decimalFmt="," then%>SELECTED<%end if%>>,</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="filaPar">
                            <%=den(4)%>
                        </td>
                        <td class="filaPar" bgcolor="white">
                            <select name="lstPrecision">
                                <option value="0" <%if precisionFmt=0 then%>SELECTED<%end if%>>0</option>
                                <option value="1" <%if precisionFmt=1 then%>SELECTED<%end if%>>1</option>
                                <option value="2" <%if precisionFmt=2 Then%>SELECTED<%end if%>>2</option>
                                <option value="3" <%if precisionFmt=3 then%>SELECTED<%end if%>>3</option>
                                <option value="4" <%if precisionFmt=4 then%>SELECTED<%end if%>>4</option>
                                <option value="5" <%if precisionFmt=5 then%>SELECTED<%end if%>>5</option>
                                <option value="6" <%if precisionFmt=6 then%>SELECTED<%end if%>>6</option>
                                <option value="7" <%if precisionFmt=7 then%>SELECTED<%end if%>>7</option>
                                <option value="8" <%if precisionFmt=8 then%>SELECTED<%end if%>>8</option>
                                <option value="9" <%if precisionFmt=9 then%>SELECTED<%end if%>>9</option>
                                <option value="10" <%if precisionFmt=10 then%>SELECTED<%end if%>>10</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="filaImpar">
                            <%=den(5)%>
                        </td>
                        <td class="filaImpar">
                            <select name="lstMiles">
                                <option value="" <%if thousanFmt="" then%>SELECTED<%end if%>>
                                    <%=den(9)%></option>
                                <option value="," <%if thousanFmt="," then%>SELECTED<%end if%>>,</option>
                                <option value="." <%if thousanFmt="." then%>SELECTED<%end if%>>.</option>
                                <option value=" " <%if thousanFmt=" " then%>SELECTED<%end if%>>
                                    <%=den(8)%></option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="filaPar">
                            <%=den(6)%>
                        </td>
                        <td class="filaPar">
                            <select name="lstFecha">
                                <option value="dd/mm/yyyy" <%if dateFmt="dd/mm/yyyy" then%>SELECTED<%end if%>>dd/mm/yyyy</option>
                                <option value="mm/dd/yyyy" <%if dateFmt="mm/dd/yyyy" then%>SELECTED<%end if%>>mm/dd/yyyy</option>
                                <option value="dd-mm-yyyy" <%if dateFmt="dd-mm-yyyy" then%>SELECTED<%end if%>>dd-mm-yyyy</option>
                                <option value="mm-dd-yyyy" <%if dateFmt="mm-dd-yyyy" then%>SELECTED<%end if%>>mm-dd-yyyy</option>
                                <option value="dd.mm.yyyy" <%if dateFmt="dd.mm.yyyy" then%>SELECTED<%end if%>>dd.mm.yyyy</option>
                                <option value="mm.dd.yyyy" <%if dateFmt="mm.dd.yyyy" then%>SELECTED<%end if%>>mm.dd.yyyy</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="filaPar">
                            <%=den(15)%>
                        </td>
                        <td class="filaPar">
                            <select name="lstMail">
                                <option value="1" <%if tipoMail="1" then%>SELECTED<%end if%>>
                                    <%=den(16)%></option>
                                <option value="0" <%if tipoMail="0" then%>SELECTED<%end if%>>
                                    <%=den(17)%></option>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <script>
            s = "<tr>"
            s += "	<td height=40px align=center>"
            if (window.opener.name != "fraOfertaClient")
                s += "<input type=submit class=button name=cmdAceptar value='<%=den(10)%>'>"
            else
                s += "<%=jsText(den(12))%>"

            s += "	</td>"
            s += "</TR>"

            document.write(s)

        </script>
    </table>
    <input type="hidden" name="chkMostrar" value="<%=mostrarFmt%>">
    </form>
</body>
<!--#include file="fsal_2.asp"-->
</html>
