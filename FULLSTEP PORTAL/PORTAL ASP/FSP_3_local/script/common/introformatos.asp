﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->

<%
''' <summary>
''' Muestra la pantalla para q el usuario pueda cambiar su configuración de formatos numericos y de fecha
''' </summary>
''' <remarks>Llamada desde: common\introformatos.asp    common\menu.asp ; Tiempo máximo: 0,2</remarks>

Idioma=request("Idioma")
set oRaiz=validarUsuario(Idioma,true,false,0)


CiaId=oRaiz.Sesion.CiaId
UsuId=oRaiz.Sesion.UsuId
decimalFmt=Request.Cookies("USU_DECIMALFMT")
precisionFmt=Request.Cookies("USU_PRECISIONFMT")
thousanFmt=Request.Cookies("USU_THOUSANFMT")
dateFmt=Request.Cookies("USU_DATEFMT")

dim IsSecure
IsSecure = left(Application ("RUTASEGURA"),8)
if IsSecure = "https://" then                       
    IsSecure = "; secure"
else
    IsSecure =""
end if

if isnull(decimalFmt) then
    Response.AddHeader "Set-Cookie","USU_DECIMALFMT=,; path=/; HttpOnly" + IsSecure
	decimalFmt=","
    Response.AddHeader "Set-Cookie","USU_THOUSANFMT=.; path=/; HttpOnly" + IsSecure
	thousanFmt="."
    Response.AddHeader "Set-Cookie","USU_DATEFMT=dd/mm/yyyy; path=/; HttpOnly" + IsSecure
	dateFmt="dd/mm/yyyy"
end if


if Request.ServerVariables("CONTENT_LENGTH")>0 then
	
	decimalfmt=request("lstdecimal")
	thousanfmt=request("lstMiles")
	datefmt=request("lstFecha")
	precisionfmt=request("lstPrecision")
	if request("chkMostrar")=1 then
		mostrarfmt=1
	else
		mostrarfmt=0
	end if
	oraiz.guardarformatosusuario CiaId, UsuId,decimalfmt,thousanfmt,precisionfmt,datefmt,mostrarfmt	

    Response.AddHeader "Set-Cookie","USU_MOSTRARFMT=" &  mostrarfmt  & "; path=/; HttpOnly" + IsSecure

	if Request.Cookies("USU_DECIMALFMT") <>decimalfmt then
		haCambiado = true
	end if
    Response.AddHeader "Set-Cookie","USU_DECIMALFMT=" & decimalfmt  & "; path=/; HttpOnly" + IsSecure

	if Request.Cookies("USU_THOUSANFMT") <>thousanfmt then
		haCambiado = true
	end if
    Response.AddHeader "Set-Cookie","USU_THOUSANFMT=" & thousanfmt  & "; path=/; HttpOnly" + IsSecure

	if Request.Cookies("USU_PRECISIONFMT") <>precisionfmt then
		haCambiado = true
	end if
    Response.AddHeader "Set-Cookie","USU_PRECISIONFMT=" & precisionfmt  & "; path=/; HttpOnly" + IsSecure

	if Request.Cookies("USU_DATEFMT") <>datefmt then
		haCambiado = true
	end if
    Response.AddHeader "Set-Cookie","USU_DATEFMT=" & datefmt  & "; path=/; HttpOnly" + IsSecure

    set oraiz=nothing	
	%>
	<HTML>
	<HEAD>

    <title><%=title%></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	</HEAD>

	<script>
	<%
	if haCambiado then
		select case request("func")
			case "seguimpedidos"
				%>
				window.opener.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/seguimpedidos.asp?Idioma=<%=Idioma%>","default_main")
				<%
			case "ofertas"
				%>
				window.opener.open("<%=Application("RUTASEGURA")%>script/solicitudesoferta/solicitudesoferta.asp?Idioma=<%=Idioma%>","default_main")
				<%
			case "pedidos"
				%>
				window.opener.open("<%=Application("RUTASEGURA")%>script/pedidos/pedidos.asp?Idioma=<%=Idioma%>","default_main")
				<%
		end select
	end if%>
		
	
	

	window.close()
	</script>
	</html>
	
	<%
	Response.End
	

end if	


dim den
den = devolverTextos(Idioma,7)


%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><%=title%></title>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">

<script src="formatos.js"></script>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">

</HEAD>

<script>
function validar()
{
var f
f= document.forms["frmFormatos"]

if (f.lstDecimal.options[f.lstDecimal.selectedIndex].value==f.lstMiles.options[f.lstMiles.selectedIndex].value)
	{
	alert ("<%=den(11)%>")
	return false
	}

return true

}
function cambiarEjemplo()
{
var f
f= document.forms["frmFormatos"]

vdecimalfmt=f.lstDecimal.options[f.lstDecimal.selectedIndex].value
vthousanfmt=f.lstMiles.options[f.lstMiles.selectedIndex].value
vprecisionfmt=f.lstPrecision.options[f.lstPrecision.selectedIndex].value
var NumSelect
if (vprecisionfmt==0)
	{NumSelect = "110000"}
else if (vprecisionfmt==1)
	{NumSelect = "110000.1"}
else if (vprecisionfmt==2)
	{NumSelect = "110000.12"}
else if (vprecisionfmt==3)
	{NumSelect = "110000.123"}
else if (vprecisionfmt==4)
	{NumSelect = "110000.1234"}
else if (vprecisionfmt==5)
	{NumSelect = "110000.12345"}
else if (vprecisionfmt==6)
	{NumSelect = "110000.123456"}
else if (vprecisionfmt==7)
	{NumSelect = "110000.1234567"}
else if (vprecisionfmt==8)
	{NumSelect = "110000.12345678"}
else if (vprecisionfmt==9)
	{NumSelect = "110000.123456789"}
else if (vprecisionfmt==10)
	{NumSelect = "110000.1234567891"}

document.getElementById("divEjemploNumber").innerHTML = num2str(NumSelect,vdecimalfmt,vthousanfmt,vprecisionfmt)

vdatefmt=f.lstFecha.options[f.lstFecha.selectedIndex].value
document.getElementById("divEjemploDate").innerHTML = date2str(new Date(2004,11,31),vdatefmt)
}


</script>


<BODY TOPMARGIN=0 leftmargin=0>

<FORM NAME=frmFormatos method=post action=introformatos.asp onsubmit="return validar();">

<input type = hidden name=Idioma value="<%=Idioma%>">
<input type = hidden name=Origen value="<%=Request("Origen")%>">
<input type = hidden name=func value="<%=Request("func")%>">

<table class=principal align=LEFT cellspacing=0 cellpadding=3 border=0 width =80% >
	<tr>
		<td align=center>
			<h2><%=den(1)%></h2>
		</td>
	</tr>
	<tr>
		<td>
			<%=den(21)%>
		</td>
	</tr>
	<TR>
		<TD>
			<table cellpadding=3 style="border-style:solid;border-width:2px;border-color:<%=cCabeceraTablas%>"  cellspacing=0 width = 100%>
				<tr>
					<td colspan=2 class="cabecera"><%=den(22)%></td>
				</tr>
				<tr>
					<td class="filaImpar"><%=den(3)%></td>
					<td class="filaImpar" width=45%>
						<select name=lstDecimal id=lstDecimal onchange="cambiarEjemplo()">
							<option value="." <%if decimalFmt="." then%>SELECTED<%end if%>>.</option>
							<option value="," <%if decimalFmt="," then%>SELECTED<%end if%>>,</option>
						</select>
					</td>
				</tr>
				<tr>
					<td   class="filaImpar"><%=den(5)%></td>
					<td   class="filaImpar">
						<select name=lstMiles id=lstMiles onchange="cambiarEjemplo()">
							<option value="" <%if thousanFmt="" then%>SELECTED<%end if%>><%=den(9)%></option>
							<option value="," <%if thousanFmt="," then%>SELECTED<%end if%>>,</option>
							<option value="." <%if thousanFmt="." then%>SELECTED<%end if%>>.</option>
							<option value=" " <%if thousanFmt=" " then%>SELECTED<%end if%>><%=den(8)%></option>
						</select>
					</td>
				</tr>
				<tr>
					<td  class="filaImpar"><%=den(4)%></td>
					<td  class="filaImpar"  bgcolor=white>
						<select name=lstPrecision onchange="cambiarEjemplo()">
							<option value=0 <%if precisionFmt=0 then%>SELECTED<%end if%>>0</option>
							<option value=1 <%if precisionFmt=1 then%>SELECTED<%end if%>>1</option>
							<option value=2 <%if precisionFmt=2 Then%>SELECTED<%end if%>>2</option>
							<option value=3 <%if precisionFmt=3 then%>SELECTED<%end if%>>3</option>
							<option value=4 <%if precisionFmt=4 then%>SELECTED<%end if%>>4</option>
							<option value=5 <%if precisionFmt=5 then%>SELECTED<%end if%>>5</option>
							<option value=6 <%if precisionFmt=6 then%>SELECTED<%end if%>>6</option>
							<option value=7 <%if precisionFmt=7 then%>SELECTED<%end if%>>7</option>
							<option value=8 <%if precisionFmt=8 then%>SELECTED<%end if%>>8</option>
							<option value=9 <%if precisionFmt=9 then%>SELECTED<%end if%>>9</option>
							<option value=10 <%if precisionFmt=10 then%>SELECTED<%end if%>>10</option>
						</select>
					</td>
				</tr>
				<tr>
					<td   class="filaPar"><%=den(24)%></td>
					<td   class="filaPar"><div name=divEjemploNumber id=divEjemploNumber><%=VisualizacionNumero(110000.123456,decimalfmt,thousanfmt,precisionfmt)%></div>
					</td>
				</tr>
				<tr>
					<td colspan=2 class="cabecera"><%=den(23)%></td>
				</tr>
				<tr>
					<td class="filaImpar"><%=den(25)%></td>
					<td class="filaImpar" >
						<select name=lstFecha id=lstFecha onchange="cambiarEjemplo()">
							<option value="dd/mm/yyyy" <%if dateFmt="dd/mm/yyyy" then%>SELECTED<%end if%>>dd/mm/yyyy</option>
							<option value="mm/dd/yyyy" <%if dateFmt="mm/dd/yyyy" then%>SELECTED<%end if%>>mm/dd/yyyy</option>
							<option value="dd-mm-yyyy" <%if dateFmt="dd-mm-yyyy" then%>SELECTED<%end if%>>dd-mm-yyyy</option>
							<option value="mm-dd-yyyy" <%if dateFmt="mm-dd-yyyy" then%>SELECTED<%end if%>>mm-dd-yyyy</option>
							<option value="dd.mm.yyyy" <%if dateFmt="dd.mm.yyyy" then%>SELECTED<%end if%>>dd.mm.yyyy</option>
							<option value="mm.dd.yyyy" <%if dateFmt="mm.dd.yyyy" then%>SELECTED<%end if%>>mm.dd.yyyy</option>
						</select>
						(d=<%=den(27)%>, m=<%=den(28)%>, y=<%=den(29)%>)
					</td>
				</tr>
				
				<tr>
					<td class="filaPar"><%=den(26)%></td>
					<td class="filaPar"><div name=divEjemploDate id=divEjemploDate><%=VisualizacionFecha(dateserial(2004,12,31),datefmt)%></div>
					</td>
				</tr>
			</table>
		</td>
	<tr>
		<td height=40px><input type=checkbox class=checkbox  name=chkMostrar checked value=1> <%=den(30)%><br><%=den(31)%></td>
	</TR>
	<tr>
		<td height=40px align=center><input class=button type=submit name=cmdAceptar value=<%=den(10)%>></td>
	</TR>
</table>



</form>
</BODY>
</HTML>
