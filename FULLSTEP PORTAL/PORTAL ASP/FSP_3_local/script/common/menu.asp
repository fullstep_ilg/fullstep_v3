<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/formatos.asp"-->


<%
''' <summary>
''' Cargar el menu
''' </summary>
''' <remarks>Llamada desde: 114 asp's de la aplicaci�n ; Tiempo m�ximo: 0,2</remarks>
dim denMenu
Idioma = Request.Cookies("USU_IDIOMA")


set oRaiz=validarUsuario(Idioma,true,true,-1)

denMenu = devolverTextos(Idioma,6)	

den1=devolverTextos(Idioma,1 )
avisoPopup=den1(12)

if oRaiz.Sesion.Pedidos > 0 then
    iConPedidos = 1
else
    iConPedidos = 0
end if
iConSolicitudes = oRaiz.Sesion.PmSolicitudes
iConCertificados = oRaiz.Sesion.Certificados
iConNoConformidades = oRaiz.Sesion.NoConformidades

set oRsMenusPadreExternos = oRaiz.DevolverMenusUrlsExternas(CStr(Idioma))
set oRsSubMenusExternos = oRaiz.DevolverSubMenusUrlsExternas(CStr(Idioma))
set oCompania = oRaiz.devolverDatosCia (oRaiz.Sesion.CiaId, Application("PORTAL"), false)
set oUsuario = oRaiz.devolverDatosUsuario (oRaiz.Sesion.UsuId, oRaiz.Sesion.CiaId, Application("PORTAL"))

set paramGen = oRaiz.DevolverParamGSPedidos(oRaiz.Sesion.CiaComp)

bActivarMenuAbiertos= oRaiz.ActivadoPedidosAbiertos (oRaiz.Sesion.CiaComp,Idioma) and paramGen(3)

iFSGAUsu = oRaiz.Sesion.SolicitudesFsga 
iBajaLog=oRaiz.Sesion.BajaCalidad
iHayVarCalPub=oRaiz.Sesion.QaVarCal 
iUnaCompradora = application("UNACOMPRADORA")
iRegistroExterno = application("REGISTRO_EXTERNO")

if not bActivarMenuAbiertos then
    sPedidosPendientes=Server.HTMLEncode(denMenu(15))
    sSeguimiento=Server.HTMLEncode(denMenu(16))
else
    sPedidosPendientes=Server.HTMLEncode(denMenu(37))
    sSeguimiento=Server.HTMLEncode(denMenu(38))
    sSeguimientoAbiertos=Server.HTMLEncode(denMenu(39))
end if

'Recupera los par�metros de activaci�n desde las cookies
set paramgen = oRaiz.Sesion.Parametros

if paramgen.DatosUsu then
    iDatosUsu = 1
else
    iDatosUsu = 0
end if
if paramgen.AdminUsu then
    iAdminUsu = 1
else
    iAdminUsu = 0
end if
if paramgen.DatosCia then
    iDatosCia = 1
else
    iDatosCia = 0
end if
if paramgen.ActivCia then
    iActivCia = 1
else
    iActivCia = 0
end if
if  paramgen.AdjunCia then
    iAdjunCia = 1
else
    iAdjunCia = 0
end if


bAccesoFSGS=paramgen.AccesoFSGS and (oRaiz.Sesion.CiaCodGs<>"")

bAccesoFSFA=paramgen.AccesoFSFA
if bAccesoFSFA then 
    bAccesoFSFA =(oRaiz.EsContactoEnGS(oRaiz.Sesion.CiaComp,oRaiz.Sesion.UsuId,oRaiz.Sesion.CiaCodGs))    
end if
bAutofactura=false
    
set paramgen = nothing

iUsuPpal = oRaiz.Sesion.UsuPpal
iUsuId = oRaiz.Sesion.UsuId
iCiaId = oRaiz.Sesion.CiaId 
iCiaCompId = oRaiz.Sesion.CiaComp
sCodProveGS=oRaiz.Sesion.CiaCodGs
if bAccesoFSFA then
    bAutofactura = (oRaiz.Sesion.Autofactura = 1)
end if
set oSolicitudes = oRaiz.Generar_CSolicitudes

iNumSolQA_Alta=0
ContactoGS = oRaiz.ContactoGS(oRaiz.Sesion.CiaComp,oRaiz.Sesion.UsuId,oRaiz.Sesion.CiaCodGs)
iNumSolQA_Alta=oSolicitudes.DevolverNumSolicitudesPorTipo_Alta(iCiaCompId, sCodProveGS, "14",Idioma, ContactoGS)
if iNumSolQA_Alta=0 then
    bSolQA_Alta = false
else
    bSolQA_Alta = true
end if

iNumSolQA=0

iNumSolQA=oSolicitudes.DevolverNumSolicitudesPorTipo(iCiaCompId, sCodProveGS, "14")
if iNumSolQA=0 then
    bSolQA = false
else
    bSolQA = true
end if
iNumEncuestas=0

iNumEncuestas_Alta=0
iNumEncuestas_Alta=oSolicitudes.DevolverNumSolicitudesPorTipo_Alta(iCiaCompId, sCodProveGS, "12",Idioma, ContactoGS)
if iNumEncuestas_Alta=0 then
    bEncuestas_Alta = false
else
    bEncuestas_Alta = true
end if

iNumEncuestas=oSolicitudes.DevolverNumSolicitudesPorTipo(iCiaCompId, sCodProveGS, "12")
if iNumEncuestas=0 then
    bEncuestas = false
else
    bEncuestas = true
end if

%>


//
// Cambiar las variables aqui. Los contenidos de los menus
// se cambian en leorarrays.js. Puede tomarse una de las opciones
// iniciales como "especial" (diferente color, diferentes fuentes, etc)
// esto sirve para resaltar una apartado de los menus
//

// URL "base" para generar los enlaces (usado en leorarrays.js)
// si es la misma que el directorio actual poner un ""

sRootPath = "";  

// ancho de cada menu
menuWidth = 200;

// Tanto porciento de recubrimiento de un menu primario por uno secundario
childOverlap = 0;

// Numero de pixels en la direccion Y entre la posicion de la opci�n y 
// la ventana que muestra las subopciones
childOffset = 0;

// Similar a childOverlap, para que sea childOverlap el que "mande"
// poner perCentOver = null;
perCentOver = null;

// Segundos que es visible una ventana tras quitar el raton de ella
secondsVisible = 0.2;

// Fuente de los menus 
// Color
fntCol = "<%=cVTEXTO%>";
// Tamanio
fntSiz = "<%=MENUFONTSIZE%>";
// Negita? true=si, false=no
fntBold = false;
// Italica? true=si, false=no
fntItal = false;
// Tipo de fuente (Arial, courier, helvetica...)
fntFam = "<%=fMENUVERTICAL%>";



// Fondo de fondo de los menus
// Color
backCol = "<%=cVNORMAL%>"
// Color de la opcion especial
activeBackColor = "#DDDDDD" 
// Color cuando se se�ala una opcion
overCol = "<%=cVACT%>";
// Color de la fuente cuando se se�ala una opcion
overFnt = "<%=cVTEXTOOVER%>";

// Bordes
// Grueso de los bordes
borWid = 1;
// Color de los bordes
borCol = "black"
// Color del borde de la opcion especial
activeBorderColor = "black" 
// Tipo de borde
borSty = "solid";
// "Padding" entre el texto y el borde
itemPad = 2;

// Imagen que indica subopcion
imgSrc = "triangle.gif";
// tama�o x e y de dicha imagen 
imgSiz = 10;

// Ancho de la linea que separa las opciones
separator = 1;
// color de dicha linea
separatorCol = "black";
// color de dicha linea en el menu especial
activeSeparatorColor = "black" 

// Para ampliar informacion sobre como adaptar este JS en el 
// caso de tener frames, consultar frames.txt
// �Esta en un frame? (true,false)?
isFrames = false;      
// Alineacion (top,left,bottom,right) 
navFrLoc = "left";   
// Frame donde iran a parar los enlaces
mainFrName="_self";
  

keepHilite = true;
// false = desaparece el texto al se�alar una opcion
NSfontOver = true;
// false = No hace falta clicar para desplegar los menus
clickStart = false;
// false = No hace falta clicar para que desaparezcan los menus
clickKill = false;

// Numero del menu especial en leorarrays.js (los menus son arMenuX, indicar aqui
// el numero X)
activeMenu = 0;			// <-- Este es el menu activo y que sale de forma diferente

   NS4 = (document.layers);
   IE4 = (document.all);
  ver4 = (NS4 || IE4);
 isMac = (navigator.appVersion.indexOf("Mac") != -1);
isMenu = (NS4 || (IE4 && !isMac));

function popUp(){return};
function popDown(){return};

if (!ver4) event = null;

if (isMenu) {
	menuVersion = 5;
}


//Drop Down Tabs Menu- Author: Dynamic Drive (http://www.dynamicdrive.com)
//Created: May 16th, 07'

var tabdropdown={
	disappeardelay: 200, //set delay in miliseconds before menu disappears onmouseout
	disablemenuclick: false, //when user clicks on a menu item with a drop down menu, disable menu item's link?
	enableiframeshim: 1, //1 or 0, for true or false
    
	//No need to edit beyond here////////////////////////
	dropmenuobj: null, ie: document.all, firefox: document.getElementById&&!document.all, previousmenuitem:null,
	currentpageurl: window.location.href.replace("http://"+window.location.hostname, "").replace(/^\//, ""), //get current page url (minus hostname, ie: http://www.dynamicdrive.com/)

	getposOffset:function(what, offsettype){
		var totaloffset=(offsettype=="left")? what.offsetLeft : what.offsetTop;
		var parentEl=what.offsetParent;
			while (parentEl!=null){
				totaloffset=(offsettype=="left")? totaloffset+parentEl.offsetLeft : totaloffset+parentEl.offsetTop;
				parentEl=parentEl.offsetParent;
			}
		return totaloffset;
	},

	showhide:function(obj, e, obj2){ //obj refers to drop down menu, obj2 refers to tab menu item mouse is currently over
		if (this.ie || this.firefox)
			this.dropmenuobj.style.left=this.dropmenuobj.style.top="-500px"
		if (e.type=="click" && obj.visibility==hidden || e.type=="mouseover"){
			if (obj2.parentNode.className.indexOf("default")==-1) //if tab isn't a default selected one
				obj2.parentNode.className="hover"
			obj.visibility="visible"
			}
		else if (e.type=="click")
			obj.visibility="hidden"
	},

	iecompattest:function(){
		return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
	},

	clearbrowseredge:function(obj, whichedge){
		var edgeoffset=0
		if (whichedge=="rightedge"){
			var windowedge=this.ie && !window.opera? this.standardbody.scrollLeft+this.standardbody.clientWidth-15 : window.pageXOffset+window.innerWidth-15
			this.dropmenuobj.contentmeasure=this.dropmenuobj.offsetWidth
		if (windowedge-this.dropmenuobj.x < this.dropmenuobj.contentmeasure)  //move menu to the left?
			edgeoffset=this.dropmenuobj.contentmeasure-obj.offsetWidth
		}
		else{
			var topedge=this.ie && !window.opera? this.standardbody.scrollTop : window.pageYOffset
			var windowedge=this.ie && !window.opera? this.standardbody.scrollTop+this.standardbody.clientHeight-15 : window.pageYOffset+window.innerHeight-18
			this.dropmenuobj.contentmeasure=this.dropmenuobj.offsetHeight
			if (windowedge-this.dropmenuobj.y < this.dropmenuobj.contentmeasure){ //move up?
				edgeoffset=this.dropmenuobj.contentmeasure+obj.offsetHeight;
				if ((this.dropmenuobj.y-topedge)< this.dropmenuobj.contentmeasure) //up no good either
					edgeoffset=this.dropmenuobj.y+obj.offsetHeight-topedge
			}
			this.dropmenuobj.firstlink.style.borderTopWidth=(edgeoffset==0)? 0 : "1px" //Add 1px top border to menu if dropping up
		}
		return edgeoffset
	},

	dropit:function(obj, e, dropmenuID){
		if (this.dropmenuobj!=null){ //hide previous menu
			this.dropmenuobj.style.visibility="hidden" //hide menu
			if (this.previousmenuitem!=null && this.previousmenuitem!=obj){
				if (this.previousmenuitem.parentNode.className.indexOf("default")==-1) //If the tab isn't a default selected one
					this.previousmenuitem.parentNode.className=""
			}
		}
		this.clearhidemenu()
		if (this.ie||this.firefox){
			obj.onmouseout=function(){tabdropdown.delayhidemenu(obj)}
			obj.onclick=function(){return !tabdropdown.disablemenuclick} //disable main menu item link onclick?
			this.dropmenuobj=document.getElementById(dropmenuID)
			this.dropmenuobj.onmouseover=function(){tabdropdown.clearhidemenu()}
			this.dropmenuobj.onmouseout=function(e){tabdropdown.dynamichide(e, obj)}
			this.dropmenuobj.onclick=function(){tabdropdown.delayhidemenu(obj)}
			this.showhide(this.dropmenuobj.style, e, obj)
			this.dropmenuobj.x=this.getposOffset(obj, "left")
			this.dropmenuobj.y=this.getposOffset(obj, "top")
			this.dropmenuobj.style.left=this.dropmenuobj.x-this.clearbrowseredge(obj, "rightedge")+"px"
			this.dropmenuobj.style.top=this.dropmenuobj.y-this.clearbrowseredge(obj, "bottomedge")+obj.offsetHeight+1+"px"
			this.previousmenuitem=obj //remember main menu item mouse moved out from (and into current menu item)
			this.positionshim() //call iframe shim function
		}
	},

	contains_firefox:function(a, b) {
		while (b.parentNode)
		if ((b = b.parentNode) == a)
			return true;
		return false;
	},

	dynamichide:function(e, obj2){ //obj2 refers to tab menu item mouse is currently over
		var evtobj=window.event? window.event : e
		if (this.ie&&!this.dropmenuobj.contains(evtobj.toElement))
			this.delayhidemenu(obj2)
		else if (this.firefox&&e.currentTarget!= evtobj.relatedTarget&& !this.contains_firefox(evtobj.currentTarget, evtobj.relatedTarget))
			this.delayhidemenu(obj2)
	},

	delayhidemenu:function(obj2){
		this.delayhide=setTimeout(function(){tabdropdown.dropmenuobj.style.visibility='hidden'; if (obj2.parentNode.className.indexOf('default')==-1) obj2.parentNode.className=''},this.disappeardelay) //hide menu
	},

	clearhidemenu:function(){
		if (this.delayhide!="undefined")
			clearTimeout(this.delayhide)
	},

	positionshim:function(){ //display iframe shim function
		if (this.enableiframeshim && typeof this.shimobject!="undefined"){
			if (this.dropmenuobj.style.visibility=="visible"){
				this.shimobject.style.width=this.dropmenuobj.offsetWidth+"px"
				this.shimobject.style.height=this.dropmenuobj.offsetHeight+"px"
				this.shimobject.style.left=this.dropmenuobj.style.left
				this.shimobject.style.top=this.dropmenuobj.style.top
			}
		this.shimobject.style.display=(this.dropmenuobj.style.visibility=="visible")? "block" : "none"
		}
	},

	hideshim:function(){
		if (this.enableiframeshim && typeof this.shimobject!="undefined")
			this.shimobject.style.display='none'
	},

isSelected:function(menuurl){
	var menuurl=menuurl.replace("http://"+menuurl.hostname, "").replace(/^\//, "")
	return (tabdropdown.currentpageurl==menuurl)
},

	init:function(menuid, dselected){
		this.standardbody=(document.compatMode=="CSS1Compat")? document.documentElement : document.body //create reference to common "body" across doctypes
		var menuitems=document.getElementById(menuid).getElementsByTagName("a")
		for (var i=0; i < menuitems.length; i++){
			if (menuitems[i].getAttribute("rel")){
				var relvalue=menuitems[i].getAttribute("rel")
				document.getElementById(relvalue).firstlink=document.getElementById(relvalue).getElementsByTagName("a")[0]
				menuitems[i].onmouseover=function(e){
					var event=typeof e!="undefined"? e : window.event
					tabdropdown.dropit(this, event, this.getAttribute("rel"))
				}
			}
			if (dselected=="auto" && typeof setalready=="undefined" && this.isSelected(menuitems[i].href)){
				 menuitems[i].parentNode.className+=" selected default"
				var setalready=true
			}
			else if (parseInt(dselected)==i){
				menuitems[i].parentNode.className+=" selected default"
            }
				
		}
	}

}

function trim(sValor)
{
    var re = / /

    var i

    var sTmp = sValor

    while (sTmp.search(re)==0)
	    sTmp = sTmp.replace(re,"")

    var sRev = ""
    for (i=0;i<sTmp.length;i++)
	    sRev=sTmp.charAt(i) + sRev


    while (sRev.search(re)==0)
	    sRev = sRev.replace(re,"")
	
    sTmp=""
    for (i=0;i<sRev.length;i++)
	    sTmp=sRev.charAt(i) + sTmp

    return(sTmp)

}

var windowOpen = window.open;
//override de window open para que el popup se abra centrado
//y advierta si est� activado el bloqueo de popup
window.open = function (url, name, features) {
    var params = new Array();
    //extraer especificaciones
    if (features) {
        var specArr = features.split(",");
        for (i = 0; i < specArr.length; i++) {
            var spec = specArr[i];
            var key = trim(spec.split("=")[0]);
            var value = trim(spec.split("=")[1]);
            params[key] = value;
        }
        if ((params["width"] || params["height"])) {
            if (params["left"] == undefined) params["left"] = (screen.width - (params["width"] == undefined ? (screen.width / 2) : params["width"])) / 2;
            if (params["top"] == undefined) params["top"] = (screen.height - (params["height"] == undefined ? (screen.height / 2) : params["height"])) / 2;
        }
    }
    features = "";
    for (i in params) {
        features += i + "=" + params[i] + ",";
    }
    features = features.substring(0, features.length - 1);
    var popUp = windowOpen(url, name, features);
    //Tarea 3301 : Si el bloqueo de popups est� activado mostramos una alerta ya que no se van a poder abrir
    if (popUp == null || typeof (popUp) == 'undefined') {
		if (navigator.userAgent.toLowerCase().indexOf('safari/') > -1)
        	alert(sBloqueoPopupActivado);
        return false;
    }
    popUp.focus();
    return popUp;
};

function windowopen(url, name, specs) {
    return window.open(url,name,specs);
} 


// <summary>
// Modificar los datos personales
// </summary>
// <remarks>Llamada desde: a href modif datos personales; Tiempo m�ximo: 0,2</remarks>
function irADatosPersonales()
{
    window.open("<%=Application("RUTASEGURA")%>script/modifdatos/modificardatos.asp","default_main");
};
function irACambioPassword(){
    window.open("<%=Application("RUTASEGURA")%>script/login/changePassword.asp","default_main");
};
function irAFormatos()
{
	window.open("<%=Application("RUTASEGURA")%>script/common/opciones.asp?Idioma=<%=Idioma%>","winFormatos","width=500,height=200,resizable=0");
};
// <summary>
// Modificar los datos de la compania
// </summary>
// <remarks>Llamada desde: a href modif datos compania; Tiempo m�ximo: 0,2</remarks>
function irADatosCia()
{
    window.open("<%=Application("RUTASEGURA")%>script/usuppal/modifcompania.asp","default_main");
};
// <summary>
// Modificar los datos de las Actividades de la compania
// </summary>
// <remarks>Llamada desde: a href modif datos Actividades compania; Tiempo m�ximo: 0,2</remarks>
function irAActividadesCia()
{
    window.open("<%=Application("RUTASEGURA")%>script/usuppal/modifActividades.asp","default_main")
}
// <summary>
// Modificar los usuarios de la compania
// </summary>
// <remarks>Llamada desde: a href modif datos compania; Tiempo m�ximo: 0,2</remarks>
function irAAdminUsuarios()
{
window.open("<%=Application("RUTASEGURA")%>script/usuppal/admonusuarios.asp","default_main")
}
// <summary>
// Cambiar el registro de la compania
// </summary>
// <remarks>Llamada desde: a href Cambiar registro compania; Tiempo m�ximo: 0,2</remarks>
function irAModifRegistro()
{
window.open("<%=Application("RUTASEGURA")%>script/usuppal/modifregcompradoras.asp" ,"default_main")
}
// <summary>
// Cambiar el usuario principal de la compania
// </summary>
// <remarks>Llamada desde: a href Cambiar usuario principal compania; Tiempo m�ximo: 0,2</remarks>
function irACambiarPpal()
{
window.open("<%=Application("RUTASEGURA")%>script/usuppal/usuarioprincipal.asp" ,"default_main")
}
// <summary>
// Adjuntar los archivos de la compania
// </summary>
// <remarks>Llamada desde: a href Adjuntar archivos compania; Tiempo m�ximo: 0,2</remarks>
function irAAdjuntarDoc()
{
window.open("<%=Application("RUTASEGURA")%>script/usuppal/especificaciones.asp" ,"default_main")
}

function GetCookie (name) {  
var arg = name + "=";  
var alen = arg.length;  
var clen = document.cookie.length;  
var cookies = document.cookie.split("&")
re = eval ("/" + name + "=/")
re2 = /=/
var i = 0;  

for (i = 0;i<=cookies.length-1;i ++)
	{
	if (cookies[i].search(re)==0)
		{
		ret = cookies[i].substr(cookies[i].search(re2)+ 1,1)
		return(ret)
		}
	}


}

var NifProve='<%=oCompania.Nif%>';
var NifUsuario='<%=oUsuario.Nif%>';
<%if iConPedidos = 1 then %>

	function irAPedidosPendientes()
	{
    
	<%if request.cookies("USU_MOSTRARFMT") = 0 then%>
		window.open("<%=Application("RUTASEGURA")%>script/pedidos/pedidos.asp?Idioma=<%=Idioma%>","default_main")
	<%else%>
		if (GetCookie("MOSTRARFMT")==1)
			{
			setTimeout('window.open("<%=Application("RUTASEGURA")%>script/pedidos/pedidos.asp?Idioma=<%=Idioma%>","default_main")',200)
			winFormat = window.open("<%=Application("RUTASEGURA")%>script/common/introformatos.asp?Idioma=<%=Idioma%>&modificar=-1&func=pedidos", "winFormat", "width=600,height=400,location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no")
			winFormat.focus()
			}
		else
			window.open("<%=Application("RUTASEGURA")%>script/pedidos/pedidos.asp?Idioma=<%=Idioma%>","default_main")
	<%end if%>
	}

	function irASeguimientoPedidos(bPedidosAbiertos)
	{
    var iAbierto=(bPedidosAbiertos?1:0);
    
	<%if request.cookies("USU_MOSTRARFMT") = 0 then%>
		window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/seguimpedidos.asp?Idioma=<%=Idioma%>&PedidosAbiertos=" + iAbierto,"default_main")
	<%else%>
		if (GetCookie("MOSTRARFMT")==1)
			{
            setTimeout('window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/seguimpedidos.asp?Idioma=<%=Idioma%>&PedidosAbiertos=" + iAbierto,"default_main")',200);    
			winFormat = window.open("<%=Application("RUTASEGURA")%>script/common/introformatos.asp?Idioma=<%=Idioma%>&modificar=-1&func=seguimpedidos", "winFormat", "width=600,height=400,location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no")
			winFormat.focus()
			}
		else{
            window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/seguimpedidos.asp?Idioma=<%=Idioma%>&PedidosAbiertos=" + iAbierto,"default_main");
        }
	<%end if%>
	}

	//Revisado por: blp. Fecha: 18/01/2012
	//M�todo que redirige al listado de pedidos entregados y pendientes
	//Llamada desde: nmenu3. M�x:0,1 seg
	function irAEntregaPedidos()
	{
        window.open("<%=Application("RUTASEGURA")%>script/common/pasarelaPMPortalWeb.asp?Idioma=<%=Idioma%>&Pagina=3","default_main");	
	}


<%end if%>

<%if iConSolicitudes then%>

<%end if%>

function irAOfertas()
{
<%if request.cookies("USU_MOSTRARFMT") = 0 then%>
	window.open("<%=Application("RUTASEGURA")%>script/solicitudesoferta/solicitudesoferta.asp?Idioma=<%=Idioma%>","default_main")
<%else%>
	if (GetCookie("MOSTRARFMT")==1)
		{
		setTimeout('window.open("<%=Application("RUTASEGURA")%>script/solicitudesoferta/solicitudesoferta.asp?Idioma=<%=Idioma%>","default_main")',200)
		winFormat = window.open("<%=Application("RUTASEGURA")%>script/common/introformatos.asp?Idioma=<%=Idioma%>&modificar=-1&func=ofertas", "winFormat", "width=600,height=400,location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no")
		winFormat.focus()
		}
	else
		{
		window.open("<%=Application("RUTASEGURA")%>script/solicitudesoferta/solicitudesoferta.asp?Idioma=<%=Idioma%>","default_main")
		}

<%end if%>


}

function irAOfertasHistoricas()
{
<%if request.cookies("USU_MOSTRARFMT") = 0 then%>
	window.open("<%=Application("RUTASEGURA")%>script/PMPortal/script/ofertas/VisorOfertas.aspx", "default_main")
<%else%>
	if (GetCookie("MOSTRARFMT")==1)
		{
		setTimeout('window.open("<%=Application("RUTASEGURA")%>script/PMPortal/script/ofertas/VisorOfertas.aspx","default_main")',200)
		winFormat = window.open("<%=Application("RUTASEGURA")%>script/common/introformatos.asp?Idioma=<%=Idioma%>&modificar=-1&func=ofertas", "winFormat", "width=600,height=400,location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no")
		winFormat.focus()
		}
	else
		window.open("<%=Application("RUTASEGURA")%>script/PMPortal/script/ofertas/VisorOfertas.aspx","default_main")
<%end if%>


}

function irASolicitudes()
{	
    window.open("<%=Application("RUTASEGURA")%>script/common/pasarelaPMPortalWeb.asp?Idioma=<%=Idioma%>&Pagina=4","default_main");
}

function irACertificados()
{	
    window.open("<%=Application("RUTASEGURA")%>script/common/pasarelaPMPortalWeb.asp?Idioma=<%=Idioma%>&Pagina=1","default_main");
}


function irANoConformidades()
{
<%if request.cookies("USU_MOSTRARFMT") = 0 then%>
	window.open("<%=Application("RUTASEGURA")%>script/noconformidades/noconformidades.asp?Idioma=<%=Idioma%>","default_main")
<%else%>
	if (GetCookie("MOSTRARFMT")==1)
		{
		setTimeout('window.open("<%=Application("RUTASEGURA")%>script/noconformidades/noconformidades.asp?Idioma=<%=Idioma%>","default_main")',200)
		winFormat = window.open("<%=Application("RUTASEGURA")%>script/common/introformatos.asp?Idioma=<%=Idioma%>&modificar=-1&func=ofertas", "winFormat", "width=600,height=400,location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no")
		winFormat.focus()
		}
	else
		{
		window.open("<%=Application("RUTASEGURA")%>script/noconformidades/noconformidades.asp?Idioma=<%=Idioma%>","default_main")
		}
<%end if%>
}

function irAVisorAutofacturas()
{
	window.open("<%=Application("RUTASEGURA")%>script/PMPortal/script/facturas/extFrames.aspx", "default_main")
}

function irAAltaFacturas()
{
    window.open("<%=Application("RUTASEGURA")%>script/PMPortal/script/solicitudes/extFrames3.aspx?TipoSolicitud=13", "default_main")
}

function irASeguimientoFacturas()
{
	window.open("<%=Application("RUTASEGURA")%>script/PMPortal/script/solicitudes/extFrames2.aspx?TipoVisor=5", "default_main")
}

function irAVariableCalidad()
{    
    window.open("<%=Application("RUTASEGURA")%>script/common/pasarelaPMPortalWeb.asp?Idioma=<%=Idioma%>&Pagina=2","default_main");
}

function irAHistoricoPedidos()
{
	window.open("<%=Application("RUTASEGURA")%>script/PMPortal2008/script/pedidos/Pedidos.aspx", "default_main")
}

function irAAceptaciones()
{
	window.open("<%=Application("RUTASEGURA")%>script/PMPortal2008/script/pedidos/Aceptaciones.aspx", "default_main")
}
function irASolicitudesQA()
{    
    window.open("<%=Application("RUTASEGURA")%>script/common/pasarelaPMPortalWeb.asp?Idioma=<%=Idioma%>&Pagina=11","default_main");
}
function irAEncuestas()
{    
    window.open("<%=Application("RUTASEGURA")%>script/common/pasarelaPMPortalWeb.asp?Idioma=<%=Idioma%>&Pagina=12","default_main");
}
function irASolicitudesQA_Alta()
{    
    window.open("<%=Application("RUTASEGURA")%>script/PMPortal/script/solicitudes/extFrames3.aspx?TipoSolicitud=14", "default_main");
}
function irAEncuestas_Alta()
{    
    window.open("<%=Application("RUTASEGURA")%>script/PMPortal/script/solicitudes/extFrames3.aspx?TipoSolicitud=12", "default_main");
}
//funcion que abrira una nueva pagina con la url indicada
//sUrl: Url que se abrira en una nueva pagina
function irAUrlExterna(sUrl,TipoLlamada){
switch(TipoLlamada)
    {
    case 2: //Llamada a URL con NIF como parametro.
    case 4: //Llamada a URL con NIF del usuario como parametro.  
    case 5: //Llamada a URL pasando usuario y contrase�a, con NIF del proveedor como parametro.
        window.open(sUrl , "_blank","directories=no,location=no,menubar=no,status=no,toolbar=no,width=850,height=630,resizable=yes,titlebar=no");
        break;
    case 3: //Llamada a URL
        if(top.loaded){
            window.open(sUrl, "_blank","width=850,height=630, resizable=yes");
        }else{
            alert('<%=JSText(denMenu(36))%>');
        }
        break;
    case 9: //Llamada a URL de la aplicaci�n.  
        window.open(sUrl, "default_main");
        break;
    }
}
//funcion que abrira un visor personalizado, pero en el mismo portal
//sPaginaCustom: Nombre del visor personalizado
function irAVisorPersonalizado(sPaginaCustom){
    window.open("<%=Application("RUTASEGURA")%>script/PMPortal/script/custom/" + sPaginaCustom, "default_main");
}
function irURLExternaBlank(url){
    window.open("<%=Application("RUTASEGURA")%>script/PMPortal/script/custom/" + url, "_blank","height=800,width=1000,resizable=yes");
}
function abrirURL(url){
    window.open("<%=Application("RUTASEGURA")%>script/PMPortal/script/custom/" + url, "_blank","height=800,width=1000,resizable=yes,scrollbars=yes");
}
// NUEVO MENU CSS
//Creamos los submenus externos

var arrSubMenus=new Array()
var arrPagCustomSubMenus=new Array()
var arrPadreSubMenus=new Array()

var ind
ind=-1
var idMenuPadreOld
idMenuPadreOld=-1
var idMenuPadreActual
var tipoLlamada 
var url
var post
var paginaCustom //Visor Personalizado
var llamada
        
<%
'Creamos los submenus externos(Con llamadas a urls externas)
Dim DenomSubMenu
Do While NOT oRsSubMenusExternos.EOF%>
    idMenuPadreActual=<%=oRsSubMenusExternos("MENU_PADRE")%>;
    tipoLlamada=<%=oRsSubMenusExternos("TIPO_LLAMADA")%>;
    <%
    if oRsSubMenusExternos("TIPO_LLAMADA")=3 then         
        set oRsLoginJDE = oRaiz.DevolverUsuarioJDE(Request.Cookies("USU_SESIONID"))
        if not oRsLoginJDE.eof then
        %>
            usuarioJDE ='<%=oRsLoginJDE("COD")%>';
            passwordJDE='<%=oRsLoginJDE("PWD")%>';
        <%else%>
            usuarioJDE ='';
            passwordJDE='';
        <%end if
    else%>
        usuarioJDE ='';
        passwordJDE='';
    <%end if%>
    url='<%=oRsSubMenusExternos("URL")%>';
    <%DenomSubMenu=oRsSubMenusExternos("DEN") %>


    //guardamos en un array la relcaci�n entre el id padre y el index
    if(idMenuPadreActual!=idMenuPadreOld){
        arrPadreSubMenus.push(<%=oRsSubMenusExternos("MENU_PADRE")%>)
    }
        
    //Metemos el HTML de los submenus de cada menu PADRE en el array "arrSubMenus"
    switch(tipoLlamada)
    {
    case 1: //Llamada a visor personalizado desarrollado por FULLSTEP
        paginaCustom ='<%=oRsSubMenusExternos("PAGINA_CUSTOM")%>'
        llamada="href='javascript:irAVisorPersonalizado(\""+paginaCustom+"\")'"
        if(idMenuPadreActual!=idMenuPadreOld){
            arrSubMenus.push("<a "+llamada+"><span><%=Server.HTMLEncode(DenomSubMenu)%></span></a>")
            //Metemos los diferentes visores personalizados que se llaman en los submenus de cada menu PADRE    
            arrPagCustomSubMenus.push(paginaCustom)
            ind+=1  
        }else{
            arrSubMenus[ind]= arrSubMenus[ind] + "<a "+llamada+"><span><%=Server.HTMLEncode(DenomSubMenu)%></span></a>" 
            //Metemos los diferentes visores personalizados que se llaman en los submenus de cada menu PADRE
            arrPagCustomSubMenus[ind]=arrPagCustomSubMenus[ind] + '; ' + paginaCustom
        }          
        break;
    case 2: //Llamada a URL con NIF como parametro.
    case 9: //Llamada a URL de la aplicaci�n.  
        url+= NifProve;
        llamada="href='javascript:irAUrlExterna(\""+url+"\"," + tipoLlamada + ")'"  
        if(idMenuPadreActual!=idMenuPadreOld){
            arrSubMenus.push("<a "+llamada+"><span><%=Server.HTMLEncode(DenomSubMenu)%></span></a>")    
            arrPagCustomSubMenus.push('')
            ind+=1
        }else{
            arrSubMenus[ind]= arrSubMenus[ind] + "<a "+llamada+"><span><%=Server.HTMLEncode(DenomSubMenu)%></span></a>" 
        }
        break;
    case 3: //Llamada a URL.      
        paginaCustom ='<%=oRsSubMenusExternos("PAGINA_CUSTOM")%>';        
	    top.url=url;
	    top.paginaCustom=paginaCustom;
        if (usuarioJDE=='' || passwordJDE==''){
            llamada="href='javascript:alert(\"<%=JSText(denMenu(35))%>\");'";
        }else{
            llamada="href='javascript:irAUrlExterna(\""+ url + paginaCustom +"\",3)'";
            top.frames['fraJDELogin'].location.href = url;
            top.login=true;
            top.loaded=false;
        }
        if(idMenuPadreActual!=idMenuPadreOld){
            arrSubMenus.push("<a "+llamada+"><span><%=Server.HTMLEncode(DenomSubMenu)%></span></a>")    
            arrPagCustomSubMenus.push('')
            ind+=1
        }else{
            arrSubMenus[ind]= arrSubMenus[ind] + "<a "+llamada+"><span><%=Server.HTMLEncode(DenomSubMenu)%></span></a>"
        }
        break;
    case 4: //Llamada a URL con NIF de usuario como parametro.
        url+= NifUsuario
        llamada="href='javascript:irAUrlExterna(\""+url+"\",4)'"  
        if(idMenuPadreActual!=idMenuPadreOld){
            arrSubMenus.push("<a "+llamada+"><span><%=Server.HTMLEncode(DenomSubMenu)%></span></a>")    
            arrPagCustomSubMenus.push('')
            ind+=1
        }else{
            arrSubMenus[ind]= arrSubMenus[ind] + "<a "+llamada+"><span><%=Server.HTMLEncode(DenomSubMenu)%></span></a>" 
        }
        break;
    case 5: //Llamada a URL pasando usuario y contrasena, con parametro NIF del proveedor con un formato determinado.

        <% if (not isNull(oRsSubMenusExternos("PLANTILLA_NIF")) and not isNull(oRsSubMenusExternos("USUARIO")) and not isNull(oRsSubMenusExternos("PWD"))) then %>      
            post='<%=Replace(oRsSubMenusExternos("PLANTILLA_NIF"),"FSNIF",oCompania.Nif)+ oRsSubMenusExternos("USUARIO") + oRsSubMenusExternos("PWD")%>';
            url+=post
        <% end if %>
    
        llamada="href='javascript:irAUrlExterna(\""+url+"\",5)'"
           
        if(idMenuPadreActual!=idMenuPadreOld){
            arrSubMenus.push("<a "+llamada+"><span><%=Server.HTMLEncode(DenomSubMenu)%></span></a>")    
            arrPagCustomSubMenus.push('')
            ind+=1
        }else{
            arrSubMenus[ind]= arrSubMenus[ind] + "<a "+llamada+"><span><%=Server.HTMLEncode(DenomSubMenu)%></span></a>" 
        }
        break;   
    case 6:        
        paginaCustom =''
        llamada="href='javascript:abrirURL(\""+url+"\")'"
        if(idMenuPadreActual!=idMenuPadreOld){
            arrSubMenus.push("<a "+llamada+"><span><%=Server.HTMLEncode(DenomSubMenu)%></span></a>")
            //Metemos los diferentes visores personalizados que se llaman en los submenus de cada menu PADRE    
            arrPagCustomSubMenus.push(url)
            ind+=1  
        }else{
            arrSubMenus[ind]= arrSubMenus[ind] + "<a "+llamada+"><span><%=Server.HTMLEncode(DenomSubMenu)%></span></a>" 
            //Metemos los diferentes visores personalizados que se llaman en los submenus de cada menu PADRE
            arrPagCustomSubMenus[ind]=arrPagCustomSubMenus[ind] + '; ' + url
        }     
        break;
    default:        
        paginaCustom =''
        llamada="href='javascript:irURLExternaBlank(\""+url+"\")'"
        if(idMenuPadreActual!=idMenuPadreOld){
            arrSubMenus.push("<a "+llamada+"><span><%=Server.HTMLEncode(DenomSubMenu)%></span></a>")
            //Metemos los diferentes visores personalizados que se llaman en los submenus de cada menu PADRE    
            arrPagCustomSubMenus.push(url)
            ind+=1  
        }else{
            arrSubMenus[ind]= arrSubMenus[ind] + "<a "+llamada+"><span><%=Server.HTMLEncode(DenomSubMenu)%></span></a>" 
            //Metemos los diferentes visores personalizados que se llaman en los submenus de cada menu PADRE
            arrPagCustomSubMenus[ind]=arrPagCustomSubMenus[ind] + '; ' + url
        }     
        break;
    }
    idMenuPadreOld=idMenuPadreActual
    
<% oRsSubMenusExternos.MoveNext
Loop
oRsSubMenusExternos.Close
%>


var nmenu1
nmenu1 = ""

<%if iDatosUsu = "1" And ((iRegistroExterno = 1 and oUsuario.Principal = 0) Or (iRegistroExterno = 0)) then%> 
nmenu1 = nmenu1 + "<a href='javascript:irADatosPersonales()'><span><%=Server.HTMLEncode(denMenu(7))%></span></a>"
<%end if%>
nmenu1 = nmenu1 + "<a href='javascript:irAFormatos()'><span><%=Server.HTMLEncode(denMenu(8))%></span></a>"
<%if iDatosUsu = "1" then%> 
nmenu1 = nmenu1 + "<a href='javascript:irACambioPassword()'><span><%=Server.HTMLEncode(denMenu(34))%></span></a>"
<%end if%>


//
// Opcion del menu 2
//
var nmenu2
nmenu2 = ""

<%if iDatosCia = "1" and ((iRegistroExterno = 1 and oUsuario.Principal = 0) Or (iRegistroExterno = 0)) then%>
nmenu2 = nmenu2 + "<a href='javascript:irADatosCia()'><span><%=Server.HTMLEncode(denMenu(9))%></span></a>"
<%end if%>

<%if iActivCia = "1" then%>
nmenu2 = nmenu2 + "<a href='javascript:irAActividadesCia()'><span><%=Server.HTMLEncode(denMenu(10))%></span></a>"
<%end if%>

<%if iAdjunCia = "1" then%>
nmenu2 = nmenu2 + "<a href='javascript:irAAdjuntarDoc()'><span><%=Server.HTMLEncode(denMenu(19))%></span></a>"
<%end if%>

<%if iAdminUsu = "1"then%>
nmenu2 = nmenu2 + "<a href='javascript:irAAdminUsuarios()'><span><%=Server.HTMLEncode(denMenu(11))%></span></a>"
<%end if%>

<%if iUnaCompradora=0 then%>
nmenu2 = nmenu2 + "<a href='javascript:irAModifRegistro()'><span><%=Server.HTMLEncode(denMenu(12))%></span></a>"
<%end if%>

<%if iAdminUsu = "1" and ((iRegistroExterno = 1 and oUsuario.Principal = 0) Or (iRegistroExterno = 0)) then%>
nmenu2 = nmenu2 + "<a href='javascript:irACambiarPpal()'><span><%=Server.HTMLEncode(denMenu(13))%></span></a>"
<%end if%>

<%if iDatosCia <> "1" and iActivCia <> "1" and iAdjunCia <> "1" and iAdminUsu <> "1" then%>
//��??
<%end if%>


var nmenu3
nmenu3 = ""
<%if UCase(Application("NOMPORTAL"))=UCase("ono") then%>
nmenu3 = nmenu3 + "<a href='javascript:irAHistoricoPedidos()'><span><%=Server.HTMLEncode(denMenu(27))%></span></a>"
nmenu3 = nmenu3 + "<a href='javascript:irAAceptaciones()'><span><%=Server.HTMLEncode(denMenu(28))%></span></a>"
<%else%>

nmenu3 = nmenu3 + "<a href='javascript:irAPedidosPendientes()'><span><%=Server.HTMLEncode(sPedidosPendientes)%></span></a>"

nmenu3 = nmenu3 + "<a href='javascript:irASeguimientoPedidos(false)'><span><%=sSeguimiento%></span></a>"

<% if bActivarMenuAbiertos then %>
    nmenu3 = nmenu3 + "<a href='javascript:irASeguimientoPedidos(true)'><span><%=sSeguimientoAbiertos%></span></a>"
<% end if %>

nmenu3 = nmenu3 + "<a href='javascript:irAEntregaPedidos()'><span><%=Server.HTMLEncode(denMenu(30))%></span></a>"
<%end if%>


var nmenu4
nmenu4 = ""

<%if bAccesoFSGS then%>
nmenu4 = nmenu4 + "<a href='javascript:irAOfertas()'><span><%=Server.HTMLEncode(denMenu(20))%></span></a>"
nmenu4 = nmenu4 + "<a href='javascript:irAOfertasHistoricas()'><span><%=Server.HTMLEncode(denMenu(33))%></span></a>"
<%end if%>

<%if iConSolicitudes then%>
nmenu4 = nmenu4 + "<a href='javascript:irASolicitudes()'><span><%=Server.HTMLEncode(denMenu(21))%></span></a>"
<%end if%>

<%if not bAccesoFSGS and not iConSolicitudes then%>
//��??
<%end if%>


var nmenu5
nmenu5 = ""
<%if (iConCertificados or iConNoConformidades or iHayVarCalPub or bSolQA or bEncuestas or bSolQA_Alta or bEncuestas_Alta) and iBajaLog=0 then%>
	<%if iConCertificados then%>
	nmenu5 = nmenu5 + "<a href='javascript:irACertificados()'><span><%=Server.HTMLEncode(denMenu(22))%></span></a>"
	<%end if%>
	<%If iConNoConformidades then%>
	nmenu5 = nmenu5 + "<a href='javascript:irANoConformidades()'><span><%=Server.HTMLEncode(denMenu(26))%></span></a>"
	<%end if%>
	<%if iHayVarCalPub then%>
	nmenu5 = nmenu5 + "<a href='javascript:irAVariableCalidad()'><span><%=Server.HTMLEncode(denMenu(25))%></span></a>"
	<%end if%>
    <%if bEncuestas_Alta then%>
	nmenu5 = nmenu5 + "<a href='javascript:irAEncuestas_Alta()'><span><%=Server.HTMLEncode(denMenu(45))%></span></a>"
	<%end if%>
    <%if bEncuestas then%>
	nmenu5 = nmenu5 + "<a href='javascript:irAEncuestas()'><span><%=Server.HTMLEncode(denMenu(42))%></span></a>"
	<%end if%>
    <%if bSolQA_Alta then%>
	nmenu5 = nmenu5 + "<a href='javascript:irASolicitudesQA_Alta()'><span><%=Server.HTMLEncode(denMenu(44))%></span></a>"
	<%end if%>
    <%if bSolQA then%>
	nmenu5 = nmenu5 + "<a href='javascript:irASolicitudesQA()'><span><%=Server.HTMLEncode(denMenu(43))%></span></a>"
	<%end if%>
<%end if%>

var nmenu6
nmenu6 = ""
<%if bAccesoFSFA then%>

    nmenu6 = nmenu6 + "<a href='javascript:irAAltaFacturas()'><span><%=Server.HTMLEncode(denMenu(40))%></span></a>"
    nmenu6 = nmenu6 + "<a href='javascript:irASeguimientoFacturas()'><span><%=Server.HTMLEncode(denMenu(16))%></span></a>"
     <%if bAutofactura then %>
        nmenu6 = nmenu6 + "<a href='javascript:irAVisorAutofacturas()'><span><%=Server.HTMLEncode(denMenu(41))%></span></a>"
    <%end if %>
<%end if%>

// FIN NUEVO MENU


/*
''' <summary>
''' funcion que dibuja el menu del portal
''' </summary>
''' <param name="opc">Sirve para determinar el color</param>  
''' <returns>Nada</returns>
''' <remarks>Llamada desde: usuarioprincipal,okmodifregcomp,okmodificardatos,okguardarusuario,modifregcompradoras,modifcompaniaclient,modifactividadesclient; Tiempo m�ximo:0</remarks>
*/ 
function dibujaMenu(opc)
{
//NUEVO MENU
var menu
menu = ""
var selmenu = 0
var nmenus = 0

if (opc==1){sel="id='current' class='selected default'"}else{sel=""}

menu = menu + "<div id='tablemenu' style='display:none'>"
menu = menu + "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr><td>"
menu = menu + "<div id='colortab' class='ddcolortabs'><ul>"

menu = menu + "<li ><a target='default_main' href='<%=Application("RUTASEGURA")%>script/login/<%=Application("NOMPORTAL")%>/inicio.asp?idioma=<%=Idioma%>'>"
menu = menu + "<span><%=Server.HTMLEncode(denMenu(1))%></span></a>"
menu = menu + "</li>"


<%if (iConCertificados  or iConNoConformidades or iHayVarCalPub=1 or bSolQA or bEncuestas or bSolQA_Alta or bEncuestas_Alta) and iBajaLog=0 then%>
	nmenus++		
	if (opc==2){
		selmenu=nmenus 
		sel="id='current' class='selected default'"
	}
	else{
	sel=""
	}
	menu = menu + "<li ><a "+sel+" href='#' onclick='return false;' rel='nmenu5'>"
	menu = menu + "<span><%=Server.HTMLEncode(denMenu(24))%></span></a>"
	menu = menu + "</li>"

<%end if%>

<%if iConSolicitudes and bAccesoFSGS then%>
	nmenus++
	if (opc==3){
		selmenu=nmenus 
		sel="id='current' class='selected default'"
	}
	else{
		sel=""
	}
	menu = menu + "<li ><a "+sel+"  href='#' onclick='return false;' rel='nmenu4'>"
	menu = menu + "<span><%=Server.HTMLEncode(denMenu(23))%></span></a>"
	menu = menu + "</li>"

<%elseif iConSolicitudes=0 and bAccesoFSGS then%>
	nmenus++
	if (opc==3){
		selmenu=nmenus 
		sel="id='current' class='selected default'"
	}
	else{
		sel=""
	}
	menu = menu + "<li ><a "+sel+"  href='#' onclick='return false;' rel='nmenu4'>"
	menu = menu + "<span><%=Server.HTMLEncode(denMenu(2))%></span></a>"
	menu = menu + "</li>"

<%elseif iConSolicitudes and not bAccesoFSGS then%>
	nmenus++
	if (opc==3){
		selmenu=nmenus 
		sel="id='current' class='selected default'"
	}
	else{
		sel=""
	}
	menu = menu + "<li ><a "+sel+" href='javascript:irASolicitudes();'>"
	menu = menu + "<span><%=Server.HTMLEncode(denMenu(23))%></span></a>"
	menu = menu + "</li>"

<%end if%>

<%if  iConPedidos = 1 or UCase(Application("NOMPORTAL"))=UCase("ono") then %>	
	nmenus++
	if (opc==4){
		selmenu=nmenus 
		sel="id='current' class='selected default'"
	}
	else{
		sel=""
	}
	menu = menu + "<li ><a "+sel+" href='#' onclick='return false;' rel='nmenu3'>"
	menu = menu + "<span><%=Server.HTMLEncode(denMenu(14))%></span></a>"
	menu = menu + "</li>"

<%end if%>

<%if bAccesoFSFA then %>
    nmenus++
	if (opc==9){
		selmenu=nmenus 
		sel="id='current' class='selected default'"
	}
	else{
		sel=""
	}
menu = menu + "<li ><a "+sel+" href='#' onclick='return false;' rel='nmenu6'>"
menu = menu + "<span><%=Server.HTMLEncode(denMenu(32))%></span></a>"
menu = menu + "</li>"
<%end if%>

<%if iFSGAUsu<>0 then %>	
	nmenus++
	if (opc==7){
		selmenu=nmenus 
		sel="id='current' class='selected default'"
	}
	else{
		sel=""
	}
	menu = menu + "<li ><a "+sel+" target='default_main' href='<%=Application("RUTASEGURA")%>script/FSGA/GestorPartes.asp?idioma=<%=Idioma%>'>"
	menu = menu + "<span><%=Server.HTMLEncode(denMenu(29))%></span></a>"
	menu = menu + "</li>"

<%end if%>

nmenus++
if (opc==5){
	selmenu=nmenus 
	sel="id='current' class='selected default'"
}
else{
	sel=""
}
menu = menu + "<li ><a "+sel+" href='#' onclick='return false;' rel='nmenu1'>"
menu = menu + "<span><%=Server.HTMLEncode(denMenu(4))%></span></a>"
menu = menu + "</li>"

<%if iUsuPpal then%>
	<%if iDatosCia = "1" or iActivCia = "1" or iAdjunCia = "1" or iAdminUsu = "1" then%>
		nmenus++
		if (opc==6){
			selmenu=nmenus 
			sel="id='current' class='selected default'"
		}
		else{
			sel=""
		}
		menu = menu + "<li ><a "+sel+" href='#' onclick='return false;' rel='nmenu2'>"
		menu = menu + "<span><%=Server.HTMLEncode(denMenu(5))%></span></a>"
		menu = menu + "</li>"

	<%end if%>
<%end if%>

<%if lcase(application("CN")) = "1" then%>
	nmenus++
	if (opc==8){
		selmenu=nmenus 
		sel="id='current' class='selected default colaboracion'"
	}
	else{
		sel="class='colaboracion'"
	}
	menu = menu + "<li ><a "+sel+" target='default_main' href='<%=Application("RUTASEGURA")%>script/PMPortal/script/cn/cn_MuroUsuario.aspx'>"
	menu = menu + "<span><%=Server.HTMLEncode(denMenu(31))%></span></a>"
	menu = menu + "</li>"
<%end if%>

var indMenusExt=0
var relacionMenuExt=''
<%
'Creamos los menus externos
Dim DenomMenu

Do While NOT oRsMenusPadreExternos.EOF%>
    nmenus++
    
    if(arrPadreSubMenus.indexOf(<%=oRsMenusPadreExternos("ID")%>)!=-1)//Se mira si el menu padre tiene submenus para indicarle la relacion
    {
        indMenusExt=arrPadreSubMenus.indexOf(<%=oRsMenusPadreExternos("ID")%>);
        relacionMenuExt="rel='menuext" + indMenusExt + "'";
    }
    else
        {relacionMenuExt="";}

    tipoLlamada='<%=oRsMenusPadreExternos("TIPO_LLAMADA")%>'
    <%    
    if oRsMenusPadreExternos("TIPO_LLAMADA")=3 then         
        set oRsLoginJDE = oRaiz.DevolverUsuarioJDE(Request.Cookies("USU_SESIONID"))
        if not oRsLoginJDE.eof then
        %>
            usuarioJDE ='<%=oRsLoginJDE("COD")%>';
            passwordJDE='<%=oRsLoginJDE("PWD")%>';
        <%else%>
            usuarioJDE ='';
            passwordJDE='';
        <%end if 
    else%>
        usuarioJDE ='';
        passwordJDE='';
    <%end if%>
    
    url='<%=oRsMenusPadreExternos("URL")%>';
    paginaCustom=''
    if(tipoLlamada!=''){
        switch(tipoLlamada) //El menu padre tiene llamada a Url externa o a Visor Personalizado
        {
        case '1': //Llamada a visor personalizado desarrollado por FULLSTEP
          paginaCustom ='<%=oRsMenusPadreExternos("PAGINA_CUSTOM")%>' //Nombre de la pagina del visor personalizado
          llamada="href='javascript:irAVisorPersonalizado(\""+paginaCustom+"\")'" 
          //Si en el parametro opc se pasa la pagina del visor personalizado se marcara el menu padre como seleccionado, ya sea el menu padre el que llame
          //al visor personalizado o uno de sus submenus
          if(paginaCustom==opc){
              sel="id='current' class='selected default'" 
              selmenu=nmenus
          }else{
            if(arrPagCustomSubMenus[indMenusExt]){
                if(arrPagCustomSubMenus[indMenusExt].indexOf(opc)!=-1)
                {
                    sel="id='current' class='selected default'" 
                    selmenu=nmenus    
                }
            }
          }
          menu = menu + "<li ><a "+sel+" "+llamada+" "+relacionMenuExt+" >"   
          break;
        case '2': //Llamada a URL con NIF como parametro.          
        case '9': //Llamada a URL de la aplicaci�n.          
          url+= NifProve
          llamada="href='javascript:irAUrlExterna(\""+url+"\","+tipoLlamada+")'"
          //Si en el parametro opc se pasa la pagina del visor personalizado se marcara el menu padre como seleccionado, ya sea el menu padre el que llame
          //al visor personalizado o uno de sus submenus
                    if(paginaCustom==opc){
              sel="id='current' class='selected default'" 
              selmenu=nmenus
          }else{
            if(arrPagCustomSubMenus[indMenusExt]){
                if(arrPagCustomSubMenus[indMenusExt].indexOf(opc)!=-1)
                {
                    sel="id='current' class='selected default'" 
                    selmenu=nmenus    
                }
            }
          }
          menu = menu + "<li ><a " +(tipoLlamada==2?'target="default_main"':'')+" " +sel+" "+llamada+" "+relacionMenuExt+" >"     
          break;
        case '3': //Llamada a URL.
            paginaCustom ='<%=oRsMenusPadreExternos("PAGINA_CUSTOM")%>';            
            top.url=url;
            top.paginaCustom=paginaCustom;
            if (usuarioJDE=='' || passwordJDE==''){
                llamada="href='javascript:alert(\"<%=JSText(denMenu(35))%>\");'";
            }else{
                llamada="href='javascript:irAUrlExterna(\""+ url + paginaCustom +"\",3)'";
                top.frames['fraJDELogin'].location.href = url;
                top.login=true;
                top.loaded=false;
            } 
            //Si en el parametro opc se pasa la pagina del visor personalizado se marcara el menu padre como seleccionado, ya sea el menu padre el que llame
            //al visor personalizado o uno de sus submenus
            if(paginaCustom==opc){
                sel="id='current' class='selected default'" 
                selmenu=nmenus
            }else{
                if(arrPagCustomSubMenus[indMenusExt]){
                    if(arrPagCustomSubMenus[indMenusExt].indexOf(opc)!=-1)
                    {
                        sel="id='current' class='selected default'" 
                        selmenu=nmenus    
                    }
                }
            }
            menu = menu + "<li ><a target='default_main'  "+sel+" "+llamada+" "+relacionMenuExt+" >" 
            break;
        case '4': //Llamada a URL con NIF como parametro.
              url+= NifUsuario
              llamada="href='javascript:irAUrlExterna(\""+url+"\",4)'"
              //Si en el parametro opc se pasa la pagina del visor personalizado se marcara el menu padre como seleccionado, ya sea el menu padre el que llame
              //al visor personalizado o uno de sus submenus
              if(paginaCustom==opc){
                  sel="id='current' class='selected default'" 
                  selmenu=nmenus
              }else{
                if(arrPagCustomSubMenus[indMenusExt]){
                    if(arrPagCustomSubMenus[indMenusExt].indexOf(opc)!=-1)
                    {
                        sel="id='current' class='selected default'" 
                        selmenu=nmenus    
                    }
                }
              }
              menu = menu + "<li ><a target='default_main'  "+sel+" "+llamada+" "+relacionMenuExt+" >"     
              break;
        case '5': //Llamada a URL pasando usuario y contrasena, con parametro NIF del proveedor con un formato determinado.

              <% if (not isNull(oRsMenusPadreExternos("PLANTILLA_NIF")) and not isNull(oRsMenusPadreExternos("USUARIO")) and not isNull(oRsMenusPadreExternos("PWD"))) then %>      
                    post='<%=Replace(oRsMenusPadreExternos("PLANTILLA_NIF"),"FSNIF",oCompania.Nif)+ oRsMenusPadreExternos("USUARIO") + oRsMenusPadreExternos("PWD")%>';
                    url+=post
              <% end if %>
    
                llamada="href='javascript:irAUrlExterna(\""+url+"\",5)'" 
    
          //Si en el parametro opc se pasa la pagina del visor personalizado se marcara el menu padre como seleccionado, ya sea el menu padre el que llame
          //al visor personalizado o uno de sus submenus
          if(paginaCustom==opc){
              sel="id='current' class='selected default'" 
              selmenu=nmenus
          }else{
            if(arrPagCustomSubMenus[indMenusExt]){
                if(arrPagCustomSubMenus[indMenusExt].indexOf(opc)!=-1)
                {
                    sel="id='current' class='selected default'" 
                    selmenu=nmenus    
                }
            }
          }
          menu = menu + "<li ><a target='default_main'  "+sel+" "+llamada+" "+relacionMenuExt+">"     
          break;
       }
    }else{
         //El menu padre no tiene llamada a Url externa o a Visor Personalizado, lo tendran sus submenus
        if(paginaCustom==opc || arrPagCustomSubMenus[indMenusExt].indexOf(opc)!=-1){
              sel="id='current' class='selected default'"
              selmenu=nmenus
        }   
        menu = menu + "<li ><a "+sel+"  href='#' onclick='return false;' "+relacionMenuExt+" >"
    }
    <%DenomMenu=oRsMenusPadreExternos("DEN") %>
	menu = menu + "<span><%=Server.HTMLEncode(DenomMenu)%></span></a>"
	menu = menu + "</li>"
    indMenusExt+=1
<% oRsMenusPadreExternos.MoveNext
Loop
oRsMenusPadreExternos.Close
%>

menu = menu + "</ul></div>"


menu = menu + "</td></tr><tr height='3'><td height='3' style='background:<%=cMenuBgAct%>;'></td></tr></table>"


	menu = menu + "<div id='nmenu1' class='dropmenudiv_a'>" + nmenu1 + "</div>"
	menu = menu + "<div id='nmenu2' class='dropmenudiv_a'>" + nmenu2 + "</div>"
    menu = menu + "<div id='nmenu6' class='dropmenudiv_a'>" + nmenu6 + "</div>"
	menu = menu + "<div id='nmenu3' class='dropmenudiv_a'>" + nmenu3 + "</div>"
	menu = menu + "<div id='nmenu4' class='dropmenudiv_a'>" + nmenu4 + "</div>"
	menu = menu + "<div id='nmenu5' class='dropmenudiv_a'>" + nmenu5 + "</div>"

    
    var idMenuExt
    for(i=0; i<= arrSubMenus.length -1; i++){ //Meto el contenido de los subMenus de urls Externas en los divs
        idMenuExt="id='menuext" + i + "'"
        menu = menu + "<div "+idMenuExt+" class='dropmenudiv_a'>" + arrSubMenus[i] + "</div>"
    }


menu = menu + "</div>"

var styles = "<%=application("RUTASEGURA")%>script/common/ddcolortabs_css.asp";

var newSS=document.createElement('link');
newSS.rel='stylesheet';
newSS.type='text/css';
newSS.href=styles;

document.getElementsByTagName("head")[0].appendChild(newSS);
document.write("<div id='popupFondo' class='FondoModal' style='display:none; position:absolute; z-index:1001; margin:auto; width:100%; top:0; left:0;'></div>")
document.write(menu)

tabdropdown.init("colortab", selmenu);

//FIN NUEVO MENU



<%on error resume next%>

if (window.parent.frames)
    {
        if (window.parent.frames.length > 0) {
            if (window.parent.frames[0].name=="fraTop")
            {
	            f = window.parent.frames["fraTop"]
	            if (!f.document.getElementById("divLogout"))
	            {
		            if(document.createStyleSheet) {
			            f.document.createStyleSheet("<%=application("RUTASEGURA")%>script/common/estilo.asp");
		            }else{
			            var styles = "<%=application("RUTASEGURA")%>script/common/estilo.asp";
			            var newSS=f.document.createElement('link');
			            newSS.rel='stylesheet';
			            newSS.type='text/css';
			            newSS.href=escape(styles);
			            f.document.getElementsByTagName("head")[0].appendChild(newSS);
		            }	
		            str="<div style='position:absolute; left:<%=leftNotifCN%>;top:<%=topNotifCN%>'>"
		            str+="    <div id='lnkNotificacionesCN' style='display:none; cursor:pointer; position:relative; float:left; 				height:30px; 				padding-left:4px; 		margin-right:2px;'>"
		            str+="        <img  id='imgNotificacionesCN' 								src='<%=application("RUTASEGURA")%>custom/<%=Application("NOMPORTAL")%>/images/alertNotif.png' 							style='vertical-align:middle; margin-right:8px; margin-top:10px;' />"
		            str+="        <div id='AlertNotificacionCN' class='fondoAlerta' style='position:absolute; text-align:center; 					vertical-align:middle; 		width:15px; height:15px; top:0px; right:0px;'>"
		            str+="            <span id='NumeroAlertNotificacionCN' class='TextoClaro Negrita' 				style='line-height:15px;'>0</span>"
		            str+="        </div>"
		            str+="    </div>"
		            str+="</div>"
		            str+="<div name=divLogout id=divLogout style='position:absolute;left:<%=leftLogout%>;top:<%=topLogout%>px'>"    
		            str+="<form method=post action='<%=application("RUTASEGURA")%>script/disconnect/disconnect.asp?disconnect1=true' 				id=form1 				name=form1 		target='_top'>"
		            str+="<table width=100% HEIGHT=100% cellspacing=5px>"
		            str+="	<tr>"
		            str+="		<td align = center >"
		            str+="			<input type=submit id='btnLogOut' class='botonLogOut' value='<%=Server.HTMLEncode(denMenu(18))%>'>"
		            str+="		</td>"
		            str+="	</TR>"
		            str+="</TABLE>"
		            str+="<input type = hidden name='btnDesconectar' value='Desconectar'> "
		            str+="</form>"
		            str+="</div>"
		            f.document.body.innerHTML= f.document.body.innerHTML + str
	            }
            f.document.getElementById("btnLogOut").value = '<%=Server.HTMLEncode(denMenu(18))%>'
            }
        }
        
    }

    if (document.images["imgCiaComp"])
	    if (document.images["imgCiaComp"].complete==false)
		    document.images["imgCiaComp"].style.visibility="hidden"	
}






