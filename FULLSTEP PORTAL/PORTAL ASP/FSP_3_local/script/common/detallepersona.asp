﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<% 
''' <summary>
''' Mostrar el detalle de una persona
''' </summary>
''' <remarks>Llamada desde: noconformidades\noconformidades11.asp   solicitudes\solicitudes11.asp   solicitudesoferta\solicitudesoferta11.asp; Tiempo máximo: 0,2</remarks>

	Idioma = request.cookies("USU_IDIOMA")
	set oRaiz=validarUsuario(Idioma,true,false,0)

	dim Den
	Den=devolverTextos(Idioma,116)
	
	per = Request("Per")


	CiaComp = oRaiz.Sesion.CiaComp

    
	set oPersona = oRaiz.Generar_CPersona
	oPersona.Cod = Per
	oPersona.CargarPersona CiaComp

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><%=title%></title>
<script src="formatos.js"></script>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">

<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
</head>
<body>

    
<table WIDTH=95% ALIGN=CENTER border="0">
	<tr>
		<td class="cabecera negrita">
			<%=Den(2)%>
		</td>
		<td class="filaPar">
			<%=HTMLEncode(oPersona.Nombre) & " " & HTMLEncode(oPersona.Apellidos)%>
		</td>
	</tr>
	<tr>
		<td class="cabecera negrita">
			<%=Den(3)%>
		</td>
		<td class="filaPar">
			<%=HTMLEncode(oPersona.Tfno)%>
		</td>
	</tr>
	<tr>
		<td class="cabecera negrita">
			<%=Den(4)%>
		</td>
		<td class="filaPar">
			<%=HTMLEncode(oPersona.Fax)%>
		</td>
	</tr>
	<tr>
		<td class="cabecera negrita">
			<%=Den(5)%>
		</td>
		<td class="filaPar">
			<a href="mailto:<%=Email%>"><%=HTMLEncode(oPersona.Email)%></a>
		</td>
	</tr>
	<tr>
		<td colspan=2 align=center>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan=2 align=center>
			<input type=button class=button name=cmdCerrar onclick="window.close()" value="<%=den(6)%>">
		</td>
	</tr>
</table> 


</form>

<%
set oPersona = nothing

set oRaiz = nothing
%>

</body>
</html>
