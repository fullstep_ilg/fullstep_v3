﻿<script language="javascript" runat="SERVER">
function devolverLocFecha(Urte,Mes,Dia,Hora,Minuto)
{
var vDate
var sDate
vDate=new Date(Urte,Mes-1,Dia,Hora,Minuto)
sDia=vDate.getDate().toString()
sDia ="00".substr(0,2 - sDia.length) + sDia

sMes = (vDate.getMonth() + 1).toString()
sMes ="00".substr(0,2 - sMes.length) + sMes

sHora = (vDate.getHours()).toString()
sHora = "00".substr(0,2 - sHora.length) + sHora

sMinu = (vDate.getMinutes()).toString()
sMinu = "00".substr(0,2 - sMinu.length) + sMinu

sDate=sDia + "/" + sMes + "/" + vDate.getFullYear() + " " + sHora + ":" + sMinu
return sDate
}
function devolverUTCFecha(Urte,Mes,Dia,Hora,Minuto)
{
var vDate
var sDate
vDate=new Date(Urte,Mes-1,Dia,Hora,Minuto)

sDia=vDate.getUTCDate().toString()
sDia ="00".substr(0,2 - sDia.length) + sDia

sMes = (vDate.getUTCMonth() + 1).toString()
sMes ="00".substr(0,2 - sMes.length) + sMes

sHora = (vDate.getUTCHours()).toString()
sHora = "00".substr(0,2 - sHora.length) + sHora

sMinu = (vDate.getUTCMinutes()).toString()
sMinu = "00".substr(0,2 - sMinu.length) + sMinu


sDate=sDia + "/" + sMes + "/" + vDate.getUTCFullYear() + " " + sHora + ":" + sMinu
return sDate
}


</script>



<%
if request.Cookies ("USU_PRECISIONFMT") ="" then 
    dim IsSecureFormato
    IsSecureFormato = left(Application ("RUTASEGURA"),8)
    if IsSecureFormato = "https://" then                       
        IsSecureFormato = "; secure"
    else
        IsSecureFormato =""
    end if

    Response.AddHeader "Set-Cookie","USU_PRECISIONFMT=2; path=/; HttpOnly" + IsSecureFormato    

    precisionfmt=2
else
    precisionfmt=cint(Request.Cookies ("USU_PRECISIONFMT"))
end if

decimalfmt=request.Cookies ("USU_DECIMALFMT")
thousandfmt=Request.Cookies ("USU_THOUSANFMT")
datefmt=Request.Cookies("USU_DATEFMT") 

''' <summary>
''' Esta funcion devuelve una cadena con un numero formateado dependiendo de los parametros
''' </summary>
''' <param name="Num">El número a formatear</param>
''' <param name="DecFmt">carácter que se utilizará como separador decimal</param>        
''' <param name="ThouFmt">carácter que se utilizará como separador de miles</param>  
''' <param name="PrecFmt">número de decimales que se mostrarán</param>  
''' <returns>Numero formateado</returns>
''' <remarks>Llamada desde: Toda pantalla q muestre numeros. Si haces un find salen 98 usos; Tiempo máximo: 0,1</remarks>
Function VisualizacionNumero(byval Num,DecFmt,ThouFmt,PrecFmt)
Dim numeroLocal
Dim snumeroLocal
Dim decimalloc
Dim i
Dim ParteDecimal
Dim tmpParteDecimal
Dim quitarCeros 
Dim newValor
dim mNum
Dim bnegativo
Dim ParteEmenos
Dim j
dim sNumAux1
dim sNumAux2
dim iNumAux1
dim iNumAux2
dim Fin 
dim PosValido 

	numeroLocal = 1/10

	snumeroLocal = cstr(numeroLocal)
	if instr(snumeroLocal,".") then
		decimalloc = "."
	else
		decimalloc = ","
	end if
	
	if isnull(num) or num="" then
		VisualizacionNumero=""
		exit function
	end if
	
	if Num < 0 then      'Trabajamos con la cantidad en valor absoluto
		bnegativo = true 'pero marcamos q es negativo
		Num = num * -1
	end if
	
	''''''
	sNum = ucase(cstr(num))

	if instr(sNum,"E") then 	
		'1.0250001E-7
		ParteEmenos	= mid(sNum,instr(sNum,"E") + 2)		
		'7		
		ParteDecimal = mid(sNum,1,instr(sNum,"E")-1)
		'1.0250001		
		ret = "0" & DecFmt
		'0,
		for i = 1 to cint(ParteEmenos)-1 
			ret = ret & "0"
		next
		'0,000000
		
		j=len(ParteDecimal)
		for i = 1 to j
			'ParteDecimal 1.0250001
			if instr(mid(ParteDecimal,i,1),".") or instr(mid(ParteDecimal,i,1),",") then
				'Tratar .
				ret=ret
				
				if mid(ParteDecimal,i+1,3) ="000" then
					'caso 1.0001E-5 redondeas a 0,00001 (1E-5)
					i=j+1
				end if
				
				if mid(ParteDecimal,i+1,3) ="999" then
					'caso 1.999E-5 redondeas a 0,00002 (2E-5)
					'ret 0,00001
					ret= mid(ret,len(ret)-1)
					'ret 0,0000
					if mid(ParteDecimal,i-1,1) ="9" then 
						'caso 9.999E-5 redondeas a 0,0001 (1E-4)
						ret= mid(ret,len(ret)-1)
						'ret 0,000
						ret= ret & "1"
						'ret 0,0001
					else
						'caso 1.999E-5 redondeas a 0,00002 (2E-5)
						ret= ret & cstr(cint(mid(ParteDecimal,i-1,1))+1)
						'ret 0,00002
					end if
					
					i=j+1
				end if
			else
				if mid(ParteDecimal,i+1,3) ="000" then
					'caso 1.10001E-5 redondeas a 0,000011 (1.1E-5)
					'ret 0,00001
					ret= ret & mid(ParteDecimal,i,1)
					'ret 0,000011
					i=j+1
				else
					if mid(ParteDecimal,i+1,3) ="999" then
						'caso 1.1999E-5 redondeas a 0,000012 (1.2E-5)
						'ret 0,000011
						ret= mid(ret,len(ret)-1)
						'ret 0,00001
						if mid(ParteDecimal,i,1) ="9" then 
							'caso 9.9999E-5 redondeas a 0,0001 (1E-4)
							'ret 0,00009
							ret= mid(ret,len(ret)-1)
							'ret 0,0000
							ret= ret & "1"
							'ret 0,0001
						else
							'caso 1.1999E-5 redondeas a 0,000012 (1.2E-5)
							'ret 0,00001
							ret= ret & cstr(cint(mid(ParteDecimal,i,1))+1)
							'ret 0,000012
						end if
						
						i=j+1						
					else
						ret= ret & mid(ParteDecimal,i,1)
					end if
				end if							
			end if
		next
		
		'Por otra parte mirar cifras significativas. 0.0000003456 a 8 deci es 0.00000035
		if (len(ret)-2 > PrecFmt) then
			PosValido =PrecFmt 
			Fin= false
			
			while (not Fin)
				sNumAux1=mid(ret,3,PosValido)
				sNumAux2="0," & mid(ret,PosValido+3)
				iNumAux1=cdbl(sNumAux1)
				iNumAux2=round(cdbl(sNumAux2))

				if (iNumAux1 <>0) then
					Fin=true
					if (iNumAux2=0) then
						ret= mid(ret,1,PosValido+2)
					else
						'Ya se q +1
						if (mid(ret,PosValido+3,1)="9") then
							ret = mid(ret,1,PosValido+1) & cstr(cint(mid(ret,PosValido+2,1)) + 1)
						else
							if (mid(ret,PosValido+2,1)="9") then
								ret = mid(ret,1,PosValido) & "1"
							else
								ret = mid(ret,1,PosValido+1) & cstr(cint(mid(ret,PosValido+2,1)) + 1)
							end if
						end if		
					end if
				else
					PosValido = PosValido +1 
				end if								
			wend
			
			if (PosValido > PrecFmt) then
				sNumAux1="0," & mid(ret,PosValido+2)
				iNumAux1=Math.round(cdbl(sNumAux1))	
						
				if (iNumAux1<>0) then
					ret = mid(ret,1,len(ret)-2) & "1"
				end if
			end if
		end if
		
		VisualizacionNumero=ret
		exit function
	end if
	''''

	
	if (PrecFmt>0) then mNum= round(num,PrecFmt) else mNum= num
	

	ParteEntera = cstr(Fix(mNum))
	sNum = cstr(mNum)
	tmpParteDecimal=""

	if instr(sNum,decimalloc)>0 then
		if (PrecFmt>0) then
			ParteDecimal = mid(sNum,instr(sNum,decimalloc) + 1,PrecFmt)
		else
			ParteDecimal = mid(sNum,instr(sNum,decimalloc) + 1)
		end if
	else
		ParteDecimal = ""
	end if

	quitarCeros = true

	for i = len(ParteDecimal) to 1 step -1
		if mid(ParteDecimal,i,1)="0" and quitarCeros then
		else
			tmpParteDecimal = mid(ParteDecimal,i,1) & tmpParteDecimal
			quitarCeros = false
		end if
	next

	ParteDecimal =tmpParteDecimal	
	newValor=""
	s=0
	for i = len(ParteEntera) to 1 step -1
		if s=3 then
			newValor = ThouFmt & newValor
			s=0
		end if
		newValor = mid(ParteEntera,i,1) & newValor
		s=s+1
	next
	
	if bnegativo then newValor = "-" & newValor end if 'Devolvemos el signo al valor
	if parteDecimal<>"" then
		ret=newValor & DecFmt & parteDecimal
	else
		ret=newValor 
	end if
	vPrec = PrecFmt
	while ret="0" and Num<>0
		vPrec=vPrec + 1
		ret = VisualizacionNumero(Num,DecFmt,ThouFmt,vPrec)
	
	wend
	
	VisualizacionNumero=ret

end function


	
'ESTA FUNCION DEVUELVE UNA CADENA CON UNA FECHA FORMATEADA DEPENDIENDO 
'DE LOS PARAMETROS SIGUIENTE
'
'Fecha: la fecha a formatear
'datfmt: formato a aplicar a dicha fecha

Function VisualizacionFecha (Fecha,datFmt)

sepFecha=mid(datFmt,3,1)
if isnull(Fecha) then
	VisualizacionFecha=""
	exit function
end if
if Fecha = "" then
	VisualizacionFecha=""
	exit function
end if
sDia=cstr(day(Fecha))
sMes=cstr(month(Fecha))
sUrte = cstr(year(Fecha))


sDia = left("00",2-len(sDia)) & sDia
sMes = left("00",2-len(sMes)) & sMes

returnFecha= replace(datFmt,"dd",sDia)
returnFecha = replace(returnFecha,"mm",sMes)
returnFecha = replace(returnFecha,"yyyy",sUrte)
VisualizacionFecha = returnFecha

end function	



Function VisualizacionUTCFecha (Fecha,datefmt)
if isnull(Fecha) then
	VisualizacionUTCFecha=""
	exit function
end if

sepFecha=mid(datefmt,3,1)

sFecha = devolverUTCFecha(Year(Fecha),Month(Fecha),Day(Fecha),Hour(Fecha),Minute(Fecha))


returnFecha = replace(sFecha,"/",sepFecha)

sDia=left(returnFecha,2)
sMes=mid(returnFecha,4,2)

if left(datefmt,1)="d" then
	returnFecha=sDia & sepFecha & sMes & mid(returnFecha,6,len(returnFecha))
else
	returnFecha=sMes & sepFecha & sDia & mid(returnFecha,6,len(returnFecha))
end if

VisualizacionUTCFecha = returnFecha

end function	

Function VisualizacionFechaLarga (Fecha,datefmt)
if isnull(Fecha) then
	VisualizacionFechaLarga=""
	exit function
end if

sepFecha=mid(datefmt,3,1)

sFecha = devolverLocFecha(Year(Fecha),Month(Fecha),Day(Fecha),Hour(Fecha),Minute(Fecha))


returnFecha = replace(sFecha,"/",sepFecha)

sDia=left(returnFecha,2)
sMes=mid(returnFecha,4,2)

if left(datefmt,1)="d" then
	returnFecha=sDia & sepFecha & sMes & mid(returnFecha,6,len(returnFecha))
else
	returnFecha=sMes & sepFecha & sDia & mid(returnFecha,6,len(returnFecha))
end if

VisualizacionFechaLarga = returnFecha

end function	



'ESTA FUNCION CONVIERTE UNA FECHA DEL FORMATO DEL USUARIO AL FORMATO dd/mm/yyyy
'
'mFecha: La fecha a convertir
'datefmt: El formato de la fecha en el que se suministra

Function Fecha(mFecha,datefmt)
dim datesep
dim returnFecha
dim mdia
dim mmes
dim manyo

if isnull(mFecha) or mFecha="" then
	Fecha=null
	exit function
end if
returnFecha=mFecha
datesep = mid(datefmt,3,1)
		
		
if left(datefmt,1)="d" then 
	mFecha = split(returnFecha,datesep)
				
	mdia=clng(mFecha(0))
	mmes=clng(mFecha(1))
	manyo=clng(mFecha(2))

else
	mFecha = split(returnFecha,datesep)
				
	mdia=clng(mFecha(1))
	mmes=clng(mFecha(0))
	manyo=clng(mFecha(2))
end if


on error resume next
returnFecha = DateSerial(manyo,mmes,mdia)

Fecha=returnFecha


end function

''' <summary>
''' Esta funcion devuelve una fecha con horas minutos y segundos
''' </summary>
''' <param name="mFecha">String con la fecha</param>
''' <param name="datefmt">Formato en que viene la fecha</param>        
''' <returns>Fecha</returns>
''' <remarks>Llamada desde: guardaroferta.asp ; Tiempo mÃ¡ximo: 0,1</remarks>
''' REvisada por EPB 21/02/2013
Function FechaHora(mFecha,datefmt)
dim datesep
dim returnFecha
dim fec
dim hor
dim mdia
dim mmes
dim manyo
dim hour
dim minute
dim second

if isnull(mFecha) or mFecha="" then
	Fecha=null
	exit function
end if
returnFecha=mFecha
datesep = mid(datefmt,3,1)
		
		
if left(datefmt,1)="d" then 
	mFecha = split(returnFecha," ")
	fec=mFecha(0)
	hor=mFecha(1)

	mFecha = split(fec,datesep)			
	mdia=clng(mFecha(0))
	mmes=clng(mFecha(1))
	manyo=clng(mFecha(2))

	mFecha = split(hor,":")			
	hour=clng(mFecha(0))
	minute=clng(mFecha(1))
	second=clng(mFecha(2))

else
	mFecha = split(returnFecha," ")
	fec=mFecha(0)
	hor=mFecha(1)
	
	mFecha = split(fec,datesep)			
	mdia=clng(mFecha(1))
	mmes=clng(mFecha(0))
	manyo=clng(mFecha(2))
	
	mFecha = split(hor,":")			
	hour=clng(mFecha(0))
	minute=clng(mFecha(1))
	second=clng(mFecha(2))
end if


on error resume next

returnFecha = DateSerial(manyo,mmes,mdia) + TimeSerial(hour, minute, second)

FechaHora=returnFecha


end function

'ESTA FUNCION DEVUELVE CONVIERTE UNA CADENA EN UN NÚMERO
'
'Num: El número a  convertir

Function Numero( byval Num)

	Dim NumeroFinal
	Dim indice
	Dim dividir 
	Dim decimales
	Dim Numerico
	Dim Comas 
	Dim Puntos
	
	
	dividir = 1
	NumeroFinal=""

	Num=replace(Num,thousandfmt,"")
	Num=replace(Num,decimalfmt,".")


	if Num="" then
		Numero = null
		exit function
	end if

	Comas = ubound (split (Num,","))
	
	Puntos = ubound(split(Num,"."))
	
		
	For indice=1 to len(Num)
		
		Select Case Mid(Num,indice,1)
		
		Case ","
		
			NumeroFinal=NumeroFinal 
			if Comas = 1 then
				if Puntos = 1 then
					if instr(1,Num,",") > instr(1,Num,".") then
						Decimales = true
					end if
				else
					Decimales = true
				end if
			end if
		
		case "."
		
			NumeroFinal= NumeroFinal
			if Puntos = 1 then
				if Comas = 1 then
					if instr(1,Num,".") > instr(1,Num,",") then
						Decimales = true
					end if
				else
					Decimales = true
				end if
			end if
			
		Case Else
			NumeroFinal=NumeroFinal & Mid(Num,indice,1)
			if decimales = true then
				dividir = dividir* 10
			end if
			
		End Select
			
	Next 
	numerofinal=cdbl(numerofinal)
	if decimales = true then
		Numerico = numerofinal / dividir
	else
		Numerico = numerofinal
	end if
	
	Numero = Numerico
				
End Function



'Convierte una cadena vacia a nulo o devuelve la misma cadena
function vacio2null(texto)
if texto="" then
	vacio2null = null
else
	vacio2null = texto
end if

end function


function null2zero(valor)
if isnull(valor) then
	null2zero=0
else
	null2zero=valor
end if
end function


function JSText(text)
'--<summary>
'--Formatea un texto para ser utilizado como string en Javascript
'--</summary>
'--<param name=text>Texto de entrada</param>
'--<remarks>Llamada desde: Páginas ASP Portal</remarks>
'--<revision>DPD 28/06/2011</revision>
dim t
if isnull(text) then
	JSText=""
	exit function
end if
t = replace(text,"\", "\\")  ' DPD Añadido para la Incidencia 17255
t = replace(t,"'","\'")
t = replace(t,"""","\""")
t = replace(t,chr(13) & chr(10), "\n")
t = replace(t,chr(10), "\n")
t = replace(t,chr(13), "\n")
JSText = t
end function


function JSAlertText(text)
'--<summary>
'--Formatea un texto para ser utilizado como string en Javascript (textos alert)
'--</summary>
'--<param name=text>Texto de entrada</param>
'--<remarks>Llamada desde: Páginas ASP Portal</remarks>
'--<revision>DPD 28/06/2011</revision>
dim t
if isnull(text) then
	JSAlertText=""
	exit function
end if
t = replace(text,"'","\'")
t = replace(t,"""","\""")
t = replace(t,chr(13) & chr(10), "\n")
t = replace(t,chr(10), "\n")
t = replace(t,chr(13), "\n")
JSAlertText = t

end function 



function JSNum(Num)

dim t
if isnull(Num) or isempty(Num) then
	JSNum="null"
	exit function
end if
t = replace(Num,",",".")
JSNum = t


end function

function JSHora(Fecha)
dim f

if isnull(Fecha) then
	JSHora = "null"
else
	JSHora ="new Date (" & year(fecha) & "," & (month(fecha)-1) & "," & day(fecha) & "," & hour(fecha) & "," & minute(fecha) & "," & second(fecha) & ")"
	
end if
end function

function JSDate(Fecha)
dim f

if isnull(Fecha) then
	JSDate = "null"
else
	JSDate ="new Date (" & year(fecha) & "," & (month(fecha)-1) & "," & day(fecha) & ")"
end if

end function 


function JSBool(Binario)

if isnull(Binario) then
	JSBool = "null"
else
	if Binario then
		JSBool= "true"
	else
		JSBool= "false"
	end if
end if

end function






function iif (exp, tpart, fpart)

if exp then
	iif = tpart
else
	iif = fpart
end if

end function





Public Function NullToStr(ByVal ValueThatCanBeNull) 
  
    If IsNull(ValueThatCanBeNull) Then
        NullToStr = ""
    Else
        NullToStr = ValueThatCanBeNull
    End If

End Function

Public Function VB2HTML(byval text)
dim t
if isnull(text) then
	VB2HTML=""
	exit function
end if
t = replace(text,chr(13) & chr(10), "<br>")
t = replace(t,chr(10), "<br>")
t = replace(t,chr(13), "<br>")
t = replace(t,"'","&#39;")
t = replace(t,"""","&#34;")

VB2HTML=t


end function

''' <summary>
''' codificar texto ascii en formato hexadecimal
''' </summary>
''' <param name="str">Texto a codificar</param>     
''' <returns>texto en formato hexadecimal</returns>
''' <remarks>Llamada desde: seguimpedidos\buscarpedidos.asp ; Tiempo máximo: 0</remarks>
function hexEncode(str)
	dim strEncoded, i, strHex
    
	strEncoded = ""
	for i = 1 to Len(str)
	    strHex=Hex(Ascw(Mid(str, i, 1)))
	    If Len(strHex)=1 Then strHex="000" & strHex
        If Len(strHex)=2 Then strHex="00" & strHex
        If Len(strHex)=3 Then strHex="0" & strHex
		strEncoded = strEncoded + strHex
	next
	hexEncode = strEncoded
end function

''' <summary>
''' decodificar texto formato hexadecimal en ascii
''' </summary>
''' <param name="str">Texto a decodificar</param>  
''' <returns>texto en formato ascii</returns>
''' <remarks>Llamada desde: seguimpedidos\buscarpedidos.asp ; Tiempo máximo: 0</remarks>
function hexDecode(str)
	dim strDecoded, i, hexValue
    
	strDecoded = ""
	for i = 1 to Len(str) step 4
		hexValue = Mid(str, i, 4)
		strDecoded = strDecoded + chrw(CLng("&h" & hexValue))
	next
	hexDecode = strDecoded
end function

''' <summary>
''' Devuelve la fecha local en UTC
''' </summary>
''' <returns>fecha UTC como un sctring</returns>
''' <remarks>Llamada desde: GetFechaLog ; Tiempo máximo: 0</remarks>
Function ConvertToUTCTime(sTime)

          Dim od, ad, oShell, atb, offsetMin
          Dim sHour, sMinute, sMonth, sDay 

'od = sTime
'if you passed sTime as sting, comment the above line and 
'uncomment the below line.
od = CDate(sTime)

'Create Shell object to read registry
Set oShell = CreateObject("WScript.Shell")

'atb = "HKEY_LOCAL_MACHINESystemCurrentControlSet" & _
'"ControlTimeZoneInformationActiveTimeBias" -->
atb ="HKEY_LOCAL_MACHINE\System\CurrentControlSet\" &_
"Control\TimeZoneInformation\ActiveTimeBias"
offsetMin = oShell.RegRead(atb) 'Reading the registry

'Convert the local time to UTC time
ad = dateadd("n", offsetMin, od) 

' If Month is single digit value, add zero
sMonth = Month(CDate(ad))
If Len(sMonth) = 1 Then
    sMonth = "0" & sMonth
End If 

'If Day is single digit, add zero
sDay = Day(CDate(ad))
If Len(sDay) = 1 Then
          sDay = "0" & sDay
End If 

'if Hour is single digit, add zero
sHour = Hour(CDate(ad))
If Len(sHour) = 1 Then
          sHour = "0" & sHour
End If 

'if Minute is single digit, add zero
sMinute = Minute(CDate(ad))
If Len(sMinute) = 1 Then
         sMinute = "0" & sMinute
End If 

sSecond = Second(CDate(ad))
If Len(sSecond) = 1 Then
         sSecond= "0" & sSecond
End If 

'Assign the reutrn value in UTC format as 2006-11-07T18:00:000Z
ConvertToUTCTime = Year(CDate(ad)) & "-" & sMonth & "-" & sDay & " " & sHour & ":" & sMinute & ":" & sSecond

End Function


''' <summary>
''' Devuelve la fecha actual con milisegundos para el log de accesos (FSAL)
''' </summary>
''' <returns>fecha como un sctring</returns>
''' <remarks>Llamada desde: todas las paginas que registren log de acceso ; Tiempo máximo: 0</remarks>
function GetFechaLog()
dim t
dim dfecha
dim temp
dim Miliseconds
dim Seconds
dim Minutes
dim Hours
dim anyo
dim mes
dim dia
dim sfecha

t = Timer
dfecha=date
' Int() behaves exacly like Floor() function, ie. it returns the biggest integer lower than function's argument
temp = Int(t)

Miliseconds = Int((t-temp) * 1000)
Seconds = temp mod 60
temp    = Int(temp/60)
Minutes = temp mod 60
Hours   = Int(temp/60)
anyo =year(dfecha)
mes =month(dfecha)
dia =day(dfecha)

sfecha=ConvertToUTCTime(cstr(anyo & "-" & mes & "-" & dia) & " " & Hours & ":" & Minutes & ":" & Seconds)

GetFechaLog = sfecha & "." & Right("00" & Miliseconds, 3)
end function

''' <summary>Devuelve un string con el formato de visualizaciÃ³n de un coste/descuento</summary>
''' <param name="Desc">DescripciÃ³n del coste/descuento</param>
''' <param name="Tipo">0=coste, 1=descuento</param>        
''' <param name="Oper">+,-,+% o -%</param>  
''' <param name="Valor">Valor del coste/descuento</param>   
''' <param name="Mon">Moneda</param> 
''' <param name="Cambio">Cambio</param> 
''' <returns>Coste/descuento formateado</returns>
''' <remarks>Llamada desde: rptPedidoDetalle.asp</remarks>

Function VisualizacionCosteDesc(Desc,Tipo,Oper,Valor,Mon,Cambio)
    Dim sDescCD   
   
    sDescCD=Desc & " ("
    if Tipo=0 then
        'Es coste
        if Oper=0 then
            sDescCD=sDescCD & "+" & visualizacionNumero((Cdbl(Valor)*Cdbl(Cambio)),decimalfmt,thousanfmt,precisionfmt) & " " & Mon & ")" 
        else
            sDescCD=sDescCD & "+" & visualizacionNumero((Cdbl(Valor)),decimalfmt,thousanfmt,precisionfmt) & "%)"
        end if
    else
        'Es descuento
        if Oper=0 then
            sDescCD=sDescCD & "-" & visualizacionNumero((Cdbl(Valor)*Cdbl(Cambio)),decimalfmt,thousanfmt,precisionfmt) & " " & Mon & ")"
        else
            sDescCD=sDescCD & "-" & visualizacionNumero((Cdbl(Valor)),decimalfmt,thousanfmt,precisionfmt) & "%)" 
        end if
    end if    

    VisualizacionCosteDesc=sDescCD
end function

'''Formatea un entero rellenando a la izquierda con ceros 
'''<summary param="number">Número a formatear</summary>
'''<summary param="size">Tamaño en caracteres del número formateado</summary>
function formatIntString(byval number,byval size)
    formatIntString = string(size - Len(number), "0") & number
end function

''' <summary>
''' Devuelve un IdRegistro para FSAL en funcion de la fecha actual+un aleatorio de 8 digitos, en formato para el log de accesos (FSAL)
''' </summary>     
''' <returns>IdRegistro</returns>
''' <remarks>Llamada desde: fsal_1.asp; Tiempo mÃ¡ximo: </remarks>
function GetIdRegistroLog()
    Dim fechaLocal
    Dim aFechaLocal
    Dim aHoraLocal
    Dim aHHmmssLocal
    Dim horaLocal
    Dim minLocal
    Dim segLocal
    Dim miliLocal
    Dim sIdRegistro
    Dim iAleatorio

    fechaLocal=GetFechaLog()
    aFechaLocal=Split(fechaLocal, " ")
    aHoraLocal=Split(aFechaLocal(1), ".")
    aHHmmssLocal=Split(aHoraLocal(0), ":")
    horaLocal=aHHmmssLocal(0)
    minLocal=aHHmmssLocal(1)
    segLocal=aHHmmssLocal(2)
    miliLocal=aHoraLocal(1)
    sIdRegistro=FormatNumber(((DateDiff("s", "1970-01-01 00:00:00", aFechaLocal(0)+" "+aHoraLocal(0))*10000000)+(CInt(miliLocal)*10000)+621355968000000000),0,,,0)
    sIdRegistro=Left(sIdRegistro, Len(sIdRegistro)-3)+miliLocal

    'Instrucción que inicializa el generador de números aleatorios
    Randomize()
    'N = (Limsup - Liminf) * Rnd + Liminf
    iAleatorio=Math.round((99999999-1)*Rnd()+1, 0)

    sIdRegistro=sIdRegistro & Right(String(8, "0") & iAleatorio, 8)

    GetIdRegistroLog=sIdRegistro
end function
%>


