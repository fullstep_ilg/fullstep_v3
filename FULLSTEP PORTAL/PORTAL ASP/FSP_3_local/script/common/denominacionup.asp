﻿<%@ Language=VBScript %>
<!--#include file="acceso.asp"-->
<!--#include file="idioma.asp"-->
<!--#include file="formatos.asp"-->

<HTML>
<HEAD>

<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">

<%
''' <summary>
''' Mostrar la descripción de una unidad
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos11dir.asp    pedidos\pedidos21.asp   pedidos\pedidos22.asp   pedidos\pedidos41.asp   
''' seguimpedidos\seguimclt41.asp   solicitudesoferta\cumpoferta.asp; Tiempo máximo: 0,2</remarks>

	Idioma = Request("Idioma")
	Cod=Request("Cod")
	
	set oRaiz=validarUsuario(Idioma,true,false,0)
    

	CiaComp = oRaiz.Sesion.CiaComp

	Den=devolverTextos(idioma,5)
		
	set oUP = oraiz.Generar_CUnidadesPedido
	
	set ador = oUP.CargarDatosUnidad(Idioma, CiaComp, Cod)
%>

<SCRIPT>
function Cerrar()
{
	//Cierra la ventana
	window.close()
}

</SCRIPT>

</HEAD>


<BODY topmargin="15" leftmargin="0">


<TABLE class="normal" align="center" WIDTH="98%" border="0" cellspacing="1" cellpadding="1">

	<TR>
		<TD width="5%" class="cabecera"><%=den(1)%></td>
		<TD class="filaPar" ><%=ador("COD").value%></td>
	</TR>
	<TR>
		<TD width="35%" class="cabecera"><%=den(2)%></td>
		<TD class="filaPar"><%=ador("DEN").value%></TD>
	</TR>

	<TR>
		<TD colspan=2 height=40px align=center >
			<INPUT TYPE="button" class="button" ID=btnCerrar value="<%=Den(5)%>" onclick="Cerrar()">
		</TD>
	</TR>
	
</TABLE>	

</BODY>
</HTML>

