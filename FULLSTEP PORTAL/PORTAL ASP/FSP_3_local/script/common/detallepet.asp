﻿<%@ Language=VBScript %>
<!--#include file="acceso.asp"-->
<!--#include file="idioma.asp"-->
<!--#include file="formatos.asp"-->

<HTML>
<HEAD>

<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">

<%
''' <summary>
''' Mostrar el detalle de un peticionario
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos11.asp   pedidos\pedidos11dir.asp    seguimpedidos\seguimclt.asp; Tiempo máximo: 0,2</remarks>

	Idioma = Request("Idioma")
	Recep=Request("Recep")
	
	if Recep="" then Recep=0
	
	Pet=Request("Pet")
	
	set oRaiz=validarUsuario(Idioma,true,false,0)
    

	CiaComp = oRaiz.Sesion.CiaComp

	Den=devolverTextos(idioma,109)
	if Recep=0 then 
		titulo=den(1) 'Peticionario
	else
		titulo=den(7) 'Receptor
	end if
	
	Set oPersonas = oRaiz.Generar_CPersonas()

	oPersonas.CargarTodasLasPersonas CiaComp,Pet
	If Not oPersonas.Item(Pet) Is Nothing Then
		set oPersona = oPersonas.Item(Pet)
    End If
    set oPersonas=nothing
    
%>


<SCRIPT>
function Cerrar()
{
	//Cierra la ventana
	window.close()
}

</SCRIPT>

</HEAD>


<BODY topmargin="15" leftmargin="0">


<TABLE class="normal" align="center" WIDTH="98%" border="0" cellspacing="1" cellpadding="1">

	<TR>
		<TD width="5%" class="cabecera"><%=den(3)%></td>
		<TD class="filaPar" ><%=HTMLEncode(oPersona.Nombre)%>&nbsp;<%=HTMLEncode(oPersona.Apellidos)%></td>
	</TR>
	<TR>
		<TD width="35%" class="cabecera"><%=den(5)%></td>
		<TD class="filaPar"><%=HTMLEncode(oPersona.Tfno)%></TD>
	</TR>
	<TR>
		<TD width="35%" class="cabecera"><%=den(4)%></td>
		<TD class="filaPar"><%=HTMLEncode(oPersona.Fax)%></TD>
	</TR>
	<TR>
		<TD width="35%" class="cabecera"><%=den(6)%></td>
		<TD class="filaPar"><%=HTMLEncode(oPersona.EMail)%></TD>
	</TR>
	<TR>
		<TD colspan=2 height=40px align=center >
			<INPUT TYPE="button" class="button" ID=btnCerrar value="<%=Den(2)%>" onclick="Cerrar()">
		</TD>
	</TR>
	
</TABLE>	

</BODY>
</HTML>

<%
	set oPersona=nothing
	set oRaiz=nothing
%>