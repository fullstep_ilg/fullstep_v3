﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<%
''' <summary>
''' Muestra los links de descarga
''' </summary>
''' <remarks>Llamada desde: common\download.asp ; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies ("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,false,false,0)

den=devolverTextos(Idioma,113)

mpath = application("RUTASEGURA")
servidor = Request.servervariables("SERVER_NAME")
mpath = replace(mpath,"http://" & servidor,"")
mpath = replace(mpath,"https://" & servidor,"")

sPath = mPath & "download/" & request("path") & "/" & request("nombre")

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<title><%=Application("TITULOVENTANAS_" & Idioma)%></title>
<script>
    function Init() {
        window.open("<%=application("RUTASEGURA")%>/download/<%=request("path") & "/" & request("nombre")%>","_self") 
    }
</script>
</head>
<body onload="Init()">
<h1 style="margin-left:10px;margin-bottom:5;"><%=den(1)%></h1>
<table class="principal" style="margin-left:10px">
	<tr>
		<td colspan="2" class="linea1">
		&nbsp;
		</td>
	</tr>
	<tr>
		<td width="20%" class="fpeque"><nobr><%=den(2)%></nobr></td>
		<td class="fpeque"><%=request("nombre")%></td>
	</tr>
	<tr>
		<td class="fpeque"><nobr><%=den(3)%></nobr></td>
		<td class="fpeque"><%=visualizacionNumero(numero(request("datasize"))/1024, decimalfmt,thousandfmt,1)%> Kb.</td>
	</tr>
	<tr>
		<td colspan="2" class="linea2">
		&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="2">
		&nbsp;
		</td>
	</tr>
	<tr>
		<td class="fpeque" colspan="2"><%=den(4)%>&nbsp;<a href="<%=application("RUTASEGURA")%>/download/<%=request("path") & "/" & request("nombre")%>" target="_self"><img border="0" SRC="images/download.gif" WIDTH="16" HEIGHT="16"></a></td>
	</tr>
	<tr>
		<td colspan="2">
		&nbsp;
		</td>
	</tr>
	<tr>
		<td class="fpeque" colspan="2"><%=den(5)%>&nbsp;<a href="<%=application("RUTASEGURA")%>/download/<%=request("path") & "/" & request("nombre")%>" target="_self"><img border="0" SRC="images/download.gif" WIDTH="16" HEIGHT="16"></a></td>
	</tr>

</table>



<p>&nbsp;</p>

</body>
</html>
