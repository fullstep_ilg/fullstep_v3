﻿<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/colores.asp"-->
<!--#include file="../common/idioma.asp"-->
<%

datefmt=Request.Cookies("USU_DATEFMT") 


idioma = Request.Cookies("USU_IDIOMA")

dim den

den= devolverTextos(Idioma,110)


if idioma="ENG" then%>
var weekend = [5,6];
<%else%>
var weekend = [6,7];
<%end if%>

var weekendColor = "<%=cWEEKENDCOLOR%>";
var fontface = "Verdana";
var fontsize = 2;
var gNow = new Date();
var ggWinCal;
isNav = (navigator.appName.indexOf("Netscape") != -1) ? true : false;
isIE = (navigator.appName.indexOf("Microsoft") != -1) ? true : false;

Calendar.Months = ["<%=Den(1)%>", "<%=Den(2)%>", "<%=Den(3)%>", "<%=Den(4)%>", "<%=Den(5)%>", "<%=Den(6)%>",
"<%=Den(7)%>", "<%=Den(8)%>", "<%=Den(9)%>", "<%=Den(10)%>", "<%=Den(11)%>", "<%=Den(12)%>"];

// Non-Leap year Month days..
Calendar.DOMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
// Leap year Month days..
Calendar.lDOMonth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

function Calendar(p_item, p_WinCal, p_month, p_year, p_format) {
	if ((p_month == null) && (p_year == null))	return;

	if (p_WinCal == null)
		this.gWinCal = ggWinCal;
	else
		this.gWinCal = p_WinCal;
	
	if (p_month == null) {
		this.gMonthName = null;
		this.gMonth = null;
		this.gYearly = true;
	} else {
		this.gMonthName = Calendar.get_month(p_month);
		this.gMonth = new Number(p_month);
		this.gYearly = false;
	}

	this.gYear = p_year;
	this.gFormat = p_format;
	this.gBGColor = "white";
	this.gFGColor = "black";
	this.gTextColor = "black";
	this.gHeaderColor = "black";
	this.gReturnItem = p_item;
}

Calendar.get_month = Calendar_get_month;
Calendar.get_daysofmonth = Calendar_get_daysofmonth;
Calendar.calc_month_year = Calendar_calc_month_year;
Calendar.print = Calendar_print;

function Calendar_get_month(monthNo) {
	return Calendar.Months[monthNo];
}

function Calendar_get_daysofmonth(monthNo, p_year) {
	/* 
	Check for leap year ..
	1.Years evenly divisible by four are normally leap years, except for... 
	2.Years also evenly divisible by 100 are not leap years, except for... 
	3.Years also evenly divisible by 400 are leap years. 
	*/
	if ((p_year % 4) == 0) {
		if ((p_year % 100) == 0 && (p_year % 400) != 0)
			return Calendar.DOMonth[monthNo];
	
		return Calendar.lDOMonth[monthNo];
	} else
		return Calendar.DOMonth[monthNo];
}

function Calendar_calc_month_year(p_Month, p_Year, incr) {
	/* 
	Will return an 1-D array with 1st element being the calculated month 
	and second being the calculated year 
	after applying the month increment/decrement as specified by 'incr' parameter.
	'incr' will normally have 1/-1 to navigate thru the months.
	*/
	var ret_arr = new Array();
	
	if (incr == -1) {
		// B A C K W A R D
		if (p_Month == 0) {
			ret_arr[0] = 11;
			ret_arr[1] = parseInt(p_Year) - 1;
		}
		else {
			ret_arr[0] = parseInt(p_Month) - 1;
			ret_arr[1] = parseInt(p_Year);
		}
	} else if (incr == 1) {
		// F O R W A R D
		if (p_Month == 11) {
			ret_arr[0] = 0;
			ret_arr[1] = parseInt(p_Year) + 1;
		}
		else {
			ret_arr[0] = parseInt(p_Month) + 1;
			ret_arr[1] = parseInt(p_Year);
		}
	}
	
	return ret_arr;
}

function Calendar_print() {
	ggWinCal.print();
}

function Calendar_calc_month_year(p_Month, p_Year, incr) {
	/* 
	Will return an 1-D array with 1st element being the calculated month 
	and second being the calculated year 
	after applying the month increment/decrement as specified by 'incr' parameter.
	'incr' will normally have 1/-1 to navigate thru the months.
	*/
	var ret_arr = new Array();
	
	if (incr == -1) {
		// B A C K W A R D
		if (p_Month == 0) {
			ret_arr[0] = 11;
			ret_arr[1] = parseInt(p_Year) - 1;
		}
		else {
			ret_arr[0] = parseInt(p_Month) - 1;
			ret_arr[1] = parseInt(p_Year);
		}
	} else if (incr == 1) {
		// F O R W A R D
		if (p_Month == 11) {
			ret_arr[0] = 0;
			ret_arr[1] = parseInt(p_Year) + 1;
		}
		else {
			ret_arr[0] = parseInt(p_Month) + 1;
			ret_arr[1] = parseInt(p_Year);
		}
	}
	
	return ret_arr;
}

// This is for compatibility with Navigator 3, we have to create and discard one object before the prototype object exists.
new Calendar();

Calendar.prototype.getMonthlyCalendarCode = function() {
	var vCode = "";
	var vHeader_Code = "";
	var vData_Code = "";
	
	// Begin Table Drawing code here..
	vCode = vCode + "<script>\n"
	vCode = vCode + "var vdatefmt = \"<%=datefmt%>\"\n"
	vCode = vCode + "function ponerFecha(e,fDay)\n"
	vCode = vCode + "{\n"

	vCode = vCode + "var bLimites = true\n"
	vCode = vCode + "var dtFecha = new Date(self.opener.str2date(fDay,vdatefmt))\n"

	if (p_valorMin)
		{
		vCode = vCode + "var dtMin = new Date(self.opener.str2date('" + p_valorMin + "',vdatefmt))\n"
		vCode = vCode + "if (dtMin>dtFecha)\n"
		vCode = vCode + "bLimites = false\n"
		}
	if (p_valorMax)
		{
		vCode = vCode + "var dtMax = new Date(self.opener.str2date('" + p_valorMax + "',vdatefmt))\n"
		vCode = vCode + "if (dtMax<dtFecha)\n"
		vCode = vCode + "bLimites = false\n"
		}


	vCode = vCode + "if (bLimites)\n"
		vCode = vCode + "{\n"
		vCode = vCode + "ret=true\n"
		if (p_function)
		vCode = vCode + "	ret=self.opener." + p_function + "(self.opener.document." + this.gReturnItem  + ",fDay)\n"
		vCode = vCode + "if (ret)\n"
			vCode = vCode + "{\n"
			vCode = vCode + "e.value = fDay\n"
			vCode = vCode + "setTimeout(\"window.close()\",300)\n"
			vCode = vCode + "}\n"
		vCode = vCode + "else\n"
			vCode = vCode + "{\n"
			vCode = vCode + "window.focus()\n"
			vCode = vCode + "}\n"
		vCode = vCode + "}\n"
	vCode = vCode + "else\n"
	vCode = vCode + "{\n"

	vCode = vCode + "if (!bLimites)"
	vCode = vCode + "alert(\"<%=den(23)%>\")\n"
	vCode = vCode + "window.focus()\n"
	vCode = vCode + "}\n"
	vCode = vCode + "}\n"
	vCode = vCode + "</SCRIPT>\n"
	
	vCode = vCode + "<TABLE BORDER=1  bordercolor=\"<%=cBORDECALENDARIO%>\" cellspacing=\"1\" BGCOLOR=\"" + this.gBGColor + "\">";
	
	vHeader_Code = this.cal_header();
	vData_Code = this.cal_data();
	vCode = vCode + vHeader_Code + vData_Code;
	
	vCode = vCode + "</table>";
	
	return vCode;
}

Calendar.prototype.show = function() {
	var vCode = "";
	
	this.gWinCal.document.open();

	// Setup the page...
	this.wwrite("<html>");
	this.wwrite("<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title><%=title%></title>");
	this.wwrite("</head>");

	this.wwrite("<body " + 
		"link=\"" + this.gLinkColor + "\" " + 
		"vlink=\"" + this.gLinkColor + "\" " +
		"alink=\"" + this.gLinkColor + "\" " +
		"text=\"" + this.gTextColor + "\">");
	this.wwriteA("<div align = \"center\"><FONT FACE='" + fontface + "' SIZE=2><B>");
	this.wwriteA(this.gMonthName + " " + this.gYear);
	this.wwriteA("</B></DIV><BR>");

	// Show navigation buttons
	var prevMMYYYY = Calendar.calc_month_year(this.gMonth, this.gYear, -1);
	var prevMM = prevMMYYYY[0];
	var prevYYYY = prevMMYYYY[1];

	var nextMMYYYY = Calendar.calc_month_year(this.gMonth, this.gYear, 1);
	var nextMM = nextMMYYYY[0];
	var nextYYYY = nextMMYYYY[1];
	
	this.wwrite("<TABLE WIDTH='100%' BORDER=0 CELLSPACING=0 CELLPADDING=0 BGCOLOR='#FFFFFF'><TR><TD ALIGN=right>");
	this.wwrite("<A HREF=\"" +
		"javascript:window.opener.Build(" + 
		"'" + this.gReturnItem + "', '" + this.gMonth + "', '" + (parseInt(this.gYear)-1) + "', '" + this.gFormat + "'" +
		");" +
		"\"><img src = \"<%=Application("RUTASEGURA")%>script/common/images/flecha_i.gif\" border = \"0\"><\/A></TD><TD ALIGN=center>");
	this.wwrite("<div align = \"center\"><FONT FACE='" + fontface + "' SIZE=1><B><%=den(21)%></B></TD><TD align=left>");
	this.wwrite("<A HREF=\"" +
		"javascript:window.opener.Build(" + 
		"'" + this.gReturnItem + "', '" + this.gMonth + "', '" + (parseInt(this.gYear)+1) + "', '" + this.gFormat + "'" +
		");" +
		"\"><img src = \"<%=Application("RUTASEGURA")%>script/common/images/flecha_d.gif\" border = \"0\"><\/A><BR></TD><TD ALIGN=right>");
	
	this.wwrite("<A HREF=\"" +
		"javascript:window.opener.Build(" + 
		"'" + this.gReturnItem + "', '" + prevMM + "', '" + prevYYYY + "', '" + this.gFormat + "'" +
		");" +
		"\"><img src = \"<%=Application("RUTASEGURA")%>script/common/images/flecha_i.gif\" border = \"0\"><\/A></TD><TD ALIGN=center>");
	this.wwrite("<div align = \"center\"><FONT FACE='" + fontface + "' SIZE=1><B><%=den(20)%></B></TD><TD>");
	this.wwrite("<A HREF=\"" +
		"javascript:window.opener.Build(" + 
		"'" + this.gReturnItem + "', '" + nextMM + "', '" + nextYYYY + "', '" + this.gFormat + "'" +
		");" +
		"\"><img src = \"<%=Application("RUTASEGURA")%>script/common/images/flecha_d.gif\" border = \"0\"><\/A></TD></TR></TABLE><BR>");

	// Get the complete calendar code for the month..
	vCode = this.getMonthlyCalendarCode();
	this.wwrite(vCode);

	this.wwrite("</font></body></html>");
	this.gWinCal.document.close();
}

Calendar.prototype.showY = function() {
	var vCode = "";
	var i;
	var vr, vc, vx, vy;		// Row, Column, X-coord, Y-coord
	var vxf = 285;			// X-Factor
	var vyf = 200;			// Y-Factor
	var vxm = 10;			// X-margin
	var vym;				// Y-margin
	if (isIE)	vym = 75;
	else if (isNav)	vym = 25;
	
	this.gWinCal.document.open();

	this.wwrite("<html>");
	this.wwrite("<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title></title>");
	this.wwrite("<style type='text/css'>\n<!--");
	for (i=0; i<12; i++) {
		vc = i % 3;
		if (i>=0 && i<= 2)	vr = 0;
		if (i>=3 && i<= 5)	vr = 1;
		if (i>=6 && i<= 8)	vr = 2;
		if (i>=9 && i<= 11)	vr = 3;
		
		vx = parseInt(vxf * vc) + vxm;
		vy = parseInt(vyf * vr) + vym;

		this.wwrite(".lclass" + i + " {position:absolute;top:" + vy + ";left:" + vx + ";}");
	}
	this.wwrite("-->\n</style>");
	this.wwrite("</head>");

	this.wwrite("<body " + 
		"link=\"" + this.gLinkColor + "\" " + 
		"vlink=\"" + this.gLinkColor + "\" " +
		"alink=\"" + this.gLinkColor + "\" " +
		"text=\"" + this.gTextColor + "\">");
	this.wwrite("<FONT FACE='" + fontface + "' SIZE=2><B>");
	this.wwrite("Year : " + this.gYear);
	this.wwrite("</B><BR>");

	// Show navigation buttons
	var prevYYYY = parseInt(this.gYear) - 1;
	var nextYYYY = parseInt(this.gYear) + 1;
	
	

	// Get the complete calendar code for each month..
	var j;
	for (i=11; i>=0; i--) {
		if (isIE)
			this.wwrite("<DIV ID=\"layer" + i + "\" CLASS=\"lclass" + i + "\">");
		else if (isNav)
			this.wwrite("<LAYER ID=\"layer" + i + "\" CLASS=\"lclass" + i + "\">");

		this.gMonth = i;
		this.gMonthName = Calendar.get_month(this.gMonth);
		vCode = this.getMonthlyCalendarCode();
		this.wwrite(this.gMonthName + "/" + this.gYear + "<br>");
		this.wwrite(vCode);

		if (isIE)
			this.wwrite("</div>");
		else if (isNav)
			this.wwrite("</layer>");
	}

	this.wwrite("</font><br></body></html>");
	this.gWinCal.document.close();
}

Calendar.prototype.wwrite = function(wtext) {
	this.gWinCal.document.writeln(wtext);
}

Calendar.prototype.wwriteA = function(wtext) {
	this.gWinCal.document.write(wtext);
}

Calendar.prototype.cal_header = function() {
	var vCode = "";
	
<%if idioma="ENG" then%>	
	vCode = vCode + "<TR>";
	vCode = vCode + "<TD WIDTH='14%' bgcolor=\"<%=cDAYCOLOR%>\"><FONT SIZE='2' FACE='" + fontface + "' COLOR=\"#FFFFFF\"><B><%=den(19)%></B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%' bgcolor=\"<%=cDAYCOLOR%>\"><FONT SIZE='2' FACE='" + fontface + "' COLOR=\"#FFFFFF\"><B><%=den(13)%></B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%' bgcolor=\"<%=cDAYCOLOR%>\"><FONT SIZE='2' FACE='" + fontface + "' COLOR=\"#FFFFFF\"><B><%=den(14)%></B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%' bgcolor=\"<%=cDAYCOLOR%>\"><FONT SIZE='2' FACE='" + fontface + "' COLOR=\"#FFFFFF\"><B><%=den(15)%></B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%' bgcolor=\"<%=cDAYCOLOR%>\"><FONT SIZE='2' FACE='" + fontface + "' COLOR=\"#FFFFFF\"><B><%=den(16)%></B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%' bgcolor=\"<%=cDAYCOLOR%>\"><FONT SIZE='2' FACE='" + fontface + "' COLOR=\"#FFFFFF\"><B><%=den(17)%></B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%' bgcolor=\"<%=cDAYCOLOR%>\"><FONT SIZE='2' FACE='" + fontface + "' COLOR=\"#FFFFFF\"><B><%=den(18)%></B></FONT></TD>";
	vCode = vCode + "</TR>";
<%else%>
	vCode = vCode + "<TR>";
	vCode = vCode + "<TD WIDTH='14%' bgcolor=\"<%=cDAYCOLOR%>\"><FONT SIZE='2' FACE='" + fontface + "' COLOR=\"#FFFFFF\"><B><%=den(13)%></B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%' bgcolor=\"<%=cDAYCOLOR%>\"><FONT SIZE='2' FACE='" + fontface + "' COLOR=\"#FFFFFF\"><B><%=den(14)%></B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%' bgcolor=\"<%=cDAYCOLOR%>\"><FONT SIZE='2' FACE='" + fontface + "' COLOR=\"#FFFFFF\"><B><%=den(15)%></B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%' bgcolor=\"<%=cDAYCOLOR%>\"><FONT SIZE='2' FACE='" + fontface + "' COLOR=\"#FFFFFF\"><B><%=den(16)%></B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%' bgcolor=\"<%=cDAYCOLOR%>\"><FONT SIZE='2' FACE='" + fontface + "' COLOR=\"#FFFFFF\"><B><%=den(17)%></B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%' bgcolor=\"<%=cDAYCOLOR%>\"><FONT SIZE='2' FACE='" + fontface + "' COLOR=\"#FFFFFF\"><B><%=den(18)%></B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%' bgcolor=\"<%=cDAYCOLOR%>\"><FONT SIZE='2' FACE='" + fontface + "' COLOR=\"#FFFFFF\"><B><%=den(19)%></B></FONT></TD>";
	vCode = vCode + "</TR>";
<%end if%>
	
	return vCode;
}

Calendar.prototype.cal_data = function() {
	var vDate = new Date();
	vDate.setDate(1);
	vDate.setMonth(this.gMonth);
	vDate.setFullYear(this.gYear);

	var vFirstDay=vDate.getDay();
	var vDay=1;
	var vLastDay=Calendar.get_daysofmonth(this.gMonth, this.gYear);
	var vOnLastDay=0;
	var vCode = "";

	/*
	Get day for the 1st of the requested month/year..
	Place as many blank cells before the 1st day of the month as necessary. 
	*/

	// Fix case: 1st day of the month is Sunday
	if (vFirstDay == 0) {
		vFirstDay = 7;
	}
	
	vCode = vCode + "<tr>";
	<%if idioma="ENG" then%>
	for (i=0; i<vFirstDay; i++) { 
	<%else%> 
	for (i=1; i<vFirstDay; i++) { 
	<%end if%> 
		vCode = vCode + "<TD WIDTH='14%'" + this.write_weekend_string(i) + "><DIV align = \"center\"><FONT SIZE='2' FACE='" + fontface + "'> </DIV></FONT></TD>";
	}

	// Write rest of the 1st week
	for (j=vFirstDay; j<<%if idioma="ENG" then%>7<%else%>8<%end if%>; j++) {
		vCode = vCode + "<TD WIDTH='14%'" + this.write_weekend_string(j) + "><DIV align = \"center\"><FONT SIZE='2' FACE='" + fontface + "'>" + 
				"<A HREF='#' " + 
					"onClick=\"ponerFecha(self.opener.document." + this.gReturnItem + ",'" + this.format_data(vDay) + "');\">" + 
				this.format_day(vDay) + 
				"</A>" + 
				"</FONT></DIV></TD>";
		vDay=vDay + 1;

/*		if (j>=7) {
		    break;
		}*/
		
	}
	vCode = vCode + "</tr>";

	// Write the rest of the weeks
	for (k=2; k<7; k++) { vCode=vCode + "<TR>";
<%if idioma="ENG" then%>
		for (j=0; j<7; j++) { 
<%else%> 
		for (j=1; j<8; j++) { 
<%end if%> 
			vCode = vCode + "<TD WIDTH='14%'" + this.write_weekend_string(j) + "><DIV align = \"center\"><FONT SIZE='2' FACE='" + fontface + "'>" + 
				"<A HREF='#' " + 
					"onClick=\"ponerFecha(self.opener.document." + this.gReturnItem + ",'" + this.format_data(vDay) + "');\">" + 
				this.format_day(vDay) + 
				"</A>" + 
				"</FONT></DIV></TD>";
			vDay=vDay + 1;

			if (vDay > vLastDay) {
				vOnLastDay = 1;
				break;
			}

/*			if (j>=7) {
			    break;
			}*/
		}

		if (j == <%if idioma="ENG" then%>6<%else%>7<%end if%>)
			vCode = vCode + "</tr>";
		if (vOnLastDay == 1)
			break;
	}
	
	// Fill up the rest of last week with proper blanks, so that we get proper square blocks
	for (m=1; m<(<%if idioma="ENG" then%>7<%else%>8<%end if%>-j); m++) {
		if (this.gYearly)
			vCode = vCode + "<TD WIDTH='14%'" + this.write_weekend_string(j+m) + 
			"><DIV align = \"center\"><FONT SIZE='2' FACE='" + fontface + "' COLOR='gray'> </DIV></FONT></TD>";
		else
			vCode = vCode + "<TD WIDTH='14%'" + this.write_weekend_string(j+m) + 
			"><DIV align = \"center\"><FONT SIZE='2' FACE='" + fontface + "' COLOR='gray'>" + m + "</DIV></FONT></TD>";
	}
	
	return vCode;
}

Calendar.prototype.format_day = function(vday) {
	var vNowDay = gNow.getDate();
	var vNowMonth = gNow.getMonth();
	var vNowYear = gNow.getFullYear();

	if (vday == vNowDay && this.gMonth == vNowMonth && this.gYear == vNowYear)
		return ("<FONT COLOR=\"RED\"><B>" + vday + "</B></FONT>");
	else
		return (vday);
}

Calendar.prototype.write_weekend_string = function(vday) {
	var i;

	// Return special formatting for the weekend day.
	for (i=0; i<weekend.length; i++) {
		if (vday == weekend[i])
			return (" BGCOLOR=\"" + weekendColor + "\"");
	}
	
	return "";
}

Calendar.prototype.format_data = function(p_day) {
	var vData;
	var vMonth = 1 + this.gMonth;
	vMonth = (vMonth.toString().length < 2) ? "0" + vMonth : vMonth;
	var vMon = Calendar.get_month(this.gMonth).substr(0,3).toUpperCase();
	var vFMon = Calendar.get_month(this.gMonth).toUpperCase();
	var vY4 = new String(this.gYear);
	var vY2 = new String(this.gYear.substr(2,2));
	var vDD = (p_day.toString().length < 2) ? "0" + p_day : p_day;
	switch (this.gFormat) {
		case "MM\/DD\/YYYY" :
		case "mm\/dd\/yyyy" :
			vData = vMonth + "\/" + vDD + "\/" + vY4;
			break;
		case "MM\/DD\/YY" :
			vData = vMonth + "\/" + vDD + "\/" + vY2;
			break;
		case "MM-DD-YYYY" :
		case "mm-dd-yyyy" :
			vData = vMonth + "-" + vDD + "-" + vY4;
			break;
		case "MM-DD-YY" :
			vData = vMonth + "-" + vDD + "-" + vY2;
			break;

		case "DD\/MON\/YYYY" :
			vData = vDD + "\/" + vMon + "\/" + vY4;
			break;
		case "DD\/MON\/YY" :
			vData = vDD + "\/" + vMon + "\/" + vY2;
			break;
		case "DD-MON-YYYY" :
			vData = vDD + "-" + vMon + "-" + vY4;
			break;
		case "DD-MON-YY" :
			vData = vDD + "-" + vMon + "-" + vY2;
			break;

		case "DD\/MONTH\/YYYY" :
			vData = vDD + "\/" + vFMon + "\/" + vY4;
			break;
		case "DD\/MONTH\/YY" :
			vData = vDD + "\/" + vFMon + "\/" + vY2;
			break;
		case "DD-MONTH-YYYY" :
			vData = vDD + "-" + vFMon + "-" + vY4;
			break;
		case "DD-MONTH-YY" :
			vData = vDD + "-" + vFMon + "-" + vY2;
			break;

		case "DD\/MM\/YYYY" :
		case "dd/mm/yyyy" :
			vData = vDD + "\/" + vMonth + "\/" + vY4;
			break;
		case "DD\/MM\/YY" :
			vData = vDD + "\/" + vMonth + "\/" + vY2;
			break;
		case "DD-MM-YYYY" :
		case "dd-mm-yyyy" :
			vData = vDD + "-" + vMonth + "-" + vY4;
			break;
		case "dd.mm.yyyy" :
			vData = vDD + "." + vMonth + "." + vY4;
			break;
		case "mm.dd.yyyy" :
			vData = vMonth + "." + vDD + "." + vY4;
			break;
		case "DD-MM-YY" :
			vData = vDD + "-" + vMonth + "-" + vY2;
			break;

		default :
			vData = vMonth + "\/" + vDD + "\/" + vY4;
	}

	return vData;
}

function Build(p_item, p_month, p_year, p_format) {
	var p_WinCal = ggWinCal;
	gCal = new Calendar(p_item, p_WinCal, p_month, p_year, p_format);

	// Customize your Calendar here..
	gCal.gBGColor="white";
	gCal.gLinkColor="black";
	gCal.gTextColor="black";
	gCal.gHeaderColor="darkgreen";

	// Choose appropriate show function
	if (gCal.gYearly)	gCal.showY();
	else	gCal.show();
}

function show_calendar() {
	/* 
		p_month : 0-11 for Jan-Dec; 12 for All Months.
		p_year	: 4-digit year
		p_format: Date format (mm/dd/yyyy, dd/mm/yy, ...)
		p_item	: Return Item.
	*/

	p_item = arguments[0];
	if (arguments[1] == null)
		p_month = new String(gNow.getMonth());
	else
		p_month = arguments[1];
	if (arguments[2] == "" || arguments[2] == null)
		p_year = new String(gNow.getFullYear().toString());
	else
		p_year = arguments[2];
	if (arguments[3] == null)
		p_format = "DD/MM/YYYY";
	else
		p_format = arguments[3];
		
	p_function = arguments[4]
	
	p_valorMin = arguments[5]
	p_valorMax = arguments[6]

	vWinCal = window.open("<%=application("RUTASEGURA")%>script/blank.htm", "Calendar", 
		"width=250,height=245,status=no,resizable=no,top=200,left=200");
	vWinCal.focus()
	vWinCal.opener = self;
	ggWinCal = vWinCal;

	Build(p_item, p_month, p_year, p_format);
}
/*
Yearly Calendar Code Starts here
*/
function show_yearly_calendar(p_item, p_year, p_format) {
	// Load the defaults..
	if (p_year == null || p_year == "")
		p_year = new String(gNow.getFullYear().toString());
	if (p_format == null || p_format == "")
		p_format = "DD/MM/YYYY";

	var vWinCal = window.open("<%=application("RUTASEGURA")%>script/blank.htm", "Calendar", "scrollbars=yes");
	vWinCal.opener = self;
	ggWinCal = vWinCal;

	Build(p_item, null, p_year, p_format);
}

