﻿<!--#include file="XSS.asp"-->

<% 

if request("download")<>"1" then
	Response.CacheControl = "no-cache" 
	Response.AddHeader "Pragma", "no-cache"
	Response.Expires = -1 
end if


'titulo común a todas las páginas
Idioma = Request.Cookies("USU_IDIOMA")
titulo="TITULOVENTANAS_" & Idioma
title=application(titulo)
%>


<%
''' <summary>
''' Validar la Sesion
''' </summary>
''' <param name="Idioma">Idioma</param>
''' <param name="CargarSesion">True objeto sesion cargas todo. false objeto sesion solo con ID</param>
''' <param name="CargarParam">True objeto sesion cargas parametros. false no cargas parametros</param> 
''' <param name="CSRFToken2">-1- no hacer nada 0- limpia token contra Cross Site Request Forgery 1- generar token contra Cross Site Request Forgery.2-comprobar token contra Cross Site Request Forgery</param>
''' <remarks>Llamada desde: 147 ficheros asp de la aplicación tienen un include para esta validacion; Tiempo máximo: 0,2</remarks>
function ValidarUsuario(Idioma,CargarSesion,CargarParam, CSRFToken)

dim oRaiz

set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
	
if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	set oraiz = nothing
	set validarUsuario=nothing
	Response.Redirect Application ("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	Response.End 
	exit function
end if

IPDir=Request.ServerVariables("LOCAL_ADDR")
PersistID=Request.Cookies("SESPP")
	
set oRaiz =  oRaiz.ValidarSesion(Request.Cookies("USU_SESIONID"),IPDir,PersistID,CargarSesion,CargarParam,CSRFToken,Request.Cookies("USU_CSRFTOKEN"))  

if oRaiz is nothing then		    
    set oRaiz = nothing
	set ValidarUsuario=nothing
	%>
	<script>
	    window.open("<%=application("RUTASEGURA")%>script/usuarionovalido.asp?Idioma=<%=Idioma%>&ErrorSesion=2", "_blank", "top:100;left:100;width:300;height:150,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no")
	</script>
	<%
	Response.End 
	exit function
end if
	
set ousuario=nothing

set ValidarUsuario=oRaiz

set oRaiz = nothing

end function

''' <summary>Validar la Sesion para acceso externo</summary>
''' <param name="Idioma">Idioma</param>
''' <param name="CargarSesion">True objeto sesion cargas todo. false objeto sesion solo con ID</param>
''' <param name="CargarParam">True objeto sesion cargas parametros. false no cargas parametros</param> 
''' <param name="CSRFToken2">-1- no hacer nada 0- limpia token contra Cross Site Request Forgery 1- generar token contra Cross Site Request Forgery.2-comprobar token contra Cross Site Request Forgery</param>
''' <remarks>Llamada desde: 147 ficheros asp de la aplicación tienen un include para esta validacion; Tiempo máximo: 0,2</remarks>
function ValidarUsuarioAccesoExterno(Idioma,CargarSesion,CargarParam, CSRFToken,Cia,Usu, IdSesionExterno)
    dim oRaiz

    set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")	
    if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	    set oraiz = nothing
	    set ValidarUsuarioAccesoExterno=nothing
	    Response.Redirect Application ("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	    Response.End 
	else
        IPDir=Request.ServerVariables("LOCAL_ADDR")
        PersistID=Request.Cookies("SESPP")
        SesionID=Request.Cookies("USU_SESIONID")
	   
        if SesionID<>"" then
            set oRaiz =  oRaiz.ValidarSesion(Request.Cookies("USU_SESIONID"),IPDir,PersistID,CargarSesion,CargarParam,CSRFToken,Request.Cookies("USU_CSRFTOKEN"))  
            if oRaiz is nothing then                
                set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
                if (oRaiz.Conectar(Application("INSTANCIA"))) = 0 then LoginAccesoExterno oRaiz,Cia,Usu,IdSesionExterno	                   	                                       
            else
                if oRaiz.Sesion.CiaCod<>Cia and oRaiz.Sesion.UsuCod<>Usu then                    
                    set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
                    if (oRaiz.Conectar(Application("INSTANCIA"))) = 0 then LoginAccesoExterno oRaiz,Cia,Usu,IdSesionExterno	            
                end if
            end if	            
        else
            'Hacer login  
            LoginAccesoExterno oRaiz,Cia,Usu,IdSesionExterno                  	            
        end if

        set ValidarUsuarioAccesoExterno=oRaiz                
        set oRaiz = nothing
    end if                         
end function

function LoginAccesoExterno(oRaiz,Cia,Usu,IdSesionExterno)           
    dim IsSecure
    IsSecure = left(Application ("RUTASEGURA"),8)
    if IsSecure = "https://" then                       
        IsSecure = "; secure"
    else
        IsSecure =""
    end if
           
    set Sesion = oRaiz.ValidarLogin(Cia,Usu,"",cint(Application("PORTAL")),Application("PYME"),false,Application("APROVISIONAMIENTO"),Application("SOLICITUDESPM"), _
                                    Application("CERTIFICADOS"),Application("NOCONFORMIDADES"),Application("CN"),Request.ServerVariables("LOCAL_ADDR"), _
                                    Request.Cookies("SESPP"),"",Application("NOMPORTAL"),0,true,IdSesionExterno)		
		    
    'Si la contraseña esta caducada redireccionamos a pantalla de cambio de contraseña
    Response.AddHeader "Set-Cookie","USU_CADPASSWORD=0; path=/; HttpOnly" + IsSecure
    if Sesion.Id ="" then 
        if oraiz.UsuarioYaLogeado = 0 then
            response.redirect application("RUTASEGURA") & "script/usuarionovalido.asp?Idioma=" & Idioma & "&ErrorSesion=0"
		else
			response.redirect application("RUTASEGURA") & "script/usuarionovalido.asp?Idioma=" & Idioma & "&ErrorSesion=1"
		end if
	else             
        set oRaiz= oRaiz.ValidarSesion(Sesion.id,Request.ServerVariables("LOCAL_ADDR"),Sesion.PersistId,true,true,CSRFToken,Request.Cookies("USU_CSRFTOKEN"))  
                  
        'En la cookie USU se escribirá el ID de sesión generado y los datos actuales MOSTRARFMT, THOUSANFMT, DECIMALFMT, PRECISIONFMT, DATEFMT, IDIOMA, TIPOMAIL
        Response.AddHeader "Set-Cookie","USU_SESIONID=" &  server.URLEncode(Sesion.Id) & "; path=/; HttpOnly" + IsSecure
        Response.AddHeader "Set-Cookie","USU_USUID=" &  server.URLEncode(Sesion.UsuId) & "; path=/; HttpOnly" + IsSecure
        Response.AddHeader "Set-Cookie","USU_CIAID=" &  server.URLEncode(Sesion.CiaId) & "; path=/; HttpOnly" + IsSecure
        Response.AddHeader "Set-Cookie","USU_MOSTRARFMT=" &  Sesion.MostrarFMT  & "; path=/; HttpOnly" + IsSecure
        Response.AddHeader "Set-Cookie","USU_DECIMALFMT=" & Sesion.DecimalFMT  & "; path=/; HttpOnly" + IsSecure
        Response.AddHeader "Set-Cookie","USU_THOUSANFMT=" & Sesion.ThousanFMT  & "; path=/; HttpOnly" + IsSecure
        Response.AddHeader "Set-Cookie","USU_DATEFMT=" & Sesion.DateFMT  & "; path=/; HttpOnly" + IsSecure
        Response.AddHeader "Set-Cookie","USU_PRECISIONFMT=" & Sesion.PrecisionFMT  & "; path=/; HttpOnly" + IsSecure
        Response.AddHeader "Set-Cookie","USU_TIPOMAIL=" & Sesion.TipoMail  & "; path=/; HttpOnly" + IsSecure
        Response.AddHeader "Set-Cookie","USU_IDIOMA=" &   Sesion.Idioma  & "; path=/; HttpOnly" + IsSecure        
        Response.AddHeader "Set-Cookie","USU_CSRFTOKEN=""; path=/; HttpOnly" + IsSecure        
        Response.AddHeader "Set-Cookie","ACCESO_EXTERNO=" & Application("REGISTRO_EXTERNO") &"; path=/; HttpOnly" + IsSecure
        Response.AddHeader "Set-Cookie","USU_SESIONID_EXTERNO=" & IdSesionExterno & "; path=/; HttpOnly" + IsSecure
	    Response.AddHeader "Set-Cookie","USU_USUCOD=" & Usu & "; path=/; HttpOnly" + IsSecure
	    Response.AddHeader "Set-Cookie","USU_CIACOD=" & Cia & "; path=/; HttpOnly" + IsSecure
		If Sesion.Parametros.EmpresaPortal Then
	        Response.AddHeader "Set-Cookie","EMPRESAPORTAL=1; path=/; HttpOnly" + IsSecure
		Else
	        Response.AddHeader "Set-Cookie","EMPRESAPORTAL=0; path=/; HttpOnly" + IsSecure
		End If
        Response.AddHeader "Set-Cookie","NOM_PORTAL=" & Application("NOMPORTAL") & "; path=/; HttpOnly" + IsSecure

        if Sesion.Parametros.unaCompradora then
			application("UNACOMPRADORA") = 1
			application("IDCIACOMPRADORA") = Sesion.Parametros.IdCiaCompradora
		else
			application("UNACOMPRADORA") = 0
        end if

        'El Id generado para la cookie persistente se almacenará en una cookie de nombre SESPP (sesión portal persistente) a la que pondremos una caducidad de 24 horas.                                        
        dFecha=Date+1

        sDia=cstr(day(dFecha))
        sUrte = cstr(year(dFecha))

        sDia = left("00",2-len(sDia)) & sDia
                                    
        select case Weekday(dFecha)
            case 1:
                sDiaWeek = "Sun, "
            case 2:
                sDiaWeek = "Mon, "
            case 3: 
                sDiaWeek = "Tue, "
            case 4: 
                sDiaWeek = "Wed, "
            case 5: 
                sDiaWeek = "Thu, "
            case 6:
                sDiaWeek = "Fri, "
            case 7:
                sDiaWeek = "Sat, "
        end select 

        select case month(dFecha)
            case 1:
                sMes= "Jan"
            case 2:
                sMes= "Feb"
            case 3:
                sMes= "Mar"
            case 4:
                sMes= "Apr"
            case 5:
                sMes= "May"
            case 6:
                sMes= "Jun"
            case 7:
                sMes= "Jul"
            case 8:
                sMes= "Aug"
            case 9:
                sMes= "Sep"
            case 10:
                sMes= "Oct"
            case 11:
                sMes= "Nov"
            case 12:
                sMes= "Dec"
        end select
                    
        dVisualizacionFecha = sDiaWeek + sDia + "-" +  sMes + "-" + sUrte + " 00:00:00 GMT"                                                                      

        Response.AddHeader "Set-Cookie","SESPP=" & server.URLEncode(Sesion.PersistID)  & "; path=/; HttpOnly; Expires=" + dVisualizacionFecha + IsSecure

		servidor = Request.servervariables("SERVER_NAME")
		path = replace(path,"http://" & servidor,"")
		path = replace(path,"https://" & servidor,"")             
	end if       
end function

''' <summary>
''' Obtiene el UsuCod y CiaCod en un array (FSAL)
''' </summary>
''' <remarks>Llamada desde: fsal_1.asp, fsal_7.asp; Tiempo máximo: </remarks>
function ObtenerUsuCodCiaCod(idSesion,iP,persistID,CSRFToken)
    Dim arrResultado : arrResultado = Array()

    set oRaiz = createobject ("FSPServer_" & Application ("NUM_VERSION") & ".CRaiz")
    if (oRaiz.Conectar(Application("INSTANCIA"))) = 1 then
	    set oRaiz = Nothing
	    Response.Redirect Application ("RUTASEGURA") & "script/imposibleconectar.asp?Idioma=" & Idioma
	    Response.End
        ObtenerUsuCodCiaCod=arrResultado
        exit function
    end if

    set oRaizConSesion=oRaiz.ValidarSesion(idSesion,iP,persistID,True,False,-1,CSRFToken) 
    if oRaizConSesion Is Nothing then
        ObtenerUsuCodCiaCod=arrResultado
        exit function
    else
        ReDim arrResultado(1)
        arrResultado(0)=oRaiz.Sesion.UsuCod
        arrResultado(1)=oRaiz.Sesion.CiaCod
    end if

    set oRaiz=Nothing
    set oRaizConSesion=Nothing

    ObtenerUsuCodCiaCod=arrResultado

end function

''' <summary>
''' Hace la sustitucion del nombre de la pagina para insertar un nuevo registro, previo a la transicion a esa pagina (FSAL)
''' </summary>
''' <remarks>Llamada desde: fsal_2.asp, fsal_7.asp; Tiempo máximo: </remarks>
Function ObtenerPaginaOrigen (pagina ,TipoFecha)
    dim sPaginaOrigen
    if TipoFecha=1 or TipoFecha=5 then
        if InStr(LCase(pagina), LCase("inicio.asp"))>0 then
            sPaginaOrigen=replace(pagina,"inicio.asp","inicio.asp")
        elseif InStr(LCase(pagina), LCase("actividadesCia.asp"))>0 then
            sPaginaOrigen=replace(pagina,"actividadesCia.asp","compania.asp")
        elseif InStr(LCase(pagina), LCase("actividades.asp"))>0 then
            sPaginaOrigen=replace(pagina,"actividades.asp","compania.asp")
        elseif InStr(LCase(pagina), LCase("actividadesciaserver.asp"))>0 then
            sPaginaOrigen=replace(pagina,"actividadesciaserver.asp","compania.asp")
        elseif InStr(LCase(pagina), LCase("usuario.asp"))>0 then
            sPaginaOrigen=replace(pagina,"usuario.asp","actividadesCia.asp")
        elseif InStr(LCase(pagina), LCase("resumen.asp"))>0 then
            sPaginaOrigen=replace(pagina,"resumen.asp","usuario.asp")
        elseif InStr(LCase(pagina), LCase("registrarproveedor.asp"))>0 then
            sPaginaOrigen=replace(pagina,"registrarproveedor.asp","resumen.asp")
        elseif InStr(LCase(pagina), LCase("imprimirOferta.asp"))>0 then
            sPaginaOrigen=replace(pagina,"imprimirOferta.asp","cumpoferta.asp")
        elseif InStr(LCase(pagina), LCase("downloadofertaexcel.asp"))>0 then
            sPaginaOrigen=replace(pagina,"downloadofertaexcel.asp","cumpoferta.asp")
        elseif InStr(LCase(pagina), LCase("guardaroferta.asp"))>0 then
            sPaginaOrigen=replace(pagina,"guardaroferta.asp","cumpoferta.asp")
        elseif InStr(LCase(pagina), LCase("excelserver.asp"))>0 then
            sPaginaOrigen=replace(pagina,"excelserver.asp","excel.asp")
        else
            sPaginaOrigen=""
        end if
    end if 
    ObtenerPaginaOrigen =sPaginaOrigen

end Function

''' <summary>
''' Llamada generica a WebServices
''' </summary>
''' <remarks>Llamada desde: fsal_2.asp, fsal_7.asp, fsal_8.asp; Tiempo máximo: </remarks>
Sub CallWebServiceASP(sWebServiceURL,sMethod,sParams)
    Dim xmlhttp

    Set xmlhttp = server.Createobject("Microsoft.XMLHTTP")

    xmlhttp.Open "POST",sWebServiceURL & "/" & sMethod,true
    xmlhttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
    xmlhttp.send sParams

End Sub

%>

