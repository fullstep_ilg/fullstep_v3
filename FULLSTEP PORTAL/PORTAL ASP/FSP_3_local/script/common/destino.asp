﻿<%@ Language=VBScript %>
<!--#include file="acceso.asp"-->
<!--#include file="idioma.asp"-->

<%
''' <summary>
''' Mostrar la descripción de un destino
''' </summary>
''' <remarks>Llamada desde: solicitudesoferta\js\auxiliares.asp; Tiempo máximo: 0,2</remarks>

	Idioma = Request.Cookies("USU_IDIOMA")
	cod=Request("cod")
	

	set oRaiz=validarUsuario(Idioma,true,false,0)
    

	CiaComp = oRaiz.Sesion.CiaComp

	Den=devolverTextos(idioma,4)
	
	set oDestinos = oraiz.Generar_CDestinos
	
	set ador = oDestinos.CargarDatosDestino(Idioma,Ciacomp, cod)

%>

<HTML>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">

<title><%=title%></title>

<SCRIPT>

function Cerrar()
{
	window.close()
}

</SCRIPT>

</HEAD>


<BODY topmargin="10" leftmargin="0">


<TABLE class="normal" align="center" WIDTH="98%" border="0" cellspacing="1" cellpadding="1">
	<TR>
		<TD width="5%" class="cabecera"><%=den(1)%></td>
		<TD class="filaPar" ><%=HTMLEncode(ador("COD").value)%></td>
	</TR>
	<TR>
		<TD width="35%" class="cabecera"><%=den(2)%></td>
		<TD class="filaPar"><%=HTMLEncode(ador("DEN").value)%></TD>
	</TR>
	<TR>
		<TD width="5%" class="cabecera"><%=den(4)%></td>
		<TD class="filaPar"><%=HTMLEncode(ador("DIR").value)%></TD>
	</TR>
	<TR>
		<TD width="10%" class="cabecera"><%=den(5)%></td>
		<TD class="filaPar"><%=HTMLEncode(ador("CP").value)%></TD>
	</TR>
	<TR>
		<TD width="10%" class="cabecera"><%=den(6)%></td>
		<TD class="filaPar"><%=HTMLEncode(ador("POB").value)%></TD>
	</TR>
	<TR>
		<TD width="10%" class="cabecera"><%=den(7)%></td>
		<TD class="filaPar"><%=HTMLEncode(ador("PROVI").value)%></TD>
	</TR>
	<TR>
		<TD width="10%" class="cabecera"><%=den(8)%></td>
		<TD class="filaPar"><%=HTMLEncode(ador("PAI").value)%></TD>
	</TR>
	<TR>
		<TD width="10%" class="cabecera"><%=den(11)%></td>
		<TD class="filaPar"><%=HTMLEncode(ador("TFNO").value)%></TD>
	</TR>
    <TR>
		<TD width="10%" class="cabecera"><%=den(12)%></td>
		<TD class="filaPar"><%=HTMLEncode(ador("FAX").value)%></TD>
	</TR>
    <TR>
		<TD width="10%" class="cabecera"><%=den(13)%></td>
		<TD class="filaPar"><%=HTMLEncode(ador("EMAIL").value)%></TD>
	</TR>
	<TR>
		<TD height=40px align=center COLSPAN=2>
			<INPUT TYPE="button" class="button" ID=Cerrar LANGUAGE=javascript class="Aprov" value="<%=Den(9)%>" onclick="Cerrar()">
		</TD>
	</TR>
	
</TABLE>	
		
<% 
ador.close
set ador = nothing
%>
	

</BODY>
</HTML>

