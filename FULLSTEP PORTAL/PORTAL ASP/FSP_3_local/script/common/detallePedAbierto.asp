﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<%
    decimalfmt=request.Cookies ("USU_DECIMALFMT")
	thousandfmt=Request.Cookies ("USU_THOUSANFMT")
	precisionfmt=cint(Request.Cookies ("USU_PRECISIONFMT"))
	datefmt=Request.Cookies("USU_DATEFMT")
    i=0
	Idioma = Request("Idioma")
	Idioma = trim(Idioma)
    IdOrden = Request("IdOrden")
    IdLinea = Request("LineaPedido")
    iTipoPedido = Request("TipoPedidoAbierto")

	set oRaiz=validarUsuario(Idioma,true,true,0)
    CiaComp=oRaiz.Sesion.CiaComp
	Set oLinea = oRaiz.Generar_CLinea()
    set oOrdenes=oRaiz.Generar_COrdenes()
    set adorOrden=oOrdenes.BuscarTodasOrdenes (CiaComp,Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"),0,null,null,null,null,null,null,null,null,null,null,null,null,null,null,1,,,,,,,,IdOrden,,,,,,true)
    oLinea.id=IdLinea   

	CiaComp = oRaiz.Sesion.CiaComp
	CiaCod = oRaiz.Sesion.CiaCod
	'Idiomas
	dim den
	den = devolverTextos(Idioma,135)	

    CiaDen = Request("CiaDen")

    set rs=oLinea.PedidoAbiertoDevolverPedidos(CiaComp)

%>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><%=title%></title>
    <link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp"/>
    <script src="../common/formatos.js"></script>
</head>
<body>
    <p><strong><%= den(1)%></strong>: <span><%=adorOrden("ANYO").value & "/" & adorOrden("NUMPEDIDO").value & "/" & adorOrden("NUMORDEN").value %></span></p>
    <p><strong><%= den(2)%></strong></p>
    
    <table align="center" WIDTH="98%" border="0" cellspacing="1" cellpadding="1">
        <tr class="cabecera">
            <th><%= den(3)%></th>
            <th><%= den(4)%></th>
            <th><%= den(5)%></th>
            <th><%= den(6)%></th>
            <th><%= den(7)%></th>
            <th><%= den(8)%></th>
            <th><%= den(9)%></th>
            <th>
                <%  'Cant.ped o Importe ped
                    if iTipoPedido=3 then
                        Response.write(den(10))
                    else
                        Response.write(den(12))
                    end if
                 %>
            </th>
            <th>
                <%  'U.P o Moneda
                    if iTipoPedido=3 then
                        Response.write(den(11))
                    else
                        Response.write(den(13))
                    end if
                 %>
            </th>
        </tr>
    

<%  while not rs.eof 
        clase=iif(i mod 2=0,"filaPar","filaImpar")
%>
        <tr  >
            <td class="<%=clase %>">
                <%=rs("ANYO").value %>
            </td>
            <td class="<%=clase %>">
                <%=rs("PEDIDO").value %>
            </td>
            <td class="<%=clase %>">
                <%=rs("ORDEN").value %>
            </td>
            <td class="<%=clase %>">
                <%=HTMLEncode(rs("NOM").value) & " " & HTMLEncode(rs("APE").value) %>
            </td>
            <td class="<%=clase %>">
                <%=Response.Write (visualizacionFecha(rs("FECHA").value,datefmt)) %>
            </td>
            <td class="<%=clase %>">
            <%
                select case rs("EST").value 
                    case 2 
                        if rs("ACEP_PROVE").value=0 then
		                   Response.write(den(20))'Pendiente de recepcionar
		                else
		                   Response.write(den(21))'Pendiente de aceptar
		                end if
                    case 3
                       Response.write(den(15))'Orden aceptada
                    case 4
                        Response.write(den(16))'En camino
                    case 5
                        Response.write(den(17))'Recibido parcialmente
                    case 6
                        Response.write(den(18))'Cerrado
                    case 21
                        Response.write(den(19))'Orden rechazada
                end select
             %>
            </td>
            <td class="<%=clase %>">
                <%=formatIntString(rs("NUMLIN").value,3) %>
            </td>
            <td class="<%=clase %>" align="right">
                <%  'Cant.ped o Importe ped
                    if iTipoPedido=3 then
                        Response.Write visualizacionNumero(rs("CANT_PED").value,decimalfmt,thousandfmt,precisionfmt)
                    else
                        Response.Write visualizacionNumero(rs("IMPORTE_PED").value*rs("CAMBIO").value,decimalfmt,thousandfmt,precisionfmt)
                    end if
                 
                 %>
            </td>
            <td class="<%=clase %>">
                <%  'U.P o Moneda
                    if iTipoPedido=3 then
                        Response.write(rs("UP").value)
                    else
                        Response.write(rs("MON").value)
                    end if
                 %>
            </td>
        </tr>
<%  
      i=i+1
      rs.movenext
    wend
 %>
    </table>
    <br />
    <center><input type="button" class="button Aprov" id="Cerrar" value="<%=Den(14)%>" onclick="javascript:window.close()"/></center>
</body>
</html>
