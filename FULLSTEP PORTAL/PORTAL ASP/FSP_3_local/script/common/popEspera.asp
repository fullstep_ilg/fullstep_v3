﻿<!--A incluir en aquellas páginas en las que se desea mostrar un popup interno mediante la funcion
showMensajeEspera('cadena texto con mensaje a mostrar')-->    
<style>
    #popupEspera
        {
            display: none;
            width: 20%;
            height: 10%;
            top: 30%;
            left: 40%;
            position: absolute;
            z-index: 1002;
            background-color: #FFF;
            text-align: center;
        }
        #popupFondo
        {
            display: none;
            position: absolute;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: #444;
            z-index:1001;
            -moz-opacity: 0.8;
            opacity:.80;
            filter: alpha(opacity=60);
        }
</style>
<script>
    function showMensajeEspera(msg) {
        //<summary>Muestra un popup mientras se realiza alguna acción</summary>
        //<parameter>Mensaje a mostrar en el popup</parameter>
        $('#popupMsg').html(msg);
        $('#popupEspera').show();
        $('#popupFondo').show();
        return false;
    }

    function hideMensajeEspera() {
        //<summary>Oculta el popup de espera</summary>
        $('#popupEspera').hide();
        $('#popupFondo').hide();
        return false;
    }
</script>
<div id="popupEspera" style="display: none">
    <h2 class="fgrande">
        
            <span id="popupMsg">dMensaje a mostrar ...</span>
            <br />
            <br />   
            <img name="imgReloj" id="imgReloj" src="../images/cursor-espera_grande.gif" width="31" height="28" />
        
    </h2>
</div>
<div id="popupFondo"  style='display: none;'>
</div>
