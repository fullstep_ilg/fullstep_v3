﻿<%@ Language=VBScript%>
<!--#include file="common/idioma.asp"-->
<!--#include file="common/formatos.asp"-->
<!--#include file="common/acceso.asp"-->
<!--#include file="common/fsal_1.asp"-->
<% Response.Expires=0 %>

<HTML>

<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script src="common/formatos.js"></script>
<script language="JavaScript" src="common/ajax.js"></script>
<!--#include file="common/fsal_3.asp"-->
<script language="JavaScript" type="text/JavaScript">
    function init() {
        if (<%=Application("FSAL")%> == 1){
            Ajax_FSALActualizar3();
        }
    }
</script>
<%
    ''' <summary>
    ''' Los datos proporcionados para logearte son incorrectos o te caduco la sesion
    ''' </summary>
    ''' <remarks>Llamada desde: common\acceso.asp       login\login.asp; Tiempo máximo: 0,2</remarks>    
	Idioma = Request("Idioma")
	Idioma = trim(Idioma)
	NomCiaComp=NoHtmlEnParametroExpuesto(Request("NomCiaComp"))
    ErrorSesion= Request("ErrorSesion")
    Bloqueo=cint(request("Bloqueo"))
	imagen=""
	Dim Den
	den=devolverTextos(Idioma,3)	
    select case ErrorSesion
        case 0
            Titulo = den(1)
            select case Bloqueo
                case -1 'USUARIO BLOQUEADO
                    Texto1 = den(13)
	                Texto2 = den(14)
                case 0 '
                    Texto1 = den(2)
	                Texto2 = den(3)
	                Texto3 = den(4)
	                Texto4 = den(5)
	                Texto5 = den(6)
                case else 'USUARIO HA REALIZADO INTENTO FALLIDO DE ACCESO A PLATAFORMA
                    Texto1 = den(2)
	                Texto2 = replace(den(15),"###",Bloqueo)
	                Texto3 = den(4)
	                Texto5 = den(6)
            end select
        case 1 'HAY SESION ACTIVA
            aviso=den(10)
            aviso2=den(11)
            Titulo = den(1) & den(7)
            Texto1 = den(8)
            imagen="../../custom/" & application("NOMPORTAL") & "/images/" & Idioma & "_" & getNavegador() & "_history.png"
            '''instrucciones
            instrucciones=den(17)
            select case getNavegador
                case "MOZ"
                    instrucciones=den(18)
                case "CHR"
                    instrucciones=den(19)
                Case "OPR"
                    instrucciones=den(19)
                case "SAF"
                    instrucciones=den(19)
            end select
        case else
            Titulo = den(9)
    end select%>

</HEAD>
<Title><%=Titulo%></title>
<script>

function mueveFoco()
{
var p
p = window.opener
if(p.document.forms["frmLogin"])
p.document.forms["frmLogin"].txtCia.focus()

}
function cerrar()
{
window.close()
}

</script>
<BODY leftmargin = 0 topMargin=0 onunload ="mueveFoco()" onload="init()">
<H1>
<%=Titulo%>
</H1>
<P>
<BR>

<%if NomCiaComp="" then%>
	<%=Texto1%>
    <%if ErrorSesion = 0 then%>
	    <BR><BR>    
	    <%=Texto2%>
	    <BR><BR>
	    <%=Texto3%>
    <%end if%>
<%else%>
    <%if ErrorSesion = 0 then%>
	    <%=Texto4%> : <%=NomCiaComp%>
    <%else%>
        <%=Texto1%>
    <%end if%>
<%end if%>

	<BR><BR>
</P>
    <% if imagen<>"" then %>
         <div style="width:100%" class="textos">
            <p><strong><%=den(10)%></strong></p>
            <p><%=den(11)%></p>
        
            <p><%=den(12)%></p>
                <p><strong><%=den(16)& " " &  getNavegadorNombre()%></strong></p>
                <p><%=instrucciones %></p>
                <center><img alt="<%=den(10)%>" src="images/<%=imagen %>" width="70%"/></center>
        
        </div>
    <%end if %>
	<center><a href ="javascript:void(null)" onclick ="cerrar()"><%=texto5%></a></center>
    


</BODY>
<!--#include file="common/fsal_2.asp"-->
</html>
