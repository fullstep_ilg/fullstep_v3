﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->


<%
''' <summary>
''' Mostrar la pantalla de pedidos para hacer su seguimiento
''' </summary>
''' <remarks>Llamada desde: common\menu.asp  common\introformatos.asp ; Tiempo máximo: 0,2</remarks>

	Idioma = Request("Idioma")
	Idioma = trim(Idioma)
    PedidosAbiertos=Request("PedidosAbiertos")

	AccesoExterno=cbool(Application("ACCESO_SERVIDOR_EXTERNO"))    
    if AccesoExterno then		
        set oRaiz=validarUsuarioAccesoExterno(Idioma,true,true,0,request("txtCIA"),request("txtUSU"),request("CodigoSesion"))
    else
	    set oRaiz=validarUsuario(Idioma,true,true,0)
	end if			
		
	DelProveedor = clng(oRaiz.Sesion.CiaId)
	set oCias = oraiz.DevolverCompaniasConPedidos (Idioma,DelProveedor,cint(Application("PORTAL")),clng(oRaiz.Sesion.CiaComp),Application("PYME"),oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"))
		
	set paramgen = oRaiz.Sesion.Parametros

	dim den

	if paramGen.unaCompradora and not oCias is nothing then
		set oCia = oCias.item(1)
		ciaComp=ocia.id
		ciaCod = oCia.cod
		ciaDen = ocia.den
		set ocia = nothing
		set oCias = nothing
		set paramgen = nothing
		set oRaiz = nothing
        
		Response.Redirect Application ("RUTASEGURA") & "script/seguimpedidos/seguimiento.asp?Idioma=" & Idioma & "&CiaComp=" & Server.URLEncode(ciaComp) & "&CiaCod=" & Server.URLEncode(ciaCod) & "&CiaDen=" & Server.URLEncode(ciaDen) & "&PedidosAbiertos=" & PedidosAbiertos
		Response.End 
	end if
	set paramgen = nothing

den = devolverTextos(Idioma,46)	
%>
<html>

<head>
<% 
    
%>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/menu.asp"></script>
<script language="JavaScript" type="text/JavaScript">
/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
    document.getElementById('tablemenu').style.display = 'block';
}
</script>
</head>
<body bgcolor="#ffffff" topMargin=0 leftmargin=0 onload="init()">
<SCRIPT>dibujaMenu(4)</script>

<h1><% = Den (1)%></h1>

<%if oCias is nothing then%>
<p class=Error><%=den(7)%></p>

<%else%>
	<h3><% = Den(2) %></h3>
	<h3><% = Den (3) %></h3>

	<div align="center">
	<table border="0" width="70%" cellspacing="2" cellpadding="1">
	  <tr>
	    <th width="40%" class="cabecera"><% = Den (4) %></th>
	    <th width="20%" class="cabecera"><% = Den (5) %></th>
	  </tr>
	  
	<%
		mclass="filaImpar"
		if not oCias is nothing then              
			for each oCia in oCias
				
				if oCia.NumOrdenActivas > 0 then%>
				
				<tr>  
					<td class=<%=mclass%>>
						<A ID=option0 LANGUAGE=javascript onmousemove="window.status='<%=oCia.Den%>'" onmouseout="window.status=''" href="<%=application("RUTASEGURA")%>script/seguimpedidos/seguimiento.asp?Idioma=<%=Idioma%>&CiaComp=<%=Server.URLEncode(ocia.id)%>&CiaCod=<%=Server.URLEncode(ocia.Cod)%>&CiaDen=<%=Server.URLEncode(ocia.den)%>PedidosAbiertos=<%=PedidosAbiertos%>">
						<%=oCia.den%>
						</A>
					</td>
					<td class=<%=mclass%> align="right">
						<%=ocia.NumOrdenActivas - ocia.NumOrdenNuevas%>
					</td>
				</tr>
					
				<%
				if mclass="filaImpar" then
					mclass="filaPar"
				else
					mclass="filaImpar" 
				end if
				end if
			next 
		end if

    
	set oraiz=nothing
		
	  %>
	</table>
	</div>
<%end if%>

</body>
</html>


