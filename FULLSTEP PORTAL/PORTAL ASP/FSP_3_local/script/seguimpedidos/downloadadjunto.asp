﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->

<%
''' <summary>
''' Descarga un adjunto
''' </summary>
''' <remarks>Llamada desde: seguimpedidos\adjuntoslinea.asp     seguimpedidos\adjuntosorden.asp ; Tiempo máximo: 0,2</remarks>

Idioma=Request.Cookies("USU_IDIOMA")

set oRaiz=validarUsuario(Idioma,true,false,0)
ID=request("Adjun")
if ID="null" then
	ID = null
end if

CiaComp=oRaiz.Sesion.CiaComp
CodProve = oRaiz.Sesion.CiaCodGs

nombre=request("nombre")
dataSize = clng(request("datasize"))

tipo=request("tipo")

set oAdjunto=oraiz.generar_CAdjunto
oAdjunto.AdjunIDPortal = null
oAdjunto.AdjunID=ID
oAdjunto.datasize = datasize

Orden = request("Orden")
Linea = request("Linea")

if isempty(Linea) then
	sP = cstr(Orden)
else
	sP = cstr(Orden) & "_" & cstr(Linea)
end if

sPath = Application ("CARPETAUPLOADS") & "\" & codprove & "\" & sP & "\"

set oError = oAdjunto.escribirADisco(CiaComp,sPath, nombre,tipo)


%>
<title><%=title%></title>
<script>
window.open("<%=application("RUTASEGURA")%>script/common/download.asp?path=<%=server.urlEncode(codprove & "/" & sP)%>&nombre=<%=server.urlencode(oERror.arg1)%>&datasize=<%=server.urlencode(datasize)%>","_blank","top=100,left=150,width=630,height=300,location=no,menubar=yes,resizable=yes,scrollbars=no,toolbar=no")
</script>

<%
Server.ScriptTimeout=OldTimeout


set oadjunto = nothing
set oraiz = nothing 

Response.End

	

%>