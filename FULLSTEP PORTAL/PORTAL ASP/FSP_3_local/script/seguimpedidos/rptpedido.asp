﻿
<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->

<%
''' <summary>
''' Imprime un pedido
''' </summary>
''' <remarks>Llamada desde: seguimpedidos\seguimclt.asp     seguimpedidos\seguimclt41.asp ; Tiempo máximo: 0,2</remarks>

	Idioma = Request("Idioma")
	Idioma = trim(Idioma)

	set oRaiz=validarUsuario(Idioma,true,true,0)
    
    set ParamGen = oRaiz.Sesion.Parametros
	
	CiaComp = oRaiz.Sesion.CiaComp
	CiaCod = oRaiz.Sesion.CiaCod
	
	'Idiomas
	dim den
	den = devolverTextos(Idioma,85)	
	
	'obtiene los formatos del usuario
	decimalfmt=request.Cookies ("USU_DECIMALFMT")
	thousanfmt=Request.Cookies ("USU_THOUSANFMT")
	precisionfmt=cint(Request.Cookies ("USU_PRECISIONFMT"))
	datefmt=Request.Cookies("USU_DATEFMT") 
	
	anyo=request("anyo")
	pedido=request("pedido")
	orden=request("orden")
	Referencia=request("Referencia")
    CiaDen = Request("CiaDen")
    PedidosAbiertos = Request("PedidosAbiertos")
	
	set oOrdenes=oRaiz.Generar_cOrdenes()
	set oDestinos=oRaiz.Generar_cDestinos()	
	
	Prove = clng(oRaiz.Sesion.CiaId)
	set oProve = oraiz.DevolverProveedorEnCiaComp(Idioma,Prove,CiaComp)
	
	CiaProv=oRaiz.Sesion.CiaCodGs

	chkEst0=clng(null2zero(vacio2null(request("chkEst0"))))
	chkEst1=clng(null2zero(vacio2null(request("chkEst1"))))
	chkEst2=clng(null2zero(vacio2null(request("chkEst2"))))
	chkEst3=clng(null2zero(vacio2null(request("chkEst3"))))
	chkEst4=clng(null2zero(vacio2null(request("chkEst4"))))
	chkEst5=clng(null2zero(vacio2null(request("chkEst5"))))
    if PedidosAbiertos=0 then
	    est = chkEst1 or chkEst2 or chkEst3 or chkEst4 or chkEst5 or chkEst0
	    chkEst6 = Request("chkEst6")
	    If isempty(chkEst6) then
		    chkEst6 = 0
	    else
		    chkEst6 = 1
	    end if
    'Calculo el parametro recep que tiene que ser 1 cuando pido las penditentes de recepcionar
        if chkEst0=0 then
		    ipend=0
	    else
		    ipend=1
	    end if
	
	    'Calculo el parametro recep que tiene que ser 1 cuando pido las penditentes de recepcionar
        if chkEst1=0 then
		    if chkEst2<>0 then
			    irecep=1
		    else
			    irecep=0
		    end if
	    else
		    irecep=1
	    end if		
	    if est=0 and chkEst6=0 then
		    est=380
	    end if
    end if
	dim paramGen
	set paramGen = oraiz.DevolverParamGSPedidos(CiaComp)
	pedDirectoEnvioEmail = paramGen.item(1)
	pedDirFac = paramGen.item(2)
    
	if pedDirectoEnvioEmail then
		set ador = oOrdenes.BuscarTodasOrdenes(CiaComp, Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"), Est, 0, null, null, null,null,null, null,null,CiaProv, null,  null, anyo,pedido,orden,null, chkEst6,txtRef,icompleta,iEmpresa,null,null,null,null,null,ipend,irecep,null,null,PedidosAbiertos)
	else
		set ador = oOrdenes.BuscarTodasOrdenes(CiaComp, Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"), Est, 0, null, null, null,null,null, null,null,CiaProv, null,  null, anyo,pedido,orden,null, chkEst6,txtRef,icompleta,iEmpresa,null,null,null,null,1,ipend,irecep,null,null,PedidosAbiertos)
	end if
	set oOrden = oRaiz.Generar_COrden()
	oOrden.id = clng(ador("ID").value)
	set adorAtr = oOrden.DevolverAtributos(CiaComp)

    iModoImp=oOrdenes.DevolverModoImputacion(CiaComp) 
    bAccesoFSSM=oRaiz.Sesion.Parametros.AccesoFSSM
    if bAccesoFSSM then
        set rsPPresup=oRaiz.DevolverPartidaPresupuestaria(CiaComp,Idioma)
        if not rsPPresup is nothing then
            if rsPPresup.recordcount>0 then
                sCodPPresupuestaria=rsPPresup("PRES5")
                sDenPPresupuestaria=rsPPresup("DEN")
            end if
        end if    
    end if

	dim sAdjuntosLinea
	dim sAdjuntosOrden
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><%=title%></title>

<script>
function Imprimir()
{	
	document.getElementById("cmdImprimir").style.visibility = "hidden"
	window.print()
	document.getElementById("cmdImprimir").style.visibility = "visible"
}

function verAdjuntosOrden(id)
{
    window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/adjuntosorden.asp?Orden=" + id, "_blank", "top=5,left=5,width=600, height=250,directories=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes")
}

function verAdjuntosLinea(orden,id,obs)
{
    window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/adjuntoslinea.asp?Orden=" + orden + "&Linea=" + id + "&Obs=" + obs, "_blank", "top=5,left=5,width=600, height=250,directories=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes")
}

</script>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/formatos.js"></script>
</head>

<script>
function init()
{
if (document.images["imgCompradora"].complete)
	document.images["imgCompradora"].style.visibility="visible"
else
	document.images["imgCompradora"].outerText=""
}
</script>
<body>
<h1><%=den(15)%>&nbsp;&nbsp;&nbsp;&nbsp;<span class="negro"><%=HTMLEncode(CiaDen)%></span></h1>

		<table width="100%">
			<tr>
				<td>
					<table width="100%">
						<tr>
							<td rowspan="2" width="33%" class="cabecera"><%=den(41)%></td>
							<td class="filaPar" colspan="3">
								<%=ador.fields("NIFEMPRESA").value%>
							</td>
						</tr>
						<tr>
							<td class="filaPar" colspan="3">
								<%=HTMLEncode(ador.fields("DENEMPRESA").value)%>
							</td>
						</tr>
						
						<% if pedDirFac then %>
						<tr>
							<td class="cabecera"><%=den(59)%></td>
							<td class="filaPar" colspan="3">
								<%=HTMLEncode(ador.fields("DIRFAC").value)%>
							</td>							
						</tr>
						<%end if%>
						
						
						
					</table>
				</td>
			</tr>
		</table>

<%
	set oPersonas = oRaiz.generar_CPersonas()
	
	if isnull(ador.Fields("PER").Value) then
		set oPersona =NOTHING
	else
		Set adoRSPer = oPersonas.DevolverDatosPersona(CiaComp, ador.Fields("PER").Value)
		set oPersona = oPersonas.Add( adoRSPer.Fields("COD").Value, adoRSPer.Fields("NOM").Value, adoRSPer.Fields("APE").Value, _
		       NullToStr(adoRSPer.Fields("CAR").Value), adoRSPer.Fields("DEP").Value, NullToStr(adoRSPer.Fields("EMAIL").Value), _
		       NullToStr(adoRSPer.Fields("TFNO").Value), adoRSPer.Fields("TFNO2").Value, adoRSPer.Fields("FAX").Value, _
		       adoRSPer.Fields("UON1").Value, adoRSPer.Fields("UON2").Value, adoRSPer.Fields("UON3").Value, _
		       adoRSPer.Fields("DENU1").Value, adoRSPer.Fields("DENU2").Value, adoRSPer.Fields("DENU3").Value, _
		       adoRSPer.Fields("EQP").Value, adoRSPer.Fields("DENDEP").Value)
		adoRSPer.Close
	end if
	'Receptor
	if isnull(ador.Fields("RECEPTOR").Value) then
		set oPersonaR =NOTHING
	else
		bLeer=false
		if  not isnull(ador.Fields("PER").Value) then
			if ador.Fields("RECEPTOR").Value = ador.Fields("PER").Value then
				set oPersonaR =oPersona
			else
				bLeer=true
			end if
		else
			bLeer=true
		end if
		if bLeer then
			Set adoRSPer = oPersonas.DevolverDatosPersona(CiaComp, ador.Fields("RECEPTOR").Value)
			set oPersonaR = oPersonas.Add( adoRSPer.Fields("COD").Value, adoRSPer.Fields("NOM").Value, adoRSPer.Fields("APE").Value, _
			   NullToStr(adoRSPer.Fields("CAR").Value), adoRSPer.Fields("DEP").Value, NullToStr(adoRSPer.Fields("EMAIL").Value), _
			   NullToStr(adoRSPer.Fields("TFNO").Value), adoRSPer.Fields("TFNO2").Value, adoRSPer.Fields("FAX").Value, _
			   adoRSPer.Fields("UON1").Value, adoRSPer.Fields("UON2").Value, adoRSPer.Fields("UON3").Value, _
			   adoRSPer.Fields("DENU1").Value, adoRSPer.Fields("DENU2").Value, adoRSPer.Fields("DENU3").Value, _
			   adoRSPer.Fields("EQP").Value, adoRSPer.Fields("DENDEP").Value)
			adoRSPer.Close
		end if
	end if
			
	'Carga los adjuntos
	if not isnull(ador("ADJUNTOS").value) then
		sAdjuntosOrden=""
		set oOrden = oRaiz.Generar_COrden()
		oOrden.ID=ador("ID").value
		set oAdjuntosOrden = oOrden.cargarAdjuntos(CiaComp,false)
		for each oAdjunto in oAdjuntosOrden
			sAdjuntosOrden=sAdjuntosOrden & oAdjunto.Nombre & " ; "
		next
		sAdjuntosOrden=mid(sAdjuntosOrden,1,len(sAdjuntosOrden)-3)
	end if

    'Costes y descuentos
    if ador("HAYCOSTES")>0 or ador("HAYDESCUENTOS")>0 then
        set adorCostesDesc=oOrden.DevolverCostesDescuentosCab(CiaComp)        
    end if
    dImporteTotal=ador("IMPORTE")*ador("CAMBIO")
    set adorImp = oOrden.Devolverlineas(CiaComp,Idioma)
    if not adorImp.eof then
	    while not adorImp.eof
            dImporteBrutoLin=adorImp("IMPORTE").value
            if not isnull(adorImp("COSTES")) then dCostesLin=adorImp("COSTES")*ador("CAMBIO")
            if not isnull(adorImp("DESCUENTOS")) then dDescuentosLin=adorImp("DESCUENTOS")*ador("CAMBIO")
            dImporteBruto=dImporteBruto+dImporteBrutoLin+dCostesLin-dDescuentosLin
            adorImp.movenext
        wend
    end if
    set oLineasDesgloseImpuesto = oRaiz.Generar_CLineasDesgImpuesto
    oLineasDesgloseImpuesto.cargarLineasDesgloseImpuestoOrden oOrden.id,CiaComp,Idioma
%>
<table width="100%" style="border:solid 1 black;page-break-after:always;">
<tr>
	<td>
		<table width="100%">		
			<tr>
				<td width="30%" class="cabecera"><%=den(1)%></td>
				<td width="20%" class="filaPar">
					<%=ador("anyo").value%>/<%=ador("numpedido").value%>/<%=ador("numorden").value%>
				</td>
				<td width="20%" class="cabecera"><%=den(51)%></td>
				<td width="30%" class="filaPar">
					<%=ador("NUMEXT").value%>
				</td>				
			</tr>
			
			<%if Referencia="" then%>
				<tr>
					<td width="30%" class="cabecera"><%=den(2)%></td>
					<td width="20%" class="filaPar">
						<%
						if not isnull(ador("FECHA").value) then
							Response.write VisualizacionFecha(ador("FECHA").value,datefmt)
						else
							Response.Write "&nbsp;"
						end if%>
					</td>
					<td width="20%" class="cabecera"><%=den(52)%></td>
					<td width="30%" class="filaPar" colspan="5">
						<%VB2HTML(ador.fields("PAG").value & " - " & ador.fields("PAGDEN").value)%>
					</td>
				</tr>
				<tr>													
					<td width="30%" class="cabecera"><%=den(32)%></td>
					<td width="20%" class="filaPar" colspan="3">
						<%
						select case ador("EST").value
							case 2:
								if ador("ACEP_PROVE").value=0 then
									Response.Write den(58)
								else
									Response.Write den(47)
								end if
							case 3:
								Response.Write den(36)
							case 4:
								Response.Write den(37)
							case 5:
								Response.Write den(38)
							case 6:
								Response.Write den(39)
							case 21:
								Response.Write den(40)
						end select%>
					</td>					
				</tr>
			<%else%>
				<tr>
					<td width="30%" class="cabecera"><%=Referencia%></td>
					<td width="20%" class="filaPar"><%=HTMLEncode(ador("REFERENCIA").value)%></td>
					<td width="20%" class="cabecera"><%=den(2)%></td>
					<td width="30%" class="filaPar">
						<%
						if not isnull(ador("FECHA").value) then
							Response.write VisualizacionFecha(ador("FECHA").value,datefmt)
						else
							Response.Write "&nbsp;"
						end if%>
					</td>
				</tr>					
				<tr>
					<td width="30%" class="cabecera"><%=den(52)%></td>
					<td width="20%" class="filaPar">
						<%VB2HTML(ador.fields("PAG").value & " - " & ador.fields("PAGDEN").value)%>
					</td>											
				    <td width="20%" class="cabecera"><%=den(32)%></td>
				    <td class="filaPar">
					<%select case ador("EST").value
						case 2:
								if ador("ACEP_PROVE").value=0 then
									Response.Write den(58)
								else
									Response.Write den(47)
								end if
						case 3:
							Response.Write den(36)
						case 4:
							Response.Write den(37)
						case 5:
							Response.Write den(38)
						case 6:
							Response.Write den(39)
						case 21:
							Response.Write den(40)
					end select%>
				    </td>
			    </tr>			
			<%end if			
			if not isnull(ador("ADJUNTOS").value) then%>
			<tr>	
                <td class="cabecera" width="20%"><%=den(49)%><a href='javascript:void(null)' onclick='verAdjuntosOrden(<%=oOrden.id%>)'><img border = 0 src= '../images/clip.gif'></A></td>
				<td class="filaPar" colspan="3"><%=sAdjuntosOrden%></td>
			</tr>
			<%end if%>
			<%if not isnull(ador("OBS").value) then%>
			<tr>	
				<td class="cabecera" width="20%"><%=den(21)%></td>
				<td class="filaPar" colspan="3"><%=ador("OBS").value%></td>
			</tr>
			<%end if%>
			<%if not adorAtr is nothing then
				while not adorAtr.eof%>
			        <tr>
				        <td width="30%" class="cabecera"><%=adorAtr("DEN").value%></td>
			            <%if adorAtr("TIPO").value=1 then%> 
			                <td class="filaPar" colspan="3"><%=VB2HTML(adorAtr("VALOR_TEXT").value)%></td>
			            <%end if%>
			            <%if adorAtr("TIPO").value=2 then%> 
			                <td class="filaPar" colspan="3"><%=visualizacionNumero(adorAtr("VALOR_NUM").value,decimalfmt,thousanfmt,precisionfmt)%></td>
			            <%end if%>
			            <%if adorAtr("TIPO").value=3 then%>
			                <td class="filaPar" colspan="3"><%=visualizacionFecha(adorAtr("VALOR_FEC").value,datefmt)%></td>
			            <%end if%>
			            <%if adorAtr("TIPO").value=4 then%>
			              <%if isnull(adorAtr("VALOR_BOOL").value) then%>
			                  <td class="filaPar" colspan="3">&nbsp;</td>
			              <%elseif adorAtr("VALOR_BOOL").value=0 then%>
			                <td class="filaPar" colspan="3"><%=den(53)%></td>
			              <%else%>
			                <td class="filaPar" colspan="3"><%=den(54)%></td>
			              <%end if
			            end if                    
			        adorAtr.movenext
			    wend
			end if%>

            <% 
                if not isnull(tipoPedidoAbierto) then %>
                <tr><td class="cabecera" colspan="4"><%=den(81) %></td></tr>
                <tr>
                    <td class="cabecera"><%=den(82) %></td>                
				    <td class="filaPar" style="text-align:right;">
					    <%=ador("PED_ABIERTO_FECINI").value %>&nbsp;					
				    </td>
                    <td class="filaPar" colspan="2"/>	
                </tr>
                <tr>
                    <td class="cabecera"><%=den(83) %></td>                
				    <td class="filaPar" style="text-align:right;">
					    <%=ador("PED_ABIERTO_FECFIN").value %>&nbsp;				
				    </td>
                    <td class="filaPar" colspan="2"/>	
                </tr>
            <% end if %>
            <!--IMPORTES-->
            <tr><td class="cabecera" colspan="4"><%=Den(61)%></td></tr>
            <tr>
                <td class="cabecera"><%=Den(62)%></td>
                <td class="filaPar" style="text-align:right;">
					<%=replace(FormatNumber(dImporteBruto,2),",",".")%>&nbsp;<%=ador("MON").value%>
                    <%MonedaPedido=ador("MON").value%>
					<%CambioPedido=ador("CAMBIO").value%>					
				</td>
                <td class="filaPar" colspan="2"/>
            </tr>
            <!--Costes/descuentos-->
            <%if ador("HAYCOSTES")>0 or ador("HAYDESCUENTOS")>0 then
                if not adorCostesDesc is nothing then
                    adorCostesDesc.movefirst
                    while not adorCostesDesc.eof                    
                        sDescCD=VisualizacionCosteDesc(iif(isnull(adorCostesDesc("DEN")),"",adorCostesDesc("DEN")),adorCostesDesc("TIPO"),adorCostesDesc("OPERACION"),adorCostesDesc("VALOR"),ador("MON").value,CambioPedido)%>
                        <tr>
                            <td class="cabecera"><%=sDescCD%></td>
                            <td class="filaPar" style="text-align:right;">
                                <%=replace(FormatNumber((adorCostesDesc("IMPORTE")*CambioPedido),2),",",".")%>&nbsp;<%=ador("MON").value%>
                            </td>
                            <td class="filaPar" colspan="2"/>
                        </tr>
                        <%adorCostesDesc.movenext
                    wend
                end if
            end if%>      
            <!--Impuestos-->
            <%for each oLineaDesgloseImpuesto in oLineasDesgloseImpuesto%>
            <tr>
                <td class="cabecera">
                    <%=Den(73)%>&nbsp;<%=oLineaDesgloseImpuesto.tipoimpuesto%>&nbsp;<%=oLineaDesgloseImpuesto.TipoValor%>%&nbsp;
                    (<%=Den(74)%>:&nbsp;<%=replace(FormatNumber(oLineaDesgloseImpuesto.BaseImponible,2),",",".")%>&nbsp;<%=ador("MON").value%>)
                </td>
                <td class="filaPar" style="text-align:right;">
                    <%=replace(FormatNumber(oLineaDesgloseImpuesto.cuota,2),",",".")%>&nbsp;<%=ador("MON").value%>
                </td>
                <td class="filaPar" colspan="2"/>
            </tr>
            <% next%>
            <!--Importe total-->       
            <tr>
                <td class="cabecera"><%=Ucase(Den(63))%></td>                 
				<td class="filaPar" style="text-align:right;">
					<%=replace(FormatNumber(dImporteTotal,2),",",".")%>&nbsp;<%=ador("MON").value%>					
				</td>
                <td class="filaPar" colspan="2"/>	
            </tr>
			           

			<%if not oPersona is nothing then %>			
			<tr colspan="2"><td class="cabecera" colspan="4"><%=den(55)%></td></tr>
			<tr>
				<td class="cabecera" width="20%"><%=den(44)%></td>			
				<td width="30%" class="filaPar">		
					<%=HTMLEncode(oPersona.nombre)%>&nbsp;<%=HTMLEncode(oPersona.Apellidos)%>
				</td>
				<td width="20%" rowspan="3" class="cabecera" valign="top"><%=den(48)%></td>
				<td width="30%" class="filaPar"><%=oPersona.UON1DEN%></td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(45)%></td>
				<td class="filaPar"><%=oPersona.tfno%></td>
				<td class="filaPar">
					<%if not isnull(oPersona.uon2) then%>
						<%=oPersona.UON2DEN%>
					<%end if%>
				</td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(46)%></td>
				<td class="filaPar">
						<a href="mailto:<%=oPersona.email%>"><%=VB2HTML(oPersona.email)%></a>
				</td>
				<td class="filaPar">
					<%if not isnull(oPersona.uon3) then%>
						<%=oPersona.UON3DEN%>
					<%end if%>
				</td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(57)%></td>
				<td class="filaPar"><%=VB2HTML(oPersona.fax)%></td>			
				<td class="cabecera"><%=den(42)%></td>
				<td class="filaPar"><%=VB2HTML(oPersona.depden)%></td>
			 </tr>		
		<%end if%>
		<%if not oPersonaR is nothing then %>			
			<tr><td class="cabecera" colspan="4"><%=den(56)%></td></tr>
			<tr>
				<td class="cabecera" width="20%"><%=den(44)%></td>			
				<td width="30%" class="filaPar">		
					<%=oPersonaR.nombre%>&nbsp;<%=oPersonaR.Apellidos%>
				</td>
				<td width="20%" rowspan="3" class="cabecera" valign="top"><%=den(48)%></td>
				<td width="30%" class="filaPar"><%=VB2HTML(oPersonaR.UON1DEN)%></td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(45)%></td>
				<td class="filaPar"><%=oPersonaR.tfno%></td>
				<td class="filaPar">
					<%if not isnull(oPersonaR.uon2) then%>
						<%=oPersonaR.UON2DEN%>
					<%end if%>
				</td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(46)%></td>
				<td class="filaPar">
						<a href="mailto:<%=oPersonaR.email%>"><%=VB2HTML(oPersonaR.email)%></a>
				</td>
				<td class="filaPar">
					<%if not isnull(oPersonaR.uon3) then%>
						<%=oPersonaR.UON3DEN%>
					<%end if%>
				</td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(57)%></td>
				<td class="filaPar"><%=VB2HTML(oPersonaR.fax)%></td>			
				<td class="cabecera"><%=den(42)%></td>
				<td class="filaPar"><%=VB2HTML(oPersonaR.depden)%></td>
			 </tr>		
		<%end if%>
	</table>
	<br>
	<br>	
<%
	OrdenId=ador("id").value
    tipoPedAbierto=ador.fields("PED_ABIERTO_TIPO").value
    if isnull(tipoPedAbierto) then
        sCantidad=Den (11) 'Cantidad
        sImporte= Den (20) 'Importe
    else
        sCantidad=Den (77) 'Cantidad abierta
        sImporte= Den (79)  'Importe abierto 
    end if 

	ador.close
	set ador = nothing
	
		
	set oOrden = oRaiz.Generar_COrden()

	oOrden.id = clng(OrdenId)

	set ador = oOrden.Devolverlineas(CiaComp,Idioma, (bAccesoFSSM and iModoImp=1))

    'Ocultar columna fecha de entrega
    iNumLinPlanEnt=0
    iNumLinPlanEntPub=0
    bOcultarFecEntrega=false
    if not ador is nothing then
        if ador.recordcount>0 then
            while not ador.eof               
                if ador("HAYPLANES")>0 then
                    iNumLinPlanEnt=iNumLinPlanEnt+1
                    if ador("PUB_PLAN_ENTREGA")=1 then iNumLinPlanEntPub=iNumLinPlanEntPub+1
                end if
                ador.movenext
            wend
        end if

        if iNumLinPlanEnt>0  then
            if iNumLinPlanEntPub>0 then set adorPlanes=oOrden.DevolverPlanesEntrega(CiaComp)            
            if (iNumLinPlanEnt=ador.recordcount) then bOcultarFecEntrega=true                    
        end if

         ador.movefirst
    end if    

%>
	
	<table width="100%">
		<tr>
			<td class="cabecera"><%=den(75)%></td>
			<td class="cabecera"><%=den(4)%></td>
			<td class="cabecera"><%=den(5)%></td>
			<td class="cabecera"><%=den(6)%></td>
			<td class="cabecera"><%=den(7)%></td>
			<td class="cabecera"><%=den(8)%></td>
			<%if not bOcultarFecEntrega then %>
			    <td class="cabecera"><%=den(9)%></td>
                <td class="cabecera"><%=den(10)%></td>
            <%end if%>
            <% if tipoPedAbierto= 3 or isnull(tipoPedAbierto) then %>
                <td class="cabecera"><%=sCantidad%></td>
            <%end if %>
            <% if tipoPedAbierto=3 then %>
			    <td class="cabecera"><%=den(78)%></td>
            <% end if %>
            <% if tipoPedAbierto= 3 or isnull(tipoPedAbierto) then %>
			    <td class="cabecera"><%=den(12)%></td>
            <% end if %>
            <td class="cabecera"><%=den(65)%>&nbsp;(<%=MonedaPedido%>)</td>
            <td class="cabecera"><%=den(66)%>&nbsp;(<%=MonedaPedido%>)</td>
			<td class="cabecera"><%=sImporte%>&nbsp;(<%=MonedaPedido%>)</td>
            <% if tipoPedAbierto=2 then %>
                <td class="cabecera"><%=den(80)%>&nbsp;(<%=MonedaPedido%>)</td><!--Importe Pedido Abierto-->
            <% end if %>
			<td class="cabecera"><%=den(34)%></td>
		</tr>

<%	

	fila = "filaImpar"
    if not ador is nothing then
	    while not ador.eof
            numLineaPedidoTmp=ador("NUMLINEAPEDIDO").value
            if numLineaPedidoTmp < 10 then
                numLineaPedido = "00" & numLineaPedidoTmp
            elseif numLineaPedidoTmp >= 100 then
                numLineaPedido = numLineaPedidoTmp
            else
            numLineaPedido = "0" & numLineaPedidoTmp
            end if

            if not isnull(ador("CANTIDAD").value) then
                cantidad=visualizacionNumero(ador("CANTIDAD").value,decimalfmt,thousandfmt,precisionfmt)
            else
                cantidad="&nbsp"
            end if
            if not isnull(ador("CANTIDAD_PED_ABIERTO").value) then
                cantidadPedida=visualizacionNumero(ador("CANTIDAD_PED_ABIERTO").value,decimalfmt,thousandfmt,precisionfmt)
            else
                cantidadPedida="0"
            end if

            dImporteBrutoLin=visualizacionNumero(ador("IMPORTE").value,decimalfmt,thousandfmt,precisionfmt)

            if not isnull(ador("IMPORTE_PED_ABIERTO").value) then
                importePedidoAb=visualizacionNumero(ador("IMPORTE_PED_ABIERTO").value*CambioPedido,decimalfmt,thousandfmt,precisionfmt)
            else
                importePedidoAb="0"
            end if	
        %>		
		    <tr>
			    <td class="<%=fila%>">
				    <%=numLineaPedido%>
			    </td>
			    <td class="<%=fila%>">
				    <%=ador("codart").value%>
			    </td>
			    <td class="<%=fila%>">
				    <%=ador("art_ext").value%>
			    </td>
			    <td class="<%=fila%>">
				    <%=ador("denart").value%>
			    </td>
			    <td class="<%=fila%>">
				    <%=ador("DESTINO").value%>
				    
				    <%if oDestinos.item(ador("DESTINO").value) is nothing then
					    set adorDest = oDestinos.CargarDatosDestino(Idioma,Ciacomp, ador("DESTINO").value, ador("LINEAID").value)
					    oDestinos.Add adorDest("COD").Value, adorDest("DEN").Value, false, adorDest("DIR"), adorDest("POB"), adorDest("CP"), adorDest("PROVI"), adorDest("PAI")
					    adorDest.close
					    set adorDest = nothing
				    end if%>
				
			    </td>
			    <td class="<%=fila%>">
				    <%=ador("UNIDAD").value%>-<%=ador("UNIden").value%>
			    </td>
			    <%if not bOcultarFecEntrega then %>
                    <%if ador("HAYPLANES")>0 then%>                                           
                        <td class="<%=fila%>">&nbsp;</td>                                             
                        <td class="<%=fila%>">&nbsp;</td> 
                    <%else%>
			            <td class="<%=fila%>">
				            <%
				            if not isnull(ador("FECENTREGA").value) then                            
					            Response.write VisualizacionFecha(ador("FECENTREGA").value,datefmt)
				            else
					            Response.Write "&nbsp;"
				            end if%>
			            </td>
                        <td class="<%=fila%>">
				            <%if ador("ENTREGA_OBL").value=1 then
					            Response.Write den(18)
				            else
					            Response.Write den(19)
				            end if%>
			            </td>
                    <%end if%>                 
                <%end if%>
		        <% 
                    if tipoPedAbierto= 3 or isnull(tipoPedAbierto) then %>
			        <td class="<%=fila%> numero">
				        <%=cantidad%>
			        </td>
                <% end if %>
                <% if tipoPedAbierto=3 then %>
                    <td class="<%=fila%> numero">
				        <%=cantidadPedida%>
			        </td>
                <%end if %>

                <% if tipoPedAbierto= 3 or isnull(tipoPedAbierto) then %>
                    <%if ador("TIPORECEPCION").value=1 then %>
                        <td class=<%=fila%> align=right>&nbsp;</td>"	 
                    <%else %> 
			            <td class="<%=fila%> numero">
				            <%=visualizacionNumero(ador("PRECIOUNITARIO").value ,decimalfmt,thousanfmt,precisionfmt)%>
			            </td>
                    <%end if %>
                <% end if %>

			    <td class="<%=fila%> numero">
				    <%=visualizacionNumero(ador("COSTES").value * CambioPedido ,decimalfmt,thousanfmt,precisionfmt)%>
			    </td>
                <td class="<%=fila%> numero">
				    <%=visualizacionNumero(ador("DESCUENTOS").value * CambioPedido ,decimalfmt,thousanfmt,precisionfmt)%>
			    </td>
			    <td class="<%=fila%> numero">
				    <%=visualizacionNumero(ador("IMPORTE").value+(ador("COSTES").value-ador("DESCUENTOS").value) * CambioPedido ,decimalfmt,thousanfmt,precisionfmt)%>
			    </td>   
                <% if tipoPedAbierto=2 then %>
                    <td class="<%=fila%> numero">
				        <%=importePedidoAb%>
			        </td> 
                <% end if %>

			    <td class="<%=fila%> numero">
				    <%=visualizacionNumero(ador("CANT_REC").value,decimalfmt,thousanfmt,precisionfmt)%>
			    </td>
		    </tr>	
            
            <!--COSTES y DESCUENTOS-->
            <%if ador("HAYCOSTES")>0 or ador("HAYDESCUENTOS")>0 then
                set adorCDs = oOrden.DevolverCostesDescuentosLinea(ador("LINEAID").value,CiaComp)   
            
                if not adorCDs is nothing then%>
                    <tr>
                        <%if not bOcultarFecEntrega then %>
                            <td colspan="9"></td>
                        <%else%>
                            <td colspan="7"></td>
                        <%end if%>
                        <td class="cabecera" colspan="4"><%=Den(67)%></td>
                    </tr>
                    <%if ador("HAYCOSTES")>0 then%>
                        <tr>
                            <%if not bOcultarFecEntrega then %>
                                <td colspan="9"></td>
                            <%else%>
                                <td colspan="7"></td>
                            <%end if%>
                            <td class="cabecera" colspan="2"><%=Den(68)%></td>
                            <td class="cabecera" colspan="2"><%=Den(70)%>&nbsp;(<%=MonedaPedido%>)</td>                   
                        </tr>
                        <%adorCDs.movefirst
                        adorCDs.filter="TIPO=0"
                        while not adorCDs.eof
                            sDescCD=VisualizacionCosteDesc(iif(isnull(adorCDs("DEN")),"",adorCDs("DEN")),adorCDs("TIPO"),adorCDs("OPERACION"),adorCDs("VALOR"),MonedaPedido,CambioPedido)%>
                            <tr>
                                <%if not bOcultarFecEntrega then %>
                                    <td colspan="9"></td>
                                <%else%>
                                    <td colspan="7"></td>
                                <%end if%>
                                <td class=<%=fila%> colspan="2"><%=sDescCD%></td>
                                <td class=<%=fila%> colspan="2" style="text-align:right;"><%=visualizacionNumero((adorCDs("IMPORTE")*CambioPedido),decimalfmt,thousanfmt,precisionfmt)%></td>                            
                            </tr>
                            <%adorCDs.movenext
                        wend
                    end if
                    if ador("HAYDESCUENTOS")>0 then%>
                        <tr>
                            <%if not bOcultarFecEntrega then %>
                                <td colspan="9"></td>
                            <%else%>
                                <td colspan="7"></td>
                            <%end if%>
                            <td class="cabecera" colspan="2"><%=Den(69)%></td>
                            <td class="cabecera" colspan="2"><%=Den(71)%>&nbsp;(<%=MonedaPedido%>)</td>
                        </tr>
                        <%adorCDs.movefirst
                        adorCDs.filter="TIPO=1"
                        while not adorCDs.eof
                            sDescCD=VisualizacionCosteDesc(iif(isnull(adorCDs("DEN")),"",adorCDs("DEN")),adorCDs("TIPO"),adorCDs("OPERACION"),adorCDs("VALOR"),MonedaPedido,CambioPedido)%>
                            <tr>
                                <%if not bOcultarFecEntrega then %>
                                    <td colspan="9"></td>
                                <%else%>
                                    <td colspan="7"></td>
                                <%end if%>
                                <td class=<%=fila%> colspan="2"><%=sDescCD%></td>
                                <td class=<%=fila%> colspan="2" style="text-align:right;"><%=visualizacionNumero((adorCDs("IMPORTE")*CambioPedido),decimalfmt,thousanfmt,precisionfmt)%></td>                            
                            </tr>
                            <%adorCDs.movenext
                        wend
                    end if
                end if  
            end if%>
            <% if (bAccesoFSSM and iModoImp=1) or not isnull(ador("ANYO_PED_ABIERTO").value) then %>
                <tr>
                    <td>&nbsp;</TD>  
                    <% if bAccesoFSSM and iModoImp=1 then %>     
                        <%if not isnull(ador("UON4").value) then
                            centro_sm=ador("UON4").value    
                        elseif not isnull(ador("UON3").value) then
                            centro_sm=ador("UON3").value
                        elseif not isnull(ador("UON2").value) then
                            centro_sm=ador("UON2").value
                        else
                            centro_sm=ador("UON1").value
                        end if %>  
                        <td align='left' class=cabecera><%=den(76)%></td>
                        <td align='left' class=<%=fila%>><%=JSText(centro_sm)%><%=iif(isnull(ador("CCOSTE_DEN").value),""," - ")%><%=JSText(ador("CCOSTE_DEN").value)%></td>
                        <td align='left' class=cabecera><%=sDenPPresupuestaria%></td>
                        <td align='left' class=<%=fila%> colspan='2'><%=JSText(ador("PRES").value)%><%=iif(isnull(ador("PP_DEN").value),""," - ")%><%=JSText(ador("PP_DEN").value)%></td>
                    <%end if %>
                    <%if not isnull(ador("ANYO_PED_ABIERTO").value) then %>
                            <td align='left' class="cabecera">
                                <%=Den(84)%>
                            </td>
                            <td align='left' class="<%=fila%>" colspan='2'>
                                <%=ador("ANYO_PED_ABIERTO").value & "/" & ador("PEDIDO_PED_ABIERTO").value & "/" & ador("ORDEN_PED_ABIERTO").value %>
                            </td>
                    <%end if %>
                </tr>
            <%end if %>
            	
		    <%set adorAtrL = oOrden.DevolverAtributosLinea(ador("LINEAID").value,CiaComp)		
		    if not (adorAtrL is nothing) or  not isnull(ador("CODPROCE").value) or (not isnull(ador("OBS").value) AND ador("OBS").value<>"") or not isnull(ador("DENCAMPO1").value) or not isnull(ador("DENCAMPO2").value) or not isnull(ador("ADJUNTOS").value) then %>

		        <tr><td>&nbsp;</td><td colspan="10"><table width="100%" CELLSPACING="0">
                
                <%if ador("HAYPLANES")>0 then
                    if ador("PUB_PLAN_ENTREGA")=1 then%> 
                        <tr>
                            <td class="tablasubrayadaright" valign="top"><div style="width:120px;height:auto;overflow:hidden;text-align:top;"><%=den(60)%></div></td>
                        
                            <%if not adorPlanes is nothing then
                                adorPlanes.filter="ID=" & ador("LINEAID")%>
                                <td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">
                                <%if adorPlanes.recordcount>0 then%> 
                                    <table>                                        
                                        <%while not adorPlanes.eof%>
                                            <tr>
                                            <td>&nbsp;</td>
                                            <td><%=VisualizacionFecha(adorPlanes("FECHA").value,datefmt)%></td>
                                            <td> - </td>
                                            <% if adorPlanes("TIPORECEPCION")=0 then %>
                                            <td><%=adorPlanes("CANTIDAD").value%></td>
                                            <td><%=ador("UNIDAD").value%></td>
                                            <%else %>
                                                <td><%=adorPlanes("IMPORTE").value%></td>
                                                <td><%=adorPlanes("MON").value%></td>
                                            <%end if %>
                                            </td></tr>
                                            <%adorPlanes.movenext
                                        wend%>
                                    </table>
                                <%end if%>
                                </div></td>
                            <%end if%>
                        </tr>
                    <%end if
                end if%>

			    <%if not isnull(ador("CODPROCE").value) then%>
			    <tr>
			    <td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=den(50)%></div></td>
			    <td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=ador("ANYOPROCE").value%>/<%=ador("GMN1").value%>/<%=ador("CODPROCE").value%>&nbsp;<%=ador("DENPROCE").value%></div></td>
			    </tr>
			    <%end if%>
	
			    <tr>
			    <td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=den(21)%></div></td>
			    <td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=VB2HTML(ador("OBS").value)%></div></td>
			    </tr>
			
			    <%if not isnull(ador("DENCAMPO1").value) and ador("DENCAMPO1").value<>"" then%>
			    <tr>
			    <td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=ador("DENCAMPO1").value%></div></td>
			    <td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=ador("VALOR1").value%></div></td>
			    </tr>
			    <%end if%>
	
			    <%if not isnull(ador("DENCAMPO2").value) and ador("DENCAMPO2").value<>"" then%>
			    <tr>
			    <td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=ador("DENCAMPO2").value%></div></td>
			    <td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=ador("VALOR2").value%></div></td>
			    </tr>
			    <%end if%>
			    <%if not  (adorAtrL is nothing) then 
			    while  not adorAtrL.EOF%>
				    <tr>
				      <td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=adorAtrL("DEN").value%></div></td>
				    <%if adorAtrL("TIPO").value=1 then%> 
				       <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=adorAtrL("VALOR_TEXT").value%></div></td>
				    <%end if%>
				    <%if adorAtrL("TIPO").value=2 then%> 
				       <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=visualizacionNumero(adorAtrL("VALOR_NUM").value,decimalfmt,thousanfmt,precisionfmt)%></div></td>
				    <%end if%>
				    <%if adorAtrL("TIPO").value=3 then%>
				       <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=visualizacionFecha(adorAtrL("VALOR_FEC").value,datefmt)%></div></td>
				    <%end if%>
				    <%if adorAtrL("TIPO").value=4 then%>
				      <%if isnull(adorAtrL("VALOR_BOOL").value) then%>
				        <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;</div></td>
				      <%elseif adorAtrL("VALOR_BOOL").value=0 then%>
				        <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=den(53)%></div></td>
				      <%else%>
				        <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=den(54)%></div></td>
				      <%end if
				    end if%>		  
				      </tr>
				      <%adorAtrL.movenext
			    wend
			    adorAtrL.close
			    end if%>
			
			    <%if not isnull(ador("ADJUNTOS").value) then
				    sAdjuntosLinea=""
				    set oLinea = oRaiz.Generar_CLinea()
				    oLinea.ID = ador("LINEAID").value
				    set oAdjuntos = oLinea.cargarAdjuntos(CiaComp,false)
				    if oAdjuntos.count > 0 then
					    for each oAdjunto in oAdjuntos 
						    sAdjuntosLinea=sAdjuntosLinea & oAdjunto.Nombre & " ; "
					    next
					    sAdjuntosLinea=mid(sAdjuntosLinea,1,len(sAdjuntosLinea)-3)
			    %>
					    <tr>
					    <td class="tablasubrayadaright">
                            <div style="width:100px;height:auto;overflow:hidden"><%=den(49)%>
                                <%if isempty(oLinea.ObsAdjun) then%>
                                    <a href='javascript:void(null)' onclick='verAdjuntosLinea(<%=oOrden.id%>,<%=oLinea.ID%>,"")'><img border = 0 src= '../images/clip.gif'></A>
                                <%else%>
                                    <a href='javascript:void(null)' onclick='verAdjuntosLinea(<%=oOrden.id%>,<%=oLinea.ID%>,<%=nulltostr(oLinea.ObsAdjun)%>)'><img border = 0 src= '../images/clip.gif'></A>
                                <%end if%>
					        </div>
					    </td>
					    <td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=sAdjuntosLinea%></div></td>
					    </tr>
				    <%
					    set oAdjuntos=nothing
				    end if
				    %>
			    <%end if%>
			
	      </table></td></tr>
	     <%end if%>
	

		    <%
		    if fila="filaImpar" then
			    fila ="filaPar"
		    else
			    fila ="filaImpar"
		    end if
		
		
		    ador.movenext
	    wend
    end if
ador.close	
set ador=nothing
	
	
	
	%>
	</table>
	<br>	
	<br>	
	<%
	
if oDestinos.count>0 then%>

	<table width="100%">
		<tr>
			<td colspan="7" class="cabecera"><%=den(31)%></td>
		</tr>
		<tr>
			<td class="cabecera"><%=den(24)%></td>
			<td class="cabecera"><%=den(25)%></td>
			<td class="cabecera"><%=den(26)%></td>
			<td class="cabecera"><%=den(27)%></td>
			<td class="cabecera"><%=den(28)%></td>
			<td class="cabecera"><%=den(29)%></td>
			<td class="cabecera"><%=den(30)%></td>
		</tr>

<%
fila = "filaImpar"
for each oDest in oDestinos	%>

		<tr>
			<td class="<%=fila%>"><%=oDest.cod%></td>
			<td class="<%=fila%>"><%=oDest.den%></td>
			<td class="<%=fila%>"><%=oDest.dir%></td>
			<td class="<%=fila%>"><%=oDest.cp%></td>
			<td class="<%=fila%>"><%=oDest.pob%></td>
			<td class="<%=fila%>"><%=oDest.provi%></td>
			<td class="<%=fila%>"><%=oDest.pai%></td>
	</tr>

<%

next
%>
</table>
<%
end if
%>
</td></tr></table><br><br>
<%
if oRaiz.Sesion.Parametros.ImprimirPoliticaPedidos then
	'Linea de separacion
%>
	<hr class="P_acep_pedidos"><br>
		
<%
	Response.Write oRaiz.PoliticaAceptacionPedidos(Idioma)
end if
set oRaiz=nothing

set oProve=nothing
set oCompania=nothing
set oOrdenes=nothing
set oDestinos = nothing
set oPersona=nothing
set oPersonas=nothing
set oOrden =nothing
set oAdjuntosOrden=nothing
%>
<br>
<center><input id="cmdImprimir" style="font-size:15px;font-weight:bold;" type="image" src="<%=APPLICATION("RUTASEGURA")%>/script/images/impresora.gif" onclick="javascript:Imprimir()"></center>
</body>
</html>
<%Response.End%>
