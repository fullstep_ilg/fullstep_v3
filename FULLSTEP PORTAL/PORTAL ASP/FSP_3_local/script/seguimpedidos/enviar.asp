﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/formatos.asp"-->
<%
''' <summary>
''' Envia un pedido
''' </summary>
''' <remarks>Llamada desde: seguimpedidos\seguimclt.asp     seguimpedidos\seguimclt41.asp ; Tiempo máximo: 0,2</remarks>

dim cia
Idioma = Request("Idioma")
set oRaiz=validarUsuario(Idioma,true,false,0)


OrdenId = request("OrdenId")

set oOrden = oRaiz.Generar_COrden()

oOrden.id = clng(OrdenId)
ciacomp=oRaiz.Sesion.CiaComp


oraiz.ComenzarTransaccion()

set oError = oOrden.RegistrarEnvio(ciacomp)
if oError.numError = 200 then
	oraiz.cancelarTransacccion()
	Response.End
	
end if

oraiz.confirmarTransaccion()


set oOrden = nothing



set oRaiz=nothing

%>
<html>
<title><%=title%></title>
<script>
function init()
	{
	var p
	p=window.parent.frames["fraSeguimClient"]
	p.envioRegistrado(<%=OrdenId%>,<%=oError.numError%>)
	
	}
</script>

<body onload="init()">

</body>
</html>

