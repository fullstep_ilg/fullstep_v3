﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<%
''' <summary>
''' Imprime los pedidos
''' </summary>
''' <remarks>Llamada desde: seguimpedidos\seguimclt.asp ; Tiempo máximo: 0,2</remarks>

	set oRaiz=validarUsuario(Idioma,true,true,0)
    
    set ParamGen = oRaiz.Sesion.Parametros

	CiaComp = oRaiz.Sesion.CiaComp
	CiaCod = oRaiz.Sesion.CiaCod

		
	Prove = clng(oRaiz.Sesion.CiaId)
	'obtiene los formatos del usuario
	decimalfmt=request.Cookies ("USU_DECIMALFMT")
	thousanfmt=Request.Cookies ("USU_THOUSANFMT")
	precisionfmt=cint(Request.Cookies ("USU_PRECISIONFMT"))
	datefmt=Request.Cookies("USU_DATEFMT") 
    
    PedidosAbiertos=Request("PedidosAbiertos")

	Idioma = Request("Idioma")
	Idioma = trim(Idioma)

	dim den
	den = devolverTextos(Idioma,85)	

%>
<html>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/formatos.js"></script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><%=title%></title>
</head>
<%
	set oProve = oraiz.DevolverProveedorEnCiaComp(Idioma,Prove,CiaComp)
		
	Referencia=request("Referencia")
    CiaDen = Request("CiaDen")
    sEstados=""
    if Request("opcTodos")<>"-1" then
	    txtArtInt=vacio2null(request("txtArtInt"))
	    txtDenArt=vacio2null(request("txtDenArt"))
	    txtPedidoProve = vacio2null(request("txtPedidoProve"))
	    txtCodExt=vacio2null(request("txtCodExt"))
        txtCesta=vacio2null(request("txtCesta"))
        txtNPed=vacio2null(request("txtNPed"))
	    'Pedidos Estándar
        if not isnull(vacio2null(Request ("lstAnyo"))) then
		    iAnyo = clng(Request ("lstAnyo"))
	    else
		    iAnyo = null
	    end if

        if PedidosAbiertos=1 then
            for each estado in Request("chkAbiertos")
                sEstados=sEstados & estado & ","
            next
            if sEstados<>"" then
                sEstados = left(sEstados,len(sEstados)-1)
            end if
            if not isnull(vacio2null(Request ("lstAnyoAbierto"))) then
		        iAnyoAbierto = clng(Request ("lstAnyoAbierto"))
	        else
		        iAnyoAbierto = null
	        end if

            txtCestaAbierto=vacio2null(request("txtCestaAbierto"))
            txtNPedAbierto=vacio2null(request("txtNPedAbierto"))
        else
	        chkEst0=clng(null2zero(vacio2null(request("chkEst0"))))
	        chkEst1=clng(null2zero(vacio2null(request("chkEst1"))))
	        chkEst2=clng(null2zero(vacio2null(request("chkEst2"))))
	        chkEst3=clng(null2zero(vacio2null(request("chkEst3"))))
	        chkEst4=clng(null2zero(vacio2null(request("chkEst4"))))
	        chkEst5=clng(null2zero(vacio2null(request("chkEst5"))))
	        est = chkEst1 or chkEst2 or chkEst3 or chkEst4 or chkEst5 or chkEst0
	        chkEst6 = Request("chkEst6")
	        If isempty(chkEst6) then
		        chkEst6 = 0
	        else
		        chkEst6 = 1
	        end if
            'Calculo el parametro recep que tiene que ser 1 cuando pido las penditentes de recepcionar
            if chkEst0=0 then
		        ipend=0
	        else
		        ipend=1
	        end if
	
	        'Calculo el parametro recep que tiene que ser 1 cuando pido las penditentes de recepcionar
            if chkEst1=0 then
		        if chkEst2<>0 then
			        irecep=1
		        else
			        irecep=0
		        end if
	        else
		        irecep=1
	        end if			
	        if est=0 and chkEst6=0 then
		        est=380
	        end if 
        end if
	
	    lstProve=oProve.Cod
	    txtFechaDesde=Fecha(vacio2null(request("txtFechaDesde")),datefmt)
	
	    txtFechaHasta=Fecha(vacio2null(request("txtFechaHasta")),datefmt)
	    txtDesdeId = vacio2null(request("txtDesdeId"))
    else
	    txtArtInt=null
	    txtDenArt=null
	    txtPedidoProve = null
	    est = 60
	    lstProve=oProve.Cod
	    txtFechaDesde=null
	    txtFechaHasta=null
	    txtDesdeId = null
	    txtCodExt= null
	    irecep=0
	    ipend=0	
    end if

	set oOrdenes=oRaiz.Generar_cOrdenes()
	
	CiaProv=oRaiz.Sesion.CiaCodGs
	
	dim sAdjuntosLinea
	dim sAdjuntosOrden

%>


<script>

function resize()
	{
	p = window.opener 
	p.winEspera.close()
	window.moveTo(60,20)
	window.resizeTo (700,550)
	window.focus()
	}

function Imprimir()
{	
	document.getElementById("cmdImprimir").style.visibility = "hidden"
	window.print()
	document.getElementById("cmdImprimir").style.visibility = "visible"
}

function verAdjuntosOrden(id)
{
    window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/adjuntosorden.asp?Orden=" + id, "_blank", "top=5,left=5,width=600, height=250,directories=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes")
}

function verAdjuntosLinea(orden,id,obs)
{
    window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/adjuntoslinea.asp?Orden=" + orden + "&Linea=" + id + "&Obs=" + obs, "_blank", "top=5,left=5,width=600, height=250,directories=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes")
}
</script>
<style>
#divimgCompradora {position:relative}
</style>
</head>

<body onload="resize()">
<h1><%=den(15)%>&nbsp;&nbsp;&nbsp;&nbsp;<span class="negro"><%=CiaDen%></span></h1>
<%

Set oPersonas = oRaiz.Generar_CPersonas()

oPersonas.CargarTodasLasPersonas CiaComp
set paramGen = oRaiz.DevolverParamGSPedidos(CiaComp)
pedDirectoEnvioEmail = paramGen(1)
pedDirFac = paramGen(2)
if pedDirectoEnvioEmail then
	set ador = oOrdenes.BuscarTodasOrdenes(CiaComp, Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"), Est, 0, null, null, txtArtInt,txtDenArt,txtCodExt, txtFechaDesde,txtFechaHasta, lstProve, null,  txtPedidoProve, ianyo,txtCesta,txtNPed,null, chkEst6,txtRef,icompleta,iEmpresa,null,null,null,null,null,ipend,irecep,,,PedidosAbiertos,sEstados, iAnyoAbierto,txtCestaAbierto,txtNPedAbierto)
else
	set ador = oOrdenes.BuscarTodasOrdenes(CiaComp, Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"), Est, 0, null, null, txtArtInt,txtDenArt,txtCodExt, txtFechaDesde,txtFechaHasta, lstProve, null,  txtPedidoProve, ianyo,txtCesta,txtNPed,null, chkEst6,txtRef,icompleta,iEmpresa,null,null,null,null,1,ipend,irecep,,,PedidosAbiertos,sEstados,iAnyoAbierto,txtCestaAbierto,txtNPedAbierto)
end if
	
if not ador is nothing then
	
	while not ador.eof 

	%>
		<table width="100%">
			<tr>
				<td>
					<table width="100%">
						<tr>
							<td rowspan="2" width="33%" class="cabecera"><%=den(41)%></td>
							<td class="filaPar" colspan="3">
								<%=ador.fields("NIFEMPRESA").value%>
							</td>
						</tr>
						<tr>
							<td class="filaPar" colspan="3">
								<%=ador.fields("DENEMPRESA").value%>
							</td>
						</tr>
						<% if pedDirFac then %>
						<tr>
							<td class="cabecera"><%=den(59)%></td>
							<td class="filaPar" colspan="3">
								<%=ador.fields("DIRFAC").value%>
							</td>							
						</tr>
						<%end if%>

					</table>
				</td>
			</tr>
		</table>
	<%
		OrdenId=ador("id").value			
		set oOrden = oRaiz.Generar_COrden()
		oOrden.id = clng(OrdenId)

		set adorAtr = oOrden.DevolverAtributos(CiaComp)
		
		if isnull(ador.Fields("PER").Value) then
			set oPersona =NOTHING
		else
			If Not oPersonas.Item(ador.Fields("PER").Value) Is Nothing Then
				set oPersona = oPersonas.Item(ador.Fields("PER").Value)
			End If
		end if	
		if isnull(ador.Fields("RECEPTOR").Value) then
			set oPersonaR =NOTHING
		else		
			If Not oPersonas.Item(ador.Fields("RECEPTOR").Value) Is Nothing Then
				set oPersonaR = oPersonas.Item(ador.Fields("RECEPTOR").Value)
			else
				set oPersonaR =nothing
			End If
		end if				
		set oDestinos=oRaiz.Generar_cDestinos()
        
        'Costes y descuentos
        if ador("HAYCOSTES")>0 or ador("HAYDESCUENTOS")>0 then
            set adorCostesDesc=oOrden.DevolverCostesDescuentosCab(CiaComp)        
        end if	
        dImporteTotal=ador("IMPORTE")*ador("CAMBIO")
        dImporteBruto=0
        set adorImp = oOrden.Devolverlineas(CiaComp,Idioma)
        if not adorImp.eof then
	        while not adorImp.eof
                dImporteBrutoLin=adorImp("IMPORTE").value
                if not isnull(adorImp("COSTES")) then dCostesLin=adorImp("COSTES")*ador("CAMBIO")
                if not isnull(adorImp("DESCUENTOS")) then dDescuentosLin=adorImp("DESCUENTOS")*ador("CAMBIO")
                dImporteBruto=dImporteBruto+dImporteBrutoLin+dCostesLin-dDescuentosLin
                adorImp.movenext
            wend
        end if
        set oLineasDesgloseImpuesto = oRaiz.Generar_CLineasDesgImpuesto
        oLineasDesgloseImpuesto.cargarLineasDesgloseImpuestoOrden oOrden.id,CiaComp,Idioma
        
        tipoPedAbierto=ador.fields("PED_ABIERTO_TIPO").value
        if isnull(tipoPedAbierto) then
            sCantidad=Den (11) 'Cantidad
            sImporte= Den (20) 'Importe
        else
            sCantidad=Den (77) 'Cantidad abierta
            sImporte= Den (79)  'Importe abierto 
        end if
		
    %>
		<table width="100%" style="border:solid 1 black;page-break-after:always;">
			<tr>
				<td>
		<table width="100%">
			<tr>
				<td width="30%" class="cabecera"><%=den(1)%></td>
				<td width="20%" class="filaPar">
					<%=ador("anyo").value%>/<%=ador("numpedido").value%>/<%=ador("numorden").value%>
				</td>
				<td width="20%" class="cabecera"><%=den(51)%></td>
				<td width="30%" class="filaPar">
					<%=ador("NUMEXT").value%>
				</td>				
			</tr>
			
			<%if Referencia="" then%>
				<tr>
					<td width="30%" class="cabecera"><%=den(2)%></td>
					<td width="20%" class="filaPar">
						<%
						if not isnull(ador("FECHA").value) then
							Response.write VisualizacionFecha(ador("FECHA").value,datefmt)
						else
							Response.Write "&nbsp;"
						end if%>
					</td>
					<td width="20%" class="cabecera"><%=den(52)%></td>
					<td width="30%" class="filaPar" colspan="5">
						<%VB2HTML(ador.fields("PAG").value & " - " & ador.fields("PAGDEN").value)%>
					</td>
				</tr>
				<tr>														
					<td width="30%" class="cabecera"><%=den(32)%></td>
					<td width="20%" class="filaPar" colspan="3">
						<%
						select case ador("EST").value
							case 2:
								if ador("ACEP_PROVE").value=0 then
									Response.Write den(58)
								else
									Response.Write den(47)
								end if
							case 3:
								Response.Write den(36)
							case 4:
								Response.Write den(37)
							case 5:
								Response.Write den(38)
							case 6:
								Response.Write den(39)
							case 21:
								Response.Write den(40)
						end select%>
					</td>					
				</tr>
			<%else%>
				<tr>
					<td width="30%" class="cabecera"><%=Referencia%></td>
					<td width="20%" class="filaPar"><%=ador("REFERENCIA").value%></td>
					<td width="20%" class="cabecera"><%=den(2)%></td>
					<td width="30%" class="filaPar">
						<%
						if not isnull(ador("FECHA").value) then
							Response.write VisualizacionFecha(ador("FECHA").value,datefmt)
						else
							Response.Write "&nbsp;"
						end if%>
					</td>
				</tr>					
				<tr>
					<td width="30%" class="cabecera"><%=den(52)%></td>
					<td width="20%" class="filaPar">
						<%VB2HTML(ador.fields("PAG").value & " - " & ador.fields("PAGDEN").value)%>
					</td>					
				    <td width="20%" class="cabecera"><%=den(32)%></td>
				    <td class="filaPar">
					    <%
					    select case ador("EST").value
						    case 2:
								    if ador("ACEP_PROVE").value=0 then
									    Response.Write den(58)
								    else
									    Response.Write den(47)
								    end if
						    case 3:
							    Response.Write den(36)
						    case 4:
							    Response.Write den(37)
						    case 5:
							    Response.Write den(38)
						    case 6:
							    Response.Write den(39)
						    case 21:
							    Response.Write den(40)
					    end select%>
				    </td>
			</tr>			
			<%end if			
			if not isnull(ador("ADJUNTOS").value) then
				'Carga los adjuntos
				sAdjuntosOrden=""
				set oAdjuntosOrden = oOrden.cargarAdjuntos(CiaComp,false)
				for each oAdjunto in oAdjuntosOrden
					sAdjuntosOrden=sAdjuntosOrden & oAdjunto.Nombre & " ; "
				next
				sAdjuntosOrden=mid(sAdjuntosOrden,1,len(sAdjuntosOrden)-3)
			
			%>
			<tr>	
				<td class="cabecera" width="20%"><%=den(49)%><a href='javascript:void(null)' onclick='verAdjuntosOrden(<%=oOrden.id%>)'><img border = 0 src= '../images/clip.gif'></A></td>
				<td class="filaPar" colspan="3"><%=sAdjuntosOrden%></td>
			</tr>
			<%end if%>
			<%if not isnull(ador("OBS").value) then%>
			<tr>	
				<td class="cabecera" width="20%"><%=den(21)%></td>
				<td class="filaPar" colspan="3"><%=ador("OBS").value%></td>
			</tr>
			<%end if%>
			<%
			if not adorAtr is nothing then
				while not adorAtr.eof%>
			        <tr>
				        <td width="30%" class="cabecera"><%=adorAtr("DEN").value%></td>
			        <%if adorAtr("TIPO").value=1 then%> 
			            <td class="filaPar" colspan="3"><%=VB2HTML(adorAtr("VALOR_TEXT").value)%></td>
			        <%end if%>
			        <%if adorAtr("TIPO").value=2 then%> 
			            <td class="filaPar" colspan="3"><%=visualizacionNumero(adorAtr("VALOR_NUM").value,decimalfmt,thousanfmt,precisionfmt)%></td>
			        <%end if%>
			        <%if adorAtr("TIPO").value=3 then%>
			            <td class="filaPar" colspan="3"><%=visualizacionFecha(adorAtr("VALOR_FEC").value,datefmt)%></td>
			        <%end if%>
			        <%if adorAtr("TIPO").value=4 then%>
			            <%if isnull(adorAtr("VALOR_BOOL").value) then%>
			                <td class="filaPar" colspan="3">&nbsp;</td>
			            <%elseif adorAtr("VALOR_BOOL").value=0 then%>
			            <td class="filaPar" colspan="3"><%=den(53)%></td>
			            <%else%>
			            <td class="filaPar" colspan="3"><%=den(54)%></td>
			            <%end if
			        end if
			        adorAtr.movenext
			    wend
			end if%>
            <!-- Pedidos abiertos cabecera -->
			<% 
                if not isnull(tipoPedAbierto) then %>
                <tr><td class="cabecera" colspan="4"><%=den(81) %></td></tr>
                <tr>
                    <td class="cabecera"><%=den(82) %></td>                
				    <td class="filaPar" style="text-align:right;">
					    <%=ador("PED_ABIERTO_FECINI").value %>&nbsp;					
				    </td>
                    <td class="filaPar" colspan="2"/>	
                </tr>
                <tr>
                    <td class="cabecera"><%=den(83) %></td>                
				    <td class="filaPar" style="text-align:right;">
					    <%=ador("PED_ABIERTO_FECFIN").value %>&nbsp;				
				    </td>
                    <td class="filaPar" colspan="2"/>	
                </tr>
            <% end if %>
            <!--IMPORTES-->
            <tr><td class="cabecera" colspan="4"><%=Den(61)%></td></tr>
            <tr>
                <td class="cabecera"><%=Den(62)%></td>
                <td class="filaPar" style="text-align:right;">
					<%=replace(FormatNumber(dImporteBruto,2),",",".")%>&nbsp;<%=ador("MON").value%>
                    <%MonedaPedido=ador("MON").value%>
					<%CambioPedido=ador("CAMBIO").value%>					
				</td>
                <td class="filaPar" colspan="2"/>
            </tr>
            <!--Costes/descuentos-->
            <%if ador("HAYCOSTES")>0 or ador("HAYDESCUENTOS")>0 then
                if not adorCostesDesc is nothing then
                    adorCostesDesc.movefirst
                    while not adorCostesDesc.eof                    
                        sDescCD=VisualizacionCosteDesc(iif(isnull(adorCostesDesc("DEN")),"",adorCostesDesc("DEN")),adorCostesDesc("TIPO"),adorCostesDesc("OPERACION"),adorCostesDesc("VALOR"),ador("MON").value,CambioPedido)%>
                        <tr>
                            <td class="cabecera"><%=sDescCD%></td>
                            <td class="filaPar" style="text-align:right;">
                                <%=replace(FormatNumber((adorCostesDesc("IMPORTE")*CambioPedido),2),",",".")%>&nbsp;<%=ador("MON").value%>
                            </td>
                            <td class="filaPar" colspan="2"/>
                        </tr>
                        <%adorCostesDesc.movenext
                    wend
                end if
            end if%>      
            <!--Impuestos-->
            <%for each oLineaDesgloseImpuesto in oLineasDesgloseImpuesto%>
            <tr>
                <td class="cabecera">
                    <%=Den(73)%>&nbsp;<%=oLineaDesgloseImpuesto.tipoimpuesto%>&nbsp;<%=oLineaDesgloseImpuesto.TipoValor%>%&nbsp;
                    (<%=Den(74)%>:&nbsp;<%=replace(FormatNumber(oLineaDesgloseImpuesto.BaseImponible,2),",",".")%>&nbsp;<%=ador("MON").value%>)
                </td>
                <td class="filaPar" style="text-align:right;">
                    <%=replace(FormatNumber(oLineaDesgloseImpuesto.cuota,2),",",".")%>&nbsp;<%=ador("MON").value%>
                </td>
                <td class="filaPar" colspan="2"/>
            </tr>
            <% next%>
            <!--Importe total-->       
            <tr>
                <td class="cabecera"><%=Ucase(Den(63))%></td>                
				<td class="filaPar" style="text-align:right;">
					<%=replace(FormatNumber(dImporteTotal,2),",",".")%>&nbsp;<%=ador("MON").value%>					
				</td>
                <td class="filaPar" colspan="2"/>	
            </tr>

			<%if not oPersona is nothing then %>			
			<tr colspan="2"><td class="cabecera" colspan="4"><%=den(55)%></td></tr>
			<tr>
				<td class="cabecera" width="20%"><%=den(44)%></td>			
				<td width="30%" class="filaPar">		
					<%=oPersona.nombre%>&nbsp;<%=oPersona.Apellidos%>
				</td>
				<td width="20%" rowspan="3" class="cabecera" valign="top"><%=den(48)%></td>
				<td width="30%" class="filaPar"><%=oPersona.UON1DEN%></td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(45)%></td>
				<td class="filaPar"><%=oPersona.tfno%></td>
				<td class="filaPar">
					<%if not isnull(oPersona.uon2) then%>
						<%=oPersona.UON2DEN%>
					<%end if%>
				</td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(46)%></td>
				<td class="filaPar">
						<a href="mailto:<%=oPersona.email%>"><%=VB2HTML(oPersona.email)%></a>
				</td>
				<td class="filaPar">
					<%if not isnull(oPersona.uon3) then%>
						<%=oPersona.UON3DEN%>
					<%end if%>
				</td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(57)%></td>
				<td class="filaPar"><%=VB2HTML(oPersona.fax)%></td>			
				<td class="cabecera"><%=den(42)%></td>
				<td class="filaPar"><%=VB2HTML(oPersona.depden)%></td>
			 </tr>		
		<%end if%>
		<%if not oPersonaR is nothing then %>			
			<tr><td class="cabecera" colspan="4"><%=den(56)%></td></tr>
			<tr>
				<td class="cabecera" width="20%"><%=den(44)%></td>			
				<td width="30%" class="filaPar">		
					<%=oPersonaR.nombre%>&nbsp;<%=oPersonaR.Apellidos%>
				</td>
				<td width="20%" rowspan="3" class="cabecera" valign="top"><%=den(48)%></td>
				<td width="30%" class="filaPar"><%=VB2HTML(oPersonaR.UON1DEN)%></td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(45)%></td>
				<td class="filaPar"><%=oPersonaR.tfno%></td>
				<td class="filaPar">
					<%if not isnull(oPersonaR.uon2) then%>
						<%=oPersonaR.UON2DEN%>
					<%end if%>
				</td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(46)%></td>
				<td class="filaPar">
						<a href="mailto:<%=oPersonaR.email%>"><%=VB2HTML(oPersonaR.email)%></a>
				</td>
				<td class="filaPar">
					<%if not isnull(oPersonaR.uon3) then%>
						<%=oPersonaR.UON3DEN%>
					<%end if%>
				</td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(57)%></td>
				<td class="filaPar"><%=VB2HTML(oPersonaR.fax)%></td>			
				<td class="cabecera"><%=den(42)%></td>
				<td class="filaPar"><%=VB2HTML(oPersonaR.depden)%></td>
			 </tr>		
		<%end if%>

		</table>
		<br>
		<br>	
		<%
		OrdenId=ador("id").value
        tipoPedAbierto=ador.fields("PED_ABIERTO_TIPO").value
        if isnull(tipoPedAbierto) then
            sCantidad=Den (11) 'Cantidad
            sImporte= Den (20) 'Importe
        else
            sCantidad=Den (77) 'Cantidad abierta
            sImporte= Den (79)  'Importe abierto 
        end if 

		oOrden.id = clng(OrdenId)
		set adorLinea = oOrden.Devolverlineas(CiaComp,Idioma)
        
        'Ocultar columna fecha de entrega
        iNumLinPlanEnt=0
        iNumLinPlanEntPub=0
        bOcultarFecEntrega=false
        if not adorLinea is nothing then
            if adorLinea.recordcount>0 then
                while not adorLinea.eof               
                    if adorLinea("HAYPLANES")>0 then
                        iNumLinPlanEnt=iNumLinPlanEnt+1
                        if adorLinea("PUB_PLAN_ENTREGA")=1 then iNumLinPlanEntPub=iNumLinPlanEntPub+1
                    end if
                    adorLinea.movenext
                wend
            end if

            if iNumLinPlanEnt>0  then
                if iNumLinPlanEntPub>0 then set adorPlanes=oOrden.DevolverPlanesEntrega(CiaComp)            
                if (iNumLinPlanEnt=ador.recordcount) then bOcultarFecEntrega=true                    
            end if

            adorLinea.movefirst
        end if          
        %>
		
		<table width="100%">
			<tr>
				<td class="cabecera"><%=den(4)%></td>
				<td class="cabecera"><%=den(5)%></td>
				<td class="cabecera"><%=den(6)%></td>
				<td class="cabecera"><%=den(7)%></td>
				<td class="cabecera"><%=den(8)%></td>
				<%if not bOcultarFecEntrega then %>
				    <td class="cabecera"><%=den(9)%></td>
				    <td class="cabecera"><%=den(10)%></td>
                <%end if%>
                <% if tipoPedAbierto = 3 or isnull(tipoPedAbierto) then %>
				    <td class="cabecera"><%=sCantidad%></td>
                <% end if %>
                <% if tipoPedAbierto = 3 then %>
                    <td class="cabecera"><%=den(78)%></td>
                <% end if %>
				<td class="cabecera"><%=den(12)%></td>
                <td class="cabecera"><%=den(65)%>&nbsp;(<%=MonedaPedido%>)</td>
                <td class="cabecera"><%=den(66)%>&nbsp;(<%=MonedaPedido%>)</td>
				<td class="cabecera"><%=sImporte%>&nbsp;(<%=MonedaPedido%>)</td>
                <% if tipoPedAbierto=2 then %>
                    <td class="cabecera"><%=den(80)%>&nbsp;(<%=MonedaPedido%>)</td>
                <% end if %>
				<td class="cabecera"><%=den(34)%></td>
                
			</tr>
	<%	
	fila = "filaImpar"
    if not adorLinea is nothing then

        
		while not adorLinea.eof
			if not isnull(adorLinea("CANTIDAD").value) then
                cantidad=visualizacionNumero(adorLinea("CANTIDAD").value,decimalfmt,thousandfmt,precisionfmt)
            else
                cantidad="&nbsp"
            end if
            if not isnull(adorLinea("CANTIDAD_PED_ABIERTO").value) then
                cantidadPedida=visualizacionNumero(adorLinea("CANTIDAD_PED_ABIERTO").value,decimalfmt,thousandfmt,precisionfmt)
            else
                cantidadPedida="0"
            end if
            if not isnull(adorLinea("IMPORTE_PED_ABIERTO").value) then
                importePedidoAb=visualizacionNumero(adorLinea("IMPORTE_PED_ABIERTO").value,decimalfmt,thousandfmt,precisionfmt)
            else
                importePedidoAb="0"
            end if
        %>
			<tr>
				<td class="<%=fila%>">
					<%=adorLinea("codart").value%>
				</td>
				<td class="<%=fila%>">
					<%=adorLinea("art_ext").value%>
				</td>
				<td class="<%=fila%>">
					<%=adorLinea("denart").value%>
				</td>
				<td class="<%=fila%>">
					<%=adorLinea("DESTINO").value%>
					
					<%if oDestinos.item(adorLinea("DESTINO").value) is nothing then
						set adorDest = oDestinos.CargarDatosDestino(Idioma,Ciacomp, adorLinea("DESTINO").value, adorLinea("LINEAID").value)
						oDestinos.Add adorDest("COD").Value, adorDest("DEN").Value, false, adorDest("DIR"), adorDest("POB"), adorDest("CP"), adorDest("PROVI"), adorDest("PAI")
						adorDest.close
						set adorDest = nothing
					end if%>
					
				</td>
				<td class="<%=fila%>">
					<%=adorLinea("UNIDAD").value%>-<%=adorLinea("UNIden").value%>
				</td>
				<%if not bOcultarFecEntrega then %>
                    <%if adorLinea("HAYPLANES")>0 then%>                                           
                        <td class="<%=fila%>">&nbsp;</td>                                             
                        <td class="<%=fila%>">&nbsp;</td> 
                    <%else%>
			            <td class="<%=fila%>">
				            <%
				            if not isnull(adorLinea("FECENTREGA").value) then                            
					            Response.write VisualizacionFecha(adorLinea("FECENTREGA").value,datefmt)
				            else
					            Response.Write "&nbsp;"
				            end if%>
			            </td>
                        <td class="<%=fila%>">
				            <%if adorLinea("ENTREGA_OBL").value=1 then
					            Response.Write den(18)
				            else
					            Response.Write den(19)
				            end if%>
			            </td>
                    <%end if%>                 
                <%end if%>		
			    <% if tipoPedAbierto = 3 or isnull(tipoPedAbierto) then %>
                    <td class="<%=fila%> numero">
				        <%=cantidad%>
                    </td>
                <% end if %>
                <% if tipoPedAbierto = 3 then %>
                    <td class="<%=fila%> numero">
				        <%=cantidadPedida%>
			        </td>
                <% end if %>
				
                <% if tipoPedAbierto= 3 or isnull(tipoPedAbierto) then %>
			        <%if adorLinea("TIPORECEPCION").value=1 then %>
                        <td class=<%=fila%> align=right>&nbsp;</td>"	 
                    <%else %> 
			            <td class="<%=fila%> numero">
				            <%=visualizacionNumero(adorLinea("PRECIOUNITARIO").value ,decimalfmt,thousanfmt,precisionfmt)%>
			            </td>
                    <%end if %>
                <% end if %>

                <td class="<%=fila%> numero">
				    <%=visualizacionNumero(adorLinea("COSTES").value * CambioPedido ,decimalfmt,thousanfmt,precisionfmt)%>
			    </td>
                <td class="<%=fila%> numero">
				    <%=visualizacionNumero(adorLinea("DESCUENTOS").value * CambioPedido ,decimalfmt,thousanfmt,precisionfmt)%>
			    </td>
				<td class="<%=fila%> numero">
					<%=visualizacionNumero((adorLinea("PRECIOUNITARIO").value * adorLinea("CANTIDAD").value)+(adorLinea("COSTES").value-adorLinea("DESCUENTOS").value) *CambioPedido,decimalfmt,thousanfmt,precisionfmt)%>
				</td>
                <% if tipoPedAbierto=2 then %>
                    <td class="<%=fila%> numero">
				        <%=importePedidoAb%>
			        </td> 
                <% end if %>
				<td class="<%=fila%> numero">
					<%=visualizacionNumero(adorLinea("CANT_REC").value,decimalfmt,thousanfmt,precisionfmt)%>
				</td>
			</tr>	
            
            <!--COSTES y DESCUENTOS-->
            
            <%if adorLinea("HAYCOSTES")>0 or adorLinea("HAYDESCUENTOS")>0 then
                set adorCDs = oOrden.DevolverCostesDescuentosLinea(adorLinea("LINEAID").value,CiaComp)   
            
                if not adorCDs is nothing then%>
                    <tr>
                        <%if not bOcultarFecEntrega then %>
                            <td colspan="9"></td>
                        <%else%>
                            <td colspan="7"></td>
                        <%end if%>
                        <td class="cabecera" colspan="4"><%=Den(67)%></td>
                    </tr>
                    <%if adorLinea("HAYCOSTES")>0 then%>
                        <tr>
                            <%if not bOcultarFecEntrega then %>
                                <td colspan="9"></td>
                            <%else%>
                                <td colspan="7"></td>
                            <%end if%>
                            <td class="cabecera" colspan="2"><%=Den(68)%></td>
                            <td class="cabecera" colspan="2"><%=Den(70)%>&nbsp;(<%=MonedaPedido%>)</td>                   
                        </tr>
                        <%adorCDs.movefirst
                        adorCDs.filter="TIPO=0"
                        while not adorCDs.eof
                            sDescCD=VisualizacionCosteDesc(iif(isnull(adorCDs("DEN")),"",adorCDs("DEN")),adorCDs("TIPO"),adorCDs("OPERACION"),adorCDs("VALOR"),MonedaPedido,CambioPedido)%>
                            <tr>
                                <%if not bOcultarFecEntrega then %>
                                    <td colspan="9"></td>
                                <%else%>
                                    <td colspan="7"></td>
                                <%end if%>
                                <td class=<%=fila%> colspan="2"><%=sDescCD%></td>
                                <td class=<%=fila%> colspan="2" style="text-align:right;"><%=visualizacionNumero((adorCDs("IMPORTE")*CambioPedido),decimalfmt,thousanfmt,precisionfmt)%></td>                            
                            </tr>
                            <%adorCDs.movenext
                        wend
                    end if
                    if adorLinea("HAYDESCUENTOS")>0 then%>
                        <tr>
                            <%if not bOcultarFecEntrega then %>
                                <td colspan="9"></td>
                            <%else%>
                                <td colspan="7"></td>
                            <%end if%>
                            <td class="cabecera" colspan="2"><%=Den(69)%></td>
                            <td class="cabecera" colspan="2"><%=Den(71)%>&nbsp;(<%=MonedaPedido%>)</td>
                        </tr>
                        <%adorCDs.movefirst
                        adorCDs.filter="TIPO=1"
                        while not adorCDs.eof
                            sDescCD=VisualizacionCosteDesc(iif(isnull(adorCDs("DEN")),"",adorCDs("DEN")),adorCDs("TIPO"),adorCDs("OPERACION"),adorCDs("VALOR"),MonedaPedido,CambioPedido)%>
                            <tr>
                                <%if not bOcultarFecEntrega then %>
                                    <td colspan="9"></td>
                                <%else%>
                                    <td colspan="7"></td>
                                <%end if%>
                                <td class=<%=fila%> colspan="2"><%=sDescCD%></td>
                                <td class=<%=fila%> colspan="2" style="text-align:right;"><%=visualizacionNumero((adorCDs("IMPORTE")*CambioPedido),decimalfmt,thousanfmt,precisionfmt)%></td>                            
                            </tr>
                            <%adorCDs.movenext
                        wend
                    end if
                end if  
            end if%>
            <!--Pedido Abierto-->	
            <td>&nbsp;</td>
            <% if not isnull(adorLinea("ANYO_PED_ABIERTO").value) then %>
                <td align='left' class="cabecera">
                    <%=Den(84)%>
                </td>
                <td align='left' class="<%=fila%>" colspan='2'>
                    <%=adorLinea("ANYO_PED_ABIERTO").value & "/" & adorLinea("PEDIDO_PED_ABIERTO").value & "/" & adorLinea("ORDEN_PED_ABIERTO").value %>
                </td>
            <% end if %>

			<%set adorAtrL = oOrden.DevolverAtributosLinea(adorLinea("LINEAID").value,CiaComp)	
			if not (adorAtrL is nothing) or  not isnull(adorLinea("CODPROCE").value) or (not isnull(adorLinea("OBS").value) AND adorLinea("OBS").value<>"") or not isnull(adorLinea("DENCAMPO1").value) or not isnull(adorLinea("DENCAMPO2").value) or not isnull(adorLinea("ADJUNTOS").value) then %>

			  <tr><td>&nbsp;</td><td colspan="12"><table width="100%" CELLSPACING="0">
                <%if adorLinea("HAYPLANES")>0 then
                    if adorLinea("PUB_PLAN_ENTREGA")=1 then%> 
                        <tr>
                            <td class="tablasubrayadaright" valign="top"><div style="width:120px;height:auto;overflow:hidden;text-align:top;"><%=den(60)%></div></td>
                        
                            <%if not adorPlanes is nothing then
                                adorPlanes.filter="ID=" & adorLinea("LINEAID")%>
                                <td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">
                                <%if adorPlanes.recordcount>0 then%> 
                                    <table>                                        
                                        <%while not adorPlanes.eof%>
                                            <tr>
                                            <td>&nbsp;</td>
                                            <td><%=VisualizacionFecha(adorPlanes("FECHA").value,datefmt)%></td>
                                            <td> - </td>
                                            <% if adorPlanes("TIPORECEPCION")=0 then %>
                                            <td><%=adorPlanes("CANTIDAD").value%></td>
                                            <td><%=ador("UNIDAD").value%></td>
                                            <%else %>
                                                <td><%=adorPlanes("IMPORTE").value%></td>
                                                <td><%=adorPlanes("MON").value%></td>
                                            <%end if %>
                                            </td></tr>
                                            <%adorPlanes.movenext
                                        wend%>
                                    </table>
                                <%end if%>
                                </div></td>

                            <%end if%>
                        </tr>
                    <%end if
                end if%>
                
				<%if not isnull(adorLinea("CODPROCE").value) then%>
				<tr>
				<td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=den(50)%></div></td>
				<td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=adorLinea("ANYOPROCE").value%>/<%=adorLinea("GMN1").value%>/<%=adorLinea("CODPROCE").value%>&nbsp;<%=adorLinea("DENPROCE").value%></div></td>
				</tr>
				<%end if%>
	
				<tr>
				<td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=den(21)%></div></td>
				<td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=VB2HTML(adorLinea("OBS").value)%></div></td>
				</tr>
				
				<%if not isnull(adorLinea("DENCAMPO1").value) and adorLinea("DENCAMPO1").value<>"" then%>
				<tr>
				<td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=adorLinea("DENCAMPO1").value%></div></td>
				<td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=adorLinea("VALOR1").value%></div></td>
				</tr>
				<%end if%>
	
				<%if not isnull(adorLinea("DENCAMPO2").value) and adorLinea("DENCAMPO2").value<>"" then%>
				<tr>
				<td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=adorLinea("DENCAMPO2").value%></div></td>
				<td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=adorLinea("VALOR2").value%></div></td>
				</tr>
				<%end if%>
				<%if not  (adorAtrL is nothing) then 
				while  not adorAtrL.EOF%>
					<tr>
					  <td class="tablasubrayadaright"><div style="width:100px;height:auto;overflow:hidden"><%=adorAtrL("DEN").value%></div></td>
					<%if adorAtrL("TIPO").value=1 then%> 
					   <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=adorAtrL("VALOR_TEXT").value%></div></td>
					<%end if%>
					<%if adorAtrL("TIPO").value=2 then%> 
					   <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=visualizacionNumero(adorAtrL("VALOR_NUM").value,decimalfmt,thousanfmt,precisionfmt)%></div></td>
					<%end if%>
					<%if adorAtrL("TIPO").value=3 then%>
					   <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=visualizacionFecha(adorAtrL("VALOR_FEC").value,datefmt)%></div></td>
					<%end if%>
					<%if adorAtrL("TIPO").value=4 then%>
					  <%if isnull(adorAtrL("VALOR_BOOL").value) then%>
					    <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;</div></td>
					  <%elseif adorAtrL("VALOR_BOOL").value=0 then%>
					    <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=den(53)%></div></td>
					  <%else%>
					    <td class="tablasubrayadaleft"><div style="width:500px;height:auto;overflow:hidden">&nbsp;<%=den(54)%></div></td>
					  <%end if
					end if%>		  
					  </tr>
					  <%adorAtrL.movenext
				wend
				adorAtrL.close
				end if%>
				
				<%if not isnull(adorLinea("ADJUNTOS").value) then
					sAdjuntosLinea=""
					set oLinea = oRaiz.Generar_CLinea()
					oLinea.ID = adorLinea("LINEAID").value
					set oAdjuntos = oLinea.cargarAdjuntos(CiaComp,false)
					if oAdjuntos.count > 0 then
						for each oAdjunto in oAdjuntos 
							sAdjuntosLinea=sAdjuntosLinea & oAdjunto.Nombre & " ; "
						next
						sAdjuntosLinea=mid(sAdjuntosLinea,1,len(sAdjuntosLinea)-3)
				%>
						<tr>
						<td class="tablasubrayadaright">
                            <div style="width:100px;height:auto;overflow:hidden"><%=den(49)%>
                                <%if isempty(oLinea.ObsAdjun) then%>
                                    <a href='javascript:void(null)' onclick='verAdjuntosLinea(<%=oOrden.id%>,<%=oLinea.ID%>,"")'><img border = 0 src= '../images/clip.gif'></A>
                                <%else%>
                                    <a href='javascript:void(null)' onclick='verAdjuntosLinea(<%=oOrden.id%>,<%=oLinea.ID%>,<%=nulltostr(oLinea.ObsAdjun)%>)'><img border = 0 src= '../images/clip.gif'></A>
                                <%end if%>
                            </div>
						</td>
						<td class="tablasubrayadaleft"><div style="width:400px;height:auto;overflow:hidden">&nbsp;<%=sAdjuntosLinea%></div></td>
						</tr>
					<%
						set oAdjuntos=nothing
					end if
					%>
				<%end if%>
				
				</table></td></tr>
			<%end if%>


			<%
			if fila="filaImpar" then
				fila ="filaPar"
			else
				fila ="filaImpar"
			end if
			
			
			adorLinea.movenext
		wend
        adorLinea.close	
    end if	
	set adorLinea=nothing
	%>
		</table>
		<br>	
		<br>	
		<%
		
	if oDestinos.count>0 then%>

		<table width="100%">
			<tr>
				<td colspan="7" class="cabecera"><%=den(31)%></td>
			</tr>
			<tr>
				<td class="cabecera"><%=den(24)%></td>
				<td class="cabecera"><%=den(25)%></td>
				<td class="cabecera"><%=den(26)%></td>
				<td class="cabecera"><%=den(27)%></td>
				<td class="cabecera"><%=den(28)%></td>
				<td class="cabecera"><%=den(29)%></td>
				<td class="cabecera"><%=den(30)%></td>
			</tr>

	<%
	fila = "filaImpar"
	for each oDest in oDestinos	%>

			<tr>
				<td class="<%=fila%>"><%=oDest.cod%></td>
				<td class="<%=fila%>"><%=oDest.den%></td>
				<td class="<%=fila%>"><%=oDest.dir%></td>
				<td class="<%=fila%>"><%=oDest.cp%></td>
				<td class="<%=fila%>"><%=oDest.pob%></td>
				<td class="<%=fila%>"><%=oDest.provi%></td>
				<td class="<%=fila%>"><%=oDest.pai%></td>
		</tr>

	<%

	next
	%>
	</table>
	<%
	end if%>
				</td>
			</tr>
		</table>
	<br>	
	<br>	
<%
	set oDestinos = nothing
	ador.movenext
	wend%>
	<%
	ador.close
	set ador = nothing
	'SI HAY PEDIDO SE  COMPRUEBA SI SE IMPRIME LA POLITICA DE PEDIDOS
	if oRaiz.Sesion.Parametros.ImprimirPoliticaPedidos then
	'Linea de separacion
	%>
		<hr class="P_acep_pedidos"><br>
		
	<%
		Response.Write oRaiz.PoliticaAceptacionPedidos(Idioma)
	end if
end if


set oRaiz=nothing
	
%>
<br>
<center><input id="cmdImprimir" style="font-size:15px;font-weight:bold;" type="image" src="<%=APPLICATION("RUTASEGURA")%>/script/images/impresora.gif" onclick="javascript:Imprimir()"></center>
</body>
</html>
