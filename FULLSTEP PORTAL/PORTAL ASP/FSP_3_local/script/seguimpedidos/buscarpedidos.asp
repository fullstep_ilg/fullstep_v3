﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/formatos.asp"-->

<%
''' <summary>
''' Busca los pedidos del usuario
''' </summary>
''' <remarks>Llamada desde: seguimpedidos\pedidos11.asp ; Tiempo máximo: 0,2</remarks>

datefmt = Request.Cookies("USU_DATEFMT")	
Idioma = Request("Idioma")
PedidosAbiertos= Request("PedidosAbiertos")

sEstados=""
set oRaiz=validarUsuario(Idioma,true,true,0)

ciacomp=oRaiz.Sesion.CiaComp
Prove = clng(oRaiz.Sesion.CiaId)
set oProve = oraiz.DevolverProveedorEnCiaComp(Idioma,Prove,CiaComp)

dim varEquiv
set adorEquiv=oRaiz.ObtenerMonedaCentral(CiaComp)
if not adorEquiv is nothing then
	varEquiv=adorEquiv("EQUIV").value
	sMonPortal = adorEquiv("MON_PORTAL").value
	adorEquiv.close
else
		set oRaiz = nothing
        
		%>
		<html>
		<body onload="window.open('<%=application("RUTASEGURA")%>script/errormonedas.asp?Idioma=<%=Idioma%>','default_main')">
		</body>
		</html>
		<%
		Response.End 
end if
set adorEquiv=nothing

If Request("busq")<>"" Then strBusqueda=oRaiz.Desencriptar(hexDecode(Request("busq")))

if Request("opcTodos")="-1" Or strBusqueda="todos"  Then
	txtCodExt = null
	txtArtInt=null
	txtDenArt=null
	txtPedidoProve = null
	est = 60
	txtFechaDesde=null
	txtFechaHasta=null
	txtDesdeId = null
	txtRef=null
	icompleta=0
	irecep=0
	ipend=0
    iAnyo=null	
    txtCesta=null
    txtNPed=null
    txtCCoste=null
    txtPPresup=null
    iAnyoAbierto=null
    txtCestaAbierto=null
    txtNPedAbierto = null

    strBusqueda=est & "##" & txtArtInt & "##" & txtDenArt & "##" & txtCodExt & _
        "##" & Year(txtFechaDesde) & "##" & Month(txtFechaDesde) & "##" & Day(txtFechaDesde) & _
        "##" & Year(txtFechaHasta) & "##" & Month(txtFechaHasta) & "##" & Day(txtFechaHasta) & _
        "##" & txtPedidoProve & "##" & chkEst6 & "##" & txtRef & _
        "##" & iCompleta & "##" & iEmpresa & "##" & irecep & "##" & ipend & _
        "##" & iAnyo & "##" & txtCesta & "##" & txtNPed & "##" & txtCCoste & "##" & txtPPresup & "##" & sEstados & _
        "##" & iAnyoAbierto & "##" & txtCestaAbierta & "##" & txtNPedAbierto 
ElseIf strBusqueda<>"" Then
    arrbusq=Split(strBusqueda,"##")
    If arrbusq(0)<>"" Then est=CInt(arrbusq(0))
    txtArtInt=vacio2null(arrbusq(1))
    txtDenArt=vacio2null(arrbusq(2))
    txtCodExt=vacio2null(arrbusq(3))
    If arrbusq(4)="" Then
        txtFechaDesde=vacio2null(arrbusq(4))
    Else
        txtFechaDesde=DateSerial(arrbusq(4), arrbusq(5), arrbusq(6))
    End If
    If arrbusq(7)="" Then
        txtFechaHasta=vacio2null(arrbusq(7))
    Else
        txtFechaHasta=DateSerial(arrbusq(7), arrbusq(8), arrbusq(9))
    End If
    txtPedidoProve=vacio2null(arrbusq(10))
    If arrbusq(11)<>"" Then chkEst6=CInt(arrbusq(11))
    txtRef=vacio2null(arrbusq(12))
    If arrbusq(13)<>"" Then iCompleta=CInt(arrbusq(13))
    If arrbusq(14)<>"" Then iEmpresa=CInt(arrbusq(14))
    If arrbusq(15)<>"" Then irecep=CInt(arrbusq(15))
    If arrbusq(16)<>"" Then ipend=CInt(arrbusq(16))
    If arrbusq(17)<>"" Then iAnyo=CInt(arrbusq(17))
    If arrbusq(18)<>"" Then txtCesta=CInt(arrbusq(18))
    If arrbusq(19)<>"" Then txtNPed=CInt(arrbusq(19))
    If arrbusq(20)<>"" Then txtCCoste=vacio2null(arrbusq(20))
    If arrbusq(21)<>"" Then txtPPresup=vacio2null(arrbusq(21))
    If arrbusq(22)<>"" then sEstados=arrbusq(22)
    If arrbusq(23)<>"" then iAnyoAbierto=arrbusq(23)
    If arrbusq(24)<>"" then txtCestaAbierta=arrbusq(24)
    If arrbusq(25)<>"" then txtNPedAbierto=arrbusq(25)
Else
	txtCodExt = vacio2null(request("txtCodExt"))
	txtArtInt=vacio2null(request("txtArtInt"))
	txtDenArt=vacio2null(request("txtDenArt"))
	txtPedidoProve = vacio2null(request("txtPedidoProve"))

    if PedidosAbiertos = 1 then
        for each estado in Request("chkAbiertos")
            sEstados=sEstados & estado & ","
        next
        if sEstados<>"" then
            sEstados = left(sEstados,len(sEstados)-1)
        end if
    else
        chkEst0=clng(null2zero(vacio2null(request("chkEst0"))))
	    chkEst1=clng(null2zero(vacio2null(request("chkEst1"))))
	    chkEst2=clng(null2zero(vacio2null(request("chkEst2"))))
	    chkEst3=clng(null2zero(vacio2null(request("chkEst3"))))
	    chkEst4=clng(null2zero(vacio2null(request("chkEst4"))))
	    chkEst5=clng(null2zero(vacio2null(request("chkEst5"))))
	    est = chkEst1 or chkEst2 or chkEst3 or chkEst4 or chkEst5 or chkEst0
	    chkEst6 = Request("chkEst6")
	    If isempty(chkEst6) then
		    chkEst6 = 0
	    else
		    chkEst6 = 1
	    end if
	    'Calculo el parametro recep que tiene que ser 1 cuando pido las penditentes de recepcionar
        if chkEst0=0 then
		    ipend=0
	    else
		    ipend=1
	    end if
	
	    'Calculo el parametro recep que tiene que ser 1 cuando pido las penditentes de recepcionar
        if chkEst1=0 then
		    if chkEst2<>0 then
			    irecep=1
		    else
			    irecep=0
		    end if
	    else
		    irecep=1
	    end if
	    if est=0 and chkEst6=0 then
		    est=380
	    end if
    end if
	
	txtFechaDesde=Fecha(vacio2null(request("txtFechaDesde")),datefmt)
	
	txtFechaHasta=Fecha(vacio2null(request("txtFechaHasta")),datefmt)
	txtDesdeId = vacio2null(request("txtDesdeId"))
	txtRef=vacio2null(request("txtRef"))
	
	if (request("chkCompleta"))="on" then
		icompleta=1
	else
		icompleta=0
	end if
	if not isnull(vacio2null(Request ("lstEmpresa"))) then
		iEmpresa = clng(Request ("lstEmpresa"))
	else
	    if not isnull(vacio2null(Request ("HidEmpresa"))) then
		    iEmpresa = clng(Request ("HidEmpresa"))
	    else
		    iEmpresa = null
	    end if
	end if
    'Pedidos Estándar
    if not isnull(vacio2null(Request ("lstAnyo"))) then
		iAnyo = clng(Request ("lstAnyo"))
	else
		iAnyo = null
	end if
    txtCesta = vacio2null(request("txtCesta"))
    txtNPed = vacio2null(request("txtNPed"))

    'Pedidos Abiertos
    if not isnull(vacio2null(Request ("lstAnyoAbierto"))) then
		iAnyoAbierto = clng(Request ("lstAnyoAbierto"))
	else
		iAnyoAbierto = null
	end if
    txtCestaAbierto = vacio2null(request("txtCestaAbierto"))
    txtNPedAbierto = vacio2null(request("txtNPedAbierto"))


    if not isnull(vacio2null(Request ("lstCentroCoste"))) then
		txtCCoste = vacio2null(Request ("lstCentroCoste"))
	else
		txtCCoste = null
	end if
    if not isnull(vacio2null(Request ("lstPPresup"))) then
		txtPPresup = vacio2null(Request ("lstPPresup"))
	else
		txtPPresup = null
	end if

    strBusqueda=est & "##" & txtArtInt & "##" & txtDenArt & "##" & txtCodExt & _
        "##" & Year(txtFechaDesde) & "##" & Month(txtFechaDesde) & "##" & Day(txtFechaDesde) & _
        "##" & Year(txtFechaHasta) & "##" & Month(txtFechaHasta) & "##" & Day(txtFechaHasta) & _
        "##" & txtPedidoProve & "##" & chkEst6 & "##" & txtRef & _
        "##" & iCompleta & "##" & iEmpresa & "##" & irecep & "##" & ipend & _
        "##" & iAnyo & "##" & txtCesta & "##" & txtNPed & "##" & txtCCoste & "##" & txtPPresup & "##" & sEstados & _
        "##" & iAnyoAbierto & "##" & txtCestaAbierto & "##" & txtNPedAbierto
end if
lstProve=oProve.Cod
strBusqueda=hexEncode(oRaiz.Encriptar(strBusqueda))
	
set oOrdenes = oRaiz.generar_COrdenes()

%>
<script>

p=window.parent.frames["fraSeguimClient"]
function init()
{
<%

    set paramGen = oRaiz.DevolverParamGSPedidos(CiaComp)
	pedDirectoEnvioEmail = paramGen(1)
	if pedDirectoEnvioEmail then
		set ador = oOrdenes.BuscarTodasOrdenes(CiaComp, Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"), Est, 0, null, null, txtArtInt,txtDenArt,txtCodExt, txtFechaDesde,txtFechaHasta, lstProve, null,  txtPedidoProve, iAnyo,txtCesta,txtNPed,null, chkEst6,txtRef,icompleta,iEmpresa,null,null,null,null,null,ipend,irecep,txtCCoste,txtPPresup,PedidosAbiertos,sEstados, iAnyoAbierto,txtCestaAbierto,txtNPedAbierto)
	else
		set ador = oOrdenes.BuscarTodasOrdenes(CiaComp, Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"), Est, 0, null, null, txtArtInt,txtDenArt,txtCodExt, txtFechaDesde,txtFechaHasta, lstProve, null,  txtPedidoProve, iAnyo,txtCesta,txtNPed,null, chkEst6,txtRef,icompleta,iEmpresa,null,null,null,null,1,ipend,irecep,txtCCoste,txtPPresup,PedidosAbiertos,sEstados, iAnyoAbierto,txtCestaAbierto,txtNPedAbierto)
	end if

%>

p.ordenes = new Array()
<%
if not ador is nothing then
	while not ador.eof
%>
		p.anyadirOrden(<%=ador("id").value%>, <%=ador("anyo").value%>, <%=ador("numpedido").value%>, <%=ador("numorden").value%>, '<%=JSText(ador("NUMEXT").value)%>', '<%=JSText(ador("PROVECOD").value)%>', '<%=JSText(ador("PROVEDEN").value)%>', <%if not isnull(ador("FECHA").value) then%> new Date (<%=year(ador("FECHA").value)%>,<%=month(ador("FECHA").value)-1%>,<%=day(ador("FECHA").value)%>)<%else%>null<%end if%>, <%=ador("EST").value%>, <%if not isnull(ador("FECEST").value) then%>new Date (<%=year(ador("FECEST").value)%>,<%=month(ador("FECEST").value)-1%>,<%=day(ador("FECEST").value)%>)<%else%>null<%end if%>, '<%=JSAlertText(ador("OBS").value)%>',<%=JSNum(ador("IMPORTE").value*ador("CAMBIO").value)%>,<%if ador("INCORRECTA").value = 1 then%>true<%else%>false<%end if%>,<%=ador("PEDIDO")%>,'<%=JSText(ador("REFERENCIA").value)%>',<%if not isnull(ador("ADJUNTOS").value) then%><%=ador("ADJUNTOS").value%><%else%>0<%end if%>,'<%=JSText(ador("APROVISIONADOR").value)%>','<%=JSText(ador("PER").value)%>','<%=JSText(ador("MON").value)%>','<%=JSText(ador("DENEMPRESA").value)%>','<%=JSText(ador("NOMRECEPTOR").value)%>','<%=JSText(ador("COD_PROVE_ERP").value)%>','<%=JSText(ador("RECEPTOR").value)%>', <%=JSNum(ador("EMPRESA").value)%>, '<%=JSText(ador("OBS").value)%>',<%=ador("acep_prove").value%>,<%=ador("abono").value%>, <%=JSDate(ador("PED_ABIERTO_FECINI").value)%>, <%=JSDate(ador("PED_ABIERTO_FECFIN").value)%>)
<%	
		ador.movenext
	wend
	ador.close
	set ador = nothing
end if

set oRaiz=nothing

%>

p.ultBusqueda='<%=strBusqueda %>';
p.mostrarOrdenes()
}
</SCRIPT>

<HTML>
<HEAD>
 <title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</HEAD>
<BODY onload="init()">




<P>&nbsp;</P>

</BODY>
</HTML>

