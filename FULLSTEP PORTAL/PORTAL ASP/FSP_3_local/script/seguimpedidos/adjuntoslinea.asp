﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/comboanyos.asp"-->
<%
''' <summary>
''' Muestra los adjuntos de la linea de pedido
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos11dir.asp ; Tiempo máximo: 0,2</remarks>


Idioma = Request.Cookies("USU_IDIOMA")

if Idioma = "" then
	Idioma = "SPA"
end if

dim oRaiz	
set oRaiz=validarUsuario(Idioma,true,false,0)

dim Den
den = devolverTextos(Idioma,108)


'obtiene los formatos del usuario
decimalfmt=request.Cookies ("USU_DECIMALFMT")
thousandfmt=Request.Cookies ("USU_THOUSANFMT")
precisionfmt=cint(Request.Cookies ("USU_PRECISIONFMT"))
datefmt=Request.Cookies("USU_DATEFMT") 

CiaComp=oRaiz.Sesion.CiaComp

set oLinea = oRaiz.Generar_CLinea()

oLinea.ID = request("Linea")
oLinea.ObsAdjun=Request("Obs")

%>
<html>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/formatos.js"></script>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><%=title%></title>

</head>
<body>
<h1><%=den(9)%></h1>

<script>
/*
''' <summary>
''' Descarga un adjunto
''' </summary>
''' <param name="id">id de input adjunto</param> 
''' <remarks>Llamada desde: N inputs de esta pantalla ; Tiempo máximo: 0</remarks>*/
function downloadAdjunto(id)
{
f = document.forms["frmAdjuntos"]

Adjunto = f.elements["txtAdjId_" + id].value
nombre  = f.elements["txtAdjNombre_" + id].value
datasize = f.elements["txtAdjTamanyo_" + id].value

if (window.opener.name== "fraPedidosClient")
	sTarget = "fraPedidosServer"
else
	sTarget = "fraSeguimServer"
window.open("<%=application("RUTASEGURA")%>script/seguimpedidos/downloadadjunto.asp?download=1&Adjun=" + Adjunto + "&Orden=<%=request("Orden")%>&linea=<%=request("Linea")%>&nombre=" + nombre + "&datasize=" + datasize + "&tipo=3",sTarget)

}

</script>

<form name="frmAdjuntos" id="frmAdjuntos">

<%
set oAdjuntos = oLinea.cargarAdjuntos(CiaComp,false)
if oAdjuntos.count > 0 or oLinea.ObsAdjun<>"" then
%>

<table class="cajetin" style="width:100%">
<tr>
  <td class=sombreado>
	<nobr><%=den(11)%></nobr>
  </td>
</tr>

<tr>
 <td>
<table name="tblDatos" id="tblDatos" style="width:100%">
<tr><td>
<div name="divObs" id="divObs" style="height:55px;overflow-y:scroll;border=1px solid #b2b2b2">
	<%=oLinea.ObsAdjun%>
</div>
</td></tr>

<tr><td>
<table name="tblAdjuntos" id="tblAdjuntos" width="100%">
	<tr>
		<td class="cabecera">
			<%=den(3)%>
		</td>
		<td class="cabecera">
			<%=den(4)%>
		</td>
		<td class="cabecera">
			<%=den(5)%>
		</td>
		<td class="cabecera">
			&nbsp;
		</td>
	</tr>
<%
fila = "filaImpar"

else
%>
<p class="error">	
	<%=den(10)%>
</p>

<%
end if
if oAdjuntos.count=0 then
%>
	<tr><td>&nbsp;</td></tr>

<%
else
for each oAdjun in oAdjuntos%>
	<tr>
		<td class="<%=fila%>">
			<input type="hidden" name="txtAdjId_<%=oAdjun.id%>" id="txtAdjId_<%=oAdjun.id%>" value="<%=oAdjun.id%>">
			<input type="hidden" name="txtAdjNombre_<%=oAdjun.id%>" id="txtAdjNombre_<%=oAdjun.id%>" value="<%=oAdjun.nombre%>">
			<input type="hidden" name="txtAdjTamanyo_<%=oAdjun.id%>" id="txtAdjTamanyo_<%=oAdjun.id%>" value="<%=oAdjun.dataSize%>">
			<a href="javascript:downloadAdjunto(<%=oAdjun.id%>)"><%=oAdjun.nombre%></A>
		</td>
		<td class="<%=fila%> numero">
			<%=visualizacionnumero(oAdjun.dataSize/1024,decimalfmt,thousandfmt,0)%>&nbsp;kb.
		</td>
		<td class="<%=fila%>">
			<%if oAdjun.Comentario<>"" then%>
			<a href="javascript:alert(&quot;<%=JSText(oAdjun.Comentario)%>&quot;)"><%=den(8)%></a>
			<%end if%>
		</td>
		<td class="<%=fila%>">
			<a href="javascript:downloadAdjunto(<%=oAdjun.id%>)"><img border="0" src="../solicitudesoferta/images/abrir.gif" WIDTH="15" HEIGHT="13"></a>
		</td>
		<%if fila ="filaImpar" then
			fila = "filaPar"
		else
			fila = "filaImpar"
		end if
		%>
	</tr>
<%next%>
</td></tr></table>

</table>

<%end if%>

</td></tr></table>


<br>
<center>
<a href="#" onclick="window.close()"><%=den(6)%></a>
</center>
</form>
</body>
</html>
<%
set oAdjuntos = nothing
set oLinea = nothing

set oRaiz = nothing
%>