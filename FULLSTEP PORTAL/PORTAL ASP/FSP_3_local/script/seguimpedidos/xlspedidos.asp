﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/formatos.asp"-->

<%
''' <summary>
''' Exporta a excel los pedidos
''' </summary>
''' <remarks>Llamada desde: seguimpedidos\seguimclt.asp     seguimpedidos\seguimclt41.asp ; Tiempo máximo: 0,2</remarks>

OldTimeOut = Server.ScriptTimeout 
Server.ScriptTimeout=600

datefmt = Request.Cookies("USU_DATEFMT")	
Idioma = Request.Cookies("USU_IDIOMA")
PedidosAbiertos=Request("PedidosAbiertos")

set oRaiz=validarUsuario(Idioma,true,true,0)

ciacomp=oRaiz.Sesion.CiaComp

CodProve = oRaiz.Sesion.CiaCodGs
Prove = clng(oRaiz.Sesion.CiaId)
set oProve = oraiz.DevolverProveedorEnCiaComp(Idioma,Prove,CiaComp)



if Request("opcTodos")<>"-1" then
	txtCodExt=vacio2null(request("txtCodExt"))
	txtArtInt=vacio2null(request("txtArtInt"))
	txtDenArt=vacio2null(request("txtDenArt"))
	txtPedidoProve = vacio2null(request("txtPedidoProve"))
	
	chkEst0=clng(null2zero(vacio2null(request("chkEst0"))))
	chkEst1=clng(null2zero(vacio2null(request("chkEst1"))))
	chkEst2=clng(null2zero(vacio2null(request("chkEst2"))))
	chkEst3=clng(null2zero(vacio2null(request("chkEst3"))))
	chkEst4=clng(null2zero(vacio2null(request("chkEst4"))))
	chkEst5=clng(null2zero(vacio2null(request("chkEst5"))))
	est = chkEst1 or chkEst2 or chkEst3 or chkEst4 or chkEst5 or chkEst0
	chkEst6 = Request("chkEst6")
	If isempty(chkEst6) then
		chkEst6 = 0
	else
		chkEst6 = 1
	end if
'Calculo el parametro recep que tiene que ser 1 cuando pido las penditentes de recepcionar
    if chkEst0=0 then
		ipend=0
	else
		ipend=1
	end if
	
	'Calculo el parametro recep que tiene que ser 1 cuando pido las penditentes de recepcionar
    if chkEst1=0 then
		if chkEst2<>0 then
			irecep=1
		else
			irecep=0
		end if
	else
		irecep=1
	end if		

	if est=0 and chkEst6=0 then
		est=380
	end if
	
	lstProve=oProve.Cod
	txtFechaDesde=Fecha(vacio2null(request("txtFechaDesde")),datefmt)
	
	txtFechaHasta=Fecha(vacio2null(request("txtFechaHasta")),datefmt)
	txtDesdeId = vacio2null(request("txtDesdeId"))
	
	txtRef=vacio2null(request("txtRef"))
	
	if (request("chkCompleta"))="on" then
		icompleta=1
	else
		icompleta=0
	end if
	if not isnull(vacio2null(Request ("lstEmpresa"))) then
		iEmpresa = clng(Request ("lstEmpresa"))
	else
	    if not isnull(vacio2null(Request ("HidEmpresa"))) then
		    iEmpresa = clng(Request ("HidEmpresa"))
	    else
		    iEmpresa = null
	    end if
	end if

    'Pedidos Abiertos
    if not isnull(vacio2null(Request ("lstAnyoAbierto"))) then
		iAnyoAbierto = clng(Request ("lstAnyoAbierto"))
	else
		iAnyoAbierto = null
	end if
    txtCestaAbierto = vacio2null(request("txtCestaAbierto"))
    txtNPedAbierto = vacio2null(request("txtNPedAbierto"))

    sEstados=""
    if PedidosAbiertos = 1 then
        for each estado in Request("chkAbiertos")
            sEstados=sEstados & estado & ","
        next
        if sEstados<>"" then
            sEstados = left(sEstados,len(sEstados)-1)
        end if
    end if

else
	txtCodExt=null
	txtArtInt=null
	txtDenArt=null
	txtPedidoProve = null
	est = 60
	lstProve=oProve.Cod
	txtFechaDesde=null
	txtFechaHasta=null
	txtDesdeId = null
	txtRef=null
	icompleta=0
	iEmpresa = null
	ipend=0
	irecep=0
end if



anyo = request("anyo")
pedido= request("pedido")
orden= request("orden")
set oXLS = oRaiz.Generar_CExcel()

strFECHAHORA="FULLSTEP_EP_" & day(now()) & "_" & month(now()) & "_" & year(now()) & "_" & hour(now()) & "_" & minute(now()) & "_" & second(now()) & ".xls"

s=""
lowerbound=65
upperbound=90
randomize
for i = 1 to 10
	s= s & chr(Int((upperbound - lowerbound + 1) * Rnd + lowerbound))
next	



sPath = Application ("CARPETAUPLOADS") & "\" & s & "\" & strFECHAHORA

Equiv = 1

oXLS.GenerarExcelOrdenes Idioma,ciaComp,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"), est,txtArtInt,txtDenArt,txtCodExt,txtFechaDesde,txtFechaHasta,lstProve,txtPedidoProve,chkEst6, anyo, pedido, orden, sPath,txtRef,icompleta,iEmpresa, ipend,irecep,PedidosAbiertos,sEstados,iAnyoAbierto,txtCestaAbierto,txtNPedAbierto

datasize = oRaiz.DataSizeFile(sPath)

svPath = s

Server.ScriptTimeout=OldTimeout
%>

<script>

var p


top.winEspera.close()


window.open("<%=Application("RUTASEGURA")%>script/common/download.asp?path=<%=server.urlencode(svPath)%>&nombre=<%=server.urlencode(strFECHAHORA)%>&datasize=<%=server.urlencode(dataSize)%>","_blank","top=100,left=150,width=630,height=300,location=no,menubar=yes,resizable=yes,scrollbars=auto,toolbar=no")


</script>

</BODY>
</HTML>
<%

set oRaiz = nothing

Response.End
%>

