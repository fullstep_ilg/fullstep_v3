﻿<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/comboanyos.asp"-->
<%
''' <summary>
''' Muestra los adjuntos del orden de pedido
''' </summary>
''' <remarks>Llamada desde: pedidos\pedidos11.asp       pedidos\pedidos11dir.asp    seguimpedidos\seguimclt.asp; Tiempo máximo: 0,2</remarks>

Idioma = Request.Cookies("USU_IDIOMA")

if Idioma = "" then
	Idioma = "SPA"
end if

dim oRaiz	
set oRaiz=validarUsuario(Idioma,true,false,0)	

dim den

den = devolverTextos(Idioma,108)


'obtiene los formatos del usuario
decimalfmt=request.Cookies ("USU_DECIMALFMT")
thousandfmt=Request.Cookies ("USU_THOUSANFMT")
precisionfmt=cint(Request.Cookies ("USU_PRECISIONFMT"))
datefmt=Request.Cookies("USU_DATEFMT") 

CiaComp=oRaiz.Sesion.CiaComp

set oOrden = oRaiz.Generar_COrden()

oOrden.ID = request("Orden")

%>
<html>
<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script SRC="../common/formatos.js"></script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<% 
    Idioma = Request.Cookies("USU_IDIOMA")
    
%>
<title><%=title%></title>

</head>
<body>
<h1><%=den(1)%></h1>

<script>
/*
''' <summary>
''' Descarga un adjunto
''' </summary>
''' <param name="id">id de input adjunto</param> 
''' <remarks>Llamada desde: N inputs de esta pantalla ; Tiempo máximo: 0</remarks>*/
function downloadAdjunto(id)
{
f = document.forms["frmAdjuntos"]
Adjunto = f.elements["txtAdjId_" + id].value
nombre  = f.elements["txtAdjNombre_" + id].value
datasize = f.elements["txtAdjTamanyo_" + id].value
if (window.opener.name== "fraPedidosClient")
	sTarget = "fraPedidosServer"
else
	sTarget = "fraSeguimServer"
window.open("<%=application("RUTASEGURA")%>script/seguimpedidos/downloadadjunto.asp?download=1&Adjun=" + Adjunto + "&orden=<%=request("Orden")%>&nombre=" + nombre + "&datasize=" + datasize + "&tipo=2",sTarget)
}

</script>

<form name="frmAdjuntos" id="frmAdjuntos">

<%
set oAdjuntos = oOrden.cargarAdjuntos(CiaComp,false)
if oAdjuntos.count > 0 then
%>
<table class="cajetin" style="width:100%">
<tr>
  <td class=sombreado>
	<nobr><%=den(11)%></nobr>
  </td>
</tr>

<tr>
 <td>
 <table name="tblAdjuntos" id="tblAdjuntos" style="width:100%">
	<tr>
		<td class="cabecera">
			<%=den(3)%>
		</td>
		<td class="cabecera">
			<%=den(4)%>
		</td>
		<td class="cabecera">
			<%=den(5)%>
		</td>
		<td class="cabecera">
			&nbsp;
		</td>
	</tr>
<%
fila = "filaImpar"

else
%>
<p class="error">	
	<%=den(7)%>
</p>

<%
end if
for each oAdjun in oAdjuntos%>
	<tr>
		<td class="<%=fila%>">
			<input type="hidden" name="txtAdjId_<%=oAdjun.id%>" id="txtAdjId_<%=oAdjun.id%>" value="<%=oAdjun.id%>">
			<input type="hidden" name="txtAdjNombre_<%=oAdjun.id%>" id="txtAdjNombre_<%=oAdjun.id%>" value="<%=oAdjun.nombre%>">
			<input type="hidden" name="txtAdjTamanyo_<%=oAdjun.id%>" id="txtAdjTamanyo_<%=oAdjun.id%>" value="<%=oAdjun.dataSize%>">
			<a href="javascript:downloadAdjunto(<%=oAdjun.id%>)"><%=oAdjun.nombre%></A>
		</td>
		<td class="<%=fila%> numero">
			<%=visualizacionnumero(oAdjun.dataSize/1024,decimalfmt,thousandfmt,0)%>&nbsp;Kb.
		</td>
		<td class="<%=fila%>">
			<%if oAdjun.Comentario<>"" then%>
			<a href="javascript:alert(&quot;<%=JSText(oAdjun.Comentario)%>&quot;)"><%=den(8)%></a>
			<%end if%>
		</td>
		<td class="<%=fila%>">
			<a href="javascript:downloadAdjunto(<%=oAdjun.id%>)"><img border="0" src="../solicitudesoferta/images/abrir.gif" WIDTH="15" HEIGHT="13"></a>
		</td>
		<%if fila ="filaImpar" then
			fila = "filaPar"
		else
			fila = "filaImpar"
		end if
		%>
	</tr>


<%next%>
</td></tr></table>
</table>
<br>
<center>
<br>
<p align=center><input class=button type=button name=cmdCerrar value='<%=den(6)%>' onclick='window.close()'></P>

</center>
</form>
</body>
</html>
<%
set oAdjuntos = nothing
set oOrden = nothing

set oRaiz = nothing
%>