﻿<!DOCTYPE HTML PUBLIC >
<%@  language="VBScript" %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<!--#include file="../common/comboanyos.asp"-->
<!--#include file="../common/fsal_1.asp"-->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="GENERATOR" content="Microsoft Visual Studio 6.0">
    <link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
    <script src="../common/calendar.asp"></script>
    <script src="../common/menu.asp"></script>
    <script src="../common/formatos.js"></script>
    <script language="JavaScript" src="../common/ajax.js"></script>
    <!--#include file="../common/fsal_3.asp"-->
</head>
<style>
    #divBusqueda
    {
        position: absolute;
        top: 70;
        left: 0;
        height: auto;
        width: 95%;
        visibility: hidden;
    }
    #divResultado
    {
        position: absolute;
        top: 70;
        left: 0;
        height: auto;
        width: 95%;
        visibility: hidden;
    }
</style>

<body topmargin="0" leftmargin="0" onload="init()">
    <% 
''' <summary>
''' Mostrar la pantalla de busqueda de pedidos para hacer su seguimiento
''' </summary>
''' <remarks>Llamada desde: seguimiento.asp; Tiempo máximo: 0</remarks> 

    Idioma = Request("Idioma")
	if Idioma = "" then
		Idioma = "SPA"
	end if
    PedidosAbiertos= Request("PedidosAbiertos")

    strBusqueda = Request("busq")

    AccesoExterno=cbool(Application("ACCESO_SERVIDOR_EXTERNO"))    
    if AccesoExterno then
	    set oRaiz=validarUsuarioAccesoExterno(Idioma,true,true,0,request.cookies("USU_CIACOD"),request.cookies("USU_USUCOD"),request.cookies("USU_SESIONID_EXTERNO"))
    else	
	    set oRaiz=validarUsuario(Idioma,true,true,0)
    end if  

	Set paramgen = oRaiz.Sesion.Parametros
	
	bAccesoFSSM=paramgen.AccesoFSSM
	
	dim den    
	den=devolverTextos(Idioma,45)

	'obtiene los formatos del usuario
	decimalfmt=request.Cookies ("USU_DECIMALFMT")
	thousandfmt=Request.Cookies ("USU_THOUSANFMT")
	precisionfmt=cint(Request.Cookies ("USU_PRECISIONFMT"))
	datefmt=Request.Cookies("USU_DATEFMT") 
	
	CiaComp=oRaiz.Sesion.CiaComp

    set paramPedidos = oRaiz.DevolverParamGSPedidos(oRaiz.Sesion.CiaComp)  

    bActivarMenuAbiertos= oRaiz.ActivadoPedidosAbiertos (oRaiz.Sesion.CiaComp,Idioma) and paramPedidos(3)
		    
	Referencia=oRaiz.DevolverCodPedido(CiaComp,Idioma)       
    if bAccesoFSSM then
        set rsPPresup=oRaiz.DevolverPartidaPresupuestaria(CiaComp,Idioma)
        if not rsPPresup is nothing then
            if rsPPresup.recordcount>0 then
                sPPresupuestaria=rsPPresup("DEN")
            end if
        end if        

        lIdProve = clng(oRaiz.Sesion.CiaId)
        set oCentros=oRaiz.Generar_COrdenes        
        set rsCentros=oCentros.DevolverCentrosCostePedidosProveedor(CiaComp,lIdProve,Idioma)        
        set rsPartidas=oCentros.DevolverPartidasPedidosProveedor(CiaComp,lIdProve,Idioma)
    end if

    'Obtiene las compañías con pedidos
    ciaDen=""
	set oCias = oraiz.DevolverCompaniasConPedidos (Idioma,oRaiz.Sesion.CiaId,cint(Application("PORTAL")),clng(oRaiz.Sesion.CiaComp),Application("PYME"),oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"))
    if paramgen.unaCompradora  and not oCias is nothing then
        set oCia = oCias.item(1)
		ciaDen = oCia.den
		set ocia = nothing
    end if
	
	set oEmpresas = oRaiz.Generar_CEmpresas()
	
	oEmpresas.CargarTodasLasEmpresasDesde CiaComp, Idioma, 0, Application("PYME")

    ScriptInit="function init(){" & Chr(13) & Chr(10) & _
        "if (" & Application("FSAL") & " == 1){" & Chr(13) & Chr(10) & _
        "Ajax_FSALActualizar3();" & Chr(13) & Chr(10) & _
        "}" & Chr(13) & Chr(10) & _
        "if (" & Application("ACCESO_SERVIDOR_EXTERNO") & "==0){document.getElementById(""tablemenu"").style.display = ""block"";}" & Chr(13) & Chr(10) & _
	    "resize();" & Chr(13) & Chr(10) & _
	    "document.getElementById(""divBusqueda"").style.visibility="""
	If strBusqueda="" Then ScriptInit=ScriptInit & "visible" Else ScriptInit=ScriptInit & "hidden"
	ScriptInit=ScriptInit & """;" & Chr(13) & Chr(10) 
    If strBusqueda="" Then 
	    ScriptInit=ScriptInit & "document.getElementById(""divResultado"").style.visibility=""hidden"";" & Chr(13) & Chr(10)
    else
        ScriptInit=ScriptInit & "document.getElementById(""divResultado"").style.visibility=""visible"";" & Chr(13) & Chr(10)
    End if
	If strBusqueda<>"" Then
	    ScriptInit=ScriptInit & "window.open('" &  application("RUTASEGURA") & "script/seguimpedidos/buscarpedidos.asp?Idioma=" & Idioma & "&busq=" & strBusqueda & "&PedidosAbiertos=" & PedidosAbiertos & "','fraSeguimServer');" & Chr(13) & Chr(10)
	End If
    ScriptInit=ScriptInit & "}"

    if bActivarMenuAbiertos then	
        if PedidosAbiertos=0 then		
            sHeader=Den(75)
        else		
            sHeader=Den(76)
        end if	
    else
        sHeader=den(1)
    end if

    if bActivarMenuAbiertos then
        sMostrarPendientes=den(84)
    else
        sMostrarPendientes=den(4)
    end if
    %>
    <script>        dibujaMenu(4)</script>
    <script>

var vdecimalfmt 
var vthousanfmt
var	vprecisionfmt
var vdatefmt

vdecimalfmt='<%=decimalfmt%>'
vthousanfmt='<%=thousandfmt%>'
vprecisionfmt='<%=precisionfmt%>'
vdatefmt='<%=datefmt%>'

var requiereRefresco
var vNormal
var ultBusqueda;
requiereRefresco=true

<%=ScriptInit %>

function resize()
{

}

function nuevaBusqueda()
{
document.getElementById("imgExcel").style.visibility="hidden"
document.getElementById("imgImpresora").style.visibility="hidden"

document.getElementById("divResultado").style.visibility="hidden"
document.getElementById("divBusqueda").style.visibility="visible"
document.forms["frmBusqueda"].txtDesdeId.value=""
document.forms["frmBusqueda"].opcTodos.value=0
}


//''' <summary>
//''' Comprueba q haya parametros
//''' </summary>
//''' <remarks>Llamada desde: frmBusqueda/onsubmit ; Tiempo máximo: 0</remarks>
function validarA()
{ 
f=document.forms["frmBusqueda"]
if (f.opcTodos!=-1)
{

var Empresa
if (f.lstEmpresa){
    Empresa = f.lstEmpresa.options[f.lstEmpresa.selectedIndex].value
}
else {
    Empresa = f.HidEmpresa.value
}
var bNingunCheckOn=true;
var bNoHayPresupSeleccionado=true;
var bNingunCheckAbiertoOn=true;
var bNingunAnyoAbierto=true;


if (f.chkAbiertos){
    for(i=0; i<f.chkAbiertos.length;i++){
        bNingunCheckAbiertoOn=!f.chkAbiertos[i].checked && bNingunCheckAbiertoOn;
    }
}
if (f.lstPPresup){
    bNoHayPresupSeleccionado = f.lstPPresup.options[f.lstPPresup.selectedIndex].value == "";
}
if (f.chkEst0){
    bNingunCheckOn = f.chkEst5.checked==false  && f.chkEst0.checked==false && f.chkEst1.checked==false && f.chkEst2.checked==false && f.chkEst3.checked==false && f.chkEst4.checked==false && f.chkEst6.checked==false
}
if (f.lstAnyoAbierto){
    bNingunAnyoAbierto = f.lstAnyoAbierto.options[f.lstAnyoAbierto.selectedIndex].value == "";
}


if (Empresa == "" && f.txtArtInt.value=="" && f.txtDenArt.value=="" && f.txtPedidoProve.value==""  && bNingunCheckOn && bNingunCheckAbiertoOn &&   f.txtFechaDesde.value=="" && f.txtFechaHasta.value=="" && f.txtCodExt.value=="" && f.txtRef.value=="" && f.lstAnyo.options[f.lstAnyo.selectedIndex].value == "" && bNingunAnyoAbierto && f.txtCesta.value=="" && f.txtNPed.value=="" <%if bAccesoFSSM then%> && f.lstCentroCoste.options[f.lstCentroCoste.selectedIndex].value == "" && bNoHayPresupSeleccionado <%end if %>)
	{     
	alert('<%=JSALERTTEXT(den(43))%>')
	return false
	}
if (f.txtFechaDesde.value!="" && f.txtFechaHasta.value!="")
	{      
	if (str2date(f.txtFechaDesde.value,vdatefmt)>str2date(f.txtFechaHasta.value,vdatefmt))
		{
		alert('<%=JSALERTTEXT(den(37))%>')
		return false
		}
	}  
if (f.txtCesta.value!="")
	{     
	if (isNaN(f.txtCesta.value))
		{
		alert('<%=JSALERTTEXT(den(71))%>')
		return false
		}
	}       
if (f.txtNPed.value!="")
	{      
	if (isNaN(f.txtNPed.value))
		{
		alert('<%=JSALERTTEXT(den(71))%>')
		return false
		}
	}    
}

return true

}



function todos()
{
	f=document.forms["frmBusqueda"]
	f.opcTodos.value=-1
	f.submit()
}


function verAdjuntosOrden(id)
{
   window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/adjuntosorden.asp?Orden=" + id, "_blank", "top=5,left=5,width=600, height=250,directories=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes")
}


function Orden(id, anyo, pedido, numorden, pedidoprove, provecod, proveden, fecemision, est, fecultmov, coment,importe,incorrecta,idPedido,referencia,adjuntos,peticionario,codaprov,mon,empresa,receptor,codErp,codReceptor,idEmpresa,obs,acep,abono,fecini,fecfin)
{
this.id=id
this.anyo = anyo
this.idPedido = idPedido
this.pedido = pedido
this.orden = numorden
this.pedidoprove = pedidoprove
this.provecod = provecod
this.proveden = proveden
this.importe = importe
this.fecemision = fecemision
this.fecini=fecini
this.fecfin=fecfin
this.est = est
this.fecultmov = fecultmov
this.coment = coment
this.expandido = false
this.incorrecta = incorrecta
this.referencia=referencia
this.adjuntos=adjuntos
this.peticionario=peticionario
this.codaprov=codaprov
this.mon=mon
this.empresa = empresa
this.receptor = receptor
this.codErp = codErp
this.codReceptor = codReceptor
this.idEmpresa = idEmpresa
this.obs = obs
this.acep = acep
this.abono = abono
this.lineas = new Array()

}



var ordenes
ordenes = new Array()

function anyadirOrden(id, anyo, pedido, orden, pedidoprove, provecod, proveden, fecemision, est, fecultmov, coment,importe,incorrecta,idPedido,referencia,adjuntos,peticionario,codaprov,mon,empresa,receptor,codErp,codReceptor,idEmpresa,obs,acep,Abono,fecini,fecfin)
{
var i
i = ordenes.length
ordenes[i]=new Orden(id,anyo,pedido,orden, pedidoprove,provecod,proveden,fecemision,est,fecultmov,coment,importe,incorrecta,idPedido,referencia,adjuntos,peticionario,codaprov,mon,empresa,receptor,codErp,codReceptor,idEmpresa,obs,acep,Abono,fecini,fecfin)
}
/*
''' <summary>
''' Expandir una orden
''' </summary>
''' <param name="i">indice del orden a expandir dentro del objeto ordenes</param>
''' <remarks>Llamada desde: seguimclt.asp ; Tiempo máximo: 0,2</remarks>*/
function expandirOrden(i)
{
	if (ordenes[i].lineas.length>0)
		{
		mostrarOrdenes()
		}
	else
		{		
		window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/seguimclt41.asp?Idioma=<%=Idioma%>&CiaComp=<%=CiaComp%>&OrdenId=" + ordenes[i].id + "&Anyo=" + ordenes[i].anyo + "&Pedido=" + ordenes[i].pedido + "&Orden=" + ordenes[i].orden + "&Prove=" + ordenes[i].provecod + "&Referencia=<%=Referencia%>&CiaDen=<%=CiaDen%>&Estado=" + ordenes[i].est + "&busq=" + ultBusqueda + "&PedidosAbiertos=<%=PedidosAbiertos%>", "_self")
		}
}

/*
''' <summary>
''' Mostrar las ordenes
''' </summary>   
''' <remarks>Llamada desde: expandirOrden   aplicarFormatos     envioRegistrado; Tiempo máximo: 0</remarks>*/
function mostrarOrdenes()
{
var str
var i
var j
var idActual
str = "<table class=principal width=100%>"
str += '<tr><td align=left class=subrallado colspan=2><%=den(14)%></td></tr>'
str += '<tr><td align=left>'

idActual=document.forms["frmBusqueda"].txtDesdeId.value

	 
str +="</td>"

str +="<td align=right><a href='javascript:void(null)' onclick='nuevaBusqueda()'><%=den(15)%></a></td>"

if (ordenes.length==0)
	{
	str +="</tr><tr><td align=center class=nodatos><%=den(16)%></td></tr></table>"
	}
else
	{
	str +="</tr><tr><td align=center colspan=2>"
	str += "<TABLE width=100% >"
	str += "	<TR>"
	str += "       <td align=left width=1% class=cabecera>&nbsp;</TD>"
	str += "       <td align=left width=7% class=cabecera><%=den(65)%></TD>"  //Empreda
	str += "       <td align=left width=7% class=cabecera><%=den(24)%></TD>"  //Nºpedido
	<%If paramgen.VerNumeroProveedor and PedidosAbiertos<>0 Then %>
	str += "       <td align=left width=7% class=cabecera><%=den(60)%></TD>"  //Nºpedido (proveedor)
	<%End If %>
	<%if Referencia<>"" then %>
		str += "       <td align=left width=10% class=cabecera><%=Referencia%></TD>"  //referencia
	<%end if%>
	str += "       <td align=left width=8% class=cabecera><%=den(26)%></TD>"  //Fecha de emisión

    <% if PedidosAbiertos=1 then %>
        str += "       <td align=left width=10% class=cabecera><%=den(73)%></TD>"  //Fecha de inicio
        str += "       <td align=left width=10% class=cabecera><%=den(74)%></TD>"  //Fecha de fin
    <% end if %>
	
    str += "       <td align=left width=20% class=cabecera><%=den(27)%></TD>"  //Estado actual
    
	str += "       <td align=left width=9% class=cabecera><%=den(51)%></TD>"  //Fecha de cambio de estado
	str += "       <td align=left width=20% class=cabecera><%=den(52)%></TD>"  //Peticionario
	str += "       <td align=left width=10% class=cabecera><%=den(38)%></TD>"  //Importe
	str += "       <td align=left width=5% class=cabecera><%=den(53)%></TD>"  //Obs
	str += "       <td align=left width=5% class=cabecera><%=den(54)%></TD>"  //Arch.
	str += "       <td align=left width=8% class=cabecera>&nbsp;</TD>"
	str += "       <td align=left width=1% class=cabecera>&nbsp;</TD>"
	str += "       <td align=left width=1% class=cabecera>&nbsp;</TD>"
	str += "	</TR>"
	clase="filaPar"
	for (i=0;i<ordenes.length;i++)
		{
		
		str += "  <TR>"
		str += "     <TD class=" + clase + " align=center><a href='javascript:expandirOrden(" + i + ")'>+</a></TD>"		
		str += "<td align=left class=" + clase + "><table width=100%><tr><td align=left width=100%><a href='javascript:void(null)' onclick='DetalleEmpresa(" + i + ")'>" + ordenes[i].empresa + "</a></td>"
		str += "       <td align=left><a href='javascript:void(null)' onclick='DetalleEmpresa(" + i + ")'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table></TD>"

		str += "       <td align=left class=" + clase + ">" + ordenes[i].anyo + "/" + ordenes[i].pedido + "/" + ordenes[i].orden + "</TD>"
		<%If paramgen.VerNumeroProveedor and PedidosAbiertos<>0  Then %>
		str += "       <td align=left class=" + clase + ">" + ordenes[i].pedidoprove + "</TD>"
		<%End If %>
		<%if Referencia<>"" then %>	
			str += "       <td align=left class=" + clase + "><div style='height:auto;overflow:hidden'>" + ordenes[i].referencia + "<div></TD>"
		<%end if%>
		str += "       <td align=left class=" + clase + ">" + date2str(new Date(ordenes[i].fecemision),vdatefmt) + "</TD>"

        <% if PedidosAbiertos=1 then %>
            str += "       <td align=left class=" + clase + ">" + date2str(new Date(ordenes[i].fecini),vdatefmt) + "</TD>"
            str += "       <td align=left class=" + clase + ">" + date2str(new Date(ordenes[i].fecfin),vdatefmt) + "</TD>"
        <% end if %>
        <% if PedidosAbiertos=0 then %>
		    str += "       <td align=left class=" + clase + ">" + denominacionEstadoOrden(ordenes[i].est,ordenes[i].acep) + "</TD>"
        <% else %>
            str += "       <td align=left class=" + clase + ">" + denominacionEstadoOrdenAbiertos(ordenes[i].est) + "</TD>"
        <% end if %>
		str += "       <td align=left class=" + clase + ">" + date2str(new Date(ordenes[i].fecultmov),vdatefmt) + "</TD>"

		if (ordenes[i].codaprov != "")
		   {
			str += "	   <td align=left class=" + clase + "><table width=100%><tr><td align=left width=100%><a href='javascript:void(null)' onclick='DetallePet(" + i + ")'>" + ordenes[i].peticionario + "</a></td>"
			str += "			<td align=left><a href='javascript:void(null)' onclick='DetallePet(" + i + ")'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table></TD>"  //Peticionario
		   }
		else
		  {
			str += "	   <td align=left class=" + clase + "><table width=100%><tr><td align=left width=100%> </td>"
			str += "			<td align=left></td></tr></table></TD>"  //Peticionario
		  }
		if (ordenes[i].abono=="1")
		{
			str += ""
			str += "       <TD align=right style='background-color:Red;color:white' class=" + clase + ">" + num2str(ordenes[i].importe ,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + ordenes[i].mon + " ABONO" + "</TD>"
		}
		else
		{
			str += ""		
			str += "       <TD align=right class=" + clase + ">" + num2str(ordenes[i].importe ,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + ordenes[i].mon + "</TD>"	
		}	
			
		//str += "       <TD align=right class=" + clase + ">" + num2str(ordenes[i].importe ,vdecimalfmt,vthousanfmt,vprecisionfmt) + " " + ordenes[i].mon + "</TD>"
		
		str += "       <td align=left class=" + clase + ">"   //las observaciones
		if (ordenes[i].coment!="")
			{
			str +="<a href='javascript:void(null)' onclick='alert(ordenes[" + i + "].coment)'><img border = 0 src= '../images/coment.gif'></a>" }
		else
			{str +="&nbsp;"}
		str +="</TD>"
		
		str += "       <td align=left class=" + clase + ">"  //los archivos adjuntos
		if (ordenes[i].adjuntos!=0)
			{
			str+= "<a href='javascript:void(null)' onclick='verAdjuntosOrden(ordenes[" + i + "].id)'><img border = 0 src= '../images/clip.gif'></A>"
			}
		else
			{str +="&nbsp;"}
		str +="</TD>"
		
		str += "       <td align=left class=" + clase + ">"
		switch (ordenes[i].est)
			{
			case 3:
				str +="<a href='javascript:void(null)' onclick='enviar(" + i + ")'><%=den(22)%></a>" 
				break;
			case 4:
			case 5:
			case 6:
				str +="&nbsp;"
				break;
			case 21:
			<%If Not bAccesoFSSM Then%>
				str +="<a href='javascript:void(null)' onclick='aceptar(" + i + ")'><%=den(42)%></a>" 
			<%End If%>
				break;
			}
		str +="</TD>"
		str += "<TD align=center class=" + clase + "><a href='javascript:void(null)' onclick ='exportarPedido(" + ordenes[i].anyo + "," + ordenes[i].pedido + "," + ordenes[i].orden + ")'><IMG border=0 SRC='../images/excelp.gif'></a> </TD>"
		str += "<TD align=center class=" + clase + "><a href='javascript:void(null)' onclick ='imprimirPedido(" + ordenes[i].anyo + "," + ordenes[i].pedido + "," + ordenes[i].orden + ")'><IMG border=0 SRC='../images/impresorap.gif'></a> </TD>"

		str += "	</TR>"

		if (clase=="filaPar")
			{
			clase="filaImpar"
			}
		else
			{
			clase="filaPar"
			}
		
		}
	str += "</table>"
		
		
	str += "</td></tr></table>"
	}
	
	
document.getElementById("divBusqueda").style.visibility="hidden"
document.getElementById("divResultado").innerHTML= str
document.getElementById("divResultado").style.visibility="visible"

document.getElementById("imgExcel").style.visibility="visible"
document.getElementById("imgImpresora").style.visibility="visible"

}




function denominacionEstadoOrden(est,acep)
{
switch(est)
	{
	case 2:
		if (acep==0)
		   return('<%=den(67)%>')
		else
		   return('<%=den(59)%>')
		break;
	case 3:
		return('<%=den(17)%>')
		break;
	case 4:
		return('<%=den(18)%>')
		break;
	case 5:
		return('<%=den(19)%>')
		break;
	case 6:
		return('<%=den(20)%>')
		break;
	case 21:
		return('<%=den(21)%>')
		break;
	}
}

function denominacionEstadoOrdenAbiertos(est)
{
switch(est)
	{
	case 100:
		return('<%=den(80)%>')
		break;
	case 101:
		return('<%=den(81)%>')
		break;
	case 102:
		return('<%=den(82)%>')
		break;
	case 103:
		return('<%=den(83)%>')
		break;
    default:
        return "";
	}
}



				

function aplicarFormatos(vdec,vthou,vprec,vdate)
{
	vdecimalfmt=vdec
	vthousanfmt=vthou
	vprecisionfmt = vprec

	vFechaDesde=str2date(document.forms["frmBusqueda"].txtFechaDesde.value,vdatefmt)
	vFechaHasta=str2date(document.forms["frmBusqueda"].txtFechaHasta.value,vdatefmt)
	vdatefmt = vdate
	document.forms["frmBusqueda"].txtFechaDesde.value=date2str(vFechaDesde,vdatefmt)
	document.forms["frmBusqueda"].txtFechaHasta.value=date2str(vFechaHasta,vdatefmt)
	
	if (document.getElementById("divResultado").style.visibility=="visible")
	{
		mostrarOrdenes()
	}

}
/*
''' <summary>
''' Acepta el pedido
''' </summary>
''' <remarks>Llamada desde: seguimclt.asp ; Tiempo máximo: 0,2</remarks>*/
function aceptar(indice)
{
	window.open("<%=Application("RUTASEGURA")%>script/pedidos/pedidos21.asp?Idioma=<%=Idioma%>&Id=" + ordenes[indice].id + "&Anyo=" + ordenes[indice].anyo + "&pedido=" + ordenes[indice].pedido + "&orden=" + ordenes[indice].orden + "&Prove=" + ordenes[indice].provecod + "&FecEmision=" + date2str(ordenes[indice].fecemision,vdatefmt) + "&CiaComp=<%=oRaiz.Sesion.CiaComp%>&IdPedido=" + ordenes[indice].idPedido + "&Referencia=<%=Referencia%>&ReferenciaValor=" + ordenes[indice].referencia + "&Importe=" + ordenes[indice].importe + "&Obs=" + ordenes[indice].obs + "&Pet=" + ordenes[indice].codaprov + "&Mon=" + ordenes[indice].mon,"default_main") 
	
}

/*
''' <summary>
''' Envia un pedido
''' </summary>
''' <param name="indice">id de input pedido</param> 
''' <remarks>Llamada desde: N a href de esta pantalla, los pedidos ; Tiempo máximo: 0</remarks>*/
function enviar(indice)
{
window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/enviar.asp?OrdenId=" + ordenes[indice].id,"fraSeguimServer")

}

function envioRegistrado(ordenId,NumError)
{
	if (NumError>0)
	{
		if (NumError==48)
			{
				alert('<%=JSALERTTEXT(den(68))%>')
			}
		else
			{				
				alert('<%=JSALERTTEXT(den(69))%>')
			}
	}	
	
	var l
	var i
	var orden
	for (orden=0;orden<=ordenes.length;orden++)
		{
		if (ordenes[orden].id == ordenId)
			{
			break;
			}
		}
	ordenes[orden].est=4
	var d
	d = new Date()
	ordenes[orden].fecultmov=d
	ordenes[orden].coment=""
	ordenes[orden].expandido = false
	ordenes[orden].lineas.length=0
	mostrarOrdenes()		
}


function DetalleEmpresa(i)
{
	winEmp = window.open("<%=application("RUTASEGURA")%>script/common/detalleempresa.asp?Emp=" + ordenes[i].idEmpresa ,"_blank","width=400,height=180,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no,scrollbars=no");
	winEmp.focus()
}


function DetallePet(i)
{
	winDest=window.open("<%=application("RUTASEGURA")%>script/common/detallepet.asp?Idioma=<%=Idioma%>&Pet=" + ordenes[i].codaprov ,"_blank","width=400,height=130,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no , scrollbars=no")
	winDest.focus()
}

var winEspera
/*
''' <summary>
''' Imprimir los pedidos
''' </summary>     
''' <remarks>Llamada desde: A HREF imprimir ; Tiempo máximo: 0</remarks>*/
function imprimirPedidos()
{
winEspera = window.open ("<%=Application("RUTASEGURA")%>script/common/winEspera.asp?msg=<%=server.urlEncode(den(64))%>&msg2=<%=server.urlEncode(den(63))%>&continua=continuarCargandoPedidos", "_blank", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
    var acceso = false
    while (acceso == false) {
        try {
            while (typeof(winEspera.document.images["imgReloj"])!= 'undefined') {
            }
            acceso = true
        }
        catch (e) {
            acceso =false
        }
	    }	

}
/*
''' <summary>
''' Imprime los pedidos
''' </summary>
''' <remarks>Llamada desde: seguimclt.asp ; Tiempo máximo: 0,2</remarks>*/
function continuarCargandoPedidos()
{
var oldAction = document.forms["frmBusqueda"].action
var oldTarget = document.forms["frmBusqueda"].target

window.open("<%=Application("RUTASEGURA")%>script/blank.htm","winImp","top=100,left=150,width=400,height=100,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no scrollbars=yes")
document.forms["frmBusqueda"].action="rptpedidos.asp?Idioma=<%=Idioma%>&Referencia=<%=Referencia%>&CiaDen=<%=CiaDen%>&PedidosAbiertos=<%=PedidosAbiertos%>"
document.forms["frmBusqueda"].target="winImp"
document.forms["frmBusqueda"].submit()
document.forms["frmBusqueda"].action = oldAction
document.forms["frmBusqueda"].target = oldTarget 
winEspera.focus()


}
/*
''' <summary>
''' Exportar los pedidos
''' </summary>     
''' <remarks>Llamada desde: A HREF Exportar ; Tiempo máximo: 0</remarks>*/
function exportarPedidos()
{
var re
var re2
var f

re=/MSIE 5.5/
re2=/Q299618/
	
re3 = /SP1/

if (navigator.userAgent.search(re)!=-1 && navigator.appMinorVersion.search(re2)==-1 && navigator.appMinorVersion.search(re3)!=-1)
	{
	window.open("<%=Application("RUTASEGURA")%>script/common/mspatch.asp","_blank","top=100,left=150,width=300,height=250,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
	window.close()
	}
else
	{	
    top.winEspera = window.open ("<%=Application("RUTASEGURA")%>script/common/winEspera.asp", "_blank", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
    var acceso = false
    // cambios realizados para compatibilidad con IE9
    // el primer acceso a top.winEspera.document.images["imgReloj"] genera un error de acceso si la ventana aún no se ha cargado
    // por eso se hace un segundo bucle controlando el error
    while (acceso == false) {
    try {
        while (typeof(top.winEspera.document.images["imgReloj"])!= 'undefined') {
		    }
        acceso = true
    }
    catch (e) {
        acceso =false
    }
    }
	var oldTarget = document.forms["frmBusqueda"].target
	var oldAction = document.forms["frmBusqueda"].action    
    var str

    f=document.forms["frmBusqueda"]
    str = "?anyo=" + f.lstAnyo.options[f.lstAnyo.selectedIndex].value
    if (f.txtCesta.value != ""){
        str = str  + "&pedido=" + f.txtCesta.value
    }
    if (f.txtNPed.value != ""){
        str = str  + "&orden="  + f.txtNPed.value  
    }
    str+="&PedidosAbiertos=<%=PedidosAbiertos %>";
    
	f.action="xlspedidos.asp" + str
	f.submit()
	f.action = oldAction
	f.target = oldTarget 
	}
}

/*
''' <summary>
''' Imprime el pedido
''' </summary>
''' <param name="anyo">Anyo de pedido</param>
''' <param name="pedido">Id de pedido</param>   
''' <param name="orden">Orden de pedido</param>        
''' <remarks>Llamada desde: seguimclt.asp ; Tiempo máximo: 0,2</remarks>*/
function imprimirPedido(anyo,pedido,orden)
{
window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/rptpedido.asp?Idioma=<%=Idioma%>&anyo=" + anyo + "&pedido=" + pedido + "&orden=" + orden + "&Referencia=<%=Referencia%>&CiaDen=<%=CiaDen%>&PedidosAbiertos=<%=PedidosAbiertos %>"  ,"_blank","top=50,left=50,width=700,height=500,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no scrollbars=yes")
}
/*
''' <summary>
''' Exportar el pedido
''' </summary>     
''' <param name="anyo">Anyo de pedido</param>
''' <param name="pedido">Id de pedido</param>   
''' <param name="orden">Orden de pedido</param>   
''' <remarks>Llamada desde: A HREF Exportar ; Tiempo máximo: 0</remarks>*/
function exportarPedido(anyo,pedido,orden)
{
var re
var re2
re=/MSIE 5.5/
re2=/Q299618/
	
re3 = /SP1/

if (navigator.userAgent.search(re)!=-1 && navigator.appMinorVersion.search(re2)==-1 && navigator.appMinorVersion.search(re3)!=-1)
	{
	window.open("<%=Application("RUTASEGURA")%>script/common/mspatch.asp","_blank","top=100,left=150,width=300,height=250,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
	window.close()
	}
else
	{
	top.winEspera = window.open ("<%=Application("RUTASEGURA")%>script/common/winEspera.asp", "_blank", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
    var acceso = false
    // cambios realizados para compatibilidad con IE9
    // el primer acceso a top.winEspera.document.images["imgReloj"] genera un error de acceso si la ventana aún no se ha cargado
    // por eso se hace un segundo bucle controlando el error
    while (acceso == false) {
    try {
        while (typeof(top.winEspera.document.images["imgReloj"])!= 'undefined') {
		    }
        acceso = true
    }
    catch (e) {
        acceso =false
    }
    }	
	window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/xlspedidos.asp?anyo=" + anyo + "&pedido=" + pedido + "&orden=" + orden + "&PedidosAbiertos=<%=PedidosAbiertos %>" ,"fraSeguimServer")
	}
}

function sincronizaCheckbox()
{
f = document.forms["frmBusqueda"]
if (f.elements["chkEst1"].checked)
	{
	f.elements["chkEst2"].checked = true
	f.elements["chkEst3"].checked = true
	}
else
	{
	f.elements["chkEst2"].checked = false
	f.elements["chkEst3"].checked = false
	}
}
function sincronizaCheckboxS()
{
f = document.forms["frmBusqueda"]
if (!f.elements["chkEst2"].checked || !f.elements["chkEst3"].checked)
	{
	f.elements["chkEst1"].checked = false
	}
}
function CargarPartidas()
{
    var sHTML
    var sCCoste
    
    sCCoste=document.getElementById("lstCentroCoste").options[document.getElementById("lstCentroCoste").selectedIndex].value    
    sHTML="<select id=lstPPresup name=lstPPresup style='Width:200px;'>"
    sHTML+="<option value =''></option>"
    <%if not isempty(rsPartidas) then
        if not rsPartidas is nothing then
            if rsPartidas.recordcount>0 then
                rsPartidas.movefirst
                While not rsPartidas.eof                
    %>
                    if (sCCoste!=""){
                        if (sCCoste=="<%=rsPartidas("CENTRO_SM")%>"){
                            sHTML+="<option value='<%=rsPartidas("PRES")%>'><%=rsPartidas("PRES")%> - <%=JSText(rsPartidas("DEN"))%></option>"
                        }
                    }
                    else{
                        sHTML+="<option value='<%=rsPartidas("PRES")%>'><%=rsPartidas("PRES")%> - <%=JSText(rsPartidas("DEN"))%></option>"
                    }
     
    <%                          
                    rsPartidas.movenext
                wend
            end if
        end if
      end if
    %>
    sHTML+="</select>"
    document.getElementById("divPartidas").innerHTML=sHTML
}

/*''' <summary>
''' Crear un cajetin con valores de tipo fecha
''' </summary>
''' <param name="Ctrl">Nombre del control a insertar</param>
''' <remarks>Llamada desde: Codigo html de la pagina ; Tiempo máximo: 0</remarks>*/
function InsertarCtrlFecha (Ctrl){

    return inputFecha(Ctrl, 95, null, null, null, "frmBusqueda", null, "", "<%=JSText(den(61))%>" + vdatefmt + "\\n<%=JSText(den(62))%>")  
}

    </script>
    <table width="100%">
        <tr>
            <td align="left" width="95%">
                <h1><%=sHeader %></h1>
            </td>
            <td width="2%" align="right">
                <a name="imgExcel" id="imgExcel" style="visibility: hidden" href="javascript:void(null)"
                    onclick="exportarPedidos()">
                    <img border="0" src="../images/excelg.GIF" width="28" height="28"></a>
            </td>
            <td width="2%" align="right">
                <a name="imgImpresora" id="imgImpresora" style="visibility: hidden" href="javascript:void(null)"
                    onclick="imprimirPedidos()">
                    <img border="0" src="../images/impresora.GIF" width="41" height="38"></a>
            </td>
        </tr>
    </table>
    <div name="divBusqueda" id="divBusqueda">
        <form name="frmBusqueda" id="frmBusqueda" method="post" action="buscarpedidos.asp?Idioma=<%=Idioma%>&PedidosAbiertos=<%=PedidosAbiertos %>"
        target="fraSeguimServer" onsubmit="return validarA()">
        <table class="principal" width="100%">
            <tr>
                <td align="left" class="subrallado" colspan="3">
                    <%=den(2)%>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="left" width="628px">
                    <%=den(5)%>
                </td>
                <td align="left">
                    <input type="submit" class="button" style="width: 90px;" name="cmdBuscar" id="cmdBuscar"
                        value="<%=den(6)%>" />
                </td>
                <td width="17%" align="right">
                    <% if PedidosAbiertos<>1 then %>
                        <a href="javascript:void(null)" onclick="return todos()"><%=sMostrarPendientes%></a>
                    <% end if %>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="left" colspan="3">
                    <table border="2" style="border-spacing: 1;" cellspacing="0" cellpadding="10" width="100%">
                        <tr>
                            <td align="left" valign="top" width="50%">
                                <table width="100%">
                                    <tr>
                                        <td align="left" width="50%">
                                            <%=den(66)%>:
                                        </td>
                                        <td align="left">
                                            <%if oEmpresas.count =1 then%>
                                            <%=oEmpresas.item(1).Den%>
                                            <input name="HidEmpresa" id="HidEmpresa" type="hidden" value="<%=oEmpresas.item(1).id%>" />
                                            <%else %>
                                            <select id="lstEmpresa" name="lstEmpresa" style="width: 100%">
                                                <option value=""></option>
                                                <%for each oEmp in oEmpresas%>
                                                <option value="<%=oEmp.id%>">
                                                    <%=oEmp.Den%></option>
                                                <%next%>
                                            </select>
                                            <%end if%>
                                        </td>
                                    </tr>
                                    <!--Nº de pedido FULLSTEP -->
                                    <tr>
                                        <td align="left">
                                            <%=den(70)%>:
                                        </td>
                                        <td align="left" style="padding-left: 0px;">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" style="padding: 0px 0px 0px 0px;">
                                                        <select id="lstAnyo" name="lstAnyo" style="width: 60px;">
                                                            <option value=""></option>
                                                            <%for i=(year(date)-10) to (year(date)+10)%>
                                                            <option value="<%=i%>" <%if i=(year(date)) then%>SELECTED<%end if%>>
                                                                <%=i%></option>
                                                            <%next%>
                                                        </select>
                                                    </td>
                                                    <td style="padding: 0px 0px 0px 0px;">
                                                        &nbsp;/&nbsp;
                                                    </td>
                                                    <td style="padding: 0px 0px 0px 0px;">
                                                        <input name="txtCesta" id="txtCesta" style="width: 43px;" />
                                                    </td>
                                                    <td style="padding: 0px 0px 0px 0px;">
                                                        &nbsp;/&nbsp;
                                                    </td>
                                                    <td align="right" style="padding: 0px 0px 0px 0px;">
                                                        <input name="txtNPed" id="txtNPed" style="width: 43px;" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <!--Nº de pedido abierto FULLSTEP -->
                                    <%
                                    if PedidosAbiertos="0" and bActivarMenuAbiertos then %>
                                        <tr>
                                            <td align="left">
                                                <%=den(85)%>:
                                            </td>
                                            <td align="left" style="padding-left: 0px;">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left" style="padding: 0px 0px 0px 0px;">
                                                            <select id="lstAnyoAbierto" name="lstAnyoAbierto" style="width: 60px;">
                                                                <option value=""></option>
                                                                <%for i=(year(date)-10) to (year(date)+10)%>
                                                                <option value="<%=i%>">
                                                                    <%=i%></option>
                                                                <%next%>
                                                            </select>
                                                        </td>
                                                        <td style="padding: 0px 0px 0px 0px;">
                                                            &nbsp;/&nbsp;
                                                        </td>
                                                        <td style="padding: 0px 0px 0px 0px;">
                                                            <input name="txtCestaAbierto" id="txtCestaAbierto" style="width: 43px;" />
                                                        </td>
                                                        <td style="padding: 0px 0px 0px 0px;">
                                                            &nbsp;/&nbsp;
                                                        </td>
                                                        <td align="right" style="padding: 0px 0px 0px 0px;">
                                                            <input name="txtNPedAbierto" id="txtNPedAbierto" style="width: 43px;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    <% end if %>
                                    <tr>
                                        <%if Referencia<>"" then %>
                                        <td align="left" width="19%">
                                            <%=Referencia%>:
                                        </td>
                                        <td align="left">
                                            <input name="txtRef" id="txtRef" maxlength="100" style="width: 97%;">
                                        </td>
                                        <%else%>
                                        <td align="left">
                                            &nbsp;<input type="hidden" name="txtRef" id="txtRef" style="width: 97%;">
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                        <%end if%>
                                    </tr>
                                    <tr>
                                        <%If paramgen.VerNumeroProveedor and PedidosAbiertos<>1 Then %>
                                            <td align="left">
                                                <nobr><%=den(7)%></nobr>
                                            </td>
                                            <td align="left">
                                                <input name="txtPedidoProve" id="txtPedidoProve" maxlength="100" style="width: 97%;">
                                            </td>
                                        <%Else %>
                                            <td align="left">
                                                &nbsp;<input name="txtpedidoProve" id="txtPedidoProve" type="hidden" style="width: 97%;" />
                                            </td>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                        <%End If %>
                                    </tr>
                                </table>
                            </td>
                            <td align="left" valign="top" width="50%">
                                <table width="100%">
                                    <tr>
                                        <td align="left" width="195px">
                                            <%=den(3)%>
                                        </td>
                                        <td align="left">
                                            <nobr>
						            <input name="txtCodExt" id="txtCodExt" style="width:189px;">&nbsp;
						            <input type="checkbox" id="chkCompleta" name="chkCompleta" class="checkbox"><%=den(48)%>
						            </nobr>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <%=den(50)%>
                                        </td>
                                        <td align="left">
                                            <input name="txtDenArt" id="txtDenArt" style="width: 189px;" maxlength="200">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="195px">
                                            <%=den(13)%>
                                        </td>
                                        <td align="left">
                                            <input name="txtArtInt" id="txtArtInt" style="width: 189px;" maxlength="60">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <nobr><%=den(8)%></nobr>
                                        </td>
                                        <td align="left">
                                            <nobr>
						            <table cellSpacing="0" cellPadding="0"><tr><td align=left>						            
						            </td><td align=left>
						            <script>						                document.write(InsertarCtrlFecha("txtFechaDesde")) </script>
						            </td><td align=left>
						            &nbsp;-&nbsp;
						            </td><td align=left>
						            <script>						                document.write(InsertarCtrlFecha("txtFechaHasta")) </script>
						            </td></tr></table>
						            </nobr>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <!--Busqueda por estados pedidos no abiertos-->
                        <tr>
                            <%if PedidosAbiertos<>1 then %>
                                <td align="left" valign="top" width="50%">
                                    <input type="checkbox" class="checkbox" id="chkEst0" name="chkEst0" value="4">
                                    <%=den(58)%><br>
                                    <input type="checkbox" class="checkbox" id="chkEst1" name="chkEst1" value="56" onclick="sincronizaCheckbox()">
                                    <%=den(9)%><br>
                                    &nbsp;&nbsp;&nbsp;<input type="checkbox" class="checkbox" id="chkEst2" name="chkEst2"
                                        onclick="sincronizaCheckboxS()" value="24">
                                    <%=den(10)%>
                                    <br>
                                    &nbsp;&nbsp;&nbsp;<input type="checkbox" class="checkbox" id="chkEst3" name="chkEst3"
                                        onclick="sincronizaCheckboxS()" value="32">
                                    <%=den(11)%>
                                    <br>
                                    <input type="checkbox" class="checkbox" id="chkEst4" name="chkEst4" value="64">
                                    <%=den(12)%>
                                    <br>
                                    <input type="checkbox" class="checkbox" id="chkEst5" name="chkEst5" value="256">
                                    <%=den(41)%>
                                    <br>
                                    <input type="checkbox" class="checkbox" id="chkEst6" name="chkEst6">
                                    <%=den(47)%>
                                    <br>
                                </td>
                            <% else %>
                                <!--Busqueda pedidos abiertos-->
                                <td align="left" valign="top" width="50%">
                                    <input type="checkbox" class="checkbox" name="chkAbiertos" value="100"/><%=den(77)%><br/>
                                    <input type="checkbox" class="checkbox" name="chkAbiertos" value="101" /><%=den(78)%><br/>
                                    <input type="checkbox" class="checkbox" name="chkAbiertos" value="102" /><%=den(79)%><br/>
                                </td>
                            <% end if %>
                            <td align="left" width="50%" valign="top">
                                <table width="100%">
                                    <%if bAccesoFSSM then %>
                                    <tr>
                                        <td align="left" width="195px">
                                            <%=den(72)%>:
                                        </td>
                                        <td align="left">
                                            <select id="lstCentroCoste" name="lstCentroCoste" style="width: 220px;" onchange="CargarPartidas()">
                                                <option value=""></option>
                                                <%if not rsCentros is nothing then
                                                if rsCentros.recordcount>0 then
                                                    rsCentros.movefirst
                                                    While not rscentros.eof
                                                        if not isnull(rscentros("UON4").value) then
                                                            sCCDEN = rscentros("UON4").value & " - " & nulltostr(rscentros("UON4DEN").value) & " (" & rscentros("UON1").value & "-" & rscentros("UON2").value & "-" & rscentros("UON3").value & ")"
                                                        Else
                                                            if not isnull(rscentros("UON3").value) then 
                                                                sCCDEN = rscentros("UON3").value & " - " & nulltostr(rscentros("UON3DEN").value) & " (" & rscentros("UON1").value & "-" & rscentros("UON2").value & ")"
                                                            Else
                                                                if not isnull(rscentros("UON2").value) then
                                                                    sCCDEN = rscentros("UON2").value & " - " & nulltostr(rscentros("UON2DEN").value) & " (" & rscentros("UON1").value & ")"
                                                                Else
                                                                    if not isnull(rscentros("UON1").value) then
                                                                        sCCDEN = rscentros("UON1").value & " - " & nulltostr(rscentros("UON1DEN").value)
                                                                    Else
                                                                        sCCDEN =""
                                                                    end if
                                                                end if
                                                            end if
                                                        End If
                                                %>
                                                <option value="<%=rscentros("CENTRO_SM")%>">
                                                    <%=sCCDEN%></option>
                                                <%          rscentros.movenext
                                                    wend
                                                end if
                                                end if
                                                %>
                                            </select>
                                        </td>
                                    </tr>
                                    <% if PedidosAbiertos<>1 then %>
                                        <tr>
                                            <td align="left">
                                                <%=sPPresupuestaria%>:
                                            </td>
                                            <td align="left">
                                                <div name="divPartidas" id="divPartidas">
                                                    <select id="lstPPresup" name="lstPPresup" style="width: 220px;">
                                                        <option value=""></option>
                                                        <%if not rsPartidas is nothing then
                                                        if rsPartidas.recordcount>0 then
                                                            rsPartidas.movefirst
                                                            While not rsPartidas.eof
                                                        %>
                                                        <option value="<%=rsPartidas("PRES")%>">
                                                            <%=rsPartidas("PRES")%>
                                                            -
                                                            <%=JSText(rsPartidas("DEN"))%></option>
                                                        <%          rsPartidas.movenext
                                                            wend
                                                        end if
                                                        end if
                                                        %>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                    <% end if %>
                                    <% end if %>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    &nbsp;
                </td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td align="left">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table width="100%">
                        <tr>
                            <td align="left" width="100%">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    &nbsp;
                </td>
            </tr>
        </table>
        <input type="hidden" name="opcTodos">
        <input type="hidden" name="txtDesdeId">
        </form>
    </div>
    <div name="divResultado" id="divResultado">
    </div>
    <%
	set oEmpresas = nothing	
	set oRaiz = nothing
    
    if bAccesoFSSM then set oCentros=nothing

    %>
</body>
<!--#include file="../common/fsal_2.asp"-->
</html>
