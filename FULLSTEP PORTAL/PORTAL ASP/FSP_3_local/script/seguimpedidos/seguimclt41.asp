﻿<!DOCTYPE HTML PUBLIC >
<%@ Language=VBScript %>
<!--#include file="../common/acceso.asp"-->
<!--#include file="../common/idioma.asp"-->
<!--#include file="../common/formatos.asp"-->
<HTML>
<HEAD>
<title><%=title%></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">

<link rel="stylesheet" type="text/css" href="<%=application("RUTASEGURA")%>script/common/estilo.asp">
<script src="../common/calendar.asp"></script>
<script SRC="../common/menu.asp"></script>
<script SRC="../common/formatos.js"></script>
</HEAD>

<body bgcolor="#ffffff" topMargin=0 leftmargin=0 onload="init(); MarcarPedidoVisitado()">
 

<%
''' <summary>
''' Muestra en detalle un pedido
''' </summary>
''' <remarks>Llamada desde: seguimpedidos\seguimclt.asp ; Tiempo máximo: 0,2</remarks>
    dim sCCDEN
	'Idiomas
	Idioma = Request("Idioma")
	Idioma = trim(Idioma)

    set oRaiz=validarUsuario(Idioma,true,true,0)
    
	bAccesoFSSM=oRaiz.Sesion.Parametros.AccesoFSSM
	
	'Obtiene los datos del pedido
	IdOrden=Request("OrdenId")
	CiaComp = Request("CiaComp")
	Anyo=Request("Anyo") 
	NumPedido=Request("Pedido")
	NumOrden=Request("Orden")
	Prove = Request("Prove")
	PedidosAbiertos=Request("PedidosAbiertos")

	Referencia = Request("Referencia")
    CiaDen = Request("CiaDen")

	Estado= Request("Estado")
	
	strBusqueda= Request("busq")

	CiaCod=oRaiz.Sesion.CiaCodGs
	
	dim den
	den = devolverTextos(Idioma,37)	

	decimalfmt=request.Cookies ("USU_DECIMALFMT")
	thousandfmt=Request.Cookies ("USU_THOUSANFMT")
	precisionfmt=cint(Request.Cookies ("USU_PRECISIONFMT"))
	datefmt=Request.Cookies("USU_DATEFMT")
	
    if bAccesoFSSM then
        set rsPPresup=oRaiz.DevolverPartidaPresupuestaria(CiaComp,Idioma)
        if not rsPPresup is nothing then
            if rsPPresup.recordcount>0 then
                sCodPPresupuestaria=rsPPresup("PRES5")
                sDenPPresupuestaria=rsPPresup("DEN")
            end if
        end if    
    end if
    if PedidosAbiertos=1 then
        sHeader=den(56)'Detalle del pedido abierto
    else
        sHeader = den(1)'Detalle del pedido
    end if
%>
<SCRIPT>    dibujaMenu(4)</script>

<SCRIPT>
var requiereRefresco
requiereRefresco=true

var vdecimalfmt 
var vthousanfmt
var	vprecisionfmt
var vdatefmt

vdecimalfmt='<%=decimalfmt%>'
vthousanfmt='<%=thousandfmt%>'
vprecisionfmt='<%=precisionfmt%>'
vdatefmt='<%=datefmt%>'

function aplicarFormatos(vdec,vtho,vpre,vdat)
{
	var vdatetmp
	vdatetmp=vdatefmt
	vdecimalfmt=vdec
	vthousanfmt=vtho
	vprecisionfmt=vpre
	vdatefmt=vdat
	
	dibujarPage()
	if(vdatefmtold==vdatetmp)
	  {
		vdatefmtold=vdatetmp
	  }
}

var vdatefmtold
vdatefmtold=vdatefmt

/*''' <summary>
''' Iniciar la pagina.
''' </summary>     
''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
function init()
{
	document.getElementById('tablemenu').style.display = 'block';
	dibujarPage()
	dibujarDirFac()
}

<%    
	set oOrdenes = oRaiz.Generar_COrdenes()
    iModoImp=oOrdenes.DevolverModoImputacion(CiaComp) 
	set adorOrden=oOrdenes.BuscarTodasOrdenes (CiaComp,Idioma,oRaiz.Sesion.Parametros.EmpresaPortal,Application("NOMPORTAL"),0,null,null,null,null,null,null,null,null,null,null,null,null,null,null,1,,,,,,,,IdOrden,,,,,,PedidosAbiertos)

    dImporteTotal=adorOrden("IMPORTE")*adorOrden("CAMBIO")
    if not isnull(adorOrden("COSTES")) then dCostes=adorOrden("COSTES")*adorOrden("CAMBIO")
    if not isnull(adorOrden("DESCUENTOS")) then dDescuentos=adorOrden("DESCUENTOS")*adorOrden("CAMBIO")

	bMostrarDirFact=false	
	sDirFac=""
	for each oField in adorOrden.fields
		if ucase(oField.name)="DIRFAC" then
			bMostrarDirFact=true			
			sDirFac=adorOrden("DIRFAC").value
			exit for
		end if
	next	
	
	set oOrden = oRaiz.Generar_COrden()
	oOrden.id = clng(IdOrden)
	RefValor=adorOrden("REFERENCIA").value
	Importe=adorOrden("IMPORTE").value*adorOrden("CAMBIO").value
	Obs=adorOrden("OBS").value
	Petic=adorOrden("PER").value
	Mon=adorOrden("MON").value
	IdPedido=adorOrden.fields("PEDIDO").value
	if adorOrden.fields("RECEPTOR").value <> adorOrden.fields("PER").value then
		NotificarReceptor = 1
	else
		NotificarReceptor = 0
	end if	
	set ador = oOrden.Devolverlineas(CiaComp,Idioma)
    if not ador.eof then
	    while not ador.eof
            dImporteBrutoLin=ador("IMPORTE").value
            if not isnull(ador("COSTES")) then dCostesLin=ador("COSTES")*adorOrden("CAMBIO")
            if not isnull(ador("DESCUENTOS")) then dDescuentosLin=ador("DESCUENTOS")*adorOrden("CAMBIO")
            dImporteBruto=dImporteBruto+dImporteBrutoLin+dCostesLin-dDescuentosLin
            ador.movenext
        wend
    end if
    set ador = oOrden.DevolverLineasDetalle(CiaComp,Idioma,sCodPPresupuestaria)
	set adorAtr = oOrden.DevolverAtributos(CiaComp)
    set oLineasDesgloseImpuesto = oRaiz.Generar_CLineasDesgImpuesto
    oLineasDesgloseImpuesto.cargarLineasDesgloseImpuestoOrden oOrden.id,CiaComp,Idioma

	sAdjuntosOrden=""
	set oAdjuntosOrden = oOrden.cargarAdjuntos(CiaComp,false)
	for each oAdjunto in oAdjuntosOrden
		sAdjuntosOrden=sAdjuntosOrden & oAdjunto.Nombre & " ; "
	next
	if sAdjuntosOrden<>"" then
		sAdjuntosOrden=mid(sAdjuntosOrden,1,len(sAdjuntosOrden)-3)
	end if
	
	Set oPersonas = oRaiz.Generar_CPersonas()
	oPersonas.CargarTodasLasPersonas CiaComp,adorOrden.fields("PER").value
	If Not oPersonas.Item(adorOrden.fields("PER").value) Is Nothing Then
		set oPersona = oPersonas.Item(adorOrden.fields("PER").value)
	else
		set oPersona=nothing
    End If
	oPersonas.CargarTodasLasPersonas CiaComp,adorOrden.fields("RECEPTOR").value
	If Not oPersonas.Item(adorOrden.fields("RECEPTOR").value) Is Nothing Then
		set oPersonaRec = oPersonas.Item(adorOrden.fields("RECEPTOR").value)
	else
		set oPersonaRec=nothing
    End If    
    set oPersonas=nothing
  
    'Ocultar columna fecha de entrega
    iNumLinPlanEnt=0
    iNumLinPlanEntPub=0
    bOcultarFecEntrega=false
    if not ador is nothing then
        if ador.recordcount>0 then
            while not ador.eof
                if ador("HAYPLANES")>0 then
                    iNumLinPlanEnt=iNumLinPlanEnt+1
                    if ador("PUB_PLAN_ENTREGA")=1 then iNumLinPlanEntPub=iNumLinPlanEntPub+1
                end if
                ador.movenext
            wend
        end if

        if (iNumLinPlanEnt=ador.recordcount) and iNumLinPlanEntPub=0 then
            bOcultarFecEntrega=true
        end if
    end if  
    ador.movefirst
%>
/*
''' <summary>
''' Envia un pedido
''' </summary>
''' <remarks>Llamada desde: cmdEnviar/onclick ; Tiempo máximo: 0</remarks>*/
function enviar()
{
window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/enviar.asp?OrdenId=<%=IdOrden%>","fraSeguimServer")

}

function MarcarPedidoVisitado()
{
	<% 
	Dim oOrden
	set oOrden = oRaiz.Generar_COrden()
	oOrden.PedidoVisitado IdOrden, CiaComp
    %>
}

function aceptar()
{
	var f
	f=document.forms["frmAceptar"]
	window.open("<%=Application("RUTASEGURA")%>script/pedidos/pedidos21.asp?Idioma=<%=Idioma%>&Id=<%=IdOrden%>&Anyo=<%=Anyo%>&pedido=<%=NumPedido%>&orden=<%=NumOrden%>&Prove=<%=Prove%>&CiaComp=<%=CiaComp%>&IdPedido=<%=IdPedido%>&Referencia=<%=Referencia%>&ReferenciaValor=<%=RefValor%>&Importe=<%=Importe%>&Obs=<%=server.urlencode(nulltostr(Obs))%>&Pet=<%=Petic%>&Mon=<%=Mon%>","default_main") 
}

function DenominacionDest(dest,LineaPedido)
{

	winDest=window.open("<%=application("RUTASEGURA")%>script/common/DenominacionDest.asp?Idioma=<%=Idioma%>&Dest=" + Var2Param(dest) + "&LineaPedido=" + Var2Param(LineaPedido) ,"_blank","width=400,height=200,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no,scrollbars=no")
	winDest.focus()
}

function DenominacionUP(uni)
{
	winUP=window.open("<%=application("RUTASEGURA")%>script/common/DenominacionUP.asp?Idioma=<%=Idioma%>&Cod=" + Var2Param(uni) ,"_blank","width=400,height=130,location=no,menubar=no,toolbar=no,resizable=no,addressbar=no, scrollbars=yes")
	winUP.focus()
}

function MostrarPlanesEntrega(IdLinea,CodArt,DenArt,Uni,Mon,TipoRecepcion)
{        
	winPE=window.open("<%=application("RUTASEGURA")%>script/common/PlanesEntrega.asp?Idioma=<%=Idioma%>&Anyo=<%=Anyo%>&Pedido=<%=NumPedido%>&Orden=<%=NumOrden%>&IdLinea=" + 
        IdLinea + "&CodArt=" + Var2Param(CodArt) + "&DenArt=" + Var2Param(DenArt) + "&Uni="+ Var2Param(Uni)+"&Mon="+Var2Param(Mon)+"&TipoRecepcion="+Var2Param(TipoRecepcion),"_blank","location=no,menubar=no,width=400,height=200,toolbar=no,resizable=yes,addressbar=no")        
	winPE.focus()      
}

function MostrarCostesDescuentos(Ambito,Id,moneda,cambio,ImporteBruto,ImporteCostes,ImporteDescuentos,ImporteNeto)
{        
    winPE=window.open("<%=application("RUTASEGURA")%>script/common/CostesDescuentos.asp?Idioma=<%=Idioma%>&Ambito=" + Ambito + "&Id=" + Id + "&Moneda=" + Var2Param(moneda) +
        "&Cambio=" + cambio + "&ImporteBruto=" + ImporteBruto + "&ImporteCostes=" + ImporteCostes + "&ImporteDescuentos=" + ImporteDescuentos + 
        "&ImporteNeto=" + ImporteNeto,"_blank","location=no,menubar=no,width=400,height=250,toolbar=no,resizable=yes,addressbar=no")        
	winPE.focus()   

}
function MostrarDetalleImpuesto(IdOrden,ImpuestoDen,ImpuestoValor,ImpuestoMoneda,Cuota)
{   
	winPE=window.open("<%=application("RUTASEGURA")%>script/common/DetalleImpuesto.asp?Idioma=<%=Idioma%>&IdOrden=" + IdOrden + "&ImpDen=" + encodeURI(ImpuestoDen) + "&ImpVal=" + ImpuestoValor + "&ImpMon=" + ImpuestoMoneda + "&cuota=" + num2str(Cuota,vdecimalfmt,vthousanfmt,vprecisionfmt) ,
                    "_blank","location=no,menubar=no,width=400,height=200,toolbar=no,resizable=yes,addressbar=no")        
	winPE.focus()      
}

function MostrarDetalleAbierto(IdOrden,LineaPedido,TipoPedidoAbierto)
{
	winDest=window.open("<%=application("RUTASEGURA")%>script/common/detallePedAbierto.asp?Idioma=<%=Idioma%>&IdOrden=" + IdOrden + "&LineaPedido=" + LineaPedido + "&TipoPedidoAbierto=" + TipoPedidoAbierto ,"_blank","width=1000,height=400,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no,scrollbars=yes")
	winDest.focus()
}
function MostrarDetalleAbierto2(IdOrden,LineaPedido)
{
	winDest=window.open("<%=application("RUTASEGURA")%>script/common/detallePedAbierto2.asp?Idioma=<%=Idioma%>&IdOrden=" + IdOrden + "&LineaPedido=" + LineaPedido  ,"_blank","width=500,height=250,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no,scrollbars=yes")
	winDest.focus()
}
/*
''' <summary>
''' Exportar el pedido
''' </summary>     
''' <remarks>Llamada desde: cmdExportar/onclick ; Tiempo máximo: 0</remarks>*/
function exportarPedido()
{
var re
var re2
re=/MSIE 5.5/
re2=/Q299618/
	
re3 = /SP1/
if (navigator.userAgent.search(re)!=-1 && navigator.appMinorVersion.search(re2)==-1 && navigator.appMinorVersion.search(re3)!=-1)
	{
	window.open("<%=Application("RUTASEGURA")%>script/common/mspatch.asp","_blank","top=100,left=150,width=300,height=250,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
	window.close()
	}
else
	{
	top.winEspera = window.open ("<%=Application("RUTASEGURA")%>script/common/winEspera.asp", "_blank", "top=100,left=150,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")
	var acceso = false
    while (acceso == false) {
        try {
            while (typeof(top.winEspera.document.images["imgReloj"])!= 'undefined') {
            }
            acceso = true
        }
        catch (e) {
            acceso =false
        }
	}	
	window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/xlspedidos.asp?Anyo=<%=Anyo%>&pedido=<%=NumPedido%>&orden=<%=NumOrden%>&PedidosAbiertos=<%=PedidosAbiertos %>" ,"fraSeguimServer")
	}

}
/*
''' <summary>
''' Imprime el pedido
''' </summary>
''' <param name="anyo">Anyo de pedido</param>
''' <param name="pedido">Id de pedido</param>   
''' <param name="orden">Orden de pedido</param>        
''' <remarks>Llamada desde: seguimclt41.asp ; Tiempo máximo: 0,2</remarks>*/
function imprimirPedido()
{
	window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/rptpedido.asp?Idioma=<%=Idioma%>&Anyo=<%=Anyo%>&pedido=<%=NumPedido%>&orden=<%=NumOrden%>&Referencia=<%=Referencia%>&CiaDen=<%=CiaDen%>&PedidosAbiertos=<%=PedidosAbiertos %>" ,"blank","top=50,left=50,width=700,height=500,location=no,menubar=no,toolbar=no,resizable=yes,addressbar=no scrollbars=yes")
}
function envioRegistrado(ordenId,NumError)
{
	if (NumError>0)
	{
		if (NumError==48)
			{
				alert('<%=JSText(den(34))%>')
			}
		else
			{				
				alert('<%=JSText(den(35))%>')
			}
	}	
	document.getElementById("cmdEnviar").style.visibility="hidden"
}

function dibujarPage()
{
<%
tipoPedAbierto=adorOrden.fields("PED_ABIERTO_TIPO").value
if isnull(tipoPedAbierto) then
    sCantidad=Den (23) 'Cantidad
    sImporte= Den (26) 'Importe
else
    sCantidad=Den (52) 'Cantidad abierta
    sImporte= Den (54)  'Importe abierto 
end if
 %>
str=""
str+="<TABLE border=0 width=100% cellspacing=2 cellpadding=1>"

//SECCION DE IMPORTES
//Importes totales
str+="  <TR align='left'><TD class=cabecera colspan=2><%=Den(38)%></TD></TR>"
//Importe bruto
str+="	<TR align='left'>"
str+="      <TD width=25% class=cabecera><%=Den(44)%></TD>"
str+="      <TD class=filaImpar><TABLE width=30%><TR><TD width=70% style='text-align:right;'>" + num2str(<%=replace(dImporteBruto,",",".")%> ,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</TD><TD width=20% style='text-align:left;'><%=JSText(adorOrden.fields("MON").value)%></TD><TD width=10%></TD></TR></TABLE></TD>"
str+="	</TR>"
//Costes
str+="	<TR align='left'>"
str+="      <TD width=25% class=cabecera><%=Den(45)%></TD>"
str+="      <TD class=filaImpar>"         
    str+="<TABLE width=30%><TR>"
    str+="<TD width=70% style='text-align:right;'>" + num2str(<%=replace(dCostes,",",".")%> ,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</TD>"
    str+="<TD width=20% style='text-align:left;'><%=JSText(adorOrden.fields("MON").value)%></TD>"            
    str+="<TD width=10% style='text-align:left;'>"
    <%if adorOrden("HAYCOSTES")>0 then%>
        str+="<a href='javascript:void(null)' onclick='MostrarCostesDescuentos(0," + "<%=adorOrden("ID").value%>" + ",\"" + "<%=adorOrden.fields("MON").value%>" + "\"," 
        str+="<%=replace(adorOrden.fields("CAMBIO").value,",",".")%>" + "," + "<%=replace(dImporteBruto,",",".")%>" + "," + "<%=replace(dCostes,",",".")%>" + "," 
        str+="<%=replace(dDescuentos,",",".")%>" + "," + "<%=replace(dImporteTotal,",",".")%>" + ")' title='<%=Den(43)%>'><IMG border=0 SRC='../images/masinform.gif'></a>"                 
    <%end if%>
    str+="</TD></TR></TABLE>"        
str+=   "</TD>"
str+="	</TR>"
//Descuentos
str+="	<TR align='left'>"
str+="      <TD width=25% class=cabecera><%=Den(46)%></TD>"
str+="      <TD class=filaImpar>"     
    str+="<TABLE width=30%><TR>"
    str+="<TD width=70% style='text-align:right;'>" + num2str(<%=replace(dDescuentos,",",".")%> ,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</TD>"
    str+="<TD width=20% style='text-align:left;'><%=JSText(adorOrden.fields("MON").value)%></TD>"    
    str+="<TD width=10% style='text-align:left;'>"
    <%if adorOrden("HAYDESCUENTOS")>0 then%>
        str+="<a href='javascript:void(null)' onclick='MostrarCostesDescuentos(0," + "<%=adorOrden("ID").value%>" + ",\"" + "<%=adorOrden.fields("MON").value%>" + "\"," 
        str+="<%=replace(adorOrden.fields("CAMBIO").value,",",".")%>" + "," + "<%=replace(dImporteBruto,",",".")%>" + "," + "<%=replace(dCostes,",",".")%>" + "," 
        str+="<%=replace(dDescuentos,",",".")%>" + "," + "<%=replace(dImporteTotal,",",".")%>" + ")' title='<%=Den(43)%>'><IMG border=0 SRC='../images/masinform.gif'></a>"                 
    <%end if%>
    str+="</TD></TR></TABLE>"
str+=   "</TD>"
str+="	</TR>"
//Impuestos
<%for each oLineaDesgloseImpuesto in oLineasDesgloseImpuesto%>
str+="	<TR align='left'>"
str+="      <TD width=25% class=cabecera><%=Den(47)%>&nbsp;<%=oLineaDesgloseImpuesto.tipoimpuesto%>&nbsp;<%=oLineaDesgloseImpuesto.TipoValor%>%&nbsp;"
str+=           "<a href='javascript:void(null)' onclick='MostrarDetalleImpuesto(" + "<%=adorOrden("ID").value%>" +  ",\"" +"<%=oLineaDesgloseImpuesto.tipoimpuesto%>" + "\"," +"<%=oLineaDesgloseImpuesto.TipoValor%>" + ",\"" +"<%=adorOrden.fields("MON").value%>" + "\"," +"<%=replace(FormatNumber(oLineaDesgloseImpuesto.cuota,2),",",".")%>" +")' "
str+="          style='color:white; text-decoration:underline;' title='<%=oLineaDesgloseImpuesto.tipoimpuesto%>&nbsp;<%=oLineaDesgloseImpuesto.TipoValor%>%'>"
str+="          (<%=Den(48)%>:&nbsp;"+ num2str(<%=replace(oLineaDesgloseImpuesto.BaseImponible,",",".")%> ,vdecimalfmt,vthousanfmt,vprecisionfmt) +"&nbsp;<%=adorOrden.fields("MON").value%>)</a></TD>"
str+="      <TD class=filaImpar>"
str+="          <TABLE width=30%><TR>"
str+="              <TD width=70% style='text-align:right;'>"+ num2str(<%=replace(oLineaDesgloseImpuesto.cuota,",",".")%> ,vdecimalfmt,vthousanfmt,vprecisionfmt) +"</TD>"
str+="              <TD width=20% style='text-align:left;'><%=JSText(adorOrden.fields("MON").value)%></TD>"
str+="              <TD width=10% style='text-align:left;'>"
str+=                   "<a href='javascript:void(null)' onclick='MostrarDetalleImpuesto(" + "<%=adorOrden("ID").value%>" +  ",\"" +"<%=oLineaDesgloseImpuesto.tipoimpuesto%>" + "\"," +"<%=oLineaDesgloseImpuesto.TipoValor%>" + ",\"" +"<%=adorOrden.fields("MON").value%>" + "\"," +"<%=replace(FormatNumber(oLineaDesgloseImpuesto.cuota,2),",",".")%>" + ")' "
str+="                   title='<%=oLineaDesgloseImpuesto.tipoimpuesto%>&nbsp;<%=oLineaDesgloseImpuesto.TipoValor%>%'><IMG border=0 SRC='../images/masinform.gif'></a>" 
str+="          </TD></TR></TABLE>"
str+="      </TD>"
str+="	</TR>"
<% next%>

//Importe total
str+="	<TR align='left'>"
str+="      <TD width=25% class=cabecera><%=Ucase(Den(42))%></TD>"
str+="      <TD class=filaImpar><TABLE width=30%><TR style='font-weight:bold;'><TD width=70% style='text-align:right;'>"+ num2str(<%=replace(dImporteTotal,",",".")%> ,vdecimalfmt,vthousanfmt,vprecisionfmt) +"</TD><TD width=20% style='text-align:left;'><%=JSText(adorOrden.fields("MON").value)%></TD><TD width=10%></TD></TR></TABLE></TD>"
str+="	</TR>"

//SECCION DATOS GENERALES
str+="  <TR align='left'><TD class=cabecera colspan=2><%=Den(2)%></TD></TR>"
//referencia,moneda, f.pago

<% if adorOrden("TIPO")=5 then %>
    //Fec.Ini, Fec.Fin en pedidos abiertos únicamente
    str+="	<TR align='left'>"
    str+="         <TD class=cabecera><%=Den(50)%></TD>"
    str+="         <TD class=filaImpar><%=VisualizacionFecha(adorOrden("PED_ABIERTO_FECINI").value,datefmt)%></TD>"
    str+="	</TR>"

    str+="	<TR align='left'>"
    str+="         <TD class=cabecera><%=Den(51)%></TD>"
    str+="         <TD class=filaImpar><%=VisualizacionFecha(adorOrden("PED_ABIERTO_FECFIN").value,datefmt)%></TD>"
    str+="	</TR>"
<% end if %>
<%if Referencia<>"" then%>
str+="	<TR align='left'>"
str+="         <TD class=cabecera><%=JSText(Referencia)%></TD>"
str+="         <TD class=filaImpar><%=JSText(adorOrden.fields("REFERENCIA").value)%></TD>"
str+="	</TR>"
<%end if%>
str+="	<TR align='left'>"
str+="      <TD width=25% class=cabecera><%=Den(7)%></TD>"
str+="      <TD class=filaImpar><%=JSText(adorOrden.fields("MON").value)%> - <%=JSText(adorOrden.fields("MONDEN").value)%></TD>"
str+="	</TR>"
str+="	<TR align='left'>"
str+="      <TD width=25% class=cabecera><%=Den(8)%></TD>"
str+="      <TD class=filaImpar><%=JSText(adorOrden.fields("PAG").value)%> - <%=JSText(adorOrden.fields("PAGDEN").value)%></TD>"
str+="	</TR>"
<%
if not adorAtr is nothing then
	while not adorAtr.eof
%>

str+="  <TR align='left'>"
str+="		<TD width=25% class=cabecera><%=JSText(adorAtr("DEN").value)%></td>"
<%if adorAtr("TIPO").value=1 then%> //texto
   str+="      <TD class=filaImpar><%=JSText(adorAtr("VALOR_TEXT").value)%></td>"
<%end if%>
<%if adorAtr("TIPO").value=2 then%> //NUMERO
   str+="      <TD class=filaImpar>" + num2str(<%=JSNum(adorAtr("VALOR_NUM").value)%>,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</TD>"
<%end if%>
<%if adorAtr("TIPO").value=3 then%>
   str+="      <TD class=filaImpar>" + date2str(<%=JSDate(adorAtr("VALOR_FEC").value)%>,vdatefmt) + "</TD>"
<%end if%>
<%if adorAtr("TIPO").value=4 then%>
  <%if isnull(adorAtr("VALOR_BOOL").value) then%>
    str+="      <TD class=filaImpar>&nbsp;</td>"
  <%elseif adorAtr("VALOR_BOOL").value=0 then%>
    str+="      <TD class=filaImpar><%=JSText(den(28))%></td>"  
  <%else%>
    str+="      <TD class=filaImpar><%=JSText(den(27))%></td>"  
  <%end if
end if
 adorAtr.movenext
 wend
  end if%>
//Observaciones 
str+="	<TR align='left'>" 
str+="		<TD class=cabecera><%=den(9)%></TD>"
str+="		<TD><%=JSText(adorOrden("OBS").value)%></TD>"
str+="	</TR>" 
str+="	<TR align='left'>" //Adjuntos
str+="      <TD width=25% class=cabecera><%=Den(10)%>"
<%if not sAdjuntosOrden="" then%>
str+="      <a href='javascript:void(null)' onclick='verAdjuntosOrden(" + "<%=IdOrden%>" + ")'><img border = 0 src= '../images/clip.gif'></A>"
<%end if%>
str+="      </TD>"
str+="      <TD class=filaImpar><%=JSText(sAdjuntosOrden)%></TD>"
str+="	</TR>"
<%if bAccesoFSSM and iModoImp=0 then %>
str+="	<TR align='left'>" 
str+="		<TD class=cabecera><%=den(36)%></TD>"
<%
if not isnull(adorOrden("CC_UON4").value) then
    sCCDEN = adorOrden("CC_UON4").value & " - " & nulltostr(adorOrden("CC_UON4DEN").value) & " (" & adorOrden("CC_UON1").value & "-" & adorOrden("CC_UON2").value & "-" & adorOrden("CC_UON3").value & ")"
Else
    if not isnull(adorOrden("CC_UON3").value) then 
        sCCDEN = adorOrden("CC_UON3").value & " - " & nulltostr(adorOrden("CC_UON3DEN").value) & " (" & adorOrden("CC_UON1").value & "-" & adorOrden("CC_UON2").value & ")"
    Else
        if not isnull(adorOrden("CC_UON2").value) then
            sCCDEN = adorOrden("CC_UON2").value & " - " & nulltostr(adorOrden("CC_UON2DEN").value) & " (" & adorOrden("CC_UON1").value & ")"
        Else
            if not isnull(adorOrden("CC_UON1").value) then
                sCCDEN = adorOrden("CC_UON1").value & " - " & nulltostr(adorOrden("CC_UON1DEN").value)
            Else
                sCCDEN =""
            end if
        end if
    end if
End If
 %>
str+="		<TD class=filaImpar><%=JSText(sCCDen)%></TD>"
str+="	</TR>" 
str+="	<TR align='left'>" 
str+="		<TD class=cabecera><%=sDenPPresupuestaria%></TD>"
str+="		<TD class=filaImpar><%=JSText(adorOrden("PRES").value)%><%=iif(isnull(adorOrden("PP_DEN").value),""," - ")%><%=JSText(adorOrden("PP_DEN").value)%></TD>"
str+="	</TR>" 
<%end if %>
//Datos del peticionario
<%If not oPersona Is Nothing then%>
str+="	<TR>" 
str+="	  <table border=0 width=100% cellspacing=1 cellpadding=1>"
str+="		<TR align='left'>"
str+="			<TD colspan=4 class=cabecera><%=den(11)%></TD>"
str+="		</TR>"
str+="		<TR align='left'>"
str+="			<TD width=15% class=cabecera><%=den(12)%></TD>"
str+="			<TD width=35%><%=JSText(opersona.Nombre)%> &nbsp; <%=JSText(opersona.Apellidos)%></TD>"
str+="			<TD width=15% class=cabecera><%=den(13)%></TD>"
str+="			<TD width=35%><%=JSText(opersona.Tfno)%></TD>"
str+="		</TR>"
str+="		<TR align='left'>"
str+="			<TD width=15% class=cabecera><%=den(14)%></TD>"
str+="			<TD><%=JSText(opersona.Fax)%></TD>"
str+="			<TD width=15% class=cabecera><%=den(15)%></TD>"
str+="			<TD><%=JSText(opersona.Email)%></TD>"
str+="		</TR>"
str+="	  </table>"
str+="	</TR>" 
<%end if%>

//Datos del receptor
<%If not opersonaRec Is Nothing then%>
str+="	<TR>" 
str+="	  <table border=0 width=100% cellspacing=1 cellpadding=1>"
str+="		<TR align='left'>"
str+="			<TD colspan=4 class=cabecera><%=den(16)%></TD>"
str+="		</TR>"
str+="		<TR align='left'>"
str+="			<TD width=15% class=cabecera><%=den(12)%></TD>"
str+="			<TD width=35%><%=JSText(opersonaRec.Nombre)%> &nbsp; <%=JSText(opersonaRec.Apellidos)%></TD>"
str+="			<TD width=15% class=cabecera><%=den(13)%></TD>"
str+="			<TD width=35%><%=JSText(opersonaRec.Tfno)%></TD>"
str+="		</TR>"
str+="		<TR align='left'>"
str+="			<TD width=15% class=cabecera><%=den(14)%></TD>"
str+="			<TD><%=JSText(opersonaRec.Fax)%></TD>"
str+="			<TD width=15% class=cabecera><%=den(15)%></TD>"
str+="			<TD><%=JSText(opersonaRec.Email)%></TD>"
str+="		</TR>"
str+="	  </table>"
str+="	</TR>" 
<%end if%>

str+="	<TR>" 
str+="	  <table border=0 width=100% cellspacing=1 cellpadding=0>" 
<%
    'Calcular el colspan
    iColspanDetalleLineas=11 + iif(not bOcultarFecEntrega,1,0) + iif(PedidosAbiertos=1,1,0)
    if adorOrden("PED_ABIERTO_TIPO").value=2 then
        iColspanDetalleLineas=iColspanDetalleLineas-3 'con esto eliminamos las columnas correspondientes a : Cantidad,UP,PU
    end if
    
 %>   
str+="      <TR align='left'><TD  colspan=<%=iColspanDetalleLineas%> class=cabecera><%=Den(17)%></TD></TR>"

str+="		<TR align='left'>" 
str+="			<td width=3% class=cabecera><% = Den (49) %></td>"  //Num. de linea de pedido
str+="			<td width=9% class=cabecera><% = Den (18) %></td>"  //Cod.Art(comp)
str+="			<td width=9% class=cabecera><% = Den (19) %></td>"  //Cod.Art(prov)
str+="			<td width=22% class=cabecera><% = Den (20) %></td>" //Denominación  
str+="			<td width=7% class=cabecera><% = Den (21) %></td>"  //Dest.
<%if not bOcultarFecEntrega then %>
    str+="			<td width=9% class=cabecera><% = Den (22) %></td>" //F.Entrega
    <% 
        if tipoPedAbierto= 3 or isnull(tipoPedAbierto) then %>
        str+="			<td width=7% class=cabecera><%=sCantidad %></td>" //Cantidad (cantidad abierta)
    <% end if %>
    <% if tipoPedAbierto= 3 then %>
        str+="			<td width=7% class=cabecera><% = Den (53) %></td>" //CantidadPedida
    <% end if %>
    <% if tipoPedAbierto= 3 or isnull(tipoPedAbierto) then %>
        str+="			<td width=4% class=cabecera><% = Den (24) %></td>"  //UP
        str+="			<td width=4% class=cabecera><% = Den (25) %></td>" //PU
    <% end if %>
    str+="			<td width=6% class=cabecera><% = Den (40) %> (<%=JSText(adorOrden.fields("MON").value)%>)</td>" //Costes
    str+="			<td width=6% class=cabecera><% = Den (41) %> (<%=JSText(adorOrden.fields("MON").value)%>)</td>" //Descuentos
    str+="			<td width=6% class=cabecera><%=sImporte %>(<%=JSText(adorOrden.fields("MON").value)%>)</td>" //Importe (Importe abierto)
    <% if tipoPedAbierto= 2 then %>
        str+="			<td width=9% class=cabecera><% = Den (55) %> (<%=JSText(adorOrden.fields("MON").value)%>)</td>" //Importe pedido
    <% end if %>
    str+="		</TR>"
<%else%>
    <% if tipoPedAbierto= 3 or isnull(tipoPedAbierto) then %>
        str+="			<td width=8% class=cabecera><%=sCantidad %></td>" //Cantidad (cantidad abierta)
    <% end if %>
    <% if tipoPedAbierto= 3 then %>
        str+="			<td width=9% class=cabecera><% = Den (53) %></td>" //CantidadPedida
    <% end if %>
    <% if tipoPedAbierto= 3 or isnull(tipoPedAbierto) then %>
    str+="			<td width=8% class=cabecera><% = Den (24) %></td>"  //UP
    str+="			<td width=4% class=cabecera><% = Den (25) %></td>" //PU
    <% end if %>
    str+="			<td width=7% class=cabecera><% = Den (40) %> (<%=JSText(adorOrden.fields("MON").value)%>)</td>" //Costes
    str+="			<td width=7% class=cabecera><% = Den (41) %> (<%=JSText(adorOrden.fields("MON").value)%>)</td>" //Descuentos
    str+="			<td width=8% class=cabecera><%=sImporte %>(<%=JSText(adorOrden.fields("MON").value)%>)</td>" //Importe (Importe abierto)
    <% if tipoPedAbierto= 2 then %>
        str+="			<td width=9% class=cabecera><% = Den (55) %> (<%=JSText(adorOrden.fields("MON").value)%>)</td>" //Importe pedido
    <% end if %>
<%end if %>

<%
mclass="filaImpar"
i=0

''Datos del pedido
tipoPedAbierto=adorOrden.fields("PED_ABIERTO_TIPO").value

if not ador.eof then
	while not ador.eof
        if not isnull(ador("CANTIDAD").value) then
            cantidad=visualizacionNumero(ador("CANTIDAD").value,decimalfmt,thousandfmt,precisionfmt)
        else
            cantidad="0"
        end if
        if not isnull(ador("CANTIDAD_PED_ABIERTO").value) then
            cantidadPedida=visualizacionNumero(ador("CANTIDAD_PED_ABIERTO").value,decimalfmt,thousandfmt,precisionfmt)
        else
            cantidadPedida="0"
        end if
        dImporteBrutoLin=ador("IMPORTE").value*adorOrden("CAMBIO").value
        
        if not isnull(ador("IMPORTE_PED_ABIERTO").value) then
            importePedidoAb=visualizacionNumero(ador("IMPORTE_PED_ABIERTO").value*adorOrden("CAMBIO").value,decimalfmt,thousandfmt,precisionfmt)
        else
            importePedidoAb=visualizacionNumero(0,decimalfmt,thousandfmt,precisionfmt)
        end if

        if not isnull(ador("COSTES")) then dCostesLin=ador("COSTES")*adorOrden("CAMBIO")
        if not isnull(ador("DESCUENTOS")) then dDescuentosLin=ador("DESCUENTOS")*adorOrden("CAMBIO")
        dImporteTotalLin=dImporteBrutoLin+dCostesLin-dDescuentosLin
%>

<%
    numLineaPedido=formatIntString(ador("NUMLINEAPEDIDO").value,3)
%>

str+="		<TR>"
str+="			<td align='left' class=<%=mclass%>><%=numLineaPedido%></td>"
str+="			<td align='left' class=<%=mclass%>><%=JSText(ador("codart").value)%></td>"
str+="			<td align='left' class=<%=mclass%>><%=JSText(ador("art_ext").value)%></td>"
str+="			<td align='left' class=<%=mclass%>><%=JSText(ador("denart").value)%></td>"
var dest1
var dest2
dest1='<%=JSText(ador("DESTDEN").value)%>'
dest2=dest1.substr(0,20)

str += "       <TD align='left' class=<%=mclass%>><div style='height:auto;overflow:hidden'><table width=100%><tr><td width=100%><a href='javascript:void(null)' onclick='DenominacionDest(\"" + "<%=JSText(ador("DESTINO").value)%>" + "\",\"" + "<%=ador("LINEAID").value%>" + "\")' title='<%=JSText(ador("DESTDEN").value)%>'>" + dest2  + "</a></td>"
str += "       <td align='left'><a href='javascript:void(null)' onclick='DenominacionDest(\"" + "<%=JSText(ador("DESTINO").value)%>" + "\",\"" + "<%=ador("LINEAID").value%>" + "\")' title='<%=JSText(ador("DESTDEN").value)%>'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table></div></TD>"

//Planes de entrega ó Fecha de entrega
<%if not bOcultarFecEntrega then %>
    <%if ador("HAYPLANES")>0 then%>
        <%if ador("PUB_PLAN_ENTREGA")=1 then%>
            str+="			<td class=<%=mclass%>><a href='javascript:void(null)' onclick='MostrarPlanesEntrega(" + "<%=ador("LINEAID").value%>" + ",\"" + "<%=ador("CODART").value%>" + "\",\"" + "<%=escape(ador("DENART").value)%>" + "\",\"" + "<%=ador("UNIDAD").value%>" + "\",\"" + "<%=ador("MON").value%>" + "\",\"" + "<%=ador("TIPORECEPCION").value%>" + "\")'><%=Den(37)%></a></td>"	           
        <%else%>
            str+="			<td class=<%=mclass%>>&nbsp;</td>"	
        <%end if%>
    <%else%>
        <%if isnull(ador("FECENTREGA")) then%>
	        str+="			<td align='left' class=<%=mclass%>>&nbsp;</td>"	
        <%else%>
	        str+="			<td align='left' class=<%=mclass%>>" + date2str(str2date('<%=VisualizacionFecha(ador("FECENTREGA").value,datefmt)%>',vdatefmtold),vdatefmt) + "</td>"	
        <%end if%>
    <%end if%>
<%end if%>

<% if tipoPedAbierto= 3 or isnull(tipoPedAbierto) then %>
    str+="			<td class=<%=mclass%> align=right><%=cantidad %></td>"	
<% end if %>

//Cantidad pedida
<% if tipoPedAbierto= 3 then %>
    str += "       <TD  class=<%=mclass%>><table width=100%><tr><td align='right' width=100%><%=cantidadPedida %></td>"
<% if cantidadPedida<>"0" then %>
    str += "       <td align='left'><a href='javascript:void(null)' onclick='MostrarDetalleAbierto(<%=IdOrden%>,<%=ador("LINEAID").value%>,<%=tipoPedAbierto %>)'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr>"
<% end if %>
    str += "</table></TD>"
<% end if %>

<% if tipoPedAbierto= 3 or isnull(tipoPedAbierto) then %>
    str += "       <TD align='left' class=<%=mclass%>><table width=100%><tr><td width=100%><a href='javascript:void(null)' onclick='DenominacionUP(\"" + "<%=JSText(ador("UNIDAD").value)%>" +"\")' title='<%=JSText(ador("UNIDEN").value)%>'><NOBR><%=JSText(ador("UNIDAD").value)%></NOBR></a></td>"
    str += "       <td align='left'><a href='javascript:void(null)' onclick='DenominacionUP(\"" + "<%=JSText(ador("UNIDAD").value)%>" + "\")' title='<%=JSText(ador("UNIDEN").value)%>'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr></table>"
    str += "</TD>"

    <%if ador("TIPORECEPCION").value=1 then %>
        str+="			<td class=<%=mclass%> align=right>&nbsp;</td>"	 
    <%else %> 
        str+="			<td class=<%=mclass%> align=right>" + num2str((<%=replace((ador("PRECIOUNITARIO").value*adorOrden("CAMBIO").value) ,",",".")%>),vdecimalfmt,vthousanfmt,vprecisionfmt) +"</td>"	
    <%end if %>
<% end if %>
//Importes de línea
str+="			<td class=<%=mclass%> align=right><table><tr><td>" + num2str((<%=replace(dCostesLin,",",".")%>) ,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</td>" 
<%if ador("HAYCOSTES")>0 then%>
    str+="<td><a href='javascript:void(null)' onclick='MostrarCostesDescuentos(1," + "<%=ador("LINEAID").value%>" + ",\"" + "<%=adorOrden.fields("MON").value%>" + "\"," 
    str+=<%=replace(adorOrden.fields("CAMBIO").value,",",".")%> + "," + <%=replace(dImporteBrutoLin,",",".")%> + "," 
    str+=<%=replace(dCostesLin,",",".")%> + "," + <%=replace(dDescuentosLin,",",".")%> + "," 
    str+=<%=replace(dImporteTotalLin,",",".")%> + ")' title='<%=Den(43)%>'><IMG border=0 SRC='../images/masinform.gif'></a></td>"     
<%end if%>	
str+="			</tr></table></td>"
str+="			<td class=<%=mclass%> align=right><table><tr><td>" + num2str((<%=replace(dDescuentosLin,",",".")%>) ,vdecimalfmt,vthousanfmt,vprecisionfmt) +"</td>"
<%if ador("HAYDESCUENTOS")>0 then%>
    str+="<td><a href='javascript:void(null)' onclick='MostrarCostesDescuentos(1," + "<%=ador("LINEAID").value%>" + ",\"" + "<%=adorOrden.fields("MON").value%>" + "\"," 
    str+=<%=replace(adorOrden.fields("CAMBIO").value,",",".")%> + "," + <%=replace(dImporteBrutoLin,",",".")%> + "," 
    str+=<%=replace(dCostesLin,",",".")%> + "," + <%=replace(dDescuentosLin,",",".")%> + "," 
    str+=<%=replace(dImporteTotalLin,",",".")%> + ")' title='<%=Den(43)%>'><IMG border=0 SRC='../images/masinform.gif'></a></td>"     
<%end if%>	
str+="			</tr></table></td>"
str+="			<td class=<%=mclass%> align=right>" + num2str((<%=replace(dImporteTotalLin,",",".")%>) ,vdecimalfmt,vthousanfmt,vprecisionfmt) +"</td>"	
//Importe pedido
<%if tipoPedAbierto= 2 then %>	
    str += "       <TD class=<%=mclass%>><table width=100%><tr><td align='right' width=100%><%=importePedidoAb %></td>"
    <% if importePedidoAb<>0 then %>
        str += "       <td align='left'><a href='javascript:void(null)' onclick='MostrarDetalleAbierto(<%=IdOrden%>,<%=ador("LINEAID").value%>,<%=tipoPedAbierto %>)'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr>"
    <% end if %>
    str += "</table></TD>"
<% end if %>

str+="<TD><input TYPE=hidden id='Linea" + <%=i%> + "' NAME='Linea" + <%=i%> + "' VALUE=<%=ador("LINEAID").value%>></TD>"
str+="<TD><input TYPE=hidden id='FecEntrega" + <%=i%> + "' NAME='FecEntrega" + <%=i%> + "' VALUE='" + date2str(str2date('<%=VisualizacionFecha(ador("FECENTREGA").value,datefmt)%>',vdatefmtold),vdatefmt) + "'></TD>"
str+="		</TR>"		

// Centro de coste  partida presupuestaria
<%
if (bAccesoFSSM and iModoImp=1) or not isnull(ador("ANYO_PED_ABIERTO").value) then %>
str+="	<TR>" 
str+="		<TD>&nbsp;</TD>"   
<%if (bAccesoFSSM and iModoImp=1) then %>     
    <%if not isnull(ador("UON4").value) then
        centro_sm=ador("UON4").value    
    elseif not isnull(ador("UON3").value) then
        centro_sm=ador("UON3").value
    elseif not isnull(ador("UON2").value) then
        centro_sm=ador("UON2").value
    else
        centro_sm=ador("UON1").value
    end if %>
    str+="		<TD align='left' class=cabecera><%=den(36)%></TD>"
    str+="		<TD align='left' class=<%=mclass%>><%=JSText(centro_sm)%><%=iif(isnull(ador("CCOSTE_DEN").value),""," - ")%><%=JSText(ador("CCOSTE_DEN").value)%></TD>"
    str+="		<TD align='left' class=cabecera><%=sDenPPresupuestaria%></TD>"
    str+="		<TD align='left' class=<%=mclass%> colspan='2'><%=JSText(ador("PRES").value)%><%=iif(isnull(ador("PP_DEN").value),""," - ")%><%=JSText(ador("PP_DEN").value)%></TD>"
<%end if %>
<%if not isnull(ador("ANYO_PED_ABIERTO").value) then %>
    str +="		<TD align='left' class=cabecera><%=Den(57)%></TD>"

    str += "       <TD align='left' class=<%=mclass%> colspan='2'><table width=100%><tr><td width=100%><%=JSText(ador("ANYO_PED_ABIERTO").value)%>/<%=ador("PEDIDO_PED_ABIERTO").value%>/<%=ador("ORDEN_PED_ABIERTO").value%></td>"
    
    str += "       <td align='left'><a href='javascript:void(null)' onclick='MostrarDetalleAbierto2(<%=IdOrden%>,<%=ador("LINEAID").value%>)'><IMG border=0 SRC='../images/masinform.gif'></a></td></tr>"
    
    str += "</table></TD>"

<%end if %>
str+="	</TR>" 
<%end if %>

// Ahora muestra el proceso,las observaciones y los campos personalizados
<%set adorAtrL = oOrden.DevolverAtributosLinea(ador("LINEAID").value,CiaComp)
  if not (adorAtrL is nothing) or not isnull(ador("CODPROCE").value) or not isnull(ador("OBS").value)  or not isnull(ador("DENCAMPO1").value)  or not isnull(ador("DENCAMPO2").value) then %>
	  str += "	<TR><TD>&nbsp;</TD><TD colspan=10><TABLE width='100%' CELLSPACING=0>"

	  <%if not isnull(ador("CODPROCE").value) then %>
		str += "	<TR align='left'>"
		str += "	<TD class='tablasubrayadaright'><div style='width:100px;height:auto;overflow:hidden'><%=den(29)%></div></TD>"
		str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;<%=JSText(ador("ANYOPROCE").value)%>/<%=JSText(ador("GMN1").value)%>/<%=JSText(ador("CODPROCE").value)%> &nbsp; <%=JSText(ador("DENPROCE").value)%></div></TD>"
		str += "	</TR>"
	  <%end if%>
	
	  str += "	<TR align='left'>"
	  str += "	<TD class='tablasubrayadaright'><div style='width:100px;height:auto;overflow:hidden'><%=den(9)%></div></TD>"
	  str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;<%=JSText(ador("OBS").value)%></div></TD>"
	  str += "	</TR>"
	
	  <%if not isnull(ador("DENCAMPO1").value) then %>
		  str += "	<TR align='left'>"
		  str += "	<TD class='tablasubrayadaright'><div style='width:100px;height:auto;overflow:hidden'><%=JSText(ador("DENCAMPO1").value)%></div></TD>"
		  str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;<%=JSText(ador("VALOR1").value)%></div></TD>"
		  str += "	</TR>"
	  <%end if%>
	
	  <%if not isnull(ador("DENCAMPO2").value) then %>
		  str += "	<TR align='left'>"
		  str += "	<TD class='tablasubrayadaright'><div style='width:100px;height:auto;overflow:hidden'><%=JSText(ador("DENCAMPO2").value)%></div></TD>"
		  str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;<%=JSText(ador("VALOR2").value)%></div></TD>"
		  str += "	</TR>"
	  <%end if%>

	  <%if not  (adorAtrL is nothing) then 
	  while  not adorAtrL.EOF%>
		  str += "	<TR>"
		  str += "	<TD align='left' class='tablasubrayadaright'><div style='width:100px;height:auto;overflow:hidden'><%=JSText(adorAtrL("DEN").value)%></div></TD>"
		<%if adorAtrL("TIPO").value=1 then%> //texto
		   str += "	<TD align='left' class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;<%=JSText(adorAtrL("VALOR_TEXT").value)%></div></TD>"
		<%end if%>
		<%if adorAtrL("TIPO").value=2 then%> //NUMERO
		   str += "	<TD class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;" + num2str(<%=JSNum(adorAtrL("VALOR_NUM").value)%>,vdecimalfmt,vthousanfmt,vprecisionfmt) + "</div></TD>"
		<%end if%>
		<%if adorAtrL("TIPO").value=3 then%>
		   str += "	<TD align='left' class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;" + date2str(<%=JSDate(adorAtrL("VALOR_FEC").value)%>,vdatefmt) + "</div></TD>"
		<%end if%>
		<%if adorAtrL("TIPO").value=4 then%>
		  <%if isnull(adorAtrL("VALOR_BOOL").value) then%>
		    str += "	<TD align='left' class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;</div></TD>"
		  <%elseif adorAtrL("VALOR_BOOL").value=0 then%>
		    str += "	<TD align='left' class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;<%=JSText(den(28))%></div></TD>"
		  <%else%>
		    str += "	<TD align='left' class='tablasubrayadaleft'><div style='width:500px;height:auto;overflow:hidden'>&nbsp;<%=JSText(den(27))%></div></TD>"
		  <%end if
		end if%>		  
		  str += "	</TR>"
		  <%adorAtrL.movenext
      wend
      adorAtrL.close
	  end if%>

    <% 
	sAdjuntosLinea=""
	set oLinea = oRaiz.Generar_CLinea()
	oLinea.ID = ador("LINEAID").value
	set oAdjuntos = oLinea.cargarAdjuntos(CiaComp,false)
	if oAdjuntos.count > 0 then
    %>
		str += "	<tr>"
		str += "	<td class='tablasubrayadaright'><div style='width:100px;height:auto;overflow:hidden'><%=den(10)%>"
    <%if isempty(oLinea.ObsAdjun) then%>
        str += "        <a href='javascript:void(null)' onclick='verAdjuntosLinea(" + "<%=IdOrden%>" + "," + "<%=oLinea.ID%>" + ",\"\")'><img border = 0 src= '../images/clip.gif'></A>"
    <%else%>
        str += "        <a href='javascript:void(null)' onclick='verAdjuntosLinea(" + "<%=IdOrden%>" + "," + "<%=oLinea.ID%>" + "," + <%=nulltostr(oLinea.ObsAdjun)%> + ")'><img border = 0 src= '../images/clip.gif'></A>"
    <%end if%>
        str += "	</div></td>"
    <%
        for each oAdjunto in oAdjuntos 
			sAdjuntosLinea=sAdjuntosLinea & oAdjunto.Nombre & " ; "
		next
		sAdjuntosLinea=mid(sAdjuntosLinea,1,len(sAdjuntosLinea)-3)
	%>

		str += "	<td class='tablasubrayadaleft'><div style='width:400px;height:auto;overflow:hidden'>&nbsp;<%=JSText(sAdjuntosLinea)%></div></td>"
		str += "	</tr>"
	<%
		set oAdjuntos=nothing
	end if
	%>
	  
	  str += "	</TABLE></TD></TR>"
<%end if
set adorAtrL=nothing%>

<%		if mclass="filaImpar" then
			mclass="filaPar"
		else
			mclass="filaImpar"
		end if
		
		i=i+1
		ador.movenext
	wend
end if	
%>

str+="	   </table>"
str+="	  </tr>"
str+="</table>"	
str+="<input TYPE=hidden id=txtcontador NAME=txtcontador VALUE= " + <%=i%> + ">"
document.getElementById("divAceptar").innerHTML=str	
}

function dibujarDirFac()
{
	var Row
	Row=document.getElementById("DirFac")
	<%if bMostrarDirFact then%>
		Row.style.display='inline'
	<%else%>
		Row.style.display='none'		
	<%end if%>
}

function verAdjuntosOrden(id)
{
   window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/adjuntosorden.asp?Orden=" + id, "_blank", "top=5,left=5,width=600, height=250,directories=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes")
}

function verAdjuntosLinea(orden,id,obs)
{
   window.open("<%=Application("RUTASEGURA")%>script/seguimpedidos/adjuntoslinea.asp?Orden=" + orden + "&Linea=" + id + "&Obs=" + obs, "_blank", "top=5,left=5,width=600, height=250,directories=no,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes")
}

</SCRIPT>
<table>
<tr><td></td></tr>
<tr>
<td><h1><%=sHeader%>&nbsp;<%=adorOrden("anyo").value%> / <%=adorOrden("numpedido").value%> / <%=adorOrden("numorden").value%>&nbsp;&nbsp;&nbsp;  <%=replace(FormatNumber(dImporteTotal,2),",",".")%>&nbsp;<%=adorOrden("MON").value%></h1></td>
<td>
  <table border=0>
     <TR bgcolor=white COLSPAN=2>
		<TD><%=den(3)%> &nbsp; <%=adorOrden("DENEMPRESA").value%></TD>
	</TR>
	<TR id="DirFac" bgcolor=white COLSPAN=2>
		<TD><%=den(33)%> &nbsp; <%=sDirFac%></TD>
	</TR>
    <TR bgcolor=white COLSPAN=2>
		<TD><%=den(4)%> &nbsp; <%=VisualizacionFecha(adorOrden("FECHA").value,datefmt)%></TD>
	</TR>
	<TR>
		<%select case Estado
		case 21
			If Not bAccesoFSSM Then%>
		<TD><input class=button type=submit name=cmdAceptar value='<%=den(5)%>' onclick='return aceptar()'></TD>
			<%Else%>
			<TD></TD>
			<%End If%>
		<%case 3%>
		<TD><input class=button type=submit id=cmdEnviar name=cmdEnviar value='<%=den(30)%>' onclick='return enviar()'></TD>
		<%case else%>
		<TD></TD>
		<%end select%>
	</TR>
  </table>
</td>

<td width="2%" align="right">
	<a href="javascript:void(null)" onclick="exportarPedido()"><img border="0" SRC="../images/excelg.GIF" WIDTH="28" HEIGHT="28"></a>
</td>
<td width="2%" align="right">
	<a href="javascript:void(null)" onclick="imprimirPedido()"><img border="0" SRC="../images/impresora.GIF" WIDTH="41" HEIGHT="38"></a>
</td>
</TR>
<tr><td colspan="4" align="right"><a href="<%=application("RUTASEGURA")%>script/seguimpedidos/seguimclt.asp?Idioma=<%=Idioma %>&busq=<%=strBusqueda %>&PedidosAbiertos=<%=PedidosAbiertos %>">&lt;&lt; <%=den(31) %></a> &nbsp; &nbsp;</td></tr>
</table>
<div align=center name=divAceptar id=divAceptar>
</div>

<form name=frmAceptar method=post target action="pedidos21.asp">
<input type=hidden name=txtIdioma value=<%=Idioma%>>
<input type=hidden name=txtIdOrden value=<%=IdOrden%>>
<input type=hidden name=txtIdPedido value=<%=IdPedido%>>
<input type=hidden name=txtCiaComp value=<%=CiaComp%>>
<input type=hidden name=txtAnyo value=<%=Anyo%>>
<input type=hidden name=txtNumPedido value=<%=NumPedido%>>
<input type=hidden name=txtNumOrden value=<%=NumOrden%>>
<input type=hidden name=txtNotificarRecept value=<%=NotificarReceptor%>>
</form>
<form name=frmRechazar method=post target action="pedidos22.asp">
<input type=hidden name=txtIdioma value=<%=Idioma%>>
<input type=hidden name=txtIdOrden value=<%=IdOrden%>>
<input type=hidden name=txtIdPedido value=<%=IdPedido%>>
<input type=hidden name=txtCiaComp value=<%=CiaComp%>>
<input type=hidden name=txtAnyo value=<%=Anyo%>>
<input type=hidden name=txtNumPedido value=<%=NumPedido%>>
<input type=hidden name=txtNumOrden value=<%=NumOrden%>>
<input type=hidden name=txtNotificarRecept value=<%=NotificarReceptor%>>
</form>
<P>&nbsp;</p>

</BODY>
</HTML>

<%
adorOrden.close
set adorOrden= nothing
ador.close
if not adorAtr is nothing then 
	adorAtr.close
	set adorAtr=nothing
end if
set oPersona=nothing
set ador=nothing
set oOrden=nothing
set oraiz = nothing

%>
