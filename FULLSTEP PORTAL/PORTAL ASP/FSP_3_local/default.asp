﻿<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader "Pragma", "no-cache" %>
<% Response.Expires = -1 %> 

<% 

sQry= Request.QueryString ()

if sQry="" then
	sQry = "Idioma=SPA"
	Idioma = "SPA"
else
	Idioma =request("Idioma") 
	if Idioma = "" then
		sQry = sQry & "&Idioma=SPA"
		Idioma = "SPA"
	end if

end if

IF UCase(APPLICATION("NOMPORTAL"))=UCase("GES") then
	if Idioma = "SPA" then
		Response.Redirect "http://www.gestamp.com/proveedores/login"
	else
		Response.Redirect "http://www.gestamp.com/suppliers/login"
	end if
end if

if trim(Request.ServerVariables("HTTPS"))="on" then
	Response.Redirect Application ("RUTASEGURA") & "script/proveedor?" & sQry
else
	Response.Redirect Application ("RUTANORMAL") & "custom/" & Application ("NOMPORTAL") & "/public/default.asp?" & sQry
end if
%>