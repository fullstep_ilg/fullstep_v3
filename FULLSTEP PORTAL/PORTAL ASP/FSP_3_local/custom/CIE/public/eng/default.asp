﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>:: The Fullstep Platform::</title>
<link href="../estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 5px;
	margin-top: 0px;
	background-image: url();
	background-color: #EBEBEB;
}
.Estilo1 {font-size: x-large}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--



function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
//-->
</script>
</head>

<body onLoad="MM_preloadImages('../images/buyers_r.gif','../images/EP_buyers_r.gif','../images/español_r.gif','../images/contact_r.gif','../images/tutorials_r.gif','../images/contact_r.gif','../images/suppliers_r.gif')">
<br>
<br>
<!--inicio tabla contenedora -->
<div align="center">
    <table width="934" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="60" bgcolor="#FFFFFF"><div align="left">
          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="8%">&nbsp;</td>
              <td width="72%" valign="bottom"><a href="http://www.fullstep.com" target="_blank"><img src="../images/logo_eng.png" width="200" height="54" border="0"></a></td>
            <td width="20%" class="textosres"><div align="center"><a href="../default.asp">español</a></div></td>
            </tr>
          </table>
        </div></td>
        <td width="9" rowspan="4" background="../images/sombra_dcha.gif" bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
     
      <tr>
        <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="../images/GraficoPlataformaFullstep_eng.jpg" width="925" height="357"></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#FFFFFF"><table width="80%" border="0">
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="925" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
          
          <tr>
            <td width="6">&nbsp;</td>
            <td width="360" align="center" valign="bottom" background="../images/externos_eng.jpg"><table width="90%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td width="9%" height="22">&nbsp;</td>
                <td width="83%" class="textos"><div align="center"></div></td>
                <td width="8%">&nbsp;</td>
              </tr>
              <tr>
                <td height="80">&nbsp;</td>
                <td valign="middle" class="textos"><div align="center" class="subtexto"><a href="javascript:void(null)" onClick="window.open('<%=application("RUTASEGURA")%>/script/proveedor','_blank','status=yes,location=yes,menubar=yes,toolbar=yes,resizable=yes,top=0,left=0,width=' + (screen.availWidth-10) + ',height=' + (screen.availHeight-200) + ',scrollbars=yes')"> Supplier Portal</a></div></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <div align="center"></div></td>
            <td width="50">&nbsp;</td>
            <td width="360" valign="bottom" background="../images/internos_eng.jpg"><table width="99%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td width="9%" height="22">&nbsp;</td>
                <td width="83%" class="textos"><div align="center"></div></td>
                <td width="8%">&nbsp;</td>
              </tr>
              <tr>
                <td height="80">&nbsp;</td>
                <td valign="middle" class="textos"><div align="center" class="subtexto"><a href="javascript:void(null)" onclick="window.open('<%=application("RUTASEGURA")%>/gs?Idioma=eng,'_blank','status=yes,location=yes,menubar=yes,toolbar=yes,resizable=yes,top=0,left=0,width=' + (screen.availWidth-10) + ',height=' + (screen.availHeight-200) + ',scrollbars=yes')"> Negotiation </a></div>
                  <div align="center" class="subtexto"><a  href="javascript:void(null)" onClick="window.open('<%=application("RUTASEGURA")%>/fsnweb?Idioma=eng','_blank','status=yes,location=yes,menubar=yes,toolbar=yes,resizable=yes,top=0,left=0,width=' + (screen.availWidth-10) + ',height=' + (screen.availHeight-200) + ',scrollbars=yes')"><br>
                    Other applications</a> </div>
                  <br></td>
                <td>&nbsp;</td>
              </tr>
            </table></td>
            <td width="50">&nbsp;</td>
            <td width="10">&nbsp;</td>
            <td width="50"><div align="center">
              </div></td>
            <td width="290" ><table width="166" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td width="9%" height="22">&nbsp;</td>
                <td width="83%" class="textos"><div align="center"><img src="../images/soporte_txt_eng.jpg" width="261" height="26"></div></td>
                <td width="8%">&nbsp;</td>
              </tr>
              <tr>
                <td height="80">&nbsp;</td>
                <td align="center" valign="middle"><div align="center" class="subtextos">Tel. + 34 902 996 926 <br>
                  <a href="mailto: atencionalcliente@fullstep.com">atencionalcliente@fullstep.com</a></div><br>
                  <span class="subtitulo">Website optimized for 1280 x1024<br>
                  Supported browsers: Internet Explorer and Firefox</span></td>
                <td>&nbsp;</td>
              </tr>
            </table></td>
            <td width="21">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2"><img src="../images/recuadro_inf.jpg" width="932" height="22"></td>
      </tr>
    </table>
    
</div>
<!-- fin tabla contenedora -->
</body>
</html>
