﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>::Fullstep Networks - Sistemas y Gestión de Compras::</title>
<link href="estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 5px;
	margin-top: 0px;
	background-image: url(images/fondo.gif);
}
.Estilo1 {font-size: x-large}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}
//-->
</script>
</head>

<body onLoad="MM_preloadImages('images/imagen%20_chica.jpg','images/imagen%20_carros.jpg','images/imagen%20_mundo.jpg','images/imagen_cuadro.gif','images/buyers_r.gif','images/EP_buyers_r.gif','images/deutsch_r.gif','images/espa&ntilde;ol_r.gif','images/contact_r.gif','images/english_r.gif','images/tutorials_r.gif','images/contact_r.gif','images/suppliers_r.gif')">
<br><br>
<!--inicio tabla contenedora -->

<table width="815" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <!--DWLayoutTable-->
  <tr>
    <td height="89"><img src="images/logo.jpg" name="Image5" width="150" height="89" id="Image5"></td>
    <td colspan="3" align="right" valign="bottom"><table width="145" border="0" align="right" cellpadding="0" cellspacing="0">
      
    </table></td>
  </tr>
  
  <tr>
    <td height="1" colspan="4" align="center"><img src="images/linearoja.gif" width="815" height="1"></td>
  </tr>
  <tr>
    <td height="19" colspan="3"></td>
  </tr>
  <tr>
    <td width="170" height="145" valign="top"><img src="images/imagen.jpg" name="Image9" width="145" border="0" usemap="default.asp#Map" id="Image9"></td>
    <td colspan="3" rowspan="2" valign="top"><table width="620" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20">&nbsp;</td>
        <td width="615" align="left" class="titulo">Portal  de compras y aprovisionamiento </td>
      </tr>
	  
      <tr>
        <td width="20" height="223"></td>
        <td width="625" rowspan="2">
		<!--inicio tabla central-->
		<table width="609" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="298" rowspan="5" align="right"><div align="left">
              <p class="textos">Cada organizaci&oacute;n es &uacute;nica y necesita un modelo operativo de compras y aprovisionamiento adecuado a sus necesidades. La singularidad de cada organizaci&oacute;n hace que las rigideces impuestas por nuevos sistemas supongan la aparici&oacute;n de lagunas en los procesos de compras y aprovisionamiento, que acaban generando insatisfacci&oacute;n en los usuarios. <br>
                <br>
Las soluciones de FULLSTEP se adaptan para responder a lo que precisa su empresa, sin sobredimensionar las soluciones y reduciendo al m&aacute;ximo los tiempos de implantaci&oacute;n, con el objetivo de a&ntilde;adir el m&aacute;ximo valor a nuestros clientes.</p>
              </div></td>
            <td width="311" height="37">
			<table width="160" align="center" cellpadding="0" cellspacing="0">
			  <tr>
			    <td>&nbsp;</td>
			    <td><div align="left"><a href="javascript:void(null)" onclick="window.open('<%=application("RUTASEGURA")%>/script/proveedor','_blank','status=yes,location=yes,menubar=yes,toolbar=yes,resizable=yes,top=0,left=0,width=' + (screen.availWidth-10) + ',height=' + (screen.availHeight-200) + ',scrollbars=yes')"  onMouseOver="MM_swapImage('Image61','','images/proveedores_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="images/proveedores.gif" name="Image61" width="150" height="14" border="0" id="Image6"></a></div></td>
			    </tr>
			  <tr>
                <td>&nbsp;</td>
                <td><div align="left"><a href="javascript:void(null)" onclick="window.open('<%=application("RUTASEGURA")%>/gs?Idioma=spa','_blank','status=yes,location=yes,menubar=yes,toolbar=yes,resizable=yes,top=0,left=0,width=' + (screen.availWidth-10) + ',height=' + (screen.availHeight-200) + ',scrollbars=yes')"  onMouseOver="MM_swapImage('Image6','','images/compradores_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="images/compradores.gif" name="Image6" width="150" height="14" border="0" id="Image6"></a></div></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><div align="left"><a  href="javascript:void(null)" onclick="window.open('<%=application("RUTASEGURA")%>/ep?Idioma=spa','_blank','status=yes,location=yes,menubar=yes,toolbar=yes,resizable=yes,top=0,left=0,width=' + (screen.availWidth-10) + ',height=' + (screen.availHeight-200) + ',scrollbars=yes')" onMouseOver="MM_swapImage('Image7','','images/aprovisionadores_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="images/aprovisionadores.gif" name="Image7" width="170" height="14" border="0" id="Image7"></a></div></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="left">&nbsp;</td>
            </tr>
          <tr>
            <td height="85" align="left"><div align="center">
              <table width="66%"  border="0">
                  <tr>
                    <td class="solutions"><div align="center">&quot;...soluciones inteligentes creadas por compradores para compradores...&quot; </div></td>
                  </tr>
                          </table>
              </div></td>
          </tr>
         
        </table>
		 <!--fin tabla central-->
		</td>
      </tr>
      <tr>
        <td></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="170" height="107" valign="top"><table width="155" cellspacing="0" cellpadding="0">
      <tr>
        <td width="10">&nbsp;</td>
        <td width="149">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><a href="ger/default.asp" onMouseOver="MM_swapImage('Image11','','images/deutsch_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="images/deutsch.gif" name="Image11" width="64" height="12" border="0" id="Image11"></a></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><a href="eng/default.asp" onMouseOver="MM_swapImage('Image21','','images/english_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="images/english.gif" name="Image21" width="64" height="12" border="0" id="Image21"></a></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="19" colspan="4">&nbsp;</td>
  </tr>
  
  
  <tr>
    <td height="10" colspan="4" valign="top" bgcolor="#E4E4E4" class="subtitulo"><div align="center"><img src="images/lineagris.gif" width="1" height="1"></div>      </td>
  </tr>
  <tr>
    <td height="50" bgcolor="#E4E4E4" class="subtitulo"><div align="left">
      <div align="left">&nbsp;FULLSTEP NETWORKS, S.L.<br>
        &nbsp;Antonio de Cabezón, 83, 4º <br>
    &nbsp;28700 Madrid </div></td>
    <td width="325" valign="top" bgcolor="#E4E4E4" class="subtitulo"><div align="center"></div></td>
   
    <td width="320" bgcolor="#E4E4E4" class="subtitulo"><div align="center">Internet explorer 6.0 o superior requerido.<br>
          <nobr>Para conseguir la &uacute;ltima versi&oacute;n,<a href="http://www.microsoft.com/windows/ie_intl/es/default.mspx" target="_blank"> pulse aqu&iacute;</a>.</nobr><br>
    </div></td>
  </tr>
  <tr>
    <td height="20" colspan="4" bgcolor="#6D93A2" class="registro">
      <div align="center"></div>
    <div align="center" class="subtit">COPYRIGHT &copy; 2006 - FULLSTEP NETWORKS</div></td>
  </tr>
</table>
<!-- fin tabla contenedora -->


<map name="Map">
  
  <area shape="rect" coords="72,3,145,74" alt="Any questions? Please call us +34 912 962 000" onMouseOver="MM_swapImage('Image9','','images/imagen%20_chica.jpg',1)" onMouseOut="MM_swapImgRestore()">
  <area shape="rect" coords="2,1,73,72"    onMouseOver="MM_swapImage('Image9','','images/imagen%20_carros.jpg',1)" onMouseOut="MM_swapImgRestore()">
  <area shape="rect" coords="1,73,72,144"  onMouseOver="MM_swapImage('Image9','','images/imagen%20_mundo.jpg',1)" onMouseOut="MM_swapImgRestore()">
  <area shape="rect" coords="73,73,146,143"  onMouseOver="MM_swapImage('Image9','','images/imagen_cuadro.gif',1)" onMouseOut="MM_swapImgRestore()">
</map>
</body>
</html>
