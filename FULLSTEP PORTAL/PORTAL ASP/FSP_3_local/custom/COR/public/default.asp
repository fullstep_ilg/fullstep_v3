<!DOCTYPE html>
<html lang="es-ES">
    <head>
        <meta charset="utf-8" />  
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />  
        <meta name="viewport" content="width=device-width, initial-scale=1" />  
        
        <title>Fullstep</title>
        
        <meta name="title" content=""/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        
        <link rel="canonical" href="/" />

		<link href="css/reset.css" rel="stylesheet" type="text/css" />        
    	<link href="css/font-awesome.min.css" rel="stylesheet" />  
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
        <link href="css/style.css" rel="stylesheet" />  
        <link href="css/graficos.css" rel="stylesheet" /> 
        <!--[if lt IE 9]>
          	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
           
        
    </head>
    <body>       
        
        
        <header id="heading">             
            
      		<div class="container">  
            
           		<div class="text"> 
            
                    <div class="logotipo">
            			<a href="#" title="Fullstep"><img src="images/logo_fullstep.png" alt="Fullstep" /></a>
                    </div>  
                    
                    <h1>ACCESO PRINCIPAL</h1> 
                    <!--
                         
                    <nav>                      
                        <ul>
                            <li><a href="#" title="Ayuda">Ayuda</a></li>
                            <li>|</li>
                            <li>
                            	<strong class="trigger">Idioma <i class="fa fa-caret-down"></i></strong>
                            	<ul>
	                            	<li><a href="#" title="Español">Español</a></li>
	                            	<li><a href="#" title="English">English</a></li>
                            	</ul>
                            </li>                                 
                        </ul> 
                    </nav>                   
                           -->     
           		</div>   
                    
           	</div>
            
        </header>        
    			
        
        
    	<section id="home"> 
            
            <div class="container">
            
           
           		<div class="section-graficos">
                
                 <div class="contenedor_graficos">
                 	<div class="raton"><img src="images/pase_raton.gif" alt="Pase el ratón por las fases del Ciclo del Gasto" title="Pase el ratón por las fases del Ciclo del Gasto"></div>
                    <div class="graficos-circulos">
                   
            <div id="texto_cabecera"></div>
            <div id="global">
                <div id="bloque1" data-img-src="Punto1.png"></div>
                <div id="bloque2" data-img-src="Punto2.png"></div>
                <div id="bloque3" data-img-src="Punto3.png"></div>
                <div id="bloque4" data-img-src="Punto4.png"></div>
                <div id="bloque5" data-img-src="Punto5.png"></div>
                <div id="bloque6" data-img-src="Punto6.png"></div>
            </div>
                    
                    
                    </div>
                 
                 </div>
                 
                 
                <div class="acceso-graficos"> <a  href="javascript:void(null)" onClick="window.open('<%=application("RUTASEGURA")%>/fsnweb?Idioma=spa','_blank','status=yes,location=yes,menubar=yes,toolbar=yes,resizable=yes,top=0,left=0,width=' + (screen.availWidth-10) + ',height=' + (screen.availHeight-200) + ',scrollbars=yes')"><img src="images/usuarios_internos.jpg" alt="Usuarios internos"></a>
                <a href="javascript:void(null)" onClick="window.open('<%=application("RUTASEGURA")%>/script/proveedor','_blank','status=yes,location=yes,menubar=yes,toolbar=yes,resizable=yes,top=0,left=0,width=' + (screen.availWidth-10) + ',height=' + (screen.availHeight-200) + ',scrollbars=yes')"><img src="images/portal_proveedores.jpg" alt="Portal Proveedores"></a> 
                
                <a href="javascript:void(null)" onclick="window.open('<%=application("RUTASEGURA")%>/gs?Idioma=spa','_blank','status=yes,location=yes,menubar=yes,toolbar=yes,resizable=yes,top=0,left=0,width=' + (screen.availWidth-10) + ',height=' + (screen.availHeight-200) + ',scrollbars=yes')"><img src="images/global_sourcing.jpg" alt="Global Sourcing"></a>  
            
            	</div>
                
                
            </div>
              
    	</section>
        
        
        <footer id="footer">
        
        	<div class="container">            
            
            	<div class="footer-soporte">
                	
               	  <div class="enlaces-footer">
                        <div class="ico-consultoria"><a href="#">Consultoria</a></div>
                        <div class="separa"></div>
                        <div class="ico-plataforma"><a href="#">Plataforma <br>de compras</a></div>
                        <div class="separa"></div>
                        <div class="ico-servicios"><a href="#">Servicios de<br>
compras y bpo</a></div>
                        </div>
                
                    <div class="soporte">
                    
                        <div class="soporte-titular">
                            <h3>SOPORTE A USUARIOS</h3>
                            <p><strong>Tel. 902 996 926</strong><br /><a href="mailto:atencionalcliente@fullstep.com">atencionalcliente@fullstep.com</a></p>
                        </div>
                        
                        <div class="soporte-horario">
                            <p><strong>Horario de Atención al Cliente</strong></p>
                            <p>Lunes a Jueves:8:00 - 21:00</p>
                            <p>Viernes: 8:00 - 19:00</p>
                        </div>
                        
                    </div>
                    
              	</div>
            
           		<div class="text">                
                    
                    <a href="#"><img src="images/fullstep.png" alt="fullstep" /></a> 
                    
                <!--    <p><a href="#">Aviso Legal</a> | <a href="#">Política de Cookies</a></p>    -->                
                    <p>&copy; FULLSTEP Todos los derechos reservados.</p>                            
                    
               	</div>
               
            </div>
            
        </footer>
                       
                       
        <script type="text/javascript" src="js/jquery.min.js"></script>
        
        <script type="text/javascript" src="js/main.js"></script>  
             
    	<script type="text/javascript" src="js/graficos.js"></script> 
    
    </body>
</html>