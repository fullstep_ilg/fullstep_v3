﻿<%@ Language=VBScript %>

<%

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>European VAT number</title>
</head>
<body>
<table border="0" width="490" cellspacing="0" cellpadding="0" height="9">
  <tr>
    <td width="89" height="9" valign="bottom">
      <p align="center"><font face="verdana" size="1"></font></td>

    <td width="401" height="9" valign="middle" bgcolor="#094176">
		<%if UCase(Application("NOMPORTAL")) = UCase("gestamp") then%>
		<font face="verdana" color="white" size="1"><b> &gt;&gtUCase(;Company Code&lt;&lt;</b></font>
		<%else%>
      <font face="verdana" color="white" size="1"><b> &gt;&gt;European VAT number&lt;&lt;</b></font></td>
		<%end if%>
    </tr>

  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1"><br><br><br>
      If you do not know it, it is usually built with your country code plus your VAT number.  It is the first of the three codes needed to access the suppliers' private zone once your company is registered.</font></td>
  </tr>
  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1"><br>
      <b>Example</b></font></td>
  </tr>
  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1">
      Company: MYCOMPUTER, S.L. with TAX CODE: F20025291</font></td>
  </tr>
  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1" COLOR="RED">
      COMPANY CODE = EUROPEAN VAT NUMBER = <b>ESF20025291</b></font></td>
  </tr>
</table>
<p><span style="position: absolute; left: -1; top: 2"><img border="0" src="../../<%=application("NOMPORTAL")%>/images/logoayuda.gif"></span></p>

</body>
</html>