﻿<%@ Language=VBScript %>

<%

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<%if UCase(Application("NOMPORTAL")) = UCase("mut") then%>
	<title>Erabiltzailearen kodea</title>
	<%else%>
<title>Benutzercode</title>
		<%end if%>	
</head>
<body>
<table border="0" width="490" cellspacing="0" cellpadding="0" height="9">
  <tr>
    <td width="89" height="9" valign="bottom">
      <p align="center"><font face="verdana" size="1"></font></p></td>

    <td width="401" height="9" valign="center" bgcolor="#094176">
      <font face="verdana" color="white" size="1"><b>
		  <%if UCase(Application("NOMPORTAL")) = UCase("mut") then%>
      &gt;&gt;Erabiltzailearen kodea&lt;&lt;
	<%else%> 
		  &gt;&gt; Benutzercode &lt;&lt;</b></font>
		  <%end if%>
		  </td>

    </tr>

  <tr>
    <td width="475" valign="bottom" colspan="2" align="middle">
      <p align="left"><font face="verdana" size="1"><br><br><br>
		  <%if UCase(Application("NOMPORTAL")) = UCase("mut") then%>
Erabiltzailearen pasahitza pertsonala (gutxienez sei karaktere). Hornitzaileentzako sarbidean erregistroa baimendua egon eta gero, behar diren hiru gakoetako bigarrena da.<br><br>
Kodea ez da "Konpainiaren datu orokorretan" erregistratu zenuen konpainiaren kodearekin bat etorri behar.
<%else%>		  
      Persönliches Passwort des Benutzers (mind. sechs Zeichen); dies ist der zweite von drei notwendigen Schlüsseln, um nach Anmeldung Zugang zum Privatbereich der Anbieter zu erhalten.
      <br><br>Dieser Code muss nicht mit dem Firmencode übereinstimmen, den Sie unter &quot;Allgemeine Angaben zur Gesellschaft&quot; registriert haben.</font></p></td>
		  <%end if%> 
  </tr>
</table>
<p><span style="LEFT: -1px; POSITION: absolute; TOP: 2px"><img src="../../<%=application("NOMPORTAL")%>/images/logoayuda.gif" border="0"></span></p>

</body>
</html>