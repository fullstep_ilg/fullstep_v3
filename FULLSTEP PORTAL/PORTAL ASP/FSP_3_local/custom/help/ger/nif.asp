﻿<%@ Language=VBScript %>

<%

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%if UCase(Application("NOMPORTAL")) = UCase("ficosa") OR UCase(Application("NOMPORTAL")) = UCase("huf") then%>
<title>D.U.N.S</title>
	<%else if UCase(Application("NOMPORTAL")) = UCase("mut") then%>
		<title>NIF</title>
<%else%>
<title>Steuernummer</title>
<%end if%>
<%end if%>
</head>
<body>
<table border="0" width="490" cellspacing="0" cellpadding="0" height="9">
  <tr>
    <td width="89" height="9" valign="bottom">
      <p align="center"><font face="verdana" size="1"></font></td>

    <td width="401" height="9" valign="middle" bgcolor="#094176">
      <font face="verdana" color="white" size="1"><b>
		  <%if UCase(Application("NOMPORTAL")) = UCase("ficosa") or UCase(Application("NOMPORTAL")) = UCase("HUF") then%>
		&gt;&gt; D.U.N.S Nummer ®
		<%else if UCase(Application("NOMPORTAL")) = UCase("mut") then%>	  
		NIF	
		<%else%>
		&gt;&gt; Steuernummer
		<%end if%>
			<%end if%>
			</b></font></td>
    </tr>
  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1"><br><br><br>
      <%if UCase(Application("NOMPORTAL")) = UCase("ficosa") or UCase(Application("NOMPORTAL")) = UCase("HUF") then%>
      Dies ist ein neunstelliger Zahlencode, der weltweit als Standard zur eindeutigen Identifizierung von Unternehmen auf der ganzen Welt gilt.
      Wenn Sie noch nicht über einen solchen Code verfügen, können Sie ihn kostenlos bei dieser Adresse anfordern: <a target="_blank" href="http://www.dnb.com/us/duns_update/">http://www.dnb.com/us/duns_update/</a>
		  <%else if UCase(Application("NOMPORTAL")) = UCase("mut") then%>	  
		Idatzi zure zerga-identifikazio zenbakia, espazio edo marratxorik gabe, zenbakiak eta letrak soilik (Espainiako NIF adibidea: B82661935)	
      <%else%>
      Tragen Sie Ihre Steuernummer ohne Zwischenräume oder Bindestriche ein.
      <%end if%>
		  <%end if%>
      </font></td>
  </tr>
</table>
<p><span style="position: absolute; left: -1; top: 2"><img border="0" src="../../<%=application("NOMPORTAL")%>/images/logoayuda.gif"></span></p>

</body>
</html>