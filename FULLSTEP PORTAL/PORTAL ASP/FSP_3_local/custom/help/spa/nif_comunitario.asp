﻿<%@ Language=VBScript %>

<%

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%if UCase(Application("NOMPORTAL")) = UCase("federlan") or UCase(Application("NOMPORTAL")) = UCase("ormazabal") then%>
<title>NIF Comunitario de su empresa</title>
<%else%>
<title>Código de Compañía</title>
<%end if%>
</head>
<body>
<table border="0" width="490" cellspacing="0" cellpadding="0" height="9">
  <tr>
    <td width="89" height="9" valign="bottom">
      <p align="center"><font face="verdana" size="1"></font></td>

    <td width="401" height="9" valign="middle" bgcolor="#094176">
	<%if UCase(Application("NOMPORTAL")) = UCase("gestamp") then%>
	  <font face="verdana" color="white" size="1"><b>&gt;&gt;Código de compañía&lt;&lt;</b></font></td>
	<%else%>
      <font face="verdana" color="white" size="1"><b>&gt;&gt;NIF Comunitario de su empresa.&lt;&lt;</b></font></td>
	<%end if%>
    </tr>

  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1"><br><br>
	  <%if UCase(Application("NOMPORTAL")) = UCase("gestamp") then%>
      <strong>Empresas españolas</strong>: introduzca su número de identificación fiscal, sin espacios ni guiones, precedido de la sigla del país (ES).<br>
      Ejemplo: Empresa española con NIF B82669333.  Introduzca: ESB82669333
<br><br>
	  <strong>Empresas no españolas</strong>: introduzca su número de identificación fiscal, precedido de la sigla de su país (2 letras). 
        Ejemplo: Empresa francesa con NIF 482239174.  Introduzca: FR482239174
		<%else if UCase(Application("NOMPORTAL")) = UCase("yell") then%>El código de compañía es la primera de las tres claves necesarias para acceder a la zona privada de proveedores una vez solicitada el alta.<br><br>

        <strong>ESPAÑA</strong>: Para las empresas españolas el código se construye anteponiendo ES al NIF (Número de identificación fiscal). <br>
        <strong>ARGENTINA</strong>: Para las empresas argentinas el código se construye anteponiendo AR al CUIT (Código único de identificación tributaria). <br>
        <strong>CHILE</strong>: Para las empresas chilenas el código se construye anteponiendo CH al  RUT (Rol único tributario). <br>
        <strong>PERÚ</strong>: Para las empresas peruanas el código se construye anteponiendo PE al RUC (Registro único de contribuyente). <br>

      <%else%>
      Para las empresas españolas el código se construye anteponiendo <b>ES</b> al <b>NIF</b>.
Es la primera de las tres claves necesarias para acceder a la zona privada de proveedores una vez solicitada el alta.</font>
<%end if%>
<%end if%>
</td>
  </tr>
  <%IF UCase(APPLICATION("NOMPORTAL")) = UCase("federlan") or UCase(Application("NOMPORTAL")) = UCase("ormazabal") then%>
  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1"><br>
      <b>Ejemplo</b></font></td>
  </tr>
  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1">
      Empresa: MYCOMPUTER, S.L. con NIF: F20025291</font></td>
  </tr>
  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1" COLOR="RED">
      CÓDIGO COMPAÑÍA = NIF COMUNITARIO = <b>ESF20025291</b></font></td>
  </tr>
  <%end if%> 
</table>
<p><span style="position: absolute; left: -1; top: 2"><img border="0" src="../../<%=application("NOMPORTAL")%>/images/logoayuda.gif"></span></p>

</body>
</html>