﻿<%@ Language=VBScript %>

<%

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Código de usuario</title>
</head>
<body>
<table border="0" width="490" cellspacing="0" cellpadding="0" height="9">
  <tr>
    <td width="89" height="9" valign="bottom">
      <p align="center"><font face="verdana" size="1"></font></p></td>

    <td width="401" height="9" valign="center" bgcolor="#094176">
      <font face="verdana" color="white" size="1"><b>&gt;&gt; Código de usuario &lt;&lt;</b></font></td>

    </tr>

  <tr>
    <td width="475" valign="bottom" colspan="2" align="middle">
      <p align="left"><font face="verdana" size="1"><br><br><br>
      Clave personal del usuario (mín. seis caracteres). Es la segunda de las
      tres claves necesarias para acceder a la zona privada de proveedores una
      vez aceptada el alta.<br>
      Este código no tiene porque coincidir con el código de compañía que
      registró en &quot;Datos generales de Compañía&quot;</font></p></td>
  </tr>
</table>
<p><span style="LEFT: -1px; POSITION: absolute; TOP: 2px"><img src="../../<%=application("NOMPORTAL")%>/images/logoayuda.gif" border="0"></span></p>

</body>
</html>