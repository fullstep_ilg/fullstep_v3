﻿<%@ Language=VBScript %>

<%

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%if UCase(Application("NOMPORTAL")) = UCase("ficosa") then%>
<title>D.U.N.S</title>
<%else%>
<title>N.I.F</title>
<%end if%>
</head>
<body>
<table border="0" width="490" cellspacing="0" cellpadding="0" height="9">
  <tr>
    <td width="89" height="9" valign="bottom">
      <p align="center"><font face="verdana" size="1"></font></td>

    <td width="401" height="9" valign="middle" bgcolor="#094176">
      <font face="verdana" color="white" size="1"><b><%if UCase(Application("NOMPORTAL")) = UCase("ficosa") or UCase(Application("NOMPORTAL")) = UCase("HUF") then%>
		&gt;&gt; Número D.U.N.S®
		<%else%>
		&gt;&gt; N.I.F
		<%end if%></b></font></td>
    </tr>
  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1"><br><br>
      <%if UCase(Application("NOMPORTAL")) = UCase("ficosa") or UCase(Application("NOMPORTAL")) = UCase("HUF") then%>
      Es una codificación de nueve dígitos reconocida como stardard universal para identificar de forma inequívoca a empresas de todo el mundo.
      Si todavía no dispone de este código, puede solicitarlo sin coste desde la dirección <a target="_blank" href="http://www.dnb.com">http://www.dnb.com</a>
      <%else%>
	  <%if UCase(Application("NOMPORTAL")) = UCase("gestamp") then%>
      <strong>Empresas españolas</strong>: introduzca su número de identificación fiscal, sin espacios ni guiones.<br>
      Ejemplo: Empresa española con NIF B82669333.  Introduzca: B82669333<br><br>
	  <strong>Empresas no españolas</strong>: introduzca su número de identificación fiscal, sin espacios ni guiones, precedido de la sigla de su país.  Este código debe ser idéntico al introducido en el campo &quot;Código de compañía&quot;.  <br>
	  Ejemplo: Empresa francesa con NIF 482239174   Introduzca: FR482239174
      <%else%>
	  Introduzca su número de identificación fiscal, sin espacios ni guiones, sólo números y letras (ejemplo de NIF español: B82661935)
	  <%end if%>
	  <%end if%>
      </font></td>
  </tr>
  
<%IF UCase(APPLICATION("NOMPORTAL")) = UCase("federlan") then%>
  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1"><br>
      <b>Ejemplo</b></font></td>
  </tr>
  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1">
      Empresa: FAGOR EDERLAN</font></td>
  </tr>
  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1" COLOR="RED">
      NIF = <b>F20025292</b></font></td>
  </tr>
  
<%end if%>  
</table>
<p><span style="position: absolute; left: -1; top: 2"><img border="0" src="../../<%=application("NOMPORTAL")%>/images/logoayuda.gif"></span></p>

</body>
</html>