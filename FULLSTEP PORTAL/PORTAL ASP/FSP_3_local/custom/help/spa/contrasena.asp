﻿<%@ Language=VBScript %>

<%

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Contraseña</title>
</head>
<body>
<table border="0" width="490" cellspacing="0" cellpadding="0" height="9">
  <tr>
    <td width="89" height="9" valign="bottom">
      <p align="center"><font face="verdana" size="1"></font></td>

    <td width="401" height="9" valign="middle" bgcolor="#094176">
      <font face="verdana" color="white" size="1"><b>&gt;&gt; Contraseña &lt;&lt;</b></font></td>

    </tr>

  <tr>
    <td width="475" valign="bottom" colspan="2">
      <p align="left"><font face="verdana" size="1">
<%if UCase(Application("NOMPORTAL")) = UCase("AER") or UCase(Application("NOMPORTAL")) = UCase("SCA") or UCase(Application("NOMPORTAL")) = UCase("ILU") or UCase(Application("NOMPORTAL")) = UCase("IBC") or UCase(Application("NOMPORTAL")) = UCase("AJE") or UCase(Application("NOMPORTAL")) = UCase("HRE") or UCase(Application("NOMPORTAL")) = UCase("COS") then%>
La contraseña debe cumplir con las siguientes condiciones: 
<ul><li>La contraseña debe ser al menos de 6 caracteres. </li>
<li>Debe contener al menos una mayúscula, una minúscula, un número y un signo de puntuación.</li>
<li>No puede contener el código del usuario, o partes de código del usuario que excedan de dos caracteres consecutivos.</li></ul>
<%else%>
Contraseña del usuario (mín. seis
      caracteres) es la tercera de las tres claves necesarias para acceder a la
      zona privada de proveedores una vez solicitada el alta.
<%end if%>
</font></p></td>
  </tr>
</table>
<p><span style="position: absolute; left: -1; top: 2"><img border="0" src="../../<%=application("NOMPORTAL")%>/images/logoayuda.gif"></span></p>

</body>
</html>