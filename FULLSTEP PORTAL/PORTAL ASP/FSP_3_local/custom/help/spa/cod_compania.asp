﻿<%@ Language=VBScript %>

<%

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%if UCase(Application("NOMPORTAL")) = UCase("lcx") then%>
<title>NIF Compañía</title>
<%else%>
<title>Código de Compañía</title>
<%end if%>
</head>
<body>
<table border="0" width="490" cellspacing="0" cellpadding="0" height="9">
  <tr>
    <td width="89" height="9" valign="bottom">
      <p align="center"><font face="verdana" size="1"></font></td>

    <td width="401" height="9" valign="middle" bgcolor="#094176">
	<font face="verdana" color="white" size="1"><b>
	 <%if UCase(Application("NOMPORTAL")) = UCase("lcx") then%>
      <strong>&gt;&gt;NIF Compañía&lt;&lt;</strong>
	  <%else%> 
     &gt;&gt; Código de Compañía&lt;&lt;</b></font>
	 <%end if%>
	 </td>

    </tr>

  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1"><br><br><br>
	  <%if UCase(Application("NOMPORTAL")) = UCase("cfg") then%>
Por favor introduzca su NIF.
<%else if UCase(Application("NOMPORTAL")) = UCase("clb") or UCase(Application("NOMPORTAL")) = UCase("bcv") or UCase(Application("NOMPORTAL")) = UCase("lcx") or UCase(Application("NOMPORTAL")) = UCase("rea") then%>
Por favor introduzca el NIF de su compañía (independientemente que deba volver a escribirlo en el campo NIF que se encuentra más abajo en esta misma página). El Código de Compañía es la primera de las tres claves necesarias para acceder a la zona privada de proveedores una vez solicitada el alta. 
<%else if UCase(Application("NOMPORTAL")) = UCase("yell") then%>El código de compañía es la primera de las tres claves necesarias para acceder a la zona privada de proveedores una vez solicitada el alta.

ESPAÑA: Para las empresas españolas el código se construye anteponiendo ES al NIF (Número de identificación fiscal). 
ARGENTINA: Para las empresas argentinas el código se construye anteponiendo AR al CUIT (Código único de identificación tributaria). 
CHILE: Para las empresas chilenas el código se construye anteponiendo CH al  RUT (Rol único tributario). 
PERÚ: Para las empresas peruanas el código se construye anteponiendo PE al RUC (Registro único de contribuyente). 

<%else if Application("NOMPORTAL") = "fer" or Application("NOMPORTAL") = "qen" or Application("NOMPORTAL") = "beg" then%>
	Código de identificación de su empresa. Es la primera de las tres claves
      necesarias para acceder a la zona privada de proveedores una vez
      aceptado el alta.  Este código deberá coincidir con el CIF de su Compañía y deberá estar precedido por las 2 letras de su país (ejemplo de NIF español: ESB82661935)   
      
      <%else if UCase(Application("NOMPORTAL")) = UCase("cip") then%>Código de identificación de su empresa. Es la primera de las tres claves necesarias para acceder a la zona privada de proveedores una vez aceptado el alta. Este código deberá coincidir con el CIF de su Compañía.
<%else if UCase(Application("NOMPORTAL")) = UCase("ppl") then%>Este código es el identificador de proveedor que asigna el banco.<br>
En el alta, que aún no está asignado, se teclea uno provisional compuesto por el número de identificación fiscal, sin espacios ni guiones, precedido de la sigla del país.<br>
Ejemplos: empresa española con NIF B82669333, introduce ESB82669333.  Empresa francesa con identificación 482239174, introduce FR482239174.<BR>
En la notificación de aceptación de su alta en el portal, le informaremos del código definitivo asignado por Banco Popular.

      <%else if UCase(Application("NOMPORTAL")) = UCase("aer") or UCase(Application("NOMPORTAL")) = UCase("prored") or UCase(Application("NOMPORTAL")) = UCase("ibc") then%><strong>Código de identificación de su empresa. </strong>Es la primera de las tres claves necesarias para acceder a la zona privada de proveedores una vez solicitada el alta.
En el campo "Código de Compañía" deberá proceder según sea su caso: <br><br>
<strong>Empresas españolas:</strong> introduzca su número de identificación fiscal, sin espacios ni guiones, precedido de la sigla del país (ES). Ejemplo: Empresa española con NIF B82669333. Introduzca: ESB82669333 <br><br> 
<strong>Empresas no españolas:</strong> introduzca su número de identificación fiscal precedido de la sigla de su país (2 letras). Ejemplo: Empresa francesa con NIF 482239174. Introduzca: FR482239174 

<%else if UCase(Application("NOMPORTAL")) = UCase("oca") or UCase(Application("NOMPORTAL")) = UCase("ser") or UCase(Application("NOMPORTAL")) = UCase("ete") then%>Código de identificación de su empresa. Es la primera de las tres claves necesarias para acceder a la zona privada de proveedores una vez solicitada el alta. Este código deberá ser la combinación del código de su país y su NIF, como se indica en el ejemplo: ESA00000000.     
 <%else if UCase(Application("NOMPORTAL")) = UCase("rsi") then%>Código de identificación de su empresa. Este código le será facilitado por RSI.
 <%else if UCase(Application("NOMPORTAL")) = UCase("aje") then%>Para rellenar este campo es necesario realizar una combinación del código del país donde radica la compañía y el numero de identificación tributaria del país donde esta registrada la empresa.  <br><br>
Ejemplos: <br><br>

* Colombia = CO + N.I.T. = CO1548962418<br>
* Guatemala = GT + R.T.U. = GT124567<br>
* Mexico = MX + R.F.C = MXCUPA80025469<br>
* Perú = PE + R.U.C = PE10452410256<br>


<%else if UCase(Application("NOMPORTAL")) = UCase("ctr") then%>Introduzca su número de identificación fiscal (NIF/CIF).  Es la primera de las tres claves necesarias para acceder a la zona privada de proveedores, una vez solicitada el alta.
<%else if UCase(Application("NOMPORTAL")) = UCase("hre") or UCase(Application("NOMPORTAL")) = UCase("sam") then%>Para rellenar este campo utilice el CIF comunitario de su empresa.<br><br>
Por ejemplo: <br><br>España: ES+CIF=ESB12345678<br>
                     UK: GB+VAT=GBX12345678
                           			<%else%>
      Código de identificación de su empresa. Es la primera de las tres claves
      necesarias para acceder a la zona privada de proveedores una vez
      solicitada el alta.</font></td>
<%end if%>
<%end if%>
<%end if%>
<%end if%>
<%end if%>
<%end if%>
<%end if%>
<%end if%>
<%end if%>
<%end if%>
<%end if%>
<%end if%>
  </tr>
</table>
<p><span style="position: absolute; left: -1; top: 2"><img border="0" src="../../<%=application("NOMPORTAL")%>/images/logoayuda.gif"></span></p>

</body>
</html>