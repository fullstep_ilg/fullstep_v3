﻿<%@ Language=VBScript %>

<%

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%if UCase(Application("NOMPORTAL")) = UCase("ficosa") then%>
<title>D.U.N.S</title>
<%else%>
<%if UCase(Application("NOMPORTAL")) = UCase("afi") then%>
	  <title>FULLSTEP ID #</title>
<%else%>
<title>Tax Code</title>
<%end if%>
<%end if%>
</head>
<body>
<table border="0" width="490" cellspacing="0" cellpadding="0" height="9">
  <tr>
    <td width="89" height="9" valign="bottom">
      <p align="center"><font face="verdana" size="1"></font></td>

    <td width="401" height="9" valign="middle" bgcolor="#094176">
      <font face="verdana" color="white" size="1"><b><%if UCase(Application("NOMPORTAL")) = UCase("ficosa") or UCase(Application("NOMPORTAL")) = UCase("HUF") then%>
		&gt;&gt; D.U.N.S Number ®
		<%else%>
	  <%if UCase(Application("NOMPORTAL")) = UCase("afi") then%>
	  &gt;&gt; FULLSTEP ID #
		<%else%>
		&gt;&gt; Tax Code</b></font>
		<%end if%>
		<%end if%></td>
    </tr>
  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1"><br><br><br>
      <%if UCase(Application("NOMPORTAL")) = UCase("ficosa") or UCase(Application("NOMPORTAL")) = UCase(UCase("HUF")) then%>
      It is an unique nine-digit sequence recognized as the universal standard for identifying businesses worlwide.
      If you do not have it yet, you can request it from <a target="_blank" href="http://www.dnb.com">http://www.dnb.com</a> at no cost.
      <%else%>
	  <%if UCase(Application("NOMPORTAL")) = UCase("gestamp") then%>
      <strong>Spanish companies</strong>: please fill in your tax identification number, without spaces or dashes.
          Example: Spanish company with NIF B82669333. Fill in with: B82669333
<br>
<br>
	  <strong>Non spanish companies</strong>: please fill in your tax identification number,  without  spaces or dashes, adding two letters in the beginning corresponding to your country code. This code must be the same code that you filled in in the &quot;Company code&quot; section.
          Example: French company with NIF 482239174. Fill in with: FR482239174
		   <%else%>
	  <%if UCase(Application("NOMPORTAL")) = UCase("afi") then%>
	      This field is required for internal system purposes at Azteca. If you haven&#146;t been assigned one by the buyer already, please call 1-708-563-6619.Please enter your Company ID.
      <%else%>
	<%if UCase(Application("NOMPORTAL")) = UCase("fer") then%>
      Please fill in your tax identification number, without  spaces or dashes, adding two letters in the beginning corresponding to your country code.<br>
      Example: Spanish company with tax number B82669333.  Please fill in: ESB82669333<br>
	  Example: French company with tax number 482239174.   Please fill in: FR482239174
      <%else%>
      Introduce your tax code without spaces or hyphens between.
      <%end if%>
	  <%end if%>
  	  <%end if%>
   	  <%end if%>
      </font></td>
  </tr>
<%IF UCase(APPLICATION("NOMPORTAL")) = UCase("federlan") then%>
  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1"><br>
      <b>Example</b></font></td>
  </tr>
  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1">
      Company: EXAMPLE COMPANY</font></td>
  </tr>
  <tr>
    <td width="475" valign="bottom" colspan="2" align="center">
      <p align="left"><font face="verdana" size="1" COLOR="RED">
      Tax code = <b>DE820025292</b></font></td>
  </tr>
<%end if%>  
</table>
<p><span style="position: absolute; left: -1; top: 2"><img border="0" src="../../<%=application("NOMPORTAL")%>/images/logoayuda.gif"></span></p>

</body>
</html>