﻿<%@ Language=VBScript %>

<%

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>User code</title>
</head>
<body>
<table border="0" width="490" cellspacing="0" cellpadding="0" height="9">
  <tr>
    <td width="89" height="9" valign="bottom">
      <p align="center"><font face="verdana" size="1"></font></p></td>

    <td width="401" height="9" valign="center" bgcolor="#094176">
      <font face="verdana" color="white" size="1"><b>
	  <%if UCase(Application("NOMPORTAL")) = UCase("afi") then%>
	  &gt;&gt; User ID
	  <%else%>
	  &gt;&gt; Personal user code 
	  <%end if%>
	  </b></font></td>

    </tr>

  <tr>
    <td width="475" valign="bottom" colspan="2" align="middle">
      <p align="left"><font face="verdana" size="1"><br><br><br>
	  <%if UCase(Application("NOMPORTAL")) = UCase("afi") then%>
	It is your personal user identification (min. six characters). It is the second of the three codes needed to access the supplier private zone once your company is registered.
	<%else%>
      Personal user code (min. six characters). It is the second of the three codes needed to access the supplier private zone once your company is registered.<br>
      This code should not be the same as the company code that you included in &quot;Company general data&quot;.
	  <%end if%></font></p></td>
  </tr>
</table>
<p><span style="LEFT: -1px; POSITION: absolute; TOP: 2px"><img src="../../<%=application("NOMPORTAL")%>/images/logoayuda.gif" border="0"></span></p>

</body>
</html>