Namespace Fullstep.PMPortalNotificador
    Public Class TiposDeDatos
        Public Enum TipoGeneral
            SinTipo = 0
            TipoString = 1
            TipoNumerico = 2
            TipoFecha = 3
            TipoBoolean = 4
            TipoTextoCorto = 5
            TipoTextoMedio = 6
            TipoTextoLargo = 7
            TipoArchivo = 8
            TipoDesglose = 9
        End Enum



        Public Enum TipoCampoGS
            SinTipo = 0
            'Public Enum TipoCampoSC
            DescrBreve = 1
            DescrDetallada = 2
            Importe = 3
            Cantidad = 4
            FecNecesidad = 5
            IniSuministro = 6
            FinSuministro = 7
            ArchivoEspecific = 8
            PrecioUnitario = 9
            'End Enum

            EstadoNoConf = 42
            EstadoInternoNoConf = 43


            Proveedor = 100
            FormaPago = 101
            Moneda = 102
            Material = 103
            CodArticulo = 104
            Unidad = 105
            Desglose = 106
            Pais = 107
            Provincia = 108
            Dest = 109
            PRES1 = 110
            Pres2 = 111
            Pres3 = 112
            Pres4 = 113
        End Enum

        Public Structure LongitudesDeCodigos

            Dim giLongCodART As Integer
            Dim giLongCodCAL As Integer
            Dim giLongCodCOM As Integer
            Dim giLongCodDEP As Integer
            Dim giLongCodDEST As Integer
            Dim giLongCodEQP As Integer
            Dim giLongCodGMN1 As Integer
            Dim giLongCodGMN2 As Integer
            Dim giLongCodGMN3 As Integer
            Dim giLongCodGMN4 As Integer
            Dim giLongCodMON As Integer
            Dim giLongCodOFEEST As Integer
            Dim giLongCodPAG As Integer
            Dim giLongCodPAI As Integer
            Dim giLongCodPER As Integer
            Dim giLongCodPERF As Integer
            Dim giLongCodPRESCON1 As Integer
            Dim giLongCodPRESCON2 As Integer
            Dim giLongCodPRESCON3 As Integer
            Dim giLongCodPRESCON4 As Integer
            Dim giLongCodPRESCONCEP31 As Integer
            Dim giLongCodPRESCONCEP32 As Integer
            Dim giLongCodPRESCONCEP33 As Integer
            Dim giLongCodPRESCONCEP34 As Integer
            Dim giLongCodPRESCONCEP41 As Integer
            Dim giLongCodPRESCONCEP42 As Integer
            Dim giLongCodPRESCONCEP43 As Integer
            Dim giLongCodPRESCONCEP44 As Integer
            Dim giLongCodPRESPROY1 As Integer
            Dim giLongCodPRESPROY2 As Integer
            Dim giLongCodPRESPROY3 As Integer
            Dim giLongCodPRESPROY4 As Integer
            Dim giLongCodPROVE As Integer
            Dim giLongCodPROVI As Integer
            Dim giLongCodROL As Integer
            Dim giLongCodUNI As Integer
            Dim giLongCodUON1 As Integer
            Dim giLongCodUON2 As Integer
            Dim giLongCodUON3 As Integer
            Dim giLongCodUSU As Integer
            Dim giLongCodACT1 As Integer
            Dim giLongCodACT2 As Integer
            Dim giLongCodACT3 As Integer
            Dim giLongCodACT4 As Integer
            Dim giLongCodACT5 As Integer
            Dim giLongCia As Integer
            Dim giLongCodCAT1 As Integer
            Dim giLongCodCAT2 As Integer
            Dim giLongCodCAT3 As Integer
            Dim giLongCodCAT4 As Integer
            Dim giLongCodCAT5 As Integer
            Dim giLongCodGRUPOPROCE As Integer

        End Structure


        Public Enum IdsFicticios
            EstadoActual = 1000
            Comentario = 1020
            EstadoInterno = 1010
            Grupo = 900
        End Enum

    End Class

End Namespace
