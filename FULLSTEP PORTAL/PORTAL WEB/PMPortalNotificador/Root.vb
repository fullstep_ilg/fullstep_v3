Namespace Fullstep.PMPortalNotificador
    <Serializable()> _
    Public Class Root

        Friend mDBServer As Fullstep.PMPortalDatabaseServer.Root

        Friend msUserCode As String = ""
        Friend msSesionId As String = ""
        Friend msIPDir As String = ""
        Friend msPersistID As String = ""

        Friend mRemottingServer As Boolean = False
        Friend mIsAuthenticated As Boolean = False

        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Notificar() As Notificar
            Dim oNotif As Notificar
            Authenticate()
            oNotif = New Notificar(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
            Return oNotif
        End Function

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">Usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: PMPortalServer\Security.vb\PMNotif   PMPortalNotificador\Notificar.vb\New; Tiempo m�ximo: 0</remarks>
        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String _
                       , ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            mDBServer = dbserver
            mRemottingServer = remotting
            msUserCode = UserCode
            msSesionId = SesionId
            msIPDir = IPDir
            msPersistID = PersistID
            mIsAuthenticated = isAuthenticated
        End Sub

        Friend Function DBServer() As Fullstep.PMportalDatabaseServer.Root
            If mDBServer Is Nothing Then
                mDBServer = New Fullstep.PMportalDatabaseServer.Root
            End If
            Return mDBServer
        End Function

        Friend Sub Authenticate()
            If Not mIsAuthenticated Then Throw New System.Security.SecurityException("User not authenticated!")
        End Sub


    End Class

End Namespace
