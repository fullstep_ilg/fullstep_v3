Option Explicit On 
Option Strict On

Imports System.Runtime.InteropServices
Namespace Fullstep.PMPortalNotificador
    Friend Class Encrypter

        Private Const ClaveSQLtod As String = "aldj�w"

        ''' <summary>
        ''' Encripta/desencripta datos
        ''' </summary>
        ''' <param name="Data">Dato a encriptar/desencriptar</param>
        ''' <param name="Usu">Persona q encripta/desencripta</param>
        ''' <param name="Encrypting">True encripta/ False desencripta</param>
        ''' <param name="Fecha">Fecha para encriptar/desencriptar</param>
        ''' <returns>Datos Encriptados/desencriptados</returns>
        ''' <remarks>Llamada desde: Notificar.vb/EnviarMail ; Tempo maximo: 0sg</remarks>
        Friend Shared Function Encrypt(ByVal Data As String, Optional ByVal Usu As String = "", Optional ByVal Encrypting As Boolean = True, Optional ByVal Fecha As Date = #3/5/1974#) As String

            Dim Crypt As New FSNCrypt.FSNAES

            If Encrypting Then
                Encrypt = Crypt.EncryptStringToString(Fecha, Usu, Data)
            Else
                Encrypt = Crypt.DecryptStringFromString(Fecha, Usu, Data)
            End If

            Crypt = Nothing
        End Function
    End Class
End Namespace

