Namespace Fullstep.PMPortalNotificador
    Module modUtilidades
        Public Function DBNullToSomething(Optional ByVal value As Object = Nothing) As Object
            If IsDBNull(value) Then
                Return Nothing
            Else
                Return value
            End If

        End Function
        Public Function GenerateRandomPath() As String
            Dim s As String = ""
            Dim i As Integer
            Dim lowerbound As Integer = 65
            Dim upperbound As Integer = 90
            Randomize()
            For i = 1 To 10
                s = s & Chr(Int((upperbound - lowerbound + 1) * Rnd() + lowerbound))
            Next
            Return s

        End Function
        Public Function igDateToDate(ByVal s As String) As DateTime

            Try
                If s = Nothing Or s = "" Then
                    Return Nothing
                End If
                Dim arr As Array = s.Split("-")

                Return DateSerial(arr(0), arr(1), arr(2))


            Catch ex As Exception
                Return Nothing

            End Try

        End Function
        Public Function DateToigDate(ByVal dt As DateTime) As String
            Try
                If dt = Nothing Then
                    Return ""
                End If
                '2005-7-14-12-7-14-244
                'anyo-m-di-ho-m-se-mil

                Dim s As String = Year(dt).ToString() + "-" + Month(dt).ToString() + "-" + Microsoft.VisualBasic.Day(dt).ToString() + "-" + Hour(dt).ToString() + "-" + Minute(dt).ToString() + "-" + Second(dt).ToString()
                Return s

            Catch ex As Exception

                Return ""
            End Try
        End Function

    End Module
End Namespace