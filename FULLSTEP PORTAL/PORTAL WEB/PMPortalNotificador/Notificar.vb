Imports System.Configuration
Imports System.Web.UI

Namespace Fullstep.PMPortalNotificador
    Public Class Notificar
        Inherits Root
        Private moLongitudesDeCodigos As TiposDeDatos.LongitudesDeCodigos
        Private mlIdUsu As Long
        Private mlIdCia As Long

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">Usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

        Public Property IdUsuario() As Long
            Get
                IdUsuario = mlIdUsu
            End Get
            Set(ByVal Value As Long)
                mlIdUsu = Value
            End Set
        End Property

        Public Property IdProveCia() As Long
            Get
                IdProveCia = mlIdCia
            End Get
            Set(ByVal Value As Long)
                mlIdCia = Value
            End Set
        End Property

        ''' <summary>
        ''' Enviar un Mail
        ''' </summary>
        ''' <param name="sFrom">From</param>
        ''' <param name="Subject">Subject</param>
        ''' <param name="Para">Para</param>
        ''' <param name="Message">Message</param>
        ''' <param name="isHTML">cuerpo en html o un txt</param>
        ''' <param name="isPortal">mail de portal u otro origen</param>
        ''' <remarks>Llamada desde: Esta misma pantalla en cada funcion q notifica algo; Tiempo m�ximo: 0,2</remarks>
        Public Sub EnviarMail(ByVal lCiaComp As Long, ByVal sFrom As String, ByVal Subject As String, ByVal Para As String, ByVal Message As String, Optional ByVal isHTML As Boolean = False, Optional ByVal isPortal As Boolean = True, Optional ByVal sCC As String = "", Optional ByVal sCCO As String = "", Optional ByVal bEsPM As Boolean = False, _
                              Optional ByVal sCodProve As String = Nothing, Optional ByVal lIdInstancia As Long = Nothing, Optional ByVal sAdjuntos As String = Nothing, Optional ByVal sRuta As String = Nothing)
            Dim SmtpMail As System.Web.Mail.SmtpMail
            Dim MyMail As New System.Web.Mail.MailMessage
            Dim oFlds As System.Collections.IDictionary
            Dim Data As DataSet
            Const cdoAnonymous = 0       ' Do not authenticate
            Const cdoBasic = 1           ' Basic (clear-text) authentication
            Const cdoNTLM = 2            ' NTLM

            If mRemottingServer Then
                Data = DBServer.Notificador_DevolverAutenticacion(msSesionId, msIPDir, msPersistID)
            Else
                Data = DBServer.Notificador_DevolverAutenticacion()
            End If

            oFlds = MyMail.Fields
            Const ConfigNamespace As String = "http://schemas.microsoft.com/cdo/configuration/"

            oFlds.Add(ConfigNamespace + "smtpserver", Data.Tables(0).Rows(0).Item("P_SERVIDOR").ToString)

            If ConfigurationSettings.AppSettings("MAILSENDUSING") = Nothing Then
                oFlds.Add(ConfigNamespace + "sendusing", 2)
            Else
                oFlds.Add(ConfigNamespace + "sendusing", CInt(ConfigurationSettings.AppSettings("MAILSENDUSING")))
            End If

            Select Case Val(Data.Tables(0).Rows(0).Item("P_AUTENTICACION").ToString)
                Case 1
                    oFlds.Add(ConfigNamespace + "smtpauthenticate", cdoBasic)
                    oFlds.Add(ConfigNamespace + "sendusername", Data.Tables(0).Rows(0).Item("P_USU").ToString)
                    oFlds.Add(ConfigNamespace + "sendpassword", Encrypter.Encrypt(Data.Tables(0).Rows(0).Item("P_PWD").ToString, "PWD", False))
                Case 2
                    oFlds.Add(ConfigNamespace + "smtpauthenticate", cdoNTLM)
                Case Else
                    oFlds.Add(ConfigNamespace + "smtpauthenticate", cdoAnonymous)
            End Select

            MyMail.From = sFrom
            MyMail.Cc = sCC
            MyMail.Bcc = sCCO
            MyMail.Subject = Subject
            MyMail.To = Para
            MyMail.Body = Message

            Dim auxAdjuntos As String = sAdjuntos
            Dim sTemp As String
            Dim sPath As String
            Dim myAttachment As Web.Mail.MailAttachment
            If sRuta <> "" Then
                'Acaba en ;
                sTemp = ConfigurationSettings.AppSettings("temp") & "\" & sRuta & "\"

                While InStr(auxAdjuntos, ";", CompareMethod.Binary)
                    sPath = Left(auxAdjuntos, InStr(auxAdjuntos, ";", CompareMethod.Binary) - 1)
                    Try
                        myAttachment = New Web.Mail.MailAttachment(sTemp & sPath)
                        MyMail.Attachments.Add(myAttachment)
                    Catch ex As Exception
                        Exit Sub
                    End Try

                    auxAdjuntos = Right(auxAdjuntos, auxAdjuntos.Length - InStr(auxAdjuntos, ";", CompareMethod.Binary))
                End While
            End If

            MyMail.Priority = Web.Mail.MailPriority.Normal
            If isHTML = True Then
                MyMail.BodyFormat = Web.Mail.MailFormat.Html
            Else
                MyMail.BodyFormat = Web.Mail.MailFormat.Text
            End If

            Web.Mail.SmtpMail.SmtpServer = Data.Tables(0).Rows(0).Item("P_SERVIDOR").ToString
            Web.Mail.SmtpMail.Send(MyMail)

            GrabarEnviarmailGS(lCiaComp, sFrom, Subject, Para, Message, sCC, sCCO, bEsPM, isHTML, False, "", sCodProve, lIdInstancia, sAdjuntos, sRuta)

            Data = Nothing
            SmtpMail = Nothing
            MyMail = Nothing

        End Sub

        Private Function ReplaceV(ByVal s1 As String, ByVal s2 As String, ByVal s3 As Object) As String
            ReplaceV = Replace(s1, s2, IIf(IsDBNull(s3), "", s3))
        End Function

        ''' <summary>
        ''' Graba en bbdd el mail enviado, sea correctamente enviado o no 
        ''' </summary>
        ''' <param name="From">From</param>
        ''' <param name="Subject">Subject</param>
        ''' <param name="Para">Para</param>
        ''' <param name="Message">Message</param>
        ''' <param name="isHTML">si es html o txt</param>
        ''' <param name="bPortal">mail de portal u otro origen</param>
        ''' <param name="bError">correctamente enviado o no </param>
        ''' <param name="sTextoError">no correctamente enviado, aqui viene el posible error</param>
        ''' <remarks>Llamada desde: EnviarMail    NotificacionRealizarAccion        NotificacionDevolucionSolicitud; Tiempo m�ximo: 0,2</remarks>
        Private Sub GrabarEnviarmailGS(ByVal lIdCiaComp As Long, ByVal From As String, ByVal Subject As String, ByVal Para As String, ByVal Message As String, ByVal sCC As String, ByVal sCCO As String, ByVal bEsPM As Boolean, ByVal isHTML As Boolean, ByVal bKO As Boolean, ByVal sTextoError As String, ByVal sCodProve As String, ByVal lIdInstancia As Long, ByVal sAdjuntos As String, ByVal sRuta As String)
            If mRemottingServer Then
                DBServer.Notificador_GrabarEnviarMensajeGS(lIdCiaComp, From, Subject, Para, Message, sCC, sCCO, bEsPM, isHTML, bKO, sTextoError, sAdjuntos, sRuta, sCodProve, 0, lIdInstancia, 0, "", msSesionId, msIPDir, msPersistID)
            Else
                DBServer.Notificador_GrabarEnviarMensajeGS(lIdCiaComp, From, Subject, Para, Message, sCC, sCCO, bEsPM, isHTML, bKO, sTextoError, sAdjuntos, sRuta, sCodProve, 0, lIdInstancia, 0, "")
            End If
        End Sub
    End Class
End Namespace
