<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/> <xsl:decimal-format name="european" decimal-separator=',' grouping-separator='.' />
<xsl:template match="/DOCUMENTO">
<TABLE BORDER="1" WIDTH="100%" ROWS="4" COLS="2" STYLE="border-collapse: collapse" BGCOLOR="#DDDDFF">
<TR>
<TD>
<FONT FACE="Tahoma" SIZE="2"><B>
<xsl:value-of select="TIPO"/>
</B>
</FONT>
</TD>
<TD>
<FONT FACE="Tahoma" SIZE="2">
<xsl:value-of select="ID"/>
</FONT>
</TD>
</TR>
<TR>
<TD>
<FONT FACE="Tahoma" SIZE="2"><B>
Requester</B>
</FONT>
</TD>
<TD>
<FONT FACE="Tahoma" SIZE="2">
<xsl:value-of select="PETICIONARIO"/>
</FONT>
</TD>
</TR>
<TR>
<TD>
<FONT FACE="Tahoma" SIZE="2"><B>
Registration date</B>
</FONT>
</TD>
<TD>
<FONT FACE="Tahoma" SIZE="2">
<xsl:value-of select="FECHA"/>
</FONT>
</TD>
</TR>
</TABLE>

<xsl:apply-templates select="//GRUPO"/>

</xsl:template>

<xsl:template match="GRUPO">
<P>
<DIV STYLE="background-color: #FFDDDD">
<FONT FACE="Tahoma" SIZE="2"><B>
<xsl:value-of select="NOMBRE"/>
</B></FONT></DIV></P>
<xsl:apply-templates select=".//CAMPO"/>
</xsl:template>

<xsl:template match="CAMPO">
<FONT FACE="Tahoma" SIZE="2">
<B>
<xsl:value-of select="NOMBRE"/>: </B>
<xsl:value-of select="./VALOR/VALOR_DATO"/><xsl:text> </xsl:text>
<xsl:value-of select="./VALOR/VALOR_DEN"/><BR></BR>
</FONT>
<xsl:apply-templates select="./DESGLOSE"/>
</xsl:template>

<xsl:template match="DESGLOSE">
<P>
<xsl:apply-templates select=".//FILA_DESGLOSE"/>
</P>
</xsl:template>

<xsl:template match="FILA_DESGLOSE">
<xsl:apply-templates select=".//COLUMNA_DESGLOSE"/> 
<BR></BR>
</xsl:template>


<xsl:template match="COLUMNA_DESGLOSE">
<FONT FACE="Tahoma" SIZE="1">
<B><xsl:value-of select="NOMBRE"/></B>:<xsl:text> </xsl:text>
<xsl:value-of select="./VALOR/VALOR_DATO"/><xsl:text> </xsl:text>
<xsl:value-of select="./VALOR/VALOR_DEN"/><BR></BR>
</FONT>
</xsl:template>

</xsl:stylesheet>