<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/> <xsl:decimal-format name="european" decimal-separator=',' grouping-separator='.' />
<xsl:template match="/DOCUMENTO">
<TABLE BORDER="1" WIDTH="100%" ROWS="4" COLS="2" STYLE="border-collapse: collapse" BGCOLOR="#DDDDFF">
<TR>
<TD>
<FONT FACE="Tahoma" SIZE="2"><B>
<xsl:value-of select="TIPO"/>
</B>
</FONT>
</TD>
<TD>
<FONT FACE="Tahoma" SIZE="2">
<xsl:value-of select="ID"/>
</FONT>
</TD>
</TR>
<TR>
<TD>
<FONT FACE="Tahoma" SIZE="2"><B>
Peticionario</B>
</FONT>
</TD>
<TD>
<FONT FACE="Tahoma" SIZE="2">
<xsl:value-of select="PETICIONARIO"/>
</FONT>
</TD>
</TR>
  <TR>
    <TD>
      <FONT FACE="Tahoma" SIZE="2">
        <B>
          Unidad de Negocio
        </B>
      </FONT>
    </TD>
    <TD>
      <FONT FACE="Tahoma" SIZE="2">
        <xsl:value-of select="UNQA"/>
      </FONT>
    </TD>
  </TR>
<TR>
<TD>
<FONT FACE="Tahoma" SIZE="2"><B>
Fecha de alta</B>
</FONT>
</TD>
<TD>
<FONT FACE="Tahoma" SIZE="2">
<xsl:value-of select="FECHA"/>
</FONT>
</TD>
</TR>
<TR>
<TD>
<FONT FACE="Tahoma" SIZE="2"><B>
Resolver antes de</B>
</FONT>
</TD>
<TD>
<FONT FACE="Tahoma" SIZE="2">
<xsl:value-of select="FEC_LIM"/>
</FONT>
</TD>
</TR>
  <TR>
    <TD>
      <FONT FACE="Tahoma" SIZE="2">
        <B>
          Comentario Alta
        </B>
      </FONT>
    </TD>
    <TD>
      <FONT FACE="Tahoma" SIZE="2">
        <xsl:value-of select="COMMENT_ALTA" disable-output-escaping="yes"/>
      </FONT>
    </TD>
  </TR>
  <TR>
    <TD>
      <FONT FACE="Tahoma" SIZE="2">
        <B>
          Comentario Cierre
        </B>
      </FONT>
    </TD>
    <TD>
      <FONT FACE="Tahoma" SIZE="2">
        <xsl:value-of select="COMMENT_CIERRE" disable-output-escaping="yes"/>
      </FONT>
    </TD>
  </TR>
</TABLE>

<P>
<TABLE BORDER="0" WIDTH="100%" COLS="2" STYLE="border-collapse: collapse">
<tr BGCOLOR="#FFFFCC"><td colspan="2"><FONT FACE="Tahoma" SIZE="2" text-decoration="underline"><B>Acciones solicitadas</B></FONT></td></tr>
<xsl:apply-templates select="//ACCION"/>
</TABLE>
</P>

<xsl:apply-templates select="//GRUPO"/>

</xsl:template>


<xsl:template match="ACCION">
 <TR>
  <TD><FONT FACE="Tahoma" SIZE="2"><xsl:value-of select="NOM_ACCION"/></FONT></TD>
  <TD><FONT FACE="Tahoma" SIZE="2"><xsl:value-of select="FECLIM_ACCION"/></FONT></TD>
 </TR>
</xsl:template>

<xsl:template match="GRUPO">
<P>
<DIV STYLE="background-color: #FFDDDD">
<FONT FACE="Tahoma" SIZE="2"><B>
<xsl:value-of select="NOMBRE"/>
</B></FONT></DIV></P>
<xsl:apply-templates select=".//CAMPO"/>
</xsl:template>

<xsl:template match="CAMPO">
<FONT FACE="Tahoma" SIZE="2">
<B>
<xsl:value-of select="NOMBRE"/>: </B>
<xsl:value-of select="./VALOR/VALOR_DATO"/><xsl:text> </xsl:text>
<xsl:value-of select="./VALOR/VALOR_DEN"/><BR></BR>
</FONT>
<xsl:apply-templates select="./DESGLOSE"/>
</xsl:template>

<xsl:template match="DESGLOSE">
<P>
<xsl:apply-templates select=".//FILA_DESGLOSE"/>
</P>
</xsl:template>

<xsl:template match="FILA_DESGLOSE">
<xsl:apply-templates select=".//COLUMNA_DESGLOSE"/> 
<BR></BR>
</xsl:template>


<xsl:template match="COLUMNA_DESGLOSE">
<FONT FACE="Tahoma" SIZE="1">
<B><xsl:value-of select="NOMBRE"/></B>:<xsl:text> </xsl:text>
<xsl:value-of select="./VALOR/VALOR_DATO"/><xsl:text> </xsl:text>
<xsl:value-of select="./VALOR/VALOR_DEN"/><BR></BR>
</FONT>
</xsl:template>

</xsl:stylesheet>