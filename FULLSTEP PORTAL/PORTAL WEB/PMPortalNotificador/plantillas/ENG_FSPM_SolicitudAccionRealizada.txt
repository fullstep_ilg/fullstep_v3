_________________________________________________________________________________________
		FULLSTEP PM
		AVISO DE ACCI�N SOBRE SOLICITUD
	
 Esta notificaci�n es para indicarle que se ha realizado una acci�n sobre
 la solicitud de compra @ID "@DESCR_BREVE" con fecha de @FECHA_ALTA .

 Comentarios de la persona que realiz� la acci�n:

 	@COMENTARIO	


 Un saludo

 
PD: A continuaci�n le presentamos la informaci�n relativa a la solicitud:
_________________________________________________________________________________________

Tipo de solicitud:		@TIPOCOD - @TIPODEN	

Datos de la solicitud:
 
 Identificador:		@ID
 Descripci�n breve:		@DESCR_BREVE 
 Fecha de alta: 		@FECHA_ALTA
 Fecha de necesidad:	@FECHA_NECESIDAD
 Importe aproximado:	@IMPORTE (@MON_COD - @MON_DEN)
 Enlace:	@LINKAPR
_______________________________________________________________________________________

Datos de la acci�n: 
 Descripci�n:		@ACC_DEN
_______________________________________________________________________________________

Datos de la persona que realiz� la acci�n:
 
 Codigo Compa��a: 		@COD_COM
 Denominacion: 			@DEN_COM
 NIF:				@NIF
 
 Usuario:		@NOM_USU @APE_USU
 Tel�fono:		@TFNO1_USU
 Email:			@EMAIL_USU
 Fax:			@FAX_USU
_______________________________________________________________________________________


Datos del peticionario:

 C�digo:			@PET_COD
 Nombre:			@PET_NOM @PET_APE
 Tel�fono:		@PET_TFNO
 Email:			@PET_EMAIL
 Fax:			@PET_FAX
 Unidad organizativa:	@PET_UON
 Departamento:		@PET_DEP_COD - @PET_DEP_DEN
 
