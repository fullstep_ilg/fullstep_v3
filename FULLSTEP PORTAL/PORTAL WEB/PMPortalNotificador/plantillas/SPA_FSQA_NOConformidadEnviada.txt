_______________________________________________________________________________________
		FULLSTEP PORTAL
		AVISO DE ENVIO DE NO CONFORMIDAD
	
 Esta notificación es para indicarle que el proveedor @PROVE_DEN ha enviado una respuesta a
 la no conformidad @DEN.

 
 Un saludo

 
PD: A continuación le presentamos la información relativa a la no conformidad:
_______________________________________________________________________________________


Datos de la no conformidad 
_____________________
 
Identificador:  @ID 
Nombre:  @DEN 
Fecha de envio:  @FECHA_RESPUESTA 
Versión:  @VERSION 
 
 
Datos del proveedor 
___________________
   
Código:  @PROVE_COD 
Nombre:  @PROVE_DEN 
NIF:  @PROVE_NIF 
Usuario:  @USU_NOM 
Email:  @USU_EMAIL 
 
 
 
