_________________________________________________________________________________________
		FULLSTEP FSPM
		AVISO DE DEVOLUCIÓN DE SOLICITUD
	
 Esta notificación es para indicarle que se le ha devuelto para su aprobación 
 la solicitud de compra @ID "@DESCR_BREVE" con fecha de @FECHA_ALTA .

 Comentarios del cumplimentador:

 	@COMENTARIO	


 Un saludo

 
PD: A continuación le presentamos la información relativa a su solicitud:
_________________________________________________________________________________________

Tipo de solicitud:		@TIPOCOD - @TIPODEN	

Datos de la solicitud:
 
 Identificador:		@ID
 Descripción breve:		@DESCR_BREVE 
 Fecha de alta: 		@FECHA_ALTA
 Fecha de necesidad:	@FECHA_NECESIDAD
 Importe aproximado:	@IMPORTE (@MON_COD - @MON_DEN)
 Enlace:	@LINKAPR
 
_______________________________________________________________________________________

Datos del cumplimentador :
 

 Codigo Compañía: 		@COD_COM
 Denominacion: 			@DEN_COM
 NIF:				@NIF
 
 Usuario:		@NOM_USU @APE_USU
 Teléfono:		@TFNO1_USU
 Email:			@EMAIL_USU
 Fax:			@FAX_USU

_______________________________________________________________________________________


Datos del peticionario:

 Código:			@PET_COD
 Nombre:			@PET_NOM @PET_APE
 Teléfono:		@PET_TFNO
 Email:			@PET_EMAIL
 Fax:			@PET_FAX
 Unidad organizativa:	@PET_UON
 Departamento:		@PET_DEP_COD - @PET_DEP_DEN
 
 
