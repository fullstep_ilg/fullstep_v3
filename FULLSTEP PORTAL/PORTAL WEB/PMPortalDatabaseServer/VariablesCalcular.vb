Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Exception
Imports System.IO
Imports System.Object


Namespace Fullstep.PMPortalDatabaseServer

    Module VariablesCalcular

        Private mDBConnection As String
        Private sUsu As String
        Private dFecha As DateTime
        Private lCompradora As Integer
        Private cn As New SqlConnection

        Private Function Variables_Get(ByVal lSolicitud As Integer, ByVal iNivel As Short) As DataSet

            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cm.Connection = cn
                cm.CommandText = "FSQA_GET_VARIABLES"
                cm.Parameters.AddWithValue("@CIA", lCompradora)
                cm.Parameters.AddWithValue("@SOLICITUD", lSolicitud)
                cm.Parameters.AddWithValue("@NIVEL", iNivel)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

            Catch

            End Try

            Return dr

        End Function

        Private Function ObtenerCalificacion(ByVal sProve As String, ByVal lVarCal As Integer, ByVal iNivel As Short, ByVal Puntos As Double) As Object
            Dim sRes() As Object
            Dim oDS As DataSet
            Dim lIDcalif As Integer
            Dim sCalif As String = String.Empty
            Dim iEq As New USPExpress.USPExpression

            If IsNothing(Puntos) Then
                Puntos = 0
            End If

            Dim cm As New SqlCommand
            Dim dr As New DataSet

            Try
                cm.Connection = cn
                cm.CommandText = "FSQA_GET_CALIFICATION"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCompradora)
                cm.Parameters.AddWithValue("@VARCAL", lVarCal)
                cm.Parameters.AddWithValue("@NIVEL", iNivel)
                cm.Parameters.AddWithValue("@VALUE", Puntos)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch

            End Try

            oDS = dr

            If oDS.Tables(0).Rows.Count > 0 Then
                lIDcalif = DBNullToSomething(oDS.Tables(0).Rows(0).Item("IDCAL"))
                sCalif = DBNullToSomething(oDS.Tables(0).Rows(0).Item("CAL"))
            End If

            oDS = Nothing

            ReDim sRes(1)
            sRes(0) = lIDcalif
            sRes(1) = sCalif

            ObtenerCalificacion = sRes

        End Function
    End Module
End Namespace