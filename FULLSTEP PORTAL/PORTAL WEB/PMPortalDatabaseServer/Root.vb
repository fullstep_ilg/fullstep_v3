Imports System.Configuration
Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml
Imports Fullstep.FSNLibrary

Namespace Fullstep.PMPortalDatabaseServer
    Public Class Root
        Inherits MarshalByRefObject
#Region "Propiedades Conexi�n a BD/Authenticate"
        Private mDBLogin As String = ""
        ReadOnly Property DBLogin() As String
            Get
                Return mDBLogin
            End Get
        End Property
        Private mDBPassword As String = ""
        ReadOnly Property DBPassword() As String
            Get
                Return mDBPassword
            End Get
        End Property
        Private mDBName As String = ""
        ReadOnly Property DBName() As String
            Get
                Return mDBName
            End Get
        End Property
        Private mDBServer As String = ""
        ReadOnly Property DBServer() As String
            Get
                Return mDBServer
            End Get
        End Property
        'La he tenido que a�adir porque en CFacturae.vb se necesita para acceder a BD desde la CAPA DE negocio �?�?�?�?�?�?
        Private mDBConnection As String = ""
        ReadOnly Property DBConnection() As String
            Get
                Return mDBConnection
            End Get
        End Property
        Private mIsAuthenticated As Boolean = False
        Private Sub Authenticate()
            If Not mIsAuthenticated Then Throw New System.Security.SecurityException("User not authenticated!")
        End Sub
        Private mImpersonateLogin As String = ""
        ReadOnly Property ImpersonateLogin() As String
            Get
                Return mImpersonateLogin
            End Get
        End Property
        Private mImpersonatePassword As String = ""
        ReadOnly Property ImpersonatePassword() As String
            Get
                Return mImpersonatePassword
            End Get
        End Property
        Private mImpersonateDominio As String = ""
        ReadOnly Property ImpersonateDominio() As String
            Get
                Return mImpersonateDominio
            End Get
        End Property
#End Region
        Private mUserCod As String = ""
        Private mUserPassword As String = ""
        Private mUserCiaCod As String = ""
        Private mUserCiaId As Long = 0
        Private mUserCiaComp As Long = 0
        Private mUserCn As Boolean
#Region " Data Access Methods "
#Region " Accion data access methods"
        ''' <summary>
        ''' cargar una accion
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="lId">Id de la accion</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>dataset con los datos</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Accion.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Function Accion_Load(ByVal lCiaComp As Long, ByVal lId As Integer, ByVal sIdi As String,
                                    Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_LOAD_ACCION"
                cm.Parameters.AddWithValue("@ACCION", lId)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Activo data access methods"
        ''' <summary>
        ''' Devuelve el detalle del activo especificado
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="sIdioma">C�digo del idioma del usuario</param>
        ''' <param name="sCodActivo">C�digo del activo que se quiere buscar</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un DataSet con el activo en caso de encontrar el registro</returns>
        ''' <remarks>El activo debe tener traducci�n en ACTIVO_DEN</remarks>
        Public Function Activo_Detalle(ByVal lCiaComp As Long, ByVal sIdioma As String, ByVal sCodActivo As String,
                                       Optional ByVal SesionId As String = Nothing, Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSSM_DETALLE_ACTIVO"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@IDIOMA", sIdioma)
                cm.Parameters.AddWithValue("@CODACTIVO", sCodActivo)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region " Adjunto data access methods "
        ''' <summary>
        ''' Graba el fichero adjunto de Contrato que se adjunta 
        ''' </summary>
        ''' <param name="sPer">Cod de persona</param>
        ''' <param name="sProve">Cod de proveedor</param>
        ''' <param name="sNom">Nombre del fichero</param>
        ''' <param name="sPath">Path donde esta el fichero adjunto</param>
        ''' <returns>retorna el id del fichero grabado</returns>
        ''' <remarks></remarks>
        Public Function Adjunto_Save_Adjun_Contrato_Wizard(ByVal lCiaComp As Long, ByVal sPer As String, ByVal sProve As String, ByVal sNom As String,
                                                           ByVal sPath As String, ByVal DBName As String, ByVal DBServer As String,
                                                           Optional ByVal idContrato As Long = Nothing, Optional ByVal sComent As String = "") As Long()
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim lIdAdjuntoContrato As Long
            Dim arrReturn(1) As Long
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_SAVE_ADJUN_CONTRATO_WIZARD"
                cm.Parameters.AddWithValue("@ID_ADJUN", SqlDbType.Int).Direction = ParameterDirection.InputOutput
                cm.Parameters("@ID_ADJUN").Value = 0
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@PROVE", sProve)
                cm.Parameters.AddWithValue("@NOM", sNom)
                cm.Parameters.AddWithValue("@COMENT", sComent)
                If Not idContrato = Nothing Then cm.Parameters.AddWithValue("@ID_CONTRATO", idContrato)
                cm.CommandType = CommandType.StoredProcedure
                cm.ExecuteNonQuery()

                lIdAdjuntoContrato = cm.Parameters("@ID_ADJUN").Value

                Dim sCarpeta As String = sPath
                Dim sNombreFichero As String = sCarpeta & "\" & sNom

                arrReturn(0) = lIdAdjuntoContrato
                If Not (sCarpeta = "" And sNom = "") Then
                    Dim input As System.IO.FileStream = New System.IO.FileStream(sNombreFichero, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                    arrReturn(1) = input.Length
                    Dim buffer(input.Length - 1) As Byte
                    input.Read(buffer, 0, buffer.Length)
                    input.Close()

                    GrabarNuevoAdjunto(lCiaComp, lIdAdjuntoContrato, buffer)
                End If
                Return arrReturn
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Realiza el guardado del adjunto a traves de FILESTREAM
        ''' </summary>
        ''' <param name="lIdAdjunto">ID adjunto</param>
        ''' <param name="Buffer">Buffer que ha cogido el adjunto</param>
        ''' <remarks>Llamada desde:Create; Tiempo m�ximo:0,6</remarks>
        Public Sub GrabarNuevoAdjunto(ByVal lCiaComp As Long, ByVal lIdAdjunto As Long, ByVal Buffer As Byte())
            Dim cn As New SqlConnection
            Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext
            cn = NuevaConexion()

            mi_impersionationContext = RealizarSuplantacion()

            cn.Open()
            Dim tx As SqlTransaction = cn.BeginTransaction()

            Dim oSqlFileStream As SqlTypes.SqlFileStream = GetSqlFileStreamForWriting(lCiaComp, lIdAdjunto, cn, tx)
            If Not oSqlFileStream Is Nothing Then
                oSqlFileStream.Write(Buffer, 0, Buffer.Length)
                oSqlFileStream.Close()
            End If

            tx.Commit()
            cn.Close()
            DeshacerSuplantacion(mi_impersionationContext)
        End Sub
        ''' <summary>
        ''' Para poder trabajar con FILESTREAM necesitamos suplantacion de identidad, es decir, necesitamos un usuario SQL que pueda
        ''' trabajar con este tipo de ficheros. Para ellos necesitamos que el usuario este creado con esos permisos en SQL
        ''' </summary>
        ''' <returns>Contexto de la suplantacion</returns>
        ''' <remarks>Llamada desde:GrabarNuevoAdjunto; Tiempo m�ximo:0seg.</remarks>
        Private Function GetSqlFileStreamForWriting(ByVal lCiaComp As Long, ByVal lIdAdjunto As Long, ByVal cn As SqlConnection, ByVal tx As SqlTransaction) As SqlTypes.SqlFileStream
            Dim sPath As String = Nothing
            Dim arTransactionContext() As Byte = Nothing

            GetPathNameAndTxContext(lCiaComp, lIdAdjunto, cn, tx, sPath, arTransactionContext)

            '4 - open the filestream to the blob
            If Not sPath Is Nothing And Not arTransactionContext Is Nothing Then
                GetSqlFileStreamForWriting = New SqlTypes.SqlFileStream(sPath, arTransactionContext, FileAccess.ReadWrite)
            Else
                Return Nothing
            End If
        End Function
        ''' <summary>
        ''' Obtiene la referencia del adjunto en BBDD para poder trabajar (Escribir) con el 
        ''' </summary>
        ''' <param name="lIdAdjunto">Explicaci�n par�metro 1</param>
        ''' <param name="cn">Conexion</param>        
        ''' <param name="tx">Transaccion lectura adjunto</param>
        ''' <param name="Path">Path del adjunto</param>
        ''' <param name="TxContext">TRANSACTIONCONTEXT del adjunto</param>
        ''' <remarks>Llamada desde:GetSqlFileStreamForWriting; Tiempo m�ximo:0,35seg.</remarks>
        Private Sub GetPathNameAndTxContext(ByVal lCiaComp As Long, ByVal lIdAdjunto As Long, ByVal cn As SqlConnection, ByVal tx As SqlTransaction, ByRef Path As String, ByRef TxContext As Byte())
            Dim cm As New SqlCommand

            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = FSGS(lCiaComp) & "FSPM_LEER_ADJUNTO"
            cm.Parameters.AddWithValue("@ID", lIdAdjunto)
            cm.Connection = cn
            cm.Transaction = tx

            Dim da As New SqlDataAdapter
            da.SelectCommand = cm
            Dim ds As New System.Data.DataSet
            da.Fill(ds)

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim oRow As DataRow = ds.Tables(0).Rows(0)

                If Not oRow.IsNull("PATHNAME") Then Path = oRow("PATHNAME")
                If Not oRow.IsNull("TRANSACTIONCONTEXT") Then TxContext = CType(oRow("TRANSACTIONCONTEXT"), Byte()) 'reader.GetSqlBytes(1).Buffer;            

                If Path Is Nothing Then
                    'Si se inserta un registro con la columna varbinary(max) a NULL no se crea archivo y FilePath devuelve nulo, con lo que no se puede
                    'crear el sqlFileStream.
                    'Insertando '' en la columna varbinary(max) se crea un registro de longitud cero y ya se puede obtener un handle para el archivo
                    Dim oCom As New SqlCommand
                    oCom.CommandType = CommandType.Text
                    oCom.CommandText = "UPDATE " & FSGS(lCiaComp) & "PM_COPIA_ADJUN SET DATA=cast('' as varbinary(max)) WHERE ID = " & lIdAdjunto
                    oCom.Connection = cn
                    oCom.Transaction = tx
                    oCom.ExecuteNonQuery()

                    Dim obj As Object = cm.ExecuteScalar()
                    If Not obj.Equals(DBNull.Value) Then Path = DirectCast(obj, String)
                End If
            End If
        End Sub
        ''' <summary>
        ''' Para poder trabajar con FILESTREAM necesitamos suplantacion de identidad, es decir, necesitamos un usuario SQL que pueda
        ''' trabajar con este tipo de ficheros. Para ellos necesitamos que el usuario este creado con esos permisos en SQL
        ''' </summary>
        ''' <returns>Contexto de la suplantacion</returns>
        ''' <remarks>Llamada desde:GrabarNuevoAdjunto; Tiempo m�ximo:0seg.</remarks>
        Private Function RealizarSuplantacion() As Security.Principal.WindowsImpersonationContext
            Dim impersonationContext As Security.Principal.WindowsImpersonationContext
            If mImpersonateLogin = "" Or mImpersonatePassword = "" Then
                Err.Raise(10000, , "Corrupted LICENSE file !")
            End If
            impersonationContext = modUtilidades.RealizarSuplantacion(ImpersonateLogin, ImpersonatePassword, ImpersonateDominio)
            Return impersonationContext
        End Function
        ''' <summary>
        ''' Deshace la suplantacion
        ''' </summary>
        ''' <remarks>Llamada desde:GrabarNuevoAdjunto; Tiempo m�ximo:0seg.</remarks>
        Private Sub DeshacerSuplantacion(ByVal impersonationContext As Security.Principal.WindowsImpersonationContext)
            impersonationContext.Undo()
        End Sub
        ''' <summary>
        ''' Abre una nueva conexion con seguridad integrada para que podamos trabajar con SQLFILESTREAM
        ''' </summary>
        ''' <returns>Nueva conexion</returns>
        ''' <remarks>Llamada desde:GrabarNuevoAdjunto; Tiempo m�ximo:0seg.</remarks>
        Private Function NuevaConexion() As SqlConnection
            Dim sDBConnection As String = "Integrated Security=true;server=" & DBServer & ";initial catalog=" & DBName
            Dim cn As SqlConnection = New SqlConnection(sDBConnection)
            Return cn
        End Function
        ''' <summary>
        ''' guarda el comentario del adjunto
        ''' </summary>
        ''' <param name="idAdjunto">id del adjunto a grabar</param>
        ''' <param name="tipo">tipo del adjunto</param>
        ''' <param name="Comentario">comentario a grabar</param>
        ''' <param name="Instancia">Instancia del adjunto</param>
        ''' <remarks></remarks>
        Public Sub GuardarComentario(ByVal lCiaComp As Long, ByVal idAdjunto As Integer, ByVal Tipo As Integer, ByVal Comentario As String, ByVal Instancia As Integer)
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_GUARDARCOMENTARIOADJUNTO"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@ID", idAdjunto)
                cm.Parameters.AddWithValue("@TIPO", Tipo)
                cm.Parameters.AddWithValue("@INSTANCIA", Instancia)
                cm.Parameters.AddWithValue("@COMENT", Comentario)
                cm.ExecuteNonQuery()
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Sub
        Public Function Adjunto_LeerContratoAdjunto(ByVal lCiaComp As Long, ByVal lIdContrato As Long, ByVal lIdArchivoContrato As Long) As Byte()
            Dim sBBDD As String = mDBName
            Dim sServidor As String = mDBServer
            Dim sCadena As String = "Integrated Security=true;server=" & sServidor & ";initial catalog=" & sBBDD
            Dim sqlConnection As New SqlConnection(sCadena)
            Dim sSQL As String
            Dim sqlCommand As New SqlCommand()
            sqlCommand.Connection = sqlConnection
            Try
                sqlConnection.Open()
                'Retrieve the file path of the SQL FILESTREAM BLOB in the first row. 
                If lIdArchivoContrato <> 0 Then
                    sSQL = "SELECT DATA.PathName() FROM PM_COPIA_ADJUN PCA WITH (NOLOCK) WHERE PCA.ID=" & lIdArchivoContrato
                Else
                    sSQL = "SELECT DATA.PathName() FROM PM_COPIA_ADJUN PCA WITH (NOLOCK) WHERE PCA.ID_CONTRATO=" & lIdContrato
                End If
                sqlCommand.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                sqlCommand.CommandType = CommandType.Text
                sqlCommand.Parameters.Add("@stmt", SqlDbType.NText, sSQL.Length)
                sqlCommand.Parameters("@stmt").Value = sSQL

                Dim filePath As String = Nothing
                Dim pathObj As Object = sqlCommand.ExecuteScalar()
                If Not pathObj.Equals(DBNull.Value) Then
                    filePath = DirectCast(pathObj, String)
                Else
                    Throw New System.Exception("DATA.PathName() PM_COPIA_ADJUN failed to read the path name for the PM_COPIA_ADJUN column.")
                End If

                ' Obtain a transaction context. All FILESTREAM BLOB operations occur within a transaction context to maintain data consistency. 
                Dim transaction As SqlTransaction = sqlConnection.BeginTransaction("mainTranaction")
                sqlCommand.Parameters.Clear()
                sqlCommand.Transaction = transaction
                sSQL = "SELECT GET_FILESTREAM_TRANSACTION_CONTEXT()"
                sqlCommand.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                sqlCommand.CommandType = CommandType.Text
                sqlCommand.Parameters.Add("@stmt", SqlDbType.NText, sSQL.Length)
                sqlCommand.Parameters("@stmt").Value = sSQL

                Dim txContext As Byte() = Nothing
                Dim obj As Object = sqlCommand.ExecuteScalar()

                If Not obj.Equals(DBNull.Value) Then
                    txContext = DirectCast(obj, Byte())
                Else
                    Throw New System.Exception("GET_FILESTREAM_TRANSACTION_CONTEXT() failed")
                End If

                Dim sqlFileStream As New SqlTypes.SqlFileStream(filePath, txContext, FileAccess.Read)
                Dim buffer As Byte() = New Byte(sqlFileStream.Length - 1) {}

                sqlFileStream.Read(buffer, 0, buffer.Length)

                ' Close the FILESTREAM handle. 
                sqlFileStream.Close()
                sqlCommand.Transaction.Commit()
                Adjunto_LeerContratoAdjunto = buffer
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' <summary>
        ''' Cargar un adjunto
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iTipo">Tipo de adjunto</param>
        ''' <param name="AdjunId">Id del adjunto</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Adjunto.vb\ ; Tiempo m�ximo: 0,2</remarks>
        Public Function Adjunto_LoadFromRequest(ByVal lCiaComp As Long, ByVal iTipo As Integer, ByVal AdjunId As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Object
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSWS_GETADJUN"
                cm.Parameters.AddWithValue("@TIPO", iTipo)
                cm.Parameters.AddWithValue("@ID", AdjunId)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Carga el fichero adjunto solicitado
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>   
        ''' <param name="iTipo">Tipo de fichero (1: es de un campo normal, 3: es de un campo dentro de un desglose </param>   
        ''' <param name="AdjunId">Id del adjunto</param>      
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>nada</returns>
        ''' <remarks>Llamada desde: Adjunto.vb\LoadInstFromRequest; Tiempo m�ximo:0,1</remarks>
        Public Function Adjunto_LoadInstFromRequest(ByVal lCiaComp As Long, ByVal iTipo As Integer, ByVal AdjunId As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Object
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSWS_GETINSTADJUN"
                cm.Parameters.AddWithValue("@TIPO", iTipo)
                cm.Parameters.AddWithValue("@ID", AdjunId)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Determina si el archivo es de una extensi�n bloqueada o no
        ''' </summary>
        ''' <param name="Extension"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Adjunto_EstaenListaNegra(ByVal Extension As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Boolean
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "SELECT BLOQUEADA FROM UPLOADEXT WITH(NOLOCK) WHERE EXT=" & StrToSQLNULL(Extension)
                cm.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                If dr.Tables(0).Rows.Count = 0 Then
                    Adjunto_EstaenListaNegra = True
                ElseIf dr.Tables(0).Rows(0)("BLOQUEADA") = 0 Then
                    Adjunto_EstaenListaNegra = False
                Else
                    Adjunto_EstaenListaNegra = True
                End If
            Catch ex As Exception
                Throw ex
            Finally
                cn.Close()
            End Try
        End Function

        Public Function Adjunto_CtrlEsTuyo(ByVal lCiaComp As Long, ByVal iTipo As Integer, ByVal AdjunId As Long, ByVal Instancia As Long, ByVal Proveedor As String, ByVal Campo As Long, ByVal Nombre As String, ByVal Fecha As DateTime _
        , ByVal bDscgTodos As Boolean, Optional ByVal UsaIdContrato As Boolean = False, Optional ByVal EsArchivoContrato As Boolean = False, Optional ByVal ArchContratoBd As Boolean = False, Optional ByVal EsAltafactura As Boolean = False _
        , Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Boolean
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As SqlDataReader
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSWS_CTRLADJUN"
                cm.Parameters.AddWithValue("@TIPO", iTipo)
                cm.Parameters.AddWithValue("@ID", AdjunId)
                cm.Parameters.AddWithValue("@INSTANCIA", Instancia)
                cm.Parameters.AddWithValue("@PROVEEDOR", Proveedor)
                cm.Parameters.AddWithValue("@CAMPO", Campo)
                cm.Parameters.AddWithValue("@ESTODOS", IIf(bDscgTodos, 1, 0))
                cm.Parameters.AddWithValue("@NOMBRE", Nombre)
                cm.Parameters.AddWithValue("@FECHA", Fecha)
                cm.Parameters.AddWithValue("@USAIDCONTRATO", IIf(UsaIdContrato, 1, 0))
                cm.Parameters.AddWithValue("@ESARCHCONTRATO", IIf(EsArchivoContrato, 1, 0))
                cm.Parameters.AddWithValue("@ARCHCONTRATOBD", IIf(ArchContratoBd, 1, 0))
                cm.Parameters.AddWithValue("@ESALTAFACTURA", IIf(EsAltafactura, 1, 0))
                cm.CommandType = CommandType.StoredProcedure
                dr = cm.ExecuteReader()

                If dr.Read() Then
                    Return (dr.Item("ACEPTABLE") = 1)
                Else
                    Return False
                End If
            Catch e As Exception
                Throw e
            Finally
                If Not dr Is Nothing AndAlso Not dr.IsClosed Then
                    dr.Close()
                End If
                cn.Dispose()
                cm.Dispose()
            End Try

        End Function
#End Region
#Region " Adjuntos data access methods "
        ''' <summary>
        ''' funcion que crea nuevos ids de adjuntos para los adjuntos copiados en una linea de desglose
        ''' </summary>
        ''' <param name="AdjuntosAct">Adjuntos actuales</param>
        ''' <param name="AdjuntosNew">Adjuntos nuevos</param>
        ''' <param name="bDefecto">Si el adjunto es un adjunto por defecto vendra true</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param> 
        ''' <returns>retorna un array con los ids de adjuntos nuevos</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Adjuntos.vb\CrearAdjuntosCopiados ; Tiempo m�ximo: 0,2</remarks>
        Public Function Adjuntos_CrearAdjuntosCopiados(ByVal lCiaComp As Long, ByVal AdjuntosAct As String, ByVal AdjuntosNew As String, ByVal bDefecto As Boolean,
                                                       Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As String()
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim iUltimoAdjuntoGrabado As Long
            Dim sAdjuntos As String
            Dim arrAdjuntosReturn As String()
            Dim i As Short
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_CREAR_ADJUNTOS_COPIADOS"
                cm.Parameters.AddWithValue("@ADJUNTOSACT", AdjuntosAct)
                cm.Parameters.AddWithValue("@ADJUNTOSNEW", AdjuntosNew)
                cm.Parameters.AddWithValue("@LAST_ADJUNTO", 0)
                cm.Parameters.AddWithValue("@DEFECTO", IIf(bDefecto, 1, 0))
                cm.Parameters("@LAST_ADJUNTO").Direction = ParameterDirection.Output
                cm.Parameters("@LAST_ADJUNTO").Size = -1
                cm.CommandType = CommandType.StoredProcedure
                cm.ExecuteNonQuery()

                iUltimoAdjuntoGrabado = DBNullToDbl(cm.Parameters("@LAST_ADJUNTO").Value())
                sAdjuntos = AdjuntosAct & IIf(AdjuntosAct <> "" And AdjuntosNew <> "", "," & AdjuntosNew, AdjuntosNew)
                arrAdjuntosReturn = sAdjuntos.Split(",")
                For i = 0 To arrAdjuntosReturn.Length - 2
                    arrAdjuntosReturn(i) = (iUltimoAdjuntoGrabado - (i + 1)).ToString
                Next
                arrAdjuntosReturn(arrAdjuntosReturn.Length - 1) = iUltimoAdjuntoGrabado.ToString

                Return arrAdjuntosReturn
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Funcion que devuelve el adjunto de contrato de ese campo
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="idContrato">Id del contrato</param>
        ''' <param name="idArchivoContrato">Id del adjunto del contrato</param>
        ''' <param name="idCampo">id del campo del adjunto</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>retorna el dataset con los datos del adjunto</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Adjuntos.vb\LoadInst_Adj_Contrato ; Tiempo m�ximo: 0,2</remarks>
        Public Function Adjuntos_Contrato_Load(ByVal lCiaComp As Long, ByVal idContrato As Long, ByVal idArchivoContrato As Long, ByVal idCampo As Long,
                                               Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Object
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSWS_GETADJUNTO_CONTRATO"
                If idArchivoContrato <> 0 Then cm.Parameters.AddWithValue("@ID", idArchivoContrato)
                cm.Parameters.AddWithValue("@ID_CONTRATO", idContrato)
                cm.Parameters.AddWithValue("@ID_CAMPO", idCampo)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Funci�n que devuelve un dataset con los adjuntos requeridos por los par�metros
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iTipo">Tipo de adjuntos (1=Adjuntos de campo normal, 3=Adjuntos de campo de desglose)</param>
        ''' <param name="sIdAdjun">String con los Ids de los adjuntos ya guardados en alguna versi�n anterior</param>
        ''' <param name="sIdAdjunNew">String con los Ids de los adjuntos que el usuario haya guardado en esta misma versi�n</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>>
        ''' <returns>Un dataset con los adjuntos solicitados</returns>
        ''' <remarks>Llamada desde: la funci�n LoadInst de Adjuntos.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Function Adjuntos_InstLoad(ByVal lCiaComp As Long, ByVal iTipo As Integer, ByVal sIdAdjun As String, ByVal sIdAdjunNew As String,
                                          Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Object
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim newDr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSWS_GETINSTADJUNTOS"
                cm.Parameters.AddWithValue("@TIPO", iTipo)
                If Not String.IsNullOrEmpty(sIdAdjun) Then cm.Parameters.AddWithValue("@IDS", sIdAdjun)
                If Not String.IsNullOrEmpty(sIdAdjunNew) Then cm.Parameters.AddWithValue("@IDSNEW", sIdAdjunNew)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try

        End Function
        ''' <summary>
        ''' Funci�n que devuelve un dataset con los adjuntos requeridos por los par�metros
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iTipo">Tipo de adjuntos (1=Adjuntos de campo normal, 3=Adjuntos de campo de desglose)</param>
        ''' <param name="sIdAdjun">String con los Ids de los adjuntos ya guardados en alguna versi�n anterior</param>
        ''' <param name="sIdAdjunNew">String con los Ids de los adjuntos que el usuario haya guardado en esta misma versi�n</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un dataSet con los adjuntos solicitados</returns>
        ''' <remarks>Llamada desde: la funci�n Load de Adjuntos.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Function Adjuntos_Load(ByVal lCiaComp As Long, ByVal iTipo As Integer, ByVal sIdAdjun As String, ByVal sIdAdjunNew As String, ByVal sIdi As String,
                                      Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Object
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSWS_GETADJUNTOS"
                cm.Parameters.AddWithValue("@TIPO", iTipo)
                If sIdAdjun = "" Then sIdAdjun = Nothing
                If sIdAdjunNew = "" Then sIdAdjunNew = Nothing
                cm.Parameters.AddWithValue("@IDS", sIdAdjun)
                cm.Parameters.AddWithValue("@IDSNEW", sIdAdjunNew)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Devuelve los archivos adjuntos por instancia y campo
        ''' </summary>
        ''' <param name="lID">Id de la instancia</param>
        ''' <param name="lcampo">Codigo del campo</param>
        ''' <returns>Los archivos adjuntos que correspondan</returns>
        ''' <remarks>
        ''' Llamada desde: PmServer/cAdjuntos/LoadIdsPorInstanciayCampo
        ''' Tiempo m�ximo: 1 seg</remarks>
        Public Function Adjuntos_LoadIdsPorInstanciayCampo(ByVal lCiaComp As Long, ByVal lID As Long, ByVal lcampo As Long,
                                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_GETADJUNTOS_INSTANCIA_CAMPO"
                cm.Parameters.AddWithValue("@ID", lID)
                cm.Parameters.AddWithValue("@CAMPO", lcampo)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region " Almacen data access methods "
        ''' <summary>
        ''' Carga un almacen
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lId">Id de almacen</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Almacen.vb\LoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function Almacen_Get(ByVal lCiaComp As Long, ByVal lId As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm = New SqlCommand
                cm.Connection = cn
                cm.CommandText = "SELECT ID, COD, DEN FROM " & FSGS(lCiaComp) & "ALMACEN WITH (NOLOCK) WHERE ID = " & lId
                cm.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Almacenes data access methods "
        ''' <summary>
        ''' Carga todos los almacenes 
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCodAlmacen">Almacen</param>
        ''' <param name="sCodCentro">Centro</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Almacenes.vb\LoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function Almacenes_Load(ByVal lCiaComp As Long, Optional ByVal sCodAlmacen As String = Nothing, Optional ByVal sCodCentro As String = Nothing,
                                       Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm = New SqlCommand
                cm.Connection = cn
                cm.CommandText = "FSPM_GETALMACENES"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                If Not IsNothing(sCodAlmacen) Then cm.Parameters.AddWithValue("@COD", sCodAlmacen)
                If Not IsNothing(sCodCentro) Then cm.Parameters.AddWithValue("@COD_CENTRO", sCodCentro)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Articulos data access methods "
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="Cod"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Articulo_Get_Properties(ByVal lCiaComp As Long, ByVal Cod As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandText = FSGS(lCiaComp) & "FSWS_ARTICULO_PROPERTIES"
                cm.Parameters.AddWithValue("@COD", Cod)
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr.Tables(0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try

        End Function
		''' <summary>
		''' Carga de articulos
		''' </summary>
		''' <param name="lCiaComp">Id de la compa�ia</param>
		''' <param name="sCod">c�digo del art�culo</param>
		''' <param name="sDen">denominaci�n del art�culo</param>
		''' <param name="bCoincid">si est� a 1 coincidencia total, sino hace un LIKE</param>
		''' <param name="sPer">Persona</param>
		''' <param name="iDesdeFila">Para paginaci�n: n� fila a partir de la cual muestra los datos</param>
		''' <param name="iNumFilas">N� m�ximo de registros a mostra</param>
		''' <param name="sIdioma">Idioma</param>
		''' <param name="sGmn1">Nivel 1 de la familia de materiales</param>
		''' <param name="sGmn2">Nivel 2 de la familia de materiales</param>
		''' <param name="sGmn3">Nivel 3 de la familia de materiales</param>
		''' <param name="sGmn4">Nivel 4 de la familia de materiales</param>
		''' <param name="sMoneda">Moneda</param>
		''' <param name="bCargarUltAdj">Indicador de si carga o no el �ltimo precio adjudicado para el art�culo</param>
		''' <param name="sCodOrgCompras">cod. de la organizaci�n de compra</param>
		''' <param name="sCodCentro">cod. de centro</param>
		''' <param name="sCodPri">para paginaci�n:  c�digo del 1� art�culo a mostrar</param>
		''' <param name="sCodUlt">para paginaci�n:  c�digo del �ltimo art�culo mostrado</param>
		''' <param name="bCargarOrg">si est� a 1 carga la organizacion de compra</param>
		''' <param name="SesionId">Id de sesion</param>
		''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
		''' <param name="PersistID">Valor de la cookie persistente</param>
		''' <returns>Un dataset con los datos cargados</returns>
		''' <remarks>Llamada desde: PMPortalServer\Articulos.vb\LoadData   PMPortalNotificador\Notificar.vb\ObtenerDenominacionValorCampoGS; Tiempo m�ximo: 0,2</remarks>
		Public Function Articulos_Get(ByVal lCiaComp As Long, ByVal sCod As String, ByVal sDen As String, ByVal bCoincid As Boolean, Optional ByVal sPer As String = Nothing,
									  Optional ByVal iDesdeFila As Integer = Nothing, Optional ByVal iNumFilas As Long = Nothing, Optional ByVal sIdioma As String = "SPA",
									  Optional ByVal sGmn1 As String = Nothing, Optional ByVal sGmn2 As String = Nothing, Optional ByVal sGmn3 As String = Nothing, Optional ByVal sGmn4 As String = Nothing,
									  Optional ByVal sMoneda As String = "EUR", Optional ByVal bCargarUltAdj As Boolean = False, Optional ByVal sCodOrgCompras As String = Nothing,
									  Optional ByVal sCodCentro As String = Nothing, Optional ByVal sCodPri As String = Nothing, Optional ByVal sCodUlt As String = Nothing,
									  Optional ByVal bCargarOrg As Boolean = False, Optional ByVal CodProve As String = Nothing,
									  Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
			Authenticate(SesionId, IPDir, PersistID)

			Dim cn As New SqlConnection(mDBConnection)
			Dim cm As New SqlCommand
			Dim dr As New DataSet
			Try
				cn.Open()
				cm.Connection = cn
				cm.CommandText = FSGS(lCiaComp) & "FSWS_ARTICULOS"
				cm.Parameters.AddWithValue("@COD", sCod)
				cm.Parameters.AddWithValue("@DEN", sDen)
				cm.Parameters.AddWithValue("@COINCID", IIf(bCoincid, 1, 0))
				cm.Parameters.AddWithValue("@PER", sPer)
				cm.Parameters.AddWithValue("@FILAS", iNumFilas)
				cm.Parameters.AddWithValue("@DESDEFILA", iDesdeFila)
				cm.Parameters.AddWithValue("@IDI", sIdioma)
				cm.Parameters.AddWithValue("@GMN1", IIf(sGmn1 = "", Nothing, sGmn1))
				cm.Parameters.AddWithValue("@GMN2", IIf(sGmn2 = "", Nothing, sGmn2))
				cm.Parameters.AddWithValue("@GMN3", IIf(sGmn3 = "", Nothing, sGmn3))
				cm.Parameters.AddWithValue("@GMN4", IIf(sGmn4 = "", Nothing, sGmn4))
				cm.Parameters.AddWithValue("@INSTANCIAMONEDA", sMoneda)
				cm.Parameters.AddWithValue("@CARGAR_ULT_ADJ", IIf(bCargarUltAdj, 1, 0))
				If Not String.IsNullOrEmpty(sCodPri) Then cm.Parameters.AddWithValue("@CODINI", sCodPri)
				If Not String.IsNullOrEmpty(sCodUlt) Then cm.Parameters.AddWithValue("@CODULT", sCodUlt)
				If Not sCodOrgCompras Is Nothing Then cm.Parameters.AddWithValue("@CODORGCOMPRAS", sCodOrgCompras)
				If Not sCodCentro Is Nothing Then cm.Parameters.AddWithValue("@CODCENTRO", sCodCentro)
				If Not IsNothing(bCargarOrg) Then cm.Parameters.AddWithValue("@CARGAR_ORG", IIf(bCargarOrg = True, 1, 0))
				If Not String.IsNullOrEmpty(CodProve) Then cm.Parameters.AddWithValue("@CODPROVEEDOR", CodProve)
				cm.CommandType = CommandType.StoredProcedure
				Dim da As New SqlDataAdapter
				da.SelectCommand = cm
				da.Fill(dr)
			Catch e As Exception
				Throw e
			Finally
				cn.Dispose()
				cm.Dispose()
			End Try
			Return dr
		End Function
		''' Revisado por: blp. Fecha: 24/01/2012
		''' <summary>
		''' Recuperamos los art�culos en funci�n de los criterios pasados como par�metros
		''' </summary>
		''' <param name="lCiaComp">C�digo de la compa��a</param>
		''' <param name="lProveID">Id del proveedor</param>
		''' <param name="sCod">C�digo del art�culo</param>
		''' <param name="sDen">denominaci�n del art�culo</param>
		''' <param name="bCoincid">si est� a 1 coincidencia total, sino hace un LIKE</param>
		''' <param name="sPer">c�digo de la persona (</param>
		''' <param name="iDesdeFila">Para paginaci�n: n� fila a partir de la cual muestra los datos</param>
		''' <param name="iNumFilas">N� m�ximo de registros a mostrar</param>
		''' <param name="sIdioma">C�digo del idioma</param>
		''' <param name="sGmn1">Nivel 1 de la familia de materiales</param>
		''' <param name="sGmn2">Nivel 2 de la familia de materiales</param>
		''' <param name="sGmn3">Nivel 3 de la familia de materiales</param>
		''' <param name="sGmn4">Nivel 4 de la familia de materiales</param>
		''' <param name="sMoneda">C�digo de la moneda de la instancia</param>
		''' <param name="bCargarUltAdj">Indicador de si carga o no el �ltimo precio adjudicado para el art�culo</param>
		''' <param name="sCodOrgCompras">cod. de la organizaci�n de compras</param>
		''' <param name="sCodCentro">cod. de centro</param>
		''' <param name="sCodPri">para paginaci�n:  c�digo del 1� art�culo a mostrar</param>
		''' <param name="sCodUlt">para paginaci�n:  c�digo del �ltimo art�culo mostrado</param>
		''' <param name="bCargarOrg">si est� a 1 carga la organizacion de compras</param>
		''' <param name="SesionId">Id de sesion</param>
		''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
		''' <param name="PersistID">Valor de la cookie persistente</param>
		''' <returns>Datset con una tabla que contiene los art�culos</returns>
		''' <remarks>Llamada desde Articulos.vb. M�x. 1 seg.</remarks>
		Public Function Articulos_Prove_Get(ByVal lCiaComp As Long, ByVal lProveID As Long, ByVal sCod As String, ByVal sDen As String, ByVal bCoincid As Boolean,
                                            Optional ByVal sPer As String = Nothing, Optional ByVal iDesdeFila As Integer = Nothing, Optional ByVal iNumFilas As Long = Nothing,
                                            Optional ByVal sIdioma As String = "SPA", Optional ByVal sGmn1 As String = Nothing, Optional ByVal sGmn2 As String = Nothing,
                                            Optional ByVal sGmn3 As String = Nothing, Optional ByVal sGmn4 As String = Nothing, Optional ByVal sMoneda As String = "EUR",
                                            Optional ByVal bCargarUltAdj As Boolean = False, Optional ByVal sCodOrgCompras As String = Nothing, Optional ByVal sCodCentro As String = Nothing,
                                            Optional ByVal sCodPri As String = Nothing, Optional ByVal sCodUlt As String = Nothing, Optional ByVal bCargarOrg As Boolean = False,
                                            Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_ARTICULOS_PROVE"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@PROVE", lProveID)
                cm.Parameters.AddWithValue("@COD", sCod)
                cm.Parameters.AddWithValue("@DEN", sDen)
                cm.Parameters.AddWithValue("@COINCID", IIf(bCoincid, 1, 0))
                cm.Parameters.AddWithValue("@PER", sPer)
                cm.Parameters.AddWithValue("@FILAS", iNumFilas)
                cm.Parameters.AddWithValue("@DESDEFILA", iDesdeFila)
                cm.Parameters.AddWithValue("@IDI", sIdioma)
                cm.Parameters.AddWithValue("@GMN1", IIf(sGmn1 = "", Nothing, sGmn1))
                cm.Parameters.AddWithValue("@GMN2", IIf(sGmn2 = "", Nothing, sGmn2))
                cm.Parameters.AddWithValue("@GMN3", IIf(sGmn3 = "", Nothing, sGmn3))
                cm.Parameters.AddWithValue("@GMN4", IIf(sGmn4 = "", Nothing, sGmn4))
                cm.Parameters.AddWithValue("@INSTANCIAMONEDA", sMoneda)
                cm.Parameters.AddWithValue("@CARGAR_ULT_ADJ", IIf(bCargarUltAdj, 1, 0))
                If Not String.IsNullOrEmpty(sCodPri) Then cm.Parameters.AddWithValue("@CODINI", sCodPri)
                If Not String.IsNullOrEmpty(sCodUlt) Then cm.Parameters.AddWithValue("@CODULT", sCodUlt)
                If Not sCodOrgCompras Is Nothing Then cm.Parameters.AddWithValue("@CODORGCOMPRAS", sCodOrgCompras)
                If Not sCodCentro Is Nothing Then cm.Parameters.AddWithValue("@CODCENTRO", sCodCentro)
                If Not IsNothing(bCargarOrg) Then cm.Parameters.AddWithValue("@CARGAR_ORG", IIf(bCargarOrg = True, 1, 0))
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Auxiliar data access methods "
        ''' <summary>
        ''' Carga las longitudes de los campos personalizados 
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Root.vb\Load_LongitudesDeCodigos     PMPortalNotificador\Notificar.vb\Load_LongitudesDeCodigos; Tiempo m�ximo: 0,2</remarks>
        Public Function LongitudesCampo_Get(ByVal lCiaComp As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "SELECT LONGITUD FROM " & FSGS(lCiaComp) & "DIC WITH(NOLOCK) ORDER BY ID"
                cm.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        Private Function FSGS(ByVal lCiaComp As Long) As String
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim sConsulta As String
            Dim sSRV As Object
            Dim sBD As String

            sConsulta = "SELECT FSGS_SRV,FSGS_BD FROM CIAS WITH(NOLOCK) WHERE ID=" & lCiaComp
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = sConsulta
                cm.CommandType = CommandType.Text
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try

            If dr.Tables(0).Rows.Count = 0 Then
                Return ""
            Else
                sSRV = dr.Tables(0).Rows(0).Item("FSGS_SRV")
                sBD = dr.Tables(0).Rows(0).Item("FSGS_BD")
                If IsDBNull(sSRV) Then
                    Return sBD & ".dbo."
                Else
                    Return sSRV & "." & sBD & ".dbo."
                End If
            End If
        End Function
#End Region
#Region " Roles data access methods"
        ''' <summary>
        ''' Devolver Etapas Bloqueo Salida
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lId">Bloque</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Bloque.vb\DevolverEtapasBloqueoSalida ; Tiempo m�ximo: 0,2</remarks>
        Public Function Bloque_LoadEtapasBloqueoSalida(ByVal lCiaComp As Long, ByVal lId As Integer, ByVal sIdi As String,
                                                       Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_DEVOLVER_ETAPAS_BLOQUEO"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@BLOQUE", lId)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function

        Public Function Roles_Load(ByVal lCiaComp As Long, ByVal lIdBloque As Integer, ByVal codProve As String, ByVal idContacto As Integer,
                                   Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandText = FSGS(lCiaComp) & "FSPM_ROLES_PORTAL"
                cm.Parameters.AddWithValue("@BLOQUE", lIdBloque)
                cm.Parameters.AddWithValue("@PROVE", codProve)
                cm.Parameters.AddWithValue("@CONTACTO", idContacto)

                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region " Campo data access methods "
        ''' <summary>
        ''' Obtiene los Campos calculados a partir de una solicitud
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lId">Id de campo</param>
        ''' <param name="lSolicitud">Solicitud</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Campo.vb\LoadCalculados ; Tiempo m�ximo: 0,2</remarks>
        Public Function Campo_LoadCamposCalculados(ByVal lCiaComp As Long, ByVal lId As Long, ByVal lSolicitud As Long, Optional ByVal sProve As String = Nothing, Optional ByVal idUsu As Integer = Nothing,
                                                   Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_LOADFIELDSDESGLOSECALC"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CAMPO", lId)
                cm.Parameters.AddWithValue("@SOLICITUD", lSolicitud)
                If Not String.IsNullOrEmpty(sProve) Then cm.Parameters.AddWithValue("@PROVE", sProve)
                If Not String.IsNullOrEmpty(idUsu) Then cm.Parameters.AddWithValue("@CONTACTO", idUsu)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Obtiene los Campos calculados a partir de una instancia
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lId">ID de campo</param>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Campo.vb\LoadInstCalculados ; Tiempo m�ximo: 0,2</remarks>
        Public Function Campo_LoadInstCamposCalculados(ByVal lCiaComp As Long, ByVal lId As Long, Optional ByVal lInstancia As Long = Nothing, Optional ByVal sProve As String = Nothing,
                                                       Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_LOADINSTFIELDSDESGLOSECALC"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CAMPO", lId)
                cm.Parameters.AddWithValue("@INSTANCIA", lInstancia)
                cm.Parameters.AddWithValue("@PROVE", sProve)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Carga el campo
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lID">Id de campo</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lSolicitud">Solicitud</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Campo.vb\Load ; Tiempo m�ximo: 0,2</remarks>
        Public Function Campo_Load(ByVal lCiaComp As Long, ByVal lID As Long, Optional ByVal sIdi As String = Nothing, Optional ByVal lSolicitud As Long = Nothing,
                                   Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSWS_GETFIELD"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@ID", lID)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@ATTACH", 1)
                cm.Parameters.AddWithValue("@SOLICITUD", lSolicitud)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
                dr.Relations.Add("REL_CAMPO_LINEA", dr.Tables(0).Columns("ID"), dr.Tables(1).Columns("CAMPO"), False)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Funcion que devuelve el desglose de un campo de una instancia
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lID">Identificador del campo</param>
        ''' <param name="lIDCopiaCampo">Identificador del copia campo</param>
        ''' <param name="sIdi">Idioma de la aplicacion</param>
        ''' <param name="lInstancia">Id de la instancia</param>
        ''' <param name="sUsu">Usuario</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <param name="lVersion">Version del Desglose</param>
        ''' <param name="bDefecto">Booleano que indica si es la instancia por defecto</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>   
        ''' <param name="bNuevoWorkflow">Booleano que indica si es un nuevo proceso</param>
        ''' <param name="lVersionBd">Version de la instancia en bbdd. Los certificados, una vez q hay instancia, para determinar si los 
        ''' campos del desglose son visible/editables usan la version de la tabla certificado.
        ''' En caso de desgloses No pop siempre llega a cero, pero esta bien pq @VERSION contendra la version de bbdd y se asigna @VERSION_BD_CERT=@VERSION</param>
        ''' <param name="bSacarNumLinea">Si se saca NumLinea en el desglose o no . Tarea 3369</param> 
        ''' <returns>Un dataset con los datos de desglose de instancia</returns>
        ''' <remarks>Llamada desde: Campo/LoadInstDesglose; Tiempo m�ximo: 0,5 seg</remarks>
        Public Function CampoInst_LoadDesglose(ByVal lCiaComp As Long, ByVal lID As Long, ByRef lIDCopiaCampo As Long, Optional ByVal sIdi As String = Nothing,
                        Optional ByVal lInstancia As Long = Nothing, Optional ByVal sUsu As String = Nothing, Optional ByVal sProve As String = Nothing,
                        Optional ByVal lVersion As Integer = Nothing, Optional ByVal bDefecto As Boolean = False, Optional ByVal SesionId As String = "",
                        Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "", Optional ByVal bNuevoWorkFlow As Boolean = False,
                        Optional ByVal lVersionBd As Long = Nothing, Optional ByVal bSacarNumLinea As Boolean = False) As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim parentCols(2) As DataColumn
            Dim childCols(2) As DataColumn
            Try
                cn.Open()
                cm.Connection = cn
                If bNuevoWorkFlow Then
                    cm.CommandText = FSGS(lCiaComp) & "FSPM_GET_DESGLOSE_INSTANCIA"
                Else
                    cm.CommandText = FSGS(lCiaComp) & "FSWS_GETINSTDESGLOSE"
                End If
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@ID", lID)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@INSTANCIA", lInstancia)
                cm.Parameters.AddWithValue("@USU", sUsu)
                cm.Parameters.AddWithValue("@COPIA_CAMPO", lIDCopiaCampo)
                cm.Parameters("@COPIA_CAMPO").Direction = ParameterDirection.Output
                cm.Parameters.AddWithValue("@PROVE", sProve)
                cm.Parameters.AddWithValue("@VERSION", lVersion)
                cm.Parameters.AddWithValue("@DEFECTO", IIf(bDefecto, 1, 0))
                If Not bNuevoWorkFlow Then cm.Parameters.AddWithValue("@VERSION_BD_CERT", lVersionBd)
                If bSacarNumLinea Then cm.Parameters.AddWithValue("@SALENUMLINEA", 1)
                da.SelectCommand = cm
                da.Fill(dr)

                lIDCopiaCampo = IIf(IsDBNull(cm.Parameters("@COPIA_CAMPO").Value), Nothing, cm.Parameters("@COPIA_CAMPO").Value)

                dr.Tables(2).TableName = "LINEAS"
                dr.Tables(3).TableName = "ADJUNTOS"

                Dim bhaylineas As Boolean
                If bNuevoWorkFlow = False Then
                    bhaylineas = True
                    dr.Tables(4).TableName = "ESTADO"
                    If dr.Tables.Count > 5 And Not bDefecto Then
                        dr.Tables(IIf(bhaylineas, 5, 4)).TableName = "LINEAS_SIN"
                        dr.Tables(IIf(bhaylineas, 6, 5)).TableName = "ADJUNTOS_SIN"
                        dr.Tables(IIf(bhaylineas, 7, 6)).TableName = "ESTADO_SIN"
                    ElseIf dr.Tables.Count > 7 And bDefecto Then
                        dr.Tables(IIf(bhaylineas, 7, 8)).TableName = "LINEAS_SIN"
                        dr.Tables(IIf(bhaylineas, 8, 9)).TableName = "ADJUNTOS_SIN"
                        dr.Tables(IIf(bhaylineas, 9, 10)).TableName = "ESTADO_SIN"
                    End If
                End If

                Dim keysCampos(0) As DataColumn
                keysCampos(0) = dr.Tables(0).Columns("ID")
                dr.Tables(0).PrimaryKey = keysCampos

                Dim keysDesglose(2) As DataColumn
                keysDesglose(0) = dr.Tables("LINEAS").Columns("LINEA")
                keysDesglose(1) = dr.Tables("LINEAS").Columns("CAMPO_PADRE")
                keysDesglose(2) = dr.Tables("LINEAS").Columns("CAMPO_HIJO")
                dr.Tables("LINEAS").PrimaryKey = keysDesglose

                Dim sTblLineas As String = "LINEAS"
                Dim sTblAdjuntos As String = "ADJUNTOS"
                If bNuevoWorkFlow = False Then
                    If dr.Tables.Count > 5 And Not bDefecto Then
                        sTblLineas = "LINEAS_SIN"
                        sTblAdjuntos = "ADJUNTOS_SIN"
                    ElseIf dr.Tables.Count > 7 And bDefecto Then
                        sTblLineas = "LINEAS_SIN"
                        sTblAdjuntos = "ADJUNTOS_SIN"
                    End If
                End If

                parentCols(0) = dr.Tables(sTblLineas).Columns("CAMPO_PADRE")
                parentCols(1) = dr.Tables(sTblLineas).Columns("CAMPO_HIJO")
                parentCols(2) = dr.Tables(sTblLineas).Columns("LINEA")
                childCols(0) = dr.Tables(sTblAdjuntos).Columns("CAMPO_PADRE")
                childCols(1) = dr.Tables(sTblAdjuntos).Columns("CAMPO_HIJO")
                childCols(2) = dr.Tables(sTblAdjuntos).Columns("LINEA")

                dr.Relations.Add("REL_LINEA_ADJUNTO", parentCols, childCols, False)
                dr.Relations.Add("REL_CAMPO_LISTA", dr.Tables(0).Columns("COPIA_CAMPO_DEF"), dr.Tables(1).Columns("CAMPO_DEF"), False)
                If bNuevoWorkFlow Then dr.Relations.Add("REL_CAMPO_BLOQUEO", dr.Tables(0).Columns("COPIA_CAMPO_DEF"), dr.Tables(4).Columns("CAMPO"), False)

                If bDefecto Then
                    parentCols(0) = dr.Tables(5).Columns("CAMPO_PADRE")
                    parentCols(1) = dr.Tables(5).Columns("CAMPO_HIJO")
                    parentCols(2) = dr.Tables(5).Columns("LINEA")

                    childCols(0) = dr.Tables(6).Columns("CAMPO_PADRE")
                    childCols(1) = dr.Tables(6).Columns("CAMPO_HIJO")
                    childCols(2) = dr.Tables(6).Columns("LINEA")

                    dr.Relations.Add("REL_LINEA_ADJUNTO_DEFECTO", parentCols, childCols, False)
                End If
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' carga el desglose
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lID">Id de campo</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lSolicitud">Solicitud</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Campo.vb\LoadDesglose ; Tiempo m�ximo: 0,2</remarks>
        Public Function Campo_LoadDesglose(ByVal lCiaComp As Long, ByVal lID As Long, Optional ByVal sIdi As String = Nothing, Optional ByVal lSolicitud As Long = Nothing,
                                           Optional ByVal bNuevoWorkfl As Boolean = False, Optional ByVal sProve As String = Nothing, Optional ByVal idUsu As Integer = Nothing,
                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.StoredProcedure
                If bNuevoWorkfl Then
                    cm.CommandText = FSGS(lCiaComp) & "FSPM_GETDESGLOSE"
                    If Not String.IsNullOrEmpty(sProve) Then cm.Parameters.AddWithValue("@PROVE", sProve)
                    If Not String.IsNullOrEmpty(idUsu) Then cm.Parameters.AddWithValue("@CONTACTO", idUsu)
                Else
                    cm.CommandText = FSGS(lCiaComp) & "FSWS_GETDESGLOSE"
                    cm.Parameters.AddWithValue("@PORTAL", 1)
                End If
                cm.Parameters.AddWithValue("@ID", lID)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@SOLICITUD", lSolicitud)

                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                Dim parentCols(2) As DataColumn
                Dim childCols(2) As DataColumn
                dr.Tables(2).TableName = "LINEAS"
                dr.Tables(3).TableName = "ADJUNTOS"

                Dim keysDesglose(2) As DataColumn
                keysDesglose(0) = dr.Tables(2).Columns("LINEA")
                keysDesglose(1) = dr.Tables(2).Columns("CAMPO_PADRE")
                keysDesglose(2) = dr.Tables(2).Columns("CAMPO_HIJO")

                dr.Tables(2).PrimaryKey = keysDesglose

                parentCols(0) = dr.Tables(2).Columns("CAMPO_PADRE")
                parentCols(1) = dr.Tables(2).Columns("CAMPO_HIJO")
                parentCols(2) = dr.Tables(2).Columns("LINEA")
                childCols(0) = dr.Tables(3).Columns("CAMPO_PADRE")
                childCols(1) = dr.Tables(3).Columns("CAMPO_HIJO")
                childCols(2) = dr.Tables(3).Columns("LINEA")

                dr.Relations.Add("REL_LINEA_ADJUNTO", parentCols, childCols, False)
                dr.Relations.Add("REL_CAMPO_LISTA", dr.Tables(0).Columns("ID"), dr.Tables(1).Columns("CAMPO"), False)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Carga un campo de la instancia
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>    
        ''' <param name="lInstancia">Id de la instancia</param>      
        ''' <param name="lId">Id del campo</param>   
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lCiaProv">Proveedor</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>   
        ''' <returns>Un dataset</returns>
        ''' <remarks>Llamada desde: Campo.vb\LoadInst; Tiempo m�ximo:0,1</remarks>
        Public Function CampoInst_Load(ByVal lCiaComp As Long, ByVal lInstancia As Long, ByVal lID As Long, Optional ByVal sIdi As String = Nothing, Optional ByVal lCiaProv As Long = Nothing,
                                       Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSWS_GETINSTFIELD"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@INSTANCIA", lInstancia)
                cm.Parameters.AddWithValue("@ID", lID)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                da.SelectCommand = cm
                da.Fill(dr)
                dr.Relations.Add("REL_CAMPO_LINEA", dr.Tables(0).Columns("ID"), dr.Tables(1).Columns("CAMPO_DEF"), False)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Carga los adjuntos por defecto
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="Solicitud">Solicitud</param>
        ''' <param name="CampoPadre">ID de campo desglose</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Root.vb\Load_AdjuntosDefecto ; Tiempo m�ximo: 0,2</remarks>
        Public Function Campo_Load_AdjuntosDefecto(ByVal lCiaComp As Long, ByVal Solicitud As Long, ByVal CampoPadre As Long,
                                                   Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GETINSTDESGLOSE_ADJUNTODEFECTO"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@SOLICITUD", Solicitud)
                cm.Parameters.AddWithValue("@ID", CampoPadre)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Recoge los valores del combo de tipo lista enlazada que se necesita
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>     
        ''' <param name="lIdCampo">El Id del campo del combo</param>
        ''' <param name="iOrdenCampoPadre">El Orden del campo padre para obtener los valores referentes al valor del campo padre</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lInstancia">El Id de la instancia con la que se esta trabajando</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>los valores del combo de tipo lista enlazada que se necesita</returns>
        ''' <remarks>Llamada desde: PMPortalServer/Campo.vb\DevolverListaEnlazada;  Tiempo maximo:0,1</remarks>
        Public Function Campo_DevolverListaEnlazada(ByVal lCiaComp As Long, ByVal lIdCampo As Long, ByVal iOrdenCampoPadre As Integer, ByVal sIdi As String,
                                                    ByVal lInstancia As Long, ByVal lBloque As Long, ByVal lSolicitud As Long, ByVal sCodProve As String,
                                                    Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPMQA_DEVOLVER_VALORES_LISTA"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDCAMPO", lIdCampo)
                cm.Parameters.AddWithValue("@ORDEN_CAMPOPADRE", iOrdenCampoPadre)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@INSTANCIA", lInstancia)
                cm.Parameters.AddWithValue("@BLOQUE_ACTUAL", lBloque)
                cm.Parameters.AddWithValue("@SOLICITUD", lSolicitud)
                cm.Parameters.AddWithValue("@PROVE", sCodProve)
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>Recoge los valores del combo de tipo lista enlazada a un campo de tipo material</summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>   
        ''' <param name="lIdCampo">El Id del campo del combo</param>
        ''' <param name="sGMN1">GMN1</param>
        ''' <param name="sGMN2">GMN2</param>
        ''' <param name="sGMN3">GMN3</param>
        ''' <param name="sGMN4">GMN4</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <returns></returns>
        Public Function Campo_DevolverListaCampoPadreMaterial(ByVal lCiaComp As Long, ByVal lIdCampo As Long, ByVal sGMN1 As String, ByVal sGMN2 As String, ByVal sGMN3 As String, ByVal sGMN4 As String, ByVal sIdi As String,
                                                    Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPMQA_DEVOLVER_VALORES_LISTA_CAMPOPADRE_MATERIAL"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDCAMPO", lIdCampo)
                cm.Parameters.AddWithValue("@GMN1", sGMN1)
                If sGMN2.Length > 0 Then cm.Parameters.AddWithValue("@GMN2", sGMN2)
                If sGMN3.Length > 0 Then cm.Parameters.AddWithValue("@GMN3", sGMN3)
                If sGMN4.Length > 0 Then cm.Parameters.AddWithValue("@GMN4", sGMN4)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region " Centro data access methods "
        ''' <summary>
        ''' Carga un centro
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCod">Cod de centro</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Centro.vb\LoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function Centro_Get(ByVal lCiaComp As Long, ByVal sCod As String,
                                   Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "SELECT COD,DEN FROM " & FSGS(lCiaComp) & "CENTROS WITH (NOLOCK) WHERE COD = '" & sCod & "'"
                cm.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Centros data access methods "
        ''' <summary>
        '''  Carga todos los centros
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCodCentro">Centros</param>
        ''' <param name="sCodOrgCompras">Organizacion de compras</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Centros.vb\LoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function Centros_Load(ByVal lCiaComp As Long, Optional ByVal sCodCentro As String = Nothing, Optional ByVal sCodOrgCompras As String = Nothing,
                                     Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GETCENTROS"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@COD", IIf(IsNothing(sCodCentro), Nothing, sCodCentro))
                If Not IsNothing(sCodOrgCompras) Then cm.Parameters.AddWithValue("@COD_ORGCOMPRAS", sCodOrgCompras)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region "Centros de Coste data access methods"
        Public Function CentrosCoste_Load(ByVal lCiaComp As Long, ByVal sUsuario As String, ByVal sIdioma As String, Optional ByVal bVerUON As Boolean = False, Optional ByVal sDesde As String = "",
                                          Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSSM_DEVOLVER_CENTROS_COSTE"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@IDI", sIdioma)
                cm.Parameters.AddWithValue("@VERUON", bVerUON)
                If sUsuario <> "" Then cm.Parameters.AddWithValue("@USU", sUsuario)
                cm.Parameters.AddWithValue("@DESDE", sDesde)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)

                Dim i As Byte
                For Each dt As DataTable In dr.Tables
                    dt.TableName = "Tabla_" & i.ToString
                    i += 1
                Next
                Dim parentCols(0) As DataColumn
                Dim childCols(0) As DataColumn
                Dim dt0(0) As DataColumn
                Dim dt1(1) As DataColumn

                dt0(0) = dr.Tables(0).Columns("COD0")
                dt1(0) = dr.Tables(1).Columns("COD0")
                dt1(1) = dr.Tables(1).Columns("ID_CENTRO_SM")

                dr.Tables(0).PrimaryKey = dt0
                dr.Tables(1).PrimaryKey = dt1

                parentCols(0) = dr.Tables(0).Columns("COD0")
                childCols(0) = dr.Tables(1).Columns("COD0")

                dr.Relations.Add("REL_NIV0_NIV1", parentCols, childCols, False)

                If dr.Tables.Count > 2 Then
                    ReDim parentCols(0)
                    ReDim childCols(0)

                    parentCols(0) = dr.Tables(1).Columns("COD")
                    childCols(0) = dr.Tables(2).Columns("UON1")

                    dr.Relations.Add("REL_NIV1_NIV2", parentCols, childCols, False)

                    ReDim parentCols(1)
                    ReDim childCols(1)

                    parentCols(0) = dr.Tables(2).Columns("UON1")
                    parentCols(1) = dr.Tables(2).Columns("COD")
                    childCols(0) = dr.Tables(3).Columns("UON1")
                    childCols(1) = dr.Tables(3).Columns("UON2")
                    dr.Relations.Add("REL_NIV2_NIV3", parentCols, childCols, False)

                    ReDim parentCols(2)
                    ReDim childCols(2)

                    parentCols(0) = dr.Tables(3).Columns("UON1")
                    parentCols(1) = dr.Tables(3).Columns("UON2")
                    parentCols(2) = dr.Tables(3).Columns("COD")
                    childCols(0) = dr.Tables(4).Columns("UON1")
                    childCols(1) = dr.Tables(4).Columns("UON2")
                    childCols(2) = dr.Tables(4).Columns("UON3")
                    dr.Relations.Add("REL_NIV3_NIV4", parentCols, childCols, False)
                End If
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Funcion al que se la pasa el codigo de centro de coste y devuelve la denominacion y las Uons
        ''' </summary>
        ''' <param name="lCiaComp">cod compa�ia</param>
        ''' <param name="sCod">codigo del centro de coste</param>
        ''' <param name="sIdioma">idioma de la aplicacion</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function CentrosCoste_BuscarCentro(ByVal lCiaComp As Long, ByVal sCod As String, ByVal sIdioma As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm = New SqlCommand
                cm.Connection = cn
                cm.CommandText = "FSPM_BUSCAR_CENTROCOSTE"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@IDI", sIdioma)
                cm.Parameters.AddWithValue("@COD", sCod)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)

                If dr.Tables.Count > 0 Then
                    Return dr.Tables(0)
                Else
                    Return New DataTable("CENTRO")
                End If
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region " Certificado Access Methods "
        ''' Revisado por: Jbg. Fecha: 13/12/2011
        ''' <summary>
        ''' Deshace la creaci�n de un certificado
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>    
        ''' <param name="lCiaProve">Id de proveedor conectado</param>      
        ''' <param name="Prove">Codigo de proveedor conectado</param>
        ''' <param name="Certificado">Id de certificado a eliminar</param>   
        ''' <param name="Instancia">Id de instancia a eliminar</param>
        ''' <param name="IdCreate">Los registros de portal/copia_campo hay q quitarlos</param>   
        ''' <param name="SoloServerPortal">Si solo se han hecho inserciones en bbdd portal</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>   
        ''' <remarks>Llamada desde: guardarinstancia.aspx/GuardarSinWorkflow; Tiempo m�ximo:0,1</remarks>
        Public Sub Certificado_DesCrearCertificado(ByVal lCiaComp As Long, ByVal lCiaProve As Long, ByVal Prove As String, ByVal Certificado As String,
                                            ByVal Instancia As String, ByVal IdCreate As String, ByVal SoloServerPortal As Boolean,
                                            Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As SqlCommand
            Dim sql As String

            cn.Open()
            cm = New SqlCommand("BEGIN DISTRIBUTED TRANSACTION", cn)
            cm.ExecuteNonQuery()
            cm = New SqlCommand("SET XACT_ABORT ON", cn)
            cm.ExecuteNonQuery()
            Try
                If Not SoloServerPortal Then
                    'SERVER2
                    cm = New SqlCommand
                    cm.Connection = cn
                    sql = "EXEC FSQA_ELIMINAR_CERTIFICADO @CERTIFICADO=@CERTIFICADO, @INSTANCIA=@INSTANCIA ,@VERSION=@VERSION"
                    cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@CERTIFICADO INT OUTPUT, @INSTANCIA INT , @VERSION INT', @CERTIFICADO, @INSTANCIA, @VERSION"
                    cm.CommandType = CommandType.Text
                    cm.Parameters.Add("@stmt", SqlDbType.NText, sql.Length)
                    cm.Parameters("@stmt").Value = sql
                    cm.Parameters.AddWithValue("@CERTIFICADO", SqlDbType.Int).Direction = ParameterDirection.InputOutput
                    cm.Parameters("@CERTIFICADO").Value = CLng(Certificado)
                    cm.Parameters.AddWithValue("@INSTANCIA", CLng(Instancia))
                    cm.Parameters.AddWithValue("@VERSION", 1)
                    cm.ExecuteNonQuery()
                End If

                'SERVER3
                sql = "ALTER TABLE COPIA_CAMPO_ADJUN NOCHECK CONSTRAINT ALL " + vbCrLf
                sql += "ALTER TABLE COPIA_CAMPO NOCHECK CONSTRAINT ALL " + vbCrLf
                sql += "DELETE FROM COPIA_CAMPO_ADJUN WHERE CIA=" + CStr(lCiaProve) + " AND CAMPO IN (SELECT ID FROM COPIA_CAMPO WITH(NOLOCK) WHERE CIA=" + CStr(lCiaProve) + " AND IDCREATE=" + CStr(IdCreate) + ")" + vbCrLf
                sql += "DELETE FROM COPIA_CAMPO WHERE CIA=" + CStr(lCiaProve) + " AND IDCREATE=" + CStr(IdCreate) + vbCrLf
                sql += "ALTER TABLE COPIA_CAMPO CHECK CONSTRAINT ALL " + vbCrLf
                sql += "ALTER TABLE COPIA_CAMPO_ADJUN CHECK CONSTRAINT ALL " + vbCrLf

                cm = New SqlCommand
                cm.Connection = cn
                cm.CommandText = "EXEC sp_executesql @stmt"
                cm.CommandType = CommandType.Text
                cm.Parameters.Add("@stmt", SqlDbType.NText, sql.Length)
                cm.Parameters("@stmt").Value = sql
                cm.ExecuteNonQuery()

                cm = New SqlCommand("SET XACT_ABORT OFF", cn)
                cm.ExecuteNonQuery()
                cm = New SqlCommand("COMMIT TRANSACTION", cn)
                cm.ExecuteNonQuery()
            Catch ex As Exception
                cm = New SqlCommand("SET XACT_ABORT OFF", cn)
                cm.ExecuteNonQuery()
                cm = New SqlCommand("IF @@TRANCOUNT>0 ROLLBACK TRANSACTION", cn)
                cm.ExecuteNonQuery()
                Throw ex
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Sub
        ''' <summary>
        ''' Procedimiento que carga los datos generales de un certificado
        ''' </summary>
        ''' <param name="lCiaComp">El id de la compa�ia para obtener donde esta la base de datos de la compa�ia</param>
        ''' <param name="lId">Id de certificado</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="bPortal">InputOutput. Null al entrar para indicar llamada desde Portal. 
        '''         Al salir tiene 1 si esta 'guardada sin enviar' y 0 eoc</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <remarks>Llamada desde: Server/Certificado/Load; Tiempo m�ximo:0,3 seg </remarks>
        Public Function Certificado_Load(ByVal lCiaComp As Long, ByVal sIdi As String, Optional ByVal lId As Long = Nothing, Optional ByRef bPortal As Boolean = False,
                                         Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
				cm.CommandText = FSGS(lCiaComp) & "FSQA_LOADCERTIFICADO"
				cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CERTIFICADO", lId)
                cm.Parameters.AddWithValue("@PORTAL", SqlDbType.TinyInt)
                cm.Parameters("@PORTAL").Direction = ParameterDirection.Output
                cm.Parameters.AddWithValue("@IDIOMA", sIdi)
                da.SelectCommand = cm
                da.Fill(dr)

                bPortal = SQLBinaryToBoolean(cm.Parameters("@PORTAL").Value)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Procedimiento que carga datos de quien realizo las diferentes versiones(respuestas) de un certificado 
        ''' </summary>
        ''' <param name="lCiaComp">El id de la compa�ia para obtener donde esta la base de datos de la compa�ia</param>
        ''' <param name="lId">Id de certificado</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <remarks>Llamada desde: Server/Certificado/CargarRespuestas; Tiempo m�ximo:0,3 seg </remarks>
        Public Function Certificado_CargarRespuestas(ByVal lCiaComp As Long, ByVal sProve As String, Optional ByVal lId As Long = Nothing, Optional ByVal sIdi As String = "SPA",
                                                     Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSQA_LOADRESPUESTASCERTIFICADO"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@PROVE", sProve)
                cm.Parameters.AddWithValue("@CERTIFICADO", lId)
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Carga los id de copia_campo para la version actual y para la version q se le pasa como parametro
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="lId">Instancia</param>
        ''' <param name="iVersion">Version</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <remarks>Llamada desde: CargarCorrespondenciaCampos.vb\CargarCorrespondenciaCampos ; Tiempo m�ximo: 0,2</remarks>
        Public Function Certificado_CargarCorrespondenciaCampos(ByVal lCiaComp As Long, ByVal lId As Long, ByVal iVersion As Integer,
                                                                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSQA_LOADCORRESPONDENCIACAMPOS"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CERTIFICADO", lId)
                cm.Parameters.AddWithValue("@VERSION", iVersion)
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Carga los certificados publicados del proveedor
        ''' </summary>
        ''' <param name="lCiaComp">El id de la compa�ia para obtener donde esta la base de datos de la compa�ia</param>
        ''' <param name="Prove">C�digo del proveedor en el portal</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: Certificados\Load_Certificados; Tiempo m�ximo: 2sg</remarks>
        Public Function Load_Certificados(ByVal lCiaComp As Long, ByVal Prove As String, Optional ByVal sIdi As String = "SPA",
                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
				cm.CommandText = FSGS(lCiaComp) & "FSQA_GET_CERTIFICADOS_PROVE"
				cm.CommandType = CommandType.StoredProcedure
				cm.Parameters.AddWithValue("@PROVE", Prove)
				Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Determina si el lIdCampo q se le pasa es de instancia lIdInstancia o no
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa��a</param>
        ''' <param name="lIdCampo">Id de copia_campo o Id de campo</param>
        ''' <param name="lIdInstancia">Instancia a la q ver si pertenece</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param> 
        ''' <returns>Si lIdCampo es id de instancia o no</returns>
        ''' <remarks>Llamada desde: Certificado.vb\ControlPrimerGuardadoCert ; Tiempo maximo:0,2</remarks>
        Public Function Certificado_ControlPrimerGuardadoCert(ByVal lCiaComp As Long, ByVal lIdCampo As Long, ByVal lIdInstancia As Long,
                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Boolean
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSQA_COMPROBACIONCAMPOCERTIFICADO"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDCAMPO", lIdCampo)
                cm.Parameters.AddWithValue("@INSTANCIA", lIdInstancia)
                da.SelectCommand = cm
                da.Fill(dr)

                Return (dr.Tables(0).Rows(0).Item("RES") = 0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Function Certificado_HayCertificadosPendientesCumplimentar(ByVal lCumpOEnviar As Long, ByVal lCiaComp As Long, ByVal sProve As String, Optional ByVal lId As Long = Nothing, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Boolean
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSQA_CERTIFICADO_PDTE_CUMPLIMENTAR"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@PROVEEDOR", sProve)
                cm.Parameters.AddWithValue("@IDCERTIFICADO", lId)
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@TIPO", lCumpOEnviar)
                da.SelectCommand = cm
                da.Fill(dr)

                Return (dr.Tables(0).Rows(0).Item("RES") <> 0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region "Colaboraci�n Access Methods"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="ProveCod"></param>
        ''' <param name="IdCon"></param>
        ''' <param name="Idioma"></param>
        ''' <param name="tipo"></param>
        ''' <param name="megusta"></param>
        ''' <param name="LimiteMensajesCargados"></param>
        ''' <param name="Pagina"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function CN_ObtenerMensajesUsuario(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdCon As Integer, ByVal Idioma As String,
                ByVal tipo As Integer, ByVal megusta As Boolean,
                ByVal LimiteMensajesCargados As Integer, ByVal Pagina As Integer,
                ByVal Discrepancias As Boolean, ByVal Factura As Integer, ByVal Linea As Integer,
                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "CN_OBTENER_MENSAJESPROVEEDOR"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@PROVE", ProveCod)
                cm.Parameters.AddWithValue("@CON", IdCon)
                cm.Parameters.AddWithValue("@IDI", Idioma)
                If Not tipo = 0 Then cm.Parameters.AddWithValue("@TIPO", tipo)
                cm.Parameters.AddWithValue("@LIMITEMENSAJESCARGADOS", LimiteMensajesCargados)
                cm.Parameters.AddWithValue("@PAGINA", Pagina)
                cm.Parameters.AddWithValue("@DISCREPANCIAS", Discrepancias)
                If Not Factura = 0 Then cm.Parameters.AddWithValue("@FACTURA", Factura)
                If Not Linea = 0 Then cm.Parameters.AddWithValue("@LINEA", Linea)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                dr.Tables(0).TableName = "MENSAJES"
                dr.Tables(1).TableName = "RESPUESTAS"
                dr.Tables(2).TableName = "MEGUSTA"
                dr.Tables(3).TableName = "MEGUSTARESPUESTA"
                dr.Tables(4).TableName = "ADJUNTOS"

                Return dr
            Catch ex As SqlException
                Throw ex
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Comprueba el n�mero de mensajes nuevos del usuario por tipo.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function CN_Comprobar_Notificaciones(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdCon As Integer,
                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            cn.Open()
            Try
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "CN_COMPROBAR_NOTIFICACIONES_PROVE"
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                With cm.Parameters
                    .AddWithValue("@PROVE", ProveCod)
                    .AddWithValue("@CON", IdCon)
                End With
                da.SelectCommand = cm
                da.Fill(dr)

                dr.Tables(0).TableName = "NOTIFICACIONES"
                dr.Tables(1).TableName = "URGENTES"

                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        Public Function CN_Mensajes_GetAdjuntoMensaje(ByVal lCiaComp As Integer, ByVal DGuid As String) As Byte()
            Dim cn As New SqlConnection
            Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

            cn = NuevaConexion()
            mi_impersionationContext = RealizarSuplantacion()
            cn.Open()

            Dim tx As SqlTransaction = cn.BeginTransaction()
            Dim oSqlFileStream As SqlTypes.SqlFileStream = CN_GetSqlFileStreamAdjunto(lCiaComp, 0, DGuid, cn, tx)
            If Not oSqlFileStream Is Nothing Then
                Dim Buffer(oSqlFileStream.Length - 1) As Byte
                oSqlFileStream.Read(Buffer, 0, Buffer.Length)
                oSqlFileStream.Close()
                CN_Mensajes_GetAdjuntoMensaje = Buffer
            Else
                CN_Mensajes_GetAdjuntoMensaje = Nothing
            End If
            tx.Commit()
            cn.Close()
            DeshacerSuplantacion(mi_impersionationContext)
        End Function
        Public Function CN_Mensajes_GetImagenUsuario(ByVal lCiaComp As Integer, ByVal DGuid As String) As Byte()
            Dim cn As New SqlConnection
            Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

            cn = NuevaConexion()
            mi_impersionationContext = RealizarSuplantacion()
            cn.Open()

            Dim tx As SqlTransaction = cn.BeginTransaction()
            Dim oSqlFileStream As SqlTypes.SqlFileStream = CN_GetSqlFileStreamImagen(lCiaComp, DGuid, cn, tx)
            If Not oSqlFileStream Is Nothing Then
                Dim Buffer(oSqlFileStream.Length - 1) As Byte
                oSqlFileStream.Read(Buffer, 0, Buffer.Length)
                oSqlFileStream.Close()
                CN_Mensajes_GetImagenUsuario = Buffer
            Else
                CN_Mensajes_GetImagenUsuario = Nothing
            End If
            tx.Commit()
            cn.Close()
            DeshacerSuplantacion(mi_impersionationContext)
        End Function
        Public Function CN_Obtener_Mensaje(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdCon As Integer,
                ByVal Idioma As String, ByVal IdMensaje As Integer,
                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "CN_OBTENER_MENSAJE_PROVE"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@PROVE", ProveCod)
                cm.Parameters.AddWithValue("@CON", IdCon)
                cm.Parameters.AddWithValue("@IDI", Idioma)
                cm.Parameters.AddWithValue("@IDMENSAJE", IdMensaje)
                da.SelectCommand = cm
                da.Fill(dr)

                dr.Tables(0).TableName = "MENSAJES"
                dr.Tables(1).TableName = "RESPUESTAS"
                dr.Tables(2).TableName = "MEGUSTA"
                dr.Tables(3).TableName = "MEGUSTARESPUESTA"
                dr.Tables(4).TableName = "ADJUNTOS"

                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="ProveCod"></param>
        ''' <param name="IdCon"></param>
        ''' <param name="Idioma"></param>
        ''' <param name="IdMensaje"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function CN_MensajeUrgente_Leido(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdCon As Integer,
                ByVal Idioma As String, ByVal IdMensaje As Integer,
                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "CN_MENSAJEURGENTE_LEIDO_PROVE"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@PROVE", ProveCod)
                cm.Parameters.AddWithValue("@CON", IdCon)
                cm.Parameters.AddWithValue("@IDI", Idioma)
                cm.Parameters.AddWithValue("@IDMENSAJE", IdMensaje)
                da.SelectCommand = cm
                da.Fill(dr)

                If dr.Tables.Count > 0 Then
                    dr.Tables(0).TableName = "MENSAJES"
                    dr.Tables(1).TableName = "RESPUESTAS"
                    dr.Tables(2).TableName = "MEGUSTA"
                    dr.Tables(3).TableName = "MEGUSTARESPUESTA"
                    dr.Tables(4).TableName = "ADJUNTOS"
                End If
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Function User_LoadUserDataCN(ByVal lCiaComp As Integer, ByVal ProveCodGS As String, ByVal IdUsu As Integer,
                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "CN_OBTENER_CONTACTO"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@PROVEGS", ProveCodGS)
                cm.Parameters.AddWithValue("@IDUSU", IdUsu)
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="ProveCod"></param>
        ''' <param name="IdCon"></param>
        ''' <param name="IdMensaje"></param>
        ''' <param name="Ocultar"></param>
        ''' <remarks></remarks>
        Public Sub CN_OcultarMensaje(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdCon As Integer,
                                    ByVal IdMensaje As Integer, ByVal Ocultar As Boolean,
                                    Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "CN_OCULTAR_MENSAJE_PROVE"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@PROVE", ProveCod)
                cm.Parameters.AddWithValue("@CON", IdCon)
                cm.Parameters.AddWithValue("@MEN", IdMensaje)
                cm.Parameters.AddWithValue("@OCULTAR", Ocultar)
                cm.ExecuteNonQuery()
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Sub
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="ProveCod"></param>
        ''' <param name="IdCon"></param>
        ''' <param name="IdMensaje"></param>
        ''' <param name="FechaAlta"></param>
        ''' <param name="Contenido"></param>
        ''' <param name="MeGusta"></param>
        ''' <param name="IdRespuesta"></param>
        ''' <param name="Adjuntos"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function CN_InsertarRespuesta(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdCon As Integer, ByVal IdMensaje As Integer,
                                            ByVal FechaAlta As DateTime, ByVal Contenido As String, ByVal MeGusta As Boolean,
                                            ByVal IdRespuesta As Nullable(Of Integer), ByRef Adjuntos As DataTable,
                                            Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Integer
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim da As New SqlDataAdapter
            cn.Open()
            Dim transaccion As SqlTransaction = cn.BeginTransaction()
            Try
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "CN_INSERTAR_RESPUESTA"
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion
                With cm.Parameters
                    .AddWithValue("@PROVE", ProveCod)
                    .AddWithValue("@CON", IdCon)
                    .AddWithValue("@IDMENSAJE", IdMensaje)
                    .AddWithValue("@FECHAALTA", FechaAlta)
                    .AddWithValue("@CONTENIDO", Contenido)
                    .AddWithValue("@MEGUSTA", MeGusta)
                    .AddWithValue("@IDRESPUESTA", IIf(IdRespuesta.HasValue, IdRespuesta, Nothing))
                    .Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
                End With
                cm.ExecuteNonQuery()

                Dim Id As Integer = cm.Parameters("@ID").Value

                If Not Adjuntos Is Nothing Then
                    cm = New SqlCommand(FSGS(lCiaComp) & "CN_ASOCIAR_MENSAJE_ADJUNTOS", cn)
                    cm.CommandType = CommandType.StoredProcedure
                    cm.CommandTimeout = 300
                    cm.Transaction = transaccion
                    With cm.Parameters
                        .AddWithValue("@RESP", Id)
                        .AddWithValue("@MEN", IdMensaje)
                        .Add("@ID", SqlDbType.Int, 0, "ID").Direction = ParameterDirection.Output
                        .Add("@NOMBRE", SqlDbType.NVarChar, 200, "NOMBRE")
                        .Add("@GUID", SqlDbType.NVarChar, 50, "GUID")
                        .Add("@SIZE", SqlDbType.Float, 0, "SIZE")
                        .Add("@SIZEUNIT", SqlDbType.NVarChar, 10, "SIZEUNIT")
                        .Add("@TIPOADJUNTO", SqlDbType.TinyInt, 0, "TIPOADJUNTO")
                    End With
                    da.InsertCommand = cm
                    da.Update(Adjuntos)
                End If

                transaccion.Commit()
                Dim nombreFichero As String
                For Each row As DataRow In Adjuntos.Rows
                    nombreFichero = row("URL")
                    If Not nombreFichero = String.Empty Then
                        Dim input As System.IO.FileStream = New System.IO.FileStream(nombreFichero, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                        Dim buffer(input.Length - 1) As Byte
                        input.Read(buffer, 0, buffer.Length)
                        input.Close()
                        CN_GrabarNuevoAdjun(lCiaComp, row("ID"), buffer)
                    End If
                Next
                Return Id
            Catch e As Exception
                Return 0
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        Public Sub CN_GrabarNuevoAdjun(ByVal lCiaComp As Integer, ByVal Id As Long, ByVal Buffer As Byte())
            Dim cn As SqlConnection
            Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext

            cn = NuevaConexion()
            mi_impersionationContext = RealizarSuplantacion()
            cn.Open()
            Dim tx As SqlTransaction = cn.BeginTransaction()
            Dim oSqlFileStream As SqlTypes.SqlFileStream = CN_GetSqlFileStreamAdjunto(lCiaComp, Id, String.Empty, cn, tx)
            If Not oSqlFileStream Is Nothing Then
                oSqlFileStream.Write(Buffer, 0, Buffer.Length)
                oSqlFileStream.Close()
            End If
            tx.Commit()
            cn.Close()
            DeshacerSuplantacion(mi_impersionationContext)
        End Sub
        Private Function CN_GetSqlFileStreamAdjunto(ByVal lCiaComp As Integer, ByVal Id As Long, ByVal DGuid As String, ByVal cn As SqlConnection, ByVal tx As SqlTransaction) As SqlTypes.SqlFileStream
            Dim sPath As String = Nothing
            Dim arTransactionContext() As Byte = Nothing
            CN_GetPathNameAndTxContextAdjunto(lCiaComp, Id, DGuid, cn, tx, sPath, arTransactionContext)
            '4 - open the filestream to the blob
            If Not sPath Is Nothing And Not arTransactionContext Is Nothing Then
                CN_GetSqlFileStreamAdjunto = New SqlTypes.SqlFileStream(sPath, arTransactionContext, FileAccess.ReadWrite)
            Else
                CN_GetSqlFileStreamAdjunto = Nothing
            End If
        End Function
        Private Sub CN_GetPathNameAndTxContextAdjunto(ByVal lCiaComp As Integer, ByVal Id As Long, ByVal DGuid As String, ByVal cn As SqlConnection, ByVal tx As SqlTransaction,
             ByRef Path As String, ByRef TxContext As Byte())
            Dim cm As New SqlCommand
            cm = New SqlCommand(FSGS(lCiaComp) & "CN_ADJUNTOS_LEER")
            cm.CommandType = CommandType.StoredProcedure
            With cm.Parameters
                If Not Id = 0 Then .AddWithValue("@ID", Id)
                If Not DGuid Is String.Empty Then .AddWithValue("@DGUID", DGuid)
            End With
            cm.Connection = cn
            cm.Transaction = tx
            Dim da As New SqlDataAdapter
            da.SelectCommand = cm
            Dim ds As New System.Data.DataSet
            da.Fill(ds)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim oRow As DataRow = ds.Tables(0).Rows(0)
                If Not oRow.IsNull("PATHNAME") Then Path = oRow("PATHNAME")
                If Not oRow.IsNull("TRANSACTIONCONTEXT") Then TxContext = CType(oRow("TRANSACTIONCONTEXT"), Byte()) 'reader.GetSqlBytes(1).Buffer;
                If Path Is Nothing Then
                    'Si se inserta un registro con la columna varbinary(max) a NULL no se crea archivo y FilePath devuelve nulo, con lo que no se puede
                    'crear el sqlFileStream.
                    'Insertando '' en la columna varbinary(max) se crea un registro de longitud cero y ya se puede obtener un handle para el archivo
                    Dim oCom As New SqlCommand
                    oCom.CommandType = CommandType.Text
                    oCom.CommandText = "UPDATE " & FSGS(lCiaComp) & "CN_ADJUN SET DATA=cast('' as varbinary(max)) WHERE ID=" & Id & ""
                    oCom.Connection = cn
                    oCom.Transaction = tx
                    oCom.ExecuteNonQuery()
                    Dim obj As Object = cm.ExecuteScalar()
                    If Not obj.Equals(DBNull.Value) Then Path = DirectCast(obj, String)
                End If
            End If
        End Sub
        Private Function CN_GetSqlFileStreamImagen(ByVal lCiaComp As Integer, ByVal DGuid As String, ByVal cn As SqlConnection, ByVal tx As SqlTransaction) As SqlTypes.SqlFileStream
            Dim sPath As String
            Dim arTransactionContext() As Byte
            CN_GetPathNameAndTxContextImagen(lCiaComp, DGuid, cn, tx, sPath, arTransactionContext)
            '4 - open the filestream to the blob
            If Not sPath Is Nothing And Not arTransactionContext Is Nothing Then
                CN_GetSqlFileStreamImagen = New SqlTypes.SqlFileStream(sPath, arTransactionContext, FileAccess.ReadWrite)
            Else
                CN_GetSqlFileStreamImagen = Nothing
            End If
        End Function
        Private Sub CN_GetPathNameAndTxContextImagen(ByVal lCiaComp As Integer, ByVal DGuid As String, ByVal cn As SqlConnection, ByVal tx As SqlTransaction,
            ByRef Path As String, ByRef TxContext As Byte())
            Dim cm As New SqlCommand
            cm = New SqlCommand(FSGS(lCiaComp) & "CN_USUARIOS_LEER")
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@DGUID", DGuid)
            cm.Connection = cn
            cm.Transaction = tx
            Dim da As New SqlDataAdapter
            da.SelectCommand = cm
            Dim ds As New System.Data.DataSet
            da.Fill(ds)

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim oRow As DataRow = ds.Tables(0).Rows(0)
                If Not oRow.IsNull("PATHNAME") Then Path = oRow("PATHNAME")
                If Not oRow.IsNull("TRANSACTIONCONTEXT") Then TxContext = CType(oRow("TRANSACTIONCONTEXT"), Byte()) 'reader.GetSqlBytes(1).Buffer;
                If Path Is Nothing Then
                    'Si se inserta un registro con la columna varbinary(max) a NULL no se crea archivo y FilePath devuelve nulo, con lo que no se puede
                    'crear el sqlFileStream.
                    'Insertando '' en la columna varbinary(max) se crea un registro de longitud cero y ya se puede obtener un handle para el archivo
                    Dim oCom As New SqlCommand
                    oCom.CommandType = CommandType.Text
                    oCom.CommandText = "UPDATE " & FSGS(lCiaComp) & "CN_USU_ADJUN SET DATA=cast('' as varbinary(max)) WHERE DGUID=" & DGuid & ""
                    oCom.Connection = cn
                    oCom.Transaction = tx
                    oCom.ExecuteNonQuery()
                    Dim obj As Object = cm.ExecuteScalar()
                    If Not obj.Equals(DBNull.Value) Then Path = DirectCast(obj, String)
                End If
            End If
        End Sub
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="IdMensaje"></param>
        ''' <param name="IdRespuesta"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function CN_InsertarMeGusta(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdCon As Integer,
                            ByVal Idi As String, ByVal IdMensaje As Integer, ByVal IdRespuesta As Integer,
                            Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm = New SqlCommand(FSGS(lCiaComp) & "CN_INSERTAR_MEGUSTA", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                With cm.Parameters
                    .AddWithValue("@PROVE", ProveCod)
                    .AddWithValue("@CON", IdCon)
                    .AddWithValue("@IDI", Idi)
                    .AddWithValue("@IDMENSAJE", IdMensaje)
                    If Not IdRespuesta = 0 Then
                        .AddWithValue("@IDRESPUESTA", IdRespuesta)
                    End If
                End With
                da.SelectCommand = cm
                da.Fill(dr)

                dr.Tables(0).TableName = "MENSAJES"
                dr.Tables(1).TableName = "RESPUESTAS"
                dr.Tables(2).TableName = "MEGUSTA"
                dr.Tables(3).TableName = "MEGUSTARESPUESTA"
                dr.Tables(4).TableName = "ADJUNTOS"

                Return dr
            Catch e As Exception
                Return Nothing
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Devuelve los datos del adjunto partiendo del identificador unico GUID
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function CN_Obtener_Nombre_Adjunto(ByVal lCiaComp As Integer, ByVal DGuid As String,
                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm = New SqlCommand(FSGS(lCiaComp) & "CN_OBTENER_NOMBRE_ADJUNTO", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Parameters.AddWithValue("@DGUID", DGuid)
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr.Tables(0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        Public Function CN_ObtenerResultadosBusqueda(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdCon As Integer, ByVal Idioma As String, ByVal Idioma_FTS As Integer,
                    ByVal TipoOrdenacion As Integer, ByVal TextoBusqueda As String,
                    Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "CN_BUSCADOR_PROVE"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@PROVE", ProveCod)
                cm.Parameters.AddWithValue("@CON", IdCon)
                cm.Parameters.AddWithValue("@IDI", Idioma)
                cm.Parameters.AddWithValue("@LCID", Idioma_FTS)
                cm.Parameters.AddWithValue("@SEARCH", TextoBusqueda)
                cm.Parameters.AddWithValue("@ORDERTYPE", TipoOrdenacion)
                da.SelectCommand = cm
                da.Fill(dr)

                dr.Tables(0).TableName = "MENSAJES"
                dr.Tables(1).TableName = "RESPUESTAS"
                dr.Tables(2).TableName = "MEGUSTA"
                dr.Tables(3).TableName = "MEGUSTARESPUESTA"
                dr.Tables(4).TableName = "ADJUNTOS"

                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Function CN_Comprobar_HayMensajesHistoricos(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdCon As Integer,
                            Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Boolean
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "CN_COMPROBAR_HAYMENSAJESHISTORICOS"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@PROVE", ProveCod)
                cm.Parameters.AddWithValue("@IDCON", IdCon)
                da.SelectCommand = cm
                da.Fill(dr)

                Return CType(dr.Tables(0).Rows(0).Item(0), Boolean)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Function CN_Comprobar_PermisoConsulta_NoConformidad(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal Instancia As Integer,
                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "CN_COMPROBAR_PERMISOCONSULTA_PORTAL_NC"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@PROVE", ProveCod)
                cm.Parameters.AddWithValue("@INSTANCIA", Instancia)
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr.Tables(0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Function CN_Comprobar_PermisoConsulta_Certificado(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal Instancia As Integer,
                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "CN_COMPROBAR_PERMISOCONSULTA_PORTAL_CERT"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@PROVE", ProveCod)
                cm.Parameters.AddWithValue("@INSTANCIA", Instancia)
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr.Tables(0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Function CN_Comprobar_PermisoConsulta_Factura(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal Contacto As Integer,
                ByVal IdFactura As Integer, ByVal Idioma As String,
                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_FACTURAS_PROVE"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@PROVE", ProveCod)
                cm.Parameters.AddWithValue("@ICONTACTO", Contacto)
                cm.Parameters.AddWithValue("@IDI", Idioma)
                cm.Parameters.AddWithValue("@FAC_RECTIFICATIVA", True)
                cm.Parameters.AddWithValue("@FAC_ORIGINAL", True)
                cm.Parameters.AddWithValue("@IDFACTURA", IdFactura)
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr.Tables(0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Function CN_Comprobar_PermisoConsulta_Solicitud(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal Idioma As String, ByVal Instancia As Integer,
                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "CN_COMPROBAR_PERMISOCONSULTA_PORTAL_SOLICITUD"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@PROVE", ProveCod)
                cm.Parameters.AddWithValue("@IDI", Idioma)
                cm.Parameters.AddWithValue("@INSTANCIA", Instancia)
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr.Tables(0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Guarda la imagen del usuario
        ''' </summary>
        ''' <param name="pathFoto">Directorio fisico de la imagen</param>
        ''' <param name="ProveCod">C�digo del proveedor</param>
        ''' <param name="IdContacto">Id del contacto</param>
        ''' <param name="DGuidImagen">Identificativo de la imagen</param>
        ''' <remarks></remarks>
        Public Sub CN_Actualizar_Imagen_Usuario(ByVal lCiaComp As Integer, ByVal pathFoto As String,
                ByVal ProveCod As String, ByVal IdContacto As Integer, ByRef DGuidImagen As String,
                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Authenticate(SesionId, IPDir, PersistID)

            Dim da As New SqlDataAdapter
            Dim dr As New DataSet
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Try
                If DGuidImagen Is String.Empty Then
                    cn.Open()
                    cm.Connection = cn
                    cm.CommandText = FSGS(lCiaComp) & "CN_UPDATE_IMAGEN_USU"
                    cm.CommandType = CommandType.StoredProcedure
                    cm.Parameters.Clear()
                    cm.Parameters.AddWithValue("@PROVE", ProveCod)
                    cm.Parameters.AddWithValue("@CON", IdContacto)
                    cm.Parameters.Add("@DGUID", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output
                    cm.ExecuteNonQuery()

                    DGuidImagen = cm.Parameters("@DGUID").Value
                End If
                Dim nombreFichero As String
                nombreFichero = pathFoto
                If Not nombreFichero = String.Empty Then
                    Dim input As System.IO.FileStream = New System.IO.FileStream(nombreFichero, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                    Dim buffer(input.Length - 1) As Byte
                    input.Read(buffer, 0, buffer.Length)
                    input.Close()
                    CN_GrabarImagen(lCiaComp, DGuidImagen, buffer)
                End If
            Catch e As Exception
                cm.ExecuteNonQuery()
                Throw e
            Finally
                If cn.State = ConnectionState.Open Then
                    cn.Dispose()
                    cm.Dispose()
                End If
            End Try
        End Sub
        Public Sub CN_GrabarImagen(ByVal lCiaComp As Integer, ByVal DGuidImagen As String, ByVal Buffer As Byte())
            Dim cn As SqlConnection
            Dim mi_impersionationContext As Security.Principal.WindowsImpersonationContext
            cn = NuevaConexion()
            mi_impersionationContext = RealizarSuplantacion()
            cn.Open()
            Dim tx As SqlTransaction = cn.BeginTransaction()
            Dim oSqlFileStream As SqlTypes.SqlFileStream = CN_GetSqlFileStreamImagen(lCiaComp, DGuidImagen, cn, tx)
            If Not oSqlFileStream Is Nothing Then
                oSqlFileStream.Write(Buffer, 0, Buffer.Length)
                oSqlFileStream.Close()
            End If
            tx.Commit()
            cn.Close()
            DeshacerSuplantacion(mi_impersionationContext)
        End Sub
        ''' <summary>
        ''' Guarda las opciones de CN del usuario
        ''' </summary>
        ''' <param name="bNotificarResumenActividad">Opci�n de usuario: Resumen de actividad</param>
        ''' <param name="bConfiguracionDesocultar">Volver a mostrar automaticamente los mensajes ocultos en los que haya actividad</param>
        ''' <remarks>Llamado desde: ConsultasCN.asmx.vb  ; Tiempo m�ximo: 0,1</remarks>
        ''' <revisado>JVS 08/02/2012</revisado>''' 
        Public Sub CN_Update_Opciones_Usuario(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdContacto As Integer,
                   ByVal bNotificarResumenActividad As Boolean, ByVal bConfiguracionDesocultar As Boolean,
                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Authenticate(SesionId, IPDir, PersistID)
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "CN_UPDATE_OPCIONES_USU"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.Clear()
                cm.Parameters.AddWithValue("@PROVE", ProveCod)
                cm.Parameters.AddWithValue("@CON", IdContacto)
                cm.Parameters.AddWithValue("@NOTIFRESACTI", bNotificarResumenActividad)
                cm.Parameters.AddWithValue("@CONFIGDESOCU", bConfiguracionDesocultar)
                cm.ExecuteNonQuery()
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Sub
#End Region
#Region "Contrato"
        ''' <summary>
        ''' Procedimeinto que carga un contrato y sus datos relacionados (Empresa, proveedor, moneda)
        ''' </summary>
        ''' <param name="sIdi">Idioma de la aplicacion</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <remarks>
        ''' Llamada desde: PmServer/Contrato/Load
        ''' Tiempo m�ximo: 0,3 seg</remarks>
        Public Function Contrato_Load(ByVal lCiaComp As Long, ByVal lID As Long, ByVal sIdi As String,
                                      Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GET_CONTRATO"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@ID", lID)
                cm.Parameters.AddWithValue("@IDIOMA", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Inserta los datos generales del la contrato.
        ''' </summary>
        ''' <param name="lIdSolicitud">Identificador de la solicitud</param>
        ''' <param name="sPeticionario">Codigo Persona</param>
        ''' <param name="sCodMoneda">Codigo Moneda</param>
        ''' <param name="lIdEmpresa">Id Empresa</param>
        ''' <param name="sCodProve">Codigo Proveedor</param>
        ''' <param name="lIdContacto">ID del contacto del proveedor</param>
        ''' <param name="dFechaInicio">Fecha inicio del contrato</param>
        ''' <param name="dFechaFin">fecha expiracion contrato</param>
        ''' <param name="lIdInstancia">ID instancia</param>
        ''' <param name="lIdContrato">ID del xcontrato</param>
        ''' <param name="sCodigoContrato">C�digo del contrato</param>
        ''' <param name="sNombreContrato">Nombre Contrato</param>
        ''' <param name="sProceso">Proceso</param>
        ''' <param name="sCadenaItems">Cadena con Items</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un objeto de tipo Error con el codigo de error que resulte(si es que ocurre)</returns>
        ''' <remarks>
        ''' Llamada desde: Contrato.vb --> Create_Prev
        ''' Tiempo m�ximo: </remarks>
        Public Function Contrato_Create_Prev(ByVal lIdSolicitud As Long, ByVal sPeticionario As String, ByVal sCodMoneda As String,
                                             ByVal lIdEmpresa As Long, ByVal sCodProve As String, ByVal lIdContacto As Long, ByVal dFechaInicio As Date,
                                             ByVal dFechaFin As Date, ByRef lIdInstancia As Long, ByRef lIdContrato As Long, ByRef sCodigoContrato As String,
                                             ByVal sNombreContrato As String, ByVal sProceso As String, ByVal sCadenaItems As String, Optional ByVal SesionId As String = "",
                                             Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Object
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim oFileW As System.IO.StreamWriter
            Dim dFechaIni As DateTime
            Dim sFile As String = "\PM_log_Contrato_"
            Dim iErrorVar As Long
            Dim sArrProceso() As String '// (0) -->anyo //  (1) GMN1_proce // (2) (proce)

            'Para el log
            dFechaIni = System.DateTime.Now
            cn.Open()
            Try
                cm = New SqlCommand("BEGIN TRANSACTION", cn)
                cm.ExecuteNonQuery()

                cm = New SqlCommand("FSPM_CREATE_CONTRATO_PREV", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 60
                cm.Parameters.AddWithValue("@SOLICITUD", lIdSolicitud)
                cm.Parameters.AddWithValue("@PETICIONARIO", sPeticionario)
                If sNombreContrato <> "" Then sNombreContrato = Replace(sNombreContrato, ".doc", "")
                cm.Parameters.AddWithValue("@NOMBRE", sNombreContrato)
                If sProceso <> "" Then
                    sArrProceso = Split(sProceso, "/")
                    cm.Parameters.AddWithValue("@ANYO", sArrProceso(0))
                    cm.Parameters.AddWithValue("@GMN1_PROCE", sArrProceso(1))
                    cm.Parameters.AddWithValue("@PROCE", sArrProceso(2))
                    cm.Parameters.AddWithValue("@ITEMS", sCadenaItems)
                End If
                cm.Parameters.AddWithValue("@EMP", lIdEmpresa)
                cm.Parameters.AddWithValue("@COD_PROVE", sCodProve)
                cm.Parameters.AddWithValue("@ID_CONTACTO", lIdContacto)
                cm.Parameters.AddWithValue("@MON", sCodMoneda)
                cm.Parameters.AddWithValue("@FEC_INI", dFechaInicio)
                cm.Parameters.AddWithValue("@FEC_FIN", IIf(dFechaFin = "#12:00:00 AM#", System.DBNull.Value, dFechaFin))
                cm.Parameters.AddWithValue("@ID", SqlDbType.Int).Direction = ParameterDirection.InputOutput
                cm.Parameters("@ID").Value = lIdInstancia
                cm.Parameters.AddWithValue("@CONTRATO", SqlDbType.Int).Direction = ParameterDirection.InputOutput
                cm.Parameters("@CONTRATO").Value = lIdContrato
                cm.Parameters.Add("@CODIGO_CONTRATO", SqlDbType.NVarChar, 50).Direction = ParameterDirection.InputOutput
                cm.Parameters("@CODIGO_CONTRATO").Value = IIf(IsNothing(sCodigoContrato), "", sCodigoContrato)
                cm.Parameters.AddWithValue("@ERRORVAR", SqlDbType.Int).Direction = ParameterDirection.InputOutput
                cm.Parameters("@ERRORVAR").Value = iErrorVar
                cm.ExecuteNonQuery()

                lIdInstancia = cm.Parameters("@ID").Value
                lIdContrato = cm.Parameters("@CONTRATO").Value
                sCodigoContrato = cm.Parameters("@CODIGO_CONTRATO").Value
                iErrorVar = cm.Parameters("@ERRORVAR").Value

                cm = New SqlCommand("COMMIT TRANSACTION", cn)
                cm.CommandTimeout = 60
                cm.ExecuteNonQuery()

                If iErrorVar <> 0 Then
                    oFileW = New System.IO.StreamWriter(ConfigurationSettings.AppSettings.Get("logtemp") & sFile & sPeticionario & ".txt", True)
                    oFileW.WriteLine()
                    oFileW.WriteLine("Error Contrato_Create_Prev: Error capturado1: " & iErrorVar)
                    oFileW.WriteLine("Hora inicio: " & Format(dFechaIni, "dd/MM/yyyy HH:mm:ss"))
                    oFileW.WriteLine("Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
                    oFileW.Close()
                    Return iErrorVar
                End If
            Catch e As Exception
                oFileW = New System.IO.StreamWriter(ConfigurationSettings.AppSettings.Get("logtemp") & sFile & sPeticionario & ".txt", True)
                oFileW.WriteLine()
                oFileW.WriteLine("Error Contrato_Create_Prev: Error capturado2: " & e.Message)
                oFileW.WriteLine("Hora inicio: " & Format(dFechaIni, "dd/MM/yyyy HH:mm:ss"))
                oFileW.WriteLine("Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
                oFileW.Close()
                cm = New SqlCommand("IF @@TRANCOUNT>0 ROLLBACK TRANSACTION", cn)
                cm.ExecuteNonQuery()
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return Nothing
        End Function
        ''' <summary>
        ''' Devuelve el ID de un contrato a partir del Id de la instancia
        ''' </summary>
        ''' <param name="lIdInstancia">ID Instancia</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>      
        ''' <returns>ID contrato</returns>
        ''' <remarks>Llamada desde:root.vb --> BuscarIdContrato; Tiempo m�ximo:0,1seg.</remarks>
        Public Function Contrato_BuscarIdContrato(ByVal lIdInstancia As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Long
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim result As Double
            Dim sConsulta As String
            Try
                cn.Open()
                cm.Connection = cn
                sConsulta = "SELECT TOP 1 C.ID FROM CONTRATO C WITH(NOLOCK) WHERE C.INSTANCIA=@INSTANCIA"
                cm.Parameters.AddWithValue("@INSTANCIA", lIdInstancia)
                cm.CommandText = sConsulta
                cm.CommandType = CommandType.Text
                result = CDbl(cm.ExecuteScalar())

                Return result
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
#End Region
#Region " Departamento data access methods "
        ''' <summary>
        ''' carga un departamento
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCod">Codido de departamento</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Departamento.vb\LoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function Departamento_Get(ByVal lCiaComp As Long, ByVal sCod As String,
                                         Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm = New SqlCommand
                cm.Connection = cn
                cm.CommandText = "SELECT COD,DEN,FECACT FROM " & FSGS(lCiaComp) & "DEP WITH (NOLOCK) WHERE COD = '" & sCod & "'"
                cm.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Departamentos data access methods "
        ''' <summary>
        ''' carga todos los departamentos
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="NivelACargar">De q nivel de departamento cargar</param>
        ''' <param name="UON1">Unidad organizativa de nivel 1</param>
        ''' <param name="UON2">Unidad organizativa de nivel 2</param>
        ''' <param name="UON3">Unidad organizativa de nivel 3</param>
        ''' <param name="sUsu">Usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Departamentos.vb\LoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function Departamentos_Load(ByVal lCiaComp As Long, Optional ByVal NivelACargar As String = Nothing,
                                           Optional ByVal UON1 As String = Nothing, Optional ByVal UON2 As String = Nothing, Optional ByVal UON3 As String = Nothing,
                                           Optional ByVal sUsu As String = Nothing,
                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GETDEPARTAMENTOS"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@NIVEL", NivelACargar)
                cm.Parameters.AddWithValue("@UON1", UON1)
                cm.Parameters.AddWithValue("@UON2", UON2)
                cm.Parameters.AddWithValue("@UON3", UON3)
                cm.Parameters.AddWithValue("@USU", sUsu)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Destinos data access methods "
        ''' <summary>
        ''' Carga los destinos
        ''' </summary>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sPer">Persona</param>
        ''' <param name="sCod">Codigo de destino</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Destinos.vb\LoadData     PMPortalNotificador\Notificar.vb\ObtenerDenominacionValorCampoGS; Tiempo m�ximo: 0,2</remarks>
        Public Function Destinos_Get(ByVal sIdi As String, ByVal lCiaComp As Long, ByVal sPer As String, Optional ByVal sCod As String = Nothing,
                                     Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSWS_DESTINOS"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@PER", sPer)
                cm.Parameters.AddWithValue("@COD", sCod)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region "DiagBloques & DiagEnlaces data access methods"
        ''' <summary>
        ''' Carga un diagrama bloque
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lID">Id de diagrama bloque</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\DiagBloques.vb\LoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function DiagBloques_Get(ByVal lCiaComp As Long, ByVal lID As Long, ByVal sIdi As String,
                                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_DEVOLVER_DIAGBLOQUES"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@INSTANCIA", lID)
                cm.Parameters.AddWithValue("@IDIOMA", sIdi)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Carga un diagrama enlace
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lID">Id de diagrama enlace</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\DiagEnlaces.vb\LoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function DiagEnlaces_Get(ByVal lCiaComp As Long, ByVal lID As Long, ByVal sIdi As String,
                                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_DEVOLVER_DIAGENLACES"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@BLOQUE", lID)
                cm.Parameters.AddWithValue("@IDIOMA", sIdi)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                dr.Relations.Add("REL_ENLACE_EXTRAPOINTS", dr.Tables(0).Columns("ID"), dr.Tables(1).Columns("ENLACE"), False)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try

            Return dr
        End Function
#End Region
#Region " Dictionaty data access methods "
        ''' <summary>
        ''' Carga los textos
        ''' </summary>
        ''' <param name="iModule">Modulo de textos</param>
        ''' <param name="sLanguage">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: TablaExterna_DescripcionReg          PMPortalServer\Dictionary.vb\LoadData     PMPortalServer\TablaExterna.vb\DescripcionReg      
        ''' PMPortalNotificador\Notificar.vb\HTMLTratamientoDesglose     PMPortalNotificador\Notificar.vb\TEXTOTratamientoDesglose       
        ''' PMPortalNotificador\Notificar.vb\ObtenerDenominacionValorCampoGS; Tiempo m�ximo: 0,2</remarks>
        Public Function Dictionary_GetData(ByVal iModule As Integer, ByVal sLanguage As String,
                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Data.DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GETDICTIONARYDATA"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@MODULEID", iModule)
                cm.Parameters.AddWithValue("@LANGUAGE", sLanguage)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region "Email"
        Public Function Load_Registro_Email(ByVal lCiaComp As Long, ByVal lIDMail As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSQA_GET_REGISTRO_EMAIL"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDMAIL", lIDMail)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region "Empresa"
        ''' <summary>
        ''' Procedimiento que carga Los datos de una empresa
        ''' </summary>
        ''' <param name="lCiaComp">ID de la compa�ia</param>
        ''' <param name="lID">ID empresa</param>
        ''' <param name="sIdi">Idioma del usuario</param>
        ''' <remarks>
        ''' Llamada desde:PmServer/Empresa/Load
        ''' Tiempo máximo: 0,2 seg
        ''' </remarks>
        Public Function Empresa_Load(ByVal lCiaComp As Long, ByVal lId As Long, ByVal sIdi As String,
                                     Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GET_EMPRESA"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@ID", lId)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
        End Function
#End Region
#Region "Empresas data access methods"
        ''' <summary>
        ''' Procedimiento que carga los datos de las empresas
        ''' </summary>
        ''' <param name="sIdi">Idioma de la aplicaci�n</param>
        ''' <param name="sNIF">NIF o parte del NIF que se pasa como criterio de b�squeda</param>
        ''' <param name="sDen">Nombre o parte del nombre de la empresa que se pasa como criterio de b�squeda</param>
        ''' <param name="sCP">CP o parte del CP que se pasa como criterio de b�squeda</param>
        ''' <param name="sDir">Direcci�n o parte de la direcci�n que se pasa como criterio de b�squeda</param>
        ''' <param name="sPob">Poblaci�n o parte de la poblaci�n que se pasa como criterio de b�squeda</param>
        ''' <param name="sPais">Pa�s o parte del pa�s que se pasa como criterio de b�squeda</param>
        ''' <param name="sProvincia">Provincia</param>
        ''' <remarks>Llamada desde: PMServer/Empresas/LoadData
        ''' Tiempo m�ximo: 0,2 seg </remarks>
        Public Function Empresas_Get(ByVal lCiaComp As Long, ByVal sIdi As String, Optional ByVal sNIF As String = Nothing, Optional ByVal sDen As String = Nothing,
                                     Optional ByVal sCP As String = Nothing, Optional ByVal sDir As String = Nothing, Optional ByVal sPob As String = Nothing,
                                     Optional ByVal sPais As String = Nothing, Optional ByVal sProvincia As String = Nothing, Optional lId As Long = Nothing, Optional ByVal AutoCompletar As String = Nothing,
                                     Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_GET_EMPRESAS_USUARIO"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDI", sIdi)
                If String.IsNullOrEmpty(sNIF) Then cm.Parameters.AddWithValue("@NIF", sNIF)
                If String.IsNullOrEmpty(sDen) Then cm.Parameters.AddWithValue("@DEN", sDen)
                If String.IsNullOrEmpty(sDir) Then cm.Parameters.AddWithValue("@DIR", sDir)
                If String.IsNullOrEmpty(sCP) Then cm.Parameters.AddWithValue("@CP", sCP)
                If String.IsNullOrEmpty(sPais) Then cm.Parameters.AddWithValue("@PAI", sPais)
                If String.IsNullOrEmpty(sProvincia) Then cm.Parameters.AddWithValue("@PROVI", sProvincia)
                cm.Parameters.AddWithValue("@ID", lId)
                If Not String.IsNullOrEmpty(AutoCompletar) Then cm.Parameters.AddWithValue("@PARAMETROAUTOCOMPLETAR", AutoCompletar)

                da.SelectCommand = cm
                Dim dt As New DataTable
                da.Fill(dt)

                Return dt
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region " Estados No Conformidad data access methods"
        ''' <summary>
        ''' carga los estados
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="sDen">Denom de estado</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\EstadosNoConf.vb\LoadData        PMPortalNotificador\Notificar.vb\ObtenerDenominacionValorCampoGS; Tiempo m�ximo: 0,2</remarks>
        Public Function EstadosNoConf_Get(ByVal lCiaComp As Long, ByVal lInstancia As Long, Optional ByVal sDen As String = Nothing, Optional ByVal sIdi As String = Nothing,
                                          Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_ESTADOS_NOCONF"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@INSTANCIA", lInstancia)
                cm.Parameters.AddWithValue("@DEN", sDen)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Estados Factura data access methods"
        ''' <summary>
        ''' Procedimiento que carga los Estados de la factura
        ''' </summary>
        ''' <param name="sIdi">Idioma de la aplicación</param>
        ''' <remarks>
        ''' Llamada desde: PmServer/Estadosfactura/Load
        ''' Tiempo máximo: 0,25 seg</remarks>
        Public Function EstadosFactura_Get(ByVal lCiaComp As Long, ByVal sIdi As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_ESTADOS_FACTURA"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDI", sIdi)
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region " Factura data access methods "
        ''' <summary>
        ''' Carga los descuentos de la factura
        ''' </summary>
        ''' <param name="lCiaComp"></param>
        ''' <param name="sCodProve"></param>
        ''' <param name="bTipo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Factura_LoadCostesDescuentosLinea(ByVal lCiaComp As Long, ByVal sCodProve As String, ByVal bTipo As Byte, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GET_LIN_FACTURAS_CD"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@PROVE", sCodProve)
                cm.Parameters.AddWithValue("@TIPO", bTipo)
                da.SelectCommand = cm
                da.Fill(ds)

                Return ds
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Carga los descuentos de la factura
        ''' </summary>
        ''' <param name="lCiaComp"></param>
        ''' <param name="sCodProve"></param>
        ''' <param name="bTipo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Facturas_LoadCostesDescuentos(ByVal lCiaComp As Long, ByVal sCodProve As String, ByVal bTipo As Byte, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GET_FACTURAS_CD"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@PROVE", sCodProve)
                cm.Parameters.AddWithValue("@TIPO", bTipo)
                da.SelectCommand = cm
                da.Fill(ds)

                Return ds
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Carga los impuestos de las facturas
        ''' </summary>
        ''' <param name="lCiaComp"></param>
        ''' <param name="sIdioma"></param>
        ''' <param name="sCod"></param>
        ''' <param name="sDesc"></param>
        ''' <param name="sPais"></param>
        ''' <param name="sProv"></param>
        ''' <param name="sCodArt"></param>
        ''' <param name="sCodMat"></param>
        ''' <param name="bRepercutido"></param>
        ''' <param name="bRetenido"></param>
        ''' <param name="iConceptoImpuesto">Concepto del impuesto</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Facturas_LoadImpuestosFacturas(ByVal lCiaComp As Long, ByVal sIdioma As String, ByVal sCod As String, ByVal sDesc As String, ByVal sPais As String, ByVal sProv As String, ByVal sCodArt As String, ByVal sCodMat As String, ByVal bRepercutido As Boolean, ByVal bRetenido As Boolean, ByVal iConceptoImpuesto As Nullable(Of FSNLibrary.TiposDeDatos.ConceptoImpuesto), Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GET_IMPUESTOS"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@IDI", sIdioma)
                cm.Parameters.AddWithValue("@COD", sCod)
                cm.Parameters.AddWithValue("@DESCR", sDesc)
                cm.Parameters.AddWithValue("@PAIS", sPais)
                cm.Parameters.AddWithValue("@PROV", sProv)
                cm.Parameters.AddWithValue("@ART", sCodArt)
                If sCodMat <> "" Then
                    Dim arrMat() As String = Split(sCodMat, " ")
                    Select Case arrMat.Length
                        Case 1
                            cm.Parameters.AddWithValue("@GMN1", arrMat(0))
                        Case 2
                            cm.Parameters.AddWithValue("@GMN1", arrMat(0))
                            cm.Parameters.AddWithValue("@GMN2", arrMat(1))
                        Case 3
                            cm.Parameters.AddWithValue("@GMN1", arrMat(0))
                            cm.Parameters.AddWithValue("@GMN2", arrMat(1))
                            cm.Parameters.AddWithValue("@GMN3", arrMat(2))
                        Case 4
                            cm.Parameters.AddWithValue("@GMN1", arrMat(0))
                            cm.Parameters.AddWithValue("@GMN2", arrMat(1))
                            cm.Parameters.AddWithValue("@GMN3", arrMat(2))
                            cm.Parameters.AddWithValue("@GMN4", arrMat(3))
                    End Select
                End If
                cm.Parameters.AddWithValue("@REPERCUTIDO", bRepercutido)
                cm.Parameters.AddWithValue("@RETENIDO", bRetenido)
                If iConceptoImpuesto Is Nothing Then
                    cm.Parameters.AddWithValue("@CONCEPTO", -1)
                Else
                    cm.Parameters.AddWithValue("@CONCEPTO", iConceptoImpuesto)
                End If
                da.SelectCommand = cm
                da.Fill(ds)

                Return ds
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Devuelve los distintos estados de una factura
        ''' </summary>
        ''' <param name="lCiaComp">id de la compa�ia</param>
        ''' <param name="sIdioma">idioma de la aplicacion</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Facturas_LoadEstados(ByVal lCiaComp As Long, ByVal sIdioma As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_ESTADOS_FACTURA"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@IDI", sIdioma)
                da.SelectCommand = cm
                da.Fill(ds)

                If ds.Tables.Count > 0 Then
                    Return ds.Tables(0)
                Else
                    Return Nothing
                End If
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Devuelve las facturas que puede ver un proveedor
        ''' </summary>
        ''' <param name="lCiaComp"></param>
        ''' <param name="sIdioma"></param>
        ''' <param name="sProve"></param>
        ''' <param name="Contacto"></param>
        ''' <param name="Empresa"></param>
        ''' <param name="NumFactura"></param>
        ''' <param name="FecFacturaDesde"></param>
        ''' <param name="FecFacturaHasta"></param>
        ''' <param name="FecContaDesde"></param>
        ''' <param name="FecContaHasta"></param>
        ''' <param name="ImporteDesde"></param>
        ''' <param name="ImporteHasta"></param>
        ''' <param name="CentroCoste"></param>
        ''' <param name="Partidas"></param>
        ''' <param name="Estado"></param>
        ''' <param name="A�oPedido"></param>
        ''' <param name="NumCesta"></param>
        ''' <param name="NumPedido"></param>
        ''' <param name="Articulo"></param>
        ''' <param name="Albaran"></param>
        ''' <param name="PedidoERP"></param>
        ''' <param name="NumFacturaSAP"></param>
        ''' <param name="Gestor"></param>
        ''' <param name="FacturaOriginal"></param>
        ''' <param name="FacturaRectificativa"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Facturas_Load(ByVal lCiaComp As Long, ByVal sIdioma As String, ByVal sProve As String, ByVal Contacto As Long, ByVal Empresa As String,
                                      ByVal NumFactura As String, ByVal FecFacturaDesde As Date, ByVal FecFacturaHasta As Date, ByVal FecContaDesde As Date,
                                      ByVal FecContaHasta As Date, ByVal ImporteDesde As Double, ByVal ImporteHasta As Double, ByVal CentroCoste As String,
                                      ByVal Partidas As DataTable, ByVal Estado As String, ByVal A�oPedido As String, ByVal NumCesta As Integer, ByVal NumPedido As Long,
                                      ByVal Articulo As String, ByVal Albaran As String, ByVal PedidoERP As String, ByVal NumFacturaSAP As String, ByVal Gestor As String,
                                      ByVal FacturaOriginal As Boolean, ByVal FacturaRectificativa As Boolean, Optional ByVal SesionId As String = "",
                                      Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_FACTURAS_PROVE"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDI", sIdioma)
                cm.Parameters.AddWithValue("@PROVE", sProve)
                cm.Parameters.AddWithValue("@ICONTACTO", Contacto)
                cm.Parameters.AddWithValue("@EMP", Empresa)
                cm.Parameters.AddWithValue("@NUM_FACTURA", NumFactura)
                If FecFacturaDesde <> Nothing Then cm.Parameters.AddWithValue("@FECHA_DESDE", FecFacturaDesde)
                If FecFacturaHasta <> Nothing Then cm.Parameters.AddWithValue("@FECHA_HASTA", FecFacturaHasta)
                cm.Parameters.AddWithValue("@ESTADO", Estado)
                If FecContaDesde <> Nothing Then cm.Parameters.AddWithValue("@FECHA_CONTA_DESDE", FecContaDesde)
                If FecContaHasta <> Nothing Then cm.Parameters.AddWithValue("@FECHA_CONTA_HASTA", FecContaHasta)
                cm.Parameters.AddWithValue("@PEDIDO_ERP", PedidoERP)
                cm.Parameters.AddWithValue("@FACTURA_ERP", NumFacturaSAP)
                cm.Parameters.AddWithValue("@IMPORTE_DESDE", ImporteDesde)
                cm.Parameters.AddWithValue("@IMPORTE_HASTA", ImporteHasta)
                cm.Parameters.AddWithValue("@ALBARAN", Albaran)
                cm.Parameters.AddWithValue("@ARTICULO", Articulo)
                cm.Parameters.AddWithValue("@CENTRO_COSTE", CentroCoste)
                cm.Parameters.AddWithValue("@GESTOR", Gestor)
                cm.Parameters.AddWithValue("@NUM_CESTA", NumCesta)
                cm.Parameters.AddWithValue("@NUM_PEDIDO", NumPedido)
                cm.Parameters.AddWithValue("@ANYO_PEDIDO", A�oPedido)
                If FacturaRectificativa Then cm.Parameters.AddWithValue("@FAC_RECTIFICATIVA", 1)
                If FacturaOriginal Then cm.Parameters.AddWithValue("@FAC_ORIGINAL", 1)
                Dim ParamPartidas As New SqlParameter("@PARTIDAS", SqlDbType.Structured)
                ParamPartidas.TypeName = "Tabla_Uons_Partidas"
                ParamPartidas.Value = Partidas
                cm.Parameters.Add(ParamPartidas)
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Procedimeinto que carga una factura y sus datos relacionados (Empresa, proveedor, ...)
        ''' </summary>
        ''' <param name="sIdi">Idioma de la aplicacion</param>
        ''' <remarks>
        ''' Llamada desde: FSNServer/PM/Factura/Load
        ''' Tiempo máximo: 0,3 seg</remarks>
        Public Function Factura_Load(ByVal lCiaComp As Long, ByVal lID As Long, ByVal sIdi As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GET_FACTURA"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@ID", lID)
                cm.Parameters.AddWithValue("@IDIOMA", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Devuelve los email enviados en esta factura
        ''' </summary>
        ''' <param name="lInstancia">Id de la instancia de la factura</param>
        ''' <returns></returns>
        ''' <remarks>Llamada desde detalleFactura</remarks>
        Public Function Factura_Load_Historico_Notificaciones(ByVal lCiaComp As Long, ByVal lInstancia As Long, ByVal sProve As String,
                                                              Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GET_HISTORICO_NOTIFICACIONES"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@INSTANCIA", lInstancia)
                cm.Parameters.AddWithValue("@PROVE", sProve)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Devuelve el motivo introducio de una factura anulada
        ''' </summary>
        ''' <param name="lID">Id de la factura</param>
        ''' <returns>El motivo de anulación</returns>
        ''' <remarks>Llamada desde: Factura.vb --> DevolverMotivoAnulacion; Tiempo: 0,01 sg</remarks>
        Function Factura_DevolverMotivoAnulacion(ByVal lCiaComp As Long, ByVal lID As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As String
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As SqlDataReader
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GET_MOTIVO_ANULACION"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@FACTURA", lID)
                dr = cm.ExecuteReader()

                If dr.Read() Then
                    Factura_DevolverMotivoAnulacion = dr.Item("MOTIVO_ANULACION")
                Else
                    Factura_DevolverMotivoAnulacion = Nothing
                End If
            Catch e As Exception
                Throw e
            Finally
                If Not dr Is Nothing AndAlso Not dr.IsClosed Then
                    dr.Close()
                End If
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Devuelve los atributos para el buscador de costes/descuentos que cumplen los criterios pasados como parámetros.
        ''' </summary>
        ''' <param name="iTipo">1: Coste; 2: Descuento</param>
        ''' <param name="sCod">Cód del atributo</param>
        ''' <param name="sDen">Denominación del atributo</param>
        ''' <returns>los atributos que cumplen los criterios pasados como parámetros.</returns>
        ''' <remarks>Llamada desde: Factura.vb --> Load_Atributos_BuscadorCostes</remarks>
        Public Function Factura_Load_Atributos_Buscador(ByVal lCiaComp As Long, ByVal iTipo As Short, ByVal sCod As String, ByVal sDen As String, ByVal sCodArt As String, ByVal sCodMat As String,
                                                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GET_ATRIBUTOS_BUSCADOR"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@TIPO", iTipo)
                If sCod <> "" Then cm.Parameters.AddWithValue("@COD", sCod)
                If sDen <> "" Then cm.Parameters.AddWithValue("@DEN", sDen)
                If sCodArt <> "" Then cm.Parameters.AddWithValue("@CODART", sCodArt)
                If sCodMat <> "" Then
                    Dim arrMat() As String
                    arrMat = Split(sCodMat, Space(1))
                    Dim i As Byte
                    Dim x As Byte = 1
                    For i = 0 To arrMat.Length - 1
                        If arrMat(i) <> String.Empty Then
                            Select Case x
                                Case 1
                                    cm.Parameters.AddWithValue("@GMN1", arrMat(i))
                                    x += 1
                                Case 2
                                    cm.Parameters.AddWithValue("@GMN2", arrMat(i))
                                    x += 1
                                Case 3
                                    cm.Parameters.AddWithValue("@GMN3", arrMat(i))
                                    x += 1
                                Case 4
                                    cm.Parameters.AddWithValue("@GMN4", arrMat(i))
                                    x += 1
                            End Select
                        End If
                    Next
                End If
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Function Factura_DevolverDetalleAlbaran(ByVal lCiaComp As Long, ByVal lFactura As Long, ByVal sAlbaran As String,
                                                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GET_DETALLE_ALBARAN"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@FACTURA", lFactura)
                cm.Parameters.AddWithValue("@ALBARAN", sAlbaran)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Funcion que lee el Filestream de la factura y devuelve el fichero
        ''' </summary>
        ''' <param name="lIdEFactura">id  de la efactura</param>
        ''' <returns>retorna buffer de bytes del adjunto</returns>
        ''' <remarks></remarks>
        Public Function Factura_LeerEFactura(ByVal lCiaComp As Long, ByVal lIdEFactura As Long) As Byte()
            Dim sBBDD As String = mDBName
            Dim sServidor As String = mDBServer
            Dim sCadena As String = "Integrated Security=true;server=" & sServidor & ";initial catalog=" & sBBDD
            Dim sqlConnection As New SqlConnection(sCadena)
            Dim sqlCommand As New SqlCommand()

            sqlCommand.Connection = sqlConnection
            Try
                Dim BDGS As String = FSGS(lCiaComp)

                sqlConnection.Open()
                'Retrieve the file path of the SQL FILESTREAM BLOB in the first row. 
                sqlCommand.CommandText = "SELECT DATA.PathName() FROM " & BDGS & "FACTURA_E FE WITH (NOLOCK) WHERE FE.ID=" & lIdEFactura

                Dim filePath As String = Nothing
                Dim pathObj As Object = sqlCommand.ExecuteScalar()
                If Not pathObj.Equals(DBNull.Value) Then
                    filePath = DirectCast(pathObj, String)
                Else
                    Throw New System.Exception("DATA.PathName() FACTURA_E failed to read the path name for the DATA column.")
                End If

                'Obtain a transaction context. All FILESTREAM BLOB operations occur within a transaction context to maintain data consistency. 
                Dim transaction As SqlTransaction = sqlConnection.BeginTransaction("mainTranaction")
                sqlCommand.Transaction = transaction
                sqlCommand.CommandText = "SELECT GET_FILESTREAM_TRANSACTION_CONTEXT()"

                Dim txContext As Byte() = Nothing
                Dim obj As Object = sqlCommand.ExecuteScalar()

                If Not obj.Equals(DBNull.Value) Then
                    txContext = DirectCast(obj, Byte())
                Else
                    Throw New System.Exception("GET_FILESTREAM_TRANSACTION_CONTEXT() failed")
                End If

                Dim sqlFileStream As New SqlTypes.SqlFileStream(filePath, txContext, FileAccess.Read)
                Dim buffer As Byte() = New Byte(sqlFileStream.Length - 1) {}

                sqlFileStream.Read(buffer, 0, buffer.Length)
                ' Close the FILESTREAM handle. 
                sqlFileStream.Close()
                sqlCommand.Transaction.Commit()

                Factura_LeerEFactura = buffer
            Catch e As Exception
                Throw e
            Finally
                sqlConnection.Dispose()
                sqlCommand.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function CN_Obtener_Datos_NuevaDiscrepancia_Proveedor(ByVal Factura As Integer, ByVal Linea As Integer,
                                                ByVal lCiaComp As Integer, ByVal Idioma As String,
                                                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.CommandText = FSGS(lCiaComp) & "CN_OBTENER_DATOS_NUEVADISCREPANCIA_PROVEEDOR"
                cm.CommandType = CommandType.StoredProcedure
                cm.Connection = cn
                cm.Parameters.AddWithValue("@FACTURA", Factura)
                cm.Parameters.AddWithValue("@LINEA", Linea)
                cm.Parameters.AddWithValue("@IDIOMA", Idioma)
                da.SelectCommand = cm
                da.Fill(dr)

                dr.Tables(0).TableName = "CATEGORIA"
                dr.Tables(1).TableName = "GESTOR"
                dr.Tables(2).TableName = "RECEPTOR"
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Sub CN_InsertarDiscrepancia(ByVal CiaComp As Integer, ByVal Prove As String, ByVal IdCon As Integer,
                        ByVal tipo As Integer, ByVal FechaAlta As DateTime, ByVal Titulo As String, ByVal Contenido As String,
                        ByVal Fecha As Nullable(Of DateTime), ByVal Donde As String, ByVal Categoria As Integer,
                        ByVal UON0 As Boolean, ByVal Para_UON As DataTable, ByVal Para_Prove_Tipo As Integer, ByVal Para_Prove_Con As DataTable,
                        ByVal GMN1 As String, ByVal GMN2 As String, ByVal GMN3 As String, ByVal GMN4 As String, ByVal MatQA As Nullable(Of Integer),
                        ByVal ProceCompra_Anyo As Nullable(Of Integer), ByVal ProceCompra_GMN1 As String, ByVal ProceCompra_Codigo As Nullable(Of Integer),
                        ByVal ProceParaResp As Boolean, ByVal ProceParaInvitado As Boolean,
                        ByVal ProceParaCompradores As Boolean, ByVal ProceParaProve As Boolean,
                        ByVal Para_EstrucCompras As DataTable, ByVal Para_EstrucCompras_MatGS As Boolean,
                        ByVal Para_Grupo_EP As Boolean, ByVal Para_Grupo_GS As Boolean, ByVal Para_Grupo_PM As Boolean,
                        ByVal Para_Grupo_QA As Boolean, ByVal Para_Grupo_SM As Boolean, ByVal Para_Grupos_Grupos As DataTable,
                        ByRef Adjuntos As DataTable, ByVal Factura As Integer, ByVal Linea As Integer,
                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter

            cn.Open()
            Dim transaccion As SqlTransaction = cn.BeginTransaction()
            Try
                cm.Connection = cn
                cm.CommandText = FSGS(CiaComp) & "CN_INSERTAR_MENSAJE"
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion
                With cm.Parameters
                    .AddWithValue("@PROVE", Prove)
                    .AddWithValue("@IDCON", IdCon)
                    .AddWithValue("@TIPO", tipo)
                    .AddWithValue("@FECALTA", FechaAlta)
                    .AddWithValue("@TITULO", Titulo)
                    .AddWithValue("@CONTENIDO", Contenido)
                    .AddWithValue("@CUANDO", IIf(Fecha.HasValue, Fecha, Nothing))
                    .AddWithValue("@DONDE", Donde)
                    .AddWithValue("@CATEGORIA", Categoria)
                    .AddWithValue("@PROCE_ANYO", IIf(ProceCompra_Anyo.HasValue, ProceCompra_Anyo, Nothing))
                    .AddWithValue("@PROCE_GMN1", ProceCompra_GMN1)
                    .AddWithValue("@PROCE_COD", IIf(ProceCompra_Codigo.HasValue, ProceCompra_Codigo, Nothing))
                    .AddWithValue("@PROCEPARARESP", ProceParaResp)
                    .AddWithValue("@PROCEPARAINVITADO", ProceParaInvitado)
                    .AddWithValue("@PROCEPARACOMP", ProceParaCompradores)
                    .AddWithValue("@PROCEPARAPROVE", ProceParaProve)
                    .AddWithValue("@UON0", UON0)
                    .AddWithValue("@GMN1", GMN1)
                    .AddWithValue("@GMN2", GMN2)
                    .AddWithValue("@GMN3", GMN3)
                    .AddWithValue("@GMN4", GMN4)
                    .AddWithValue("@MATQA", MatQA)
                    .AddWithValue("@PROVEPARA", Para_Prove_Tipo)
                    .AddWithValue("@COMPPARA", Para_EstrucCompras_MatGS)
                    .AddWithValue("@USUEPPARA", Para_Grupo_EP)
                    .AddWithValue("@USUGSPARA", Para_Grupo_GS)
                    .AddWithValue("@USUPMPARA", Para_Grupo_PM)
                    .AddWithValue("@USUQAPARA", Para_Grupo_QA)
                    .AddWithValue("@USUSMPARA", Para_Grupo_SM)
                    .Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
                End With
                cm.ExecuteNonQuery()

                Dim Id As Integer = cm.Parameters("@ID").Value
                If Not Para_UON Is Nothing Then
                    cm = New SqlCommand(FSGS(CiaComp) & "CN_ASOCIAR_MENSAJE_USU", cn)
                    cm.CommandType = CommandType.StoredProcedure
                    cm.CommandTimeout = 300
                    cm.Transaction = transaccion
                    With cm.Parameters
                        .AddWithValue("@MEN", Id)
                        .Add("@UON1", SqlDbType.NVarChar, 20, "UON1")
                        .Add("@UON2", SqlDbType.NVarChar, 20, "UON2")
                        .Add("@UON3", SqlDbType.NVarChar, 20, "UON3")
                        .Add("@DEP", SqlDbType.NVarChar, 20, "DEP")
                        .Add("@USU", SqlDbType.NVarChar, 20, "USU")
                    End With
                    da.InsertCommand = cm
                    da.Update(Para_UON)
                End If

                If Not Adjuntos Is Nothing Then
                    cm = New SqlCommand(FSGS(CiaComp) & "CN_ASOCIAR_MENSAJE_ADJUNTOS", cn)
                    cm.CommandType = CommandType.StoredProcedure
                    cm.CommandTimeout = 300
                    cm.Transaction = transaccion
                    With cm.Parameters
                        .AddWithValue("@MEN", Id)
                        .Add("@ID", SqlDbType.Int, 0, "ID").Direction = ParameterDirection.Output
                        .Add("@NOMBRE", SqlDbType.NVarChar, 200, "NOMBRE")
                        .Add("@GUID", SqlDbType.NVarChar, 50, "GUID")
                        .Add("@SIZE", SqlDbType.Float, 0, "SIZE")
                        .Add("@SIZEUNIT", SqlDbType.NVarChar, 10, "SIZEUNIT")
                        .Add("@TIPO", SqlDbType.TinyInt, 0, "TIPOADJUNTO")
                        .Add("@URL", SqlDbType.NVarChar, -1, "URL")
                    End With
                    da.InsertCommand = cm
                    da.Update(Adjuntos)
                End If

                cm = New SqlCommand(FSGS(CiaComp) & "CN_INSERTAR_DISCREPANCIA", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 300
                cm.Transaction = transaccion
                With cm.Parameters
                    .AddWithValue("@IDMENSAJE", Id)
                    .AddWithValue("@FACTURA", Factura)
                    .AddWithValue("@LINEA", Linea)
                End With
                cm.ExecuteNonQuery()

                transaccion.Commit()
                Dim nombreFichero As String
                For Each row As DataRow In Adjuntos.Rows
                    nombreFichero = row("URL")
                    If Not nombreFichero = String.Empty Then
                        Dim input As System.IO.FileStream = New System.IO.FileStream(nombreFichero, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                        Dim buffer(input.Length - 1) As Byte
                        input.Read(buffer, 0, buffer.Length)
                        input.Close()
                        CN_GrabarNuevoAdjun(CiaComp, row("ID"), buffer)
                    End If
                Next
            Catch e As Exception
                transaccion.Rollback()
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Sub
        ''' <summary>
        ''' Obtener l�neas de factura
        ''' </summary>
        ''' <param name="Idioma">Idioma</param>
        ''' <param name="Factura">ID Factura</param>
        ''' <returns>l��neas de factura</returns>
        ''' <remarks></remarks>
        ''' <revisado>JVS 22/06/2012</revisado>
        Public Function CN_Obtener_Lineas_Factura(ByVal CiaComp As Integer, ByVal Idioma As String, ByVal Factura As Integer, ByVal Linea As Integer,
                                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(CiaComp) & "CN_OBTENER_LINEAS_FACTURA_DISCRP"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@FACTURA", Factura)
                If Not Linea = 0 Then cm.Parameters.AddWithValue("@LINEA", Linea)
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr.Tables(0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Sub CN_Cerrar_Discrepancia(ByVal CiaComp As Integer, ByVal Factura As Integer, ByVal Linea As Integer, ByVal IdDiscrepancia As Integer,
                                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(CiaComp) & "CN_CERRAR_DISCREPANCIAS"
                cm.CommandType = CommandType.StoredProcedure
                If Not Factura = 0 Then cm.Parameters.AddWithValue("@FACTURA", Factura)
                If Not Linea = 0 Then cm.Parameters.AddWithValue("@LINEA", Linea)
                If Not IdDiscrepancia = 0 Then cm.Parameters.AddWithValue("@IDDISCREPANCIA", IdDiscrepancia)
                cm.ExecuteNonQuery()
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Sub
        Public Function CN_Comprobar_DiscrepanciasCerradas(ByVal CiaComp As Integer, ByVal IdFactura As Integer, ByVal Linea As Integer,
                                            Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Integer
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(CiaComp) & "CN_COMPROBAR_DISCREPANCIASCERRADAS"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDFACTURA", IdFactura)
                If Not Linea = 0 Then cm.Parameters.AddWithValue("@LINEA", Linea)
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr.Tables(0).Rows.Count
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
#End Region
#Region " FormasPago data access methods "
        ''' <summary>
        ''' Cargar las formas
        ''' </summary>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCod">Codigo de forma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\FormasPago.vb\LoadData         PMPortalNotificador\Notificar.vb/ObtenerDenominacionValorCampoGS; Tiempo m�ximo: 0,2</remarks>
        Public Function FormasPago_Get(ByVal sIdi As String, ByVal lCiaComp As Long, Optional ByVal sCod As String = Nothing,
                                       Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_FORMASPAGO"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                If Not String.IsNullOrEmpty(sCod) Then cm.Parameters.AddWithValue("@COD", sCod)
                cm.Parameters.AddWithValue("@IDIOMA", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Formulario data access methods "
        ''' <summary>
        ''' carga los campos de un formulario
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lID">Id de formulario</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lSolicitud">Solicitud</param>
        ''' <param name="lBloque">Bloque</param>
        ''' <param name="sPer">Persona</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Formulario.vb\Load ; Tiempo m�ximo: 0,2</remarks>
        Public Function Formulario_Load(ByVal lCiaComp As Long, ByVal lID As Long, Optional ByVal sIdi As String = Nothing, Optional ByVal lSolicitud As Long = Nothing,
                                        Optional ByVal lBloque As Long = Nothing, Optional ByVal sPer As String = Nothing, Optional ByVal idRol As Integer = Nothing,
                                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.StoredProcedure
                If idRol <> Nothing Then
                    cm.CommandText = FSGS(lCiaComp) & "FSPM_GET_FORMULARIO"
                    cm.Parameters.AddWithValue("@BLOQUE", lBloque)
                    cm.Parameters.AddWithValue("@IDROL", idRol)
                Else
                    cm.CommandText = "FSPM_GETFORM"
                    cm.Parameters.AddWithValue("@CIA", lCiaComp)
                    cm.Parameters.AddWithValue("@SOLICPROVE", 2)
                End If

                cm.Parameters.AddWithValue("@ID", lID)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@SOLICITUD", lSolicitud)

                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                dr.Relations.Add("REL_FORM_GRUPO", dr.Tables(0).Columns("ID"), dr.Tables(1).Columns("FORMULARIO"), False)
                dr.Relations.Add("REL_GRUPO_CAMPO", dr.Tables(1).Columns("ID"), dr.Tables(2).Columns("GRUPO"), False)
                dr.Relations.Add("REL_GRUPO_LISTA", dr.Tables(1).Columns("ID"), dr.Tables(3).Columns("GRUPO"), False)
                dr.Relations.Add("REL_GRUPO_ADJUNTO", dr.Tables(1).Columns("ID"), dr.Tables(4).Columns("GRUPO"), False)

                If Not idRol = Nothing Then
                    dr.Relations.Add("REL_CAMPO_BLOQUEO", dr.Tables(1).Columns("ID"), dr.Tables(5).Columns("GRUPO"), False)
                End If
            Catch e As Exception
                Throw e
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' carga los campos calculados
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lIdSolicitud">Solicitud</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Formulario.vb\LoadCamposCalculados ; Tiempo m�ximo: 0,2</remarks>
        Public Function Formulario_LoadCamposCalculados(ByVal lCiaComp As Long, ByVal lId As Long, ByVal lIdSolicitud As Long, Optional ByVal bNuevoWorkfl As Boolean = False, Optional ByVal sProve As String = Nothing, Optional ByVal idUsu As Integer = Nothing,
                                                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.StoredProcedure
                If bNuevoWorkfl = True Then
                    cm.CommandText = FSGS(lCiaComp) & "FSPM_LOADFIELDSCALC"
                    cm.Parameters.AddWithValue("@FORMULARIO", lId)
                    If Not String.IsNullOrEmpty(sProve) Then cm.Parameters.AddWithValue("@PROVE", sProve)
                    If Not String.IsNullOrEmpty(idUsu) Then cm.Parameters.AddWithValue("@CONTACTO", idUsu)
                Else
                    cm.CommandText = FSGS(lCiaComp) & "FSQA_LOADFIELDSCALC"
                    cm.Parameters.AddWithValue("@SOLIC_PROVE", 2)
                End If
                cm.Parameters.AddWithValue("@SOLICITUD", lIdSolicitud)

                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " GruposMaterial data access methods "
        ''' <summary>
        ''' Sobrecarga de la funci�n GruposMaterial_Get que devuelve los C�digos de Material GMN1, GMN2, GMN3, GMN4 del proveedor
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sGmn1">Nivel 1 de la familia de materiales</param>
        ''' <param name="sGmn2">Nivel 2 de la familia de materiales</param>
        ''' <param name="sGmn3">Nivel 3 de la familia de materiales</param>
        ''' <param name="sGmn4">Nivel 4 de la familia de materiales</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="bSelCualquierMatPortal">Seleccionar cualquier material portal</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\GrupoMatNivel1.vb\LoadData  PMPortalServer\GrupoMatNivel2.vb\LoadData  PMPortalServer\GrupoMatNivel3.vb\LoadData 
        '''  PMPortalServer\GrupoMatNivel4.vb\LoadData   PMPortalServer\GruposMaterial.vb\LoadData; Tiempo m�ximo: 0,2</remarks>
        Public Function GruposMaterial_Get(ByVal lCiaComp As Long, Optional ByVal sGMN1 As String = Nothing, Optional ByVal sGMN2 As String = Nothing,
                                           Optional ByVal sGMN3 As String = Nothing, Optional ByVal sGMN4 As String = Nothing, Optional ByVal sIdi As String = "SPA",
                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "",
                                           Optional ByVal bSelCualquierMatPortal As Boolean = False) As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try

                cn.Open()
                cm.Connection = cn
                If sGMN1 = "" Then sGMN1 = Nothing
                If sGMN2 = "" Then sGMN2 = Nothing
                If sGMN3 = "" Then sGMN3 = Nothing
                If sGMN4 = "" Then sGMN4 = Nothing

                cm.CommandText = "FSPM_GMNS"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@GMN1", sGMN1)
                cm.Parameters.AddWithValue("@GMN2", sGMN2)
                cm.Parameters.AddWithValue("@GMN3", sGMN3)
                cm.Parameters.AddWithValue("@GMN4", sGMN4)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                Dim parentCols(0) As DataColumn
                Dim childCols(0) As DataColumn
                parentCols(0) = dr.Tables(0).Columns("COD")
                childCols(0) = dr.Tables(1).Columns("GMN1")

                dr.Relations.Add("REL_NIV1_NIV2", parentCols, childCols, False)

                ReDim parentCols(1)
                ReDim childCols(1)
                parentCols(0) = dr.Tables(1).Columns("GMN1")
                parentCols(1) = dr.Tables(1).Columns("COD")
                childCols(0) = dr.Tables(2).Columns("GMN1")
                childCols(1) = dr.Tables(2).Columns("GMN2")
                dr.Relations.Add("REL_NIV2_NIV3", parentCols, childCols, False)

                ReDim parentCols(2)
                ReDim childCols(2)
                parentCols(0) = dr.Tables(2).Columns("GMN1")
                parentCols(1) = dr.Tables(2).Columns("GMN2")
                parentCols(2) = dr.Tables(2).Columns("COD")
                childCols(0) = dr.Tables(3).Columns("GMN1")
                childCols(1) = dr.Tables(3).Columns("GMN2")
                childCols(2) = dr.Tables(3).Columns("GMN3")

                dr.Relations.Add("REL_NIV3_NIV4", parentCols, childCols, False)
            Catch e As Exception
                Throw e
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
            Return dr
        End Function
        ''' Revisado por: blp. Fecha: 24/01/2012
        ''' <summary>
        ''' Sobrecarga de la funci�n GruposMaterial_Get que devuelve los C�digos de Material GMN1, GMN2, GMN3, GMN4 del proveedor
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la empresa</param>
        ''' <param name="lProveID">C�digo del proveedor</param>
        ''' <param name="sGMN1">Codigo de material, nivel 1</param>
        ''' <param name="sGMN2">Codigo de material, nivel 2</param>
        ''' <param name="sGMN3">Codigo de material, nivel 3</param>
        ''' <param name="sGMN4">Codigo de material, nivel 4</param>
        ''' <param name="sIdi">Idioma del usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="bSelCualquierMatPortal">Seleccionar cualquier material portal</param>
        ''' <returns>Dataset con 4 tablas con los c�digos de material</returns>
        ''' <remarks>Llamada desde GruposMaterial.vb. M�x. 1 seg.</remarks>
        Public Function GruposMaterial_Get(ByVal lCiaComp As Long, ByVal lProveID As Long, Optional ByVal sGMN1 As String = Nothing, Optional ByVal sGMN2 As String = Nothing,
                                           Optional ByVal sGMN3 As String = Nothing, Optional ByVal sGMN4 As String = Nothing, Optional ByVal sIdi As String = "SPA",
                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "",
                                           Optional ByVal bSelCualquierMatPortal As Boolean = False, Optional ByVal iNivelSeleccion As Integer = 0) As DataSet

            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try

                cn.Open()
                cm.Connection = cn
                If sGMN1 = "" Then sGMN1 = Nothing
                If sGMN2 = "" Then sGMN2 = Nothing
                If sGMN3 = "" Then sGMN3 = Nothing
                If sGMN4 = "" Then sGMN4 = Nothing

                If bSelCualquierMatPortal Then
                    cm.CommandText = "FSPM_GMNS_PROVE_PORTAL"
                Else
                    cm.CommandText = "FSPM_GMNS_PROVE"
                    cm.Parameters.AddWithValue("@CIA", lCiaComp)
                    cm.Parameters.AddWithValue("@PROVE", lProveID)
                End If
                cm.Parameters.AddWithValue("@NIVEL_SELECCION", iNivelSeleccion)

                cm.Parameters.AddWithValue("@GMN1", sGMN1)
                cm.Parameters.AddWithValue("@GMN2", sGMN2)
                cm.Parameters.AddWithValue("@GMN3", sGMN3)
                cm.Parameters.AddWithValue("@GMN4", sGMN4)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                Dim parentCols(0) As DataColumn
                Dim childCols(0) As DataColumn
                If (iNivelSeleccion >= 2) Or iNivelSeleccion = 0 Then


                    parentCols(0) = dr.Tables(0).Columns("COD")
                    childCols(0) = dr.Tables(1).Columns("GMN1")

                    dr.Relations.Add("REL_NIV1_NIV2", parentCols, childCols, False)

                    ReDim parentCols(1)
                    ReDim childCols(1)
                    If (iNivelSeleccion >= 3) Or iNivelSeleccion = 0 Then


                        parentCols(0) = dr.Tables(1).Columns("GMN1")
                        parentCols(1) = dr.Tables(1).Columns("COD")
                        childCols(0) = dr.Tables(2).Columns("GMN1")
                        childCols(1) = dr.Tables(2).Columns("GMN2")
                        dr.Relations.Add("REL_NIV2_NIV3", parentCols, childCols, False)

                        ReDim parentCols(2)
                        ReDim childCols(2)

                        If (iNivelSeleccion >= 4) Or iNivelSeleccion = 0 Then
                            parentCols(0) = dr.Tables(2).Columns("GMN1")
                            parentCols(1) = dr.Tables(2).Columns("GMN2")
                            parentCols(2) = dr.Tables(2).Columns("COD")
                            childCols(0) = dr.Tables(3).Columns("GMN1")
                            childCols(1) = dr.Tables(3).Columns("GMN2")
                            childCols(2) = dr.Tables(3).Columns("GMN3")


                            dr.Relations.Add("REL_NIV3_NIV4", parentCols, childCols, False)
                        End If
                    End If
                End If
            Catch e As Exception
                Throw e
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
            Return dr

        End Function
#End Region
#Region " GrupoMatNivel3 data access methods "
        ''' <summary>
        '''  Carga de materiales
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iNumMaximo">Numero maximo de registros a cargar</param>
        ''' <param name="sGmn1">Nivel 1 de la familia de materiales</param>
        ''' <param name="sGmn2">Nivel 2 de la familia de materiales</param>
        ''' <param name="sGmn3">Nivel 3 de la familia de materiales</param>
        ''' <param name="sCod">Codigo a cargar</param>
        ''' <param name="sDen">Denominacion a cargar</param>
        ''' <param name="bCoincid">Si se hace like o se hace =</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="bOrdenadosPorDen">Si se ordena por den o no</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\GrupoMatNivel3.vb\CargarTodosLosGruposMatDesde ; Tiempo m�ximo: 0,2</remarks>
        Public Function GrupoMatNivel3_CargarTodosLosGruposMatDesde(ByVal lCiaComp As Long, ByVal iNumMaximo As Integer, Optional ByVal sGMN1 As String = Nothing, Optional ByVal sGMN2 As String = Nothing,
                                                                    Optional ByVal sGMN3 As String = Nothing, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing,
                                                                    Optional ByVal bCoincid As Boolean = False, Optional ByVal sIdi As String = "SPA", Optional ByVal bOrdenadosPorDen As Boolean = False,
                                                                    Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GMN4"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@NUMMAX", iNumMaximo)
                cm.Parameters.AddWithValue("@GMN1", sGMN1)
                cm.Parameters.AddWithValue("@GMN2", sGMN2)
                cm.Parameters.AddWithValue("@GMN3", sGMN3)
                cm.Parameters.AddWithValue("@COD", sCod)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@COINCID", IIf(bCoincid, 1, 0))
                cm.Parameters.AddWithValue("@ORDENPORDEN", IIf(bOrdenadosPorDen, 1, 0))
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " GrupoMatNivel2 data access methods "
        ''' <summary>
        '''  Carga de materiales
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iNumMaximo">Numero maximo de registros a cargar</param>
        ''' <param name="sGmn1">Nivel 1 de la familia de materiales</param>
        ''' <param name="sGmn2">Nivel 2 de la familia de materiales</param>
        ''' <param name="sCod">Codigo a cargar</param>
        ''' <param name="sDen">Denominacion a cargar</param>
        ''' <param name="bCoincid">Si se hace like o se hace =</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="bOrdenadosPorDen">Si se ordena por den o no</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\GrupoMatNivel2.vb\CargarTodosLosGruposMatDesde; Tiempo m�ximo: 0,2</remarks>
        Public Function GrupoMatNivel2_CargarTodosLosGruposMatDesde(ByVal lCiaComp As Long, ByVal iNumMaximo As Integer, Optional ByVal sGMN1 As String = Nothing, Optional ByVal sGMN2 As String = Nothing,
                                                                    Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal bCoincid As Boolean = False,
                                                                    Optional ByVal sIdi As String = "SPA", Optional ByVal bOrdenadosPorDen As Boolean = False,
                                                                    Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GMN3"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@NUMMAX", iNumMaximo)
                cm.Parameters.AddWithValue("@GMN1", sGMN1)
                cm.Parameters.AddWithValue("@GMN2", sGMN2)
                cm.Parameters.AddWithValue("@COD", sCod)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@COINCID", IIf(bCoincid, 1, 0))
                cm.Parameters.AddWithValue("@ORDENPORDEN", IIf(bOrdenadosPorDen, 1, 0))
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " GrupoMatNivel1 data access methods "
        ''' <summary>
        '''  Carga de materiales
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iNumMaximo">Numero maximo de registros a cargar</param>
        ''' <param name="sGmn1">Nivel 1 de la familia de materiales</param>
        ''' <param name="sCod">Codigo a cargar</param>
        ''' <param name="sDen">Denominacion a cargar</param>
        ''' <param name="bCoincid">Si se hace like o se hace =</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="bOrdenadosPorDen">Si se ordena por den o no</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\GrupoMatNivel1.vb\CargarTodosLosGruposMatDesde ; Tiempo m�ximo: 0,2</remarks>
        Public Function GrupoMatNivel1_CargarTodosLosGruposMatDesde(ByVal lCiaComp As Long, ByVal iNumMaximo As Integer, Optional ByVal sGMN1 As String = Nothing,
                                                                    Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal bCoincid As Boolean = False,
                                                                    Optional ByVal sIdi As String = "SPA", Optional ByVal bOrdenadosPorDen As Boolean = False,
                                                                    Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GMN2"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@NUMMAX", iNumMaximo)
                cm.Parameters.AddWithValue("@GMN1", sGMN1)
                cm.Parameters.AddWithValue("@COD", sCod)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@COINCID", IIf(bCoincid, 1, 0))
                cm.Parameters.AddWithValue("@ORDENPORDEN", IIf(bOrdenadosPorDen, 1, 0))
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " GruposMatNivel1 data access methods "
        ''' <summary>
        '''  Carga de materiales
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iNumMaximo">Numero maximo de registros a cargar</param>
        ''' <param name="sCod">Codigo a cargar</param>
        ''' <param name="sDen">Denominacion a cargar</param>
        ''' <param name="bCoincid">Si se hace like o se hace =</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="bOrdenadosPorDen">Si se ordena por den o no</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\GruposMatNivel1.vb\CargarTodosLosGruposMatDesde       PMPortalNotificador\Notificar.vb\ObtenerDenominacionValorCampoGS; Tiempo m�ximo: 0,2</remarks>
        Public Function GruposMatNivel1_CargarTodosLosGruposMatDesde(ByVal lCiaComp As Long, ByVal iNumMaximo As Integer, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing,
                                                                     Optional ByVal bCoincid As Boolean = False, Optional ByVal sIdi As String = "SPA", Optional ByVal bOrdenadosPorDen As Boolean = False,
                                                                     Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GMN1"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@NUMMAX", iNumMaximo)
                cm.Parameters.AddWithValue("@COD", sCod)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@COINCID", IIf(bCoincid, 1, 0))
                cm.Parameters.AddWithValue("@ORDENPORDEN", IIf(bOrdenadosPorDen, 1, 0))
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Idiomas data access methods "
        ''' <summary>
        ''' Carga los idiomas
        ''' </summary>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Idiomas.vb\Load ; Tiempo m�ximo: 0,2</remarks>
        ''' Revisado por: Jbg. Fecha: 14/08/2012
        Public Function Idiomas_Load(Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "SELECT COD,DEN FROM IDIOMAS WITH (NOLOCK)"
                cm.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Instancias data access methods "
        ''' <summary>
        ''' Carga los procesos que requieren la intervenci�n del usuario y el hist�rico de procesos en los que particip�.
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lTipo">Id de solicitud</param>
        ''' <param name="sFecha">Fecha de alta</param>
        ''' <param name="lIdInstancia">Instancia</param>
        ''' <param name="iEstado">Estado de instancia</param>
        ''' <param name="sFormatoFecha">Formato de fecha</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancias.vb\DevolverWorkflow ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancias_Workflow(ByVal lCiaComp As Long, ByVal sProve As String, ByVal EmpresaPortal As Boolean, ByVal NomPortal As String,
                                            Optional ByVal sIdi As String = "SPA", Optional ByVal lTipo As Long = Nothing,
                                            Optional ByVal sFecha As String = Nothing, Optional ByVal lIdInstancia As Long = 0, Optional ByVal iEstado As Integer = 0, Optional ByVal sFormatoFecha As String = "",
                                            Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_GETWORKFLOW_PROVE"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@PRV", sProve)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@TI", lTipo)
                cm.Parameters.AddWithValue("@FEC", sFecha)
                cm.Parameters.AddWithValue("@INS", lIdInstancia)
                cm.Parameters.AddWithValue("@FILAESTADO", iEstado)
                cm.Parameters.AddWithValue("@FORMATOFECHA", sFormatoFecha)
                If EmpresaPortal Then cm.Parameters.AddWithValue("@NOM_PORTAL", NomPortal)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Saca las solicitudes relacionadas , las q tengan campos de tipo Importe solicitudes vinculadas
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="idSolPadre">Instancia</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancias.vb\DevolverSolicitudesPadre ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancias_Padre(ByVal lCiaComp As Long, ByVal sIdi As String, Optional ByVal idSolPadre As Integer = 0,
                                         Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_GETINSTANCIAS_PADRE"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@ID", idSolPadre)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Procedimiento que actualiza el estado de una instancia a En proceso
        ''' </summary>
        ''' <param name="sInstancias">Identificador de la instancia</param>
        ''' <param name="sUsuario">Codigo de usuario</param>
        ''' <remarks>
        ''' Llamada desde: PmServer/Instancia/ActualizarEnProceso2
        ''' tiempo m�ximo: 0,5 seg</remarks>
        Public Sub Actualizar_En_Proceso2(ByVal lCiaComp As Long, ByVal sInstancias As String, ByVal CodProveGS As String,
                                                    Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            'Actualiza el campo EN_PROCESO a 2 de la tabla INSTANCIA de varias instancias
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Try
                cn.Open()
                With cm
                    .Connection = cn
                    .CommandType = CommandType.StoredProcedure
                    .CommandText = FSGS(lCiaComp) & "FSPM_ACTUALIZAR_EN_PROCESO_2"
                    .Parameters.AddWithValue("@IDS", sInstancias)
                    .Parameters.AddWithValue("@PROVE", CodProveGS)
                    .ExecuteNonQuery()
                End With
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Sub
#End Region
#Region " Instancia data access methods "
        ''' <summary>
        ''' Procedimeinto que carga los grupos y campos de una instancia dada
        ''' </summary>
        ''' <param name="lID">instancia</param>
        ''' <param name="lVersion">Version</param> 
        ''' <param name="sUsuario">Codigo usuario</param>     
        ''' <param name="sProve">Codigo proveedor</param> 
        ''' <remarks>Llamada desde=PmServer/Instancia/CargarCumplimentacionFactura
        '''; Tiempo m�ximo=depende de los campos..grupos.</remarks>
        Public Function Instancia_CargarCumplimentacionFactura(ByVal lCiaComp As Long, ByVal lID As Long, ByVal lVersion As Long,
                                                    Optional ByVal sUsuario As String = Nothing, Optional ByVal sProve As String = Nothing,
                                                    Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Integer
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As SqlDataReader
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GET_CONF_CUMP_FACTURA"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@ID", lID)
                cm.Parameters.AddWithValue("@VERSION", lVersion)
                cm.Parameters.AddWithValue("@PROVE", sProve)
                dr = cm.ExecuteReader()

                If dr.Read() Then Instancia_CargarCumplimentacionFactura = dr.Item("ESCRITURA")
            Catch e As Exception
                Throw e
            Finally
                If Not dr Is Nothing AndAlso Not dr.IsClosed Then
                    dr.Close()
                End If
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Funcion que devuelve si una instancia tiene sustitucion de destinatario
        ''' </summary>
        ''' <param name="Destinatario">Codigo de persona que es destinatario</param>
        ''' <param name="Per">Codigo de persona</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Una variable booleana que indica si existe un sustituto de destinatario</returns>
        ''' <remarks>
        ''' Llamada desde: PmServer/Instancia/EssustitutoDestinatario
        ''' Tiempo m�ximo: 0,3 seg</remarks>
        Public Function Instancia_EsSustitutodeDestinatario(ByVal Per As String, ByVal Destinatario As String,
                                                            Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Boolean
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Dim sConsulta As String

            Dim lRes As Boolean
            Try
                lRes = False

                cm.Connection = cn
                sConsulta = "SELECT U.PER FROM USU U WITH (NOLOCK) INNER JOIN USU U2 WITH (NOLOCK) ON U.FSWS_SUSTITUCION = U2.COD AND U2.PER= '" & Per & "' WHERE U.PER= '" & Destinatario & "'"
                cm.CommandText = sConsulta
                cm.CommandType = CommandType.Text
                da.SelectCommand = cm
                da.Fill(ds)
                If ds.Tables(0).Rows.Count > 0 Then
                    lRes = True
                Else
                    lRes = False
                End If
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
            Return lRes
        End Function
        ''' <summary>
        ''' Obtendr� la etapa actual en la que se encuentra la instancia bas�ndose en la persona o proveedor que se pasa por par�metro, 
        ''' que ser� un usuario/proveedor al que se le ha trasladado la solicitud
        ''' </summary>
        ''' <param name="Instancia">Id de la instancia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos de la etapa actual</returns>
        ''' <remarks>Llamada desde: Instancia.vb --> DevolverEtapaActualTraslado; Tiempo m�ximo: 1 sg</remarks>
        Public Function Instancia_DevolverEtapaActualTraslado(ByVal lCiaComp As Long, ByVal Instancia As Long, ByVal sIdi As String,
                                                              Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_DEVOLVER_ETAPA_ACTUAL_TRASLADO"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@INSTANCIA", Instancia)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Carga la instancia
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="RequestID">Instancia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_Load(ByVal lCiaComp As Long, ByVal RequestID As Long, ByVal sIdi As String, ByVal TieneWorkflow As Boolean,
                                       Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPMQA_GET_INSTANCIA"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@ID", RequestID)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@NUEVO_WORKFL", TieneWorkflow)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Cargar Grupos Y Campos
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lCiaProve">Id de proveedor conectado</param> 
        ''' <param name="lID">Instancia</param>
        ''' <param name="lVersion">Version</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="sUsuario">usuario</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <param name="bRecuperarEnviada">Recuperar Enviada si/no</param>
        ''' <param name="bNuevoWorkflow">PM/QA</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_CargarGruposYCampos(ByVal lCiaComp As Long, ByVal lCiaProve As Long, ByVal lID As Long, ByVal lVersion As Long, ByVal sIdi As String, ByVal sUsuario As String,
                                                      ByVal sProve As String, ByVal bRecuperarEnviada As Boolean, ByVal bNuevoWorkflow As Boolean,
                                                      Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                If bNuevoWorkflow Then
                    cm.CommandText = FSGS(lCiaComp) & "FSPM_GETFIELDS_INSTANCIA"
                Else
                    cm.CommandText = FSGS(lCiaComp) & "FSQA_GETFIELDS_INSTANCIA"
                End If
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@ID", lID)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@VERSION", lVersion)
                cm.Parameters.AddWithValue("@USU", sUsuario)
                cm.Parameters.AddWithValue("@PROVE", sProve)
                Dim da As New SqlDataAdapter
                Dim cont As Integer
                da.SelectCommand = cm
                da.Fill(dr)

                dr.Relations.Add("REL_INST_GRUPO", dr.Tables(0).Columns("ID"), dr.Tables(1).Columns("INSTANCIA"), False)
                dr.Relations.Add("REL_GRUPO_CAMPO", dr.Tables(1).Columns("ID"), dr.Tables(2).Columns("GRUPO"), False)
                dr.Relations.Add("REL_GRUPO_LISTA", dr.Tables(1).Columns("ID"), dr.Tables(3).Columns("GRUPO"), False)
                dr.Relations.Add("REL_GRUPO_ADJUNTO", dr.Tables(1).Columns("ID"), dr.Tables(4).Columns("GRUPO"), False)
                dr.Relations.Add("REL_CAMPO_ADJUN", dr.Tables(2).Columns("ID_CAMPO"), dr.Tables(4).Columns("CAMPO"), False)
                cont = 5
                If bNuevoWorkflow Then
                    dr.Relations.Add("REL_GRUPO_BLOQUEO", dr.Tables(1).Columns("ID"), dr.Tables(cont).Columns("GRUPO"), False)
                    cont = cont + 1
                End If
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Se llama a esta funci�n para completar los datos cargados en la solicitud con         
        '''                todos aquellos campos que bien por estar ocultos o ser de solo lectura no se    
        '''                han podido recoger del formulario.
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="lIdCampo">Identificador del campo</param>
        ''' <param name="sProve">c�digo del proveedor </param>   
        ''' <param name="sAprob">c�digo del aprobador </param>   
        ''' <param name="bNuevoWorkflow">true PM/false QA</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>ds: dataset completado con los campos no visibles y de solo lectura </returns>
        ''' <remarks>Llamada desde:Instancia/CompletarFormulario Tiempo m�ximo:0 sg</remarks>
        Public Function Instancia_LoadInvisibles(ByVal lCiaComp As Long, ByVal lIdCampo As Long, ByVal sProve As String, Optional ByVal sAprob As String = Nothing, Optional ByVal bNuevoWorkflow As Boolean = False,
                                                 Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                If bNuevoWorkflow Then
                    cm.CommandText = FSGS(lCiaComp) & "FSPM_GETINSTINVISIBLEFIELDS"
                Else
                    cm.CommandText = FSGS(lCiaComp) & "FSQA_GETINSTINVISIBLEFIELDS"
                End If
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDCAMPO", lIdCampo)
                cm.Parameters.AddWithValue("@USU", sAprob)
                cm.Parameters.AddWithValue("@PROVE", sProve)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Salva los datos de instancia
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lCiaProve">Id de proveedor conectado</param> 
        ''' <param name="lIdInstancia">Instancia</param>
        ''' <param name="lVersion">Version</param>
        ''' <param name="sPer">Persona</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <param name="sMon">Moneda</param>
        ''' <param name="oDs">Datos a salvar</param>
        ''' <param name="bEnviar">Indica si se guarda sin enviar o se envian datos al producto</param>
        ''' <param name="dImporte">Importe de la instancia</param>
        ''' <param name="bPedidoAut">Indica si es un pedido automatico</param>
        ''' <param name="iCertificado">Id de certificado</param>
        ''' <param name="iNoConformidad">Id de no conformidad</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <remarks>Llamada desde: PMPortalserver/Instancia.vb/Save ; Tiempo m�ximo: 0,2</remarks>
        Public Sub Instancia_Save(ByVal lCiaComp As Long, ByVal lCiaProve As Long, ByVal lIdInstancia As Long, ByVal lVersion As Long, ByVal sPer As String,
                                       ByVal sProve As String, ByVal sMon As String, ByVal oDs As DataSet, ByVal bEnviar As Boolean, Optional ByVal dImporte As Double = 0,
                                       Optional ByVal bPedidoAut As Boolean = False, Optional ByVal iCertificado As Integer = Nothing, Optional ByVal iNoConformidad As Integer = Nothing,
                                       Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim sql As String

            cn.Open()
            cm = New SqlCommand("BEGIN DISTRIBUTED TRANSACTION", cn)
            cm.ExecuteNonQuery()
            cm = New SqlCommand("SET XACT_ABORT ON", cn)
            cm.ExecuteNonQuery()

            Dim daQuery As SqlDataAdapter
            Dim dsQuery As DataSet
            Dim daQueryPortal As SqlDataAdapter
            Dim dsQueryPortal As DataSet
            Dim drQuery As DataRow
            Dim dtQuery As DataTable
            Dim bPortal As Boolean = False
            Dim bNuevoWorkflow As Boolean = False

            If iCertificado <> Nothing And iCertificado <> 0 Then
                bNuevoWorkflow = False
            ElseIf iNoConformidad <> Nothing And iNoConformidad <> 0 Then
                bNuevoWorkflow = False
            Else
                bNuevoWorkflow = True
            End If

            Try
                If bNuevoWorkflow = False Then
                    'PRIMERO VOY A COMPROBAR SI YA SE HA GUARDADO ANTERIORMENTE EN EL PORTAL
                    sql = "SELECT COUNT(*) NUMERO FROM COPIA_CAMPO WITH (NOLOCK) WHERE INSTANCIA = @INSTANCIA AND CIA = @PROVE"
                    cm = New SqlCommand
                    cm.Connection = cn
                    cm.CommandText = sql
                    cm.Parameters.AddWithValue("@INSTANCIA", lIdInstancia)
                    cm.Parameters.AddWithValue("@PROVE", lCiaProve)
                    daQuery = New SqlDataAdapter
                    dsQuery = New DataSet

                    daQuery.SelectCommand = cm

                    daQuery.Fill(dsQuery)
                    bPortal = (dsQuery.Tables(0).Rows(0).Item("NUMERO") > 0)

                    If bPortal Then
                        'Antes voy a asegurarme de que el tipo en TEMPDESGLOSEADJUN/TEMPADJUN sea correcto pq
                        'EXISTE LA POSIBILIDAD DE "GUARDAR SIN ENVIAR". Vamos NUEVO para PM y VIEJO para PORTAL
                        For Each row As DataRow In oDs.Tables("TEMPDESGLOSEADJUN").Rows
                            If row("TIPO") = 1 Then
                                sql = "SELECT ISNULL(CLDA.ID_GS,0) AS ID_GS,ADJUN_PORTAL FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK) "
                                sql += "INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON CLDA.CAMPO_HIJO = C.ID AND CLDA.CIA = C.CIA "
                                sql += "WHERE C.INSTANCIA = " + CStr(lIdInstancia) + " AND C.CIA = " + CStr(lCiaProve)
                                sql += "AND CLDA.ID=" + CStr(row("ID"))

                                cm = New SqlCommand
                                cm.Connection = cn
                                cm.CommandText = sql
                                cm.CommandType = CommandType.Text

                                daQueryPortal = New SqlDataAdapter
                                dsQueryPortal = New DataSet

                                daQueryPortal.SelectCommand = cm
                                daQueryPortal.Fill(dsQueryPortal)

                                If dsQueryPortal.Tables(0).Rows.Count > 0 Then
                                    If dsQueryPortal.Tables(0).Rows(0)(0) = 0 Then
                                        row("TIPO") = 2
                                        row("ID") = dsQueryPortal.Tables(0).Rows(0)("ADJUN_PORTAL")
                                    Else
                                        If row("ID") <> dsQueryPortal.Tables(0).Rows(0)("ID_GS") Then
                                            row("ID") = dsQueryPortal.Tables(0).Rows(0)("ID_GS")
                                        End If
                                    End If
                                End If
                            End If
                        Next
                        For Each row As DataRow In oDs.Tables("TEMPADJUN").Rows
                            If row("TIPO") = 1 Then
                                sql = "SELECT ISNULL(CCA.ID_GS,0) AS ID_GS,ADJUN_PORTAL FROM COPIA_CAMPO_ADJUN CCA WITH (NOLOCK) "
                                sql += "INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON CCA.CAMPO = C.ID AND CCA.CIA = C.CIA "
                                sql += "WHERE C.INSTANCIA = " + CStr(lIdInstancia) + " AND C.CIA = " + CStr(lCiaProve)
                                sql += "AND CCA.ID=" + CStr(row("ID"))

                                cm = New SqlCommand
                                cm.Connection = cn
                                cm.CommandText = sql
                                cm.CommandType = CommandType.Text

                                daQueryPortal = New SqlDataAdapter
                                dsQueryPortal = New DataSet

                                daQueryPortal.SelectCommand = cm
                                daQueryPortal.Fill(dsQueryPortal)

                                If dsQueryPortal.Tables(0).Rows.Count > 0 Then
                                    If dsQueryPortal.Tables(0).Rows(0)(0) = 0 Then
                                        row("TIPO") = 2
                                        row("ID") = dsQueryPortal.Tables(0).Rows(0)("ADJUN_PORTAL")
                                    End If
                                End If
                            End If
                        Next
                    End If
                Else
                    'Por si acaso borro todo lo que haya en copia_campo, etc para esta versi�n:
                    'Borrando
                    sql = "DELETE FROM COPIA_CAMPO_ADJUN FROM COPIA_CAMPO_ADJUN CCA INNER JOIN COPIA_CAMPO C ON CCA.CAMPO = C.ID AND CCA.CIA = C.CIA WHERE C.INSTANCIA = @INSTANCIA AND C.CIA = @CIAPROVE" & vbCrLf
                    sql += "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA INNER JOIN COPIA_CAMPO C ON CLDA.CAMPO_HIJO = C.ID AND CLDA.CIA = C.CIA WHERE C.INSTANCIA = @INSTANCIA AND C.CIA = @CIAPROVE" & vbCrLf
                    sql += "DELETE FROM COPIA_LINEA_DESGLOSE FROM COPIA_LINEA_DESGLOSE CLD INNER JOIN COPIA_CAMPO C ON CLD.CAMPO_HIJO = C.ID AND CLD.CIA = C.CIA WHERE C.INSTANCIA = @INSTANCIA AND C.CIA = @CIAPROVE" & vbCrLf
                    sql += "DELETE FROM COPIA_CAMPO WHERE INSTANCIA = @INSTANCIA AND CIA = @CIAPROVE" & vbCrLf
                    sql += "DELETE FROM PARTICIPANTES WHERE INSTANCIA = @INSTANCIA AND CIA = @CIAPROVE" & vbCrLf

                    cm = New SqlCommand
                    cm.Connection = cn
                    cm.CommandText = sql
                    cm.CommandType = CommandType.Text
                    cm.Parameters.AddWithValue("@INSTANCIA", lIdInstancia)
                    cm.Parameters.AddWithValue("@CIAPROVE", lCiaProve)
                    cm.ExecuteNonQuery()
                End If

                If bPortal = False Then
                    If iCertificado <> Nothing Then
                        sql = "exec FSQA_LOAD_IDS_ULT_VERSION_CERT @INSTANCIA=@INSTANCIA ,@VERSION =@VERSION , @CERTIFICADO = @CERTIFICADO "
                        cm = New SqlCommand
                        cm.Connection = cn
                        cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT , @VERSION INT , @CERTIFICADO INT', @INSTANCIA, @VERSION, @CERTIFICADO"

                        cm.CommandType = CommandType.Text

                        cm.Parameters.Add("@stmt", SqlDbType.NText, sql.Length)
                        cm.Parameters("@stmt").Value = sql
                        cm.Parameters.AddWithValue("@INSTANCIA", lIdInstancia)
                        cm.Parameters.AddWithValue("@VERSION", lVersion)
                        cm.Parameters.AddWithValue("@CERTIFICADO", iCertificado)
                    Else
                        'SI ES LA PRIMERA VEZ QUE GUARDO EN EL PORTAL, COPIO DEL GS LA INSTANCIA AL PORTAL
                        sql = "SELECT C.ID,C.INSTANCIA,C.VALOR_TEXT,C.VALOR_NUM,C.VALOR_FEC,C.VALOR_BOOL,FC.GRUPO,FC.ES_SUBCAMPO " & vbCrLf
                        sql &= "FROM COPIA_CAMPO C WITH(NOLOCK) INNER JOIN FORM_CAMPO FC WITH(NOLOCK) ON FC.ID=C.COPIA_CAMPO_DEF WHERE C.INSTANCIA=@INSTANCIA AND C.NUM_VERSION=@VERSION" & vbCrLf
                        sql &= "SELECT CA.*,FC.GRUPO FROM COPIA_CAMPO_ADJUN CA WITH(NOLOCK) INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.ID=CA.CAMPO INNER JOIN FORM_CAMPO FC ON FC.ID=CC.COPIA_CAMPO_DEF " & vbCrLf
                        sql &= "WHERE CC.INSTANCIA=@INSTANCIA AND CC.NUM_VERSION=@VERSION" & vbCrLf
                        sql &= "SELECT CLD.* FROM COPIA_LINEA_DESGLOSE CLD WITH(NOLOCK) INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.ID=CLD.CAMPO_HIJO WHERE CC.INSTANCIA=@INSTANCIA AND CC.NUM_VERSION=@VERSION" & vbCrLf
                        sql &= "SELECT CLDA.* FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH(NOLOCK) INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CLDA.CAMPO_HIJO=CC.ID WHERE CC.INSTANCIA=@INSTANCIA AND CC.NUM_VERSION=@VERSION" & vbCrLf
                        sql &= "SELECT NC.CAMPO_PADRE,NC.LINEA,NC.NOCONFORMIDAD,NC.INSTANCIA,NC.VERSION,NC.ESTADO,NC.ESTADO_INT,NC.COMENT FROM NOCONF_ACC NC WITH(NOLOCK) WHERE NC.INSTANCIA=@INSTANCIA AND NC.VERSION=@VERSION"

                        cm = New SqlCommand
                        cm.Connection = cn
                        cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT , @VERSION INT', @INSTANCIA, @VERSION"
                        cm.CommandType = CommandType.Text

                        cm.Parameters.Add("@stmt", SqlDbType.NText, sql.Length)
                        cm.Parameters("@stmt").Value = sql
                        cm.Parameters.AddWithValue("@INSTANCIA", lIdInstancia)
                        cm.Parameters.AddWithValue("@VERSION", lVersion)
                    End If

                    daQuery = New SqlDataAdapter
                    dsQuery = New DataSet
                    daQuery.SelectCommand = cm
                    daQuery.Fill(dsQuery)

                    For Each dtQuery In dsQuery.Tables
                        For Each drQuery In dtQuery.Rows
                            drQuery.Item(0) = drQuery.Item(0)
                        Next
                    Next

                    sql = " INSERT INTO COPIA_CAMPO (CIA, ID, GRUPO, INSTANCIA, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL, ES_SUBCAMPO)" _
                        + " VALUES (@CIA, @ID, @GRUPO, @INSTANCIA, @VALOR_TEXT, @VALOR_NUM, @VALOR_FEC, @VALOR_BOOL, @ES_SUBCAMPO) "
                    cm = New SqlCommand
                    cm.Connection = cn
                    cm.CommandText = sql
                    cm.CommandType = CommandType.Text
                    cm.Parameters.AddWithValue("@CIA", lCiaProve)
                    cm.Parameters.Add("@ID", SqlDbType.Int, 4, "ID")
                    cm.Parameters.Add("@GRUPO", SqlDbType.Int, 4, "GRUPO")
                    cm.Parameters.AddWithValue("@INSTANCIA", lIdInstancia)
                    cm.Parameters.Add("@VALOR_TEXT", SqlDbType.NVarChar, 4000, "VALOR_TEXT")
                    cm.Parameters.Add("@VALOR_NUM", SqlDbType.Float, 8, "VALOR_NUM")
                    cm.Parameters.Add("@VALOR_FEC", SqlDbType.DateTime, 8, "VALOR_FEC")
                    cm.Parameters.Add("@VALOR_BOOL", SqlDbType.TinyInt, 1, "VALOR_BOOL")
                    cm.Parameters.Add("@ES_SUBCAMPO", SqlDbType.TinyInt, 1, "ES_SUBCAMPO")
                    da.UpdateCommand = cm

                    da.Update(dsQuery.Tables(0))
                    sql = " INSERT INTO COPIA_CAMPO_ADJUN (ID_GS,CIA,  GRUPO, ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA, RUTA)" _
                        + " VALUES (@ID, @CIA, @GRUPO, @ADJUN, @CAMPO, @NOM, @IDIOMA, @DATASIZE, @PER, @COMENT, @FECALTA, @RUTA) "
                    cm = New SqlCommand
                    cm.Connection = cn
                    cm.CommandText = sql
                    cm.CommandType = CommandType.Text
                    cm.Parameters.Add("@ID", SqlDbType.Int, 4, "ID")
                    cm.Parameters.AddWithValue("@CIA", lCiaProve)
                    cm.Parameters.Add("@GRUPO", SqlDbType.Int, 4, "GRUPO")
                    cm.Parameters.Add("@ADJUN", SqlDbType.Int, 4, "ADJUN")
                    cm.Parameters.Add("@CAMPO", SqlDbType.Int, 4, "CAMPO")
                    cm.Parameters.Add("@NOM", SqlDbType.NVarChar, 300, "NOM")
                    cm.Parameters.Add("@IDIOMA", SqlDbType.VarChar, 3, "IDIOMA")
                    cm.Parameters.Add("@DATASIZE", SqlDbType.Float, 8, "DATASIZE")
                    cm.Parameters.Add("@PER", SqlDbType.VarChar, 20, "PER")
                    cm.Parameters.Add("@COMENT", SqlDbType.NVarChar, 500, "COMENT")
                    cm.Parameters.Add("@FECALTA", SqlDbType.DateTime, 8, "FECALTA")
                    cm.Parameters.Add("@RUTA", SqlDbType.NVarChar, 2000, "RUTA")
                    da.UpdateCommand = cm
                    da.Update(dsQuery.Tables(1))

                    sql = " INSERT INTO COPIA_LINEA_DESGLOSE (CIA, LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL,LINEA_OLD)" _
                        + " VALUES (@CIA, @LINEA, @CAMPO_PADRE, @CAMPO_HIJO, @VALOR_TEXT, @VALOR_NUM, @VALOR_FEC, @VALOR_BOOL, @LINEA) "
                    cm = New SqlCommand
                    cm.Connection = cn
                    cm.CommandText = sql
                    cm.CommandType = CommandType.Text
                    cm.Parameters.AddWithValue("@CIA", lCiaProve)
                    cm.Parameters.Add("@LINEA", SqlDbType.Int, 4, "LINEA")
                    cm.Parameters.Add("@CAMPO_PADRE", SqlDbType.Int, 4, "CAMPO_PADRE")
                    cm.Parameters.Add("@CAMPO_HIJO", SqlDbType.Int, 4, "CAMPO_HIJO")
                    cm.Parameters.Add("@VALOR_NUM", SqlDbType.Float, 8, "VALOR_NUM")
                    cm.Parameters.Add("@VALOR_TEXT", SqlDbType.NVarChar, 4000, "VALOR_TEXT")
                    cm.Parameters.Add("@VALOR_FEC", SqlDbType.DateTime, 8, "VALOR_FEC")
                    cm.Parameters.Add("@VALOR_BOOL", SqlDbType.TinyInt, 1, "VALOR_BOOL")
                    da.UpdateCommand = cm

                    da.Update(dsQuery.Tables(2))

                    sql = " INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (ID_GS, CIA,  LINEA, CAMPO_PADRE, CAMPO_HIJO,  ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA, RUTA)" _
                        + " VALUES (@ID_GS, @CIA, @LINEA, @CAMPO_PADRE, @CAMPO_HIJO, @ADJUN,  @NOM, @IDIOMA, @DATASIZE, @PER, @COMENT, @FECALTA, @RUTA) "
                    cm = New SqlCommand
                    cm.Connection = cn
                    cm.CommandText = sql
                    cm.CommandType = CommandType.Text
                    cm.Parameters.Add("@ID_GS", SqlDbType.Int, 4, "ID")
                    cm.Parameters.AddWithValue("@CIA", lCiaProve)
                    cm.Parameters.Add("@LINEA", SqlDbType.Int, 4, "LINEA")
                    cm.Parameters.Add("@CAMPO_PADRE", SqlDbType.Int, 4, "CAMPO_PADRE")
                    cm.Parameters.Add("@CAMPO_HIJO", SqlDbType.Int, 4, "CAMPO_HIJO")
                    cm.Parameters.Add("@ADJUN", SqlDbType.Int, 4, "ADJUN")
                    cm.Parameters.Add("@NOM", SqlDbType.NVarChar, 300, "NOM")
                    cm.Parameters.Add("@IDIOMA", SqlDbType.VarChar, 3, "IDIOMA")
                    cm.Parameters.Add("@DATASIZE", SqlDbType.Float, 8, "DATASIZE")
                    cm.Parameters.Add("@PER", SqlDbType.VarChar, 20, "PER")
                    cm.Parameters.Add("@COMENT", SqlDbType.NVarChar, 500, "COMENT")
                    cm.Parameters.Add("@FECALTA", SqlDbType.DateTime, 8, "FECALTA")
                    cm.Parameters.Add("@RUTA", SqlDbType.NVarChar, 2000, "RUTA")

                    da.UpdateCommand = cm
                    da.Update(dsQuery.Tables(3))
                End If

                'ACTUALIZO CON LOS CAMBIOS REALIZADOS POR EL PROVEEDOR
                sql = "CREATE TABLE #TEMPORAL_DESGLOSE (ID INT PRIMARY KEY NONCLUSTERED NOT NULL IDENTITY (1, 1), LINEA INT NOT NULL, CAMPO_PADRE INT NOT NULL, CAMPO_HIJO INT NOT NULL, VALOR_TEXT NVARCHAR(4000), VALOR_NUM FLOAT, VALOR_FEC DATETIME, VALOR_BOOL TINYINT, LINEA_OLD INT) " + vbCrLf
                sql += "CREATE TABLE #TEMPORAL_ADJUN (ID INT PRIMARY KEY NONCLUSTERED NOT NULL IDENTITY (1, 1), CAMPO INT NOT NULL, ADJUN INT NOT NULL, TIPO INT NOT NULL) "
                sql += "CREATE TABLE #TEMPORAL_DESGLOSE_ADJUN (ID INT PRIMARY KEY NONCLUSTERED NOT NULL IDENTITY (1, 1), LINEA INT NOT NULL, CAMPO_PADRE INT NOT NULL, CAMPO_HIJO INT NOT NULL, ADJUN INT NOT NULL, TIPO INT NOT NULL) "
                cm = New SqlCommand

                With cm
                    .Connection = cn
                    .CommandType = CommandType.Text
                    .CommandText = sql
                End With
                cm.ExecuteNonQuery()

                cm = New SqlCommand
                sql = " UPDATE COPIA_CAMPO SET VALOR_TEXT = @VALOR_TEXT, VALOR_NUM = @VALOR_NUM, VALOR_FEC = @VALOR_FEC, VALOR_BOOL = @VALOR_BOOL WHERE ID = @ID AND CIA=" + lCiaProve.ToString + vbCrLf
                cm.CommandText = sql
                cm.CommandType = CommandType.Text
                cm.Connection = cn
                cm.Parameters.Add("@ID", SqlDbType.Int, 4, "CAMPO")
                cm.Parameters.Add("@VALOR_TEXT", SqlDbType.NVarChar, 4000, "VALOR_TEXT")
                cm.Parameters.Add("@VALOR_NUM", SqlDbType.Float, 8, "VALOR_NUM")
                cm.Parameters.Add("@VALOR_FEC", SqlDbType.DateTime, 8, "VALOR_FEC")
                cm.Parameters.Add("@VALOR_BOOL", SqlDbType.TinyInt, 1, "VALOR_BOOL")
                da.InsertCommand = cm
                da.Update(oDs.Tables("TEMP"))

                cm = New SqlCommand
                cm.Connection = cn
                sql = "INSERT INTO #TEMPORAL_ADJUN (CAMPO, ADJUN, TIPO) VALUES (@CAMPO, @ADJUN, @TIPO)"
                cm.CommandText = sql
                cm.CommandType = CommandType.Text
                cm.Parameters.Add("@CAMPO", SqlDbType.Int, 4, "CAMPO")
                cm.Parameters.Add("@ADJUN", SqlDbType.Int, 4, "ID")
                cm.Parameters.Add("@TIPO", SqlDbType.Int, 4, "TIPO")
                da.InsertCommand = cm
                da.Update(oDs.Tables("TEMPADJUN"))

                'Participantes a asignar
                cm = New SqlCommand
                sql = "INSERT INTO PARTICIPANTES (CIA,INSTANCIA, ROL,PER,PROVE,CON,TIPO) VALUES (@CIA, @INSTANCIA, @ROL, @PER, @PROVE,@CON, @TIPO)"
                cm.CommandText = sql
                cm.CommandType = CommandType.Text
                cm.Connection = cn
                cm.Parameters.AddWithValue("@CIA", lCiaProve)
                cm.Parameters.AddWithValue("@INSTANCIA", lIdInstancia)
                cm.Parameters.Add("@ROL", SqlDbType.Int, 4, "ROL")
                cm.Parameters.Add("@TIPO", SqlDbType.SmallInt, 2, "TIPO")
                cm.Parameters.Add("@PER", SqlDbType.NVarChar, 20, "PER")
                cm.Parameters.Add("@PROVE", SqlDbType.NVarChar, 20, "PROVE")
                cm.Parameters.Add("@CON", SqlDbType.Int, 4, "CON")
                da.InsertCommand = cm
                da.Update(oDs.Tables("TEMP_PARTICIPANTES"))

                'AHORA BORRAR LOS QUE ESTEN DEN COPIA_CAMPO_ADJUN Y LOS HAYA QUITADO
                If bPortal = False Then
                    sql = "DELETE FROM COPIA_CAMPO_ADJUN FROM COPIA_CAMPO_ADJUN CCA INNER JOIN COPIA_CAMPO CC ON CCA.CAMPO = CC.ID AND @INSTANCIA = CC.INSTANCIA AND CCA.CIA = CC.CIA AND CCA.CIA = " + lCiaProve.ToString() + " WHERE NOT EXISTS(SELECT * FROM #TEMPORAL_ADJUN T WHERE T.CAMPO = CCA.CAMPO AND T.ADJUN = CCA.ID_GS)" + vbCrLf
                Else
                    sql = "DELETE FROM COPIA_CAMPO_ADJUN FROM COPIA_CAMPO_ADJUN CCA INNER JOIN COPIA_CAMPO CC ON CCA.CAMPO = CC.ID AND @INSTANCIA = CC.INSTANCIA AND CCA.CIA = CC.CIA AND CCA.CIA = " + lCiaProve.ToString() + " WHERE NOT EXISTS(SELECT * FROM #TEMPORAL_ADJUN T WHERE T.CAMPO = CCA.CAMPO AND T.ADJUN = CCA.ID)" + vbCrLf
                End If
                cm = New SqlCommand(sql, cn)
                cm.Parameters.AddWithValue("@INSTANCIA", lIdInstancia)
                cm.ExecuteNonQuery()

                'AHORA METO LOS NUEVOS DE COPIA_ADJUN DEL PORTAL
                sql = "INSERT INTO COPIA_CAMPO_ADJUN (CIA, GRUPO, ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA, ADJUN_PORTAL)"
                sql += "SELECT " + lCiaProve.ToString() + ", C.GRUPO, NULL, C.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), NULL, CA.COMENT, GETDATE(), CA.ID " + vbCrLf
                sql += "  FROM COPIA_ADJUN CA WITH (NOLOCK) INNER JOIN #TEMPORAL_ADJUN T ON T.ADJUN = CA.ID INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON C.ID = T.CAMPO WHERE C.CIA = " + lCiaProve.ToString() + " AND T.TIPO = 2"
                cm = New SqlCommand(sql, cn)
                cm.ExecuteNonQuery()


                cm = New SqlCommand
                sql = "INSERT INTO #TEMPORAL_DESGLOSE (LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL, LINEA_OLD) VALUES (@LINEA, @CAMPO_PADRE, @CAMPO_HIJO, @VALOR_TEXT, @VALOR_NUM, @VALOR_FEC, @VALOR_BOOL, @LINEA_OLD)"
                cm.CommandText = sql
                cm.CommandType = CommandType.Text
                cm.Connection = cn
                cm.Parameters.Add("@LINEA", SqlDbType.Int, 4, "LINEA")
                cm.Parameters.Add("@CAMPO_PADRE", SqlDbType.Int, 4, "CAMPO_PADRE")
                cm.Parameters.Add("@CAMPO_HIJO", SqlDbType.Int, 4, "CAMPO_HIJO")
                cm.Parameters.Add("@VALOR_TEXT", SqlDbType.NVarChar, 4000, "VALOR_TEXT")
                cm.Parameters.Add("@VALOR_NUM", SqlDbType.Float, 8, "VALOR_NUM")
                cm.Parameters.Add("@VALOR_FEC", SqlDbType.DateTime, 8, "VALOR_FEC")
                cm.Parameters.Add("@VALOR_BOOL", SqlDbType.TinyInt, 1, "VALOR_BOOL")
                cm.Parameters.Add("@LINEA_OLD", SqlDbType.Int, 4, "LINEA_OLD")
                da.InsertCommand = cm

                da.Update(oDs.Tables("TEMPDESGLOSE"))

                cm = New SqlCommand
                cm.Connection = cn
                sql = "INSERT INTO #TEMPORAL_DESGLOSE_ADJUN (LINEA, CAMPO_PADRE, CAMPO_HIJO, ADJUN, TIPO) VALUES (@LINEA, @CAMPO_PADRE, @CAMPO_HIJO, @ADJUN, @TIPO)"
                cm.Parameters.Add("@LINEA", SqlDbType.Int, 4, "LINEA")
                cm.Parameters.Add("@CAMPO_PADRE", SqlDbType.Int, 4, "CAMPO_PADRE")
                cm.Parameters.Add("@CAMPO_HIJO", SqlDbType.Int, 4, "CAMPO_HIJO")
                cm.Parameters.Add("@ADJUN", SqlDbType.Int, 4, "ID")
                cm.Parameters.Add("@TIPO", SqlDbType.Int, 4, "TIPO")
                cm.CommandText = sql
                da.InsertCommand = cm

                da.Update(oDs.Tables("TEMPDESGLOSEADJUN"))
                sql = "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN "
                sql += "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA"
                sql += "         INNER JOIN #TEMPORAL_DESGLOSE TD"
                sql += "                 ON CLDA.CAMPO_PADRE = TD.CAMPO_PADRE"
                sql += "                AND CLDA.CAMPO_HIJO = TD.CAMPO_HIJO"
                sql += " WHERE NOT EXISTS (SELECT * FROM #TEMPORAL_DESGLOSE_ADJUN TDA WHERE CLDA.ID_GS = TDA.ADJUN AND TDA.TIPO = 1)"
                cm = New SqlCommand(sql, cn)
                cm.ExecuteNonQuery()

                sql = "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
                sql += "  FROM COPIA_LINEA_DESGLOSE CLD " & vbCrLf
                sql += "         INNER JOIN #TEMPORAL_DESGLOSE TD " & vbCrLf
                sql += "                 ON CLD.CAMPO_PADRE = TD.CAMPO_PADRE " & vbCrLf
                sql += "                AND CLD.CAMPO_HIJO = TD.CAMPO_HIJO " & vbCrLf
                sql += " WHERE NOT EXISTS (SELECT * FROM #TEMPORAL_DESGLOSE TD2 WHERE CLD.CAMPO_PADRE=TD2.CAMPO_PADRE AND CLD.CAMPO_HIJO = TD2.CAMPO_HIJO AND CLD.LINEA = TD2.LINEA_OLD) AND CLD.CIA = " + lCiaProve.ToString & vbCrLf
                cm = New SqlCommand(sql, cn)
                cm.ExecuteNonQuery()

                sql = "ALTER TABLE COPIA_LINEA_DESGLOSE NOCHECK CONSTRAINT ALL " & vbCrLf
                sql += "ALTER TABLE COPIA_LINEA_DESGLOSE_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
                sql += "UPDATE COPIA_LINEA_DESGLOSE  " & vbCrLf
                sql += "   SET VALOR_TEXT = T.VALOR_TEXT, " & vbCrLf
                sql += "       VALOR_NUM = T.VALOR_NUM, " & vbCrLf
                sql += "       VALOR_FEC = T.VALOR_FEC, " & vbCrLf
                sql += "       VALOR_BOOL = T.VALOR_BOOL, " & vbCrLf
                sql += "                LINEA = T.LINEA " & vbCrLf
                sql += "  FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
                sql += "         INNER JOIN #TEMPORAL_DESGLOSE T " & vbCrLf
                sql += "                 ON CLD.CAMPO_PADRE = T.CAMPO_PADRE " & vbCrLf
                sql += "                AND CLD.CAMPO_HIJO = T.CAMPO_HIJO " & vbCrLf
                sql += "                AND CLD.LINEA = T.LINEA_OLD " & vbCrLf
                sql += "                AND CLD.CIA = " + lCiaProve.ToString & vbCrLf
                sql += "UPDATE COPIA_LINEA_DESGLOSE_ADJUN " & vbCrLf
                sql += "   SET LINEA = T.LINEA " & vbCrLf
                sql += "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLD WITH (NOLOCK) " & vbCrLf
                sql += "       INNER JOIN #TEMPORAL_DESGLOSE T " & vbCrLf
                sql += "               ON CLD.CIA = " + lCiaProve.ToString & vbCrLf
                sql += "              AND CLD.CAMPO_PADRE = T.CAMPO_PADRE " & vbCrLf
                sql += "              AND CLD.CAMPO_HIJO = T.CAMPO_HIJO " & vbCrLf
                sql += "              AND CLD.LINEA = T.LINEA_OLD " & vbCrLf
                sql += "ALTER TABLE COPIA_LINEA_DESGLOSE_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
                sql += "ALTER TABLE COPIA_LINEA_DESGLOSE CHECK CONSTRAINT ALL " & vbCrLf
                cm = New SqlCommand(sql, cn)
                cm.ExecuteNonQuery()
                sql = "INSERT INTO COPIA_LINEA_DESGLOSE (CIA, LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL) " & vbCrLf
                sql += "SELECT " + lCiaProve.ToString + ", T.LINEA, T.CAMPO_PADRE, T.CAMPO_HIJO, T.VALOR_NUM, T.VALOR_TEXT, T.VALOR_FEC, T.VALOR_BOOL  " & vbCrLf
                sql += "FROM #TEMPORAL_DESGLOSE T WHERE ISNULL(LINEA_OLD,0) = 0 "
                cm = New SqlCommand(sql, cn)
                cm.ExecuteNonQuery()
                ''Demo Best Practice Fullstep / 2008 / 55 
                ''  -En ninguna etapa anterior lo rellenan ni siquiera lo ven PERO la linea existe. 
                ''  -As� que INSERT INTO #TEMPORAL_DESGLOSE lo mete
                ''  -pero INSERT INTO COPIA_LINEA_DESGLOSE no lo mete.
                ''  -El anterior FROM #TEMPORAL_DESGLOSE T WHERE ISNULL(LINEA_OLD,0) = 0 no lo mete pq LINEA_OLD no 
                ''   es cero.
                sql = "INSERT INTO COPIA_LINEA_DESGLOSE (CIA, LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL) " & vbCrLf
                sql += "SELECT " + lCiaProve.ToString + ", T.LINEA, T.CAMPO_PADRE, T.CAMPO_HIJO, T.VALOR_NUM, T.VALOR_TEXT, T.VALOR_FEC, T.VALOR_BOOL  " & vbCrLf
                sql += "FROM #TEMPORAL_DESGLOSE T WHERE ISNULL(LINEA_OLD,0) <> 0 "
                sql += "AND NOT EXISTS(SELECT * FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) WHERE CLD.CAMPO_PADRE = T.CAMPO_PADRE AND CLD.CAMPO_HIJO = T.CAMPO_HIJO AND CLD.LINEA = T.LINEA_OLD)" & vbCrLf
                cm = New SqlCommand(sql, cn)
                cm.ExecuteNonQuery()

                sql = "INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN(CIA, CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, COMENT, FECALTA,ADJUN_PORTAL)"
                sql += "SELECT " + lCiaProve.ToString + ", T.CAMPO_PADRE, T.CAMPO_HIJO,  T.LINEA, CA.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.COMENT, GETDATE(), CA.ID"
                sql += "  FROM COPIA_ADJUN CA WITH (NOLOCK) "
                sql += "        INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T"
                sql += "                ON CA.ID = T.ADJUN"
                sql += "                WHERE T.TIPO = 2"
                cm = New SqlCommand(sql, cn)
                cm.ExecuteNonQuery()

                '---------- Defecto ---------------
                For Each row As DataRow In oDs.Tables("TEMPDESGLOSEADJUN").Rows
                    If row("TIPO") = 3 Then
                        sql = "INSERT INTO COPIA_ADJUN (DATA, FECALTA, NOM, COMENT,ADJUN_ORIGEN, CIA, USU) " + vbCrLf
                        sql += "SELECT A.DATA, GETDATE(), A.NOM, A.COMENT, A.ID," + lCiaProve.ToString + ",A.PER" + vbCrLf
                        sql += " FROM " & FSGS(lCiaComp) & "LINEA_DESGLOSE_ADJUN A WITH (NOLOCK)" + vbCrLf
                        sql += " WHERE A.ID=" + CStr(row.Item("ID")) + vbCrLf
                        cm = New SqlCommand
                        cm.Connection = cn
                        cm.CommandText = sql
                        cm.CommandType = CommandType.Text
                        cm.ExecuteNonQuery()

                        cm = New SqlCommand
                        dr = New DataSet
                        cm.Connection = cn
                        cm.CommandText = "SELECT MAX(ID) AS INDCA FROM COPIA_ADJUN "
                        cm.CommandType = CommandType.Text
                        da.SelectCommand = cm
                        da.Fill(dr)

                        sql = "INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN(CIA, CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, COMENT, FECALTA,ADJUN_PORTAL)"
                        sql += "SELECT " + lCiaProve.ToString + "," & CStr(row.Item("CAMPO_PADRE"))
                        sql += "," & CStr(row.Item("CAMPO_HIJO")) & "," & CStr(row.Item("LINEA"))
                        sql += ",CA.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.COMENT, GETDATE(), CA.ID"
                        sql += "  FROM COPIA_ADJUN CA  WITH (NOLOCK) WHERE CA.ID =" & CStr(dr.Tables(0).Rows(0).Item("INDCA"))
                        cm = New SqlCommand(sql, cn)
                        cm.ExecuteNonQuery()
                    End If
                Next
                ''''''''''
                sql = "DELETE FROM NOCONF_ACC WHERE CIA = " & lCiaProve & " AND INSTANCIA =" & lIdInstancia.ToString
                cm = New SqlCommand(sql, cn)
                cm.ExecuteNonQuery()

                sql = " INSERT INTO NOCONF_ACC (CIA, CAMPO_PADRE, LINEA, NOCONFORMIDAD, INSTANCIA, VERSION, ESTADO, ESTADO_INT, COMENT)" _
                                       + " VALUES (" & lCiaProve & ", @CAMPO_PADRE, @LINEA, @NOCONFORMIDAD, @INSTANCIA, @VERSION, @ESTADO,  @ESTADO_INT, @COMENT) "
                cm = New SqlCommand
                cm.Connection = cn
                cm.CommandText = sql
                cm.CommandType = CommandType.Text
                cm.Parameters.Add("@CAMPO_PADRE", SqlDbType.Int, 4, "CAMPO_PADRE")
                cm.Parameters.Add("@LINEA", SqlDbType.Int, 4, "LINEA")
                cm.Parameters.AddWithValue("@NOCONFORMIDAD", iNoConformidad)
                cm.Parameters.AddWithValue("@INSTANCIA", lIdInstancia)
                cm.Parameters.AddWithValue("@VERSION", lVersion)
                cm.Parameters.Add("@ESTADO", SqlDbType.NVarChar, 20, "ESTADO")
                cm.Parameters.Add("@ESTADO_INT", SqlDbType.Float, 8, "ESTADO_INT")
                cm.Parameters.Add("@COMENT", SqlDbType.NVarChar, 500, "COMENT")
                da.InsertCommand = cm

                da.Update(oDs.Tables("TEMPNOCONF_ACC"))


                cm = New SqlCommand("SET XACT_ABORT OFF", cn)
                cm.ExecuteNonQuery()
                cm = New SqlCommand("COMMIT TRANSACTION", cn)
                cm.ExecuteNonQuery()

                'Tratamiento de errores:
            Catch e As SqlException
                cm = New SqlCommand("SET XACT_ABORT OFF", cn)
                cm.ExecuteNonQuery()
                cm = New SqlCommand("IF @@TRANCOUNT>0 ROLLBACK TRANSACTION", cn)
                cm.ExecuteNonQuery()
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Sub
        ''' <summary>
        ''' Inserta los datos generales de la instancia.
        ''' </summary>
        ''' <param name="lIdSolicitud">Id de la solicitud</param>
        ''' <param name="lIdWorkFlow">Id del workFlow</param>
        ''' <param name="dImportePedidoDirecto">Importe del pedido Directo</param>
        ''' <param name="sPeticionarioProve">Codigo de proveedor</param>
        ''' <param name="iPeticionarioProveCon">Id de contacto</param>
        ''' <param name="sMon">Codigo de moneda</param>
        ''' <param name="lIdInstancia">Id de la instancia</param>
        ''' <remarks> Llamada desde: PmServer/Instancia/CreatePrev Tiempo m�ximo: </remarks>
        Public Sub Instancia_Create_Prev(ByVal lCiaComp As Long, ByVal lIdSolicitud As Long, ByVal IdFormulario As Long, ByVal lIdWorkFlow As Long, ByVal dImportePedidoDirecto As Double,
                                          ByVal sPeticionarioProve As String, ByVal iPeticionarioProveCon As Integer, ByVal sMon As String, ByRef lIdInstancia As Long,
                                         Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "BEGIN TRANSACTION"
                cm.ExecuteNonQuery()

                cm = New SqlCommand(FSGS(lCiaComp) & "FSPM_CREATE_NEW_INSTANCE", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandTimeout = 60
                cm.Parameters.AddWithValue("@SOLICITUD", lIdSolicitud)
                cm.Parameters.AddWithValue("@FORMULARIO", IdFormulario)
                cm.Parameters.AddWithValue("@WORKFLOW", lIdWorkFlow)
                cm.Parameters.AddWithValue("@IMPORTE_PEDIDO_DIRECTO", dImportePedidoDirecto)
                cm.Parameters.AddWithValue("@PETPROVE", sPeticionarioProve)
                cm.Parameters.AddWithValue("@PETPROVECON", iPeticionarioProveCon)
                cm.Parameters.AddWithValue("@MON", sMon)
                cm.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.InputOutput
                cm.Parameters("@ID").Value = lIdInstancia
                cm.ExecuteNonQuery()

                lIdInstancia = cm.Parameters("@ID").Value

                cm = New SqlCommand("COMMIT TRANSACTION", cn)
                cm.CommandTimeout = 60
                cm.ExecuteNonQuery()
            Catch e As SqlException
                cm = New SqlCommand("IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION", cn)
                cm.ExecuteNonQuery()
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Sub
        ''' <summary>
        ''' Cargar los estados de una No Conformidad
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer/Instancia.vb/CargarTiposEstadoAccion ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_CargarTiposEstadoAccion(ByVal lCiaComp As Long, ByVal lInstancia As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_LOADESTADOSINSTANCIA"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@INSTANCIA", lInstancia)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        '''<summary>Comprobar que la acci�n sea de rechazo o anulaci�n</summary>
        ''' <param name="idAccion">Id de la acci�n</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Devuelve true si la acci�n es de tipo rechazo o anulaci�n</returns>
        Public Function Instancia_AccionSinControlDisponible(ByVal lCiaComp As Long, ByVal idAccion As Integer, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Boolean
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As SqlDataReader = Nothing
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandText = FSGS(lCiaComp) & "FSPM_DEVOLVER_TIPO_ACCION"
                cm.Parameters.AddWithValue("@ID", idAccion)
                dr = cm.ExecuteReader()
                If dr.HasRows() Then
                    dr.Read()
                    '2: Rechazo definitivo; 3: Anulaci�n
                    If dr.Item("TIPO_RECHAZO") = 2 Or dr.Item("TIPO_RECHAZO") = 3 Then
                        Instancia_AccionSinControlDisponible = True
                    Else
                        Instancia_AccionSinControlDisponible = False
                    End If
                End If
            Catch e As Exception
                Throw e
            Finally
                If Not dr Is Nothing AndAlso Not dr.IsClosed Then dr.Close()
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Obtiene los Campos calculados a partir de una instancia y version
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lId">instancia</param>
        ''' <param name="lVersion">Version</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_LoadCamposCalculados(ByVal lCiaComp As Long, ByVal lId As Long, ByVal lVersion As Long,
                Optional ByVal sIdi As String = Nothing, Optional ByVal sProve As String = Nothing, Optional Workflow As Boolean = True,
                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_LOADINSTFIELDSCALC"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@INSTANCIA", lId)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@PROVE", sProve)
                cm.Parameters.AddWithValue("@VERSION", lVersion)
                cm.Parameters.AddWithValue("@WORKFLOW", Workflow)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Obtiene el hist�rico de una solicitud devolviendo por todas las etapas en las que ha estado.
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lID">Id de instancia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalserver/Instancia.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_CargarHistoricoEstados(ByVal lCiaComp As Long, ByVal lID As Long, Optional ByVal sIdi As String = Nothing,
                                                         Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandText = "FSPM_DEVOLVER_HISTORICO_INSTANCIA"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@INSTANCIA", lID)
                cm.Parameters.AddWithValue("@IDI", sIdi)

                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Obtener el Comentario del Estado
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lID">ID de instancia_est</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb\CargarComentarioEstado ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_DevolverComentarioEstado(ByVal lCiaComp As Long, ByVal lID As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As String
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSWS_DEVOLVER_COMENT_ESTADO"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@ID", lID)
                Dim dr As SqlDataReader = cm.ExecuteReader()
                If dr.Read() Then
                    Instancia_DevolverComentarioEstado = dr.Item("COMENT").ToString()
                    dr.Close()
                End If
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Obtendr� la etapa actual en la que se encuentra la instancia bas�ndose en la persona o proveedor que se pasa por par�metro:
        ''' 1- Puede que sea el rol de un bloque activo
        ''' 2- Puede que sea un rol de tipo lista autoasignable
        ''' 3- Puede que no sea un rol, sino un usuario/proveedor al que se le ha trasladado la solicitud, o que est� a la espera de que se la devuelvan
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="Instancia">Instancia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb\DevolverEtapaActua ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_DevolverEtapaActual(ByVal lCiaComp As Long, ByVal Instancia As Long, ByVal sIdi As String, ByVal sProve As String,
                                                      Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_DEVOLVER_ETAPA_ACTUAL"
                cm.Parameters.AddWithValue("@INSTANCIA", Instancia)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@PROVE", sProve)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Obtiene los enlaces a siguientes etapas a partir de la accion
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lIdInstancia">Instancia</param>
        ''' <param name="lAccion">Accion</param>
        ''' <param name="sPer">Persona</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb\DevolverSiguientesEtapas ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_ObtenerSiguientesEtapas(ByVal lCiaComp As Long, ByVal lIdInstancia As Long, ByVal lAccion As Long, ByVal sPer As String, ByVal sIdi As String,
                                                          ByVal lBloque As Long, ByVal lRol As Long,
                                                          Optional ByVal dsFactura As DataSet = Nothing, Optional ByVal SesionId As String = "",
                                                          Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim dr As New DataSet
            dr = PMPortalDatabaseServer.DevolverSiguientesEtapas(mDBConnection, lCiaComp, lIdInstancia, lAccion, sPer, sIdi, lBloque, lRol, dsFactura)
            Return dr
        End Function
        ''' <summary>Obtiene los enlaces a siguientes etapas a partir de la accion</summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="idInstancia">Instancia</param>
        ''' <param name="idAccion">Accion</param>
        ''' <param name="codProve">C�digo del proveedor</param>
        ''' <param name="codIdioma">Idioma</param>
        ''' <param name="idBloque">Bloque</param>
        ''' <param name="idRol">Rol</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb\DevolverSiguientesEtapas</remarks>
        Public Function Instancia_ObtenerSiguientesEtapasForm(ByVal lCiaComp As Long, ByVal idInstancia As Long, ByVal idAccion As Long, ByVal codProve As String, ByVal codIdioma As String,
                                                          ByVal ds As DataSet, ByVal idBloque As Long, ByVal idRol As Long, ByRef sRolPorWebService As String,
                                                          Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim dr As New DataSet
            dr = PMPortalDatabaseServer.DevolverSiguientesEtapasForm(mDBConnection, lCiaComp, idInstancia, idAccion, codProve, codIdioma, ds, idBloque, idRol, sRolPorWebService)
            Return dr
        End Function
        ''' <summary>
        ''' Obtiene los enlaces a siguientes etapas a partir de la accion
        ''' </summary>
        ''' <param name="lAccion">Accion</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb\DevolverSiguientesEtapas ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_ObtenerEnlaces(ByVal lBloqueOrigen As Long, ByVal lAccion As Long, ByVal lCiaComp As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)
            Return Fullstep.PMPortalDatabaseServer.ObtenerEnlaces(mDBConnection, lBloqueOrigen, lAccion, lCiaComp)
        End Function
        ''' <summary>
        ''' Devolver datos para realizar un Traslado
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb\DevolverDatosTraslado ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_DevolverDatosTraslado(ByVal lCiaComp As Long, ByVal lInstancia As Long, ByVal sProve As String,
                                                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim sSQL As String
            Dim sCodProveGS As String
            'Obtener El Codigo del Proveedor en el GS
            sSQL = "SELECT COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID " &
            " WHERE RC.CIA_COMP = " & lCiaComp & " AND C.COD = '" & sProve & "'"
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = sSQL
                cm.CommandType = CommandType.Text
                da.SelectCommand = cm
                da.Fill(dr)

                If dr.Tables(0).Rows.Count = 0 Then
                    sCodProveGS = ""
                Else
                    sCodProveGS = dr.Tables(0).Rows(0).Item("COD_PROVE_CIA")
                End If

                cm = New SqlCommand
                cm.Connection = cn
                cm.CommandText = "SELECT TOP 1 IE.PER PERSONA_EST,P.NOM + ' ' + P.APE NOM_PERSONA_EST,IE.FECHA FECHA_EST ,IE.FEC_LIMITE " &
                " FROM " & FSGS(lCiaComp) & "INSTANCIA_EST IE WITH (NOLOCK) " &
                " INNER JOIN " & FSGS(lCiaComp) & "INSTANCIA_BLOQUE IB WITH (NOLOCK) ON IE.BLOQUE=IB.ID AND IB.ESTADO=1 AND IB.TRASLADADA=1 " &
                " LEFT JOIN " & FSGS(lCiaComp) & "PER P WITH (NOLOCK) ON IE.PER=P.COD" &
                " WHERE IE.DESTINATARIO_PROV='" & DblQuote(sCodProveGS) & "' AND IE.INSTANCIA=" & lInstancia &
                " ORDER BY IE.ID DESC"
                cm.CommandType = CommandType.Text
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Carga las etapas por las que pasar�
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="Accion">Accion</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb\DevolverEtapasRealizadas ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_DevolverEtapasRealizadas(ByVal lCiaComp As Long, ByVal Accion As Integer, ByVal BloqueDestino As String, ByVal sIdi As String,
                                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_LOAD_ETAPAS_REALIZADAS"
                cm.Parameters.AddWithValue("@ACCION", Accion)
                cm.Parameters.AddWithValue("@BLOQUEDESTINO", BloqueDestino)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Obtiene las precondiciones de una acci�n de una instancia
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="IdAccion">Accion</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb\ObtenerPrecondicionesAccion ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_LoadPrecondicionesAccion(ByVal lCiaComp As Long, ByVal lInstancia As Integer, ByVal IdAccion As Integer, ByVal sIdi As String,
                                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_GET_PRECONDICIONES"
                cm.CommandType = CommandType.StoredProcedure
                Dim sol As Integer = 0
                cm.Parameters.AddWithValue("@BSOLICITUD", sol)
                cm.Parameters.AddWithValue("@IDACC", IdAccion)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@INSTANCIA", lInstancia)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                dr.Tables(0).ChildRelations.Add("PRECOND_CONDICIONES", dr.Tables(0).Columns("ID"), dr.Tables(1).Columns("ACCION_PRECOND"))
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Devolver procesos EP relacionados a la instancia
        ''' </summary>
        ''' <param name="lID">Instancia</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb\DevolverProcesosRelacionados ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_CargarProcesos(ByVal lID As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSEP_DEVOLVER_PROCESOS_SOLICITUD"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@ID", lID)
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Obtener Importe Adjudicado
        ''' </summary>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb\ObtenerImporteAdjudicado ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_ObtenerImporteAdjudicado(ByVal lInstancia As Integer, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Double
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim result As Double
            Dim sConsulta As String
            Try
                cn.Open()
                cm.Connection = cn
                sConsulta = "SELECT SUM(A.PREC) IMP_ADJ FROM AR_ITEM A INNER JOIN ITEM I ON A.ANYO=I.ANYO AND A.GMN1=I.GMN1 AND A.PROCE=I.PROCE AND A.ITEM=I.ID WHERE I.SOLICIT = @INSTANCIA "
                cm.Parameters.AddWithValue("@INSTANCIA", lInstancia)
                cm.CommandText = sConsulta
                cm.CommandType = CommandType.Text
                result = CDbl(cm.ExecuteScalar())
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return result
        End Function
        ''' <summary>
        ''' Obtener Titulo de la instancia
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb\DevolverTitulo ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_DevolverTitulo(ByVal lCiaComp As Long, ByVal sIdi As String, ByVal lInstancia As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim sConsulta As String
            Dim BBDD As String
            Try
                BBDD = FSGS(lCiaComp)

                cm.Connection = cn
                sConsulta = "SELECT TOP 1 CASE WHEN C1.SUBTIPO=" & TiposDeDatos.TipoCampoGS.Importe & " THEN CONVERT(VARCHAR ,C.VALOR_FEC,103)  "
                sConsulta = sConsulta & " WHEN C1.TIPO_CAMPO_GS=" & TiposDeDatos.TipoCampoGS.Proveedor & " THEN PROVE.COD + '-' + PROVE.DEN "
                sConsulta = sConsulta & " WHEN C1.TIPO_CAMPO_GS=" & TiposDeDatos.TipoCampoGS.Moneda & " THEN MON.DEN_" & sIdi
                sConsulta = sConsulta & " WHEN C1.TIPO_CAMPO_GS=" & TiposDeDatos.TipoCampoGS.Pais & " THEN PAI.DEN "
                sConsulta = sConsulta & " WHEN C1.TIPO_CAMPO_GS=" & TiposDeDatos.TipoCampoGS.Provincia & " THEN PROVI.DEN "
                sConsulta = sConsulta & " WHEN C1.TIPO_CAMPO_GS=" & TiposDeDatos.TipoCampoGS.Dest & " THEN D.DEN_" & sIdi
                sConsulta = sConsulta & " WHEN C1.TIPO_CAMPO_GS=" & TiposDeDatos.TipoCampoGS.Persona & " THEN P.NOM +' '+ P.APE "
                sConsulta = sConsulta & " WHEN C1.TIPO_CAMPO_GS=" & TiposDeDatos.TipoCampoGS.Departamento & " THEN DEP2.COD + '-' + DEP2.DEN  "
                sConsulta = sConsulta & " WHEN C1.TIPO_CAMPO_GS=" & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " THEN O.COD + '-'+ O.DEN "
                sConsulta = sConsulta & " WHEN C1.TIPO_CAMPO_GS=" & TiposDeDatos.TipoCampoGS.Centro & " THEN CE.COD + '-' + CE.DEN "
                sConsulta = sConsulta & " WHEN C1.TIPO_CAMPO_GS=" & TiposDeDatos.TipoCampoGS.Almacen & " THEN A.COD + '-' + A.DEN "
                sConsulta = sConsulta & " WHEN C1.TIPO_CAMPO_GS=" & TiposDeDatos.TipoCampoGS.Material & " THEN REPLACE(SUBSTRING(C.VALOR_TEXT,(SELECT SUM(LONGITUD) "
                sConsulta = sConsulta & " FROM " & BBDD & "DIC WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1,"
                sConsulta = sConsulta & "(SELECT LONGITUD FROM " & BBDD & "DIC WHERE NOMBRE= 'GMN4')),' ' ,'')+' - '+(SELECT DEN_SPA FROM " & BBDD & "GMN4 WHERE GMN1=  REPLACE   (SUBSTRING(C.VALOR_TEXT,1,"
                sConsulta = sConsulta & "(SELECT LONGITUD FROM " & BBDD & "DIC WHERE NOMBRE= 'GMN1')) ,' ' ,'') AND GMN2=REPLACE(SUBSTRING(C.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM " & BBDD & "DIC WHERE NOMBRE= 'GMN1')+1,"
                sConsulta = sConsulta & "(SELECT LONGITUD FROM " & BBDD & "DIC WHERE NOMBRE= 'GMN2')),' ' ,'') AND GMN3=REPLACE(SUBSTRING(C.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM " & BBDD & "DIC WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2')+1,"
                sConsulta = sConsulta & "(SELECT LONGITUD FROM " & BBDD & "DIC WHERE NOMBRE= 'GMN3')),' ','') AND COD=REPLACE(SUBSTRING(C.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM " & BBDD & "DIC WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1,"
                sConsulta = sConsulta & "(SELECT LONGITUD FROM " & BBDD & "DIC WHERE NOMBRE= 'GMN4')),' ' ,'') ) ELSE C.VALOR_TEXT END DESCR"
                sConsulta = sConsulta & " FROM " & BBDD & "INSTANCIA I WITH (NOLOCK)"
                sConsulta = sConsulta & " INNER JOIN " & BBDD & "COPIA_CAMPO C WITH (NOLOCK) ON C.INSTANCIA=I.ID AND C.NUM_VERSION=(SELECT MAX(NUM_VERSION) FROM " & BBDD & "VERSION_INSTANCIA WHERE INSTANCIA=I.ID)"
                sConsulta = sConsulta & " INNER JOIN " & BBDD & "FORM_CAMPO C1 WITH (NOLOCK) ON C1.ID=C.COPIA_CAMPO_DEF"
                sConsulta = sConsulta & " LEFT JOIN " & BBDD & "PROVE PROVE WITH (NOLOCK)  ON C.VALOR_TEXT=PROVE.COD"
                sConsulta = sConsulta & " LEFT JOIN " & BBDD & "PER P WITH (NOLOCK) ON C.VALOR_TEXT=P.COD"
                sConsulta = sConsulta & " LEFT JOIN " & BBDD & "MON MON WITH (NOLOCK) ON C.VALOR_TEXT=MON.COD "
                sConsulta = sConsulta & " LEFT JOIN " & BBDD & "PAI PAI WITH (NOLOCK) ON C.VALOR_TEXT=PAI.COD "
                sConsulta = sConsulta & " LEFT JOIN " & BBDD & "PROVI PROVI WITH (NOLOCK) ON C.VALOR_TEXT=PROVI.COD "
                sConsulta = sConsulta & " LEFT JOIN " & BBDD & "DEST D WITH (NOLOCK) ON C.VALOR_TEXT=D.COD "
                sConsulta = sConsulta & " LEFT JOIN " & BBDD & "DEP AS DEP2 WITH (NOLOCK) ON C.VALOR_TEXT=DEP2.COD"
                sConsulta = sConsulta & " LEFT JOIN " & BBDD & "ORGCOMPRAS O WITH (NOLOCK) ON C.VALOR_TEXT=O.COD "
                sConsulta = sConsulta & " LEFT JOIN " & BBDD & "CENTROS CE WITH (NOLOCK) ON C.VALOR_TEXT=CE.COD "
                sConsulta = sConsulta & " LEFT JOIN " & BBDD & "ALMACEN A WITH (NOLOCK) ON C.VALOR_NUM=A.ID"
                sConsulta = sConsulta & " WHERE (C1.TITULO=1 Or C1.TIPO_CAMPO_GS=" & TiposDeDatos.TipoCampoGS.DescrBreve & ")"
                sConsulta = sConsulta & " AND I.ID=" & lInstancia
                sConsulta = sConsulta & " ORDER BY I.ID DESC,C1.TITULO DESC"
                cm.CommandText = sConsulta
                cm.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Devolver Importe Solicitudes Vinculadas
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lID">Instancia</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb\DevolverImporteSolicitudesVinculadas ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_DevolverImporteSolicitudesVinculadas(ByVal lCiaComp As Long, ByVal lID As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim sConsulta As String

            sConsulta = "SELECT MAX(I.ID) INSTANCIA,SUM(T.VALOR_NUM)*MAX(MON.EQUIV) ACUM"
            sConsulta = sConsulta & " FROM " & FSGS(lCiaComp) & "COPIA_CAMPO CC1 WITH (NOLOCK)"
            sConsulta = sConsulta & " INNER JOIN " & FSGS(lCiaComp) & "FORM_CAMPO CCF1 WITH (NOLOCK) ON CCF1.ID = CC1.COPIA_CAMPO_DEF"
            sConsulta = sConsulta & " INNER JOIN (SELECT INSTANCIA INS,VALOR_NUM FROM " & FSGS(lCiaComp) & "COPIA_CAMPO CC2 WITH (NOLOCK)"
            sConsulta = sConsulta & " INNER JOIN " & FSGS(lCiaComp) & "INSTANCIA  I WITH (NOLOCK) ON I.ID = CC2.INSTANCIA "
            sConsulta = sConsulta & " INNER JOIN " & FSGS(lCiaComp) & "FORM_CAMPO CCF2 WITH (NOLOCK) ON CCF2.ID=CC2.COPIA_CAMPO_DEF WHERE ES_IMPORTE_TOTAL=1 AND CCF2.ES_SUBCAMPO=0"
            sConsulta = sConsulta & " AND NUM_VERSION = (SELECT MAX(NUM_VERSION) FROM " & FSGS(lCiaComp) & "COPIA_CAMPO CC "
            sConsulta = sConsulta & " WHERE CC.COPIA_CAMPO_DEF=CCF2.ID "
            sConsulta = sConsulta & " AND (I.ESTADO = " & TiposDeDatos.TipoEstadoSolic.Pendiente & " OR I.ESTADO = " & TiposDeDatos.TipoEstadoSolic.SCPendiente & " OR I.ESTADO = " & TiposDeDatos.TipoEstadoSolic.SCAprobada & "  OR I.ESTADO = " & TiposDeDatos.TipoEstadoSolic.SCCerrada & ")"
            sConsulta = sConsulta & ")) T ON T.INS = CC1.INSTANCIA"
            sConsulta = sConsulta & " INNER JOIN " & FSGS(lCiaComp) & "INSTANCIA I WITH (NOLOCK) ON I.ID = CC1.VALOR_NUM"
            sConsulta = sConsulta & " INNER JOIN " & FSGS(lCiaComp) & "MON MON WITH (NOLOCK) ON MON.COD = I.MON "
            sConsulta = sConsulta & " WHERE TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.RefSolicitud & " AND NUM_VERSION=(SELECT MAX(NUM_VERSION) FROM " & FSGS(lCiaComp) & "COPIA_CAMPO CC WHERE CC.COPIA_CAMPO_DEF=CCF1.ID)"
            sConsulta = sConsulta & " AND I.ID = " & lID
            sConsulta = sConsulta & " GROUP BY CC1.VALOR_NUM"

            Try
                cm.Connection = cn
                cm.CommandText = sConsulta
                cm.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Indica si algun desglose del formulario de la solicitud tiene vinculaciones a otro/s desglose/s
        ''' </summary>
        ''' <param name="lIdInstancia">Identificador de la solicitud</param>
        ''' <returns>si algun desglose del formulario de la solicitud tiene vinculaciones o no </returns>
        ''' <remarks>Llamada desde: Solicitud.vb\ExisteVinculaciones; Tiempo maximo: 0,1</remarks>
        Public Function Instancia_ExisteVinculaciones(ByVal lCiaComp As Long, ByVal lIdInstancia As Long) As Boolean
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm = New SqlCommand
                cm.Connection = cn
                cm.CommandText = "FSPM_DEVOLVER_NUM_VINCULACIONES"
                cm.Parameters.AddWithValue("@INSTANCIA", lIdInstancia)
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)

                Return (dr.Tables(0).Rows(0).Item("NUM") > 0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Funcion que Comprueba el Estado En proceso de una instancia
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="InstanciaID">Instancia</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <remarks>Llamada desde: Instancia.vb\ComprobarEnProceso ; Tiempo m�ximo: 0,2</remarks>
        Public Function Instancia_ComprobarEnProceso(ByVal lCiaComp As Long, ByVal InstanciaID As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Integer
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As SqlDataReader
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSQA_EN_PROCESO"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@ID", InstanciaID)
                dr = cm.ExecuteReader()
                If dr.Read() Then Instancia_ComprobarEnProceso = dr.Item("EN_PROCESO")
            Catch e As Exception
                Throw e
            Finally
                If Not dr Is Nothing AndAlso Not dr.IsClosed Then
                    dr.Close()
                End If
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Actualiza el campo EN_PROCESO de la tabla INSTANCIA
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="iEnProceso">valor para en_proceso</param>
        ''' <param name="iBloque">Bloque</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb\Actualizar_En_proceso ; Tiempo m�ximo: 0,2</remarks>
        Public Sub Instancia_Actualizar_En_Proceso(ByVal lCiaComp As Long, ByVal lInstancia As Long, ByVal iEnProceso As Integer, Optional ByVal iBloque As Integer = Nothing,
                                                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")        '
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm = New SqlCommand("BEGIN TRANSACTION", cn)
                cm.ExecuteNonQuery()
                With cm
                    .Connection = cn
                    .CommandType = CommandType.StoredProcedure
                    .CommandText = FSGS(lCiaComp) & "FSPM_ACTUALIZAR_EN_PROCESO"
                    .Parameters.AddWithValue("@ID", lInstancia)
                    .Parameters.AddWithValue("@EN_PROCESO", iEnProceso)
                    .Parameters.AddWithValue("@BLOQUE", iBloque)
                    .ExecuteNonQuery()
                End With

                cm = New SqlCommand("COMMIT TRANSACTION", cn)
                cm.ExecuteNonQuery()
            Catch e As SqlException
                cm = New SqlCommand("ROLLBACK TRANSACTION", cn)
                cm.ExecuteNonQuery()
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Sub
        ''' <summary>
        ''' Nos devuelva de las tablas INSTANCIA y VERSION_INSTANCIA el valor de los campos EN_PROCESO, 
        ''' NUM_VERSION,  TIPO de la �ltima versi�n de VERSION_INSTANCIA y si el Certificado sigue siendo 
        ''' el activo para el proveedor (Si existe en la tabla PROVE_CERTIF)
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="RequestID">Id de instancia</param>
        ''' <param name="lCertificado">Id de certificado</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Dataset con los datos indicados</returns>
        ''' <remarks>Llamada desde: Instancia.ComprobarGrabacionCertif; Tiempo m�ximo: 0,1</remarks>
        Public Function Instancia_ComprobarGrabacionCertif(ByVal lCiaComp As Long, ByVal RequestID As Long, ByVal lCertificado As Long,
                                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSQA_DATOS_GRABAR_CERTIF"
                cm.Parameters.AddWithValue("@INSTANCIA", RequestID)
                cm.Parameters.AddWithValue("@CERTIFICADO", lCertificado)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Nos devuelva de las tablas INSTANCIA y VERSION_INSTANCIA el valor de los campos EN_PROCESO, 
        ''' NUM_VERSION,  TIPO de la �ltima versi�n de VERSION_INSTANCIA 
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="RequestID">Id de instancia</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Dataset con los datos indicados</returns>
        ''' <remarks>Llamada desde: Instancia.ComprobarGrabacionNC; Tiempo m�ximo: 0,1</remarks>
        Public Function Instancia_ComprobarGrabacionNC(ByVal lCiaComp As Long, ByVal RequestID As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSQA_DATOS_GRABAR_NC"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@INSTANCIA", RequestID)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Los calculados en un desglose hacen uso de un campo fuera del desglose, esta funci�n lo obtiene aunque este no visible
        ''' </summary>
        ''' <param name="lCiaComp">codigo de compa�ia</param>
        ''' <param name="lId">Instancia</param>
        ''' <param name="lIdCampo">id del copia_campo q es el desglose, es decir, campo_padre de copia_linea_desglose</param>        
        ''' <param name="lVersion">Version</param>
        ''' <param name="sIdi">Idioma</param> 
        ''' <param name="sUsuario">Usuario de la empresa de proveedor q esta dando de alta</param>
        ''' <param name="sProve">Proveedor</param>   
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>   
        ''' <returns>Dataset con el/los campos (no de desglose) q usa el desglose con id lIdCampo</returns>
        ''' <remarks>Llamada desde: Instancia.vb\LoadDesglosePadreVisible; Tiempo m�ximo: 0,1</remarks>
        Public Function Instancia_LoadDesglosePadreVisible(ByVal lCiaComp As Long, ByVal lId As Long, ByVal lIdCampo As Integer, ByVal lVersion As Long,
                                                           Optional ByVal sIdi As String = Nothing, Optional ByVal sUsuario As String = Nothing, Optional ByVal sProve As String = Nothing,
                                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_LOADINSTDESGLOSEPADREVISIBLE"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@INSTANCIA", lId)
                cm.Parameters.AddWithValue("@IDCAMPO", lIdCampo)
                cm.Parameters.AddWithValue("@VERSION", lVersion)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@USU", sUsuario)
                cm.Parameters.AddWithValue("@PROVE", sProve)
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function

        ''' <summary>
        ''' Control Vulnerabilidad ImpExp
        ''' </summary>
        ''' <param name="lCiaComp">Compania</param>
        ''' <param name="IdQa">Id certif/No Conf</param>
        ''' <param name="Proveedor">Proveedor</param>
        ''' <param name="Instancia">Instancia</param>
        ''' <param name="EsCertificado">1->certif/0->No Conf</param>
        ''' <returns></returns>
        Public Function Instancia_CtrlVulnerabilidad_ImpExp(ByVal lCiaComp As Long, ByVal IdQa As Long, ByVal Proveedor As String, ByVal Instancia As Long, ByVal EsCertificado As Boolean _
                                                            , Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Boolean
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim sGs As String
            Dim sSql As String
            Try
                cn.Open()
                cm.Connection = cn
                sGs = FSGS(lCiaComp)
                If EsCertificado Then
                    sSql = "SELECT 1 AS ACEPTABLE FROM " & sGs & "CERTIFICADO C WITH(NOLOCK) WHERE C.ID=" & DBNullToStr(IdQa) & " AND C.INSTANCIA=" & DBNullToStr(Instancia) & " AND C.PROVE=" & StrToSQLNULL(Proveedor) & " AND C.PUBLICADA=1"
                Else
                    sSql = "SELECT 1 AS ACEPTABLE FROM " & sGs & "NOCONFORMIDAD NC WITH(NOLOCK) WHERE NC.ID=" & DBNullToStr(IdQa) & " AND NC.INSTANCIA=" & DBNullToStr(Instancia) & " AND NC.PROVE=" & StrToSQLNULL(Proveedor)
                End If
                cm.CommandText = sSql
                cm.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                If dr.Tables(0).Rows.Count = 0 Then
                    Return False
                Else
                    Return True
                End If

            Catch ex As Exception
                Throw ex
            Finally
                cn.Close()
            End Try
        End Function

        ''' <summary>
        ''' Los calculados en un desglose hacen uso de un campo fuera del desglose, esta funci�n lo obtiene aunque este no visible
        ''' </summary>
        ''' <param name="lCiaComp">codigo de compa�ia</param>
        ''' <param name="lId">Id del formulario de la Solicitud q se esta dando de alta</param>        
        ''' <param name="lIdCampo">id del form_campo q es el desglose, es decir, campo_padre de linea_desglose</param>         
        ''' <param name="sIdi">Idioma</param> 
        ''' <param name="lIdSolicitud">Solicitud q se esta dando de alta</param>        
        ''' <param name="sPer">Persona de la empresa de proveedor q esta dando de alta</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param> 
        ''' <returns>Dataset con el/los campos (no de desglose) q usa el desglose con id lIdCampo</returns>
        ''' <remarks>Llamada desde: Formulario.vb\LoadDesglosePadreVisible; Tiempo m�ximo: 0,1</remarks>
        Public Function Formulario_LoadDesglosePadreVisible(ByVal lCiaComp As Long, ByVal lId As Long, ByVal lIdCampo As Integer, Optional ByVal sIdi As String = Nothing, Optional ByVal lIdSolicitud As Long = Nothing,
                                                            Optional ByVal sPer As String = Nothing, Optional ByVal sProve As String = Nothing, Optional ByVal idUsu As Integer = Nothing,
                                                            Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandText = FSGS(lCiaComp) & "FSPM_LOADDESGLOSEPADREVISIBLE"
                If sPer <> Nothing Then cm.Parameters.AddWithValue("@PER", sPer)
                If Not String.IsNullOrEmpty(sProve) Then cm.Parameters.AddWithValue("@PROVE", sProve)
                If Not String.IsNullOrEmpty(idUsu) Then cm.Parameters.AddWithValue("@CONTACTO", idUsu)
                cm.Parameters.AddWithValue("@FORMULARIO", lId)
                cm.Parameters.AddWithValue("@IDCAMPO", lIdCampo)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@SOLICITUD", lIdSolicitud)
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Actualiza en la instancia el campo de seguimiento para monitorizarla o no.
        ''' </summary>
        ''' <param name="lInstancia">id de la solicitud</param>
        ''' <param name="iSeg">(1/0) 1: se monitorizará</param>
        ''' <remarks>Llamada desde: PMServer --> Instancia.vb -->ActualizarMonitorizacion; Tiempo máximo: 0sg.</remarks>
        Public Sub Instancia_ActualizarMonitorizacion(ByVal lCiaComp As Long, ByVal lInstancia As Long, ByVal iSeg As Byte, ByVal sProve As String, ByVal sContacto As Long,
                                                      Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Try
                cn.Open()
                With cm
                    .Connection = cn
                    .CommandType = CommandType.StoredProcedure
                    .CommandText = FSGS(lCiaComp) & "FSPM_ACTUALIZAR_MONITORIZACION_PROVE"
                    .Parameters.AddWithValue("@ID", lInstancia)
                    .Parameters.AddWithValue("@SEG", iSeg)
                    .Parameters.AddWithValue("@PROVE", sProve)
                    .Parameters.AddWithValue("@IDCONTACTO", sContacto)
                    .ExecuteNonQuery()
                End With
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Sub
        ''' <summary>
        ''' Elimina la instancia de BD
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub EliminarInstancia(ByVal lCiaComp As Long, ByVal lInstancia As Long, Optional ByVal lIdfactura As Long = 0,
                                     Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Try
                cn.Open()
                If lIdfactura > 0 Then
                    With cm
                        .Connection = cn
                        .CommandType = CommandType.StoredProcedure
                        .CommandText = "FSPM_ELIMINAR_FACTURA"
                        .Parameters.AddWithValue("@CIA", lCiaComp)
                        .Parameters.AddWithValue("@ID", lIdfactura)
                        .ExecuteNonQuery()
                    End With
                    cm = New SqlCommand
                End If

                With cm
                    .Connection = cn
                    .CommandType = CommandType.StoredProcedure
                    .CommandText = "FSWS_ELIMINAR_INSTANCIA"
                    .Parameters.AddWithValue("@CIA", lCiaComp)
                    .Parameters.AddWithValue("@ID", lInstancia)
                    .ExecuteNonQuery()
                End With
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Sub
        ''' <summary>
        ''' Funci�n que devuelve la denominaci�n de la etapa en curso de la solicitud.
        ''' </summary>
        ''' <param name="lInstancia">Id de la instancia</param>
        ''' <param name="sIdioma">Idioma del usuario</param>
        ''' <returns>Devuelve la denominaci�n de la etapa en curso de la solicitud</returns>
        ''' <remarks>Llamada desde:NWdetalleSolicitud.aspx.vb Tiempo m�ximo: 0,1</remarks>
        Public Function Instancia_CargarEstapaActual(ByVal lCiaComp As Long, ByVal lInstancia As Long, ByVal sIdioma As String, ByVal bLitEstado As Boolean,
                                                     Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As String
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim sServidorGS As String
            Try
                sServidorGS = FSGS(lCiaComp)
                cm.Connection = cn
                cm.CommandText = "Select BD.DEN FROM " & sServidorGS & "INSTANCIA_BLOQUE IB With (NOLOCK) INNER JOIN " & sServidorGS & "PM_COPIA_BLOQUE_DEN BD With(NOLOCK) " &
                " On IB.BLOQUE=BD.BLOQUE WHERE INSTANCIA = " & lInstancia & " And BD.IDI='" & DblQuote(sIdioma) & "' AND ESTADO = 1"
                cm.CommandType = CommandType.Text
                da.SelectCommand = cm
                da.Fill(dr)
                If dr.Tables(0).Rows.Count > 1 Then
                    If bLitEstado = True Then
                        Instancia_CargarEstapaActual = "##"
                    Else
                        Instancia_CargarEstapaActual = DBNullToStr(dr.Tables(0).Rows(0).Item("DEN"))
                    End If
                ElseIf dr.Tables(0).Rows.Count > 0 Then
                    Instancia_CargarEstapaActual = DBNullToStr(dr.Tables(0).Rows(0).Item("DEN"))
                Else
                    Instancia_CargarEstapaActual = Nothing
                End If
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Function Instancia_ComprobarLineasAsociadas(ByVal lCiaComp As Long, ByVal idDesglose As Long, ByVal numLinea As Integer,
                                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Boolean
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As SqlDataReader
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_COMPROBAR_LINEA_ELIMINAR"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDDESGLOSE", idDesglose)
                cm.Parameters.AddWithValue("@NUMLINEA", numLinea)
                dr = cm.ExecuteReader()

                If dr.Read() Then Instancia_ComprobarLineasAsociadas = dr.Item("RESULT")
            Catch e As Exception
                Throw e
            Finally
                If Not dr Is Nothing AndAlso Not dr.IsClosed Then
                    dr.Close()
                End If
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Actualiza el tiempo de procesamiento de las solicitudes
        ''' </summary>
        ''' <param name="lID">Id (campo identity). Si no tiene nada hay que hacer una insert, si no, una update.</param>
        ''' <param name="lInstancia">Id de la instancia que se est� procesando</param>
        ''' <param name="sCodUsuario">Cod. del usuario</param>
        ''' <param name="iFecha">Fecha que hay que actualizar (inicio, fin,..)</param>
        ''' <remarks>Llamada desde: PMServer --> Instancia.vb -->ActualizarTiempoProcesamiento; Tiempo m�ximo: 0sg.</remarks>
        Public Sub Instancia_ActualizarTiempoProcesamiento(ByVal lCiaComp As Long, Optional ByRef lID As Long = 0, Optional ByVal lInstancia As Long = 0,
                                                           Optional ByVal sCodUsuario As String = Nothing, Optional ByVal lBloque As Long = 0, Optional ByVal iFecha As Short = 0,
                                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Try
                cn.Open()
                With cm
                    .Connection = cn
                    .CommandType = CommandType.StoredProcedure
                    .CommandText = FSGS(lCiaComp) & "FSWS_ACTUALIZAR_TIEMPO_PROC"
                    If Not lInstancia = 0 Then .Parameters.AddWithValue("@INSTANCIA", lInstancia)
                    If Not String.IsNullOrEmpty(sCodUsuario) Then .Parameters.AddWithValue("@USUARIO", sCodUsuario)
                    .Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.InputOutput
                    If Not lID = 0 Then
                        .Parameters("@ID").Value = lID
                        .Parameters.AddWithValue("@CAMPOFECHA", iFecha)
                    End If
                    If Not lBloque = 0 Then .Parameters.AddWithValue("@BLOQUE", lBloque)
                    .ExecuteNonQuery()

                    If lID = Nothing Then lID = .Parameters("@ID").Value
                End With
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Sub
        ''' <summary>Devuelve los �tems de una instancia</summary>
        ''' <param name="lCiaComp">Id de la cia</param>
        ''' <param name="IdInstancia">Id de la instancia</param>
        ''' <returns>datatable con los items</returns>
        ''' <remarks>Llamada desde: Instancia.DevolverItems</remarks>
        Public Function Instancia_DevolverItems(ByVal lCiaComp As Long, ByVal IdInstancia As Integer, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Dim sServidorGS As String

            Try
                sServidorGS = FSGS(lCiaComp)
                cn.Open()
                cm.Connection = cn
                cm.CommandText = sServidorGS & "FSPM_DEVOLVER_ITEMS_INSTANCIA"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDINSTANCIA", IdInstancia)
                da.SelectCommand = cm
                da.Fill(ds)
                Return ds.Tables(0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' funcion que devuelve los items de un proceso de solicitud
        ''' </summary>
        ''' <param name="lInstancia">Id de la instancia</param>
        ''' <returns>Un DataSet con los datos de los items de un proceso de solicitud</returns>
        ''' <remarks>
        ''' Llamada desde: PmServer/Instancia/ItemsprocesoSolicitud
        ''' Tiempo m�ximo: 0,6 seg</remarks>
        Public Function Instancia_ItemsProcesoSolicitud(ByVal lCiaComp As Integer, ByVal lInstancia As Long,
                                                     Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSEP_DEVOLVER_PROCESOS_SOLICITUD"
                cm.Parameters.AddWithValue("@ID", lInstancia)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region " Linea data access methods"
        ''' <summary>
        ''' Función que devolverá un dataset con los pagos asociados a facturas aociadas a su vez a una línea de pedido. Si no se recibe línea de pedido devuelve todas las líneas de pedido y sus pagos.
        ''' </summary>
        ''' <param name="idioma">Idioma en el que se quieren recuperar los datos en multiidioma</param>
        ''' <param name="IdLinea">Integer. Id de la línea de pedido</param>
        ''' <returns>Dataset con los pagos asociados a facturas aociadas a su vez a una línea de pedido. 
        ''' Si no se recibe línea de pedido devuelve todas las líneas de pedido y sus pagos.</returns>
        ''' <remarks></remarks>
        Public Function Linea_DevolverPagos(ByVal lCiaComp As Long, ByVal idioma As String, Optional ByVal IdLinea As Integer = 0, Optional ByVal lIdFactura As Long = 0) As DataSet
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Using cn
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandText = FSGS(lCiaComp) & "FSEP_GET_PAGOS"
                cm.Parameters.AddWithValue("IDIOMA", idioma)
                If Not IsNothing(IdLinea) And IdLinea <> 0 Then
                    cm.Parameters.AddWithValue("LINEA_PEDIDO", IdLinea)
                Else
                    cm.Parameters.AddWithValue("LINEA_PEDIDO", DBNull.Value)
                End If
                If Not IsNothing(lIdFactura) And lIdFactura <> 0 Then
                    cm.Parameters.AddWithValue("FACTURA", lIdFactura)
                Else
                    cm.Parameters.AddWithValue("FACTURA", DBNull.Value)
                End If
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr
            End Using
        End Function
#End Region
#Region " No conformidad Access Methods "
        ''' <summary>
        ''' Carga la NoConformidad
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa��a</param>
        ''' <param name="sProve">Codigo de Porveedor</param>
        ''' <param name="lId">Id de no conformidad</param>
        ''' <param name="lVersion">version a cargar</param>     
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="bPortal">Ultima grabaci�n desde portal o qa</param>
        ''' <param name="DesdeImpExp">Solo desde Imprimir/Exportar se usa Coment Alta y Ultima accion por separado, luego, dos select</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>   
        ''' <remarks>Llamada desde:NoConformidad.vb\Load; Tiempo m�ximo: 0,1</remarks>
        Public Function NoConformidad_Load(ByVal lCiaComp As Long, ByVal sProve As String, Optional ByVal lId As Long = Nothing, Optional ByVal lVersion As Long = Nothing,
                                           Optional ByVal sIdi As String = "SPA", Optional ByRef bPortal As Boolean = False, Optional ByVal DesdeImpExp As Boolean = False,
                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSQA_LOADNOCONFORMIDAD"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@NOCONFORMIDAD", lId)
                cm.Parameters.AddWithValue("@VERSION", lVersion)
                cm.Parameters.AddWithValue("@PROVE", sProve)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@PORTAL", SqlDbType.TinyInt)
                cm.Parameters("@PORTAL").Direction = ParameterDirection.Output
                cm.Parameters.AddWithValue("@IMPEXP_COMEN_ALTA", DesdeImpExp)
                da.SelectCommand = cm
                da.Fill(dr)

                bPortal = SQLBinaryToBoolean(cm.Parameters("@PORTAL").Value)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Procedimiento que carga datos de quien realizo las diferentes versiones(respuestas) de una no conformidad
        ''' </summary>
        ''' <param name="lCiaComp">El id de la compa�ia para obtener donde esta la base de datos de la compa�ia</param>
        ''' <param name="sProve">C�digo del proveedor en el portal</param>
        ''' <param name="lId">Id de no conformidad</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <remarks>Llamada desde: Server/NoConformidad/CargarRespuestas; Tiempo m�ximo:0,3 seg </remarks>
        Public Function NoConformidad_CargarRespuestas(ByVal lCiaComp As Long, ByVal sProve As String, Optional ByVal lId As Long = Nothing, Optional ByVal sIdi As String = "SPA",
                                                       Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSQA_LOADRESPUESTASNOCONFORMIDAD"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@PROVE", sProve)
                cm.Parameters.AddWithValue("@NOCONFORMIDAD", lId)
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Carga los id de copia_campo para la version actual y para la version q se le pasa como parametro
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="lId">Instancia</param>
        ''' <param name="iVersion">Version</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <remarks>Llamada desde: CargarCorrespondenciaCampos.vb\CargarCorrespondenciaCampos ; Tiempo m�ximo: 0,2</remarks>
        Public Function NoConformidad_CargarCorrespondenciaCampos(ByVal lCiaComp As Long, ByVal lId As Long, ByVal iVersion As Integer,
                    Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSQA_LOADCORRESPONDENCIACAMPOS"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CERTIFICADO", lId)
                cm.Parameters.AddWithValue("@VERSION", iVersion)
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Lista de solicitudes q tiene un usuario.
        ''' </summary>
        ''' <param name="iCia">Id de la compa�ia</param> 
        ''' <param name="sUsu">usuario</param>
        ''' <param name="sIdi">idioma en q ver los textos</param>
        ''' <param name="bEsCombo">sql para cargar combo o no</param>
        ''' <param name="iTipoSolicitud">tipo de solicitud</param>
        ''' <param name="bSeguimiento">la carga del combo de seguimiento mira en vista de peticionario por lo de definir un rol como lista/departamento/etc. El resto mira instancia.peticionario</param>
        ''' <param name="bNuevoWorkfl">true PM/false QA</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>   
        ''' <remarks>Llamada desde: NoConformidades.vb\Load_Tipo_NoConformidades; Tiempo m�ximo:0,1</remarks>
        Public Function Tipo_NoConformidades(ByVal iCia As Integer, ByVal sUsu As String, Optional ByVal sIdi As String = "SPA", Optional ByVal bEsCombo As Integer = 0,
                                             Optional ByVal iTipoSolicitud As Integer = Nothing, Optional ByVal bSeguimiento As Integer = 0, Optional ByVal bNuevoWorkfl As Boolean = False,
                                             Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSQA_GET_TIPONOCONFORMIDADES"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", iCia)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@USU", sUsu)
                cm.Parameters.AddWithValue("@COMBO", bEsCombo)
                If Not iTipoSolicitud = Nothing Then cm.Parameters.AddWithValue("@TIPO", iTipoSolicitud)
                cm.Parameters.AddWithValue("@DESDESEGUIMIENTO", bSeguimiento)
                If bNuevoWorkfl Then cm.Parameters.AddWithValue("@NUEVO_WORKFL", 1)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' Revisado por: Jbg; Fecha: 27/11/2011
        ''' <summary>
        ''' Cargar las no conformidades cerradas de un proveedor seg�n los parametros dados. Se le a�aden un par de 
        ''' columnas para temas javascript de comilla simple, doble y saltos de linea.
        ''' </summary>
        ''' <param name="iCia">Id de la compa�ia</param> 
        ''' <param name="sProve">Proveedor</param>        
        ''' <param name="lId">Id de la No conformidad</param>  
        ''' <param name="lTipo">Tipo de No conformidad</param>  
        ''' <param name="sFecha">Fecha apertura desde de No conformidad</param>  
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>   
        ''' <returns>Dataset con las no conformidades cerradas</returns>
        ''' <remarks>Llamada desde: NoConformidades.vb\Load_Data_NoConformidades_Cerradas; Tiempo m�ximo:0,1</remarks>
        Public Function Data_NoConformidades_Cerradas(ByVal iCia As Integer, ByVal EmpresaPortal As Boolean, ByVal Nom_Portal As String,
                                                      Optional ByVal sProve As String = Nothing, Optional ByVal lId As Long = 0,
                                                      Optional ByVal lTipo As Long = 0, Optional ByVal sFecha As String = Nothing,
                                                      Optional ByVal sIdi As String = Nothing,
                                                      Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSQA_LOADNOCONFORMIDADESCERRADAS"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", iCia)
                cm.Parameters.AddWithValue("@PROVE", sProve)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@ID", lId)
                cm.Parameters.AddWithValue("@TIPO", lTipo)
                cm.Parameters.AddWithValue("@FECHA", sFecha)
                If EmpresaPortal Then cm.Parameters.AddWithValue("@NOM_PORTAL", Nom_Portal)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                dr.Tables(0).Columns.Add("RCOMENT_CIERRE", System.Type.GetType("System.String"))
                dr.Tables(0).Columns.Add("RCOMENT_ALTA", System.Type.GetType("System.String"))
                dr.Tables(0).Columns.Add("COMENT_IMPR", System.Type.GetType("System.String"))

                dr.Tables(0).Columns.Add("ID_NOCONF_ENCRYPTED", System.Type.GetType("System.String"))
                dr.Tables(0).Columns.Add("ID_INS_ENCRYPTED", System.Type.GetType("System.String"))

                For Each row As DataRow In dr.Tables(0).Rows
                    Select Case DBNullToStr(row("CIERRE"))
                        Case "2"
                            row("CIERRE_DEN") = dr.Tables(1).Rows(0).Item("DEN")
                        Case "3"
                            row("CIERRE_DEN") = dr.Tables(1).Rows(1).Item("DEN")
                        Case Else
                            row("CIERRE_DEN") = dr.Tables(1).Rows(2).Item("DEN")
                    End Select
                Next
                dr.Tables.RemoveAt(1)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' carga todos los contactos que tiene el proveedor, y se�ala cuales estan seleccionados para notificar la emisi�n y cuales no
        ''' </summary>
        ''' <param name="lIdNoConformidad">NoConformida</param>
        ''' <param name="CodProve">Proveedor</param>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\NoConformidad.vb\Load_Notificados_Proveedor ; Tiempo m�ximo: 0,2</remarks>
        Public Function Load_Notificados_Proveedor(ByVal lIdNoConformidad As Long, ByVal CodProve As String, ByVal lCiaComp As Long,
                                                   Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm = New SqlCommand
                cm.Connection = cn
                cm.CommandType = CommandType.Text
                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "FSQA_NOTIFICADOS_PROVEEDOR @IDNOCONFORMIDAD, @PROVE"
                cm.Parameters.AddWithValue("@IDNOCONFORMIDAD", lIdNoConformidad)
                cm.Parameters.AddWithValue("@PROVE", CodProve)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Guarda los cambios realizados en los notificados del proveedor
        ''' </summary>
        ''' <param name="lIdNoConformidad">Id de la noconformidad</param>
        ''' <param name="CodProve">C�digo del proveedor de la noconformidad</param>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sNotificadosProveedor">Cadena de los notificados seleccionados</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <remarks>Llamada desde: PMPortalServer\NoConformidad.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Sub Guardar_Notificados_Proveedor(ByVal lIdNoConformidad As Long, ByVal CodProve As String, ByVal lCiaComp As Long, ByVal sNotificadosProveedor As String,
                                                 Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim iErrorVar As Integer
            Try
                cn.Open()
                cm = New SqlCommand
                cm.Connection = cn
                cm.CommandType = CommandType.Text
                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "FSQA_GUARDAR_NOTIFICADOS_PROVEEDOR @IDNOCONFORMIDAD, @PROVE, @NOTIFICADOSPROVEEDOR, @ERRORVAR"
                cm.Parameters.AddWithValue("@IDNOCONFORMIDAD", lIdNoConformidad)
                cm.Parameters.AddWithValue("@PROVE", CodProve)
                cm.Parameters.AddWithValue("@NOTIFICADOSPROVEEDOR", sNotificadosProveedor)
                cm.Parameters.AddWithValue("@ERRORVAR", SqlDbType.Int).Direction = ParameterDirection.InputOutput
                cm.Parameters("@ERRORVAR").Value = iErrorVar
                cm.ExecuteNonQuery()

                iErrorVar = cm.Parameters("@ERRORVAR").Value
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Sub
        ''' <summary>
        ''' Procedimiento que carga el ultimo comentario de cierree de una no conformidad dado su id
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa��a</param>
        ''' <param name="lIdNoConformidad">Identificador de la no conformidad</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>   
        ''' <remarks>
        ''' Llamada desde: PmPortalServer/NoConformidad/CargarComentarioCierre
        ''' Tiempo m�ximo: 0,1 seg</remarks>
        Public Function NoConformidad_LoadComentCierre(ByVal lCiaComp As Long, ByVal lIdNoConformidad As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.Text
                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "FSQA_LOADNOCONFORMIDAD_COMENTCIERRE @NOCONFORMIDAD"
                cm.Parameters.AddWithValue("@NOCONFORMIDAD", lIdNoConformidad)
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region " Notificador data access methods "
        ''' Revisado por: jbg; Fecha: 04/07/2013
        ''' <summary>
        ''' Devolver Autenticacion
        ''' </summary>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalNotificador\Notificar.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Function Notificador_DevolverAutenticacion(Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm = New SqlCommand
                cm.Connection = cn
                cm.CommandText = "SELECT P_SERVIDOR, P_PUERTO, P_AUTENTICACION, P_FROM, P_USU, P_PWD, P_SSL, P_METODO_ENTREGA, P_DOMINIO, P_FROMNAME FROM PARGEN_MAIL WITH (NOLOCK) WHERE ID=1"
                cm.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Organizacion de Compras data access methods "
        ''' <summary>
        '''  carga la organizacion
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCod">codigo de organizacion</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\OrganizacionCompras.vb\LoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function OrganizacionCompras_Get(ByVal lCiaComp As Long, ByVal sCod As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm = New SqlCommand
                cm.Connection = cn
                cm.CommandText = "SELECT COD,DEN FROM " & FSGS(lCiaComp) & "ORGCOMPRAS WITH (NOLOCK) WHERE COD = '" & sCod & "'"
                cm.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Organizaciones de Compras data access methods "
        ''' <summary>
        ''' carga las organizaciones
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\OrganizacionesCompras.vb\LoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function OrganizacionesCompras_Load(ByVal lCiaComp As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm = New SqlCommand
                cm.Connection = cn
                cm.CommandText = "FSPM_GETORGANIZACIONESCOMPRAS"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Paises data access methods"
        ''' <summary>
        ''' Obtiene los pa�ses
        ''' </summary>
        ''' <param name="sIdioma">Idioma del usuario</param>
        ''' <param name="lCiaComp">C�digo de la compa��a</param>
        ''' <param name="sCod">C�digo del pa�s</param>
        ''' <param name="sDen">Denominaci�n del pa�s o parte de ella</param>
        ''' <param name="bCoincid">Si coincide exactamente o se hace un LIKE</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <remarks>Llamada desde: PMPortalServer --> Paises.vb --> LoadData; Tiempo m�ximo: 1sg</remarks>
        Public Function Paises_Get(ByVal sIdioma As String, ByVal lCiaComp As Long, ByVal sCod As String, ByVal sDen As String, ByVal bCoincid As Boolean,
                                   Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_PAISES"
                cm.Parameters.AddWithValue("@IDIOMA", sIdioma)
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@COD", sCod)
                cm.Parameters.AddWithValue("@DEN", sDen)
                cm.Parameters.AddWithValue("@COINCID", IIf(bCoincid, 1, 0))
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Participantes data access methods"
        ''' <summary>
        ''' carga los datos del participante
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lRol">Rol</param>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="sNom">Nombre </param>
        ''' <param name="sApe">apellido</param>
        ''' <param name="sUON1">Unidad organizativa de nivel 1</param>
        ''' <param name="sUON2">Unidad organizativa de nivel 2</param>
        ''' <param name="sUON3">Unidad organizativa de nivel 3</param>
        ''' <param name="sDep">Departamento</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Participantes.vb\LoadDataPersonas ; Tiempo m�ximo: 0,2</remarks>
        Public Function Participantes_Persona_Load(ByVal lCiaComp As Long, ByVal lRol As Integer, Optional ByVal lInstancia As Integer = Nothing, Optional ByVal sNom As String = Nothing, Optional ByVal sApe As String = Nothing,
                                                   Optional ByVal sUON1 As String = Nothing, Optional ByVal sUON2 As String = Nothing, Optional ByVal sUON3 As String = Nothing, Optional ByVal sDep As String = Nothing,
                                                   Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_PARTICIPANTES_PER"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@ROL", lRol)
                cm.Parameters.AddWithValue("@INSTANCIA", lInstancia)
                cm.Parameters.AddWithValue("@FILNOM", sNom)
                cm.Parameters.AddWithValue("@FILAPE", sApe)
                cm.Parameters.AddWithValue("@FILUON1", sUON1)
                cm.Parameters.AddWithValue("@FILUON2", sUON2)
                cm.Parameters.AddWithValue("@FILUON3", sUON3)
                cm.Parameters.AddWithValue("@FILDEP", sDep)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region "Partida data access methods"
        ''' <summary>
        ''' Obtiene el detalle de una partida (cod, denominaci�n, centro, fechas de inicio y de fin)
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="sPRES5">Cod partida presupuestaria</param>
        ''' <param name="sTexto">Cadena que contiene las uons y los c�digos de pres5</param>
        ''' <param name="sIdioma">Idioma del usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>El detalle de la partida</returns>
        ''' <remarks>Llamada desde: Partida.vb --> BuscarDetallePartida; Tiempo m�ximo: 1 sg;</remarks>
        Public Function Partida_Detalle(ByVal lCiaComp As Long, ByVal sPRES5 As String, ByVal sTexto As String, ByVal sIdioma As String,
                                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSSM_DEVOLVER_DETALLE_PARTIDA"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@IDI", sIdioma)
                cm.Parameters.AddWithValue("@PRES0", sPRES5)
                If sTexto <> Nothing Then
                    Dim sPartida As String = Right(sTexto, Len(sTexto) - InStrRev(sTexto, "#"))
                    'quitamos la partida de la cadena para que solo queden las uons
                    sTexto = Left(sTexto, InStrRev(sTexto, "#") - 1)
                    Dim sUONs As Object = Split(sTexto, "#")
                    cm.Parameters.AddWithValue("@UON1", sUONs(0))
                    If UBound(sUONs) >= 1 Then
                        cm.Parameters.AddWithValue("@UON2", sUONs(1))
                        If UBound(sUONs) >= 2 Then
                            cm.Parameters.AddWithValue("@UON3", sUONs(2))
                            If UBound(sUONs) = 3 Then
                                cm.Parameters.AddWithValue("@UON4", sUONs(3))
                            End If
                        End If
                    End If
                    Dim sPartidas As Object = Split(sPartida, "|")
                    cm.Parameters.AddWithValue("@PRES1", sPartidas(0))
                    If UBound(sPartidas) >= 1 Then
                        cm.Parameters.AddWithValue("@PRES2", sPartidas(1))
                        If UBound(sPartidas) >= 2 Then
                            cm.Parameters.AddWithValue("@PRES3", sPartidas(2))
                            If UBound(sPartidas) = 3 Then
                                cm.Parameters.AddWithValue("@PRES4", sPartidas(3))
                            End If
                        End If
                    End If
                End If
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region "A�o Partida data access methods"
        Public Function AnyoPartida_Load(ByVal lCiaComp As Long, ByVal sPartida0 As String, ByVal sPartida As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand()
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm = New SqlCommand

                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSSM_BUSCAR_ANYOPARTIDA"
                cm.Parameters.AddWithValue("@PRES0", sPartida0)
                cm.Parameters.AddWithValue("@PRES", sPartida)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region " Personas data access methods"
        ''' <summary>Procedimiento que carga los datos de una persona</summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCod">C�digo de la persona</param>   
        ''' <param name="bBajaLog">Incluir o no bajas l�gicas</param>
        ''' <remarks>Llamada desde: FSNServer.Persona.LoadData</remarks>
        Public Function Persona_Load(ByVal lCiaComp As Long, ByVal sCod As String, Optional ByVal bBajaLog As Boolean = False, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSWS_DEVOLVER_DATOS_PERSONA"
                cm.Parameters.AddWithValue("@COD", sCod)
                cm.Parameters.AddWithValue("@BAJALOG", bBajaLog)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(ds)

                Return ds.Tables(0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Carga los datos de personas
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCod">Codigo de persona</param>
        ''' <param name="sNom">Nombre de persona</param>
        ''' <param name="sApe">Apellidos de persona</param>
        ''' <param name="sUON1">Unidad organizativa de nivel 1</param>
        ''' <param name="sUON2">Unidad organizativa de nivel 2</param>
        ''' <param name="sUON3">Unidad organizativa de nivel 3</param>
        ''' <param name="sDEP">Departamento</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Personas.vb\LoadData    PMPortalServer\User.vb\LoadUONPersona ; Tiempo m�ximo: 0,2</remarks>
        Public Function Personas_Load(ByVal lCiaComp As Long, Optional ByVal sCod As String = Nothing, Optional ByVal sNom As String = Nothing, Optional ByVal sApe As String = Nothing,
                                      Optional ByVal sUON1 As String = Nothing, Optional ByVal sUON2 As String = Nothing, Optional ByVal sUON3 As String = Nothing, Optional ByVal sDEP As String = Nothing,
                                      Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "", Optional ByVal bCtlBajaLog As Boolean = True) As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSWS_PERSONAS"
                cm.Parameters.AddWithValue("@COD", sCod)
                cm.Parameters.AddWithValue("@NOM", sNom)
                cm.Parameters.AddWithValue("@APE", sApe)
                cm.Parameters.AddWithValue("@UON1", sUON1)
                cm.Parameters.AddWithValue("@UON2", sUON2)
                cm.Parameters.AddWithValue("@UON3", sUON3)
                cm.Parameters.AddWithValue("@DEP", sDEP)
                cm.Parameters.AddWithValue("@BAJALOG", IIf(bCtlBajaLog, 1, 0))
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Carga los datos del Receptor
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCod">Codigo del receptor</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Personas.vb\LoadDataReceptor ; Tiempo m�ximo: 0,2</remarks>
        Public Function Personas_LoadReceptor(ByVal lCiaComp As Long, ByVal sCod As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSEP_OBT_DATOS_RECEPTOR"
                cm.Parameters.AddWithValue("@RECEPTOR", sCod)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr

        End Function
        ''' <summary>
        ''' Carga los datos de la Factura
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="facturaId">Id de la factura</param>
        ''' <param name="sIdioma">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Factura.vb\LoadDataFactura ; Tiempo m�ximo: </remarks>
        Public Function Factura_LoadFactura(ByVal lCiaComp As Long, ByVal facturaId As Integer, Optional ByVal sIdioma As String = "", Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSEP_OBT_DATOS_FACTURA"
                cm.Parameters.AddWithValue("@FACTURAID", facturaId)
                cm.Parameters.AddWithValue("@IDIOMA", sIdioma)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Presupuestos data access methods "
        ''' <summary>
        ''' Carga el arbol de presupuestos
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iTipo">Tipo de presupuesto</param>
        ''' <param name="EsUONVacia">Si los parametros uon q se pasan son todos vacios o no</param>
        ''' <param name="iAnyo">A�o del presupuesto</param>
        ''' <param name="sUON1">Unidad organizativa de nivel 1</param>
        ''' <param name="sUON2">Unidad organizativa de nivel 2</param>
        ''' <param name="sUON3">Unidad organizativa de nivel 3</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="Codigo">Codigo del presupuesto</param>
        ''' <param name="Denominacion">Denominacion del presupuesto</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\UnidadesOrg.vb\CargarPresupuestos ; Tiempo m�ximo: 0,2</remarks>
        Public Function Presupuestos_Get(ByVal lCiaComp As Long, ByVal iTipo As Integer, ByVal EsUONVacia As Integer, Optional ByVal iAnyo As Integer = Nothing, Optional ByVal sUon1 As String = Nothing, Optional ByVal sUon2 As String = Nothing, Optional ByVal sUon3 As String = Nothing, Optional ByVal sIdi As String = "SPA", Optional ByVal Codigo As String = Nothing, Optional ByVal Denominacion As String = Nothing, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_ARBOLPRESUPUESTOS"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@TIPO", iTipo)
                cm.Parameters.AddWithValue("@UONVacia", EsUONVacia)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@ANYO", iAnyo)
                cm.Parameters.AddWithValue("@UON1", sUon1)
                cm.Parameters.AddWithValue("@UON2", sUon2)
                cm.Parameters.AddWithValue("@UON3", sUon3)
                cm.Parameters.AddWithValue("@CODIGO", Codigo)
                cm.Parameters.AddWithValue("@DENOMINACION", Denominacion)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)

                Dim parentCols(0) As DataColumn
                Dim childCols(0) As DataColumn
                parentCols(0) = dr.Tables(0).Columns("COD")
                childCols(0) = dr.Tables(1).Columns("PRES0")

                dr.Relations.Add("REL_NIV0_NIV1", parentCols, childCols, False)

                parentCols(0) = dr.Tables(1).Columns("COD")
                childCols(0) = dr.Tables(2).Columns("PRES1")

                dr.Relations.Add("REL_NIV1_NIV2", parentCols, childCols, False)

                ReDim parentCols(1)
                ReDim childCols(1)
                parentCols(0) = dr.Tables(2).Columns("PRES1")
                parentCols(1) = dr.Tables(2).Columns("COD")
                childCols(0) = dr.Tables(3).Columns("PRES1")
                childCols(1) = dr.Tables(3).Columns("PRES2")
                dr.Relations.Add("REL_NIV2_NIV3", parentCols, childCols, False)

                ReDim parentCols(2)
                ReDim childCols(2)
                parentCols(0) = dr.Tables(3).Columns("PRES1")
                parentCols(1) = dr.Tables(3).Columns("PRES2")
                parentCols(2) = dr.Tables(3).Columns("COD")
                childCols(0) = dr.Tables(4).Columns("PRES1")
                childCols(1) = dr.Tables(4).Columns("PRES2")
                childCols(2) = dr.Tables(4).Columns("PRES3")

                dr.Relations.Add("REL_NIV3_NIV4", parentCols, childCols, False)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Cargar Unidades Organizativas
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="sUsu">Usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\UnidadesOrg.vb\CargarUnidadesOrganizativas ; Tiempo m�ximo: 0,2</remarks>
        Public Function UnidadesOrg_DevolverUO(ByVal lCiaComp As Long, Optional ByVal sIdi As String = "SPA", Optional ByVal sUsu As String = Nothing,
                                               Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_DEVOLVER_UO"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@USU", sUsu)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Devuelve el nombre (pargen_lit.den) del presupeuesto
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iTipo">Tipo de presupuesto</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\UnidadesOrg.vb\CargarCadenaPresupuesto ; Tiempo m�ximo: 0,2</remarks>
        Public Function UnidadesOrg_DevolverCadenaPresupuesto(ByVal lCiaComp As Long, ByVal iTipo As String, Optional ByVal sIdi As String = "SPA",
                                                              Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_PRESUP_LIT"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@TIPO", iTipo)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " PresContables data access methods "
        ''' <summary>
        ''' Carga el presupuesto
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iPres1">presupuesto de nivel 1</param>
        ''' <param name="iPres2">presupuesto de nivel 2</param>
        ''' <param name="iPres3">presupuesto de nivel 3</param>
        ''' <param name="iPres4">presupuesto de nivel 4</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\PresContablesNivel1.vb\LoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function PresContables_Get(ByVal lCiaComp As Long, ByVal iPres1 As Integer, Optional ByVal iPres2 As Integer = Nothing,
                                          Optional ByVal iPres3 As Integer = Nothing, Optional ByVal iPres4 As Integer = Nothing,
                                          Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_PRES2"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@PRES1", iPres1)
                cm.Parameters.AddWithValue("@PRES2", iPres2)
                cm.Parameters.AddWithValue("@PRES3", iPres3)
                cm.Parameters.AddWithValue("@PRES4", iPres4)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " PresProyectos data access methods "
        ''' <summary>
        ''' Carga el presupuesto
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iPres1">presupuesto de nivel 1</param>
        ''' <param name="iPres2">presupuesto de nivel 2</param>
        ''' <param name="iPres3">presupuesto de nivel 3</param>
        ''' <param name="iPres4">presupuesto de nivel 4</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\PresProyectosNivel1.vb\LoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function PresProyectos_Get(ByVal lCiaComp As Long, ByVal iPres1 As Integer, Optional ByVal iPres2 As Integer = Nothing,
                                          Optional ByVal iPres3 As Integer = Nothing, Optional ByVal iPres4 As Integer = Nothing,
                                          Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_PRES1"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@PRES1", iPres1)
                cm.Parameters.AddWithValue("@PRES2", iPres2)
                cm.Parameters.AddWithValue("@PRES3", iPres3)
                cm.Parameters.AddWithValue("@PRES4", iPres4)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " PresConceptos3 data access methods "
        ''' <summary>
        ''' Carga el presupuesto
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iPres1">presupuesto de nivel 1</param>
        ''' <param name="iPres2">presupuesto de nivel 2</param>
        ''' <param name="iPres3">presupuesto de nivel 3</param>
        ''' <param name="iPres4">presupuesto de nivel 4</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\PresConceptos3Nivel1.vb\LoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function PresConceptos3_Get(ByVal lCiaComp As Long, ByVal iPres1 As Integer, Optional ByVal iPres2 As Integer = Nothing,
                                           Optional ByVal iPres3 As Integer = Nothing, Optional ByVal iPres4 As Integer = Nothing,
                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_PRES3"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@PRES1", iPres1)
                cm.Parameters.AddWithValue("@PRES2", iPres2)
                cm.Parameters.AddWithValue("@PRES3", iPres3)
                cm.Parameters.AddWithValue("@PRES4", iPres4)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " PresConceptos4 data access methods "
        ''' <summary>
        ''' Carga el presupuesto
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iPres1">presupuesto de nivel 1</param>
        ''' <param name="iPres2">presupuesto de nivel 2</param>
        ''' <param name="iPres3">presupuesto de nivel 3</param>
        ''' <param name="iPres4">presupuesto de nivel 4</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\PresConceptos4Nivel1.vb\LoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function PresConceptos4_Get(ByVal lCiaComp As Long, ByVal iPres1 As Integer, Optional ByVal iPres2 As Integer = Nothing,
                                           Optional ByVal iPres3 As Integer = Nothing, Optional ByVal iPres4 As Integer = Nothing,
                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_PRES4"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@PRES1", iPres1)
                cm.Parameters.AddWithValue("@PRES2", iPres2)
                cm.Parameters.AddWithValue("@PRES3", iPres3)
                cm.Parameters.AddWithValue("@PRES4", iPres4)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Proveedor data access methods "
        ''' <summary>
        '''  Carga el proveeedor
        ''' </summary>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCod">Codigo de proveedor</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Proveedor.vb\Load ; Tiempo m�ximo: 0,2</remarks>
        Public Function Proveedor_Get(ByVal sIdi As String, ByVal lCiaComp As Long, ByVal sCod As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_PROVE"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@PROVE", sCod)
                cm.Parameters.AddWithValue("@PORTAL", 0)
                cm.Parameters.AddWithValue("@IDIOMA", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' carga los contactos de QA
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCod">Proveedor</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Proveedor.vb\CargarContactosQA ; Tiempo m�ximo: 0,2</remarks>
        Public Function Proveedor_CargarContactos_QA(ByVal lCiaComp As Long, ByVal sCod As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_DEVOLVER_CONTACTOS"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@PROVE", sCod)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        Public Function Proveedor_LoadActivities_NivelSuperior(ByVal sIdi As String, ByVal IdCia As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandText = "FSP_GET_PROVE_ACTIVITIES"
                cm.Parameters.AddWithValue("@CIA", IdCia)
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch ex As Exception
                Throw ex
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Function Obtener_Info_PersonaFisica(ByVal IdCia As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandText = "FSP_GET_PERSONAFISICA_INFO"
                cm.Parameters.AddWithValue("@CIA", IdCia)
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr.Tables(0)
            Catch ex As Exception
                Throw ex
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Sub Guardar_Info_PersonaFisica(ByVal IdCia As Long, ByVal UsuCod As String, ByVal Nombre As String, ByVal PrimerApellido As String, ByVal SegundoApellido As String, ByVal UsuNif As String,
                                               Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandText = "FSP_SAVE_PERSONAFISICA_INFO"
                cm.Parameters.AddWithValue("@CIA", IdCia)
                cm.Parameters.AddWithValue("@USUCOD", UsuCod)
                If Not String.IsNullOrEmpty(Nombre) Then cm.Parameters.AddWithValue("@NOMBRE", Nombre)
                If Not String.IsNullOrEmpty(PrimerApellido) Then cm.Parameters.AddWithValue("@PRIMERAPELLIDO", PrimerApellido)
                If Not String.IsNullOrEmpty(SegundoApellido) Then cm.Parameters.AddWithValue("@SEGUNDOAPELLIDO", SegundoApellido)
                If Not String.IsNullOrEmpty(UsuNif) Then cm.Parameters.AddWithValue("@USUNIF", UsuNif)
                cm.ExecuteNonQuery()
            Catch ex As Exception
                Throw ex
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Sub
#End Region
#Region " Proveedores data access methods "
        ''' <summary>
        ''' carga los datos de los todos los proveedores
        ''' </summary>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCod">Proveedor</param>
        ''' <param name="sDen">Denominacion de proveedor</param>
        ''' <param name="sNif">Nif de proveedor</param>
        ''' <param name="bUsaLike">Si coincide exactamente o se hace un LIKE</param>
        ''' <param name="sUser">Usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <param name="sOrgCompras">Org Compra</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Proveedores.vb  PMPortalNotificador\Notificar.vb; Tiempo m�ximo: 0,2</remarks>
        Public Function Proveedores_Load(ByVal sIdi As String, ByVal lCiaComp As Long, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing,
                                         Optional ByVal sNif As String = Nothing, Optional ByVal bUsaLike As Boolean = True, Optional ByVal sUser As String = Nothing,
                                         Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "", Optional ByVal sOrgCompras As String = Nothing) As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_PROVES"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@COD", sCod)
                cm.Parameters.AddWithValue("@DEN", sDen)
                cm.Parameters.AddWithValue("@NIF", sNif)
                cm.Parameters.AddWithValue("@USALIKE", IIf(bUsaLike, 1, 0))
                cm.Parameters.AddWithValue("@USU", sUser)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@ORGCOMPRAS", sOrgCompras)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Proveedores ERP data access methods "
        ''' <summary>
        ''' carga los datos de los todos los proveedores
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sUser">Usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <param name="sOrgCompras">Org Compra</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Proveedores.vb  PMPortalNotificador\Notificar.vb; Tiempo m�ximo: 0,2</remarks>
        Public Function ProveedoresERP_CargarProveedoresERPtoDS(ByVal lCiaComp As Long, Optional ByVal sProve As String = Nothing, Optional ByVal sOrgCompras As String = Nothing,
                                                                Optional ByVal sCodERP As String = Nothing, Optional ByVal sUser As String = Nothing,
                                                                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim rs As New DataSet
            Dim cn As New SqlConnection(mDBConnection)
            Dim sqlCmd As New SqlCommand
            Dim SqlParam As New SqlParameter
            Dim da As New SqlDataAdapter
            Try
                SqlParam = sqlCmd.Parameters.AddWithValue("@PROVE", sProve)
                SqlParam = sqlCmd.Parameters.AddWithValue("@ORGCOMPRAS", sOrgCompras)
                SqlParam = sqlCmd.Parameters.AddWithValue("@COD_ERP", sCodERP)
                sqlCmd.CommandText = FSGS(lCiaComp) & "FSPM_DEVOLVER_PROVES_ERP"
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Connection = cn
                da.SelectCommand = sqlCmd
                da.Fill(rs)

                Return rs
            Catch ex As Exception
                Throw ex
            Finally
                cn.Close()
                da.Dispose()
                sqlCmd.Dispose()
            End Try
        End Function
#End Region
#Region " Provincias data access methods "
        ''' <summary>
        ''' Obtiene las provincias
        ''' </summary>
        ''' <param name="sIdioma">Idioma del usuario</param>
        ''' <param name="lCiaComp">C�digo de la compa��a</param>
        ''' <param name="sPais">C�digo del pa�s</param>
        ''' <param name="sCod">C�digo de la provincia</param>
        ''' <param name="sDen">Denominaci�n de la provincia o parte de ella</param>
        ''' <param name="bUsaLike">Si coincide exactamente o se hace un LIKE</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <remarks>Llamada desde: PMPortalServer --> Provincias.vb --> LoadData; Tiempo m�ximo: 1sg</remarks>
        Public Function Provincias_Load(ByVal sIdioma As String, ByVal lCiaComp As Long, ByVal sPais As String, Optional ByVal sCod As String = Nothing,
                                        Optional ByVal sDen As String = Nothing, Optional ByVal bUsaLike As Boolean = True,
                                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_PROVIS"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@PAI", sPais)
                cm.Parameters.AddWithValue("@COD", sCod)
                cm.Parameters.AddWithValue("@DEN", sDen)
                cm.Parameters.AddWithValue("@USALIKE", IIf(bUsaLike, 1, 0))
                cm.Parameters.AddWithValue("@IDIOMA", sIdioma)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region "Puntuaciones data access methods"
        ''' <summary>  
        ''' Ver las puntuaciones de las variables de calidad publicadas en el portal  
        ''' </summary>  
        ''' <param name="sCodProve">C�digo de proveedor</param>   
        ''' <param name="sIdioma">idioma para los textos</param>      
        ''' <param name="sPyme">C�digo de pyme</param>      
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param> 
        ''' <returns>Las puntuaciones de las variables de calidad publicadas en el portal</returns>  
        ''' <remarks>Llamada desde:pmportalserver/cvariablescalidad/LoadData; Tiempo m�ximo: 2</remarks>  
        Public Function PuntuacionesProveedor(ByVal lCiaComp As Long, ByVal sCodProve As String,
                                      ByVal sIdioma As String, Optional ByVal sPyme As String = "",
                                      Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSP_GET_FICHACALIDAD_PROVE"
                cm.Parameters.AddWithValue("@CODPROVE", sCodProve)
                cm.Parameters.AddWithValue("@IDIOMA", sIdioma)
                If sPyme <> "" Then cm.Parameters.AddWithValue("@PYME", sPyme)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr.Tables(0)
        End Function
#End Region
#Region " Rol data access methods"
        ''' <summary>
        ''' Funcion que carga los listados relacionados con un rol
        ''' </summary>
        ''' <param name="sIdi">Idioma de la aplicaci�n</param>
        ''' <param name="bCopia">Variable booleana que indica si se debe hacer una copia</param>
        ''' <param name="lBloque">Identificador del bloque dentro del rol</param>
        ''' <param name="lId">Identificador del rol</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un DataSet con los datos de los listados relacionados con el rol</returns>
        ''' <remarks>
        ''' Llamada desdE: PmServer/Rol/CargarListados
        ''' Tiempo m�ximo: 0,3 seg</remarks>
        Public Function Rol_LoadListados(ByVal lCiaComp As Long, ByVal lId As Integer, Optional ByVal lBloque As Integer = Nothing, Optional ByVal sIdi As String = "SPA", Optional ByVal bCopia As Boolean = False,
                                         Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_LISTADOS_ROL"
                cm.Parameters.AddWithValue("@BLOQUE", lBloque)
                cm.Parameters.AddWithValue("@ROL", lId)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Carga las posibles acciones de un rol
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lId">Rol</param>
        ''' <param name="lBloque">Bloque</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Rol.vb\CargarAcciones ; Tiempo m�ximo: 0,2</remarks>
        Public Function Rol_LoadAcciones(ByVal lCiaComp As Long, ByVal lId As Integer, Optional ByVal lBloque As Integer = Nothing, Optional ByVal sIdi As String = "SPA", Optional bSoloRechazadas As Boolean = False,
                                         Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_ACCIONES_ROL"
                cm.Parameters.AddWithValue("@BLOQUE", lBloque)
                cm.Parameters.AddWithValue("@ROL", lId)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                If bSoloRechazadas Then cm.Parameters.AddWithValue("@RECHAZOS", 1)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Funcion que carga el Id de la accion "guardar", si es q existe.
        ''' </summary>
        ''' <param name="lId">Identificador del rol al que esta asociado esa persona</param>
        ''' <param name="lBloque">Identificador del bloque dentro de la instancia</param>
        ''' <remarks>Llamada desde: Rol.vb\CargarIdGuardar; Tiempo m�ximo:0,1</remarks>
        Public Function Rol_LoadIdGuardar(ByVal lCiaComp As Long, ByVal lId As Integer, ByVal lBloque As Integer) As DataSet
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_IDACCIONGUARDAR_ROL"
                cm.Parameters.AddWithValue("@BLOQUE", lBloque)
                cm.Parameters.AddWithValue("@ROL", lId)
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Obtiene los datos de los participantes en un rol
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lId">Rol</param>
        ''' <param name="lBloque">Bloque</param>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Rol.vb\CargarParticipantes ; Tiempo m�ximo: 0,2</remarks>
        Public Function Rol_LoadParticipantes(ByVal lCiaComp As Long, ByVal sIdi As String, ByVal lId As Integer, Optional ByVal lBloque As Integer = Nothing, Optional ByVal lInstancia As Integer = Nothing,
                                              Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim DsParticipantes As DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_ROL_PARTICIPANTES"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@BLOQUE", lBloque)
                cm.Parameters.AddWithValue("@ROL", lId)
                cm.Parameters.AddWithValue("@INSTANCIA", lInstancia)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                dr.Relations.Add("ROL_BLOQUES", dr.Tables(0).Columns("ROL"), dr.Tables(1).Columns("ROL"))

                DsParticipantes = New DataSet

                Dim dt As DataTable
                dt = New DataTable

                DsParticipantes.Tables.Add(dt)
                dt.Columns.Add("ROL", GetType(Integer))
                dt.Columns.Add("RDEN", GetType(String))
                'dt.Columns.Add("BLOQUE", GetType(Integer))
                dt.Columns.Add("BDEN", GetType(String))
                dt.Columns.Add("TIPO", GetType(Integer))
                dt.Columns.Add("RESTRINGIR", GetType(Integer))
                dt.Columns.Add("PART", GetType(String))
                dt.Columns.Add("PARTNOM", GetType(String))
                dt.Columns.Add("ID_CONTACTO", GetType(Integer))

                Dim newRow As DataRow
                For Each r As DataRow In dr.Tables(0).Rows
                    newRow = dt.NewRow
                    newRow("ROL") = r("ROL")
                    newRow("RDEN") = r("RDEN")
                    newRow("BDEN") = ""
                    newRow("TIPO") = r("TIPO")
                    newRow("RESTRINGIR") = r("RESTRINGIR")
                    newRow("PART") = r("PART")
                    newRow("PARTNOM") = r("PARTNOM")
                    newRow("ID_CONTACTO") = r("ID_CONTACTO")
                    For Each childRow As DataRow In r.GetChildRows("ROL_BLOQUES")
                        newRow("BDEN") = newRow("BDEN") & ", " & childRow("BDEN")
                    Next
                    newRow("BDEN") = newRow("BDEN").ToString().TrimStart(",", " ")
                    dt.Rows.Add(newRow)
                Next
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return DsParticipantes
        End Function
#End Region
#Region " Solicitud data access methods "
        ''' <summary>
        ''' Carga de una solicitud
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="RequestID">Solictud</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Solicitud.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Function Solicitud_Load(ByVal lCiaComp As Long, ByVal RequestID As Long, Optional ByVal sIdi As String = Nothing, Optional ByVal bPet As Boolean = False,
                                       Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_GET_SOLICITUD"
                cm.Parameters.AddWithValue("@ID", RequestID)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.Parameters.AddWithValue("@PET", IIf(bPet, 1, 0))
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Completa el Dataset con toda la informaci�n de la pantalla de donde se graba con la informaci�n de los
        ''' campos ocultos o de solo lectura q el jsalta.js/montarformulariosubmit no tiene pq haber recogido. 
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="RequestId">Solictud</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Solicitud.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Function Solicitud_LoadInvisibles(ByVal lCiaComp As Long, ByVal RequestId As Long, Optional ByVal bNuevoWorkfl As Boolean = False, Optional ByVal sProve As String = Nothing, Optional ByVal idUsu As Integer = Nothing,
                                                 Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                If bNuevoWorkfl = True Then
                    cm.CommandText = FSGS(lCiaComp) & "FSPM_GETINVISIBLEFIELDS"
                    If Not String.IsNullOrEmpty(sProve) Then cm.Parameters.AddWithValue("@PROVE", sProve)
                    If Not String.IsNullOrEmpty(idUsu) Then cm.Parameters.AddWithValue("@CONTACTO", idUsu)
                Else
                    cm.CommandText = FSGS(lCiaComp) & "FSQA_GETINVISIBLEFIELDS"
                    cm.Parameters.AddWithValue("@SOLIC_PROVE", 2)
                End If
                cm.Parameters.AddWithValue("@ID", RequestId)

                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>Funcion que devuelve las precondiciones que tiene una accion</summary>
        ''' <param name="iAccion">Identificador de la accion</param>
        ''' <param name="idioma">Idioma del usuario</param>
        ''' <returns>Un DataSet con los datos de las precondiciones de la accion buscada</returns>
        ''' <remarks>Llamada desde: PmServer/Solicitud/LoadPrecondicionesAccion</remarks>
        Public Function Solicitud_LoadPrecondicionesAccion(ByVal lCiaComp As Long, ByVal iAccion As Integer, ByVal idioma As String,
                                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandText = FSGS(lCiaComp) & "FSPM_GET_PRECONDICIONES"
                cm.Parameters.AddWithValue("@BSOLICITUD", 1)
                cm.Parameters.AddWithValue("@IDACC", iAccion)
                cm.Parameters.AddWithValue("@IDI", idioma)

                da.SelectCommand = cm
                da.Fill(dr)

                dr.Tables(0).ChildRelations.Add("PRECOND_CONDICIONES", dr.Tables(0).Columns("ID"), dr.Tables(1).Columns("ACCION_PRECOND"))
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' carga un campo de la instancia
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lCampo">Campo</param>
        ''' <param name="lInstancia">Si llamada es desde guardarInstancia: 0. Eoc: Instancia. Motivo lCampo es guardarInstancia->CopiaCampo.Id o Eoc->FormCampo.Id</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb\cargarCampo ; Tiempo m�ximo: 0,2</remarks>
        Public Function cargarCampo(ByVal lCiaComp As Long, ByVal lCampo As Long, ByVal lInstancia As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim BBDD As String
            Try
                cn.Open()
                cm.Connection = cn
                BBDD = FSGS(lCiaComp)
                Dim sConsulta As String = "SELECT FC.ID,ES_SUBCAMPO,SUBTIPO,CC.VALOR_NUM,CC.VALOR_FEC,CC.VALOR_BOOL,CC.VALOR_TEXT, CC.ID AS CCID,FC.TIPO_CAMPO_GS "
                sConsulta &= "FROM " & BBDD & "FORM_CAMPO FC WITH (NOLOCK) "
                sConsulta &= " INNER JOIN " & BBDD & "COPIA_CAMPO CC WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF=FC.ID"
                If (lInstancia = 0) Then
                    sConsulta &= " WHERE CC.ID = " & lCampo
                Else
                    sConsulta &= " INNER JOIN " & BBDD & "INSTANCIA I WITH (NOLOCK) ON CC.INSTANCIA=I.ID AND CC.NUM_VERSION=I.ULT_VERSION AND I.ID=" & lInstancia
                    sConsulta &= " WHERE CC.COPIA_CAMPO_DEF = " & lCampo
                End If
                cm.CommandText = sConsulta
                cm.CommandType = CommandType.Text
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Obtener Valor del campo especificado en la linea especificada
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lCampo">Id de campo</param>
        ''' <param name="nLinea">Linea de desglose</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Instancia.vb\cargarCampoDesglose ; Tiempo m�ximo: 0,2</remarks>
        Public Function cargarCampoDesglose(ByVal lCiaComp As Long, ByVal lCampo As Long, ByVal nLinea As Long, ByVal lVersion As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim sConsulta As String
            Dim BBDD As String
            Try
                cn.Open()
                cm.Connection = cn
                BBDD = FSGS(lCiaComp)
                sConsulta = "SELECT CLD.*,CCD.SUBTIPO,CCD.TIPO_CAMPO_GS"
                sConsulta = sConsulta & " FROM " & BBDD & "COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) "
                sConsulta = sConsulta & " INNER JOIN " & BBDD & "COPIA_DESGLOSE CD WITH (NOLOCK) ON CLD.CAMPO_PADRE=CD.CAMPO_PADRE AND CLD.CAMPO_HIJO=CD.CAMPO_HIJO"
                sConsulta = sConsulta & " INNER JOIN " & BBDD & "COPIA_CAMPO CC WITH (NOLOCK) ON CC.ID=CD.CAMPO_HIJO"
                sConsulta = sConsulta & " INNER JOIN " & BBDD & "FORM_CAMPO CCD WITH (NOLOCK) ON CCD.ID=CC.COPIA_CAMPO_DEF"
                sConsulta = sConsulta & " WHERE CC.COPIA_CAMPO_DEF=" & lCampo & " AND LINEA=" & nLinea & " AND NUM_VERSION=" & lVersion
                cm.CommandText = sConsulta
                cm.CommandType = CommandType.Text
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Solicitudes data access methods "
        ''' <summary>
        ''' carga las solicitudes
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Solicitudes.vb\LoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function Solicitudes_Load(ByVal lCiaComp As Long, ByVal sProve As String, Optional ByVal sIdi As String = Nothing,
                                         Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_LOADSOLICITUDES_PROVE"
                cm.Parameters.AddWithValue("@PROVE", sProve)
                cm.Parameters.AddWithValue("@TIPO", 0)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function

        Public Function Solicitudes_LoadAlta(ByVal lCiaComp As Long, ByVal codProve As String, ByVal idContacto As Integer, Optional ByVal tipoSolicitud As Integer = Nothing, Optional ByVal idioma As String = Nothing,
                                             Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_LOADSOLICITUDES_ALTA_PORTAL"
                cm.Parameters.AddWithValue("@PROVE", codProve)
                cm.Parameters.AddWithValue("@CONTACTO", idContacto)
                If tipoSolicitud <> Nothing And tipoSolicitud <> 0 Then cm.Parameters.AddWithValue("@TIPO", tipoSolicitud)
                If idioma <> Nothing Then cm.Parameters.AddWithValue("@IDI", idioma)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Tablas Externas data access methods "
        ''' <summary>
        ''' Devolver la informacion de campos de una tabla externa
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lId">Id de tabla externa</param>
        ''' <param name="sArt">valor del articulo</param>
        ''' <param name="bFiltroArticulo">Si se filtra por articulo o no</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde:  PMPortalServer\TablaExterna.vb\LoadDefTabla    Root.vb/TablaExterna_DescripcionReg ; Tiempo m�ximo: 0,2</remarks>
        Public Function TablaExterna_LoadDefTabla(ByVal lCiaComp As Long, ByVal lId As Long, Optional ByVal sArt As String = Nothing, Optional ByVal bFiltroArticulo As Boolean = True,
                                                  Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim ds As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_DEFTABLAEXTERNA"
                cm.Parameters.AddWithValue("@ID", lId)
                If Not sArt Is Nothing And sArt <> "" Then cm.Parameters.AddWithValue("@ART", sArt)
                cm.Parameters.AddWithValue("@FILTROART", IIf(bFiltroArticulo, 1, 0))
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(ds)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return ds
        End Function
        ''' <summary>
        ''' Devolver la informacion de campos de una tabla externa
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lId">Id de tabla externa</param>
        ''' <param name="sArt">valor del articulo</param>
        ''' <param name="bFiltroArticulo">Si se filtra por articulo o no</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\TablaExterna.vb\LoadData     root.vb/TablaExterna_DescripcionReg; Tiempo m�ximo: 0,2</remarks>
        Public Function TablaExterna_LoadData(ByVal lCiaComp As Long, ByVal lId As Long, Optional ByVal sArt As String = Nothing, Optional ByVal bFiltroArticulo As Boolean = True,
                                              Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim ds As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GETTABLAEXTERNA"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@ID", lId)
                If Not sArt Is Nothing And sArt <> "" Then cm.Parameters.AddWithValue("@ART", sArt)
                cm.Parameters.AddWithValue("@FILTROART", IIf(bFiltroArticulo, 1, 0))
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(ds)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return ds
        End Function
        ''' <summary>
        ''' Devolver la descripcion de la tabla externa
        ''' </summary>
        ''' <param name="lCiaComp">Codigo de compania</param>
        ''' <param name="lTablaExterna">De q tabla externa</param>
        ''' <param name="oValorKey">Clave tabla externa</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="sDateFormat">Formato fecha</param>
        ''' <param name="NumberFormat">Formato numerico</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param> 
        ''' <returns>Descripcion de la tabla externa</returns>
        ''' <remarks>Llamada desde: PMPortalDatabaseServer\Root.vb; Tiempo m�ximo: 0,2</remarks>
        Public Function TablaExterna_DescripcionReg(ByVal lCiaComp As Long, ByVal lTablaExterna As Long, ByVal oValorKey As Object, ByVal sIdi As String, ByVal sDateFormat As String, ByVal NumberFormat As Globalization.NumberFormatInfo, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As String
            Dim sValor As String
            Dim dsData As DataSet = TablaExterna_LoadData(lCiaComp, lTablaExterna, bFiltroArticulo:=False, SesionId:=SesionId, IPDir:=IPDir, PersistID:=PersistID)
            Dim dsDefTabla As DataSet = TablaExterna_LoadDefTabla(lCiaComp, lTablaExterna, bFiltroArticulo:=False, SesionId:=SesionId, IPDir:=IPDir, PersistID:=PersistID)
            If Not (dsData Is Nothing Or dsDefTabla Is Nothing Or oValorKey Is Nothing Or oValorKey.ToString() = "") Then
                Dim sPrimaryKey As String = dsDefTabla.Tables(1).Rows(0).Item("PRIMARYKEY")
                Dim sCampoMostrar As String = dsDefTabla.Tables(1).Rows(0).Item("CAMPO_MOSTRAR")
                Dim sTipoClave As String
                Dim sTipoMostrar As String
                For Each oRow As DataRow In dsDefTabla.Tables(0).Rows
                    If UCase(oRow.Item("CAMPO")) = UCase(sPrimaryKey) Then
                        sTipoClave = oRow.Item("TIPOCAMPO")
                    ElseIf UCase(oRow.Item("CAMPO")) = UCase(sCampoMostrar) Then
                        sTipoMostrar = oRow.Item("TIPOCAMPO")
                    End If
                    If Not sTipoClave Is Nothing And (Not sTipoMostrar Is Nothing Or sCampoMostrar Is Nothing) Then _
                        Exit For
                Next
                If Not sCampoMostrar Is Nothing Then
                    Dim dsTextos As DataSet = Dictionary_GetData(116, sIdi)
                    For Each oRow As DataRow In dsData.Tables(0).Rows
                        If oRow.Item(sPrimaryKey) = oValorKey Then
                            Select Case sTipoClave
                                Case "FECHA"
                                    sValor = Format(CType(oRow.Item(sPrimaryKey), Date), sDateFormat)
                                Case "NUMERICO"
                                    Dim dblValor As Double = CType(oRow.Item(sPrimaryKey), Double)
                                    sValor = dblValor.ToString("N" & IIf((dblValor Mod 1) > 0, "", "0"), NumberFormat)
                                Case Else
                                    sValor = oRow.Item(sPrimaryKey).ToString()
                            End Select
                            If Not sCampoMostrar Is Nothing Then
                                Select Case sTipoMostrar
                                    Case "FECHA"
                                        sValor = sValor & " " & Format(CType(oRow.Item(sCampoMostrar), Date), sDateFormat)
                                    Case "NUMERICO"
                                        Dim dblValor As Double = CType(oRow.Item(sCampoMostrar), Double)
                                        sValor = dblValor.ToString("N" & IIf((dblValor Mod 1) > 0, "", "0"), NumberFormat)
                                    Case Else
                                        If dsData.Tables(0).Columns(sCampoMostrar).DataType.ToString() = "System.Boolean" Then
                                            If oRow.Item(sCampoMostrar) Then
                                                sValor = sValor & " " & dsTextos.Tables(0).Select("ID=4")(0).Item(1)
                                            Else
                                                sValor = sValor & " " & dsTextos.Tables(0).Select("ID=5")(0).Item(1)
                                            End If
                                        Else
                                            sValor = sValor & " " & oRow.Item(sCampoMostrar).ToString()
                                        End If
                                End Select
                            End If
                        End If
                    Next
                End If
            End If
            Return sValor
        End Function
        ''' <summary>
        ''' Funcion que devuelve un registro de una tabla externa
        ''' </summary>
        ''' <param name="sTexto">Texto a devolver</param>
        ''' <param name="sArt">Codigo de articulo</param>
        ''' <param name="bBusqExacta">Variable booleana que indica si se debe realizar una b�squeda exacta</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un registro que coincide con el que estamos buscando en la tabla externa</returns>
        ''' <remarks>
        ''' Llamada desde: PmServer/TablaExterna/BuscarRegistro
        ''' Tiempo m�ximo: 0,4 seg</remarks>
        Public Function TablaExterna_BuscarRegistro(ByVal lCiaComp As Long, ByVal lTablaExterna As Long, ByVal sTexto As String, Optional ByVal bBusqExacta As Boolean = True, Optional ByVal sArt As String = Nothing,
                                                    Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataRow
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.CommandText = FSGS(lCiaComp) & "FSPM_BUSQREGEXTERNA"
                cm.Parameters.AddWithValue("@IDTABLA", lTablaExterna)
                cm.Parameters.AddWithValue("@TEXTO", sTexto)
                cm.Parameters.AddWithValue("@BUSQEXACTA", IIf(bBusqExacta, 1, 0))
                If Not IsNothing(sArt) Then
                    If sArt <> "" Then cm.Parameters.AddWithValue("@FILTROART", sArt)
                End If
                cm.CommandType = CommandType.StoredProcedure
                cm.Connection = cn
                da.SelectCommand = cm
                da.Fill(ds)
                da.Dispose()
                If ds.Tables.Count > 0 Then
                    Return ds.Tables(0).Rows(0)
                Else
                    Return Nothing
                End If
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region " Monedas data access methods "
        ''' <summary>
        ''' Devolver en un dataset 2(cod + den ) o todos los campos de la tabla monedas
        ''' </summary>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCod">Codigo de moneda</param>
        ''' <param name="sDen">Denominacion de proveedor</param>
        ''' <param name="bCoincid">Si se hace like o se hace =</param>
        ''' <param name="bAlldatos">saca todos los datos o no</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Monedas.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Function Monedas_Get(ByVal sIdi As String, ByVal lCiaComp As Long, ByVal sCod As String, ByVal sDen As String, ByVal bCoincid As Boolean, Optional ByVal bAlldatos As Boolean = False,
                                    Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_MONEDAS"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@COD", sCod)
                cm.Parameters.AddWithValue("@DEN", sDen)
                cm.Parameters.AddWithValue("@COINCID", IIf(bCoincid, 1, 0))
                cm.Parameters.AddWithValue("@IDIOMA", sIdi)
                cm.Parameters.AddWithValue("@ALLDATOS", bAlldatos)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Unidades data access methods "
        ''' <summary>
        ''' Carga las unidades
        ''' </summary>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCod">Codigo de Unidad</param>
        ''' <param name="sDen">Denominacion de Unidad</param>
        ''' <param name="bUsaLike">Si coincide exactamente o se hace un LIKE</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\Unidades.vb\LoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function Unidades_Load(ByVal sIdi As String, ByVal lCiaComp As Long, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal bUsaLike As Boolean = True,
                                      Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_UNIDADES"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@COD", sCod)
                cm.Parameters.AddWithValue("@DEN", sDen)
                cm.Parameters.AddWithValue("@USALIKE", IIf(bUsaLike, 1, 0))
                cm.Parameters.AddWithValue("@IDIOMA", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Unidades Neg access methods "
        ''' <summary>
        ''' Devolver un recordset con el registro de unidades qa indicado en el idioma indicado
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lId">Unidad a cargar.</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: pmserver.unidadesneg.UsuLoadData ; Tiempo m�ximo: 0,2</remarks>
        Public Function UnidadesNegocioUsu_Get(ByVal lCiaComp As Long, ByVal sIdi As String, ByVal lId As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSQA_GET_UNIDADESNEG_DEN"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@IDIOMA", sIdi)
                cm.Parameters.AddWithValue("@ID", lId)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Devuelve las unidades de negocio del proveedor en el portal de las que tenga variables de calidad publicadas en portal
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de compania</param>  
        ''' <param name="sCodProve">C�digo de proveedor</param>   
        ''' <param name="sIdioma">idioma para los textos</param>    
        ''' <param name="sPyme">C�digo de pyme</param> 
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\CVariablesCalidad.vb    PMPortalServer\UnidadesNeg.vb; Tiempo m�ximo: 0,2</remarks>
        Public Function UnidadesNegocio_PuntuacionesProveedor(ByVal lCiaComp As Long, ByVal sCodProve As String, ByVal sIdioma As String,
                                                              ByVal EmpresaPortal As Boolean, ByVal NomPortal As String, Optional ByVal sPyme As String = "",
                                                              Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSP_GET_UNIDADESNEGOCIO_PUNTUACIONES_PORTAL"
                cm.Parameters.AddWithValue("@CODPROVE", sCodProve)
                cm.Parameters.AddWithValue("@IDIOMA", sIdioma)
                If EmpresaPortal Then cm.Parameters.AddWithValue("@NOM_PORTAL", NomPortal)
                If Not String.IsNullOrEmpty(sPyme) Then cm.Parameters.AddWithValue("@PYME", sPyme)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
#End Region
#Region " UnidadesOrg data access methods "
        ''' <summary>
        ''' Carga la relacion de uons
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\UnidadesOrg.vb\CargarTodasUnidadesOrganizativas ; Tiempo m�ximo: 0,2</remarks>
        Public Function UnidadesOrganizativas_Get(ByVal lCiaComp As Long, ByVal sIdi As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_GETUONS"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                Dim parentCols(0) As DataColumn
                Dim childCols(0) As DataColumn
                parentCols(0) = dr.Tables(0).Columns("COD0")
                childCols(0) = dr.Tables(1).Columns("COD0")

                dr.Relations.Add("REL_NIV0_NIV1", parentCols, childCols, False)

                ReDim parentCols(0)
                ReDim childCols(0)
                parentCols(0) = dr.Tables(1).Columns("COD")
                childCols(0) = dr.Tables(2).Columns("UON1")

                dr.Relations.Add("REL_NIV1_NIV2", parentCols, childCols, False)

                ReDim parentCols(1)
                ReDim childCols(1)
                parentCols(0) = dr.Tables(2).Columns("UON1")
                parentCols(1) = dr.Tables(2).Columns("COD")
                childCols(0) = dr.Tables(3).Columns("UON1")
                childCols(1) = dr.Tables(3).Columns("UON2")
                dr.Relations.Add("REL_NIV2_NIV3", parentCols, childCols, False)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Cargar las Unidades Con Presupuestos
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iTipo">UnidadesConPresupuestos_Get</param>
        ''' <param name="iAnyo">A�o de presupuesto</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\UnidadesOrg.vb\CargarUnidadesConPresupuestos ; Tiempo m�ximo: 0,2</remarks>
        Public Function UnidadesConPresupuestos_Get(ByVal lCiaComp As Long, ByVal iTipo As Integer, Optional ByVal iAnyo As Integer = Nothing, Optional ByVal sIdi As String = "SPA",
                                                    Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_UONSPRES"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@TIPO", iTipo)
                cm.Parameters.AddWithValue("@ANYO", iAnyo)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Cargar el arbol de Unidades Con Presupuestos
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iTipo">Tipo de presupuesto</param>
        ''' <param name="sPer">Persona</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\UnidadesOrg.vb\CargarArbolUnidadesConPresupuestos ; Tiempo m�ximo: 0,2</remarks>
        Public Function UnidadesConPresupuestosArbol_Get(ByVal lCiaComp As Long, ByVal iTipo As Integer, Optional ByVal sPer As String = Nothing, Optional ByVal sIdi As String = "SPA",
                                                         Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_UONS"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@TIPO", iTipo)
                cm.Parameters.AddWithValue("@PER", sPer)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " User data access methods "
        ''' <summary>
        ''' Cargar los datos del usuario
        ''' </summary>
        ''' <param name="sCia">Codigo compania</param>
        ''' <param name="sCod">Usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\User.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Function User_LoadUserData(ByVal sCia As String, ByVal sCod As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSP_LOGIN"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", sCia)
                cm.Parameters.AddWithValue("@COD", sCod)
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Devuelve si se puede ver o no el detalle de la persona y el flujo de la solicitud
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCod">Proveedor</param>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\User.vb/VerDetallePersona ; Tiempo m�ximo: 0,2</remarks>
        Public Function User_VerDetallePersona(ByVal lCiaComp As Long, ByVal sCod As String, ByVal lInstancia As Long,
                                               Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_VER_DETALLE_PERSONA"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@PER", sCod)
                cm.Parameters.AddWithValue("@INSTANCIA", lInstancia)
                cm.Parameters.AddWithValue("@PROVE", 1)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                If dr.Tables(0).Rows.Count = 0 Then
                    Dim dtRow As DataRow = dr.Tables(0).NewRow
                    dtRow("VER_DETALLE_PER") = False
                    dtRow("VER_FLUJO") = False
                    dr.Tables(0).Rows.Add(dtRow)
                End If
                Return dr.Tables(0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Funcion que nos devuelve el codigo del Proveedor en el GS
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\User.vb\ObtenerProv ; Tiempo m�ximo: 0,2</remarks>
        Function User_ObtenerProvGS(ByVal lCiaComp As Long, ByVal sProve As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As String            '
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim sConsulta As String

            sConsulta = "SELECT COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK)"
            sConsulta = sConsulta + " INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID"
            sConsulta = sConsulta + " WHERE RC.CIA_COMP = " & lCiaComp & " AND C.COD = '" & sProve & "'"
            Try
                cm.Connection = cn
                cm.CommandText = sConsulta
                cm.CommandType = CommandType.Text
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try

            If dr.Tables(0).Rows.Count = 0 Then
                Return ""
            Else
                Return dr.Tables(0).Rows(0).Item(0)
            End If
        End Function
        ''' <summary>
        ''' Devuelve la configuraci�n de la pantalla de la ficha de puntuaciones del proveedor del usuario 
        ''' (Variables colapsadas, unidades de negocio visibles, ancho de columnas...)
        ''' </summary>
        ''' <param name="IdCiaComp">Id Compa�ia compradora</param>
        ''' <param name="IdUsu">Id del usuario logueado en portal</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un datatable con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\User.vb\ObtenerConfiguracionesCalificacion ; Tiempo m�ximo: 0,2</remarks>
        Public Function User_ObtenerConfiguracionesCalificacion(ByVal IdCiaComp As Integer, ByVal IdUsu As Integer,
                                                                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSP_GET_CONFIGURACION_FICHA_CALIDAD"
                cm.Parameters.AddWithValue("@IDCIACOMP", IdCiaComp)
                cm.Parameters.AddWithValue("@IDUSU", IdUsu)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr.Tables(0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' De no tener configuraci�n guardada para el usuario se obtiene la configuraci�n por defecto
        ''' </summary>
        ''' <param name="IdCiaComp">Id Compa�ia compradora</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\User.vb\ObtenerConfiguracionesDefectoCalificacion ; Tiempo m�ximo: 0,2</remarks>
        Public Function User_ObtenerConfiguracionesDefectoCalificacion(ByVal IdCiaComp As Integer, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(IdCiaComp) & "FSP_GET_CONFIGURACION_DEFECTO_FICHA_CALIDAD"
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr.Tables(0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Guarda la nueva configuraci�n de pantalla del usuario
        ''' </summary>
        ''' <param name="IdCiaComp">Id Compa�ia compradora</param>
        ''' <param name="IdUsu">Id del usuario logueado en portal</param>
        ''' <param name="Config_UNQA_Usu">Configuraci�n de visibilidad y ancho de las columnas de las unidades de negocio</param>
        ''' <param name="Config_VarCal_Usu">Configuraci�n de las variables de calidad colapsadas / expandidas</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <remarks>Llamada desde: PMPortalServer\User.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Sub User_GuardarConfiguracionesCalificacion(ByVal IdCiaComp As Integer, ByVal IdUsu As Integer,
                                                           ByVal Config_UNQA_Usu As String, ByVal Config_VarCal_Usu As String,
                                                           Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSP_GUARDAR_CONFIGURACION_FICHACALIDAD"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDCIACOMP", IdCiaComp)
                cm.Parameters.AddWithValue("@IDUSU", IdUsu)
                cm.Parameters.AddWithValue("@CONFIGUNQA", Config_UNQA_Usu)
                cm.Parameters.AddWithValue("@CONFIGVARCAL", Config_VarCal_Usu)
                cm.ExecuteNonQuery()
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Sub
#End Region
#Region " UserIdentity data access methods "
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\UserIdentity.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Function UserIdentity_GetActions(Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As UserIdentity_Actions
            Authenticate(SesionId, IPDir, PersistID)

            Dim Act As New UserIdentity_Actions
            ReDim Act.ID(1)
            Act.ID(0) = 1
            ReDim Preserve Act.ID(UBound(Act.ID) + 1)
            Act.ID(1) = 2
            Return Act
        End Function
#End Region
#Region "Validaciones Integraci�n"
        Public Function Validaciones_DevolverMapper(ByVal lCiaComp As Long, ByVal lIdEmpresa As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As String
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As SqlDataReader
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_DEVOLVER_MAPPER"
                cm.Parameters.AddWithValue("@EMPRESA", lIdEmpresa)
                cm.CommandType = CommandType.StoredProcedure
                dr = cm.ExecuteReader()
                If dr.Read() Then
                    Validaciones_DevolverMapper = dr.Item("MAPPER")
                Else
                    Validaciones_DevolverMapper = String.Empty
                End If
            Catch e As Exception
                Throw e
            Finally
                If Not dr Is Nothing AndAlso Not dr.IsClosed Then
                    dr.Close()
                End If
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        Public Function DatosMapperBD(ByVal lCiaComp As Integer, ByVal bSave As Boolean, ByVal Campo As Integer, ByVal Idioma As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim sSql As String

            Try
                cn.Open()
                cm.Connection = cn
                If bSave Then
                    sSql = "SELECT CVL.ORDEN,ISNULL(CVL.VALOR_TEXT_" & Idioma & ",'') VALOR_TEXT " _
                     & " FROM COPIA_CAMPO CC WITH(NOLOCK)" _
                     & " LEFT JOIN CAMPO_VALOR_LISTA CVL WITH(NOLOCK) ON CVL.CAMPO=CC.COPIA_CAMPO_DEF " _
                     & " WHERE CC.ID=" & Campo
                Else
                    sSql = "SELECT CVL.ORDEN,ISNULL(CVL.VALOR_TEXT_" & Idioma & ",'') VALOR_TEXT " _
                    & " FROM CAMPO_VALOR_LISTA CVL WITH(NOLOCK) WHERE CVL.CAMPO=" & Campo
                End If
                cm.CommandType = CommandType.Text
                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                cm.CommandType = CommandType.Text
                cm.Parameters.Add("@stmt", SqlDbType.NText, sSql.Length)
                cm.Parameters("@stmt").Value = sSql
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region "VariablesCalidad data access methods"
        ''' <summary>  
        ''' Ver las puntuaciones de las variables de calidad publicadas en el portal  
        ''' </summary>  
        ''' <param name="sCodProve">C�digo de proveedor</param>   
        ''' <param name="sIdioma">idioma para los textos</param>      
        ''' <param name="sPyme">C�digo de pyme</param>      
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param> 
        ''' <returns>Las puntuaciones de las variables de calidad publicadas en el portal</returns>  
        ''' <remarks>Llamada desde:pmportalserver/cvariablescalidad/LoadData; Tiempo m�ximo: 2</remarks>  
        Public Function Variables_PuntuacionesProveedor(ByVal lCiaComp As Long, ByVal sCodProve As String,
                                      ByVal sIdioma As String, Optional ByVal sPyme As String = "",
                                      Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSP_GET_VARIABLES_CALIDAD_PUBLICADAS_PORTAL"
                cm.Parameters.AddWithValue("@CODPROVE", sCodProve)
                cm.Parameters.AddWithValue("@IDIOMA", sIdioma)
                If sPyme <> "" Then cm.Parameters.AddWithValue("@PYME", sPyme)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr.Tables(0)
        End Function
#End Region
#Region "Notificacion data access methods"
        ''' <summary>
        ''' Devolver Datos EMail
        ''' </summary>
        ''' <param name="lCia">Codigo de compania</param>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="iTipo">html/txt</param>
        ''' <param name="lBloque">Bloque</param>
        ''' <param name="lAccion">Accion</param>
        ''' <param name="lEnlace">Enlace</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalNotificador\Notificar.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Function Notificador_PMDevolverDatosEMail(ByVal lCia As Long, ByVal lInstancia As Long, ByVal iTipo As Integer, Optional ByVal lBloque As Long = 0,
                                                         Optional ByVal lAccion As Long = 0, Optional ByVal lEnlace As Long = 0,
                                                         Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_SOLICITUD_DATOS_EMAIL"
                cm.Parameters.AddWithValue("@CIA", lCia)
                cm.Parameters.AddWithValue("@ID", lInstancia)
                cm.Parameters.AddWithValue("@TYPE", iTipo)
                If lBloque <> Nothing And lBloque <> 0 Then cm.Parameters.AddWithValue("@BLOQUE", lBloque)
                If lAccion <> Nothing And lAccion <> 0 Then cm.Parameters.AddWithValue("@ACCION", lAccion)
                If lEnlace <> Nothing And lEnlace <> 0 Then cm.Parameters.AddWithValue("@ENLACE", lEnlace)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Devolver Datos Mensaje
        ''' </summary>
        ''' <param name="lCia">Codigo de compania</param>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="iTipo">html/txt</param>
        ''' <param name="sIdioma">Idioma</param>
        ''' <param name="lCiaProve">Id de proveedor conectado</param> 
        ''' <param name="sCodUsu">Usuario</param>
        ''' <param name="lBloque">Bloque</param>
        ''' <param name="lAccion">Accion</param>
        ''' <param name="lEnlace">Enlace</param>
        ''' <param name="sFormatoFecha">Formato de fecha</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalNotificador\Notificar.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Function Notificador_PMDevolverDatosMensaje(ByVal lCia As Long, ByVal lInstancia As Long, ByVal iTipo As Integer, ByVal sIdioma As String, ByVal lCiaProve As Long,
                                                           ByVal sCodUsu As String, Optional ByVal lBloque As Long = 0, Optional ByVal lAccion As Long = 0,
                                                           Optional ByVal lEnlace As Long = 0, Optional ByVal sFormatoFecha As String = "", Optional ByVal SesionId As String = "",
                                                           Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_SOLICITUD_DATOS_MENSAJE"
                cm.Parameters.AddWithValue("@CIA", lCia)
                cm.Parameters.AddWithValue("@CIA_PROVE", lCiaProve)
                cm.Parameters.AddWithValue("@ID", lInstancia)
                cm.Parameters.AddWithValue("@TYPE", iTipo)
                If lBloque <> Nothing And lBloque <> 0 Then cm.Parameters.AddWithValue("@BLOQUE", lBloque)
                If lAccion <> Nothing And lAccion <> 0 Then cm.Parameters.AddWithValue("@ACCION", lAccion)
                If lEnlace <> Nothing And lEnlace <> 0 Then cm.Parameters.AddWithValue("@ENLACE", lEnlace)
                cm.Parameters.AddWithValue("@COD_USU", sCodUsu)
                cm.Parameters.AddWithValue("@FORMATOFECHA", sFormatoFecha)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' se Notifican los Enlaces (Cambios de Etapa)
        ''' </summary>
        ''' <param name="lCia">Codigo de compania</param>
        ''' <param name="lAccion">Accion</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalNotificador\Notificar.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Function Notificador_PMDevolverEnlacesAccion(ByVal lCia As Long, ByVal lAccion As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_DEVOLVER_ENLACESACCION"
                cm.Parameters.AddWithValue("@CIA", lCia)
                cm.Parameters.AddWithValue("@ACCION", lAccion)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Graba en bbdd el mail enviado, sea correctamente enviado o no 
        ''' </summary>
        ''' <param name="From">From</param>
        ''' <param name="Subject">Subject</param>
        ''' <param name="Para">Para</param>
        ''' <param name="Message">Message</param>
        ''' <param name="isHTML">si es html o txt</param>
        ''' <param name="bPortal">mail de portal u otro origen</param>
        ''' <param name="bError">correctamente enviado o no </param>
        ''' <param name="sTextoError">no correctamente enviado, aqui viene el posible error</param>
        ''' <param name="lIdUsu">Usuario</param>
        ''' <param name="lIdCia">Compania</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <remarks>Llamada desde: PMPortalNotificador\Notificar.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Sub Notificador_GrabarEnviarMensaje(ByVal From As String, ByVal Subject As String, ByVal Para As String, ByVal Message As String, ByVal isHTML As Boolean,
                                                   ByVal bPortal As Boolean, ByVal bError As Boolean, ByVal sTextoError As String, ByVal lIdUsu As Long, ByVal lIdCia As Long, ByVal sCC As String, ByVal sCCO As String,
                                                   Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand

            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "SP_INSERT_REGISTRO_MAIL"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lIdCia)
                cm.Parameters.AddWithValue("@SUBJECT", Subject)
                If IsNothing(cm.Parameters("@SUBJECT").Value) Then cm.Parameters("@SUBJECT").Value = "Error antes de crear subject"
                cm.Parameters.AddWithValue("@PARA", Para)
                If IsNothing(cm.Parameters("@PARA").Value) Then cm.Parameters("@PARA").Value = "Error antes de crear para"
                cm.Parameters.AddWithValue("@CC", sCC)
                cm.Parameters.AddWithValue("@CCO", sCCO)
                cm.Parameters.AddWithValue("@DIR_RESPUESTA", From)
                cm.Parameters.AddWithValue("@CUERPO", Message)
                If IsNothing(cm.Parameters("@CUERPO").Value) Then cm.Parameters("@CUERPO").Value = "Error antes de crear cuerpo"
                cm.Parameters.AddWithValue("@ACUSE_RECIBO", 0)
                If IsNothing(cm.Parameters("@ACUSE_RECIBO").Value) Then cm.Parameters("@ACUSE_RECIBO").Value = 0
                cm.Parameters.AddWithValue("@TIPO", IIf(isHTML, 1, 0))
                cm.Parameters.AddWithValue("@PRODUCTO", 2)
                cm.Parameters.AddWithValue("@USU", lIdUsu)
                cm.Parameters.AddWithValue("@KO", IIf(bError, 1, 0))
                cm.Parameters.AddWithValue("@TEXTO_ERROR", sTextoError)
                cm.ExecuteNonQuery()
            Catch ex As Exception
                Try
                    subDelay(3)

                    cm = New SqlCommand("SP_INSERT_REGISTRO_MAIL", cn)
                    cm.CommandType = CommandType.StoredProcedure
                    cm.Parameters.AddWithValue("@CIA", lIdCia)
                    cm.Parameters.AddWithValue("@SUBJECT", Subject)
                    If IsNothing(cm.Parameters("@SUBJECT").Value) Then cm.Parameters("@SUBJECT").Value = "Error antes de crear subject"
                    cm.Parameters.AddWithValue("@PARA", Para)
                    If IsNothing(cm.Parameters("@PARA").Value) Then cm.Parameters("@PARA").Value = "Error antes de crear para"
                    cm.Parameters.AddWithValue("@CC", sCC)
                    cm.Parameters.AddWithValue("@CCO", sCCO)
                    cm.Parameters.AddWithValue("@DIR_RESPUESTA", From)
                    cm.Parameters.AddWithValue("@CUERPO", Message)
                    If IsNothing(cm.Parameters("@CUERPO").Value) Then cm.Parameters("@CUERPO").Value = "Error antes de crear cuerpo"
                    cm.Parameters.AddWithValue("@ACUSE_RECIBO", 0)
                    If IsNothing(cm.Parameters("@ACUSE_RECIBO").Value) Then cm.Parameters("@ACUSE_RECIBO").Value = 0
                    cm.Parameters.AddWithValue("@TIPO", IIf(isHTML, 1, 0))
                    cm.Parameters.AddWithValue("@PRODUCTO", 2)
                    cm.Parameters.AddWithValue("@USU", lIdUsu)
                    cm.Parameters.AddWithValue("@KO", IIf(bError, 1, 0))
                    cm.Parameters.AddWithValue("@TEXTO_ERROR", sTextoError)
                    cm.ExecuteNonQuery()
                Catch ex2 As Exception
                End Try
            Finally
                cn.Close()
                cn = Nothing
                cm = Nothing
            End Try
        End Sub
        Public Sub subDelay(ByVal sngDelay As Single)
            Dim sngStart As DateTime             ' Start time.
            Dim sngStop As DateTime              ' Stop time.
            Dim sngNow As DateTime               ' Current time.

            sngStart = System.DateTime.Now ' Get current timer.
            sngStop = DateAdd(DateInterval.Second, sngDelay, sngStart)    ' Set up stop time based on
            ' delay.
            While sngNow < sngStop
                sngNow = System.DateTime.Now ' Get current timer again.
            End While        ' Has time elapsed?
        End Sub
        ''' Revisado por: Jbg. Fecha: 24/10/2011
        ''' <summary>
        ''' Funci�n que graba en la tabla REGISTRO_EMAIL toda la informaci�n del email enviado.
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>    
        ''' <param name="From">El from del mensaje</param>
        ''' <param name="Subject">El asunto</param>
        ''' <param name="Para">El destinatario</param>
        ''' <param name="Message">El mensaje</param>
        ''' <param name="CC">Copia a</param>
        ''' <param name="CCO">Copia oculta</param>
        ''' <param name="bEsPm">Si estamos en PM o no.</param>
        ''' <param name="bHTML">Si el email se ha enviado con formato HTML o no.</param>
        ''' <param name="bKO">Si ha ido bien el env�o.</param>
        ''' <param name="sTextoError">El texto de error (si hay)</param>
        ''' <param name="sAttachments">Los adjuntos atachados en el email</param>
        ''' <param name="Ruta">La ruta de los ficheros adjuntos</param>
        ''' <param name="CodProve">Proveedor al q se le envia el mail</param>
        ''' <param name="IdMail">Tipo de Notificacion. Es un valor de FSNLibrary.TipoNotificacion</param>
        ''' <param name="lIdInstancia">Id de la instancia a la que asociaremos la notificaci�n</param>
        ''' <param name="EntidadNotificacion">Identifica para q producto es el mail</param>
        ''' <param name="ParaNombre">Nombre y apellidos del q recibe el mail</param> 
        ''' <param name="lIdError">Id del error</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>   
        ''' <remarks>Llamada desde: PMNotificador --> Notificar.vb --> Enviarmail. Tiempo m�ximo: 0 sg.</remarks>
        Public Sub Notificador_GrabarEnviarMensajeGS(ByVal lCiaComp As Long, ByVal From As String, ByVal Subject As String, ByVal Para As String, ByVal Message As String,
                                                    ByVal CC As String, ByVal CCO As String, ByVal bEsPm As Boolean, ByVal bHTML As Boolean, ByVal bKO As Boolean, ByVal sTextoError As String,
                                                    ByVal sAttachments As String, ByVal Ruta As String, ByVal CodProve As String, ByVal IdMail As Integer, ByVal lIdInstancia As Long,
                                                    ByVal EntidadNotificacion As Integer, ByVal ParaNombre As String, Optional ByVal lIdError As Long = -1,
                                                    Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "")
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim lRegMailId As Long
            Dim lRegMailAdjunId As Long
            Dim sPath As String
            Dim sTemp As String = ""
            Dim oFileStream As System.IO.FileStream
            Dim byteBuffer() As Byte
            Dim lDataSize As Long
            Dim BBDD As String = FSGS(lCiaComp)
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "BEGIN TRANSACTION"
                cm.ExecuteNonQuery()

                cm = New SqlCommand(BBDD & "FSGS_INSERT_REGISTRO_MAIL", cn)
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@SUBJECT", Subject)
                If IsNothing(cm.Parameters("@SUBJECT").Value) _
                OrElse (cm.Parameters("@SUBJECT").Value = "") Then
                    cm.Parameters("@SUBJECT").Value = "Error antes de crear subject"
                End If
                cm.Parameters.AddWithValue("@PARA", Para)
                If IsNothing(cm.Parameters("@PARA").Value) _
                OrElse (cm.Parameters("@PARA").Value = "") Then
                    cm.Parameters("@PARA").Value = "Error antes de crear para"
                End If
                cm.Parameters.AddWithValue("@CC", CC)
                cm.Parameters.AddWithValue("@CCO", CCO)
                cm.Parameters.AddWithValue("@DIR_RESPUESTA", From)
                cm.Parameters.AddWithValue("@CUERPO", Message)
                If IsNothing(cm.Parameters("@CUERPO").Value) _
                OrElse (cm.Parameters("@CUERPO").Value = "") Then
                    'Error antes de crear cuerpo
                    cm.Parameters("@CUERPO").Value = "Error antes de crear cuerpo"
                End If
                cm.Parameters.AddWithValue("@ACUSE_RECIBO", 0)
                If IsNothing(cm.Parameters("@ACUSE_RECIBO").Value) Then cm.Parameters("@ACUSE_RECIBO").Value = 0
                cm.Parameters.AddWithValue("@TIPO", IIf(bHTML, 1, 0))
                cm.Parameters.AddWithValue("@PRODUCTO", IIf(bEsPm, 2, 3))
                cm.Parameters.AddWithValue("@USU", "")
                cm.Parameters.AddWithValue("@KO", IIf(bKO, 1, 0))
                cm.Parameters.AddWithValue("@TEXTO_ERROR", sTextoError)
                cm.Parameters.AddWithValue("@IDENTIFICADOR", SqlDbType.Int).Direction = ParameterDirection.Output
                cm.Parameters.AddWithValue("@PROVE", CodProve)
                cm.Parameters.AddWithValue("@ID_EMAIL", IdMail)
                If lIdInstancia <> 0 Then cm.Parameters.AddWithValue("@INSTANCIA", lIdInstancia)
                If EntidadNotificacion <> 0 Then cm.Parameters.AddWithValue("@ENTIDAD", EntidadNotificacion)
                If ParaNombre <> "" Then cm.Parameters.AddWithValue("@PARA_NOMBRE", ParaNombre)
                If lIdError > -1 Then
                    cm.Parameters.AddWithValue("@ID_ERROR", lIdError)
                Else
                    cm.Parameters.AddWithValue("@ID_ERROR", System.DBNull.Value)
                End If
                cm.ExecuteNonQuery()

                lRegMailId = cm.Parameters("@IDENTIFICADOR").Value

                If Ruta <> "" Then
                    sTemp = ConfigurationSettings.AppSettings("temp") & "\" & Ruta & "\"

                    While InStr(sAttachments, ";", CompareMethod.Binary)
                        sPath = Left(sAttachments, InStr(sAttachments, ";", CompareMethod.Binary) - 1)
                        Try
                            cm = New SqlCommand(BBDD & "FSGS_INSERT_REGISTRO_MAIL_ADJUN", cn)
                            cm.CommandType = CommandType.StoredProcedure
                            cm.Parameters.AddWithValue("@REG_ID", lRegMailId)
                            cm.Parameters.AddWithValue("@NOM", sPath)
                            cm.Parameters.AddWithValue("@IDENTIFICADOR", SqlDbType.Int).Direction = ParameterDirection.Output
                            cm.ExecuteNonQuery()

                            lRegMailAdjunId = cm.Parameters("@IDENTIFICADOR").Value

                            ''Updatar DATA
                            oFileStream = New System.IO.FileStream(sTemp & sPath, IO.FileMode.Open, FileAccess.Read)

                            ReDim byteBuffer(oFileStream.Length)
                            lDataSize = oFileStream.Length

                            Dim sAux() As String = Split(oFileStream.Name, "\")
                            Dim sNom As String = sAux(UBound(sAux))
                            Call oFileStream.Read(byteBuffer, 0, Int32.Parse(oFileStream.Length.ToString()))

                            cm = New SqlCommand(BBDD & "FSGS_UPDATE_REGISTRO_MAIL_ADJUN", cn)
                            cm.CommandType = CommandType.StoredProcedure

                            cm.Parameters.AddWithValue("@REG_ID", lRegMailId)
                            cm.Parameters.AddWithValue("@IDENTIFICADOR", lRegMailAdjunId)
                            cm.Parameters.AddWithValue("@INIT", 0)
                            cm.Parameters("@INIT").Value = 0
                            cm.Parameters.AddWithValue("@DATA", byteBuffer)
                            cm.ExecuteNonQuery()

                            oFileStream.Close()
                            sAttachments = Right(sAttachments, sAttachments.Length - InStr(sAttachments, ";", CompareMethod.Binary))
                        Catch ex As Exception
                            sAttachments = Right(sAttachments, sAttachments.Length - InStr(sAttachments, ";", CompareMethod.Binary))
                        End Try
                    End While
                End If

                cm = New SqlCommand("COMMIT TRANSACTION", cn)
                cm.ExecuteNonQuery()
            Catch ex As Exception
                Try
                    cm = New SqlCommand(BBDD & "FSGS_UPDATE_REGISTRO_MAIL", cn)
                    cm.CommandType = CommandType.StoredProcedure

                    cm.Parameters.AddWithValue("@IDENTIFICADOR", lRegMailId)
                    cm.Parameters.AddWithValue("@TEXTO_ERROR", ex.Message)
                    cm.ExecuteNonQuery()

                    cm = New SqlCommand("COMMIT TRANSACTION", cn)
                    cm.ExecuteNonQuery()

                Catch ex2 As Exception
                    cm = New SqlCommand("ROLLBACK TRANSACTION", cn)
                    cm.ExecuteNonQuery()
                End Try
            Finally
                cn.Close()
                cn = Nothing
                cm = Nothing
                oFileStream = Nothing
            End Try
        End Sub
        ''' <summary>
        ''' Funci�n que graba en la tabla ERRORES toda la informaci�n del email NO enviado.
        ''' </summary>
        '''  <param name="lCiaComp">Id de la compa�ia</param>   
        ''' <param name="sPagina">Donde da el error</param>
        ''' <param name="sUsuario">Usuario</param>
        ''' <param name="sEx_FullName">Nombre de error</param>
        ''' <param name="sEx_Message">Mensaje de error</param>
        ''' <param name="sEx_StackTrace">Datos de la pila de errores</param>
        ''' <param name="sSv_Query_String">Datos del Query String pasado</param>
        ''' <param name="sSv_Http_User_Agent">Tipo de excepcion devuelta</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Id del error</returns>
        ''' <remarks>Llamada desde: PMNotificador --> Notificar.vb --> Enviarmail. Tiempo m�ximo: 0 sg.</remarks>
        Public Function Notificador_GrabarErroresGS(ByVal lCiaComp As Long, ByVal sPagina As String, ByVal sUsuario As String, ByVal sEx_FullName As String,
                        ByVal sEx_Message As String, ByVal sEx_StackTrace As String, ByVal sSv_Query_String As String, ByVal sSv_Http_User_Agent As String,
                        Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Long
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim BBDD As String = FSGS(lCiaComp)
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.StoredProcedure
                cm.CommandText = BBDD & "FSPM_INSERTAR_ERROR"
                cm.Parameters.AddWithValue("@PAGINA", IIf(String.IsNullOrEmpty(sPagina), String.Empty, sPagina))
                cm.Parameters.AddWithValue("@USUARIO", IIf(String.IsNullOrEmpty(sUsuario), String.Empty, sUsuario))
                cm.Parameters.AddWithValue("@FULLNAME", IIf(String.IsNullOrEmpty(sEx_FullName), String.Empty, sEx_FullName))
                cm.Parameters.AddWithValue("@MESSAGE", IIf(String.IsNullOrEmpty(sEx_Message), String.Empty, sEx_Message))
                cm.Parameters.AddWithValue("@STACKTRACE", IIf(String.IsNullOrEmpty(sEx_StackTrace), String.Empty, sEx_StackTrace))
                cm.Parameters.AddWithValue("@QUERYSTRING", IIf(String.IsNullOrEmpty(sSv_Query_String), String.Empty, sSv_Query_String))
                cm.Parameters.AddWithValue("@USERAGENT", IIf(String.IsNullOrEmpty(sSv_Http_User_Agent), String.Empty, sSv_Http_User_Agent))
                cm.Parameters.AddWithValue("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
                cm.ExecuteNonQuery()

                Notificador_GrabarErroresGS = DBNullToSomething(cm.Parameters("@ID").Value)
            Catch ex As Exception
                Notificador_GrabarErroresGS = -1
            Finally
                cn.Close()
                cn = Nothing
                cm = Nothing
            End Try
        End Function
#End Region
#Region "Grupo data access methods"
#End Region
#Region " Entregas data access methods "
        ''' Revisado por: blp. Fecha: 26/10/2011
        ''' <summary>
        ''' M�todo que devuelve las entregas efectuadas por el proveedor.
        ''' </summary>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lProveID">Id del proveedor</param>
        ''' <param name="lCiaComp">C�digo de la empresa</param>
        ''' <param name="Anio">A�o del pedido (no de las entregas)</param>
        ''' <param name="NumPedido">N�mero del Pedido ("cesta" en la visualizaci�n en pantalla)</param>
        ''' <param name="NumOrden">n�mero de la orden ("Pedido" ebn la visualizaci�n en pantalla)</param>
        ''' <param name="NumPedidoERP">N�mero dado en la aplicaci�n ERP del cliente al pedido</param>
        ''' <param name="FecRecepDesde">Fecha a partir de la cual filtrar la fecha de recepci�n de las entregas</param>
        ''' <param name="FecRecepHasta">Fecha hasta la cual filtrar la fecha de recepci�n de las entregas</param>
        ''' <param name="VerPedidosSinRecep">
        '''		Si vale 1: Se muestran los pedidos con recepciones y los que a�n no tienen ninguna recepci�n 
        '''			   y que su fecha de entrega solicitada est� comprendida entre las fechas FECRECEPDESDE y FECRECEPHASTA
        '''		Si vale 0: No se muestran los pedidos sin recepciones
        ''' </param>
        ''' <param name="NumAlbaran">Albar�n del env�o</param>
        ''' <param name="NumPedProve">N� de pedido dado por el proveedor</param>
        ''' <param name="Articulo">Parte del nombre o descripci�n del art�culo</param>
        ''' <param name="FecEntregaSolicitDesde">Fecha a partir de la cual filtrar la fecha de entrega solicitada por el cliente</param>
        ''' <param name="FecEntregaSolicitHasta">Fecha hasta la cual filtrar la fecha de entrega solicitada por el cliente</param>
        ''' <param name="CodEmpresa">ID de la empresa</param>
        ''' <param name="FecEntregaIndicadaProveDesde">Fecha a partir de la cual filtrar la fecha de entrega indicada por el proveedor</param>
        ''' <param name="FecEntregaIndicadaProveHasta">Fecha hasta la cual filtrar la fecha de entrega indicada por el proveedor</param>
        ''' <param name="FecEmisionDesde">Fecha a partir de la cual filtrar la fecha de emisi�n del pedido</param>
        ''' <param name="FecEmisionHasta">Fecha hasta la cual filtrar la fecha de emisi�n</param>
        ''' <param name="Centro">C�digo del centro de coste por el cual filtrar las entregas</param>
        ''' <param name="Partidas">
        ''' Conjunto de todos los c�digos de partida presupuestaria por lo que se ha filtrado
        ''' La divisi�n entre cada conjunto de partidas es ',' y dentro de cada conjunto es el car�cter '@'
        ''' </param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Dataset con el listado de entregas solicitado</returns>
        ''' <remarks>Llamada desde entregas.vb->LoadData; M�x inferior a 1seg.</remarks>
        Public Function Entregas_Get(ByVal sIdi As String, ByVal lProveID As Long, ByVal lCiaComp As Long, ByVal Anio As Integer, ByVal NumPedido As Integer, ByVal NumOrden As Integer,
                                     ByVal NumPedidoERP As String, ByVal FecRecepDesde As Date, ByVal FecRecepHasta As Date, ByVal VerPedidosSinRecep As Boolean, ByVal NumAlbaran As String,
                                     ByVal NumPedProve As String, ByVal Articulo As String, ByVal FecEntregaSolicitDesde As Date, ByVal FecEntregaSolicitHasta As Date, ByVal NumFactura As String,
                                     ByVal CodEmpresa As Integer, ByVal FecEntregaIndicadaProveDesde As Date, ByVal FecEntregaIndicadaProveHasta As Date, ByVal FecEmisionDesde As Date,
                                     ByVal FecEmisionHasta As Date, ByVal Centro As String, ByVal Partidas As String, ByVal EmpresaPortal As Boolean, ByVal NomPortal As String,
                                     Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSP_OBT_TODAS_ENTREGASPEDIDOS"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDIOMA", sIdi)
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@PROVE", lProveID)
                cm.Parameters.AddWithValue("@ANYO", Anio)
                cm.Parameters.AddWithValue("@NUMPEDIDO", NumPedido)
                cm.Parameters.AddWithValue("@NUMORDEN", NumOrden)
                cm.Parameters.AddWithValue("@NUMPEDIDOERP", NumPedidoERP)
                cm.Parameters.AddWithValue("@FECRECEPDESDE", IIf(FecRecepDesde = Date.MinValue, System.DBNull.Value, FecRecepDesde))
                cm.Parameters.AddWithValue("@FECRECEPHASTA", IIf(FecRecepHasta = Date.MinValue, System.DBNull.Value, FecRecepHasta))
                cm.Parameters.AddWithValue("@VERPEDIDOSSINRECEP", VerPedidosSinRecep)
                cm.Parameters.AddWithValue("@NUMALBARAN", NumAlbaran)
                cm.Parameters.AddWithValue("@NUMPEDPROVE", NumPedProve)
                cm.Parameters.AddWithValue("@ARTICULO", Articulo)
                cm.Parameters.AddWithValue("@FECENTREGASOLICITDESDE", IIf(FecEntregaSolicitDesde = Date.MinValue, System.DBNull.Value, FecEntregaSolicitDesde))
                cm.Parameters.AddWithValue("@FECENTREGASOLICITHASTA", IIf(FecEntregaSolicitHasta = Date.MinValue, System.DBNull.Value, FecEntregaSolicitHasta))
                cm.Parameters.AddWithValue("@NUMFACTURA", NumFactura)
                cm.Parameters.AddWithValue("@CODEMPRESA", CodEmpresa)
                cm.Parameters.AddWithValue("@FECENTREGAINDICADAPROVEDESDE", IIf(FecEntregaIndicadaProveDesde = Date.MinValue, System.DBNull.Value, FecEntregaIndicadaProveDesde))
                cm.Parameters.AddWithValue("@FECENTREGAINDICADAPROVEHASTA", IIf(FecEntregaIndicadaProveHasta = Date.MinValue, System.DBNull.Value, FecEntregaIndicadaProveHasta))
                cm.Parameters.AddWithValue("@FECEMISIONDESDE", IIf(FecEmisionDesde = Date.MinValue, System.DBNull.Value, FecEmisionDesde))
                cm.Parameters.AddWithValue("@FECEMISIONHASTA", IIf(FecEmisionHasta = Date.MinValue, System.DBNull.Value, FecEmisionHasta))
                cm.Parameters.AddWithValue("@CENTRO", Centro)
                cm.Parameters.AddWithValue("@PARTIDASPRES", Partidas)
                If EmpresaPortal Then cm.Parameters.AddWithValue("@NOM_PORTAL", NomPortal)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch ex As SqlException
                Throw ex
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
        End Function
        ''' Revisado por: blp. Fecha: 28/10/2011
        ''' <summary>
        ''' Obtiene la configuraci�n de los campos a mostrar y su ancho en el Visor de Entregas (EntregasPedidos.aspx) del portal para el usuario y la compa��a (proveedor) dados
        ''' </summary>
        ''' <param name="CodUsu">C�digo del usuario</param>
        ''' <param name="lCiaComp">Codigo del proveedor</param>        
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <remarks>
        ''' Llamadas desde Entregas.vb-->Obt_Conf_Visor_Entregas(). M�x 1 seg
        ''' </remarks>
        Public Function Entregas_Obt_Conf_Visor_Entregas(ByVal CodUsu As String, ByVal lCiaComp As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dt As New DataTable
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSP_OBT_CONF_VISOR_RECEPCIONES"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@USU", CodUsu)
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dt)
                Return dt
            Catch ex As SqlException
                Throw ex
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dt.Dispose()
            End Try
        End Function
        Public Function CodificacionPersonalizadaRecepciones(lCiaComp As Long) As Boolean
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As SqlDataReader
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "SELECT CODRECEPERP FROM " & FSGS(lCiaComp) & "PARGEN_PED WITH (NOLOCK)"
                cm.CommandType = CommandType.Text
                dr = cm.ExecuteReader()
                If dr.Read() Then
                    Return (dr.Item("CODRECEPERP") = 1)
                Else
                    Return False
                End If
            Catch e As Exception
                Throw e
            Finally
                If Not dr Is Nothing AndAlso Not dr.IsClosed Then
                    dr.Close()
                End If
                cn.Dispose()
                cm.Dispose()
            End Try

        End Function
        ''' Revisado por: blp. Fecha: 28/10/2011
        ''' <summary>
        ''' Guarda la configuraci�n de los campos a mostrar y su ancho en el Visor de Entregas (EntregasPedidos.aspx) del portal para el usuario y la compa��a (proveedor) dados
        ''' </summary>
        ''' <param name="CodUsu">C�digo del usuario</param>
        ''' <param name="lCiaComp">Codigo del proveedor</param>
        ''' <param name="Retrasado_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Retrasado_width">Ancho del campo</param>
        ''' <param name="Albaran_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Albaran_width">Ancho del campo</param>
        ''' <param name="Frecep_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Frecep_width">Ancho del campo</param>
        ''' <param name="Emp_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Emp_width">Ancho del campo</param>
        ''' <param name="Erp_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Erp_width">Ancho del campo</param>
        ''' <param name="Pedido_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Pedido_width">Ancho del campo</param>
        ''' <param name="NumPedProve_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="NumPedProve_width">Ancho del campo</param>
        ''' <param name="Femision_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Femision_width">Ancho del campo</param>
        ''' <param name="Fentrega_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Fentrega_width">Ancho del campo</param>
        ''' <param name="Fentregaprove_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Fentregaprove_width">Ancho del campo</param>
        ''' <param name="Art_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Art_width">Ancho del campo</param>
        ''' <param name="Cantped_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Cantped_width">Ancho del campo</param>
        ''' <param name="Cantrec_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Cantrec_width">Ancho del campo</param>
        ''' <param name="Cantpend_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Cantpend_width">Ancho del campo</param>
        ''' <param name="Uni_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Uni_width">Ancho del campo</param>
        ''' <param name="Prec_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Prec_width">Ancho del campo</param>
        ''' <param name="Impped_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Imped_width">Ancho del campo</param>
        ''' <param name="Imprec_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Imprec_width">Ancho del campo</param>
        ''' <param name="Imppend_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Imppend_width">Ancho del campo</param>
        ''' <param name="Mon_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Mon_width">Ancho del campo</param>
        ''' <param name="Cc1_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Cc1_width">Ancho del campo</param>
        ''' <param name="Cc2_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Cc2_width">Ancho del campo</param>
        ''' <param name="Cc3_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Cc3_width">Ancho del campo</param>
        ''' <param name="Cc4_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Cc4_width">Ancho del campo</param>
        ''' <param name="Partida1_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Partida1_width">Ancho del campo</param>
        ''' <param name="Partida2_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Partida2_width">Ancho del campo</param>
        ''' <param name="Partida3_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Partida3_width">Ancho del campo</param>
        ''' <param name="Partida4_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Partida4_width">Ancho del campo</param>
        ''' <param name="bloq_factura_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="bloq_factura_width">Ancho del campo</param>
        ''' <param name="num_linea_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="num_linea_width">Ancho del campo</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <remarks>
        ''' Llamadas desde Entregas.vb-->Mod_Conf_Visor_Entregas(). M�x 1 seg
        ''' </remarks>
        Public Sub Entregas_Mod_Conf_Visor_Entregas(ByVal CodUsu As String, ByVal lCiaComp As Long, ByVal Retrasado_visible As Boolean, ByVal Retrasado_width As Long,
                                    ByVal Albaran_visible As Boolean, ByVal Albaran_width As Long, ByVal Frecep_visible As Boolean, ByVal Frecep_width As Long,
                                    ByVal Emp_visible As Boolean, ByVal Emp_width As Long, ByVal Erp_visible As Boolean, ByVal Erp_width As Long, ByVal Pedido_visible As Boolean,
                                    ByVal Pedido_width As Long, ByVal NumPedProve_visible As Boolean, ByVal NumPedProve_width As Long, ByVal Femision_visible As Boolean,
                                    ByVal Femision_width As Long, ByVal Fentrega_visible As Boolean, ByVal Fentrega_width As Long, ByVal Fentregaprove_visible As Boolean,
                                    ByVal Fentregaprove_width As Long, ByVal Art_visible As Boolean, ByVal Art_width As Long, ByVal Cantped_visible As Boolean,
                                    ByVal Cantped_width As Long, ByVal Cantrec_visible As Boolean, ByVal Cantrec_width As Long, ByVal Cantpend_visible As Boolean,
                                    ByVal Cantpend_width As Long, ByVal Uni_visible As Boolean, ByVal Uni_width As Long, ByVal Prec_visible As Boolean,
                                    ByVal Prec_width As Long, ByVal Impped_visible As Boolean, ByVal Imped_width As Long, ByVal Imprec_visible As Boolean,
                                    ByVal Imprec_width As Long, ByVal Imppend_visible As Boolean, ByVal Imppend_width As Long, ByVal Mon_visible As Boolean,
                                    ByVal Mon_width As Long, ByVal Cc1_visible As Boolean, ByVal Cc1_width As Long, ByVal Cc2_visible As Boolean, ByVal Cc2_width As Long,
                                    ByVal Cc3_visible As Boolean, ByVal Cc3_width As Long, ByVal Cc4_visible As Boolean, ByVal Cc4_width As Long, ByVal Partida1_visible As Boolean,
                                    ByVal Partida1_width As Long, ByVal Partida2_visible As Boolean, ByVal Partida2_width As Long, ByVal Partida3_visible As Boolean,
                                    ByVal Partida3_width As Long, ByVal Partida4_visible As Boolean, ByVal Partida4_width As Long, ByVal bloq_factura_visible As Boolean,
                                    ByVal bloq_factura_width As Long, ByVal num_linea_visible As Boolean, ByVal num_linea_width As Long, ByVal factura_visible As Boolean,
                                    ByVal factura_width As Long, ByVal receptor_visible As Boolean, ByVal receptor_width As Long, ByVal importe_albaran_visible As Boolean,
                                    ByVal importe_albaran_width As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "",
                                    Optional ByVal PersistID As String = "", Optional ByVal CODRECEPERP_visible As Boolean = True, Optional ByVal CODRECEPERP_width As Long = 0)
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            If CodUsu IsNot Nothing AndAlso CodUsu <> String.Empty AndAlso lCiaComp <> 0 Then
                Try
                    cn.Open()
                    cm.Connection = cn
                    cm.CommandText = "FSP_MOD_CONF_VISOR_RECEPCIONES"
                    cm.CommandType = CommandType.StoredProcedure
                    cm.Parameters.AddWithValue("@USU", CodUsu)
                    cm.Parameters.AddWithValue("@CIA", lCiaComp)
                    cm.Parameters.AddWithValue("@RETRASADO_VISIBLE", Retrasado_visible)
                    cm.Parameters.AddWithValue("@RETRASADO_WIDTH", Retrasado_width)
                    cm.Parameters.AddWithValue("@ALBARAN_VISIBLE", Albaran_visible)
                    cm.Parameters.AddWithValue("@ALBARAN_WIDTH", Albaran_width)
                    cm.Parameters.AddWithValue("@FRECEP_VISIBLE", Frecep_visible)
                    cm.Parameters.AddWithValue("@FRECEP_WIDTH", Frecep_width)
                    cm.Parameters.AddWithValue("@EMP_VISIBLE", Emp_visible)
                    cm.Parameters.AddWithValue("@EMP_WIDTH", Emp_width)
                    cm.Parameters.AddWithValue("@ERP_VISIBLE", Erp_visible)
                    cm.Parameters.AddWithValue("@ERP_WIDTH", Erp_width)
                    cm.Parameters.AddWithValue("@PEDIDO_VISIBLE", Pedido_visible)
                    cm.Parameters.AddWithValue("@PEDIDO_WIDTH", Pedido_width)
                    cm.Parameters.AddWithValue("@NUMPEDPROVE_VISIBLE", NumPedProve_visible)
                    cm.Parameters.AddWithValue("@NUMPEDPROVE_WIDTH", NumPedProve_width)
                    cm.Parameters.AddWithValue("@FEMISION_VISIBLE", Femision_visible)
                    cm.Parameters.AddWithValue("@FEMISION_WIDTH", Femision_width)
                    cm.Parameters.AddWithValue("@FENTREGA_VISIBLE", Fentrega_visible)
                    cm.Parameters.AddWithValue("@FENTREGA_WIDTH", Fentrega_width)
                    cm.Parameters.AddWithValue("@FENTREGAPROVE_VISIBLE", Fentregaprove_visible)
                    cm.Parameters.AddWithValue("@FENTREGAPROVE_WIDTH", Fentregaprove_width)
                    cm.Parameters.AddWithValue("@ART_VISIBLE", Art_visible)
                    cm.Parameters.AddWithValue("@ART_WIDTH", Art_width)
                    cm.Parameters.AddWithValue("@CANTPED_VISIBLE", Cantped_visible)
                    cm.Parameters.AddWithValue("@CANTPED_WIDTH", Cantped_width)
                    cm.Parameters.AddWithValue("@CANTREC_VISIBLE", Cantrec_visible)
                    cm.Parameters.AddWithValue("@CANTREC_WIDTH", Cantrec_width)
                    cm.Parameters.AddWithValue("@CANTPEND_VISIBLE", Cantpend_visible)
                    cm.Parameters.AddWithValue("@CANTPEND_WIDTH", Cantpend_width)
                    cm.Parameters.AddWithValue("@UNI_VISIBLE", Uni_visible)
                    cm.Parameters.AddWithValue("@UNI_WIDTH", Uni_width)
                    cm.Parameters.AddWithValue("@PREC_VISIBLE", Prec_visible)
                    cm.Parameters.AddWithValue("@PREC_WIDTH", Prec_width)
                    cm.Parameters.AddWithValue("@IMPPED_VISIBLE", Impped_visible)
                    cm.Parameters.AddWithValue("@IMPPED_WIDTH", Imped_width)
                    cm.Parameters.AddWithValue("@IMPREC_VISIBLE", Imprec_visible)
                    cm.Parameters.AddWithValue("@IMPREC_WIDTH", Imprec_width)
                    cm.Parameters.AddWithValue("@IMPPEND_VISIBLE", Imppend_visible)
                    cm.Parameters.AddWithValue("@IMPPEND_WIDTH", Imppend_width)
                    cm.Parameters.AddWithValue("@MON_VISIBLE", Mon_visible)
                    cm.Parameters.AddWithValue("@MON_WIDTH", Mon_width)
                    cm.Parameters.AddWithValue("@CC1_VISIBLE", Cc1_visible)
                    cm.Parameters.AddWithValue("@CC1_WIDTH", Cc1_width)
                    cm.Parameters.AddWithValue("@CC2_VISIBLE", Cc2_visible)
                    cm.Parameters.AddWithValue("@CC2_WIDTH", Cc2_width)
                    cm.Parameters.AddWithValue("@CC3_VISIBLE", Cc3_visible)
                    cm.Parameters.AddWithValue("@CC3_WIDTH", Cc3_width)
                    cm.Parameters.AddWithValue("@CC4_VISIBLE", Cc4_visible)
                    cm.Parameters.AddWithValue("@CC4_WIDTH", Cc4_width)
                    cm.Parameters.AddWithValue("@PARTIDA1_VISIBLE", Partida1_visible)
                    cm.Parameters.AddWithValue("@PARTIDA1_WIDTH", Partida1_width)
                    cm.Parameters.AddWithValue("@PARTIDA2_VISIBLE", Partida2_visible)
                    cm.Parameters.AddWithValue("@PARTIDA2_WIDTH", Partida2_width)
                    cm.Parameters.AddWithValue("@PARTIDA3_VISIBLE", Partida3_visible)
                    cm.Parameters.AddWithValue("@PARTIDA3_WIDTH", Partida3_width)
                    cm.Parameters.AddWithValue("@PARTIDA4_VISIBLE", Partida4_visible)
                    cm.Parameters.AddWithValue("@PARTIDA4_WIDTH", Partida4_width)
                    cm.Parameters.AddWithValue("@BLOQ_FACTURA_VISIBLE", bloq_factura_visible)
                    cm.Parameters.AddWithValue("@BLOQ_FACTURA_WIDTH", bloq_factura_width)
                    cm.Parameters.AddWithValue("@NUM_LINEA_VISIBLE", num_linea_visible)
                    cm.Parameters.AddWithValue("@NUM_LINEA_WIDTH", num_linea_width)
                    cm.Parameters.AddWithValue("@FACTURA_VISIBLE", factura_visible)
                    cm.Parameters.AddWithValue("@FACTURA_WIDTH", factura_width)
                    cm.Parameters.AddWithValue("@RECEPTOR_VISIBLE", receptor_visible)
                    cm.Parameters.AddWithValue("@RECEPTOR_WIDTH", receptor_width)
                    cm.Parameters.AddWithValue("@IMPORTE_ALBARAN_VISIBLE", importe_albaran_visible)
                    cm.Parameters.AddWithValue("@IMPORTE_ALBARAN_WIDTH", importe_albaran_width)
                    cm.Parameters.AddWithValue("@CODRECEPERP_VISIBLE", CODRECEPERP_visible)
                    cm.Parameters.AddWithValue("@CODRECEPERP_WIDTH", CODRECEPERP_width)
                    cm.ExecuteNonQuery()
                Catch ex As SqlException
                    Throw ex
                Finally
                    cn.Close()
                    cn.Dispose()
                    cm.Dispose()
                    dr.Dispose()
                End Try
            End If
        End Sub
#End Region
#Region " Ofertas data access methods "
        ''' Revisado por: auv. Fecha: 06/03/2013
        ''' <summary>
        ''' M�todo que devuelve el nif del proveedor conectado.
        ''' </summary>
        ''' <param name="lCiaProve">Id de proveedor conectado</param>  
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Dataset con el nif del proveedor conectado</returns>
        ''' <remarks>Llamada desde Ofertas.vb->LoadDataProve; M�x inferior a 1seg.</remarks>
        Public Function OfertasProveData_Get(ByVal lCiaProve As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As System.Data.DataSet
            Authenticate(SesionId, IPDir, PersistID)
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm = New SqlCommand
                cm.Connection = cn
                cm.CommandText = "SELECT C.NIF FROM CIAS C WITH(NOLOCK) WHERE ID=" & lCiaProve
                cm.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " OfertasHistoricas data access methods "
        ''' Revisado por: auv. Fecha: 16/01/2013
        ''' <summary>
        ''' M�todo que devuelve las ofertas realizadas por el proveedor.
        ''' </summary>
        ''' <param name="lCiaId">ID de la empresa compradora</param>
        ''' <param name="lCiaProve">ID de la empresa proveedora</param>
        ''' <param name="FecOfeDesde">Fecha a partir de la cual filtrar la fecha de ofertas hist�ricas</param>
        ''' <param name="FecOfeHasta">Fecha hasta la cual filtrar la fecha de ofertas hist�ricas</param>
        ''' <param name="Anio">Filtro por a�o del proceso</param>
        ''' <param name="Gmn1">Filtro por GMN1 del proceso</param>
        ''' <param name="Proce">Filtro por c�digo del proceso</param>
        ''' <param name="DenProce">Filtro por denominaci�n del proceso</param>
        ''' <param name="CodArt">Filtro por c�digo de art�culo</param>
        ''' <param name="DenArt">Filtro por denominaci�n de art�culo</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Dataset con el listado de ofertas historicas solicitado</returns>
        ''' <remarks>Llamada desde ofertasHistoricas.vb->LoadData; M�x inferior a 1seg.</remarks>
        Public Function OfertasHistoricas_Get(ByVal lCiaId As Long, ByVal lCiaProve As Long, ByVal FecOfeDesde As Date, ByVal FecOfeHasta As Date, ByVal Anio As Integer,
                                              ByVal Gmn1 As String, ByVal Proce As Integer, ByVal DenProce As String, ByVal CodArt As String, ByVal DenArt As String,
                                              ByVal EmpresaPortal As Boolean, ByVal NomPortal As String,
                                              Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSP_GET_PROCE_OFE"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaId)
                cm.Parameters.AddWithValue("@CIA_PROVE", lCiaProve)
                cm.Parameters.AddWithValue("@FECOFEDESDE", IIf(FecOfeDesde = Date.MinValue, System.DBNull.Value, FecOfeDesde))
                cm.Parameters.AddWithValue("@FECOFEHASTA", IIf(FecOfeHasta = Date.MinValue, System.DBNull.Value, FecOfeHasta))
                cm.Parameters.AddWithValue("@ANYO", Anio)
                cm.Parameters.AddWithValue("@GMN1", Gmn1)
                cm.Parameters.AddWithValue("@PROCE", Proce)
                cm.Parameters.AddWithValue("@DEN_PROCE", DenProce)
                cm.Parameters.AddWithValue("@COD_ART", CodArt)
                cm.Parameters.AddWithValue("@DEN_ART", DenArt)
                If EmpresaPortal Then cm.Parameters.AddWithValue("@NOM_PORTAL", NomPortal)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch ex As SqlException
                Throw ex
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
        End Function
        ''' Revisado por: auv. Fecha: 16/01/2013
        ''' <summary>
        ''' M�todo que devuelve las ofertas realizadas por el proveedor.
        ''' </summary>
        ''' <param name="lCiaId">ID de la empresa compradora</param>
        ''' <param name="lCiaProve">ID de la empresa proveedora</param>
        ''' <param name="Anio">Filtro por a�o del proceso</param>
        ''' <param name="Gmn1">Filtro por GMN1 del proceso</param>
        ''' <param name="Proce">Filtro por c�digo del proceso</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Dataset con el listado de ofertas historicas solicitado</returns>
        ''' <remarks>Llamada desde ofertasHistoricas.vb->LoadData; M�x inferior a 1seg.</remarks>
        Public Function OfertaHistoricaAdjun_Get(ByVal lCiaId As Long, ByVal lCiaProve As Long, ByVal Anio As Integer, ByVal Gmn1 As String, ByVal Proce As Integer, ByVal NumOfe As Integer,
                                                 Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Long
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSP_GET_PROCE_OFE_ADJUN"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaId)
                cm.Parameters.AddWithValue("@CIA_PROVE", lCiaProve)
                cm.Parameters.AddWithValue("@ANYO", Anio)
                cm.Parameters.AddWithValue("@GMN1", Gmn1)
                cm.Parameters.AddWithValue("@PROCE", Proce)
                cm.Parameters.AddWithValue("@NUMOFE", NumOfe)
                cm.Parameters("@ID").Direction = ParameterDirection.Output
                cm.ExecuteNonQuery()
                Return cm.Parameters("@ID").Value
            Catch e As Exception
                Throw e
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
        End Function
        'Obtiene una lista de los diferentes a�os para el proveedor
        Public Function OfertasHistoricasAnyos_Get(ByVal lCiaId As Long, ByVal lCiaProve As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                FSGS(lCiaId)
                cm.CommandText = "SELECT DISTINCT PO.ANYO FROM " & FSGS(lCiaId) & "PROCE_OFE PO WITH (NOLOCK) WHERE PO.PROVE=(SELECT COD FROM " & FSGS(lCiaId) &
                    "PROVE WITH (NOLOCK) WHERE FSP_COD=(SELECT COD FROM CIAS WITH (NOLOCK) WHERE ID=" & lCiaProve & "))"
                cm.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
        'Obtiene una lista de los diferentes GMN1 para el proveedor
        Public Function OfertasHistoricasGmn1s_Get(ByVal lCiaId As Long, ByVal lCiaProve As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                FSGS(lCiaId)
                cm.CommandText = "SELECT DISTINCT PO.GMN1 FROM " & FSGS(lCiaId) & "PROCE_OFE PO WITH (NOLOCK) WHERE PO.PROVE=(SELECT COD FROM " & FSGS(lCiaId) &
                    "PROVE WITH (NOLOCK) WHERE FSP_COD=(SELECT COD FROM CIAS WITH (NOLOCK) WHERE ID=" & lCiaProve & "))"
                cm.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
#Region " Parametros data access methods"
        ''' Revisado por: blp. Fecha: 31/10/2011
        ''' <summary>
        ''' Funci�n que devuelve un texto concreto de la tabla PARGEN_LIT para el idioma requerido
        ''' </summary>
        ''' <param name="lId">Id del literal</param>
        ''' <param name="sIdi">C�dgio de idioma</param>
        ''' <param name="lCiaComp">ID de la empresa</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>  
        ''' <returns>Devuelve el texto concreto de la tabla PARGEN_LIT para el idioma requerido</returns>
        ''' <remarks>
        ''' Llamada desde: CParametros.CargarLiteralParametros
        ''' Tiempo m�ximo: 0 sec.
        ''' </remarks>
        Public Function Parametros_CargarLiteralParametros(ByVal lId As Long, ByVal sIdi As String, ByVal lCiaComp As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As String
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSP_OBT_PARAM_LITERAL"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDIOMA", sIdi)
                cm.Parameters.AddWithValue("@IDLIT", lId)
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr.Tables(0).Rows(0).Item(0).ToString
            Catch ex As Exception
                Throw ex
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
        End Function
#End Region
#Region " Parametros Generales data access methods "
        ''' Revisado por:blp. Fecha: 29/05/2012
        ''' <summary>
        ''' Funcion que devuelve los par�metros generales de la instalaci�n
        ''' </summary>
        ''' <param name="CiaComp">C�digo de la empresa cliente</param>
        ''' <returns>Un objeto del tipo ParametrosGenerales con los que correspondan de la aplicaci�n</returns>
        ''' <remarks>
        ''' Llamada desdE: PmServer/root/GetAccesType
        ''' Tiempo m�ximo: 0,15 seg</remarks>
        Public Function Parametros_GetData(ByVal CiaComp As String) As Data.DataSet
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_PARGEN_INTERNO"
                cm.Parameters.AddWithValue("@CIA", CiaComp)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Funcion que devuelve los parametros generales
        ''' </summary>
        ''' <param name="lCiaComp">id de la compa�ia</param>
        ''' <param name="usercode"></param>
        ''' <param name="userpassword"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Parametros_CargarParametrosGenerales(ByVal lCiaComp As Long, Optional ByVal usercode As String = "", Optional ByVal userpassword As String = "") As DataSet
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSP_GET_PARAMETROS"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr
            Catch ex As Exception
                Throw ex
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                dr.Dispose()
            End Try
        End Function
        ''' <summary>Devuelve el valor del par�metro PARGEN_INTERNO.ACCESO_EXTERNO</summary>
        ''' <returns>llamada desde: PMPortalServer.Root.Load_AccesoServidorExterno</returns>
        Public Function Parametros_GetAccesoServidorExterno() As Boolean
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim ds As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSP_GET_ACCESOEXTERNO"
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(ds)

                Return SQLBinaryToBoolean(ds.Tables(0).Rows(0)("ACCESO_EXTERNO"))
            Catch ex As Exception
                Throw ex
            Finally
                cn.Close()
                cn.Dispose()
                cm.Dispose()
                ds.Dispose()
            End Try
        End Function

#End Region
#Region "Pedido data access methods"
        Function Pedido_DevolverDetalle(ByVal lCiaComp As Long, ByVal lID As Long, ByVal sIdioma As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_GET_DETALLE_PEDIDO"
                cm.Parameters.AddWithValue("@ID", lID)
                cm.Parameters.AddWithValue("@IDI", sIdioma)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region "PRES5 data access methods"
        Public Function Partidas_LoadPartidasInstalacion(ByVal lCiaComp As Long, ByVal sIdioma As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim sCommand As String = String.Empty
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_DEVOLVER_PARTIDAS"
                cm.Parameters.AddWithValue("@IDIOMA", sIdioma)
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dt)

                Return dt
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Devuelve las partidas y las uons para mostrarlas en un treeview. A�ade las relaciones a las tablas del dataset.
        ''' </summary>
        ''' <param name="sPRES5">C�digo de la partida presupuetaria</param>
        ''' <param name="sUsuario">C�digo de la persona</param>
        ''' <param name="sIdioma">Idioma del usuario</param>
        ''' <param name="sCentroCoste">C�digo del centro de coste del que se quieren mostrar las partidas</param>
        ''' <param name="bVigentes">Si true, muestra las partidas vigentes, si false, todas las partidas</param>
        ''' <param name="dFechaInicioDesde">Fecha de inicio desde para buscar partidas</param>
        ''' <param name="dFechaFinHasta">Fecha fin hasta para buscar partidas</param>
        ''' <param name="sFormatoFecha">Formato en el que llegan las fechas</param>
        ''' <returns>Devuelve un dataset</returns>
        ''' <remarks>Llamada desde: SMServer --> PRES5.vb --> Partidas_LoadData; Tiempo m�ximo: 1 sg;</remarks>
        Public Function Partidas_Load(ByVal lCiaComp As Long, ByVal sPRES5 As String, ByVal sUsuario As String, ByVal sIdioma As String,
                                      Optional ByVal sCentroCoste As String = Nothing, Optional ByVal bVigentes As Boolean = True, Optional ByVal dFechaInicioDesde As Date = Nothing,
                                      Optional ByVal dFechaFinHasta As Date = Nothing, Optional ByVal sFormatoFecha As String = Nothing,
                                      Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim index As Integer = 1
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSSM_DEVOLVER_PARTIDAS"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@PRES5", sPRES5)
                cm.Parameters.AddWithValue("@IDI", sIdioma)
                If sCentroCoste <> Nothing Then
                    Dim sUONs As Object = Split(sCentroCoste, "#")
                    cm.Parameters.AddWithValue("@UON1", sUONs(0))
                    If UBound(sUONs) >= 1 Then
                        cm.Parameters.AddWithValue("@UON2", sUONs(1))
                        If UBound(sUONs) >= 2 Then
                            cm.Parameters.AddWithValue("@UON3", sUONs(2))
                            If UBound(sUONs) = 3 Then
                                cm.Parameters.AddWithValue("@UON4", sUONs(3))
                            End If
                        End If
                    End If
                End If
                cm.Parameters.AddWithValue("@VIGENTES", bVigentes)
                If dFechaInicioDesde <> Nothing Then cm.Parameters.AddWithValue("@FECHAINICIODESDE", dFechaInicioDesde)
                If dFechaFinHasta <> Nothing Then cm.Parameters.AddWithValue("@FECHAFINHASTA", dFechaFinHasta)
                cm.Parameters.AddWithValue("@FORMATOFECHA", sFormatoFecha)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)

                Dim i As Byte
                For Each dt As DataTable In dr.Tables
                    dt.TableName = "Tabla_" & i.ToString
                    i += 1
                Next

                '******************************************************************
                Dim parentCols(0) As DataColumn
                Dim childCols(0) As DataColumn
                Dim dtPk0(0) As DataColumn
                Dim dtPk1(1) As DataColumn
                Dim dtPk2(2) As DataColumn
                Dim dtPk3(3) As DataColumn
                Dim dtPk4(4) As DataColumn
                Dim dtPk5(5) As DataColumn

                'Establezco las claves primarias de las tablas de las UONS
                dtPk0(0) = dr.Tables(0).Columns("COD0")
                dr.Tables(0).PrimaryKey = dtPk0
                dtPk1(0) = dr.Tables(1).Columns("COD0")
                dtPk1(1) = dr.Tables(1).Columns("COD")
                dr.Tables(1).PrimaryKey = dtPk1
                dtPk2(0) = dr.Tables(2).Columns("COD0")
                dtPk2(1) = dr.Tables(2).Columns("UON1")
                dtPk2(2) = dr.Tables(2).Columns("COD")
                dr.Tables(2).PrimaryKey = dtPk2
                dtPk3(0) = dr.Tables(3).Columns("COD0")
                dtPk3(1) = dr.Tables(3).Columns("UON1")
                dtPk3(2) = dr.Tables(3).Columns("UON2")
                dtPk3(3) = dr.Tables(3).Columns("COD")
                dr.Tables(3).PrimaryKey = dtPk3
                dtPk4(0) = dr.Tables(4).Columns("COD0")
                dtPk4(1) = dr.Tables(4).Columns("UON1")
                dtPk4(2) = dr.Tables(4).Columns("UON2")
                dtPk4(3) = dr.Tables(4).Columns("UON3")
                dtPk4(4) = dr.Tables(4).Columns("COD")
                dr.Tables(4).PrimaryKey = dtPk4
                dtPk5(0) = dr.Tables(5).Columns("COD0")
                dtPk5(1) = dr.Tables(5).Columns("UON1")
                dtPk5(2) = dr.Tables(5).Columns("UON2")
                dtPk5(3) = dr.Tables(5).Columns("UON3")
                dtPk5(4) = dr.Tables(5).Columns("UON4")
                dtPk5(5) = dr.Tables(5).Columns("COD")
                dr.Tables(5).PrimaryKey = dtPk5

                parentCols(0) = dr.Tables(0).Columns("COD0")
                childCols(0) = dr.Tables(1).Columns("COD0")

                dr.Relations.Add("RELACION_1", parentCols, childCols, False)

                If dr.Tables(2).Rows.Count > 0 Then
                    ReDim parentCols(0)
                    ReDim childCols(0)
                    index = 2

                    parentCols(0) = dr.Tables(1).Columns("COD")
                    childCols(0) = dr.Tables(2).Columns("UON1")

                    dr.Relations.Add("RELACION_2", parentCols, childCols, False)

                    If dr.Tables(3).Rows.Count > 0 Then
                        ReDim parentCols(1)
                        ReDim childCols(1)
                        index = 3

                        parentCols(0) = dr.Tables(2).Columns("UON1")
                        parentCols(1) = dr.Tables(2).Columns("COD")
                        childCols(0) = dr.Tables(3).Columns("UON1")
                        childCols(1) = dr.Tables(3).Columns("UON2")
                        dr.Relations.Add("RELACION_3", parentCols, childCols, False)

                        If dr.Tables(4).Rows.Count > 0 Then
                            ReDim parentCols(2)
                            ReDim childCols(2)
                            index = 4

                            parentCols(0) = dr.Tables(3).Columns("UON1")
                            parentCols(1) = dr.Tables(3).Columns("UON2")
                            parentCols(2) = dr.Tables(3).Columns("COD")
                            childCols(0) = dr.Tables(4).Columns("UON1")
                            childCols(1) = dr.Tables(4).Columns("UON2")
                            childCols(2) = dr.Tables(4).Columns("UON3")
                            dr.Relations.Add("RELACION_4", parentCols, childCols, False)
                        End If
                    End If
                End If
                ReDim parentCols(3)
                ReDim childCols(3)

                parentCols(0) = dr.Tables(index).Columns("UON1")
                parentCols(1) = dr.Tables(index).Columns("UON2")
                parentCols(2) = dr.Tables(index).Columns("UON3")
                parentCols(3) = dr.Tables(index).Columns("UON4")
                childCols(0) = dr.Tables(5).Columns("UON1")
                childCols(1) = dr.Tables(5).Columns("UON2")
                childCols(2) = dr.Tables(5).Columns("UON3")
                childCols(3) = dr.Tables(5).Columns("UON4")
                dr.Relations.Add("RELACION_" & index + 1, parentCols, childCols, False)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region " Tipos Pedido data access methods "
        ''' <summary>
        ''' Carga en un dataset los tipos de pedido
        ''' </summary>
        ''' <returns>Dataset con los tipos de pedido</returns>
        ''' <remarks>Llamada desde: TiposPedidoServer.aspx.vb; Tiempo m�ximo: 0,1 sg</remarks>
        Public Function TiposPedido_Get(ByVal lCiaComp As Long, ByVal sIdioma As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_TIPOSPEDIDO"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@IDI", sIdioma)
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region " ViasPago data access methods "
        ''' <summary>
        ''' Cargar las v�as de pago
        ''' </summary>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCod">Codigo de forma</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>    
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\FormasPago.vb\LoadData         PMPortalNotificador\Notificar.vb/ObtenerDenominacionValorCampoGS; Tiempo m�ximo: 0,2</remarks>
        Public Function ViasPago_Get(ByVal sIdi As String, ByVal lCiaComp As Long, Optional ByVal sCod As String = Nothing, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSPM_VIASPAGO"
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                If sCod <> Nothing Then cm.Parameters.AddWithValue("@COD", sCod)
                cm.Parameters.AddWithValue("@IDIOMA", sIdi)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return dr
        End Function
#End Region
        Public Function Servicio_Load(ByVal lCiaComp As Integer, ByVal Servicio As String, ByVal CampoId As String, ByVal Instancia As Long,
                                      Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_OBTENER_SERVICIO"
                cm.Parameters.AddWithValue("@SERVICIO", Servicio)
                cm.Parameters.AddWithValue("@CAMPOID", CampoId)
                cm.Parameters.AddWithValue("@INSTANCIA", Instancia)
                cm.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)

                dr.Tables(0).TableName = "URL"
                dr.Tables(1).TableName = "PARAM_ENTRADA"
                dr.Tables(2).TableName = "PARAM_SALIDA"
                If dr.Tables.Count > 3 Then
                    dr.Tables(3).TableName = "PARAM_PETICIONARIO"
                    dr.Tables(4).TableName = "PARAMETROS_ENTRADA_GENERICOS"
                End If

                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
#End Region
#Region " Security "
        ''' <summary>
        ''' Comprueba si es usuario valido
        ''' </summary>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="CodUser">Codigo de usuario</param>
        ''' <param name="PasswordUser">Password de usuario</param>
        ''' <param name="CiaId">Id de compania</param>
        ''' <param name="CiaCod">Codigo de compania</param> 
        ''' <param name="CiaComp">Codigo de compania compradora</param>     
        ''' <param name="AccesoCn">si el usuario tiene cn o no</param>
        ''' <returns>Si es usuario valido devuelve true, sino false</returns>
        ''' <remarks>Llamada desde: Page_PreInit; Tiempo maximo:0,2</remarks>
        Public Function Login(ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByRef CodUser As String, ByRef PasswordUser As String, ByRef CiaId As Long,
                              ByRef CiaCod As String, ByRef CiaComp As Long, ByRef AccesoCn As Boolean, ByRef CodProveGS As String) As Boolean
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim bLoginOK As Boolean = False
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSP_LOGIN_SOBRETABLA_SESION"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@SESIONID", SesionId)
                cm.Parameters.AddWithValue("@IPDIR", IPDir)
                cm.Parameters.AddWithValue("@PERSISTID", PersistID)

                Dim dr As SqlDataReader = cm.ExecuteReader()
                If dr.Read() Then
                    bLoginOK = True

                    mUserCod = dr.Item("USUCOD").ToString()
                    mUserPassword = dr.Item("PWD").ToString() 'La guardamos encriptada con la fecha fija
                    mUserCiaCod = dr.Item("CIACOD").ToString()
                    mUserCiaId = dr.Item("CIAID")
                    mUserCiaComp = dr.Item("CIACOMP")

                    mUserCn = SQLBinaryToBoolean(dr.Item("CN"))
                    CodProveGS = dr.Item("CIACODGS")

                    CodUser = mUserCod
                    PasswordUser = mUserPassword
                    CiaId = mUserCiaId
                    CiaCod = mUserCiaCod
                    CiaComp = mUserCiaComp

                    AccesoCn = mUserCn

                    dr.Close()
                Else
                    bLoginOK = False
                End If
            Catch e As Exception
                bLoginOK = False
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
            Return bLoginOK
        End Function
        ''' <summary>
        ''' Authenticate las funciones del root
        ''' </summary>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <remarks>Llamada desde: Todas las funciones de esta panatalla; tiempo maximo:0 </remarks>
        Private Sub Authenticate(ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String)
            If SesionId <> "" Then
                'Single call mode. Must authenticate
                Dim usercode As String = ""
                Dim userpassword As String = ""
                Dim ciaId As Long = 0
                Dim ciaCode As String = ""
                Dim ciaComp As Long = 0
                Dim AccesoCN As Boolean
                Dim CodProveGS As String = ""
                If Not Login(SesionId, IPDir, PersistID, usercode, userpassword, ciaId, ciaCode, ciaComp, AccesoCN, CodProveGS) Then
                    Throw New Security.SecurityException("User not authenticated!")
                End If
            Else
                If mUserCod = "" Then
                    'No ha habido remoting y no ha pasado por el m�todo Login
                    Throw New Security.SecurityException("User not authenticated!")
                End If
            End If
        End Sub
#End Region
#Region " Constructor "
        Public Sub New()
            'Comprobar tema de licencias y cargar datos de conexi�n
            Dim sDB As String = ConfigurationSettings.AppSettings("DB:FS_WS")
            If sDB Is Nothing Then
                Err.Raise(10000, , "Configuration file corrupted. Missing FS_WS entry !")
            End If
            'Tenemos que extraer el login y la contrase�a de conexi�n.
            Dim doc As New XmlDocument
            Dim sdblogin As String = ""
            Dim sdbPassword As String = ""
            Try
                doc.Load(System.AppDomain.CurrentDomain.BaseDirectory() & "License.xml")
            Catch
                Err.Raise(10000, , "LICENSE file missing!")
            End Try
            Dim child As XmlNode
            child = doc.SelectSingleNode("/LICENSE/DB_LOGIN")
            If Not (child Is Nothing) Then
                mDBLogin = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
                'mDBLogin = child.InnerText
            End If
            child = Nothing
            child = doc.SelectSingleNode("/LICENSE/DB_PASSWORD")
            If Not (child Is Nothing) Then
                mDBPassword = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
                'mDBPassword = child.InnerText
            Else
                mDBPassword = ""
            End If
            child = doc.SelectSingleNode("/LICENSE/DB_NAME")
            If Not (child Is Nothing) Then
                mDBName = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
            End If
            child = Nothing
            child = doc.SelectSingleNode("/LICENSE/DB_SERVER")
            If Not (child Is Nothing) Then
                mDBServer = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
            End If
            child = Nothing
            child = doc.SelectSingleNode("/LICENSE/IMP_USER")
            If Not (child Is Nothing) Then
                mImpersonateLogin = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
            End If
            child = Nothing
            child = doc.SelectSingleNode("/LICENSE/IMP_PWD")
            If Not (child Is Nothing) Then
                mImpersonatePassword = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
            Else
                mImpersonatePassword = ""
            End If
            child = Nothing
            child = doc.SelectSingleNode("/LICENSE/IMP_DOM")
            If Not (child Is Nothing) Then
                mImpersonateDominio = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
            End If
            If mDBServer = "" Or mDBName = "" Or mDBLogin = "" Then
                Err.Raise(10000, , "Corrupted LICENSE file !")
            End If

            'PENDIENTE METER ENCRIPTACI�N
            sDB = sDB.Replace("DB_LOGIN", mDBLogin)
            sDB = sDB.Replace("DB_PASSWORD", mDBPassword)
            sDB = sDB.Replace("DB_NAME", mDBName)
            sDB = sDB.Replace("DB_SERVER", mDBServer)
            doc = Nothing
            mDBConnection = sDB

        End Sub
#End Region
#Region " Data structures"
        <Serializable()>
        Public Structure UserIdentity_Actions
            Public ID() As Integer
        End Structure
#End Region
#Region " FileStream"
        ''' <summary>
        ''' Funcion que devuelve un array de strings con las reglas del proceso
        ''' </summary>
        ''' <param name="anyo">A�o del proceso</param>
        ''' <param name="gmn1">Grupo de Material del Nivel 1 del proceso</param>
        ''' <param name="proce">Identificador del proceso</param>
        ''' <param name="lCiaComp">Identificador de la compa��a en el portal</param>
        ''' <returns>Un array de strings con el formato nombrefichero|identificador de cada archivo que est� publicado como regla de la subasta</returns>
        ''' <remarks>
        ''' Llamada desde: 
        ''' Tiempo m�ximo: </remarks>
        Public Function ReglasSubastaProce(ByVal anyo As Long, ByVal gmn1 As String, ByVal proce As Long, ByVal lCiaComp As Long) As String()
            Dim reglas As String()
            ReDim reglas(0)
            Dim sBBDD As String = mDBName
            Dim sServidor As String = mDBServer

            'Obtener el servidor y la base de datos de GS para el acceso con seguridad integrada
            Dim datosGS As String
            datosGS = Replace(FSGS(lCiaComp), ".dbo.", "") : datosGS = Replace(datosGS, "[", "") : datosGS = Replace(datosGS, "]", "")
            If InStr(datosGS, ".") > 0 Then
                sServidor = Split(datosGS, ".")(0)
                sBBDD = Split(datosGS, ".")(1)
            Else
                sBBDD = datosGS
            End If

            Dim sCadena As String = "Integrated Security=true;server=" & sServidor & ";initial catalog=" & sBBDD
            Dim sqlConnection As New SqlConnection(sCadena)
            Dim sqlCommand As New SqlCommand()
            sqlCommand.Connection = sqlConnection
            Try
                sqlConnection.Open()
                sqlCommand.Parameters.AddWithValue("@ANYO", anyo)
                sqlCommand.Parameters.AddWithValue("@GMN1", gmn1)
                sqlCommand.Parameters.AddWithValue("@PROCE", proce)
                'Retrieve the file path of the SQL FILESTREAM BLOB in the first row. 
                sqlCommand.CommandText = "SELECT NOM, ADJUN_PROCE FROM " & FSGS(lCiaComp) & "PROCE_SUBASTA_REGLAS WITH (NOLOCK) WHERE ANYO= @ANYO AND GMN1 = GMN1 AND PROCE = @PROCE"

                Dim dr As New DataSet
                sqlCommand.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter
                da.SelectCommand = sqlCommand
                da.Fill(dr)

                Dim oPRow As DataRow
                If dr.Tables(0).Rows.Count > 0 Then
                    For Each oPRow In dr.Tables(0).Rows
                        ReDim Preserve reglas(UBound(reglas) + 1)
                        reglas(UBound(reglas) - 1) = oPRow.Item(1).ToString & "|" & oPRow.Item(0).ToString
                    Next
                End If

                ReglasSubastaProce = reglas
            Catch e As Exception
                Throw e
            Finally
                sqlConnection.Dispose()
                sqlCommand.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Procedimiento que devuelve un fichero adjunto a un proceso de la base de datos en formato filestream
        ''' </summary>
        ''' <param name="idadjun">Identificador del fichero adjunto en la tabla ADJUN_PROCE del GS</param>
        ''' <returns >buffer con el contenido del fichero </returns>
        ''' <remarks>
        ''' Llamada desde: Adjunto.vb -> LeerReglasSubastaProce()
        ''' Tiempo m�ximo: </remarks>
        Public Function LeerAdjunProce(ByVal IdAdjun As Long, ByVal lCiaComp As Long) As Byte()
            Dim sBBDD As String = mDBName
            Dim sServidor As String = mDBServer

            'Obtener el servidor y la base de datos de GS para el acceso con seguridad integrada
            Dim datosGS As String
            datosGS = Replace(FSGS(lCiaComp), ".dbo.", "") : datosGS = Replace(datosGS, "[", "") : datosGS = Replace(datosGS, "]", "")
            If InStr(datosGS, ".") > 0 Then
                sServidor = Split(datosGS, ".")(0)
                sBBDD = Split(datosGS, ".")(1)
            Else
                sBBDD = datosGS
            End If

            Dim sCadena As String = "Integrated Security=true;server=" & sServidor & ";initial catalog=" & sBBDD
            Dim sqlConnection As New SqlConnection(sCadena)
            Dim sqlCommand As New SqlCommand()

            sqlCommand.Connection = sqlConnection
            Try
                sqlConnection.Open()
                'Retrieve the file path of the SQL FILESTREAM BLOB in the first row. 
                sqlCommand.CommandText = "SELECT DATA.PathName() FROM " & FSGS(lCiaComp) & "ADJUN_PROCE A_P WITH (NOLOCK) WHERE A_P.ID=" & IdAdjun
                Dim filePath As String = Nothing

                Dim pathObj As Object = sqlCommand.ExecuteScalar()
                If Not pathObj.Equals(DBNull.Value) Then
                    filePath = DirectCast(pathObj, String)
                Else
                    Throw New System.Exception("DATA.PathName() ADJUN_PROCE failed to read the path name for the ADJUN_PROCE column.")
                End If

                ' Obtain a transaction context. All FILESTREAM BLOB operations occur within a transaction context to maintain data consistency. 
                Dim transaction As SqlTransaction = sqlConnection.BeginTransaction("mainTranaction")
                sqlCommand.Transaction = transaction

                sqlCommand.CommandText = "SELECT GET_FILESTREAM_TRANSACTION_CONTEXT()"
                Dim txContext As Byte() = Nothing
                Dim obj As Object = sqlCommand.ExecuteScalar()

                If Not obj.Equals(DBNull.Value) Then
                    txContext = DirectCast(obj, Byte())
                Else
                    Throw New System.Exception("GET_FILESTREAM_TRANSACTION_CONTEXT() failed")
                End If

                Dim sqlFileStream As New SqlTypes.SqlFileStream(filePath, txContext, FileAccess.Read)
                Dim buffer As Byte() = New Byte(sqlFileStream.Length - 1) {}

                sqlFileStream.Read(buffer, 0, buffer.Length)

                ' Close the FILESTREAM handle. 
                sqlFileStream.Close()
                sqlCommand.Transaction.Commit()

                LeerAdjunProce = buffer
            Catch e As Exception
                Throw e
            Finally
                sqlConnection.Dispose()
                sqlCommand.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Funcion que devuelve un string para la generaci�n de carpetas temporales
        ''' </summary>
        ''' <returns>String aleatorio de longitud 10</returns>
        ''' <remarks>
        ''' Llamada desde: 
        ''' Tiempo m�ximo: </remarks>
        Public Function GenerateRandomPath() As String
            Dim s As String = ""
            Dim i As Integer
            Dim lowerbound As Integer = 65
            Dim upperbound As Integer = 90
            Randomize()
            For i = 1 To 10
                s = s & Chr(Int((upperbound - lowerbound + 1) * Rnd() + lowerbound))
            Next
            Return s
        End Function
#End Region
#Region "Integracion"
        ''' Revisado por: blp. Fecha: 03/02/2012
        ''' <summary>
        ''' Funci�n que nos devuelve si existe integraci�n de entrada o de entrada/salida para una entidad concreta
        ''' </summary>
        ''' <param name="iTabla">Id de Tabla en TABLAS_INTEGrACION_ERP </param>
        ''' <returns>Devuelve booleano</returns>
        ''' <remarks>Llamada desde ValidacionesIntegracion.vb\HayIntegracionEntredaoentradaSalida
        ''' Tiempo maximo 0,1 sec</remarks>
        Public Function HayIntegracionEntradaoEntradaSalida(ByVal iTabla As Integer, ByVal lCiaComp As Long) As Boolean
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Dim bRes As Boolean
            bRes = False
            Try
                cm.Connection = cn
                'Si est� activa la integraci�n de pedidos en sentido Salida o entrada<->salida:
                cm.CommandText = "SELECT FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA AND PORT =0"
                cm.CommandType = CommandType.Text
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                Dim sServer As String = String.Empty
                cn.Open()
                sServer = cm.ExecuteScalar

                cm.Dispose()
                cn.Close()
                cm = New SqlCommand
                cm.Connection = cn
                If Not sServer = String.Empty Then
                    cm.CommandText = "SELECT TOP 1 SENTIDO FROM " & sServer & "TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=@TABLA AND SENTIDO IN (2,3) AND ACTIVA=1"
                    cm.CommandType = CommandType.Text
                    cm.Parameters.AddWithValue("@TABLA", iTabla)
                    da.SelectCommand = cm
                    cn.Open()
                    da.Fill(ds)

                    bRes = (ds.Tables(0).Rows.Count > 0)
                End If

                Return bRes
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' Revisado por: jim 30/06/2014
        ''' <summary>
        ''' Funci�n que nos devuelve si existe integraci�n de salida o de entrada/salida para una entidad concreta
        ''' </summary>
        ''' <param name="iTabla">Id de Tabla en TABLAS_INTEGrACION_ERP </param>
        ''' <returns>Devuelve booleano</returns>
        ''' <remarks>Llamada desde ValidacionesIntegracion.vb\HayIntegracionEntredaoentradaSalida
        ''' Tiempo maximo 0,1 sec</remarks>
        Public Function HayIntegracionSalidaoEntradaSalida(ByVal iTabla As Integer, ByVal lCiaComp As Long) As Boolean
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Dim bRes As Boolean
            bRes = False
            Try
                cm.Connection = cn
                'Si est� activa la integraci�n de pedidos en sentido Salida o entrada<->salida:
                cm.CommandText = "SELECT FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA AND PORT =0"
                cm.CommandType = CommandType.Text
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                Dim sServer As String = String.Empty
                cn.Open()
                sServer = cm.ExecuteScalar

                cm.Dispose()
                cn.Close()
                cm = New SqlCommand
                cm.Connection = cn
                If Not sServer = String.Empty Then
                    cm.CommandText = "SELECT TOP 1 SENTIDO FROM " & sServer & "TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=@TABLA AND SENTIDO IN (1,3) AND ACTIVA=1"
                    cm.CommandType = CommandType.Text
                    cm.Parameters.AddWithValue("@TABLA", iTabla)
                    da.SelectCommand = cm
                    cn.Open()
                    da.Fill(ds)

                    bRes = (ds.Tables(0).Rows.Count > 0)
                End If

                Return bRes
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region "Validaciones integracion"
        ''' <summary>
        ''' Función que nos dice si hay integracion Activa para una entidad concreta
        ''' </summary>
        ''' <param name="iTabla">Id de Tabla en TABLAS_INTEGrACION_ERP </param>
        ''' <returns>Devuelve booleano</returns>
        ''' <remarks>Llamada desde Integracion.vb\HayIntegracion
        ''' Tiempo maximo 0 sec</remarks>
        Public Function HayIntegracion(ByVal lCiaComp As Long, ByVal iTabla As Integer, Optional ByVal iSentido As Short = SentidoIntegracion.SinSentido) As Boolean
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Dim bRes As Short
            Try
                bRes = False

                cm.Connection = cn
                cm.CommandText = "FSPM_HAY_INTEGRACION"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@CIA", lCiaComp)
                cm.Parameters.AddWithValue("@SENTIDO", iSentido)
                cm.Parameters.AddWithValue("@TABLA", iTabla)
                cm.Parameters.AddWithValue("@RESP", 0)
                cm.Parameters("@RESP").Direction = ParameterDirection.Output
                cm.Parameters("@RESP").Size = -1
                da.SelectCommand = cm
                da.Fill(ds)

                bRes = DBNullToDbl(cm.Parameters("@RESP").Value())
                If bRes > 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region "CFacturae data access methods "
        ''' <summary>
        ''' Se extrae informaci�n general de la factura
        ''' </summary>
        ''' <returns>DataTable con la informaci�n solicitada</returns>
        ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
        ''' Tiempo m�ximo: menor a 0,5 seg</remarks>
        Public Function obtDatosFactura(ByVal lCiaComp As Long, ByRef idFact As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = FSGS(lCiaComp) & "FSPM_GET_DATOS_GENE_FACTURA"
                cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
                cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
                cmd.Parameters("@FACT_ID").Value = idFact
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Se extrae informaci�n relativa a los costes y descuentos a nivel de factura de la factura par�metro
        ''' </summary>
        ''' <param name="idFact">Identificador de la factura de la cual se extraer�n los datos</param>
        ''' <returns>DataTable con la informaci�n solicitada</returns>
        ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
        ''' Tiempo m�ximo: menor a 0,5 seg</remarks>
        Public Function obtCostesYDescuentosFactura(ByVal lCiaComp As Long, ByVal idFact As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = FSGS(lCiaComp) & "FSPM_GET_COSTYDESC_FACTURA"
                cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
                cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
                cmd.Parameters("@FACT_ID").Value = idFact
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Se extrae informaci�n relativa a los impuestos de la factura par�metro
        ''' </summary>
        ''' <param name="idFact">Identificador de la factura de la cual se extraer�n los datos</param>
        ''' <returns>DataTable con la informaci�n solicitada</returns>
        ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
        ''' Tiempo m�ximo: menor a 0,5 seg</remarks>
        Public Function obtImpuestosRepercutidosRetenidosFactura(ByVal lCiaComp As Long, ByVal idFact As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = FSGS(lCiaComp) & "FSPM_GET_IMP_REPYRET_FACTURA"
                cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
                cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
                cmd.Parameters("@FACT_ID").Value = idFact
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Se extrae la informaci�n general del proveedor, el comprador, de la factura par�metro
        ''' </summary>
        ''' <param name="idFact">Identificador de la factura de la cual se extraer�n los datos</param>
        ''' <param name="psIdioma">Algunos datos a extraer son multiidioma y deben extraerse en el idioma par�metro</param>
        ''' <returns>DataTable con la informaci�n solicitada</returns>
        ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
        ''' Tiempo m�ximo: menor a 0,5 seg</remarks>
        Public Function obtDatosGeneralesProveedor(ByVal lCiaComp As Long, ByVal idFact As String, ByVal psIdioma As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = FSGS(lCiaComp) & "FSPM_GET_DATOS_GENE_PROVE_FACTURA"
                cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
                cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
                cmd.Parameters("@FACT_ID").Value = idFact
                cmd.Parameters.Add("@FS_IDI_COD", SqlDbType.NVarChar, 3)
                cmd.Parameters("@FS_IDI_COD").Direction = ParameterDirection.Input
                cmd.Parameters("@FS_IDI_COD").Value = psIdioma
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Se extrae la informaci�n de contacto del proveedor, el comprador, de la factura par�metro
        ''' </summary>
        ''' <param name="idFact">Identificador de la factura de la cual se extraer�n los datos</param>
        ''' <returns>DataTable con la informaci�n solicitada</returns>
        ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
        ''' Tiempo m�ximo: menor a 0,5 seg</remarks>
        Public Function obtDatosDeContactoProveedor(ByVal lCiaComp As Long, ByVal idFact As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = FSGS(lCiaComp) & "FSPM_GET_DATOS_CONT_PROVE_FACTURA"
                cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
                cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
                cmd.Parameters("@FACT_ID").Value = idFact
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        '''  Se extrae la informaci�n general de la empresa, el comprador, de la factura par�metro
        ''' </summary>
        ''' <param name="idFact">Identificador de la factura de la cual se extraer�n los datos</param>
        ''' <param name="psIdioma">Algunos datos a extraer son multiidioma y deben extraerse en el idioma par�metro</param>
        ''' <returns>DataTable con la informaci�n solicitada</returns>
        ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
        ''' Tiempo m�ximo: menor a 0,5 seg</remarks>
        Public Function obtDatosGeneralesComprador(ByVal lCiaComp As Long, ByVal idFact As String, ByVal psIdioma As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = FSGS(lCiaComp) & "FSPM_GET_DATOS_GENE_COMP_FACTURA"
                cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
                cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
                cmd.Parameters("@FACT_ID").Value = idFact
                cmd.Parameters.Add("@FS_IDI_COD", SqlDbType.NVarChar, 3)
                cmd.Parameters("@FS_IDI_COD").Direction = ParameterDirection.Input
                cmd.Parameters("@FS_IDI_COD").Value = psIdioma
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        '''  Se extrae la informaci�n general de los items de la factura par�metro
        ''' </summary>
        ''' <param name="idFact">Identificador de la factura de la cual se extraer�n los datos</param>
        ''' <returns>DataTable con la informaci�n solicitada</returns>
        ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
        ''' Tiempo m�ximo: menor a 0,5 seg</remarks>
        Public Function obtDatosGeneralesItems(ByVal lCiaComp As Long, ByVal idFact As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = FSGS(lCiaComp) & "FSPM_GET_DATOS_GENE_ITEMS_FACTURA"
                cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
                cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
                cmd.Parameters("@FACT_ID").Value = idFact
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Se extrae la informaci�n relativa a costes y descuentos de la l�nea par�metro de la factura par�metro.
        ''' </summary>
        ''' <param name="idFact">Identificador de la factura de la cual se extraer�n los datos</param>
        ''' <param name="numLineaFact">N�mero de la l�nea de la factura para la cual se deben extraer datos</param>
        ''' <returns>DataTable con la informaci�n solicitada</returns>
        ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.construirItems
        ''' Tiempo m�ximo: menor a 0,5 seg</remarks>
        Public Function obtDatosCostesDescuentosItem(ByVal lCiaComp As Long, ByVal idFact As String, ByVal numLineaFact As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = FSGS(lCiaComp) & "FSPM_GET_COSTYDESC_ITEMS_FACTURA"
                cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
                cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
                cmd.Parameters("@FACT_ID").Value = idFact
                cmd.Parameters.Add("@FACT_LIN", SqlDbType.Int)
                cmd.Parameters("@FACT_LIN").Direction = ParameterDirection.Input
                cmd.Parameters("@FACT_LIN").Value = numLineaFact
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Se extrae la informaci�n relativa al albar�n al que pertenece el art�culo par�metro de la factura par�metro
        ''' </summary>
        ''' <param name="codArticulo">C�digo del art�culo de cuyo albar�n se deben sacar datos</param>
        ''' <returns>DataTable con la informaci�n solicitada</returns>
        ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.construirItems
        ''' Tiempo m�ximo: menor a 0,5 seg</remarks>
        Public Function obtDatosAlbaranes(ByVal lCiaComp As Long, ByVal idFact As String, ByVal codArticulo As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = FSGS(lCiaComp) & "FSPM_GET_DATOS_ALBARANES_FACTURA"
                cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
                cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
                cmd.Parameters("@FACT_ID").Value = idFact
                cmd.Parameters.Add("@ART", SqlDbType.VarChar, 20)
                cmd.Parameters("@ART").Direction = ParameterDirection.Input
                cmd.Parameters("@ART").Value = codArticulo
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Este m�todo devuelve un datatable que contiene el c�digo  v�lido para facturae del idioma. Se le pasa como par�metro el c�digo
        ''' FS del idioma
        ''' </summary>
        ''' <param name="psIdioma">Idioma de cuyo c�digo se quiere extraer el mapeo para obtener el c�digo v�lido para facturae</param>
        ''' <returns>DataTable con la informaci�n solicitada</returns>
        ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.construirInvoiceIssueData
        ''' Tiempo m�ximo: menor a 0,5 seg</remarks>
        Public Function obtCodigoFacturaeIdioma(ByVal lCiaComp As Long, ByRef psIdioma As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = FSGS(lCiaComp) & "FSPM_GET_COD_IDI_FACTURAE"
                cmd.Parameters.Add("@FS_IDI_COD", SqlDbType.NVarChar, 3)
                cmd.Parameters("@FS_IDI_COD").Direction = ParameterDirection.Input
                cmd.Parameters("@FS_IDI_COD").Value = psIdioma
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Se extrae informaci�n la denominaci�n de la moneda.
        ''' </summary>
        ''' <param name="psIdioma">Idioma en el que se publica la factura en pdf</param>
        ''' <returns>Datatable con la informaci�n solicitada</returns>
        ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
        ''' Tiempo m�ximo: menor a 0,5 seg</remarks>
        Public Function obtNombreMoneda(ByVal lCiaComp As Long, ByRef isoA3CurrencyCode As String, ByVal psIdioma As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = FSGS(lCiaComp) & "FSPM_GET_COD_MON_FACTURA"
                cmd.Parameters.Add("@FACTURAE_MON_A3", SqlDbType.VarChar, 3)
                cmd.Parameters("@FACTURAE_MON_A3").Direction = ParameterDirection.Input
                cmd.Parameters("@FACTURAE_MON_A3").Value = isoA3CurrencyCode
                cmd.Parameters.Add("@FS_IDI_COD", SqlDbType.NVarChar, 3)
                cmd.Parameters("@FS_IDI_COD").Direction = ParameterDirection.Input
                cmd.Parameters("@FS_IDI_COD").Value = psIdioma
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Se extrae informaci�n la denominaci�n del pa�s.
        ''' </summary>
        ''' <param name="isoA3CountryCode">C�digo ISO A3 del pa�s</param>
        ''' <param name="psIdioma">Idioma en el que se publica la factura en pdf</param>
        ''' <returns>Datatable con la informaci�n solicitada</returns>
        ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
        ''' Tiempo m�ximo: menor a 0,5 seg</remarks>
        Public Function obtNombrePais(ByVal lCiaComp As Long, ByRef isoA3CountryCode As String, ByVal psIdioma As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = FSGS(lCiaComp) & "FSPM_GET_COD_PAI_FACTURA"
                cmd.Parameters.Add("@FACTURAE_PAI_A3", SqlDbType.VarChar, 3)
                cmd.Parameters("@FACTURAE_PAI_A3").Direction = ParameterDirection.Input
                cmd.Parameters("@FACTURAE_PAI_A3").Value = isoA3CountryCode
                cmd.Parameters.Add("@FS_IDI_COD", SqlDbType.NVarChar, 3)
                cmd.Parameters("@FS_IDI_COD").Direction = ParameterDirection.Input
                cmd.Parameters("@FS_IDI_COD").Value = psIdioma
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Se extrae informaci�n la denominaci�n de la unidad de medida.
        ''' </summary>
        ''' <param name="isoUniCode">C�digo ISO de la unidad de medida</param>
        ''' <param name="psIdioma">Idioma en el que se publica la factura en pdf</param>
        ''' <returns>Datatable con la informaci�n solicitada</returns>
        ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.obtDatosBD
        ''' Tiempo m�ximo: menor a 0,5 seg</remarks>
        Public Function obtNombreUnidad(ByVal lCiaComp As Long, ByRef isoUniCode As String, ByVal psIdioma As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = FSGS(lCiaComp) & "FSPM_GET_COD_UNI_FACTURA"
                cmd.Parameters.Add("@FACTURAE_UNI", SqlDbType.VarChar, 2)
                cmd.Parameters("@FACTURAE_UNI").Direction = ParameterDirection.Input
                cmd.Parameters("@FACTURAE_UNI").Value = isoUniCode
                cmd.Parameters.Add("@FS_IDI_COD", SqlDbType.NVarChar, 3)
                cmd.Parameters("@FS_IDI_COD").Direction = ParameterDirection.Input
                cmd.Parameters("@FS_IDI_COD").Value = psIdioma
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Se extrae informaci�n relativa a los certificados para firma digital.
        ''' </summary>
        ''' <param name="idFact">Identificador de la factura de la cual se extraer�n los datos</param>
        ''' <returns>Datatable con la informaci�n solicitada</returns>
        ''' <remarks>Llamado desde: FULLSTEP.FSNServer.App_Classes.IM.CFacturae.crearFacV3_2
        ''' Tiempo m�ximo: menor a 0,5 seg</remarks>
        Public Function obtDatosFirma(ByVal lCiaComp As Long, ByRef idFact As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = FSGS(lCiaComp) & "FSPM_GET_DATOS_FIRMA_FACTURA"
                cmd.Parameters.Add("@FACT_ID", SqlDbType.Int)
                cmd.Parameters("@FACT_ID").Direction = ParameterDirection.Input
                cmd.Parameters("@FACT_ID").Value = idFact
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region "Personalizaci�n LCX"
        Public Function getAllFacturasLCX(idi As String, nifProve As String, nifempresa As String, numFactura As String, dFechaDesde As Date, ByVal dFechaHasta As Date,
                                          dFechaAbonoDesde As Date, dFechaAbonoHasta As Date, dImporteDesde As Double, dImporteHasta As Double, sestadosFactura As String,
                                          idClave As String, bClaveCompleta As Boolean, bSuplido As Boolean, bMisFacturas As Boolean) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "FSPM_LCX_GET_FACTURAS"
                cmd.Parameters.Add("@IDI", SqlDbType.NVarChar)
                cmd.Parameters("@IDI").Value = idi
                cmd.Parameters.Add("@FECHADESDE", SqlDbType.Date)
                cmd.Parameters("@FECHADESDE").Value = IIf(dFechaDesde = "#12:00:00 AM#", System.DBNull.Value, dFechaDesde)
                cmd.Parameters.Add("@FECHAHASTA", SqlDbType.Date)
                cmd.Parameters("@FECHAHASTA").Value = IIf(dFechaHasta = "#12:00:00 AM#", System.DBNull.Value, dFechaHasta)
                cmd.Parameters.Add("@FECHAABONODESDE", SqlDbType.Date)
                cmd.Parameters("@FECHAABONODESDE").Value = IIf(dFechaAbonoDesde = "#12:00:00 AM#", System.DBNull.Value, dFechaAbonoDesde)
                cmd.Parameters.Add("@FECHAABONOHASTA", SqlDbType.Date)
                cmd.Parameters("@FECHAABONOHASTA").Value = IIf(dFechaAbonoHasta = "#12:00:00 AM#", System.DBNull.Value, dFechaAbonoHasta)
                cmd.Parameters.Add("@IMPORTEDESDE", SqlDbType.Float)
                cmd.Parameters("@IMPORTEDESDE").Value = IIf(dImporteDesde = 0, System.DBNull.Value, dImporteDesde)
                cmd.Parameters.Add("@IMPORTEHASTA", SqlDbType.Float)
                cmd.Parameters("@IMPORTEHASTA").Value = IIf(dImporteHasta = 0, System.DBNull.Value, dImporteHasta)
                cmd.Parameters.Add("@NUM_FACT", SqlDbType.VarChar)
                cmd.Parameters("@NUM_FACT").Value = numFactura
                cmd.Parameters.Add("@NIF_PROVE", SqlDbType.VarChar)
                cmd.Parameters("@NIF_PROVE").Value = strToDBNull(nifProve)
                cmd.Parameters.Add("@NIF", SqlDbType.VarChar)
                cmd.Parameters("@NIF").Value = strToDBNull(nifempresa)
                cmd.Parameters.Add("@ESTADOS", SqlDbType.VarChar)
                cmd.Parameters("@ESTADOS").Value = sestadosFactura
                cmd.Parameters.Add("@ID_CLAVE", SqlDbType.VarChar)
                cmd.Parameters("@ID_CLAVE").Value = idClave
                cmd.Parameters.Add("@CLAVECOMPLETA", SqlDbType.Int)
                cmd.Parameters("@CLAVECOMPLETA").Value = bClaveCompleta
                cmd.Parameters.Add("@MISFACTURAS", SqlDbType.Int)
                cmd.Parameters("@MISFACTURAS").Value = bMisFacturas
                cmd.Parameters.Add("@SUPLIDO", SqlDbType.Int)
                cmd.Parameters("@SUPLIDO").Value = bSuplido
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Function getEstadosFacturaLCX(idi As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "FSPM_LCX_GET_ESTADOS_FACTURA"
                cmd.Parameters.Add("@IDI", SqlDbType.VarChar)
                cmd.Parameters("@IDI").Value = idi
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region "Personalizaci�n PPL"
        ''' <summary>Devuelve las facturas del proveedor indicado</summary>
        ''' <param name="oFiltro">Objeto con los filtros</param>
        ''' <returns>Datatble con los datos de las facturas</returns>
        ''' <remarks>llamada desde: PMPortalServer.PPL_Facturas</remarks>
        Public Function getFacturasPPL(ByVal oFiltro As Object) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "FSP_PPL_GET_FACTURAS"
                cmd.Parameters.AddWithValue("@IDI", oFiltro.Idioma)
                cmd.Parameters.AddWithValue("@NIF", oFiltro.NIFCia)
                If oFiltro.Estado <> 0 Then cmd.Parameters.AddWithValue("@ESTADO_WEB_PROVEEDOR", oFiltro.Estado)
                If oFiltro.NumFactura <> String.Empty Then cmd.Parameters.AddWithValue("@NUM_FACTURA", oFiltro.NumFactura)
                If oFiltro.Sociedad <> String.Empty Then cmd.Parameters.AddWithValue("@SOCIEDAD", oFiltro.Sociedad)
                If oFiltro.Lote <> String.Empty Then cmd.Parameters.AddWithValue("@LOTE", oFiltro.Lote)
                If Not oFiltro.FecFacturaDesde.Equals(Date.MinValue) Then cmd.Parameters.AddWithValue("@FECHADESDE", oFiltro.FecFacturaDesde)
                If Not oFiltro.FecFacturaHasta.Equals(Date.MinValue) Then cmd.Parameters.AddWithValue("@FECHAHASTA", oFiltro.FecFacturaHasta)
                If Not oFiltro.FecRecepDesde.Equals(Date.MinValue) Then cmd.Parameters.AddWithValue("@FECHARECEPCIONDESDE", oFiltro.FecRecepDesde)
                If Not oFiltro.FecRecepHasta.Equals(Date.MinValue) Then cmd.Parameters.AddWithValue("@FECHARECEPCIONHASTA", oFiltro.FecRecepHasta)
                If Not oFiltro.FecPagoDesde.Equals(Date.MinValue) Then cmd.Parameters.AddWithValue("@FECHAPAGODESDE", oFiltro.FecPagoDesde)
                If Not oFiltro.FecPagoHasta.Equals(Date.MinValue) Then cmd.Parameters.AddWithValue("@FECHAPAGOHASTA", oFiltro.FecPagoHasta)
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>Devuelve los estados de las facturas</summary>
        ''' <param name="sIdioma">Idioma</param>
        ''' <returns>Datatable con los estados de las facturas</returns>
        ''' <remarks>llamada desde: PMPortalServer.PPL_Facturas</remarks>
        Public Function getEstadosFacturasPPL(ByVal sIdioma As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "FSP_PPL_GET_ESTADOS_FACTURA"
                cmd.Parameters.AddWithValue("@IDI", sIdioma)
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
        ''' <summary>Devuelve las sociedades a las que el proveedor ha emitido facturas</summary>
        ''' <param name="sNIFCia">NIF del proveedor</param>
        ''' <returns></returns>
        Public Function getSociedadesPPL(ByVal sNIFCia As String) As DataTable
            Dim cnn As New SqlConnection(mDBConnection)
            Dim dt As DataTable
            Dim cmd As New SqlCommand
            Dim da As SqlDataAdapter
            Try
                cnn.Open()
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "FSP_PPL_GET_SOCIEDADES_FACTURAS"
                cmd.Parameters.AddWithValue("@NIF", sNIFCia)
                da = New SqlDataAdapter(cmd)
                dt = New DataTable
                da.Fill(dt)
                Return dt
            Catch e As Exception
                Throw e
            Finally
                cnn.Dispose()
                cmd.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region "Destinos data access methods"
        ''' <summary>
        ''' Funci�n que devuelve un dataset con los datos del destino aprovisionadores para el usuario actual
        ''' </summary>
        ''' <param name="sIdi">Codigo del idioma</param>
        ''' <param name="Destino">Codigo del destino</param>
        ''' <returns>un dataset con los datos del destino aprovisionadores para el usuario actual</returns>
        ''' <remarks>
        ''' Llamada desde: La funci�n CargarDatosDestino de Destino.vb
        ''' Tiempo: 0 seg</remarks>
        Public Function Destinos_CargarDatosDestino(ByVal sIdi As String, ByVal Destino As String, ByVal lCiaComp As Long) As DataSet
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter

            Using cn
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "SP_DEVOLVER_DESTINOS"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@COD", Destino)
                cm.Parameters.AddWithValue("@IDI", sIdi)
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            End Using
        End Function
#End Region
#Region "FSAL"
        Public Function FSAL_RegistrarAccesosASPX(ByVal tipo As Int16, ByVal sProducto As String, ByVal fechapet As String, ByVal fecha As String, ByVal pagina As String, ByVal ipost As Int16, ByVal iP As String, ByVal sUsuCod As String, ByVal sPaginaOrigen As String, ByVal sNavegador As String, ByVal sIdRegistro As String, ByVal sProveCod As String, ByVal sQueryString As String) As Integer
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim da As New SqlDataAdapter
            Dim rvp As New SqlParameter("@OUTPUT", SqlDbType.Int)     'Se declara y a�ade el parametro de salida, pero no le damos tratamiento de momento.
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSAL_REGISTRAR_ACCESO_ASPX_FSGS" '
                cm.Parameters.AddWithValue("@TIPO", tipo)
                cm.Parameters.AddWithValue("@PAGINA", pagina)
                cm.Parameters.AddWithValue("@FPETICION", fechapet)
                cm.Parameters.AddWithValue("@FECHA", fecha)
                cm.Parameters.AddWithValue("@IDSESION", "")
                If (sUsuCod Is Nothing) OrElse (sUsuCod = "") Then sUsuCod = "AntesLogin"
                cm.Parameters.AddWithValue("@USUCOD", sUsuCod)
                cm.Parameters.AddWithValue("@IPDIR", iP)
                cm.Parameters.AddWithValue("@PRODUCTO", sProducto)
                If sPaginaOrigen Is Nothing Then sPaginaOrigen = ""
                cm.Parameters.AddWithValue("@PAGINAORIGEN", sPaginaOrigen)
                cm.Parameters.AddWithValue("@POSTBACK", ipost)
                cm.Parameters.AddWithValue("@NAVEGADOR", sNavegador)
                cm.Parameters.AddWithValue("@IDREGISTRO", sIdRegistro)
                If sProveCod Is Nothing Then sProveCod = ""
                cm.Parameters.AddWithValue("@CIACOD", sProveCod)
                If sQueryString Is Nothing Then sQueryString = ""
                cm.Parameters.AddWithValue("@QUERYSTRING", sQueryString)
                rvp.Direction = ParameterDirection.Output
                cm.Parameters.Add(rvp)
                cm.CommandType = CommandType.StoredProcedure
                cm.ExecuteNonQuery()
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Function FSAL_ActualizarAccesosASPX(ByVal tipo As Int16, ByVal fechapet As String, ByVal fecha As String, ByVal sIdRegistro As String, ByVal pagina As String, ByVal iP As String) As Integer
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim da As New SqlDataAdapter
            Dim rvp As New SqlParameter("@OUTPUT", SqlDbType.Int)
            Dim iIntentosExec As Integer = 5
            Dim iMilisegundosEspera As Integer = 200
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = "FSAL_REGISTRAR_ACCESO_ASPX_FSGS"
                cm.Parameters.AddWithValue("@TIPO", tipo)
                cm.Parameters.AddWithValue("@PAGINA", pagina)
                cm.Parameters.AddWithValue("@FPETICION", fechapet)
                cm.Parameters.AddWithValue("@FECHA", fecha)
                cm.Parameters.AddWithValue("@IDSESION", "")
                cm.Parameters.AddWithValue("@USUCOD", "USUCOD")
                cm.Parameters.AddWithValue("@IPDIR", iP)
                cm.Parameters.AddWithValue("@PRODUCTO", "PRODUCTO")
                cm.Parameters.AddWithValue("@NAVEGADOR", "NAVEGADOR")
                cm.Parameters.AddWithValue("@POSTBACK", 0)
                cm.Parameters.AddWithValue("@IDREGISTRO", sIdRegistro)
                rvp.Direction = ParameterDirection.Output
                cm.Parameters.Add(rvp)
                cm.CommandType = CommandType.StoredProcedure
                cm.ExecuteNonQuery()
                'Se comprueba que el stored se ha ejecutado hasta el final y si no, se espera un lapso y se vuelve a intentar, un m�ximo de 3 veces.
                'Esto se debe a que puede llegar antes la llamada al webservice para actualizar que para registrar, y en ese caso, 
                'al no existir registro que actualizar, el stored devuelve un valor <> 0 sin llegar a terminar toda su ejecucion.
                If CInt(rvp.Value) <> 0 Then
                    Do Until (CInt(rvp.Value) = 0) Or (iIntentosExec = 0)
                        Threading.Thread.Sleep(iMilisegundosEspera)
                        cm.ExecuteNonQuery()
                        iIntentosExec -= 1
                    Loop
                End If
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
#End Region
#Region "TextosGS"
        Public Function TextoGs_Carga(ByVal lCiaComp As Long, ByVal Modulo As Integer, ByVal ID As Integer, ByVal Idioma As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As String
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim sSql, sRes As String

            Try
                cm.Connection = cn
                cm.CommandType = CommandType.Text
                sSql = "SELECT TEXT_" & Idioma & " FROM TEXTOS_GS WITH(NOLOCK) WHERE MODULO=" & Modulo & " AND ID=" & ID
                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                cm.Parameters.Add("@stmt", SqlDbType.NText, sSql.Length)
                cm.Parameters("@stmt").Value = sSql
                da.SelectCommand = cm
                da.Fill(dr)

                sRes = ""
                If dr.Tables(0).Rows.Count > 0 Then
                    sRes = "" & dr.Tables(0).Rows(0).Item(0)
                End If
                Return sRes
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
#End Region
#Region "Errores"
        Public Function Errores_Create(ByVal lCiaComp As Integer, ByVal sPagina As String, ByVal sUsuario As String, ByVal sEx_FullName As String, ByVal sEx_Message As String, ByVal sEx_StackTrace As String,
                                       ByVal sSv_Query_String As String, ByVal sSv_Http_User_Agent As String,
                                       Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Long
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Try
                cn.Open()
                With cm
                    .Connection = cn
                    .CommandType = CommandType.StoredProcedure
                    .CommandText = FSGS(lCiaComp) & "FSPM_INSERTAR_ERROR"
                    .Parameters.AddWithValue("@PAGINA", "PORTAL: " & IIf(String.IsNullOrEmpty(sPagina), String.Empty, sPagina))
                    .Parameters.AddWithValue("@USUARIO", IIf(String.IsNullOrEmpty(sUsuario), String.Empty, sUsuario))
                    .Parameters.AddWithValue("@FULLNAME", IIf(String.IsNullOrEmpty(sEx_FullName) OrElse sEx_FullName = "", String.Empty, sEx_FullName))
                    .Parameters.AddWithValue("@MESSAGE", IIf(String.IsNullOrEmpty(sEx_Message), String.Empty, sEx_Message))
                    .Parameters.AddWithValue("@STACKTRACE", IIf(String.IsNullOrEmpty(sEx_StackTrace) OrElse sEx_StackTrace = "", String.Empty, sEx_StackTrace))
                    .Parameters.AddWithValue("@QUERYSTRING", IIf(String.IsNullOrEmpty(sSv_Query_String) OrElse sSv_Query_String = "", String.Empty, sSv_Query_String))
                    .Parameters.AddWithValue("@USERAGENT", IIf(String.IsNullOrEmpty(sSv_Http_User_Agent) OrElse sSv_Http_User_Agent = "", String.Empty, sSv_Http_User_Agent))
                    .Parameters.AddWithValue("@ID", SqlDbType.Int).Direction = ParameterDirection.Output
                    .ExecuteNonQuery()
                    Errores_Create = DBNullToSomething(cm.Parameters("@ID").Value)
                End With
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
#End Region
    End Class
End Namespace