Namespace Fullstep.PMPortalDatabaseServer
    Public Class TiposDeDatos
        Public Enum TipoGeneral
            SinTipo = 0
            TipoString = 1
            TipoNumerico = 2
            TipoFecha = 3
            TipoBoolean = 4
            TipoTextoCorto = 5
            TipoTextoMedio = 6
            TipoTextoLargo = 7
            TipoArchivo = 8
            TipoDesglose = 9
        End Enum


        Public Enum TipoDeSolicitud
            SolicitudDeCompras = 1
            Certificado = 2
            NoConformidad = 3
            Otros = 0
        End Enum

        Public Enum TipoCampoGS
            SinTipo = 0
            'Public Enum TipoCampoSC
            DescrBreve = 1
            DescrDetallada = 2
            Importe = 3
            Cantidad = 4
            FecNecesidad = 5
            IniSuministro = 6
            FinSuministro = 7
            ArchivoEspecific = 8
            PrecioUnitario = 9
            'End Enum

            'Public Enum TipoCampoCertificado
            NombreCertif = 20
            FecObtencion = 21
            Certificado = 23
            'End Enum

            'Public Enum TipoCampoNoConformidad
            Titulo = 30
            Motivo = 31
            CausasYConclus = 32
            Peso = 33
            Acciones = 34
            Accion = 35
            Fec_inicio = 36
            Fec_cierre = 37
            Responsable = 38
            Observaciones = 39
            Documentación = 40


            EstadoNoConf = 42
            EstadoInternoNoConf = 43
            PiezasDefectuosas = 44
            ImporteRepercutido = 45
            Subtipo = 46

            'End Enum

            Proveedor = 100
            FormaPago = 101
            Moneda = 102
            Material = 103
            CodArticulo = 104
            Unidad = 105
            Desglose = 106
            Pais = 107
            Provincia = 108
            Dest = 109
            PRES1 = 110
            Pres2 = 111
            Pres3 = 112
            Pres4 = 113

            Persona = 115
            ProveContacto = 116
            Rol = 117
            DenArticulo = 118
            NuevoCodArticulo = 119
            NumSolicitERP = 120
            UnidadOrganizativa = 121
            Departamento = 122
            OrganizacionCompras = 123
            Centro = 124
            Almacen = 125

            RefSolicitud = 128
        End Enum

        Public Structure ParametrosGenerales

            Dim gbSolicitudesCompras As Boolean
            Dim gsAccesoFSPM As TipoAccesoFSPM
            Dim gsAccesoFSQA As TipoAccesoFSQA
            Dim gbAccesoQACertificados As Boolean
            Dim gbAccesoQANoConformidad As Boolean
            Dim gbOblCodPedido As Boolean
            Dim gbOblCodPedDir As Boolean
            Dim nomPedERP() As Dictionary(Of FSNLibrary.Idioma, String)
            Dim nomPedDirERP() As Dictionary(Of FSNLibrary.Idioma, String)

        End Structure


        Public Enum TipoFecha
            FechaAlta = 1
            UnDiaDespues = 2
            DosDiasDespues = 3
            TresDiasDespues = 4
            CuatroDiasDespues = 5
            CincoDiasDespues = 6
            SeisDiasDespues = 7
            UnaSemanaDespues = 8
            DosSemanaDespues = 9
            TresSemanaDespues = 10
            UnMesDespues = 11
            DosMesDespues = 12
            TresMesDespues = 13
            CuatroMesDespues = 14
            CincoMesDespues = 15
            SeisMesDespues = 16
            UnAnyoDespues = 17
        End Enum

    Public Enum TipoPonderacionCert
        SinPonderacion = 0
        EscalaContinua = 1
        EscalaDiscreta = 2
        Formula = 3
        Manual = 4
        SiNo = 5
        PorCaducidad = 6
        Automatico = 7
    End Enum
    Public Enum TipoPonderacionVariables
        PondNCNumero = 0
        PondNCMediaPesos = 1
        PondNCSumaPesos = 2
        PondCertificado = 3
        PondIntEscContinua = 4
        PondIntFormula = 5
        PondManual = 6
        PondNoPonderacion = 99
    End Enum

    Public Enum TipoEstadoSolic
        Guardada = 0
        Enviada = 1
        Pendiente = 2
        Rechazada = 6
        Aprobada = 7
        Anulada = 8

        SCPendiente = 100
        SCAprobada = 101
        SCRechazada = 102
        SCAnulada = 103
        SCCerrada = 104

        CertificadoPub = 200
        NoConformidadEnviada = 300
        NoConformidadGuardada = 301
    End Enum


    Public Enum TipoAccionSolicitud
        Alta = 1
        Modificacion = 2
        Traslado = 3
        Devolucion = 4
        Aprobacion = 5
        Rechazo = 6
        Anulacion = 7
        Reemision = 8
    End Enum

    Public Enum TipoEstadoOrdenEntrega
        PendienteDeAprobacion = 0
        DenegadoParcialAprob = 1
        EmitidoAlProveedor = 2
        AceptadoPorProveedor = 3
        EnCamino = 4
        EnRecepcion = 5
        RecibidoYCerrado = 6
        Anulado = 20
        RechazadoPorProveedor = 21
        DenegadoTotalAprobador = 22
    End Enum

    Public Enum TipoEstadoProceso

        sinitems = 1        'Sin validar y sin items
        ConItemsSinValidar = 2      'Con items y sin validar
        validado = 3        ' Validado y sin provedores asignados
        Conproveasignados = 4  ' Validado y con proveedores asignados pero sin validar la asignacion
        conasignacionvalida = 5 ' Con asignacion de proveedores validada
        conpeticiones = 6
        conofertas = 7
        ConObjetivosSinNotificar = 8
        ConObjetivosSinNotificarYPreadjudicado = 9
        PreadjYConObjNotificados = 10
        ParcialmenteCerrado = 11
        conadjudicaciones = 12
        ConAdjudicacionesNotificadas = 13
        Cerrado = 20

    End Enum

        Public Enum TipoCampoPredefinido
            Normal = 0
            CampoGS = 1  ' Solicitud de compras
            Atributo = 2   ' Atributo
            Calculado = 3
            Certificado = 4
            NoConformidad = 5
            Externo = 6 ' Campo de tabla externa
        End Enum


        Public Enum TipoAccesoFSPM
            AccesoFSPMSolicCompra = 1
            AccesoFSPMCompleto = 2
            SinAcceso = 3
        End Enum

        Public Enum TipoAccesoFSQA
            AccesoFSQABasico = 1
            SinAcceso = 2
            Modulos = 3
        End Enum

        Public Enum TipoAmbitoProceso
            AmbProceso = 1
            AmbGrupo = 2
            AmbItem = 3
        End Enum

        Public Enum TipoEstadoNoConformidad
            Guardada = 0
            Abierta = 1
            CierrePosEnPlazo = 2
            CierrePosFueraPlazo = 3
            CierreNegativo = 4
        End Enum

        Public Enum TipoEstadoAcciones
            SinRevisar = 1
            Aprobada = 2
            Rechazada = 3
            FinalizadaSinRevisar = 4
            FinalizadaRevisada = 5
        End Enum

        Public Enum IdsFicticios
            EstadoActual = 1000
            Comentario = 1020
            EstadoInterno = 1010
            Grupo = 900
        End Enum

        Public Enum CalidadSubtipo
            CalIntegracion = 1
            CalManual = 2
            CalCertificado = 3
            CalNoConformidad = 4
        End Enum

        Public Enum lcx_estado_factura
            Registrada = 1
            Anulada = 4
            Pendiente = 13
            Pagada = 23
            Retroceso = 34
        End Enum

        Public Enum lcx_estado_factura_visor
            pendiente = 1
            anulada = 2
            pagada = 3
        End Enum

        Public Enum TipoVisor
            Solicitudes = 1
            NoConformidades = 2
            Certificados = 3
            Contratos = 4
            Facturas = 5
        End Enum
#Region "CONFIGURACION WORKFLOW"
        Public Enum TipoCampoCondicionEnlace
            CampoDeFormulario = 1
            Peticionario = 2
            DepPetcionario = 3
            UONPeticionario = 4
            NumProcesAbiertos = 5
            ImporteAdjudicado = 6
            ImporteSolicitud = 7
            ImporteAbierto = 8
            ImporteEmitido = 9
            ImporteFactura = 10
            ImporteCostesPlanificados = 11
            ImporteCostesNoPlanificados = 12
            ImporteDctosPlanificados = 13
            ImporteDctosNoPlanificados = 14
            NumDiscrepanciasAbiertas = 15
        End Enum
        Public Enum TipoValorCondicionEnlace
            CampoDeFormulario = 1
            Peticionario = 2
            DepPetcionario = 3
            UONPeticionario = 4
            NumProcesAbiertos = 5
            ImporteAdjudicado = 6
            ImporteSolicitud = 7
            ImporteAbierto = 8
            ImporteEmitido = 9
            ValorEstatico = 10
        End Enum
        Public Enum TipoBloque
            Intermedio = 0
            Peticionario = 1
            Final = 2
        End Enum
        Public Enum TipoPMRol
            RolComprador = 1
            RolProveedor = 2
            RolUsuario = 3
            RolPeticionario = 4
            RolObservador = 5
            RolGestor = 6
            RolReceptor = 7
            RolPeticionarioProve = 8
        End Enum
        Public Enum ComoAsignarPMRol
            PorCampo = 1
            PorPMRol = 2
            PorLista = 3
            PorServicioExt = 4
        End Enum
        Public Enum CuandoAsignarPMRol
            EnEtapa = 1
            Ahora = 2
            ProveedorDelContrato = 3
        End Enum
#End Region
    End Class
End Namespace
