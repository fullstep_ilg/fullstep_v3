﻿Imports System.Data.SqlClient
Imports Fullstep.FSNLibrary

Namespace Fullstep.PMPortalDatabaseServer
    Partial Public Class Root

        Public Function getERPFromOrgCompras(ByVal lCiaComp As Long, ByVal orgCompras As String,
                                             Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As String
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As SqlCommand = New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Dim sSql As String
            Try
                cn.Open()
                cm.Connection = cn
                sSql = "SELECT ERP FROM ERP_ORGCOMPRAS WITH(NOLOCK) WHERE ORGCOMPRAS=" & orgCompras
                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                cm.CommandType = CommandType.Text
                cm.Parameters.Add("@stmt", SqlDbType.NText, sSql.Length)
                cm.Parameters("@stmt").Value = sSql

                da.SelectCommand = cm
                da.Fill(ds)
                If (ds.Tables(0).Rows.Count = 0) Then
                    Return 0
                Else
                    Return ds.Tables(0).Rows(0)("ERP")
                End If
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        Public Function hayIntegracionTablaErp(ByVal lCiaComp As Long, ByVal erp As String, ByVal iTabla As Integer, ByVal sentidoIntegracion1 As Integer, ByVal sentidoIntegracion2 As Integer,
                                               Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Boolean
            Authenticate(SesionId, IPDir, PersistID)
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Dim sSql As String

            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.Text
                sSql = "SELECT ACTIVA " _
                    & " FROM TABLAS_INTEGRACION_ERP TI WITH(NOLOCK)" _
                    & "  WHERE TI.ERP = " & erp & " AND TABLA= " & iTabla & " AND SENTIDO "

                If sentidoIntegracion2 <> SentidoIntegracion.SinSentido Then
                    sSql &= "IN (" & sentidoIntegracion1 & "," & sentidoIntegracion2 & ")"
                Else
                    sSql &= "=" & sentidoIntegracion1
                End If

                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                cm.Parameters.Add("@stmt", SqlDbType.NText, sSql.Length)
                cm.Parameters("@stmt").Value = sSql

                da.SelectCommand = cm
                da.Fill(ds)

                If (ds.Tables(0).Rows.Count = 0) Then
                    Return False
                Else
                    Return ds.Tables(0).Rows(0)("ACTIVA") = 1
                End If
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Function hayIntegracionArticuloErp(ByVal lCiaComp As Long, ByVal codArticulo As String, ByVal erp As String,
                                                  Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Boolean
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As SqlCommand = New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter

            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSINT_COMPROBAR_INTEGRACION_ARTICULO_ERP"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@ARTICULO", codArticulo)
                cm.Parameters.AddWithValue("@ERP", erp)
                da.SelectCommand = cm
                da.Fill(ds)

                If (ds.Tables(0).Rows.Count = 0) Then
                    Return False
                Else
                    Return ds.Tables(0).Rows(0)("ESTADO") = 4
                End If
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        Public Function HayIntegracionSalidaWCF(ByVal lCiaComp As Long, ByVal entidad As Integer, Optional ByVal empresa As Integer = 0, Optional ByVal erp As Integer = 0,
                                                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Boolean
            Return HayIntegracionTablaEmp(lCiaComp, entidad, TipoDestinoIntegracion.WCF, SentidoIntegracion.Salida, SentidoIntegracion.EntradaYSalida, empresa, erp)
        End Function
        Public Function HayIntegracionTablaEmp(ByVal lCiaComp As Long, ByVal iTabla As Integer, ByVal tipoIntegracion As Integer, ByVal sentidoIntegracion1 As Integer,
                                               ByVal sentidoIntegracion2 As Integer, Optional ByVal empresa As Integer = 0, Optional ByVal erp As Integer = 0,
                                               Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As Boolean
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As SqlCommand = New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Dim sSql As String

            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandType = CommandType.Text
                sSql = "SELECT 1 FROM TABLAS_INTEGRACION_ERP T WITH(NOLOCK) "
                sSql &= " INNER JOIN ERP_SOCIEDAD ES WITH(NOLOCK) ON ES.ERP=T.ERP "
                sSql &= " INNER JOIN EMP WITH(NOLOCK) ON EMP.SOCIEDAD=ES.SOCIEDAD "
                sSql &= " WHERE T.ACTIVA=1 "
                sSql &= " AND EMP.ERP = 1 "
                sSql &= " AND T.DESTINO_TIPO = " & tipoIntegracion
                sSql &= " AND T.TABLA = " & iTabla
                If empresa <> 0 Then
                    sSql &= " AND EMP.ID= " & empresa
                End If
                If erp <> 0 Then
                    sSql &= " AND ES.ERP= " & erp
                End If
                sSql &= " AND T.SENTIDO "

                If sentidoIntegracion2 <> SentidoIntegracion.SinSentido Then
                    sSql &= " IN (" & sentidoIntegracion1 & "," & sentidoIntegracion2 & ")"
                Else
                    sSql &= " =" & sentidoIntegracion1
                End If

                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                cm.Parameters.Add("@stmt", SqlDbType.NText, sSql.Length)
                cm.Parameters("@stmt").Value = sSql

                da.SelectCommand = cm
                da.Fill(ds)

                If (ds.Tables(0).Rows.Count = 0) Then
                    Return False
                Else
                    Return ds.Tables(0).Rows(0)(0) = 1
                End If
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        Public Function Solicitud_Devolver_Mapper(ByVal lCiaComp As Long, ByVal OrgCompras As String, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn

                cm.CommandText = FSGS(lCiaComp) & "FSPM_DEVOLVER_MAPPER"
                cm.Parameters.AddWithValue("@ORGCOMPRAS", OrgCompras)
                cm.CommandType = CommandType.StoredProcedure
                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Function INT_ObtenerDatosConexionFSIS(ByVal lCiaComp As Long, ByVal iErp As Integer, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim sSql As String

            Try
                cm.Connection = cn
                cm.CommandType = CommandType.Text
                If iErp <> 0 Then
                    sSql = "SELECT SERVICEBINDINGTYPE, SERVICESECURITYMODE, SERVICECLIENTCREDENTIALTYPE, SERVICEPROXYCREDENTIALTYPE "
                    sSql &= "FROM PARGEN_INTEGRACION WITH(NOLOCK) WHERE ERP=" & iErp
                Else
                    sSql = "SELECT TOP 1 SERVICEBINDINGTYPE, SERVICESECURITYMODE, SERVICECLIENTCREDENTIALTYPE, SERVICEPROXYCREDENTIALTYPE "
                    sSql &= "FROM PARGEN_INTEGRACION WITH(NOLOCK)"
                End If
                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                cm.Parameters.Add("@stmt", SqlDbType.NText, sSql.Length)
                cm.Parameters("@stmt").Value = sSql
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        Public Function INT_ObtenerCredencialesWCF(ByVal lCiaComp As Long, ByVal iEntidadIntegracion As Integer, ByVal iErp As Integer,
                                                   Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim sSql As String

            Try
                cm.Connection = cn
                cm.CommandType = CommandType.Text
                If iErp <> 0 Then
                    sSql = "SELECT WCF_ORIG_USU, WCF_ORIG_PWD, WCF_ORIG_FECPWD FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) "
                    sSql &= "WHERE TABLA=" & iEntidadIntegracion & " And ERP=" & iErp
                Else
                    sSql = "SELECT TOP 1 WCF_ORIG_USU, WCF_ORIG_PWD, WCF_ORIG_FECPWD FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) "
                    sSql &= "WHERE TABLA=" & iEntidadIntegracion
                End If
                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                cm.Parameters.Add("@stmt", SqlDbType.NText, sSql.Length)
                cm.Parameters("@stmt").Value = sSql
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        '''<summary>Función que obtiene la ruta de instalación de FSIS.</summary>
        '''<returns>Dataset con la ruta de instalación de FSIS.</returns>
        '''<remarks>Llamada desde: Integracion.ObtenerRutaInstalacionFSIS</remarks> 
        Public Function INT_ObtenerRutaInstalacionFSIS(ByVal lCiaComp As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim sSql As String

            Try
                cm.Connection = cn
                cm.CommandType = CommandType.Text
                sSql = "SELECT TOP 1 RUTAXBAP FROM PARGEN_RUTAS WITH(NOLOCK) WHERE NOMBREPROY='FullstepIS'"
                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                cm.Parameters.Add("@stmt", SqlDbType.NText, sSql.Length)
                cm.Parameters("@stmt").Value = sSql
                da.SelectCommand = cm
                da.Fill(dr)

                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
    End Class
End Namespace