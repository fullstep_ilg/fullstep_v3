Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Exception
Imports System.IO
Imports System.Object
Imports Fullstep.PMPortalDatabaseServer.TiposDeDatos

Namespace Fullstep.PMPortalDatabaseServer
    Module ComprobarEtapaWorkf
        Private mDBConnection As String
        Private cn As New SqlConnection

        Public Function ObtenerEnlaces(ByVal pDBConnection As String, ByVal lBloqueOrigen As Long, ByVal lAccion As Long, ByVal lCiaComp As Long) As DataSet
            Dim oDS As New DataSet

            If cn.State = ConnectionState.Open Then cn.Close()

            mDBConnection = pDBConnection
            cn.ConnectionString = mDBConnection
            cn.Open()
            'Ahora obtiene los enlaces:
            oDS = Enlaces_Get(lBloqueOrigen, lAccion, lCiaComp)

            Return oDS
        End Function
        Private Function Enlaces_Get(ByVal lBloqueOrigen As Long, ByVal lAccion As Long, ByVal lCiaComp As Long) As DataSet
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim sSQL As String
            Try
                cm.Connection = cn
                sSQL = "SELECT ID,BLOQUE_DESTINO,FORMULA,BLOQUEA FROM PM_COPIA_ENLACE WITH (NOLOCK) WHERE BLOQUE_ORIGEN=" & lBloqueOrigen & " AND ACCION=" & lAccion
                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                cm.CommandType = CommandType.Text
                cm.Parameters.Add("@stmt", SqlDbType.NText, sSQL.Length)
                cm.Parameters("@stmt").Value = sSQL
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            End Try
            Return dr
        End Function
        ''' <summary>
        ''' Comprueba las condiciones para pasar de etapa
        ''' </summary>
        ''' <param name="pDBConnection">la cadena de conexi�n</param>
        ''' <param name="lIdEnlace">El id del enlace actual</param>
        ''' <param name="lInstancia">El id de la instancia</param>
        ''' <param name="lVersion">versi�n de la instancia</param>
        ''' <param name="lCiaComp">C�digo de la compa��a</param>
        ''' <returns>True o False si cumple o no las condiciones</returns>
        ''' <remarks>Llamada desde: DevolverSiguientesEtapas; Tiempo m�ximo: 1 sg.</remarks>
        Private Function ComprobarCondicionesEnlace(ByVal lIdEnlace As Long, ByVal lInstancia As Integer, ByVal lVersion As Integer, ByVal lCiaComp As Long, ByVal esAlta As Boolean, Optional ByVal dsFactura As DataSet = Nothing) As Boolean
            Dim cm As New SqlCommand
            Dim cmValorCampo As SqlCommand
            Dim daValorCampo As SqlDataAdapter
            Dim dsValorCampo As DataSet
            Dim oValorCampo As Object
            Dim oValor As Object
            Dim iTipo As Fullstep.PMPortalDatabaseServer.TiposDeDatos.TipoGeneral
            Dim sConsulta As String
            Dim oDSFormula As New DataSet
            Dim oDSCond As New DataSet
            Dim oCond As DataRow
            Dim i As Long
            Dim sFormula As String
            Dim sVariables() As String
            Dim dValues() As Double
            Dim iEq As New USPExpress.USPExpression
            Dim sSQL As String
            Dim sMoneda As String
            Dim dCambioEnlace As Double
            Dim dCambioInstancia As Double
            Dim dst As New DataSet

            cm = New SqlCommand
            cm.Connection = cn
            sSQL = "SELECT FORMULA FROM PM_COPIA_ENLACE WITH(NOLOCK) WHERE ID=" & lIdEnlace
            cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
            cm.CommandType = CommandType.Text
            cm.Parameters.Add("@stmt", SqlDbType.NText, sSQL.Length)
            cm.Parameters("@stmt").Value = sSQL
            Dim da As New SqlDataAdapter
            da.SelectCommand = cm
            da.Fill(oDSFormula)
            sFormula = oDSFormula.Tables(0).Rows(0).Item("FORMULA")

            'Cambio de moneda 
            cm = New SqlCommand
            cm.Connection = cn
            sSQL = "SELECT CAMBIO FROM INSTANCIA I WITH(NOLOCK) WHERE ID=" & lInstancia
            cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
            cm.CommandType = CommandType.Text
            cm.Parameters.Add("@stmt", SqlDbType.NText, sSQL.Length)
            cm.Parameters("@stmt").Value = sSQL
            da.SelectCommand = cm
            da.Fill(dst)
            dCambioInstancia = DBNullToSomething(dst.Tables(0).Rows(0)("CAMBIO"))
            Try
                cm = New SqlCommand
                cm.Connection = cn
                sSQL = "SELECT PEC.COD,TIPO_CAMPO,CAMPO,OPERADOR,TIPO_VALOR,CAMPO_VALOR,VALOR_TEXT,VALOR_NUM,VALOR_FEC,VALOR_BOOL,MON,EQUIV CAMBIO " _
                                & " FROM PM_COPIA_ENLACE_CONDICION PEC WITH(NOLOCK) " _
                                & " LEFT JOIN MON WITH(NOLOCK) ON MON.COD=PEC.MON" _
                                & " WHERE ENLACE=" & lIdEnlace
                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                cm.CommandType = CommandType.Text
                cm.Parameters.Add("@stmt", SqlDbType.NText, sSQL.Length)
                cm.Parameters("@stmt").Value = sSQL
                da.SelectCommand = cm
                da.Fill(oDSCond)
                i = 0
                For Each oCond In oDSCond.Tables(0).Rows
                    ReDim Preserve sVariables(i)
                    ReDim Preserve dValues(i)
                    sMoneda = DBNullToSomething(oCond.Item("MON"))
                    dCambioEnlace = DBNullToDbl(oCond.Item("CAMBIO"))
                    sVariables(i) = oCond.Item("COD")
                    dValues(i) = 0 'El valor ser� 1 o 0 dependiendo de si se cumple o no la condici�n
                    oValor = Nothing
                    oValorCampo = Nothing

                    '<EXPRESION IZQUIEDA> <OPERADOR> <EXPRESION DERECHA>
                    'Primero evaluamos <EXPRESION IZQUIERDA> 
                    Select Case oCond.Item("TIPO_CAMPO")
                        Case TiposDeDatos.TipoCampoCondicionEnlace.CampoDeFormulario 'Campo de formulario
                            Dim dCampos As DataRow
                            If esAlta Then
                                'S�lo devolver� una fila
                                dCampos = dsFactura.Tables("TEMP").Select("CAMPO=" & oCond.Item("CAMPO"))(0)
                            Else
                                sConsulta = ""
                                sConsulta = sConsulta & "SELECT FC.SUBTIPO,CC.VALOR_NUM,CC.VALOR_FEC,CC.VALOR_BOOL,CC.VALOR_TEXT,FC.TIPO_CAMPO_GS TIPOGS " & vbCrLf
                                sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK) " & vbCrLf
                                sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH(NOLOCK) ON FC.ID=CC.COPIA_CAMPO_DEF " & vbCrLf
                                sConsulta = sConsulta & "WHERE CC.INSTANCIA=@INSTANCIA AND CC.NUM_VERSION=@VERSION AND FC.ID=@CAMPO" & vbCrLf

                                cmValorCampo = New SqlCommand
                                cmValorCampo.Connection = cn
                                cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT, @VERSION INT, @CAMPO INT',@INSTANCIA=@INSTANCIA,@VERSION=@VERSION,@CAMPO=@CAMPO"
                                cmValorCampo.CommandType = CommandType.Text
                                cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                                cmValorCampo.Parameters("@stmt").Value = sConsulta
                                cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)
                                cmValorCampo.Parameters.AddWithValue("@VERSION", lVersion)
                                cmValorCampo.Parameters.AddWithValue("@CAMPO", oCond.Item("CAMPO"))

                                daValorCampo = New SqlDataAdapter
                                daValorCampo.SelectCommand = cmValorCampo
                                dsValorCampo = New DataSet
                                daValorCampo.Fill(dsValorCampo)
                                dCampos = dsValorCampo.Tables(0).Rows(0)
                            End If

                            Select Case dCampos.Item("SUBTIPO")
                                Case 2
                                    oValorCampo = DBNullToSomething(dCampos.Item("VALOR_NUM"))
                                Case 3
                                    oValorCampo = DBNullToSomething(dCampos.Item("VALOR_FEC"))
                                Case 4
                                    oValorCampo = DBNullToSomething(dCampos.Item("VALOR_BOOL"))
                                Case Else
                                    oValorCampo = DBNullToSomething(dCampos.Item("VALOR_TEXT"))

                                    Dim sPresup() As String
                                    Dim bEsPresup As Boolean = False
                                    Dim lIdPres As Integer
                                    Dim sTabla As String
                                    Dim sCampos As String

                                    If DBNullToSomething(dCampos.Item("TIPOGS")) = TiposDeDatos.TipoCampoGS.PRES1 Then
                                        If oValorCampo Is Nothing Then
                                            oValorCampo = ""
                                        Else
                                            bEsPresup = True
                                            sPresup = Split(dCampos.Item("VALOR_TEXT"), "_")
                                            lIdPres = CInt(sPresup(1))
                                            sTabla = FSGS(lCiaComp) & "PRES1_NIV" & CInt(sPresup(0))
                                            sCampos = "CONVERT(VARCHAR(4),ANYO) + ' - ' + COD AS VALOR"
                                        End If
                                    ElseIf DBNullToSomething(dCampos.Item("TIPOGS")) = TiposDeDatos.TipoCampoGS.Pres2 Then
                                        If oValorCampo Is Nothing Then
                                            oValorCampo = ""
                                        Else
                                            bEsPresup = True
                                            sPresup = Split(dCampos.Item("VALOR_TEXT"), "_")
                                            lIdPres = CInt(sPresup(1))
                                            sTabla = FSGS(lCiaComp) & "PRES2_NIV" & CInt(sPresup(0))
                                            sCampos = "CONVERT(VARCHAR(4),ANYO) + ' - ' + COD AS VALOR"
                                        End If
                                    ElseIf DBNullToSomething(dCampos.Item("TIPOGS")) = TiposDeDatos.TipoCampoGS.Pres3 Then
                                        If oValorCampo Is Nothing Then
                                            oValorCampo = ""
                                        Else
                                            bEsPresup = True
                                            sPresup = Split(dCampos.Item("VALOR_TEXT"), "_")
                                            lIdPres = CInt(sPresup(1))
                                            sTabla = FSGS(lCiaComp) & "PRES3_NIV" & CInt(sPresup(0))
                                            sCampos = "COD AS VALOR"
                                        End If
                                    ElseIf DBNullToSomething(dCampos.Item("TIPOGS")) = TiposDeDatos.TipoCampoGS.Pres4 Then
                                        If oValorCampo Is Nothing Then
                                            oValorCampo = ""
                                        Else
                                            bEsPresup = True
                                            sPresup = Split(dCampos.Item("VALOR_TEXT"), "_")
                                            lIdPres = CInt(sPresup(1))
                                            sTabla = FSGS(lCiaComp) & "PRES4_NIV" & CInt(sPresup(0))
                                            sCampos = "COD AS VALOR"
                                        End If
                                    End If

                                    If bEsPresup = True Then
                                        Dim oDSPresup As New DataSet

                                        sConsulta = "SELECT " & sCampos & " FROM " & sTabla & " WITH (NOLOCK) WHERE ID=" & lIdPres
                                        cmValorCampo = New SqlCommand
                                        cmValorCampo.Connection = cn
                                        cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                                        cmValorCampo.CommandType = CommandType.Text
                                        cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                                        cmValorCampo.Parameters("@stmt").Value = sConsulta
                                        da.SelectCommand = cm
                                        da.Fill(oDSPresup)

                                        oValorCampo = oDSPresup.Tables(0).Rows(0).Item("VALOR")
                                    End If
                            End Select
                            iTipo = dCampos.Item("SUBTIPO")
                            If (sMoneda <> "" Or oCond.Item("TIPO_VALOR") = 6 Or oCond.Item("TIPO_VALOR") = 7) Then
                                If dCambioInstancia <> 0 Then
                                    oValorCampo = oValorCampo / dCambioInstancia
                                End If
                            End If
                        Case TipoCampoCondicionEnlace.Peticionario 'Peticionario
                            sConsulta = ""
                            If esAlta Then
                                sConsulta = sConsulta & "SELECT PETICIONARIO_PROVE FROM " & FSGS(lCiaComp) & "INSTANCIA I WITH(NOLOCK) WHERE I.ID = @INSTANCIA "
                            Else
                                sConsulta = sConsulta & "SELECT PETICIONARIO FROM " & FSGS(lCiaComp) & "INSTANCIA I WITH(NOLOCK) WHERE I.ID = @INSTANCIA "
                            End If

                            cmValorCampo = New SqlCommand
                            cmValorCampo.Connection = cn
                            cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                            cmValorCampo.CommandType = CommandType.Text
                            cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                            cmValorCampo.Parameters("@stmt").Value = sConsulta
                            cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                            daValorCampo = New SqlDataAdapter
                            daValorCampo.SelectCommand = cmValorCampo
                            dsValorCampo = New DataSet
                            daValorCampo.Fill(dsValorCampo)

                            If esAlta Then
                                oValorCampo = DBNullToSomething(dsValorCampo.Tables(0).Rows(0).Item("PETICIONARIO_PROVE"))
                            Else
                                oValorCampo = DBNullToSomething(dsValorCampo.Tables(0).Rows(0).Item("PETICIONARIO"))
                            End If

                            iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                        Case TipoCampoCondicionEnlace.DepPetcionario 'Departamento del peticionario
                            If esAlta Then
                                oValorCampo = ""
                            Else
                                sConsulta = ""
                                sConsulta = sConsulta & "SELECT P.DEP FROM " & FSGS(lCiaComp) & "PER P WITH(NOLOCK) INNER JOIN " & FSGS(lCiaComp) & "INSTANCIA I WITH (NOLOCK) ON I.PETICIONARIO = P.COD WHERE I.ID = @INSTANCIA "

                                cmValorCampo = New SqlCommand
                                cmValorCampo.Connection = cn
                                cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                                cmValorCampo.CommandType = CommandType.Text
                                cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                                cmValorCampo.Parameters("@stmt").Value = sConsulta
                                cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                                daValorCampo = New SqlDataAdapter
                                daValorCampo.SelectCommand = cmValorCampo
                                dsValorCampo = New DataSet
                                daValorCampo.Fill(dsValorCampo)

                                oValorCampo = DBNullToSomething(dsValorCampo.Tables(0).Rows(0).Item("DEP"))
                            End If
                            iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                        Case TipoCampoCondicionEnlace.UONPeticionario 'UON del peticionario
                            If esAlta Then
                                oValorCampo = ""
                            Else
                                sConsulta = ""
                                sConsulta = sConsulta & "SELECT P.UON1, P.UON2, P.UON3 FROM " & FSGS(lCiaComp) & "PER P WITH(NOLOCK) INNER JOIN " & FSGS(lCiaComp) & "INSTANCIA I WITH (NOLOCK) ON I.PETICIONARIO = P.COD WHERE I.ID = @INSTANCIA "

                                cmValorCampo = New SqlCommand
                                cmValorCampo.Connection = cn
                                cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                                cmValorCampo.CommandType = CommandType.Text
                                cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                                cmValorCampo.Parameters("@stmt").Value = sConsulta
                                cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                                daValorCampo = New SqlDataAdapter
                                daValorCampo.SelectCommand = cmValorCampo
                                dsValorCampo = New DataSet
                                daValorCampo.Fill(dsValorCampo)

                                With dsValorCampo.Tables(0).Rows(0)
                                    If DBNullToSomething(.Item("UON3")) = Nothing Then
                                        If DBNullToSomething(.Item("UON2")) = Nothing Then
                                            oValorCampo = DBNullToSomething(.Item("UON1"))
                                        Else
                                            oValorCampo = DBNullToSomething(.Item("UON1")) & "-" & DBNullToSomething(.Item("UON2"))
                                        End If
                                    Else
                                        oValorCampo = DBNullToSomething(.Item("UON1")) & "-" & DBNullToSomething(.Item("UON2")) & "-" & DBNullToSomething(.Item("UON3"))
                                    End If
                                End With
                            End If
                            iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio
                        Case TipoCampoCondicionEnlace.NumProcesAbiertos 'N� de procesos de compra abiertos
                            sConsulta = ""
                            sConsulta = sConsulta & "SELECT COUNT(1) AS NUM_PROC FROM (SELECT DISTINCT ANYO,GMN1,COD FROM " & FSGS(lCiaComp) & "PROCE WITH (NOLOCK) WHERE SOLICIT=@INSTANCIA" &
                                " UNION SELECT DISTINCT ANYO,GMN1,PROCE FROM " & FSGS(lCiaComp) & "PROCE_GRUPO WITH (NOLOCK) WHERE SOLICIT=@INSTANCIA " &
                                " UNION SELECT DISTINCT ANYO,GMN1,PROCE FROM " & FSGS(lCiaComp) & "ITEM WITH (NOLOCK) WHERE SOLICIT=@INSTANCIA) AS  NUM_PROC "

                            cmValorCampo = New SqlCommand
                            cmValorCampo.Connection = cn
                            cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                            cmValorCampo.CommandType = CommandType.Text
                            cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                            cmValorCampo.Parameters("@stmt").Value = sConsulta
                            cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                            daValorCampo = New SqlDataAdapter
                            daValorCampo.SelectCommand = cmValorCampo
                            dsValorCampo = New DataSet
                            daValorCampo.Fill(dsValorCampo)

                            oValorCampo = DBNullToSomething(dsValorCampo.Tables(0).Rows(0).Item("NUM_PROC"))
                            iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
                        Case TipoCampoCondicionEnlace.ImporteAdjudicado 'Importe adj
                            sConsulta = ""
                            sConsulta = sConsulta & "SELECT SUM(A.PREC) IMP_ADJ FROM AR_ITEM A WITH(NOLOCK) INNER JOIN ITEM I WITH (NOLOCK) ON A.ANYO=I.ANYO AND A.GMN1=I.GMN1 AND A.PROCE=I.PROCE AND A.ITEM=I.ID WHERE I.SOLICIT = @INSTANCIA "

                            cmValorCampo = New SqlCommand
                            cmValorCampo.Connection = cn
                            cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                            cmValorCampo.CommandType = CommandType.Text
                            cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                            cmValorCampo.Parameters("@stmt").Value = sConsulta
                            cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                            daValorCampo = New SqlDataAdapter
                            daValorCampo.SelectCommand = cmValorCampo
                            dsValorCampo = New DataSet
                            daValorCampo.Fill(dsValorCampo)

                            oValorCampo = DBNullToSomething(dsValorCampo.Tables(0).Rows(0).Item("IMP_ADJ"))
                            iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
                        Case TipoCampoCondicionEnlace.ImporteSolicitud 'Importe de la solicitud de compra
                            sConsulta = ""
                            sConsulta = sConsulta & "SELECT IMPORTE FROM " & FSGS(lCiaComp) & "INSTANCIA I WITH(NOLOCK) WHERE I.ID = @INSTANCIA "

                            cmValorCampo = New SqlCommand
                            cmValorCampo.Connection = cn
                            cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                            cmValorCampo.CommandType = CommandType.Text
                            cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                            cmValorCampo.Parameters("@stmt").Value = sConsulta
                            cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                            daValorCampo = New SqlDataAdapter
                            daValorCampo.SelectCommand = cmValorCampo
                            dsValorCampo = New DataSet
                            daValorCampo.Fill(dsValorCampo)

                            oValorCampo = DBNullToSomething(dsValorCampo.Tables(0).Rows(0).Item("IMPORTE"))
                            If dCambioInstancia <> 0 Then
                                oValorCampo = oValorCampo / dCambioInstancia
                            End If

                            iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
                        Case TipoCampoCondicionEnlace.ImporteFactura 'ImporteFactura
                            If dsFactura Is Nothing Then
                                sConsulta = ""
                                sConsulta = sConsulta & "SELECT SUM(F.IMPORTE) IMPORTE "
                                sConsulta = sConsulta & "FROM FACTURA F WITH(NOLOCK) "
                                sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID = F.INSTANCIA AND I.ID = @INSTANCIA"

                                cmValorCampo = New SqlCommand(sConsulta)
                                cmValorCampo.Connection = cn
                                cmValorCampo.CommandTimeout = 60
                                cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                                cmValorCampo.CommandType = CommandType.Text
                                cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                                cmValorCampo.Parameters("@stmt").Value = sConsulta
                                cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                                oValorCampo = cmValorCampo.ExecuteScalar()
                            Else
                                oValorCampo = dsFactura.Tables("FACTURA").Rows(0).Item("IMPORTE")
                            End If

                            iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
                        Case TipoCampoCondicionEnlace.ImporteCostesPlanificados 'ImporteCostesPlanificados
                            sConsulta = ""
                            sConsulta = sConsulta & "SELECT COALESCE((SELECT SUM(LFCD.IMPORTE) IMPORTECOSTESLINEA_PLANIFICADOS "
                            sConsulta = sConsulta & "FROM LIN_FACTURA_CD LFCD WITH(NOLOCK) "
                            sConsulta = sConsulta & "INNER JOIN FACTURA F WITH(NOLOCK) ON LFCD.FACTURA = F.ID "
                            sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID = F.INSTANCIA AND I.ID = @INSTANCIA "
                            sConsulta = sConsulta & "WHERE LFCD.PLANIFICADO = 1 AND LFCD.TIPO = 0),0)"


                            cmValorCampo = New SqlCommand(sConsulta)
                            cmValorCampo.Connection = cn
                            cmValorCampo.CommandTimeout = 60
                            cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                            cmValorCampo.CommandType = CommandType.Text
                            cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                            cmValorCampo.Parameters("@stmt").Value = sConsulta
                            cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                            oValorCampo = cmValorCampo.ExecuteScalar()
                            iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
                        Case TipoCampoCondicionEnlace.ImporteCostesNoPlanificados 'ImporteCostesNoPlanificados
                            If dsFactura Is Nothing Then
                                sConsulta = ""
                                sConsulta = sConsulta & "DECLARE @IMPORTECOSTESLINEA_NO_PLANIFICADOS FLOAT" & vbCrLf
                                sConsulta = sConsulta & "DECLARE @IMPORTECOSTESCABECERA_NO_PLANIFICADOS FLOAT" & vbCrLf
                                sConsulta = sConsulta & "     SET @IMPORTECOSTESLINEA_NO_PLANIFICADOS = COALESCE((SELECT SUM(LFCD.IMPORTE) IMPORTECOSTESLINEA_NO_PLANIFICADOS " & vbCrLf
                                sConsulta = sConsulta & "FROM LIN_FACTURA_CD LFCD WITH(NOLOCK) " & vbCrLf
                                sConsulta = sConsulta & "INNER JOIN FACTURA F WITH(NOLOCK) ON LFCD.FACTURA = F.ID " & vbCrLf
                                sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID = F.INSTANCIA AND I.ID = @INSTANCIA " & vbCrLf
                                sConsulta = sConsulta & "WHERE LFCD.PLANIFICADO = 0 AND LFCD.TIPO = 0),0)"
                                sConsulta = sConsulta & "     SET @IMPORTECOSTESCABECERA_NO_PLANIFICADOS = COALESCE((SELECT SUM(FCD.IMPORTE) IMPORTECOSTESCABECERA_NO_PLANIFICADOS " & vbCrLf
                                sConsulta = sConsulta & "FROM FACTURA_CD FCD WITH(NOLOCK) " & vbCrLf
                                sConsulta = sConsulta & "INNER JOIN FACTURA F WITH(NOLOCK) ON FCD.FACTURA = F.ID " & vbCrLf
                                sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID = F.INSTANCIA AND I.ID = @INSTANCIA " & vbCrLf
                                sConsulta = sConsulta & "WHERE FCD.PLANIFICADO = 0 AND FCD.TIPO = 0),0)" & vbCrLf
                                sConsulta = sConsulta & "SELECT @IMPORTECOSTESLINEA_NO_PLANIFICADOS + @IMPORTECOSTESCABECERA_NO_PLANIFICADOS AS IMPORTECOSTES_NO_PLANIFICADOS" & vbCrLf

                                cmValorCampo = New SqlCommand(sConsulta)
                                cmValorCampo.Connection = cn
                                cmValorCampo.CommandTimeout = 60
                                cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                                cmValorCampo.CommandType = CommandType.Text
                                cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                                cmValorCampo.Parameters("@stmt").Value = sConsulta
                                cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                                oValorCampo = cmValorCampo.ExecuteScalar()
                            Else
                                Dim oDTCostesCabecera As DataTable = CType(dsFactura.Tables("COSTES"), DataTable)
                                Dim oDTCostesLinea As DataTable = CType(dsFactura.Tables("LINEA_COSTES"), DataTable)
                                Dim dCosteCabeceraNoPlanificado As Double = 0
                                Dim dCosteLineaNoPlanificado As Double = 0
                                For Each drCosteLinea As DataRow In oDTCostesLinea.Select("PLANIFICADO=0")
                                    dCosteLineaNoPlanificado = dCosteLineaNoPlanificado + drCosteLinea("IMPORTE")
                                Next
                                For Each drCosteCabecera As DataRow In oDTCostesCabecera.Select("PLANIFICADO=0")
                                    dCosteCabeceraNoPlanificado = dCosteCabeceraNoPlanificado + drCosteCabecera("IMPORTE")
                                Next
                                oValorCampo = dCosteLineaNoPlanificado + dCosteCabeceraNoPlanificado
                            End If
                            iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
                        Case TipoCampoCondicionEnlace.ImporteDctosPlanificados 'ImporteDctosPlanificados
                            sConsulta = ""
                            sConsulta = sConsulta & "SELECT COALESCE((SELECT SUM(LFCD.IMPORTE) IMPORTEDESCUENTOSLINEA_PLANIFICADOS "
                            sConsulta = sConsulta & "FROM LIN_FACTURA_CD LFCD WITH(NOLOCK) "
                            sConsulta = sConsulta & "INNER JOIN FACTURA F WITH(NOLOCK) ON LFCD.FACTURA = F.ID "
                            sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID = F.INSTANCIA AND I.ID = @INSTANCIA "
                            sConsulta = sConsulta & "WHERE LFCD.PLANIFICADO = 1 AND LFCD.TIPO = 1),0)"

                            cmValorCampo = New SqlCommand(sConsulta)
                            cmValorCampo.Connection = cn
                            cmValorCampo.CommandTimeout = 60
                            cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                            cmValorCampo.CommandType = CommandType.Text
                            cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                            cmValorCampo.Parameters("@stmt").Value = sConsulta
                            cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                            oValorCampo = cmValorCampo.ExecuteScalar()
                            iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
                        Case TipoCampoCondicionEnlace.ImporteDctosNoPlanificados 'ImporteDctosNoPlanificados
                            If dsFactura Is Nothing Then
                                sConsulta = ""
                                sConsulta = sConsulta & "DECLARE @IMPORTEDESCUENTOSLINEA_NO_PLANIFICADOS FLOAT" & vbCrLf
                                sConsulta = sConsulta & "DECLARE @IMPORTEDESCUENTOSCABECERA_NO_PLANIFICADOS FLOAT" & vbCrLf
                                sConsulta = sConsulta & "     SET @IMPORTEDESCUENTOSLINEA_NO_PLANIFICADOS = COALESCE((SELECT SUM(LFCD.IMPORTE) IMPORTEDESCUENTOSLINEA_NO_PLANIFICADOS " & vbCrLf
                                sConsulta = sConsulta & "FROM LIN_FACTURA_CD LFCD WITH(NOLOCK) " & vbCrLf
                                sConsulta = sConsulta & "INNER JOIN FACTURA F WITH(NOLOCK) ON LFCD.FACTURA = F.ID " & vbCrLf
                                sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID = F.INSTANCIA AND I.ID = @INSTANCIA " & vbCrLf
                                sConsulta = sConsulta & "WHERE LFCD.PLANIFICADO = 0 AND LFCD.TIPO = 1),0)" & vbCrLf
                                sConsulta = sConsulta & "     SET @IMPORTEDESCUENTOSCABECERA_NO_PLANIFICADOS = COALESCE((SELECT SUM(FCD.IMPORTE) IMPORTEDESCUENTOSCABECERA_NO_PLANIFICADOS " & vbCrLf
                                sConsulta = sConsulta & "FROM FACTURA_CD FCD WITH(NOLOCK) " & vbCrLf
                                sConsulta = sConsulta & "INNER JOIN FACTURA F WITH(NOLOCK) ON FCD.FACTURA = F.ID " & vbCrLf
                                sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID = F.INSTANCIA AND I.ID = @INSTANCIA " & vbCrLf
                                sConsulta = sConsulta & "WHERE FCD.PLANIFICADO = 0 AND FCD.TIPO = 1),0)" & vbCrLf
                                sConsulta = sConsulta & "SELECT @IMPORTEDESCUENTOSLINEA_NO_PLANIFICADOS + @IMPORTEDESCUENTOSCABECERA_NO_PLANIFICADOS AS IMPORTEDESCUENTOS_NO_PLANIFICADOS" & vbCrLf

                                cmValorCampo = New SqlCommand(sConsulta)
                                cmValorCampo.Connection = cn
                                cmValorCampo.CommandTimeout = 60
                                cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                                cmValorCampo.CommandType = CommandType.Text
                                cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                                cmValorCampo.Parameters("@stmt").Value = sConsulta
                                cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                                oValorCampo = cmValorCampo.ExecuteScalar()
                            Else
                                Dim oDTDescuentosCabecera As DataTable = CType(dsFactura.Tables("DESCUENTOS"), DataTable)
                                Dim oDTDescuentosLinea As DataTable = CType(dsFactura.Tables("LINEA_DESCUENTOS"), DataTable)
                                Dim dDescuentoCabeceraNoPlanificado As Double = 0
                                Dim dDescuentoLineaNoPlanificado As Double = 0
                                For Each drDescuentoLinea As DataRow In oDTDescuentosLinea.Select("PLANIFICADO=0")
                                    dDescuentoLineaNoPlanificado = dDescuentoLineaNoPlanificado + drDescuentoLinea("IMPORTE")
                                Next
                                For Each drDescuentoCabecera As DataRow In oDTDescuentosCabecera.Select("PLANIFICADO=0")
                                    dDescuentoCabeceraNoPlanificado = dDescuentoCabeceraNoPlanificado + drDescuentoCabecera("IMPORTE")
                                Next
                                oValorCampo = dDescuentoLineaNoPlanificado + dDescuentoCabeceraNoPlanificado
                            End If
                            iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
                        Case TipoCampoCondicionEnlace.NumDiscrepanciasAbiertas 'NumDiscrepanciasAbiertas
                            sConsulta = ""
                            sConsulta = sConsulta & "SELECT COALESCE((SELECT COUNT(1) DISCREPANCIAS FROM LIN_FACTURA_DISCRP LFD WITH(NOLOCK) "
                            sConsulta = sConsulta & "INNER JOIN LIN_FACTURA LF WITH(NOLOCK) ON LF.FACTURA = LFD.FACTURA AND LF.LINEA = LFD.LINEA "
                            sConsulta = sConsulta & "INNER JOIN FACTURA F WITH(NOLOCK) ON LF.FACTURA = F.ID "
                            sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID = F.INSTANCIA AND I.ID = @INSTANCIA "
                            sConsulta = sConsulta & "GROUP BY LFD.FACTURA, LFD.EST HAVING LFD.EST = 0),0) --DISCREPANCIAS ABIERTAS"

                            cmValorCampo = New SqlCommand(sConsulta)
                            cmValorCampo.Connection = cn
                            cmValorCampo.CommandTimeout = 60
                            cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                            cmValorCampo.CommandType = CommandType.Text
                            cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                            cmValorCampo.Parameters("@stmt").Value = sConsulta
                            cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                            oValorCampo = cmValorCampo.ExecuteScalar()
                            iTipo = TiposDeDatos.TipoGeneral.TipoNumerico
                    End Select

                    'Luego <EXPRESION DERECHA>
                    Select Case oCond.Item("TIPO_VALOR")
                        Case TipoValorCondicionEnlace.CampoDeFormulario 'Campo de formulario
                            Dim dCampos As DataRow
                            If esAlta Then
                                dCampos = dsFactura.Tables("TEMP").Select("CAMPO = " & oCond.Item("CAMPO_VALOR"))(0)
                            Else
                                sConsulta = ""
                                sConsulta = sConsulta & "SELECT FC.SUBTIPO,CC.VALOR_NUM,CC.VALOR_FEC,CC.VALOR_BOOL,CC.VALOR_TEXT " & vbCrLf
                                sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK) " & vbCrLf
                                sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH(NOLOCK) ON FC.ID=CC.COPIA_CAMPO_DEF " & vbCrLf
                                sConsulta = sConsulta & "WHERE CC.INSTANCIA= @INSTANCIA AND CC.NUM_VERSION= @VERSION AND CD.ID=@CAMPO" & vbCrLf

                                cmValorCampo = New SqlCommand
                                cmValorCampo.Connection = cn
                                cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT,@VERSION INT,@CAMPO INT',@INSTANCIA=@INSTANCIA,@VERSION=@VERSION,@CAMPO=@CAMPO"
                                cmValorCampo.CommandType = CommandType.Text
                                cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                                cmValorCampo.Parameters("@stmt").Value = sConsulta
                                cmValorCampo.Parameters.AddWithValue("@CAMPO", oCond.Item("CAMPO_VALOR"))
                                cmValorCampo.Parameters.AddWithValue("@VERSION", lVersion)
                                cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                                daValorCampo = New SqlDataAdapter
                                daValorCampo.SelectCommand = cmValorCampo
                                dsValorCampo = New DataSet
                                daValorCampo.Fill(dsValorCampo)
                                dCampos = dsValorCampo.Tables(0).Rows(0)
                            End If

                            Select Case dCampos.Item("SUBTIPO")
                                Case 2
                                    oValor = DBNullToSomething(dCampos.Item("VALOR_NUM"))
                                Case 3
                                    oValor = DBNullToSomething(dCampos.Item("VALOR_FEC"))
                                Case 4
                                    oValor = DBNullToSomething(dCampos.Item("VALOR_BOOL"))
                                Case Else
                                    oValor = DBNullToSomething(dCampos.Item("VALOR_TEXT"))
                            End Select
                            If (sMoneda <> "" Or oCond.Item("TIPO_CAMPO") = 6 Or oCond.Item("TIPO_CAMPO") = 7) Then
                                If dCambioInstancia <> 0 Then
                                    oValor = oValor / dCambioInstancia
                                End If
                            End If
                        Case TipoValorCondicionEnlace.Peticionario 'Peticionario
                            If esAlta Then
                                sConsulta = "SELECT PETICIONARIO_PROVE FROM " & FSGS(lCiaComp) & "INSTANCIA I WITH(NOLOCK) WHERE I.ID = @INSTANCIA "
                            Else
                                sConsulta = "SELECT PETICIONARIO FROM " & FSGS(lCiaComp) & "INSTANCIA I WITH(NOLOCK) WHERE I.ID = @INSTANCIA "
                            End If

                            cmValorCampo = New SqlCommand
                            cmValorCampo.Connection = cn
                            cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                            cmValorCampo.CommandType = CommandType.Text
                            cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                            cmValorCampo.Parameters("@stmt").Value = sConsulta
                            cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                            daValorCampo = New SqlDataAdapter
                            daValorCampo.SelectCommand = cmValorCampo
                            dsValorCampo = New DataSet
                            daValorCampo.Fill(dsValorCampo)

                            If esAlta Then
                                oValor = DBNullToSomething(dsValorCampo.Tables(0).Rows(0).Item("PETICIONARIO_PROVE"))
                            Else
                                oValor = DBNullToSomething(dsValorCampo.Tables(0).Rows(0).Item("PETICIONARIO"))
                            End If
                        Case TipoValorCondicionEnlace.DepPetcionario 'Departamento del peticionario
                            If esAlta Then
                                oValor = ""
                            Else
                                sConsulta = "SELECT P.DEP FROM " & FSGS(lCiaComp) & "PER P WITH (NOLOCK) INNER JOIN " & FSGS(lCiaComp) & "INSTANCIA I WITH (NOLOCK) ON I.PETICIONARIO = P.COD WHERE I.ID = @INSTANCIA "

                                cmValorCampo = New SqlCommand
                                cmValorCampo.Connection = cn
                                cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                                cmValorCampo.CommandType = CommandType.Text
                                cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                                cmValorCampo.Parameters("@stmt").Value = sConsulta
                                cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                                daValorCampo = New SqlDataAdapter
                                daValorCampo.SelectCommand = cmValorCampo
                                dsValorCampo = New DataSet
                                daValorCampo.Fill(dsValorCampo)

                                oValor = DBNullToSomething(dsValorCampo.Tables(0).Rows(0).Item("DEP"))
                            End If
                        Case TipoValorCondicionEnlace.UONPeticionario 'UON del peticionario
                            If esAlta Then
                                oValor = ""
                            Else
                                sConsulta = "SELECT P.UON1, P.UON2, P.UON3 FROM " & FSGS(lCiaComp) & "PER P WITH (NOLOCK) INNER JOIN " & FSGS(lCiaComp) & "INSTANCIA I WITH (NOLOCK) ON I.PETICIONARIO = P.COD WHERE I.ID = @INSTANCIA "

                                cmValorCampo = New SqlCommand
                                cmValorCampo.Connection = cn
                                cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                                cmValorCampo.CommandType = CommandType.Text
                                cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                                cmValorCampo.Parameters("@stmt").Value = sConsulta
                                cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                                daValorCampo = New SqlDataAdapter
                                daValorCampo.SelectCommand = cmValorCampo
                                dsValorCampo = New DataSet
                                daValorCampo.Fill(dsValorCampo)

                                With dsValorCampo.Tables(0).Rows(0)
                                    If DBNullToSomething(.Item("UON3")) = Nothing Then
                                        If DBNullToSomething(.Item("UON2")) = Nothing Then
                                            oValor = DBNullToSomething(.Item("UON1"))
                                        Else
                                            oValor = DBNullToSomething(.Item("UON1")) & "-" & DBNullToSomething(.Item("UON2"))
                                        End If
                                    Else
                                        oValor = DBNullToSomething(.Item("UON1")) & "-" & DBNullToSomething(.Item("UON2")) & "-" & DBNullToSomething(.Item("UON3"))
                                    End If
                                End With
                            End If
                        Case TipoValorCondicionEnlace.NumProcesAbiertos 'N� de procesos de compra abiertos
                            sConsulta = "SELECT COUNT(1) AS NUM_PROC FROM (SELECT DISTINCT ANYO,GMN1,COD FROM " & FSGS(lCiaComp) & "PROCE  WITH (NOLOCK) WHERE SOLICIT=@INSTANCIA" &
                                " UNION SELECT DISTINCT ANYO,GMN1,PROCE FROM " & FSGS(lCiaComp) & "PROCE_GRUPO WITH (NOLOCK)  WHERE SOLICIT=@INSTANCIA " &
                                " UNION SELECT DISTINCT ANYO,GMN1,PROCE FROM " & FSGS(lCiaComp) & "ITEM WITH (NOLOCK) WHERE SOLICIT=@INSTANCIA) AS  NUM_PROC "

                            cmValorCampo = New SqlCommand
                            cmValorCampo.Connection = cn
                            cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                            cmValorCampo.CommandType = CommandType.Text
                            cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                            cmValorCampo.Parameters("@stmt").Value = sConsulta
                            cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                            daValorCampo = New SqlDataAdapter
                            daValorCampo.SelectCommand = cmValorCampo
                            dsValorCampo = New DataSet
                            daValorCampo.Fill(dsValorCampo)

                            oValor = DBNullToSomething(dsValorCampo.Tables(0).Rows(0).Item("NUM_PROC"))
                        Case TipoValorCondicionEnlace.ImporteAdjudicado 'Importe adj (LO DEVUELVE EN MONEDA CENTRAL...Y SE COMPARA CON MONEDA DEL FORMULARIO --->PENDIENTE DE CAMBIAR)
                            sConsulta = "SELECT SUM(A.PREC) IMP_ADJ FROM " & FSGS(lCiaComp) & "AR_ITEM A WITH (NOLOCK) INNER JOIN " & FSGS(lCiaComp) & "ITEM I  WITH (NOLOCK) ON A.ANYO=I.ANYO AND A.GMN1_PROCE=I.GMN1_PROCE AND A.PROCE=I.PROCE AND A.ITEM=I.ID WHERE I.SOLICIT = @INSTANCIA "

                            cmValorCampo = New SqlCommand
                            cmValorCampo.Connection = cn
                            cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                            cmValorCampo.CommandType = CommandType.Text
                            cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                            cmValorCampo.Parameters("@stmt").Value = sConsulta
                            cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                            daValorCampo = New SqlDataAdapter
                            daValorCampo.SelectCommand = cmValorCampo
                            dsValorCampo = New DataSet
                            daValorCampo.Fill(dsValorCampo)

                            oValor = DBNullToSomething(dsValorCampo.Tables(0).Rows(0).Item("IMP_ADJ"))
                        Case TipoValorCondicionEnlace.ImporteSolicitud 'Importe de la solicitud de compra
                            sConsulta = "SELECT IMPORTE FROM INSTANCIA I WITH (NOLOCK) WHERE I.ID = @INSTANCIA "

                            cmValorCampo = New SqlCommand
                            cmValorCampo.Connection = cn
                            cmValorCampo.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt, N'@INSTANCIA INT',@INSTANCIA=@INSTANCIA"
                            cmValorCampo.CommandType = CommandType.Text
                            cmValorCampo.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                            cmValorCampo.Parameters("@stmt").Value = sConsulta
                            cmValorCampo.Parameters.AddWithValue("@INSTANCIA", lInstancia)

                            daValorCampo = New SqlDataAdapter
                            daValorCampo.SelectCommand = cmValorCampo
                            dsValorCampo = New DataSet
                            daValorCampo.Fill(dsValorCampo)

                            oValor = DBNullToSomething(dsValorCampo.Tables(0).Rows(0).Item("IMPORTE"))

                            If dCambioInstancia <> 0 Then oValor = oValor / dCambioInstancia
                        Case TipoValorCondicionEnlace.ValorEstatico 'valor est�tico
                            Select Case iTipo
                                Case 2
                                    oValor = DBNullToSomething(oCond.Item("VALOR_NUM"))
                                Case 3
                                    oValor = DBNullToSomething(oCond.Item("VALOR_FEC"))
                                Case 4
                                    oValor = DBNullToSomething(oCond.Item("VALOR_BOOL"))
                                Case Else
                                    If oCond.Item("TIPO_CAMPO") = 4 Then 'UON DEL PETICIONARIO
                                        Dim oUON() As String
                                        Dim iIndice As Integer
                                        oValor = Nothing
                                        oUON = Split(oCond.Item("VALOR_TEXT"), "-")
                                        For iIndice = 0 To UBound(oUON)
                                            If Trim(oUON(iIndice)) <> "" Then
                                                oValor = oValor & Trim(oUON(iIndice)) & "-"
                                            End If
                                        Next iIndice
                                        oValor = Left(oValor, Len(oValor) - 1)
                                    Else
                                        oValor = DBNullToSomething(oCond.Item("VALOR_TEXT"))
                                    End If
                            End Select
                            If sMoneda <> "" And dCambioEnlace <> 0 Then
                                oValor = oValor / dCambioEnlace
                            ElseIf (oCond.Item("TIPO_CAMPO") = 6 Or oCond.Item("TIPO_CAMPO") = 7) And dCambioInstancia <> 0 Then
                                oValor = oValor / dCambioInstancia
                            End If
                    End Select

                    'y por �ltimo con el OPERADOR obtenemos el valor
                    Select Case iTipo
                        Case 2, 3
                            If oValorCampo Is Nothing Then 'Condici�n vac�a
                                If oValor Is Nothing Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Else
                                Select Case oCond.Item("OPERADOR")
                                    Case ">"
                                        If oValorCampo > oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case "<"
                                        If oValorCampo < oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case ">="
                                        If oValorCampo >= oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case "<="
                                        If oValorCampo <= oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case "="
                                        If oValorCampo = oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case "<>"
                                        If oValorCampo <> oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                End Select
                            End If
                        Case 4
                            If oValorCampo Is Nothing Then 'Condici�n vac�a
                                If oValor Is Nothing Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Else
                                If oValorCampo = oValor Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            End If
                        Case Else
                            If oValorCampo Is Nothing Then 'Condici�n vac�a
                                If oValor Is Nothing Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Else
                                Select Case UCase(oCond.Item("OPERADOR"))
                                    Case "="
                                        If UCase(oValorCampo) = UCase(oValor) Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If

                                    Case "LIKE"
                                        If Left(oValor, 1) = "*" Then
                                            If Right(oValor, 1) = "*" Then
                                                oValor = oValor.ToString.Replace("*", "")
                                                If InStr(oValorCampo.ToString, oValor.ToString) >= 0 Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            Else
                                                oValor = oValor.ToString.Replace("*", "")
                                                If oValorCampo.ToString.EndsWith(oValor.ToString) Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            End If
                                        Else
                                            If Right(oValor, 1) = "*" Then
                                                oValor = oValor.ToString.Replace("*", "")
                                                If oValorCampo.ToString.StartsWith(oValor.ToString) Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            Else
                                                If UCase(oValorCampo.ToString) = UCase(oValor.ToString) Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            End If
                                        End If
                                    Case "<>"
                                        If UCase(oValorCampo.ToString) <> UCase(oValor.ToString) Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                End Select
                            End If
                    End Select
                    i += 1
                Next
                oValor = Nothing
                Try
                    iEq.Parse(sFormula, sVariables)
                    oValor = iEq.Evaluate(dValues)

                Catch ex As USPExpress.ParseException
                Catch ex0 As Exception
                End Try
            Catch ex As Exception
                Throw ex
            End Try
            Return (oValor > 0)
        End Function
        Private Function ComprobarCondicionesEnlaceForm(ByVal idEnlace As Long, ByVal idInstancia As Integer, ByVal lCiaComp As Long, ByVal ds As DataSet) As Boolean
            Return ComprobarCondicionesEnlace(idEnlace, idInstancia, 0, lCiaComp, True, ds)
        End Function
        Public Function DevolverSiguientesEtapas(ByVal pDBConnection As String, ByVal lCiaComp As Long, ByVal Instancia As Integer, ByVal Accion As Integer,
                                                 ByVal sProve As String, ByVal sIdi As String, ByVal lBloque As Long, ByVal lRol As Long,
                                                 Optional ByVal dsFactura As DataSet = Nothing) As DataSet
            Dim oDS As New DataSet
            Dim oRow As DataRow
            Dim cm As New SqlCommand
            Dim lNumVersion As Long
            Dim lCaminoOrigen As Long
            Dim dr As New DataSet
            Dim sSQL As String
            Dim sCodProv As String

            If cn.State = ConnectionState.Open Then cn.Close()

            mDBConnection = pDBConnection
            cn.ConnectionString = mDBConnection
            cn.Open()

            'Obtenemos la versi�n
            Try
                cm = New SqlCommand
                cm.Connection = cn
                sSQL = "SELECT MAX(NUM_VERSION) AS VERSION FROM VERSION_INSTANCIA WHERE INSTANCIA=" & Instancia
                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "SP_EXECUTESQL @stmt"
                cm.CommandType = CommandType.Text
                cm.Parameters.Add("@stmt", SqlDbType.NText, sSQL.Length)
                cm.Parameters("@stmt").Value = sSQL
                Dim da As New SqlDataAdapter
                da.SelectCommand = cm
                da.Fill(oDS)
            Catch e As SqlException
                cn.Close()
                Exit Function
            End Try
            If oDS.Tables.Count > 0 Then lNumVersion = oDS.Tables(0).Rows(0).Item("VERSION")

            'Obtener el Codigo del Proveedor en el GS
            sCodProv = ObtenerProv(lCiaComp, sProve)

            'Obtenemos el identificador del camino del bloque origen:
            Try
                cm = New SqlCommand
                cm.Connection = cn
                sSQL = "SELECT ID FROM INSTANCIA_BLOQUE WITH (NOLOCK) WHERE INSTANCIA=" & Instancia & " AND BLOQUE=" & lBloque & " AND ESTADO=1"
                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                cm.CommandType = CommandType.Text
                cm.Parameters.Add("@stmt", SqlDbType.NText, sSQL.Length)
                cm.Parameters("@stmt").Value = sSQL
                Dim da As New SqlDataAdapter
                oDS = New DataSet
                da.SelectCommand = cm
                da.Fill(oDS)
            Catch e As SqlException
                cn.Close()
                Exit Function
            End Try
            If oDS.Tables.Count > 0 Then lCaminoOrigen = oDS.Tables(0).Rows(0).Item("ID")

            'Ahora obtiene los enlaces:
            oDS = Enlaces_Get(lBloque, Accion, lCiaComp)

            'Para cada enlace comprobar� su condici�n:
            Dim bPasarDeEtapa As Boolean
            Dim sConsulta As String
            Dim sIds As String
            Dim sIds1 As String
            If oDS.Tables(0).Rows.Count > 0 Then
                sIds = ""
                For Each oRow In oDS.Tables(0).Rows
                    bPasarDeEtapa = False
                    If IsDBNull(oRow.Item("FORMULA")) Then
                        'Es un cambio de estapa obligatorio.
                        bPasarDeEtapa = True
                    Else  'Tiene que evaluar la f�rmula:
                        If ComprobarCondicionesEnlace(oRow.Item("ID"), Instancia, lNumVersion, lCiaComp, False, dsFactura) Then
                            bPasarDeEtapa = True
                        End If
                    End If

                    If bPasarDeEtapa = True Then
                        sIds = sIds & " B.ID=" & oRow.Item("BLOQUE_DESTINO") & " OR"
                        sIds1 = sIds1 & " B.BLOQUE=" & oRow.Item("BLOQUE_DESTINO") & " OR"
                    End If
                Next

                If Not String.IsNullOrEmpty(sIds) Then
                    sIds = Mid(sIds, 1, Len(sIds) - 3) & ")"
                    sIds1 = Mid(sIds1, 1, Len(sIds1) - 2) & ")"

                    sConsulta = "SELECT BD.BLOQUE,BD.DEN,B.TIPO FROM PM_COPIA_BLOQUE_DEN BD WITH (NOLOCK) INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON BD.BLOQUE=B.ID WHERE BD.IDI='" & sIdi & "' AND (" & sIds & vbCrLf

                    sConsulta = sConsulta & " SELECT DISTINCT B.ROL,R.DEN,IR.PER,IR.PROVE,CASE WHEN IR.PER IS NOT NULL THEN PER.NOM+' '+PER.APE ELSE CASE WHEN PROVE.COD IS NOT NULL THEN PROVE.DEN ELSE NULL END END NOMBRE,"
                    sConsulta = sConsulta & "R.COMO_ASIGNAR,NULL AS CON,R.TIPO,R.GESTOR_FACTURA,CASE WHEN IR.PER IS NULL AND IR.PROVE IS NULL AND NOT R.COMO_ASIGNAR=" & ComoAsignarPMRol.PorLista & " THEN 1 ELSE 0 END ASIGNAR "
                    sConsulta = sConsulta & "FROM PM_COPIA_ROL_BLOQUE B WITH (NOLOCK) "
                    sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R WITH (NOLOCK) ON B.ROL=R.ID "
                    sConsulta = sConsulta & "INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.ROL=R.ID "
                    sConsulta = sConsulta & "LEFT JOIN PER WITH (NOLOCK) ON IR.PER=PER.COD LEFT JOIN PROVE WITH (NOLOCK) ON IR.PROVE=PROVE.COD WHERE INSTANCIA=" & Instancia & " AND (" & sIds1

                    Try
                        cm = New SqlCommand
                        cm.Connection = cn
                        cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                        cm.CommandType = CommandType.Text
                        cm.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                        cm.Parameters("@stmt").Value = sConsulta
                        Dim da As New SqlDataAdapter
                        da.SelectCommand = cm
                        da.Fill(dr)
                    Catch e As SqlException
                        cn.Close()
                        Exit Function
                    End Try
                End If
            End If

            cn.Close()
            Return dr
        End Function
        Public Function DevolverSiguientesEtapasForm(ByVal pDBConnection As String, ByVal lCiaComp As Long, ByVal idInstancia As Integer, ByVal idAccion As Integer, ByVal codProve As String,
                                                 ByVal codIdioma As String, ByVal ds As DataSet, ByVal idBloque As Long, ByVal idRol As Long, ByRef sRolPorWebService As String) As DataSet
            Dim cm As New SqlCommand
            Dim da As New SqlDataAdapter
            Dim dr As New DataSet
            Dim oDS As New DataSet
            Dim oRow As DataRow
            Dim sRolPorWebServiceSelect As String = ""

            If cn.State = ConnectionState.Open Then cn.Close()

            mDBConnection = pDBConnection
            cn.ConnectionString = mDBConnection
            cn.Open()
            sRolPorWebService = -1

            Try
                'Ahora obtiene los enlaces:
                oDS = Enlaces_Get(idBloque, idAccion, lCiaComp)

                'Para cada enlace comprobar� su condici�n:
                Dim bPasarDeEtapa As Boolean
                Dim sConsulta, sConsulta1, sConsulta2 As String
                Dim sIds As String
                If oDS.Tables(0).Rows.Count > 0 Then
                    sIds = ""
                    For Each oRow In oDS.Tables(0).Rows
                        bPasarDeEtapa = False
                        If IsDBNull(oRow.Item("FORMULA")) Then
                            bPasarDeEtapa = True 'Es un cambio de estapa obligatorio
                        Else  'Tiene que evaluar la f�rmula
                            If ComprobarCondicionesEnlaceForm(oRow.Item("ID"), idInstancia, lCiaComp, ds) Then
                                bPasarDeEtapa = True
                            End If
                        End If

                        If bPasarDeEtapa = True Then
                            sIds = sIds & " B.ID=" & oRow.Item("BLOQUE_DESTINO") & " OR"
                        End If
                    Next

                    If Not String.IsNullOrEmpty(sIds) Then
                        sIds = Mid(sIds, 1, Len(sIds) - 3) & ")"

                        sConsulta1 = "SELECT BD.BLOQUE,BD.DEN,B.TIPO "
                        sConsulta1 = sConsulta1 & "FROM PM_COPIA_BLOQUE_DEN BD WITH (NOLOCK) "
                        sConsulta1 = sConsulta1 & "INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON BD.BLOQUE=B.ID "
                        sConsulta1 = sConsulta1 & " WHERE BD.IDI='" & codIdioma & "' AND (" & sIds & vbCrLf

                        sConsulta2 = "SELECT B.ID,B.TIPO,R.ID AS ROL,R.TIPO AS TIPO_ROL,R.CUANDO_ASIGNAR,R.COMO_ASIGNAR,R.CAMPO,R.PER,R.PROVE "
                        sConsulta2 = sConsulta2 & "FROM PM_COPIA_BLOQUE B WITH(NOLOCK) "
                        sConsulta2 = sConsulta2 & "LEFT JOIN PM_COPIA_ROL_BLOQUE RB WITH(NOLOCK) ON RB.BLOQUE=B.ID "
                        sConsulta2 = sConsulta2 & "LEFT JOIN PM_COPIA_ROL R WITH(NOLOCK) ON R.ID=RB.ROL "
                        sConsulta2 = sConsulta2 & "WHERE (" & sIds & " "
                        cm = New SqlCommand
                        cm.Connection = cn
                        cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                        cm.CommandType = CommandType.Text
                        cm.Parameters.Add("@stmt", SqlDbType.NText, sConsulta2.Length)
                        cm.Parameters("@stmt").Value = sConsulta2

                        da.SelectCommand = cm
                        da.Fill(dr)

                        sConsulta2 = ""
                        For Each row As DataRow In dr.Tables(0).Rows
                            If row("TIPO") = TipoBloque.Final Then
                                If Not String.IsNullOrEmpty(sConsulta2) Then _
                                sConsulta2 &= " UNION "

                                sConsulta2 &= "Select DISTINCT B.ROL,R.DEN,R.PER,R.PROVE,"
                                sConsulta2 &= "CASE When PER Is Not NULL Then PER.NOM + ' ' + PER.APE ELSE CASE WHEN PROVE.COD IS NOT NULL THEN PROVE.DEN ELSE NULL END END AS NOMBRE,R.COMO_ASIGNAR,R.CON, "
                                sConsulta2 &= "R.TIPO,CASE WHEN R.PER IS NULL AND R.PROVE IS NULL AND NOT R.COMO_ASIGNAR=" & ComoAsignarPMRol.PorLista & " THEN 1 ELSE 0 END AS ASIGNAR,"
                                sConsulta2 &= "R.GESTOR_FACTURA "
                                sConsulta2 &= "FROM PM_COPIA_ROL_BLOQUE B WITH(NOLOCK) "
                                sConsulta2 &= "INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON B.ROL=R.ID "
                                sConsulta2 &= "LEFT JOIN PER WITH(NOLOCK) ON R.PER=PER.COD LEFT JOIN PROVE WITH (NOLOCK) ON R.PROVE=PROVE.COD "
                                sConsulta2 &= "WHERE B.BLOQUE=" & CStr(row("ID"))
                            ElseIf Not IsDBNull(row("TIPO_ROL")) Then
                                Select Case row("TIPO_ROL")
                                    Case TipoPMRol.RolPeticionario, TipoPMRol.RolPeticionarioProve
                                        If Not String.IsNullOrEmpty(sConsulta2) Then _
                                        sConsulta2 &= " UNION "

                                        sConsulta2 &= "SELECT DISTINCT B.ROL,R.DEN,R.PER,R.PROVE,"
                                        sConsulta2 &= "CASE WHEN PER IS NOT NULL THEN PER.NOM + ' ' + PER.APE ELSE CASE WHEN PROVE.COD IS NOT NULL THEN PROVE.DEN ELSE NULL END END AS NOMBRE,"
                                        sConsulta2 &= "R.COMO_ASIGNAR,R.CON,R.TIPO,CASE WHEN R.PER IS NULL AND R.PROVE IS NULL AND NOT R.COMO_ASIGNAR=" & ComoAsignarPMRol.PorLista & " THEN 1 ELSE 0 END AS ASIGNAR,R.GESTOR_FACTURA "
                                        sConsulta2 &= "FROM PM_COPIA_ROL_BLOQUE B WITH(NOLOCK) "
                                        sConsulta2 &= "INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON R.ID=B.ROL "
                                        sConsulta2 &= "LEFT JOIN PER WITH(NOLOCK) ON PER.COD=R.PER LEFT JOIN PROVE WITH (NOLOCK) ON PROVE.COD=R.PROVE "
                                        sConsulta2 &= "WHERE B.BLOQUE=" & CStr(row("ID")) & "AND R.ID=" & CStr(row("ROL"))
                                    Case TipoPMRol.RolObservador
                                        If row("CUANDO_ASIGNAR") = CuandoAsignarPMRol.EnEtapa Then
                                            Select Case row("COMO_ASIGNAR")
                                                Case ComoAsignarPMRol.PorCampo 'TIRAREMOS DE LA TEMPORAL DE CAMPOS
                                                    Dim dCampos() As DataRow
                                                    Dim i As Integer
                                                    Dim sCod As Object = Nothing
                                                    Dim sDen As String = String.Empty
                                                    Dim sSql As String

                                                    'S�lo devolver� una fila
                                                    dCampos = ds.Tables("TEMP").Select("CAMPO = " & row("CAMPO"))
                                                    If dCampos.Length > 0 Then
                                                        For i = 0 To dCampos.GetUpperBound(0)
                                                            Select Case dCampos(0)("SUBTIPO")
                                                                Case TipoGeneral.TipoNumerico
                                                                    sCod = IIf(IsDBNull(dCampos(0)("VALOR_NUM")), "", dCampos(0)("VALOR_NUM"))
                                                                Case TipoGeneral.TipoFecha
                                                                    sCod = IIf(IsDBNull(dCampos(0)("VALOR_FEC")), "", dCampos(0)("VALOR_FEC"))
                                                                Case TipoGeneral.TipoBoolean
                                                                    sCod = IIf(IsDBNull(dCampos(0)("VALOR_BOOL")), "", dCampos(0)("VALOR_BOOL"))
                                                                Case Else
                                                                    sCod = IIf(IsDBNull(dCampos(0)("VALOR_TEXT")), "", dCampos(0)("VALOR_TEXT"))
                                                            End Select
                                                        Next

                                                        'PERSONA
                                                        Dim dsPersonas As New DataSet
                                                        'Carga los datos de la persona:
                                                        cm = New SqlCommand
                                                        sSql = "SELECT NOM,APE FROM PER WITH(NOLOCK) WHERE COD='" & sCod & "'"

                                                        cm.Connection = cn
                                                        cm.CommandTimeout = 60
                                                        cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                                                        cm.Parameters.Add("@stmt", SqlDbType.NText, sSql.Length)
                                                        cm.Parameters("@stmt").Value = sSql
                                                        cm.CommandType = CommandType.Text
                                                        da.SelectCommand = cm
                                                        da.Fill(dsPersonas)

                                                        If dsPersonas.Tables(0).Rows.Count > 0 Then _
                                                            sDen = dsPersonas.Tables(0).Rows(0)("NOM") & " " & dsPersonas.Tables(0).Rows(0)("APE")

                                                        'Sacamos el participante del dataset
                                                        If Not String.IsNullOrEmpty(sConsulta2) Then _
                                                            sConsulta2 &= " UNION "

                                                        sConsulta2 &= "SELECT DISTINCT R.ID ROL,R.DEN,'" & sCod & "' PER,R.PROVE, '" & sDen & "' NOMBRE,R.COMO_ASIGNAR,R.CON,"
                                                        sConsulta2 &= "R.TIPO, CASE WHEN R.PER IS NULL AND R.PROVE IS NULL AND NOT R.COMO_ASIGNAR=" & ComoAsignarPMRol.PorLista & " THEN 1 ELSE 0 END AS ASIGNAR,"
                                                        sConsulta2 &= "R.GESTOR_FACTURA "
                                                        sConsulta2 &= "FROM PM_COPIA_ROL R WITH(NOLOCK) "
                                                        sConsulta2 &= "INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON B.ID=R.BLOQUE_ASIGNA   "
                                                        sConsulta2 &= "LEFT JOIN PER WITH (NOLOCK) ON PER.COD=R.PER LEFT JOIN PROVE WITH (NOLOCK) ON PROVE.COD=R.PROVE "
                                                        sConsulta2 &= "WHERE B.ID=" & CStr(row("ID")) & " AND R.ID=" & CStr(row("ROL"))
                                                    End If
                                                Case ComoAsignarPMRol.PorPMRol 'TIRAREMOS DE LA TEMPORAL DE PARTICIPANTES
                                                    Dim sCodParticipante As String
                                                    Dim sNomParticipante As String = String.Empty
                                                    Dim dCampos() As DataRow
                                                    Dim sSql As String

                                                    If ds.Tables("TEMP_PARTICIPANTES").Rows.Count > 0 Then 'S�lo devolver� una fila
                                                        dCampos = ds.Tables("TEMP_PARTICIPANTES").Select("ROL=" & row("ROL"))

                                                        If dCampos.Length > 0 Then
                                                            If row("TIPO_ROL") = TipoPMRol.RolProveedor Then
                                                                Dim dsProveedores As New DataSet
                                                                Dim sContactos As String

                                                                'S�lo devolver� una fila
                                                                sCodParticipante = IIf(IsDBNull(dCampos(0)("PROVE")), "", dCampos(0)("PROVE"))
                                                                sContactos = IIf(IsDBNull(dCampos(0)("CON")), "", dCampos(0)("CON"))

                                                                'Carga los datos de la persona:
                                                                cm = New SqlCommand
                                                                sSql = "SELECT DEN FROM PROVE WITH(NOLOCK) WHERE COD='" & sCodParticipante & "'"

                                                                cm.Connection = cn
                                                                cm.CommandTimeout = 60
                                                                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                                                                cm.Parameters.Add("@stmt", SqlDbType.NText, sSql.Length)
                                                                cm.Parameters("@stmt").Value = sSql
                                                                cm.CommandType = CommandType.Text
                                                                da.SelectCommand = cm
                                                                da.Fill(dsProveedores)

                                                                If dsProveedores.Tables(0).Rows.Count > 0 Then _
                                                                    sNomParticipante = dsProveedores.Tables(0).Rows(0)("DEN")

                                                                'Sacamos el participante del dataset
                                                                If Not String.IsNullOrEmpty(sConsulta2) Then _
                                                                    sConsulta2 &= " UNION "

                                                                sConsulta2 &= "SELECT DISTINCT B.ROL,R.DEN,R.PER,'" & sCodParticipante & "' PROVE, '" & sNomParticipante & "' NOMBRE,"
                                                                sConsulta2 &= "R.COMO_ASIGNAR, '" & sContactos & "' CON,"
                                                                sConsulta2 &= "R.TIPO,CASE WHEN R.PER IS NULL AND R.PROVE IS NULL AND NOT R.COMO_ASIGNAR=" & ComoAsignarPMRol.PorLista & " THEN 1 ELSE 0 END AS ASIGNAR,"
                                                                sConsulta2 &= "R.GESTOR_FACTURA "
                                                                sConsulta2 &= "FROM PM_COPIA_ROL_BLOQUE B WITH (NOLOCK) "
                                                                sConsulta2 &= "INNER JOIN PM_COPIA_ROL R WITH (NOLOCK) ON R.ID=B.ROL "
                                                                sConsulta2 &= "LEFT JOIN PER WITH (NOLOCK) ON PER.COD=R.PER LEFT JOIN PROVE WITH (NOLOCK) ON PROVE.COD=R.PROVE "
                                                                sConsulta2 &= "WHERE B.BLOQUE=" & CStr(row("ID")) & " AND R.ID=" & CStr(row("ROL"))
                                                            Else 'PERSONA
                                                                Dim dsPersonas As New DataSet
                                                                'S�lo devolver� una fila
                                                                sCodParticipante = IIf(IsDBNull(dCampos(0)("PER")), "", dCampos(0)("PER"))

                                                                'Carga los datos de la persona:
                                                                cm = New SqlCommand
                                                                sSql = "SELECT NOM,APE FROM PER WITH(NOLOCK) WHERE COD='" & sCodParticipante & "'"

                                                                cm.Connection = cn
                                                                cm.CommandTimeout = 60
                                                                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                                                                cm.Parameters.Add("@stmt", SqlDbType.NText, sSql.Length)
                                                                cm.Parameters("@stmt").Value = sSql
                                                                cm.CommandType = CommandType.Text
                                                                da.SelectCommand = cm
                                                                da.Fill(dsPersonas)

                                                                If dsPersonas.Tables(0).Rows.Count > 0 Then _
                                                                    sNomParticipante = dsPersonas.Tables(0).Rows(0)("NOM") & " " & dsPersonas.Tables(0).Rows(0)("APE")

                                                                'Sacamos el participante del dataset
                                                                If Not String.IsNullOrEmpty(sConsulta2) Then _
                                                                    sConsulta2 &= " UNION "

                                                                sConsulta2 &= "SELECT DISTINCT B.ROL,R.DEN,'" & sCodParticipante & "' PER,R.PROVE, '" & sNomParticipante & "' NOMBRE,"
                                                                sConsulta2 &= "R.COMO_ASIGNAR,R.CON,R.TIPO,"
                                                                sConsulta2 &= "CASE WHEN R.PER IS NULL AND R.PROVE IS NULL AND NOT R.COMO_ASIGNAR=" & ComoAsignarPMRol.PorLista & " THEN 1 ELSE 0 END AS ASIGNAR,"
                                                                sConsulta2 &= "R.GESTOR_FACTURA "
                                                                sConsulta2 &= "FROM PM_COPIA_ROL_BLOQUE B WITH(NOLOCK) "
                                                                sConsulta2 &= "INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON R.ID=B.ROL "
                                                                sConsulta2 &= "LEFT JOIN PER WITH (NOLOCK) ON PER.COD=R.PER LEFT JOIN PROVE WITH (NOLOCK) ON PROVE.COD=R.PROVE "
                                                                sConsulta2 &= "WHERE B.BLOQUE=" & CStr(row("ID")) & " AND R.ID=" & CStr(row("ROL"))
                                                            End If
                                                        End If
                                                    End If
                                                Case ComoAsignarPMRol.PorLista
                                                    If Not String.IsNullOrEmpty(sConsulta2) Then _
                                                        sConsulta2 &= " UNION "

                                                    sConsulta2 &= "SELECT DISTINCT B.ROL,R.DEN,R.PER,R.PROVE,"
                                                    sConsulta2 &= "CASE WHEN PER IS NOT NULL THEN PER.NOM + ' ' + PER.APE ELSE CASE WHEN PROVE.COD IS NOT NULL THEN PROVE.DEN ELSE NULL END END NOMBRE,"
                                                    sConsulta2 &= "R.COMO_ASIGNAR,R.CON,R.TIPO,CASE WHEN R.PER IS NULL AND R.PROVE IS NULL AND NOT R.COMO_ASIGNAR=" & ComoAsignarPMRol.PorLista & " THEN 1 ELSE 0 END ASIGNAR,"
                                                    sConsulta2 &= "R.GESTOR_FACTURA "
                                                    sConsulta2 &= "FROM PM_COPIA_ROL_BLOQUE B WITH(NOLOCK) "
                                                    sConsulta2 &= "INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON R.ID=B.ROL "
                                                    sConsulta2 &= "LEFT JOIN PER WITH(NOLOCK) ON PER.COD=R.PER LEFT JOIN PROVE WITH (NOLOCK) ON PROVE.COD=R.PROVE "
                                                    sConsulta2 &= "WHERE B.BLOQUE=" & CStr(row("ID")) & " AND R.ID=" & CStr(row("ROL"))
                                            End Select
                                        Else '2 AHORA
                                            If Not String.IsNullOrEmpty(sConsulta2) Then _
                                                sConsulta2 &= " UNION "

                                            sConsulta2 &= "SELECT DISTINCT B.ROL,R.DEN,R.PER,R.PROVE,"
                                            sConsulta2 &= "CASE WHEN PER IS NOT NULL THEN PER.NOM + ' ' + PER.APE ELSE CASE WHEN PROVE.COD IS NOT NULL THEN PROVE.DEN ELSE NULL END END NOMBRE,"
                                            sConsulta2 &= "R.COMO_ASIGNAR,R.CON,R.TIPO,CASE WHEN R.PER IS NULL AND R.PROVE IS NULL AND NOT R.COMO_ASIGNAR=" & ComoAsignarPMRol.PorLista & " THEN 1 ELSE 0 END AS ASIGNAR,"
                                            sConsulta2 &= "R.GESTOR_FACTURA "
                                            sConsulta2 &= "FROM PM_COPIA_ROL_BLOQUE B WITH (NOLOCK) "
                                            sConsulta2 &= "INNER JOIN PM_COPIA_ROL R WITH (NOLOCK) ON R.ID=B.ROL "
                                            sConsulta2 &= "LEFT JOIN PER WITH (NOLOCK) ON PER.COD=R.PER LEFT JOIN PROVE WITH (NOLOCK) ON PROVE.COD=R.PROVE "
                                            sConsulta2 &= " WHERE B.BLOQUE=" & CStr(row("ID")) & " AND R.ID=" & CStr(row("ROL"))
                                        End If
                                    Case Else
                                        If row("CUANDO_ASIGNAR") = CuandoAsignarPMRol.EnEtapa Then
                                            Select Case row("COMO_ASIGNAR")
                                                Case ComoAsignarPMRol.PorCampo 'TIRAREMOS DE LA TEMPORAL DE CAMPOS
                                                    Dim dCampos() As DataRow
                                                    Dim i As Integer
                                                    Dim sCod As Object = Nothing
                                                    Dim sDen As String = String.Empty
                                                    Dim sSql As String

                                                    'S�lo devolver� una fila
                                                    dCampos = ds.Tables("TEMP").Select("CAMPO = " & row("CAMPO"))

                                                    If dCampos.Length > 0 Then
                                                        For i = 0 To dCampos.GetUpperBound(0)
                                                            Select Case dCampos(0)("SUBTIPO")
                                                                Case TipoGeneral.TipoNumerico
                                                                    sCod = IIf(IsDBNull(dCampos(0)("VALOR_NUM")), "", dCampos(0)("VALOR_NUM"))
                                                                Case TipoGeneral.TipoFecha
                                                                    sCod = IIf(IsDBNull(dCampos(0)("VALOR_FEC")), "", dCampos(0)("VALOR_FEC"))
                                                                Case TipoGeneral.TipoBoolean
                                                                    sCod = IIf(IsDBNull(dCampos(0)("VALOR_BOOL")), "", dCampos(0)("VALOR_BOOL"))
                                                                Case Else
                                                                    sCod = IIf(IsDBNull(dCampos(0)("VALOR_TEXT")), "", dCampos(0)("VALOR_TEXT"))
                                                            End Select
                                                        Next

                                                        If row("TIPO_ROL") = TipoPMRol.RolProveedor Then
                                                            Dim dsProveedores As New DataSet

                                                            'Carga los datos del proveedor:
                                                            cm = New SqlCommand
                                                            sSql = "SELECT P.DEN FROM PROVE P WITH(NOLOCK) WHERE COD='" & sCod & "'"

                                                            cm.Connection = cn
                                                            cm.CommandTimeout = 60
                                                            cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                                                            cm.Parameters.Add("@stmt", SqlDbType.NText, sSql.Length)
                                                            cm.Parameters("@stmt").Value = sSql
                                                            cm.CommandType = CommandType.Text
                                                            da.SelectCommand = cm
                                                            da.Fill(dsProveedores)

                                                            If dsProveedores.Tables(0).Rows.Count > 0 Then _
                                                                sDen = dsProveedores.Tables(0).Rows(0)("DEN")

                                                            'Sacamos el participante del dataset
                                                            If Not String.IsNullOrEmpty(sConsulta2) Then _
                                                                sConsulta2 &= " UNION "

                                                            sConsulta2 &= "SELECT DISTINCT B.ROL,R.DEN,R.PER,'" & sCod & "' PROVE,'" & sDen & "' NOMBRE,R.COMO_ASIGNAR,"
                                                            sConsulta2 &= IIf(IsDBNull(dCampos(0)("CONTACTOS")), "R.CON", "'" & dCampos(0)("CONTACTOS") & "'") & " CON,"
                                                            sConsulta2 &= "R.TIPO,CASE WHEN R.PER IS NULL AND R.PROVE IS NULL AND NOT R.COMO_ASIGNAR=" & ComoAsignarPMRol.PorLista & " THEN 1 ELSE 0 END ASIGNAR,"
                                                            sConsulta2 &= "R.GESTOR_FACTURA "
                                                            sConsulta2 &= "FROM PM_COPIA_ROL_BLOQUE B WITH(NOLOCK) "
                                                            sConsulta2 &= "INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON R.ID=B.ROL "
                                                            sConsulta2 &= "LEFT JOIN PER WITH (NOLOCK) ON PER.COD=R.PER LEFT JOIN PROVE WITH (NOLOCK) ON PROVE.COD=R.PROVE "
                                                            sConsulta2 &= "WHERE B.BLOQUE=" & CStr(row("ID")) & " AND R.ID=" & CStr(row("ROL"))
                                                        Else 'PERSONA
                                                            Dim dsPersonas As New DataSet
                                                            'Carga los datos de la persona:
                                                            cm = New SqlCommand
                                                            sSql = "SELECT NOM,APE FROM PER WITH(NOLOCK) WHERE COD='" & sCod & "'"

                                                            cm.Connection = cn
                                                            cm.CommandTimeout = 60
                                                            cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                                                            cm.Parameters.Add("@stmt", SqlDbType.NText, sSql.Length)
                                                            cm.Parameters("@stmt").Value = sSql
                                                            cm.CommandType = CommandType.Text
                                                            da.SelectCommand = cm
                                                            da.Fill(dsPersonas)

                                                            If dsPersonas.Tables(0).Rows.Count > 0 Then _
                                                                sDen = dsPersonas.Tables(0).Rows(0)("NOM") & " " & dsPersonas.Tables(0).Rows(0)("APE")

                                                            'Sacamos el participante del dataset
                                                            If Not String.IsNullOrEmpty(sConsulta2) Then _
                                                                sConsulta2 &= " UNION "

                                                            sConsulta2 &= "SELECT DISTINCT B.ROL,R.DEN,'" & sCod & "' PER,R.PROVE,'" & sDen & "' NOMBRE,R.COMO_ASIGNAR,R.CON,"
                                                            sConsulta2 &= "R.TIPO, CASE WHEN R.PER IS NULL AND R.PROVE IS NULL AND NOT R.COMO_ASIGNAR=" & ComoAsignarPMRol.PorLista & " THEN 1 ELSE 0 END ASIGNAR,"
                                                            sConsulta2 &= "R.GESTOR_FACTURA "
                                                            sConsulta2 &= "FROM PM_COPIA_ROL_BLOQUE B WITH (NOLOCK) "
                                                            sConsulta2 &= "INNER JOIN PM_COPIA_ROL R WITH (NOLOCK) ON R.ID=B.ROL "
                                                            sConsulta2 &= "LEFT JOIN PER WITH (NOLOCK) ON PER.COD=R.PER LEFT JOIN PROVE WITH (NOLOCK) ON PROVE.COD=R.PROVE "
                                                            sConsulta2 &= "WHERE B.BLOQUE=" & CStr(row("ID")) & " AND R.ID=" & CStr(row("ROL"))
                                                        End If
                                                    End If
                                                Case ComoAsignarPMRol.PorPMRol 'TIRAREMOS DE LA TEMPORAL DE PARTICIPANTES
                                                    Dim sCodParticipante As String
                                                    Dim sNomParticipante As String = String.Empty
                                                    Dim dCampos() As DataRow
                                                    Dim sSql As String

                                                    If ds.Tables("TEMP_PARTICIPANTES").Rows.Count > 0 Then 'S�lo devolver� una fila
                                                        dCampos = ds.Tables("TEMP_PARTICIPANTES").Select("ROL = " & row("ROL"))

                                                        If dCampos.Length > 0 Then
                                                            If row("TIPO_ROL") = TipoPMRol.RolProveedor Then
                                                                Dim dsProveedores As New DataSet
                                                                Dim sContactos As String

                                                                'S�lo devolver� una fila
                                                                sCodParticipante = IIf(IsDBNull(dCampos(0)("PROVE")), "", dCampos(0)("PROVE"))
                                                                sContactos = IIf(IsDBNull(dCampos(0)("CON")), "", dCampos(0)("CON"))

                                                                'Carga los datos de la persona:
                                                                cm = New SqlCommand
                                                                sSql = "SELECT DEN FROM PROVE WITH(NOLOCK) WHERE COD='" & sCodParticipante & "'"

                                                                cm.Connection = cn
                                                                cm.CommandTimeout = 60
                                                                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                                                                cm.Parameters.Add("@stmt", SqlDbType.NText, sSql.Length)
                                                                cm.Parameters("@stmt").Value = sSql
                                                                cm.CommandType = CommandType.Text
                                                                da.SelectCommand = cm
                                                                da.Fill(dsProveedores)

                                                                If dsProveedores.Tables(0).Rows.Count > 0 Then _
                                                                    sNomParticipante = dsProveedores.Tables(0).Rows(0)("DEN")


                                                                'Sacamos el participante del dataset
                                                                If Not String.IsNullOrEmpty(sConsulta2) Then _
                                                                    sConsulta2 &= " UNION "

                                                                sConsulta2 &= "SELECT DISTINCT B.ROL,R.DEN,R.PER,'" & sCodParticipante & "' PROVE, '" & sNomParticipante & "' NOMBRE,R.COMO_ASIGNAR,"
                                                                sConsulta2 &= "'" & sContactos & "' CON,"
                                                                sConsulta2 &= "R.TIPO, CASE WHEN R.PER IS NULL AND R.PROVE IS NULL AND NOT R.COMO_ASIGNAR=" & ComoAsignarPMRol.PorLista & " THEN 1 ELSE 0 END ASIGNAR,"
                                                                sConsulta2 &= "R.GESTOR_FACTURA "
                                                                sConsulta2 &= "FROM PM_COPIA_ROL_BLOQUE B WITH(NOLOCK) "
                                                                sConsulta2 &= "INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON R.ID=B.ROL "
                                                                sConsulta2 &= "LEFT JOIN PER WITH(NOLOCK) ON PER.COD=R.PER LEFT JOIN PROVE WITH (NOLOCK) ON PROVE.COD=R.PROVE "
                                                                sConsulta2 &= "WHERE B.BLOQUE=" & CStr(row("ID")) & " AND R.ID=" & CStr(row("ROL"))
                                                            Else 'PERSONA
                                                                Dim dsPersonas As New DataSet
                                                                'S�lo devolver� una fila
                                                                sCodParticipante = IIf(IsDBNull(dCampos(0)("PER")), "", dCampos(0)("PER"))

                                                                'Carga los datos de la persona:
                                                                cm = New SqlCommand
                                                                sSql = "SELECT NOM,APE FROM PER WITH(NOLOCK) WHERE COD='" & sCodParticipante & "'"

                                                                cm.Connection = cn
                                                                cm.CommandTimeout = 60
                                                                cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                                                                cm.Parameters.Add("@stmt", SqlDbType.NText, sSql.Length)
                                                                cm.Parameters("@stmt").Value = sSql
                                                                cm.CommandType = CommandType.Text
                                                                da.SelectCommand = cm
                                                                da.Fill(dsPersonas)

                                                                If dsPersonas.Tables(0).Rows.Count > 0 Then _
                                                                    sNomParticipante = dsPersonas.Tables(0).Rows(0)("NOM") & " " & dsPersonas.Tables(0).Rows(0)("APE")

                                                                'Sacamos el participante del dataset
                                                                If Not String.IsNullOrEmpty(sConsulta2) Then _
                                                                    sConsulta2 &= " UNION "

                                                                sConsulta2 &= "SELECT DISTINCT B.ROL,R.DEN,'" & sCodParticipante & "' PER,R.PROVE, '" & sNomParticipante & "' NOMBRE,R.COMO_ASIGNAR,R.CON,"
                                                                sConsulta2 &= "R.TIPO,CASE WHEN R.PER IS NULL AND R.PROVE IS NULL AND NOT R.COMO_ASIGNAR=" & ComoAsignarPMRol.PorLista & " THEN 1 ELSE 0 END ASIGNAR,"
                                                                sConsulta2 &= "R.GESTOR_FACTURA "
                                                                sConsulta2 &= "FROM PM_COPIA_ROL_BLOQUE B WITH(NOLOCK) "
                                                                sConsulta2 &= "INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON R.ID=B.ROL "
                                                                sConsulta2 &= "LEFT JOIN PER WITH(NOLOCK) ON PER.COD=R.PER LEFT JOIN PROVE WITH (NOLOCK) ON PROVE.COD=R.PROVE "
                                                                sConsulta2 &= "WHERE B.BLOQUE=" & CStr(row("ID")) & " AND R.ID = " & CStr(row("ROL"))
                                                            End If
                                                        End If
                                                    End If
                                                Case ComoAsignarPMRol.PorLista
                                                    If Not String.IsNullOrEmpty(sConsulta2) Then _
                                                        sConsulta2 &= " UNION "

                                                    sConsulta2 &= "SELECT DISTINCT B.ROL,R.DEN,R.PER,R.PROVE,"
                                                    sConsulta2 &= "CASE WHEN PER IS NOT NULL THEN PER.NOM + ' ' + PER.APE ELSE CASE WHEN PROVE.COD IS NOT NULL THEN PROVE.DEN ELSE NULL END END NOMBRE,"
                                                    sConsulta2 &= "R.COMO_ASIGNAR,R.CON,R.TIPO, CASE WHEN R.PER IS NULL AND R.PROVE IS NULL AND NOT R.COMO_ASIGNAR=" & ComoAsignarPMRol.PorLista & " THEN 1 ELSE 0 END ASIGNAR,R.GESTOR_FACTURA "
                                                    sConsulta2 &= "FROM PM_COPIA_ROL_BLOQUE B WITH(NOLOCK) "
                                                    sConsulta2 &= "INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON R.ID=B.ROL "
                                                    sConsulta2 &= "LEFT JOIN PER WITH(NOLOCK) ON PER.COD=R.PER LEFT JOIN PROVE WITH (NOLOCK) ON PROVE.COD=R.PROVE "
                                                    sConsulta2 &= "WHERE B.BLOQUE=" & CStr(row("ID")) & " AND R.ID=" & CStr(row("ROL"))
                                                Case ComoAsignarPMRol.PorServicioExt
                                                    If sRolPorWebService = "-1" Then sRolPorWebService = ""
                                                    sRolPorWebService = sRolPorWebService & CStr(row("ID")) & "@"
                                                    sRolPorWebService = sRolPorWebService & CStr(row("ROL")) & ","

                                                    sRolPorWebServiceSelect = sRolPorWebServiceSelect & CStr(row("ROL")) & ","
                                            End Select
                                        ElseIf row("CUANDO_ASIGNAR") = CuandoAsignarPMRol.ProveedorDelContrato Then
                                            Dim sCod As String
                                            Dim sDen As String = String.Empty
                                            Dim sSql As String

                                            sCod = ds.Tables("CONTRATO").Rows(0).Item("PROVE")

                                            'PROVEEDOR
                                            Dim dsProveedores As New DataSet
                                            'Carga los datos del proveedor:
                                            cm = New SqlCommand
                                            sSql = "SELECT P.DEN FROM PROVE P WITH(NOLOCK) WHERE COD='" & sCod & "'"

                                            cm.Connection = cn
                                            cm.CommandTimeout = 60
                                            cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                                            cm.Parameters.Add("@stmt", SqlDbType.NText, sSql.Length)
                                            cm.Parameters("@stmt").Value = sSql
                                            cm.CommandType = CommandType.Text
                                            da.SelectCommand = cm
                                            da.Fill(dsProveedores)

                                            If dsProveedores.Tables(0).Rows.Count > 0 Then _
                                                sDen = dsProveedores.Tables(0).Rows(0)("DEN")

                                            'Sacamos el participante del dataset
                                            If Not String.IsNullOrEmpty(sConsulta2) Then _
                                                sConsulta2 &= " UNION "

                                            sConsulta2 &= "SELECT DISTINCT B.ROL,R.DEN,R.PER,'" & sCod & "' PROVE, '" & sDen & "' NOMBRE,R.COMO_ASIGNAR,R.CON CON,"
                                            sConsulta2 &= "R.TIPO,CASE WHEN R.PER IS NULL AND R.PROVE IS NULL AND NOT R.COMO_ASIGNAR=" & ComoAsignarPMRol.PorLista & " THEN 1 ELSE 0 END AS ASIGNAR,"
                                            sConsulta2 &= "R.GESTOR_FACTURA "
                                            sConsulta2 &= "FROM PM_COPIA_ROL_BLOQUE B WITH(NOLOCK) "
                                            sConsulta2 &= "INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON R.ID=B.ROL "
                                            sConsulta2 &= "LEFT JOIN PER WITH(NOLOCK) ON PER.COD=R.PER LEFT JOIN PROVE WITH (NOLOCK) ON PROVE.COD=R.PROVE "
                                            sConsulta2 &= "WHERE B.BLOQUE=" & CStr(row("ID")) & " AND R.ID=" & CStr(row("ROL"))
                                        Else '2 AHORA
                                            If Not String.IsNullOrEmpty(sConsulta2) Then _
                                                sConsulta2 &= " UNION "

                                            sConsulta2 &= "SELECT DISTINCT B.ROL,R.DEN,R.PER,R.PROVE,"
                                            sConsulta2 &= "CASE WHEN PER IS NOT NULL THEN PER.NOM + ' ' + PER.APE ELSE CASE WHEN PROVE.COD IS NOT NULL THEN PROVE.DEN ELSE NULL END END NOMBRE,"
                                            sConsulta2 &= "R.COMO_ASIGNAR,R.CON,R.TIPO,CASE WHEN R.PER IS NULL AND R.PROVE IS NULL AND NOT R.COMO_ASIGNAR=" & ComoAsignarPMRol.PorLista & " THEN 1 ELSE 0 END ASIGNAR,"
                                            sConsulta2 &= "R.GESTOR_FACTURA "
                                            sConsulta2 &= "FROM PM_COPIA_ROL_BLOQUE B WITH(NOLOCK) "
                                            sConsulta2 &= "INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON R.ID=B.ROL "
                                            sConsulta2 &= "LEFT JOIN PER WITH(NOLOCK) ON PER.COD=R.PER LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=R.PROVE "
                                            sConsulta2 &= "WHERE B.BLOQUE=" & CStr(row("ID")) & " AND R.ID=" & CStr(row("ROL"))
                                        End If
                                End Select
                            End If
                        Next
                        If Not (sRolPorWebService = "-1") Then
                            sRolPorWebService = Left(sRolPorWebService, Len(sRolPorWebService) - 1)
                            sRolPorWebServiceSelect = Left(sRolPorWebServiceSelect, Len(sRolPorWebServiceSelect) - 1)

                            If Not String.IsNullOrEmpty(sConsulta2) Then _
                            sConsulta2 = sConsulta2 & " UNION "

                            sConsulta2 &= "SELECT R.ID ROL,R.DEN DEN,NULL PER,NULL PROVE,NULL NOMBRE,4 COMO_ASIGNAR,NULL CON,3 TIPO,0 ASIGNAR,R.GESTOR_FACTURA "
                            sConsulta2 &= "FROM PM_COPIA_ROL R WITH(NOLOCK) WHERE R.ID IN (" & sRolPorWebServiceSelect & ")"
                        End If
                        dr = New DataSet
                        sConsulta = sConsulta1 & sConsulta2
                        Try
                            cm = New SqlCommand
                            cm.Connection = cn
                            cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                            cm.CommandType = CommandType.Text
                            cm.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                            cm.Parameters("@stmt").Value = sConsulta
                            da.SelectCommand = cm
                            da.Fill(dr)
                        Catch e As SqlException
                            cn.Close()
                            Exit Function
                        End Try
                    Else
                        'Hay cambios de etapas, pero las condiciones no se cumplen.Se quedar� colgada:
                        sConsulta = "SELECT NULL AS INVALIDA"
                        cm.Connection = cn
                        cm.CommandText = "EXEC " & FSGS(lCiaComp) & "sp_executesql @stmt"
                        cm.CommandType = CommandType.Text
                        cm.Parameters.Add("@stmt", SqlDbType.NText, sConsulta.Length)
                        cm.Parameters("@stmt").Value = sConsulta
                        da.SelectCommand = cm
                        da.Fill(dr)
                    End If
                End If
            Catch e As SqlException
                cn.Close()
                Exit Function
            End Try

            cn.Close()
            Return dr
        End Function
        Private Function FSGS(ByVal lCiaComp As Long) As String
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim sConsulta As String
            Dim sSRV As Object
            Dim sBD As String

            sConsulta = "SELECT FSGS_SRV,FSGS_BD  FROM CIAS WITH(NOLOCK) WHERE ID=" & lCiaComp

            Try
                cm.Connection = cn
                cm.CommandText = sConsulta
                cm.CommandType = CommandType.Text
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            End Try

            cn.Close()

            If dr.Tables(0).Rows.Count = 0 Then
                Return ""
            Else
                sSRV = dr.Tables(0).Rows(0).Item("FSGS_SRV")
                sBD = dr.Tables(0).Rows(0).Item("FSGS_BD")
                If IsDBNull(sSRV) Then
                    Return sBD & ".dbo."
                Else
                    Return sSRV & "." & sBD & ".dbo."
                End If
            End If
        End Function
        Function ObtenerProv(ByVal lCiaComp As Long, ByVal sProve As String) As String
            'Funcion que nos devuelve el codigo del Proveedor en el GS
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Dim sConsulta As String

            sConsulta = "SELECT COD_PROVE_CIA FROM REL_CIAS RC WITH(NOLOCK)"
            sConsulta = sConsulta + " INNER JOIN CIAS C WITH(NOLOCK) ON RC.CIA_PROVE=C.ID"
            sConsulta = sConsulta + " WHERE RC.CIA_COMP=" & lCiaComp & " AND C.COD='" & sProve & "'"

            Try
                cm.Connection = cn
                cm.CommandText = sConsulta
                cm.CommandType = CommandType.Text
                da.SelectCommand = cm
                da.Fill(dr)
            Catch e As Exception
                Throw e
            End Try

            cn.Close()

            If dr.Tables(0).Rows.Count = 0 Then
                Return ""
            Else
                Return dr.Tables(0).Rows(0).Item(0)
            End If
        End Function
    End Module
End Namespace