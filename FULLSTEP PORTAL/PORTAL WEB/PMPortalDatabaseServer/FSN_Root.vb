﻿Imports System.Data.SqlClient
Namespace Fullstep.PMPortalDatabaseServer
    Partial Public Class Root
        Inherits MarshalByRefObject

#Region "Escenarios"
        Public Function FSN_Get_Escenarios_Usuario(ByVal lCiaComp As Long, ByVal Idioma As String, ByVal TipoVisor As Integer, ByRef IdEscenario As Integer,
                                                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As SqlCommand = New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSN_ESCENARIOS_GET_ESCENARIOS_PROVEEDOR"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDI", Idioma)
                cm.Parameters.AddWithValue("@TIPOVISOR", TipoVisor)
                cm.Parameters.AddWithValue("@IDESCENARIO", SqlDbType.Int).Direction = ParameterDirection.InputOutput
                cm.Parameters("@IDESCENARIO").Value = IdEscenario
                da.SelectCommand = cm
                da.Fill(ds)

                IdEscenario = cm.Parameters("@IDESCENARIO").Value
                ds.Tables(0).TableName = "ESCENARIOS_USUARIO"
                ds.Tables(1).TableName = "ESCENARIO_SOLICITUD_FORMULARIOS_USUARIO"
                ds.Tables(2).TableName = "ESCENARIO_FILTROS_USUARIO"
                ds.Tables(3).TableName = "ESCENARIO_FILTRO_CAMPOS_USUARIO"
                ds.Tables(4).TableName = "ESCENARIO_FILTRO_CONDICIONES_USUARIO"
                ds.Tables(5).TableName = "ESCENARIO_VISTAS_USUARIO"
                ds.Tables(6).TableName = "ESCENARIO_VISTA_CAMPOS_USUARIO"

                Return ds
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function

        Public Function FSN_ObtenerSolicitudes(ByVal lCiaComp As Long, ByVal Prove As String, ByVal IdCon As Integer, ByVal Idioma As String, ByVal IdFormulario As Integer,
                ByVal SentenciaWhere As String, ByVal SentenciaOrder As String, ByVal SentenciaWhere_CamposGenerales As String,
                ByVal Pagina As Integer, ByVal NumeroRegistros As Integer, ByVal BuscarProveArt As Integer,
                ByVal AbtasUsted As Boolean, ByVal Participo As Boolean, ByVal Pendientes As Boolean,
                ByVal PendientesDevolucion As Boolean, ByVal Trasladadas As Boolean, ByVal Otras As Boolean,
                ByVal FiltroDesglose As Boolean, ByVal VistaDesglose As Boolean, ByVal TipoSolicitud As Nullable(Of Integer),
                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As New SqlCommand
            Dim dr As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSPM_GET_INSTANCIAS_PORTAL"
                cm.CommandType = CommandType.StoredProcedure
                If Pagina = -1 Then
                    cm.CommandTimeout = 0
                Else
                    cm.CommandTimeout = 120
                End If

                cm.Parameters.AddWithValue("@PROVE", Prove)
                cm.Parameters.AddWithValue("@CON", IdCon)
                cm.Parameters.AddWithValue("@IDI", Idioma)
                cm.Parameters.AddWithValue("@TIPOSOLICITUD", TipoSolicitud)
                If Not IdFormulario = 0 Then cm.Parameters.AddWithValue("@IDFORMULARIO", IdFormulario)
                If Not SentenciaWhere = "" Then cm.Parameters.AddWithValue("@WHERE_FILTRO", SentenciaWhere)
                If Not SentenciaOrder = "" Then cm.Parameters.AddWithValue("@ORDER_FILTRO", SentenciaOrder)
                If Not SentenciaWhere_CamposGenerales = "" Then cm.Parameters.AddWithValue("@WHERE_FILTRO_CAMPOS_GENERALES", SentenciaWhere_CamposGenerales)

                cm.Parameters.AddWithValue("@PAGENUMBER", Pagina)
                cm.Parameters.AddWithValue("@PAGESIZE", NumeroRegistros)
                cm.Parameters.AddWithValue("@BUSCARPROVEARTICULO", BuscarProveArt)

                cm.Parameters.AddWithValue("@ABIERTASUSTED", AbtasUsted)
                cm.Parameters.AddWithValue("@PARTICIPO", Participo)
                cm.Parameters.AddWithValue("@PENDIENTES", Pendientes)
                cm.Parameters.AddWithValue("@PENDIENTESDEVOLUCION", PendientesDevolucion)
                cm.Parameters.AddWithValue("@TRASLADADAS", Trasladadas)
                cm.Parameters.AddWithValue("@OTRAS", Otras)
                cm.Parameters.AddWithValue("@FILTRODESGLOSE", FiltroDesglose)
                cm.Parameters.AddWithValue("@VISTADESGLOSE", VistaDesglose)

                da.SelectCommand = cm
                da.Fill(dr)
                Return dr
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Function FSN_Get_TextoLargo_Instancia(ByVal lCiaComp As Long, ByVal Instancia As Integer, ByVal IdCampoOrigen As Integer, ByVal EsDesglose As Boolean,
                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As String
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As SqlCommand = New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSN_ESCENARIOS_GET_TEXTOLARGO_INSTANCIA"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@INSTANCIA", Instancia)
                cm.Parameters.AddWithValue("@IDCAMPOORIGEN", IdCampoOrigen)
                cm.Parameters.AddWithValue("@ESDESGLOSE", EsDesglose)
                da.SelectCommand = cm
                da.Fill(ds)

                Return ds.Tables(0).Rows(0)(0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
                da.Dispose()
            End Try
        End Function
        Public Function FSN_Get_Opciones_Lista_Campo(ByVal lCiaComp As Long, ByVal IdCampo As Integer,
                                            Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As SqlCommand = New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSN_GET_OPCIONES_LISTA_CAMPO"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@IDCAMPO", IdCampo)
                da.SelectCommand = cm
                da.Fill(ds)

                Return ds.Tables(0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        Public Function FSN_Get_Opciones_Lista_EstadoHomologacion(ByVal lCiaComp As Long, SolicitudFormularioVinculados As Dictionary(Of String, FSNLibrary.SerializableKeyValuePair(Of Integer, String)), Optional ByVal SesionId As String = "",
                                                                  Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataTable
            Authenticate(SesionId, IPDir, PersistID)
            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As SqlCommand = New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSN_GET_OPCIONES_LISTA_ESTADOHOMOLOGACION"
                cm.CommandType = CommandType.StoredProcedure

                Dim dtSolicitudes As New DataTable("SOLICITUDES")
                dtSolicitudes.Columns.Add("SOLICITUD", GetType(Integer))
                For Each oSol As KeyValuePair(Of String, FSNLibrary.SerializableKeyValuePair(Of Integer, String)) In SolicitudFormularioVinculados
                    Dim drSol As DataRow = dtSolicitudes.NewRow
                    drSol("SOLICITUD") = CType(oSol.Key, Integer)
                    dtSolicitudes.Rows.Add(drSol)
                Next
                Dim ParamOrdenesId As New SqlParameter("SOLICITUDES", SqlDbType.Structured)
                ParamOrdenesId.TypeName = "ORDENESID"
                ParamOrdenesId.Value = dtSolicitudes
                cm.Parameters.Add(ParamOrdenesId)

                da.SelectCommand = cm
                da.Fill(ds)

                Return ds.Tables(0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
        ''' <summary>
        ''' Devuelve los datos de los proveedores ERP para la el proveedor y la organizacion de compras, se utiliza para el campo proveedor ERP(143) de PM
        ''' </summary>
        ''' <returns>Dataset con los datos de los proveedores ERP</returns>
        ''' <remarks></remarks>
        Public Function ProvesErp_CargarProveedoresERPtoDS(ByVal lCiaComp As Long, Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "") As DataSet
            Authenticate(SesionId, IPDir, PersistID)

            Dim rs As New DataSet
            Dim cn As New SqlConnection(mDBConnection)
            Dim sqlCmd As New SqlCommand
            Dim SqlParam As New SqlParameter
            Dim da As New SqlDataAdapter
            Try
                sqlCmd.CommandText = FSGS(lCiaComp) & "FSPM_DEVOLVER_PROVES_ERP"
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Connection = cn

                da.SelectCommand = sqlCmd
                da.Fill(rs)

                Return rs
            Catch ex As Exception
                Throw ex
            Finally
                cn.Close()
                da.Dispose()
                sqlCmd.Dispose()
            End Try
        End Function
        Public Function FSN_Get_Escenarios_TiposSolicitud_Formulario(ByVal lCiaComp As Long, ByVal Usuario As String, ByVal Idioma As String, ByVal IdsSolicitud As String, ByVal TipoSolicitud As Nullable(Of Integer),
                                Optional ByVal SesionId As String = "", Optional ByVal IPDir As String = "", Optional ByVal PersistID As String = "", Optional ByVal sProve As String = Nothing) As DataTable
            Authenticate(SesionId, IPDir, PersistID)

            Dim cn As New SqlConnection(mDBConnection)
            Dim cm As SqlCommand = New SqlCommand
            Dim ds As New DataSet
            Dim da As New SqlDataAdapter
            Try
                cn.Open()
                cm.Connection = cn
                cm.CommandText = FSGS(lCiaComp) & "FSN_GET_TIPOS_SOLICITUD"
                cm.CommandType = CommandType.StoredProcedure
                cm.Parameters.AddWithValue("@USU", Usuario)
                cm.Parameters.AddWithValue("@IDI", Idioma)
                cm.Parameters.AddWithValue("@TIPOSOLICITUD", TipoSolicitud)
                If IdsSolicitud IsNot String.Empty Then cm.Parameters.AddWithValue("@IDSSOLICITUD", IdsSolicitud)
                cm.Parameters.AddWithValue("@PROVE", sProve)
                da.SelectCommand = cm
                da.Fill(ds)

                Return ds.Tables(0)
            Catch e As Exception
                Throw e
            Finally
                cn.Dispose()
                cm.Dispose()
            End Try
        End Function
#End Region
    End Class
End Namespace