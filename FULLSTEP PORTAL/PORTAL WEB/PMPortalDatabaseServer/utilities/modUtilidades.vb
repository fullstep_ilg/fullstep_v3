Imports System.Runtime.InteropServices

Namespace Fullstep.PMPortalDatabaseServer
    Module modUtilidades
#Region "Variables suplantacion"
        Private Const LOGON32_LOGON_INTERACTIVE As Integer = 2
        Private Const LOGON32_PROVIDER_DEFAULT As Integer = 0

        Declare Function LogonUserA Lib "advapi32.dll" (ByVal lpszUsername As String, _
                                ByVal lpszDomain As String, _
                                ByVal lpszPassword As String, _
                                ByVal dwLogonType As Integer, _
                                ByVal dwLogonProvider As Integer, _
                                ByRef phToken As IntPtr) As Integer

        Declare Auto Function DuplicateToken Lib "advapi32.dll" ( _
                                ByVal ExistingTokenHandle As IntPtr, _
                                ByVal ImpersonationLevel As Integer, _
                                ByRef DuplicateTokenHandle As IntPtr) As Integer

        Declare Auto Function RevertToSelf Lib "advapi32.dll" () As Long
        Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Long
        Public Declare Function FindMimeFromData Lib "urlmon.dll" (ByVal pBC As IntPtr, <MarshalAs(UnmanagedType.LPWStr)> ByVal pwzUrl As String, <MarshalAs(UnmanagedType.LPArray, ArraySubType:=UnmanagedType.I1, SizeParamIndex:=3)> ByVal pBuffer As Byte(), ByVal cbSize As Integer, _
      <MarshalAs(UnmanagedType.LPWStr)> ByVal pwzMimeProposed As String, _
      ByVal dwMimeFlags As Integer, <MarshalAs(UnmanagedType.LPWStr)> _
      ByRef ppwzMimeOut As String, ByVal dwReserved As Integer) As Integer
#End Region
        Function StrToSQLNULL(ByVal StrThatCanBeEmpty As Object) As Object
            If IsDBNull(StrThatCanBeEmpty) Then
                StrToSQLNULL = "NULL"
            Else
                If StrThatCanBeEmpty = "" Then
                    StrToSQLNULL = "NULL"
                Else
                    StrToSQLNULL = "'" & DblQuote(CStr(StrThatCanBeEmpty)) & "'"
                End If
            End If
        End Function
        Public Function DblQuote(ByVal StrToDblQuote As String) As String
            DblQuote = Replace(StrToDblQuote, "'", "''")
        End Function
        Function DateToSQLDate(ByVal DateThatCanBeEmpty As Object) As Object
            Try
                If IsDBNull(DateThatCanBeEmpty) Then
                    Return "NULL"
                Else
                    If IsDate(DateThatCanBeEmpty) Then
                        Return "Convert(datetime,'" & Format(DateThatCanBeEmpty, "MM-dd-yyyy") & "',110)"
                    Else
                        If DateThatCanBeEmpty = "" Then
                            Return "NULL"
                        Else
                            Return "Convert(datetime,'" & Format(DateThatCanBeEmpty, "MM-dd-yyyy") & "',110)"
                        End If
                    End If
                End If
            Catch ex As Exception
                Return "NULL"
            End Try
        End Function
        Public Function DblToSQLFloat(ByVal DblToConvert As Object) As String
            Dim StrToConvert As String
            Try
                If DblToConvert Is Nothing Then
                    DblToSQLFloat = "NULL"
                    Exit Function
                End If
                If IsDBNull(DblToConvert) Then
                    DblToSQLFloat = "NULL "
                    Exit Function
                End If
                If DblToConvert = "" Then
                    DblToSQLFloat = "NULL"
                    Exit Function
                End If
                StrToConvert = CStr(DblToConvert)
                StrToConvert = Replace(StrToConvert, ",", ".")

                DblToSQLFloat = StrToConvert
            Catch ex As Exception
                If IsNumeric(DblToConvert) Then
                    StrToConvert = CStr(DblToConvert)
                    StrToConvert = Replace(StrToConvert, ",", ".")
                    Return StrToConvert
                Else
                    Return "NULL"
                End If
            End Try
        End Function
        Public Function SQLBinaryToBoolean(ByVal var As Object) As Boolean
            If IsDBNull(var) Then
                Return False
            Else
                If var = 1 Then
                    Return True
                Else
                    Return False
                End If
            End If
        End Function
        Public Function DBNullToSomething(Optional ByVal value As Object = Nothing) As Object
            If IsDBNull(value) Then
                Return Nothing
            Else
                Return value
            End If
        End Function
        Public Function DBNullToDbl(Optional ByVal value As Object = Nothing) As Double
            If IsDBNull(value) Then
                Return 0
            ElseIf value Is Nothing Then
                Return 0
            Else
                Return CDbl(value)
            End If
        End Function
        Public Function DateToSQLTimeDate(ByVal DateThatCanBeEmpty As Object) As Object
            Try
                If IsDBNull(DateThatCanBeEmpty) Then
                    Return "NULL"
                Else
                    If IsDate(DateThatCanBeEmpty) Then
                        Return "Convert(datetime,'" & Format(DateThatCanBeEmpty, "MM-dd-yyyy HH:mm:ss") & "',110)"
                    Else
                        If DateThatCanBeEmpty = "" Then
                            Return "NULL"
                        Else
                            Return "Convert(datetime,'" & Format(DateThatCanBeEmpty, "MM-dd-yyyy HH:mm:ss") & "',110)"
                        End If
                    End If
                End If
            Catch ex As Exception
                Return "NULL"
            End Try
        End Function
        ''' <summary>
        ''' Para poder trabajar con FILESTREAM necesitamos suplantacion de identidad, es decir, necesitamos un usuario SQL que pueda
        ''' trabajar con este tipo de ficheros. Para ellos necesitamos que el usuario este creado con esos permisos en SQL
        ''' </summary>
        ''' <returns>Contexto de la suplantacion</returns>
        ''' <remarks>Llamada desde:GrabarNuevoAdjunto; Tiempo m�ximo:0seg.</remarks>
        Public Function RealizarSuplantacion(ByVal UserName As String, ByVal password As String, ByVal Domain As String) As Security.Principal.WindowsImpersonationContext
            Dim impersonationContext As Security.Principal.WindowsImpersonationContext = Nothing
            Dim impersonateValidUser As Boolean
            Dim tempWindowsIdentity As Security.Principal.WindowsIdentity
            Dim token As IntPtr = IntPtr.Zero
            Dim tokenDuplicate As IntPtr = IntPtr.Zero

            impersonateValidUser = False
            If RevertToSelf() Then
                If LogonUserA(UserName, Domain, password, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, token) <> 0 Then
                    If DuplicateToken(token, 2, tokenDuplicate) <> 0 Then
                        tempWindowsIdentity = New Security.Principal.WindowsIdentity(tokenDuplicate)
                        impersonationContext = tempWindowsIdentity.Impersonate()
                        If Not impersonationContext Is Nothing Then
                            impersonateValidUser = True
                        End If
                    End If
                End If
            End If
            If Not tokenDuplicate.Equals(IntPtr.Zero) Then
                CloseHandle(tokenDuplicate)
            End If
            If Not token.Equals(IntPtr.Zero) Then
                CloseHandle(token)
            End If

            Return impersonationContext
        End Function
    End Module
End Namespace