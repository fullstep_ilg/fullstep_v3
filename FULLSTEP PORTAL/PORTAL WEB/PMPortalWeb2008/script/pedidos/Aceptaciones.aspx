﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Aceptaciones.aspx.vb" Inherits="PMPortalWeb2008.Aceptaciones"  %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
	<link href="../../../common/estilo.asp" type="text/css" rel="stylesheet"/>
	<script src="../../../common/formatos.js"></script>
	<script src="../../../common/menu.asp"></script>
	<script language="JavaScript1.2" src="../../../js/hiermenus.js"></script>
    <link href="../styles/Custom.css" rel="stylesheet" type="text/css" />
    <link href="../styles/estilos.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<script type="text/javascript">dibujaMenu(2)</script>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        
        <table width="100%">
            <tr>
                <td colspan="6">
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <asp:Table ID="tblCabecera" runat="server" Width="100%" 
                        BackImageUrl="~/App_Themes/Ono/images/cabeceraFondo.jpg" BorderWidth="0px" 
                        CellPadding="0" CellSpacing="0">                    
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Image ID="imgTitulo" runat="server" ImageUrl="~/App_Themes/Ono/images/noconformidades.gif"  />                            
                            </asp:TableCell>
	                        <asp:TableCell Width="100%" VerticalAlign="Bottom">
                                &nbsp;&nbsp;<asp:Label ID="lblTitulo" runat="server" CssClass="LabelTituloCabeceraCustomControl"></asp:Label>
	                        </asp:TableCell> 
	                        <asp:TableCell Width="6px" VerticalAlign="Bottom">
	                            <asp:Image ID="imgBordeRedondeadoCabecera" runat="server" 
                                    ImageUrl="~/App_Themes/Ono/images/CabeceraFondoRedondo.jpg"  BorderWidth="0px" />	                        
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblDesde" runat="server" Text="DFecha Desde:" CssClass="Etiqueta"></asp:Label>
                </td>
                <td>
                    <igsch:webdatechooser ID="wdcDesde" runat="server" Value="" NullDateLabel="" Width="110px" Editable="true" SkinID="Calendario">                        
                        <DropButton ImageUrl1="../../App_Themes/Ono/images/calendar-color.png"></DropButton>
                    </igsch:webdatechooser>
                </td>
                <td align="center">
                    <asp:Label ID="lblHasta" runat="server" Text="DFecha Hasta:" CssClass="Etiqueta"></asp:Label>                           
                </td>                        
                <td>
                    <igsch:webdatechooser ID="wdcHasta" runat="server" Value="" NullDateLabel="" Width="110px" Editable="true" SkinID="Calendario">
                        <DropButton ImageUrl1="../../App_Themes/Ono/images/calendar-color.png"></DropButton>
                    </igsch:webdatechooser>
                </td>
                <td>
                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="Boton" />
                </td>
                <td width="40%"  >
                </td>
            </tr>
            <tr><td colspan="6"><hr size="5px" class="LineaSeparatoria" /></td></tr>
            <tr><td colspan="6"><br /></td></tr>
            </table>
            
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>                    
                    <table style="position:absolute; width:100%; height:400px; z-index:10000; ">
                    <tr>
                        <td align="center">                                                        
                            <table style="border-style: solid; border-width: 1px; z-index:10001; background-color:White;">
                                <tr>
                                    <td align="center">
                                        <asp:Image ID="ImgProgress" runat="server" ImageUrl="../../App_Themes/Ono/images/cargando.gif" style="vertical-align:middle" />
                                        <asp:Label ID="LblProcesando" runat="server" ForeColor="Black" Text="Cargando ..."></asp:Label>	
                                    </td>
                                </tr>	                        
                            </table>
                        </td>
                    </tr>
                    </table>
                </ProgressTemplate>
            </asp:UpdateProgress>
            
            <asp:UpdatePanel ID="pnlGrid" runat="server">
            <Triggers>
                <asp:PostBackTrigger ControlID="imgbtnExcel" />               
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />   
            </Triggers>
                <ContentTemplate>             
            <table width="100%">
            <tr>
                <td width="33%">
                    <asp:Label ID="lblFiltrarPor" runat="server" Text="DTipo de Filtro:" CssClass="Etiqueta"></asp:Label>
                    <asp:RadioButton ID="optFiltroSeleccion" GroupName="optFiltrarPor"  
                        runat="server" AutoPostBack="true"  />
                    <asp:ImageButton ID="imgFiltrar1" runat="server" />        
                    <asp:RadioButton ID="optFiltrar2" GroupName="optFiltrarPor" runat ="server" AutoPostBack="true" />
                    <asp:ImageButton ID="imgFiltrar2" runat="server" />
                </td>
                <td align="center" width="34%">
                    <table cellpadding="2">
                    <tr>                       
                        <td><asp:ImageButton runat="server" ID="btnFirstPage" /></td>
                        <td><asp:ImageButton runat="server" ID="btnPreviousPage" /></td>
                        <td><asp:Label ID="lblPagina" runat="server" Text="Página"></asp:Label></td>
                        <td><asp:DropDownList ID="ddlPage" runat="server" CausesValidation="False" AutoPostBack="True"></asp:DropDownList></td>
                        <td>
                            <asp:Label ID="lblDe" runat="server" Text="de"></asp:Label>&nbsp;
                            <asp:Label ID="lblPagTot" runat="server" Text="1"></asp:Label>
                        </td>
                        <td><asp:ImageButton runat="server" ID="btnNextPage" /></td>
                        <td><asp:ImageButton runat="server" ID="btnLastPage" /></td>
                    </tr>           
                    </table>
                </td>
                <td align="right" width="33%">
                    <asp:ImageButton ID="imgbtnExcel" runat="server" />
                </td>
            </tr>                   
            <tr>                
                <td colspan="3">  
                    <igtbl:UltraWebGrid ID="uwgAceptaciones" runat="server" SkinID="GridFiltros"  
                        Height="200px" Width="325px">                                                               
                        <Bands> 
                            <igtbl:UltraGridBand ChildBandColumn="POSIC">                        
                                <Columns>
                                    <igtbl:UltraGridColumn BaseColumnName="Ebeln" IsBound="True" Key="Ebeln" Width="21%">
                                    </igtbl:UltraGridColumn>                                    
                                    <igtbl:UltraGridColumn BaseColumnName="Aedat" IsBound="True" Key="Aedat" Width="21%">
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Kbetr" IsBound="True" Key="Kbetr" DataType="System.Decimal" Width="16%">
                                    </igtbl:UltraGridColumn>  
                                    <igtbl:UltraGridColumn BaseColumnName="Bldat" IsBound="True" Key="Bldat" Width="21%">
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="WrbtrC" IsBound="True" Key="WrbtrC" DataType="System.Decimal" Width="21%">
                                    </igtbl:UltraGridColumn>                                                                     
                                </Columns>
                                <AddNewRow Visible="NotSet" View="NotSet"></AddNewRow>
                            </igtbl:UltraGridBand>
                            <igtbl:UltraGridBand>
                                <Columns>
                                    <igtbl:UltraGridColumn BaseColumnName="Belnr" IsBound="True" Key="Belnr" Width="8%">
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Xblnr" IsBound="True" Key="Xblnr" Width="8%">
                                    </igtbl:UltraGridColumn> 
                                    <igtbl:UltraGridColumn BaseColumnName="Ebelp" IsBound="True" Key="Ebelp" Width="8%">                                    
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Matnr" IsBound="True" Key="Matnr" Width="8%">                                        
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Descr" IsBound="True" Key="Descr" Width="12%">
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Menge" IsBound="True" Key="Menge" DataType="System.Decimal" Width="7%">                                        
                                    </igtbl:UltraGridColumn>  
                                    <igtbl:UltraGridColumn BaseColumnName="CantRecep" IsBound="True" Key="CantRecep" DataType="System.Decimal" Width="7%">
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="CantPend" IsBound="True" Key="CantPend" DataType="System.Decimal" Width="7%">
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Wrbtr" IsBound="True" Key="Wrbtr" DataType="System.Decimal" Width="7%">
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Fecha" IsBound="True" Key="Fecha" Width="8%">
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="FechaEr" IsBound="True" Key="FechaEr" Width="8%">
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Diferencia" IsBound="True" Key="Diferencia" Width="7%">
                                    </igtbl:UltraGridColumn>
                                    <igtbl:TemplatedColumn AllowGroupBy="No" AllowRowFiltering="False" CellButtonDisplay="Always" Key="PDF" Width="5%">
                                        <CellTemplate>
                                            <asp:ImageButton ID="imgbtnPDF" runat="server" ImageAlign="Middle" CommandName = "DescargaPDF" />
                                        </CellTemplate>
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </igtbl:TemplatedColumn>                                    
                                </Columns>
                                <AddNewRow Visible="NotSet" View="NotSet"></AddNewRow>
                            </igtbl:UltraGridBand>
                        </Bands>
                    </igtbl:UltraWebGrid>       
                    <igtxt:WebDateTimeEdit ID="wdtBldat" runat="server"></igtxt:WebDateTimeEdit>
                    <igtxt:WebDateTimeEdit ID="wdtAedat" runat="server"></igtxt:WebDateTimeEdit>
                    <igtxt:WebDateTimeEdit ID="wdtFecha" runat="server"></igtxt:WebDateTimeEdit>
                    <igtxt:WebDateTimeEdit ID="wdtFechaER" runat="server"></igtxt:WebDateTimeEdit>
                    
                </td>
            </tr>
            <tr>
                <td></td>
                <td align="center">
                    <table cellpadding="2">
                        <tr>
                            <td><asp:ImageButton runat="server" ID="btnFirstPage2" /></td>
                            <td><asp:ImageButton runat="server" ID="btnPreviousPage2" /></td>
                            <td><asp:Label ID="lblPagina2" runat="server" Text="Página"></asp:Label></td>
                            <td><asp:DropDownList ID="ddlPage2" runat="server" CausesValidation="False" AutoPostBack="True"></asp:DropDownList></td>
                            <td>
                                <asp:Label ID="lblDe2" runat="server" Text="de"></asp:Label>&nbsp;
                                <asp:Label ID="lblPagTot2" runat="server" Text="1"></asp:Label>
                            </td>
                            <td><asp:ImageButton runat="server" ID="btnNextPage2" /></td>
                            <td><asp:ImageButton runat="server" ID="btnLastPage2" /></td>
                        </tr>           
                    </table> 
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Label ID="lblNoDatos" runat="server" Text="DNo hay datos para esa selección" Visible="False" ></asp:Label>
                </td>
            </tr>
        </table>
        
        <igtblexp:ultrawebgridexcelexporter ID="UltraWebGridExcelExporter1" 
            runat='server'>
        </igtblexp:ultrawebgridexcelexporter>
        
        </ContentTemplate>
        </asp:UpdatePanel>             
    </form>
</body>
</html>
