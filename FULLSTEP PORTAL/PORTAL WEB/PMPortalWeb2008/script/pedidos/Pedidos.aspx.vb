﻿Imports Infragistics.WebUI
Imports System.Net
Imports System.IO
Imports System.Threading

Partial Public Class Pedidos
    Inherits System.Web.UI.Page

    Private FSWSServer As Fullstep.PMPortalServer.Root
    Private oUser As Fullstep.PMPortalServer.User
    Private oTextos As DataTable
    Private Const cNOHAYDATOS As String = "No hay datos para esa selección"

    Public gsUserWS As String = ""
    Public gsPwdWS As String = ""

    ''' <summary>
    ''' Carga inicial de la página
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Tiempo máximo 1 sec</remarks>
    Private Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim FSPMServer As New Fullstep.PMPortalServer.Root
        Dim CiaCode As String
        Dim Username As String
        Dim Password As String
        Dim oUser As Fullstep.PMPortalServer.User


        Dim lCiaComp As Long = Request.Cookies("USU")("CIACOMP")

        ' Decode the encoded string.
        CiaCode = HttpUtility.UrlDecode(Request.Cookies("USU")("CIAFSPM"))


        Username = Fullstep.PMPortalServer.Encrypter.Encrypt(Request.Cookies("USU")("COD"), , False, Fullstep.PMPortalServer.Encrypter.TipoDeUsuario.Persona, , True)

        Password = Fullstep.PMPortalServer.Encrypter.Encrypt(Request.Cookies("USU")("PWD"), , False, Fullstep.PMPortalServer.Encrypter.TipoDeUsuario.Persona, , True)

        If Not Session("FS_PM_User") Is Nothing Then
            oUser = Session("FS_PM_User")

            If oUser.Cod <> Username Then
                oUser = FSPMServer.Login(CiaCode, Username, Password, False)
                If oUser Is Nothing Then
                    Response.Redirect(ConfigurationManager.AppSettings("rutanormal"))
                Else
                    oUser.LoadUserData(Username)
                    FSPMServer.Load_LongitudesDeCodigos(lCiaComp)
                    Session("FS_PM_User") = oUser
                    Session("FS_PM_Server") = FSPMServer
                    HttpContext.Current.User = Session("FS_PM_User")
                    Exit Sub
                End If
            End If

            If Not Request.Cookies("USU")("Idioma") Is Nothing Then
                If oUser.Idioma <> Request.Cookies("USU")("Idioma") Then
                    oUser = FSPMServer.Login(CiaCode, Username, Password, False)
                    If oUser Is Nothing Then
                        Response.Redirect(ConfigurationManager.AppSettings("rutanormal"))


                    Else
                        oUser.LoadUserData(Username)
                        FSPMServer.Load_LongitudesDeCodigos(lCiaComp)
                        Session("FS_PM_User") = oUser
                        Session("FS_PM_Server") = FSPMServer
                        HttpContext.Current.User = Session("FS_PM_User")
                        Exit Sub
                    End If

                End If
            End If


        Else
            oUser = FSPMServer.Login(CiaCode, Username, Password, False)
            If oUser Is Nothing Then
                Response.Redirect(ConfigurationManager.AppSettings("rutanormal"))


            Else
                oUser.LoadUserData(Username)
                FSPMServer.Load_LongitudesDeCodigos(lCiaComp)
                Session("FS_PM_User") = oUser
                Session("FS_PM_Server") = FSPMServer
                HttpContext.Current.User = Session("FS_PM_User")
                Exit Sub
            End If

        End If

    End Sub

    ''' <summary>
    ''' Carga inicial de la página
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Tiempo máximo 1 sec</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        CargarParametrosWebService()

        FSWSServer = Session("FS_PM_Server")
        oUser = Session("FS_PM_User")

        Dim sIdi As String = oUser.Idioma
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If
        Dim oDict As Fullstep.PMPortalServer.Dictionary = FSWSServer.Get_Dictionary()
        oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.VisorPedidosAceptaciones, sIdi)
        oTextos = oDict.Data.Tables(0)

        uwgPedidos.DisplayLayout.Pager.PageSize = CInt(ConfigurationManager.AppSettings("PageSize"))

        If Not IsPostBack Then
            Dim ci As New Globalization.CultureInfo(ReferenciaCultural(oUser.Idioma))
            ci.DateTimeFormat = oUser.DateFormat
            wdcDesde.CalendarLayout.Culture = ci
            wdcHasta.CalendarLayout.Culture = ci

            wdcDesde.Value = DateAdd(DateInterval.Month, -6, Now())
            wdcHasta.Value = Now
            'wdcDesde.Value = "01/01/2008"
            'wdcHasta.Value = "01/05/2008"
            optFiltroSeleccion.Checked = True

            lblTitulo.Text = oTextos.Rows(45).Item(1)
            lblDesde.Text = oTextos.Rows(46).Item(1) & ":"
            lblHasta.Text = oTextos.Rows(47).Item(1) & ":"
            lblFiltrarPor.Text = oTextos.Rows(48).Item(1) & ":"
            btnBuscar.Text = oTextos.Rows(51).Item(1)
            lblNoDatos.Text = oTextos.Rows(62).Item(1) 'No hay datos para los parámetros seleccionados

            uwgPedidos.DataSource = ObtenerPedidos()


            With uwgPedidos.Bands(0)
                .Columns.FromKey("Aedat").SortIndicator = UltraWebGrid.SortIndicator.Descending
                .SortedColumns.Add(.Columns.FromKey("Aedat"), True)
            End With
            uwgPedidos.DataBind()
            InitPaginador()
        End If


        uwgPedidos.DisplayLayout.FilterOptionsDefault.FilterUIType = IIf(optFiltroSeleccion.Checked = True, Infragistics.WebUI.UltraWebGrid.FilterUIType.HeaderIcons, Infragistics.WebUI.UltraWebGrid.FilterUIType.FilterRow)
        imgTitulo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/Pedidos.gif"
        imgFiltrar1.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/filtro_cabecera.gif"
        imgFiltrar2.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/filtro_columna.gif"
        imgbtnExcel.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/excel.gif"

    End Sub

    ''' <summary>
    ''' En este procedimiento cargaremos los parámetros que vamos a utilizar en las llamadas a los WebServices
    ''' </summary>
    ''' <remarks>Llamada desde Page_Load
    ''' Tiempo máximo 0 sec</remarks>
    Private Sub CargarParametrosWebService()
        'Leemos los parámetros de conexion al WebService del fichero .ini
        Dim fs = New FileStream(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory(), "ConexionWSOno.ini"), FileMode.Open, FileAccess.Read)
        Dim d As New StreamReader(fs)
        d.BaseStream.Seek(0, SeekOrigin.Begin)
        If d.Peek() > -1 Then
            gsUserWS = d.ReadLine()

            If InStr(1, gsUserWS, "USER=") Then
                gsUserWS = Strings.Right(gsUserWS, Len(gsUserWS) - 5)
            End If
        End If
        If d.Peek() > -1 Then
            gsPwdWS = d.ReadLine()

            If InStr(1, gsPwdWS, "PWD=") Then
                gsPwdWS = Strings.Right(gsPwdWS, Len(gsPwdWS) - 4)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Función que devuelve una colección de pedidos tras llamar al webservice
    ''' </summary>
    ''' <returns>Una colección de pedidos</returns>
    ''' <remarks>Llamada desde: Page_Load y el botón Buscar
    ''' Tiempo máximo: 1 sec.</remarks>
    Private Function ObtenerPedidos() As IEnumerable(Of WSHistPed.ZMPVP_PEDIDOS)

        Dim WSPedidos As New WSHistPed.ZMPVP_WS_RECUP_HIST_PED
        Dim rPedidos As New WSHistPed.ZmpvpRecupHistorialPedidoResponse

        Dim sUserWS As String = ""
        Dim sPwdWS As String = ""

        'Pasamos el login de acceso al webservice
        Dim oCredentials As New NetworkCredential(gsUserWS, gsPwdWS)
        WSPedidos.Credentials = oCredentials

        'Llamamos al webservice
        Dim o As New WSHistPed.ZmpvpRecupHistorialPedido
        o.F_FIN = Format(CDate(wdcHasta.Value), "ddMMyyyy")
        o.F_INI = Format(CDate(wdcDesde.Value), "ddMMyyyy")
        o.STCD1 = oUser.NIFCia
        Try
            rPedidos = WSPedidos.ZmpvpRecupHistorialPedido(o)

            Dim SesionTimeOut As TimeSpan = New TimeSpan(0, 0, Session.Timeout, 0, 0)
            Cache.Insert("Pedidos", rPedidos.TI_PEDIDOS, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, SesionTimeOut)

            Return rPedidos.TI_PEDIDOS
        Catch
            OcultarGrid()
            Return Nothing
        End Try

    End Function

    ''' <summary>
    ''' Evento que salta al cargarse la grid y en el controlaremos si tiene datos o no, para mostrarla u ocultarla
    ''' </summary>
    ''' <param name="sender">la grid de Pedidos</param>
    ''' <param name="e">Argumentos de evento</param>
    ''' <remarks>Tiempo máximo: 0 sec</remarks>
    Private Sub uwgPedidos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles uwgPedidos.DataBound
        Dim oPedidos As IEnumerable(Of WSHistPed.ZMPVP_PEDIDOS) = Cache("Pedidos")
        If oPedidos IsNot Nothing Then
            If oPedidos.Count = 0 OrElse (oPedidos.Count = 1 And oPedidos(0).OBSERV = cNOHAYDATOS) Then
                OcultarGrid()
            Else
                MostrarGrid()
            End If
        Else
            OcultarGrid()
        End If
    End Sub

    ''' <summary>
    ''' Procedimiento en el que ocultaremos la grid y sus controles relacionados
    ''' </summary>
    ''' <remarks>Llamada desde uwgPedidos_DataBound
    ''' Tiempo máximo 0 sec</remarks>
    Private Sub OcultarGrid()
        lblFiltrarPor.Visible = False
        optFiltroSeleccion.Visible = False
        imgFiltrar1.Visible = False
        optFiltrar2.Visible = False
        imgFiltrar2.Visible = False
        imgbtnExcel.Visible = False
        uwgPedidos.Visible = False
        lblNoDatos.Visible = True
        btnFirstPage.Visible = False
        btnPreviousPage.Visible = False
        lblPagina.Visible = False
        ddlPage.Visible = False
        lblDe.Visible = False
        lblPagTot.Visible = False
        btnNextPage.Visible = False
        btnLastPage.Visible = False

    End Sub

    ''' <summary>
    ''' Procedimiento en el que mostraremos la grid y sus controles relacionados
    ''' </summary>
    ''' <remarks>Llamada desde uwgPedidos_DataBound
    ''' Tiempo máximo 0 sec</remarks>
    Private Sub MostrarGrid()
        lblFiltrarPor.Visible = True
        optFiltroSeleccion.Visible = True
        imgFiltrar1.Visible = True
        optFiltrar2.Visible = True
        imgFiltrar2.Visible = True
        imgbtnExcel.Visible = True
        uwgPedidos.Visible = True
        lblNoDatos.Visible = False
        btnFirstPage.Visible = True
        btnPreviousPage.Visible = True
        lblPagina.Visible = True
        ddlPage.Visible = True
        lblDe.Visible = True
        lblPagTot.Visible = True
        btnNextPage.Visible = True
        btnLastPage.Visible = True
    End Sub

    ''' <summary>
    ''' Evento que salta tras aplicar un filtro en la grid, y en el que nos iremos a la primera página de la misma
    ''' </summary>
    ''' <param name="sender">La propia grid</param>
    ''' <param name="e">Argumentos de evento</param>
    ''' <remarks>Tiempo máximo 0 sec</remarks>
    Private Sub uwgPedidos_RowFilterApplied(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.FilterEventArgs) Handles uwgPedidos.RowFilterApplied
        uwgPedidos.DisplayLayout.Pager.CurrentPageIndex = 1
        uwgPedidos.DataSource = Cache("Pedidos")
        uwgPedidos.DataBind()
        InitPaginador()
    End Sub


    ''' <summary>
    ''' Evento que salta al seleccionar un filtro.
    ''' Si se trata del filtro de "=" y se trata de una fecha, lo transforma a la fecha que utiliza para la comparacion
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo=0seg.</remarks>
    Private Sub uwgPedidos_RowFilterApplying(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.FilterEventArgs) Handles uwgPedidos.RowFilterApplying

        If e.ActiveColumnFilter.Column.Band.Index = 1 Then
            uwgPedidos.DisplayLayout.FilterOptionsDefault.AllowRowFiltering = UltraWebGrid.RowFiltering.OnClient
        Else
            uwgPedidos.DisplayLayout.FilterOptionsDefault.AllowRowFiltering = UltraWebGrid.RowFiltering.OnServer
        End If

        If Not optFiltroSeleccion.Checked AndAlso IsDate(e.AppliedFilterCondition.CompareValue) Then
            Dim dayCompareValue As Integer = Day(e.AppliedFilterCondition.CompareValue)
            Dim monthCompareValue As Integer = Month(e.AppliedFilterCondition.CompareValue)
            Dim yearCompareValue As Integer = Year(e.AppliedFilterCondition.CompareValue)

            If dayCompareValue <= 12 Then
                e.AppliedFilterCondition.CompareValue = New Date(yearCompareValue, dayCompareValue, monthCompareValue)
            End If
        ElseIf Not IsNumeric(e.AppliedFilterCondition.CompareValue) Then
            If UCase(e.AppliedFilterCondition.CompareValue) = "(MI_ALL)" Then
                e.Cancel = True
            End If
        End If


    End Sub

    ''' <summary>
    ''' Evento que salta al inicializarse el diseño de la grid, y en el que aplicaremos textos y formatos a la misma
    ''' </summary>
    ''' <param name="sender">La propia grid</param>
    ''' <param name="e">Argumentos de evento</param>
    ''' <remarks>Tiempo máximo 1 sec</remarks>
    Protected Sub uwgPedidos_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgPedidos.InitializeLayout
        Dim oPedidos As IEnumerable(Of WSHistPed.ZMPVP_PEDIDOS) = Cache("Pedidos")
        If oPedidos.Count = 0 OrElse (oPedidos.Count = 1 And oPedidos(0).Observ = cNOHAYDATOS) Then
            If oPedidos.Count = 1 Then
                If oPedidos(0).Observ <> "" Then
                    lblNoDatos.Text = oPedidos(0).Observ
                End If
            End If

        Else
            'Recorremos las columnas para ir metiendo los valores a los filtros para todas los registros (no sólo los que se muestran)
            For Each ugColumn As UltraWebGrid.UltraGridColumn In e.Layout.Grid.Bands(0).Columns
                GatherFilterDataForColumn(ugColumn)
            Next
        End If

        wdtAedat.DisplayModeFormat = oUser.DateFormat.ShortDatePattern
        wdtAedat.EditModeFormat = oUser.DateFormat.ShortDatePattern
        wdtAedat.DataMode = Infragistics.WebUI.WebDataInput.DateDataMode.Date

        wdtEindt.DisplayModeFormat = oUser.DateFormat.ShortDatePattern
        wdtEindt.EditModeFormat = oUser.DateFormat.ShortDatePattern
        wdtEindt.DataMode = Infragistics.WebUI.WebDataInput.DateDataMode.Date



        With uwgPedidos.Bands(0).Columns
            .FromKey("Ebeln").Header.Caption = oTextos.Rows(12).Item(1) ' "Nº Pedido"
            .FromKey("Ebeln").Header.Title = oTextos.Rows(1).Item(1) '"Número de Pedido"

            .FromKey("Aedat").Header.Caption = oTextos.Rows(2).Item(1) '"Fecha"
            .FromKey("Aedat").Header.Title = oTextos.Rows(2).Item(1) '"Fecha"
            .FromKey("Aedat").DataType = "System.DateTime"
            .FromKey("Aedat").Format = oUser.DateFormat.ShortDatePattern
            .FromKey("Aedat").EditorControlID = wdtAedat.UniqueID
            .FromKey("Aedat").Type = Infragistics.WebUI.UltraWebGrid.ColumnType.Custom

            .FromKey("Kbetr").Header.Caption = oTextos.Rows(50).Item(1) '"Importe total"
            .FromKey("Kbetr").Header.Title = oTextos.Rows(50).Item(1) '"Importe total"
            .FromKey("Kbetr").Format = "###,###,###,###,##0." & Replace(Space(oUser.NumberFormat.NumberDecimalDigits), " ", "0")

            .FromKey("Solic").Header.Caption = oTextos.Rows(4).Item(1) '"Solicitante"
            .FromKey("Solic").Header.Title = oTextos.Rows(4).Item(1) '"Solicitante"

            .FromKey("Ekgrp").Header.Caption = oTextos.Rows(5).Item(1) '"Comprador"
            .FromKey("Ekgrp").Header.Title = oTextos.Rows(5).Item(1) '"Comprador"

            .FromKey("Zterm").Header.Caption = oTextos.Rows(7).Item(1) '"Forma de Pago"
            .FromKey("Zterm").Header.Title = oTextos.Rows(7).Item(1) '"Forma de Pago"

            .FromKey("Observ").Header.Caption = oTextos.Rows(8).Item(1) '"Observaciones"
            .FromKey("Observ").Header.Title = oTextos.Rows(8).Item(1) '"Observaciones"

            .FromKey("PDF").Header.Caption = oTextos.Rows(10).Item(1) '"PDF"
            .FromKey("PDF").Header.Title = oTextos.Rows(11).Item(1) '"Descarga Pedido en PDF"

        End With

        With uwgPedidos.Bands(1).Columns

            .FromKey("Ebelp").Header.Caption = oTextos.Rows(0).Item(1) ' "Núm."
            .FromKey("Ebelp").Header.Title = oTextos.Rows(13).Item(1) '"Número de Posición"

            .FromKey("Matnr").Header.Caption = oTextos.Rows(44).Item(1) '"Material"
            .FromKey("Matnr").Header.Title = oTextos.Rows(44).Item(1) '"Material"

            .FromKey("Txz01").Header.Caption = oTextos.Rows(14).Item(1) '  "Descr. Corta"
            .FromKey("Txz01").Header.Title = oTextos.Rows(17).Item(1) '"Descripción Corta"

            .FromKey("DescLarga").Header.Caption = oTextos.Rows(16).Item(1) '"Descr. Larga"
            .FromKey("DescLarga").Header.Title = oTextos.Rows(16).Item(1) '"Descripción Larga"

            .FromKey("Idnlf").Header.Caption = oTextos.Rows(18).Item(1) '"Fabricante"
            .FromKey("Idnlf").Header.Title = oTextos.Rows(18).Item(1) '"Fabricante"

            .FromKey("Menge").Header.Caption = oTextos.Rows(19).Item(1) '"Cantidad"
            .FromKey("Menge").Header.Title = oTextos.Rows(19).Item(1) '"Cantidad"

            .FromKey("Netpr").Header.Caption = oTextos.Rows(20).Item(1) '"Precio Neto"
            .FromKey("Netpr").Header.Title = oTextos.Rows(20).Item(1) '"Precio Neto"
            .FromKey("Netpr").Format = "###,###,###,###,##0." & Replace(Space(oUser.NumberFormat.NumberDecimalDigits), " ", "0")

            .FromKey("Kbetr").Header.Caption = oTextos.Rows(3).Item(1) '"Importe"
            .FromKey("Kbetr").Header.Title = oTextos.Rows(3).Item(1) '"Importe"
            .FromKey("Kbetr").Format = "###,###,###,###,##0." & Replace(Space(oUser.NumberFormat.NumberDecimalDigits), " ", "0")

            .FromKey("Eindt").Header.Caption = oTextos.Rows(21).Item(1) '"Fecha entrega"
            .FromKey("Eindt").Header.Title = oTextos.Rows(22).Item(1) '"Fecha de entrega"
            .FromKey("Eindt").DataType = "System.DateTime"
            .FromKey("Eindt").Format = oUser.DateFormat.ShortDatePattern
            .FromKey("Eindt").EditorControlID = wdtEindt.UniqueID
            .FromKey("Eindt").Type = Infragistics.WebUI.UltraWebGrid.ColumnType.Custom

            .FromKey("City1").Header.Caption = oTextos.Rows(23).Item(1) '"Dir. entrega"
            .FromKey("City1").Header.Title = oTextos.Rows(24).Item(1) '"Dirección de entrega"

        End With

        uwgPedidos.DisplayLayout.GroupByBox.Prompt = "(" & oTextos.Rows(49).Item(1) & ")" '"desplace aquí una columna para agrupar por ese concepto"

        With uwgPedidos.DisplayLayout.FilterOptionsDefault
            .AllString = "(" & oTextos.Rows(25).Item(1) & ")" '(All)
            .ContainsString = oTextos.Rows(26).Item(1) 'Contains
            .DoesNotContainString = oTextos.Rows(27).Item(1) 'Does Not Contain
            .DoesNotEndWithString = oTextos.Rows(28).Item(1) 'Does Not End With
            .DoesNotStartWithString = oTextos.Rows(29).Item(1) 'Does Not Start With
            .EmptyString = oTextos.Rows(30).Item(1) '(Empty)
            .EndsWithString = oTextos.Rows(31).Item(1) 'Ends With
            .EqualsString = oTextos.Rows(32).Item(1) 'Equals
            .GreaterThanOrEqualsString = oTextos.Rows(33).Item(1) 'Greater Than Or Equals
            .GreaterThanString = oTextos.Rows(34).Item(1) 'Greater Than
            .LessThanOrEqualsString = oTextos.Rows(35).Item(1) 'Less Than Or Equals
            .LessThanString = oTextos.Rows(36).Item(1) 'Less Than
            .LikeString = oTextos.Rows(37).Item(1) 'Like
            .NonEmptyString = oTextos.Rows(38).Item(1) '(Non Empty)
            .NotEqualsString = oTextos.Rows(39).Item(1) 'Not Equals
            .NotLikeString = oTextos.Rows(40).Item(1) 'Not Like
            .StartsWithString = oTextos.Rows(41).Item(1) 'Stars With
        End With

    End Sub

    ''' <summary>
    ''' Procedimiento en el que vamos a reunir mediante Linq la información de todos los registros de una columna 
    ''' para mostrarlos en los filtros
    ''' </summary>
    ''' <param name="oColumn">Columna en cuestión</param>
    ''' <remarks>Llamada desde uwgPedidos_InitializeLayout
    ''' Tiempo máximo 0 sec</remarks>
    Private Sub GatherFilterDataForColumn(ByVal oColumn As UltraWebGrid.UltraGridColumn)
        Dim oPedidos As IEnumerable(Of WSHistPed.ZMPVP_PEDIDOS) = Cache("Pedidos")

        Select Case oColumn.Key
            Case "Ebeln"
                Dim aFilters = From oRow In oPedidos _
                Where oRow.Ebeln IsNot DBNull.Value _
                Select sFilter = _
                oRow.Ebeln.Replace("""", "\""") _
                Distinct Order By sFilter

                oColumn.GatherFilterData = [Shared].DefaultableBoolean.False
                oColumn.FilterCollectionValues.Clear()
                For Each sFilter In aFilters
                    oColumn.FilterCollectionValues.Add(sFilter, sFilter)
                Next
            Case "Aedat"
                'Where oRow.AEDAT IsNot DBNull.Value _
                Dim aFilters = From oRow In oPedidos _
                Select sFilter = _
                oRow.AEDAT _
                Distinct Order By sFilter

                oColumn.GatherFilterData = [Shared].DefaultableBoolean.False
                oColumn.FilterCollectionValues.Clear()
                For Each sFilter In aFilters
                    oColumn.FilterCollectionValues.Add(sFilter, Format(CDate(sFilter), oUser.DateFormat.ShortDatePattern))
                Next
            Case "Kbetr"
                Dim aFilters = From oRow In oPedidos _
                Select sFilter = _
                oRow.Kbetr _
                Distinct Order By sFilter

                oColumn.GatherFilterData = [Shared].DefaultableBoolean.False
                oColumn.FilterCollectionValues.Clear()
                For Each sFilter In aFilters
                    oColumn.FilterCollectionValues.Add(CDbl(sFilter), sFilter.ToString("N", oUser.NumberFormat))
                Next
            Case "Solic"
                Dim aFilters = From oRow In oPedidos _
                Where oRow.Solic IsNot DBNull.Value _
                Select sFilter = _
                oRow.Solic.Replace("""", "\""") _
                Distinct Order By sFilter

                oColumn.GatherFilterData = [Shared].DefaultableBoolean.False
                oColumn.FilterCollectionValues.Clear()
                For Each sFilter In aFilters
                    oColumn.FilterCollectionValues.Add(sFilter, sFilter)
                Next
            Case "Ekgrp"
                Dim aFilters = From oRow In oPedidos _
                Where oRow.Ekgrp IsNot DBNull.Value _
                Select sFilter = _
                oRow.Ekgrp.Replace("""", "\""") _
                Distinct Order By sFilter

                oColumn.GatherFilterData = [Shared].DefaultableBoolean.False
                oColumn.FilterCollectionValues.Clear()
                For Each sFilter In aFilters
                    oColumn.FilterCollectionValues.Add(sFilter, sFilter)
                Next
            Case "Zterm"
                Dim aFilters = From oRow In oPedidos _
                Where oRow.Zterm IsNot DBNull.Value _
                Select sFilter = _
                oRow.Zterm.Replace("""", "\""") _
                Distinct Order By sFilter

                oColumn.GatherFilterData = [Shared].DefaultableBoolean.False
                oColumn.FilterCollectionValues.Clear()
                For Each sFilter In aFilters
                    oColumn.FilterCollectionValues.Add(sFilter, sFilter)
                Next
            Case "Observ"
                Dim aFilters = From oRow In oPedidos _
                Where oRow.Observ IsNot DBNull.Value _
                Select sFilter = _
                oRow.Observ.Replace("""", "\""") _
                Distinct Order By sFilter

                oColumn.GatherFilterData = [Shared].DefaultableBoolean.False
                oColumn.FilterCollectionValues.Clear()
                For Each sFilter In aFilters
                    oColumn.FilterCollectionValues.Add(sFilter, sFilter)
                Next
        End Select
    End Sub

    ''' <summary>
    ''' Evento que salta al cargarse cada registro de la grid y en el personalizaremos cada pedido (si está leido o no)
    ''' </summary>
    ''' <param name="sender">La grid</param>
    ''' <param name="e">Argumentos de evento</param>
    ''' <remarks>Tiempo máximo 1 sec</remarks>
    Protected Sub uwgPedidos_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgPedidos.InitializeRow

        Dim sAux As String = ""
        Select Case e.Row.Band.Index
            Case 0
                If e.Row.Cells.FromKey("ESTADOLEC").Value <> Nothing AndAlso e.Row.Cells.FromKey("ESTADOLEC").Value.ToString <> "X" Then
                    e.Row.Style.Font.Bold = True
                End If

                e.Row.Cells.FromKey("Observ").Value = Replace(e.Row.Cells.FromKey("Observ").Value, "crlf", vbCrLf)

                'Ahora recogemos las líneas para el campo Observ

                'ImageButton de PDF
                Dim tc As Infragistics.WebUI.UltraWebGrid.TemplatedColumn = CType(e.Row.Band.Columns.FromKey("PDF"), Infragistics.WebUI.UltraWebGrid.TemplatedColumn)
                Dim imgBoton As ImageButton = CType(CType(tc.CellItems(e.Row.Index), Infragistics.WebUI.UltraWebGrid.CellItem).FindControl("imgbtnPDF"), ImageButton)
                imgBoton.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/rellenar_12x12.gif"
                ScriptManager1.RegisterPostBackControl(imgBoton)

            Case 1
                If e.Row.ParentRow.Cells.FromKey("ESTADOLEC").Value <> Nothing AndAlso e.Row.ParentRow.Cells.FromKey("ESTADOLEC").Value.ToString <> "X" Then
                    e.Row.Style.Font.Bold = True
                End If

                e.Row.Cells.FromKey("DescLarga").Value = Replace(e.Row.Cells.FromKey("DescLarga").Value, "crlf", vbCrLf)

        End Select

    End Sub

    ''' <summary>
    ''' Evento que salta al pinchar sobre el botón PDF de un pedido y que descargará en una nueva ventana su detalle en PDF
    ''' </summary>
    ''' <param name="sender">la grid</param>
    ''' <param name="e">argumentos de evento</param>
    ''' <remarks>Tiempo máximo 0 sec</remarks>
    Protected Sub uwgPedidos_ItemCommand(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.UltraWebGridCommandEventArgs) Handles uwgPedidos.ItemCommand
        If e.InnerCommandEventArgs.CommandName = "DescargaPDF" Then
            Dim WSImpPed As New WSImpPed.ZMPVP_WS_IMP_PED

            'Pasamos el login de acceso al webservice
            Dim oCredentials As New NetworkCredential(gsUserWS, gsPwdWS)
            WSImpPed.Credentials = oCredentials

            'Dim sNumPedido As String = CType(Cache("Pedidos"), IEnumerable(Of WSHistPed.ZmpvpPedidos))(e.InnerCommandEventArgs.CommandArgument).Ebeln

            Dim sNumPedido As String = e.InnerCommandEventArgs.CommandArgument

            'Se hace la llamada al webservice que provoca la descarga del pdf 
            Dim o As New WSImpPed.ZMPVP_IMP_FORM_PEDIDO
            o.PEDIDO = sNumPedido
            Dim sResult As WSImpPed.ZMPVP_IMP_FORM_PEDIDOResponse = WSImpPed.ZMPVP_IMP_FORM_PEDIDO(o)

            If UCase(sResult.OK) <> "ERROR!!" Then
                'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "download", "window.open('Download.aspx?file=" & HttpContext.Current.Server.UrlEncode(ConfigurationManager.AppSettings("Ruta_PDFs") & sNumPedido) & ".pdf', '', 'height=1,width=1,left=2000,top=2000,toolbar=no,menubar=no','true');window.focus();", True)
                'ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "download", "window.open('Download.aspx?file=" & HttpContext.Current.Server.UrlEncode(ConfigurationManager.AppSettings("Ruta_PDFs") & sNumPedido) & ".pdf', '', 'height=1,width=1,left=2000,top=2000,toolbar=no,menubar=no','true');window.focus();", True)

                Dim path As String = ConfigurationManager.AppSettings("Ruta_PDFs") & sResult.OK
                Dim file As System.IO.FileInfo = New System.IO.FileInfo(path)
                Dim iNumIntentosPDF = CInt(ConfigurationManager.AppSettings("NumIntentosPDF"))
                Dim iIntentos As Integer = 0

                While iIntentos < iNumIntentosPDF
                    If file.Exists Then

                        'Y una vez que abrimos el PDF marcamos la fila como Leída llamando al webservice de actualización
                        Dim WSActPed As New WSActPed.ZMPVP_WS_ACT_PED
                        'Pasamos el login de acceso al webservice
                        oCredentials = New NetworkCredential(gsUserWS, gsPwdWS)
                        WSActPed.Credentials = oCredentials
                        Dim obj As New WSActPed.ZMPVP_ACTUALIZAR_PED
                        obj.PEDIDO = sNumPedido
                        WSActPed.ZMPVP_ACTUALIZAR_PED(obj)

                        'Finalmente actualizamos la grid llamando al WebService para que venga actualizado el campo estado
                        Dim colPedidos As IEnumerable(Of WSHistPed.ZMPVP_PEDIDOS) = Nothing
                        colPedidos = ObtenerPedidos()
                        If colPedidos IsNot Nothing Then
                            uwgPedidos.DataSource = colPedidos
                            uwgPedidos.DataBind()
                            InitPaginador()
                        End If
                        
                        Response.Clear()
                        Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
                        Response.AddHeader("Content-Length", file.Length.ToString())
                        Response.ContentType = "application/octet-stream"
                        Response.WriteFile(file.FullName)
                        Response.End()
                        Exit While
                    Else
                        'Si el fichero no existe, esperamos y reintentamos
                        Thread.Sleep(TimeSpan.FromSeconds(CDbl(ConfigurationManager.AppSettings("SegundosEntreIntentos"))))
                        iIntentos = iIntentos + 1
                    End If
                End While
                If iIntentos = iNumIntentosPDF Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "NoFichero", "alert('" & oTextos.Rows(61).Item(1) & " (" & sResult.OK & ")');", True) '"No se encuentra el fichero solicitado (xxx.pdf)"                     
                    Exit Sub
                End If

            Else
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "NoPDF", "alert('" & sNumPedido & " " & oTextos.Rows(59).Item(1) & "');", True)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Evento click del botón de buscar, en el que llamaremos al webservice para refrescar los datos
    ''' </summary>
    ''' <param name="sender">botón buscar</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Tiempo máximo 1 sec</remarks>
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        uwgPedidos.DisplayLayout.Pager.CurrentPageIndex = 1

        Dim colPedidos As IEnumerable(Of WSHistPed.ZMPVP_PEDIDOS) = Nothing
        colPedidos = ObtenerPedidos()
        If colPedidos IsNot Nothing Then
            uwgPedidos.DataSource = colPedidos
            With uwgPedidos.Bands(0)
                '.SortedColumns.Clear()
                .Columns.FromKey("Aedat").SortIndicator = UltraWebGrid.SortIndicator.Descending
                .SortedColumns.Add(.Columns.FromKey("Aedat"), True)
            End With
            uwgPedidos.DataBind()
            InitPaginador()
        End If
    End Sub

    ''' <summary>
    ''' Evento que salta al cambiar de página en la grid
    ''' </summary>
    ''' <param name="sender">La propia grid</param>
    ''' <param name="e">Argumentos de evento</param>
    ''' <remarks>Tiempo máximo 0 sec</remarks>
    Private Sub uwgPedidos_PageIndexChanged(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.PageEventArgs) Handles uwgPedidos.PageIndexChanged
        uwgPedidos.DataSource = Cache("Pedidos")
        uwgPedidos.DataBind()
    End Sub

    ''' <summary>
    ''' Evento que salta al ordenar una columna de la grid y en el que nos iremos a la primera página de la misma
    ''' </summary>
    ''' <param name="sender">La propia grid</param>
    ''' <param name="e">Argumentos de evento</param>
    ''' <remarks>Tiempo máximo 0 sec</remarks>
    Private Sub uwgPedidos_SortColumn(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.SortColumnEventArgs) Handles uwgPedidos.SortColumn

        If e.BandNo = 0 Then
            uwgPedidos.DisplayLayout.Pager.CurrentPageIndex = 1
            uwgPedidos.DataSource = Cache("Pedidos")
            uwgPedidos.DataBind()
            InitPaginador()
        End If

    End Sub

    ''' <summary>
    ''' Evento que salta al seleccionar la opción de filtro por selección y en el que recargaremos la grid para actualizar el tipo de filtrado
    ''' </summary>
    ''' <param name="sender">el optionbutton</param>
    ''' <param name="e">argumentos de evento</param>
    ''' <remarks>Tiempo máximo 0 sec</remarks>
    Private Sub optFiltroSeleccion_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optFiltroSeleccion.CheckedChanged
        'uwgPedidos.Rows.ColumnFilters.Clear()
        uwgPedidos.DataSource = Cache("Pedidos")
        uwgPedidos.DataBind()
        InitPaginador()
    End Sub


    ''' <summary>
    ''' Evento que salta al seleccionar la opción de filtro por fila y en el que recargaremos la grid para actualizar el tipo de filtrado
    ''' </summary>
    ''' <param name="sender">el optionbutton</param>
    ''' <param name="e">argumentos de evento</param>
    ''' <remarks>Tiempo máximo 0 sec</remarks>
    Private Sub optFiltrar2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optFiltrar2.CheckedChanged
        'uwgPedidos.Rows.ColumnFilters.Clear()
        uwgPedidos.DataSource = Cache("Pedidos")
        uwgPedidos.DataBind()
        InitPaginador()
    End Sub

#Region "Controles Paginación"

    ''' <summary>
    ''' Procedimiento controlador de la paginación, en el que determinaremos en qué página estamos
    ''' </summary>
    ''' <remarks>Llamada desde todos los eventos/funciones y procedimientos que recargan la grid 
    ''' Tiempo máximo 0 sec</remarks>
    Public Sub InitPaginador()

        With uwgPedidos.DisplayLayout.Pager

            'Actualizamos las combos de paginación
            Dim i As Integer
            ddlPage.Items.Clear()
            ddlPage2.Items.Clear()
            For i = 0 To .PageCount - 1
                ddlPage.Items.Insert(i, New ListItem(i + 1, i))
                ddlPage2.Items.Insert(i, New ListItem(i + 1, i))
            Next
            If (.CurrentPageIndex - 1) >= ddlPage.Items.Count Then .CurrentPageIndex = ddlPage.Items.Count
            ddlPage.Items(.CurrentPageIndex - 1).Selected = True
            ddlPage2.Items(.CurrentPageIndex - 1).Selected = True

            lblPagTot.Text = .PageCount
            lblPagTot2.Text = .PageCount

            If .PageCount = 1 Then
                btnFirstPage.Enabled = False
                'btnFirstPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero_desactivado.gif")
                btnFirstPage.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/primero_desactivado.gif"
                btnPreviousPage.Enabled = False
                btnPreviousPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior_desactivado.gif")
                btnFirstPage.Enabled = True
                btnFirstPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
                btnPreviousPage.Enabled = True
                btnPreviousPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")

                btnFirstPage2.Enabled = False
                'btnFirstPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero_desactivado.gif")
                btnFirstPage2.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/primero_desactivado.gif"
                btnPreviousPage2.Enabled = False
                btnPreviousPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior_desactivado.gif")
                btnFirstPage2.Enabled = True
                btnFirstPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
                btnPreviousPage2.Enabled = True
                btnPreviousPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")

                btnFirstPage.Visible = False
                btnPreviousPage.Visible = False
                lblPagina.Visible = False
                ddlPage.Visible = False
                lblDe.Visible = False
                lblPagTot.Visible = False
                btnNextPage.Visible = False
                btnLastPage.Visible = False

                btnFirstPage2.Visible = False
                btnPreviousPage2.Visible = False
                lblPagina2.Visible = False
                ddlPage2.Visible = False
                lblDe2.Visible = False
                lblPagTot2.Visible = False
                btnNextPage2.Visible = False
                btnLastPage2.Visible = False
            Else
                btnFirstPage.Visible = True
                btnPreviousPage.Visible = True
                lblPagina.Visible = True
                ddlPage.Visible = True
                lblDe.Visible = True
                lblPagTot.Visible = True
                btnNextPage.Visible = True
                btnLastPage.Visible = True

                btnFirstPage2.Visible = True
                btnPreviousPage2.Visible = True
                lblPagina2.Visible = True
                ddlPage2.Visible = True
                lblDe2.Visible = True
                lblPagTot2.Visible = True
                btnNextPage2.Visible = True
                btnLastPage2.Visible = True

                Select Case .CurrentPageIndex
                    Case 1
                        btnFirstPage.Enabled = False
                        'btnFirstPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero_desactivado.gif")
                        btnFirstPage.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/primero_desactivado.gif"
                        btnPreviousPage.Enabled = False
                        btnPreviousPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior_desactivado.gif")
                        btnNextPage.Enabled = True
                        btnNextPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente.gif")
                        btnLastPage.Enabled = True
                        btnLastPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo.gif")

                        btnFirstPage2.Enabled = False
                        'btnFirstPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero_desactivado.gif")
                        btnFirstPage2.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/primero_desactivado.gif"
                        btnPreviousPage2.Enabled = False
                        btnPreviousPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior_desactivado.gif")
                        btnNextPage2.Enabled = True
                        btnNextPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente.gif")
                        btnLastPage2.Enabled = True
                        btnLastPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo.gif")

                    Case .PageCount
                        btnFirstPage.Enabled = True
                        'btnFirstPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
                        btnFirstPage.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/primero.gif"
                        btnPreviousPage.Enabled = True
                        btnPreviousPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")
                        btnNextPage.Enabled = False
                        btnNextPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente_desactivado.gif")
                        btnLastPage.Enabled = False
                        btnLastPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo_desactivado.gif")

                        btnFirstPage2.Enabled = True
                        'btnFirstPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
                        btnFirstPage2.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/primero.gif"
                        btnPreviousPage2.Enabled = True
                        btnPreviousPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")
                        btnNextPage2.Enabled = False
                        btnNextPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente_desactivado.gif")
                        btnLastPage2.Enabled = False
                        btnLastPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo_desactivado.gif")

                    Case Else
                        btnFirstPage.Enabled = True
                        'btnFirstPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
                        btnFirstPage.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/primero.gif"
                        btnPreviousPage.Enabled = True
                        btnPreviousPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")
                        btnNextPage.Enabled = True
                        btnNextPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente.gif")
                        btnLastPage.Enabled = True
                        btnLastPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo.gif")

                        btnFirstPage2.Enabled = True
                        'btnFirstPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
                        btnFirstPage2.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/primero.gif"
                        btnPreviousPage2.Enabled = True
                        btnPreviousPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")
                        btnNextPage2.Enabled = True
                        btnNextPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente.gif")
                        btnLastPage2.Enabled = True
                        btnLastPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo.gif")
                End Select
            End If
        End With

        'End If
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de primera página
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de primera página
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnFirstPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFirstPage.Click, btnFirstPage2.Click

        uwgPedidos.DisplayLayout.Pager.CurrentPageIndex = 1
        uwgPedidos.DataSource = Cache("Pedidos")
        uwgPedidos.DataBind()
        InitPaginador()
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de página anterior
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de página anterior
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnPreviousPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPreviousPage.Click, btnPreviousPage2.Click

        uwgPedidos.DisplayLayout.Pager.CurrentPageIndex = CInt(ddlPage.SelectedItem.Text) - 1
        uwgPedidos.DataSource = Cache("Pedidos")
        uwgPedidos.DataBind()
        InitPaginador()
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de página siguiente
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de página siguiente
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnNextPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNextPage.Click, btnNextPage2.Click

        uwgPedidos.DisplayLayout.Pager.CurrentPageIndex = CInt(ddlPage.SelectedItem.Text) + 1
        uwgPedidos.DataSource = Cache("Pedidos")
        uwgPedidos.DataBind()
        InitPaginador()
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de última página
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de última página
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnLastPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLastPage.Click, btnLastPage2.Click

        uwgPedidos.DisplayLayout.Pager.CurrentPageIndex = uwgPedidos.DisplayLayout.Pager.PageCount
        uwgPedidos.DataSource = Cache("Pedidos")
        uwgPedidos.DataBind()
        InitPaginador()
    End Sub

    ''' <summary>
    ''' Evento que se lanza al seleccionar alguna página de la combo de paginación
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: La combo de paginación
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub ddlPage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPage.SelectedIndexChanged, ddlPage2.SelectedIndexChanged

        uwgPedidos.DisplayLayout.Pager.CurrentPageIndex = CInt(ddlPage.SelectedItem.Text)
        uwgPedidos.DataSource = Cache("Pedidos")
        uwgPedidos.DataBind()
        InitPaginador()
    End Sub
#End Region


    ''' <summary>
    ''' Botón que exportará el contenido de la grid a excel
    ''' </summary>
    ''' <param name="sender">el propio botón</param>
    ''' <param name="e">Argumentos de evento</param>
    ''' <remarks>Tiempo máximo 0 sec</remarks>
    Protected Sub imgbtnExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnExcel.Click

        uwgPedidos.DisplayLayout.Pager.AllowPaging = False
        uwgPedidos.DataSource = Cache("Pedidos")
        uwgPedidos.DataBind()

        UltraWebGridExcelExporter1.DownloadName = "Pedidos.xls"
        UltraWebGridExcelExporter1.Export(uwgPedidos)

        uwgPedidos.DisplayLayout.Pager.AllowPaging = True
        uwgPedidos.DataSource = Cache("Pedidos")
        uwgPedidos.DataBind()

    End Sub

    ''' <summary>
    ''' Optiene la referencia cultural del idioma especificado
    ''' </summary>
    ''' <param name="Idioma">Variable de tipo Idioma</param>
    ''' <returns>Referencia cultural del idioma</returns>
    Public Function ReferenciaCultural(ByVal Idioma As String) As String
        Select Case Idioma
            Case "SPA"
                Return "es-ES_tradnl"
            Case "ENG"
                Return "en-US"
            Case "GER"
                Return "de-DE"
            Case Else
                Return Idioma
        End Select
    End Function
End Class

