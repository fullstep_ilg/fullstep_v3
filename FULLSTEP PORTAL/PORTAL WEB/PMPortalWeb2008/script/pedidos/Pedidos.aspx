﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Pedidos.aspx.vb" Inherits="PMPortalWeb2008.Pedidos" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
	<link href="../../../common/estilo.asp" type="text/css" rel="stylesheet"/>
	<script src="../../../common/formatos.js"></script>
	<script src="../../../common/menu.asp"></script>
	<script language="JavaScript1.2" src="../../../js/hiermenus.js"></script>
    <link href="../styles/Custom.css" rel="stylesheet" type="text/css" />
    <link href="../styles/estilos.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<script type="text/javascript">dibujaMenu(2)</script>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>   
                 
        <table width="100%">
            <tr>
                <td colspan="6">
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <asp:Table ID="tblCabecera" runat="server" Width="100%" 
                        BackImageUrl="~/App_Themes/Ono/images/cabeceraFondo.jpg" BorderWidth="0px" 
                        CellPadding="0" CellSpacing="0">                    
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Image ID="imgTitulo" runat="server" ImageUrl="~/App_Themes/Ono/images/noconformidades.gif"  />                            
                            </asp:TableCell>
	                        <asp:TableCell Width="100%" VerticalAlign="Bottom">
                                &nbsp;&nbsp;<asp:Label ID="lblTitulo" runat="server" CssClass="LabelTituloCabeceraCustomControl"></asp:Label>
	                        </asp:TableCell> 
	                        <asp:TableCell Width="6px" VerticalAlign="Bottom">
	                            <asp:Image ID="imgBordeRedondeadoCabecera" runat="server" 
                                    ImageUrl="~/App_Themes/Ono/images/CabeceraFondoRedondo.jpg"  BorderWidth="0px" />	                        
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblDesde" runat="server" Text="DFecha Desde:" CssClass="Etiqueta"></asp:Label>
                </td>
                <td>
                    <igsch:webdatechooser ID="wdcDesde" runat="server" Value="" NullDateLabel="" Width="110px" Editable="true" SkinID="Calendario">
                        <DropButton ImageUrl1="images/calendar-color.png"></DropButton>
                    </igsch:webdatechooser>
                </td>
                <td align="center">
                    <asp:Label ID="lblHasta" runat="server" Text="DFecha Hasta:" CssClass="Etiqueta"></asp:Label>                           
                </td>                        
                <td>
                    <igsch:webdatechooser ID="wdcHasta" runat="server" Value="" NullDateLabel="" Width="110px" Editable="true" SkinID="Calendario">
                        <DropButton ImageUrl1="images/calendar-color.png"></DropButton>
                    </igsch:webdatechooser>
                </td>
                <td>
                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="Boton" />
                </td>
                <td width="40%"  >
                </td>
            </tr>
            <tr><td colspan="6"><hr size="5px" class="LineaSeparatoria" /></td></tr>
            <tr><td colspan="6"><br /></td></tr>
            </table>
            
            
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>                    
                    <table style="position:absolute; width:100%; height:400px; z-index:10000; ">
                    <tr>
                        <td align="center">                            
                            <table style="border-style: solid; border-width: 1px; z-index:10001; background-color:White;">
                                <tr>
                                    <td align="center">
                                        <asp:Image ID="ImgProgress" runat="server" ImageUrl="../../App_Themes/Ono/images/cargando.gif" style="vertical-align:middle" />
                                        <asp:Label ID="LblProcesando" runat="server" ForeColor="Black" Text="Cargando ..."></asp:Label>	
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </table>
                </ProgressTemplate>
            </asp:UpdateProgress>
        
            <asp:UpdatePanel ID="pnlGrid" runat="server" >
            <Triggers>
                <asp:PostBackTrigger ControlID="imgbtnExcel" />               
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />                   
            </Triggers>
                <ContentTemplate>                                                     
            <table width="100%">
            <tr>
                <td width="33%">
                    <asp:Label ID="lblFiltrarPor" runat="server" Text="DTipo de Filtro:" CssClass="Etiqueta"></asp:Label>
                    <asp:RadioButton ID="optFiltroSeleccion" GroupName="optFiltrarPor"  
                        runat="server" AutoPostBack="true"  />
                    <asp:ImageButton ID="imgFiltrar1" runat="server" />        
                    <asp:RadioButton ID="optFiltrar2" GroupName="optFiltrarPor" runat ="server" AutoPostBack="true" />
                    <asp:ImageButton ID="imgFiltrar2" runat="server" />
                </td>
                <td align="center" width="34%">
                    <table cellpadding="2">
                    <tr>                       
                        <td><asp:ImageButton runat="server" ID="btnFirstPage" /></td>
                        <td><asp:ImageButton runat="server" ID="btnPreviousPage" /></td>
                        <td><asp:Label ID="lblPagina" runat="server" Text="Página"></asp:Label></td>
                        <td><asp:DropDownList ID="ddlPage" runat="server" CausesValidation="False" AutoPostBack="True"></asp:DropDownList></td>
                        <td>
                            <asp:Label ID="lblDe" runat="server" Text="de"></asp:Label>&nbsp;
                            <asp:Label ID="lblPagTot" runat="server" Text="1"></asp:Label>
                        </td>
                        <td><asp:ImageButton runat="server" ID="btnNextPage" /></td>
                        <td><asp:ImageButton runat="server" ID="btnLastPage" /></td>
                    </tr>           
                    </table>
                </td>
                <td align="right" width="33%">
                    <asp:ImageButton ID="imgbtnExcel" runat="server" />
                </td>
            </tr>                   
            <tr>                
                <td colspan="3">  
                    <igtbl:UltraWebGrid ID="uwgPedidos" runat="server" SkinID="GridFiltros" 
                        Height="200px" Width="325px">
                        <Bands> 
                            <igtbl:UltraGridBand ChildBandColumn="POSIC">                        
                                <Columns>
                                    <igtbl:UltraGridColumn BaseColumnName="Ebeln" IsBound="True" Key="Ebeln" Width="10%">
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Aedat" IsBound="True" Key="Aedat" Width="10%"  >
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="1" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="1" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Kbetr" IsBound="True" Key="Kbetr" DataType="System.Decimal"  Width="10%">
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="2" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="2" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Solic" IsBound="True" Key="Solic"  Width="15%">
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="3" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="3" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Ekgrp" IsBound="True" Key="Ekgrp" Width="15%">
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="4" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="4" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Zterm" IsBound="True" Key="Zterm" Width="15%">
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="5" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="5" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>                                        
                                    <igtbl:UltraGridColumn BaseColumnName="Observ" IsBound="True" Key="Observ" Width="20%">
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="6" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="6" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>        
                                    <igtbl:UltraGridColumn BaseColumnName="EstadoLec" IsBound="True" 
                                        Key="EstadoLec" Hidden="True">
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="7" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="7" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:TemplatedColumn AllowGroupBy="No" AllowRowFiltering="False" CellButtonDisplay="Always" Key="PDF" Width="5%">
                                        <CellTemplate>
                                            <asp:ImageButton ID="imgbtnPDF" runat="server" ImageAlign="Middle" CommandName="DescargaPDF" CommandArgument="<%# Container.Cell.Row.Cells(0).Value %>"  />
                                        </CellTemplate>
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="8" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="8" />
                                        </Footer>
                                    </igtbl:TemplatedColumn>
                                </Columns>
                                <AddNewRow Visible="NotSet" View="NotSet"></AddNewRow>
                            </igtbl:UltraGridBand>
                            <igtbl:UltraGridBand >                        
                                <Columns>
                                    <igtbl:UltraGridColumn BaseColumnName="Ebelp" IsBound="True" Key="Ebelp" Width="5%">
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Matnr" IsBound="True" Key="Matnr" Width="10%">
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="1" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="1" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Txz01" IsBound="True" Key="Txz01" Width="10%">
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="2" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="2" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="DescLarga" IsBound="True" Key="DescLarga" Width="15%">
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="3" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="3" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Idnlf" IsBound="True" Key="Idnlf" Width="10%">
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="4" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="4" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Menge" IsBound="True" Key="Menge" Width="8%">
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="5" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="5" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>                                        
                                    <igtbl:UltraGridColumn BaseColumnName="Netpr" IsBound="True" Key="Netpr" DataType="System.Decimal" Width="8%">
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="6" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="6" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Kbetr" IsBound="True" Key="Kbetr" DataType="System.Decimal" Width="9%">
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="7" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="7" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Eindt" IsBound="True" Key="Eindt" Width="10%">
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="8" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="8" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="City1" IsBound="True" Key="City1" Width="15%">
                                        <Header>
                                            <RowLayoutColumnInfo OriginX="9" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="9" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>                                        
                                </Columns>
                                <AddNewRow Visible="NotSet" View="NotSet"></AddNewRow>
                            </igtbl:UltraGridBand>
                        </Bands>
                    </igtbl:UltraWebGrid>                    
                    <igtxt:WebDateTimeEdit ID="wdtAedat" runat="server"></igtxt:WebDateTimeEdit>
                    <igtxt:WebDateTimeEdit ID="wdtEindt" runat="server"></igtxt:WebDateTimeEdit>                    
                </td>
            </tr>
            <tr>
                <td></td>
                <td align="center">
                    <table cellpadding="2">
                        <tr>
                            <td><asp:ImageButton runat="server" ID="btnFirstPage2" /></td>
                            <td><asp:ImageButton runat="server" ID="btnPreviousPage2" /></td>
                            <td><asp:Label ID="lblPagina2" runat="server" Text="Página"></asp:Label></td>
                            <td><asp:DropDownList ID="ddlPage2" runat="server" CausesValidation="False" AutoPostBack="True"></asp:DropDownList></td>
                            <td>
                                <asp:Label ID="lblDe2" runat="server" Text="de"></asp:Label>&nbsp;
                                <asp:Label ID="lblPagTot2" runat="server" Text="1"></asp:Label>
                            </td>
                            <td><asp:ImageButton runat="server" ID="btnNextPage2" /></td>
                            <td><asp:ImageButton runat="server" ID="btnLastPage2" /></td>
                        </tr>           
                    </table> 
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Label ID="lblNoDatos" runat="server" Text="DNo hay datos para esa selección" Visible="False"></asp:Label>
                </td>
            </tr>
        </table>   
        
        <igtblexp:ultrawebgridexcelexporter ID="UltraWebGridExcelExporter1" 
            runat='server'>
        </igtblexp:ultrawebgridexcelexporter>
        
        </ContentTemplate>
        </asp:UpdatePanel>
       
    </form>
</body>
</html>
