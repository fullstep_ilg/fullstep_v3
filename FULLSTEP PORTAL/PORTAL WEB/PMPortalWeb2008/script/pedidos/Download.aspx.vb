﻿Imports System.Threading

Partial Class Download
    Inherits System.Web.UI.Page
    Private FSWSServer As Fullstep.PMPortalServer.Root
    Private oUser As Fullstep.PMPortalServer.User
    Private oTextos As DataTable

    ''' <summary>
    ''' Esta página va a buscar el pdf del pedido o aceptacion que le hayamos pasado. 
    ''' </summary>
    ''' <param name="sender">la pagina</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Tiempo máximo variable en función de cuanto tarde en generarse el pdf</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strRequest As String = Request.QueryString("file") '-- if something was passed to the file querystring  
        Dim path As String = strRequest
        Dim file As System.IO.FileInfo = New System.IO.FileInfo(path)
        Dim iNumIntentosPDF = CInt(ConfigurationManager.AppSettings("NumIntentosPDF"))
        Dim iIntentos As Integer = 0

        FSWSServer = Session("FS_PM_Server")
        oUser = Session("FS_PM_User")

        Dim sIdi As String = oUser.Idioma
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If
        Dim oDict As Fullstep.PMPortalServer.Dictionary = FSWSServer.Get_Dictionary()
        oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.VisorPedidosAceptaciones, sIdi)
        oTextos = oDict.Data.Tables(0)

        While iIntentos < iNumIntentosPDF
            If file.Exists Then
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
                Response.AddHeader("Content-Length", file.Length.ToString())
                Response.ContentType = "application/octet-stream"
                Response.WriteFile(file.FullName)
                Response.End()
                Exit While
            Else
                'Si el fichero no existe, esperamos y reintentamos
                Thread.Sleep(TimeSpan.FromSeconds(CDbl(ConfigurationManager.AppSettings("SegundosEntreIntentos"))))
                iIntentos = iIntentos + 1
            End If
        End While
        If iIntentos = iNumIntentosPDF Then
            Response.Write(oTextos.Rows(61).Item(1)) '"No se encuentra el fichero solicitado"
        End If

    End Sub
End Class
