﻿Imports Infragistics.WebUI
Imports System.Net
Imports System.IO
Imports System.Threading



Partial Public Class Aceptaciones
    Inherits System.Web.UI.Page

    Private FSWSServer As Fullstep.PMPortalServer.Root
    Private oUser As Fullstep.PMPortalServer.User
    Private oTextos As DataTable

    Public gsUserWS As String = ""
    Public gsPwdWS As String = ""

    ''' <summary>
    ''' Carga inicial de la página
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Tiempo máximo 1 sec</remarks>
    Private Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim FSPMServer As New Fullstep.PMPortalServer.Root
        Dim CiaCode As String
        Dim Username As String
        Dim Password As String
        Dim oUser As Fullstep.PMPortalServer.User


        Dim lCiaComp As Long = Request.Cookies("USU")("CIACOMP")

        ' Decode the encoded string.
        CiaCode = HttpUtility.UrlDecode(Request.Cookies("USU")("CIAFSPM"))


        Username = Fullstep.PMPortalServer.Encrypter.Encrypt(Request.Cookies("USU")("COD"), , False, Fullstep.PMPortalServer.Encrypter.TipoDeUsuario.Persona, , True)

        Password = Fullstep.PMPortalServer.Encrypter.Encrypt(Request.Cookies("USU")("PWD"), , False, Fullstep.PMPortalServer.Encrypter.TipoDeUsuario.Persona, , True)

        If Not Session("FS_PM_User") Is Nothing Then
            oUser = Session("FS_PM_User")

            If oUser.Cod <> Username Then
                oUser = FSPMServer.Login(CiaCode, Username, Password, False)
                If oUser Is Nothing Then
                    Response.Redirect(ConfigurationManager.AppSettings("rutanormal"))
                Else
                    oUser.LoadUserData(Username)
                    FSPMServer.Load_LongitudesDeCodigos(lCiaComp)
                    Session("FS_PM_User") = oUser
                    Session("FS_PM_Server") = FSPMServer
                    HttpContext.Current.User = Session("FS_PM_User")
                    Exit Sub
                End If
            End If

            If Not Request.Cookies("USU")("Idioma") Is Nothing Then
                If oUser.Idioma <> Request.Cookies("USU")("Idioma") Then
                    oUser = FSPMServer.Login(CiaCode, Username, Password, False)
                    If oUser Is Nothing Then
                        Response.Redirect(ConfigurationManager.AppSettings("rutanormal"))


                    Else
                        oUser.LoadUserData(Username)
                        FSPMServer.Load_LongitudesDeCodigos(lCiaComp)
                        Session("FS_PM_User") = oUser
                        Session("FS_PM_Server") = FSPMServer
                        HttpContext.Current.User = Session("FS_PM_User")
                        Exit Sub
                    End If

                End If
            End If


        Else
            oUser = FSPMServer.Login(CiaCode, Username, Password, False)
            If oUser Is Nothing Then
                Response.Redirect(ConfigurationManager.AppSettings("rutanormal"))


            Else
                oUser.LoadUserData(Username)
                FSPMServer.Load_LongitudesDeCodigos(lCiaComp)
                Session("FS_PM_User") = oUser
                Session("FS_PM_Server") = FSPMServer
                HttpContext.Current.User = Session("FS_PM_User")
                Exit Sub
            End If

        End If

    End Sub

    ''' <summary>
    ''' Carga inicial de la página
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Tiempo máximo 1 sec</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        CargarParametrosWebService()

        FSWSServer = Session("FS_PM_Server")
        oUser = Session("FS_PM_User")

        Dim sIdi As String = oUser.Idioma
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If
        Dim oDict As Fullstep.PMPortalServer.Dictionary = FSWSServer.Get_Dictionary()
        oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.VisorPedidosAceptaciones, sIdi)
        oTextos = oDict.Data.Tables(0)

        uwgAceptaciones.DisplayLayout.Pager.PageSize = CInt(ConfigurationManager.AppSettings("PageSize"))

        If Not IsPostBack Then
            Dim ci As New Globalization.CultureInfo(ReferenciaCultural(oUser.Idioma))
            ci.DateTimeFormat = oUser.DateFormat
            wdcDesde.CalendarLayout.Culture = ci
            wdcHasta.CalendarLayout.Culture = ci

            wdcDesde.Value = DateAdd(DateInterval.Month, -6, Now())
            wdcHasta.Value = Now
            optFiltroSeleccion.Checked = True

            lblTitulo.Text = oTextos.Rows(52).Item(1)
            lblDesde.Text = oTextos.Rows(46).Item(1) & ":"
            lblHasta.Text = oTextos.Rows(47).Item(1) & ":"
            lblFiltrarPor.Text = oTextos.Rows(48).Item(1) & ":"
            btnBuscar.Text = oTextos.Rows(51).Item(1)
            lblNoDatos.Text = oTextos.Rows(62).Item(1) 'No hay datos para los parámetros seleccionados

            Dim colAceptaciones As IEnumerable(Of WSAcept.ZMPVP_ACEPTACIONES) = Nothing
            colAceptaciones = ObtenerAceptaciones()
            If colAceptaciones IsNot Nothing Then
                uwgAceptaciones.DataSource = colAceptaciones
                uwgAceptaciones.DataBind()
                InitPaginador()
            End If
        End If


        uwgAceptaciones.DisplayLayout.FilterOptionsDefault.FilterUIType = IIf(optFiltroSeleccion.Checked = True, Infragistics.WebUI.UltraWebGrid.FilterUIType.HeaderIcons, Infragistics.WebUI.UltraWebGrid.FilterUIType.FilterRow)

        imgTitulo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/Aceptaciones.gif"
        imgFiltrar1.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/filtro_cabecera.gif"
        imgFiltrar2.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/filtro_columna.gif"
        imgbtnExcel.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/excel.gif"

    End Sub

    ''' <summary>
    ''' En este procedimiento cargaremos los parámetros que vamos a utilizar en las llamadas a los WebServices
    ''' </summary>
    ''' <remarks>Llamada desde Page_Load
    ''' Tiempo máximo 0 sec</remarks>
    Private Sub CargarParametrosWebService()
        'Leemos los parámetros de conexion al WebService del fichero .ini
        Dim fs = New FileStream(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory(), "ConexionWSOno.ini"), FileMode.Open, FileAccess.Read)
        Dim d As New StreamReader(fs)
        d.BaseStream.Seek(0, SeekOrigin.Begin)
        If d.Peek() > -1 Then
            gsUserWS = d.ReadLine()

            If InStr(1, gsUserWS, "USER=") Then
                gsUserWS = Strings.Right(gsUserWS, Len(gsUserWS) - 5)
            End If
        End If
        If d.Peek() > -1 Then
            gsPwdWS = d.ReadLine()

            If InStr(1, gsPwdWS, "PWD=") Then
                gsPwdWS = Strings.Right(gsPwdWS, Len(gsPwdWS) - 4)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Función que devuelve una colección de Aceptaciones tras llamar al webservice
    ''' </summary>
    ''' <returns>Una colección de Aceptaciones</returns>
    ''' <remarks>Llamada desde: Page_Load y el botón Buscar
    ''' Tiempo máximo: 1 sec.</remarks>
    Private Function ObtenerAceptaciones() As IEnumerable(Of WSAcept.ZMPVP_ACEPTACIONES)
        Dim WSAceptaciones As New WSAcept.ZMPVP_WS_RECUP_HIST_ACEP_MOD
        Dim oAceptaciones As New WSAcept.ZmpvpRecupHistorialAceptResponse

        Dim sUserWS As String = ""
        Dim sPwdWS As String = ""

        'Pasamos el login de acceso al webservice
        Dim oCredentials As New NetworkCredential(gsUserWS, gsPwdWS)
        WSAceptaciones.Credentials = oCredentials

        'Llamamos al webservice
        Dim o As New WSAcept.ZmpvpRecupHistorialAcept
        o.EJERCICIO = CStr(Year(CDate(wdcHasta.Value)))
        o.F_FIN = Format(CDate(wdcHasta.Value), "ddMMyyyy")
        o.F_INI = Format(CDate(wdcDesde.Value), "ddMMyyyy")
        o.STCD1 = oUser.NIFCia

        Try
            oAceptaciones = WSAceptaciones.ZmpvpRecupHistorialAcept(o)

            Dim SesionTimeOut As TimeSpan = New TimeSpan(0, 0, Session.Timeout, 0, 0)
            Cache.Insert("Aceptaciones", oAceptaciones.TiAceptaciones, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, SesionTimeOut)

            Return oAceptaciones.TiAceptaciones
        Catch
            OcultarGrid()
            Return Nothing
        End Try

    End Function

    ''' <summary>
    ''' Evento que salta al cargarse la grid y en el controlaremos si tiene datos o no, para mostrarla u ocultarla
    ''' </summary>
    ''' <param name="sender">la grid de Aceptaciones</param>
    ''' <param name="e">Argumentos de evento</param>
    ''' <remarks>Tiempo máximo: 0 sec</remarks>
    Private Sub uwgAceptaciones_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles uwgAceptaciones.DataBound
        Dim oAceptaciones As IEnumerable(Of WSAcept.ZMPVP_ACEPTACIONES) = Cache("Aceptaciones")

        If oAceptaciones IsNot Nothing Then
            If oAceptaciones.Count = 0 OrElse (oAceptaciones.Count = 1 AndAlso oAceptaciones(0).BLDAT.ToString = "0000-00-00") Then
                OcultarGrid()
            Else
                MostrarGrid()
            End If
        Else
            OcultarGrid()
        End If
    End Sub

    ''' <summary>
    ''' Procedimiento en el que ocultaremos la grid y sus controles relacionados
    ''' </summary>
    ''' <remarks>Llamada desde uwgAceptaciones_DataBound
    ''' Tiempo máximo 0 sec</remarks>
    Private Sub OcultarGrid()
        lblFiltrarPor.Visible = False
        optFiltroSeleccion.Visible = False
        imgFiltrar1.Visible = False
        optFiltrar2.Visible = False
        imgFiltrar2.Visible = False
        imgbtnExcel.Visible = False
        uwgAceptaciones.Visible = False
        lblNoDatos.Visible = True
        btnFirstPage.Visible = False
        btnPreviousPage.Visible = False
        lblPagina.Visible = False
        ddlPage.Visible = False
        lblDe.Visible = False
        lblPagTot.Visible = False
        btnNextPage.Visible = False
        btnLastPage.Visible = False
    End Sub

    ''' <summary>
    ''' Procedimiento en el que mostraremos la grid y sus controles relacionados
    ''' </summary>
    ''' <remarks>Llamada desde uwgAceptaciones_DataBound
    ''' Tiempo máximo 0 sec</remarks>
    Private Sub MostrarGrid()
        lblFiltrarPor.Visible = True
        optFiltroSeleccion.Visible = True
        imgFiltrar1.Visible = True
        optFiltrar2.Visible = True
        imgFiltrar2.Visible = True
        imgbtnExcel.Visible = True
        uwgAceptaciones.Visible = True
        lblNoDatos.Visible = False
        btnFirstPage.Visible = True
        btnPreviousPage.Visible = True
        lblPagina.Visible = True
        ddlPage.Visible = True
        lblDe.Visible = True
        lblPagTot.Visible = True
        btnNextPage.Visible = True
        btnLastPage.Visible = True
    End Sub

    ''' <summary>
    ''' Evento que salta tras aplicar un filtro en la grid, y en el que nos iremos a la primera página de la misma
    ''' </summary>
    ''' <param name="sender">La propia grid</param>
    ''' <param name="e">Argumentos de evento</param>
    ''' <remarks>Tiempo máximo 0 sec</remarks>
    Private Sub uwgAceptaciones_RowFilterApplied(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.FilterEventArgs) Handles uwgAceptaciones.RowFilterApplied
        uwgAceptaciones.DisplayLayout.Pager.CurrentPageIndex = 1
        uwgAceptaciones.DataSource = Cache("Aceptaciones")
        uwgAceptaciones.DataBind()
        InitPaginador()
    End Sub


    ''' <summary>
    ''' Evento que salta al seleccionar un filtro.
    ''' Si se trata del filtro de "=" y se trata de una fecha, lo transforma a la fecha que utiliza para la comparacion
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Tiempo máximo=0seg.</remarks>
    Private Sub uwgAceptaciones_RowFilterApplying(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.FilterEventArgs) Handles uwgAceptaciones.RowFilterApplying

        If e.ActiveColumnFilter.Column.Band.Index = 1 Then
            uwgAceptaciones.DisplayLayout.FilterOptionsDefault.AllowRowFiltering = UltraWebGrid.RowFiltering.OnClient
        Else
            uwgAceptaciones.DisplayLayout.FilterOptionsDefault.AllowRowFiltering = UltraWebGrid.RowFiltering.OnServer
        End If



        If Not optFiltroSeleccion.Checked AndAlso IsDate(e.AppliedFilterCondition.CompareValue) Then
            Dim dayCompareValue As Integer = Day(e.AppliedFilterCondition.CompareValue)
            Dim monthCompareValue As Integer = Month(e.AppliedFilterCondition.CompareValue)
            Dim yearCompareValue As Integer = Year(e.AppliedFilterCondition.CompareValue)

            If dayCompareValue <= 12 Then
                e.AppliedFilterCondition.CompareValue = New Date(yearCompareValue, dayCompareValue, monthCompareValue)
            End If
        ElseIf Not IsNumeric(e.AppliedFilterCondition.CompareValue) Then
            If UCase(e.AppliedFilterCondition.CompareValue) = "(MI_ALL)" Then
                e.Cancel = True
            End If
        End If


    End Sub


    Protected Sub uwgAceptaciones_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgAceptaciones.InitializeLayout
        Dim oAceptaciones As IEnumerable(Of WSAcept.ZMPVP_ACEPTACIONES) = Cache("Aceptaciones")
        If oAceptaciones.Count = 0 OrElse (oAceptaciones.Count = 1 AndAlso oAceptaciones(0).BLDAT.ToString = "0000-00-00") Then
            If oAceptaciones.Count = 1 Then
                If oAceptaciones(0).EBELN <> "" Then
                    lblNoDatos.Text = oAceptaciones(0).EBELN
                End If
            End If
        Else
            'Recorremos las columnas para ir metiendo los valores a los filtros para todas los registros (no sólo los que se muestran)
            For Each ugColumn As UltraWebGrid.UltraGridColumn In e.Layout.Grid.Bands(0).Columns
                GatherFilterDataForColumn(ugColumn)
            Next
        End If

        wdtBldat.DisplayModeFormat = oUser.DateFormat.ShortDatePattern
        wdtBldat.EditModeFormat = oUser.DateFormat.ShortDatePattern
        wdtBldat.DataMode = Infragistics.WebUI.WebDataInput.DateDataMode.Date

        wdtAedat.DisplayModeFormat = oUser.DateFormat.ShortDatePattern
        wdtAedat.EditModeFormat = oUser.DateFormat.ShortDatePattern
        wdtAedat.DataMode = Infragistics.WebUI.WebDataInput.DateDataMode.Date

        wdtFecha.DisplayModeFormat = oUser.DateFormat.ShortDatePattern
        wdtFecha.EditModeFormat = oUser.DateFormat.ShortDatePattern
        wdtFecha.DataMode = Infragistics.WebUI.WebDataInput.DateDataMode.Date

        wdtFechaER.DisplayModeFormat = oUser.DateFormat.ShortDatePattern
        wdtFechaER.EditModeFormat = oUser.DateFormat.ShortDatePattern
        wdtFechaER.DataMode = Infragistics.WebUI.WebDataInput.DateDataMode.Date

        'Ponemos los textos de la grid 
        With uwgAceptaciones.Bands(0).Columns
            .FromKey("EBELN").Header.Caption = oTextos.Rows(12).Item(1) ' "Nº pedido"
            .FromKey("EBELN").Header.Title = oTextos.Rows(1).Item(1) '"Nº pedido"

            .FromKey("Aedat").Header.Caption = oTextos.Rows(55).Item(1) '"Fecha pedido"
            .FromKey("Aedat").Header.Title = oTextos.Rows(55).Item(1) '"Fecha pedido"
            .FromKey("Aedat").DataType = "System.DateTime"
            .FromKey("Aedat").Format = oUser.DateFormat.ShortDatePattern
            .FromKey("Aedat").EditorControlID = wdtAedat.UniqueID
            .FromKey("Aedat").Type = Infragistics.WebUI.UltraWebGrid.ColumnType.Custom

            .FromKey("Kbetr").Header.Caption = oTextos.Rows(63).Item(1) '"Importe pedido"
            .FromKey("Kbetr").Header.Title = oTextos.Rows(63).Item(1) '"Importe pedido"
            .FromKey("Kbetr").Format = "###,###,###,###,##0." & Replace(Space(oUser.NumberFormat.NumberDecimalDigits), " ", "0")

            .FromKey("Bldat").Header.Caption = oTextos.Rows(54).Item(1) '"Fecha documento"
            .FromKey("Bldat").Header.Title = oTextos.Rows(54).Item(1) '"Fecha documento"
            .FromKey("Bldat").DataType = "System.DateTime"
            .FromKey("Bldat").Format = oUser.DateFormat.ShortDatePattern
            .FromKey("Bldat").EditorControlID = wdtBldat.UniqueID
            .FromKey("Bldat").Type = Infragistics.WebUI.UltraWebGrid.ColumnType.Custom

            .FromKey("WrbtrC").Header.Caption = oTextos.Rows(64).Item(1) '"Importe recepcionado"
            .FromKey("WrbtrC").Header.Title = oTextos.Rows(64).Item(1) '"Importe recepcionado"
            .FromKey("WrbtrC").Format = "###,###,###,###,##0." & Replace(Space(oUser.NumberFormat.NumberDecimalDigits), " ", "0")

        End With

        With uwgAceptaciones.Bands(1).Columns

            .FromKey("Belnr").Header.Caption = oTextos.Rows(53).Item(1) ' "Nº documento"
            .FromKey("Belnr").Header.Title = oTextos.Rows(53).Item(1) '"Nº documento"     

            .FromKey("Xblnr").Header.Caption = oTextos.Rows(65).Item(1) '"Nº Albarán"
            .FromKey("Xblnr").Header.Title = oTextos.Rows(65).Item(1) '"Nº Albarán"

            .FromKey("Ebelp").Header.Caption = oTextos.Rows(66).Item(1) ' "Línea de Pedido"
            .FromKey("Ebelp").Header.Title = oTextos.Rows(66).Item(1) '"Línea de Pedido"

            .FromKey("Matnr").Header.Caption = oTextos.Rows(44).Item(1) '"Material"
            .FromKey("Matnr").Header.Title = oTextos.Rows(44).Item(1) '"Material"

            .FromKey("Descr").Header.Caption = oTextos.Rows(14).Item(1) '  "Descrip. Corta"
            .FromKey("Descr").Header.Title = oTextos.Rows(17).Item(1) '"Descripción Corta"

            .FromKey("Menge").Header.Caption = oTextos.Rows(67).Item(1) '"Cantidad pedido"
            .FromKey("Menge").Header.Title = oTextos.Rows(67).Item(1) '"Cantidad pedido"
            .FromKey("Menge").Format = "###,###,###,###,##0." & Replace(Space(oUser.NumberFormat.NumberDecimalDigits), " ", "0")

            .FromKey("CantRecep").Header.Caption = oTextos.Rows(68).Item(1) '"Cantidad recepcionada"
            .FromKey("CantRecep").Header.Title = oTextos.Rows(68).Item(1) '"Cantidad recepcionada"
            .FromKey("CantRecep").Format = "###,###,###,###,##0." & Replace(Space(oUser.NumberFormat.NumberDecimalDigits), " ", "0")

            .FromKey("CantPend").Header.Caption = oTextos.Rows(69).Item(1) '"Cantidad pendiente"
            .FromKey("CantPend").Header.Title = oTextos.Rows(69).Item(1) '"Cantidad pendiente"
            .FromKey("CantPend").Format = "###,###,###,###,##0." & Replace(Space(oUser.NumberFormat.NumberDecimalDigits), " ", "0")

            .FromKey("Wrbtr").Header.Caption = oTextos.Rows(70).Item(1) '"Importe recepción"
            .FromKey("Wrbtr").Header.Title = oTextos.Rows(70).Item(1) '"Importe recepción"
            .FromKey("Wrbtr").Format = "###,###,###,###,##0." & Replace(Space(oUser.NumberFormat.NumberDecimalDigits), " ", "0")

            .FromKey("Fecha").Header.Caption = oTextos.Rows(71).Item(1) '"Fecha entrega pedido"
            .FromKey("Fecha").Header.Title = oTextos.Rows(71).Item(1) '"Fecha entrega pedido"
            .FromKey("Fecha").DataType = "System.DateTime"
            .FromKey("Fecha").Format = oUser.DateFormat.ShortDatePattern
            .FromKey("Fecha").EditorControlID = wdtFecha.UniqueID
            .FromKey("Fecha").Type = Infragistics.WebUI.UltraWebGrid.ColumnType.Custom

            .FromKey("FechaEr").Header.Caption = oTextos.Rows(72).Item(1) '"Fecha entrega real"
            .FromKey("FechaEr").Header.Title = oTextos.Rows(72).Item(1) '"Fecha entrega real"
            .FromKey("FechaEr").DataType = "System.DateTime"
            .FromKey("FechaEr").Format = oUser.DateFormat.ShortDatePattern
            .FromKey("FechaEr").EditorControlID = wdtFechaER.UniqueID
            .FromKey("FechaEr").Type = Infragistics.WebUI.UltraWebGrid.ColumnType.Custom

            .FromKey("Diferencia").Header.Caption = oTextos.Rows(73).Item(1) '"Diferencia fechas (d)"
            .FromKey("Diferencia").Header.Title = oTextos.Rows(73).Item(1) '"Diferencia fechas (d)"

            .FromKey("PDF").Header.Caption = oTextos.Rows(10).Item(1) '"PDF"
            .FromKey("PDF").Header.Title = oTextos.Rows(60).Item(1) '"Descarga Acta en PDF"

        End With

        uwgAceptaciones.DisplayLayout.GroupByBox.Prompt = "(" & oTextos.Rows(49).Item(1) & ")" '"desplace aquí una columna para agrupar por ese concepto"

        With uwgAceptaciones.DisplayLayout.FilterOptionsDefault
            .AllString = "(" & oTextos.Rows(25).Item(1) & ")" '(All)
            .ContainsString = oTextos.Rows(26).Item(1) 'Contains
            .DoesNotContainString = oTextos.Rows(27).Item(1) 'Does Not Contain
            .DoesNotEndWithString = oTextos.Rows(28).Item(1) 'Does Not End With
            .DoesNotStartWithString = oTextos.Rows(29).Item(1) 'Does Not Start With
            .EmptyString = oTextos.Rows(30).Item(1) '(Empty)
            .EndsWithString = oTextos.Rows(31).Item(1) 'Ends With
            .EqualsString = oTextos.Rows(32).Item(1) 'Equals
            .GreaterThanOrEqualsString = oTextos.Rows(33).Item(1) 'Greater Than Or Equals
            .GreaterThanString = oTextos.Rows(34).Item(1) 'Greater Than
            .LessThanOrEqualsString = oTextos.Rows(35).Item(1) 'Less Than Or Equals
            .LessThanString = oTextos.Rows(36).Item(1) 'Less Than
            .LikeString = oTextos.Rows(37).Item(1) 'Like
            .NonEmptyString = oTextos.Rows(38).Item(1) '(Non Empty)
            .NotEqualsString = oTextos.Rows(39).Item(1) 'Not Equals
            .NotLikeString = oTextos.Rows(40).Item(1) 'Not Like
            .StartsWithString = oTextos.Rows(41).Item(1) 'Stars With
        End With

    End Sub

    ''' <summary>
    ''' Procedimiento en el que vamos a reunir mediante Linq la información de todos los registros de una columna 
    ''' para mostrarlos en los filtros
    ''' </summary>
    ''' <param name="oColumn">Columna en cuestión</param>
    ''' <remarks>Llamada desde uwgAceptaciones_InitializeLayout
    ''' Tiempo máximo 0 sec</remarks>
    Private Sub GatherFilterDataForColumn(ByVal oColumn As UltraWebGrid.UltraGridColumn)
        Dim oAceptaciones As IEnumerable(Of WSAcept.ZMPVP_ACEPTACIONES) = Cache("Aceptaciones")

        Select Case oColumn.Key
            Case "EBELN"
                Dim aFilters = From oRow In oAceptaciones _
                Where oRow.Ebeln IsNot DBNull.Value _
                Select sFilter = _
                oRow.Ebeln.Replace("""", "\""") _
                Distinct Order By sFilter

                oColumn.GatherFilterData = [Shared].DefaultableBoolean.False
                oColumn.FilterCollectionValues.Clear()
                For Each sFilter In aFilters
                    oColumn.FilterCollectionValues.Add(sFilter, sFilter)
                Next
            Case "Aedat"
                'Where oRow.AEDAT IsNot DBNull.Value _
                Dim aFilters = From oRow In oAceptaciones _
                Select sFilter = _
                oRow.AEDAT _
                Distinct Order By sFilter

                oColumn.GatherFilterData = [Shared].DefaultableBoolean.False
                oColumn.FilterCollectionValues.Clear()
                For Each sFilter In aFilters
                    oColumn.FilterCollectionValues.Add(sFilter, Format(CDate(sFilter), oUser.DateFormat.ShortDatePattern))
                Next
            Case "Kbetr"
                Dim aFilters = From oRow In oAceptaciones _
                Select sFilter = _
                oRow.Kbetr _
                Distinct Order By sFilter

                oColumn.GatherFilterData = [Shared].DefaultableBoolean.False
                oColumn.FilterCollectionValues.Clear()
                For Each sFilter In aFilters
                    oColumn.FilterCollectionValues.Add(CDbl(sFilter), sFilter.ToString("N", oUser.NumberFormat))
                Next
                'Case "Belnr"
                '    Dim aFilters = From oRow In oAceptaciones _
                '    Where oRow.Belnr IsNot DBNull.Value _
                '    Select sFilter = _
                '    oRow.Belnr.Replace("""", "\""") _
                '    Distinct Order By sFilter

                '    oColumn.GatherFilterData = [Shared].DefaultableBoolean.False
                '    oColumn.FilterCollectionValues.Clear()
                '    For Each sFilter In aFilters
                '        oColumn.FilterCollectionValues.Add(sFilter, sFilter)
                '    Next
            Case "Bldat"
                'Where oRow.BLDAT IsNot DBNull.Value _
                Dim aFilters = From oRow In oAceptaciones _
                Select sFilter = _
                oRow.BLDAT _
                Distinct Order By sFilter

                oColumn.GatherFilterData = [Shared].DefaultableBoolean.False
                oColumn.FilterCollectionValues.Clear()
                For Each sFilter In aFilters
                    oColumn.FilterCollectionValues.Add(sFilter, Format(CDate(sFilter), oUser.DateFormat.ShortDatePattern))
                Next
            Case "WrbtrC"
                Dim aFilters = From oRow In oAceptaciones _
                Select sFilter = _
                oRow.WRBTR_C _
                Distinct Order By sFilter

                oColumn.GatherFilterData = [Shared].DefaultableBoolean.False
                oColumn.FilterCollectionValues.Clear()
                For Each sFilter In aFilters
                    oColumn.FilterCollectionValues.Add(CDbl(sFilter), sFilter.ToString("N", oUser.NumberFormat))
                Next

                'Case "Xblnr"
                '    Dim aFilters = From oRow In oAceptaciones _
                '    Where oRow.Xblnr IsNot DBNull.Value _
                '    Select sFilter = _
                '    oRow.Xblnr.Replace("""", "\""") _
                '    Distinct Order By sFilter

                '    oColumn.GatherFilterData = [Shared].DefaultableBoolean.False
                '    oColumn.FilterCollectionValues.Clear()
                '    For Each sFilter In aFilters
                '        oColumn.FilterCollectionValues.Add(sFilter, sFilter)
                '    Next
        End Select
    End Sub

    Protected Sub uwgAceptaciones_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgAceptaciones.InitializeRow
        Select Case e.Row.Band.Index
            Case 1

                'ImageButton de PDF
                Dim tc As Infragistics.WebUI.UltraWebGrid.TemplatedColumn = CType(e.Row.Band.Columns.FromKey("PDF"), Infragistics.WebUI.UltraWebGrid.TemplatedColumn)
                Dim oCell As Infragistics.WebUI.UltraWebGrid.UltraGridCell = CType(e.Row.Cells.FromKey("PDF"), Infragistics.WebUI.UltraWebGrid.UltraGridCell)

                Dim imgBoton As ImageButton = CType(CType(tc.CellItems(tc.CellItems.Count - 1), Infragistics.WebUI.UltraWebGrid.CellItem).FindControl("imgbtnPDF"), ImageButton)

                imgBoton.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/rellenar_12x12.gif"

                ScriptManager1.RegisterPostBackControl(imgBoton)
        End Select

    End Sub

    ''' <summary>
    ''' Evento que salta al pinchar sobre el botón PDF de una aceptacion y que descargará en una nueva ventana su acta de aceptación en PDF
    ''' </summary>
    ''' <param name="sender">la grid</param>
    ''' <param name="e">argumentos de evento</param>
    ''' <remarks>Tiempo máximo 0 sec</remarks>
    Protected Sub uwgAceptaciones_ItemCommand(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.UltraWebGridCommandEventArgs) Handles uwgAceptaciones.ItemCommand

        If e.InnerCommandEventArgs.CommandName = "DescargaPDF" Then
            Dim WSImpAcept As New WSImpAcept.ZMPVP_WS_IMP_ACTAS

            'Pasamos el login de acceso al webservice
            Dim oCredentials As New NetworkCredential(gsUserWS, gsPwdWS)
            WSImpAcept.Credentials = oCredentials

            Dim oAceptaciones As IEnumerable(Of WSAcept.ZMPVP_ACEPTACIONES)
            oAceptaciones = CType(Cache("Aceptaciones"), IEnumerable(Of WSAcept.ZMPVP_ACEPTACIONES))

            Dim sNumPedido As String = CType(e.ParentControl, UltraWebGrid.CellItem).Cell.Row.ParentRow.Cells.FromKey("Ebeln").Value
            Dim sDocumento As String = CType(e.ParentControl, UltraWebGrid.CellItem).Cell.Row.Cells.FromKey("Belnr").Value
            Dim sEjercicio As String = Year(CDate(CType(e.ParentControl, UltraWebGrid.CellItem).Cell.Row.ParentRow.Cells.FromKey("Bldat").Value)).ToString


            'Se hace la llamada al webservice que provoca la descarga del pdf 
            Dim o As New WSImpAcept.ZMPVP_IMP_FORM_ACTA
            o.BELNR = sDocumento
            o.EBELN = sNumPedido
            o.GJAHR = sEjercicio
            Dim sResult As WSImpAcept.ZMPVP_IMP_FORM_ACTAResponse = WSImpAcept.ZMPVP_IMP_FORM_ACTA(o)
            If UCase(sResult.OK) <> "ERROR!!" Then

                'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "download", "window.open('Download.aspx?file=" & HttpContext.Current.Server.UrlEncode(ConfigurationManager.AppSettings("Ruta_PDFs") & sResult) & "', '', 'height=1,width=1,left=2000,top=2000,toolbar=no,menubar=no','true');window.focus();", True)
                'ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), "download", "window.open('Download.aspx?file=" & HttpContext.Current.Server.UrlEncode(ConfigurationManager.AppSettings("Ruta_PDFs") & sResult) & ".pdf', '', 'height=1,width=1,left=2000,top=2000,toolbar=no,menubar=no','true');window.focus();", True)

                Dim path As String = ConfigurationManager.AppSettings("Ruta_PDFs") & sResult.OK
                Dim file As System.IO.FileInfo = New System.IO.FileInfo(path)
                Dim iNumIntentosPDF = CInt(ConfigurationManager.AppSettings("NumIntentosPDF"))
                Dim iIntentos As Integer = 0

                While iIntentos < iNumIntentosPDF
                    If file.Exists Then
                        Response.Clear()
                        Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
                        Response.AddHeader("Content-Length", file.Length.ToString())
                        Response.ContentType = "application/octet-stream"
                        Response.WriteFile(file.FullName)
                        Response.End()
                        Exit While
                    Else
                        'Si el fichero no existe, esperamos y reintentamos
                        Thread.Sleep(TimeSpan.FromSeconds(CDbl(ConfigurationManager.AppSettings("SegundosEntreIntentos"))))
                        iIntentos = iIntentos + 1
                    End If
                End While
                If iIntentos = iNumIntentosPDF Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "NoFichero", "alert('" & oTextos.Rows(61).Item(1) & " (" & sResult.OK & ")');", True) '"No se encuentra el fichero solicitado (xxx.pdf)"                     
                End If

            Else
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "NoPDF", "alert('" & sDocumento & " " & oTextos.Rows(59).Item(1) & "');", True)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Evento click del botón de buscar, en el que llamaremos al webservice para refrescar los datos
    ''' </summary>
    ''' <param name="sender">botón buscar</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Tiempo máximo 1 sec</remarks>
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        uwgAceptaciones.DisplayLayout.Pager.CurrentPageIndex = 1

        Dim colAceptaciones As IEnumerable(Of WSAcept.ZMPVP_ACEPTACIONES) = Nothing
        colAceptaciones = ObtenerAceptaciones()
        If colAceptaciones IsNot Nothing Then
            uwgAceptaciones.DataSource = colAceptaciones
            uwgAceptaciones.DataBind()
            InitPaginador()
        End If
    End Sub

    ''' <summary>
    ''' Evento que salta al cambiar de página en la grid
    ''' </summary>
    ''' <param name="sender">La propia grid</param>
    ''' <param name="e">Argumentos de evento</param>
    ''' <remarks>Tiempo máximo 0 sec</remarks>
    Private Sub uwgAceptaciones_PageIndexChanged(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.PageEventArgs) Handles uwgAceptaciones.PageIndexChanged
        uwgAceptaciones.DataSource = Cache("Aceptaciones")
        uwgAceptaciones.DataBind()
    End Sub

    ''' <summary>
    ''' Evento que salta al ordenar una columna de la grid y en el que nos iremos a la primera página de la misma
    ''' </summary>
    ''' <param name="sender">La propia grid</param>
    ''' <param name="e">Argumentos de evento</param>
    ''' <remarks>Tiempo máximo 0 sec</remarks>
    Private Sub uwgAceptaciones_SortColumn(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.SortColumnEventArgs) Handles uwgAceptaciones.SortColumn

        If e.BandNo = 0 Then
            uwgAceptaciones.DisplayLayout.Pager.CurrentPageIndex = 1
            uwgAceptaciones.DataSource = Cache("Aceptaciones")
            uwgAceptaciones.DataBind()
            InitPaginador()
        End If

    End Sub

    ''' <summary>
    ''' Evento que salta al seleccionar la opción de filtro por selección y en el que recargaremos la grid para actualizar el tipo de filtrado
    ''' </summary>
    ''' <param name="sender">el optionbutton</param>
    ''' <param name="e">argumentos de evento</param>
    ''' <remarks>Tiempo máximo 0 sec</remarks>
    Private Sub optFiltroSeleccion_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optFiltroSeleccion.CheckedChanged
        'uwgAceptaciones.Rows.ColumnFilters.Clear()
        uwgAceptaciones.DataSource = Cache("Aceptaciones")
        uwgAceptaciones.DataBind()
        InitPaginador()
    End Sub


    ''' <summary>
    ''' Evento que salta al seleccionar la opción de filtro por fila y en el que recargaremos la grid para actualizar el tipo de filtrado
    ''' </summary>
    ''' <param name="sender">el optionbutton</param>
    ''' <param name="e">argumentos de evento</param>
    ''' <remarks>Tiempo máximo 0 sec</remarks>
    Private Sub optFiltrar2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optFiltrar2.CheckedChanged
        'uwgAceptaciones.Rows.ColumnFilters.Clear()
        uwgAceptaciones.DataSource = Cache("Aceptaciones")
        uwgAceptaciones.DataBind()
        InitPaginador()
    End Sub

#Region "Controles Paginación"

    ''' <summary>
    ''' Procedimiento controlador de la paginación, en el que determinaremos en qué página estamos
    ''' </summary>
    ''' <remarks>Llamada desde todos los eventos/funciones y procedimientos que recargan la grid 
    ''' Tiempo máximo 0 sec</remarks>
    Public Sub InitPaginador()

        With uwgAceptaciones.DisplayLayout.Pager

            'Actualizamos las combos de paginación
            Dim i As Integer
            ddlPage.Items.Clear()
            ddlPage2.Items.Clear()
            For i = 0 To .PageCount - 1
                ddlPage.Items.Insert(i, New ListItem(i + 1, i))
                ddlPage2.Items.Insert(i, New ListItem(i + 1, i))
            Next
            If (.CurrentPageIndex - 1) >= ddlPage.Items.Count Then .CurrentPageIndex = ddlPage.Items.Count
            ddlPage.Items(.CurrentPageIndex - 1).Selected = True
            ddlPage2.Items(.CurrentPageIndex - 1).Selected = True

            lblPagTot.Text = .PageCount
            lblPagTot2.Text = .PageCount

            If .PageCount = 1 Then
                btnFirstPage.Enabled = False
                'btnFirstPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero_desactivado.gif")
                btnFirstPage.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/primero_desactivado.gif"
                btnPreviousPage.Enabled = False
                btnPreviousPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior_desactivado.gif")
                btnFirstPage.Enabled = True
                btnFirstPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
                btnPreviousPage.Enabled = True
                btnPreviousPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")

                btnFirstPage2.Enabled = False
                'btnFirstPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero_desactivado.gif")
                btnFirstPage2.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/primero_desactivado.gif"
                btnPreviousPage2.Enabled = False
                btnPreviousPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior_desactivado.gif")
                btnFirstPage2.Enabled = True
                btnFirstPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
                btnPreviousPage2.Enabled = True
                btnPreviousPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")

                btnFirstPage.Visible = False
                btnPreviousPage.Visible = False
                lblPagina.Visible = False
                ddlPage.Visible = False
                lblDe.Visible = False
                lblPagTot.Visible = False
                btnNextPage.Visible = False
                btnLastPage.Visible = False

                btnFirstPage2.Visible = False
                btnPreviousPage2.Visible = False
                lblPagina2.Visible = False
                ddlPage2.Visible = False
                lblDe2.Visible = False
                lblPagTot2.Visible = False
                btnNextPage2.Visible = False
                btnLastPage2.Visible = False
            Else
                btnFirstPage.Visible = True
                btnPreviousPage.Visible = True
                lblPagina.Visible = True
                ddlPage.Visible = True
                lblDe.Visible = True
                lblPagTot.Visible = True
                btnNextPage.Visible = True
                btnLastPage.Visible = True

                btnFirstPage2.Visible = True
                btnPreviousPage2.Visible = True
                lblPagina2.Visible = True
                ddlPage2.Visible = True
                lblDe2.Visible = True
                lblPagTot2.Visible = True
                btnNextPage2.Visible = True
                btnLastPage2.Visible = True

                Select Case .CurrentPageIndex
                    Case 1
                        btnFirstPage.Enabled = False
                        'btnFirstPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero_desactivado.gif")
                        btnFirstPage.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/primero_desactivado.gif"
                        btnPreviousPage.Enabled = False
                        btnPreviousPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior_desactivado.gif")
                        btnNextPage.Enabled = True
                        btnNextPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente.gif")
                        btnLastPage.Enabled = True
                        btnLastPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo.gif")

                        btnFirstPage2.Enabled = False
                        'btnFirstPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero_desactivado.gif")
                        btnFirstPage2.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/primero_desactivado.gif"
                        btnPreviousPage2.Enabled = False
                        btnPreviousPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior_desactivado.gif")
                        btnNextPage2.Enabled = True
                        btnNextPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente.gif")
                        btnLastPage2.Enabled = True
                        btnLastPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo.gif")

                    Case .PageCount
                        btnFirstPage.Enabled = True
                        'btnFirstPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
                        btnFirstPage.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/primero.gif"
                        btnPreviousPage.Enabled = True
                        btnPreviousPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")
                        btnNextPage.Enabled = False
                        btnNextPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente_desactivado.gif")
                        btnLastPage.Enabled = False
                        btnLastPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo_desactivado.gif")

                        btnFirstPage2.Enabled = True
                        'btnFirstPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
                        btnFirstPage2.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/primero.gif"
                        btnPreviousPage2.Enabled = True
                        btnPreviousPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")
                        btnNextPage2.Enabled = False
                        btnNextPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente_desactivado.gif")
                        btnLastPage2.Enabled = False
                        btnLastPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo_desactivado.gif")

                    Case Else
                        btnFirstPage.Enabled = True
                        'btnFirstPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
                        btnFirstPage.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/primero.gif"
                        btnPreviousPage.Enabled = True
                        btnPreviousPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")
                        btnNextPage.Enabled = True
                        btnNextPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente.gif")
                        btnLastPage.Enabled = True
                        btnLastPage.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo.gif")

                        btnFirstPage2.Enabled = True
                        'btnFirstPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "primero.gif")
                        btnFirstPage2.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("rutaPMPortal2008") & "App_themes/" & Page.Theme & "/images/primero.gif"
                        btnPreviousPage2.Enabled = True
                        btnPreviousPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "anterior.gif")
                        btnNextPage2.Enabled = True
                        btnNextPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "siguiente.gif")
                        btnLastPage2.Enabled = True
                        btnLastPage2.ImageUrl = String.Format("~/App_Themes/{0}/images/{1}", Me.Page.Theme, "ultimo.gif")
                End Select
            End If
        End With

    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de primera página
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de primera página
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnFirstPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFirstPage.Click, btnFirstPage2.Click

        uwgAceptaciones.DisplayLayout.Pager.CurrentPageIndex = 1
        uwgAceptaciones.DataSource = Cache("Aceptaciones")
        uwgAceptaciones.DataBind()
        InitPaginador()
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de página anterior
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de página anterior
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnPreviousPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPreviousPage.Click, btnPreviousPage2.Click

        uwgAceptaciones.DisplayLayout.Pager.CurrentPageIndex = CInt(ddlPage.SelectedItem.Text) - 1
        uwgAceptaciones.DataSource = Cache("Aceptaciones")
        uwgAceptaciones.DataBind()
        InitPaginador()
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de página siguiente
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de página siguiente
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnNextPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNextPage.Click, btnNextPage2.Click

        uwgAceptaciones.DisplayLayout.Pager.CurrentPageIndex = CInt(ddlPage.SelectedItem.Text) + 1
        uwgAceptaciones.DataSource = Cache("Aceptaciones")
        uwgAceptaciones.DataBind()
        InitPaginador()
    End Sub

    ''' <summary>
    ''' Evento que se lanza al pulsar sobre el botón de última página
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: El botón de última página
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub btnLastPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLastPage.Click, btnLastPage2.Click

        uwgAceptaciones.DisplayLayout.Pager.CurrentPageIndex = uwgAceptaciones.DisplayLayout.Pager.PageCount
        uwgAceptaciones.DataSource = Cache("Aceptaciones")
        uwgAceptaciones.DataBind()
        InitPaginador()
    End Sub

    ''' <summary>
    ''' Evento que se lanza al seleccionar alguna página de la combo de paginación
    ''' </summary>
    ''' <param name="sender">control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde: La combo de paginación
    ''' Tiempo máximo: 0 Sec</remarks>
    Private Sub ddlPage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPage.SelectedIndexChanged, ddlPage2.SelectedIndexChanged

        uwgAceptaciones.DisplayLayout.Pager.CurrentPageIndex = CInt(ddlPage.SelectedItem.Text)
        uwgAceptaciones.DataSource = Cache("Aceptaciones")
        uwgAceptaciones.DataBind()
        InitPaginador()
    End Sub
#End Region

    Protected Sub imgbtnExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnExcel.Click
        uwgAceptaciones.DisplayLayout.Pager.AllowPaging = False
        uwgAceptaciones.DataSource = Cache("Aceptaciones")
        uwgAceptaciones.DataBind()

        UltraWebGridExcelExporter1.DownloadName = "Aceptaciones.xls"
        UltraWebGridExcelExporter1.Export(uwgAceptaciones)

        uwgAceptaciones.DisplayLayout.Pager.AllowPaging = True
        uwgAceptaciones.DataSource = Cache("Aceptaciones")
        uwgAceptaciones.DataBind()
    End Sub

    ''' <summary>
    ''' Optiene la referencia cultural del idioma especificado
    ''' </summary>
    ''' <param name="Idioma">Variable de tipo Idioma</param>
    ''' <returns>Referencia cultural del idioma</returns>
    Public Function ReferenciaCultural(ByVal Idioma As String) As String
        Select Case Idioma
            Case "SPA"
                Return "es-ES_tradnl"
            Case "ENG"
                Return "en-US"
            Case "GER"
                Return "de-DE"
            Case Else
                Return Idioma
        End Select
    End Function

End Class