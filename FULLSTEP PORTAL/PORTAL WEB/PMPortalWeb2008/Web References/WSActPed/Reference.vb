﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.3643
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Xml.Serialization

'
'This source code was auto-generated by Microsoft.VSDesigner, Version 2.0.50727.3643.
'
Namespace WSActPed
    
    'CODEGEN: The optional WSDL extension element 'Policy' from namespace 'http://schemas.xmlsoap.org/ws/2004/09/policy' was not handled.
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.3053"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Web.Services.WebServiceBindingAttribute(Name:="binding", [Namespace]:="urn:sap-com:document:sap:rfc:functions")>  _
    Partial Public Class ZMPVP_WS_ACT_PED
        Inherits System.Web.Services.Protocols.SoapHttpClientProtocol
        
        Private ZMPVP_ACTUALIZAR_PEDOperationCompleted As System.Threading.SendOrPostCallback
        
        Private useDefaultCredentialsSetExplicitly As Boolean
        
        '''<remarks/>
        Public Sub New()
            MyBase.New
            Me.Url = Global.PMPortalWeb2008.My.MySettings.Default.PMPortalWeb2008_WSActPed_ZMPVP_WS_ACT_PEDService
            If (Me.IsLocalFileSystemWebService(Me.Url) = true) Then
                Me.UseDefaultCredentials = true
                Me.useDefaultCredentialsSetExplicitly = false
            Else
                Me.useDefaultCredentialsSetExplicitly = true
            End If
        End Sub
        
        Public Shadows Property Url() As String
            Get
                Return MyBase.Url
            End Get
            Set
                If (((Me.IsLocalFileSystemWebService(MyBase.Url) = true)  _
                            AndAlso (Me.useDefaultCredentialsSetExplicitly = false))  _
                            AndAlso (Me.IsLocalFileSystemWebService(value) = false)) Then
                    MyBase.UseDefaultCredentials = false
                End If
                MyBase.Url = value
            End Set
        End Property
        
        Public Shadows Property UseDefaultCredentials() As Boolean
            Get
                Return MyBase.UseDefaultCredentials
            End Get
            Set
                MyBase.UseDefaultCredentials = value
                Me.useDefaultCredentialsSetExplicitly = true
            End Set
        End Property
        
        '''<remarks/>
        Public Event ZMPVP_ACTUALIZAR_PEDCompleted As ZMPVP_ACTUALIZAR_PEDCompletedEventHandler
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Bare)>  _
        Public Function ZMPVP_ACTUALIZAR_PED(<System.Xml.Serialization.XmlElementAttribute("ZMPVP_ACTUALIZAR_PED", [Namespace]:="urn:sap-com:document:sap:rfc:functions")> ByVal ZMPVP_ACTUALIZAR_PED1 As ZMPVP_ACTUALIZAR_PED) As <System.Xml.Serialization.XmlElementAttribute("ZMPVP_ACTUALIZAR_PEDResponse", [Namespace]:="urn:sap-com:document:sap:rfc:functions")> ZMPVP_ACTUALIZAR_PEDResponse
            Dim results() As Object = Me.Invoke("ZMPVP_ACTUALIZAR_PED", New Object() {ZMPVP_ACTUALIZAR_PED1})
            Return CType(results(0),ZMPVP_ACTUALIZAR_PEDResponse)
        End Function
        
        '''<remarks/>
        Public Overloads Sub ZMPVP_ACTUALIZAR_PEDAsync(ByVal ZMPVP_ACTUALIZAR_PED1 As ZMPVP_ACTUALIZAR_PED)
            Me.ZMPVP_ACTUALIZAR_PEDAsync(ZMPVP_ACTUALIZAR_PED1, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub ZMPVP_ACTUALIZAR_PEDAsync(ByVal ZMPVP_ACTUALIZAR_PED1 As ZMPVP_ACTUALIZAR_PED, ByVal userState As Object)
            If (Me.ZMPVP_ACTUALIZAR_PEDOperationCompleted Is Nothing) Then
                Me.ZMPVP_ACTUALIZAR_PEDOperationCompleted = AddressOf Me.OnZMPVP_ACTUALIZAR_PEDOperationCompleted
            End If
            Me.InvokeAsync("ZMPVP_ACTUALIZAR_PED", New Object() {ZMPVP_ACTUALIZAR_PED1}, Me.ZMPVP_ACTUALIZAR_PEDOperationCompleted, userState)
        End Sub
        
        Private Sub OnZMPVP_ACTUALIZAR_PEDOperationCompleted(ByVal arg As Object)
            If (Not (Me.ZMPVP_ACTUALIZAR_PEDCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent ZMPVP_ACTUALIZAR_PEDCompleted(Me, New ZMPVP_ACTUALIZAR_PEDCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        Public Shadows Sub CancelAsync(ByVal userState As Object)
            MyBase.CancelAsync(userState)
        End Sub
        
        Private Function IsLocalFileSystemWebService(ByVal url As String) As Boolean
            If ((url Is Nothing)  _
                        OrElse (url Is String.Empty)) Then
                Return false
            End If
            Dim wsUri As System.Uri = New System.Uri(url)
            If ((wsUri.Port >= 1024)  _
                        AndAlso (String.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) = 0)) Then
                Return true
            End If
            Return false
        End Function
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=true, [Namespace]:="urn:sap-com:document:sap:rfc:functions")>  _
    Partial Public Class ZMPVP_ACTUALIZAR_PED
        
        Private pEDIDOField As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property PEDIDO() As String
            Get
                Return Me.pEDIDOField
            End Get
            Set
                Me.pEDIDOField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=true, [Namespace]:="urn:sap-com:document:sap:rfc:functions")>  _
    Partial Public Class ZMPVP_ACTUALIZAR_PEDResponse
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.3053")>  _
    Public Delegate Sub ZMPVP_ACTUALIZAR_PEDCompletedEventHandler(ByVal sender As Object, ByVal e As ZMPVP_ACTUALIZAR_PEDCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.3053"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class ZMPVP_ACTUALIZAR_PEDCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As ZMPVP_ACTUALIZAR_PEDResponse
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),ZMPVP_ACTUALIZAR_PEDResponse)
            End Get
        End Property
    End Class
End Namespace
