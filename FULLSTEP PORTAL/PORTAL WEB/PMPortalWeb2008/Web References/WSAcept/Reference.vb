﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.3643
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Xml.Serialization

'
'This source code was auto-generated by Microsoft.VSDesigner, Version 2.0.50727.3643.
'
Namespace WSAcept
    
    'CODEGEN: The optional WSDL extension element 'Policy' from namespace 'http://schemas.xmlsoap.org/ws/2004/09/policy' was not handled.
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.3053"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Web.Services.WebServiceBindingAttribute(Name:="binding", [Namespace]:="urn:sap-com:document:sap:rfc:functions")>  _
    Partial Public Class ZMPVP_WS_RECUP_HIST_ACEP_MOD
        Inherits System.Web.Services.Protocols.SoapHttpClientProtocol
        
        Private ZmpvpRecupHistorialAceptOperationCompleted As System.Threading.SendOrPostCallback
        
        Private useDefaultCredentialsSetExplicitly As Boolean
        
        '''<remarks/>
        Public Sub New()
            MyBase.New
            Me.Url = Global.PMPortalWeb2008.My.MySettings.Default.PMPortalWeb2008_WSAcept_ZMPVP_WS_RECUP_HIST_ACEPTService
            If (Me.IsLocalFileSystemWebService(Me.Url) = true) Then
                Me.UseDefaultCredentials = true
                Me.useDefaultCredentialsSetExplicitly = false
            Else
                Me.useDefaultCredentialsSetExplicitly = true
            End If
        End Sub
        
        Public Shadows Property Url() As String
            Get
                Return MyBase.Url
            End Get
            Set
                If (((Me.IsLocalFileSystemWebService(MyBase.Url) = true)  _
                            AndAlso (Me.useDefaultCredentialsSetExplicitly = false))  _
                            AndAlso (Me.IsLocalFileSystemWebService(value) = false)) Then
                    MyBase.UseDefaultCredentials = false
                End If
                MyBase.Url = value
            End Set
        End Property
        
        Public Shadows Property UseDefaultCredentials() As Boolean
            Get
                Return MyBase.UseDefaultCredentials
            End Get
            Set
                MyBase.UseDefaultCredentials = value
                Me.useDefaultCredentialsSetExplicitly = true
            End Set
        End Property
        
        '''<remarks/>
        Public Event ZmpvpRecupHistorialAceptCompleted As ZmpvpRecupHistorialAceptCompletedEventHandler
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Bare)>  _
        Public Function ZmpvpRecupHistorialAcept(<System.Xml.Serialization.XmlElementAttribute("ZmpvpRecupHistorialAcept", [Namespace]:="urn:sap-com:document:sap:rfc:functions")> ByVal ZmpvpRecupHistorialAcept1 As ZmpvpRecupHistorialAcept) As <System.Xml.Serialization.XmlElementAttribute("ZmpvpRecupHistorialAceptResponse", [Namespace]:="urn:sap-com:document:sap:rfc:functions")> ZmpvpRecupHistorialAceptResponse
            Dim results() As Object = Me.Invoke("ZmpvpRecupHistorialAcept", New Object() {ZmpvpRecupHistorialAcept1})
            Return CType(results(0),ZmpvpRecupHistorialAceptResponse)
        End Function
        
        '''<remarks/>
        Public Overloads Sub ZmpvpRecupHistorialAceptAsync(ByVal ZmpvpRecupHistorialAcept1 As ZmpvpRecupHistorialAcept)
            Me.ZmpvpRecupHistorialAceptAsync(ZmpvpRecupHistorialAcept1, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub ZmpvpRecupHistorialAceptAsync(ByVal ZmpvpRecupHistorialAcept1 As ZmpvpRecupHistorialAcept, ByVal userState As Object)
            If (Me.ZmpvpRecupHistorialAceptOperationCompleted Is Nothing) Then
                Me.ZmpvpRecupHistorialAceptOperationCompleted = AddressOf Me.OnZmpvpRecupHistorialAceptOperationCompleted
            End If
            Me.InvokeAsync("ZmpvpRecupHistorialAcept", New Object() {ZmpvpRecupHistorialAcept1}, Me.ZmpvpRecupHistorialAceptOperationCompleted, userState)
        End Sub
        
        Private Sub OnZmpvpRecupHistorialAceptOperationCompleted(ByVal arg As Object)
            If (Not (Me.ZmpvpRecupHistorialAceptCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent ZmpvpRecupHistorialAceptCompleted(Me, New ZmpvpRecupHistorialAceptCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        Public Shadows Sub CancelAsync(ByVal userState As Object)
            MyBase.CancelAsync(userState)
        End Sub
        
        Private Function IsLocalFileSystemWebService(ByVal url As String) As Boolean
            If ((url Is Nothing)  _
                        OrElse (url Is String.Empty)) Then
                Return false
            End If
            Dim wsUri As System.Uri = New System.Uri(url)
            If ((wsUri.Port >= 1024)  _
                        AndAlso (String.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) = 0)) Then
                Return true
            End If
            Return false
        End Function
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=true, [Namespace]:="urn:sap-com:document:sap:rfc:functions")>  _
    Partial Public Class ZmpvpRecupHistorialAcept
        
        Private eJERCICIOField As String
        
        Private f_FINField As String
        
        Private f_INIField As String
        
        Private sTCD1Field As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property EJERCICIO() As String
            Get
                Return Me.eJERCICIOField
            End Get
            Set
                Me.eJERCICIOField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property F_FIN() As String
            Get
                Return Me.f_FINField
            End Get
            Set
                Me.f_FINField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property F_INI() As String
            Get
                Return Me.f_INIField
            End Get
            Set
                Me.f_INIField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property STCD1() As String
            Get
                Return Me.sTCD1Field
            End Get
            Set
                Me.sTCD1Field = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="urn:sap-com:document:sap:rfc:functions")>  _
    Partial Public Class ZMPVP_ACEPTACIONES_POS
        
        Private eBELPField As String
        
        Private bELNRField As String
        
        Private xBLNRField As String
        
        Private mATNRField As String
        
        Private dESCRField As String
        
        Private mENGEField As Decimal
        
        Private cANT_RECEPField As Decimal
        
        Private cANT_PENDField As Decimal
        
        Private wRBTRField As Decimal
        
        Private fECHAField As Date
        
        Private fECHA_ERField As Date
        
        Private dIFERENCIAField As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property EBELP() As String
            Get
                Return Me.eBELPField
            End Get
            Set
                Me.eBELPField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property BELNR() As String
            Get
                Return Me.bELNRField
            End Get
            Set
                Me.bELNRField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property XBLNR() As String
            Get
                Return Me.xBLNRField
            End Get
            Set
                Me.xBLNRField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property MATNR() As String
            Get
                Return Me.mATNRField
            End Get
            Set
                Me.mATNRField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property DESCR() As String
            Get
                Return Me.dESCRField
            End Get
            Set
                Me.dESCRField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property MENGE() As Decimal
            Get
                Return Me.mENGEField
            End Get
            Set
                Me.mENGEField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property CANT_RECEP() As Decimal
            Get
                Return Me.cANT_RECEPField
            End Get
            Set
                Me.cANT_RECEPField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property CANT_PEND() As Decimal
            Get
                Return Me.cANT_PENDField
            End Get
            Set
                Me.cANT_PENDField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property WRBTR() As Decimal
            Get
                Return Me.wRBTRField
            End Get
            Set
                Me.wRBTRField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType:="date")>  _
        Public Property FECHA() As Date
            Get
                Return Me.fECHAField
            End Get
            Set
                Me.fECHAField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType:="date")>  _
        Public Property FECHA_ER() As Date
            Get
                Return Me.fECHA_ERField
            End Get
            Set
                Me.fECHA_ERField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property DIFERENCIA() As String
            Get
                Return Me.dIFERENCIAField
            End Get
            Set
                Me.dIFERENCIAField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="urn:sap-com:document:sap:rfc:functions")>  _
    Partial Public Class ZMPVP_ACEPTACIONES
        
        Private eBELNField As String
        
        Private aEDATField As Date
        
        Private kBETRField As Decimal
        
        Private wRBTR_CField As Decimal
        
        Private bLDATField As Date
        
        Private vSTATField As String
        
        Private pOSICField() As ZMPVP_ACEPTACIONES_POS
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property EBELN() As String
            Get
                Return Me.eBELNField
            End Get
            Set
                Me.eBELNField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType:="date")>  _
        Public Property AEDAT() As Date
            Get
                Return Me.aEDATField
            End Get
            Set
                Me.aEDATField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property KBETR() As Decimal
            Get
                Return Me.kBETRField
            End Get
            Set
                Me.kBETRField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property WRBTR_C() As Decimal
            Get
                Return Me.wRBTR_CField
            End Get
            Set
                Me.wRBTR_CField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType:="date")>  _
        Public Property BLDAT() As Date
            Get
                Return Me.bLDATField
            End Get
            Set
                Me.bLDATField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property VSTAT() As String
            Get
                Return Me.vSTATField
            End Get
            Set
                Me.vSTATField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlArrayAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified),  _
         System.Xml.Serialization.XmlArrayItemAttribute("item", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=false)>  _
        Public Property POSIC() As ZMPVP_ACEPTACIONES_POS()
            Get
                Return Me.pOSICField
            End Get
            Set
                Me.pOSICField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3082"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=true, [Namespace]:="urn:sap-com:document:sap:rfc:functions")>  _
    Partial Public Class ZmpvpRecupHistorialAceptResponse
        
        Private tiAceptacionesField() As ZMPVP_ACEPTACIONES
        
        '''<remarks/>
        <System.Xml.Serialization.XmlArrayAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified),  _
         System.Xml.Serialization.XmlArrayItemAttribute("item", Form:=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable:=false)>  _
        Public Property TiAceptaciones() As ZMPVP_ACEPTACIONES()
            Get
                Return Me.tiAceptacionesField
            End Get
            Set
                Me.tiAceptacionesField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.3053")>  _
    Public Delegate Sub ZmpvpRecupHistorialAceptCompletedEventHandler(ByVal sender As Object, ByVal e As ZmpvpRecupHistorialAceptCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.3053"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class ZmpvpRecupHistorialAceptCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As ZmpvpRecupHistorialAceptResponse
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),ZmpvpRecupHistorialAceptResponse)
            End Get
        End Property
    End Class
End Namespace
