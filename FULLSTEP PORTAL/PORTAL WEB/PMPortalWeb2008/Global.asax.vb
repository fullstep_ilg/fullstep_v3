﻿Imports System.Web.SessionState

Public Class Global_asax
    Inherits System.Web.HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

    Private Sub Global_AcquireRequestState(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.AcquireRequestState
        '''''*****************************************************************
        '''''                   CODIGO DE VALIDACIÓN 
        '''''*****************************************************************
        ''''If Request.Cookies("USU") Is Nothing Then
        ''''    'La llamada se ha realizado desde el FSPM
        ''''    'Response.Write(Request.Url.AbsoluteUri)

        ''''    'Response.Redirect(ConfigurationManager.AppSettings("rutanormal") & "/../../../")


        ''''Else
        ''''    Dim FSPMServer As New Fullstep.PMPortalServer.Root
        ''''    Dim CiaCode As String
        ''''    Dim Username As String
        ''''    Dim Password As String
        ''''    Dim oUser As Fullstep.PMPortalServer.User


        ''''    Dim lCiaComp As Long = Request.Cookies("USU")("CIACOMP")

        ''''    ' Decode the encoded string.
        ''''    CiaCode = HttpUtility.UrlDecode(Request.Cookies("USU")("CIAFSPM"))


        ''''    Username = Fullstep.PMPortalServer.Encrypter.Encrypt(Request.Cookies("USU")("COD"), , False, Fullstep.PMPortalServer.Encrypter.TipoDeUsuario.Persona, , True)

        ''''    Password = Fullstep.PMPortalServer.Encrypter.Encrypt(Request.Cookies("USU")("PWD"), , False, Fullstep.PMPortalServer.Encrypter.TipoDeUsuario.Persona, , True)

        ''''    If Not Session("FS_PM_User") Is Nothing Then
        ''''        oUser = Session("FS_PM_User")

        ''''        If oUser.Cod <> Username Then
        ''''            oUser = FSPMServer.Login(CiaCode, Username, Password, False)
        ''''            If oUser Is Nothing Then
        ''''                Response.Redirect(ConfigurationManager.AppSettings("rutanormal"))
        ''''            Else
        ''''                oUser.LoadUserData(Username)
        ''''                FSPMServer.Load_LongitudesDeCodigos(lCiaComp)
        ''''                Session("FS_PM_User") = oUser
        ''''                Session("FS_PM_Server") = FSPMServer
        ''''                HttpContext.Current.User = Session("FS_PM_User")
        ''''                Exit Sub
        ''''            End If
        ''''        End If

        ''''        If Not Request.Cookies("USU")("Idioma") Is Nothing Then
        ''''            If oUser.Idioma <> Request.Cookies("USU")("Idioma") Then
        ''''                oUser = FSPMServer.Login(CiaCode, Username, Password, False)
        ''''                If oUser Is Nothing Then
        ''''                    Response.Redirect(ConfigurationManager.AppSettings("rutanormal"))


        ''''                Else
        ''''                    oUser.LoadUserData(Username)
        ''''                    FSPMServer.Load_LongitudesDeCodigos(lCiaComp)
        ''''                    Session("FS_PM_User") = oUser
        ''''                    Session("FS_PM_Server") = FSPMServer
        ''''                    HttpContext.Current.User = Session("FS_PM_User")
        ''''                    Exit Sub
        ''''                End If

        ''''            End If
        ''''        End If


        ''''    Else
        ''''        oUser = FSPMServer.Login(CiaCode, Username, Password, False)
        ''''        If oUser Is Nothing Then
        ''''            Response.Redirect(ConfigurationManager.AppSettings("rutanormal"))


        ''''        Else
        ''''            oUser.LoadUserData(Username)
        ''''            FSPMServer.Load_LongitudesDeCodigos(lCiaComp)
        ''''            Session("FS_PM_User") = oUser
        ''''            Session("FS_PM_Server") = FSPMServer
        ''''            HttpContext.Current.User = Session("FS_PM_User")
        ''''            Exit Sub
        ''''        End If

        ''''    End If

        ''''End If
        '*******************************************************************
    End Sub

End Class