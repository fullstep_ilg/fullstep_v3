﻿Imports System.Configuration
Imports System.Xml
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
Imports System.Threading
Imports FSALPortalWinService.Encrypter
Imports System.IO
Imports System.Web.Services

Public Class root

    Private mDBLogin As String = ""
    Private mDBPassword As String = ""
    Private mDBName As String = ""
    Private mDBServer As String = ""
    Private mDBConnection As String = ""
    Private mIsAuthenticated As Boolean = False

    Public Sub New()
        'Comprobar tema de licencias y cargar datos de conexión
        Dim sDB As String = ConfigurationManager.AppSettings("DB:FS_WS")
        If sDB Is Nothing Then
            Throw New Exception("Configuration file corrupted. Missing FS_WS entry !")
        End If
        'Tenemos que extraer el login y la contraseña de conexión.
        Dim doc As New XmlDocument
        Dim sdblogin As String = ""
        Dim sdbPassword As String = ""
        Try
            doc.Load(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory(), "License.xml"))

        Catch e As Exception
            Throw New Exception("LICENSE file missing!")
        End Try

        Dim child As XmlNode
        child = doc.SelectSingleNode("/LICENSE/DB_LOGIN")
        If Not (child Is Nothing) Then
            mDBLogin = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
            'mDBLogin = child.InnerText
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/DB_PASSWORD")
        If Not (child Is Nothing) Then
            mDBPassword = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
            'mDBPassword = child.InnerText
        Else
            mDBPassword = ""
        End If

        child = doc.SelectSingleNode("/LICENSE/DB_NAME")
        If Not (child Is Nothing) Then
            mDBName = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If
        child = Nothing
        child = doc.SelectSingleNode("/LICENSE/DB_SERVER")
        If Not (child Is Nothing) Then
            mDBServer = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
        End If

        If mDBServer = "" Or mDBName = "" Or mDBLogin = "" Then
            Throw New Exception("Corrupted LICENSE file !")
        End If

        'PENDIENTE METER ENCRIPTACIÓN

        sDB = sDB.Replace("DB_LOGIN", mDBLogin)
        sDB = sDB.Replace("DB_PASSWORD", mDBPassword)
        sDB = sDB.Replace("DB_NAME", mDBName)
        sDB = sDB.Replace("DB_SERVER", mDBServer)
        doc = Nothing
        mDBConnection = sDB

    End Sub
    ''' <summary>
    ''' Funcion que devuelve los parámetros generales de la instalación
    ''' </summary>
    ''' <returns>Un objeto del tipo ParametrosGenerales con los que correspondan de la aplicación</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb/LeerParametrosEjecucionServicioFSAL
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function CargaParametrosFSAL() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SELECT LOGELIMINAR FROM PARGEN_INTERNO"
            cm.CommandType = CommandType.Text
            cm.CommandTimeout = 60
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function CargaParametrosFSAL(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb/LeerParametrosEjecucionServicioFSAL
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerAccesosAlmacenados() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm = New SqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED", cn)
            cm.ExecuteNonQuery()
            cm.Connection = cn
            cm.CommandText = "FSAL_ACCESOSATRASLADAR"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerAccesosAlmacenados(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function
    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub AccesosTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_ACCESOSTRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub AccesosTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub AccesosTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub


    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub AccesosMinTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_ACCESOSMINTRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub AccesosMinTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub AccesosMinTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub

    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub AccesosHoraTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_ACCESOSHORATRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub AccesosHoraTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub AccesosHoraTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub


    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub AccesosDiaTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_ACCESOSDIATRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub AccesosDiaTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub AccesosDiaTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub

    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub AccesosSemanaTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_ACCESOSSEMANATRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub AccesosSemanaTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub AccesosSemanaTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub


    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub AccesosMesTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_ACCESOSMESTRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub AccesosMesTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub AccesosMesTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub



    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub AccesosAnyoTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_ACCESOSANYOTRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub AccesosAnyoTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub AccesosAnyoTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub



    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub AccesosDisSemanaTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_ACCESOSDISSEMANATRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub AccesosDisSemanaTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub AccesosDisSemanaTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub


    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub AccesosDisMesTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_ACCESOSDISMESTRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub AccesosDisMesTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub AccesosDisMesTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub


    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub AccesosDisAnyoTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_ACCESOSDISANYOTRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub AccesosDisAnyoTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub AccesosDisAnyoTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub



    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub EventosTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_EVENTOSTRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub EventosTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub EventosTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub


    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub EventosEstadisticasTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_EVENTOS_ESTTRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub EventosEstadisticasTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub EventosEstadisticasTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub


    Private Function FSGS() As String
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Dim sConsulta As String
        Dim sSRV As Object
        Dim sBD As String

        sConsulta = "SELECT FSGS_SRV,FSGS_BD " & vbCrLf _
      & "  FROM CIAS " & vbCrLf _
      & " WHERE FSGS_BD is not null and FCEST=3"

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED"
            cm.CommandType = CommandType.Text
            cm.ExecuteNonQuery()
            cm = New SqlCommand
            cm.Connection = cn
            cm.CommandText = sConsulta
            cm.CommandType = CommandType.Text
            cm.CommandTimeout = 60
            da.SelectCommand = cm
            da.Fill(dr)
        Catch

        End Try

        cn.Close()

        If dr.Tables(0).Rows.Count = 0 Then
            Return ""
        Else
            sSRV = dr.Tables(0).Rows(0).Item("FSGS_SRV")
            sBD = dr.Tables(0).Rows(0).Item("FSGS_BD")
            If IsDBNull(sSRV) Then
                Return sBD & ".dbo."
            Else
                Return sSRV & "." & sBD & ".dbo."
            End If
        End If

    End Function

    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub AccesosTratadosLOGGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_ACCESOSTRATADOS_LOG_GS"
            cm.CommandType = CommandType.StoredProcedure
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub AccesosTratadosLOGGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub AccesosTratadosLOGGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub



    ''' <summary>
    ''' Prodecimiento que trata a los errores una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los errores </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub ErroresTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_ERRORESTRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub ErroresTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub ErroresTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub



    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub KPIsDisMinTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_KPISDISMINTRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub KPIsDisMinTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub KPIsDisMinTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub


    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub KPIsDisHoraTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_KPISDISHORATRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub KPIsDisHoraTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub KPIsDisHoraTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub


    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub KPIsDisDiaTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_KPISDISDIATRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub KPIsDisDiaTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub KPIsDisDiaTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub


    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub KPIsDisSemanaTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_KPISDISSEMANATRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub KPIsDisSemanaTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub KPIsDisSemanaTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub


    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub KPIsDisMesTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_KPISDISMESTRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub KPIsDisMesTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub KPIsDisMesTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub


    ''' <summary>
    ''' Prodecimiento que trata a los registros una vez cargados en Corporate
    ''' </summary>
    ''' <param name="dt">DataTtable que contiene los ids de los registros </param>
    ''' <param name="iLogEliminar">Integer que nos dice si tenemos o no que eliminar estos registros (0 No eliminar /1 Eliminar) </param>
    ''' <param name="iOk">Integer que nos dice si se ha ncargado bien los registros en corporate </param>
    ''' <remarks>
    ''' Llamada desdE:FSALPortalWinService.service1/root.vb  
    ''' Tiempo máximo:  seg</remarks>
    Public Sub KPIsDisAnyoTratadosFSGS(ByVal dt As DataTable, ByVal iLogEliminar As Integer, ByVal iOk As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_KPISDISANYOTRATADOS"
            Dim ParamIds As New SqlParameter("@TAB", SqlDbType.Structured)
            ParamIds.TypeName = "Tabla_FSAL_IDS"
            ParamIds.Value = dt
            cm.Parameters.Add(ParamIds)
            cm.Parameters.AddWithValue("@LOGELIMINAR", iLogEliminar)
            cm.Parameters.AddWithValue("@OK", iOk)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            cm.ExecuteNonQuery()
        Catch e As SqlException
            Dim ex As New Exception("Sub KPIsDisAnyoTratadosFSGS(): " & e.Message)
            Throw (ex)
        Catch e As Exception
            Dim ex As New Exception("Sub KPIsDisAnyoTratadosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub



    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerAccesosFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_ACCESOSATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerAccesosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerAccesosMinFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_ACCESOSMINATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerAccesosMinFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerAccesosHoraFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_ACCESOSHORAATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerAccesosHoraFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerAccesosDiaFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_ACCESOSDIAATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerAccesosDiaFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerAccesosSemanaFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_ACCESOSSEMANAATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerAccesosSemanaFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerAccesosMesFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_ACCESOSMESATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerAccesosMesFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function


    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerAccesosAnyoFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_ACCESOSANYOATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerAccesosAnyoFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function


    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerAccesosDisSemanaFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_ACCESOSDISSEMANAATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerAccesosDisSemanaFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function


    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerAccesosDisMesFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_ACCESOSDISMESATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerAccesosDisMesFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerAccesosDisAnyoFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_ACCESOSDISANYOATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerAccesosDisAnyoFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function


    ''' <summary>
    ''' Funcion que devuelve los registros(eventos) a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>

    Public Function ObtenerEventosFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_EVENTOSATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerEventosFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que devuelve los registros(eventos) a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>

    Public Function ObtenerEventosEstadisticasFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_EVENTOS_ESTATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerEventosEstadisticasFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que devuelve los accesos de GS a trasladar a FSAL_ACCESOS
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerAccesosLogGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_ACCESOSATRASLADARLOG_GS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerAccesosLogGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que graba accesos de GS en FSAL_ACCESOS
    ''' </summary>
    ''' <param name="dt">DataTable que contiene los accesos a trasladar</param>
    ''' <returns>Un objeto del tipo Integer que nos dice si la insercion ha sido correcta o no</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: </remarks>
    Public Function TrasladarAccesosLogGS(ByVal dt As DataTable) As Integer
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = FSGS() & "FSAL_TRASLADARLOG_GS"
            cm.Parameters.AddWithValue("@TAB", dt)
            cm.Parameters.AddWithValue("@RES", 0)
            cm.Parameters("@RES").Direction = ParameterDirection.Output
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 900
            cm.ExecuteNonQuery()

            Dim ires As Integer
            ires = DBNullToInteger(cm.Parameters("@RES").Value())

            If ires = 0 Then
                Return 0 'Accesos correctamente insertados FSAL_ACCESOS
            Else
                Return 1 'Accesos incorrectamente insertados en FSAL_ACCESOS
            End If

        Catch e As Exception
            Dim ex As New Exception("Function TrasladarAccesosLogGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que lleva las tablas por periodo de tiempo (minuto, hora, dia, semana, mes, anyo) a partir de la de accesos
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function CrearEstadisticasAccesosFSAL(ByVal fecha As Date, ByVal entorno As String, ByVal pyme As String) As Integer
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Dim numReg As Integer
        Dim fec As String

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_ESTADISTICAS_ENTORNO"
            cm.CommandType = CommandType.StoredProcedure
            fec = fecha.Year & "-" & fecha.Month & "-" & fecha.Day & " " & fecha.Hour & ":" & fecha.Minute & ":" & fecha.Second
            cm.Parameters.AddWithValue("@fecha", fec)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.CommandTimeout = 1800
            da.SelectCommand = cm
            numReg = cm.ExecuteNonQuery()
            Return numReg
        Catch e As Exception
            Dim ex As New Exception("Function CrearEstadisticasAccesosFSAL(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que rellena la tabla Eventos de la tabla log y transferencia de certificados, contratos y facturas
    ''' </summary>
    ''' <param name="fecha"></param>
    ''' <param name="entorno"></param>
    ''' <param name="pyme"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function TransferirEventosFSAL(ByVal fecha As Date, ByVal entorno As String, ByVal pyme As String) As Integer
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Dim numReg As Integer

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_TRANSFERIR_EVENTOS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecha", fecha)
            cm.Parameters.AddWithValue("@entorno", entorno)
            If (Not pyme Is Nothing) Then
                cm.Parameters.AddWithValue("@pyme", pyme)
            End If
            cm.CommandTimeout = 900
            da.SelectCommand = cm
            numReg = cm.ExecuteNonQuery()
            Return numReg
        Catch e As Exception
            Dim ex As New Exception("Function TransferirEventosFSAL(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que llena las tablas por periodo de tiempo (minuto, hora, dia, semana, mes, anyo) a partir de la de accesos
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function CrearEstadisticasEventosFSAL(ByVal fecha As Date, ByVal entorno As String, ByVal pyme As String) As Integer
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Dim numReg As Integer

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_ESTADISTICAS_EVENTOS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecha", fecha)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.CommandTimeout = 1800
            da.SelectCommand = cm
            numReg = cm.ExecuteNonQuery()
            Return numReg
        Catch e As Exception
            Dim ex As New Exception("Function CrearEstadisticasEventosFSAL(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    Public Function CrearEstadisticasErroresFSAL(ByVal fecha As Date, ByVal entorno As String, ByVal pyme As String) As Integer
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim numReg As Integer

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_ESTADISTICAS_ERRORES"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecha", fecha)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.CommandTimeout = 1800
            da.SelectCommand = cm
            numReg = cm.ExecuteNonQuery()
            Return numReg
        Catch e As Exception
            Dim ex As New Exception("Function CrearEstadisticasErroresFSAL(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function


    ''' <summary>
    ''' Funcion que llena las tablas por periodo de tiempo (minuto, hora, dia, semana, mes, anyo) a partir de la de accesos
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function CrearEstadisticasKPIsFSAL(ByVal fecha As Date, ByVal entorno As String, ByVal pyme As String) As Integer
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter
        Dim numReg As Integer

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_ESTADISTICAS_KPIS"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@fecha", fecha)
            cm.Parameters.AddWithValue("@entorno", entorno)
            cm.Parameters.AddWithValue("@pyme", pyme)
            cm.CommandTimeout = 1800
            da.SelectCommand = cm
            numReg = cm.ExecuteNonQuery()
            Return numReg
        Catch e As Exception
            Dim ex As New Exception("Function CrearEstadisticasKPIsFSAL(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function


    ''' <summary>
    ''' Funcion que devuelve el entorno en el que estamos
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerEntorno()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_OBTENER_ENTORNO"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 60
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerEntorno(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que devuelve la pyme donde estamos
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>

    Public Function ObtenerPyme()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_OBTENER_PYME"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 60
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerPyme(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function


    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerErroresFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_ERRORESATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerErroresFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function


    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerKPIsDisMinFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_KPISDISMINATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerKPIsDisMinFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerKPIsDisHoraFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_KPISDISHORAATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerKPIsDisHoraFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerKPIsDisDiaFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_KPISDISDIAATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerKPIsDisDiaFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerKPIsDisSemanaFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_KPISDISSEMANAATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerKPIsDisSemanaFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerKPIsDisMesFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_KPISDISMESATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerKPIsDisMesFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Funcion que devuelve los registros a trasladar a Corporate
    ''' </summary>
    ''' <returns>Un dataset con los datos</returns>
    ''' <remarks>
    ''' Llamada desdE: Service1.vb
    ''' Tiempo máximo: 0,15 seg</remarks>
    Public Function ObtenerKPIsDisAnyoFSGS() As Data.DataSet
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_KPISDISANYOATRASLADAR_FSGS"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 300
            da.SelectCommand = cm
            da.Fill(dr)
            Return dr
        Catch e As Exception
            Dim ex As New Exception("Function ObtenerKPIsDisAnyoFSGS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function


    Public Sub BackOBorradoHistAccesosFSAL(periodo As Integer, borrar_backup As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_BACKBORRADO_ACCESOS_FSGS"
            cm.Parameters.AddWithValue("@PERIODO", periodo)
            cm.Parameters.AddWithValue("@BORRAR_BACK", borrar_backup)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 1800
            cm.ExecuteNonQuery()
        Catch e As Exception
            Dim ex As New Exception("Sub BackOBorradoHistAccesosFSAL(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub

    Public Sub BackOBorradoHistEventosFSAL(periodo As Integer, borrar_backup As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_BACKBORRADO_EVENTOS_FSGS"
            cm.Parameters.AddWithValue("@PERIODO", periodo)
            cm.Parameters.AddWithValue("@BORRAR_BACK", borrar_backup)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 1800
            cm.ExecuteNonQuery()
        Catch e As Exception
            Dim ex As New Exception("Sub BackOBorradoHistEventosFSAL(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub

    Public Sub BackOBorradoHistErroresFSAL(periodo As Integer, borrar_backup As Integer)
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim dr As New DataSet
        Dim da As New SqlDataAdapter

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_BACKBORRADO_ERRORES_FSGS"
            cm.Parameters.AddWithValue("@PERIODO", periodo)
            cm.Parameters.AddWithValue("@BORRAR_BACK", borrar_backup)
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 1800
            cm.ExecuteNonQuery()
        Catch e As Exception
            Dim ex As New Exception("Sub BackOBorradoHistErroresFSAL(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Sub


    Public Function CatalogarKPIS()
        Dim cn As New SqlConnection(mDBConnection)
        Dim cm As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim numReg As Integer

        Try
            cn.Open()
            cm.Connection = cn
            cm.CommandText = "FSAL_CATALOGAR_PAGINAS_POR_KPI"
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandTimeout = 1800
            da.SelectCommand = cm
            numReg = cm.ExecuteNonQuery()
            Return numReg
        Catch e As Exception
            Dim ex As New Exception("Function CatalogarKPIS(): " & e.Message)
            Throw (ex)
        Finally
            cn.Dispose()
            cm.Dispose()
            da.Dispose()
        End Try

    End Function


    ''' <summary>
    ''' Tratamos de convertir el objeto recibido a Integer
    ''' </summary>
    ''' <param name="value">Valor a convertir a Integer</param>
    ''' <returns>Valor convertido a Integer. Si no es posible o el value es null o nothing, devuelve 0</returns>
    ''' <remarks>Llamado desde toda la aplicación, donde se requiera su uso. Tiempo máx inferior a 0,1 seg</remarks>
    Public Function DBNullToInteger(Optional ByVal value As Object = Nothing) As Integer
        If IsDBNull(value) Then
            Return 0
        ElseIf value Is Nothing Then
            Return 0
        Else
            Return CInt(value)
        End If
    End Function

End Class