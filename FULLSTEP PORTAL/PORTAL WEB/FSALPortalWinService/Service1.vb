﻿Imports System.Threading
Imports System.Configuration
Imports System.Xml
Imports System.Runtime.InteropServices
Imports FSALPortalWinService.Encrypter
Imports System.Data.SqlClient

Public Class Service1
    Private WithEvents temporizador As Timers.Timer

    'Si no se introduce modo de ejecucion, hora de ejecucion o dia de arranque, que no se ejecute 
    Private ActivoServicioFSAL As Boolean = False
    Private ThreadActivo As Boolean = False

    Private m_modoEjecucion_FSAL As String '(C: Cada x minutos, D: Cada x dias, S: Cada x semanas, M: MensualCada x meses)
    Private m_Periodo_FSAL As String 'Numero de minutos, dias, semanas o meses
    Private m_hora_FSAL As String 'Hora de ejecucion
    Private m_dia_FSAL As String '1 Lunes .. 7 Domingo (Modo Ejecucion S), 1 .. 31 (Modo Ejecucion M)

    'Service FSAL
    Private m_sFile_FSAL As String
    Private g_iLogEliminar As Integer
    Private g_sEntorno As String
    Private g_sPyme As String
    Private m_sErrorFile_FSAL As String

    Private Const c_iRegistrosATrasladar As Integer = 1000    'Bloque de registros simultaneos en cada traslado. NO MODIFICAR!! En los SP se recogen de 1000 en 1000. 
    '                                                         'Solo modificar en caso de cambiar la cantidad tambien en los SP, y establecer aqui la misma.
    Private Const c_iMaxIntentosTraslado As Integer = 100     'Numero de intentos o traslados maximos en cada ejecucion para cada tipo de traslado

    Protected Overrides Sub OnStart(ByVal args() As String)
#If DEBUG Then
        ' When debugging a service, you have to attach to a running process. This gives me 
        ' time to attach for debugging to slowdown startup so I can set a breakpoint. 
        System.Diagnostics.Debugger.Launch()
        'System.Threading.Thread.Sleep(15000)
#End If

        LeerParametrosEjecucionServicioFSAL()
        ArrancarTemporizador()

    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

#Region "temporizador"
    ''' <summary>
    ''' Arrancar el temporizador
    ''' </summary>
    ''' <remarks>Llamada desde=Service1.OnStart; Tiempo maximo=0seg.</remarks>
    Private Sub ArrancarTemporizador()

        temporizador = New Timers.Timer
        temporizador.AutoReset = True

        'Cada minuto se comprueba si se ha cumplido el periodo indicado para su ejecucion.
        temporizador.Interval = (1000 * 60)

        temporizador.Enabled = True

        If ActivoServicioFSAL Then
            ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicioFSAL))
        End If

        temporizador.Start()

    End Sub
    ''' <summary>
    ''' Cuando pasa el intervalo de tiempo indicado para el Temporizador debe ejecutar los diferentes servicios
    ''' </summary>
    ''' <param name="sender">objeto de sistema</param>
    ''' <param name="e">evento de sistema</param>
    ''' <remarks>Llamada desde: sistema; Tiempo maximo: 0,3</remarks>
    Private Sub temporizador_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles temporizador.Elapsed

        If FechaProximaEjecucionServicioFSAL() < Now Then
            ActivoServicioFSAL = True
        End If
        If ActivoServicioFSAL And Not ThreadActivo Then
            ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf EjecucionServicioFSAL))
        End If

    End Sub
#End Region

    Private Sub EjecucionServicioFSAL()
        Dim sPath As String = ConfigurationManager.AppSettings("Path")

        ThreadActivo = True

        Dim dFechaEjecucion As Date
        dFechaEjecucion = FechaProximaEjecucionServicioFSAL()

        m_sFile_FSAL = sPath & "\ServicioFSAL(" & ObtenerNombreServicio() & ").log"
        m_sErrorFile_FSAL = sPath & "\ServicioFSAL_Error(" & ObtenerNombreServicio() & ").log"

        Dim dFechaRangoDesde As Date = LeerFechaServicioFSAL()

        If DateDiff("n", Now(), dFechaRangoDesde) <= 0 Then

            Try

                'Obtenemos la fecha/hora de inicio de la ejecucion
                Dim dFechaIniEjecucion As Date = Now()

                'Transformamos la hora a UTC que es como estan en BBDD
                Dim curTimeZone As TimeZone = TimeZone.CurrentTimeZone
                Dim currentOffset As TimeSpan = curTimeZone.GetUtcOffset(DateTime.Now)

                dFechaRangoDesde = DateAdd(DateInterval.Minute, -currentOffset.TotalMinutes, CType(dFechaRangoDesde, DateTime))

                'Puede ser necesario aumentar hacia atras unos minutos el rango de tiempo considerado para las estadisticas,
                'debido a que si se hacen estadisticas para registros que se han recibido de otras ejecuciones de otros servicios,
                'es necesario tener en cuenta que estos pueden llegar a la BBDD cuando ya no se tendran en cuenta para las estadisticas
                'por el desfase entre el rango de tiempo de estos y el rango de tiempo considerado para las estadisticas.
                If IsNumeric(ConfigurationManager.AppSettings("MINUTOS_RETRASO_FECHADESDE")) Then
                    dFechaRangoDesde = DateAdd(DateInterval.Minute, CInt(ConfigurationManager.AppSettings("MINUTOS_RETRASO_FECHADESDE")) * -1, CType(dFechaRangoDesde, DateTime))
                End If


                '*******Inicio de los traspasos y estadisticas*******

                'TRASLADAMOS LAS SESIONES DE GS A FSAL_ACCESOS
                If ConfigurationManager.AppSettings("TRASLADO_GS_A_FSAL_ACCESOS") = 1 Then
                    TrasladarSesionesGsFSAL()
                End If

                'CREAMOS LAS ESTADISTICAS DE ACCESOS EN LAS TABLAS FSAL_USUARIOS_MINUTO, FSAL_USUARIOS_HORA, FSAL_USUARIOS_DIA, FSAL_USUARIOS_SEMANA, FSAL_USUARIOS_MES, FSAL_USUARIOS_ANYO, FSAL_USUARIOS_DIS_SEMANA, FSAL_USUARIOS_DIS_MES, FSAL_USUARIOS_DIS_ANYO
                If ConfigurationManager.AppSettings("ESTADISTICAS_FSAL_ACCESOS") = 1 Then
                    CrearEstadisticasAccesosFSAL(dFechaRangoDesde)
                End If

                'TRASLADAMOS EVENTOS A LA TABLA FSAL_EVENTOS
                If ConfigurationManager.AppSettings("TRASLADO_EVENTOS_A_FSAL_EVENTOS") = 1 Then
                    TrasladarEventosFSAL(dFechaRangoDesde)
                End If

                'CREAMOS LAS ESTADISTICAS DE EVENTOS EN LA TABLA FSAL_EVENTOS_ESTADISTICAS
                If ConfigurationManager.AppSettings("ESTADISTICAS_FSAL_EVENTOS") = 1 Then
                    CrearEstadisticasEventosFSAL(dFechaRangoDesde)
                End If

                'TRASLADAMOS ERRORES A LA TABLA FSAL_ERRORES
                If ConfigurationManager.AppSettings("TRASLADO_ERRORES_A_FSAL_ERRORES") = 1 Then
                    TrasladarErroresFSAL(dFechaRangoDesde)
                End If

                'CATALOGAMOS LOS ACCESOS POR KPI EN FSAL_ACCESOS
                If ConfigurationManager.AppSettings("CATALOGAR_FSAL_ACCESOS_POR_KPI") = 1 Then
                    CatalogarAccesosKPIsFSAL()
                End If

                'CREAMOS LAS ESTADISTICAS DE KPIs EN LAS TABLAS FSAL_KPIS_MINUTO, FSAL_KPIS_HORA, FSAL_KPIS_DIA, FSAL_KPIS_SEMANA, FSAL_KPIS_MES, FSAL_KPIS_ANYO
                If ConfigurationManager.AppSettings("ESTADISTICAS_FSAL_KPIS") = 1 Then
                    CrearEstadisticasKPIsFSAL(dFechaRangoDesde)
                End If

                'TRASLADAMOS LOS DATOS DE LAS TABLAS ORIGEN Y TABLAS ESTADISTICAS A CORPORATE
                If (ConfigurationManager.AppSettings("TRASLADO_LOG_A_CORPORATE") = 1) AndAlso
                    (Not Global.FSALPortalWinService.My.MySettings.Default.FSALPortalWinService_FSALWebService_FSALWebService = "") Then
                    TrasladarLogFSAL()
                End If

                'BORRAMOS LOS DATOS DE LAS TABLAS ORIGEN CON OPCION DE TRASPASO A HISTORICOS
                If (ConfigurationManager.AppSettings("BORRAR_TABLAS_ORIGEN_LOG_FSAL") = 1) Then
                    TrasladoAHistoricosBorradoEnOrigenFSAL()
                End If

                '*******Fin de los traspasos y estadisticas*******

                'Marcamos la ultima ejecucion del servicio escribiendo en el fichero de control
                BorrarFicheroServicioFSAL()
                'Escribir Fichero log , para indicarle cuando se realizo el traspaso por ultima vez
                EscribirServicioFSAL(dFechaEjecucion, dFechaIniEjecucion, Now(), dFechaRangoDesde)

            Catch e As Exception

                EscribirError("Error en la ejecucion " & g_sEntorno & " " & g_sPyme & ": " & e.Message)
                ' Aunque se produzca error no se deberia de parar el servicio
                ' Por ejemplo en el reinicio de las bases de datos
            End Try

        End If

        ActivoServicioFSAL = False 'Depues de ejecutar que no vuelva a ejecutarse hasta que no coincida con la proxima ejecucion
        ThreadActivo = False

    End Sub

    Private Sub CatalogarKPIS()
        Dim oRoot As New root
        oRoot.CatalogarKPIS()
    End Sub

    ''' <summary>
    ''' Borra el fichero log para posteriormente volver a escribirlo
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioFSAL; Tiempo maximo=0seg.</remarks>
    Private Sub BorrarFicheroServicioFSAL()
        If System.IO.File.Exists(m_sFile_FSAL) Then
            System.IO.File.Delete(m_sFile_FSAL)
        End If
    End Sub

    Private Function FechaProximaEjecucionServicioFSAL() As Date

        'fecha/hora, o la actual o la de finalizacion de anterior ejecucion
        'If no existe fichero de proximidad FSAL
        If Not System.IO.File.Exists(m_sFile_FSAL) Then
            FechaProximaEjecucionServicioFSAL = Date.Now
        Else
            'fecha anterior ejecucion con la hora de ejecucion del servicio
            FechaProximaEjecucionServicioFSAL = System.IO.File.GetLastWriteTime(m_sFile_FSAL)
        End If

        Select Case m_modoEjecucion_FSAL
            Case "C"
                'fecha/hora + minutos m_Periodo_FSAL
                FechaProximaEjecucionServicioFSAL = FechaProximaEjecucionServicioFSAL.AddMinutes(m_Periodo_FSAL)
            Case "D"
                'fecha/hora + dias m_Periodo_FSAL y aplicamos la hora m_hora_FSAL
                FechaProximaEjecucionServicioFSAL = FechaProximaEjecucionServicioFSAL.AddDays(m_Periodo_FSAL)
                FechaProximaEjecucionServicioFSAL = CDate(FechaProximaEjecucionServicioFSAL.Date & " " & m_hora_FSAL)
            Case "S"
                'fecha/hora + semanas m_Periodo_FSAL y aplicamos la hora m_hora_FSAL y que ademas sea dia de la semana m_dia_FSAL 1 Lunes .. 7 Domingo
                FechaProximaEjecucionServicioFSAL = FechaProximaEjecucionServicioFSAL.AddDays(m_Periodo_FSAL * 7)
                FechaProximaEjecucionServicioFSAL = CDate(FechaProximaEjecucionServicioFSAL.Date & " " & m_hora_FSAL)
                Do Until FechaProximaEjecucionServicioFSAL.DayOfWeek = m_dia_FSAL
                    FechaProximaEjecucionServicioFSAL = FechaProximaEjecucionServicioFSAL.AddDays(-1) 'restamos un dia hasta hacer coincidir el dia de la semana
                Loop
            Case "M"
                'fecha/hora + meses m_Periodo_FSAL y aplicamos la hora m_hora_FSAL y que ademas sea dia del mes m_dia_FSAL 1 .. 31
                FechaProximaEjecucionServicioFSAL = FechaProximaEjecucionServicioFSAL.AddMonths(m_Periodo_FSAL)
                FechaProximaEjecucionServicioFSAL = CDate(FechaProximaEjecucionServicioFSAL.Date & " " & m_hora_FSAL)
                Do Until FechaProximaEjecucionServicioFSAL.Day = m_dia_FSAL
                    FechaProximaEjecucionServicioFSAL = FechaProximaEjecucionServicioFSAL.AddDays(-1) 'restamos un dia hasta hacer coincidir el dia del mes
                Loop
            Case Else
                FechaProximaEjecucionServicioFSAL = Date.MaxValue
        End Select

        Return FechaProximaEjecucionServicioFSAL

    End Function

    ''' <summary>
    ''' Escribe un fichero log para indicarnos cuando se ha ejecutado el servicio
    ''' </summary>
    ''' <param name="fechaEjecucion">Fecha para la activacion de la ejecucion del servicio</param>
    ''' <param name="fechaIniEjecucion">Fecha inicio de ejecucion del servicio</param>
    ''' <param name="fechaFinEjecucion">Fecha fin de ejecucion del servicio</param>
    ''' <param name="fechaRangoDesde">Fecha utilizada como comienzo del rango para el traspaso y estadisticas, en UTC</param>
    ''' <remarks>Llamada desde=EjecucionServicioFSAL; Tiempo .</remarks>
    Private Sub EscribirServicioFSAL(ByVal fechaEjecucion As Date, ByVal fechaIniEjecucion As DateTime, ByVal fechaFinEjecucion As Date, ByVal fechaRangoDesde As Date)
        Dim str1 = "Fichero log del Servicio de Traspasos y Estadisticas FSAL"
        Dim str2 = "========================================================="
        Dim oFileW As System.IO.StreamWriter
        oFileW = New System.IO.StreamWriter(m_sFile_FSAL, True)
        oFileW.WriteLine(str1)
        oFileW.WriteLine(str2)
        oFileW.WriteLine("Fecha Activacion para la Ejecucion = " & fechaEjecucion)
        oFileW.WriteLine("Fecha Inicio de Ejecucion = " & fechaIniEjecucion)
        oFileW.WriteLine("Fecha Fin de Ejecucion = " & fechaFinEjecucion)
        oFileW.WriteLine("Fecha Comienzo del Rango en Traspasos y Estadisticas = " & fechaRangoDesde & " (UTC)")

        oFileW.Close()
    End Sub


    ''' <summary>
    ''' Lee del fichero log cuando se ha iniciado la ejecucion del servicio por ultima vez
    ''' </summary>
    ''' <remarks>Llamada desde: EjecucionServicioFSAL; Tiempo .</remarks>
    Private Function LeerFechaServicioFSAL() As Date
        Dim returnFecha As Date

        returnFecha = Now()

        Try
            If System.IO.File.Exists(m_sFile_FSAL) Then
                Dim oFileR As New System.IO.StreamReader(m_sFile_FSAL)
                Dim linea As String
                While oFileR.Peek() <> -1
                    linea = oFileR.ReadLine()
                    If InStr(linea, "Fecha Inicio de Ejecucion") Then
                        returnFecha = CDate(Trim(Split(linea, "=")(1)))
                        Exit While
                    End If
                End While
                oFileR.Close()
                oFileR = Nothing
            End If
        Catch e As Exception
            'Se utiliza la fecha actual
        End Try

        Return returnFecha

    End Function

    ''' <summary>
    ''' Escribe un fichero log con los errores controlados que se han producido durante la ejecucion del servicio
    ''' </summary>
    ''' <param name="sMensaje">Mensaje del error controlado que hay que escribir en el log</param>
    ''' <remarks>Llamada desde=EjecucionServicioFSAL, TrasladarLogFSAL, TrasladarLogFSAL_GS; Tiempo .</remarks>
    Private Sub EscribirError(ByVal sMensaje As String)
        Dim oFileW As System.IO.StreamWriter
        Dim bExisteLog As Boolean = System.IO.File.Exists(m_sErrorFile_FSAL)

        oFileW = New System.IO.StreamWriter(m_sErrorFile_FSAL, True)
        If Not bExisteLog Then
            oFileW.WriteLine("Fichero de Errores del Servicio de Traspasos y Estadisticas FSAL")
            oFileW.WriteLine("================================================================")
        End If
        oFileW.WriteLine(Format(Now(), "yyyy/MM/dd HH:mm:ss") & " => " & sMensaje)
        oFileW.Close()
    End Sub

    ''' <summary>
    ''' Obtiene los ids del datatable pasado por parametro
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerTablaIDS(ByVal dt As DataTable) As DataTable
        Dim dtIDs As New DataTable
        Dim dtIDsAux As New DataTable
        Dim dnewID As DataRow

        Try

            dtIDs.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dtIDsAux = dtIDs.Clone

            For x As Integer = 0 To dt.Rows.Count - 1
                dnewID = dtIDsAux.NewRow
                dnewID.Item("ID") = dt.Rows(x).Item("ID")
                dtIDsAux.Rows.Add(dnewID)
            Next

        Catch e As Exception
            'Se pasa un DataTable vacio.
        End Try

        Return dtIDsAux

    End Function

    ''' <summary>
    ''' Ejecutar el webService con el usuario administrador
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioTrasladarLogAccesos; Tiempo maximo=0,2seg.</remarks>
    Private Sub TrasladarSesionesGsFSAL()
        ''Genero Dataset a enviar 
        Dim dsAccATrasladar As DataSet
        Dim oRoot As New root
        Dim dtIDs As DataTable
        Dim iRes As Integer 'Traslado correcto 0 incorrecto 1
        Dim iRegistrosATrasladar As Integer = c_iRegistrosATrasladar
        Dim iMaxIntentosTraslado As Integer = c_iMaxIntentosTraslado

        ''INICIO TRATAMIENTO DE REGISTROS DE ACCESOS GS
        Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
            ''''Obtengo los Accesos a trasladar de GS y traspaso lo pongo a 1
            dsAccATrasladar = oRoot.ObtenerAccesosLogGS()
            dtIDs = ObtenerTablaIDS(dsAccATrasladar.Tables(0))
            If dsAccATrasladar.Tables(0).Rows.Count > 0 Then

                'g_sEntorno = dsAccATrasladar.Tables(0).Rows(0).Item("FSAL_ENTORNO").ToString()
                'g_sPyme = dsAccATrasladar.Tables(0).Rows(0).Item("FSAL_PYME").ToString()

                Try
                    iRes = oRoot.TrasladarAccesosLogGS(dsAccATrasladar.Tables(0))
                Catch ex As Exception
                    EscribirError("Error traslado accesos LOG GS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                    iRes = 1
                End Try

                oRoot.AccesosTratadosLOGGS(dtIDs, 0, iRes)

            End If
            iRegistrosATrasladar = dsAccATrasladar.Tables(0).Rows.Count
            iMaxIntentosTraslado -= 1
        Loop
        ''FIN TRATAMIENTO DE REGISTROS DE ACCESOS FSGS

    End Sub

    ''' <summary>
    ''' Ejecutar el webService con el usuario administrador
    ''' Escribe mensajes en el event viewer para hacer seguimiento de instalacion
    ''' </summary>
    ''' <remarks>Llamada desde=EjecucionServicioTrasladarLogAccesos; Tiempo mÃ¡ximo=0,2seg.</remarks>
    Private Sub TrasladarLogFSAL()
        Dim dsAccATrasladar As DataSet
        Dim dsEventosATrasladar As DataSet
        Dim dsAccMinATrasladar As DataSet
        Dim dsAccHoraATrasladar As DataSet
        Dim dsAccDiaATrasladar As DataSet
        Dim dsAccSemanaATrasladar As DataSet
        Dim dsAccMesTrasladar As DataSet
        Dim dsAccAnyoTrasladar As DataSet
        Dim dsAccDisSemanaTrasladar As DataSet
        Dim dsAccDisMesTrasladar As DataSet
        Dim dsAccDisAnyoTrasladar As DataSet
        Dim dsEventosEstadisticasATrasladar As DataSet
        Dim dsErroresATrasladar As DataSet
        Dim dsKPIsDisMinATrasladar As DataSet
        Dim dsKPIsDisHoraATrasladar As DataSet
        Dim dsKPIsDisDiaATrasladar As DataSet
        Dim dsKPIsDisSemanaATrasladar As DataSet
        Dim dsKPIsDisMesATrasladar As DataSet
        Dim dsKPIsDisAnyoATrasladar As DataSet
        Dim iRes As Integer 'Traslado correcto 0 incorrecto 1
        Dim oRoot As New root
        Dim dtIDs As DataTable
        Dim webServiceFsal As New FSALWebService.FSALWebService
        Dim iRegistrosATrasladar As Integer = c_iRegistrosATrasladar
        Dim iMaxIntentosTraslado As Integer = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_ACCESOS_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE REGISTROS DE ACCESOS FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los Accesos a trasladar de FSGS y traspaso lo pongo a 1
                dsAccATrasladar = oRoot.ObtenerAccesosFSGS()
                dtIDs = ObtenerTablaIDS(dsAccATrasladar.Tables(0))
                If dsAccATrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsAccATrasladar.Tables(0).Rows(0).Item("FSAL_ENTORNO").ToString()
                    'g_sPyme = dsAccATrasladar.Tables(0).Rows(0).Item("FSAL_PYME").ToString()

                    Try
                        iRes = webServiceFsal.CallFSALWebService(dsAccATrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado accesos FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.AccesosTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsAccATrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE ACCESOS FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_EVENTOS_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE REGISTROS DE EVENTOS FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                'Obtengo los eventos a trasladar de FSGS y traspaso lo pongo a 1
                dsEventosATrasladar = oRoot.ObtenerEventosFSGS()
                dtIDs = ObtenerTablaIDS(dsEventosATrasladar.Tables(0))
                If dsEventosATrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsEventosATrasladar.Tables(0).Rows(0).Item("FSAL_ENTORNO").ToString()
                    'g_sPyme = dsEventosATrasladar.Tables(0).Rows(0).Item("FSAL_PYME").ToString()

                    Try
                        'INICIO REORDENO LAS COLUMNAS PORQUE SI NO ME DA ERROR AL ENVIARLO A CORPORATE
                        dsEventosATrasladar.Tables(0).Columns("ID").SetOrdinal(0)
                        dsEventosATrasladar.Tables(0).Columns("FSAL_ENTORNO").SetOrdinal(1)
                        dsEventosATrasladar.Tables(0).Columns("FECHA_EVENTO").SetOrdinal(2)
                        dsEventosATrasladar.Tables(0).Columns("TIPO_EVENTO").SetOrdinal(3)
                        dsEventosATrasladar.Tables(0).Columns("DATA").SetOrdinal(4)
                        dsEventosATrasladar.Tables(0).Columns("FECACT").SetOrdinal(5)
                        dsEventosATrasladar.Tables(0).Columns("FSAL_PYME").SetOrdinal(6)
                        dsEventosATrasladar.Tables(0).Columns("TRASPASO").SetOrdinal(7)

                        iRes = webServiceFsal.FSALWebServiceEventos(dsEventosATrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado eventos FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.EventosTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsEventosATrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE EVENTOS FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_EVENTOS_ESTADISTICAS_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE REGISTROS DE EVENTOS_ESTADISTICAS FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                'Obtengo los eventos estadisticas a trasladar de FSGS y traspaso lo pongo a 1
                dsEventosEstadisticasATrasladar = oRoot.ObtenerEventosEstadisticasFSGS()
                dtIDs = ObtenerTablaIDS(dsEventosEstadisticasATrasladar.Tables(0))
                If dsEventosEstadisticasATrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsEventosEstadisticasATrasladar.Tables(0).Rows(0).Item("FSAL_ENTORNO").ToString()
                    'g_sPyme = dsEventosEstadisticasATrasladar.Tables(0).Rows(0).Item("FSAL_PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceEventosEstadisticas(dsEventosEstadisticasATrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado eventos estadisticas FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.EventosEstadisticasTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsEventosEstadisticasATrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE EVENTOS ESTADISTICAS FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_USUARIOS_MINUTO_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE ACCESOS_USUARIOS_MIN DE FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los accesos a trasladar de FSGS y traspaso lo pongo a 1
                dsAccMinATrasladar = oRoot.ObtenerAccesosMinFSGS()
                dtIDs = ObtenerTablaIDS(dsAccMinATrasladar.Tables(0))
                If dsAccMinATrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsAccMinATrasladar.Tables(0).Rows(0).Item("ENTORNO").ToString()
                    'g_sPyme = dsAccMinATrasladar.Tables(0).Rows(0).Item("PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceAccesosCadaCincoMinutos(dsAccMinATrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado accesos usuarios minuto FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.AccesosMinTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsAccMinATrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_MIN FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_USUARIOS_HORA_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_HORA FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los Accesos a trasladar de FSGS y traspaso lo pongo a 1
                dsAccHoraATrasladar = oRoot.ObtenerAccesosHoraFSGS()
                dtIDs = ObtenerTablaIDS(dsAccHoraATrasladar.Tables(0))
                If dsAccHoraATrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsAccHoraATrasladar.Tables(0).Rows(0).Item("ENTORNO").ToString()
                    'g_sPyme = dsAccHoraATrasladar.Tables(0).Rows(0).Item("PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceAccesosHora(dsAccHoraATrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado accesos usuarios hora FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.AccesosHoraTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsAccHoraATrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_HORA FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_USUARIOS_DIA_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_DIA FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los Accesos a trasladar de FSGS y traspaso lo pongo a 1
                dsAccDiaATrasladar = oRoot.ObtenerAccesosDiaFSGS()
                dtIDs = ObtenerTablaIDS(dsAccDiaATrasladar.Tables(0))
                If dsAccDiaATrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsAccDiaATrasladar.Tables(0).Rows(0).Item("ENTORNO").ToString()
                    'g_sPyme = dsAccDiaATrasladar.Tables(0).Rows(0).Item("PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceAccesosDia(dsAccDiaATrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado accesos usuarios dia FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.AccesosDiaTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsAccDiaATrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_DIA FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_USUARIOS_SEMANA_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_SEMANA FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los Accesos a trasladar de FSGS y traspaso lo pongo a 1
                dsAccSemanaATrasladar = oRoot.ObtenerAccesosSemanaFSGS()
                dtIDs = ObtenerTablaIDS(dsAccSemanaATrasladar.Tables(0))
                If dsAccSemanaATrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsAccSemanaATrasladar.Tables(0).Rows(0).Item("ENTORNO").ToString()
                    'g_sPyme = dsAccSemanaATrasladar.Tables(0).Rows(0).Item("PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceAccesosSemana(dsAccSemanaATrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado accesos usuarios semana FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.AccesosSemanaTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsAccSemanaATrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_SEMANA FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_USUARIOS_MES_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_MES FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los Accesos a trasladar de FSGS y traspaso lo pongo a 1
                dsAccMesTrasladar = oRoot.ObtenerAccesosMesFSGS()
                dtIDs = ObtenerTablaIDS(dsAccMesTrasladar.Tables(0))
                If dsAccMesTrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsAccMesTrasladar.Tables(0).Rows(0).Item("ENTORNO").ToString()
                    'g_sPyme = dsAccMesTrasladar.Tables(0).Rows(0).Item("PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceAccesosMes(dsAccMesTrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado accesos usuarios mes FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.AccesosMesTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsAccMesTrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_MES FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_USUARIOS_AYNO_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_ANYO FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los Accesos a trasladar de FSGS y traspaso lo pongo a 1
                dsAccAnyoTrasladar = oRoot.ObtenerAccesosAnyoFSGS()
                dtIDs = ObtenerTablaIDS(dsAccAnyoTrasladar.Tables(0))
                If dsAccAnyoTrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsAccAnyoTrasladar.Tables(0).Rows(0).Item("ENTORNO").ToString()
                    'g_sPyme = dsAccAnyoTrasladar.Tables(0).Rows(0).Item("PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceAccesosAnyo(dsAccAnyoTrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado accesos usuarios anyo FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.AccesosAnyoTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsAccAnyoTrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_ANYO FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_USUARIOS_DIS_SEMANA_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_DIS_SEMANA FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los Accesos a trasladar de FSGS y traspaso lo pongo a 1
                dsAccDisSemanaTrasladar = oRoot.ObtenerAccesosDisSemanaFSGS()
                dtIDs = ObtenerTablaIDS(dsAccDisSemanaTrasladar.Tables(0))
                If dsAccDisSemanaTrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsAccDisSemanaTrasladar.Tables(0).Rows(0).Item("ENTORNO").ToString()
                    'g_sPyme = dsAccDisSemanaTrasladar.Tables(0).Rows(0).Item("PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceAccesosDisSemana(dsAccDisSemanaTrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado accesos usuarios distintos semana FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.AccesosDisSemanaTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsAccDisSemanaTrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_SEMANA_DIS FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_USUARIOS_DIS_MES_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_DIS_MES FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los Accesos a trasladar de FSGS y traspaso lo pongo a 1
                dsAccDisMesTrasladar = oRoot.ObtenerAccesosDisMesFSGS()
                dtIDs = ObtenerTablaIDS(dsAccDisMesTrasladar.Tables(0))
                If dsAccDisMesTrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsAccDisMesTrasladar.Tables(0).Rows(0).Item("ENTORNO").ToString()
                    'g_sPyme = dsAccDisMesTrasladar.Tables(0).Rows(0).Item("PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceAccesosDisMes(dsAccDisMesTrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado accesos usuarios distintos mes FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.AccesosDisMesTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsAccDisMesTrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_DIS_MES FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_USUARIOS_DIS_ANYO_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_DIS_ANYO FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los Accesos a trasladar de FSGS y traspaso lo pongo a 1
                dsAccDisAnyoTrasladar = oRoot.ObtenerAccesosDisAnyoFSGS()
                dtIDs = ObtenerTablaIDS(dsAccDisAnyoTrasladar.Tables(0))
                If dsAccDisAnyoTrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsAccDisAnyoTrasladar.Tables(0).Rows(0).Item("ENTORNO").ToString()
                    'g_sPyme = dsAccDisAnyoTrasladar.Tables(0).Rows(0).Item("PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceAccesosDisAnyo(dsAccDisAnyoTrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado accesos usuarios distintos anyo FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.AccesosDisAnyoTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsAccDisAnyoTrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE ACCESOS_USUARIOS_DIS_ANYO FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_ERRORES_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE REGISTROS DE ERRORES FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los errores a trasladar de FSGS y traspaso lo pongo a 1
                dsErroresATrasladar = oRoot.ObtenerErroresFSGS()
                dtIDs = ObtenerTablaIDS(dsErroresATrasladar.Tables(0))
                If dsErroresATrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsErroresATrasladar.Tables(0).Rows(0).Item("FSAL_ENTORNO").ToString()
                    'g_sPyme = dsErroresATrasladar.Tables(0).Rows(0).Item("FSAL_PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceErrores(dsErroresATrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado errores FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.ErroresTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsErroresATrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE ERRORES FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_KPIS_DIS_MINUTO_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE KPIS_DIS_MINUTO DE FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los kpis a trasladar de FSGS y traspaso lo pongo a 1
                dsKPIsDisMinATrasladar = oRoot.ObtenerKPIsDisMinFSGS()
                dtIDs = ObtenerTablaIDS(dsKPIsDisMinATrasladar.Tables(0))
                If dsKPIsDisMinATrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsKPIsDisMinATrasladar.Tables(0).Rows(0).Item("ENTORNO").ToString()
                    'g_sPyme = dsKPIsDisMinATrasladar.Tables(0).Rows(0).Item("PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceKPIsDisMinuto(dsKPIsDisMinATrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado KPIs distintos minuto FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.KPIsDisMinTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsKPIsDisMinATrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE KPIS_DIS_MINUTO FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_KPIS_DIS_HORA_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE KPIS_DIS_HORA DE FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los kpis a trasladar de FSGS y traspaso lo pongo a 1
                dsKPIsDisHoraATrasladar = oRoot.ObtenerKPIsDisHoraFSGS()
                dtIDs = ObtenerTablaIDS(dsKPIsDisHoraATrasladar.Tables(0))
                If dsKPIsDisHoraATrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsKPIsDisHoraATrasladar.Tables(0).Rows(0).Item("ENTORNO").ToString()
                    'g_sPyme = dsKPIsDisHoraATrasladar.Tables(0).Rows(0).Item("PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceKPIsDisHora(dsKPIsDisHoraATrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado KPIs distintos hora FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.KPIsDisHoraTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsKPIsDisHoraATrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE KPIS_DIS_HORA FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_KPIS_DIS_DIA_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE KPIS_DIS_DIA DE FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los kpis a trasladar de FSGS y traspaso lo pongo a 1
                dsKPIsDisDiaATrasladar = oRoot.ObtenerKPIsDisDiaFSGS()
                dtIDs = ObtenerTablaIDS(dsKPIsDisDiaATrasladar.Tables(0))
                If dsKPIsDisDiaATrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsKPIsDisDiaATrasladar.Tables(0).Rows(0).Item("ENTORNO").ToString()
                    'g_sPyme = dsKPIsDisDiaATrasladar.Tables(0).Rows(0).Item("PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceKPIsDisDia(dsKPIsDisDiaATrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado KPIs distintos dia FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.KPIsDisDiaTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsKPIsDisDiaATrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE KPIS_DIS_DIA FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_KPIS_DIS_SEMANA_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE KPIS_DIS_SEMANA DE FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los kpis a trasladar de FSGS y traspaso lo pongo a 1
                dsKPIsDisSemanaATrasladar = oRoot.ObtenerKPIsDisSemanaFSGS()
                dtIDs = ObtenerTablaIDS(dsKPIsDisSemanaATrasladar.Tables(0))
                If dsKPIsDisSemanaATrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsKPIsDisSemanaATrasladar.Tables(0).Rows(0).Item("ENTORNO").ToString()
                    'g_sPyme = dsKPIsDisSemanaATrasladar.Tables(0).Rows(0).Item("PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceKPIsDisSemana(dsKPIsDisSemanaATrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado KPIs distintos semana FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.KPIsDisSemanaTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsKPIsDisSemanaATrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE KPIS_DIS_SEMANA FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_KPIS_DIS_MES_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE KPIS_DIS_MES DE FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los kpis a trasladar de FSGS y traspaso lo pongo a 1
                dsKPIsDisMesATrasladar = oRoot.ObtenerKPIsDisMesFSGS()
                dtIDs = ObtenerTablaIDS(dsKPIsDisMesATrasladar.Tables(0))
                If dsKPIsDisMesATrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsKPIsDisMesATrasladar.Tables(0).Rows(0).Item("ENTORNO").ToString()
                    'g_sPyme = dsKPIsDisMesATrasladar.Tables(0).Rows(0).Item("PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceKPIsDisMes(dsKPIsDisMesATrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado KPIs distintos mes FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.KPIsDisMesTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsKPIsDisMesATrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE KPIS_DIS_MES FSGS
        End If

        iRegistrosATrasladar = c_iRegistrosATrasladar
        iMaxIntentosTraslado = c_iMaxIntentosTraslado

        If ConfigurationManager.AppSettings("TRASLADO_FSAL_KPIS_DIS_ANYO_A_CORPORATE") = 1 Then
            'INICIO TRATAMIENTO DE KPIS_DIS_ANYO DE FSGS
            Do Until (iRegistrosATrasladar < c_iRegistrosATrasladar) Or (iMaxIntentosTraslado = 0)
                ''''Obtengo los kpis a trasladar de FSGS y traspaso lo pongo a 1
                dsKPIsDisAnyoATrasladar = oRoot.ObtenerKPIsDisAnyoFSGS()
                dtIDs = ObtenerTablaIDS(dsKPIsDisAnyoATrasladar.Tables(0))
                If dsKPIsDisAnyoATrasladar.Tables(0).Rows.Count > 0 Then

                    'g_sEntorno = dsKPIsDisAnyoATrasladar.Tables(0).Rows(0).Item("ENTORNO").ToString()
                    'g_sPyme = dsKPIsDisAnyoATrasladar.Tables(0).Rows(0).Item("PYME").ToString()

                    Try
                        iRes = webServiceFsal.FSALWebServiceKPIsDisAnyo(dsKPIsDisAnyoATrasladar.Tables(0))
                    Catch ex As Exception
                        EscribirError("Error traslado KPIs distintos anyo FSGS " & g_sEntorno & " " & g_sPyme & ": " & ex.Message)
                        iRes = 1
                    End Try

                    oRoot.KPIsDisAnyoTratadosFSGS(dtIDs, g_iLogEliminar, iRes)

                End If
                iRegistrosATrasladar = dsKPIsDisAnyoATrasladar.Tables(0).Rows.Count
                iMaxIntentosTraslado -= 1
            Loop
            'FIN TRATAMIENTO DE REGISTROS DE KPIS_DIS_ANYO FSGS
        End If

    End Sub

    ''' <summary>
    ''' Comprueba el valor de las variables configuradas y hace el traspaso de datos a las tablas correspondientes y el borrado de estos de las tablas actuales.
    ''' </summary>
    Private Sub TrasladoAHistoricosBorradoEnOrigenFSAL()
        Dim periodoDiasAccesos, periodoDiasEventos, periodoDiasErrores, guardarAntesDeBorrar As Integer
        Dim oRoot As New root

        guardarAntesDeBorrar = ConfigurationManager.AppSettings("GUARDAR_ANTES_BORRAR_FSAL")
        periodoDiasAccesos = ConfigurationManager.AppSettings("BORRAR_ACCESOS_PERIODO_FSAL")
        periodoDiasEventos = ConfigurationManager.AppSettings("BORRAR_EVENTOS_PERIODO_FSAL")
        periodoDiasErrores = ConfigurationManager.AppSettings("BORRAR_ERRORES_PERIODO_FSAL")


        oRoot.BackOBorradoHistAccesosFSAL(periodoDiasAccesos, guardarAntesDeBorrar)
        oRoot.BackOBorradoHistEventosFSAL(periodoDiasEventos, guardarAntesDeBorrar)
        oRoot.BackOBorradoHistErroresFSAL(periodoDiasErrores, guardarAntesDeBorrar)

    End Sub

    Private Sub LeerParametrosEjecucionServicioFSAL()
        Dim oRoot As New root
        Dim data As DataSet
        Dim sPath As String
        Dim dr1 As DataSet
        Dim dr2 As DataSet

        Try

            data = oRoot.CargaParametrosFSAL()
            g_iLogEliminar = DBNullToInteger(data.Tables(0).Rows(0).Item("LOGELIMINAR"))

            sPath = ConfigurationManager.AppSettings("Path")
            m_sFile_FSAL = sPath & "\ServicioFSAL(" & ObtenerNombreServicio() & ").log"

            If ConfigurationManager.AppSettings("FSAL_ENTORNO") = "" Then
                dr1 = oRoot.ObtenerEntorno()
                g_sEntorno = dr1.Tables(0).Rows(0).Item(0).ToString()
            Else
                g_sEntorno = ConfigurationManager.AppSettings("FSAL_ENTORNO")
            End If

            Try
                If ConfigurationManager.AppSettings("FSAL_PYME") = "" Then
                    dr2 = oRoot.ObtenerPyme()
                    g_sPyme = dr2.Tables(0).Rows(0).Item(0)
                Else
                    g_sPyme = ConfigurationManager.AppSettings("FSAL_PYME")
                End If
            Catch ex As Exception
                g_sPyme = ""
            End Try

            If ConfigurationManager.AppSettings("MODO_EJECUCION_FSAL") = "" _
                OrElse ConfigurationManager.AppSettings("PERIODO_FSAL") = "" _
                OrElse Not IsNumeric(ConfigurationManager.AppSettings("PERIODO_FSAL")) Then
                ActivoServicioFSAL = False ' Se desactiva para que no se ejecute
            Else
                ''Configuracion del modo de ejecucion del servicio.
                ''Toma los parametros de la configuracion.
                m_modoEjecucion_FSAL = ConfigurationManager.AppSettings("MODO_EJECUCION_FSAL")
                m_Periodo_FSAL = ConfigurationManager.AppSettings("PERIODO_FSAL")
                m_hora_FSAL = IIf(ConfigurationManager.AppSettings("HORA_FSAL") = "", "00:00", ConfigurationManager.AppSettings("HORA_FSAL"))
                m_dia_FSAL = IIf(Not IsNumeric(ConfigurationManager.AppSettings("DIA_FSAL")), 1, ConfigurationManager.AppSettings("DIA_FSAL"))
                Select Case m_modoEjecucion_FSAL
                    Case "C", "D", "S", "M"
                        ActivoServicioFSAL = True
                    Case Else
                        ActivoServicioFSAL = False ' Se desactiva para que no se ejecute
                End Select
            End If

        Catch e As Exception
            Dim ex As New Exception("Sub LeerParametrosEjecucionServicioFSAL(): " & e.Message)
            Throw (ex)
        End Try

    End Sub

    ''' <summary>
    ''' Llena las tablas de accesos por minuto, hora, dia, semana, mes y anyo a partir de la de accesos.
    ''' </summary>
    ''' <param name="fecha"></param>
    ''' <remarks></remarks>
    Private Sub CrearEstadisticasAccesosFSAL(ByVal fecha As Date)
        Dim oRoot As New root
        oRoot.CrearEstadisticasAccesosFSAL(fecha, g_sEntorno, g_sPyme)
    End Sub

    ''' <summary>
    ''' Traslada a FSAL_EVENTOS ciertos eventos identificados.
    ''' </summary>
    ''' <param name="fecha"></param>
    ''' <remarks></remarks>
    Private Sub TrasladarEventosFSAL(ByVal fecha As Date)
        Dim oRoot As New root
        oRoot.TransferirEventosFSAL(fecha, g_sEntorno, g_sPyme)
    End Sub

    ''' <summary>
    ''' Llena la tabla de FSAL_EVENTOS_ESTADISTICAS a partir de la de eventos.
    ''' </summary>
    ''' <param name="fecha"></param>
    ''' <remarks></remarks>
    Private Sub CrearEstadisticasEventosFSAL(ByVal fecha As Date)
        Dim oRoot As New root
        oRoot.CrearEstadisticasEventosFSAL(fecha, g_sEntorno, g_sPyme)
    End Sub

    ''' <summary>
    ''' Traslada a FSAL_ERRORES los errores registrados.
    ''' </summary>
    ''' <param name="fecha"></param>
    ''' <remarks></remarks>
    Private Sub TrasladarErroresFSAL(ByVal fecha As Date)
        Dim oRoot As New root
        oRoot.CrearEstadisticasErroresFSAL(fecha, g_sEntorno, g_sPyme)
    End Sub

    ''' <summary>
    ''' Cataloga los accesos por kpi en la tabla accesos.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CatalogarAccesosKPIsFSAL()
        Dim oRoot As New root
        oRoot.CatalogarKPIS()
    End Sub

    ''' <summary>
    ''' Llena las tablas de kpis por minuto, hora, dia, semana, mes y anyo a partir de la tabla accesos.
    ''' </summary>
    ''' <param name="fecha"></param>
    ''' <remarks></remarks>
    Private Sub CrearEstadisticasKPIsFSAL(ByVal fecha As Date)
        Dim oRoot As New root
        oRoot.CrearEstadisticasKPIsFSAL(fecha, g_sEntorno, g_sPyme)
    End Sub

    Public Function DBNullToStr(Optional ByVal value As Object = Nothing) As String
        If IsDBNull(value) Then
            Return ""
        Else
            Return value
        End If
    End Function

    ''' <summary>
    ''' Tratamos de convertir el objeto recibido a Integer
    ''' </summary>
    ''' <param name="value">Valor a convertir a Integer</param>
    ''' <returns>Valor convertido a Integer. Si no es posible o el value es null o nothing, devuelve 0</returns>
    ''' <remarks>Llamado desde toda la aplicaciÃ³n, donde se requiera su uso. Tiempo mÃ¡x inferior a 0,1 seg</remarks>
    Public Function DBNullToInteger(Optional ByVal value As Object = Nothing) As Integer
        If IsDBNull(value) Then
            Return 0
        ElseIf value Is Nothing Then
            Return 0
        Else
            Return CInt(value)
        End If
    End Function

    ''' <summary>
    ''' Obtiene el nombre del servicio a partir del license que tiene el servicio
    ''' </summary>
    ''' <returns>Devuelve string con Nombre del servicio</returns>
    ''' <remarks>Llamada desde=Propia pagina; Tiempo máximo=0,1seg.</remarks>
    Private Function ObtenerNombreServicio() As String
        Dim sNombre As String = ""
        Dim doc As New XmlDocument

        Try
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory() & "FSALPortalWinService.exe.config")
        Catch
            Err.Raise(10000, , System.AppDomain.CurrentDomain.BaseDirectory() & "FSALPortalWinService.exe.config file missing!")
        End Try

        Dim nodeList As XmlNodeList
        nodeList = doc.SelectNodes("/configuration/appSettings")

        Dim title As XmlNode

        Dim str As String
        Dim posiInicial, posi1, posi2 As Integer

        For Each title In nodeList
            str = title.InnerXml
            posiInicial = InStr(str, "NombreServicio")
            If posiInicial > 0 Then
                posi1 = InStr(posiInicial, str, "value")
                If posi1 > 0 Then
                    posi1 = posi1 + 7
                    posi2 = InStr(posi1, str, "/>") - 2
                    sNombre = Mid(str, posi1, (posi2 - posi1))

                End If

            End If
        Next

        If sNombre = "" Then
            Err.Raise(10000, , "Corrupted Config file !")
        End If

        doc = Nothing

        Return sNombre

    End Function

End Class