﻿Option Explicit On
Option Strict On

Imports System.Runtime.InteropServices

Public Class Encrypter
    Public Enum TipoDeUsuario
        Administrador = 1
        Persona = 2
    End Enum
    Private Const ClaveSQLtod As String = "aldjÃ±w"
    Private Const ClaveADMpar As String = "pruiop"
    Private Const ClaveADMimp As String = "ljkdag"
    Private Const ClavePar As String = "agkagÂ´"
    Private Const ClaveImpar As String = "h+hlL_"

    Public Shared Function EncriptarAcceso(ByVal sCadena As String, ByVal sDato As String, ByVal Encriptando As Boolean, Optional ByVal Fecha As Date = #3/5/1974#) As String
        If sDato = "" Then Return ""

        Dim Crypt As New FSNCrypt.FSNAES
        Dim sClave As String

        If Len(sCadena) Mod 2 = 0 Then
            sClave = ClavePar
        Else
            sClave = ClaveImpar
        End If

        If Encriptando Then
            EncriptarAcceso = Crypt.EncryptStringToString(Fecha, sClave, sDato)
        Else
            EncriptarAcceso = Crypt.DecryptStringFromString(Fecha, sClave, sDato)
        End If

        Crypt = Nothing
    End Function

    Public Shared Function Encrypt(ByVal Data As String, Optional ByVal Usu As String = "", Optional ByVal Encrypting As Boolean = True, Optional ByVal UserType As TipoDeUsuario = TipoDeUsuario.Persona, Optional ByVal Fecha As Date = #3/5/1974#) As String
        Dim Crypt As New FSNCrypt.FSNAES
        Dim sKeyString As String
        Dim diacrypt As Integer
        Dim sFecha As String

        sFecha = Format(Fecha, "dd/MM/yyyy HH\:mm\:ss")
        diacrypt = Fecha.Day

        If diacrypt Mod 2 = 0 Then
            If UserType = TipoDeUsuario.Administrador Then
                sKeyString = ClaveADMpar
            Else
                sKeyString = ClavePar
            End If
        Else
            If UserType = TipoDeUsuario.Administrador Then
                sKeyString = ClaveADMimp
            Else
                sKeyString = ClaveImpar
            End If
        End If

        If Encrypting Then
            Encrypt = Crypt.EncryptStringToString(Fecha, Usu & sKeyString, Data)
        Else
            Encrypt = Crypt.DecryptStringFromString(Fecha, Usu & sKeyString, Data)
        End If

        Crypt = Nothing
    End Function
End Class


