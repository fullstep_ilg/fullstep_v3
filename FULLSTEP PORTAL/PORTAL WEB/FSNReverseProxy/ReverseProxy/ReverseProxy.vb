﻿Imports System.Configuration
Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Web
Imports System.Diagnostics
Imports System.Xml
Imports System.Data.SqlClient

Namespace FSNReverseProxy
    ''' <summary>
    ''' Handler all Client's requests and deliver the web site
    ''' </summary>
    Public Class ReverseProxy
        Implements IHttpHandler
        Implements System.Web.SessionState.IRequiresSessionState
#Region "Propiedades Conexión a BD/Authenticate"
        Private mDBLogin As String = ""
        ReadOnly Property DBLogin() As String
            Get
                Return mDBLogin
            End Get
        End Property
        Private mDBPassword As String = ""
        ReadOnly Property DBPassword() As String
            Get
                Return mDBPassword
            End Get
        End Property
        Private mDBName As String = ""
        ReadOnly Property DBName() As String
            Get
                Return mDBName
            End Get
        End Property
        Private mDBServer As String = ""
        ReadOnly Property DBServer() As String
            Get
                Return mDBServer
            End Get
        End Property
        'La he tenido que añadir porque en CFacturae.vb se necesita para acceder a BD desde la CAPA DE negocio ¿?¿?¿?¿?¿?¿?
        Private mDBConnection As String = ""
        ReadOnly Property DBConnection() As String
            Get
                Return mDBConnection
            End Get
        End Property
        Private _user As String
        Public Property User() As String
            Get
                Return _user
            End Get
            Set(ByVal value As String)
                _user = value
            End Set
        End Property
        Private _password As String
        Public Property Password() As String
            Get
                Return _password
            End Get
            Set(ByVal value As String)
                _password = value
            End Set
        End Property
#End Region
#Region "Logica"
        Public Property LoggedUserSession() As Boolean
            Get
                If HttpContext.Current.Session("LoggedUserSession") Is Nothing Then
                    Return False
                Else
                    Return CType(HttpContext.Current.Session("LoggedUserSession"), Boolean)
                End If
            End Get
            Set(ByVal value As Boolean)
                HttpContext.Current.Session("LoggedUserSession") = value
            End Set
        End Property
        Public Property First() As Boolean
            Get
                If HttpContext.Current.Session("First") Is Nothing Then
                    Return True
                Else
                    Return CType(HttpContext.Current.Session("First"), Boolean)
                End If
            End Get
            Set(ByVal value As Boolean)
                HttpContext.Current.Session("First") = value
            End Set
        End Property

        ''' <summary>
        ''' Method calls when client request the server
        ''' </summary>
        ''' <param name="context">HTTP context for client</param>
        Public Sub ProcessRequest(context As HttpContext) Implements IHttpHandler.ProcessRequest
            ' *************              ***********                **********
            ' *           *   ------->   * Reverse *   --------->   * Remote *
            ' * Navigator *              *  Proxy  *                * Server *
            ' *           *   <-------   *         *   <---------   *        *
            ' *************              ***********                **********
            If LoggedUser() Then
                If Not User = "" AndAlso Not Password = "" Then
                    ' Create a connexion to the Remote Server to redirect all requests                
                    Dim server As New RemoteServer(context, User, Password)

                    ' Create a request with same data in navigator request
                    Dim request As HttpWebRequest
                    request = server.GetRequest()

                    ' Send the request to the remote server and return the response
                    Dim response As HttpWebResponse = server.GetResponse(request)
                    Dim responseData As Byte() = server.GetResponseStreamBytes(response)

                    ' Handle cookies to navigator
                    server.SetContextCookies(response)

                    ' Send the response to client
                    context.Response.ContentEncoding = Encoding.UTF8
                    context.Response.ContentType = response.ContentType
                    context.Response.OutputStream.Write(responseData, 0, responseData.Length)

                    ' Close streams            
                    response.Close()
                    context.Response.[End]()
                Else
                    'La llamada se ha realizado desde el FSPM
                    'HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutanormal") & "/../../../", False)
                    HttpContext.Current.Response.StatusCode = 404
                    HttpContext.Current.Response.StatusDescription = "User not logged"
                    HttpContext.Current.Response.Write("<h1>User not logged</h1>")
                    HttpContext.Current.Response.[End]()
                End If
            Else
                'La llamada se ha realizado desde el FSPM
                'HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings("rutanormal") & "/../../../", False)
                HttpContext.Current.Response.StatusCode = 404
                HttpContext.Current.Response.StatusDescription = "User not logged"
                HttpContext.Current.Response.Write("<h1>User not logged</h1>")
                HttpContext.Current.Response.[End]()
            End If
        End Sub
        Private Function LoggedUser() As Boolean
            If HttpContext.Current.Request.Cookies("USU_SESIONID") Is Nothing Then
                LoggedUserSession = False
            Else
                Dim SessionID As String = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies("USU_SESIONID").Value)
                BDConecction()
                Dim cn As New SqlConnection(mDBConnection)
                Dim cm As SqlCommand
                Dim ds As DataSet
                Dim da As SqlDataAdapter
                Try
                    cn.Open()
                    ds = New DataSet
                    cm = New SqlCommand
                    cm.Connection = cn
                    cm.CommandType = CommandType.StoredProcedure
                    cm.CommandText = "FSP_JDE_LOGIN"
                    cm.Parameters.AddWithValue("@SESIONID", SessionID)

                    da = New SqlDataAdapter
                    da.SelectCommand = cm
                    da.Fill(ds)

                    If ds.Tables(0).Rows.Count = 1 Then
                        _user = ds.Tables(0).Rows(0)("COD").ToString
                        _password = ds.Tables(0).Rows(0)("PWD").ToString
                        LoggedUserSession = True
                    Else
                        LoggedUserSession = False
                    End If
                Catch ex As SqlException
                    Return False
                Finally
                    cn.Close()
                    cn.Dispose()
                    cm.Dispose()
                    ds.Dispose()
                End Try
                LoggedUserSession = True
            End If
            Return LoggedUserSession
        End Function
        Private Sub BDConecction()
            'Comprobar tema de licencias y cargar datos de conexión
            Dim sDB As String = ConfigurationManager.AppSettings("DB:FS_WS")
            If sDB Is Nothing Then
                Err.Raise(10000, , "Configuration file corrupted. Missing FS_WS entry !")
            End If
            'Tenemos que extraer el login y la contraseña de conexión.
            Dim doc As New XmlDocument
            Dim sdblogin As String = ""
            Dim sdbPassword As String = ""
            Try
                doc.Load(System.AppDomain.CurrentDomain.BaseDirectory() & "License.xml")
            Catch
                Err.Raise(10000, , "LICENSE file missing!")
            End Try
            Dim child As XmlNode
            child = doc.SelectSingleNode("/LICENSE/DB_LOGIN")
            If Not (child Is Nothing) Then
                mDBLogin = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
                'mDBLogin = child.InnerText
            End If
            child = Nothing
            child = doc.SelectSingleNode("/LICENSE/DB_PASSWORD")
            If Not (child Is Nothing) Then
                mDBPassword = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
                'mDBPassword = child.InnerText
            Else
                mDBPassword = ""
            End If
            child = doc.SelectSingleNode("/LICENSE/DB_NAME")
            If Not (child Is Nothing) Then
                mDBName = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
            End If
            child = Nothing
            child = doc.SelectSingleNode("/LICENSE/DB_SERVER")
            If Not (child Is Nothing) Then
                mDBServer = Encrypter.Encrypt(child.InnerText, , False, Encrypter.TipoDeUsuario.Administrador)
            End If

            'PENDIENTE METER ENCRIPTACIÓN
            sDB = sDB.Replace("DB_LOGIN", mDBLogin)
            sDB = sDB.Replace("DB_PASSWORD", mDBPassword)
            sDB = sDB.Replace("DB_NAME", mDBName)
            sDB = sDB.Replace("DB_SERVER", mDBServer)
            doc = Nothing
            mDBConnection = sDB
        End Sub
#End Region
#Region "IHttpHandler Members"

        Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
            Get
                Return True
            End Get
        End Property

#End Region
    End Class
End Namespace
