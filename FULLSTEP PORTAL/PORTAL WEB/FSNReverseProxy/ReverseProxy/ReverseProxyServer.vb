﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Web
Imports System.Configuration
Imports System.Net
Imports System.IO

Namespace FSNReverseProxy
    ''' <summary>
    ''' Manage communication between the proxy server and the remote server
    ''' </summary>
    Friend Class RemoteServer
        Private _remoteUrl As String
        Private _context As HttpContext

        ''' <summary>
        ''' Initialize the communication with the Remote Server
        ''' </summary>
        ''' <param name="context">Context</param>
        Public Sub New(context As HttpContext, ByVal User As String, ByVal Password As String)
            _context = context

            ' Convert the URL received from navigator to URL for server
            Dim serverUrl As String = ConfigurationManager.AppSettings("RemoteWebSite")
            _remoteUrl = context.Request.Url.AbsoluteUri.Replace("http://" & context.Request.Url.Host, serverUrl)
            _remoteUrl = _remoteUrl.Replace("XXXXX", User)
            _remoteUrl = _remoteUrl.Replace("YYYYY", Password)
        End Sub
        ''' <summary>
        ''' Return address to communicate to the remote server
        ''' </summary>
        Public ReadOnly Property RemoteUrl() As String
            Get
                Return _remoteUrl
            End Get
        End Property
        ''' <summary>
        ''' Create a request the remote server
        ''' </summary>
        ''' <returns>Request to send to the server</returns>
        Public Function GetRequest() As HttpWebRequest
            Dim cookieContainer As New CookieContainer()
            ' Create a request to the server            
            Dim request As HttpWebRequest = DirectCast(WebRequest.Create(_remoteUrl), HttpWebRequest)

            ' Set some options
            request.Method = _context.Request.HttpMethod
            request.UserAgent = _context.Request.UserAgent
            request.KeepAlive = True
            request.CookieContainer = cookieContainer

            ' Send Cookie extracted from the incoming request
            For i As Integer = 0 To _context.Request.Cookies.Count - 1
                Dim navigatorCookie As HttpCookie = _context.Request.Cookies(i)

                Dim c As Cookie
                If InStr(navigatorCookie.Value, ",") = 0 Then
                    c = New Cookie(navigatorCookie.Name, navigatorCookie.Value)
                Else
                    Try
                        c = New Cookie(navigatorCookie.Name, HttpUtility.UrlEncode(","))
                    Catch ex As Exception
                        Try
                            c = New Cookie(navigatorCookie.Name, HttpContext.Current.Server.UrlEncode(","))
                        Catch ex2 As Exception
                            c = New Cookie(navigatorCookie.Name, "")
                        End Try
                    End Try
                End If

                c.Domain = New Uri(_remoteUrl).Host
                c.Expires = navigatorCookie.Expires
                c.HttpOnly = navigatorCookie.HttpOnly
                c.Path = navigatorCookie.Path
                c.Secure = navigatorCookie.Secure
                cookieContainer.Add(c)
            Next

            ' For POST, write the post data extracted from the incoming request
            If request.Method = "POST" Then
                Dim clientStream As Stream = _context.Request.InputStream
                Dim clientPostData As Byte() = New Byte(_context.Request.InputStream.Length - 1) {}
                clientStream.Read(clientPostData, 0, CInt(_context.Request.InputStream.Length))

                request.ContentType = _context.Request.ContentType
                request.ContentLength = clientPostData.Length
                Dim stream As Stream = request.GetRequestStream()
                stream.Write(clientPostData, 0, clientPostData.Length)
                stream.Close()
            End If

            Return request
        End Function
        ''' <summary>
        ''' Send the request to the remote server and return the response
        ''' </summary>
        ''' <param name="request">Request to send to the server</param>
        ''' <returns>Response received from the remote server or null if page not found</returns>
        Public Function GetResponse(request As HttpWebRequest) As HttpWebResponse
            Dim response As HttpWebResponse
            Try
                response = DirectCast(request.GetResponse(), HttpWebResponse)
            Catch generatedExceptionName As System.Net.WebException
                ' Send 404 to client 
                _context.Response.StatusCode = 404
                _context.Response.StatusDescription = "Page Not Found"
                _context.Response.Write("<h1>Page not found. " & request.Address.AbsoluteUri & "</h1>")
                _context.Response.[End]()
                Return Nothing
            End Try

            Return response
        End Function
        ''' <summary>
        ''' Return the response in bytes array format
        ''' </summary>
        ''' <param name="response">Response received from the remote server</param>
        ''' <returns></returns>
        Public Function GetResponseStreamBytes(response As HttpWebResponse) As Byte()
            Dim bufferSize As Integer = 256
            Dim buffer As Byte() = New Byte(bufferSize - 1) {}
            Dim responseStream As Stream
            Dim memoryStream As New MemoryStream()
            Dim remoteResponseCount As Integer
            Dim responseData As Byte()

            responseStream = response.GetResponseStream()
            remoteResponseCount = responseStream.Read(buffer, 0, bufferSize)

            While remoteResponseCount > 0
                memoryStream.Write(buffer, 0, remoteResponseCount)
                remoteResponseCount = responseStream.Read(buffer, 0, bufferSize)
            End While

            responseData = memoryStream.ToArray()

            memoryStream.Close()
            responseStream.Close()

            memoryStream.Dispose()
            responseStream.Dispose()

            Return responseData
        End Function
        ''' <summary>
        ''' Set cookies received from remote server to response of navigator
        ''' </summary>
        ''' <param name="response">Response received from the remote server</param>
        Public Sub SetContextCookies(response As HttpWebResponse)
            _context.Response.Cookies.Clear()

            For Each receivedCookie As Cookie In response.Cookies

                Dim c As HttpCookie
                If InStr(receivedCookie.Value, ",") = 0 Then
                    c = New HttpCookie(receivedCookie.Name, receivedCookie.Value)
                Else
                    Try
                        c = New HttpCookie(receivedCookie.Name, HttpUtility.UrlEncode(","))
                    Catch ex As Exception
                        Try
                            c = New HttpCookie(receivedCookie.Name, HttpContext.Current.Server.UrlEncode(","))
                        Catch ex2 As Exception
                            c = New HttpCookie(receivedCookie.Name, "")
                        End Try
                    End Try
                End If

                c.Domain = _context.Request.Url.Host
                c.Expires = receivedCookie.Expires
                c.HttpOnly = receivedCookie.HttpOnly
                c.Path = receivedCookie.Path
                c.Secure = receivedCookie.Secure
                _context.Response.Cookies.Add(c)
            Next
        End Sub
    End Class
End Namespace