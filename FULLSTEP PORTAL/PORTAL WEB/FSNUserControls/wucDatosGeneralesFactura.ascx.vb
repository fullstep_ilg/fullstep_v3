﻿Imports Infragistics.Web.UI.GridControls
Imports Fullstep.FSNLibrary

Public Class wucDatosGeneralesFactura
    Inherits System.Web.UI.UserControl

#Region " PROPIEDADES "
    Private oTextos As DataTable
    Public WriteOnly Property Textos() As DataTable
        Set(ByVal Value As DataTable)
            oTextos = Value
        End Set
    End Property

    Private m_sProveedor As String
    Public WriteOnly Property Proveedor() As String
        Set(ByVal Value As String)
            m_sProveedor = Value
        End Set
    End Property

    Private m_sCodProveedor As String
    Public WriteOnly Property CodProveedor() As String
        Set(ByVal Value As String)
            m_sCodProveedor = Value
        End Set
    End Property

    Private m_sEmpresa As String
    Public WriteOnly Property Empresa() As String
        Set(ByVal Value As String)
            m_sEmpresa = Value
        End Set
    End Property

    Private m_lIdEmpresa As Long
    Public WriteOnly Property IdEmpresa() As Long
        Set(ByVal Value As Long)
            m_lIdEmpresa = Value
        End Set
    End Property

    Private m_dImporte As Double
    Public WriteOnly Property Importe() As Double
        Set(ByVal Value As Double)
            m_dImporte = Value
        End Set
    End Property

    Private m_dImporteBruto As Double
    Public WriteOnly Property ImporteBruto() As Double
        Set(ByVal Value As Double)
            m_dImporteBruto = Value
        End Set
    End Property

    Private m_sNumeroFactura As String
    Public WriteOnly Property NumeroFactura() As String
        Set(ByVal Value As String)
            m_sNumeroFactura = Value
        End Set
    End Property

    Private m_dFechaFactura As Date
    Public WriteOnly Property FechaFactura() As Date
        Set(ByVal Value As Date)
            m_dFechaFactura = Value
        End Set
    End Property

    Private m_dRetencionGarantia As Double
    Public WriteOnly Property RetencionGarantia() As Double
        Set(ByVal Value As Double)
            m_dRetencionGarantia = Value
        End Set
    End Property

    Private m_sNumeroERP As String
    Public WriteOnly Property NumeroERP() As String
        Set(ByVal Value As String)
            m_sNumeroERP = Value
        End Set
    End Property

    Private m_dFechaContabilizacion As Date
    Public WriteOnly Property FechaContabilizacion() As Date
        Set(ByVal Value As Date)
            m_dFechaContabilizacion = Value
        End Set
    End Property

    Private m_dCostes As Double
    ''' <summary>
    ''' Total costes Generales de la factura
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property Costes() As Double
        Set(ByVal Value As Double)
            m_dCostes = Value
        End Set
    End Property

    Private m_sCodFormaPago As String
    Public WriteOnly Property CodFormaPago() As String
        Set(ByVal Value As String)
            m_sCodFormaPago = Value
        End Set
    End Property

    Private m_sFormaPago As String
    Public WriteOnly Property FormaPago() As String
        Set(ByVal Value As String)
            m_sFormaPago = Value
        End Set
    End Property

    Private m_sSituacionActual As String
    Public WriteOnly Property SituacionActual() As String
        Set(ByVal Value As String)
            m_sSituacionActual = Value
        End Set
    End Property

    Private m_dDescuentos As Double
    ''' <summary>
    ''' Total descuentos generales de la factura
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property Descuentos() As Double
        Set(ByVal Value As Double)
            m_dDescuentos = Value
        End Set
    End Property

    Private m_sMoneda As String
    Public WriteOnly Property Moneda() As String
        Set(ByVal Value As String)
            m_sMoneda = Value
        End Set
    End Property

    Private m_sCodViaPago As String
    Public WriteOnly Property CodViaPago() As String
        Set(ByVal Value As String)
            m_sCodViaPago = Value
        End Set
    End Property

    Private m_sViaPago As String
    Public WriteOnly Property ViaPago() As String
        Set(ByVal Value As String)
            m_sViaPago = Value
        End Set
    End Property

    Private m_sObservaciones As String
    Public WriteOnly Property Observaciones() As String
        Set(ByVal Value As String)
            m_sObservaciones = Value
        End Set
    End Property

    Private m_sLiteralNumeroERP As String
    Public WriteOnly Property LiteralNumeroERP() As String
        Set(ByVal Value As String)
            m_sLiteralNumeroERP = Value
        End Set
    End Property

    Private m_sPathServicio As String
    Public WriteOnly Property PathServicio() As String
        Set(ByVal Value As String)
            m_sPathServicio = Value
        End Set
    End Property

    Private m_dsDataSourceCostes As DataTable
    Public WriteOnly Property DataSourceCostes() As DataTable
        Set(ByVal Value As DataTable)
            m_dsDataSourceCostes = Value
        End Set
    End Property

    Private m_dsDataSourceDescuentos As DataTable
    Public WriteOnly Property DataSourceDescuentos() As DataTable
        Set(ByVal Value As DataTable)
            m_dsDataSourceDescuentos = Value
        End Set
    End Property

    Private m_dsDataSourceRetencionGarantia As DataTable
    Public WriteOnly Property DataSourceRetencionGarantia() As DataTable
        Set(ByVal Value As DataTable)
            m_dsDataSourceRetencionGarantia = Value
        End Set
    End Property

    Private m_dsDataSourceFormasPago As DataSet
    Public WriteOnly Property DataSourceFormasPago() As DataSet
        Set(ByVal Value As DataSet)
            m_dsDataSourceFormasPago = Value
        End Set
    End Property

    Private m_dsDataSourceViasPago As DataSet
    Public WriteOnly Property DataSourceViasPago() As DataSet
        Set(ByVal Value As DataSet)
            m_dsDataSourceViasPago = Value
        End Set
    End Property

    Private m_lInstancia As Long
    Public WriteOnly Property Instancia() As Long
        Set(ByVal Value As Long)
            m_lInstancia = Value
        End Set
    End Property

    Private m_oNumberFormat As System.Globalization.NumberFormatInfo
    Public Property NumberFormat() As System.Globalization.NumberFormatInfo
        Get
            NumberFormat = m_oNumberFormat
        End Get
        Set(ByVal Value As System.Globalization.NumberFormatInfo)
            m_oNumberFormat = Value
        End Set
    End Property

    Private m_oDateFormat As System.Globalization.DateTimeFormatInfo
    Public WriteOnly Property DateFormat() As System.Globalization.DateTimeFormatInfo
        Set(ByVal Value As System.Globalization.DateTimeFormatInfo)
            m_oDateFormat = Value
        End Set
    End Property

    Private m_dsCostesFacturasProveedor As DataSet
    Public WriteOnly Property CostesFacturasProveedor() As DataSet
        Set(ByVal Value As DataSet)
            m_dsCostesFacturasProveedor = Value
        End Set
    End Property

    Private m_dsDescuentosFacturasProveedor As DataSet
    Public WriteOnly Property DescuentosFacturasProveedor() As DataSet
        Set(ByVal Value As DataSet)
            m_dsDescuentosFacturasProveedor = Value
        End Set
    End Property

    Private m_dsAtributosBuscadorCostes As DataSet
    Public Property AtributosBuscadorCostes() As DataSet
        Get
            Return m_dsAtributosBuscadorCostes
        End Get
        Set(ByVal Value As DataSet)
            m_dsAtributosBuscadorCostes = Value
        End Set
    End Property

    Private m_dsAtributosBuscadorDescuentos As DataSet
    Public Property AtributosBuscadorDescuentos() As DataSet
        Get
            Return m_dsAtributosBuscadorDescuentos
        End Get
        Set(ByVal Value As DataSet)
            m_dsAtributosBuscadorDescuentos = Value
        End Set
    End Property


    Private m_dtImpuestos As DataTable
    Public WriteOnly Property Impuestos() As DataTable
        Set(ByVal Value As DataTable)
            m_dtImpuestos = Value
        End Set
    End Property

    Private m_bEditable As Boolean
    Public Property Editable() As Boolean
        Get
            Return m_bEditable
        End Get
        Set(ByVal Value As Boolean)
            m_bEditable = Value
        End Set
    End Property

    Private _RutaBuscadorArticulo As String
    Public Property RutaBuscadorArticulo As String
        Get
            Return _RutaBuscadorArticulo
        End Get
        Set(ByVal value As String)
            _RutaBuscadorArticulo = value
        End Set
    End Property

    Private _PropiedadesBuscadorArticulo As String = String.Empty
    Public Property PropiedadesBuscadorArticulo As String
        Get
            Return _PropiedadesBuscadorArticulo
        End Get
        Set(ByVal value As String)
            _PropiedadesBuscadorArticulo = value
        End Set
    End Property

    Private _RutaBuscadorMaterial As String
    Public Property RutaBuscadorMaterial As String
        Get
            Return _RutaBuscadorMaterial
        End Get
        Set(ByVal value As String)
            _RutaBuscadorMaterial = value
        End Set
    End Property

    Private _RutaImagenes As String
    Public Property RutaImagenes As String
        Get
            Return _RutaImagenes
        End Get
        Set(ByVal value As String)
            _RutaImagenes = value
        End Set
    End Property

    Private m_lIdCosteGenerico As Long
    Public WriteOnly Property IdCosteGenerico() As Long
        Set(ByVal Value As Long)
            m_lIdCosteGenerico = Value
        End Set
    End Property

    Private m_lIdDescuentoGenerico As Long
    Public WriteOnly Property IdDescuentoGenerico() As Long
        Set(ByVal Value As Long)
            m_lIdDescuentoGenerico = Value
        End Set
    End Property

    Private m_dSumaCostesLineas As Double
    Public WriteOnly Property SumaCostesLineas() As Double
        Set(ByVal Value As Double)
            m_dSumaCostesLineas = Value
        End Set
    End Property

    Private m_dSumaDescuentosLineas As Double
    Public WriteOnly Property SumaDescuentosLineas() As Double
        Set(ByVal Value As Double)
            m_dSumaDescuentosLineas = Value
        End Set
    End Property

    Private m_sSufijoCache As String
    Public WriteOnly Property SufijoCache() As String
        Set(ByVal Value As String)
            m_sSufijoCache = Value
        End Set
    End Property
#End Region

    Public Event EventoBuscarCostes(ByVal sCod As String, ByVal sDen As String, ByVal sCodArt As String, ByVal sCodMat As String)
    Public Event EventoBuscarDescuentos(ByVal sCod As String, ByVal sDen As String, ByVal sCodArt As String, ByVal sCodMat As String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarTextos()
            CargarDatos()
            CreateColumnsRetencionGarantia()

            If m_bEditable Then
                lblFormaPago.Visible = False
                ddlFormasPago.Visible = True
                lblViaPago.Visible = False
                ddlViasPago.Visible = True
                lblObservaciones.Visible = False
                txtObservaciones.Visible = True

                CargarFormasPago()
                CargarViasPago()
                txtObservaciones.InnerText = m_sObservaciones
            End If

            If m_dsDataSourceCostes.Rows.Count = 0 AndAlso m_dsDataSourceDescuentos.Rows.Count = 0 Then
                If Not m_bEditable Then
                    pnlNoExistenCostesDescuentos.Visible = True
                    pnlDesgloseCostesDescuentosGenerales.Visible = False
                    'habrá que quitarlos de la parte aspx
                    imgAgregarCosteNoExisten.Visible = False
                    imgAgregarDescuentoNoExisten.Visible = False
                    lblAgregarCosteNoExisten.Visible = False
                    lblAgregarDescuentoNoExisten.Visible = False
                Else
                    rpCostesEditable.DataSource = m_dsDataSourceCostes
                    rpCostesEditable.DataBind()
                    lblNoHayCostes.Style.Add("visibility", "visible")
                    divAgregarCoste.Style.Add("visibility", "visible")
                    pnlCostesEditable.Style.Add("visibility", "hidden")

                    rpDescuentosEditable.DataSource = m_dsDataSourceDescuentos
                    rpDescuentosEditable.DataBind()
                    lblNoHayDescuentos.Style.Add("visibility", "visible")
                    divAgregarDescuento.Style.Add("visibility", "visible")
                    pnlDescuentosEditable.Style.Add("visibility", "hidden")
                End If
            Else
                If m_bEditable Then
                    rpCostesEditable.DataSource = m_dsDataSourceCostes
                    rpCostesEditable.DataBind()
                    
                    rpDescuentosEditable.DataSource = m_dsDataSourceDescuentos
                    rpDescuentosEditable.DataBind()
                End If

            If m_dsDataSourceCostes.Rows.Count > 0 Then
                pnlCostesEditable.Visible = m_bEditable 'incluye el repeater más la tabla con el total
                divAgregarCoste.Style.Add("visibility", If(m_bEditable, "visible", "hidden"))
                If Not m_bEditable Then
                    rpCostes.DataSource = m_dsDataSourceCostes
                    rpCostes.DataBind()
                End If
                rpCostes.Visible = Not m_bEditable
            Else
                rpCostes.Visible = False
                lblNoHayCostes.Style.Add("visibility", "visible")
                divAgregarCoste.Style.Add("visibility", If(m_bEditable, "visible", "hidden"))
                pnlCostesEditable.Style.Add("visibility", If(m_bEditable, "hidden", "visible"))
            End If

            If m_dsDataSourceDescuentos.Rows.Count > 0 Then
                pnlDescuentosEditable.Visible = m_bEditable 'incluye el repeater más la tabla con el total
                divAgregarDescuento.Style.Add("visibility", If(m_bEditable, "visible", "hidden"))
                If Not m_bEditable Then
                    rpDescuentos.DataSource = m_dsDataSourceDescuentos
                    rpDescuentos.DataBind()
                End If
                rpDescuentos.Visible = Not m_bEditable
            Else
                rpDescuentos.Visible = False
                lblNoHayDescuentos.Style.Add("visibility", "visible")
                divAgregarDescuento.Style.Add("visibility", If(m_bEditable, "visible", "hidden"))
                pnlDescuentosEditable.Style.Add("visibility", If(m_bEditable, "hidden", "visible"))
            End If
                End If

            If m_dtImpuestos.Rows.Count > 0 Then
                Dim oDTFactura As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable)

                If DBNullToDbl(oDTFactura.Rows(0).Item("IMPORTE_BRUTO")) > 0 Then
                    For Each drImpuesto In m_dtImpuestos.Rows
                        drImpuesto("IMPORTE_CD") = ((DBNullToDbl(drImpuesto("IMPORTE")) * DBNullToDbl(oDTFactura.Rows(0).Item("TOT_COSTES"))) / oDTFactura.Rows(0).Item("IMPORTE_BRUTO"))
                        drImpuesto("IMPORTE") = drImpuesto("IMPORTE") + drImpuesto("IMPORTE_CD")
                    Next
                    m_dtImpuestos.AcceptChanges()
                End If

                rpImpuestos.DataSource = m_dtImpuestos
                rpImpuestos.DataBind()
            End If

                wdgRetencionGarantia.DataSource = m_dsDataSourceRetencionGarantia
                wdgRetencionGarantia.DataBind()


                BusquedaCostesGenerales.Tipo = TipoCostesDecuentos.Costes
                BusquedaCostesGenerales.Nivel = NivelCostesDescuentos.Cabecera
                BusquedaCostesGenerales.NombreTabla = "tblCostesGenerales"
                BusquedaCostesGenerales.PanelBuscadorClientID = mpePanelBuscadorCostes.ClientID
                BusquedaCostesGenerales.CDFacturasProveedor = m_dsCostesFacturasProveedor
                BusquedaCostesGenerales.Textos = oTextos
                BusquedaCostesGenerales.RutaBuscadorArticulo = _RutaBuscadorArticulo
                BusquedaCostesGenerales.PropiedadesBuscadorArticulo = _PropiedadesBuscadorArticulo
                BusquedaCostesGenerales.RutaBuscadorMaterial = _RutaBuscadorMaterial

                BusquedaDescuentosGenerales.Tipo = TipoCostesDecuentos.Descuentos
                BusquedaDescuentosGenerales.Nivel = NivelCostesDescuentos.Cabecera
                BusquedaDescuentosGenerales.NombreTabla = "tblDescuentosGenerales"
                BusquedaDescuentosGenerales.PanelBuscadorClientID = mpePanelBuscadorDescuentos.ClientID
                BusquedaDescuentosGenerales.CDFacturasProveedor = m_dsDescuentosFacturasProveedor
                BusquedaDescuentosGenerales.Textos = oTextos
                BusquedaDescuentosGenerales.RutaBuscadorArticulo = _RutaBuscadorArticulo
                BusquedaDescuentosGenerales.PropiedadesBuscadorArticulo = _PropiedadesBuscadorArticulo
                BusquedaDescuentosGenerales.RutaBuscadorMaterial = _RutaBuscadorMaterial

        Else
                If m_dsDataSourceCostes.Rows.Count = 0 AndAlso m_dsDataSourceDescuentos.Rows.Count = 0 Then
                    If Not m_bEditable Then
                        pnlNoExistenCostesDescuentos.Visible = True
                        pnlDesgloseCostesDescuentosGenerales.Visible = False
                        'habrá que quitarlos de la parte aspx
                        imgAgregarCosteNoExisten.Visible = False
                        imgAgregarDescuentoNoExisten.Visible = False
                        lblAgregarCosteNoExisten.Visible = False
                        lblAgregarDescuentoNoExisten.Visible = False
                    Else
                        rpCostesEditable.DataSource = m_dsDataSourceCostes
                        rpCostesEditable.DataBind()
                        lblNoHayCostes.Style.Add("visibility", "visible")
                        divAgregarCoste.Style.Add("visibility", "visible")
                        pnlCostesEditable.Style.Add("visibility", "hidden")

                        rpDescuentosEditable.DataSource = m_dsDataSourceDescuentos
                        rpDescuentosEditable.DataBind()
                        lblNoHayDescuentos.Style.Add("visibility", "visible")
                        divAgregarDescuento.Style.Add("visibility", "visible")
                        pnlDescuentosEditable.Style.Add("visibility", "hidden")
                    End If
                Else
                    If m_bEditable Then
                        rpCostesEditable.DataSource = m_dsDataSourceCostes
                        rpCostesEditable.DataBind()
                        rpDescuentosEditable.DataSource = m_dsDataSourceDescuentos
                        rpDescuentosEditable.DataBind()
                    End If

                    If m_dsDataSourceCostes.Rows.Count > 0 Then
                        pnlCostesEditable.Visible = m_bEditable 'incluye el repeater más la tabla con el total
                        pnlCostesEditable.Style.Add("visibility", If(m_bEditable, "visible", "hidden"))
                        divAgregarCoste.Style.Add("visibility", If(m_bEditable, "visible", "hidden"))
                        lblNoHayCostes.Style.Add("visibility", "hidden")
                        If Not m_bEditable Then
                            rpCostes.DataSource = m_dsDataSourceCostes
                            rpCostes.DataBind()
                        End If
                        rpCostes.Visible = Not m_bEditable
                    Else
                        rpCostes.Visible = False
                        lblNoHayCostes.Style.Add("visibility", "visible")
                        divAgregarCoste.Style.Add("visibility", If(m_bEditable, "visible", "hidden"))
                        pnlCostesEditable.Style.Add("visibility", If(m_bEditable, "visible", "hidden"))
                    End If

                    If m_dsDataSourceDescuentos.Rows.Count > 0 Then
                        pnlDescuentosEditable.Visible = m_bEditable 'incluye el repeater más la tabla con el total
                        divAgregarDescuento.Style.Add("visibility", If(m_bEditable, "visible", "hidden"))
                        pnlDescuentosEditable.Style.Add("visibility", If(m_bEditable, "visible", "hidden"))
                        lblNoHayDescuentos.Style.Add("visibility", "hidden")
                        If Not m_bEditable Then
                            rpDescuentos.DataSource = m_dsDataSourceDescuentos
                            rpDescuentos.DataBind()
                        End If
                        rpDescuentos.Visible = Not m_bEditable
                    Else
                        rpDescuentos.Visible = False
                        lblNoHayDescuentos.Style.Add("visibility", "visible")
                        divAgregarDescuento.Style.Add("visibility", If(m_bEditable, "visible", "hidden"))
                        pnlDescuentosEditable.Style.Add("visibility", If(m_bEditable, "visible", "hidden"))
                    End If
                End If

            lblCostesGenerales.Text = FormatNumber(m_dCostes, m_oNumberFormat)
            lblDescuentosGenerales.Text = FormatNumber(m_dDescuentos, m_oNumberFormat)
            lblImporteBrutoTotal.Text = FormatNumber(m_dImporteBruto + m_dSumaCostesLineas - m_dSumaDescuentosLineas + m_dCostes - m_dDescuentos, m_oNumberFormat)
            lblImporteTotal.Text = FormatNumber(m_dImporte, m_oNumberFormat)
            lblTotalDesgloseCostes.Text = FormatNumber(Replace(m_dCostes, ".", ","), m_oNumberFormat)
            lblTotalDesgloseDescuentos.Text = FormatNumber(Replace(m_dDescuentos, ".", ","), m_oNumberFormat)
        End If
    End Sub


#Region "Carga TEXTOS y DATOS"

    Private Sub CargarTextos()
        lblTituloDatosGenerales.Text = String.Format("{0}", oTextos(0).Item(1))
        lblLitProveedor.Text = String.Format("{0}:", oTextos(20).Item(1))
        lblLitEmpresa.Text = String.Format("{0}:", oTextos(1).Item(1))
        lblLitImporteTotal.Text = String.Format("{0}:", oTextos(2).Item(1))
        lblLitNumeroFactura.Text = String.Format("{0}:", oTextos(3).Item(1))
        lblLitFechaFactura.Text = String.Format("{0}:", oTextos(4).Item(1))
        lblLitRetencionGarantia.Text = String.Format("{0}:", oTextos(5).Item(1))
        lblLitNumeroERP.Text = String.Format("{0}:", m_sLiteralNumeroERP)
        lblLitFechaContabilizacion.Text = String.Format("{0}:", oTextos(6).Item(1))
        lblLitCostesGenerales.Text = String.Format("{0}:", oTextos(7).Item(1))
        lblLitFormaPago.Text = String.Format("{0}:", oTextos(8).Item(1))
        lblLitSituacionActual.Text = String.Format("{0}:", oTextos(9).Item(1))
        lblLitDescuentosGenerales.Text = String.Format("{0}:", oTextos(10).Item(1))
        lblLitViaPago.Text = String.Format("{0}:", oTextos(11).Item(1))
        lblLitObservaciones.Text = String.Format("{0}:", oTextos(12).Item(1))

        lblLitImporteBruto.Text = String.Format("{0}:", oTextos(75).Item(1))
        lblLitImporteBrutoLineas.Text = oTextos(13).Item(1)
        lblLitImporteBrutoTotal.Text = String.Format("{0}:", oTextos(157).Item(1))
        lblLitCostesGenerales.Text = String.Format("{0}:", oTextos(15).Item(1))
        lblLitDescuentosGenerales.Text = String.Format("{0}:", oTextos(16).Item(1))
        lblLitImporteTotal.Text = String.Format("{0}:", oTextos(17).Item(1))

        lblLitPeriodoOriginal.Text = String.Format("{0}:", oTextos(18).Item(1))
        lblLitFacturaOriginal.Text = String.Format("{0}:", oTextos(19).Item(1))

        lblTituloCostesDescuentosGenerales.Text = oTextos(113).Item(1)
        lblNoExistenCostesDescuentos.Text = oTextos(114).Item(1)
        lblAgregarCosteNoExisten.Text = oTextos(108).Item(1)
        lblAgregarDescuentoNoExisten.Text = oTextos(109).Item(1)
        lblTituloCostes.Text = oTextos(7).Item(1)
        lblAgregarCoste.Text = oTextos(108).Item(1)
        lblNoHayCostes.Text = oTextos(115).Item(1)
        lblTituloDescuentos.Text = oTextos(10).Item(1)
        lblAgregarDescuento.Text = oTextos(109).Item(1)
        lblNoHayDescuentos.Text = oTextos(116).Item(1)
        lblTituloRetencionGarantia.Text = oTextos(117).Item(1)
        lblLitTotalRetencionGarantia.Text = String.Format("{0}:", oTextos(118).Item(1))
        lblCostesFacturas.Text = oTextos(7).Item(1)
        lblDescuentosFacturas.Text = oTextos(10).Item(1)
    End Sub

    Private Sub CargarDatos()
        lblProveedor.Text = m_sProveedor
        lblEmpresa.Text = m_sEmpresa
        lblImporteBruto.Text = FormatNumber(m_dImporteBruto, m_oNumberFormat)
        lblMonedaImporteBruto.Text = " " & m_sMoneda
        lblImporteBrutoLineas.Text = FormatNumber(m_dImporteBruto + m_dSumaCostesLineas - m_dSumaDescuentosLineas, m_oNumberFormat)
        lblMonedaImporteBrutoLineas.Text = " " & m_sMoneda
        lblImporteBrutoTotal.Text = FormatNumber(m_dImporteBruto + m_dSumaCostesLineas - m_dSumaDescuentosLineas + m_dCostes - m_dDescuentos, m_oNumberFormat)
        lblMonedaImporteBrutoTotal.Text = " " & m_sMoneda
        lblImporteTotal.Text = FormatNumber(m_dImporte, m_oNumberFormat)
        lblMonedaImporteTotal.Text = " " & m_sMoneda
        lblNumeroFactura.Text = m_sNumeroFactura
        lblFechaFactura.Text = FormatDate(m_dFechaFactura, m_oDateFormat)
        lblRetencionGarantia.Text = FormatNumber(m_dRetencionGarantia, m_oNumberFormat) & " " & m_sMoneda
        lblNumeroERP.Text = m_sNumeroERP
        If m_dFechaContabilizacion <> Nothing Then
            lblFechaContabilizacion.Text = FormatDate(m_dFechaContabilizacion, m_oDateFormat)
            lblFechaContabilizacion.Visible = True
            lblLitFechaContabilizacion.Visible = True
        End If
        lblSignoCostesGenerales.Text = "+ "
        lblCostesGenerales.Text = FormatNumber(m_dCostes, m_oNumberFormat) '& " " & m_sMoneda
        lblMonedaCostesGenerales.Text = " " & m_sMoneda
        lblFormaPago.Text = m_sFormaPago
        hFormaPago.Value = m_sCodFormaPago
        lblSituacionActual.Text = m_sSituacionActual
        lblSignoDescuentosGenerales.Text = "- "
        lblDescuentosGenerales.Text = FormatNumber(m_dDescuentos, m_oNumberFormat) '& " " & m_sMoneda
        lblMonedaDescuentosGenerales.Text = " " & m_sMoneda
        lblViaPago.Text = m_sViaPago
        hViaPago.Value = m_sCodViaPago
        lblObservaciones.Text = m_sObservaciones

        lblPeriodoOriginal.Text = ""
        lblFacturaOriginal.Text = ""

        lblTotalRetencionGarantia.Text = FormatNumber(m_dRetencionGarantia, m_oNumberFormat) & " " & m_sMoneda
        lblTotalPorcentajeRetencionGarantia.Text = FormatNumber((m_dRetencionGarantia * 100) / If(m_dImporte = 0, 1, m_dImporte), m_oNumberFormat) & " %"

        FSNPanelDatosEmpresa.ServicePath = m_sPathServicio
        FSNPanelDatosEmpresa.Titulo = oTextos(1).Item(1)
        FSNPanelDatosEmpresa.SubTitulo = oTextos(21).Item(1)

        FSNPanelDatosProveedor.ServicePath = m_sPathServicio
        FSNPanelDatosProveedor.Titulo = oTextos(20).Item(1)
        FSNPanelDatosProveedor.SubTitulo = oTextos(22).Item(1)

        lblEmpresa.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosEmpresa.AnimationClientID & "', event, '" & FSNPanelDatosEmpresa.DynamicPopulateClientID & "', '" & m_lIdEmpresa & "'); return false;")
        lblProveedor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosProveedor.AnimationClientID & "', event, '" & FSNPanelDatosProveedor.DynamicPopulateClientID & "', '" & m_sCodProveedor & "'); return false;")
        lblSituacionActual.Attributes.Add("onclick", "VerFlujo(" & m_lInstancia & ",'" & m_sNumeroFactura & "'); return false;")
        lblRetencionGarantia.Attributes.Add("onclick", "$find('" & mpePanelRetencionGarantia.ClientID & "').show()")

        imgAgregarCoste.ImageUrl = _RutaImagenes & "Anyadir.png"
        imgAgregarDescuento.ImageUrl = _RutaImagenes & "Anyadir.png"
        imgAgregarCosteNoExisten.ImageUrl = _RutaImagenes & "Anyadir.png"
        imgAgregarDescuentoNoExisten.ImageUrl = _RutaImagenes & "Anyadir.png"

        imgAgregarCoste.Attributes.Add("onclick", "$find('" & mpePanelBuscadorCostes.ClientID & "').show()")
        imgAgregarDescuento.Attributes.Add("onclick", "$find('" & mpePanelBuscadorDescuentos.ClientID & "').show()")
        imgAgregarCosteNoExisten.Attributes.Add("onclick", "$find('" & mpePanelBuscadorCostes.ClientID & "').show()")
        imgAgregarDescuentoNoExisten.Attributes.Add("onclick", "$find('" & mpePanelBuscadorDescuentos.ClientID & "').show()")

        lblAgregarCoste.Attributes.Add("onclick", "$find('" & mpePanelBuscadorCostes.ClientID & "').show()")
        lblAgregarDescuento.Attributes.Add("onclick", "$find('" & mpePanelBuscadorDescuentos.ClientID & "').show()")
        lblAgregarCosteNoExisten.Attributes.Add("onclick", "$find('" & mpePanelBuscadorCostes.ClientID & "').show()")
        lblAgregarDescuentoNoExisten.Attributes.Add("onclick", "$find('" & mpePanelBuscadorDescuentos.ClientID & "').show()")

        imgCerrar.ImageUrl = _RutaImagenes & "Cerrar.png"
        imgCerrarCostesFacturas.ImageUrl = _RutaImagenes & "Cerrar.png"
        imgCerrarDescuentosFacturas.ImageUrl = _RutaImagenes & "Cerrar.png"
        imgRetencionGarantia.Src = _RutaImagenes & "porcentaje.png"
        imgCostesFacturas.Src = _RutaImagenes & "impuestos.jpg"
        imgDescuentosFacturas.Src = _RutaImagenes & "impuestos.jpg"

        lblLitTotalDesgloseCostes.Text = String.Format("{0}:", oTextos(23).Item(1))
        lblTotalDesgloseCostes.Text = FormatNumber(Replace(m_dCostes, ".", ","), m_oNumberFormat)
        lblMonedaTotalDesgloseCostes.Text = " " & m_sMoneda

        lblLitTotalDesgloseDescuentos.Text = String.Format("{0}:", oTextos(24).Item(1))
        lblTotalDesgloseDescuentos.Text = FormatNumber(Replace(m_dDescuentos, ".", ","), m_oNumberFormat)
        lblMonedaTotalDesgloseDescuentos.Text = " " & m_sMoneda
    End Sub

#End Region
#Region "Carga DROPDOWNS"
    ''' <summary>
    ''' Carga las formas de pago en el dropdown y establace la opción seleccionada
    ''' </summary>
    Sub CargarFormasPago()
        ddlFormasPago.DataSource = m_dsDataSourceFormasPago
        ddlFormasPago.DataBind()
        ddlFormasPago.SelectedValue = m_sCodFormaPago
    End Sub

    ''' <summary>
    ''' Carga las vías de pago en el dropdown y establace la opción seleccionada
    ''' </summary>
    Sub CargarViasPago()
        ddlViasPago.DataSource = m_dsDataSourceViasPago
        ddlViasPago.DataBind()
        ddlViasPago.SelectedValue = m_sCodViaPago
    End Sub
#End Region
#Region "Carga RETENCION GARANTIA"
    Private Sub CreateColumnsRetencionGarantia()
        Me.wdgRetencionGarantia.Columns.Add(AddColumn("ARTICULO", oTextos(26).Item(1), 45))
        Me.wdgRetencionGarantia.Columns.Add(AddColumn("PEDIDO", oTextos(27).Item(1), 10))
        Me.wdgRetencionGarantia.Columns.Add(AddColumn("ALBARAN", oTextos(28).Item(1), 10))
        Me.wdgRetencionGarantia.Columns.Add(AddColumn("IMPORTE", oTextos(14).Item(1), 15, "Right"))
        Me.wdgRetencionGarantia.Columns.Add(AddColumn("PORCENTAJE", oTextos(29).Item(1), 10, "Right"))
        Me.wdgRetencionGarantia.Columns.Add(AddColumn("IM_RETGARANTIA", oTextos(30).Item(1), 10, "Right"))
        Me.wdgRetencionGarantia.Columns.Add(AddColumn("MON", "", 0, , False))
    End Sub

    Private Function AddColumn(ByVal fieldName As String, ByVal headerText As String, ByVal Width As Integer, Optional ByVal Alineacion As String = "Left", Optional ByVal Visible As Boolean = True) As BoundDataField
        Dim field As New BoundDataField(True)
        field.Key = fieldName
        field.DataFieldName = fieldName
        field.Header.Text = headerText
        field.Hidden = Not Visible
        field.Width = Unit.Percentage(Width)
        field.CssClass = Alineacion
        Return field
    End Function

    Private Sub wdgRetencionGarantia_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdgRetencionGarantia.InitializeRow
        e.Row.Items.Item(sender.Columns.FromKey("IMPORTE").Index).Text = FormatNumber(Replace(e.Row.Items.Item(sender.Columns.FromKey("IMPORTE").Index).Value, ".", ","), m_oNumberFormat) & " " & e.Row.Items.Item(sender.Columns.FromKey("MON").Index).Value
        e.Row.Items.Item(sender.Columns.FromKey("PORCENTAJE").Index).Text = FormatNumber(Replace(e.Row.Items.Item(sender.Columns.FromKey("PORCENTAJE").Index).Value, ".", ","), m_oNumberFormat) & " %"
        e.Row.Items.Item(sender.Columns.FromKey("IM_RETGARANTIA").Index).Text = FormatNumber(Replace(e.Row.Items.Item(sender.Columns.FromKey("IM_RETGARANTIA").Index).Value, ".", ","), m_oNumberFormat) & " " & e.Row.Items.Item(sender.Columns.FromKey("MON").Index).Value
    End Sub

#End Region

#Region "Repeaters COSTES/DESCUENTOS/IMPUESTOS"

    Private Sub rpCostes_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpCostes.ItemDataBound
        Dim lbl As Label
        If e.Item.ItemType = ListItemType.Footer Then
            CType(e.Item.Controls(0).FindControl("lblTotal"), Label).Text = String.Format("{0}: ", oTextos(23).Item(1)) & FormatNumber(Replace(m_dCostes, ".", ","), m_oNumberFormat) & " " & m_sMoneda
        End If
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            lbl = CType(e.Item.FindControl("lblValor"), Label)
            lbl.Text = FormatNumber(Replace(e.Item.DataItem("VALOR"), ".", ","), m_oNumberFormat)
            lbl = CType(e.Item.FindControl("lblImporte"), Label)
            lbl.Text = FormatNumber(Replace(e.Item.DataItem("IMPORTE"), ".", ","), m_oNumberFormat) & " " & m_sMoneda
        End If
    End Sub

    Private Sub rpCostesEditable_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpCostesEditable.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            CType(e.Item.FindControl("lblImporte"), Label).Text = FormatNumber(Replace(e.Item.DataItem("IMPORTE"), ".", ","), m_oNumberFormat)
            CType(e.Item.Controls(0).FindControl("lblMoneda"), Label).Text = m_sMoneda
            DirectCast(e.Item.Controls(0).FindControl("imgEliminarCoste"), HtmlControls.HtmlInputImage).Src = _RutaImagenes & "eliminar.png"
        End If
    End Sub

    Private Sub rpDescuentos_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpDescuentos.ItemDataBound
        Dim lbl As Label
        If e.Item.ItemType = ListItemType.Footer Then
            CType(e.Item.Controls(0).FindControl("lblTotal"), Label).Text = String.Format("{0}: ", oTextos(24).Item(1)) & FormatNumber(Replace(m_dDescuentos, ".", ","), m_oNumberFormat) & " " & m_sMoneda
        End If
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            lbl = CType(e.Item.FindControl("lblValor"), Label)
            lbl.Text = FormatNumber(Replace(e.Item.DataItem("VALOR"), ".", ","), m_oNumberFormat)
            lbl = CType(e.Item.FindControl("lblImporte"), Label)
            lbl.Text = FormatNumber(Replace(e.Item.DataItem("IMPORTE"), ".", ","), m_oNumberFormat) & " " & m_sMoneda
        End If
    End Sub

    Private Sub rpDescuentosEditable_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpDescuentosEditable.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            CType(e.Item.FindControl("lblImporte"), Label).Text = FormatNumber(Replace(e.Item.DataItem("IMPORTE"), ".", ","), m_oNumberFormat)
            CType(e.Item.Controls(0).FindControl("lblMoneda"), Label).Text = m_sMoneda
            DirectCast(e.Item.Controls(0).FindControl("imgEliminarDescuento"), HtmlControls.HtmlInputImage).Src = _RutaImagenes & "eliminar.png"
        End If
    End Sub

    Dim dImporteTotalImpuestos As Double
    Private Sub rpImpuestos_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpImpuestos.ItemDataBound
        Static dImporteImpuestosOld As Double
        Select Case e.Item.ItemType
            Case ListItemType.Header
                dImporteTotalImpuestos = 0
                dImporteImpuestosOld = Numero(Replace(lblImporteTotal.Text, m_sMoneda, ""), m_oNumberFormat.NumberDecimalSeparator, m_oNumberFormat.NumberGroupSeparator) - Numero(Replace(lblImporteBrutoTotal.Text, m_sMoneda, ""), m_oNumberFormat.NumberDecimalSeparator, m_oNumberFormat.NumberGroupSeparator)
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim lbl As Label
                lbl = CType(e.Item.FindControl("lblLitImpuesto"), Label)
                Dim sDenImpuesto As String = e.Item.DataItem("DEN")
                If sDenImpuesto.Length > 40 Then _
                    sDenImpuesto = sDenImpuesto.Substring(0, 40).Trim & "..."
                lbl.Text = sDenImpuesto & If(e.Item.DataItem("RETENIDO") = 1, " " & oTextos(25).Item(1), "") & ":"
                lbl = CType(e.Item.FindControl("lblImpuesto"), Label)
                lbl.Text = If(e.Item.DataItem("RETENIDO") = 1, "- ", "+ ") & FormatNumber(Replace(e.Item.DataItem("IMPORTE"), ".", ","), m_oNumberFormat) & " " & m_sMoneda
                dImporteTotalImpuestos = dImporteTotalImpuestos + If(e.Item.DataItem("RETENIDO") = 1, e.Item.DataItem("IMPORTE") * (-1), e.Item.DataItem("IMPORTE"))
            Case ListItemType.Footer
                If IsPostBack Then
                    lblImporteTotal.Text = FormatNumber(Numero(lblImporteTotal.Text, m_oNumberFormat.NumberDecimalSeparator, m_oNumberFormat.NumberGroupSeparator) + dImporteTotalImpuestos - dImporteImpuestosOld, m_oNumberFormat)
                    ActualizarImporteFactura(Numero(lblImporteTotal.Text))
                End If
        End Select
    End Sub
#End Region

    ''' <summary>
    ''' Procedimiento que recalcula los diferentes importes en el resumen de la cabecera
    ''' </summary>
    ''' <param name="Tipo">Tipo de actualizacion:impuestos, costes o descuentos, o importe bruto</param>
    ''' <param name="ImporteNetoLineasConCD">Importe Neto de las lineas con costes y descuentos incluidos, sin impuestos</param>
    ''' <param name="dtImpuestos">datatable con los impuestos actualizados tras realizar alguna modificacion de impuestos en las lineas de factura</param>
    ''' <remarks></remarks>
    Public Sub RecalcularImportesCabecera(ByVal Tipo As Byte, ByVal ImporteNetoLineasConCD As Double, ByVal dtImpuestos As DataTable, ByVal ImporteNetoLineasConCDYImpuestos As Double)
        Select Case Tipo
            Case 0
                'Impuestos
                rpImpuestos.DataSource = dtImpuestos
                rpImpuestos.DataBind()

            Case 1, 2
                'Costes y descuentos
                lblImporteBrutoLineas.Text = FormatNumber(ImporteNetoLineasConCD, m_oNumberFormat)
                lblImporteBrutoTotal.Text = FormatNumber(ImporteNetoLineasConCD + m_dCostes - m_dDescuentos, m_oNumberFormat)
                lblImporteTotal.Text = FormatNumber(ImporteNetoLineasConCDYImpuestos + m_dCostes - m_dDescuentos, m_oNumberFormat)
            Case 3
                Dim oDTFactura As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable)
                lblImporteBruto.Text = FormatNumber(oDTFactura.Rows(0).Item("IMPORTE_BRUTO"), m_oNumberFormat)
        End Select
        upImpuestosCabecera.Update()
    End Sub

    Sub BuscarCostes(ByVal sCod As String, ByVal sDen As String, ByVal sCodArt As String, ByVal sCodMat As String) Handles BusquedaCostesGenerales.EventoBuscarCostes
        RaiseEvent EventoBuscarCostes(sCod, sDen, sCodArt, sCodMat)
        BusquedaCostesGenerales.AtributosBuscadorCostesDescuentos = AtributosBuscadorCostes
    End Sub

    Sub BuscarDescuentos(ByVal sCod As String, ByVal sDen As String, ByVal sCodArt As String, ByVal sCodMat As String) Handles BusquedaDescuentosGenerales.EventoBuscarDescuentos
        RaiseEvent EventoBuscarDescuentos(sCod, sDen, sCodArt, sCodMat)
        BusquedaDescuentosGenerales.AtributosBuscadorCostesDescuentos = AtributosBuscadorDescuentos
    End Sub


    Private Sub btnActualizarImpuestos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarImpuestos.Click
        RecalcularImpuestosCostesDescuentosGenerales()
        Dim dt As DataTable = RecalcularImpuestosCabecera()
        RecalcularImportesCabecera(0, 0, dt, 0)
    End Sub

    Private Sub ActualizarImporteFactura(ByVal dImporte As Double)
        Dim oDTFactura As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable)
        oDTFactura.Rows(0).Item("IMPORTE") = dImporte
        oDTFactura.AcceptChanges()
    End Sub

    Private Sub RecalcularImpuestosCostesDescuentosGenerales()
        Dim oDTFactura As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable)
        Dim dtImpuestosRepercutidos As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(8)
        Dim dtImpuestosRetenidos As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(9)

        If DBNullToDbl(oDTFactura.Rows(0).Item("IMPORTE_BRUTO")) > 0 Then
            For Each drImpuesto In dtImpuestosRepercutidos.Rows
                drImpuesto("IMPORTE_CD") = ((DBNullToDbl(drImpuesto("IMPORTE")) * DBNullToDbl(oDTFactura.Rows(0).Item("TOT_COSTES"))) / oDTFactura.Rows(0).Item("IMPORTE_BRUTO"))
            Next
            dtImpuestosRepercutidos.AcceptChanges()
        
            For Each drImpuesto In dtImpuestosRetenidos.Rows
                drImpuesto("IMPORTE_CD") = ((DBNullToDbl(drImpuesto("IMPORTE")) * DBNullToDbl(oDTFactura.Rows(0).Item("TOT_DESCUENTOS"))) / oDTFactura.Rows(0).Item("IMPORTE_BRUTO"))
            Next
            dtImpuestosRetenidos.AcceptChanges()
        End If
    End Sub

    ''' <summary>
    ''' Funcion que devuelve un datatable con los impuestos agrupados para actualizar el repeater de impuestos de la cabecera(usercontrol de DatosGenerales)
    ''' </summary>
    ''' <remarks></remarks>
    Private Function RecalcularImpuestosCabecera() As DataTable
        'Junto en una misma tabla los impuestos retenidos y repercutidos
        Dim dtImpuestos As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(8).Copy
        dtImpuestos.Merge(CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(9))

        'agrupo los impuestos
        Dim drAll2 = From orders In dtImpuestos.AsEnumerable()
            Where orders.Item("IMPORTE") IsNot DBNull.Value
            Group orders By Den = DBNullToStr(orders.Field(Of String)("DEN")), Retenido = orders.Field(Of Integer)("RETENIDO")
            Into Importe = Sum(DBNullToDbl(orders.Field(Of Double)("IMPORTE"))), Importe_CD = Sum(DBNullToDbl(orders.Field(Of Double)("IMPORTE_CD")))
            Select New With {Importe, Den, Retenido, Importe_CD}

        ' Y lo paso de nuevo a un datatable
        Dim dt As New DataTable
        dt.Columns.Add("DEN")
        dt.Columns.Add("IMPORTE")
        dt.Columns.Add("RETENIDO")
        dt.Columns.Add("IMPORTE_CD")
        Dim newRow As DataRow
        For Each item In drAll2
            newRow = dt.NewRow
            Dim sDenImpuesto As String = item.Den
            If sDenImpuesto.Length > 40 Then _
                sDenImpuesto = item.Den.Substring(0, 40).Trim & "..."
            newRow("DEN") = sDenImpuesto
            newRow("IMPORTE") = item.Importe + item.Importe_CD
            newRow("RETENIDO") = item.Retenido
            dt.Rows.Add(newRow)
        Next
        Return dt
    End Function
End Class