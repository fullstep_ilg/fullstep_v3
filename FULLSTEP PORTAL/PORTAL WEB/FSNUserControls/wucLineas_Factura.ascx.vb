﻿Imports Infragistics.Web.UI.GridControls
Imports Fullstep.FSNLibrary
Public Class wucLineas_Factura
    Inherits System.Web.UI.UserControl


    Public Event EventoCargarDetalleAlbaran(ByVal sAlbaran As String)
    Public Event eventProvinciasItemRequested(ByVal sCodPais As String)
    Public Event EventoBuscarImpuestos(ByVal sCod As String, ByVal sDescr As String, ByVal sPais As String, ByVal sProv As String, ByVal sCodArt As String, ByVal sCodMat As String, ByVal bImpRepercutido As Boolean, ByVal bImpRetenido As Boolean, ByVal iConceptoImpuesto As Nullable(Of TiposDeDatos.ConceptoImpuesto))
    Public Event EventoBuscarCostes(ByVal sCod As String, ByVal sDescr As String, ByVal sCodArt As String, ByVal sCodMat As String)
    Public Event EventoBuscarDescuentos(ByVal sCod As String, ByVal sDescr As String, ByVal sCodArt As String, ByVal sCodMat As String)
    Public Event EventoRecalcularImportes(ByVal Tipo As Byte, ByVal ImporteNetoLineasConCD As Double, ByVal dtImpuestos As DataTable, ByVal ImporteNetoLineasConCDYImpuestos As Double)

    Private m_dTotalImpuestosRepercutidos As Double = 0
    Private m_dTotalImpuestosRetenidos As Double = 0
    Private m_dTotalCostesAlbaran As Double = 0
    Private m_dTotalDescuentosAlbaran As Double = 0
    Private _dtPaises As DataTable
    Private _dtProvincias As DataTable
    Private _sCodPaisProve As String
    Private _dtImpuestos As DataTable
    Private _dsFactura As DataSet
    Private _RutaBuscadorArticulo As String
    Private _PropiedadesBuscadorArticulo As String = String.Empty
    Private _RutaBuscadorMaterial As String
    Private _RutaImagenes As String
    Private _ImporteTotalLineasConCD As Double 'Importe total de las lineas con costes y descuentos, sin impuestos
    Private _ImporteTotalLineasConCDYImpuestos As Double 'Importe total de las lineas con costes y descuentos y con impuestos
    Private _ImporteBrutoLineaSeleccionada As Double
    Private _DenominacionArticuloLineaSeleccionada As String
    Private _CodLineaSeleccionada As String
    Private _CodArticuloLineaSeleccionada As String

#Region " PROPIEDADES "
    Private oTextos As DataTable
    ''' <summary>
    ''' Propiedad de los paises
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property Paises() As DataTable
        Set(ByVal Value As DataTable)
            _dtPaises = Value
        End Set
    End Property
    ''' <summary>
    ''' Ruta para abrir el buscador de Articulo
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property RutaBuscadorArticulo As String
        Get
            Return _RutaBuscadorArticulo
        End Get
        Set(ByVal value As String)
            _RutaBuscadorArticulo = value
        End Set
    End Property
    Public Property PropiedadesBuscadorArticulo As String
        Get
            Return _PropiedadesBuscadorArticulo
        End Get
        Set(ByVal value As String)
            _PropiedadesBuscadorArticulo = value
        End Set
    End Property
    ''' <summary>
    ''' Ruta para abrir el buscador de materiales
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property RutaBuscadorMaterial As String
        Get
            Return _RutaBuscadorMaterial
        End Get
        Set(ByVal value As String)
            _RutaBuscadorMaterial = value
        End Set
    End Property
    ''' <summary>
    ''' Ruta donde están las imágenes del control
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property RutaImagenes As String
        Get
            Return _RutaImagenes
        End Get
        Set(ByVal value As String)
            _RutaImagenes = value
        End Set
    End Property
    ''' <summary>
    ''' Propiedad de las provincias
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property Provincias() As DataTable
        Set(ByVal Value As DataTable)
            _dtProvincias = Value
        End Set
    End Property
    Public WriteOnly Property Textos() As DataTable
        Set(ByVal Value As DataTable)
            oTextos = Value
        End Set
    End Property
    Private m_dtDataSource As DataTable
    Public WriteOnly Property DataSource() As DataTable
        Set(ByVal Value As DataTable)
            m_dtDataSource = Value
        End Set
    End Property
    Private m_oNumberFormat As System.Globalization.NumberFormatInfo
    Public Property NumberFormat() As System.Globalization.NumberFormatInfo
        Get
            NumberFormat = m_oNumberFormat
        End Get
        Set(ByVal Value As System.Globalization.NumberFormatInfo)
            m_oNumberFormat = Value
        End Set
    End Property
    Private m_oDateFormat As System.Globalization.DateTimeFormatInfo
    Public WriteOnly Property DateFormat() As System.Globalization.DateTimeFormatInfo
        Set(ByVal Value As System.Globalization.DateTimeFormatInfo)
            m_oDateFormat = Value
        End Set
    End Property
    Private m_sPathServicio As String
    Public WriteOnly Property PathServicio() As String
        Set(ByVal Value As String)
            m_sPathServicio = Value
        End Set
    End Property
    Private m_sIdFactura As String
    Public WriteOnly Property IdFactura() As String
        Set(ByVal Value As String)
            m_sIdFactura = Value
        End Set
    End Property
    Private m_sNumFactura As String
    Public WriteOnly Property NumFactura() As String
        Set(ByVal Value As String)
            m_sNumFactura = Value
        End Set
    End Property
    Private m_sCodProve As String
    Public WriteOnly Property CodProve() As String
        Set(ByVal Value As String)
            m_sCodProve = Value
        End Set
    End Property
    Private m_sMoneda As String
    Public WriteOnly Property Moneda() As String
        Set(ByVal Value As String)
            m_sMoneda = Value
        End Set
    End Property
    Public WriteOnly Property CodPaisProve() As String
        Set(ByVal Value As String)
            _sCodPaisProve = Value
        End Set
    End Property
    Private m_dsImpuestosBuscador As DataSet
    Public WriteOnly Property ImpuestosBuscador() As DataSet
        Set(ByVal Value As DataSet)
            m_dsImpuestosBuscador = Value
        End Set
    End Property
    Private m_dsCostesBuscador As DataSet
    Public WriteOnly Property CostesBuscador() As DataSet
        Set(ByVal Value As DataSet)
            m_dsCostesBuscador = Value
        End Set
    End Property
    Private m_dsDescuentosBuscador As DataSet
    Public WriteOnly Property DescuentosBuscador() As DataSet
        Set(ByVal Value As DataSet)
            m_dsDescuentosBuscador = Value
        End Set
    End Property
    Private m_dsCostesUtilizadosBuscador As DataSet
    Public WriteOnly Property CostesUtilizadosBuscador() As DataSet
        Set(ByVal Value As DataSet)
            m_dsCostesUtilizadosBuscador = Value
        End Set
    End Property
    Private m_dsDescuentosUtilizadosBuscador As DataSet
    Public WriteOnly Property DescuentosUtilizadosBuscador() As DataSet
        Set(ByVal Value As DataSet)
            m_dsDescuentosUtilizadosBuscador = Value
        End Set
    End Property
    Private m_dsImpuestosUtilizadosBuscador As DataSet
    Public WriteOnly Property ImpuestosUtilizadosBuscador() As DataSet
        Set(ByVal Value As DataSet)
            m_dsImpuestosUtilizadosBuscador = Value
        End Set
    End Property
    Private m_dtImpuestosFactura As DataTable
    Public WriteOnly Property ImpuestosFactura() As DataTable
        Set(ByVal Value As DataTable)
            m_dtImpuestosFactura = Value
        End Set
    End Property
    Private m_dtImpuestosRepercutidos As DataTable
    Public WriteOnly Property ImpuestosRepercutidos() As DataTable
        Set(ByVal Value As DataTable)
            m_dtImpuestosRepercutidos = Value
        End Set
    End Property
    Private m_dtImpuestosRetenidos As DataTable
    Public WriteOnly Property ImpuestosRetenidos() As DataTable
        Set(ByVal Value As DataTable)
            m_dtImpuestosRetenidos = Value
        End Set
    End Property
    Private m_bEditable As Boolean
    Public WriteOnly Property Editable() As Boolean
        Set(ByVal Value As Boolean)
            m_bEditable = Value
        End Set
    End Property
    Private m_bCostesPorLinea As Boolean
    Public WriteOnly Property CostesPorLinea() As Boolean
        Set(ByVal Value As Boolean)
            m_bCostesPorLinea = Value
        End Set
    End Property
    Private m_lIdCosteGenerico As Long
    Public WriteOnly Property IdCosteGenerico() As Long
        Set(ByVal Value As Long)
            m_lIdCosteGenerico = Value
        End Set
    End Property
    Private m_lIdDescuentoGenerico As Long
    Public WriteOnly Property IdDescuentoGenerico() As Long
        Set(ByVal Value As Long)
            m_lIdDescuentoGenerico = Value
        End Set
    End Property
    Private m_sSufijoCache As String
    Public WriteOnly Property SufijoCache() As String
        Set(ByVal Value As String)
            m_sSufijoCache = Value
        End Set
    End Property
    Private _dtPartidas As DataTable
    Public Property PartidasDataTable As DataTable
        Get
            Return _dtPartidas
        End Get
        Set(ByVal value As DataTable)
            _dtPartidas = value
        End Set
    End Property
    Private m_bMostrarLeyendaOtrosGestores As Boolean
    Public WriteOnly Property MostrarLeyendaOtrosGestores() As Boolean
        Set(ByVal Value As Boolean)
            m_bMostrarLeyendaOtrosGestores = Value
        End Set
    End Property
    Private m_bAccesoSM As Boolean
    Public WriteOnly Property AccesoSM() As Boolean
        Set(ByVal Value As Boolean)
            m_bAccesoSM = Value
        End Set
    End Property
#Region "Detalle del albarán"

    Private m_sNumeroRecepcionERP As String
    Public WriteOnly Property AlbaranNumeroRecepcionERP() As String
        Set(ByVal Value As String)
            m_sNumeroRecepcionERP = Value
        End Set
    End Property

    Private m_dFechaRecepcion As String
    Public WriteOnly Property AlbaranFechaRecepcion() As String
        Set(ByVal Value As String)
            m_dFechaRecepcion = Value
        End Set
    End Property

    Private m_sCodReceptor As String
    Public WriteOnly Property CodReceptor() As String
        Set(ByVal Value As String)
            m_sCodReceptor = Value
        End Set
    End Property

    Private m_sNombreReceptor As String
    Public WriteOnly Property NombreReceptor() As String
        Set(ByVal Value As String)
            m_sNombreReceptor = Value
        End Set
    End Property

    Private m_sCargo As String
    Public WriteOnly Property Cargo() As String
        Set(ByVal Value As String)
            m_sCargo = Value
        End Set
    End Property

    Private m_sDepartamento As String
    Public WriteOnly Property Departamento() As String
        Set(ByVal Value As String)
            m_sDepartamento = Value
        End Set
    End Property

    Private m_sEmail As String
    Public WriteOnly Property Email() As String
        Set(ByVal Value As String)
            m_sEmail = Value
        End Set
    End Property

    Private m_sTelefono As String
    Public WriteOnly Property Telefono() As String
        Set(ByVal Value As String)
            m_sTelefono = Value
        End Set
    End Property

    Private m_sFax As String
    Public WriteOnly Property Fax() As String
        Set(ByVal Value As String)
            m_sFax = Value
        End Set
    End Property

    Private m_sOrganizacion As String
    Public WriteOnly Property Organizacion() As String
        Set(ByVal Value As String)
            m_sOrganizacion = Value
        End Set
    End Property

    Private m_dtCostesAlbaran As DataTable
    Public WriteOnly Property CostesAlbaran() As DataTable
        Set(ByVal Value As DataTable)
            m_dtCostesAlbaran = Value
        End Set
    End Property

    Private m_dtDescuentosAlbaran As DataTable
    Public WriteOnly Property DescuentosAlbaran() As DataTable
        Set(ByVal Value As DataTable)
            m_dtDescuentosAlbaran = Value
        End Set
    End Property

#End Region
#End Region
#Region "Eventos Grid"
    Public Sub whdgLineas_CellSelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.SelectedCellEventArgs) Handles whdgLineas.CellSelectionChanged
        'Se recogen estas variables para mostrarlas cuando se muestran los paneles de detalle de costes, impuestos y descuentos
        _ImporteBrutoLineaSeleccionada = DBNullToDbl(e.CurrentSelectedCells(0).Row.Items.FindItemByKey("CANT_PED").Value) * DBNullToDbl(e.CurrentSelectedCells(0).Row.Items.FindItemByKey("PREC_UP").Value)
        _DenominacionArticuloLineaSeleccionada = DBNullToStr(e.CurrentSelectedCells(0).Row.Items.FindItemByKey("ART_DEN").Value)
        _CodLineaSeleccionada = DBNullToStr(e.CurrentSelectedCells(0).Row.Items.FindItemByKey("LINEA_GRID").Text)
        _CodArticuloLineaSeleccionada = DBNullToStr(e.CurrentSelectedCells(0).Row.Items.FindItemByKey("ART_INT").Value)
        Select Case e.CurrentSelectedCells.Item(0).Column.Key
            Case "ALBARAN"
                RaiseEvent EventoCargarDetalleAlbaran(e.CurrentSelectedCells(0).Value)

                imgDetalleAlbaran.Src = _RutaImagenes & "recepcion_small.jpg"
                lblDetalleAlbaran.Text = oTextos(51)(1) & " " & e.CurrentSelectedCells(0).Value
                imgCalculoImporte.Src = _RutaImagenes & "CostesDescuentos€.png"

                If m_bEditable Then
                    lblLitEliminarAlbaran.Visible = True
                    lblLitEliminarAlbaran.Text = oTextos(52)(1)
                    lblLitEliminarAlbaran.OnClientClick = "return borrarAlbaran(" & Request("ID") & ",'" & e.CurrentSelectedCells(0).Value & "');"
                End If

                lblLitFechaRecepcion.Text = String.Format("{0}:", oTextos(53).Item(1))
                lblFechaRecepcion.Text = FormatDate(m_dFechaRecepcion, m_oDateFormat)
                lblLitNumeroRecepcionERP.Text = String.Format("{0}:", oTextos(54).Item(1))
                lblNumeroRecepcionERP.Text = m_sNumeroRecepcionERP
                'Datos receptor
                lblTituloDatosReceptor.Text = oTextos(101).Item(1)
                lblLitCodReceptor.Text = String.Format("{0}:", oTextos(39).Item(1))
                lblLitNombreReceptor.Text = String.Format("{0}:", oTextos(55).Item(1))
                lblLitCargo.Text = String.Format("{0}:", oTextos(56).Item(1))
                lblLitDepartamento.Text = String.Format("{0}:", oTextos(57).Item(1))
                lblLitEmail.Text = String.Format("{0}:", oTextos(58).Item(1))
                lblLitTelefono.Text = String.Format("{0}:", oTextos(59).Item(1))
                lblLitFax.Text = String.Format("{0}:", oTextos(60).Item(1))
                lblLitOrganizacion.Text = String.Format("{0}:", oTextos(61).Item(1))
                lblCodReceptor.Text = m_sCodReceptor
                lblNombreReceptor.Text = m_sNombreReceptor
                lblCargo.Text = m_sCargo
                lblDepartamento.Text = m_sDepartamento
                lblEmail.Text = m_sEmail
                lblTelefono.Text = m_sTelefono
                lblFax.Text = m_sFax
                lblOrganizacion.Text = m_sOrganizacion

                If m_dtCostesAlbaran.Rows.Count > 0 OrElse m_dtDescuentosAlbaran.Rows.Count > 0 Then
                    imgCostesDescuentosAlbaran.Src = _RutaImagenes & "candado_close.png" ' "images/recepcion_small.jpg"
                    lblTituloCostesDescuentosAlbaran.Text = oTextos(95).Item(1)
                    lblTituloCostesAlbaran.Text = oTextos(7).Item(1)
                    lblTituloDescuentosAlbaran.Text = oTextos(10).Item(1)
                    pnlCostesDescuentosAlbaran.Visible = True

                    'Costes
                    If m_dtCostesAlbaran.Rows.Count > 0 Then
                        rpCostesAlbaran.DataSource = m_dtCostesAlbaran
                        rpCostesAlbaran.DataBind()
                    Else
                        rpCostesAlbaran.Visible = False
                        lblSinCostesAlbaran.Text = oTextos(96).Item(1)
                        lblSinCostesAlbaran.Visible = True
                    End If
                    'Descuentos
                    If m_dtDescuentosAlbaran.Rows.Count > 0 Then
                        rpDescuentosAlbaran.DataSource = m_dtDescuentosAlbaran
                        rpDescuentosAlbaran.DataBind()
                    Else
                        rpDescuentosAlbaran.Visible = False
                        lblSinDescuentosAlbaran.Text = oTextos(97).Item(1)
                        lblSinDescuentosAlbaran.Visible = True
                    End If
                End If

                UpPanelDetalleAlbaran.Update()
                mpePanelDetalleAlbaran.Show()

                e.CurrentSelectedCells.Remove(e.CurrentSelectedCells(0))
        End Select
    End Sub

    ''' <summary>
    ''' evento que se lanza por cada fila del datasource asociado al grid de lineas de factura
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub whdgLineas_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgLineas.InitializeRow
        Dim dtImpuestosLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(10)
        Dim dtCostesLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(11)
        Dim dtDescuentosLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(13)
        Dim dtLineas As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
        Static importeBruto As Double
        Static importeNeto As Double
        Static importeImpuestos As Double
        Static importeTotal As Double

        If e.Row.Index = 0 Then
            'Si es la primera fila se inicializan las variables
            importeBruto = 0
            importeNeto = 0
            importeImpuestos = 0
            importeTotal = 0
        End If

        Dim drImpuestosLinea() As DataRow = dtImpuestosLinea.Select("LINEA=" & e.Row.Items.Item(sender.Columns.FromKey("LINEA").Index).Value)
        Dim drCostesLinea() As DataRow = dtCostesLinea.Select("LINEA=" & e.Row.Items.Item(sender.Columns.FromKey("LINEA").Index).Value)
        Dim drDescuentosLinea() As DataRow = dtDescuentosLinea.Select("LINEA=" & e.Row.Items.Item(sender.Columns.FromKey("LINEA").Index).Value)
        Dim drLineaFactura() As DataRow = dtLineas.Select("LINEA=" & e.Row.Items.Item(sender.Columns.FromKey("LINEA").Index).Value)
        With e.Row.Items
            .Item(sender.Columns.FromKey("LINEA_GRID").Index).Text = (e.Row.Index + 1).ToString.PadLeft(3, "0")
            .Item(sender.Columns.FromKey("CANT_PED").Index).Text = .Item(sender.Columns.FromKey("CANT_PED").Index).Value & " " & .Item(sender.Columns.FromKey("UP").Index).Value
            .Item(sender.Columns.FromKey("PREC_UP").Index).Text = FormatNumber(Replace(DBNullToDbl(.Item(sender.Columns.FromKey("PREC_UP").Index).Value), ".", ","), m_oNumberFormat) & " " & e.Row.Items.Item(sender.Columns.FromKey("MON").Index).Value
            .Item(sender.Columns.FromKey("PREC_UP_POR_CANT_FORMATEADO").Index).Text = FormatNumber(Replace(DBNullToDbl(.Item(sender.Columns.FromKey("PREC_UP").Index).Value) * DBNullToDbl(.Item(sender.Columns.FromKey("CANT_PED").Index).Value), ".", ","), m_oNumberFormat) & " " & e.Row.Items.Item(sender.Columns.FromKey("MON").Index).Value
            .Item(sender.Columns.FromKey("PEDIDO").Index).Text = .Item(sender.Columns.FromKey("NUM_PEDIDO").Index).Value
            .Item(sender.Columns.FromKey("PEDIDO").Index).CssClass = "Subrayado itemSeleccionable"
            .Item(sender.Columns.FromKey("ALBARAN").Index).Text = .Item(sender.Columns.FromKey("ALBARAN").Index).Text
            .Item(sender.Columns.FromKey("ALBARAN").Index).CssClass = "Subrayado itemSeleccionable"
            .Item(sender.Columns.FromKey("OBS").Index).Text = "<img src='" & _RutaImagenes & "observaciones.png" & "' style='text-align:center;'/>"
            .FindItemByKey("OBS").CssClass = "itemSeleccionable"

            Dim objImporteImpuestos(1) As String
            objImporteImpuestos = CalculoImporteImpuestoLinea(drImpuestosLinea)
            Dim importeCostes As String = CalculoImporteCostesLinea(drCostesLinea)
            Dim importeDescuentos As String = CalculoImporteDescuentosLinea(drDescuentosLinea)

            'Columna que se visualiza con el boton para mostrar el detalle de impuestos
            If objImporteImpuestos(1) = String.Empty Then
                If m_bEditable Then
                    .Item(sender.Columns.FromKey("IMPUESTOS").Index).Text = "<table style='width:100%'><tr><td style='align:left'><span style='align:right; color:red;'>" & oTextos(62).Item(1) & "</span></td><td style='align:right'><div style='text-align:right'><img  src='" & _RutaImagenes & "3puntos.JPG' onclick='AbrirPanelImpuestos(0)' style='align:right; cursor: pointer;'/></div></td></tr></table>"
                Else
                    .Item(sender.Columns.FromKey("IMPUESTOS").Index).Text = ""
                End If
            Else
                .Item(sender.Columns.FromKey("IMPUESTOS").Index).Text = "<table style='width:100%'><tr><td style='align:left'><span style='align:right;'>" & objImporteImpuestos(1) & "</span></td><td style='align:right'><div style='text-align:right'><img src='" & _RutaImagenes & "3puntos.JPG' onclick='AbrirPanelImpuestos(1)' style='align:right; cursor: pointer;'/></div></td></tr></table>"
            End If

            'Columna que no se visualiza con el valor de los impuestos
            .Item(sender.Columns.FromKey("TOTAL_IMP").Index).Value = CDbl(objImporteImpuestos(0))

            'Columna que no se visualiza con el valor de los Costes
            .Item(sender.Columns.FromKey("TOTAL_COSTES").Index).Value = CDbl(importeCostes)
            .Item(sender.Columns.FromKey("TOTAL_COSTES_FORMATEADO").Index).Value = FormatNumber(Replace(importeCostes, ".", ","), m_oNumberFormat) & " " & e.Row.Items.Item(sender.Columns.FromKey("MON").Index).Value

            'Columna que se visualiza con el boton para mostrar el detalle de costes
            If importeCostes > 0 OrElse m_bEditable Then
                .Item(sender.Columns.FromKey("TOTAL_COSTES_VISIBLE").Index).Text = "<table style='width:100%'><tr><td style='align:left'><span style='align:right;'>" & FormatNumber(Replace(importeCostes, ".", ","), m_oNumberFormat) & " " & e.Row.Items.Item(sender.Columns.FromKey("MON").Index).Value & "</span></td>"
                If importeCostes > 0 OrElse m_bCostesPorLinea OrElse (Not m_bCostesPorLinea AndAlso .Item(sender.Columns.FromKey("ALBARAN").Index).Text <> "") Then
                    .Item(sender.Columns.FromKey("TOTAL_COSTES_VISIBLE").Index).Text = .Item(sender.Columns.FromKey("TOTAL_COSTES_VISIBLE").Index).Text & "<td style='align:right'><div style='text-align:right'><img src='" & _RutaImagenes & "3puntos.JPG' onclick='AbrirPanelCostesLinea()' style='align:right; cursor: pointer;'/></div></td>"
                End If
                .Item(sender.Columns.FromKey("TOTAL_COSTES_VISIBLE").Index).Text = .Item(sender.Columns.FromKey("TOTAL_COSTES_VISIBLE").Index).Text & "</tr></table>"
            Else
                .Item(sender.Columns.FromKey("TOTAL_COSTES_VISIBLE").Index).Text = "<div style='text-align:right; padding-right:5px'>" & FormatNumber(Replace(importeCostes, ".", ","), m_oNumberFormat) & " " & e.Row.Items.Item(sender.Columns.FromKey("MON").Index).Value & "</div>"
            End If


            'Columna que no se visualiza con el valor de los Descuentos
            .Item(sender.Columns.FromKey("TOTAL_DCTOS").Index).Value = CDbl(importeDescuentos)
            .Item(sender.Columns.FromKey("TOTAL_DCTOS_FORMATEADO").Index).Value = FormatNumber(Replace(importeDescuentos, ".", ","), m_oNumberFormat) & " " & e.Row.Items.Item(sender.Columns.FromKey("MON").Index).Value

            'Columna que se visualiza con el boton para mostrar el detalle de descuentos
            If importeDescuentos > 0 OrElse m_bEditable Then
                .Item(sender.Columns.FromKey("TOTAL_DCTOS_VISIBLE").Index).Text = "<table style='width:100%'><tr><td style='align:left'><span style='align:right;'>" & FormatNumber(Replace(importeDescuentos, ".", ","), m_oNumberFormat) & " " & e.Row.Items.Item(sender.Columns.FromKey("MON").Index).Value & "</span></td>"
                If importeDescuentos > 0 OrElse m_bCostesPorLinea OrElse (Not m_bCostesPorLinea AndAlso .Item(sender.Columns.FromKey("ALBARAN").Index).Text <> "") Then
                    .Item(sender.Columns.FromKey("TOTAL_DCTOS_VISIBLE").Index).Text = .Item(sender.Columns.FromKey("TOTAL_DCTOS_VISIBLE").Index).Text & "<td style='align:right'><div style='text-align:right'><img src='" & _RutaImagenes & "3puntos.JPG' onclick='AbrirPanelDescuentosLinea()' style='align:right; cursor: pointer;'/></div></td>"
                End If
                .Item(sender.Columns.FromKey("TOTAL_DCTOS_VISIBLE").Index).Text = .Item(sender.Columns.FromKey("TOTAL_DCTOS_VISIBLE").Index).Text & "</tr></table>"
            Else
                .Item(sender.Columns.FromKey("TOTAL_DCTOS_VISIBLE").Index).Text = "<div style='text-align:right; padding-right:5px'>" & FormatNumber(Replace(importeDescuentos, ".", ","), m_oNumberFormat) & " " & e.Row.Items.Item(sender.Columns.FromKey("MON").Index).Value & "</div>"
            End If

            Dim dImporteTotalLinea As Double
            Dim dImporteSinImpuestos As Double
            dImporteTotalLinea = DBNullToDbl(.Item(sender.Columns.FromKey("PREC_UP").Index).Value) * DBNullToDbl(e.Row.Items.Item(sender.Columns.FromKey("CANT_PED").Index).Value)
            dImporteTotalLinea += DBNullToDbl(Replace(.Item(sender.Columns.FromKey("TOTAL_COSTES").Index).Value, m_sMoneda, ""))
            dImporteTotalLinea -= DBNullToDbl(.Item(sender.Columns.FromKey("TOTAL_DCTOS").Index).Value)
            dImporteSinImpuestos = dImporteTotalLinea
            dImporteTotalLinea += DBNullToDbl(objImporteImpuestos(0))

            .Item(sender.Columns.FromKey("IMPORTE_SIN_IMP_FORMATEADO").Index).Value = FormatNumber(Replace(dImporteSinImpuestos, ".", ","), m_oNumberFormat) & " " & .Item(sender.Columns.FromKey("MON").Index).Value

            .Item(sender.Columns.FromKey("IMPORTE").Index).Text = "<div style='text-align:right; padding-right:5px'>" & FormatNumber(Replace(dImporteTotalLinea, ".", ","), m_oNumberFormat) & " " & .Item(sender.Columns.FromKey("MON").Index).Value & "</div>"
            .Item(sender.Columns.FromKey("IMPORTE").Index).CssClass = "Subrayado itemSeleccionable"

            importeBruto += DBNullToDbl(.Item(sender.Columns.FromKey("CANT_PED").Index).Value) * DBNullToDbl(.Item(sender.Columns.FromKey("PREC_UP").Index).Value)
            importeNeto += (DBNullToDbl(.Item(sender.Columns.FromKey("CANT_PED").Index).Value) * DBNullToDbl(.Item(sender.Columns.FromKey("PREC_UP").Index).Value)) + DBNullToDbl(.Item(sender.Columns.FromKey("TOTAL_COSTES").Index).Value) - DBNullToDbl(.Item(sender.Columns.FromKey("TOTAL_DCTOS").Index).Value)
            importeImpuestos += DBNullToDbl(objImporteImpuestos(0))
            importeTotal = importeNeto + importeImpuestos
            lblImpuestosDato.Text = FormatNumber(Replace(importeImpuestos, ".", ","), m_oNumberFormat) & " " & m_sMoneda
            lblImporteBrutoDato.Text = FormatNumber(Replace(importeBruto, ".", ","), m_oNumberFormat) & " " & m_sMoneda
            lblImporteNetoDato.Text = FormatNumber(Replace(importeNeto, ".", ","), m_oNumberFormat) & " " & m_sMoneda
            _ImporteTotalLineasConCD = importeNeto
            _ImporteTotalLineasConCDYImpuestos = importeTotal
            lblImporteTotalDato.Text = FormatNumber(Replace(importeTotal, ".", ","), m_oNumberFormat) & " " & m_sMoneda

            'Actualizamos el datatable con los costes, descuentos e impuestos
            If m_bEditable Then
                Dim dtLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
                Dim drLinea() As DataRow = dtLinea.Select("LINEA=" & e.Row.Items.Item(sender.Columns.FromKey("LINEA").Index).Value)
                drLinea(0)("TOTAL_IMP") = CDbl(objImporteImpuestos(0))
                drLinea(0)("TOTAL_COSTES") = CDbl(importeCostes)
                drLinea(0)("TOTAL_DCTOS") = CDbl(importeDescuentos)
                drLinea(0)("IMPORTE_SIN_IMP") = dImporteSinImpuestos
                drLinea(0)("IMPORTE") = dImporteTotalLinea
                drLinea(0).AcceptChanges()
            End If

            'Discrepancias
            If Not DBNullToInteger(drLineaFactura(0)("TIENE_DISCREP")) > 0 Then
                e.Row.Items.FindItemByKey("TIENE_DISCREP").Text = "<img id='imgDiscrepancias_" & CType(e.Row.DataItem.Item.Row.Item("LINEA"), Integer) & "' src='" & _RutaImagenes & "Discrepancia.png" & "' class='Link' style='text-align:center; display:none;'/>"
                e.Row.Items.FindItemByKey("TIENE_DISCREP").Text &= "<img id='imgDiscrepanciasCerradas_" & CType(e.Row.DataItem.Item.Row.Item("LINEA"), Integer) & "' src='" & _RutaImagenes & "Discrepancias_cerradas.png" & "' class='Link' style='text-align:center; display:none;'/>"
            Else
                If DBNullToInteger(drLineaFactura(0)("TIENE_DISCREP_ABTAS")) > 0 Then
                    e.Row.Items.FindItemByKey("TIENE_DISCREP").Text = "<img id='imgDiscrepancias_" & CType(e.Row.DataItem.Item.Row.Item("LINEA"), Integer) & "' src='" & _RutaImagenes & "Discrepancia.png" & "' class='Link' style='text-align:center;'/>"
                    e.Row.Items.FindItemByKey("TIENE_DISCREP").Text &= "<img id='imgDiscrepanciasCerradas_" & CType(e.Row.DataItem.Item.Row.Item("LINEA"), Integer) & "' src='" & _RutaImagenes & "Discrepancias_cerradas.png" & "' class='Link' style='text-align:center; display:none;'/>"
                Else
                    e.Row.Items.FindItemByKey("TIENE_DISCREP").Text = "<img id='imgDiscrepancias_" & CType(e.Row.DataItem.Item.Row.Item("LINEA"), Integer) & "' src='" & _RutaImagenes & "Discrepancia.png" & "' class='Link' style='text-align:center; display:none;'/>"
                    e.Row.Items.FindItemByKey("TIENE_DISCREP").Text &= "<img id='imgDiscrepanciasCerradas_" & CType(e.Row.DataItem.Item.Row.Item("LINEA"), Integer) & "' src='" & _RutaImagenes & "Discrepancias_cerradas.png" & "' class='Link' style='text-align:center;'/>"
                End If
            End If
            'Partidas
            If m_bAccesoSM Then
                If DBNullToSomething(.Item(sender.Columns.FromKey("PARTIDA").Index).Value) <> Nothing Then
                    Dim sPres5 As String = .Item(sender.Columns.FromKey("PRES0").Index).Value
                    .Item(sender.Columns.FromKey("PARTIDA_" & sPres5).index).Text = .Item(sender.Columns.FromKey("PARTIDA").Index).Value
                End If
            End If
        End With

    End Sub

#End Region
    ''' <summary>
    ''' Carga de la pagina
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarTextos()

            CreateColumns()

            CargarLineasFactura()

            If m_bEditable Then
                'Combos del Panel de buscador de impuestos
                CargarComboPaisesImpuestos()
                'Combo del Panel de buscador de costes
                CargarComboCostesUtilizados()
                'Combo del Panel de buscador de descuentos
                CargarComboDescuentosUtilizados()
                CargarComboConceptoImpuestos()
                If Not m_bCostesPorLinea Then
                    lblTipoCoste_BusqCostesLinea.CssClass = "Etiqueta"
                    lblTipoCoste_BusqCostesLinea.Text = Replace(oTextos(35).Item(1), "X", "")
                    lblAlbaran_BusqCostesLinea.CssClass = "Etiqueta"
                    FilaTipoCostes.Visible = False
                    lblTipoDescuento_BusqDescuentosLinea.CssClass = "Etiqueta"
                    lblTipoDescuento_BusqDescuentosLinea.Text = Replace(oTextos(36).Item(1), "X", "")
                    lblAlbaran_BusqDescuentosLinea.CssClass = "Etiqueta"
                    FilaTipoDescuento.Visible = False
                End If
            End If

            If m_bMostrarLeyendaOtrosGestores Then LeyendaOtrosGestores.Style.Add("display", "")
            If m_dtImpuestosRepercutidos.Rows.Count = 0 AndAlso m_dtImpuestosRetenidos.Rows.Count = 0 Then pnlResumenImpuestos.Style.Add("display", "none")

            If m_dtImpuestosRepercutidos.Rows.Count > 0 Then
                rpImpuestosRepercutidos.DataSource = m_dtImpuestosRepercutidos
                rpImpuestosRepercutidos.DataBind()
            End If

            If m_dtImpuestosRetenidos.Rows.Count > 0 Then
                rpImpuestosRetenidos.DataSource = m_dtImpuestosRetenidos
                rpImpuestosRetenidos.DataBind()
            End If


            'Inicializo a -1 el nuevo id q ue se insertara  a los nuevos impuestos, a partir de ahi ira decreciendo con los nuevos impuestos (-2,-3....)
            hid_UltimoNuevoIdImpuesto.Value = -1
            'Inicializo a -1 el nuevo id q ue se insertara  a los nuevos costes, a partir de ahi ira decreciendo con los nuevos impuestos (-2,-3....)
            hid_UltimoNuevoIdCoste.Value = -1

            hid_LineasEliminadas.Value = String.Empty
            Session("LineasEliminadas") = String.Empty

            CargarUrlImagenes()
        Else
            CargarLineasFactura()
        End If
        If Not ScriptManager.GetCurrent(Me.Page).IsInAsyncPostBack Then
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosDiscrepancias") Then
                Dim sVariableJavascriptTextosDiscrepancias As String = "var TextosDiscrepancias = new Array();"
                For i As Integer = 121 To 140
                    sVariableJavascriptTextosDiscrepancias &= "TextosDiscrepancias[" & i - 120 & "]='" & JSText(oTextos(i).Item(1)) & "';"
                Next
                sVariableJavascriptTextosDiscrepancias &= "TextosDiscrepancias[21]='" & JSText(oTextos(154).Item(1)) & "';"
                sVariableJavascriptTextosDiscrepancias &= "TextosDiscrepancias[22]='" & JSText(oTextos(155).Item(1)) & "';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosDiscrepancias", sVariableJavascriptTextosDiscrepancias, True)
            End If
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumeroFactura") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumeroFactura", "var numFactura='" & m_sNumFactura & "'; var CodProve='" & m_sCodProve & "';", True)
            End If
            If m_bEditable Then
                WriteScripts()
            End If
        End If
    End Sub
    Private Sub CargarUrlImagenes()
        imgImpuestosFacturas.Src = _RutaImagenes & "impuestos.jpg"
        imgDetalleImpuesto.Src = _RutaImagenes & "impuestos.jpg"
        imgAgregarImpuesto.ImageUrl = _RutaImagenes & "Anyadir.png"
        imgCerrarDetalleimpuestos.ImageUrl = _RutaImagenes & "Cerrar.png"
        imgCerrarImpuestosFacturas.ImageUrl = _RutaImagenes & "Cerrar.png"
        imgCerrarPanelMotivoImpuestoExento.ImageUrl = _RutaImagenes & "Cerrar.png"
        imgMotivoImpuestoExento.Src = _RutaImagenes & "impuestos.jpg"
        imgDetalleCoste.Src = _RutaImagenes & "impuestos.jpg"
        imgAgregarCoste.ImageUrl = _RutaImagenes & "Anyadir.png"
        imgCerrarDetalleCostes.ImageUrl = _RutaImagenes & "Cerrar.png"
        imgCerrarCostesFacturas.ImageUrl = _RutaImagenes & "Cerrar.png"
        imgCerrarObservaciones.ImageUrl = _RutaImagenes & "Cerrar.png"
        imgCostesFacturas.Src = _RutaImagenes & "impuestos.jpg"
        imgDetalleDescuento.Src = _RutaImagenes & "impuestos.jpg"
        imgAgregarDescuento.ImageUrl = _RutaImagenes & "Anyadir.png"
        imgCerrarDetalleDescuentos.ImageUrl = _RutaImagenes & "Cerrar.png"
        imgCerrarDescuentosFacturas.ImageUrl = _RutaImagenes & "Cerrar.png"
        imgDescuentosFacturas.Src = _RutaImagenes & "impuestos.jpg"
        imgCerrarCalculoImporte.ImageUrl = _RutaImagenes & "Cerrar.png"
        imgCalculoImporte.Src = _RutaImagenes & "impuestos.jpg"
        imgCerrarDetalleAlbaran.ImageUrl = _RutaImagenes & "Cerrar.png"
        imgCerrarConfirm.ImageUrl = _RutaImagenes & "Cerrar.png"
        imgConfirm.ImageUrl = _RutaImagenes & "Icono_Error_Amarillo_40x40.gif"
        FSNPanelDatosDetallePedido.ServicePath = m_sPathServicio
        FSNPanelDatosDetallePedido.Titulo = oTextos(73).Item(1)
        FSNPanelDatosDetallePedido.ImagenPopUp = _RutaImagenes & "camion.jpg"

        imgNuevaDiscrepancia.ImageUrl = _RutaImagenes & "Discrepancia.png"
    End Sub
    Private Sub CargarTextos()
        lblTituloLineas.Text = oTextos(93).Item(1)

        If m_bEditable Then
            'Panel de buscador de impuestos
            lblBuscadorImpuestos.Text = oTextos(67).Item(1)
            lblCodigo.Text = String.Format("{0}:", oTextos(39).Item(1))
            lblDescripcion.Text = String.Format("{0}:", oTextos(40).Item(1))
            lblPais.Text = String.Format("{0}:", oTextos(103).Item(1))
            lblProvincia.Text = String.Format("{0}:", oTextos(104).Item(1))
            lblMaterial.Text = String.Format("{0}:", oTextos(41).Item(1))
            lblArticulo.Text = String.Format("{0}:", oTextos(42).Item(1))
            chkImp_repercutido.Text = oTextos(105).Item(1)
            chkImp_retenido.Text = oTextos(106).Item(1)
            btnBuscarImpuestos.Text = oTextos(47).Item(1)
            btnLimpiarImpuestos.Text = oTextos(48).Item(1)
            btnAceptarImpuesto.Text = oTextos(49).Item(1)
            btnCancelarImpuesto.Text = oTextos(50).Item(1)
            lblAvisoErrorImpuesto.Text = oTextos(107).Item(1)
            lblConceptoAplicable.Text = oTextos(145).Item(1)

            'Panel de buscador de costes por linea de factura
            lblBuscadorCostes.Text = oTextos(7).Item(1)
            lblTipoCoste_BusqCostesLinea.Text = String.Format("{0}:", oTextos(31).Item(1))
            rbCosteLinea.Text = Replace(oTextos(33).Item(1), "X", "")
            rbCosteAlbaran.Text = Replace(oTextos(35).Item(1), "X", "")
            lblCostesSeleccionados.Text = String.Format("{0}:", oTextos(37).Item(1))
            lblCodigo_BusqCostesLinea.Text = String.Format("{0}:", oTextos(39).Item(1))
            lblDescripcion_BusqCostesLinea.Text = String.Format("{0}:", oTextos(40).Item(1))
            lblFamMateriales_BusqCostesLinea.Text = String.Format("{0}:", oTextos(41).Item(1))
            lblArticulo_BusqCostesLinea.Text = String.Format("{0}:", oTextos(42).Item(1))
            btnBuscarCostes.Text = oTextos(47).Item(1)
            btnLimpiarCostes.Text = oTextos(48).Item(1)
            lblSinResultado.Text = oTextos(43).Item(1)
            lblLitCosteGenerico.Text = String.Format("{0}:", oTextos(45).Item(1))
            btnAceptarCoste.Text = oTextos(49).Item(1)
            btnCancelarCoste.Text = oTextos(50).Item(1)

            'Panel de buscador de descuentos por linea de factura
            lblBuscadorDescuentos.Text = oTextos(10).Item(1)
            lblTipoDescuento_BusqDescuentosLinea.Text = String.Format("{0}:", oTextos(32).Item(1))
            rbDescuentoLinea.Text = Replace(oTextos(34).Item(1), "X", "")
            rbDescuentoAlbaran.Text = Replace(oTextos(36).Item(1), "X", "")
            lblDescuentosSeleccionados.Text = String.Format("{0}:", oTextos(37).Item(1))
            lblCodigo_BusqDescuentosLinea.Text = String.Format("{0}:", oTextos(39).Item(1))
            lblDescripcion_BusqDescuentosLinea.Text = String.Format("{0}:", oTextos(40).Item(1))
            lblFamMateriales_BusqDescuentosLinea.Text = String.Format("{0}:", oTextos(41).Item(1))
            lblArticulo_BusqDescuentosLinea.Text = String.Format("{0}:", oTextos(42).Item(1))
            btnBuscarDescuentos.Text = oTextos(47).Item(1)
            btnLimpiarDescuentos.Text = oTextos(48).Item(1)
            lblSinResultadoDescuentos.Text = oTextos(44).Item(1)
            lblLitDescuentoGenerico.Text = String.Format("{0}:", oTextos(46).Item(1))
            btnAceptarDescuento.Text = oTextos(49).Item(1)
            btnCancelarDescuento.Text = oTextos(50).Item(1)

            'Panel de detalle de los impuestos
            lblAgregarImpuesto.Text = oTextos(98).Item(1)

            'Panel de detalle de los costes de las lineas de factura
            lblLiteralCostes.Text = oTextos(77).Item(1)
            lblAgregarCoste.Text = oTextos(108).Item(1)

            'Panel de detalle de los descuentos de las lineas de factura
            lblLiteralDescuentos.Text = oTextos(80).Item(1)
            lblAgregarDescuento.Text = oTextos(109).Item(1)

            With whdgImpuestos.Columns
                If .Item("COD") IsNot Nothing Then .Item("COD").Header.Text = oTextos(39).Item(1)
                If .Item("DEN") IsNot Nothing Then .Item("DEN").Header.Text = oTextos(63).Item(1)
                If .Item("RETENIDO") IsNot Nothing Then .Item("RETENIDO").Header.Text = oTextos(99).Item(1)
                If .Item("PORCENTAJE") IsNot Nothing Then .Item("PORCENTAJE").Header.Text = oTextos(100).Item(1)
            End With

            With whdgBuscadorCostes.Columns
                If .Item("COD") IsNot Nothing Then .Item("COD").Header.Text = oTextos(39).Item(1)
                If .Item("DEN") IsNot Nothing Then .Item("DEN").Header.Text = oTextos(63).Item(1)
            End With

            With whdgBuscadorDescuentos.Columns
                If .Item("COD") IsNot Nothing Then .Item("COD").Header.Text = oTextos(39).Item(1)
                If .Item("DEN") IsNot Nothing Then .Item("DEN").Header.Text = oTextos(63).Item(1)
            End With
        End If

        'Totales detalle líneas
        lblImporteBruto.Text = String.Format("{0}:", oTextos(75).Item(1))
        lblImporteNeto.Text = String.Format("{0}:", oTextos(94).Item(1))
        lblImpuestos.Text = String.Format("{0}:", oTextos(67).Item(1))
        lblImporteTotal.Text = String.Format("{0}:", oTextos(17).Item(1))

        'Panel de cálculo del importe
        lblTituloCalculoImporte.Text = oTextos(91).Item(1)
        lblCILitImporteBruto.Text = String.Format("{0}:", oTextos(75).Item(1))
        lblCILitTotalCostes.Text = String.Format("{0}:", oTextos(23).Item(1))
        lblCILitTotalDescuentos.Text = String.Format("{0}:", oTextos(24).Item(1))
        lblCILitImporteSinImpuestos.Text = String.Format("{0}:", oTextos(92).Item(1))
        lblCILitImporteTotal.Text = String.Format("{0}:", oTextos(17).Item(1))

        'Panel de motivo de exencion de un impuesto
        lblMotivoImpuestoExento.Text = oTextos(67).Item(1)
        lblImpuestoSeleccionado.Text = String.Format("{0}:", oTextos(110).Item(1))
        lblMotivo.Text = String.Format("{0}:", oTextos(111).Item(1))
        btnAceptarMotivImpExento.Text = oTextos(49).Item(1)
        btnCancelarMotivImpExento.Text = oTextos(50).Item(1)
        lblResumenImpuestos.Text = oTextos(112).Item(1)

        'Panel de observaciones
        lblTituloObservaciones.Text = oTextos(12).Item(1)
        hSinObservaciones.Value = oTextos(119).Item(1)

        'Discrepancias
        lblNuevaDiscrepancia.Text = oTextos(120).Item(1)
        lblConfirm.Text = oTextos(149).Item(1) & vbCrLf & oTextos(150).Item(1) 'El impuesto que ha seleccionado no pertenece al mismo grupo de impuestos que el resto de impuestos que ya ha seleccionado en la factura. '¿Desea modificar el impuesto en todas las líneas?
        btnAceptarConfirm.Text = oTextos(49).Item(1) 'Aceptar
        btnCancelarConfirm.Text = oTextos(50).Item(1) 'Cancelar

        'Gestores
        lblLeyendaOtrosGestores.Text = oTextos(151).Item(1)
    End Sub
    ''' <summary>
    ''' Procedimiento que crea las columnas del grid de lineas de facturas
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub CreateColumns()
        AddColumn("TIENE_DISCREP", oTextos(152).Item(1), 40)
        AddColumn("FACTURA", "", 0, False)
        AddColumn("LINEA", "", 0, False)
        AddColumnUnbound("LINEA_GRID", oTextos(26).Item(1), 35)
        AddColumn("ART_INT", oTextos(39).Item(1), 90)
        AddColumn("ART_DEN", oTextos(63).Item(1), 190)
        AddColumn("CANT_PED", oTextos(64).Item(1), 50)
        AddColumn("UP", oTextos(65).Item(1), 0, False)
        AddColumn("MON", "", 0, False)
        AddColumn("PREC_UP", oTextos(66).Item(1), 80)
        AddColumnUnbound("PREC_UP_POR_CANT_FORMATEADO", "", 0, False)
        AddColumnUnbound("TOTAL_COSTES", "", 0, False)
        AddColumnUnbound("TOTAL_COSTES_FORMATEADO", "", 0, False)
        AddColumnUnbound("TOTAL_COSTES_VISIBLE", oTextos(7).Item(1), 110, )
        AddColumnUnbound("TOTAL_DCTOS", "", 0, False)
        AddColumnUnbound("TOTAL_DCTOS_FORMATEADO", "", 0, False)
        AddColumnUnbound("TOTAL_DCTOS_VISIBLE", oTextos(10).Item(1), 110, )
        AddColumnUnbound("IMPUESTOS", oTextos(67).Item(1), 270, )
        AddColumnUnbound("TOTAL_IMP", "", 0, False)
        AddColumn("IMPORTE", oTextos(68).Item(1), 100)
        AddColumn("IMPORTE_SIN_IMP", "", 0, False)
        AddColumnUnbound("IMPORTE_SIN_IMP_FORMATEADO", "", 0, False)
        AddColumn("PEDIDO", oTextos(69).Item(1), 100)
        AddColumn("NUM_PEDIDO", "", 0, False)
        AddColumn("NUM_PED_ERP", oTextos(70).Item(1), 120)
        AddColumn("OBS", oTextos(71).Item(1), 30)
        AddColumn("ALBARAN", oTextos(28).Item(1), 80)
        AddColumn("PARTIDA", "", 0, False)
        AddColumn("PRES0", "", 0, False)
        If m_bAccesoSM Then
            AddColumn("CENTRO", oTextos(72).Item(1), 200)
            For Each orow As DataRow In _dtPartidas.Rows
                AddColumnUnbound("PARTIDA_" & orow("PRES5"), orow("DEN"), 200)
            Next
        End If
    End Sub
    ''' <summary>
    ''' Añade una columna que esta enlazada a fuente de datos al grid de lineas de factura
    ''' </summary>
    ''' <param name="fieldName"></param>
    ''' <param name="headerText"></param>
    ''' <param name="Width"></param>
    ''' <param name="Visible"></param>
    ''' <remarks></remarks>
    Private Sub AddColumn(ByVal fieldName As String, ByVal headerText As String, ByVal Width As Integer, Optional ByVal Visible As Boolean = True)
        Dim field As New BoundDataField(True)
        field.Key = fieldName
        field.DataFieldName = fieldName
        field.Header.Text = headerText
        field.Hidden = Not Visible
        field.Width = Unit.Pixel(Width)
        field.CssClass = "headerNoWrap"
        Me.whdgLineas.Columns.Add(field)
    End Sub
    ''' <summary>
    ''' Añade una columna que no esta enlazada a fuente de datos al grid de lineas de factura
    ''' </summary>
    ''' <param name="fieldName"></param>
    ''' <param name="headerText"></param>
    ''' <param name="Width"></param>
    ''' <param name="Visible"></param>
    ''' <remarks></remarks>
    Private Sub AddColumnUnbound(ByVal fieldName As String, ByVal headerText As String, ByVal Width As Integer, Optional ByVal Visible As Boolean = True)
        Dim field As New UnboundField(True)
        field.Key = fieldName
        field.Header.Text = headerText
        field.Hidden = Not Visible
        field.Width = Unit.Pixel(Width)
        field.CssClass = "ColsGridDetalleFactura headerNoWrap"
        Me.whdgLineas.Columns.Add(field)
    End Sub
    ''' <summary>
    ''' Procedimiento que registra scripts de los controles de pantalla
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub WriteScripts()
        Me.lblAgregarImpuesto.Attributes.Add("onclick", "AbrirBuscadorImpuestos()")
        Me.imgAgregarImpuesto.Attributes.Add("onclick", "AbrirBuscadorImpuestos()")
        Me.lblAgregarCoste.Attributes.Add("onclick", "AbrirBuscadorCostes()")
        Me.imgAgregarCoste.Attributes.Add("onclick", "AbrirBuscadorCostes()")
        Me.lblAgregarDescuento.Attributes.Add("onclick", "AbrirBuscadorDescuentos()")
        Me.imgAgregarDescuento.Attributes.Add("onclick", "AbrirBuscadorDescuentos()")

        'Buscador de impuestos
        imgArticuloLupa.Attributes.Add("onclick", "javascript:window.open('" & _RutaBuscadorArticulo & If(InStr(_RutaBuscadorArticulo, "?") > 0, "&", "?") & "ClientId=" & txtArticulo.ClientID & "&idHidControl=" & hidArticulo.ClientID & "&Origen=LineasFacturas', '_blank'," & If(_PropiedadesBuscadorArticulo <> Nothing, "'" & _PropiedadesBuscadorArticulo & "'", "'width=500,height=400,status=yes,resizable=no,top=200,left=200'") & ");return false;")
        imgMaterialLupa.Attributes.Add("onclick", "javascript:window.open('" & _RutaBuscadorMaterial & If(InStr(_RutaBuscadorMaterial, "?") > 0, "&", "?") & "idControl=" & txtMaterial.ClientID & "&ClientId=" & hidMaterial.ClientID & "&MaterialGS_BBDD=" & hidMaterial.ClientID & "&SessionId=" & HttpContext.Current.Session("sSession") & "&Origen=LineasFacturas', '_blank', 'width=550,height=550,status=yes,resizable=no,top=200,left=200');return false;")

        'Buscador de costes de linea
        imgArticuloLupaCostes.Attributes.Add("onclick", "javascript:window.open('" & _RutaBuscadorArticulo & If(InStr(_RutaBuscadorArticulo, "?") > 0, "&", "?") & "ClientId=" & txtArticulo_BusqCosteLinea.ClientID & "&idHidControl=" & hidArticuloCostes.ClientID & "&Origen=LineasFacturas', '_blank', " & If(_PropiedadesBuscadorArticulo <> Nothing, "'" & _PropiedadesBuscadorArticulo & "'", "'width=500,height=400,status=yes,resizable=no,top=200,left=200'") & ");return false;")
        imgMaterialLupaCostes.Attributes.Add("onclick", "javascript:window.open('" & _RutaBuscadorMaterial & If(InStr(_RutaBuscadorMaterial, "?") > 0, "&", "?") & "idControl=" & txtMaterial_BusqCosteLinea.ClientID & "&ClientId=" & hidMaterialCostes.ClientID & "&MaterialGS_BBDD=" & hidMaterialCostes.ClientID & "&SessionId=" & HttpContext.Current.Session("sSession") & "&Origen=LineasFacturas', '_blank', 'width=550,height=550,status=yes,resizable=no,top=200,left=200');return false;")

        'Buscador de descuentos de linea
        imgArticuloLupaDescuentos.Attributes.Add("onclick", "javascript:window.open('" & _RutaBuscadorArticulo & If(InStr(_RutaBuscadorArticulo, "?") > 0, "&", "?") & "ClientId=" & txtArticulo_BusqDescuentosLinea.ClientID & "&idHidControl=" & hidArticuloDescuentos.ClientID & "&Origen=LineasFacturas', '_blank', " & If(_PropiedadesBuscadorArticulo <> Nothing, "'" & _PropiedadesBuscadorArticulo & "'", "'width=500,height=400,status=yes,resizable=no,top=200,left=200'") & ");return false;")
        imgMaterialLupaDescuentos.Attributes.Add("onclick", "javascript:window.open('" & _RutaBuscadorMaterial & If(InStr(_RutaBuscadorMaterial, "?") > 0, "&", "?") & "idControl=" & txtMaterial_BusqDescuentosLinea.ClientID & "&ClientId=" & hidMaterialDescuentos.ClientID & "&MaterialGS_BBDD=" & hidMaterialDescuentos.ClientID & "&SessionId=" & HttpContext.Current.Session("sSession") & "&Origen=LineasFacturas', '_blank', 'width=550,height=550,status=yes,resizable=no,top=200,left=200');return false;")


        txtArticulo.Attributes.Add("onKeyDown", "return ControlArticuloImpuestos(this, event);")
        txtArticulo_BusqCosteLinea.Attributes.Add("onKeyDown", "return ControlArticuloCostes(this, event);")
        txtArticulo_BusqDescuentosLinea.Attributes.Add("onKeyDown", "return ControlArticuloDescuentos(this, event);")


        wddPaises.ClientEvents.DropDownClosing = "cambioPais"
        If Not Page.ClientScript.IsClientScriptBlockRegistered("cambioPais") Then
            Dim sScript As String = "function cambioPais(sender, eventArgs) {" & vbCrLf & _
                " var comboProv = $find('" & wddProvincias.ClientID & "')" & vbCrLf & _
                    " comboProv.loadItems();" & vbCrLf & _
                " } " & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "cambioPais", sScript, True)
        End If
    End Sub
#Region "IMPUESTOS"

    ''' <summary>
    ''' Procedimiento que carga en el grid los impuestos filtrando por unos criterios establecidos
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnBuscarImpuestos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarImpuestos.Click
        'Limpiamos la selección  de la grid
        whdgImpuestos.GridView.Behaviors.Selection.SelectedRows.Clear()

        Dim sPais As String = String.Empty
        Dim sProv As String = String.Empty
        Dim iConceptoImpuesto As Nullable(Of TiposDeDatos.ConceptoImpuesto) = Nothing

        If Not wddPaises.SelectedItem Is Nothing Then
            sPais = wddPaises.SelectedItem.Value
        End If

        If Not wddProvincias.SelectedItem Is Nothing Then
            sProv = wddProvincias.SelectedItem.Value
        End If

        If Not wddConceptoImpuestos.SelectedItem Is Nothing Then
            iConceptoImpuesto = CType(wddConceptoImpuestos.SelectedItem.Value, TiposDeDatos.ConceptoImpuesto)
        End If

        RaiseEvent EventoBuscarImpuestos(txtCodigo.Text, txtDescripcion.Text, sPais, sProv, hidArticulo.Value, hidMaterial.Value, chkImp_repercutido.Checked, chkImp_retenido.Checked, iConceptoImpuesto)

        whdgImpuestos.DataSource = m_dsImpuestosBuscador
        whdgImpuestos.DataBind()
        upWhdgImpuestos.Update()
    End Sub

    Private Sub RecalcularImpuestoLinea(Optional ByVal OldValorImp As Double = 0, Optional ByVal LineaARecalcular As Integer = 0)
        Dim linea As String = If(LineaARecalcular > 0, LineaARecalcular, hid_FacturaUpdating.Value) 'Recojo la linea de factura a la que hay que modificar el importe del impuesto
        Dim dtLineasImpuestos As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(10)

        If dtLineasImpuestos.Select("LINEA=" & linea).Length > 0 Then
            Dim dtImpuestosLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
            Dim dtCostesLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(11)
            Dim dtDescuentosLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(13)

            Dim drLineaFactura() As DataRow = dtImpuestosLinea.Select("LINEA=" & linea)
            Dim drCostesLinea() As DataRow = dtCostesLinea.Select("LINEA=" & linea)
            Dim drDescuentosLinea() As DataRow = dtDescuentosLinea.Select("LINEA=" & linea)

            Dim importeCostes As Double
            Dim importeDescuentos As Double
            Dim importeImpuesto As Double

            For Each drCoste As DataRow In drCostesLinea
                importeCostes = importeCostes + DBNullToDbl(drCoste("IMPORTE"))
            Next

            For Each drDescuento As DataRow In drDescuentosLinea
                importeDescuentos = importeDescuentos + DBNullToDbl(drDescuento("IMPORTE"))
            Next

            For Each drImpuesto As DataRow In dtLineasImpuestos.Select("LINEA=" & linea)
                'Calculamos el importe del impuesto seleccionado en relacion al importe de la linea de factura
                importeImpuesto = (DBNullToDbl(drLineaFactura(0)("PREC_UP")) * DBNullToDbl(drLineaFactura(0)("CANT_PED")))
                importeImpuesto = importeImpuesto + DBNullToDbl(importeCostes)
                importeImpuesto = importeImpuesto - DBNullToDbl(importeDescuentos)
                importeImpuesto = DBNullToDbl((importeImpuesto * drImpuesto("VALOR"))) / 100

                drImpuesto("IMPORTE") = importeImpuesto
                drImpuesto.AcceptChanges()

                ActualizarResumenImpuestos(drImpuesto("RETENIDO"), drImpuesto, False, True, OldValorImp, linea, drLineaFactura(0)("TOTAL_IMP"))
            Next
        End If
    End Sub

    ''' <summary>
    ''' Boton de aceptar impuesto del buscador de impuestos de lineas de factura
    ''' </summary>
    ''' <param name="sender">Control que lanza el evento</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Max 0,5 seg</remarks>
    Private Sub btnAceptarImpuesto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarImpuesto.Click
        'Dim dtLineasImpuestos As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
        Dim dtImpuestos As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(10)
        Dim filaSeleccionada As Infragistics.Web.UI.GridControls.GridRecord = whdgImpuestos.GridView.Behaviors.Selection.SelectedRows(0)
        Dim bEsCompatible As Boolean = True 'Nos indica si el Grupo de Compatibilidad del impuesto elegido para la línea es el mismo que el de las demás líneas
        Dim linea As String = hid_FacturaUpdating.Value 'Recojo la linea de factura a la que añadiria el impuesto
        Dim GrupoCompatibilidad As Integer = DBNullToInteger(filaSeleccionada.Items.FindItemByKey("GRP_COMP").Value)
        Dim LineasConImpuestosdeOtroGrupo = From lineasImp As DataRow In dtImpuestos _
                                            Where lineasImp("GRP_COMP") <> GrupoCompatibilidad _
                                        Select lineasImp("LINEA") Distinct.ToList
        If LineasConImpuestosdeOtroGrupo.Any Then
            bEsCompatible = False
        End If
        If bEsCompatible Then
            '1. Seleccionamos el impuesto en la línea
            seleccionarImpuesto(linea)
        Else
            '2. Preguntamos si quiere cambiar el impuesto en todas las líneas.
            '''' Para ello, mostramos el panel de confirmación
            btnAceptarConfirm.CommandArgument = "AceptarReemplazarImpuestos"
            Dim script As String = "$('#pnlConfirm').css('display', 'block');centrarPnl('pnlConfirm');"
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, "mostrarPnlConfirm", script, True)
        End If
    End Sub

    ''' <summary>
    ''' Procedimiento que actualizara los importes de los impuestos en el resumen
    ''' </summary>
    ''' <param name="Retenido">0 si no es retenido, 1 si lo es</param>
    ''' <remarks></remarks>
    Private Sub ActualizarResumenImpuestos(ByVal Retenido As Byte, ByVal drImpuesto As DataRow, Optional ByVal bEliminar As Boolean = False, Optional ByVal bSustituirImporte As Boolean = False, Optional ByVal OldValorImp As Double = 0, Optional ByVal Linea As Short = 0, Optional ByVal OldImporteImp As Double = 0)
        pnlResumenImpuestos.Style.Add("display", "")
        Select Case Retenido
            Case 0
                Dim dtResumImpuestosRepercutidos As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(8)
                Dim drResumenImpuesto() As DataRow = dtResumImpuestosRepercutidos.Select("COD='" & drImpuesto("COD") & "' AND VALOR=" & drImpuesto("VALOR"))

                rpImpuestosRepercutidos.Visible = True
                If drResumenImpuesto.Count > 0 Then
                    If bEliminar Then
                        'Si se esta eliminando un impuesto se resta
                        drResumenImpuesto(0)("IMPORTE") -= drImpuesto("IMPORTE")
                        If Decimal.Round(CDec(drResumenImpuesto(0)("IMPORTE")), 4) = 0 Then 'Por el redondeo se quedan decimales Ej: 0.000000000000000027
                            'Si se elimina el unico impuesto de ese tipo se elimina la linea del resumen
                            dtResumImpuestosRepercutidos.Rows.Remove(drResumenImpuesto(0))
                            If dtResumImpuestosRepercutidos.Rows.Count = 0 Then
                                'Si ya no hay lineas de impuestos repercutidos se oculta
                                rpImpuestosRepercutidos.Visible = False
                                If rpImpuestosRetenidos.Visible = False Then
                                    'Si los 2 repeater de impuestos estan ocultos, oculto todo el panel
                                    pnlResumenImpuestos.Style.Add("display", "none")
                                End If
                            End If
                        End If
                    ElseIf bSustituirImporte Then
                        drResumenImpuesto(0)("IMPORTE") = drResumenImpuesto(0)("IMPORTE") + drImpuesto("IMPORTE") - OldImporteImp
                    Else
                        drResumenImpuesto(0)("IMPORTE") += drImpuesto("IMPORTE")
                    End If
                Else
                    'No existia aun en las lineas ese impuesto
                    Dim newRow As DataRow = dtResumImpuestosRepercutidos.NewRow
                    newRow("COD") = drImpuesto("COD")
                    newRow("DEN") = drImpuesto("DEN")
                    newRow("VALOR") = drImpuesto("VALOR")
                    newRow("IMPORTE") = drImpuesto("IMPORTE")
                    newRow("RETENIDO") = 0
                    newRow("COMMENT") = drImpuesto("COMMENT")
                    dtResumImpuestosRepercutidos.Rows.Add(newRow)
                End If
                m_dTotalImpuestosRepercutidos = 0
                rpImpuestosRepercutidos.DataSource = dtResumImpuestosRepercutidos
                rpImpuestosRepercutidos.DataBind()
                upResumentImpuestoRepercutidos.Update()
            Case 1
                Dim dtResumImpuestosRetenidos As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(9)
                Dim dtImpuestosLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(10)

                Dim drLineasConImpuesto() As DataRow = dtImpuestosLinea.Select("COD='" & drImpuesto("COD") & "' AND VALOR=" & DBNullToDbl(drImpuesto("VALOR")))
                Dim drLineasConImpuestoOld() As DataRow = dtImpuestosLinea.Select("COD='" & drImpuesto("COD") & "' AND VALOR=" & OldValorImp)

                Dim drResumenImpuesto() As DataRow

                Dim ImpARestar As Double = 0
                If OldValorImp > 0 Then
                    'Se obtendra el importe antiguo del impuesto para restarlo
                    Dim drLineaOldConImpuesto() As DataRow = dtImpuestosLinea.Select("COD='" & drImpuesto("COD") & "' AND VALOR=" & DBNullToDbl(drImpuesto("VALOR")) & " AND LINEA=" & Linea)
                    ImpARestar = (DBNullToDbl(drLineaOldConImpuesto(0)("IMPORTE")) / DBNullToDbl(drImpuesto("VALOR"))) * OldValorImp
                End If

                If OldValorImp > 0 AndAlso drLineasConImpuesto.Count = 1 AndAlso drLineasConImpuestoOld.Count = 0 Then
                    drResumenImpuesto = dtResumImpuestosRetenidos.Select("COD='" & drImpuesto("COD") & "' AND VALOR=" & OldValorImp)
                ElseIf drLineasConImpuesto.Count = 1 AndAlso drLineasConImpuestoOld.Count = 0 Then
                    drResumenImpuesto = dtResumImpuestosRetenidos.Select("COD='" & drImpuesto("COD") & "'")
                Else
                    drResumenImpuesto = dtResumImpuestosRetenidos.Select("COD='" & drImpuesto("COD") & "' AND VALOR=" & DBNullToDbl(drImpuesto("VALOR")))
                End If

                rpImpuestosRetenidos.Visible = True

                If drResumenImpuesto.Count > 0 Then
                    If drLineasConImpuesto.Count > 1 Then bSustituirImporte = False
                    If bEliminar Then
                        drResumenImpuesto(0)("IMPORTE") -= drImpuesto("IMPORTE")
                        If Decimal.Round(CDec(drResumenImpuesto(0)("IMPORTE")), 4) = 0 Then
                            'Si se elimina el unico impuesto de ese tipo se elimina la linea del resumen
                            dtResumImpuestosRetenidos.Rows.Remove(drResumenImpuesto(0))
                            If dtResumImpuestosRetenidos.Rows.Count = 0 Then
                                'Si ya no hay lineas de impuestos repercutidos se oculta
                                rpImpuestosRetenidos.Visible = False
                                If rpImpuestosRepercutidos.Visible = False Then
                                    'Si los 2 repeater de impuestos estan ocultos, oculto todo el panel
                                    pnlResumenImpuestos.Style.Add("display", "none")
                                End If
                            End If
                        End If
                    ElseIf bSustituirImporte Then
                        drResumenImpuesto(0)("IMPORTE") = drImpuesto("IMPORTE")
                        drResumenImpuesto(0)("VALOR") = drImpuesto("VALOR")
                    Else
                        drResumenImpuesto(0)("IMPORTE") += drImpuesto("IMPORTE")
                        drResumenImpuesto(0)("VALOR") = drImpuesto("VALOR")
                    End If

                Else
                    'No existia aun en las lineas ese impuesto
                    Dim newRow As DataRow = dtResumImpuestosRetenidos.NewRow
                    newRow("COD") = drImpuesto("COD")
                    newRow("DEN") = drImpuesto("DEN")
                    newRow("VALOR") = drImpuesto("VALOR")
                    newRow("IMPORTE") = drImpuesto("IMPORTE")
                    newRow("RETENIDO") = 1
                    newRow("COMMENT") = drImpuesto("COMMENT")
                    dtResumImpuestosRetenidos.Rows.Add(newRow)
                End If
                m_dTotalImpuestosRetenidos = 0

                If drLineasConImpuestoOld.Count = 0 Then
                    'Se comprueba si existe el impuesto antiguo en el resumen para quitarlo
                    Dim drImpuestoResum() As DataRow = dtResumImpuestosRetenidos.Select("COD='" & drImpuesto("COD") & "' AND VALOR=" & OldValorImp)
                    If drImpuestoResum.Count > 0 Then
                        dtResumImpuestosRetenidos.Rows.Remove(drImpuestoResum(0))
                    End If
                End If
                If ImpARestar > 0 Then
                    Dim drImpuestoResum() As DataRow = dtResumImpuestosRetenidos.Select("COD='" & drImpuesto("COD") & "' AND VALOR=" & OldValorImp)
                    If drImpuestoResum.Count > 0 Then
                        drImpuestoResum(0)("IMPORTE") = drImpuestoResum(0)("IMPORTE") - ImpARestar
                    End If
                End If

                rpImpuestosRetenidos.DataSource = dtResumImpuestosRetenidos
                rpImpuestosRetenidos.DataBind()
                upResumenImpuestosRetenidos.Update()
        End Select
        RecalcularImpuestosCostesDescuentosGenerales()
        Dim dt As DataTable = RecalcularImpuestosCabecera()
        RaiseEvent EventoRecalcularImportes(0, 0, dt, 0)
    End Sub

    Private Sub RecalcularImpuestosCostesDescuentosGenerales()
        Dim oDTFactura As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable)
        Dim dtImpuestosRepercutidos As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(8)
        Dim dtImpuestosRetenidos As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(9)

        If DBNullToDbl(oDTFactura.Rows(0).Item("IMPORTE_BRUTO")) > 0 Then
            For Each drImpuesto In dtImpuestosRepercutidos.Rows
                drImpuesto("IMPORTE_CD") = ((DBNullToDbl(drImpuesto("IMPORTE")) * DBNullToDbl(oDTFactura.Rows(0).Item("TOT_COSTES"))) / oDTFactura.Rows(0).Item("IMPORTE_BRUTO"))
            Next
            dtImpuestosRepercutidos.AcceptChanges()

            For Each drImpuesto In dtImpuestosRetenidos.Rows
                drImpuesto("IMPORTE_CD") = ((DBNullToDbl(drImpuesto("IMPORTE")) * DBNullToDbl(oDTFactura.Rows(0).Item("TOT_DESCUENTOS"))) / oDTFactura.Rows(0).Item("IMPORTE_BRUTO"))
            Next
            dtImpuestosRetenidos.AcceptChanges()
        End If
    End Sub

    ''' <summary>
    ''' Funcion que devuelve un datatable con los impuestos agrupados para actualizar el repeater de impuestos de la cabecera(usercontrol de DatosGenerales)
    ''' </summary>
    ''' <remarks></remarks>
    Private Function RecalcularImpuestosCabecera() As DataTable
        'Junto en una misma tabla los impuestos retenidos y repercutidos
        Dim dtImpuestos As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(8).Copy
        dtImpuestos.Merge(CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(9))

        'agrupo los impuestos
        Dim drAll2 = From orders In dtImpuestos.AsEnumerable()
            Where orders.Item("IMPORTE") IsNot DBNull.Value
            Group orders By Den = DBNullToStr(orders.Field(Of String)("DEN")), Retenido = orders.Field(Of Integer)("RETENIDO")
            Into Importe = Sum(DBNullToDbl(orders.Field(Of Double)("IMPORTE"))), Importe_CD = Sum(DBNullToDbl(orders.Field(Of Double)("IMPORTE_CD")))
            Select New With {Importe, Den, Retenido, Importe_CD}

        ' Y lo paso de nuevo a un datatable
        Dim dt As New DataTable
        dt.Columns.Add("DEN")
        dt.Columns.Add("IMPORTE")
        dt.Columns.Add("RETENIDO")
        dt.Columns.Add("IMPORTE_CD")
        Dim newRow As DataRow
        For Each item In drAll2
            newRow = dt.NewRow
            Dim sDenImpuesto As String = item.Den
            If sDenImpuesto.Length > 40 Then _
                sDenImpuesto = item.Den.Substring(0, 40).Trim & "..."
            newRow("DEN") = sDenImpuesto
            newRow("IMPORTE") = item.Importe + item.Importe_CD
            newRow("RETENIDO") = item.Retenido
            dt.Rows.Add(newRow)
        Next
        Return dt
    End Function

    ''' <summary>
    ''' Proceso que carga el combo de paises del panel de impuestos
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub CargarComboPaisesImpuestos()


        Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem

        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = ""
        oItem.Text = ""

        wddProvincias.NullText = ""
        wddProvincias.CurrentValue = ""
        wddPaises.Items.Add(oItem)

        For Each oRow As DataRow In _dtPaises.Rows
            oItem = New Infragistics.Web.UI.ListControls.DropDownItem
            oItem.Value = DBNullToStr(oRow.Item("COD"))
            oItem.Text = DBNullToStr(oRow.Item("DEN"))

            If oItem.Value = _sCodPaisProve Then
                oItem.Selected = True
                wddPaises.Items.Add(oItem)
                wddPaises.CurrentValue = oItem.Text
            Else
                wddPaises.Items.Add(oItem)
            End If
        Next


        wddPaises.EnableAutoFiltering = Infragistics.Web.UI.ListControls.AutoFiltering.Client
        wddPaises.AutoFilterQueryType = Infragistics.Web.UI.ListControls.AutoFilterQueryTypes.Contains
        wddPaises.AutoFilterSortOrder = Infragistics.Web.UI.ListControls.DropDownAutoFilterSortOrder.Ascending
    End Sub

    ''' Revisado por: blp. Fecha: 06/03/2013
    ''' <summary>
    ''' Proceso que carga el combo de conceptos del panel de impuestos
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub CargarComboConceptoImpuestos()
        'En funcion del concepto de factura
        'cargamos en el wddConceptoImpuestos los textos (con otextos): (Gastos y Ambos) o (Inversion y Ambos)
        Dim oDropDownItem As New Infragistics.Web.UI.ListControls.DropDownItem
        If conceptoFactura = TiposDeDatos.ConceptoFactura.Gasto OrElse conceptoFactura = TiposDeDatos.ConceptoFactura.Inversion Then
            If conceptoFactura = TiposDeDatos.ConceptoFactura.Gasto Then
                oDropDownItem.Value = TiposDeDatos.ConceptoImpuesto.Gasto
                oDropDownItem.Text = oTextos(146).Item(1)
                wddConceptoImpuestos.Items.Add(oDropDownItem)
            ElseIf conceptoFactura = TiposDeDatos.ConceptoFactura.Inversion Then
                oDropDownItem.Value = TiposDeDatos.ConceptoImpuesto.Inversion
                oDropDownItem.Text = oTextos(147).Item(1)
                wddConceptoImpuestos.Items.Add(oDropDownItem)
            End If
            oDropDownItem = New Infragistics.Web.UI.ListControls.DropDownItem
            oDropDownItem.Value = TiposDeDatos.ConceptoImpuesto.Ambos
            oDropDownItem.Text = oTextos(148).Item(1)
            wddConceptoImpuestos.Items.Add(oDropDownItem)
            'Seleccionamos el primer registro
            wddConceptoImpuestos.Items(0).Selected = True
        End If
    End Sub

    ''' <summary>
    ''' Concepto de la factura a la que corresponden las líneas de este control
    ''' </summary>
    Private ReadOnly Property conceptoFactura As TiposDeDatos.ConceptoFactura
        Get
            If ViewState("conceptoFactura") IsNot Nothing Then
                Return ViewState("conceptoFactura")
            Else
                If Cache("dsFactura" & m_sSufijoCache) IsNot Nothing Then
                    Dim oConceptoFactura As TiposDeDatos.ConceptoFactura = CType(DBNullToInteger(CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(0).Rows(0).Item("CONCEPTO")), TiposDeDatos.ConceptoFactura)
                    ViewState.Add("conceptoFactura", oConceptoFactura)
                    Return oConceptoFactura
                Else
                    'Si no hay datos en caché mejor refrescamos la pantalla
                    Response.Redirect(Request.RawUrl)
                End If
            End If
        End Get
    End Property

    ''' <summary>
    ''' Carga la combo con las provincias del panel de impuestos
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub CargarComboProvinciasImpuestos()

        wddProvincias.Items.Clear()

        Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem

        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = ""
        oItem.Text = ""

        wddProvincias.Items.Add(oItem)

        For Each oRow As DataRow In _dtProvincias.Rows
            oItem = New Infragistics.Web.UI.ListControls.DropDownItem
            oItem.Value = DBNullToStr(oRow.Item("COD"))
            oItem.Text = DBNullToStr(oRow.Item("DEN"))

            wddProvincias.Items.Add(oItem)
        Next

    End Sub

    ''' <summary>
    ''' Evento que salta al cambiar el valor de la combo TIPO. Carga los valores de subtipo relacionados con el tipo seleccionado.
    ''' </summary>
    ''' <param name="sender">propios del evento</param>
    ''' <param name="e">propios del evento</param>        
    ''' <remarks>Llamada desde; Tiempo mÃ¡ximo=0,1seg.</remarks>
    Private Sub wddProvincias_ItemsRequested(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownItemsRequestedEventArgs) Handles wddProvincias.ItemsRequested
        Dim sCodPais As String


        sCodPais = wddPaises.SelectedValue

        If sCodPais <> "" Then
            RaiseEvent eventProvinciasItemRequested(sCodPais)
        Else
            wddProvincias.Items.Clear()
        End If

    End Sub

    ''' <summary>
    ''' Funcion que calcula el importe de los impuestos de una linea
    ''' </summary>
    ''' <param name="drImpuestos">impuestos para esa linea de factura</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CalculoImporteImpuestoLinea(ByVal drImpuestos() As DataRow) As String()
        Dim objReturn(1) As String
        Dim Importe As Double
        Dim sDescrImpuesto As String = String.Empty
        For Each drImpuesto As DataRow In drImpuestos
            Select Case drImpuesto("RETENIDO")
                Case 0
                    Importe = Importe + DBNullToDbl(drImpuesto("IMPORTE"))
                Case 1
                    Importe = Importe - DBNullToDbl(drImpuesto("IMPORTE"))
            End Select
            If sDescrImpuesto = String.Empty Then
                Dim sDenImpuesto As String = drImpuesto("DEN")
                If sDenImpuesto.Length > 30 Then _
                    sDenImpuesto = sDenImpuesto.Substring(0, 30).Trim & "..."
                sDescrImpuesto = sDenImpuesto & " (" & drImpuesto("VALOR") & "%)"
            Else
                sDescrImpuesto = sDescrImpuesto & "..."
            End If
        Next
        objReturn(0) = Importe.ToString
        objReturn(1) = sDescrImpuesto
        Return objReturn
    End Function

    ''' <summary>
    ''' Evento que se utiliza para mostrar el modalpopup de detalle de albarán
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnMostrarPanelDetalleAlbaran_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMostrarPanelDetalleAlbaran.Click
        CargarPanelDetalleAlbaran()
    End Sub

    ''' <summary>
    ''' Procedimiento que actualiza los datos del detalle de albarán de una linea antes de mostrarlos
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarPanelDetalleAlbaran()
        Dim sAlbaran As String = DBNullToStr(CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6).Select("LINEA=" & Me.hid_FacturaUpdating.Value)(0)("ALBARAN"))

        RaiseEvent EventoCargarDetalleAlbaran(sAlbaran)

        imgDetalleAlbaran.Src = _RutaImagenes & "recepcion_small.jpg"
        lblDetalleAlbaran.Text = oTextos(51)(1) & " " & sAlbaran
        imgCalculoImporte.Src = _RutaImagenes & "CostesDescuentos€.png"

        If m_bEditable Then
            lblLitEliminarAlbaran.Visible = True
            lblLitEliminarAlbaran.Text = oTextos(52)(1)
            lblLitEliminarAlbaran.OnClientClick = "return borrarAlbaran(" & Request("ID") & ",'" & sAlbaran & "','" & oTextos(153)(1) & "');"
        End If

        lblLitFechaRecepcion.Text = String.Format("{0}:", oTextos(53).Item(1))
        lblFechaRecepcion.Text = FormatDate(m_dFechaRecepcion, m_oDateFormat)
        lblLitNumeroRecepcionERP.Text = String.Format("{0}:", oTextos(54).Item(1))
        lblNumeroRecepcionERP.Text = m_sNumeroRecepcionERP
        'Datos receptor
        lblTituloDatosReceptor.Text = oTextos(101).Item(1)
        lblLitCodReceptor.Text = String.Format("{0}:", oTextos(39).Item(1))
        lblLitNombreReceptor.Text = String.Format("{0}:", oTextos(55).Item(1))
        lblLitCargo.Text = String.Format("{0}:", oTextos(56).Item(1))
        lblLitDepartamento.Text = String.Format("{0}:", oTextos(57).Item(1))
        lblLitEmail.Text = String.Format("{0}:", oTextos(58).Item(1))
        lblLitTelefono.Text = String.Format("{0}:", oTextos(59).Item(1))
        lblLitFax.Text = String.Format("{0}:", oTextos(60).Item(1))
        lblLitOrganizacion.Text = String.Format("{0}:", oTextos(61).Item(1))
        lblCodReceptor.Text = m_sCodReceptor
        lblNombreReceptor.Text = m_sNombreReceptor
        lblCargo.Text = m_sCargo
        lblDepartamento.Text = m_sDepartamento
        lblEmail.Text = m_sEmail
        lblTelefono.Text = m_sTelefono
        lblFax.Text = m_sFax
        lblOrganizacion.Text = m_sOrganizacion

        If m_dtCostesAlbaran.Rows.Count > 0 OrElse m_dtDescuentosAlbaran.Rows.Count > 0 Then
            imgCostesDescuentosAlbaran.Src = _RutaImagenes & "candado_close.png" ' "images/recepcion_small.jpg"
            lblTituloCostesDescuentosAlbaran.Text = oTextos(95).Item(1)
            lblTituloCostesAlbaran.Text = oTextos(7).Item(1)
            lblTituloDescuentosAlbaran.Text = oTextos(10).Item(1)
            pnlCostesDescuentosAlbaran.Visible = True

            'Costes
            If m_dtCostesAlbaran.Rows.Count > 0 Then
                rpCostesAlbaran.DataSource = m_dtCostesAlbaran
                rpCostesAlbaran.DataBind()
            Else
                rpCostesAlbaran.Visible = False
                lblSinCostesAlbaran.Text = oTextos(96).Item(1)
                lblSinCostesAlbaran.Visible = True
            End If
            'Descuentos
            If m_dtDescuentosAlbaran.Rows.Count > 0 Then
                rpDescuentosAlbaran.DataSource = m_dtDescuentosAlbaran
                rpDescuentosAlbaran.DataBind()
            Else
                rpDescuentosAlbaran.Visible = False
                lblSinDescuentosAlbaran.Text = oTextos(97).Item(1)
                lblSinDescuentosAlbaran.Visible = True
            End If
        End If

        UpPanelDetalleAlbaran.Update()
        mpePanelDetalleAlbaran.Show()
    End Sub

    ''' <summary>
    ''' Evento que se utiliza para mostrar el modalpopup de detalle de impuestos
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnMostrarPanelDetalleImpuestos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMostrarPanelDetalleImpuestos.Click
        CargarPanelDetalleImpuestos()
    End Sub

    ''' <summary>
    ''' Procedimiento que actualiza los datos del detalle de impuestos de una linea antes de mostrarlos
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarPanelDetalleImpuestos()
        Dim dtImpuestos As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(10)
        Dim dtImpuestosLinea() As DataRow = dtImpuestos.Select("LINEA=" & Me.hid_FacturaUpdating.Value)
        Dim dtLineaFactura() As DataRow = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6).Select("LINEA=" & Me.hid_FacturaUpdating.Value)

        _ImporteBrutoLineaSeleccionada = DBNullToDbl(dtLineaFactura(0)("CANT_PED")) * DBNullToDbl(dtLineaFactura(0)("PREC_UP"))
        _DenominacionArticuloLineaSeleccionada = DBNullToStr(dtLineaFactura(0)("ART_DEN"))
        _CodLineaSeleccionada = Me.hid_FacturaUpdating.Value.ToString.PadLeft(3, "0")
        _CodArticuloLineaSeleccionada = DBNullToStr(dtLineaFactura(0)("ART_INT"))

        lblLiteralImpuestosDetalle.Text = oTextos(74).Item(1)
        lblDescripcionLinea.Text = _CodLineaSeleccionada & " " & _CodArticuloLineaSeleccionada & " " & _DenominacionArticuloLineaSeleccionada
        lblCalculoImporteLinea.Text = String.Format("{0}: ", oTextos(75).Item(1)) & FormatNumber(Replace(_ImporteBrutoLineaSeleccionada.ToString, ".", ","), m_oNumberFormat) & " " & m_sMoneda

        rptImpuestosLineaFactura.DataSource = dtImpuestosLinea
        rptImpuestosLineaFactura.DataBind()
        mpePanelDetalleImpuestos.Show()

    End Sub


    ''' <summary>
    ''' Procedimiento que salta al cambiar el porcentaje de un impuesto retenido en el panel de impuestos de cada linea de factura
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub PorcentajeImpuestosChange(ByVal sender As Object, ByVal e As Infragistics.Web.UI.EditorControls.TextEditorValueChangedEventArgs)
        If e.NewValue <= 0 Then
            DirectCast(sender, Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = e.OldValue
        Else
            Dim dtImpuestosLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(10)
            Dim LineaFactura As String = hid_FacturaUpdating.Value
            Dim repItem As RepeaterItem = DirectCast(DirectCast(sender, Infragistics.Web.UI.EditorControls.WebNumericEditor).Parent, RepeaterItem)
            Dim CodigoImpuesto As String = DirectCast(repItem.FindControl("lblCodigo"), Label).Text
            Dim drImpuesto() As DataRow = dtImpuestosLinea.Select("LINEA=" & LineaFactura & " AND COD='" & CodigoImpuesto & "'")
            Dim dtLineasFactura As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)


            'Seleccionamos la linea de factura para obtener sus campos CANTIDAD, PRECIO...
            Dim drLineaFactura() As DataRow = dtLineasFactura.Select("LINEA=" & LineaFactura)

            Dim ImporteImpuesto As Double

            'Calculamos el importe del impuesto en relacion al importe de la linea de factura
            ImporteImpuesto = (DBNullToDbl(drLineaFactura(0)("PREC_UP")) * DBNullToDbl(drLineaFactura(0)("CANT_PED")))
            ImporteImpuesto += DBNullToDbl(drLineaFactura(0)("TOTAL_COSTES"))
            ImporteImpuesto -= DBNullToDbl(drLineaFactura(0)("TOTAL_DCTOS"))
            ImporteImpuesto = DBNullToDbl((ImporteImpuesto * e.NewValue)) / 100


            'Actualizamos el valor del impuesto retenido en la tabla cacheada
            drImpuesto(0)("VALOR") = e.NewValue
            'Recogemos el importe antiguo del impuesto para calcular el nuevo importe
            Dim dOldImporteImpuesto As Double = DBNullToDbl(drImpuesto(0)("IMPORTE"))
            drImpuesto(0)("IMPORTE") = ImporteImpuesto

            ImporteImpuesto = ImporteImpuesto * -1
            'Ponemos el importe del impuesto despues de calcularlo
            DirectCast(repItem.FindControl("lblImporteImpuestos"), Label).Text = FormatNumber(Replace(ImporteImpuesto.ToString, ".", ","), m_oNumberFormat)

            'Actualizamos el importe total de impuestos de la linea de factura
            Dim repeaterImpuestos As Repeater = DirectCast(repItem.Parent, Repeater)
            Dim repItemFooter As RepeaterItem = DirectCast(repeaterImpuestos.Controls(repeaterImpuestos.Controls.Count - 1), RepeaterItem)
            Dim sOldImporte As String = DirectCast(repItemFooter.FindControl("lblTotalImporteImpuestosFooter"), Label).Text.Replace(m_sMoneda, "").Trim
            Dim dImporte As Double = CDbl(sOldImporte) + (dOldImporteImpuesto * -1) 'le sumamos el antiguo importe del impuesto ya que es un importe negativo por ser impuesto retenido
            dImporte += ImporteImpuesto 'le restamos el nuevo importe del impuesto (negativo)
            DirectCast(repItemFooter.FindControl("lblTotalImporteImpuestosFooter"), Label).Text = FormatNumber(Replace(dImporte.ToString, ".", ","), m_oNumberFormat) & " " & m_sMoneda


            'Actualizamos el importe neto de la linea de factura en el grid
            RecalcularImpuestoLinea(e.OldValue)
            CargarLineasFactura()
            upLineas.Update()

            'ActualizarResumenImpuestos(1, drImpuesto(0))
        End If
    End Sub

    ''' <summary>
    ''' evento del repeater al realizar alguna accion
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub rptImpuestosLineaFactura_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptImpuestosLineaFactura.ItemCommand
        Dim dtImpuestosLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(10)
        Dim dtLineasFactura As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
        Dim idImpuesto As String = DirectCast(e.Item.FindControl("hid_idImpuesto"), HiddenField).Value
        Dim LineaSeleccionada As String = hid_FacturaUpdating.Value  'DirectCast(e.Item.FindControl("hid_Linea"), HiddenField).Value
        Select Case e.CommandName
            Case "EliminarImpuestoLinea"
                For Each dr As DataRow In dtImpuestosLinea.Rows
                    If dr.RowState <> DataRowState.Deleted AndAlso dr("ID") = idImpuesto Then
                        ActualizarResumenImpuestos(dr("RETENIDO"), dr, True)
                        dtImpuestosLinea.Rows.Remove(dr)
                        Exit For
                    End If
                Next
                CargarLineasFactura()
                upLineas.Update()
                CargarPanelDetalleImpuestos()
            Case "VerMotivoExencion"
                Dim drImpuestoLinea() As DataRow = dtImpuestosLinea.Select("ID=" & idImpuesto)
                txtMotivoExencion.Text = DBNullToStr(drImpuestoLinea(0)("COMENT"))
                upMotivoExecionImpuesto.Update()
                mpePnlMotivoImpuestoExento.Show()
            Case "AplicarRestoLineas"
                'Selecciono el impuesto que quiero copiar
                Dim drLineaCopiar() As DataRow = dtImpuestosLinea.Select("ID=" & idImpuesto)
                Dim arrLineasActualizar As New List(Of String())

                Dim idNewImpuesto As Short = hid_UltimoNuevoIdImpuesto.Value
                'Recorro las lineas de factura
                For Each dr As DataRow In dtLineasFactura.Rows
                    'Si la linea es distinta que la linea del impuesto seleccionado la comprobare para ver si se le puede aplicar
                    If dr("LINEA") <> LineaSeleccionada Then
                        'Selecciono los impuestos de esa linea de factura
                        Dim drLineasActualizar() As DataRow = dtImpuestosLinea.Select("LINEA=" & dr("LINEA"))
                        Dim bImpuestoExiste As Boolean
                        Dim itemLinea(4) As String
                        For Each drLineaActualizar As DataRow In drLineasActualizar
                            'Compruebo que el codigo de impuesto que quiero introducir en esa linea no este ya
                            If drLineaActualizar("COD") = drLineaCopiar(0)("COD") Then
                                bImpuestoExiste = True
                            End If
                        Next
                        If bImpuestoExiste = False Then
                            'Si no existe ese impuesto en la linea de factura lo añado a la lista de lineas al que añadir ese impuesto copiado
                            itemLinea(0) = dr("LINEA")
                            itemLinea(1) = dr("CANT_PED")
                            itemLinea(2) = dr("PREC_UP")
                            itemLinea(3) = dr("TOTAL_COSTES")
                            itemLinea(4) = dr("TOTAL_DCTOS")
                            arrLineasActualizar.Add(itemLinea)
                        End If
                        bImpuestoExiste = False
                    End If

                Next
                Dim newRow As DataRow
                For i = 0 To arrLineasActualizar.Count - 1
                    Dim item(4) As String
                    item = arrLineasActualizar(i)
                    newRow = dtImpuestosLinea.NewRow
                    newRow("ID") = idNewImpuesto
                    newRow("LINEA") = item(0)
                    newRow("COD") = drLineaCopiar(0)("COD")
                    newRow("DEN") = drLineaCopiar(0)("DEN")
                    newRow("VALOR") = drLineaCopiar(0)("VALOR")
                    newRow("RETENIDO") = drLineaCopiar(0)("RETENIDO")
                    newRow("COMENT") = drLineaCopiar(0)("COMENT")
                    newRow("COMMENT") = drLineaCopiar(0)("COMMENT")
                    newRow("GRP_COMP") = drLineaCopiar(0)("GRP_COMP")
                    If drLineaCopiar(0)("RETENIDO") = 1 Then
                        'Si es un impuesto retenido el importe tendra que ser negativo porque restara al importe de la linea de factura
                        newRow("IMPORTE") = (((DBNullToDbl(item(1)) * DBNullToDbl(item(2))) + DBNullToDbl(item(3)) - DBNullToDbl(item(4))) * DBNullToDbl(drLineaCopiar(0)("VALOR")) / 100) * -1
                    Else
                        newRow("IMPORTE") = ((DBNullToDbl(item(1)) * DBNullToDbl(item(2))) + DBNullToDbl(item(3)) - DBNullToDbl(item(4))) * DBNullToDbl(drLineaCopiar(0)("VALOR")) / 100
                    End If
                    dtImpuestosLinea.Rows.Add(newRow)
                    ActualizarResumenImpuestos(newRow("RETENIDO"), newRow)
                    idNewImpuesto -= 1
                Next
                hid_UltimoNuevoIdImpuesto.Value = idNewImpuesto
                CargarLineasFactura()
                upLineas.Update()
                mpePanelDetalleImpuestos.Hide()
        End Select
    End Sub

    ''' <summary>
    ''' evento que salta por cada impuesto del repeater del detalle de impuestos de cada linea
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub rptImpuestosLineaFactura_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptImpuestosLineaFactura.ItemDataBound
        Static ImporteTotal As Double
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                DirectCast(e.Item.FindControl("hid_idImpuesto"), HiddenField).Value = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("ID"))
                DirectCast(e.Item.FindControl("lblCodigo"), Label).Text = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("COD"))
                DirectCast(e.Item.FindControl("lblDenominacion"), Label).Text = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("DEN"))
                DirectCast(e.Item.FindControl("chkImpRetenido"), CheckBox).Checked = DBNullToInteger(DirectCast(e.Item.DataItem, DataRow)("RETENIDO"))
                If m_bEditable Then
                    'Si se puede añadir o modificar impuestos
                    If DBNullToInteger(DirectCast(e.Item.DataItem, DataRow)("RETENIDO")) = 1 Then
                        DirectCast(e.Item.FindControl("wnePorcentaje"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = DBNullToDbl(DirectCast(e.Item.DataItem, DataRow)("VALOR"))
                        DirectCast(e.Item.FindControl("lblPorcentaje"), Label).Text = "%"
                        Dim dImporte As Double = DBNullToDbl(DirectCast(e.Item.DataItem, DataRow)("IMPORTE")) * -1 'Al ser retenido hay que pasar el importe a negativo
                        DirectCast(e.Item.FindControl("lblImporteImpuestos"), Label).Text = FormatNumber(Replace(dImporte.ToString, ".", ","), m_oNumberFormat)
                        ImporteTotal += dImporte
                    Else
                        DirectCast(e.Item.FindControl("lblPorcentaje"), Label).Text = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("VALOR")) & "%"
                        DirectCast(e.Item.FindControl("wnePorcentaje"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Visible = False
                        Dim dImporte As Double = DBNullToDbl(DirectCast(e.Item.DataItem, DataRow)("IMPORTE"))
                        DirectCast(e.Item.FindControl("lblImporteImpuestos"), Label).Text = FormatNumber(Replace(dImporte.ToString, ".", ","), m_oNumberFormat)
                        ImporteTotal += dImporte
                    End If

                    DirectCast(e.Item.FindControl("imgEliminarImpuestoLinea"), ImageButton).ImageUrl = _RutaImagenes & "eliminar.png"
                    DirectCast(e.Item.FindControl("imgAplicarRestoLineas"), ImageButton).ImageUrl = _RutaImagenes & "Aplicar.jpg"
                    DirectCast(e.Item.FindControl("imgAplicarRestoLineas"), ImageButton).ToolTip = oTextos(156).Item(1)
                    DirectCast(e.Item.FindControl("imgVerMotivoExencion"), ImageButton).ImageUrl = _RutaImagenes & "Observaciones.png"

                    Dim sComentExencion As String = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("COMENT"))
                    If sComentExencion <> String.Empty Then
                        DirectCast(e.Item.FindControl("imgVerMotivoExencion"), ImageButton).Visible = True
                        DirectCast(e.Item.FindControl("txtMotivoExencion2"), TextBox).Text = sComentExencion
                    Else
                        DirectCast(e.Item.FindControl("imgVerMotivoExencion"), ImageButton).Visible = False
                    End If
                Else
                    If DBNullToInteger(DirectCast(e.Item.DataItem, DataRow)("RETENIDO")) = 1 Then
                        DirectCast(e.Item.FindControl("wnePorcentaje"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Visible = False
                        DirectCast(e.Item.FindControl("lblPorcentaje"), Label).Text = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("VALOR")) & "%"
                        Dim dImporte As Double = DBNullToDbl(DirectCast(e.Item.DataItem, DataRow)("IMPORTE")) * -1 'Al ser retenido hay que pasar el importe a negativo
                        DirectCast(e.Item.FindControl("lblImporteImpuestos"), Label).Text = FormatNumber(Replace(dImporte.ToString, ".", ","), m_oNumberFormat)
                        ImporteTotal += dImporte
                    Else
                        DirectCast(e.Item.FindControl("lblPorcentaje"), Label).Text = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("VALOR")) & "%"
                        DirectCast(e.Item.FindControl("wnePorcentaje"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Visible = False
                        Dim dImporte As Double = DBNullToDbl(DirectCast(e.Item.DataItem, DataRow)("IMPORTE"))
                        DirectCast(e.Item.FindControl("lblImporteImpuestos"), Label).Text = FormatNumber(Replace(dImporte.ToString, ".", ","), m_oNumberFormat)
                        ImporteTotal += dImporte
                    End If
                    DirectCast(e.Item.FindControl("CeldaEliminarImpuesto"), HtmlTableCell).Visible = False
                    DirectCast(e.Item.FindControl("CeldaAplicarImpuesto"), HtmlTableCell).Visible = False
                    DirectCast(e.Item.FindControl("CeldaVerMotivoExencion"), HtmlTableCell).Visible = False
                    lblAgregarImpuesto.Visible = False
                    imgAgregarImpuesto.Visible = False
                End If
            Case ListItemType.Footer
                DirectCast(e.Item.FindControl("lblTotalImpuestosFooter"), Label).Text = String.Format("{0}: ", oTextos(76).Item(1))
                DirectCast(e.Item.FindControl("lblTotalImporteImpuestosFooter"), Label).Text = FormatNumber(Replace(ImporteTotal.ToString, ".", ","), m_oNumberFormat) & " " & m_sMoneda
            Case ListItemType.Header
                DirectCast(e.Item.FindControl("lblCodigoHeader"), Label).Text = oTextos(39).Item(1)
                DirectCast(e.Item.FindControl("lblDenominacionHeader"), Label).Text = oTextos(63).Item(1)
                DirectCast(e.Item.FindControl("lblImpRetenidoHeader"), Label).Text = oTextos(99).Item(1)
                DirectCast(e.Item.FindControl("lblPorcentajeHeader"), Label).Text = oTextos(100).Item(1)
                DirectCast(e.Item.FindControl("lblImporteImpuestosHeader"), Label).Text = oTextos(2).Item(1)
        End Select

    End Sub
    ''' <summary>
    ''' Evento que salta por cada linea del datasource del repeater del resumen de impuestos repercutidos
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub rpImpuestosRepercutidos_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpImpuestosRepercutidos.ItemDataBound
        Dim lbl As Label
        Select Case e.Item.ItemType
            Case ListItemType.Header
                CType(e.Item.FindControl("lblImpReper"), HtmlGenericControl).InnerHtml = oTextos(142).Item(1) 'Impuestos Retenidos
                CType(e.Item.FindControl("lblCodImpReper"), HtmlGenericControl).InnerHtml = oTextos(39).Item(1) 'Código
                CType(e.Item.FindControl("lblDenImpReper"), HtmlGenericControl).InnerHtml = oTextos(63).Item(1) 'Denominación
                CType(e.Item.FindControl("lblComenImpReper"), HtmlGenericControl).InnerHtml = oTextos(141).Item(1) 'Comentario
                CType(e.Item.FindControl("lblImporteImpReper"), HtmlGenericControl).InnerHtml = oTextos(144).Item(1) 'Importe Impuestos
            Case ListItemType.Item, ListItemType.AlternatingItem
                lbl = DirectCast(e.Item.FindControl("lblValor"), Label)
                lbl.Text = FormatNumber(Replace(e.Item.DataItem("VALOR"), ".", ","), m_oNumberFormat) '& " %"
                lbl = DirectCast(e.Item.FindControl("lblImporte"), Label)
                lbl.Text = FormatNumber(Replace(e.Item.DataItem("IMPORTE"), ".", ","), m_oNumberFormat) & " " & m_sMoneda
                m_dTotalImpuestosRepercutidos = m_dTotalImpuestosRepercutidos + e.Item.DataItem("IMPORTE")
            Case ListItemType.Footer
                DirectCast(e.Item.Controls(0).FindControl("lblTotal"), Label).Text = String.Format("{0}: ", oTextos(76).Item(1)) & FormatNumber(Replace(m_dTotalImpuestosRepercutidos, ".", ","), m_oNumberFormat) & " " & m_sMoneda
        End Select
    End Sub
    ''' <summary>
    ''' Evento que salta por cada linea del datasource del repeater del resumen de impuestos retenidos
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub rpImpuestosRetenidos_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpImpuestosRetenidos.ItemDataBound
        Dim lbl As Label
        Select Case e.Item.ItemType
            Case ListItemType.Header
                CType(e.Item.FindControl("lblImpReten"), HtmlGenericControl).InnerHtml = oTextos(143).Item(1) 'Impuestos Retenidos
                CType(e.Item.FindControl("lblCodImpReten"), HtmlGenericControl).InnerHtml = oTextos(39).Item(1) 'Código
                CType(e.Item.FindControl("lblDenImpReten"), HtmlGenericControl).InnerHtml = oTextos(63).Item(1) 'Denominación
                CType(e.Item.FindControl("lblComImpReten"), HtmlGenericControl).InnerHtml = oTextos(141).Item(1) 'Comentario
                CType(e.Item.FindControl("lblImporteImpReten"), HtmlGenericControl).InnerHtml = oTextos(144).Item(1) 'Importe Impuestos
            Case ListItemType.Item, ListItemType.AlternatingItem
                lbl = DirectCast(e.Item.FindControl("lblValor"), Label)
                lbl.Text = FormatNumber(Replace(DBNullToDbl(e.Item.DataItem("VALOR")), ".", ","), m_oNumberFormat) '& " %"
                lbl = DirectCast(e.Item.FindControl("lblImporte"), Label)
                lbl.Text = FormatNumber(Replace(Math.Abs(DBNullToDbl(e.Item.DataItem("IMPORTE"))), ".", ","), m_oNumberFormat) & " " & m_sMoneda
                m_dTotalImpuestosRetenidos = m_dTotalImpuestosRetenidos + Math.Abs(DBNullToDbl(e.Item.DataItem("IMPORTE")))
            Case ListItemType.Footer
                DirectCast(e.Item.Controls(0).FindControl("lblTotal"), Label).Text = String.Format("{0}: ", oTextos(76).Item(1)) & FormatNumber(Replace(m_dTotalImpuestosRetenidos, ".", ","), m_oNumberFormat) & " " & m_sMoneda
        End Select
    End Sub

    ''' <summary>
    ''' procedimiento que enlaza el datasource con el grid de lineas de factura
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarLineasFactura()
        Dim ds As New DataSet
        ds.Tables.Add(m_dtDataSource.Copy)

        If ds.Tables(0).Rows.Count <= 20 Then
            whdgLineas.Height = Nothing
        End If

        whdgLineas.DataSource = ds
        whdgLineas.DataBind()
    End Sub


    ''' <summary>
    ''' Procedimiento que acepta la observacion que se introduce cuando se añade un impuesto exento
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnAceptarMotivImpExento_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarMotivImpExento.Click
        Dim linea As String = hid_FacturaUpdating.Value 'Recojo la linea de factura a la que añadiria el impuesto
        Dim mostrarCambios As Boolean = True
        If Not String.IsNullOrEmpty(btnAceptarMotivImpExento.CommandArgument) Then _
            mostrarCambios = CBool(btnAceptarMotivImpExento.CommandArgument)
        Dim refrescar As Boolean = True
        If mostrarCambios Then
            refrescar = seleccionarImpuestoExento(linea, mostrarCambios)
            If refrescar Then
                upLineas.Update()
            End If
        Else
            Dim lineasConImpuesto = Cache("lineasConImpuesto" & m_sSufijoCache)
            actualizarImpuestos(lineasConImpuesto)
            Cache.Remove("lineasConImpuesto" & m_sSufijoCache)
            btnAceptarMotivImpExento.CommandArgument = ""
            btnCancelarMotivImpExento.Style("display") = "block"
            Dim script As String = "$('#" & imgCerrarPanelMotivoImpuestoExento.ClientID & "').css('display', 'block');"
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, "mostrarimgCerrarPanelMotivoImpuestoExento", script, True)
            upMotivoExecionImpuesto.Update()
        End If
    End Sub
    ''' <summary>
    ''' procedimiento donde se inicializa cada fila del grid de buscador de impuestos
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub whdgImpuestos_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgImpuestos.InitializeRow
        If e.Row.Items.FindItemByKey("RETENIDO").Value = 0 Then
            e.Row.Items.FindItemByKey("RETENIDO").Text = ""
        Else
            e.Row.Items.FindItemByKey("RETENIDO").Text = "<img src='" & _RutaImagenes & "aprobado.gif' style='text-align:center;'/>"
        End If
    End Sub


    ''' <summary>
    ''' Procedimiento que limpia los campos del buscador de descuentos
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnLimpiarImpuestos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarImpuestos.Click
        txtCodigo.Text = ""
        hidArticulo.Value = ""
        txtMaterial.Text = ""
        hidMaterial.Value = ""
        txtCodigo.Text = ""
        txtDescripcion.Text = ""
        wddPaises.CurrentValue = ""
        wddPaises.ClearSelection()
        wddProvincias.ClearSelection()
        wddProvincias.CurrentValue = ""
        wddConceptoImpuestos.ClearSelection()
        wddConceptoImpuestos.CurrentValue = ""
        upBuscadorImpuestos.Update()
    End Sub

    ''' <summary>
    ''' Ejecuta la acción que implica que el usuario haya pulsado Aceptar en el panel confirmar
    ''' </summary>
    ''' <param name="sender">Control que lanza el evento</param>
    ''' <param name="e">Argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. Max 0,5 seg</remarks>
    Private Sub btnAceptarConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarConfirm.Click
        'Si aceptamos reemplazar impuestos, hay que:
        '- Recorrer todas las líneas, 
        '- Borrar los impuestos no compatibles con el impuesto elegido
        '- Añadir el impuesto añadido en las líneas que no lo tengan, si es que las hay.
        Dim dtLineasFactura As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
        Dim dtImpuestos As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(10)

        'Lista con los Id de las lineas de la factura
        Dim lineas = From impuestosLinea In dtLineasFactura _
                     Select impuestosLinea("LINEA") Distinct.ToList
        If lineas.Any Then
            'Impuesto seleccionado
            Dim filaSeleccionada As Infragistics.Web.UI.GridControls.GridRecord = whdgImpuestos.GridView.Behaviors.Selection.SelectedRows(0)
            'Grupo de compatibilidad del impuesto seleccionado
            Dim GrupoCompatibilidad As Integer = DBNullToInteger(filaSeleccionada.Items.FindItemByKey("GRP_COMP").Value)
            'Lista de impuestos de las líneas de la factura que no tienen un grupos de compatibilidad igual que el del impuesto que hay que pegar en todas las líneas y que, por consiguiente, hay que eliminar
            Dim impuestosAEliminar = From impuestosLinea In dtImpuestos.Copy _
                                     Where impuestosLinea("GRP_COMP") <> GrupoCompatibilidad _
                                    Select impuestosLinea("ID") _
                                    Distinct.ToList

            Dim lineasConImpuesto = From impuestosLinea In dtImpuestos.Copy _
                                     Where impuestosLinea("GRP_COMP") <> GrupoCompatibilidad _
                                    Select impuestosLinea("LINEA") _
                                    Distinct.ToList

            'Eliminar los impuestos que no son del mismo Grupo en todas las líneas
            For Each impuestoID As String In impuestosAEliminar
                Dim dr As DataRow() = dtImpuestos.Select("ID = " & impuestoID)
                If dr(0).RowState <> DataRowState.Deleted Then
                    ActualizarResumenImpuestos(dr(0)("RETENIDO"), dr(0), True)
                    dtImpuestos.Rows.Remove(dr(0))
                    'dr(0).Delete()
                End If
            Next

            'Comprobar si el impuesto elegido es Exento. Si lo es, hay que pasar un motivo de exención
            Dim iRetenido As Byte = DBNullToInteger(filaSeleccionada.Items.FindItemByKey("RETENIDO").Value)
            Dim dValor As Double = DBNullToDbl(filaSeleccionada.Items.FindItemByKey("VALOR").Value)
            If iRetenido = 0 AndAlso dValor = 0 Then
                'Si no es retenido y su valor es igual a 0 es que es un impuesto EXENTO que no tiene valor y habra que indicar el motivo
                lblDescImpuestoExento.Text = filaSeleccionada.Items.FindItemByKey("COD").Value & " - " & filaSeleccionada.Items.FindItemByKey("DEN").Value & " (" & dValor.ToString & "%)"
                Dim mostrarCambios As Boolean = False
                btnAceptarMotivImpExento.CommandArgument = mostrarCambios
                txtMotivoExencion.Text = String.Empty
                'No tiene otra opción más que aceptar el impuesto. En este caso no hay marcha atrás, por lo que ocultamos el cancelar
                btnCancelarMotivImpExento.Style("display") = "none"
                Dim script As String = "$('#" & imgCerrarPanelMotivoImpuestoExento.ClientID & "').css('display', 'none');"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, "ocultarimgCerrarPanelMotivoImpuestoExento", script, True)
                upMotivoExecionImpuesto.Update()
                mpePnlMotivoImpuestoExento.Show()
                Cache.Insert("lineasConImpuesto" & m_sSufijoCache, lineasConImpuesto, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, 5, 0, 0, 0))
            Else
                actualizarImpuestos(lineasConImpuesto)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Actualiza el impuesto en las líneas en las que hay que actualizar
    ''' </summary>
    ''' <param name="lineasConImpuesto">Líneas que deben ser actualizadas</param>
    ''' <remarks>btnAceptarConfirm_Click. Max 0,5 seg</remarks>
    Private Sub actualizarImpuestos(ByVal lineasConImpuesto As List(Of Object))
        'Seleccionar el impuesto elegido en todas las líneas que tenían impuesto
        For Each linea As String In lineasConImpuesto
            'Cuando vaya a cambiarse el impuesto de todas las líneas, no mostramos aviso de que alguna ya tiene ese impuesto, ya lo tiene, no es necesario avisar
            Dim bMostrarCambios As Boolean = False
            seleccionarImpuesto(linea, bMostrarCambios)
        Next
        upLineas.Update()
        mpePanelDetalleImpuestos.Hide()
        btnAceptarConfirm.CommandArgument = ""
        Dim script As String = "$('#pnlConfirm').css('display', 'none');"
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, "ocultarPnlConfirm", script, True)
    End Sub

    ''' <summary>
    ''' Función que selecciona en la celda del impuesto, el impuesto elegido en el panel buscador de impuestos, después de comprobar una serie de requisitos
    ''' Si se le pasa el parámetro cambiarTodos a true, cambia todos los impuestos de las líneas por el impuesto seleccionado
    ''' </summary>
    ''' <param name="Linea">Línea en la que modificar el impuesto</param>
    ''' <param name="mostrarCambios">Indica si quieremos hacer refrescos en pantalla, por los cambios hechos, o no. Si no los hace en la función, debemos hacerlos después de ejecutar la función</param>
    ''' <remarks>Llamada desde btnAceptarConfirm_click y btnAceptarImpuesto_Click. Max 0,3 seg</remarks>
    Private Sub seleccionarImpuesto(ByVal Linea As String, Optional ByVal mostrarCambios As Boolean = True)
        Dim dtImpuestosLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(10)
        Dim dtLineasImpuestos As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
        Dim idNewImpuesto As Short = If(hid_UltimoNuevoIdImpuesto.Value = "", -1, hid_UltimoNuevoIdImpuesto.Value)
        Dim filaSeleccionada As Infragistics.Web.UI.GridControls.GridRecord = whdgImpuestos.GridView.Behaviors.Selection.SelectedRows(0)
        Dim iRetenido As Byte = DBNullToInteger(filaSeleccionada.Items.FindItemByKey("RETENIDO").Value)
        Dim dValor As Double = DBNullToDbl(filaSeleccionada.Items.FindItemByKey("VALOR").Value)
        If iRetenido = 0 AndAlso dValor = 0 Then
            If mostrarCambios Then
                'Si no es retenido y su valor es igual a 0 es que es un impuesto EXENTO que no tiene valor y habra que indicar el motivo
                lblDescImpuestoExento.Text = filaSeleccionada.Items.FindItemByKey("COD").Value & " - " & filaSeleccionada.Items.FindItemByKey("DEN").Value & " (" & dValor.ToString & "%)"
                btnAceptarMotivImpExento.CommandArgument = mostrarCambios
                'Cuando no mostremos cambios, sólo vamos a pasar el motivo de Exento una vez y lo usaremos para todos los demás casos, si es que los hay, es decir, cuando hayamos hecho un bucle para cambiar todos los impuestos
                txtMotivoExencion.Text = String.Empty
                upMotivoExecionImpuesto.Update()
                mpePnlMotivoImpuestoExento.Show()
            Else
                seleccionarImpuestoExento(Linea, mostrarCambios)
            End If
        Else
            'Seleccionamos los impuestos de la linea si los tuviese
            Dim drImpuesto() As DataRow = dtImpuestosLinea.Select("LINEA=" & Linea)
            'Seleccionamos la linea de factura para obtener sus campos CANTIDAD, PRECIO...
            Dim drLineaFactura() As DataRow = dtLineasImpuestos.Select("LINEA=" & Linea)

            Dim ImporteImpuesto As Double

            'Calculamos el importe del impuesto seleccionado en relacion al importe de la linea de factura
            ImporteImpuesto = (DBNullToDbl(drLineaFactura(0)("PREC_UP")) * DBNullToDbl(drLineaFactura(0)("CANT_PED")))
            ImporteImpuesto = ImporteImpuesto + DBNullToDbl(drLineaFactura(0)("TOTAL_COSTES"))
            ImporteImpuesto = ImporteImpuesto - DBNullToDbl(drLineaFactura(0)("TOTAL_DCTOS"))
            ImporteImpuesto = DBNullToDbl((ImporteImpuesto * dValor)) / 100

            If drImpuesto.Count = 0 Then
                'Si no tiene impuestos se añade el seleccionado
                Dim newRow As DataRow
                newRow = dtImpuestosLinea.NewRow
                newRow("ID") = idNewImpuesto
                newRow("LINEA") = Linea
                newRow("COD") = filaSeleccionada.Items.FindItemByKey("COD").Value
                newRow("DEN") = filaSeleccionada.Items.FindItemByKey("DEN").Value
                newRow("COMMENT") = filaSeleccionada.Items.FindItemByKey("COMMENT").Value
                newRow("GRP_COMP") = filaSeleccionada.Items.FindItemByKey("GRP_COMP").Value
                'Si el impuesto es retenido no tendra valor y se lo tendra que poner luego el usuario en el panel de detalle de impuestos
                If iRetenido = 1 Then
                    newRow("RETENIDO") = 1
                    newRow("VALOR") = System.DBNull.Value
                    newRow("IMPORTE") = System.DBNull.Value
                Else
                    newRow("RETENIDO") = 0
                    newRow("VALOR") = dValor
                    newRow("IMPORTE") = ImporteImpuesto
                End If

                dtImpuestosLinea.Rows.Add(newRow)
                CargarLineasFactura()
                If mostrarCambios Then
                    upLineas.Update()
                End If
                If iRetenido = 0 Then
                    ActualizarResumenImpuestos(iRetenido, newRow)
                End If
                If mostrarCambios Then
                    If iRetenido = 1 Then
                        CargarPanelDetalleImpuestos()
                        upDetalleImpuestosLinea.Update()
                    Else
                        mpePanelDetalleImpuestos.Hide()
                    End If
                End If
                hid_UltimoNuevoIdImpuesto.Value = idNewImpuesto - 1

            Else
                'Si tiene impuestos se comprobara que no existe para la linea un impuesto con el mismo codigo
                Dim bImpuestoEncontrado As Boolean
                For Each drLineaImp As DataRow In drImpuesto
                    If drLineaImp("COD") = filaSeleccionada.Items.FindItemByKey("COD").Value Then
                        bImpuestoEncontrado = True
                        Exit For
                    End If
                Next
                If bImpuestoEncontrado = False Then
                    Dim newRow As DataRow
                    newRow = dtImpuestosLinea.NewRow
                    newRow("ID") = idNewImpuesto
                    newRow("LINEA") = Linea
                    newRow("COD") = filaSeleccionada.Items.FindItemByKey("COD").Value
                    newRow("DEN") = filaSeleccionada.Items.FindItemByKey("DEN").Value
                    newRow("COMMENT") = filaSeleccionada.Items.FindItemByKey("COMMENT").Value
                    newRow("GRP_COMP") = filaSeleccionada.Items.FindItemByKey("GRP_COMP").Value
                    'Si el impuesto es retenido no tendra valor y se lo tendra que poner luego el usuario en el panel de detalle de impuestos
                    If iRetenido = 1 Then
                        newRow("RETENIDO") = 1
                        newRow("VALOR") = System.DBNull.Value
                        newRow("IMPORTE") = System.DBNull.Value
                    Else
                        newRow("RETENIDO") = 0
                        newRow("VALOR") = dValor
                        newRow("IMPORTE") = ImporteImpuesto
                    End If
                    dtImpuestosLinea.Rows.Add(newRow)
                    hid_UltimoNuevoIdImpuesto.Value = idNewImpuesto - 1
                    CargarLineasFactura()
                    If mostrarCambios Then
                        upLineas.Update()
                    End If
                    If iRetenido = 0 Then
                        ActualizarResumenImpuestos(iRetenido, newRow)
                    End If
                    If mostrarCambios Then
                        If iRetenido = 1 Then
                            CargarPanelDetalleImpuestos()
                            upDetalleImpuestosLinea.Update()
                        Else
                            mpePanelDetalleImpuestos.Hide()
                        End If
                    End If
                Else
                    If mostrarCambios Then
                        lblAvisoErrorImpuesto.Visible = True
                        upAvisoErrorImpuesto.Update()
                        mpePanelBuscadorImpuestos.Show()
                    End If
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Función que selecciona en la celda del impuesto, el impuesto elegido en el panel buscador de impuestos, después de comprobar una serie de requisitos
    ''' Si se le pasa el parámetro cambiarTodos a true, cambia todos los impuestos de las líneas por el impuesto seleccionado
    ''' </summary>
    ''' <param name="Linea">Línea en la que modificar el impuesto</param>
    ''' <param name="mostrarCambios">Indica si quieremos mostrar el aviso cuando la línea en la que se cambia el impuesto, ya tiene un impuesto con ese código</param>
    ''' <returns>Boolean que indica si hay que ejecutar refresco de la pantalla o no</returns>
    ''' <remarks>Llamada desde btnAceptarConfirm_click y btnAceptarImpuesto_Click. Max 0,3 seg</remarks>
    Private Function seleccionarImpuestoExento(ByVal linea As String, Optional ByVal mostrarCambios As Boolean = True) As Boolean
        Dim dtImpuestosLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(10)
        Dim dtLineasImpuestos As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
        Dim idNewImpuesto As Short = hid_UltimoNuevoIdImpuesto.Value
        Dim filaSeleccionada As Infragistics.Web.UI.GridControls.GridRecord = whdgImpuestos.GridView.Behaviors.Selection.SelectedRows(0)

        'Seleccionamos los impuestos de la linea si los tuviese
        Dim drImpuesto() As DataRow = dtImpuestosLinea.Select("LINEA=" & linea)
        'Seleccionamos la linea de factura para obtener sus campos 
        Dim drLineaFactura() As DataRow = dtLineasImpuestos.Select("LINEA=" & linea)
        Dim refrescoPantalla As Boolean
        If drImpuesto.Count = 0 Then
            'Si no tiene impuestos se añade el seleccionado
            Dim newRow As DataRow
            newRow = dtImpuestosLinea.NewRow
            newRow("ID") = idNewImpuesto
            newRow("LINEA") = linea
            newRow("COD") = filaSeleccionada.Items.FindItemByKey("COD").Value
            newRow("DEN") = filaSeleccionada.Items.FindItemByKey("DEN").Value
            newRow("RETENIDO") = 0
            newRow("VALOR") = 0
            newRow("IMPORTE") = 0
            newRow("COMENT") = txtMotivoExencion.Text
            newRow("COMMENT") = filaSeleccionada.Items.FindItemByKey("COMMENT").Value
            newRow("GRP_COMP") = filaSeleccionada.Items.FindItemByKey("GRP_COMP").Value
            If Not mostrarCambios Then
                txtMotivoExencion.Text = String.Empty
            End If

            dtImpuestosLinea.Rows.Add(newRow)
            hid_UltimoNuevoIdImpuesto.Value = idNewImpuesto - 1
            CargarLineasFactura()
            ActualizarResumenImpuestos(0, newRow)

            refrescoPantalla = True
        Else
            'Si tiene impuestos se comprobara que no existe para la linea un impuesto con el mismo codigo
            Dim bImpuestoEncontrado As Boolean
            For Each drLineaImp As DataRow In drImpuesto
                If drLineaImp("COD") = filaSeleccionada.Items.FindItemByKey("COD").Value AndAlso drLineaImp("ID") >= 0 Then
                    bImpuestoEncontrado = True
                    Exit For
                End If
            Next
            If bImpuestoEncontrado = False Then
                Dim newRow As DataRow
                newRow = dtImpuestosLinea.NewRow
                newRow("ID") = idNewImpuesto
                newRow("LINEA") = linea
                newRow("COD") = filaSeleccionada.Items.FindItemByKey("COD").Value
                newRow("DEN") = filaSeleccionada.Items.FindItemByKey("DEN").Value
                newRow("RETENIDO") = 0
                newRow("VALOR") = 0
                newRow("IMPORTE") = 0
                newRow("COMENT") = txtMotivoExencion.Text
                newRow("COMMENT") = filaSeleccionada.Items.FindItemByKey("COMMENT").Value
                newRow("GRP_COMP") = filaSeleccionada.Items.FindItemByKey("GRP_COMP").Value
                If Not mostrarCambios Then
                    txtMotivoExencion.Text = String.Empty
                End If

                dtImpuestosLinea.Rows.Add(newRow)
                hid_UltimoNuevoIdImpuesto.Value = idNewImpuesto - 1
                CargarLineasFactura()

                ActualizarResumenImpuestos(0, newRow)

                refrescoPantalla = True
            Else
                If mostrarCambios Then
                    lblAvisoErrorImpuesto.Visible = True
                    upAvisoErrorImpuesto.Update()
                    mpePanelBuscadorImpuestos.Show()
                End If
                refrescoPantalla = False
            End If
        End If
        mpePanelDetalleImpuestos.Hide()
        mpePnlMotivoImpuestoExento.Hide()
        Return refrescoPantalla
    End Function
#End Region
#Region "COSTES"
    ''' <summary>
    ''' Evento que se utiliza para mostrar el modalpopup de detalle de costes de linea
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnMostrarPanelDetalleCostes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMostrarPanelDetalleCostes.Click
        CargarPanelDetalleCostes()
    End Sub

    ''' <summary>
    ''' Procedimiento que actualiza los datos del detalle de costes de una linea antes de mostrarlos
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarPanelDetalleCostes()
        Dim dtCostes As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(11)
        Dim dtCostesLinea() As DataRow = dtCostes.Select("LINEA=" & Me.hid_FacturaUpdating.Value)
        Dim dtLineaFactura() As DataRow = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6).Select("LINEA=" & Me.hid_FacturaUpdating.Value)

        _ImporteBrutoLineaSeleccionada = DBNullToDbl(dtLineaFactura(0)("CANT_PED")) * DBNullToDbl(dtLineaFactura(0)("PREC_UP"))
        _DenominacionArticuloLineaSeleccionada = DBNullToStr(dtLineaFactura(0)("ART_DEN"))
        _CodLineaSeleccionada = Me.hid_FacturaUpdating.Value.ToString.PadLeft(3, "0")
        _CodArticuloLineaSeleccionada = DBNullToStr(dtLineaFactura(0)("ART_INT"))

        lblDescripcionLinea_Costes.Text = _CodLineaSeleccionada & "    " & _CodArticuloLineaSeleccionada & "      " & _DenominacionArticuloLineaSeleccionada
        lblCalculoImporteLinea_Costes.Text = String.Format("{0}: ", oTextos(75).Item(1)) & FormatNumber(Replace(_ImporteBrutoLineaSeleccionada.ToString, ".", ","), m_oNumberFormat) & " " & m_sMoneda

        If Not m_bEditable OrElse (Not m_bCostesPorLinea AndAlso DBNullToStr(dtLineaFactura(0)("ALBARAN")) = "") Then
            lblAgregarCoste.Visible = False
            imgAgregarCoste.Visible = False
        End If

        rptCostesLineaFactura.DataSource = dtCostesLinea
        rptCostesLineaFactura.DataBind()
        mpePanelDetalleCostes.Show()
    End Sub

    ''' <summary>
    ''' Funcion que calcula el importe de los costes de una linea
    ''' </summary>
    ''' <param name="drCostes">costes para esa linea de factura</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CalculoImporteCostesLinea(ByVal drCostes() As DataRow) As String

        Dim Importe As Double

        For Each drCoste As DataRow In drCostes
            Importe = Importe + DBNullToDbl(drCoste("IMPORTE"))
        Next

        Return Importe.ToString
    End Function


    ''' <summary>
    ''' Carga el combo de costes utilizados con anterioridad en otras facturas en el buscador de costes por linea
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarComboCostesUtilizados()
        Dim oItem As WebControls.ListItem

        oItem = New WebControls.ListItem
        oItem.Value = ""
        oItem.Text = ""
        ddlCostesFacturas.Items.Add(oItem)

        For Each oRow As DataRow In m_dsCostesUtilizadosBuscador.Tables(0).Rows
            oItem = New WebControls.ListItem
            oItem.Value = DBNullToStr(oRow.Item("ID"))
            oItem.Text = DBNullToStr(oRow.Item("DEN"))
            ddlCostesFacturas.Items.Add(oItem)
        Next

    End Sub
    Protected Sub ddlOperacionesCostes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objNumeric As Infragistics.Web.UI.EditorControls.WebNumericEditor = DirectCast(sender.parent.parent, System.Web.UI.WebControls.RepeaterItem).FindControl("wnePorcentaje")

        If objNumeric.Value Is Nothing OrElse objNumeric.Value = 0 Then 'Si no se ha metido nada en el webnumeric nos saltamos el evento
            Exit Sub
        End If

        Dim Valor As Double = objNumeric.ValueDouble

        Dim dtCostesLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(11)
        Dim LineaFactura As String = hid_FacturaUpdating.Value
        Dim repItem As RepeaterItem = DirectCast(sender.parent.parent, System.Web.UI.WebControls.RepeaterItem)
        Dim idCoste As String = DirectCast(repItem.FindControl("hid_idCosteLinea"), HiddenField).Value
        Dim sDenominacionCoste As String = DirectCast(repItem.FindControl("lblDenominacionCosteLinea"), Label).Text
        Dim sAlbaran As String = DirectCast(repItem.FindControl("hid_Albaran"), HiddenField).Value
        Dim ddlOperaciones As DropDownList = DirectCast(repItem.FindControl("ddlOperaciones"), DropDownList)
        Dim dtLineasFactura As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
        Dim drCostes() As DataRow = dtCostesLinea.Select("LINEA=" & LineaFactura & " AND ID='" & idCoste & "'")
        Dim bPorAlbaran As Boolean
        Dim numLineas As Byte

        'Muestro el boton de aplicar al resto de lineas cuando sabemos que el coste ya tiene un valor
        DirectCast(repItem.FindControl("imgAplicarCosteRestoLineas"), ImageButton).Visible = True

        If DBNullToInteger(drCostes(0)("ACT_ALBARAN")) = 1 Then
            'El coste se ha introducido por Albaran y habra que actualizar los costes de todas las lineas de ese albaran
            drCostes = dtCostesLinea.Select("ALBARAN='" & sAlbaran & "' AND ID='" & idCoste & "'")
            numLineas = drCostes.Count
            bPorAlbaran = True
        Else
            'Coste a nivel de linea
            drCostes = dtCostesLinea.Select("LINEA=" & LineaFactura & " AND ID='" & idCoste & "'")
        End If

        Dim importeTotalCosteAlbaran As Double = 0
        'Actualizamos la tabla de Costes de de linea
        For Each drCosteAlbaran As DataRow In drCostes
            'El coste se ha introducido por linea y solo actualizaremos esa linea
            Dim ImporteCoste As Double

            'Calculamos el importe del coste
            Select Case ddlOperaciones.SelectedValue
                Case "+"
                    drCosteAlbaran("OPERACION") = "+"
                    If bPorAlbaran Then
                        ImporteCoste = Valor / numLineas
                    Else
                        ImporteCoste = Valor
                    End If
                Case "+%"
                    drCosteAlbaran("OPERACION") = "+%"
                    Dim drLineaFactura() As DataRow = dtLineasFactura.Select("LINEA=" & drCosteAlbaran("LINEA"))
                    ImporteCoste = (DBNullToDbl(drLineaFactura(0)("PREC_UP")) * DBNullToDbl(drLineaFactura(0)("CANT_PED")))
                    ImporteCoste = DBNullToDbl((ImporteCoste * Valor)) / 100
            End Select

            'Actualizamos el valor del impuesto retenido en la tabla cacheada
            drCosteAlbaran("VALOR") = Valor
            'Recogemos el importe antiguo del impuesto para calcular el nuevo importe
            Dim dOldImporteCoste As Double = DBNullToDbl(drCosteAlbaran("IMPORTE"))
            drCosteAlbaran("IMPORTE") = ImporteCoste
            'Calculamos el importe que se metera en la tabla de costes por albaran
            importeTotalCosteAlbaran += ImporteCoste

            'Solo se actualizara el importe del coste en el detalle de costes de la linea que se esta modificando porque esta visible
            'Del resto de lineas que tienen el mismo albaran se modificaran solo en el datatable
            If drCosteAlbaran("ID") = idCoste Then
                'Ponemos el importe del coste despues de calcularlo
                DirectCast(repItem.FindControl("lblImporteCostes"), Label).Text = FormatNumber(Replace(ImporteCoste.ToString, ".", ","), m_oNumberFormat)

                'Actualizamos el importe total de costes de la linea de factura
                Dim repeaterCostes As Repeater = DirectCast(repItem.Parent, Repeater)
                Dim repItemFooter As RepeaterItem = DirectCast(repeaterCostes.Controls(repeaterCostes.Controls.Count - 1), RepeaterItem)
                Dim sOldImporte As String = DirectCast(repItemFooter.FindControl("lblTotalImporteCostesFooter"), Label).Text.Replace(m_sMoneda, "").Trim
                If sOldImporte = "" Then sOldImporte = 0
                Dim dImporte As Double = CDbl(sOldImporte) - dOldImporteCoste 'le restamos el antiguo importe del coste
                dImporte += ImporteCoste 'le sumamos el nuevo importe del coste
                DirectCast(repItemFooter.FindControl("lblTotalImporteCostesFooter"), Label).Text = FormatNumber(Replace(dImporte.ToString, ".", ","), m_oNumberFormat) & " " & m_sMoneda
            End If

            RecalcularImpuestoLinea(, drCosteAlbaran("LINEA"))
        Next

        'Actualizamos el importe neto de la linea de factura en el grid
        CargarLineasFactura()
        upLineas.Update()

        If bPorAlbaran Then
            'Actualizamos ahora la tabla de costes a nivel de Albaran
            Dim dtCostesAlbaran As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(12)
            drCostes = dtCostesAlbaran.Select("ALBARAN='" & sAlbaran & "' AND DEN='" & sDenominacionCoste & "'")
            If drCostes.Length > 0 Then
                drCostes(0)("VALOR") = Valor
                drCostes(0)("IMPORTE") = importeTotalCosteAlbaran
                drCostes(0)("OPERACION") = ddlOperaciones.SelectedValue
            End If
        End If

        RaiseEvent EventoRecalcularImportes(2, _ImporteTotalLineasConCD, Nothing, _ImporteTotalLineasConCDYImpuestos)
    End Sub
    ''' <summary>
    ''' Procedimiento que salta al cambiar el porcentaje o importe de un coste en el panel de detalle de costes de cada linea de factura
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub PorcentajeCostesChange(ByVal sender As Object, ByVal e As Infragistics.Web.UI.EditorControls.TextEditorValueChangedEventArgs)
        If e.NewValue <= 0 Then
            DirectCast(sender, Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = e.OldValue
        Else
            Dim dtCostesLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(11)
            Dim LineaFactura As String = hid_FacturaUpdating.Value
            Dim repItem As RepeaterItem = DirectCast(DirectCast(sender, Infragistics.Web.UI.EditorControls.WebNumericEditor).Parent.Parent, RepeaterItem)
            Dim idCoste As String = DirectCast(repItem.FindControl("hid_idCosteLinea"), HiddenField).Value
            Dim sDenominacionCoste As String = DirectCast(repItem.FindControl("lblDenominacionCosteLinea"), Label).Text
            Dim sAlbaran As String = DirectCast(repItem.FindControl("hid_Albaran"), HiddenField).Value
            Dim ddlOperaciones As DropDownList = DirectCast(repItem.FindControl("ddlOperaciones"), DropDownList)
            Dim dtLineasFactura As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
            Dim drCostes() As DataRow = dtCostesLinea.Select("LINEA=" & LineaFactura & " AND ID='" & idCoste & "'")
            Dim bPorAlbaran As Boolean
            Dim numLineas As Byte

            'Muestro el boton de aplicar al resto de lineas cuando sabemos que el coste ya tiene un valor
            DirectCast(repItem.FindControl("imgAplicarCosteRestoLineas"), ImageButton).Visible = True

            If DBNullToInteger(drCostes(0)("ACT_ALBARAN")) = 1 Then
                'El coste se ha introducido por Albaran y habra que actualizar los costes de todas las lineas de ese albaran
                drCostes = dtCostesLinea.Select("ALBARAN='" & sAlbaran & "' AND DEN='" & sDenominacionCoste & "'")
                numLineas = drCostes.Count
                bPorAlbaran = True
            Else
                'Coste a nivel de linea
                drCostes = dtCostesLinea.Select("LINEA=" & LineaFactura & " AND ID='" & idCoste & "'")
            End If

            Dim importeTotalCosteAlbaran As Double = 0
            'Actualizamos la tabla de Costes de de linea
            For Each drCosteAlbaran As DataRow In drCostes
                'El coste se ha introducido por linea y solo actualizaremos esa linea
                Dim ImporteCoste As Double

                'Calculamos el importe del coste
                Select Case ddlOperaciones.SelectedValue
                    Case "+"
                        drCosteAlbaran("OPERACION") = "+"
                        If bPorAlbaran Then
                            ImporteCoste = e.NewValue / numLineas
                        Else
                            ImporteCoste = e.NewValue
                        End If
                    Case "+%"
                        drCosteAlbaran("OPERACION") = "+%"
                        Dim drLineaFactura() As DataRow = dtLineasFactura.Select("LINEA=" & drCosteAlbaran("LINEA"))
                        ImporteCoste = (DBNullToDbl(drLineaFactura(0)("PREC_UP")) * DBNullToDbl(drLineaFactura(0)("CANT_PED")))
                        ImporteCoste = DBNullToDbl((ImporteCoste * e.NewValue)) / 100
                End Select

                'Actualizamos el valor del impuesto retenido en la tabla cacheada
                drCosteAlbaran("VALOR") = e.NewValue
                'Recogemos el importe antiguo del impuesto para calcular el nuevo importe
                Dim dOldImporteCoste As Double = DBNullToDbl(drCosteAlbaran("IMPORTE"))
                drCosteAlbaran("IMPORTE") = ImporteCoste
                'Calculamos el importe que se metera en la tabla de costes por albaran
                importeTotalCosteAlbaran += ImporteCoste

                'Solo se actualizara el importe del coste en el detalle de costes de la linea que se esta modificando porque esta visible
                'Del resto de lineas que tienen el mismo albaran se modificaran solo en el datatable
                If drCosteAlbaran("ID") = idCoste Then
                    'Ponemos el importe del coste despues de calcularlo
                    DirectCast(repItem.FindControl("lblImporteCostes"), Label).Text = FormatNumber(Replace(ImporteCoste.ToString, ".", ","), m_oNumberFormat)

                    'Actualizamos el importe total de costes de la linea de factura
                    Dim repeaterCostes As Repeater = DirectCast(repItem.Parent, Repeater)
                    Dim repItemFooter As RepeaterItem = DirectCast(repeaterCostes.Controls(repeaterCostes.Controls.Count - 1), RepeaterItem)
                    Dim sOldImporte As String = DirectCast(repItemFooter.FindControl("lblTotalImporteCostesFooter"), Label).Text.Replace(m_sMoneda, "").Trim
                    If sOldImporte = "" Then sOldImporte = 0
                    Dim dImporte As Double = CDbl(sOldImporte) - dOldImporteCoste 'le restamos el antiguo importe del coste
                    dImporte += ImporteCoste 'le sumamos el nuevo importe del coste
                    DirectCast(repItemFooter.FindControl("lblTotalImporteCostesFooter"), Label).Text = FormatNumber(Replace(dImporte.ToString, ".", ","), m_oNumberFormat) & " " & m_sMoneda
                End If

                RecalcularImpuestoLinea(, drCosteAlbaran("LINEA"))
            Next

            CargarLineasFactura()
            upLineas.Update()

            If bPorAlbaran Then
                'Actualizamos ahora la tabla de costes a nivel de Albaran
                Dim dtCostesAlbaran As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(12)
                drCostes = dtCostesAlbaran.Select("ALBARAN='" & sAlbaran & "' AND DEN='" & sDenominacionCoste & "'")
                If drCostes.Length > 0 Then
                    drCostes(0)("VALOR") = e.NewValue
                    drCostes(0)("IMPORTE") = importeTotalCosteAlbaran
                    drCostes(0)("OPERACION") = ddlOperaciones.SelectedValue
                End If
            End If
        End If

        RaiseEvent EventoRecalcularImportes(2, _ImporteTotalLineasConCD, Nothing, _ImporteTotalLineasConCDYImpuestos)
    End Sub


    ''' <summary>
    ''' evento del repeater al realizar alguna accion
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub rptCostesLineaFactura_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptCostesLineaFactura.ItemCommand
        Dim dtCostesLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(11)
        Dim dtLineasFactura As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
        Dim idCoste As String = DirectCast(e.Item.FindControl("hid_idCosteLinea"), HiddenField).Value
        Dim sAlbaran As String = DirectCast(e.Item.FindControl("hid_Albaran"), HiddenField).Value
        Dim sDenCoste As String = DirectCast(e.Item.FindControl("lblDenominacionCosteLinea"), Label).Text
        Dim LineaSeleccionada As String = hid_FacturaUpdating.Value
        Select Case e.CommandName
            Case "EliminarCosteLinea"
                Dim listDeletedRowsCostes As New List(Of String)
                For Each dr As DataRow In dtCostesLinea.Rows
                    If dr.RowState <> DataRowState.Deleted Then
                        If sAlbaran = String.Empty Then
                            'Si se elimina un coste por linea se eliminara solo en esa linea
                            If DBNullToStr(dr("ID")) = idCoste Then
                                dr.Delete()
                                Exit For
                            End If
                        Else
                            'Si se elimina un coste por albaran se eliminara en todas las lineas de factura que tengan ese albaran
                            If DBNullToStr(dr("ALBARAN")) = sAlbaran And sDenCoste = dr("DEN") Then
                                listDeletedRowsCostes.Add(dr("ID"))
                            End If
                        End If
                    End If
                Next

                If sAlbaran <> String.Empty Then
                    'Borramos las filas de costes por albaran que han sido marcadas anteriormente
                    Dim drToDelete() As DataRow
                    For Each sId As String In listDeletedRowsCostes
                        drToDelete = dtCostesLinea.Select("ID=" & sId)
                        dtCostesLinea.Rows.Remove(drToDelete(0))
                    Next

                    'Borramos de la tabla de albaranes
                    Dim dtCostesAlbaran As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(12)
                    For Each drAlbaran As DataRow In dtCostesAlbaran.Select("ALBARAN='" & sAlbaran & "' AND DEN='" & sDenCoste & "'")
                        dtCostesAlbaran.Rows.Remove(drAlbaran)
                    Next
                End If

                RecalcularImpuestoLinea()
                CargarLineasFactura()
                upLineas.Update()
                RaiseEvent EventoRecalcularImportes(2, _ImporteTotalLineasConCD, Nothing, _ImporteTotalLineasConCDYImpuestos)
                CargarPanelDetalleCostes()
            Case "AplicarCosteRestoLineas"
                'Selecciono el Coste que quiero copiar
                Dim drLineaCopiar() As DataRow = dtCostesLinea.Select("ID=" & idCoste)
                Dim arrLineasActualizar As New List(Of String())

                Dim idNewcoste As Short = hid_UltimoNuevoIdCoste.Value
                'Recorro las lineas de factura
                For Each dr As DataRow In dtLineasFactura.Rows
                    'Si la linea es distinta que la linea del coste seleccionado la comprobare para ver si se le puede aplicar
                    If dr("LINEA") <> LineaSeleccionada Then
                        'Selecciono los costes de esa linea de factura
                        Dim drLineasActualizar() As DataRow = dtCostesLinea.Select("LINEA=" & dr("LINEA"))
                        Dim bCosteExiste As Boolean
                        Dim itemLinea(5) As String
                        For Each drLineaActualizar As DataRow In drLineasActualizar
                            'Compruebo que el codigo de coste que quiero introducir en esa linea no este ya
                            If drLineaActualizar("DEN") = drLineaCopiar(0)("DEN") Then
                                bCosteExiste = True
                            End If
                        Next
                        If bCosteExiste = False Then
                            'Si no existe ese coste en la linea de factura lo añado a la lista de lineas al que añadir ese coste copiado
                            itemLinea(0) = dr("LINEA")
                            itemLinea(1) = dr("CANT_PED")
                            itemLinea(2) = dr("PREC_UP")
                            itemLinea(3) = dr("TOTAL_COSTES")
                            itemLinea(4) = dr("TOTAL_DCTOS")
                            itemLinea(5) = dr("ALBARAN")
                            arrLineasActualizar.Add(itemLinea)
                        End If
                        bCosteExiste = False
                    End If

                Next
                Dim newRow As DataRow
                For i = 0 To arrLineasActualizar.Count - 1
                    Dim item(5) As String
                    item = arrLineasActualizar(i)
                    newRow = dtCostesLinea.NewRow
                    newRow("ID") = idNewcoste
                    newRow("LINEA") = item(0)
                    newRow("FACTURA") = drLineaCopiar(0)("FACTURA")
                    newRow("TIPO") = drLineaCopiar(0)("TIPO")
                    newRow("DEN") = drLineaCopiar(0)("DEN")
                    newRow("VALOR") = drLineaCopiar(0)("VALOR")

                    Dim numLineasMismoAlbaran As Integer = 0
                    Select Case drLineaCopiar(0)("OPERACION")
                        Case "+"
                            If arrLineasActualizar(i)(5) <> String.Empty Then
                                'HabrÃ¡ que dividir el valor entre las lÃ­neas que forman el albarÃ¡n
                                For j = 0 To arrLineasActualizar.Count - 1
                                    If arrLineasActualizar(j)(5) = arrLineasActualizar(i)(5) Then
                                        numLineasMismoAlbaran = numLineasMismoAlbaran + 1
                                    End If
                                Next
                                If numLineasMismoAlbaran > 0 Then _
                                    newRow("IMPORTE") = drLineaCopiar(0)("VALOR") / numLineasMismoAlbaran
                            Else
                                newRow("IMPORTE") = drLineaCopiar(0)("VALOR")
                            End If
                        Case "+%"
                            Dim importeCoste As Double
                            importeCoste = (DBNullToDbl(arrLineasActualizar(i)(2)) * DBNullToDbl(arrLineasActualizar(i)(1)))
                            importeCoste = DBNullToDbl((importeCoste * drLineaCopiar(0)("VALOR"))) / 100
                            newRow("IMPORTE") = importeCoste
                    End Select
                    newRow("OPERACION") = drLineaCopiar(0)("OPERACION")
                    newRow("DEF_ATRIB_ID") = drLineaCopiar(0)("DEF_ATRIB_ID")
                    newRow("ALBARAN") = arrLineasActualizar(i)(5)
                    newRow("ACT_ALBARAN") = If(arrLineasActualizar(i)(5) <> String.Empty, 1, 0)
                    dtCostesLinea.Rows.Add(newRow)

                    'Ahora se graban los costes a nivel de Albaran
                    Dim dtCostesAlbaran As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(12)
                    Dim drCostesAlbaran() As DataRow = dtCostesAlbaran.Select("ALBARAN='" & arrLineasActualizar(i)(5) & "' AND DEN='" & drLineaCopiar(0)("DEN") & "'")
                    If drCostesAlbaran.Length = 0 Then
                        Dim newRowAlbaran As DataRow
                        newRowAlbaran = dtCostesAlbaran.NewRow
                        newRowAlbaran("ID") = idNewcoste
                        newRowAlbaran("FACTURA") = m_sIdFactura
                        newRowAlbaran("ALBARAN") = arrLineasActualizar(i)(5)
                        newRowAlbaran("TIPO") = 0 '0 por ser Coste, 1 si es descuento
                        newRowAlbaran("DEN") = drLineaCopiar(0)("DEN")
                        newRowAlbaran("DEF_ATRIB_ID") = drLineaCopiar(0)("DEF_ATRIB_ID")
                        newRowAlbaran("VALOR") = drLineaCopiar(0)("VALOR")
                        If drLineaCopiar(0)("OPERACION") = "+" Then
                            If numLineasMismoAlbaran > 0 Then _
                                    newRowAlbaran("IMPORTE") = drLineaCopiar(0)("VALOR") / numLineasMismoAlbaran
                        Else
                            newRowAlbaran("IMPORTE") = drLineaCopiar(0)("VALOR")
                        End If
                        newRowAlbaran("OPERACION") = drLineaCopiar(0)("OPERACION")
                        newRowAlbaran("PLANIFICADO") = 0 'No planificado
                        dtCostesAlbaran.Rows.Add(newRowAlbaran)
                    End If

                    idNewcoste -= 1
                Next
                hid_UltimoNuevoIdCoste.Value = idNewcoste

                RecalcularImpuestoLinea()
                CargarLineasFactura()
                upLineas.Update()
                RaiseEvent EventoRecalcularImportes(2, _ImporteTotalLineasConCD, Nothing, _ImporteTotalLineasConCDYImpuestos)
                mpePanelDetalleCostes.Hide()
        End Select
    End Sub

    Protected Sub PorcentajeChange()

    End Sub

    ''' <summary>
    ''' evento que salta por cada coste del repeater del detalle de costes de cada linea
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub rptCostesLineaFactura_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptCostesLineaFactura.ItemDataBound
        Static ImporteTotal As Double
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                DirectCast(e.Item.FindControl("hid_idCosteLinea"), HiddenField).Value = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("ID"))
                DirectCast(e.Item.FindControl("hid_Albaran"), HiddenField).Value = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("ALBARAN"))
                DirectCast(e.Item.FindControl("lblDenominacionCosteLinea"), Label).Text = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("DEN"))
                If DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("ALBARAN")) <> String.Empty Then
                    DirectCast(e.Item.FindControl("lblCostePorLineaAlbaran"), Label).Text = oTextos(78).Item(1)
                Else
                    DirectCast(e.Item.FindControl("lblCostePorLineaAlbaran"), Label).Text = oTextos(79).Item(1)
                End If
                Dim iPlanificado As Integer = DBNullToInteger(DirectCast(e.Item.DataItem, DataRow)("PLANIFICADO"))
                If m_bEditable AndAlso iPlanificado = 0 Then
                    DirectCast(e.Item.FindControl("lblPorcentaje"), Label).Visible = False
                    DirectCast(e.Item.FindControl("wnePorcentaje"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = DBNullToDbl(DirectCast(e.Item.DataItem, DataRow)("VALOR"))
                    DirectCast(e.Item.FindControl("lblOperaciones"), Label).Visible = False
                    DirectCast(e.Item.FindControl("ddlOperaciones"), DropDownList).Attributes.Add("onchange", "javascript: { if (document.getElementById('" & DirectCast(e.Item.FindControl("wnePorcentaje"), Infragistics.Web.UI.EditorControls.WebNumericEditor).ClientID & "').value == '0') return false;}")
                    Dim oListItem As ListItem = DirectCast(e.Item.FindControl("ddlOperaciones"), DropDownList).Items.FindByValue(DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("OPERACION")))
                    If Not oListItem Is Nothing Then
                        oListItem.Selected = True
                    End If

                    Dim dImporte As Double = DBNullToDbl(DirectCast(e.Item.DataItem, DataRow)("IMPORTE"))
                    DirectCast(e.Item.FindControl("lblImporteCostes"), Label).Text = FormatNumber(Replace(dImporte.ToString, ".", ","), m_oNumberFormat)
                    ImporteTotal += dImporte
                    If DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("ALBARAN")) = String.Empty Then
                        If DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("VALOR")) = String.Empty Then
                            DirectCast(e.Item.FindControl("imgAplicarCosteRestoLineas"), ImageButton).Visible = False
                        End If
                    End If
                    DirectCast(e.Item.FindControl("imgEliminarCosteLinea"), ImageButton).ImageUrl = _RutaImagenes & "eliminar.png"
                    DirectCast(e.Item.FindControl("imgAplicarCosteRestoLineas"), ImageButton).ImageUrl = _RutaImagenes & "Aplicar.jpg"
                    DirectCast(e.Item.FindControl("imgAplicarCosteRestoLineas"), ImageButton).ToolTip = oTextos(156).Item(1)
                Else
                    DirectCast(e.Item.FindControl("tdPorcentaje"), HtmlTableCell).Style("background-color") = "#EEEEEE"
                    DirectCast(e.Item.FindControl("lblPorcentaje"), Label).Text = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("VALOR"))
                    DirectCast(e.Item.FindControl("wnePorcentaje"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Visible = False
                    DirectCast(e.Item.FindControl("tdOperaciones"), HtmlTableCell).Style("background-color") = "#EEEEEE"
                    DirectCast(e.Item.FindControl("ddlOperaciones"), DropDownList).Visible = False
                    DirectCast(e.Item.FindControl("lblOperaciones"), Label).Text = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("OPERACION"))
                    Dim dImporte As Double = DBNullToDbl(DirectCast(e.Item.DataItem, DataRow)("IMPORTE"))
                    DirectCast(e.Item.FindControl("lblImporteCostes"), Label).Text = FormatNumber(Replace(dImporte.ToString, ".", ","), m_oNumberFormat)
                    ImporteTotal += dImporte
                    DirectCast(e.Item.FindControl("CeldaEliminarCoste"), HtmlTableCell).Visible = False
                    DirectCast(e.Item.FindControl("CeldaAplicarCoste"), HtmlTableCell).Visible = False
                End If
            Case ListItemType.Footer
                DirectCast(e.Item.FindControl("lblTotalCostesFooter"), Label).Text = String.Format("{0}: ", oTextos(23).Item(1))
                DirectCast(e.Item.FindControl("lblTotalImporteCostesFooter"), Label).Text = FormatNumber(Replace(ImporteTotal.ToString, ".", ","), m_oNumberFormat) & " " & m_sMoneda
        End Select
    End Sub

    Private Sub btnBuscarCostes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarCostes.Click

        RaiseEvent EventoBuscarCostes(txtCodigo_BusqCostesLinea.Text, txtDescripcion_BusqCostesLinea.Text, hidArticuloCostes.Value, hidMaterialCostes.Value)

        If m_dsCostesBuscador.Tables(0).Rows.Count = 0 Then
            phCosteGenerico.Visible = True
            whdgBuscadorCostes.Visible = False
        Else
            phCosteGenerico.Visible = False
            whdgBuscadorCostes.Visible = True
            whdgBuscadorCostes.DataSource = m_dsCostesBuscador
            whdgBuscadorCostes.DataBind()
        End If
    End Sub

    Private Sub btnAceptarCoste_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarCoste.Click
        Dim linea As String = hid_FacturaUpdating.Value 'Recojo la linea de factura a la que añadiria el coste
        Dim dtCostesLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(11)
        Dim dtLineas As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
        Dim idNewCoste As Short = If(hid_UltimoNuevoIdCoste.Value = "", -1, hid_UltimoNuevoIdCoste.Value)


        'Seleccionamos los costes de la linea si los tuviese
        Dim drCoste() As DataRow = dtCostesLinea.Select("LINEA=" & linea)
        'Seleccionamos la linea de factura para obtener sus campos CANTIDAD, PRECIO...
        Dim drLineaFactura() As DataRow = dtLineas.Select("LINEA=" & linea)
        Dim filaSeleccionada As Infragistics.Web.UI.GridControls.GridRecord
        Dim sDenominacion As String = Nothing
        Dim lDefAtribId As Long = Nothing

        If ddlCostesFacturas.SelectedValue = String.Empty AndAlso phCosteGenerico.Visible = False Then
            'Deberia haber seleccionado el coste del grid
            If whdgBuscadorCostes.GridView.Behaviors.Selection.SelectedRows.Count > 0 Then
                filaSeleccionada = whdgBuscadorCostes.GridView.Behaviors.Selection.SelectedRows(0)
                sDenominacion = DBNullToStr(filaSeleccionada.Items.FindItemByKey("DEN").Value)
                lDefAtribId = DBNullToInteger(filaSeleccionada.Items.FindItemByKey("ID").Value)
            Else
                'si acepta y no ha seleccionado ni del combo...ni del grid, cerramos el buscador
                mpePanelBuscadorCostes.Hide()
            End If
        ElseIf phCosteGenerico.Visible = True Then
            'Deberia haber escrito un coste generico
            If txtCosteGenerico.Text <> String.Empty Then
                lDefAtribId = m_lIdCosteGenerico
                sDenominacion = txtCosteGenerico.Text
            Else
                mpePanelBuscadorCostes.Hide()
            End If
        Else
            'Deberia haber seleccionado el coste del combo de ultimos costes utilizados en facturas
            sDenominacion = ddlCostesFacturas.SelectedItem.Text
            lDefAtribId = ddlCostesFacturas.SelectedValue
        End If

        Dim sAlbaran As String = DBNullToStr(drLineaFactura(0)("ALBARAN"))

        If rbCosteLinea.Visible AndAlso rbCosteLinea.Checked Then
            Dim drValidacionLinea() As DataRow = dtCostesLinea.Select("LINEA=" & linea & " AND DEF_ATRIB_ID=" & lDefAtribId)
            If drValidacionLinea.Count > 0 Then
                'No se puede añadir el mismo coste a una linea a la que ya se le ha añadido
                mpePanelBuscadorCostes.Hide()
                CargarPanelDetalleCostes()
                Exit Sub
            End If

            'Inserto el Coste a nivel de linea en la tabla de costes de linea
            Dim newRow As DataRow
            newRow = dtCostesLinea.NewRow
            newRow("ID") = idNewCoste
            newRow("FACTURA") = m_sIdFactura
            newRow("LINEA") = linea
            newRow("TIPO") = 0 '0 por ser Coste, 1 si es descuento
            newRow("DEN") = sDenominacion
            newRow("DEF_ATRIB_ID") = lDefAtribId
            dtCostesLinea.Rows.Add(newRow)
            RecalcularImpuestoLinea()
            CargarLineasFactura()
            upLineas.Update()

            CargarPanelDetalleCostes()
            upDetalleCostesLinea.Update()
            mpePanelBuscadorCostes.Hide()
        Else

            Dim drValidacionAlbaran() As DataRow = dtCostesLinea.Select("LINEA=" & linea & " AND DEF_ATRIB_ID=" & lDefAtribId)
            If drValidacionAlbaran.Count > 0 Then
                'No se puede añadir el mismo descuento a un albaran al que ya se le ha añadido
                'PENDIENTE
                mpePanelBuscadorCostes.Hide()
                CargarPanelDetalleCostes()
                Exit Sub
            End If
            'Coste a nivel de Albaran, se graban los costes en cada una de las lineas de factura que tengan ese Albaran, el importe
            'se actualizada cuando desde la pantalla de detalle una vez introducido el usuario introduzca el valor del coste
            If sAlbaran <> String.Empty Then
                Dim drLineasAlbaran() As DataRow = dtLineas.Select("ALBARAN='" & sAlbaran & "'")
                For Each drLineaAlbaran As DataRow In drLineasAlbaran
                    Dim newRow As DataRow
                    newRow = dtCostesLinea.NewRow
                    newRow("ID") = idNewCoste
                    newRow("FACTURA") = m_sIdFactura
                    newRow("LINEA") = drLineaAlbaran("LINEA")
                    newRow("TIPO") = 0 '0 por ser Coste, 1 si es descuento
                    newRow("DEN") = sDenominacion
                    newRow("ACT_ALBARAN") = 1 'Le indicamos que cuando en el detalle indique el valor sabra que filas tiene que actualizar
                    If sAlbaran <> String.Empty Then
                        newRow("ALBARAN") = sAlbaran
                    End If
                    newRow("DEF_ATRIB_ID") = lDefAtribId
                    dtCostesLinea.Rows.Add(newRow)
                    idNewCoste -= 1
                Next
            End If

            'Ahora se graban los costes a nivel de Albaran
            Dim dtCostesAlbaran As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(12)
            Dim newRowAlbaran As DataRow
            newRowAlbaran = dtCostesAlbaran.NewRow
            newRowAlbaran("ID") = idNewCoste
            newRowAlbaran("FACTURA") = m_sIdFactura
            newRowAlbaran("ALBARAN") = sAlbaran
            newRowAlbaran("TIPO") = 0 '0 por ser Coste, 1 si es descuento
            newRowAlbaran("DEN") = sDenominacion
            newRowAlbaran("DEF_ATRIB_ID") = lDefAtribId
            newRowAlbaran("PLANIFICADO") = 0 'No planificado
            dtCostesAlbaran.Rows.Add(newRowAlbaran)

            RecalcularImpuestoLinea()
            CargarLineasFactura()
            upLineas.Update()

            CargarPanelDetalleCostes()
            upDetalleCostesLinea.Update()
            mpePanelBuscadorCostes.Hide()
        End If
        hid_UltimoNuevoIdCoste.Value = idNewCoste - 1

    End Sub
    ''' <summary>
    ''' Procedimiento que limpia los campos del buscador de costes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnLimpiarCostes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarCostes.Click
        txtCodigo_BusqCostesLinea.Text = ""
        hidArticuloCostes.Value = ""
        txtMaterial_BusqCosteLinea.Text = ""
        hidMaterialCostes.Value = ""
        txtCodigo_BusqCostesLinea.Text = ""
        txtDescripcion_BusqCostesLinea.Text = ""
        upBusquedaCostes.Update()
    End Sub
#End Region
#Region "DESCUENTOS"
    ''' <summary>
    ''' Evento que se utiliza para mostrar el modalpopup de detalle de descuentos de linea
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnMostrarPanelDetalleDescuentos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMostrarPanelDetalleDescuentos.Click
        CargarPanelDetalleDescuentos()
    End Sub

    ''' <summary>
    ''' Procedimiento que actualiza los datos del detalle de descuentos de una linea antes de mostrarlos
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarPanelDetalleDescuentos()
        Dim dtDescuentos As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(13)
        Dim dtDescuentosLinea() As DataRow = dtDescuentos.Select("LINEA=" & Me.hid_FacturaUpdating.Value)
        Dim dtLineaFactura() As DataRow = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6).Select("LINEA=" & Me.hid_FacturaUpdating.Value)

        _ImporteBrutoLineaSeleccionada = DBNullToDbl(dtLineaFactura(0)("CANT_PED")) * DBNullToDbl(dtLineaFactura(0)("PREC_UP"))
        _DenominacionArticuloLineaSeleccionada = DBNullToStr(dtLineaFactura(0)("ART_DEN"))
        _CodLineaSeleccionada = Me.hid_FacturaUpdating.Value.ToString.PadLeft(3, "0")
        _CodArticuloLineaSeleccionada = DBNullToStr(dtLineaFactura(0)("ART_INT"))

        lblDescripcionLinea_Descuentos.Text = _CodLineaSeleccionada & " " & _CodArticuloLineaSeleccionada & " " & _DenominacionArticuloLineaSeleccionada
        lblCalculoImporteLinea_Descuentos.Text = String.Format("{0}: ", oTextos(75).Item(1)) & FormatNumber(Replace(_ImporteBrutoLineaSeleccionada.ToString, ".", ","), m_oNumberFormat) & " " & m_sMoneda

        If Not m_bEditable OrElse (Not m_bCostesPorLinea AndAlso DBNullToStr(dtLineaFactura(0)("ALBARAN")) = "") Then
            lblAgregarDescuento.Visible = False
            imgAgregarDescuento.Visible = False
        End If

        rptDescuentosLineaFactura.DataSource = dtDescuentosLinea
        rptDescuentosLineaFactura.DataBind()
        mpePanelDetalleDescuentos.Show()
    End Sub

    ''' <summary>
    ''' Funcion que calcula el importe de los descuentos de una linea
    ''' </summary>
    ''' <param name="drDescuentos">descuentos para esa linea de factura</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CalculoImporteDescuentosLinea(ByVal drDescuentos() As DataRow) As String

        Dim Importe As Double

        For Each drDescuento As DataRow In drDescuentos
            Importe = Importe + DBNullToDbl(drDescuento("IMPORTE"))
        Next

        Return Importe.ToString
    End Function


    ''' <summary>
    ''' Carga el combo de descuentos utilizados con anterioridad en otras facturas en el buscador de descuentos por linea
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarComboDescuentosUtilizados()
        Dim oItem As WebControls.ListItem

        oItem = New WebControls.ListItem
        oItem.Value = ""
        oItem.Text = ""
        ddlDescuentosFacturas.Items.Add(oItem)

        For Each oRow As DataRow In m_dsDescuentosUtilizadosBuscador.Tables(0).Rows
            oItem = New WebControls.ListItem
            oItem.Value = DBNullToStr(oRow.Item("ID"))
            oItem.Text = DBNullToStr(oRow.Item("DEN"))
            ddlDescuentosFacturas.Items.Add(oItem)
        Next
    End Sub

    Protected Sub ddlOperacionesDescuentos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objNumeric As Infragistics.Web.UI.EditorControls.WebNumericEditor = DirectCast(sender.parent.parent, System.Web.UI.WebControls.RepeaterItem).FindControl("wnePorcentaje")

        If objNumeric.Value Is Nothing OrElse objNumeric.Value = 0 Then
            Exit Sub
        End If

        Dim Valor As Double = objNumeric.ValueDouble

        Dim dtDescuentosLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(13)
        Dim LineaFactura As String = hid_FacturaUpdating.Value
        Dim repItem As RepeaterItem = DirectCast(sender.parent.parent, System.Web.UI.WebControls.RepeaterItem)
        Dim idDescuento As String = DirectCast(repItem.FindControl("hid_idDescuentoLinea"), HiddenField).Value
        Dim sDenominacionDescuento As String = DirectCast(repItem.FindControl("lblDenominacionDescuentoLinea"), Label).Text
        Dim sAlbaran As String = DirectCast(repItem.FindControl("hid_Albaran"), HiddenField).Value
        Dim ddlOperaciones As DropDownList = DirectCast(repItem.FindControl("ddlOperaciones"), DropDownList)
        Dim dtLineasFactura As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
        Dim drDescuentos() As DataRow = dtDescuentosLinea.Select("LINEA=" & LineaFactura & " AND ID='" & idDescuento & "'")
        Dim bPorAlbaran As Boolean
        Dim numLineas As Byte

        'Muestro el boton de aplicar al resto de lineas cuando sabemos que el Descuento ya tiene un valor
        DirectCast(repItem.FindControl("imgAplicarDescuentoRestoLineas"), ImageButton).Visible = True

        If DBNullToInteger(drDescuentos(0)("ACT_ALBARAN")) = 1 Then
            'El Descuento se ha introducido por Albaran y habra que actualizar los Descuentos de todas las lineas de ese albaran
            drDescuentos = dtDescuentosLinea.Select("ALBARAN='" & sAlbaran & "' AND ID='" & idDescuento & "'")
            numLineas = drDescuentos.Count
            bPorAlbaran = True
        Else
            'Coste a nivel de linea
            drDescuentos = dtDescuentosLinea.Select("LINEA=" & LineaFactura & " AND ID='" & idDescuento & "'")
        End If

        Dim importeTotalDescuentoAlbaran As Double = 0
        'Actualizamos la tabla de Descuentos de de linea
        For Each drDescuentoAlbaran As DataRow In drDescuentos
            'El Descuento se ha introducido por linea y solo actualizaremos esa linea
            Dim ImporteDescuento As Double

            'Calculamos el importe del Descuento
            Select Case ddlOperaciones.SelectedValue
                Case "-"
                    drDescuentoAlbaran("OPERACION") = "-"
                    If bPorAlbaran Then
                        ImporteDescuento = Valor / numLineas
                    Else
                        ImporteDescuento = Valor
                    End If
                Case "-%"
                    drDescuentoAlbaran("OPERACION") = "-%"
                    Dim drLineaFactura() As DataRow = dtLineasFactura.Select("LINEA=" & drDescuentoAlbaran("LINEA"))
                    ImporteDescuento = (DBNullToDbl(drLineaFactura(0)("PREC_UP")) * DBNullToDbl(drLineaFactura(0)("CANT_PED")))
                    ImporteDescuento = DBNullToDbl((ImporteDescuento * Valor)) / 100
            End Select

            'Actualizamos el valor del Descuento  en la tabla cacheada
            drDescuentoAlbaran("VALOR") = Valor
            'Recogemos el importe antiguo del impuesto para calcular el nuevo importe
            Dim dOldImporteDescuento As Double = DBNullToDbl(drDescuentoAlbaran("IMPORTE"))
            drDescuentoAlbaran("IMPORTE") = ImporteDescuento
            'Calculamos el importe que se metera en la tabla de costes por albaran
            importeTotalDescuentoAlbaran += ImporteDescuento

            'Solo se actualizara el importe del coste en el detalle de costes de la linea que se esta modificando porque esta visible
            'Del resto de lineas que tienen el mismo albaran se modificaran solo en el datatable
            If drDescuentoAlbaran("ID") = idDescuento Then
                'Ponemos el importe del coste despues de calcularlo
                DirectCast(repItem.FindControl("lblImporteDescuentos"), Label).Text = FormatNumber(Replace(ImporteDescuento.ToString, ".", ","), m_oNumberFormat)

                'Actualizamos el importe total de costes de la linea de factura
                Dim repeaterDescuentos As Repeater = DirectCast(repItem.Parent, Repeater)
                Dim repItemFooter As RepeaterItem = DirectCast(repeaterDescuentos.Controls(repeaterDescuentos.Controls.Count - 1), RepeaterItem)
                Dim sOldImporte As String = DirectCast(repItemFooter.FindControl("lblTotalImporteDescuentosFooter"), Label).Text.Replace(m_sMoneda, "").Trim
                If sOldImporte = "" Then sOldImporte = 0
                Dim dImporte As Double = CDbl(sOldImporte) - dOldImporteDescuento 'le restamos el antiguo importe del Descuento
                dImporte += ImporteDescuento 'le restamos el nuevo importe del Descuento
                DirectCast(repItemFooter.FindControl("lblTotalImporteDescuentosFooter"), Label).Text = FormatNumber(Replace(dImporte.ToString, ".", ","), m_oNumberFormat) & " " & m_sMoneda
            End If

            RecalcularImpuestoLinea(, drDescuentoAlbaran("LINEA"))
        Next

        'Actualizamos el importe neto de la linea de factura en el grid
        CargarLineasFactura()
        upLineas.Update()

        If bPorAlbaran Then
            'Actualizamos ahora la tabla de descuentos a nivel de Albaran
            Dim dtDescuentosAlbaran As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(14)
            drDescuentos = dtDescuentosAlbaran.Select("ALBARAN='" & sAlbaran & "' AND DEN='" & sDenominacionDescuento & "'")
            drDescuentos(0)("VALOR") = Valor
            drDescuentos(0)("IMPORTE") = importeTotalDescuentoAlbaran
            drDescuentos(0)("OPERACION") = ddlOperaciones.SelectedValue
        End If


        RaiseEvent EventoRecalcularImportes(1, _ImporteTotalLineasConCD, Nothing, _ImporteTotalLineasConCDYImpuestos)
    End Sub



    ''' <summary>
    ''' Procedimiento que salta al cambiar el porcentaje o importe de un descuento en el panel de detalle de descuentos de cada linea de factura
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub PorcentajeDescuentosChange(ByVal sender As Object, ByVal e As Infragistics.Web.UI.EditorControls.TextEditorValueChangedEventArgs)
        If e.NewValue <= 0 Then
            DirectCast(sender, Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = e.OldValue
        Else
            Dim dtDescuentosLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(13)
            Dim LineaFactura As String = hid_FacturaUpdating.Value
            Dim repItem As RepeaterItem = DirectCast(DirectCast(sender, Infragistics.Web.UI.EditorControls.WebNumericEditor).Parent.Parent, RepeaterItem)
            Dim idDescuento As String = DirectCast(repItem.FindControl("hid_idDescuentoLinea"), HiddenField).Value
            Dim sDenominacionDescuento As String = DirectCast(repItem.FindControl("lblDenominacionDescuentoLinea"), Label).Text
            Dim sAlbaran As String = DirectCast(repItem.FindControl("hid_Albaran"), HiddenField).Value
            Dim ddlOperaciones As DropDownList = DirectCast(repItem.FindControl("ddlOperaciones"), DropDownList)
            Dim dtLineasFactura As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
            Dim drDescuentos() As DataRow = dtDescuentosLinea.Select("LINEA=" & LineaFactura & " AND ID='" & idDescuento & "'")
            Dim bPorAlbaran As Boolean
            Dim numLineas As Byte

            'Muestro el boton de aplicar al resto de lineas cuando sabemos que el Descuento ya tiene un valor
            DirectCast(repItem.FindControl("imgAplicarDescuentoRestoLineas"), ImageButton).Visible = True

            If DBNullToInteger(drDescuentos(0)("ACT_ALBARAN")) = 1 Then
                'El Descuento se ha introducido por Albaran y habra que actualizar los Descuentos de todas las lineas de ese albaran
                drDescuentos = dtDescuentosLinea.Select("ALBARAN='" & sAlbaran & "' AND DEN='" & sDenominacionDescuento & "'")
                numLineas = drDescuentos.Count
                bPorAlbaran = True
            Else
                'Coste a nivel de linea
                drDescuentos = dtDescuentosLinea.Select("LINEA=" & LineaFactura & " AND ID='" & idDescuento & "'")
            End If

            Dim importeTotalDescuentoAlbaran As Double = 0
            'Actualizamos la tabla de Descuentos de de linea
            For Each drDescuentoAlbaran As DataRow In drDescuentos
                'El Descuento se ha introducido por linea y solo actualizaremos esa linea
                Dim ImporteDescuento As Double

                'Calculamos el importe del Descuento
                Select Case ddlOperaciones.SelectedValue
                    Case "-"
                        drDescuentoAlbaran("OPERACION") = "-"
                        If bPorAlbaran Then
                            ImporteDescuento = e.NewValue / numLineas
                        Else
                            ImporteDescuento = e.NewValue
                        End If
                    Case "-%"
                        drDescuentoAlbaran("OPERACION") = "-%"
                        Dim drLineaFactura() As DataRow = dtLineasFactura.Select("LINEA=" & drDescuentoAlbaran("LINEA"))
                        ImporteDescuento = (DBNullToDbl(drLineaFactura(0)("PREC_UP")) * DBNullToDbl(drLineaFactura(0)("CANT_PED")))
                        ImporteDescuento = DBNullToDbl((ImporteDescuento * e.NewValue)) / 100
                End Select

                'Actualizamos el valor del Descuento  en la tabla cacheada
                drDescuentoAlbaran("VALOR") = e.NewValue
                'Recogemos el importe antiguo del impuesto para calcular el nuevo importe
                Dim dOldImporteDescuento As Double = DBNullToDbl(drDescuentoAlbaran("IMPORTE"))
                drDescuentoAlbaran("IMPORTE") = ImporteDescuento
                'Calculamos el importe que se metera en la tabla de costes por albaran
                importeTotalDescuentoAlbaran += ImporteDescuento

                'Solo se actualizara el importe del coste en el detalle de costes de la linea que se esta modificando porque esta visible
                'Del resto de lineas que tienen el mismo albaran se modificaran solo en el datatable
                If drDescuentoAlbaran("ID") = idDescuento Then
                    'Ponemos el importe del coste despues de calcularlo
                    DirectCast(repItem.FindControl("lblImporteDescuentos"), Label).Text = FormatNumber(Replace(ImporteDescuento.ToString, ".", ","), m_oNumberFormat)

                    'Actualizamos el importe total de costes de la linea de factura
                    Dim repeaterDescuentos As Repeater = DirectCast(repItem.Parent, Repeater)
                    Dim repItemFooter As RepeaterItem = DirectCast(repeaterDescuentos.Controls(repeaterDescuentos.Controls.Count - 1), RepeaterItem)
                    Dim sOldImporte As String = DirectCast(repItemFooter.FindControl("lblTotalImporteDescuentosFooter"), Label).Text.Replace(m_sMoneda, "").Trim
                    If sOldImporte = "" Then sOldImporte = 0
                    Dim dImporte As Double = CDbl(sOldImporte) - dOldImporteDescuento 'le restamos el antiguo importe del Descuento
                    dImporte += ImporteDescuento 'le restamos el nuevo importe del Descuento
                    DirectCast(repItemFooter.FindControl("lblTotalImporteDescuentosFooter"), Label).Text = FormatNumber(Replace(dImporte.ToString, ".", ","), m_oNumberFormat) & " " & m_sMoneda
                End If

                RecalcularImpuestoLinea(, drDescuentoAlbaran("LINEA"))
            Next

            'Actualizamos el importe neto de la linea de factura en el grid
            CargarLineasFactura()
            upLineas.Update()

            If bPorAlbaran Then
                'Actualizamos ahora la tabla de descuentos a nivel de Albaran
                Dim dtDescuentosAlbaran As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(14)
                drDescuentos = dtDescuentosAlbaran.Select("ALBARAN='" & sAlbaran & "' AND DEN='" & sDenominacionDescuento & "'")
                drDescuentos(0)("VALOR") = e.NewValue
                drDescuentos(0)("IMPORTE") = importeTotalDescuentoAlbaran
                drDescuentos(0)("OPERACION") = ddlOperaciones.SelectedValue
            End If
        End If

        RaiseEvent EventoRecalcularImportes(1, _ImporteTotalLineasConCD, Nothing, _ImporteTotalLineasConCDYImpuestos)
    End Sub


    ''' <summary>
    ''' evento del repeater al realizar alguna accion
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub rptDescuentosLineaFactura_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptDescuentosLineaFactura.ItemCommand
        Dim dtDescuentosLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(13)
        Dim dtLineasFactura As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
        Dim idDescuento As String = DirectCast(e.Item.FindControl("hid_idDescuentoLinea"), HiddenField).Value
        Dim sAlbaran As String = DirectCast(e.Item.FindControl("hid_Albaran"), HiddenField).Value
        Dim sDenDescuento As String = DirectCast(e.Item.FindControl("lblDenominacionDescuentoLinea"), Label).Text
        Dim LineaSeleccionada As String = hid_FacturaUpdating.Value
        Select Case e.CommandName
            Case "EliminarDescuentoLinea"
                Dim listDeletedRowsDescuentos As New List(Of String)
                For Each dr As DataRow In dtDescuentosLinea.Rows
                    If dr.RowState <> DataRowState.Deleted Then
                        If sAlbaran = String.Empty Then
                            'Si se elimina un Descuento por linea se eliminara solo en esa linea
                            If DBNullToStr(dr("ID")) = idDescuento Then
                                dr.Delete()
                                Exit For
                            End If
                        Else
                            'Si se elimina un Descuento por albaran se eliminara en todas las lineas de factura que tengan ese albaran
                            If DBNullToStr(dr("ALBARAN")) = sAlbaran AndAlso sDenDescuento = DBNullToStr(dr("DEN")) Then
                                listDeletedRowsDescuentos.Add(dr("ID"))
                            End If
                        End If
                    End If
                Next

                If sAlbaran <> String.Empty Then
                    'Borramos las filas de descuentos por albaran que han sido marcadas anteriormente
                    Dim drToDelete() As DataRow
                    For Each sId As String In listDeletedRowsDescuentos
                        drToDelete = dtDescuentosLinea.Select("ID=" & sId)
                        dtDescuentosLinea.Rows.Remove(drToDelete(0))
                    Next

                    'Borramos de la tabla de albaranes
                    Dim dtDescuentosAlbaran As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(14)
                    For Each drAlbaran As DataRow In dtDescuentosAlbaran.Select("ALBARAN='" & sAlbaran & "' AND DEN='" & sDenDescuento & "'")
                        dtDescuentosAlbaran.Rows.Remove(drAlbaran)
                    Next
                End If

                RecalcularImpuestoLinea()
                CargarLineasFactura()
                upLineas.Update()
                RaiseEvent EventoRecalcularImportes(1, _ImporteTotalLineasConCD, Nothing, _ImporteTotalLineasConCDYImpuestos)
                CargarPanelDetalleDescuentos()
            Case "AplicarDescuentoRestoLineas"
                'Selecciono el Descuento que quiero copiar
                Dim drLineaCopiar() As DataRow = dtDescuentosLinea.Select("ID=" & idDescuento)
                Dim arrLineasActualizar As New List(Of String())

                Dim idNewDescuento As Short = hid_UltimoNuevoIdDescuento.Value
                'Recorro las lineas de factura
                For Each dr As DataRow In dtLineasFactura.Rows
                    'Si la linea es distinta que la linea del Descuento seleccionado la comprobare para ver si se le puede aplicar
                    If dr("LINEA") <> LineaSeleccionada Then
                        'Selecciono los Descuentos de esa linea de factura
                        Dim drLineasActualizar() As DataRow = dtDescuentosLinea.Select("LINEA=" & dr("LINEA"))
                        Dim bDescuentoExiste As Boolean
                        Dim itemLinea(5) As String
                        For Each drLineaActualizar As DataRow In drLineasActualizar
                            'Compruebo que el codigo de Descuento que quiero introducir en esa linea no este ya
                            If drLineaActualizar("DEN") = drLineaCopiar(0)("DEN") Then
                                bDescuentoExiste = True
                            End If
                        Next
                        If bDescuentoExiste = False Then
                            'Si no existe ese Descuento en la linea de factura lo añado a la lista de lineas al que añadir ese coste copiado
                            itemLinea(0) = dr("LINEA")
                            itemLinea(1) = dr("CANT_PED")
                            itemLinea(2) = dr("PREC_UP")
                            itemLinea(3) = dr("TOTAL_COSTES")
                            itemLinea(4) = dr("TOTAL_DCTOS")
                            itemLinea(5) = dr("ALBARAN")
                            arrLineasActualizar.Add(itemLinea)
                        End If
                        bDescuentoExiste = False
                    End If

                Next
                Dim newRow As DataRow
                For i = 0 To arrLineasActualizar.Count - 1
                    Dim item(5) As String
                    item = arrLineasActualizar(i)
                    newRow = dtDescuentosLinea.NewRow
                    newRow("ID") = idNewDescuento
                    newRow("LINEA") = item(0)
                    newRow("FACTURA") = drLineaCopiar(0)("FACTURA")
                    newRow("TIPO") = drLineaCopiar(0)("TIPO")
                    newRow("DEN") = drLineaCopiar(0)("DEN")
                    newRow("VALOR") = drLineaCopiar(0)("VALOR")

                    Dim numLineasMismoAlbaran As Integer = 0
                    Select Case drLineaCopiar(0)("OPERACION")
                        Case "-"
                            If arrLineasActualizar(i)(5) <> String.Empty Then
                                'HabrÃƒÂ¡ que dividir el valor entre las lÃƒÂ­neas que forman el albarÃƒÂ¡n
                                For j = 0 To arrLineasActualizar.Count - 1
                                    If arrLineasActualizar(j)(5) = arrLineasActualizar(i)(5) Then
                                        numLineasMismoAlbaran = numLineasMismoAlbaran + 1
                                    End If
                                Next
                                If numLineasMismoAlbaran > 0 Then _
                                    newRow("IMPORTE") = drLineaCopiar(0)("VALOR") / numLineasMismoAlbaran
                            Else
                                newRow("IMPORTE") = drLineaCopiar(0)("VALOR")
                            End If
                        Case "-%"
                            Dim importeDescuento As Double
                            importeDescuento = (DBNullToDbl(arrLineasActualizar(i)(2)) * DBNullToDbl(arrLineasActualizar(i)(1)))
                            importeDescuento = DBNullToDbl((importeDescuento * drLineaCopiar(0)("VALOR"))) / 100
                            newRow("IMPORTE") = importeDescuento
                    End Select
                    newRow("OPERACION") = drLineaCopiar(0)("OPERACION")
                    newRow("DEF_ATRIB_ID") = drLineaCopiar(0)("DEF_ATRIB_ID")
                    newRow("ALBARAN") = arrLineasActualizar(i)(5)
                    newRow("ACT_ALBARAN") = If(arrLineasActualizar(i)(5) <> String.Empty, 1, 0)
                    dtDescuentosLinea.Rows.Add(newRow)

                    'Ahora se graban los costes a nivel de Albaran
                    Dim dtDescuentosAlbaran As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(14)
                    Dim drDescuentosAlbaran() As DataRow = dtDescuentosAlbaran.Select("ALBARAN='" & arrLineasActualizar(i)(5) & "' AND DEN='" & drLineaCopiar(0)("DEN") & "'")
                    If drDescuentosAlbaran.Length = 0 Then
                        Dim newRowAlbaran As DataRow
                        newRowAlbaran = dtDescuentosAlbaran.NewRow
                        newRowAlbaran("ID") = idNewDescuento
                        newRowAlbaran("FACTURA") = m_sIdFactura
                        newRowAlbaran("ALBARAN") = arrLineasActualizar(i)(5)
                        newRowAlbaran("TIPO") = 1 '1 por ser descuento
                        newRowAlbaran("DEN") = drLineaCopiar(0)("DEN")
                        newRowAlbaran("DEF_ATRIB_ID") = drLineaCopiar(0)("DEF_ATRIB_ID")
                        newRowAlbaran("VALOR") = drLineaCopiar(0)("VALOR")
                        If drLineaCopiar(0)("OPERACION") = "+" Then
                            If numLineasMismoAlbaran > 0 Then _
                                    newRowAlbaran("IMPORTE") = drLineaCopiar(0)("VALOR") / numLineasMismoAlbaran
                        Else
                            newRowAlbaran("IMPORTE") = drLineaCopiar(0)("VALOR")
                        End If
                        newRowAlbaran("OPERACION") = drLineaCopiar(0)("OPERACION")
                        newRowAlbaran("PLANIFICADO") = 0 'No planificado
                        dtDescuentosAlbaran.Rows.Add(newRowAlbaran)
                    End If

                    idNewDescuento -= 1
                Next
                hid_UltimoNuevoIdDescuento.Value = idNewDescuento
                RecalcularImpuestoLinea()
                CargarLineasFactura()
                upLineas.Update()
                RaiseEvent EventoRecalcularImportes(1, _ImporteTotalLineasConCD, Nothing, _ImporteTotalLineasConCDYImpuestos)
                mpePanelDetalleDescuentos.Hide()
        End Select
    End Sub
    ''' <summary>
    ''' evento que salta por cada Descuento del repeater del detalle de Descuentos de cada linea
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub rptDescuentosLineaFactura_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDescuentosLineaFactura.ItemDataBound
        Static ImporteTotal As Double
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                DirectCast(e.Item.FindControl("hid_idDescuentoLinea"), HiddenField).Value = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("ID"))
                DirectCast(e.Item.FindControl("hid_Albaran"), HiddenField).Value = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("ALBARAN"))
                DirectCast(e.Item.FindControl("lblDenominacionDescuentoLinea"), Label).Text = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("DEN"))
                If DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("ALBARAN")) <> String.Empty Then
                    DirectCast(e.Item.FindControl("lblDescuentoPorLineaAlbaran"), Label).Text = oTextos(81).Item(1)
                Else
                    DirectCast(e.Item.FindControl("lblDescuentoPorLineaAlbaran"), Label).Text = oTextos(82).Item(1)
                End If

                Dim iPlanificado As Integer = DBNullToInteger(DirectCast(e.Item.DataItem, DataRow)("PLANIFICADO"))
                If m_bEditable AndAlso iPlanificado = 0 Then
                    DirectCast(e.Item.FindControl("lblPorcentaje"), Label).Visible = False
                    DirectCast(e.Item.FindControl("lblOperaciones"), Label).Visible = False
                    DirectCast(e.Item.FindControl("wnePorcentaje"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = DBNullToDbl(DirectCast(e.Item.DataItem, DataRow)("VALOR"))
                    DirectCast(e.Item.FindControl("ddlOperaciones"), DropDownList).Attributes.Add("onchange", "javascript: { if (document.getElementById('" & DirectCast(e.Item.FindControl("wnePorcentaje"), Infragistics.Web.UI.EditorControls.WebNumericEditor).ClientID & "').value == '0') return false;}")
                    Dim oListItem As ListItem = DirectCast(e.Item.FindControl("ddlOperaciones"), DropDownList).Items.FindByValue(DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("OPERACION")))
                    If Not oListItem Is Nothing Then
                        oListItem.Selected = True
                    End If
                    Dim dImporte As Double = DBNullToDbl(DirectCast(e.Item.DataItem, DataRow)("IMPORTE"))
                    DirectCast(e.Item.FindControl("lblImporteDescuentos"), Label).Text = FormatNumber(Replace(dImporte.ToString, ".", ","), m_oNumberFormat)
                    ImporteTotal += dImporte
                    If DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("ALBARAN")) = String.Empty Then
                        If DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("VALOR")) = String.Empty Then
                            DirectCast(e.Item.FindControl("imgAplicarDescuentoRestoLineas"), ImageButton).Visible = False
                        End If
                    End If
                    DirectCast(e.Item.FindControl("imgEliminarDescuentoLinea"), ImageButton).ImageUrl = _RutaImagenes & "eliminar.png"
                    DirectCast(e.Item.FindControl("imgAplicarDescuentoRestoLineas"), ImageButton).ImageUrl = _RutaImagenes & "Aplicar.jpg"
                    DirectCast(e.Item.FindControl("imgAplicarDescuentoRestoLineas"), ImageButton).ToolTip = oTextos(156).Item(1)
                Else
                    DirectCast(e.Item.FindControl("tdPorcentaje"), HtmlTableCell).Style("background-color") = "#EEEEEE"
                    DirectCast(e.Item.FindControl("lblPorcentaje"), Label).Text = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("VALOR"))
                    DirectCast(e.Item.FindControl("wnePorcentaje"), Infragistics.Web.UI.EditorControls.WebNumericEditor).Visible = False
                    DirectCast(e.Item.FindControl("tdOperaciones"), HtmlTableCell).Style("background-color") = "#EEEEEE"
                    DirectCast(e.Item.FindControl("ddlOperaciones"), DropDownList).Visible = False
                    DirectCast(e.Item.FindControl("lblOperaciones"), Label).Text = DBNullToStr(DirectCast(e.Item.DataItem, DataRow)("OPERACION"))
                    Dim dImporte As Double = DBNullToDbl(DirectCast(e.Item.DataItem, DataRow)("IMPORTE"))
                    DirectCast(e.Item.FindControl("lblImporteDescuentos"), Label).Text = FormatNumber(Replace(dImporte.ToString, ".", ","), m_oNumberFormat)
                    ImporteTotal += dImporte
                    DirectCast(e.Item.FindControl("CeldaEliminarDescuento"), HtmlTableCell).Visible = False
                    DirectCast(e.Item.FindControl("CeldaAplicarDescuento"), HtmlTableCell).Visible = False
                End If
            Case ListItemType.Footer
                DirectCast(e.Item.FindControl("lblTotalDescuentosFooter"), Label).Text = String.Format("{0}: ", oTextos(24).Item(1))
                DirectCast(e.Item.FindControl("lblTotalImporteDescuentosFooter"), Label).Text = FormatNumber(Replace(ImporteTotal.ToString, ".", ","), m_oNumberFormat) & " " & m_sMoneda
        End Select
    End Sub

    ''' <summary>
    ''' Procedimiento que retorna los descuentos encontrados en base a unos filtros y los asigana al grid del buscador de descuentos
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnBuscarDescuentos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarDescuentos.Click

        RaiseEvent EventoBuscarDescuentos(txtCodigo_BusqDescuentosLinea.Text, txtDescripcion_BusqDescuentosLinea.Text, hidArticuloDescuentos.Value, hidMaterialDescuentos.Value)

        If m_dsDescuentosBuscador.Tables(0).Rows.Count = 0 Then
            phDescuentoGenerico.Visible = True
            whdgBuscadorDescuentos.Visible = False
        Else
            phDescuentoGenerico.Visible = False
            whdgBuscadorDescuentos.Visible = True
            whdgBuscadorDescuentos.DataSource = m_dsDescuentosBuscador
            whdgBuscadorDescuentos.DataBind()
        End If
    End Sub
    ''' <summary>
    ''' procedimiento que desde el buscador de descuentos inserta un nuevo descuento a la linea de factura
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnAceptarDescuento_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarDescuento.Click
        Dim linea As String = hid_FacturaUpdating.Value 'Recojo la linea de factura a la que añadiria el Descuento
        Dim dtDescuentosLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(13)
        Dim dtLineas As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
        Dim idNewDescuento As Short = If(hid_UltimoNuevoIdDescuento.Value = "", -1, hid_UltimoNuevoIdDescuento.Value)


        'Seleccionamos los Descuentos de la linea si los tuviese
        Dim drDescuento() As DataRow = dtDescuentosLinea.Select("LINEA=" & linea)
        'Seleccionamos la linea de factura para obtener sus campos CANTIDAD, PRECIO...
        Dim drLineaFactura() As DataRow = dtLineas.Select("LINEA=" & linea)
        Dim filaSeleccionada As Infragistics.Web.UI.GridControls.GridRecord
        Dim sDenominacion As String = Nothing
        Dim lDefAtribId As Long = Nothing

        If ddlDescuentosFacturas.SelectedValue = String.Empty AndAlso phDescuentoGenerico.Visible = False Then
            'Deberia haber seleccionado el Descuento del grid
            If whdgBuscadorDescuentos.GridView.Behaviors.Selection.SelectedRows.Count > 0 Then
                filaSeleccionada = whdgBuscadorDescuentos.GridView.Behaviors.Selection.SelectedRows(0)
                sDenominacion = DBNullToStr(filaSeleccionada.Items.FindItemByKey("DEN").Value)
                lDefAtribId = DBNullToInteger(filaSeleccionada.Items.FindItemByKey("ID").Value)
            Else
                'si acepta y no ha seleccionado ni del combo...ni del grid, cerramos el buscador
                mpePanelBuscadorDescuentos.Hide()
            End If
        ElseIf phDescuentoGenerico.Visible = True Then
            'Deberia haber escrito un Descuento generico
            If txtDescuentoGenerico.Text <> String.Empty Then
                lDefAtribId = m_lIdDescuentoGenerico
                sDenominacion = txtDescuentoGenerico.Text
            Else
                mpePanelBuscadorDescuentos.Hide()
            End If
        Else
            'Deberia haber seleccionado el Descuento del combo de ultimos Descuentos utilizados en facturas
            sDenominacion = ddlDescuentosFacturas.SelectedItem.Text
            lDefAtribId = ddlDescuentosFacturas.SelectedValue
        End If

        Dim sAlbaran As String = DBNullToStr(drLineaFactura(0)("ALBARAN"))

        If rbDescuentoLinea.Visible AndAlso rbDescuentoLinea.Checked Then
            Dim drValidacionLinea() As DataRow = dtDescuentosLinea.Select("LINEA=" & linea & " AND DEF_ATRIB_ID=" & lDefAtribId)
            If drValidacionLinea.Count > 0 Then
                'No se puede añadir el mismo descuento a una linea a la que ya se le ha añadido
                mpePanelBuscadorDescuentos.Hide()
                CargarPanelDetalleDescuentos()
                Exit Sub
            End If

            'Inserto el Descuento a nivel de linea en la tabla de Descuentos de linea
            Dim newRow As DataRow
            newRow = dtDescuentosLinea.NewRow
            newRow("ID") = idNewDescuento
            newRow("FACTURA") = m_sIdFactura
            newRow("LINEA") = linea
            newRow("TIPO") = 1 '1 por ser Descuento, 0 si es coste
            newRow("DEN") = sDenominacion
            If sAlbaran <> String.Empty Then
                newRow("ALBARAN") = sAlbaran
            End If
            newRow("DEF_ATRIB_ID") = lDefAtribId
            dtDescuentosLinea.Rows.Add(newRow)

            RecalcularImpuestoLinea()
            CargarLineasFactura()
            upLineas.Update()

            CargarPanelDetalleDescuentos()
            upDetalleDescuentosLinea.Update()
            mpePanelBuscadorDescuentos.Hide()
        Else

            Dim drValidacionAlbaran() As DataRow = dtDescuentosLinea.Select("LINEA=" & linea & " AND DEF_ATRIB_ID=" & lDefAtribId)
            If drValidacionAlbaran.Count > 0 Then
                'No se puede añadir el mismo descuento a un albaran al que ya se le ha añadido
                'PENDIENTE
                mpePanelBuscadorDescuentos.Hide()
                CargarPanelDetalleDescuentos()
                Exit Sub
            End If
            'Descuento a nivel de Albaran, se graban los Descuentos en cada una de las lineas de factura que tengan ese Albaran, el importe
            'se actualizada cuando desde la pantalla de detalle una vez introducido el usuario introduzca el valor del Descuento
            If sAlbaran <> String.Empty Then
                Dim drLineasAlbaran() As DataRow = dtLineas.Select("ALBARAN='" & sAlbaran & "'")
                For Each drLineaAlbaran As DataRow In drLineasAlbaran
                    Dim newRow As DataRow
                    newRow = dtDescuentosLinea.NewRow
                    newRow("ID") = idNewDescuento
                    newRow("FACTURA") = m_sIdFactura
                    newRow("LINEA") = drLineaAlbaran("LINEA")
                    newRow("TIPO") = 1
                    newRow("DEN") = sDenominacion
                    newRow("ACT_ALBARAN") = 1 'Le indicamos que cuando en el detalle indique el valor sabra que filas tiene que actualizar
                    newRow("ALBARAN") = sAlbaran
                    newRow("DEF_ATRIB_ID") = lDefAtribId
                    dtDescuentosLinea.Rows.Add(newRow)
                    idNewDescuento -= 1
                Next
            End If

            'Ahora se graban los Descuentos a nivel de Albaran
            Dim dtDescuentosAlbaran As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(14)
            Dim newRowAlbaran As DataRow
            newRowAlbaran = dtDescuentosAlbaran.NewRow
            newRowAlbaran("ID") = idNewDescuento
            newRowAlbaran("FACTURA") = m_sIdFactura
            newRowAlbaran("ALBARAN") = sAlbaran
            newRowAlbaran("TIPO") = 1 '1 por ser Descuento, 0 si es coste
            newRowAlbaran("DEN") = sDenominacion
            newRowAlbaran("DEF_ATRIB_ID") = lDefAtribId
            newRowAlbaran("PLANIFICADO") = 0 'No planificado
            dtDescuentosAlbaran.Rows.Add(newRowAlbaran)

            RecalcularImpuestoLinea()
            CargarLineasFactura()
            upLineas.Update()

            CargarPanelDetalleDescuentos()
            upDetalleDescuentosLinea.Update()
            mpePanelBuscadorDescuentos.Hide()
        End If
        hid_UltimoNuevoIdDescuento.Value = idNewDescuento - 1

    End Sub

    ''' <summary>
    ''' Procedimiento que limpia los campos del buscador de descuentos
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnLimpiarDescuentos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarDescuentos.Click
        txtCodigo_BusqDescuentosLinea.Text = ""
        hidArticuloDescuentos.Value = ""
        txtMaterial_BusqDescuentosLinea.Text = ""
        hidMaterialDescuentos.Value = ""
        txtCodigo_BusqDescuentosLinea.Text = ""
        txtDescripcion_BusqDescuentosLinea.Text = ""
        upBuscadorInternoDescuentos.Update()
    End Sub
#End Region
    Private Sub rpCostesAlbaran_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpCostesAlbaran.ItemDataBound
        Dim lbl As Label
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                lbl = DirectCast(e.Item.FindControl("lblValor"), Label)
                lbl.Text = FormatNumber(Replace(e.Item.DataItem("VALOR"), ".", ","), m_oNumberFormat) & " %"
                lbl = DirectCast(e.Item.FindControl("lblImporte"), Label)
                lbl.Text = FormatNumber(Replace(e.Item.DataItem("IMPORTE"), ".", ","), m_oNumberFormat) & " " & m_sMoneda
                m_dTotalImpuestosRepercutidos = m_dTotalImpuestosRepercutidos + e.Item.DataItem("IMPORTE")
            Case ListItemType.Footer
                DirectCast(e.Item.Controls(0).FindControl("lblTotal"), Label).Text = String.Format("{0}: ", oTextos(76).Item(1)) & FormatNumber(Replace(m_dTotalImpuestosRepercutidos, ".", ","), m_oNumberFormat) & " " & m_sMoneda
        End Select
    End Sub
    Private Sub rpDescuentosAlbaran_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpDescuentosAlbaran.ItemDataBound
        Dim lbl As Label
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                lbl = DirectCast(e.Item.FindControl("lblValor"), Label)
                lbl.Text = FormatNumber(Replace(e.Item.DataItem("VALOR"), ".", ","), m_oNumberFormat) & " %"
                lbl = DirectCast(e.Item.FindControl("lblImporte"), Label)
                lbl.Text = FormatNumber(Replace(e.Item.DataItem("IMPORTE"), ".", ","), m_oNumberFormat) & " " & m_sMoneda
                m_dTotalImpuestosRepercutidos = m_dTotalImpuestosRepercutidos + e.Item.DataItem("IMPORTE")
            Case ListItemType.Footer
                DirectCast(e.Item.Controls(0).FindControl("lblTotal"), Label).Text = String.Format("{0}: ", oTextos(76).Item(1)) & FormatNumber(Replace(m_dTotalImpuestosRepercutidos, ".", ","), m_oNumberFormat) & " " & m_sMoneda
        End Select
    End Sub
    ''' <summary>
    ''' Elimina las líneas (con sus costes, descuentos e impuestos) del albarán seleccionado
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>Llamada desde: Al hacer click en el enlace de eliminar el albarán.</remarks>
    Private Sub lblLitEliminarAlbaran_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblLitEliminarAlbaran.Click
        Dim sAlbaran As String = hAlbaranAEliminar.Value
        Dim dtLineas As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
        Dim dtImpuestosLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(10)
        Dim dtCostesLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(11)
        Dim dtCostesAlbaran As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(12)
        Dim dtDescuentosLinea As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(13)
        Dim dtDescuentosAlbaran As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(14)

        For Each drCosteAlbaran As DataRow In dtCostesAlbaran.Select("ALBARAN='" & sAlbaran & "'")
            dtCostesLinea.Rows.Remove(drCosteAlbaran)
        Next

        For Each drDescuentoAlbaran As DataRow In dtDescuentosAlbaran.Select("ALBARAN='" & sAlbaran & "'")
            dtDescuentosAlbaran.Rows.Remove(drDescuentoAlbaran)
        Next

        For Each drLinea As DataRow In dtLineas.Select("ALBARAN='" & sAlbaran & "'")
            hid_LineasEliminadas.Value = If(Len(hid_LineasEliminadas.Value) > 0, hid_LineasEliminadas.Value & ",", "") & drLinea("LINEA")

            Dim drImpuestosLinea() As DataRow = dtImpuestosLinea.Select("LINEA=" & drLinea("LINEA"))
            Dim drCostesLinea() As DataRow = dtCostesLinea.Select("LINEA=" & drLinea("LINEA"))
            Dim drDescuentosLinea() As DataRow = dtDescuentosLinea.Select("LINEA=" & drLinea("LINEA"))

            For Each drCoste As DataRow In drCostesLinea
                dtCostesLinea.Rows.Remove(drCoste)
            Next

            For Each drDescuento As DataRow In drDescuentosLinea
                dtDescuentosLinea.Rows.Remove(drDescuento)
            Next

            For Each drImpuesto As DataRow In drImpuestosLinea
                ActualizarResumenImpuestos(drImpuesto("RETENIDO"), drImpuesto, True)
                dtImpuestosLinea.Rows.Remove(drImpuesto)
            Next

            dtLineas.Rows.Remove(drLinea)
        Next

        Session("LineasEliminadas") = Session("LineasEliminadas") & If(Len(Session("LineasEliminadas")) > 0, ",", "") & hid_LineasEliminadas.Value

        Dim ds As New DataSet
        ds.Tables.Add(dtLineas.Copy)
        whdgLineas.Rows.Clear()
        whdgLineas.DataSource = ds
        whdgLineas.DataBind()
        upLineas.Update()

        RecalcularImporteBrutoFactura()
        RaiseEvent EventoRecalcularImportes(3, 0, Nothing, 0)
        RecalcularImpuestosCostesDescuentosGenerales()

        Dim dt As DataTable = RecalcularImpuestosCabecera()
        RaiseEvent EventoRecalcularImportes(0, 0, dt, 0)
        RaiseEvent EventoRecalcularImportes(2, _ImporteTotalLineasConCD, Nothing, _ImporteTotalLineasConCDYImpuestos)
        mpePanelDetalleAlbaran.Hide()

    End Sub

    ''' <summary>
    ''' Recalcula el importe bruto de la factura cuando se han eliminado líneas.
    ''' </summary>
    ''' <remarks>Llamada desde: lblLitEliminarAlbaran_Click</remarks>
    Private Sub RecalcularImporteBrutoFactura()
        Dim dNuevoImporteBruto As Double
        Dim dtLineas As DataTable = CType(Cache("dsFactura" & m_sSufijoCache), DataSet).Tables(6)
        For Each drLinea As DataRow In dtLineas.Rows
            dNuevoImporteBruto = dNuevoImporteBruto + (drLinea("CANT_PED") * drLinea("PREC_UP"))
        Next
        Dim oDTFactura As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable)
        oDTFactura.Rows(0).Item("IMPORTE_BRUTO") = dNuevoImporteBruto
        oDTFactura.AcceptChanges()
    End Sub
End Class