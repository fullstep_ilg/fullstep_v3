﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucBusqueda_Empresas.ascx.vb" Inherits="FSNUserControls.wucBusqueda_Empresas" %>

<script language="javascript" type="text/javascript">
    var SelectedRow;
    function ValueChanged_wddPaises_CambioPais(elem, event) {
        comboProvincias = $find("<%= wddProvincias.clientID %>")
        if (comboProvincias)
            comboProvincias.set_currentValue("", true)
    }

    function InputKeyDown_wwdProvincias_Inicializar(elem, event) {
        comboProvincias = $find("<%= wddProvincias.clientID %>")
        if (comboProvincias)
            comboProvincias.set_currentValue("",true)
    }

    /*Descripcion:= Funcion que nos devuelve los valores de la empresa seleccionado a la pagina origen.
    Llamada desde:= Evento que salta al pulsar en el boton Aceptar*/
    function aceptar() {
        p = window.opener;        
        if (p) {
            if (!SelectedRow) return;

            sID = SelectedRow.get_cellByColumnKey("ID").get_value();
            sDen = SelectedRow.get_cellByColumnKey("DEN").get_value();
            sNif = SelectedRow.get_cellByColumnKey("NIF").get_value();
           
            switch (wOpener) {
                case 'desdeSolicitud':
                    p.SeleccionarEmpresaSolicitud($('[id$=hidIdControl]').val(), sID, sDen, sNif);
                    break;
                case 'visorSolicitudes':
                    p = window.opener;
                    p.document.getElementById(IDCONTROLHID).value = sID;
                    p.document.getElementById(IDCONTROL).value = sNif + '-' + sDen;
                    break;
                default:
                    p.SeleccionarEmpresa(sID, sDen, sNif); 
                    break;
            }                         
        }
        window.close();
    }

    function wdg_Empresas_RowSelectionChanged(grid, args) {
        SelectedRow = grid.get_behaviors().get_selection().get_selectedRows().getItem(0);
    }

    /*Revisado por: blp. Fecha: 30/01/2012
    <summary>
    Método necesario para ordenar las columnas del grid
    </summary>
    <remarks>Llamada desde: evento ColumnSorting del grid. Máx. 0,1 seg.</remarks>*/
    function wdg_Empresas_Sorting_ColumnSorting(grid, args) {
        var sorting = grid.get_behaviors().get_sorting();
        var col = args.get_column();
        var sortedCols = sorting.get_sortedColumns();
        for (var x = 0; x < sortedCols.length; ++x) {
            if (col.get_key() == sortedCols[x].get_key()) {
                args.set_clear(false);
                break;
            }
        }
    }    
</script>
<table width="100%" cellpadding="0" border="0">
    <tr>
        <td align="left">
            <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
        </td>       
    </tr>
</table>
<asp:HiddenField runat="server" ID="hidIdControl" />    
            
<table cellpadding="0" cellspacing="0" style="margin-left:5px;margin-right:5px; border-spacing:2px;" border="0" width="99%">
	<tr>
	    <td>
			<table id="tblBusqueda" border="0" width="100%">
				<tr><td><asp:label id="lblNIF" runat="server" CssClass="Etiqueta"></asp:label></td>
				    <td><asp:label id="lblDen" runat="server" CssClass="Etiqueta"></asp:label></td>
				</tr>
				<tr>
					<td><asp:TextBox ID="txtNIF" runat="server" Width="200px"></asp:TextBox></td>
					<td><asp:TextBox ID="txtDen" runat="server" Width="400px"></asp:TextBox></td>
				</tr>
				<tr>
					<td><asp:label id="lblCP" runat="server" CssClass="Etiqueta"></asp:label></td>
					<td><asp:label id="lblDir" runat="server" CssClass="Etiqueta"></asp:label></td>
				</tr>
				<tr>
					<td><asp:TextBox ID="txtCP" runat="server" Width="200px" MaxLength="5"></asp:TextBox></td>
					<td><asp:TextBox ID="txtDir" runat="server" Width="400px"></asp:TextBox></td>
				</tr>
				<tr>
					<td><asp:label id="lblPais" runat="server" CssClass="Etiqueta"></asp:label></td>
					<td><asp:label id="lblProv" runat="server" CssClass="Etiqueta"></asp:label></td>
				</tr>				
				<tr>
					<td style="width:200px;padding-top:5px;">
					    <div style="background-color:White;width:200px">
                            <ig:WebDropDown ID="wddPaises" runat="server" Width="200px" EnableClosingDropDownOnSelect="true" DropDownAnimationDuration="1" DropDownContainerWidth="200"  EnableDropDownAsChild="false">                    
                                <ClientEvents ValueChanged="ValueChanged_wddPaises_CambioPais" />
                            </ig:WebDropDown>
                        </div>
                    </td>
					<td style="width:400px;padding-top:5px;">
					<table width="100%">
					    <tr>
					        <td>
					                <div style="background-color:White;width:400px">
                                        <ig:WebDropDown ID="wddProvincias" runat="server" Width="400px" TextField="Text" ValueField="Value"
                                                            EnableAutoCompleteFirstMatch="false" DropDownContainerWidth="400px" DropDownContainerHeight="200px" EnableMultipleSelection="false" EnableDropDownAsChild="false"  EnableClosingDropDownOnSelect="true">
                                             <ClientEvents InputKeyDown="InputKeyDown_wwdProvincias_Inicializar" />
                                             
                                        </ig:WebDropDown>
                                    </div>
					        </td>
					        <td>
					            <fsn:FSNButton ID="btnBuscar" runat="server" Text="Buscar"></fsn:FSNButton>
					        </td>
					    </tr>
					</table>
                   </td>  
				</tr>				
			</table>
		</td>
	</tr>
</table>				
				
<!--Grid Empresas -->
<asp:UpdatePanel ID="UpdatePanelEmpresas" runat="server" UpdateMode="conditional">
	<ContentTemplate>
		<table border="0" width="100%" style="margin-left:5px;margin-right:5px;" cellspacing="0">
		    <tr>
			    <td colspan="3" style="height: 5px">
			    </td>
		    </tr>
		    <tr>
			    <td colspan="3" style="text-align: left;background:#868686">
				    <asp:label id="lblEmpresasEncontradas" runat="server" CssClass="Normal"></asp:label>
			    </td>
		    </tr>
		    <tr>
			    <td colspan="3" rowspan="2">
			        <div id="divArt" style="padding-top:5px">
				        <ig:WebHierarchicalDataGrid id="wdg_Empresas" runat="server"   AutoGenerateColumns="false" Width="100%" Height="270px">
                            <Columns>
                                <ig:BoundDataField Key="ID" DataFieldName="ID" Hidden="true"></ig:BoundDataField>
                                <ig:BoundDataField Key="NIF" DataFieldName="NIF" CssClass ="Mano" Width="10%"></ig:BoundDataField>
                                <ig:BoundDataField Key="DEN" DataFieldName="DEN" CssClass ="Mano"  Width="35%"></ig:BoundDataField>
                                <ig:BoundDataField Key="DIR" DataFieldName="DIR" CssClass ="Mano" Width="15%"></ig:BoundDataField>
                                <ig:BoundDataField Key="CP" DataFieldName="CP" CssClass ="ManoBold" Width="10%"></ig:BoundDataField>
                                <ig:BoundDataField Key="PAI_DEN" DataFieldName="PAI_DEN" CssClass ="ManoBold" Width="15%"></ig:BoundDataField>
                                <ig:BoundDataField Key="PROVI_DEN" DataFieldName="PROVI_DEN" CssClass ="ManoBold" Width="15%" ></ig:BoundDataField>
                            </Columns>
                            <Behaviors>
                                <ig:Selection CellClickAction="Row" RowSelectType="Single" Enabled="true">
                                    <SelectionClientEvents RowSelectionChanged="wdg_Empresas_RowSelectionChanged" />
                                </ig:Selection>
                                <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true"></ig:Filtering>
                                <ig:Sorting Enabled="true" SortingMode="Multi" >
                                    <SortingClientEvents ColumnSorting="wdg_Empresas_Sorting_ColumnSorting" />                                        
                                </ig:Sorting>
                            </Behaviors>
                            
                        </ig:WebHierarchicalDataGrid>
			        </div>							
			    </td>
		    </tr>		
		    <tr style="height:20px" >
			    <td colspan="3">
				    &nbsp;
			    </td>
		    </tr>
		    <tr>
		        <td colspan="3">
			        <table width="100%">
                        <tr>
			                <td align="right" width="50%">
					                <fsn:FSNButton ID="btnAceptar" runat="server" Text="Aceptar" Alineacion="Right" OnClientClick="aceptar();return false;"></fsn:FSNButton>
			                </td>
			                <td align="left" width="50%">		
					                <fsn:FSNButton ID="btnCancelar" runat="server" Text="Cancelar" Alineacion="Left" OnClientClick="window.close();return false;"></fsn:FSNButton>
			                </td>
			            </tr>
			        </table>
		        </td>
	        </tr>
	    </table>
	</ContentTemplate>
	<Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnBuscar" />
    </Triggers>
</asp:UpdatePanel>			
	
<!-- Alertas de validación -->
<asp:Button ID="btnAlert" runat="server" Style="display: none" />
<ajx:ModalPopupExtender ID="mpeAlert" runat="server" Enabled="True" PopupControlID="panAlert" DropShadow="true" TargetControlID="btnAlert" OkControlID="btnAceptarAlert" BehaviorID="Alert" BackgroundCssClass="modalBackground" OnOkScript="popUpShowed = null"/>
<asp:Panel ID="panAlert" runat="server" Style="display: none" Width="370px" Height="100px" CssClass="modalPopup">
    <table>
        <tr><td height="5px" colspan="3"></td></tr>
        <tr><td width="5px"></td><td><asp:Image ID="Image2" runat="server" SkinId="ErrorGrande" /></td><td><div id="divAlertContent"></div></td></tr>
        <tr>
            <td></td>
            <td colspan="2" align="center">
                <table>
                    <tr><td><fsn:FSNButton ID="btnAceptarAlert" runat="server" Text="OK" Alineacion="Left"></fsn:FSNButton></td></tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
        
<!-- Cargando -->
<asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
	<div style="position: relative; top: 30%; text-align: center;">
		<asp:Image ID="ImgProgress" runat="server" SkinId="ImgProgress" />
        <asp:Label ID="LblProcesando" runat="server" Text="..."></asp:Label>	
	</div>	
</asp:Panel>
<ajx:ModalPopupExtender ID="ModalProgress" runat="server" 
    TargetControlID="panelUpdateProgress"
	BackgroundCssClass="modalBackground" 
    PopupControlID="panelUpdateProgress" />