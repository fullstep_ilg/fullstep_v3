﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucUsuarios.ascx.vb" Inherits="FSNUserControls.wucUsuarios" %>
<script type="text/javascript" language="javascript">
    var SelectedRow;

    function wdg_Usuarios_RowSelectionChanged(grid, args) {
        SelectedRow = grid.get_behaviors().get_selection().get_selectedRows().getItem(0);
    }

    /*<%--Revisado por: blp. Fecha: 30/01/2012
    <summary>
    Método necesario para ordenar las columnas del grid
    </summary>
    <remarks>Llamada desde: evento ColumnSorting del grid. Máx. 0,1 seg.</remarks>
    --%>*/
    function wdg_Usuarios_Sorting_ColumnSorting(grid, args) {
        var sorting = grid.get_behaviors().get_sorting();
        var col = args.get_column();
        var sortedCols = sorting.get_sortedColumns();
        for (var x = 0; x < sortedCols.length; ++x) {
            if (col.get_key() == sortedCols[x].get_key()) {
                args.set_clear(false);
                break;
            }
        }
    }

    // The client event ‘RowSelectionChanged’ takes two parameters sender and e
    // sender  is the object which is raising the event
    // e is the RowSelectionChangedEventArgs

    function WebDataGrid_RowSelectionChanged(sender, e) {

        //Gets the selected rows collection of the WebDataGrid
        var selectedRows = e.getSelectedRows();
        //Gets the row that is selected from the selected rows collection
        var row = selectedRows.getItem(0);
        //Gets the second cell object in the row
        //In this case it is ProductName cell 
        var cell;
        cell = row.get_cell(3);

        //Gets reference to the WebDropDown
        var dropdown = null;
        dropdown = $find("<%= ugtxtUO_.clientID %>");

        //Gets the text in the cell
        var text = cell.get_text();
        if (dropdown != null) {
            //Sets the text of the value display to the product name of the selected row
            dropdown.set_currentValue(text, true);
            dropdown.closeDropDown()
            dropdown = null;
        }
    }


    function wdg_Usuarios_RowSelectionChanged(grid, args) {
        SelectedRow = grid.get_behaviors().get_selection().get_selectedRows().getItem(0);

        p = window.opener
        if (p) {
            if (!SelectedRow)
                return

            var sCod = SelectedRow.get_cellByColumnKey("COD").get_value()
            var sNom = SelectedRow.get_cellByColumnKey("NOM").get_value()
            var sApe = SelectedRow.get_cellByColumnKey("APE").get_value()
            var sEmail = SelectedRow.get_cellByColumnKey("EMAIL").get_value()

            window.opener.usu_seleccionado(sCod, sNom + ' ' + sApe, sEmail)
            window.close();
        }
    }

    function selectedIndexChanged(sender, e) {
        var newSelectedItem = e.getNewSelection()[0];
        var oldSelectedItem = e.getOldSelection()[0];
        if (newSelectedItem != null) {
            newSelectedItem._element.className = 'igdd_FullstepListItem igdd_FullstepListItemB igdd_FullstepListItemSelected';
            newSelectedItem = null;
        }
        if (oldSelectedItem != null) {
            oldSelectedItem._element.className = 'igdd_FullstepListItem igdd_FullstepListItemB';
            oldSelectedItem = null;
        }
    }
</script>

<asp:Panel ID="pnlTitulo" runat="server" Width="100%">
            <table width="100%" cellpadding="0" border="0">
                <tr>
                    <td>
                        <fsn:FSNPageHeader ID="FSNPageHeader" runat="server">
                        </fsn:FSNPageHeader>
                    </td>
                </tr>
            </table>
        </asp:Panel>

 <table id="Table1" style="Z-INDEX: 106;  WIDTH: 391px; HEIGHT: 64px"
				cellspacing="0" cellpadding="2" width="391" align="center" border="0">
				<TR>
                    
                            <td nowrap="nowrap">
						        <asp:Label id="lblUO" runat="server" CssClass="Etiqueta" Width="100%"></asp:Label>
                            </td>
					        <td>
                                <ig:WebDropDown ID="ugtxtUO_" 
                                runat="server" Width="200px" EnableClosingDropDownOnBlur="true" EnableClosingDropDownOnSelect="true" 
                                DropDownContainerWidth="345">
                                    <Items>
                                        <ig:DropDownItem>
                                        </ig:DropDownItem>
                                    </Items>
                                    <ItemTemplate>
                                        <ig:WebDataGrid ID="ugtxtUO_wdg" runat="server"
                                            AutoGenerateColumns="false" Width="100%" ShowHeader="false" EnableAjaxViewState="true" EnableDataViewState="true" OnRowSelectionChanged="ugtxtUO_wdg_RowSelectionChanged"  EnableAjax="False">
                                            <Columns>
                                                <ig:BoundDataField DataFieldName="COD_UON1" Key="COD_UON1" Width="0%">
                                                </ig:BoundDataField>
                                                <ig:BoundDataField DataFieldName="COD_UON2" Key="COD_UON2" Width="0%">
                                                </ig:BoundDataField>
                                                <ig:BoundDataField DataFieldName="COD_UON3" Key="COD_UON3" Width="0%">
                                                </ig:BoundDataField>
                                                <ig:BoundDataField DataFieldName="DEN" Key="DEN" Width="100%">
                                                </ig:BoundDataField>
                                            </Columns>
                                            <Behaviors>
                                                <ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Row">
                                                    <SelectionClientEvents RowSelectionChanged="WebDataGrid_RowSelectionChanged" />
                                                    <AutoPostBackFlags RowSelectionChanged="true" />
                                                </ig:Selection>
                                                <ig:RowSelectors Enabled="false" />
                                            </Behaviors>
                                        </ig:WebDataGrid>
                                    </ItemTemplate>
                                    <ClientEvents />
                                </ig:WebDropDown>
					        </TD>
                            <td>
                                <asp:Button runat="server" ID="cmdBuscar" CssClass="boton" />
                            </td>
                        
					
				</TR>
				<TR>
					<TD nowrap="nowrap">
						<asp:Label id="lblDep" runat="server" CssClass="Etiqueta" Width="100%"></asp:Label></TD>
					<TD>
                        <asp:UpdatePanel ID="UP_Uon" runat="server" UpdateMode="Conditional">
                            <Triggers >
                                <asp:AsyncPostBackTrigger ControlID="ugtxtUO_" EventName="SelectionChanged" />
                            </Triggers>
                            <ContentTemplate>
						        <ig:WebDropDown ID="ugtxtDEP" runat="server" Width="200px" EnableClosingDropDownOnSelect="true"
						        DropDownContainerHeight="200px" DropDownContainerWidth="" DropDownAnimationType="EaseOut" 
						        EnablePaging="False" PageSize="12">
							        <ClientEvents SelectionChanged="selectedIndexChanged" />
						        </ig:WebDropDown>
                            </ContentTemplate>
                        </asp:UpdatePanel>
					</TD>
                    
				</TR>
				<TR>
					<TD nowrap="nowrap">
						<asp:Label id="lblNombre" runat="server" CssClass="Etiqueta" Width="100%"></asp:Label></TD>
					<TD>
						<asp:TextBox ID="txtNombre" runat="server" Width="160px"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD nowrap="nowrap">
						<asp:Label id="lblApe" runat="server" Width="100%" CssClass="Etiqueta"></asp:Label></TD>
					<TD>
						<asp:TextBox ID="txtApe" runat="server" Width="160px"></asp:TextBox></TD>
				</TR>
			</TABLE>
            <asp:Label id="lblResult" style="Z-INDEX: 101; "
				runat="server" CssClass="Normal" Width="528px">Resultado de la búsqueda:</asp:Label>
			
			<HR style="Z-INDEX: 102; WIDTH: 100%; ; HEIGHT: 4px"
				color="gainsboro" noShade SIZE="4">
			<asp:Label id="lblPulsar" style="Z-INDEX: 103;"
				runat="server" CssClass="Normal" Width="672px">Pulse sobre el proveedor para seleccionarlo o haga una nueva búsqueda</asp:Label>
			
        <div id="divContenedor" style="width:100%; height:auto">
        <asp:UpdatePanel ID="UP_uwgUsuarios" runat="server" UpdateMode="Conditional" >
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="cmdBuscar" EventName="Click" />
        </Triggers>
        <ContentTemplate >
            <ig:WebHierarchicalDataGrid id="wdg_Usuarios" runat="server" AutoGenerateColumns="false" Width="100%" Height="340px">
                 <Columns>
                    <ig:BoundDataField Key="COD" DataFieldName="COD" Hidden="true"></ig:BoundDataField>
                    <ig:BoundDataField Key="NOM" DataFieldName="NOM" CssClass ="Mano" Hidden="true" ></ig:BoundDataField>
                    <ig:BoundDataField Key="APE" DataFieldName="APE" CssClass ="Mano" Hidden="true"  ></ig:BoundDataField>
                    <ig:BoundDataField Key="NOMBRE" DataFieldName="NOMBRE" CssClass ="Mano" Hidden="true" ></ig:BoundDataField>
                    <ig:BoundDataField Key="UON1" DataFieldName="UON1" CssClass ="ManoBold" Hidden="true" ></ig:BoundDataField>
                    <ig:BoundDataField Key="UON2" DataFieldName="UON2" CssClass ="ManoBold" Hidden="true" ></ig:BoundDataField>
                    <ig:BoundDataField Key="UON3" DataFieldName="UON3" CssClass ="ManoBold" Hidden="true" ></ig:BoundDataField>
                    <ig:BoundDataField Key="DEP" DataFieldName="DEP" CssClass ="ManoBold" Hidden="true" ></ig:BoundDataField>
                    <ig:BoundDataField Key="NOM_UO" DataFieldName="NOM_UO" CssClass ="ManoBold" Hidden="true" ></ig:BoundDataField>
                    <ig:BoundDataField Key="SOL_COMPRA" DataFieldName="SOL_COMPRA" CssClass ="ManoBold" Hidden="true" ></ig:BoundDataField>
                    <ig:BoundDataField Key="CAR" DataFieldName="CAR" CssClass ="ManoBold" Hidden="true" ></ig:BoundDataField>
                    <ig:BoundDataField Key="TFNO" DataFieldName="TFNO" CssClass ="ManoBold" Hidden="true" ></ig:BoundDataField>
                    <ig:BoundDataField Key="FAX" DataFieldName="FAX" CssClass ="ManoBold" Hidden="true" ></ig:BoundDataField>
                    <ig:BoundDataField Key="EMAIL" DataFieldName="EMAIL" CssClass ="ManoBold" Hidden="true" ></ig:BoundDataField>
                    <ig:BoundDataField Key="USUCOD" DataFieldName="USUCOD" CssClass ="ManoBold" Hidden="true" ></ig:BoundDataField>
                    <ig:BoundDataField Key="NOM_DEP" DataFieldName="NOM_DEP" CssClass ="ManoBold" ></ig:BoundDataField>
                    <ig:UnboundField  Key="USUARIO" CssClass ="ManoBold"></ig:UnboundField>
                </Columns>
                <Behaviors>
                    <ig:Selection CellClickAction="Row" RowSelectType="Single" Enabled="true">
                        <SelectionClientEvents RowSelectionChanged="wdg_Usuarios_RowSelectionChanged" />
                    </ig:Selection>
                    <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true"></ig:Filtering>
                    <ig:Sorting Enabled="true" SortingMode="Multi" >
                        <SortingClientEvents ColumnSorting="wdg_Usuarios_Sorting_ColumnSorting" />                                        
                    </ig:Sorting>
                </Behaviors>
                            
            </ig:WebHierarchicalDataGrid>
        </ContentTemplate>
        </asp:UpdatePanel>
            
        </div>