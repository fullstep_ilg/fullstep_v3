﻿Imports Infragistics.Web.UI
Imports Fullstep.FSNLibrary

Public Class wucBusquedaAvanzadaFacturas
    Inherits System.Web.UI.UserControl

    Private oTextos As DataTable
    Private _dtEstadosFacturas As DataTable
    Private _dtPartidas As DataTable

    Private _RutaBuscadorCentroCoste As String = String.Empty
    Private _RutaBuscadorArticulo As String = String.Empty
    Private _PropiedadesBuscadorArticulo As String = String.Empty
    Private _RutaBuscadorPartidas As String = String.Empty
    Private _RutaBuscadorGestor As String = String.Empty
    Private _RutaWsAutoComplete As String = String.Empty
    Private _AccesoFSSM As Boolean
    Private _OblCodPedDir As Boolean
    Private _Partidas As String
    Private mlFiltro As Long = 0

#Region " PROPIEDADES "
    Public Property AccesoFSSM As Boolean
        Get
            Return _AccesoFSSM
        End Get
        Set(ByVal value As Boolean)
            _AccesoFSSM = value
        End Set
    End Property

    Public Property OblCodPedDir As Boolean
        Get
            Return _OblCodPedDir
        End Get
        Set(ByVal value As Boolean)
            _OblCodPedDir = value
        End Set
    End Property

    Public ReadOnly Property Partidas As DataTable
        Get
            Dim dtPartidasPress As New DataTable
            dtPartidasPress.Columns.Add("UON1", GetType(System.String))
            dtPartidasPress.Columns.Add("UON2", GetType(System.String))
            dtPartidasPress.Columns.Add("UON3", GetType(System.String))
            dtPartidasPress.Columns.Add("UON4", GetType(System.String))
            dtPartidasPress.Columns.Add("PRES0", GetType(System.String))
            dtPartidasPress.Columns.Add("PRES1", GetType(System.String))
            dtPartidasPress.Columns.Add("PRES2", GetType(System.String))
            dtPartidasPress.Columns.Add("PRES3", GetType(System.String))
            dtPartidasPress.Columns.Add("PRES4", GetType(System.String))
            For Each item As DataListItem In dlPartidas.Items
                Dim sPartida As String = DirectCast(item.FindControl("hid_Partida"), HiddenField).Value
                Dim sPres5 As String = DirectCast(item.FindControl("hid_Pres5"), HiddenField).Value
                Dim ArrUON() As String = Split(sPartida, "#")

                If ArrUON.Length > 1 Then
                    Dim drPres As DataRow = dtPartidasPress.NewRow
                    Select Case ArrUON.Length - 1
                        Case 1
                            drPres("UON1") = ArrUON(0)
                        Case 2
                            drPres("UON1") = ArrUON(0)
                            drPres("UON2") = ArrUON(1)
                        Case 3
                            drPres("UON1") = ArrUON(0)
                            drPres("UON2") = ArrUON(1)
                            drPres("UON3") = ArrUON(2)
                        Case 4
                            drPres("UON1") = ArrUON(0)
                            drPres("UON2") = ArrUON(1)
                            drPres("UON3") = ArrUON(2)
                            drPres("UON4") = ArrUON(3)
                    End Select


                    Dim ArrPartidas() As String = Split(ArrUON(4), "|")
                    drPres("PRES0") = sPres5
                    For i = 0 To ArrPartidas.Length - 1
                        drPres("PRES" & (i + 1).ToString) = ArrPartidas(i)
                    Next
                    For i = ArrPartidas.Length To 3
                        drPres("PRES" & (i + 1).ToString) = System.DBNull.Value
                    Next
                    dtPartidasPress.Rows.Add(drPres)
                End If
            Next
            Return dtPartidasPress
        End Get
    End Property

    Public Property EstadosFacturas As DataTable
        Get
            Return _dtEstadosFacturas
        End Get
        Set(ByVal value As DataTable)
            _dtEstadosFacturas = value
        End Set
    End Property

    Public Property FechaFacturaDesde() As Date
        Get
            FechaFacturaDesde = IIf(dteFechaFacturaDesde.Value <> Nothing, dteFechaFacturaDesde.Value, CDate(Nothing))
        End Get
        Set(ByVal Value As Date)
            dteFechaFacturaDesde.Value = Value
        End Set
    End Property

    Public Property FechaFacturaHasta() As Date
        Get
            FechaFacturaHasta = IIf(dteFechaFacturaHasta.Value <> Nothing, dteFechaFacturaHasta.Value, CDate(Nothing))
        End Get
        Set(ByVal Value As Date)
            dteFechaFacturaHasta.Value = Value
        End Set
    End Property

    Public Property FechaContabilizacionDesde() As Date
        Get
            FechaContabilizacionDesde = IIf(dteContabilizacionDesde.Value <> Nothing, dteContabilizacionDesde.Value, CDate(Nothing))
        End Get
        Set(ByVal Value As Date)
            dteContabilizacionDesde.Value = Value
        End Set
    End Property

    Public Property FechaContabilizacionHasta() As Date
        Get
            FechaContabilizacionHasta = IIf(dteContabilizacionHasta.Value <> Nothing, dteContabilizacionHasta.Value, CDate(Nothing))
        End Get
        Set(ByVal Value As Date)
            dteContabilizacionHasta.Value = Value
        End Set
    End Property

    Public Property ArticuloTexto() As String
        Get
            ArticuloTexto = txtArticulo.Text
        End Get
        Set(ByVal Value As String)
            txtArticulo.Text = Value
        End Set
    End Property

    'Almacenar para la BBDD
    Public Property ArticuloHidden() As String
        Get
            ArticuloHidden = hidArticulo.Value
        End Get
        Set(ByVal Value As String)
            '(gmn1-gmn2-gmn3-gmn4)
            hidArticulo.Value = Value
        End Set
    End Property

    Public Property ProveedorTexto() As String
        Get
            ProveedorTexto = txtProveedor.Text
        End Get
        Set(ByVal Value As String)
            txtProveedor.Text = Value
        End Set
    End Property

    'Almacenar para la BBDD
    Public Property ProveedorHidden() As String
        Get
            ProveedorHidden = hidProveedor.Value
        End Get
        Set(ByVal Value As String)
            hidProveedor.Value = Value
        End Set
    End Property

    Public Property EmpresaTexto() As String
        Get
            EmpresaTexto = txtEmpresa.Text
        End Get
        Set(ByVal Value As String)
            txtEmpresa.Text = Value
        End Set
    End Property

    'Almacenar para la BBDD
    Public Property EmpresaHidden() As String
        Get
            EmpresaHidden = hidEmpresa.Value
        End Get
        Set(ByVal Value As String)
            hidEmpresa.Value = Value
        End Set
    End Property

    ''' <summary>
    ''' Propiedad del gestor de la partida
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property GestorHidden() As String
        Get
            GestorHidden = hid_Gestor.Value
        End Get
        Set(ByVal Value As String)
            hid_Gestor.Value = Value
        End Set
    End Property

    Public Property GestorTexto() As String
        Get
            GestorTexto = txtGestor.Text
        End Get
        Set(ByVal Value As String)
            txtGestor.Text = Value
        End Set
    End Property

    ''' <summary>
    ''' Propiedad del centro de coste
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CentroCosteHidden() As String
        Get
            CentroCosteHidden = hid_CentroCostes.Value
        End Get
        Set(ByVal Value As String)
            hid_CentroCostes.Value = Value
        End Set
    End Property

    Public Property AnyoPedido() As String
        Get
            AnyoPedido = ddlAnyo.CurrentValue
        End Get
        Set(ByVal Value As String)
            If Value Is Nothing Then
                ddlAnyo.SelectedItemIndex = 10 'Inc 41 no se carga el año correctamente
            Else
                ddlAnyo.CurrentValue = Value
            End If
        End Set
    End Property

    ''' <summary>
    ''' Propiedad del numero de cesta(PEDIDO.NUM)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property NumCesta() As Long
        Get
            NumCesta = strToInt(txtNumCesta.Text)
        End Get
        Set(ByVal value As Long)
            If txtNumCesta.Text = "0" Then
                txtNumCesta.Text = ""
            Else
                txtNumCesta.Text = CStr(NumCesta)
            End If
        End Set
    End Property
    ''' <summary>
    ''' Propiedad del numero de pedido(ORDEN_ENTRAGA.NUM)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property NumPedido() As Long
        Get
            NumPedido = strToInt(txtNumPedido.Text)
        End Get
        Set(ByVal value As Long)
            If txtNumPedido.Text = "0" Then
                txtNumPedido.Text = ""
            Else
                txtNumPedido.Text = CStr(NumPedido)
            End If
        End Set
    End Property

    Public Property PedidoERP As String
        Get
            PedidoERP = txtPedidoERP.Text
        End Get
        Set(ByVal value As String)
            txtPedidoERP.Text = PedidoERP
        End Set
    End Property

    ''' <summary>
    ''' Ruta para abrir el buscador de Empresa
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property RutaBuscadorPartidas As String
        Get
            Return _RutaBuscadorPartidas
        End Get
        Set(ByVal value As String)
            _RutaBuscadorPartidas = value
        End Set
    End Property

    ''' <summary>
    ''' Ruta para abrir el buscador de Articulo
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property RutaBuscadorArticulo As String
        Get
            Return _RutaBuscadorArticulo
        End Get
        Set(ByVal value As String)
            _RutaBuscadorArticulo = value
        End Set
    End Property

    ''' <summary>
    ''' Porpiedades de la ventana para abrir el buscador de Articulo
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PropiedadesBuscadorArticulo As String
        Get
            Return _PropiedadesBuscadorArticulo
        End Get
        Set(ByVal value As String)
            _PropiedadesBuscadorArticulo = value
        End Set
    End Property



    ''' <summary>
    ''' Ruta para abrir el buscador de Articulo
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property RutaBuscadorGestor As String
        Get
            Return _RutaBuscadorGestor
        End Get
        Set(ByVal value As String)
            _RutaBuscadorGestor = value
        End Set
    End Property

    ''' <summary>
    ''' Ruta para abrir el buscador de Centro de coste
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property RutaBuscadorCentroCoste As String
        Get
            Return _RutaBuscadorCentroCoste
        End Get
        Set(ByVal value As String)
            _RutaBuscadorCentroCoste = value
        End Set
    End Property

    Public Property RutaWsAutoComplete As String
        Get
            Return _RutaWsAutoComplete
        End Get
        Set(ByVal value As String)
            _RutaWsAutoComplete = value
        End Set
    End Property

    Public Property Estado As Integer
        Get
            If wddEstado.SelectedValue <> Nothing Then
                Estado = CLng(wddEstado.SelectedValue)
            Else
                Estado = 0
            End If
        End Get
        Set(ByVal Value As Integer)
            If Value = -1 Then
                wddEstado.SelectedItemIndex = -1 'limpiar Dropdown estado
            Else
                wddEstado.SelectedValue = Value
            End If
        End Set
    End Property

    Public Property NumeroAlbaran() As String
        Get
            NumeroAlbaran = txtNumeroAlbaran.Text
        End Get
        Set(ByVal value As String)
            txtNumeroAlbaran.Text = value
        End Set
    End Property

    Public Property NumeroFacturaSAP() As String
        Get
            NumeroFacturaSAP = txtNumeroFacturaSAP.Text
        End Get
        Set(ByVal value As String)
            txtNumeroFacturaSAP.Text = value
        End Set
    End Property

    Public Property FacturaOriginal() As Boolean
        Get
            FacturaOriginal = chkFacturaOriginal.Checked
        End Get
        Set(ByVal value As Boolean)
            chkFacturaOriginal.Checked = value
        End Set
    End Property

    Public Property FacturaRectificativa() As Boolean
        Get
            FacturaRectificativa = chkFacturaRectificativa.Checked
        End Get
        Set(ByVal value As Boolean)
            chkFacturaRectificativa.Checked = value
        End Set
    End Property


    Public Property ImporteDesde() As Double
        Get
            If wneImporteDesde.Text <> Nothing Then
                ImporteDesde = CDbl(wneImporteDesde.Value)
            Else
                ImporteDesde = Nothing
            End If
        End Get
        Set(ByVal Value As Double)
            wneImporteDesde.Value = IIf(Value = 0, "", Value)
        End Set
    End Property
    Public Property ImporteHasta() As Double
        Get
            If wneImporteHasta.Text <> Nothing Then
                ImporteHasta = CDbl(wneImporteHasta.Value)
            Else
                ImporteHasta = Nothing
            End If
        End Get
        Set(ByVal Value As Double)
            wneImporteHasta.Value = IIf(Value = 0, Nothing, Value)
        End Set
    End Property

    Public WriteOnly Property Filtro() As Long
        Set(ByVal Value As Long)
            If Not IsPostBack Then
                CargarEstados()
            End If
            mlFiltro = Value
        End Set
    End Property

    Public WriteOnly Property Textos() As DataTable
        Set(ByVal Value As DataTable)
            oTextos = Value
        End Set
    End Property

    Private m_sRefCultural As String
    Public WriteOnly Property RefCultural() As String
        Set(ByVal Value As String)
            m_sRefCultural = Value
        End Set
    End Property

    Private m_oDateFormat As System.Globalization.DateTimeFormatInfo
    Public WriteOnly Property DateFormat() As System.Globalization.DateTimeFormatInfo
        Set(ByVal Value As System.Globalization.DateTimeFormatInfo)
            m_oDateFormat = Value
        End Set
    End Property

    Public Property PartidasDataTable As DataTable
        Get
            Return _dtPartidas
        End Get
        Set(ByVal value As DataTable)
            _dtPartidas = value
        End Set
    End Property

    Public Property FilaProveedorEmpresaVisible() As Boolean
        Get
            FilaProveedorEmpresaVisible = filaProveedorEmpresa.Visible
        End Get
        Set(ByVal Value As Boolean)
            filaProveedorEmpresa.Visible = Value
            If Value Then
                WriteScriptsProveedorEmpresa()
            End If
        End Set
    End Property
#End Region

#Region "Carga Inicial"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub

        WriteScripts()
        InicializacionControles()

        CargarIdiomasControles()

        'Si no hay acceso al modulo SM no se cargan las partidas, ni gestor, ni centro de coste
        If _AccesoFSSM Then
            CargarControlesPartidas()

        Else
            CeldaCentroCoste.Visible = False
            CeldaLblCentroCoste.Visible = False
            CeldaLblGestor.Visible = False
            CeldaGestor.Visible = False
            CeldaPartidas.Visible = False
        End If

        If _OblCodPedDir Then
            CeldaPedERP.Visible = True
            CeldaLblPedERP.Visible = True
        Else
            CeldaPedERP.Visible = False
            CeldaLblPedERP.Visible = False
        End If

        'Si no hay integracion en facturas no se muestra el campo Factura Erp
        If Session("HayIntegracionFacturas") Then
            CeldaNumFacturaERP.Visible = True
            CeldaLblNumFacturaERP.Visible = True
        Else
            CeldaNumFacturaERP.Visible = False
            CeldaLblNumFacturaERP.Visible = False
        End If



        CargarAnyos()

        If mlFiltro = Nothing Then
            CargarEstados()
        End If
    End Sub

    ''' <summary>
    ''' Inicializa los componentes y funciones javascript de los elementos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
    Private Sub InicializacionControles()

        ddlAnyo.CurrentValue = ""
        wddEstado.CurrentValue = "" 'limpiar Dropdown estado

        
        dteFechaFacturaDesde.NullText = ""
        dteFechaFacturaHasta.NullText = ""
        dteContabilizacionDesde.NullText = ""
        dteContabilizacionHasta.NullText = ""

        txtCentroCoste.Attributes.Add("onkeydown", "javascript:return eliminarCentroCoste(event)")
        txtGestor.Attributes.Add("onkeydown", "javascript:return eliminarGestor(event)")
        txtArticulo.Attributes.Add("onkeydown", "javascript:return eliminarArticulo(event)")

        'Configuramos la visibilidad de los campos de la busqueda avanzada segun permisos


    End Sub

    ''' <summary>
    ''' Inicializa javascript de los elementos
    ''' </summary>
    ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
    Private Sub WriteScripts()

        'Funcion que sirve para eliminar el Gestor
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarGestor") Then
            Dim sScript As String = "function eliminarGestor(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    "  " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campoGestor = document.getElementById('" & txtGestor.ClientID & "')" & vbCrLf & _
                    " if (campoGestor) { campoGestor.value = ''; } " & vbCrLf & _
                    " campoHiddenGestor = document.getElementById('" & hid_Gestor.ClientID & "')" & vbCrLf & _
                    " if (campoHiddenGestor) { campoHiddenGestor.value = ''; } " & vbCrLf & _
            " } " & vbCrLf & _
            "  " & vbCrLf & _
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarGestor", sScript, True)
        End If

        'Funcion que sirve para eliminar el Centro de coste
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarCentroCoste") Then
            Dim sScript As String = "function eliminarCentroCoste(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    "  " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campoCentroCoste = document.getElementById('" & txtCentroCoste.ClientID & "')" & vbCrLf & _
                    " if (campoCentroCoste) { campoCentroCoste.value = ''; } " & vbCrLf & _
                    " campoHiddenCentroCoste = document.getElementById('" & hid_CentroCostes.ClientID & "')" & vbCrLf & _
                    " if (campoHiddenCentroCoste) { campoHiddenCentroCoste.value = ''; } " & vbCrLf & _
            " } " & vbCrLf & _
            "  " & vbCrLf & _
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarCentroCoste", sScript, True)
        End If

        'Funcion que sirve para eliminar el articulo relacionado
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarArticulo") Then
            Dim sScript As String = "function eliminarArticulo(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    "  " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campoArticulo = document.getElementById('" & txtArticulo.ClientID & "')" & vbCrLf & _
                    " if (campoArticulo) { campoArticulo.value = ''; } " & vbCrLf & _
                    " campoHiddenArticulo = document.getElementById('" & hidArticulo.ClientID & "')" & vbCrLf & _
                    " if (campoHiddenArticulo) { campoHiddenArticulo.value = ''; } " & vbCrLf & _
             " } " & vbCrLf & _
            "  " & vbCrLf & _
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarArticulo", sScript, True)
        End If

        imgCentroCosteLupa.Attributes.Add("onclick", "javascript:window.open('" & _RutaBuscadorCentroCoste & If(InStr(_RutaBuscadorCentroCoste, "?") > 0, "&", "?") & "idControl=" & txtCentroCoste.ClientID & "&idHidControl=" & hid_CentroCostes.ClientID & "', '_blank', 'width=850,height=630,status=yes,resizable=no,top=200,left=200');return false;")
        imgGestorLupa.Attributes.Add("onclick", "javascript:window.open('" & _RutaBuscadorGestor & If(InStr(_RutaBuscadorGestor, "?") > 0, "&", "?") & "Desde=IM&idControl=" & txtGestor.ClientID & "&idHidControl=" & hid_Gestor.ClientID & "', '_blank', 'width=850,height=630,status=yes,resizable=no,top=200,left=200');return false;")
        imgArticuloLupa.Attributes.Add("onclick", "javascript:window.open('" & _RutaBuscadorArticulo & If(InStr(_RutaBuscadorArticulo, "?") > 0, "&", "?") & "ClientId=" & txtArticulo.ClientID & "&idHidControl=" & hidArticulo.ClientID & "&Origen=VisorFacturas', '_blank'," & If(_PropiedadesBuscadorArticulo <> Nothing, "'" & _PropiedadesBuscadorArticulo & "'", "'width=500,height=400,status=yes,resizable=no,top=200,left=200'") & ");return false;")
        txtArticulo.Attributes.Add("onkeydown", "javascript:return eliminarArticulo(event)")
        txtArticulo_AutoCompleteExtender.ServicePath = _RutaWsAutoComplete

    End Sub

    Private Sub WriteScriptsProveedorEmpresa()
        imgProveedorLupa.OnClientClick = "window.open('" & ConfigurationManager.AppSettings("RutaFS") & "_Common/BuscadorProveedores.aspx?PM=true&Contr=true', '_blank', 'width=830,height=530,status=yes,resizable=no,top=150,left=150');return false;"
        imgEmpresaLupa.Attributes.Add("onClick", "javascript:window.open('" & ConfigurationManager.AppSettings("rutaFS") & "_common/BuscadorEmpresas.aspx','_blank','width=800,height=600, location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no');return false;")
        txtEmpresa_AutoCompleteExtender.ServicePath = _RutaWsAutoComplete
        txtProveedor_AutoCompleteExtender.ServicePath = _RutaWsAutoComplete

        'Funcion que sirve para eliminar el proveedor
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarProveedor") Then
            Dim sScript As String = "function eliminarProveedor(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    "  " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campoProveedor = document.getElementById('" & txtProveedor.ClientID & "')" & vbCrLf & _
                    " if (campoProveedor) { campoProveedor.value = ''; } " & vbCrLf & _
                    " campoHiddenProveedor = document.getElementById('" & hidProveedor.ClientID & "')" & vbCrLf & _
                    " if (campoHiddenProveedor) { campoHiddenProveedor.value = ''; } " & vbCrLf & _
            " } " & vbCrLf & _
            "  " & vbCrLf & _
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarProveedor", sScript, True)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered("prove_seleccionado") Then
            Dim sScript As String = " function prove_seleccionado2(sCIF, sProveCod, sProveDen) {" & vbCrLf & _
                    " document.getElementById('" & hidProveedor.ClientID & "').value = sProveCod " & vbCrLf & _
                    " document.getElementById('" & txtProveedor.ClientID & "').value = sProveDen " & vbCrLf & _
            " } " & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "prove_seleccionado", sScript, True)
        End If

        'Funcion que sirve para eliminar la empresa relacionada
        If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarEmpresa") Then
            Dim sScript As String = "function eliminarEmpresa(event) {" & vbCrLf & _
            " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                    "  " & vbCrLf & _
            " } else { " & vbCrLf & _
                " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                    " campoEmpresa = document.getElementById('" & txtEmpresa.ClientID & "')" & vbCrLf & _
                    " if (campoEmpresa) { campoEmpresa.value = ''; } " & vbCrLf & _
                    " campoHiddenEmpresa = document.getElementById('" & hidEmpresa.ClientID & "')" & vbCrLf & _
                    " if (campoHiddenEmpresa) { campoHiddenEmpresa.value = ''; } " & vbCrLf & _
            " } " & vbCrLf & _
            "  " & vbCrLf & _
            " } }" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarEmpresa", sScript, True)
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered("seleccionarEmpresa") Then
            Dim sScript As String = "function SeleccionarEmpresa(idEmpresa, nombreEmpresa, nifEmpresa) {" & vbCrLf & _
                    "oIdEmpresa = document.getElementById('" & hidEmpresa.ClientID & "');" & vbCrLf & _
                    " if (oIdEmpresa) { oIdEmpresa.value = idEmpresa; } " & vbCrLf & _
                    "otxtEmpresa = document.getElementById('" & txtEmpresa.ClientID & "');" & vbCrLf & _
                    " if (otxtEmpresa) { otxtEmpresa.value = nombreEmpresa; } " & vbCrLf & _
            " } " & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "seleccionarEmpresa", sScript, True)
        End If
    End Sub

    ''' <summary>
    ''' Carga los controles con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0seg.</remarks>
    Private Sub CargarIdiomasControles()
        lblEmpresa.Text = String.Format("{0}:", oTextos(16).Item(1))
        lblProveedor.Text = String.Format("{0}:", oTextos(1).Item(1))
        lblFechaFactura.Text = String.Format("{0}:", oTextos(6).Item(1))
        lblEstado.Text = String.Format("{0}:", oTextos(7).Item(1))
        lblNumeroAlbaran.Text = String.Format("{0}:", oTextos(8).Item(1))
        lblFechaContabilizacion.Text = String.Format("{0}:", oTextos(9).Item(1))
        lblPedido.Text = String.Format("{0}:", oTextos(10).Item(1))
        lblPedidoERP.Text = String.Format("{0}:", oTextos(11).Item(1))
        lblImporte.Text = String.Format("{0}:", oTextos(12).Item(1))
        lblArticulo.Text = String.Format("{0}:", oTextos(13).Item(1))
        lblNumeroFacturaSAP.Text = String.Format("{0}:", oTextos(14).Item(1))
        lblCentroCoste.Text = String.Format("{0}:", oTextos(15).Item(1))
        lblGestor.Text = String.Format("{0}:", oTextos(27).Item(1))
        chkFacturaOriginal.Text = oTextos(45).Item(1)
        chkFacturaRectificativa.Text = oTextos(46).Item(1)

        txtwmeNumCesta.WatermarkText = oTextos(51).Item(1)
        txtwmeNumPedido.WatermarkText = oTextos(52).Item(1)
    End Sub

    ''' <summary>
    ''' Carga los años en el combo de años de pedido. Desde el actual menos 10 hasta el actual más 10.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarAnyos()
        Dim iAnyoActual As Integer
        Dim iInd As Integer

        Dim ddItem As Infragistics.Web.UI.ListControls.DropDownItem
        iAnyoActual = Year(Now)
        ddlAnyo.Items.Clear()
        For iInd = iAnyoActual - 10 To iAnyoActual + 10
            ddItem = New Infragistics.Web.UI.ListControls.DropDownItem
            ddItem.Value = iInd
            ddItem.Text = iInd.ToString
            ddlAnyo.Items.Add(ddItem)
        Next
        ddlAnyo.SelectedItemIndex = ddlAnyo.Items.IndexOf(ddlAnyo.Items.FindItemByValue(iAnyoActual.ToString))
    End Sub
#End Region
    

    ''' <summary>
    ''' Nos indica si un nodo esta dentro de una lista o no
    ''' </summary>
    ''' <param name="listaElementos">Cadena de nodos seleccionada</param>
    ''' <param name="valorABuscar">Valor del nodo a buscar en la lista</param>        
    ''' <returns>True o False si a enontrado o no el nodo</returns>
    ''' <remarks>Llamada desde=SyncChanges//CargarCertificados; Tiempo máximo=0,2seg.</remarks>
    Private Function buscarElemento(ByVal listaElementos As String, ByVal valorABuscar As String) As Boolean
        Dim pos As Integer
        Dim elementosSeleccionados() As String
        Dim bEncontrado As Boolean = False
        pos = InStr(listaElementos, valorABuscar)
        If pos > 0 Then
            If Len(listaElementos) = Len(valorABuscar) Then 'El nodo coincide con la seleccion
                Return True
            Else
                If pos - 1 > 0 Then
                    bEncontrado = False
                    elementosSeleccionados = System.Text.RegularExpressions.Regex.Split(listaElementos, "[\s$][\s$][\s$]")
                    For i = 0 To UBound(elementosSeleccionados)
                        If elementosSeleccionados(i) = valorABuscar Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next
                    Return bEncontrado
                Else
                    Return True

                End If

            End If
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Enlaza la fuente de datos con el datalist de Partidas presupuestarias
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarControlesPartidas()
        dlPartidas.DataSource = _dtPartidas
        dlPartidas.DataBind()
    End Sub

    ''' <summary>
    ''' Carga los distintos tipos de estados
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarEstados()
        wddEstado.Items.Clear()

        Dim oItem As Infragistics.Web.UI.ListControls.DropDownItem
        wddEstado.Items.Clear()
        oItem = New Infragistics.Web.UI.ListControls.DropDownItem
        oItem.Value = 0
        oItem.Text = String.Empty
        wddEstado.Items.Add(oItem)
        For Each drRow As DataRow In _dtEstadosFacturas.Rows
            oItem = New Infragistics.Web.UI.ListControls.DropDownItem
            oItem.Value = DBNullToInteger(drRow("ID"))
            oItem.Text = drRow("DEN")
            wddEstado.Items.Add(oItem)
        Next

        wddEstado.EnableAutoFiltering = Infragistics.Web.UI.ListControls.AutoFiltering.Client
        wddEstado.AutoFilterQueryType = Infragistics.Web.UI.ListControls.AutoFilterQueryTypes.Contains
        wddEstado.AutoFilterSortOrder = Infragistics.Web.UI.ListControls.DropDownAutoFilterSortOrder.Ascending
    End Sub

    ''' <summary>
    ''' Evento que salta por cada partida de la instalación
    ''' </summary>
    ''' <param name="sender">datalist de partidas</param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub dlPartidas_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlPartidas.ItemDataBound
        Dim fila As DataRow = DirectCast(e.Item.DataItem, DataRowView).Row
        Dim txtPartida As TextBox
        With e.Item
            DirectCast(.FindControl("lblPartida"), Label).Text = String.Format("{0}:", fila("DEN"))
            Dim sHiddenControlID As String = DirectCast(.FindControl("hid_Partida"), HiddenField).ClientID
            Dim sPartida As String = fila("PRES5")
            DirectCast(.FindControl("hid_Pres5"), HiddenField).Value = sPartida
            txtPartida = DirectCast(.FindControl("txtPartida"), TextBox)
            Dim sTextControlID As String = txtPartida.ClientID
            DirectCast(.FindControl("imgPartidaLupa"), ImageButton).Attributes.Add("onclick", "javascript:AbrirPartidas('" & _RutaBuscadorPartidas & "','" & Me.txtCentroCoste.ClientID & "','" & hid_CentroCostes.ClientID & "','" & sTextControlID & "','" & sHiddenControlID & "','" & sPartida & "','" & Me.hid_CentroCostes.Value & "','" & Me.txtCentroCoste.Text & "','" & fila("DEN") & "'); return false;")

            'Escribo los scripts de javascript para estos campos del datalist

            'Funcion que sirve para eliminar el Gestor
            If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarPartida_" & sPartida) Then
                Dim sScript As String = "function eliminarPartida_" & sPartida & "(event) {" & vbCrLf & _
                " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                        "  " & vbCrLf & _
                " } else { " & vbCrLf & _
                    " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                        " campoPartida = document.getElementById('" & sTextControlID & "')" & vbCrLf & _
                        " if (campoPartida) { campoPartida.value = ''; } " & vbCrLf & _
                        " campoHiddenPartida = document.getElementById('" & sHiddenControlID & "')" & vbCrLf & _
                        " if (campoHiddenPartida) { campoHiddenPartida.value = ''; } " & vbCrLf & _
                " } " & vbCrLf & _
                "  " & vbCrLf & _
                " } }" & vbCrLf
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarPartida_" & sPartida, sScript, True)

                txtPartida.Attributes.Add("onkeydown", "javascript:return eliminarPartida_" & sPartida & "(event)")
            End If
        End With
    End Sub
End Class