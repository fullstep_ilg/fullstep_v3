﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucBusquedaAvanzadaFacturas.ascx.vb" Inherits="FSNUserControls.wucBusquedaAvanzadaFacturas" %>
<script language="javascript" type="text/javascript" >

    /*<%--
    ''' <summary>
    ''' Function que comprueba el valor del control pasado. Si es numérico lo devuelve, si no, no.
    ''' </summary>
    ''' txt: control que lanza el evento
    ''' <remarks>Llamada desde evento onchange de controles textbox; Tiempo máximo:0,1seg.</remarks>--%>*/
    function validarsolonum(txt) {
        var str = '';
        for (var i = 0; i < txt.value.length; i++) {
            if (txt.value.charCodeAt(i) >= 48 && txt.value.charCodeAt(i) <= 57)
                str += txt.value.charAt(i);
        }
        txt.value = str;
    }

    /*<%--
    ''' <summary>
    ''' Function que comprueba que el valor tecleado es numérico. Si no lo es, cancela el evento.
    ''' </summary>
    ''' <remarks>Llamada desde evento onKeyPress de controles textbox; Tiempo máximo:0,1seg.</remarks>--%>*/
    function validarkeynum() {
        if (event.keyCode < 48 || event.keyCode > 57) {
            event.returnValue = false;
        }
    }

    /*<%--
    ''' <summary>
    ''' Function que comprueba que los valore pegados son numéricos. Si no lo son, impide el pagado
    ''' </summary>
    ''' <remarks>Llamada desde evento onpaste de controles textbox; Tiempo máximo:0,1seg.</remarks>--%>*/
    function validarpastenum() {
        var texto = window.clipboardData.getData('Text');
        var str = '';
        for (var i = 0; i < texto.length; i++) {
            if (texto.charCodeAt(i) >= 48 && texto.charCodeAt(i) <= 57)
                str += texto.charAt(i);
        }
        if (texto != str) event.returnValue = false;
    }

    function CentroCoste_seleccionado(idControl,idHidControl,sUON1, sUON2, sUON3, sUON4, sDen) {
        var sValor = '';

        //lo que se guarda en valor_text
        if (sUON1)
            sValor = sUON1;
        if (sUON2)
            sValor = sValor + '#' + sUON2;
        if (sUON3)
            sValor = sValor + '#' + sUON3;
        if (sUON4)
            sValor = sValor + '#' + sUON4;

        txtCentroCostes = document.getElementById(idControl)
        txtCentroCostes.value=sDen
        hidCentroCostes = document.getElementById(idHidControl)
        hidCentroCostes.value = sValor

    }

    function Partida_seleccionada(IdControlPartida, IdControlPartidaDen, sUON1, sUON2, sUON3, sUON4, sContrato, sDen, IdControlCentroCosteDen, IdControlCentroCoste, sDenCC) {
        var sValor = '';

        //lo que se guarda en valor_text
        if (sUON1)
            sValor = sUON1;
        if (sUON2)
            sValor = sValor + '#' + sUON2;
        if (sUON3)
            sValor = sValor + '#' + sUON3;
        if (sUON4)
            sValor = sValor + '#' + sUON4;
        if (sContrato)
            sValor = sValor + '#' + sContrato;

        if (IdControlPartida == "")
            return

        txtPartida = document.getElementById(IdControlPartidaDen)
        txtPartida.value = sDen
        hidPartida = document.getElementById(IdControlPartida)
        hidPartida.value = sValor


        if (IdControlCentroCoste != "") {
            var separador = sValor.lastIndexOf('#');
            if (separador > 0)
                var sValor = sValor.substr(0, separador);

            txtCentroCostes = document.getElementById(IdControlCentroCosteDen)
            txtCentroCostes.value = sDenCC
            hidCentroCostes = document.getElementById(IdControlCentroCoste)
            hidCentroCostes.value = sValor
            
        }
    }

    function usu_seleccionado(sUsuCod, sUsuNom, sUsuEmail) {
        var txtGestor = document.getElementById('<%= txtGestor.ClientID %>')
        var hidGestor = document.getElementById('<%= hid_Gestor.ClientID %>')
        txtGestor.value = sUsuNom + ' (' + sUsuEmail + ')'
        hidGestor.value = sUsuCod
    }

    //Recoge el ID de la empresa seleccionada con el autocompletar
    function Articulo_selected(sender, e) {
        oHid_Articulo = document.getElementById('<%= hidArticulo.ClientID %>');
        if (oHid_Articulo)
            oHid_Articulo.value = e._value;
    }

    //Recoge el ID de la empresa seleccionada con el autocompletar
    function Empresa_selected(sender, e) {
         oIdEmpresa = document.getElementById('<%= hidEmpresa.ClientID %>')
         if (oIdEmpresa)  
            oIdEmpresa.value = e._value;
     }

    //Recoge el ID del proveedor seleccionado con el autocompletar
    function Proveedor_selected(sender, e) {
         oIdProveedor = document.getElementById('<%= hidProveedor.ClientID %>')
         if (oIdProveedor) 
            oIdProveedor.value = e._value;
    }
    
    //Funcion que abre el buscador de partidas
    function AbrirPartidas(rutaPartidas, idTxtCentroCoste, idHidCentroCoste, idTxtPartida, idHidPartida, Partida, CentroCoste, CentroCosteDen, PartidaDen) {
        var txtCentroCoste = document.getElementById(idTxtCentroCoste)
        var hidCentroCoste = document.getElementById(idHidCentroCoste)
        var hidPartida = document.getElementById(idHidPartida)

        window.open(rutaPartidas + "?idTxtCentroCoste=" + idTxtCentroCoste + "&idHidCentroCoste=" + idHidCentroCoste + "&idTxtPartida=" + idTxtPartida + "&idHidPartida=" + idHidPartida + "&Partida=" + Partida + "&CentroCoste=" + escape(hidCentroCoste.value) + "&CentroCosteDen=" + txtCentroCoste.value + "&PartidaDen=" + PartidaDen, "_blank", "width=850,height=630,status=yes,resizable=no,top=200,left=200")
    }

    
 </script>
 
<table width="99%" id="marco" runat="server">
<tr id="filaProveedorEmpresa" runat="server" visible="false">
    <td><asp:Label ID="lblProveedor" runat="server" Text="DProveedor" CssClass="Etiqueta"></asp:Label></td>
                        <td><table style="background-color: White; width: 150px; border: solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                             <tr><td><asp:TextBox ID="txtProveedor" runat="server" Width="240px" BorderWidth="0px" BackColor="White" Height="16px"></asp:TextBox>
                                     <ajx:AutoCompleteExtender ID="txtProveedor_AutoCompleteExtender" runat="server" CompletionSetCount="10"
                                          DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1" ServiceMethod="GetProveedores"
                                          ServicePath="" TargetControlID="txtProveedor" EnableCaching="False" OnClientItemSelected="Proveedor_selected">
                                     </ajx:AutoCompleteExtender>
                                  </td>
                                  <td style="border-left: solid 1px #BBBBBB; padding-left: 2px; padding-right: 0px">
                                    <asp:ImageButton ID="imgProveedorLupa" runat="server" SkinID="BuscadorLupa" />
                                  </td>
                             </tr>
                             <asp:HiddenField ID="hidProveedor" runat="server" />
                             </table>
                        </td>
                        <td><asp:Label ID="lblEmpresa" runat="server" Text="DEmpresa:" CssClass="Etiqueta"></asp:Label></td>
                        <td>
                        <table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtEmpresa" runat="server" Width="240px" BorderWidth="0px"></asp:TextBox>
                                    <ajx:AutoCompleteExtender ID="txtEmpresa_AutoCompleteExtender" 
                                     runat="server" CompletionSetCount="10" DelimiterCharacters="" Enabled="True"
                                     MinimumPrefixLength="1" ServiceMethod="GetEmpresas" ServicePath=""
                                     TargetControlID="txtEmpresa" EnableCaching="False" OnClientItemSelected="Empresa_selected"  ></ajx:AutoCompleteExtender >
                                </td>
                                <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                                    <asp:ImageButton id="imgEmpresaLupa" runat="server" SkinID="BuscadorLupa" /> 
                                </td>
                            </tr>                        
                            <asp:HiddenField ID="hidEmpresa" runat="server" />
                        </table>
                        </td>
</tr>
<tr><td><asp:Label ID="lblFechaFactura" runat="server" Text="DFecha factura (desde-hasta):" CssClass="Etiqueta"></asp:Label></td>
    <td><div style="float:left">
            <div style="display:table-cell;padding-right:5px"><igpck:WebDatePicker ID="dteFechaFacturaDesde" runat="server" Width="100px"  SkinID="Calendario"></igpck:WebDatePicker></div>
            <div style="display:table-cell;padding-right:5px;vertical-align:middle">-</div>
            <div style="display:table-cell;padding-right:5px"><igpck:WebDatePicker ID="dteFechaFacturaHasta" runat="server" Width="100px"  SkinID="Calendario"></igpck:WebDatePicker></div>
        </div>
    </td>
    <td><asp:Label ID="lblEstado" runat="server" Text="DEstado:" CssClass="Etiqueta"></asp:Label></td>
    <td><ig:WebDropDown ID="wddEstado" runat="server" Width="250px" EnableClosingDropDownOnSelect="true" EnableClientRendering="false">                    
        </ig:WebDropDown>
    </td>
    <td><asp:Label ID="lblNumeroAlbaran" runat="server" Text="DNumero Albaran:" CssClass="Etiqueta"></asp:Label></td>
    <td><asp:TextBox ID="txtNumeroAlbaran" runat="server" Width="235px"></asp:TextBox></td>
</tr>
<tr><td><asp:Label ID="lblFechaContabilizacion" runat="server" Text="DFecha Contabilizacion (desde-hasta):" CssClass="Etiqueta"></asp:Label></td>
    <td><div style="float:left">
            <div style="display:table-cell;padding-right:5px"><igpck:WebDatePicker ID="dteContabilizacionDesde" runat="server" Width="100px"  SkinID="Calendario"></igpck:WebDatePicker></div>
            <div style="display:table-cell;padding-right:5px;vertical-align:middle">-</div>
            <div style="display:table-cell;padding-right:5px"><igpck:WebDatePicker ID="dteContabilizacionHasta" runat="server" Width="100px"  SkinID="Calendario"></igpck:WebDatePicker></div>
        </div>
    </td>
    <td><asp:Label ID="lblPedido" runat="server" Text="DPedido:" CssClass="Etiqueta"></asp:Label></td>
    <td><div style="float:left">
            <div style="display:table-cell;padding-right:5px"><ig:WebDropDown ID="ddlAnyo" runat="server" AutoPostBack="false" EnableMultipleSelection="false"
                                                        EnableClosingDropDownOnSelect="true" BackColor="White" Width="60px" EnableCustomValues="false"
                                                        DropDownContainerWidth="70" EnableDropDownAsChild="false" EnableCustomValueSelection="false"
                                                         EnableMarkingMatchedText="true" AutoSelectOnMatch="true">
                                                    </ig:WebDropDown>
            </div>
            <div style="display:table-cell;padding-right:5px;vertical-align:middle"><asp:TextBox CssClass="CajaTexto" ID="txtNumCesta" runat="server" onchange="validarsolonum(this)" onkeypress="return validarkeynum()" onpaste="validarpastenum()" Columns="12"></asp:TextBox>
                                        <ajx:TextBoxWatermarkExtender WatermarkCssClass="waterMark" ID="txtwmeNumCesta" runat="server" TargetControlID="txtNumCesta"></ajx:TextBoxWatermarkExtender></div>
            <div style="display:table-cell;padding-right:5px;vertical-align:middle"><asp:TextBox CssClass="CajaTexto" ID="txtNumPedido" runat="server" onchange="validarsolonum(this)" onkeypress="return validarkeynum()" onpaste="validarpastenum()" Columns="12"></asp:TextBox>
                                        <ajx:TextBoxWatermarkExtender WatermarkCssClass="waterMark" ID="txtwmeNumPedido" runat="server" TargetControlID="txtNumPedido"></ajx:TextBoxWatermarkExtender></div>
        </div></td>
    <td id="CeldaLblPedERP" runat="server"><asp:Label ID="lblPedidoERP" runat="server" Text="DRef. en factura:" CssClass="Etiqueta"></asp:Label></td>
    <td id="CeldaPedERP" runat="server"><asp:TextBox ID="txtPedidoERP" runat="server" Width="235px"></asp:TextBox></td>
</tr>
<tr><td><asp:Label ID="lblImporte" runat="server" Text="DImporte (desde-hasta):" CssClass="Etiqueta"></asp:Label></td>
    <td><div style="float:left">
            <div style="display:table-cell;padding-right:5px"><igpck:WebNumericEditor ID="wneImporteDesde" runat="server" BorderStyle="Solid" BorderColor="#BBBBBB" Width="96px"></igpck:WebNumericEditor></div>
            <div style="display:table-cell;padding-right:5px;vertical-align:middle">-</div>
            <div style="display:table-cell;padding-right:5px"><igpck:WebNumericEditor id="wneImporteHasta" runat="server" BorderStyle="Solid" BorderColor="#BBBBBB" Width="96px"></igpck:WebNumericEditor></div>
        </div>
    </td>
    <td><asp:Label ID="lblArticulo" runat="server" Text="DArticulo:" CssClass="Etiqueta"></asp:Label></td>
    <td><table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
        <tr><td>
                <asp:TextBox ID="txtArticulo" runat="server" Width="240px" BorderWidth="0px"></asp:TextBox>
                <ajx:AutoCompleteExtender ID="txtArticulo_AutoCompleteExtender" 
                    runat="server" CompletionSetCount="10" DelimiterCharacters="" Enabled="True"
                    MinimumPrefixLength="1" ServiceMethod="GetArticulos" ServicePath="" OnClientItemSelected="Articulo_selected"
                    TargetControlID="txtArticulo" EnableCaching="False" ></ajx:AutoCompleteExtender >
             </td>
             <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                  <asp:ImageButton id="imgArticuloLupa" runat="server" SkinID="BuscadorLupa" /> 
             </td>
        </tr>                        
        <asp:HiddenField ID="hidArticulo" runat="server" />
        </table>
    </td>
    <td id="CeldaLblNumFacturaERP" runat="server"><asp:Label ID="lblNumeroFacturaSAP" runat="server" Text="DNum Factura SAP:" CssClass="Etiqueta"></asp:Label></td>
    <td id="CeldaNumFacturaERP" runat="server"><asp:TextBox ID="txtNumeroFacturaSAP" runat="server" Width="235px"></asp:TextBox></td>
</tr>
<tr>
<td id="CeldaLblCentroCoste" runat="server"><asp:Label ID="lblCentroCoste" runat="server" Text="DCentroCoste:" CssClass="Etiqueta"></asp:Label></td>
    <td id="CeldaCentroCoste" runat ="server">
        <table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
        <tr><td>
                <asp:TextBox ID="txtCentroCoste" runat="server" Width="240px" BorderWidth="0px" onchange="CentroCosteChange(this)"></asp:TextBox>
             </td>
             <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                  <asp:ImageButton id="imgCentroCosteLupa" runat="server" SkinID="BuscadorLupa" /> 
             </td>
        </tr>                        
        <asp:HiddenField ID="hid_CentroCostes" runat="server" />
        </table>
    </td>
<td id="CeldaLblGestor" runat="server"><asp:Label ID="lblGestor" runat="server" Text="DGestor:" CssClass="Etiqueta"></asp:Label></td>
    <td id="CeldaGestor" runat="server">
        <table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
        <tr><td>
                <asp:TextBox ID="txtGestor" runat="server" Width="240px" BorderWidth="0px"></asp:TextBox>
             </td>
             <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                  <asp:ImageButton id="imgGestorLupa" runat="server" SkinID="BuscadorLupa" /> 
             </td>
        </tr>                        
        <asp:HiddenField ID="hid_Gestor" runat="server" />
        </table>
    </td>

<td colspan="2"><asp:CheckBox ID="chkFacturaOriginal" runat="server" Text="DFactura Original" Checked="true"  CssClass="Etiqueta" /> <asp:CheckBox ID="chkFacturaRectificativa" runat="server" Checked="true" Text="DFactura Rectificativa" CssClass="Etiqueta" /></td>
    
</tr>
<tr>
    <td id="CeldaPartidas" runat="server" colspan="2">
        <asp:DataList ID="dlPartidas" runat="server" Width="100%" RepeatDirection="Vertical" >
        <ItemTemplate>
            <table style="width:100%">
                <tr>
                    <td style="width:40%"><asp:Label ID="lblPartida" runat="server" Text="DPartida:" CssClass="Etiqueta"></asp:Label></td>
                    <td style="width:60%">
                        <table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                        <tr><td >
                                <asp:TextBox ID="txtPartida" runat="server" Width="240px" BorderWidth="0px"></asp:TextBox>
                             </td>
                             <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px;">
                                  <asp:ImageButton id="imgPartidaLupa" runat="server" SkinID="BuscadorLupa" /> 
                             </td>
                        </tr>                        
                        <asp:HiddenField ID="hid_Partida" runat="server" />
                        <asp:HiddenField ID="hid_Pres5" runat="server" />
                        </table>
                    </td>
                </tr>
            </table>
        </ItemTemplate>
        </asp:DataList> 
        
    </td>
</tr>
</table>