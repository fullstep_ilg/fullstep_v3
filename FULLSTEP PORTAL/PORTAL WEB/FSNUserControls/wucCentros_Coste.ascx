﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucCentros_Coste.ascx.vb" Inherits="FSNUserControls.wucCentros_Coste" %>

<script language="javascript" type="text/javascript">
    function FindClicked(txtbxID, treeID) {

        // Get the TextBox that has the Regular Expression
        var txtbx = document.getElementById(txtbxID);

        // Set Regular Expression variable
        var regex = txtbx.value;

        if (regex == '')
            return false

        var tree = $find(treeID);
        var nodeCollection = tree.getNodes();

        clearSelectedNodes(tree)

        for (i = 0; i <= nodeCollection.get_length() - 1; i++) {
            var node = tree.getNodes(i).getNode(0)
            if(SearchInTree(node, regex)==true)
                break
        }
        txtbx.selectionStart = txtbx.value.length;
        txtbx.selectionEnd = txtbx.value.length;
        txtbx.focus();
    }

    function SearchInTree(node, regex) {
        for (var i=0; i <= node.getItems().get_length() - 1; i++) {
            var childNode = node.get_childNode(i)
            if (childNode.get_text().search(regex)!=-1) {
                childNode.set_selected(true)
                expandNodeParents(childNode)
                childNode.get_element().scrollIntoView()
                return true
            } else {
                if(childNode.hasChildren()==true)
                    SearchInTree(childNode, regex) 
            }
        }
        return false
    }

    //funcion que desselecciona los nodos seleccionados
    function clearSelectedNodes(tree) {
        var selectedNodes = tree.get_selectedNodes()
        for (i = 0; i <= selectedNodes.length - 1; i++) {
            selectedNodes[i].set_selected(false)
        }
    }
    //funcion que expande los nodos padres del nodo 
    function expandNodeParents(node) {
        var parentNodeCollection = new Array();
        var parentNode = node.get_parentNode()
        parentNodeCollection[0] = parentNode
        var i = 1
        while (parentNode.get_parentNode() != null) {
            parentNode = parentNode.get_parentNode()
            parentNodeCollection[i] = parentNode
            i += 1
        }
        for (x = parentNodeCollection.length - 1; x >= 0; x--) {
            parentNodeCollection[x].set_expanded(true)
        }
    }

</script>

<div>
     <asp:Panel ID="pnlTitulo" runat="server" Width="100%">
            <table width="100%" cellpadding="0" border="0">
                <tr>
                    <td>
                        <fsn:FSNPageHeader ID="FSNPageHeader" runat="server">
                        </fsn:FSNPageHeader>
                    </td>
                </tr>
            </table>
        </asp:Panel>

    <table cellspacing="1" cellpadding="3" width="100%" border="0">
    <tr><td colspan="2"><asp:Label ID="lblCentro" runat="server" CssClass="Etiqueta" Text="Centro:"></asp:Label>
        <asp:TextBox ID="txtCentro" runat="server" Width="400px" onfocus="this.value = this.value;"></asp:TextBox></td>
        </tr>
    <tr><td colspan="2">
            <div id="dCamposFiltro" class="dContenedor" style="width:95%; overflow:hidden;border-top: hidden; border-right: hidden; border-bottom: hidden">
                <ig:WebDataTree ID="wdtCentros_Coste" runat="server" Width="95%"  BorderStyle="Solid" BorderColor="Black">
                    <NodeSettings SelectedCssClass="NodeSelected" />
                </ig:WebDataTree>
			</div>
			</td></tr>
    <tr><td align="right" width="50%"><fsn:FSNButton ID="btnSeleccionar" runat="server" Text="Seleccionar" Alineacion="Right" OnClientClick="return seleccionarCentroCoste(); return false;"></fsn:FSNButton></td>
		<td align="left" width="50%"><fsn:FSNButton ID="btnCancelar" runat="server" Text="Cancelar" Alineacion="Left" OnClientClick="window.close()"></fsn:FSNButton></td>
	    <td></td>
	</tr>
    </table>
</div> 