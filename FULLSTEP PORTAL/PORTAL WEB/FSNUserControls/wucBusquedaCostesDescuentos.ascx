﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="wucBusquedaCostesDescuentos.ascx.vb" Inherits="FSNUserControls.wucBusquedaCostesDescuentos" %>

<asp:UpdatePanel ID="UpdBuscadorCostesDescuentos" ChildrenAsTriggers="true" EnableViewState="true" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
    <table width="100%" border="0" >
    <tr id="tipoCD" runat="server" style="display: none"><td colspan="2">
        <table width="90%" border="0">
        <tr><td><asp:Label ID="lblLitCDTipo" runat="server" CssClass="Etiqueta"></asp:Label></td></tr>
        <tr><td><asp:RadioButtonList ID="rblCDTipo" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Selected="True"></asp:ListItem>
                <asp:ListItem style="padding-left: 25px"></asp:ListItem>
                </asp:RadioButtonList></td></tr>
        </table>
        </td>
    </tr>

    <tr><td nowrap="nowrap"><asp:Label ID="lblLitCDFacturasAnteriores" runat="server" CssClass="Etiqueta"></asp:Label></td>
        <td><asp:DropDownList ID="ddlCDFacturas" DataTextField="DEN" DataValueField="DEN" runat="server" Width="300px"></asp:DropDownList></td>
    </tr>
    <tr><td colspan="2">

        <table width="100%" border="0" style="border: 1px solid #cccccc; padding: 5px 10px 5px 10px">
        <tr><td><asp:Label ID="lblCDCodigo" runat="server" CssClass="Etiqueta"></asp:Label></td>
            <td align="left"><asp:TextBox ID="txtCDCodigo" runat="server" Width="250px" MaxLength="200"></asp:TextBox></td>
        </tr>
        <tr><td><asp:Label ID="lblCDDescripcion" runat="server" CssClass="Etiqueta"></asp:Label></td>
            <td align="left"><asp:TextBox ID="txtCDDescripcion" runat="server" Width="250px" MaxLength="200"></asp:TextBox></td>
        </tr>
        <tr><td><asp:Label ID="lblCDMaterial" runat="server" CssClass="Etiqueta"></asp:Label></td>
            <td>
                <table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:TextBox ID="txtCDMaterial" runat="server" Width="235px" BorderWidth="0px"></asp:TextBox>
                        </td>
                        <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                            <asp:ImageButton id="imgCDMaterialLupa" runat="server" SkinID="BuscadorLupa" /> 
                        </td>
                    </tr>                        
                    <asp:HiddenField ID="hidCDMaterial" runat="server" />
                </table>
            </td>
        </tr>
        <tr><td><asp:Label ID="lblCDArticulo" runat="server" CssClass="Etiqueta"></asp:Label></td>
            <td>
                <table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:TextBox ID="txtCDArticulo" runat="server" Width="235px" BorderWidth="0px"></asp:TextBox>
                            <ajx:AutoCompleteExtender ID="txtCDArticulo_AutoCompleteExtender" 
                             runat="server" CompletionSetCount="10" DelimiterCharacters="" Enabled="True"
                             MinimumPrefixLength="1" ServiceMethod="GetArticulosContratos" ServicePath="/App_Pages/_Common/App_Services/AutoComplete.asmx"
                             TargetControlID="txtCDArticulo" EnableCaching="False" ></ajx:AutoCompleteExtender >
                        </td>
                        <td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
                            <asp:ImageButton id="imgCDArticuloLupa" runat="server" SkinID="BuscadorLupa" /> 
                        </td>
                    </tr>                        
                    <asp:HiddenField ID="hidCDArticulo" runat="server" />
                </table>
            </td>
        </tr> 
        <tr><td colspan="2" align="center">
            <table><tr><td><fsn:FSNButton ID="btnCDBuscar" runat="server" Alineacion="Right"></fsn:FSNButton></td>
                    <td><fsn:FSNButton ID="btnCDLimpiar" runat="server" Alineacion="Left"></fsn:FSNButton></td></tr>
            </table>
            </td>
        </tr>
        </table>
    
        <ig:WebHierarchicalDataGrid id="whdgBuscadorCostesDescuentos" Width="100%" Height="200px" runat="server" AutoGenerateColumns="false" ShowHeader="true" EnableAjax="true" EnableDataViewState="true">
        <Columns>
        <ig:BoundDataField Key="COD" DataFieldName="COD" Header-Text="Código"></ig:BoundDataField>
        <ig:BoundDataField Key="DEN" DataFieldName="DEN" Header-Text="Denominación"></ig:BoundDataField>
        <ig:BoundDataField Key="ID" DataFieldName="ID" Hidden="true"></ig:BoundDataField>
        </Columns>
        <Behaviors>
            <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Row" >
                <SelectionClientEvents RowSelectionChanged="WebDataGrid_RowSelectionChanged" />
            </ig:Selection>      
            <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true" AnimationEnabled="true"></ig:Filtering> 
            <ig:VirtualScrolling ScrollingMode="Virtual" Enabled="true"></ig:VirtualScrolling>
            <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting> 
            <ig:RowSelectors Enabled="false" RowNumbering="false"></ig:RowSelectors>
        </Behaviors>
        </ig:WebHierarchicalDataGrid>
        <asp:PlaceHolder ID="phCDGenerico" Visible="false" runat="server">
        <div style="height:20px; margin-top:20px; margin-left:10px"><asp:Label ID="lblCDSinResultado" runat="server" CssClass="Etiqueta"></asp:Label></div>
        <div style="height:20px; margin-top:10px; margin-left:10px"><asp:Label ID="lblLitCDGenerico" runat="server" CssClass="Etiqueta"></asp:Label></div>
        <div style="height:20px; margin-top:0px; margin-left:10px"><asp:TextBox ID="txtCDGenerico" runat="server" Width="250px" MaxLength="200"></asp:TextBox></div>
        </asp:PlaceHolder>
        
    </td></tr>
                    <tr><td colspan="2" align="center">
    <table><tr><td><fsn:FSNButton ID="btnCDAceptar" runat="server" Alineacion="Right"></fsn:FSNButton></td>
            <td><fsn:FSNButton ID="btnCDCancelar" runat="server" Alineacion="Left"></fsn:FSNButton></td></tr></table>
    </td>
</tr>
    </table>
</ContentTemplate>
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="btnCDBuscar" EventName="click" />
    <asp:AsyncPostBackTrigger ControlID="btnCDLimpiar" EventName="click" />
</Triggers>
</asp:UpdatePanel>