﻿Imports Fullstep.FSNLibrary
Public Class wucUsuarios
    Inherits System.Web.UI.UserControl

    Private m_sCualquiera As String
    Private m_sUsuario As String
    Private m_lRol As Integer
    Private m_lInstancia As Integer
    Private _Textos As DataTable
    Private _RestricDEPTraslado As Boolean
    Private _FSQARestNotificadosInternosDep As Boolean
    Private _RestricUOTraslado As Boolean
    Private _FSQARestNotificadosInternosUO As Boolean
    Private _UON1 As String
    Private _UON2 As String
    Private _UON3 As String
    Private _Dep As String
    Private _UnidadesOrgDs As DataSet
    Private _DepartamentosDt As DataTable
    Private _PersonasDs As DataSet

    Public Event eventCargarDepartamentos(ByVal sUon1 As String, ByVal sUon2 As String, ByVal sUon3 As String)
    Public Event eventCargarPersonas(ByVal sNom As String, ByVal sApe As String, ByVal sUon1 As String, ByVal sUon2 As String, ByVal sUon3 As String, ByVal sDep As String)

#Region " PROPIEDADES"
    ''' <summary>
    ''' Propiedad con los textos de la pantalla
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property Textos() As DataTable
        Set(ByVal Value As DataTable)
            _Textos = Value
        End Set
    End Property

    Public Property RestricDEPTraslado As Boolean
        Get
            Return _RestricDEPTraslado
        End Get
        Set(ByVal value As Boolean)
            _RestricDEPTraslado = value
        End Set
    End Property

    Public Property FSQARestNotificadosInternosDep As Boolean
        Get
            Return _FSQARestNotificadosInternosDep
        End Get
        Set(ByVal value As Boolean)
            _FSQARestNotificadosInternosDep = value
        End Set
    End Property

    Public Property RestricUOTraslado As Boolean
        Get
            Return _RestricUOTraslado
        End Get
        Set(ByVal value As Boolean)
            _RestricUOTraslado = value
        End Set
    End Property

    Public Property FSQARestNotificadosInternosUO As Boolean
        Get
            Return _FSQARestNotificadosInternosUO
        End Get
        Set(ByVal value As Boolean)
            _FSQARestNotificadosInternosUO = value
        End Set
    End Property

    Public Property UON1 As String
        Get
            Return _UON1
        End Get
        Set(ByVal value As String)
            _UON1 = value
        End Set
    End Property

    Public Property UON2 As String
        Get
            Return _UON2
        End Get
        Set(ByVal value As String)
            _UON2 = value
        End Set
    End Property

    Public Property UON3 As String
        Get
            Return _UON3
        End Get
        Set(ByVal value As String)
            _UON3 = value
        End Set
    End Property
    Public Property Dep As String
        Get
            Return _Dep
        End Get
        Set(ByVal value As String)
            _Dep = value
        End Set
    End Property
    Public Property Cualquiera As String
        Get
            Return m_sCualquiera
        End Get
        Set(ByVal value As String)
            m_sCualquiera = value
        End Set
    End Property

    Public Property Usuario As String
        Get
            Return m_sUsuario
        End Get
        Set(ByVal value As String)
            m_sUsuario = value
        End Set
    End Property

    Public Property Rol As Integer
        Get
            Return m_lRol
        End Get
        Set(ByVal value As Integer)
            m_lRol = value
        End Set
    End Property

    Public Property Instancia As Integer
        Get
            Return m_lInstancia
        End Get
        Set(ByVal value As Integer)
            m_lInstancia = value
        End Set
    End Property

    Public Property UnidadesOrgDs As DataSet
        Get
            Return _UnidadesOrgDs
        End Get
        Set(ByVal value As DataSet)
            _UnidadesOrgDs = value
        End Set
    End Property

    Public Property DepartamentosDt As DataTable
        Get
            Return _DepartamentosDt
        End Get
        Set(ByVal value As DataTable)
            _DepartamentosDt = value
        End Set
    End Property

    Public Property PersonasDs As DataSet
        Get
            Return _PersonasDs
        End Get
        Set(ByVal value As DataSet)
            _PersonasDs = value
        End Set
    End Property
#End Region


    Protected WithEvents ugtxtUO_wdg As Infragistics.Web.UI.GridControls.WebDataGrid

    ''' <summary>
    ''' Carga la pantalla
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Objeto con los datos del evento</param>
    ''' <remarks>Llamada desde: guardarinstancia.aspx       NWrealizarAccion.aspx      jsAlta.js     altaContrato.aspx   
    ''' DetalleContrato.aspx  altaNoconformidad.aspx   detalleNoConformidad.aspx    BuscadorSolicitudes.aspx; Tiempo máximo:0 </remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        FSNPageHeader.WidthImagenCabecera = "40px"
        FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "/UserControls/images/usuario_buscar.gif"

        CargarTextos()


        'Carga los datos seleccionados:        
        If Not Page.IsPostBack Then
            txtApe.Text = Request("APE")
            txtNombre.Text = Request("NOMBRE")
            Buscar()
            UP_uwgUsuarios.Update()
        End If
        
    End Sub

    ''' <summary>
    ''' Establece los textos de los controles de acuerdo con el idioma del usuario
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:0</remarks> 
    Private Sub CargarTextos()
        If Request("desde") = "QA" Then
            FSNPageHeader.TituloCabecera = _Textos(13)(1)
        ElseIf Request("IDControl") <> Nothing Then
            FSNPageHeader.TituloCabecera = _Textos(11)(1)
        ElseIf Request("idFilaGrid") <> Nothing Then
            FSNPageHeader.TituloCabecera = _Textos(11)(1)
        Else
            FSNPageHeader.TituloCabecera = _Textos(0)(1)
        End If

        Me.lblUO.Text = String.Format("{0}:", _Textos(1)(1))
        Me.lblNombre.Text = String.Format("{0}:", _Textos(2)(1))
        Me.lblApe.Text = String.Format("{0}:", _Textos(3)(1))
        Me.lblDep.Text = String.Format("{0}:", _Textos(4)(1))
        Me.cmdBuscar.Text = _Textos(5)(1)
        Me.lblResult.Text = _Textos(6)(1)
        Me.lblPulsar.Text = _Textos(7)(1)
        m_sCualquiera = _Textos(8)(1)
        m_sUsuario = _Textos(9)(1)
    End Sub

    ''' <summary>
    ''' Carga los departamentos en función de la unidad organizativa seleccionada
    ''' </summary>
    ''' <param name="UON1">Cod de la unidad organizativa de nivel 1</param>
    ''' <param name="UON2">Cod de la unidad organizativa de nivel 2</param>
    ''' <param name="UON3">Cod de la unidad organizativa de nivel 3</param>
    ''' <remarks>LLamada desde: Page_Load; Tiempo máximo: 0,2 sg</remarks>
    Public Sub CargarDepartamentos(Optional ByVal UON1 As String = Nothing, Optional ByVal UON2 As String = Nothing, Optional ByVal UON3 As String = Nothing)


        ugtxtDEP.TextField = "DEN"
        ugtxtDEP.ValueField = "COD"
        ugtxtDEP.NullText = ""
        ugtxtDEP.DataSource = _DepartamentosDt
        ugtxtDEP.DataBind()

        ugtxtDEP.SelectedItemIndex = -1

        For Each oItem As Infragistics.Web.UI.ListControls.DropDownItem In ugtxtDEP.Items
            oItem.CssClass = "igdd_FullstepListItemB"
        Next


    End Sub

    Public Sub CargarUnidadesOrganizativas()
        Dim ugtxtUO_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = ugtxtUO_.Controls(0).Controls(0).FindControl("ugtxtUO_wdg")
        ugtxtUO_wdg.DataSource = _UnidadesOrgDs
        ugtxtUO_wdg.DataBind()
        ugtxtUO_wdg.Columns("COD_UON1").Hidden = True
        ugtxtUO_wdg.Columns("COD_UON2").Hidden = True
        ugtxtUO_wdg.Columns("COD_UON3").Hidden = True
        ugtxtUO_.TextField = "DEN"


        Dim SelectedRowIndex As Integer = IIf(Request("UO") = String.Empty, 0, CInt(Request("UO")))
        ugtxtUO_wdg.Behaviors.Selection.SelectedRows.Add(ugtxtUO_wdg.Rows(SelectedRowIndex))
        ugtxtUO_.Items(0).Text = DBNullToStr(ugtxtUO_wdg.Rows(SelectedRowIndex).Items(3).Value)
        ugtxtUO_.CurrentValue = DBNullToStr(ugtxtUO_wdg.Rows(SelectedRowIndex).Items(3).Text)

        'Carga la combo de departamentos
        If ugtxtUO_wdg.Behaviors.Selection.SelectedRows.Count > 0 Then
            With ugtxtUO_wdg.Behaviors.Selection.SelectedRows(0)
                Dim sUON1 As String = IIf(.Items(0).Value Is System.DBNull.Value, Nothing, .Items(0).Value)
                Dim sUON2 As String = IIf(.Items(1).Value Is System.DBNull.Value, Nothing, .Items(1).Value)
                Dim sUON3 As String = IIf(.Items(2).Value Is System.DBNull.Value, Nothing, .Items(2).Value)

                RaiseEvent eventCargarDepartamentos(sUON1, sUON2, sUON3)
                CargarDepartamentos()
            End With
            If Request("DEP") <> Nothing And Request("DEP") <> "" Then
                ugtxtDEP.SelectedItemIndex = Request("DEP")
                If Not ugtxtDEP.SelectedItem Is Nothing Then ugtxtDEP.CurrentValue = ugtxtDEP.SelectedItem.Text
            End If
            If ugtxtDEP.SelectedItem IsNot Nothing Then
                ugtxtDEP.SelectedItem.CssClass = "igdd_FullstepListItemB igdd_FullstepListItemSelected"
            End If
        End If
    End Sub

    ''' <summary>
    ''' Carga en el grid los usuarios que cumplen las condiciones señaladas en los combo superiores
    ''' dependiendo de las restricciones que tiene el usuario que lanza la busqueda
    ''' </summary>
    ''' <remarks></remarks>    
    Private Sub Buscar()
        

        Dim ugtxtUO_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = ugtxtUO_.Controls(0).Controls(0).FindControl("ugtxtUO_wdg")
        If ugtxtUO_wdg.Behaviors.Selection.SelectedRows.Count > 0 Then
            With ugtxtUO_wdg.Behaviors.Selection.SelectedRows(0)
                _UON1 = IIf(.Items(0).Value Is System.DBNull.Value, Nothing, .Items(0).Value)
                _UON2 = IIf(.Items(1).Value Is System.DBNull.Value, Nothing, .Items(1).Value)
                _UON3 = IIf(.Items(2).Value Is System.DBNull.Value, Nothing, .Items(2).Value)
            End With
        End If

        If ugtxtDEP.ActiveItemIndex >= 0 Then
            If Not ugtxtDEP.Items(ugtxtDEP.ActiveItemIndex) Is Nothing Then
                If ugtxtDEP.Items(ugtxtDEP.ActiveItemIndex).Text <> m_sCualquiera Then
                    _Dep = ugtxtDEP.Items(ugtxtDEP.ActiveItemIndex).Value
                End If
            End If
        End If

        If ugtxtDEP.Items.Count = 0 Then
            _UON1 = ""
            _UON2 = ""
            _UON3 = ""
            _Dep = ""
        End If


        RaiseEvent eventCargarPersonas(IIf(txtNombre.Text = "", Nothing, txtNombre.Text), IIf(txtApe.Text = "", Nothing, txtApe.Text), _UON1, _UON2, _UON3, _Dep)


        wdg_Usuarios.DataSource = _PersonasDs
        wdg_Usuarios.DataBind()

    End Sub

    Private Sub cmdBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBuscar.Click
        Buscar()
        UP_uwgUsuarios.Update()
    End Sub

    ''' <summary>
    ''' Método que se lanza con la llamada a la función loadItems desde javascript. 
    ''' </summary>
    ''' <param name="sender">Control que lanza el evento con la llamada a su método loadItems en javascript</param>
    ''' <param name="e">parámetros del evento</param>
    ''' <remarks>Llamada desde usuario.aspx. Tiempo máximo inferior a 0,2 seg</remarks>
    Private Sub ugtxtDEP_ItemsRequested(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownItemsRequestedEventArgs) Handles ugtxtDEP.ItemsRequested
        Dim aUONS As String() = Split(e.Value, "@@")
        Dim sUON1 As String = IIf(aUONS(0) <> String.Empty, aUONS(0), Nothing)
        Dim sUON2 As String = IIf(aUONS(1) <> String.Empty, aUONS(1), Nothing)
        Dim sUON3 As String = IIf(aUONS(2) <> String.Empty, aUONS(2), Nothing)


        CargarDepartamentos(sUON1, sUON2, sUON3)

    End Sub

    ''' <summary>
    ''' Método que se lanza con el evento RowSelectionChanged del control
    ''' </summary>
    ''' <param name="sender">Control que lanza el evento</param>
    ''' <param name="e">parámetros del evento</param>
    ''' <remarks>Llamada desde usuario.aspx. Tiempo máximo inferior a 0,2 seg</remarks>
    Public Sub ugtxtUO_wdg_RowSelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.SelectedRowEventArgs) Handles ugtxtUO_wdg.RowSelectionChanged
        Dim oSelectedRow As Infragistics.Web.UI.GridControls.GridRecord = CType(sender, Infragistics.Web.UI.GridControls.WebDataGrid).Behaviors.Selection.SelectedRows(0)
        Dim sUON1 As String = IIf(null2str(oSelectedRow.Items(0).Value) <> String.Empty, oSelectedRow.Items(0).Value, Nothing)
        Dim sUON2 As String = IIf(null2str(oSelectedRow.Items(1).Value) <> String.Empty, oSelectedRow.Items(1).Value, Nothing)
        Dim sUON3 As String = IIf(null2str(oSelectedRow.Items(2).Value) <> String.Empty, oSelectedRow.Items(2).Value, Nothing)

        If sUON1 <> Session("UON1") Or sUON2 <> Session("UON2") Or sUON3 <> Session("UON3") Then

            Session("UON1") = sUON1
            Session("UON2") = sUON2
            Session("UON3") = sUON3

            RaiseEvent eventCargarDepartamentos(sUON1, sUON2, sUON3)
            CargarDepartamentos(sUON1, sUON2, sUON3)

            Me.UP_Uon.Update()
        End If
    End Sub

    Private Sub wdg_Usuarios_InitializeBand(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.BandEventArgs) Handles wdg_Usuarios.InitializeBand
        DirectCast(sender, Infragistics.Web.UI.GridControls.WebHierarchicalDataGrid).Columns("USUARIO").Header.Text = "Usuario"
        DirectCast(sender, Infragistics.Web.UI.GridControls.WebHierarchicalDataGrid).Columns("NOM_DEP").Header.Text = "Departamento"
        DirectCast(sender, Infragistics.Web.UI.GridControls.WebHierarchicalDataGrid).Columns("NOM_DEP").Width = Unit.Percentage(35)
    End Sub

    Private Sub wdg_Usuarios_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdg_Usuarios.InitializeRow
        e.Row.Items.FindItemByKey("USUARIO").Value = e.Row.Items.FindItemByKey("NOM").Value & " " & e.Row.Items.FindItemByKey("APE").Value
    End Sub
End Class