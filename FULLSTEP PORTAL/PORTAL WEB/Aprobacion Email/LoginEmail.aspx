﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/master/CabeceraLoginMail.Master" CodeBehind="LoginEmail.aspx.vb" Inherits="AprobacionEmail.LoginEmail"  %>
<%@ MasterType VirtualPath="~/master/CabeceraLoginMail.Master" %>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="divContenedor" style="width:100%; height:84%;">
        <div id="divCabecera" style="float:left; width:100%; height:20%;"></div>
        <div style="float:left; width:20%; height:40%"></div>
        <div class="CapaCentroLogin">
            <asp:Panel ID="Panel1" runat="server" style="width: 80%; height: 100%; padding: 5px; display:table" class="Rectangulo" >
                    <div id="divRectangulo" style="display:table-cell; vertical-align:middle">
                        <table  style="width: 100%;" cellpadding="0" cellspacing="10" border="0" id="TablaAutenticacion" runat="server">
                        <tr><td align="center" colspan="2">
                            <asp:Label ID="lblInfoAcceso" runat="server" Text="DPara validar la aprobación introduzca su contraseña" 
                            CssClass="Rotulo"></asp:Label></td></tr>
                        <tr><td align="center" colspan="2">
                            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="CajaTextoLogin" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                ControlToValidate="txtPassword" CssClass="Rotulo" ErrorMessage="*" ForeColor=""></asp:RequiredFieldValidator>
                        </td></tr>
                        <tr><td align="right">
                                <input id="btnValidar" type="button" class="boton" value="DValidar" runat="server" />
                            </td>
                            <td align="left">
                                <input id="btnCancelar" type="button" class="boton" onclick="javascript:window.top.close(); return false;" value="DCancelar" runat="server" />
                            </td>
                            </tr>
                        </table>
                    </div>
                    <div id="divError" style=" display:table-row; vertical-align:middle; text-align:center">
                        <asp:Label ID="lblError" runat="server" 
                        Text="DNo es posible iniciar la sesión. El usuario introducido no es correcto. Por favor, inténtelo de nuevo." 
                        CssClass="MensajeError" Visible="False"></asp:Label>
                    </div>
                    <div style="clear:both; text-align:left; display:table-row;">
                        <span id="txtInfoIntentos" runat="server" visible="false" style="text-align:left; margin-left:3em; margin-top:2em; display:inline-block;" class="Rotulo">DLe quedan X intentos, después se bloqueará la cuenta.</span>
                    </div>
                    <div style="clear:both; text-align:left; display:table-row;">
                        <span id="txtCuentaBloqueada" runat="server" visible="false" style="text-align:left; margin-top:1em; margin-left:3em; display:inline-block;" class="Rotulo">DCuenta de usuario bloqueada.</span>
                    </div>
                    <div style="clear:both; text-align:left; display:table-row;">
                        <span id="txtContactoAdmin" runat="server" visible="false" style="text-align:left; margin-left:3em; display:inline-block;" class="Rotulo">DPor favor, póngase en contacto con el administrador.</span>
                    </div>
                </asp:Panel>
                
        </div>
        <div style="float:left;width:20%;height:40%; "></div>
        <div id="divPie" class="PieLogin""></div>
    </div>
    
</asp:Content>



