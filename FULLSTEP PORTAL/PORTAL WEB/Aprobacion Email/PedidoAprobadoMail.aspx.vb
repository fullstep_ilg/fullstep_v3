﻿Public Class PedidoAprobadoMail
    Inherits FSNPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.ModuloIdioma = FSNLibrary.TiposDeDatos.ModulosIdiomas.Login
        lblTexto.Text = Server.UrlDecode(Request("Texto"))
        linkCerrar.InnerText = Me.Textos(11)
    End Sub

End Class