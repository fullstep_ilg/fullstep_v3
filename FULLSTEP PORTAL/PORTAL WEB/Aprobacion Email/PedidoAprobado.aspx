﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PedidoAprobado.aspx.vb" Inherits="AprobacionEmail.PedidoAprobado" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />
<title>FULLSTEP NETWORKS</title>
<link rel="STYLESHEET" type="text/css" href="http://demo.fullstep.net/templates/css/plantilla.css"/>
<style type="text/css">
    .Capa
    {
        text-align:center;
    }
</style>
</head>

<body style=" margin:0 0 0 0; " background:"http://demo.fullstep.net/templates/img/fondo_cuadros.gif">

<form id="form1" runat="server">

<!-- #### TABLA TOTAL #### -->

<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
    <td align="center" valign="top">


<!-- #### TABLA PAGINA #### -->
	
	<table width="770" cellspacing="0" cellpadding="0" border="0" height="100%">
	<tr>
	    <td align="center" valign="top" class="tdblanco">

		<!-- tira superior -->
		
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="tdrojo">
		<tr>
		    <td width="577"><a href="http://www.fullstep.com" onfocus="blur()"><asp:Image ID="imgLogo" runat="server" ImageAlign="Middle" SkinID="LogoCab" /></a></td>
		    <td width="193" class="ver14blanco"><strong></strong><img 

src="http://demo.fullstep.net/templates/img/dot_no.gif" width="45" height="1" alt="" border="0"></td>
		</tr>
		<tr>
			<td colspan="2" background="http://demo.fullstep.net/templates/img/fondo_rayas.gif"><img 

src="../img/dot_no.gif" width="770" height="5" alt="" border="0"></td>
		</tr>
		</table>
		
		<!-- fin tira superior -->

		
		<table width="600" cellspacing="0" cellpadding="0" border="0">
		<tr>
		    <td height="80" class="ver11negro">
                <div style="text-align:center; margin:10px"><asp:Label ID="lblTexto" runat="server">DLe comunicamos que se ha procesado correctamente la aprobación 
            del pedido.</asp:Label></div><div class="Capa"><a id="linkCerrar" href="#" runat="server" style="cursor:pointer"  onclick="javascript:window.top.close(); return false;">Cerrar</a></div>
            </td>
		</tr>
		</table>
	</tr>
	</table>
     </td>
</table>
</form>
</body> 
</html>