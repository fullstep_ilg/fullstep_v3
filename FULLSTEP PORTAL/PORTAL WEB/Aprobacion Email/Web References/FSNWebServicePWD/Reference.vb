﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Xml.Serialization

'
'This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
'
Namespace FSNWebServicePWD
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1055.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Web.Services.WebServiceBindingAttribute(Name:="CambioPWDSoap", [Namespace]:="http://tempuri.org/FSNWebServicePWD/CambioPWD")>  _
    Partial Public Class CambioPWD
        Inherits System.Web.Services.Protocols.SoapHttpClientProtocol
        
        Private CambiarLoginOperationCompleted As System.Threading.SendOrPostCallback
        
        Private LoginLDAPOperationCompleted As System.Threading.SendOrPostCallback
        
        Private useDefaultCredentialsSetExplicitly As Boolean
        
        '''<remarks/>
        Public Sub New()
            MyBase.New
            Me.Url = Global.AprobacionEmail.My.MySettings.Default.AprobacionEmail_FSNWebServicePWD_CambioPWD
            If (Me.IsLocalFileSystemWebService(Me.Url) = true) Then
                Me.UseDefaultCredentials = true
                Me.useDefaultCredentialsSetExplicitly = false
            Else
                Me.useDefaultCredentialsSetExplicitly = true
            End If
        End Sub
        
        Public Shadows Property Url() As String
            Get
                Return MyBase.Url
            End Get
            Set
                If (((Me.IsLocalFileSystemWebService(MyBase.Url) = true)  _
                            AndAlso (Me.useDefaultCredentialsSetExplicitly = false))  _
                            AndAlso (Me.IsLocalFileSystemWebService(value) = false)) Then
                    MyBase.UseDefaultCredentials = false
                End If
                MyBase.Url = value
            End Set
        End Property
        
        Public Shadows Property UseDefaultCredentials() As Boolean
            Get
                Return MyBase.UseDefaultCredentials
            End Get
            Set
                MyBase.UseDefaultCredentials = value
                Me.useDefaultCredentialsSetExplicitly = true
            End Set
        End Property
        
        '''<remarks/>
        Public Event CambiarLoginCompleted As CambiarLoginCompletedEventHandler
        
        '''<remarks/>
        Public Event LoginLDAPCompleted As LoginLDAPCompletedEventHandler
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/FSNWebServicePWD/CambioPWD/CambiarLogin", RequestNamespace:="http://tempuri.org/FSNWebServicePWD/CambioPWD", ResponseNamespace:="http://tempuri.org/FSNWebServicePWD/CambioPWD", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function CambiarLogin( _
                    ByVal adm As Integer,  _
                    ByVal usu As String,  _
                    ByVal PWDActual As String,  _
                    ByVal usuNuevo As String,  _
                    ByVal PWDNueva As String,  _
                    ByVal FechaPWD_year As Integer,  _
                    ByVal FechaPWD_month As Integer,  _
                    ByVal FechaPWD_day As Integer,  _
                    ByVal FechaPWD_hour As Integer,  _
                    ByVal FechaPWD_min As Integer,  _
                    ByVal FechaPWD_sec As Integer,  _
                    ByVal iChange As Integer,  _
                    ByVal Mail As String,  _
                    ByVal FechaAnteriorPWd_year As Integer,  _
                    ByVal FechaAnteriorPWd_month As Integer,  _
                    ByVal FechaAnteriorPWd_day As Integer,  _
                    ByVal FechaAnteriorPWd_hour As Integer,  _
                    ByVal FechaAnteriorPWd_min As Integer,  _
                    ByVal FechaAnteriorPWd_sec As Integer) As Integer
            Dim results() As Object = Me.Invoke("CambiarLogin", New Object() {adm, usu, PWDActual, usuNuevo, PWDNueva, FechaPWD_year, FechaPWD_month, FechaPWD_day, FechaPWD_hour, FechaPWD_min, FechaPWD_sec, iChange, Mail, FechaAnteriorPWd_year, FechaAnteriorPWd_month, FechaAnteriorPWd_day, FechaAnteriorPWd_hour, FechaAnteriorPWd_min, FechaAnteriorPWd_sec})
            Return CType(results(0),Integer)
        End Function
        
        '''<remarks/>
        Public Overloads Sub CambiarLoginAsync( _
                    ByVal adm As Integer,  _
                    ByVal usu As String,  _
                    ByVal PWDActual As String,  _
                    ByVal usuNuevo As String,  _
                    ByVal PWDNueva As String,  _
                    ByVal FechaPWD_year As Integer,  _
                    ByVal FechaPWD_month As Integer,  _
                    ByVal FechaPWD_day As Integer,  _
                    ByVal FechaPWD_hour As Integer,  _
                    ByVal FechaPWD_min As Integer,  _
                    ByVal FechaPWD_sec As Integer,  _
                    ByVal iChange As Integer,  _
                    ByVal Mail As String,  _
                    ByVal FechaAnteriorPWd_year As Integer,  _
                    ByVal FechaAnteriorPWd_month As Integer,  _
                    ByVal FechaAnteriorPWd_day As Integer,  _
                    ByVal FechaAnteriorPWd_hour As Integer,  _
                    ByVal FechaAnteriorPWd_min As Integer,  _
                    ByVal FechaAnteriorPWd_sec As Integer)
            Me.CambiarLoginAsync(adm, usu, PWDActual, usuNuevo, PWDNueva, FechaPWD_year, FechaPWD_month, FechaPWD_day, FechaPWD_hour, FechaPWD_min, FechaPWD_sec, iChange, Mail, FechaAnteriorPWd_year, FechaAnteriorPWd_month, FechaAnteriorPWd_day, FechaAnteriorPWd_hour, FechaAnteriorPWd_min, FechaAnteriorPWd_sec, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub CambiarLoginAsync( _
                    ByVal adm As Integer,  _
                    ByVal usu As String,  _
                    ByVal PWDActual As String,  _
                    ByVal usuNuevo As String,  _
                    ByVal PWDNueva As String,  _
                    ByVal FechaPWD_year As Integer,  _
                    ByVal FechaPWD_month As Integer,  _
                    ByVal FechaPWD_day As Integer,  _
                    ByVal FechaPWD_hour As Integer,  _
                    ByVal FechaPWD_min As Integer,  _
                    ByVal FechaPWD_sec As Integer,  _
                    ByVal iChange As Integer,  _
                    ByVal Mail As String,  _
                    ByVal FechaAnteriorPWd_year As Integer,  _
                    ByVal FechaAnteriorPWd_month As Integer,  _
                    ByVal FechaAnteriorPWd_day As Integer,  _
                    ByVal FechaAnteriorPWd_hour As Integer,  _
                    ByVal FechaAnteriorPWd_min As Integer,  _
                    ByVal FechaAnteriorPWd_sec As Integer,  _
                    ByVal userState As Object)
            If (Me.CambiarLoginOperationCompleted Is Nothing) Then
                Me.CambiarLoginOperationCompleted = AddressOf Me.OnCambiarLoginOperationCompleted
            End If
            Me.InvokeAsync("CambiarLogin", New Object() {adm, usu, PWDActual, usuNuevo, PWDNueva, FechaPWD_year, FechaPWD_month, FechaPWD_day, FechaPWD_hour, FechaPWD_min, FechaPWD_sec, iChange, Mail, FechaAnteriorPWd_year, FechaAnteriorPWd_month, FechaAnteriorPWd_day, FechaAnteriorPWd_hour, FechaAnteriorPWd_min, FechaAnteriorPWd_sec}, Me.CambiarLoginOperationCompleted, userState)
        End Sub
        
        Private Sub OnCambiarLoginOperationCompleted(ByVal arg As Object)
            If (Not (Me.CambiarLoginCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent CambiarLoginCompleted(Me, New CambiarLoginCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/FSNWebServicePWD/CambioPWD/LoginLDAP", RequestNamespace:="http://tempuri.org/FSNWebServicePWD/CambioPWD", ResponseNamespace:="http://tempuri.org/FSNWebServicePWD/CambioPWD", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function LoginLDAP(ByVal UserName As String, ByVal UserPassword As String) As Integer
            Dim results() As Object = Me.Invoke("LoginLDAP", New Object() {UserName, UserPassword})
            Return CType(results(0),Integer)
        End Function
        
        '''<remarks/>
        Public Overloads Sub LoginLDAPAsync(ByVal UserName As String, ByVal UserPassword As String)
            Me.LoginLDAPAsync(UserName, UserPassword, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub LoginLDAPAsync(ByVal UserName As String, ByVal UserPassword As String, ByVal userState As Object)
            If (Me.LoginLDAPOperationCompleted Is Nothing) Then
                Me.LoginLDAPOperationCompleted = AddressOf Me.OnLoginLDAPOperationCompleted
            End If
            Me.InvokeAsync("LoginLDAP", New Object() {UserName, UserPassword}, Me.LoginLDAPOperationCompleted, userState)
        End Sub
        
        Private Sub OnLoginLDAPOperationCompleted(ByVal arg As Object)
            If (Not (Me.LoginLDAPCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent LoginLDAPCompleted(Me, New LoginLDAPCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        Public Shadows Sub CancelAsync(ByVal userState As Object)
            MyBase.CancelAsync(userState)
        End Sub
        
        Private Function IsLocalFileSystemWebService(ByVal url As String) As Boolean
            If ((url Is Nothing)  _
                        OrElse (url Is String.Empty)) Then
                Return false
            End If
            Dim wsUri As System.Uri = New System.Uri(url)
            If ((wsUri.Port >= 1024)  _
                        AndAlso (String.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) = 0)) Then
                Return true
            End If
            Return false
        End Function
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1055.0")>  _
    Public Delegate Sub CambiarLoginCompletedEventHandler(ByVal sender As Object, ByVal e As CambiarLoginCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1055.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class CambiarLoginCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As Integer
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),Integer)
            End Get
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1055.0")>  _
    Public Delegate Sub LoginLDAPCompletedEventHandler(ByVal sender As Object, ByVal e As LoginLDAPCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.1055.0"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class LoginLDAPCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As Integer
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),Integer)
            End Get
        End Property
    End Class
End Namespace
