﻿Imports System.Data
Imports Fullstep.FSNLibrary
Imports Fullstep.FSNServer

Public Class FSNPage
    Inherits System.Web.UI.Page
    Implements IFSNPage

    Private _FSNUser As FSNServer.User
    Private _FSNAcceso As FSNLibrary.TiposDeDatos.ParametrosGenerales
    Private _FSNServer As FSNServer.Root
    Private _Idioma As String
    Private _ModuloIdioma As FSNLibrary.TiposDeDatos.ModulosIdiomas

    Public ReadOnly Property FSNUser() As FSNServer.User
        Get
            If _FSNUser Is Nothing Then
                _FSNUser = Session("FSN_User")
            End If

            Return _FSNUser
        End Get
    End Property

    Public ReadOnly Property Usuario() As FSNServer.User Implements IFSNPage.Usuario
        Get
            Return CType(Me.FSNUser, FSNServer.User)
        End Get
    End Property

    Public ReadOnly Property Acceso() As FSNLibrary.TiposDeDatos.ParametrosGenerales Implements IFSNPage.Acceso
        Get
            Return Me.FSNServer.TipoAcceso
        End Get
    End Property

    Public ReadOnly Property FSNServer() As FSNServer.Root Implements IFSNPage.FSNServer
        Get
            If _FSNServer Is Nothing Then
                _FSNServer = Session("FSN_Server")
            End If
            Return _FSNServer
        End Get
    End Property

    Public ReadOnly Property Idioma() As String Implements IFSNPage.Idioma
        Get
            If _Idioma Is Nothing Then
                If Not FSNUser Is Nothing Then
                    _Idioma = FSNUser.Idioma
                End If
                If _Idioma Is Nothing Then
                    If Not Request.QueryString("Idioma") Is Nothing Then
                        _Idioma = Request.QueryString("Idioma")
                    End If
                End If
                If _Idioma Is Nothing Then
                    _Idioma = System.Configuration.ConfigurationManager.AppSettings("idioma")
                End If
            End If
            Return _Idioma
        End Get
    End Property

    Public Property ModuloIdioma() As FSNLibrary.TiposDeDatos.ModulosIdiomas Implements IFSNPage.ModuloIdioma
        Get
            Return _ModuloIdioma
        End Get
        Set(ByVal Value As FSNLibrary.TiposDeDatos.ModulosIdiomas)
            _ModuloIdioma = Value
        End Set
    End Property

    Public ReadOnly Property Textos(ByVal iTexto As Integer) As String Implements IFSNPage.Textos
        Get
            If HttpContext.Current.Cache("Textos_" & Me.Idioma & "_" & _ModuloIdioma) Is Nothing Then
                Dim FSNDict As FSNServer.Dictionary
                If Me.FSNServer Is Nothing Then
                    FSNDict = New FSNServer.Dictionary()
                    FSNDict.LoadData(_ModuloIdioma, Me.Idioma)
                Else
                    FSNDict = Me.FSNServer.Get_Object(GetType(FSNServer.Dictionary))
                    FSNDict.LoadData(_ModuloIdioma, Me.Idioma)
                End If
                HttpContext.Current.Cache.Insert("Textos_" & Me.Idioma & "_" & _ModuloIdioma, FSNDict.Data, Nothing, DateTime.Now.AddMinutes(10), TimeSpan.Zero)
            End If
            Return HttpContext.Current.Cache("Textos_" & Me.Idioma & "_" & _ModuloIdioma).Tables(0).Rows(iTexto).Item(1)
        End Get
    End Property

    Public WriteOnly Property Seccion() As String
        Set(ByVal value As String)
            Dim webService As New FSNSessionService.SessionService
            webService.SetSessionData(Session("sessionId"), "Seccion", value)
        End Set
    End Property

    Public ReadOnly Property ScriptMgr() As System.Web.UI.ScriptManager Implements IFSNPage.ScriptMgr
        Get
            Return ScriptManager.GetCurrent(Me.Page)
        End Get
    End Property

    ''' <summary>
    ''' Realiza el login si el usuario no está autenticado
    ''' </summary>
    ''' <param name="sSessionID">ID de la sesión</param>
    Public Sub Login(ByVal sSessionID As String)
        Dim webService As New FSNSessionService.SessionService
        Dim Username As String = webService.GetSessionData(sSessionID, "Usuario")
        'Si finaliza la Session del SessionService
        If Username Is Nothing Then
            Response.Redirect("default.aspx")
            Response.End()
        End If
        'Si No hay Session o la Session que hay es de otro usuario distinto al del SessionService
        If FSNUser Is Nothing OrElse Username <> FSNUser.Cod Then
            Session.Clear()
        End If
        Session("sSession") = sSessionID
        If FSNUser Is Nothing OrElse Username <> FSNUser.Cod OrElse HttpContext.Current.User.Identity.Name = "" Then
            _FSNServer = New FSNServer.Root
            Dim Password As String = String.Empty
            'Dim oUser As FSNServer.User
            Select Case _FSNServer.TipoDeAutenticacion
                Case TiposDeAutenticacion.Windows, TiposDeAutenticacion.LDAP
                    _FSNUser = _FSNServer.Login(Username)
                Case Else
                    Password = webService.GetSessionData(sSessionID, "Pwd")
                    _FSNUser = _FSNServer.Login(Username, Password, False)
            End Select

            If _FSNUser Is Nothing Then
                Response.Redirect("default.aspx")
                Response.End()
            Else

                _FSNUser.AccesoCN = FSNServer.TipoAcceso.gbAccesoFSCN
                _FSNUser.LoadUserData(Username)
                If _FSNUser.AccesoPM Or _FSNUser.AccesoQA Or _FSNUser.AccesoEP Or _FSNUser.AccesoSM Then
                    If _FSNUser.AccesoQA Then
                        If Application("Nivel_UnQa") Is Nothing Then
                            _FSNServer.Load_UnQa()
                            Application("Nivel_UnQa") = FSNServer.NivelesUnQa
                        End If
                    End If
                    Dim sIdioma As String = _FSNUser.Idioma
                    If _FSNUser.Idioma = "" Then
                        _FSNUser.Idioma = ConfigurationManager.AppSettings("Idioma")
                    End If
                    _FSNServer.Load_LongitudesDeCodigos()
                    Session("FSN_User") = _FSNUser
                    Session("FSN_Server") = _FSNServer

                Else
                    Response.Redirect("default.aspx")
                End If

                HttpContext.Current.User = Session("FSN_User")

                'A falta de más investigación esto es necesario para los WebPart
                ' Crear cookie de autenticaciÃƒÂ³n de usuario
                Dim ticket As FormsAuthenticationTicket = New FormsAuthenticationTicket(1, _
                    FSNUser.Cod, _
                    DateTime.Now, _
                    DateTime.Now.AddMinutes(Session.Timeout), _
                    False, _
                    FSNUser.Cod, _
                    FormsAuthentication.FormsCookiePath)
                ' Encrypt the ticket.
                Dim encTicket As String = FormsAuthentication.Encrypt(ticket)
                ' Create the cookie.
                Response.Cookies.Add(New HttpCookie(FormsAuthentication.FormsCookieName, encTicket))

            End If
        End If
    End Sub

    ''' <summary>
    ''' Inicializa la sesión si recibe el parámetro SessionId. 
    ''' Si no hay sesión iniciada redirige a la página de Login.
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce al principio de la inicialización de la página.</remarks>
    Private Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Not TypeOf Me.Master Is CabeceraLoginMail And Not TypeOf Me Is PedidoAprobadoMail Then
            If Not IsPostBack AndAlso Request("SessionId") IsNot Nothing Then
                Login(Request("SessionId"))
            ElseIf HttpContext.Current.User.Identity.Name = "" AndAlso Request("SessionId") IsNot Nothing Then
                Login(Request("SessionId"))
            End If

            If Usuario Is Nothing Then
                Response.Redirect("default.aspx")
            End If
        ElseIf Not IsPostBack() Then
            Session.Abandon()
            Session.Clear()
        End If
    End Sub


    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim sw As IO.StringWriter = New IO.StringWriter
        Dim htmlWriter As HtmlTextWriter = New HtmlTextWriter(sw)
        MyBase.Render(htmlWriter)
        Dim html As String = sw.ToString
        Dim startPoint As Integer = html.IndexOf("<input type=""hidden"" name=""__VIEWSTATE""")
        If (startPoint >= 0) Then
            Dim endPoint As Integer = (html.IndexOf("/>", startPoint) + 2)
            Dim viewStateInput As String = html.Substring(startPoint, (endPoint - startPoint))
            html = html.Remove(startPoint, (endPoint - startPoint))
            Dim formEndStart As Integer = html.IndexOf("</form>")
            If (formEndStart >= 0) Then
                html = html.Insert(formEndStart, String.Format("{0}", viewStateInput))
            End If
        End If
        writer.Write(html)
    End Sub

End Class
