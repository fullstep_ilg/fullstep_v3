﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Threading

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class FSAL_RegistrarAccesos
    Inherits System.Web.Services.WebService
    Private m_FSAL As Fullstep.PMPortalServer.FSAL
    <WebMethod()> _
    Public Sub FSALRegistrarAccesos(Tipo As Int16, Producto As String, fechapet As String, fecha As String, ByVal pagina As String, ByVal iPost As Int16, ByVal iP As String, ByVal sUsuCod As String, ByVal sPaginaOrigen As String, ByVal sNavegador As String)
        m_FSAL = New Fullstep.PMPortalServer.FSAL
        m_FSAL.RegistrarAcceso(Tipo, Producto, fechapet, fecha, pagina, iPost, iP, sUsuCod, sPaginaOrigen, sNavegador)
    End Sub
    <WebMethod()> _
    Public Sub FSALActualizarAccesos(Tipo As Int16, fechapet As String)
        m_FSAL = New Fullstep.PMPortalServer.FSAL
        Dim fecha As DateTime = DateTime.UtcNow
        Dim ms As Integer = fecha.Millisecond
        Dim sfecha As String = Format(fecha, "yyyy-MM-dd HH:mm:ss") & "." & ms
        m_FSAL = m_FSAL.ActualizarAcceso(Tipo, fechapet, sfecha)
    End Sub

End Class