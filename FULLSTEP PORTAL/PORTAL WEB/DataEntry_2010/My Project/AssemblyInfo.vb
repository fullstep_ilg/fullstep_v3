﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Web.UI

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("DataEntry")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("FULLSTEP")> 
<Assembly: AssemblyProduct("DataEntry")>
<Assembly: AssemblyCopyright("Copyright © FULLSTEP 2017")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("fb91cb94-bbfc-41b4-a8a0-ae15d5b881aa")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.*")>
<Assembly: AssemblyFileVersion("3.6.1")>
<Assembly: TagPrefix("Fullstep.DataEntry", "fsde")> 

<Assembly: WebResource("Fullstep.DataEntry.Buscador.png", "image/png")> 
<Assembly: WebResource("Fullstep.DataEntry.trespuntos.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.DataEntry.calendar-color.png", "image/png")> 
<Assembly: WebResource("Fullstep.DataEntry.edit2.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.DataEntry.factura.png", "image/png")> 
<Assembly: WebResource("Fullstep.DataEntry.ig_cmboDownXP1.bmp", "image/bmp")> 
<Assembly: WebResource("Fullstep.DataEntry.info.gif", "image/gif")> 
<Assembly: WebResource("Fullstep.DataEntry.favoritos.gif", "image/gif")> 

<Assembly: WebResource("Fullstep.DataEntry.formatos.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.DataEntry.popupDescrLarga.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.DataEntry.popupTextoMedio.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.DataEntry.dateCalendar.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.DataEntry.dropdown.js", "application/x-javascript")> 
<Assembly: WebResource("Fullstep.DataEntry.fsGeneralEntry.js", "application/x-javascript")> 