// flag to avoid multiple listeners to same mouse-down event
var globalListenerWasCreated = false;
// optional references to drop-down calendars
// Notes: in case of single global calendar it can be replaced by a simple object instead of array
//  or it can be not used at all: igcal_getCalendarById("SharedCalendar") will return calendar object dynamically.
var dropDownCalendars = new Array();
// it is called when drop-down calendar is initialized
// Note: that name should match with the ClientSideEvents.InitializeCalendar property
//  which is set in aspx for WebCalendar
var vTabContainer
function preInitCalendar(id)
{
vTabContainer=id
}
function initCalendarEvent(oCalendar) {
	oCalendar.Contenedor = vTabContainer
	// ensure absolute position
	// Note: it can be skipped if style in aspx has that
	oCalendar.element.style.position = "absolute";
	oCalendar.element.style.zIndex= 4000
	// assume that calendar on start is visible
	// create boolean member variable, which simplifies visibility test
	oCalendar.isDisplayed = true;
	// optional global cash of calendar-reference
	// Note: igcal_getCalendarById("SharedCalendar") or something like that
	//  can be used instead of dropDownCalendars[0]
	dropDownCalendars[dropDownCalendars.length] = oCalendar;
	// hide drop-down calendar
	showDropDown(oCalendar, null, false);
}

/*
''' <summary>
''' it is called by date click events of WebCalendar
''' Note: that name should match with the ClientSideEvents.DateClicked property
''' which is set in aspx for WebCalendar
''' </summary>
''' <param name="oCalendar">control donde cliquaste</param>
''' <param name="oDate">Fecha antes de cliquear</param>        
''' <param name="oEvent">evento desencadenado</param>   
''' <remarks>Llamada desde: modInstancia.vb		certificados/desglose.aspx		noconformidad/desglose.aspx	
solicitudes/desglose.aspx; Tiempo m�ximo: 0</remarks>*/
function calendarDateClickedEvent(oCalendar, oDate, oEvent) {
    var FechaAntes = oCalendar.oEdit.text
	// update editor with latest date and hide calendar
    showDropDown(oCalendar, oDate, false, true);

    var FechaDespues = oCalendar.oEdit.text

    try {
        if (oCalendar.oEdit.fsEntry.CtrlCambioRechazo == true) {
            if (FechaAntes != FechaDespues) {
                fCambioNc(oCalendar.oEdit, "", oEvent);
            }
        }
    }
    catch (e) { }	
}
// it is called by custom-button click event of WebDateTimeEdit
// Note: that name should match with the ClientSideEvents.CustomButtonPress property
//  which is set in aspx for WebDateTimeEdit
function openDropDownEvent(oEdit, text, oEvent)
{
	// open drop-down calendar
	ctlEdit = oEdit.ID.split("__")[0]
	re=/__t/i
	for (i=0;i<dropDownCalendars.length ;i++)
		{
		showDropDown(dropDownCalendars[i], null, false);
		}
	for (i=0;i<dropDownCalendars.length && dropDownCalendars[i].ID.search(oEdit.ID.replace(re,""))!=0;i++)
		{
		}
	i=0
	openDropDown(oEdit, dropDownCalendars[i]);
	// Note: if dropDownCalendars is not used, then following commented line
	//  can be used instead of line above	
	//openDropDown(oEdit, igcal_getCalendarById("SharedCalendar"));
}
// it is called by spin and focus events of WebDateTimeEdit
// Note: that name should match with the ClientSideEvents.KeyDown/Spin/Focus/etc. properties
//  which are set in aspx for WebDateTimeEdit
function closeDropDownEvent(oEdit, text, oEvent)
{
    // hide calendar
	showDropDown(oEdit.oCalendar, null, false);
}
// open calendar and attach it to WebDateTimeEdit
// oEdit - reference to the owner of calendar (WebDateTimeEdit)
// oCalendar - the WebCalendar which should be dropped and attached to oEdit
function openDropDown(oEdit, oCalendar)
{
	if(oCalendar == null) return;
	// add listener to mouse click events for page
	if(!globalListenerWasCreated)
		ig_csom.addEventListener(window.document, "mousedown", globalMouseDown, false);
	globalListenerWasCreated = true;
	// set reference of calendar to editor:
	// create member variable, which points to drop-down calendar
	oEdit.oCalendar = oCalendar;
	// if it belongs to another oEdit, then close oCalendar
	if(oCalendar.oEdit != oEdit)
	{
		showDropDown(oCalendar, null, false);
		// set reference in oCalendar to this oEdit
		// create member variable, which points to the owner oEdit
		oCalendar.oEdit = oEdit;
	}
	// show calendar with date from editor
	// if calendar is already opened, then hide calendar (last param)
	showDropDown(oCalendar, oEdit.getDate(), true, true, true);
}
// synchronize dates in DateEdit and calendar, and show/close calendar
function showDropDown(oCalendar, date, show, update, toggle)
{
	if(oCalendar == null) return;
	if(toggle == true && oCalendar.isDisplayed == true)
		show = update = false;
	// update editor with latest date
	if(update == true)
	{
	    if (oCalendar.isDisplayed) {
		    //Comprobar que las fechas introducidas son correctas.
	        if (oCalendar.oEdit.fsEntry.FechaNoAnteriorAlSistema != 0) {
	            var fechaActual = new Date();
	            if ((fechaActual.getYear() > date.getYear()) || ((fechaActual.getYear() == date.getYear()) && (fechaActual.getMonth() > date.getMonth())) || ((fechaActual.getYear() == date.getYear()) && (fechaActual.getMonth() == date.getMonth()) && (fechaActual.getDate() > date.getDate()))) {
		            alert(sFechaNoAnteriorAlSistema)
		            oCalendar.oEdit.fsEntry.setDataValue("");
		            oCalendar.oEdit.fsEntry.setValue("");
		            oCalendar.oEdit.fsEntry.setValue("");
		            oCalendar.oEdit.fsEntry.setText("");
		            oCalendar.oEdit.fsEntry.setDate("");
		            oCalendar.oEdit.fsEntry.old = null;
		        }
		    }
			if ((oCalendar.oEdit.fsEntry.tipoGS == 6) || (oCalendar.oEdit.fsEntry.tipoGS == 7)) {				

				if (oCalendar.oEdit.fsEntry.tipoGS == 6 ) {  //El campo es INICIO DE SUMINISTRO
				//Mirar si la fecha de FIN de SUMINISTRO es menor a la de Inicio
				    if (oCalendar.oEdit.fsEntry.idDataEntryDependent) {
				        oFSEntryFFIN = fsGeneralEntry_getById(oCalendar.oEdit.fsEntry.idDataEntryDependent)
				        if ((oFSEntryFFIN) && (oFSEntryFFIN.tipoGS == 7) && (oFSEntryFFIN.getValue())) {
				            if (date > oFSEntryFFIN.getValue()) {
				                //Cargar el ALERT de la BBDD						
				                alert(sMensajeFecha)
				                oCalendar.oEdit.setDate("");
				            } else {
				                oCalendar.oEdit.setDate(date);
				            }
				        } else {
				            oCalendar.oEdit.setDate(date);
				        }
				    } else {
				        oCalendar.oEdit.setDate(date);
				    }
						
				}else
					if (oCalendar.oEdit.fsEntry.tipoGS == 7 ){
					    if (oCalendar.oEdit.fsEntry.idDataEntryDependent) {
					        oFSEntryFINI = fsGeneralEntry_getById(oCalendar.oEdit.fsEntry.idDataEntryDependent)
					        if ((oFSEntryFINI) && (oFSEntryFINI.tipoGS == 6) && (oFSEntryFINI.getValue()))
					            if (oFSEntryFINI.getValue() > date) {
					                //Cargar el ALERT de la BBDD							
					                alert(sMensajeFecha)
					                oCalendar.oEdit.setDate("");
					            } else {
					                oCalendar.oEdit.setDate(date);
					            }
					        else {
					            oCalendar.oEdit.setDate(date);
					        }
					    } else {
					        oCalendar.oEdit.setDate(date);
					    }
					}	
			}else
			{
			    if (oCalendar.oEdit.fsEntry.tipoGS == 27 || oCalendar.oEdit.fsEntry.tipoGS == 28) {
					oCalendar.oEdit.setDate(date);
					
					oFSEntry = fsGeneralEntry_getById("txtfsFecDesPub")
					if (oFSEntry)
					{
						if (oFSEntry.getValue())
						{					
							var DespubMes=oFSEntry.getValue().getMonth() + 1
							var DespubDia=oFSEntry.getValue().getDate()
							var DespubAnyo=oFSEntry.getValue().getFullYear()			
						}
						else
						{					
							var DespubMes=null
							var DespubDia=null
							var DespubAnyo=null
			            }
			            if (oCalendar.oEdit.fsEntry.tipoGS == 27) {
			                PublicarCertificado('FechaDespublicacion')
						}else{
						PublicarCertificado('FechaLimCumplimentacion')
						}
						//window.open("publicarCertificado.aspx?Certificado=" + document.forms("frmDetalle").item("Certificado").value + "&FecDespubAnyo=" + DespubAnyo + "&FecDespubMes=" + DespubMes + "&FecDespubDia=" + DespubDia + "&Refrescar=1" ,"iframeWSServer")
			}
				}
				else
					if (oCalendar.oEdit.fsEntry.id == "GeneralFecha") { //DataEntry pag. Trasladarusu.aspx 					
						fechaActual = new Date();
						if ((fechaActual.getYear() > date.getYear()) || ((fechaActual.getYear() == date.getYear()) && (fechaActual.getMonth() > date.getMonth())) || ((fechaActual.getYear() == date.getYear()) && (fechaActual.getMonth() == date.getMonth()) && (fechaActual.getDate() > date.getDate()))) {
							alert(sMensajeFechaTraslado);
							oCalendar.oEdit.setDate("");
						}else
							oCalendar.oEdit.setDate(date);
					}else							
						oCalendar.oEdit.setDate(date);
			}
		}else
			oCalendar.setSelectedDate(oCalendar.oEdit.getDate());
	}
	// check current state of calendar
	if(oCalendar.isDisplayed == show)
		return;
	if (show) globalMouseDown();
	// show/hide calendar
	oCalendar.element.style.zIndex=2000
	oCalendar.element.style.display = show ? "block" : "none";
	oCalendar.element.style.visibility = show ? "visible" : "hidden";
	oCalendar.isDisplayed = show;
	if(show)
		positionCalendar(oCalendar);
}
// set position of calendar below DateEdit
function positionCalendar(oCalendar)
{
    //elem: Elemento contenedor del calendario
    var elem = oCalendar.oEdit.Element;
    // left and top position of calendar
    var x = 0, y = elem.offsetHeight;
    if (y == null) y = 0;
    // width and height of super parent (document)
    var w = 0, h = 0;
    while (elem != null) {
        //if (!(elem.id.search(oCalendar.Contenedor + "_div")==0 || elem.id.search(oCalendar.Contenedor + "_cp")==0 || elem.id.search("igtab" + oCalendar.Contenedor)==0))
        {
            if (elem.offsetLeft != null) x += elem.offsetLeft;
            if (elem.offsetTop != null) y += elem.offsetTop;
        }
        h = elem.offsetHeight;
        w = elem.offsetWidth;
        elem = elem.offsetParent;
    }

    //retiramos de la posici�n del objeto lo relativo a los elementos que est�n por encima del contenedor del calendario
    elem = oCalendar.oEdit.Element;

    while (elem != null) {
        if (elem.scrollLeft != undefined) {
            x -= elem.scrollLeft
            y -= elem.scrollTop
        }
        elem = elem.parentNode;
    }

    var myWH = new Array();
    myWH = WindowSize(); //[0]-->ancho, [1]-->alto

    //Si el calendario no cabe por encima de control que lo contiene, lo pondremos por debajo de todas todas (aunque ah� tampoco quepa)
    //Por ello, solo comprobaremos si es neceario modificar la ubicaci�n que ya hemos calculado para el calendario, cuando sepamos que cabe arriba.
    elem = oCalendar.element;
    var calendarHeight = elem.offsetHeight;
    var calendarWidth = elem.offsetWidth;

    if ((calendarHeight < y + window.document.documentElement.scrollTop) && (calendarWidth < x + window.document.documentElement.scrollLeft)) {
        // check if calendar fits below editor
        // if not, then move it above editor
        var calendarBottomEnd = y + elem.offsetHeight;
        var calendarRightEnd = x + elem.offsetWidth;

        if (elem.offsetHeight < y) //Solo si el ancho calendario cabe encima del control que lo contiene sin desaparecer en el scroll horizontal
            if (calendarBottomEnd > myWH[1]) //if (calendarBottomEnd > myWH[1] + window.document.documentElement.scrollTop)
                y -= (elem.offsetHeight + oCalendar.oEdit.Element.offsetHeight);

        if (elem.offsetWidth < x) //Solo si el ancho calendario cabe encima del control que lo contiene sin desaparecer en el scroll vertical
            if (calendarRightEnd > myWH[0]) //if (calendarRightEnd > myWH[0] + window.document.documentElement.scrollLeft)
                x = w - elem.offsetWidth - 20 + window.document.documentElement.scrollLeft;
    }


    oCalendar.element.style.left = x + "px";
    oCalendar.element.style.top = y + window.document.documentElement.scrollTop + "px";


}
// process mouse click events for page: close drop-down
function globalMouseDown(evt)
{
	try{
		CerrarDesplegables(evt);
	}
	catch(e){
		CloseDateCalendars(evt);
	}
}

function CloseDateCalendars(evt){
	// reference to visible dropped-down calendar
	var oCalendar = null;
	// Note: 2 commented lines below can be used instead of a loop,
	//  if dropDownCalendars is not used and a single global calendar with id "SharedCalendar" is used
	//oCalendar = igcal_getCalendarById("SharedCalendar");
	//if(oCalendar != null && !oCalendar.isDisplayed) oCalendar = null;
	var i = dropDownCalendars.length;
	for(var i = 0; i < dropDownCalendars.length; i++) if(dropDownCalendars[i].isDisplayed)
	{
		oCalendar = dropDownCalendars[i];
		break;
	}
	// check if opened calendar was found
	if(oCalendar == null)
		return;
	// find source element
	if(evt == null) evt = window.event;
	if(evt != null)
	{
		var elem = evt.srcElement;
		if(elem == null) if((elem = evt.target) == null) o = this;
		while(elem != null)
		{
			// ignore events that belong to calendar
			if(elem == oCalendar.element) return;
			elem = elem.offsetParent;
		}
	}
	// close calendar
	showDropDown(oCalendar, null, false, false);
}

//Devuelve el ancho y alto de la ventana del navegador
function WindowSize() {
    var myWidth = 0, myHeight = 0;
    if (typeof (window.innerWidth) == 'number') {
        //Non-IE
        myWidth = window.innerWidth;
        myHeight = window.innerHeight;
    } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        //IE 6+ in 'standards compliant mode'
        myWidth = document.documentElement.clientWidth;
        myHeight = document.documentElement.clientHeight;
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        //IE 4 compatible
        myWidth = document.body.clientWidth;
        myHeight = document.body.clientHeight;
    }
    var myWH = new Array();
    myWH[0] = myWidth;
    myWH[1] = myHeight;
    return myWH;
}