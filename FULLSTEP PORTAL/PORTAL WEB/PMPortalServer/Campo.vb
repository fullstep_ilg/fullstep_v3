Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Campo
        Inherits Security
        Private mlId As Integer

        Private mlIdCopiaCampo As Long
        Private mlIdGrupo As Integer

        Private moDen As MultiIdioma
        Private moAyuda As MultiIdioma
        Private miTipo As Integer
        Private miSubtipo As Integer
        Private miIntro As Integer
        Private mdValorNum As Double
        Private msValorText As String
        Private mdtValorDate As Date
        Private mbValorBool As Boolean
        Private mdMinNum As Double
        Private mdMaxNum As Double
        Private mdtMinDate As Date
        Private mdtMaxDate As Date
        Private mlIdAtribGS As Long
        Private mlIdTipoCampoGS As Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS
        Private miOrden As Integer
        Private mlIdSolicitud As Long
        Private mlIdInstancia As Long
        Private msGMN1 As String
        Private msGMN2 As String
        Private msGMN3 As String
        Private msGMN4 As String
        Private msFormula As String
        Private mlOrigenCalculo As Integer
        Private mlCampoPadre As Integer
        Private msSelCualquierMatPortal As String
        Property CampoPadre() As Integer
            Get
                Return mlCampoPadre
            End Get
            Set(ByVal Value As Integer)
                mlCampoPadre = Value
            End Set
        End Property
        Property OrigenCalculo() As Integer
            Get
                Return mlOrigenCalculo
            End Get
            Set(ByVal Value As Integer)
                mlOrigenCalculo = Value
            End Set
        End Property
        Private miLineasPreconf As Integer
        Private moData As DataSet
        Private moDenSolicitud As MultiIdioma
        Private moDenGrupo As MultiIdioma
        Property IdInstancia() As Long
            Get
                Return mlIdInstancia
            End Get
            Set(ByVal Value As Long)
                mlIdInstancia = Value
            End Set
        End Property
        Property IdSolicitud() As Long
            Get
                Return mlIdSolicitud
            End Get
            Set(ByVal Value As Long)
                mlIdSolicitud = Value
            End Set
        End Property
        Property Id() As Integer
            Get
                Id = mlId
            End Get
            Set(ByVal Value As Integer)
                mlId = Value
            End Set
        End Property
        Property IdCopiaCampo() As Long
            Get
                IdCopiaCampo = mlIdCopiaCampo
            End Get
            Set(ByVal Value As Long)
                mlIdCopiaCampo = Value
            End Set
        End Property
        Property IdGrupo() As Integer
            Get
                Return mlIdGrupo
            End Get
            Set(ByVal Value As Integer)
                mlIdGrupo = Value
            End Set
        End Property
        Property Den(ByVal Idioma As String) As String
            Get
                Den = moDen(Idioma)

            End Get
            Set(ByVal Value As String)
                If Not moDen.Contains(Idioma) Then
                    moDen.Add(Idioma, Value)
                Else
                    moDen(Idioma) = Value
                End If

            End Set
        End Property
        Property Ayuda(ByVal Idioma As String) As String
            Get
                Ayuda = moAyuda(Idioma)

            End Get
            Set(ByVal Value As String)
                If Not moAyuda.Contains(Idioma) Then
                    moAyuda.Add(Idioma, Value)
                Else
                    moAyuda(Idioma) = Value
                End If

            End Set
        End Property
        Property Tipo() As Integer
            Get
                Tipo = miTipo
            End Get
            Set(ByVal Value As Integer)
                miTipo = Value
            End Set
        End Property
        Property Subtipo() As Integer
            Get
                Subtipo = miSubtipo
            End Get
            Set(ByVal Value As Integer)
                miSubtipo = Value
            End Set
        End Property
        Property Intro() As Integer
            Get
                Intro = miIntro
            End Get
            Set(ByVal Value As Integer)
                miIntro = Value
            End Set
        End Property
        Property ValorNum() As Double
            Get
                ValorNum = mdValorNum
            End Get
            Set(ByVal Value As Double)
                mdValorNum = Value
            End Set
        End Property
        Property ValorText() As String
            Get
                ValorText = msValorText
            End Get
            Set(ByVal Value As String)
                msValorText = Value
            End Set
        End Property
        Property ValorDate() As Date
            Get
                ValorDate = mdtValorDate
            End Get
            Set(ByVal Value As Date)
                mdtValorDate = Value
            End Set
        End Property
        Property ValorBool() As Boolean
            Get
                ValorBool = mbValorBool
            End Get
            Set(ByVal Value As Boolean)
                mbValorBool = Value
            End Set
        End Property
        Property SelCualquierMatPortal() As String
            Get
                SelCualquierMatPortal = msSelCualquierMatPortal
            End Get
            Set(ByVal Value As String)
                msSelCualquierMatPortal = Value
            End Set
        End Property
        Property MinNum() As Double
            Get
                MinNum = mdMinNum
            End Get
            Set(ByVal Value As Double)
                mdMinNum = Value
            End Set
        End Property
        Property MaxNum() As Double
            Get
                MaxNum = mdMaxNum
            End Get
            Set(ByVal Value As Double)
                mdMaxNum = Value
            End Set
        End Property
        Property MinDate() As Date
            Get
                MinDate = mdtMinDate
            End Get
            Set(ByVal Value As Date)
                mdtMinDate = Value
            End Set
        End Property
        Property MaxDate() As Date
            Get
                MaxDate = mdtMaxDate
            End Get
            Set(ByVal Value As Date)
                mdtMaxDate = Value
            End Set
        End Property
        Property IdAtribGS() As Long
            Get
                IdAtribGS = mlIdAtribGS
            End Get
            Set(ByVal Value As Long)
                mlIdAtribGS = Value
            End Set
        End Property
        Property IdTipoCampoGS() As Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS
            Get
                IdTipoCampoGS = mlIdTipoCampoGS
            End Get
            Set(ByVal Value As Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS)
                mlIdTipoCampoGS = Value
            End Set
        End Property
        Property Orden() As Integer
            Get
                Orden = miOrden
            End Get
            Set(ByVal Value As Integer)
                miOrden = Value
            End Set
        End Property
        Property DenGrupo(ByVal Idioma As String) As String
            Get
                DenGrupo = moDenGrupo(Idioma)

            End Get
            Set(ByVal Value As String)
                If Not moDenGrupo.Contains(Idioma) Then
                    moDenGrupo.Add(Idioma, Value)
                Else
                    moDenGrupo(Idioma) = Value
                End If


            End Set
        End Property
        Property DenSolicitud(ByVal Idioma As String) As String
            Get
                DenSolicitud = moDenSolicitud(Idioma)

            End Get
            Set(ByVal Value As String)
                If Not moDenSolicitud.Contains(Idioma) Then
                    moDenSolicitud.Add(Idioma, Value)
                Else
                    moDenSolicitud(Idioma) = Value
                End If


            End Set
        End Property
        Property Data() As DataSet
            Get
                Return moData
            End Get
            Set(ByVal Value As DataSet)
                moData = Value
            End Set
        End Property
        Property LineasPreconf() As Integer
            Get
                Return miLineasPreconf
            End Get
            Set(ByVal Value As Integer)
                miLineasPreconf = Value
            End Set
        End Property
        Property GMN1() As String
            Get
                Return msGMN1
            End Get
            Set(ByVal Value As String)
                msGMN1 = Value
            End Set
        End Property
        Property GMN2() As String
            Get
                Return msGMN2
            End Get
            Set(ByVal Value As String)
                msGMN2 = Value
            End Set
        End Property
        Property GMN3() As String
            Get
                Return msGMN3
            End Get
            Set(ByVal Value As String)
                msGMN3 = Value
            End Set
        End Property
        Property GMN4() As String
            Get
                Return msGMN4
            End Get
            Set(ByVal Value As String)
                msGMN4 = Value
            End Set
        End Property
        Property Formula() As String
            Get
                Return msFormula
            End Get
            Set(ByVal Value As String)
                msFormula = Value
            End Set
        End Property
        Private _tipoSolicitud As Integer
        Public Property TipoSolicitud() As Integer
            Get
                Return _tipoSolicitud
            End Get
            Set(ByVal value As Integer)
                _tipoSolicitud = value
            End Set
        End Property
        Private _instanciaMoneda As String
        Public Property InstanciaMoneda() As String
            Get
                Return _instanciaMoneda
            End Get
            Set(ByVal value As String)
                _instanciaMoneda = value
            End Set
        End Property
        ''' <summary>
        ''' Carga el campo
        ''' </summary>
        ''' <param name="lCiaComp">Codigo de compania</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lSolicitud">Solicitud</param>
        ''' <remarks>Llamada desde:  certificados\desglose.aspx.vb       _common\atachedfiles.ascx.vb	_common\atachedfilesDesglose.aspx.vb	_common\ayudacampo.aspx.vb
        ''' _common\campos.ascx.vb		_common\desglose.ascx.vb		_common\presAsig.aspx.vb		_common\presupuestos.aspx.vb; Tiempo maximo: 0,2</remarks>        
        Public Sub Load(ByVal lCiaComp As Long, Optional ByVal sIdi As String = Nothing, Optional ByVal lSolicitud As Long = Nothing)
            Authenticate()
            Dim data As DataSet
            Dim oRow As DataRow
            Dim oIdiomas As New Idiomas(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
            Dim oIdi As Idioma

            If mRemottingServer Then
                data = DBServer.Campo_Load(lCiaComp, mlId, sIdi, lSolicitud, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Campo_Load(lCiaComp, mlId, sIdi, lSolicitud)
            End If

            oRow = data.Tables(0).Rows(0)
            mlId = oRow.Item("ID")
            If sIdi = Nothing Then
                oIdiomas.Load()
            Else
                oIdiomas.Add(sIdi, Nothing)
            End If

            For Each oIdi In oIdiomas.Idiomas
                moDen(oIdi.Cod) = oRow.Item("DEN_" & oIdi.Cod).ToString
                moAyuda(oIdi.Cod) = oRow.Item("AYUDA_" & oIdi.Cod).ToString
                moDenGrupo(oIdi.Cod) = oRow.Item("DENGRUPO_" & oIdi.Cod).ToString
                moDenSolicitud(oIdi.Cod) = oRow.Item("DENSOL_" & oIdi.Cod).ToString
            Next
            mlIdSolicitud = oRow.Item("SOLICITUD")
            _tipoSolicitud=oRow.Item("TIPO_SOLICITUD")
            miTipo = oRow.Item("TIPO")
            miSubtipo = oRow.Item("SUBTIPO")
            miIntro = oRow.Item("INTRO")
            mdValorNum = DBNullToSomething(oRow.Item("VALOR_NUM"))
            mdtValorDate = DBNullToSomething(oRow.Item("VALOR_FEC"))
            mbValorBool = (DBNullToSomething(oRow.Item("VALOR_BOOL")) = 1)
            msValorText = DBNullToSomething(oRow.Item("VALOR_TEXT"))
            mdMinNum = DBNullToSomething(oRow.Item("MINNUM"))
            mdMaxNum = DBNullToSomething(oRow.Item("MAXNUM"))
            mdtMinDate = DBNullToSomething(oRow.Item("MINFEC"))
            mdtMaxDate = DBNullToSomething(oRow.Item("MAXFEC"))
            miOrden = oRow.Item("ORDEN")
            mlIdAtribGS = DBNullToSomething(oRow.Item("ID_ATRIB_GS"))
            mlIdTipoCampoGS = DBNullToSomething(oRow.Item("TIPO_CAMPO_GS"))
            miLineasPreconf = DBNullToSomething(oRow.Item("LINEAS_PRECONF"))
            msGMN1 = DBNullToSomething(oRow.Item("GMN1"))
            msGMN2 = DBNullToSomething(oRow.Item("GMN2"))
            msGMN3 = DBNullToSomething(oRow.Item("GMN3"))
            msGMN4 = DBNullToSomething(oRow.Item("GMN4"))
            msFormula = DBNullToSomething(oRow.Item("FORMULA"))
            mlOrigenCalculo = DBNullToSomething(oRow.Item("ORIGEN_CALC_DESGLOSE"))
            mlCampoPadre = DBNullToSomething(oRow.Item("CAMPO_PADRE"))
            msSelCualquierMatPortal = oRow.Item("SEL_CUALQUIER_MAT_PORTAL")  'Sandra


            data = Nothing
            oIdiomas = Nothing
        End Sub
        ''' <summary>
        ''' Carga un campo de la instancia
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>    
        ''' <param name="lInstancia">Id de la instancia</param>    
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lCiaProv">Proveedor</param>
        ''' <remarks>Llamada desde: attachedfiles.aspx;ayudacampo.aspx;campos.asx;desglose.ascx;presupuestos.aspx;guardarinstancia.aspx;modinstancia.aspx; Tiempo m�ximo:1</remarks>
        Public Sub LoadInst(ByVal lCiaComp As Long, ByVal lInstancia As Long, Optional ByVal sIdi As String = Nothing, Optional ByVal lCiaProv As Long = Nothing)
            Authenticate()
            Dim data As DataSet
            Dim oRow As DataRow
            Dim oIdiomas As New Idiomas(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
            Dim oIdi As Idioma

            If mRemottingServer Then
                data = DBServer.CampoInst_Load(lCiaComp, lInstancia, mlId, sIdi, lCiaProv, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.CampoInst_Load(lCiaComp, lInstancia, mlId, sIdi, lCiaProv)
            End If

            oRow = data.Tables(0).Rows(0)
            mlId = oRow.Item("ID")
            If sIdi = Nothing Then
                oIdiomas.Load()
            Else
                oIdiomas.Add(sIdi, Nothing)
            End If

            For Each oIdi In oIdiomas.Idiomas
                moDen(oIdi.Cod) = oRow.Item("DEN_" & oIdi.Cod).ToString
                moAyuda(oIdi.Cod) = oRow.Item("AYUDA_" & oIdi.Cod).ToString
                moDenGrupo(oIdi.Cod) = oRow.Item("DENGRUPO_" & oIdi.Cod).ToString
                moDenSolicitud(oIdi.Cod) = oRow.Item("DENSOL_" & oIdi.Cod).ToString
            Next
            mlIdInstancia = oRow.Item("INSTANCIA")
            _tipoSolicitud = oRow.Item("TIPO_SOLICITUD")
            _instanciaMoneda = oRow.Item("MON")
            miTipo = oRow.Item("TIPO")
            miSubtipo = oRow.Item("SUBTIPO")
            miIntro = oRow.Item("INTRO")
            mdValorNum = DBNullToSomething(oRow.Item("VALOR_NUM"))
            mdtValorDate = DBNullToSomething(oRow.Item("VALOR_FEC"))
            mbValorBool = (DBNullToSomething(oRow.Item("VALOR_BOOL")) = 1)
            msValorText = DBNullToSomething(oRow.Item("VALOR_TEXT"))
            mdMinNum = DBNullToSomething(oRow.Item("MINNUM"))
            mdMaxNum = DBNullToSomething(oRow.Item("MAXNUM"))
            mdtMinDate = DBNullToSomething(oRow.Item("MINFEC"))
            mdtMaxDate = DBNullToSomething(oRow.Item("MAXFEC"))
            miOrden = oRow.Item("ORDEN")
            mlIdAtribGS = DBNullToSomething(oRow.Item("ID_ATRIB_GS"))
            mlIdTipoCampoGS = DBNullToSomething(oRow.Item("TIPO_CAMPO_GS"))
            miLineasPreconf = DBNullToSomething(oRow.Item("LINEAS_PRECONF"))
            msFormula = DBNullToSomething(oRow.Item("FORMULA"))
            mlOrigenCalculo = DBNullToSomething(oRow.Item("ORIGEN_CALC_DESGLOSE"))
            mlCampoPadre = DBNullToSomething(oRow.Item("CAMPO_PADRE"))
            mlIdCopiaCampo = DBNullToSomething(oRow.Item("COPIA_CAMPO_DEF"))
            mlIdGrupo = DBNullToSomething(oRow.Item("GRUPO"))
            msSelCualquierMatPortal = oRow.Item("SEL_CUALQUIER_MAT_PORTAL")   'Sandra

            data = Nothing
            oIdiomas = Nothing
        End Sub
        ''' <summary>
        ''' carga el desglose
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lSolicitud">Solicitud</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: script\_common\campos.ascx  script\_common\desglose.ascx  ; Tiempo m�ximo: 0,2</remarks>
        Public Function LoadDesglose(ByVal lCiaComp As Long, Optional ByVal sIdi As String = Nothing, Optional ByVal lSolicitud As Long = Nothing, Optional ByVal bNuevoWorkfl As Boolean = False, Optional ByVal sProve As String = Nothing, Optional ByVal idUsu As Integer = Nothing) As DataSet
            Authenticate()

            If mRemottingServer Then
                moData = DBServer.Campo_LoadDesglose(lCiaComp, mlId, sIdi, lSolicitud, bNuevoWorkfl, sProve, idUsu, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.Campo_LoadDesglose(lCiaComp, mlId, sIdi, lSolicitud, bNuevoWorkfl, sProve, idUsu)
            End If
            Return moData
        End Function
        ''' <summary>
        ''' Funcion que devuelve el desglose de un campo de una instancia
        ''' </summary>
        ''' <param name="lCiaComp">compania</param>
        ''' <param name="sIdi">Idioma de la aplicacion</param>
        ''' <param name="lInstancia">Id de la instancia</param>
        ''' <param name="sUsu">Usuario</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <param name="lVersion">Version del Desglose</param>
        ''' <param name="bNuevoWorkflow">Booleano que indica si es un nuevo proceso</param>
        ''' <param name="bDefecto">Booleano que indica si es la instancia por defecto</param>
        ''' <param name="lVersionBd">Version de la instancia en bbdd. Los certificados, una vez q hay instancia, para determinar si los 
        ''' campos del desglose son visible/editables usan la version de la tabla certificado.
        ''' En caso de desgloses No pop siempre llega a cero, pero esta bien pq @VERSION contendra la version de bbdd y se asigna @VERSION_BD_CERT=@VERSION</param>
        ''' <param name="bSacarNumLinea">Si se saca NumLinea en el desglose o no . Tarea 3369</param>
        ''' <returns>Un dataset con los datos de desglose de instancia</returns>
        ''' <remarks>
        ''' Llamada desde: Campos/Page_Load, Desglose/Page_Load y CargarValoresDefecto, NoConformidad/CargarEstadosNoConformidad,impexp/Page_Load, detallleSolicConsulta/CargarDesglose, modInstancia/GuardarSolicitud y RecorrerDesglose
        ''' Tiempo m�ximo: 0,5 seg </remarks>
        Public Function LoadInstDesglose(ByVal lCiaComp As Long, Optional ByVal sIdi As String = Nothing, Optional ByVal lInstancia As Long = Nothing, Optional ByVal sUsu As String = Nothing, Optional ByVal sProve As String = Nothing, Optional ByVal lVersion As Long = Nothing, Optional ByVal bNuevoWorkflow As Boolean = False, Optional ByVal bDefecto As Boolean = False, Optional ByVal lVersionBd As Long = Nothing, Optional ByVal bSacarNumLinea As Boolean = False) As DataSet
            Authenticate()

            If mRemottingServer Then
                moData = DBServer.CampoInst_LoadDesglose(lCiaComp, mlId, mlIdCopiaCampo, sIdi, lInstancia, sUsu, sProve, lVersion, bDefecto, msSesionId, msIPDir, msPersistID, bNuevoWorkflow, lVersionBd, bSacarNumLinea)
            Else
                moData = DBServer.CampoInst_LoadDesglose(lCiaComp, mlId, mlIdCopiaCampo, sIdi, lInstancia, sUsu, sProve, lVersion, bDefecto, , , , bNuevoWorkflow, lVersionBd, bSacarNumLinea)
            End If

            Return moData
        End Function
        ''' <summary>
        ''' Obtiene los Campos calculados a partir de una solicitud
        ''' </summary>
        ''' <param name="lCiaComp">codigo de compa�ia</param>
        ''' <param name="lSolicitud">Solicitud</param>        
        ''' <param name="sProve">Proveedor</param>  
        ''' <returns>Campos calculados en la solicitud</returns>
        ''' <remarks>Llamada desde: ayudacampo.aspx     guardarinstancia.aspx   recalcularimportes.aspx; Tiempo m�ximo:0,1</remarks>
        Public Function LoadCalculados(ByVal lCiaComp As Long, ByVal lSolicitud As Long, Optional ByVal sProve As String = Nothing, Optional ByVal idUsu As Integer = Nothing) As DataSet
            Authenticate()

            If mRemottingServer Then
                moData = DBServer.Campo_LoadCamposCalculados(lCiaComp, mlId, lSolicitud, sProve, idUsu, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.Campo_LoadCamposCalculados(lCiaComp, mlId, lSolicitud, sProve, idUsu)
            End If
            Return moData
        End Function
        ''' <summary>
        ''' Obtiene los Campos calculados a partir de una instancia 
        ''' </summary>
        ''' <param name="lCiaComp">Codigo de compania</param>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: script\_common\AyudaCampo.aspx  script\_common\guardarinstancia.aspx  script\_common\recalcularimportes.aspx   
        ''' script\_common\modulos\modInstancia.vb; Tiempo m�ximo: 0,2</remarks>
        Public Function LoadInstCalculados(ByVal lCiaComp As Long, ByVal lInstancia As Long, ByVal sProve As String) As DataSet
            Authenticate()

            If mRemottingServer Then
                moData = DBServer.Campo_LoadInstCamposCalculados(lCiaComp, mlId, lInstancia, sProve, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.Campo_LoadInstCamposCalculados(lCiaComp, mlId, lInstancia, sProve)
            End If

            Return moData
        End Function
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
            moDen = New MultiIdioma
            moAyuda = New MultiIdioma
            moDenSolicitud = New MultiIdioma
            moDenGrupo = New MultiIdioma
        End Sub
        Public Shared Function ExtraerDescrIdioma(ByVal Text_Den As String, ByVal Idioma As String, Optional ByVal SepTextos As Char = "#", Optional ByVal SepIdioma As Char = "|") As String
            Dim arrTextos() As String = Text_Den.Split(SepTextos)
            If arrTextos.Length > 1 Then
                For Each sTexto As String In arrTextos
                    If Left(sTexto, sTexto.IndexOf(SepIdioma)) = Idioma Then Return Right(sTexto, sTexto.Length - sTexto.IndexOf(SepIdioma) - 1)
                Next
            Else
                If Text_Den.IndexOf(SepIdioma) > 0 AndAlso Left(Text_Den, Text_Den.IndexOf(SepIdioma)) = Idioma Then
                    Return Right(Text_Den, Text_Den.Length - Text_Den.IndexOf(SepIdioma) - 1)
                Else
                    Return Text_Den
                End If
            End If
        End Function
        ''' <summary>
        ''' Recoge los valores del combo de tipo lista enlazada que se necesita
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>    
        ''' <param name="lIdCampo">El Id del campo del combo</param>
        ''' <param name="iOrdenCampoPadre">El Orden del campo padre para obtener los valores referentes al valor del campo padre</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lInstancia">El Id de la instancia con la que se esta trabajando</param>
        ''' <returns>los valores del combo de tipo lista enlazada que se necesita</returns>
        ''' <remarks>Llamada desde: PMPortalServer/Campo.vb/DevolverListaEnlazada;  Tiempo maximo:0,1</remarks>
        Public Function DevolverListaEnlazada(ByVal lCiaComp As Long, ByVal lIdCampo As Long, ByVal iOrdenCampoPadre As Integer, ByVal sIdi As String, ByVal lInstancia As Long, ByVal lBloque As Long, ByVal lSolicitud As Long, ByVal sCodProve As String) As DataSet
            Authenticate()

            If mRemottingServer Then
                Return DBServer.Campo_DevolverListaEnlazada(lCiaComp, lIdCampo, iOrdenCampoPadre, sIdi, lInstancia, lBloque, lSolicitud, sCodProve, msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.Campo_DevolverListaEnlazada(lCiaComp, lIdCampo, iOrdenCampoPadre, sIdi, lInstancia, lBloque, lSolicitud, sCodProve)
            End If
        End Function
        ''' <summary>Devuelve los valores de una lista cuyo origen es un campo de tipo material</summary>
        ''' <param name="lIdCampo">Id del campo</param>
        ''' <param name="sGMN1">GMN1</param>
        ''' <param name="sGMN2">GMN2</param>
        ''' <param name="sGMN3">GMN3</param>
        ''' <param name="sGMN4">GMN4</param>
        ''' <returns>Dataset con los valores de la lista</returns>
        ''' <remarks>Llamada desde: ConsultasPMWEB.asmx</remarks>
        Public Function DevolverListaCampoPadreMaterial(ByVal lCiaComp As Long, ByVal lIdCampo As Long, ByVal sGMN1 As String, ByVal sGMN2 As String, ByVal sGMN3 As String, ByVal sGMN4 As String, ByVal sIdi As String) As DataSet
            Authenticate()

            If mRemottingServer Then
                Return DBServer.Campo_DevolverListaCampoPadreMaterial(lCiaComp, lIdCampo, sGMN1, sGMN2, sGMN3, sGMN4, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.Campo_DevolverListaCampoPadreMaterial(lCiaComp, lIdCampo, sGMN1, sGMN2, sGMN3, sGMN4, sIdi)
            End If

        End Function
    End Class
End Namespace
