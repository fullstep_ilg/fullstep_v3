Namespace Fullstep.PMPortalServer

    <Serializable()> _
    Public Class Dictionary
        Inherits Security
        Private mvarData As Data.DataSet

#Region " Bussiness methods "
        Public Function GetText(ByVal ID As Integer) As String
            Dim datarow() As Data.DataRow
            If mvarData Is Nothing Then Return ""
            datarow = mvarData.Tables(0).Select("ID=" & ID.ToString)
            If datarow.Length = 0 Then Return ""
            Return datarow(0).Item(1)

        End Function

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return mvarData
            End Get
        End Property
        ''' <summary>
        ''' Carga los textos
        ''' </summary>
        ''' <param name="ModuleID">Modulo de textos</param>
        ''' <param name="Language">Idioma</param>
        ''' <remarks>Llamada desde: Todos los vb del PMPortalWeb; Tiempo m�ximo: 0,2</remarks>
        Public Sub LoadData(ByVal ModuleID As Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas, Optional ByVal Language As String = "SPA")
            Authenticate()
            If mRemottingServer Then
                mvarData = DBServer.Dictionary_GetData(ModuleID, Language, msSesionId, msIPDir, msPersistID)
            Else
                mvarData = DBServer.Dictionary_GetData(ModuleID, Language)
            End If
        End Sub
#End Region

#Region " Constructor "
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
#End Region

    End Class
End Namespace
