Imports System.Linq
Namespace Fullstep.PMPortalServer
    <Serializable()>
    Public Class UnidadesNeg
        Inherits Fullstep.PMPortalServer.Security

        Private moDataUsu As DataSet
        Public ReadOnly Property UsuData() As Data.DataSet
            Get
                Return moDataUsu
            End Get
        End Property
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
        ''' <summary>
        ''' Devolver un recordset con el registro de unidades qa indicado en el idioma indicado
        ''' </summary>
        ''' <param name="lCiaComp">codigo de compania</param>
        ''' <param name="Idioma">Idioma. Saca codigo + den en idioma usuario</param>
        ''' <param name="lId">Unidad a cargar.</param>
        ''' <remarks>Llamada desde: noConformidad.aspx ; Tiempo: instantaneo</remarks>
        Public Sub UsuLoadData(ByVal lCiaComp As Long, ByVal Idioma As String, ByVal lId As String)
            Authenticate()
            If mRemottingServer Then
                moDataUsu = DBServer.UnidadesNegocioUsu_Get(lCiaComp, Idioma, lId, msSesionId, msIPDir, msPersistID)
            Else
                moDataUsu = DBServer.UnidadesNegocioUsu_Get(lCiaComp, Idioma, lId)
            End If
        End Sub
        ''' <summary>
        ''' Obtiene las unidades de negocio del proveedor con variables de calidad publicadas en portal para ver las puntuaciones
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de compania</param>  
        ''' <param name="sCodProve">C�digo de proveedor</param>   
        ''' <param name="sIdioma">idioma para los textos</param>    
        ''' <param name="sPyme">C�digo de pyme</param> 
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: PMPortalServer\CVariablesCalidad.vb     script\variablescalidad\PuntuacionesProveedor.asp; Tiempo m�ximo: 0,2</remarks>
        Public Function UnidadesNegocio_PuntuacionesProveedor(ByVal lCiaComp As Long, ByVal sCodProve As String, ByVal sIdioma As String,
                                                              ByVal EmpresaPortal As Boolean, ByVal NomPortal As String, Optional ByVal sPyme As String = "") As DataTable
            Authenticate()
            Dim dsUnidadesNegocio As DataSet
            If mRemottingServer Then
                dsUnidadesNegocio = DBServer.UnidadesNegocio_PuntuacionesProveedor(lCiaComp, sCodProve, sIdioma, EmpresaPortal, NomPortal, sPyme, msSesionId, msIPDir, msPersistID)
            Else
                dsUnidadesNegocio = DBServer.UnidadesNegocio_PuntuacionesProveedor(lCiaComp, sCodProve, sIdioma, EmpresaPortal, NomPortal, sPyme)
            End If
            Dim dtUnidadesNegocio As DataTable
            If EmpresaPortal Then
                Dim listaUNQAsAccesibles As IList = dsUnidadesNegocio.Tables(1).Rows.OfType(Of DataRow).Select(Function(x) x("UNQA")).ToList()
                dtUnidadesNegocio = dsUnidadesNegocio.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) (y("NIVEL") = 0 OrElse listaUNQAsAccesibles.Contains(y("ID")))).CopyToDataTable
            Else
                dtUnidadesNegocio = dsUnidadesNegocio.Tables(0)
            End If
            Return dtUnidadesNegocio
        End Function
    End Class
End Namespace