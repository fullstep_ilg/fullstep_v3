﻿Namespace Fullstep.PMPortalServer
    <Serializable()> _
     Public Class Facturas
        Inherits Fullstep.PMPortalServer.Security

        Private m_lNumeroFacturasPendientes As Long

        ''' <summary>
        ''' Método para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">código de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petición</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo máximo: 0</remarks>
        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

        Public Property NumeroFacturasPendientes() As Long
            Get
                Return m_lNumeroFacturasPendientes
            End Get

            Set(ByVal Value As Long)
                m_lNumeroFacturasPendientes = Value
            End Set
        End Property

        Private Enum Tipo
            Coste = 0
            Descuento = 1
        End Enum

        ''' <summary>
        ''' Devuelve los costes de una factura
        ''' </summary>
        ''' <param name="lCiaComp">id de la compañia</param>
        ''' <param name="sCodProve">codigo del proveedor</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function LoadCostesFacturas(ByVal lCiaComp As Long, ByVal sCodProve As String) As DataSet
            Authenticate()
            Return DBServer.Facturas_LoadCostesDescuentos(lCiaComp, sCodProve, Tipo.Coste)
        End Function

        ''' <summary>
        ''' Devuelve los costes de linea de una factura
        ''' </summary>
        ''' <param name="lCiaComp">id de la compañia</param>
        ''' <param name="sCodProve">codigo del proveedor</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function LoadCostesLineaFacturas(ByVal lCiaComp As Long, ByVal sCodProve As String) As DataSet
            Authenticate()
            Return DBServer.Factura_LoadCostesDescuentosLinea(lCiaComp, sCodProve, Tipo.Coste)
        End Function


        ''' <summary>
        ''' Devuelve los descuentos de linea de una factura
        ''' </summary>
        ''' <param name="lCiaComp">id de la compañia</param>
        ''' <param name="sCodProve">codigo del proveedor</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function LoadDescuentosLineaFacturas(ByVal lCiaComp As Long, ByVal sCodProve As String) As DataSet
            Authenticate()
            Return DBServer.Factura_LoadCostesDescuentosLinea(lCiaComp, sCodProve, Tipo.Descuento)
        End Function

        ''' <summary>
        ''' Devuelve los distintos estados de una factura
        ''' </summary>
        ''' <param name="lCiaComp">id de la compañia</param>
        ''' <param name="sIdioma">idioma de la aplicacion</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function LoadEstados(ByVal lCiaComp As Long, ByVal sIdioma As String) As DataTable
            Authenticate()
            Return DBServer.Facturas_LoadEstados(lCiaComp, sIdioma)
        End Function

        ''' <summary>
        ''' Devuelve los descuentos de una factura
        ''' </summary>
        ''' <param name="lCiaComp">id de la compañia</param>
        ''' <param name="sCodProve">codigo del proveedor</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function LoadDescuentosFacturas(ByVal lCiaComp As Long, ByVal sCodProve As String) As DataSet
            Authenticate()
            Return DBServer.Facturas_LoadCostesDescuentos(lCiaComp, sCodProve, Tipo.Descuento)
        End Function

        ''' <summary>
        ''' Devuelve los impuestos de facturas con los criterios indicados
        ''' </summary>
        ''' <param name="lCiaComp">id de la compañia</param>
        ''' <param name="sIdioma">idioma</param>
        ''' <param name="sCod">codigo del impuesto</param>
        ''' <param name="sDesc">denominacion del impuesto</param>
        ''' <param name="sPais">cod del pais</param>
        ''' <param name="sProv">cod de la provincia</param>
        ''' <param name="sCodArt">cod del articulo</param>
        ''' <param name="sCodMat">cod del material</param>
        ''' <param name="bRepercutido">indica si es un impuesto repercutido</param>
        ''' <param name="bRetenido">indica si es un impuesto retenido</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function LoadImpuestosFacturas(ByVal lCiaComp As Long, ByVal sIdioma As String, ByVal sCod As String, ByVal sDesc As String, ByVal sPais As String, ByVal sProv As String, ByVal sCodArt As String, ByVal sCodMat As String, ByVal bRepercutido As Boolean, ByVal bRetenido As Boolean, ByVal iConceptoImpuesto As Nullable(Of FSNLibrary.TiposDeDatos.ConceptoImpuesto)) As DataSet
            Authenticate()
            Return DBServer.Facturas_LoadImpuestosFacturas(lCiaComp, sIdioma, sCod, sDesc, sPais, sProv, sCodArt, sCodMat, bRepercutido, bRetenido, iConceptoImpuesto)
        End Function


        ''' <summary>
        ''' Carga todas las facturas para el usuario pasado como parÃ¡metro
        ''' </summary>
        ''' <param name="sUsu">Cod de la persona</param>
        ''' <param name="sIdioma">Idioma en la que salen las denominaciones</param>
        ''' <returns>las facturas de ese usuario</returns>
        ''' <remarks>Llamada desde: VisorFacturas.aspx</remarks>
        Public Function Visor_Facturas(ByVal lCiaComp As Long, ByVal sIdioma As String, ByVal sProve As String, ByVal Contacto As Long, ByVal Empresa As String, ByVal NumFactura As String, ByVal FecFacturaDesde As Date, ByVal FecFacturaHasta As Date, ByVal FecContaDesde As Date, ByVal FecContaHasta As Date, ByVal ImporteDesde As Double, ByVal ImporteHasta As Double, ByVal CentroCoste As String, ByVal Partidas As DataTable, ByVal Estado As String, ByVal AñoPedido As String, ByVal NumCesta As Long, ByVal NumPedido As Long, ByVal Articulo As String, ByVal Albaran As String, ByVal PedidoERP As String, ByVal NumFacturaSAP As String, ByVal Gestor As String, ByVal FacturaOriginal As Boolean, ByVal FacturaRectificativa As Boolean) As DataSet

            Authenticate()

            Dim ds As DataSet

            ds = DBServer.Facturas_Load(lCiaComp, sIdioma, sProve, Contacto, Empresa, NumFactura, FecFacturaDesde, FecFacturaHasta, FecContaDesde, FecContaHasta, ImporteDesde, ImporteHasta, CentroCoste, Partidas, Estado, AñoPedido, NumCesta, NumPedido, Articulo, Albaran, PedidoERP, NumFacturaSAP, Gestor, FacturaOriginal, FacturaRectificativa)

            For Each oColumn As DataColumn In ds.Tables(0).Columns
                If UCase(oColumn.DataType.FullName) = "SYSTEM.DATETIME" Then
                    For Each oRow As DataRow In ds.Tables(0).Rows
                        If Not IsDBNull(oRow.Item(oColumn.ColumnName)) Then
                            oRow.Item(oColumn.ColumnName) = Format(oRow.Item(oColumn.ColumnName), "d")
                        End If
                    Next
                End If
            Next

            m_lNumeroFacturasPendientes = ds.Tables(0).Select("ICONO=1").Length
            ds.Tables(0).TableName = "FACTURAS"
            Visor_Facturas = ds
        End Function

    End Class
End Namespace
