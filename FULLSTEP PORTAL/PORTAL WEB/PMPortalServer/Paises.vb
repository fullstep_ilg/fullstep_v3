Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Paises
        Inherits Fullstep.PMPortalServer.Security
        Private moPaises As DataSet
        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moPaises
            End Get
        End Property

        ''' <summary>
        ''' Obtiene los pa�ses
        ''' </summary>
        ''' <param name="sIdioma">Idioma del usuario</param>
        ''' <param name="lCiaComp">C�digo de la compa��a</param>
        ''' <param name="sCod">C�digo del pa�s</param>
        ''' <param name="sDen">Denominaci�n del pa�s o parte de ella</param>
        ''' <param name="bCoincid">Si coincide exactamente o se hace un LIKE</param>
        ''' <remarks>Llamada desde: PMPortalWeb --> campos.ascx.vb, desglose.ascx.vb, paisesserver.aspx.vb, impexp.aspx.vb; Tiempo m�ximo: 1sg</remarks>
        Public Sub LoadData(ByVal sIdioma As String, ByVal lCiaComp As Long, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal bCoincid As Boolean = False)
            Authenticate()
            If mRemottingServer Then
                moPaises = DBServer.Paises_Get(sIdioma, lCiaComp, sCod, sDen, bCoincid, msSesionId, msIPDir, msPersistID)
            Else
                moPaises = DBServer.Paises_Get(sIdioma, lCiaComp, sCod, sDen, bCoincid)
            End If
        End Sub
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

    End Class
End Namespace