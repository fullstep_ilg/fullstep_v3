Namespace Fullstep.PMPortalServer
    Public Class Activo
        Inherits Fullstep.PMPortalServer.Security

        ''' <summary>
        ''' Obtiene el detalle del activo
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="sIdioma">Idioma del usuario</param>
        ''' <param name="sCodActivo">C�digo del activo que se quiere buscar</param>
        ''' <returns>El detalle del activo</returns>
        ''' <remarks>Llamada desde: detalleactivo.aspx; Tiempo m�ximo: 1 sg;</remarks>
        Public Function DetalleActivo(ByVal lCiaComp As Long, ByVal sIdioma As String, ByVal sCodActivo As String) As DataSet
            Authenticate()
            If mRemottingServer Then
                DetalleActivo = DBServer.Activo_Detalle(lCiaComp, sIdioma, sCodActivo, msSesionId, msIPDir, msPersistID)
            Else
                DetalleActivo = DBServer.Activo_Detalle(lCiaComp, sIdioma, sCodActivo)
            End If
        End Function

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace
