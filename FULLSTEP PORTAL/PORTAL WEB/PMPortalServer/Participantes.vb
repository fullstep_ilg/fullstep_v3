Namespace Fullstep.PMPortalServer
    <Serializable()> _
Public Class Participantes

        Inherits Security

        Private moData As DataSet
        Private mlInstancia As Integer
        Private mlRol As Integer

        Public Property Instancia() As Integer
            Get
                Return mlInstancia
            End Get
            Set(ByVal Value As Integer)
                mlInstancia = Value
            End Set
        End Property
        Public Property Rol() As Integer
            Get
                Return mlRol
            End Get
            Set(ByVal Value As Integer)
                mlRol = Value
            End Set
        End Property

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moData
            End Get

        End Property

        ''' <summary>
        ''' carga los datos del participante
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sNom">Nombre </param>
        ''' <param name="sApe">apellido</param>
        ''' <param name="sUON1">Unidad organizativa de nivel 1</param>
        ''' <param name="sUON2">Unidad organizativa de nivel 2</param>
        ''' <param name="sUON3">Unidad organizativa de nivel 3</param>
        ''' <param name="sDep">Departamento</param>
        ''' <remarks>Llamada desde: script\solicitudes\participantesserver.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Sub LoadDataPersonas(ByVal lCiaComp As Long, Optional ByVal sNom As String = Nothing, Optional ByVal sApe As String = Nothing, Optional ByVal sUON1 As String = Nothing, Optional ByVal sUON2 As String = Nothing, Optional ByVal sUON3 As String = Nothing, Optional ByVal sDep As String = Nothing)

            Authenticate()
            If mRemottingServer Then
                moData = DBServer.Participantes_Persona_Load(lCiaComp, mlRol, mlInstancia, sNom, sApe, sUON1, sUON2, sUON3, sDep, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.Participantes_Persona_Load(lCiaComp, mlRol, mlInstancia, sNom, sApe, sUON1, sUON2, sUON3, sDep)
            End If

        End Sub
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

    End Class
End Namespace