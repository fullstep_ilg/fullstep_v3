Namespace Fullstep.PMPortalServer
    <Serializable()> _
        Public Class UnidadesOrg
        Inherits Security

        Private moData As DataSet



        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moData
            End Get

        End Property


        ''' <summary>
        ''' Cargar las Unidades Con Presupuestos
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iTipo">Tipo de presupuesto</param>
        ''' <param name="iAnyo">A�o de presupuesto</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <remarks>Llamada desde: script\_common\presAsig.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Sub CargarUnidadesConPresupuestos(ByVal lCiaComp As Long, ByVal iTipo As Integer, Optional ByVal iAnyo As Integer = Nothing, Optional ByVal sIdi As String = "spa")
            Authenticate()
            If mRemottingServer Then

                moData = DBServer.UnidadesConPresupuestos_Get(lCiaComp, iTipo, iAnyo, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.UnidadesConPresupuestos_Get(lCiaComp, iTipo, iAnyo, sIdi)
            End If

        End Sub

        ''' <summary>
        ''' Cargar el arbol de Unidades Con Presupuestos
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iTipo">Tipo de presupuesto</param>
        ''' <param name="sPer">Persona</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <remarks>Llamada desde: script\_common\presupuestos.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Sub CargarArbolUnidadesConPresupuestos(ByVal lCiaComp As Long, ByVal iTipo As Integer, Optional ByVal sPer As String = Nothing, Optional ByVal sIdi As String = "spa")
            Authenticate()
            If mRemottingServer Then

                moData = DBServer.UnidadesConPresupuestosArbol_Get(lCiaComp, iTipo, sPer, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.UnidadesConPresupuestosArbol_Get(lCiaComp, iTipo, sPer, sIdi)
            End If

            moData.Relations.Add("REL_UON0_UON1", moData.Tables(0).Columns("UON0"), moData.Tables(1).Columns("UON0"), False)
            moData.Relations.Add("REL_UON1_UON2", moData.Tables(1).Columns("UON1COD"), moData.Tables(2).Columns("UON1COD"), False)

            Dim oPrimaryCols(1) As DataColumn
            Dim oSecondaryCols(1) As DataColumn

            oPrimaryCols(0) = moData.Tables(2).Columns("UON1COD")
            oPrimaryCols(1) = moData.Tables(2).Columns("UON2COD")
            oSecondaryCols(0) = moData.Tables(3).Columns("UON1COD")
            oSecondaryCols(1) = moData.Tables(3).Columns("UON2COD")
            moData.Relations.Add("REL_UON2_UON3", oPrimaryCols, oSecondaryCols, False)


        End Sub
        ''' <summary>
        '''  Carga el arbol de presupuestos
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iTipo">Tipo de presupuesto</param>
        ''' <param name="iAnyo">A�o del presupuesto</param>
        ''' <param name="sUon1">Unidad organizativa de nivel 1</param>
        ''' <param name="sUon2">Unidad organizativa de nivel 2</param>
        ''' <param name="sUon3">Unidad organizativa de nivel 3</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="Codigo">Codigo del presupuesto</param>
        ''' <param name="Denominacion">Denominacion del presupuesto</param>
        ''' <param name="iEsUONVacia">Si los parametros uon q se pasan son todos vacios o no</param> 
        ''' <remarks>Llamada desde: script\_common\pres.aspx   script\_common\presAsig.aspx; Tiempo m�ximo: 0,2</remarks>
        Public Sub CargarPresupuestos(ByVal lCiaComp As Long, ByVal iTipo As Integer, Optional ByVal iAnyo As Integer = Nothing, Optional ByVal sUon1 As String = Nothing, Optional ByVal sUon2 As String = Nothing, Optional ByVal sUon3 As String = Nothing, Optional ByVal sIdi As String = "spa", Optional ByVal Codigo As String = Nothing, Optional ByVal Denominacion As String = Nothing, Optional ByVal iEsUONVacia As Integer = 0)
            Authenticate()
            If mRemottingServer Then
                moData = DBServer.Presupuestos_Get(lCiaComp, iTipo, iEsUONVacia, iAnyo, sUon1, sUon2, sUon3, sIdi, Codigo, Denominacion, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.Presupuestos_Get(lCiaComp, iTipo, iEsUONVacia, iAnyo, sUon1, sUon2, sUon3, sIdi, Codigo, Denominacion)
            End If

        End Sub

        ''' <summary>
        ''' Carga la relacion de uons
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <remarks>Llamada desde: script\_common\UnidadesOrganizativas.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Sub CargarTodasUnidadesOrganizativas(ByVal lCiaComp As Long, ByVal sIdi As String)
            Authenticate()
            If mRemottingServer Then

                moData = DBServer.UnidadesOrganizativas_Get(lCiaComp, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.UnidadesOrganizativas_Get(lCiaComp, sIdi)
            End If

        End Sub

        ''' <summary>
        ''' Cargar Unidades Organizativas
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="sUsu">Usuario</param>
        ''' <remarks>Llamada desde: script\_common\usuarios.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Sub CargarUnidadesOrganizativas(ByVal lCiaComp As Long, Optional ByVal sIdi As String = "spa", Optional ByVal sUsu As String = Nothing)
            Authenticate()
            If mRemottingServer Then

                moData = DBServer.UnidadesOrg_DevolverUO(lCiaComp, sIdi, sUsu, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.UnidadesOrg_DevolverUO(lCiaComp, sIdi, sUsu)
            End If

        End Sub

        ''' <summary>
        ''' Devuelve el nombre (pargen_lit.den) del presupeuesto
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iTipo">Tipo de presupuesto</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <remarks>Llamada desde: script\_common\presAsig.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Sub CargarCadenaPresupuesto(ByVal lCiaComp As Integer, ByVal iTipo As Integer, Optional ByVal sIdi As String = "spa")
            Authenticate()
            If mRemottingServer Then

                moData = DBServer.UnidadesOrg_DevolverCadenaPresupuesto(lCiaComp, iTipo, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.UnidadesOrg_DevolverCadenaPresupuesto(lCiaComp, iTipo, sIdi)
            End If

        End Sub

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace
