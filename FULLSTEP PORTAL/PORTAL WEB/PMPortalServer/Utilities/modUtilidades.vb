Namespace Fullstep.PMPortalServer

    Module modUtilidades
        Public Function DBNullToSomething(Optional ByVal value As Object = Nothing) As Object
            If IsDBNull(value) Then
                Return Nothing
            Else
                Return value
            End If

        End Function
        Public Function DBNullToStr(Optional ByVal value As Object = Nothing) As String
            If IsDBNull(value) Then
                Return ""
            Else
                Return value
            End If
        End Function
        Public Function GenerateRandomPath() As String
            Dim s As String = ""
            Dim i As Integer
            Dim lowerbound As Integer = 65
            Dim upperbound As Integer = 90
            Randomize()
            For i = 1 To 10
                s = s & Chr(Int((upperbound - lowerbound + 1) * Rnd() + lowerbound))
            Next
            Return s

        End Function

        ''' <summary>
        ''' Tratamos de convertir el objeto recibido a Integer
        ''' </summary>
        ''' <param name="value">Valor a convertir a Integer</param>
        ''' <returns>Valor convertido a Integer. Si no es posible o el value es null o nothing, devuelve 0</returns>
        ''' <remarks>Llamado desde toda la aplicaci�n, donde se requiera su uso. Tiempo m�x inferior a 0,1 seg</remarks>
        Public Function DBNullToInteger(Optional ByVal value As Object = Nothing) As Integer
            If IsDBNull(value) Then
                Return 0
            ElseIf value Is Nothing Then
                Return 0
            Else
                Return CInt(value)
            End If
        End Function
        ''' <summary>
        ''' Tratamos de convertir el objeto recibido a Boolean
        ''' </summary>
        ''' <param name="value">Valor a convertir a Boolean</param>
        ''' <returns>Valor convertido a Boolean. Si no es posible o el value es null o nothing, devuelve False</returns>
        ''' <remarks>Llamado desde toda la aplicaci�n, donde se requiera su uso. Tiempo m�x inferior a 0,1 seg</remarks>
        Public Function DBNullToBoolean(Optional ByVal value As Object = Nothing) As Boolean
            If IsDBNull(value) Then
                Return False
            ElseIf value Is Nothing Then
                Return False
            Else
                Return CType(value, Boolean)
            End If
        End Function

        ''' <summary>
        ''' Tratamos de convertir el objeto recibido a Double
        ''' </summary>
        ''' <param name="value">Valor a convertir a Double</param>
        ''' <returns>Valor convertido a double. Si no es posible o el value es null o nothing, devuelve 0</returns>
        ''' <remarks>Llamado desde toda la aplicaci�n, donde se requiera su uso. Tiempo m�x inferior a 0,1 seg</remarks>
        Public Function DBNullToDbl(Optional ByVal value As Object = Nothing) As Double
            If IsDBNull(value) Then
                Return 0
            ElseIf value Is Nothing Then
                Return 0
            Else
                Try
                    Dim valueDbl As Double = CDbl(value)
                    Return valueDbl
                Catch ex As Exception
                    Return 0
                End Try
            End If
        End Function
        Public Function igDateToDate(ByVal s As String) As DateTime

            Try
                If s = Nothing Or s = "" Then
                    Return Nothing
                End If
                Dim arr As Array = s.Split("-")

                Return DateSerial(arr(0), arr(1), arr(2))


            Catch ex As Exception
                Return Nothing

            End Try

        End Function
        Public Function DateToigDate(ByVal dt As DateTime) As String
            Try
                If dt = Nothing Then
                    Return ""
                End If
                '2005-7-14-12-7-14-244
                'anyo-m-di-ho-m-se-mil

                Dim s As String = Year(dt).ToString() + "-" + Month(dt).ToString() + "-" + Day(dt).ToString() + "-" + Hour(dt).ToString() + "-" + Minute(dt).ToString() + "-" + Second(dt).ToString()
                Return s

            Catch ex As Exception

                Return ""
            End Try
        End Function
        Public Function SQLBinaryToBoolean(ByVal var As Object) As Boolean

            If IsDBNull(var) Then
                Return False
            Else
                If var = 1 Then
                    Return True
                Else
                    Return False
                End If
            End If


        End Function

        Public Function DevolverFechaRelativaA(ByVal iTipoFecha As Fullstep.PMPortalServer.TiposDeDatos.TipoFecha, ByVal dtFechaFija As Date) As Object

            If Not dtFechaFija = Nothing Then
                Return dtFechaFija
            End If
            Select Case iTipoFecha
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.FechaAlta
                    Return Now
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.UnDiaDespues
                    Return DateAdd(DateInterval.Day, 1, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.DosDiasDespues
                    Return DateAdd(DateInterval.Day, 2, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.TresDiasDespues
                    Return DateAdd(DateInterval.Day, 3, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.CuatroDiasDespues
                    Return DateAdd(DateInterval.Day, 4, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.CincoDiasDespues
                    Return DateAdd(DateInterval.Day, 5, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.SeisDiasDespues
                    Return DateAdd(DateInterval.Day, 6, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.UnaSemanaDespues
                    Return DateAdd(DateInterval.Day, 7, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.DosSemanaDespues
                    Return DateAdd(DateInterval.Day, 14, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.TresSemanaDespues
                    Return DateAdd(DateInterval.Day, 21, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.UnMesDespues
                    Return DateAdd(DateInterval.Month, 1, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.DosMesDespues
                    Return DateAdd(DateInterval.Month, 2, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.TresMesDespues
                    Return DateAdd(DateInterval.Month, 3, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.CuatroMesDespues
                    Return DateAdd(DateInterval.Month, 4, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.CincoMesDespues
                    Return DateAdd(DateInterval.Month, 5, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.SeisMesDespues
                    Return DateAdd(DateInterval.Month, 6, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.UnAnyoDespues
                    Return DateAdd(DateInterval.Year, 1, Today)
                Case Else

                    Return DBNull.Value
            End Select

        End Function

        ''' <summary>
        ''' Devuelve un string con el numero que recibe formateado con el formato que recibe
        ''' </summary>
        ''' <param name="dNumber">Numero a formatear</param>
        ''' <param name="oNumberFormat">formato</param>
        ''' <returns>String con el numero formateado</returns>
        ''' <remarks></remarks>
        Public Function FormatNumber(ByVal dNumber As Double, ByVal oNumberFormat As System.Globalization.NumberFormatInfo) As String
            Return dNumber.ToString("N", oNumberFormat)
        End Function
    End Module
End Namespace
