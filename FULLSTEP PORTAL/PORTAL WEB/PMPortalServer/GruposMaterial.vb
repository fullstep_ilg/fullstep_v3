Namespace Fullstep.PMPortalServer
    <Serializable()> _
        Public Class GruposMaterial
        Inherits Security

        Private moData As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moData
            End Get

        End Property


        ''' Revisado por: blp. Fecha:24/01/2012
        ''' <summary>
        ''' Cargamos en moData (propiedad Data) del objeto GruposMaterial los datos de los C�digos de Material GMN1, GMN2, GMN3, GMN4 del proveedor
        ''' Esta funci�n es una sobregarga del loadData original (que no tiene en cuenta el proveedor).
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la empresa</param>
        ''' <param name="lProveID">C�digo del proveedor</param>
        ''' <param name="sGMN1">Codigo de material, nivel 1</param>
        ''' <param name="sGMN2">Codigo de material, nivel 2</param>
        ''' <param name="sGMN3">Codigo de material, nivel 3</param>
        ''' <param name="sGMN4">Codigo de material, nivel 4</param>
        ''' <param name="sIdi">Idioma del usuario</param>
        ''' <param name="bSelCualquierMatPortal">Seleccionar cualquier material</param>
        ''' <remarks>Llamada desde materiales.aspx.vb. M�x. 1 seg.</remarks>
        Public Sub LoadData(ByVal lCiaComp As Long, ByVal lProveID As Long, Optional ByVal sGMN1 As String = Nothing, Optional ByVal sGMN2 As String = Nothing,
                            Optional ByVal sGMN3 As String = Nothing, Optional ByVal sGMN4 As String = Nothing, Optional ByVal sIdi As String = "SPA",
                           Optional ByVal bSelCualquierMatPortal As Boolean = False, Optional ByVal iNivelSeleccion As Integer = 0)
            Authenticate()
            moData = DBServer.GruposMaterial_Get(lCiaComp, lProveID, sGMN1:=sGMN1, sGMN2:=sGMN2, sGMN3:=sGMN3, sGMN4:=sGMN4,
                                                 sIdi:=sIdi, bSelCualquierMatPortal:=bSelCualquierMatPortal, iNivelSeleccion:=iNivelSeleccion)

        End Sub

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace
