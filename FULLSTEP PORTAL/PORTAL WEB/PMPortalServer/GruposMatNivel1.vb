Namespace Fullstep.PMPortalServer
    <Serializable()> _
        Public Class GruposMatNivel1

        Inherits Security

        Private mCol As New Collection

        ''' <summary>
        ''' Carga en la coleccion de la clase el Material q se le proporciona
        ''' </summary>
        ''' <param name="sCod">Cod de Material</param>
        ''' <param name="sDen">denominacion del Material</param>
        ''' <returns>Material cargado en coleccion</returns>
        ''' <remarks>Llamada desde: CargarTodosLosGruposMatDesde; Tiempo maximo: 0</remarks>
        Public Function Add(ByVal sCod As String, ByVal sDen As String) As GrupoMatNivel1

            Dim oNewObject As New GrupoMatNivel1(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)

            With oNewObject

                .Cod = sCod
                .Den = sDen
            End With

            mCol.Add(oNewObject, sCod)
            Return oNewObject
        End Function

        Public Function Add(ByVal oGrupoMatNivel1 As GrupoMatNivel1) As GrupoMatNivel1
            mCol.Add(oGrupoMatNivel1, oGrupoMatNivel1.Cod)
            Return oGrupoMatNivel1
        End Function

        Public Function Item(ByVal sCod As String) As GrupoMatNivel1
            Return mCol(sCod)

        End Function

        ''' <summary>
        '''  Carga de materiales
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iNumMaximo">Numero maximo de registros a cargar</param>
        ''' <param name="CaracteresInicialesCod">Codigo a cargar</param>
        ''' <param name="CaracteresInicialesDen">Denominacion a cargar</param>
        ''' <param name="CoincidenciaTotal">Si se hace like o se hace =</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="OrdenadosPorDen">Si se ordena por den o no</param>
        ''' <remarks>Llamada desde: _common\articulosserver.aspx.vb/Page_Load   _common\campos.ascx.vb/Page_Load     _common\desglose.ascx.vb/Page_Load     
        ''' _common\desglose.ascx.vb/CargarValoresDefecto      _common\validararticulo.aspx.vb/Page_Load    noconformidad\impexp.aspx.vb/DenGS; Tiempo maximo:0,2</remarks>
        Public Sub CargarTodosLosGruposMatDesde(ByVal lCiaComp As Long, ByVal iNumMaximo As Integer, Optional ByVal sIdi As String = "SPA", Optional ByVal CaracteresInicialesCod As String = Nothing, Optional ByVal CaracteresInicialesDen As String = Nothing, Optional ByVal CoincidenciaTotal As Boolean = False, Optional ByVal OrdenadosPorDen As Boolean = False)
            Dim oRow As DataRow
            Dim oData As DataSet
            Authenticate()
            If mRemottingServer Then
                oData = DBServer.GruposMatNivel1_CargarTodosLosGruposMatDesde(lCiaComp, iNumMaximo, CaracteresInicialesCod, CaracteresInicialesDen, CoincidenciaTotal, sIdi, OrdenadosPorDen, msSesionId, msIPDir, msPersistID)
            Else
                oData = DBServer.GruposMatNivel1_CargarTodosLosGruposMatDesde(lCiaComp, iNumMaximo, CaracteresInicialesCod, CaracteresInicialesDen, CoincidenciaTotal, sIdi, OrdenadosPorDen)
            End If


            If oData.Tables(0).Rows.Count = 0 Then

            Else
                For Each oRow In oData.Tables(0).Rows
                    Me.Add(oRow.Item("COD"), oRow.Item("G1DEN"))
                Next

            End If
        End Sub
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace

