﻿Imports System.Collections.Generic
Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class cnDiscrepancias
        Inherits Fullstep.PMPortalServer.Security
        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal mRemottingServer As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, mRemottingServer, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
        Public Function Obtener_Datos_NuevaDiscrepancia_Proveedor(ByVal factura As Integer, ByVal linea As Integer, _
                            ByVal lCiaComp As Integer, ByVal Idi As String) As List(Of cn_fsItem)
            Authenticate()

            Dim dsDatos As DataSet
            Try
                dsDatos = DBServer.CN_Obtener_Datos_NuevaDiscrepancia_Proveedor(factura, linea, lCiaComp, Idi, _
                                                             msSesionId, msIPDir, msPersistID)

                Dim lItems As New List(Of cn_fsItem)
                Dim item As cn_fsItem

                With dsDatos
                    If .Tables("CATEGORIA").Rows.Count = 0 Then Return Nothing
                    item = New cn_fsItem
                    item.value = .Tables("CATEGORIA").Rows(0)("ID")
                    item.text = .Tables("CATEGORIA").Rows(0)("NOMBRECOMPLETO")
                    lItems.Add(item)
                    If Not .Tables("GESTOR").Rows.Count = 0 Then
                        item = New cn_fsItem
                        item.value = "itemParaUON" & _
                            IIf(IsDBNull(.Tables("GESTOR").Rows(0)("UON0")), "", "-UON0_" & .Tables("GESTOR").Rows(0)("UON0")) & _
                            IIf(IsDBNull(.Tables("GESTOR").Rows(0)("UON1")), "", "-UON1_" & .Tables("GESTOR").Rows(0)("UON1")) & _
                            IIf(IsDBNull(.Tables("GESTOR").Rows(0)("UON2")), "", "-UON2_" & .Tables("GESTOR").Rows(0)("UON2")) & _
                            IIf(IsDBNull(.Tables("GESTOR").Rows(0)("UON3")), "", "-UON3_" & .Tables("GESTOR").Rows(0)("UON3")) & _
                            IIf(IsDBNull(.Tables("GESTOR").Rows(0)("DEP")), "", "-DEP_" & .Tables("GESTOR").Rows(0)("DEP")) & _
                            IIf(IsDBNull(.Tables("GESTOR").Rows(0)("GESTOR")), "", "-USU_" & .Tables("GESTOR").Rows(0)("GESTOR"))
                        item.text = IIf(IsDBNull(.Tables("GESTOR").Rows(0)("UON1")), "", .Tables("GESTOR").Rows(0)("UON1") & "-") & _
                            IIf(IsDBNull(.Tables("GESTOR").Rows(0)("UON2")), "", .Tables("GESTOR").Rows(0)("UON2") & "-") & _
                            IIf(IsDBNull(.Tables("GESTOR").Rows(0)("UON3")), "", .Tables("GESTOR").Rows(0)("UON3") & "-") & _
                            IIf(IsDBNull(.Tables("GESTOR").Rows(0)("DEP")), "", .Tables("GESTOR").Rows(0)("DEP") & "-") & .Tables("GESTOR").Rows(0)("DEN").ToString
                        lItems.Add(item)
                    End If
                    If Not .Tables("RECEPTOR").Rows.Count = 0 Then
                        item = New cn_fsItem
                        item.value = "itemParaUON" & _
                            IIf(IsDBNull(.Tables("RECEPTOR").Rows(0)("UON0")), "", "-UON0_" & .Tables("RECEPTOR").Rows(0)("UON0")) & _
                            IIf(IsDBNull(.Tables("RECEPTOR").Rows(0)("UON1")), "", "-UON1_" & .Tables("RECEPTOR").Rows(0)("UON1")) & _
                            IIf(IsDBNull(.Tables("RECEPTOR").Rows(0)("UON2")), "", "-UON2_" & .Tables("RECEPTOR").Rows(0)("UON2")) & _
                            IIf(IsDBNull(.Tables("RECEPTOR").Rows(0)("UON3")), "", "-UON3_" & .Tables("RECEPTOR").Rows(0)("UON3")) & _
                            IIf(IsDBNull(.Tables("RECEPTOR").Rows(0)("DEP")), "", "-DEP_" & .Tables("RECEPTOR").Rows(0)("DEP")) & _
                            IIf(IsDBNull(.Tables("RECEPTOR").Rows(0)("RECEPTOR")), "", "-USU_" & .Tables("RECEPTOR").Rows(0)("RECEPTOR"))
                        item.text = IIf(IsDBNull(.Tables("RECEPTOR").Rows(0)("UON1")), "", .Tables("RECEPTOR").Rows(0)("UON1") & "-") & _
                            IIf(IsDBNull(.Tables("RECEPTOR").Rows(0)("UON2")), "", .Tables("RECEPTOR").Rows(0)("UON2") & "-") & _
                            IIf(IsDBNull(.Tables("RECEPTOR").Rows(0)("UON3")), "", .Tables("RECEPTOR").Rows(0)("UON3") & "-") & _
                            IIf(IsDBNull(.Tables("RECEPTOR").Rows(0)("DEP")), "", .Tables("RECEPTOR").Rows(0)("DEP") & "-") & .Tables("RECEPTOR").Rows(0)("DEN").ToString
                        If Not lItems.Count = 2 OrElse Not lItems(1).value = item.value Then
                            lItems.Add(item)
                        End If
                    End If
                End With

                Return lItems
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Public Sub InsertarDiscrepancia(ByVal CiaComp As Integer, ByVal Mensaje As cnMensaje, _
                                        ByVal Factura As Integer, ByVal Linea As Integer, ByVal dtAdjuntos As DataTable)
            Authenticate()
            With Mensaje
                Dim view As New DataView(.Para.Para_UON)
                Dim dtParaUON As DataTable = view.ToTable(True, "UON1", "UON2", "UON3", "DEP", "USU")
                DBServer.CN_InsertarDiscrepancia(CiaComp, .InfoUsuario.ProveCod, .InfoUsuario.Con, _
                    .Tipo, .FechaAlta, .Titulo, .Contenido, .Fecha, .Donde, .Categoria.Id, .Para.Para_UON_UON0, _
                    dtParaUON, .Para.Para_Prove_Tipo, .Para.Para_Prove_Con, _
                    .Para.Para_Material_GMN1, .Para.Para_Material_GMN2, .Para.Para_Material_GMN3, .Para.Para_Material_GMN4, .Para.Para_Material_QA, _
                    .Para.Para_ProceCompra_Anyo, .Para.Para_ProceCompra_GMN1, .Para.Para_ProceCompra_Cod, _
                    .Para.Para_ProcesoCompra_Responsable, .Para.Para_ProcesoCompra_Invitados, _
                    .Para.Para_ProcesoCompra_Compradores, .Para.Para_ProcesoCompra_Proveedores, _
                    .Para.Para_EstructuraCompras_Equipo, .Para.Para_EstructuraCompras_MaterialGS, _
                    .Para.Para_Grupo_EP, .Para.Para_Grupo_GS, .Para.Para_Grupo_PM, .Para.Para_Grupo_QA, .Para.Para_Grupo_SM, _
                    .Para.Para_Grupos_Grupos, dtAdjuntos, Factura, Linea, msSesionId, msIPDir, msPersistID)
            End With
        End Sub
        ''' <summary>
        ''' Obtiene las líneas de una factura
        ''' </summary>
        ''' <param name="sIdioma">Idioma en q mostrar los textos</param> 
        ''' <param name="iFactura">Id de factura </param>
        ''' <returns>Las líneas de la factura</returns>
        ''' <remarks>Llamado desde: Factura.asmx.vb  ; Tiempo máximo: 0,3</remarks>
        ''' <revisado>JVS 22/06/2012</revisado>
        Public Function Obtener_Lineas_Factura(ByVal CiaComp As Integer, ByVal sIdioma As String, _
                                ByVal iFactura As Integer, ByVal Linea As Integer, ByVal UsuFormat As System.Globalization.NumberFormatInfo) As List(Of cnLineaFactura)
            Authenticate()

            Dim dtDatos As DataTable
            Try
                dtDatos = DBServer.CN_Obtener_Lineas_Factura(CiaComp, sIdioma, iFactura, Linea, msSesionId, msIPDir, msPersistID)

                Dim oLineas As New List(Of cnLineaFactura)
                Dim oLineaFact As cnLineaFactura

                For Each row As DataRow In dtDatos.Rows
                    oLineaFact = New cnLineaFactura
                    With oLineaFact
                        .Id = row("LINEA")
                        .NumLinea = row("NUM")
                        .Codigo = row("ART_INT").ToString
                        .Denominacion = row("ART_DEN").ToString
                        If Not IsDBNull(row("CANT_PED")) Then
                            .Cantidad = CType(row("CANT_PED"), Double)
                            .CantidadFormatoUsu = FormatNumber(.Cantidad, UsuFormat) & " " & row("UP")
                        End If
                        .Unidad = row("UP")
                        If Not IsDBNull(row("PREC_UP")) Then
                            .Precio = CType(row("PREC_UP"), Double)
                            .PrecioFormatoUsu = FormatNumber(.Precio, UsuFormat) & " " & row("MON")
                        End If
                        If Not IsDBNull(row("TOTAL_COSTES")) Then
                            .TotalCostes = row("TOTAL_COSTES")
                            .TotalCostesFormatoUsu = FormatNumber(.TotalCostes, UsuFormat) & " " & row("MON")
                        End If
                        If Not IsDBNull(row("TOTAL_DCTOS")) Then
                            .TotalDescuentos = row("TOTAL_DCTOS")
                            .TotalDescuentosFormatoUsu = FormatNumber(.TotalDescuentos, UsuFormat) & " " & row("MON")
                        End If
                        If Not IsDBNull(row("IMPORTE")) Then
                            .Importe = row("IMPORTE")
                            .ImporteFormatoUsu = FormatNumber(.Importe, UsuFormat) & " " & row("MON")
                        End If
                        .Unidad = row("UP")
                        .Moneda = row("MON")
                        .Obs = row("OBS").ToString
                        .PedidoFs = row("PEDIDO_FS").ToString
                        .RefFactura = row("NUM_PED_ERP").ToString
                        .Albaran = row("ALBARAN").ToString
                        .DocSAP = row("NUM_FACTURA_SAP").ToString
                    End With
                    oLineas.Add(oLineaFact)
                Next

                Return oLineas
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Public Sub Cerrar_Discrepancias(ByVal CiaComp As Integer, Optional Factura As Integer = 0, _
                                Optional Linea As Integer = 0, Optional ByVal IdDiscrepancia As Integer = 0)
            Authenticate()

            Try
                DBServer.CN_Cerrar_Discrepancia(CiaComp, Factura, Linea, IdDiscrepancia, msSesionId, msIPDir, msPersistID)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function Comprobar_DiscrepanciasCerradas(ByVal CiaComp As Integer, ByVal IdFactura As Integer, ByVal Linea As Integer) As Integer
            Authenticate()
            Try
                Return DBServer.CN_Comprobar_DiscrepanciasCerradas(CiaComp, IdFactura, Linea)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace