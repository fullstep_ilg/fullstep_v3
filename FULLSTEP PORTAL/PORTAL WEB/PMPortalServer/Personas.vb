Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Personas
        Inherits Fullstep.PMPortalServer.Security
        Private moPersonas As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moPersonas
            End Get

        End Property

        ''' <summary>
        ''' Carga los datos de personas
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCod">Codigo de persona</param>
        ''' <param name="sNom">Nombre de persona</param>
        ''' <param name="sApe">Apellidos de persona</param>
        ''' <param name="sUON1">Unidad organizativa de nivel 1</param>
        ''' <param name="sUON2">Unidad organizativa de nivel 2</param>
        ''' <param name="sUON3">Unidad organizativa de nivel 3</param>
        ''' <param name="sDEP">Departamento</param> 
        ''' <remarks>Llamada desde: script\_common\campos.ascx    script\_common\desglose.ascx     script\_common\detallepersona.aspx   script\_common\usuarios.aspx
        ''' script\certificados\certificado.aspx; Tiempo m�ximo: 0,2</remarks>
        Public Sub LoadData(ByVal lCiaComp As Long, Optional ByVal sCod As String = Nothing, Optional ByVal sNom As String = Nothing, Optional ByVal sApe As String = Nothing, Optional ByVal sUON1 As String = Nothing, Optional ByVal sUON2 As String = Nothing, Optional ByVal sUON3 As String = Nothing, Optional ByVal sDEP As String = Nothing, Optional ByVal bCtlBajaLog As Boolean = True)
            Authenticate()
            If mRemottingServer Then
                moPersonas = DBServer.Personas_Load(lCiaComp, sCod, sNom, sApe, sUON1, sUON2, sUON3, sDEP, msSesionId, msIPDir, msPersistID, bCtlBajaLog)
            Else
                moPersonas = DBServer.Personas_Load(lCiaComp, sCod, sNom, sApe, sUON1, sUON2, sUON3, sDEP, , , , bCtlBajaLog)
            End If
        End Sub
        ''' <summary>
        ''' Carga los datos del receptor
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCodReceptor">Codigo de persona</param>
        ''' <remarks>Llamada desde:Cinsultas.asmx.vb Obtener_DatosReceptor; Tiempo m�ximo: </remarks>
        Public Sub LoadDataReceptor(ByVal lCiaComp As Long, ByVal sCodReceptor As String)
            Authenticate()
            If mRemottingServer Then
                moPersonas = DBServer.Personas_LoadReceptor(lCiaComp, sCodReceptor, msSesionId, msIPDir, msPersistID)
            Else
                moPersonas = DBServer.Personas_LoadReceptor(lCiaComp, sCodReceptor)
            End If
        End Sub


        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace