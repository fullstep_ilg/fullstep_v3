﻿Imports System.Threading
Namespace Fullstep.PMPortalServer

    <Serializable()> _
    Public Class Contrato
        Inherits Security

        Private m_lID As Long
        Private m_sCodigo As String 'Código del contrato
        Private m_sNombre As String
        Private m_lInstancia As Long
        Private m_sCodProve As String
        Private m_sDenProve As String
        Private m_lIdContacto As Long
        Private m_sContacto As String
        Private m_sCodMoneda As String
        Private m_sDenMoneda As String
        Private m_lIdEmpresa As Long
        Private m_sEmpresa As String
        Private m_dFechaInicio As Date
        Private m_dFechaFin As Object
        Private m_iAlerta As Integer
        Private m_dImporteDesde As Object
        Private m_dImporteHasta As Object
        Private m_iRepetirEmail As Integer
        Private m_sNotificados As String
        Private m_sProceso As String
        Private m_sCadenaItems As String 'Cadena de Items separado /
        Private m_sNombreAdjuntoContrato As String
        Private m_sPathContrato As String
        Private m_iEstado As Integer
        Private m_iPeriodoAlerta As Integer
        Private m_iPeriodoRepetirEmail As Integer




#Region "Propiedades"

        Public Property ID() As Long
            Get
                Return m_lID
            End Get

            Set(ByVal Value As Long)
                m_lID = Value
            End Set
        End Property

        Public Property Codigo() As String
            Get
                Return m_sCodigo
            End Get

            Set(ByVal Value As String)
                m_sCodigo = Value
            End Set
        End Property

        Public Property Instancia() As Long
            Get
                Return m_lInstancia
            End Get

            Set(ByVal Value As Long)
                m_lInstancia = Value
            End Set
        End Property

        Public Property Nombre() As String
            Get
                Return m_sNombre
            End Get

            Set(ByVal Value As String)
                m_sNombre = Value
            End Set
        End Property

        Public Property CodProve() As String
            Get
                Return m_sCodProve
            End Get

            Set(ByVal Value As String)
                m_sCodProve = Value
            End Set
        End Property

        Public Property DenProve() As String
            Get
                Return m_sDenProve
            End Get

            Set(ByVal Value As String)
                m_sDenProve = Value
            End Set
        End Property


        Public Property CodMoneda() As String
            Get
                Return m_sCodMoneda
            End Get

            Set(ByVal Value As String)
                m_sCodMoneda = Value
            End Set
        End Property


        Public Property DenMoneda() As String
            Get
                Return m_sDenMoneda
            End Get

            Set(ByVal Value As String)
                m_sDenMoneda = Value
            End Set
        End Property

        Public Property IdContacto() As Long
            Get
                Return m_lIdContacto
            End Get

            Set(ByVal Value As Long)
                m_lIdContacto = Value
            End Set
        End Property

        Public Property Contacto() As String
            Get
                Return m_sContacto
            End Get

            Set(ByVal Value As String)
                m_sContacto = Value
            End Set
        End Property

        Public Property IdEmpresa() As Long
            Get
                Return m_lIdEmpresa
            End Get

            Set(ByVal Value As Long)
                m_lIdEmpresa = Value
            End Set
        End Property

        Public Property Empresa() As String
            Get
                Return m_sEmpresa
            End Get

            Set(ByVal Value As String)
                m_sEmpresa = Value
            End Set
        End Property

        Public Property FechaInicio() As Date
            Get
                Return m_dFechaInicio
            End Get

            Set(ByVal Value As Date)
                m_dFechaInicio = Value
            End Set
        End Property

        Public Property FechaFin() As Object
            Get
                Return m_dFechaFin
            End Get

            Set(ByVal Value As Object)
                m_dFechaFin = Value
            End Set
        End Property

        Public Property Alerta() As Integer
            Get
                Return m_iAlerta
            End Get

            Set(ByVal Value As Integer)
                m_iAlerta = Value
            End Set
        End Property


        Public Property AlertaImporteDesde() As Object
            Get
                Return m_dImporteDesde
            End Get

            Set(ByVal Value As Object)
                m_dImporteDesde = Value
            End Set
        End Property

        Public Property AlertaImporteHasta() As Object
            Get
                Return m_dImporteHasta
            End Get

            Set(ByVal Value As Object)
                m_dImporteHasta = Value
            End Set
        End Property

        Public Property RepetirEmail() As Integer
            Get
                Return m_iRepetirEmail
            End Get

            Set(ByVal Value As Integer)
                m_iRepetirEmail = Value
            End Set
        End Property



        Public Property NotificadosNombreyEmail() As String
            Get
                Return m_sNotificados
            End Get

            Set(ByVal Value As String)
                m_sNotificados = Value
            End Set
        End Property

        Public Property Proceso() As String
            Get
                Return m_sProceso
            End Get

            Set(ByVal Value As String)
                m_sProceso = Value
            End Set
        End Property

        Public Property Items() As String
            Get
                Return m_sCadenaItems
            End Get

            Set(ByVal Value As String)
                m_sCadenaItems = Value
            End Set
        End Property

        Public Property NombreAdjuntoContrato() As String
            Get
                Return m_sNombreAdjuntoContrato
            End Get

            Set(ByVal Value As String)
                m_sNombreAdjuntoContrato = Value
            End Set
        End Property

        Public Property PathContrato() As String
            Get
                Return m_sPathContrato
            End Get

            Set(ByVal Value As String)
                m_sPathContrato = Value
            End Set
        End Property

        Public Property Estado() As Integer
            Get
                Return m_iEstado
            End Get

            Set(ByVal Value As Integer)
                m_iEstado = Value
            End Set
        End Property



#End Region
        ''' <summary>
        ''' Método para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">código de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petición</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo máximo: 0</remarks>
        Friend Sub New(ByRef dbserver As PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub



        ''' <summary>
        ''' Inserta los datos generales del contrato.
        ''' </summary>
        ''' <returns>Devuelve un objeto de error (si ha habido).</returns>
        ''' <remarks>Llamada desde: guardarinstancia.aspx.vb --> GuardarContrato; Tiempo máximo: 1 sg.</remarks>
        Public Function Create_Prev(ByVal lIdSolicitud As Long, ByVal sPeticionario As String, ByRef lIdInstancia As Long) As Object
            Dim oError As Object
            Dim lIdContrato As Long
            Authenticate()

            oError = DBServer.Contrato_Create_Prev(lIdSolicitud, sPeticionario, m_sCodMoneda, m_lIdEmpresa, m_sCodProve, m_lIdContacto, m_dFechaInicio, m_dFechaFin, lIdInstancia, m_lID, m_sCodigo, m_sNombre, m_sProceso, m_sCadenaItems)

            Create_Prev = oError
        End Function


        ''' <summary>
        ''' Cargar las propiedades de la Instancia
        ''' </summary>
        ''' <param name="sIdi">Idioma</param>        
        ''' <param name="bNuevoWorkflow">True Pm False Qa</param>
        ''' <remarks>Llamada desde: Multiples pantallas (todos los oIntancia.load); Tiempo máximo: 0,2</remarks>
        Public Sub Load(ByVal lCiaComp As Long, Optional ByVal sIdi As String = Nothing)
            Authenticate()
            Dim data As DataSet
            Dim row As DataRow

            If mRemottingServer Then
                data = DBServer.Contrato_Load(lCiaComp, m_lID, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Contrato_Load(lCiaComp, m_lID, sIdi)
            End If


            If Not data.Tables.Count = 0 Then
                If Not data.Tables(0).Rows.Count = 0 Then

                    m_lInstancia = data.Tables(0).Rows(0).Item("INSTANCIA")
                    m_sNombre = DBNullToSomething(data.Tables(0).Rows(0).Item("NOMBRE"))
                    m_lIdEmpresa = DBNullToSomething(data.Tables(0).Rows(0).Item("EMP"))
                    m_sCodProve = DBNullToSomething(data.Tables(0).Rows(0).Item("PROVE"))
                    m_lIdContacto = DBNullToSomething(data.Tables(0).Rows(0).Item("CONTACTO"))
                    m_sCodMoneda = DBNullToSomething(data.Tables(0).Rows(0).Item("MON"))
                    m_dFechaInicio = DBNullToSomething(data.Tables(0).Rows(0).Item("FEC_INI"))
                    If Not IsDBNull(data.Tables(0).Rows(0).Item("FEC_FIN")) Then
                        m_dFechaFin = DBNullToSomething(data.Tables(0).Rows(0).Item("FEC_FIN"))
                    End If
                    m_iAlerta = DBNullToSomething(data.Tables(0).Rows(0).Item("ALERTA"))
                    m_iRepetirEmail = DBNullToSomething(data.Tables(0).Rows(0).Item("REPETIR_EMAIL"))
                    m_iPeriodoAlerta = DBNullToSomething(data.Tables(0).Rows(0).Item("PERIODO_ALERTA"))
                    m_iPeriodoRepetirEmail = DBNullToSomething(data.Tables(0).Rows(0).Item("PERIODO_REPETIR_EMAIL"))
                    m_dImporteDesde = DBNullToDbl(data.Tables(0).Rows(0).Item("ALERTA_IMPORTE_DESDE"))
                    m_dImporteHasta = DBNullToDbl(data.Tables(0).Rows(0).Item("ALERTA_IMPORTE_HASTA"))
                    m_sEmpresa = DBNullToSomething(data.Tables(0).Rows(0).Item("EMPRESA"))
                    m_sDenProve = DBNullToSomething(data.Tables(0).Rows(0).Item("PROVEEDOR"))
                    m_sDenMoneda = DBNullToSomething(data.Tables(0).Rows(0).Item("MONEDA"))
                    m_sContacto = DBNullToSomething(data.Tables(0).Rows(0).Item("CONTACTO_DEN"))

                    m_sNombreAdjuntoContrato = DBNullToSomething(data.Tables(0).Rows(0).Item("NOMBRE_ADJUNTO_CONTRATO"))
                    m_iEstado = DBNullToInteger(data.Tables(0).Rows(0).Item("ESTADO_CONTRATO"))

                    Dim sCadena As String = ""
                    If data.Tables.Count > 1 Then
                        If data.Tables(1).Rows.Count Then
                            For i As Integer = 0 To data.Tables(1).Rows.Count - 1
                                If sCadena <> "" Then sCadena = sCadena & ";"
                                sCadena = sCadena & data.Tables(1).Rows(i).Item("NOMBRE") & "(" & data.Tables(1).Rows(i).Item("EMAIL") & ")"
                            Next
                        End If
                    End If
                    m_sNotificados = sCadena



                End If
            End If
            data = Nothing
        End Sub

        ''' <summary>
        ''' Devuelve el ID de un contrato a partir del Id de la instancia
        ''' </summary>
        ''' <param name="lIdInstancia">ID Instancia</param>      
        ''' <returns>ID contrato</returns>
        ''' <remarks>Llamada desde=ComprobarAprobContratos.aspx.vb; Tiempo máximo:0,1seg.</remarks>
        Public Function BuscarIdContrato(ByVal lIdInstancia As Long) As Long
            Dim lResultado As Long

            lResultado = DBServer.Contrato_BuscarIdContrato(lIdInstancia)

            Return lResultado
        End Function


    End Class
End Namespace
