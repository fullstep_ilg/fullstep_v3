﻿Namespace Fullstep.PMPortalServer

    <Serializable()> _
    Public Class TiposPedido
        Inherits Fullstep.PMPortalServer.Security

        ''' <summary>
        ''' Carga en un dataset los tipos de pedido
        ''' </summary>
        ''' <param name="sIdi">Idioma en el que hay que devolver los datos</param>
        ''' <returns>Dataset con los tipos de pedido</returns>
        ''' <remarks>Llamada desde: TiposPedidoServer.aspx.vb; Tiempo máximo: 0,1 sg</remarks>
        Public Function LoadData(ByVal lCiaComp As Long, ByVal sIdi As String) As DataSet
            Authenticate()
            LoadData = DBServer.TiposPedido_Get(lCiaComp, sIdi)
        End Function

        ''' <summary>
        ''' Método para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">código de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petición</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo máximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace
