Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Idioma
        Inherits Security

        Private msCod As String
        Private msDen As String

        Property Den() As String
            Get
                Den = msDen
            End Get
            Set(ByVal Value As String)
                msDen = Value
            End Set
        End Property

        Property Cod() As String
            Get
                Cod = msCod
            End Get
            Set(ByVal Value As String)
                msCod = Value
            End Set
        End Property
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

    End Class
End Namespace