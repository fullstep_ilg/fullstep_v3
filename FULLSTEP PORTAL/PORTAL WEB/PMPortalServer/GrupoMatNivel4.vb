Namespace Fullstep.PMPortalServer
    <Serializable()> _
        Public Class GrupoMatNivel4
        Inherits Security

        Private moData As DataSet
        Private msGMN1Cod As String
        Private msGMN1Den As String
        Private msGMN2Cod As String
        Private msGMN2Den As String
        Private msGMN3Cod As String
        Private msGMN3Den As String
        Private msCod As String
        Private msDen As String


        Property Cod() As String
            Get
                Return msCod
            End Get
            Set(ByVal Value As String)
                msCod = Value
            End Set
        End Property
        Property Den() As String
            Get
                Return msDen
            End Get
            Set(ByVal Value As String)
                msDen = Value
            End Set
        End Property

        Property GMN1Cod() As String
            Get
                Return msGMN1Cod
            End Get
            Set(ByVal Value As String)
                msGMN1Cod = Value
            End Set
        End Property
        Property GMN1Den() As String
            Get
                Return msGMN1Den
            End Get
            Set(ByVal Value As String)
                msGMN1Den = Value
            End Set
        End Property

        Property GMN2Cod() As String
            Get
                Return msGMN2Cod
            End Get
            Set(ByVal Value As String)
                msGMN2Cod = Value
            End Set
        End Property
        Property GMN2Den() As String
            Get
                Return msGMN2Den
            End Get
            Set(ByVal Value As String)
                msGMN2Den = Value
            End Set
        End Property

        Property GMN3Cod() As String
            Get
                Return msGMN3Cod
            End Get
            Set(ByVal Value As String)
                msGMN3Cod = Value
            End Set
        End Property
        Property GMN3Den() As String
            Get
                Return msGMN3Den
            End Get
            Set(ByVal Value As String)
                msGMN3Den = Value
            End Set
        End Property

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moData
            End Get

        End Property

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace

