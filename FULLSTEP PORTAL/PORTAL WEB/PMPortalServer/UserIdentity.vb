
Imports Fullstep.PMPortalServer.Root
Imports System.Security.Principal
Imports System.Threading
Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Friend Class UserIdentity
        Inherits Security
        Implements IIdentity
        Private mUserProfile As String = ""
        Private mUserActions As Fullstep.PMPortalDatabaseServer.Root.UserIdentity_Actions

#Region "Business methods"
        Public Function UserActions() As Fullstep.PMPortalDatabaseServer.Root.UserIdentity_Actions
            Return mUserActions
        End Function
        Public ReadOnly Property AuthenticationType() As String Implements System.Security.Principal.IIdentity.AuthenticationType
            Get
                Return "FSWS"
            End Get
        End Property
        ''' <summary>
        ''' Propiedad del interfaz IIdentity
        ''' </summary>
        ''' <returns>Si esta autentificado</returns>
        ''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
        Public ReadOnly Property IsAuthenticated() As Boolean Implements System.Security.Principal.IIdentity.IsAuthenticated
            Get
                If Len(msUserCode) > 0 Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property
        ''' <summary>
        ''' Propiedad del interfaz IIdentity
        ''' </summary>
        ''' <returns>usuario</returns>
        ''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
        Public ReadOnly Property Name() As String Implements System.Security.Principal.IIdentity.Name
            Get
                Return msUserCode
            End Get
        End Property
        Friend Function HasAction(ByVal Action As String) As Boolean
            Dim i As Integer = 0
            Dim x As Integer
            If mUserActions.ID Is Nothing Then Return False
            x = mUserActions.ID.Length
            While i < x
                If mUserActions.ID(i).ToString.Equals(Action) Then
                    Return True
                Else
                    i = i + 1
                End If
            End While
            Return False
        End Function
#End Region

#Region " Constructor"
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)

            If mRemottingServer Then
                mUserActions = dbserver.UserIdentity_GetActions(msSesionId, msIPDir, msPersistID)
            Else
                mUserActions = dbserver.UserIdentity_GetActions
            End If

        End Sub
#End Region

    End Class

End Namespace