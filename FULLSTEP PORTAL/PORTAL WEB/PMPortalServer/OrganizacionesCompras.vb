Namespace Fullstep.PMPortalServer
    <Serializable()> _
Public Class OrganizacionesCompras
        Inherits Fullstep.PMPortalServer.Security
        Private m_sCodOrganizacion As String
        Private moOrganizacionesCompras As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moOrganizacionesCompras
            End Get

        End Property

        ''' <summary>
        ''' carga las organizaciones
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <remarks>Llamada desde: script\_common\articulosserver.aspx  script\_common\campos.ascx   script\_common\desglose.ascx  script\_common\proveedores.aspx; Tiempo m�ximo: 0,2</remarks>
        Public Sub LoadData(ByVal lCiaComp As Long)
            Authenticate()
            If mRemottingServer Then
                moOrganizacionesCompras = DBServer.OrganizacionesCompras_Load(lCiaComp, msSesionId, msIPDir, msPersistID)
            Else
                moOrganizacionesCompras = DBServer.OrganizacionesCompras_Load(lCiaComp)
            End If
        End Sub
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

    End Class
End Namespace
