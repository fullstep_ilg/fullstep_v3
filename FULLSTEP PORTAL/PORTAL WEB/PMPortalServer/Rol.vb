Namespace Fullstep.PMPortalServer

    <Serializable()> _
Public Class Rol
        Inherits Security

        Private mlId As Integer
        Private msDen As String
        Private miTipo As Short
        Private msPer As String
        Private moParticipantes As DataSet
        Private mlBloque As Integer
        Private msBloqueDen As String

        Property Id() As Integer
            Get
                Return mlId
            End Get
            Set(ByVal Value As Integer)
                mlId = Value
            End Set
        End Property

        Property Den() As String
            Get
                Return msDen
            End Get
            Set(ByVal Value As String)
                msDen = Value
            End Set
        End Property
        Property Tipo() As Short
            Get
                Return miTipo
            End Get
            Set(ByVal Value As Short)
                miTipo = Value
            End Set
        End Property

        Property Per() As String
            Get
                Return msPer
            End Get
            Set(ByVal Value As String)
                msPer = Value

            End Set
        End Property
        Public Property Participantes() As DataSet
            Get
                Participantes = moParticipantes
            End Get
            Set(ByVal Value As DataSet)
                moParticipantes = Value
            End Set
        End Property

        Property Bloque() As Integer
            Get
                Return mlBloque
            End Get
            Set(ByVal Value As Integer)
                mlBloque = Value
            End Set
        End Property

        ''' <summary>
        ''' Carga las posibles acciones de un rol
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: script\solicitudes\detalleSolicitud.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Function CargarAcciones(ByVal lCiaComp As Long, ByVal sIdi As String, Optional ByVal SoloRechazadas As Boolean = False) As DataSet
            Authenticate()

            Dim oData As DataSet
            If mRemottingServer Then
                oData = DBServer.Rol_LoadAcciones(lCiaComp, mlId, mlBloque, sIdi, SoloRechazadas, msSesionId, msIPDir, msPersistID)
            Else
                oData = DBServer.Rol_LoadAcciones(lCiaComp, mlId, mlBloque, sIdi, SoloRechazadas)
            End If
            Return oData
        End Function

        ''' <summary>
        ''' Funcion que carga los listados relacionados con un rol
        ''' </summary>
        ''' <param name="sIdi">Idioma de la aplicaci�n</param>
        ''' <param name="bCopia">Variable booleana que indica si se debe hacer una copia</param>
        ''' <returns>Un DataSet con los datos de los listados relacionados con el rol</returns>
        ''' <remarks>
        ''' Llamada desdE: PmWeb/NWGestionInstancia/Page_Load, PmWeb/NWDetalleSolicitud/Page_Load
        ''' Tiempo m�ximo: 0,3 seg</remarks>
        Public Function CargarListados(ByVal lCiaComp As Long, Optional ByVal sIdi As String = "SPA", Optional ByVal bCopia As Boolean = False) As DataSet
            Dim oData As DataSet

            Authenticate()

            If mRemottingServer Then
                oData = DBServer.Rol_LoadListados(lCiaComp, mlId, mlBloque, sIdi, bCopia, msSesionId, msIPDir, msPersistID)
            Else
                oData = DBServer.Rol_LoadListados(lCiaComp, mlId, mlBloque, sIdi, bCopia)
            End If

            Return oData
        End Function
        ''' <summary>
        ''' Obtiene los datos de los participantes en un rol
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sIdioma">Idioma</param>
        ''' <param name="lInstancia">Instancia</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: solicitudes\detalleSolicitud.aspx    script\contratos\GestionContrato.aspx; Tiempo m�ximo: 0,2</remarks>
        Public Function CargarParticipantes(ByVal lCiaComp As Long, ByVal sIdioma As String, Optional ByVal lInstancia As Integer = Nothing) As DataSet
            Dim oData As DataSet

            Authenticate()

            If mRemottingServer Then
                oData = DBServer.Rol_LoadParticipantes(lCiaComp, sIdioma, mlId, mlBloque, lInstancia, msSesionId, msIPDir, msPersistID)
            Else
                oData = DBServer.Rol_LoadParticipantes(lCiaComp, sIdioma, mlId, mlBloque, lInstancia)
            End If

            moParticipantes = oData

        End Function

        Public Sub CargarRolActual(ByVal lCiaComp As Long, ByVal lIdBloque As Integer, ByVal codProve As String, ByVal idContacto As Integer)
            Dim oData As DataSet

            Authenticate()

            If mRemottingServer Then
                oData = DBServer.Roles_Load(lCiaComp, lIdBloque, codProve, idContacto, msSesionId, msIPDir, msPersistID)
            Else
                oData = DBServer.Roles_Load(lCiaComp, lIdBloque, codProve, idContacto)
            End If

            If oData.Tables.Count > 0 AndAlso oData.Tables(0).Rows.Count > 0 Then
                mlBloque = lIdBloque
                With oData.Tables(0).Rows(0)
                    mlId = .Item("ROL")
                    msDen = .Item("DEN").ToString
                    miTipo = .Item("TIPO").ToString
                End With
            End If
        End Sub

        ''' <summary>
        ''' Funcion que carga el Id de la accion "guardar", si es q existe.
        ''' Tarea 1959 Vinculaciones Desglose
        ''' Tras un guardado con mover fila y/o eliminar fila se ha actualiza la tabla INSTANCIA_LINEAS_VINCULAR y la q era la
        ''' linea x ahora es la linea y. Las diferentes llamadas a popupDesgloseClickEvent se crean una vez al abrir el detalle
        ''' de la solicitud y por ello el index tras el guardado ha dejado de ser valido. 
        ''' Con este id en NWgestionInstancia.aspx le indico si debe usar la linea de popupDesgloseClickEvent o calcular la linea
        ''' a partir de la q tenga popupDesgloseClickEvent.
        ''' </summary>
        ''' <returns>Id de la accion "guardar", si es q existe</returns>
        ''' <remarks>Llamada desde:NWgestionInstancia.aspx; Tiempo m�ximo:0,1</remarks>
        Public Function CargarIdGuardar(ByVal lCiaComp As Long) As Long
            Dim oData As DataSet
            Authenticate()
            oData = DBServer.Rol_LoadIdGuardar(lCiaComp, mlId, mlBloque)

            If oData.Tables(0).Rows.Count = 0 Then
                Return 0
            Else
                Return oData.Tables(0).Rows(0).Item("ACCION")
            End If
        End Function
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)

        End Sub
    End Class
End Namespace