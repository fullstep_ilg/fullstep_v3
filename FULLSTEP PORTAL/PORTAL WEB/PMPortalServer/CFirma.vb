﻿Imports es.mityc.javasign.trust
Imports es.mityc.javasign.xml.xades.policy
Imports es.mityc.firmaJava.libreria.xades
Imports es.mityc.javasign.xml.refs
Imports java.io
Imports java.security.cert
Imports java.security
Imports javax.xml.parsers
Imports es.mityc.javasign.pkstore
Imports es.mityc.javasign.pkstore.keystore
Imports sviudes.blogspot.com
Imports java.util
Imports es.mityc.firmaJava.libreria.utilidades
Imports System.Configuration
Imports System.IO

Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class CFirma
        Inherits Security

        ''' <summary>
        ''' Constructura del objeto. Solo llama a la constructora de su superclase.
        ''' </summary>
        ''' <remarks>Llamada desde:¿?
        ''' Tiempo máximo: menor a 0,1
        ''' Revisado por: auv. 18/05/2012</remarks>
        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

        ''' <summary>
        ''' Funcionalidad que permite firmar un fichero que sea un documento XML facturae, dado un certificado digital
        ''' con clave privada y dada la clave del mismo
        ''' </summary>
        ''' <param name="psNomFichFacSinFirma">Nombre+Path del fichero a firmar</param>
        ''' <param name="psNomFichFacConFirma">Nombre+Path del fichero donde se dejará el documento firmado</param>
        ''' <param name="psNomFichCertDig">Nombre+Path del fichero donde reside el certificado digital</param>
        ''' <param name="psValClaveCertDig">Clave del certificado digital</param>
        ''' <remarks>Llamada desde:¿?
        ''' Tiempo máximo: Variable, en función del fichero a firmar principalmente</remarks>
        ''' <remarks>Revisado por: auv. 18/05/2012</remarks>
        Public Sub firmaFacV3_2Manual(ByVal psNomFichFacSinFirma As String, ByVal psNomFichFacConFirma As String, _
                                            ByVal psNomFichCertDig As String, ByVal psValClaveCertDig As String)
            Dim privateKey As PrivateKey = Nothing
            Dim provider As Provider = Nothing
            Dim certificate As java.security.cert.X509Certificate = Nothing

            certificate = Me.loadCertificate(psNomFichCertDig, psValClaveCertDig, privateKey, provider)
            If Not (IsDBNull(certificate)) And Not (IsNothing(certificate)) Then
                'Si encontramos el certificado.
                Try
                    'Política de firma (Con las librerías JAVA, esto se define en tiempo de ejecución)
                    TrustFactory.instance = es.mityc.javasign.trust.TrustExtendFactory.newInstance
                    TrustFactory.truster = es.mityc.javasign.trust.MyPropsTruster.getInstance
                    PoliciesManager.POLICY_SIGN = New es.mityc.javasign.xml.xades.policy.facturae.Facturae31Manager
                    PoliciesManager.POLICY_VALIDATION = New es.mityc.javasign.xml.xades.policy.facturae.Facturae31Manager

                    'Crear datos a firmar
                    Dim dataToSign As DataToSign = New DataToSign
                    dataToSign.setXadesFormat(EnumFormatoFirma.XAdES_BES) 'XAdES-EPES
                    dataToSign.setEsquema(XAdESSchemas.XAdES_132)
                    dataToSign.setPolicyKey("facturae31") 'Da igual lo que pongamos aquí, la política de firma se define arriba
                    dataToSign.setAddPolicy(True)
                    dataToSign.setXMLEncoding("UTF-8")
                    dataToSign.setEnveloped(True)
                    dataToSign.addObject(New ObjectToSign(New AllXMLToSign(), "Descripcion del documento", Nothing, "text/xml", Nothing))
                    dataToSign.setDocument(Me.loadXML(psNomFichFacSinFirma))

                    'Firmar
                    Dim res As Object() = New FirmaXML().signFile(certificate, dataToSign, privateKey, provider)

                    'Guardamos la firma a un fichero
                    UtilidadTratarNodo.saveDocumentToOutputStream(res(0), New FileOutputStream(psNomFichFacConFirma), True)
                Catch ex As Exception
                    Throw New Exception("Error en el proceso de firmado, message =" & ex.Message & "|||||StackTrace =" & ex.StackTrace)
                End Try
            Else
                Throw New Exception("No se pudo obtener el objeto certificado")
            End If
        End Sub

        ''' <summary>
        ''' Funcionalidad que permite firmar un fichero que sea un documento XML facturae, dada la clave del certificado
        ''' y el directorio de la empresa cuyo certificado digital se va a emplear.
        ''' </summary>
        ''' <param name="psDirectorioEmp">Es el nombre que identifica el directorio digital de la empresa</param>
        ''' <param name="psValClaveCertDig">Clave del certificado digital</param>
        ''' <param name="psNomFichSinFirma">Nombre+Path del fichero a firmar</param>
        ''' <param name="psNomFichConFirma">Nombre+Path del fichero donde se dejará el documento firmado</param>
        ''' <remarks>Llamada desde:¿?
        ''' Tiempo máximo: Variable, en función del fichero a firmar principalmente</remarks>
        ''' <remarks>Revisado por: auv. 18/05/2012</remarks>
        Public Sub firmaFacV3_2Automatica(ByVal psDirectorioEmp As String, ByVal psValClaveCertDig As String, _
                                               ByVal psNomFichSinFirma As String, ByVal psNomFichConFirma As String)
            Dim privateKey As PrivateKey = Nothing
            Dim provider As Provider = Nothing
            Dim certificate = Nothing
            Dim directorioRaiz As String = Nothing
            Dim directorioEmpresa As String = Nothing
            Dim nombreFicheroCertificadoConClavePrivada As String = Nothing
            Dim fos As FileOutputStream = Nothing

            directorioRaiz = IIf(IsNothing(System.Configuration.ConfigurationManager.AppSettings("Certificados")), "", System.Configuration.ConfigurationManager.AppSettings("Certificados"))
            directorioEmpresa = directorioRaiz & "\" & psDirectorioEmp
            If hayPfxOP12(directorioEmpresa) Then
                nombreFicheroCertificadoConClavePrivada = directorioEmpresa & "\" & nombrePfxOP12(directorioEmpresa)
                certificate = Me.loadCertificate(nombreFicheroCertificadoConClavePrivada, psValClaveCertDig, privateKey, provider)
                If Not (IsDBNull(certificate)) And Not (IsNothing(certificate)) Then
                    Try
                        'Política de firma (Con las librerías JAVA, esto se define en tiempo de ejecución)
                        TrustFactory.instance = es.mityc.javasign.trust.TrustExtendFactory.newInstance
                        TrustFactory.truster = es.mityc.javasign.trust.MyPropsTruster.getInstance
                        PoliciesManager.POLICY_SIGN = New es.mityc.javasign.xml.xades.policy.facturae.Facturae31Manager
                        PoliciesManager.POLICY_VALIDATION = New es.mityc.javasign.xml.xades.policy.facturae.Facturae31Manager
                        'Crear datos a firmar
                        Dim dataToSign As DataToSign = New DataToSign
                        dataToSign.setXadesFormat(EnumFormatoFirma.XAdES_BES) 'XAdES-EPES
                        dataToSign.setEsquema(XAdESSchemas.XAdES_132)
                        dataToSign.setPolicyKey("facturae31") 'Da igual lo que pongamos aquí, la política de firma se define arriba
                        dataToSign.setAddPolicy(True)
                        dataToSign.setXMLEncoding("UTF-8")
                        dataToSign.setEnveloped(True)
                        dataToSign.addObject(New ObjectToSign(New AllXMLToSign(), "Descripcion del documento", Nothing, "text/xml", Nothing))
                        dataToSign.setDocument(Me.loadXML(psNomFichSinFirma))
                        'Firmar
                        Dim res As Object() = New FirmaXML().signFile(certificate, dataToSign, privateKey, provider)
                        'Guardamos la firma a un fichero
                        fos = New FileOutputStream(psNomFichConFirma)
                        UtilidadTratarNodo.saveDocumentToOutputStream(res(0), fos, True)
                        fos.close()
                    Catch ex As Exception
                        Throw New Exception("Error en el proceso de firmado, message =" & ex.Message & "|||||StackTrace =" & ex.StackTrace)
                    End Try
                Else
                    Throw New Exception("No se pudo obtener el objeto certificado")
                End If
            Else
                Throw New Exception("PFX o P12 no encontrado en " & directorioEmpresa)
            End If
        End Sub

        ''' <summary>
        ''' Dado el path donde reside el fichero con el certificado digital y dada su clave, se devuelve un objeto con el certificado digital.
        ''' Además se construye el objeto de la clave privada y se construye el proveedor.
        ''' </summary>
        ''' <param name="path">Nombre+path donde reside el certificado digital</param>
        ''' <param name="password">Clave del certificado digital</param>
        ''' <param name="privateKey">Objeto clave privada que será devuelto construido por la función</param>
        ''' <param name="provider">Proveedor que será devuelto construido por la función</param>
        ''' <returns>Devuelve el objeto que contiene el certificado digital</returns>
        ''' <remarks>Llamada desde: CFirma.firmaFacV3_2Manual; CFirmafirmaFacV3_2Automatica
        ''' Tiempo máximo: menor a 0,1</remarks>
        ''' <remarks>Revisado por: auv. 18/05/2012</remarks>
        Private Function loadCertificate(ByVal path As String, ByVal password As String, ByRef privateKey As PrivateKey, ByRef provider As Provider) As java.security.cert.X509Certificate
            Dim resultado As java.security.cert.X509Certificate
            resultado = Nothing
            Dim certificate As java.security.cert.X509Certificate = Nothing

            'Cargar certificado de fichero PFX o P12
            Dim ks As KeyStore = KeyStore.getInstance("PKCS12")
            ks.load(New BufferedInputStream(New FileInputStream(path)), password.ToCharArray())
            Dim storeManager As IPKStoreManager = New KSStore(ks, New PassStoreKS(password))
            Dim certificates As List = storeManager.getSignCertificates()

            'Si encontramos el certificado...   
            If certificates.size = 1 Then
                certificate = certificates.get(0)
                resultado = certificate

                'Obtención de la clave privada asociada al certificado
                privateKey = storeManager.getPrivateKey(certificate)

                'Obtención del provider encargado de las labores criptográficas
                provider = storeManager.getProvider(certificate)
            End If

            Return resultado
        End Function

        ''' <summary>
        ''' Dado el lugar donde reside un fichero XML se construye un objeto con su contenido y se devuelve como resultado de la función
        ''' </summary>
        ''' <param name="path">Nombre+path donde reside el fichero XML</param>
        ''' <returns>Devuelve un objeto que contiene el documento XML</returns>
        ''' <remarks>Llamada desde: CFirma.firmaFacV3_2Manual; CFirmafirmaFacV3_2Automatica
        ''' Tiempo máximo: Variable, en función del fichero</remarks>
        ''' <remarks>Revisado por: auv. 18/05/2012</remarks>
        Private Function loadXML(ByVal path As String) As org.w3c.dom.Document
            Dim resultado As org.w3c.dom.Document
            resultado = Nothing
            Dim bis As BufferedInputStream = Nothing
            Dim fis As FileInputStream = Nothing

            Dim dbf As DocumentBuilderFactory = DocumentBuilderFactory.newInstance()
            dbf.setNamespaceAware(True)
            fis = New FileInputStream(path)
            bis = New BufferedInputStream(fis)
            resultado = dbf.newDocumentBuilder().parse(bis)
            fis.close()
            bis.close()

            Return resultado
        End Function

        ''' <summary>
        ''' Función que indica si se detecta un fichero con extensión ".pfx" o ".p12" o no.
        ''' </summary>
        ''' <param name="pathDir">Directorio en el que se debe buscar un fichero con extensión ".pfx" o ".p12"</param>
        ''' <returns>Devuelve un booleano, True si hay al menos un fichero con extensión ".pfx" o ".p12" en el directorio parámetro, falso
        ''' en otro caso</returns>
        ''' <remarks>Llamado desde: CFacturaePDF.firmaFacV3_2Automatica
        ''' Tiempo máximo: variable, depende del número de ficheros que existan en el directorio parámetro</remarks>
        ''' <remarks>Revisado por: auv. 30/05/2012</remarks>
        Private Function hayPfxOP12(ByVal pathDir As String) As Boolean
            Dim hay As Boolean = False
            Dim contFich As Integer = 0
            Dim di As DirectoryInfo = Nothing
            Dim diari As FileInfo() = Nothing
            Dim encontrado As Boolean = False

            di = New DirectoryInfo(pathDir)
            diari = di.GetFiles
            encontrado = False
            contFich = LBound(diari)
            While contFich <= UBound(diari) And Not encontrado
                If diari(contFich).Name.Length > 4 Then
                    If diari(contFich).Name.Substring(diari(contFich).Name.Length - 4, 4) = ".pfx" Or diari(contFich).Name.Substring(diari(contFich).Name.Length - 4, 4) = ".p12" Then
                        encontrado = True
                    Else
                        contFich = contFich + 1
                    End If
                Else
                    contFich = contFich + 1
                End If
            End While

            hay = encontrado

            Return hay
        End Function

        ''' <summary>
        ''' Función que devolverá el nombre sin path del primer fichero ".pfx" o ".p12" que encuentre. Si no encuentra ninguno devolvera un 
        ''' string vacío.
        ''' </summary>
        ''' <param name="pathDir">Directorio en el que se debe buscar un fichero con extensión ".pfx" o ".p12"</param>
        ''' <returns>Devuelve un string, vacío si no hay un fichero con extensión ".pfx" o ".p12", si lo hay, devuelve el nombre del primer
        ''' fichero encontrado con extensión ".pfx" o ".p12"</returns>
        ''' <remarks>Llamado desde: CFacturaePDF.firmaFacV3_2Automatica
        ''' Tiempo máximo: variable, depende del número de ficheros que existan en el directorio parámetro</remarks>
        ''' <remarks>Revisado por: auv. 30/05/2012</remarks>
        Private Function nombrePfxOP12(ByVal pathDir As String) As String
            Dim nombre As String = ""
            Dim contFich As Integer = 0
            Dim di As DirectoryInfo = Nothing
            Dim diari As FileInfo() = Nothing
            Dim encontrado As Boolean = False

            di = New DirectoryInfo(pathDir)
            diari = di.GetFiles
            encontrado = False
            contFich = LBound(diari)
            While contFich <= UBound(diari) And Not encontrado
                If diari(contFich).Name.Length > 4 Then
                    If diari(contFich).Name.Substring(diari(contFich).Name.Length - 4, 4) = ".pfx" Or diari(contFich).Name.Substring(diari(contFich).Name.Length - 4, 4) = ".p12" Then
                        encontrado = True
                    Else
                        contFich = contFich + 1
                    End If
                Else
                    contFich = contFich + 1
                End If
            End While

            If encontrado Then
                nombre = diari(contFich).Name
            End If

            Return nombre
        End Function

    End Class
End Namespace
