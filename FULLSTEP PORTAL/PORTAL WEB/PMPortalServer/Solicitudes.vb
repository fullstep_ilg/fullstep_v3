Namespace Fullstep.PMPortalServer

    <Serializable()> _
    Public Class Solicitudes

        Inherits Fullstep.PMPortalServer.Security

        Private moSolicitudes As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moSolicitudes
            End Get

        End Property

        ''' <summary>
        ''' carga las solicitudes
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <remarks>Llamada desde: solicitudes\seguimientoSolicitudes.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Sub LoadData(ByVal lCiaComp As Long, ByVal sProve As String, Optional ByVal sIdi As String = Nothing)
            Authenticate()
            If mRemottingServer Then
                moSolicitudes = DBServer.Solicitudes_Load(lCiaComp, sProve, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                moSolicitudes = DBServer.Solicitudes_Load(lCiaComp, sProve, sIdi)
            End If
        End Sub

        Public Function DevolverSolicitudesAlta(ByVal lCiaComp As Long, ByVal codProve As String, ByVal idContacto As Integer, Optional ByVal tipoSolicitud As Integer = Nothing, Optional ByVal idioma As String = Nothing) As DataSet
            Dim dsSolicitudes As DataSet

            Authenticate()
            If mRemottingServer Then
                dsSolicitudes = DBServer.Solicitudes_LoadAlta(lCiaComp, codProve, idContacto, tipoSolicitud, idioma, msSesionId, msIPDir, msPersistID)
            Else
                dsSolicitudes = DBServer.Solicitudes_LoadAlta(lCiaComp, codProve, idContacto, tipoSolicitud, idioma)
            End If
            Return dsSolicitudes
        End Function

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

    End Class
End Namespace