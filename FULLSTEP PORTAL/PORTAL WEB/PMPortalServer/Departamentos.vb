Namespace Fullstep.PMPortalServer
    <Serializable()> _
Public Class Departamentos
        Inherits Fullstep.PMPortalServer.Security

        Private moDepartamentos As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moDepartamentos
            End Get

        End Property

        ''' <summary>
        ''' carga todos los departamentos
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="NivelACargar">De q nivel de departamento cargar</param>
        ''' <param name="UON1">Unidad organizativa de nivel 1</param>
        ''' <param name="UON2">Unidad organizativa de nivel 2</param>
        ''' <param name="UON3">Unidad organizativa de nivel 3</param>
        ''' <param name="sUsu">Usuario</param>
        ''' <remarks>Llamada desde: Consultas.asmx.vb; Tiempo m�ximo: 0,1 sg;</remarks>
        Public Sub LoadData(ByVal lCiaComp As Long, Optional ByVal NivelACargar As String = Nothing, Optional ByVal UON1 As String = Nothing, Optional ByVal UON2 As String = Nothing, Optional ByVal UON3 As String = Nothing, Optional ByVal sUsu As String = Nothing)
            Authenticate()
            If mRemottingServer Then
                moDepartamentos = DBServer.Departamentos_Load(lCiaComp, NivelACargar, UON1, UON2, UON3, sUsu, msSesionId, msIPDir, msPersistID)
            Else
                moDepartamentos = DBServer.Departamentos_Load(lCiaComp, NivelACargar, UON1, UON2, UON3, sUsu)
            End If
        End Sub

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

    End Class
End Namespace