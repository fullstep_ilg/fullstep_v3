﻿Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Persona
        Inherits Fullstep.PMPortalServer.Security

        Private _sCodigo As String
        Private _sNombre As String
        Private _sApellidos As String
        Private _sTelefono As String
        Private _sFax As String
        Private _sEMail As String
        Private _sDepartamento As String
        Private _sDenDepartamento As String
        Private _sUON1 As String
        Private _sUON2 As String
        Private _sUON3 As String
        Private _sCargo As String

#Region "Propiedades"
        Public Property Codigo As String
            Get
                Return _sCodigo
            End Get
            Set(value As String)
                _sCodigo = value
            End Set
        End Property
        Public Property Nombre As String
            Get
                Return _sNombre
            End Get
            Set(value As String)
                _sNombre = value
            End Set
        End Property
        Public Property Apellidos As String
            Get
                Return _sApellidos
            End Get
            Set(value As String)
                _sApellidos = value
            End Set
        End Property
        Public Property Telefono As String
            Get
                Return _sTelefono
            End Get
            Set(value As String)
                _sTelefono = value
            End Set
        End Property
        Public Property Fax As String
            Get
                Return _sFax
            End Get
            Set(value As String)
                _sFax = value
            End Set
        End Property
        Public Property EMail As String
            Get
                Return _sEMail
            End Get
            Set(value As String)
                _sEMail = value
            End Set
        End Property
        Public Property Departamento As String
            Get
                Return _sDepartamento
            End Get
            Set(value As String)
                _sDepartamento = value
            End Set
        End Property
        Public Property DenDepartamento As String
            Get
                Return _sDenDepartamento
            End Get
            Set(value As String)
                _sDenDepartamento = value
            End Set
        End Property
        Public Property UON1 As String
            Get
                Return _sUON1
            End Get
            Set(value As String)
                _sUON1 = value
            End Set
        End Property
        Public Property UON2 As String
            Get
                Return _sUON2
            End Get
            Set(value As String)
                _sUON2 = value
            End Set
        End Property
        Public Property UON3 As String
            Get
                Return _sUON3
            End Get
            Set(value As String)
                _sUON3 = value
            End Set
        End Property
        Public Property Cargo As String
            Get
                Return _sCargo
            End Get
            Set(value As String)
                _sCargo = value
            End Set
        End Property
#End Region

#Region "Constructor"

        ''' <summary>
        ''' Método para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">código de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petición</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo máximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

#End Region

#Region "Métodos públicos"

        ''' <summary>Carga los datos de una persona</summary>
        ''' <param name="lCiaComp">Id de la compañia</param>
        ''' <param name="sCod">Codigo de la persona</param>    
        ''' <param name="bBajaLog">Incluir o no las bajas lógicas</param>
        ''' <remarks>Llamada desde: campos.ascx.Page_Load</remarks>
        Public Sub LoadData(ByVal lCiaComp As Long, ByVal sCod As String, Optional ByVal bBajaLog As Boolean = False)
            Authenticate()

            Dim dtDatos As DataTable = DBServer.Persona_Load(lCiaComp, sCod, bBajaLog)
            If Not dtDatos Is Nothing AndAlso dtDatos.Rows.Count > 0 Then
                _sCodigo = dtDatos.Rows(0)("COD")
                If Not dtDatos.Rows(0).IsNull("NOM") Then _sNombre = dtDatos.Rows(0)("NOM")
                _sApellidos = dtDatos.Rows(0)("APE")
                If Not dtDatos.Rows(0).IsNull("TFNO") Then _sTelefono = dtDatos.Rows(0)("TFNO")
                If Not dtDatos.Rows(0).IsNull("FAX") Then _sFax = dtDatos.Rows(0)("FAX")
                If Not dtDatos.Rows(0).IsNull("EMAIL") Then _sEMail = dtDatos.Rows(0)("EMAIL")
                _sDepartamento = dtDatos.Rows(0)("DEP")
                If Not dtDatos.Rows(0).IsNull("DEP_DEN") Then _sDenDepartamento = dtDatos.Rows(0)("DEP_DEN")
                If Not dtDatos.Rows(0).IsNull("UON1") Then _sUON1 = dtDatos.Rows(0)("UON1")
                If Not dtDatos.Rows(0).IsNull("UON2") Then _sUON2 = dtDatos.Rows(0)("UON2")
                If Not dtDatos.Rows(0).IsNull("UON3") Then _sUON3 = dtDatos.Rows(0)("UON3")
                If Not dtDatos.Rows(0).IsNull("CAR") Then _sCargo = dtDatos.Rows(0)("CAR")
            End If
        End Sub

        ''' <summary>Devuelve un string con la denominación de la persona</summary>
        ''' <param name="bMostrarEMail">Mostrar email o no</param>        
        ''' <remarks>Llamada desde: campos.ascx.Page_Load</remarks>
        Public Function Denominacion(ByVal bMostrarEMail As Boolean) As String
            Dim sDenominacion As String = String.Empty

            If Not _sNombre Is Nothing Then sDenominacion = _sNombre & " " & _sApellidos
            If bMostrarEMail And Not _sEMail Is Nothing Then sDenominacion &= " (" & _sEMail & ")"

            Return sDenominacion
        End Function

        ''' <summary>Devuelve un string con las UONs de la persona concatenadas</summary>           
        ''' <remarks>Llamada desde: campos.ascx.Page_Load</remarks>
        Public Function UONs() As String
            Dim sUONs As String

            If _sUON3 Is Nothing Then
                If _sUON2 Is Nothing Then
                    sUONs = _sUON1
                Else
                    sUONs = _sUON1 & "-" & _sUON2
                End If
            Else
                sUONs = _sUON1 & "-" & _sUON2 & "-" & _sUON3
            End If

            Return sUONs
        End Function

#End Region

    End Class
End Namespace
