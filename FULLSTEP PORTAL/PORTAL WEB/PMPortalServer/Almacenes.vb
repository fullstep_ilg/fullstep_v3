Namespace Fullstep.PMPortalServer
    <Serializable()> _
Public Class Almacenes
        Inherits Fullstep.PMPortalServer.Security


        Private moAlmacenes As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moAlmacenes
            End Get

        End Property

        ''' <summary>
        ''' Carga todos los almacenes
        ''' </summary>
        ''' <param name="lCiaComp">Codigo de compania</param>
        ''' <param name="sCodAlmacen">Almacen</param>
        ''' <param name="sCodCentro">Centro</param>
        ''' <remarks>Llamada desde: script\_common\campos.ascx  script\_common\desglose.ascx   DataEntry_2010\GeneralEntry.vb; Tiempo m�ximo: 0,2</remarks>
        Public Sub LoadData(ByVal lCiaComp As Long, Optional ByVal sCodAlmacen As String = Nothing, Optional ByVal sCodCentro As String = Nothing)
            Authenticate()
            If Not IsNothing(sCodCentro) Then
                If mRemottingServer Then
                    moAlmacenes = DBServer.Almacenes_Load(lCiaComp, , sCodCentro, msSesionId, msIPDir, msPersistID)
                Else
                    moAlmacenes = DBServer.Almacenes_Load(lCiaComp, , sCodCentro)
                End If
            Else
                If mRemottingServer Then
                    moAlmacenes = DBServer.Almacenes_Load(lCiaComp, , , msSesionId, msIPDir, msPersistID)
                Else
                    moAlmacenes = DBServer.Almacenes_Load(lCiaComp)
                End If

            End If
        End Sub
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

    End Class
End Namespace