Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Monedas
        Inherits Fullstep.PMPortalServer.Security
        Private moMonedas As DataSet
        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moMonedas
            End Get

        End Property

        Public Sub LoadData(ByVal sIdi As String, ByVal lCiaComp As Long, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal bCoincid As Boolean = False, Optional ByVal bAllDatos As Boolean = False)
            'jbg160309
            'Que hace: 
            '   Devolver en un dataset 2(cod + den ) o todos los campos de la tabla monedas
            'Param. Entrada:
            '   sIdi    idioma
            '   lCiaComp codigo de compania
            '   sCod    codigo de moneda
            '   sDen    denominaci�n de moneda
            '   bCoincid    coincidencia total o por like 'sCod%'
            '   bAlldatos    t2(cod + den ) o todos los campos de la tabla
            'Param. Salida:
            '   Lo dicho en "que hace", un recordset
            'Llamada desde: 
            '   Todo combo de monedas. Carga de monedas para comprobar/traer datos...Vamos N aspx, pmserver, pmdatab...
            'Tiempo:    
            '   Instantaneo
            Authenticate()
            If mRemottingServer Then
                moMonedas = DBServer.Monedas_Get(sIdi, lCiaComp, sCod, sDen, bCoincid, bAllDatos, msSesionId, msIPDir, msPersistID)
            Else
                moMonedas = DBServer.Monedas_Get(sIdi, lCiaComp, sCod, sDen, bCoincid, bAllDatos)
            End If
        End Sub
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

    End Class
End Namespace