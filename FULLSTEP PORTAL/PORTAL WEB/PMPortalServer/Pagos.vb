﻿
Imports Fullstep
Imports Fullstep.PMPortalServer
Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Pagos
        Inherits Lista(Of Fullstep.PMPortalServer.Pago)

        ''' <summary>
        ''' AÃ±ade un nuevo elemento CPago a la lista
        ''' </summary>
        ''' <param name="IIdPago">Integer. ID de Pago</param>
        ''' <param name="IIdFra">Integer. ID de Factura</param>
        ''' <param name="sNumPago">String. NÃºmero de Pago</param>
        ''' <param name="sNumFra">String. NÃºmero de Factura</param>
        ''' <param name="dtFecha">Datetime. Fecha del Pago</param>
        ''' <param name="ILineaFactura">Integer. Linea de factura</param>
        ''' <param name="ILineaPedido">Integer. Id de la lÃ­nea de pedido</param>
        ''' <returns>El elemento CPago aÃ±adido</returns>
        Public Overloads Function Add(ByVal IIdPago As Integer, ByVal IIdFra As Integer, ByVal sNumPago As String, ByVal sNumFra As String, ByVal dtFecha As DateTime, ByVal ILineaFactura As Integer, ByVal ILineaPedido As Integer) As Pago

            'create a new object
            Dim objnewmember As Pago

            objnewmember = New Pago(mDBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)

            With objnewmember
                .IdPago = IIdPago
                .IdFra = IIdFra
                .NumPago = sNumPago
                .NumFra = sNumFra
                .Fecha = dtFecha
                '.LineaFactura = ILineaFactura
                .LineaPedido = ILineaPedido
            End With

            Me.Add(objnewmember)
            Return objnewmember

        End Function
        ''' <summary>
        ''' FunciÃ³n que devuelve un objeto de tipo CPagos con los datos de los pagos vinculados a las facturas vinculadas a su vez a la linea de pedido dada. Si no hay lÃ­nea de pedido, devuelve todas la lÃ­neas -> facturas -> pagos
        ''' </summary>
        ''' <param name="idioma">Idioma en el que se quieren recuperar los datos en multiidioma</param>
        ''' <param name="Linea_Pedido">Integer. Opcional. Id de la lÃ­nea de pedido de la que se quiere recuperar los pagos vinculados a las facturas vinculadas a esa lÃ­nea</param>
        ''' <returns>Un objeto de la clase CPagos con la relaciÃ³n de pagos-facturas-lineas de facturas-lineas de pedido</returns>
        ''' <remarks>
        ''' Llamada desde: </remarks>
        Public Function CargarPagos(ByVal lCiaComp As Long, ByVal idioma As String, Optional ByVal Linea_Pedido As Integer = 0, Optional ByVal lIdFactura As Long = 0) As Pagos
            Dim oCPagos As Pagos
            Dim oCPago As Pago
            Dim oRoot As New Fullstep.PMPortalDatabaseServer.Root

            oCPagos = New Pagos(mDBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)

            Dim DsResul As New DataSet
            DsResul = mDBServer.Linea_DevolverPagos(lCiaComp, idioma, Linea_Pedido, lIdFactura)
            If DsResul.Tables(0).Rows.Count > 0 Then
                For i As Long = 0 To DsResul.Tables(0).Rows.Count - 1
                    oCPago = New Pago(mDBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
                    With DsResul.Tables(0).Rows(i)
                        oCPago.IdPago = .Item("PAGO_ID")
                        oCPago.IdFra = .Item("FAC_ID")
                        oCPago.NumPago = .Item("PAGO_NUM")
                        oCPago.NumFra = .Item("FAC_NUM")
                        oCPago.Fecha = .Item("FECHA")
                        'oCPago.LineaFactura = .Item("LINEA_FACTURA")
                        oCPago.LineaPedido = .Item("LINEA_PEDIDO")

                        'If IsDBNull(.Item("EST_ID")) then
                        'If DBNull.Value.Equals(.Item("EST_ID")) Then
                        If .Item("EST_ID") Is DBNull.Value Then
                            oCPago.EstadoId = 0
                        Else
                            oCPago.EstadoId = .Item("EST_ID")
                        End If
                        If .Item("EST_DEN") Is DBNull.Value Then
                            oCPago.EstadoDen = ""
                        Else
                            oCPago.EstadoDen = .Item("EST_DEN")
                        End If

                    End With
                    oCPagos.Add(oCPago)
                    oCPago = Nothing
                Next
            Else
                Return Nothing
            End If

            Return oCPagos

        End Function

        ''' <summary>
        ''' FunciÃ³n que devuelve un dataset con los estados de Pago existentes en el idioma que usa el usuario.
        ''' </summary>
        ''' <returns>un dataset con los estados de Pago existentes en el idioma que usa el usuario</returns>
        ''' <remarks>
        ''' Llamada desde: EpWeb.Seguimientoaspx.vb.EstadosPago</remarks>
        Public Function CargarEstadosPago(ByVal idioma As FSNLibrary.Idioma) As DataSet

            Dim EPDBServer As New Fullstep.PMPortalDatabaseServer.Root
            'Dim dsEstadosPago As DataSet = EPDBServer.Pagos_DevolverEstadosPago(idioma.ToString)
            'Return dsEstadosPago
        End Function

        ''' <summary>
        ''' Método para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">código de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petición</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo máximo: 0</remarks>
        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace
