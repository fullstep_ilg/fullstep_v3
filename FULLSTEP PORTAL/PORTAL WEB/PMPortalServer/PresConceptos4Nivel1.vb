Namespace Fullstep.PMPortalServer
    <Serializable()> _
        Public Class PresConceptos4Nivel1
        Inherits Security

        Private moData As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moData
            End Get

        End Property


        ''' <summary>
        ''' Carga el presupuesto
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iPres1">presupuesto de nivel 1</param>
        ''' <param name="iPres2">presupuesto de nivel 2</param>
        ''' <param name="iPres3">presupuesto de nivel 3</param>
        ''' <param name="iPres4">presupuesto de nivel 4</param>
        ''' <remarks>Llamada desde: script\_common\campos.ascx    script\_common\desglose.ascx  \script\_common\presAsig.aspx   script\_common\presupuestos.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Sub LoadData(ByVal lCiaComp As Long, ByVal iPres1 As Integer, Optional ByVal iPres2 As Integer = Nothing, Optional ByVal iPres3 As Integer = Nothing, Optional ByVal iPres4 As Integer = Nothing)
            Authenticate()
            If mRemottingServer Then
                moData = DBServer.PresConceptos4_Get(lCiaComp, iPres1, iPres2, iPres3, iPres4, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.PresConceptos4_Get(lCiaComp, iPres1, iPres2, iPres3, iPres4)
            End If

        End Sub


        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace
