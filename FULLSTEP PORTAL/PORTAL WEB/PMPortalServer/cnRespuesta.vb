﻿Imports System.Collections.Generic

Public Class cnRespuesta
    Private _IdRespuesta As Integer
    Public Property IdRespuesta() As Integer
        Get
            Return _IdRespuesta
        End Get
        Set(ByVal value As Integer)
            _IdRespuesta = value
        End Set
    End Property
    Private _EnProceso As String
    Public Property EnProceso() As String
        Get
            Return _EnProceso
        End Get
        Set(ByVal value As String)
            _EnProceso = value
        End Set
    End Property
    Private _IdMensaje As Integer
    Public Property IdMensaje() As Integer
        Get
            Return _IdMensaje
        End Get
        Set(ByVal value As Integer)
            _IdMensaje = value
        End Set
    End Property
    Private _InfoUsuario As cnUsuario
    Public Property InfoUsuario() As cnUsuario

        Get
            Return _InfoUsuario
        End Get
        Set(ByVal value As cnUsuario)
            _InfoUsuario = value
        End Set
    End Property
    Private _Contenido As String
    Public Property Contenido() As String
        Get
            Return _Contenido
        End Get
        Set(ByVal value As String)
            _Contenido = value
        End Set
    End Property
    Private _MeGusta As cnMeGusta
    Public Property MeGusta() As cnMeGusta
        Get
            Return _MeGusta
        End Get
        Set(ByVal value As cnMeGusta)
            _MeGusta = value
        End Set
    End Property
    Private _FechaAlta As DateTime
    Public Property FechaAlta() As DateTime
        Get
            Return _FechaAlta
        End Get
        Set(ByVal value As DateTime)
            _FechaAlta = value
        End Set
    End Property
    Private _FechaAltaRelativa As String
    Public Property FechaAltaRelativa() As String
        Get
            Return _FechaAltaRelativa
        End Get
        Set(ByVal value As String)
            _FechaAltaRelativa = value
        End Set
    End Property
    Private _FechaActualizacion As DateTime
    Public Property FechaActualizacion() As DateTime
        Get
            Return _FechaActualizacion
        End Get
        Set(ByVal value As DateTime)
            _FechaActualizacion = value
        End Set
    End Property
    Private _FechaActualizacionRelativa As String
    Public Property FechaActualizacionRelativa() As String
        Get
            Return _FechaActualizacionRelativa
        End Get
        Set(ByVal value As String)
            _FechaActualizacionRelativa = value
        End Set
    End Property
    Private _UsuarioCitado As cnUsuario
    Public Property UsuarioCitado() As cnUsuario
        Get
            Return _UsuarioCitado
        End Get
        Set(ByVal value As cnUsuario)
            _UsuarioCitado = value
        End Set
    End Property
    Private _IdMensajeCitado As Nullable(Of Integer)
    Public Property IdMensajeCitado() As Nullable(Of Integer)
        Get
            Return _IdMensajeCitado
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _IdMensajeCitado = value
        End Set
    End Property
    Private _Visible As Boolean
    Public Property Visible() As Boolean
        Get
            Return _Visible
        End Get
        Set(ByVal value As Boolean)
            _Visible = value
        End Set
    End Property
    Private _Adjuntos As List(Of cnAdjunto)
    Public Property Adjuntos() As List(Of cnAdjunto)
        Get
            Return _Adjuntos
        End Get
        Set(ByVal value As List(Of cnAdjunto))
            _Adjuntos = value
        End Set
    End Property
    Private _PermisoEditar As Boolean
    Public Property PermisoEditar() As Boolean
        Get
            Return _PermisoEditar
        End Get
        Set(ByVal value As Boolean)
            _PermisoEditar = value
        End Set
    End Property
    Private _MensajeHistorico As Boolean
    Public Property MensajeHistorico() As Boolean
        Get
            Return _MensajeHistorico
        End Get
        Set(ByVal value As Boolean)
            _MensajeHistorico = value
        End Set
    End Property
    Private _Leido As Boolean
    Public Property Leido() As Boolean
        Get
            Return _Leido
        End Get
        Set(ByVal value As Boolean)
            _Leido = value
        End Set
    End Property
    Private _IdDiscrepancia As Integer
    Public Property IdDiscrepancia() As Integer
        Get
            Return _IdDiscrepancia
        End Get
        Set(ByVal value As Integer)
            _IdDiscrepancia = value
        End Set
    End Property
End Class
