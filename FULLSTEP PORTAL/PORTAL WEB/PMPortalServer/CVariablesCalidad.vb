Imports System.Linq
Namespace Fullstep.PMPortalServer
    <Serializable()>
    Public Class CVariablesCalidad
        Inherits Fullstep.PMPortalServer.Security
        Private moVariables As DataTable

        Public ReadOnly Property Data() As Data.DataTable
            Get
                Return moVariables
            End Get

        End Property

        ''' <summary>  
        ''' Ver las puntuaciones de las variables de calidad publicadas en el portal  
        ''' </summary>  
        ''' <param name="lcodCia">C�digo de compania</param>      
        ''' <param name="sCodProve">C�digo de proveedor</param>   
        ''' <param name="sIdioma">idioma para los textos</param>      
        ''' <param name="sPyme">C�digo de pyme</param>      
        ''' <returns>Las puntuaciones de las variables de calidad publicadas en el portal</returns>  
        ''' <remarks>Llamada desde:Web\script\variablescalidad\variablescal.aspx/page_load; Tiempo m�ximo: 2</remarks>  
        Public Sub LoadData(ByVal lCiaComp As Long, ByVal sCodProve As String, ByVal sIdioma As String, Optional ByVal sPyme As String = "")
            Authenticate()
            If mRemottingServer Then
                moVariables = DBServer.Variables_PuntuacionesProveedor(lCiaComp, sCodProve, sIdioma, sPyme, msSesionId, msIPDir, msPersistID)
            Else
                moVariables = DBServer.Variables_PuntuacionesProveedor(lCiaComp, sCodProve, sIdioma, sPyme)
            End If
        End Sub
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

        ''' <summary>
        ''' Monta la tabla a mostrar con las puntuaciones del proveedor
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia compradora</param>
        ''' <param name="sCodProve">C�digo del proveedor</param>
        ''' <param name="sIdioma">Idioma de usuario para mostrar las denominaciones de las variables y las UNQA</param>
        ''' <param name="sPyme">Si es o no pyme</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetPuntuacionesVariablesCalidad(ByVal lCiaComp As Long, ByVal sCodProve As String, ByVal sIdioma As String,
                                                        ByVal EmpresaPortal As Boolean, ByVal NomPortal As String, Optional ByVal sPyme As String = "") As DataTable

            Authenticate()

            Dim dtPuntuacionesProveedor As DataTable
            If mRemottingServer Then
                dtPuntuacionesProveedor = DBServer.Variables_PuntuacionesProveedor(lCiaComp, sCodProve, sIdioma, sPyme, msSesionId, msIPDir, msPersistID)
            Else
                dtPuntuacionesProveedor = DBServer.Variables_PuntuacionesProveedor(lCiaComp, sCodProve, sIdioma, sPyme)
            End If

            Dim dtUnidadesNegocio As DataTable
            If mRemottingServer Then
                dtUnidadesNegocio = DBServer.UnidadesNegocio_PuntuacionesProveedor(lCiaComp, sCodProve, sIdioma, EmpresaPortal, NomPortal, sPyme, msSesionId, msIPDir, msPersistID).tables(0)
            Else
                dtUnidadesNegocio = DBServer.UnidadesNegocio_PuntuacionesProveedor(lCiaComp, sCodProve, sIdioma, EmpresaPortal, NomPortal, sPyme).tables(0)
            End If

            Dim dtPuntuaciones As DataTable
            If mRemottingServer Then
                dtPuntuaciones = DBServer.PuntuacionesProveedor(lCiaComp, sCodProve, sIdioma, sPyme, msSesionId, msIPDir, msPersistID)
            Else
                dtPuntuaciones = DBServer.PuntuacionesProveedor(lCiaComp, sCodProve, sIdioma, sPyme)
            End If

            ConfigurarDatasetPuntuacionesProveedor(dtPuntuacionesProveedor, dtUnidadesNegocio)
            CompletarDatosPuntuacionesProveedor(dtPuntuacionesProveedor, dtPuntuaciones)

            Return CrearTableResultados(dtPuntuacionesProveedor)
        End Function

        ''' <summary>
        ''' Creaci�n de las columnas de las unidades de negocio
        ''' </summary>
        ''' <param name="dtVariablesCalidad">Tabla de las variables de calidad</param>
        ''' <param name="dtUNQAs">Table con las Unidades de negocio</param>
        ''' <param name="IdPadre">Id del padre del nivel a crear</param>
        ''' <remarks></remarks>
        Private Sub ConfigurarDatasetPuntuacionesProveedor(ByRef dtVariablesCalidad As DataTable, ByVal dtUNQAs As DataTable, Optional ByVal IdPadre As Nullable(Of Integer) = Nothing)
            Dim dRows() As DataRow
            If IdPadre Is Nothing Then
                dRows = dtUNQAs.Select("IDPADRE IS NULL", "COD")
            Else
                dRows = dtUNQAs.Select("IDPADRE=" & IdPadre, "COD")
            End If

            If dRows.Length > 0 Then
                For Each oRow As DataRow In dRows
                    Dim dColumna As DataColumn

                    dColumna = New DataColumn("CALI_" & oRow("ID"), System.Type.GetType("System.String"))
                    dtVariablesCalidad.Columns.Add(dColumna)

                    dColumna = New DataColumn("PUNT_" & oRow("ID"), System.Type.GetType("System.Double"))
                    dtVariablesCalidad.Columns.Add(dColumna)

                    dColumna = New DataColumn("FACT_" & oRow("ID"), System.Type.GetType("System.DateTime"))
                    dtVariablesCalidad.Columns.Add(dColumna)

                    ConfigurarDatasetPuntuacionesProveedor(dtVariablesCalidad, dtUNQAs, oRow("ID"))
                Next
            End If

        End Sub

        ''' <summary>
        ''' Completa los datos de las puntuaciones
        ''' </summary>
        ''' <param name="dtResultados">Tabla resultado para cargar la grid</param>
        ''' <param name="dtPuntuaciones">Tabla de puntuaciones obtenidas de base de datos</param>
        ''' <remarks></remarks>
        Private Sub CompletarDatosPuntuacionesProveedor(ByRef dtResultados As DataTable, ByVal dtPuntuaciones As DataTable)
            Dim oRowPuntuaciones() As DataRow
            Dim oVariableCalidad As DataRow

            For i As Integer = dtResultados.Rows.Count - 1 To 0 Step -1
                oVariableCalidad = dtResultados.Rows(i)

                oRowPuntuaciones = dtPuntuaciones.Select("VARCAL=" & oVariableCalidad("ID") & " AND NIVEL=" & oVariableCalidad("NIVEL"))

                If oRowPuntuaciones.Length = 0 Then
                    dtResultados.Rows.RemoveAt(i)
                Else
                    For Each oPuntuacion As DataRow In oRowPuntuaciones
                        oVariableCalidad("CALI_" & oPuntuacion("UNQA")) = oPuntuacion("CALDEN")
                        oVariableCalidad("PUNT_" & oPuntuacion("UNQA")) = oPuntuacion("PUNT")
                        oVariableCalidad("FACT_" & oPuntuacion("UNQA")) = oPuntuacion("FECACT")
                    Next
                End If
            Next
        End Sub

        ''' <summary>
        ''' Crea las columnas necesarias en la tabla para cargar la grid
        ''' </summary>
        ''' <param name="dtPuntuacionesProveedor">Tabla con las puntuaciones en base de datos</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function CrearTableResultados(ByVal dtPuntuacionesProveedor As DataTable) As DataTable
            Dim dtResultados As New DataTable
            Dim oColumn As DataColumn

            oColumn = New DataColumn("HASCHILDS", System.Type.GetType("System.Boolean"))
            dtResultados.Columns.Add(oColumn)

            oColumn = New DataColumn("EXPANDED", System.Type.GetType("System.Boolean"))
            dtResultados.Columns.Add(oColumn)

            oColumn = New DataColumn("VISIBLE", System.Type.GetType("System.Boolean"))
            dtResultados.Columns.Add(oColumn)

            oColumn = New DataColumn("ID", System.Type.GetType("System.Int64"))
            dtResultados.Columns.Add(oColumn)

            oColumn = New DataColumn("DEN", System.Type.GetType("System.String"))
            dtResultados.Columns.Add(oColumn)

            oColumn = New DataColumn("IDPADRE", System.Type.GetType("System.Int64"))
            dtResultados.Columns.Add(oColumn)

            oColumn = New DataColumn("NIVEL", System.Type.GetType("System.Int16"))
            dtResultados.Columns.Add(oColumn)

            For Each odtColumn As DataColumn In dtPuntuacionesProveedor.Columns
                If Split(odtColumn.ColumnName, "_")(0) = "CALI" OrElse
                    Split(odtColumn.ColumnName, "_")(0) = "PUNT" OrElse
                    Split(odtColumn.ColumnName, "_")(0) = "FACT" Then

                    oColumn = New DataColumn(odtColumn.ColumnName, System.Type.GetType(odtColumn.DataType.FullName))
                    dtResultados.Columns.Add(oColumn)
                End If
            Next

            Dim rRows() As DataRow = dtPuntuacionesProveedor.Select("NIVEL=0", "COD")
            Dim VariablesCalidadConfiguracionUsuario() As String = Split("")
            AgregarHijos(rRows, dtResultados, dtPuntuacionesProveedor)

            Return dtResultados
        End Function

        ''' <summary>
        ''' Monta las filas de las variables de calidad en orden a su jerarquia y completa los datos de las puntuaciones
        ''' </summary>
        ''' <param name="rows">Filas del nivel correspondiente</param>
        ''' <param name="tableResultado">Tabla con el resultado del procedimiento para cargar la grid</param>
        ''' <param name="tablePuntuaciones">Tabla de las puntuaciones</param>
        ''' <remarks></remarks>
        Private Sub AgregarHijos(ByVal rows() As DataRow, ByRef tableResultado As DataTable, ByVal tablePuntuaciones As DataTable)
            Dim oRow As DataRow
            Dim rHijos() As DataRow

            For Each rRow As DataRow In rows
                oRow = tableResultado.NewRow

                oRow("ID") = rRow("ID")
                oRow("DEN") = rRow("DEN")
                oRow("IDPADRE") = rRow("IDPADRE")
                oRow("NIVEL") = rRow("NIVEL")
                oRow("EXPANDED") = True
                oRow("VISIBLE") = True

                For Each odtColumn As DataColumn In tablePuntuaciones.Columns
                    If Split(odtColumn.ColumnName, "_")(0) = "CALI" OrElse
                        Split(odtColumn.ColumnName, "_")(0) = "PUNT" OrElse
                        Split(odtColumn.ColumnName, "_")(0) = "FACT" Then

                        oRow(odtColumn.ColumnName) = rRow(odtColumn.ColumnName)
                    End If
                Next

                rHijos = tablePuntuaciones.Select("IDPADRE=" & oRow("ID") & " AND NIVEL=" & oRow("NIVEL") + 1, "COD")

                If Not rHijos.Length = 0 Then
                    oRow("HASCHILDS") = True
                Else
                    oRow("HASCHILDS") = False
                End If

                tableResultado.Rows.Add(oRow)
                AgregarHijos(rHijos, tableResultado, tablePuntuaciones)
            Next
        End Sub
    End Class
End Namespace