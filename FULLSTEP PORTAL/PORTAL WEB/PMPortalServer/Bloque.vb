Namespace Fullstep.PMPortalServer
    <Serializable()> _
Public Class Bloque
        Inherits Security

        Private mlId As Integer

        Property Id() As Integer
            Get
                Return mlId
            End Get
            Set(ByVal Value As Integer)
                mlId = Value
            End Set
        End Property

        ''' <summary>
        ''' Devolver Etapas Bloqueo Salida
        ''' </summary>
        ''' <param name="lCiaComp">Codigo de compania</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: script\contratos\GestionContrato.aspx   script\solicitudes\detalleSolicitud.aspx; Tiempo m�ximo: 0,2</remarks>
        Public Function DevolverEtapasBloqueoSalida(ByVal lCiaComp As Long, ByVal sIdi As String) As DataSet
            Authenticate()

            Dim data As DataSet
            If mRemottingServer Then
                data = DBServer.Bloque_LoadEtapasBloqueoSalida(lCiaComp, mlId, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Bloque_LoadEtapasBloqueoSalida(lCiaComp, mlId, sIdi)
            End If

            Return data
        End Function
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)

        End Sub
    End Class
End Namespace