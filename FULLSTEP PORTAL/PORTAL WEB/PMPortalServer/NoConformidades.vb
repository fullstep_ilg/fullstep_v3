Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class NoConformidades
        Inherits Fullstep.PMPortalServer.Security

        Private moNoConformidades As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moNoConformidades
            End Get

        End Property

        ''' <summary>
        ''' Lista de solicitudes q tiene un usuario.
        ''' </summary>
        ''' <param name="iCia">Id de la compa�ia</param> 
        ''' <param name="sUsu">usuario</param>
        ''' <param name="sIdi">idioma en q ver los textos</param>
        ''' <param name="bEsCombo">sql para cargar combo o no</param>
        ''' <param name="iTipoSolicitud">tipo de solicitud</param>
        ''' <param name="bSeguimiento">la carga del combo de seguimiento mira en vista de peticionario por lo de definir un rol como lista/departamento/etc. El resto mira instancia.peticionario</param>
        ''' <param name="bNuevoWorkflow">true PM/false QA</param>
        ''' <remarks>Llamada desde:script\noconformidad\NoConformidadVisorCerradas.aspx; Tiempo m�ximo:0,1</remarks>
        Public Sub Load_Tipo_NoConformidades(ByVal iCia As Integer, ByVal sUsu As String, Optional ByVal sIdi As String = "SPA", Optional ByVal bEsCombo As Integer = 0, Optional ByVal iTipoSolicitud As Integer = Nothing, Optional ByVal bSeguimiento As Integer = 0, Optional ByVal bNuevoWorkfl As Boolean = False)
            Authenticate()
            If mRemottingServer Then
                moNoConformidades = DBServer.Tipo_NoConformidades(iCia, sUsu, sIdi, bEsCombo, iTipoSolicitud, bSeguimiento, bNuevoWorkfl, msSesionId, msIPDir, msPersistID)
            Else
                moNoConformidades = DBServer.Tipo_NoConformidades(iCia, sUsu, sIdi, bEsCombo, iTipoSolicitud, bSeguimiento, bNuevoWorkfl)
            End If
        End Sub
        ''' Revisado por: Jbg. Fecha: 1/12/2011
        ''' <summary>
        ''' Cargar las no conformidades cerradas de un proveedor seg�n los parametros dados. 
        ''' </summary>
        ''' <param name="iCia">Id de la compa�ia</param> 
        ''' <param name="sProve">Proveedor</param>        
        ''' <param name="lId">Id de la No conformidad</param>  
        ''' <param name="lTipo">Tipo de No conformidad</param>  
        ''' <param name="sFecha">Fecha apertura desde de No conformidad</param>  
        ''' <param name="sIdi">Idioma</param>
        ''' <remarks>Llamada desde: NoConformidadVisorCerradas.aspx/btnBuscar_Click; Tiempo m�ximo:0,1</remarks>
        Public Sub Load_Data_NoConformidades_Cerradas(ByVal iCia As Integer, ByVal EmpresaPortal As Boolean, ByVal NomPortal As String, Optional ByVal sProve As String = Nothing,
                                                      Optional ByVal lId As Long = 0, Optional ByVal lTipo As Long = 0,
                                                      Optional ByVal sFecha As String = Nothing, Optional ByVal sIdi As String = Nothing)
            Authenticate()
            If mRemottingServer Then
                moNoConformidades = DBServer.Data_NoConformidades_Cerradas(iCia, EmpresaPortal, NomPortal, sProve, lId, lTipo, sFecha, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                moNoConformidades = DBServer.Data_NoConformidades_Cerradas(iCia, EmpresaPortal, NomPortal, sProve, lId, lTipo, sFecha, sIdi)
            End If
        End Sub
        ''' Revisado por: Jbg. Fecha: 1/12/2011
        ''' <summary>
        ''' Cargar las no conformidades cerradas de un proveedor seg�n los parametros dados. 
        ''' </summary>
        ''' <param name="iCia">Id de la compa�ia</param> 
        ''' <param name="sProve">Proveedor</param>        
        ''' <param name="lId">Id de la No conformidad</param>  
        ''' <param name="lTipo">Tipo de No conformidad</param>  
        ''' <param name="sFecha">Fecha apertura desde de No conformidad</param>  
        ''' <param name="sIdi">Idioma</param>
        ''' <remarks>Llamada desde:NoConformidadVisorCerradas.aspx/odsGrid_ObjectCreating; Tiempo m�ximo:0,1</remarks>
        Public Function Devolver_NoConformidades_Cerradas(ByVal iCia As Integer, ByVal EmpresaPortal As Boolean, ByVal NomPortal As String, Optional ByVal sProve As String = Nothing, Optional ByVal lId As Long = 0, Optional ByVal lTipo As Long = 0,
                                                          Optional ByVal sFecha As String = Nothing, Optional ByVal sIdi As String = Nothing) As DataSet
            Load_Data_NoConformidades_Cerradas(iCia, EmpresaPortal, NomPortal, sProve, lId, lTipo, sFecha, sIdi)

            Return moNoConformidades
        End Function
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace