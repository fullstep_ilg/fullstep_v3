Namespace Fullstep.PMPortalServer
    <Serializable()> _
Public Class Centros
        Inherits Fullstep.PMPortalServer.Security


        Private moCentros As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moCentros
            End Get

        End Property

        ''' <summary>
        ''' Carga todos los centros 
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCodCentro">Centros</param>
        ''' <param name="sCodOrgCompras">Organizacion de compras</param>
        ''' <remarks>Llamada desde: _common\articulosserver.aspx.vb _common\campos.ascx.vb  _common\CentroUnicoServer.aspx.vb   _common\desglose.ascx.vb    DataEntry_2010\GeneralEntry.vb; Tiempo m�ximo: 0,2</remarks>
        Public Sub LoadData(ByVal lCiaComp As Long, Optional ByVal sCodCentro As String = Nothing, Optional ByVal sCodOrgCompras As String = Nothing)
            Authenticate()
            If mRemottingServer Then
                If Not IsNothing(sCodOrgCompras) Then
                    moCentros = DBServer.Centros_Load(lCiaComp, , sCodOrgCompras, msSesionId, msIPDir, msPersistID)
                Else
                    moCentros = DBServer.Centros_Load(lCiaComp, , , msSesionId, msIPDir, msPersistID)
                End If
            Else
                If Not IsNothing(sCodOrgCompras) Then
                    moCentros = DBServer.Centros_Load(lCiaComp, , sCodOrgCompras)
                Else
                    moCentros = DBServer.Centros_Load(lCiaComp)
                End If
            End If
        End Sub
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

    End Class
End Namespace