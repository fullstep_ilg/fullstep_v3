
Namespace Fullstep.PMPortalServer
    <Serializable()> _
        Public Class Certificado
        Inherits Fullstep.PMPortalServer.Security

        Private mlId As Long
        Private msProve As String
        Private mlCon As Long
        Private mbPub As Boolean
        Private mbBaja As Boolean
        Private mlNumVersion As Long
        Private mdtFecPub As Date
        Private mdtFecLimCumplim As Date
        Private mbPlazoCumplimentacionSuperado As Boolean
        Private mlInstancia As Long
        Private msUsu As String
        Private msPer As String
        Private msDep As String
        Private msUon1 As String
        Private msUon2 As String
        Private msUon3 As String
        Private msNomSolicitante As String

        Private mdtFecRespuesta As Date
        Private mdtFecDespub As Date
        Private mbPortal As Boolean

        Private mdsData As DataSet

        Private msCodPeticionario As String
        Private msNombrePeticionario As String

        Private mlIDSolicitud As Long
        Private msDenSolictud As String

        Private mbEnvioConjunto As Boolean
        Private mbFueraPlazo As Boolean

#Region "Properties"
        Property Portal() As Boolean
            Get
                Return mbPortal
            End Get
            Set(ByVal Value As Boolean)
                mbPortal = Value
            End Set
        End Property
        ReadOnly Property Data() As DataSet
            Get
                Return mdsData

            End Get
        End Property
        Property Id() As Long
            Get
                Return mlId
            End Get
            Set(ByVal Value As Long)
                mlId = Value
            End Set
        End Property
        Property Prove() As String
            Get
                Return msProve
            End Get
            Set(ByVal Value As String)
                msProve = Value
            End Set
        End Property
        Property Con() As Long
            Get
                Return mlCon
            End Get
            Set(ByVal Value As Long)
                mlCon = Value
            End Set
        End Property
        Property Pub() As Boolean
            Get
                Return mbPub
            End Get
            Set(ByVal Value As Boolean)
                mbBaja = Value
            End Set
        End Property
        Property Baja() As Boolean
            Get
                Return mbBaja
            End Get
            Set(ByVal Value As Boolean)
                mbBaja = Value
            End Set
        End Property
        Property Version() As Long
            Get
                Return mlNumVersion
            End Get
            Set(ByVal Value As Long)
                mlNumVersion = Value
            End Set
        End Property
        Property FecPub() As Date
            Get
                Return mdtFecPub
            End Get
            Set(ByVal Value As Date)
                mdtFecPub = Value
            End Set
        End Property
        Property FecLimCumplim() As Date
            Get
                Return mdtFecLimCumplim
            End Get
            Set(ByVal Value As Date)
                mdtFecLimCumplim = Value
            End Set
        End Property
        Property PlazoLimCumplimSuperado() As Boolean
            Get
                Return mbPlazoCumplimentacionSuperado
            End Get
            Set(ByVal Value As Boolean)
                mbPlazoCumplimentacionSuperado = Value
            End Set
        End Property
        Property Instancia() As Long
            Get
                Return mlInstancia
            End Get
            Set(ByVal Value As Long)
                mlInstancia = Value
            End Set
        End Property
        Property Usu() As String
            Get
                Return msUsu
            End Get
            Set(ByVal Value As String)
                msUsu = Value
            End Set
        End Property
        Property Per() As String
            Get
                Return msPer
            End Get
            Set(ByVal Value As String)
                msPer = Value
            End Set
        End Property
        Property Dep() As String
            Get
                Return msDep
            End Get
            Set(ByVal Value As String)
                msDep = Value
            End Set
        End Property
        Property Uon1() As String
            Get
                Return msUon1
            End Get
            Set(ByVal Value As String)
                msUon1 = Value
            End Set
        End Property
        Property Uon2() As String
            Get
                Return msUon2
            End Get
            Set(ByVal Value As String)
                msUon2 = Value
            End Set
        End Property
        Property Uon3() As String
            Get
                Return msUon3
            End Get
            Set(ByVal Value As String)
                msUon3 = Value
            End Set
        End Property
        Property NomSolicitante() As String
            Get
                Return msNomSolicitante
            End Get
            Set(ByVal Value As String)
                msNomSolicitante = Value
            End Set
        End Property
        Property FecRespuesta() As Date
            Get
                Return mdtFecRespuesta
            End Get
            Set(ByVal Value As Date)
                mdtFecRespuesta = Value
            End Set
        End Property
        Property FecDespub() As Date
            Get
                Return mdtFecDespub
            End Get
            Set(ByVal Value As Date)
                mdtFecDespub = Value
            End Set
        End Property
        Property EnvioConjunto() As Boolean
            Get
                Return mbEnvioConjunto
            End Get
            Set(ByVal Value As Boolean)
                mbEnvioConjunto = Value
            End Set
        End Property
        Property FueraPlazo() As Boolean
            Get
                Return mbFueraPlazo
            End Get
            Set(ByVal Value As Boolean)
                mbFueraPlazo = Value
            End Set
        End Property

        ''' <summary>
        ''' Obtener/Establecer el Peticionario de la instancia.
        ''' El stored de carga devuelve columna CERTIFICADO.PER y otra columna INSTANCIA.PER AS IPETICIONARIO. Tratamos IPETICIONARIO.
        ''' </summary>
        ''' <returns>Peticionario de la instancia</returns>
        ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase Certificado; Tiempo maximo:0</remarks>
        Public Property PeticionarioCod() As String
            Get
                Return msCodPeticionario
            End Get

            Set(ByVal Value As String)
                msCodPeticionario = Value
            End Set
        End Property
        ''' <summary>
        ''' Obtener/Establecer el Nombre del Peticionario de la instancia.
        ''' El stored de carga devuelve columna P.NOM+P.APE AS NOMBRE y otra columna PPET.NOM+PPET.APE AS NOMBREPETICIONARIO. Donde 
        ''' se hace JOIN PER P ON CERTIFICADO.PER ... y JOIN PER PPET ON INSTANCIA.PER ...
        ''' Tratamos NOMBREPETICIONARIO.
        ''' </summary>
        ''' <returns>Nombre del Peticionario de la instancia</returns>
        ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase Certificado; Tiempo maximo:0</remarks>
        Public Property Peticionario() As String
            Get
                Return msNombrePeticionario
            End Get

            Set(ByVal Value As String)
                msNombrePeticionario = Value
            End Set
        End Property
        ''' <summary>
        ''' Obtener/Establecer la denominaci�n de la Solicitud, en la q este basada el certificado, lo devuelve en el idioma 
        ''' del usuario conectado. El stored de carga devuelve columna SOLICTUD.DEN + @IDI AS DEN
        ''' </summary>
        ''' <returns>Denominaci�n de la Solicitud</returns>
        ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase Certificado; Tiempo maximo:0</remarks>
        Public Property TipoDen() As String
            Get
                Return msDenSolictud
            End Get

            Set(ByVal Value As String)
                msDenSolictud = Value
            End Set
        End Property
        ''' <summary>
        ''' Obtener/Establecer el ID de la Solicitud, en la q este basada el certificado.
        ''' El stored de carga devuelve columna SOLICTUD.ID AS IDSOL
        ''' </summary>
        ''' <returns>ID de la Solicitud</returns>
        ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase Certificado; Tiempo maximo:0</remarks>
        Public Property IDSolicitud() As Long
            Get
                Return mlIDSolicitud
            End Get

            Set(ByVal Value As Long)
                mlIDSolicitud = Value
            End Set
        End Property
        Private _FecCumplimentacion As Date
        Public Property FecCumplimentacion() As Date
            Get
                Return _FecCumplimentacion
            End Get
            Set(ByVal value As Date)
                _FecCumplimentacion = value
            End Set
        End Property
        ''' <summary>
        ''' 0->Vigente, -1->Expirado, 1->Prox. expirar
        ''' </summary>
        ''' <remarks></remarks>
        Private _Expirado As Integer
        Public Property Expirado() As Integer
            Get
                Return _Expirado
            End Get
            Set(ByVal value As Integer)
                _Expirado = value
            End Set
        End Property
#End Region
        ''' <summary>
        ''' Cargar las respuestas (la informacion de cuando se realizo la version del certificado)
        ''' </summary>
        ''' <param name="lCiaComp">codigo de compania</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <returns>Respuestas</returns>
        ''' <remarks>Llamada desde: certificados\certificado.aspx.vb/RellenarComboRespuestas; Tiempo maximo:0,2</remarks>
        Public Function CargarRespuestas(ByVal lCiaComp As Long, Optional ByVal sIdi As String = "SPA") As DataSet
            Authenticate()
            Dim data As DataSet
            Dim oRow As DataRow
            Dim oIdiomas As New Idiomas(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
            Dim oIdi As Idioma


            If mRemottingServer Then
                data = DBServer.Certificado_CargarRespuestas(lCiaComp, msProve, mlId, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Certificado_CargarRespuestas(lCiaComp, msProve, mlId, sIdi)
            End If
            Return data

        End Function
        ''' <summary>
        ''' Procedimiento que carga un certificado
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa��a</param>
        ''' <param name="sIdi">Idioma en el q sacar los textos</param>
        ''' <remarks>
        ''' Llamada desde: _common\guardarinstancia.aspx    _common\modulos\modInstancia.vb     certificados\certificado.aspx
        '''     certificados\enviocertificadoKO.aspx     certificados\envioCertificadoOk.aspx     noconformidad\impexp.aspx
        ''' Tiempo m�ximo: 0,5 seg</remarks>
        Public Sub Load(ByVal lCiaComp As Long, ByVal sIdi As String)
            Authenticate()

            Dim data As DataSet
            Dim oRow As DataRow
            If mRemottingServer Then
                data = DBServer.Certificado_Load(lCiaComp, sIdi, mlId, mbPortal, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Certificado_Load(lCiaComp, sIdi, mlId, mbPortal)
            End If

            oRow = data.Tables(0).Rows(0)
            mlId = oRow.Item("ID")

            If IsDBNull(oRow.Item("CONTACTO")) Then
                mlCon = 0
            Else
                mlCon = oRow.Item("CONTACTO")
            End If
            mbPub = SQLBinaryToBoolean(oRow.Item("PUBLICADA"))
            mbBaja = SQLBinaryToBoolean(oRow.Item("BAJA"))
            mlNumVersion = IIf(IsDBNull(oRow.Item("NUM_VERSION")), 0, oRow.Item("NUM_VERSION"))
            mdtFecPub = DBNullToSomething(oRow.Item("FEC_PUBLICACION"))
            mdtFecLimCumplim = DBNullToSomething(oRow.Item("FEC_LIM_CUMPLIM"))
            mbPlazoCumplimentacionSuperado = IIf(oRow.Item("PLAZOCUMPLIMENTACIONSUPERADO") = 1, True, False)
            mlInstancia = oRow.Item("INSTANCIA")
            msUsu = DBNullToSomething(oRow.Item("USU"))
            msPer = DBNullToSomething(oRow.Item("PER"))
            msDep = DBNullToSomething(oRow.Item("DEP"))
            msUon1 = DBNullToSomething(oRow.Item("UON1"))
            msUon2 = DBNullToSomething(oRow.Item("UON2"))
            msUon3 = DBNullToSomething(oRow.Item("UON3"))
            mdtFecRespuesta = DBNullToSomething(oRow.Item("FEC_RESPUESTA"))
            mdtFecDespub = DBNullToSomething(oRow.Item("FEC_DESPUB"))
            msNomSolicitante = DBNullToSomething(oRow.Item("NOMBRE"))
            mbEnvioConjunto = IIf(oRow.Item("ENVIO_CONJUNTO") = 1, True, False)

            mbFueraPlazo = IIf(oRow.Item("FUERA_PLAZO") = 1, True, False)
            msCodPeticionario = DBNullToSomething(oRow.Item("IPETICIONARIO"))
            msNombrePeticionario = DBNullToSomething(oRow.Item("NOMBREPETICIONARIO"))

            mlIDSolicitud = DBNullToSomething(oRow.Item("IDSOL"))
            msDenSolictud = DBNullToSomething(oRow.Item("DEN"))

            _FecCumplimentacion = DBNullToSomething(oRow.Item("FEC_CUMPLIM"))
            _Expirado = DBNullToInteger(oRow.Item("EXPIRADO"))
            mdsData = data
        End Sub
        ''' <summary>
        ''' cargar los id de campo en version de pantalla y en bbdd (para los guardados sin enviar sin salir y volver a guardar)
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa��a</param>
        ''' <param name="CargaReverse">Booleano, si se cargan ambas correspondencias.</param>
        ''' <param name="oDSParesReverse">Correspondencia con key IDCAMPOACTUAL (Pantalla)</param>
        ''' <returns>Correspondencia con key IDCAMPOVERSION (bd)</returns>
        ''' <remarks>Llamada desde: _common\guardarinstancia.aspx.vb   _common\modulos\modInstancia.vb; Tiempo maximo:0,2</remarks>
        Public Function CargarCorrespondenciaCampos(ByVal lCiaComp As Long, Optional ByVal CargaReverse As Boolean = False, Optional ByRef oDSParesReverse As DataSet = Nothing) As DataSet
            Authenticate()

            Dim data As DataSet
            If mRemottingServer Then
                data = DBServer.Certificado_CargarCorrespondenciaCampos(lCiaComp, mlId, mlNumVersion, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Certificado_CargarCorrespondenciaCampos(lCiaComp, mlId, mlNumVersion)
            End If

            If CargaReverse Then
                oDSParesReverse = data.Copy

                Dim keyReverse(0) As DataColumn
                keyReverse(0) = oDSParesReverse.Tables(0).Columns("IDCAMPOACTUAL")
                oDSParesReverse.Tables(0).PrimaryKey = keyReverse
            End If

            Dim key(0) As DataColumn
            key(0) = data.Tables(0).Columns("IDCAMPOVERSION")
            data.Tables(0).PrimaryKey = key

            Return data
        End Function

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

        ''' <summary>
        ''' Determina si el lIdCampo q se le pasa es instancia lIdInstancia o de solicitud
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa��a</param>
        ''' <param name="lIdCampo">Id de copia_campo o Id de campo</param>
        ''' <param name="lIdInstancia">Instancia a la q ver si pertenece</param>
        ''' <returns>Si lIdCampo es id de instancia o no</returns>
        ''' <remarks>Llamada desde: _common\guardarinstancia.aspx.vb ; Tiempo maximo:0,2</remarks>
        Public Function ControlPrimerGuardadoCert(ByVal lCiaComp As Long, ByVal lIdCampo As Long, ByVal lIdInstancia As Long) As Boolean

            Authenticate()
            Dim data As Boolean

            If mRemottingServer Then
                data = DBServer.Certificado_ControlPrimerGuardadoCert(lCiaComp, lIdCampo, lIdInstancia, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Certificado_ControlPrimerGuardadoCert(lCiaComp, lIdCampo, lIdInstancia)
            End If

            Return data

        End Function

        Public Function HayCertificadosPendientesCumplimentar(ByVal lCiaComp As Long) As Boolean

            Authenticate()
            Dim data As Boolean

            If mRemottingServer Then
                data = DBServer.Certificado_HayCertificadosPendientesCumplimentar(0, lCiaComp, msProve, mlId, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Certificado_HayCertificadosPendientesCumplimentar(0, lCiaComp, msProve, mlId)
            End If

            Return data

        End Function

        Public Function HayCertificadosPendientesEnviar(ByVal lCiaComp As Long) As Boolean

            Authenticate()
            Dim data As Boolean

            If mRemottingServer Then
                data = DBServer.Certificado_HayCertificadosPendientesCumplimentar(1, lCiaComp, msProve, mlId, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Certificado_HayCertificadosPendientesCumplimentar(1, lCiaComp, msProve, mlId)
            End If

            Return data

        End Function

    End Class

End Namespace