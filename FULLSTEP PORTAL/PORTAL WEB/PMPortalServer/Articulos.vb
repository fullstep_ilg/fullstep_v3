Namespace Fullstep.PMPortalServer
    <Serializable()> _
        Public Class Articulos
        Inherits Security
        Private msGMN1 As String
        Private msGMN2 As String
        Private msGMN3 As String
        Private msGMN4 As String
        Private moArticulos As DataSet
        Property GMN1() As String
            Get
                GMN1 = msGMN1
            End Get
            Set(ByVal Value As String)
                msGMN1 = Value
            End Set
        End Property
        Property GMN2() As String
            Get
                GMN2 = msGMN2
            End Get
            Set(ByVal Value As String)
                msGMN2 = Value
            End Set
        End Property
        Property GMN3() As String
            Get
                GMN3 = msGMN3
            End Get
            Set(ByVal Value As String)
                msGMN3 = Value
            End Set
        End Property
        Property GMN4() As String
            Get
                GMN4 = msGMN4
            End Get
            Set(ByVal Value As String)
                msGMN4 = Value
            End Set
        End Property
        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moArticulos
            End Get

        End Property
		''' <summary>
		''' Pendiente determinar si esta funci�n puede reemplazarse en todos los sitios donde se sigue usando por la sobrecarga inmediatamente inferior.
		''' En este caso no se tiene en cuenta el c�digo del proveedor a la hora de devolver los art�culos (en la inferior s�).
		''' Hay que revisar todos los sitios donde se usa para garantizar que no haya problemas.
		''' Hasta entonces, se mantienen ambas versiones de la funci�n.
		''' </summary>
		''' <param name="lCiaComp">C�digo de la compa��a</param>
		''' <param name="sCod">C�digo del art�culo</param>
		''' <param name="sDen">denominaci�n del art�culo</param>
		''' <param name="bCoincid">si est� a 1 coincidencia total, sino hace un LIKE</param>
		''' <param name="sPer">c�digo de la persona (</param>
		''' <param name="iDesdeFila">Para paginaci�n: n� fila a partir de la cual muestra los datos</param>
		''' <param name="iNumFilas">N� m�ximo de registros a mostrar</param>
		''' <param name="sIdioma">C�digo del idioma</param>
		''' <param name="sMoneda">C�digo de la moneda de la instancia</param>
		''' <param name="bCargarUltAdj">Indicador de si carga o no el �ltimo precio adjudicado para el art�culo</param>
		''' <param name="sCodOrgCompras">cod. de la organizaci�n de compras</param>
		''' <param name="sCodCentro">cod. de centro</param>
		''' <param name="sCodPri">para paginaci�n:  c�digo del 1� art�culo a mostrar</param>
		''' <param name="sCodUlt">para paginaci�n:  c�digo del �ltimo art�culo mostrado</param>
		''' <param name="bCargarOrg">si est� a 1 carga la organizacion de compras</param>
		''' <remarks>Llamada desde articulosServer.aspx.vb. M�x. 1 seg.</remarks>
		Public Sub LoadData(ByVal lCiaComp As Long, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal bCoincid As Boolean = False,
							Optional ByVal sPer As String = Nothing, Optional ByVal iDesdeFila As Long = Nothing, Optional ByVal iNumFilas As Long = Nothing,
							Optional ByVal sIdioma As String = Nothing, Optional ByVal sMoneda As String = "EUR", Optional ByVal bCargarUltAdj As Boolean = False,
							Optional ByVal sCodOrgCompras As String = Nothing, Optional ByVal sCodCentro As String = Nothing, Optional ByVal sCodPri As String = Nothing,
							Optional ByVal sCodUlt As String = Nothing, Optional ByVal bCargarOrg As Boolean = False, Optional ByVal CodProve As String = Nothing)
			Authenticate()
			If mRemottingServer Then
				moArticulos = DBServer.Articulos_Get(lCiaComp, sCod, sDen, bCoincid, sPer, iDesdeFila, iNumFilas, sIdioma, msGMN1, msGMN2, msGMN3, msGMN4, sMoneda, bCargarUltAdj,
													 sCodOrgCompras, sCodCentro, sCodPri, sCodUlt, bCargarOrg, CodProve, msSesionId, msIPDir, msPersistID)
			Else
				moArticulos = DBServer.Articulos_Get(lCiaComp, sCod, sDen, bCoincid, sPer, iDesdeFila, iNumFilas, sIdioma, msGMN1, msGMN2, msGMN3, msGMN4, sMoneda, bCargarUltAdj,
													 sCodOrgCompras, sCodCentro, sCodPri, sCodUlt, bCargarOrg, CodProve)
			End If
		End Sub
		Public Function Articulo_Get_Properties(ByVal lCiaComp As Long, ByVal Cod As String) As PropiedadesArticulo
            Authenticate()

            Dim dt As DataTable
            If mRemottingServer Then
                dt = DBServer.Articulo_Get_Properties(lCiaComp, Cod, msSesionId, msIPDir, msPersistID)
            Else
                dt = DBServer.Articulo_Get_Properties(lCiaComp, Cod)
            End If

            If dt.Rows.Count > 0 Then
                Return New PropiedadesArticulo With {.ExisteArticulo = True, _
                                                    .Codigo = Cod, _
                                                    .Denominacion = dt.Rows(0)("DEN"), _
                                                    .Concepto = dt.Rows(0)("CONCEPTO"), _
                                                    .Almacenamiento = dt.Rows(0)("ALMACENAR"), _
                                                    .Recepcion = dt.Rows(0)("RECEPCIONAR"), _
                                                    .TipoRecepcion = dt.Rows(0)("TIPORECEPCION"), _
                                                    .Generico = dt.Rows(0)("GENERICO")}
            Else
                Return New PropiedadesArticulo With {.ExisteArticulo = False, _
                                                    .Codigo = Cod, _
                                                    .Denominacion = String.Empty, _
                                                    .Concepto = 0, _
                                                    .Almacenamiento = 0, _
                                                    .Recepcion = 0, _
                                                    .TipoRecepcion = 0, _
                                                    .Generico = 0}
            End If
        End Function
        ''' Revisado por: blp. Fecha:24/01/2012
        ''' <summary>
        ''' Cargamos en moArticulos (propiedad Data) del objeto Articulos los datos de art�culos teniendo en cuenta el proveedor
        ''' Esta funci�n es una sobregarga del loadData original (que no tiene en cuenta el proveedor al devolver los art�culos).
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa��a</param>
        ''' <param name="lProveID">Id del proveedor</param>
        ''' <param name="sCod">C�digo del art�culo</param>
        ''' <param name="sDen">denominaci�n del art�culo</param>
        ''' <param name="bCoincid">si est� a 1 coincidencia total, sino hace un LIKE</param>
        ''' <param name="sPer">c�digo de la persona (</param>
        ''' <param name="iDesdeFila">Para paginaci�n: n� fila a partir de la cual muestra los datos</param>
        ''' <param name="iNumFilas">N� m�ximo de registros a mostrar</param>
        ''' <param name="sIdioma">C�digo del idioma</param>
        ''' <param name="sMoneda">C�digo de la moneda de la instancia</param>
        ''' <param name="bCargarUltAdj">Indicador de si carga o no el �ltimo precio adjudicado para el art�culo</param>
        ''' <param name="sCodOrgCompras">cod. de la organizaci�n de compras</param>
        ''' <param name="sCodCentro">cod. de centro</param>
        ''' <param name="sCodPri">para paginaci�n:  c�digo del 1� art�culo a mostrar</param>
        ''' <param name="sCodUlt">para paginaci�n:  c�digo del �ltimo art�culo mostrado</param>
        ''' <param name="bCargarOrg">si est� a 1 carga la organizacion de compras</param>
        ''' <remarks>Llamada desde articulosServer.aspx.vb. M�x. 1 seg.</remarks>
        Public Sub LoadData(ByVal lCiaComp As Long, ByVal lProveID As Long, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal bCoincid As Boolean = False, Optional ByVal sPer As String = Nothing, Optional ByVal iDesdeFila As Long = Nothing, Optional ByVal iNumFilas As Long = Nothing, Optional ByVal sIdioma As String = Nothing, Optional ByVal sMoneda As String = "EUR", Optional ByVal bCargarUltAdj As Boolean = False, Optional ByVal sCodOrgCompras As String = Nothing, Optional ByVal sCodCentro As String = Nothing, Optional ByVal sCodPri As String = Nothing, Optional ByVal sCodUlt As String = Nothing, Optional ByVal bCargarOrg As Boolean = False)
            Authenticate()
            If mRemottingServer Then
                moArticulos = DBServer.Articulos_Prove_Get(lCiaComp:=lCiaComp, lProveID:=lProveID, sCod:=sCod, sDen:=sDen, bCoincid:=bCoincid, sPer:=sPer, iDesdeFila:=iDesdeFila, iNumFilas:=iNumFilas, sIdioma:=sIdioma, sGmn1:=msGMN1, sGmn2:=msGMN2, sGmn3:=msGMN3, sGmn4:=msGMN4, sMoneda:=sMoneda, bCargarUltAdj:=bCargarUltAdj, sCodOrgCompras:=sCodOrgCompras, sCodCentro:=sCodCentro, sCodPri:=sCodPri, sCodUlt:=sCodUlt, bCargarOrg:=bCargarOrg, SesionId:=msSesionId, IPDir:=msIPDir, PersistID:=msPersistID)
            Else
                moArticulos = DBServer.Articulos_Prove_Get(lCiaComp:=lCiaComp, lProveID:=lProveID, sCod:=sCod, sDen:=sDen, bCoincid:=bCoincid, sPer:=sPer, iDesdeFila:=iDesdeFila, iNumFilas:=iNumFilas, sIdioma:=sIdioma, sGmn1:=msGMN1, sGmn2:=msGMN2, sGmn3:=msGMN3, sGmn4:=msGMN4, sMoneda:=sMoneda, bCargarUltAdj:=bCargarUltAdj, sCodOrgCompras:=sCodOrgCompras, sCodCentro:=sCodCentro, sCodPri:=sCodPri, sCodUlt:=sCodUlt, bCargarOrg:=bCargarOrg)
            End If
        End Sub
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
    Public Class PropiedadesArticulo
        Private _existeArticulo As Boolean
        Public Property ExisteArticulo() As Boolean
            Get
                Return _existeArticulo
            End Get
            Set(ByVal value As Boolean)
                _existeArticulo = value
            End Set
        End Property
        Private _codigo As String
        Public Property Codigo() As String
            Get
                Return _codigo
            End Get
            Set(ByVal value As String)
                _codigo = value
            End Set
        End Property
        Private _denominacion As String
        Public Property Denominacion() As String
            Get
                Return _denominacion
            End Get
            Set(ByVal value As String)
                _denominacion = value
            End Set
        End Property
        Private _concepto As Integer
        Public Property Concepto() As Integer
            Get
                Return _concepto
            End Get
            Set(ByVal value As Integer)
                _concepto = value
            End Set
        End Property
        Private _almacenamiento As Integer
        Public Property Almacenamiento() As Integer
            Get
                Return _almacenamiento
            End Get
            Set(ByVal value As Integer)
                _almacenamiento = value
            End Set
        End Property
        Private _recepcion As Integer
        Public Property Recepcion() As Integer
            Get
                Return _recepcion
            End Get
            Set(ByVal value As Integer)
                _recepcion = value
            End Set
        End Property
        Private _tipoRecepcion As Integer
        Public Property TipoRecepcion() As Integer
            Get
                Return _tipoRecepcion
            End Get
            Set(ByVal value As Integer)
                _tipoRecepcion = value
            End Set
        End Property
        Private _generico As Integer
        Public Property Generico() As Integer
            Get
                Return _generico
            End Get
            Set(ByVal value As Integer)
                _generico = value
            End Set
        End Property
    End Class
End Namespace