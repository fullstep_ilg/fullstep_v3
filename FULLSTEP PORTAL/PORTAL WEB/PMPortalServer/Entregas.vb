﻿Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Entregas
        Inherits Security
        Private moEntregas As DataSet
        Private moEntregasConfiguration As DataTable

        ''' <summary>
        ''' Devuelve las entregas cargadas por el método LoadData
        ''' </summary>
        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moEntregas
            End Get
        End Property

        ''' <summary>
        ''' Devuelve la configuración de los campos de las entregas a mostrar en pantalla (y también sus anchos)
        ''' </summary>
        Public ReadOnly Property DataConfiguration() As Data.DataTable
            Get
                Return moEntregasConfiguration
            End Get
        End Property

        ''' Revisado por: blp. Fecha: 26/10/2011
        ''' <summary>
        ''' Método que carga las entregas efectuadas por el proveedor.
        ''' </summary>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lProveID">ID del proveedor</param>
        ''' <param name="lCiaComp">Código de la empresa</param>
        ''' <param name="Anio">Año del pedido (no de las entregas)</param>
        ''' <param name="NumPedido">Número del Pedido ("cesta" en la visualización en pantalla)</param>
        ''' <param name="NumOrden">número de la orden ("Pedido" ebn la visualización en pantalla)</param>
        ''' <param name="NumPedidoERP">Número dado en la aplicación ERP del cliente al pedido</param>
        ''' <param name="FecRecepDesde">Fecha a partir de la cual filtrar la fecha de recepción de las entregas</param>
        ''' <param name="FecRecepHasta">Fecha hasta la cual filtrar la fecha de recepción de las entregas</param>
        ''' <param name="VerPedidosSinRecep">
        '''		Si vale 1: Se muestran los pedidos con recepciones y los que aún no tienen ninguna recepción 
        '''			   y que su fecha de entrega solicitada está comprendida entre las fechas FECRECEPDESDE y FECRECEPHASTA
        '''		Si vale 0: No se muestran los pedidos sin recepciones
        ''' </param>
        ''' <param name="NumAlbaran">Albarán del envío</param>
        ''' <param name="NumPedProve">Nº de pedido dado por el proveedor</param>
        ''' <param name="Articulo">Parte del nombre o descripción del artículo</param>
        ''' <param name="FecEntregaSolicitDesde">Fecha a partir de la cual filtrar la fecha de entrega solicitada por el cliente</param>
        ''' <param name="FecEntregaSolicitHasta">Fecha hasta la cual filtrar la fecha de entrega solicitada por el cliente</param>
        ''' <param name="CodEmpresa">ID de la empresa</param>
        ''' <param name="FecEntregaIndicadaProveDesde">Fecha a partir de la cual filtrar la fecha de entrega indicada por el proveedor</param>
        ''' <param name="FecEntregaIndicadaProveHasta">Fecha hasta la cual filtrar la fecha de entrega indicada por el proveedor</param>
        ''' <param name="FecEmisionDesde">Fecha a partir de la cual filtrar la fecha de emisión del pedido</param>
        ''' <param name="FecEmisionHasta">Fecha hasta la cual filtrar la fecha de emisión</param>
        ''' <param name="Centro">Código del centro de coste por el cual filtrar las entregas</param>
        ''' <param name="Partidas">
        ''' Conjunto de todos los códigos de partida presupuestaria por lo que se ha filtrado
        ''' La división entre cada conjunto de partidas es ',' y dentro de cada conjunto es el carácter '@'
        ''' </param>
        ''' <remarks>Llamada desde entregasPedidos.aspx.vb; Máx inferior a 1seg.</remarks>
        Public Sub LoadData(ByVal sIdi As String, ByVal lProveID As Long, ByVal lCiaComp As Long, ByVal Anio As Integer, ByVal NumPedido As Integer, ByVal NumOrden As Integer,
                            ByVal NumPedidoERP As String, ByVal FecRecepDesde As Date, ByVal FecRecepHasta As Date, ByVal VerPedidosSinRecep As Boolean, ByVal NumAlbaran As String,
                            ByVal NumPedProve As String, ByVal Articulo As String, ByVal FecEntregaSolicitDesde As Date, ByVal FecEntregaSolicitHasta As Date, ByVal NumFactura As String,
                            ByVal CodEmpresa As Integer, ByVal FecEntregaIndicadaProveDesde As Date, ByVal FecEntregaIndicadaProveHasta As Date, ByVal FecEmisionDesde As Date,
                            ByVal FecEmisionHasta As Date, ByVal Centro As String, ByVal Partidas As String, ByVal EmpresaPortal As Boolean, ByVal NomPortal As String)
            Authenticate()
            If mRemottingServer Then
                moEntregas = DBServer.Entregas_Get(sIdi, lProveID, lCiaComp, Anio, NumPedido, NumOrden, NumPedidoERP, FecRecepDesde, FecRecepHasta, VerPedidosSinRecep, NumAlbaran,
                                                   NumPedProve, Articulo, FecEntregaSolicitDesde, FecEntregaSolicitHasta, NumFactura, CodEmpresa, FecEntregaIndicadaProveDesde,
                                                   FecEntregaIndicadaProveHasta, FecEmisionDesde, FecEmisionHasta, Centro, Partidas, EmpresaPortal, NomPortal, msSesionId, msIPDir, msPersistID)
            Else
                moEntregas = DBServer.Entregas_Get(sIdi, lProveID, lCiaComp, Anio, NumPedido, NumOrden, NumPedidoERP, FecRecepDesde, FecRecepHasta, VerPedidosSinRecep, NumAlbaran,
                                                   NumPedProve, Articulo, FecEntregaSolicitDesde, FecEntregaSolicitHasta, NumFactura, CodEmpresa, FecEntregaIndicadaProveDesde,
                                                   FecEntregaIndicadaProveHasta, FecEmisionDesde, FecEmisionHasta, Centro, Partidas, EmpresaPortal, NomPortal)
            End If

            If moEntregas IsNot Nothing Then
                moEntregas.Tables(0).TableName = "ENTREGAS"
                moEntregas.Tables(1).TableName = "PARTIDAS_DEN"
                moEntregas.Tables(2).TableName = "CENTROSCOSTE"
            End If
        End Sub

        ''' Revisado por: blp. Fecha: 04/11/2011
        ''' <summary>
        ''' Versión sobrecargada de la función anterior, carga las entregas del último mes y se muestran los pedidos sin recepción. Ver más información en función anterior.
        ''' </summary>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lProveID">código del proveedor</param>
        ''' <param name="lCiaComp">Código de la empresa</param>
        ''' <remarks>Llamada desde entregasPedidos.aspx.vb; Máx inferior a 1seg.</remarks>
        Public Sub LoadData(ByVal sIdi As String, ByVal lProveID As Long, ByVal lCiaComp As Long, ByVal EmpresaPortal As Boolean, ByVal NomPortal As String)
            Authenticate()
            Dim VerPedidosSinRecep As Boolean = True
            LoadData(sIdi, lProveID, lCiaComp, Nothing, Nothing, Nothing, "", DateTime.Now.AddMonths(-1).Date, DateTime.Now.Date, VerPedidosSinRecep, "", "", "",
                     Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, "", Nothing, EmpresaPortal, NomPortal)
        End Sub

        ''' Revisado por: blp. Fecha: 05/12/2011
        ''' <summary>
        ''' Método para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">código de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petición</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde la creación de instancias. Máx. 0,1 seg.</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

        ''' Revisado por: blp. Fecha: 05/12/2011
        ''' <summary>
        ''' Método que obtiene la configuración de los campos a mostrar y su ancho en el Visor de Entregas (EntregasPedidos.aspx) del portal para el usuario y la compañía (proveedor) dados
        ''' </summary>
        ''' <param name="CodUsu">Código del usuario</param>
        ''' <param name="lProveID">Código de la empresa proveedora</param>
        ''' <remarks>Llamada desde PMPortalWeb\script\entregas\entregasPedidos.asmx.vb->CreatewhdgEntregasColumns</remarks>
        Public Sub Obt_Conf_Visor_Entregas(ByVal CodUsu As String, ByVal lProveID As Long)
            Authenticate()
            If mRemottingServer Then
                moEntregasConfiguration = DBServer.Entregas_Obt_Conf_Visor_Entregas(CodUsu, lProveID, msSesionId, msIPDir, msPersistID)
            Else
                moEntregasConfiguration = DBServer.Entregas_Obt_Conf_Visor_Entregas(CodUsu, lProveID)
            End If
        End Sub

        ''' Revisado por: blp. Fecha: 28/10/2011
        ''' <summary>
        ''' Guarda la configuración de los campos a mostrar y su ancho en el Visor de Entregas (EntregasPedidos.aspx) del portal para el usuario y la compañía (proveedor) dados
        ''' </summary>
        ''' <param name="CodUsu">Código del usuario</param>
        ''' <param name="lProveID">Codigo del proveedor</param>
        ''' <param name="Retrasado_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Retrasado_width">Ancho del campo</param>
        ''' <param name="Albaran_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Albaran_width">Ancho del campo</param>
        ''' <param name="Frecep_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Frecep_width">Ancho del campo</param>
        ''' <param name="Emp_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Emp_width">Ancho del campo</param>
        ''' <param name="Erp_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Erp_width">Ancho del campo</param>
        ''' <param name="Pedido_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Pedido_width">Ancho del campo</param>
        ''' <param name="numPedProve_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="numPedProve_width">Ancho del campo</param>
        ''' <param name="Femision_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Femision_width">Ancho del campo</param>
        ''' <param name="Fentrega_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Fentrega_width">Ancho del campo</param>
        ''' <param name="Fentregaprove_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Fentregaprove_width">Ancho del campo</param>
        ''' <param name="Art_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Art_width">Ancho del campo</param>
        ''' <param name="Cantped_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Cantped_width">Ancho del campo</param>
        ''' <param name="Cantrec_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Cantrec_width">Ancho del campo</param>
        ''' <param name="Cantpend_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Cantpend_width">Ancho del campo</param>
        ''' <param name="Uni_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Uni_width">Ancho del campo</param>
        ''' <param name="Prec_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Prec_width">Ancho del campo</param>
        ''' <param name="Impped_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Imped_width">Ancho del campo</param>
        ''' <param name="Imprec_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Imprec_width">Ancho del campo</param>
        ''' <param name="Imppend_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Imppend_width">Ancho del campo</param>
        ''' <param name="Mon_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Mon_width">Ancho del campo</param>
        ''' <param name="CC1_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="CC1_width">Ancho del campo</param>
        ''' <param name="CC2_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="CC2_width">Ancho del campo</param>
        ''' <param name="CC3_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="CC3_width">Ancho del campo</param>
        ''' <param name="CC4_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="CC4_width">Ancho del campo</param>
        ''' <param name="Partida1_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Partida1_width">Ancho del campo</param>
        ''' <param name="Partida2_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Partida2_width">Ancho del campo</param>
        ''' <param name="Partida3_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Partida3_width">Ancho del campo</param>
        ''' <param name="Partida4_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="Partida4_width">Ancho del campo</param>
        ''' <param name="bloq_factura_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="bloq_factura_width">Ancho del campo</param>
        ''' <param name="num_linea_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="num_linea_width">Ancho del campo</param>
        ''' <param name="factura_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="factura_width">Ancho del campo</param>
        ''' <param name="receptor_visible">0->No mostrar el campo en pantalla 1->Mostrar el campo</param>
        ''' <param name="receptor_width">Ancho del campo</param>
        ''' <remarks>
        ''' Llamadas desde EntregasPedido.aspx. Máx 1 seg
        ''' </remarks>
        Public Sub Mod_Conf_Visor_Entregas(ByVal CodUsu As String, ByVal lProveID As Long, ByVal Retrasado_visible As Boolean, ByVal Retrasado_width As Long, ByVal Albaran_visible As Boolean, ByVal Albaran_width As Long, ByVal Frecep_visible As Boolean, ByVal Frecep_width As Long, ByVal Emp_visible As Boolean, ByVal Emp_width As Long, ByVal Erp_visible As Boolean, ByVal Erp_width As Long, ByVal Pedido_visible As Boolean, ByVal Pedido_width As Long, ByVal NumPedProve_visible As Boolean, ByVal NumPedProve_width As Long, ByVal Femision_visible As Boolean, ByVal Femision_width As Long, ByVal Fentrega_visible As Boolean, ByVal Fentrega_width As Long, ByVal Fentregaprove_visible As Boolean, ByVal Fentregaprove_width As Long, ByVal Art_visible As Boolean, ByVal Art_width As Long, ByVal Cantped_visible As Boolean, ByVal Cantped_width As Long, ByVal Cantrec_visible As Boolean, ByVal Cantrec_width As Long, ByVal Cantpend_visible As Boolean, ByVal Cantpend_width As Long, ByVal Uni_visible As Boolean, ByVal Uni_width As Long, ByVal Prec_visible As Boolean, ByVal Prec_width As Long, ByVal Impped_visible As Boolean, ByVal Imped_width As Long, ByVal Imprec_visible As Boolean, ByVal Imprec_width As Long, ByVal Imppend_visible As Boolean, ByVal Imppend_width As Long, ByVal Mon_visible As Boolean, ByVal Mon_width As Long, ByVal CC1_visible As Boolean, ByVal CC1_width As Long, ByVal CC2_visible As Boolean, ByVal CC2_width As Long, ByVal CC3_visible As Boolean, ByVal CC3_width As Long, ByVal CC4_visible As Boolean, ByVal CC4_width As Long, ByVal Partida1_visible As Boolean, ByVal Partida1_width As Long, ByVal Partida2_visible As Boolean, ByVal Partida2_width As Long, ByVal Partida3_visible As Boolean, ByVal Partida3_width As Long, ByVal Partida4_visible As Boolean, ByVal Partida4_width As Long, ByVal bloq_factura_visible As Boolean, ByVal bloq_factura_width As Long, ByVal num_linea_visible As Boolean, ByVal num_linea_width As Long, ByVal factura_visible As Boolean, ByVal factura_width As Long, ByVal receptor_visible As Boolean, ByVal receptor_width As Long, Optional ByVal importe_albaran_visible As String = "", Optional ByVal importe_albaran_width As Long = 0, Optional ByVal msSesionId As String = "", Optional ByVal msIPDir As String = "", Optional ByVal msPersistID As String = "" _
                                           , Optional ByVal CODRECEPERP_visible As Boolean = True, Optional ByVal CODRECEPERP_width As Long = 0)
            Authenticate()
            If mRemottingServer Then
                DBServer.Entregas_Mod_Conf_Visor_Entregas(CodUsu, lProveID, Retrasado_visible, Retrasado_width, Albaran_visible, Albaran_width, Frecep_visible, Frecep_width, Emp_visible, Emp_width, Erp_visible, Erp_width, Pedido_visible, Pedido_width, NumPedProve_visible, NumPedProve_width, Femision_visible, Femision_width, Fentrega_visible, Fentrega_width, Fentregaprove_visible, Fentregaprove_width, Art_visible, Art_width, Cantped_visible, Cantped_width, Cantrec_visible, Cantrec_width, Cantpend_visible, Cantpend_width, Uni_visible, Uni_width, Prec_visible, Prec_width, Impped_visible, Imped_width, Imprec_visible, Imprec_width, Imppend_visible, Imppend_width, Mon_visible, Mon_width, CC1_visible, CC1_width, CC2_visible, CC2_width, CC3_visible, CC3_width, CC4_visible, CC4_width, Partida1_visible, Partida1_width, Partida2_visible, Partida2_width, Partida3_visible, Partida3_width, Partida4_visible, Partida4_width, bloq_factura_visible, bloq_factura_width, num_linea_visible, num_linea_width, factura_visible, factura_width, receptor_visible, receptor_width, importe_albaran_visible, importe_albaran_width, msSesionId, msIPDir, msPersistID, CODRECEPERP_visible, CODRECEPERP_width)
            Else
                DBServer.Entregas_Mod_Conf_Visor_Entregas(CodUsu, lProveID, Retrasado_visible, Retrasado_width, Albaran_visible, Albaran_width, Frecep_visible, Frecep_width, Emp_visible, Emp_width, Erp_visible, Erp_width, Pedido_visible, Pedido_width, NumPedProve_visible, NumPedProve_width, Femision_visible, Femision_width, Fentrega_visible, Fentrega_width, Fentregaprove_visible, Fentregaprove_width, Art_visible, Art_width, Cantped_visible, Cantped_width, Cantrec_visible, Cantrec_width, Cantpend_visible, Cantpend_width, Uni_visible, Uni_width, Prec_visible, Prec_width, Impped_visible, Imped_width, Imprec_visible, Imprec_width, Imppend_visible, Imppend_width, Mon_visible, Mon_width, CC1_visible, CC1_width, CC2_visible, CC2_width, CC3_visible, CC3_width, CC4_visible, CC4_width, Partida1_visible, Partida1_width, Partida2_visible, Partida2_width, Partida3_visible, Partida3_width, Partida4_visible, Partida4_width, bloq_factura_visible, bloq_factura_width, num_linea_visible, num_linea_width, factura_visible, factura_width, receptor_visible, receptor_width, importe_albaran_visible, importe_albaran_width, , , , CODRECEPERP_visible, CODRECEPERP_width)
            End If
        End Sub

        Public Function CodificacionPersonalizadaRecepciones(lCiaComp As Long) As Boolean
            Authenticate()
            Return DBServer.CodificacionPersonalizadaRecepciones(lCiaComp)
        End Function
    End Class
End Namespace