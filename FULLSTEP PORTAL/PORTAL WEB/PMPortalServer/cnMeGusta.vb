﻿Imports System.Collections.Generic

Public Class cnMeGusta
    Private _UsuariosMeGusta As List(Of cnUsuario)
    Public Property UsuariosMeGusta() As List(Of cnUsuario)
        Get
            Return _UsuariosMeGusta
        End Get
        Set(ByVal value As List(Of cnUsuario))
            _UsuariosMeGusta = value
        End Set
    End Property
    Private _NumeroUsuariosMeGusta As Integer
    Public Property NumeroUsuariosMeGusta() As Integer
        Get
            Return _NumeroUsuariosMeGusta
        End Get
        Set(ByVal value As Integer)
            _NumeroUsuariosMeGusta = value
        End Set
    End Property
    Private _MeGusta As Boolean
    Public Property MeGusta() As Boolean
        Get
            Return _MeGusta
        End Get
        Set(ByVal value As Boolean)
            _MeGusta = value
        End Set
    End Property
    Private _UltimoMeGustaUsu As Boolean
    Public Property UltimoMeGustaUsu() As Boolean
        Get
            Return _UltimoMeGustaUsu
        End Get
        Set(ByVal value As Boolean)
            _UltimoMeGustaUsu = value
        End Set
    End Property
End Class
