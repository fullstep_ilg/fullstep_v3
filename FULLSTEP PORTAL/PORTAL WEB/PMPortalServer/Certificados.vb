Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Certificados
        Inherits Fullstep.PMPortalServer.Security

        Private moNoConformidades As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moNoConformidades
            End Get

        End Property

        ''' <summary>
        ''' Carga los certificados publicados del proveedor
        ''' </summary>
        ''' <param name="iCia">El id de la compa�ia para obtener donde esta la base de datos de la compa�ia</param>
        ''' <param name="sUsu">C�digo del proveedor en el portal</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <returns>Certificados</returns>
        ''' <remarks>Llamada desde: certificados\CertificadosVisor.aspx.vb; Tiempo m�ximo: 2sg</remarks>
        Public Function Load_Certificados(ByVal iCia As Integer, ByVal sUsu As String, Optional ByVal sIdi As String = "SPA") As DataSet
			Authenticate()
			Dim dsCertificados As New DataSet
			If mRemottingServer Then
				dsCertificados = DBServer.Load_Certificados(iCia, sUsu, sIdi, msSesionId, msIPDir, msPersistID)
			Else
				dsCertificados = DBServer.Load_Certificados(iCia, sUsu, sIdi)
			End If
			dsCertificados.Tables(0).Columns.Add("DEN", System.Type.GetType("System.String"))
			dsCertificados.Tables(0).Columns("DEN").Expression = "DEN_" & sIdi
			Return dsCertificados
		End Function
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace

