﻿Namespace Fullstep.PMPortalServer
    Public Class Escenario_Campo
        Private _esCampoGeneral As Boolean
        Public Property EsCampoGeneral() As Boolean
            Get
                Return _esCampoGeneral
            End Get
            Set(ByVal value As Boolean)
                _esCampoGeneral = value
            End Set
        End Property
        Private _esCampoDesglose As Boolean
        Public Property EsCampoDesglose() As Boolean
            Get
                Return _esCampoDesglose
            End Get
            Set(ByVal value As Boolean)
                _esCampoDesglose = value
            End Set
        End Property
        Private _id As Integer
        Public Property Id() As Integer
            Get
                Return _id
            End Get
            Set(ByVal value As Integer)
                _id = value
            End Set
        End Property
        Private _denominacion As String
        Public Property Denominacion() As String
            Get
                Return _denominacion
            End Get
            Set(ByVal value As String)
                _denominacion = value
            End Set
        End Property
        Private _nombrePersonalizado As String
        Public Property NombrePersonalizado() As String
            Get
                Return _nombrePersonalizado
            End Get
            Set(ByVal value As String)
                _nombrePersonalizado = value
            End Set
        End Property
        Private _esLista As Boolean
        Public Property EsLista() As Boolean
            Get
                Return _esLista
            End Get
            Set(ByVal value As Boolean)
                _esLista = value
            End Set
        End Property
        Private _tipoCampo As TiposDeDatos.TipoGeneral
        Public Property TipoCampo() As TiposDeDatos.TipoGeneral
            Get
                Return _tipoCampo
            End Get
            Set(ByVal value As TiposDeDatos.TipoGeneral)
                _tipoCampo = value
            End Set
        End Property
        Private _tipoCampoGS As Integer
        Public Property TipoCampoGS() As Integer
            Get
                Return _tipoCampoGS
            End Get
            Set(ByVal value As Integer)
                _tipoCampoGS = value
            End Set
        End Property
        Private _camposDesglose As Dictionary(Of String, Escenario_Campo)
        Public Property CamposDesglose() As Dictionary(Of String, Escenario_Campo)
            Get
                Return _camposDesglose
            End Get
            Set(ByVal value As Dictionary(Of String, Escenario_Campo))
                _camposDesglose = value
            End Set
        End Property
        Private _campoDesglose As Integer
        Public Property CampoDesglose() As Integer
            Get
                Return _campoDesglose
            End Get
            Set(ByVal value As Integer)
                _campoDesglose = value
            End Set
        End Property
        Private _campoPadre As Integer
        Public Property CampoPadre() As Integer
            Get
                Return _campoPadre
            End Get
            Set(ByVal value As Integer)
                _campoPadre = value
            End Set
        End Property
        Private _campoHijo As Integer
        Public Property CampoHijo() As Integer
            Get
                Return _campoHijo
            End Get
            Set(ByVal value As Integer)
                _campoHijo = value
            End Set
        End Property
        Private _denominacion_BD As String
        Public Property Denominacion_BD() As String
            Get
                Return _denominacion_BD
            End Get
            Set(ByVal value As String)
                _denominacion_BD = value
            End Set
        End Property
        Private _ordenVisualizacion As Integer
        Public Property OrdenVisualizacion() As Integer
            Get
                Return _ordenVisualizacion
            End Get
            Set(ByVal value As Integer)
                _ordenVisualizacion = value
            End Set
        End Property
        Private _anchoVisualizacion As Double = 100
        Public Property AnchoVisualizacion() As Double
            Get
                Return _anchoVisualizacion
            End Get
            Set(ByVal value As Double)
                _anchoVisualizacion = value
            End Set
        End Property
        Private _ordenFormulario As Integer
        Public Property OrdenFormulario() As Integer
            Get
                Return _ordenFormulario
            End Get
            Set(ByVal value As Integer)
                _ordenFormulario = value
            End Set
        End Property
        Private _opcionesLista As List(Of cn_fsItem)
        Public Property OpcionesLista() As List(Of cn_fsItem)
            Get
                Return _opcionesLista
            End Get
            Set(ByVal value As List(Of cn_fsItem))
                _opcionesLista = value
            End Set
        End Property
        Private _noFiltrar As Boolean
        Public Property NoFiltrar() As Boolean
            Get
                Return _noFiltrar
            End Get
            Set(ByVal value As Boolean)
                _noFiltrar = value
            End Set
        End Property
        Private _visible As Boolean
        Public Property Visible() As Boolean
            Get
                Return _visible
            End Get
            Set(ByVal value As Boolean)
                _visible = value
            End Set
        End Property


        Public Sub New()
            CamposDesglose = New Dictionary(Of String, Escenario_Campo)
            OpcionesLista = New List(Of cn_fsItem)
        End Sub
    End Class
End Namespace