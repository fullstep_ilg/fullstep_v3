﻿Namespace Fullstep.PMPortalServer

    Public Class FSAL
        Inherits Security
        Public Sub ActualizarAcceso(ByVal tipo As Integer, ByVal fechapet As String, ByVal fecha As String, ByVal sIdRegistro As String, ByVal pagina As String, ByVal iP As String)
            DBServer.FSAL_ActualizarAccesosASPX(tipo, fechapet, fecha, sIdRegistro, pagina, iP)
        End Sub
        Public Sub RegistrarAcceso(ByVal tipo As Integer, ByVal sProducto As String, ByVal fechapet As String, ByVal fecha As String, ByVal pagina As String, ByVal ipost As Int16, _
                                   ByVal iP As String, ByVal sUsuCod As String, ByVal sPaginaOrigen As String, ByVal sNavegador As String, ByVal sIdRegistro As String, _
                                   ByVal sProveCod As String, ByVal sQueryString As String)
            DBServer.FSAL_RegistrarAccesosASPX(tipo, sProducto, fechapet, fecha, pagina, ipost, iP, sUsuCod, sPaginaOrigen, sNavegador, sIdRegistro, sProveCod, sQueryString)
        End Sub
    End Class

End Namespace
