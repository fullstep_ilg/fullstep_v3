Namespace Fullstep.PMPortalServer
    <Serializable()> _
        Public Class GrupoMatNivel3
        Inherits Security

        Private moData As DataSet
        Private msGMN1Cod As String
        Private msGMN1Den As String
        Private msGMN2Cod As String
        Private msGMN2Den As String
        Private msCod As String
        Private msDen As String
        Private moGruposMatNivel4 As GruposMatNivel4
        Property Cod() As String
            Get
                Return msCod
            End Get
            Set(ByVal Value As String)
                msCod = Value
            End Set
        End Property

        Property Den() As String
            Get
                Return msDen
            End Get
            Set(ByVal Value As String)
                msDen = Value
            End Set
        End Property
        Property GMN1Cod() As String
            Get
                Return msGMN1Cod
            End Get
            Set(ByVal Value As String)
                msGMN1Cod = Value
            End Set
        End Property
        Property GMN1Den() As String
            Get
                Return msGMN1Den
            End Get
            Set(ByVal Value As String)
                msGMN1Den = Value
            End Set
        End Property



        Property GMN2Cod() As String
            Get
                Return msGMN2Cod
            End Get
            Set(ByVal Value As String)
                msGMN2Cod = Value
            End Set
        End Property
        Property GMN2Den() As String
            Get
                Return msGMN2Den
            End Get
            Set(ByVal Value As String)
                msGMN2Den = Value
            End Set
        End Property


        Property GruposMatNivel4() As GruposMatNivel4
            Get
                Return moGruposMatNivel4

            End Get
            Set(ByVal Value As GruposMatNivel4)
                moGruposMatNivel4 = Value
            End Set
        End Property

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moData
            End Get

        End Property

        ''' <summary>
        ''' Carga de materiales
        ''' </summary>
        ''' <param name="lCiaComp">Codigo de compania</param>
        ''' <param name="iNumMaximo">Numero maximo de registros a cargar</param>
        ''' <param name="sIdi">idioma</param>
        ''' <param name="CaracteresInicialesCod">Codigo a cargar</param>
        ''' <param name="CaracteresInicialesDen">Denominacion a cargar</param>
        ''' <param name="CoincidenciaTotal">Si se hace like o se hace =</param>
        ''' <param name="OrdenadosPorDen">Si se ordena por den o no</param>
        ''' <remarks>Llamada desde: _common\articulosserver.aspx.vb/Page_Load   _common\campos.ascx.vb/Page_Load     _common\desglose.ascx.vb/Page_Load     
        ''' _common\desglose.ascx.vb/CargarValoresDefecto      _common\validararticulo.aspx.vb/Page_Load    noconformidad\impexp.aspx.vb/DenGS; Tiempo maximo:0,2</remarks>
        Public Sub CargarTodosLosGruposMatDesde(ByVal lCiaComp As Long, ByVal iNumMaximo As Integer, Optional ByVal sIdi As String = "SPA", Optional ByVal CaracteresInicialesCod As String = Nothing, Optional ByVal CaracteresInicialesDen As String = Nothing, Optional ByVal CoincidenciaTotal As Boolean = False, Optional ByVal OrdenadosPorDen As Boolean = False)
            Dim oRow As DataRow
            Authenticate()
            If mRemottingServer Then
                moData = DBServer.GrupoMatNivel3_CargarTodosLosGruposMatDesde(lCiaComp, iNumMaximo, msGMN1Cod, msGMN2Cod, msCod, CaracteresInicialesCod, CaracteresInicialesDen, CoincidenciaTotal, sIdi, OrdenadosPorDen, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.GrupoMatNivel3_CargarTodosLosGruposMatDesde(lCiaComp, iNumMaximo, msGMN1Cod, msGMN2Cod, msCod, CaracteresInicialesCod, CaracteresInicialesDen, CoincidenciaTotal, sIdi, OrdenadosPorDen)
            End If


            If moData.Tables(0).Rows.Count = 0 Then
                moGruposMatNivel4 = New GruposMatNivel4(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)

            Else
                moGruposMatNivel4 = New GruposMatNivel4(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
                For Each oRow In moData.Tables(0).Rows
                    moGruposMatNivel4.Add(msGMN1Cod, oRow.Item("G1DEN"), msGMN2Cod, oRow.Item("G2DEN"), msCod, oRow.Item("G3DEN"), oRow.Item("COD"), oRow.Item("G4DEN"))
                Next

            End If
        End Sub

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace

