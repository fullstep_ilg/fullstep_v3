﻿Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class PRES5
        Inherits Fullstep.PMPortalServer.Security
        ''' <summary>
        ''' Devuelve las partidas y las uons para mostrarlas en un treeview. Añade las relaciones a las tablas del dataset.
        ''' </summary>
        ''' <param name="sPRES5">Código de la partida presupuetaria</param>
        ''' <param name="sUsuario">Código de la persona</param>
        ''' <param name="sIdioma">Idioma del usuario</param>
        ''' <param name="sCentroCoste">Código del centro de coste del que se quieren mostrar las partidas</param>
        ''' <param name="bVigentes">Si true, muestra las partidas vigentes, si false, todas las partidas</param>
        ''' <param name="dFechaInicioDesde">Fecha de inicio desde para buscar partidas</param>
        ''' <param name="dFechaFinHasta">Fecha fin hasta para buscar partidas</param>
        ''' <param name="sFormatoFecha">Formato en el que llegan las fechas</param>
        ''' <returns>Dataset con todas las partidas y sus uons relacionadas</returns>
        Public Function Partidas_LoadData(ByVal lCiaComp As Long, ByVal sPRES5 As String, ByVal sUsuario As String, ByVal sIdioma As String, Optional ByVal sCentroCoste As String = Nothing, Optional ByVal bVigentes As Boolean = True, Optional ByVal dFechaInicioDesde As Date = Nothing, Optional ByVal dFechaFinHasta As Date = Nothing, Optional ByVal sFormatoFecha As String = Nothing) As DataSet
            Authenticate()
            Partidas_LoadData = DBServer.Partidas_Load(lCiaComp, sPRES5, sUsuario, sIdioma, sCentroCoste, bVigentes, dFechaInicioDesde, dFechaFinHasta, sFormatoFecha)
        End Function

        Public Function Partidas_LoadPartidasInstalacion(ByVal lCiaComp As Long, ByVal sIdioma As String) As DataTable
            Authenticate()
            Partidas_LoadPartidasInstalacion = DBServer.Partidas_LoadPartidasInstalacion(lCiaComp, sIdioma)
        End Function

        ''' <summary>
        ''' Método para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">código de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petición</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo máximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace
