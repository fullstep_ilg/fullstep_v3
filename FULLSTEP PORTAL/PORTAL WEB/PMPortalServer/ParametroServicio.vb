﻿Namespace Fullstep.PMPortalServer
    Public Class ParametroServicio
        Public Sub New()
        End Sub
        Private _Den As String
        Public Property Den() As String
            Get
                Return _Den
            End Get
            Set(ByVal value As String)
                _Den = value
            End Set
        End Property
        Private _IndError As Integer
        Public Property IndError() As Integer
            Get
                Return _IndError
            End Get
            Set(ByVal value As Integer)
                _IndError = value
            End Set
        End Property
        Private _Directo As Integer
        Public Property Directo() As Integer
            Get
                Return _Directo
            End Get
            Set(ByVal value As Integer)
                _Directo = value
            End Set
        End Property
        Private _valorText As String
        Public Property valorText() As String
            Get
                Return _valorText
            End Get
            Set(ByVal value As String)
                _valorText = value
            End Set
        End Property
        Private _valorNum As Double
        Public Property valorNum() As Double
            Get
                Return _valorNum
            End Get
            Set(ByVal value As Double)
                _valorNum = value
            End Set
        End Property
        Private _valorFec As String
        Public Property valorFec() As String
            Get
                Return _valorFec
            End Get
            Set(ByVal value As String)
                _valorFec = value
            End Set
        End Property
        Private _valorBool As Integer
        Public Property valorBool() As Integer
            Get
                Return _valorBool
            End Get
            Set(ByVal value As Integer)
                _valorBool = value
            End Set
        End Property
        Private _Campo As Long
        Public Property Campo() As Long
            Get
                Return _Campo
            End Get
            Set(ByVal value As Long)
                _Campo = value
            End Set
        End Property
        Private _Tipo As Integer
        ''' <summary>
        ''' 6-texto
        ''' 2-numero
        ''' 3-fecha
        ''' 4-boolean
        ''' </summary>
        Public Property Tipo() As Integer
            Get
                Return _Tipo
            End Get
            Set(ByVal value As Integer)
                _Tipo = value
            End Set
        End Property
        Private _TipoCampo As Integer
        ''' <summary>
        ''' 0/1- campo formulario
        ''' 2-Peticionario
        ''' 3-Dep. del Peticionario
        ''' 4-UON del Peticionario
        ''' 5-Importe adjudicado a la solicitud
        ''' 6-Importe de la solicitud de compra
        ''' 10-Valor Estatico, es decir, Directo
        ''' </summary>
        Public Property TipoCampo() As Integer
            Get
                Return _TipoCampo
            End Get
            Set(ByVal value As Integer)
                _TipoCampo = value
            End Set
        End Property
        Private _EsSubcampo As Integer
        ''' <summary>
        ''' 0 - campo 1 - desglose
        ''' </summary>
        Public Property EsSubcampo() As Integer
            Get
                Return _EsSubcampo
            End Get
            Set(ByVal value As Integer)
                _EsSubcampo = value
            End Set
        End Property

    End Class
End Namespace