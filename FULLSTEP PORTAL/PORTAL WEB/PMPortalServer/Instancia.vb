Namespace Fullstep.PMPortalServer

    <Serializable()>
    Public Class Instancia
        Inherits Fullstep.PMPortalServer.Security
#Region "Properties"
        Private mlID As Long
        Private mlSolicitud As Long
        Private moSolicitud As Solicitud
        Private moGrupos As Grupos
        Private m_sPeticionario As String
        Private m_sPeticionarioProve As String
        Private m_iPeticionarioProveContacto As Integer
        Private m_lEstado As Long
        Private m_lVersion As Long
        Private m_dFecAlta As Date
        Private m_sNombrePet As String
        Private m_oHistoricoEst As DataSet
        Private m_dImporte As Double
        Private m_sMoneda As String
        Private m_dCambio As Double
        Private m_sDenBloqueActual As String
        Private moDen As MultiIdioma
        Private moProcesos As DataSet
        Private moPedidos As DataSet
        Private m_lIdHistorico As Long
        Private m_bDetalleEditable As Boolean
        Private m_sAprobadorActual As String
        Private m_sNomAprobadorActual As String
        Private m_sFecAsigAprobador As Date
        Private m_sDestinatarioEst As String
        Private m_sNomDestinatario As String
        Private m_sPersonaEst As String
        Private m_sNomPersonaEst As String
        Private m_bTrasladada As Boolean
        Private m_bVer_flujo As Boolean
        Private m_dFechaEstado As Date
        Private m_dFecLimiteTraslado As Date
        Private m_sComentario As String
        Private m_sCampoImporte As String
        Private moData As DataSet
        Private miEtapaActual As Long
        Private msDenEtapaActual As String
        Private miRolActual As Long
        Private miInstanciaBloque As Long
        Private mbInstanciaBloqueTrasladada As Boolean
        Private miInstanciaBloqueBloq As Integer
        Private m_iEnProceso As Integer
        Private mlDiferenciaUTCServidor As Long
        Private m_iTipoVersion As Integer
        Private m_bCertifActivo As Boolean
        Private m_bCertifPublicado As Boolean
        Private mbVerDetalleFlujo As Boolean
        Private mbVerDetallePersona As Boolean
        Private m_iAccion As Integer
        Public Property DetalleEditable() As Boolean
            Get
                Return m_bDetalleEditable
            End Get

            Set(ByVal Value As Boolean)
                m_bDetalleEditable = Value
            End Set
        End Property
        Public Property VerDetalleFlujo() As Boolean
            Get
                Return mbVerDetalleFlujo
            End Get
            Set(ByVal Value As Boolean)
                mbVerDetalleFlujo = Value
            End Set
        End Property
        Public Property Ver_flujo() As Boolean
            Get
                Return m_bVer_flujo
            End Get

            Set(ByVal Value As Boolean)
                m_bVer_flujo = Value
            End Set
        End Property
        Property Pedidos() As DataSet
            Get
                Pedidos = moPedidos
            End Get

            Set(ByVal Value As DataSet)
                moPedidos = Value
            End Set
        End Property
        Property Procesos() As DataSet
            Get
                Procesos = moProcesos
            End Get

            Set(ByVal Value As DataSet)
                moProcesos = Value
            End Set
        End Property
        Property Den(ByVal Idioma As String) As String
            Get
                Den = moDen(Idioma)
            End Get

            Set(ByVal Value As String)
                If Not moDen.Contains(Idioma) Then
                    moDen.Add(Idioma, Value)
                Else
                    moDen(Idioma) = Value
                End If

            End Set
        End Property
        Public Property BloqueActual() As String
            Get
                Return m_sDenBloqueActual
            End Get

            Set(ByVal Value As String)
                m_sDenBloqueActual = Value
            End Set
        End Property
        Public Property Moneda() As String
            Get
                Return m_sMoneda
            End Get

            Set(ByVal Value As String)
                m_sMoneda = Value
            End Set
        End Property
        Public Property Cambio() As Double
            Get
                Return m_dCambio
            End Get

            Set(ByVal Value As Double)
                m_dCambio = Value
            End Set
        End Property
        Public Property Importe() As Double
            Get
                Return m_dImporte
            End Get

            Set(ByVal Value As Double)
                m_dImporte = Value
            End Set
        End Property
        Public Property HistoricoEst() As DataSet
            Get
                Return m_oHistoricoEst
            End Get

            Set(ByVal Value As DataSet)
                m_oHistoricoEst = Value
            End Set
        End Property
        Public Property ID() As Long
            Get
                Return mlID
            End Get

            Set(ByVal Value As Long)
                mlID = Value
            End Set
        End Property
        Public Property Solicitud() As Solicitud
            Get
                Solicitud = moSolicitud
            End Get
            Set(ByVal Value As Solicitud)
                moSolicitud = Value
            End Set
        End Property
        Property Grupos() As Grupos
            Get
                Grupos = moGrupos

            End Get
            Set(ByVal Value As Grupos)
                moGrupos = Value
            End Set
        End Property
        Public Property Peticionario() As String
            Get
                Return m_sPeticionario
            End Get

            Set(ByVal Value As String)
                m_sPeticionario = Value
            End Set
        End Property
        Public Property PeticionarioProve() As String
            Get
                Return m_sPeticionarioProve
            End Get

            Set(ByVal Value As String)
                m_sPeticionarioProve = Value
            End Set
        End Property
        Public Property PeticionarioProveContacto() As Integer
            Get
                Return m_iPeticionarioProveContacto
            End Get

            Set(ByVal Value As Integer)
                m_iPeticionarioProveContacto = Value
            End Set
        End Property
        Public Property NombrePeticionario() As String
            Get
                Return m_sNombrePet
            End Get

            Set(ByVal Value As String)
                m_sNombrePet = Value
            End Set
        End Property
        Public Property Estado() As Long
            Get
                Return m_lEstado
            End Get

            Set(ByVal Value As Long)
                m_lEstado = Value
            End Set
        End Property
        Public Property Version() As Long
            Get
                Return m_lVersion
            End Get

            Set(ByVal Value As Long)
                m_lVersion = Value
            End Set
        End Property
        Public Property FechaAlta() As Date
            Get
                Return m_dFecAlta
            End Get

            Set(ByVal Value As Date)
                m_dFecAlta = Value
            End Set
        End Property
        Public Property IdHistorico() As Long
            Get
                Return m_lIdHistorico
            End Get

            Set(ByVal Value As Long)
                m_lIdHistorico = Value
            End Set
        End Property
        Public Property AprobadorActual() As String
            Get
                Return m_sAprobadorActual
            End Get

            Set(ByVal Value As String)
                m_sAprobadorActual = Value
            End Set
        End Property
        Public Property NombreAprobadorActual() As String
            Get
                Return m_sNomAprobadorActual
            End Get

            Set(ByVal Value As String)
                m_sNomAprobadorActual = Value
            End Set
        End Property
        Public Property FecAsigAprobador() As Date
            Get
                Return m_sFecAsigAprobador
            End Get

            Set(ByVal Value As Date)
                m_sFecAsigAprobador = Value
            End Set
        End Property
        Public Property DestinatarioEst() As String
            Get
                Return m_sDestinatarioEst
            End Get

            Set(ByVal Value As String)
                m_sDestinatarioEst = Value
            End Set
        End Property
        Public Property NombreDestinatario() As String
            Get
                Return m_sNomDestinatario
            End Get

            Set(ByVal Value As String)
                m_sNomDestinatario = Value
            End Set
        End Property
        Public Property PersonaEst() As String
            Get
                Return m_sPersonaEst
            End Get

            Set(ByVal Value As String)
                m_sPersonaEst = Value
            End Set
        End Property
        Public Property NombrePersonaEst() As String
            Get
                Return m_sNomPersonaEst
            End Get

            Set(ByVal Value As String)
                m_sNomPersonaEst = Value
            End Set
        End Property
        Public Property Trasladada() As Boolean
            Get
                Return m_bTrasladada
            End Get

            Set(ByVal Value As Boolean)
                m_bTrasladada = Value
            End Set
        End Property
        Public Property FechaEstado() As Date
            Get
                Return m_dFechaEstado
            End Get

            Set(ByVal Value As Date)
                m_dFechaEstado = Value
            End Set
        End Property
        Public Property FecLimiteTraslado() As Date
            Get
                Return m_dFecLimiteTraslado
            End Get

            Set(ByVal Value As Date)
                m_dFecLimiteTraslado = Value
            End Set
        End Property
        Public Property ComentarioEstado() As String
            Get
                Return m_sComentario
            End Get

            Set(ByVal Value As String)
                m_sComentario = Value
            End Set
        End Property
        Public Property CampoImporte() As String
            Get
                Return m_sCampoImporte
            End Get

            Set(ByVal Value As String)
                m_sCampoImporte = Value
            End Set
        End Property
        Public Property Etapa() As Long
            Get
                Return miEtapaActual
            End Get
            Set(ByVal Value As Long)
                miEtapaActual = Value
            End Set
        End Property
        Public Property DenEtapaActual() As String
            Get
                Return msDenEtapaActual
            End Get
            Set(ByVal Value As String)
                msDenEtapaActual = Value
            End Set
        End Property
        Public Property RolActual() As Long
            Get
                Return miRolActual
            End Get
            Set(ByVal Value As Long)
                miRolActual = Value
            End Set
        End Property
        Public Property InstanciaBloque() As Integer
            Get
                Return miInstanciaBloque
            End Get
            Set(ByVal Value As Integer)
                miInstanciaBloque = Value
            End Set
        End Property
        Public Property InstanciaBloqueTrasladada() As Boolean
            Get
                Return mbInstanciaBloqueTrasladada
            End Get
            Set(ByVal Value As Boolean)
                mbInstanciaBloqueTrasladada = Value
            End Set
        End Property
        Public Property InstanciaBloqueBloq() As Integer
            Get
                Return miInstanciaBloqueBloq
            End Get
            Set(ByVal Value As Integer)
                miInstanciaBloqueBloq = Value
            End Set
        End Property
        Public Property EnProceso() As Integer
            Get
                Return m_iEnProceso
            End Get
            Set(ByVal Value As Integer)
                m_iEnProceso = Value
            End Set
        End Property
        Public Property DiferenciaUTCServidor() As Long
            Get
                Return mlDiferenciaUTCServidor
            End Get

            Set(ByVal Value As Long)
                mlDiferenciaUTCServidor = Value
            End Set
        End Property
        Public Property TipoVersion() As Integer
            Get
                Return m_iTipoVersion
            End Get
            Set(ByVal Value As Integer)
                m_iTipoVersion = Value
            End Set
        End Property
        Public Property CertifActivo() As Boolean
            Get
                Return m_bCertifActivo
            End Get
            Set(ByVal Value As Boolean)
                m_bCertifActivo = Value
            End Set
        End Property
        Public Property CertifPublicado() As Boolean
            Get
                Return m_bCertifPublicado
            End Get
            Set(ByVal Value As Boolean)
                m_bCertifPublicado = Value
            End Set
        End Property
        Public Property Accion() As Integer
            Get
                Return m_iAccion
            End Get
            Set(ByVal Value As Integer)
                m_iAccion = Value
            End Set
        End Property
#End Region
        ''' <summary>
        ''' Cargar las propiedades de la Instancia
        ''' </summary>
        ''' <param name="lCiaComp">Codigo de compania</param>
        ''' <param name="sIdi">Idioma</param>        
        ''' <remarks>Llamada desde: Multiples pantallas (todos los oIntancia.load); Tiempo m�ximo: 0,2</remarks>
        Public Sub Load(ByVal lCiaComp As Long, ByVal sIdi As String, Optional TieneWorkflow As Boolean = False)
            Authenticate()
            Dim data As DataSet

            If mRemottingServer Then
                data = DBServer.Instancia_Load(lCiaComp, mlID, sIdi, TieneWorkflow, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Instancia_Load(lCiaComp, mlID, sIdi, TieneWorkflow)
            End If

            If Not data.Tables(0).Rows.Count = 0 Then
                m_sPeticionario = data.Tables(0).Rows(0).Item("PETICIONARIO").ToString
                m_sNombrePet = data.Tables(0).Rows(0).Item("NOMBRE_PET").ToString
                m_lEstado = DBNullToSomething(data.Tables(0).Rows(0).Item("ESTADO"))
                m_lVersion = DBNullToSomething(data.Tables(0).Rows(0).Item("NUM_VERSION"))
                m_dFecAlta = DBNullToSomething(data.Tables(0).Rows(0).Item("FEC_ALTA"))
                m_dImporte = DBNullToSomething(data.Tables(0).Rows(0).Item("IMPORTE"))
                m_sMoneda = DBNullToSomething(data.Tables(0).Rows(0).Item("MON"))
                m_dCambio = DBNullToSomething(data.Tables(0).Rows(0).Item("CAMBIO"))
                m_lIdHistorico = DBNullToSomething(data.Tables(0).Rows(0).Item("INSTANCIA_EST"))
                m_sPeticionarioProve = DBNullToSomething(data.Tables(0).Rows(0).Item("PETICIONARIO_PROVE"))
                m_iPeticionarioProveContacto = DBNullToSomething(data.Tables(0).Rows(0).Item("PETICIONARIO_CON"))

                mlSolicitud = DBNullToSomething(data.Tables(0).Rows(0).Item("SOLICITUD"))
                moSolicitud = New Solicitud(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
                moSolicitud.ID = mlSolicitud
                moSolicitud.TipoSolicit = DBNullToSomething(data.Tables(0).Rows(0).Item("TIPO"))
                moSolicitud.Formulario = New Formulario(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
                moSolicitud.Formulario.Id = DBNullToSomething(data.Tables(0).Rows(0).Item("FORMULARIO"))
                moSolicitud.Workflow = DBNullToSomething(data.Tables(0).Rows(0).Item("WORKFLOW"))
                moSolicitud.Den(sIdi) = DBNullToSomething(data.Tables(0).Rows(0).Item("DEN_SOLICIT"))
                m_bTrasladada = IIf(data.Tables(0).Rows(0).Item("TRASLADADA") = 1, True, False)
                m_sCampoImporte = DBNullToSomething(data.Tables(0).Rows(0).Item("CAMPO_IMPORTE"))

                m_iEnProceso = DBNullToSomething(data.Tables(0).Rows(0).Item("EN_PROCESO"))

                moDen(sIdi) = data.Tables(0).Rows(0).Item("DESCR").ToString

                If Not TieneWorkflow Then m_iTipoVersion = DBNullToSomething(data.Tables(0).Rows(0).Item("VITIPO"))
            End If

            data = Nothing
        End Sub
        ''' <summary>
        ''' Procedimiento que elimina una instancia con la factura relacionada
        ''' </summary>
        ''' <param name="lCiaComp">ID de la compa��a</param>
        ''' <param name="lIdFactura">ID Factura</param>
        ''' <remarks>
        ''' LLamada desde: /Facturas/EliminarFactura/page_load
        ''' Tiempo m�ximo: 0,5 seg</remarks>
        Public Sub EliminarFactura(ByVal lCiaComp As Long, ByVal lIdFactura As Long)
            Authenticate()
            If mRemottingServer Then
                DBServer.EliminarInstancia(lCiaComp, mlID, lIdFactura, msSesionId, msIPDir, msPersistID)
            Else
                DBServer.EliminarInstancia(lCiaComp, mlID, lIdFactura)
            End If
        End Sub
        ''' <summary>
        ''' Actualiza en la instancia el campo de seguimiento para monitorizarla o no.
        ''' </summary>
        ''' <param name="iSeg">1 si se monitoriza, 0 si no.</param>
        ''' <param name="sProve">Codigo del proveedor</param>
        ''' <param name="sContacto">Id del contacto</param>
        ''' <remarks></remarks>
        Public Sub ActualizarMonitorizacion(ByVal lCiaComp As Long, ByVal iSeg As Byte, ByVal sProve As String, ByVal sContacto As Long)
            Authenticate()
            If mRemottingServer Then
                DBServer.Instancia_ActualizarMonitorizacion(lCiaComp, mlID, iSeg, sProve, sContacto, msSesionId, msIPDir, msPersistID)
            Else
                DBServer.Instancia_ActualizarMonitorizacion(lCiaComp, mlID, iSeg, sProve, sContacto)
            End If
        End Sub
        ''' <summary>
        ''' Obtendr� la etapa actual en la que se encuentra la instancia bas�ndose en la persona o proveedor que se pasa por par�metro, 
        ''' que ser� un usuario/proveedor al que se le ha trasladado la solicitud
        ''' </summary>
        ''' <param name="Instancia">Id de la instancia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="sPer">Cod. de la persona</param>
        ''' <returns>Un dataset con los datos de la etapa actual</returns>
        ''' <remarks>Llamada desde: PMWeb --> workflow/gestiontrasladada.aspx.vb; Tiempo m�ximo: 1 sg</remarks>
        Public Sub DevolverEtapaActualTraslado(ByVal lCiaComp As Long, ByVal sIdi As String)
            Authenticate()
            Dim data As DataSet
            Dim i As Integer
            If mRemottingServer Then
                data = DBServer.Instancia_DevolverEtapaActualTraslado(lCiaComp, mlID, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Instancia_DevolverEtapaActualTraslado(lCiaComp, mlID, sIdi)
            End If


            If Not data.Tables(0).Rows.Count = 0 Then
                For i = 0 To data.Tables(0).Rows.Count - 1
                    msDenEtapaActual = DBNullToSomething(data.Tables(0).Rows(i).Item("BLOQUE_DEN"))
                    miEtapaActual = DBNullToSomething(data.Tables(0).Rows(i).Item("BLOQUE"))
                    miRolActual = DBNullToSomething(data.Tables(0).Rows(i).Item("ROL"))
                    miInstanciaBloque = DBNullToSomething(data.Tables(0).Rows(i).Item("ID"))
                    mbInstanciaBloqueTrasladada = SQLBinaryToBoolean(data.Tables(0).Rows(i).Item("TRASLADADA"))
                    miInstanciaBloqueBloq = data.Tables(0).Rows(i).Item("BLOQ")
                    m_sDestinatarioEst = DBNullToSomething(data.Tables(0).Rows(i).Item("CUMPLIMENTADOR"))
                    If DBNullToDbl(data.Tables(0).Rows(i).Item("COMO_ASIGNAR")) = 3 And IsDBNull(data.Tables(0).Rows(i).Item("PER_ROL")) Then
                        m_sAprobadorActual = Nothing 'Solo lo uso para saber si est� asignada una persona al rol
                    Else
                        m_sAprobadorActual = data.Tables(0).Rows(i).Item("PER_ROL")
                    End If
                    mbVerDetalleFlujo = SQLBinaryToBoolean(data.Tables(0).Rows(i).Item("VER_FLUJO"))
                    mbVerDetallePersona = SQLBinaryToBoolean(data.Tables(0).Rows(i).Item("VER_DETALLE_PER"))
                Next
            End If

            data = Nothing

        End Sub
        ''' <summary>
        ''' Funcion que devuelve si una instancia tiene sustitucion de destinatario
        ''' </summary>
        ''' <param name="sPer">Codigo de persona</param>
        ''' <param name="sDestinatario">codigo de destinatario</param>
        ''' <returns>Una variable booleana que indica si existe un sustituto de destinatario</returns>
        ''' <remarks>
        ''' Llamada desde: PmWeb/gestiontrasladada/Page_load
        ''' Tiempo m�ximo: 0,3 seg</remarks>
        Public Function EsSustitutodeDestinatario(ByVal sPer As String, ByVal sDestinatario As String) As Boolean
            Authenticate()
            Return DBServer.Instancia_EsSustitutodeDestinatario(sPer, sDestinatario)

            If mRemottingServer Then
                Return DBServer.Instancia_EsSustitutodeDestinatario(sPer, sDestinatario, msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.Instancia_EsSustitutodeDestinatario(sPer, sDestinatario)
            End If
        End Function
        ''' <summary>
        ''' Carga los campos de la instancia en la clase, carga tb la diferencia horaria del servidor respecto a la hora 0 UTC
        ''' </summary>
        ''' <param name="sIdi">Codigo idioma</param>
        ''' <param name="sUsuario">Codigo usuario</param> 
        ''' <param name="sProve">Codigo proveedor</param>       
        ''' <param name="bNuevoWorkfl">si tiene workflow o no</param>
        ''' <remarks>Llamada desde=Detalle.aspx.vb // NWgestionInstancia.aspx.vb
        '''                        instanciaasignada.aspx.vb   // gestiontrasladada.aspx.vb
        '''                        flowDiagramExport.aspx.vb   // flowDiagram.aspx.vb
        '''                        detalleSolicConsulta.aspx.vb// aprobacion.aspx.vb
        '''                        NWDetalleSolicitud.aspx.vb  // impexp.aspx.vb
        '''                        noconformidadRevisor.aspx.vb// service.asmx.vb
        '''                        detalleCertificado.aspx.vb
        '''; Tiempo m�ximo=depende de los campos..grupos.</remarks>
        Public Sub CargarCamposInstancia(ByVal lCiaComp As Long, ByVal lCiaProve As Long, ByVal sIdi As String, Optional ByVal sUsuario As String = Nothing,
                                         Optional ByVal sProve As String = Nothing, Optional ByVal bRecuperarEnviada As Boolean = True,
                                         Optional ByVal bNuevoWorkflow As Boolean = False)
            Authenticate()
            Dim data As DataSet
            Dim oFormRow As DataRow
            Dim oGroupRow As DataRow
            Dim oGrupo As Grupo

            If mRemottingServer Then
                data = DBServer.Instancia_CargarGruposYCampos(lCiaComp, lCiaProve, mlID, m_lVersion, sIdi, sUsuario, sProve, bRecuperarEnviada, bNuevoWorkflow, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Instancia_CargarGruposYCampos(lCiaComp, lCiaProve, mlID, m_lVersion, sIdi, sUsuario, sProve, bRecuperarEnviada, bNuevoWorkflow)
            End If

            If Not data.Tables(0).Rows.Count = 0 Then
                For Each oFormRow In data.Tables(0).Rows
                    moGrupos = New Grupos(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)

                    For Each oGroupRow In oFormRow.GetChildRows("REL_INST_GRUPO")
                        oGrupo = New Grupo(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
                        oGrupo.Id = oGroupRow.Item("ID")
                        oGrupo.NumCampos = oGroupRow.Item("NUMCAMPOS")
                        oGrupo.Den(sIdi) = oGroupRow.Item("DEN_" & sIdi).ToString

                        oGrupo.DSCampos = New DataSet
                        oGrupo.DSCampos.Merge(oGroupRow.GetChildRows("REL_GRUPO_CAMPO"))
                        oGrupo.DSCampos.Merge(oGroupRow.GetChildRows("REL_GRUPO_LISTA"))
                        If oGrupo.DSCampos.Tables.Count > 1 Then
                            oGrupo.DSCampos.Relations.Add("REL_CAMPO_LISTA", oGrupo.DSCampos.Tables(0).Columns("ID"), oGrupo.DSCampos.Tables(1).Columns("CAMPO_DEF"), False)
                        End If
                        oGrupo.DSCampos.Merge(oGroupRow.GetChildRows("REL_GRUPO_ADJUNTO"))
                        If oGrupo.DSCampos.Tables.Count > 2 Then
                            oGrupo.DSCampos.Relations.Add("REL_CAMPO_ADJUN", oGrupo.DSCampos.Tables(0).Columns("ID_CAMPO"), oGrupo.DSCampos.Tables(2).Columns("CAMPO"), False)
                        ElseIf (oGrupo.DSCampos.Tables.Count = 2 And oGrupo.DSCampos.Relations.Count = 0) Then
                            oGrupo.DSCampos.Relations.Add("REL_CAMPO_ADJUN", oGrupo.DSCampos.Tables(0).Columns("ID_CAMPO"), oGrupo.DSCampos.Tables(1).Columns("CAMPO"), False)
                        End If
                        Dim lTables As Integer
                        lTables = oGrupo.DSCampos.Tables.Count

                        oGrupo.DSCampos.Merge(oGroupRow.GetChildRows("REL_GRUPO_BLOQUEO"))

                        If oGrupo.DSCampos.Tables.Count > lTables Then
                            oGrupo.DSCampos.Relations.Add("REL_CAMPO_BLOQUEO", oGrupo.DSCampos.Tables(0).Columns("ID"), oGrupo.DSCampos.Tables(lTables).Columns("CAMPO"), False)
                            lTables = lTables + 1
                        End If

                        moGrupos.Add(oGrupo)
                    Next
                Next

                ''Carga el la instancia la diferencia horaria del servidor si se trata de cert o noconf
                If Not bNuevoWorkflow Then
                    If data.Tables.Count >= 5 Then
                        mlDiferenciaUTCServidor = data.Tables(5).Rows(0).Item("DIFERENCIA")
                    End If
                End If
            End If
            data = Nothing
        End Sub
        ''' <summary>
        ''' Obtiene los Campos calculados a partir de una instancia y version
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: script\_common\AyudaCampo.aspx  script\_common\guardarinstancia.aspx      script\_common\modulos\modInstancia
        ''' script\_common\recalcularimportes.aspx.vb; Tiempo m�ximo: 0,2</remarks>
        Public Function LoadCamposCalculados(ByVal lCiaComp As Long, Optional ByVal sProve As String = Nothing, Optional Workflow As Boolean = True) As DataSet
            Authenticate()
            Dim data As DataSet
            If mRemottingServer Then
                data = DBServer.Instancia_LoadCamposCalculados(lCiaComp, mlID, m_lVersion, Nothing, sProve, Workflow, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Instancia_LoadCamposCalculados(lCiaComp, mlID, m_lVersion, Nothing, sProve, Workflow)
            End If
            Return data
        End Function
        ''' <summary>
        ''' Se llama a esta funci�n para completar los datos cargados en la solicitud con         
        '''                todos aquellos campos que bien por estar ocultos o ser de solo lectura no se    
        '''                han podido recoger del formulario.
        ''' </summary>
        ''' <param name="ds"> dataset con los campos recogidos de la solicitud (escritura)</param>
        ''' <param name="sPer">c�digo de la persona </param>        
        ''' <param name="bNuevoWorkflow">true/false</param>
        ''' <param name="bMaper">true/false. Si hay integraci�n y se est� cargando el dataset para pasarselo al mapper.</param>   
        ''' <param name="sIdioma">idioma del usuario</param>
        ''' <returns>ds: dataset completado con los campos no visibles y de solo lectura </returns>
        ''' <remarks>Llamada desde:guardarInstancia.aspx --> GuardarSinWorkflow, GuardarConWorkflow; Tiempo m�ximo:0 sg</remarks>
        Public Function CompletarFormulario(ByVal lCiaComp As Long, ByVal ds As DataSet, ByVal sProve As String, Optional ByVal sAprob As String = "", Optional ByVal bNuevoWorkflow As Boolean = False) As DataSet
            Dim idCampo As Long
            Dim dRowsCcd() As DataRow
            Dim bPadreNoVisibleHijoVa As Boolean = False

            If ds.Tables("TEMP").Rows.Count < 1 Then
                Return ds
            Else
                idCampo = ds.Tables("TEMP").Rows(0).Item("CAMPO")
            End If

            Dim dsInvisibles As DataSet
            If mRemottingServer Then
                dsInvisibles = DBServer.Instancia_LoadInvisibles(lCiaComp, idCampo, sProve, sAprob, bNuevoWorkflow, msSesionId, msIPDir, msPersistID)
            Else
                dsInvisibles = DBServer.Instancia_LoadInvisibles(lCiaComp, idCampo, sProve, sAprob, bNuevoWorkflow)
            End If

            Dim dRow As DataRow
            Dim dNewRow As DataRow
            Dim dRows() As DataRow
            Dim dRowsAdjun() As DataRow
            'METEMOS LOS CAMPOS SIMPLES QUE ESTUVIERAN OCULTOS JUNTO CON LOS VALORES
            For Each dRow In dsInvisibles.Tables(0).Rows
                If ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("ID") & " AND CAMPO_DEF=-1000").Length = 1 Then
                    'lo ha metido el jsalta aun siendo oculto.
                    dNewRow = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("ID"))(0)
                    dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
                Else
                    dNewRow = ds.Tables("TEMP").NewRow
                    dNewRow.Item("CAMPO") = dRow.Item("ID")
                    dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                    dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                    dNewRow.Item("CAMBIO") = dRow.Item("CAMBIO")
                    Try
                        If dRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                    Catch ex As Exception
                    End Try
                    dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                    dNewRow.Item("ES_SUBCAMPO") = 0
                    dNewRow.Item("SUBTIPO") = dRow.Item("SUBTIPO")
                    dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
                    Try
                        ds.Tables("TEMP").Rows.Add(dNewRow)
                    Catch ex As Exception
                    End Try
                End If
            Next
            '------------------------------------------------------------------------------------------------------------
            If dsInvisibles.Tables.Count > 9 Then
                'METEMOS LOS GRUPOS QUE ESTUVIERAN OCULTOS El motivo de estar ocultos es q estan en grupos
                'no solicitados de una no conformidad
                For Each dRow In dsInvisibles.Tables(6).Rows
                    dNewRow = ds.Tables("TEMPNOCONF_SOLICIT_ACC").NewRow
                    dNewRow.Item("COPIA_CAMPO") = dRow.Item("COPIA_CAMPO")
                    dNewRow.Item("SOLICITAR") = 0
                    dNewRow.Item("FECHA_LIMITE") = dRow.Item("FECHA_LIMITE")
                    dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
                    Try
                        ds.Tables("TEMPNOCONF_SOLICIT_ACC").Rows.Add(dNewRow)
                    Catch ex As Exception
                    End Try
                Next
                'METEMOS LAS ACCIONES QUE ESTUVIERAN OCULTOS El motivo de estar ocultos es q estan en grupos
                'no solicitados de una no conformidad
                For Each dRow In dsInvisibles.Tables(7).Rows
                    dNewRow = ds.Tables("TEMPNOCONF_ACC").NewRow
                    dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                    dNewRow.Item("LINEA") = dRow.Item("LINEA")
                    dNewRow.Item("ESTADO") = dRow.Item("ESTADO")
                    dNewRow.Item("ESTADO_INT") = dRow.Item("ESTADO_INT")
                    dNewRow.Item("COMENT") = dRow.Item("COMENT")
                    dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                    dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
                    Try
                        ds.Tables("TEMPNOCONF_ACC").Rows.Add(dNewRow)
                    Catch ex As Exception
                    End Try
                Next
                'METEMOS LOS CAMPOS SIMPLES QUE ESTUVIERAN OCULTOS JUNTO CON LOS VALORES. El motivo de estar ocultos es q estan en grupos
                'no solicitados de una no conformidad
                For Each dRow In dsInvisibles.Tables(8).Rows
                    dNewRow = ds.Tables("TEMP").NewRow
                    dNewRow.Item("CAMPO") = dRow.Item("ID")
                    dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                    dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                    dNewRow.Item("CAMBIO") = dRow.Item("CAMBIO")
                    Try
                        If dRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                    Catch ex As Exception
                    End Try
                    dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                    dNewRow.Item("ES_SUBCAMPO") = 0
                    dNewRow.Item("SUBTIPO") = dRow.Item("SUBTIPO")
                    dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
                    Try
                        ds.Tables("TEMP").Rows.Add(dNewRow)
                    Catch ex As Exception

                    End Try
                Next
                'METEMOS LOS ADJUNTOS DE CAMPOS SIMPLES QUE ESTUVIERAN OCULTOS JUNTO CON LOS VALORES. El motivo de estar ocultos es q estan en grupos
                'no solicitados de una no conformidad
                For Each dRow In dsInvisibles.Tables(9).Rows
                    dNewRow = ds.Tables("TEMPADJUN").NewRow
                    dNewRow.Item("CAMPO") = dRow.Item("CAMPO")
                    dNewRow.Item("ID") = dRow.Item("ID")
                    dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
                    Try
                        ds.Tables("TEMPADJUN").Rows.Add(dNewRow)
                    Catch ex As Exception
                    End Try
                Next
                'METEMOS LOS CAMPOS DE DESGLOSE QUE ESTUVIERAN OCULTOS. El motivo de estar ocultos es q estan en grupos
                'no solicitados de una no conformidad
                For Each dRow In dsInvisibles.Tables(10).Rows
                    dNewRow = ds.Tables("TEMP").NewRow
                    dNewRow.Item("CAMPO") = dRow.Item("CAMPO_HIJO")
                    dNewRow.Item("ES_SUBCAMPO") = 1
                    dNewRow.Item("CAMPO_DEF") = dRow.Item("CAMPO_HIJO_DEF")
                    Try
                        ds.Tables("TEMP").Rows.Add(dNewRow)
                    Catch ex As Exception
                    End Try

                    dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                    dNewRow.Item("LINEA") = 0
                    dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                    dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                    dNewRow.Item("CAMPO_PADRE_DEF") = dRow.Item("CAMPO_PADRE_DEF")
                    dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("CAMPO_HIJO_DEF")
                    Try
                        ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                    Catch ex As Exception
                    End Try
                Next
                'METEMOS LAS LINEAS DE DESGLOSE QUE ESTUVIERAN OCULTOS JUNTO CON LOS VALORES. El motivo de estar ocultos es q estan en grupos
                'no solicitados de una no conformidad
                For Each dRow In dsInvisibles.Tables(11).Rows
                    dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                    dNewRow.Item("LINEA") = dRow.Item("LINEA")
                    dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                    dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                    dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                    dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                    dNewRow.Item("VALOR_FEC") = dRow.Item("VALOR_FEC")
                    dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                    dNewRow.Item("CAMBIO") = dRow.Item("CAMBIO")
                    dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                    dNewRow.Item("CAMPO_PADRE_DEF") = dRow.Item("CAMPO_PADRE_DEF")
                    dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("CAMPO_HIJO_DEF")
                    Try
                        ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                    Catch ex As Exception
                    End Try
                Next
                ''METEMOS LOS ADJUNTOS DE LINEAS DE DESGLOSE QUE ESTUVIERAN OCULTOS JUNTO CON LOS VALORES. El motivo de estar ocultos es q estan en grupos
                'no solicitados de una no conformidad
                For Each dRow In dsInvisibles.Tables(12).Rows
                    dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                    dNewRow.Item("LINEA") = dRow.Item("LINEA")
                    dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                    dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                    dNewRow.Item("ID") = dRow.Item("ADJUN")
                    dNewRow.Item("TIPO") = 1

                    dNewRow.Item("CAMPO_PADRE_DEF") = dRow.Item("CAMPO_PADRE_DEF")
                    dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("CAMPO_HIJO_DEF")
                    Try
                        ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                    Catch ex As Exception
                    End Try
                Next
            End If
            '------------------------------------------------------------------------------------------------------------
            'METEMOS LOS CAMPOS DE DESGLOSE QUE ESTUVIERAN OCULTOS
            For Each dRow In dsInvisibles.Tables(1).Rows
                If ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_HIJO")).Length = 0 Then
                    dNewRow = ds.Tables("TEMP").NewRow
                    dNewRow.Item("CAMPO") = dRow.Item("CAMPO_HIJO")
                    dNewRow.Item("ES_SUBCAMPO") = 1
                    dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
                    ds.Tables("TEMP").Rows.Add(dNewRow)
                Else
                    'No popup, los invisibles pero relacionados (ej: material-articulo) si q estaran en TEMP pero con el CAMPO_DEF a -1000
                    If ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_HIJO") & " AND CAMPO_DEF=-1000").Length = 1 Then
                        dNewRow = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_HIJO"))(0)
                        dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")

                        dRowsCcd = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString)
                        For Each Row As DataRow In dRowsCcd
                            Row.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
                        Next
                    End If
                End If
            Next
            If dsInvisibles.Tables(1).Rows.Count = 0 Then 'No hay lineas, MontarFormularioSubmit falla no crea linea 0 para campos ocultos No popup
                For Each dRow In dsInvisibles.Tables(4).Rows
                    If ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_HIJO")).Length = 0 Then
                        dNewRow = ds.Tables("TEMP").NewRow
                        dNewRow.Item("CAMPO") = dRow.Item("CAMPO_HIJO")
                        dNewRow.Item("ES_SUBCAMPO") = 1
                        dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
                        Try
                            ds.Tables("TEMP").Rows.Add(dNewRow)
                        Catch ex As Exception
                        End Try

                        dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                        dNewRow.Item("LINEA") = 0
                        dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                        dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

                        dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
                        dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
                        dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
                        Try
                            ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                        Catch ex As Exception
                        End Try
                    Else
                        'No popup, los invisibles pero relacionados (ej: material-articulo) si q estaran en TEMP pero con el CAMPO_DEF a -1000
                        If ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_HIJO") & " AND CAMPO_DEF=-1000").Length = 1 Then
                            dNewRow = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_HIJO"))(0)
                            dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")

                            dRowsCcd = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString)
                            For Each Row As DataRow In dRowsCcd
                                Row.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
                            Next
                        End If
                    End If
                Next
            End If
            'METEMOS LOS adjuntos DE CAMPOS QUE ESTUVIERAN OCULTOS
            For Each dRow In dsInvisibles.Tables(5).Rows
                If ds.Tables("TEMPADJUN").Select("CAMPO=" + dRow.Item("CAMPO").ToString + " AND ID=" + dRow.Item("ID").ToString).Length = 0 Then
                    dNewRow = ds.Tables("TEMPADJUN").NewRow
                    dNewRow.Item("CAMPO") = dRow.Item("CAMPO")
                    dNewRow.Item("ID") = dRow.Item("ID")
                    dNewRow.Item("TIPO") = 1
                    dNewRow.Item("CAMPO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
                    Try
                        ds.Tables("TEMPADJUN").Rows.Add(dNewRow)
                    Catch ex As Exception
                    End Try
                End If
            Next
            Dim iLinea As Integer
            For Each dRow In dsInvisibles.Tables(1).Rows
                If dRow.Item("VISIBLE") = 0 OrElse dRow.Item("ESCRITURA") = 0 Then 'en este datatable vienen los visibles y los invisibles. Solo se deben a�adir los datos de los invisibles
                    ' Este IF comprueba que este campo invisible pertenece a un desglose con alg�n campo visible
                    If UBound(dsInvisibles.Tables(1).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND VISIBLE = 1")) > 0 Then
                        'Si hay alg�n campo visible en este desglose habr� que obtener que nuevo n�mero de l�nea tiene ahora

                        dRows = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)
                        If dRows.Length > 0 Then 'si fuera igual a 0 indicar�a que se ha borrado esa l�nea y por tanto no hay que guardarlo
                            iLinea = dRows(0).Item("LINEA")
                            'comprobamos que no exista ya que los desgloses de popup si que se cargan aunque est� invisible
                            If ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString).Length = 0 Then
                                Dim cont = 0
                                If DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Material OrElse DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Unidad Then
                                    'En el caso de que sea el campo sea la estructura de MATERIAL o la UNIDAD hay que comprobar que si el articulo tambi�n est� oculto que cargue el material o la unidad.
                                    Dim sql As String
                                    sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND VISIBLE=1 AND ESCRITURA=1 AND (TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.CodArticulo & " OR TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.DenArticulo & ")"
                                    cont = dsInvisibles.Tables(1).Select(sql).Length
                                End If

                                If (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Material AndAlso DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Unidad) _
                                OrElse (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Material And cont = 0) _
                                OrElse (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Unidad And cont = 0) Then

                                    dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                                    dNewRow.Item("LINEA") = iLinea
                                    dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                    dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                                    dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                                    dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                                    dNewRow.Item("VALOR_FEC") = dRow.Item("VALOR_FEC")
                                    dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                                    dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                                    dNewRow.Item("CAMBIO") = dRow.Item("CAMBIO")
                                    dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
                                    dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
                                    dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")

                                    ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)

                                    dRowsAdjun = dsInvisibles.Tables(2).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + iLinea.ToString)
                                    For Each Adjun As DataRow In dRowsAdjun
                                        dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                                        dNewRow.Item("LINEA") = iLinea
                                        dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                        dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

                                        dNewRow.Item("ID") = Adjun.Item("ADJUN")
                                        dNewRow.Item("TIPO") = 1
                                        dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
                                        dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
                                        dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
                                        Try
                                            ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                                        Catch ex As Exception
                                        End Try
                                    Next
                                End If
                            Else
                                Dim sql, sql2, sql3 As String
                                sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND VISIBLE = 0)"
                                sql = sql + " OR (TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND VISIBLE = 1))"
                                sql2 = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & "OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & ") AND VISIBLE = 1)"
                                sql3 = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND VISIBLE = 1)"
                                sql3 = sql3 + " OR (TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND VISIBLE = 1))"

                                If ((DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo) And (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Centro) And (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Material)) _
                                Or ((dsInvisibles.Tables(1).Select(sql).Length < 2) And (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo)) _
                                Or ((dsInvisibles.Tables(1).Select(sql3).Length = 0) And (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Material)) _
                                Or ((DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Centro) And (dsInvisibles.Tables(1).Select(sql2).Length = 0)) Then
                                    'If (dRow.Item("TIPO_CAMPO_GS") <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo) 
                                    'Or ((dsInvisibles.Tables(1).Select(sql).Length < 2) And (dRow.Item("TIPO_CAMPO_GS") = TiposDeDatos.TipoCampoGS.NuevoCodArticulo)) Then
                                    dNewRow = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)(0)

                                    If IsDBNull(dNewRow.Item("VALOR_TEXT")) Then
                                        dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                                    ElseIf dNewRow.Item("VALOR_TEXT") = "nulo" Then
                                        dNewRow.Item("VALOR_TEXT") = System.DBNull.Value
                                    Else
                                        dNewRow.Item("VALOR_TEXT") = dNewRow.Item("VALOR_TEXT")
                                    End If
                                    dNewRow.Item("VALOR_NUM") = IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))
                                    dNewRow.Item("VALOR_FEC") = IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))
                                    dNewRow.Item("VALOR_BOOL") = IIf(IsDBNull(dNewRow.Item("VALOR_BOOL")), dRow.Item("VALOR_BOOL"), dNewRow.Item("VALOR_BOOL"))
                                    dNewRow.Item("CAMBIO") = IIf(IsDBNull(dNewRow.Item("CAMBIO")), dRow.Item("CAMBIO"), dNewRow.Item("CAMBIO"))

                                    dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
                                    dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
                                    dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
                                End If
                            End If
                        End If
                    Else ' al no haber campos visibles de este desglose habr� que a�adirlo tal y como viene

                        dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                        dNewRow.Item("LINEA") = dRow.Item("LINEA")
                        dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                        dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                        dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                        dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                        dNewRow.Item("VALOR_FEC") = dRow.Item("VALOR_FEC")
                        dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                        dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                        dNewRow.Item("CAMBIO") = dRow.Item("CAMBIO")
                        dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
                        dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
                        dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
                        Try
                            ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                        Catch ex As Exception
                        End Try


                        dRowsAdjun = dsInvisibles.Tables(2).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + dRow.Item("LINEA").ToString)
                        For Each Adjun As DataRow In dRowsAdjun
                            dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                            dNewRow.Item("LINEA") = dRow.Item("LINEA")
                            dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                            dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

                            dNewRow.Item("ID") = Adjun.Item("ADJUN")
                            dNewRow.Item("TIPO") = 1
                            dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
                            dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
                            dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")
                            Try
                                ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                            Catch ex As Exception
                            End Try
                        Next
                    End If
                End If
            Next
            For Each dRow In dsInvisibles.Tables(1).Rows
                'Si estaba entre los invisibles  el PADRE, ed, el DESGLOSE que mas da PM_CONF_CUMP_BLOQUE
                'del hijo, seguro que no esta por GenerarDataSet y habra que meterlo
                dRows = dsInvisibles.Tables(0).Select("ID=" + dRow.Item("CAMPO_PADRE").ToString)
                bPadreNoVisibleHijoVa = (dRows.Length > 0)
                ''
                If bPadreNoVisibleHijoVa Then
                    dRows = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)
                    If dRows.Length > 0 Then 'si fuera igual a 0 indicar�a que se ha borrado esa l�nea y por tanto no hay que guardarlo
                        iLinea = dRows(0).Item("LINEA")
                        'comprobamos que no exista ya que los desgloses de popup si que se cargan aunque est� invisible
                        If ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString).Length = 0 Then

                            dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                            dNewRow.Item("LINEA") = iLinea
                            dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                            dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                            dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                            If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                                dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                            End If
                            Try
                                dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                                If dRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                                dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                                dNewRow.Item("CAMBIO") = dRow.Item("CAMBIO")
                            Catch ex As Exception
                            End Try
                            dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")

                            dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
                            dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
                            dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")

                            Try
                                ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                            Catch ex As Exception
                            End Try

                            If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
                                dRowsAdjun = dsInvisibles.Tables(2).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + iLinea.ToString)
                                For Each Adjun As DataRow In dRowsAdjun
                                    dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                                    dNewRow.Item("LINEA") = iLinea
                                    dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                    dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

                                    dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
                                    dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
                                    dNewRow.Item("CAMPO_HIJO_DEF") = Adjun.Item("COPIA_CAMPO_DEF")

                                    dNewRow.Item("ID") = Adjun.Item("ADJUN")
                                    dNewRow.Item("TIPO") = 1
                                    Try
                                        ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                                    Catch ex As Exception
                                    End Try
                                Next
                            End If
                        Else
                            'Para que no se cargue CodArticulo =119 en el caso de que el NuevoCodArt este OCULTO y la DENOMINACION visible
                            Dim sql, sql2 As String
                            sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString
                            sql = sql + " AND CAMPO_HIJO = " + dRow.Item("CAMPO_HIJO").ToString
                            sql = sql + " AND LINEA = " + iLinea.ToString
                            dRows = dsInvisibles.Tables(1).Select(sql)

                            sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND VISIBLE = 0)"
                            sql = sql + " OR (TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND VISIBLE = 1))"

                            sql2 = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & "OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & ") AND VISIBLE = 1)"

                            If dRows.Length > 0 Then
                                If ((DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo) And (DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Centro)) _
                                Or ((dsInvisibles.Tables(1).Select(sql).Length < 2) And (DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo)) _
                                Or ((DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Centro) And (dsInvisibles.Tables(1).Select(sql2).Length = 0)) Then
                                    dNewRow = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)(0)

                                    'dNewRow.Item("VALOR_TEXT") tendr� el valor "nulo" cuando en el desglose se muestre solamente la denominacion el articulo no sea generico
                                    'y introducimos una nueva denominacion
                                    If IsDBNull(dNewRow.Item("VALOR_TEXT")) Then
                                        dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                                    ElseIf dNewRow.Item("VALOR_TEXT") = "nulo" Then
                                        dNewRow.Item("VALOR_TEXT") = System.DBNull.Value
                                    Else
                                        dNewRow.Item("VALOR_TEXT") = dNewRow.Item("VALOR_TEXT")
                                    End If
                                    If (IsDBNull(dNewRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                                        dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                                    End If
                                    Try
                                        dNewRow.Item("VALOR_NUM") = IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))
                                        If dRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))), DBNullToSomething(IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))))
                                    Catch ex As Exception
                                    End Try
                                    Try
                                        dNewRow.Item("VALOR_FEC") = IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))
                                        dNewRow.Item("VALOR_BOOL") = IIf(IsDBNull(dNewRow.Item("VALOR_BOOL")), dRow.Item("VALOR_BOOL"), dNewRow.Item("VALOR_BOOL"))
                                        dNewRow.Item("CAMBIO") = IIf(IsDBNull(dNewRow.Item("CAMBIO")), dRow.Item("CAMBIO"), dNewRow.Item("CAMBIO"))
                                    Catch ex As Exception
                                    End Try

                                    dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
                                    dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
                                    dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")

                                End If
                            End If
                        End If
                    Else ' al no haber campos visibles de este desglose habr� que a�adirlo tal y como viene
                        dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
                        If dRowsCcd.Length = 0 Then
                            If bNuevoWorkflow Then
                                dRowsCcd = dsInvisibles.Tables(6).Select("CAMPO_PADRE=" & dRow.Item("CAMPO_PADRE"))
                            ElseIf dsInvisibles.Tables.Count >= 14 Then 'QA: Depende de si la NC tiene datos de acciones o no
                                '14-> 6 fijas + 7 (Optional) Nc Acc + 1 fija ESTA SELECT
                                '15-> 6 fijas + 7 (Optional) Nc Acc + 1 fija ESTA SELECT + 1 (Optional) desglose no popup invisible sin lineas 
                                dRowsCcd = dsInvisibles.Tables(13).Select("CAMPO_PADRE=" & dRow.Item("CAMPO_PADRE"))
                            Else  'QA: Depende de si la NC tiene datos de acciones o no
                                '7-> 6 fijas + 1 fija ESTA SELECT
                                '8-> 6 fijas + 1 fija ESTA SELECT + 1 (Optional) desglose no popup invisible sin lineas 
                                dRowsCcd = dsInvisibles.Tables(6).Select("CAMPO_PADRE=" & dRow.Item("CAMPO_PADRE"))
                            End If

                            dNewRow = ds.Tables("TEMP").NewRow
                            dNewRow.Item("CAMPO") = dRowsCcd(0)("CAMPO_PADRE")
                            dNewRow.Item("ES_SUBCAMPO") = 0
                            dNewRow.Item("SUBTIPO") = dRowsCcd(0)("SUBTIPO_PADRE")
                            dNewRow.Item("TIPOGS") = dRowsCcd(0)("TIPO_CAMPO_GS_PADRE")
                            dNewRow.Item("CAMPO_DEF") = dRowsCcd(0)("COPIA_CAMPO_DEF_PADRE")
                            Try
                                ds.Tables("TEMP").Rows.Add(dNewRow)
                            Catch ex As Exception
                            End Try

                            dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
                        End If

                        dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                        dNewRow.Item("LINEA") = dRow.Item("LINEA")
                        dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                        dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                        dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                        If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                            dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                        End If

                        Try
                            dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                            If dRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                            dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                            dNewRow.Item("CAMBIO") = dRow.Item("CAMBIO")
                        Catch ex As Exception

                        End Try

                        dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")

                        dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
                        dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("COPIA_CAMPO_DEF")

                        Try
                            ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                        Catch ex As Exception
                        End Try

                        If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
                            dRowsAdjun = dsInvisibles.Tables(2).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + dRow.Item("LINEA").ToString)
                            For Each Adjun As DataRow In dRowsAdjun
                                dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                                dNewRow.Item("LINEA") = dRow.Item("LINEA")
                                dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

                                dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_PADRE"))
                                dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
                                dNewRow.Item("CAMPO_HIJO_DEF") = Adjun.Item("COPIA_CAMPO_DEF")

                                dNewRow.Item("ID") = Adjun.Item("ADJUN")
                                dNewRow.Item("TIPO") = 1
                                Try
                                    ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                                Catch ex As Exception
                                End Try
                            Next
                        End If
                    End If
                End If
            Next

            dRows = ds.Tables("TEMPDESGLOSE").Select("LINEA_OLD IS NULL")
            Dim lCampoPadre As Long
            Dim dDefaultRow As DataRow
            Dim lMaxLine As Long

            lMaxLine = 0

            Dim auxDataTable As New DataTable("GRUPOS")
            auxDataTable.Columns.Add("CAMPO_PADRE", System.Type.GetType("System.Int32"))
            auxDataTable.Columns.Add("LINEAS", System.Type.GetType("System.Int32"))
            Dim auxRow As DataRow

            For Each dRow In dRows
                If auxDataTable.Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString).Length > 0 Then
                    auxRow = auxDataTable.Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString)(0)
                Else
                    auxRow = auxDataTable.NewRow
                    auxRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                    auxRow.Item("LINEAS") = 0
                End If

                If DBNullToSomething(auxRow.Item("LINEAS")) < DBNullToSomething(dRow.Item("LINEA")) Then
                    auxRow.Item("LINEAS") = dRow.Item("LINEA")

                End If
                If auxRow.RowState = DataRowState.Detached Then
                    auxDataTable.Rows.Add(auxRow)
                End If
            Next

            Dim i As Long
            For Each auxRow In auxDataTable.Rows
                For i = 1 To auxRow.Item("LINEAS")
                    If ds.Tables("TEMPDESGLOSE").Select("LINEA=" + i.ToString()).Length > 0 Then
                        If dsInvisibles.Tables(1).Select("LINEA=" + i.ToString()).Length > 0 Then
                            For Each dDefaultRow In dsInvisibles.Tables(1).Select("CAMPO_PADRE = " + auxRow.Item("CAMPO_PADRE").ToString + " AND VISIBLE = 0 AND LINEA = " + i.ToString)
                                If ds.Tables("TEMPDESGLOSE").Select("LINEA=" + i.ToString + " AND CAMPO_PADRE = " + dDefaultRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO = " + dDefaultRow.Item("CAMPO_HIJO").ToString).Length = 0 Then
                                    dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                                    dNewRow.Item("LINEA") = i
                                    dNewRow.Item("CAMPO_PADRE") = dDefaultRow.Item("CAMPO_PADRE").ToString
                                    dNewRow.Item("CAMPO_HIJO") = dDefaultRow.Item("CAMPO_HIJO")
                                    dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("VALOR_TEXT")
                                    If (IsDBNull(dDefaultRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dDefaultRow.Item("LISTA_TEXT"))) Then
                                        dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("LISTA_TEXT")
                                    End If
                                    dNewRow.Item("VALOR_NUM") = dDefaultRow.Item("VALOR_NUM")
                                    dNewRow.Item("VALOR_FEC") = dDefaultRow.Item("VALOR_FEC")
                                    dNewRow.Item("VALOR_BOOL") = dDefaultRow.Item("VALOR_BOOL")
                                    dNewRow.Item("CAMBIO") = dDefaultRow.Item("CAMBIO")
                                    dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dDefaultRow.Item("CAMPO_PADRE"))
                                    dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
                                    dNewRow.Item("CAMPO_HIJO_DEF") = dDefaultRow.Item("COPIA_CAMPO_DEF")
                                    ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                                Else
                                    dNewRow = ds.Tables("TEMPDESGLOSE").Select("LINEA=" + i.ToString + " AND CAMPO_PADRE = " + dDefaultRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO = " + dDefaultRow.Item("CAMPO_HIJO").ToString)(0)
                                    If IsDBNull(dNewRow.Item("LINEA_OLD")) Then
                                        dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("VALOR_TEXT")
                                        If (IsDBNull(dDefaultRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dDefaultRow.Item("LISTA_TEXT"))) Then
                                            dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("LISTA_TEXT")
                                        End If
                                        dNewRow.Item("VALOR_NUM") = dDefaultRow.Item("VALOR_NUM")
                                        dNewRow.Item("VALOR_FEC") = dDefaultRow.Item("VALOR_FEC")
                                        dNewRow.Item("VALOR_BOOL") = dDefaultRow.Item("VALOR_BOOL")
                                        dNewRow.Item("CAMBIO") = dDefaultRow.Item("CAMBIO")
                                        dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dDefaultRow.Item("CAMPO_PADRE"))
                                        dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
                                        dNewRow.Item("CAMPO_HIJO_DEF") = dDefaultRow.Item("COPIA_CAMPO_DEF")
                                    End If
                                End If
                            Next
                        Else
                            For Each dDefaultRow In dsInvisibles.Tables(4).Rows
                                If auxRow.Item("CAMPO_PADRE") = dDefaultRow.Item("CAMPO_PADRE") Then
                                    If ds.Tables("TEMPDESGLOSE").Select("LINEA IS NULL AND CAMPO_HIJO = " + dDefaultRow.Item("CAMPO_HIJO").ToString).Length = 0 Then
                                        dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                                        dNewRow.Item("LINEA") = i
                                        dNewRow.Item("CAMPO_PADRE") = auxRow.Item("CAMPO_PADRE")
                                        dNewRow.Item("CAMPO_HIJO") = dDefaultRow.Item("CAMPO_HIJO")
                                        dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("VALOR_TEXT")
                                        If (IsDBNull(dDefaultRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dDefaultRow.Item("LISTA_TEXT"))) Then
                                            dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("LISTA_TEXT")
                                        End If
                                        dNewRow.Item("VALOR_NUM") = dDefaultRow.Item("VALOR_NUM")
                                        If dDefaultRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoFecha Then dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dDefaultRow.Item("VALOR_NUM")), DBNullToSomething(dDefaultRow.Item("VALOR_FEC")))
                                        dNewRow.Item("VALOR_BOOL") = dDefaultRow.Item("VALOR_BOOL")
                                        dNewRow.Item("CAMBIO") = dDefaultRow.Item("CAMBIO")

                                        dRowsCcd = ds.Tables("TEMP").Select("CAMPO=" & dDefaultRow.Item("CAMPO_PADRE"))
                                        dNewRow.Item("CAMPO_PADRE_DEF") = dRowsCcd(0).Item("CAMPO_DEF")
                                        dNewRow.Item("CAMPO_HIJO_DEF") = dDefaultRow.Item("COPIA_CAMPO_DEF")

                                        Try
                                            ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                                        Catch ex As Exception
                                        End Try
                                    End If
                                End If
                            Next
                        End If
                    End If
                Next
            Next
            'se escapa el caso de desglose no popup invisible sin lineas en la versi�n
            If (dsInvisibles.Tables.Count = 9) OrElse (dsInvisibles.Tables.Count = 15) Then

                Dim IdTblsOcultas As Integer = 8
                If dsInvisibles.Tables.Count = 15 Then IdTblsOcultas = 14

                'METEMOS LOS CAMPOS DE DESGLOSE QUE ESTUVIERAN OCULTOS. El motivo de estar ocultos es q el desglose esta oculto y en ningun  momento ha tenido lineas
                For Each dRow In dsInvisibles.Tables(IdTblsOcultas).Rows
                    If ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_HIJO")).Length = 0 Then
                        dNewRow = ds.Tables("TEMP").NewRow
                        dNewRow.Item("CAMPO") = dRow.Item("CAMPO_HIJO")
                        dNewRow.Item("ES_SUBCAMPO") = 1
                        dNewRow.Item("CAMPO_DEF") = dRow.Item("CAMPO_HIJO_DEF")
                        Try
                            ds.Tables("TEMP").Rows.Add(dNewRow)
                        Catch ex As Exception
                        End Try

                        dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                        dNewRow.Item("LINEA") = 0
                        dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                        dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                        dNewRow.Item("CAMPO_PADRE_DEF") = dRow.Item("CAMPO_PADRE_DEF")
                        dNewRow.Item("CAMPO_HIJO_DEF") = dRow.Item("CAMPO_HIJO_DEF")
                        Try
                            ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                        Catch ex As Exception
                        End Try
                    End If
                Next
            End If

            'Vincular
            If Not (ds.Tables("VINCULAR") Is Nothing) AndAlso (bNuevoWorkflow) Then
                'Qa tiene la tabla pero no usa.
                For Each dRow In dsInvisibles.Tables(7).Rows
                    dNewRow = ds.Tables("VINCULAR").NewRow

                    dNewRow.Item("DESGLOSEVINCULADA") = dRow.Item("DESGLOSEVINCULADA")
                    dNewRow.Item("LINEAVINCULADA") = dRow.Item("LINEAVINCULADA")
                    dNewRow.Item("INSTANCIAORIGEN") = dRow.Item("INSTANCIAORIGEN")
                    dNewRow.Item("DESGLOSEORIGEN") = dRow.Item("DESGLOSEORIGEN")
                    dNewRow.Item("LINEAORIGEN") = dRow.Item("LINEAORIGEN")

                    ds.Tables("VINCULAR").Rows.Add(dNewRow)
                Next
            End If

            Return ds
        End Function
        ''' <summary>
        ''' Cargar los estados de una No Conformidad
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: script\_common\desglose.ascx    script\_common\guardarinstancia.aspx    script\noconformidad\impexp.aspx   
        ''' PMPortalNotificador\Notificar.vb; Tiempo m�ximo: 0,2</remarks>
        Public Function CargarTiposEstadoAccion(ByVal lCiaComp As Long) As DataSet
            Authenticate()

            Dim data As DataSet
            If mRemottingServer Then
                data = DBServer.Instancia_CargarTiposEstadoAccion(lCiaComp, mlID, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Instancia_CargarTiposEstadoAccion(lCiaComp, mlID)
            End If

            Return data
        End Function
        '''<summary>Comprobar que la acci�n sea de rechazo o anulaci�n</summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="idAccion">Id de la acci�n</param>
        ''' <returns>Devuelve true si la acci�n es de tipo rechazo o anulaci�n</returns>
        Public Function AccionSinControlDisponible(ByVal lCiaComp As Long, ByVal idAccion As Integer) As Boolean
            Authenticate()

            If mRemottingServer Then
                Return DBServer.Instancia_AccionSinControlDisponible(lCiaComp, idAccion, msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.Instancia_AccionSinControlDisponible(lCiaComp, idAccion)
            End If
        End Function

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
            moDen = New MultiIdioma
        End Sub
        ''' <summary>
        ''' Obtiene el hist�rico de una solicitud devolviendo por todas las etapas en las que ha estado.
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <remarks>Llamada desde: script\solicitudes\comentariosSolicitud.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Sub CargarHistoricoEstados(ByVal lCiaComp As Long, Optional ByVal sIdi As String = Nothing)
            Authenticate()

            If mRemottingServer Then
                m_oHistoricoEst = DBServer.Instancia_CargarHistoricoEstados(lCiaComp, mlID, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                m_oHistoricoEst = DBServer.Instancia_CargarHistoricoEstados(lCiaComp, mlID, sIdi)
            End If

        End Sub
        ''' <summary>
        ''' Obtener el Comentario del Estado
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="ID">ID de instancia_est</param>
        ''' <returns>Comentario</returns>
        ''' <remarks>Llamada desde: script\solicitudes\comentestado.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Function CargarComentarioEstado(ByVal lCiaComp As Long, ByVal ID As Long) As String
            Authenticate()

            If mRemottingServer Then
                CargarComentarioEstado = DBServer.Instancia_DevolverComentarioEstado(lCiaComp, ID, msSesionId, msIPDir, msPersistID)
            Else
                CargarComentarioEstado = DBServer.Instancia_DevolverComentarioEstado(lCiaComp, ID)
            End If

        End Function
        ''' <summary>
        ''' Obtendr� la etapa actual en la que se encuentra la instancia bas�ndose en la persona o proveedor que se pasa por par�metro:
        ''' 1- Puede que sea el rol de un bloque activo
        ''' 2- Puede que sea un rol de tipo lista autoasignable
        ''' 3- Puede que no sea un rol, sino un usuario/proveedor al que se le ha trasladado la solicitud, o que est� a la espera de que se la devuelvan
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <remarks>Llamada desde: script\_common\guardarinstancia.aspx    script\contratos\GestionContrato.aspx   script\solicitudes\detalleSolicitud.aspx
        ''' script\solicitudes\devolucion.aspx   solicitudes\seguimientoSolicitudes.aspx; Tiempo m�ximo: 0,2</remarks>
        Public Sub DevolverEtapaActual(ByVal lCiaComp As Long, ByVal sIdi As String, ByVal sProve As String)
            Authenticate()
            Dim data As DataSet
            Dim row As DataRow
            Dim i As Integer
            Dim bCarga As Boolean

            If mRemottingServer Then
                data = DBServer.Instancia_DevolverEtapaActual(lCiaComp, mlID, sIdi, sProve, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Instancia_DevolverEtapaActual(lCiaComp, mlID, sIdi, sProve)
            End If

            If Not data.Tables(0).Rows.Count = 0 Then
                For i = 0 To data.Tables(0).Rows.Count - 1
                    bCarga = True
                    'Hay un caso que no funciona bien. Tengo el proveedor definido como rol en la 
                    'etapa y aparte se le ha trasladado la solicitud.
                    'En este caso salen 2 lineas y la valida es la de trasladada.
                    If data.Tables(0).Rows.Count > 1 Then
                        If IsDBNull(data.Tables(0).Rows(i).Item("CUMPLIMENTADOR")) Then
                            bCarga = False
                        End If
                    End If
                    If bCarga Then
                        msDenEtapaActual = DBNullToSomething(data.Tables(0).Rows(i).Item("BLOQUE_DEN"))
                        miEtapaActual = DBNullToSomething(data.Tables(0).Rows(i).Item("BLOQUE"))
                        miRolActual = DBNullToSomething(data.Tables(0).Rows(i).Item("ROL"))
                        miInstanciaBloque = DBNullToSomething(data.Tables(0).Rows(i).Item("ID"))
                        mbInstanciaBloqueTrasladada = SQLBinaryToBoolean(data.Tables(0).Rows(i).Item("TRASLADADA"))
                        m_sDestinatarioEst = DBNullToSomething(data.Tables(0).Rows(i).Item("CUMPLIMENTADOR"))
                        miInstanciaBloqueBloq = data.Tables(0).Rows(i).Item("BLOQ")
                        m_bVer_flujo = SQLBinaryToBoolean(data.Tables(0).Rows(i).Item("VER_FLUJO"))
                    End If
                Next
            End If

            data = Nothing

        End Sub
        ''' <summary>
        ''' Obtiene los enlaces a siguientes etapas a partir de la accion
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="lAccion">Accion</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: script\_common\guardarinstancia.aspx  script\solicitudes\RealizarAccion.aspx; Tiempo m�ximo: 0,2</remarks>
        Public Function DevolverSiguientesEtapas(ByVal lCiaComp As Long, ByVal lAccion As Integer, ByVal sProve As String, ByVal sIdi As String,
                                                 ByVal lBloque As Long, ByVal lRol As Long,
                                                 Optional ByVal dsFactura As DataSet = Nothing) As DataSet
            Authenticate()

            If mRemottingServer Then
                moData = DBServer.Instancia_ObtenerSiguientesEtapas(lCiaComp, mlID, lAccion, sProve, sIdi, lBloque, lRol, dsFactura, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.Instancia_ObtenerSiguientesEtapas(lCiaComp, mlID, lAccion, sProve, sIdi, lBloque, lRol, dsFactura)
            End If

            Return moData
        End Function
        ''' <summary>Obtiene los enlaces a siguientes etapas a partir de la accion</summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="idAccion">Accion</param>
        ''' <param name="codProve">C�digo del proveedor</param>
        ''' <param name="codIdioma">Idioma</param>
        ''' <param name="idBloque">Bloque</param>
        ''' <param name="idRol">Rol</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: script\_common\guardarinstancia.aspx  script\solicitudes\RealizarAccion.aspx</remarks>
        Public Function DevolverSiguientesEtapasForm(ByVal lCiaComp As Long, ByVal idAccion As Long, ByVal codProve As String, ByVal codIdioma As String, ByVal ds As DataSet, ByVal idBloque As Long, ByVal idRol As Long, ByRef sRolPorWebService As String) As DataSet
            Authenticate()

            If mRemottingServer Then
                moData = DBServer.Instancia_ObtenerSiguientesEtapasForm(lCiaComp, mlID, idAccion, codProve, codIdioma, ds, idBloque, idRol, sRolPorWebService, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.Instancia_ObtenerSiguientesEtapasForm(lCiaComp, mlID, idAccion, codProve, codIdioma, ds, idBloque, idRol, sRolPorWebService)
            End If

            Return moData
        End Function
        ''' <summary>
        ''' Devolver datos para realizar un Traslado
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <remarks>Llamada desde: script\_common\guardarinstancia.aspx  script\contratos\GestionContratoTrasladada.aspx   script\solicitudes\detalleSolicitud.aspx
        ''' script\solicitudes\devolucion.aspx; Tiempo m�ximo: 0,2</remarks>
        Public Sub DevolverDatosTraslado(ByVal lCiacomp As Long, ByVal sProveGS As String)
            Authenticate()
            Dim data As DataSet
            Dim i As Integer

            If mRemottingServer Then
                data = DBServer.Instancia_DevolverDatosTraslado(lCiacomp, mlID, sProveGS, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Instancia_DevolverDatosTraslado(lCiacomp, mlID, sProveGS)
            End If

            If Not data.Tables(0).Rows.Count = 0 Then
                For i = 0 To data.Tables(0).Rows.Count - 1
                    m_sNomPersonaEst = DBNullToSomething(data.Tables(0).Rows(i).Item("NOM_PERSONA_EST"))
                    m_sPersonaEst = DBNullToSomething(data.Tables(0).Rows(i).Item("PERSONA_EST"))
                    m_dFechaEstado = DBNullToSomething(data.Tables(0).Rows(i).Item("FECHA_EST"))
                    m_dFecLimiteTraslado = DBNullToSomething(data.Tables(0).Rows(i).Item("FEC_LIMITE"))
                Next
            End If

            data = Nothing

        End Sub
        ''' <summary>
        ''' Carga las etapas por las que pasar�
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="lAccion">Accion</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: script\solicitudes\accionRealizadaOK.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Function DevolverEtapasRealizadas(ByVal lCiaComp As Long, ByVal lAccion As Integer, ByVal sBloqueDestino As String, ByVal sIdi As String) As DataSet
            Authenticate()

            If mRemottingServer Then
                moData = DBServer.Instancia_DevolverEtapasRealizadas(lCiaComp, lAccion, sBloqueDestino, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.Instancia_DevolverEtapasRealizadas(lCiaComp, lAccion, sBloqueDestino, sIdi)
            End If

            Return moData
        End Function
        ''' <summary>
        ''' Obtiene las precondiciones de una acci�n de una instancia
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="lIdAccion">Accion</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: script\_common\guardarinstancia.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Function ObtenerPrecondicionesAccion(ByVal lCiaComp As Long, ByVal lIdAccion As Integer, ByVal sIdi As String) As DataSet
            Authenticate()

            Dim data As DataSet
            If mRemottingServer Then
                data = DBServer.Instancia_LoadPrecondicionesAccion(lCiaComp, mlID, lIdAccion, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Instancia_LoadPrecondicionesAccion(lCiaComp, mlID, lIdAccion, sIdi)
            End If

            Return data
        End Function
        ''' <summary>
        ''' Devolver procesos EP relacionados a la instancia
        ''' </summary>
        ''' <remarks>Llamada desde: script\_common\campos.ascx   script\_common\desglose.ascx    script\_common\guardarinstancia.aspx; Tiempo m�ximo: 0,2</remarks>
        Public Sub DevolverProcesosRelacionados()
            Authenticate()

            If mRemottingServer Then
                moProcesos = DBServer.Instancia_CargarProcesos(mlID, msSesionId, msIPDir, msPersistID)
            Else
                moProcesos = DBServer.Instancia_CargarProcesos(mlID)
            End If

        End Sub
        ''' <summary>
        ''' Obtener Importe Adjudicado
        ''' </summary>
        ''' <returns>Importe Adjudicado</returns>
        ''' <remarks>Llamada desde: script\_common\campos.ascx   script\_common\desglose.ascx    script\_common\guardarinstancia.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Function ObtenerImporteAdjudicado() As Double
            Authenticate()

            Dim dImporte As Double
            If mRemottingServer Then
                dImporte = DBServer.Instancia_ObtenerImporteAdjudicado(mlID, msSesionId, msIPDir, msPersistID)
            Else
                dImporte = DBServer.Instancia_ObtenerImporteAdjudicado(mlID)
            End If

            Return dImporte
        End Function
        Public Sub CargarCumplimentacionFactura(ByVal lCiaComp As Long, Optional ByVal sUsuario As String = Nothing, Optional ByVal sProve As String = Nothing)
            Authenticate()
            m_bDetalleEditable = DBServer.Instancia_CargarCumplimentacionFactura(lCiaComp, mlID, m_lVersion, sUsuario, sProve)
        End Sub
        ''' <summary>
        ''' Obtener Titulo de la instancia
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <returns>Titulo de la instancia</returns>
        ''' <remarks>Llamada desde: script\_common\campos.ascx ; Tiempo m�ximo: 0,2</remarks>
        Public Function DevolverTitulo(ByVal lCiaComp As Long, ByVal sIdi As String) As String

            Authenticate()
            Dim data As DataSet

            If mRemottingServer Then
                data = DBServer.Instancia_DevolverTitulo(lCiaComp, sIdi, mlID, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Instancia_DevolverTitulo(lCiaComp, sIdi, mlID)
            End If
            Try


                If data.Tables.Count > 0 Then
                    If data.Tables(0).Rows.Count > 0 Then
                        Return data.Tables(0).Rows(0).Item(0)
                    End If
                End If
            Catch ex As Exception
                Return ""
            End Try


        End Function
        ''' <summary>
        ''' Devolver Importe Solicitudes Vinculadas
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <returns>Importe Solicitudes Vinculadas</returns>
        ''' <remarks>Llamada desde: script\_common\campos.ascx ; Tiempo m�ximo: 0,2</remarks>
        Public Function DevolverImporteSolicitudesVinculadas(ByVal lCiaComp As Long) As Long

            Authenticate()
            Dim data As DataSet

            If mRemottingServer Then
                data = DBServer.Instancia_DevolverImporteSolicitudesVinculadas(lCiaComp, mlID, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Instancia_DevolverImporteSolicitudesVinculadas(lCiaComp, mlID)
            End If
            Try


                If data.Tables.Count > 0 Then
                    If data.Tables(0).Rows.Count > 0 Then
                        Return data.Tables(0).Rows(0).Item(1)
                    End If
                End If
            Catch ex As Exception
                Return 0
            End Try
        End Function
        ''' <summary>
        ''' carga un campo de la instancia
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="lCampo">Campo</param>
        ''' <param name="bGuardando">Si llamada es desde guardarInstancia: true. Eoc: false. Motivo lCampo es guardarInstancia->CopiaCampo.Id o Eoc->FormCampo.Id</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: script\_common\campos.ascx   script\_common\desglose.ascx.; Tiempo m�ximo: 0,2</remarks>
        Public Function cargarCampo(ByVal lCiaComp As Long, ByVal lCampo As Long, Optional ByVal bGuardando As Boolean = False) As DataSet
            Authenticate()

            If mRemottingServer Then
                Return DBServer.cargarCampo(lCiaComp, lCampo, IIf(bGuardando, 0, mlID), msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.cargarCampo(lCiaComp, lCampo, IIf(bGuardando, 0, mlID))
            End If

        End Function
        ''' <summary>
        ''' Obtener Valor del campo especificado en la linea especificada
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="lCampo">Id de campo</param>
        ''' <param name="nLinea">Linea de desglose</param>
        ''' <returns>Valor del campo especificado en la linea especificada</returns>
        ''' <remarks>Llamada desde: script\_common\desglose.ascx ; Tiempo m�ximo: 0,2</remarks>
        Public Function cargarCampoDesglose(ByVal lCiaComp As Long, ByVal lCampo As Long, ByVal nLinea As Long, ByVal FSPMServer As PMPortalServer.Root) As Object
            Authenticate()
            Dim ds As DataSet
            Dim oValorCampo As Object
            Dim sPresup() As String
            Dim lIdPres As Integer

            If mRemottingServer Then
                ds = DBServer.cargarCampoDesglose(lCiaComp, lCampo, nLinea, m_lVersion, msSesionId, msIPDir, msPersistID)
            Else
                ds = DBServer.cargarCampoDesglose(lCiaComp, lCampo, nLinea, m_lVersion)
            End If

            Select Case ds.Tables(0).Rows(0).Item("SUBTIPO")
                Case TiposDeDatos.TipoGeneral.TipoNumerico
                    Return DBNullToSomething(ds.Tables(0).Rows(0).Item("VALOR_NUM"))
                Case TiposDeDatos.TipoGeneral.TipoFecha
                    Return DBNullToSomething(ds.Tables(0).Rows(0).Item("VALOR_FEC"))
                Case TiposDeDatos.TipoGeneral.TipoBoolean
                    Return DBNullToSomething(ds.Tables(0).Rows(0).Item("VALOR_BOOL"))
                Case Else
                    If DBNullToSomething(ds.Tables(0).Rows(0).Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.PRES1 Then
                        sPresup = Split(ds.Tables(0).Rows(0).Item("VALOR_TEXT"), "_")
                        lIdPres = CInt(sPresup(1))

                        Dim oPres1 As Fullstep.PMPortalServer.PresProyectosNivel1 = FSPMServer.Get_PresProyectosNivel1
                        Select Case CInt(sPresup(0))
                            Case 1
                                oPres1.LoadData(lCiaComp, lIdPres)
                            Case 2
                                oPres1.LoadData(lCiaComp, Nothing, lIdPres)
                            Case 3
                                oPres1.LoadData(lCiaComp, Nothing, Nothing, lIdPres)
                            Case 4
                                oPres1.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPres)
                        End Select
                        oValorCampo = oPres1.Data.Tables(0).Rows(0).Item("ANYO") + " - " + oPres1.Data.Tables(0).Rows(0).Item("PRES" & sPresup(0))
                        oPres1 = Nothing
                    ElseIf DBNullToSomething(ds.Tables(0).Rows(0).Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Pres2 Then
                        sPresup = Split(ds.Tables(0).Rows(0).Item("VALOR_TEXT"), "_")
                        lIdPres = CInt(sPresup(1))

                        Dim oPres2 As Fullstep.PMPortalServer.PresContablesNivel1 = FSPMServer.Get_PresContablesNivel1
                        Select Case CInt(sPresup(0))
                            Case 1
                                oPres2.LoadData(lCiaComp, lIdPres)
                            Case 2
                                oPres2.LoadData(lCiaComp, Nothing, lIdPres)
                            Case 3
                                oPres2.LoadData(lCiaComp, Nothing, Nothing, lIdPres)
                            Case 4
                                oPres2.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPres)
                        End Select
                        oValorCampo = oPres2.Data.Tables(0).Rows(0).Item("ANYO") + " - " + oPres2.Data.Tables(0).Rows(0).Item("PRES" & sPresup(0))
                        oPres2 = Nothing
                    ElseIf DBNullToSomething(ds.Tables(0).Rows(0).Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Pres3 Then
                        sPresup = Split(ds.Tables(0).Rows(0).Item("VALOR_TEXT"), "_")
                        lIdPres = CInt(sPresup(1))

                        Dim oPres3 As Fullstep.PMPortalServer.PresConceptos3Nivel1 = FSPMServer.Get_PresConceptos3Nivel1
                        Select Case CInt(sPresup(0))
                            Case 1
                                oPres3.LoadData(lCiaComp, lIdPres)
                            Case 2
                                oPres3.LoadData(lCiaComp, Nothing, lIdPres)
                            Case 3
                                oPres3.LoadData(lCiaComp, Nothing, Nothing, lIdPres)
                            Case 4
                                oPres3.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPres)
                        End Select
                        oValorCampo = oPres3.Data.Tables(0).Rows(0).Item("PRES" & sPresup(0))
                        oPres3 = Nothing
                    ElseIf DBNullToSomething(ds.Tables(0).Rows(0).Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Pres4 Then
                        sPresup = Split(ds.Tables(0).Rows(0).Item("VALOR_TEXT"), "_")
                        lIdPres = CInt(sPresup(1))

                        Dim oPres4 As Fullstep.PMPortalServer.PresConceptos4Nivel1 = FSPMServer.Get_PresConceptos4Nivel1
                        Select Case CInt(sPresup(0))
                            Case 1
                                oPres4.LoadData(lCiaComp, lIdPres)
                            Case 2
                                oPres4.LoadData(lCiaComp, Nothing, lIdPres)
                            Case 3
                                oPres4.LoadData(lCiaComp, Nothing, Nothing, lIdPres)
                            Case 4
                                oPres4.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPres)
                        End Select
                        oValorCampo = oPres4.Data.Tables(0).Rows(0).Item("PRES" & sPresup(0))
                        oPres4 = Nothing
                    Else
                        oValorCampo = DBNullToSomething(ds.Tables(0).Rows(0).Item("VALOR_TEXT"))
                    End If
                    Return oValorCampo
            End Select
            ds = Nothing
        End Function
        ''' <summary>
        ''' Obtener el valor de INSTANCIA.EN_PROCESO
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <returns>valor de INSTANCIA.EN_PROCESO</returns>
        ''' <remarks>Llamada desde: script\_common\ComprobarEnProceso.aspx  script\certificados\certificado.aspx      script\certificados\CertificadosVisor.aspx
        ''' script\noconformidad\noconformidad.aspx; Tiempo m�ximo: 0,2</remarks>
        Public Function ComprobarEnProceso(ByVal lCiaComp As Long) As Integer
            Authenticate()
            If mRemottingServer Then
                ComprobarEnProceso = DBServer.Instancia_ComprobarEnProceso(lCiaComp, mlID, msSesionId, msIPDir, msPersistID)
            Else
                ComprobarEnProceso = DBServer.Instancia_ComprobarEnProceso(lCiaComp, mlID)
            End If
        End Function
        ''' <summary>
        ''' Indica si algun desglose de la instancia tiene vinculaciones a otro/s desglose/s
        ''' </summary>
        ''' <returns>si algun desglose dela instancia tiene vinculaciones o no </returns>
        ''' <remarks>Llamada desde: NWGestionInstancia.aspx/form_load; Tiempo maximo: 0,1</remarks>
        Public Function ExisteVinculaciones(ByVal lCiaComp As Long) As Boolean
            Authenticate()
            Return DBServer.Instancia_ExisteVinculaciones(lCiaComp, mlID)
        End Function

        ''' <summary>Inserta los datos generales de la instancia.</summary>
        ''' <remarks>Llamada desde: guardarinstancia.aspx.vb; Tiempo m�ximo: 1 sg.</remarks>
        Public Sub Create_Prev(ByVal lCiaComp As Long)
            Authenticate()

            If mRemottingServer Then
                DBServer.Instancia_Create_Prev(lCiaComp, Solicitud.ID, Solicitud.Formulario.Id, CLng(Solicitud.Workflow.ToString), Solicitud.PedidoDirecto, PeticionarioProve, PeticionarioProveContacto, Moneda, mlID, msSesionId, msIPDir, msPersistID)
            Else
                DBServer.Instancia_Create_Prev(lCiaComp, Solicitud.ID, Solicitud.Formulario.Id, CLng(Solicitud.Workflow.ToString), Solicitud.PedidoDirecto, PeticionarioProve, PeticionarioProveContacto, Moneda, mlID)
            End If
        End Sub

        ''' <summary>
        ''' Actualiza el campo EN_PROCESO de la tabla INSTANCIA
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="iEnProceso">valor para en_proceso</param>
        ''' <param name="iBloque">Bloque</param>
        ''' <remarks>Llamada desde: script\_common\guardarinstancia.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Sub Actualizar_En_proceso(ByVal lCiaComp As Long, ByVal iEnProceso As Integer, Optional ByVal iBloque As Integer = Nothing)
            Authenticate()
            If mRemottingServer Then
                DBServer.Instancia_Actualizar_En_Proceso(lCiaComp, mlID, iEnProceso, iBloque, msSesionId, msIPDir, msPersistID)
            Else
                DBServer.Instancia_Actualizar_En_Proceso(lCiaComp, mlID, iEnProceso, iBloque)
            End If
        End Sub
        ''' <summary>
        ''' Nos devuelva de las tablas INSTANCIA y VERSION_INSTANCIA el valor de los campos EN_PROCESO, 
        ''' NUM_VERSION,  TIPO de la �ltima versi�n de VERSION_INSTANCIA y si el Certificado sigue siendo 
        ''' el activo para el proveedor (Si existe en la tabla PROVE_CERTIF)y si el certificado sigue estando PUBLICADO
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="lCertificado">Id de certificado</param>
        ''' <remarks>Llamada desde: controlarVersionQA.aspx; Tiempo m�ximo: 0,1</remarks>
        Public Sub ComprobarGrabacionCertif(ByVal lCiaComp As Long, ByVal lCertificado As Long)
            Authenticate()
            Dim data As DataSet
            Dim i As Integer

            If mRemottingServer Then
                data = DBServer.Instancia_ComprobarGrabacionCertif(lCiaComp, mlID, lCertificado, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Instancia_ComprobarGrabacionCertif(lCiaComp, mlID, lCertificado)
            End If

            For i = 0 To data.Tables(0).Rows.Count - 1 'EN REALIDAD SOLO DEVOLVER� UNA FILA
                m_lVersion = DBNullToSomething(data.Tables(0).Rows(i).Item("NUM_VERSION"))

                m_iEnProceso = DBNullToSomething(data.Tables(0).Rows(i).Item("EN_PROCESO"))

                m_iTipoVersion = DBNullToSomething(data.Tables(0).Rows(i).Item("VITIPO"))

                m_bCertifActivo = SQLBinaryToBoolean(data.Tables(0).Rows(i).Item("HAY_CERTIF"))

                m_bCertifPublicado = SQLBinaryToBoolean(data.Tables(0).Rows(i).Item("PUBLICADA"))
            Next
        End Sub
        ''' <summary>
        ''' Nos devuelva de las tablas INSTANCIA y VERSION_INSTANCIA el valor de los campos EN_PROCESO, 
        ''' NUM_VERSION,  TIPO de la �ltima versi�n de VERSION_INSTANCIA 
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <remarks>Llamada desde: controlarVersionQA.aspx; Tiempo m�ximo: 0,1</remarks>
        Public Sub ComprobarGrabacionNC(ByVal lCiaComp As Long)
            Authenticate()
            Dim data As DataSet
            Dim i As Integer

            If mRemottingServer Then
                data = DBServer.Instancia_ComprobarGrabacionNC(lCiaComp, mlID, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Instancia_ComprobarGrabacionNC(lCiaComp, mlID)
            End If

            For i = 0 To data.Tables(0).Rows.Count - 1 'EN REALIDAD SOLO DEVOLVER� UNA FILA
                m_lVersion = DBNullToSomething(data.Tables(0).Rows(i).Item("NUM_VERSION"))

                m_iEnProceso = DBNullToSomething(data.Tables(0).Rows(i).Item("EN_PROCESO"))

                m_iTipoVersion = DBNullToSomething(data.Tables(0).Rows(i).Item("VITIPO"))
            Next
        End Sub
        ''' <summary>
        ''' Los calculados en un desglose hacen uso de un campo fuera del desglose, esta funci�n lo obtiene aunque este no visible
        ''' </summary>
        ''' <param name="lCiaComp">codigo de compa�ia</param>
        ''' <param name="lIdCampo">id del copia_campo q es el desglose, es decir, campo_padre de copia_linea_desglose</param>        
        ''' <param name="sUsuario">Usuario de la empresa de proveedor q esta dando de alta</param>
        ''' <param name="sProve">Proveedor</param>   
        ''' <returns>Dataset con el/los campos (no de desglose) q usa el desglose con id lIdCampo</returns>
        ''' <remarks>Llamada desde: recalcularimportes.aspx; Tiempo m�ximo: 0,1</remarks>
        Public Function LoadDesglosePadreVisible(ByVal lCiaComp As Long, ByVal lIdCampo As Integer, Optional ByVal sUsuario As String = Nothing, Optional ByVal sProve As String = Nothing) As DataSet
            Authenticate()

            If mRemottingServer Then
                moData = DBServer.Instancia_LoadDesglosePadreVisible(lCiaComp, mlID, lIdCampo, m_lVersion, Nothing, sUsuario, sProve, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.Instancia_LoadDesglosePadreVisible(lCiaComp, mlID, lIdCampo, m_lVersion, Nothing, sUsuario, sProve)
            End If

            Return moData

        End Function
        ''' <summary>
        ''' Funci�n que lanza las validaciones de la integraci�n 
        ''' </summary>
        ''' <returns>Una posible excepciion que pueda dar</returns>
        ''' <remarks>Llamada desde: GuardarInstancia.aspx\GenerarDataSetyHacerComprobaciones(),GuardarInstancia.aspx\GenerarConWorkflow
        ''' Tiempo m�ximo 1 sec</remarks>
        Public Function ControlMapperFactura(ByVal lCiaComp As Long, ByVal lIdEmpresa As Long, ByVal sCodProve As String, ByVal lIdFactura As Long, ByVal lIdAccion As Long) As Exception
            Authenticate()

            Dim oError As Exception
            Dim iNumError As Short
            Dim ValidacionesIntegracion As New ValidacionesIntegracion(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)

            If Not ValidacionesIntegracion.EvalMapperFacturas(iNumError, lCiaComp, lIdEmpresa, sCodProve, lIdFactura, lIdAccion) Then
                oError = New Exception()
                oError.Source = CStr(iNumError)
                Return oError
            End If

            ControlMapperFactura = Nothing
        End Function
		Public Function ControlMapper(ByVal lCiaComp As Long, ByVal FSPMServer As PMPortalServer.Root, ByVal ds As DataSet, ByVal idioma As String, ByVal AccionRol As Long,
									  ByVal sBloquesDestino As String, ByVal bAlta As Boolean, Optional peticionario As String = Nothing, Optional peticionarioProve As String = "",
									  Optional peticionarioProveCon As Integer = 0) As Object
			Authenticate()

			Dim iNumError As Short
			Dim strError As String = String.Empty
			Dim ValidacionesIntegracion As New ValidacionesIntegracion(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)

			Dim iTipoValidacionIntegracion As TipoValidacionIntegracion
			iTipoValidacionIntegracion = ValidacionesIntegracion.ControlMapper(lCiaComp, FSPMServer, ds, idioma, iNumError, strError, IIf(bAlta, 0, mlID), Solicitud.ID, AccionRol,
														 sBloquesDestino, peticionario, peticionarioProve, peticionarioProveCon)
			If Not iTipoValidacionIntegracion = TipoValidacionIntegracion.SinMensaje Then
				Return New With {.tipoValidacionIntegracion = iTipoValidacionIntegracion, .iNumError = iNumError, .strError = strError}
			End If

			ControlMapper = Nothing
		End Function

		''' <summary>
		''' Devuelve la denominaci�n de la etapa en curso de la solicitud
		''' </summary>
		''' <param name="sIdioma">Codigo del idioma del usuario</param>
		''' <returns>Un entero que devolvera el estado de la aprobacion</returns>
		''' <remarks>
		''' Llamada desde: Ninguna
		''' tiempo m�ximo: 0,4 seg</remarks>
		Public Function CargarEstapaActual(ByVal lCiaComp As Long, ByVal sIdioma As String, Optional bLitEstado As Boolean = False) As String
            Authenticate()
            CargarEstapaActual = DBServer.Instancia_CargarEstapaActual(lCiaComp, mlID, sIdioma, bLitEstado)
        End Function
        Public Function ComprobarLineasAsociadas(ByVal lCiaComp As Long, ByVal idDesglose As Long, ByVal numLinea As Integer) As Boolean
            Authenticate()
            Return DBServer.Instancia_ComprobarLineasAsociadas(lCiaComp, idDesglose, numLinea)
        End Function
        ''' <summary>
        ''' Actualiza el tiempo de procesamiento de las solicitudes
        ''' </summary>
        ''' <param name="lCiaComp">Id de la Cia</param>
        ''' <param name="lIDTiempoProc">Id (campo identity). Si no tiene nada hay que hacer una insert, si no, una update.</param>
        ''' <param name="sCodUsuario">Cod del usuario</param>
        ''' <param name="iFecha">Fecha que hay que actualizar (inicio, fin,..)</param>
        ''' <remarks>Llamada desde: guardarinstancia</remarks>
        Public Sub ActualizarTiempoProcesamiento(ByVal lCiaComp As Long, Optional ByRef lIDTiempoProc As Long = Nothing, Optional ByVal sCodUsuario As String = Nothing,
                                                 Optional ByVal lBloque As Long = 0, Optional ByVal iFecha As Short = 0)
            Authenticate()
            If mRemottingServer Then
                DBServer.Instancia_ActualizarTiempoProcesamiento(lCiaComp, lIDTiempoProc, mlID, sCodUsuario, lBloque, iFecha, msSesionId, msIPDir, msPersistID)
            Else
                DBServer.Instancia_ActualizarTiempoProcesamiento(lCiaComp, lIDTiempoProc, mlID, sCodUsuario, lBloque, iFecha)
            End If
        End Sub
        ''' <summary>Devuelve los �tems de una instancia</summary>
        ''' <param name="lCiaComp">Id de la Cia</param>       
        ''' <returns>lista de objetos con el c�digo de los �tems</returns>
        ''' <remarks>llamada desde: seguimientoSolicitudes.aspx</remarks>
        Public Function DevolverItems(ByVal lCiaComp As Integer, ByVal bSoloTexto As Boolean) As Object
            Dim oItems As New List(Of Object)
            Dim Texto As String

            Authenticate()
            Dim dtItems As DataTable = DBServer.Instancia_DevolverItems(lCiaComp, mlID)

            If Not dtItems Is Nothing AndAlso dtItems.Rows.Count > 0 Then

                For Each drItem As DataRow In dtItems.Rows
                    If bSoloTexto Then
                        If Not (Texto = "") Then
                            Texto = Texto & "; "
                        End If
                        Texto = Texto & drItem("ART4")
                    Else
                        oItems.Add({New With {.Codigo = drItem("ART4")}})
                    End If
                Next
            End If

            If bSoloTexto Then
                Return Texto
            Else
                Return oItems
            End If
        End Function
        Public Function CargaComboMapper(ByVal lCiaComp As Integer, ByVal bSave As Boolean, ByVal campo As Integer, ByVal idioma As String) As DataSet
            Authenticate()

            If mRemottingServer Then
                Return DBServer.DatosMapperBD(lCiaComp, bSave, campo, idioma, msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.DatosMapperBD(lCiaComp, bSave, campo, idioma)
            End If
        End Function
        ''' <summary>
        ''' funcion que devuelve los items de un proceso de solicitud
        ''' </summary>
        ''' <returns>Un DataSet con los datos de los items de un proceso de solicitud</returns>
        ''' <remarks>
        ''' Llamada desde: PmWeb/EliminarSolicitud/Page_load
        ''' Tiempo m�ximo: 0,6 seg</remarks>
        Public Function ItemsProcesoSolicitud(ByVal lCiaComp As Integer) As DataSet
            Authenticate()
            moData = DBServer.Instancia_ItemsProcesoSolicitud(lCiaComp, mlID)
            Return moData
        End Function
        ''' <summary>
        ''' Procedimiento que elimina una instancia
        ''' </summary>
        ''' <param name="sUsu">Codigo de usuario que realiza la accion</param>
        ''' <remarks>
        ''' LLamada desde: PmWeb/Eliminarsolicitud/page_load
        ''' Tiempo m�ximo: 0,5 seg</remarks>
        Public Sub Eliminar(ByVal lCiaComp As Integer, Optional ByVal sUsu As String = Nothing)
            Authenticate()
            DBServer.EliminarInstancia(lCiaComp, mlID)
        End Sub

        ''' <summary>
        ''' Control Vulnerabilidad ImpExp
        ''' </summary>
        ''' <param name="lCiaComp">Compania</param>
        ''' <param name="IdQa">Id certif/No Conf</param>
        ''' <param name="Proveedor">Proveedor</param>
        ''' <param name="Instancia">Instancia</param>
        ''' <param name="EsCertificado">1->certif/0->No Conf</param>
        Public Function CtrlVulnerabilidad_ImpExp(ByVal lCiaComp As Long, ByVal IdQa As Long, ByVal Proveedor As String, ByVal Instancia As Long, ByVal EsCertificado As Boolean) As Boolean
            Authenticate()

            If mRemottingServer Then
                Return DBServer.Instancia_CtrlVulnerabilidad_ImpExp(lCiaComp, IdQa, Proveedor, Instancia, EsCertificado, msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.Instancia_CtrlVulnerabilidad_ImpExp(lCiaComp, IdQa, Proveedor, Instancia, EsCertificado)
            End If
        End Function
    End Class
End Namespace