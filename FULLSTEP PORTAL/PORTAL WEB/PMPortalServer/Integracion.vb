﻿Imports Fullstep.FSNLibrary
Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Integracion
        Inherits Fullstep.PMPortalServer.Security

        ''' <summary>
        ''' Método para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">código de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petición</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo máximo: 0</remarks>
        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

        ''' <summary>
        ''' Función que nos dice si la integración esta activa y en el sentido que le indicamos
        ''' </summary>
        ''' <param name="TablaIntegracion">Parametro opcional con la entidad a analizar</param>
        ''' <returns>Booleano</returns>
        ''' <remarks>Tiempo maximo 0 sec</remarks>
        Public Function HayIntegracion(ByVal lCiaComp As Long, ByVal TablaIntegracion As Integer, Optional ByVal Sentido As Short = SentidoIntegracion.SinSentido) As Boolean
            Authenticate()
            HayIntegracion = DBServer.HayIntegracion(lCiaComp, TablaIntegracion, Sentido)
        End Function
        Protected Function hayIntegracionTablaErp(ByVal lCiaComp As Long, ByVal sErp As String, ByVal tablaIntegracion As Integer, ByVal sentido1 As Integer, Optional ByVal sentido2 As Integer = SentidoIntegracion.SinSentido) As Boolean
            Authenticate()

            If mRemottingServer Then
                Return DBServer.hayIntegracionTablaErp(lCiaComp, sErp, tablaIntegracion, sentido1, sentido2, msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.hayIntegracionTablaErp(lCiaComp, sErp, tablaIntegracion, sentido1, sentido2)
            End If
        End Function
        Public Function hayIntegracionPM(ByVal lCiaComp As Long, ByVal erp As String) As Boolean
            Authenticate()
            Return hayIntegracionTablaErp(erp, TablasIntegracion.PM, SentidoIntegracion.Salida, SentidoIntegracion.EntradaYSalida)
        End Function
        Public Function hayIntegracionArticulos(ByVal lCiaComp As Long, ByVal erp As String) As Boolean
            Authenticate()
            Return hayIntegracionTablaErp(erp, TablasIntegracion.art4, SentidoIntegracion.Salida, SentidoIntegracion.EntradaYSalida)
        End Function
        ''' <summary>
        ''' Obtiene el id de ERP de una organización de compras
        ''' </summary>
        ''' <param name="sOrgCompras"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getERPFromOrgCompras(ByVal lCiaComp As Long, ByVal sOrgCompras As String) As String
            Authenticate()

            If mRemottingServer Then
                Return DBServer.getERPFromOrgCompras(lCiaComp, sOrgCompras, msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.getERPFromOrgCompras(lCiaComp, sOrgCompras)
            End If
        End Function
        Public Function estaIntegradoArticulo(ByVal lCiaComp As Long, ByVal codArticulo As String, ByVal sErp As String) As Boolean
            Authenticate()

            If mRemottingServer Then
                Return DBServer.hayIntegracionArticuloErp(lCiaComp, codArticulo, sErp, msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.hayIntegracionArticuloErp(lCiaComp, codArticulo, sErp)
            End If
        End Function
        ''' <summary>Obtiene las credenciales (nombre de usuario y contraseña) de acceso al ERP del cliente para poder establecer una conexón para la integración ví­a WCF.</summary>
        ''' <param name="iEntidadIntegracion">La entidad a integrar.</param>
        ''' <param name="iErp">El ERP de la entidad a integrar.</param>
        ''' <param name="iServiceBindingType">OUT: El tipo del binding del servicio (0, 1 y 3 = BasicHttpSecurityMode, todo lo demÃ¡s es SecurityMode).</param>
        ''' <param name="iServiceSecurityMode">OUT: El modo de seguridad del servicio (0 = None, 1 = Transport, 2 = Message, 3 = TransportWithMessageCredential, 4 = TransportCredentialOnly).</param>
        ''' <param name="iClientCredentialType">OUT: El tipo de credenciales del cliente (0 = None, 1 = Basic, 2 = Digest, 3 = Ntlm, 4 = Windows, 5 = Certificate).</param>
        ''' <param name="iProxyCredentialType">OUT: El tipo de credenciales del proxy (0 = None, 1 = Basic, 2 = Digest, 3 = Ntlm, 4 = Windows, 5 = Certificate).</param>
        ''' <param name="sNombre">OUT: El nombre de usuario.</param>
        ''' <param name="sContrasenya">OUT: La contraseÃ±a desencriptada del usuario.</param>
        ''' <remarks>Llamada desde: ValidacionesIntegracion/RemoteValidacionesWCF</remarks>
        Public Sub ObtenerCredencialesWCF(ByVal lCiaComp As Long, ByVal iEntidadIntegracion As Integer, ByVal iErp As Integer, ByRef iServiceBindingType As Integer, ByRef iServiceSecurityMode As Integer,
                                      ByRef iClientCredentialType As Integer, ByRef iProxyCredentialType As Integer, ByRef sNombre As String, ByRef sContrasenya As String)
            Authenticate()

            Dim dr As DataSet
            Dim sFecContrasenya As String

            If mRemottingServer Then
                dr = DBServer.INT_ObtenerDatosConexionFSIS(lCiaComp, iErp, msSesionId, msIPDir, msPersistID)
            Else
                dr = DBServer.INT_ObtenerDatosConexionFSIS(lCiaComp, iErp)
            End If
            If dr.Tables(0).Rows.Count = 0 Then
                iServiceBindingType = 0
                iServiceSecurityMode = 0
                iClientCredentialType = 0
                iProxyCredentialType = 0
            Else
                With dr.Tables(0).Rows(0)
                    iServiceBindingType = IIf(IsDBNull(.Item("SERVICEBINDINGTYPE")), 0, .Item("SERVICEBINDINGTYPE").ToString)
                    iServiceSecurityMode = IIf(IsDBNull(.Item("SERVICESECURITYMODE")), 0, .Item("SERVICESECURITYMODE").ToString)
                    iClientCredentialType = IIf(IsDBNull(.Item("SERVICECLIENTCREDENTIALTYPE")), 0, .Item("SERVICECLIENTCREDENTIALTYPE").ToString)
                    iProxyCredentialType = IIf(IsDBNull(.Item("SERVICEPROXYCREDENTIALTYPE")), 0, .Item("SERVICEPROXYCREDENTIALTYPE").ToString)
                End With
            End If
            dr.Reset()

            If mRemottingServer Then
                dr = DBServer.INT_ObtenerCredencialesWCF(lCiaComp, iEntidadIntegracion, iErp, msSesionId, msIPDir, msPersistID)
            Else
                dr = DBServer.INT_ObtenerCredencialesWCF(lCiaComp, iEntidadIntegracion, iErp)
            End If
            If dr.Tables(0).Rows.Count = 0 Then
                sNombre = ""
                sContrasenya = ""
            Else
                With dr.Tables(0).Rows(0)
                    sNombre = IIf(IsDBNull(.Item("WCF_ORIG_USU")), "", .Item("WCF_ORIG_USU").ToString)
                    sContrasenya = IIf(IsDBNull(.Item("WCF_ORIG_PWD")), "", .Item("WCF_ORIG_PWD").ToString)
                    sFecContrasenya = IIf(IsDBNull(.Item("WCF_ORIG_FECPWD")), "", .Item("WCF_ORIG_FECPWD").ToString)
                End With
                If sFecContrasenya = "" Then
                    sContrasenya = ""
                Else
                    sContrasenya = Encrypter.EncryptInt(sContrasenya, sNombre, False, Encrypter.TipoDeUsuario.Persona, sFecContrasenya).ToString
                End If
            End If
        End Sub
        ''' <summary>Obtiene la ruta padre del servicio WCF de IntegraciÃƒÂ³n.</summary>
        ''' <returns>La ruta padre del servicio WCF.</returns>
        ''' <remarks>Llamada desde: ValidacionesIntegracion/RemoteValidacionesWCF</remarks>
        Public Function ObtenerRutaInstalacionFSIS(ByVal lCiaComp As Long) As String
            Authenticate()

            Dim dr As DataSet
            If mRemottingServer Then
                dr = DBServer.INT_ObtenerRutaInstalacionFSIS(lCiaComp, msSesionId, msIPDir, msPersistID)
            Else
                dr = DBServer.INT_ObtenerRutaInstalacionFSIS(lCiaComp)
            End If
            If IsNothing(dr.Tables(0)) Then
                Return ""
            Else
                Return DBNullToStr(dr.Tables(0).Rows(0).Item("RUTAXBAP"))
            End If
        End Function

    End Class
End Namespace
