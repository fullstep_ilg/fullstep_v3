Namespace Fullstep.PMPortalServer
    Public Class TiposDeDatos
        Public Enum TipoGeneral
            SinTipo = 0
            TipoString = 1
            TipoNumerico = 2
            TipoFecha = 3
            TipoBoolean = 4
            TipoTextoCorto = 5
            TipoTextoMedio = 6
            TipoTextoLargo = 7
            TipoArchivo = 8
            TipoDesglose = 9
            TipoCheckBox = 10
            TipoPresBajaLog = 11
            TipoDepBajaLog = 12
            TipoDesgloseOblig = 13
            TipoDesgloseObligNoPopup = 14
            TipoEditor = 15

        End Enum

        Public Enum ModulosIdiomas
            Menu = 1
            GestionTraslado = 2
            Desglose = 3
            Devolucion = 4
            DevolucionOK = 5
            DetalleCampoArchivo = 6
            Certificado = 7
            EnvioCertificadoOk = 8
            EnvioCertificadoKO = 9
            DesgloseControl = 10
            DetallePersona = 11
            AttachFile = 12
            DownloadFile = 13
            BusquedaArticulos = 14
            ValidarArticulo = 15
            Solicitud = 16
            NoConformidad = 17
            Presupuestos = 18
            EnvioNoConformidadOk = 19
            EnvioNoConformidadKO = 20
            VariablesCalidad = 21
            SeguimientoSolicitudes = 22
            ComentariosSolic = 23
            ComentEstado = 24
            flowDiagram = 25
            RealizarAccion = 26
            AccionRealizadaOK = 27
            CertifProveedores = 28
            Proveedores = 29
            Usuarios = 30
            AvisosPrecondiciones = 31
            NoConformidades = 32
            GuardarInstancia = 33
            DetalleDenominacionPresupuestos = 34
            UnidadesOrganizativas = 35
            EntregaPedidos = 36
            Paginador = 37
            DetallePedido = 38
            ImpExp = 44
            DetalleProveedor = 45
            DetalleEmpresa = 46

            MailProveedores = 97

            BusquedaNuevoCodArticulos = 102
            BusquedaSolicitudes = 104
            DetalleSeguimientoPadre = 105
            PopUpDesglose = 106

            TablaExterna = 116
            AyudaCampo = 117

            VisorCertificados = 118

            Otros = 101

            Activos = 124
            CentrosCoste = 125
            Partidas = 126

            VisorPedidosAceptaciones = 127
            BusquedaMateriales = 128

            Colaboracion = 130
            PanelInfo = 131
            EnvioMails = 132
            PerfilUsuarioCN = 133

            Adjuntos = 136
            FileStream = 137
            Editor = 138
            BusquedaEmpresas = 139

            DetalleFactura = 167
            FacturaPDF = 168
            OfertasHistoricas = 171
            OfertasPDF = 172

            VisorFacturasLCX = 173
            AltaFacturasLCX = 175
            VisorFacturasPPL = 176
            EstadosFacturaPPL = 177

            VisorSolicitudes = 178
            AltaFacturas = 179
            AltaSolicitudes = 180
            AltaSolicitud = 181
            Espera = 182

            PaginaError = 183
        End Enum

        Public Enum TipoCampoGS
            SinTipo = 0
            'Public Enum TipoCampoSC
            DescrBreve = 1
            DescrDetallada = 2
            Importe = 3
            Cantidad = 4
            FecNecesidad = 5
            IniSuministro = 6
            FinSuministro = 7
            ArchivoEspecific = 8
            PrecioUnitario = 9
            PrecioUnitarioAdj = 10
            ProveedorAdj = 11
            CantidadAdj = 12
            'End Enum

            'Public Enum TipoCampoCertificado
            NombreCertif = 20
            FecObtencion = 21
            Certificado = 23
            FecDespublicacion = 27
            'End Enum

            'Public Enum TipoCampoNoConformidad
            Titulo = 30
            Motivo = 31
            CausasYConclus = 32
            Peso = 33
            Acciones = 34
            Accion = 35
            Fec_inicio = 36
            Fec_cierre = 37
            Responsable = 38
            Observaciones = 39
            Documentación = 40


            EstadoNoConf = 42
            EstadoInternoNoConf = 43
            PiezasDefectuosas = 44
            ImporteRepercutido = 45
            Subtipo = 46
            'End Enum


            Proveedor = 100
            FormaPago = 101
            Moneda = 102
            Material = 103
            CodArticulo = 104
            Unidad = 105
            Desglose = 106
            Pais = 107
            Provincia = 108
            Dest = 109
            PRES1 = 110
            Pres2 = 111
            Pres3 = 112
            Pres4 = 113

            Contacto = 114

            Persona = 115
            ProveContacto = 116
            Rol = 117
            DenArticulo = 118
            NuevoCodArticulo = 119
            NumSolicitERP = 120
            UnidadOrganizativa = 121
            Departamento = 122
            OrganizacionCompras = 123
            Centro = 124
            Almacen = 125
            ListadosPersonalizados = 126
            ImporteSolicitudesVinculadas = 127
            RefSolicitud = 128

            CentroCoste = 129
            Partida = 130
            Activo = 131
            TipoPedido = 132
            InicioAbono = 139
            FinAbono = 140
            UnidadPedido = 142
            ProveedorERP = 143
            Comprador = 144
            Empresa = 149
            EstadoHomologacion = 150
            AnyoPartida = 151
        End Enum


        Public Structure LongitudesDeCodigos

            Dim giLongCodART As Integer
            Dim giLongCodCAL As Integer
            Dim giLongCodCOM As Integer
            Dim giLongCodDEP As Integer
            Dim giLongCodDEST As Integer
            Dim giLongCodEQP As Integer
            Dim giLongCodGMN1 As Integer
            Dim giLongCodGMN2 As Integer
            Dim giLongCodGMN3 As Integer
            Dim giLongCodGMN4 As Integer
            Dim giLongCodMON As Integer
            Dim giLongCodOFEEST As Integer
            Dim giLongCodPAG As Integer
            Dim giLongCodPAI As Integer
            Dim giLongCodPER As Integer
            Dim giLongCodPERF As Integer
            Dim giLongCodPRESCON1 As Integer
            Dim giLongCodPRESCON2 As Integer
            Dim giLongCodPRESCON3 As Integer
            Dim giLongCodPRESCON4 As Integer
            Dim giLongCodPRESCONCEP31 As Integer
            Dim giLongCodPRESCONCEP32 As Integer
            Dim giLongCodPRESCONCEP33 As Integer
            Dim giLongCodPRESCONCEP34 As Integer
            Dim giLongCodPRESCONCEP41 As Integer
            Dim giLongCodPRESCONCEP42 As Integer
            Dim giLongCodPRESCONCEP43 As Integer
            Dim giLongCodPRESCONCEP44 As Integer
            Dim giLongCodPRESPROY1 As Integer
            Dim giLongCodPRESPROY2 As Integer
            Dim giLongCodPRESPROY3 As Integer
            Dim giLongCodPRESPROY4 As Integer
            Dim giLongCodPROVE As Integer
            Dim giLongCodPROVI As Integer
            Dim giLongCodROL As Integer
            Dim giLongCodUNI As Integer
            Dim giLongCodUON1 As Integer
            Dim giLongCodUON2 As Integer
            Dim giLongCodUON3 As Integer
            Dim giLongCodUSU As Integer
            Dim giLongCodACT1 As Integer
            Dim giLongCodACT2 As Integer
            Dim giLongCodACT3 As Integer
            Dim giLongCodACT4 As Integer
            Dim giLongCodACT5 As Integer
            Dim giLongCia As Integer
            Dim giLongCodCAT1 As Integer
            Dim giLongCodCAT2 As Integer
            Dim giLongCodCAT3 As Integer
            Dim giLongCodCAT4 As Integer
            Dim giLongCodCAT5 As Integer
            Dim giLongCodGRUPOPROCE As Integer
            Dim giLongCodDENART As Integer

        End Structure

        Public Enum TipoFecha
            FechaAlta = 1
            UnDiaDespues = 2
            DosDiasDespues = 3
            TresDiasDespues = 4
            CuatroDiasDespues = 5
            CincoDiasDespues = 6
            SeisDiasDespues = 7
            UnaSemanaDespues = 8
            DosSemanaDespues = 9
            TresSemanaDespues = 10
            UnMesDespues = 11
            DosMesDespues = 12
            TresMesDespues = 13
            CuatroMesDespues = 14
            CincoMesDespues = 15
            SeisMesDespues = 16
            UnAnyoDespues = 17
        End Enum

		Public Enum TipoDeSolicitud
			Otros = 0
			SolicitudDeCompras = 1
			Certificado = 2
			NoConformidad = 3
			Abono = 4
			Contrato = 5
			Proyecto = 6
			AutoFactura = 7
			PedidoExpress = 8
			PedidoNegociado = 9
			SolicitudDePedidoCatalogo = 10
			SolicitudDePedidoContraAbierto = 11
			Encuesta = 12
			Factura = 13
			SolicitudQA = 14
		End Enum
	End Class
	Public Enum TipoAtributo
		CuentaContable = 886
	End Enum
	Public Enum TipoEstadoSolic
		Guardada = 0
		Enviada = 1
		Pendiente = 2
		Rechazada = 6
		Aprobada = 7
		Anulada = 8
		SCPendiente = 100
		SCAprobada = 101
		SCRechazada = 102
		SCAnulada = 103
		SCCerrada = 104
		CPublicable = 200
		NoConformidadEnviada = 300
		NoConformidadGuardada = 301

	End Enum
	Public Enum TipoAccionSolicitud
		Alta = 1
		Modificacion = 2
		Traslado = 3
		Devolucion = 4
		Aprobacion = 5
		Rechazo = 6
		Anulacion = 7
		Reemision = 8
		RechazoEficazRevisor = 9 'Para Controlar  el rechazo del revisor
		RechazoEficazRevisorAnterior = -9
		RechazoNoEficazRevisor = 10
		RechazoNoEficazRevisorAnterior = -10
		CierreNoConformidad = 11
		CierreAnteriorNoConformidad = -11
		AprobacionNoConformidad = 12 'Cuando aprueba el revisor
	End Enum
	Public Enum TipoEstadoOrdenEntrega
		PendienteDeAprobacion = 0
		DenegadoParcialAprob = 1
		EmitidoAlProveedor = 2
		AceptadoPorProveedor = 3
		EnCamino = 4
		EnRecepcion = 5
		RecibidoYCerrado = 6
		Anulado = 20
		RechazadoPorProveedor = 21
		DenegadoTotalAprobador = 22
	End Enum
	Public Enum TipoEstadoProceso

        sinitems = 1        'Sin validar y sin items
        ConItemsSinValidar = 2      'Con items y sin validar
        validado = 3        ' Validado y sin provedores asignados
        Conproveasignados = 4  ' Validado y con proveedores asignados pero sin validar la asignacion
        conasignacionvalida = 5 ' Con asignacion de proveedores validada
        conpeticiones = 6
        conofertas = 7
        ConObjetivosSinNotificar = 8
        ConObjetivosSinNotificarYPreadjudicado = 9
        PreadjYConObjNotificados = 10
        ParcialmenteCerrado = 11
        conadjudicaciones = 12
        ConAdjudicacionesNotificadas = 13
        Cerrado = 20

    End Enum
	Public Enum TipoCampoPredefinido
		Normal = 0
		CampoGS = 1  ' Solicitud de compras
		Atributo = 2  ' Atributo
		Calculado = 3
		Certificado = 4
		NoConformidad = 5
	End Enum
	Public Enum TipoEstadoNoConformidad
		Guardada = 0
		Abierta = 1
		CierrePosEnPlazo = 2
		CierrePosFueraPlazo = 3
		CierreNegativo = 4
		CierrePosSinRevisar = 5
		CierreNegSinRevisar = 6
	End Enum
	Public Enum TipoEstadoAcciones
		SinRevisar = 1
		Aprobada = 2
		Rechazada = 3
		FinalizadaSinRevisar = 4
		FinalizadaRevisada = 5
	End Enum
	Public Enum IdsFicticios
		EstadoActual = 1000
		Comentario = 1020
		EstadoInterno = 1010
		Grupo = 900
		FechaLimite = 1030
	End Enum
	Public Enum TipoRechazoAccion
		RechazoTemporal = 1
		RechazoDefinitivo = 2
		Anulacion = 3
	End Enum
	Public Enum TipoBloqueoEtapa
		SinBloqueo = 0
		BloqueoInicio = 1
		BloqueoSalida = 2
	End Enum
	Public Enum TipoAvisoBloqueo
		Bloqueo = 0
		Aviso = 1
		SinAsignar = -1
	End Enum
	Public Enum TipoValidacionIntegracion
		SinMensaje = 0
		Mensaje_Bloqueo = 1
		Mensaje_Aviso = 2
	End Enum
End Namespace