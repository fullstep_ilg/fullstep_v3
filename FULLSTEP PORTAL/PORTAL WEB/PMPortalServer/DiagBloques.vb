Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class DiagBloques
        Inherits Security
        Private moDiagBloques As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moDiagBloques
            End Get

        End Property
        ''' <summary>
        ''' Carga un diagrama bloque
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lID">Id de diagrama bloque</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <remarks>Llamada desde:  solicitudes\flowDiagram.aspx.vb  solicitudes\flowDiagramExport.aspx.vb; Tiempo m�ximo: 0,2</remarks>
        Public Sub LoadData(ByVal lCiaComp As Long, ByVal lID As Long, ByVal sIdi As String)
            Authenticate()
            If mRemottingServer Then
                moDiagBloques = DBServer.DiagBloques_Get(lCiaComp, lID, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                moDiagBloques = DBServer.DiagBloques_Get(lCiaComp, lID, sIdi)
            End If
        End Sub

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

    End Class
End Namespace