Imports System.Security.Principal
Imports System.Threading

Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class User
        Inherits Security
        Implements IPrincipal

        Private mUser As UserIdentity
        Private msCodCia As String
        Private msCod As String
        Private msPassword As String
        Private msNombre As String
        Private msCodProve As String
        Private msAprobadorActual As String

        Private msUON1 As String
        Private msUON2 As String
        Private msUON3 As String

        Private msThousanFmt As String
        Private msDecimalFmt As String
        Private miPrecisionFmt As Integer
        Private msDateFmt As String
        Private msMon As String
        Private miTipoEmail As Integer
        Private msIdioma As FSNLibrary.Idioma
        Private msRefCultural As String
        Private msEmail As String
        Private moNumberFormat As System.Globalization.NumberFormatInfo
        Private moDateFormat As System.Globalization.DateTimeFormatInfo
        Private mlIdCia As Long
        Private mlIdUsu As Long
        Private mlCiaComp As Long

        Private msDenCia As String
        Private msNIFCia As String
        Private msApellidos As String
        Private msTelefono As String
        Private msFax As String

        Public Property CodCia() As String
            Get
                Return msCodCia
            End Get
            Set(ByVal Value As String)
                msCodCia = Value
            End Set
        End Property
        Public Property DenCia() As String
            Get
                Return msDenCia
            End Get
            Set(ByVal Value As String)
                msDenCia = Value
            End Set
        End Property
        Public Property NIFCia() As String
            Get
                Return msNIFCia
            End Get
            Set(ByVal Value As String)
                msNIFCia = Value
            End Set
        End Property
        Public Property Cod() As String
            Get
                Cod = msCod
            End Get
            Set(ByVal Value As String)
                msCod = Value
            End Set
        End Property
        ''' <summary>
        ''' Para devolver el Password del usuario, para autentificar los webservice
        ''' </summary>
        ''' <returns>Password del usuario</returns>
        ''' <remarks>Llamada desde: pmportalweb/consultas.asmx; Tiempo maximo:0</remarks>
        Public Property Password() As String
            Get
                Password = msPassword
            End Get
            Set(ByVal Value As String)
                msPassword = Value
            End Set
        End Property
        Public Property Nombre() As String
            Get
                Return msNombre
            End Get
            Set(ByVal Value As String)
                msNombre = Value
            End Set
        End Property
        Public Property Apellidos() As String
            Get
                Return msApellidos
            End Get
            Set(ByVal Value As String)
                msApellidos = Value
            End Set
        End Property
        Public Property CodProve() As String
            Get
                Return msCodProve
            End Get
            Set(ByVal Value As String)
                msCodProve = Value
            End Set
        End Property
        Public Property AprobadorActual() As String
            Get
                Return msAprobadorActual
            End Get
            Set(ByVal Value As String)
                msAprobadorActual = Value
            End Set
        End Property
        Public Property UON1Aprobador() As String
            Get
                Return msUON1
            End Get
            Set(ByVal Value As String)
                msUON1 = Value
            End Set
        End Property
        Public Property UON2Aprobador() As String
            Get
                Return msUON2
            End Get
            Set(ByVal Value As String)
                msUON2 = Value
            End Set
        End Property
        Public Property UON3Aprobador() As String
            Get
                Return msUON3
            End Get
            Set(ByVal Value As String)
                msUON3 = Value
            End Set
        End Property
        Public Property ThousanFmt() As String
            Get
                If msThousanFmt = Nothing Then
                    msThousanFmt = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator
                    msDecimalFmt = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator
                End If
                ThousanFmt = msThousanFmt

            End Get
            Set(ByVal Value As String)
                msThousanFmt = Value

            End Set
        End Property
        Public Property DecimalFmt()
            Get
                If msDecimalFmt = Nothing Then
                    msThousanFmt = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator
                    msDecimalFmt = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator
                End If
                DecimalFmt = msDecimalFmt

            End Get
            Set(ByVal Value)
                msDecimalFmt = Value
            End Set
        End Property
        Public Property PrecisionFmt() As String
            Get
                PrecisionFmt = miPrecisionFmt
            End Get
            Set(ByVal Value As String)
                miPrecisionFmt = Value
            End Set
        End Property
        Public Property DateFmt() As String
            Get
                DateFmt = msDateFmt
            End Get
            Set(ByVal Value As String)
                msDateFmt = Value
            End Set
        End Property
        Public Property Mon() As String
            Get
                Mon = msMon
            End Get
            Set(ByVal Value As String)
                msMon = Value
            End Set
        End Property
        Public Property TipoEmail() As Integer
            Get
                TipoEmail = miTipoEmail

            End Get
            Set(ByVal Value As Integer)
                miTipoEmail = Value
            End Set
        End Property
        Public Property Idioma() As String
            Get
                Idioma = msIdioma
            End Get
            Set(ByVal Value As String)
                msIdioma = Value
            End Set
        End Property

        Public Property RefCultural() As String
            Get
                Return Me.msRefCultural
            End Get
            Set(ByVal value As String)
                msRefCultural = value
            End Set
        End Property
        Public Property Telefono() As String
            Get
                Telefono = msTelefono
            End Get
            Set(ByVal Value As String)
                msTelefono = Value
            End Set
        End Property
        Public Property Email() As String
            Get
                Email = msEmail
            End Get
            Set(ByVal Value As String)
                msEmail = Value
            End Set
        End Property
        Public Property Fax() As String
            Get
                Fax = msFax
            End Get
            Set(ByVal Value As String)
                msFax = Value
            End Set
        End Property
        Public Property NumberFormat() As System.Globalization.NumberFormatInfo
            Get
                NumberFormat = moNumberFormat
            End Get
            Set(ByVal Value As System.Globalization.NumberFormatInfo)
                moNumberFormat = Value
            End Set
        End Property
        Public Property DateFormat() As System.Globalization.DateTimeFormatInfo
            Get
                Return moDateFormat
            End Get
            Set(ByVal Value As System.Globalization.DateTimeFormatInfo)
                moDateFormat = Value
            End Set
        End Property
        Property IdCia() As Long
            Get
                Return mlIdCia
            End Get
            Set(ByVal Value As Long)
                mlIdCia = Value
            End Set
        End Property
        Property IdUsu() As Long
            Get
                Return mlIdUsu
            End Get
            Set(ByVal Value As Long)
                mlIdUsu = Value
            End Set
        End Property
        Private _nifUsu As String
        Public Property NIFUsu() As String
            Get
                Return _nifUsu
            End Get
            Set(ByVal value As String)
                _nifUsu = value
            End Set
        End Property

        Public ReadOnly Property Identity() As System.Security.Principal.IIdentity Implements System.Security.Principal.IPrincipal.Identity
            Get
                Return mUser
            End Get
        End Property
        Private _accesoCN As Boolean
        Public Property AccesoCN() As Boolean
            Get
                Return _accesoCN
            End Get
            Set(ByVal value As Boolean)
                _accesoCN = value
            End Set
        End Property
        Public ReadOnly Property CNTieneImagen() As Boolean
            Get
                Return _CNGuidImagenUsu IsNot String.Empty
            End Get
        End Property
        Private _CNGuidImagenUsu As String
        Public Property CNGuidImagenUsu() As String
            Get
                Return _CNGuidImagenUsu
            End Get
            Set(ByVal value As String)
                _CNGuidImagenUsu = value
            End Set
        End Property
        Private _CNNotificarResumenActividad As Boolean
        Public Property CNNotificarResumenActividad() As Boolean
            Get
                Return _CNNotificarResumenActividad
            End Get
            Set(ByVal value As Boolean)
                _CNNotificarResumenActividad = value
            End Set
        End Property
        Private _CNConfiguracionDesocultar As Boolean
        Public Property CNConfiguracionDesocultar() As Boolean
            Get
                Return _CNConfiguracionDesocultar
            End Get
            Set(ByVal value As Boolean)
                _CNConfiguracionDesocultar = value
            End Set
        End Property
        Private _DepDenominacion As String
        Public Property DepDenominacion() As String
            Get
                Return _DepDenominacion
            End Get
            Set(ByVal value As String)
                _DepDenominacion = value
            End Set
        End Property
        Private _CodProveGS As String
        Public Property CodProveGS() As String
            Get
                Return _CodProveGS
            End Get
            Set(ByVal value As String)
                _CodProveGS = value
            End Set
        End Property
        Private _IdContacto As Integer
        Public Property IdContacto() As Integer
            Get
                Return _IdContacto
            End Get
            Set(ByVal value As Integer)
                _IdContacto = value
            End Set
        End Property
        Public Function IsInRole(ByVal Action As String) As Boolean Implements System.Security.Principal.IPrincipal.IsInRole
            'La usaremos realmente para decir si contiene una acci�n de 
            ' seguridad espec�fica
            Return mUser.HasAction(Action)
        End Function
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
            mUser = New UserIdentity(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
        ''' <summary>
        ''' Cargar los datos del usuario
        ''' </summary>
        ''' <param name="sCod">C�digo de usuario</param>
        ''' <remarks>Llamada desde: PMPortalWeb --> Global.asax.vb, _common/QAEnProceso.aspx.vb, login/login.aspx.vb, solicitudes/EnProceso.aspx.vb ; Tiempo m�ximo; 1 sg.</remarks>
        Public Sub LoadUserData(ByVal sCod As String, ByVal lCiaComp As Integer)
            Dim data As DataSet
            Dim row As DataRow

            Authenticate()
            msCod = sCod
            If mRemottingServer Then
                data = DBServer.User_LoadUserData(msCodCia, msCod, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.User_LoadUserData(msCodCia, msCod)
            End If

            If Not data.Tables(0).Rows.Count = 0 Then
                row = data.Tables(0).Rows(0)
                mlIdCia = row.Item("CIAID")
                mlIdUsu = row.Item("ID")
                _IdContacto = row.Item("IDCONTACTO")

                msNombre = DBNullToStr(row.Item("NOM"))
                msApellidos = DBNullToStr(row.Item("APE"))
                msCodProve = DBNullToStr(row.Item("CIACOD"))
                msDenCia = DBNullToStr(row.Item("CIADEN"))
                msNIFCia = DBNullToStr(row.Item("NIF"))
                msThousanFmt = DBNullToStr(row.Item("THOUSANFMT"))
                msDecimalFmt = DBNullToStr(row.Item("DECIMALFMT"))
                miPrecisionFmt = DBNullToSomething(row.Item("PRECISIONFMT"))
                msDateFmt = DBNullToStr(row.Item("DATEFMT"))

                msMon = DBNullToStr(row.Item("MONCOD_USU"))
                If msMon = Nothing Then
                    msMon = DBNullToStr(row.Item("MONCOD_CIA"))
                End If
                miTipoEmail = DBNullToSomething(row.Item("TIPOEMAIL"))
                msTelefono = DBNullToStr(row.Item("TFNO"))
                msEmail = DBNullToStr(row.Item("EMAIL"))
                msFax = DBNullToStr(row.Item("FAX"))
                msIdioma = DBNullToStr(row.Item("IDICOD"))
                RefCultural = DBNullToStr(row.Item("LANGUAGETAG"))
                _nifUsu = DBNullToStr(row.Item("USUNIF"))
               
                'formato de fechas:
                moDateFormat = New System.Globalization.CultureInfo(Me.RefCultural, False).DateTimeFormat
                If Not msDateFmt = Nothing Then
                    moDateFormat.ShortDatePattern = Replace(msDateFmt, "mm", "MM")
                Else
                    moDateFormat.ShortDatePattern = "dd/MM/yyyy"
                End If

                moDateFormat.ShortTimePattern = "HH:mm"

                'formato num�rico:
                moNumberFormat = New System.Globalization.CultureInfo(Me.RefCultural, False).NumberFormat
                If Not miPrecisionFmt = Nothing Then
                    moNumberFormat.NumberDecimalDigits = miPrecisionFmt
                End If
                If Not msDecimalFmt = Nothing Then
                    moNumberFormat.NumberDecimalSeparator = msDecimalFmt
                End If
                If Not msThousanFmt = Nothing Then
                    moNumberFormat.NumberGroupSeparator = msThousanFmt
                End If

                If Not row.IsNull("PWD") AndAlso Not row.IsNull("FECPWD") Then msPassword = Encrypter.Encrypt(DBNullToSomething(row.Item("PWD")), msCod, False, Encrypter.TipoDeUsuario.Persona, row.Item("FECPWD"))

                '========================================
                'PROPIEDADES DE COLABORATION NETWORK
                '========================================
                If AccesoCN Then
                    data = DBServer.User_LoadUserDataCN(lCiaComp, CodProveGS, IdUsu, msSesionId, msIPDir, msPersistID)
                    If data.Tables(0).Rows.Count = 0 Then
                        _DepDenominacion = String.Empty
                        _CNGuidImagenUsu = String.Empty
                    Else
                        row = data.Tables(0).Rows(0)
                        _DepDenominacion = row("DEPDEN")
                        _CNGuidImagenUsu = row("GUIDIMAGENUSU").ToString
                        _IdContacto = row("IDCONTACTO")
                        'Opciones de notificaci�n
                        _CNNotificarResumenActividad = row("FSCN_NOTIFICAR_RESUMEN_ACTIVIDAD")
                        _CNConfiguracionDesocultar = row("FSCN_DESOCULTAR_AUT")
                    End If
                End If
                '========================================
                '========================================
            End If
            row = Nothing
            data = Nothing
        End Sub
        ''' <summary>
        ''' Carga los datos de personas
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <remarks>Llamada desde: script\solicitudes\solicitudes.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Sub LoadUONPersona(ByVal lCiaComp As Long)

            Dim data As DataSet

            Authenticate()
            If mRemottingServer Then
                data = DBServer.Personas_Load(lCiaComp, msAprobadorActual, , , , , , , msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Personas_Load(lCiaComp, msAprobadorActual)
            End If

            If data.Tables(0).Rows.Count > 0 Then
                msUON1 = DBNullToSomething(data.Tables(0).Rows(0).Item("UON1"))
                msUON2 = DBNullToSomething(data.Tables(0).Rows(0).Item("UON2"))
                msUON3 = DBNullToSomething(data.Tables(0).Rows(0).Item("UON3"))
            End If
        End Sub
        ''' <summary>
        ''' Devuelve si se puede ver o no el detalle de la persona y el flujo de la solicitud
        ''' </summary>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <returns>si se puede ver o no el detalle de la persona y el flujo de la solicitud</returns>
        ''' <remarks>Llamada desde: script\_common\guardarinstancia.aspx   script\solicitudes\accionRealizadaOK.aspx  script\solicitudes\comentariosSolicitud.aspx
        ''' script\solicitudes\detalleSolicitud.aspx    script\solicitudes\devolucion.aspx    script\solicitudes\flowDiagram.aspx script\solicitudes\RealizarAccion.aspx
        ''' ; Tiempo m�ximo: 0,2</remarks>
        Public Function VerDetallePersona(ByVal lInstancia As Long, ByVal lCiaComp As Long) As DataTable
            Authenticate()
            If mRemottingServer Then
                Return DBServer.User_VerDetallePersona(lCiaComp, Me.CodProve, lInstancia, msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.User_VerDetallePersona(lCiaComp, Me.CodProve, lInstancia)
            End If
        End Function
        ''' <summary>
        ''' Funcion que nos devuelve el codigo del Proveedor en el GS
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <returns>el codigo del Proveedor en el GS</returns>
        ''' <remarks>Llamada desde: script\_common\guardarinstancia.aspx ; Tiempo m�ximo: 0,2</remarks>
        Function ObtenerProv(ByVal lCiaComp As Long) As String
            Authenticate()
            If mRemottingServer Then
                Return DBServer.User_ObtenerProvGS(lCiaComp, Me.CodProve, msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.User_ObtenerProvGS(lCiaComp, Me.CodProve)
            End If
        End Function
        ''' <summary>
        ''' Obtiene la configuraci�n de la pantalla de puntuaciones del usuario
        ''' </summary>
        ''' <param name="IdCiaComp">Id Compa�ia compradora</param>
        ''' <param name="IdUsu">Id del usuario logueado en portal</param>
        ''' <returns>Un datatable con los datos cargados</returns>
        ''' <remarks>Llamada desde: script\variablescalidad\PuntuacionesProveedor.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Function ObtenerConfiguracionesCalificacion(ByVal IdCiaComp As Integer, ByVal IdUsu As Integer) As DataTable
            Authenticate()
            If mRemottingServer Then
                Return DBServer.User_ObtenerConfiguracionesCalificacion(IdCiaComp, IdUsu, msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.User_ObtenerConfiguracionesCalificacion(IdCiaComp, IdUsu)
            End If
        End Function
        ''' <summary>
        ''' Obtiene la configuraci�n por defecto de la pantalla de puntuaciones
        ''' </summary>
        ''' <param name="IdCiaComp">Id Compa�ia compradora</param>
        ''' <returns>Un datatable con los datos cargados</returns>
        ''' <remarks>Llamada desde: script\variablescalidad\PuntuacionesProveedor.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Function ObtenerConfiguracionesDefectoCalificacion(ByVal IdCiaComp As Integer) As DataTable
            Authenticate()
            If mRemottingServer Then
                Return DBServer.User_ObtenerConfiguracionesDefectoCalificacion(IdCiaComp, msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.User_ObtenerConfiguracionesDefectoCalificacion(IdCiaComp)
            End If
        End Function
        ''' <summary>
        ''' Guarda la configuraci�n de la pantalla de puntuaciones del usuario
        ''' </summary>
        ''' <param name="IdCiaComp">Id Compa�ia compradora</param>
        ''' <param name="IdUsu">Id del usuario logueado en portal</param>
        ''' <param name="Config_UNQA_Usu">Configuraci�n de visibilidad y ancho de las columnas de las unidades de negocio</param>
        ''' <param name="Config_VarCal_Usu">Configuraci�n de las variables de calidad colapsadas / expandidas</param>
        ''' <remarks>Llamada desde: script\variablescalidad\PuntuacionesProveedor.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Sub GuardarConfiguracionesCalificacion(ByVal IdCiaComp As Integer, ByVal IdUsu As Integer, ByVal Config_UNQA_Usu As String, ByVal Config_VarCal_Usu As String)
            Authenticate()
            If mRemottingServer Then
                DBServer.User_GuardarConfiguracionesCalificacion(IdCiaComp, IdUsu, Config_UNQA_Usu, Config_VarCal_Usu, msSesionId, msIPDir, msPersistID)
            Else
                DBServer.User_GuardarConfiguracionesCalificacion(IdCiaComp, IdUsu, Config_UNQA_Usu, Config_VarCal_Usu)
            End If
        End Sub
        ''' <summary>
        ''' Establece/Devuelve el codigo de compania compradora
        ''' </summary>
        ''' <returns>codigo de compania compradora</returns>
        ''' <remarks>Llamada desde: PMPortalServer/Root.vb/login; Tiempo maximo:0</remarks>
        Property CiaComp() As Long
            Get
                Return mlCiaComp
            End Get
            Set(ByVal Value As Long)
                mlCiaComp = Value
            End Set
        End Property
#Region "M�todos CN"
        ''' <summary>
        ''' Guarda las opciones de CN del usuario
        ''' </summary>
        ''' <param name="sUsuCod">C�digo de usuario </param>
        ''' <param name="bNotificarResumenActividad">Opci�n de usuario: Resumen de actividad</param>
        ''' <param name="bNotificarIncluidoGrupo">Opci�n de usuario: Me incluyen en un grupo corporativo</param>
        ''' <param name="bConfiguracionDesocultar">Volver a mostrar autom�ticamente los mensajes ocultos en los que haya actividad</param>
        ''' <remarks>Llamada desde: cn_UserProfile.aspx.vb ; Tiempo m�ximo: 0,1</remarks>
        ''' <revision>JVS 02/02/2012</revision>
        Public Sub CN_Update_Opciones_Usuario(ByVal IdCiaComp As Integer, ByVal ProveCod As String, ByVal IdContacto As Integer, _
                ByVal bNotificarResumenActividad As Boolean, ByVal bConfiguracionDesocultar As Boolean)
            Authenticate()
            DBServer.CN_Update_Opciones_Usuario(IdCiaComp, ProveCod, IdContacto, bNotificarResumenActividad, bConfiguracionDesocultar, _
                                                             msSesionId, msIPDir, msPersistID)
        End Sub
        ''' <summary>
        ''' Guarda la imagen del usuario
        ''' </summary>
        ''' <param name="sUsuCod">C�digo de usuario </param>
        ''' <param name="sNombreImagen">Nombre de la imagen</param>
        ''' <remarks>Llamada desde: cn_UserProfile.aspx.vb ; Tiempo m�ximo: 0,1</remarks>
        ''' <revision>JVS 14/02/2012</revision>
        Public Sub CN_Actualizar_Imagen_Usuario(ByVal IdCiaComp As Integer, ByVal pathFoto As String, _
                ByVal ProveCod As String, ByVal IdContacto As Integer, ByVal DGuidImagen As String)
            Authenticate()
            DBServer.CN_Actualizar_Imagen_Usuario(IdCiaComp, pathFoto, ProveCod, IdContacto, DGuidImagen, _
                                                             msSesionId, msIPDir, msPersistID)
        End Sub
#End Region
    End Class
End Namespace