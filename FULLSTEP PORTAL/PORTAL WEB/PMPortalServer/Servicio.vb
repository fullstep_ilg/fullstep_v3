﻿Namespace Fullstep.PMPortalServer
    <Serializable()>
    Public Class Servicio
        Inherits Security

        Private datosServicio As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return datosServicio
            End Get
        End Property

        ''' <summary>
        ''' Procedimiento que carga los datos del servicio
        ''' </summary>
        ''' <param name="sServicio"></param>
        ''' <param name="sCampoId"></param>
        ''' <param name="Instancia">Instancia</param>
        ''' <remarks>
        ''' Llamada desde:
        ''' </remarks>
        Public Sub LoadData(ByVal lCiaComp As Integer, ByVal sServicio As String, ByVal sCampoId As String, ByVal Instancia As Long)
            Authenticate()
            datosServicio = DBServer.Servicio_Load(lCiaComp, sServicio, sCampoId, Instancia)
        End Sub

        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace