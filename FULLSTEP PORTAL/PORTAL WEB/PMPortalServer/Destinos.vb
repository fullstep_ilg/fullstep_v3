Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Destinos
        Inherits Security
        Private moDestinos As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moDestinos
            End Get

        End Property
        ''' <summary>
        ''' Funci�n que devuelve un dataset con los datos del destino aprovisionadores para el usuario actual
        ''' </summary>
        ''' <param name="sIdi">Codigo del idioma</param>
        ''' <param name="Destino">Codigo del destino</param>
        ''' <returns>un dataset con los datos del destino aprovisionadores para el usuario actual</returns>
        ''' <remarks>
        ''' Llamada desde: La funci�n CargarDatosDestino de Destino.vb
        ''' Tiempo: 0 seg</remarks>
        Public Function CargarDatosDestino(ByVal sIdi As String, ByVal Destino As String, ByVal lCiaComp As Long) As DataSet
            Authenticate()
            Return DBServer.Destinos_CargarDatosDestino(sIdi, Destino, lCiaComp)
        End Function
       

        Public Function CargarUnDestino(ByVal sIdi As String, sIdDest As String, ByVal lCiaComp As Long) As Destino
            Dim dsDatosDestino As DataSet
            Authenticate()
            dsDatosDestino = DBServer.Destinos_CargarDatosDestino(sIdi, sIdDest, lCiaComp)
            dsDatosDestino.Tables(0).TableName = "DESTINOS"
            DBServer.Destinos_CargarDatosDestino(sIdi, sIdDest, lCiaComp)
            Dim key() As DataColumn = {dsDatosDestino.Tables("DESTINOS").Columns("COD")}
            dsDatosDestino.Tables("DESTINOS").PrimaryKey = key
            Dim objnewmember As Destino

            For i As Integer = 0 To dsDatosDestino.Tables("DESTINOS").Rows.Count - 1
                With dsDatosDestino.Tables("DESTINOS").Rows(i)
                    objnewmember = New Destino(mDBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
                    objnewmember.Cod = DBNullToSomething(.Item("COD"))
                    objnewmember.Den = DBNullToSomething(.Item("DEN"))
                    objnewmember.Dir = IIf(IsNothing(DBNullToSomething(.Item("DIR"))) Or IsDBNull(DBNullToSomething(.Item("DIR"))), "", DBNullToSomething(.Item("DIR")))
                    objnewmember.Poblacion = IIf(IsNothing(DBNullToSomething(.Item("POB"))) Or IsDBNull(DBNullToSomething(.Item("POB"))), "", DBNullToSomething(.Item("POB")))
                    objnewmember.CP = IIf(IsNothing(DBNullToSomething(.Item("CP"))) Or IsDBNull(DBNullToSomething(.Item("CP"))), "", DBNullToSomething(.Item("CP")))
                    objnewmember.Provincia = IIf(IsNothing(DBNullToSomething(.Item("PROVI"))) Or IsDBNull(DBNullToSomething(.Item("PROVI"))), "", DBNullToSomething(.Item("PROVI")))
                    objnewmember.Pais = IIf(IsNothing(DBNullToSomething(.Item("PAI"))) Or IsDBNull(DBNullToSomething(.Item("PAI"))), "", DBNullToSomething(.Item("PAI")))
                    objnewmember.Tfno = .Item("TFNO").ToString
                    objnewmember.FAX = .Item("FAX").ToString
                    objnewmember.Email = .Item("Email").ToString
                End With
            Next

            dsDatosDestino.Clear()
            dsDatosDestino = Nothing
            Return objnewmember
        End Function
        
        ''' <summary>
        ''' Carga los destinos
        ''' </summary>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sPer">Persona</param>
        ''' <param name="sCod">Codigo de destino</param>
        ''' <remarks>Llamada desde: _common\campos.ascx.vb  _common\desglose.ascx.vb; Tiempo m�ximo: 0,1</remarks>
        Public Sub LoadData(ByVal sIdi As String, ByVal lCiaComp As Long, ByVal sPer As String, Optional ByVal sCod As String = Nothing)
            Authenticate()
            If mRemottingServer Then
                moDestinos = DBServer.Destinos_Get(sIdi, lCiaComp, sPer, sCod, msSesionId, msIPDir, msPersistID)
            Else
                moDestinos = DBServer.Destinos_Get(sIdi, lCiaComp, sPer, sCod)
            End If
        End Sub

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
       
    End Class
End Namespace