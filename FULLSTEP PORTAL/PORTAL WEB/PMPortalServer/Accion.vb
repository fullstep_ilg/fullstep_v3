Namespace Fullstep.PMPortalServer
    <Serializable()> _
Public Class Accion
        Inherits Security

        Private mlId As Integer
        Private msDen As String
        Private miTipoRechazo As Integer
        Private miTipo As Integer
        Private mbGuardar As Boolean

        Property Id() As Integer
            Get
                Return mlId
            End Get
            Set(ByVal Value As Integer)
                mlId = Value
            End Set
        End Property

        Property Den() As String
            Get
                Return msDen
            End Get
            Set(ByVal Value As String)
                msDen = Value
            End Set
        End Property

        Property TipoRechazo() As Integer
            Get
                Return miTipoRechazo
            End Get
            Set(ByVal Value As Integer)
                miTipoRechazo = Value
            End Set
        End Property

        Property Tipo() As Integer
            Get
                Return miTipo
            End Get
            Set(ByVal Value As Integer)
                miTipo = Value
            End Set
        End Property
        Property Guardar() As Boolean
            Get
                Return mbGuardar
            End Get
            Set(ByVal Value As Boolean)
                mbGuardar = Value
            End Set
        End Property

        ''' <summary>
        ''' Cargar una accion
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <remarks>Llamada desde: _common\guardarinstancia.aspx.vb solicitudes\accionRealizadaOK.aspx.vb  solicitudes\RealizarAccion.aspx.vb; Tiempo m�ximo: 0,2</remarks>
        Public Sub CargarAccion(ByVal lCiaComp As Long, ByVal sIdi As String)
            Authenticate()

            Dim oData As DataSet
            Dim oRow As DataRow

            If mRemottingServer Then
                oData = DBServer.Accion_Load(lCiaComp, mlId, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                oData = DBServer.Accion_Load(lCiaComp, mlId, sIdi)
            End If

            If oData.Tables.Count > 0 Then
                If oData.Tables(0).Rows.Count > 0 Then
                    oRow = oData.Tables(0).Rows(0)

                    msDen = oRow.Item("DEN")
                    miTipoRechazo = oRow.Item("TIPO_RECHAZO")
                    miTipo = oRow.Item("TIPO")
                    mbGuardar = SQLBinaryToBoolean(oRow.Item("GUARDA"))
                End If
            End If
        End Sub


        Public Function ObtenerEnlaces(ByVal lBloqueOrigen As Long, ByVal lAccion As Long, ByVal lCiaComp As Long) As DataSet
            Authenticate()
            Return DBServer.Instancia_ObtenerEnlaces(lBloqueOrigen, lAccion, lCiaComp)
        End Function

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

    End Class

End Namespace
