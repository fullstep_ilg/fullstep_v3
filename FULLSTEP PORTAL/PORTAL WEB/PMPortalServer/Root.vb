
Imports System.Threading
Imports System.Reflection
Imports System.Runtime.Remoting
Imports System.Runtime.Remoting.Channels
Imports System.Runtime.Remoting.Channels.http
Imports System.Configuration
Imports System.Security.Principal
Imports Fullstep.PMPortalDatabaseServer

Namespace Fullstep.PMPortalServer

    <Serializable()> _
        Public Class Root
        Private mDBServer As Fullstep.PMPortalDatabaseServer.Root
        Private moTipoAcceso As FSNLibrary.TiposDeDatos.ParametrosGenerales
        Private msUserCode As String = ""
        Private mRemottingServer As Boolean = False
        Private mIsAuthenticated As Boolean = False
        Private msSesionId As String = ""
        Private msIPDir As String = ""
        Private msPersistId As String = ""
        Private _bAccesoServidorExterno As Boolean
        Private moLongitudesDeCodigos As TiposDeDatos.LongitudesDeCodigos

        Public Property AccesoServidorExterno As Boolean
            Get
                Return _bAccesoServidorExterno
            End Get
            Set(value As Boolean)
                _bAccesoServidorExterno = value
            End Set
        End Property

        Property LongitudesDeCodigos() As TiposDeDatos.LongitudesDeCodigos
            Get
                Return moLongitudesDeCodigos
            End Get
            Set(ByVal Value As TiposDeDatos.LongitudesDeCodigos)
                moLongitudesDeCodigos = Value
            End Set
        End Property
        ''' <summary>Carga la propiedad AccesoServidorExterno</summary>
        Public Sub Load_AccesoServidorExterno()
            _bAccesoServidorExterno = DBServer.Parametros_GetAccesoServidorExterno()
        End Sub

        ''' <summary>
        ''' Carga las longitudes de los campos personalizados 
        ''' </summary>
        ''' <param name="lCiaComp">Id de compania</param>
        ''' <remarks>Llamada desde: PMPortalWeb\script\FSPMPage.vb ; Tiempo m�ximo: 0,2</remarks>
        Public Sub Load_LongitudesDeCodigos(ByVal lCiaComp As Long)
            Dim data As DataSet
            Dim row As DataRow
            Dim oLongs As TiposDeDatos.LongitudesDeCodigos
            On Error Resume Next
            Authenticate()
            If mRemottingServer Then
                data = DBServer.LongitudesCampo_Get(lCiaComp, msSesionId, msIPDir, msPersistId)
            Else
                data = DBServer.LongitudesCampo_Get(lCiaComp)
            End If

            Dim i As Integer = 0
            With data.Tables(0)
                oLongs.giLongCodART = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodCAL = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodCOM = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodDEP = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodDEST = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodEQP = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodGMN1 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodGMN2 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodGMN3 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodGMN4 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodMON = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodOFEEST = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPAG = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPAI = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPER = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPERF = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPRESCON1 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPRESCON2 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPRESCON3 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPRESCON4 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPRESPROY1 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPRESPROY2 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPRESPROY3 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPRESPROY4 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPROVE = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPROVI = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodROL = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodUNI = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodUON1 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodUON2 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodUON3 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodUSU = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodACT1 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodACT2 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodACT3 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodACT4 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodACT5 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPRESCONCEP31 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPRESCONCEP32 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPRESCONCEP33 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPRESCONCEP34 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPRESCONCEP41 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPRESCONCEP42 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPRESCONCEP43 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodPRESCONCEP44 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodCAT1 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodCAT2 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodCAT3 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodCAT4 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodCAT5 = .Rows(i).Item(0)
                i += 1
                oLongs.giLongCodGRUPOPROCE = .Rows(i).Item(0)
                i += 1
                i += 1
                oLongs.giLongCodDENART = .Rows(i).Item(0)
            End With

            moLongitudesDeCodigos = oLongs

        End Sub
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase especificada
        ''' </summary>
        ''' <param name="Tipo">Tipo de datos de la clase FSNServer a devolver</param>
        ''' <returns>Un objeto de la clase especificada</returns>
        Public Function Get_Object(ByVal Tipo As Type) As Object
            Authenticate()
            Dim pars() As Object = {DBServer(), mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated}
            Return Activator.CreateInstance(Tipo, pars)
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase; Tiempo m�ximo: 0</remarks>
        Public Function Get_Activo() As Activo
            Authenticate()
            Dim oActivo As New Fullstep.PMPortalServer.Activo(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oActivo
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase; Tiempo m�ximo: 0</remarks>
        Public Function Get_Adjunto() As Adjunto
            Dim oAdjun As Adjunto
            Authenticate()
            oAdjun = New Adjunto(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oAdjun
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Adjuntos() As Adjuntos
            Dim oAdjuns As Adjuntos
            Authenticate()
            oAdjuns = New Adjuntos(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oAdjuns
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Articulos() As Articulos
            Authenticate()
            Dim oArts As New Fullstep.PMPortalServer.Articulos(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oArts
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Campo() As Campo
            Dim oCampo As Campo
            Authenticate()
            oCampo = New Campo(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oCampo
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Certificado() As Certificado
            Dim oCertificado As Certificado
            Authenticate()
            oCertificado = New Certificado(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oCertificado
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Certificados() As Certificados
            Dim oCertificados As Certificados
            Authenticate()
            oCertificados = New Certificados(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oCertificados
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Departamento() As Departamento
            Dim oDepartamento As Departamento
            Authenticate()
            oDepartamento = New Departamento(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oDepartamento
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Departamentos() As Departamentos
            Dim oDepartamentos As Departamentos
            Authenticate()
            oDepartamentos = New Departamentos(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oDepartamentos
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Destino() As Destino
            Dim oDestino As Destino
            Authenticate()
            oDestino = New Destino(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oDestino
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Destinos() As Destinos
            Authenticate()
            Dim oDestinos As New Fullstep.PMPortalServer.Destinos(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oDestinos

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Dictionary() As Dictionary
            Authenticate()
            Dim oDictionary As New Fullstep.PMPortalServer.Dictionary(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oDictionary
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function get_EstadosNoConf() As EstadosNoConf
            Authenticate()
            Dim oEstadosNoConf As New Fullstep.PMPortalServer.EstadosNoConf(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oEstadosNoConf
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_FormasPago() As FormasPago
            Authenticate()
            Dim oFormasPago As New Fullstep.PMPortalServer.FormasPago(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oFormasPago

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Formulario() As Formulario
            Authenticate()
            Dim oFormulario As New Fullstep.PMPortalServer.Formulario(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oFormulario

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function get_GruposMaterial() As Fullstep.PMPortalServer.GruposMaterial

            Authenticate()
            Dim oGruposMaterial As New Fullstep.PMPortalServer.GruposMaterial(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oGruposMaterial

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_GruposMatNivel1() As Fullstep.PMPortalServer.GruposMatNivel1
            Authenticate()
            Dim oGruposMatNivel1 As New Fullstep.PMPortalServer.GruposMatNivel1(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oGruposMatNivel1

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_GruposMatNivel2() As Fullstep.PMPortalServer.GruposMatNivel2
            Authenticate()
            Dim oGruposMatNivel2 As New Fullstep.PMPortalServer.GruposMatNivel2(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oGruposMatNivel2

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_GruposMatNivel3() As Fullstep.PMPortalServer.GruposMatNivel3
            Authenticate()
            Dim oGruposMatNivel3 As New Fullstep.PMPortalServer.GruposMatNivel3(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oGruposMatNivel3

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_GruposMatNivel4() As Fullstep.PMPortalServer.GruposMatNivel4
            Authenticate()
            Dim oGruposMatNivel4 As New Fullstep.PMPortalServer.GruposMatNivel4(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oGruposMatNivel4

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_GrupoMatNivel1() As Fullstep.PMPortalServer.GrupoMatNivel1
            Authenticate()
            Dim oGrupoMatNivel1 As New Fullstep.PMPortalServer.GrupoMatNivel1(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oGrupoMatNivel1

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_GrupoMatNivel2() As Fullstep.PMPortalServer.GrupoMatNivel2
            Authenticate()
            Dim oGrupoMatNivel2 As New Fullstep.PMPortalServer.GrupoMatNivel2(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oGrupoMatNivel2

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_GrupoMatNivel3() As Fullstep.PMPortalServer.GrupoMatNivel3
            Authenticate()
            Dim oGrupoMatNivel3 As New Fullstep.PMPortalServer.GrupoMatNivel3(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oGrupoMatNivel3

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_GrupoMatNivel4() As Fullstep.PMPortalServer.GrupoMatNivel4
            Authenticate()
            Dim oGrupoMatNivel4 As New Fullstep.PMPortalServer.GrupoMatNivel4(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oGrupoMatNivel4

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Instancia() As Instancia
            Dim oInstancia As Instancia
            Authenticate()
            oInstancia = New Instancia(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oInstancia
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Monedas() As Fullstep.PMPortalServer.Monedas
            Authenticate()
            Dim oMonedas As New Fullstep.PMPortalServer.Monedas(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oMonedas
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_NoConformidad() As NoConformidad
            Authenticate()
            Dim oNoConformidad As New Fullstep.PMPortalServer.NoConformidad(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oNoConformidad
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_NoConformidades() As NoConformidades
            Authenticate()
            Dim oNoConformidades As New Fullstep.PMPortalServer.NoConformidades(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oNoConformidades
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Paises() As Fullstep.PMPortalServer.Paises
            Authenticate()
            Dim oPaises As New Fullstep.PMPortalServer.Paises(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oPaises
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Partida() As Partida
            Authenticate()
            Dim oPartida As New Fullstep.PMPortalServer.Partida(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oPartida
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Personas() As Personas
            Dim oPersonas As Personas
            Authenticate()
            oPersonas = New Personas(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oPersonas
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Persona() As Persona
            Dim oPersona As Persona
            Authenticate()
            oPersona = New Persona(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oPersona
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_PresProyectosNivel1() As Fullstep.PMPortalServer.PresProyectosNivel1
            Authenticate()
            Dim oPres1 As New Fullstep.PMPortalServer.PresProyectosNivel1(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oPres1

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_PresContablesNivel1() As Fullstep.PMPortalServer.PresContablesNivel1
            Authenticate()
            Dim oPres2 As New Fullstep.PMPortalServer.PresContablesNivel1(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oPres2

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_PresConceptos3Nivel1() As Fullstep.PMPortalServer.PresConceptos3Nivel1
            Authenticate()
            Dim oPres3 As New Fullstep.PMPortalServer.PresConceptos3Nivel1(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oPres3

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_PresConceptos4Nivel1() As Fullstep.PMPortalServer.PresConceptos4Nivel1
            Authenticate()
            Dim oPres4 As New Fullstep.PMPortalServer.PresConceptos4Nivel1(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oPres4

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function get_Proveedor() As Fullstep.PMPortalServer.Proveedor
            Authenticate()
            Dim oProve As New Fullstep.PMPortalServer.Proveedor(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oProve

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function get_Proveedores() As Fullstep.PMPortalServer.Proveedores
            Authenticate()
            Dim oProves As New Fullstep.PMPortalServer.Proveedores(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oProves

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Provincias() As Provincias
            Dim oProvincias As Provincias
            Authenticate()
            oProvincias = New Provincias(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oProvincias
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Solicitud() As Solicitud
            Dim oSolicitud As Solicitud
            Authenticate()
            oSolicitud = New Solicitud(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oSolicitud
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Unidad() As Unidad
            Dim oUnidad As Unidad
            Authenticate()
            oUnidad = New Unidad(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oUnidad
        End Function
        ''' <summary>
        ''' Obtiene una instancia de la clase Contrato
        ''' </summary>
        ''' <returns>Contrato</returns>
        ''' <remarks></remarks>
        Public Function Get_Contrato() As Contrato
            Dim oContrato As Contrato
            Authenticate()
            oContrato = New Contrato(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oContrato
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function get_Unidades() As Fullstep.PMPortalServer.Unidades
            Authenticate()
            Dim oUnis As New Fullstep.PMPortalServer.Unidades(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oUnis

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function get_UnidadesOrg() As Fullstep.PMPortalServer.UnidadesOrg
            Authenticate()
            Dim oUnis As New Fullstep.PMPortalServer.UnidadesOrg(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oUnis

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function get_UnidadesNeg() As Fullstep.PMPortalServer.UnidadesNeg
            Authenticate()
            Dim oUnis As New Fullstep.PMPortalServer.UnidadesNeg(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oUnis
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_User() As User
            Authenticate()
            Dim ouser As New Fullstep.PMPortalServer.User(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return ouser
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Variables() As Fullstep.PMPortalServer.CVariablesCalidad
            Authenticate()
            Dim oVariables As New Fullstep.PMPortalServer.CVariablesCalidad(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oVariables
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Solicitudes() As Fullstep.PMPortalServer.Solicitudes
            Authenticate()
            Dim oSolicitudes As New Fullstep.PMPortalServer.Solicitudes(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oSolicitudes
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Instancias() As Fullstep.PMPortalServer.Instancias
            Authenticate()
            Dim oInstancias As New Fullstep.PMPortalServer.Instancias(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oInstancias
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_DiagBloques() As Fullstep.PMPortalServer.DiagBloques
            Authenticate()
            Dim oDiagBloques As New Fullstep.PMPortalServer.DiagBloques(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oDiagBloques
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_DiagEnlaces() As DiagEnlaces
            Authenticate()
            Dim oDiagEnlaces As New Fullstep.PMPortalServer.DiagEnlaces(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oDiagEnlaces

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Rol() As Rol
            Authenticate()
            Dim oRol As New Fullstep.PMPortalServer.Rol(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oRol

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Accion() As Accion
            Authenticate()
            Dim oAccion As New Fullstep.PMPortalServer.Accion(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oAccion

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Bloque() As Bloque
            Authenticate()
            Dim Bloque As New Fullstep.PMPortalServer.Bloque(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return Bloque

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Participantes() As Participantes
            Authenticate()
            Dim Participantes As New Fullstep.PMPortalServer.Participantes(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return Participantes

        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_OrganizacionCompras() As OrganizacionCompras
            Dim oOrganizacionCompras As OrganizacionCompras
            Authenticate()
            oOrganizacionCompras = New OrganizacionCompras(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oOrganizacionCompras
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_OrganizacionesCompras() As OrganizacionesCompras
            Dim oOrganizacionesCompras As OrganizacionesCompras
            Authenticate()
            oOrganizacionesCompras = New OrganizacionesCompras(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oOrganizacionesCompras
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Centro() As Centro
            Dim oCentro As Centro
            Authenticate()
            oCentro = New Centro(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oCentro
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Centros() As Centros
            Dim oCentros As Centros
            Authenticate()
            oCentros = New Centros(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oCentros
        End Function
        Public Function Get_Centros_SM() As Centros_SM
            Dim oCentros_SM As Centros_SM
            Authenticate()
            oCentros_SM = New Centros_SM(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oCentros_SM
        End Function
        Public Function Get_Press5() As PRES5
            Dim oPress5 As PRES5
            Authenticate()
            oPress5 = New PRES5(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oPress5
        End Function
        Public Function Get_Empresas() As Empresas
            Dim oEmpresas As Empresas
            Authenticate()
            oEmpresas = New Empresas(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oEmpresas
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Almacen() As Almacen
            Dim oAlmacen As Almacen
            Authenticate()
            oAlmacen = New Almacen(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oAlmacen
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Almacenes() As Almacenes
            Dim oAlmacenes As Almacenes
            Authenticate()
            oAlmacenes = New Almacenes(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oAlmacenes
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function get_TablaExterna() As Fullstep.PMPortalServer.TablaExterna
            Authenticate()
            Dim oTablaExterna As New Fullstep.PMPortalServer.TablaExterna(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oTablaExterna
        End Function
        ''' Revisado por: blp. Fecha: 26/10/2011
        ''' <summary>
        ''' Funci�n que devuelve un objeto de tipo Entregas cuando la autenticaci�n es correcta
        ''' </summary>
        ''' <returns>Objecto Entregas</returns>
        ''' <remarks>Llamada desde: all� donde se requiera un objeto de este tipo. M�x: 0,1 seg.</remarks>
        Public Function Get_Entregas() As Entregas
            Dim oEntregas As Entregas
            Authenticate()
            oEntregas = New Entregas(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oEntregas
        End Function
        ''' Revisado por: auv. Fecha: 18/02/2013
        ''' <summary>
        ''' Funci�n que devuelve un objeto de tipo Ofertas cuando la autenticaci�n es correcta
        ''' </summary>
        ''' <returns>Objecto Entregas</returns>
        ''' <remarks>Llamada desde: all� donde se requiera un objeto de este tipo. M�x: 0,1 seg.</remarks>
        Public Function Get_Ofertas() As Ofertas
            Dim oOfertas As Ofertas
            Authenticate()
            oOfertas = New Ofertas(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oOfertas
        End Function
        ''' Revisado por: auv. Fecha: 16/01/2013
        ''' <summary>
        ''' Funci�n que devuelve un objeto de tipo OfertasHistoricas cuando la autenticaci�n es correcta
        ''' </summary>
        ''' <returns>Objecto Entregas</returns>
        ''' <remarks>Llamada desde: all� donde se requiera un objeto de este tipo. M�x: 0,1 seg.</remarks>
        Public Function Get_OfertasHistoricas() As OfertasHistoricas
            Dim oOfertasHistoricas As OfertasHistoricas
            Authenticate()
            oOfertasHistoricas = New OfertasHistoricas(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oOfertasHistoricas
        End Function
        ''' Revisado por: blp. Fecha: 02/02/2012
        ''' <summary>
        ''' Funci�n que devuelve un objeto de tipo ValidacionesIntegracion cuando la autenticaci�n es correcta
        ''' </summary>
        ''' <returns>Objecto Entregas</returns>
        ''' <remarks>Llamada desde: all� donde se requiera un objeto de este tipo. M�x: 0,1 seg.</remarks>
        Public Function Get_ValidacionesIntegracion() As ValidacionesIntegracion
            Dim oValidacionesIntegracion As ValidacionesIntegracion
            Authenticate()
            oValidacionesIntegracion = New ValidacionesIntegracion(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oValidacionesIntegracion
        End Function
        Property TipoAcceso() As FSNLibrary.TiposDeDatos.ParametrosGenerales
            Get
                Return moTipoAcceso
            End Get
            Set(ByVal Value As FSNLibrary.TiposDeDatos.ParametrosGenerales)
                moTipoAcceso = Value
            End Set
        End Property
        Public Function GetAccessType(ByVal lCiaComp As Long) As FSNLibrary.TiposDeDatos.ParametrosGenerales
            Authenticate()

            Dim data As DataSet
            data = DBServer.Parametros_GetData(lCiaComp)

            moTipoAcceso.g_bAccesoFSFA = FSNLibrary.Encrypter.EncriptarAcceso("FA", FSNLibrary.modUtilidades.DBNullToStr(data.Tables(0).Rows(0).Item("ACCESO_FSFA")), False) = "FULLSTEP FA"
            moTipoAcceso.gbMaterialVerTodosNiveles = DBNullToBoolean(data.Tables(0).Rows(0).Item("MOSTRAR_TODOS_NIVELES_MATERIAL"))

            data = DBServer.Parametros_CargarParametrosGenerales(lCiaComp)

            'Tabla 0: PARGEN_INTERNO
            'Tabla 1 :PARGEN_PED
            'Tabla 2: PARGEN_LIT
            moTipoAcceso.gbAccesoFSSM = FSNLibrary.Encrypter.EncriptarAcceso("SM", DBNullToStr(data.Tables(0).Rows(0).Item("ACCESO_FSSM")), False) = "FULLSTEP SM"
            moTipoAcceso.gbOblCodPedDir = data.Tables(1).Rows(0)("OBLCODPEDDIR")
            moTipoAcceso.gbOblCodPedido = data.Tables(1).Rows(0)("OBLCODPEDIDO")

            Dim textnomPedERP() As DataRow = data.Tables(2).Select("ID=31")

            moTipoAcceso.nomPedERP = New Dictionary(Of FSNLibrary.Idioma, String)

            For Each fila As DataRow In textnomPedERP
                moTipoAcceso.nomPedERP.Add(CType(fila.Item("IDI").ToString(), FSNLibrary.Idioma), fila.Item("DEN").ToString())
            Next

            Return moTipoAcceso
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Factura() As Factura
            Dim oFactura As Factura
            Authenticate()
            oFactura = New Factura(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oFactura
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_TiposPedido() As TiposPedido
            Dim oTiposPedido As TiposPedido
            Authenticate()
            oTiposPedido = New TiposPedido(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oTiposPedido
        End Function
        ''' Revisado por: blp. Fecha: 08/11/2011
        ''' <summary>
        ''' Funci�n que devuelve un objeto de tipo Parametros cuando la autenticaci�n es correcta
        ''' </summary>
        ''' <returns>Objecto Parametros</returns>
        ''' <remarks>Llamada desde: all� donde se requiera un objeto de este tipo. M�x: 0,1 seg.</remarks>
        Public Function Get_Parametros() As Parametros
            Dim oParametros As Parametros
            Authenticate()
            oParametros = New Parametros(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oParametros
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase Proveedores ERP
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_ProveedoresERP() As ProveedoresERP
            Dim oProveedoresERP As ProveedoresERP
            Authenticate()
            oProveedoresERP = New ProveedoresERP(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oProveedoresERP
        End Function
        ''' <summary>
        ''' Funci�n que instancia y devuelve un objeto de la clase
        ''' </summary>
        ''' <returns>Un objeto de la clase</returns>
        ''' <remarks>Llamada desde: Cada pantalla/clase q desee manejar un objeto de la clase ; Tiempo m�ximo: 0</remarks>
        Public Function Get_Escenarios() As Escenarios
            Dim oEscenarios As Escenarios
            Authenticate()
            oEscenarios = New Escenarios(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oEscenarios
        End Function
        Public Function Get_CProveERPs() As CProveERPs
            Dim oCProveERPs As CProveERPs
            Authenticate()
            oCProveERPs = New CProveERPs(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oCProveERPs
        End Function
        Public Function Get_Errores() As Errores
            Dim oErrores As Errores
            Authenticate()
            oErrores = New Errores(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oErrores
        End Function
        Public Function Load_AdjuntosDefecto(ByVal lCiaComp As Long, ByVal Solicitud As Long, ByVal CampoPadre As Long, Optional ByVal UserCode As String = "", Optional ByVal UserPassword As String = "") As DataSet
            Return DBServer.Campo_Load_AdjuntosDefecto(lCiaComp, Solicitud, CampoPadre, UserCode, UserPassword)
        End Function
        Private mImpersonateLogin As String = ""
        Property ImpersonateLogin() As String
            Get
                Return mImpersonateLogin
            End Get
            Set(ByVal Value As String)
                mImpersonateLogin = Value
            End Set
        End Property
        Private mImpersonatePassword As String = ""
        Property ImpersonatePassword() As String
            Get
                Return mImpersonatePassword
            End Get
            Set(ByVal Value As String)
                mImpersonatePassword = Value
            End Set
        End Property
        Private mImpersonateDominio As String = ""
        Property ImpersonateDominio() As String
            Get
                Return mImpersonateDominio
            End Get
            Set(ByVal Value As String)
                mImpersonateDominio = Value
            End Set

        End Property
        Public Sub Impersonate()
            Authenticate()
            mImpersonateLogin = DBServer.ImpersonateLogin
            mImpersonatePassword = DBServer.ImpersonatePassword
            mImpersonateDominio = DBServer.ImpersonateDominio
        End Sub

#Region "Custom LCX/PPL"
        Public Function Get_FacturasLCX() As LCX_Facturas
            Authenticate()
            Dim oFacts As New LCX_Facturas(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oFacts
        End Function

        Public Function Get_FacturasPPL() As PPL_Facturas
            Authenticate()
            Dim oFacts As New PPL_Facturas(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistId, mIsAuthenticated)
            Return oFacts
        End Function
#End Region

#Region " Security "
        ''' <summary>
        ''' Comprueba si es usuario valido
        ''' </summary>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <returns>Si es usuario valido devuelve un objeto Usuario</returns>
        ''' <remarks>Llamada desde: Page_PreInit; Tiempo maximo:0,2</remarks>
        Public Function Login(ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String) As User
            Dim usercode As String = ""
            Dim userpassword As String = ""
            Dim ciaId As Long = 0
            Dim ciaCode As String = ""
            Dim ciaComp As Long = 0
            Dim Cn As Boolean
            Dim CodProveGS As String = ""

            Dim Valida As Boolean = DBServer.Login(SesionId, IPDir, PersistID, usercode, userpassword, ciaId, ciaCode, ciaComp, Cn, CodProveGS)

            If Not Valida Then
                Return Nothing
            Else

                msUserCode = usercode
                'mUserPassword = userpassword 'La guardamos encriptada con la fecha fija
                mIsAuthenticated = True

                msSesionId = SesionId
                msIPDir = IPDir
                msPersistId = PersistID

                Dim oUser As Fullstep.PMPortalServer.User
                oUser = Get_User()

                oUser.Cod = usercode
                oUser.IdCia = ciaId
                oUser.CodCia = ciaCode
                oUser.CiaComp = ciaComp
                oUser.CodProveGS = CodProveGS

                oUser.AccesoCN = Cn

                Dim currentdomain As AppDomain = Thread.GetDomain
                currentdomain.SetPrincipalPolicy(PrincipalPolicy.UnauthenticatedPrincipal)
                Dim OldPrincipal As IPrincipal = Thread.CurrentPrincipal
                Thread.CurrentPrincipal = oUser
                Try
                    If Not TypeOf OldPrincipal Is User Then
                        currentdomain.SetThreadPrincipal(oUser)
                    End If
                Catch
                End Try
                Return oUser
            End If

        End Function
#End Region
#Region "Constructor"
        Public Sub New()
            ' see if we need to configure remoting at all
            If Len(DB_SERVER) > 0 Then
                ' create and register our custom HTTP channel
                ' that uses the binary formatter
                Dim properties As New Hashtable
                properties("name") = "HttpBinary"

                Dim formatter As New BinaryClientFormatterSinkProvider

                Dim channel As New HttpChannel(properties, formatter, Nothing)
                Try
                    ChannelServices.RegisterChannel(channel)
                Catch
                    'Puede que ya est� redirigido
                End Try

                ' register the data portal types as being remote
                If Len(DB_SERVER) > 0 Then
                    Try
                        RemotingConfiguration.RegisterWellKnownClientType( _
                          GetType(Fullstep.PMPortalDatabaseServer.Root), DB_SERVER)
                    Catch
                        'Puede que ya est� redirigido
                    End Try
                    mRemottingServer = True
                End If

            End If
        End Sub

        Private Function DB_SERVER() As String
            Return ConfigurationManager.AppSettings("DBServer")
        End Function
        Private Function DBServer() As Fullstep.PMPortalDatabaseServer.Root
            If mDBServer Is Nothing Then
                mDBServer = New Fullstep.PMPortalDatabaseServer.Root
            End If
            Return mDBServer
        End Function
        Private Sub Authenticate()
            If Not mIsAuthenticated Then Throw New System.Security.SecurityException("User not authenticated!")
        End Sub
#End Region
    End Class
End Namespace

