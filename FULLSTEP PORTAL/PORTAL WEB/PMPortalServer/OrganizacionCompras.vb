Namespace Fullstep.PMPortalServer

    <Serializable()> _
    Public Class OrganizacionCompras
        Inherits Fullstep.PMPortalServer.Security

        Private msCod As String
        Private msDen As String

        Private moOrganizacionCompras As DataSet

        Public Property Cod() As String
            Get
                Return msCod
            End Get

            Set(ByVal Value As String)
                msCod = Value
            End Set
        End Property

        Public Property Den() As String
            Get
                Return msDen
            End Get

            Set(ByVal Value As String)
                msDen = Value
            End Set
        End Property

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moOrganizacionCompras
            End Get

        End Property

        ''' <summary>
        ''' carga la organizacion
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <remarks>Llamada desde: script\_common\articulosserver.aspx  script\_common\campos.ascx   script\_common\desglose.ascx  script\_common\proveedores.aspx; Tiempo m�ximo: 0,2</remarks>
        Public Sub LoadData(ByVal lCiaComp As Long)
            Authenticate()
            If mRemottingServer Then
                moOrganizacionCompras = DBServer.OrganizacionCompras_Get(lCiaComp, msCod, msSesionId, msIPDir, msPersistID)
            Else
                moOrganizacionCompras = DBServer.OrganizacionCompras_Get(lCiaComp, msCod)
            End If
        End Sub

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

    End Class
End Namespace
