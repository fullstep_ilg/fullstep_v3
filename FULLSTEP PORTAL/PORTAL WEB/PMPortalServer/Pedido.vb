﻿Namespace Fullstep.PMPortalServer
    Public Class Pedido
        Inherits Fullstep.PMPortalServer.Security

        Private mlID As Long
        Private m_sNumPedidoCompleto As String
        Private m_sPeticionario As String
        Private m_dFecha As Date
        Private m_iEstado As Integer


        Private m_lAnyo As Long
        Private m_lNumPedido As Long
        Private m_lNumOrden As Long
        Private m_sProv As String
        Private m_sDenProv As String

        Private m_dImporte As Double
        Private m_dCambio As Double
        Private m_sMoneda As String

        Private m_sIDPeticionario As String

        Private m_sIDReceptor As String
        Private m_sReceptor As String
        Private m_sObservaciones As String
        Private m_sNumPedERP As String
        Private m_iTipo As Integer
        Private m_sDenTipoPedido As String

        Private m_oAtributos As DataTable
        Private m_oAdjuntos As DataTable
        Private m_oLineas As DataTable

        Public Property ID() As Long
            Get
                Return mlID
            End Get

            Set(ByVal Value As Long)
                mlID = Value
            End Set
        End Property

        Public Property Estado() As Integer
            Get
                Return m_iEstado
            End Get

            Set(ByVal Value As Integer)
                m_iEstado = Value
            End Set
        End Property

        Public Property NumPedidoCompleto() As String
            Get
                Return m_sNumPedidoCompleto
            End Get

            Set(ByVal Value As String)
                m_sNumPedidoCompleto = Value
            End Set
        End Property

        Public Property Peticionario() As String
            Get
                Return m_sPeticionario
            End Get
            Set(ByVal Value As String)
                m_sPeticionario = Value
            End Set
        End Property

        Public Property Fecha() As Date
            Get
                Return m_dFecha
            End Get

            Set(ByVal Value As Date)
                m_dFecha = Value
            End Set
        End Property

        Public Property DenTipoPedido() As String
            Get
                Return m_sDenTipoPedido
            End Get

            Set(ByVal Value As String)
                m_sDenTipoPedido = Value
            End Set
        End Property

        Public Property NumPedErp() As String
            Get
                Return m_sNumPedERP
            End Get

            Set(ByVal Value As String)
                m_sNumPedERP = Value
            End Set
        End Property


        ''' <summary>Funcion que devuelve el detalle de un pedido</summary>
        ''' <remarks>
        ''' Llamada desde: detalleFactura.aspx
        ''' Tiempo máximo:0,2 seg</remarks>
        Public Sub LoadDetalle(ByVal lCiaComp As Long, ByVal sIdioma As String)
            Dim oDS As DataSet

            Authenticate()
            oDS = DBServer.Pedido_DevolverDetalle(lCiaComp, mlID, sIdioma)

            If Not oDS.Tables(0).Rows.Count = 0 Then

                m_sNumPedidoCompleto = DBNullToStr(oDS.Tables(0).Rows(0).Item("PEDIDO"))
                m_sPeticionario = DBNullToStr(oDS.Tables(0).Rows(0).Item("PETICIONARIO"))
                m_dFecha = DBNullToStr(oDS.Tables(0).Rows(0).Item("FECHA"))
                m_iEstado = DBNullToStr(oDS.Tables(0).Rows(0).Item("EST"))
                m_sDenTipoPedido = DBNullToStr(oDS.Tables(0).Rows(0).Item("TIPO_PEDIDO"))
                m_sNumPedERP = DBNullToStr(oDS.Tables(0).Rows(0).Item("NUM_PED_ERP"))

            End If

            oDS = Nothing
        End Sub

        ''' <summary>
        ''' Método para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">código de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petición</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo máximo: 0</remarks>
        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace