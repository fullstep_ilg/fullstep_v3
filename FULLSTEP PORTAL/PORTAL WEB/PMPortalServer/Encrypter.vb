Option Explicit On 
Option Strict On

Imports System.Runtime.InteropServices
Namespace Fullstep.PMPortalServer
    Public Class Encrypter
        Public Enum TipoDeUsuario
            Administrador = 1
            Persona = 2

        End Enum
        Private Const ClaveADMpar As String = "pruiop"
        Private Const ClaveADMimp As String = "ljkdag"
        Private Const ClaveUSUpar As String = "agkag�"
        Private Const ClaveUSUimp As String = "h+hlL_"

        ''' <summary>
        ''' Encripta/desencripta datos
        ''' </summary>
        ''' <param name="Data">Dato a encriptar/desencriptar</param>
        ''' <param name="Usu">Persona q encripta/desencripta</param>
        ''' <param name="Encrypting">True encripta/ False desencripta</param>
        ''' <param name="UserType">Tipo de usuario q encripta/desencript</param>
        ''' <param name="Fecha">Fecha para encriptar/desencriptar</param>
        ''' <returns>Datos Encriptados/desencriptados</returns>
        ''' <remarks>Llamada desde: FSPMPage.Page_PreInit, GuardarInstancia.aspx/GuardarSinWorkflow_ProcesarXML ,GuardarInstancia.aspx/GuardarConWorkflow_ProcesarXML 
        ''' ; Tiempo maximo: 0,1sg</remarks>
        Public Shared Function Encrypt(ByVal Data As String, Optional ByVal Usu As String = "", Optional ByVal Encrypting As Boolean = True, Optional ByVal UserType As TipoDeUsuario = TipoDeUsuario.Persona, Optional ByVal Fecha As Date = #3/5/1974#) As String

            Dim Crypt As New FSNCrypt.FSNAES
            Dim diacrypt As Integer
            Dim sKeyString As String

            diacrypt = Fecha.Day

            If diacrypt Mod 2 = 0 Then
                If UserType = TipoDeUsuario.Administrador Then
                    sKeyString = ClaveADMpar
                Else
                    sKeyString = ClaveUSUpar
                End If
            Else
                If UserType = TipoDeUsuario.Administrador Then
                    sKeyString = ClaveADMimp
                Else
                    sKeyString = ClaveUSUimp
                End If
            End If

            If Encrypting Then
                Encrypt = Crypt.EncryptStringToString(Fecha, sKeyString, Data)
            Else
                Encrypt = Crypt.DecryptStringFromString(Fecha, sKeyString, Data)
            End If

            Crypt = Nothing
        End Function

        ''' <summary>
        ''' Encripta/desencripta datos
        ''' </summary>
        ''' <param name="Data">Dato a encriptar/desencriptar</param>
        ''' <param name="Seed">Seed q encripta/desencripta</param>
        ''' <param name="Encrypting">True encripta/ False desencripta</param>
        ''' <param name="Fecha">Fecha para encriptar/desencriptar</param>
        ''' <returns>Datos Encriptados/desencriptados</returns>
        ''' <remarks>Llamada desde: FSPServer/Notificar.vb/EnviarMail
        ''' ; Tiempo maximo: 0,1sg</remarks>
        Public Shared Function EncryptTOD(ByVal Data As String, ByVal Seed As String, Optional ByVal Encrypting As Boolean = True, Optional ByVal Fecha As Date = #3/5/1974#) As String

            Dim Crypt As New FSNCrypt.FSNAES

            If Encrypting Then
                EncryptTOD = Crypt.EncryptStringToString(Fecha, Seed, Data)
            Else
                EncryptTOD = Crypt.DecryptStringFromString(Fecha, Seed, Data)
            End If

            Crypt = Nothing
        End Function

        ''' <summary>
        ''' Encripta/desencripta datos como si fuera FSNWEB
        ''' </summary>
        ''' <param name="Data">Dato a encriptar/desencriptar</param>
        ''' <param name="Usu">Persona q encripta/desencripta</param>
        ''' <param name="Encrypting">True encripta/ False desencripta</param>
        ''' <param name="UserType">Tipo de usuario q encripta/desencript</param>
        ''' <param name="Fecha">Fecha para encriptar/desencriptar</param>
        ''' <returns>Datos Encriptados/desencriptados</returns>
        ''' <remarks>Llamada desde: Integracion.ObtenerCredencialesWCF
        ''' ; Tiempo maximo: 0,1sg</remarks>
        Public Shared Function EncryptInt(ByVal Data As String, Optional ByVal Usu As String = "", Optional ByVal Encrypting As Boolean = True, Optional ByVal UserType As TipoDeUsuario = TipoDeUsuario.Persona, Optional ByVal Fecha As Date = #3/5/1974#) As String

            Dim Crypt As New FSNCrypt.FSNAES
            Dim diacrypt As Integer
            Dim sKeyString As String

            diacrypt = Fecha.Day

            If diacrypt Mod 2 = 0 Then
                If UserType = TipoDeUsuario.Administrador Then
                    sKeyString = Usu & ClaveADMpar
                Else
                    sKeyString = Usu & ClaveUSUpar
                End If
            Else
                If UserType = TipoDeUsuario.Administrador Then
                    sKeyString = Usu & ClaveADMimp
                Else
                    sKeyString = Usu & ClaveUSUimp
                End If
            End If

            If Encrypting Then
                EncryptInt = Crypt.EncryptStringToString(Fecha, sKeyString, Data)
            Else
                EncryptInt = Crypt.DecryptStringFromString(Fecha, sKeyString, Data)
            End If

            Crypt = Nothing
        End Function
    End Class
End Namespace