﻿Public Class PPL_Facturas
    Inherits Lista(Of PPL_Factura)

    Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
    End Sub
    ''' <summary>Devuelve las facturas del proveedor indicado</summary>
    ''' <param name="oFiltro">Objeto con las condiciones de filtro</param>
    ''' <returns>Lista de objetos PPL_Factura con los datos de las facturas</returns>
    ''' <remarks>Llamada desde: PPL_Facturas.aspx</remarks>
    Public Function cargarTodasLasFacturas(ByVal oFiltro As Object) As List(Of PPL_Factura)
        Dim facturas As New List(Of PPL_Factura)

        Authenticate()

        Dim dtFacturas As DataTable = mDBServer.getFacturasPPL(oFiltro)
        For Each dr As DataRow In dtFacturas.Rows
            Dim oFac As New PPL_Factura
            oFac.ID = dr("ID")
            oFac.NIF = IIf(dr.IsNull("NIF"), String.Empty, dr("NIF"))
            oFac.NumFactura = IIf(dr.IsNull("NUM_FACTURA"), String.Empty, dr("NUM_FACTURA"))
            If Not dr.IsNull("FECHA") Then oFac.Fecha = dr("FECHA")
            oFac.Sociedad = IIf(dr.IsNull("SOCIEDAD"), String.Empty, dr("SOCIEDAD"))
            oFac.Pedido = IIf(dr.IsNull("PEDIDO"), String.Empty, dr("PEDIDO"))
            If Not dr.IsNull("FECHA_ENTRADA") Then oFac.FechaEntrada = dr("FECHA_ENTRADA")
            If Not dr.IsNull("FECHA_VENCIMIENTO") Then oFac.FechaVencimiento = dr("FECHA_VENCIMIENTO")
            If Not dr.IsNull("FECHA_PAGO") Then oFac.FechaPago = dr("FECHA_PAGO")
            If Not dr.IsNull("NOMINAL") Then oFac.Nominal = Convert.ToDouble(dr("NOMINAL"))
            If Not dr.IsNull("IMPUESTOS") Then oFac.Impuestos = Convert.ToDouble(dr("IMPUESTOS"))
            If Not dr.IsNull("TOTAL") Then oFac.Total = Convert.ToDouble(dr("TOTAL"))
            oFac.Reparto = IIf(dr.IsNull("REPARTO"), String.Empty, dr("REPARTO"))
            oFac.Lote = IIf(dr.IsNull("LOTE"), String.Empty, dr("LOTE"))
            If Not dr.IsNull("FECHA_ESTADO") Then oFac.FechaEstado = dr("FECHA_ESTADO")
            oFac.IdEstadoWebProveedor = dr("ID_ESTADO")
            oFac.EstadoWebProveedorDen = dr("ESTADO_WEB_PROVEEDOR_DEN")

            facturas.Add(oFac)
        Next
        Return facturas
    End Function
    ''' <summary>Devuelve un array con la lista de estados de facturas</summary>
    ''' <returns>Lista con los estados de las facturas</returns>
    ''' <remarks>Llamada desde: PPL_Facturas.aspx</remarks>
    Public Function devolverEstadosFacturas(ByVal sIdioma As String) As List(Of Object)
        Dim oEstados As New List(Of Object)

        Authenticate()

        Dim dtEstados As DataTable = mDBServer.getEstadosFacturasPPL(sIdioma)
        For Each dr As DataRow In dtEstados.Rows
            oEstados.Add(New With {.Id = dr("ID"), .Den = dr("DEN")})
        Next

        Return oEstados
    End Function
    ''' <summary>Devuleve las sociedades a las que el proveedor ha emitido facturas</summary>
    ''' <param name="sNIF">NIF del proveedor</param>
    ''' <returns>Lista con las sociedades</returns>
    ''' <remarks>llamada desde: PPL_Facturas.aspx</remarks>
    Public Function devolverSociedades(ByVal sNIF As String) As List(Of String)
        Dim oSociedades As New List(Of String)

        Authenticate()

        Dim dtSociedades As DataTable = mDBServer.getSociedadesPPL(sNIF)
        For Each dr As DataRow In dtSociedades.Rows
            oSociedades.Add(dr("SOCIEDAD"))
        Next

        Return oSociedades
    End Function
End Class
