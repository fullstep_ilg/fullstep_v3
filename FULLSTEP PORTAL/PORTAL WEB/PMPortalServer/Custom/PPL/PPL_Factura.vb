﻿Public Class PPL_Factura
    Public Sub New()
        MyBase.New()

    End Sub

    Public Property ID As Integer
    Public Property NIF As String
    Public Property NumFactura As String
    Public Property Fecha As Nullable(Of Date)
    Public Property Sociedad As String
    Public Property Pedido As String
    Public Property FechaEntrada As Nullable(Of Date)
    Public Property FechaVencimiento As Nullable(Of Date)
    Public Property FechaPago As Nullable(Of Date)
    Public Property Nominal As Nullable(Of Double)
    Public Property Impuestos As Nullable(Of Double)
    Public Property Total As Nullable(Of Double)
    Public Property Reparto As String
    Public Property Lote As String
    Public Property FechaEstado As Nullable(Of Date)
    Public Property IdEstadoWebProveedor As Integer
    Public Property EstadoWebProveedorDen As String
End Class
