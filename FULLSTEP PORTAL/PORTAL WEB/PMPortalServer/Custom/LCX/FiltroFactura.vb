﻿Imports System.Text.RegularExpressions
Imports Fullstep.PMPortalDatabaseServer.TiposDeDatos

Public Class FiltroFactura
    Public idioma As String
    Public fechaDesde As Date
    Public fechaHasta As Date
    Public fechaAbonoDesde As Date = Nothing
    Public fechaAbonoHasta As Date = Nothing
    Public nif_prove As String
    Public nif_empresa As String
    Public idSituacion As Integer = Nothing
    Public m_sidClave As String
    Public numFactura As String
    Public importeDesde As Double = Nothing
    Public importeHasta As Double = Nothing
    Private sEstado As String
    Private _estado As LCX_EstadoFactura
    Public misFacturas As Boolean
    Public suplidos As Boolean

    Public Sub New()
        fechaDesde = Today.AddMonths(-13)
        fechaHasta = Today
        _estado = New LCX_EstadoFactura()
    End Sub

    Public Property idClave As String
        Get
            If Me.claveCompleta Then
                Return m_sidClave
            Else
                Return m_sidClave.Replace("*", "%")
            End If
        End Get
        Set(value As String)
            m_sidClave = value
        End Set
    End Property
    Public ReadOnly Property claveCompleta As Boolean
        Get
            If m_sidClave Is Nothing Then
                Return True
            Else
                If m_sidClave(0) = "%" Or m_sidClave(m_sidClave.Length - 1) = "%" Or m_sidClave(0) = "*" Or m_sidClave(m_sidClave.Length - 1) = "*" Then
                    Return False
                End If
                Return True
            End If
        End Get
    End Property

    Public Property Estado() As LCX_EstadoFactura
        Get
            Return _estado
        End Get
        Set(value As LCX_EstadoFactura)
            _estado = value
        End Set
    End Property


End Class
