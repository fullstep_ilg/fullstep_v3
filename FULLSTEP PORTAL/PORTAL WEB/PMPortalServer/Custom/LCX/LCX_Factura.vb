﻿Imports Fullstep.PMPortalDatabaseServer.TiposDeDatos

Public Class LCX_Factura

    Private _ID As Long

    Private _NIF_EMISOR As String

    Private _NOM_PROVE As String

    Private _NUM_FACT As String

    Private _ID_EMP As System.Nullable(Of Integer)

    Private _NIF_EMP As String

    Private _NOM_EMP As String

    Private _FECHA As System.Nullable(Of Date)

    Private _FECHA_REGISTRO As System.Nullable(Of Date)

    Private _ID_CLAVE As String

    Private _IMPORTE As System.Nullable(Of Double)

    Private _ESTADO As LCX_EstadoFactura

    Private _SITUACION As String

    Private _FECHA_ABONO As System.Nullable(Of Date)

    Private _CUENTA_ABONO As String

    Private _CANAL As String

    Private _FECACT As Date

    Private _SUPLIDO As String

    Public Sub New()
        MyBase.New()
    End Sub

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property
    Public Property NIF_EMISOR() As String
        Get
            Return Me._NIF_EMISOR
        End Get
        Set(value As String)
            If (String.Equals(Me._NIF_EMISOR, value) = False) Then
                Me._NIF_EMISOR = value
            End If
        End Set
    End Property

    Public Property NOM_PROVE() As String
        Get
            Return Me._NOM_PROVE
        End Get
        Set(value As String)
            If (String.Equals(Me._NOM_PROVE, value) = False) Then
                Me._NOM_PROVE = value
            End If
        End Set
    End Property

    Public Property NUM_FACT() As String
        Get
            Return Me._NUM_FACT
        End Get
        Set(value As String)
            If (String.Equals(Me._NUM_FACT, value) = False) Then
                Me._NUM_FACT = value
            End If
        End Set
    End Property

    Public Property ID_EMP() As System.Nullable(Of Integer)
        Get
            Return Me._ID_EMP
        End Get
        Set(value As System.Nullable(Of Integer))
            If (Me._ID_EMP.Equals(value) = False) Then
                Me._ID_EMP = value
            End If
        End Set
    End Property

    Public Property NIF_EMP() As String
        Get
            Return Me._NIF_EMP
        End Get
        Set(value As String)
            If (String.Equals(Me._NIF_EMP, value) = False) Then
                Me._NIF_EMP = value
            End If
        End Set
    End Property

    Public Property NOM_EMP() As String
        Get
            Return Me._NOM_EMP
        End Get
        Set(value As String)
            If (String.Equals(Me._NOM_EMP, value) = False) Then
                Me._NOM_EMP = value
            End If
        End Set
    End Property

    Public Property FECHA() As System.Nullable(Of Date)
        Get
            Return Me._FECHA
        End Get
        Set(value As System.Nullable(Of Date))
            If (Me._FECHA.Equals(value) = False) Then
                Me._FECHA = value
            End If
        End Set
    End Property

    Public Property FECHA_REGISTRO() As System.Nullable(Of Date)
        Get
            Return Me._FECHA_REGISTRO
        End Get
        Set(value As System.Nullable(Of Date))
            If (Me._FECHA_REGISTRO.Equals(value) = False) Then
                Me._FECHA_REGISTRO = value
            End If
        End Set
    End Property

    Public Property CLAVE() As String
        Get
            Return Me._ID_CLAVE
        End Get
        Set(value As String)
            If (String.Equals(Me._ID_CLAVE, value) = False) Then
                Me._ID_CLAVE = value
            End If
        End Set
    End Property

    Public Property IMPORTE() As System.Nullable(Of Double)
        Get
            Return Me._IMPORTE
        End Get
        Set(value As System.Nullable(Of Double))
            If (Me._IMPORTE.Equals(value) = False) Then
                Me._IMPORTE = value
            End If
        End Set
    End Property

    Public Property Estado As LCX_EstadoFactura
        Get
            Return _ESTADO
        End Get
        Set(value As LCX_EstadoFactura)
            _ESTADO = value

        End Set
    End Property

    Public Property EstadoVisor As lcx_estado_factura_visor
        Get
            Select Case _ESTADO.EstadoReal
                Case lcx_estado_factura.Registrada Or lcx_estado_factura.Pendiente
                    Return lcx_estado_factura_visor.pendiente
                Case lcx_estado_factura.Anulada Or lcx_estado_factura.Retroceso
                    Return lcx_estado_factura_visor.anulada
                Case lcx_estado_factura.Registrada
                    Return lcx_estado_factura_visor.pendiente
                Case Else
                    Return lcx_estado_factura_visor.pendiente
            End Select
        End Get
        Set(value As lcx_estado_factura_visor)
            _ESTADO.EstadoVisor = value
        End Set
    End Property

    Public Property SITUACION() As String
        Get
            Return Me.Estado.Denominacion
        End Get
        Set(value As String)
            Me.Estado.Denominacion = value
        End Set
    End Property

    Public Property FECHA_ABONO() As System.Nullable(Of Date)
        Get
            Return Me._FECHA_ABONO
        End Get
        Set(value As System.Nullable(Of Date))
            If (Me._FECHA_ABONO.Equals(value) = False) Then
                Me._FECHA_ABONO = value
            End If
        End Set
    End Property

    Public Property CUENTA_ABONO() As String
        Get
            Return Me._CUENTA_ABONO
        End Get
        Set(value As String)
            If (String.Equals(Me._CUENTA_ABONO, value) = False) Then
                Me._CUENTA_ABONO = value
            End If
        End Set
    End Property

    Public Property CANAL() As String
        Get
            Return Me._CANAL
        End Get
        Set(value As String)
            If (String.Equals(Me._CANAL, value) = False) Then
                Me._CANAL = value
            End If
        End Set
    End Property

    Public Property FECHAACT() As System.Nullable(Of Date)
        Get
            Return Me._FECACT
        End Get
        Set(value As System.Nullable(Of Date))
            If (Me._FECACT.Equals(value) = False) Then
                Me._FECACT = value
            End If
        End Set
    End Property

    Public Property SUPLIDO() As String
        Get
            Return Me._SUPLIDO
        End Get
        Set(value As String)
            If (String.Equals(Me._SUPLIDO, value) = False) Then
                Me._SUPLIDO = value
            End If
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return Me.NUM_FACT & " " & Me.FECHA & " " & Me.NOM_EMP
    End Function
End Class
