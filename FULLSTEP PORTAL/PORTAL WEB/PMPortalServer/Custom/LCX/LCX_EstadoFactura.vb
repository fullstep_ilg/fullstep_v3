﻿Imports Fullstep.PMPortalDatabaseServer.TiposDeDatos

Public Class LCX_EstadoFactura
    Private _EstadoReal As lcx_estado_factura
    Private _EstadoVisor As lcx_Estado_factura_visor
    Public Property Denominacion As String

    Public Sub New()
        _EstadoReal = 0
    End Sub

    Public Sub New(e As lcx_estado_factura, den As String)
        _EstadoReal = e
        Denominacion = den
    End Sub

    Public Property EstadoVisor As lcx_Estado_factura_visor
        Get
            Return _EstadoVisor
        End Get
        Set(value As lcx_Estado_factura_visor)
            _EstadoVisor = value
        End Set
    End Property

    Public Property EstadoReal As lcx_estado_factura
        Get
            Return _EstadoReal
        End Get
        Set(value As lcx_estado_factura)
            _EstadoReal = value
            Select Case _EstadoReal
                Case lcx_estado_factura.Registrada Or lcx_estado_factura.Pendiente
                    _EstadoVisor = lcx_Estado_factura_visor.Pendiente
                Case lcx_estado_factura.Anulada Or lcx_estado_factura.Retroceso
                    _EstadoVisor = lcx_Estado_factura_visor.Anulada
                Case lcx_estado_factura.Registrada
                    _EstadoVisor = lcx_Estado_factura_visor.Pendiente
                Case Else
                    _EstadoVisor = lcx_Estado_factura_visor.Pendiente
            End Select
        End Set
    End Property

    Public Overrides Function ToString() As String
        Dim lista As String = Nothing
        If _EstadoReal = 0 Then
            Select Case _EstadoVisor
                Case lcx_Estado_factura_visor.Pendiente
                    lista = lcx_estado_factura.Registrada & "," & lcx_estado_factura.Pendiente
                Case lcx_Estado_factura_visor.Anulada
                    lista = lcx_estado_factura.Anulada & "," & lcx_estado_factura.Retroceso
                Case lcx_Estado_factura_visor.Pagada
                    lista = lcx_estado_factura.Pagada
            End Select
        Else
            lista = _EstadoReal
        End If
        Return lista
    End Function
End Class
