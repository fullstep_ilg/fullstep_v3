﻿Public Class LCX_Facturas
    Inherits Lista(Of LCX_Factura)

    Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
        MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
    End Sub
    Public Function cargarTodasLasFacturas(filtro As filtroFactura) As List(Of LCX_Factura)
        Dim dtFacturas As DataTable
        Dim facturas As New List(Of LCX_Factura)

        If Not filtro.suplidos And Not filtro.misFacturas Then
            Return facturas
        End If

        dtFacturas = mDBServer.getAllFacturasLCX(filtro.idioma, filtro.nif_prove, filtro.nif_empresa, filtro.numFactura, filtro.fechaDesde, filtro.fechaHasta, filtro.fechaAbonoDesde, filtro.fechaAbonoHasta, filtro.importeDesde, filtro.importeHasta, filtro.Estado.ToString, filtro.idClave, filtro.claveCompleta, filtro.suplidos, filtro.misFacturas)
        For Each dr As DataRow In dtFacturas.Rows
            Dim f As New LCX_Factura
            f.ID = dr("ID")
            f.NIF_EMISOR = dr("NIF_EMISOR")
            f.SUPLIDO = IIf(IsDBNull(dr("SUPLIDO")), String.Empty, dr("SUPLIDO"))
            f.NOM_PROVE = dr("NOM_PROVE")
            f.NUM_FACT = dr("NUM_FACT")
            f.ID_EMP = dr("ID_EMP")
            f.NIF_EMP = dr("NIF_EMP")
            f.NOM_EMP = dr("NOM_EMP")
            f.FECHA = dr("FECHA")
            f.FECHA_REGISTRO = dr("FECHA_REGISTRO")
            f.CLAVE = dr("ID_CLAVE")
            f.IMPORTE = dr("IMPORTE")
            f.Estado = New LCX_EstadoFactura(dr("ID_SITUACION"), dr("DEN_SITUACION"))
            'f.Estado.EstadoReal = dr("ID_SITUACION")
            'f.SITUACION = dr("DEN_SITUACION")
            f.FECHA_ABONO = dr("FECHA_ABONO")
            f.CUENTA_ABONO = IIf(IsDBNull(dr("CUENTA_ABONO")), "", dr("CUENTA_ABONO"))
            f.CANAL = dr("CANAL")
            f.FECHAACT = dr("FECACT")

            facturas.Add(f)
        Next
        Return facturas
    End Function
    Public Function haySuplidos(oFiltro As FiltroFactura) As Boolean
        Dim dtFacturas As DataTable
        Dim bSuplidoEncontrado As Boolean = False

        dtFacturas = mDBServer.getAllFacturasLCX(oFiltro.idioma, oFiltro.nif_prove, oFiltro.nif_empresa, oFiltro.numFactura, oFiltro.fechaDesde, oFiltro.fechaHasta, oFiltro.fechaAbonoDesde, oFiltro.fechaAbonoHasta, oFiltro.importeDesde, oFiltro.importeHasta, oFiltro.Estado.ToString, oFiltro.idClave, oFiltro.claveCompleta, True, False)
        For Each oLinea As DataRow In dtFacturas.Rows
            If Not IsDBNull(oLinea("SUPLIDO")) And Not bSuplidoEncontrado Then
                If Not oLinea("SUPLIDO") = String.Empty Then
                    bSuplidoEncontrado = True
                End If
            End If
        Next

        Return bSuplidoEncontrado
    End Function
    Public Function getEstadosFactura(idi As String) As List(Of LCX_EstadoFactura)
        Dim dt As DataTable
        Dim estados As New List(Of LCX_EstadoFactura)
        dt = mDBServer.getEstadosFacturaLCX(idi)
        For Each dr As DataRow In dt.Rows
            estados.Add(New LCX_EstadoFactura(dr("ID"), dr("DEN")))
        Next
        Return estados
    End Function
    Public Overrides Function toString() As String
        Dim str As String = ""
        For Each o As LCX_Factura In Me
            str = str & o.ToString & vbCrLf
        Next
        Return str
    End Function
End Class