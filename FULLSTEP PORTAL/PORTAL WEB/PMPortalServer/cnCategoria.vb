﻿Public Class cnCategoria
    Private _Id As Integer
    Public Property Id() As Integer
        Get
            Return _Id
        End Get
        Set(ByVal value As Integer)
            _Id = value
        End Set
    End Property
    Private _Nivel As Integer
    Public Property Nivel() As Integer
        Get
            Return _Nivel
        End Get
        Set(ByVal value As Integer)
            _Nivel = value
        End Set
    End Property
    Private _CategoriaPadre As Nullable(Of Integer)
    Public Property CategoriaPadre() As Nullable(Of Integer)
        Get
            Return _CategoriaPadre
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _CategoriaPadre = value
        End Set
    End Property
    Private _Denominacion As String
    Public Property Denominacion() As String
        Get
            Return _Denominacion
        End Get
        Set(ByVal value As String)
            _Denominacion = value
        End Set
    End Property
End Class
