Namespace Fullstep.PMPortalServer

    <Serializable()> _
    Public Class Solicitud
        Inherits Security
        Private mlID As Long
        Private msCod As String
        Private moDen As MultiIdioma
        Private moDescr As MultiIdioma
        Private miTipo As Integer
        Private msGestor As String
        Private msNombreGestor As String
        Private mlFormulario As Long
        Private mlWorkflow As Long
        Private mbPub As Boolean
        Private mdPedidoDirecto As Double
        Private mbBaja As Boolean
        Private moFormulario As Formulario
        Private mdsAdjuntos As DataTable
        Private msDenTipo As String
        Private miTipoSolicit As Integer
        Private miBloque As Integer

        Public Property dsAdjuntos() As DataTable
            Get
                dsAdjuntos = mdsAdjuntos
            End Get
            Set(ByVal Value As DataTable)
                mdsAdjuntos = Value
            End Set
        End Property

        Public Property ID() As Long
            Get
                Return mlID
            End Get

            Set(ByVal Value As Long)
                mlID = Value
            End Set
        End Property

        Public Property Codigo() As String
            Get
                Return msCod
            End Get
            Set(ByVal Value As String)
                msCod = Value
            End Set
        End Property

        Property Den(ByVal Idioma As String) As String
            Get
                Den = moDen(Idioma)

            End Get
            Set(ByVal Value As String)
                If Not moDen.Contains(Idioma) Then
                    moDen.Add(Idioma, Value)
                Else
                    moDen(Idioma) = Value
                End If

            End Set
        End Property

        Property Descr(ByVal Idioma As String) As String
            Get
                Descr = moDescr(Idioma)

            End Get
            Set(ByVal Value As String)
                If Not moDescr.Contains(Idioma) Then
                    moDescr.Add(Idioma, Value)
                Else
                    moDescr(Idioma) = Value
                End If

            End Set
        End Property

        Public Property DenTipo() As String
            Get
                DenTipo = msDenTipo
            End Get
            Set(ByVal Value As String)
                msDenTipo = Value
            End Set
        End Property

        Public Property Tipo() As Integer
            Get
                Tipo = miTipo
            End Get
            Set(ByVal Value As Integer)
                miTipo = Value
            End Set
        End Property
        Public Property Gestor() As String
            Get
                Gestor = msGestor
            End Get
            Set(ByVal Value As String)
                msGestor = Value
            End Set
        End Property

        Public Property NombreGestor() As String
            Get
                NombreGestor = msNombreGestor
            End Get
            Set(ByVal Value As String)
                msNombreGestor = Value
            End Set
        End Property
        Public Property Formulario() As Formulario
            Get
                Formulario = moFormulario
            End Get
            Set(ByVal Value As Formulario)
                moFormulario = Value
            End Set
        End Property
        Public Property Workflow() As Long
            Get
                Workflow = mlWorkflow
            End Get
            Set(ByVal Value As Long)
                mlWorkflow = Value
            End Set
        End Property
        Public Property Pub() As Boolean
            Get
                Pub = mbPub
            End Get
            Set(ByVal Value As Boolean)
                mbPub = Value
            End Set
        End Property
        Public Property PedidoDirecto() As Double
            Get
                Return mdPedidoDirecto

            End Get
            Set(ByVal Value As Double)
                mdPedidoDirecto = Value

            End Set
        End Property
        Public ReadOnly Property Baja() As Boolean
            Get
                Baja = mbBaja
            End Get
        End Property
        Public Property BloqueActual() As Integer
            Get
                BloqueActual = miBloque
            End Get
            Set(ByVal Value As Integer)
                miBloque = Value
            End Set
        End Property
        Public Property TipoSolicit() As Integer
            Get
                TipoSolicit = miTipoSolicit
            End Get
            Set(ByVal Value As Integer)
                miTipoSolicit = Value
            End Set
        End Property

        ''' <summary>
        ''' Carga de una solicitud
        ''' </summary>
        ''' <param name="lCiaComp">Codigo de compa�ia</param>
        ''' <param name="sIdi">Idioma</param>        
        ''' <remarks>Llamada desde: Hay 22 aspx en la aplicaci�n q lo usan; Tiempo m�ximo: 0,1</remarks>
        Public Sub Load(ByVal lCiaComp As Long, ByVal sIdi As String, Optional ByVal bPet As Boolean = False)
            Authenticate()
            Dim data As DataSet
            If mRemottingServer Then
                data = DBServer.Solicitud_Load(lCiaComp, mlID, sIdi, bPet, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Solicitud_Load(lCiaComp, mlID, sIdi, bPet)
            End If

            If Not data.Tables(0).Rows.Count = 0 Then
                With data.Tables(0).Rows(0)
                    msCod = .Item("COD").ToString
                    moDen(sIdi) = .Item("DEN_" & sIdi).ToString
                    moDescr(sIdi) = data.Tables(0).Rows(0).Item("DESCR_" & sIdi).ToString

                    miTipo = data.Tables(0).Rows(0).Item("TIPO").ToString
                    msDenTipo = DBNullToSomething(.Item("DENTIPO"))
                    miTipoSolicit = .Item("TIPO_SOLICIT").ToString
                    msGestor = .Item("GESTOR").ToString
                    msNombreGestor = .Item("NOM_GESTOR").ToString
                    mlFormulario = DBNullToSomething(.Item("FORMULARIO"))
                    mlWorkflow = DBNullToSomething(.Item("WORKFLOW"))
                    mbPub = (DBNullToSomething(.Item("PUB")) = 1)
                    mdPedidoDirecto = DBNullToSomething(.Item("PEDIDO_DIRECTO"))
                    mbBaja = (.Item("BAJA") = 1)

                    miBloque = DBNullToSomething(.Item("BLOQUE"))
                End With

                moFormulario = New Formulario(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
                moFormulario.Id = mlFormulario
                mdsAdjuntos = New DataTable

                mdsAdjuntos = data.Tables(1)
            End If
            data = Nothing
        End Sub
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
            moDen = New MultiIdioma
            moDescr = New MultiIdioma
        End Sub

        ''' <summary>
        ''' Completa el Dataset con toda la informaci�n de la pantalla de donde se graba con la informaci�n de los
        ''' campos ocultos o de solo lectura q el jsalta.js/montarformulariosubmit no tiene pq haber recogido.
        ''' </summary>
        ''' <param name="lCiaComp">compania</param>
        ''' <param name="ds">Dataset con toda la informaci�n de la pantalla de donde se graba</param>
        ''' <returns>Devuelve el objeto dataset generado</returns>
        ''' <remarks>Llamada desde: script/_common/guardarinstancia.aspx/GuardarSinWorkflow
        '''                 script/_common/guardarinstancia.aspx/GuardarConWorkflow
        '''     ;Tiempo m�ximo: 0,1</remarks>
        Public Function CompletarFormulario(ByVal lCiaComp As Long, ByVal ds As DataSet, Optional ByVal bNuevoWorkflow As Boolean = False, Optional ByVal bMaper As Boolean = False, Optional ByVal sProve As String = Nothing, Optional ByVal idUsu As Integer = Nothing) As DataSet
            Dim dsInvisibles As DataSet

            If mRemottingServer Then
                dsInvisibles = DBServer.Solicitud_LoadInvisibles(lCiaComp, mlID, bNuevoWorkflow, sProve, idUsu, msSesionId, msIPDir, msPersistID)
            Else
                dsInvisibles = DBServer.Solicitud_LoadInvisibles(lCiaComp, mlID, bNuevoWorkflow, sProve, idUsu)
            End If
            Dim dRow As DataRow
            Dim dNewRow As DataRow
            Dim dRows() As DataRow
            Dim dRowsAdjun() As DataRow

            Dim bNoAdjQaPop As Boolean
            Dim bPadreNoVisibleHijoVa As Boolean = False

            'METEMOS LOS CAMPOS SIMPLES QUE ESTUVIERAN OCULTOS JUNTO CON LOS VALORES
            For Each dRow In dsInvisibles.Tables(0).Rows
                dNewRow = ds.Tables("TEMP").NewRow
                dNewRow.Item("CAMPO") = dRow.Item("ID")
                dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                Try
                    dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                    dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                    dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                Catch ex As Exception
                End Try

                dNewRow.Item("ES_SUBCAMPO") = 0
                dNewRow.Item("SUBTIPO") = dRow.Item("SUBTIPO")
                dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")
                If bMaper Then
                    dNewRow.Item("GRUPO") = dRow.Item("GRUPO")
                    dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
                    dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
                End If
                Try
                    ds.Tables("TEMP").Rows.Add(dNewRow)
                Catch ex As Exception
                End Try
            Next

            'METEMOS LOS ADJUNTOS DE LOS CAMPOS SIMPLES OCULTOS
            If Not (ds.Tables("TEMPADJUN") Is Nothing) Then
                For Each dRow In dsInvisibles.Tables(5).Rows
                    If ds.Tables("TEMPADJUN").Select("CAMPO=" + dRow.Item("CAMPO").ToString + " AND ID=" + dRow.Item("ID").ToString).Length = 0 Then
                        dNewRow = ds.Tables("TEMPADJUN").NewRow
                        dNewRow.Item("CAMPO") = dRow.Item("CAMPO")
                        dNewRow.Item("TIPO") = dRow.Item("TIPO")
                        dNewRow.Item("ID") = dRow.Item("ID")

                        Try
                            ds.Tables("TEMPADJUN").Rows.Add(dNewRow)
                        Catch ex As Exception
                        End Try
                    End If
                Next
            End If

            'METEMOS LOS CAMPOS DE DESGLOSE QUE ESTUVIERAN OCULTOS
            For Each dRow In dsInvisibles.Tables(1).Rows
                If ds.Tables("TEMP").Select("CAMPO=" & dRow.Item("CAMPO_HIJO")).Length = 0 Then
                    dNewRow = ds.Tables("TEMP").NewRow
                    dNewRow.Item("CAMPO") = dRow.Item("CAMPO_HIJO")
                    dNewRow.Item("ES_SUBCAMPO") = 1
                    Try
                        ds.Tables("TEMP").Rows.Add(dNewRow)
                    Catch ex As Exception
                    End Try

                End If

            Next
            Dim iLinea As Integer

            For Each dRow In dsInvisibles.Tables(1).Rows
                If dRow.Item("VISIBLE") = 0 Then 'en este datatable vienen los visibles y los invisibles. Solo se deben a�adir los datos de los invisibles
                    ' Este IF comprueba que este campo invisible pertenece a un desglose con alg�n campo visible
                    If UBound(dsInvisibles.Tables(1).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND VISIBLE = 1")) > 0 Then
                        'Si hay alg�n campo visible en este desglose habr� que obtener que nuevo n�mero de l�nea tiene ahora

                        dRows = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)
                        If dRows.Length > 0 Then 'si fuera igual a 0 indicar�a que se ha borrado esa l�nea y por tanto no hay que guardarlo
                            iLinea = dRows(0).Item("LINEA")
                            'comprobamos que no exista ya que los desgloses de popup si que se cargan aunque est� invisible
                            If ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString).Length = 0 Then
                                dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                                dNewRow.Item("LINEA") = iLinea
                                dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                                dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                                If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                                    dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                                End If
                                Try
                                    dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                                    dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                                    dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                                Catch ex As Exception
                                End Try

                                dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                                dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")
                                If bMaper Then
                                    dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
                                    dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
                                End If
                                Try
                                    'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
                                    'Intenta meter algo que ya he metido. Solo desglose popup.
                                    ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                                Catch ex As Exception
                                End Try

                                If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
                                    dRowsAdjun = dsInvisibles.Tables(3).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + iLinea.ToString)
                                    If (dRowsAdjun.Length > 0) Then
                                        For Each Adjun As DataRow In dRowsAdjun
                                            dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                                            dNewRow.Item("LINEA") = iLinea
                                            dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                            dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                                            dNewRow.Item("ID") = Adjun.Item("ADJUN")
                                            dNewRow.Item("TIPO") = 3
                                            Try
                                                ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                                            Catch ex As Exception
                                            End Try
                                        Next
                                    End If
                                End If
                            Else
                                'Para que no se cargue CodArticulo =119 en el caso de que el NuevoCodArt este OCULTO y la DENOMINACION visible
                                Dim sql, sql2 As String
                                sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND VISIBLE = 0)"
                                sql = sql + " OR (TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND VISIBLE = 1))"

                                sql2 = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & "OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & ") AND VISIBLE = 1)"

                                If ((DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo)) _
                                 Or ((dsInvisibles.Tables(1).Select(sql).Length < 2) And (DBNullToSomething(dRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo)) Then

                                    dNewRow = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)(0)

                                    'dNewRow.Item("VALOR_TEXT") = IIf(IsDBNull(dNewRow.Item("VALOR_TEXT")), dRow.Item("VALOR_TEXT"), dNewRow.Item("VALOR_TEXT"))
                                    If IsDBNull(dNewRow.Item("VALOR_TEXT")) Then
                                        dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                                    ElseIf dNewRow.Item("VALOR_TEXT") = "nulo" Then
                                        dNewRow.Item("VALOR_TEXT") = System.DBNull.Value
                                    Else
                                        dNewRow.Item("VALOR_TEXT") = dNewRow.Item("VALOR_TEXT")
                                    End If
                                    If (IsDBNull(dNewRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                                        dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                                    End If
                                    Try
                                        dNewRow.Item("VALOR_NUM") = IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))
                                        dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))), DBNullToSomething(IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))))
                                    Catch ex As Exception
                                    End Try
                                    Try
                                        dNewRow.Item("VALOR_FEC") = IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))
                                        dNewRow.Item("VALOR_BOOL") = IIf(IsDBNull(dNewRow.Item("VALOR_BOOL")), dRow.Item("VALOR_BOOL"), dNewRow.Item("VALOR_BOOL"))
                                    Catch ex As Exception
                                    End Try

                                    dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")
                                    If bMaper Then
                                        dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
                                        dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
                                    End If
                                End If
                            End If
                        End If
                    Else ' al no haber campos visibles de este desglose habr� que a�adirlo tal y como viene
                        bNoAdjQaPop = False
                        If Not bNuevoWorkflow Then
                            If ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString).Length = 0 Then
                            Else
                                bNoAdjQaPop = True
                            End If
                        End If

                        dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                        dNewRow.Item("LINEA") = dRow.Item("LINEA")
                        dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                        dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                        dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                        If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                            dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                        End If
                        Try
                            dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                            dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                            dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                        Catch ex As Exception
                        End Try

                        dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                        dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")
                        If bMaper Then
                            dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
                            dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
                        End If

                        Try
                            'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
                            'Intenta meter algo que ya he metido. Solo desglose popup.       
                            ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                        Catch ex As Exception
                        End Try

                        If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
                            dRowsAdjun = dsInvisibles.Tables(3).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + iLinea.ToString)
                            If (dRowsAdjun.Length > 0) AndAlso (Not bNoAdjQaPop) Then
                                For Each Adjun As DataRow In dRowsAdjun
                                    dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                                    dNewRow.Item("LINEA") = iLinea
                                    dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                    dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

                                    dNewRow.Item("ID") = Adjun.Item("ADJUN")
                                    dNewRow.Item("TIPO") = 3
                                    Try
                                        ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                                    Catch ex As Exception
                                    End Try
                                Next
                            End If
                        End If
                    End If
                End If
            Next
            '--------------------------------------------------------------
            For Each dRow In dsInvisibles.Tables(2).Rows
                'Si estaba entre los invisibles  el PADRE, ed, el DESGLOSE que mas da PM_CONF_CUMP_BLOQUE
                'del hijo, seguro que no esta por GenerarDataSet y habra que meterlo
                dRows = dsInvisibles.Tables(0).Select("ID=" + dRow.Item("CAMPO_PADRE").ToString)
                bPadreNoVisibleHijoVa = (dRows.Length > 0)
                ''
                If bPadreNoVisibleHijoVa Then
                    dRows = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)
                    If dRows.Length > 0 Then 'si fuera igual a 0 indicar�a que se ha borrado esa l�nea y por tanto no hay que guardarlo
                        iLinea = dRows(0).Item("LINEA")
                        'comprobamos que no exista ya que los desgloses de popup si que se cargan aunque est� invisible
                        If ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString).Length = 0 Then

                            dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                            dNewRow.Item("LINEA") = iLinea
                            dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                            dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                            dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                            If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                                dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                            End If
                            Try
                                dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                                dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                                dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                            Catch ex As Exception
                            End Try
                            dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                            dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")
                            If bMaper Then
                                dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
                                dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
                            End If
                            Try
                                'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
                                'Intenta meter algo que ya he metido. Solo desglose popup.
                                ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                            Catch ex As Exception
                            End Try

                            If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
                                dRowsAdjun = dsInvisibles.Tables(4).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + iLinea.ToString)
                                If (dRowsAdjun.Length > 0) Then
                                    For Each Adjun As DataRow In dRowsAdjun
                                        dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                                        dNewRow.Item("LINEA") = iLinea
                                        dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                        dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

                                        dNewRow.Item("ID") = Adjun.Item("ADJUN")
                                        dNewRow.Item("TIPO") = 3
                                        Try
                                            ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                                        Catch ex As Exception
                                        End Try
                                    Next
                                End If
                            End If
                        Else
                            'Para que no se cargue CodArticulo =119 en el caso de que el NuevoCodArt este OCULTO y la DENOMINACION visible
                            Dim sql, sql2 As String
                            sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString
                            sql = sql + " AND CAMPO_HIJO = " + dRow.Item("CAMPO_HIJO").ToString
                            sql = sql + " AND LINEA = " + iLinea.ToString
                            dRows = dsInvisibles.Tables(1).Select(sql)

                            sql = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND VISIBLE = 0)"
                            sql = sql + " OR (TIPO_CAMPO_GS =  " & TiposDeDatos.TipoCampoGS.DenArticulo & " AND VISIBLE = 1))"

                            sql2 = "CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND LINEA = " + iLinea.ToString + " AND ((TIPO_CAMPO_GS= " & TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.DenArticulo & "OR TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.CodArticulo & ") AND VISIBLE = 1)"

                            If dRows.Length > 0 Then
                                If ((DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.NuevoCodArticulo) And (DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Centro)) _
                            Or ((dsInvisibles.Tables(1).Select(sql).Length < 2) And (DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo)) _
                            Or ((DBNullToSomething(dRows(0).Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Centro) And (dsInvisibles.Tables(1).Select(sql2).Length = 0)) Then
                                    dNewRow = ds.Tables("TEMPDESGLOSE").Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA_OLD = " + dRow.Item("LINEA").ToString)(0)

                                    'dNewRow.Item("VALOR_TEXT") tendr� el valor "nulo" cuando en el desglose se muestre solamente la denominacion el articulo no sea generico
                                    'y introducimos una nueva denominacion
                                    If IsDBNull(dNewRow.Item("VALOR_TEXT")) Then
                                        dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                                    ElseIf dNewRow.Item("VALOR_TEXT") = "nulo" Then
                                        dNewRow.Item("VALOR_TEXT") = System.DBNull.Value
                                    Else
                                        dNewRow.Item("VALOR_TEXT") = dNewRow.Item("VALOR_TEXT")
                                    End If
                                    If (IsDBNull(dNewRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                                        dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                                    End If
                                    Try
                                        dNewRow.Item("VALOR_NUM") = IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))
                                        dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(IIf(IsDBNull(dNewRow.Item("VALOR_NUM")), dRow.Item("VALOR_NUM"), dNewRow.Item("VALOR_NUM"))), DBNullToSomething(IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))))
                                    Catch ex As Exception
                                    End Try
                                    Try
                                        dNewRow.Item("VALOR_FEC") = IIf(IsDBNull(dNewRow.Item("VALOR_FEC")), dRow.Item("VALOR_FEC"), dNewRow.Item("VALOR_FEC"))
                                        dNewRow.Item("VALOR_BOOL") = IIf(IsDBNull(dNewRow.Item("VALOR_BOOL")), dRow.Item("VALOR_BOOL"), dNewRow.Item("VALOR_BOOL"))
                                    Catch ex As Exception
                                    End Try

                                    dNewRow.Item("TIPOGS") = dRows(0).Item("TIPO_CAMPO_GS")
                                    If bMaper Then
                                        dNewRow.Item("IDATRIB") = dRows(0).Item("ID_ATRIB_GS")
                                        dNewRow.Item("VALERP") = dRows(0).Item("VALIDACION_ERP")
                                    End If
                                End If
                            End If
                        End If
                    Else ' al no haber campos visibles de este desglose habr� que a�adirlo tal y como viene
                        dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                        dNewRow.Item("LINEA") = dRow.Item("LINEA")
                        dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                        dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")
                        dNewRow.Item("VALOR_TEXT") = dRow.Item("VALOR_TEXT")
                        If (IsDBNull(dRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dRow.Item("LISTA_TEXT"))) Then
                            dNewRow.Item("VALOR_TEXT") = dRow.Item("LISTA_TEXT")
                        End If

                        Try
                            dNewRow.Item("VALOR_NUM") = dRow.Item("VALOR_NUM")
                            dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dRow.Item("VALOR_NUM")), DBNullToSomething(dRow.Item("VALOR_FEC")))
                            dNewRow.Item("VALOR_BOOL") = dRow.Item("VALOR_BOOL")
                        Catch ex As Exception
                        End Try

                        dNewRow.Item("LINEA_OLD") = dRow.Item("LINEA")
                        dNewRow.Item("TIPOGS") = dRow.Item("TIPO_CAMPO_GS")
                        If bMaper Then
                            dNewRow.Item("VALERP") = dRow.Item("VALIDACION_ERP")
                            dNewRow.Item("IDATRIB") = dRow.Item("ID_ATRIB_GS")
                        End If
                        Try
                            'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
                            'Intenta meter algo que ya he metido. Solo desglose popup.       
                            ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                        Catch ex As Exception
                        End Try

                        If Not (ds.Tables("TEMPDESGLOSEADJUN") Is Nothing) Then
                            dRowsAdjun = dsInvisibles.Tables(4).Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO=" + dRow.Item("CAMPO_HIJO").ToString + " AND LINEA = " + dRow.Item("LINEA").ToString)
                            If (dRowsAdjun.Length > 0) Then
                                For Each Adjun As DataRow In dRowsAdjun
                                    dNewRow = ds.Tables("TEMPDESGLOSEADJUN").NewRow

                                    dNewRow.Item("LINEA") = dRow.Item("LINEA")
                                    dNewRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                                    dNewRow.Item("CAMPO_HIJO") = dRow.Item("CAMPO_HIJO")

                                    dNewRow.Item("ID") = Adjun.Item("ADJUN")
                                    dNewRow.Item("TIPO") = 3
                                    Try
                                        ds.Tables("TEMPDESGLOSEADJUN").Rows.Add(dNewRow)
                                    Catch ex As Exception
                                    End Try
                                Next
                            End If
                        End If
                    End If
                End If
            Next
            '--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            dRows = ds.Tables("TEMPDESGLOSE").Select("LINEA_OLD IS NULL")
            Dim lCampoPadre As Long
            Dim dDefaultRow As DataRow

            Dim lMaxLine As Long
            lMaxLine = 0

            Dim auxDataTable As New DataTable("GRUPOS")
            auxDataTable.Columns.Add("CAMPO_PADRE", System.Type.GetType("System.Int32"))
            auxDataTable.Columns.Add("LINEAS", System.Type.GetType("System.Int32"))
            Dim auxRow As DataRow

            For Each dRow In dRows
                If auxDataTable.Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString).Length > 0 Then

                    auxRow = auxDataTable.Select("CAMPO_PADRE=" + dRow.Item("CAMPO_PADRE").ToString)(0)
                Else
                    auxRow = auxDataTable.NewRow
                    auxRow.Item("CAMPO_PADRE") = dRow.Item("CAMPO_PADRE")
                    auxRow.Item("LINEAS") = 0
                End If

                If DBNullToSomething(auxRow.Item("LINEAS")) < DBNullToSomething(dRow.Item("LINEA")) Then
                    auxRow.Item("LINEAS") = dRow.Item("LINEA")

                End If
                If auxRow.RowState = DataRowState.Detached Then
                    auxDataTable.Rows.Add(auxRow)
                End If
            Next
            Dim i As Long

            For Each auxRow In auxDataTable.Rows
                For i = 1 To auxRow.Item("LINEAS")
                    If dsInvisibles.Tables(1).Select("LINEA=" + i.ToString()).Length > 0 Then
                        For Each dDefaultRow In dsInvisibles.Tables(1).Select("CAMPO_PADRE = " + auxRow.Item("CAMPO_PADRE").ToString + " AND VISIBLE = 0 AND LINEA = 1")
                            If ds.Tables("TEMPDESGLOSE").Select("LINEA=" + i.ToString + " AND CAMPO_PADRE = " + dDefaultRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO = " + dDefaultRow.Item("CAMPO_HIJO").ToString).Length = 0 Then
                                dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                                dNewRow.Item("LINEA") = i
                                dNewRow.Item("CAMPO_PADRE") = dDefaultRow.Item("CAMPO_PADRE").ToString
                                dNewRow.Item("CAMPO_HIJO") = dDefaultRow.Item("CAMPO_HIJO")
                                dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("VALOR_TEXT")
                                If (IsDBNull(dDefaultRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dDefaultRow.Item("LISTA_TEXT"))) Then
                                    dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("LISTA_TEXT")
                                End If

                                Try
                                    dNewRow.Item("VALOR_NUM") = dDefaultRow.Item("VALOR_NUM")
                                    dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dDefaultRow.Item("VALOR_NUM")), DBNullToSomething(dDefaultRow.Item("VALOR_FEC")))
                                    dNewRow.Item("VALOR_BOOL") = dDefaultRow.Item("VALOR_BOOL")
                                Catch ex As Exception
                                End Try

                                dNewRow.Item("TIPOGS") = dDefaultRow.Item("TIPO_CAMPO_GS")
                                If bMaper Then
                                    dNewRow.Item("VALERP") = dDefaultRow.Item("VALIDACION_ERP")
                                    dNewRow.Item("IDATRIB") = dDefaultRow.Item("ID_ATRIB_GS")
                                End If

                                Try
                                    'caso Presupuesto nivel 4: Campo Oculto que controla por otro lado el maper.
                                    'Intenta meter algo que ya he metido. Solo desglose popup.       
                                    ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                                Catch ex As Exception
                                End Try
                            Else
                                dNewRow = ds.Tables("TEMPDESGLOSE").Select("LINEA=" + i.ToString + " AND CAMPO_PADRE = " + dDefaultRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO = " + dDefaultRow.Item("CAMPO_HIJO").ToString)(0)
                                If DBNullToSomething(dDefaultRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Material And DBNullToSomething(dDefaultRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Unidad Then
                                    dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("VALOR_TEXT")
                                End If

                                If (IsDBNull(dDefaultRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dDefaultRow.Item("LISTA_TEXT"))) Then
                                    dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("LISTA_TEXT")
                                End If

                                Try
                                    dNewRow.Item("VALOR_NUM") = dDefaultRow.Item("VALOR_NUM")
                                    dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dDefaultRow.Item("VALOR_NUM")), DBNullToSomething(dDefaultRow.Item("VALOR_FEC")))
                                    dNewRow.Item("VALOR_BOOL") = dDefaultRow.Item("VALOR_BOOL")
                                Catch ex As Exception
                                End Try

                                dNewRow.Item("TIPOGS") = dDefaultRow.Item("TIPO_CAMPO_GS")
                                If bMaper Then
                                    dNewRow.Item("VALERP") = dDefaultRow.Item("VALIDACION_ERP")
                                    dNewRow.Item("IDATRIB") = dDefaultRow.Item("ID_ATRIB_GS")
                                End If

                            End If

                        Next
                    Else
                        For Each dDefaultRow In dsInvisibles.Tables(1).Select("CAMPO_PADRE = " + auxRow.Item("CAMPO_PADRE").ToString + " AND VISIBLE = 0")
                            If ds.Tables("TEMPDESGLOSE").Select("LINEA=" + i.ToString + " AND CAMPO_PADRE = " + dDefaultRow.Item("CAMPO_PADRE").ToString + " AND CAMPO_HIJO = " + dDefaultRow.Item("CAMPO_HIJO").ToString).Length = 0 Then
                                dNewRow = ds.Tables("TEMPDESGLOSE").NewRow
                                dNewRow.Item("LINEA") = i
                                dNewRow.Item("CAMPO_PADRE") = dDefaultRow.Item("CAMPO_PADRE").ToString
                                dNewRow.Item("CAMPO_HIJO") = dDefaultRow.Item("CAMPO_HIJO")
                                dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("VALOR_TEXT")
                                If (IsDBNull(dDefaultRow.Item("VALOR_TEXT"))) And (Not IsDBNull(dDefaultRow.Item("LISTA_TEXT"))) Then
                                    dNewRow.Item("VALOR_TEXT") = dDefaultRow.Item("LISTA_TEXT")
                                End If

                                Try
                                    dNewRow.Item("VALOR_NUM") = dDefaultRow.Item("VALOR_NUM")
                                    dNewRow.Item("VALOR_FEC") = DevolverFechaRelativaA(DBNullToSomething(dDefaultRow.Item("VALOR_NUM")), DBNullToSomething(dDefaultRow.Item("VALOR_FEC")))
                                    dNewRow.Item("VALOR_BOOL") = dDefaultRow.Item("VALOR_BOOL")
                                Catch ex As Exception
                                End Try
                                dNewRow.Item("TIPOGS") = dDefaultRow.Item("TIPO_CAMPO_GS")
                                If bMaper Then
                                    dNewRow.Item("VALERP") = dDefaultRow.Item("VALIDACION_ERP")
                                    dNewRow.Item("IDATRIB") = dDefaultRow.Item("ID_ATRIB_GS")
                                End If

                                Try
                                    ds.Tables("TEMPDESGLOSE").Rows.Add(dNewRow)
                                Catch ex As Exception

                                End Try
                            End If
                        Next
                    End If
                Next

            Next

            Return ds

        End Function

        ''' <summary>Funcion que devuelve las precondiciones que tiene una accion</summary>
        ''' <param name="iAccion">Identificador de la accion</param>
        ''' <param name="idioma">Idioma del usuario</param>
        ''' <returns>Un DataSet con los datos de las precondiciones de la accion buscada</returns>
        ''' <remarks>Llamada desde: PmWeb/GuardarInstancia/GuardarConWorkflow</remarks>
        Public Function ObtenerPrecondicionesAccion(lCiaComp As Long, iAccion As Integer, idioma As String) As DataSet
            Authenticate()
            Dim data As DataSet

            If mRemottingServer Then
                data = DBServer.Solicitud_LoadPrecondicionesAccion(lCiaComp, iAccion, idioma, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Solicitud_LoadPrecondicionesAccion(lCiaComp, iAccion, idioma)
            End If

            Return data
        End Function
    End Class
End Namespace