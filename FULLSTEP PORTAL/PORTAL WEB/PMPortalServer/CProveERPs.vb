Option Strict Off
Option Explicit On

Namespace Fullstep.PMPortalServer
    <Serializable()>
    Public Class CProveERPs
        Inherits Lista(Of CProveERP)

        ''' <summary>
        ''' Constructor del objeto Empresas
        ''' </summary>
        ''' <param name="dbserver">Servidor de la bbdd</param>
        ''' <param name="isAuthenticated">Si est� autenticado</param>
        ''' <remarks></remarks>
        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

        ''' <summary>
        ''' Devuelve los datos de los proveedores ERP para la el proveedor y la organizacion de compras, se utiliza para el campo proveedor ERP(143) de PM
        ''' </summary>
        ''' <returns>Dataset con los datos de los proveedores ERP</returns>
        ''' <remarks></remarks>
        Public Function CargarProveedoresERPtoDS(ByVal lCiaComp As Integer) As DataSet
            Dim rs As New DataSet
            rs = mDBServer.ProvesErp_CargarProveedoresERPtoDS(lCiaComp)
            Return rs
        End Function
    End Class
End Namespace