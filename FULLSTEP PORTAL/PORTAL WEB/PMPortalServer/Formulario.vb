Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Formulario
        Inherits Security
        Private mlId As Long
        Private msCod As String
        Private moGrupos As Grupos
        Property Id() As Long
            Get
                Id = mlId
            End Get
            Set(ByVal Value As Long)
                mlId = Value
            End Set
        End Property
        Property Cod() As String
            Get
                Cod = msCod
            End Get
            Set(ByVal Value As String)
                msCod = Value
            End Set
        End Property
        Property Grupos() As Grupos
            Get
                Grupos = moGrupos

            End Get
            Set(ByVal Value As Grupos)
                moGrupos = Value
            End Set
        End Property

        ''' <summary>
        ''' Carga el formulario
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lSolicitud">Solicitud</param>
        ''' <param name="lBloque">Bloque</param>
        ''' <param name="sPer">Persona</param>
        ''' <remarks>Llamada desde: certificados\certificado.aspx.vb ; Tiempo maximo: 0,2</remarks>
        Public Sub Load(ByVal lCiaComp As Long, Optional ByVal sIdi As String = Nothing, Optional ByVal lSolicitud As Long = Nothing, Optional ByVal lBloque As Long = Nothing, Optional ByVal sPer As String = Nothing, Optional ByVal idRol As Integer = Nothing)
            Authenticate()
            Dim data As DataSet
            Dim oFormRow As DataRow
            Dim oGroupRow As DataRow
            Dim oIdiomas As New Idiomas(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
            Dim oIdi As Idioma
            Dim oGrupo As Grupo

            If mRemottingServer Then
                data = DBServer.Formulario_Load(lCiaComp, mlId, sIdi, lSolicitud, lBloque, sPer, idRol, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Formulario_Load(lCiaComp, mlId, sIdi, lSolicitud, lBloque, sPer, idRol)
            End If

            If Not data.Tables(0).Rows.Count = 0 Then
                If sIdi = Nothing Then
                    oIdiomas.Load()
                Else
                    oIdiomas.Add(sIdi, Nothing)
                End If

                For Each oFormRow In data.Tables(0).Rows
                    msCod = DBNullToSomething(oFormRow.Item("COD").ToString)
                    moGrupos = New Grupos(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)

                    For Each oGroupRow In oFormRow.GetChildRows("REL_FORM_GRUPO")
                        oGrupo = New Grupo(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
                        oGrupo.Id = oGroupRow.Item("ID")
                        oGrupo.NumCampos = oGroupRow.Item("NUMCAMPOS")
                        For Each oIdi In oIdiomas.Idiomas
                            oGrupo.Den(oIdi.Cod) = oGroupRow.Item("DEN_" & oIdi.Cod).ToString
                        Next
                        oGrupo.DSCampos = New DataSet
                        oGrupo.DSCampos.Merge(oGroupRow.GetChildRows("REL_GRUPO_CAMPO"))
                        oGrupo.DSCampos.Merge(oGroupRow.GetChildRows("REL_GRUPO_LISTA"))
                        If oGrupo.DSCampos.Tables.Count > 1 Then
                            oGrupo.DSCampos.Relations.Add("REL_CAMPO_LISTA", oGrupo.DSCampos.Tables(0).Columns("ID"), oGrupo.DSCampos.Tables(1).Columns("CAMPO"), False)
                        End If
                        oGrupo.DSCampos.Merge(oGroupRow.GetChildRows("REL_GRUPO_ADJUNTO"))
                        If oGrupo.DSCampos.Tables.Count > 2 Then
                            oGrupo.DSCampos.Relations.Add("REL_CAMPO_ADJUN", oGrupo.DSCampos.Tables(0).Columns("ID"), oGrupo.DSCampos.Tables(2).Columns("CAMPO"), False)
                        ElseIf (oGrupo.DSCampos.Tables.Count = 2 And oGrupo.DSCampos.Relations.Count = 0) Then
                            oGrupo.DSCampos.Relations.Add("REL_CAMPO_ADJUN", oGrupo.DSCampos.Tables(0).Columns("ID"), oGrupo.DSCampos.Tables(1).Columns("CAMPO"), False)
                        End If
                        moGrupos.Add(oGrupo)
                    Next
                Next
            End If
            data = Nothing
        End Sub
        ''' <summary>
        ''' Obtiene los Campos calculados a partir de una solicitud
        ''' </summary>
        ''' <param name="lCiaComp">codigo de compa�ia</param>
        ''' <param name="lIdSolicitud">Solicitud</param>     
        ''' <returns>Campos calculados en la solicitud</returns>
        ''' <remarks>Llamada desde: ayudacampo.aspx     guardarinstancia.aspx   recalcularimportes.aspx; Tiempo m�ximo:0,1</remarks>
        Public Function LoadCamposCalculados(ByVal lCiaComp As Long, ByVal lIdSolicitud As Long, ByVal sIdi As String, Optional ByVal bNuevoWorkfl As Boolean = False, Optional ByVal sProve As String = Nothing, Optional ByVal idUsu As Integer = Nothing) As DataSet
            Authenticate()

            Dim data As DataSet
            If mRemottingServer Then
                data = DBServer.Formulario_LoadCamposCalculados(lCiaComp, mlId, lIdSolicitud, bNuevoWorkfl, sProve, idUsu, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Formulario_LoadCamposCalculados(lCiaComp, mlId, lIdSolicitud, bNuevoWorkfl, sProve, idUsu)
            End If
            Return data
        End Function
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

        ''' <summary>
        ''' Los calculados en un desglose hacen uso de un campo fuera del desglose, esta funci�n lo obtiene aunque este no visible
        ''' </summary>
        ''' <param name="lCiaComp">codigo de compa�ia</param>
        ''' <param name="lIdSolicitud">Solicitud q se esta dando de alta</param>        
        ''' <param name="lIdCampo">id del form_campo q es el desglose, es decir, campo_padre de linea_desglose</param>         
        ''' <param name="sPer">Persona de la empresa de proveedor q esta dando de alta</param>
        ''' <returns>Dataset con el/los campos (no de desglose) q usa el desglose con id lIdCampo</returns>
        ''' <remarks>Llamada desde: recalcularimportes.aspx; Tiempo m�ximo: 0,1</remarks>
        Public Function LoadDesglosePadreVisible(ByVal lCiaComp As Long, ByVal lIdSolicitud As Long, ByVal lIdCampo As Integer, Optional ByVal sPer As String = Nothing, Optional ByVal sProve As String = Nothing, Optional ByVal idUsu As Integer = Nothing) As DataSet
            Authenticate()
            Dim data As DataSet

            If mRemottingServer Then
                data = DBServer.Formulario_LoadDesglosePadreVisible(lCiaComp, mlId, lIdCampo, Nothing, lIdSolicitud, sPer, sProve, idUsu, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Formulario_LoadDesglosePadreVisible(lCiaComp, mlId, lIdCampo, Nothing, lIdSolicitud, sPer, sProve, idUsu)
            End If

            Return data

        End Function
    End Class
End Namespace