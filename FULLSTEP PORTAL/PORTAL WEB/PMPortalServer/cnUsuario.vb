﻿Public Class cnUsuario
    Private _Cod As String
    Public Property Cod() As String
        Get
            Return _Cod
        End Get
        Set(ByVal value As String)
            _Cod = value
        End Set
    End Property
    Private _ProveCod As String
    Public Property ProveCod() As String
        Get
            Return _ProveCod
        End Get
        Set(ByVal value As String)
            _ProveCod = value
        End Set
    End Property
    Private _Con As Integer
    Public Property Con() As Integer
        Get
            Return _Con
        End Get
        Set(ByVal value As Integer)
            _Con = value
        End Set
    End Property
    Private _Nombre As String
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property
    Private _SituacionUO As String
    Public Property SituacionUO() As String
        Get
            Return _SituacionUO
        End Get
        Set(ByVal value As String)
            _SituacionUO = value
        End Set
    End Property
    Private _ImagenUrl As String
    Public Property ImagenUrl() As String
        Get
            Return _ImagenUrl
        End Get
        Set(ByVal value As String)
            _ImagenUrl = value
        End Set
    End Property
End Class
