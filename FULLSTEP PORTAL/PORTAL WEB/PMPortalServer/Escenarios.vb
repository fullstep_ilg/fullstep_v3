﻿Imports System.Linq
Imports Fullstep.FSNLibrary

Namespace Fullstep.PMPortalServer
    <Serializable()>
    Public Class Escenarios
        Inherits Security
        ''' <summary>
        ''' Constructor de la clase Escenarios
        ''' </summary>
        ''' <param name="dbserver">Servidor de la bbdd</param>
        ''' <param name="isAuthenticated">Si está autenticado</param>
        ''' <revision></revision>
        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
        ''' <summary>
        ''' Obtiene los escenarios que el usuario puede utilizar por tipo de visor
        ''' </summary>
        ''' <param name="TipoVisor">El tipo de visor para obtener los escenarios (1-solicitudes,2-no conf,3-certif,4-contratos)</param>
        ''' <remarks></remarks>
        Public Function FSN_Get_Escenarios_Usuario(ByVal lCiaComp As Long, ByVal Idioma As String, ByVal iTipoVisor As TipoVisor) As Object
            Authenticate()

            Dim idEscenarioSeleccionado As Integer = 0
            Dim dEscenarios As New Dictionary(Of String, Escenario)
            Dim iEscenario As Escenario
            Dim dsEscenarios As DataSet = DBServer.FSN_Get_Escenarios_Usuario(lCiaComp, Idioma, iTipoVisor, idEscenarioSeleccionado)

            iEscenario = FSN_Get_Escenario_Defecto(Idioma, iTipoVisor)
            iEscenario.Cargado = True
            dEscenarios(iEscenario.Id) = iEscenario

            'Creamos la estructura del escenario, con sus filtros vinculados y estos a sus vistas.
            For Each escenario As DataRow In dsEscenarios.Tables("ESCENARIOS_USUARIO").Rows
                iEscenario = Create_Escenario(Idioma, escenario,
                        dsEscenarios.Tables("ESCENARIO_SOLICITUD_FORMULARIOS_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                        dsEscenarios.Tables("ESCENARIO_FILTROS_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                        dsEscenarios.Tables("ESCENARIO_FILTRO_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                        dsEscenarios.Tables("ESCENARIO_FILTRO_CONDICIONES_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                        dsEscenarios.Tables("ESCENARIO_VISTAS_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                        dsEscenarios.Tables("ESCENARIO_VISTA_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), iTipoVisor)

                dEscenarios(iEscenario.Id) = iEscenario
            Next

            Return {dEscenarios, idEscenarioSeleccionado}
        End Function
        Public Function FSN_Get_Escenario_Usuario(ByVal lCiaComp As Long, ByVal Idioma As String, ByVal iTipoVisor As TipoVisor, ByVal IdEscenario As Integer) As Object
            Authenticate()

            Dim dsEscenario As DataSet = DBServer.FSN_Get_Escenarios_Usuario(lCiaComp, Idioma, iTipoVisor, IdEscenario)
            Dim iEscenario As New Escenario
            Dim lCarpetas As New List(Of cn_fsItem)
            Dim lSolicitudes As New List(Of cn_fsItem)
            If Not dsEscenario.Tables("ESCENARIOS_USUARIO").Rows.Count = 0 Then
                Dim escenario As DataRow = dsEscenario.Tables("ESCENARIOS_USUARIO").Rows(0)
                iEscenario = Create_Escenario(Idioma, escenario,
                    dsEscenario.Tables("ESCENARIO_SOLICITUD_FORMULARIOS_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                    dsEscenario.Tables("ESCENARIO_FILTROS_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                    dsEscenario.Tables("ESCENARIO_FILTRO_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                    dsEscenario.Tables("ESCENARIO_FILTRO_CONDICIONES_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                    dsEscenario.Tables("ESCENARIO_VISTAS_USUARIO").Select("ESCENARIO=" & escenario("ID")),
                    dsEscenario.Tables("ESCENARIO_VISTA_CAMPOS_USUARIO").Select("ESCENARIO=" & escenario("ID")), iTipoVisor)
            End If

            Return iEscenario
        End Function
        Public Function FSN_Get_TextoLargo_Instancia(ByVal lCiaComp As Long, ByVal Instancia As Integer, ByVal IdCampoOrigen As Integer, ByVal EsDesglose As Boolean) As String
            Authenticate()
            Return DBServer.FSN_Get_TextoLargo_Instancia(lCiaComp, Instancia, IdCampoOrigen, EsDesglose)
        End Function
        Public Function FSN_Get_Opciones_Lista(ByVal lCiaComp As Long, ByVal Idioma As String, ByVal IdCampo As Integer, ByVal TipoCampo As TiposDeDatos.TipoGeneral) As List(Of cn_fsItem)
            Authenticate()
            Dim dtEscenarios As DataTable = DBServer.FSN_Get_Opciones_Lista_Campo(lCiaComp, IdCampo)
            Dim lTiposSolicitud As New List(Of cn_fsItem)
            Dim iTipoSolicitud As cn_fsItem
            For Each tipoSolicitud As DataRow In dtEscenarios.Rows
                iTipoSolicitud = New cn_fsItem
                With iTipoSolicitud
                    .value = tipoSolicitud("ORDEN")
                    Select Case TipoCampo
                        Case TiposDeDatos.TipoGeneral.TipoNumerico
                            .text = tipoSolicitud("VALOR_NUM")
                        Case TiposDeDatos.TipoGeneral.TipoFecha
                            .text = tipoSolicitud("VALOR_FEC")
                        Case Else
                            .text = tipoSolicitud("VALOR_TEXT_" & Idioma)
                    End Select
                End With
                lTiposSolicitud.Add(iTipoSolicitud)
            Next
            Return lTiposSolicitud
        End Function
        Public Function FSN_Get_Opciones_Lista_EstadoHomologacion(ByVal lCiaComp As Long, ByVal Idioma As String, SolicitudFormularioVinculados As Dictionary(Of String, SerializableKeyValuePair(Of Integer, String))) As List(Of cn_fsItem)
            Authenticate()
            Dim dtEstados As DataTable = DBServer.FSN_Get_Opciones_Lista_EstadoHomologacion(lCiaComp, SolicitudFormularioVinculados)
            Dim lEstados As New List(Of cn_fsItem)
            Dim oEstado As cn_fsItem
            For Each drEstado As DataRow In dtEstados.Rows
                oEstado = New cn_fsItem
                With oEstado
                    .value = drEstado("ORDEN")
                    .text = drEstado("VALOR_TEXT_" & Idioma)
                End With
                lEstados.Add(oEstado)
            Next
            Return lEstados
        End Function
        Public Function FSN_Get_Opciones_Lista_TipoSolicitud(ByVal lCiaComp As Long, ByVal Usuario As String, ByVal Idioma As String, ByVal IdsSolicitud As String, ByVal TipoVisor As TipoVisor, ByVal sProve As String) As List(Of cn_fsItem)
            Authenticate()
            Dim TipoSolicitud As Integer?
            Select Case TipoVisor
                Case TipoVisor.NoConformidades
                    TipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad
                Case TipoVisor.Certificados
                    TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado
                Case TipoVisor.Contratos
                    TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Contrato
                Case TipoVisor.Facturas
                    TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Factura
                Case TipoVisor.Encuestas
                    TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Encuesta
                Case TipoVisor.SolicitudesQA
                    TipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudQA
                Case Else
                    TipoSolicitud = Nothing
            End Select
            Dim dtEscenarios As DataTable = DBServer.FSN_Get_Escenarios_TiposSolicitud_Formulario(lCiaComp, Usuario, Idioma, IdsSolicitud, TipoSolicitud, sProve:=sProve)
            Dim lTiposSolicitud As New List(Of cn_fsItem)
            Dim iTipoSolicitud As cn_fsItem
            For Each rtipoSolicitud As DataRow In dtEscenarios.Rows
                iTipoSolicitud = New cn_fsItem
                With iTipoSolicitud
                    .value = rtipoSolicitud("ID")
                    .text = rtipoSolicitud("DEN")
                End With
                lTiposSolicitud.Add(iTipoSolicitud)
            Next
            Return lTiposSolicitud
        End Function
        Public Function FSN_Get_Escenario_Defecto(ByVal Idioma As String, ByVal iTipoVisor As Integer) As Escenario
            Dim Textos As DataTable
            Dim oDict As Dictionary = New Dictionary(mDBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
            oDict.LoadData(TiposDeDatos.ModulosIdiomas.VisorSolicitudes, Idioma)
            Textos = oDict.Data.Tables(0)

            Dim iEscenario As New Escenario
            Dim iEscenarioFiltro As EscenarioFiltro
            Dim iEscenarioFiltroCampo, iEscenarioCampoVista As Escenario_Campo
            Dim iEscenarioVista As EscenarioVista
            Dim iEscenarioFiltroCondicion As EscenarioFiltroCondicion
            Dim ordenCampoVista As Integer = 0

            With iEscenario
                .Cargado = True
                .Id = 0
                .Visible = False
                .Nombre = "****** " & Textos(38)(1) & " *****"
                .AplicaTodas = True
                .TipoVisor = iTipoVisor

                iEscenarioFiltro = New EscenarioFiltro
                With iEscenarioFiltro
                    .Id = 0
                    .Escenario = iEscenario.Id
                    .Nombre = "****** " & Textos(38)(1) & " *****" 'Pendientes
                    .FormulaAvanzada = False
                    iEscenarioFiltroCondicion = New EscenarioFiltroCondicion
                    For Each escenarioFiltroCampo As Integer In {CamposGeneralesVisor.DENOMINACION, CamposGeneralesVisor.FECHAALTA, CamposGeneralesVisor.IMPORTE, CamposGeneralesVisor.ESTADO}
                        iEscenarioFiltroCampo = New Escenario_Campo
                        With iEscenarioFiltroCampo
                            iEscenarioFiltroCampo = Get_Campo_General(escenarioFiltroCampo, Idioma, iTipoVisor)
                            If escenarioFiltroCampo = CamposGeneralesVisor.ESTADO Then
                                iEscenarioFiltroCampo.Visible = False
                            Else
                                iEscenarioFiltroCampo.Visible = True
                            End If
                        End With
                        .Filtro_Campos.Add(iEscenarioFiltroCampo)
                    Next
                    'EN EL ESCENARIO POR DEFECTO FILTRAREMOS POR ESTADO Y OBTENDREMOS LAS SOLICITUDES HASTA HOY Y DESDE EL PARAMETRO QUE INDIQUE EL WEB.CONFIG
                    'AÑADIMOS LA CONDICION DEL ESTADO
                    iEscenarioFiltroCondicion = New EscenarioFiltroCondicion
                    With iEscenarioFiltroCondicion
                        .IdFiltro = iEscenarioFiltro.Id
                        .Orden = 1
                        .EsLista = True
                        .IdCampo = CamposGeneralesVisor.ESTADO
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .EsCampoGeneral = True
                        .Denominacion = Textos(29)(1)
                        .Denominacion_BD = "ESTADO"
                        .Operador = Operadores.Campos.ES
                        .Valores.Add("1000###" & Textos(38)(1)) 'Pendientes
                    End With
                    .FormulaCondiciones.Add(iEscenarioFiltroCondicion)
                End With
                .EscenarioFiltros(iEscenarioFiltro.Id) = iEscenarioFiltro

                iEscenarioVista = New EscenarioVista
                With iEscenarioVista
                    .Id = 0
                    .Escenario = iEscenario.Id
                    .Nombre = Textos(9)(1)
                    .Defecto = True
                    .Orden = "FECHA DESC"
                    For Each campoVista As Integer In {CamposGeneralesVisor.TIPOSOLICITUD, CamposGeneralesVisor.DENOMINACION,
                            CamposGeneralesVisor.IDENTIFICADOR, CamposGeneralesVisor.FECHAALTA, CamposGeneralesVisor.USUARIO,
                            CamposGeneralesVisor.SITUACIONACTUAL, CamposGeneralesVisor.PETICIONARIO,
                            CamposGeneralesVisor.DEPARTAMENTO, CamposGeneralesVisor.IMPORTE, CamposGeneralesVisor.MARCASEGUIMIENTO}
                        iEscenarioCampoVista = Get_Campo_General(campoVista, Idioma, iTipoVisor)
                        iEscenarioCampoVista.OrdenVisualizacion = ordenCampoVista
                        .Campos_Vista.Add(iEscenarioCampoVista)
                        ordenCampoVista += 1
                    Next
                End With
                .EscenarioVistas(iEscenarioVista.Id) = iEscenarioVista
            End With

            Return iEscenario
        End Function
#Region "Utilidades"
        Private Function Create_Escenario(ByVal Idioma As String, ByVal Escenario As DataRow, ByVal EscenarioSolicitudFormulario As DataRow(), ByVal EscenarioFiltros As DataRow(),
                ByVal EscenarioFiltroCampos As DataRow(), ByVal EscenarioFiltroCondiciones As DataRow(),
                ByVal EscenarioVistas As DataRow(), ByVal EscenarioVistaCampos As DataRow(), ByVal iTipoVisor As TipoVisor) As Escenario
            Dim Textos As DataTable
            Dim oDict As Dictionary = New Dictionary(mDBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.VisorSolicitudes, Idioma)
            Textos = oDict.Data.Tables(0)
            Dim iEscenario As New Escenario
            Dim iEscenarioFiltro As EscenarioFiltro
            Dim iEscenarioFiltroCampo As Escenario_Campo
            Dim iEscenarioFiltroCondicion As EscenarioFiltroCondicion
            Dim iEscenarioVista As EscenarioVista
            Dim iEscenarioVistaCampo As Escenario_Campo
            Dim dtEscenarioFiltroCampos As New DataTable
            Dim campoPadre As Integer
            With iEscenario
                .Cargado = (EscenarioFiltros.Length > 0 AndAlso EscenarioVistas.Length > 0)
                .Id = DBNullToInteger(Escenario("ID"))
                .Visible = True
                .Nombre = Escenario("NOMBRE").ToString
                .Editable = False
                .AplicaTodas = DBNullToBoolean(Escenario("APLICATODAS"))
                'Anotamos las solicitudes a las que se aplica el escenario
                For Each formularioVinculado As DataRow In EscenarioSolicitudFormulario
                    .SolicitudFormularioVinculados(formularioVinculado("SOLICITUD")) = New SerializableKeyValuePair(Of Integer, String)(CType(formularioVinculado("FORMULARIO"), Integer), formularioVinculado("DEN"))
                Next
                'Añadimos los filtros del escenario
                For Each escenarioFiltro As DataRow In EscenarioFiltros
                    iEscenarioFiltro = New EscenarioFiltro
                    With iEscenarioFiltro
                        .Id = DBNullToInteger(escenarioFiltro("ID"))
                        .Escenario = DBNullToInteger(escenarioFiltro("ESCENARIO"))
                        .Nombre = escenarioFiltro("NOMBRE").ToString
                        .FormulaAvanzada = DBNullToBoolean(escenarioFiltro("FORMULAAVANZADA"))
                        .ErrorFormula = DBNullToBoolean(escenarioFiltro("ERROR_FORMULA"))

                        If Not EscenarioFiltroCampos.Length = 0 Then dtEscenarioFiltroCampos = EscenarioFiltroCampos.CopyToDataTable

                        For Each escenarioFiltroCampo As DataRow In (From campo In EscenarioFiltroCampos Where campo.Item("FILTRO") = iEscenarioFiltro.Id)
                            iEscenarioFiltroCampo = New Escenario_Campo
                            With iEscenarioFiltroCampo
                                If escenarioFiltroCampo("ESGENERAL") Then
                                    iEscenarioFiltroCampo = Get_Campo_General(DBNullToInteger(escenarioFiltroCampo("CAMPO")), Idioma, iTipoVisor)
                                Else
                                    .Id = DBNullToInteger(escenarioFiltroCampo("CAMPO"))
                                    .EsCampoGeneral = DBNullToBoolean(escenarioFiltroCampo("ESGENERAL"))
                                    .Denominacion = escenarioFiltroCampo("DEN_" & Idioma).ToString
                                    .TipoCampo = DBNullToInteger(escenarioFiltroCampo("SUBTIPO"))
                                    .EsLista = (DBNullToBoolean(escenarioFiltroCampo("INTRO")) OrElse .TipoCampo = TiposDeDatos.TipoGeneral.TipoBoolean)
                                    .TipoCampoGS = DBNullToInteger(escenarioFiltroCampo("TIPO_CAMPO_GS"))
                                    Select Case .TipoCampoGS
                                        Case TiposDeDatos.TipoCampoGS.FormaPago, TiposDeDatos.TipoCampoGS.Moneda, TiposDeDatos.TipoCampoGS.Unidad,
                                            TiposDeDatos.TipoCampoGS.UnidadPedido, TiposDeDatos.TipoCampoGS.Dest, TiposDeDatos.TipoCampoGS.Departamento,
                                            TiposDeDatos.TipoCampoGS.OrganizacionCompras, TiposDeDatos.TipoCampoGS.Centro, TiposDeDatos.TipoCampoGS.Almacen,
                                            TiposDeDatos.TipoCampoGS.TipoPedido
                                            .EsLista = True
                                        Case TiposDeDatos.TipoCampoGS.Pais
                                            .EsLista = True
                                            campoPadre = .Id
                                        Case TiposDeDatos.TipoCampoGS.Provincia
                                            .EsLista = True
                                            .CampoPadre = campoPadre
                                            iEscenarioFiltro.Filtro_Campos(iEscenarioFiltro.Filtro_Campos.Count - 1).CampoHijo = .Id
                                    End Select
                                    .CampoDesglose = DBNullToInteger(escenarioFiltroCampo("CAMPO_PADRE"))
                                    .Denominacion_BD = "C_" & IIf(IsDBNull(escenarioFiltroCampo("CAMPO_PADRE")), "", escenarioFiltroCampo("CAMPO_PADRE") & "_") &
                                    DBNullToInteger(escenarioFiltroCampo("TIPO_CAMPO_GS")) & "_" & DBNullToInteger(escenarioFiltroCampo("CAMPO"))
                                    If .EsCampoGeneral AndAlso .Id = 14 Then .OpcionesLista = Get_Opciones_Lista_Estado(Textos, iTipoVisor)
                                End If
                                iEscenarioFiltroCampo.Visible = DBNullToBoolean(escenarioFiltroCampo("VISIBLE"))
                            End With
                            .Filtro_Campos.Add(iEscenarioFiltroCampo)
                        Next
                        For Each escenarioFiltroCondicion As DataRow In (From campo In EscenarioFiltroCondiciones Where campo.Item("FILTRO") = iEscenarioFiltro.Id)
                            iEscenarioFiltroCondicion = New EscenarioFiltroCondicion
                            With iEscenarioFiltroCondicion
                                If DBNullToBoolean(escenarioFiltroCondicion("ESGENERAL")) Then
                                    Dim oCampo As Escenario_Campo = Get_Campo_General(DBNullToInteger(escenarioFiltroCondicion("CAMPO")), Idioma, iTipoVisor)
                                    .EsCampoGeneral = True
                                    .IdCampo = oCampo.Id
                                    .EsLista = oCampo.EsLista
                                    .TipoCampo = oCampo.TipoCampo
                                    .TipoCampoGS = oCampo.TipoCampoGS
                                    .Denominacion = oCampo.Denominacion
                                    .Denominacion_BD = oCampo.Denominacion_BD
                                Else
                                    .EsCampoGeneral = False
                                    If IsDBNull(escenarioFiltroCondicion("CAMPO")) Then
                                        .IdCampo = -1
                                    Else
                                        .IdCampo = DBNullToInteger(escenarioFiltroCondicion("CAMPO"))
                                    End If
                                    .TipoCampo = DBNullToInteger(escenarioFiltroCondicion("SUBTIPO"))
                                    .TipoCampoGS = DBNullToInteger(escenarioFiltroCondicion("TIPO_CAMPO_GS"))
                                    If {TiposDeDatos.TipoCampoGS.FormaPago, TiposDeDatos.TipoCampoGS.Moneda, TiposDeDatos.TipoCampoGS.Unidad,
                                    TiposDeDatos.TipoCampoGS.UnidadPedido, TiposDeDatos.TipoCampoGS.Dest, TiposDeDatos.TipoCampoGS.Departamento,
                                    TiposDeDatos.TipoCampoGS.OrganizacionCompras, TiposDeDatos.TipoCampoGS.Centro, TiposDeDatos.TipoCampoGS.Almacen,
                                    TiposDeDatos.TipoCampoGS.TipoPedido, TiposDeDatos.TipoCampoGS.Pais, TiposDeDatos.TipoCampoGS.Provincia}.Contains(.TipoCampoGS) Then
                                        .EsLista = True
                                    Else
                                        .EsLista = (DBNullToBoolean(escenarioFiltroCondicion("INTRO")) OrElse .TipoCampo = TiposDeDatos.TipoGeneral.TipoBoolean)
                                    End If
                                    .Denominacion = escenarioFiltroCondicion("DENOMINACION").ToString
                                    .Denominacion_BD = escenarioFiltroCondicion("DENOMINACION_BD").ToString
                                    .TieneError = IsDBNull(escenarioFiltroCondicion("CAMPO"))
                                End If
                                .IdFiltro = iEscenarioFiltro.Id
                                .Orden = DBNullToInteger(escenarioFiltroCondicion("ORDEN"))
                                .Operador = DBNullToInteger(escenarioFiltroCondicion("OPERADOR"))

                                For Each valor As String In Split(DBNullToStr(escenarioFiltroCondicion("VALOR")), "#|#")
                                    .Valores.Add(valor)
                                Next
                            End With
                            .FormulaCondiciones.Add(iEscenarioFiltroCondicion)
                        Next
                    End With
                    .EscenarioFiltros(escenarioFiltro("ID")) = iEscenarioFiltro
                Next

                'Añadimos las vistas del escenario
                For Each escenarioVista As DataRow In EscenarioVistas
                    iEscenarioVista = New EscenarioVista
                    With iEscenarioVista
                        .Id = DBNullToInteger(escenarioVista("ID"))
                        .Escenario = DBNullToInteger(escenarioVista("ESCENARIO"))
                        .Nombre = escenarioVista("NOMBRE").ToString
                        .Orden = escenarioVista("ORDEN").ToString

                        For Each EscenarioVistaCampo As DataRow In (From campo In EscenarioVistaCampos Where campo.Item("VISTA") = iEscenarioVista.Id)
                            iEscenarioVistaCampo = New Escenario_Campo
                            If DBNullToBoolean(EscenarioVistaCampo("ESGENERAL")) Then
                                iEscenarioVistaCampo = Get_Campo_General(EscenarioVistaCampo("CAMPO"), Idioma, iTipoVisor)
                            Else
                                With iEscenarioVistaCampo
                                    .Id = DBNullToInteger(EscenarioVistaCampo("CAMPO"))
                                    .EsCampoGeneral = False
                                    .EsCampoDesglose = DBNullToBoolean(EscenarioVistaCampo("ESDESGLOSE"))
                                    .TipoCampo = DBNullToInteger(EscenarioVistaCampo("SUBTIPO"))
                                    .TipoCampoGS = DBNullToInteger(EscenarioVistaCampo("TIPO_CAMPO_GS"))
                                    .Denominacion = EscenarioVistaCampo("DEN_" & Idioma).ToString
                                    If .EsCampoDesglose Then
                                        .Denominacion_BD = "C_" & DBNullToInteger(EscenarioVistaCampo("CAMPO_PADRE")) & "_" & DBNullToInteger(EscenarioVistaCampo("TIPO_CAMPO_GS")) & "_" & DBNullToInteger(EscenarioVistaCampo("CAMPO"))
                                    Else
                                        .Denominacion_BD = "C_" & DBNullToInteger(EscenarioVistaCampo("TIPO_CAMPO_GS")) & "_" & DBNullToInteger(EscenarioVistaCampo("CAMPO"))
                                    End If
                                End With
                            End If
                            iEscenarioVistaCampo.NombrePersonalizado = EscenarioVistaCampo("NOMBREPERSONALIZADO").ToString
                            iEscenarioVistaCampo.AnchoVisualizacion = DBNullToDbl(EscenarioVistaCampo("ANCHO"))
                            .Campos_Vista.Add(iEscenarioVistaCampo)
                        Next
                    End With
                    .EscenarioVistas(escenarioVista("ID")) = iEscenarioVista
                Next
            End With
            Return iEscenario
        End Function
        Private Function Get_Campo_General(ByVal Id As Integer, ByVal Idioma As String, ByVal iTipoVisor As TipoVisor) As Escenario_Campo
            Dim Textos As DataTable
            Dim oDict As Dictionary = New Dictionary(mDBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.VisorSolicitudes, Idioma)
            Textos = oDict.Data.Tables(0)

            Dim oCampo_General As Escenario_Campo
            oCampo_General = New Escenario_Campo
            With oCampo_General
                .EsCampoGeneral = True
                .Visible = True
                .EsCampoDesglose = False
                .Id = Id
                Select Case Id
                    Case CamposGeneralesVisor.DENOMINACION  'denominación
                        .Denominacion = Textos(16)(1)
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .Denominacion_BD = "DEN"
                        .AnchoVisualizacion = 200
                    Case CamposGeneralesVisor.ORIGEN   'departamento peticionario
                        .Denominacion = Textos(17)(1)
                        .EsLista = True
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .Denominacion_BD = "ORIGEN"
                    Case CamposGeneralesVisor.FECHAALTA  'fecha de alta
                        .Denominacion = Textos(18)(1)
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha
                        .Denominacion_BD = "FECHA"
                    Case CamposGeneralesVisor.IDENTIFICADOR  'identificador
                        .Denominacion = Textos(19)(1)
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico
                        .Denominacion_BD = "ID"
                    Case CamposGeneralesVisor.IMPORTE  'importe
                        .Denominacion = Textos(20)(1)
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico
                        .Denominacion_BD = "IMPORTE"
                    Case CamposGeneralesVisor.INFOPEDIDOSASOCIADOS  'Información de pedidos asociados a la solicitud
                        .Denominacion = Textos(21)(1)
                        .EsLista = True
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .Denominacion_BD = "PEDIDOS"
                    Case CamposGeneralesVisor.PETICIONARIO 'peticionario
                        .Denominacion = Textos(22)(1)
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .TipoCampoGS = -1
                        .Denominacion_BD = "PETICIONARIO"
                    Case CamposGeneralesVisor.INFOPROCESOSASOCIADOS  'Información de procesos asociados a la solicitud
                        .Denominacion = Textos(23)(1)
                        .EsLista = True
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .Denominacion_BD = "PROCESOS"
                    Case CamposGeneralesVisor.SITUACIONACTUAL  'situación actual
                        .Denominacion = Textos(24)(1)
                        .EsLista = True
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .Denominacion_BD = "ETAPA"
                    Case CamposGeneralesVisor.TIPOSOLICITUD  'tipo solicitud
                        .Denominacion = Textos(28)(1)
                        .EsLista = True
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .Denominacion_BD = "ID_SOLIC"
                        .AnchoVisualizacion = 100
                    Case CamposGeneralesVisor.USUARIO  'usuario
                        .Denominacion = Textos(25)(1)
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .TipoCampoGS = -2
                        .Denominacion_BD = "USUARIO"
                    Case CamposGeneralesVisor.MOTIVOVISIBILIDAD  '¿visibilidad?
                        .Denominacion = Textos(26)(1)
                        .EsLista = True
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .Denominacion_BD = "MOTIVO_VISIBILIDAD"
                        .OpcionesLista = Get_Opciones_Lista_MotivoVisibilidad(Textos)
                    Case CamposGeneralesVisor.MARCASEGUIMIENTO  'Marca para monitorización de solicitudes
                        .Denominacion = Textos(27)(1)
                        .EsLista = True
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoBoolean
                        .OpcionesLista = Get_Opciones_Lista_Boolean(Textos)
                        .Denominacion_BD = "SEG"
                        .AnchoVisualizacion = 25
                    Case CamposGeneralesVisor.ESTADO  'Estado (Guardadas, En curso, Cerradas, Anuladas, Rechazadas, Finalizadas y Pendientes)
                        .Denominacion = Textos(29)(1)
                        .EsLista = True
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .Denominacion_BD = "ESTADO"
                        .OpcionesLista = Get_Opciones_Lista_Estado(Textos, iTipoVisor)
                    Case CamposGeneralesVisor.FECHATRASLADO  'Fecha de traslado
                        .Denominacion = Textos(30)(1)
                        .EsLista = False
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha
                        .Denominacion_BD = "FECHA_ACT"
                    Case CamposGeneralesVisor.PERSONATRASLADO  'Persona a la que se ha trasladado
                        .Denominacion = Textos(31)(1)
                        .EsLista = False
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .TipoCampoGS = TiposDeDatos.TipoCampoGS.Persona
                        .Denominacion_BD = "TRASLADO_A_USU"
                    Case CamposGeneralesVisor.PROVEEDORTRASLADO  'Proveedor a la que se ha trasladado
                        .Denominacion = Textos(32)(1)
                        .EsLista = False
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .TipoCampoGS = TiposDeDatos.TipoCampoGS.Proveedor
                        .Denominacion_BD = "TRASLADO_A_PROV"
                    Case CamposGeneralesVisor.FECHALIMITEDEVOLUCION  'Fecha limite devolucion
                        .Denominacion = Textos(33)(1)
                        .EsLista = False
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha
                        .Denominacion_BD = "FECHA_LIMITE"
                    Case CamposGeneralesVisor.PROVEEDOR
                        .Denominacion = Textos(34)(1)
                        .EsLista = False
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .TipoCampoGS = TiposDeDatos.TipoCampoGS.Proveedor
                        .Denominacion_BD = "IDM.PROVE"
                    Case CamposGeneralesVisor.ARTICULO
                        .Denominacion = Textos(35)(1)
                        .EsLista = False
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .TipoCampoGS = TiposDeDatos.TipoCampoGS.NuevoCodArticulo
                        .Denominacion_BD = "IDM.ART4"
                    Case CamposGeneralesVisor.FECHAFINFLUJO
                        .Denominacion = Textos(36)(1)
                        .EsLista = False
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha
                        .Denominacion_BD = "FEC_FIN_FLUJO"
                    Case CamposGeneralesVisor.DEPARTAMENTO
                        .Denominacion = Textos(37)(1)
                        .EsLista = True
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoString
                        .Denominacion_BD = "DEP"
                    Case CamposGeneralesVisor.FECHAULTIMAAPROBACION
                        .Denominacion = Textos(139)(1)
                        .EsLista = False
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha
                        .Denominacion_BD = "FECULTAPROBACION"
                    Case CamposGeneralesVisor.ESTADOHOMOLOGACION
                        .Denominacion = Textos(140)(1)
                        .EsLista = True
                        .TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico
                        .TipoCampoGS = TiposDeDatos.TipoCampoGS.EstadoHomologacion
                        .Denominacion_BD = "ESTADO_HOMOLOGACION"
                End Select
            End With
            Return oCampo_General
        End Function
        Private Function Get_Opciones_Lista_MotivoVisibilidad(ByVal Textos As DataTable) As List(Of cn_fsItem)
            Dim lOpcionesLista As New List(Of cn_fsItem)
            Dim iOpcionLista As cn_fsItem
            For Each motivoVisibilidad As Integer In [Enum].GetValues(GetType(MotivoVisibilidadSolicitud))
                iOpcionLista = New cn_fsItem
                iOpcionLista.value = motivoVisibilidad
                Select Case motivoVisibilidad
                    Case MotivoVisibilidadSolicitud.SolicitudAbiertaUsted  'Solicitudes abiertas por usted
                        iOpcionLista.text = Textos(48)(1)
                    Case MotivoVisibilidadSolicitud.SolicitudPendiente  'Solicitudes pendientes 
                        iOpcionLista.text = Textos(49)(1)
                    Case MotivoVisibilidadSolicitud.SolicitudPenditenteDevolucion  'Solicitudes pendientes de devolucion
                        iOpcionLista.text = Textos(50)(1)
                    Case MotivoVisibilidadSolicitud.SolicitudTrasladadaEsperaDevolucion 'Solicitudes trasladadas en espera de devolución
                        iOpcionLista.text = Textos(51)(1)
                    Case MotivoVisibilidadSolicitud.SolicitudParticipo 'Procesos en los que ha participado
                        iOpcionLista.text = Textos(52)(1)
                    Case MotivoVisibilidadSolicitud.SolicitudObservador   'es observador
                        iOpcionLista.text = Textos(53)(1)
                    Case MotivoVisibilidadSolicitud.SolicitudObservadorSustitucion
                        iOpcionLista.text = Textos(54)(1)
                    Case Else '
                        iOpcionLista.text = ""
                End Select
                lOpcionesLista.Add(iOpcionLista)
            Next
            Return lOpcionesLista
        End Function
        Private Function Get_Opciones_Lista_Estado(ByVal Textos As DataTable, ByVal iTipoVisor As TipoVisor) As List(Of cn_fsItem)
            Dim lOpcionesLista As New List(Of cn_fsItem)
            Dim iOpcionLista As cn_fsItem
            For estado As Integer = 1 To IIf(iTipoVisor = TipoVisor.Solicitudes, 8, 6)
                iOpcionLista = New cn_fsItem
                iOpcionLista.text = Textos(37 + estado)(1)
                Select Case estado
                    Case 1 'Pendientes
                        iOpcionLista.value = "1000"
                    Case 2 'Guardadas
                        iOpcionLista.value = "0"
                    Case 3 'En curso
                        iOpcionLista.value = "2"
                    Case 4 'Rechazadas
                        iOpcionLista.value = "6"
                    Case 5 'Anuladas
                        iOpcionLista.value = "8"
                    Case 6 'Finalizadas
                        iOpcionLista.value = "100#101#102#103"
                    Case 7 'Cerradas
                        iOpcionLista.value = "104"
                    Case Else
                        iOpcionLista.value = "1000#0#2#6#8#100#101#102#103#104"
                End Select

                lOpcionesLista.Add(iOpcionLista)
            Next
            Return lOpcionesLista
        End Function
        Public Function Get_Opciones_Lista_Boolean(ByVal Textos As DataTable) As List(Of cn_fsItem)
            Dim lOpcionesLista As New List(Of cn_fsItem)
            Dim iOpcionLista As cn_fsItem
            iOpcionLista = New cn_fsItem
            iOpcionLista.value = ""
            iOpcionLista.text = ""
            lOpcionesLista.Add(iOpcionLista)
            iOpcionLista = New cn_fsItem
            iOpcionLista.value = 1
            iOpcionLista.text = Textos(46)(1)
            lOpcionesLista.Add(iOpcionLista)
            iOpcionLista = New cn_fsItem
            iOpcionLista.value = 0
            iOpcionLista.text = Textos(47)(1)
            lOpcionesLista.Add(iOpcionLista)
            Return lOpcionesLista
        End Function
#End Region
    End Class
End Namespace