﻿Imports System.Globalization
Imports Fullstep.FSNLibrary
Imports System.Configuration
Imports System.Collections.Generic

Namespace Fullstep.PMPortalServer
    Public Class cnMensajes
        Inherits Security
#Region "Generales"
        ''' <summary>
        ''' Constructor de la clase cnMensajes
        ''' </summary>
        ''' <param name="dbserver">Servidor de la bbdd</param>
        ''' <param name="UserCode">Codigo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petición</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>'
        ''' <param name="isAuthenticated">Si está autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo máximo: 0</remarks>
        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal mRemottingServer As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, mRemottingServer, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="Idioma"></param>
        ''' <param name="UsuFormatDate"></param>
        ''' <param name="LimiteMensajesCargados"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ObtenerMensajesUsuario(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdCon As Integer, ByVal Idioma As Fullstep.FSNLibrary.Idioma, _
                ByVal UsuFormatDate As String, ByVal NumbreFormat As System.Globalization.NumberFormatInfo, _
                ByVal tipo As Integer, ByVal megusta As Boolean, ByVal LimiteMensajesCargados As Integer, _
                ByVal Pagina As Integer, ByVal TimeZoneOffSet As Double, ByVal Discrepancias As Boolean, _
                ByVal Factura As Integer, ByVal Linea As Integer) As List(Of cnMensaje)
            Authenticate()

            Try
                Dim dsDatos As New DataSet
                dsDatos = DBServer.CN_ObtenerMensajesUsuario(lCiaComp, ProveCod, IdCon, Idioma.ToString, _
                                                             tipo, megusta, LimiteMensajesCargados, Pagina, _
                                                             Discrepancias, Factura, Linea, _
                                                             msSesionId, msIPDir, msPersistID)

                Dim oMensajesUsuario As New List(Of cnMensaje)

                For Each row As DataRow In dsDatos.Tables("MENSAJES").Rows
                    With oMensajesUsuario
                        .Add(CrearMensaje(row, Idioma, ProveCod, IdCon, UsuFormatDate, NumbreFormat, _
                                          dsDatos.Tables("RESPUESTAS"), dsDatos.Tables("MEGUSTA"), _
                                          dsDatos.Tables("MEGUSTARESPUESTA"), dsDatos.Tables("ADJUNTOS"), False, TimeZoneOffSet, Discrepancias))
                    End With
                Next

                Return oMensajesUsuario
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="row"></param>
        ''' <param name="Idioma"></param>
        ''' <param name="UsuFormatDate"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function CrearMensaje(ByVal row As DataRow, ByVal Idioma As Fullstep.FSNLibrary.Idioma, _
                                      ByVal ProveCod As String, ByVal IdCon As Integer, _
                                      ByVal UsuFormatDate As String, ByVal NumberFormat As Globalization.NumberFormatInfo, _
                                      ByVal Respuestas As DataTable, ByVal MeGusta As DataTable, _
                                      ByVal MeGustaRespuesta As DataTable, ByVal Adjuntos As DataTable, _
                                      ByVal Buscador As Boolean, ByVal TimeZoneOffSet As Double, Optional ByVal Discrepancia As Boolean = False) As cnMensaje
            Dim ocnMensaje As New cnMensaje
            Dim iAdjunto As cnAdjunto
            Dim UltimoMeGustaUsu As Boolean
            With ocnMensaje
                .Categoria = New cnCategoria
                .InfoUsuario = New cnUsuario
                .MeGusta = New cnMeGusta

                .IdMensaje = row("ID")
                .Categoria.Denominacion = row("CATEGORIA")
                .InfoUsuario.Cod = row("USU").ToString
                .InfoUsuario.ProveCod = row("PROVE").ToString
                .InfoUsuario.Con = IIf(IsDBNull(row("IDCON")), 0, row("IDCON"))
                If IsDBNull(row("APE")) Then
                    .InfoUsuario.ProveCod = row("PROVE")
                    .InfoUsuario.Con = row("CON")
                    .InfoUsuario.Nombre = IIf(row("NOMPROVE") Is DBNull.Value, row("APEPROVE"), row("NOMPROVE") & " " & row("APEPROVE"))
                    .InfoUsuario.SituacionUO = row("PROVEDEN")
                    .InfoUsuario.ImagenUrl = ConfigurationManager.AppSettings("rutanormal") & "script/cn/Thumbnail.ashx?t=0&u=" & row("GUIDIMAGENPROVE").ToString & "&d=" & Now
                Else
                    .InfoUsuario.Cod = row("USU")
                    .InfoUsuario.Nombre = IIf(row("NOM") Is DBNull.Value, row("APE"), row("NOM") & " " & row("APE"))
                    .InfoUsuario.SituacionUO = IIf(IsDBNull(row("SITUACIONUO")), row("PROVEDEN"), row("SITUACIONUO"))
                    .InfoUsuario.ImagenUrl = ConfigurationManager.AppSettings("rutanormal") & "script/cn/Thumbnail.ashx?t=0&u=" & row("GUIDIMAGENUSU").ToString & "&d=" & Now
                End If
                .Titulo = row("TITULO").ToString
                .Contenido = row("CONTENIDO").ToString
                .Tipo = row("TIPO")
                .Leido = IsDBNull(row("LEIDO"))
                .Oculto = row("OCULTO")
                '.MensajeHistorico = row("MENSAJEHISTORICO")
                .IdDiscrepancia = DBNullToInteger(row("IDDISCREPANCIA"))
                If Discrepancia Then
                    .DiscrepanciaCerrada = row("DISCREPANCIAABIERTA")
                End If
                If .Tipo = 2 Then
                    .Fecha = DateAdd(DateInterval.Minute, -TimeZoneOffSet, CType(row("CUANDO"), DateTime))
                    .MesCorto = StrConv(MonthName(CType(.Fecha, DateTime).Month, True), VbStrConv.ProperCase)
                    .Dia = CType(.Fecha, DateTime).Day
                    .Hora = CType(.Fecha, DateTime).ToString("t")
                    Dim FechaHora As DateTime
                    FechaHora = TimeValue((Math.Abs(TimeZoneOffSet) \ 60) & ":" & Math.Round((((-TimeZoneOffSet / 60) - (-TimeZoneOffSet \ 60)) * 60), 2))
                    .Cuando = CType(.Fecha, DateTime).ToString("dddd, dd MMMM yyyy HH:mm", CultureInfo.CreateSpecificCulture(Idioma.RefCultural())) & _
                        " (GMT" & IIf(TimeZoneOffSet = 0, "", IIf(TimeZoneOffSet > 0, " -", " +") & FechaHora.ToString("hh:mm")) & ")"
                    .Donde = row("DONDE").ToString
                End If
                .FechaAlta = CType(row("FECALTA"), DateTime).ToLocalTime
                .FechaActualizacion = CType(row("FECACT"), DateTime).ToLocalTime

                'REPUESTAS 
                Dim oRespuestas As DataRow() = Respuestas.Select("MEN=" & row("ID"))
                .NumeroRespuestas = oRespuestas.Length
                .TieneRespuestasOcultas = False
                If Not .NumeroRespuestas = 0 Then
                    .Respuestas = New List(Of cnRespuesta)
                    Dim usuRespuesta As cnRespuesta
                    Dim countVisible As Integer = 1
                    For Each rowRespuesta As DataRow In oRespuestas
                        usuRespuesta = New cnRespuesta
                        With usuRespuesta
                            If Buscador Then
                                .Visible = IIf(IsDBNull(rowRespuesta("VISIBLE")), False, True)
                            Else
                                .Visible = IIf(countVisible > ocnMensaje.NumeroRespuestas - 2, True, False)
                                countVisible += 1
                            End If
                            If Not .Visible Then ocnMensaje.TieneRespuestasOcultas = True
                            .PermisoEditar = False
                            .IdRespuesta = rowRespuesta("ID")
                            .IdMensaje = rowRespuesta("MEN")
                            .Contenido = rowRespuesta("CONTENIDO").ToString
                            .FechaAlta = CType(rowRespuesta("FECALTA"), DateTime).ToLocalTime
                            .FechaActualizacion = CType(rowRespuesta("FECACT"), DateTime).ToLocalTime
                            .Leido = IsDBNull(rowRespuesta("VISIBLE"))
                            .IdDiscrepancia = ocnMensaje.IdDiscrepancia
                            .InfoUsuario = New cnUsuario
                            If IsDBNull(rowRespuesta("USU")) Then
                                .InfoUsuario.ProveCod = rowRespuesta("PROVE")
                                .InfoUsuario.Con = rowRespuesta("CON")
                                .InfoUsuario.Nombre = IIf(rowRespuesta("NOMPROVE") Is DBNull.Value, rowRespuesta("APEPROVE"), rowRespuesta("NOMPROVE") & " " & rowRespuesta("APEPROVE"))
                                .InfoUsuario.SituacionUO = rowRespuesta("PROVEDEN")
                                .InfoUsuario.ImagenUrl = ConfigurationManager.AppSettings("rutanormal") & "script/cn/Thumbnail.ashx?t=0&u=" & rowRespuesta("GUIDIMAGENPROVE").ToString & "&d=" & Now
                            Else
                                .InfoUsuario.Cod = rowRespuesta("USU")
                                .InfoUsuario.Nombre = IIf(rowRespuesta("NOM") Is DBNull.Value, rowRespuesta("APE"), rowRespuesta("NOM") & " " & rowRespuesta("APE"))
                                .InfoUsuario.SituacionUO = rowRespuesta("SITUACIONUO")
                                .InfoUsuario.ImagenUrl = ConfigurationManager.AppSettings("rutanormal") & "script/cn/Thumbnail.ashx?t=0&u=" & rowRespuesta("GUIDIMAGENUSU").ToString
                            End If
                            If Not IsDBNull(rowRespuesta("USUCITADO")) Then
                                .UsuarioCitado = New cnUsuario
                                .UsuarioCitado.Cod = rowRespuesta("USUCITADO")
                                .UsuarioCitado.Nombre = IIf(rowRespuesta("NOMCITADO") Is DBNull.Value, rowRespuesta("APECITADO"), rowRespuesta("NOMCITADO") & " " & rowRespuesta("APECITADO"))
                                .UsuarioCitado.SituacionUO = rowRespuesta("SITUACIONUOCITADO")
                            End If
                            If Not IsDBNull(rowRespuesta("PROVECITADO")) Then
                                .UsuarioCitado = New cnUsuario
                                .InfoUsuario.ProveCod = rowRespuesta("PROVECITADO")
                                .InfoUsuario.Con = rowRespuesta("CONCITADO")
                                .UsuarioCitado.Nombre = IIf(rowRespuesta("NOMPROVECITADO") Is DBNull.Value, rowRespuesta("APEPROVECITADO"), rowRespuesta("NOMPROVECITADO") & " " & rowRespuesta("APEPROVECITADO"))
                                .UsuarioCitado.SituacionUO = rowRespuesta("PROVEDENCITADO")
                            End If

                            .MeGusta = New cnMeGusta
                            Dim oMeGustaRespuesta As DataRow() = MeGustaRespuesta.Select("RESP=" & rowRespuesta("ID"))
                            .MeGusta.NumeroUsuariosMeGusta = oMeGustaRespuesta.Length
                            If .MeGusta.NumeroUsuariosMeGusta = 0 Then
                                .MeGusta.MeGusta = False
                            Else
                                Dim usuMeGustaRespuesta As cnUsuario
                                .MeGusta.UsuariosMeGusta = New List(Of cnUsuario)
                                .MeGusta.MeGusta = False
                                UltimoMeGustaUsu = True
                                For Each rowMeGustaRespuesta As DataRow In oMeGustaRespuesta
                                    If Not IsDBNull(rowMeGustaRespuesta("PROVE")) AndAlso rowMeGustaRespuesta("PROVE") = ProveCod AndAlso rowMeGustaRespuesta("CON") = IdCon Then
                                        .MeGusta.MeGusta = True
                                    Else
                                        usuMeGustaRespuesta = New cnUsuario
                                        With usuMeGustaRespuesta
                                            If IsDBNull(rowMeGustaRespuesta("USU")) Then
                                                .ProveCod = rowMeGustaRespuesta("PROVE")
                                                .Con = rowMeGustaRespuesta("CON")
                                                .Nombre = IIf(rowMeGustaRespuesta("NOMPROVE") Is DBNull.Value, rowMeGustaRespuesta("APEPROVE"), rowMeGustaRespuesta("NOMPROVE") & " " & rowMeGustaRespuesta("APEPROVE"))
                                                .SituacionUO = rowMeGustaRespuesta("PROVEDEN")
                                            Else
                                                .Cod = rowMeGustaRespuesta("USU")
                                                .Nombre = IIf(rowMeGustaRespuesta("NOM") Is DBNull.Value, rowMeGustaRespuesta("APE"), rowMeGustaRespuesta("NOM") & " " & rowMeGustaRespuesta("APE"))
                                                .SituacionUO = rowMeGustaRespuesta("SITUACIONUO")
                                            End If
                                        End With
                                        .MeGusta.UsuariosMeGusta.Add(usuMeGustaRespuesta)
                                    End If
                                    If UltimoMeGustaUsu AndAlso Not IsDBNull(rowMeGustaRespuesta("PROVE")) AndAlso rowMeGustaRespuesta("PROVE") = ProveCod AndAlso rowMeGustaRespuesta("CON") = IdCon Then
                                        .MeGusta.UltimoMeGustaUsu = True
                                    Else
                                        UltimoMeGustaUsu = False
                                    End If
                                Next
                            End If

                            .Adjuntos = New List(Of cnAdjunto)
                            For Each rowAdjunto As DataRow In Adjuntos.Select("MEN=" & rowRespuesta("MEN") & " AND RESP=" & rowRespuesta("ID"))
                                iAdjunto = New cnAdjunto
                                With iAdjunto
                                    .IdAdjunto = rowAdjunto("ID")
                                    .Nombre = rowAdjunto("NOMBRE").ToString
                                    .Guid = rowAdjunto("DGUID").ToString
                                    .Size = DBNullToDbl(rowAdjunto("SIZE"))
                                    .SizeUnit = rowAdjunto("SIZEUNIT").ToString
                                    .SizeToString = modUtilidades.FormatNumber(.Size, NumberFormat) & " " & .SizeUnit
                                    .TipoAdjunto = rowAdjunto("TIPOADJUNTO")
                                    .Url = rowAdjunto("DGUID").ToString
                                    Select Case .TipoAdjunto
                                        Case 0
                                            .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "images/link.png"
                                        Case 1, 2
                                            .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "script/cn/" & "Thumbnail.ashx?f=" & _
                                                            iAdjunto.Guid & "&t=" & .TipoAdjunto
                                        Case 10
                                            .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "images/Attach.png"
                                        Case 11
                                            .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "images/excelAttach.png"
                                        Case 12
                                            .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "images/wordAttach.png"
                                        Case 13
                                            .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "images/pdfAttach.png"
                                        Case Else
                                            .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "images/Attach.png"
                                    End Select
                                End With
                                .Adjuntos.Add(iAdjunto)
                            Next
                        End With
                        .Respuestas.Add(usuRespuesta)
                    Next
                End If

                'ME GUSTA
                Dim oMeGusta As DataRow() = MeGusta.Select("MEN=" & row("ID"))
                .MeGusta.NumeroUsuariosMeGusta = oMeGusta.Length
                If .MeGusta.NumeroUsuariosMeGusta = 0 Then
                    .MeGusta.MeGusta = False
                Else
                    Dim usuMeGusta As cnUsuario
                    .MeGusta.UsuariosMeGusta = New List(Of cnUsuario)
                    .MeGusta.MeGusta = False
                    UltimoMeGustaUsu = True
                    For Each rowMeGusta As DataRow In oMeGusta
                        If Not IsDBNull(rowMeGusta("PROVE")) AndAlso rowMeGusta("PROVE") = ProveCod AndAlso rowMeGusta("CON") = IdCon Then
                            .MeGusta.MeGusta = True
                        Else
                            usuMeGusta = New cnUsuario
                            With usuMeGusta
                                If IsDBNull(rowMeGusta("USU")) Then
                                    .ProveCod = rowMeGusta("PROVE")
                                    .Con = rowMeGusta("CON")
                                    .Nombre = IIf(rowMeGusta("NOMPROVE") Is DBNull.Value, rowMeGusta("APEPROVE"), rowMeGusta("NOMPROVE") & " " & rowMeGusta("APEPROVE"))
                                    .SituacionUO = rowMeGusta("PROVEDEN")
                                Else
                                    .Cod = rowMeGusta("USU")
                                    .Nombre = IIf(rowMeGusta("NOM") Is DBNull.Value, rowMeGusta("APE"), rowMeGusta("NOM") & " " & rowMeGusta("APE"))
                                    .SituacionUO = rowMeGusta("SITUACIONUO")
                                End If
                            End With
                            .MeGusta.UsuariosMeGusta.Add(usuMeGusta)
                        End If
                        If UltimoMeGustaUsu AndAlso Not IsDBNull(rowMeGusta("PROVE")) AndAlso rowMeGusta("PROVE") = ProveCod AndAlso rowMeGusta("CON") = IdCon Then
                            .MeGusta.UltimoMeGustaUsu = True
                        Else
                            UltimoMeGustaUsu = False
                        End If
                    Next
                End If

                'ADJUNTOS
                .Adjuntos = New List(Of cnAdjunto)
                For Each rowAdjunto As DataRow In Adjuntos.Select("MEN=" & row("ID") & " AND RESP IS NULL")
                    iAdjunto = New cnAdjunto
                    With iAdjunto
                        .IdAdjunto = rowAdjunto("ID")
                        .Nombre = rowAdjunto("NOMBRE").ToString
                        .Guid = rowAdjunto("DGUID").ToString
                        .Size = DBNullToDbl(rowAdjunto("SIZE"))
                        .SizeUnit = rowAdjunto("SIZEUNIT").ToString
                        .SizeToString = modUtilidades.FormatNumber(.Size, NumberFormat) & " " & .SizeUnit
                        .TipoAdjunto = rowAdjunto("TIPOADJUNTO")
                        .Url = rowAdjunto("DGUID").ToString
                        Select Case .TipoAdjunto
                            Case 0
                                .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "images/link.png"
                            Case 1, 2
                                .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "script/cn/" & "Thumbnail.ashx?f=" & _
                                                iAdjunto.Guid & "&t=" & .TipoAdjunto
                            Case 11
                                .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "images/excelAttach.png"
                            Case 12
                                .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "images/wordAttach.png"
                            Case 13
                                .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "images/pdfAttach.png"
                            Case Else
                                .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "images/Attach.png"
                        End Select
                    End With
                    .Adjuntos.Add(iAdjunto)
                Next
            End With

            Return ocnMensaje
        End Function
        Public Function CN_Mensajes_GetAdjuntoMensaje(ByVal lCiaComp As Integer, ByVal DGuid As String) As Byte()
            Authenticate()
            Return DBServer.CN_Mensajes_GetAdjuntoMensaje(lCiaComp, DGuid)
        End Function
        Public Function CN_Mensajes_GetImagenUsuario(ByVal lCiaComp As Integer, ByVal DGuid As String) As Byte()
            Authenticate()
            Return DBServer.CN_Mensajes_GetImagenUsuario(lCiaComp, DGuid)
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="oRespueta"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function InsertarRespuesta(ByVal lCiaComp As Integer, ByVal oRespueta As cnRespuesta, ByVal dtAdjuntos As DataTable) As Integer
            Authenticate()
            With oRespueta
                Dim IdRespuesta As Integer = DBServer.CN_InsertarRespuesta(lCiaComp, .InfoUsuario.ProveCod, _
                                .InfoUsuario.Con, .IdMensaje, .FechaAlta, .Contenido, .MeGusta.MeGusta, _
                                .IdMensajeCitado, dtAdjuntos, msSesionId, msIPDir, msPersistID)
                Return IdRespuesta
            End With
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="ProveCod"></param>
        ''' <param name="IdCon"></param>
        ''' <param name="Idioma"></param>
        ''' <param name="UsuFormatDate"></param>
        ''' <param name="UsuNumberFormat"></param>
        ''' <param name="IdMensaje"></param>
        ''' <param name="IdRespuesta"></param>
        ''' <param name="TimeZoneOffSet"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function InsertarMeGusta(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdCon As Integer, ByVal Idioma As Fullstep.FSNLibrary.Idioma, _
                                        ByVal UsuFormatDate As String, ByVal UsuNumberFormat As Globalization.NumberFormatInfo, _
                                        ByVal IdMensaje As Integer, ByVal IdRespuesta As Integer, ByVal TimeZoneOffSet As Double) As cnMensaje
            Authenticate()

            Dim dsDatos As New DataSet
            dsDatos = DBServer.CN_InsertarMeGusta(lCiaComp, ProveCod, IdCon, Idioma, IdMensaje, IdRespuesta, msSesionId, msIPDir, msPersistID)

            Return CrearMensaje(dsDatos.Tables("MENSAJES").Rows(0), Idioma, ProveCod, IdCon, UsuFormatDate, UsuNumberFormat, _
                                dsDatos.Tables("RESPUESTAS"), dsDatos.Tables("MEGUSTA"), _
                                dsDatos.Tables("MEGUSTARESPUESTA"), dsDatos.Tables("ADJUNTOS"), False, TimeZoneOffSet)
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="Guid"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Obtener_Nombre_Adjunto(ByVal lCiaComp As Integer, ByVal DGuid As String) As String
            Authenticate()

            Dim row As DataRow = DBServer.CN_Obtener_Nombre_Adjunto(lCiaComp, DGuid, msSesionId, msIPDir, msPersistID).Rows(0)
            Return row("NOMBRE")
        End Function
        ' ''' <summary>
        ' ''' 
        ' ''' </summary>
        ' ''' <param name="UsuCod"></param>
        ' ''' <param name="Idioma"></param>
        ' ''' <param name="UsuFormatDate"></param>
        ' ''' <param name="UsuNumberFormat"></param>
        ' ''' <param name="IdMensaje"></param>
        ' ''' <returns></returns>
        ' ''' <remarks></remarks>
        'Public Function Obtener_Mensaje(ByVal UsuCod As String, ByVal Idioma As Fullstep.FSNLibrary.Idioma, _
        '                            ByVal UsuFormatDate As String, ByVal UsuNumberFormat As Globalization.NumberFormatInfo, _
        '                            ByVal IdMensaje As Integer, ByVal TimeZoneOffSet As Double) As cnMensaje
        '    Authenticate()

        '    Dim dsDatos As New DataSet
        '    dsDatos = DBServer.CN_Obtener_Mensaje(UsuCod, Idioma, IdMensaje)

        '    Return CrearMensaje(dsDatos.Tables("MENSAJES").Rows(0), Idioma, UsuCod, UsuFormatDate, UsuNumberFormat, _
        '                        dsDatos.Tables("RESPUESTAS"), dsDatos.Tables("MEGUSTA"), _
        '                        dsDatos.Tables("MEGUSTARESPUESTA"), dsDatos.Tables("ADJUNTOS"), False, TimeZoneOffSet)
        'End Function
        ' ''' <summary>
        ' ''' 
        ' ''' </summary>
        ' ''' <param name="UsuCod"></param>
        ' ''' <param name="Idioma"></param>
        ' ''' <param name="UsuFormatDate"></param>
        ' ''' <param name="UsuNumberFormat"></param>
        ' ''' <param name="IdMensaje"></param>
        ' ''' <returns></returns>
        ' ''' <remarks></remarks>
        'Public Function MensajeUrgente_Leido(ByVal UsuCod As String, ByVal Idioma As Fullstep.FSNLibrary.Idioma, _
        '                            ByVal UsuFormatDate As String, ByVal UsuNumberFormat As Globalization.NumberFormatInfo, _
        '                            ByVal IdMensaje As Integer, ByVal TimeZoneOffSet As Double) As cnMensaje
        '    Authenticate()

        '    Dim dsDatos As New DataSet
        '    dsDatos = DBServer.CN_MensajeUrgente_Leido(UsuCod, Idioma, IdMensaje)

        '    If dsDatos.Tables.Count = 0 Then
        '        Return Nothing
        '    Else
        '        Return CrearMensaje(dsDatos.Tables("MENSAJES").Rows(0), Idioma, UsuCod, UsuFormatDate, UsuNumberFormat, _
        '                        dsDatos.Tables("RESPUESTAS"), dsDatos.Tables("MEGUSTA"), _
        '                        dsDatos.Tables("MEGUSTARESPUESTA"), dsDatos.Tables("ADJUNTOS"), False, TimeZoneOffSet)
        '    End If
        'End Function
        ' ''' <summary>
        ' ''' Oculta el mensaje seleccionado
        ' ''' </summary>
        ' ''' <param name="UsuCod">Código de usuario que oculta el mensaje</param>
        ' ''' <param name="IdMensaje">Id del mensaje a ocultar</param>
        ' ''' <remarks></remarks>
        'Public Sub OcultarMensaje(ByVal UsuCod As String, ByVal IdMensaje As Integer)
        '    Authenticate()

        '    DBServer.CN_OcultarMensaje(UsuCod, IdMensaje)
        'End Sub
#End Region
#Region "Notificaciones"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="UsuCod"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Comprobar_Notificaciones(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdCon As Integer) As List(Of cn_fsItem)
            Authenticate()

            Dim dsDatos As New DataSet
            dsDatos = DBServer.CN_Comprobar_Notificaciones(lCiaComp, ProveCod, IdCon, msSesionId, msIPDir, msPersistID)

            Dim oContadoresNuevosMensajes As New List(Of cn_fsItem)
            Dim item As cn_fsItem
            Dim totalNuevos As Integer = 0
            For Each row As DataRow In dsDatos.Tables("NOTIFICACIONES").Rows
                item = New cn_fsItem
                item.value = row("TIPO")
                item.text = row("NOTIFICACIONES")
                totalNuevos += CType(item.text, Integer)
                oContadoresNuevosMensajes.Add(item)
            Next
            If totalNuevos > 0 Then
                item = New cn_fsItem
                item.value = 0
                item.text = totalNuevos
                oContadoresNuevosMensajes.Add(item)
            End If
            For Each row As DataRow In dsDatos.Tables("URGENTES").Rows
                item = New cn_fsItem
                item.value = row("TIPO")
                item.text = row("MEN")
                oContadoresNuevosMensajes.Add(item)
            Next
            Return oContadoresNuevosMensajes
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="UsuCod"></param>
        ''' <param name="Idioma"></param>
        ''' <param name="UsuFormatDate"></param>
        ''' <param name="UsuNumberFormat"></param>
        ''' <param name="IdMensaje"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Obtener_Mensaje(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdCon As Integer, ByVal Idioma As Fullstep.FSNLibrary.Idioma, _
                                    ByVal UsuFormatDate As String, ByVal UsuNumberFormat As Globalization.NumberFormatInfo, _
                                    ByVal IdMensaje As Integer, ByVal TimeZoneOffSet As Double) As cnMensaje
            Authenticate()

            Dim dsDatos As New DataSet
            dsDatos = DBServer.CN_Obtener_Mensaje(lCiaComp, ProveCod, IdCon, Idioma, IdMensaje, msSesionId, msIPDir, msPersistID)

            Return CrearMensaje(dsDatos.Tables("MENSAJES").Rows(0), Idioma, ProveCod, IdCon, UsuFormatDate, UsuNumberFormat, _
                                dsDatos.Tables("RESPUESTAS"), dsDatos.Tables("MEGUSTA"), _
                                dsDatos.Tables("MEGUSTARESPUESTA"), dsDatos.Tables("ADJUNTOS"), False, TimeZoneOffSet)
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="lCiaComp"></param>
        ''' <param name="ProveCod"></param>
        ''' <param name="IdCon"></param>
        ''' <param name="Idioma"></param>
        ''' <param name="UsuFormatDate"></param>
        ''' <param name="UsuNumberFormat"></param>
        ''' <param name="IdMensaje"></param>
        ''' <param name="TimeZoneOffSet"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function MensajeUrgente_Leido(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdCon As Integer, ByVal Idioma As Fullstep.FSNLibrary.Idioma, _
                                    ByVal UsuFormatDate As String, ByVal UsuNumberFormat As Globalization.NumberFormatInfo, _
                                    ByVal IdMensaje As Integer, ByVal TimeZoneOffSet As Double) As cnMensaje
            Authenticate()

            Dim dsDatos As New DataSet
            dsDatos = DBServer.CN_MensajeUrgente_Leido(lCiaComp, ProveCod, IdCon, Idioma, IdMensaje, msSesionId, msIPDir, msPersistID)

            If dsDatos.Tables.Count = 0 Then
                Return Nothing
            Else
                Return CrearMensaje(dsDatos.Tables("MENSAJES").Rows(0), Idioma, ProveCod, IdCon, UsuFormatDate, UsuNumberFormat, _
                                dsDatos.Tables("RESPUESTAS"), dsDatos.Tables("MEGUSTA"), _
                                dsDatos.Tables("MEGUSTARESPUESTA"), dsDatos.Tables("ADJUNTOS"), False, TimeZoneOffSet)
            End If
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="lCiaComp"></param>
        ''' <param name="ProveCod"></param>
        ''' <param name="IdCon"></param>
        ''' <param name="IdMensaje"></param>
        ''' <param name="Ocultar"></param>
        ''' <remarks></remarks>
        Public Sub OcultarMostrarMensaje(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdCon As Integer, _
                                              ByVal IdMensaje As Integer, ByVal Ocultar As Boolean)
            Authenticate()

            DBServer.CN_OcultarMensaje(lCiaComp, ProveCod, IdCon, IdMensaje, Ocultar, msSesionId, msIPDir, msPersistID)
        End Sub
#End Region
#Region "Buscador"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="lCiaComp"></param>
        ''' <param name="ProveCod"></param>
        ''' <param name="IdCon"></param>
        ''' <param name="Idioma"></param>
        ''' <param name="Idioma_FTS"></param>
        ''' <param name="UsuFormatDate"></param>
        ''' <param name="NumbreFormat"></param>
        ''' <param name="TipoOrdenacion"></param>
        ''' <param name="TextoBusqueda"></param>
        ''' <param name="TimeZoneOffSet"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ObtenerResultadosBusqueda(ByVal lCiaComp As Integer, ByVal ProveCod As String, ByVal IdCon As Integer, ByVal Idioma As Fullstep.FSNLibrary.Idioma, ByVal Idioma_FTS As Integer, _
                            ByVal UsuFormatDate As String, ByVal NumbreFormat As System.Globalization.NumberFormatInfo, _
                            ByVal TipoOrdenacion As Integer, ByVal TextoBusqueda As String, ByVal TimeZoneOffSet As Double) As List(Of cnMensaje)
            Authenticate()

            Try
                Dim dsDatos As New DataSet
                dsDatos = DBServer.CN_ObtenerResultadosBusqueda(lCiaComp, ProveCod, IdCon, Idioma, Idioma_FTS, _
                                                                TipoOrdenacion, TextoBusqueda, msSesionId, msIPDir, msPersistID)

                Dim oMensajesUsuario As New List(Of cnMensaje)

                For Each row As DataRow In dsDatos.Tables("MENSAJES").Rows
                    With oMensajesUsuario
                        .Add(CrearMensaje(row, Idioma, ProveCod, IdCon, UsuFormatDate, NumbreFormat, _
                                          dsDatos.Tables("RESPUESTAS"), dsDatos.Tables("MEGUSTA"), _
                                          dsDatos.Tables("MEGUSTARESPUESTA"), dsDatos.Tables("ADJUNTOS"), True, TimeZoneOffSet))
                    End With
                Next

                Return oMensajesUsuario
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region
    End Class
End Namespace