﻿Imports System.ServiceModel

Namespace Fullstep.PMPortalServer
    <Serializable()>
    Public Class ValidacionesIntegracion
        Inherits Security

        Private moLongitudesDeCodigos As TiposDeDatos.LongitudesDeCodigos

        ''' Revisado por: blp. Fecha: 02/02/2012
        ''' <summary>
        ''' Método para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">código de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petición</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde la creación de instancias. Máx. 0,1 seg.</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

        ''' <summary>
        ''' Función que nos dice si hay integracion entrada salida en alguna de las tablas de un array
        ''' </summary>
        ''' <param name="tablas">Array de Id's de tabla para los que se quiere averiguar si hay integración</param>
        ''' <param name="lCiaComp">Código de la Cliente</param>
        ''' <returns>Booleano</returns>
        ''' <remarks>Llamada desde seguimiento.aspx.vb y Recepcion.aspx.vb
        ''' Tiempo máximo 0,1 sec</remarks>
        Public Function HayIntegracionEntradaoEntradaSalida(ByVal tablas As Integer(), ByVal lCiaComp As Long) As Boolean
            Dim hayIntegracion As Boolean = False
            For Each tabla As Integer In tablas
                hayIntegracion = DBServer.HayIntegracionEntradaoEntradaSalida(tabla, lCiaComp)
                If hayIntegracion Then Exit For
            Next
            HayIntegracionEntradaoEntradaSalida = hayIntegracion
        End Function

        ''' <summary>
        ''' Función que nos dice si hay integracion entrada salida en alguna de las tablas de un array
        ''' </summary>
        ''' <param name="tablas">Array de Id's de tabla para los que se quiere averiguar si hay integración</param>
        ''' <param name="lCiaComp">Código de la Cliente</param>
        ''' <returns>Booleano</returns>
        ''' <remarks>Llamada desde seguimiento.aspx.vb y Recepcion.aspx.vb
        ''' Tiempo máximo 0,1 sec</remarks>
        Public Function HayIntegracionSalidaoEntradaSalida(ByVal tablas As Integer(), ByVal lCiaComp As Long) As Boolean
            Dim hayIntegracion As Boolean = False
            For Each tabla As Integer In tablas
                hayIntegracion = DBServer.HayIntegracionSalidaoEntradaSalida(tabla, lCiaComp)
                If hayIntegracion Then Exit For
            Next
            HayIntegracionSalidaoEntradaSalida = hayIntegracion
        End Function
        ''' <summary>Devuelve True si existe integración de salida (o entrada y salida) con WCF para una empresa y una entidad</summary>
        ''' <param name="entidad">Entidad</param>
        ''' <param name="empresa">Empresa</param>
        ''' <param name="erp">ERP</param>
        ''' <returns>True si existe integración. False si no.</returns>
        ''' <remarks>Llamada desde ValidacionesIntegracion.vb\ControlMapper()</remarks>
        Public Function HayIntegracionSalidaWCF(ByVal lCiaComp As Long, ByVal entidad As Integer, Optional ByVal empresa As Integer = 0, Optional ByVal erp As Integer = 0) As Boolean
            Authenticate()

            If mRemottingServer Then
                Return DBServer.HayIntegracionSalidaWCF(lCiaComp, entidad, empresa, erp, msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.HayIntegracionSalidaWCF(lCiaComp, entidad, empresa, erp)
            End If
        End Function

        ' ''' <summary>
        ' ''' Función que llama a la mapper para hacer las validaciones de integracion
        ' ''' </summary>
        ' ''' <param name="iNumError">Numero de error a devolver</param>
        ' ''' <param name="lCiaComp">Id de la compañia</param>
        ' ''' <param name="lIdEmpresa">Id de la empresa</param>
        ' ''' <param name="sCodProve">Cod del proveedor</param>
        ' ''' <param name="lIdFactura">Id de la factura</param>
        ' ''' <param name="lIdAccion">Id de la acción</param>
        ' ''' <returns>Devuelve True si no ha habido errores, False si los ha habido</returns>
        ' ''' <remarks>Llamada desde Instancia.ControlMapperFactura()
        ' ''' Tiempo máximo 1 sec</remarks>
        Public Function EvalMapperFacturas(ByRef iNumError As Short, ByVal lCiaComp As Long, ByVal lIdEmpresa As Long, ByVal sCodProve As String, ByVal lIdFactura As Long, ByVal lIdAccion As Long) As Boolean
            Dim Maper As Object = ""
            Dim NomMapper As String = ""
            Try

                NomMapper = DBServer.Validaciones_DevolverMapper(lCiaComp, lIdEmpresa)
                Maper = CreateObject(NomMapper & ".clsInPedidosDirectos")
                If Not Maper Is Nothing Then
                    EvalMapperFacturas = Maper.ValidarFactura(iNumError, sCodProve, lIdFactura, lIdAccion)
                End If

            Catch ex As Exception
                EvalMapperFacturas = True
            Finally
                Maper = Nothing
            End Try
        End Function
		''' <summary>Función que hará las validaciones de integración en el momento de emitir un solicitud PM </summary>
		''' <param name="ds">Dataset con los datos para validar</param>
		''' <param name="idioma">Codigo de idioma</param>
		''' <param name="iNumError">Posible numero de error</param>
		''' <param name="strError">posible texto de error</param>
		''' <param name="idInstancia">Id de instancia</param>
		''' <param name="idSolicitud ">Identificador de la solicitud</param>
		''' <param name="accionRol">Identificador de la acción</param>
		''' <param name="sBloquesDestino">Identificadores de los bloques destinos</param>
		''' <param name="peticionario">Codigo del peticionario</param>
		''' <param name="peticionarioProve">Codigo del proveedor que actúa de peticionario</param>
		''' <param name="peticionarioProveCon">Id del contacto del proveedor que ha emitico la solicitud</param>
		''' <returns>True si ha ido Ok, False si ha habido error</returns>
		''' <remarks>Llamada desde Instancia.vb\ControlMapper()</remarks>
		Public Function ControlMapper(ByVal lCiaComp As Long, ByVal FSPMServer As PMPortalServer.Root, ByVal ds As DataSet, ByVal idioma As String,
									  ByRef iNumError As Short, ByRef strError As String, Optional ByVal idInstancia As Long = Nothing,
									  Optional ByVal idSolicitud As Long = Nothing, Optional ByVal accionRol As Long = 0,
									  Optional ByVal sBloquesDestino As String = "", Optional ByVal peticionario As String = Nothing,
									  Optional ByVal peticionarioProve As String = "", Optional ByVal peticionarioProveCon As Integer = 0) As TipoValidacionIntegracion
			Dim dsMatArt As DataSet

			ControlMapper = TipoValidacionIntegracion.SinMensaje
			strError = ""
			Try
				FSPMServer.Load_LongitudesDeCodigos(lCiaComp)
				moLongitudesDeCodigos = FSPMServer.LongitudesDeCodigos
				dsMatArt = CreaListas(ds, idioma, idInstancia, idSolicitud, accionRol, peticionario, peticionarioProve, peticionarioProveCon)

				If Not HayIntegracionSalidaWCF(lCiaComp, FSNLibrary.TablasIntegracion.PM) Then
					ControlMapper = EvalMapper(lCiaComp, dsMatArt, idioma, iNumError, strError, peticionario, peticionarioProve, peticionarioProveCon, idInstancia, idSolicitud, accionRol, sBloquesDestino)
				Else
					iNumError = -100
					ControlMapper = ValidacionesWCF_Portal(lCiaComp, strError, dsMatArt, idioma, sBloquesDestino)
				End If
			Catch ex As Exception 'ok no hay mapper
				ControlMapper = TipoValidacionIntegracion.SinMensaje
			End Try
		End Function
		''' <summary>Esta función devuelve un data set con los campos de la instancia necesarios para las validaciones de integración desde la mapper.</summary>
		''' <param name="ds">DataSet con los datos de entrada</param>
		''' <param name="Idioma">Idioma del usuario</param>
		''' <param name="idInstancia">Id de instancia</param>
		''' <param name="idSolicitud ">Identificador de la solicitud</param>
		''' <param name="accionRol">Identificador de la acción</param>
		''' <param name="peticionario">Codigo del peticionario</param>
		''' <param name="peticionarioProve">Codigo del proveedor que actúa de peticionario</param>
		''' <param name="peticionarioProveCon">Id del contacto del proveedor que ha emitico la solicitud</param>
		''' <returns>DataSet con los datos</returns>
		''' <remarks>Llamada desde: PMPortalServer.ValidacionesIntegracion.ControlMapper</remarks>
		Private Function CreaListas(ByVal ds As DataSet, ByVal Idioma As String, Optional ByVal idInstancia As Long = Nothing, Optional ByVal idSolicitud As Long = Nothing, Optional ByVal accionRol As Long = 0,
                                    Optional ByVal peticionario As String = Nothing, Optional ByVal peticionarioProve As String = "", Optional ByVal peticionarioProveCon As Integer = 0) As DataSet
            Dim dsMatArt As New DataSet
            Dim dtNoDesglMat, dtNoDesglArt, dtNoDesglAlm, dtNoDesglPre, dtNoDesglAtb, dtNoDesglProve, dtNoDesglCentroSM, dtNoDesglTipoPedido, dtNoDesglPartida, dtNoDesglFecIniRectif, dtNoDesglFecFinRectif,
                dtNoDesglFecIniSuministro, dtNoDesglFecFinSuministro, dtNoDesglOrgCompras, dtNoDesglCen, dtNoDesglPersona, dtSiDesglMat, dtSiDesglArt, dtSiDesglAlm, dtSiDesglPre, dtSiDesglAtb, dtSiDesglCen,
                dtSiDesglOrgCompras, dtSiDesglTablaExt, dtSiDesglImporteLin, dtSiDesglCant, dtSiDesglPrecUn, dtSiDesglPartida, dtSiDesglFecIniRectif, dtSiDesglFecFinRectif, dtSiDesglFecIniSuministro,
                dtSiDesglFecFinSuministro As DataTable

            Dim dsInforGeneral As New DataTable

            With dsMatArt.Tables
                dsInforGeneral = .Add("InforGen")
                dtNoDesglMat = .Add("NoDMa")
                dtNoDesglArt = .Add("NoDAr")
                dtNoDesglAlm = .Add("NoDAl")
                dtNoDesglPre = .Add("NoDPr")
                dtNoDesglAtb = .Add("NoDAt")
                dtNoDesglProve = .Add("NoDProve")
                dtNoDesglCentroSM = .Add("NoDCenSM")
                dtNoDesglTipoPedido = .Add("NoDTipoPed")
                dtNoDesglPartida = .Add("NoDPartida")
                dtNoDesglFecIniRectif = .Add("NoDFecIniRectif")
                dtNoDesglFecFinRectif = .Add("NoDFecFinRectif")
                dtNoDesglFecIniSuministro = .Add("NoDFecIniSuministro")
                dtNoDesglFecFinSuministro = .Add("NoDFecFinSuministro")
                dtNoDesglOrgCompras = .Add("NoDOr")
                dtNoDesglCen = .Add("NoDCe")
                dtNoDesglPersona = .Add("NoDPersona")
                dtSiDesglMat = .Add("SiDMa")
                dtSiDesglArt = .Add("SiDAr")
                dtSiDesglAlm = .Add("SiDAl")
                dtSiDesglPre = .Add("SiDPr")
                dtSiDesglAtb = .Add("SiDAt")
                dtSiDesglCen = .Add("SiDCe")
                dtSiDesglOrgCompras = .Add("SiDOr")
                dtSiDesglTablaExt = .Add("SiDTExt")
                dtSiDesglImporteLin = .Add("SiDIm")
                dtSiDesglCant = .Add("SiDCant")
                dtSiDesglPrecUn = .Add("SiDPrec")
                dtSiDesglPartida = .Add("SiDPartida")
                dtSiDesglFecIniRectif = .Add("SiDFecIniRectif")
                dtSiDesglFecFinRectif = .Add("SiDFecFinRectif")
                dtSiDesglFecIniSuministro = .Add("SiDFecIniSuministro")
                dtSiDesglFecFinSuministro = .Add("SiDFecFinSuministro")
            End With

            With dtNoDesglMat.Columns
                .Add("GRUPO", Type.GetType("System.Int32"))
                .Add("ORDEN", Type.GetType("System.Int32"))
                .Add("GMN1", Type.GetType("System.String"))
                .Add("GMN2", Type.GetType("System.String"))
                .Add("GMN3", Type.GetType("System.String"))
                .Add("GMN4", Type.GetType("System.String"))
            End With
            With dtNoDesglArt.Columns
                .Add("GRUPO", Type.GetType("System.Int32"))
                .Add("ORDEN", Type.GetType("System.Int32"))
                .Add("ART4", Type.GetType("System.String"))
            End With
            With dtNoDesglAlm.Columns
                .Add("GRUPO", Type.GetType("System.Int32"))
                .Add("ALMACEN", Type.GetType("System.String"))
            End With
            With dtNoDesglPre.Columns
                .Add("GRUPO", Type.GetType("System.Int32"))
                .Add("PRESUP4", Type.GetType("System.String"))
            End With
            With dtNoDesglProve.Columns
                .Add("GRUPO", Type.GetType("System.Int32"))
                .Add("PROVE", Type.GetType("System.String"))
            End With
            With dtNoDesglCentroSM.Columns
                .Add("GRUPO", Type.GetType("System.Int32"))
                .Add("CENTRO_SM", Type.GetType("System.String"))
            End With
            With dtNoDesglTipoPedido.Columns
                .Add("GRUPO", Type.GetType("System.Int32"))
                .Add("TIPO_PEDIDO", Type.GetType("System.String"))
            End With
            With dtNoDesglAtb.Columns
                .Add("GRUPO", Type.GetType("System.Int32"))
                .Add("ID", Type.GetType("System.Int32"))
                .Add("VALOR", Type.GetType("System.String"))
                .Add("VALERP", Type.GetType("System.Int32"))
            End With
            With dtNoDesglPartida.Columns
                .Add("GRUPO", Type.GetType("System.Int32"))
                .Add("PARTIDA", Type.GetType("System.String"))
            End With
            With dtNoDesglFecIniRectif.Columns
                .Add("GRUPO", Type.GetType("System.Int32"))
                .Add("FECINIRECTIF", Type.GetType("System.DateTime"))
            End With
            With dtNoDesglFecFinRectif.Columns
                .Add("GRUPO", Type.GetType("System.Int32"))
                .Add("FECFINRECTIF", Type.GetType("System.DateTime"))
            End With
            With dtNoDesglFecIniSuministro.Columns
                .Add("GRUPO", Type.GetType("System.Int32"))
                .Add("FECINISUMINISTRO", Type.GetType("System.DateTime"))
            End With
            With dtNoDesglFecFinSuministro.Columns
                .Add("GRUPO", Type.GetType("System.Int32"))
                .Add("FECFINSUMINISTRO", Type.GetType("System.DateTime"))
            End With
            With dtNoDesglOrgCompras.Columns
                .Add("GRUPO", Type.GetType("System.Int32"))
                .Add("ORGCOMPRAS", Type.GetType("System.String"))
            End With
            With dtNoDesglCen.Columns
                .Add("GRUPO", Type.GetType("System.Int32"))
                .Add("CENTRO", Type.GetType("System.String"))
            End With
            With dtNoDesglPersona.Columns
                .Add("GRUPO", Type.GetType("System.Int32"))
                .Add("PER", Type.GetType("System.String"))
            End With
            With dtSiDesglMat.Columns
                .Add("LINEA", Type.GetType("System.Int32"))
                .Add("PADRE", Type.GetType("System.Int32"))
                .Add("GMN1", Type.GetType("System.String"))
                .Add("GMN2", Type.GetType("System.String"))
                .Add("GMN3", Type.GetType("System.String"))
                .Add("GMN4", Type.GetType("System.String"))
            End With
            With dtSiDesglArt.Columns
                .Add("LINEA", Type.GetType("System.Int32"))
                .Add("PADRE", Type.GetType("System.Int32"))
                .Add("ART4", Type.GetType("System.String"))
            End With
            With dtSiDesglAlm.Columns
                .Add("LINEA", Type.GetType("System.Int32"))
                .Add("PADRE", Type.GetType("System.Int32"))
                .Add("ALMACEN", Type.GetType("System.String"))
            End With
            With dtSiDesglPre.Columns
                .Add("LINEA", Type.GetType("System.Int32"))
                .Add("PADRE", Type.GetType("System.Int32"))
                .Add("PRESUP4", Type.GetType("System.String"))
            End With
            With dtSiDesglAtb.Columns
                .Add("LINEA", Type.GetType("System.Int32"))
                .Add("PADRE", Type.GetType("System.Int32"))
                .Add("ID", Type.GetType("System.Int32"))
                .Add("VALOR", Type.GetType("System.String"))
                .Add("VALERP", Type.GetType("System.String"))
            End With
            With dtSiDesglCen.Columns
                .Add("LINEA", Type.GetType("System.Int32"))
                .Add("PADRE", Type.GetType("System.Int32"))
                .Add("CENTRO", Type.GetType("System.String"))
            End With
            With dtSiDesglOrgCompras.Columns
                .Add("LINEA", Type.GetType("System.Int32"))
                .Add("PADRE", Type.GetType("System.Int32"))
                .Add("ORGCOMPRAS", Type.GetType("System.String"))
            End With
            With dtSiDesglTablaExt.Columns
                .Add("LINEA", Type.GetType("System.Int32"))
                .Add("PADRE", Type.GetType("System.Int32"))
                .Add("TABLA_EXTERNA", Type.GetType("System.Int32"))
                .Add("VALOR_TABLAEXT", Type.GetType("System.String"))
            End With
            With dtSiDesglImporteLin.Columns
                .Add("LINEA", Type.GetType("System.Int32"))
                .Add("PADRE", Type.GetType("System.Int32"))
                .Add("CALCULADO", Type.GetType("System.Int32"))
                .Add("VALOR_IMPORTE", Type.GetType("System.String"))
            End With
            With dtSiDesglCant.Columns
                .Add("LINEA", Type.GetType("System.Int32"))
                .Add("PADRE", Type.GetType("System.Int32"))
                .Add("CANTIDAD", Type.GetType("System.Double"))
            End With
            With dtSiDesglPrecUn.Columns
                .Add("LINEA", Type.GetType("System.Int32"))
                .Add("PADRE", Type.GetType("System.Int32"))
                .Add("PRECIO", Type.GetType("System.Double"))
            End With
            With dtSiDesglPartida.Columns
                .Add("LINEA", Type.GetType("System.Int32"))
                .Add("PADRE", Type.GetType("System.Int32"))
                .Add("PARTIDA", Type.GetType("System.String"))
            End With
            With dtSiDesglFecIniRectif.Columns
                .Add("LINEA", Type.GetType("System.Int32"))
                .Add("PADRE", Type.GetType("System.Int32"))
                .Add("FECINIRECTIF", Type.GetType("System.DateTime"))
            End With
            With dtSiDesglFecFinRectif.Columns
                .Add("LINEA", Type.GetType("System.Int32"))
                .Add("PADRE", Type.GetType("System.Int32"))
                .Add("FECFINRECTIF", Type.GetType("System.DateTime"))
            End With
            With dtSiDesglFecIniSuministro.Columns
                .Add("LINEA", Type.GetType("System.Int32"))
                .Add("PADRE", Type.GetType("System.Int32"))
                .Add("FECINISUMINISTRO", Type.GetType("System.DateTime"))
            End With
            With dtSiDesglFecFinSuministro.Columns
                .Add("LINEA", Type.GetType("System.Int32"))
                .Add("PADRE", Type.GetType("System.Int32"))
                .Add("FECFINSUMINISTRO", Type.GetType("System.DateTime"))
            End With
            With dsInforGeneral.Columns
                .Add("PERSONA", Type.GetType("System.String"))
                .Add("PETPROVE", Type.GetType("System.String"))
                .Add("PETPROVECON", Type.GetType("System.Int32"))
                .Add("INSTANCIA", Type.GetType("System.Int32"))
                .Add("SOLICITUD", Type.GetType("System.Int32"))
                .Add("ACCION", Type.GetType("System.Int32"))
            End With

            Dim row As DataRow
            Dim dtNew As DataRow
            Dim sMaterial As String
            Dim iDato As Integer
            Dim iGrupo As Integer
            Dim lTablaExt As Long

            Dim TipoGs As Integer

            Try
                'Información General
                dtNew = dsInforGeneral.NewRow
                dtNew.Item("PERSONA") = IIf(IsNothing(peticionario), "", peticionario)
                dtNew.Item("PETPROVE") = IIf(IsNothing(peticionarioProve), "", peticionarioProve)
                dtNew.Item("PETPROVECON") = IIf(IsNothing(peticionarioProveCon), "", peticionarioProveCon)
                dtNew.Item("INSTANCIA") = IIf(IsNothing(idInstancia), 0, idInstancia)
                dtNew.Item("SOLICITUD") = IIf(IsNothing(idSolicitud), 0, idSolicitud)
                dtNew.Item("ACCION") = IIf(IsNothing(accionRol), 0, accionRol)
                dsInforGeneral.Rows.Add(dtNew)

                For Each row In ds.Tables("TEMP").Rows
                    If Not IsDBNull(row("TIPOGS")) Then
                        If Not IsDBNull(row("VALOR_TEXT")) OrElse Not IsDBNull(row("VALOR_NUM")) OrElse Not IsDBNull(row("VALOR_FEC")) OrElse Not IsDBNull(row("VALOR_BOOL")) Then

                            TipoGs = row("TIPOGS")
                            If Not IsDBNull(row("GRUPO")) Then
                                iGrupo = row("GRUPO")
                            Else
                                iGrupo = 0
                            End If

                            If Not IsDBNull(row("VALOR_TEXT")) Then
                                Select Case TipoGs
                                    Case TiposDeDatos.TipoCampoGS.Material
                                        dtNew = dtNoDesglMat.NewRow
                                        dtNew.Item("GRUPO") = iGrupo
                                        dtNew.Item("ORDEN") = iDato
                                        sMaterial = row("VALOR_TEXT")
                                        dtNew.Item("GMN1") = Trim(Left(sMaterial, moLongitudesDeCodigos.giLongCodGMN1))
                                        sMaterial = Mid(sMaterial, moLongitudesDeCodigos.giLongCodGMN1 + 1)
                                        dtNew.Item("GMN2") = Trim(Left(sMaterial, moLongitudesDeCodigos.giLongCodGMN2))
                                        sMaterial = Mid(sMaterial, moLongitudesDeCodigos.giLongCodGMN2 + 1)
                                        dtNew.Item("GMN3") = Trim(Left(sMaterial, moLongitudesDeCodigos.giLongCodGMN3))
                                        sMaterial = Mid(sMaterial, moLongitudesDeCodigos.giLongCodGMN3 + 1)
                                        dtNew.Item("GMN4") = Trim(sMaterial)
                                        dtNoDesglMat.Rows.Add(dtNew)
                                    Case TiposDeDatos.TipoCampoGS.NuevoCodArticulo
                                        dtNew = dtNoDesglArt.NewRow
                                        dtNew.Item("GRUPO") = iGrupo
                                        dtNew.Item("ORDEN") = iDato
                                        dtNew.Item("ART4") = row("VALOR_TEXT")
                                        dtNoDesglArt.Rows.Add(dtNew)
                                    Case TiposDeDatos.TipoCampoGS.Pres4
                                        dtNew = dtNoDesglPre.NewRow
                                        dtNew.Item("GRUPO") = iGrupo
                                        dtNew.Item("PRESUP4") = row("VALOR_TEXT")
                                        dtNoDesglPre.Rows.Add(dtNew)
                                    Case TiposDeDatos.TipoCampoGS.Proveedor
                                        dtNew = dtNoDesglProve.NewRow
                                        dtNew.Item("GRUPO") = iGrupo
                                        dtNew.Item("PROVE") = row("VALOR_TEXT")
                                        dtNoDesglProve.Rows.Add(dtNew)
                                    Case TiposDeDatos.TipoCampoGS.CentroCoste
                                        dtNew = dtNoDesglCentroSM.NewRow
                                        dtNew.Item("GRUPO") = iGrupo
                                        dtNew.Item("CENTRO_SM") = row("VALOR_TEXT")
                                        dtNoDesglCentroSM.Rows.Add(dtNew)
                                    Case TiposDeDatos.TipoCampoGS.TipoPedido
                                        dtNew = dtNoDesglTipoPedido.NewRow
                                        dtNew.Item("GRUPO") = iGrupo
                                        dtNew.Item("TIPO_PEDIDO") = row("VALOR_TEXT")
                                        dtNoDesglTipoPedido.Rows.Add(dtNew)
                                    Case TiposDeDatos.TipoCampoGS.Partida
                                        dtNew = dtNoDesglPartida.NewRow
                                        dtNew.Item("GRUPO") = iGrupo
                                        dtNew.Item("PARTIDA") = row("VALOR_TEXT")
                                        dtNoDesglPartida.Rows.Add(dtNew)
                                    Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
                                        dtNew = dtNoDesglOrgCompras.NewRow
                                        dtNew.Item("GRUPO") = iGrupo
                                        dtNew.Item("ORGCOMPRAS") = row("VALOR_TEXT")
                                        dtNoDesglOrgCompras.Rows.Add(dtNew)
                                    Case TiposDeDatos.TipoCampoGS.Centro
                                        dtNew = dtNoDesglCen.NewRow
                                        dtNew.Item("GRUPO") = iGrupo
                                        dtNew.Item("CENTRO") = row("VALOR_TEXT")
                                        dtNoDesglCen.Rows.Add(dtNew)
                                    Case TiposDeDatos.TipoCampoGS.SinTipo
                                        If Not IsDBNull(row("IDATRIB")) Then
                                            dtNew = dtNoDesglAtb.NewRow
                                            dtNew.Item("GRUPO") = iGrupo
                                            dtNew.Item("ID") = row("IDATRIB")
                                            dtNew.Item("VALERP") = row("VALERP")
                                            dtNew.Item("VALOR") = row("VALOR_TEXT")
                                            dtNoDesglAtb.Rows.Add(dtNew)
                                        End If
                                    Case TiposDeDatos.TipoCampoGS.Persona
                                        dtNew = dtNoDesglPersona.NewRow
                                        dtNew.Item("GRUPO") = iGrupo
                                        dtNew.Item("PER") = row("VALOR_TEXT")
                                        dtNoDesglPersona.Rows.Add(dtNew)
                                    Case Else
                                        'Nada
                                End Select
                            ElseIf Not IsDBNull(row("VALOR_NUM")) Then
                                Select Case TipoGs
                                    Case TiposDeDatos.TipoCampoGS.Almacen
                                        dtNew = dtNoDesglAlm.NewRow
                                        dtNew.Item("GRUPO") = iGrupo
                                        dtNew.Item("ALMACEN") = row("VALOR_NUM")
                                        dtNoDesglAlm.Rows.Add(dtNew)
                                    Case TiposDeDatos.TipoCampoGS.SinTipo
                                        If Not IsDBNull(row("IDATRIB")) Then
                                            dtNew = dtNoDesglAtb.NewRow
                                            dtNew.Item("GRUPO") = iGrupo
                                            dtNew.Item("ID") = row("IDATRIB")
                                            dtNew.Item("VALERP") = row("VALERP")
                                            dtNew.Item("VALOR") = row("VALOR_NUM")
                                            dtNoDesglAtb.Rows.Add(dtNew)
                                        End If
                                End Select
                            ElseIf Not IsDBNull(row("VALOR_FEC")) Then
                                If TipoGs = TiposDeDatos.TipoCampoGS.InicioAbono Then
                                    dtNew = dtNoDesglFecIniRectif.NewRow
                                    dtNew.Item("GRUPO") = iGrupo
                                    dtNew.Item("FECINIRECTIF") = row("VALOR_FEC")
                                    dtNoDesglFecIniRectif.Rows.Add(dtNew)
                                End If
                                If TipoGs = TiposDeDatos.TipoCampoGS.FinAbono Then
                                    dtNew = dtNoDesglFecFinRectif.NewRow
                                    dtNew.Item("GRUPO") = iGrupo
                                    dtNew.Item("FECFINRECTIF") = row("VALOR_FEC")
                                    dtNoDesglFecFinRectif.Rows.Add(dtNew)
                                End If
                                If TipoGs = TiposDeDatos.TipoCampoGS.IniSuministro Then
                                    dtNew = dtNoDesglFecIniSuministro.NewRow
                                    dtNew.Item("GRUPO") = iGrupo
                                    dtNew.Item("FECINISUMINISTRO") = row("VALOR_FEC")
                                    dtNoDesglFecIniSuministro.Rows.Add(dtNew)
                                End If
                                If TipoGs = TiposDeDatos.TipoCampoGS.FinSuministro Then
                                    dtNew = dtNoDesglFecFinSuministro.NewRow
                                    dtNew.Item("GRUPO") = iGrupo
                                    dtNew.Item("FECFINSUMINISTRO") = row("VALOR_FEC")
                                    dtNoDesglFecFinSuministro.Rows.Add(dtNew)
                                End If
                            ElseIf Not IsDBNull(row("VALOR_BOOL")) Then
                                TipoGs = row("TIPOGS")
                                Select Case TipoGs
                                    Case TiposDeDatos.TipoCampoGS.SinTipo
                                        If Not IsDBNull(row("IDATRIB")) Then
                                            dtNew = dtNoDesglAtb.NewRow
                                            dtNew.Item("GRUPO") = iGrupo
                                            dtNew.Item("ID") = row("IDATRIB")
                                            dtNew.Item("VALERP") = row("VALERP")
                                            dtNew.Item("VALOR") = row("VALOR_BOOL")
                                            dtNoDesglAtb.Rows.Add(dtNew)
                                        End If
                                End Select
                            End If
                        End If
                    End If
                Next

                Dim dtNewS As DataRow
                For Each row In ds.Tables("TEMPDESGLOSE").Rows
                    'Tipo GS:
                    If Not IsDBNull(row("TIPOGS")) Then
                        If Not IsDBNull(row("VALOR_TEXT")) OrElse Not IsDBNull(row("VALOR_NUM")) Then
                            TipoGs = row("TIPOGS")
                            If Not IsDBNull(row("VALOR_TEXT")) Then
                                Select Case TipoGs
                                    Case TiposDeDatos.TipoCampoGS.Material
                                        dtNewS = dtSiDesglMat.NewRow
                                        dtNewS.Item("LINEA") = row("LINEA")
                                        dtNewS.Item("PADRE") = row("CAMPO_PADRE")
                                        sMaterial = row("VALOR_TEXT")
                                        dtNewS.Item("GMN1") = Trim(Left(sMaterial, moLongitudesDeCodigos.giLongCodGMN1))
                                        sMaterial = Mid(sMaterial, moLongitudesDeCodigos.giLongCodGMN1 + 1)
                                        dtNewS.Item("GMN2") = Trim(Left(sMaterial, moLongitudesDeCodigos.giLongCodGMN2))
                                        sMaterial = Mid(sMaterial, moLongitudesDeCodigos.giLongCodGMN2 + 1)
                                        dtNewS.Item("GMN3") = Trim(Left(sMaterial, moLongitudesDeCodigos.giLongCodGMN3))
                                        sMaterial = Mid(sMaterial, moLongitudesDeCodigos.giLongCodGMN3 + 1)
                                        dtNewS.Item("GMN4") = Trim(sMaterial)
                                        dtSiDesglMat.Rows.Add(dtNewS)
                                    Case TiposDeDatos.TipoCampoGS.NuevoCodArticulo
                                        dtNewS = dtSiDesglArt.NewRow
                                        dtNewS.Item("LINEA") = row("LINEA")
                                        dtNewS.Item("PADRE") = row("CAMPO_PADRE")
                                        dtNewS.Item("ART4") = Trim(row("VALOR_TEXT"))
                                        dtSiDesglArt.Rows.Add(dtNewS)
                                    Case TiposDeDatos.TipoCampoGS.Pres4
                                        dtNewS = dtSiDesglPre.NewRow
                                        dtNewS.Item("LINEA") = row("LINEA")
                                        dtNewS.Item("PADRE") = row("CAMPO_PADRE")
                                        dtNewS.Item("PRESUP4") = row("VALOR_TEXT")
                                        dtSiDesglPre.Rows.Add(dtNewS)
                                    Case TiposDeDatos.TipoCampoGS.SinTipo
                                        If Not IsDBNull(row("IDATRIB")) Then
                                            dtNewS = dtSiDesglAtb.NewRow
                                            dtNewS.Item("LINEA") = row("LINEA")
                                            dtNewS.Item("PADRE") = row("CAMPO_PADRE")
                                            dtNewS.Item("ID") = row("IDATRIB")
                                            dtNewS.Item("VALERP") = row("VALERP")
                                            dtNewS.Item("VALOR") = row("VALOR_TEXT")
                                            dtSiDesglAtb.Rows.Add(dtNewS)
                                            'TablaExterna
                                        ElseIf Not IsDBNull(row("TABLA_EXTERNA")) Then
                                            lTablaExt = row("TABLA_EXTERNA")
                                            If Not IsDBNull(row("VALOR_TEXT")) Then
                                                dtNewS = dtSiDesglTablaExt.NewRow
                                                dtNewS.Item("LINEA") = row("LINEA")
                                                dtNewS.Item("PADRE") = row("CAMPO_PADRE")
                                                dtNewS.Item("VALOR_TABLAEXT") = row("VALOR_TEXT")
                                                dtNewS.Item("TABLA_EXTERNA") = row("TABLA_EXTERNA")
                                                dtSiDesglTablaExt.Rows.Add(dtNewS)
                                            ElseIf Not IsDBNull(row("VALOR_NUM")) Then
                                                dtNewS = dtSiDesglTablaExt.NewRow
                                                dtNewS.Item("LINEA") = row("LINEA")
                                                dtNewS.Item("PADRE") = row("CAMPO_PADRE")
                                                dtNewS.Item("VALOR_TABLAEXT") = row("VALOR_NUM")
                                                dtNewS.Item("TABLA_EXTERNA") = row("TABLA_EXTERNA")
                                                dtSiDesglTablaExt.Rows.Add(dtNewS)
                                            End If
                                        End If
                                    Case TiposDeDatos.TipoCampoGS.Almacen
                                        dtNewS = dtSiDesglAlm.NewRow
                                        dtNewS.Item("LINEA") = row("LINEA")
                                        dtNewS.Item("PADRE") = row("CAMPO_PADRE")
                                        dtNewS.Item("ALMACEN") = row("VALOR_TEXT")
                                        dtSiDesglAlm.Rows.Add(dtNewS)
                                    Case TiposDeDatos.TipoCampoGS.Centro
                                        dtNew = dtSiDesglCen.NewRow
                                        dtNew.Item("LINEA") = row("LINEA")
                                        dtNew.Item("PADRE") = row("CAMPO_PADRE")
                                        dtNew.Item("CENTRO") = row("VALOR_TEXT")
                                        dtSiDesglCen.Rows.Add(dtNew)
                                    Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
                                        dtNew = dtSiDesglOrgCompras.NewRow
                                        dtNew.Item("LINEA") = row("LINEA")
                                        dtNew.Item("PADRE") = row("CAMPO_PADRE")
                                        dtNew.Item("ORGCOMPRAS") = row("VALOR_TEXT")
                                        dtSiDesglOrgCompras.Rows.Add(dtNew)
                                    Case TiposDeDatos.TipoCampoGS.Partida
                                        dtNew = dtSiDesglPartida.NewRow
                                        dtNew.Item("LINEA") = row("LINEA")
                                        dtNew.Item("PADRE") = row("CAMPO_PADRE")
                                        dtNew.Item("PARTIDA") = row("VALOR_TEXT")
                                        dtSiDesglPartida.Rows.Add(dtNew)
                                    Case Else
                                        'Nada
                                End Select
                            ElseIf Not IsDBNull(row("VALOR_NUM")) Then
                                Select Case TipoGs
                                    Case TiposDeDatos.TipoCampoGS.Almacen
                                        dtNewS = dtSiDesglAlm.NewRow
                                        dtNewS.Item("LINEA") = row("LINEA")
                                        dtNewS.Item("PADRE") = row("CAMPO_PADRE")
                                        dtNewS.Item("ALMACEN") = row("VALOR_NUM")
                                        dtSiDesglAlm.Rows.Add(dtNewS)
                                    Case TiposDeDatos.TipoCampoGS.Cantidad
                                        dtNew = dtSiDesglCant.NewRow
                                        dtNew.Item("LINEA") = row("LINEA")
                                        dtNew.Item("PADRE") = row("CAMPO_PADRE")
                                        dtNew.Item("CANTIDAD") = row("VALOR_NUM")
                                        dtSiDesglCant.Rows.Add(dtNew)
                                    Case TiposDeDatos.TipoCampoGS.PrecioUnitario
                                        dtNew = dtSiDesglPrecUn.NewRow
                                        dtNew.Item("LINEA") = row("LINEA")
                                        dtNew.Item("PADRE") = row("CAMPO_PADRE")
                                        dtNew.Item("PRECIO") = row("VALOR_NUM")
                                        dtSiDesglPrecUn.Rows.Add(dtNew)
                                    Case TiposDeDatos.TipoCampoGS.SinTipo
                                        If Not IsDBNull(row("IDATRIB")) Then
                                            dtNewS = dtSiDesglAtb.NewRow
                                            dtNewS.Item("LINEA") = row("LINEA")
                                            dtNewS.Item("PADRE") = row("CAMPO_PADRE")
                                            dtNewS.Item("ID") = row("IDATRIB")
                                            dtNewS.Item("VALERP") = row("VALERP")
                                            dtNewS.Item("VALOR") = row("VALOR_NUM")
                                            dtSiDesglAtb.Rows.Add(dtNewS)
                                        ElseIf Not IsDBNull(row("CALCULADO")) Then
                                            If (UCase(row("CALCULADO_TIT")) Like "*IMPORTE*LIN*" Or UCase(row("CALCULADO_TIT")) Like "*IMPORTE*LÍN*") Then
                                                dtNewS = dtSiDesglImporteLin.NewRow
                                                dtNewS.Item("LINEA") = row("LINEA")
                                                dtNewS.Item("PADRE") = row("CAMPO_PADRE")
                                                dtNewS.Item("CALCULADO") = row("CALCULADO")
                                                dtNewS.Item("VALOR_IMPORTE") = 0 'Inicializo
                                                'El valor del campo está actualizado en el dataset dsAux; buscamos el valor del campo
                                                Dim dtAux As DataRow
                                                Dim find(2) As Object
                                                find(0) = row("LINEA")
                                                find(1) = row("CAMPO_PADRE")
                                                find(2) = row("CAMPO_HIJO")
                                                dtAux = ds.Tables("TEMPDESGLOSE").Rows.Find(find)
                                                dtNewS.Item("VALOR_IMPORTE") = dtAux("VALOR_NUM")
                                                dtSiDesglImporteLin.Rows.Add(dtNewS)
                                                dtAux = Nothing
                                                find = Nothing
                                            End If
                                        End If
                                End Select
                            End If
                        ElseIf Not IsDBNull(row("VALOR_FEC")) Then
                            TipoGs = row("TIPOGS")
                            Select Case TipoGs
                                Case TiposDeDatos.TipoCampoGS.InicioAbono
                                    dtNewS = dtSiDesglFecIniRectif.NewRow
                                    dtNewS.Item("LINEA") = row("LINEA")
                                    dtNewS.Item("PADRE") = row("CAMPO_PADRE")
                                    dtNewS.Item("FECINIRECTIF") = row("VALOR_FEC")
                                    dtSiDesglFecIniRectif.Rows.Add(dtNewS)
                                Case TiposDeDatos.TipoCampoGS.FinAbono
                                    dtNewS = dtSiDesglFecFinRectif.NewRow
                                    dtNewS.Item("LINEA") = row("LINEA")
                                    dtNewS.Item("PADRE") = row("CAMPO_PADRE")
                                    dtNewS.Item("FECFINRECTIF") = row("VALOR_FEC")
                                    dtSiDesglFecFinRectif.Rows.Add(dtNewS)
                                Case TiposDeDatos.TipoCampoGS.IniSuministro
                                    dtNewS = dtSiDesglFecIniSuministro.NewRow
                                    dtNewS.Item("LINEA") = row("LINEA")
                                    dtNewS.Item("PADRE") = row("CAMPO_PADRE")
                                    dtNewS.Item("FECINISUMINISTRO") = row("VALOR_FEC")
                                    dtSiDesglFecIniSuministro.Rows.Add(dtNewS)
                                Case TiposDeDatos.TipoCampoGS.FinSuministro
                                    dtNewS = dtSiDesglFecFinSuministro.NewRow
                                    dtNewS.Item("LINEA") = row("LINEA")
                                    dtNewS.Item("PADRE") = row("CAMPO_PADRE")
                                    dtNewS.Item("FECFINSUMINISTRO") = row("VALOR_FEC")
                                    dtSiDesglFecFinSuministro.Rows.Add(dtNewS)
                                Case TiposDeDatos.TipoCampoGS.SinTipo
                                    If Not IsDBNull(row("IDATRIB")) Then
                                        dtNewS = dtSiDesglAtb.NewRow
                                        dtNewS.Item("LINEA") = row("LINEA")
                                        dtNewS.Item("PADRE") = row("CAMPO_PADRE")
                                        dtNewS.Item("ID") = row("IDATRIB")
                                        dtNewS.Item("VALERP") = row("VALERP")
                                        dtNewS.Item("VALOR") = row("VALOR_FEC")
                                        dtSiDesglAtb.Rows.Add(dtNewS)
                                    End If
                            End Select
                        ElseIf Not IsDBNull(row("VALOR_BOOL")) Then
                            TipoGs = row("TIPOGS")
                            Select Case TipoGs
                                Case TiposDeDatos.TipoCampoGS.SinTipo
                                    If Not IsDBNull(row("IDATRIB")) Then
                                        dtNewS = dtSiDesglAtb.NewRow
                                        dtNewS.Item("LINEA") = row("LINEA")
                                        dtNewS.Item("PADRE") = row("CAMPO_PADRE")
                                        dtNewS.Item("ID") = row("IDATRIB")
                                        dtNewS.Item("VALERP") = row("VALERP")
                                        dtNewS.Item("VALOR") = row("VALOR_BOOL")
                                        dtSiDesglAtb.Rows.Add(dtNewS)
                                    End If
                            End Select
                        End If
                        'Importe Línea
                    End If
                Next
            Catch ex As Exception
            End Try

            Return dsMatArt
        End Function
		''' <summary>Función que llama a la mapper para hacer las validaciones de integracion</summary>
		''' <param name="dsMatArt">dataset con las tablas con los datos a validar</param>
		''' <param name="idioma">Codigo de idioma</param>
		''' <param name="iNumError">Posible numero de error</param>
		''' <param name="strError">posible texto de error</param>
		''' <param name="idInstancia">Id de instancia</param>
		''' <param name="idSolicitud ">Identificador de la solicitud</param>
		''' <param name="accionRol">Identificador de la acción</param>
		''' <param name="sBloquesDestino">Identificadores de los bloques destinos</param>
		''' <param name="peticionario">Codigo del peticionario</param>
		''' <returns>Devuelve True si no ha habido errores, False si los ha habido</returns>
		''' <remarks>Llamada desde ValidacionesIntegracion.ControlMapper()</remarks>
		Private Function EvalMapper(ByVal lCiaComp As Long, ByVal dsMatArt As DataSet, ByVal idioma As String, ByRef iNumError As Short, ByRef strError As String, Optional ByVal peticionario As String = Nothing,
									Optional ByVal peticionarioProve As String = "", Optional ByVal peticionarioProveCon As Integer = 0, Optional ByVal idInstancia As Long = Nothing,
									Optional ByVal idSolicitud As Long = Nothing, Optional ByVal accionRol As Long = 0, Optional ByVal sBloquesDestino As String = "") As TipoValidacionIntegracion
			Dim Maper As Object = Nothing
			Dim Instancia As String = System.Configuration.ConfigurationManager.AppSettings("instanciamaper")
			Try
				EvalMapper = True

				Dim arrMatGmn1(0) As String
				Dim arrMatGmn2(0) As String
				Dim arrMatGmn3(0) As String
				Dim arrMatGmn4(0) As String
				Dim arrArt(0) As String
				Dim arrItem(0) As String
				Dim arrAlm(0) As String
				Dim arrPres(0) As String
				Dim arrAtribItem(0) As String
				Dim arrAtribId(0) As Integer
				Dim arrAtribValor(0) As String
				Dim arrAtribValERP(0) As String
				Dim arrCentro(0) As String
				Dim arrOrgCompras(0) As String
				Dim arrProceAtribOrden(0) As String
				Dim arrProceAtribId(0) As Integer
				Dim arrProceAtribValor(0) As String
				Dim arrProceAtribValERP(0) As String
				Dim arrTablaExternaItem(0) As String
				Dim arrTablaExternaId(0) As Integer
				Dim arrTablaExternaValor(0) As String
				Dim arrImporteLinItem(0) As String
				Dim arrImporteLinValor(0) As Double
				Dim arrProve(0) As String
				Dim arrCentroSM(0) As String
				Dim arrCantidad(0) As Double
				Dim arrPrecUni(0) As Double
				Dim arrTipoPedido(0) As String
				Dim arrPartida(0) As String
				Dim arrPartidaNoDesglose(0) As String
				Dim arrFecIniRectifAbono(0) As String
				Dim arrFecFinRectifAbono(0) As String
				Dim arrOrgComprasNoDesgl(0) As String
				Dim arrCentroNoDesgl(0) As String
				Dim arrPresCab(0) As String
				Dim arrFechaIniSuministro(0) As String
				Dim arrFechaFinSuministro(0) As String
				Dim arrFechaIniSuministroC(0) As String
				Dim arrFechaFinSuministroC(0) As String
				Dim arrPersona(0) As String

				Dim HayDatos As Boolean
				strError = ""

				HayDatos = CreaArrays(dsMatArt, arrMatGmn1, arrMatGmn2, arrMatGmn3, arrMatGmn4, arrArt, arrItem _
				, arrAlm, arrPres, arrAtribItem, arrAtribId, arrAtribValor, arrAtribValERP, arrCentro, arrOrgCompras _
				, arrProceAtribOrden, arrProceAtribId, arrProceAtribValor, arrProceAtribValERP, arrTablaExternaItem, arrTablaExternaId _
				, arrTablaExternaValor, arrImporteLinItem, arrImporteLinValor, arrProve, arrCentroSM, arrCantidad, arrPrecUni _
				, arrTipoPedido, arrPartida, arrPartidaNoDesglose, arrFecIniRectifAbono, arrFecFinRectifAbono, arrOrgComprasNoDesgl, arrCentroNoDesgl, arrPresCab _
				, arrFechaIniSuministroC, arrFechaFinSuministroC, arrFechaIniSuministro, arrFechaFinSuministro, arrPersona)

				'EvalMapper sera TipoValidacionIntegracion.SinMensaje todo ok o TipoValidacionIntegracion.Mensaje_Bloqueo Bloqueo hasta llegar a TratarLineaPortal que es donde apareceran los avisos
				'Obtiene el nombre de la Mapper en función de la Organización de Compras
				Dim NomMapper As String = ""
				Dim orgcompras As String = ""
				If arrOrgComprasNoDesgl.Length > 0 Then
					For i As Integer = 0 To arrOrgComprasNoDesgl.Length - 1
						If arrOrgComprasNoDesgl(i) <> Nothing Then
							orgcompras = arrOrgComprasNoDesgl(i)
							Exit For
						End If
					Next
				End If
				If orgcompras = "" Then
					If arrOrgCompras.Length > 0 Then
						For i As Integer = 0 To arrOrgCompras.Length - 1
							If arrOrgCompras(i) <> Nothing Then
								orgcompras = arrOrgCompras(i)
								Exit For
							End If
						Next
					End If
				End If

				Dim NomMapperDat As DataSet = ObtenerNomMapper(lCiaComp, orgcompras)
				If Not NomMapperDat.Tables(0).Rows.Count = 0 Then
					NomMapper = NomMapperDat.Tables(0).Rows(0).Item(0)
				End If

				If HayDatos And NomMapper <> "" Then
					'----   VALIDACIONES EXTERNAS DE INTEGRACIÓN: Valida el valor de un atributo contra una función (BAPI) de SAP------------
					'El centro puede estar fuera o dentro del desglose
					Dim sCentroAprov As String = ""
					sCentroAprov = IIf(arrCentroNoDesgl(0) = Nothing, IIf(arrCentro(0) = Nothing, "", arrCentro(0)), arrCentroNoDesgl(0))

					'1.- Atributos con ValidacionERP a nivel de línea de desglose:
					Dim arrAtribMapId() As Integer
					ReDim arrAtribMapId(0)
					Dim arrAtribMapValor() As String
					ReDim arrAtribMapValor(0)
					Dim arrParam2() As Boolean
					ReDim arrParam2(0)
					Dim arrParam3() As Boolean
					ReDim arrParam3(0)

					'Inicializa arrParam2:
					For i As Integer = 0 To UBound(arrAtribId, 1)
						ReDim Preserve arrParam2(i)
						arrParam2(i) = False
					Next
					If NecesarioValidarOrden(arrAtribId, arrAtribValor, arrAtribValERP, arrAtribMapId, arrAtribMapValor) Then
						If Not Maper Is Nothing Then
							Maper = Nothing
						End If
						Maper = CreateObject(NomMapper & ".clsValidacion")
						If Maper Is Nothing Then
							EvalMapper = TipoValidacionIntegracion.SinMensaje
							Exit Function
						End If
						strError = Maper.fStrValidarAtributoPortal(Instancia, arrAtribId, arrAtribValor, sCentroAprov, idioma, arrParam2, idInstancia, accionRol)
						'Se pasan los arrays de atributos completos (en lugar de arratribMapId) para que actualice con el índice correcto arrParam2
						If strError <> "" Then
							iNumError = -100
							EvalMapper = TipoValidacionIntegracion.Mensaje_Bloqueo
						End If
					End If

					'2.- Atributos con ValidacionERP fuera de desglose:
					If EvalMapper Then
						ReDim arrAtribMapId(0)
						ReDim arrAtribMapValor(0)
						If NecesarioValidarOrden(arrProceAtribId, arrProceAtribValor, arrProceAtribValERP, arrAtribMapId, arrAtribMapValor) Then
							If Not Maper Is Nothing Then
								Maper = Nothing
							End If
							Maper = CreateObject(NomMapper & ".clsValidacion")
							If Maper Is Nothing Then
								EvalMapper = TipoValidacionIntegracion.SinMensaje
								Exit Function
							End If
							strError = Maper.fStrValidarAtributoPortal(Instancia, arrAtribMapId, arrAtribMapValor, sCentroAprov, idioma, arrParam3, idInstancia, accionRol)
							'No se usa arrParam3, se mantiene porque es necesario en la función de la mapper
							If strError <> "" Then
								iNumError = -100
								EvalMapper = TipoValidacionIntegracion.Mensaje_Bloqueo
							End If
						End If
					End If
					'----------------------------------------------------------------------------------------------

					If Not Maper Is Nothing Then
						Maper = Nothing
					End If
					Maper = CreateObject(NomMapper & ".clsInPedidosDirectos")

					'En segundo lugar, el resto de validaciones 
					If EvalMapper Then
						If Maper Is Nothing Then
							EvalMapper = TipoValidacionIntegracion.SinMensaje
							Exit Function
						End If
						'Aqui es donde puede ser cualquier valor de TipoValidacionIntegracion
						EvalMapper = Maper.TratarLineaPortal(iNumError, strError, arrMatGmn1, arrMatGmn2, arrMatGmn3, arrMatGmn4, arrArt, arrItem, arrAlm, arrPres, arrAtribItem, arrAtribId, arrAtribValor,
							arrProceAtribOrden, arrProceAtribId, arrProceAtribValor, arrOrgCompras, arrCentro, arrParam2, peticionario, idInstancia, arrTablaExternaItem, arrTablaExternaId, arrTablaExternaValor,
							arrImporteLinItem, arrImporteLinValor, arrProve, arrCentroSM, arrCantidad, arrPrecUni, idSolicitud, arrTipoPedido, arrPartida, arrPartidaNoDesglose, accionRol, arrFecIniRectifAbono,
							arrFecFinRectifAbono, arrOrgComprasNoDesgl, arrCentroNoDesgl, arrPresCab, arrFechaIniSuministroC, arrFechaFinSuministroC, arrFechaIniSuministro, arrFechaFinSuministro, sBloquesDestino,
							arrPersona, peticionarioProve, peticionarioProveCon)
					End If
				End If
			Catch ex As Exception
				EvalMapper = TipoValidacionIntegracion.SinMensaje
			Finally
				Maper = Nothing
			End Try
		End Function
		''' <summary>Función que lee los campos del DataSet y crea los arrays con los valores correspondientes. 
		''' Estos arrays se pasarán a la función que realiza las validaciones definidas para cada ERP en la mapper.</summary>
		''' <param name="dsMatArt">DataSet con los datos de la instancia</param>
		''' <param name="arrMatGmn1">Array con los GMN1 del artículo</param>
		''' <param name="arrMatGmn2">Array con los GMN2 del artículo</param>
		''' <param name="arrMatGmn3">Array con los GMN3 del artículo</param>
		''' <param name="arrMatGmn4">Array con los GMN4 del artículo</param>
		''' <param name="arrArt">Array con los códigos de artículos</param>
		''' <param name="arrItem">Array con los items</param>
		''' <param name="arrAlm">Array con los ID de los almacenes (por línea)</param>
		''' <param name="arrPres">Array con los presupuestos</param>
		''' <param name="arrAtribItem">Array con los ID de los items (por cada atributo e item)</param>
		''' <param name="arrAtribId">Array con los ID de los atributos</param>
		''' <param name="arrAtribValor">Array con los valores de los atributos</param>
		''' <param name="arrAtribValERP">Array con los </param>
		''' <param name="arrCentro">Array con los centros</param>
		''' <param name="arrOrgCompras">Array con las organziaciones de compras</param>
		''' <param name="arrProceAtribOrden">Array con los ID de los campos de tipo atributo a nivel de cabecera</param>
		''' <param name="arrProceAtribId">Array con los ID de los atributos a nivel de cabecera</param>
		''' <param name="arrProceAtribValor">Array con los valores de los atributos a nivel de cabecera</param>
		''' <param name="arrTablaExternaItem">Array con los ID de los campos de tipo tabla externa</param>
		''' <param name="arrTablaExternaId">Array con los ID de las tablas externas</param>
		''' <param name="arrTablaExternaValor">Array con los valores de las tablas externas</param>
		''' <param name="arrImporteLinItem">Array con los ID de los campos de tipo importe de las líneas</param>
		''' <param name="arrImporteLinValor">Array con el valor de los importes de las líneas</param>    
		''' <param name="arrProve">Array con los proveedores</param>
		''' <param name="arrCentroSM">Array con los centros de coste (Centros SM)</param>
		''' <param name="arrCantItem">Cantidad del item</param>
		''' <param name="arrTipoPedido">Tipo de Pedido</param>
		''' <param name="arrPartida">Partida presupuestaria</param>
		''' <param name="arrPartidaNoDesglose">Partida presupuestaria (fuera del desglose)</param>
		''' <returns>Todos los arrays que se han creado</returns>
		''' <remarks>Llamada desde: PMPortalServer.ValidacionesIntegracion.EvalMapper </remarks>
		Private Function CreaArrays(ByVal dsMatArt As DataSet, ByRef arrMatGmn1() As String, ByRef arrMatGmn2() As String, ByRef arrMatGmn3() As String,
                ByRef arrMatGmn4() As String, ByRef arrArt() As String, ByRef arrItem() As String, ByRef arrAlm() As String, ByRef arrPres() As String,
                ByRef arrAtribItem() As String, ByRef arrAtribId() As Integer, ByRef arrAtribValor() As String, ByRef arrAtribValERP() As String,
                ByRef arrCentro() As String, ByRef arrOrgCompras() As String, ByRef arrProceAtribOrden() As String, ByRef arrProceAtribId() As Integer,
                ByRef arrProceAtribValor() As String, ByRef arrProceAtribValERP() As String, ByRef arrTablaExternaItem() As String, ByRef arrTablaExternaId() As Integer,
                ByRef arrTablaExternaValor() As String, ByRef arrImporteLinItem() As String, ByRef arrImporteLinValor() As Double, ByRef arrProve() As String,
                ByRef arrCentroSM() As String, ByRef arrCantItem() As Double, ByRef arrPrecItem() As Double, ByRef arrTipoPedido() As String, ByRef arrPartida() As String,
                ByRef arrPartidaNoDesglose() As String, ByRef arrFecIniRectifAbono() As String, ByRef arrFecFinRectifAbono() As String, ByRef arrOrgComprasNoDesgl() As String,
                ByRef arrCentroNoDesgl() As String, ByRef arrPresCab() As String, ByRef arrFechaIniSuministroC() As String, ByRef arrFechaFinSuministroC() As String,
                ByRef arrFechaIniSuministro() As String, ByRef arrFechaFinSuministro() As String, ByRef arrPersona() As String) As Boolean
            Dim iIndice As Integer = -1
            Dim iIndiceOrgLin As Integer = -1
            Dim iIndiceAtrib As Integer = -1
            Dim iIndiceAtribLin As Integer = -1
            Dim iIndiceTablaExtLin As Integer = -1
            Dim iIndiceImporteLin As Integer = -1
            Dim iIndiceProve As Integer = -1
            Dim iIndiceCentroSM As Integer = -1
            Dim iIndiceTipoPed As Integer = -1
            Dim iIndicePartida As Integer = -1
            Dim iIndiceFecIniRectifAbono As Integer = -1
            Dim iIndiceFecFinRectifAbono As Integer = -1
            Dim iIndiceFecIniSuministro As Integer = -1
            Dim iIndiceFecFinSuministro As Integer = -1
            Dim iIndiceFecIniSuministroC As Integer = -1
            Dim iIndiceFecFinSuministroC As Integer = -1
            Dim iIndiceOrg As Integer = -1
            Dim iIndiceCentro As Integer = -1
            Dim iIndicePres As Integer = -1
            Dim EnCurso As String = ""
            Dim CentroLinea As String = ""
            Dim OrgComprasLinea As String = ""

            CreaArrays = True

            'Inicializa los arrays
            arrMatGmn1(iIndice + 1) = ""
            arrMatGmn2(iIndice + 1) = ""
            arrMatGmn3(iIndice + 1) = ""
            arrMatGmn4(iIndice + 1) = ""
            arrItem(iIndice + 1) = ""
            arrArt(iIndice + 1) = ""
            arrAlm(iIndice + 1) = ""
            arrPresCab(iIndice + 1) = ""
            arrProve(iIndice + 1) = ""
            arrCentroSM(iIndice + 1) = ""
            arrTipoPedido(iIndice + 1) = ""
            arrPartidaNoDesglose(iIndice + 1) = ""
            arrOrgComprasNoDesgl(iIndice + 1) = ""
            arrCentroNoDesgl(iIndice + 1) = ""

            iIndice = -1
            For Each rowMat As DataRow In dsMatArt.Tables("NoDMa").Rows
                arrMatGmn1(iIndice + 1) = rowMat("GMN1")
                arrMatGmn2(iIndice + 1) = rowMat("GMN2")
                arrMatGmn3(iIndice + 1) = rowMat("GMN3")
                arrMatGmn4(iIndice + 1) = rowMat("GMN4")
                arrItem(iIndice + 1) = rowMat("GRUPO")
                For Each rowArt As DataRow In dsMatArt.Tables("NoDAr").Rows
                    If rowArt("GRUPO") = rowMat("GRUPO") Then
                        If rowArt("ORDEN") - 1 = rowMat("ORDEN") Then
                            arrArt(iIndice + 1) = rowArt("ART4")
                            Exit For
                        End If
                    End If
                Next
                iIndice = iIndice + 1
            Next

            iIndice = -1
            For Each rowAlm As DataRow In dsMatArt.Tables("NoDAl").Rows
                arrAlm(iIndice + 1) = rowAlm("ALMACEN")
                iIndice = iIndice + 1
            Next

            iIndice = -1
            For Each rowPr As DataRow In dsMatArt.Tables("NoDPr").Rows
                arrPresCab(iIndice + 1) = IIf(rowPr("PRESUP4") <> "", rowPr("PRESUP4"), "")
                iIndice = iIndice + 1
            Next

            For Each rowIt As DataRow In dsMatArt.Tables("NoDAt").Rows
                ReDim Preserve arrProceAtribOrden(iIndiceAtrib + 1)
                ReDim Preserve arrProceAtribId(iIndiceAtrib + 1)
                ReDim Preserve arrProceAtribValor(iIndiceAtrib + 1)
                ReDim Preserve arrProceAtribValERP(iIndiceAtrib + 1)
                arrProceAtribOrden(iIndiceAtrib + 1) = "PM"
                arrProceAtribId(iIndiceAtrib + 1) = rowIt("ID")
                arrProceAtribValor(iIndiceAtrib + 1) = rowIt("VALOR")
                arrProceAtribValERP(iIndiceAtrib + 1) = rowIt("VALERP")
                iIndiceAtrib = iIndiceAtrib + 1
            Next

            For Each rowProve As DataRow In dsMatArt.Tables("NoDProve").Rows
                ReDim Preserve arrProve(iIndiceProve + 1)
                arrProve(iIndiceProve + 1) = rowProve("PROVE")
                iIndiceProve = iIndiceProve + 1
            Next

            For Each rowCentroSM As DataRow In dsMatArt.Tables("NoDCenSM").Rows
                ReDim Preserve arrCentroSM(iIndiceCentroSM + 1)
                arrCentroSM(iIndiceCentroSM + 1) = rowCentroSM("CENTRO_SM")
                iIndiceCentroSM = iIndiceProve + 1
            Next

            For Each rowCentroSM As DataRow In dsMatArt.Tables("NoDTipoPed").Rows
                ReDim Preserve arrTipoPedido(iIndiceTipoPed + 1)
                arrTipoPedido(iIndiceTipoPed + 1) = rowCentroSM("TIPO_PEDIDO")
                iIndiceTipoPed = iIndiceTipoPed + 1
            Next

            For Each rowPartida As DataRow In dsMatArt.Tables("NoDPartida").Rows
                ReDim Preserve arrPartidaNoDesglose(iIndicePartida + 1)
                arrPartidaNoDesglose(iIndicePartida + 1) = rowPartida("PARTIDA")
                iIndicePartida = iIndicePartida + 1
            Next

            For Each rowFecIniRectif As DataRow In dsMatArt.Tables("NoDFecIniRectif").Rows
                ReDim Preserve arrFecIniRectifAbono(iIndiceFecIniRectifAbono + 1)
                arrFecIniRectifAbono(iIndiceFecIniRectifAbono + 1) = rowFecIniRectif("FECINIRECTIF")
                iIndiceFecIniRectifAbono = iIndiceFecIniRectifAbono + 1
            Next

            For Each rowFecFinRectif As DataRow In dsMatArt.Tables("NoDFecFinRectif").Rows
                ReDim Preserve arrFecFinRectifAbono(iIndiceFecFinRectifAbono + 1)
                arrFecFinRectifAbono(iIndiceFecFinRectifAbono + 1) = rowFecFinRectif("FECFINRECTIF")
                iIndiceFecFinRectifAbono = iIndiceFecFinRectifAbono + 1
            Next

            For Each rowFecIniSumninistroC As DataRow In dsMatArt.Tables("NoDFecIniSuministro").Rows
                ReDim Preserve arrFechaIniSuministroC(iIndiceFecIniSuministroC + 1)
                arrFechaIniSuministroC(iIndiceFecIniSuministroC + 1) = CDate(rowFecIniSumninistroC("FECINISUMINISTRO")).ToString("yyyy-MM-dd")
                iIndiceFecIniSuministroC = iIndiceFecIniSuministroC + 1
            Next

            For Each rowFecFinSuministroC As DataRow In dsMatArt.Tables("NoDFecFinSuministro").Rows
                ReDim Preserve arrFechaFinSuministroC(iIndiceFecFinSuministroC + 1)
                arrFechaFinSuministroC(iIndiceFecFinSuministroC + 1) = CDate(rowFecFinSuministroC("FECFINSUMINISTRO")).ToString("yyyy-MM-dd")
                iIndiceFecFinSuministroC = iIndiceFecFinSuministroC + 1
            Next

            For Each rowOrgCompras As DataRow In dsMatArt.Tables("NoDOr").Rows
                ReDim Preserve arrOrgCompras(iIndiceOrg + 1)
                arrOrgComprasNoDesgl(iIndiceOrg + 1) = rowOrgCompras("ORGCOMPRAS")
                iIndiceOrg = iIndiceOrg + 1
            Next

            For Each rowCentro As DataRow In dsMatArt.Tables("NoDCe").Rows
                ReDim Preserve arrCentroNoDesgl(iIndiceCentro + 1)
                arrCentroNoDesgl(iIndiceCentro + 1) = rowCentro("CENTRO")
                iIndiceCentro = iIndiceCentro + 1
            Next

            iIndice = -1
            For Each rowMat As DataRow In dsMatArt.Tables("SiDMa").Rows
                ReDim Preserve arrMatGmn1(iIndice + 1)
                ReDim Preserve arrMatGmn2(iIndice + 1)
                ReDim Preserve arrMatGmn3(iIndice + 1)
                ReDim Preserve arrMatGmn4(iIndice + 1)
                ReDim Preserve arrArt(iIndice + 1)
                ReDim Preserve arrItem(iIndice + 1)
                ReDim Preserve arrAlm(iIndice + 1)
                ReDim Preserve arrPres(iIndice + 1)
                ReDim Preserve arrCentro(iIndice + 1)
                ReDim Preserve arrOrgCompras(iIndice + 1)
                ReDim Preserve arrCantItem(iIndice + 1)
                ReDim Preserve arrPrecItem(iIndice + 1)
                ReDim Preserve arrPartida(iIndice + 1)

                arrMatGmn1(iIndice + 1) = rowMat("GMN1")
                arrMatGmn2(iIndice + 1) = rowMat("GMN2")
                arrMatGmn3(iIndice + 1) = rowMat("GMN3")
                arrMatGmn4(iIndice + 1) = rowMat("GMN4")
                arrArt(iIndice + 1) = ""
                arrItem(iIndice + 1) = rowMat("PADRE") & rowMat("LINEA")
                arrAlm(iIndice + 1) = ""
                arrPres(iIndice + 1) = ""

                For Each rowArt As DataRow In dsMatArt.Tables("SiDAr").Rows
                    If rowMat("PADRE") = rowArt("PADRE") Then
                        If rowMat("LINEA") = rowArt("LINEA") Then
                            arrArt(iIndice + 1) = rowArt("ART4")
                            Exit For
                        End If
                    End If
                Next

                For Each rowAlm As DataRow In dsMatArt.Tables("SiDAl").Rows
                    If rowAlm("PADRE") = rowMat("PADRE") AndAlso rowAlm("LINEA") = rowMat("LINEA") Then
                        arrAlm(iIndice + 1) = rowAlm("ALMACEN")
                        Exit For
                    End If
                Next

                For Each rowPr As DataRow In dsMatArt.Tables("SiDPr").Rows
                    If rowPr("PADRE") = rowMat("PADRE") AndAlso rowPr("LINEA") = rowMat("LINEA") Then
                        arrPres(iIndice + 1) = IIf(rowPr("PRESUP4") <> "", rowPr("PRESUP4"), "")
                        Exit For
                    End If
                Next
                'Centros:
                For Each rowCe As DataRow In dsMatArt.Tables("SiDCe").Rows
                    If rowCe("PADRE") = rowMat("PADRE") AndAlso rowCe("LINEA") = rowMat("LINEA") Then
                        arrCentro(iIndice + 1) = rowCe("CENTRO")
                        Exit For
                    End If
                Next
                'Organización compras:
                For Each rowOr As DataRow In dsMatArt.Tables("SiDOr").Rows
                    If rowOr("PADRE") = rowMat("PADRE") AndAlso rowOr("LINEA") = rowMat("LINEA") Then
                        arrOrgCompras(iIndice + 1) = rowOr("ORGCOMPRAS")
                        Exit For
                    End If
                Next
                'Cantidad Item:
                For Each rowOr As DataRow In dsMatArt.Tables("SiDCant").Rows
                    If rowOr("PADRE") = rowMat("PADRE") AndAlso rowOr("LINEA") = rowMat("LINEA") Then
                        arrCantItem(iIndice + 1) = rowOr("CANTIDAD")
                        Exit For
                    End If
                Next
                'Precio Item:
                For Each rowOr As DataRow In dsMatArt.Tables("SiDPrec").Rows
                    If rowOr("PADRE") = rowMat("PADRE") AndAlso rowOr("LINEA") = rowMat("LINEA") Then
                        arrPrecItem(iIndice + 1) = rowOr("PRECIO")
                        Exit For
                    End If
                Next
                'Partida:
                For Each rowOr As DataRow In dsMatArt.Tables("SiDPartida").Rows
                    If rowOr("PADRE") = rowMat("PADRE") AndAlso rowOr("LINEA") = rowMat("LINEA") Then
                        arrPartida(iIndice + 1) = rowOr("PARTIDA")
                        Exit For
                    End If
                Next
                iIndice = iIndice + 1
            Next

            For Each rowIt As DataRow In dsMatArt.Tables("SiDAt").Rows
                ReDim Preserve arrAtribItem(iIndiceAtribLin + 1)
                ReDim Preserve arrAtribId(iIndiceAtribLin + 1)
                ReDim Preserve arrAtribValor(iIndiceAtribLin + 1)
                ReDim Preserve arrAtribValERP(iIndiceAtribLin + 1)
                arrAtribItem(iIndiceAtribLin + 1) = CStr(rowIt("PADRE")) & CStr(rowIt("LINEA"))
                arrAtribId(iIndiceAtribLin + 1) = rowIt("ID")
                arrAtribValor(iIndiceAtribLin + 1) = rowIt("VALOR")
                arrAtribValERP(iIndiceAtribLin + 1) = rowIt("VALERP")
                iIndiceAtribLin = iIndiceAtribLin + 1
            Next
            'TablasExternas
            For Each rowTe As DataRow In dsMatArt.Tables("SiDTExt").Rows
                ReDim Preserve arrTablaExternaItem(iIndiceTablaExtLin + 1)
                ReDim Preserve arrTablaExternaId(iIndiceTablaExtLin + 1)
                ReDim Preserve arrTablaExternaValor(iIndiceTablaExtLin + 1)
                arrTablaExternaItem(iIndiceTablaExtLin + 1) = CStr(rowTe("PADRE")) & CStr(rowTe("LINEA"))
                arrTablaExternaId(iIndiceTablaExtLin + 1) = rowTe("TABLA_EXTERNA")
                arrTablaExternaValor(iIndiceTablaExtLin + 1) = rowTe("VALOR_TABLAEXT")
                iIndiceTablaExtLin = iIndiceTablaExtLin + 1
            Next
            'Importe Línea
            For Each rowIl As DataRow In dsMatArt.Tables("SiDIm").Rows
                ReDim Preserve arrImporteLinItem(iIndiceImporteLin + 1)
                ReDim Preserve arrImporteLinValor(iIndiceImporteLin + 1)
                arrImporteLinItem(iIndiceImporteLin + 1) = CStr(rowIl("PADRE")) & CStr(rowIl("LINEA"))
                If IsDBNull(rowIl("VALOR_IMPORTE")) Then
                    arrImporteLinValor(iIndiceImporteLin + 1) = 0
                Else
                    arrImporteLinValor(iIndiceImporteLin + 1) = rowIl("VALOR_IMPORTE")
                End If
                iIndiceImporteLin = iIndiceImporteLin + 1
            Next
            'Fecha Inicio Rectificacion Abono
            For Each rowFecIni As DataRow In dsMatArt.Tables("SiDFecIniRectif").Rows
                ReDim Preserve arrFecIniRectifAbono(iIndiceFecIniRectifAbono + 1)
                arrFecIniRectifAbono(iIndiceFecIniRectifAbono + 1) = CStr(rowFecIni("PADRE")) & CStr(rowFecIni("LINEA"))
                If IsDBNull(rowFecIni("FECINIRECTIF")) Then
                    arrFecIniRectifAbono(iIndiceFecIniRectifAbono + 1) = 0
                Else
                    arrFecIniRectifAbono(iIndiceFecIniRectifAbono + 1) = rowFecIni("FECINIRECTIF")
                End If
                iIndiceFecIniRectifAbono = iIndiceFecIniRectifAbono + 1
            Next
            'Fecha Fin Rectificacion Abono
            For Each rowFecFin As DataRow In dsMatArt.Tables("SiDFecFinRectif").Rows
                ReDim Preserve arrFecFinRectifAbono(iIndiceFecFinRectifAbono + 1)
                arrFecFinRectifAbono(iIndiceFecFinRectifAbono + 1) = CStr(rowFecFin("PADRE")) & CStr(rowFecFin("LINEA"))
                If IsDBNull(rowFecFin("FECFINRECTIF")) Then
                    arrFecFinRectifAbono(iIndiceFecFinRectifAbono + 1) = 0
                Else
                    arrFecFinRectifAbono(iIndiceFecFinRectifAbono + 1) = rowFecFin("FECFINRECTIF")
                End If
                iIndiceFecFinRectifAbono = iIndiceFecFinRectifAbono + 1
            Next
            'Fecha Inicio Suministro
            For Each rowFecIniSuministro As DataRow In dsMatArt.Tables("SiDFecIniSuministro").Rows
                ReDim Preserve arrFechaIniSuministro(iIndiceFecIniSuministro + 1)
                arrFechaIniSuministro(iIndiceFecIniSuministro + 1) = CStr(rowFecIniSuministro("PADRE")) & CStr(rowFecIniSuministro("LINEA"))
                If IsDBNull(rowFecIniSuministro("FECINISUMINISTRO")) Then
                    arrFechaIniSuministro(iIndiceFecIniSuministro + 1) = 0
                Else
                    arrFechaIniSuministro(iIndiceFecIniSuministro + 1) = CDate(rowFecIniSuministro("FECINISUMINISTRO")).ToString("yyyy-MM-dd")
                End If
                iIndiceFecIniSuministro = iIndiceFecIniSuministro + 1
            Next
            'Fecha Fin Suministro
            For Each rowFecFinSuministro As DataRow In dsMatArt.Tables("SiDFecFinSuministro").Rows
                ReDim Preserve arrFechaFinSuministro(iIndiceFecFinSuministro + 1)
                arrFechaFinSuministro(iIndiceFecFinSuministro + 1) = CStr(rowFecFinSuministro("PADRE")) & CStr(rowFecFinSuministro("LINEA"))
                If IsDBNull(rowFecFinSuministro("FECFINSUMINISTRO")) Then
                    arrFechaFinSuministro(iIndiceFecFinSuministro + 1) = 0
                Else
                    arrFechaFinSuministro(iIndiceFecFinSuministro + 1) = CDate(rowFecFinSuministro("FECFINSUMINISTRO")).ToString("yyyy-MM-dd")
                End If
                iIndiceFecFinSuministro = iIndiceFecFinSuministro + 1
            Next

            iIndice = 0
            For Each drPersona As DataRow In dsMatArt.Tables("NoDPersona").Rows
                arrPersona(iIndice) = drPersona("PER")
                iIndice += 1
            Next
        End Function
		Public Function ValidacionesWCF_Portal(ByVal lCiaComp As Long, ByRef strError As String, ByVal dsMatArt As DataSet, ByVal idioma As String,
											   Optional ByVal sBloquesDestino As String = "") As TipoValidacionIntegracion
			'Llamamos al webservice
			Dim objectInfoValidacion As CRespuestaValidacion = RemoteValidacionesWCF(lCiaComp, dsMatArt, FSNLibrary.TablasIntegracion.PM, idioma, sBloquesDestino)
			strError = objectInfoValidacion.mensajeField
			Return CType(objectInfoValidacion.bloqueoField, TipoValidacionIntegracion)
		End Function
		''' <summary>Llama al servicio de integración de FSIS de validación de pedidos.</summary>
		''' <param name="DS">DataSet con toda la información del pedido.</param>
		''' <param name="idEntidad">Número de entidad</param>
		''' <param name="idioma">Idioma del usuario</param>
		''' <param name="sBloquesDestino">Identificadores de los bloques destinos</param>
		''' <returns>Devuelve el mensaje de error en caso de producirse o cadena vacía si todo va bien.</returns>
		''' <remarks>LLamada desde: PmServer/ValidacionesIntegracion/ValidacionesWCF_Portal()</remarks>
		Private Function RemoteValidacionesWCF(ByVal lCiaComp As Long, ByVal DS As DataSet, idEntidad As Integer, idioma As String,
											   ByVal sBloquesDestino As String) As CRespuestaValidacion
			Dim oIntegracion As New PMPortalServer.Integracion(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
			Dim sRuta, sNombre, sContrasenya As String
			Dim iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType As Integer
			sNombre = Nothing
			sContrasenya = Nothing

			Try
				'Se llama a un método de FSIS para deducir el erp y de esta forma hacer las validaciones correspondientes
				oIntegracion.ObtenerCredencialesWCF(lCiaComp, idEntidad, 0, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, sNombre, sContrasenya)
				sRuta = oIntegracion.ObtenerRutaInstalacionFSIS(lCiaComp)
				sRuta = Split(sRuta, "Exportar")(0) & "Validaciones.svc"

				Dim oValidacionesSolicitudRequest As ValidacionesSolicitudRequest
				Dim oValidacionesSolicitudResponse As ValidacionesSolicitudResponse

				oValidacionesSolicitudRequest = New ValidacionesSolicitudRequest(DS, idEntidad, idioma, sBloquesDestino)

				Dim myChannelFactory As ChannelFactory(Of IValidaciones)
				Dim myEndpoint As New EndpointAddress(sRuta)

				Select Case iServiceBindingType
					Case 1 'WSHttpBinding
						Dim iSecurityMode As SecurityMode = iServiceSecurityMode
						Dim myBinding As New WSHttpBinding(iSecurityMode)
						With myBinding
							.Security.Transport.ClientCredentialType = iClientCredentialType
							.Security.Transport.ProxyCredentialType = iProxyCredentialType
						End With
						myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
					Case Else 'BasicHttpBinding
						Dim iBasicSecurityMode As BasicHttpSecurityMode = iServiceSecurityMode
						Dim myBinding As New BasicHttpBinding(iBasicSecurityMode)
						With myBinding
							.Security.Transport.ClientCredentialType = iClientCredentialType
							.Security.Transport.ProxyCredentialType = iProxyCredentialType
						End With
						myChannelFactory = New ChannelFactory(Of IValidaciones)(myBinding, myEndpoint)
				End Select

				With (myChannelFactory.Credentials)
					Select Case iClientCredentialType
						Case HttpClientCredentialType.Windows
							With .Windows.ClientCredential
								.UserName = sNombre
								.Password = sContrasenya
							End With
						Case HttpClientCredentialType.Basic
							With .UserName
								.UserName = sNombre
								.Password = sContrasenya
							End With
						Case HttpClientCredentialType.Certificate
							.ClientCertificate.SetCertificate(System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine,
															  System.Security.Cryptography.X509Certificates.StoreName.My,
															  System.Security.Cryptography.X509Certificates.X509FindType.FindBySubjectName, "client")
					End Select
				End With

				Dim wcfProxyClient As IValidaciones = myChannelFactory.CreateChannel()
				oValidacionesSolicitudResponse = wcfProxyClient.ValidacionesSolicitud(oValidacionesSolicitudRequest)
				myChannelFactory.Close()

				Return oValidacionesSolicitudResponse.ValidacionesSolicitudResult
			Catch ex As Exception
				Return Nothing
			End Try
		End Function
		''' <summary>Funcion que devuelve los datos del Mapper de una solicitud</summary>
		''' <param name="sOrgCompras">Codigo de la organizacion de compras</param>
		''' <returns>Un DataSet con los datos del mapper a cargar</returns>
		''' <remarks>LLamada desde: PmServer/ValidacionesIntegracion/EvalMapper()</remarks>
		Public Function ObtenerNomMapper(ByVal lCiaComp As Long, ByVal sOrgCompras As String) As DataSet
            Authenticate()
            Dim data As DataSet

            If mRemottingServer Then
                data = DBServer.Solicitud_Devolver_Mapper(lCiaComp, sOrgCompras, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Solicitud_Devolver_Mapper(lCiaComp, sOrgCompras)
            End If
            Return data
        End Function
        Private Function NecesarioValidarOrden(ByRef arrAtribId() As Integer, ByRef arrAtribValor() As String, ByRef arrAtribValERP() As String, ByRef arrAtribMapId() As Integer, ByRef arrAtribMapValor() As String) As Boolean
            Dim k As Integer = -1

            For i As Integer = 0 To UBound(arrAtribId, 1)
                If arrAtribValor(i) <> "" And arrAtribValERP(i) = "1" Then
                    ReDim Preserve arrAtribMapId(k + 1)
                    ReDim Preserve arrAtribMapValor(k + 1)
                    arrAtribMapId(k + 1) = arrAtribId(i)
                    arrAtribMapValor(k + 1) = arrAtribValor(i)
                    k = k + 1
                End If
            Next

            Return (k <> -1)
        End Function
    End Class

End Namespace