﻿Imports System.Collections.Generic

Public Class cn_fsTreeViewItem
    Private _value As String
    Public Property value() As String
        Get
            Return _value
        End Get
        Set(ByVal value As String)
            _value = value
        End Set
    End Property

    Private _text As String
    Public Property text() As String
        Get
            Return _text
        End Get
        Set(ByVal value As String)
            _text = value
        End Set
    End Property

    Private _selectable As Boolean
    Public Property selectable() As Boolean
        Get
            Return _selectable
        End Get
        Set(ByVal value As Boolean)
            _selectable = value
        End Set
    End Property

    Private _type As Integer
    Public Property type() As Integer
        Get
            Return _type
        End Get
        Set(ByVal value As Integer)
            _type = value
        End Set
    End Property

    Private _children As List(Of cn_fsTreeViewItem)
    Public Property children() As List(Of cn_fsTreeViewItem)
        Get
            Return _children
        End Get
        Set(ByVal value As List(Of cn_fsTreeViewItem))
            _children = value
        End Set
    End Property

    Private _nivel As String
    Public Property nivel() As String
        Get
            Return _nivel
        End Get
        Set(ByVal value As String)
            _nivel = value
        End Set
    End Property

    Private _expanded As Boolean
    Public Property expanded() As Boolean
        Get
            Return _expanded
        End Get
        Set(ByVal value As Boolean)
            _expanded = value
        End Set
    End Property
End Class
