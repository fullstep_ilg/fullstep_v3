﻿Namespace Fullstep.PMPortalServer
    Public Class Empresas
        Inherits Fullstep.PMPortalServer.Security

        Private moCentros_SM As DataSet
        Public Function ObtenerEmpresas(ByVal lCiaComp As Long, ByVal sIdi As String, Optional ByVal sNIF As String = Nothing, Optional ByVal sDen As String = Nothing,
                                        Optional ByVal sCP As String = Nothing, Optional ByVal sDir As String = Nothing, Optional ByVal sPob As String = Nothing,
                                        Optional ByVal sPais As String = Nothing, Optional ByVal sProvincia As String = Nothing, Optional lId As Long = Nothing,
                                        Optional Autocompletar As String = Nothing) As DataTable
            Authenticate()
            Dim dtEmpresas As DataTable = DBServer.Empresas_Get(lCiaComp, sIdi, sNIF, sDen, sCP, sDir, sPob, sPais, sProvincia, lId, Autocompletar)
            dtEmpresas.TableName = "EMPRESAS_USU"
            Return dtEmpresas
        End Function
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace