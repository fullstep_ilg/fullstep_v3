Imports System.Configuration

Namespace Fullstep.PMPortalServer
    <Serializable()> _
Public Class Adjunto
        Inherits Security

        Private mlId As Long
        Private msNom As String
        Private msComent As String
        Private msRuta As String
        Private msIdioma As String
        Private mlDataSize As Long
        Private mdtFecAlta As Date
        Private msProve As String
        Private mlIdCia As Long
        Private mlIdUsu As Long
        Property IdCia() As Long
            Get
                Return mlIdCia
            End Get
            Set(ByVal Value As Long)
                mlIdCia = Value
            End Set
        End Property
        Property IdUsu() As Long
            Get
                Return mlIdUsu
            End Get
            Set(ByVal Value As Long)
                mlIdUsu = Value
            End Set
        End Property
        Property Id() As Long
            Get
                Id = mlId
            End Get
            Set(ByVal Value As Long)
                mlId = Value
            End Set
        End Property
        Property Nom() As String
            Get
                Nom = msNom
            End Get
            Set(ByVal Value As String)
                msNom = Value
            End Set
        End Property
        Property Coment() As String
            Get
                Return msComent
            End Get
            Set(ByVal Value As String)
                msComent = Value
            End Set
        End Property
        Property Prove() As String
            Get
                Return msProve

            End Get
            Set(ByVal Value As String)
                msProve = Value
            End Set
        End Property
        Property DataSize() As Long
            Get
                DataSize = mlDataSize
            End Get
            Set(ByVal Value As Long)
                mlDataSize = Value
            End Set
        End Property
        ''' <summary>
        ''' Retorna el path en el que se guardaran los ficheros para luego zipearlo
        ''' </summary>
        ''' <param name="iTipo">Tipo de fichero (1: es de un campo normal, 3: es de un campo dentro de un desglose)</param>
        ''' <param name="sPath">path del adjunto</param>
        ''' <param name="bInstancia">se lee de Instancia o solicitud</param>
        ''' <returns>path completo del adjunto</returns>
        ''' <remarks>Llamada desde: _common\attach.aspx.vb\Page_Load ; Tiempo m�ximo: 0,2</remarks>
        Public Function SaveAdjunToDiskZip(ByVal lCiaComp As Long, ByVal iTipo As Integer, ByVal sPath As String, Optional ByVal bInstancia As Boolean = False) As String
            Dim sFolderPath As String

            If Not IO.Directory.Exists(sPath) Then IO.Directory.CreateDirectory(sPath)

            sFolderPath = sPath
            sPath &= "\" & msNom

            Dim sFileName As String = System.IO.Path.GetFileNameWithoutExtension(sPath)
            Dim sExtension As String = System.IO.Path.GetExtension(sPath)
            'Se comprueba si ya existe el fichero ante la posibilidad de que adjunten 2 veces un fichero con el mismo nombre
            If System.IO.File.Exists(sPath) Then
                Dim ind As Byte = 1
                While System.IO.File.Exists(sFolderPath & "\" & sFileName & "_" & ind.ToString & sExtension)
                    ind += 1
                End While

                sPath = sFolderPath & "\" & sFileName & "_" & ind.ToString & sExtension
            End If
            Return sPath
        End Function
        Public Function LeerContratoAdjunto(ByVal lCiaComp As Long, ByVal lIdContrato As Long, ByVal lIdArchivoContrato As Long) As Byte()
            Authenticate()

            LeerContratoAdjunto = DBServer.Adjunto_LeerContratoAdjunto(lCiaComp, lIdContrato, lIdArchivoContrato)
        End Function
        ''' <summary>
        ''' Graba el fichero adjunto de Contrato que se adjunta desde el Wizard de GS
        ''' </summary>
        ''' <param name="sPer">Cod de persona</param>
        ''' <param name="sProve">Cod de proveedor</param>
        ''' <param name="sNom">Nombre del fichero</param>
        ''' <param name="sPath">Path donde esta el fichero adjunto</param>
        ''' <returns>retorna el id del fichero grabado</returns>
        ''' <remarks></remarks>
        Public Function Save_Adjun_Contrato_Wizard(ByVal lCiaComp As Long, ByVal sPer As String, ByVal sProve As String, ByVal sNom As String, ByVal sPath As String, Optional ByVal idContrato As Long = Nothing, Optional ByVal sComent As String = "") As Long()
            Authenticate()

            Return DBServer.Adjunto_Save_Adjun_Contrato_Wizard(lCiaComp, sPer, sProve, sNom, sPath, DBServer.DBName, DBServer.DBServer, idContrato, sComent)
        End Function
        ''' <summary>
        ''' guarda el comentario del adjunto
        ''' </summary>
        ''' <param name="idAdjunto">id del adjunto a grabar</param>
        ''' <param name="tipo">tipo del adjunto</param>
        ''' <param name="Comentario">comentario a grabar</param>
        ''' <param name="Instancia">Instancia del adjunto</param>
        ''' <remarks></remarks>
        Public Sub GuardarComentarioAdjunto(ByVal lCiaComp As Long, ByVal idAdjunto As Integer, ByVal tipo As Integer, ByVal Comentario As String, ByVal Instancia As Integer)
            Authenticate()
            DBServer.GuardarComentario(lCiaComp, idAdjunto, tipo, Comentario, Instancia)
        End Sub
        ''' <summary>
        ''' Cargar un adjunto
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="iTipo">Tipo de adjunto</param>
        ''' <remarks>Llamada desde: _common\attach.aspx.vb\Page_Load ; Tiempo m�ximo: 0,2</remarks>
        Public Sub LoadFromRequest(ByVal lCiaComp As Long, ByVal iTipo As Integer)
            Authenticate()
            Dim data As DataSet
            Dim oRow As DataRow

            If mRemottingServer Then
                data = DBServer.Adjunto_LoadFromRequest(lCiaComp, iTipo, mlId, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Adjunto_LoadFromRequest(lCiaComp, iTipo, mlId)
            End If

            oRow = data.Tables(0).Rows(0)
            mlId = oRow.Item("ID")
            msNom = oRow.Item("NOM")
            msComent = DBNullToSomething(oRow.Item("COMENT"))
            mlDataSize = oRow.Item("DATASIZE")
        End Sub
        ''' <summary>
        ''' Carga el fichero adjunto solicitado
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>    
        ''' <param name="iTipo">Tipo de fichero (1: es de un campo normal, 3: es de un campo dentro de un desglose</param>
        ''' <remarks>Llamada desde: attach.aspx.vb --> Page_Load; Tiempo m�ximo:1</remarks>
        Public Sub LoadInstFromRequest(ByVal lCiaComp As Long, ByVal iTipo As Integer)
            Authenticate()

            Dim data As DataSet
            Dim oRow As DataRow
            If mRemottingServer Then
                data = DBServer.Adjunto_LoadInstFromRequest(lCiaComp, iTipo, mlId, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Adjunto_LoadInstFromRequest(lCiaComp, iTipo, mlId)
            End If

            Try
                oRow = data.Tables(0).Rows(0)
                mlId = oRow.Item("ID")
                msNom = oRow.Item("NOM")
                msComent = DBNullToSomething(oRow.Item("COMENT"))
                mlDataSize = DBNullToSomething(oRow.Item("DATASIZE"))
            Catch ex As Exception
                mlDataSize = -1000
            End Try
        End Sub
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
        ''' <summary>
        ''' Procedimiento que devuelve el fichero de reglas de subasta para un proceso en formato buffer (array de bytes)
        ''' Si se trata de un solo archivo, se descarga el archivo de reglas
        ''' En caso de tratarse de varios archivos, se guardan en disco, se genera un archivo zip con ellos y es el zip el que se descarga
        ''' </summary>
        ''' <param name="anyo">A�o del proceso</param>
        ''' <param name="gmn1">Grupo de Material del Nivel 1 del proceso</param>
        ''' <param name="proce">Identificador del proceso</param>
        ''' <param name="lCiaComp">Identificador de la compa��a en el portal</param>
        ''' <param type="ByRef" name="filename ">Nombre del fichero a descargar</param>
        ''' <returns >buffer con el contenido del fichero a descargar</returns>
        ''' <remarks>
        ''' Llamada desde: 
        ''' Tiempo m�ximo: </remarks>
        Public Function LeerReglasSubastaProce(ByVal anyo As Long, ByVal gmn1 As String, ByVal proce As Long, ByVal lCiaComp As Long, ByRef filename As String) As Byte()
            Authenticate()

            Dim reglas As String()
            Dim r As Long
            Dim i As Long
            Dim spath As String
            Dim basepath As String
            Dim byteBuffer As Byte() = Nothing
            Dim nombreFichero As String
            Dim idFichero As Long
            'Consultamos en el servidor el n� de documentos para saber si hacer un zip o devolverlo directamente
            'reglas ser� un array cuyo n�mero de elementos ser� el n� de documentos.
            'Cada string de reglas tendr� el nombre del archivo y el identificador de descarga separados por el caracter "|"
            reglas = DBServer.ReglasSubastaProce(anyo, gmn1, proce, lCiaComp)
            Select Case UBound(reglas)
                Case 0
                    ' No hay documentaci�n de reglas de subasta                    
                    LeerReglasSubastaProce = byteBuffer
                Case 1
                    ' S�lo hay un archivo, as� que se descarga directamente
                    idFichero = Split(reglas(0), "|")(0)
                    filename = Split(reglas(0), "|")(1)
                    'Se obtiene el fichero en formato buffer del sql.filestream
                    LeerReglasSubastaProce = DBServer.LeerAdjunProce(idFichero, lCiaComp)
                Case Else
                    'Hay m�s de uno
                    'Se guardar�n los ficheros a disco,y se generar� un zip, que ser� el archivo que se descargue.
                    Dim oFileStream As System.IO.FileStream
                    Dim offset As Integer = 50000

                    'Se crea una carpeta temporal para la descarga
                    basepath = ConfigurationManager.AppSettings("temp") + "\" + modUtilidades.GenerateRandomPath()
                    If Not IO.Directory.Exists(basepath) Then IO.Directory.CreateDirectory(basepath)

                    For r = 0 To UBound(reglas) - 1
                        'Se guarda cada docuemnto en la carpeta temporal
                        idFichero = Split(reglas(r), "|")(0)
                        nombreFichero = Split(reglas(r), "|")(1)
                        spath = basepath + "\" + nombreFichero
                        oFileStream = New System.IO.FileStream(spath, IO.FileMode.CreateNew)
                        'Se obtiene el fichero en formato buffer del sql.filestream
                        byteBuffer = DBServer.LeerAdjunProce(idFichero, lCiaComp)
                        mlDataSize = byteBuffer.Length
                        i = 0
                        While i < mlDataSize - 1
                            If byteBuffer Is Nothing Or byteBuffer.Length = 0 Then
                                i = mlDataSize
                            Else
                                oFileStream.Write(byteBuffer, 0, byteBuffer.Length)
                                i = i + byteBuffer.Length
                            End If
                        End While
                        oFileStream.Close()
                    Next

                    ' Hacer zip
                    Dim NombreFicheroZip As String
                    NombreFicheroZip = FSNLibrary.modUtilidades.Comprimir(basepath, "*.*", basepath, True, False)
                    ' Obtenemos el nombre del archivo y el tama�o en bytes separados por los caracteres "##"
                    Dim resByteBuffer As Byte()
                    NombreFicheroZip = Split(NombreFicheroZip, "##")(0)
                    filename = NombreFicheroZip
                    NombreFicheroZip = basepath + "\" + NombreFicheroZip
                    oFileStream = New System.IO.FileStream(NombreFicheroZip, IO.FileMode.Open)
                    ReDim resByteBuffer(oFileStream.Length)
                    oFileStream.Read(resByteBuffer, 0, oFileStream.Length)
                    LeerReglasSubastaProce = resByteBuffer
            End Select
        End Function
        ''' <summary>
        ''' Determina si el archivo es de una extensi�n bloqueada o no
        ''' </summary>
        ''' <param name="Extension">Extension</param>
        ''' <returns>Si es una extensi�n bloqueada true sino false</returns>
        ''' <remarks>Llamada desde:attachfile.aspx    attachfilemail.aspx; Tiempo m�ximo: 0</remarks>
        Public Function EstaenListaNegra(ByVal Extension As String) As Boolean
            Extension = Right(Extension, Len(Extension) - 1) 'Quita el punto. Viene .doc � .png
            If mRemottingServer Then
                EstaenListaNegra = DBServer.Adjunto_EstaenListaNegra(Extension, msSesionId, msIPDir, msPersistID)
            Else
                EstaenListaNegra = DBServer.Adjunto_EstaenListaNegra(Extension)
            End If
        End Function

        Public Function CtrlEsTuyo(ByVal lCiaComp As Long, ByVal iTipo As Integer, ByVal Instancia As String, ByVal Proveedor As String, ByVal Campo As Long, ByVal Nombre As String, ByVal Fecha As String, ByVal bDscgTodos As Boolean _
                                   , Optional ByVal UsaIdContrato As Boolean = False, Optional ByVal EsArchivoContrato As Boolean = False, Optional ByVal ArchContratoBd As Boolean = False, Optional ByVal EsAltafactura As Boolean = False) As Boolean
            Authenticate()

            If mRemottingServer Then
                Return DBServer.Adjunto_CtrlEsTuyo(lCiaComp, iTipo, mlId, Instancia, Proveedor, Campo, Nombre, Fecha, bDscgTodos, UsaIdContrato, EsArchivoContrato, ArchContratoBd, EsAltafactura, msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.Adjunto_CtrlEsTuyo(lCiaComp, iTipo, mlId, Instancia, Proveedor, Campo, Nombre, Fecha, bDscgTodos, UsaIdContrato, EsArchivoContrato, ArchContratoBd, EsAltafactura)
            End If
            'End If
        End Function
    End Class
End Namespace