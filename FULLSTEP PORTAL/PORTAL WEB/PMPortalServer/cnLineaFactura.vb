﻿Namespace Fullstep.PMPortalServer
    Public Class cnLineaFactura
        Private _Data As DataSet
        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return _Data
            End Get
        End Property
        Private _Id As String
        Public Property Id() As Integer
            Get
                Return _Id
            End Get
            Set(ByVal value As Integer)
                _Id = value
            End Set
        End Property
        Private _NumLinea As Integer
        Public Property NumLinea() As Integer
            Get
                Return _NumLinea
            End Get
            Set(ByVal value As Integer)
                _NumLinea = value
            End Set
        End Property
        Private _Codigo As String
        Public Property Codigo() As String
            Get
                Return _Codigo
            End Get
            Set(ByVal value As String)
                _Codigo = value
            End Set
        End Property
        Private _Denominacion As String
        Public Property Denominacion() As String
            Get
                Return _Denominacion
            End Get
            Set(ByVal value As String)
                _Denominacion = value
            End Set
        End Property
        Private _Cantidad As Double
        Public Property Cantidad() As Double
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As Double)
                _Cantidad = value
            End Set
        End Property
        Private _CantidadFormatoUsu As String
        Public Property CantidadFormatoUsu() As String
            Get
                Return _CantidadFormatoUsu
            End Get
            Set(ByVal value As String)
                _CantidadFormatoUsu = value
            End Set
        End Property
        Private _Precio As Double
        Public Property Precio() As Double
            Get
                Return _Precio
            End Get
            Set(ByVal value As Double)
                _Precio = value
            End Set
        End Property
        Private _PrecioFormatoUsu As String
        Public Property PrecioFormatoUsu() As String
            Get
                Return _PrecioFormatoUsu
            End Get
            Set(ByVal value As String)
                _PrecioFormatoUsu = value
            End Set
        End Property
        Private _TotalCostes As Double
        Public Property TotalCostes() As Double
            Get
                Return _TotalCostes
            End Get
            Set(ByVal value As Double)
                _TotalCostes = value
            End Set
        End Property
        Private _TotalCostesFormatoUsu As String
        Public Property TotalCostesFormatoUsu() As String
            Get
                Return _TotalCostesFormatoUsu
            End Get
            Set(ByVal value As String)
                _TotalCostesFormatoUsu = value
            End Set
        End Property
        Private _TotalDescuentos As Double
        Public Property TotalDescuentos() As Double
            Get
                Return _TotalDescuentos
            End Get
            Set(ByVal value As Double)
                _TotalDescuentos = value
            End Set
        End Property
        Private _TotalDescuentosFormatoUsu As String
        Public Property TotalDescuentosFormatoUsu() As String
            Get
                Return _TotalDescuentosFormatoUsu
            End Get
            Set(ByVal value As String)
                _TotalDescuentosFormatoUsu = value
            End Set
        End Property
        Private _Importe As Double
        Public Property Importe() As Double
            Get
                Return _Importe
            End Get
            Set(ByVal value As Double)
                _Importe = value
            End Set
        End Property
        Private _ImportaFormatoUsu As String
        Public Property ImporteFormatoUsu() As String
            Get
                Return _ImportaFormatoUsu
            End Get
            Set(ByVal value As String)
                _ImportaFormatoUsu = value
            End Set
        End Property
        Private _Unidad As String
        Public Property Unidad() As String
            Get
                Return _Unidad
            End Get
            Set(ByVal value As String)
                _Unidad = value
            End Set
        End Property
        Private _Moneda As String
        Public Property Moneda() As String
            Get
                Return _Moneda
            End Get
            Set(ByVal value As String)
                _Moneda = value
            End Set
        End Property
        Private _Obs As String
        Public Property Obs() As String
            Get
                Return _Obs
            End Get
            Set(ByVal value As String)
                _Obs = value
            End Set
        End Property
        Private _PedidoFs As String
        Public Property PedidoFs() As String
            Get
                Return _PedidoFs
            End Get
            Set(ByVal value As String)
                _PedidoFs = value
            End Set
        End Property
        Private _RefFactura As String
        Public Property RefFactura() As String
            Get
                Return _RefFactura
            End Get
            Set(ByVal value As String)
                _RefFactura = value
            End Set
        End Property
        Private _Albaran As String
        Public Property Albaran() As String
            Get
                Return _Albaran
            End Get
            Set(ByVal value As String)
                _Albaran = value
            End Set
        End Property
        Private _DocSAP As String
        Public Property DocSAP() As String
            Get
                Return _DocSAP
            End Get
            Set(ByVal value As String)
                _DocSAP = value
            End Set
        End Property
    End Class
End Namespace
