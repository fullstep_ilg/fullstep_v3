Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Grupo
        Inherits Security

        Private mlId As Long

        Private moDen As MultiIdioma
        Private mlNumCampos As Long



        Private mdsCampos As DataSet


        Property Id() As Long
            Get
                Id = mlId
            End Get
            Set(ByVal Value As Long)
                mlId = Value
            End Set
        End Property
        Property Den(ByVal Idioma As String) As String
            Get
                Den = moDen(Idioma)

            End Get
            Set(ByVal Value As String)
                If Not moDen.Contains(Idioma) Then
                    moDen.Add(Idioma, Value)
                Else
                    moDen(Idioma) = Value
                End If

            End Set
        End Property

        Property NumCampos() As Long
            Get
                Return mlNumCampos
            End Get
            Set(ByVal Value As Long)
                mlNumCampos = Value
            End Set
        End Property

        Property DSCampos() As DataSet
            Get
                DSCampos = mdsCampos
            End Get
            Set(ByVal Value As DataSet)
                mdsCampos = Value
            End Set
        End Property


        Friend Sub CopyDen(ByVal oDen As MultiIdioma)
            moDen = oDen
        End Sub
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
            moDen = New MultiIdioma
        End Sub

    End Class
End Namespace