Namespace Fullstep.PMPortalServer
    <Serializable()> _
        Public Class GruposMatNivel2
        Inherits Security


        Private msGMN1Cod As String

        Private mCol As New Collection


        ''' <summary>
        ''' Carga en la coleccion de la clase el Material q se le proporciona
        ''' </summary>
        ''' <param name="sGMN1Cod">Cod Gmn1 del material</param>
        ''' <param name="sGMN1Den">Den Gmn1 del material</param>
        ''' <param name="sCod">Cod de Material</param>
        ''' <param name="sDen">denominacion del Material</param>
        ''' <returns>Material cargado en coleccion</returns>
        ''' <remarks>Llamada desde: GruposMatNivel1.vb/CargarTodosLosGruposMatDesde; Tiempo maximo: 0</remarks>
        Public Function Add(ByVal sGMN1Cod As String, ByVal sGMN1Den As String, ByVal sCod As String, ByVal sDen As String) As GrupoMatNivel2

            Dim oNewObject As New GrupoMatNivel2(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)

            With oNewObject

                .Cod = sCod
                .Den = sDen
                .GMN1Cod = sGMN1Cod
                .GMN1Den = sGMN1Den
            End With

            mCol.Add(oNewObject, sCod)
            Return oNewObject
        End Function

        Public Function Add(ByVal oGrupoMatNivel2 As GrupoMatNivel2) As GrupoMatNivel2
            mCol.Add(oGrupoMatNivel2, oGrupoMatNivel2.Cod)
            Return oGrupoMatNivel2
        End Function
        Public Function Item(ByVal sCod As String) As GrupoMatNivel2
            Return mCol(sCod)

        End Function



        Property GMN1Cod() As String
            Get
                Return msGMN1Cod
            End Get
            Set(ByVal Value As String)
                msGMN1Cod = Value
            End Set
        End Property




        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace

