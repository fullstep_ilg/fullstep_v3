Namespace Fullstep.PMPortalServer
    <Serializable()> _
        Public Class Unidades
        Inherits Security
        Private moData As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moData
            End Get

        End Property


        ''' <summary>
        ''' Carga las unidades
        ''' </summary>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lCiaComp">Codigo de compania</param>
        ''' <param name="sCod">Codigo de Unidad</param>
        ''' <param name="sDen">Denominacion de Unidad</param>
        ''' <param name="bLike">Si coincide exactamente o se hace un LIKE</param>
        ''' <remarks>Llamada desde: script\_common\campos.ascx  script\_common\desglose.ascx   DataEntry_2010\GeneralEntry.vb; Tiempo m�ximo: 0,2</remarks>
        Public Sub LoadData(ByVal sIdi As String, ByVal lCiaComp As Long, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal bLike As Boolean = True)
            Authenticate()
            If mRemottingServer Then
                moData = DBServer.Unidades_Load(sIdi, lCiaComp, sCod, sDen, bLike, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.Unidades_Load(sIdi, lCiaComp, sCod, sDen, bLike)
            End If

        End Sub



        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace
