Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Provincias

        Inherits Security
        Private moData As DataSet
        Private msPais As String

        Public Property Pais() As String
            Get
                Return msPais
            End Get
            Set(ByVal Value As String)
                msPais = Value
            End Set
        End Property
        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moData
            End Get

        End Property

        ''' <summary>
        ''' Obtiene las provincias
        ''' </summary>
        ''' <param name="sIdioma">Idioma del usuario</param>
        ''' <param name="lCiaComp">C�digo de la compa��a</param>
        ''' <param name="sCod">C�digo de la provincia</param>
        ''' <param name="sDen">Denominaci�n de la provincia o parte de ella</param>
        ''' <remarks>Llamada desde: PMPortalWeb --> /_common/provinciasserver.aspx, campos.ascx, desglose.ascx, impexp.aspx; Tiempo m�ximo: 1sg</remarks>
        Public Sub LoadData(ByVal sIdioma As String, ByVal lCiaComp As Long, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing)
            Authenticate()
            If mRemottingServer Then
                moData = DBServer.Provincias_Load(sIdioma, lCiaComp, msPais, sCod, sDen, True, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.Provincias_Load(sIdioma, lCiaComp, msPais, sCod, sDen, True)
            End If
        End Sub
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace