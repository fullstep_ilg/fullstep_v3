
Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Security
        Friend mDBServer As Fullstep.PMPortalDatabaseServer.Root
        Friend msUserCode As String = ""
        Friend msSesionId As String = ""
        Friend msIPDir As String = ""
        Friend msPersistID As String = ""
        Friend mRemottingServer As Boolean = False
        Friend mIsAuthenticated As Boolean = False

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: 144 ficheros vb del PMportalServer ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean _
                       , ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String _
                       , ByVal isAuthenticated As Boolean)
            mDBServer = dbserver
            mRemottingServer = remotting

            msUserCode = UserCode

            mIsAuthenticated = isAuthenticated

            msSesionId = SesionId
            msIPDir = IPDir
            msPersistID = PersistID
        End Sub

        Friend Sub New()
        End Sub

        Friend Function DBServer() As Fullstep.PMPortalDatabaseServer.Root
            If mDBServer Is Nothing Then
                mDBServer = New Fullstep.PMPortalDatabaseServer.Root
            End If
            Return mDBServer
        End Function

        Friend Sub Authenticate()
            If Not mIsAuthenticated Then Throw New System.Security.SecurityException("User not authenticated!")
        End Sub
    End Class
End Namespace
