﻿Imports Fullstep.FSNLibrary
Namespace Fullstep.PMPortalServer
    Public Class Email
        Inherits Security

        Private moData As DataSet

        Private mlID As Long
        Private msAsunto As String
        Private msPara As String
        Private msCC As String
        Private msCCO As String
        Private msDe As String
        Private mdFechaEnvio As DateTime
        Private msCuerpo As String
        Private msProve As String
        Private mlInstancia As Long
        Private mbIsHTML As Boolean
        Private miTipoEmail As Integer
        Private miEntidadEmail As Integer
        Private msDeNombre As String
        Private msParaNombre As String

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moData
            End Get
        End Property

        Public Property ID() As Long
            Get
                Return mlID
            End Get

            Set(ByVal Value As Long)
                mlID = Value
            End Set
        End Property

        Public Property Asunto() As String
            Get
                Return msAsunto
            End Get

            Set(ByVal Value As String)
                msAsunto = Value
            End Set
        End Property

        Public Property Para() As String
            Get
                Return msPara
            End Get

            Set(ByVal Value As String)
                msPara = Value
            End Set
        End Property

        Public Property CC() As String
            Get
                Return msCC
            End Get

            Set(ByVal Value As String)
                msCC = Value
            End Set
        End Property

        Public Property CCO() As String
            Get
                Return msCCO
            End Get

            Set(ByVal Value As String)
                msCCO = Value
            End Set
        End Property

        Public Property De() As String
            Get
                Return msDe
            End Get

            Set(ByVal Value As String)
                msDe = Value
            End Set
        End Property

        Public Property FechaEnvio() As DateTime
            Get
                Return mdFechaEnvio
            End Get

            Set(ByVal Value As DateTime)
                mdFechaEnvio = Value
            End Set
        End Property

        Public Property Cuerpo() As String
            Get
                Return msCuerpo
            End Get

            Set(ByVal Value As String)
                msCuerpo = Value
            End Set
        End Property

        Public Property Prove() As String
            Get
                Return msProve
            End Get

            Set(ByVal Value As String)
                msProve = Value
            End Set
        End Property

        Public Property Instancia() As Long
            Get
                Return mlInstancia
            End Get

            Set(ByVal Value As Long)
                mlInstancia = Value
            End Set
        End Property

        Public Property IsHTML() As Boolean
            Get
                Return mbIsHTML
            End Get

            Set(ByVal Value As Boolean)
                mbIsHTML = Value
            End Set
        End Property

        ''' Revisado por: Jbg; Fecha: 24/10/2011
        ''' <summary>
        ''' El mail para q era (ejemplo: cierre positivo de NC)
        ''' </summary>
        ''' <remarks>Llamada desde: PMWeb2008/_common/EnvioMails.aspx.vb; Tiempo máximo: 0</remarks>
        Public Property TipoEMail() As Integer
            Get
                Return miTipoEmail
            End Get

            Set(ByVal Value As Integer)
                miTipoEmail = Value
            End Set
        End Property

        ''' Revisado por: Jbg; Fecha: 24/10/2011
        ''' <summary>
        ''' De q producto se envio mail
        ''' </summary>
        ''' <remarks>Llamada desde: PMWeb2008/_common/EnvioMails.aspx.vb; Tiempo máximo: 0</remarks>
        Public Property EntidadEMail() As Integer
            Get
                Return miEntidadEmail
            End Get

            Set(ByVal Value As Integer)
                miEntidadEmail = Value
            End Set
        End Property

        ''' Revisado por: Jbg; Fecha: 24/10/2011
        ''' <summary>
        ''' Nombre y apellidos del q envia el mail
        ''' </summary>
        ''' <remarks>Llamada desde: PMWeb2008/_common/EnvioMails.aspx.vb; Tiempo máximo: 0</remarks>
        Public Property DeNombre() As String
            Get
                Return msDeNombre
            End Get

            Set(ByVal Value As String)
                msDeNombre = Value
            End Set
        End Property

        ''' Revisado por: Jbg; Fecha: 24/10/2011
        ''' <summary>
        '''Nombre y apellidos del q recibe el mail
        ''' </summary>
        ''' <remarks>Llamada desde: PMWeb2008/_common/EnvioMails.aspx.vb; Tiempo máximo: 0</remarks>
        Public Property ParaNombre() As String
            Get
                Return msParaNombre
            End Get

            Set(ByVal Value As String)
                msParaNombre = Value
            End Set
        End Property

        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

        ''' Revisado por: Jbg; Fecha: 24/10/2011
        ''' <summary>
        ''' Cargar los datos de un mail ya enviado
        ''' </summary>
        ''' <param name="lIdMail">clave de REGISTRO_EMAIL</param>
        ''' <param name="Proveedor">Proveedor al q se envio el mail</param>
        ''' <remarks>Llamada desde: PMWeb2008/_common/EnvioMails.aspx.vb; Tiempo máximo: 0,1</remarks>
        Public Sub Load_Registro_Email(ByVal lCiaComp As Long, ByVal lIdMail As Long, ByVal Proveedor As String)
            Authenticate()

            If mRemottingServer Then
                moData = DBServer.Load_Registro_Email(lCiaComp, lIdMail, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.Load_Registro_Email(lCiaComp, lIdMail)
            End If

            mlID = DBNullToSomething(Data.Tables(0).Rows(0).Item("ID"))
            msAsunto = DBNullToSomething(Data.Tables(0).Rows(0).Item("SUBJECT"))
            msPara = DBNullToSomething(Data.Tables(0).Rows(0).Item("PARA"))
            msCC = DBNullToSomething(Data.Tables(0).Rows(0).Item("CC"))
            msCCO = DBNullToSomething(Data.Tables(0).Rows(0).Item("CCO"))
            msDe = DBNullToSomething(Data.Tables(0).Rows(0).Item("DIR_RESPUESTA"))
            mdFechaEnvio = DBNullToSomething(Data.Tables(0).Rows(0).Item("FECHA"))
            mbIsHTML = DBNullToSomething(Data.Tables(0).Rows(0).Item("TIPO"))
            msCuerpo = DBNullToSomething(Data.Tables(0).Rows(0).Item("CUERPO"))
            msProve = DBNullToSomething(Data.Tables(0).Rows(0).Item("PROVE"))
            mlInstancia = DBNullToSomething(Data.Tables(0).Rows(0).Item("INSTANCIA"))
            miTipoEmail = DBNullToSomething(Data.Tables(0).Rows(0).Item("ID_EMAIL"))
            miEntidadEmail = DBNullToSomething(Data.Tables(0).Rows(0).Item("ENTIDAD_EMAIL"))
            msDeNombre = DBNullToSomething(Data.Tables(0).Rows(0).Item("DE_NOMBRE"))
            msParaNombre = DBNullToSomething(Data.Tables(0).Rows(0).Item("PARA_NOMBRE"))
        End Sub

        ''' Revisado por: Jbg; Fecha: 24/10/2011
        ''' <summary>
        ''' Se utiliza para enviar mails, pasandole el cuerpo del mail,asunto...
        ''' </summary>
        ''' <param name="lIdCiaComp">Id de compania</param>
        ''' <param name="De">El from del mensaje</param>
        ''' <param name="Asunto">El asunto</param>
        ''' <param name="Para">El destinatario</param>
        ''' <param name="Cuerpo">El mensaje</param>
        ''' <param name="isHTML">Si el formato del email es HTML o no</param>
        ''' <param name="bEsPM">Si es mail de producto Pm o no</param>
        ''' <param name="CodProve">Proveedor al q se le envia el mail</param>
        ''' <param name="Instancia">Instancia</param>
        ''' <param name="CC">El CC del mensaje</param>
        ''' <param name="CCO">El CCO del mensaje</param>
        ''' <param name="sAdjuntos">Adjuntos</param>
        ''' <param name="sRuta">Ruta de adjuntos</param>
        ''' <remarks>Llamada desde: PMWeb2008/_common/EnvioMails.aspx.vb; Tiempo máximo: 0,1</remarks>
        Public Sub EnvioMail(ByVal lIdCiaComp As Long, ByVal De As String, ByVal Asunto As String, ByVal Para As String, ByVal Cuerpo As String, _
                              ByVal isHTML As Boolean, ByVal bEsPM As Boolean, ByVal CodProve As String, ByVal Instancia As Long, ByVal CC As String, ByVal CCO As String, _
                              ByVal sAdjuntos As String, ByVal sRuta As String)
            Dim oNotificador As PMPortalServer.Notificar

            Authenticate()

            Try
                oNotificador = New PMPortalServer.Notificar(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
                oNotificador.EnviarMail(lIdCiaComp, De, Asunto, Para, Cuerpo, isHTML, True, CC, CCO, bEsPM, CodProve, Instancia, sAdjuntos, sRuta)

            Catch ex As Exception
                Throw ex
            Finally
                If Not IsNothing(oNotificador) Then
                    oNotificador.FuerzaFinalizeSmtpClient()
                    oNotificador = Nothing
                End If
            End Try
        End Sub
    End Class
End Namespace