
Namespace Fullstep.PMPortalServer
    <Serializable()> _
        Public Class NoConformidad
        Inherits Fullstep.PMPortalServer.Security

        Private mlId As Long
        Private msProve As String
        Private mlCon As Long
        Private mbPub As Boolean
        Private mbBaja As Boolean
        Private mlNumVersion As Long
        Private mdtFecPub As Date
        Private mlInstancia As Long
        Private msUsu As String
        Private msPer As String
        Private msDep As String
        Private msUon1 As String
        Private msUon2 As String
        Private msUon3 As String
        Private msNomSolicitante As String
        Private mdtFecLimCumplimentacion As Date
        Private mdtFecRespuesta As Date
        Private mbPortal As Boolean
        Private miEstado As Integer

        Private mdsData As DataSet
        Private moAcciones As DataTable

        Private mlUnidadQA As Long
        Private msUnidadQADen As String

        Private msComentCierre As String
        Private msComentAlta As String
        Private msComentCierreBR As String 'A�ADIDO XA LA FIGURA DEL REVISOR
        Private msComentAltaBR As String

        Private msCodPeticionario As String
        Private msNombrePeticionario As String
        Private mlIDSolicitud As Long
        Private msDenSolictud As String
        Private m_sPerUltAcc As String
        Private m_sComentUltAcc As String
        Private m_sAccionUlt As String
        Private moDataEstados As DataTable
        Private m_bEs_Escalacion As Boolean
        Private mdFecAplicPlan As Date


        Public ReadOnly Property UnidadQA() As Long
            Get
                Return mlUnidadQA
            End Get
        End Property

        Public ReadOnly Property UnidadQADen() As String
            Get
                Return msUnidadQADen
            End Get
        End Property

        Property Portal() As Boolean
            Get
                Return mbPortal
            End Get
            Set(ByVal Value As Boolean)
                mbPortal = Value
            End Set
        End Property
        Property Es_Escalacion() As Boolean
            Get
                Return m_bEs_Escalacion
            End Get
            Set(ByVal Value As Boolean)
                m_bEs_Escalacion = Value
            End Set
        End Property
        ReadOnly Property Data() As DataSet
            Get
                Return mdsData

            End Get
        End Property
        Public Property Acciones() As DataTable
            Get
                Return moAcciones
            End Get

            Set(ByVal Value As DataTable)
                moAcciones = Value
            End Set
        End Property

        Property Id() As Long
            Get
                Return mlId
            End Get
            Set(ByVal Value As Long)
                mlId = Value
            End Set
        End Property

        Property Prove() As String
            Get
                Return msProve
            End Get
            Set(ByVal Value As String)
                msProve = Value
            End Set
        End Property
        Property Con() As Long
            Get
                Return mlCon
            End Get
            Set(ByVal Value As Long)
                mlCon = Value
            End Set
        End Property

        Property Pub() As Boolean
            Get
                Return mbPub
            End Get
            Set(ByVal Value As Boolean)
                mbBaja = Value
            End Set
        End Property

        Property Baja() As Boolean
            Get
                Return mbBaja
            End Get
            Set(ByVal Value As Boolean)
                mbBaja = Value
            End Set
        End Property

        Property Version() As Long
            Get
                Return mlNumVersion
            End Get
            Set(ByVal Value As Long)
                mlNumVersion = Value
            End Set
        End Property

        Property FecLimCumplimentacion() As Date
            Get
                Return mdtFecLimCumplimentacion
            End Get
            Set(ByVal Value As Date)
                mdtFecLimCumplimentacion = Value
            End Set
        End Property
        Public Property FechaAplicPlan() As Date
            Get
                Return mdFecAplicPlan
            End Get
            Set(ByVal Value As Date)
                mdFecAplicPlan = Value
            End Set
        End Property
        Property Instancia() As Long
            Get
                Return mlInstancia
            End Get
            Set(ByVal Value As Long)
                mlInstancia = Value
            End Set
        End Property

        Property Usu() As String
            Get
                Return msUsu
            End Get
            Set(ByVal Value As String)
                msUsu = Value
            End Set
        End Property

        Property Per() As String
            Get
                Return msPer
            End Get
            Set(ByVal Value As String)
                msPer = Value
            End Set
        End Property

        Property Dep() As String
            Get
                Return msDep
            End Get
            Set(ByVal Value As String)
                msDep = Value
            End Set
        End Property

        Property Uon1() As String
            Get
                Return msUon1
            End Get
            Set(ByVal Value As String)
                msUon1 = Value
            End Set
        End Property

        Property Uon2() As String
            Get
                Return msUon2
            End Get
            Set(ByVal Value As String)
                msUon2 = Value
            End Set
        End Property

        Property Uon3() As String
            Get
                Return msUon3
            End Get
            Set(ByVal Value As String)
                msUon3 = Value
            End Set
        End Property
        Property NomSolicitante() As String
            Get
                Return msNomSolicitante
            End Get
            Set(ByVal Value As String)
                msNomSolicitante = Value
            End Set
        End Property

        Property FecRespuesta() As Date
            Get
                Return mdtFecRespuesta
            End Get
            Set(ByVal Value As Date)
                mdtFecRespuesta = Value
            End Set
        End Property
        Property Estado() As Integer
            Get
                Return miEstado
            End Get
            Set(ByVal Value As Integer)
                miEstado = Value
            End Set
        End Property
        Public Property ComentAlta() As String
            Get
                Return msComentAlta
            End Get

            Set(ByVal Value As String)
                msComentAlta = Value
            End Set
        End Property
        Public Property ComentCierre() As String
            Get
                Return msComentCierre
            End Get

            Set(ByVal Value As String)
                msComentCierre = Value
            End Set
        End Property
        Public Property ComentAltaBR() As String
            Get
                Return msComentAltaBR
            End Get

            Set(ByVal Value As String)
                msComentAltaBR = Value
            End Set
        End Property
        Public Property ComentCierreBR() As String
            Get
                Return msComentCierreBR
            End Get

            Set(ByVal Value As String)
                msComentCierreBR = Value
            End Set
        End Property

        ''' <summary>
        ''' Obtener/Establecer el Peticionario de la No conformidad. El stored de carga devuelve columna NOCONFORMIDAD.PER 
        ''' </summary>
        ''' <returns>Peticionario de la No conformidad</returns>
        ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase NoConformidad; Tiempo maximo:0</remarks>
        Public Property PeticionarioCod() As String
            Get
                Return msCodPeticionario
            End Get

            Set(ByVal Value As String)
                msCodPeticionario = Value
            End Set
        End Property

        ''' <summary>
        ''' Obtener/Establecer el Nombre del Peticionario de la No conformidad.
        ''' </summary>
        ''' <returns>Nombre del Peticionario de la instancia</returns>
        ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase Certificado; Tiempo maximo:0</remarks>
        Public Property Peticionario() As String
            Get
                Return msNombrePeticionario
            End Get

            Set(ByVal Value As String)
                msNombrePeticionario = Value
            End Set
        End Property

        ''' <summary>
        ''' Obtener/Establecer la denominaci�n de la Solicitud, en la q este basada la No conformidad, lo devuelve en el idioma 
        ''' del usuario conectado. El stored de carga devuelve columna SOLICTUD.DEN + @IDI AS DEN
        ''' </summary>
        ''' <returns>Denominaci�n de la Solicitud</returns>
        ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase NoConformidad; Tiempo maximo:0</remarks>
        Public Property TipoDen() As String
            Get
                Return msDenSolictud
            End Get

            Set(ByVal Value As String)
                msDenSolictud = Value
            End Set
        End Property

        ''' <summary>
        ''' Obtener/Establecer el ID de la Solicitud, en la q este basada la No conformidad.
        ''' El stored de carga devuelve columna SOLICTUD.ID AS IDSOL
        ''' </summary>
        ''' <returns>ID de la Solicitud</returns>
        ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase NoConformidad; Tiempo maximo:0</remarks>
        Public Property IDSolicitud() As Long
            Get
                Return mlIDSolicitud
            End Get

            Set(ByVal Value As Long)
                mlIDSolicitud = Value
            End Set
        End Property

        ''' <summary>
        ''' Obtener/Establecer la Persona q realizo la ultima acci�n sobre la No conformidad. 
        ''' El stored de carga devuelve columna INSTANCIA_EST.PER 
        ''' </summary>
        ''' <returns>Persona q realizo la ultima acci�n sobre la No conformidad</returns>
        ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase NoConformidad; Tiempo maximo:0</remarks>
        Public Property AccionEstadoPersona() As String
            Get
                Return m_sPerUltAcc
            End Get

            Set(ByVal Value As String)
                m_sPerUltAcc = Value
            End Set
        End Property

        ''' <summary>
        ''' Obtener/Establecer el comentario de la ultima acci�n sobre la No conformidad. 
        ''' El stored de carga devuelve columna INSTANCIA_EST.COMENT 
        ''' </summary>
        ''' <returns>Comentario de la ultima acci�n sobre la No conformidad</returns>
        ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase NoConformidad; Tiempo maximo:0</remarks>
        Public Property AccionEstadoComentario() As String
            Get
                Return m_sComentUltAcc
            End Get

            Set(ByVal Value As String)
                m_sComentUltAcc = Value
            End Set
        End Property

        ''' <summary>
        ''' Obtener/Establecer la ultima acci�n sobre la No conformidad. 
        ''' El stored de carga devuelve columna INSTANCIA_EST.ACCION 
        ''' </summary>
        ''' <returns>Ultima acci�n sobre la No conformidad</returns>
        ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase NoConformidad; Tiempo maximo:0</remarks>
        Public Property AccionEstado() As String
            Get
                Return m_sAccionUlt
            End Get

            Set(ByVal Value As String)
                m_sAccionUlt = Value
            End Set
        End Property

        ''' <summary>
        ''' Obtener/Establecer una tabla con las acciones de la No conformidad. 
        ''' El stored de carga devuelve una SELECT ... FROM NOCONF_ACC NA WITH (NOLOCK) ..., es el ds.tables(2)
        ''' </summary>
        ''' <returns>Tabla con las acciones de la No conformidad</returns>
        ''' <remarks>Llamada desde: Toda pantalla q haga uso de la clase NoConformidad; Tiempo maximo:0</remarks>
        Public ReadOnly Property EstadosComentariosInternosAcciones() As Data.DataTable
            Get
                Return moDataEstados
            End Get
        End Property

        ''' <summary>
        ''' Cargar las respuestas (la informacion de cuando se realizo la version de la no conformidad)
        ''' </summary>
        ''' <param name="lCiaComp">codigo de compania</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <returns>Respuestas</returns>
        ''' <remarks>Llamada desde: noconformidad\noconformidad.aspx.vb/RellenarComboRespuestas; Tiempo maximo:0,2</remarks>
        Public Function CargarRespuestas(ByVal lCiaComp As Long, Optional ByVal sIdi As String = "SPA") As DataSet
            Authenticate()
            Dim data As DataSet

            If mRemottingServer Then
                data = DBServer.NoConformidad_CargarRespuestas(lCiaComp, msProve, mlId, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.NoConformidad_CargarRespuestas(lCiaComp, msProve, mlId, sIdi)
            End If
            Return data

        End Function

        ''' <summary>
        ''' Carga de las propiedades del objeto NoConformidad
        ''' </summary>
        ''' <param name="lCiaComp">codigo de compa�ia</param>
        ''' <param name="sIdi">Idioma</param>      
        ''' <param name="DesdeImpExp">Solo desde Imprimir/Exportar se usa Coment Alta y Ultima accion por separado, luego, dos select</param>
        ''' <remarks>Llamada desde: mensajes.aspx/page_load  noconformidad/desglose.aspx/page_load
        '''   noconformidad.aspx/page_load    enviarnoconformidad.aspx/page_load; Tiempo m�ximo:0,1</remarks>
        Public Sub Load(ByVal lCiaComp As Long, Optional ByVal sIdi As String = "SPA", Optional ByVal DesdeImpExp As Boolean = False)

            Authenticate()
            Dim data As DataSet
            Dim oRow As DataRow
            Dim oIdiomas As New Idiomas(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
            Dim oIdi As Idioma


            If mRemottingServer Then
                data = DBServer.NoConformidad_Load(lCiaComp, msProve, mlId, mlNumVersion, sIdi, mbPortal, DesdeImpExp, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.NoConformidad_Load(lCiaComp, msProve, mlId, mlNumVersion, sIdi, mbPortal, DesdeImpExp)
            End If

            oRow = data.Tables(0).Rows(0)
            mlId = oRow.Item("ID")

            If mlNumVersion = Nothing Then
                mlNumVersion = oRow.Item("VERSION")
            End If
            mlInstancia = oRow.Item("INSTANCIA")
            msUsu = DBNullToSomething(oRow.Item("USU"))
            msPer = DBNullToSomething(oRow.Item("PER"))
            msDep = DBNullToSomething(oRow.Item("DEP"))
            msUon1 = DBNullToSomething(oRow.Item("UON1"))
            msUon2 = DBNullToSomething(oRow.Item("UON2"))
            msUon3 = DBNullToSomething(oRow.Item("UON3"))
            mdtFecRespuesta = DBNullToSomething(oRow.Item("FEC_RESPUESTA"))
            mdtFecLimCumplimentacion = DBNullToSomething(oRow.Item("FEC_LIM_RESOL"))
            mdFecAplicPlan = DBNullToSomething(oRow.Item("FEC_APLICACION_PLAN"))
            msNomSolicitante = DBNullToSomething(oRow.Item("NOMBRE"))
            miEstado = DBNullToSomething(oRow.Item("ESTADO"))
            mlUnidadQA = DBNullToSomething(oRow.Item("UNQA"))
            msUnidadQADen = DBNullToSomething("" & oRow.Item("UNQADEN"))
            m_bEs_Escalacion = DBNullToBoolean(oRow.Item("ES_NCE"))
            msComentCierre = ""
            msComentCierreBR = ""
            msComentAlta = DBNullToSomething(data.Tables(0).Rows(0).Item("COMENT_ALTA"))

            If Not IsDBNull(data.Tables(0).Rows(0).Item("COMENT_ALTA")) Then
                msComentAltaBR = Replace(data.Tables(0).Rows(0).Item("COMENT_ALTA"), Chr(10), "<BR/>")
            Else
                msComentAltaBR = ""
            End If

            msCodPeticionario = DBNullToSomething(data.Tables(0).Rows(0).Item("PER"))
            msNombrePeticionario = DBNullToSomething(data.Tables(0).Rows(0).Item("NOMBRE"))

            msDenSolictud = DBNullToSomething(data.Tables(0).Rows(0).Item("DEN"))
            mlIDSolicitud = DBNullToSomething(data.Tables(0).Rows(0).Item("IDSOL"))

            m_sPerUltAcc = DBNullToSomething(data.Tables(0).Rows(0).Item("PERSONAULT"))
            m_sComentUltAcc = DBNullToSomething(data.Tables(0).Rows(0).Item("COMENTULT"))
            m_sAccionUlt = DBNullToSomething(data.Tables(0).Rows(0).Item("ACCIONULT"))

            moDataEstados = data.Tables(2)
            moDataEstados.TableName = "NOCONF_ACC"

            mdsData = data

            moAcciones = data.Tables(1)

        End Sub

        ''' <summary>
        ''' cargar los id de campo en version de pantalla y en bbdd (para los guardados sin enviar sin salir y volver a guardar)
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa��a</param>
        ''' <returns>Correspondencia</returns>
        ''' <remarks>Llamada desde: _common\guardarinstancia.aspx.vb   _common\modulos\modInstancia.vb; Tiempo maximo:0,2</remarks>
        Public Function CargarCorrespondenciaCampos(ByVal lCiaComp As Long) As DataSet

            Authenticate()
            Dim data As DataSet

            If mRemottingServer Then
                data = DBServer.NoConformidad_CargarCorrespondenciaCampos(lCiaComp, mlId, mlNumVersion, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.NoConformidad_CargarCorrespondenciaCampos(lCiaComp, mlId, mlNumVersion)
            End If
            Dim key(0) As DataColumn
            key(0) = data.Tables(0).Columns("IDCAMPOVERSION")
            data.Tables(0).PrimaryKey = key



            Return data

        End Function
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub


        ''' <summary>
        ''' carga todos los contactos que tiene el proveedor, y se�ala cuales estan seleccionados para notificar la emisi�n y cuales no
        ''' </summary>
        ''' <param name="lIdNoConformidad">NoConformida</param>
        ''' <param name="CodProve">Proveedor</param>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <returns>Un dataset con los datos cargados</returns>
        ''' <remarks>Llamada desde: script\noconformidad\noconformidad.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Function Load_Notificados_Proveedor(ByVal lIdNoConformidad As Long, ByVal CodProve As String, ByVal lCiaComp As Long) As DataSet
            Authenticate()
            Dim iRet As DataSet

            If mRemottingServer Then
                iRet = DBServer.Load_Notificados_Proveedor(lIdNoConformidad, CodProve, lCiaComp, msSesionId, msIPDir, msPersistID)
            Else
                iRet = DBServer.Load_Notificados_Proveedor(lIdNoConformidad, CodProve, lCiaComp)
            End If

            Return iRet
        End Function

        ''' <summary>
        ''' Guarda los cambios realizados en los notificados del proveedor
        ''' </summary>
        ''' <param name="lIdNoConformidad">Id de la noconformidad</param>
        ''' <param name="CodProve">C�digo del proveedor de la noconformidad</param>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sNotificadosProveedor">Cadena de los notificados seleccionados</param>
        ''' <remarks>Llamada desde: script\_common\guardarinstancia.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Sub Guardar_Notificados_Proveedor(ByVal lIdNoConformidad As Long, ByVal CodProve As String, ByVal lCiaComp As Long, ByVal sNotificadosProveedor As String)
            Authenticate()

            If mRemottingServer Then
                DBServer.Guardar_Notificados_Proveedor(lIdNoConformidad, CodProve, lCiaComp, sNotificadosProveedor, msSesionId, msIPDir, msPersistID)
            Else
                DBServer.Guardar_Notificados_Proveedor(lIdNoConformidad, CodProve, lCiaComp, sNotificadosProveedor)
            End If
        End Sub

        ''' <summary>
        ''' Carga el bot�n de finalizar las acciones
        ''' </summary>
        ''' <param name="sTexto">texto multiidioma para el texto del bot�n</param>
        ''' <param name="sTitulo">texto multiidioma para el tooltip del bot�n</param>
        ''' <returns>tabla con los botones</returns>
        ''' <remarks>Llamada desde: PMWeb\script\noconformidad\desglose.aspx\Page_Load       
        ''' PMWeb\script\noconformidad\noconformidad.aspx\page_load; Tiempo maximo:0</remarks>
        Public Function CargarBotonesAprobarRechazarAcciones(ByVal sTexto As String, ByVal sTitulo As String) As DataTable
            Dim dt As DataTable
            Dim dc As DataColumn

            dt = New DataTable("ACCIONES")
            dc = New DataColumn("ID")
            dc.DataType = System.Type.GetType("System.String")
            dt.Columns.Add(dc)
            dc = New DataColumn("IMAGEN")
            dc.DataType = System.Type.GetType("System.String")
            dt.Columns.Add(dc)
            dc = New DataColumn("TEXTO")
            dc.DataType = System.Type.GetType("System.String")
            dt.Columns.Add(dc)
            dc = New DataColumn("JAVASCRIPTEVENT")
            dc.DataType = System.Type.GetType("System.String")
            dt.Columns.Add(dc)
            dc = New DataColumn("TITULO")
            dc.DataType = System.Type.GetType("System.String")
            dt.Columns.Add(dc)
            dc = New DataColumn("WIDTH")
            dc.DataType = System.Type.GetType("System.Double")
            dt.Columns.Add(dc)

            Dim dr As DataRow

            dr = dt.NewRow
            dr.Item("ID") = "cmdFinAccion"
            dr.Item("IMAGEN") = Nothing
            dr.Item("TEXTO") = sTexto
            dr.Item("JAVASCRIPTEVENT") = "finalizarAccion"
            dr.Item("TITULO") = sTitulo
            dr.Item("WIDTH") = 150
            dt.Rows.Add(dr)

            Return dt
        End Function

        ''' <summary>
        ''' Devolver un Dataset con los datos de los estados de las acciones.
        ''' Sus parametros son objetos por la llamada desde noConformidad/desglose.aspx, desde este solo se 
        ''' tiene eso, los objetos.
        ''' </summary>
        ''' <param name="lCiaComp">codigo de compa�ia</param>
        ''' <param name="Idioma">Idioma con el q se esta trabajando</param>  
        ''' <param name="oUser">Objeto User. Para conocer al proveedor y aprobador actual</param>  
        ''' <param name="TextoComentario"> Para establecer la denominaci�n del comentario</param>  
        ''' <param name="TextoEstado">OPara establecer la denominaci�n del estado</param>
        ''' <returns>Dataset con los datos de los estados de las acciones</returns>
        ''' <remarks>Llamada desde: DetallenoConformidad.aspx/Page_Load   noConformidadRevisor.aspx/Page_Load
        ''' noConformidad/desglose.aspx/Page_Load; Tiempo m�ximo: 0</remarks>
        Public Function CargarEstadosComentariosInternosAcciones(ByVal lCiaComp As Long, ByVal Idioma As String, ByRef oUser As Fullstep.PMPortalServer.User, ByVal TextoComentario As String, ByVal TextoEstado As String) As DataSet
            Dim sIdi As String = Idioma
            Dim oCampo As Campo = New Campo(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
            oCampo.Id = -1000

            'generamos un dataset con la misma estructura que un desglose para trasladar los datos de los estados de las acciones
            Dim oDS As DataSet = oCampo.LoadInstDesglose(lCiaComp, sIdi, -1, oUser.AprobadorActual, oUser.CodProveGS, -1)

            Dim oDTCabecera As DataTable
            Dim oRow As DataRow
            Dim oNewRow As DataRow

            oDTCabecera = oDS.Tables(0)
            oRow = oDTCabecera.NewRow

            'Comentario
            oRow = oDTCabecera.NewRow
            oRow.Item("ID") = Fullstep.PMPortalServer.IdsFicticios.Comentario
            oRow.Item("COPIA_CAMPO_DEF") = Fullstep.PMPortalServer.IdsFicticios.Comentario
            oRow.Item("GRUPO") = Fullstep.PMPortalServer.IdsFicticios.Grupo
            oRow.Item("DEN_" + sIdi) = TextoComentario
            oRow.Item("AYUDA_" + sIdi) = ""
            oRow.Item("TIPO") = 1
            oRow.Item("SUBTIPO") = Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoLargo
            oRow.Item("INTRO") = 0
            oRow.Item("TIPO_CAMPO_GS") = Fullstep.PMPortalServer.IdsFicticios.Comentario
            oRow.Item("ESCRITURA") = 0
            oRow.Item("VISIBLE") = 1
            oDTCabecera.Rows.Add(oRow)

            'Estado interno
            oRow = oDTCabecera.NewRow
            oRow.Item("ID") = Fullstep.PMPortalServer.IdsFicticios.EstadoInterno
            oRow.Item("COPIA_CAMPO_DEF") = Fullstep.PMPortalServer.IdsFicticios.EstadoInterno
            oRow.Item("GRUPO") = Fullstep.PMPortalServer.IdsFicticios.Grupo
            oRow.Item("DEN_" + sIdi) = TextoEstado
            oRow.Item("AYUDA_" + sIdi) = ""
            oRow.Item("TIPO") = 1
            oRow.Item("SUBTIPO") = Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoString
            oRow.Item("INTRO") = 1
            oRow.Item("TIPO_CAMPO_GS") = Fullstep.PMPortalDatabaseServer.TiposDeDatos.TipoCampoGS.EstadoInternoNoConf
            oRow.Item("ESCRITURA") = 0
            oRow.Item("VISIBLE") = 1
            oDTCabecera.Rows.Add(oRow)

            'Fecha Limite
            oRow = oDTCabecera.NewRow
            oRow.Item("ID") = Fullstep.PMPortalServer.IdsFicticios.FechaLimite
            oRow.Item("COPIA_CAMPO_DEF") = Fullstep.PMPortalServer.IdsFicticios.FechaLimite
            oRow.Item("GRUPO") = Fullstep.PMPortalServer.IdsFicticios.Grupo
            oRow.Item("DEN_" + sIdi) = ""
            oRow.Item("AYUDA_" + sIdi) = ""
            oRow.Item("TIPO") = 1
            oRow.Item("SUBTIPO") = Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoFecha
            oRow.Item("INTRO") = 0
            oRow.Item("ESCRITURA") = 0
            oRow.Item("VISIBLE") = 0
            oDTCabecera.Rows.Add(oRow)

            Dim i As Integer
            i = 1
            If Not mdsData Is Nothing Then
                Dim oDTLineasDesglose As DataTable = oDS.Tables(2)
                For Each oRow In moDataEstados.Rows

                    oNewRow = oDTLineasDesglose.NewRow
                    oNewRow.Item("CAMPO_PADRE") = oRow.Item("CAMPO_PADRE")
                    oNewRow.Item("LINEA") = oRow.Item("LINEA")
                    oNewRow.Item("CAMPO_HIJO") = Fullstep.PMPortalServer.IdsFicticios.Comentario
                    oNewRow.Item("VALOR_TEXT") = oRow.Item("COMENT")
                    oDTLineasDesglose.Rows.Add(oNewRow)

                    oNewRow = oDTLineasDesglose.NewRow
                    oNewRow.Item("CAMPO_PADRE") = oRow.Item("CAMPO_PADRE")
                    oNewRow.Item("LINEA") = oRow.Item("LINEA")
                    oNewRow.Item("CAMPO_HIJO") = Fullstep.PMPortalServer.IdsFicticios.EstadoInterno
                    oNewRow.Item("VALOR_NUM") = oRow.Item("ESTADO_INT")
                    oDTLineasDesglose.Rows.Add(oNewRow)

                Next

                Dim oRowFECLIM As DataRow
                For Each oRowFECLIM In moAcciones.Rows

                    oNewRow = oDTLineasDesglose.NewRow
                    oNewRow.Item("CAMPO_PADRE") = oRowFECLIM.Item("COPIA_CAMPO")
                    oNewRow.Item("LINEA") = 0
                    oNewRow.Item("CAMPO_HIJO") = Fullstep.PMPortalServer.IdsFicticios.FechaLimite
                    oNewRow.Item("VALOR_FEC") = oRowFECLIM.Item("FECHA_LIMITE")
                    oDTLineasDesglose.Rows.Add(oNewRow)

                Next

            End If

            Return oDS

        End Function

        ''' <summary>
        ''' Carga de las propiedades del objeto NoConformidad: msComentCierre y msComentCierreBR
        ''' </summary>
        ''' <param name="lCiaComp">codigo de compa�ia</param>
        ''' <remarks>Llamada desde: impexp.aspx/page_load; Tiempo m�ximo: 0,1</remarks>
        Public Sub CargarComentarioCierre(ByVal lCiaComp As Long)
            Authenticate()
            Dim data As DataSet

            If mRemottingServer Then
                data = DBServer.NoConformidad_LoadComentCierre(lCiaComp, mlId, msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.NoConformidad_LoadComentCierre(lCiaComp, mlId)
            End If
            If data.Tables(0).Rows.Count = 0 Then
                msComentCierre = ""
                msComentCierreBR = ""
            Else
                msComentCierre = DBNullToSomething(data.Tables(0).Rows(0).Item("COMENT_CIERRE"))

                If Not IsDBNull(data.Tables(0).Rows(0).Item("COMENT_CIERRE")) Then
                    msComentCierreBR = Replace(data.Tables(0).Rows(0).Item("COMENT_CIERRE"), Chr(10), "<BR/>")
                End If
            End If
        End Sub

    End Class
End Namespace
