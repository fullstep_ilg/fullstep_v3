﻿Imports System.IO
Namespace Fullstep.PMPortalServer
    <Serializable()>
    Public Class Errores
        Inherits Security
        Private mlID As Long
        Public Property ID() As Long
            Get
                Return mlID
            End Get
            Set(ByVal Value As Long)
                mlID = Value
            End Set
        End Property
        ''' <summary>
        ''' Procedimiento que crea un objeto de tipo error
        ''' </summary>
        ''' <param name="sPagina">Página donde se ha generado el error</param>
        ''' <param name="sUsuario">Usuario que ha generado el error</param>
        ''' <param name="sEx_FullName">Nombre de la excepcion generada</param>
        ''' <param name="sEx_Message">Mensaje de la excepcion generada</param>
        ''' <param name="sEx_StackTrace">Datos de la pila de errores</param>
        ''' <param name="sSv_Query_String">Datos del Query String pasado</param>
        ''' <param name="sSv_Http_User_Agent">Tipo de excepcion devuelta</param>
        ''' <remarks>
        ''' Llamada desde: PMWeb/Errores/Page_load, WebServicePM/QAPuntuaciones/CalcularPuntuacion - CalcularPuntuaciones - RecalcularVariables Tiempo máximo: 0,4 seg
        ''' </remarks>
        Public Sub Create(ByVal lCiaComp As Integer, ByVal sPagina As String, ByVal sUsuario As String, ByVal sEx_FullName As String, ByVal sEx_Message As String, ByVal sEx_StackTrace As String, ByVal sSv_Query_String As String, ByVal sSv_Http_User_Agent As String)
            Authenticate()
            mlID = DBServer.Errores_Create(lCiaComp, sPagina, sUsuario, sEx_FullName, sEx_Message, sEx_StackTrace, sSv_Query_String, sSv_Http_User_Agent)
        End Sub
        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace