﻿Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Centros_SM
        Inherits Fullstep.PMPortalServer.Security

        Private moCentros_SM As DataSet

        ''' <summary>
        ''' Funcion que carga los centros de coste
        ''' </summary>
        ''' <param name="lCiaComp">id de compañia</param>
        ''' <param name="sUsuario">cod usuario</param>
        ''' <param name="sIdioma">idioma de la aplicacion</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function LoadData(ByVal lCiaComp As Long, ByVal sUsuario As String, ByVal sIdioma As String) As DataSet
            Authenticate()
            If mRemottingServer Then
                moCentros_SM = DBServer.CentrosCoste_Load(lCiaComp, sUsuario, sIdioma, , , msSesionId, msIPDir, msPersistID)
            Else
                moCentros_SM = DBServer.CentrosCoste_Load(lCiaComp, sUsuario, sIdioma)
            End If

            Return moCentros_SM
        End Function

        ''' <summary>
        ''' Funcion al que se la pasa el codigo de centro de coste y devuelve la denominacion y las Uons
        ''' </summary>
        ''' <param name="lCiaComp">cod compañia</param>
        ''' <param name="sCod">codigo del centro de coste</param>
        ''' <param name="sIdioma">idioma de la aplicacion</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Buscar_Centro(ByVal lCiaComp As Long, ByVal sCod As String, ByVal sIdioma As String) As DataTable
            Authenticate()

            Dim dtCentro As DataTable
            If mRemottingServer Then
                dtCentro = DBServer.CentrosCoste_BuscarCentro(lCiaComp, sCod, sIdioma, msSesionId, msIPDir, msPersistID)
            Else
                dtCentro = DBServer.CentrosCoste_BuscarCentro(lCiaComp, sCod, sIdioma)
            End If

            Return dtCentro
        End Function


        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class

    
End Namespace
