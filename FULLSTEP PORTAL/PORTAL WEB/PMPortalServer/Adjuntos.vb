Imports System.Configuration
Namespace Fullstep.PMPortalServer
    <Serializable()> _
Public Class Adjuntos
        Inherits Security

        Private moData As DataSet
        Property Data() As DataSet
            Get
                Return moData
            End Get
            Set(ByVal Value As DataSet)
                moData = Value
            End Set
        End Property
        Public Sub Load(ByVal lCiaComp As Long, ByVal iTipo As Integer, Optional ByVal sIdAdjuntos As String = "", Optional ByVal sIdAdjuntosNew As String = "", Optional ByVal sIdi As String = Nothing)
            '************************************************************************************************
            '*** Descripci�n: Funci�n que devuelve un dataset con los adjuntos requeridos por los par�metros
            '*** Par�metros de entrada: 
            '***    lCiaComp: C�digo de la compa��a
            '***    iTipo: Tipo de adjuntos (1=Adjuntos de campo normal, 3=Adjuntos de campo de desglose)
            '***    sIdAdjuntos: String con los Ids de los adjuntos ya guardados en alguna versi�n anterior
            '***    sIdAdjuntosNew: String con los Ids de los adjuntos que el usuario haya guardado en esta misma versi�n
            '*** Devuelve: Un dataSet con los adjuntos solicitados
            '*** Llamada desde: los eventos Load de atachedfiles.ascx.vb y desglose.ascx
            '*** Tiempo m�ximo: 0 sec            
            '************************************************************************************************
            Authenticate()
            If mRemottingServer Then
                moData = DBServer.Adjuntos_Load(lCiaComp, iTipo, sIdAdjuntos, sIdAdjuntosNew, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.Adjuntos_Load(lCiaComp, iTipo, sIdAdjuntos, sIdAdjuntosNew, sIdi)
            End If
        End Sub
        Public Sub LoadInst(ByVal lCiaComp As Long, ByVal iTipo As Integer, Optional ByVal sIdAdjuntos As String = "", Optional ByVal sIdAdjuntosNew As String = "")
            '************************************************************************************************
            'HHF 18/03/2009
            '*** Descripci�n: Funci�n que devuelve un dataset con los adjuntos requeridos por los par�metros
            '*** Par�metros de entrada: 
            '***    lCiaComp: C�digo de la compa��a
            '***    iTipo: Tipo de adjuntos (1=Adjuntos de campo normal, 3=Adjuntos de campo de desglose)
            '***    sIdAdjuntos: String con los Ids de los adjuntos ya guardados en alguna versi�n anterior
            '***    sIdAdjuntosNew: String con los Ids de los adjuntos que el usuario haya guardado en esta misma versi�n
            '***    bPortal: Booleana que nos indica si debemos recuperar los datos de la BD de Portal (true) o de GS (false)
            '*** Par�metros de salida: No tiene
            '*** Devuelve: Un dataSet con los adjuntos solicitados
            '*** Llamada desde: los eventos Load de attachedfiles.ascx.vb y desglose.ascx
            '*** Tiempo m�ximo: 0 sec            
            '************************************************************************************************
            Authenticate()
            If mRemottingServer Then
                moData = DBServer.Adjuntos_InstLoad(lCiaComp, iTipo, sIdAdjuntos, sIdAdjuntosNew, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.Adjuntos_InstLoad(lCiaComp, iTipo, sIdAdjuntos, sIdAdjuntosNew)
            End If
        End Sub
        ''' <summary>
        ''' Devuelve los adjuntos de tipo contrato que tenga ese campo
        ''' </summary>
        ''' <param name="lCiaComp">id de la compa�ia</param>
        ''' <param name="idContrato">id del contrato</param>
        ''' <param name="idArchivoContrato">id del adjunto del contrato</param>
        ''' <param name="idCampo">id del campo</param>
        ''' <remarks></remarks>
        Public Sub LoadInst_Adj_Contrato(ByVal lCiaComp As Long, ByVal idContrato As Long, ByVal idArchivoContrato As Long, ByVal idCampo As Long)
            Authenticate()
            moData = DBServer.Adjuntos_Contrato_Load(lCiaComp, idContrato, idArchivoContrato, idCampo)
        End Sub
        Public Function CrearAdjuntosCopiados(ByVal lCiaComp As Long, ByVal AdjuntosAct As String, ByVal AdjuntosNew As String, ByVal bDefecto As Boolean) As String()
            Authenticate()
            Return DBServer.Adjuntos_CrearAdjuntosCopiados(lCiaComp, AdjuntosAct, AdjuntosNew, bDefecto)
        End Function
        ''' <summary>
        ''' Devuelve los adjuntos por instancia y campo
        ''' </summary>
        ''' <param name="lId">Id del adjunto</param>
        ''' <param name="lCampo">Codigo del campo</param>
        ''' <remarks>
        ''' Llamada desde:PmWeb2008/VisorSolicitudes/gvSolicitudes_RowDataBound
        ''' Tiempo m�ximo: 1 seg</remarks>
        Public Sub LoadAdjuntosPorInstanciayCampo(ByVal lCiaComp As Long, ByVal lId As Long, ByVal lCampo As Long)
            Authenticate()
            moData = DBServer.Adjuntos_LoadIdsPorInstanciayCampo(lCiaComp, lId, lCampo)
        End Sub        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace