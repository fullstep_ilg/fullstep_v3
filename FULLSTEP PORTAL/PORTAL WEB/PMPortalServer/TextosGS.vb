﻿Namespace Fullstep.PMPortalServer
    <Serializable()>
    Public Class TextosGS
        Inherits Security

        Public Sub New(ByRef dbserver As PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

        ''' <summary>
        ''' Funcion que carga el texto de mensaje de error utilizado en GS
        ''' </summary>
        ''' <param name="Modulo">Modulo de idioma</param>
        ''' <param name="ID">Id del texto</param>
        ''' <param name="Idioma">Idioma de la aplicación</param>
        ''' <param name="StrError">Error</param>
        ''' <returns>Una variable de tipo string con el error del texto de GS</returns>
        ''' <remarks>Llamada desde: PmWeb/guardarInstancia/PrepararConfirmacionRealizarAccion</remarks>
        Public Function MensajeError(ByVal lCiaComp As Long, ByVal Modulo As Integer, ByVal ID As Integer, Optional ByVal Idioma As String = "SPA", Optional ByVal StrError As String = "") As String
            Authenticate()

            Dim sRes As String
            If mRemottingServer Then
                sRes = DBServer.TextoGs_Carga(lCiaComp, Modulo, ID, Idioma, msSesionId, msIPDir, msPersistID)
            Else
                sRes = DBServer.TextoGs_Carga(lCiaComp, Modulo, ID, Idioma)
            End If

            If InStr(sRes, "@") <> 0 Then
                sRes = Replace(sRes, "@", StrError)
            End If

            MensajeError = sRes
        End Function
    End Class
End Namespace