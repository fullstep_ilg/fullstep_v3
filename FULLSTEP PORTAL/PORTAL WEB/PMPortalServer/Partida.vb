Namespace Fullstep.PMPortalServer
    Public Class Partida
        Inherits Fullstep.PMPortalServer.Security
        ''' <summary>
        ''' Obtiene el detalle de una partida (cod, denominaci�n, centro, fechas de inicio y de fin)
        ''' </summary>
        ''' <param name="lCiaComp">C�digo de la compa�ia</param>
        ''' <param name="sPRES5">Cod partida presupuestaria</param>
        ''' <param name="sTexto">Cadena que contiene las uons y los c�digos de pres5</param>
        ''' <param name="sIdioma">Idioma del usuario</param>
        ''' <returns>El detalle de la partida</returns>
        ''' <remarks>Llamada desde: detallepartida.aspx; Tiempo m�ximo: 1 sg;</remarks>
        Public Function BuscarDetallePartida(ByVal lCiaComp As Long, ByVal sPRES5 As String, ByVal sTexto As String, ByVal sIdioma As String) As DataSet
            Authenticate()
            If mRemottingServer Then
                BuscarDetallePartida = DBServer.Partida_Detalle(lCiaComp, sPRES5, sTexto, sIdioma, msSesionId, msIPDir, msPersistID)
            Else
                BuscarDetallePartida = DBServer.Partida_Detalle(lCiaComp, sPRES5, sTexto, sIdioma)
            End If
        End Function

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

#Region "A�o partida"

        Public Function DevolverDataAnyoPartida(ByVal lCiaComp As Long, ByVal sPartida0 As String, ByVal sPartida As String) As DataSet
            Authenticate()
            Return DBServer.AnyoPartida_Load(lCiaComp, sPartida0, sPartida)
        End Function
#End Region
    End Class
End Namespace
