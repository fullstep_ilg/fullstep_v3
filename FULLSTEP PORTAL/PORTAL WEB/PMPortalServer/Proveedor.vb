Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Proveedor
        Inherits Security

        Private msCod As String
        Private msDen As String
        Private msDir As String
        Private msCP As String
        Private msPob As String
        Private msPaiCod As String
        Private msPaiDen As String
        Private msProviCod As String
        Private msProviDen As String
        Private msMonCod As String
        Private msMonDen As String
        Private msObs As String
        Private msNIF As String
        Private msUrl As String

        Private moContactos As DataSet

        Property Cod() As String
            Get
                Cod = msCod
            End Get
            Set(ByVal Value As String)
                msCod = Value
            End Set
        End Property
        Property Den() As String
            Get
                Den = msDen
            End Get
            Set(ByVal Value As String)
                msDen = Value
            End Set
        End Property
        Property Dir() As String
            Get
                Dir = msDir
            End Get
            Set(ByVal Value As String)
                msDir = Value
            End Set
        End Property
        Property CP() As String
            Get
                CP = msCP
            End Get
            Set(ByVal Value As String)
                msCP = Value
            End Set
        End Property
        Property Pob() As String
            Get
                Pob = msPob
            End Get
            Set(ByVal Value As String)
                msPob = Value
            End Set
        End Property
        Property PaiCod() As String
            Get
                PaiCod = msPaiCod
            End Get
            Set(ByVal Value As String)
                msPaiCod = Value
            End Set
        End Property
        Property PaiDen() As String
            Get
                PaiDen = msPaiDen
            End Get
            Set(ByVal Value As String)
                msPaiDen = Value
            End Set
        End Property
        Property ProviCod() As String
            Get
                ProviCod = msProviCod
            End Get
            Set(ByVal Value As String)
                msProviCod = Value

            End Set
        End Property
        Property ProviDen() As String
            Get
                ProviDen = msProviDen
            End Get
            Set(ByVal Value As String)
                msProviDen = Value
            End Set
        End Property
        Property MonCod() As String
            Get
                MonCod = msMonCod
            End Get
            Set(ByVal Value As String)
                msMonCod = Value
            End Set
        End Property
        Property MonDen() As String
            Get
                MonDen = msMonDen
            End Get
            Set(ByVal Value As String)
                msMonDen = Value
            End Set
        End Property
        Property Obs() As String
            Get
                Obs = msObs
            End Get
            Set(ByVal Value As String)
                msObs = Value
            End Set
        End Property
        Property NIF() As String
            Get
                NIF = msNIF
            End Get
            Set(ByVal Value As String)
                msNIF = Value
            End Set
        End Property
        Property Url() As String
            Get
                Url = msUrl
            End Get
            Set(ByVal Value As String)
                msUrl = Value
            End Set
        End Property
        Public Property Contactos() As DataSet
            Get
                Return moContactos
            End Get

            Set(ByVal Value As DataSet)
                moContactos = Value
            End Set
        End Property
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
        ''' <summary>
        ''' Carga el proveeedor
        ''' </summary>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <remarks>Llamada desde: script\_common\campos.ascx   script\_common\desglose.ascx  ; Tiempo m�ximo: 0,2</remarks>
        Public Sub Load(ByVal sIdi As String, ByVal lCiaComp As Long)
            Dim oDS As DataSet

            Authenticate()
            If mRemottingServer Then
                oDS = DBServer.Proveedor_Get(sIdi, lCiaComp, msCod, msSesionId, msIPDir, msPersistID)
            Else
                oDS = DBServer.Proveedor_Get(sIdi, lCiaComp, msCod)
            End If

            If Not oDS.Tables(0).Rows.Count = 0 Then
                With oDS.Tables(0).Rows(0)
                    msCod = .Item("COD")
                    msDen = .Item("DEN")
                    msDir = DBNullToSomething(.Item("DIR"))
                    msCP = DBNullToSomething(.Item("CP"))
                    msPob = DBNullToSomething(.Item("POB"))
                    msPaiCod = DBNullToSomething(.Item("PAICOD"))
                    msPaiDen = DBNullToSomething(.Item("PAIDEN"))
                    msProviCod = DBNullToSomething(.Item("PROVICOD"))
                    msProviDen = DBNullToSomething(.Item("PROVIDEN"))
                    msMonCod = .Item("MONCOD")
                    msMonDen = .Item("MONDEN")
                    msObs = DBNullToSomething(.Item("OBS"))
                    msNIF = DBNullToSomething(.Item("NIF"))
                    msUrl = DBNullToSomething(.Item("URLPROVE"))
                End With
            End If
            oDS = Nothing
        End Sub
        ''' <summary>
        ''' carga los contactos de QA
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <remarks>Llamada desde: script\solicitudes\contactosserver2.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Sub CargarContactosQA(ByVal lCiaComp As Long)
            Authenticate()
            If mRemottingServer Then
                moContactos = DBServer.Proveedor_CargarContactos_QA(lCiaComp, msCod, msSesionId, msIPDir, msPersistID)
            Else
                moContactos = DBServer.Proveedor_CargarContactos_QA(lCiaComp, msCod)
            End If
        End Sub
        Public Function LoadActivities_NivelSuperior(ByVal sIdi As String, ByVal IdCia As Long) As DataTable
            Authenticate()
            Dim ds As DataSet
            If mRemottingServer Then
                ds = DBServer.Proveedor_LoadActivities_NivelSuperior(sIdi, IdCia, msSesionId, msIPDir, msPersistID)
            Else
                ds = DBServer.Proveedor_LoadActivities_NivelSuperior(sIdi, IdCia)
            End If
            Return ds.Tables(0)
        End Function
        Public Sub Guardar_Info_PersonaFisica(ByVal IdCia As Integer, ByVal UsuCod As String, _
                                              ByVal Nombre As String, ByVal PrimerApellido As String, ByVal SegundoApellido As String, ByVal UsuNif As String)
            Authenticate()
            If mRemottingServer Then
                DBServer.Guardar_Info_PersonaFisica(IdCia, Nombre, PrimerApellido, SegundoApellido, msSesionId, msIPDir, msPersistID)
            Else
                DBServer.Guardar_Info_PersonaFisica(IdCia, UsuCod, Nombre, PrimerApellido, SegundoApellido, UsuNif)
            End If
        End Sub
        Public Function Obtener_Info_PersonaFisica(ByVal IdCia As Integer) As Dictionary(Of String, String)
            Authenticate()
            Dim dt As DataTable
            If mRemottingServer Then
                dt = DBServer.Obtener_Info_PersonaFisica(IdCia, msSesionId, msIPDir, msPersistID)
            Else
                dt = DBServer.Obtener_Info_PersonaFisica(IdCia)
            End If
            Dim infoPersonaFisica As New Dictionary(Of String, String)
            infoPersonaFisica("NOMBRE") = dt.Rows(0)("PERSONAFISICA_NOMBRE").ToString
            infoPersonaFisica("PRIMER_APELLIDO") = dt.Rows(0)("PERSONAFISICA_APELLIDO1").ToString
            infoPersonaFisica("SEGUNDO_APELLIDO") = dt.Rows(0)("PERSONAFISICA_APELLIDO2").ToString
            infoPersonaFisica("ACEPTACION_CONDICIONES") = CType(dt.Rows(0)("ACEPTACION_CONDICIONES"), Boolean)
            Return infoPersonaFisica
        End Function
    End Class
End Namespace
