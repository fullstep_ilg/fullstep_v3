
Imports System
Imports System.Collections
Namespace Fullstep.PMPortalServer
    Public Class MultiIdioma
        Inherits DictionaryBase

        Default Public Property Item(ByVal key As String) As String
            Get
                Return CType(Dictionary(key), String)
            End Get
            Set(ByVal Value As String)
                Dictionary(key) = Value
            End Set
        End Property

        Public ReadOnly Property Keys() As ICollection
            Get
                Return Dictionary.Keys
            End Get
        End Property

        Public ReadOnly Property Values() As ICollection
            Get
                Return Dictionary.Values
            End Get
        End Property

        Public Sub Add(ByVal key As String, ByVal value As String)
            Dictionary.Add(key, value)
        End Sub

        Public Function Contains(ByVal key As String) As Boolean
            Return Dictionary.Contains(key)
        End Function

        Public Sub Remove(ByVal key As String)
            Dictionary.Remove(key)
        End Sub

    End Class

End Namespace