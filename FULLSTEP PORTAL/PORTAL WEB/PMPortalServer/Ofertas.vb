﻿Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class Ofertas
        Inherits Security

        Private iPargenInternoFirmaDigOfe As Integer
        Private sProveNif As String

        Public ReadOnly Property PargenInternoFirmaDigOfe() As Integer
            Get
                Return iPargenInternoFirmaDigOfe
            End Get
        End Property

        Public ReadOnly Property ProveNif() As String
            Get
                Return sProveNif
            End Get
        End Property

        ''' Revisado por: auv. Fecha: 07/01/2013
        ''' <summary>
        ''' Método que carga el valor del pargen interno FIRMA_DIG_OFERTAS.
        ''' </summary>
        ''' <param name="lCiaComp">Código de la empresa compradora</param>
        ''' <param name="lCiaProve">Código del proveedor</param>
        ''' <remarks>Llamada desde OfertasPDF.aspx.vb; Máx inferior a 1seg.</remarks>
        Public Sub LoadDataPargenInterno(ByVal lCiaComp As Long, ByVal lCiaProve As Long)
            Authenticate()
            Dim oParametosDataSet As System.Data.DataSet
            If mRemottingServer Then
                oParametosDataSet = DBServer.Parametros_GetData(lCiaComp)
            Else
                oParametosDataSet = DBServer.Parametros_GetData(lCiaComp)
            End If
            iPargenInternoFirmaDigOfe = CInt(oParametosDataSet.Tables(0).Rows(0)("FIRMA_DIG_OFERTAS"))
        End Sub

        ''' Revisado por: auv. Fecha: 11/01/2013
        ''' <summary>
        ''' Método que carga el nif del proveedor conectado.
        ''' </summary>
        ''' <param name="lCiaProve">Código del proveedor</param>
        ''' <remarks>Llamada desde visorOfertas.aspx.vb; Máx inferior a 1seg.</remarks>
        Public Sub LoadDataProve(ByVal lCiaProve As Long)
            Authenticate()
            Dim oParametosDataSet As System.Data.DataSet
            If mRemottingServer Then
                oParametosDataSet = DBServer.OfertasProveData_Get(lCiaProve)
            Else
                oParametosDataSet = DBServer.OfertasProveData_Get(lCiaProve)
            End If
            sProveNif = CStr(oParametosDataSet.Tables(0).Rows(0)("NIF"))
        End Sub

        ''' Revisado por: auv. Fecha: 06/03/2013
        ''' <summary>
        ''' Método para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">código de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petición</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde la creación de instancias. Máx. 0,1 seg.</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

    End Class
End Namespace
