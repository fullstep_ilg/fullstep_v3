Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class EstadosNoConf
        Inherits Security
        Private moEstados As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moEstados
            End Get

        End Property

        ''' <summary>
        ''' carga los estados
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="sDen">Denom de estado</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <remarks>Llamada desde: _common\campos.ascx.vb  _common\desglose.ascx.vb; Tiempo m�ximo: 0,1</remarks>
        Public Sub LoadData(ByVal lCiaComp As Long, ByVal lInstancia As Long, Optional ByVal sDen As String = Nothing, Optional ByVal sIdi As String = Nothing)
            Authenticate()
            If mRemottingServer Then
                moEstados = DBServer.EstadosNoConf_Get(lCiaComp, lInstancia, sDen, sIdi, msSesionId, msIPDir, msPersistID)
            Else
                moEstados = DBServer.EstadosNoConf_Get(lCiaComp, lInstancia, sDen, sIdi)
            End If
        End Sub
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class

End Namespace

