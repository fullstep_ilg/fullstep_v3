Imports System.Configuration
Imports System.Web.UI

Namespace Fullstep.PMPortalServer
    Public Class Notificar
        Inherits Security

        Private moLongitudesDeCodigos As TiposDeDatos.LongitudesDeCodigos
        Private mlIdUsu As Long
        Private mlIdCia As Long

        Private Email As Fullstep.FSNLibraryCOM.Email

        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">Usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

        ''' <summary>
        ''' Devolver /Establecer la propiedad Usuario Id
        ''' </summary>
        ''' <remarks></remarks>
        Public Property IdUsuario() As Long
            Get
                IdUsuario = mlIdUsu
            End Get
            Set(ByVal Value As Long)
                mlIdUsu = Value
            End Set
        End Property

        ''' <summary>
        ''' Devolver /Establecer la propiedad Compania Id
        ''' </summary>
        ''' <remarks></remarks>
        Public Property IdProveCia() As Long
            Get
                IdProveCia = mlIdCia
            End Get
            Set(ByVal Value As Long)
                mlIdCia = Value
            End Set
        End Property

        ''' <summary>
        ''' Enviar un Mail
        ''' </summary>
        ''' <param name="lCiaComp">Id de compania</param>
        ''' <param name="sFrom">From</param>
        ''' <param name="Subject">Subject</param>
        ''' <param name="Para">Para</param>
        ''' <param name="Message">Message</param>
        ''' <param name="isHTML">cuerpo en html o un txt</param>
        ''' <param name="isPortal">mail de portal u otro origen</param>
        ''' <param name="sCC">El CC del mensaje</param>
        ''' <param name="sCCO">El CCO del mensaje</param>
        ''' <param name="bEsPM">Si es mail de producto Pm o no</param>
        ''' <param name="sCodProve">Proveedor al q se le envia el mail</param>
        ''' <param name="lIdInstancia">Instancia</param>
        ''' <param name="sAdjuntos">Adjuntos</param>
        ''' <param name="sRuta">Ruta de adjuntos</param>
        ''' <remarks>Llamada desde: Esta misma pantalla en cada funcion q notifica algo; Tiempo m�ximo: 0,2</remarks>
        Public Sub EnviarMail(ByVal lCiaComp As Long, ByVal sFrom As String, ByVal Subject As String, ByVal Para As String, ByVal Message As String _
                            , Optional ByVal isHTML As Boolean = False, Optional ByVal isPortal As Boolean = True, Optional ByVal sCC As String = "" _
                            , Optional ByVal sCCO As String = "", Optional ByVal bEsPM As Boolean = False, Optional ByVal sCodProve As String = Nothing _
                            , Optional ByVal lIdInstancia As Long = Nothing, Optional ByVal sAdjuntos As String = Nothing, Optional ByVal sRuta As String = "")

            Dim Data As DataSet
            Dim iError As Integer = 2
            Dim iAuthentication As Integer
            Dim sUsu As String
            Dim strError As String = ""
            Dim lIdError As Long = -1

            If mRemottingServer Then
                Data = DBServer.Notificador_DevolverAutenticacion(msSesionId, msIPDir, msPersistID)
            Else
                Data = DBServer.Notificador_DevolverAutenticacion()
            End If

            Try
                iAuthentication = DBNullToInteger(Data.Tables(0).Rows(0).Item("P_AUTENTICACION"))
                Select Case iAuthentication
                    Case 2  'windows
                        sUsu = Data.Tables(0).Rows(0).Item("P_USU")
                    Case 1 'basica
                        sUsu = Data.Tables(0).Rows(0).Item("P_FROM")
                    Case Else
                        sUsu = ""
                End Select

                Email = New Fullstep.FSNLibraryCOM.Email(Data.Tables(0).Rows(0).Item("P_SERVIDOR").ToString, Data.Tables(0).Rows(0).Item("P_PUERTO").ToString _
                                                    , Data.Tables(0).Rows(0).Item("P_AUTENTICACION"), Data.Tables(0).Rows(0).Item("P_SSL") _
                                                    , Data.Tables(0).Rows(0).Item("P_METODO_ENTREGA").ToString, sUsu _
                                                    , Encrypter.EncryptTOD(Data.Tables(0).Rows(0).Item("P_PWD").ToString, "PWD", False) _
                                                    , Data.Tables(0).Rows(0).Item("P_DOMINIO").ToString, False)

                If sRuta <> "" Then sRuta = ConfigurationManager.AppSettings("temp") & "\" & sRuta & "\"

                'Subject sin "de parte de"
                Email.EnviarMail(Data.Tables(0).Rows(0).Item("P_FROM").ToString, sFrom, Subject, Para, Message, Data.Tables(0).Rows(0).Item("P_FROMNAME").ToString, , sCC, sCCO _
                                 , sAdjuntos, sRuta, isHTML, False, , iError, strError)

                If iError = 2 Then '2-ok
                    GrabarEnviarmailGS(lCiaComp, Data.Tables(0).Rows(0).Item("P_FROM").ToString, Subject, Para, Message, sCC, sCCO, bEsPM, isHTML, False, "", sCodProve, lIdInstancia, sAdjuntos, sRuta)
                Else '1- adjuntos 0- excepcion 3 - error ��log??
                    With System.Web.HttpContext.Current
                        lIdError = DBServer.Notificador_GrabarErroresGS(lCiaComp, .Request.FilePath, msUserCode, _
                                              "FSNLibraryCOM.Email Indica error", strError, "", _
                                              .Request.QueryString.ToString(), .Request.UserAgent)
                    End With
                    GrabarEnviarmailGS(lCiaComp, Data.Tables(0).Rows(0).Item("P_FROM").ToString, Subject, Para, Message, sCC, sCCO, bEsPM, isHTML, True, strError, sCodProve, lIdInstancia, sAdjuntos, sRuta, lIdError)
                End If

            Catch ex As Exception
                With System.Web.HttpContext.Current
                    lIdError = DBServer.Notificador_GrabarErroresGS(lCiaComp, .Request.FilePath, msUserCode, _
                                          ex.GetType().FullName, ex.Message, ex.StackTrace, _
                                          .Request.QueryString.ToString(), .Request.UserAgent)
                End With
                GrabarEnviarmailGS(lCiaComp, Data.Tables(0).Rows(0).Item("P_FROM").ToString, Subject, Para, Message, sCC, sCCO, bEsPM, isHTML, True, ex.Message, sCodProve, lIdInstancia, sAdjuntos, sRuta, lIdError)
            Finally
                Email.FuerzaFinalizeSmtpClient()
                Email = Nothing
                Data = Nothing
            End Try

        End Sub

        ''' <summary>
        ''' Graba en bbdd el mail enviado, sea correctamente enviado o no 
        ''' </summary>
        ''' <param name="lIdCiaComp">Id de compania</param>
        ''' <param name="From">From</param>
        ''' <param name="Subject">Subject</param>
        ''' <param name="Para">Para</param>
        ''' <param name="Message">Message</param>
        ''' <param name="sCC">El CC del mensaje</param>
        ''' <param name="sCCO">El CCO del mensaje</param>
        ''' <param name="bEsPM">Si es mail de producto Pm o no</param>
        ''' <param name="isHTML">si es html o txt</param>
        ''' <param name="bKO">correctamente enviado o no</param>
        ''' <param name="sTextoError">no correctamente enviado, aqui viene el posible error</param>
        ''' <param name="sCodProve">Proveedor al q se le envia el mail</param>
        ''' <param name="lIdInstancia">Instancia</param>
        ''' <param name="sAdjuntos">Adjuntos</param>
        ''' <param name="sRuta">Ruta de adjuntos</param>
        ''' <param name="lIdError">Id del error</param>
        ''' <remarks>Llamada desde: EnviarMail       ; Tiempo m�ximo: 0,2</remarks>
        Private Sub GrabarEnviarmailGS(ByVal lIdCiaComp As Long, ByVal From As String, ByVal Subject As String, ByVal Para As String, ByVal Message As String, ByVal sCC As String, ByVal sCCO As String, ByVal bEsPM As Boolean, ByVal isHTML As Boolean, ByVal bKO As Boolean, ByVal sTextoError As String, ByVal sCodProve As String, ByVal lIdInstancia As Long, ByVal sAdjuntos As String, ByVal sRuta As String, Optional ByVal lIdError As Long = -1)
            If mRemottingServer Then
                DBServer.Notificador_GrabarEnviarMensajeGS(lIdCiaComp, From, Subject, Para, Message, sCC, sCCO, bEsPM, isHTML, bKO, sTextoError, sAdjuntos, sRuta, sCodProve, 0, lIdInstancia, 0, "", lIdError, msSesionId, msIPDir, msPersistID)
            Else
                DBServer.Notificador_GrabarEnviarMensajeGS(lIdCiaComp, From, Subject, Para, Message, sCC, sCCO, bEsPM, isHTML, bKO, sTextoError, sAdjuntos, sRuta, sCodProve, 0, lIdInstancia, 0, "", lIdError)
            End If
        End Sub

        ''' <summary>
        ''' Al destruir la clase hay q asegurarse de liberar todo
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overrides Sub Finalize()
            MyBase.Finalize()

            If Not IsNothing(Email) Then
                Email.FuerzaFinalizeSmtpClient()
                Email = Nothing
            End If
        End Sub

        ''' <summary>
        ''' Hay q asegurarse de liberar todo
        ''' </summary>
        ''' <remarks>Llamada desde: Toda funcion q quiera notificar; Tiempo maximo:0</remarks>
        Public Sub FuerzaFinalizeSmtpClient()
            If Not IsNothing(Email) Then
                Email.FuerzaFinalizeSmtpClient()
                Email = Nothing
            End If
        End Sub
    End Class
End Namespace
