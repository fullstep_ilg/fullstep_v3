Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class TablaExterna
        Inherits Security

#Region "Varialbes privadas"
        Private mlID As Long
        Private moDefTabla As DataSet
        Private moData As DataSet
        Private msPrimaryKey As String
        Private msArtKey As String
        Private msCampoMostrar As String
        Private msNombre As String
#End Region
#Region "Propiedades"
        Public Property ID() As Long
            Get
                Return mlID
            End Get
            Set(ByVal Value As Long)
                If mlID <> Value Then
                    mlID = Value
                    moData = Nothing
                    moDefTabla = Nothing
                    msPrimaryKey = Nothing
                    msArtKey = Nothing
                    msCampoMostrar = Nothing
                    msNombre = Nothing
                End If
            End Set
        End Property
        Public ReadOnly Property Nombre() As String
            Get
                Return msNombre
            End Get
        End Property
        Public ReadOnly Property DefTabla() As Data.DataSet
            Get
                Return moDefTabla
            End Get

        End Property
        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moData
            End Get
        End Property
        Public ReadOnly Property PrimaryKey() As String
            Get
                Return msPrimaryKey
            End Get
        End Property
        Public ReadOnly Property ArtKey() As String
            Get
                Return msArtKey
            End Get
        End Property
        Public ReadOnly Property CampoMostrar() As String
            Get
                Return msCampoMostrar
            End Get
        End Property
#End Region
#Region "M�todos"
        Public Sub LoadDefTabla(ByVal lCiaComp As Long)
            LoadDefTabla(lCiaComp, mlID)
        End Sub
        Public Sub LoadDefTabla(ByVal lCiaComp As Long, ByVal bFiltroArticulo As Boolean)
            LoadDefTabla(lCiaComp, mlID, Nothing, bFiltroArticulo)
        End Sub
        Public Sub LoadDefTabla(ByVal lCiaComp As Long, ByVal lId As Long)
            LoadDefTabla(lCiaComp, lId, Nothing, True)
        End Sub
        Public Sub LoadDefTabla(ByVal lCiaComp As Long, ByVal lId As Long, ByVal bFiltroArticulo As Boolean)
            LoadDefTabla(lCiaComp, lId, Nothing, bFiltroArticulo)
        End Sub
        Public Sub LoadDefTabla(ByVal lCiaComp As Long, ByVal sArt As String)
            LoadDefTabla(lCiaComp, mlID, sArt, True)
        End Sub
        ''' <summary>
        ''' Devolver la informacion de campos de una tabla externa
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lId">Id de tabla externa</param>
        ''' <param name="sArt">valor del articulo</param>
        ''' <param name="bFiltroArticulo">Si se filtra por articulo o no</param>
        ''' <remarks>Llamada desde: Difrentes sobrecargas de LoadDefTabla   script\_common\campos.ascx    script\_common\desglose.ascx   script\_common\tablaexterna.aspx
        ''' script\_common\tablaexternareg.aspx    script\_common\tablaexternaserver.aspx    script\noconformidad\impexp.aspx; Tiempo m�ximo: 0,2</remarks>
        Public Sub LoadDefTabla(ByVal lCiaComp As Long, ByVal lId As Long, ByVal sArt As String, ByVal bFiltroArticulo As Boolean)
            Authenticate()
            If mRemottingServer Then
                moDefTabla = DBServer.TablaExterna_LoadDefTabla(lCiaComp, lId, sArt, bFiltroArticulo, msSesionId, msIPDir, msPersistID)
            Else
                moDefTabla = DBServer.TablaExterna_LoadDefTabla(lCiaComp, lId, sArt, bFiltroArticulo)
            End If
            mlID = lId
            msNombre = DBNullToSomething(moDefTabla.Tables(1).Rows(0).Item("DENOMINACION"))
            msPrimaryKey = moDefTabla.Tables(1).Rows(0).Item("PRIMARYKEY")
            msArtKey = DBNullToSomething(moDefTabla.Tables(1).Rows(0).Item("ARTKEY"))
            msCampoMostrar = DBNullToSomething(moDefTabla.Tables(1).Rows(0).Item("CAMPO_MOSTRAR"))
        End Sub
        Public Sub LoadData(ByVal lCiaComp As Long)
            LoadData(lCiaComp, mlID)
        End Sub
        Public Sub LoadData(ByVal lCiaComp As Long, ByVal bFiltroArticulo As Boolean)
            LoadData(lCiaComp, mlID, Nothing, bFiltroArticulo)
        End Sub
        Public Sub LoadData(ByVal lCiaComp As Long, ByVal lId As Long)
            LoadData(lCiaComp, lId, Nothing, True)
        End Sub
        Public Sub LoadData(ByVal lCiaComp As Long, ByVal lId As Long, ByVal bFiltroArticulo As Boolean)
            LoadData(lCiaComp, lId, Nothing, bFiltroArticulo)
        End Sub
        Public Sub LoadData(ByVal lCiaComp As Long, ByVal sArt As String)
            LoadData(lCiaComp, mlID, sArt, True)
        End Sub
        ''' <summary>
        ''' Devolver la informacion de campos de una tabla externa
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="lId">Id de tabla externa</param>
        ''' <param name="sArt">valor del articulo</param>
        ''' <param name="bFiltroArticulo">Si se filtra por articulo o no</param>
        ''' <remarks>Llamada desde: Diferntes sobrecargas en esta pantalla  script\_common\campos.ascx    script\_common\desglose.ascx   script\_common\tablaexterna.aspx
        ''' script\_common\tablaexternareg.aspx    script\_common\tablaexternaserver.aspx    script\noconformidad\impexp.aspx; Tiempo m�ximo: 0,2</remarks>
        Public Sub LoadData(ByVal lCiaComp As Long, ByVal lId As Long, ByVal sArt As String, ByVal bFiltroArticulo As Boolean)
            Authenticate()
            If mRemottingServer Then
                moData = DBServer.TablaExterna_LoadData(lCiaComp, lId, sArt, bFiltroArticulo, msSesionId, msIPDir, msPersistID)
            Else
                moData = DBServer.TablaExterna_LoadData(lCiaComp, lId, sArt, bFiltroArticulo)
            End If
        End Sub
        Public Function BuscarRegistro(ByVal lCiaComp As Long, ByVal sTexto As String, Optional ByVal sArt As String = Nothing, Optional ByVal bBusqExacta As Boolean = False) As DataRow
            Authenticate()
            Return DBServer.TablaExterna_BuscarRegistro(lCiaComp, mlID, sTexto, bBusqExacta, sArt)
        End Function
        Public Function DescripcionReg(ByVal lCiaComp As Long, ByVal ValorKey As Object, ByVal sIdi As String, ByVal DateFormat As Globalization.DateTimeFormatInfo, ByVal NumberFormat As Globalization.NumberFormatInfo) As String
            Dim sValor As String
            Dim oRow As DataRow = BuscarRegistro(lCiaComp, ValorKey, Nothing, True)
            sValor = DescripcionReg(oRow, sIdi, DateFormat, NumberFormat)
            Return sValor
        End Function
        Public Function DescripcionReg(ByVal RegTabla As DataRow, ByVal sIdi As String, ByVal DateFormat As Globalization.DateTimeFormatInfo, ByVal NumberFormat As Globalization.NumberFormatInfo) As String
            Dim sValor As String = Nothing
            If Not (moDefTabla Is Nothing Or RegTabla Is Nothing) Then
                Dim sTipoClave As String
                Dim sTipoMostrar As String
                For Each oRow As DataRow In moDefTabla.Tables(0).Rows
                    If UCase(oRow.Item("CAMPO")) = UCase(msPrimaryKey) Then
                        sTipoClave = oRow.Item("TIPOCAMPO")
                    ElseIf UCase(oRow.Item("CAMPO")) = UCase(msCampoMostrar) Then
                        sTipoMostrar = oRow.Item("TIPOCAMPO")
                    End If
                    If Not sTipoClave Is Nothing And (Not sTipoMostrar Is Nothing Or msCampoMostrar Is Nothing) Then _
                        Exit For
                Next
                If Not sTipoClave Is Nothing Then
                    Dim dsTextos As DataSet = DBServer.Dictionary_GetData(TiposDeDatos.ModulosIdiomas.TablaExterna, sIdi)
                    Dim oRow As DataRow = RegTabla
                    If Not IsNothing(oRow) Then
                        Select Case sTipoClave
                            Case "FECHA"
                                sValor = CType(oRow.Item(msPrimaryKey), Date).ToString("d", DateFormat)
                            Case "NUMERICO"
                                Dim dblValor As Double = CType(oRow.Item(msPrimaryKey), Double)
                                sValor = dblValor.ToString("N" & IIf((dblValor Mod 1) > 0, "", "0"), NumberFormat)
                            Case Else
                                sValor = oRow.Item(msPrimaryKey).ToString()
                        End Select
                        If Not msCampoMostrar Is Nothing Then
                            Select Case sTipoMostrar
                                Case "FECHA"
                                    sValor = sValor & CType(oRow.Item(msCampoMostrar), Date).ToString("d", DateFormat)
                                Case "NUMERICO"
                                    Dim dblValor As Double = CType(oRow.Item(msCampoMostrar), Double)
                                    sValor = sValor & dblValor.ToString("N" & IIf((dblValor Mod 1) > 0, "", "0"), NumberFormat)
                                Case Else
                                    If oRow.Table.Columns(msCampoMostrar).DataType.ToString() = "System.Boolean" Then
                                        If oRow.Item(msCampoMostrar) Then
                                            sValor = sValor & " " & dsTextos.Tables(0).Select("ID=4")(0).Item(1)
                                        Else
                                            sValor = sValor & " " & dsTextos.Tables(0).Select("ID=5")(0).Item(1)
                                        End If
                                    Else
                                        sValor = sValor & " " & oRow.Item(msCampoMostrar).ToString()
                                    End If
                            End Select
                        End If
                    End If
                End If
            End If
            Return sValor
        End Function
        Public Function CodigoArticuloReg(ByVal ValorKey As Object) As String
            Dim sCodArt As String = Nothing
            If Not msArtKey Is Nothing Then
                For Each oRow As DataRow In moData.Tables(0).Rows
                    If oRow.Item(msPrimaryKey) = ValorKey Then
                        sCodArt = DBNullToSomething(oRow.Item(msArtKey))
                        Exit For
                    End If
                Next
            End If
            Return sCodArt
        End Function
#End Region
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
#Region "M�todos privados"
        Private Function QuitarEspacios(ByVal Cadena As String) As String
            Cadena = Trim(Cadena)
            Do While InStr(Cadena, "  ") > 0
                Replace(Cadena, "  ", " ")
            Loop
            Return Cadena
        End Function
#End Region
    End Class
End Namespace