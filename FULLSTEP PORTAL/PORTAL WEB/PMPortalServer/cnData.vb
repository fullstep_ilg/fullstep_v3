﻿
Namespace Fullstep.PMPortalServer
    Public Class cnData
        Inherits Fullstep.PMPortalServer.Security
        ''' <summary>
        ''' Método para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">código de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petición</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo máximo: 0</remarks>
        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal mRemottingServer As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, mRemottingServer, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="CiaComp"></param>
        ''' <param name="ProveCod"></param>
        ''' <param name="IdContacto"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Comprobar_HayMensajesHistoricos(ByVal CiaComp As Integer, ByVal ProveCod As String, ByVal IdContacto As Integer) As Boolean
            Authenticate()
            Try
                Return DBServer.CN_Comprobar_HayMensajesHistoricos(CiaComp, ProveCod, IdContacto, msSesionId, msIPDir, msPersistID)
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Public Function Comprobar_PermisoConsulta_NoConformidad(ByVal CiaComp As Integer, ByVal ProveCod As String, _
                                ByVal Instancia As Integer) As System.Collections.Generic.Dictionary(Of String, Integer)
            Authenticate()
            Try
                Dim dt As DataTable = DBServer.CN_Comprobar_PermisoConsulta_NoConformidad(CiaComp, ProveCod, Instancia, msSesionId, msIPDir, msPersistID)
                Dim oNC As New System.Collections.Generic.Dictionary(Of String, Integer)
                If dt.Rows.Count = 1 Then
                    oNC("idNC") = dt.Rows(0)("ID")
                    oNC("instanciaNC") = dt.Rows(0)("INSTANCIA")
                End If
                Return oNC
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Public Function Comprobar_PermisoConsulta_Certificado(ByVal CiaComp As Integer, ByVal ProveCod As String, _
                                ByVal Instancia As Integer) As System.Collections.Generic.Dictionary(Of String, Integer)
            Authenticate()
            Try
                Dim dt As DataTable = DBServer.CN_Comprobar_PermisoConsulta_Certificado(CiaComp, ProveCod, Instancia, msSesionId, msIPDir, msPersistID)
                Dim oCert As New System.Collections.Generic.Dictionary(Of String, Integer)
                If dt.Rows.Count = 1 Then
                    oCert("idCert") = dt.Rows(0)("ID")
                    oCert("instanciaCert") = IIf(dt.Rows(0)("ESALTA") = 0, 0, dt.Rows(0)("INSTANCIA"))
                End If
                Return oCert
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Public Function Comprobar_PermisoConsulta_Factura(ByVal CiaComp As Integer, ByVal ProveCod As String, ByVal Contacto As Integer, ByVal Idioma As String, _
                                ByVal IdFactura As Integer) As System.Collections.Generic.Dictionary(Of String, Integer)
            Authenticate()
            Try
                Dim dt As DataTable = DBServer.CN_Comprobar_PermisoConsulta_Factura(CiaComp, ProveCod, Contacto, IdFactura, Idioma, msSesionId, msIPDir, msPersistID)
                Dim oFact As New System.Collections.Generic.Dictionary(Of String, Integer)
                If dt.Rows.Count = 1 Then
                    oFact("idFact") = dt.Rows(0)("ID")
                End If
                Return oFact
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Public Function Comprobar_PermisoConsulta_Solicitud(ByVal CiaComp As Integer, ByVal ProveCod As String, ByVal Idioma As String, _
                                ByVal Instancia As Integer) As System.Collections.Generic.Dictionary(Of String, String)
            Authenticate()
            Try
                Dim dt As DataTable = DBServer.CN_Comprobar_PermisoConsulta_Solicitud(CiaComp, ProveCod, Idioma, Instancia, msSesionId, msIPDir, msPersistID)
                Dim oSol As New System.Collections.Generic.Dictionary(Of String, String)
                If dt.Rows.Count = 1 Then
                    oSol("instanciaSol") = dt.Rows(0)("INSTANCIA")
                    oSol("contrato") = dt.Rows(0)("CONTRATO").ToString
                    oSol("codContrato") = dt.Rows(0)("CODIGO_CONTRATO").ToString
                End If
                Return oSol
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
    End Class
End Namespace

