Option Strict Off
Option Explicit On

Namespace Fullstep.PMPortalServer
    <Serializable()>
    <System.Runtime.InteropServices.ProgId("CProveERP_NET.CProveERP")> Public Class CProveERP

        Private m_sCod As String
        Private m_sDen As String
        Private m_sNif As Object
        Private m_vCampo1 As Object
        Private m_vCampo2 As Object
        Private m_vCampo3 As Object
        Private m_vCampo4 As Object
        Private m_bBajaLogica As Boolean
        Private mbUltimo As Boolean

        Public Property BajaLogica() As Boolean
            Get
                BajaLogica = m_bBajaLogica
            End Get
            Set(ByVal Value As Boolean)
                m_bBajaLogica = Value
            End Set
        End Property
        Public Property Cod() As String
            Get
                Cod = m_sCod
            End Get
            Set(ByVal Value As String)
                m_sCod = Value
            End Set
        End Property
        Public Property Den() As String
            Get
                Den = m_sDen
            End Get
            Set(ByVal Value As String)
                m_sDen = Value
            End Set
        End Property
        Public Property Ultimo() As Boolean
            Get
                Ultimo = mbUltimo
            End Get
            Set(ByVal Value As Boolean)
                mbUltimo = Value
            End Set
        End Property
        Public Property Nif() As Object
            Get
                Nif = m_sNif
            End Get
            Set(ByVal Value As Object)
                m_sNif = Value
            End Set
        End Property
        Public Property Campo1() As Object
            Get
                Campo1 = m_vCampo1
            End Get
            Set(ByVal Value As Object)
                m_vCampo1 = Value
            End Set
        End Property
        Public Property Campo2() As Object
            Get
                Campo2 = m_vCampo2
            End Get
            Set(ByVal Value As Object)
                m_vCampo2 = Value
            End Set
        End Property
        Public Property Campo3() As Object
            Get
                Campo3 = m_vCampo3
            End Get
            Set(ByVal Value As Object)
                m_vCampo3 = Value
            End Set
        End Property
        Public Property Campo4() As Object
            Get
                Campo4 = m_vCampo4
            End Get
            Set(ByVal Value As Object)
                m_vCampo4 = Value
            End Set
        End Property
    End Class
End Namespace