﻿Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class ProveedoresERP
        Inherits Security

        ''' <summary>
        ''' Método para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">código de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petición</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo máximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

        ''' <summary>
        ''' Devuelve los datos de los proveedores ERP para la el proveedor y la organizacion de compras, se utiliza para el campo proveedor ERP(143) de PM
        ''' </summary>
        ''' <param name="sProve">Codigo del proveedor</param>
        ''' <param name="sOrgCompras">Codigo de la organizacion de compras</param>
        ''' <returns>Dataset con los datos de los proveedores ERP</returns>
        ''' <remarks></remarks>
        Public Function CargarProveedoresERPtoDS(ByVal lCiaComp As Long, Optional ByVal sProve As String = Nothing, Optional ByVal sOrgCompras As String = Nothing, Optional ByVal sCodERP As String = Nothing) As DataSet
            Dim rs As New DataSet
            rs = DBServer.ProveedoresERP_CargarProveedoresERPtoDS(lCiaComp, sProve, sOrgCompras, sCodERP)
            Return rs
        End Function
    End Class

End Namespace
