﻿Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class ViasPago
        Inherits Security
        Private moViasPago As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moViasPago
            End Get
        End Property

        ''' <summary>
        ''' Procedimiento que carga los datos de las vías de pago disponibles
        ''' </summary>
        ''' <param name="sIdi">Idioma de la aplicación</param>
        ''' <param name="lCiaComp">Id de la compañia</param>
        ''' <param name="sCod">Código de la vía de pago</param>
        ''' <remarks>
        ''' Llamada desde: /Facturas/DetalleFactura.aspx
        ''' Tiempo máximo: 0,32 seg </remarks>
        Public Sub LoadData(ByVal sIdi As String, ByVal lCiaComp As Long, Optional ByVal sCod As String = "")
            Authenticate()
            moViasPago = DBServer.ViasPago_Get(sIdi, lCiaComp, sCod)
        End Sub

        Public Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace

