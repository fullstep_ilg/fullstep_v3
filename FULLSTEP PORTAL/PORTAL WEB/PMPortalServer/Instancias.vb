Namespace Fullstep.PMPortalServer

    <Serializable()> _
Public Class Instancias
        Inherits Fullstep.PMPortalServer.Security

        Private moInstancias As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moInstancias
            End Get

        End Property

        ''' <summary>
        ''' Carga los procesos que requieren la intervenci�n del usuario y el hist�rico de procesos en los que particip�.
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sProve">Proveedor</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lTipo">Id de solicitud</param>
        ''' <param name="sFecha">Fecha de alta</param>
        ''' <param name="lIdInstancia">Instancia</param>
        ''' <param name="iEstado">Estado de instancia</param>
        ''' <param name="sFormatoFecha">Formato de fecha</param>
        ''' <remarks>Llamada desde: script\solicitudes\seguimientoSolicitudes.aspx ; Tiempo m�ximo: 0,2</remarks>
        Public Sub DevolverWorkflow(ByVal lCiaComp As Long, ByVal sProve As String, ByVal EmpresaPortal As Boolean, ByVal NomPortal As String,
                                    Optional ByVal sIdi As String = "SPA", Optional ByVal lTipo As Long = Nothing, Optional ByVal sFecha As String = Nothing, Optional ByVal lIdInstancia As Long = 0, Optional ByVal iEstado As Integer = 0, Optional ByVal sFormatoFecha As String = "")
            Authenticate()
            If mRemottingServer Then
                moInstancias = DBServer.Instancias_Workflow(lCiaComp, sProve, EmpresaPortal, NomPortal, sIdi, lTipo, sFecha, lIdInstancia, iEstado, sFormatoFecha, msSesionId, msIPDir, msPersistID)
            Else
                moInstancias = DBServer.Instancias_Workflow(lCiaComp, sProve, EmpresaPortal, NomPortal, sIdi, lTipo, sFecha, lIdInstancia, iEstado, sFormatoFecha)
            End If
        End Sub

        ''' <summary>
        ''' Saca las solicitudes relacionadas , las q tengan campos de tipo Importe solicitudes vinculadas
        ''' </summary>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="idSolPadre">Instancia</param>
        ''' <remarks>Llamada desde: script\_common\detallesolicitudpadre.aspx     script\_common\guardarinstancia.aspx; Tiempo m�ximo: 0,2</remarks>
        Public Sub DevolverSolicitudesPadre(ByVal lCiaComp As Long, ByVal sIdi As String, Optional ByVal idSolPadre As Integer = 0)
            Authenticate()
            If mRemottingServer Then
                moInstancias = DBServer.Instancias_Padre(lCiaComp, sIdi, idSolPadre, msSesionId, msIPDir, msPersistID)
            Else
                moInstancias = DBServer.Instancias_Padre(lCiaComp, sIdi, idSolPadre)
            End If
        End Sub

        Public Function DevolverSolicitudes(ByVal lCiaComp As Long, ByVal ProveCod As String, ByVal IdCon As Integer, ByVal Idioma As String, ByVal IdEscenario As Integer,
             ByVal SentenciaWhere As String, ByVal SentenciaOrder As String, ByVal SentenciaWhere_CamposGenerales As String,
             ByVal Pagina As Integer, ByVal NumeroRegistros As Integer, ByVal BuscarProveArt As Integer,
             Optional ByVal AbtasUsted As Boolean = True, Optional ByVal Participo As Boolean = True, Optional ByVal Pendientes As Boolean = True,
             Optional ByVal PendientesDevolucion As Boolean = True, Optional ByVal Trasladadas As Boolean = True, Optional ByVal Otras As Boolean = True,
             Optional ByVal FiltroDesglose As Boolean = False, Optional ByVal VistaDesglose As Boolean = False,
             Optional ByVal TipoVisor As FSNLibrary.TipoVisor = FSNLibrary.TipoVisor.Solicitudes) As DataSet
            Authenticate()

            Dim TipoSolicitud As Integer?
            Select Case TipoVisor
                Case FSNLibrary.TipoVisor.NoConformidades
                    TipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad
                Case FSNLibrary.TipoVisor.Certificados
                    TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado
                Case FSNLibrary.TipoVisor.Contratos
                    TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Contrato
                Case FSNLibrary.TipoVisor.Facturas
                    TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Factura
                Case FSNLibrary.TipoVisor.Encuestas
                    TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Encuesta
                Case FSNLibrary.TipoVisor.SolicitudesQA
					TipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudQA
				Case FSNLibrary.TipoVisor.Proyectos
					TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Proyecto
				Case FSNLibrary.TipoVisor.Otras
                    TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Otros
                Case FSNLibrary.TipoVisor.PedidosExpres
                    TipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoExpress
                Case Else
                    TipoSolicitud = Nothing
            End Select
            If mRemottingServer Then
                Return DBServer.FSN_ObtenerSolicitudes(lCiaComp, ProveCod, IdCon, Idioma, IdEscenario, SentenciaWhere, SentenciaOrder, SentenciaWhere_CamposGenerales,
                                                       Pagina, NumeroRegistros, BuscarProveArt, AbtasUsted, Participo, Pendientes, PendientesDevolucion,
                                                       Trasladadas, Otras, FiltroDesglose, VistaDesglose, TipoSolicitud,
                                                       msSesionId, msIPDir, msPersistID)
            Else
                Return DBServer.FSN_ObtenerSolicitudes(lCiaComp, ProveCod, IdCon, Idioma, IdEscenario, SentenciaWhere, SentenciaOrder, SentenciaWhere_CamposGenerales,
                                                       Pagina, NumeroRegistros, BuscarProveArt, AbtasUsted, Participo, Pendientes, PendientesDevolucion,
                                                       Trasladadas, Otras, FiltroDesglose, VistaDesglose, TipoSolicitud)
            End If
        End Function
        ''' <summary>
        ''' Procedimiento que actualiza el estado de una instancia a En proceso
        ''' </summary>
        ''' <param name="sInstancias">Identificador de la instancia</param>
        ''' <param name="CodProveGS">Codigo de usuario</param>
        ''' <remarks>
        ''' Llamada desde: PmWeb/detalleSolicConsulta/AprobarRechazarInstancia, PmWeb/PROCESARMultAprobRech/Page_load , PmWeb/visorSolicitudes/Procesar_Acciones
        ''' tiempo m�ximo: 0,5 seg</remarks>
        Public Sub Actualizar_En_Proceso2(ByVal lCiaComp As Long, ByVal sInstancias As String, ByVal CodProveGS As String)
            Authenticate()
            If mRemottingServer Then
                DBServer.Actualizar_En_Proceso2(lCiaComp, sInstancias, CodProveGS, msSesionId, msIPDir, msPersistID)
            Else
                DBServer.Actualizar_En_Proceso2(lCiaComp, sInstancias, CodProveGS)
            End If

        End Sub
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace