Namespace Fullstep.PMPortalServer
    <Serializable()> _
        Public Class Proveedores
        Inherits Security
        Private moData As DataSet

        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moData
            End Get

        End Property


        ''' <summary>
        ''' carga los datos de los todos los proveedores
        ''' </summary>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="lCiaComp">Id de la compa�ia</param>
        ''' <param name="sCod">Proveedor</param>
        ''' <param name="sDen">Denominacion de proveedor</param>
        ''' <param name="sNif">Nif de proveedor</param>
        ''' <param name="sUser">Usuario</param>
        ''' <param name="sOrgCompras">Codigo de la organizacion de compras</param>
        ''' <remarks>Llamada desde: script\_common\proveedores.aspx.vb     script\solicitudes\detalleSolicitud.aspx   script\solicitudes\proveedorescontacto.aspx; Tiempo m�ximo: 0,2</remarks>
        Public Sub LoadData(ByVal sIdi As String, ByVal lCiaComp As Long, Optional ByVal sCod As String = Nothing, Optional ByVal sDen As String = Nothing, Optional ByVal sNif As String = Nothing, Optional ByVal sUser As String = Nothing, Optional ByVal sOrgCompras As String = Nothing)
            Authenticate()
            If mRemottingServer Then
                moData = DBServer.Proveedores_Load(sIdi, lCiaComp, sCod, sDen, sNif, True, sUser, msSesionId, msIPDir, msPersistID, sOrgCompras)
            Else
                moData = DBServer.Proveedores_Load(sIdi, lCiaComp, sCod, sDen, sNif, True, sUser, , , sOrgCompras)
            End If

        End Sub



        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace
