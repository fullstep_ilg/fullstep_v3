﻿Namespace Fullstep.PMPortalServer
    Public Class Parametros
        Inherits Security

        ''' Revisado por: blp. Fecha: 24/01/2012
        ''' <summary>
        ''' Función que devuelve un texto concreto de la tabla PARGEN_LIT para el idioma requerido
        ''' </summary>
        ''' <param name="lId">Id del literal</param>
        ''' <param name="sIdi">Códgio de idioma</param>
        ''' <param name="lCiaComp">ID de la empresa</param>
        ''' <returns>string literal</returns>
        ''' <remarks>
        ''' Llamada desde: entregasPedidos.aspx
        ''' Tiempo máximo: 0,1 seg.</remarks>
        Public Function CargarLiteralParametros(ByVal lId As Long, ByVal sIdi As String, ByVal lCiaComp As Long) As String
            Authenticate()
            Dim DBServer As New PMPortalDatabaseServer.Root

            Return DBServer.Parametros_CargarLiteralParametros(lId, sIdi, lCiaComp)

        End Function


        ''' <summary>
        ''' Funcion que devuelve los parametros generales
        ''' </summary>
        ''' <param name="lCiaComp">id de la compaÃ±ia</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function CargarParametrosGenerales(ByVal lCiaComp As Long) As DataSet
            Authenticate()

            Dim DBServer As New PMPortalDatabaseServer.Root

            Return DBServer.Parametros_CargarParametrosGenerales(lCiaComp)
        End Function

        ''' Revisado por: blp. Fecha: 24/01/2012
        ''' <summary>
        ''' Constructor de la clase Parametros
        ''' </summary>
        ''' <param name="dbserver">Servidor de la BBDD</param>
        ''' <param name="remotting">Si es conexión remota</param>
        ''' <param name="UserCode">Codigo de usuario</param>
        ''' <param name="UserPassword">Password de usuario</param>
        ''' <param name="isAuthenticated">Si está autenticado</param>
        ''' <remarks>
        ''' Llamada desde: Todos los sitios donde se instancie un objeto de esta clase
        ''' Tiempo máximo: 0 sec</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

    End Class
End Namespace
