﻿
Namespace Fullstep.PMPortalServer
    <Serializable()> _
    Public Class OfertasHistoricas
        Inherits Security

        Private moOfertasHistoricas As DataSet
        Private moOfertaHistoricaAdjun As Long
        Private moOfertasHistoricasAnyos As DataSet
        Private moOfertasHistoricasGmn1s As DataSet

        ''' <summary>
        ''' Devuelve las ofertas históricas cargadas por el método LoadData
        ''' </summary>
        Public ReadOnly Property Data() As Data.DataSet
            Get
                Return moOfertasHistoricas
            End Get
        End Property

        ''' <summary>
        ''' Devuelve las ofertas históricas cargadas por el método LoadAdjun
        ''' </summary>
        Public ReadOnly Property Adjun() As Long
            Get
                Return moOfertaHistoricaAdjun
            End Get
        End Property

        ''' <summary>
        ''' Devuelve los Anyos de las ofertas históricas
        ''' </summary>
        Public ReadOnly Property Anyos() As Data.DataSet
            Get
                Return moOfertasHistoricasAnyos
            End Get
        End Property

        ''' <summary>
        ''' Devuelve los GMN1s de las ofertas históricas
        ''' </summary>
        Public ReadOnly Property Gmn1s() As Data.DataSet
            Get
                Return moOfertasHistoricasGmn1s
            End Get
        End Property


        ''' Revisado por: auv. Fecha: 07/01/2013
        ''' <summary>
        ''' Método que carga las ofertas históricas efectuadas por el proveedor.
        ''' </summary>
        ''' <param name="lCiaComp">Código de la empresa compradora</param>
        ''' <param name="lCiaProve">Código del proveedor</param>
        ''' <param name="FecOfeDesde">Fecha a partir de la cual filtrar la fecha de ofertas históricas</param>
        ''' <param name="FecOfeHasta">Fecha hasta la cual filtrar la fecha de ofertas históricas</param>
        ''' <param name="Anio">Año del proceso</param>
        ''' <param name="Gmn1">GMN1 del proceso</param>
        ''' <param name="Proce">Código del proceso</param>
        ''' <param name="DenProce">Denominación del proceso</param>
        ''' <param name="CodArt">Código del artículo</param>
        ''' <param name="DenArt">Denominación del artículo</param>
        ''' <remarks>Llamada desde visorOfertas.aspx.vb; Máx inferior a 1seg.</remarks>
        Public Sub LoadData(ByVal lCiaComp As Long, ByVal lCiaProve As Long, ByVal FecOfeDesde As Date, ByVal FecOfeHasta As Date, ByVal Anio As Integer, ByVal Gmn1 As String, ByVal Proce As Integer,
                            ByVal DenProce As String, ByVal CodArt As String, ByVal DenArt As String, ByVal EmpresaPortal As Boolean, ByVal NomPortal As String)
            Authenticate()
            If mRemottingServer Then
                moOfertasHistoricas = DBServer.OfertasHistoricas_Get(lCiaComp, lCiaProve, FecOfeDesde, FecOfeHasta, Anio, Gmn1, Proce, DenProce, CodArt, DenArt, EmpresaPortal, NomPortal, msSesionId, msIPDir, msPersistID)
            Else
                moOfertasHistoricas = DBServer.OfertasHistoricas_Get(lCiaComp, lCiaProve, FecOfeDesde, FecOfeHasta, Anio, Gmn1, Proce, DenProce, CodArt, DenArt, EmpresaPortal, NomPortal)
            End If

            If moOfertasHistoricas IsNot Nothing Then
                moOfertasHistoricas.Tables(0).TableName = "OFERTASHISTORICAS"
            End If
        End Sub

        Public Sub LoadAnyos(ByVal lCiaComp As Long, ByVal lCiaProve As Long)
            Authenticate()
            If mRemottingServer Then
                moOfertasHistoricasAnyos = DBServer.OfertasHistoricasAnyos_Get(lCiaComp, lCiaProve, msSesionId, msIPDir, msPersistID)
            Else
                moOfertasHistoricasAnyos = DBServer.OfertasHistoricasAnyos_Get(lCiaComp, lCiaProve)
            End If

            If moOfertasHistoricasAnyos IsNot Nothing Then
                moOfertasHistoricasAnyos.Tables(0).TableName = "OFERTASHISTORICAS_ANYOS"
            End If
        End Sub

        Public Sub LoadGmn1s(ByVal lCiaComp As Long, ByVal lCiaProve As Long)
            Authenticate()
            If mRemottingServer Then
                moOfertasHistoricasGmn1s = DBServer.OfertasHistoricasGmn1s_Get(lCiaComp, lCiaProve, msSesionId, msIPDir, msPersistID)
            Else
                moOfertasHistoricasGmn1s = DBServer.OfertasHistoricasGmn1s_Get(lCiaComp, lCiaProve)
            End If

            If moOfertasHistoricasGmn1s IsNot Nothing Then
                moOfertasHistoricasGmn1s.Tables(0).TableName = "OFERTASHISTORICAS_GMN1S"
            End If
        End Sub

        ''' Revisado por: blp. Fecha: 17/01/2013
        ''' <summary>
        ''' Método para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">código de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petición</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde la creación de instancias. Máx. 0,1 seg.</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub

    End Class
End Namespace