Namespace Fullstep.PMPortalServer

    <Serializable()> _
Public Class Destino
        Inherits Fullstep.PMPortalServer.Security

        Private msCod As String
        Private msDen As String
        Private msDir As String
        Private msCP As String
        Private msPoblacion As String
        Private msPais As String
        Private msProvincia As String
        Private msTfno As String
        Private msFAX As String
        Private msEmail As String

        Public Property Cod() As String
            Get
                Return msCod
            End Get

            Set(ByVal Value As String)
                msCod = Value
            End Set
        End Property

        Public Property Den() As String
            Get
                Return msDen
            End Get

            Set(ByVal Value As String)
                msDen = Value
            End Set
        End Property

        Public Property Dir() As String
            Get
                Return msDir
            End Get

            Set(ByVal Value As String)
                msDir = Value
            End Set
        End Property


        Public Property CP() As String
            Get
                Return msCP
            End Get

            Set(ByVal Value As String)
                msCP = Value
            End Set
        End Property

        Public Property Poblacion() As String
            Get
                Return msPoblacion
            End Get

            Set(ByVal Value As String)
                msPoblacion = Value
            End Set
        End Property

        Public Property Pais() As String
            Get
                Return msPais
            End Get

            Set(ByVal Value As String)
                msPais = Value
            End Set
        End Property

        Public Property Provincia() As String
            Get
                Return msProvincia
            End Get

            Set(ByVal Value As String)
                msProvincia = Value
            End Set
        End Property
        Public Property Tfno() As String
            Get
                Tfno = msTfno
            End Get
            Set(ByVal Value As String)
                msTfno = Value
            End Set
        End Property

        Public Property FAX() As String
            Get
                FAX = msFAX
            End Get
            Set(ByVal Value As String)
                msFAX = Value
            End Set
        End Property

        Public Property Email() As String
            Get
                Email = msEmail
            End Get
            Set(ByVal Value As String)
                msEmail = Value
            End Set
        End Property
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace