Namespace Fullstep.PMPortalServer
    <Serializable()>
    Public Class Idiomas
        Inherits Security

        Private moIdiomas As Collection
        Public ReadOnly Property Idiomas() As Collection
            Get
                Return moIdiomas
            End Get

        End Property
        ''' <summary>
        ''' Carga los idiomas
        ''' </summary>
        ''' <remarks>Llamada desde: Campo.vb   Formulario.vb  Instancia.vb  Solicitud.vb; Tiempo m�ximo: 0,2</remarks>
        Friend Sub Load()
            Dim data As DataSet
            Dim i As Integer
            If mRemottingServer Then
                data = DBServer.Idiomas_Load(msSesionId, msIPDir, msPersistID)
            Else
                data = DBServer.Idiomas_Load()
            End If

            If Not data.Tables(0).Rows.Count = 0 Then
                For i = 0 To data.Tables(0).Rows.Count - 1
                    Me.Add(data.Tables(0).Rows(i).Item("COD").ToString(), data.Tables(0).Rows(i).Item("DEN").ToString())
                Next
            End If
            data = Nothing
        End Sub
        ''' <summary>
        ''' Carga en la coleccion de la clase el idioma q se le proporciona
        ''' </summary>
        ''' <param name="sCod">Codigo de idioma</param>
        ''' <param name="sDen">Denominacion de idioma</param>
        ''' <remarks>Llamada desde: Load; Tiempo maximo: 0</remarks>
        Friend Sub Add(ByVal sCod As String, ByVal sDen As String)
            Dim oIdi As New Idioma(DBServer, mRemottingServer, msUserCode, msSesionId, msIPDir, msPersistID, mIsAuthenticated)
            If moIdiomas Is Nothing Then
                moIdiomas = New Collection
            End If
            oIdi.Cod = sCod
            oIdi.Den = sDen

            moIdiomas.Add(oIdi, sCod)
        End Sub
        ''' <summary>
        ''' M�todo para generar una instancia de la clase
        ''' </summary>
        ''' <param name="dbserver">servidor</param>
        ''' <param name="remotting">remotting</param>
        ''' <param name="UserCode">c�digo de usuario</param>
        ''' <param name="SesionId">Id de sesion</param>
        ''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
        ''' <param name="PersistID">Valor de la cookie persistente</param>
        ''' <param name="isAuthenticated">Autenticado</param>
        ''' <remarks>Llamada desde: Pantallas q deseen tener un objeto de esta clase ; Tiempo m�ximo: 0</remarks>
        Friend Sub New(ByRef dbserver As Fullstep.PMPortalDatabaseServer.Root, ByVal remotting As Boolean, ByVal UserCode As String, ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal isAuthenticated As Boolean)
            MyBase.New(dbserver, remotting, UserCode, SesionId, IPDir, PersistID, isAuthenticated)
        End Sub
    End Class
End Namespace