﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Errores.aspx.vb" Inherits="Fullstep.PMPortalWeb.Errores" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <br />
        <br />
        <asp:Label CssClass="TituloErrorUsuario" ID="lblTitulo" runat="server" Width="80%"></asp:Label>
        <hr color="lightgrey" />
        <br />
        <ul class="TituloErrorUsuario" style="list-style-position: outside; list-style-type: square; position: static">
            <li>
                <asp:Label CssClass="SubTituloErrorUsuario" ID="lblSubTitulo1" runat="server" Font-Size="10"></asp:Label>
                <br />
                <br />
                <a href="http://www.fullstep.com/FSGE" class="SubTituloErrorUsuario" style="font-size: 10pt">
                    <b>
                        <asp:Literal runat="server" ID="lEnlaceFSGE"></asp:Literal></b></a><br />
                <br />
            </li>
            <li>
                <asp:Label CssClass="SubTituloErrorUsuario" ID="lblSubTitulo2" runat="server" Font-Size="10"></asp:Label>
                <br />
                <br />
                <table cellpadding="0">
                    <tr>
                        <td width="306" height="79">
                            <img src="<%=ConfigurationManager.AppSettings("ruta")%>images/helpdesk.jpg" />
                        </td>
                        <td>
                            <table class="SubTituloErrorUsuario" bgcolor="whitesmoke">
                                <tr>
                                    <td>
                                        <asp:Literal runat="server" ID="lPorTelefono"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Literal runat="server" ID="lViaEmail"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Literal runat="server" ID="lHorario1"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <br />

                <table bgcolor="gray" cellspacing="1" border="0" height="70">
                    <tr>
                        <td bgcolor="whitesmoke">
                            <table border="0" cellpadding="7" cellspacing="4">
                                <tr>
                                    <td>
                                        <asp:Label CssClass="SubTituloErrorUsuario" ID="lblIdentificadorError" runat="server" Font-Size="12"
                                            Font-Bold="True"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label CssClass="TituloSinDatos" ID="lblIdError" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <table id="tbVolver" cellpadding="0" cellspacing="4" runat="server">
                    <tr>
                        <td valign="middle" width="20">
                            <img src="<%=ConfigurationManager.AppSettings("ruta")%>images/volver_bullet_cuad_b.gif" border="0" /></td>
                        <td width="55">
                            <asp:HyperLink CssClass="SubTituloErrorUsuario" Font-Size="10" Font-Bold="True" runat="server" ID="hlInicio"></asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </li>
        </ul>
    </form>
</body>
</html>
