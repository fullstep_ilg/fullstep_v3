Partial Class disconnect
    Inherits FSPMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' Revisado por:blp Fecha: 12/12/2012
    ''' <summary>
    ''' Carga de la p�gina: Se eliminan todas las variables de sesi�n de .NET
    ''' </summary>
    ''' <param name="sender">P�gina</param>
    ''' <param name="e">argumentos del evento</param>
    ''' <remarks>Llamada desde el evento. M�x 0,1 seg.</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Session.RemoveAll()
        Response.Redirect(ConfigurationManager.AppSettings("RUTANORMAL") & "../../script/disconnect/disconnect.asp?disconnect2=true&Qry=" & Request("Qry") & "&Idioma=" & Request("Idioma"))
    End Sub

End Class
