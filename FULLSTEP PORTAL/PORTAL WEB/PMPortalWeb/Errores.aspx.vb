﻿Namespace Fullstep.PMPortalWeb
    Public Class Errores
        Inherits FSPMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not FSPMServer Is Nothing Then
                Dim oError As Fullstep.PMPortalServer.Errores = FSPMServer.Get_Errores()
                Dim sPagina As String = Request("aspxerrorpath")
                Dim sUsuario As String = FSPMUser.Cod
                Dim sv_Http_User_Agent As String = Request.ServerVariables("HTTP_USER_AGENT")

                If sPagina = Nothing Then
                    sPagina = Request.Path
                End If
                Dim tmpExcepcion As Exception
                tmpExcepcion = Server.GetLastError

                If Not tmpExcepcion Is Nothing Then
                    Dim sSv_Query_String As String = tmpExcepcion.Data("QueryString")
                    tmpExcepcion = tmpExcepcion.GetBaseException
                    oError.Create(FSPMUser.CiaComp, sPagina, sUsuario, tmpExcepcion.GetType().FullName, tmpExcepcion.Message, tmpExcepcion.StackTrace, sSv_Query_String, sv_Http_User_Agent)
                Else
                    oError.Create(FSPMUser.CiaComp, sPagina, sUsuario, String.Empty, String.Empty, String.Empty, String.Empty, sv_Http_User_Agent)
                End If

                Dim sIdi As String = FSPMUser.Idioma
                If sIdi = Nothing Then sIdi = ConfigurationManager.AppSettings("idioma")

                Dim oDict As Fullstep.PMPortalServer.Dictionary = FSPMServer.Get_Dictionary()
                oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.PaginaError, FSPMUser.Idioma)

                Dim oTextos As DataTable
                oTextos = oDict.Data.Tables(0)

                lblTitulo.Text = oTextos.Rows(0).Item(1)
                lblSubTitulo1.Text = oTextos.Rows(1).Item(1)
                lEnlaceFSGE.Text = oTextos.Rows(2).Item(1)
                lblSubTitulo2.Text = oTextos.Rows(3).Item(1)
                lPorTelefono.Text = oTextos.Rows(4).Item(1)
                lViaEmail.Text = oTextos.Rows(5).Item(1)
                lHorario1.Text = oTextos.Rows(6).Item(1)
                lblIdentificadorError.Text = oTextos.Rows(7).Item(1)
                lblIdError.Text = oError.ID

                tbVolver.Visible = False
            Else
				Dim sRuta As String = ConfigurationManager.AppSettings("rutanormal")
				sRuta = Replace(sRuta, "PMPortal", "login")
				Response.Redirect(sRuta & ConfigurationManager.AppSettings("custom") & "/login.asp", False)
			End If
        End Sub
    End Class
End Namespace