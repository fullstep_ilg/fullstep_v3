﻿Imports ExpertPdf.HtmlToPdf
Imports System.IO
Imports SBPAdES
Imports SBPDF
Imports SBPDFSecurity
Imports SBCustomCertStorage
Imports SBX509
Imports SBCRLStorage

Imports SBPDFCore
Imports SBCRL
Imports SBOCSPClient
Imports SBWinCertStorage

Imports System.Diagnostics
Imports SBDC

Namespace Fullstep.PMPortalWeb
    Public Class OfertasPDF_ActiveX
        Inherits OfertasPDF

        Public Sub New()
            'SBUtils.Unit.SetLicenseKey("AF479A648A42969644F109C690E12B1402F11DBD9EB213B43821FD62B787AE111989D1C38A5E2278F9D19F3D1D6AD85D87B7DA6DBAEDC72150960800413FB48E6067B17B03A5AB32A4417F35B4A17DA29FF2C9512DBC2D7AAE5CE117889C2FDC64CB65C6F6A9F1891D0CEEE134994DFF0DC19B95ABFDC55161B144E9482299618BE29FA9C8EFB89EB666049899C11907610B664CDAA723A1E18820A18A671B68C88C661854CC1B4DC48BA8806ED30AF02DAB7B25A63DE63258CE2F616F93D040DA6BC54212072542DBD41F7A343485A23C9AEF476404980F00B0125997FC7A4869186411F543FB4ED74A897E46B75351983715EEF95E6E443B25D156D010A57A")
            SBUtils.Unit.SetLicenseKey("D87ED795C5A7644874C8FB5A6511DCD2E749149FBF075DDBA61EB8D5ABDDEE5A6E038CAFF7B58DB6DE7381FFCA43B3FAEFC85556CF183FE2C04E9F99298BE64B9919355F733B68027B1D74BF6B3F31766AC0625A5F35EEF8A769E09F6D8104A88F9F903AE94AA6D5B0533A8E3D3EB5D38CF0CE1B32A3C30FE6F920D0008827D0D044A482CD3301016067FB7C7673C3E19BF6C154FE1441516BAA080E8AEF03CA6688512CE01456DE00A8C1E73A283E4C1AFBC60202CAD4338A92D67DF70EA19FB05A4325104F8C5756DB2FE8D85D4642E93D6CDC328EA8C2A1229491E7C152651B2C92E29B48681A2494415DBB6A12F53F01F6EB2661CE26FB8CA8FD15E3EDCA") 'LICENCIA PARA: PDF & DC
            SBUtils.Unit.SetLicenseKey("4DB89E15AA2DC9085C31E6C8303C56121CFD151D9FF51C33B5D33A8D1C5F0C4263612478226CA8F1CC81215324826699CED43A570EEACB5FBA63F1CDF2959C0F28B1CA4FE8A18F228D051815985EDEB1359027EC50476A03875EA9EF1FE77D3EDD2BACA26DB1522C825CDCC1B60C86AFE66B96E10051050555466D35A61199E4BC1F0B49D927BD7C3D7584C1C8D79C6191874F094E51D8E3C123C3C88098EE23D142FF7430A68DB832593ED64CA8F439966CEA5DBB6605156A5F077AA5AEC95778915674EBF3364712AEEA8AE4212ACCB0074CF143BFB85A3B33A5FB4B8B4AF80D1DC4537C20C1F5A859DCB62080654D26E2FB3ACAD48A6C9DA8538E23F95F73") 'LICENCIA PARA: PKI
            SBPDF.Unit.Initialize()
            SBPAdES.Unit.Initialize()
            SBPDFSecurity.Unit.Initialize()
        End Sub

        ''' Revisado por: auv. Fecha:17/01/2013
        ''' <summary>
        ''' Carga de la pagina
        ''' </summary>
        ''' <param name="sender">Control que lanza el evento de carga</param>
        ''' <param name="e">argumentos del evento</param>
        ''' <remarks>Llamada desde el evento Load de la página. Máx inferior a 1 seg.</remarks>
        Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not (iEstado = FIN_KO) Then
                If Request.Url.Query().StartsWith("?&") Or ((Not IsNothing(HttpUtility.ParseQueryString(Request.Url.Query())("result"))) AndAlso CStr(HttpUtility.ParseQueryString(Request.Url.Query())("result")) = "1") Then
                    If (IsPostBack) Then
                        Response.Write("<script>window.close()</script>")
                        Return
                    End If

                    Dim sessionId As String = Request.QueryString("sessionid")
                    Dim fileName As String = (If((sessionId Is Nothing), [String].Empty, DirectCast(Cache(sessionId), [String])))

                    sRutaHtml = Path.ChangeExtension(fileName, ".html")
                    sRutaPdf = Path.ChangeExtension(fileName, ".pdf")
                    Dim sResult As String = HttpUtility.ParseQueryString(Request.Url.Query())("result")
                    If sResult = 1 Then
                        Dim a As String = ""
                    End If
                    If Request.Url.Query().StartsWith("?&") Then
                        Dim b As String = ""
                    End If
                    'If File.Exists(sRutaHtml) Or File.Exists(sRutaPdf) Then
                    Try
                        If (Request.HttpMethod.Equals("POST", StringComparison.OrdinalIgnoreCase)) Then
                            If (sessionId Is Nothing) Then
                                Response.StatusCode = 400
                                Response.StatusDescription = "No session id specified"
                                Me.finalizar(FIN_KO)
                            ElseIf (Not Request.ContentType.Equals("application/xml", StringComparison.OrdinalIgnoreCase)) Then
                                Response.StatusCode = 415
                                Response.StatusDescription = "Only XML documents are accepted"
                                Me.finalizar(FIN_KO)
                            ElseIf (Request.ContentLength = 0) Then
                                Response.StatusCode = 400
                                Response.StatusDescription = "No content"
                                Me.finalizar(FIN_KO)
                            Else
                                'Dim signature As Byte() = New Byte(Request.InputStream.Length - 1) {}
                                'Request.InputStream.Read(signature, 0, signature.Length)
                                Dim signature As Byte() = New Byte(Request.ContentLength - 1) {}
                                Dim received As Integer = 0
                                While received < signature.Length
                                    received += Request.InputStream.Read(signature, received, signature.Length - received)
                                End While
                                Try
                                    FinishSigning(fileName, signature)
                                    Response.StatusCode = 200
                                    Cache(sessionId) = fileName
                                    Me.finalizar(FIN_OK)
                                Catch err As Exception
                                    Response.StatusCode = 500
                                    Response.StatusDescription = err.Message
                                    Throw New Exception()
                                End Try
                            End If
                        Else
                            If (String.IsNullOrEmpty(fileName)) Then
                                'labelResult.Text = "Empty session"
                                Me.finalizar(FIN_KO)
                            Else
                                Cache.Remove(sessionId)
                                Dim data As String = Request.QueryString("data")
                                If data IsNot Nothing Then
                                    FinishSigning(fileName, Convert.FromBase64String(data))
                                    Me.finalizar(FIN_OK)
                                Else
                                    Me.finalizar(FIN_KO)
                                End If
                                'labelResult.Text = String.Format("The signed document has been saved as<br />{0}", fileName)
                            End If
                        End If
                    Catch ex As Exception
                        Me.finalizar(FIN_KO)
                    End Try
                    'End If
                Else
                    Dim oOfertas As PMPortalServer.Ofertas

                    If Not IsPostBack Then
                        sRutaHtml = HttpUtility.ParseQueryString(Request.Url.Query())("rutaHtml")
                        sRutaPdf = HttpUtility.ParseQueryString(Request.Url.Query())("rutaPdf")
                        Try
                            If sRutaHtml = "" Or sRutaPdf = "" Then
                                Throw New Exception()
                            End If
                            If (Not File.Exists(sRutaHtml)) Or (Not File.Exists(sRutaPdf)) Then
                                Throw New Exception()
                            End If
                            '        CargarTextos()
                            '        Me.strHTML.Value = sRutaHtml
                            '        Me.strPDF.Value = sRutaPdf
                            'Me.crearPDF()
                            oOfertas = FSPMServer.Get_Ofertas
                            oOfertas.LoadDataProve(IdCia)
                            sNifProve = oOfertas.ProveNif
                            '        Me.strNifProve.Value = sNifProve
                            iTipoFirma = Me.obtCasoDeFirmado()
                            '        Me.tipoFirma.Value = CStr(iTipoFirma)
                            Select Case iTipoFirma
                                Case SIN_FIRMA
                                    Me.controlActiveX.Style.Add("display", "none")
                                    Me.finalizar(FIN_OK)
                                Case FIRMA_BASIC, FIRMA_ENHACED_SELLO, FIRMA_ENHACED_SELLO_OCSP
                                    'SBUtils.Unit.SetLicenseKey("AF479A648A42969644F109C690E12B1402F11DBD9EB213B43821FD62B787AE111989D1C38A5E2278F9D19F3D1D6AD85D87B7DA6DBAEDC72150960800413FB48E6067B17B03A5AB32A4417F35B4A17DA29FF2C9512DBC2D7AAE5CE117889C2FDC64CB65C6F6A9F1891D0CEEE134994DFF0DC19B95ABFDC55161B144E9482299618BE29FA9C8EFB89EB666049899C11907610B664CDAA723A1E18820A18A671B68C88C661854CC1B4DC48BA8806ED30AF02DAB7B25A63DE63258CE2F616F93D040DA6BC54212072542DBD41F7A343485A23C9AEF476404980F00B0125997FC7A4869186411F543FB4ED74A897E46B75351983715EEF95E6E443B25D156D010A57A")
                                    SBUtils.Unit.SetLicenseKey("D87ED795C5A7644874C8FB5A6511DCD2E749149FBF075DDBA61EB8D5ABDDEE5A6E038CAFF7B58DB6DE7381FFCA43B3FAEFC85556CF183FE2C04E9F99298BE64B9919355F733B68027B1D74BF6B3F31766AC0625A5F35EEF8A769E09F6D8104A88F9F903AE94AA6D5B0533A8E3D3EB5D38CF0CE1B32A3C30FE6F920D0008827D0D044A482CD3301016067FB7C7673C3E19BF6C154FE1441516BAA080E8AEF03CA6688512CE01456DE00A8C1E73A283E4C1AFBC60202CAD4338A92D67DF70EA19FB05A4325104F8C5756DB2FE8D85D4642E93D6CDC328EA8C2A1229491E7C152651B2C92E29B48681A2494415DBB6A12F53F01F6EB2661CE26FB8CA8FD15E3EDCA") 'LICENCIA PARA: PDF & DC
                                    SBUtils.Unit.SetLicenseKey("4DB89E15AA2DC9085C31E6C8303C56121CFD151D9FF51C33B5D33A8D1C5F0C4263612478226CA8F1CC81215324826699CED43A570EEACB5FBA63F1CDF2959C0F28B1CA4FE8A18F228D051815985EDEB1359027EC50476A03875EA9EF1FE77D3EDD2BACA26DB1522C825CDCC1B60C86AFE66B96E10051050555466D35A61199E4BC1F0B49D927BD7C3D7584C1C8D79C6191874F094E51D8E3C123C3C88098EE23D142FF7430A68DB832593ED64CA8F439966CEA5DBB6605156A5F077AA5AEC95778915674EBF3364712AEEA8AE4212ACCB0074CF143BFB85A3B33A5FB4B8B4AF80D1DC4537C20C1F5A859DCB62080654D26E2FB3ACAD48A6C9DA8538E23F95F73") 'LICENCIA PARA: PKI
                                    'Dim sExtDoc As String = "pdf"
                                    'Dim iNum As Integer = 0
                                    'Dim sSplitPdf As String() = Nothing
                                    'Dim n As String = Nothing

                                    'Dim sPath = "C:/WINDOWS/Temp/"

                                    'sSplitPdf = sRutaPdf.Split("\")
                                    'n = sSplitPdf(sSplitPdf.Length - 1)
                                    'sSplitPdf = n.Split(".")
                                    'n = sSplitPdf(0)

                                    Dim signedFile As String = Path.ChangeExtension(sRutaPdf, ".tmp") 'Dim signedFile As String = sPath & n & ".tmp"

                                    Cache(Session.SessionID) = signedFile
                                    File.Copy(sRutaPdf, signedFile, True) 'File.Copy(sPath & n & "." & sExtDoc, signedFile, True)

                                    Dim state As TElDCAsyncState
                                    Dim doc As New TElPDFDocument()
                                    'Dim handler As New TElPDFPublicKeySecurityHandler()
                                    Dim handler As New TElPDFAdvancedPublicKeySecurityHandler()
                                    Dim signature As TElPDFSignature
                                    Dim input As System.IO.FileStream = New System.IO.FileStream(signedFile, FileMode.Open, FileAccess.ReadWrite)

                                    Try
                                        doc.Open(input)
                                        handler.SignatureType = TSBPDFPublicKeySignatureType.pstPKCS7SHA1

                                        handler.HashAlgorithm = SBConstants.__Global.SB_ALGORITHM_DGST_SHA1
                                        handler.CustomName = "Adobe.PPKMS"
                                        handler.PAdESSignatureType = TSBPAdESSignatureType.pastBasic
                                        handler.AutoCollectRevocationInfo = False
                                        handler.IgnoreChainValidationErrors = False
                                        handler.SignatureSizeEstimationStrategy = TSBPAdESSignatureSizeEstimationStrategy.psesBasic

                                        signature = doc.Signatures(doc.AddSignature())
                                        signature.SignatureType = SBPDF.__Global.stDocument
                                        signature.Invisible = True
                                        signature.Handler = handler
                                        signature.SigningTime = DateTime.UtcNow

                                        signature.ExtraSpace = 1000

                                        state = doc.InitiateAsyncOperation()

                                    Catch ex As Exception
                                        Throw New Exception
                                    Finally
                                        input.Close()
                                    End Try

                                    output = New MemoryStream()
                                    state.SaveToStream(output, SBDCXMLEnc.__Global.DCXMLEncoding())

                                    paramDataActiveX.Attributes.Add("value", Convert.ToBase64String(output.ToArray()))
                                    paramIdActiveX.Attributes.Add("value", Session.SessionID)
                                Case Else
                                    Me.controlActiveX.Style.Add("display", "none")
                                    Me.finalizar(FIN_OK)
                            End Select

                        Catch ex As Exception
                            Me.finalizar(FIN_KO)
                        End Try
                    Else
                    End If
                End If
                '-----------------------------------------------------------------------------------------------------------------------
                'Dim iContCert As Integer
                'Dim certAct As TElX509Certificate
                'Dim row As System.Web.UI.WebControls.TableRow
                'Dim cell As System.Web.UI.WebControls.TableCell
                'Dim label As System.Web.UI.WebControls.Label
                'Dim oOfertas As PMPortalServer.Ofertas

                'If Not IsPostBack Then
                '    sRutaHtml = HttpUtility.ParseQueryString(Request.Url.Query())("rutaHtml")
                '    sRutaPdf = HttpUtility.ParseQueryString(Request.Url.Query())("rutaPdf")
                '    Try
                '        If sRutaHtml = "" Or sRutaPdf = "" Then
                '            Throw New Exception()
                '        End If
                '        If (Not File.Exists(sRutaHtml)) Or (Not File.Exists(sRutaPdf)) Then
                '            Throw New Exception()
                '        End If
                '        CargarTextos()
                '        Me.strHTML.Value = sRutaHtml
                '        Me.strPDF.Value = sRutaPdf
                '        Me.crearPDF()
                '        oOfertas = FSPMServer.Get_Ofertas
                '        oOfertas.LoadDataProve(IdCia)
                '        sNifProve = oOfertas.ProveNif
                '        Me.strNifProve.Value = sNifProve
                '        iTipoFirma = Me.obtCasoDeFirmado()
                '        Me.tipoFirma.Value = CStr(iTipoFirma)
                '        Select Case iTipoFirma
                '            Case SIN_FIRMA
                '                Me.formOfertasPDF.Style.Add("display", "none")
                '                Me.finalizar(FIN_OK)
                '            Case FIRMA_BASIC, FIRMA_ENHACED_SELLO, FIRMA_ENHACED_SELLO_OCSP
                '                oCertificadosRoot = New TElWinCertStorage
                '                oCertificadosRoot.StorageType = TSBStorageType.stSystem
                '                oCertificadosRoot.AccessType = TSBStorageAccessType.atLocalMachine
                '                oCertificadosRoot.ReadOnly = True
                '                oCertificadosRoot.SystemStores.BeginUpdate()
                '                Try
                '                    oCertificadosRoot.SystemStores.Add("ROOT")
                '                Catch ex As Exception
                '                    Throw New Exception()
                '                Finally
                '                    oCertificadosRoot.SystemStores.EndUpdate()
                '                End Try
                '                iContCert = 0
                '                While iContCert < oCertificadosRoot.Count
                '                    certAct = oCertificadosRoot.Certificates(iContCert)
                '                    If InStr(certAct.SubjectName.CommonName, "NIF " & sNifProve) > 0 Then
                '                        row = New TableRow
                '                        row.Attributes.Add("onclick", "rowClick(" & CStr(iContCert) & ")")
                '                        cell = New TableCell
                '                        cell.CssClass = "bordeado"
                '                        label = New Label
                '                        If Len(certAct.SubjectName.CommonName) > 43 Then
                '                            label.Text = Left(certAct.SubjectName.CommonName, 40) & "..."
                '                        Else
                '                            label.Text = certAct.SubjectName.CommonName
                '                        End If
                '                        label.ToolTip = certAct.SubjectName.CommonName
                '                        cell.Controls.Add(label)
                '                        row.Cells.Add(cell)
                '                        cell = New TableCell
                '                        cell.CssClass = "bordeado"
                '                        label = New Label
                '                        If Len(certAct.IssuerName.CommonName) > 43 Then
                '                            label.Text = Left(certAct.IssuerName.CommonName, 40) & "..."
                '                        Else
                '                            label.Text = certAct.IssuerName.CommonName
                '                        End If
                '                        label.ToolTip = certAct.IssuerName.CommonName
                '                        cell.Controls.Add(label)
                '                        row.Cells.Add(cell)
                '                        cell = New TableCell
                '                        cell.CssClass = "bordeado"
                '                        label = New Label
                '                        label.Text = CStr(certAct.ValidTo())
                '                        label.ToolTip = CStr(certAct.ValidTo())
                '                        cell.Controls.Add(label)
                '                        row.Cells.Add(cell)
                '                        tblCerts.Rows.Add(row)
                '                        If Me.fila.Value > -1 And Me.fila.Value = iContCert Then
                '                            row.CssClass = "highlight"
                '                        End If
                '                    End If
                '                    iContCert = iContCert + 1
                '                    'PARA PRUEBAS sUrlSello = "http://ocsp.izenpe.com:8093"
                '                End While
                '            Case Else
                '                Me.formOfertasPDF.Style.Add("display", "none")
                '                Me.finalizar(FIN_OK)
                '        End Select
                '    Catch ex As Exception
                '        Me.finalizar(FIN_KO)
                '    End Try
                'Else
                '    sRutaHtml = Me.strHTML.Value
                '    sRutaPdf = Me.strPDF.Value
                '    Try
                '        sNifProve = Me.strNifProve.Value
                '        iTipoFirma = CInt(Me.tipoFirma.Value)
                '        iFilaElegida = CInt(Me.fila.Value)

                '        Me.btnContinuar.Style.Add("display", "none")
                '        oCertificadosRoot = New TElWinCertStorage
                '        oCertificadosRoot.StorageType = TSBStorageType.stSystem
                '        oCertificadosRoot.AccessType = TSBStorageAccessType.atLocalMachine
                '        oCertificadosRoot.ReadOnly = True
                '        oCertificadosRoot.SystemStores.BeginUpdate()
                '        Try
                '            oCertificadosRoot.SystemStores.Add("ROOT")
                '        Catch ex As Exception
                '            Throw New Exception()
                '        Finally
                '            oCertificadosRoot.SystemStores.EndUpdate()
                '        End Try
                '        iContCert = 0
                '        While iContCert < oCertificadosRoot.Count
                '            certAct = oCertificadosRoot.Certificates(iContCert)
                '            If InStr(certAct.SubjectName.CommonName, "NIF " & sNifProve) > 0 Then 'DEBE CONECTARSE AL FINALIZAR LA TAREA
                '                row = New TableRow
                '                cell = New TableCell
                '                cell.CssClass = "bordeado"
                '                label = New Label
                '                If Len(certAct.SubjectName.CommonName) > 43 Then
                '                    label.Text = Left(certAct.SubjectName.CommonName, 40) & "..."
                '                Else
                '                    label.Text = certAct.SubjectName.CommonName
                '                End If
                '                label.ToolTip = certAct.SubjectName.CommonName
                '                cell.Controls.Add(label)
                '                row.Cells.Add(cell)
                '                cell = New TableCell
                '                cell.CssClass = "bordeado"
                '                label = New Label
                '                If Len(certAct.IssuerName.CommonName) > 43 Then
                '                    label.Text = Left(certAct.IssuerName.CommonName, 40) & "..."
                '                Else
                '                    label.Text = certAct.IssuerName.CommonName
                '                End If
                '                label.ToolTip = certAct.IssuerName.CommonName
                '                cell.Controls.Add(label)
                '                row.Cells.Add(cell)
                '                cell = New TableCell
                '                cell.CssClass = "bordeado"
                '                label = New Label
                '                label.Text = CStr(certAct.ValidTo())
                '                label.ToolTip = CStr(certAct.ValidTo())
                '                cell.Controls.Add(label)
                '                row.Cells.Add(cell)
                '                tblCerts.Rows.Add(row)
                '                If Me.fila.Value > -1 And Me.fila.Value = iContCert Then
                '                    row.CssClass = "highlight"
                '                End If
                '            End If 'DEBE CONECTARSE AL FINALIZAR LA TAREA
                '            iContCert = iContCert + 1
                '        End While
                '        oCertificadoFirma = New TElX509Certificate
                '        oCertificadosRoot.Certificates(iFilaElegida).Clone(oCertificadoFirma, True)

                '        Select Case iTipoFirma
                '            Case SIN_FIRMA
                '            Case FIRMA_BASIC
                '                Me.firmaDigital()
                '            Case FIRMA_ENHACED_SELLO
                '                Me.firmaDigital()
                '            Case FIRMA_ENHACED_SELLO_OCSP
                '                Me.firmaDigital()
                '        End Select
                '        Me.finalizar(FIN_OK)
                '    Catch ex As Exception
                '        Me.finalizar(FIN_KO)
                '    End Try
                'End If
            End If
        End Sub

    End Class
End Namespace