﻿
Imports Fullstep.PMPortalServer
Imports Fullstep.PMPortalServer.TiposDeDatos
Imports System.Linq
Imports System.Text
Imports System.Collections.Generic
Imports Infragistics.Web.UI.GridControls
Imports System.Collections.ObjectModel
Imports System.Web.Script.Serialization
Imports System.IO

Namespace Fullstep.PMPortalWeb
    Partial Class visorOfertas
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

#End Region

        Private _PaginadorTop As Paginador

        ''' Revisado por:auv. Fecha:17/01/2013
        ''' <summary>
        ''' PreCargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init

            setCultureInfoUsuario()

            Me.ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.OfertasHistoricas

            InitializeComponent()

            Me.FindControl("Form1").Controls.Add(Fullstep.PMPortalWeb.CommonInstancia.InsertarCalendario(FSPMUser.Idioma.ToString))

            If Not Me.IsPostBack Then
                'Vaciamos la caché porque es una carga nueva. De otro modo, sería necesario controlar los filtros para saber si la carga ha cambiado
                BorrarCacheOfertasHistoricas()
                BorrarCacheOfertaHistoricaAdjun()
                BorrarCacheOfertasHistoricasAnyos()
                BorrarCacheOfertasHistoricasGmn1s()
                InicializarValoresControles()
            End If

            Me._PaginadorTop = TryCast(Me.whdgOfertasHistoricas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("wdgPaginador"), Paginador)
            AddHandler Me._PaginadorTop.PageChanged, AddressOf _Paginador_PageChanged

            Me._PaginadorTop.MostrarBotonExcel = True
        End Sub

        ''' Revisado por: auv. Fecha:17/01/2013
        ''' <summary>
        ''' Carga de la pagina
        ''' </summary>
        ''' <param name="sender">Control que lanza el evento de carga</param>
        ''' <param name="e">argumentos del evento</param>
        ''' <remarks>Llamada desde el evento Load de la página. Máx inferior a 1 seg.</remarks>
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Dim oParametrosMostrarPDF As Object

            If Not IsPostBack Then 'Carga Inicial
                CargarTextos()
                CargarwhdgOfertasHistoricas(False)
            Else
                Select Case Request("__EVENTTARGET")
                    Case btnBuscar.ClientID
                    Case btnLimpiar.ClientID
                    Case "whdgOfertasHistoricas_ctl00"
                        Select Case Request("__EVENTARGUMENT")
                            Case "ColumnSorting" 'FiltradoSortBy
                                CargarwhdgOfertasHistoricas(False)
                            Case "undefined" 'FiltradoCondicion
                                CargarwhdgOfertasHistoricas(False)
                        End Select
                    Case "whdgOfertasHistoricas"
                        Select Case Request("__EVENTARGUMENT")
                            Case "undefined" 'FiltradoGroupBy
                                CargarwhdgOfertasHistoricas(False)
                        End Select
                    Case "whdgOfertasHistoricas$ctl00$ctl01$wdgPaginador$btnNextPage", "whdgOfertasHistoricas$ctl00$ctl01$wdgPaginador$btnLastPage", "whdgOfertasHistoricas$ctl00$ctl01$wdgPaginador$btnFirstPage", "whdgOfertasHistoricas$ctl00$ctl01$wdgPaginador$btnPreviousPage", "whdgOfertasHistoricas$ctl00$ctl01$wdgPaginador$ddlPage"
                        CargarwhdgOfertasHistoricas(False)
                    Case ""
                        Select Case Request("__EVENTARGUMENT")
                            Case "Excel"
                                Exportar()
                                Exit Sub
                        End Select
                    Case "PDF"
                        'Dim serializer As New JavaScriptSerializer 'Estas tres lineas quedan comentadas mientras no se consigan pasar los parámetros mediante JSON
                        'oParametrosMostrarPDF = serializer.Deserialize(Of Dictionary(Of String, String))(Request("__EVENTARGUMENT"))
                        'Me.MostrarPDF(oParametrosMostrarPDF("codigo"), oParametrosMostrarPDF("numofe"))
                        Dim partes() As String
                        partes = Request("__EVENTARGUMENT").Split("#")
                        Me.MostrarPDF(partes(0), partes(1))
                        CargarwhdgOfertasHistoricas(False)
                    Case Else
                        CargarwhdgOfertasHistoricas(False)
                End Select

            End If

            If Not (ScriptMgr.IsInAsyncPostBack AndAlso ScriptMgr.AsyncPostBackSourceElementID = "whdgOfertasHistoricas" AndAlso whdgOfertasHistoricas.GridView.Behaviors.Filtering.Filtered) Then
                'El InicializarPaginador se lanza al filtrar los datos. Si lo lanzamos aquí tb deja de funcionar el recuento de resultados que se hace dentro de la función.
                'Por eso excluimos el caso concreto en que se filtra
                InicializarPaginador()
            End If
        End Sub

        ''' Revisado por: auv. Fecha:17/01/2013
        ''' <summary>
        ''' Configura la grid para su posterior exportación. Exporta la Grid a una hoja excel
        ''' </summary>
        ''' <remarks>Llamada desde:Page_load; Tiempo ejecucion:0,3seg.</remarks>
        Private Sub Exportar()
            CargarwhdgOfertasHistoricas(True)

            WebExcelExporter1.DownloadName = Date.Now.ToShortDateString & "_" & Textos(0) & ".xls"
            WebExcelExporter1.Export(whdgOfertasHistoricas.GridView)
        End Sub

        Private Sub MostrarPDF(ByVal ID As Long, ByVal sNomFich As String)
            Dim wsAdjuntos As New FSN_Adjuntos.FSN_Adjuntos
            Dim byteBuffer As Byte()
            Dim sPath As String
            byteBuffer = wsAdjuntos.LeerAdjunto(ID, 0, 0, 27)
            sPath = ConfigurationManager.AppSettings("temp") + "\"
            Call Me.Response.AddHeader("Content-Type", "application/octet-stream")
            Call Me.Response.AddHeader("Content-Disposition", "attachment;filename=""" & sNomFich & """")
            Call Me.Response.BinaryWrite(byteBuffer)
            Call Me.Response.End()
        End Sub

        ''' Revisado por: auv. Fecha:17/01/2013
        ''' <summary>
        ''' Carga de los elementos con el idioma del usuario
        ''' </summary>
        ''' <remarks>Llamada desde:Page_load; Tiempo máximo=0,2</remarks>
        Private Sub CargarTextos()
            whdgOfertasHistoricas.Key = Textos(0) 'Histórico de ofertas
            lblHistoricoOfertas.Text = Textos(0) 'Histórico de ofertas
            lblFecEnvOfeDesde.Text = Textos(1) 'Fecha de envío de la oferta (desde - hasta):
            lblFecEnvOfeHasta.Text = Textos(2) '-

            lblBusquedaAvanzada.Text = Textos(3) 'Seleccione los criterios de búsqueda avanzados
            btnBuscar.Text = Textos(11) 'Buscar 
            btnLimpiar.Text = Textos(12) 'Limpiar

            lblAnyo.Text = Textos(5) 'Año:
            lblGMA.Text = Textos(6) 'GMA:
            lblCod.Text = Textos(7) 'Código:m
            lblDen.Text = Textos(8) 'Denominación:
            lblCodArt.Text = Textos(9) 'Código de artículo:
            lblDenArt.Text = Textos(10) 'Descripción de artículo:
        End Sub

        ''' Revisado por: auv. Fecha:17/01/2013
        ''' <summary>
        ''' Tras la carga del grid se "sincroniza" con el Custom Pager
        ''' </summary>
        ''' <param name="sender">control</param>
        ''' <param name="e">evento de sistema</param>  
        ''' <remarks>Llamada desde: sistema; Tiempo máximo:0 </remarks>
        Private Sub whdgOfertasHistoricas_GroupedColumnsChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GroupedColumnsChangedEventArgs) Handles whdgOfertasHistoricas.GroupedColumnsChanged
            InicializarPaginador()
            upOfertasHistoricas.Update()
        End Sub

        ''' Revisado por: auv. Fecha:17/01/2013
        ''' <summary>
        ''' Recuperamos las ofertas históricas desde bdd
        ''' </summary>
        ''' <param name="botonBusqueda">Dependiendo el botón de búsqueda, se aplicarán unos filtros u otros</param>
        ''' <returns>Devuelve dataset con las ofertas historicas</returns>
        ''' <remarks>Llamada desde visorOfertas.aspx.vb, máx 1 seg.
        ''' </remarks>
        Private Function ObtenerDatasetOfertasHistoricas() As DataSet
            Dim oOfertasHistoricas As PMPortalServer.OfertasHistoricas = FSPMServer.Get_OfertasHistoricas

            'Obtener filtros
            Dim dFecOfeDesde As Date
            Dim dFecOfeHasta As Date
            Dim iAnio As Integer
            Dim sGmn1 As String
            Dim iProce As Integer
            Dim sDenProce As String
            Dim sCodArt As String
            Dim sDenArt As String

            dFecOfeDesde = IIf(txtFecEnvOfeDesde.Valor Is DBNull.Value, Nothing, txtFecEnvOfeDesde.Valor)
            dFecOfeHasta = IIf(txtFecEnvOfeHasta.Valor Is DBNull.Value, Nothing, txtFecEnvOfeHasta.Valor)

            If (ddAnyo.SelectedValue IsNot Nothing AndAlso ddAnyo.SelectedValue <> String.Empty) Then
                iAnio = ddAnyo.SelectedValue
            Else
                iAnio = Nothing
            End If
            If (ddGMA.SelectedValue IsNot Nothing AndAlso ddGMA.SelectedValue <> String.Empty) Then
                sGmn1 = ddGMA.SelectedValue
            Else
                sGmn1 = Nothing
            End If
            If (txtCod.Text IsNot Nothing AndAlso txtCod.Text <> String.Empty) Then
                iProce = txtCod.Text
            Else
                iProce = Nothing
            End If
            If (txtDen.Text IsNot Nothing AndAlso txtDen.Text <> String.Empty) Then
                sDenProce = txtDen.Text
            Else
                sDenProce = Nothing
            End If
            If (txtCodArt.Text IsNot Nothing AndAlso txtCodArt.Text <> String.Empty) Then
                sCodArt = txtCodArt.Text
            Else
                sCodArt = Nothing
            End If
            If (txtDenArt.Text IsNot Nothing AndAlso txtDenArt.Text <> String.Empty) Then
                sDenArt = txtDenArt.Text
            Else
                sDenArt = Nothing
            End If

            oOfertasHistoricas.LoadData(IdCiaComp, IdCia, dFecOfeDesde, dFecOfeHasta, iAnio, sGmn1, iProce, sDenProce, sCodArt, sDenArt, EmpresaPortal, NomPortal)

            If Not oOfertasHistoricas.Data Is Nothing Then Me.InsertarEnCache("dsOfertasHistoricas_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString, oOfertasHistoricas.Data, Caching.CacheItemPriority.BelowNormal)
            Return oOfertasHistoricas.Data
        End Function

        ''' Revisado por: auv. Fecha:22/01/2013
        ''' <summary>
        ''' Recuperamos los anyos de las ofertas históricas desde bdd
        ''' </summary>
        ''' <returns>Devuelve dataset con los anyos de las ofertas históricas</returns>
        ''' <remarks>Llamada desde visorOfertas.aspx.vb, máx 1 seg.
        ''' </remarks>
        Private Function ObtenerDatasetOfertasHistoricasAnyos() As DataSet
            Dim oOfertasHistoricas As PMPortalServer.OfertasHistoricas = FSPMServer.Get_OfertasHistoricas

            oOfertasHistoricas.LoadAnyos(IdCiaComp, IdCia)

            If Not oOfertasHistoricas.Anyos Is Nothing Then Me.InsertarEnCache("dsOfertasHistoricasAnyos_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString, oOfertasHistoricas.Anyos, Caching.CacheItemPriority.BelowNormal)
            Return oOfertasHistoricas.Anyos
        End Function

        ''' Revisado por: auv. Fecha:22/01/2013
        ''' <summary>
        ''' Recuperamos los gmn1s de las ofertas históricas desde bdd
        ''' </summary>
        ''' <returns>Devuelve dataset con los gmn1s de las ofertas históricas</returns>
        ''' <remarks>Llamada desde visorOfertas.aspx.vb, máx 1 seg.
        ''' </remarks>
        Private Function ObtenerDatasetOfertasHistoricasGmn1s() As DataSet
            Dim oOfertasHistoricas As PMPortalServer.OfertasHistoricas = FSPMServer.Get_OfertasHistoricas

            oOfertasHistoricas.LoadGmn1s(IdCiaComp, IdCia)

            If Not oOfertasHistoricas.Gmn1s Is Nothing Then Me.InsertarEnCache("dsOfertasHistoricasGmn1s_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString, oOfertasHistoricas.Gmn1s, Caching.CacheItemPriority.BelowNormal)
            Return oOfertasHistoricas.Gmn1s
        End Function

        ''' <summary>
        ''' Dataset de OfertasHistoricas
        ''' </summary>
        Private ReadOnly Property DatasetOfertasHistoricas() As DataSet
            Get
                If HttpContext.Current.Cache("dsOfertasHistoricas_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString) IsNot Nothing Then
                    Return HttpContext.Current.Cache("dsOfertasHistoricas_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString)
                Else
                    Return ObtenerDatasetOfertasHistoricas()
                End If
            End Get
        End Property

        ''' Revisado por: auv. Fecha:17/01/2013
        ''' <summary>
        ''' Método que se lanza al ordenar por una columna en el grid whdgOfertasHistoricas. Actualiza el paginador y el grid para mostrar los cambios.
        ''' </summary>
        ''' <param name="sender">El control que lanza el evento</param>
        ''' <param name="e">argumentos del evento</param>
        ''' <remarks>Llamada desde el evento de filtrado. Máx 1 seg.</remarks>
        Private Sub whdgOfertasHistoricas_ColumnSorted(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.SortingEventArgs) Handles whdgOfertasHistoricas.ColumnSorted
            Me._PaginadorTop.PageNumber = 1
            InicializarPaginador()
            upOfertasHistoricas.Update()
            CType(whdgOfertasHistoricas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("wdgPaginador").FindControl("upPaginador"), UpdatePanel).Update()
        End Sub

        ''' Revisado por: auv. Fecha:17/01/2013
        ''' <summary>
        ''' Método que se lanza al aplicar filtros en el grid whdgOfertasHistoricas. Actualiza el paginador y el grid para mostrar los cambios.
        ''' </summary>
        ''' <param name="sender">El control que lanza el evento</param>
        ''' <param name="e">argumentos del evento</param>
        ''' <remarks>Llamada desde el evento de filtrado. Máx 1 seg.</remarks>
        Private Sub whdgOfertasHistoricas_DataFiltered(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.FilteredEventArgs) Handles whdgOfertasHistoricas.DataFiltered
            Me._PaginadorTop.PageNumber = 1
            InicializarPaginador()
            upOfertasHistoricas.Update()
            CType(whdgOfertasHistoricas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("wdgPaginador").FindControl("upPaginador"), UpdatePanel).Update()

            'blp Incidencia 31900.5/2012/196 - 24152
            'En IE el control webhierarchicaldatagrid tiene un bug y no muestra bien el desplegable de los filtros cuando 
            'la altura del control no está fijada y hay un número pequeño de registros (de 0 a 5 aproximadamente)
            'Como el prototipo de la tarea 2126 pide que no se establezca altura al control (para que no haya scroll vertical en el grid),
            'vamos a determinar la altura del control por css sólo cuando haya una cantidad de registros baja.
            'Esto va a afectar visualmente a la pantalla pero de forma mínima (Al fijar la altura por css, 
            'el alto del recuadro de datos del grid se ajusta a los datos aunque el grid tenga un alto mayor,no visible, 
            'que crea un scroll vertical en la pantalla)
            Dim numRegistros As Long = 1
            If whdgOfertasHistoricas.GridView.Behaviors.Filtering.Filtered Then
                whdgOfertasHistoricas.GridView.Behaviors.Paging.Enabled = False
                numRegistros += whdgOfertasHistoricas.GridView.Rows.Count
                whdgOfertasHistoricas.GridView.Behaviors.Paging.Enabled = True
            Else
                If DatasetOfertasHistoricas IsNot Nothing AndAlso DatasetOfertasHistoricas.Tables("OFERTASHISTORICAS") IsNot Nothing AndAlso DatasetOfertasHistoricas.Tables("OFERTASHISTORICAS").Rows.Count > 0 Then _
                    numRegistros += DatasetOfertasHistoricas.Tables("OFERTASHISTORICAS").Rows.Count
            End If
            If numRegistros <= 5 Then
                Me.whdgOfertasHistoricas.Style.Add("Height", "500px")
            Else
                Me.whdgOfertasHistoricas.Style.Remove("Height")
            End If
        End Sub

        ''' Revisado por: blp. Fecha: 07/12/2011
        ''' <summary>
        ''' Método mediante el cual cargamos en el control whdgOfertasHistoricas los datos de ofertas históricas
        ''' </summary>
        ''' <param name="paraExportacion">
        '''      False->La carga del grid no es para exportación sino para mostrar en pantalla por lo que se pagina
        '''      True->La carga del grid es para exportación por lo que no se pagina a sólo saldrían en la exportación el número de registros indicado en la paginación
        ''' </param>
        ''' <remarks>Llamada desde Load() y Exportar. Máx 1 seg</remarks>
        Private Sub CargarwhdgOfertasHistoricas(ByVal paraExportacion As Boolean)
            'Necesitamos que se genere el datasetEntregas para usarlo dentro del CreatewhdgEntregasColumns (si es que no está en caché, como por ejemplo al pulsar un botón de búsqueda)
            'por lo que primero lo generamos aquí y luego hacemos el databound
            whdgOfertasHistoricas.DataSource = DatasetOfertasHistoricas()
            whdgOfertasHistoricas.GridView.DataSource = whdgOfertasHistoricas.DataSource
            'Los DataKeyFields deben definirse entre el Datasource y el Databind
            whdgOfertasHistoricas.DataMember = "OFERTASHISTORICAS"
            whdgOfertasHistoricas.DataKeyFields = "CODIGO, DESCRIPCION, FECOFE, NUMOFE, DETALLE, ADJUNID"

            'Añadir campos al webdatagrid
            CreatewhdgOfertasHistoricasColumnsYConfiguracion()
            whdgOfertasHistoricas.DataBind()
            whdgOfertasHistoricas.GridView.Columns("Key_ADJUNID").Hidden = True
            If Not paraExportacion Then
                whdgOfertasHistoricas.GridView.Behaviors.Paging.Enabled = True
                whdgOfertasHistoricas.GridView.Behaviors.Paging.PageSize = ConfigurationManager.AppSettings("PaginacionOfertasHistoricas")
            Else
                whdgOfertasHistoricas.GridView.Behaviors.Paging.Enabled = False
            End If

            'blp Incidencia 31900.5/2012/196 - 24152
            'En IE el control webhierarchicaldatagrid tiene un bug y no muestra bien el desplegable de los filtros cuando 
            'la altura del control no está fijada y hay un número pequeño de registros (de 0 a 5 aproximadamente)
            'Como el prototipo de la tarea 2126 pide que no se establezca altura al control (para que no haya scroll vertical en el grid),
            'vamos a determinar la altura del control por css sólo cuando haya una cantidad de registros baja.
            'Esto va a afectar visualmente a la pantalla pero de forma mínima (Al fijar la altura por css, 
            'el alto del recuadro de datos del grid se ajusta a los datos aunque el grid tenga un alto mayor,no visible, 
            'que crea un scroll vertical en la pantalla)
            'vamos a determinar la altura mínima del control sólo cuando haya una cantidad de registros baja
            If whdgOfertasHistoricas.DataSource IsNot Nothing AndAlso CType(whdgOfertasHistoricas.DataSource, DataSet).Tables.Count > 0 AndAlso CType(whdgOfertasHistoricas.DataSource, DataSet).Tables(0).Rows.Count <= 5 Then
                'Me.whdgEntregas.Height = Unit.Pixel(500)
                Me.whdgOfertasHistoricas.Style.Add("Height", "500px")
            Else
                Me.whdgOfertasHistoricas.Style.Remove("Height")
            End If
        End Sub

#Region "Anyos"
        ''' Revisado por: auv. Fecha:17/01/2013
        ''' <summary>
        ''' Función que devuelve un objeto List con los años de los procesos presentes en las ofertas históricas.
        ''' </summary>
        ''' <returns>Un dataset con los años presentes en las ofertas históricas.</returns>
        ''' <remarks>
        ''' Llamada desde: visorOfertas.vb.InicializarValoresControles. Máx 0,2 seg</remarks>
        Public Function CargarAnyos() As List(Of ListItem)
            Dim oOfertasHistoricas As PMPortalServer.OfertasHistoricas = FSPMServer.Get_OfertasHistoricas
            Dim ds As DataSet

            ds = ObtenerDatasetOfertasHistoricasAnyos()

            Dim query As List(Of ListItem) = From Datos In ds.Tables("OFERTASHISTORICAS_ANYOS") _
                                           Order By Datos.Item("ANYO") _
                                           Select New ListItem(Datos.Item("ANYO"), Datos.Item("ANYO")) Distinct.ToList()
            Return query
        End Function
#End Region

#Region "GMA"
        ''' Revisado por: auv. Fecha:18/01/2013
        ''' <summary>
        ''' Función que devuelve un objeto List con los GMN1 de los procesos presentes en las ofertas históricas.
        ''' </summary>
        ''' <returns>Un dataset con los gmn1s presentes en las ofertas históricas.</returns>
        ''' <remarks>
        ''' Llamada desde: visorOfertas.vb.InicializarValoresControles. Máx 0,2 seg</remarks>
        Public Function CargarGMA() As List(Of ListItem)
            Dim oOfertasHistoricas As PMPortalServer.OfertasHistoricas = FSPMServer.Get_OfertasHistoricas
            Dim ds As DataSet

            ds = ObtenerDatasetOfertasHistoricasGmn1s()

            Dim query As List(Of ListItem) = From Datos In ds.Tables("OFERTASHISTORICAS_GMN1S") _
                                           Order By Datos.Item("GMN1") _
                                           Select New ListItem(Datos.Item("GMN1"), Datos.Item("GMN1")) Distinct.ToList()
            Return query
        End Function
#End Region

        ''' Revisado por: auv. Fecha:17/01/2013
        ''' <summary>
        ''' Inserta los valores iniciales en los filtros: último mes de fecha de envío de oferta
        ''' Configura los combos
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub InicializarValoresControles()
            If Not IsPostBack Then
                txtFecEnvOfeDesde.Valor = DateTime.Now.AddYears(-1).Date
                txtFecEnvOfeHasta.Valor = DateTime.Now.Date
            End If

            Dim lista As List(Of ListItem)

            ' DropDown Años
            lista = CargarAnyos()
            If lista.Any Then _
             lista.Insert(0, New ListItem(String.Empty, String.Empty))
            'Conservar seleccionado el valor del dropdown
            Dim anyoSeleccionado As String
            If ddAnyo.SelectedValue IsNot Nothing Then
                anyoSeleccionado = ddAnyo.SelectedValue
            End If
            ddAnyo.DataSource = lista
            If lista.Exists(Function(anyo As ListItem) anyo.Value = anyoSeleccionado) Then _
                ddAnyo.SelectedValue = lista.Find(Function(anyo As ListItem) anyo.Value = anyoSeleccionado).Value
            ddAnyo.DataBind()

            ' DropDown GMA
            lista = CargarGMA()
            If lista.Any Then _
             lista.Insert(0, New ListItem(String.Empty, String.Empty))
            'Conservar seleccionado el valor del dropdown
            Dim gmaSeleccionado As String
            If ddGMA.SelectedValue IsNot Nothing Then
                gmaSeleccionado = ddGMA.SelectedValue
            End If
            ddGMA.DataSource = lista
            If lista.Exists(Function(gma As ListItem) gma.Value = gmaSeleccionado) Then _
                ddGMA.SelectedValue = lista.Find(Function(gma As ListItem) gma.Value = gmaSeleccionado).Value
            ddGMA.DataBind()

            'Actualizar el panel de búsqueda
            'upBusqueda.Update()
        End Sub

        ''' Revisado por: auv. Fecha:17/01/2013
        ''' <summary>
        ''' Método que se lanza en el evento de click del botón de búsqueda
        ''' Limpia los datos guardados, carga los de la búsqueda, reinicia el paginador
        ''' </summary>
        ''' <param name="sender">control que lanza el evento</param>
        ''' <param name="e">Argumentos del evento</param>
        ''' <remarks>llamada desde el evento. Máx. 1 seg.</remarks>
        Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
            BorrarCacheOfertasHistoricas()
            BorrarCacheOfertaHistoricaAdjun()
            BorrarCacheOfertasHistoricasAnyos()
            BorrarCacheOfertasHistoricasGmn1s()
            InicializarValoresControles()
            CargarwhdgOfertasHistoricas(False)
            InicializarPaginador()
            RellenarLabelFiltros()
            upOfertasHistoricas.Update()
        End Sub

        ''' Revisado por: auv. Fecha: 17/01/2013
        ''' <summary>
        ''' Método que borra de la caché los datos de ofertas históricas
        ''' </summary>
        ''' <remarks>Llamada desde los botones btnBuscar. Máx. 0,2 seg</remarks>
        Private Sub BorrarCacheOfertasHistoricas()
            If HttpContext.Current.Cache("dsOfertasHistoricas_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString) IsNot Nothing Then _
                HttpContext.Current.Cache.Remove("dsOfertasHistoricas_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString)
        End Sub

        ''' Revisado por: auv. Fecha: 23/01/2013
        ''' <summary>
        ''' Método que borra de la caché los datos de el adjunto de la oferta histórica
        ''' </summary>
        ''' <remarks>Llamada desde los botones btnBuscar. Máx. 0,2 seg</remarks>
        Private Sub BorrarCacheOfertaHistoricaAdjun()
            If HttpContext.Current.Cache("dsOfertaHistoricaAdjun_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString) IsNot Nothing Then _
                HttpContext.Current.Cache.Remove("dsOfertaHistoricaAdjun_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString)
        End Sub

        ''' Revisado por: auv. Fecha: 17/01/2013
        ''' <summary>
        ''' Método que borra de la caché los datos de anyos de las ofertas históricas
        ''' </summary>
        ''' <remarks>Llamada desde los botones btnBuscar. Máx. 0,2 seg</remarks>
        Private Sub BorrarCacheOfertasHistoricasAnyos()
            If HttpContext.Current.Cache("dsOfertasHistoricasAnyos_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString) IsNot Nothing Then _
                HttpContext.Current.Cache.Remove("dsOfertasHistoricasAnyos_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString)
        End Sub

        ''' Revisado por: auv. Fecha: 17/01/2013
        ''' <summary>
        ''' Método que borra de la caché los datos de gmn1s de las ofertas históricas
        ''' </summary>
        ''' <remarks>Llamada desde los botones btnBuscar. Máx. 0,2 seg</remarks>
        Private Sub BorrarCacheOfertasHistoricasGmn1s()
            If HttpContext.Current.Cache("dsOfertasHistoricasGmn1s_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString) IsNot Nothing Then _
                HttpContext.Current.Cache.Remove("dsOfertasHistoricasGmn1s_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString)
        End Sub

        ''' Revisado por: auv. Fecha:17/01/2013
        ''' <summary>
        ''' Método que limpia los criterios de búsqueda.
        ''' </summary>
        ''' <param name="sender">Control btnLimpiar</param>
        ''' <param name="e">parámetros del evento</param>
        ''' <remarks>llamada desde el control, al hacer click. Tiempo máximo inferios a 0,1 seg.</remarks>
        Private Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
            'Recorremos los controles del panel y los vamos limpiando según el tipo (panel de búsqueda general)
            VaciarControlesRecursivo(tblBusquedaGeneral)
            'Recorremos los controles del panel y los vamos limpiando según el tipo (panel de búsqueda avanzada)
            VaciarControlesRecursivo(pnlParametros)
            'Ocultamos el label con el resumen de los filtros generales
            divBusqueda.Style("display") = "none"
        End Sub

        ''' Revisado por: auv. Fecha:17/01/2013
        ''' <summary>
        ''' Método que recorre de manera recursiva el control recibido y vacía las selecciones de todos los controles
        ''' </summary>
        ''' <param name="oControlBase">Control a recorrer</param>
        ''' <remarks>Llamada desde btnLimpiar_Click</remarks>
        Private Sub VaciarControlesRecursivo(ByVal oControlBase As Object)
            For Each oControl In oControlBase.Controls
                Select Case oControl.GetType().ToString
                    Case GetType(DropDownList).ToString
                        If CType(oControl, DropDownList).SelectedItem IsNot Nothing Then _
                            CType(oControl, DropDownList).SelectedItem.Selected = False
                    Case GetType(TextBox).ToString
                        CType(oControl, TextBox).Text = String.Empty
                    Case GetType(Infragistics.WebUI.WebSchedule.WebDateChooser).ToString
                        CType(oControl, Infragistics.WebUI.WebSchedule.WebDateChooser).Value = Nothing
                    Case GetType(CheckBox).ToString
                        CType(oControl, CheckBox).Checked = False
                    Case GetType(Fullstep.DataEntry.GeneralEntry).ToString
                        'Actualmente hay un único tipo de control generalEntry (TipoFecha) en la página.
                        'Si se añaden más, es posible que haya que añadir un select case para contemplar cada caso.
                        CType(oControl, Fullstep.DataEntry.GeneralEntry).Valor = Nothing
                        CType(CType(oControl, Fullstep.DataEntry.GeneralEntry).Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebDateTimeEdit).Value = Nothing
                    Case Else
                        If oControl.Controls IsNot Nothing AndAlso oControl.Controls.Count > 0 Then
                            VaciarControlesRecursivo(oControl)
                        End If
                End Select
            Next
        End Sub

        ''' Revisado por: blp. Fecha: 07/12/2011
        ''' <summary>
        ''' Crea las columnas fijas del grid en función de los datos guardados en la configuración (tabla FSP_CONF_VISOR_OFERTAS_HISTORICAS)
        ''' y añade los campos al panel de configuración
        ''' </summary>
        ''' <remarks>Llamada desde CargarwhdgOfertasHistoricas. Máx. 0,5 seg</remarks>
        Private Sub CreatewhdgOfertasHistoricasColumnsYConfiguracion()
            Me.EditColumnYConfiguracion("CODIGO", Textos(16)) 'Código del proceso
            Me.EditColumnYConfiguracion("DESCRIPCION", Textos(17)) 'Descripción del proceso de compras
            Me.EditColumnYConfiguracion("FECOFE", Textos(18)) 'Fecha de la oferta
            Me.EditColumnYConfiguracion("NUMOFE", Textos(19)) 'Nº de oferta
            Me.EditColumnYConfiguracion("DETALLE", Textos(24)) 'Oferta
            Me.EditColumnYConfiguracion("ADJUNID", "ADJUNID") 'ID del adjunto
            'Configurar Anchos y Visibilidad
            confAnchosYVisibilidad()
        End Sub

        ''' Revisado por: blp. Fecha: 12/12/2011
        ''' <summary>
        ''' Función que añade una columna al control whdgOfertasHistoricas, vinculada al origen de datos de éste.
        ''' Añade el mismo campo al panel de configuración
        ''' </summary>
        ''' <param name="fieldName">Nombre del campo en el origen de datos</param>
        ''' <param name="headerText">Título de la columna</param>
        ''' <param name="headerTooltip">Tooltip de la columna</param>
        ''' <remarks>Llamada desde CreatewhdgOfertasHistoricasColumns. Máx. 0,1 seg.</remarks>
        Private Sub EditColumnYConfiguracion(ByVal fieldName As String, ByVal headerText As String, Optional ByVal headerTooltip As String = "")
            'Configurar el grid
            If Not whdgOfertasHistoricas.GridView.Columns.Item("Key_" & fieldName) Is Nothing Then
                With whdgOfertasHistoricas.GridView.Columns.Item("Key_" & fieldName)
                    .Header.Text = headerText
                    If headerTooltip = String.Empty Then headerTooltip = headerText
                    .Header.Tooltip = headerTooltip
                End With
                If whdgOfertasHistoricas.Columns.Item("Key_" & fieldName) IsNot Nothing Then
                    whdgOfertasHistoricas.Columns.Item("Key_" & fieldName).Header.Text = headerText
                    If headerTooltip = String.Empty Then headerTooltip = headerText
                    whdgOfertasHistoricas.Columns.Item("Key_" & fieldName).Header.Tooltip = headerTooltip
                End If
                anyadirTextoAColumnaNoFiltrada("Key_" & fieldName)
            End If
        End Sub

        ''' Revisado por: blp. Fecha: 10/04/2012
        ''' <summary>
        ''' Añade el alt "Filtro no aplicado" a las columnas no filtradas del whdgOfertasHistoricas
        ''' </summary>
        ''' <param name="fieldKey">Key de la columna a la que queremos añadir el texto</param>
        ''' <remarks>Llamada desde EditColumnYConfiguracion y AddColumnYConfiguracion. Máx. 0,1 seg.</remarks>
        Private Sub anyadirTextoAColumnaNoFiltrada(ByVal fieldKey As String)
            Dim fieldSetting As Infragistics.Web.UI.GridControls.ColumnFilteringSetting
            fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
            fieldSetting.ColumnKey = fieldKey
            fieldSetting.BeforeFilterAppliedAltText = Textos(23) 'Filtro no aplicado 
            Me.whdgOfertasHistoricas.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
            Me.whdgOfertasHistoricas.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        End Sub

        ''' Revisado por: blp. Fecha: 30/01/2012
        ''' <summary>
        ''' Configurar Anchos y Visibilidad de los campos del webhierarchicaldatagrid
        ''' </summary>
        ''' <remarks>Llamada desde CreatewhdgOfertasHistoricasColumns. Máx. 0,1 seg.</remarks>
        Private Sub confAnchosYVisibilidad()
            'Obtenemos la relación entre los campos de la tabla de configuracion de anchos y visibilidad con los campos de la tabla de ofertas históricas
            Dim camposOfertasHistoricasYConfiguracion As List(Of Campos) = relacionarOfertasHistoricasyConfiguracion()
            For Each oColumna As GridField In whdgOfertasHistoricas.GridView.Columns
                If oColumna.Header.Text = "ADJUNID" Then
                    oColumna.Hidden = True
                Else
                    oColumna.Hidden = False
                End If
                oColumna.Hidden = False
                Dim campo As Campos = camposOfertasHistoricasYConfiguracion.Find(Function(x As Campos) x.CampoOfertaHistorica = oColumna.Key.Replace("Key_", ""))
                oColumna.Width = Unit.Pixel(campo.Width)
                If whdgOfertasHistoricas.Columns("Key_" & campo.CampoOfertaHistorica) IsNot Nothing Then _
                    whdgOfertasHistoricas.Columns("Key_" & campo.CampoOfertaHistorica).Width = Unit.Pixel(campo.Width)
            Next
        End Sub

        ''' Revisado por: blp. Fecha: 22/12/2011
        ''' <summary>
        ''' Devuelve la relación entre campos del grid, del dataset de ofertas históricas y el datatable con los datos de ancho y visibilidad
        ''' </summary>
        ''' <returns>Lista de Campos con la relación</returns>
        ''' <remarks>Llamada desde confAnchosYVisibilidad. Máx. 0,1 seg.</remarks>
        Private ReadOnly Property relacionarOfertasHistoricasyConfiguracion() As List(Of Campos)
            Get
                Dim relacionOfertasHistoricasyConfiguracion As New List(Of Campos)

                relacionOfertasHistoricasyConfiguracion.Add(New Campos("CODIGO", "CODIGO_VISIBLE", "CODIGO_WIDTH", "180"))
                relacionOfertasHistoricasyConfiguracion.Add(New Campos("DESCRIPCION", "DESCRIPCION_VISIBLE", "DESCRIPCION_WIDTH", "280"))
                relacionOfertasHistoricasyConfiguracion.Add(New Campos("FECOFE", "FECOFE_VISIBLE", "FECOFE_WIDTH", "130"))
                relacionOfertasHistoricasyConfiguracion.Add(New Campos("NUMOFE", "NUMOFE_VISIBLE", "NUMOFE_WIDTH", "80"))
                relacionOfertasHistoricasyConfiguracion.Add(New Campos("DETALLE", "DETALLE_VISIBLE", "DETALLE_WIDTH", "300"))
                relacionOfertasHistoricasyConfiguracion.Add(New Campos("ADJUNID", "ADJUNID_VISIBLE", "ADJUNID_WIDTH", "0"))
                Return relacionOfertasHistoricasyConfiguracion
            End Get
        End Property

        ''' <summary>
        ''' Estructura compuesta por 5 elementos q nos va a permitir almacenar el nombre del campo en el datatable de oferta histórica (DatasetOfertasHistoricas.Tables("OFERTASHISTORICAS")), el nombre del campo VISIBLE en la tabla de configuración (FSP_CONF_VISOR_OFERTAS_HISTORICAS), el nombre del campo WIDTH en esa misma tabla, si es visible y su anchura.
        ''' </summary>
        Private Structure Campos
            Public CampoOfertaHistorica As String
            Public CampoConfiguracionVisible As String
            Public CampoConfiguracionWidth As String
            Public Visible As Boolean
            Public Width As Double

            ''' Revisado por: blp. Fecha: 15/12/2011
            ''' <summary>
            ''' Método para crear una nueva instancia de la estructura Campos
            ''' </summary>
            ''' <param name="sCampoOfertaHistorica">nombre del campo en el datatable de ofertas históricas (DatasetOfertasHistoricas.Tables("OFERTASHISTORICAS"))</param>
            ''' <param name="sCampoConfiguracionVisible">nombre del campo VISIBLE en la tabla de configuración (FSP_CONF_VISOR_RECEPCIONES)</param>
            ''' <param name="sCampoConfiguracionWidth">nombre del campo WIDTH en la tabla de configuración (FSP_CONF_VISOR_RECEPCIONES)</param>
            ''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
            Sub New(ByVal sCampoOfertaHistorica As String, ByVal sCampoConfiguracionVisible As String, ByVal sCampoConfiguracionWidth As String)
                Me.CampoOfertaHistorica = sCampoOfertaHistorica
                Me.CampoConfiguracionVisible = sCampoConfiguracionVisible
                Me.CampoConfiguracionWidth = sCampoConfiguracionWidth
            End Sub

            ''' Revisado por: blp. Fecha: 15/12/2011
            ''' <summary>
            ''' Método para crear una nueva instancia de la estructura Campos
            ''' </summary>
            ''' <param name="sCampoOfertaHistorica">nombre del campo en el datatable de ofertas históricas (DatasetOfertasHistoricas.Tables("OFERTASHISTORICAS"))</param>
            ''' <param name="sCampoConfiguracionVisible">nombre del campo VISIBLE en la tabla de configuración (FSP_CONF_VISOR_RECEPCIONES)</param>
            ''' <param name="sCampoConfiguracionWidth">nombre del campo WIDTH en la tabla de configuración (FSP_CONF_VISOR_RECEPCIONES)</param>
            ''' <param name="dblWidth">ancho del campo</param>
            ''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
            Sub New(ByVal sCampoOfertaHistorica As String, ByVal sCampoConfiguracionVisible As String, ByVal sCampoConfiguracionWidth As String, ByVal dblWidth As Double)
                Me.CampoOfertaHistorica = sCampoOfertaHistorica
                Me.CampoConfiguracionVisible = sCampoConfiguracionVisible
                Me.CampoConfiguracionWidth = sCampoConfiguracionWidth
                Me.Width = dblWidth
            End Sub

            ''' Revisado por: blp. Fecha: 15/12/2011
            ''' <summary>
            ''' Método para crear una nueva instancia de la estructura Campos
            ''' </summary>
            ''' <param name="sCampoOfertaHistorica">nombre del campo en el datatable de ofertas históricas (DatasetOfertasHistoricas.Tables("OFERTASHISTORICAS"))</param>
            ''' <param name="sCampoConfiguracionVisible">nombre del campo VISIBLE en la tabla de configuración (FSP_CONF_VISOR_RECEPCIONES)</param>
            ''' <param name="sCampoConfiguracionWidth">nombre del campo WIDTH en la tabla de configuración (FSP_CONF_VISOR_RECEPCIONES)</param>
            ''' <param name="bVisible">Es visible</param>
            ''' <param name="dblWidth">ancho del campo</param>
            ''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
            Sub New(ByVal sCampoOfertaHistorica As String, ByVal sCampoConfiguracionVisible As String, ByVal sCampoConfiguracionWidth As String, ByVal bVisible As Boolean, ByVal dblWidth As Double)
                Me.CampoOfertaHistorica = sCampoOfertaHistorica
                Me.CampoConfiguracionVisible = sCampoConfiguracionVisible
                Me.CampoConfiguracionWidth = sCampoConfiguracionWidth
                Me.Visible = bVisible
                Me.Width = dblWidth
            End Sub
        End Structure

        ''' Revisado por: blp. Fecha: 12/12/2011
        ''' <summary>
        ''' Método que se lanza en el evento InitializeRow (cuando se enlazan los datos del origen de datos con la línea)
        ''' </summary>
        ''' <param name="sender">Control que lanza el evento (whdgOfertasHistoricas)</param>
        ''' <param name="e">Argumentos del evento</param>
        ''' <remarks>Llamada desde el evento. Máx 0,1 seg</remarks>
        Private Sub whdgOfertasHistoricas_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgOfertasHistoricas.InitializeRow
            If (Not (e.Row.Items.FindItemByKey("Key_DETALLE").Value Is Nothing)) AndAlso _
            (Not (e.Row.Items.FindItemByKey("Key_DETALLE").Value = String.Empty)) AndAlso _
            (Not (e.Row.Items.FindItemByKey("Key_DETALLE").Value = "")) Then
                e.Row.Items.FindItemByKey("Key_DETALLE").CssClass = "ofertaHistoricaSeleccionable"
            End If
        End Sub

#Region "Paginador"
        ''' Revisado por: blp. Fecha: 15/12/2011
        ''' <summary>
        ''' Inicializamos el paginador
        ''' </summary>
        ''' <remarks>Llamada desde load y botones de búsqueda. Máx. 0,1 seg.</remarks>
        Private Sub InicializarPaginador()
            Dim numRegistros As Long = 0
            If whdgOfertasHistoricas.GridView.Behaviors.Filtering.Filtered Then
                whdgOfertasHistoricas.GridView.Behaviors.Paging.Enabled = False
                numRegistros = whdgOfertasHistoricas.GridView.Rows.Count
                whdgOfertasHistoricas.GridView.Behaviors.Paging.Enabled = True
            Else
                If DatasetOfertasHistoricas IsNot Nothing AndAlso DatasetOfertasHistoricas.Tables("OFERTASHISTORICAS") IsNot Nothing AndAlso DatasetOfertasHistoricas.Tables("OFERTASHISTORICAS").Rows.Count > 0 Then _
                    numRegistros = DatasetOfertasHistoricas.Tables("OFERTASHISTORICAS").Rows.Count
            End If
            Me._PaginadorTop.SetContext(numRegistros, ConfigurationManager.AppSettings("PaginacionOfertasHistoricas"))

            Me.whdgOfertasHistoricas.GridView.Behaviors.Paging.PageIndex = 0
        End Sub

        ''' Revisado por: blp. Fecha: 30/01/2012
        ''' <summary>
        ''' Método que actualiza el contador de páginas del paginador
        ''' </summary>
        ''' <param name="sender">Control que lanza el evento PageChanged</param>
        ''' <param name="e">argumentos del evento</param>
        ''' <remarks>Llamada desde evento PageChanged. Máximo: 0,1 seg.</remarks>
        Private Sub _Paginador_PageChanged(ByVal sender As Object, ByVal e As PageSettingsChangedEventArgs)
            If (e.TotalNumberOfPages - 1) < e.PageIndex Then
                Me.whdgOfertasHistoricas.GridView.Behaviors.Paging.PageIndex = 0
            Else
                Me.whdgOfertasHistoricas.GridView.Behaviors.Paging.PageIndex = e.PageIndex
            End If

            'Actualizamos tb el otro paginador
            Dim oPagSender As Paginador = TryCast(sender, Paginador)
            If oPagSender IsNot Nothing Then
                Me._PaginadorTop.PageNumber = e.PageNumber
                Me._PaginadorTop.ActualizarPaginador()
            End If
            upOfertasHistoricas.Update()
        End Sub
#End Region

#Region "Filtros"

        ''' Revisado por: blp. Fecha: 20/01/2012
        ''' <summary>
        ''' Método que recorre todos los filtros existentes y prepara el texto que aparecerá 
        ''' en pantalla con los filtros seleccionados cuando el panel de búsqueda está oculto.
        ''' No se comprueba que los filtros están en las opciones de filtro (ni que hay permisos para ver fras y pagos) porque 
        ''' en la llamada desde gestionCookieFiltrosBusqueda ya se ha comprobado en SeleccionarFiltrosEnPanelBusqueda (método que se llama anteriormente)
        ''' y en la llamada desde btnBuscar_Click sería muy extraño que un dato seleccionado en el panel desapareciese entre el momento que se selecciona y que se pulsa al botÃ³n de buscar
        ''' </summary>
        ''' <remarks>Llamada desde gestionCookieFiltrosBusqueda y btnBuscar_Click. Máx 0,1 seg.</remarks>
        Private Sub RellenarLabelFiltros()
            ActualizarFiltros()
            Dim sListaFiltros As String = String.Empty
            Dim lista As List(Of ListItem)
            For Each oFiltro As Filtro In FiltrosAnyadidos
                Dim sValor As String = oFiltro.Valor
                Select Case oFiltro.tipo
                    Case TipoFiltro.FechaOfertaDesde
                        If oFiltro.ValorFecha <> DateTime.MinValue Then
                            sListaFiltros += "<b>" & Textos(21) & ":</b> " 'Fecha de oferta desde
                            sListaFiltros += FormatDate(oFiltro.ValorFecha, FSPMUser.DateFormat) & "; "
                        End If
                    Case TipoFiltro.FechaOfertaHasta
                        If oFiltro.ValorFecha <> DateTime.MinValue Then
                            sListaFiltros += "<b>" & Textos(22) & ":</b> " 'Fecha de oferta hasta
                            sListaFiltros += FormatDate(oFiltro.ValorFecha, FSPMUser.DateFormat) & "; "
                        End If
                    Case TipoFiltro.AnioProceso
                        sListaFiltros += "<b>" & Textos(5) & "</b> " & sValor & "; "
                    Case TipoFiltro.GMN1Proceso
                        sListaFiltros += "<b>" & Textos(6) & "</b> " & sValor & "; "
                    Case TipoFiltro.CodigoProceso
                        sListaFiltros += "<b>" & Textos(7) & "</b> " & sValor & "; "
                    Case TipoFiltro.DenominacionProceso
                        sListaFiltros += "<b>" & Textos(8) & "</b> " & sValor & "; "
                    Case TipoFiltro.CodigoArticulo
                        sListaFiltros += "<b>" & Textos(9) & "</b> " & sValor & "; "
                    Case TipoFiltro.DenominacionArticulo
                        sListaFiltros += "<b>" & Textos(10) & "</b> " & sValor & "; "
                End Select
            Next
            If sListaFiltros.Length > 0 Then
                lblBusqueda.Text = sListaFiltros
                divBusqueda.Style("display") = "block"
            Else
                lblBusqueda.Text = String.Empty
                divBusqueda.Style("display") = "none"
            End If
            cpeBusquedaAvanzada.Collapsed = True
            cpeBusquedaAvanzada.ClientState = "true"
        End Sub

        ''' <summary>
        ''' Tipos de filtro en el buscador avanzado
        ''' </summary>
        <Serializable()> _
        Public Enum TipoFiltro As Integer
            FechaOfertaDesde = 0
            FechaOfertaHasta = 1
            AnioProceso = 2
            GMN1Proceso = 3
            CodigoProceso = 4
            DenominacionProceso = 5
            CodigoArticulo = 6
            DenominacionArticulo = 7
        End Enum

        <Serializable()> _
        Public Structure Filtro
            Dim itipo As TipoFiltro
            Dim svalor As String
            Dim bvalores() As Boolean
            Dim dtvalorfecha As DateTime
            Dim sfilterInfo As String

            Public Property tipo() As TipoFiltro
                Get
                    Return itipo
                End Get
                Set(ByVal value As TipoFiltro)
                    itipo = value
                End Set
            End Property

            Public Property Valor() As String
                Get
                    Return svalor
                End Get
                Set(ByVal value As String)
                    svalor = value
                End Set
            End Property

            Public Property Valores() As Boolean()
                Get
                    Return bvalores
                End Get
                Set(ByVal value As Boolean())
                    bvalores = value
                End Set
            End Property

            Public Property ValorFecha() As DateTime
                Get
                    Return dtvalorfecha
                End Get
                Set(ByVal value As DateTime)
                    dtvalorfecha = value
                End Set
            End Property

            ''' <summary>
            ''' Propiedad del filtro que nos indica cualquier dato adicional que convenga conocer.
            ''' En el caso de las partidas presupuestarias, sirve para guardar la partida de nivel 0 / Nivel al que corresponde el filtro
            ''' (dado que puede haber más de un filtro de partida presupuestaria)
            ''' </summary>
            Public Property Info() As String
                Get
                    Return sfilterInfo
                End Get
                Set(ByVal value As String)
                    sfilterInfo = value
                End Set
            End Property

            ''' Revisado por: blp. Fecha: 20/01/2012
            ''' <summary>
            ''' Método que crea una nueva instancia de la estructura
            ''' </summary>
            ''' <param name="Tipo">Tipo de Filtro</param>
            ''' <param name="Valor">Valor string del filtro</param>
            ''' <remarks>Llamada desde la creación de la instancia. Máx. 0,1 seg.</remarks>
            Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As String)
                itipo = Tipo
                svalor = Valor
            End Sub

            ''' Revisado por: blp. Fecha: 20/01/2012
            ''' <summary>
            ''' Método que crea una nueva instancia de la estructura
            ''' </summary>
            ''' <param name="Tipo">Tipo de Filtro</param>
            ''' <param name="Valor">Valor fecha del filtro</param>
            ''' <remarks>Llamada desde la creación de la instancia. Máx. 0,1 seg.</remarks>
            Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As DateTime)
                itipo = Tipo
                dtvalorfecha = Valor
            End Sub

            ''' Revisado por: blp. Fecha: 20/01/2012
            ''' <summary>
            ''' Método que crea una nueva instancia de la estructura
            ''' </summary>
            ''' <param name="Tipo">Tipo de Filtro</param>
            ''' <param name="Valor">Valor boolean del filtro</param>
            ''' <remarks>Llamada desde la creación de la instancia. Máx. 0,1 seg.</remarks>
            Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As Boolean())
                itipo = Tipo
                bvalores = Valor
            End Sub

            ''' Revisado por: blp. Fecha: 20/01/2012
            ''' <summary>
            ''' Crear un filtro nuevo
            ''' </summary>
            ''' <param name="Tipo">Tipo de filtro</param>
            ''' <param name="Valor">Valor tipo string del filtro creado</param>
            ''' <param name="Info">Info adicional que se desee conservar del filtro</param>
            ''' <remarks>Llamada desde la creación de la instancia. Máx. 0,1 seg.</remarks>
            Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As String, ByVal Info As String)
                itipo = Tipo
                svalor = Valor
                sfilterInfo = Info
            End Sub
        End Structure

        Public Property FiltrosAnyadidos() As List(Of Filtro)
            Get
                If ViewState("FiltrosAnyadidos") Is Nothing Then
                    Return New List(Of Filtro)
                Else
                    Return CType(ViewState("FiltrosAnyadidos"), List(Of Filtro))
                End If
            End Get
            Set(ByVal value As List(Of Filtro))
                ViewState("FiltrosAnyadidos") = value
            End Set
        End Property

        ''' Revisado por: blp. Fecha: 20/01/2012
        ''' <summary>
        ''' Procedimiento que añade los filtros que se hayan seleccionado en el panel buscador a la propiedad FiltrosAnyadidos
        ''' de la página Seguimiento.aspx.vb
        ''' </summary>
        ''' <remarks>LLamada desde el propio objeto: btnBuscar_Click, dlOrdenes_itemCommand, Page_Load
        ''' Tiempo maximo 1 sec</remarks>
        Private Sub ActualizarFiltros()
            Dim lis As New List(Of Filtro)
            'Fecha Pedido desde
            Dim dtFecOfeDesde As Nullable(Of Date) = IIf(txtFecEnvOfeDesde.Valor Is DBNull.Value, Nothing, txtFecEnvOfeDesde.Valor)
            If Not dtFecOfeDesde Is Nothing Then _
                lis.Add(New Filtro(TipoFiltro.FechaOfertaDesde, CType(dtFecOfeDesde, DateTime)))
            'fecha Pedido hasta
            Dim dtFecOfeHasta As Nullable(Of Date) = IIf(txtFecEnvOfeHasta.Valor Is DBNull.Value, Nothing, txtFecEnvOfeHasta.Valor)
            If Not dtFecOfeHasta Is Nothing Then _
                lis.Add(New Filtro(TipoFiltro.FechaOfertaHasta, CType(dtFecOfeHasta, DateTime)))
            'AnioProceso
            If Not String.IsNullOrEmpty(ddAnyo.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.AnioProceso, CType(ddAnyo.SelectedValue, Integer)))
            'GMN1Proceso
            If Not String.IsNullOrEmpty(ddGMA.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.GMN1Proceso, CType(ddAnyo.SelectedValue, String)))
            'CodigoProceso
            If Not String.IsNullOrEmpty(txtCod.Text) Then _
                lis.Add(New Filtro(TipoFiltro.CodigoProceso, CType(txtCod.Text, Integer)))
            'DenominacionProceso
            If Not String.IsNullOrEmpty(txtDen.Text) Then _
                lis.Add(New Filtro(TipoFiltro.DenominacionProceso, CType(txtDen.Text, String)))
            'CodigoArticulo
            If Not String.IsNullOrEmpty(txtCodArt.Text) Then _
                lis.Add(New Filtro(TipoFiltro.CodigoArticulo, CType(txtCodArt.Text, String)))
            'DenominacionArticulo
            If Not String.IsNullOrEmpty(txtDenArt.Text) Then _
                lis.Add(New Filtro(TipoFiltro.DenominacionArticulo, CType(txtDenArt.Text, String)))

            FiltrosAnyadidos = lis
        End Sub

#End Region

        ''' Revisado por: blp. Fecha: 12/03/2012
        ''' <summary>
        ''' Establece las opciones de idioma/decimales/fechas/etc del usuario
        ''' </summary>
        ''' <remarks>Llamada desde Init. Máx. 0,1 seg.</remarks>
        Private Sub setCultureInfoUsuario()
            Me.ScriptMgr.EnableScriptGlobalization = True
            Me.ScriptMgr.EnableScriptLocalization = True

            setDateFormat(Me.whdgOfertasHistoricas.GridView.Columns.FromKey("Key_FECOFE"))

        End Sub

        ''' Revisado por blp. Fecha: 12/04/2012
        ''' <summary>
        ''' Dar el formato del usuario a las columnas numéricas del grid
        ''' </summary>
        ''' <param name="field">Columna seleccionada</param>
        ''' <remarks>Llamada desde setCultureInfoUsuario. 0,1 seg.</remarks>
        'Private Sub setNumberFormat(ByVal field As Infragistics.Web.UI.GridControls.BoundDataField)
        '    field.DataFormatString = "{n}"
        '    'field.DataFormatString = "{0:n" & FSPMUser.NumberFormat.NumberDecimalDigits & "}"
        'End Sub

        ''' Revisado por blp. Fecha: 12/04/2012
        ''' <summary>
        ''' Dar el formato del usuario a las columnas de fecha del grid
        ''' </summary>
        ''' <param name="field">columna seleccionada</param>
        ''' <remarks>Llamada desde setCultureInfoUsuario. 0,1 seg.</remarks>
        Private Sub setDateFormat(ByVal field As Infragistics.Web.UI.GridControls.BoundDataField)
            field.DataFormatString = "{0:" & FSPMUser.DateFormat.ShortDatePattern.ToString & "}"
        End Sub

    End Class
End Namespace