﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OfertasPDF.aspx.vb" Inherits="Fullstep.PMPortalWeb.OfertasPDF" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>OfertasPDF</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
	<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
	<meta content="JavaScript" name="vs_defaultClientScript">
</head>
<body>  
    <%--<form id="formOfertasPDF" name="formOfertasPDF" runat="server">
        <input id="strHTML" name="strHTML" type="hidden" runat="server" value="-1"/>
        <input id="strPDF" name="strPDF" type="hidden" runat="server" value="-1"/>
        <input id="strNifProve" name="strPDF" type="hidden" runat="server" value="-1"/>
        <input id="tipoFirma" name="tipoFirma" type="hidden" runat="server" value="-1"/>
        <input id="fila" name="fila" type="hidden" runat="server" value="-1"/>
        <table style="width:100% ; height: 20%">
                <tr>
                    <td>
                        <asp:Label id="lblSelecCert" Text="DSeleccione el certificado a utilizar para la firma digital" runat="server" CssClass="fntLogin"></asp:Label>
                    </td>
                </tr>
        </table>
        <div style="overflow: auto ; width:100% ; min-width:950px ; height: 60% ; max-height: 450px">
            <asp:Table id="tblCerts" runat="server" cellspacing="0" cellpadding ="0" style="cursor: default ; width:98% ; min-width:925px ; height: 100% ; max-height: 450px" CssClass="bordeado fntLogin">
                <asp:TableRow CssClass="ugfilatablaCabecera">
                    <asp:TableCell CssClass="bordeado">
                        <asp:Label id="lblIndiceSujCert" Text="DSujetoCertificado" runat="server"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell CssClass="bordeado">
                        <asp:Label id="lblIndiceEmiCert" Text="DEmisorCertificado" runat="server"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell CssClass="bordeado">
                        <asp:Label id="lblIndiceFecExpCert" Text="DFechaExpiracionCertificado" runat="server"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:table>
        </div>
        <table style="width:100% ;  height: 20%">
            <tr>
                <td>
                    <fsn:FSNButton id="btnContinuar" runat="server" Text="DContinuar" Alineacion="right" style="margin-right:10px;"></fsn:FSNButton>
                </td>
            </tr>
        </table>
    </form>
    <script type="text/javascript">

        function rowClick(_row) {
            var estiloHighLight
            var estiloActual
            estiloHighLight = "highlight"; 
            if (parseInt(document.getElementById('fila').value) > -1) {
                estiloActual=document.getElementById('tblCerts').childNodes[0].childNodes[parseInt(document.getElementById('fila').value) + 1].className
                //COMIENZO
                while (estiloActual.substring(0, estiloHighLight.length + 1) == estiloHighLight + " ") {
                    estiloActual = estiloActual.substring(estiloHighLight.length + 1, estiloActual.length);
                }
                //MEDIO
                while (estiloActual.match(" " + estiloHighLight + " ") != null) {
                    estiloActual = estiloActual.replace(" " + estiloHighLight + " ", " ");
                }                
                //FIN
                if (estiloActual.substring(estiloActual.length - estiloHighLight.length - 1, estiloActual.length) == " " + estiloHighLight) {
                    estiloActual = estiloActual.substring(0, estiloActual.length - estiloHighLight.length - 1);
                }
                //EXACTO
                if (estiloHighLight == estiloActual) {
                    estiloActual = "";
                }
                document.getElementById('tblCerts').childNodes[0].childNodes[parseInt(document.getElementById('fila').value) + 1].className = estiloActual;
            }
            document.getElementById('fila').value = _row;
            if (document.getElementById('tblCerts').childNodes[0].childNodes[_row + 1].className == "") {
                document.getElementById('tblCerts').childNodes[0].childNodes[_row + 1].className = estiloHighLight;
            }
            else{
                document.getElementById('tblCerts').childNodes[0].childNodes[_row + 1].className = document.getElementById('tblCerts').childNodes[0].childNodes[_row + 1].className + " " + estiloHighLight;
            }            
       }

    </script>--%>
</body>
</html>
