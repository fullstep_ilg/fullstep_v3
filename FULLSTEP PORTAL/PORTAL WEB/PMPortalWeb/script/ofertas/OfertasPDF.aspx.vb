﻿Imports ExpertPdf.HtmlToPdf
Imports System.IO
Imports SBPAdES
Imports SBPDF
Imports SBPDFSecurity
Imports SBCustomCertStorage
Imports SBX509
Imports SBHTTPSClient
Imports SBHTTPTSPClient
Imports SBCRLStorage

Imports SBPDFCore
Imports SBCRL
Imports SBOCSPClient
Imports SBWinCertStorage

Imports System.Diagnostics
Imports SBDC

Namespace Fullstep.PMPortalWeb
    Public Class OfertasPDF
        Inherits FSPMPage

        
        Protected Const FIN_OK As Integer = 1
        Protected Const FIN_KO As Integer = -1
        Protected Const SIN_FIRMA As Integer = 0
        Protected Const FIRMA_BASIC As Integer = 1
        Protected Const FIRMA_ENHACED_SELLO As Integer = 2
        Protected Const FIRMA_ENHACED_SELLO_OCSP As Integer = 3
        Protected sRutaHtml As String
        Protected sRutaPdf As String
        Protected iTipoFirma As Integer
        Protected sNifProve As String
        Protected iFilaElegida As Integer = -1
        Protected oCertificadosRoot As TElWinCertStorage
        Protected oCertificadoFirma As TElX509Certificate
        Protected iEstado As Integer = 0


        Protected output As MemoryStream

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Protected Sub InitializeComponent()

        End Sub

        Sub New()
            ' This call is required by the Windows Form Designer.
            InitializeComponent()
            ' Add any initialization after the InitializeComponent() call.
            SBUtils.Unit.SetLicenseKey("D87ED795C5A7644874C8FB5A6511DCD2E749149FBF075DDBA61EB8D5ABDDEE5A6E038CAFF7B58DB6DE7381FFCA43B3FAEFC85556CF183FE2C04E9F99298BE64B9919355F733B68027B1D74BF6B3F31766AC0625A5F35EEF8A769E09F6D8104A88F9F903AE94AA6D5B0533A8E3D3EB5D38CF0CE1B32A3C30FE6F920D0008827D0D044A482CD3301016067FB7C7673C3E19BF6C154FE1441516BAA080E8AEF03CA6688512CE01456DE00A8C1E73A283E4C1AFBC60202CAD4338A92D67DF70EA19FB05A4325104F8C5756DB2FE8D85D4642E93D6CDC328EA8C2A1229491E7C152651B2C92E29B48681A2494415DBB6A12F53F01F6EB2661CE26FB8CA8FD15E3EDCA") 'LICENCIA PARA: PDF & DC
            SBUtils.Unit.SetLicenseKey("4DB89E15AA2DC9085C31E6C8303C56121CFD151D9FF51C33B5D33A8D1C5F0C4263612478226CA8F1CC81215324826699CED43A570EEACB5FBA63F1CDF2959C0F28B1CA4FE8A18F228D051815985EDEB1359027EC50476A03875EA9EF1FE77D3EDD2BACA26DB1522C825CDCC1B60C86AFE66B96E10051050555466D35A61199E4BC1F0B49D927BD7C3D7584C1C8D79C6191874F094E51D8E3C123C3C88098EE23D142FF7430A68DB832593ED64CA8F439966CEA5DBB6605156A5F077AA5AEC95778915674EBF3364712AEEA8AE4212ACCB0074CF143BFB85A3B33A5FB4B8B4AF80D1DC4537C20C1F5A859DCB62080654D26E2FB3ACAD48A6C9DA8538E23F95F73") 'LICENCIA PARA: PKI
            SBPDF.Unit.Initialize()
            SBPAdES.Unit.Initialize()
            SBPDFSecurity.Unit.Initialize()
        End Sub

        ''' Revisado por:auv. Fecha:03/06/2013
        ''' <summary>
        ''' PreCargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
        Protected Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            Me.ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.OfertasPDF

            InitializeComponent()
        End Sub

        ''' Revisado por: auv. Fecha:17/01/2013
        ''' <summary>
        ''' Carga de la pagina
        ''' </summary>
        ''' <param name="sender">Control que lanza el evento de carga</param>
        ''' <param name="e">argumentos del evento</param>
        ''' <remarks>Llamada desde el evento Load de la página. Máx inferior a 1 seg.</remarks>
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

            For i As Integer = 0 To Session.Count - 1
                System.Diagnostics.Debug.WriteLine(Session.Keys(i).ToString() + " - " + Session(i).ToString())
            Next


            Dim a As System.Web.HttpCookieCollection
            a = Request.Cookies
            System.Diagnostics.Debug.WriteLine("Request")
            System.Diagnostics.Debug.WriteLine("----------")
            For j As Integer = 0 To a.Count - 1
                System.Diagnostics.Debug.WriteLine("Name - " + a(j).Name)
                System.Diagnostics.Debug.WriteLine("Path - " + a(j).Path)
                System.Diagnostics.Debug.WriteLine("Domain - " + a(j).Domain)
                System.Diagnostics.Debug.WriteLine("Value - " + a(j).Value)

                System.Diagnostics.Debug.WriteLine("---")
            Next

            Dim b As System.Web.HttpCookieCollection
            b = Response.Cookies
            System.Diagnostics.Debug.WriteLine("Response")
            System.Diagnostics.Debug.WriteLine("----------")
            For k As Integer = 0 To b.Count - 1
                System.Diagnostics.Debug.WriteLine("Name - " + b(k).Name)
                System.Diagnostics.Debug.WriteLine("Path - " + b(k).Path)
                System.Diagnostics.Debug.WriteLine("Domain - " + b(k).Domain)
                System.Diagnostics.Debug.WriteLine("Value - " + b(k).Value)
                System.Diagnostics.Debug.WriteLine("---")
            Next

            If Not (Request.Url.Query().StartsWith("?&") Or ((Not IsNothing(HttpUtility.ParseQueryString(Request.Url.Query())("result"))) AndAlso CStr(HttpUtility.ParseQueryString(Request.Url.Query())("result")) = "1")) Then
                If Not IsPostBack Then
                    sRutaHtml = HttpUtility.ParseQueryString(Request.Url.Query())("rutaHtml")
                    sRutaPdf = HttpUtility.ParseQueryString(Request.Url.Query())("rutaPdf")
                    Try
                        If sRutaHtml = "" Or sRutaPdf = "" Then
                            Throw New Exception()
                        End If
                        If (Not File.Exists(sRutaHtml)) Or (Not File.Exists(sRutaPdf)) Then
                            Throw New Exception()
                        End If
                        Me.crearPDF()
                        Select Case Me.obtCasoDeFirmado()
                            Case SIN_FIRMA
                                Me.finalizar(FIN_OK)
                                iEstado = FIN_OK
                            Case FIRMA_BASIC, FIRMA_ENHACED_SELLO, FIRMA_ENHACED_SELLO_OCSP
                            Case Else
                                Me.finalizar(FIN_OK)
                                iEstado = FIN_OK
                        End Select
                    Catch ex As Exception
                        iEstado = FIN_KO
                        Me.finalizar(FIN_KO)
                    End Try
                Else
                End If
            Else
            End If
        End Sub

        ''' Revisado por: auv. Fecha:06/03/2013
        ''' <summary>
        ''' Este procedimiento coge el fichero html de sRutaHtml y lo convierte a un fichero pdf en la ruta sRutaPdf.
        ''' Para ello usa el componente ExpertPDF.
        ''' </summary>
        ''' <remarks>Llamada desde:Page_load; Tiempo máximo=0,2</remarks>
        Protected Sub crearPDF()
            Dim pdfConverter As ExpertPdf.HtmlToPdf.PdfConverter = New PdfConverter()
            Dim pdfDocument As PdfDocument.Document
            Dim arrByte As Byte()

            pdfConverter.LicenseKey = "HzQtPyc/LCYvPygxLz8sLjEuLTEmJiYm"    'Le pasamos la clave de licencia
            pdfConverter.PdfDocumentOptions.LeftMargin = 15
            pdfConverter.PdfDocumentOptions.RightMargin = 15
            pdfConverter.PdfDocumentOptions.TopMargin = 15
            pdfConverter.PdfDocumentOptions.BottomMargin = 15

            Try
                pdfConverter.ScriptsEnabled = True 'Para que no elimine los scripts del fichero sRutaHtml
                pdfConverter.ScriptsEnabledInImage = True
                pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlFile(sRutaHtml) 'Se construye un objeto PdfDocument.Document a partir del fichero de sRutaHtml
                arrByte = pdfDocument.Save() 'Se salva el objeto pdf a un array de bytes
                File.Delete(sRutaPdf) 'Se borra el fichero preexistente
                File.WriteAllBytes(sRutaPdf, arrByte) 'Se escribe el fichero pdf
            Catch ex As Exception
                Throw New Exception()
            End Try
            pdfConverter = Nothing
            pdfDocument = Nothing
        End Sub

        ''' Revisado por: auv. Fecha:06/03/2013
        ''' <summary>
        ''' Obtiene el valor del parámetro interno FIRMA_DIG_OFERTAS
        ''' </summary>
        ''' <remarks>Llamada desde:Page_load; Tiempo máximo=0,2</remarks>
        Protected Function obtCasoDeFirmado() As Integer
            Dim oOfertas As PMPortalServer.Ofertas = FSPMServer.Get_Ofertas
            oOfertas.LoadDataPargenInterno(IdCiaComp, IdCia)
            Return oOfertas.PargenInternoFirmaDigOfe
        End Function

        ''' Revisado por: auv. Fecha:06/03/2013
        ''' <summary>
        ''' Obtiene el valor del parámetro interno FIRMA_DIG_OFERTAS
        ''' </summary>
        ''' <param name="iTipoFin">Si se ha concluido con éxito la generación del Pdf y su firma o no.</param>
        ''' <remarks>Llamada desde:Page_load; Tiempo máximo=0,2</remarks>
        Protected Sub finalizar(iTipoFin As Integer)
            If File.Exists(Path.ChangeExtension(sRutaHtml, ".tmp")) Then
                File.Delete(Path.ChangeExtension(sRutaHtml, ".tmp"))
            End If
            Select Case iTipoFin
                'Si se finaliza apropiadamente (OK), se borra exclusivamente el fichero sRutaHtml, que es a lo que está
                'esperando la parte Asp classic, a que desaparezca el fichero Html para poder continuar.
                'Si se finaliza inadecuadamente(cualquier cosa menos OK), se borra primero el fichero sRutaPdf y luego
                'se borra el fichero sRutaHtml. Así el Asp classic sabrá que al no existir el fichero sRutaPdf algo 
                'funcionó mal
                Case FIN_OK
                    If File.Exists(sRutaHtml) Then
                        File.Delete(sRutaHtml)
                    End If
                Case Else
                    If File.Exists(sRutaPdf) Then
                        File.Delete(sRutaPdf)
                    End If
                    If File.Exists(sRutaHtml) Then
                        File.Delete(sRutaHtml)
                    End If
            End Select
        End Sub

        Protected Sub FinishSigning(ByRef fileName As String, ByRef signature As Byte())

            Dim state As New TElDCAsyncState()
            Dim input As New MemoryStream(signature)

            state.LoadFromStream(input, SBDCXMLEnc.__Global.DCXMLEncoding())

            Dim doc As New TElPDFDocument()
            Dim handler As New TElPDFAdvancedPublicKeySecurityHandler()

            handler.PAdESSignatureType = TSBPAdESSignatureType.pastBasic
            handler.AutoCollectRevocationInfo = False
            handler.IgnoreChainValidationErrors = False
            handler.SignatureSizeEstimationStrategy = TSBPAdESSignatureSizeEstimationStrategy.psesBasic

            Dim Fich = New System.IO.FileStream(fileName, FileMode.Open, FileAccess.ReadWrite)
            Try
                doc.CompleteAsyncOperation(Fich, state, handler)
            Catch ex As Exception
                Throw New Exception()
            Finally
                Fich.Close()
            End Try

            File.Copy(fileName, Path.ChangeExtension(fileName, ".pdf"), True)
            fileName = Path.ChangeExtension(fileName, ".pdf")
        End Sub

    End Class
End Namespace