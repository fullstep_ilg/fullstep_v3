﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OfertasPDF_Java.aspx.vb" Inherits="Fullstep.PMPortalWeb.OfertasPDF_Java"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
	<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
	<meta content="JavaScript" name="vs_defaultClientScript">
</head>
<body> 
    <div id="controlJava" align="center" runat="server">
        <br />
        <br />
        <applet id="signerJava" archive="com.secureblackbox.jar" 
        code="Applet.com.secureblackbox.dc.applet.ElDCServerApplet" width="500" height="240"> 
            <param id="paramDataJava" runat="server" name="Data" enableviewstate="false"/>
            <param id="paramIdJava" runat="server" name="SessionID" enableviewstate="false"/>
            <param name="Color" value="#FFFFFF" />
            <param name="DataURL" value="./OfertasPDF_Java.aspx?result=1" /><%--./Result.aspx?result=1--%>
            <param id="sAlias" runat="server" name="txtAlias" enableviewstate="false"/>
            <param id="sNombre" runat="server" name="txtNombre" enableviewstate="false"/>
            <param id="sEmisor" runat="server" name="txtEmisor" enableviewstate="false"/>
            <param id="sExpira" runat="server" name="txtExpira" enableviewstate="false"/>
            <param id="sContinuar" runat="server" name="txtContinuar" enableviewstate="false"/>
            <param id="sFinalizar1" runat="server" name="txtbtnFinalizar" enableviewstate="false"/>
            <param id="sFinalizar2" runat="server" name="txtlblFinalizar" enableviewstate="false"/>
            <param id="sFirmaOK" runat="server" name="txtFirmaOK" enableviewstate="false"/>
            <param id="sFirmaKO" runat="server" name="txtFirmaKO" enableviewstate="false"/>
            <param id="sKeyPassword" runat="server" name="txtKeyPassword" enableviewstate="false"/>
            <param id="sSeleccione" runat="server" name="txtSeleccione" enableviewstate="false"/>
            <param id="sLoading" runat="server" name="txtLoading" enableviewstate="false"/>
            <param id="sSigningSending" runat="server" name="txtSigningSending" enableviewstate="false"/>
            <%--<param name="GoURL" value="./Result.aspx" />--%>
            <%--<param name="PKCS11Registry" value="\Software\EldoS\SecureBlackbox\DC" />--%>
        </applet>
    </div>
    <%--<form id="formOfertasPDF" name="formOfertasPDF" runat="server">
        <input id="strHTML" name="strHTML" type="hidden" runat="server" value="-1"/>
        <input id="strPDF" name="strPDF" type="hidden" runat="server" value="-1"/>
        <input id="strNifProve" name="strPDF" type="hidden" runat="server" value="-1"/>
        <input id="tipoFirma" name="tipoFirma" type="hidden" runat="server" value="-1"/>
        <input id="fila" name="fila" type="hidden" runat="server" value="-1"/>
        <table style="width:100% ; height: 20%">
                <tr>
                    <td>
                        <asp:Label id="lblSelecCert" Text="DSeleccione el certificado a utilizar para la firma digital" runat="server" CssClass="fntLogin"></asp:Label>
                    </td>
                </tr>
        </table>
        <div style="overflow: auto ; width:100% ; min-width:950px ; height: 60% ; max-height: 450px">
            <asp:Table id="tblCerts" runat="server" cellspacing="0" cellpadding ="0" style="cursor: default ; width:98% ; min-width:925px ; height: 100% ; max-height: 450px" CssClass="bordeado fntLogin">
                <asp:TableRow CssClass="ugfilatablaCabecera">
                    <asp:TableCell CssClass="bordeado">
                        <asp:Label id="lblIndiceSujCert" Text="DSujetoCertificado" runat="server"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell CssClass="bordeado">
                        <asp:Label id="lblIndiceEmiCert" Text="DEmisorCertificado" runat="server"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell CssClass="bordeado">
                        <asp:Label id="lblIndiceFecExpCert" Text="DFechaExpiracionCertificado" runat="server"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:table>
        </div>
        <table style="width:100% ;  height: 20%">
            <tr>
                <td>
                    <fsn:FSNButton id="btnContinuar" runat="server" Text="DContinuar" Alineacion="right" style="margin-right:10px;"></fsn:FSNButton>
                </td>
            </tr>
        </table>
    </form>--%>
    <%--<script type="text/javascript">

        function rowClick(_row) {
            var estiloHighLight
            var estiloActual
            estiloHighLight = "highlight"; 
            if (parseInt(document.getElementById('fila').value) > -1) {
                estiloActual=document.getElementById('tblCerts').childNodes[0].childNodes[parseInt(document.getElementById('fila').value) + 1].className
                //COMIENZO
                while (estiloActual.substring(0, estiloHighLight.length + 1) == estiloHighLight + " ") {
                    estiloActual = estiloActual.substring(estiloHighLight.length + 1, estiloActual.length);
                }
                //MEDIO
                while (estiloActual.match(" " + estiloHighLight + " ") != null) {
                    estiloActual = estiloActual.replace(" " + estiloHighLight + " ", " ");
                }                
                //FIN
                if (estiloActual.substring(estiloActual.length - estiloHighLight.length - 1, estiloActual.length) == " " + estiloHighLight) {
                    estiloActual = estiloActual.substring(0, estiloActual.length - estiloHighLight.length - 1);
                }
                //EXACTO
                if (estiloHighLight == estiloActual) {
                    estiloActual = "";
                }
                document.getElementById('tblCerts').childNodes[0].childNodes[parseInt(document.getElementById('fila').value) + 1].className = estiloActual;
            }
            document.getElementById('fila').value = _row;
            if (document.getElementById('tblCerts').childNodes[0].childNodes[_row + 1].className == "") {
                document.getElementById('tblCerts').childNodes[0].childNodes[_row + 1].className = estiloHighLight;
            }
            else{
                document.getElementById('tblCerts').childNodes[0].childNodes[_row + 1].className = document.getElementById('tblCerts').childNodes[0].childNodes[_row + 1].className + " " + estiloHighLight;
            }            
       }

    </script>--%>
</body>
</html>
