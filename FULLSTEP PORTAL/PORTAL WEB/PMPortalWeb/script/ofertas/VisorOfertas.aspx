﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="visorOfertas.aspx.vb"
    Inherits="Fullstep.PMPortalWeb.visorOfertas" %>

<%@ Register TagPrefix="pag" Src="~/script/_common/Paginador.ascx" TagName="Paginador" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
    <title></title>
    <meta content="True" name="vs_showGrid">
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <script src="../../../common/formatos.js"></script>
    <script src="../../../common/menu.asp"></script>
    <script type="text/javascript">
        /*''' <summary>
        ''' Iniciar la pagina.
        ''' </summary>     
        ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
        function Init() {
            document.getElementById('tablemenu').style.display = 'block';
        }
    </script>
</head>
<body class="common" onload="Init()">
    <script type="text/javascript">
        /*<%--Si se modifican las opciones del usuario, esta página requiere refresco--%>*/
        var requiereRefresco
        requiereRefresco = true
        /*<%-- 
        Revisado por: blp. fecha: 13/03/2012
        Recargamos la página para que se carguen los cambios en la configuarión del usuario
        vdec:   Separador de decimales
        vthou:  Separador de miles
        vprec:  Número de decimales
        vdate:  Formato fecha
        Llamada desde WEB PORTAL/script/common/opciones.asp. Máx: 0,01 seg
        --%>*/
        function aplicarFormatos(vdec, vthou, vprec, vdate) {
            window.location.reload();
        }

        /*<%--
        ''' Revisado por: blp. Fecha: 22/11/2011
        ''' <summary>
        ''' Function que comprueba el valor del control pasado. Si es numérico lo devuelve, si no, no.
        ''' </summary>
        ''' txt: control que lanza el evento
        ''' <remarks>Llamada desde evento onchange de controles textbox; Tiempo máximo:0,1seg.</remarks>--%>*/
        function validarsolonum(txt) {
            var str = '';
            for (var i = 0; i < txt.value.length; i++) {
                if (txt.value.charCodeAt(i) >= 48 && txt.value.charCodeAt(i) <= 57)
                    str += txt.value.charAt(i);
            }
            txt.value = str;
        }

        /*<%--
        ''' Revisado por: blp. Fecha: 22/11/2011
        ''' <summary>
        ''' Function que comprueba que el valor tecleado es numérico. Si no lo es, cancela el evento.
        ''' </summary>
        ''' <remarks>Llamada desde evento onKeyPress de controles textbox; Tiempo máximo:0,1seg.</remarks>--%>*/
        function validarkeynum() {
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.returnValue = false;
            }
        }

        /*<%--
        ''' Revisado por: blp. Fecha: 22/11/2011
        ''' <summary>
        ''' Function que comprueba que los valore pegados son numéricos. Si no lo son, impide el pagado
        ''' </summary>
        ''' <remarks>Llamada desde evento onpaste de controles textbox; Tiempo máximo:0,1seg.</remarks>--%>*/
        function validarpastenum() {
            var texto = window.clipboardData.getData('Text');
            var str = '';
            for (var i = 0; i < texto.length; i++) {
                if (texto.charCodeAt(i) >= 48 && texto.charCodeAt(i) <= 57)
                    str += texto.charAt(i);
            }
            if (texto != str) event.returnValue = false;
        }

        //AJAX

        var x = 0;
        var y = 0;
        var xmlHttp;
        var ColCheck = 0;

        //Creamos el objeto xmlHttpRequest
        CreateXmlHttp();

        /*<%--Revisado por: blp. Fecha: 14/12/2011
        ''' <summary>
        ''' Function que crea el objeto para la llamada AJAX
        ''' </summary>
        ''' <remarks>Llamada desde; Tiempo máximo:0seg.</remarks>--%>*/
        function CreateXmlHttp() {
            // Probamos con IE
            try {
                // Funcionará para JavaScript 5.0
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (oc) {
                    xmlHttp = null;
                }
            }

            // Si no se trataba de un IE, probamos con esto
            if (!xmlHttp && typeof XMLHttpRequest != "undefined") {
                xmlHttp = new XMLHttpRequest();
            }

            return xmlHttp;
        }

        /*<%--
        Revisado por: blp. Fecha: 14/12/2011
        Descripcion:Evento que salta cuando pinchamos en los filtros de la grid.
        Sirve para que tenga una "memoria" de los datos del filtro para evitar ir la 2ª vez a memoria.
        Parametros:
        gridName:= Id de la grid:
        oColumFilter:= inf. de la columna que filtras
        oColum:=inf de la columna
        workingFilterList:=lista de los datos del filtro
        lastKnownFilterList:=Datos del filtro
        tiempo ejecucion:=0seg.
        --%>*/
        function uwgOfertasHistoricas_BeforeFilterPopulated(gridName, oColumnFilter, oColumn, workingFilterList, lastKnownFilterList) {
            // if we have a lastKnownFilterList then we have already acquired the data, so if it's the customerId column
            // just reuse the object.
            if (lastKnownFilterList) {
                // use this method to set the lastknown list to the working list.
                oColumnFilter.setWorkingFilterList(lastKnownFilterList);
                // returning true will stop the WebGrid from calling the server for data
                return true;
            }
        }

        /*<%--Revisado por: blp. Fecha: 14/12/2011
        <summary>
        Provoca el postback para hacer la esportación a excel
        </summary>
        <remarks>Llamada desde: Icono de exportar a Excel</remarks>
        --%>*/
        function MostrarExcel() {
            __doPostBack("", "Excel");
        }

        /*
        //<%'Revisado por: auv. Fecha: 23/01/2013%>
        Se lanza al pulsar el botón limpiar.
        */
        function btnLimpiarClick() {
            //            var oTxtFecEnvOfeDesde = $get("<%=txtFecEnvOfeDesde.clientID %>")
            //            oTxtFecEnvOfeDesde.value = "";
            //            var oTxtFecEnvOfeHasta = $get("<%=txtFecEnvOfeHasta.clientID %>")
            //            oTxtFecEnvOfeHasta.value = "";
            var oDdAnyo = $get("<%=ddAnyo.clientID %>")
            oDdAnyo.value = "";
            var oDdGMA = $get("<%=ddGMA.clientID %>")
            oDdGMA.value = "";
            var oTxtCod = $get("<%=txtCod.clientID %>")
            oTxtCod.value = "";
            var oTxtDen = $get("<%=txtDen.clientID %>")
            oTxtDen.value = "";
            var oTxtCodArt = $get("<%=txtCodArt.clientID %>")
            oTxtCodArt.value = "";
            var oTxtDenArt = $get("<%=txtDenArt.clientID %>")
            oTxtDenArt.value = "";
            return false;
        }

        //<%'Revisado por: blp. Fecha: 20/01/2012%>
        //<%'<summary>%>
        //<%'lblBusqueda y cpeBusquedaAvanzada no pueden estar visibles al mismo tiempo %>
        //<%'Mostramos el panel resumen de los filtros (divBusqueda) cuando se oculta el panel de búsqueda y lo ocultamos cuando este último se muestra%>
        //<%'</summary>%>
        //<%'<remarks>Llamada desde el evento onClick de cliente del control pnlCabeceraRecepcion. Max. 0,1 seg.</remarks>%>
        function checkCollapseStatus() {
            var oControlcpeBusqueda = $find("<%=cpeBusquedaAvanzada.clientID %>")
            var oControllblBusqueda = $get("<%=divBusqueda.clientID %>")
            if (oControlcpeBusqueda) {
                if (oControlcpeBusqueda.get_Collapsed() == true) {
                    oControllblBusqueda.style.display = "none"
                }
                else {
                    oControllblBusqueda.style.display = "block"
                }
            }
            oControlcpeBusqueda = null;
            oControllblBusqueda = null;
        }

        /*<%--Revisado por: blp. Fecha: 30/01/2012
        <summary>
        Método necesario para ordenar las columnas del grid
        </summary>
        <remarks>Llamada desde: evento ColumnSorting del grid. Máx. 0,1 seg.</remarks>
        --%>*/
        function whdgOfertasHistoricas_Sorting_ColumnSorting(grid, args) {
            var sorting = grid.get_behaviors().get_sorting();
            var col = args.get_column();
            var sortedCols = sorting.get_sortedColumns();
            for (var x = 0; x < sortedCols.length; ++x) {
                if (col.get_key() == sortedCols[x].get_key()) {
                    args.set_clear(false);
                    break;
                }
            }
        }

        /*
        //<%'Revisado por: auv. Fecha: 22/01/2013%>
        Se lanza al pulsar una celda de la grid. Dependiendo de la celda pulsada, realizará una acción u otra.
        */
        function grid_CellClick(sender, e) {
            if (e.get_type() == "cell") {
                var row = row = e._props[1]._row;
                if (0 <= e._props[1]._row._address && e._props[1]._row._address < e._grid._rows.get_length() && 0 <= e._props[1]._column._address && e._props[1]._column._address < e._grid._columns.get_length()) {
                    if (e._props[1]._column._address == row.get_cellByColumnKey("Key_DETALLE")._address) {
                        if (!(typeof row.get_cellByColumnKey("Key_DETALLE").get_value() === "undefined")) {
                            if (!(row.get_cellByColumnKey("Key_DETALLE").get_value() == null)) {
                                if (!(row.get_cellByColumnKey("Key_DETALLE").get_value() == "")) {
                                    oPar = new Object();
                                    oPar.IDAdjun = row.get_cellByColumnKey("Key_ADJUNID").get_value();
                                    oPar.Detalle = row.get_cellByColumnKey("Key_DETALLE").get_value();
                                    //__doPostBack("PDF", JSON.stringify(oPar)); //Se deja esta instrucción comentada porque por algún motivo no funcionan las llamadas a JSON
                                    __doPostBack("PDF", oPar.IDAdjun + "#" + oPar.Detalle);
                                }
                            }
                        }
                    }
                }
            }
        }

        var ModalProgress = '<%=ModalProgress.ClientID%>';
    </script>
    <iframe id="iframeWSServer" style="z-index: 106; left: 8px; visibility: hidden; position: absolute;
        top: 200px" name="iframeWSServer" src="<%=Application("RUTASEGURA")%>script/blank.htm">
    </iframe>
    <script>
        dibujaMenu(3)
    </script>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="smOfertasHistoricas" runat="server" EnablePageMethods="true">
        <Services>
            <asp:ServiceReference Path="~/Consultas.asmx" />
        </Services>
    </asp:ScriptManager>
    <%--BÚSQUEDA GENERAL--%>
    <table width="100%" cellpadding="0" border="0">
        <tr>
            <td width="100%">
                <img src="images/ofertas_historicas.jpg" />
                <asp:Label ID="lblHistoricoOfertas" Text="DHistorico de ofertas" runat="server" class="rotuloGrande"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="100%">
                <table cellpadding="0" border="0" id="tblBusquedaGeneral" runat="server">
                    <tr>
                        <td>
                            <asp:Label ID="lblFecEnvOfeDesde" runat="server" Text="DFecha de envío de la oferta(desde - hasta):"
                                CssClass="Normal"></asp:Label>
                        </td>
                        <td>
                            <%--<igpck:WebDatePicker runat="server" ID="dpFechaLimCumplim" NullText="" SkinID="Calendario"></igpck:WebDatePicker>--%>
                            <fsde:GeneralEntry ID="txtFecEnvOfeDesde" runat="server" Width="118px" DropDownGridID="txtFecEnvOfeDesde"
                                Tipo="TipoFecha" Tag="txtFecEnvOfeDesde" Independiente="1" Height="22px">
                                <InputStyle CssClass="CajaTexto" Width="100px"></InputStyle>
                            </fsde:GeneralEntry>
                        </td>
                        <td>
                            <asp:Label ID="lblFecEnvOfeHasta" runat="server" Text="D-" CssClass="Normal"></asp:Label>
                        </td>
                        <td>
                            <fsde:GeneralEntry ID="txtFecEnvOfeHasta" runat="server" Width="118px" DropDownGridID="txtFecEnvOfeHasta"
                                Tipo="TipoFecha" Tag="txtFecEnvOfeHasta" Independiente="1" Height="22px">
                                <InputStyle CssClass="CajaTexto" Width="100px"></InputStyle>
                            </fsde:GeneralEntry>
                        </td>
                        <td width="20px">
                        </td>
                        <td>
                            <fsn:FSNButton ID="btnBuscar" runat="server" Text="DBuscar" Alineacion="left" Style="margin-right: 10px;"></fsn:FSNButton>
                        </td>
                        <td width="20px">
                        </td>
                        <td>
                            <fsn:FSNButton ID="btnLimpiar" runat="server" Text="DLimpiar" Alineacion="Left" OnClientClick="btnLimpiarClick();return false;"></fsn:FSNButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <%-- Búsqueda Avanzada --%>
    <asp:Panel runat="server" ID="pnlBusquedaAvanzada" Style="cursor: pointer" Font-Bold="True"
        Height="20px" Width="100%" CssClass="PanelCabecera" onClick="checkCollapseStatus();">
        <asp:Image ID="imgExpandir" runat="server" SkinID="Contraer" />
        <asp:Label ID="lblBusquedaAvanzada" runat="server" Text=" DSeleccione los criterios de búsqueda generales"
            Font-Bold="True" ForeColor="White"></asp:Label>
    </asp:Panel>
    <asp:Panel ID="pnlParametros" runat="server" CssClass="Rectangulo" Width="100%">
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td>
                    <table id="tblBusquedaAvanzadaProcesosOutter" cellpadding="0" border="0" runat="server">
                        <tr>
                            <td>
                                <fieldset>
                                    <legend>
                                        <%=Textos(4) %></legend>
                                    <table id="tblBusquedaAvanzadaProcessosInner" cellpadding="0" border="0" runat="server">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblAnyo" runat="server" Text="DAnyo:" CssClass="Normal"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList CssClass="desplegable" ID="ddAnyo" DataTextField="Text" DataValueField="Value"
                                                    runat="server" Width="60px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblGMA" runat="server" Text="DGMA:" CssClass="Normal"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList CssClass="desplegable" ID="ddGMA" DataTextField="Text" DataValueField="Value"
                                                    runat="server" Width="60px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCod" runat="server" Text="DCódigo:" CssClass="Normal"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCod" CssClass="CajaTexto" runat="server" Width="100px" onchange="validarsolonum(this)"
                                                    onkeypress="return validarkeynum()" onpaste="validarpastenum()"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDen" runat="server" Text="DDenominación:" CssClass="Normal"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDen" CssClass="CajaTexto" runat="server" Width="305px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="tblBusquedaAvanzadaArticulos" cellpadding="0" border="0" runat="server">
                        <tr>
                            <td>
                                <asp:Label ID="lblCodArt" runat="server" Text="DCódigoArtículo:" CssClass="Normal"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCodArt" CssClass="CajaTexto" runat="server" Width="230px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="lblDenArt" runat="server" Text="DDenominaciónArtículo:" CssClass="Normal"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtDenArt" CssClass="CajaTexto" runat="server" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <cc1:CollapsiblePanelExtender ID="cpeBusquedaAvanzada" runat="server" CollapseControlID="pnlBusquedaAvanzada"
        ExpandControlID="pnlBusquedaAvanzada" SuppressPostBack="true" TargetControlID="pnlParametros"
        ImageControlID="imgExpandir" SkinID="FondoRojo" Collapsed="true">
    </cc1:CollapsiblePanelExtender>
    <div id="divBusqueda" runat="server" style="margin: 5px; background-color: #DCDCDC;">
        <asp:Label ID="lblBusqueda" runat="server"></asp:Label>
    </div>
    <%-- Grid --%>
    <asp:UpdatePanel ID="upOfertasHistoricas" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <ig:WebHierarchicalDataGrid ID="whdgOfertasHistoricas" runat="server" AutoGenerateColumns="false"
                Width="100%" ShowHeader="true" EnableAjax="false" EnableAjaxViewState="false"
                EnableDataViewState="false">
                <ClientEvents Click="grid_CellClick" />
                <Columns>
                    <ig:BoundDataField Key="Key_CODIGO" DataFieldName="CODIGO"></ig:BoundDataField>
                    <ig:BoundDataField Key="Key_DESCRIPCION" DataFieldName="DESCRIPCION"></ig:BoundDataField>
                    <ig:BoundDataField Key="Key_FECOFE" DataFieldName="FECOFE"></ig:BoundDataField>
                    <ig:BoundDataField Key="Key_NUMOFE" DataFieldName="NUMOFE"></ig:BoundDataField>
                    <ig:BoundDataField Key="Key_DETALLE" DataFieldName="DETALLE"></ig:BoundDataField>
                    <ig:BoundDataField Key="Key_ADJUNID" DataFieldName="ADJUNID"></ig:BoundDataField>
                </Columns>
                <Behaviors>
                    <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                    <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true" AnimationEnabled="false"></ig:Filtering>
                    <ig:Paging Enabled="true" PagerAppearance="Top">
                        <PagerTemplate>
                            <div id="divContenedor" style="display: inline; height: 100%">
                                <pag:Paginador ID="wdgPaginador" runat="server" PaintTotalRecord="false" ShowAlwaysPag="true" />
                            </div>                            
                        </PagerTemplate>
                    </ig:Paging>
                    <ig:VirtualScrolling Enabled="false"></ig:VirtualScrolling>
                    <ig:Sorting Enabled="true" SortingMode="Multi">
                        <SortingClientEvents ColumnSorting="whdgOfertasHistoricas_Sorting_ColumnSorting" />
                    </ig:Sorting>
                </Behaviors>
                <GroupingSettings EnableColumnGrouping="True" GroupAreaVisibility="Visible" />
            </ig:WebHierarchicalDataGrid>
            <div id="div_Label" runat="server" class="EtiquetaScroll" style="display: none; position: absolute">
                <asp:Label ID="label2" runat="server" Font-Bold="true"></asp:Label>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <fsn:FSNPanelInfo ID="FSNPanelDatosReceptor" runat="server" ServicePath="~/Consultas.asmx"
        ServiceMethod="Obtener_DatosReceptor" TipoDetalle="0">
    </fsn:FSNPanelInfo>
    <script type="text/javascript" src="../_common/js/jsUpdateProgress.js"></script>
    <cc1:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress" Style="display: none">
        <div style="position: relative; top: 30%; text-align: center;">
            <asp:Image ID="ImgProgress" runat="server" SkinID="ImgProgress" />
            <asp:Label ID="LblProcesando" runat="server" ForeColor="Black" Text="Cargando ..."></asp:Label>
        </div>
    </asp:Panel>
    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    <ig:WebDocumentExporter ID="WebDocumentExporter1" runat="server">
    </ig:WebDocumentExporter>
    <ig:WebExcelExporter ID="WebExcelExporter1" runat="server">
    </ig:WebExcelExporter>
    </form>
    <script type="text/javascript">

        checkCollapseStatus();

    </script>
</body>
</html>
