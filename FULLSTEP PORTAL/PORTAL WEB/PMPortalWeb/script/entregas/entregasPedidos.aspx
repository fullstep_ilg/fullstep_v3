﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="entregasPedidos.aspx.vb" Inherits="Fullstep.PMPortalWeb.entregasPedidos"%>
<%@ Register TagPrefix="pag" Src="~/script/_common/Paginador.ascx" TagName="Paginador"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
    <head id="Head1" runat="server">
	    <title></title>
	    <meta content="True" name="vs_showGrid">
	    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
	    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
	    <meta content="JavaScript" name="vs_defaultClientScript">
	    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	    <script src="../../../common/formatos.js"></script>
	    <script src="../../../common/menu.asp"></script>
        <script type="text/javascript">
            /*''' <summary>
            ''' Iniciar la pagina.
            ''' </summary>     
            ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
            function Init() {
                if (accesoExterno == 0) {document.getElementById('tablemenu').style.display = 'block';}
            }
        </script>
    </head>
    <body class="common" onload="Init()">
    <script type="text/javascript">
        /*<%-- 
        Revisado por: blp. fecha: 13/03/2012
        Recargamos la página para que se carguen los cambios en la configuarión del usuario
        vdec:   Separador de decimales
        vthou:  Separador de miles
        vprec:  Número de decimales
        vdate:  Formato fecha
        Llamada desde WEB PORTAL/script/common/opciones.asp. Máx: 0,01 seg
        --%>*/
        function aplicarFormatos(vdec, vthou, vprec, vdate) {
            window.location.reload();
        }
        /*<%--
        ''' Revisado por: blp. Fecha: 22/11/2011
        ''' <summary>
        ''' Function que comprueba el valor del control pasado. Si es numérico lo devuelve, si no, no.
        ''' </summary>
        ''' txt: control que lanza el evento
        ''' <remarks>Llamada desde evento onchange de controles textbox; Tiempo máximo:0,1seg.</remarks>--%>*/
        function validarsolonum(txt) {
            var str = '';
            for (var i = 0; i < txt.value.length; i++) {
                if (txt.value.charCodeAt(i) >= 48 && txt.value.charCodeAt(i) <= 57)
                    str += txt.value.charAt(i);
            }
            txt.value = str;
        }

        /*<%--
        ''' Revisado por: blp. Fecha: 22/11/2011
        ''' <summary>
        ''' Function que comprueba que el valor tecleado es numérico. Si no lo es, cancela el evento.
        ''' </summary>
        ''' <remarks>Llamada desde evento onKeyPress de controles textbox; Tiempo máximo:0,1seg.</remarks>--%>*/
        function validarkeynum() {
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.returnValue = false;
            }
        }

        /*<%--
        ''' Revisado por: blp. Fecha: 22/11/2011
        ''' <summary>
        ''' Function que comprueba que los valore pegados son numéricos. Si no lo son, impide el pagado
        ''' </summary>
        ''' <remarks>Llamada desde evento onpaste de controles textbox; Tiempo máximo:0,1seg.</remarks>--%>*/
        function validarpastenum() {
            var texto = window.clipboardData.getData('Text');
            var str = '';
            for (var i = 0; i < texto.length; i++) {
                if (texto.charCodeAt(i) >= 48 && texto.charCodeAt(i) <= 57)
                    str += texto.charAt(i);
            }
            if (texto != str) event.returnValue = false;
        }
        /*<%--
        ''' Revisado por: blp. Fecha: 02/11/2011
        ''' <summary>
        ''' Function que muestra el botón de guardado de la configuración del grid de entregas, cuando se modifica el ancho de alguna columna.
        ''' Taqmbién modifica el ancho de las columnos para evitar el redimensionamiento de los an
        ''' </summary>
        ''' sender  is the object which is raising the event
        ''' e is the ColumnResizedEventArgs
        ''' <remarks>Llamada desde evento columnResized del control whdgEntregas; Tiempo máximo:0,1seg.</remarks>--%>*/
        function whdgEntregas_anchoModificado(sender, e) {
            document.getElementById("<%=btnGuardarConfiguracion.clientID%>").style.display = "block";
        }

        /*<%--
        ''' Revisado por: blp. Fecha: 02/11/2011
        ''' <summary>
        ''' Function que oculta el botón btnGuardarConfiguracion
        ''' </summary>
        ''' <remarks>Llamada desde btnGuardarConfiguracion; Tiempo máximo:0,1seg.</remarks>--%>*/
        function ocultarBtnGuardarConfiguracion() {
            document.getElementById("<%=btnGuardarConfiguracion.clientID%>").style.display = "none";
        }

        //AJAX
    
            var x = 0;
            var y = 0;        
            var xmlHttp;
            var ColCheck=0;
        
            //Creamos el objeto xmlHttpRequest
	        CreateXmlHttp();
    
        /*<%--Revisado por: blp. Fecha: 14/12/2011
        ''' <summary>
        ''' Function que crea el objeto para la llamada AJAX
        ''' </summary>
        ''' <remarks>Llamada desde; Tiempo máximo:0seg.</remarks>--%>*/        
        function CreateXmlHttp()
        {
	        // Probamos con IE
	        try
	        {
		        // Funcionará para JavaScript 5.0
		        xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	        }
	        catch(e)
	        {
		        try
		        {
			        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		        }
		        catch(oc)
		        {   
			        xmlHttp = null;
		        }
	        }

	        // Si no se trataba de un IE, probamos con esto
	        if(!xmlHttp && typeof XMLHttpRequest != "undefined")
	        {
		        xmlHttp = new XMLHttpRequest();
	        }

	        return xmlHttp;
        }

        /*<%--
            Revisado por: blp. Fecha: 14/12/2011
            Descripcion:Evento que salta cuando pinchamos en los filtros de la grid.
            Sirve para que tenga una "memoria" de los datos del filtro para evitar ir la 2ª vez a memoria.
            Parametros:
            gridName:= Id de la grid:
            oColumFilter:= inf. de la columna que filtras
            oColum:=inf de la columna
            workingFilterList:=lista de los datos del filtro
            lastKnownFilterList:=Datos del filtro
            tiempo ejecucion:=0seg.
        --%>*/
        function uwgEntregas_BeforeFilterPopulated(gridName, oColumnFilter, oColumn, workingFilterList, lastKnownFilterList) {
            // if we have a lastKnownFilterList then we have already acquired the data, so if it's the customerId column
            // just reuse the object.
            if (lastKnownFilterList) {
                // use this method to set the lastknown list to the working list.
                oColumnFilter.setWorkingFilterList(lastKnownFilterList);
                // returning true will stop the WebGrid from calling the server for data
                return true;
            }
        }

        /*<%--Revisado por: blp. Fecha: 14/12/2011
        <summary>
        Provoca el postback para hacer la esportación a excel
        </summary>
        <remarks>Llamada desde: Icono de exportar a Excel</remarks>
        --%>*/
        function MostrarExcel() {
            __doPostBack("", "Excel");
        }

        /*<%--Revisado por: blp. Fecha: 14/12/2011
        <summary>
        Provoca el postback para hacer la exportación a Pdf
        </summary>
        <remarks>Llamada desde: Icono de exportar a Pdf</remarks>
        --%>*/
        function MostrarPdf() {
            __doPostBack("", "Pdf");
        }

        /*<%--Revisado por: blp. Fecha: 14/12/2011
        <summary>
        Abre el panel de configuración de los campos a mostrar en el grid
        </summary> 
        <remarks>Llamada desde: evento click del control btnConfigurar</remarks>
        --%>*/
        function abrirPanelConfiguracion() {
            var mpeConfig = $find("mpeConfig")
            mpeConfig.show()
            mpeConfig = null;
        }

        /*<%--Revisado por: blp. Fecha: 15/12/2011
        <summary>
        Cierra el panel de configuración de los campos a mostrar en el grid
        </summary>
        <remarks>Llamada desde: evento click del control btnCancelar</remarks>
        --%>*/
        function cerrarPanelConfiguracion() {
            var mpeConfig = $find("mpeConfig")
            mpeConfig.hide()
            mpeConfig = null;
            cancelarCambiosConfiguracion();
        }

        /*<%--Revisado por: blp. Fecha: 30/01/2012
        <summary>
        Cierra el panel de configuración de los campos a mostrar en el grid
        y abre el de Guardado de configuración.
        </summary>
        <remarks>Llamada desde: evento click del control btnCancelar</remarks>
        --%>*/
        function cerrarPanelConfiguracionMostrarGuardar() {
            //vaciamos la lista de cambios
            checkboxes = new Array();
            cerrarPanelConfiguracion()
            document.getElementById("<%=btnGuardarConfiguracion.clientID%>").style.display = "block";
        }
        /*<%--Revisado por: blp. Fecha: 19/07/2013
        <summary>
        Cancela los cambios en el panel de configuración y lo deja con los checks tal y como estaban inicialmente
        </summary>
        <remarks>Llamada desde: evento click del control btnCerrar</remarks>
        --%>*/
        function cancelarCambiosConfiguracion() {
            if (checkboxes.length > 0) {
                //El orden inverso es fundamental para devolver los checks al estado original
                for (var i = (checkboxes.length-1); i > -1; i--) {
                    var oChk = document.getElementById(checkboxes[i].id);
                    if(oChk)
                    {
                        oChk.checked = checkboxes[i].estadoOriginal;
                    }
                }
                //vaciamos la lista
                checkboxes = new Array();
            }
        }
        /*<%--Revisado por: blp. Fecha: 20/01/2012
        <summary>
        Abrir el buscador de articulos en una ventana nueva
        </summary>
        <remarks>Llamada desde: evento click del control btnPnlBusquedaArt</remarks>
        --%>*/
        function AbrirPanelArticulos() {
            /*<%-- 
            Actualmente no es necesario pasar ninguno de los siguientes valores a la página articulosserver.
            mat = Material preseleccionado
            matro = 1: Material como sólo lectura, 0: No
            Index = No se usa en articulosserver
            TabContainer = No se usa en articulosserver
            ClientID = Id del control que contiene el artículo en esta página
            restric = string con los valores de GMN1, GMN2, GMN3 y GMN4 preseleccionados colocados uno tras otro
            MatClientId = Id del control que contiene el material en esta página
            InstanciaMoneda = Moneda por la que filtrar cuando se recupera el último precio adjudicado para el artículo
            CargarUltAdj = Indicador de si carga o no el último precio adjudicado para el artículo
            CodOrgCompras, OrgComprasRO, CodCentro, CentroRO -> Parámetros relacionados con las Organizaciones de compras y Centros de coste que no son necesarios en este caso.
            Origen -> Valor que nos permite saber si la página se solicitó desde entregasPedidos.
            --%>*/
            windowopen("../_common/articulosserver.aspx?mat=&matro=0&Index=0&TabContainer=&ClientID=&restric=&MatClientId=&InstanciaMoneda=&CargarUltAdj=false&CodOrgCompras=&OrgComprasRO=&CodCentro=&CentroRO=&Origen=entregasPedidos", "_blank", "width=500,height=400,status=yes,resizable=no,top=200,left=200")
        }
        //<%'Revisado por: blp. Fecha: 20/01/2012%>
        //<%'<summary>%>
        //<%'lblBusqueda y cpeBusquedaAvanzada no pueden estar visibles al mismo tiempo %>
        //<%'Mostramos el panel resumen de los filtros (divBusqueda) cuando se oculta el panel de búsqueda y lo ocultamos cuando este último se muestra%>
        //<%'</summary>%>
        //<%'<remarks>Llamada desde el evento onClick de cliente del control pnlCabeceraRecepcion. Max. 0,1 seg.</remarks>%>
        function checkCollapseStatus() {
            var oControlcpeBusqueda = $find("<%=cpeBusquedaAvanzada.clientID %>")
            var oControllblBusqueda = $get("<%=divBusqueda.clientID %>")
            if (oControlcpeBusqueda) {
                if (oControlcpeBusqueda.get_Collapsed() == true) {
                    oControllblBusqueda.style.display = "none"
                }
                else {
                    oControllblBusqueda.style.display = "block"
                }
            }
            oControlcpeBusqueda = null;
            oControllblBusqueda = null;
        }

        /*<%--Revisado por: blp. Fecha: 30/01/2012
        <summary>
        Método necesario para ordenar las columnas del grid
        </summary>
        <remarks>Llamada desde: evento ColumnSorting del grid. Máx. 0,1 seg.</remarks>
        --%>*/
        function whdgEntregas_Sorting_ColumnSorting(grid, args) {
            var sorting = grid.get_behaviors().get_sorting();
            var col = args.get_column();
            var sortedCols = sorting.get_sortedColumns();
            for (var x = 0; x < sortedCols.length; ++x) {
                if (col.get_key() == sortedCols[x].get_key()) {
                    args.set_clear(false);
                    break;
                }
            }
        }
        /*<%'Una vez obtenido el detalle de los pagos de la factura, muestra el modalpopup.
        'Llamada desde: onCellSelectionChanged
        %>*/ 
        function OnGetFactura(result) {
            $get('divFacturaContent').innerHTML = result;
            popUpShowed = $find('<%=mpeFactura.ClientID%>');
            popUpShowed.show();
            popUpShowed._backgroundElement.style.zIndex += 10;
            popUpShowed._foregroundElement.style.zIndex += 10;
        }
        /*<%'Revisado por: mpf. Fecha: 06/09/2012
        'Se lanza al pulsar una celda de la grid. Dependiendo de la celda pulsada, realizará una acción u otra.
        %>*/
        function onCellSelectionChanged(sender, e) {
            var row = e.getSelectedCells().getItem(0).get_row();
            if (e.getSelectedCells().getItem(0).get_column().get_key() == "Key_FACTURA") {
                if (row.get_cellByColumnKey("Key_INSTANCIA").get_value() == 0) {
                    $get("<%=lblAlbaranFactura.ClientID %>").innerText = (row.get_cellByColumnKey("Key_ALBARAN").get_value());
                    $get("<%=lblArticuloFactura.ClientID %>").innerText = (row.get_cellByColumnKey("Key_ARTCOD").get_value());
                    Fullstep.PMPortalWeb.Consultas.Obtener_DatosEntregaFactura(row.get_cellByColumnKey("Key_FACTURAID").get_value(), OnGetFactura)
                }
                else {
                    window.location.href("../facturas/DetalleFactura.aspx?ID=" + (row.get_cellByColumnKey("Key_FACTURAID").get_value()) + "&ORIGEN=entregasPedidos");
                }
            }
            if (e.getSelectedCells().getItem(0).get_column().get_key() == "Key_RECEPTOR") {
                FSNMostrarPanel('<%=FSNPanelDatosReceptor.AnimationClientID%>', event, '<%=FSNPanelDatosReceptor.DynamicPopulateClientID%>', row.get_cellByColumnKey("Key_RECEPTOR").get_value());
                Fullstep.PMPortalWeb.Consultas.Obtener_DatosReceptor(row.get_cellByColumnKey("Key_RECEPTOR").get_value())
            }
        }

        var ModalProgress = '<%=ModalProgress.ClientID%>';
        /*<%'Revisado por: blp. Fecha: 19/07/2013
        'Vamos a controlar los cambios en los checks del panel de configuración para que cuando se cancelen los cambios, se vuelvan a dejar como estaban.
        %>*/
        var checkboxes = new Array();
        function controlChecksPanelConfiguracion(control) {
            var oChk = new Object();
            oChk.id = control.id;
            oChk.estadoOriginal = !control.checked;
            checkboxes.push(oChk);
        }
        </script>
        <IFRAME id="iframeWSServer" style="Z-INDEX: 106; LEFT: 8px; VISIBILITY: hidden; POSITION: absolute; TOP: 200px"
			    name="iframeWSServer" src="<%=Application("RUTASEGURA")%>script/blank.htm"></IFRAME>
	    <script>
	        dibujaMenu(4)
        </script>
	    <form id="Form1" method="post" runat="server">
            <asp:ScriptManager ID="smEntregasPedidos" runat="server" EnablePageMethods ="true">
                <Services>
                    <asp:ServiceReference Path="~/Consultas.asmx" />
                </Services>
            </asp:ScriptManager>

            <%-- Panel Configuracion Grid --%>

            <input id="btnOcultoConfig" type="button" value="button" runat="server" style="display: none" />
            <asp:Panel ID="pnlConfig" runat="server" BackColor="White" BorderColor="DimGray"
                BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center" Style="display: none;
                padding: 5px" Width="480px">
                <asp:LinkButton runat="server" ID="btnCerrar" style="text-align:right;float:right;" OnClientClick="cancelarCambiosConfiguracion(); return false;">
                    <img src="../_common/images/Bt_Cerrar.png" style="border:0px" />
                </asp:LinkButton>
                <fieldset style="padding:5px;">
                    <legend style="vertical-align: top">
                        <asp:Label ID="lblConfig" runat="server" Text="DMostrar los siguientes Campos" class="Etiqueta"></asp:Label>
                    </legend>
                    <table id="tblPnlConfig" runat="server" cellpadding="0" cellspacing="2" border="0" style="text-align:left;">
                    </table>
                </fieldset>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-top:5px;">
                <tr>
                    <td style="padding-right:3px;">
                        <fsn:FSNButton ID="btnAceptar" runat="server" Text="DBuscar" Alineacion="Right" OnClientClick="cerrarPanelConfiguracionMostrarGuardar(); return false;"></fsn:FSNButton>
                    </td>
                    <td style="padding-left:3px;">
                        <fsn:FSNButton ID="btnCancelar" runat="server" Text="DCancelar" Alineacion="Left" OnClientClick="cerrarPanelConfiguracion(); return false;"></fsn:FSNButton>
                    </td>
                </tr>
                </table>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="mpeConfig" runat="server" BackgroundCssClass="modalBackground"
                PopupControlID="pnlConfig" TargetControlID="btnOcultoConfig" CancelControlID="btnCerrar"></cc1:ModalPopupExtender>

            <%-- Contenido --%>
            <table width="100%" cellpadding="0" border="0">
                <tr>
                    <td width="2%"></td>
                    <td width="23%">
                        <img src="images/camion.jpg" />
                        <asp:Label id="lblEntregasPedidos" Text="DEntregas de pedidos" runat="server" class="rotuloGrande"></asp:Label>
                    </td>
                    <td width="75%">
                        <%-- Búsqueda General --%>
                        <fieldset>
                            <legend style="vertical-align: top">
                                <asp:Label ID="lblBusquedaGeneral" runat="server" Text="DBúsqueda General" class="Etiqueta"></asp:Label>
                            </legend>
                            <table width="100%" cellpadding="0" border="0" id="tblBusquedaGeneral" runat="server">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblFecRecepDesde" runat="server" Text="DDesde fecha de recepción:" CssClass="Normal"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <fsde:generalentry id="txtFecRecepDesde" runat="server" Width="118px" DropDownGridID="txtFecRecepDesde"
                                            Tipo="TipoFecha" Tag="txtFecRecepDesde" Independiente="1" Height="22px">
                                            <InputStyle CssClass="CajaTexto"></InputStyle>
                                        </fsde:generalentry>
                                    </td>
                                    <td width="20px">
                                    </td>
                                    <td>
                                        <asp:CheckBox class="Normal" ID="cbVerPedidosSinRecep" runat="server" Text="DVer pedidos sin recepciones"></asp:CheckBox>
                                    </td>
                                    <td align="right">
                                        <fsn:FSNButton ID="btnBuscarGeneral" runat="server" Text="DBuscar" Alineacion="Left"></fsn:FSNButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblFecRecepHasta" runat="server" Text="DHasta fecha de recepción:" CssClass="Normal"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <fsde:generalentry id="txtFecRecepHasta" runat="server" Width="118px" DropDownGridID="txtFecRecepHasta"
                                            Tipo="TipoFecha" Tag="txtFecRecepHasta" Independiente="1" Height="22px">
                                            <InputStyle CssClass="CajaTexto"></InputStyle>
                                        </fsde:generalentry>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Label ID="LblNumAlbaran" runat="server" Text="DNº Albarán:" CssClass="Normal"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="CajaTexto" ID="txtNumAlbaran" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblNumPedido" runat="server" Text="DNº de pedido:" CssClass="Normal"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList CssClass="desplegable" ID="ddAnyo" DataTextField="Text" DataValueField="Value" runat="server" Width="60px"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="CajaTexto" ID="txtNumCesta" runat="server" onchange="validarsolonum(this)" onkeypress="return validarkeynum()" onpaste="validarpastenum()"></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender WatermarkCssClass="waterMark" ID="txtwmeNumCesta" runat="server" TargetControlID="txtNumCesta" WatermarkText="DNº de Cesta"></cc1:TextBoxWatermarkExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="CajaTexto" ID="txtNumPedido" runat="server" onchange="validarsolonum(this)" onkeypress="return validarkeynum()" onpaste="validarpastenum()"></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender WatermarkCssClass="waterMark" ID="txtwmeNumPedido" runat="server" TargetControlID="txtNumPedido" WatermarkText="DNº de Pedido"></cc1:TextBoxWatermarkExtender>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblNumPedERP" runat="server" Text="DNº Pedido ERP:" CssClass="Normal"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="CajaTexto" ID="txtNumPedERP" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <%-- Búsqueda Avanzada --%>
            <asp:Panel runat="server" ID="pnlBusquedaAvanzada" Style="cursor: pointer" Font-Bold="True"
                Height="20px" Width="100%" CssClass="PanelCabecera" onClick="checkCollapseStatus();">
                <asp:Image ID="imgExpandir" runat="server" SkinID="Contraer" />
                <asp:Label ID="lblBusquedaAvanzada" runat="server" Text=" DSeleccione los criterios de búsqueda generales"
                Font-Bold="True" ForeColor="White"></asp:Label>
            </asp:Panel>
            <asp:Panel ID="pnlParametros" runat="server" CssClass="Rectangulo" Width="100%">
                <table width="100%" border="0" cellpadding="4" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Label ID="lblArticulo" runat="server" Text="DArtículo:" CssClass="Normal"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtArticulo" CssClass="CajaTexto" runat="server" Width="220px"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender WatermarkCssClass="waterMark" ID="txtwmeArticulo" runat="server" TargetControlID="txtArticulo" WatermarkText="DNombre, descripción..."></cc1:TextBoxWatermarkExtender>
                            <cc1:AutoCompleteExtender ID="txtArticulo_AutoCompleteExtender" runat="server"
                                CompletionInterval="500" CompletionSetCount="10" MinimumPrefixLength="3" ServiceMethod="AutoCompletarArticulo"
                                TargetControlID="txtArticulo" UseContextKey="True">
                            </cc1:AutoCompleteExtender>
                            <a id="btnPnlBusquedaArt" style="CURSOR: hand" onclick="javascript:AbrirPanelArticulos()" name="btnPnlBusquedaArt">
                                <img src="../_common/images/lupa.gif" style="border:0px" />
                            </a>
                        </td>
                        <td></td>
                        <td><asp:Label ID="lblFecPedidoDesde" runat="server" Text="DFecha de pedido:" CssClass="Normal"></asp:Label></td>
                        <td>
                            <fsde:generalentry id="txtFecPedidoDesde" runat="server" Width="118px" DropDownGridID="txtFecPedidoDesde"
                                Tipo="TipoFecha" Tag="txtFecPedidoDesde" Independiente="1" Height="22px">
                                <InputStyle CssClass="CajaTexto"></InputStyle>
                            </fsde:generalentry>
                        </td>
                        <td></td>
                        <td><asp:Label ID="lblFecPedidoHasta" runat="server" Text="DHasta:" CssClass="Normal"></asp:Label></td>
                        <td>
                            <fsde:generalentry id="txtFecPedidoHasta" runat="server" Width="118px" DropDownGridID="txtFecPedidoHasta"
                                Tipo="TipoFecha" Tag="txtFecPedidoHasta" Independiente="1" Height="22px">
                                <InputStyle CssClass="CajaTexto"></InputStyle>
                            </fsde:generalentry>                        
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="lblNumPedProve" runat="server" Text="DNº pedido proveedor:" CssClass="Normal"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtNumPedProve" CssClass="CajaTexto" runat="server" Width="220px"></asp:TextBox>
                        </td>
                        <td></td>
                        <td><asp:Label ID="lblFecEntregaSolicitDesde" runat="server" Text="DFecha de entrega solicitada:" CssClass="Normal"></asp:Label></td>
                        <td>
                            <fsde:generalentry id="txtFecEntregaSolicitDesde" runat="server" Width="118px" DropDownGridID="txtFecEntregaSolicitDesde"
                                Tipo="TipoFecha" Tag="txtFecEntregaSolicitDesde" Independiente="1" Height="22px">
                                <InputStyle CssClass="CajaTexto"></InputStyle>
                            </fsde:generalentry>                        
                        </td>
                        <td></td>
                        <td><asp:Label ID="lblFecEntregaSolicitHasta" runat="server" Text="DHasta:" CssClass="Normal"></asp:Label></td>
                        <td>
                            <fsde:generalentry id="txtFecEntregaSolicitHasta" runat="server" Width="118px" DropDownGridID="txtFecEntregaSolicitHasta"
                                Tipo="TipoFecha" Tag="txtFecEntregaSolicitHasta" Independiente="1" Height="22px">
                                <InputStyle CssClass="CajaTexto"></InputStyle>
                            </fsde:generalentry>
                        </td>
                    </tr>
                    <tr id="trNumFactura" runat="server">
                        <td><asp:Label ID="lblNumFactura" runat="server" Text="DNº factura:" CssClass="Normal"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtNumFactura" CssClass="CajaTexto" runat="server" Width="220px"></asp:TextBox>
                        </td>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <asp:UpdatePanel runat="server" ID="upBusqueda" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                    <tr>
                        <td><asp:Label ID="lblEmpresa" runat="server" Text="DArtículo:" CssClass="Normal"></asp:Label></td>
                        <td>
                            <asp:DropDownList class="desplegable" ID="ddEmpresa" DataTextField="Text" DataValueField="Value" runat="server" Width="222px"></asp:DropDownList>
                        </td>
                        <td></td>
                        <td><asp:Label ID="lblFecEntregaIndicadaDesde" runat="server" Text="DFecha de entrega indicada:" CssClass="Normal"></asp:Label></td>
                        <td>
                            <fsde:generalentry id="txtFecEntregaIndicadaDesde" runat="server" Width="118px" DropDownGridID="txtFecEntregaIndicadaDesde"
                                Tipo="TipoFecha" Tag="txtFecEntregaIndicadaDesde" Independiente="1" Height="22px">
                                <InputStyle CssClass="CajaTexto"></InputStyle>
                            </fsde:generalentry>
                        </td>
                        <td></td>
                        <td><asp:Label ID="lblFecEntregaIndicadaHasta" runat="server" Text="DHasta:" CssClass="Normal"></asp:Label></td>
                        <td>
                            <fsde:generalentry id="txtFecEntregaIndicadaHasta" runat="server" Width="118px" DropDownGridID="txtFecEntregaIndicadaHasta"
                                Tipo="TipoFecha" Tag="txtFecEntregaIndicadaHasta" Independiente="1" Height="22px">
                                <InputStyle CssClass="CajaTexto"></InputStyle>
                            </fsde:generalentry>
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="lblCentro" runat="server" Text="DCentro de coste:" CssClass="Normal"></asp:Label></td>
                        <td>
                            <asp:DropDownList class="desplegable" ID="ddCentro" DataTextField="Text" DataValueField="Value" runat="server" Width="222px"></asp:DropDownList>
                        </td>
                        <td></td>
                        <td colspan=5>
                            <%-- Partidas presupuestarias --%>
                            <asp:PlaceHolder ID="phrPartidasPresBuscador" runat="server">
                            </asp:PlaceHolder>
                        </td>
                    </tr>
                    </ContentTemplate>
                    </asp:UpdatePanel>                    
                    <tr>
                        <td colspan="8">
                            <fsn:FSNButton ID="btnBuscarBusquedaAvanzada" runat="server" Text="DBuscar" Alineacion="left" style="margin-right:10px;"></fsn:FSNButton>
                            <fsn:FSNButton ID="btnLimpiarBusquedaAvanzada" runat="server" Text="DLimpiar" Alineacion="Left"></fsn:FSNButton>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <cc1:CollapsiblePanelExtender ID="cpeBusquedaAvanzada" runat="server" CollapseControlID="pnlBusquedaAvanzada"
                ExpandControlID="pnlBusquedaAvanzada" SuppressPostBack="true" TargetControlID="pnlParametros"
                ImageControlID="imgExpandir" SkinID="FondoRojo" Collapsed="true">
            </cc1:CollapsiblePanelExtender>
            <div id="divBusqueda" runat="server" style="margin:5px;background-color: #DCDCDC;">
                <asp:Label ID="lblBusqueda" runat="server"></asp:Label>
            </div>
            <br />
            <asp:UpdatePanel ID="upEntregas" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <table border="0" width="99%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:Panel ID="panSinPaginacion" runat="server" Visible="True" Width="100%">
                                    <div style="border: 1px solid #CCCCCC;" align="right">
                                        <table border="0" align="right">
                                            <tr>
                                                <td align="right">
                                                    <fsn:FSNButton ID="btnGuardarConfiguracion" runat="server" Text="DSalvar Configuración" style="display:none;" OnClientClick="ocultarBtnGuardarConfiguracion();"></fsn:FSNButton>
                                                </td>
                                                <td align="right">
                                                    <fsn:FSNButton ID="btnConfigurar" runat="server" Text="DConfigurar" OnClientClick="abrirPanelConfiguracion(); return false;"></fsn:FSNButton>
                                                </td>
                                                <td align="right" width="175px">
                                                    <div style="height:22px; float:right; line-height:22px;">
                                                        <div onclick="MostrarExcel();return false;" class="botonDerechaCabecera">
                                                            <div ID="ibExcel" runat="server" style="background-repeat:no-repeat;height:20px;">
                                                                <span ID="litExcel" runat="server" style="margin-left:20px;"></span>
                                                            </div>
                                                        </div>
                                                        <div onclick="MostrarPdf();return false;" class="botonDerechaCabecera">            
                                                            <div ID="ibPDF" runat="server" style="background-repeat:no-repeat;height:20px;">
                                                                <span ID="litPDF" runat="server" style="margin-left:20px;"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <ig:WebHierarchicalDataGrid ID="whdgEntregas" runat="server" AutoGenerateColumns="false" Width="100%" ShowHeader="true" EnableAjax="false" 
                                    EnableAjaxViewState="false" EnableDataViewState="false">
                                    <Columns>
                                        <ig:UnboundField Key="Key_RETRASADO"></ig:UnboundField>
                                        <ig:BoundDataField Key="Key_CODRECEPERP" DataFieldName="CODRECEPERP"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_ALBARAN" DataFieldName="ALBARAN"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_FECHARECEP" DataFieldName="FECHARECEP" DataFormatString="{0:d}"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_DENEMPRESA" DataFieldName="DENEMPRESA"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_NUM_PED_ERP" DataFieldName="NUM_PED_ERP"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_ANYOPEDIDOORDEN" DataFieldName="ANYOPEDIDOORDEN"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_NUMPEDPROVE" DataFieldName="NUMPEDPROVE"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_FECHAEMISION" DataFieldName="FECHAEMISION"></ig:BoundDataField>  <%-- DataFormatString="{0:MM-dd-yyyy}" --%>
                                        <ig:BoundDataField Key="Key_FECENTREGASOLICITADA" DataFieldName="FECENTREGASOLICITADA"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_FECENTREGAINDICADAPROVE" DataFieldName="FECENTREGAINDICADAPROVE"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_ARTDEN" DataFieldName="ARTDEN"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_CANTLINEAPEDIDATOTAL" DataFieldName="CANTLINEAPEDIDATOTAL"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_CANTLINEARECIBIDAPARCIAL" DataFieldName="CANTLINEARECIBIDAPARCIAL"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_CANTLINEAPENDIENTETOTAL" DataFieldName="CANTLINEAPENDIENTETOTAL"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_UNIDEN" DataFieldName="UNIDEN"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_PREC_UP" DataFieldName="PREC_UP"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_IMPORTELINEATOTAL" DataFieldName="IMPORTELINEATOTAL"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_IMPORTELINEARECIBIDOPARCIAL" DataFieldName="IMPORTELINEARECIBIDOPARCIAL"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_IMPORTELINEAPENDIENTETOTAL" DataFieldName="IMPORTELINEAPENDIENTETOTAL"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_MONDEN" DataFieldName="MONDEN"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_NUMLINEA" DataFieldName="NUMLINEA" DataFormatString="{0:d}"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_FACTURA" DataFieldName="NUM_FACTURA" CssClass="itemSeleccionable"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_INSTANCIA" DataFieldName="INSTANCIA" Hidden="true"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_FACTURAID" DataFieldName="ID_NUM_FACTURA" Hidden="true"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_ARTCOD" DataFieldName="ARTCOD" Hidden="true"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_RECEPTOR" DataFieldName="RECEPTOR"></ig:BoundDataField>
                                        <ig:BoundDataField Key="Key_IMPORTETOTALALBARAN" DataFieldName="IMPORTETOTALALBARAN"></ig:BoundDataField>
                                        <ig:UnBoundField Key="Key_IM_BLOQUEO"></ig:UnBoundField>
                                    </Columns>
                                    <Behaviors>
                                        <ig:ColumnResizing Enabled="true">
                                            <ColumnResizingClientEvents ColumnResized="whdgEntregas_anchoModificado" />
                                        </ig:ColumnResizing>
                                        <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true" AnimationEnabled="false">
                                            <ColumnSettings>
                                                <ig:ColumnFilteringSetting ColumnKey="Key_RETRASADO" EditorID="ddpwhdgEntregasRETRASADO" />
                                                <ig:ColumnFilteringSetting ColumnKey="Key_IM_BLOQUEO" EditorID="ddpwhdgEntregasIM_BLOQUEO" />
                                            </ColumnSettings>
                                        </ig:Filtering>
                                        <ig:Paging Enabled="true" PagerAppearance="Both">
                                            <PagerTemplate>
                                                <pag:Paginador id="wdgPaginador" runat="server" />
                                            </PagerTemplate>
                                        </ig:Paging>
                                        <ig:VirtualScrolling Enabled="false"></ig:VirtualScrolling>
                                        <ig:Sorting Enabled="true" SortingMode="Multi" >
                                            <SortingClientEvents ColumnSorting="whdgEntregas_Sorting_ColumnSorting" />                                        
                                        </ig:Sorting>
                                        <ig:Selection CellClickAction="Cell" CellSelectType="Single" SelectionClientEvents-CellSelectionChanged="onCellSelectionChanged"></ig:Selection>
                                    </Behaviors>
                                    <GroupingSettings EnableColumnGrouping="True" GroupAreaVisibility="Visible" />
                                    <EditorProviders>
                                        <ig:DropDownProvider id="ddpwhdgEntregasRETRASADO">
                                            <EditorControl runat="server" ID="ecddpwhdgEntregasRETRASADO" ClientIDMode="Predictable" DataKeyFields="EUERoleID"
                                                DisplayMode="DropDownList" DropDownContainerMaxHeight="200px" EnableAnimations="False"
                                                EnableDropDownAsChild="False" >
                                            </EditorControl>
                                        </ig:DropDownProvider>
                                        <ig:DropDownProvider id="ddpwhdgEntregasIM_BLOQUEO">
                                            <EditorControl runat="server" ID="ecddpwhdgEntregasIM_BLOQUEO" ClientIDMode="Predictable" DataKeyFields="EUERoleID"
                                                DisplayMode="DropDownList" DropDownContainerMaxHeight="200px" EnableAnimations="False"
                                                EnableDropDownAsChild="False" >
                                            </EditorControl>
                                        </ig:DropDownProvider>
                                    </EditorProviders>
                                    </ig:WebHierarchicalDataGrid>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div id="div_Label" runat="server" class="EtiquetaScroll" style="display: none; position: absolute">
                        <asp:Label ID="label2" runat="server" Font-Bold="true"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:Button ID="btnFactura" runat="server" Style="display: none" />
    <cc1:ModalPopupExtender ID="mpeFactura" runat="server" Enabled="True" PopupControlID="panFactura" TargetControlID="btnFactura" CancelControlID="imgCerrarFactura" />
    <asp:Panel ID="panFactura" runat="server" Style="display: none" Width="600px" CssClass="modalPopup">
        <div id="Cabecera"><table width="100%">
        <tr><asp:Label ID="lblLitFactura" runat="server" CssClass="RotuloGrande"></asp:Label>&nbsp;<asp:Label ID="lblLitAlbaran" runat="server" CssClass="RotuloGrande"></asp:Label>&nbsp;<asp:Label ID="lblAlbaranFactura" runat="server" CssClass="RotuloGrande"></asp:Label>&nbsp;<asp:Label ID="lblLitArticuloFactura" runat="server" CssClass="RotuloGrande"></asp:Label>&nbsp;<asp:Label ID="lblArticuloFactura" runat="server" CssClass="RotuloGrande"></asp:Label></td>
            <td align="right""><asp:ImageButton id="imgCerrarFactura" runat="server" /></td></tr></table>        
        </div>
        <p><br /></p>
        <div id="divFacturaContent"></div>
        <p><br /></p>
    </asp:Panel>

            <fsn:FSNPanelInfo ID="FSNPanelDatosEntregas" runat="server" ServicePath="~/Consultas.asmx" ServiceMethod="Obtener_DatosEntregaFactura" TipoDetalle="0"></fsn:FSNPanelInfo>
             <fsn:FSNPanelInfo ID="FSNPanelDatosReceptor" runat="server" ServicePath="~/Consultas.asmx" ServiceMethod="Obtener_DatosReceptor" TipoDetalle="0"></fsn:FSNPanelInfo>
            <script type="text/javascript" src="../_common/js/jsUpdateProgress.js"></script>
            <cc1:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
                BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
            <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress" Style="display: none">
                <div style="position: relative; top: 30%; text-align: center;">
                    <asp:Image ID="ImgProgress" runat="server" SkinID="ImgProgress" />
                    <asp:Label ID="LblProcesando" runat="server" ForeColor="Black" Text="Cargando ..."></asp:Label>
                </div>
            </asp:Panel>
            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="panelUpdateProgress"
                BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
            <ig:WebDocumentExporter ID ="WebDocumentExporter1" runat="server"></ig:WebDocumentExporter>
            <ig:WebExcelExporter ID ="WebExcelExporter1" runat="server"></ig:WebExcelExporter>
    	    </form>
            <script type="text/javascript">
                //<%--Vamos a comprobar siempre que se cargue la página si están desplegados o no tanto el panel de búsqueda avanzada como el label de filtros para que no coincidan (ocurre en algún caso, con el panel de configuración de por medio)--%>
                checkCollapseStatus()            
            </script>
        </body>
</html>
