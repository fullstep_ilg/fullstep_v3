﻿Imports Fullstep.PMPortalServer
Imports Fullstep.PMPortalServer.TiposDeDatos
Imports System.Linq
Imports System.Text
Imports System.Collections.Generic
Imports Infragistics.Web.UI.GridControls
Imports System.Collections.ObjectModel

Namespace Fullstep.PMPortalWeb
    Partial Class entregasPedidos
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

#End Region

        Private _PaginadorBottom As Paginador
        Private _PaginadorTop As Paginador

        Private bCodificacionPersonalizadaRecepcionesActivada As Boolean
        ''' Revisado por:blp. Fecha: 20/10/2011
        ''' <summary>
        ''' PreCargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init

            setCultureInfoUsuario()

            Me.ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.EntregaPedidos

            InitializeComponent()

            Me.FindControl("Form1").Controls.Add(Fullstep.PMPortalWeb.CommonInstancia.InsertarCalendario(FSPMUser.Idioma.ToString))

            If Not Me.IsPostBack Then
                'Vaciamos la caché porque es una carga nueva. De otro modo, sería necesario controlar los filtros para saber si la carga ha cambiado
                BorrarCacheEntregas()
                InicializarValoresControles()
            End If

            'Crear los combos de partidas presupuestarias y llenarlos de contenido
            If Me.Acceso.gbAccesoFSSM Then
                CargarCombosPartidas()
            End If

            Me._PaginadorBottom = TryCast(Me.whdgEntregas.GridView.Behaviors.Paging.PagerTemplateContainerBottom.FindControl("wdgPaginador"), Paginador)
            AddHandler Me._PaginadorBottom.PageChanged, AddressOf _Paginador_PageChanged
            Me._PaginadorTop = TryCast(Me.whdgEntregas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("wdgPaginador"), Paginador)
            AddHandler Me._PaginadorTop.PageChanged, AddressOf _Paginador_PageChanged

        End Sub

        ''' Revisado por: blp. Fecha: 23/11/2011
        ''' <summary>
        ''' Carga de la pagina
        ''' </summary>
        ''' <param name="sender">Control que lanza el evento de carga</param>
        ''' <param name="e">argumentos del evento</param>
        ''' <remarks>Llamada desde el evento Load de la página. Máx inferior a 1 seg.</remarks>
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            'Código de albaran personalizado
            Dim oEntregas As PMPortalServer.Entregas = FSPMServer.Get_Entregas
            If oEntregas.CodificacionPersonalizadaRecepciones(Session("FS_Portal_User").CiaComp) Then
                Dim tablas() As Integer = {TablasIntegracion.Rec_Aprov, TablasIntegracion.Rec_Directo}
                If HayIntegracionSalidaoEntradaSalida(tablas) Then
                    bCodificacionPersonalizadaRecepcionesActivada = True 'Num. Albarán ERP
                End If
            End If

            If Not IsPostBack Then
                CargarTextos()
                InicializarControlesExportacion()
                imgCerrarFactura.ImageUrl = "../_common/images/Bt_Cerrar.png"
            Else

                If Request("__EVENTARGUMENT") = "Excel" Then
                    Exportar(1)
                    Exit Sub
                ElseIf Request("__EVENTARGUMENT") = "Pdf" Then
                    Exportar(2)
                    Exit Sub
                End If
            End If

            CargarwhdgEntregas(False, TipoBotonBusqueda.Ninguno)

            If Not (ScriptMgr.IsInAsyncPostBack AndAlso ScriptMgr.AsyncPostBackSourceElementID = "whdgEntregas" AndAlso whdgEntregas.GridView.Behaviors.Filtering.Filtered) Then
                'El InicializarPaginador se lanza al filtrar los datos. Si lo lanzamos aquí tb deja de funcionar el recuento de resultados que se hace dentro de la función.
                'Por eso excluimos el caso concreto en que se filtra
                InicializarPaginador()
            End If

            If Not Me.Acceso.g_bAccesoFSFA Then
                'Oculto el label y el textbox de numero factura en el caso de que cuando no esta visible el modulo de facturación
                lblNumFactura.Visible = False
                txtNumFactura.Visible = False
                trNumFactura.Visible = False 'Oculto la columna
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)
            End If
        End Sub

        ''' Revisado por: blp. Fecha:09/03/2012
        ''' <summary>
        ''' Pone imágenes a los controles de exportación
        ''' </summary>
        ''' <remarks>Llamada desde Load. Máx. 0,01 seg.</remarks>
        Private Sub InicializarControlesExportacion()
            ibExcel.Style("background-image") = ConfigurationManager.AppSettings("RUTANORMAL") & "App_Themes/" & Me.Page.Theme & "/images/Excel.png"
            ibPDF.Style("background-image") = ConfigurationManager.AppSettings("RUTANORMAL") & "App_Themes/" & Me.Page.Theme & "/images/PDF.png"
        End Sub

        ''' Revisado por: blp. Fecha: 07/12/2011
        ''' <summary>
        ''' Configura la grid para su posterior exportación. Exporta la Grid a una hoja excel o a Pdf
        ''' </summary>
        ''' <param name="iTipoExportacion"> Tipo Exportacion (Excel = 1 // Pdf = 2)</param>
        ''' <remarks>Llamada desde:Page_load; Tiempo ejecucion:0,3seg.</remarks>
        Private Sub Exportar(ByVal iTipoExportacion As Byte)
            CargarwhdgEntregas(True, TipoBotonBusqueda.Ninguno)

            If iTipoExportacion = 1 Then 'Excel
                WebExcelExporter1.DownloadName = Date.Now.ToShortDateString & "_" & Textos(0) & ".xls"
                WebExcelExporter1.Export(whdgEntregas.GridView)

            Else 'PDF
                With WebDocumentExporter1
                    .TargetPaperOrientation = Infragistics.Documents.Reports.Report.PageOrientation.Landscape
                    .DownloadName = Date.Now.ToShortDateString & "_" & Textos(0) & ".xls"
                    .Format = Infragistics.Documents.Reports.Report.FileFormat.PDF
                    .EnableStylesExport = True
                    .DataExportMode = DataExportMode.AllDataInDataSource
                    .CustomFont = New System.Drawing.Font("Arial, Verdana", 4, System.Drawing.GraphicsUnit.Pixel)
                    .Margins = Infragistics.Documents.Reports.Report.PageMargins.GetPageMargins("Narrow")
                    .TargetPaperSize = Infragistics.Documents.Reports.Report.PageSizes.GetPageSize("A4")
                    .Export(whdgEntregas.GridView)
                End With
            End If
        End Sub

        ''' Revisado por: blp. Fecha: 31/10/2011
        ''' <summary>
        ''' Carga de los elementos con el idioma del usuario
        ''' </summary>
        ''' <remarks>Llamada desde:Page_load; Tiempo máximo=0,2</remarks>
        Private Sub CargarTextos()
            whdgEntregas.Key = Textos(0)
            lblEntregasPedidos.Text = Textos(0) 'Entregas de pedidos
            lblBusquedaGeneral.Text = Textos(1) & ":" ' Búsqueda General
            lblFecRecepDesde.Text = Textos(2) & ":" 'Desde fecha de recepción
            cbVerPedidosSinRecep.Text = Textos(4) 'Ver pedidos sin recepciones
            lblFecRecepHasta.Text = Textos(3) & ":" 'Hasta fecha de recepción
            LblNumAlbaran.Text = Textos(6) & ":" 'Nº albarán
            lblNumPedido.Text = Textos(5) & ":" 'Nº de Cesta
            txtwmeNumPedido.WatermarkText = Textos(5) 'Nº de pedido
            txtwmeNumCesta.WatermarkText = Textos(33) 'Nº de Cesta

            Dim Acceso As FSNLibrary.TiposDeDatos.ParametrosGenerales = Me.Acceso
            If Acceso.gbOblCodPedDir Or Acceso.gbOblCodPedido Then
                Dim oParametros As PMPortalServer.Parametros = FSPMServer.Get_Parametros
                lblNumPedERP.Text = oParametros.CargarLiteralParametros(Fullstep.FSNLibrary.TiposDeDatos.LiteralesParametros.PedidoERP, FSPMUser.Idioma, IdCiaComp) & ":" 'Literal del num pedido ERP
            End If

            btnBuscarGeneral.Text = Textos(13) 'Buscar
            lblArticulo.Text = Textos(8) & ":" 'Artículo
            lblFecPedidoDesde.Text = Textos(12) & ":" 'Fecha de pedido
            lblFecPedidoHasta.Text = Textos(11) & ":" 'Hasta
            lblEmpresa.Text = Textos(9) & ":" 'Empresa
            lblFecEntregaSolicitDesde.Text = Textos(10) & ":" 'Fecha de entrega solicitada
            lblFecEntregaSolicitHasta.Text = Textos(11) & ":" 'Hasta
            lblNumPedProve.Text = Textos(7) & ":" 'Nº pedido proveedor
            lblFecEntregaIndicadaDesde.Text = Textos(53) & ":" 'Fecha de entrega indicada
            lblFecEntregaIndicadaHasta.Text = Textos(11) & ":" 'Hasta
            lblCentro.Text = Textos(31) & ":" 'Centro de Coste
            lblBusquedaAvanzada.Text = Textos(51) 'Seleccione los criterios de búsqueda generales
            btnBuscarBusquedaAvanzada.Text = Textos(13) ' Buscar
            btnLimpiarBusquedaAvanzada.Text = Textos(14) 'Limpiar
            btnConfigurar.Text = Textos(52) ' Configurar
            btnGuardarConfiguracion.Text = Textos(54) ' Salvar Configuración
            txtwmeArticulo.WatermarkText = Textos(55) 'Nombre, descripción…
            litExcel.InnerHtml = Textos(56) 'Excel
            litPDF.InnerHtml = Textos(57) 'PDF
            lblConfig.Text = Textos(69) 'Mostrar los siguientes Campos
            btnAceptar.Text = Textos(70) 'Aceptar
            btnCancelar.Text = Textos(71) 'Cancelar
            lblNumFactura.Text = Textos(81) & ":" 'Nº factura
            'carga de texto para divFacturaContent
            lblLitFactura.Text = Textos(79) 'Factura
            lblLitAlbaran.Text = Textos(17) 'Albaran
            lblLitArticuloFactura.Text = Textos(8) 'Artículo
            Me.whdgEntregas.GroupingSettings.EmptyGroupAreaText = Textos(74)
        End Sub

        ''' Revisado por: blp. Fecha:10/04/2012
        ''' <summary>
        ''' Tras la carga del grid se "sincroniza" con el Custom Pager
        ''' </summary>
        ''' <param name="sender">control</param>
        ''' <param name="e">evento de sistema</param>  
        ''' <remarks>Llamada desde: sistema; Tiempo máximo:0 </remarks>
        Private Sub whdgEntregas_GroupedColumnsChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GroupedColumnsChangedEventArgs) Handles whdgEntregas.GroupedColumnsChanged
            InicializarPaginador()
            upEntregas.Update()
        End Sub

        ''' Revisado por: blp. Fecha: 02/11/2011
        ''' <summary>
        ''' Recuperamos las entregas desde bdd
        ''' </summary>
        ''' <param name="botonBusqueda">Dependiendo el botón de búsqueda, se aplicarán unos filtros u otros</param>
        ''' <returns>Devuelve dataset con las entregas</returns>
        ''' <remarks>Llamada desde entregasPedidos.aspx.vb, máx 1 seg.
        ''' </remarks>
        Private Function ObtenerDatasetEntregas(ByVal botonBusqueda As TipoBotonBusqueda) As DataSet
            Dim oEntregas As PMPortalServer.Entregas = FSPMServer.Get_Entregas

            'Obtener filtros
            Dim dFecRecepDesde As Date
            Dim dFecRecepHasta As Date
            Dim bVerPedidosSinRecep As Boolean
            Dim sNumAlbaran As String
            Dim iAnio As Integer
            Dim iNumCesta As Integer
            Dim iNumPedido As Integer
            Dim sNumPedERP As String = String.Empty
            Dim sArticulo As String
            Dim dFecPedidoDesde As Date
            Dim dFecPedidoHasta As Date
            Dim iEmpresa As Integer
            Dim dFecEntregaSolicitDesde As Date
            Dim dFecEntregaSolicitHasta As Date
            Dim sNumPedProve As String
            Dim dFecEntregaIndicadaDesde As Date
            Dim dFecEntregaIndicadaHasta As Date
            Dim sNumFactura As String 'añadir factura

            dFecRecepDesde = IIf(txtFecRecepDesde.Valor Is DBNull.Value, Nothing, txtFecRecepDesde.Valor)
            dFecRecepHasta = IIf(txtFecRecepHasta.Valor Is DBNull.Value, Nothing, txtFecRecepHasta.Valor)
            bVerPedidosSinRecep = cbVerPedidosSinRecep.Checked
            sNumAlbaran = txtNumAlbaran.Text
            If (ddAnyo.SelectedValue IsNot Nothing AndAlso ddAnyo.SelectedValue <> String.Empty) Then
                iAnio = ddAnyo.SelectedValue
            Else
                iAnio = Nothing
            End If
            If (txtNumCesta.Text IsNot Nothing AndAlso txtNumCesta.Text <> String.Empty) Then
                iNumCesta = txtNumCesta.Text
            Else
                iNumCesta = Nothing
            End If
            If (txtNumPedido.Text IsNot Nothing AndAlso txtNumPedido.Text <> String.Empty) Then
                iNumPedido = txtNumPedido.Text
            Else
                iNumPedido = Nothing
            End If
            Dim Acceso As FSNLibrary.TiposDeDatos.ParametrosGenerales = Me.Acceso
            If Acceso.gbOblCodPedDir Or Acceso.gbOblCodPedido Then sNumPedERP = txtNumPedERP.Text
            sArticulo = txtArticulo.Text
            dFecPedidoDesde = IIf(txtFecPedidoDesde.Valor Is DBNull.Value, Nothing, txtFecPedidoDesde.Valor)
            dFecPedidoHasta = IIf(txtFecPedidoHasta.Valor Is DBNull.Value, Nothing, txtFecPedidoHasta.Valor)
            If (ddEmpresa.SelectedValue IsNot Nothing AndAlso ddEmpresa.SelectedValue <> String.Empty) Then
                iEmpresa = ddEmpresa.SelectedValue
            Else
                iEmpresa = Nothing
            End If
            dFecEntregaSolicitDesde = IIf(txtFecEntregaSolicitDesde.Valor Is DBNull.Value, Nothing, txtFecEntregaSolicitDesde.Valor)
            dFecEntregaSolicitHasta = IIf(txtFecEntregaSolicitHasta.Valor Is DBNull.Value, Nothing, txtFecEntregaSolicitHasta.Valor)
            sNumPedProve = txtNumPedProve.Text
            sNumFactura = txtNumFactura.Text
            dFecEntregaIndicadaDesde = IIf(txtFecEntregaIndicadaDesde.Valor Is DBNull.Value, Nothing, txtFecEntregaIndicadaDesde.Valor)
            dFecEntregaIndicadaHasta = IIf(txtFecEntregaIndicadaHasta.Valor Is DBNull.Value, Nothing, txtFecEntregaIndicadaHasta.Valor)

            Dim sCentro As String = String.Empty
            If Acceso.gbAccesoFSSM Then
                If (ddCentro.SelectedValue IsNot Nothing) Then
                    sCentro = ddCentro.SelectedValue
                Else
                    sCentro = String.Empty
                End If
            End If

            'Partidas presupuestarias
            Dim sPartidas As String = String.Empty
            'Si no se ha pulsado ningún botón de búsqueda y no hay nada filtrado en pantalla, hacemos la búsqueda básica.
            If botonBusqueda = TipoBotonBusqueda.Ninguno Then
                oEntregas.LoadData(FSPMUser.Idioma, IdCia, IdCiaComp, EmpresaPortal, NomPortal)
            ElseIf botonBusqueda = TipoBotonBusqueda.General Then
                'Sólo se filtra por los campos de búsqueda generales, aunque haya datos en los otros
                oEntregas.LoadData(FSPMUser.Idioma, IdCia, IdCiaComp, iAnio, iNumCesta, iNumPedido, sNumPedERP, dFecRecepDesde, dFecRecepHasta, bVerPedidosSinRecep, sNumAlbaran, "", "",
                                   Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, "", sPartidas, EmpresaPortal, NomPortal)
            ElseIf botonBusqueda = TipoBotonBusqueda.Avanzado Then
                If Acceso.gbAccesoFSSM Then
                    'Partidas presupuestarias
                    For Each tabla As Control In phrPartidasPresBuscador.Controls
                        If TypeOf tabla Is Table Then
                            For Each fila As Control In CType(tabla, Table).Rows
                                For Each celda As Control In CType(fila, TableRow).Cells
                                    For Each ddl As Control In CType(celda, TableCell).Controls
                                        If TypeOf ddl Is System.Web.UI.WebControls.DropDownList Then
                                            If Not String.IsNullOrEmpty(CType(ddl, System.Web.UI.WebControls.DropDownList).SelectedValue) Then
                                                Dim sPartida As String = CType(ddl, System.Web.UI.WebControls.DropDownList).SelectedValue.ToString
                                                'El string de la partida se compone de los códigos de cada pres0, 1, 2, etc, separados por @.
                                                'Pero si la partida es de nivel inferior a 4, no tiene separadores vacíos (o sea, algo como @@ o @@@)
                                                'Añadimos tantos @ como sean necesarios para completar la partida
                                                Dim numSeparadores As Integer = sPartida.Count(Function(c As Char) c = "@")
                                                If numSeparadores < 4 Then
                                                    For x As Integer = 0 To 4 - numSeparadores
                                                        sPartida += "@"
                                                    Next
                                                End If
                                            End If
                                        ElseIf TypeOf ddl Is System.Web.UI.WebControls.Label Then
                                            For Each ddl2 As Control In CType(ddl, Label).Controls
                                                If TypeOf ddl2 Is System.Web.UI.WebControls.DropDownList Then
                                                    If Not String.IsNullOrEmpty(CType(ddl2, System.Web.UI.WebControls.DropDownList).SelectedValue) Then
                                                        Dim sPartida As String = CType(ddl, System.Web.UI.WebControls.DropDownList).SelectedValue.ToString
                                                        'El string de la partida se compone de los códigos de cada pres0, 1, 2, etc, separados por @.
                                                        'Pero si la partida es de nivel inferior a 4, no tiene separadores vacíos (o sea, algo como @@ o @@@)
                                                        'Añadimos tantos @ como sean necesarios para completar la partida
                                                        Dim numSeparadores As Integer = sPartida.Count(Function(c As Char) c = "@")
                                                        If numSeparadores < 4 Then
                                                            For x As Integer = 0 To 4 - numSeparadores
                                                                sPartida += "@"
                                                            Next
                                                        End If
                                                        sPartidas += sPartida
                                                    End If
                                                End If
                                            Next
                                        End If
                                    Next
                                Next
                            Next
                        End If
                    Next
                End If
                oEntregas.LoadData(FSPMUser.Idioma, IdCia, IdCiaComp, iAnio, iNumCesta, iNumPedido, sNumPedERP, dFecRecepDesde, dFecRecepHasta, bVerPedidosSinRecep, sNumAlbaran,
                                   sNumPedProve, sArticulo, dFecEntregaSolicitDesde, dFecEntregaSolicitHasta, sNumFactura, iEmpresa, dFecEntregaIndicadaDesde, dFecEntregaIndicadaHasta,
                                   dFecPedidoDesde, dFecPedidoHasta, sCentro, sPartidas, EmpresaPortal, NomPortal)
            End If

            If Not oEntregas.Data Is Nothing Then Me.InsertarEnCache("dsEntregas_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString, oEntregas.Data)
            Return oEntregas.Data
        End Function

        ''' <summary>
        ''' Dataset de Entregas
        ''' </summary>
        Private ReadOnly Property DatasetEntregas(Optional ByVal botonOrigen As TipoBotonBusqueda = TipoBotonBusqueda.Ninguno) As DataSet
            Get
                If HttpContext.Current.Cache("dsEntregas_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString) IsNot Nothing Then
                    Return HttpContext.Current.Cache("dsEntregas_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString)
                Else
                    Return ObtenerDatasetEntregas(botonOrigen)
                End If
            End Get
        End Property

        ''' Revisado por: blp. Fecha:27/01/2012
        ''' <summary>
        ''' Método que se lanza al ordenar por una columna en el grid whdgEntregas. Actualiza el paginador y el grid para mostrar los cambios.
        ''' </summary>
        ''' <param name="sender">El control que lanza el evento</param>
        ''' <param name="e">argumentos del evento</param>
        ''' <remarks>Llamada desde el evento de filtrado. Máx 1 seg.</remarks>
        Private Sub whdgEntregas_ColumnSorted(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.SortingEventArgs) Handles whdgEntregas.ColumnSorted
            Me._PaginadorBottom.PageNumber = 1
            Me._PaginadorTop.PageNumber = 1
            InicializarPaginador()
            upEntregas.Update()
            CType(whdgEntregas.GridView.Behaviors.Paging.PagerTemplateContainerBottom.FindControl("wdgPaginador").FindControl("upPaginador"), UpdatePanel).Update()
            CType(whdgEntregas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("wdgPaginador").FindControl("upPaginador"), UpdatePanel).Update()
        End Sub

        ''' Revisado por: blp. Fecha:27/01/2012
        ''' <summary>
        ''' Método que se lanza al aplicar filtros en el grid whdgEntregas. Actualiza el paginador y el grid para mostrar los cambios.
        ''' </summary>
        ''' <param name="sender">El control que lanza el evento</param>
        ''' <param name="e">argumentos del evento</param>
        ''' <remarks>Llamada desde el evento de filtrado. Máx 1 seg.</remarks>
        Private Sub whdgEntregas_DataFiltered(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.FilteredEventArgs) Handles whdgEntregas.DataFiltered
            Me._PaginadorBottom.PageNumber = 1
            Me._PaginadorTop.PageNumber = 1
            InicializarPaginador()
            upEntregas.Update()
            CType(whdgEntregas.GridView.Behaviors.Paging.PagerTemplateContainerBottom.FindControl("wdgPaginador").FindControl("upPaginador"), UpdatePanel).Update()
            CType(whdgEntregas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("wdgPaginador").FindControl("upPaginador"), UpdatePanel).Update()

            'blp Incidencia 31900.5/2012/196 - 24152
            'En IE el control webhierarchicaldatagrid tiene un bug y no muestra bien el desplegable de los filtros cuando 
            'la altura del control no está fijada y hay un número pequeño de registros (de 0 a 5 aproximadamente)
            'Como el prototipo de la tarea 2126 pide que no se establezca altura al control (para que no haya scroll vertical en el grid),
            'vamos a determinar la altura del control por css sólo cuando haya una cantidad de registros baja.
            'Esto va a afectar visualmente a la pantalla pero de forma mínima (Al fijar la altura por css, 
            'el alto del recuadro de datos del grid se ajusta a los datos aunque el grid tenga un alto mayor,no visible, 
            'que crea un scroll vertical en la pantalla)
            Dim numRegistros As Long = 1
            If whdgEntregas.GridView.Behaviors.Filtering.Filtered Then
                whdgEntregas.GridView.Behaviors.Paging.Enabled = False
                numRegistros += whdgEntregas.GridView.Rows.Count
                whdgEntregas.GridView.Behaviors.Paging.Enabled = True
            Else
                If DatasetEntregas IsNot Nothing AndAlso DatasetEntregas.Tables("Entregas") IsNot Nothing AndAlso DatasetEntregas.Tables("Entregas").Rows.Count > 0 Then _
                    numRegistros += DatasetEntregas.Tables("Entregas").Rows.Count
            End If
            If numRegistros <= 5 Then
                'Me.whdgEntregas.Height = Unit.Pixel(500)
                Me.whdgEntregas.Style.Add("Height", "500px")
            Else
                Me.whdgEntregas.Style.Remove("Height")
            End If
        End Sub

        ''' Revisado por: blp. Fecha:27/01/2012
        ''' <summary>
        ''' Añade al dropdownprovider ddpwhdgEntregas dos opciones para poder filtrar por ellas
        ''' </summary>
        ''' <remarks>Llamada desde cargarwhdgEntregas. Máx. 0,1 seg.</remarks>
        Private Sub AnyadirOpcionesFiltradowhdgEntregas()
            'FILTRO RETRASADO
            Dim owhdgEntregasDropDownProvider As DropDownProvider = whdgEntregas.GridView.EditorProviders.GetProviderById("ddpwhdgEntregasRETRASADO")
            If owhdgEntregasDropDownProvider IsNot Nothing Then
                owhdgEntregasDropDownProvider.EditorControl.Items.Clear()
                Dim oDropDownItem As New Infragistics.Web.UI.ListControls.DropDownItem
                oDropDownItem.Text = String.Empty
                oDropDownItem.Value = String.Empty
                owhdgEntregasDropDownProvider.EditorControl.Items.Add(oDropDownItem)
                oDropDownItem = New Infragistics.Web.UI.ListControls.DropDownItem
                oDropDownItem.Text = Textos(67)
                oDropDownItem.Value = "Retrasado"
                owhdgEntregasDropDownProvider.EditorControl.Items.Add(oDropDownItem)
            End If
            'FILTRO IM_BLOQUEO
            owhdgEntregasDropDownProvider = Nothing
            owhdgEntregasDropDownProvider = whdgEntregas.GridView.EditorProviders.GetProviderById("ddpwhdgEntregasIM_BLOQUEO")
            If owhdgEntregasDropDownProvider IsNot Nothing Then
                owhdgEntregasDropDownProvider.EditorControl.Items.Clear()
                Dim oDropDownItem As New Infragistics.Web.UI.ListControls.DropDownItem
                oDropDownItem.Text = String.Empty
                oDropDownItem.Value = String.Empty
                owhdgEntregasDropDownProvider.EditorControl.Items.Add(oDropDownItem)
                oDropDownItem = New Infragistics.Web.UI.ListControls.DropDownItem
                oDropDownItem.Text = Textos(77)
                oDropDownItem.Value = "Bloqueado"
                owhdgEntregasDropDownProvider.EditorControl.Items.Add(oDropDownItem)
            End If

        End Sub

#Region "PartidasPresupuestarias"

        ''' Revisado por: blp. Fecha: 07/12/2011
        ''' <summary>
        ''' Función que nos devuelve un Datatable con las configuraciones de cada partida presupuestaria
        ''' </summary>
        ''' <returns>Datatable con las configuraciones de cada partida presupuestaria</returns>
        ''' <remarks>Llamado desde CargarCombosPartidas
        ''' Tiempo máximo: 0,1 sec</remarks>
        Private Function CargarPartidasPresupuestarias() As DataTable
            Dim PartidasPres = New DataTable("PartidasPres")
            PartidasPres.Columns.Add("PRES0", GetType(String))
            PartidasPres.Columns.Add("NIVEL_PRES0", GetType(String))
            PartidasPres.Columns.Add("PARTIDA_DEN", GetType(String))
            Dim oPRES0Cods As IEnumerable(Of String) = PRES0Cods(TipoOrigenPartidaPres.ListadoEntregas)
            If oPRES0Cods.Any Then
                For Each PRES0 As String In oPRES0Cods
                    Dim queryPartida = From nomPartida In DatasetEntregas.Tables("PARTIDAS_DEN").Rows _
                                   Select nomPartida(PRES0) Take 1
                    Dim sPartida As String = PRES0
                    If queryPartida(0).ToString <> String.Empty Then sPartida = queryPartida(0).ToString

                    Dim query = (From Datos In DatasetEntregas.Tables("ENTREGAS").AsEnumerable() _
                            Where (Not IsDBNull(Datos.Item("NIVEL_DEN_" & PRES0))) _
                            Select New With {Key .PRES0 = PRES0, Key .NIVEL_PRES0 = Datos.Item("NIVEL_DEN_" & PRES0), Key .PARTIDA_DEN = sPartida}).Distinct()

                    For Each partida In query
                        PartidasPres.Rows.Add(New Object() {partida.PRES0, partida.NIVEL_PRES0, partida.PARTIDA_DEN})
                    Next
                Next
            End If
            Return PartidasPres
        End Function

        '''Revisado por: blp. Fecha: 20/10/2011
        ''' <summary>
        ''' Procedimiento que crea tantos combos en el buscador como partidas presupuestarias se usen
        ''' Y rellena cada uno de los combos con las partidas que existan en las líneas de pedido de los pedidos emitidos por el usuario
        ''' IMPORTANTE:
        ''' Este método no puede ir dentro de InicializarValoresControles porque en InicializarValoresControles se da valor a controles que existen en el aspx
        ''' mientras que aquí se crean dinámicamente controles que añadimos al aspx
        ''' Por eso InicializarValoresControles se llama en el Load sólo en la carga inicial y CargarCombosPartidas en cada postback
        ''' </summary>
        ''' <remarks>Llamado desde Page_Load, btnBuscarGeneral_click, btnBuscarBusquedaAvanzada_click
        ''' Tiempo máximo: 0 sec</remarks>
        Private Sub CargarCombosPartidas()
            Dim dtPartidasPres As DataTable

            dtPartidasPres = CargarPartidasPresupuestarias()

            If dtPartidasPres.Rows.Count > 0 Then
                Dim TablaAsp As New Table
                Dim FilaAsp As New TableRow
                Dim Celda1Asp As New TableCell
                Dim Celda2Asp As New TableCell
                Dim query = From oPartidas In dtPartidasPres.AsEnumerable _
                            Select New With {Key .PRES0 = oPartidas.Item("PRES0"), Key .PARTIDA_DEN = oPartidas("PARTIDA_DEN")} Distinct
                'Para cada partida de la que hay datos en el grid, actualizaremos su combo en el buscador
                For Each oPres0 In query
                    Dim lista As New List(Of ListItem)
                    lista = PartidasPresup(oPres0.PRES0)
                    If lista.Any Then
                        If phrPartidasPresBuscador.FindControl("tbDdlPP") Is Nothing _
                        OrElse phrPartidasPresBuscador.FindControl("tbDdlPP").FindControl("tr_" & oPres0.PRES0) Is Nothing Then
                            'Insertar un combo en el panel de combos de partidas presupuestarias
                            'PlaceHolder->phrPartidasPresBuscador
                            Dim ddlPartida As New DropDownList()
                            Dim litPartida As New Literal()
                            Dim lblContenedor As New Label()
                            Celda1Asp = New TableCell
                            Celda2Asp = New TableCell
                            FilaAsp = New TableRow
                            Celda1Asp.Width = "140"
                            Celda2Asp.Width = "350"
                            Celda1Asp.Style.Add("padding-top", "5px")
                            Celda2Asp.Style.Add("padding-top", "5px")
                            Celda1Asp.HorizontalAlign = HorizontalAlign.Left
                            Celda1Asp.Style.Add("text-align", "left")
                            Celda2Asp.HorizontalAlign = HorizontalAlign.Right
                            litPartida.Text = oPres0.PARTIDA_DEN

                            ddlPartida.ID = "ddlPartidaPres_" & oPres0.PRES0
                            FilaAsp.ID = "tr_" & oPres0.PRES0
                            ddlPartida.DataTextField = "Text"
                            ddlPartida.DataValueField = "Value"
                            ddlPartida.Width = Unit.Percentage(100)
                            Celda1Asp.Controls.Add(litPartida)
                            Celda2Asp.Controls.Add(ddlPartida)
                            FilaAsp.Cells.Add(Celda1Asp)
                            FilaAsp.Cells.Add(Celda2Asp)
                            FilaAsp.Width = Unit.Percentage(100)
                            If phrPartidasPresBuscador.FindControl("tbDdlPP") Is Nothing Then
                                TablaAsp.Rows.Add(FilaAsp)
                            Else
                                'FilaAsp.Visible = True
                                phrPartidasPresBuscador.FindControl("tbDdlPP").Controls.Add(FilaAsp)
                            End If
                            'ARRIBA HAY UNA LÍNEA QUE HACE ESTO: lista = PartidasPresup(oPres0.PRES0)
                            'La función PartidasPresup devuelve un campo con el texto " - " y valor "" cuando hay registros sin partida, por lo quevamos a controlarlo:
                            If lista(0).Text = " - " Then
                                'Si se da el caso, vaciamos el texto
                                lista(0).Text = String.Empty
                            Else
                                'Si no, lo añadimos a la lista
                                lista.Insert(0, New ListItem(String.Empty, String.Empty))
                            End If
                            ddlPartida.DataSource = lista
                            ddlPartida.DataBind()
                        Else
                            'Modificar el combo contenido en la tabla
                            Dim tabladdls As Table = CType(phrPartidasPresBuscador.Controls(0), Table)
                            Dim trddl As TableRow = CType(tabladdls.FindControl("tr_" & oPres0.PRES0), TableRow)
                            Dim ddlPartida As DropDownList = TryCast(trddl.FindControl("ddlPartidaPres_" & oPres0.PRES0), DropDownList)
                            If ddlPartida IsNot Nothing Then
                                '--Primero guardamos el valor que pueda tener previamente seleccionado el dropdownlist
                                Dim sSelectedValue As String = ddlPartida.SelectedValue
                                'ARRIBA HAY UNA LÍNEA QUE HACE ESTO: lista = PartidasPresup(oPres0.PRES0)
                                'La función PartidasPresup devuelve un campo con el texto " - " y valor "" cuando hay registros sin partida, por lo quevamos a controlarlo:
                                If lista(0).Text = " - " Then
                                    'Si se da el caso, vaciamos el texto
                                    lista(0).Text = String.Empty
                                Else
                                    'Si no, lo añadimos a la lista
                                    lista.Insert(0, New ListItem(String.Empty, String.Empty))
                                End If
                                '--Le pasamos la nueva lista
                                ddlPartida.DataSource = lista
                                ddlPartida.DataBind()
                                '--Reseleccionamos el valor previamente elegido, si es que está en la lista
                                If ddlPartida.Items.FindByValue(sSelectedValue) IsNot Nothing Then
                                    ddlPartida.Items.FindByValue(sSelectedValue).Selected = True
                                End If
                            End If
                        End If
                    End If
                Next
                'Si hemos creado combos de partidas nuevas, los añadimos
                If phrPartidasPresBuscador.FindControl("tbDdlPP") Is Nothing Then
                    TablaAsp.Width = Unit.Percentage(100)
                    TablaAsp.CellPadding = 0
                    TablaAsp.CellSpacing = 0
                    TablaAsp.ID = "tbDdlPP"
                    phrPartidasPresBuscador.Controls.Add(TablaAsp)
                Else
                    'Determinamos la visibilidad de los combos
                    For Each oTR As Control In phrPartidasPresBuscador.FindControl("tbDdlPP").Controls
                        Dim ComboVisible As Boolean = False
                        For Each oPres0 In query
                            If oTR.ID = "tr_" & oPres0.PRES0 Then
                                ComboVisible = True
                                Exit For
                            End If
                        Next
                        oTR.Visible = ComboVisible
                    Next
                End If
            End If
        End Sub

        ''' Revisado por: blp. Fecha: 07/12/2011
        ''' <summary>
        ''' Función que devuelve un objeto List que contiene los nodos de una partida presupuestaria, presentes en los pedidos
        ''' </summary>
        ''' <param name="Pres0">String que indica a la función la partida presupuestaria de la que se quieren recuperar los nodos presentes en las líneas de pedido de las órdenes</param>
        ''' <returns>Objeto List que contiene los nodos de la partida presupuestaria seleccionada que están presentes en las líneas de pedido de las órdenes</returns>
        ''' <remarks>Llamado desde CargarCombosPartidas
        ''' Tiempo máximo: 0,1 sec</remarks>
        Public Function PartidasPresup(ByVal Pres0 As String) As List(Of ListItem)
            Dim query As List(Of ListItem) ', sPartidaDen As String
            query = From Datos In DatasetEntregas.Tables("ENTREGAS") _
                Order By Datos.Item("PARTIDA_COD_" & Pres0).ToString _
                Select New ListItem((Datos.Item("PARTIDA_COD_" & Pres0).ToString.Replace("###", "-").Replace("##", "-").Replace("#", "-") & " - " & Datos.Item("NIVEL_DEN_" & Pres0)).Replace("- -", " -"), Datos.Item("PARTIDA_COD_" & Pres0).ToString.Replace("###", "@").Replace("##", "@").Replace("#", "@")) _
                Distinct.ToList()
            Return query
        End Function

        ''' Revisado por: blp. Fecha: 07/12/2011
        ''' <summary>
        ''' Método mediante el cual cargamos en el control whdgEntregas los datos de entregas
        ''' </summary>
        ''' <param name="paraExportacion">
        '''      False->La carga del grid no es para exportación sino para mostrar en pantalla por lo que se pagina
        '''      True->La carga del grid es para exportación por lo que no se pagina a sólo saldrían en la exportación el número de registros indicado en la paginación
        ''' </param>
        ''' <param name="botonBusqueda">
        ''' Botón Búsqueda General -> Se filtra sólo por los criterios de búsqueda de la búsqueda general
        ''' Botón Búsqueda Avanzada -> Se filtra por todos los criterios de búsqueda (generales y avanzados)
        ''' </param>
        ''' <remarks>Llamada desde Load() y Exportar. Máx 1 seg</remarks>
        Private Sub CargarwhdgEntregas(ByVal paraExportacion As Boolean, ByVal botonOrigen As TipoBotonBusqueda)
            'Necesitamos que se genere el datasetEntregas para usarlo dentro del CreatewhdgEntregasColumns (si es que no está en caché, como por ejemplo al pulsar un botón de búsqueda)
            'por lo que primero lo generamos aquí y luego hacemos el databound
            whdgEntregas.DataSource = DatasetEntregas(botonOrigen)
            whdgEntregas.GridView.DataSource = whdgEntregas.DataSource
            'Los DataKeyFeilds deben definirse entre el Datasource y el Databind
            whdgEntregas.DataMember = "ENTREGAS"
            whdgEntregas.DataKeyFields = "LINEAPEDID, LINEARECEPID"
            'Añadir campos al webdatagrid
            CreatewhdgEntregasColumnsYConfiguracion()
            whdgEntregas.DataBind()

            If Not paraExportacion Then
                whdgEntregas.GridView.Behaviors.Paging.Enabled = True
                whdgEntregas.GridView.Behaviors.Paging.PageSize = ConfigurationManager.AppSettings("PaginacionEntregas")
            Else
                whdgEntregas.GridView.Behaviors.Paging.Enabled = False
            End If

            'Añadir filtro personalizado a columna Retrasado
            AnyadirOpcionesFiltradowhdgEntregas()

            'blp Incidencia 31900.5/2012/196 - 24152
            'En IE el control webhierarchicaldatagrid tiene un bug y no muestra bien el desplegable de los filtros cuando 
            'la altura del control no está fijada y hay un número pequeño de registros (de 0 a 5 aproximadamente)
            'Como el prototipo de la tarea 2126 pide que no se establezca altura al control (para que no haya scroll vertical en el grid),
            'vamos a determinar la altura del control por css sólo cuando haya una cantidad de registros baja.
            'Esto va a afectar visualmente a la pantalla pero de forma mínima (Al fijar la altura por css, 
            'el alto del recuadro de datos del grid se ajusta a los datos aunque el grid tenga un alto mayor,no visible, 
            'que crea un scroll vertical en la pantalla)
            'vamos a determinar la altura mínima del control sólo cuando haya una cantidad de registros baja
            If whdgEntregas.DataSource IsNot Nothing AndAlso CType(whdgEntregas.DataSource, DataSet).Tables.Count > 0 AndAlso CType(whdgEntregas.DataSource, DataSet).Tables(0).Rows.Count <= 5 Then
                'Me.whdgEntregas.Height = Unit.Pixel(500)
                Me.whdgEntregas.Style.Add("Height", "500px")
            Else
                Me.whdgEntregas.Style.Remove("Height")
            End If




        End Sub


#End Region

#Region "Empresas"
        ''' Revisado por: blp. Fecha: 22/11/2011
        ''' <summary>
        ''' Función que devuelve un objeto List con las empresas de coste presentes en las entregas.
        ''' </summary>
        ''' <returns>Un dataset con las empresas de coste presentes en las entregas.</returns>
        ''' <remarks>
        ''' Llamada desde: entregasPedidos.vb.InicializarValoresControles. Máx 0,2 seg</remarks>
        Public Function CargarEmpresas() As List(Of ListItem)
            Dim query As List(Of ListItem) = From Datos In DatasetEntregas.Tables("ENTREGAS") _
                    Order By Datos.Item("DENEMPRESA") _
                      Select New ListItem(Datos.Item("DENEMPRESA"), Datos.Item("IDEMPRESA")) Distinct.ToList()
            Return query
        End Function

#End Region

#Region "CentrosdeCoste"
        ''' Revisado por: blp. Fecha: 22/11/2011
        ''' <summary>
        ''' Función que devuelve un objeto List con los Centros de coste presentes en las entregas.
        ''' </summary>
        ''' <returns>Un dataset con los Centros de coste presentes en las entregas.</returns>
        ''' <remarks>
        ''' Llamada desde: entregasPedidos.vb.InicializarValoresControles. Máx 0,2 seg</remarks>
        Public Function CargarCentrosDeCoste() As List(Of ListItem)
            Dim query As List(Of ListItem)
            query = From Datos In DatasetEntregas.Tables("CENTROSCOSTE") _
                        Order By Datos.Item("CCDEN") Ascending _
                        Select New ListItem(Datos.Item("CCDEN").ToString, Datos.Item("CCCOD").ToString) Distinct.ToList()
            Return query
        End Function

#End Region

#Region "Anyos"
        ''' Revisado por: blp. Fecha: 22/11/2011
        ''' <summary>
        ''' Función que devuelve un objeto List con los Años de coste presentes en las entregas.
        ''' </summary>
        ''' <returns>Un dataset con los Años de coste presentes en las entregas.</returns>
        ''' <remarks>
        ''' Llamada desde: entregasPedidos.vb.InicializarValoresControles. Máx 0,2 seg</remarks>
        Public Function CargarAnyos() As List(Of ListItem)
            Dim query As List(Of ListItem) = From Datos In DatasetEntregas.Tables("ENTREGAS") _
                                           Order By Datos.Item("ANYO") _
                                           Select New ListItem(Datos.Item("ANYO"), Datos.Item("ANYO")) Distinct.ToList()
            Return query
        End Function
#End Region

        ''' Revisado por: blp .Fecha: 18/11/2011
        ''' <summary>
        ''' Inserta los valores iniciales en los filtros: último mes de fecha de recepción y ver pedidos sin recepciones activado
        ''' Configura los combos
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub InicializarValoresControles()
            If Not IsPostBack Then
                txtFecRecepDesde.Valor = DateTime.Now.AddMonths(-1).Date
                txtFecRecepHasta.Valor = DateTime.Now.Date
                cbVerPedidosSinRecep.Checked = True
                txtArticulo_AutoCompleteExtender.ContextKey = FSPMUser.Cod.ToString & "_" & FSPMUser.Idioma.ToString
            End If

            Dim lista As List(Of ListItem)
            ' DropDown Años
            lista = CargarAnyos()
            If lista.Any Then _
             lista.Insert(0, New ListItem(String.Empty, String.Empty))
            'Conservar seleccionado el valor del dropdown
            Dim anyoSeleccionado As String = String.Empty
            If ddAnyo.SelectedValue IsNot Nothing Then
                anyoSeleccionado = ddAnyo.SelectedValue
            End If
            ddAnyo.DataSource = lista
            If lista.Exists(Function(anyo As ListItem) anyo.Value = anyoSeleccionado) Then _
                ddAnyo.SelectedValue = lista.Find(Function(anyo As ListItem) anyo.Value = anyoSeleccionado).Value
            ddAnyo.DataBind()
            ' DropDown Empresa
            lista = CargarEmpresas()
            If lista.Any Then _
             lista.Insert(0, New ListItem(String.Empty, String.Empty))
            'Conservar seleccionado el valor del dropdown
            Dim EmpresaSeleccionada As String
            If ddEmpresa.SelectedValue IsNot Nothing Then
                EmpresaSeleccionada = ddEmpresa.SelectedValue
            End If
            ddEmpresa.DataSource = lista
            If lista.Exists(Function(Esa As ListItem) Esa.Value = EmpresaSeleccionada) Then _
                ddEmpresa.SelectedValue = lista.Find(Function(Esa As ListItem) Esa.Value = EmpresaSeleccionada).Value
            ddEmpresa.DataBind()
            'DropDown(Centros de Coste)
            lista = CargarCentrosDeCoste()
            If lista.Any Then _
             lista.Insert(0, New ListItem(String.Empty, String.Empty))
            'Conservar seleccionado el valor del dropdown
            Dim CentroSeleccionado As String
            If ddCentro.SelectedValue IsNot Nothing Then
                CentroSeleccionado = ddCentro.SelectedValue
            End If
            ddCentro.DataSource = lista
            If lista.Exists(Function(Centro As ListItem) Centro.Value = CentroSeleccionado) Then _
                ddCentro.SelectedValue = lista.Find(Function(Centro As ListItem) Centro.Value = CentroSeleccionado).Value
            ddCentro.DataBind()

            Dim Acceso As FSNLibrary.TiposDeDatos.ParametrosGenerales = Me.Acceso
            txtNumPedERP.Visible = (Acceso.gbOblCodPedDir Or Acceso.gbOblCodPedido)
            lblNumPedERP.Visible = (Acceso.gbOblCodPedDir Or Acceso.gbOblCodPedido)

            'Actualizar el panel de búsqueda
            upBusqueda.Update()
        End Sub

        ''' Revisado por: blp. Fecha: 14/12/2011
        ''' <summary>
        ''' Método que se lanza en el evento de click del botón de búsqueda general
        ''' Limpia los datos guardados, carga los de la búsqueda, reinicia el paginador
        ''' </summary>
        ''' <param name="sender">control que lanza el evento</param>
        ''' <param name="e">Argumentos del evento</param>
        ''' <remarks>llamada desde el evento. Máx. 1 seg.</remarks>
        Private Sub btnBuscarGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarGeneral.Click
            BorrarCacheEntregas()
            CargarwhdgEntregas(False, TipoBotonBusqueda.General)
            InicializarValoresControles()
            'Recorremos los controles del panel de búsqueda avanzada y los vamos limpiando según el tipo
            VaciarControlesRecursivo(pnlParametros)
            'Crear los combos de partidas presupuestarias y llenarlos de contenido
            If Me.Acceso.gbAccesoFSSM Then
                CargarCombosPartidas()
            End If
            InicializarPaginador()
            RellenarLabelFiltros()
            upEntregas.Update()
        End Sub

        ''' Revisado por: blp. Fecha: 14/12/2011
        ''' <summary>
        ''' Método que se lanza en el evento de click del botón de búsqueda avanzada
        ''' Limpia los datos guardados, carga los de la búsqueda, reinicia el paginador
        ''' </summary>
        ''' <param name="sender">control que lanza el evento</param>
        ''' <param name="e">Argumentos del evento</param>
        ''' <remarks>llamada desde el evento. Máx. 1 seg.</remarks>
        Private Sub btnBuscarBusquedaAvanzada_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarBusquedaAvanzada.Click
            BorrarCacheEntregas()
            CargarwhdgEntregas(False, TipoBotonBusqueda.Avanzado)
            InicializarValoresControles()
            'Crear los combos de partidas presupuestarias y llenarlos de contenido
            If Me.Acceso.gbAccesoFSSM Then
                CargarCombosPartidas()
            End If
            InicializarPaginador()
            RellenarLabelFiltros()
            upEntregas.Update()
        End Sub

        ''' Revisado por: blp. Fecha: 07/12/2011
        ''' <summary>
        ''' Método que borra de la caché los datos de entregas
        ''' </summary>
        ''' <remarks>Llamada desde los botones btnBuscarGeneral y btnBusquedaAvanzada. Máx. 0,2 seg</remarks>
        Private Sub BorrarCacheEntregas()
            If HttpContext.Current.Cache("dsEntregas_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString) IsNot Nothing Then _
                HttpContext.Current.Cache.Remove("dsEntregas_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString)
        End Sub

        ''' <summary>
        ''' Tipos de botones desde los que se puede solicitar la carga del listado de entregas
        ''' </summary>
        Public Enum TipoBotonBusqueda
            Ninguno = 0
            General = 1
            Avanzado = 2
        End Enum

        ''' <summary>
        ''' Tipos de origen desde los que se pueden recuperar partidas
        ''' </summary>
        Public Enum TipoOrigenPartidaPres
            ListadoEntregas = 0
            ConfiguracionSM = 1
        End Enum

        ''' Revisado por: blp. Fecha:22/11/2011
        ''' <summary>
        ''' Método que limpia los criterios de búsqueda.
        ''' </summary>
        ''' <param name="sender">Control btnLimpiarBuscquedaAvanzada</param>
        ''' <param name="e">parámetros del evento</param>
        ''' <remarks>llamada desde el control, al hacer click. Tiempo máximo inferios a 0,1 seg.</remarks>
        Private Sub btnLimpiarBusquedaAvanzada_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarBusquedaAvanzada.Click
            'Recorremos los controles del panel y los vamos limpiando según el tipo
            VaciarControlesRecursivo(pnlParametros)
            'Recorremos los controles del panel y los vamos limpiando según el tipo
            VaciarControlesRecursivo(tblBusquedaGeneral)
            'Ocultamos el label con el resumen de los filtros generales
            divBusqueda.Style("display") = "none"
        End Sub

        ''' Revisado por: blp. Fecha:22/11/2011
        ''' <summary>
        ''' Método que recorre de manera recursiva el control recibido y vacía las selecciones de todos los controles
        ''' </summary>
        ''' <param name="oControlBase">Control a recorrer</param>
        ''' <remarks>Llamada desde btnLimpiar_Click</remarks>
        Private Sub VaciarControlesRecursivo(ByVal oControlBase As Object)
            For Each oControl In oControlBase.Controls
                Select Case oControl.GetType().ToString
                    Case GetType(DropDownList).ToString
                        If CType(oControl, DropDownList).SelectedItem IsNot Nothing Then _
                            CType(oControl, DropDownList).SelectedItem.Selected = False
                    Case GetType(TextBox).ToString
                        CType(oControl, TextBox).Text = String.Empty
                    Case GetType(Infragistics.WebUI.WebSchedule.WebDateChooser).ToString
                        CType(oControl, Infragistics.WebUI.WebSchedule.WebDateChooser).Value = Nothing
                    Case GetType(CheckBox).ToString
                        CType(oControl, CheckBox).Checked = False
                    Case GetType(Fullstep.DataEntry.GeneralEntry).ToString
                        'Actualmente hay un único tipo de control generalEntry (TipoFecha) en la página.
                        'Si se añaden más, es posible que haya que añadir un select case para contemplar cada caso.
                        CType(oControl, Fullstep.DataEntry.GeneralEntry).Valor = Nothing
                        CType(CType(oControl, Fullstep.DataEntry.GeneralEntry).Controls(0).Controls(0).Controls(0).Controls(0), Infragistics.WebUI.WebDataInput.WebDateTimeEdit).Value = Nothing
                    Case Else
                        If oControl.Controls IsNot Nothing AndAlso oControl.Controls.Count > 0 Then
                            VaciarControlesRecursivo(oControl)
                        End If
                End Select
            Next
        End Sub

        ''' Revisado por: blp. Fecha: 30/01/2012
        ''' <summary>
        ''' Método que edita las condiciones de las celdas del PDF exportado
        ''' </summary>
        ''' <param name="sender">Control que lanza el evento</param>
        ''' <param name="e">argumentos del evento</param>
        ''' <remarks>Llamada desde evento CellExported del control WebDocumentExporter1. Máx. 0,1 seg.</remarks>
        Private Sub WebDocumentExporter1_CellExported(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.DocumentCellExportedEventArgs) Handles WebDocumentExporter1.CellExported
            If e.IsFooterCell Then
                e.ReportCellElement.Borders.All = New Infragistics.Documents.Reports.Report.Border(New Infragistics.Documents.Reports.Graphics.Pen(New Infragistics.Documents.Reports.Graphics.Color(System.Drawing.Color.Black)))
                e.ReportCellElement.Paddings.All = 1
            End If
        End Sub

        ''' Revisado por: blp. Fecha: 07/12/2011
        ''' <summary>
        ''' Crea las columnas fijas del grid en función de los datos guardados en la configuración (tabla FSP_CONF_VISOR_RECEPCIONES)
        ''' y añade los campos al panel de configuración
        ''' </summary>
        ''' <remarks>Llamada desde CargarwhdgEntregas. Máx. 0,5 seg</remarks>
        Private Sub CreatewhdgEntregasColumnsYConfiguracion()
            Dim oParametros As PMPortalServer.Parametros = FSPMServer.Get_Parametros
            Me.EditColumnYConfiguracion("RETRASADO", Textos(67))
            If bCodificacionPersonalizadaRecepcionesActivada Then
                Me.EditColumnYConfiguracion("CODRECEPERP", oParametros.CargarLiteralParametros(47, Me.FSPMUser.Idioma, IdCiaComp)) 'Nº Albarán en ERP
            Else
                If whdgEntregas.Columns("Key_CODRECEPERP") IsNot Nothing Then _
                    whdgEntregas.Columns("Key_CODRECEPERP").Hidden = True
                whdgEntregas.GridView.Columns("Key_CODRECEPERP").Hidden = True
            End If
            Me.EditColumnYConfiguracion("ALBARAN", Textos(17)) 'Albarán
            Me.EditColumnYConfiguracion("FECHARECEP", Textos(18), Textos(59)) 'Fec.Recepción
            Me.EditColumnYConfiguracion("DENEMPRESA", Textos(9)) 'Empresa

            Dim Acceso As FSNLibrary.TiposDeDatos.ParametrosGenerales = Me.Acceso
            If Acceso.gbOblCodPedDir Or Acceso.gbOblCodPedido Then
                Me.EditColumnYConfiguracion("NUM_PED_ERP", oParametros.CargarLiteralParametros(Fullstep.FSNLibrary.TiposDeDatos.LiteralesParametros.PedidoERP, FSPMUser.Idioma, IdCiaComp)) 'Literal del num pedido ERP
            Else
                If whdgEntregas.Columns("Key_NUM_PED_ERP") IsNot Nothing Then _
                    whdgEntregas.Columns("Key_NUM_PED_ERP").Hidden = True
                whdgEntregas.GridView.Columns("Key_NUM_PED_ERP").Hidden = True
            End If
            Me.EditColumnYConfiguracion("ANYOPEDIDOORDEN", Textos(19), Textos(5)) 'Num. Fullstep
            Me.EditColumnYConfiguracion("NUMPEDPROVE", Textos(7)) 'Num. Fullstep
            Me.EditColumnYConfiguracion("FECHAEMISION", Textos(20), Textos(12)) 'Fec. Pedido
            Me.EditColumnYConfiguracion("FECENTREGASOLICITADA", Textos(21), Textos(10)) 'F.Entrega Sol.
            Me.EditColumnYConfiguracion("FECENTREGAINDICADAPROVE", Textos(58), Textos(53)) 'Fec.Entrega Ind.
            Me.EditColumnYConfiguracion("ARTDEN", Textos(8)) 'Artículo
            Me.EditColumnYConfiguracion("CANTLINEAPEDIDATOTAL", Textos(22), Textos(60)) 'Pedido
            Me.EditColumnYConfiguracion("CANTLINEARECIBIDAPARCIAL", Textos(23), Textos(61)) 'Recibido
            Me.EditColumnYConfiguracion("CANTLINEAPENDIENTETOTAL", Textos(24), Textos(62)) 'Pendiente
            Me.EditColumnYConfiguracion("UNIDEN", Textos(25)) 'Unidad
            Me.EditColumnYConfiguracion("PREC_UP", Textos(26), Textos(63)) 'Prec.Uni
            Me.EditColumnYConfiguracion("IMPORTELINEATOTAL", Textos(64), Textos(27)) 'Imp.Pedido
            Me.EditColumnYConfiguracion("IMPORTELINEARECIBIDOPARCIAL", Textos(28), Textos(65)) 'Imp.Recib.
            Me.EditColumnYConfiguracion("IMPORTELINEAPENDIENTETOTAL", Textos(29), Textos(66)) 'Imp.Pendiente
            Me.EditColumnYConfiguracion("MONDEN", Textos(30)) 'Moneda
            Me.EditColumnYConfiguracion("NUMLINEA", Textos(78)) 'Línea
            Me.EditColumnYConfiguracion("RECEPTOR", Textos(80)) 'Receptor MPF 
            Me.EditColumnYConfiguracion("IMPORTETOTALALBARAN", Textos(85)) 'Importe Total albaran NSA
            If Acceso.g_bAccesoFSFA Then
                Me.EditColumnYConfiguracion("IM_BLOQUEO", Textos(76), Textos(77)) 'Bloqueo Factura
                Me.EditColumnYConfiguracion("FACTURA", Textos(79)) 'Factura 
            Else
                If whdgEntregas.Columns("Key_IM_BLOQUEO") IsNot Nothing Then _
                    whdgEntregas.Columns("Key_IM_BLOQUEO").Hidden = True
                whdgEntregas.GridView.Columns("Key_IM_BLOQUEO").Hidden = True
                whdgEntregas.Columns("Key_FACTURA").Hidden = True
                whdgEntregas.GridView.Columns("Key_FACTURA").Hidden = True

            End If
            'CENTROS DE COSTE Y PARTIDAS
            If Acceso.gbAccesoFSSM Then
                Dim oPRES0Cods = PRES0Cods(TipoOrigenPartidaPres.ConfiguracionSM)
                If oPRES0Cods.Any Then
                    For Each columnName As String In oPRES0Cods
                        If whdgEntregas.GridView.Columns.Item("Key_CCDEN_" & columnName) Is Nothing Then _
                            Me.AddColumnYConfiguracion("CCDEN_" & columnName, Textos(31)) 'Centro de Coste
                        'Añadir al panel de configuración
                        anyadirAPanelConfiguracion("CCDEN_" & columnName, Textos(31))

                        Dim queryPartida = From nomPartida In DatasetEntregas.Tables("PARTIDAS_DEN").Rows _
                                       Select nomPartida(columnName) Take 1
                        Dim sPartida As String = columnName
                        If queryPartida(0).ToString <> String.Empty Then sPartida = queryPartida(0).ToString
                        If whdgEntregas.GridView.Columns.Item("Key_NIVEL_DEN_" & columnName) Is Nothing Then _
                            Me.AddColumnYConfiguracion("NIVEL_DEN_" & columnName, sPartida)
                        'Añadir al panel de configuración
                        anyadirAPanelConfiguracion("NIVEL_DEN_" & columnName, sPartida)
                    Next
                End If
            End If

            'Configurar Anchos y Visibilidad
            confAnchosYVisibilidad()
            'Cargar desde base de datos la info del panel de configuración
            CargarInfoPanelConfiguracion()
        End Sub

        ''' Revisado por: blp. Fecha: 12/12/2011
        ''' <summary>
        ''' Función que añade una columna al control whdgEntregas, vinculada al origen de datos de éste.
        ''' </summary>
        ''' <param name="fieldName">Nombre del campo en el origen de datos</param>
        ''' <param name="headerText">Título de la columna</param>
        ''' <param name="headerTooltip">Tooltip de la columna</param>
        ''' <remarks>Llamada desde CreatewhdgEntregasColumns. Máx. 0,1 seg.</remarks>
        Private Sub AddColumnYConfiguracion(ByVal fieldName As String, ByVal headerText As String, Optional ByVal headerTooltip As String = "")
            Dim field As New BoundDataField(True)
            field.Key = "Key_" + fieldName
            field.DataFieldName = fieldName
            field.Header.Text = headerText
            If headerTooltip = String.Empty Then headerTooltip = headerText
            field.Header.Tooltip = headerTooltip
            If Me.whdgEntregas.Columns IsNot Nothing AndAlso Me.whdgEntregas.Columns(field.Key) Is Nothing Then _
                Me.whdgEntregas.Columns.Add(field)
            Me.whdgEntregas.GridView.Columns.Add(field)
            anyadirTextoAColumnaNoFiltrada("Key_" & fieldName)
            'Añadir al panel de configuración
            anyadirAPanelConfiguracion(fieldName, headerText)
        End Sub

        ''' Revisado por: blp. Fecha: 12/12/2011
        ''' <summary>
        ''' Función que añade una columna al control whdgEntregas, vinculada al origen de datos de éste.
        ''' Añade el mismo campo al panel de configuración
        ''' </summary>
        ''' <param name="fieldName">Nombre del campo en el origen de datos</param>
        ''' <param name="headerText">Título de la columna</param>
        ''' <param name="headerTooltip">Tooltip de la columna</param>
        ''' <remarks>Llamada desde CreatewhdgEntregasColumns. Máx. 0,1 seg.</remarks>
        Private Sub EditColumnYConfiguracion(ByVal fieldName As String, ByVal headerText As String, Optional ByVal headerTooltip As String = "")
            'Configurar el grid
            If Not whdgEntregas.GridView.Columns.Item("Key_" & fieldName) Is Nothing Then
                With whdgEntregas.GridView.Columns.Item("Key_" & fieldName)
                    .Header.Text = headerText
                    If headerTooltip = String.Empty Then headerTooltip = headerText
                    .Header.Tooltip = headerTooltip
                End With
                If whdgEntregas.Columns.Item("Key_" & fieldName) IsNot Nothing Then
                    whdgEntregas.Columns.Item("Key_" & fieldName).Header.Text = headerText
                    If headerTooltip = String.Empty Then headerTooltip = headerText
                    whdgEntregas.Columns.Item("Key_" & fieldName).Header.Tooltip = headerTooltip
                End If
                anyadirTextoAColumnaNoFiltrada("Key_" & fieldName)
            End If
            'Añadir al panel de configuración
            anyadirAPanelConfiguracion(fieldName, headerText)
        End Sub

        ''' Revisado por: blp. Fecha: 10/04/2012
        ''' <summary>
        ''' Añade el alt "Filtro no aplicado" a las columnas no filtradas del whdgEntregas
        ''' </summary>
        ''' <param name="fieldKey">Key de la columna a la que queremos añadir el texto</param>
        ''' <remarks>Llamada desde EditColumnYConfiguracion y AddColumnYConfiguracion. Máx. 0,1 seg.</remarks>
        Private Sub anyadirTextoAColumnaNoFiltrada(ByVal fieldKey As String)
            Dim fieldSetting As Infragistics.Web.UI.GridControls.ColumnFilteringSetting
            fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
            fieldSetting.ColumnKey = fieldKey
            fieldSetting.BeforeFilterAppliedAltText = Textos(75) 'Filtro no aplicado
            Me.whdgEntregas.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
            Me.whdgEntregas.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
        End Sub


        ''' Revisado por: blp. Fecha: 30/01/2012
        ''' <summary>
        ''' Configurar Anchos y Visibilidad de los campos del webhierarchicaldatagrid
        ''' </summary>
        ''' <remarks>Llamada desde CreatewhdgEntregasColumns. Máx. 0,1 seg.</remarks>
        Private Sub confAnchosYVisibilidad()
            'Obtenemos la relación entre los campos de la tabla de configuracion de anchos y visibilidad con los campos de la tabla de entregas
            Dim camposEntregasYConfiguracion As List(Of Campos) = relacionarEntregasyConfiguracion()
            'Coger los datos de ancho y visibilidad de la tabla desde base de datos
            Dim oEntregas As PMPortalServer.Entregas = FSPMServer.Get_Entregas
            oEntregas.Obt_Conf_Visor_Entregas(FSPMUser.Cod, IdCia)
            Dim dtConfiguracionEntregas As DataTable = oEntregas.DataConfiguration
            'whdgEntregas.DefaultColumnWidth = "43"
            If dtConfiguracionEntregas IsNot Nothing AndAlso dtConfiguracionEntregas.Rows.Count > 0 Then
                'Cuando el número de campos en el grid es mayor que el campos en la tabla de configuración (y en esa tabla hay dos columnas por cada campo, por eso su valor es (dtConfiguracionEntregas.Columns.Count / 2)
                'significa que hay más campos de Partidas de los 4 previstos (o que se han añadido campos al grid y no a la tabla de configuración pero esa es otra historia).
                'Cuando haya datos configurados esos campos NO LOS VAMOS A MOSTRAR
                'lo cual, si alguna vez ocurre que hay más de 4 (harto improbable) es un fallo pero son las limitaciones del sistema que hemos creado
                'If whdgEntregas.GridView.Columns.Count > dtConfiguracionEntregas.Rows.Count Then
                If whdgEntregas.GridView.Columns.Count > (IIf(dtConfiguracionEntregas.Columns.Count > 0, dtConfiguracionEntregas.Columns.Count / 2, 0)) Then
                    For Each oCampoGrid As GridField In whdgEntregas.GridView.Columns
                        Dim nombreColumna As String = oCampoGrid.Key.ToString.Replace("Key_", "")
                        Dim campoConfig As DataColumn = dtConfiguracionEntregas.Columns.Item(nombreColumna)
                        If campoConfig Is Nothing Then
                            oCampoGrid.Hidden = True
                        End If
                    Next
                End If

                For Each oColumn As DataColumn In dtConfiguracionEntregas.Columns
                    If oColumn.ColumnName.IndexOf("_VISIBLE") > 0 Then
                        Dim visible As Boolean = dtConfiguracionEntregas.Rows(0).Item(oColumn.ColumnName)
                        Dim campo As Campos = camposEntregasYConfiguracion.Find(Function(x As Campos) x.CampoConfiguracionVisible = oColumn.ColumnName)
                        If campo.CampoEntrega IsNot Nothing Then
                            If whdgEntregas.Columns("Key_" & campo.CampoEntrega) IsNot Nothing Then _
                                whdgEntregas.Columns("Key_" & campo.CampoEntrega).Hidden = Not visible
                            whdgEntregas.GridView.Columns("Key_" & campo.CampoEntrega).Hidden = Not visible
                        End If
                    ElseIf oColumn.ColumnName.IndexOf("_WIDTH") > 0 Then
                        Dim ancho As Integer = dtConfiguracionEntregas.Rows(0).Item(oColumn.ColumnName)
                        Dim campo As Campos = camposEntregasYConfiguracion.Find(Function(x As Campos) x.CampoConfiguracionWidth = oColumn.ColumnName)
                        If ancho > 0 Then
                            If campo.CampoEntrega IsNot Nothing Then
                                If whdgEntregas.Columns("Key_" & campo.CampoEntrega) IsNot Nothing Then _
                                    whdgEntregas.Columns("Key_" & campo.CampoEntrega).Width = Unit.Pixel(ancho)
                                whdgEntregas.GridView.Columns("Key_" & campo.CampoEntrega).Width = Unit.Pixel(ancho)
                            End If
                        Else
                            If campo.CampoEntrega IsNot Nothing Then
                                If whdgEntregas.Columns("Key_" & campo.CampoEntrega) IsNot Nothing Then _
                                    whdgEntregas.Columns("Key_" & campo.CampoEntrega).Width = Unit.Pixel(campo.Width)
                                whdgEntregas.GridView.Columns("Key_" & campo.CampoEntrega).Width = Unit.Pixel(campo.Width)
                            End If
                        End If
                    End If
                Next
            Else
                For Each oColumna As GridField In whdgEntregas.GridView.Columns
                    Dim campo As Campos
                    If oColumna.Key = "Key_NUM_PED_ERP" OrElse oColumna.Key = "Key_FACTURA" OrElse oColumna.Key = "Key_IM_BLOQUEO" Then
                        Dim Acceso As FSNLibrary.TiposDeDatos.ParametrosGenerales = Me.Acceso
                        If (oColumna.Key = "Key_NUM_PED_ERP" AndAlso (Acceso.gbOblCodPedDir Or Acceso.gbOblCodPedido)) _
                            OrElse _
                           ((oColumna.Key = "Key_FACTURA" OrElse oColumna.Key = "Key_IM_BLOQUEO") AndAlso Acceso.g_bAccesoFSFA) _
                           OrElse (oColumna.Key = "Key_CODRECEPERP" AndAlso bCodificacionPersonalizadaRecepcionesActivada) Then
                            oColumna.Hidden = False
                            campo = camposEntregasYConfiguracion.Find(Function(x As Campos) x.CampoEntrega = oColumna.Key.Replace("Key_", ""))
                            oColumna.Width = Unit.Pixel(campo.Width)
                        End If
                    Else
                        oColumna.Hidden = False
                        campo = camposEntregasYConfiguracion.Find(Function(x As Campos) x.CampoEntrega = oColumna.Key.Replace("Key_", ""))
                        oColumna.Width = Unit.Pixel(campo.Width)
                    End If
                    
                    If whdgEntregas.Columns("Key_" & campo.CampoEntrega) IsNot Nothing Then _
                        whdgEntregas.Columns("Key_" & campo.CampoEntrega).Width = Unit.Pixel(campo.Width)
                Next
            End If
            'Forzar la no visibilidad de las columnas ocultas. Bug de Infragistics 11.2.20112.2225
            If whdgEntregas.Columns("Key_INSTANCIA") IsNot Nothing Then _
                whdgEntregas.Columns("Key_INSTANCIA").Hidden = True
            If whdgEntregas.Columns("Key_FACTURAID") IsNot Nothing Then _
                whdgEntregas.Columns("Key_FACTURAID").Hidden = True
            If whdgEntregas.Columns("Key_ARTCOD") IsNot Nothing Then _
                whdgEntregas.Columns("Key_ARTCOD").Hidden = True
            whdgEntregas.GridView.Columns("Key_INSTANCIA").Hidden = True
            whdgEntregas.GridView.Columns("Key_FACTURAID").Hidden = True
            whdgEntregas.GridView.Columns("Key_ARTCOD").Hidden = True
        End Sub

        ''' Revisado por: blp. Fecha: 30/01/2012
        ''' <summary>
        ''' Añade al panel de configuración un checkbox con el campo pasado por parámetros
        ''' </summary>
        ''' <param name="fieldName">Código del campo</param>
        ''' <param name="fieldDen">Denominación del campo</param>
        ''' <remarks>Llamada desde EditColumnYConfiguracion y AddColumnYConfiguracion. Máx. 0,1 seg.</remarks>
        Private Sub anyadirAPanelConfiguracion(ByVal fieldName As String, ByVal fieldDen As String)
            'Lo añadimos siempre que no esté ya presente
            If tblPnlConfig.FindControl("chk" & fieldName) Is Nothing Then
                Dim numFilas As Double = tblPnlConfig.Rows.Count
                Dim tbcCelda As New HtmlTableCell
                Dim chkCampo As New CheckBox
                chkCampo.ID = "chk" & fieldName
                chkCampo.Text = fieldDen
                chkCampo.TextAlign = TextAlign.Right
                chkCampo.Attributes.Add("onclick", "controlChecksPanelConfiguracion(this)")
                tbcCelda.Controls.Add(chkCampo)

                Dim iMaxFilas As Integer = 14
                'Para explicar el 14:
                'Al rellenar el panel de configuración con los campos de la grid hay que tener en cuenta que, 
                'aunque haya más de 4 partidas presupuestarias PRES0, sólo se va a guardar la configuración de las 4 primeras
                'por lo que el máximo de campos configurables posible será 27 pero, en el caso improbable de que haya más, tb los mostraremos

                Dim iNumCeldas As Double = 0
                Dim iFilaDeNuevaCelda As Integer = 0
                For Each ofila As HtmlTableRow In tblPnlConfig.Rows
                    For Each celda As HtmlTableCell In ofila.Cells
                        iNumCeldas += 1
                    Next
                Next
                iFilaDeNuevaCelda = (iNumCeldas Mod iMaxFilas) 'La nueva fila debería ser esto + 1 pero como tblPnlConfig.Rows(index) es de base cero, no hace falta sumar uno
                Dim iColSpan As Integer = CInt(iNumCeldas / iMaxFilas)
                If (iNumCeldas / iMaxFilas) Mod CInt(iNumCeldas / iMaxFilas) > 0 Then
                    iColSpan = CInt(iNumCeldas / iMaxFilas) + 1
                End If

                If iColSpan > 3 Then
                    'El ancho del panel es 480px y está pensado para tres columnas.
                    'Para cada columna nueva, añadimos 160px pero antes comprobamos si hay al menos 120 px por columna, para no añadir ancho innecesariamente
                    Dim iAnchoMaximo As Integer = 900
                    For i = 4 To iColSpan
                        If ((pnlConfig.Width.Value / i) < 160) AndAlso (pnlConfig.Width.Value < iAnchoMaximo) Then
                            pnlConfig.Width = Unit.Pixel(pnlConfig.Width.Value + 160)
                        End If
                    Next
                End If

                If numFilas < iMaxFilas Then
                    Dim tbrfila As New HtmlTableRow
                    tbrfila.Cells.Add(tbcCelda)
                    tblPnlConfig.Rows.Add(tbrfila)
                Else
                    Dim fila As HtmlTableRow = tblPnlConfig.Rows(iFilaDeNuevaCelda)
                    fila.Cells.Add(tbcCelda)
                End If
            End If
        End Sub

        ''' Revisado por: blp. Fecha: 22/12/2011
        ''' <summary>
        ''' Devuelve la relación entre campos del grid, del dataset de entregas y el datatable con los datos de ancho y visibilidad
        ''' La única excepción es el campo RETRASADO, que no existe en el Dataset
        ''' </summary>
        ''' <returns>Lista de Campos con la relación</returns>
        ''' <remarks>Llamada desde confAnchosYVisibilidad. Máx. 0,1 seg.</remarks>
        Private ReadOnly Property relacionarEntregasyConfiguracion() As List(Of Campos)
            Get
                Dim relacionEntregasyConfiguracion As New List(Of Campos)
                relacionEntregasyConfiguracion.Add(New Campos("RETRASADO", "RETRASADO_VISIBLE", "RETRASADO_WIDTH", "60"))
                If bCodificacionPersonalizadaRecepcionesActivada Then
                    relacionEntregasyConfiguracion.Add(New Campos("CODRECEPERP", "CODRECEPERP_VISIBLE", "CODRECEPERP_WIDTH", "50"))
                End If
                relacionEntregasyConfiguracion.Add(New Campos("ALBARAN", "ALBARAN_VISIBLE", "ALBARAN_WIDTH", "50"))
                relacionEntregasyConfiguracion.Add(New Campos("FECHARECEP", "FRECEP_VISIBLE", "FRECEP_WIDTH", "60"))
                relacionEntregasyConfiguracion.Add(New Campos("DENEMPRESA", "EMP_VISIBLE", "EMP_WIDTH", "60"))
                Dim Acceso As FSNLibrary.TiposDeDatos.ParametrosGenerales = Me.Acceso
                If Acceso.gbOblCodPedDir Or Acceso.gbOblCodPedido Then
                    relacionEntregasyConfiguracion.Add(New Campos("NUM_PED_ERP", "ERP_VISIBLE", "ERP_WIDTH", "70"))
                End If
                relacionEntregasyConfiguracion.Add(New Campos("ANYOPEDIDOORDEN", "PEDIDO_VISIBLE", "PEDIDO_WIDTH", "80"))
                relacionEntregasyConfiguracion.Add(New Campos("NUMPEDPROVE", "NUMPEDPROVE_VISIBLE", "NUMPEDPROVE_WIDTH", "60"))
                relacionEntregasyConfiguracion.Add(New Campos("FECHAEMISION", "FEMISION_VISIBLE", "FEMISION_WIDTH", "60"))
                relacionEntregasyConfiguracion.Add(New Campos("FECENTREGASOLICITADA", "FENTREGA_VISIBLE", "FENTREGA_WIDTH", "60"))
                relacionEntregasyConfiguracion.Add(New Campos("FECENTREGAINDICADAPROVE", "FENTREGAPROVE_VISIBLE", "FENTREGAPROVE_WIDTH", "60"))
                relacionEntregasyConfiguracion.Add(New Campos("ARTDEN", "ART_VISIBLE", "ART_WIDTH", "80"))
                relacionEntregasyConfiguracion.Add(New Campos("CANTLINEAPEDIDATOTAL", "CANTPED_VISIBLE", "CANTPED_WIDTH", "50"))
                relacionEntregasyConfiguracion.Add(New Campos("CANTLINEARECIBIDAPARCIAL", "CANTREC_VISIBLE", "CANTREC_WIDTH", "50"))
                relacionEntregasyConfiguracion.Add(New Campos("CANTLINEAPENDIENTETOTAL", "CANTPEND_VISIBLE", "CANTPEND_WIDTH", "50"))
                relacionEntregasyConfiguracion.Add(New Campos("UNIDEN", "UNI_VISIBLE", "UNI_WIDTH", "40"))
                relacionEntregasyConfiguracion.Add(New Campos("PREC_UP", "PREC_VISIBLE", "PREC_WIDTH", "50"))
                relacionEntregasyConfiguracion.Add(New Campos("IMPORTELINEATOTAL", "IMPPED_VISIBLE", "IMPPED_WIDTH", "50"))
                relacionEntregasyConfiguracion.Add(New Campos("IMPORTELINEARECIBIDOPARCIAL", "IMPREC_VISIBLE", "IMPREC_WIDTH", "50"))
                relacionEntregasyConfiguracion.Add(New Campos("IMPORTELINEAPENDIENTETOTAL", "IMPPEND_VISIBLE", "IMPPEND_WIDTH", "50"))
                relacionEntregasyConfiguracion.Add(New Campos("MONDEN", "MON_VISIBLE", "MON_WIDTH", "40"))
                relacionEntregasyConfiguracion.Add(New Campos("NUMLINEA", "NUM_LINEA_VISIBLE", "NUM_LINEA_WIDTH", "40"))
                relacionEntregasyConfiguracion.Add(New Campos("RECEPTOR", "RECEPTOR_VISIBLE", "RECEPTOR_WIDTH", "40"))
                relacionEntregasyConfiguracion.Add(New Campos("IMPORTETOTALALBARAN", "IMPORTE_ALBARAN_VISIBLE", "IMPORTE_ALBARAN_WIDTH", "50"))
                If Acceso.g_bAccesoFSFA Then
                    relacionEntregasyConfiguracion.Add(New Campos("IM_BLOQUEO", "BLOQ_FACTURA_VISIBLE", "BLOQ_FACTURA_WIDTH", "40"))
                    relacionEntregasyConfiguracion.Add(New Campos("FACTURA", "FACTURA_VISIBLE", "FACTURA_WIDTH", "40")) 'PARA QUE NO SE VEA LA COLUMNA
                End If
                'CENTROS DE COSTE Y PARTIDAS
                If Acceso.gbAccesoFSSM Then
                    'Sólo configuramos las 4 primeras porque no se han definido más campos para centros y partidas en la tabla de configuación de anchos y visibilidad
                    Dim queryColumnNames = From columnas As DataColumn In DatasetEntregas.Tables(0).Columns _
                                           Where columnas.ColumnName.StartsWith("NIVEL_DEN_") _
                                           Select columnas.ColumnName.ToString.Replace("NIVEL_DEN_", "") Distinct
                    If queryColumnNames.Any Then
                        Dim maxCentros As Integer = 4
                        Dim iIndice As Integer = 1
                        For Each columnName As String In queryColumnNames
                            relacionEntregasyConfiguracion.Add(New Campos("CCDEN_" & columnName, "CC" & iIndice & "_VISIBLE", "CC" & iIndice & "_WIDTH", "80"))
                            relacionEntregasyConfiguracion.Add(New Campos("NIVEL_DEN_" & columnName, "PARTIDA" & iIndice & "_VISIBLE", "PARTIDA" & iIndice & "_WIDTH", "100"))
                            iIndice += 1
                            If iIndice > 4 Then Exit For
                        Next
                    End If
                End If
                Return relacionEntregasyConfiguracion
            End Get
        End Property

        ''' <summary>
        ''' Estructura compuesta por 5 elementos q nos va a permitir almacenar el nombre del campo en el datatable de entregas (DatasetEntregas.Tables("ENTREGAS")), el nombre del campo VISIBLE en la tabla de configuración (FSP_CONF_VISOR_RECEPCIONES), el nombre del campo WIDTH en esa misma tabla, si es visible y su anchura.
        ''' </summary>
        Private Structure Campos
            Public CampoEntrega As String
            Public CampoConfiguracionVisible As String
            Public CampoConfiguracionWidth As String
            Public Visible As Boolean
            Public Width As Double

            ''' Revisado por: blp. Fecha: 15/12/2011
            ''' <summary>
            ''' Método para crear una nueva instancia de la estructura Campos
            ''' </summary>
            ''' <param name="sCampoEntrega">nombre del campo en el datatable de entregas (DatasetEntregas.Tables("ENTREGAS"))</param>
            ''' <param name="sCampoConfiguracionVisible">nombre del campo VISIBLE en la tabla de configuración (FSP_CONF_VISOR_RECEPCIONES)</param>
            ''' <param name="sCampoConfiguracionWidth">nombre del campo WIDTH en la tabla de configuración (FSP_CONF_VISOR_RECEPCIONES)</param>
            ''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
            Sub New(ByVal sCampoEntrega As String, ByVal sCampoConfiguracionVisible As String, ByVal sCampoConfiguracionWidth As String)
                Me.CampoEntrega = sCampoEntrega
                Me.CampoConfiguracionVisible = sCampoConfiguracionVisible
                Me.CampoConfiguracionWidth = sCampoConfiguracionWidth
            End Sub

            ''' Revisado por: blp. Fecha: 15/12/2011
            ''' <summary>
            ''' Método para crear una nueva instancia de la estructura Campos
            ''' </summary>
            ''' <param name="sCampoEntrega">nombre del campo en el datatable de entregas (DatasetEntregas.Tables("ENTREGAS"))</param>
            ''' <param name="sCampoConfiguracionVisible">nombre del campo VISIBLE en la tabla de configuración (FSP_CONF_VISOR_RECEPCIONES)</param>
            ''' <param name="sCampoConfiguracionWidth">nombre del campo WIDTH en la tabla de configuración (FSP_CONF_VISOR_RECEPCIONES)</param>
            ''' <param name="dblWidth">ancho del campo</param>
            ''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
            Sub New(ByVal sCampoEntrega As String, ByVal sCampoConfiguracionVisible As String, ByVal sCampoConfiguracionWidth As String, ByVal dblWidth As Double)
                Me.CampoEntrega = sCampoEntrega
                Me.CampoConfiguracionVisible = sCampoConfiguracionVisible
                Me.CampoConfiguracionWidth = sCampoConfiguracionWidth
                Me.Width = dblWidth
            End Sub

            ''' Revisado por: blp. Fecha: 15/12/2011
            ''' <summary>
            ''' Método para crear una nueva instancia de la estructura Campos
            ''' </summary>
            ''' <param name="sCampoEntrega">nombre del campo en el datatable de entregas (DatasetEntregas.Tables("ENTREGAS"))</param>
            ''' <param name="sCampoConfiguracionVisible">nombre del campo VISIBLE en la tabla de configuración (FSP_CONF_VISOR_RECEPCIONES)</param>
            ''' <param name="sCampoConfiguracionWidth">nombre del campo WIDTH en la tabla de configuración (FSP_CONF_VISOR_RECEPCIONES)</param>
            ''' <param name="bVisible">Es visible</param>
            ''' <param name="dblWidth">ancho del campo</param>
            ''' <remarks>Llamada desde New. Máx. 0,1 seg.</remarks>
            Sub New(ByVal sCampoEntrega As String, ByVal sCampoConfiguracionVisible As String, ByVal sCampoConfiguracionWidth As String, ByVal bVisible As Boolean, ByVal dblWidth As Double)
                Me.CampoEntrega = sCampoEntrega
                Me.CampoConfiguracionVisible = sCampoConfiguracionVisible
                Me.CampoConfiguracionWidth = sCampoConfiguracionWidth
                Me.Visible = bVisible
                Me.Width = dblWidth
            End Sub
        End Structure

        ''' Revisado por: blp. Fecha: 12/12/2011
        ''' <summary>
        ''' Método que se lanza en el evento InitializeRow (cuando se enlazan los datos del origen de datos con la línea)
        ''' En él vamos a configurar la columna de entrega con retrasos (mostrar la imagen de retraso en las entregas retrasadas)
        ''' </summary>
        ''' <param name="sender">Control que lanza el evento (whdgEntregas)</param>
        ''' <param name="e">Argumentos del evento</param>
        ''' <remarks>Llamada desde el evento. Máx 0,1 seg</remarks>
        Private Sub whdgEntregas_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgEntregas.InitializeRow
            Dim oData As DataRow = e.Row.DataItem.Item.Row
            Dim fechaIndicadaProve As Date = IIf(oData("FECENTREGAINDICADAPROVE") Is DBNull.Value, Date.MinValue, oData("FECENTREGAINDICADAPROVE"))
            Dim fechaSolicitada As Date = IIf(oData("FECENTREGASOLICITADA") Is DBNull.Value, Date.MinValue, oData("FECENTREGASOLICITADA"))
            Dim fechaReferencia As Date = IIf(fechaIndicadaProve = Date.MinValue, fechaSolicitada, fechaIndicadaProve)
            If fechaReferencia <> Date.MinValue AndAlso fechaReferencia < Date.Now Then
                If (oData("EST") <> 6 AndAlso oData("EST") <> 20) Then 'Sólo para pedidos no cerrados (ni anulados ni recibidos en su totalidad)
                    If DBNullToInt(oData("CANTLINEAPENDIENTETOTAL")) > 0 Then
                        If sender.Columns.FromKey("Key_RETRASADO") IsNot Nothing Then
                            Dim Index As Integer = sender.Columns.FromKey("Key_RETRASADO").Index()
                            Dim UrlImage As String = "../_common/images/Ico_Pendiente.gif"
                            e.Row.Items.Item(Index).Text = "<img src='" & UrlImage & "' style='text-align:center;' title='" & Textos(68) & "' />"
                            e.Row.Items.Item(Index).Value = "Retrasado"
                        End If
                    End If
                End If
            End If
            If Me.Acceso.g_bAccesoFSFA Then
                If DBNullToInt(oData("IM_BLOQUEO")) > 0 Then
                    If sender.Columns.FromKey("Key_IM_BLOQUEO") IsNot Nothing Then
                        Dim Index As Integer = sender.Columns.FromKey("Key_IM_BLOQUEO").Index()
                        Dim UrlImage As String = "../_common/images/stopx40.png"
                        Dim sObservaciones As String = DBNullToStr(oData("IM_BLOQUEO_OBS"))
                        Dim sHTMLImgBloqueo As String = "<img id='img_IM_BLOQUEO' class='img_IM_BLOQUEO' name='img_IM_BLOQUEO' src='" & UrlImage & "' style='text-align:center;"
                        If Not String.IsNullOrEmpty(sObservaciones) Then
                            sHTMLImgBloqueo += "cursor:pointer;"
                        End If
                        sHTMLImgBloqueo += "' observaciones='" & sObservaciones & "' />"
                        e.Row.Items.Item(Index).Text = sHTMLImgBloqueo
                        e.Row.Items.Item(Index).Value = "Bloqueado"
                        If Not String.IsNullOrEmpty(sObservaciones) Then
                            If Not Me.ClientScript.IsClientScriptIncludeRegistered("JQUERY") Then _
                                Me.ClientScript.RegisterClientScriptInclude("JQUERY", "../../js/jquery/jquery.min.js")
                            If Not Me.ClientScript.IsClientScriptIncludeRegistered("JQUERYMIGRATE") Then _
                                Me.ClientScript.RegisterClientScriptInclude("JQUERYMIGRATE", "../../js/jquery/jquery-migrate.min.js")
                            If Not Me.ClientScript.IsClientScriptIncludeRegistered("JQUERYUI") Then _
                                Me.ClientScript.RegisterClientScriptInclude("JQUERYUI", "../../js/jquery/jquery.ui.min.js")
                            Dim sScript As String
                            sScript = "$(document).ready(function(){" & vbCrLf
                            sScript += "    $('#pnl_BLOQUEO_OBSERVACIONES').remove();" & vbCrLf
                            sScript += "    $('body').prepend('<div id=""pnl_BLOQUEO_OBSERVACIONES"" class=""pnl_BLOQUEO_OBSERVACIONES"" style=""display:none;""></div>');" & vbCrLf
                            sScript += "    $('.img_IM_BLOQUEO').live('click',function(event){" & vbCrLf
                            sScript += "        Ocultar_pnl_BLOQUEO_OBSERVACIONES();" & vbCrLf
                            sScript += "        $('#pnl_BLOQUEO_OBSERVACIONES').css('position', 'absolute');" & vbCrLf
                            sScript += "        $('#pnl_BLOQUEO_OBSERVACIONES').css('top', $(this).position().top + 25);" & vbCrLf
                            sScript += "        $('#pnl_BLOQUEO_OBSERVACIONES').css('left', $(this).position().left);" & vbCrLf
                            sScript += "        $('#pnl_BLOQUEO_OBSERVACIONES').text($(this).attr('observaciones'));" & vbCrLf
                            sScript += "        if($(this).attr('observaciones')!=''){" & vbCrLf
                            sScript += "            $('#pnl_BLOQUEO_OBSERVACIONES').show('fast');" & vbCrLf
                            sScript += "        }" & vbCrLf
                            sScript += "        $('#pnl_BLOQUEO_OBSERVACIONES').focus();" & vbCrLf
                            sScript += "    });" & vbCrLf
                            sScript += "    $('#pnl_BLOQUEO_OBSERVACIONES').live('click', function (event) {" & vbCrLf
                            sScript += "        Ocultar_pnl_BLOQUEO_OBSERVACIONES();" & vbCrLf
                            sScript += "    });" & vbCrLf
                            sScript += "});" & vbCrLf
                            sScript += "function Ocultar_pnl_BLOQUEO_OBSERVACIONES() {" & vbCrLf
                            sScript += "    $('#pnl_BLOQUEO_OBSERVACIONES').hide();" & vbCrLf
                            sScript += "}" & vbCrLf
                            If Not Me.ClientScript.IsStartupScriptRegistered("JQUERYMODAL") Then _
                                Me.ClientScript.RegisterStartupScript(Me.GetType, "JQUERYMODAL", sScript, True)
                        End If
                    End If
                End If

            End If
           
        End Sub

        ''' Revisado por: blp. Fecha: 15/12/2011
        ''' <summary>
        ''' Devuelve un listado con los nombre de las partidas presupuestarias definidas
        ''' </summary>
        ''' <param name="origenPartida">
        ''' Origen de datos desde el que vamos a recuperar las partidas. 
        '''    ConfiguracionSM: Devolvemos las partidas presentes en la configuración SM (columnas de DatasetEntregas.Tables("PARTIDAS_DEN"))
        '''    ListadoEntregas: Devolvemos las partidas presentes en el listado de Entregas (lo recuperamos de DatasetEntregas.Tables("ENTREGAS"))
        ''' </param>
        Private ReadOnly Property PRES0Cods(ByVal origenPartida As TipoOrigenPartidaPres) As IEnumerable(Of String)
            Get
                If origenPartida = TipoOrigenPartidaPres.ConfiguracionSM Then
                    'Cuando la tabla PARTIDAS_DEN tiene datos, tiene una nica fila.
                    'En esta consulta cogemos los nombres de las columnas, más adelante haremos uso de los datos que hay en la fila.
                    'Si no hay datos en la fila, no nos interesa recuperar las columnas, por lo que añadimos una condición para que no devuelva datos
                    If DatasetEntregas.Tables("PARTIDAS_DEN").Rows.Count > 0 Then
                        Dim query = From columnas As DataColumn In DatasetEntregas.Tables("PARTIDAS_DEN").Columns _
                               Select columnas.ColumnName Distinct
                        Return query
                    Else
                        'Consulta que devuelve un objeto tipo iEnumerable(of String) vacío
                        Dim query = From columnas As DataColumn In DatasetEntregas.Tables("PARTIDAS_DEN").Columns _
                               Select columnas.ColumnName Distinct Where 1 = 2
                        Return query
                    End If
                Else
                    Dim query = From columnas As DataColumn In DatasetEntregas.Tables("ENTREGAS").Columns _
                                           Where columnas.ColumnName.StartsWith("NIVEL_DEN_") _
                                           Select columnas.ColumnName.ToString.Replace("NIVEL_DEN_", "") Distinct
                    Return query
                End If
            End Get
        End Property

#Region "Paginador"
        ''' Revisado por: blp. Fecha: 15/12/2011
        ''' <summary>
        ''' Inicializamos el paginador
        ''' </summary>
        ''' <remarks>Llamada desde load y botones de búsqueda. Máx. 0,1 seg.</remarks>
        Private Sub InicializarPaginador()
            Dim numRegistros As Long = 0
            If whdgEntregas.GridView.Behaviors.Filtering.Filtered Then
                whdgEntregas.GridView.Behaviors.Paging.Enabled = False
                numRegistros = whdgEntregas.GridView.Rows.Count
                whdgEntregas.GridView.Behaviors.Paging.Enabled = True
            Else
                If DatasetEntregas IsNot Nothing AndAlso DatasetEntregas.Tables("Entregas") IsNot Nothing AndAlso DatasetEntregas.Tables("Entregas").Rows.Count > 0 Then _
                    numRegistros = DatasetEntregas.Tables("Entregas").Rows.Count
            End If
            Me._PaginadorBottom.SetContext(numRegistros, ConfigurationManager.AppSettings("PaginacionEntregas"))
            Me._PaginadorTop.SetContext(numRegistros, ConfigurationManager.AppSettings("PaginacionEntregas"))
        End Sub

        ''' Revisado por: blp. Fecha: 30/01/2012
        ''' <summary>
        ''' Método que actualiza el contador de páginas del paginador
        ''' </summary>
        ''' <param name="sender">Control que lanza el evento PageChanged</param>
        ''' <param name="e">argumentos del evento</param>
        ''' <remarks>Llamada desde evento PageChanged. Máximo: 0,1 seg.</remarks>
        Private Sub _Paginador_PageChanged(ByVal sender As Object, ByVal e As PageSettingsChangedEventArgs)
            If (e.TotalNumberOfPages - 1) < e.PageIndex Then
                Me.whdgEntregas.GridView.Behaviors.Paging.PageIndex = 0
            Else
                Me.whdgEntregas.GridView.Behaviors.Paging.PageIndex = e.PageIndex
            End If

            'Actualizamos tb el otro paginador
            Dim oPagSender As Paginador = TryCast(sender, Paginador)
            If oPagSender IsNot Nothing Then
                If oPagSender.ClientID = Me._PaginadorTop.ClientID Then
                    Me._PaginadorBottom.PageNumber = e.PageNumber
                    Me._PaginadorBottom.ActualizarPaginador()
                Else
                    Me._PaginadorTop.PageNumber = e.PageNumber
                    Me._PaginadorTop.ActualizarPaginador()
                End If
            End If
            upEntregas.Update()
        End Sub
#End Region

        ''' Revisado por: blp. Fecha: 15/12/2011
        ''' <summary>
        ''' En el evento click del control btnGuardarConfiguracion
        ''' Guardamos la configuración de los campos a mostrar en el webdatagrid: tanto visibilidad (con la info del panel), como ancho de las columnas
        ''' </summary>
        ''' <param name="sender">Control que lanza el evento</param>
        ''' <param name="e">Argumentos del evento</param>
        ''' <remarks>Llamada desde el evento. Máx. 0,5 seg.</remarks>
        Private Sub btnGuardarConfiguracion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardarConfiguracion.Click
            Dim oEntregas As PMPortalServer.Entregas = FSPMServer.Get_Entregas
            Dim dtConfiguracionEntregas As DataTable = oEntregas.DataConfiguration
            'Obtenemos la relación entre los campos de la tabla de configuracion de anchos y visibilidad con los campos de la tabla de entregas
            Dim camposEntregasYConfiguracionIni As List(Of Campos) = relacionarEntregasyConfiguracion()
            Dim camposEntregasYConfiguracionFinal As New List(Of Campos)
            'Dim camposDatosConfiguracion As New List(Of ConfiguracionCampo)
            For Each oCampos As Campos In camposEntregasYConfiguracionIni
                oCampos.Visible = False
                oCampos.Width = 0
                'Visibles
                If tblPnlConfig.FindControl("chk" & oCampos.CampoEntrega) IsNot Nothing Then
                    Dim chk As CheckBox = TryCast(tblPnlConfig.FindControl("chk" & oCampos.CampoEntrega), CheckBox)
                    If chk IsNot Nothing Then
                        If chk.Checked Then
                            oCampos.Visible = True
                        End If
                    End If
                End If
                'Anchos
                If whdgEntregas.GridView.Columns.Item("Key_" & oCampos.CampoEntrega) IsNot Nothing Then
                    oCampos.Width = whdgEntregas.GridView.Columns.Item("Key_" & oCampos.CampoEntrega).Width.Value
                End If
                camposEntregasYConfiguracionFinal.Add(oCampos)
            Next
            'Guardar
            With camposEntregasYConfiguracionFinal
                oEntregas.Mod_Conf_Visor_Entregas(FSPMUser.Cod, IdCia, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "RETRASADO_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "RETRASADO_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "ALBARAN_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "ALBARAN_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "FRECEP_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "FRECEP_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "EMP_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "EMP_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "ERP_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "ERP_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "PEDIDO_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "PEDIDO_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "NUMPEDPROVE_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "NUMPEDPROVE_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "FEMISION_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "FEMISION_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "FENTREGA_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "FENTREGA_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "FENTREGAPROVE_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "FENTREGAPROVE_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "ART_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "ART_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "CANTPED_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "CANTPED_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "CANTREC_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "CANTREC_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "CANTPEND_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "CANTPEND_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "UNI_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "UNI_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "PREC_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "PREC_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "IMPPED_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "IMPPED_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "IMPREC_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "IMPREC_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "IMPPEND_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "IMPPEND_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "MON_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "MON_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "CC1_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "CC1_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "CC2_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "CC2_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "CC3_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "CC3_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "CC4_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "CC4_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "PARTIDA1_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "PARTIDA1_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "PARTIDA2_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "PARTIDA2_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "PARTIDA3_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "PARTIDA3_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "PARTIDA4_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "PARTIDA4_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "BLOQ_FACTURA_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "BLOQ_FACTURA_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "NUM_LINEA_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "NUM_LINEA_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "FACTURA_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "FACTURA_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "RECEPTOR_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "RECEPTOR_WIDTH").Width, _
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "IMPORTE_ALBARAN_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "IMPORTE_ALBARAN_WIDTH").Width,
                , , ,
                .Find(Function(x As Campos) x.CampoConfiguracionVisible = "CODRECEPERP_VISIBLE").Visible,
                .Find(Function(x As Campos) x.CampoConfiguracionWidth = "CODRECEPERP_WIDTH").Width)

            End With
            'Configurar anchos y visibilidad de nuevo
            confAnchosYVisibilidad()
            upEntregas.Update()
        End Sub

        ''' Revisado por: blp. Fecha: 19/12/2011
        ''' <summary>
        ''' Cargamos los valores de configuración del grid de datos en el panel de Configuración
        ''' </summary>
        ''' <remarks>Llamada desde CreatewhdgEntregasColumnsYConfiguracion. Máx. 0,2 seg.</remarks>
        Private Sub CargarInfoPanelConfiguracion()
            Dim oEntregas As PMPortalServer.Entregas = FSPMServer.Get_Entregas
            oEntregas.Obt_Conf_Visor_Entregas(FSPMUser.Cod, IdCia)
            Dim dtConfiguracionEntregas As DataTable = oEntregas.DataConfiguration
            'Obtenemos la relación entre los campos de la tabla de configuracion de anchos y visibilidad con los campos de la tabla de entregas
            Dim camposEntregasYConfiguracion As List(Of Campos) = relacionarEntregasyConfiguracion()
            For Each oCampos As Campos In camposEntregasYConfiguracion
                If tblPnlConfig.FindControl("chk" & oCampos.CampoEntrega) IsNot Nothing Then
                    Dim chk As CheckBox = TryCast(tblPnlConfig.FindControl("chk" & oCampos.CampoEntrega), CheckBox)
                    If chk IsNot Nothing Then
                        If dtConfiguracionEntregas.Rows.Count > 0 AndAlso dtConfiguracionEntregas.Rows(0).Item(oCampos.CampoConfiguracionVisible) IsNot Nothing Then
                            chk.Checked = CBool(dtConfiguracionEntregas.Rows(0).Item(oCampos.CampoConfiguracionVisible))
                        Else
                            chk.Checked = True
                        End If
                    End If
                End If
            Next
        End Sub

        ''' Revisado por: blp. Fecha: 10/01/2012
        ''' <summary>
        ''' Autocompletar el artículo
        ''' </summary>
        ''' <param name="prefixText">Texto escrito</param>
        ''' <param name="count">Número de resultados que se recuperan</param>
        ''' <param name="contextKey">texto (probablemente el nombre de usuario) para identificar el dataset de entregas en caché a usar</param>
        ''' <remarks>Llamada desde:EntregasPedidos.aspx -> uwgEntregas_AfterColumnSizeChangeHandler. Máx: 0,1 seg</remarks>
        <System.Web.Services.WebMethodAttribute(), _
        System.Web.Script.Services.ScriptMethodAttribute()> _
        Public Shared Function AutoCompletarArticulo(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
            If HttpContext.Current.Cache("dsEntregas_" & contextKey) Is Nothing Then
                Return Nothing
            End If
            Dim dsEntregas As DataSet
            dsEntregas = CType(HttpContext.Current.Cache("dsEntregas_" & contextKey), DataSet)

            Dim arrayPalabras As String() = Split(prefixText)
            Dim sbusqueda = From Datos In dsEntregas.Tables("ENTREGAS") _
                          Let w = UCase(DBNullToSomething(Datos.Item("ARTCOD")) & " " & DBNullToSomething(Datos.Item("ARTDEN"))) _
                          Where arrayPalabras.All(Function(palabra) w Like "*" & strToSQLLIKE(palabra, True) & "*") _
                          Select Datos.Item("ARTDEN") Distinct.Take(count).ToArray()
            Dim resul As String()
            ReDim resul(UBound(sbusqueda))
            For i As Integer = 0 To UBound(sbusqueda)
                resul(i) = sbusqueda(i).ToString()
            Next
            Return resul
        End Function

#Region "Filtros"

        ''' Revisado por: blp. Fecha: 20/01/2012
        ''' <summary>
        ''' Método que recorre todos los filtros existentes y prepara el texto que aparecerá 
        ''' en pantalla con los filtros seleccionados cuando el panel de búsqueda está oculto.
        ''' No se comprueba que los filtros están en las opciones de filtro (ni que hay permisos para ver fras y pagos) porque 
        ''' en la llamada desde gestionCookieFiltrosBusqueda ya se ha comprobado en SeleccionarFiltrosEnPanelBusqueda (método que se llama anteriormente)
        ''' y en la llamada desde btnBuscar_Click sería muy extraño que un dato seleccionado en el panel desapareciese entre el momento que se selecciona y que se pulsa al botÃ³n de buscar
        ''' </summary>
        ''' <remarks>Llamada desde gestionCookieFiltrosBusqueda y btnBuscar_Click. Máx 0,1 seg.</remarks>
        Private Sub RellenarLabelFiltros()
            ActualizarFiltros()
            Dim sListaFiltros As String = String.Empty
            Dim sListaFiltrosPartidasPres As String = String.Empty
            Dim lista As List(Of ListItem)
            For Each oFiltro As Filtro In FiltrosAnyadidos
                Dim sValor As String = oFiltro.Valor
                Select Case oFiltro.tipo
                    Case TipoFiltro.Articulo
                        sListaFiltros += "<b>" & Textos(8) & ":</b> " & sValor & "; "
                    Case TipoFiltro.FechaPedidoDesde
                        If oFiltro.ValorFecha <> DateTime.MinValue Then
                            sListaFiltros += "<b>" & Textos(12) & " " & Textos(72) & "</b> " ' Desde Fecha:
                            sListaFiltros += FormatDate(oFiltro.ValorFecha, FSPMUser.DateFormat) & "; "
                        End If
                    Case TipoFiltro.FechaPedidoHasta
                        If oFiltro.ValorFecha <> DateTime.MinValue Then
                            'Este sistema para no poner dos veces el texto "Fecha de Pedido" sólo funciona si no se cambia el orden en que son añadidas las fechas al filtro en ActualizarFiltros()
                            If sListaFiltros.Contains(Textos(12)) Then
                                sListaFiltros += "<b>" & Textos(11) & "</b> " ' HAsta Fecha:
                            Else
                                sListaFiltros += "<b>" & Textos(12) & " " & Textos(11) & "</b> " ' HAsta Fecha:
                            End If
                            sListaFiltros += FormatDate(oFiltro.ValorFecha, FSPMUser.DateFormat) & "; "
                        End If
                    Case TipoFiltro.NumPedProveedor
                        sListaFiltros += "<b>" & Textos(7) & ":</b> " & sValor & "; "
                    Case TipoFiltro.FechaEntregaSolicitDesde
                        If oFiltro.ValorFecha <> DateTime.MinValue Then
                            sListaFiltros += "<b>" & Textos(10) & " " & Textos(72) & "</b> " ' Desde Fecha:
                            sListaFiltros += FormatDate(oFiltro.ValorFecha, FSPMUser.DateFormat) & "; "
                        End If
                    Case TipoFiltro.FechaEntregaSolicitHasta
                        If oFiltro.ValorFecha <> DateTime.MinValue Then
                            'Este sistema para no poner dos veces el texto "Fecha de Entrega Solicitada" sólo funciona si no se cambia el orden en que son añadidas las fechas al filtro en ActualizarFiltros()
                            If sListaFiltros.Contains(Textos(10)) Then
                                sListaFiltros += "<b>" & Textos(11) & "</b> " ' HAsta Fecha:
                            Else
                                sListaFiltros += "<b>" & Textos(10) & " " & Textos(11) & "</b> " ' HAsta Fecha:
                            End If
                            sListaFiltros += FormatDate(oFiltro.ValorFecha, FSPMUser.DateFormat) & "; "
                        End If
                    Case TipoFiltro.NumFactura
                        sListaFiltros += "<b>" & Textos(81) & ":</b> " & sValor & "; " 'Nº factura
                    Case TipoFiltro.Empresa
                        lista = CargarEmpresas()
                        Dim sEmpresa = From oEmpresa As ListItem In lista _
                                                 Where oEmpresa.Value = sValor _
                                                 Select oEmpresa.Text Take (1)
                        sListaFiltros += "<b>" & Textos(9) & ":</b> " & sEmpresa(0) & "; "
                    Case TipoFiltro.FechaEntregaIndicDesde
                        If oFiltro.ValorFecha <> DateTime.MinValue Then
                            sListaFiltros += "<b>" & Textos(53) & " " & Textos(72) & "</b> " ' Desde Fecha:
                            sListaFiltros += FormatDate(oFiltro.ValorFecha, FSPMUser.DateFormat) & "; "
                        End If
                    Case TipoFiltro.FechaEntregaIndicHasta
                        If oFiltro.ValorFecha <> DateTime.MinValue Then
                            'Este sistema para no poner dos veces el texto "Fecha de Entrega Indicada" sólo funciona si no se cambia el orden en que son añadidas las fechas al filtro en ActualizarFiltros()
                            If sListaFiltros.Contains(Textos(53)) Then
                                sListaFiltros += "<b>" & Textos(11) & "</b> " ' HAsta Fecha:
                            Else
                                sListaFiltros += "<b>" & Textos(53) & " " & Textos(11) & "</b> " ' HAsta Fecha:
                            End If
                            sListaFiltros += FormatDate(oFiltro.ValorFecha, FSPMUser.DateFormat) & "; "
                        End If
                    Case TipoFiltro.CentroCoste
                        lista = CargarCentrosDeCoste()
                        Dim sCentroDeCoste = From oCentroCoste As ListItem In lista _
                                                 Where oCentroCoste.Value = sValor _
                                                 Select oCentroCoste.Text Take (1)
                        sListaFiltros += "<b>" & Textos(31) & ":</b> " & sCentroDeCoste(0) & "; "
                    Case TipoFiltro.PartidaPresupuestaria
                        Dim ddlPartidaID As String = "ddlPartidaPres_" & oFiltro.Info
                        Dim dllPartidaPresID As String = "ddlPartidaPres_" & oFiltro.Info
                        If phrPartidasPresBuscador IsNot Nothing _
                        AndAlso phrPartidasPresBuscador.FindControl("tbDdlPP") IsNot Nothing _
                        AndAlso phrPartidasPresBuscador.FindControl("tbDdlPP").FindControl(dllPartidaPresID) IsNot Nothing Then
                            Dim oDdlPartidaPres As DropDownList = TryCast(phrPartidasPresBuscador.FindControl("tbDdlPP").FindControl(dllPartidaPresID), DropDownList)
                            'En teoría este if es innecesario porque todas las comprobaciones se hacen en la función
                            'SeleccionarFiltrosEnPanelBusqueda(). Por si acaso, lo dejamos.
                            If oDdlPartidaPres.Items.FindByValue(sValor) IsNot Nothing Then
                                Dim sTextoPartida As String = oDdlPartidaPres.Items.FindByValue(sValor).Text
                                If sListaFiltrosPartidasPres = String.Empty Then
                                    sListaFiltrosPartidasPres += "<b>" & Textos(73) & ":</b> " & sTextoPartida
                                Else
                                    sListaFiltrosPartidasPres += ", " & sTextoPartida
                                End If
                            End If
                        End If
                End Select
            Next
            If sListaFiltrosPartidasPres.Length > 0 Then sListaFiltrosPartidasPres += "; "
            sListaFiltros += sListaFiltrosPartidasPres
            If sListaFiltros.Length > 0 Then
                lblBusqueda.Text = sListaFiltros
                divBusqueda.Style("display") = "block"
            Else
                lblBusqueda.Text = String.Empty
                divBusqueda.Style("display") = "none"
            End If
            cpeBusquedaAvanzada.Collapsed = True
            cpeBusquedaAvanzada.ClientState = "true"
        End Sub

        ''' <summary>
        ''' Tipos de filtro en el buscador avanzado
        ''' </summary>
        <Serializable()> _
        Public Enum TipoFiltro As Integer
            Articulo = 0
            NumPedProveedor = 1
            Empresa = 3
            CentroCoste = 4
            PartidaPresupuestaria = 5
            FechaPedidoDesde = 6
            FechaPedidoHasta = 7
            FechaEntregaSolicitDesde = 8
            FechaEntregaSolicitHasta = 9
            FechaEntregaIndicDesde = 10
            FechaEntregaIndicHasta = 11
            NumFactura = 12 ' mpf Filtras por numero de factura
        End Enum

        <Serializable()> _
        Public Structure Filtro
            Dim itipo As TipoFiltro
            Dim svalor As String
            Dim bvalores() As Boolean
            Dim dtvalorfecha As DateTime
            Dim sfilterInfo As String

            Public Property tipo() As TipoFiltro
                Get
                    Return itipo
                End Get
                Set(ByVal value As TipoFiltro)
                    itipo = value
                End Set
            End Property

            Public Property Valor() As String
                Get
                    Return svalor
                End Get
                Set(ByVal value As String)
                    svalor = value
                End Set
            End Property

            Public Property Valores() As Boolean()
                Get
                    Return bvalores
                End Get
                Set(ByVal value As Boolean())
                    bvalores = value
                End Set
            End Property

            Public Property ValorFecha() As DateTime
                Get
                    Return dtvalorfecha
                End Get
                Set(ByVal value As DateTime)
                    dtvalorfecha = value
                End Set
            End Property

            ''' <summary>
            ''' Propiedad del filtro que nos indica cualquier dato adicional que convenga conocer.
            ''' En el caso de las partidas presupuestarias, sirve para guardar la partida de nivel 0 / Nivel al que corresponde el filtro
            ''' (dado que puede haber más de un filtro de partida presupuestaria)
            ''' </summary>
            Public Property Info() As String
                Get
                    Return sfilterInfo
                End Get
                Set(ByVal value As String)
                    sfilterInfo = value
                End Set
            End Property

            ''' Revisado por: blp. Fecha: 20/01/2012
            ''' <summary>
            ''' Método que crea una nueva instancia de la estructura
            ''' </summary>
            ''' <param name="Tipo">Tipo de Filtro</param>
            ''' <param name="Valor">Valor string del filtro</param>
            ''' <remarks>Llamada desde la creación de la instancia. Máx. 0,1 seg.</remarks>
            Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As String)
                itipo = Tipo
                svalor = Valor
            End Sub

            ''' Revisado por: blp. Fecha: 20/01/2012
            ''' <summary>
            ''' Método que crea una nueva instancia de la estructura
            ''' </summary>
            ''' <param name="Tipo">Tipo de Filtro</param>
            ''' <param name="Valor">Valor fecha del filtro</param>
            ''' <remarks>Llamada desde la creación de la instancia. Máx. 0,1 seg.</remarks>
            Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As DateTime)
                itipo = Tipo
                dtvalorfecha = Valor
            End Sub

            ''' Revisado por: blp. Fecha: 20/01/2012
            ''' <summary>
            ''' Método que crea una nueva instancia de la estructura
            ''' </summary>
            ''' <param name="Tipo">Tipo de Filtro</param>
            ''' <param name="Valor">Valor boolean del filtro</param>
            ''' <remarks>Llamada desde la creación de la instancia. Máx. 0,1 seg.</remarks>
            Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As Boolean())
                itipo = Tipo
                bvalores = Valor
            End Sub

            ''' Revisado por: blp. Fecha: 20/01/2012
            ''' <summary>
            ''' Crear un filtro nuevo
            ''' </summary>
            ''' <param name="Tipo">Tipo de filtro</param>
            ''' <param name="Valor">Valor tipo string del filtro creado</param>
            ''' <param name="Info">Info adicional que se desee conservar del filtro</param>
            ''' <remarks>Llamada desde la creación de la instancia. Máx. 0,1 seg.</remarks>
            Public Sub New(ByVal Tipo As TipoFiltro, ByVal Valor As String, ByVal Info As String)
                itipo = Tipo
                svalor = Valor
                sfilterInfo = Info
            End Sub
        End Structure

        Public Property FiltrosAnyadidos() As List(Of Filtro)
            Get
                If ViewState("FiltrosAnyadidos") Is Nothing Then
                    Return New List(Of Filtro)
                Else
                    Return CType(ViewState("FiltrosAnyadidos"), List(Of Filtro))
                End If
            End Get
            Set(ByVal value As List(Of Filtro))
                ViewState("FiltrosAnyadidos") = value
            End Set
        End Property

        ''' Revisado por: blp. Fecha: 20/01/2012
        ''' <summary>
        ''' Procedimiento que añade los filtros que se hayan seleccionado en el panel buscador a la propiedad FiltrosAnyadidos
        ''' de la página Seguimiento.aspx.vb
        ''' </summary>
        ''' <remarks>LLamada desde el propio objeto: btnBuscar_Click, dlOrdenes_itemCommand, Page_Load
        ''' Tiempo maximo 1 sec</remarks>
        Private Sub ActualizarFiltros()
            Dim lis As New List(Of Filtro)
            'Artículo
            If Not String.IsNullOrEmpty(txtArticulo.Text) Then _
                lis.Add(New Filtro(TipoFiltro.Articulo, txtArticulo.Text))
            'Fecha Pedido desde
            Dim dtFecPedidoDesde As Nullable(Of Date) = IIf(txtFecPedidoDesde.Valor Is DBNull.Value, Nothing, txtFecPedidoDesde.Valor)
            If Not dtFecPedidoDesde Is Nothing Then _
                lis.Add(New Filtro(TipoFiltro.FechaPedidoDesde, CType(dtFecPedidoDesde, DateTime)))
            'fecha Pedido hasta
            Dim dtFecPedidoHasta As Nullable(Of Date) = IIf(txtFecPedidoHasta.Valor Is DBNull.Value, Nothing, txtFecPedidoHasta.Valor)
            If Not dtFecPedidoHasta Is Nothing Then _
                lis.Add(New Filtro(TipoFiltro.FechaPedidoHasta, CType(dtFecPedidoHasta, DateTime)))
            'Nº pedido proveedor
            If Not String.IsNullOrEmpty(txtNumPedProve.Text) Then
                lis.Add(New Filtro(TipoFiltro.NumPedProveedor, CType(txtNumPedProve.Text, String)))
            End If
            'Fecha Entrega Solicitada desde
            Dim dtFecEntregaSolicitDesde As Nullable(Of Date) = IIf(txtFecEntregaSolicitDesde.Valor Is DBNull.Value, Nothing, txtFecEntregaSolicitDesde.Valor)
            If Not dtFecEntregaSolicitDesde Is Nothing Then _
                lis.Add(New Filtro(TipoFiltro.FechaEntregaSolicitDesde, CType(dtFecEntregaSolicitDesde, DateTime)))
            'fecha Entrega Solicitada hasta
            Dim dtFecEntregaSolicitHasta As Nullable(Of Date) = IIf(txtFecEntregaSolicitHasta.Valor Is DBNull.Value, Nothing, txtFecEntregaSolicitHasta.Valor)
            If Not dtFecEntregaSolicitHasta Is Nothing Then _
                lis.Add(New Filtro(TipoFiltro.FechaEntregaSolicitHasta, CType(dtFecEntregaSolicitHasta, DateTime)))
            'Nº factura 'mpf filtro num factura
            If Not String.IsNullOrEmpty(txtNumFactura.Text) Then
                lis.Add(New Filtro(TipoFiltro.NumFactura, CType(txtNumFactura.Text, String)))
            End If
            'Empresa
            If Not String.IsNullOrEmpty(ddEmpresa.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.Empresa, ddEmpresa.SelectedValue))
            'Fecha Entrega Indicada desde
            Dim dtFecEntregaIndicadaDesde As Nullable(Of Date) = IIf(txtFecEntregaIndicadaDesde.Valor Is DBNull.Value, Nothing, txtFecEntregaIndicadaDesde.Valor)
            If Not dtFecEntregaIndicadaDesde Is Nothing Then _
                lis.Add(New Filtro(TipoFiltro.FechaEntregaIndicDesde, CType(dtFecEntregaIndicadaDesde, DateTime)))
            'fecha Entrega Indicada hasta
            Dim dtFecEntregaIndicadaHasta As Nullable(Of Date) = IIf(txtFecEntregaIndicadaHasta.Valor Is DBNull.Value, Nothing, txtFecEntregaIndicadaHasta.Valor)
            If Not dtFecEntregaIndicadaHasta Is Nothing Then _
                lis.Add(New Filtro(TipoFiltro.FechaEntregaIndicHasta, CType(dtFecEntregaIndicadaHasta, DateTime)))
            'Centro Coste
            If Not String.IsNullOrEmpty(ddCentro.SelectedValue) Then _
                lis.Add(New Filtro(TipoFiltro.CentroCoste, ddCentro.SelectedValue))
            'Partidas presupuestarias
            For Each tabla As Control In phrPartidasPresBuscador.Controls
                If TypeOf tabla Is Table Then
                    For Each fila As Control In CType(tabla, Table).Rows
                        For Each celda As Control In CType(fila, TableRow).Cells
                            For Each ddl As Control In CType(celda, TableCell).Controls
                                If TypeOf ddl Is System.Web.UI.WebControls.DropDownList Then
                                    If Not String.IsNullOrEmpty(CType(ddl, System.Web.UI.WebControls.DropDownList).SelectedValue) Then _
                                    lis.Add(New Filtro( _
                                                    TipoFiltro.PartidaPresupuestaria, _
                                                    CType(ddl, System.Web.UI.WebControls.DropDownList).SelectedValue.ToString, _
                                                    (ddl.ID).Replace("ddlPartidaPres_", "") _
                                            ))
                                ElseIf TypeOf ddl Is System.Web.UI.WebControls.Label Then
                                    For Each ddl2 As Control In CType(ddl, Label).Controls
                                        If TypeOf ddl2 Is System.Web.UI.WebControls.DropDownList Then
                                            If Not String.IsNullOrEmpty(CType(ddl2, System.Web.UI.WebControls.DropDownList).SelectedValue) Then _
                                            lis.Add(New Filtro( _
                                                            TipoFiltro.PartidaPresupuestaria, _
                                                            CType(ddl2, System.Web.UI.WebControls.DropDownList).SelectedValue.ToString, _
                                                            (ddl2.ID).Replace("ddlPartidaPres_", "") _
                                                    ))
                                        End If
                                    Next
                                End If
                            Next
                        Next
                    Next
                End If
            Next

            FiltrosAnyadidos = lis
        End Sub

#End Region

#Region "Validación Intergracion"
        Private ReadOnly Property HayIntegracionPedidosEntradaOEntradaSalida() As Boolean
            Get
                If HttpContext.Current.Session("HayIntegracionEntradaoEntradaSalida") Is Nothing Then
                    Dim tablas As Integer()
                    ReDim tablas(1)
                    tablas(0) = TablasIntegracion.PED_Aprov
                    tablas(1) = TablasIntegracion.PED_directo
                    HttpContext.Current.Session("HayIntegracionEntradaoEntradaSalida") = HayIntegracionEntradaoEntradaSalida(tablas)
                End If

                Return HttpContext.Current.Session("HayIntegracionEntradaoEntradaSalida")
            End Get
        End Property

        ''' Revisado por: blp. Fecha: 03/02/2012
        ''' <summary>
        ''' Función que nos dice si hay integracion entrada salida en alguna de las tablas de un array
        ''' </summary>
        ''' <param name="tablas">Array de Id's de tabla para los que se quiere averiguar si hay integración</param>
        ''' <returns>Booleano</returns>
        ''' <remarks>Llamada desde seguimiento.aspx.vb y Recepcion.aspx.vb
        ''' Tiempo máximo 0,1 sec</remarks>
        Public Function HayIntegracionEntradaoEntradaSalida(ByVal tablas As Integer()) As Boolean
            Dim hayIntegracion As Boolean = False
            Dim oValidacionesIntegracion As PMPortalServer.ValidacionesIntegracion = FSPMServer.Get_ValidacionesIntegracion
            hayIntegracion = oValidacionesIntegracion.HayIntegracionEntradaoEntradaSalida(tablas, IdCiaComp)
            Return hayIntegracion
        End Function

        ''' <summary>
        ''' Función que nos dice si hay integracion salida o entrada/salida en alguna de las tablas de un array
        ''' </summary>
        ''' <param name="tablas">Array de Id's de tabla para los que se quiere averiguar si hay integración</param>
        ''' <returns>Booleano</returns>
        ''' <remarks>Llamada desde seguimiento.aspx.vb y Recepcion.aspx.vb
        ''' Tiempo máximo 0,1 sec</remarks>
        Public Function HayIntegracionSalidaoEntradaSalida(ByVal tablas As Integer()) As Boolean
            Dim hayIntegracion As Boolean = False
            Dim oValidacionesIntegracion As PMPortalServer.ValidacionesIntegracion = FSPMServer.Get_ValidacionesIntegracion
            hayIntegracion = oValidacionesIntegracion.HayIntegracionSalidaoEntradaSalida(tablas, IdCiaComp)
            Return hayIntegracion
        End Function

#End Region

        ''' Revisado por: blp. Fecha: 12/03/2012
        ''' <summary>
        ''' Establece las opciones de idioma/decimales/fechas/etc del usuario
        ''' </summary>
        ''' <remarks>Llamada desde Init. Máx. 0,1 seg.</remarks>
        Private Sub setCultureInfoUsuario()
            Me.ScriptMgr.EnableScriptGlobalization = True
            Me.ScriptMgr.EnableScriptLocalization = True

            setNumberFormat(Me.whdgEntregas.GridView.Columns.FromKey("Key_CANTLINEAPEDIDATOTAL"))
            setNumberFormat(Me.whdgEntregas.GridView.Columns.FromKey("Key_CANTLINEARECIBIDAPARCIAL"))
            setNumberFormat(Me.whdgEntregas.GridView.Columns.FromKey("Key_CANTLINEAPENDIENTETOTAL"))
            setNumberFormat(Me.whdgEntregas.GridView.Columns.FromKey("Key_PREC_UP"))
            setNumberFormat(Me.whdgEntregas.GridView.Columns.FromKey("Key_IMPORTELINEATOTAL"))
            setNumberFormat(Me.whdgEntregas.GridView.Columns.FromKey("Key_IMPORTELINEARECIBIDOPARCIAL"))
            setNumberFormat(Me.whdgEntregas.GridView.Columns.FromKey("Key_IMPORTELINEAPENDIENTETOTAL"))
            setNumberFormat(Me.whdgEntregas.GridView.Columns.FromKey("Key_IMPORTETOTALALBARAN"))

            setDateFormat(Me.whdgEntregas.GridView.Columns.FromKey("Key_FECHARECEP"))
            setDateFormat(Me.whdgEntregas.GridView.Columns.FromKey("Key_FECHAEMISION"))
            setDateFormat(Me.whdgEntregas.GridView.Columns.FromKey("Key_FECENTREGASOLICITADA"))
            setDateFormat(Me.whdgEntregas.GridView.Columns.FromKey("Key_FECENTREGAINDICADAPROVE"))
        End Sub

        ''' Revisado por blp. Fecha: 12/04/2012
        ''' <summary>
        ''' Dar el formato del usuario a las columnas numéricas del grid
        ''' </summary>
        ''' <param name="field">Columna seleccionada</param>
        ''' <remarks>Llamada desde setCultureInfoUsuario. 0,1 seg.</remarks>
        Private Sub setNumberFormat(ByVal field As Infragistics.Web.UI.GridControls.BoundDataField)
            field.DataFormatString = "{0:n" & FSPMUser.NumberFormat.NumberDecimalDigits & "}"
        End Sub

        ''' Revisado por blp. Fecha: 12/04/2012
        ''' <summary>
        ''' Dar el formato del usuario a las columnas de fecha del grid
        ''' </summary>
        ''' <param name="field">columna seleccionada</param>
        ''' <remarks>Llamada desde setCultureInfoUsuario. 0,1 seg.</remarks>
        Private Sub setDateFormat(ByVal field As Infragistics.Web.UI.GridControls.BoundDataField)
            field.DataFormatString = "{0:" & FSPMUser.DateFormat.ShortDatePattern.ToString & "}"
        End Sub
    End Class
End Namespace