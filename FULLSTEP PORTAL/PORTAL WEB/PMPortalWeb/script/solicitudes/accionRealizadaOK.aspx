<%@ Page Language="vb" AutoEventWireup="false" Codebehind="accionRealizadaOK.aspx.vb" Inherits="Fullstep.PMPortalWeb.accionRealizadaOK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
		<title>accionRealizadaOK</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../../common/estilo.asp" type="text/css" rel="stylesheet">
		<script src="../../../common/formatos.js"></script>
		<script src="../../../common/menu.asp"></script>
        <script type="text/javascript">
            /*''' <summary>
            ''' Iniciar la pagina.
            ''' </summary>     
            ''' <remarks>Llamada desde: page.onload; Tiempo m�ximo:0</remarks>*/
            function Init() {
                if (accesoExterno == 0) { document.getElementById('tablemenu').style.display = 'block'; }
            }
        </script>
    </head>
	<body onload="Init()">
		<script>
		function BuscarPeticionario()
		{
		    var Cod=document.forms["Form1"].elements["txtPeticionario"].value   
			window.open("<%=ConfigurationManager.AppSettings("rutanormal")%>/script/_common/detallepersona.aspx?CodPersona=" + Cod.toString(),"_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250")
		}
		</script>	
		<form id="Form1" method="post" runat="server">
            <script type="text/javascript">
                switch (Number(tipoVisor)) {
                    case 1:
                    case 4:
                        dibujaMenu(3);
                        break;
                    case 5:
                        dibujaMenu(9);
                        break;
                    case 2:
                    case 3:
                    case 6:
                    case 7:
                        dibujaMenu(2);
                        break;
                    default:
                        dibujaMenu(99);
                }
            </script>
			<TABLE id="tblGeneral" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 50px"
				cellSpacing="1" cellPadding="1" width="95%" border="0" runat="server">
				<TR>
					<TD style="HEIGHT: 30px" width="100%"><asp:label id="lblTitulo" runat="server" Width="100%" CssClass="Titulo">DEmisi�n de solicitud tipo XXX</asp:label></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 75px" valign="top">
						<TABLE id="tblCabecera" class="cabeceraSolicitud" cellSpacing="1" cellPadding="1" width="100%" border="0" style="HEIGHT: 100px">
							<TR>
								<TD width="7%">
									<asp:label id="lblLitId" runat="server" CssClass="captionBlue" Width="100%">DIdentificador de la solicitud:</asp:label></TD>
								<TD width="20%">
									<asp:label id="lblId" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:label></TD>
								<TD width="5%"></TD>
								<TD width="5%">
									<asp:Label id="lblFecha" runat="server" CssClass="captionBlue" Width="100%">lblFecha</asp:Label></TD>
								<TD width="10%">
									<asp:Label id="lblFechaBD" runat="server" CssClass="captionDarkBlue" Width="100%">lblFechaBD</asp:Label></TD>
							</TR>
							<TR>
								<TD>
									<asp:Label id="lblDen" runat="server" CssClass="captionBlue" Width="100%">lblDen</asp:Label></TD>
								<TD>
									<asp:Label id="lblDenBD" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:Label></TD>
								<TD></TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD>
									<asp:label id="lblLitPeticionario" runat="server" CssClass="captionBlue" Width="100%">DPeticionario:</asp:label></TD>
								<TD>
									<asp:label id="lblPeticionario" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:label></TD>
								<TD><a id="cmdBuscarPet" runat="server" style="CURSOR: hand" onclick="javascript:BuscarPeticionario()"
										name="cmdBuscarPet"><IMG src="../_common/images/help.gif" border="0"></a></TD>
								<TD></TD>
								<TD></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 25px" vAlign="bottom">
						<asp:Label id="lblMensaje" runat="server" CssClass="caption" Width="100%">lblMensaje</asp:Label></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 50px" vAlign="bottom">
						<asp:Label id="lblEtapas" runat="server" Width="100%" CssClass="fntLogin">lblEtapas</asp:Label></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 67px">
						<asp:ListBox id="lstEtapas" runat="server" Width="100%" Height="150px" CssClass="fntLogin"></asp:ListBox></TD>
				</TR>
			</TABLE>
			<input id="txtPeticionario"	type="hidden" name="txtPeticionario" runat="server" />
		</form>
	</body>
</html>
