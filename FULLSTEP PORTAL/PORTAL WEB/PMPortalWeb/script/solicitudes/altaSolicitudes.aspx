﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="altaSolicitudes.aspx.vb" Inherits="Fullstep.PMPortalWeb.altaSolicitudes" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../../common/estilo.asp" type="text/css" rel="stylesheet" />
    <script src="../../../common/menu.asp"></script>
    <script type="text/javascript">
        function Init() {
            if (accesoExterno == 0) { document.getElementById('tablemenu').style.display = 'block'; }
        }
    </script>
</head>
<body onload="Init()">
    <script type="text/javascript">
        function wdgSolicitudes_RowSelectionChanged(sender, e) {

            var row = e.getSelectedRows().getItem(0);
            var idSolicitud = row.get_cellByColumnKey("ID").get_value();
            window.open('altaSolicitud.aspx?Solicitud=' + idSolicitud, '_self');
        }
    </script>
    <script>dibujaMenu(2);</script>
    <form id="form1" runat="server" style="margin-top:2em;">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
        <div class="margenVert1">
            <asp:Label ID="lblParrafo" runat="server" CssClass="Rotulo10"></asp:Label>
            <asp:Label ID="lblSinDatos" runat="server" CssClass="Rotulo" ForeColor="Red" Visible="False"></asp:Label>
        </div>
        <table id="tblSolicitudes" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
            <tr>
                <td class="cabeceraTabla Texto12">
                    <asp:Label ID="lblCabeceraAltaSolicitudes" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="Texto12">
                    <ig:WebDataGrid ID="wdgSolicitudes" runat="server" AutoGenerateColumns="false" ShowHeader="false">
                        <Columns>
                            <ig:BoundDataField DataFieldName="ID" Key="ID" Hidden="true"></ig:BoundDataField>
                            <ig:BoundDataField DataFieldName="TIPO" Key="TIPO" Hidden="true"></ig:BoundDataField>
                            <ig:BoundDataField DataFieldName="COD" Key="COD" Hidden="true"></ig:BoundDataField>
                            <ig:BoundDataField DataFieldName="DEN" Key="DEN" Width="100%"></ig:BoundDataField>
                        </Columns>
                        <Behaviors>
                            <ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Row" SelectedCellCssClass="SelectedRow">
                                <SelectionClientEvents RowSelectionChanged="wdgSolicitudes_RowSelectionChanged" />
                            </ig:Selection>
                        </Behaviors>
                    </ig:WebDataGrid>
                </td>
            </tr>
        </table>
        <div class="margenVert1"><asp:Label ID="lblFooter" runat="server" CssClass="Rotulo10"></asp:Label></div>
    </form>
</body>
</html>
