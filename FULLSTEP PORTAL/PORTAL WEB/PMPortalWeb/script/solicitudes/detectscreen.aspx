<%@ Page Language="vb" AutoEventWireup="false" Codebehind="detectscreen.aspx.vb" Inherits="detectscreen"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>detectscreen</title>
	<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
	<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
	<meta name="vs_defaultClientScript" content="JavaScript">
	<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	<script type="text/javascript" id="clientEventHandlersJS" language="javascript">
	    function window_onload() {
	        if ('deviceXDPI' in screen) {
	            DpiX = screen.deviceXDPI;
	            DpiY = screen.deviceYDPI;
	        } else {
	            DpiX = 96;
	            DpiY = 96;
	        }
	        top.location.href = "detectscreen.aspx?action=set&DpiX=" + DpiX + "&DpiY=" + DpiY + "&Instancia=" + document.getElementById("Instancia").value + "&Codigo=" + document.getElementById("CodContrato").value + "&NumFactura=" + document.getElementById("NumFactura").value
        }
	</script>
	</head>
	<body MS_POSITIONING="GridLayout" language="javascript" onload="return window_onload()">
		<form id="frmDetect" method="post" runat="server">
			<input id="Instancia" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 16px" type="hidden"
				name="Instancia" runat="server"/>
            <input id="CodContrato" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 36px" type="hidden"
				name="CodContrato" runat="server"/>	
            <input id="NumFactura" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 36px" type="hidden"
				name="NumFactura" runat="server"/>				
		</form>
	</body>
</html>
