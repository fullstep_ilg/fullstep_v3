Partial Class EnProceso
    Inherits FSPMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Protected WithEvents lblProveedor As System.Web.UI.WebControls.Label
    Protected WithEvents lblProveedorBD As System.Web.UI.WebControls.Label
    Protected Titulo As String

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    ''' <summary>
    ''' Cargar la pagina. 
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>        
    ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.SeguimientoSolicitudes

        Dim sInstancia As String = Request("Instancia")
        Dim Tipo As String = Request("Tipo")

        lblId.Text = Textos(1)
        lblFecha.Text = Textos(27)
        lblTipo.Text = Textos(2)
        btnCerrar.Value = Textos(29)
        lblEnProceso.Text = Textos(28)
        Titulo = Textos(0)
        lblIdBD.Text = sInstancia
        lblTipoBD.Text = Tipo
    End Sub

End Class
