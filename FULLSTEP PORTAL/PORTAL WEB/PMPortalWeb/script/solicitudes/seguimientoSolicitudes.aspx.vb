Namespace Fullstep.PMPortalWeb
    Partial Class seguimientoSolicitudes
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object
        ''' <summary>
        ''' PreCargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()

            txtFecDesde.NullText = ""
            'Me.txtFecDesde.Portal = True
        End Sub

#End Region

        Private m_sIdiEtapasParalelo As String
        Private m_sIdiEstados(4) As String
        Private oInstancia As Fullstep.PMportalServer.Instancia
        Private sEnProceso As String

        ''' <summary>
        ''' Mostrar la pagina de seguimiento de solicitudes
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">evento de sistema</param>           
        ''' <returns>Nada</returns>
        ''' <remarks>
        ''' ; Tiempo m�ximo: 0,4</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Dim oSolicitudes As Fullstep.PMPortalServer.Solicitudes
            Dim lCiaComp As Long = IdCiaComp

            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.SeguimientoSolicitudes

            'Textos:
            lblTitulo.Text() = Textos(0)
            lblIdent.Text = Textos(1)
            lblTipo.Text = Textos(2)
            lblDesde.Text = Textos(3)
            cmdCargar.Text = Textos(18)
            m_sIdiEtapasParalelo = Textos(19)

            m_sIdiEstados(1) = Textos(20)  'Finalizada
            m_sIdiEstados(2) = Textos(21)  'Rechazada
            m_sIdiEstados(3) = Textos(22)  'Anulada
            m_sIdiEstados(4) = Textos(23)  'Cerrada

            ssEstados.Rows(0).Cells(0).Value = "<a style=""cursor:pointer"" onclick=""VerSolicitudesPorEstado(0);"">" & Textos(6) & "</a>" 'Requieren su intervenci�n

            uwgParticipacion.Rows(0).Cells(0).Value = "<a style=""cursor:pointer"" onclick=""VerSolicitudesPorEstado(1);"">" & Textos(7) & "</a>" 'En curso
            uwgParticipacion.Rows(1).Cells(0).Value = "<a style=""cursor:pointer"" onclick=""VerSolicitudesPorEstado(2);"">" & Textos(9) & "</a>" 'Rechazadas
            uwgParticipacion.Rows(2).Cells(0).Value = "<a style=""cursor:pointer"" onclick=""VerSolicitudesPorEstado(3);"">" & Textos(10) & "</a>" 'Anuladas
            uwgParticipacion.Rows(3).Cells(0).Value = "<a style=""cursor:pointer"" onclick=""VerSolicitudesPorEstado(4);"">" & Textos(8) & "</a>" 'Finalizadas

            lblProcIntervencion.Text = Textos(4)  'Procesos que requieren su intervenci�n
            lblProcParticipacion.Text = Textos(5)  'Hist�rico de procesos en los que usted particip�

            sEnProceso = Textos(26) 'Solicitud en Proceso

            'Carga el combo de los tipos de solicitud:
            oSolicitudes = FSPMServer.Get_Solicitudes
            oSolicitudes.LoadData(lCiaComp, FSPMUser.CodProveGS, Idioma)

            wddTipoSolicitud.DataSource = oSolicitudes.Data
            wddTipoSolicitud.TextField = "DEN"
            wddTipoSolicitud.ValueField = "ID"
            wddTipoSolicitud.DataBind()
            wddTipoSolicitud.Items.Insert(0, New Infragistics.Web.UI.ListControls.DropDownItem(Textos(24)))

            If Not Page.IsPostBack Then
                If wddTipoSolicitud.SelectedItemIndex = -1 Then
                    wddTipoSolicitud.SelectedItemIndex = 0 '(Cualquier tipo)
                End If

                hidTipoSol.Value = Me.wddTipoSolicitud.SelectedItemIndex
            Else
                wddTipoSolicitud.SelectedItemIndex = hidTipoSol.Value
            End If

            'Carga la grid:
            If Page.IsPostBack = False Then
                CargarGridSolicitudes()
            ElseIf Request("__EVENTARGUMENT") <> Nothing Then
                CargarGridSolicitudes(Request("__EVENTARGUMENT"))
            End If

            'Caption de la grid de instancias:
            uwgSolicitudes.Bands(0).Columns.FromKey("ID_INS").Header.Caption = Textos(13)
            uwgSolicitudes.Bands(0).Columns.FromKey("COD").Header.Caption = Textos(11)
            uwgSolicitudes.Bands(0).Columns.FromKey("FECHA").Header.Caption = Textos(14)
            uwgSolicitudes.Bands(0).Columns.FromKey("DESCR").Header.Caption = Textos(12)
            uwgSolicitudes.Bands(0).Columns.FromKey("ESTADO_DEN").Header.Caption = Textos(15)
            uwgSolicitudes.Bands(0).Columns.FromKey("NOM_USU").Header.Caption = Textos(16)
            uwgSolicitudes.Bands(0).Columns.FromKey("ORIGEN").Header.Caption = Textos(25)
            uwgSolicitudes.Bands(0).Columns.FromKey("COMENT").Header.Caption = Textos(17)
            uwgSolicitudes.Bands(0).Columns.FromKey("ITEM").Header.Caption = Textos(30)

            uwgSolicitudes.Bands(0).Columns.FromKey("TIPO").Hidden = True
            uwgSolicitudes.Bands(0).Columns.FromKey("CONTRATO").Hidden = True
            uwgSolicitudes.Bands(0).Columns.FromKey("CODIGO_CONTRATO").Hidden = True

            lblTituloMultItems.InnerText = Textos(30)

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutanormal") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutanormal", "var rutanormal ='" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "';", True)
            End If
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "lCiaComp") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "lCiaComp", "var lCiaComp =" & IdCiaComp & ";", True)
            End If
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)
            End If

            oSolicitudes = Nothing
        End Sub

        Private Sub cmdExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmdExcel.Click
            'Exporta a excel la grid:

            UltraWebGridExcelExporter1.DownloadName = Me.lblTitulo.Text
            Me.UltraWebGridExcelExporter1.Export(Me.uwgSolicitudes)
        End Sub

        Private Sub cmdCargar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCargar.Click
            'Carga la grid:
            Dim iEstado As Integer
            Dim i As Integer

            If ssEstados.Rows(0).Cells.FromKey("FLECHA").Style.BackgroundImage <> "" Then
                iEstado = 0
            Else
                For i = 0 To 3
                    If uwgParticipacion.Rows(i).Cells.FromKey("FLECHA").Style.BackgroundImage <> "" Then
                        iEstado = i + 1
                    End If
                Next i
            End If

            CargarGridSolicitudes(iEstado)
        End Sub

        ''' <summary>Establecer el aspecto general del grid uwgSolicitudes</summary>
        ''' <param name="sender">uwgSolicitudes</param>
        ''' <param name="e">evento de sistema</param>              
        ''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0 </remarks>
        Private Sub uwgSolicitudes_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgSolicitudes.InitializeRow
            If (e.Row.Cells.FromKey("EN_PROCESO").Value = 1 OrElse e.Row.Cells.FromKey("EN_PROCESO").Value = 2) Then
                e.Row.Cells.FromKey("ESTADO_DEN").Value = sEnProceso
                e.Row.Cells.FromKey("NOM_USU").Value = sEnProceso
                e.Row.Cells.FromKey("ORIGEN").Value = sEnProceso
            Else
                Select Case e.Row.Cells.FromKey("ESTADO").Value
                    Case Fullstep.PMPortalServer.TipoEstadoSolic.SCRechazada, Fullstep.PMPortalServer.TipoEstadoSolic.SCAnulada, Fullstep.PMPortalServer.TipoEstadoSolic.Enviada, Fullstep.PMPortalServer.TipoEstadoSolic.Aprobada, Fullstep.PMPortalServer.TipoEstadoSolic.SCPendiente, Fullstep.PMPortalServer.TipoEstadoSolic.SCAprobada, Fullstep.PMPortalServer.TipoEstadoSolic.CPublicable, Fullstep.PMPortalServer.TipoEstadoSolic.NoConformidadEnviada
                        e.Row.Cells.FromKey("ESTADO_DEN").Value = m_sIdiEstados(1)  'Finalizada

                    Case Fullstep.PMPortalServer.TipoEstadoSolic.Rechazada
                        e.Row.Cells.FromKey("ESTADO_DEN").Value = m_sIdiEstados(2)  'Rechazada

                    Case Fullstep.PMPortalServer.TipoEstadoSolic.Anulada
                        e.Row.Cells.FromKey("ESTADO_DEN").Value = m_sIdiEstados(3)  'Anulada

                    Case Fullstep.PMPortalServer.TipoEstadoSolic.SCCerrada
                        e.Row.Cells.FromKey("ESTADO_DEN").Value = m_sIdiEstados(4)  'Cerrada

                    Case Else  'Est� guardada o en curso.Se muestra la etapa actual
                        If e.Row.Cells.FromKey("DENETAPA_ACTUAL").Value <> Nothing Then
                            e.Row.Cells.FromKey("ESTADO_DEN").Value = e.Row.Cells.FromKey("DENETAPA_ACTUAL").Value
                        Else
                            e.Row.Cells.FromKey("ESTADO_DEN").Value = m_sIdiEtapasParalelo
                        End If
                End Select
            End If

            If e.Row.Cells.FromKey("ITEM").Value = "##" Then e.Row.Cells.FromKey("ITEM").Value = "<a onclick=""MostrarPanelMultiplesItems(" & e.Row.Cells.FromKey("ID_INS").Value & "); return false;"" class=""TablaLink"">" & Textos(31) & "</a>"

            'A VER SI HAY Q DES-LINKAR EL CAMPO
            'Carga los datos de la instancia:
            Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim lCiaComp As Long = IdCiaComp
            Dim sIdi As String = Idioma
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If
            oInstancia = FSPMServer.Get_Instancia
            oInstancia.ID = e.Row.Cells.FromKey("ID_INS").Value()
            oInstancia.DevolverEtapaActual(lCiaComp, sIdi, FSPMUser.CodProveGS)
            If oInstancia.Ver_flujo = True Then
                e.Row.Cells.FromKey("COMENT").Value = "123123"
                e.Row.Cells.FromKey("COMENT").Text = "OK"
                e.Row.Cells.FromKey("ESTADO_DEN").Value = "<a onclick=""VerComentariosSolicitud(" & e.Row.Cells.FromKey("ID_INS").Value & "); return false;"">" & e.Row.Cells.FromKey("DENETAPA_ACTUAL").Value & "</a>" '<img src=images/coment.gif onclick=""VerComentariosSolicitud(" & e.Row.Cells.FromKey("ID_INS").Value & "); return false;"">"
            End If

            'para mostrar correctamente la fecha en el pop-up en proceso.
            e.Row.Cells.FromKey("FECHA_ALTA").Value = e.Row.Cells.FromKey("FECHA").Value

            If e.Row.Cells.FromKey("DESCR").Value <> Nothing Then
                e.Row.Cells.FromKey("DESCR").Value = "<a onclick=""ComprobarEnProceso(" & e.Row.Cells.FromKey("ID_INS").Value & ",'" & e.Row.Cells.FromKey("COD").Value & "','" & e.Row.Cells.FromKey("FECHA_ALTA").Value & "'," & e.Row.Cells.FromKey("TIPO").Value & ",'" & e.Row.Cells.FromKey("CODIGO_CONTRATO").Value & "'," & IIf(e.Row.Cells.FromKey("CONTRATO").Value = Nothing, 0, e.Row.Cells.FromKey("CONTRATO").Value) & ");return false;"">" & e.Row.Cells.FromKey("DESCR").Value & "</a>"
            End If
            e.Row.Cells.FromKey("COD").Value = "<a onclick=""ComprobarEnProceso(" & e.Row.Cells.FromKey("ID_INS").Value & ",'" & e.Row.Cells.FromKey("COD").Value & "','" & e.Row.Cells.FromKey("FECHA_ALTA").Value & "'," & e.Row.Cells.FromKey("TIPO").Value & ",'" & e.Row.Cells.FromKey("CODIGO_CONTRATO").Value & "'," & IIf(e.Row.Cells.FromKey("CONTRATO").Value = Nothing, 0, e.Row.Cells.FromKey("CONTRATO").Value) & ");return false;"">" & e.Row.Cells.FromKey("COD").Value & "</a>"
        End Sub

        Private Sub uwgSolicitudes_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgSolicitudes.InitializeLayout
            'a�ade la columna de estado:
            uwgSolicitudes.Bands(0).Columns.Add("ESTADO_DEN")
            uwgSolicitudes.Bands(0).Columns.FromKey("ESTADO_DEN").Move(6)
            uwgSolicitudes.Bands(0).Columns.Add("FECHA_ALTA")

            ''Oculta las columnas:
            uwgSolicitudes.Bands(0).Columns.FromKey("ID_SOLIC").Hidden = True
            uwgSolicitudes.Bands(0).Columns.FromKey("ESTADO").Hidden = True
            uwgSolicitudes.Bands(0).Columns.FromKey("DENETAPA_ACTUAL").Hidden = True
            uwgSolicitudes.Bands(0).Columns.FromKey("VER_DETALLE_PER").Hidden = True
            uwgSolicitudes.Bands(0).Columns.FromKey("EN_PROCESO").Hidden = True
            uwgSolicitudes.Bands(0).Columns.FromKey("FECHA_ALTA").Hidden = True

            'a�ade la columna del comentario:
            uwgSolicitudes.Bands(0).Columns.Add("COMENT")

            'formatos:
            uwgSolicitudes.Bands(0).Columns.FromKey("FECHA").Format = FSPMUser.DateFormat.ShortDatePattern

            'Redimensiona las columnas:
            uwgSolicitudes.Bands(0).Columns.FromKey("COD").Width = Unit.Percentage(8)
            uwgSolicitudes.Bands(0).Columns.FromKey("DESCR").Width = Unit.Percentage(20)
            uwgSolicitudes.Bands(0).Columns.FromKey("ID_INS").Width = Unit.Percentage(6)
            uwgSolicitudes.Bands(0).Columns.FromKey("FECHA").Width = Unit.Percentage(8)
            uwgSolicitudes.Bands(0).Columns.FromKey("ITEM").Width = Unit.Percentage(11)
            uwgSolicitudes.Bands(0).Columns.FromKey("ESTADO_DEN").Width = Unit.Percentage(16)
            uwgSolicitudes.Bands(0).Columns.FromKey("NOM_USU").Width = Unit.Percentage(15)
            uwgSolicitudes.Bands(0).Columns.FromKey("ORIGEN").Width = Unit.Percentage(8)
            uwgSolicitudes.Bands(0).Columns.FromKey("COMENT").Width = Unit.Percentage(8)
            uwgSolicitudes.Bands(0).Columns.FromKey("COMENT").Hidden = True


            uwgSolicitudes.DisplayLayout.Bands(0).Columns.FromKey("COD").CellStyle.CssClass = "TablaLink"
            uwgSolicitudes.DisplayLayout.Bands(0).Columns.FromKey("DESCR").CellStyle.CssClass = "TablaLink"
            uwgSolicitudes.DisplayLayout.Bands(0).Columns.FromKey("ESTADO_DEN").CellStyle.CssClass = "TablaLink"

            uwgSolicitudes.Bands(0).Columns.FromKey("COD").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free
            uwgSolicitudes.Bands(0).Columns.FromKey("DESCR").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free
            uwgSolicitudes.Bands(0).Columns.FromKey("ID_INS").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free
            uwgSolicitudes.Bands(0).Columns.FromKey("FECHA").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free
            uwgSolicitudes.Bands(0).Columns.FromKey("ITEM").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free
            uwgSolicitudes.Bands(0).Columns.FromKey("ESTADO_DEN").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free
            uwgSolicitudes.Bands(0).Columns.FromKey("NOM_USU").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free
            uwgSolicitudes.Bands(0).Columns.FromKey("ORIGEN").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free
            uwgSolicitudes.Bands(0).Columns.FromKey("COMENT").AllowResize = Infragistics.WebUI.UltraWebGrid.AllowSizing.Free

        End Sub

        ''' <summary>Carga la grid con las Solicitudes</summary>         
        ''' <remarks>Llamada desde: page_load   uwgParticipacion_ClickCellButton  cmdCargar_Click ssEstados_ClickCellButton; Tiempo m�ximo:0,3</remarks>
        Private Sub CargarGridSolicitudes(Optional ByVal iFilaEstado As Integer = 0)
            ''''Carga la grid de solicitudes con las condiciones seleccionadas:
            Dim Instancias As Fullstep.PMPortalServer.Instancias
            Dim lId As Long
            Dim lTipo As Long
            Dim lCiaComp As Long = IdCiaComp
            Dim sFechaBD As String

            Instancias = FSPMServer.Get_Instancias

            lId = 0
            If Trim(Me.txtID.Text) <> "" Then
                If IsNumeric(Me.txtID.Text) Then
                    lId = CLng(Me.txtID.Text)
                End If
            End If

            lTipo = 0
            If Me.wddTipoSolicitud.SelectedItemIndex > 0 Then 'algo selecc y q no sea "Cualquier tipo"
                lTipo = CLng(Me.wddTipoSolicitud.SelectedItem.Value)
            End If

            If txtFecDesde.Value = Nothing Or IsTime(txtFecDesde.Value) Then
                Instancias.DevolverWorkflow(lCiaComp, FSPMUser.CodProve, EmpresaPortal, NomPortal, Idioma, lTipo, Nothing, lId, iFilaEstado, FSPMUser.DateFmt)
            Else
                sFechaBD = DateToBDDate(txtFecDesde.Value)
                Instancias.DevolverWorkflow(lCiaComp, FSPMUser.CodProve, EmpresaPortal, NomPortal, Idioma, lTipo, sFechaBD, lId, iFilaEstado, FSPMUser.DateFmt)
            End If

            If Instancias.Data.Tables.Count > 0 Then
                ssEstados.Rows(0).Cells(2).Value = Instancias.Data.Tables(0).Rows(0).Item("PENDIENTES")

                uwgParticipacion.Rows(0).Cells(2).Value = Instancias.Data.Tables(1).Rows(0).Item("EN_CURSO")
                uwgParticipacion.Rows(1).Cells(2).Value = Instancias.Data.Tables(2).Rows(0).Item("RECHAZADAS")
                uwgParticipacion.Rows(2).Cells(2).Value = Instancias.Data.Tables(3).Rows(0).Item("ANULADAS")
                uwgParticipacion.Rows(3).Cells(2).Value = Instancias.Data.Tables(4).Rows(0).Item("FINALIZADAS")

                uwgSolicitudes.DataSource = Instancias.Data.Tables(5)
                uwgSolicitudes.DataBind()

                Dim indice As Integer
                Dim bOcultarColumnaUsuario As Boolean = True
                For indice = 0 To uwgSolicitudes.Rows.Count - 1
                    If uwgSolicitudes.Rows(indice).Cells.FromKey("VER_DETALLE_PER").Value = 1 Then
                        uwgSolicitudes.Rows(indice).Cells.FromKey("NOM_USU").Style.CssClass = "TablaLink"
                        bOcultarColumnaUsuario = False
                    Else
                        uwgSolicitudes.Rows(indice).Cells.FromKey("NOM_USU").Value = ""
                    End If
                Next

                If bOcultarColumnaUsuario = True Then
                    uwgSolicitudes.Bands(0).Columns.FromKey("NOM_USU").Hidden = True
                End If
            End If

            'ahora dibuja la flecha que indica que estado se ha seleccionado
            Dim i As Integer
            If iFilaEstado = 0 Then
                ssEstados.Rows(0).Cells(1).Style.CustomRules = "Background-repeat:no-repeat;background-position:center"
                ssEstados.Rows(0).Cells(1).Style.BackgroundImage = "images/FlechaIzdaNegra.gif"

                For i = 0 To 3
                    uwgParticipacion.Rows(i).Cells(1).Style.CustomRules = ""
                    uwgParticipacion.Rows(i).Cells(1).Style.BackgroundImage = ""
                Next
            Else
                ssEstados.Rows(0).Cells(1).Style.CustomRules = ""
                ssEstados.Rows(0).Cells(1).Style.BackgroundImage = ""

                For i = 1 To 4
                    If i = iFilaEstado Then
                        uwgParticipacion.Rows(i - 1).Cells(1).Style.CustomRules = "Background-repeat:no-repeat;background-position:center"
                        uwgParticipacion.Rows(i - 1).Cells(1).Style.BackgroundImage = "images/FlechaIzdaNegra.gif"
                    Else
                        uwgParticipacion.Rows(i - 1).Cells(1).Style.CustomRules = ""
                        uwgParticipacion.Rows(i - 1).Cells(1).Style.BackgroundImage = ""
                    End If
                Next i

            End If

            Instancias = Nothing

        End Sub
        ''' <summary>
        ''' Funcion que se ejecuta al exportar el grid se solicitudes, se retocan las celdas que tienen enlaces.
        ''' </summary>
        ''' <param name="sender">objeto  UltraWebGridExcelExporter</param>
        ''' <param name="e">Fila exportada</param>
        ''' <remarks></remarks>
        Private Sub UltraWebGridExcelExporter1_RowExported(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.ExcelExport.RowExportedEventArgs) Handles UltraWebGridExcelExporter1.RowExported

            Dim tmRow As Infragistics.Excel.WorksheetRow

            tmRow = e.CurrentWorksheet.Rows(e.CurrentRowIndex)
            e.CurrentWorksheet.Columns(0).Width = 7000
            e.CurrentWorksheet.Columns(1).Width = 7000
            e.CurrentWorksheet.Columns(4).Width = 7000
            Dim sPatron As String = "<A *>*</A>"
            Dim sCadena As String
            If tmRow.Cells(0).Value IsNot Nothing AndAlso tmRow.Cells(0).Value.ToString.ToUpper Like sPatron Then
                Dim sPos As Short = tmRow.Cells(0).Value.ToString.IndexOf(">")
                sCadena = tmRow.Cells(0).Value.ToString.Substring(0, sPos + 1)
                tmRow.Cells(0).Value = tmRow.Cells(0).Value.ToString.Replace(sCadena, "")
                tmRow.Cells(0).Value = tmRow.Cells(0).Value.ToString.Replace("</a>", "")
            End If
            If tmRow.Cells(1).Value IsNot Nothing AndAlso tmRow.Cells(1).Value.ToString.ToUpper Like sPatron Then
                Dim sPos As Short = tmRow.Cells(1).Value.ToString.IndexOf(">")
                sCadena = tmRow.Cells(1).Value.ToString.Substring(0, sPos + 1)
                tmRow.Cells(1).Value = tmRow.Cells(1).Value.ToString.Replace(sCadena, "")
                tmRow.Cells(1).Value = tmRow.Cells(1).Value.ToString.Replace("</a>", "")
            End If
            If tmRow.Cells(4).Value IsNot Nothing AndAlso tmRow.Cells(4).Value.ToString.ToUpper Like sPatron Then
                Dim sPos As Short = tmRow.Cells(4).Value.ToString.IndexOf(">")
                sCadena = tmRow.Cells(4).Value.ToString.Substring(0, sPos + 1)
                tmRow.Cells(4).Value = tmRow.Cells(4).Value.ToString.Replace(sCadena, "")
                tmRow.Cells(4).Value = tmRow.Cells(4).Value.ToString.Replace("</a>", "")

                If tmRow.Cells(4).Value = Textos(31) Then
                    tmRow.Cells(4).Value = ObtenerItemsInstancia(IdCiaComp, tmRow.Cells(2).Value, True)
                End If
            End If

        End Sub

        ''' <summary>Obtiene los items de una instancia</summary>
        ''' <param name="idInstancia">Id de la instancia</param>
        ''' <returns>array de objetos con el c�digo de los �tems de la instancia</returns>
        ''' ''' <remarks>llamada desde: seguimientoSolicitudes.aspx</remarks>
        <System.Web.Services.WebMethod(True),
        System.Web.Script.Services.ScriptMethod()>
        Public Shared Function ObtenerItemsInstancia(ByVal lCiaComp As Integer, ByVal idInstancia As Integer, ByVal bSoloTexto As Boolean) As Object
            Dim FSPMServer As Fullstep.PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
            Dim oInstancia As Fullstep.PMPortalServer.Instancia

            oInstancia = FSPMServer.Get_Instancia
            oInstancia.ID = idInstancia
            Return oInstancia.DevolverItems(lCiaComp, bSoloTexto)
        End Function
    End Class
End Namespace