Namespace Fullstep.PMPortalWeb
    Partial Class detalleSolicitud
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private _bSoloLectura As Boolean = False
        Protected Property bSoloLectura() As Boolean
            Get
                _bSoloLectura = ViewState("bSoloLectura")
                Return _bSoloLectura
            End Get
            Set(ByVal value As Boolean)
                _bSoloLectura = value
                ViewState("bSoloLectura") = _bSoloLectura
            End Set
        End Property
        Private _oInstancia As Fullstep.PMPortalServer.Instancia
        ''' <summary>
        ''' Crear el objeto Instancia para la pantalla. Es Request("Instancia").
        ''' </summary>
        ''' <returns>Objeto Instancia</returns>
        ''' <remarks>Llamada desde:Page_Load Page_Init; Tiempo m�ximo:0,2</remarks>
        Protected ReadOnly Property oInstancia() As Fullstep.PMPortalServer.Instancia
            Get
                If _oInstancia Is Nothing Then
                    If IsPostBack Then
                        _oInstancia = CType(Cache("oInstancia_" & FSPMUser.Cod), Fullstep.PMPortalServer.Instancia)
                    Else
                        _oInstancia = FSPMServer.Get_Instancia
                        _oInstancia.ID = Request("Instancia")

                        _oInstancia.Load(IdCiaComp, Idioma, True)
                        _oInstancia.Solicitud.Load(IdCiaComp, Idioma)
                        _oInstancia.CargarCamposInstancia(IdCiaComp, FSPMUser.IdCia, Idioma, , FSPMUser.CodProveGS, False, True)

                        InsertarEnCache("oInstancia_" & FSPMUser.Cod, _oInstancia)
                    End If
                End If
                Return _oInstancia
            End Get
        End Property

        ''' <summary>
        ''' Precarga de pagina, crea los controles, es decir, tabs, desglose, campos -> entrys
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>      
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,3</remarks>
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            InitializeComponent()

            Dim oGrupo As Fullstep.PMPortalServer.Grupo
            Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
            Dim oucCampos As Fullstep.PMPortalWeb.campos
            Dim oucDesglose As Fullstep.PMPortalWeb.desgloseControl
            Dim lIndex As Integer = 0
            Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden
            Dim oRow As DataRow
			Dim oAcciones As DataTable

			ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.GestionTraslado

            'Rellena el tab :
            uwtGrupos.Tabs.Clear()
            AplicarEstilosTab(uwtGrupos)
			ConfigurarEstadoSolicitud()

			If Not oInstancia.Grupos.Grupos Is Nothing Then
                'Comprueba si el proveedor es rol activo o traslado activo en la solicitud:
                oInstancia.DevolverEtapaActual(IdCiaComp, Idioma, FSPMUser.CodProveGS)
                oInstancia.DevolverDatosTraslado(IdCiaComp, FSPMUser.CodProveGS)

                'Es una por la que pas�
                If oInstancia.RolActual = 0 Or oInstancia.Etapa = 0 Then bSoloLectura = True

                For Each oGrupo In oInstancia.Grupos.Grupos
                    oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

                    oInputHidden.ID = "txtPre_" + lIndex.ToString
                    oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                    Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                    Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                    oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(Idioma), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                    oTabItem.Key = lIndex.ToString
                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                    uwtGrupos.Tabs.Add(oTabItem)
                    lIndex += 1

                    If oGrupo.DSCampos.Tables.Count > 0 Then
                        If oGrupo.NumCampos <= 1 Then
                            For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                                If oRow.Item("VISIBLE") = 1 Then
                                    Exit For
                                End If

                            Next
                            If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Desglose Or oRow.Item("SUBTIPO") = Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoDesglose Then
                                If oRow.Item("VISIBLE") = 0 Then
                                    uwtGrupos.Tabs.Remove(oTabItem)
                                Else
                                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                                    oTabItem.ContentPane.UserControlUrl = "../_common/desglose.ascx"
                                    oucDesglose = oTabItem.ContentPane.UserControl
                                    oucDesglose.ID = oGrupo.Id.ToString
                                    oucDesglose.Campo = oRow.Item("ID_CAMPO")
                                    oucDesglose.TabContainer = uwtGrupos.ClientID
                                    oucDesglose.Instancia = oInstancia.ID
                                    oucDesglose.Ayuda = DBNullToSomething(oRow.Item("AYUDA_" & Idioma))
                                    oucDesglose.TieneIdCampo = True
                                    oucDesglose.SoloLectura = bSoloLectura Or (oRow.Item("ESCRITURA") = 0)
                                    oucDesglose.Version = oInstancia.Version
                                    oInputHidden.Value = oucDesglose.ClientID
                                    oucDesglose.Acciones = oAcciones
                                    oucDesglose.InstanciaMoneda = oInstancia.Moneda
                                    oucDesglose.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                                End If
                            Else
                                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                                oTabItem.ContentPane.UserControlUrl = "../_common/campos.ascx"
                                oucCampos = oTabItem.ContentPane.UserControl
                                oucCampos.Instancia = oInstancia.ID
                                oucCampos.ID = oGrupo.Id.ToString
                                oucCampos.dsCampos = oGrupo.DSCampos
                                oucCampos.IdGrupo = oGrupo.Id
                                oucCampos.Idi = Idioma
                                oucCampos.TabContainer = uwtGrupos.ClientID
                                oucCampos.Version = oInstancia.Version
                                oucCampos.SoloLectura = bSoloLectura
                                oucCampos.Acciones = oAcciones
                                oucCampos.InstanciaEstado = oInstancia.Estado
                                oucCampos.InstanciaMoneda = oInstancia.Moneda
                                oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                                oucCampos.Formulario = oInstancia.Solicitud.Formulario
                                oucCampos.ObjInstancia = oInstancia
                                oInputHidden.Value = oucCampos.ClientID
                            End If

                        Else
                            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                            oTabItem.ContentPane.UserControlUrl = "../_common/campos.ascx"
                            oucCampos = oTabItem.ContentPane.UserControl
                            oucCampos.Instancia = oInstancia.ID
                            oucCampos.ID = oGrupo.Id.ToString
                            oucCampos.dsCampos = oGrupo.DSCampos
                            oucCampos.IdGrupo = oGrupo.Id
                            oucCampos.Idi = Idioma
                            oucCampos.TabContainer = uwtGrupos.ClientID
                            oucCampos.Version = oInstancia.Version
                            oucCampos.SoloLectura = bSoloLectura
                            oInputHidden.Value = oucCampos.ClientID
                            oucCampos.Acciones = oAcciones
                            oucCampos.InstanciaEstado = oInstancia.Estado
                            oucCampos.InstanciaMoneda = oInstancia.Moneda
                            oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                            oucCampos.Formulario = oInstancia.Solicitud.Formulario
                            oucCampos.ObjInstancia = oInstancia
                        End If
                    End If
                    FindControl("frmDetalle").Controls.Add(oInputHidden)
                Next
            End If

            Dim oucParticipantes As Fullstep.PMPortalWeb.participantes
            Dim bParticipantes As Boolean = False
            Dim oRol As Fullstep.PMPortalServer.Rol

            Dim oProveedores As Fullstep.PMPortalServer.Proveedores
            oProveedores = FSPMServer.get_Proveedores
            oProveedores.LoadData(Idioma, IdCiaComp, FSPMUser.CodProveGS)
            If oProveedores.Data.Tables.Count > 0 AndAlso oProveedores.Data.Tables(0).Rows.Count > 0 Then
                sCodProve = oProveedores.Data.Tables(0).Rows(0).Item("COD")
            End If
            oProveedores = Nothing

            If oInstancia.RolActual = 0 Or oInstancia.Etapa = 0 Then 'Es una por la que pas�
            ElseIf oInstancia.DestinatarioEst = sCodProve Then  'El proveedor es el destinatario
            Else  'Es el rol actual:
                bParticipantes = True
            End If

            If bParticipantes = True Then
                'Si hay roles a asignar en esta etapa del workflow:
                oRol = FSPMServer.Get_Rol
                oRol.Id = oInstancia.RolActual
                oRol.Bloque = oInstancia.Etapa
                oRol.CargarParticipantes(IdCiaComp, Idioma, oInstancia.ID)
                If oRol.Participantes.Tables(0).Rows.Count > 0 Then
                    oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                    oTabItem.Text = Textos(25) 'Participantes
                    oTabItem.Key = "PARTICIPANTES"

                    uwtGrupos.Tabs.Add(oTabItem)

                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                    oTabItem.ContentPane.UserControlUrl = "participantes.ascx"

                    oucParticipantes = oTabItem.ContentPane.UserControl
                    oucParticipantes.Idi = Idioma
                    oucParticipantes.TabContainer = uwtGrupos.ClientID

                    oucParticipantes.dsParticipantes = oRol.Participantes

                    oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden
                    oInputHidden.ID = "txtPre_PARTICIPANTES"
                    oInputHidden.Value = oucParticipantes.ClientID
                    FindControl("frmDetalle").Controls.Add(oInputHidden)
                End If

            End If

            FindControl("frmDetalle").Controls.Add(Fullstep.PMPortalWeb.CommonInstancia.InsertarCalendario(Idioma))
        End Sub
#End Region
        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
        Private sCodProve As String

        ''' <summary>
        ''' Mostrar la pagina del detalle de una solicitud
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">evento de sistema</param>           
        ''' <remarks>Llamada desde: guardarinstancia.aspx.vb/GuardarConWorkflow  guardarinstancia.aspx.vb/cmdCancelar_ServerClick 
        ''' guardarinstancia.aspx.vb/cmdCancelarDevolucion_ServerClick           devolucion.aspx.vb/cmdCancelar_ServerClick
        ''' RealizarAccion.aspx.vb/cmdCancelar_ServerClick                       seguimientoSolicitudes.aspx/mostrarVentana
        ''' ; Tiempo m�ximo: 0,4</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.GestionTraslado

            If Page.IsPostBack Then Exit Sub

            ConfigurarCabeceraMenu()
            ConfigurarEstadoSolicitud()

            Dim drDetallePersona As DataRow = FSPMUser.VerDetallePersona(oInstancia.ID, IdCiaComp).Rows(0)

            If drDetallePersona("VER_DETALLE_PER") Then
                If oInstancia.PeticionarioProve = "" Then
                    imgInfCreadoPor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosPeticionario.AnimationClientID & "', event, '" & FSNPanelDatosPeticionario.DynamicPopulateClientID & "', '" & oInstancia.Peticionario & "'); return false;")
                Else
                    imgInfCreadoPor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosProveedor.AnimationClientID & "', event, '" & FSNPanelDatosProveedor.DynamicPopulateClientID & "', '" & oInstancia.PeticionarioProve & "'); return false;")
                End If
                imgInfTrasladadaPor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosTrasladadaPor.AnimationClientID & "', event, '" & FSNPanelDatosTrasladadaPor.DynamicPopulateClientID & "', '" & oInstancia.PersonaEst & "'); return false;")
            Else
                imgInfCreadoPor.Visible = False
                lblLitCreadoPor.Visible = False
                lblCreadoPor.Visible = False
                imgInfTrasladadaPor.Visible = False
            End If
            If drDetallePersona("VER_FLUJO") Then
                HyperDetalle.NavigateUrl = "javascript:irAcomentarios()"
            Else
                HyperDetalle.Visible = False
            End If

            imgInfTipo.Attributes.Add("onclick", "DetalleTipoSolicitud()")

            'Textos de idiomas:
            lblLitTipo.Text = Textos(1)
            lblLitCreadoPor.Text = Textos(3)
            lblLitFecAlta.Text = Textos(30)
            lblLitEstado.Text = Textos(45) & ":"
            HyperDetalle.Text = Textos(5)
            lblLitFechaTraslado.Text = Textos(8)
            lblLitTrasladadaPor.Text = Textos(7)
            lblCamposObligatorios.Text = Textos(28)

            cadenaespera.Value = Textos(26)

            If Not oInstancia.CampoImporte Is Nothing Then
                lblLitImporte.Text = oInstancia.CampoImporte & ":"
                lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSPMUser.NumberFormat) & " " & oInstancia.Moneda
            Else
                If oInstancia.Solicitud.Tipo = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras Then
                    lblLitImporte.Text = Textos(17)
                    lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSPMUser.NumberFormat) & " " & oInstancia.Moneda
                Else
                    lblImporte.Visible = False
                End If
            End If

            'Textos Panel info
            FSNPanelDatosPeticionario.Titulo = Textos(32) '"Peticionario"
            FSNPanelDatosPeticionario.SubTitulo = Textos(31) '"Informacion detallada"

            Dim oValidacionesIntegracion As PMPortalServer.ValidacionesIntegracion = FSPMServer.Get_ValidacionesIntegracion
            PantallaMaper.Value = oValidacionesIntegracion.HayIntegracionSalidaoEntradaSalida({TablasIntegracion.PM}, IdCiaComp)
            'Datos de la instancia:

            lblTipo.Text = oInstancia.Solicitud.Codigo + " - " + oInstancia.Solicitud.Den(Idioma)

            If oInstancia.PeticionarioProve = "" Then
                lblCreadoPor.Text = oInstancia.NombrePeticionario
            Else
                lblCreadoPor.Text = oInstancia.PeticionarioProve + " (" + oInstancia.NombrePeticionario + ")"
            End If
            lblEstado.Text = oInstancia.CargarEstapaActual(FSPMUser.CiaComp, Idioma)
            lblFecAlta.Text = modUtilidades.FormatDate(oInstancia.FechaAlta, FSPMUser.DateFormat)
            txtIdTipo.Value = oInstancia.Solicitud.ID
            Instancia.Value = oInstancia.ID
            Version.Value = oInstancia.Version
            
            Bloque.Value = oInstancia.Etapa

            If oInstancia.RolActual = 0 Or oInstancia.Etapa = 0 Then  'Es una por la que pas�
                lblLitTrasladadaPor.Visible = False
                lblLitFechaTraslado.Visible = False
                lblLitFechaDevolucion.Visible = False
                imgInfTrasladadaPor.Visible = False
            ElseIf oInstancia.DestinatarioEst = sCodProve Then  'El proveedor es el destinatario
                If oInstancia.FecLimiteTraslado = Nothing Then
                    lblLitFechaDevolucion.Visible = False
                    lblFechaDevolucion.Visible = False
                Else
                    lblLitFechaDevolucion.Text = Textos(11)
                    lblFechaDevolucion.Text = FormatDate(oInstancia.FecLimiteTraslado, FSPMUser.DateFormat)
                End If
                lblTrasladadaPor.Text = oInstancia.NombrePersonaEst
                lblFechaTraslado.Text = modUtilidades.FormatDate(oInstancia.FechaEstado, FSPMUser.DateFormat)
            Else  'Es el rol actual:
                lblLitTrasladadaPor.Visible = False
                lblLitFechaTraslado.Visible = False
                lblLitFechaDevolucion.Visible = False
                imgInfTrasladadaPor.Visible = False
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim sClientTexts As String
            sClientTexts = ""
            sClientTexts += "var arrTextosML = new Array();"
            sClientTexts += "arrTextosML[0] = '" + JSText(Textos(20)) + "';"
            sClientTexts += "arrTextosML[1] = '" + JSText(Textos(22)) + "';"
            sClientTexts += "arrTextosML[2] = '" + JSText(Textos(46)) + "';"
            sClientTexts += "arrTextosML[3] = '" + JSText(Textos(47)) + "';"

            If oInstancia.InstanciaBloqueBloq = Fullstep.PMPortalServer.TipoBloqueoEtapa.BloqueoSalida Then
                Dim sMensaje As String
                Dim oBloque As Fullstep.PMPortalServer.Bloque
                Dim oDS As DataSet

                oBloque = FSPMServer.Get_Bloque
                oBloque.Id = oInstancia.Etapa
                oDS = oBloque.DevolverEtapasBloqueoSalida(IdCiaComp, Idioma)

                sMensaje = Textos(23) & vbCrLf & Textos(24) & vbCrLf
                For Each oRow As DataRow In oDS.Tables(0).Rows
                    sMensaje = sMensaje & oRow.Item("DEN") & vbCrLf
                Next
                sClientTexts += "arrTextosML[2] = '" + JSText(sMensaje) + "';"

                oBloque = Nothing
            End If
            sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim ilGMN1 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
            Dim ilGMN2 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
            Dim ilGMN3 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
            Dim ilGMN4 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN4
            Dim sScript As String

            sScript = ""
            sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
            sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
            sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
            sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)

            If Not Page.ClientScript.IsClientScriptBlockRegistered("tipoVisor") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tipoVisor", "<script>var tipoVisor=" & IIf(Request.QueryString("TipoVisor") Is Nothing, 0, Request.QueryString("TipoVisor")) & ";</script>")

            If Not Page.ClientScript.IsClientScriptBlockRegistered("rutaVolver") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaVolver", "<script>var rutaVolver = " & IIf(Request.QueryString("TipoVisor") Is Nothing,
                                                                                                                          "'seguimientoSolicitudes.aspx'",
                                                                                                                          "'VisorSolicitudes.aspx?" & Replace(Request.QueryString("volver"), "**", "&") & "'") & ";</script>")

            If Not Page.ClientScript.IsClientScriptBlockRegistered("rutaFacturas") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFacturas", "<script>var rutaFacturas=" & IIf(Request.QueryString("TipoVisor") Is Nothing, False.ToString.ToLower(), True.ToString.ToLower()) & ";</script>")

            If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array();</script>")

            If Not Page.ClientScript.IsClientScriptBlockRegistered("varFila") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFila", "<script>var sFila = '" & JSText(Textos(27)) & "';</script>")

            If Not ClientScript.IsStartupScriptRegistered("oRol_Id") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "oRol_Id", "var oRol_Id=" & oInstancia.RolActual & ";", True)

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)
            End If
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutanormal") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutanormal", "var rutanormal ='" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "';", True)
            End If

            Dim sClientTextVars As String
            sClientTextVars = ""
            sClientTextVars += "vdecimalfmt='" + FSPMUser.DecimalFmt + "';"
            sClientTextVars += "vthousanfmt='" + FSPMUser.ThousanFmt + "';"
            sClientTextVars += "vprecisionfmt='" + FSPMUser.PrecisionFmt + "';"
            sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)
        End Sub
        ''' Revisado por: sra. Fecha: 24/01/2012
        ''' <summary>
        ''' Configura la cabecera, muestra en el menu las posibles acciones
        ''' Activamos los botones que pueden aparecer en la pantalla
        ''' </summary>
        ''' <remarks>Llamada desde:=Page_load; Tiempo m�ximo:=0,1seg.</remarks>
        Private Sub ConfigurarCabeceraMenu()
            FSNPageHeader.TituloCabecera = If(oInstancia.Den(Idioma) <> "", oInstancia.Den(Idioma), oInstancia.Solicitud.Den(Idioma))
            FSNPageHeader.UrlImagenCabecera = "images/SolicitudesPMSmall.jpg"

            FSNPageHeader.TextoBotonCalcular = Textos(19)
            FSNPageHeader.TextoBotonDevolver = Textos(13)
            FSNPageHeader.TextoBotonAccion1 = Textos(21) 'Otras
            FSNPageHeader.TextoBotonVolver = Textos(29)
            FSNPageHeader.TextoBotonEliminar = Textos(43)
            FSNPageHeader.TextoBotonGuardar = Textos(44)
            FSNPageHeader.TextoBotonImpExp = Textos(48)
            FSNPageHeader.OnClientClickEliminar = "EliminarInstancia(); return false;"
            FSNPageHeader.OnClientClickGuardar = "Guardar(); return false;"
            FSNPageHeader.VisibleBotonImpExp = True
            FSNPageHeader.OnClientClickImpExp = "return cmdImpExp_onclick();"
            FSNPageHeader.VisibleBotonVolver = True

            FSNPageHeader.OnClientClickVolver = "return Volver()"

            'Comprobamos si hay que hacer visible el boton de calcular
            If oInstancia.PeticionarioProve = FSPMUser.CodProve AndAlso Not {TipoEstadoSolic.Anulada,  'Estado anulada o finalizada
                                                                            TipoEstadoSolic.SCPendiente,
                                                                            TipoEstadoSolic.SCAprobada,
                                                                            TipoEstadoSolic.SCRechazada,
                                                                            TipoEstadoSolic.SCAnulada,
                                                                            TipoEstadoSolic.SCCerrada}.Contains(oInstancia.Estado) Then
                'SI EXISTE ALGUN CAMPO DE TIPO=3 (CALCULADO) HACEMOS VISIBLE EL BOTON DE CALCULAR
                FSNPageHeader.VisibleBotonCalcular = oInstancia.Grupos.Grupos.OfType(Of PMPortalServer.Grupo).Where(Function(x) x.DSCampos.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) y("TIPO") = TipoCampoPredefinido.Calculado).Any).Any()
                If FSNPageHeader.VisibleBotonCalcular Then FSNPageHeader.OnClientClickCalcular = "return CalcularCamposCalculados()"
            End If

            If oInstancia.PeticionarioProve = FSPMUser.CodProve Then 'Si es el peticionario podr� hacer algo, sino no: 
                Select Case oInstancia.Estado
                    Case TipoEstadoSolic.Guardada   'Guardada
                        FSNPageHeader.VisibleBotonEliminar = True
                        FSNPageHeader.VisibleBotonGuardar = True
                    Case TipoEstadoSolic.Rechazada  'Rechazada    
						FSNPageHeader.VisibleBotonAccion1 = True
						lblCamposObligatorios.Visible = False
						'FSNPageHeader.VisibleBotonEliminar = True  'No se pueden eliminar solicitudes rechazadas
					Case TipoEstadoSolic.Anulada
						lblCamposObligatorios.Visible = False
				End Select
            End If
            If Not oInstancia.Estado = TipoEstadoSolic.Anulada Then
                If oInstancia.RolActual = 0 Or oInstancia.Etapa = 0 Then  'Es una por la que pas�
                    FSNPageHeader.VisibleBotonAccion1 = False
                    FSNPageHeader.VisibleBotonGuardar = False
                    FSNPageHeader.VisibleBotonEnviarAFavoritos = False
                ElseIf oInstancia.DestinatarioEst = sCodProve Then  'El proveedor es el destinatario
                    FSNPageHeader.VisibleBotonDevolver = True
                    FSNPageHeader.OnClientClickDevolver = "Devolver(); return false;"
                Else
                    Dim oRol As Fullstep.PMPortalServer.Rol
                    'Carga las acciones posibles en el bot�n de Otras acciones:
                    oRol = FSPMServer.Get_Rol
                    oRol.Id = oInstancia.RolActual
                    oRol.Bloque = oInstancia.Etapa

                    'Carga las acciones
                    Dim oDSAcciones As DataSet
                    If oInstancia.Estado = TipoEstadoSolic.Rechazada = True Then
                        oDSAcciones = oRol.CargarAcciones(IdCiaComp, Idioma, True)
                    Else
                        oDSAcciones = oRol.CargarAcciones(IdCiaComp, Idioma)
                    End If

                    Dim oPopMenu As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
                    Dim oItem As Infragistics.WebUI.UltraWebNavigator.Item

                    oPopMenu = uwPopUpAcciones
                    Dim blnAprobar As Boolean = False
                    Dim blnRechazar As Boolean = False
                    Dim iAccionesRestarOtrasAcciones As Integer
                    Dim iOtrasAcciones As Integer
                    If oDSAcciones.Tables.Count > 0 Then
                        If oDSAcciones.Tables(0).Rows.Count > 1 Then
                            'Comprobamos si entre las acciones hay de tipo aprobar y rechazar
                            For Each oRow As DataRow In oDSAcciones.Tables(0).Rows
                                If DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                                    blnAprobar = True
                                    iAccionesRestarOtrasAcciones = iAccionesRestarOtrasAcciones + 1
                                    If blnRechazar Then Exit For
                                ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                                    blnRechazar = True
                                    iAccionesRestarOtrasAcciones = iAccionesRestarOtrasAcciones + 1
                                    If blnAprobar Then Exit For
                                End If
                            Next
                            iOtrasAcciones = oDSAcciones.Tables(0).Rows.Count - iAccionesRestarOtrasAcciones
                            Dim cont As Integer = 0
                            For Each oRow In oDSAcciones.Tables(0).Rows
                                If iOtrasAcciones > 3 AndAlso DBNullToSomething(oRow.Item("RA_APROBAR")) <> 1 AndAlso DBNullToSomething(oRow.Item("RA_RECHAZAR")) <> 1 Then
                                    If IsDBNull(oRow.Item("DEN")) Then
                                        oItem = oPopMenu.Items.Add("&nbsp;")
                                    Else
                                        If oRow.Item("DEN") = "" Then
                                            oItem = oPopMenu.Items.Add("&nbsp;")
                                        Else
                                            oItem = oPopMenu.Items.Add(DBNullToStr(oRow.Item("DEN")))
                                        End If
                                    End If
                                    oItem.TargetUrl = "javascript:EjecutarAccion(" + oRow.Item("ACCION").ToString() + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "')"
                                ElseIf DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                                    FSNPageHeader.OnClientClickAprobar = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                                    FSNPageHeader.TextoBotonAprobar = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                                ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                                    FSNPageHeader.OnClientClickRechazar = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                                    FSNPageHeader.TextoBotonRechazar = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                                Else
                                    If cont = 0 Then
                                        FSNPageHeader.OnClientClickAccion1 = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                                        FSNPageHeader.TextoBotonAccion1 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                                        FSNPageHeader.VisibleBotonAccion1 = True
                                        cont = 1
                                    ElseIf cont = 1 Then
                                        FSNPageHeader.OnClientClickAccion2 = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                                        FSNPageHeader.TextoBotonAccion2 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                                        FSNPageHeader.VisibleBotonAccion2 = True
                                        cont = 2
                                    Else
                                        FSNPageHeader.OnClientClickAccion3 = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                                        FSNPageHeader.TextoBotonAccion3 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                                        FSNPageHeader.VisibleBotonAccion3 = True
                                    End If
                                End If
                            Next
                            If blnAprobar Then
                                FSNPageHeader.VisibleBotonAprobar = True
                            End If
                            If blnRechazar Then
                                FSNPageHeader.VisibleBotonRechazar = True
                            End If
                            If iOtrasAcciones > 3 Then
                                FSNPageHeader.VisibleBotonAccion1 = True
                                FSNPageHeader.OnClientClickAccion1 = "igmenu_showMenu('uwPopUpAcciones', event); return false;"
                            End If
                        Else
                            If oDSAcciones.Tables(0).Rows.Count = 0 Then
                                FSNPageHeader.VisibleBotonAccion1 = False
                            Else
                                Dim oRow As DataRow
                                oRow = oDSAcciones.Tables(0).Rows(0)
                                If DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                                    FSNPageHeader.VisibleBotonAccion1 = False
                                    FSNPageHeader.VisibleBotonAprobar = True
                                    FSNPageHeader.OnClientClickAprobar = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                                    FSNPageHeader.TextoBotonAprobar = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                                ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                                    FSNPageHeader.VisibleBotonAccion1 = False
                                    FSNPageHeader.VisibleBotonRechazar = True
                                    FSNPageHeader.OnClientClickRechazar = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                                    FSNPageHeader.TextoBotonRechazar = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                                Else
                                    FSNPageHeader.VisibleBotonAccion1 = True
                                    FSNPageHeader.OnClientClickAccion1 = "EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + "," + oInstancia.Etapa.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                                    FSNPageHeader.TextoBotonAccion1 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                                End If
                            End If
                        End If
                    Else
                        FSNPageHeader.VisibleBotonAccion1 = False
                    End If
                End If
            End If
        End Sub
        Private Sub ConfigurarEstadoSolicitud()
            Dim sEtapaActual As String
            Select Case oInstancia.Estado
                Case TipoEstadoSolic.Guardada   'Guardada
                    sEtapaActual = Textos(34)
                Case TipoEstadoSolic.Pendiente  'En curso
                    sEtapaActual = Textos(40)
                    bSoloLectura = True
                Case TipoEstadoSolic.SCPendiente, TipoEstadoSolic.SCAprobada, TipoEstadoSolic.SCRechazada, TipoEstadoSolic.SCAnulada, TipoEstadoSolic.SCCerrada
                    sEtapaActual = Textos(41)
                    bSoloLectura = True
				Case TipoEstadoSolic.Rechazada  'Rechazada    'No se podra editar valores en solicitudes rechazadas
					sEtapaActual = Textos(37)
					bSoloLectura = True
				Case TipoEstadoSolic.Anulada  'Anulada 'No se podra editar valores en solicitudes anuladas
					sEtapaActual = Textos(38)
					bSoloLectura = True
				Case TipoEstadoSolic.SCCerrada  'Cerrada  
                    sEtapaActual = Textos(39)
                    bSoloLectura = True
                Case Else
                    sEtapaActual = oInstancia.CargarEstapaActual(FSPMUser.CiaComp, Idioma, True)
                    If sEtapaActual = "##" Then
                        sEtapaActual = Textos(42)
                    Else
                        sEtapaActual = Textos(40)
                    End If
            End Select
            lblIDInstanciayEstado.Text = oInstancia.ID & " (" & sEtapaActual & ")"
        End Sub
#Region "PageMethods"
        <Services.WebMethod(EnableSession:=True)>
        <Script.Services.ScriptMethod()>
        Public Shared Function EliminarInstancia(ByVal idInstancia As Integer) As Boolean
            Try
                Dim PMPortalServer As Fullstep.PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
                Dim FSPMUser As Fullstep.PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")

                'Elimina la instancia:
                Dim oInstancia As Fullstep.PMPortalServer.Instancia
                Dim oDS As DataSet

                oInstancia = PMPortalServer.Get_Instancia
                oInstancia.ID = idInstancia
                oDS = oInstancia.ItemsProcesoSolicitud(FSPMUser.CiaComp)

                If oDS.Tables(0).Rows.Count = 0 Then
                    oInstancia.Eliminar(FSPMUser.CiaComp)
                    Return True
                Else 'Existen procesos asociados
                    Return False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region
    End Class
End Namespace