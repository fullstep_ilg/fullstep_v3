<%@ Register TagPrefix="cc1" Namespace="Syncfusion.Web.UI.WebControls.Diagram" Assembly="Syncfusion.Diagram.Web, Version=4.102.0.62, Culture=neutral, PublicKeyToken=3d67ed1f87d44c89" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="flowDiagramExport.aspx.vb" Inherits="Fullstep.PMPortalWeb.flowDiagramExport"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
		<title>flowDiagramExport</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../../common/estilo.asp" type="text/css" rel="stylesheet">
		<script src="../../../common/formatos.js"></script>
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:button id="cmdBMP" style="Z-INDEX: 103; LEFT: 128px; POSITION: absolute; TOP: 80px" runat="server"
				Width="56px" Text="BMP" CssClass="boton"></asp:button><asp:xml id="xmlPres" runat="server"></asp:xml><INPUT id="Instancia" style="Z-INDEX: 105; LEFT: 8px; WIDTH: 40px; POSITION: absolute; TOP: 8px; HEIGHT: 22px"
				type="hidden" size="1" name="Instancia" runat="server">
			<cc1:diagramwebcontrol id="dwc" style="Z-INDEX: 100; LEFT: 32px; POSITION: absolute; TOP: 56px" runat="server"
				Height="16px" Width="144px" Visible="False" EnableViewState="False" SaveToSessionState="False"></cc1:diagramwebcontrol><asp:button id="cmdGIF" style="Z-INDEX: 101; LEFT: 216px; POSITION: absolute; TOP: 80px" runat="server"
				Width="56px" Text="GIF" CssClass="boton"></asp:button><asp:button id="cmdPDF" style="Z-INDEX: 104; LEFT: 304px; POSITION: absolute; TOP: 80px" runat="server"
				Width="56px" Text="PDF" CssClass="boton"></asp:button></form>
	</body>
</html>
