Imports Syncfusion.Windows.Forms.Diagram

<Serializable()> _
Public Class cDiagWebBloque
    Inherits Symbol
    Private outerRect As Rectangle = Nothing
    Public Shared tppX As Integer
    Public Shared tppY As Integer
    Public Id As Long


    Public Sub New(ByVal Id As Long, ByVal x As Long, ByVal y As Long, ByVal width As Long, ByVal height As Long, ByVal text As String, ByVal color As Color)

        ''' Clase para los bloques del workflow, con lo necesario para representarlos.
        ''' Heredan de la clase gen�rica de nodo SYMBOL de Essential Diagram.

        ' Identificador del bloque

        Me.Id = Id

        ' Define el rect�ngulo exterior.

        Me.outerRect = New Rectangle(x / tppX, y / tppY, width / tppX, height / tppY)
        Me.outerRect.Name = "Rectangle"
        Me.outerRect.FillStyle.Color = color
        Me.AppendChild(outerRect)

        ' Define la etiqueta de texto.

        Dim lbl As Label = Me.AddLabel(text, BoxPosition.Center)
        lbl.BackgroundStyle.Color = color.Transparent
        lbl.FontStyle.Family = "Tahoma"
        lbl.FontStyle.Size = 8
        lbl.HorizontalAlignment = StringAlignment.Center
        lbl.VerticalAlignment = StringAlignment.Center

    End Sub
End Class
