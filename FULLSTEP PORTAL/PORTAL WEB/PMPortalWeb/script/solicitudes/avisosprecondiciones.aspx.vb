Partial Class avisosprecondiciones
    Inherits FSPMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    ''' <summary>
    ''' Cargar la pagina. 
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>        
    ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oItem As System.Collections.Specialized.NameValueCollection
        Dim loop1 As Integer
        Dim arr1() As String
        Dim sRequest As String
        Dim arrAviso() As String

        Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")

        Dim sIdi As String = Session("FS_Portal_User").Idioma.ToString
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        Dim oDict As Fullstep.PMPortalServer.Dictionary = FSWSServer.Get_Dictionary()
        oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.AvisosPrecondiciones, sIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        lblTitulo.Text = oTextos.Rows(0).Item(1)

        Dim dtAvisos As DataTable = New DataTable
        Dim newRow As DataRow

        dtAvisos.Columns.Add("TIPO", System.Type.GetType("System.Int32"))
        dtAvisos.Columns.Add("TIPO_DEN", System.Type.GetType("System.Int32"))
        dtAvisos.Columns.Add("ID", System.Type.GetType("System.String"))
        dtAvisos.Columns.Add("MENSAJE", System.Type.GetType("System.String"))

        oItem = Request.Form
        arr1 = oItem.AllKeys
        For loop1 = 0 To arr1.GetUpperBound(0)
            sRequest = arr1(loop1).ToString

            arrAviso = oItem(sRequest).Split("|")

            newRow = dtAvisos.NewRow
            newRow.Item("TIPO") = arrAviso(0)
            newRow.Item("ID") = arrAviso(1)
            newRow.Item("MENSAJE") = arrAviso(2)

            dtAvisos.Rows.Add(newRow)
        Next

        uwgAvisos.DataSource = dtAvisos
        uwgAvisos.DataBind()

        'Idiomas en las caption de la grid: 
        uwgAvisos.Bands(0).Columns.FromKey("TIPO_DEN").Header.Caption = oTextos.Rows(1).Item(1)
        uwgAvisos.Bands(0).Columns.FromKey("ID").Header.Caption = oTextos.Rows(2).Item(1)
        uwgAvisos.Bands(0).Columns.FromKey("MENSAJE").Header.Caption = oTextos.Rows(3).Item(1)
    End Sub

    Private Sub uwgAvisos_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgAvisos.InitializeLayout
        With e.Layout.Bands(0)
            'Oculta las columnas:
            .Columns.FromKey("TIPO").Hidden = True

            .Columns.FromKey("TIPO_DEN").Width = Unit.Percentage(15)
            .Columns.FromKey("ID").Width = Unit.Percentage(10)
            .Columns.FromKey("MENSAJE").Width = Unit.Percentage(75)
        End With
    End Sub


    Private Sub uwgAvisos_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgAvisos.InitializeRow
        If e.Row.Cells.FromKey("TIPO").Value = 2 Then
            e.Row.Cells.FromKey("TIPO_DEN").Value = "Bloqueante"
            'La fila se ver� en rojo:
            e.Row.Style.CssClass = "ugfilatablaRoja"
        Else
            e.Row.Cells.FromKey("TIPO_DEN").Value = "Mensaje"
            'La fila se ver� en azul:
            e.Row.Style.CssClass = "ugfilatablaHist"
        End If

    End Sub

End Class
