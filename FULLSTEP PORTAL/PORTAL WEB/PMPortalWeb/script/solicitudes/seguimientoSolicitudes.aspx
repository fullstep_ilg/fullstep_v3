<%@ Page Language="vb" AutoEventWireup="false" Codebehind="seguimientoSolicitudes.aspx.vb" Inherits="Fullstep.PMPortalWeb.seguimientoSolicitudes"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
	    <title>seguimientoSolicitudes</title>
	    <meta content="True" name="vs_showGrid">
	    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
	    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
	    <meta content="JavaScript" name="vs_defaultClientScript">
	    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	    <LINK href="../../../common/estilo.asp" type="text/css" rel="stylesheet">
	    <script src="../../../common/formatos.js"></script>
	    <script src="../../../common/menu.asp"></script>
        <script src="../../js/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="../../js/jsUtilities.js" type="text/javascript"></script>
        <script src="../../js/jquery/jquery.tmpl.min.js" type="text/javascript"></script>
		<script src="~/js/jsUtilities.js"></script>
        <script type="text/javascript">
            /*''' <summary>
            ''' Iniciar la pagina.
            ''' </summary>     
            ''' <remarks>Llamada desde: page.onload; Tiempo m�ximo:0</remarks>*/
            function Init() {
                if (accesoExterno == 0) { document.getElementById('tablemenu').style.display = 'block'; }
            }
        </script>
    </head>
    <body onload="Init()">
        <script type="text/javascript">
            function ComprobarEnProceso(id_ins, cod, fecha, tipo, codContrato, contrato) {
                window.open("../_common/comprobarEnProceso.aspx?instancia=" + id_ins + "&tipo=" + cod + "&FecAlta=" + fecha + "&TipoSolicitud=" + tipo + "&Codigo=" + codContrato + "&Contrato=" + contrato, "iframeWSServer", "width=450,height=375,status=yes,resizable=no,top=200,left=200")
            }

            function VerComentariosSolicitud(IdInstancia) {
                window.open("comentariosSolicitud.aspx?Instancia=" + IdInstancia, "_blank", "width=1000,height=560,status=yes,resizable=no,top=200,left=200")
            }

            function mostrarVentana(enProceso, cert, inst, tipo, FecAlta, TipoSolicitud, CodContrato, Contrato) {
                if (enProceso != 0) //En PROCESO 
                    window.open("../solicitudes/EnProceso.aspx?Instancia=" + inst + "&Tipo=" + tipo + "&FecAlta=" + FecAlta, "_blank", "width=600,height=200,status=yes,resizable=no,top=200,left=200")
                else {
                    if (TipoSolicitud == 5)
                        //Tipo Contrato
                        window.open("../contratos/GestionContrato.aspx?Instancia=" + inst + "&Codigo=" + CodContrato + "&Contrato=" + Contrato, "_self");
                    else
                        window.open("../solicitudes/detalleSolicitud.aspx?Instancia=" + inst, "_self");
                }
            }

            /*''' <summary>
            ''' Evento que salta tras elegir un tipo de solicitud en el combo.
            ''' </summary>
            ''' <param name="sender">componente input</param>
            ''' <param name="eventArgs">evento</param>    
            ''' <remarks>Llamada desde=evento del dropdown; Tiempo maximo=0seg.</remarks>*/
            function wddTipoSolicitud_selectedIndexChanged(sender, eventArgs) {
                document.getElementById("hidTipoSol").value = sender.get_selectedItemIndex()
            }


            /*''' <summary>
            ''' Cuando hacemos click sobre uno de los estados del men� de la izquierda. Se lanza un postback para que se recargue la p�gina.
            ''' </summary>
            ''' <param name="id">fila sobre la que se ha pulsado</param>
            ''' <param name="eventArgs">evento</param>    
            ''' <remarks>Llamada desde=click del ultrawebgrid uwgParticipacion; Tiempo maximo=0seg.</remarks>*/
            function VerSolicitudesPorEstado(id) {
                __doPostBack("EstadoSolicitudes", id);
            }
        </script>
	
	    <IFRAME id="iframeWSServer" style="Z-INDEX: 106; LEFT: 8px; VISIBILITY: hidden; POSITION: absolute; TOP: 200px"
				    name="iframeWSServer" src="<%=Application("RUTASEGURA")%>script/blank.htm"></IFRAME>
	    <script>		    dibujaMenu(3)</script>
	    <form id="Form1" method="post" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
		    <asp:label id="lblTitulo" style="Z-INDEX: 103; LEFT: 10px; POSITION: absolute; margin-top:15pt;" runat="server"
			    Width="30%" CssClass="titulo"></asp:label><INPUT id="txtIdTipo" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 8px" type="hidden"
			    size="1" name="txtIdTipo" runat="server">
		    <TABLE id="tblMenu" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; margin-top:1pt;" borderColor="whitesmoke"
			    height="90%" cellSpacing="1" cellPadding="1" width="180" bgColor="whitesmoke" border="0">
			    <TR>
				    <TD class="EstadosFondo1" height="5%">
					    <TABLE id="Tablehidden" style="HEIGHT: 32px" cellSpacing="1" cellPadding="1" width="100%"
						    border="0">
						    <TR>
							    <TD>&nbsp;</TD>
						    </TR>
					    </TABLE>
				    </TD>
			    </TR>
			    <TR>
				    <TD class="EstadosFondo1" style="HEIGHT: 5.13%" vAlign="middle"><asp:label id="lblProcIntervencion" runat="server" Width="100%" CssClass="UnderlineRed"></asp:label></TD>
			    </TR>
			    <TR>
				    <TD class="EstadosFondo1" style="HEIGHT: 2.45%"><igtbl:ultrawebgrid id="ssEstados" runat="server" Width="200px" Height="148px">
						    <Rows>
							    <igtbl:UltraGridRow Height="">
								    <Cells>
									    <igtbl:UltraGridCell Key="DEN"></igtbl:UltraGridCell>
									    <igtbl:UltraGridCell Key="FLECHA"></igtbl:UltraGridCell>
									    <igtbl:UltraGridCell Key="NUM"></igtbl:UltraGridCell>
								    </Cells>
							    </igtbl:UltraGridRow>
						    </Rows>
						    <DisplayLayout ColHeadersVisibleDefault="No" RowHeightDefault="20px" Version="4.00" GridLinesDefault="Horizontal"
							    ScrollBarView="Horizontal" RowSelectorsDefault="No" Name="ssEstados">
							    <AddNewBox>
								    <Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
								    </Style>
							    </AddNewBox>
							    <Pager>
								    <Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
								    </Style>
							    </Pager>
							    <HeaderStyleDefault BorderStyle="Solid" BackColor="LightGray">
								    <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
							    </HeaderStyleDefault>
							    <FrameStyle Width="200px" BorderWidth="0px" Font-Size="8pt" Font-Names="Verdana" Height="148px"
								    CssClass="EstadosFondo1"></FrameStyle>
							    <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
								    <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
							    </FooterStyleDefault>
							    <ActivationObject AllowActivation="False" BorderColor="Transparent"></ActivationObject>
							    <EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
							    <RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="EstadosRow">
								    <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
							    </RowStyleDefault>
						    </DisplayLayout>
						    <Bands>
							    <igtbl:UltraGridBand>
								    <Columns>
									    <igtbl:UltraGridColumn Key="DEN" BaseColumnName="">
                                            <CellStyle CssClass="EstadosBoton"></CellStyle>
										    <Footer Key="DEN"></Footer>
										    <Header Key="DEN"></Header>
									    </igtbl:UltraGridColumn>
									    <igtbl:UltraGridColumn Key="FLECHA" Width="20%" BaseColumnName="">
										    <Footer Key="FLECHA"></Footer>
										    <Header Key="FLECHA"></Header>
									    </igtbl:UltraGridColumn>
									    <igtbl:UltraGridColumn Key="NUM" Width="40%" BaseColumnName="">
										    <CellStyle HorizontalAlign="Right"></CellStyle>
										    <Footer Key="NUM"></Footer>
										    <Header Key="NUM"></Header>
									    </igtbl:UltraGridColumn>
								    </Columns>
							    </igtbl:UltraGridBand>
						    </Bands>
					    </igtbl:ultrawebgrid></TD>
			    </TR>
			    <TR>
				    <TD class="EstadosFondo1" style="HEIGHT: 5.67%" vAlign="middle"><asp:label id="lblProcParticipacion" runat="server" Width="100%" CssClass="UnderlineBlue"></asp:label></TD>
			    </TR>
			    <TR>
				    <TD class="EstadosFondo1" style="HEIGHT: 9.58%"><igtbl:ultrawebgrid id="uwgParticipacion" runat="server" Width="200px" Height="120px">
						    <Rows>
							    <igtbl:UltraGridRow Height="">
								    <Cells>
									    <igtbl:UltraGridCell Key="DEN" Text=""></igtbl:UltraGridCell>
									    <igtbl:UltraGridCell Key="FLECHA" Text=""></igtbl:UltraGridCell>
									    <igtbl:UltraGridCell Key="NUM" Text=""></igtbl:UltraGridCell>
								    </Cells>
							    </igtbl:UltraGridRow>
							    <igtbl:UltraGridRow Height="">
								    <Cells>
									    <igtbl:UltraGridCell Key="DEN" Text=""></igtbl:UltraGridCell>
									    <igtbl:UltraGridCell Key="FLECHA" Text=""></igtbl:UltraGridCell>
									    <igtbl:UltraGridCell Key="NUM" Text=""></igtbl:UltraGridCell>
								    </Cells>
							    </igtbl:UltraGridRow>
							    <igtbl:UltraGridRow Height="">
								    <Cells>
									    <igtbl:UltraGridCell Key="DEN" Text=""></igtbl:UltraGridCell>
									    <igtbl:UltraGridCell Key="FLECHA" Text=""></igtbl:UltraGridCell>
									    <igtbl:UltraGridCell Key="NUM" Text=""></igtbl:UltraGridCell>
								    </Cells>
							    </igtbl:UltraGridRow>
							    <igtbl:UltraGridRow Height="">
								    <Cells>
									    <igtbl:UltraGridCell Key="DEN" Text=""></igtbl:UltraGridCell>
									    <igtbl:UltraGridCell Key="FLECHA" Text=""></igtbl:UltraGridCell>
									    <igtbl:UltraGridCell Key="NUM" Text=""></igtbl:UltraGridCell>
								    </Cells>
							    </igtbl:UltraGridRow>
						    </Rows>
						    <DisplayLayout ColHeadersVisibleDefault="No" RowHeightDefault="20px" Version="4.00" GridLinesDefault="Horizontal"
							    BorderCollapseDefault="Separate" RowSelectorsDefault="No" Name="uwgParticipacion">
							    <AddNewBox>
								    <Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
								    </Style>
							    </AddNewBox>
							    <Pager>
								    <Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
								    </Style>
							    </Pager>
							    <HeaderStyleDefault BorderStyle="Solid" BackColor="LightGray">
								    <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
							    </HeaderStyleDefault>
							    <FrameStyle Width="200px" BorderWidth="1px" Font-Size="8pt" Font-Names="Verdana" BorderStyle="None"
								    Height="120px" CssClass="EstadosFondo1"></FrameStyle>
							    <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
								    <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
							    </FooterStyleDefault>
							    <ActivationObject AllowActivation="False"></ActivationObject>
							    <EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
							    <RowStyleDefault BorderWidth="1px" BorderStyle="Solid" CssClass="EstadosRow">
								    <Padding Left="3px"></Padding>
								    <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
							    </RowStyleDefault>
						    </DisplayLayout>
						    <Bands>
							    <igtbl:UltraGridBand>
								    <Columns>
									    <igtbl:UltraGridColumn Key="DEN" BaseColumnName="">
										    <CellStyle CssClass="EstadosBoton"></CellStyle>
										    <Footer Key="DEN"></Footer>
										    <Header Key="DEN"></Header>
									    </igtbl:UltraGridColumn>
									    <igtbl:UltraGridColumn Key="FLECHA" Width="15%" BaseColumnName="" AllowUpdate="No">
										    <CellStyle HorizontalAlign="Left"></CellStyle>
										    <Footer Key="FLECHA"></Footer>
										    <Header Key="FLECHA"></Header>
									    </igtbl:UltraGridColumn>
									    <igtbl:UltraGridColumn Key="NUM" Width="40%" BaseColumnName="">
										    <CellStyle HorizontalAlign="Right"></CellStyle>
										    <Footer Key="NUM"></Footer>
										    <Header Key="NUM"></Header>
									    </igtbl:UltraGridColumn>
								    </Columns>
							    </igtbl:UltraGridBand>
						    </Bands>
					    </igtbl:ultrawebgrid></TD>
			    </TR>
			    <TR>
				    <TD class="EstadosFondo1" vAlign="bottom" height="40%"></TD>
			    </TR>
		    </TABLE>
		    <TABLE id="tblDatos" style="Z-INDEX: 101; margin-left: 206px; margin-top:10pt" height="90%"
			    cellSpacing="1" cellPadding="1" width="70%" border="0">
			    <TR>
				    <TD class="EstadosFondo1" height="5%" width="100%">
					    <TABLE id="Table1" style="HEIGHT: 32px" cellSpacing="1" cellPadding="1" width="100%" border="0">
						    <TR>
							    <TD><asp:label id="lblTipo" runat="server" CssClass="captionBlue" Height="16px"></asp:label></TD>
							    <TD><input id="hidTipoSol" type="hidden" runat="server" />
                                    <ig:WebDropDown ID ="wddTipoSolicitud" runat="server" EnableViewState="false" Height="22px" Width="200px" 
                                        EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" EnableClosingDropDownOnBlur="true">
                                        <ClientEvents SelectionChanged="wddTipoSolicitud_selectedIndexChanged"/>
                                    </ig:WebDropDown> 
                                </TD>
							    <TD><asp:label id="lblIdent" runat="server" CssClass="captionBlue"></asp:label></TD>
							    <TD><asp:textbox id="txtID" runat="server" Width="80px" CssClass="input"></asp:textbox></TD>
							    <TD><asp:label id="lblDesde" runat="server" CssClass="captionBlue"></asp:label></TD>
							    <TD>
                                    <igpck:WebDatePicker runat="server" ID="txtFecDesde" Width="100px" dropdowncalendarID="calendarioPicker" SkinID="Calendario"></igpck:WebDatePicker>
                                    <igpck:WebMonthCalendar runat="server" ID="calendarioPicker"></igpck:WebMonthCalendar>
                                </TD>
							    <TD><asp:button id="cmdCargar" runat="server" Width="78px" CssClass="botonCargar" CausesValidation="False"></asp:button></TD>
							    <TD><asp:imagebutton id="cmdExcel" runat="server" ImageUrl="images/excel.gif"></asp:imagebutton></TD>
						    </TR>
					    </TABLE>
				    </TD>
			    </TR>
			    <TR>
				    <TD class="EstadosFondo1" height="80%" valign="top"><igtbl:ultrawebgrid id="uwgSolicitudes"  runat="server" Width="100%" Height="100%">
						    <DisplayLayout AllowSortingDefault="OnClient" RowHeightDefault="20px" Version="4.00" HeaderClickActionDefault="SortSingle"
							    BorderCollapseDefault="Separate" RowSelectorsDefault="No" Name="uwgSolicitudes" TableLayout="Fixed">
							    <%--<HeaderStyleDefault BorderStyle="Solid" CssClass="VisorHead">
								    <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
							    </HeaderStyleDefault>--%>
							    <FrameStyle Width="100%" BorderWidth="1px" Font-Size="8pt" Font-Names="Verdana" BorderStyle="Solid"
								    CssClass="VisorFrame" TextOverflow="Ellipsis"></FrameStyle>
							    <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
								    <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
							    </FooterStyleDefault>
							    <EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
							    <RowStyleDefault BorderWidth="1px" BorderColor="Gray" BorderStyle="Solid">
								    <Padding Left="3px"></Padding>
								    <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
							    </RowStyleDefault>
						    </DisplayLayout>

						    <Bands >                                
							    <igtbl:UltraGridBand></igtbl:UltraGridBand>
						    </Bands>
					    </igtbl:ultrawebgrid></TD>
			    </TR>
		    </TABLE>
		    <igtblexp:ultrawebgridexcelexporter id="UltraWebGridExcelExporter1" runat="server"></igtblexp:ultrawebgridexcelexporter>

            <asp:Panel ID="pnlMultItems" runat="server" BackColor="White" BorderColor="DimGray" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center" Style="display: none; padding: 5px" Width="350px">
                <div style="display:inline-block; text-align:right;float:right;">
                    <asp:LinkButton runat="server" ID="btnCerrarMultItems" OnClientClick="CerrarPanelMultiplesItems();return false;" style="text-align:right;">
                        <img src="../_common/images/Bt_Cerrar.png" style="border:0px" />
                    </asp:LinkButton>                    
                </div>
                <div style="text-align:center; margin-top:1em; margin-bottom:1em; display:inline-block; height:20em; overflow:scroll;"> 
                    <table id="tblMultiplesItems" runat="server" style="border: 1px solid #E8E8E8; width:100%; table-layout:fixed;border-collapse:separate;" >
                        <thead><tr style="height:20px; background-color:#F0F0F0;"><th><span id="lblTituloMultItems" runat="server" class="captionBlue">DItems</span></th></tr></thead>                                              
                    </table>
                </div>                
            </asp:Panel>
            <asp:Button runat="server" ID="btnHiddenMultItems" style="display:none;"/> 
            <cc1:ModalPopupExtender ID="mpeAdjAnt" runat="server" PopupControlID="pnlMultItems"    
                TargetControlID="btnHiddenMultItems" BackgroundCssClass="modalBackground" CancelControlID="btnCerrarMultItems">
            </cc1:ModalPopupExtender>

            <script id="tmplCampoTexto" type="text/x-jquery-tmpl">
                <tr style="vertical-align:middle">
                    <td style="height:20px; vertical-align:middle; text-align:left; padding-left:0.5em;"><span>${Codigo}</span></td>                    
                </tr>
            </script>

            <script type="text/javascript">
                function MostrarPanelMultiplesItems(idInstancia) {
                    //eliminar l�neas
                    $("#tblMultiplesItems").find('tr:not(:first)').remove();

                    //Cargar �tems      
                    $.when($.ajax({
                        type: "POST",
                        url: rutanormal + 'script/solicitudes/seguimientoSolicitudes.aspx/ObtenerItemsInstancia',
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify({ lCiaComp: lCiaComp, idInstancia: idInstancia, bSoloTexto: false }),
                        dataType: "json",
                        async: false
                    })).done(function (msg) {
                        var oItems = msg.d;

                        for (i = 0; i < oItems.length; i++) {
                            $("#tmplCampoTexto").tmpl(oItems[i]).appendTo("#tblMultiplesItems");                            
                        }
                    })
                                                                       

                    $find('<%=mpeAdjAnt.ClientID%>').show();
                }
                function CerrarPanelMultiplesItems() {                    
                    $find("mpeAdjAnt").hide();                    
                }
            </script>            
	    </form>		
	</body>
</html>
