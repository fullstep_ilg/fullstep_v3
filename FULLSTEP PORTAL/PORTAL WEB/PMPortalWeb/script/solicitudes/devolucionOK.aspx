<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="devolucionOK.aspx.vb" Inherits="Fullstep.PMPortalWeb.devolucionOK" %>
<!DOCTYPE HTML PUBLIC>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../../common/estilo.asp" type="text/css" rel="stylesheet">
		<script src="../../../common/formatos.js"></script>
		<script src="../../../common/menu.asp"></script>
        <script type="text/javascript">
            /*''' <summary>
            ''' Iniciar la pagina.
            ''' </summary>     
            ''' <remarks>Llamada desde: page.onload; Tiempo m�ximo:0</remarks>*/
            function Init() {
                if (accesoExterno == 0) { document.getElementById('tablemenu').style.display = 'block'; }
            }
        </script>
	</head>
	<body onload="Init()">
		<script>dibujaMenu(3)</script>
		<form id="Form1" method="post" runat="server">
			<asp:Label id="lblTitulo" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 48px" runat="server"
				Width="100%" CssClass="tituloDarkBlue"></asp:Label>
			<TABLE id="tblCabecera" style="Z-INDEX: 102; LEFT: 16px; POSITION: absolute; TOP: 78px; HEIGHT: 90px"
				cellSpacing="1" cellPadding="1" width="100%" border="0" runat="server">
				<TR height="40">
					<TD vAlign="top" width="20%" nowrap>
						<asp:Label id="lblId" runat="server" Width="100%" CssClass="captionBlue"></asp:Label>
                        <asp:Label id="lblCodContratoLiteral" runat="server" Width="100%" CssClass="captionBlue"></asp:Label>
                        </TD>
					<TD vAlign="top" width="30%" nowrap>
						<asp:Label id="lblIDBD" runat="server" Width="100%" CssClass="captionDarkBlue"></asp:Label>
                        <asp:Label id="lblCodContrato" runat="server" Width="100%" CssClass="captionDarkBlue"></asp:Label>
                        </TD>
					<TD vAlign="top" width="10%" nowrap>
						<asp:Label id="lblFecha" runat="server" Width="100%" CssClass="captionBlue"></asp:Label></TD>
					<TD vAlign="top" nowrap>
						<asp:Label id="lblFechaBD" runat="server" Width="100%" CssClass="captionDarkBlue"></asp:Label></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 28px">
						<asp:Label id="lblUsuProv" runat="server" Width="100%" CssClass="fntGray"></asp:Label></TD>
					<TD style="HEIGHT: 28px" colSpan="3">
						<asp:Label id="lblUsuProvBD" runat="server" Width="100%" CssClass="fntGray"></asp:Label></TD>
					<TD style="HEIGHT: 28px"></TD>
					<TD style="HEIGHT: 28px"></TD>
				</TR>
				<TR>
					<TD colSpan="4" rowSpan="1">
						<HR width="100%" color="gainsboro" noShade SIZE="4">
						&nbsp;</TD>
					<TD colSpan="3"></TD>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</html>
