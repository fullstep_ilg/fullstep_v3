<%@ Page Language="vb" AutoEventWireup="false" Codebehind="comentestado.aspx.vb" Inherits="Fullstep.PMPortalWeb.comentestado"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../../common/estilo.asp" type="text/css" rel="stylesheet">
		<script src="../../../common/formatos.js"></script>
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblcoment" height="90%" cellSpacing="1" cellPadding="1" width="100%" border="0"
				style="Z-INDEX: 102; LEFT: 12px; POSITION: absolute; TOP: 20px">
				<TR height="98%">
					<TD>
						<asp:TextBox id="txtComent" runat="server" Width="100%" Height="100%" TextMode="MultiLine" ReadOnly="True"></asp:TextBox></TD>
				</TR>
				<TR height="2%">
					<TD align="center"><A id="lblCerrar" onclick="window.close()" href="#" name="lblCerrar" runat="server">cerrar</A></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</html>
