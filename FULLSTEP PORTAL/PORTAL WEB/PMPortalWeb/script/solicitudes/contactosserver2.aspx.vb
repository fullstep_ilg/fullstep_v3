Namespace Fullstep.PMPortalWeb
    Partial Class contactosserver2
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oProveedor As Fullstep.PMPortalServer.Proveedor
            Dim oUser As Fullstep.PMPortalServer.User
            Dim sIdi As String = Idioma
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If

            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSPMServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Proveedores, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            Dim lCiaComp As Long = IdCiaComp

            oUser = Session("FS_Portal_User")

            Me.Label1.Text = oTextos.Rows(12).Item(1)


            oProveedor = FSPMServer.get_Proveedor
            oProveedor.Cod = Request("codProv")
            oProveedor.CargarContactosQA(LCIACOMP)

            If oProveedor.Contactos.Tables.Count > 0 Then
                uwgArticulos.DataSource = oProveedor.Contactos.Tables(0)
                uwgArticulos.DataBind()
            Else
                Exit Sub
            End If

            Dim i As Integer
            With Me.uwgArticulos.Bands(0)
                For i = .Columns.Count - 1 To 0 Step -1
                    Select Case .Columns(i).Key
                        Case "NOMBRE"
                            .Columns(i).Width = Unit.Percentage(60)
                        Case "EMAIL"
                            .Columns(i).Width = Unit.Percentage(40)
                        Case "CALIDAD", "ID"
                            .Columns(i).Hidden = True
                    End Select

                Next

                uwgArticulos.Bands(0).Columns.FromKey("NOMBRE").Header.Caption = oTextos.Rows(13).Item(1)
                uwgArticulos.Bands(0).Columns.FromKey("EMAIL").Header.Caption = oTextos.Rows(14).Item(1)
            End With
            uwgArticulos.DisplayLayout.StationaryMargins = Infragistics.WebUI.UltraWebGrid.StationaryMargins.Header
            oTextos = Nothing
            oDict = Nothing
            oUser = Nothing
        End Sub

    End Class
End Namespace