Namespace Fullstep.PMPortalWeb
    Partial Class participantes
        Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private moTextos As DataTable
        Private msTabContainer As String
        Private msIdi As String
        Private mdsParticipantes As DataSet
        Private mbSoloProveedores As Boolean
        Private mlInstancia As Long

        Property Idi() As String
            Get
                Idi = msIdi
            End Get
            Set(ByVal Value As String)
                msIdi = Value
            End Set
        End Property

        Property TabContainer() As String
            Get
                TabContainer = msTabContainer
            End Get
            Set(ByVal Value As String)
                msTabContainer = Value
            End Set
        End Property

        Property Textos() As DataTable
            Get
                Textos = moTextos
            End Get
            Set(ByVal Value As DataTable)
                moTextos = Value
            End Set
        End Property

        Property dsParticipantes() As DataSet
            Get
                dsParticipantes = mdsParticipantes
            End Get
            Set(ByVal Value As DataSet)
                mdsParticipantes = Value
            End Set
        End Property

        Public Sub New(Optional ByVal lId As Long = Nothing, Optional ByRef oDS As DataSet = Nothing, Optional ByVal sIdi As String = Nothing)
            If lId <> Nothing Then
                msIdi = sIdi
            End If
        End Sub
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Page.IsPostBack Then
                Exit Sub
            End If
            Dim otblRow As System.Web.UI.WebControls.TableRow
            Dim otblCell As System.Web.UI.WebControls.TableCell
            Dim oRowHeader As System.Web.UI.WebControls.TableRow
            Dim oCellHeader As System.Web.UI.WebControls.TableCell
            Dim oLbl As System.Web.UI.HtmlControls.HtmlGenericControl
            Dim oDSRow As System.Data.DataRow
            Dim olabel As System.Web.UI.WebControls.Label
            Dim oFSEntry As Fullstep.DataEntry.GeneralEntry
            Dim sClase As String
            Dim i As Integer
            Dim iFila As Integer
            Dim iCampo As Integer
            Dim sKeyRol As String
            Dim iInicio, iFin As Integer
            Dim lCiaComp As Long = Session("FS_Portal_User").CiaComp

            If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayEntrys1") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayEntrys1", "<script>var arrEntrys_1" + " = new Array()</script>")
            End If

            Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User

            oUser = Session("FS_Portal_User")
            msIdi = oUser.Idioma

            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSPMServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.CertifProveedores, msIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ClienteID", "<script>var vClientIDCertif ='" + Me.ClientID + "'</script>")

            'A�ade las cabeceras 
            oRowHeader = New System.Web.UI.WebControls.TableRow
            Me.tblDesglose.Rows.Add(oRowHeader)

            oCellHeader = New System.Web.UI.WebControls.TableCell
            oCellHeader.ID = "ETA"
            oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
            oLbl.InnerHtml = oTextos.Rows(5).Item(1)
            oCellHeader.Controls.Add(oLbl)
            oCellHeader.Attributes("class") = "captionBlueUnderline"
            oRowHeader.Cells.Add(oCellHeader)

            oCellHeader = New System.Web.UI.WebControls.TableCell
            oCellHeader.ID = "ROL"
            oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
            oLbl.InnerHtml = oTextos.Rows(6).Item(1)
            oCellHeader.Controls.Add(oLbl)
            oCellHeader.Attributes("class") = "captionBlueUnderline"
            oRowHeader.Cells.Add(oCellHeader)

            oCellHeader = New System.Web.UI.WebControls.TableCell
            oCellHeader.ID = "PAR"
            oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
            oLbl.InnerHtml = oTextos.Rows(7).Item(1)
            oCellHeader.Controls.Add(oLbl)
            oCellHeader.Attributes("class") = "captionBlueUnderline"
            oRowHeader.Cells.Add(oCellHeader)

            Dim oHid As New System.Web.UI.HtmlControls.HtmlInputHidden
            oHid.ID = "numRows"
            oHid.Value = mdsParticipantes.Tables(0).Rows.Count
            Me.Controls.Add(oHid)

            'ahora va generando los participantes:
            sClase = "ugfilatabla"
            iInicio = 1
            iFin = 3
            iFila = 0

            For Each oDSRow In mdsParticipantes.Tables(0).Rows
                iFila = iFila + 1
                For iCampo = iInicio To iFin
                    Select Case iCampo
                        Case 1
                            'Nombre del bloque
                            otblRow = New System.Web.UI.WebControls.TableRow
                            otblCell = New System.Web.UI.WebControls.TableCell
                            otblCell.Width = Unit.Percentage(25)
                            olabel = New System.Web.UI.WebControls.Label
                            olabel.Text = "(*)" + oDSRow.Item("BDEN")
                            olabel.Style("font-weight") = "bold"
                            otblCell.Controls.Add(olabel)
                            otblCell.CssClass = sClase
                            otblRow.Cells.Add(otblCell)

                        Case 2
                            'Nombre del rol
                            otblCell = New System.Web.UI.WebControls.TableCell
                            otblCell.Width = Unit.Percentage(25)
                            oFSEntry = New Fullstep.DataEntry.GeneralEntry
                            oFSEntry.Portal = True
                            oFSEntry.Width = Unit.Percentage(100)
                            oFSEntry.ID = "fsentryROL" + oDSRow.Item("ROL").ToString
                            oFSEntry.Tag = "ROL"

                            oFSEntry.IdContenedor = Me.ClientID
                            oFSEntry.Title = Page.Title
                            oFSEntry.WindowTitle = Me.Page.Title
                            oFSEntry.Intro = 0
                            oFSEntry.Idi = msIdi
                            oFSEntry.TabContainer = msTabContainer
                            oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Rol
                            oFSEntry.ReadOnly = 1
                            oFSEntry.Obligatorio = 0
                            oFSEntry.Calculado = 0
                            oFSEntry.Intro = 0
                            oFSEntry.Tag = ""
                            oFSEntry.NumberFormat = oUser.NumberFormat
                            oFSEntry.DateFormat = oUser.DateFormat

                            oFSEntry.Tipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio

                            oFSEntry.Valor = oDSRow.Item("ROL")
                            oFSEntry.Text = oDSRow.Item("RDEN")

                            oFSEntry.LabelStyle.CssClass = "CampoSoloLectura"
                            oFSEntry.InputStyle.CssClass = "trasparent"

                            otblCell.Controls.Add(oFSEntry)

                            otblCell.CssClass = sClase
                            otblRow.Cells.Add(otblCell)

                            otblRow.Height = Unit.Pixel(20)
                            sKeyRol = oFSEntry.ClientID
							tblDesglose.Rows.Add(otblRow)
						Case 3
                            otblCell = New System.Web.UI.WebControls.TableCell
                            otblCell.Width = Unit.Percentage(50)
                            oFSEntry = New Fullstep.DataEntry.GeneralEntry
                            oFSEntry.Portal = True
                            oFSEntry.Width = Unit.Percentage(100)
                            oFSEntry.ID = "fsentry" + "PART" + iFila.ToString
                            oFSEntry.Tag = "PART"
                            oFSEntry.IdContenedor = Me.ClientID
                            oFSEntry.WindowTitle = Me.Page.Title
							oFSEntry.Title = oDSRow.Item("BDEN")
							oFSEntry.Idi = msIdi
                            oFSEntry.TabContainer = msTabContainer
                            If oDSRow.Item("TIPO") = 2 Then
                                oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.ProveContacto
                                oFSEntry.Intro = 0
                            Else
                                oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Persona
                                oFSEntry.Intro = 1

                                Dim oParticipantes As PMPortalServer.Participantes
                                oParticipantes = FSPMServer.Get_Participantes()
                                oParticipantes.Rol = oDSRow.Item("ROL").ToString
                                oParticipantes.Instancia = Request("Instancia")
                                oParticipantes.LoadDataPersonas(lCiaComp)

                                oFSEntry.Lista = oParticipantes.Data
                            End If
                            oFSEntry.ReadOnly = 0
                            oFSEntry.Obligatorio = 1
                            oFSEntry.Calculado = 0

                            oFSEntry.NumberFormat = oUser.NumberFormat
                            oFSEntry.DateFormat = oUser.DateFormat

                            oFSEntry.Tipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio
                            oFSEntry.Tag = ""

                            If oDSRow.Item("TIPO") = 2 Then
                                If Not IsDBNull(oDSRow.Item("PART")) Then
                                    oFSEntry.Valor = oDSRow.Item("PART") + "###" + CStr(oDSRow.Item("ID_CONTACTO"))
                                Else
                                    oFSEntry.Valor = oDSRow.Item("PART")
                                End If
                            Else
                                oFSEntry.Valor = oDSRow.Item("PART")
                            End If
                            oFSEntry.Text = DBNullToSomething(oDSRow.Item("PARTNOM"))


                            If oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.ProveContacto Then 'proveedor
                                oFSEntry.InputStyle.CssClass = "TipoTextoMedio"
                                oFSEntry.MaxLength = 100
                            End If

                            oFSEntry.DependentField = sKeyRol
                            If Page.IsPostBack Then
                                oFSEntry.Valor = Request(oFSEntry.SubmitKey)
                            End If
                            otblCell.Controls.Add(oFSEntry)

                            otblCell.CssClass = sClase
                            otblRow.Cells.Add(otblCell)

                            otblRow.Height = Unit.Pixel(20)

                            Me.tblDesglose.Rows.Add(otblRow)

                    End Select
                Next 'iCampo
            Next 'rows

        End Sub

    End Class
End Namespace