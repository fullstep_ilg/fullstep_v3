Namespace Fullstep.PMPortalWeb
    Partial Class devolucionOK
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim oInstancia As Fullstep.PMPortalServer.Instancia

            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DevolucionOK

            If Not Request("CodContrato") Is Nothing Then
                Me.lblCodContratoLiteral.Text = Textos(5)
                Me.lblCodContrato.Text = Request("CodContrato")
                Me.lblId.Visible = False
                Me.lblIDBD.Visible = False
            Else
                'Datos de la instancia:
                Me.lblIDBD.Text = Request("Instancia")
                Me.lblCodContrato.Visible = False
                Me.lblCodContratoLiteral.Visible = False
            End If

            Me.lblFechaBD.Text = modUtilidades.FormatDate(Now, FSPMUser.DateFormat)
            Me.lblTitulo.Text = Textos(0)
            Me.lblId.Text = Textos(1)
            Me.lblFecha.Text = Textos(2)
            Me.lblUsuProv.Text = Textos(3)
            Me.lblUsuProvBD.Text = Request("Destinatario")

            oInstancia = Nothing

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)
            End If
        End Sub

    End Class
End Namespace