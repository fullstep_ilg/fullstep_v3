<%@ Page Language="vb" AutoEventWireup="false" Codebehind="comentariosSolicitud.aspx.vb" Inherits="Fullstep.PMPortalWeb.comentariosSolicitud"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
		<title>comentariosSolicitud</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../../common/estilo.asp" type="text/css" rel="stylesheet">
		<script src="../../../common/formatos.js"></script>
		<script type="text/javascript">
        //Abre una ventana con el diagrama de flujo de la solicitud
		function mostrarDiagramaFlujo()
		{
		    var IdInstancia = document.forms["Form1"].elements["txtInstancia"].value
		    var CodContrato = document.forms["Form1"].elements["hidCodContrato"].value
		    var NumFactura = document.forms["Form1"].elements["hidNumFactura"].value
		    window.open("flowDiagramLaunch.aspx?Instancia=" + IdInstancia + "&Codigo=" + CodContrato + "&NumFactura=" + NumFactura, "_blank", "width=700,height=400,status=yes,resizable=yes,top=200,left=200")
		}

        //Muestra el comentario completo introducido en ese cambio de etapa/traslado/rechazo,....
		function VerComentario(id) {
		    window.open("comentestado.aspx?ID=" + id, "_blank", "width=700,height=400,status=yes,resizable=no,top=200,left=200")
		}
    </script>
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
            <asp:ScriptManager ID="sm_coments" runat="server"></asp:ScriptManager>
			<TABLE id="tblComent" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" height="100%"
				cellSpacing="1" cellPadding="1" width="100%" border="0">
				<TR height="10%">
					<TD>
						<TABLE class="cabeceraSolicitud" id="tblCabecera" style="HEIGHT: 75px" cellSpacing="1"
							cellPadding="1" width="100%" border="0">
							<TR>
								<TD style="WIDTH: 15%"><asp:label id="lblId" runat="server" CssClass="captionBlue" Width="100%"></asp:label></TD>
								<TD style="WIDTH: 352px"><asp:label id="lblIdBD" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:label></TD>
								<TD style="WIDTH: 15%"><asp:label id="lblFecha" runat="server" CssClass="captionBlue" Width="100%">lblFecha</asp:label></TD>
								<TD style="WIDTH: 352px"><asp:label id="lblFechaBD" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 15%"><asp:label id="lblNombre" runat="server" CssClass="captionBlue" Width="100%"></asp:label></TD>
								<TD style="WIDTH: 352px"><asp:label id="lblNombreBD" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:label></TD>
								<TD style="WIDTH: 15%"></TD>
								<TD style="WIDTH: 352px"></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 10%"><asp:label id="lblPeticionario" runat="server" CssClass="captionBlue" Width="100%">lblPeticionario</asp:label></TD>
								<TD style="WIDTH: 352px"><asp:label id="lblPeticionarioBD" runat="server" CssClass="captionDarkBlue" Width="100%">Label</asp:label></TD>
								<TD style="WIDTH: 10%"></TD>
								<TD style="WIDTH: 352px"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR height="5%">
					<TD align="right"><A href="javascript:mostrarDiagramaFlujo()"><asp:label id="lblFlujo" runat="server" CssClass="flujo" Width="50%">lblFlujo</asp:label></A></TD>
				</TR>
				<TR height="90%">
					<TD>
                        <ig:WebHierarchicalDataGrid id="whgHistorico" runat="server" AutoGenerateColumns="false" ShowHeader="true" Width="100%" Height="100%">
                            <Columns>
                                <ig:BoundDataField DataFieldName="DEN_BLOQUE1" Key="DEN_BLOQUE1" Hidden="false"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="ROL" Key="ROL" Hidden="false"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="USUARIO" Key="USUARIO" Hidden="false"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="ACCION" Key="ACCION" Hidden="false"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="FECHA" Key="FECHA" Hidden="false"></ig:BoundDataField>
                                <ig:UnboundField Key="DEN_DESTINO" Hidden="false"></ig:UnboundField>
                                <ig:BoundDataField DataFieldName="COMENT" Key="COMENT" Hidden="false"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="BLOQUE1" Key="BLOQUE1" Hidden="true"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="BLOQUE2" Key="BLOQUE2" Hidden="true"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="PER" Key="PER" Hidden="true"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="PROVE" Key="PROVE" Hidden="true"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="ESTADO" Key="ESTADO" Hidden="true"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="DEN_BLOQUE2" Key="DEN_BLOQUE2" Hidden="true"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="ID_OTRA_ACCION" Key="ID_OTRA_ACCION" Hidden="true"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="DESTINATARIO" Key="DESTINATARIO" Hidden="true"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="ETAPAS_PARALELO" Key="ETAPAS_PARALELO" Hidden="true"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="ID" Key="ID" Hidden="true"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="CONSULTA" Key="CONSULTA" Hidden="true"></ig:BoundDataField> 
                            </Columns>
                            <Behaviors>
				                <ig:Filtering Alignment="Top" Enabled="true" Visibility="Visible"></ig:Filtering>
				                <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>
				                <ig:ColumnMoving Enabled="true"></ig:ColumnMoving>
			                </Behaviors>
			                <GroupingSettings EnableColumnGrouping="True"></GroupingSettings>
						</ig:WebHierarchicalDataGrid>
                     </TD>
				</TR>
				<tr>
					<td>
					</td>
				</tr>
				<TR height="3%">
					<TD align="center"><A id="lblCerrar" onclick="window.close()" href="#" name="lblCerrar" runat="server">cerrar</A></TD>
				</TR>
			</TABLE>
			<INPUT id="txtInstancia" type="hidden" name="txtInstancia" runat="server">
            <input id="hidCodContrato" type="hidden" name="hidCodContrato" runat="server" />
            <input id="hidNumFactura" type="hidden" name="hidNumFactura" runat="server" />					
		</form>
	</body>
</html>
