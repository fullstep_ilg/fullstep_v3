﻿wProgreso = null;

$(document).ready(function () {
    if (accesoExterno == 0) { document.getElementById('tablemenu').style.display = 'block'; }
    resize();
});

function initTab(webTab) {
    var cp = document.getElementById(webTab.ID + '_cp');
    cp.style.minHeight = '300px';
    cp = null;
};
function resize() {
    for (i = 0; i < arrDesgloses.length; i++) {
        sDiv = arrDesgloses[i].replace("tblDesglose", "divDesglose");
        if (sDiv) document.getElementById(sDiv).style.width = (parseFloat(document.body.offsetWidth) - 85) + "px";
    };
};
//Una vez guardada la solicitud se muestra el id que se le ha asignado
function PonerIdSolicitud(instancia) {
    if (document.forms["frmAlta"].elements["NEW_Instancia"].value == "") {
        if (document.getElementById("lblCabecera"))
            document.getElementById("lblCabecera").innerHTML = instancia + ' - ' + document.getElementById("lblCabecera").innerHTML;
    };
    document.getElementById("NEW_Instancia").value = instancia;

    HabilitarBotones();
    wProgreso = null;
};
function HistoryHaciaAtras() {
    if (tipoSolicitud == 13) {
        window.location = "altaSolicitudes.aspx?TipoSolicitud=13";
    } else {
        window.location = "altaSolicitudes.aspx";
    }
}
//Muestra el contenido peligroso que ha efectuado un error al guardar.
function ErrorValidacion(contenido) {
    alert(arrTextosML[2].replace("$$$", contenido));
    HabilitarBotones();
};
//Función que monta el formulario calculado y luego efectúa el submit. Llamada desde evento onClick del botón cmdCalcular.
function CalcularCamposCalculados() {
    var oFrm = MontarFormularioCalculados()
    oFrm.submit()
    oFrm = null;
    return false;
};

// Comprueba las cantidades si hay vinculación y llama a MontarSubmitGuardar (fn guardado solicitud)
function Guardar() {
    //si existe campo moneda, aunque la comprobación no sea obligatoria, debemos comprobarlo
    //o no se creará la instancia - Tarea 2918
    var oEntryMoneda = buscarCampoEnFormulario(102);
    if (oEntryMoneda) {
        if (!oEntryMoneda.getDataValue()) {
            alert(arrTextosML[0] + '\n' + oEntryMoneda.title.toString());
            return false;
        }
    }

    var frmAltaElements = document.forms["frmAlta"].elements;
    if (frmAltaElements["PantallaVinculaciones"].value == "True") {
        var instancia = document.getElementById("NEW_Instancia").value;
        var solicitud = 1;
        if (instancia > 0)
            solicitud = 0;
        if (Validar_Cantidades_Vinculadas(solicitud, instancia) == false) {
            return false
        }
        instancia = null;
        solicitud = null;
    }

    frmAltaElements = null;

    if (wProgreso == null)
        wProgreso = window.open("../_common/espera.aspx?cadena=" + document.forms["frmAlta"].elements["cadenaespera"].value, "_blank", "top=300,left=450,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")

    setTimeout("MontarSubmitGuardar()", 100)
    return false;
};
function MontarSubmitGuardar() {
    MontarFormularioSubmit(true, true, false);

    var frmSubmitElements = document.forms["frmSubmit"].elements;
    frmSubmitElements["GEN_AccionRol"].value = 0;
    frmSubmitElements["GEN_Bloque"].value = document.forms["frmAlta"].elements["Bloque"].value;
    frmSubmitElements["GEN_Enviar"].value = 0;
    frmSubmitElements["GEN_Accion"].value = "guardarsolicitud";
    frmSubmitElements["GEN_Rol"].value = oRol_Id;
    frmSubmitElements["PantallaMaper"].value = false;
    frmSubmitElements["DeDonde"].value = "Guardar";

    var oFrm = MontarFormularioCalculados();
    var sInner = oFrm.innerHTML;
    oFrm.innerHTML = "";
    document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner);
    frmSubmitElements = null;
    oFrm = null;
    sInner = null;
    document.forms["frmSubmit"].submit();
};

// <summary>Monta el formulario para el posterior cambio de etapa. (guardarInstancia.aspx)</summary>
// <param name="id">Id Accion</param>
// <param name="bloque">Id Bloque</param>        
// <param name="comp_obl">Si es necesario rellenar los campos obligatorios</param>
// <param name="guarda">si hay que guardar o versión o no</param> 
// <param name="HayMapper">Si es necesario hacer comprobaciones mapper en guardarInstancia.aspx</param>
// <remarks>Llamada desde: javascript EjecutarAccion2</remarks>
function MontarSubmitAccion(id, bloque, comp_obl, guarda, HayMapper) {
    MontarFormularioSubmit(guarda, true, HayMapper)

    var frmSubmitElements = document.forms["frmSubmit"].elements;
    frmSubmitElements["GEN_AccionRol"].value = id;
    frmSubmitElements["GEN_Bloque"].value = bloque;
    frmSubmitElements["GEN_Rol"].value = oRol_Id;
    frmSubmitElements["PantallaMaper"].value = HayMapper;
    frmSubmitElements["DeDonde"].value = "Guardar";

    var oFrm = MontarFormularioCalculados()

    var sInner = oFrm.innerHTML
    oFrm.innerHTML = ""
    document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
    frmSubmitElements = null;
    oFrm = null;
    sInner = null;
    document.forms["frmSubmit"].submit()
};
// <summary>Cuando se pulsa un botón para realizar una acción, se llama a esta función
//  Comprueba los campos obligatorios, si hay participantes o no, y llama a montarformulario</summary>
// <param name="id">Id Accion</param>
// <param name="bloque">Id Bloque</param>        
// <param name="comp_obl">Si hay que comprobar los campos obligatorios</param>
// <param name="guarda">Si hay que guardar o versión o no</param> 
// <param name="rechazo">Indica si la accion implica un rechazo, ya sea temporal o definitivo</param>
function EjecutarAccion(id, bloque, comp_obl, guarda, rechazo) {
	var respOblig;
	if (comp_obl == true || comp_obl == "true") {
        var bMensajePorMostrar = document.getElementById("bMensajePorMostrar")
        if (bMensajePorMostrar)
            if (bMensajePorMostrar.value == "1") {
                bMensajePorMostrar.value = "0";
                return false;
            }
        bMensajePorMostrar = null;
        respOblig = comprobarObligatorios();
        switch (respOblig) {
            case "":  //no falta ningun campo obligatorio
                break;
            case "filas0": //no se han introducido filas en un desglose obligatorio
            	alert(arrTextosML[0]);
                return false;
                break;
            default: //falta algun campo obligatorio
            	alert(arrTextosML[0] + '\n' + respOblig);
            	return false;
                break;
        }
    } else {
        //si existe campo moneda, aunque la comprobación no sea obligatoria, debemos comprobarlo
        //o no se creará la instancia - Tarea 2918
        var oEntryMoneda = buscarCampoEnFormulario(102);
        if (oEntryMoneda) {
            if (!oEntryMoneda.getDataValue()) {
                alert(arrTextosML[0] + '\n' + oEntryMoneda.title.toString());
                return false;
            }
        }
    }
    
	if (rechazo == false || rechazo == "false") {
		respOblig = comprobarParticipantes();
        if (respOblig !== '') {
        	alert(arrTextosML[0] + '\n' + respOblig);
            return false;
        }
    }

    var frmAltaElements = document.forms["frmAlta"].elements;
    if (frmAltaElements["PantallaVinculaciones"].value == "True") {
        var instancia = document.getElementById("NEW_Instancia").value;
        var solicitud = 1;
        if (instancia > 0)
            solicitud = 0;
        if (Validar_Cantidades_Vinculadas(solicitud, instancia) == false) {
            return false
        }
        instancia = null;
        solicitud = null;
    }

    if (wProgreso == null)
        wProgreso = window.open("../_common/espera.aspx?cadena=" + encodeURI(document.forms["frmAlta"].elements["cadenaespera"].value), "_blank", "top=300,left=450,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")

    var HayMapper = false
    if (frmAltaElements["PantallaMaper"].value == "True")
        HayMapper = true

    setTimeout("MontarSubmitAccion(" + id + "," + bloque + "," + comp_obl + "," + guarda + "," + HayMapper + ")", 100)

    frmAltaElements = null;
    HayMapper = null;
};

//Muestra el div que indica que se está cargando la solicitud
function MostrarEspera() {
    wProgreso = true;

    $("[id*='lnkBoton']").attr('disabled', 'disabled');

    document.getElementById("lblCamposObligatorios").style.display = "none"
    document.getElementById("divForm2").style.display = 'none';
    document.getElementById("divForm3").style.display = 'none';
    document.getElementById("igtabuwtGrupos").style.display = 'none';

    var i = 0;
    var bSalir = false;
    while (bSalir == false) {
        if (document.getElementById("uwtGrupos_div" + i)) {
            document.getElementById("uwtGrupos_div" + i).style.visibility = 'hidden';
            i = i + 1;
        }
        else {
            bSalir = true;
        }
    }
    document.getElementById("lblProgreso").value = document.forms["frmAlta"].elements["cadenaespera"].value;
    document.getElementById("divProgreso").style.display = 'inline';
};
//Estaba habilitando los botones antes de ocultar el progreso. Parecia q te dejaba dar a varios botones mientras estaba en progreso.
function DarTiempoAOcultarProgreso() {
    $("[id*='lnkBoton']").removeAttr('disabled');
}
//Oculta el div que indica que se está cargando la solicitud
function OcultarEspera() {
    if (wProgreso != null) wProgreso.close();
    wProgreso = null;

    document.getElementById("divProgreso").style.display = 'none';
    document.getElementById("divForm2").style.display = 'block';
    document.getElementById("divForm3").style.display = 'block';
    document.getElementById("igtabuwtGrupos").style.display = '';

    setTimeout('DarTiempoAOcultarProgreso()', 250);

    var i = 0;
    var bSalir = false;
    while (bSalir == false) {
        if (document.getElementById("uwtGrupos_div" + i)) {
            document.getElementById("uwtGrupos_div" + i).style.visibility = 'visible';
            i = i + 1;
        }
        else {
            bSalir = true;
        }
    }
};
//Habilita los botones disponibles en la solicitud y llama a ocultar espera	
function HabilitarBotones() {
    if (arrObligatorios.length > 0)
        if (document.getElementById("lblCamposObligatorios"))
            document.getElementById("lblCamposObligatorios").style.display = "";
    OcultarEspera();
};
//Evitar los enter q hacen q el 1er botón de FSPMPageHeader se ejecute. No se puede traducir en Tab en FF/Chrome. Pero si evitar el submit y q haga lo q debe.
function FFreplaceEnterKeyWithTab(event) {
    if (!window.event)//FF
        if ((event.target.type != 'textarea') && (event.which == 13))//13 is enter key
            event.preventDefault();
};
//Evitar los enter q hacen q el 1er botón de FSPMPageHeader se ejecute. Se traduce en Tab.
function replaceEnterKeyWithTab() {
    if (window.event)//IE
        if ((window.event.srcElement.type != 'textarea') && (window.event.keyCode == 13))//13 is enter key	            
            window.event.keyCode = 9; //return 9 the tab key 
};
/*
''' <summary>
''' Comprueba si se han rellenado los campos de participantes
'''	''' </summary>
''' <returns>true o false dependiendo si estan rellenos</returns>
''' <remarks>Llamada desde:NWAlta,NwDetalleSolicitud,Nwgestioninstancia ; Tiempo máximo:0 </remarks>
*/
function comprobarParticipantes() {
    if (typeof (arrObligatorios) != "undefined") {
        for (var i = 0; i < arrObligatorios.length; i++) {
            if (arrObligatorios[i] != "deleted") {
                oEntry = fsGeneralEntry_getById(arrObligatorios[i])
                if (oEntry) {
                    if (oEntry.tipoGS == 115 || oEntry.tipoGS == 116) {
                        v = oEntry.getDataValue();
                        if (v == null || v == "") return oEntry.title;
                    }
                }
            }
        }
    }
    return '';
};