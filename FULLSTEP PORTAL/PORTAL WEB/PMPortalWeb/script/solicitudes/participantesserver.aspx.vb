Namespace Fullstep.PMPortalWeb
    Partial Class participantesserver
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim i As Short
            Dim lRol As Integer
            Dim lInstancia As Integer
            Dim oParticipantes As Fullstep.PMPortalServer.Participantes
            Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & _
        "<script language=""{0}"">{1}</script>"

            Dim oGrid As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
            oGrid = Me.uwgArticulos
            Dim sTabContainer = Request("TabContainer")
            Dim lIndex As Long = Request("Index")
            Dim sIdEntry As String = Request("ClientId")
            Dim lCiaComp As Long = IdCiaComp

            sIdEntry = Replace(sIdEntry, "__t", "")
            sIdEntry = Replace(sIdEntry, "__", "_")

            If sTabContainer <> Nothing Then
                sTabContainer = Left(sTabContainer, Len(sTabContainer) - 1)
            End If
            If Request("Rol") <> Nothing Then
                lRol = Request("Rol")
            End If
            If Request("Instancia") <> Nothing Then
                lInstancia = Request("Instancia")
            End If

            oParticipantes = FSPMServer.Get_Participantes

            oParticipantes.Rol = lRol
            oParticipantes.Instancia = lInstancia
            oParticipantes.LoadDataPersonas(lciacomp)

            oGrid.ID = sIdEntry + "_dd"
            oGrid.DataSource = oParticipantes.Data
            oGrid.DataBind()
            If oGrid.Bands.Count = 0 Then
                oGrid.Bands.Add(New Infragistics.WebUI.UltraWebGrid.UltraGridBand)
            End If

            oGrid.DisplayLayout.ClientSideEvents.InitializeLayoutHandler = "preInitGrid('" + sTabContainer + "', 0,2,'" + Request("ClientId") + "',2);ddinitGridEvent"
            oGrid.DisplayLayout.ClientSideEvents.CellClickHandler = "ddcellClickEvent"
            oGrid.Height = Unit.Pixel(200)
            oGrid.CssClass = "igDropDown"
            oGrid.DisplayLayout.Bands(0).CellClickAction = Infragistics.WebUI.UltraWebGrid.CellClickAction.RowSelect
            oGrid.Bands(0).RowSelectors = Infragistics.WebUI.UltraWebGrid.RowSelectors.No
            oGrid.Bands(0).RowStyle.CssClass = "igDropDownStyle"

            oGrid.Bands(0).Columns.FromKey("PARTCOD").Hidden = True
            oGrid.Bands(0).Columns.FromKey("NOM_DEP").Hidden = True
            oGrid.Bands(0).Columns.FromKey("PARTNOM").Width = Unit.Pixel(150)

            Dim sScript As String
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", "var sNombreGrid='" + Replace(Me.uwgArticulos.ClientID, "_", "x") + "'")
            Page.ClientScript.RegisterStartupScript(Me.GetType(),"ArticulosLlamadaKey", sScript)
        End Sub

    End Class
End Namespace