﻿Imports Fullstep.PMPortalServer

Namespace Fullstep.PMPortalWeb
    Public Class altaSolicitud
        Inherits FSPMPage

#Region "Properties"
        Private _oSolicitud As PMPortalServer.Solicitud
        Private _rolActual As PMPortalServer.Rol
        Protected ReadOnly Property oSolicitud() As PMPortalServer.Solicitud
            Get
                If _oSolicitud Is Nothing Then
                    If IsPostBack Then
                        _oSolicitud = CType(Cache("oSolicitud" & FSPMUser.Cod), PMPortalServer.Solicitud)
                    Else
                        _oSolicitud = FSPMServer.Get_Solicitud()
                        _oSolicitud.ID = Request("Solicitud")

                        'Carga los datos de la solicitud:
                        _oSolicitud.Load(IdCiaComp, Idioma, True)
                        _rolActual = FSPMServer.Get_Rol()
                        _rolActual.CargarRolActual(IdCiaComp, _oSolicitud.BloqueActual, FSPMUser.CodProveGS, FSPMUser.IdContacto)
                        _oSolicitud.Formulario.Load(IdCiaComp, Idioma, _oSolicitud.ID, _oSolicitud.BloqueActual, idRol:=_rolActual.Id)

                        InsertarEnCache("oSolicitud" & FSPMUser.Cod, _oSolicitud)
                    End If
                End If
                Return _oSolicitud
            End Get
        End Property

#End Region
        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Dim oucParticipantes As participantes
            Dim oInputHidden As HtmlInputHidden
            Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab

            If Page.IsPostBack Then
                CargarCamposGrupo()
                Exit Sub
            End If

            CargarTextoEspera()

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.AltaSolicitud
            ConfigurarCabeceraMenu()
            RegisterScripts()

            lblCamposObligatorios.Text = Textos(6)
            Solicitud.Value = oSolicitud.ID
            Bloque.Value = oSolicitud.BloqueActual

            _rolActual.CargarParticipantes(IdCiaComp, Idioma)
            CargarCamposGrupo()
            If _rolActual.Participantes.Tables(0).Rows.Count > 0 Then
                oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                oTabItem.Text = Textos(7) 'Participantes
                oTabItem.Key = "PARTICIPANTES"

                uwtGrupos.Tabs.Add(oTabItem)

                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                oTabItem.ContentPane.UserControlUrl = "participantes.ascx"

                oucParticipantes = oTabItem.ContentPane.UserControl
                oucParticipantes.Idi = Idioma
                oucParticipantes.TabContainer = uwtGrupos.ClientID
                oucParticipantes.dsParticipantes = _rolActual.Participantes

                oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden
                oInputHidden.ID = "txtPre_PARTICIPANTES"
                oInputHidden.Value = oucParticipantes.ClientID 'oucParticipantes.ClientID
                Me.FindControl("frmAlta").Controls.Add(oInputHidden)
            End If
            Dim oValidacionesIntegracion As ValidacionesIntegracion = FSPMServer.Get_ValidacionesIntegracion
            PantallaMaper.Value = oValidacionesIntegracion.HayIntegracionSalidaoEntradaSalida({TablasIntegracion.PM}, IdCiaComp)
            FindControl("frmAlta").Controls.Add(Fullstep.PMPortalWeb.CommonInstancia.InsertarCalendario(Idioma))
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutanormal") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutanormal", "var rutanormal ='" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "';", True)
            End If
            If Not Page.ClientScript.IsClientScriptBlockRegistered("varFila") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFila", "<script>var sFila = '" & JSText(Textos(11)) & "';</script>")
        End Sub

        Private Sub RegisterScripts()
            Dim sClientTextVars As String
            Dim sClientTexts As String

            If Not ClientScript.IsStartupScriptRegistered("tipoSolicitud") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tipoSolicitud", "<script>var tipoSolicitud = '" & CInt(oSolicitud.TipoSolicit) & "';</script>")
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "ruta") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ruta", "var ruta='" & ConfigurationManager.AppSettings("ruta") & "';", True)
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)
            If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
            If Not ClientScript.IsStartupScriptRegistered("oRol_Id") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "oRol_Id", "var oRol_Id=" & _rolActual.Id & ";", True)

            sClientTextVars = ""
            sClientTextVars += "vdecimalfmt='" + FSPMUser.DecimalFmt + "';"
            sClientTextVars += "vthousanfmt='" + FSPMUser.ThousanFmt + "';"
            sClientTextVars += "vprecisionfmt='" + FSPMUser.PrecisionFmt + "';"
            sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
            If Not ClientScript.IsStartupScriptRegistered("VarsUser") Then _
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

            sClientTexts = ""
            sClientTexts += "var arrTextosML = new Array();"
            sClientTexts += "arrTextosML[0] = '" + JSText(Textos(8)) + "';"
            sClientTexts += "arrTextosML[1] = '" + JSText(Textos(9)) + "';"
            sClientTexts += "arrTextosML[2] = '" + JSText(Textos(10)) + "';"
            sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
            If Not Page.ClientScript.IsStartupScriptRegistered("TextosMICliente") Then _
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

            Dim sScript As String
            Dim ilGMN1 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
            Dim ilGMN2 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
            Dim ilGMN3 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
            Dim ilGMN4 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN4
            sScript = ""
            sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
            sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
            sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
            sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)
        End Sub
        Private Sub CargarTextoEspera()
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.Espera
            cadenaespera.Value = Textos(1)
            lblProgreso.Text = Textos(2)
        End Sub
        Private Sub ConfigurarCabeceraMenu()
            Dim tituloCab As String = String.Empty

            If (oSolicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.Factura) Then
                tituloCab = Textos(1)
            Else
                tituloCab = Textos(0)
            End If
            Me.instanciaPortF.Value = oSolicitud.ID * (-1)
            FSNPageHeader.TituloCabecera = tituloCab & "/" & oSolicitud.Den(Idioma)
            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/altaSolicitud.jpg"

            FSNPageHeader.TextoBotonVolver = Textos(2)
            FSNPageHeader.VisibleBotonVolver = True
            FSNPageHeader.OnClientClickVolver = "javascript:HistoryHaciaAtras();return false;"

            FSNPageHeader.TextoBotonGuardar = Textos(3)
            FSNPageHeader.VisibleBotonGuardar = True
            FSNPageHeader.OnClientClickGuardar = "Guardar(); return false;"

            FSNPageHeader.TextoBotonCalcular = Textos(4)
            FSNPageHeader.VisibleBotonCalcular = False
            FSNPageHeader.OnClientClickCalcular = "return CalcularCamposCalculados(); return false;"
            FSNPageHeader.VisibleBotonCalcular = oSolicitud.Formulario.Grupos.Grupos.OfType(Of PMPortalServer.Grupo).Where(Function(x) x.DSCampos.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) y("TIPO") = TipoCampoPredefinido.Calculado).Any).Any()
            If FSNPageHeader.VisibleBotonCalcular Then BotonCalcular.Value = 1

            Dim oDSAcciones As DataSet = _rolActual.CargarAcciones(IdCiaComp, Idioma)
            Dim oPopMenu As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
            Dim oItem As Infragistics.WebUI.UltraWebNavigator.Item
            Dim oRow As DataRow

            oPopMenu = Me.uwPopUpAcciones
            oPopMenu.Visible = False
            If oDSAcciones.Tables.Count > 0 Then
                If oDSAcciones.Tables(0).Rows.Count > 3 Then
                    oPopMenu.Visible = True
                    FSNPageHeader.VisibleBotonAccion1 = True
                    FSNPageHeader.TextoBotonAccion1 = Textos(5) ' "Otras acciones"
                    FSNPageHeader.OnClientClickAccion1 = "igmenu_showMenu('" & uwPopUpAcciones.ID & "', event); return false;"

                    For Each oRow In oDSAcciones.Tables(0).Rows
                        If IsDBNull(oRow.Item("DEN")) Then
                            oItem = oPopMenu.Items.Add("&nbsp;")
                        Else
                            If oRow.Item("DEN") = "" Then
                                oItem = oPopMenu.Items.Add("&nbsp;")
                            Else
                                oItem = oPopMenu.Items.Add(DBNullToStr(oRow.Item("DEN")))
                            End If
                        End If
                        oItem.TargetUrl = "javascript:EjecutarAccion(" + oRow.Item("ACCION").ToString() + "," + _oSolicitud.BloqueActual.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "');"
                    Next
                Else
                    Dim cont As Byte = 0
                    For Each oRow In oDSAcciones.Tables(0).Rows
                        If cont = 0 Then
                            FSNPageHeader.VisibleBotonAccion1 = True
                            FSNPageHeader.TextoBotonAccion1 = DBNullToStr(oRow.Item("DEN"))
                            FSNPageHeader.OnClientClickAccion1 = "javascript:EjecutarAccion(" + oRow.Item("ACCION").ToString() + "," + _oSolicitud.BloqueActual.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                            cont = 1
                        ElseIf cont = 1 Then
                            FSNPageHeader.VisibleBotonAccion2 = True
                            FSNPageHeader.TextoBotonAccion2 = DBNullToStr(oRow.Item("DEN"))
                            FSNPageHeader.OnClientClickAccion2 = "javascript:EjecutarAccion(" + oRow.Item("ACCION").ToString() + "," + _oSolicitud.BloqueActual.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                            cont = 2
                        Else
                            FSNPageHeader.VisibleBotonAccion3 = True
                            FSNPageHeader.TextoBotonAccion3 = DBNullToStr(oRow.Item("DEN"))
                            FSNPageHeader.OnClientClickAccion3 = "javascript:EjecutarAccion(" + oRow.Item("ACCION").ToString() + "," + _oSolicitud.BloqueActual.ToString + ",'" + (IIf(oRow.Item("CUMP_OBL_ROL") = 1, "true", "false")) + "','" + (IIf(oRow.Item("GUARDA") = 1, "true", "false")) + "','" + (IIf(DBNullToSomething(oRow.Item("TIPO_RECHAZO")) > 0, "true", "false")) + "'); return false;"
                        End If
                    Next
                End If
            End If
        End Sub
        Private Sub CargarCamposGrupo()
            Dim oGrupo As PMPortalServer.Grupo
            Dim oucCampos As campos
            Dim oucDesglose As desgloseControl
            Dim oRow As DataRow = Nothing
            Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden
            Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab

            uwtGrupos.Tabs.Clear()
            Dim lIndex As Long = 0
            AplicarEstilosTab(uwtGrupos)

            For Each oGrupo In oSolicitud.Formulario.Grupos.Grupos
                oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(Idioma), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                oTabItem.Key = lIndex
                uwtGrupos.Tabs.Add(oTabItem)

                oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

                oInputHidden.ID = "txtPre_" + lIndex.ToString
                lIndex += 1

                If oGrupo.NumCampos <= 1 Then
                    For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                        If oRow.Item("VISIBLE") = 1 Then
                            Exit For
                        End If
                    Next
                    If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Desglose Or (DBNullToSomething(oRow.Item("SUBTIPO")) = TiposDeDatos.TipoGeneral.TipoDesglose And DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) <> TiposDeDatos.TipoCampoGS.Desglose) Then 'El segundo es DesgloseActividad
                        If oRow.Item("VISIBLE") = 0 Then
                            uwtGrupos.Tabs.Remove(oTabItem)
                        Else
                            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                            oTabItem.ContentPane.UserControlUrl = "../_common/desglose.ascx"
                            oucDesglose = oTabItem.ContentPane.UserControl
                            oucDesglose.ID = oGrupo.Id.ToString
                            oucDesglose.Campo = oRow.Item("ID")
                            oucDesglose.TabContainer = uwtGrupos.ClientID
                            oucDesglose.SoloLectura = (oRow.Item("ESCRITURA") = 0)
                            oucDesglose.Solicitud = oSolicitud.ID
                            oucDesglose.TieneIdCampo = False
                            oucDesglose.Ayuda = DBNullToSomething(oRow.Item("AYUDA_" & Idioma))
                            oucDesglose.PM = True
                            oucDesglose.Titulo = DBNullToSomething(oRow.Item("DEN_" & Idioma))
                            oucDesglose.TipoSolicitud = oSolicitud.TipoSolicit
                            oInputHidden.Value = oucDesglose.ClientID
                        End If
                    Else
                        oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                        oTabItem.ContentPane.UserControlUrl = "../_common/campos.ascx"
                        oucCampos = oTabItem.ContentPane.UserControl
                        oucCampos.ID = oGrupo.Id.ToString
                        oucCampos.dsCampos = oGrupo.DSCampos
                        oucCampos.IdGrupo = oGrupo.Id
                        oucCampos.Idi = Idioma
                        oucCampos.TabContainer = uwtGrupos.ClientID
                        oucCampos.Solicitud = oSolicitud.ID
                        oucCampos.TipoSolicitud = oSolicitud.TipoSolicit
                        oucCampos.PM = True
                        oucCampos.Formulario = oSolicitud.Formulario
                        oInputHidden.Value = oucCampos.ClientID
                    End If
                Else
                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                    oTabItem.ContentPane.UserControlUrl = "../_common/campos.ascx"
                    oucCampos = oTabItem.ContentPane.UserControl
                    oucCampos.ID = oGrupo.Id.ToString
                    oucCampos.dsCampos = oGrupo.DSCampos
                    oucCampos.IdGrupo = oGrupo.Id
                    oucCampos.Idi = Idioma
                    oucCampos.TabContainer = uwtGrupos.ClientID
                    oucCampos.Solicitud = oSolicitud.ID
                    oucCampos.TipoSolicitud = oSolicitud.TipoSolicit
                    oucCampos.PM = True
                    oucCampos.Formulario = oSolicitud.Formulario
                    oInputHidden.Value = oucCampos.ClientID
                End If

                Me.FindControl("frmAlta").Controls.Add(oInputHidden)
            Next
        End Sub

    End Class
End Namespace