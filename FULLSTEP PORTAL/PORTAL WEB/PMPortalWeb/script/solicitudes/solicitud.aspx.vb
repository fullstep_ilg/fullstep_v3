
Namespace Fullstep.PMPortalWeb
    Partial Class solicitud
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here


            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oSolicitud As Fullstep.PMPortalServer.Solicitud
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
            Dim lCiaComp As Long = IdCiaComp


            Dim sIdi As String = oUser.Idioma
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If

            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSWSServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Solicitud, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)
            Me.lblDescripcion.Text = oTextos.Rows(0).Item(1)
            Me.lblArchivos.Text = oTextos.Rows(1).Item(1)

            oSolicitud = FSWSServer.Get_Solicitud
            oSolicitud.ID = Request("Solicitud")

            oSolicitud.Load(lCiaComp, sIdi)
            Me.lblTitulo.Text = oSolicitud.Den(sIdi)
            Me.txtDescripcion.Text = oSolicitud.Descr(sIdi)
            Me.uwgEspecs.DataSource = oSolicitud.dsAdjuntos
            Me.uwgEspecs.DataBind()
            Me.uwgEspecs.Bands(0).Columns.FromKey("ID").Hidden = True
            Me.uwgEspecs.Bands(0).Columns.FromKey("PER").Hidden = True
            Me.uwgEspecs.Bands(0).Columns.FromKey("IDIOMA").Hidden = True
            Me.uwgEspecs.Bands(0).Columns.FromKey("DESCARGAR").Move(11)

            Me.uwgEspecs.Bands(0).Columns.FromKey("FECALTA").Hidden = True
            Me.uwgEspecs.Bands(0).Columns.FromKey("NOMBRE").Hidden = True

            Me.uwgEspecs.Bands(0).Columns.FromKey("FECALTA").Width = Unit.Percentage(10)
            Me.uwgEspecs.Bands(0).Columns.FromKey("NOMBRE").Width = Unit.Percentage(25)
            Me.uwgEspecs.Bands(0).Columns.FromKey("NOM").Width = Unit.Percentage(25)
            Me.uwgEspecs.Bands(0).Columns.FromKey("DATASIZE").Width = Unit.Percentage(5)

            Me.uwgEspecs.Bands(0).Columns.FromKey("COMENT").Width = Unit.Percentage(30)
            Me.uwgEspecs.Bands(0).Columns.FromKey("DESCARGAR").Width = Unit.Percentage(5)

            Me.uwgEspecs.Bands(0).Columns.FromKey("FECALTA").Header.Caption = oTextos.Rows(6).Item(1)
            Me.uwgEspecs.Bands(0).Columns.FromKey("NOMBRE").Header.Caption = oTextos.Rows(7).Item(1)
            Me.uwgEspecs.Bands(0).Columns.FromKey("NOM").Header.Caption = oTextos.Rows(2).Item(1)
            Me.uwgEspecs.Bands(0).Columns.FromKey("DATASIZE").Header.Caption = oTextos.Rows(4).Item(1)
            Me.uwgEspecs.Bands(0).Columns.FromKey("COMENT").Header.Caption = oTextos.Rows(3).Item(1)
            Me.uwgEspecs.Bands(0).Columns.FromKey("DESCARGAR").Header.Caption = oTextos.Rows(5).Item(1)


        End Sub

        ''' <summary>
        ''' Inicializa una linea del grid.
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub uwgEspecs_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgEspecs.InitializeRow
            Dim oUser As Fullstep.PMPortalServer.User
            oUser = Session("FS_Portal_User")
            Dim oNumber As Double = e.Row.Cells.FromKey("DATASIZE").Value

            e.Row.Cells.FromKey("DATASIZE").Text = oNumber.ToString(oUser.NumberFormat)
        End Sub
    End Class
End Namespace