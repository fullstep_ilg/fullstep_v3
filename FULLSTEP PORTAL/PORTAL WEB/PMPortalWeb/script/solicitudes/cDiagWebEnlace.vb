Imports Syncfusion.Windows.Forms.diagram

<Serializable()> _
Public Class cDiagWebEnlace
    Inherits Link
    Public Shared tppX As Integer
    Public Shared tppY As Integer

    Public Sub New(ByVal oBloqueOrigen As CDiagWebBloque, ByVal oBloqueDestino As CDiagWebBloque, ByVal text As String, ByVal color As Color, Optional ByVal ptsIntermedios() As PointF = Nothing)

        ''' Clase para los cambios de estado del workflow, que contiene lo necesario para representarlos.
        ''' Hereda de la clase de nodo gen�rica LINK de Essential Diagram

        'Este constructor comentado permitir�a establecer los puntos origen y destino.
        'Public Sub New(ByVal oBloqueOrigen As CDiagBloque, ByVal oBloqueDestino As CDiagBloque, ByVal xOrigen As Integer, ByVal yOrigen As Integer, ByVal xDestino As Integer, ByVal yDestino As Integer, ByVal text As String, ByVal color As Color)

        'Se crean el punto origen y el de destino en los dos bloques a unir

        Dim sourceport, targetport As Port
        Dim ptIntermedio As PointF
        Dim ptCounter As Integer

        'Estas dos l�neas se utilizar�an con el constructor comentado
        'sourceport = New CirclePort(New PointF(xOrigen, yOrigen))
        'targetport = New CirclePort(New PointF(xDestino, yDestino))

        sourceport = oBloqueOrigen.CenterPort
        targetport = oBloqueDestino.CenterPort

        oBloqueOrigen.AppendChild(sourceport)
        oBloqueDestino.AppendChild(targetport)

        'El primer punto es el puerto de origen.

        Me.Connect(sourceport, Me.TailPort)

        'Quitamos el segundo y �ltimo punto para a�adir puntos intermedios si procede.

        Me.Points.RemovePoint(Me.Points.PointCount - 1)

        'A�adimos los puntos intermedios

        If Not (ptsIntermedios Is Nothing) Then

            For Each ptIntermedio In ptsIntermedios

                ptIntermedio.X = ptIntermedio.X / tppX
                ptIntermedio.Y = ptIntermedio.Y / tppY

                Me.Points.AddPoint(ptIntermedio)

            Next

        End If

        'A�adimos un punto final para conectarlo al puerto destino.

        Me.Points.AddPoint(New PointF(0, 0))

        'Lo conectamos.

        Me.Connect(targetport, Me.HeadPort)

        'Caracter�sticas de la flecha.

        Me.EndPoints.LastEndPointDecorator = New EndPointDecorator(EndPointVisuals.ClosedArrow)
        Me.LineStyle.LineColor = color

        'Texto de la flecha.

        Dim lbl As Label = Me.AddLabel(text, 50)
        lbl.BackgroundStyle.Color = color.Transparent
        lbl.FontStyle.Family = "Tahoma"
        lbl.FontStyle.Size = 8
        lbl.HorizontalAlignment = StringAlignment.Center
        lbl.VerticalAlignment = StringAlignment.Center

    End Sub
End Class
