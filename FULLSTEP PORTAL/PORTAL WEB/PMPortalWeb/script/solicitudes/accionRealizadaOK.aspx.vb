Namespace Fullstep.PMPortalWeb
    Partial Class accionRealizadaOK
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim oDict As Fullstep.PMPortalServer.Dictionary
            Dim oTextos As DataTable
            Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
            Dim oInstancia As Fullstep.PMPortalServer.Instancia
            Dim lCiaComp As Long = IdCiaComp

            Dim sIdi As String = Idioma
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If

            oDict = FSPMServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.AccionRealizadaOK, sIdi)
            oTextos = oDict.Data.Tables(0)

            'Textos de idiomas:
            Me.lblLitId.Text = oTextos.Rows(0).Item(1)
            Me.lblFecha.Text = oTextos.Rows(1).Item(1)
            Me.lblLitPeticionario.Text = oTextos.Rows(2).Item(1)
            Me.lblMensaje.Text = oTextos.Rows(4).Item(1)
            Me.lblEtapas.Text = oTextos.Rows(5).Item(1)
            Me.lblDen.Text = oTextos.Rows(6).Item(1)

            If Page.IsPostBack = True Then
                Exit Sub
            End If

            'Carga los datos de la instancia:
            oInstancia = FSPMServer.Get_Instancia
            oInstancia.ID = Request("Instancia")
            oInstancia.Load(lCiaComp, sIdi, True)
            oInstancia.Solicitud.Load(lCiaComp, sIdi)

            If Not oUser.VerDetallePersona(oInstancia.ID, lCiaComp).Rows(0)("VER_DETALLE_PER") Then
                cmdBuscarPet.Visible = False
                lblPeticionario.Visible = False
                lblLitPeticionario.Visible = False
            End If

            'Datos de la instancia:
            Me.lblFechaBD.Text = modUtilidades.FormatDate(oInstancia.FechaAlta, oUser.DateFormat)
            Me.lblId.Text = oInstancia.ID
            Me.lblPeticionario.Text = oInstancia.NombrePeticionario
            Me.txtPeticionario.Value = oInstancia.Peticionario
            Me.lblDenBD.Text = oInstancia.Solicitud.Codigo & "-" & oInstancia.Solicitud.Den(sIdi)

            'Carga la acci�n:
            Dim oAccion As Fullstep.PMPortalServer.Accion
            oAccion = FSPMServer.Get_Accion
            oAccion.Id = Request("Accion")
            oAccion.CargarAccion(lCiaComp, sIdi)
            Me.lblTitulo.Text = oTextos.Rows(3).Item(1) & " " & oAccion.Den

            If oAccion.TipoRechazo = Fullstep.PMPortalServer.TipoRechazoAccion.Anulacion Then
                'Es una anulaci�n, no provoca cambios de etapas:
                Me.tblGeneral.Rows.RemoveAt(4)
                Me.tblGeneral.Rows.RemoveAt(3)

            Else
                'Comprueba las etapas por las que pasar�:
                Dim oEtapas As DataSet
				oEtapas = oInstancia.DevolverEtapasRealizadas(lCiaComp, Request("Accion"), Replace(Trim(Request("BloqueDestino")), " ", ","), sIdi)
				If oEtapas.Tables.Count > 0 Then
                    If oEtapas.Tables(0).Rows.Count > 0 Then
                        lstEtapas.DataSource = oEtapas.Tables(0)
                        lstEtapas.DataTextField = "DEN"
                        lstEtapas.DataBind()
                    Else
                        'Si no ha provocado ning�n cambio de etapa, as� que quita del formulario las filas
                        'correspondientes a los cambios de etapas:
                        Me.tblGeneral.Rows.RemoveAt(4)
                        Me.tblGeneral.Rows.RemoveAt(3)
                    End If
                Else
                    'Si no ha provocado ning�n cambio de etapa, as� que quita del formulario las filas
                    'correspondientes a los cambios de etapas:
                    Me.tblGeneral.Rows.RemoveAt(4)
                    Me.tblGeneral.Rows.RemoveAt(3)
                End If
                oEtapas = Nothing
            End If
            If Not Page.ClientScript.IsClientScriptBlockRegistered("rutaFacturas") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFacturas", "<script>var rutaFacturas=" & IIf(oInstancia.Solicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.Factura, True.ToString.ToLower(), False.ToString.ToLower()) & ";</script>")

            Dim iTipoVisor As TipoVisor = TipoVisor.Solicitudes
            If Not Request.QueryString("TipoVisor") Is Nothing Then iTipoVisor = CType(Request.QueryString("TipoVisor"), TipoVisor)
            If Not Page.ClientScript.IsClientScriptBlockRegistered("tipoVisor") Then Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tipoVisor", "<script>var tipoVisor=" & iTipoVisor & ";</script>")

            oTextos = Nothing
            oDict = Nothing
            oUser = Nothing
            oInstancia = Nothing

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)
            End If
        End Sub

    End Class
End Namespace