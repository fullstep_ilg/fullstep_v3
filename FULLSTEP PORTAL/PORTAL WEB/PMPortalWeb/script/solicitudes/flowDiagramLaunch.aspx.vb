Partial Class flowDiagramLaunch
    Inherits FSPMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Session("DpiX") = "" Then

            Response.Redirect("detectscreen.aspx?Instancia=" & Request("Instancia") & "&Codigo=" & Request("Codigo") & "&NumFactura=" & Request("NumFactura"))

        Else

            cDiagWebBloque.tppX = 1440 / Session("DpiX")
            cDiagWebBloque.tppY = 1440 / Session("DpiY")
            cDiagWebEnlace.tppX = cDiagWebBloque.tppX
            cDiagWebEnlace.tppY = cDiagWebBloque.tppY

            Response.Redirect("flowDiagram.aspx?Instancia=" & Request("Instancia") & "&Codigo=" & Request("Codigo") & "&NumFactura=" & Request("NumFactura"))

        End If
    End Sub

End Class
