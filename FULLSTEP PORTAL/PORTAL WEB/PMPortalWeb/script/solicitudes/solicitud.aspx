<%@ Page Language="vb" AutoEventWireup="false" Codebehind="solicitud.aspx.vb" Inherits="Fullstep.PMPortalWeb.solicitud" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
		<title>solicitud</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<SCRIPT type="text/javascript"><!--

		function uwgEspecs_CellClickHandler(gridName, cellId, button){
			var row = igtbl_getRowById(cellId); 
			var columnClicked = igtbl_getColumnById(cellId)
			var cell = row.getCell(0); 
			var IdAdjunto = cell.getValue()

			window.open("espec.aspx?id=" + IdAdjunto, "_blank","width=600,height=275,status=no,resizable=no,top=200,left=200,menubar=yes")
		}
--></SCRIPT>
	</head>
	<body XMLNS:igtbl="http://schemas.infragistics.com/ASPNET/WebControls/UltraWebGrid">
		<form id="Form1" method="post" runat="server">
			<br>
			<asp:label id="lblTitulo" style="Z-INDEX: 104" runat="server" Width="100%" CssClass="subtitulo">Solicitud de material codificado</asp:label><br>
			<br>
			<asp:label id="lblDescripcion" style="Z-INDEX: 103" runat="server" Width="88px" CssClass="captionBlue"
				Height="16px">Descripción</asp:label><br>
			<asp:label id="txtDescripcion" runat="server" Width="100%" CssClass="parrafo bordeado" Height="56px"></asp:label><br>
			<br>
			<asp:label id="lblArchivos" style="Z-INDEX: 101" runat="server" Width="192px" CssClass="captionBlue"
				Height="16px">Archivos de especificación</asp:label><br>
			<TABLE class="bordeado" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD><igtbl:ultrawebgrid id="uwgEspecs" style="Z-INDEX: 105" runat="server" Width="100%" Height="200px">
							<DisplayLayout RowHeightDefault="20px" Version="4.00" BorderCollapseDefault="Separate" Name="uwgEspecs">
								<AddNewBox>
									<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">

<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
</BorderDetails>

</Style>
								</AddNewBox>
								<Pager>
									<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">

<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
</BorderDetails>

</Style>
								</Pager>
								<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" Height="200px"></FrameStyle>
								<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
									<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
								</FooterStyleDefault>
								<ClientSideEvents CellClickHandler="uwgEspecs_CellClickHandler"></ClientSideEvents>
								<ActivationObject AllowActivation="False"></ActivationObject>
								<EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
							</DisplayLayout>
							<Bands>
								<igtbl:UltraGridBand CellSpacing="1" CellPadding="3" RowSelectors="No">
									<HeaderStyle CssClass="cabecera"></HeaderStyle>
									<RowAlternateStyle CssClass="ugfilaalttabla"></RowAlternateStyle>
									<Columns>
										<igtbl:UltraGridColumn HeaderText="Descargar" Key="DESCARGAR" BaseColumnName="">
											<CellStyle BackgroundImage="./images/descargarespecif_off.gif" CustomRules="Background-repeat:no-repeat;background-position:center center"></CellStyle>
											<Header Caption="Descargar"></Header>
										</igtbl:UltraGridColumn>
									</Columns>
									<RowStyle CssClass="ugfilatabla"></RowStyle>
								</igtbl:UltraGridBand>
							</Bands>
						</igtbl:ultrawebgrid></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</html>
