<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="detalleSolicitud.aspx.vb"
    Inherits="Fullstep.PMPortalWeb.detalleSolicitud" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>detalleSolicitud</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../../../common/estilo.asp" type="text/css" rel="stylesheet">
    <script src="../../../common/formatos.js"></script>
    <script src="../../../common/menu.asp"></script>
    <script src="../_common/js/AdjacentHTML.js" type="text/javascript"></script>
    <script src="/ig_common/20043/Scripts/ig_csom.js" type="text/javascript"></script>
    <script src="/ig_common/20043/scripts/ig_WebGrid_dom.js" type="text/javascript"></script>
    <script src="/ig_common/20043/scripts/ig_WebGrid_ie.js" type="text/javascript"></script>
    <script src="/ig_common/20043/scripts/ig_WebGrid_kb.js" type="text/javascript"></script>
    <script src="/ig_common/20043/scripts/ig_WebGrid_ie6.js" type="text/javascript"></script>
    <script src="/ig_common/20043/scripts/ig_WebGrid.js" type="text/javascript"></script>
    <style type="text/css">
        html, body, form {
            width: 99.5%;
            height: 100%;
        }
    </style>
</head>
<body onresize="resize()" onload="init()" onunload="if (wProgreso != null) wProgreso.close();">
    <script src="../_common/js/jsAlta.js?v=<%=ConfigurationManager.AppSettings("versionJs")%>" type="text/javascript"></script>
    <script type="text/javascript">
        wProgreso = null;

        function resize() {
            for (i = 0; i < arrDesgloses.length; i++) {
                sDiv = arrDesgloses[i].replace("tblDesglose", "divDesglose");
                if (sDiv) document.getElementById(sDiv).style.width = (parseFloat(document.body.offsetWidth) - 85) + "px";
            };
        };

        function localEval(s) {
            eval(s);
        };

        //Muestra el detalle del tipo de solicitud
        function DetalleTipoSolicitud() {
            var IdSolicitud = document.forms["frmDetalle"].elements["txtIdTipo"].value;
            window.open("solicitud.aspx?Solicitud=" + IdSolicitud, "_blank", "width=700,height=450,status=no,resizable=no,top=100,left=100");
        };
        function cmdImpExp_onclick() {
            /*''' <summary>
            ''' Exportar el certificado 
            ''' </summary>
            ''' <remarks>Llamada desde: bt cmdImpExp; Tiempo m�ximo:0</remarks>*/
            window.open('../_common/impexp_sel.aspx?Instancia=' + document.getElementById("Instancia").value + '&TipoImpExp=1', '_new', 'fullscreen=no,height=115,width=315,location=no,menubar=no,resizable=no,scrollbars=no,status=yes,titlebar=yes,toolbar=no,left=200,top=200');
            return false;
        };
        function Devolver() {
            var respOblig = comprobarObligatorios();
            switch (respOblig) {
                case "":  //no falta ningun campo obligatorio
                    break;
                case "filas0": //no se han introducido filas en un desglose obligatorio
                    alert(arrTextosML[0]);
                    return false;
                    break;
                default: //falta algun campo obligatorio
                    alert(arrTextosML[0] + '\n' + respOblig);
                    return false;
                    break;
            }

            if (wProgreso == null)
                wProgreso = window.open("../_common/espera.aspx?cadena=" + document.forms["frmDetalle"].elements["cadenaespera"].value, "_blank", "top=300		,left=450,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")

            MontarFormularioSubmit(true);

            iVersion = document.forms["frmDetalle"].elements["Version"].value;
            document.forms["frmSubmit"].elements["GEN_Version"].value = iVersion;

            document.forms["frmSubmit"].elements["GEN_AccionRol"].value = 0;
            document.forms["frmSubmit"].elements["GEN_Bloque"].value = document.forms["frmDetalle"].elements["Bloque"].value;
            document.forms["frmSubmit"].elements["GEN_Rol"].value = oRol_Id;
            document.forms["frmSubmit"].elements["GEN_Enviar"].value = 1;
            document.forms["frmSubmit"].elements["GEN_Accion"].value = "devolversolicitud";

            oFrm = MontarFormularioCalculados();
            sInner = oFrm.innerHTML;
            oFrm.innerHTML = "";
            document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner);
            document.forms["frmSubmit"].style.display = "none";
            document.forms["frmSubmit"].submit();

            return false;
        }

        function EjecutarAccion(id, bloque, comp_obl, guarda, bloq, rechazo) {
            if (bloq == 2) {
                alert(arrTextosML[2]);
                return false;
            }

            if (comp_obl == true || comp_obl == "true") {
                bMensajePorMostrar = document.getElementById("bMensajePorMostrar")
                if (bMensajePorMostrar)
                    if (bMensajePorMostrar.value == "1") {
                        bMensajePorMostrar.value = "0";
                        return false;
                    };
                var respOblig = comprobarObligatorios();
                switch (respOblig) {
                    case "":  //no falta ningun campo obligatorio
                        break;
                    case "filas0": //no se han introducido filas en un desglose obligatorio
                        alert(arrTextosML[0]);
                        return false;
                        break;
                    default: //falta algun campo obligatorio
                        alert(arrTextosML[0] + '\n' + respOblig);
                        return false;
                        break;
                }
            }
            if (rechazo == false || rechazo == "false") {
                if (comprobarSiParticipantes() == false) {
                    alert(arrTextosML[1]);
                    return false;
                }
            }

            if (wProgreso == null)
                wProgreso = window.open("../_common/espera.aspx?cadena=" + document.forms["frmDetalle"].elements["cadenaespera"].value, "_blank", "top=300		,left=450,width=400,height=100,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no")

            var hayMapper = false
            if (document.forms["frmDetalle"].elements["PantallaMaper"].value == "True")
                hayMapper = true
            MontarFormularioSubmit(guarda, true, hayMapper);

            iVersion = document.forms["frmDetalle"].elements["Version"].value;
            document.forms["frmSubmit"].elements["GEN_Version"].value = iVersion;
            document.forms["frmSubmit"].elements["GEN_AccionRol"].value = id;
            document.forms["frmSubmit"].elements["GEN_Bloque"].value = bloque;
            document.forms["frmSubmit"].elements["GEN_Enviar"].value = 1;
            document.forms["frmSubmit"].elements["GEN_Rol"].value = oRol_Id;
            document.forms["frmSubmit"].elements["PantallaMaper"].value = hayMapper;

            oFrm = MontarFormularioCalculados();
            sInner = oFrm.innerHTML;
            oFrm.innerHTML = "";
            document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner);
            document.forms["frmSubmit"].style.display = "none";
            document.forms["frmSubmit"].submit();
            return false;
        }

        function CalcularCamposCalculados() {
            oFrm = MontarFormularioCalculados();
            oFrm.submit();
            return false;
        }

        // Funci�n que modifica el ancho m�nimo de un control
        // Llamada en el evento InitializeTabs del control uwtGrupos. M�x < 1seg
        function initTab(webTab) {
            var cp = document.getElementById(webTab.ID + '_cp');
            cp.style.minHeight = '300px';
            cp = null;
        }

        /*''' <summary>
        ''' Iniciar la pagina.
        ''' </summary>     
        ''' <remarks>Llamada desde: page.onload; Tiempo m�ximo:0</remarks>*/
        function init() {
            if (accesoExterno == 0) { document.getElementById('tablemenu').style.display = 'block'; }

            resize();
            AgruparEstilos();
            if (arrObligatorios.length > 0)
                if (document.getElementById("lblCamposObligatorios"))
                    document.getElementById("lblCamposObligatorios").style.display = "";
        }

        function irAcomentarios() {
            window.open("comentariosSolicitud.aspx?Instancia=" + document.forms["frmDetalle"].elements["Instancia"].value, "_blank", "width=1000,height=560,status=yes,resizable=no,top=200,left=200");
        }

        function HabilitarBotones() {
            //No borrar q GuardarInstancia lo llama
			if (wProgreso != null) wProgreso.close();
        }

        function Volver() {
            window.location = rutaVolver;
            return false;
        }

        function EliminarInstancia() {
            if (confirm(arrTextosML[2])) {
                PageMethods.EliminarInstancia(document.forms["frmDetalle"].elements["Instancia"].value, function (ok) {
                    if (ok) window.open('../solicitudes/VisorSolicitudes.aspx?TipoVisor=' + tipoVisor, '_self');
                    else alert(arrTextosML[3]);
                });
            };
        };

        function Guardar() {
            var frmDetalleElements = document.forms["frmDetalle"].elements;                       

            if (wProgreso == null) wProgreso = true;

            setTimeout("MontarSubmitGuardar();", 100);
        };
        function MontarSubmitGuardar() {
            MontarFormularioSubmit(true, true, false);

            var frmSubmitElements = document.forms["frmSubmit"].elements;
            frmSubmitElements["GEN_AccionRol"].value = 0;
            frmSubmitElements["GEN_Bloque"].value = document.forms["frmDetalle"].elements["Bloque"].value;
            frmSubmitElements["GEN_Enviar"].value = 0;
            frmSubmitElements["GEN_Accion"].value = "guardarsolicitud";
            frmSubmitElements["GEN_Rol"].value = oRol_Id;
            frmSubmitElements["PantallaMaper"].value = false;
            frmSubmitElements["DeDonde"].value = "Guardar";

            var oFrm = MontarFormularioCalculados();
            var sInner = oFrm.innerHTML;
            oFrm.innerHTML = "";
            document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner);
            frmSubmitElements = null;
            oFrm = null;
            sInner = null;
            document.forms["frmSubmit"].submit();
        };
    </script>    
    <form id="frmDetalle" method="post" runat="server">
        <div style="position:fixed; background-color:white; z-index:1000; width:100%;">
            <script type="text/javascript">
                 switch (Number(tipoVisor)) {
                    case 5:
                        dibujaMenu(9);
                        break;
                    case 6:
                    case 7:
                        dibujaMenu(2);
                        break;
                    default:
                        dibujaMenu(3);
                }
            </script>       
            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
            <br />
            <div style="margin-top: 1em;">
                <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
            </div>
        </div>
        <br />
        <input id="bMensajePorMostrar" type="hidden" value="0" name="bMensajePorMostrar" />
        <input id="Instancia" type="hidden" name="Instancia" runat="server" />
        <input id="Version" type="hidden" name="Version" runat="server" />
        <input id="txtEnviar" type="hidden" name="Enviar" runat="server" />
        <iframe id="iframeWSServer" style="display: none;" name="iframeWSServer" src="../blank.htm"></iframe>
        <!------------------------------------------------->
        <div style="padding-left: 15px; padding-bottom: 15px; margin-top:7em;">
            <asp:Panel ID="pnlCabecera" runat="server" BackColor="#f5f5f5" Font-Names="Arial"
                Width="95%">
                <table style="height: 15%; width: 100%; padding-bottom: 15px; padding-left: 5px"
                    cellspacing="0" cellpadding="1" border="0">
                    <tr>
                        <td style="padding-top: 5px; padding-bottom: 5px;" class="fondoCabecera">
                            <table style="width: 100%; padding-left: 10px" border="0">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lblIDInstanciayEstado" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblLitImporte" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label>
                                    </td>
                                    <td colspan="2" style="text-align:left;">
                                        <asp:Label ID="lblImporte" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblLitCreadoPor" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCreadoPor" runat="server" CssClass="label"></asp:Label>
                                        <asp:Image ID="imgInfCreadoPor" ImageUrl="images/info.gif" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblLitEstado" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblEstado" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:HyperLink ID="HyperDetalle" runat="server" CssClass="CaptionLink"></asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblLitFecAlta" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblFecAlta" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblLitTipo" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTipo" runat="server" CssClass="label"></asp:Label>
                                        <asp:Image ID="imgInfTipo" ImageUrl="images/info.gif" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblLitTrasladadaPor" runat="server" CssClass="captionDarkGraySmall"
                                            Font-Size="12px"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTrasladadaPor" runat="server" CssClass="label"></asp:Label>
                                        <asp:Image ID="imgInfTrasladadaPor" ImageUrl="images/info.gif" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblLitFechaTraslado" runat="server" CssClass="captionDarkGraySmall"
                                            Font-Size="12px"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblFechaTraslado" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblLitFechaDevolucion" runat="server" CssClass="captionDarkGraySmall"
                                            Font-Size="12px"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblFechaDevolucion" runat="server" CssClass="label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <asp:Label ID="lblCamposObligatorios" Style="z-index: 102; display: none;" runat="server"
                                            CssClass="captionRed"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
        <cc1:DropShadowExtender TrackPosition="true" ID="DropShadowExtender1" runat="server"
            Opacity="0.5" Width="3" TargetControlID="pnlCabecera" Rounded="true">
        </cc1:DropShadowExtender>
        <!---------------------------------------------------->
        <table id="Table1" height="95%" cellspacing="5" cellpadding="1" width="100%" border="0">
            <tr height="60%">
                <td width="100%" colspan="2" valign="top">
                    <igtab:UltraWebTab ID="uwtGrupos" runat="server" Width="100%" Height="100%" DisplayMode="Scrollable"
                        ThreeDEffect="False" DummyTargetUrl=" " FixedLayout="True" CustomRules="padding:10px;"
                        BorderWidth="1px" BorderStyle="Solid">
                        <DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
                            <Padding Left="20px" Right="20px"></Padding>
                        </DefaultTabStyle>
                        <ClientSideEvents InitializeTabs="initTab" />
                        <RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter"></RoundedImage>
                        <Tabs>
                            <igtab:Tab Text="New Tab"></igtab:Tab>
                            <igtab:Tab Text="New Tab"></igtab:Tab>
                        </Tabs>
                    </igtab:UltraWebTab>
                </td>
            </tr>
        </table>
        <input id="txtIdTipo" type="hidden" name="txtIdTipo" runat="server" />
        <input id="txtPeticionario" type="hidden" name="txtIdTipo" runat="server" />
        <input id="Bloque" type="hidden" name="Bloque" runat="server" />
        <div id="divDropDowns" style="visibility: hidden;"></div>
        <div id="divCalculados" style="visibility: hidden;" name="divCalculados"></div>
        <div id="divAlta" style="visibility: hidden"></div>
        <div id="divAcciones" runat="server">
            <ignav:UltraWebMenu ID="uwPopUpAcciones" runat="server" WebMenuTarget="PopupMenu"
                SubMenuImage="ig_menuTri.gif" ScrollImageTop="ig_menu_scrollup.gif" Cursor="Default"
                ScrollImageBottomDisabled="ig_menu_scrolldown_disabled.gif" ScrollImageTopDisabled="ig_menu_scrollup_disabled.gif"
                ScrollImageBottom="ig_menu_scrolldown.gif">
                <ItemStyle CssClass="ugMenuItem"></ItemStyle>
                <DisabledStyle ForeColor="LightGray">
                </DisabledStyle>
                <HoverItemStyle Cursor="Hand" CssClass="ugMenuItemHover">
                </HoverItemStyle>
                <IslandStyle Cursor="Default" BorderWidth="1px" Font-Size="8pt" Font-Names="MS Sans Serif"
                    BorderStyle="Outset" ForeColor="Black" BackColor="LightGray">
                </IslandStyle>
                <ExpandEffects ShadowColor="LightGray"></ExpandEffects>
                <TopSelectedStyle Cursor="Default">
                </TopSelectedStyle>
                <SeparatorStyle BackgroundImage="ig_menuSep.gif" CssClass="SeparatorClass" CustomRules="background-repeat:repeat-x; "></SeparatorStyle>
                <Levels>
                    <ignav:Level Index="0"></ignav:Level>
                </Levels>
            </ignav:UltraWebMenu>
        </div>
        <input id="cadenaespera" type="hidden" name="cadenaespera" runat="server" />
        <input id="PantallaMaper" type="hidden" name="PantallaMaper" runat="server" />
        <fsn:FSNPanelInfo ID="FSNPanelDatosPeticionario" runat="server" ServicePath="~/Consultas.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="0"></fsn:FSNPanelInfo>
        <fsn:FSNPanelInfo ID="FSNPanelDatosTrasladadaPor" runat="server" ServicePath="~/Consultas.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="0"></fsn:FSNPanelInfo>
        <fsn:FSNPanelInfo ID="FSNPanelDatosProveedor" runat="server" ServicePath="~/Consultas.asmx" ServiceMethod="Obtener_DatosProveedor" TipoDetalle="1"></fsn:FSNPanelInfo>
    </form>
    <form id="frmCalculados" name="frmCalculados" action="../_common/recalcularimportes.aspx"
        method="post" target="fraPMPortalServer" style="max-height: 1px;">
    </form>
    <form id="frmDesglose" name="frmDesglose" method="post" target="winDesglose" style="max-height: 1px;"></form>
    <!--<%'Pasamos el include de jquery al final de la p�gina para que no haya problemas con el fsnPageHeader de FSNWEBCONTROLS, que lleva tambi�n jquery, y sobreescrib�a el de aqu� impidiendo que funcione el tmpl %> -->
    <script src="../../js/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="../../js/jsUtilities.js" type="text/javascript"></script>
    <script src="../../js/jquery/jquery.tmpl.min.js" type="text/javascript"></script>
</body>
</html>
