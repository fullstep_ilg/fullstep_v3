<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="extFrames2.aspx.vb" Inherits="Fullstep.PMPortalWeb.extSolicitudesFrames2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title id="PageCaption">PM</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <style type="text/css">
        html, body, form {
            width: 100%;
            height: 100%;
        }
    </style>
</head>
<frameset runat="server" id="fraWS" rows="*,0" frameborder="no">
		<frame id="fraPMPortalMain" name="fraPMPortalMain" src="VisorSolicitudes.aspx?<%=request.querystring()%>" />
		<frame id="fraPMPortalServer" name="fraPMPortalServer" src="../blank.htm" scrolling="no"/>
	</frameset>
</html>
