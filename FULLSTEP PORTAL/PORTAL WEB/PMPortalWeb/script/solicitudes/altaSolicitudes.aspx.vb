﻿Imports Fullstep.PMPortalServer

Namespace Fullstep.PMPortalWeb
    Public Class altaSolicitudes
        Inherits FSPMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Dim tipoSolicitud As Integer = 0
            Dim oSolicitudes As Solicitudes
            Dim dsSolicitudes As DataSet

            ScriptManager1.EnablePageMethods = True
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.min.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.ui.min.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.tmpl.min.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery-migrate.min.js"))

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)

            If Request("TipoSolicitud") <> Nothing Then tipoSolicitud = CInt(Request("TipoSolicitud"))
            If Not ClientScript.IsStartupScriptRegistered("tipoSolicitud") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tipoSolicitud", "var tipoSolicitud=" & tipoSolicitud & ";", True)

            If tipoSolicitud = TiposDeDatos.TipoDeSolicitud.Factura Then
                ModuloIdioma = TiposDeDatos.ModulosIdiomas.AltaFacturas
            Else
                ModuloIdioma = TiposDeDatos.ModulosIdiomas.AltaSolicitudes
            End If
            CargarTextos()
            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/altaSolicitud.jpg"

            oSolicitudes = FSPMServer.Get_Solicitudes()
            If tipoSolicitud = TiposDeDatos.TipoDeSolicitud.Factura Then
                dsSolicitudes = oSolicitudes.DevolverSolicitudesAlta(FSPMUser.CiaComp, FSPMUser.CodProveGS, FSPMUser.IdContacto, TiposDeDatos.TipoDeSolicitud.Factura, Idioma)
            Else
                dsSolicitudes = oSolicitudes.DevolverSolicitudesAlta(FSPMUser.CiaComp, FSPMUser.CodProveGS, FSPMUser.IdContacto, tipoSolicitud, Idioma)
            End If

            If dsSolicitudes.Tables.Count > 0 And dsSolicitudes.Tables(0).Rows.Count > 0 Then
                wdgSolicitudes.DataSource = dsSolicitudes
                wdgSolicitudes.DataBind()
            Else
                wdgSolicitudes.Visible = False
                tblSolicitudes.Visible = False
                lblSinDatos.Visible = True
            End If
        End Sub

        Private Sub CargarTextos()

            FSNPageHeader.TituloCabecera = Textos(0)
            lblParrafo.Text = Textos(1)
            lblCabeceraAltaSolicitudes.Text = Textos(2)
            lblFooter.Text = Textos(3)
            lblSinDatos.Text = Textos(4)
        End Sub

    End Class
End Namespace