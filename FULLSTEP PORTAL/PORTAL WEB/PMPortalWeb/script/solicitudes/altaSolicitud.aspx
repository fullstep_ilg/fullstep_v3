﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="altaSolicitud.aspx.vb" Inherits="Fullstep.PMPortalWeb.altaSolicitud" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../../common/estilo.asp" type="text/css" rel="stylesheet" />
    <script src="../../../common/menu.asp"></script>
</head>

<body>
    <script src="../_common/js/jsAlta.js?v=<%=ConfigurationManager.AppSettings("versionJs")%>" type="text/javascript"></script>
    <form id="frmAlta" method="post" runat="server" onkeydown="replaceEnterKeyWithTab()" onkeypress="FFreplaceEnterKeyWithTab(event)">
        <div style="position:fixed; background-color:white; z-index:1000; width:100%;">
            <script type="text/javascript">
                if (tipoSolicitud == 13) dibujaMenu(9);
                else dibujaMenu(3);
            </script>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
                <Scripts>
                    <asp:ScriptReference Path="~/script/solicitudes/js/altaSolicitud.js" />
                </Scripts>
            </asp:ScriptManager>
            <div style="margin-top: 2em;">
                <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
            </div>
        </div>
        <input id="bMensajePorMostrar" type="hidden" value="0" name="bMensajePorMostrar" />
        <iframe id="iframeWSServer" style="display: none;" name="iframeWSServer" src="../blank.htm"></iframe>

        <div id="divAcciones" runat="server">
            <ignav:UltraWebMenu ID="uwPopUpAcciones" runat="server" WebMenuTarget="PopupMenu"
                SubMenuImage="ig_menuTri.gif" ScrollImageTop="ig_menu_scrollup.gif" Cursor="Default"
                ScrollImageBottomDisabled="ig_menu_scrolldown_disabled.gif" ScrollImageTopDisabled="ig_menu_scrollup_disabled.gif"
                ScrollImageBottom="ig_menu_scrolldown.gif">
                <ItemStyle CssClass="ugMenuItem"></ItemStyle>
                <DisabledStyle ForeColor="LightGray"></DisabledStyle>
                <HoverItemStyle Cursor="Hand" CssClass="ugMenuItemHover"></HoverItemStyle>
                <IslandStyle Cursor="Default" BorderWidth="1px" Font-Size="8pt" Font-Names="MS Sans Serif"
                    BorderStyle="Outset" ForeColor="Black" BackColor="LightGray">
                </IslandStyle>
                <ExpandEffects ShadowColor="LightGray"></ExpandEffects>
                <TopSelectedStyle Cursor="Default"></TopSelectedStyle>
                <SeparatorStyle BackgroundImage="ig_menuSep.gif" CssClass="SeparatorClass" CustomRules="background-repeat:repeat-x; "></SeparatorStyle>
                <Levels>
                    <ignav:Level Index="0"></ignav:Level>
                </Levels>
            </ignav:UltraWebMenu>
        </div>
        <div>
            <asp:Label ID="lblCamposObligatorios" Style="z-index: 102; display: none;" runat="server" CssClass="captionRed">Los campos marcados con (*) son de obligada cumplimentacion</asp:Label>
        </div>
        <div id="divProgreso" style="display: inline">
            <table id="tblProgreso" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
                <tr style="height: 50px">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:TextBox ID="lblProgreso" runat="server" BorderStyle="None" BorderWidth="0" CssClass="captionBlue"
                            Width="100%" Style="text-align: center">Su solicitud está siendo tramitada. Espere unos instantes...</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:Image ID="imgProgreso" runat="server" src="../_common/images/cursor-espera_grande.gif"></asp:Image>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divForm2" style="display: none; margin-top:6em;">
            <table cellspacing="3" cellpadding="3">
                <tr>
                    <td colspan="4">
                        <igtab:UltraWebTab ID="uwtGrupos" Style="z-index: 101" runat="server" BorderStyle="Solid" BorderWidth="1px"
                            Width="100%" EnableViewState="false" ThreeDEffect="False" DummyTargetUrl=" " FixedLayout="True"
                            CustomRules="padding:10px;" DisplayMode="Scrollable">
                            <DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
                                <Padding Left="20px" Right="20px"></Padding>
                            </DefaultTabStyle>
                            <RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter"></RoundedImage>
                            <ClientSideEvents InitializeTabs="initTab" />
                        </igtab:UltraWebTab>
                        <script type="text/javascript">
                            i = 0;
                            var bSalir = false;
                            while (bSalir == false) {
                                if (document.getElementById("uwtGrupos_div" + i)) {
                                    document.getElementById("uwtGrupos_div" + i).style.visibility = 'hidden';
                                    i = i + 1;
                                }
                                else {
                                    bSalir = true;
                                }
                            }
                            bSalir = null;
                        </script>
                    </td>
                </tr>
            </table>
            <input id="Solicitud" type="hidden" name="Solicitud" runat="server" />
            <input id="instanciaPortF" type="hidden" name="instanciaPort" runat="server" />
            <input id="txtEnviar" type="hidden" name="Enviar" runat="server" />
            <input id="Bloque" type="hidden" name="Bloque" runat="server" />
            <input id="NEW_Instancia" type="hidden" name="NEW_Instancia" runat="server" />
            <div id="divAlta" style="visibility: hidden"></div>
            <div id="divDropDowns" style="z-index: 101; visibility: hidden; position: absolute; top: 300px"></div>
            <div id="divCalculados" style="z-index: 107; visibility: hidden; position: absolute; top: 0px"></div>
            <input id="cadenaespera" type="hidden" name="cadenaespera" runat="server" />
            <input id="BotonCalcular" type="hidden" value="0" name="BotonCalcular" runat="server" />
            <input id="PantallaMaper" type="hidden" name="PantallaMaper" runat="server" />
            <input id="Favorito" type="hidden" name="Favorito" runat="server" />
            <input id="IdFavorito" type="hidden" name="IdFavorito" runat="server" />
            <input id="IdInstanciaImportar" type="hidden" name="IdInstanciaImportar" runat="server" />
            <input id="PantallaVinculaciones" type="hidden" name="PantallaVinculaciones" runat="server" />
            <input id="hid_DesdeInicio" type="hidden" runat="server" />
        </div>
    </form>
    <div id="divForm3" style="display: none">
        <form id="frmCalculados" name="frmCalculados" action="../_common/recalcularimportes.aspx" method="post" target="fraPMPortalServer" style="max-height: 1px;"></form>
        <form id="frmDesglose" name="frmDesglose" method="post" target="winDesglose" style="max-height: 1px;"></form>
    </div>
    <script type="text/javascript">
        HabilitarBotones();
        var monedaInput = $.map(arrInputs, function (x) { if (fsGeneralEntry_getById(x) != null) { if (fsGeneralEntry_getById(x).tipoGS == 102 && !fsGeneralEntry_getById(x).basedesglose) return fsGeneralEntry_getById(x); } })[0];
        if ((monedaInput) && (monedaInput.dataValue != '') && (monedaInput.dataValue != null)) {
            var nuevoTexto;
            $.each($('[nombreMoneda]'), function () {
                nuevoTexto = $(this).text().replace('()', '(' + monedaInput.dataValue + ')');
                $(this).text(nuevoTexto);
            });
        }
    </script>
    <!--<%'Pasamos el include de jquery al final de la página para que no haya problemas con el fsnPageHeader de FSNWEBCONTROLS, que lleva también jquery, y sobreescribía el de aquí impidiendo que funcione el tmpl %> -->
    <script src="../../js/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="../../js/jsUtilities.js" type="text/javascript"></script>
    <script src="../../js/jquery/jquery.tmpl.min.js" type="text/javascript"></script>
</body>
</html>
