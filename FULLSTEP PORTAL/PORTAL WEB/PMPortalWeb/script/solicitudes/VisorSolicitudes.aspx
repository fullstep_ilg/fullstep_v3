﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="VisorSolicitudes.aspx.vb" Inherits="Fullstep.PMPortalWeb.VisorSolicitudes" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../../common/estilo.asp" type="text/css" rel="stylesheet">
    <script src="../../../common/formatos.js"></script>
    <script src="../../../common/menu.asp"></script> 
    <style>
        a:link.Normal{
            color:black!important;
            font-size:12px!important;
            text-decoration:underline!important;
        }
    </style>     
    <script type="text/javascript">
        //var tipoVisor;
        function Init() {
            if (accesoExterno == 0) {
                document.getElementById('tablemenu').style.display = 'block';                                
            }
        }
    </script>
</head>
<body onload="Init()">
    <%--<script>;dibujaMenu(2);</script>--%>
    <form id="form1" runat="server" style="margin-top:2em;">
        <script>
            switch (Number(tipoVisor)) {
                case 5:
                    dibujaMenu(9);
                    break;
                case 6:
                case 7:
                    dibujaMenu(2);
                    break;
				default:
					dibujaMenu(99);
					$('.selected.default').removeClass('selected');
					break;
            }
        </script>
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
        
        <div style="padding-top:12px;"><fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>        </div>        
        <div id="pnlSolicitudesPendientes" style="display:none; position:absolute; margin-top:-2em; right:5em;">
            <asp:Image ID="imgSolicitudesPendientes" runat="server" SkinID="AlertaPeq" Style="vertical-align:middle;" />
            <asp:Label runat="server" ID="lblSolicitudesPendientes" CssClass="Etiqueta"></asp:Label>
            <asp:LinkButton ID="lnkSolicitudesPendientes" runat="server" class="Rotulo" style="display:none;"></asp:LinkButton>
        </div>
        <div id="cabeceraEscenarios" class="CabeceraBotones Texto12" style="height: 1.5em; padding: 0.2em 0.3em; cursor: pointer; margin-top:0.5em;">
            <div id="cabeceraEscenariosNombre" style="display: inline-block; width: 10em">
                <asp:Image runat="server" ID="imgEscenariosContraerBarra" SkinID="ContraerColor" Style="display: none;" />
                <asp:Image runat="server" ID="imgEscenariosExpandirBarra" SkinID="ExpandirColor" />
                <asp:Label runat="server" ID="lblEscenariosTituloBarra" CssClass="Rotulo" Style="width: 20em; line-height: 1.5em;"></asp:Label>
            </div>
            <asp:Label runat="server" ID="lblEscenariosTituloSeleccionado" CssClass="Texto12 TextoAlternativo Negrita" Style="line-height: 1.5em; margin-right: 0.5em;"></asp:Label>
            <span id="lblEscenariosSeleccionado" class="Texto12"></span>            
        </div>
        <div id="panelEscenarios" class="Texto12" style="display: none; max-height: 10em; overflow-y: auto;"></div>
        <div id="cabeceraEscenarioFiltros" class="CabeceraBotones Texto12" style="clear: both; height: 1.5em; padding: 0.2em 0.3em; cursor: pointer;">
            <div id="cabeceraFiltrosNombre" style="display: inline-block; width: 10em">
                <asp:Image runat="server" ID="imgEscenarioFiltrosContraerBarra" SkinID="ContraerColor" Style="display: none;" />
                <asp:Image runat="server" ID="imgEscenarioFiltrosExpandirBarra" SkinID="ExpandirColor" />
                <asp:Label runat="server" ID="lblEscenarioFiltrosTituloBarra" CssClass="Rotulo" Style="line-height: 1.5em;"></asp:Label>
            </div>
            <asp:Label runat="server" ID="lblEscenarioFiltrosTituloSeleccionado" CssClass="Texto12 TextoAlternativo Negrita" Style="line-height: 1.5em; margin-right: 0.5em;"></asp:Label>
            <asp:Image runat="server" ID="imgEscenarioFiltrosFormula" ImageUrl="~/Images/Formula.png" class="botonFormula" Style="border: 0.1em;" />
            <span id="lblEscenarioFiltrosSeleccionado" class="Texto12"></span>
            <div id="divFormulaFiltro" class="popupCN" style="position: absolute; display: none; padding: 0.5em;"></div>
        </div>
        <div id="panelEscenarioFiltros" class="Texto12" style="display: none; max-height: 10em; overflow-y: auto;"></div>
        <div id="cabeceraEscenarioFiltroOpciones" class="CabeceraBotones" style="height: 0.5em; text-align: center; cursor: pointer;">
            <asp:Image runat="server" ID="imgEscenarioFiltroOpcionesCollapse" SkinID="Collapse" Style="display: none; vertical-align:top;" />
            <asp:Image runat="server" ID="imgEscenarioFiltroOpcionesExpand" SkinID="Expand" Style="vertical-align:top;" />
        </div>
        <div id="panelEscenarioFiltrosOpcionesFiltrado" class="Texto12 Rectangulo" style="display: none; padding: 0.2em 0.3em; overflow: auto; max-height: 10em;"></div>
        <div id="panelEscenarioVistas" style="margin-top:0.5em;">
            <div id="btnBuscarSolicitudes" class="botonRedondeado" style="display:none; float:right; margin-right:0.5em;">
                <asp:Label runat="server" ID="lblBotonBuscar"></asp:Label>
            </div>
        </div>
        <asp:Button runat="server" ID="btnRecargarGridSolicitudes" Style="display: none;" />
        <asp:Button runat="server" ID="btnPager" Style="display: none;" />
        <asp:Button runat="server" ID="btnOrder" Style="display: none;" />
        <asp:Button runat="server" ID="btnColumnMove" Style="display: none;" />
        <ig:WebExcelExporter ID="whdgExcelExporter" runat="server"></ig:WebExcelExporter>
        <asp:UpdatePanel runat="server" ID="updSolicitudes" UpdateMode="Conditional" style="overflow-y: auto;">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnRecargarGridSolicitudes" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnPager" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnOrder" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnColumnMove" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <asp:HiddenField runat="server" ID="iNumeroSolicitudesFiltroSeleccionado" />
                <asp:HiddenField runat="server" ID="sProducto" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="sFechaIniFSAL8" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="sPagina" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="iPost" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="iP" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="sUsuCod" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="sPaginaOrigen" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="sNavegador" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="sIdRegistroFSAL8" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="sProveCod" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="sQueryString" ClientIDMode="Static" />
                <ig:WebHierarchicalDataGrid runat="server" ID="whdgExportacion"
                    Visible="false" AutoGenerateBands="false"
                    AutoGenerateColumns="false" Width="100%" EnableAjax="true"
                    EnableAjaxViewState="false" EnableDataViewState="false">
                    <ClientEvents Initialize="whdgExportacion_Initialize" />
                    <Bands>
                        <ig:Band Key="DESGLOSE" DataMember="DESGLOSE" DataKeyFields="FORM_INSTANCIA" AutoGenerateColumns="false">
                        </ig:Band>
                    </Bands>
                </ig:WebHierarchicalDataGrid>
                <ig:WebHierarchicalDataGrid runat="server" ID="whdgSolicitudes" Visible="false"
                    AutoGenerateBands="false" AutoGenerateColumns="false"
                    EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="false" Width="100%"
                    InitialDataBindDepth="-1" InitialExpandDepth="-1">
                    <ExpandCollapseAnimation SlideOpenDirection="Auto" SlideOpenDuration="300" SlideCloseDirection="Auto" SlideCloseDuration="300" />
                    <GroupingSettings EnableColumnGrouping="True" GroupAreaVisibility="Visible" />
                    <ClientEvents Click="whdgSolicitudes_CellClick" Initialize="whdgSolicitudes_Initialize" />
                    <Bands>
                        <ig:Band Key="DESGLOSE" DataMember="DESGLOSE" DataKeyFields="FORM_INSTANCIA,DESGLOSE,LINEA" AutoGenerateColumns="false">
                            <Behaviors>
                                <ig:Filtering Enabled="false"></ig:Filtering>
                                <ig:Sorting Enabled="false"></ig:Sorting>
                                <ig:ColumnResizing Enabled="true">
                                    <ColumnResizingClientEvents ColumnResized="whdgSolicitudes_DesgloseColumnResized" />
                                </ig:ColumnResizing>
                                <ig:ColumnMoving Enabled="true">
                                    <ColumnMovingClientEvents HeaderDropped="whdgSolicitudes_DesgloseHeaderDropped" />
                                </ig:ColumnMoving>
                            </Behaviors>
                        </ig:Band>
                    </Bands>
                    <Behaviors>
                        <ig:Activation Enabled="true" />
                        <ig:ColumnResizing Enabled="true">
                            <ColumnResizingClientEvents ColumnResized="whdgSolicitudes_ColumnResized" />
                        </ig:ColumnResizing>
                        <ig:Sorting Enabled="true" SortingMode="Multi">
                            <SortingClientEvents ColumnSorting="whdgSolicitudes_Sorting" ColumnSorted="whdgSolicitudes_Sorted" />
                        </ig:Sorting>
                        <ig:ColumnMoving Enabled="true">
                            <ColumnMovingClientEvents HeaderDropped="whdgSolicitudes_HeaderDropped" />
                        </ig:ColumnMoving>
                        <ig:Paging Enabled="true" PagerAppearance="Top">
                            <PagerTemplate>
                                <div class="CabeceraBotones" style="text-align: left;">
                                    <div style="display: inline-block; width: 40%; margin: 0.5em 0em; vertical-align: middle;">
                                        <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" Style="margin-left: 2em; vertical-align: bottom;" />
                                        <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" Style="margin-left: 0.4em; vertical-align: bottom;" />
                                        <asp:Label ID="lblPage" runat="server" Style="margin-left: 0.6em;"></asp:Label>
                                        <asp:DropDownList ID="PagerPageList" runat="server" Style="margin-left: 0.4em;" onchange="return IndexChanged();"></asp:DropDownList>
                                        <asp:Label ID="lblOF" runat="server" Style="margin-left: 0.6em;"></asp:Label>
                                        <asp:Label ID="lblCount" runat="server" Style="margin-left: 0.5em;"></asp:Label>
                                        <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" Style="margin-left: 0.6em; vertical-align: bottom;" />
                                        <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" Style="margin-left: 0.4em; vertical-align: bottom;" />
                                        <div id="OptionInfoPedidosProcesos" style="float: right;">
                                            <asp:CheckBox runat="server" ID="chkInfoPedidosProcesos" />
                                        </div>
                                    </div>
                                    <div style="display: inline-block; width: 60%; vertical-align: middle;">
                                        <div onclick="ExportarExcel()" class="botonDerechaCabecera" style="float: right;">
                                            <asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
                                            <asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" Style="line-height: 2em; margin-left: 3px;"></asp:Label>
                                        </div>                                        
                                    </div>
                                </div>
                            </PagerTemplate>
                        </ig:Paging>
                    </Behaviors>
                </ig:WebHierarchicalDataGrid>
            </ContentTemplate>
        </asp:UpdatePanel>        
        <div id="divCargandoSolicitudes" style="display: none; text-align: center;">
            <asp:Image runat="server" ImageUrl="~/Images/cargando.gif" />
        </div>

        <div id="panelDetalleTextoLargo" class="popupCN" style="display: none; width: 40em; position: absolute; z-index: 1002;">
            <div class="PopUp_Header_Background" style="margin: 0em; padding: 0.5em;">
                <span id="lblCampoInfo" class="Texto16 TextoClaro Negrita" style="margin-left: 0.5em;"></span>
            </div>
            <div class="bordes" style="height: 22em; margin: 1.5em; padding: 0.2em; overflow-y: auto; max-height: 30em;">
                <span id="lblCampoValor" class="Texto12"></span>
            </div>
            <div style="height: 2em; text-align: center;">
                <span id="btnCerrarInfoTextoLargo" class="botonRedondeado"></span>
            </div>
        </div>
        
        <fsn:FSNPanelInfo ID="FSNPanelDatosUsuario" runat="server" ServicePath="~/Consultas.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="3" />
        <fsn:FSNPanelInfo ID="FSNPanelDatosPersona" runat="server" ServicePath="~/Consultas.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="2" />
        <fsn:FSNPanelInfo ID="FSNPanelDatosProveedor" runat="server" ServicePath="~/Consultas.asmx" ServiceMethod="Obtener_DatosProveedor" TipoDetalle="1" />

        <div id="pnlFondoCargando" class="FondoModal" style="position: absolute; z-index: 1001; margin: auto; width: 100%; top: 0; left: 0;"></div>
        <div id="pnlCargando" class="updateProgress" style="z-index: 1002;">
            <div style="position: relative; top: 30%; text-align: center;">
                <asp:Image ID="ImgProgress" runat="server" SkinID="ImgProgress" />
                <asp:Label ID="lblCargandoDatos" runat="server" ForeColor="Black" Text=""></asp:Label>
            </div>
        </div>   

        <asp:HiddenField runat="server" ID="bActivadoFSAL" ClientIDMode="Static" Value="0" />
        <asp:HiddenField runat="server" ID="bEnviadoFSAL8Inicial" ClientIDMode="Static" Value="0" />
        <asp:HiddenField runat="server" ID="sIdRegistroFSAL1" ClientIDMode="Static" />        
    </form>   
</body>
</html>
