Imports Infragistics.Web.UI.GridControls

Namespace Fullstep.PMPortalWeb
    Partial Class comentariosSolicitud
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private m_sIdiAcciones(2) As String
        Private m_sIdiParalelas As String
        Private oInstancia As Fullstep.PMPortalServer.Instancia
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User
            Dim lCiaComp As Long = IdCiaComp
            Dim sCodContrato As String
            Dim sNumFactura As String = String.Empty
            oUser = Session("FS_Portal_User")

            Dim sIdi As String = oUser.Idioma
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If

            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSPMServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.ComentariosSolic, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            oInstancia = FSPMServer.Get_Instancia
            oInstancia.ID = Request("Instancia")
            oInstancia.Load(lCiaComp, sIdi, True)
            oInstancia.Solicitud.Load(lCiaComp, sIdi)

            If Request("NumFactura") <> Nothing Then
                sNumFactura = Request("NumFactura")
            End If
            hidNumFactura.Value = sNumFactura

            If Request("Codigo") <> Nothing Then
                sCodContrato = Request("Codigo")
            Else
                sCodContrato = ""
            End If
            hidCodContrato.Value = sCodContrato

            If Not oUser.VerDetallePersona(Request("Instancia"), lCiaComp).Rows(0)("VER_DETALLE_PER") Then
                lblPeticionario.Visible = False
                lblPeticionarioBD.Visible = False
            End If

            txtInstancia.Value = oInstancia.ID

            m_sIdiAcciones(1) = oTextos.Rows(0).Item(1)  'Traslado
            m_sIdiAcciones(2) = oTextos.Rows(1).Item(1)  'Devoluci�n
            lblCerrar.InnerText = oTextos.Rows(3).Item(1) 'Cerrar
            lblFlujo.Text = oTextos.Rows(4).Item(1) 'Pulse aqu� para ver el flujo de trabajo en modo gr�fico
            m_sIdiParalelas = oTextos.Rows(5).Item(1) '�Etapas en paralelo!

            'Cabecera:
            If sCodContrato <> "" Then
                lblId.Text = oTextos.Rows(17).Item(1)
                lblIdBD.Text = sCodContrato
            ElseIf sNumFactura <> String.Empty Then
                lblId.Text = oTextos.Rows(18).Item(1) & ":"
                lblIdBD.Text = sNumFactura
            Else
                lblId.Text = oTextos.Rows(6).Item(1)
                lblIdBD.Text = oInstancia.ID
            End If

            lblFecha.Text = oTextos.Rows(7).Item(1)
            lblFechaBD.Text = modUtilidades.FormatDate(oInstancia.FechaAlta, oUser.DateFormat)
            lblNombre.Text = oTextos.Rows(8).Item(1)
            lblNombreBD.Text = oInstancia.Solicitud.Den(sIdi)
            lblPeticionario.Text = oTextos.Rows(9).Item(1)
            lblPeticionarioBD.Text = oInstancia.NombrePeticionario

            'Rellena la grid de hist�rico de estados:
            oInstancia.CargarHistoricoEstados(lCiaComp, sIdi)
            If oInstancia.HistoricoEst.Tables.Count > 0 Then
                whgHistorico.DataSource = oInstancia.HistoricoEst
                whgHistorico.DataMember = ""
                ConfigurarGrid()
                'Idiomas en las caption de la grid:
                With whgHistorico
                    .Columns("DEN_BLOQUE1").Header.Text = oTextos.Rows(10).Item(1) 'Etapa
                    .Columns("ROL").Header.Text = oTextos.Rows(11).Item(1) 'Participante
                    .Columns("USUARIO").Header.Text = oTextos.Rows(12).Item(1) 'Usuario
                    .Columns("ACCION").Header.Text = oTextos.Rows(13).Item(1) 'Acci�n realizada
                    .Columns("FECHA").Header.Text = oTextos.Rows(14).Item(1) 'Fecha
                    .Columns("DEN_DESTINO").Header.Text = oTextos.Rows(15).Item(1) 'Destino
                    .Columns("COMENT").Header.Text = oTextos.Rows(16).Item(1) 'Comentarios
                End With
                With whgHistorico.GridView
                    .Columns("DEN_BLOQUE1").Header.Text = oTextos.Rows(10).Item(1) 'Etapa
                    .Columns("ROL").Header.Text = oTextos.Rows(11).Item(1) 'Participante
                    .Columns("USUARIO").Header.Text = oTextos.Rows(12).Item(1) 'Usuario
                    .Columns("ACCION").Header.Text = oTextos.Rows(13).Item(1) 'Acci�n realizada
                    .Columns("FECHA").Header.Text = oTextos.Rows(14).Item(1) 'Fecha
                    .Columns("DEN_DESTINO").Header.Text = oTextos.Rows(15).Item(1) 'Destino
                    .Columns("COMENT").Header.Text = oTextos.Rows(16).Item(1) 'Comentarios
                End With

                oTextos = Nothing
                whgHistorico.DataBind()
            End If
        End Sub

        ''' <summary>
        ''' Cada vez que se carga una fila del webhierarchicaldatagrid. As� ponemos los enlaces, textos, estilos,...
        ''' </summary>
        Private Sub whgHistorico_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whgHistorico.InitializeRow

            If DBNullToInt(e.Row.DataItem.Item.Row.Item("ETAPAS_PARALELO")) > 1 Then
                e.Row.Items(whgHistorico.Columns("DEN_DESTINO").Index).Text = m_sIdiParalelas
            Else
                e.Row.Items(whgHistorico.Columns("DEN_DESTINO").Index).Text = DBNullToStr(e.Row.DataItem.Item.Row.Item("DEN_BLOQUE2"))
            End If

            Select Case DBNullToInt(e.Row.DataItem.Item.Row.Item("ID_OTRA_ACCION"))
                Case Fullstep.PMPortalServer.TipoAccionSolicitud.Traslado
                    e.Row.Items(whgHistorico.Columns("ACCION").Index).Text = m_sIdiAcciones(1)
                    e.Row.Items(whgHistorico.Columns("DEN_DESTINO").Index).Text = DBNullToStr(e.Row.DataItem.Item.Row.Item("DESTINATARIO"))
                Case Fullstep.PMPortalServer.TipoAccionSolicitud.Devolucion
                    e.Row.Items(whgHistorico.Columns("ACCION").Index).Text = m_sIdiAcciones(2)
                    e.Row.Items(whgHistorico.Columns("DEN_DESTINO").Index).Text = DBNullToStr(e.Row.DataItem.Item.Row.Item("DESTINATARIO"))
            End Select

            'Estilos de las filas:
            If DBNullToInt(e.Row.DataItem.Item.Row.Item("ESTADO")) = 1 Then
                e.Row.CssClass = "ugfilatablaHistActual"
            ElseIf DBNullToInt(e.Row.DataItem.Item.Row.Item("ESTADO")) > 1 Then
                'Si ya est� tramitada o rechazada la fila se ver� en azul, sino en blanco:
                e.Row.CssClass = "ugfilatablaHist"
            End If

            'La columna del comentario ser� un hiperv�nculo:
            If DBNullToInt(Len(e.Row.Items(whgHistorico.Columns("COMENT").Index).Text)) > 25 Then
                e.Row.Items(whgHistorico.Columns("COMENT").Index).Text = Left(e.Row.Items(whgHistorico.Columns("COMENT").Index).Text, 25)
            End If
            If DBNullToStr(e.Row.Items(whgHistorico.Columns("COMENT").Index).Text) <> Nothing Then
                e.Row.Items(whgHistorico.Columns("COMENT").Index).Text = "<a onclick=""VerComentario('" & e.Row.Items(whgHistorico.Columns("ID").Index).Text & "');return false;"">" & e.Row.Items(whgHistorico.Columns("COMENT").Index).Text & "</a>"
                e.Row.Items(whgHistorico.Columns("COMENT").Index).CssClass = "TablaLink"
            End If

        End Sub
        Private Sub AnyadirColumna(ByVal fieldname As String)
            Dim unBoundField As New UnboundField
            unBoundField.Key = fieldname
            unBoundField.Header.Text = fieldname

            whgHistorico.Columns.Add(unBoundField)
        End Sub
        Private Sub ConfigurarGrid()
            Dim oUser As Fullstep.PMPortalServer.User
            oUser = Session("FS_Portal_User")

            Dim lCiaComp As Long = IdCiaComp
            If Not oUser.VerDetallePersona(Request("Instancia"), lCiaComp).Rows(0)("VER_DETALLE_PER") Then
                whgHistorico.GridView.Columns.FromKey("USUARIO").Hidden = True
            End If
            whgHistorico.Columns("DEN_DESTINO").VisibleIndex = 9

            'formato de la fecha:
            whgHistorico.Columns("FECHA").FormatValue(oUser.DateFormat.ShortDatePattern)

            'Redimensiona las columnas:
            whgHistorico.Columns("DEN_BLOQUE1").Width = Unit.Percentage(15)
            whgHistorico.Columns("ROL").Width = Unit.Percentage(10)
            whgHistorico.Columns("USUARIO").Width = Unit.Percentage(15)
            whgHistorico.Columns("ACCION").Width = Unit.Percentage(10)
            whgHistorico.Columns("FECHA").Width = Unit.Percentage(10)
            whgHistorico.Columns("DEN_BLOQUE2").Width = Unit.Percentage(15)
            whgHistorico.Columns("COMENT").Width = Unit.Percentage(15)
        End Sub
    End Class
End Namespace