<%@ Page Language="vb" AutoEventWireup="false" Codebehind="flowDiagram.aspx.vb" Inherits="Fullstep.PMPortalWeb._default"%>
<%@ Register TagPrefix="cc1" Namespace="Syncfusion.Web.UI.WebControls.Diagram" Assembly="Syncfusion.Diagram.Web, Version=4.102.0.62, Culture=neutral, PublicKeyToken=3d67ed1f87d44c89" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
		<title>flowDiagram</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../../common/estilo.asp" type="text/css" rel="stylesheet">
		<script src="../../../common/formatos.js"></script>
		<script language="javascript" id="clientEventHandlersJS">
function window_onload() {
	
	
}

function cmdImpExp_onclick() {
	window.open('flowDiagramExport.aspx?Instancia='+document.forms["frmDetalle"].elements["Instancia"].value,'_new','fullscreen=no,height=200,width=500,location=no,menubar=no,resizable=no,scrollbars=no,status=yes,titlebar=yes,toolbar=no,left=200,top=200');
}

function ResizeDiv()
{
    document.getElementById("divFlujo").style.top = (parseInt(document.getElementById("Table2").style.top) + parseInt(document.getElementById("Table2").height) + 20) + ' px'
    document.getElementById("divFlujo").style.height = (parseInt(document.body.clientHeight) - parseInt(document.getElementById("divFlujo").style.top)) + ' px'
}

window.onresize = ResizeDiv;

	</script>
	</head>
	<body MS_POSITIONING="GridLayout" onload="ResizeDiv();">
		<form id="frmDetalle" name="frmDetalle"  method="post" runat="server">
			<div id="divFlujo" style="LEFT: 8px; OVERFLOW: auto; WIDTH: 100%; POSITION: absolute; TOP: 104px; HEIGHT: 100%">
				<cc1:diagramwebcontrol id="dwc" style="Z-INDEX: 100" runat="server" Height="1px" Width="1px" SaveToSessionState="False"
					EnableViewState="False"></cc1:diagramwebcontrol>
			</div>
			<INPUT id="txtPeticionario" style="Z-INDEX: 104; LEFT: 504px; WIDTH: 40px; POSITION: absolute; TOP: 168px; HEIGHT: 22px"
				type="hidden" size="1" name="txtIdTipo" runat="server"><INPUT id="Version" style="Z-INDEX: 103; LEFT: 504px; WIDTH: 40px; POSITION: absolute; TOP: 136px; HEIGHT: 22px"
				type="hidden" size="1" name="Version" runat="server"><INPUT id="Instancia" style="Z-INDEX: 102; LEFT: 504px; WIDTH: 40px; POSITION: absolute; TOP: 104px; HEIGHT: 22px"
				type="hidden" size="1" name="Instancia" runat="server">
			<TABLE class="cabeceraSolicitud" id="Table2" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px; HEIGHT: 70px"
				height="94" cellSpacing="3" cellPadding="3" width="100%" border="0">
				<TR>
					<TD style="WIDTH: 92px; HEIGHT: 2px"><asp:label id="lblId" runat="server" Width="100%" CssClass="captionBlue"></asp:label></TD>
					<TD style="WIDTH: 394px; HEIGHT: 2px"><asp:label id="lblBDId" runat="server" Width="100%" CssClass="captionDarkBlue"></asp:label></TD>
					<TD style="WIDTH: 25px; HEIGHT: 2px"><A id="cmdDetalleSolic" style="CURSOR: hand" onclick="javascript:DetalleTipoSolicitud()"
							name="cmdDetalleSolic"></A></TD>
					<TD style="WIDTH: 140px; HEIGHT: 2px"><asp:label id="lblFecha" runat="server" Width="100%" CssClass="captionBlue"></asp:label></TD>
					<TD style="WIDTH: 202px; HEIGHT: 2px"><asp:label id="lblBDFecha" runat="server" Width="100%" CssClass="captionDarkBlue"></asp:label></TD>
					<TD style="WIDTH: 86px; HEIGHT: 2px"></TD>
					<TD style="HEIGHT: 2px"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 92px; HEIGHT: 12px"><asp:label id="lblTipo" runat="server" Width="100%" CssClass="captionBlue"></asp:label></TD>
					<TD style="WIDTH: 394px; HEIGHT: 12px"><asp:label id="lblBDTipo" runat="server" Width="100%" CssClass="captionDarkBlue"></asp:label></TD>
					<TD></TD>
					<TD style="WIDTH: 140px; HEIGHT: 12px"></TD>
					<TD style="WIDTH: 202px; HEIGHT: 12px"></TD>
					<TD style="WIDTH: 86px; HEIGHT: 12px"></TD>
					<TD style="HEIGHT: 12px"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 92px"><asp:label id="lblPeticionario" runat="server" Width="100%" CssClass="captionBlue"></asp:label></TD>
					<TD style="WIDTH: 394px"><asp:label id="lblBDPeticionario" runat="server" Width="100%" CssClass="captionDarkBlue"></asp:label></TD>
					<TD style="WIDTH: 25px"></TD>
					<TD align="right" colSpan="2"></TD>
					<TD style="WIDTH: 86px"></TD>
					<TD><INPUT id="cmdImpExp" type="button" value="Impr./Exp." runat="server" name="cmdImpExp"
							class="boton" language="javascript" onclick="return cmdImpExp_onclick()"></TD>
				</TR>
			</TABLE>
			<asp:TextBox id="txtPath" style="Z-INDEX: 106; LEFT: 504px; POSITION: absolute; TOP: 208px" runat="server"
				Width="28px" Visible="False"></asp:TextBox>
		</form>
	</body>
</html>
