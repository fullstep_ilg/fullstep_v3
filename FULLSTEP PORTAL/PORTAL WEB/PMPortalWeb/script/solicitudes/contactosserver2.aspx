<%@ Page Language="vb" AutoEventWireup="false" Codebehind="contactosserver2.aspx.vb" Inherits="Fullstep.PMPortalWeb.contactosserver2"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../../common/estilo.asp" type="text/css" rel="stylesheet">
		<script src="../../../common/formatos.js"></script>
		<SCRIPT type="text/javascript"><!--
		
function uwgArticulos_CellClickHandler(gridName, cellId, button)
{
	var row = igtbl_getRowById(cellId); 
	var cell = row.getCell(0); 
	var sNombre= cell.getValue()
	cell = row.getCell(1); 
	var sEmail = cell.getValue()
	cell = row.getCell(3);
	var lIdContacto = cell.getValue()
	
	window.opener.actualizarContacto(sNombre,sEmail,lIdContacto)
	window.close()
}
--></SCRIPT>
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:label id="Label1" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 8px" runat="server"
				CssClass="parrafo">Seleccione un contacto</asp:label>
			<igtbl:ultrawebgrid id="uwgArticulos" runat="server" Width="100%" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 32px">
				<DisplayLayout Name="uwgArticulos" BorderCollapseDefault="Separate" Version="4.00" RowHeightDefault="20px">
					<AddNewBox>
						<Style BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px"> 
							<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"> </BorderDetails> 
						</Style>
					</AddNewBox>
					<Pager>
						<Style BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px"> 
							<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"> </BorderDetails> 
						</Style>
					</Pager>
					<FrameStyle Width="100%" Font-Size="8pt" Font-Names="Verdana" Height="256px"></FrameStyle>
					<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
						<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
					</FooterStyleDefault>
					<ClientSideEvents CellClickHandler="uwgArticulos_CellClickHandler"></ClientSideEvents>
					<EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
				</DisplayLayout>
				<Bands>
					<igtbl:UltraGridBand FixedHeaderIndicator="None" CellClickAction="RowSelect" RowSelectors="No">
						<HeaderStyle CssClass="cabecera"></HeaderStyle>
						<RowAlternateStyle CssClass="ugfilaalttabla"></RowAlternateStyle>
						<RowStyle CssClass="ugfilatabla"></RowStyle>
					</igtbl:UltraGridBand>
				</Bands>
			</igtbl:ultrawebgrid>
		</form>
	</body>
</html>
