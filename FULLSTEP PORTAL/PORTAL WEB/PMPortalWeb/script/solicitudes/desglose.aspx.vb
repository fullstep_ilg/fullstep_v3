Namespace Fullstep.PMPortalWeb
    Partial Class segdesglose
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim idCampo As Integer = Request("campo")
            Dim sInput As String = Request("Input")
            Dim lInstancia As Long = Request("instancia")
            Dim lSolicitud As Long = IIf(Request("Solicitud") <> "", Request("Solicitud"), 0)
            Dim lCiaComp As Long = IdCiaComp

            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Desglose

            INPUTDESGLOSE.Value = sInput
            Instancia.Value = lInstancia

            Dim oCampo As Fullstep.PMPortalServer.Campo
            oCampo = FSPMServer.Get_Campo()
            oCampo.Id = idCampo
            If String.IsNullOrEmpty(Request("Solicitud")) Then
                oCampo.LoadInst(lCiaComp, lInstancia, Idioma)
            Else 'Alta Facturas
                instanciaPortF.Value = lSolicitud * (-1)
                Solicitud.Value = lSolicitud
                oCampo.Load(lCiaComp, Idioma, lSolicitud)
            End If

            lblTitulo.Text = Textos(0)
            lblTituloData.Text = oCampo.DenSolicitud(Idioma)
            lblSubTitulo.Text = oCampo.DenGrupo(Idioma) + ";" + oCampo.Den(Idioma)

            If Request("BotonCalcular") <> Nothing Then
                If Request("BotonCalcular") = 1 Then
                    cmdCalcular.Visible = True
                Else
                    cmdCalcular.Visible = False
                End If
            End If

            Dim oucdesglose As Fullstep.PMPortalWeb.desgloseControl
            oucdesglose = Me.FindControl("desglose")
            With oucdesglose
                .Version = Request("Version")
                .TipoSolicitud = oCampo.TipoSolicitud
                .InstanciaMoneda = oCampo.InstanciaMoneda
            End With

            InsertarCalendario()
            cmdGuardar.Value = Textos(2)
            cmdGuardar.Attributes.Add("onclick", "return actualizarCampoDesgloseYCierre()")
            cmdCalcular.Value = Textos(3)
        End Sub
        Private Sub InsertarCalendario()
            Dim SharedCalendar As New Infragistics.WebUI.WebSchedule.WebCalendar
            With SharedCalendar
                .ClientSideEvents.InitializeCalendar = "initCalendarEvent"
                .ClientSideEvents.DateClicked = "calendarDateClickedEvent"
                With .Layout
                    .NextMonthImageUrl = "ig_cal_grayN.gif"
                    .ShowTitle = "False"
                    .PrevMonthImageUrl = "ig_cal_grayP.gif"
                    .FooterFormat = "Today: {0:d}"
                    .FooterStyle.CssClass = "igCalendarFooterStyle"
                    .TodayDayStyle.CssClass = "igCalendarSelectedDayStyle"
                    .OtherMonthDayStyle.CssClass = "igCalendarOtherMonthDayStyle"
                    .CalendarStyle.CssClass = "igCalendarStyle"
                    .TodayDayStyle.CssClass = "igCalendarTodayDayStyle"
                    .DayHeaderStyle.CssClass = "igCalendarDayHeaderStyle"
                    .TitleStyle.CssClass = "igCalendarTitleStyle"
                End With
            End With
            Me.FindControl("frmAlta").Controls.Add(SharedCalendar)
        End Sub
    End Class
End Namespace