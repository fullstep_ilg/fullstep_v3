﻿Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web.Caching
Imports System.Web.Script.Serialization
Imports Fullstep.PMPortalServer
Imports Infragistics.Web.UI.GridControls

Namespace Fullstep.PMPortalWeb
    Public Class VisorSolicitudes
        Inherits FSPMPage

		Dim iTipoVisor As FSNLibrary.TipoVisor = FSNLibrary.TipoVisor.Solicitudes
		Dim MostrarOtras As Boolean = False
		Private Shared _camposArchivo As New Dictionary(Of String, Integer)
        Private Shared _camposEditor As New Dictionary(Of String, Integer)
        Private Shared _camposTexto As New Dictionary(Of String, Integer)
        Private Const VALOR_ESTADO_OTRAS As String = "1000#0#2#6#8#100#101#102#103#104"
        Dim InstanciasAprobar As New Dictionary(Of String, String)
        Dim InstanciasRechazar As New Dictionary(Of String, String)

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            'CARGAMOS LOS SCRIPT NECESARIOS PARA CARGAR LA PAGINA 
            ScriptManager1.EnablePageMethods = True
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.min.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.ui.min.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery.tmpl.min.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/jquery-migrate.min.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jsUtilities.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.multiselect.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.multiselect.filter.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.numeric.min.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.numericFormatted.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/js/jquery/plugins/jquery.inputmask.bundle.min.js"))
            ScriptManager1.CompositeScript.Scripts.Add(New ScriptReference("~/script/solicitudes/js/VisorSolicitudes.js"))

            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/Images/seguimiento.gif"

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "ruta") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ruta", "var ruta='" & ConfigurationManager.AppSettings("ruta") & "';", True)

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "ModalProgress") Then _
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModalProgress", "var ModalProgress='" & ConfigurationManager.AppSettings("ruta") & "';", True)

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)

			If Request.QueryString("TipoVisor") IsNot Nothing Then iTipoVisor = CInt(Request.QueryString("TipoVisor"))
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tipoVisor", "<script>var tipoVisor = '" & CInt(iTipoVisor) & "';</script>")

			If Request.QueryString("MostrarOtras") IsNot Nothing Then MostrarOtras = CBool(Request.QueryString("MostrarOtras"))
			Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "MostrarOtras", "<script>var MostrarOtras = " & (MostrarOtras OrElse iTipoVisor = TipoVisor.Facturas).ToString.ToLower & ";</script>")

			Establecer_Textos_Pantalla()

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumberFormatUsu") Then
                With FSPMUser
                    Dim sVariablesJavascript As String = "var UsuNumberDecimalSeparator = '" & .NumberFormat.NumberDecimalSeparator & "';"
                    sVariablesJavascript &= "var UsuNumberGroupSeparator='" & .NumberFormat.NumberGroupSeparator & "';"
                    sVariablesJavascript &= "var UsuNumberNumDecimals='" & .NumberFormat.NumberDecimalDigits & "';"
                    sVariablesJavascript &= "var UsuMask = '" & .DateFormat.ShortDatePattern & "';"
                    sVariablesJavascript &= "var UsuMaskSeparator='" & .DateFormat.DateSeparator & "';"
                    sVariablesJavascript &= "var UsuLanguageTag='" & .RefCultural & "';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumberFormatUsu", sVariablesJavascript, True)
                End With
            End If

            CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgExcel"), System.Web.UI.WebControls.Image).ImageUrl = System.Configuration.ConfigurationManager.AppSettings("ruta") & "/Images/Excel.png"

            If Request.QueryString("escenario") IsNot Nothing Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "escenarioVolver", "<script>var escenarioVolver = " & CType(Request.QueryString("escenario"), Integer) & ";</script>")

            If Request.QueryString("filtro") IsNot Nothing Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "filtroVolver", "<script>var filtroVolver = " & CType(Request.QueryString("filtro"), Integer) & ";</script>")

            If Request.QueryString("vista") IsNot Nothing Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "vistaVolver", "<script>var vistaVolver = " & CType(Request.QueryString("vista"), Integer) & ";</script>")

            If Request.QueryString("numpag") IsNot Nothing Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "numpag", "<script>var numpag = " & CType(Request.QueryString("numpag"), Integer) & ";</script>")

            Dim PersonaAnimationClientID As String = FSNPanelDatosPersona.AnimationClientID
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PersonaAnimationClientID", "<script>var PersonaAnimationClientID = '" & PersonaAnimationClientID & "'</script>")
            Dim PersonaDynamicPopulateClientID As String = FSNPanelDatosPersona.DynamicPopulateClientID
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PersonaDynamicPopulateClientID", "<script>var PersonaDynamicPopulateClientID = '" & PersonaDynamicPopulateClientID & "'</script>")
            Dim UsuarioAnimationClientID As String = FSNPanelDatosUsuario.AnimationClientID
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "UsuarioAnimationClientID", "<script>var UsuarioAnimationClientID = '" & UsuarioAnimationClientID & "'</script>")
            Dim UsuarioDynamicPopulateClientID As String = FSNPanelDatosUsuario.DynamicPopulateClientID
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "UsuarioDynamicPopulateClientID", "<script>var UsuarioDynamicPopulateClientID = '" & UsuarioDynamicPopulateClientID & "'</script>")
            Dim ProveedorAnimationClientID As String = FSNPanelDatosProveedor.AnimationClientID
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProveedorAnimationClientID", "<script>var ProveedorAnimationClientID = '" & ProveedorAnimationClientID & "'</script>")
            Dim ProveedorDynamicPopulateClientID As String = FSNPanelDatosProveedor.DynamicPopulateClientID
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ProveedorDynamicPopulateClientID", "<script>var ProveedorDynamicPopulateClientID = '" & ProveedorDynamicPopulateClientID & "'</script>")
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)
            End If

            With whdgSolicitudes
                .Behaviors.Paging.PageSize = ConfigurationManager.AppSettings("PaginacionSolicitudes")
                .GridView.Behaviors.Paging.PageSize = ConfigurationManager.AppSettings("PaginacionSolicitudes")
            End With

            If IsPostBack Then
                whdgSolicitudes.Visible = True
                Select Case Request("__EVENTTARGET")
                    Case btnRecargarGridSolicitudes.ClientID, btnPager.ClientID, "Excel"
                        HttpContext.Current.Session("chkProcesosPedidos") = Nothing
                        'Para cargar las solicitudes en el grid necesitamos el filtro que se aplica y la vista, ademas de saber si se ataca sobre todas las solicitudes
                        'o solo contra las de un tipo de solicitud/formulario. Dependiendo de que campos se desean ver obtendremos una subconsulta de las columnas necesarias
                        'para crear el grid. Tener en cuenta que necesitamos una tabla relacionada con los datos de desglose.
                        Dim bExportar As Boolean = False
                        If Request("__EVENTTARGET") = "Excel" Then bExportar = True

                        Dim serializer As New JavaScriptSerializer
                        Dim oInfoFiltroVista As Object = serializer.Deserialize(Of Object)(Request("__EVENTARGUMENT"))
                        Dim IdFormulario As Integer = serializer.Deserialize(Of Integer)(oInfoFiltroVista("IdFormulario"))
                        Dim oFiltro As EscenarioFiltro = serializer.Deserialize(Of EscenarioFiltro)(oInfoFiltroVista("Filtro"))
                        Dim oVista As EscenarioVista = serializer.Deserialize(Of EscenarioVista)(oInfoFiltroVista("Vista"))
                        Dim lSolicitudesVinculadas As String = serializer.Deserialize(Of String)(oInfoFiltroVista("SolicitudFormularioVinculados"))
                        If Request("__EVENTTARGET") = btnPager.ClientID Then
                            InstanciasAprobar = serializer.Deserialize(Of Dictionary(Of String, String))(oInfoFiltroVista("InstanciasAprobar"))
                            InstanciasRechazar = serializer.Deserialize(Of Dictionary(Of String, String))(oInfoFiltroVista("InstanciasRechazar"))
                        End If

                        Dim columNames() As String = {}
                        Dim columNamesDesglose() As String = {}
                        Dim ObtenerProcesos, ObtenerPedidos, VistaDesglose As Boolean
                        Dim bVaLaLinea As Boolean
                        Seleccionar_Columnas_Grid(oVista.Campos_Vista, columNames, columNamesDesglose, ObtenerProcesos, ObtenerPedidos, VistaDesglose, bVaLaLinea)
                        For Each oCampoTexto As Escenario_Campo In oVista.Campos_Vista
                            If (oCampoTexto.TipoCampoGS = 0 OrElse ({TiposDeDatos.TipoCampoGS.DescrBreve, TiposDeDatos.TipoCampoGS.DescrDetallada}.Contains(oCampoTexto.TipoCampoGS) AndAlso Not oCampoTexto.EsCampoGeneral)) _
                                    AndAlso {TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoCorto}.Contains(oCampoTexto.TipoCampo) Then
                                _camposTexto(oCampoTexto.Denominacion_BD) = oCampoTexto.Id
                            End If
                        Next
                        Dim oPageNumber As Integer = serializer.Deserialize(Of Integer)(oInfoFiltroVista("Page"))

                        If bExportar Then
                            Configurar_Columnas_Grid_Solicitudes_Exportar(oVista.Campos_Vista, oVista.Orden)
                        ElseIf Not Request("__EVENTTARGET") = btnPager.ClientID Then
                            Configurar_Columnas_Grid_Solicitudes(oVista.Campos_Vista, oVista.Orden)
                        End If

                        Dim cInstancias As Instancias
                        cInstancias = FSPMServer.Get_Instancias
                        Dim AbtasUsted, Participo, Pendientes, PendientesDevolucion, Trasladadas, ObservadorSustitucion As Nullable(Of Boolean)
                        Dim FiltroDesglose As Boolean
                        Dim sentenciaWhere As String = Generar_SentenciaWhere_Filtro(oFiltro.FormulaCondiciones, FiltroDesglose)
                        Dim sentenciaWhereCamposGenerales As String
                        If oFiltro.FormulaAvanzada Then
                            sentenciaWhereCamposGenerales = ""
                            AbtasUsted = True
                            Participo = True
                            Pendientes = True
                            PendientesDevolucion = True
                            Trasladadas = True
                            ObservadorSustitucion = True
                        Else
                            sentenciaWhereCamposGenerales = Generar_SentenciaWhere_CamposGenerales(oFiltro.FormulaCondiciones, AbtasUsted, Participo, Pendientes, PendientesDevolucion, Trasladadas, ObservadorSustitucion, lSolicitudesVinculadas)
                        End If
						Dim ds As DataSet = cInstancias.DevolverSolicitudes(FSPMUser.CiaComp, FSPMUser.CodProveGS, FSPMUser.IdContacto, FSPMUser.Idioma, IdFormulario,
											sentenciaWhere, oVista.Orden, sentenciaWhereCamposGenerales, oPageNumber, ConfigurationManager.AppSettings("PaginacionSolicitudes"),
											BuscarProveedorArticulo(oFiltro.FormulaCondiciones, oVista.Campos_Vista), AbtasUsted, Participo, Pendientes,
											PendientesDevolucion, Trasladadas, MostrarOtras, FiltroDesglose, VistaDesglose, iTipoVisor)
						Dim view As New DataView(ds.Tables(0))
                        Dim dsResultado As New DataSet
                        Dim dtSolicitudes As DataTable = view.ToTable("SOLICITUDES", True, columNames)
                        Dim dtSolicitudesClone As DataTable = dtSolicitudes.Clone
                        For Each columna As Escenario_Campo In oVista.Campos_Vista
                            If Not (columna.Id = CamposGeneralesVisor.IDENTIFICADOR And columna.EsCampoGeneral) AndAlso Not columna.EsCampoDesglose _
                            AndAlso Not {TiposDeDatos.TipoCampoGS.Empresa, TiposDeDatos.TipoCampoGS.Almacen}.Contains(columna.TipoCampoGS) _
                            AndAlso columna.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico Then
                                dtSolicitudesClone.Columns(columna.Denominacion_BD).DataType = GetType(System.Double)
                            End If
                        Next
                        For Each row As DataRow In dtSolicitudes.Rows
                            dtSolicitudesClone.ImportRow(row)
                        Next
                        dsResultado.Tables.Add(dtSolicitudesClone)
                        If Not IdFormulario = 0 AndAlso (columNamesDesglose.Count > 3 OrElse bVaLaLinea) Then
                            Dim listaDesgloses As List(Of String) = columNamesDesglose.Where(Function(x) Split(x, "_").Length = 4).Select(Function(x) Split(x, "_")(1)).Distinct().ToList
                            Dim query1 = From Datos In ds.Tables(0).AsEnumerable()
                                         Where (listaDesgloses.Contains(Datos.Item("DESGLOSE").ToString))
                                         Select Datos Distinct
                            Dim dtDesglose As DataTable = Nothing
                            If query1.Any Then
                                dtDesglose = query1.CopyToDataTable
                            End If
                            If dtDesglose IsNot Nothing Then
                                Dim viewDesglose As New DataView(dtDesglose)
                                dsResultado.Tables.Add(viewDesglose.ToTable("DESGLOSE", True, columNamesDesglose))

                                Dim parentColsVista(0) As DataColumn
                                Dim childColsVista(0) As DataColumn
                                parentColsVista(0) = dsResultado.Tables("SOLICITUDES").Columns("ID")
                                childColsVista(0) = dsResultado.Tables("DESGLOSE").Columns("FORM_INSTANCIA")
                                dsResultado.Relations.Add("SOLICITUD_DESGLOSE", parentColsVista, childColsVista, False)
                            End If
                        End If
                        If Pendientes Then
                            dsResultado.Tables.Add(New DataView(ds.Tables(1)).ToTable("SOLICITUDES_APROBAR", True, New String() {"ID", "ACCION_APROBAR", "BLOQUE"}))
                            dsResultado.Tables.Add(New DataView(ds.Tables(2)).ToTable("SOLICITUDES_RECHAZAR", True, New String() {"ID", "ACCION_RECHAZAR", "BLOQUE"}))
                        End If

                        If Not bExportar Then
                            Me.InsertarEnCache("dsSolicitudes_" & FSPMUser.Cod, dsResultado, CacheItemPriority.BelowNormal)
                            With whdgSolicitudes
                                .Rows.Clear()
                                .GridView.ClearDataSource()
                                .DataKeyFields = "ID,ROWNUMBER"
                                .DataMember = "SOLICITUDES"
                                .DataSource = dsResultado
                                .GridView.DataSource = dsResultado
                                Establecer_Orden_Columnas_Grid_Solicitudes(oVista.Orden)
                                .DataBind()
                            End With
                            Dim numSolicitudes As Integer
                            If ds.Tables(0).Rows.Count = 0 Then
                                numSolicitudes = 0
                            Else
                                numSolicitudes = ds.Tables(0).Rows(0)("TOTALSOLICITUDES")
                            End If
                            iNumeroSolicitudesFiltroSeleccionado.Value = numSolicitudes
                            PaginadorGrid((numSolicitudes \ ConfigurationManager.AppSettings("PaginacionSolicitudes")) + (IIf(numSolicitudes Mod ConfigurationManager.AppSettings("PaginacionSolicitudes") = 0, 0, 1)), oPageNumber)
                            updSolicitudes.Update()
                        Else
                            InsertarEnCache("dsSolicitudesExportar_" & FSPMUser.Cod, dsResultado, CacheItemPriority.BelowNormal)
                            With whdgExportacion
                                .Rows.Clear()
                                .GridView.ClearDataSource()
                                .DataKeyFields = "ID,ROWNUMBER"
                                .DataMember = "SOLICITUDES"
                                .DataSource = dsResultado
                                .GridView.DataSource = dsResultado
                                .InitialDataBindDepth = -1
                                .DataBind()
                            End With
                            ExportarExcel()
                        End If
                    Case Else
                        With whdgSolicitudes
                            .Rows.Clear()
                            .GridView.ClearDataSource()
                            .DataKeyFields = "ID,ROWNUMBER"
                            .DataMember = "SOLICITUDES"
                            .DataSource = CType(Cache("dsSolicitudes_" & FSPMUser.Cod), DataSet)
                            .GridView.DataSource = CType(Cache("dsSolicitudes_" & FSPMUser.Cod), DataSet)
                            .DataBind()
                        End With
                        updSolicitudes.Update()
                End Select
            End If
        End Sub
        Private Sub Establecer_Textos_Pantalla()
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorSolicitudes

            Select Case iTipoVisor
                Case TipoVisor.Facturas
                    FSNPageHeader.TituloCabecera = Textos(95)
                    lblSolicitudesPendientes.Text = Textos(0)
                    lnkSolicitudesPendientes.Text = Textos(1)
                Case TipoVisor.SolicitudesQA
                    FSNPageHeader.TituloCabecera = Textos(130)
                    lblSolicitudesPendientes.Text = Textos(137)
                    lnkSolicitudesPendientes.Text = Textos(138)
                Case TipoVisor.Encuestas
                    FSNPageHeader.TituloCabecera = Textos(131)
                    lblSolicitudesPendientes.Text = Textos(135)
                    lnkSolicitudesPendientes.Text = Textos(136)
                Case Else
                    FSNPageHeader.TituloCabecera = Textos(132)
                    lblSolicitudesPendientes.Text = Textos(133)
                    lnkSolicitudesPendientes.Text = Textos(134)
            End Select

            lblEscenariosTituloBarra.Text = Textos(2)
            lblEscenariosTituloSeleccionado.Text = Textos(3) & ":"
            lblEscenarioFiltrosTituloBarra.Text = Textos(4)
            lblCargandoDatos.Text = Textos(13)
            lblBotonBuscar.Text = Textos(128)
            FSNPanelDatosUsuario.Titulo = Textos(25)
            FSNPanelDatosUsuario.SubTitulo = Textos(129)
            FSNPanelDatosProveedor.Titulo = Textos(34)
            FSNPanelDatosProveedor.SubTitulo = Textos(129)

            CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), WebControls.Image).ToolTip = Textos(5)
            CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), WebControls.Image).ToolTip = Textos(6)
            CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(9)
            CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(10)
            CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), WebControls.Image).ToolTip = Textos(7)
            CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), WebControls.Image).ToolTip = Textos(8)
            CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblExcel"), Label).Text = Textos(11)
            whdgSolicitudes.GroupingSettings.EmptyGroupAreaText = Textos(12)

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
                Dim sVariableJavascriptTextosPantalla As String = "var TextosPantalla = new Array();"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[0]='" & JSText(Textos(14)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[1]='" & JSText(Textos(15)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[2]='" & JSText(Textos(58)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[3]='" & JSText(Textos(59)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[4]='" & JSText(Textos(60)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[5]='" & JSText(Textos(61)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[6]='" & JSText(Textos(62)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[7]='" & JSText(Textos(63)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[8]='" & JSText(Textos(64)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[9]='" & JSText(Textos(65)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[10]='" & JSText(Textos(66)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[11]='" & JSText(Textos(67)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[12]='" & JSText(Textos(68)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[13]='" & JSText(Textos(69)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[14]='" & JSText(Textos(70)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[15]='" & JSText(Textos(71)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[16]='" & JSText(Textos(72)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[17]='" & JSText(Textos(73)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[18]='" & JSText(Textos(74)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[19]='" & JSText(Textos(75)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[20]='" & JSText(Textos(76)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[21]='" & JSText(Textos(77)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[22]='" & JSText(Textos(78)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[23]='" & JSText(Textos(79)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[24]='" & JSText(Textos(80)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[25]='" & JSText(Textos(81)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[26]='" & JSText(Textos(82)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[27]='" & JSText(Textos(83)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[28]='" & JSText(Textos(84)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[29]='" & JSText(Textos(85)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[30]='" & JSText(Textos(86)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[31]='" & JSText(Textos(87)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[32]='" & JSText(Textos(88)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[33]='" & JSText(Textos(89)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[34]='" & JSText(Textos(90)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[35]='" & JSText(Textos(91)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[36]='" & JSText(Textos(92)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[37]='" & JSText(Textos(93)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[38]='" & JSText(Textos(94)) & "';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextosPantalla, True)
            End If
        End Sub
#Region "Cargar Grid"
        Private Shared Sub Seleccionar_Columnas_Grid(ByVal Campos_Vista As List(Of Escenario_Campo), ByRef columNames As String(), ByRef columNamesDesglose As String(),
                ByRef ObtenerProcesos As Boolean, ByRef ObtenerPedidos As Boolean, ByRef VistaDesglose As Boolean, Optional ByRef bVaLaLinea As Boolean = False)
            ReDim Preserve columNamesDesglose(2)
            columNamesDesglose(0) = "FORM_INSTANCIA"
            columNamesDesglose(1) = "DESGLOSE"
            columNamesDesglose(2) = "LINEA"

            ReDim Preserve columNames(0)
            columNames(0) = "ROWNUMBER"

            bVaLaLinea = False

            For Each column As Escenario_Campo In Campos_Vista
                If column.EsCampoDesglose Then
                    VistaDesglose = True

                    If (Not column.TipoCampo = TiposDeDatos.TipoGeneral.TipoDesglose) Then
                        ReDim Preserve columNamesDesglose(columNamesDesglose.Length)
                        columNamesDesglose(columNamesDesglose.Length - 1) = column.Denominacion_BD
                    Else
                        bVaLaLinea = True
                    End If
                Else
                    If (column.Id = CamposGeneralesVisor.TIPOSOLICITUD AndAlso column.EsCampoGeneral AndAlso Not columNames.Contains("TIPO")) Or Not columNames.Contains(column.Denominacion_BD) Then
                        If Not column.Id = CamposGeneralesVisor.MARCASEGUIMIENTO Then
                            ReDim Preserve columNames(columNames.Length)

                            If column.Id = CamposGeneralesVisor.TIPOSOLICITUD AndAlso column.EsCampoGeneral Then
                                columNames(columNames.Length - 1) = "TIPO"
                            ElseIf column.Id = CamposGeneralesVisor.PROVEEDOR AndAlso column.EsCampoGeneral Then
                                columNames(columNames.Length - 1) = "PROVEEDOR_INSTANCIA"
                                If Not columNames.Contains("PROVEEDOR") Then
                                    ReDim Preserve columNames(columNames.Length)
                                    columNames(columNames.Length - 1) = "PROVEEDOR"
                                End If
                            ElseIf column.Id = CamposGeneralesVisor.ARTICULO AndAlso column.EsCampoGeneral Then
                                columNames(columNames.Length - 1) = "ARTICULO_INSTANCIA"
                            ElseIf column.Id = CamposGeneralesVisor.ORIGEN AndAlso column.EsCampoGeneral Then
                                columNames(columNames.Length - 1) = "ORIGEN_DEN"
                            ElseIf column.Id = CamposGeneralesVisor.ESTADOHOMOLOGACION AndAlso column.EsCampoGeneral Then
                                columNames(columNames.Length - 1) = "ESTADO_HOMOLOGACION"
                                ReDim Preserve columNames(columNames.Length)
                                columNames(columNames.Length - 1) = "ESTADO_HOMOLOGACION_DEN"
                            Else
                                columNames(columNames.Length - 1) = column.Denominacion_BD
                            End If
                        End If
                    End If
                    If column.EsCampoGeneral Then
                        Select Case column.Id
                            Case CamposGeneralesVisor.INFOPROCESOSASOCIADOS
                                ObtenerProcesos = True
                            Case CamposGeneralesVisor.INFOPEDIDOSASOCIADOS
                                ObtenerPedidos = True
                            Case CamposGeneralesVisor.IDENTIFICADOR
                                ReDim Preserve columNames(columNames.Length)
                                columNames(columNames.Length - 1) = "NUM_REEMISIONES"
                            Case CamposGeneralesVisor.IMPORTE
                                ReDim Preserve columNames(columNames.Length)
                                columNames(columNames.Length - 1) = "TIPO_SOLIC"
                                ReDim Preserve columNames(columNames.Length)
                                columNames(columNames.Length - 1) = "MON"
                            Case CamposGeneralesVisor.USUARIO, CamposGeneralesVisor.PETICIONARIO, CamposGeneralesVisor.ESTADO, CamposGeneralesVisor.SITUACIONACTUAL
                                If Not columNames.Contains("EN_PROCESO") Then
                                    ReDim Preserve columNames(columNames.Length)
                                    columNames(columNames.Length - 1) = "EN_PROCESO"
                                End If
                                If {CamposGeneralesVisor.USUARIO, CamposGeneralesVisor.PETICIONARIO}.Contains(column.Id) Then
                                    If Not columNames.Contains("TRASLADADA") Then
                                        ReDim Preserve columNames(columNames.Length)
                                        columNames(columNames.Length - 1) = "TRASLADADA"
                                    End If
                                    If Not columNames.Contains("VER_DETALLE_PER") Then
                                        ReDim Preserve columNames(columNames.Length)
                                        columNames(columNames.Length - 1) = "VER_DETALLE_PER"
                                    End If
                                    If Not columNames.Contains("VER_DETALLE_PER_ROL") Then
                                        ReDim Preserve columNames(columNames.Length)
                                        columNames(columNames.Length - 1) = "VER_DETALLE_PER_ROL"
                                    End If
                                    If Not columNames.Contains("PROVEEDOR") Then
                                        ReDim Preserve columNames(columNames.Length)
                                        columNames(columNames.Length - 1) = "PROVEEDOR"
                                    End If
                                    If Not columNames.Contains("FECHA_ACT") Then
                                        ReDim Preserve columNames(columNames.Length)
                                        columNames(columNames.Length - 1) = "FECHA_ACT"
                                    End If
                                    If Not columNames.Contains("FECHA_LIMITE") Then
                                        ReDim Preserve columNames(columNames.Length)
                                        columNames(columNames.Length - 1) = "FECHA_LIMITE"
                                    End If
                                    If Not columNames.Contains("TRASLADO_A_NOMBRE") Then
                                        ReDim Preserve columNames(columNames.Length)
                                        columNames(columNames.Length - 1) = "TRASLADO_A_NOMBRE"
                                    End If
                                    If Not columNames.Contains("TRASLADO_A_PROV") Then
                                        ReDim Preserve columNames(columNames.Length)
                                        columNames(columNames.Length - 1) = "TRASLADO_A_PROV"
                                    End If
                                    If Not columNames.Contains("TRASLADO_A_USU") Then
                                        ReDim Preserve columNames(columNames.Length)
                                        columNames(columNames.Length - 1) = "TRASLADO_A_USU"
                                    End If
                                    If column.Id = CamposGeneralesVisor.USUARIO Then
                                        If Not columNames.Contains("USU") Then
                                            ReDim Preserve columNames(columNames.Length)
                                            columNames(columNames.Length - 1) = "USU"
                                        End If
                                        If Not columNames.Contains("USUARIO_PROVE") Then
                                            ReDim Preserve columNames(columNames.Length)
                                            columNames(columNames.Length - 1) = "USUARIO_PROVE"
                                        End If
                                    End If
                                    If column.Id = CamposGeneralesVisor.PETICIONARIO Then
                                        If Not columNames.Contains("PET") Then
                                            ReDim Preserve columNames(columNames.Length)
                                            columNames(columNames.Length - 1) = "PET"
                                        End If
                                        If Not columNames.Contains("PETICIONARIO_PROVE") Then
                                            ReDim Preserve columNames(columNames.Length)
                                            columNames(columNames.Length - 1) = "PETICIONARIO_PROVE"
                                        End If
                                    End If
                                End If
                                If {CamposGeneralesVisor.ESTADO, CamposGeneralesVisor.SITUACIONACTUAL}.Contains(column.Id) Then
                                    If Not columNames.Contains("ETAPA") Then
                                        ReDim Preserve columNames(columNames.Length)
                                        columNames(columNames.Length - 1) = "ETAPA"
                                    End If
                                    If Not columNames.Contains("VER_FLUJO") Then
                                        ReDim Preserve columNames(columNames.Length)
                                        columNames(columNames.Length - 1) = "VER_FLUJO"
                                    End If
                                End If
                            Case CamposGeneralesVisor.ORIGEN
                                If Not columNames.Contains("VER_DETALLE_PER") Then
                                    ReDim Preserve columNames(columNames.Length)
                                    columNames(columNames.Length - 1) = "VER_DETALLE_PER"
                                End If
                        End Select
                    End If
                End If
                If column.TipoCampo = TiposDeDatos.TipoGeneral.TipoArchivo Then
                    _camposArchivo(column.Denominacion_BD) = column.Id
                End If
                If column.TipoCampo = TiposDeDatos.TipoGeneral.TipoEditor Then
                    _camposEditor(column.Denominacion_BD) = column.Id
                End If
            Next
            If Not columNames.Contains("ID") Then
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "ID"
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "NUM_REEMISIONES"
            End If
            If Not columNames.Contains("KO") Then
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "KO"
            End If
            If Not columNames.Contains("EST_VALIDACION") Then
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "EST_VALIDACION"
            End If
            If Not columNames.Contains("ICONO") Then
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "ICONO"
            End If
            If Not columNames.Contains("ERROR") Then
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "ERROR"
            End If
            If Not columNames.Contains("FECHA_VALIDACION") Then
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "FECHA_VALIDACION"
            End If
            If Not columNames.Contains("ERROR_VALIDACION") Then
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "ERROR_VALIDACION"
            End If
            If Not columNames.Contains("ACCION_APROBAR") Then
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "ACCION_APROBAR"
            End If
            If Not columNames.Contains("ACCION_RECHAZAR") Then
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "ACCION_RECHAZAR"
            End If
            If Not columNames.Contains("BLOQUE") Then
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "BLOQUE"
            End If
            If Not columNames.Contains("ETAPA_ACTUAL") Then
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "ETAPA_ACTUAL"
            End If
            If Not columNames.Contains("OBSERVADOR") Then
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "OBSERVADOR"
            End If
            If Not columNames.Contains("ESTADO") Then
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "ESTADO"
            End If
            If Not columNames.Contains("TIPO_SOLIC") Then
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "TIPO_SOLIC"
            End If
            If Not columNames.Contains("TRASLADADA") Then
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "TRASLADADA"
            End If
            If Not columNames.Contains("OBSYPART") Then
                ReDim Preserve columNames(columNames.Length)
                columNames(columNames.Length - 1) = "OBSYPART"
            End If
        End Sub
        Private Sub Configurar_Columnas_Grid_Solicitudes(ByVal lCamposVista As List(Of Escenario_Campo), ByVal Orden As String)
            Dim dtDatosVista As New DataTable
            Dim dtDatosVistaDesglose As New DataTable
            Dim campoGrid As Infragistics.Web.UI.GridControls.UnboundField
            Dim campoGridBound As Infragistics.Web.UI.GridControls.BoundDataField
            Dim campoDesgloseGrid As Infragistics.Web.UI.GridControls.UnboundField
            Dim campoDesgloseGridBound As Infragistics.Web.UI.GridControls.BoundDataField

            With whdgSolicitudes
                .Rows.Clear()
                .Columns.Clear()
                .GridView.Columns.Clear()
                .Bands("DESGLOSE").Columns.Clear()
            End With
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorSolicitudes
            Dim campoGridTemplate As New Infragistics.Web.UI.GridControls.TemplateDataField
            With campoGridTemplate
                .Key = "ACCION_APROBAR"
                .Header.Text = "<input id='chkAprobarCheckAll' type='checkbox' style='margin-right:10px'><input id='btnAprobar' type='submit' class='botonRedondeado' value='" & Textos(55) & "'/>"
                .Header.Tooltip = Textos(55)
                .Hidden = True
                .Header.CssClass = "headerNoWrap"
                .CssClass = "center itemSeleccionable SinSalto"
                .Width = Unit.Pixel(110)
            End With
            whdgSolicitudes.Columns.Add(campoGridTemplate)
            whdgSolicitudes.GridView.Columns.Add(campoGridTemplate)
            campoGridTemplate = New Infragistics.Web.UI.GridControls.TemplateDataField
            With campoGridTemplate
                .Key = "ACCION_RECHAZAR"
                .Header.Text = "<input id='chkRechazarCheckAll' type='checkbox' style='margin-right:10px'><input id='btnRechazar' type='submit' class='botonRedondeado' value='" & Textos(56) & "'/>"
                .Header.Tooltip = Textos(56)
                .Hidden = True
                .Header.CssClass = "headerNoWrap"
                .CssClass = "center itemSeleccionable SinSalto"
                .Width = Unit.Pixel(110)
            End With
            whdgSolicitudes.Columns.Add(campoGridTemplate)
            whdgSolicitudes.GridView.Columns.Add(campoGridTemplate)
            campoGridTemplate = New Infragistics.Web.UI.GridControls.TemplateDataField
            With campoGridTemplate
                .Key = "ERROR"
                .Header.Text = ""
                .Header.Tooltip = ""
                .Hidden = True
                .Header.CssClass = "celdaImagenWebHierarchical headerNoWrap"
                .CssClass = "celdaImagenWebHierarchical itemSeleccionable SinSalto"
                .Width = Unit.Pixel(20)
            End With
            whdgSolicitudes.Columns.Add(campoGridTemplate)
            whdgSolicitudes.GridView.Columns.Add(campoGridTemplate)
            campoGridTemplate = New Infragistics.Web.UI.GridControls.TemplateDataField
            With campoGridTemplate
                .Key = "ERROR_VALIDACION"
                .Header.Text = ""
                .Header.Tooltip = ""
                .Hidden = True
                .Header.CssClass = "celdaImagenWebHierarchical headerNoWrap"
                .CssClass = "celdaImagenWebHierarchical itemSeleccionable SinSalto"
                .Width = Unit.Pixel(20)
            End With
            whdgSolicitudes.Columns.Add(campoGridTemplate)
            whdgSolicitudes.GridView.Columns.Add(campoGridTemplate)
            campoGridTemplate = New Infragistics.Web.UI.GridControls.TemplateDataField
            With campoGridTemplate
                .Key = "ERROR_INTEGRACION"
                .Header.Text = ""
                .Header.Tooltip = ""
                .Hidden = True
                .Header.CssClass = "celdaImagenWebHierarchical headerNoWrap"
                .CssClass = "celdaImagenWebHierarchical itemSeleccionable SinSalto"
                .Width = Unit.Pixel(20)
            End With
            whdgSolicitudes.Columns.Add(campoGridTemplate)
            whdgSolicitudes.GridView.Columns.Add(campoGridTemplate)
            For Each campoVista As Escenario_Campo In lCamposVista
                If campoVista.EsCampoDesglose Then
                    If campoVista.TipoCampo = TiposDeDatos.TipoGeneral.TipoDesglose Then
                        campoDesgloseGridBound = New Infragistics.Web.UI.GridControls.BoundDataField
                        With campoDesgloseGridBound
                            .Key = "LINEA"
                            .DataFieldName = "LINEA"
                            .Header.Text = IIf(campoVista.NombrePersonalizado = "", Textos(57), campoVista.NombrePersonalizado)
                            .Header.Tooltip = IIf(campoVista.NombrePersonalizado = "", Textos(57), campoVista.NombrePersonalizado)
                            .Hidden = False
                            .Width = Unit.Pixel(campoVista.AnchoVisualizacion)
                        End With
                        whdgSolicitudes.Bands("DESGLOSE").Columns.Add(campoDesgloseGridBound)
                        whdgSolicitudes.Bands("DESGLOSE").Columns("LINEA").VisibleIndex = 0
                        ModuloIdioma = TiposDeDatos.ModulosIdiomas.Menu
                    ElseIf campoVista.TipoCampoGS = 0 _
                            AndAlso {TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoCorto}.Contains(campoVista.TipoCampo) Then
                        campoDesgloseGrid = New Infragistics.Web.UI.GridControls.UnboundField
                        With campoDesgloseGrid
                            .Key = campoVista.Denominacion_BD
                            .Header.Text = IIf(campoVista.NombrePersonalizado = "", campoVista.Denominacion, campoVista.NombrePersonalizado)
                            .Header.Tooltip = IIf(campoVista.NombrePersonalizado = "", campoVista.Denominacion, campoVista.NombrePersonalizado)
                            .Hidden = IIf(campoVista.Denominacion_BD = "FORM_INSTANCIA", True, False)
                            .Width = Unit.Pixel(campoVista.AnchoVisualizacion)
                            .CssClass = "itemSeleccionable SinSalto"
                            'Si es de tipo numÃ©rico alineamos a la derecha
                            If campoVista.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico AndAlso Not campoVista.TipoCampoGS = TiposDeDatos.TipoCampoGS.Empresa Then .CssClass &= " itemRightAligment"
                        End With
                        whdgSolicitudes.Bands("DESGLOSE").Columns.Add(campoDesgloseGrid)
                    Else
                        campoDesgloseGridBound = New Infragistics.Web.UI.GridControls.BoundDataField
                        With campoDesgloseGridBound
                            .Key = campoVista.Denominacion_BD
                            .DataFieldName = campoVista.Denominacion_BD
                            .Header.Text = IIf(campoVista.NombrePersonalizado = "", campoVista.Denominacion, campoVista.NombrePersonalizado)
                            .Header.Tooltip = IIf(campoVista.NombrePersonalizado = "", campoVista.Denominacion, campoVista.NombrePersonalizado)
                            .Hidden = IIf(campoVista.Denominacion_BD = "FORM_INSTANCIA", True, False)
                            .Width = Unit.Pixel(campoVista.AnchoVisualizacion)
                            .CssClass = "itemSeleccionable SinSalto"
                            'Si es de tipo numÃ©rico alineamos a la derecha
                            If campoVista.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico AndAlso Not campoVista.TipoCampoGS = TiposDeDatos.TipoCampoGS.Empresa Then .CssClass &= " itemRightAligment"
                        End With
                        whdgSolicitudes.Bands("DESGLOSE").Columns.Add(campoDesgloseGridBound)
                    End If
                Else
                    If campoVista.TipoCampoGS = 0 _
                            AndAlso {TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoTextoCorto}.Contains(campoVista.TipoCampo) Then
                        campoGrid = New Infragistics.Web.UI.GridControls.UnboundField
                        With campoGrid
                            .Key = campoVista.Denominacion_BD
                            .Header.Text = IIf(campoVista.NombrePersonalizado = "",
                                        IIf(campoVista.EsCampoGeneral AndAlso campoVista.Id = CamposGeneralesVisor.MARCASEGUIMIENTO, "", campoVista.Denominacion),
                                        campoVista.NombrePersonalizado)
                            .Header.Tooltip = IIf(campoVista.NombrePersonalizado = "", campoVista.Denominacion, campoVista.NombrePersonalizado)
                            .Hidden = False
                            .Header.CssClass = "headerNoWrap"
                            .CssClass = "itemSeleccionable SinSalto"
                            .Width = Unit.Pixel(campoVista.AnchoVisualizacion)
                            'Si es de tipo numÃ©rico alineamos a la derecha
                            If campoVista.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico AndAlso Not campoVista.TipoCampoGS = TiposDeDatos.TipoCampoGS.Empresa Then .CssClass &= " itemRightAligment"
                        End With
                        whdgSolicitudes.Columns.Add(campoGrid)
                        whdgSolicitudes.GridView.Columns.Add(campoGrid)
                    Else
                        If Not campoVista.Id = CamposGeneralesVisor.MARCASEGUIMIENTO Then
                            campoGridBound = New Infragistics.Web.UI.GridControls.BoundDataField
                            With campoGridBound
                                .Key = IIf(campoVista.Id = CamposGeneralesVisor.SITUACIONACTUAL, "ETAPA_ACTUAL", campoVista.Denominacion_BD)
                                .DataFieldName = IIf(campoVista.Id = CamposGeneralesVisor.SITUACIONACTUAL, "ETAPA_ACTUAL", campoVista.Denominacion_BD)
                                .Header.Text = IIf(campoVista.NombrePersonalizado = "",
                                        IIf(campoVista.EsCampoGeneral AndAlso campoVista.Id = CamposGeneralesVisor.MARCASEGUIMIENTO, "", campoVista.Denominacion),
                                        campoVista.NombrePersonalizado)
                                .Header.Tooltip = IIf(campoVista.NombrePersonalizado = "", campoVista.Denominacion, campoVista.NombrePersonalizado)
                                .Hidden = False
                                .Header.CssClass = "headerNoWrap"
                                .CssClass = "itemSeleccionable SinSalto"
                                .Width = Unit.Pixel(campoVista.AnchoVisualizacion)
                                'Si es de tipo numÃ©rico alineamos a la derecha
                                If campoVista.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico AndAlso Not campoVista.TipoCampoGS = TiposDeDatos.TipoCampoGS.Empresa Then .CssClass &= " itemRightAligment"
                                'si es de tipo fecha formatear con formato usuario
                                If campoVista.EsCampoGeneral Then
                                    Select Case campoVista.Id
                                        Case CamposGeneralesVisor.TIPOSOLICITUD  'TIPO_SOLIC
                                            .Key = "TIPO"
                                            .DataFieldName = "TIPO"
                                        Case CamposGeneralesVisor.PROVEEDOR
                                            .Key = "PROVEEDOR_INSTANCIA"
                                            .DataFieldName = "PROVEEDOR_INSTANCIA"
                                        Case CamposGeneralesVisor.ARTICULO
                                            .Key = "ARTICULO_INSTANCIA"
                                            .DataFieldName = "ARTICULO_INSTANCIA"
                                        Case CamposGeneralesVisor.ORIGEN
                                            .Key = "ORIGEN_DEN"
                                            .DataFieldName = "ORIGEN_DEN"
                                    End Select
                                End If
                            End With
                            whdgSolicitudes.Columns.Add(campoGridBound)
                            whdgSolicitudes.GridView.Columns.Add(campoGridBound)
                        End If
                    End If
                End If
            Next

            ' Añadimos una columna que ocupe el resto del espacio. Esto es para que salga la barra de scroll si el grid de 2Âº nivel es mÃ¡s largo que el de 1er nivel
            campoGridTemplate = New Infragistics.Web.UI.GridControls.TemplateDataField
            With campoGridTemplate
                .Key = "EmptySpace"
                .Header.CssClass = "headerEmpty"
                .CssClass = "itemEmpty"
                ' Calculamos el tamaÃ±o del primer nivel
                Dim AnchoNivel1 As Double = (From x As GridField In whdgSolicitudes.Columns Where Not x.Hidden Select x.Width.Value).Sum()
                ' Calculamos el tamaÃ±o del segundo nivel
                Dim AnchoNivel2 As Double = 15 + (From x As GridField In whdgSolicitudes.Bands("DESGLOSE").Columns Where Not x.Hidden Select x.Width.Value).Sum()
                ' Establecemos el tamaÃ±o de esta columna
                Dim AnchoColumnaRelleno = AnchoNivel2 - AnchoNivel1
                If AnchoColumnaRelleno < 0 Then
                    AnchoColumnaRelleno = 0
                    .Hidden = True
                End If
                .Width = Unit.Pixel(AnchoColumnaRelleno)
            End With
            whdgSolicitudes.Columns.Add(campoGridTemplate)
            whdgSolicitudes.GridView.Columns.Add(campoGridTemplate)

            For Each column As String In Split(Orden, ",")
                If column IsNot String.Empty Then _
                whdgSolicitudes.GridView.Behaviors.Sorting.SortedColumns.Add(Split(column, " ")(0),
                            IIf(Split(column, " ")(1).ToLower = "asc", Infragistics.Web.UI.SortDirection.Ascending, Infragistics.Web.UI.SortDirection.Descending))
            Next
        End Sub
        Private Sub Configurar_Columnas_Grid_Solicitudes_Exportar(ByVal lCamposVista As List(Of Escenario_Campo), ByVal Orden As String)
            Dim dtDatosVista As New DataTable
            Dim dtDatosVistaDesglose As New DataTable
            Dim campoGridBoundTemplate As Infragistics.Web.UI.GridControls.BoundDataField
            Dim campoDesgloseGridBoundTemplate As Infragistics.Web.UI.GridControls.BoundDataField

            With whdgExportacion
                .Rows.Clear()
                .Columns.Clear()
                .GridView.Columns.Clear()
                .Bands("DESGLOSE").Columns.Clear()
            End With
            Dim campoGridTemplate As New Infragistics.Web.UI.GridControls.TemplateDataField
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.Menu
            With campoGridTemplate
                .Key = "ERROR"
                .Header.Text = "ERROR"
                .Hidden = True
                .Width = Unit.Pixel(20)
            End With
            whdgExportacion.Columns.Add(campoGridTemplate)
            whdgExportacion.GridView.Columns.Add(campoGridTemplate)
            campoGridTemplate = New Infragistics.Web.UI.GridControls.TemplateDataField
            With campoGridTemplate
                .Key = "ERROR_VALIDACION"
                .Header.Text = "ERROR_VALIDACION"
                .Hidden = True
                .Width = Unit.Pixel(20)
            End With
            whdgExportacion.Columns.Add(campoGridTemplate)
            whdgExportacion.GridView.Columns.Add(campoGridTemplate)
            campoGridTemplate = New Infragistics.Web.UI.GridControls.TemplateDataField
            With campoGridTemplate
                .Key = "ERROR_INTEGRACION"
                .Header.Text = "ERROR_INTEGRACION"
                .Hidden = True
                .Width = Unit.Pixel(20)
            End With
            whdgExportacion.Columns.Add(campoGridTemplate)
            whdgExportacion.GridView.Columns.Add(campoGridTemplate)
            For Each campoVista As Escenario_Campo In lCamposVista
                If campoVista.EsCampoDesglose Then
                    If campoVista.TipoCampoGS <> TiposDeDatos.TipoCampoGS.Desglose Then
                        campoDesgloseGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                        With campoDesgloseGridBoundTemplate
                            .Key = campoVista.Denominacion_BD
                            .DataFieldName = campoVista.Denominacion_BD
                            .Header.Text = IIf(campoVista.NombrePersonalizado = "", campoVista.Denominacion, campoVista.NombrePersonalizado)
                            .Hidden = IIf(campoVista.Denominacion_BD = "FORM_INSTANCIA", True, False)
                            .Width = Unit.Pixel(campoVista.AnchoVisualizacion)
                        End With
                        whdgExportacion.Bands("DESGLOSE").Columns.Add(campoDesgloseGridBoundTemplate)
                    End If
                Else
                    If Not campoVista.Id = CamposGeneralesVisor.MARCASEGUIMIENTO Then
                        campoGridBoundTemplate = New Infragistics.Web.UI.GridControls.BoundDataField
                        With campoGridBoundTemplate
                            .Key = campoVista.Denominacion_BD
                            .DataFieldName = campoVista.Denominacion_BD
                            .Header.Text = IIf(campoVista.NombrePersonalizado = "",
                                    IIf(campoVista.EsCampoGeneral AndAlso campoVista.Id = CamposGeneralesVisor.MARCASEGUIMIENTO, "", campoVista.Denominacion),
                                    campoVista.NombrePersonalizado)
                            .Hidden = False
                            .Width = Unit.Pixel(campoVista.AnchoVisualizacion)
                            'si es de tipo fecha formatear con formato usuario
                            If campoVista.EsCampoGeneral Then
                                Select Case campoVista.Id
                                    Case CamposGeneralesVisor.TIPOSOLICITUD  'TIPO_SOLIC
                                        .Key = "TIPO"
                                        .DataFieldName = "TIPO"
                                    Case CamposGeneralesVisor.PROVEEDOR
                                        .Key = "PROVEEDOR_INSTANCIA"
                                        .DataFieldName = "PROVEEDOR_INSTANCIA"
                                    Case CamposGeneralesVisor.ARTICULO
                                        .Key = "ARTICULO_INSTANCIA"
                                        .DataFieldName = "ARTICULO_INSTANCIA"
                                    Case CamposGeneralesVisor.ORIGEN
                                        .Key = "ORIGEN_DEN"
                                        .DataFieldName = "ORIGEN_DEN"
                                End Select
                            End If
                        End With
                        whdgExportacion.Columns.Add(campoGridBoundTemplate)
                        whdgExportacion.GridView.Columns.Add(campoGridBoundTemplate)
                    End If
                End If
            Next
        End Sub
        Private Sub PaginadorGrid(ByVal _pageCount As Integer, ByVal _pageNumber As Integer)
            Dim pagerList As DropDownList = DirectCast(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
            pagerList.Items.Clear()
            If _pageCount = 0 Then pagerList.Items.Add("")
            For i As Integer = 1 To _pageCount
                pagerList.Items.Add(i.ToString())
            Next
            CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = _pageCount
            If _pageCount > 0 Then pagerList.SelectedIndex = _pageNumber - 1
            Dim Desactivado As Boolean = (_pageNumber = 1)
            With CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), System.Web.UI.WebControls.Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero" & IIf(Desactivado, "_desactivado", "") & ".gif"
                If Not Desactivado Then .CssClass = "Link"
                .Enabled = Not Desactivado
            End With
            With CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), System.Web.UI.WebControls.Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior" & IIf(Desactivado, "_desactivado", "") & ".gif"
                If Not Desactivado Then .CssClass = "Link"
                .Enabled = Not Desactivado
            End With
            Desactivado = (_pageCount = 1 OrElse _pageNumber = _pageCount)
            With CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), System.Web.UI.WebControls.Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(Desactivado, "_desactivado", "") & ".gif"
                If Not Desactivado Then .CssClass = "Link"
                .Enabled = Not Desactivado
            End With
            With CType(whdgSolicitudes.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), System.Web.UI.WebControls.Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(Desactivado, "_desactivado", "") & ".gif"
                If Not Desactivado Then .CssClass = "Link"
                .Enabled = Not Desactivado
            End With
        End Sub
        Private Sub ExportarExcel()
            Dim fileName As String = HttpUtility.UrlEncode("VisorSolicitudes")
            fileName = fileName.Replace("+", "%20")

            With whdgExcelExporter
                .DownloadName = fileName
                .DataExportMode = DataExportMode.AllDataInDataSource
                .EnableStylesExport = False
                .Export(whdgExportacion)
            End With
        End Sub
#End Region
#Region "whdgSolicitudes"
        Private Sub whdgSolicitudes_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgSolicitudes.InitializeRow
            Dim nivel As Integer = CType(e.Row, ContainerGridRecord).Level
            Dim items As Infragistics.Web.UI.GridControls.GridRecordItemCollection = e.Row.Items
            Dim dataRow As DataRow = e.Row.DataItem.Item.Row
            Dim grid As ContainerGrid = items.Grid
            Dim columns As GridFieldCollection = grid.Columns
            Dim gridCellIndex As Integer

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorSolicitudes
            If nivel = 0 Then
                ' Fila de primer nivel    
                Dim iEstado As Integer
                If CType(dataRow("ICONO"), Boolean) OrElse CType(dataRow("TRASLADADA"), Boolean) Then
                    iEstado = 1000
                ElseIf CType(dataRow("OBSERVADOR"), Boolean) Then
                    iEstado = 3
                Else
                    iEstado = CType(dataRow("ESTADO"), Boolean)
                End If
                If whdgSolicitudes.GridView.Columns.Item("DEP") IsNot Nothing AndAlso Not DBNullToBoolean(e.Row.DataItem.Item.Row.Item("VER_DETALLE_PER")) Then
                    gridCellIndex = whdgSolicitudes.GridView.Columns.FromKey("DEP").Index
                    items(gridCellIndex).Text = ""
                End If
                For Each columna As GridField In whdgSolicitudes.GridView.Columns
                    ' Si es el espacio vacío del final no hacemos nada
                    If columna.Key = "EmptySpace" Then Continue For
                    gridCellIndex = whdgSolicitudes.GridView.Columns.FromKey(columna.Key).Index
                    If _camposArchivo.ContainsKey(columna.Key) Then
                        Dim cAdjuntos As Adjuntos
                        cAdjuntos = FSPMServer.Get_Adjuntos
                        cAdjuntos.LoadAdjuntosPorInstanciayCampo(FSPMUser.CiaComp, dataRow("ID"), _camposArchivo(columna.Key))
                        If cAdjuntos.Data.Tables(0).Rows.Count > 0 Then
                            Dim params As String
                            If cAdjuntos.Data.Tables(0).Rows.Count = 1 Then
                                params = "instancia=" & CType(dataRow("ID"), Integer) & "&id=" & cAdjuntos.Data.Tables(0).Rows(0).Item(0)
                                items(gridCellIndex).Text = "<span rutaAdjunto='" & params & "'>" & items(gridCellIndex).Text & "</span>"
                            Else
                                Dim campo As String = ""
                                Dim valor As String = ""
                                For Each orow As DataRow In cAdjuntos.Data.Tables(0).Rows
                                    campo = orow.Item(1)
                                    If valor <> "" Then valor = valor & "xx"
                                    valor = valor & orow.Item(0)
                                Next
                                params = "instancia=" & CType(dataRow("ID"), Integer) & "&Campo=" & campo & "&Valor=" & valor
                                items(gridCellIndex).Text = "<span rutaAdjuntos='" & params & "'>" & items(gridCellIndex).Text & "</span>"
                            End If
                        End If
                    ElseIf _camposEditor.ContainsKey(columna.Key) Then
                        items(gridCellIndex).Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "Images/edit2.gif' name='editor'/>" &
                        "<span style='display:none;' titulo='" & CType(columns(columna.Key), BoundDataField).Header.Text & "' id='" & dataRow("ID") & "' campo_origen='" & Split(columna.Key, "_")(Split(columna.Key, "_").Length - 1) & "'></span>"
                    ElseIf _camposTexto.ContainsKey(columna.Key) AndAlso Len(dataRow(columna.Key).ToString.Trim()) > 0 Then
                        items(gridCellIndex).Text = "<img class='imgTexto' src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/coment.gif' name='comentario' " &
                        "onclick='MostrarInfoTextoLargo(""" & columns(columna.Key).Header.Text & """,this,""" & CType(dataRow("ID"), Integer) & """,""" & columna.Key & """)'/>" &
                        "<span name='comentario' instanciaId='" & CType(dataRow("ID"), Integer) & "' " &
                                    "instanciaEstado='" & iEstado & "' " &
                                    "instanciaTipoSolicitud='" & DBNullToInteger(dataRow("TIPO_SOLIC")) & "' " &
                                    "instanciaBloque='" & CType(dataRow("BLOQUE"), Integer) & "'" &
                                    "observador='" & dataRow("OBSERVADOR") & "'>" & IIf(dataRow(columna.Key).length > 50, Left(dataRow(columna.Key), 50) & "...", dataRow(columna.Key)) & "</span>"
                        items(gridCellIndex).CssClass = "imgOcultoCeldaWebHierarchical"
                    Else
                        ' Preparamos el span con las propiedades de la fila
                        Dim cellText As String = "<span instanciaId='" & CType(dataRow("ID"), Integer) & "' " &
                        "instanciaEstado='" & iEstado & "' " &
                        "instanciaTipoSolicitud='" & DBNullToInteger(dataRow("TIPO_SOLIC")) & "' " &
                        "instanciaBloque='" & CType(dataRow("BLOQUE"), Integer) & "'" &
                        "observador='" & dataRow("OBSERVADOR") & "'>{0}</span>"
                        Select Case columna.Key
                            Case "ACCION_APROBAR"
                                If Not DBNullToInteger(e.Row.DataItem.Item.Row.Item("ACCION_APROBAR")) = 0 Then
                                    whdgSolicitudes.Columns("ACCION_APROBAR").Hidden = False
                                    items(gridCellIndex).Text = "<input type='checkbox' " &
                                    "data='" & dataRow("BLOQUE") & "|" & dataRow("ACCION_APROBAR") & "' " &
                                    "id='chkAprobar_" & dataRow("ID") & "'" & If(InstanciasAprobar.ContainsKey(dataRow("ID")), " checked", "") & "/>"
                                Else
                                    items(gridCellIndex).Text = "<input type='checkbox' id='chkAprobar_" & dataRow("ID") & "' disabled='true'/>"
                                End If
                            Case "ACCION_RECHAZAR"
                                If Not DBNullToInteger(dataRow("ACCION_RECHAZAR")) = 0 Then
                                    whdgSolicitudes.Columns("ACCION_RECHAZAR").Hidden = False
                                    items(gridCellIndex).Text = "<input type='checkbox' " &
                                    "data='" & dataRow("BLOQUE") & "|" & dataRow("ACCION_RECHAZAR") & "' " &
                                    "id='chkRechazar_" & dataRow("ID") & "'" & If(InstanciasRechazar.ContainsKey(dataRow("ID")), " checked", "") & "/>"
                                Else
                                    items(gridCellIndex).Text = "<input type='checkbox' id='chkRechazar_" & dataRow("ID") & "' disabled='true'/>"
                                End If
                            Case "PROCESOS"
                                If Not DBNullToSomething(dataRow("PROCESOS")) = Nothing Then
                                    Dim processCellText As String = "<span instanciaId='" & CType(dataRow("ID"), Integer) & "' " &
                                        "instanciaEstado='" & iEstado & "' " &
                                        "instanciaTipoSolicitud='" & DBNullToInteger(dataRow("TIPO_SOLIC")) & "' " &
                                        "instanciaBloque='" & CType(dataRow("BLOQUE"), Integer) & "'" &
                                        "observador='" & dataRow("OBSERVADOR") & "' {1}>{0}</span>"

                                    items(gridCellIndex).Text = String.Format(processCellText, Left(FormatoDenEstado(dataRow("PROCESOS")), FormatoDenEstado(dataRow("PROCESOS")).Length - 1),
                                                                          "onclick=""MostrarProcesosAsociados('" & columns(columna.Key).Header.Text & "','" &
                                                                            Replace(Left(FormatoDenEstado(dataRow("PROCESOS")), FormatoDenEstado(dataRow("PROCESOS")).Length - 1), ",", ",<br/>") & "')""")
                                    items(gridCellIndex).Tooltip = Replace(Left(FormatoDenEstado(dataRow("PROCESOS")), FormatoDenEstado(dataRow("PROCESOS")).Length - 1), ",", "," & vbCrLf)
                                Else
                                    items(gridCellIndex).Text = String.Format(cellText, String.Empty)
                                End If
                            Case "ERROR_VALIDACION"
                                If CType(dataRow("EST_VALIDACION"), Integer) > 1 AndAlso CType(dataRow("EST_VALIDACION"), Integer) < 99 Then
                                    whdgSolicitudes.Columns("ERROR_VALIDACION").Hidden = False
                                    items(gridCellIndex).Text = "<img style='margin:0em 0.1em;' " &
                                    "title='" & IIf(CType(dataRow("EST_VALIDACION"), Integer) = 9,
                                    Textos(98) & " " & dataRow("FECHA_VALIDACION") & " " & Mid(Textos(99), 1, InStr(Textos(99), ".")) & " " & Textos(100),
                                    Textos(98) & " " & dataRow("FECHA_VALIDACION") & " " & Textos(99) & " " & dataRow("ERROR_VALIDACION")) & "' " &
                                    "src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "/Images/Icono_Error_Amarillo.gif" & "'/>"
                                End If
                            Case "ERROR"
                                If DBNullToBoolean(dataRow("ICONO")) Then
                                    whdgSolicitudes.Columns("ERROR").Hidden = False
                                    items(gridCellIndex).Text = "<img style='margin:0em 0.1em;'  title='' class='Image16' " &
                                    "src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "/Images/Img_Ico_Alerta.gif" & "'/>"
                                End If
                            Case "ESTADO", "ETAPA", "ETAPA_ACTUAL"
                                ' Para un Estado o Etapa reemplazamos su Id por el Texto en el lenguaje correspondiente
                                If {1, 2}.Contains(CType(dataRow("EN_PROCESO"), Integer)) Then
                                    ' En Proceso
                                    items(gridCellIndex).Text = Textos(101)
                                Else
                                    Select Case CType(dataRow("ESTADO"), Integer)
                                        Case Fullstep.FSNLibrary.TipoEstadoSolic.SCRechazada, TipoEstadoSolic.SCAnulada,
                                    TipoEstadoSolic.SCPendiente, TipoEstadoSolic.SCAprobada
                                            items(gridCellIndex).Text = Textos(102)
                                        Case Fullstep.FSNLibrary.TipoEstadoSolic.Rechazada
                                            items(gridCellIndex).Text = Textos(103)
                                        Case Fullstep.FSNLibrary.TipoEstadoSolic.Anulada
                                            items(gridCellIndex).Text = Textos(104)
                                        Case Fullstep.FSNLibrary.TipoEstadoSolic.SCCerrada
                                            ' Cerrada
                                            items(gridCellIndex).Text = Textos(105)
                                        Case Else  'Esta guardada o en curso.Se muestra la etapa actual
                                            If columna.Key = "ESTADO" Then
                                                Select Case CType(dataRow("ESTADO"), Integer)
                                                    Case Fullstep.FSNLibrary.TipoEstadoSolic.Guardada
                                                        items(gridCellIndex).Text = Textos(106)
                                                    Case Fullstep.FSNLibrary.TipoEstadoSolic.EnCurso
                                                        If DBNullToBoolean(dataRow("ICONO")) Then
                                                            items(gridCellIndex).Text = Textos(107)
                                                        Else
                                                            items(gridCellIndex).Text = Textos(40)
                                                        End If
                                                End Select
                                            Else
                                                If dataRow("ETAPA_ACTUAL") = "##" Then 'Etapas en paralelo
                                                    items(gridCellIndex).Text = Textos(124)
                                                Else
                                                    items(gridCellIndex).Text = dataRow("ETAPA_ACTUAL")
                                                End If
                                                If DBNullToBoolean(dataRow("VER_FLUJO")) Then
                                                    items(gridCellIndex).Text = "<span instanciaId='" & CType(dataRow("ID"), Integer) & "'>" &
                                                    items(gridCellIndex).Text & Space(1) & "<img " &
                                                        "src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "/Images/observaciones_12x12.gif" & "'/></span>"
                                                End If
                                            End If
                                    End Select
                                End If
                            Case "ESTADO_HOMOLOGACION"
                                items(gridCellIndex).Text = String.Format(cellText, DBNullToStr(dataRow("ESTADO_HOMOLOGACION_DEN")))
                            Case "USUARIO", "PETICIONARIO"
                                ' Para Usuario o Peticionario metemos el texto dentro de un span con las propiedades del Usuario/Peticionario correspondiente
                                If {1, 2}.Contains(CType(dataRow("EN_PROCESO"), Integer)) AndAlso columna.Key = "USUARIO" Then
                                    ' Escribimos "En Proceso"
                                    items(gridCellIndex).Text = Textos(101)
                                Else
                                    If (Not CType(dataRow("TRASLADADA"), Boolean) AndAlso columna.Key = "USUARIO") OrElse columna.Key = "PETICIONARIO" Then
                                        If DBNullToBoolean(dataRow("VER_DETALLE_PER")) OrElse DBNullToBoolean(dataRow("VER_DETALLE_PER_ROL")) Then
                                            If columna.Key = "USUARIO" AndAlso Not IsDBNull(dataRow("USUARIO_PROVE")) AndAlso Not {1, 2}.Contains(CType(dataRow("EN_PROCESO"), Integer)) Then
                                                items(gridCellIndex).Text = "<span proveedor='" & dataRow("USUARIO_PROVE") & "'>" & dataRow("PROVEEDOR") & "</span>"
                                            Else
                                                items(gridCellIndex).Text = "<span " & LCase(columna.Key) & "='"
                                                If columna.Key = "USUARIO" Then
                                                    items(gridCellIndex).Text &= dataRow("USUARIO") & "'>" & dataRow("USU")
                                                Else
                                                    If String.IsNullOrEmpty(dataRow("PETICIONARIO").ToString) Then
                                                        items(gridCellIndex).Text &= dataRow("PETICIONARIO_PROVE") & "' proveedor"
                                                    Else
                                                        items(gridCellIndex).Text &= dataRow("PETICIONARIO") & "'"
                                                    End If
                                                    items(gridCellIndex).Text &= ">" & dataRow("PET")
                                                End If
                                                items(gridCellIndex).Text &= "</span>"
                                            End If
                                        Else 'en blanco
                                            items(gridCellIndex).Text = String.Empty
                                        End If
                                    ElseIf CType(dataRow("TRASLADADA"), Boolean) AndAlso columna.Key = "USUARIO" Then
                                        items(gridCellIndex).Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/TrasladarSolicitud.png" & "'/>" & Space(1) & dataRow("TRASLADO_A_NOMBRE")
                                        items(gridCellIndex).Tooltip = Textos(108) & Chr(10) & Chr(13) & Textos(109) & ": " & FormatDate(dataRow("FECHA_ACT"), FSPMUser.DateFormat, "g")
                                        If Not IsDBNull(dataRow("FECHA_LIMITE")) Then
                                            items(gridCellIndex).Tooltip &= (Chr(10) & Chr(13) & Textos(110) & " " & FormatDate(dataRow("FECHA_LIMITE"), FSPMUser.DateFormat, "d"))
                                        End If
                                        If Not IsDBNull(dataRow("TRASLADO_A_PROV")) Then
                                            items(gridCellIndex).Text = "<span proveedor='" & dataRow("TRASLADO_A_PROV") & "'>" & items(gridCellIndex).Text & "</span>"
                                        ElseIf Not IsDBNull(dataRow("TRASLADO_A_USU")) Then
                                            items(gridCellIndex).Text = "<span usuario='" & dataRow("TRASLADO_A_USU") & "'>" & items(gridCellIndex).Text & "</span>"
                                        End If
                                    End If
                                End If
                            Case "MOTIVO_VISIBILIDAD"
                                ' Para "Motivo Visibilidad" reemplazamos su Id por el Texto en el lenguaje correspondiente
                                Select Case CType(dataRow("MOTIVO_VISIBILIDAD"), Integer)
                                    Case MotivoVisibilidadSolicitud.SolicitudAbiertaUsted  'Solicitudes abiertas por usted
                                        items(gridCellIndex).Text = Textos(111)
                                    Case MotivoVisibilidadSolicitud.SolicitudParticipo
                                        items(gridCellIndex).Text = Textos(112)
                                    Case MotivoVisibilidadSolicitud.SolicitudPendiente
                                        items(gridCellIndex).Text = Textos(113)
                                    Case MotivoVisibilidadSolicitud.SolicitudPenditenteDevolucion
                                        items(gridCellIndex).Text = Textos(114)
                                    Case MotivoVisibilidadSolicitud.SolicitudTrasladadaEsperaDevolucion
                                        items(gridCellIndex).Text = Textos(115)
                                    Case Else 'Observador
                                        items(gridCellIndex).Text = Textos(116)
                                End Select
                            Case "IMPORTE"
                                If dataRow("IMPORTE") IsNot Nothing AndAlso Not IsDBNull(dataRow("IMPORTE")) Then
                                    items(gridCellIndex).Text = FSNLibrary.FormatNumber(CType(DBNullToDbl(dataRow("IMPORTE")), Double), FSPMUser.NumberFormat) & " " & dataRow("MON")
                                Else
                                    items(gridCellIndex).Text = ""
                                End If
                                ' Metemos el valor en el span
                                items(gridCellIndex).Text = String.Format(cellText, items(gridCellIndex).Text)
                            Case "ID"
                                If Not CType(dataRow("NUM_REEMISIONES"), Integer) = 0 Then
                                    items(gridCellIndex).Text = items(gridCellIndex).Text & " (" & CType(dataRow("NUM_REEMISIONES"), Integer) & ")"
                                End If
                                ' Metemos el valor en el span
                                items(gridCellIndex).Text = String.Format(cellText, items(gridCellIndex).Text)
                            Case "FECHA_ACT"
                                If CType(dataRow("TRASLADADA"), Boolean) Then
                                    items(gridCellIndex).Text = FormatDate(CType(dataRow("FECHA_ACT"), Date), FSPMUser.DateFormat, "d")
                                Else
                                    items(gridCellIndex).Text = String.Empty
                                End If
                                ' Metemos el valor en el span
                                items(gridCellIndex).Text = String.Format(cellText, items(gridCellIndex).Text)
                            Case "FECHA_LIMITE"
                                If CType(dataRow("TRASLADADA"), Boolean) Then
                                    items(gridCellIndex).Text = FormatDate(CType(dataRow("FECHA_LIMITE"), Date), FSPMUser.DateFormat, "d")
                                Else
                                    items(gridCellIndex).Text = String.Empty
                                End If
                                ' Metemos el valor en el span
                                items(gridCellIndex).Text = String.Format(cellText, items(gridCellIndex).Text)
                            Case "PROVEEDOR_INSTANCIA"
                                If IsDBNull(dataRow("PROVEEDOR_INSTANCIA")) Then
                                    If dataRow.Table.Columns.Contains("PROVEEDOR") Then
                                        items(gridCellIndex).Text = DBNullToStr(dataRow("PROVEEDOR"))
                                    End If
                                End If
                            Case Else
                                ' Si no estamos en una celda que no debe ir al detalle, metemos el texto dentro de un span con las propiedades de la fila
                                If Not {"ESTADO", "ETAPA", "USUARIO", "PETICIONARIO"}.Contains(columna.Key) Then
                                    If items(gridCellIndex).Value IsNot Nothing Then
                                        Select Case items(gridCellIndex).Value.GetType
                                            Case GetType(System.Boolean), GetType(System.Byte)
                                                ' Si es booleano, escribimos Sí/No en el lenguaje correspondiente
                                                If CType(items(gridCellIndex).Value, Boolean) Then
                                                    items(gridCellIndex).Text = Textos(46)
                                                Else
                                                    items(gridCellIndex).Text = Textos(47)
                                                End If
                                            Case GetType(System.Int16), GetType(System.Int32), GetType(System.Int64), GetType(System.Double)
                                                If Not IsDBNull(items(gridCellIndex).Value) Then items(gridCellIndex).Text = FSNLibrary.FormatNumber(CType(items(gridCellIndex).Value, Double), FSPMUser.NumberFormat)
                                            Case GetType(System.DateTime)
                                                ' Si es una fecha, la formateamos según las preferencias de usuario
                                                items(gridCellIndex).Text = FormatDate(items(gridCellIndex).Value, FSPMUser.DateFormat, "d")
                                        End Select
                                    End If
                                    ' Metemos el valor en el span
                                    items(gridCellIndex).Text = String.Format(cellText, items(gridCellIndex).Text)
                                End If
                        End Select
                    End If
                Next
            Else
                ' Fila de una banda
                Dim band As Band = CType(e.Row, ContainerGridRecord).Owner.ControlMain.Band
                For Each columna As GridField In band.Columns
                    If columna.Key = "LINEA" OrElse dataRow("DESGLOSE") = Split(columna.Key, "_")(1) Then
                        If dataRow(columna.Key) IsNot Nothing AndAlso Not IsDBNull(dataRow(columna.Key)) Then
                            If _camposArchivo.ContainsKey(columna.Key) Then
                                Dim cAdjuntos As Adjuntos
                                cAdjuntos = FSPMServer.Get_Adjuntos
                                '********************************************************************************************************************cAdjuntos.LoadAdjuntosPorInstanciayCampo(dataRow("FORM_INSTANCIA"), _camposArchivo(columna.Key))
                                If cAdjuntos.Data.Tables(0).Rows.Count > 0 Then
                                    Dim params As String
                                    If cAdjuntos.Data.Tables(0).Rows.Count = 1 Then
                                        params = "instancia=" & CType(dataRow("ID"), Integer) & "&id=" & cAdjuntos.Data.Tables(0).Rows(0).Item(0)
                                        items(columna.Index).Text = "<span rutaAdjunto='" & params & "'>" & items(columna.Index).Text & "</span>"
                                    Else
                                        Dim campo As String = ""
                                        Dim valor As String = ""
                                        For Each orow As DataRow In cAdjuntos.Data.Tables(0).Rows
                                            campo = orow.Item(1)
                                            If valor <> "" Then valor = valor & "xx"
                                            valor = valor & orow.Item(0)
                                        Next
                                        params = "instancia=" & CType(dataRow("ID"), Integer) & "&Campo=" & campo & "&Valor=" & valor
                                        items(columna.Index).Text = "<span rutaAdjuntos='" & params & "'>" & items(columna.Index).Text & "</span>"
                                    End If
                                End If
                            ElseIf _camposEditor.ContainsKey(columna.Key) Then
                                items(columna.Index).Text = "<img src='" & ConfigurationManager.AppSettings("ruta") & "Images/edit2.gif' name='editor'/>" &
                                "<span style='display:none;' titulo='" & CType(columns(columna.Key), BoundDataField).Header.Text & "' id='" & dataRow("ID") & "' campo_origen='" & Split(columna.Key, "_")(Split(columna.Key, "_").Length - 1) & "'></span>"
                            ElseIf _camposTexto.ContainsKey(columna.Key) AndAlso Len(dataRow(columna.Key).ToString.Trim()) > 0 Then
                                items(columna.Index).Text = "<img class='imgTexto' src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "images/coment.gif' name='comentario' " &
                                "onclick='MostrarInfoTextoLargo(""" & columns(columna.Key).Header.Text & """,this,""" & CType(dataRow("FORM_INSTANCIA"), Integer) & """,""" & columna.Key & """)'/>" &
                                "<span name='comentario'>" & IIf(dataRow(columna.Key).length > 50, Left(dataRow(columna.Key), 50) & "...", dataRow(columna.Key)) & "</span>"
                                items(columna.Index).CssClass = "imgOcultoCeldaWebHierarchical"
                            ElseIf columna.Key = "LINEA" Then
                                items(columna.Index).Text = CType(items(columna.Index).Value, Integer)
                            Else
                                If items(columna.Index).Value IsNot Nothing Then
                                    Select Case items(columna.Index).Value.GetType
                                        Case GetType(System.Boolean), GetType(System.Byte)
                                            ' Si es booleano, escribimos Sí/No en el lenguaje correspondiente
                                            If CType(items(columna.Index).Value, Boolean) Then
                                                items(columna.Index).Text = Textos(46)
                                            Else
                                                items(columna.Index).Text = Textos(47)
                                            End If
                                        Case GetType(System.Int16), GetType(System.Int32), GetType(System.Int64), GetType(System.Double)
                                            If Not IsDBNull(items(columna.Index).Value) Then items(columna.Index).Text = FSNLibrary.FormatNumber(CType(items(columna.Index).Value, Double), FSPMUser.NumberFormat)
                                        Case GetType(System.DateTime)
                                            ' Si es una fecha, la formateamos según las preferencias de usuario
                                            items(columna.Index).Text = FormatDate(items(columna.Index).Value, FSPMUser.DateFormat, "d")
                                    End Select
                                End If
                            End If
                        End If
                    Else
                        items(columna.Index).CssClass = "celdaValorNuloWebHierarchical"
                        items(columna.Index).Text = ""
                    End If
                Next
            End If
        End Sub
        Private Function FormatoDenEstado(ByVal sender As String) As String
            Dim sEstado As String = ""
            sEstado = Replace(sender, "(##" & CStr(TipoEstadoProceso.ConItemsSinValidar) & "##)", "(" & Textos(117) & ")")
            sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.Conproveasignados) & "##)", "(" & Textos(118) & ")")
            sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.conasignacionvalida) & "##)", "(" & Textos(119) & ")")
            sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.conpeticiones) & "##)", "(" & Textos(120) & ")")
            sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.ConObjetivosSinNotificar) & "##)", "(" & Textos(121) & ")")
            sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.PreadjYConObjNotificados) & "##)", "(" & Textos(122) & ")")
            sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.ParcialmenteCerrado) & "##)", "(" & Textos(123) & ")")
            sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.conadjudicaciones) & "##)", "(" & Textos(124) & ")")
            sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.ConAdjudicacionesNotificadas) & "##)", "(" & Textos(125) & ")")
            sEstado = Replace(sEstado, "(##" & CStr(TipoEstadoProceso.Cerrado) & "##)", "(" & Textos(126) & ")")
            Return sEstado
        End Function
        Private Sub whdgExcelExporter_Exported(sender As Object, e As Infragistics.Web.UI.GridControls.ExcelExportedEventArgs) Handles whdgExcelExporter.Exported
            Dim i As Integer
            e.Worksheet.Columns.Item(0).Width = 256 * 65
            For i = 1 To e.Worksheet.Columns.Count - 1
                e.Worksheet.Columns.Item(i).Width = 256 * 30
            Next
        End Sub
        ''' <summary>
        ''' Formatea los datos a exportar (numeros / fecha) o traduce los numeros por texos comprensibles (ejemplo: booleanos/EN_PROCESO).
        ''' </summary>
        ''' <param name="sender">grid</param>
        ''' <param name="e">evento</param>
        ''' <remarks>Llamada desde: Sistema; Tiempo máximo:0</remarks>
        Private Sub whdgExcelExporter_GridRecordItemExported(sender As Object, e As Infragistics.Web.UI.GridControls.GridRecordItemExportedEventArgs) Handles whdgExcelExporter.GridRecordItemExported
            Dim FormatoNum As String = FormatoNumeroExcel()

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorSolicitudes
            Select Case e.GridCell.Column.Key
                Case "ERROR_INTEGRACION"
                    If DBNullToBoolean(e.GridCell.Row.DataItem.Item("KO")) Then
                        whdgExportacion.Columns("ERROR_INTEGRACION").Hidden = False
                        whdgExportacion.GridView.Columns("ERROR_INTEGRACION").Hidden = False
                        e.WorksheetCell.Value = Textos(97) & " " & e.GridCell.Row.DataItem.Item("ERROR")
                    End If
                Case "ERROR_VALIDACION"
                    If CType(e.GridCell.Row.DataItem.Item("EST_VALIDACION"), Integer) > 1 AndAlso CType(e.GridCell.Row.DataItem.Item("EST_VALIDACION"), Integer) < 99 Then
                        whdgSolicitudes.Columns("ERROR_VALIDACION").Hidden = False
                        whdgSolicitudes.GridView.Columns("ERROR_VALIDACION").Hidden = False
                        e.WorksheetCell.Value = IIf(CType(e.GridCell.Row.DataItem.Item("EST_VALIDACION"), Integer) = 9,
                        Textos(108) & " " & e.GridCell.Row.DataItem.Item("FECHA_VALIDACION") & " " & Mid(Textos(109), 1, InStr(Textos(109), ".")) & " " & Textos(110),
                        Textos(108) & " " & e.GridCell.Row.DataItem.Item("FECHA_VALIDACION") & " " & Textos(109) & " " & e.GridCell.Row.DataItem.Item("ERROR_VALIDACION"))
                    End If
                Case "ESTADO", "ETAPA"
                    ' Para un Estado o Etapa reemplazamos su Id por el Texto en el lenguaje correspondiente
                    If {1, 2}.Contains(e.GridCell.Row.DataItem.Item("EN_PROCESO")) Then
                        ' En Proceso
                        e.WorksheetCell.Value = Textos(101)
                    Else
                        Select Case e.GridCell.Row.DataItem.Item("ESTADO")
                            Case TipoEstadoSolic.SCRechazada, TipoEstadoSolic.SCAnulada, TipoEstadoSolic.SCPendiente, TipoEstadoSolic.SCAprobada
                                ' Finalizada
                                e.WorksheetCell.Value = Textos(102)
                            Case Fullstep.FSNLibrary.TipoEstadoSolic.Rechazada
                                ' Rechazada
                                e.WorksheetCell.Value = Textos(103)
                            Case Fullstep.FSNLibrary.TipoEstadoSolic.Anulada
                                ' Anulada
                                e.WorksheetCell.Value = Textos(104)
                            Case Fullstep.FSNLibrary.TipoEstadoSolic.SCCerrada
                                ' Cerrada
                                e.WorksheetCell.Value = Textos(105)
                            Case Else  'Esta guardada o en curso.Se muestra la etapa actual
                                If e.GridCell.Column.Key = "ESTADO" Then
                                    Select Case e.GridCell.Row.DataItem.Item("ESTADO")
                                        Case Fullstep.FSNLibrary.TipoEstadoSolic.Guardada
                                            ' Guardada
                                            e.WorksheetCell.Value = Textos(106)
                                        Case Fullstep.FSNLibrary.TipoEstadoSolic.EnCurso
                                            ' En Curso
                                            e.WorksheetCell.Value = Textos(107)
                                    End Select
                                Else
                                    If e.GridCell.Row.DataItem.Item("ETAPA_ACTUAL") = "##" Then 'Etapas en paralelo
                                        e.WorksheetCell.Value = Textos(39)
                                    Else
                                        e.WorksheetCell.Value = e.GridCell.Row.DataItem.ITEM("ETAPA_ACTUAL")
                                    End If
                                End If
                        End Select
                    End If
                Case "PROCESOS"
                    If Not DBNullToSomething(e.GridCell.Row.DataItem.Item("PROCESOS")) = Nothing Then
                        e.WorksheetCell.Value = FormatoDenEstado(e.GridCell.Row.DataItem.Item("PROCESOS"))
                    Else
                        e.WorksheetCell.Value = String.Empty
                    End If
                Case "IMPORTE"
                    If e.GridCell.Value IsNot Nothing Then
                        If Not IsDBNull(e.GridCell.Value) Then
                            If {TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras, TiposDeDatos.TipoDeSolicitud.PedidoExpress, TiposDeDatos.TipoDeSolicitud.PedidoNegociado}.Contains(CType(e.GridCell.Row.DataItem.Item("TIPO_SOLIC"), Integer)) Then
                                e.WorksheetCell.Value = FSNLibrary.FormatNumber(CType(e.GridCell.Value, Double), FSPMUser.NumberFormat) & " " & e.GridCell.Row.DataItem.Item("MON")
                            ElseIf Not {"ID"}.Contains(e.GridCell.Column.Key) AndAlso
                        {TiposDeDatos.TipoDeSolicitud.Otros}.Contains(CType(e.GridCell.Row.DataItem.Item("TIPO_SOLIC"), Integer)) Then
                                e.WorksheetCell.CellFormat.FormatString = FormatoNum
                            End If
                        End If
                    Else
                        e.WorksheetCell.CellFormat.FormatString = FormatoNum
                    End If
                Case "USUARIO", "PETICIONARIO"
                    If {1, 2}.Contains(CType(e.GridCell.Row.DataItem.Item("EN_PROCESO"), Integer)) AndAlso e.GridCell.Column.Key = "USUARIO" Then
                        ' Escribimos "En Proceso"
                        e.WorksheetCell.Value = Textos(112)
                    Else
                        If (Not CType(e.GridCell.Row.DataItem.Item("TRASLADADA"), Boolean) AndAlso e.GridCell.Column.Key = "USUARIO") OrElse e.GridCell.Column.Key = "PETICIONARIO" Then
                            If DBNullToBoolean(e.GridCell.Row.DataItem.Item("VER_DETALLE_PER")) OrElse DBNullToBoolean(e.GridCell.Row.DataItem.Item("VER_DETALLE_PER_ROL")) Then
                                If e.GridCell.Column.Key = "USUARIO" And Not IsDBNull(e.GridCell.Row.DataItem.Item("PROVEEDOR")) AndAlso Not {1, 2}.Contains(CType(e.GridCell.Row.DataItem.Item("EN_PROCESO"), Integer)) Then
                                    e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item("PROVEEDOR")
                                Else
                                    If e.GridCell.Column.Key = "USUARIO" Then
                                        e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item("USU")
                                    Else
                                        e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item("PET")
                                    End If
                                End If
                            Else 'en blanco
                                e.WorksheetCell.Value = String.Empty
                            End If
                        ElseIf CType(e.GridCell.Row.DataItem.Item("TRASLADADA"), Boolean) AndAlso e.GridCell.Column.Key = "USUARIO" Then
                            e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item("TRASLADO_A_NOMBRE")
                        End If
                    End If
                Case "MOTIVO_VISIBILIDAD"
                    ' Para "Motivo Visibilidad" reemplazamos su Id por el Texto en el lenguaje correspondiente
                    Select Case CType(e.GridCell.Row.DataItem.Item("MOTIVO_VISIBILIDAD"), Integer)
                        Case MotivoVisibilidadSolicitud.SolicitudAbiertaUsted  'Solicitudes abiertas por usted
                            e.WorksheetCell.Value = Textos(111)
                        Case MotivoVisibilidadSolicitud.SolicitudParticipo
                            e.WorksheetCell.Value = Textos(112)
                        Case MotivoVisibilidadSolicitud.SolicitudPendiente
                            e.WorksheetCell.Value = Textos(113)
                        Case MotivoVisibilidadSolicitud.SolicitudPenditenteDevolucion
                            e.WorksheetCell.Value = Textos(114)
                        Case MotivoVisibilidadSolicitud.SolicitudTrasladadaEsperaDevolucion
                            e.WorksheetCell.Value = Textos(115)
                        Case Else 'Observador
                            e.WorksheetCell.Value = Textos(116)
                    End Select
                Case "PROVEEDOR_INSTANCIA"
                    If IsDBNull(e.GridCell.Row.DataItem.Item("PROVEEDOR_INSTANCIA")) Then
                        If e.GridCell.Row.DataItem.Item.DataView.table.Columns.Contains("PROVEEDOR") Then
                            e.WorksheetCell.Value = DBNullToStr(e.GridCell.Row.DataItem.Item("PROVEEDOR"))
                        End If
                    End If
                Case "ESTADO_HOMOLOGACION"
                    e.WorksheetCell.Value = DBNullToStr(e.GridCell.Row.DataItem.Item("ESTADO_HOMOLOGACION_DEN"))   'String.Format(cellText, DBNullToStr(DataRow("ESTADO_HOMOLOGACION_DEN")))
                Case Else
                    If _camposEditor.ContainsKey(e.GridCell.Column.Key) Then
                        Dim dsCampo As DataSet
                        Dim oInstancia As Instancia
                        Dim oValor As Object = DBNull.Value
                        oInstancia = FSPMServer.Get_Instancia
                        '******************************************************************************dsCampo = oInstancia.cargarCampoOrigen(e.GridCell.Row.DataItem.Item("ID"), Split(e.GridCell.Column.Key, "_")(Split(e.GridCell.Column.Key, "_").Length - 1))
                        oInstancia = Nothing
                        If Not dsCampo Is Nothing AndAlso dsCampo.Tables(0).Rows.Count > 0 AndAlso Not IsDBNull(dsCampo.Tables(0).Rows(0).Item("VALOR_TEXT")) Then
                            oValor = dsCampo.Tables(0).Rows(0).Item("VALOR_TEXT")
                        End If
                        dsCampo = Nothing
                        If IsDBNull(oValor) Then
                            e.WorksheetCell.Value = ""
                        Else
                            e.WorksheetCell.Value = StripTags(HttpUtility.HtmlDecode(Server.UrlDecode(oValor)))
                        End If
                    Else
                        Select Case e.GridCell.Column.Type
                            Case System.Type.GetType("System.Boolean"), System.Type.GetType("System.Byte")
                                If e.GridCell.Value IsNot System.DBNull.Value Then e.WorksheetCell.Value = IIf(e.GridCell.Value, Textos(46), Textos(47))
                            Case System.Type.GetType("System.Int16"), System.Type.GetType("System.Int32"), System.Type.GetType("System.Int64"), System.Type.GetType("System.Double")
                                If Not {"ID"}.Contains(e.GridCell.Column.Key) Then
                                    e.WorksheetCell.CellFormat.FormatString = FormatoNum
                                End If
                            Case System.Type.GetType("System.DateTime")
                                e.WorksheetCell.CellFormat.FormatString = FSPMUser.DateFormat.ShortDatePattern
                            Case Else
                        End Select
                    End If
            End Select
        End Sub
        Private Sub whdgExcelExporter_RowExported(sender As Object, e As Infragistics.Web.UI.GridControls.ExcelRowExportedEventArgs) Handles whdgExcelExporter.RowExported
            Dim nivel As Integer = e.CurrentOutlineLevel
            If e.IsHeaderRow Then
                e.Worksheet.Rows(e.CurrentRowIndex).CellFormat.Font.Bold = Infragistics.Documents.Excel.ExcelDefaultableBoolean.True
            End If
        End Sub
#End Region
#Region "Funciones"
        Private Shared Function Generar_SentenciaWhere_Filtro(ByVal FiltroCondiciones As List(Of EscenarioFiltroCondicion), Optional ByRef FiltroDesglose As Boolean = False)
            Dim sentenciaWhere As StringBuilder = New StringBuilder()
            Dim clausula As String = String.Empty
            Dim campo As String
            Dim operadorLista As String
            Dim operadorEstadoOtras As String

            For Each condicion As EscenarioFiltroCondicion In FiltroCondiciones
                If condicion.IdCampo = 0 Then
                    ' Operadores
                    Select Case condicion.Operador
                        Case Operadores.Formula.Y
                            sentenciaWhere.Append(" AND ")
                        Case Operadores.Formula.O
                            sentenciaWhere.Append(" OR ")
                        Case Operadores.Formula.PARENTESIS_ABIERTO
                            sentenciaWhere.Append(" ( ")
                        Case Operadores.Formula.PARENTESIS_CERRADO
                            sentenciaWhere.Append(" ) ")
                    End Select
                ElseIf condicion.Valores.Count > 0 Then
                    ' Condicion de campo que tiene valores
                    clausula = "({0})"

                    If condicion.EsCampoGeneral Then
                        Select Case condicion.IdCampo
                            Case CamposGeneralesVisor.FECHATRASLADO, CamposGeneralesVisor.PERSONATRASLADO,
                            CamposGeneralesVisor.PROVEEDORTRASLADO, CamposGeneralesVisor.FECHALIMITEDEVOLUCION
                                clausula = String.Format(clausula, "(TRASLADADA=1 AND {0})")
                            Case CamposGeneralesVisor.ESTADO
                                If condicion.Valores.Count = 8 Then
                                    'Se han seleccionado todas las opciones
                                    clausula = String.Format(clausula, "1 = 1")
                                End If
                            Case CamposGeneralesVisor.MOTIVOVISIBILIDAD
                                If condicion.Valores.Count = [Enum].GetValues(GetType(MotivoVisibilidadSolicitud)).Length Then
                                    'Se han seleccionado todas las opciones
                                    clausula = String.Format(clausula, "1 = 1")
                                End If
                            Case CamposGeneralesVisor.ESTADOHOMOLOGACION
                                clausula = String.Format(clausula, "1 = 1")
                        End Select
                    Else
                        If Split(condicion.Denominacion_BD, "_").Length = 4 Then FiltroDesglose = True
                    End If

                    ' Si hay parámetros que introducir a la condición
                    If clausula.IndexOf("{0}") <> -1 Then
                        Select Case condicion.TipoCampoGS
                            Case TiposDeDatos.TipoCampoGS.Material, TiposDeDatos.TipoCampoGS.CodArticulo,
                            TiposDeDatos.TipoCampoGS.DenArticulo, TiposDeDatos.TipoCampoGS.NuevoCodArticulo,
                            TiposDeDatos.TipoCampoGS.Pais, TiposDeDatos.TipoCampoGS.Provincia, TiposDeDatos.TipoCampoGS.Unidad,
                            TiposDeDatos.TipoCampoGS.Proveedor, TiposDeDatos.TipoCampoGS.FormaPago, TiposDeDatos.TipoCampoGS.Moneda,
                            TiposDeDatos.TipoCampoGS.Dest, TiposDeDatos.TipoCampoGS.Contacto, TiposDeDatos.TipoCampoGS.Persona,
                            TiposDeDatos.TipoCampoGS.ProveContacto, TiposDeDatos.TipoCampoGS.Rol, TiposDeDatos.TipoCampoGS.UnidadOrganizativa,
                            TiposDeDatos.TipoCampoGS.Departamento, TiposDeDatos.TipoCampoGS.OrganizacionCompras, TiposDeDatos.TipoCampoGS.Centro,
                            TiposDeDatos.TipoCampoGS.Almacen, TiposDeDatos.TipoCampoGS.CentroCoste, TiposDeDatos.TipoCampoGS.Partida,
                            TiposDeDatos.TipoCampoGS.Activo, TiposDeDatos.TipoCampoGS.TipoPedido, TiposDeDatos.TipoCampoGS.UnidadPedido,
                            TiposDeDatos.TipoCampoGS.ProveedorERP, TiposDeDatos.TipoCampoGS.Comprador, TiposDeDatos.TipoCampoGS.Empresa
                                campo = "ISNULL (" & condicion.Denominacion_BD & IIf(condicion.EsCampoGeneral, String.Empty, "_COD") & ",'')"
                            Case Else
                                If condicion.EsLista AndAlso Not condicion.EsCampoGeneral _
                                AndAlso Not condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoBoolean _
                                AndAlso Not condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico _
                                AndAlso Not condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha Then
                                    campo = "ISNULL (" & condicion.Denominacion_BD & "_COD,'')"
                                Else
                                    Select Case condicion.TipoCampo
                                        Case TiposDeDatos.TipoGeneral.TipoFecha
                                            campo = "ISNULL(CONVERT(DATE," & condicion.Denominacion_BD & "),'')"
                                        Case Else
                                            If condicion.IdCampo = CamposGeneralesVisor.IDENTIFICADOR AndAlso condicion.EsCampoGeneral Then
                                                campo = "RESULT." & condicion.Denominacion_BD
                                            Else
                                                campo = condicion.Denominacion_BD
                                            End If
                                    End Select
                                End If
                        End Select

                        'Establecemos el nombre del campo
                        clausula = String.Format(clausula, campo & " {0}")

                        Select Case condicion.Operador
                            Case Operadores.Campos.CONTIENE
                                If Not condicion.EsCampoGeneral AndAlso {TiposDeDatos.TipoCampoGS.CodArticulo, TiposDeDatos.TipoCampoGS.DenArticulo, TiposDeDatos.TipoCampoGS.NuevoCodArticulo}.Contains(condicion.TipoCampoGS) Then
                                    clausula = String.Format(clausula, "LIKE '%" & strToSQLLIKE(condicion.Valores(1)) & "%'")
                                Else
                                    clausula = String.Format(clausula, "LIKE '%" & strToSQLLIKE(condicion.Valores(0)) & "%'")
                                End If
                            Case Operadores.Campos.EMPIEZAPOR
                                clausula = String.Format(clausula, "LIKE '" & strToSQLLIKE(condicion.Valores(0)) & "%'")
                            Case Operadores.Campos.TERMINAEN
                                clausula = String.Format(clausula, "LIKE '%" & strToSQLLIKE(condicion.Valores(0)) & "'")
                            Case Operadores.Campos.NOCONTIENE
                                clausula = String.Format(clausula, "NOT LIKE '%" & strToSQLLIKE(condicion.Valores(0)) & "%'")
                            Case Operadores.Campos.ENTRE
                                If condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha Then
                                    clausula = String.Format(clausula, "BETWEEN Convert(date,'" & Format(CType(condicion.Valores(0), Date), "MM-dd-yyyy HH:mm:ss") & "',110) AND Convert(date,'" & Format(CType(condicion.Valores(1), Date), "MM-dd-yyyy HH:mm:ss") & "',110)")
                                Else
                                    clausula = String.Format(clausula, "BETWEEN " & condicion.Valores(0) & " AND " & condicion.Valores(1))
                                End If
                            Case Operadores.Campos.ES, Operadores.Campos.NOES
                                If condicion.Operador = Operadores.Campos.ES Then
                                    operadorLista = "IN ({0})"
                                    operadorEstadoOtras = "NOT IN ({0})"
                                Else
                                    operadorLista = "NOT IN ({0})"
                                    operadorEstadoOtras = "IN ({0})"
                                End If
                                Dim condicionesEstado As New List(Of String)
                                If condicion.EsCampoGeneral AndAlso condicion.IdCampo = CamposGeneralesVisor.ESTADO Then
                                    'Tenemos un problema. Si busca por estado pendiente (ademas de otros) pero no por las de en curso, tenemos que añadir a la sentencia
                                    'que el motivo de visibilidad no sea abt por ud, participo y otras(observador). ej: Pide pendientes y guardadas, 
                                    'sentencia where= estado in (2,0). El stored calcula las tablas temporales de pendientes, pendientes devolucion y trasladas para las pendientes
                                    'y las de abiertas por usted para las guardadas. Esta ocurriendo, que una en curso cumple los requisitos, ya que esta entre las abiertas
                                    'por ud y con estado 0. La unica forma de diferenciar es que no este en las abiertas por ud con estado 2.
                                    If condicion.Valores.Any(Function(x) Split(x, "###")(0) = "1000") AndAlso Not condicion.Valores.Any(Function(x) Split(x, "###")(0) = "2") Then
                                        condicionesEstado = condicion.Valores.Where(Function(x) Split(x, "###")(0) <> "1000").ToList
                                        clausula = "(" & IIf(condicionesEstado.Any, clausula & " {1} ", "") & "(ISNULL(ESTADO,'') {2}(2) {3} ISNULL(MOTIVO_VISIBILIDAD,'') {4}(3,4,5)))"
                                    Else
                                        condicionesEstado = condicion.Valores
                                    End If
                                    If Not condicion.Valores.Any(Function(x) Split(x, "###")(0) = "1000") AndAlso condicion.Valores.Any(Function(x) Split(x, "###")(0) = "2") Then
                                        condicionesEstado = condicion.Valores.Where(Function(x) Split(x, "###")(0) <> "2").ToList
                                        clausula = "(" & IIf(condicionesEstado.Any, clausula & " {1} ", "") & "(ISNULL(ESTADO,'') {2}(2) {3} ISNULL(MOTIVO_VISIBILIDAD,'') {4}(1,2,6,7)))"
                                    End If
                                    If condicion.Operador = Operadores.Campos.ES Then
                                        clausula = String.Format(clausula, "{0}", "OR", "IN", "AND", "IN")
                                    Else
                                        clausula = String.Format(clausula, "{0}", "AND", "NOT IN", "OR", "NOT IN")
                                    End If
                                End If

                                If condicion.EsCampoGeneral And condicion.IdCampo = CamposGeneralesVisor.ESTADO And condicion.Valores.Any(Function(x) Split(x, "###")(0) = VALOR_ESTADO_OTRAS) Then
                                    operadorEstadoOtras = String.Format(operadorEstadoOtras, Replace(VALOR_ESTADO_OTRAS, "#", ","))
                                    ' Si el campo es Estado y se ha seleccionado la opcion Otras
                                    If (condicion.Valores.Count > 1) Then
                                        clausula = String.Format(clausula, "{0} OR " & campo & " " & operadorEstadoOtras)
                                    Else
                                        clausula = String.Format(clausula, operadorEstadoOtras)
                                    End If
                                End If

                                'Establecemos el operador de lista (si es necesario)
                                clausula = String.Format(clausula, operadorLista)

                                If condicion.EsCampoGeneral AndAlso {CamposGeneralesVisor.ESTADO, CamposGeneralesVisor.MOTIVOVISIBILIDAD, CamposGeneralesVisor.ESTADOHOMOLOGACION}.Contains(condicion.IdCampo) Then
                                    ' Si es el campo Estado o MotivoVisibilidad, descomponemos los valores con ids compuestos
                                    If condicion.IdCampo = CamposGeneralesVisor.MOTIVOVISIBILIDAD Then
                                        clausula = String.Format(clausula, String.Join(",", condicion.Valores.Where(Function(x) Split(x, "###")(0) <> VALOR_ESTADO_OTRAS).SelectMany(Function(x) Split(Replace(Split(x, "###")(0), "1000", "2"), "#"))))
                                    Else
                                        clausula = String.Format(clausula, String.Join(",", condicionesEstado.Where(Function(x) Split(x, "###")(0) <> VALOR_ESTADO_OTRAS).SelectMany(Function(x) Split(Replace(Split(x, "###")(0), "1000", "2"), "#"))))
                                    End If
                                Else
                                    Select Case condicion.TipoCampoGS
                                        Case -2, -1, TiposDeDatos.TipoCampoGS.Proveedor, TiposDeDatos.TipoCampoGS.Material, TiposDeDatos.TipoCampoGS.CodArticulo,
                                                TiposDeDatos.TipoCampoGS.DenArticulo, TiposDeDatos.TipoCampoGS.NuevoCodArticulo, TiposDeDatos.TipoCampoGS.Empresa  '-2=usuario,-1=peticionario
                                            ' Si es un usuario o peticionario cogemos los ids del primer valor de la condicion separados por ###
                                            clausula = String.Format(clausula, String.Join(",", Split(condicion.Valores(0), "###").Select(Function(x) "'" & x & "'")))
                                        Case TiposDeDatos.TipoCampoGS.Provincia
                                            ' Si es una provincia cogemos el id del primer valor a partir del # e interpretado como string
                                            clausula = String.Format(clausula, "'" & Split(condicion.Valores(0), "#")(1) & "'")
                                        Case Else
                                            Select Case condicion.TipoCampo
                                                Case TiposDeDatos.TipoGeneral.TipoNumerico
                                                    clausula = String.Format(clausula, String.Join(",", condicion.Valores.Select(Function(x) Split(x, "###")(1))))
                                                Case TiposDeDatos.TipoGeneral.TipoFecha
                                                    clausula = String.Format(clausula, String.Join(",", condicion.Valores.Select(Function(x) "Convert(date,'" & Format(CType(Split(x, "###")(1), Date), "MM-dd-yyyy HH:mm:ss") & "',110)")))
                                                Case Else
                                                    clausula = String.Format(clausula, String.Join(",", condicion.Valores.Select(Function(x) "'" & Split(x, "###")(0) & "'")))
                                            End Select
                                    End Select
                                End If
                            Case Operadores.Campos.HACE
                                Dim intervalo As DateInterval
                                Dim valor As Double = CType(condicion.Valores(1), Double)
                                Select Case condicion.Valores(0)
                                    Case 0 'hora
                                        intervalo = DateInterval.Hour
                                    Case 1
                                        intervalo = DateInterval.Day
                                        valor = valor
                                    Case 2
                                        intervalo = DateInterval.Day
                                        valor = valor * 7
                                    Case 3 'mes
                                        intervalo = DateInterval.Month
                                    Case Else 'años
                                        intervalo = DateInterval.Year
                                End Select
                                Dim fechaRelativa As Date = DateAdd(intervalo, (-1) * valor, Now.Date)
                                clausula = String.Format(clausula, ">= Convert(date,'" & Format(fechaRelativa, "MM-dd-yyyy HH:mm:ss") & "',110)")
                            Case Operadores.Campos.PERIODO
                                Dim fechaPeriodo As Date
                                Select Case CType(condicion.Valores(0), Integer)
                                    Case 0 'hoy
                                        fechaPeriodo = Now.Date
                                    Case 1 'semana
                                        fechaPeriodo = DateAdd(DateInterval.Day, (-1) * IIf(Now.DayOfWeek = DayOfWeek.Sunday, 6, Now.DayOfWeek - 1), Now.Date)
                                    Case 2 'mes
                                        fechaPeriodo = New Date(Now.Year, Now.Month, 1)
                                    Case 3 'trimestre
                                        fechaPeriodo = New Date(Now.Year, (((Now.Month - 1) \ 3) * 3) + 1, 1)
                                    Case 4 'semestre
                                        fechaPeriodo = New Date(Now.Year, (((Now.Month - 1) \ 6) * 6) + 1, 1)
                                    Case Else 'año
                                        fechaPeriodo = New Date(Now.Year, 1, 1)
                                End Select
                                clausula = String.Format(clausula, ">= Convert(date,'" & Format(fechaPeriodo, "MM-dd-yyyy HH:mm:ss") & "',110)")
                            Case Else
                                Select Case condicion.Operador
                                    Case Operadores.Campos.IGUAL
                                        clausula = String.Format(clausula, "= {0}")
                                    Case Operadores.Campos.DISTINTO
                                        clausula = String.Format(clausula, "<> {0}")
                                    Case Operadores.Campos.MAYOR
                                        clausula = String.Format(clausula, "> {0} ")
                                    Case Operadores.Campos.MAYORIGUAL
                                        clausula = String.Format(clausula, ">= {0}")
                                    Case Operadores.Campos.MENOR
                                        clausula = String.Format(clausula, "< {0}")
                                    Case Operadores.Campos.MENORIGUAL
                                        clausula = String.Format(clausula, "<= {0}")
                                End Select
                                If condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha Then
                                    clausula = String.Format(clausula, "Convert(date,'" & Format(CType(condicion.Valores(0), Date), "MM-dd-yyyy HH:mm:ss") & "',110)")
                                ElseIf condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico Then
                                    clausula = String.Format(clausula, condicion.Valores(0))
                                Else
                                    clausula = String.Format(clausula, "'" & condicion.Valores(0) & "'")
                                End If
                        End Select
                    End If
                    'Añadimos la cláusula a la sentencia
                    sentenciaWhere.Append(clausula)
                Else
                    'Si estamos en una formula avanzada en la que inicialmente la condicion tenia un valor y se edita en pantalla para no tener valor, necesitamos
                    'que el hueco de esa condicion exista para que la formula no casque
                    sentenciaWhere.Append("1=1")
                End If
            Next
            Return sentenciaWhere.ToString()
        End Function
        Private Shared Function Generar_SentenciaWhere_CamposGenerales(ByVal FiltroCondiciones As List(Of EscenarioFiltroCondicion),
                    Optional ByRef AbtasUsted As Nullable(Of Boolean) = True, Optional ByRef Participo As Nullable(Of Boolean) = True,
                    Optional ByRef Pendientes As Nullable(Of Boolean) = True, Optional ByRef PendientesDevolucion As Nullable(Of Boolean) = True,
                    Optional ByRef Trasladadas As Nullable(Of Boolean) = True, Optional ByRef ObservadorSustitucion As Nullable(Of Boolean) = True, Optional ByVal idsSolicitud As String = "")
            Dim sentenciaWhere As StringBuilder = New StringBuilder
            Dim clausula As String = String.Empty
            Dim OperadorLista As String
            Dim OperadorEstadoOtras As String

            Dim crearCondicion As Boolean = True

            If idsSolicitud <> "" Then
                clausula = "(S.ID IN (" & idsSolicitud & "))"
                sentenciaWhere.Append(clausula)
            End If

            'Recorremos las condiciones generales de los campos FechaAlta, Identificador, Peticionario, TipoSolicitud y Estado que tengan valores
            For Each condicion As EscenarioFiltroCondicion In FiltroCondiciones.Where(Function(x) x.EsCampoGeneral And {CamposGeneralesVisor.FECHAALTA, CamposGeneralesVisor.IDENTIFICADOR, CamposGeneralesVisor.PETICIONARIO, CamposGeneralesVisor.TIPOSOLICITUD, CamposGeneralesVisor.ESTADO, CamposGeneralesVisor.ESTADOHOMOLOGACION}.Contains(x.IdCampo) And x.Valores.Count > 0)
                clausula = "({0})"
                ' Si hay más cláusulas en la sentencia añadimos un AND previamente
                If sentenciaWhere.Length > 0 Then clausula = " AND " & clausula

                Select Case condicion.IdCampo
                    Case CamposGeneralesVisor.FECHAALTA
                        clausula = String.Format(clausula, "I.FEC_ALTA {0}")
                    Case CamposGeneralesVisor.IDENTIFICADOR
                        clausula = String.Format(clausula, "I.ID {0}")
                    Case CamposGeneralesVisor.PETICIONARIO
                        clausula = String.Format(clausula, "I.PETICIONARIO {0}")
                    Case CamposGeneralesVisor.TIPOSOLICITUD
                        clausula = String.Format(clausula, "S.ID {0}")
                    Case CamposGeneralesVisor.ESTADO
                        If condicion.Valores.Count = 8 Then
                            'Se han seleccionado todas las opciones
                            clausula = String.Format(clausula, "1 = 1")
                        Else
                            clausula = String.Format(clausula, "I.ESTADO {0}")
                        End If
                    Case CamposGeneralesVisor.ESTADOHOMOLOGACION
                        clausula = String.Format(clausula, "IV.ESTADO_HOMOLOGACION {0}")
                End Select

                ' Si hay parámetros que introducir a la condición
                If clausula.IndexOf("{0}") <> -1 Then
                    Select Case condicion.Operador
                        Case Operadores.Campos.ENTRE
                            If condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha Then
                                clausula = String.Format(clausula, "BETWEEN Convert(date,'" & Format(CType(condicion.Valores(0), Date), "MM-dd-yyyy HH:mm:ss") & "',110) AND Convert(date,'" & Format(CType(condicion.Valores(1), Date), "MM-dd-yyyy HH:mm:ss") & "',110)")
                            Else
                                clausula = String.Format(clausula, "BETWEEN " & condicion.Valores(0) & " AND " & condicion.Valores(1))
                            End If
                        Case Operadores.Campos.ES, Operadores.Campos.NOES
                            If condicion.Operador = Operadores.Campos.ES Then
                                OperadorLista = "IN ({0})"
                                OperadorEstadoOtras = "NOT IN ({0})"
                            Else
                                OperadorLista = "NOT IN ({0})"
                                OperadorEstadoOtras = "IN ({0})"
                            End If

                            If condicion.IdCampo = CamposGeneralesVisor.ESTADO And condicion.Valores.Any(Function(x) Split(x, "###")(0) = VALOR_ESTADO_OTRAS) Then
                                OperadorEstadoOtras = String.Format(OperadorEstadoOtras, Replace(VALOR_ESTADO_OTRAS, "#", ","))
                                ' Si el campo es Estado y se ha seleccionado la opcion Otras
                                If (condicion.Valores.Count > 1) Then
                                    clausula = String.Format(clausula, "{0} OR I.Estado " & OperadorEstadoOtras)
                                Else
                                    clausula = String.Format(clausula, OperadorEstadoOtras)
                                End If
                            End If

                            'Establecemos el operador de lista (si es necesario)
                            clausula = String.Format(clausula, OperadorLista)

                            If condicion.IdCampo = CamposGeneralesVisor.ESTADO Then
                                ' Si es el campo Estado o MotivoVisibilidad, descomponemos los valores con ids compuestos                               
                                clausula = String.Format(clausula, String.Join(",", condicion.Valores.Where(Function(x) Split(x, "###")(0) <> VALOR_ESTADO_OTRAS).SelectMany(Function(x) Split(Replace(Split(x, "###")(0), "1000", "2"), "#"))))

                                For Each valor As String In condicion.Valores
                                    Select Case Split(valor, "###")(0)
                                        Case "1000" 'Pendientes
                                            If condicion.Operador = Operadores.Campos.ES Then
                                                If AbtasUsted Is Nothing Then AbtasUsted = False
                                                If Participo Is Nothing Then Participo = False
                                                If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = False
                                                Pendientes = True
                                                PendientesDevolucion = True
                                                Trasladadas = True
                                            Else
                                                If Pendientes Is Nothing Then Pendientes = False
                                                If PendientesDevolucion Is Nothing Then PendientesDevolucion = False
                                                If Trasladadas Is Nothing Then Trasladadas = False
                                                AbtasUsted = True
                                                Participo = True
                                                ObservadorSustitucion = True
                                            End If
                                        Case "0" 'Guardadas
                                            If condicion.Operador = Operadores.Campos.ES Then
                                                If Participo Is Nothing Then Participo = False
                                                If Pendientes Is Nothing Then Pendientes = False
                                                If PendientesDevolucion Is Nothing Then PendientesDevolucion = False
                                                If Trasladadas Is Nothing Then Trasladadas = False
                                                If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = False
                                                AbtasUsted = True
                                            Else
                                                If AbtasUsted Is Nothing Then AbtasUsted = False
                                                Participo = True
                                                Pendientes = True
                                                PendientesDevolucion = True
                                                Trasladadas = True
                                                ObservadorSustitucion = True
                                            End If
                                        Case "2" 'En curso
                                            If condicion.Operador = Operadores.Campos.ES Then
                                                If Pendientes Is Nothing Then Pendientes = False
                                                If PendientesDevolucion Is Nothing Then PendientesDevolucion = False
                                                If Trasladadas Is Nothing Then Trasladadas = False
                                                AbtasUsted = True
                                                Participo = True
                                                ObservadorSustitucion = True
                                            Else
                                                If AbtasUsted Is Nothing Then AbtasUsted = False
                                                If Participo Is Nothing Then Participo = False
                                                If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = False
                                                Pendientes = True
                                                PendientesDevolucion = True
                                                Trasladadas = True
                                            End If
                                        Case "6" 'Rechazadas
                                            If condicion.Operador = Operadores.Campos.ES Then
                                                If PendientesDevolucion Is Nothing Then PendientesDevolucion = False
                                                If Trasladadas Is Nothing Then Trasladadas = False
                                                AbtasUsted = True
                                                Participo = True
                                                'Pendientes = False
                                                ObservadorSustitucion = True
                                            Else
                                                If AbtasUsted Is Nothing Then AbtasUsted = False
                                                If Participo Is Nothing Then Participo = False
                                                If Pendientes Is Nothing Then Pendientes = True
                                                If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = False
                                                PendientesDevolucion = True
                                                Trasladadas = True
                                            End If
                                        Case "8" 'Anuladas
                                            If condicion.Operador = Operadores.Campos.ES Then
                                                If PendientesDevolucion Is Nothing Then PendientesDevolucion = False
                                                If Trasladadas Is Nothing Then Trasladadas = False
                                                AbtasUsted = True
                                                Participo = True
                                                'Pendientes = False
                                                ObservadorSustitucion = True
                                            Else
                                                If AbtasUsted Is Nothing Then AbtasUsted = False
                                                If Participo Is Nothing Then Participo = False
                                                If Pendientes Is Nothing Then Pendientes = True
                                                If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = False
                                                PendientesDevolucion = True
                                                Trasladadas = True
                                            End If
                                        Case "100#101#102#103" 'Finalizadas
                                            If condicion.Operador = Operadores.Campos.ES Then
                                                If Pendientes Is Nothing Then Pendientes = False
                                                If PendientesDevolucion Is Nothing Then PendientesDevolucion = False
                                                If Trasladadas Is Nothing Then Trasladadas = False
                                                AbtasUsted = True
                                                Participo = True
                                                ObservadorSustitucion = True
                                            Else
                                                If AbtasUsted Is Nothing Then AbtasUsted = False
                                                If Participo Is Nothing Then Participo = False
                                                If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = False
                                                Pendientes = True
                                                PendientesDevolucion = True
                                                Trasladadas = True
                                            End If
                                        Case "104" 'Cerradas
                                            If condicion.Operador = Operadores.Campos.ES Then
                                                If Pendientes Is Nothing Then Pendientes = False
                                                If PendientesDevolucion Is Nothing Then PendientesDevolucion = False
                                                If Trasladadas Is Nothing Then Trasladadas = False
                                                AbtasUsted = True
                                                Participo = True
                                                ObservadorSustitucion = True
                                            Else
                                                If AbtasUsted Is Nothing Then AbtasUsted = False
                                                If Participo Is Nothing Then Participo = False
                                                If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = False
                                                Pendientes = True
                                                PendientesDevolucion = True
                                                Trasladadas = True
                                            End If
                                        Case Else
                                            If condicion.Operador = Operadores.Campos.ES Then
                                                If AbtasUsted Is Nothing Then AbtasUsted = False
                                                If Participo Is Nothing Then Participo = False
                                                If Pendientes Is Nothing Then Pendientes = False
                                                If PendientesDevolucion Is Nothing Then PendientesDevolucion = False
                                                If Trasladadas Is Nothing Then Trasladadas = False
                                                ObservadorSustitucion = True
                                            Else
                                                If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = False
                                                AbtasUsted = True
                                                Participo = True
                                                Pendientes = True
                                                PendientesDevolucion = True
                                                Trasladadas = True
                                            End If
                                    End Select
                                Next
                            Else
                                Select Case condicion.TipoCampoGS
                                    Case -1 '-1=peticionario
                                        ' Si es un usuario o peticionario cogemos los ids del primer valor de la condicion separados por ###
                                        clausula = String.Format(clausula, String.Join(",", Split(condicion.Valores(0), "###").Select(Function(x) "'" & x & "'")))
                                    Case TiposDeDatos.TipoCampoGS.EstadoHomologacion
                                        clausula = String.Format(clausula, String.Join(",", condicion.Valores.Select(Function(x) "'" & Split(x, "###")(0) & "'")))
                                    Case Else
                                        Select Case condicion.TipoCampo
                                            Case TiposDeDatos.TipoGeneral.TipoFecha
                                                clausula = String.Format(clausula, String.Join(",", condicion.Valores.Select(Function(x) "Convert(date,'" & Format(CType(x, Date), "MM-dd-yyyy HH:mm:ss") & "',110),")))
                                            Case TiposDeDatos.TipoGeneral.TipoNumerico
                                                clausula = String.Format(clausula, String.Join(",", condicion.Valores))
                                            Case Else
                                                clausula = String.Format(clausula, String.Join(",", condicion.Valores.Select(Function(x) "'" & strToSQLLIKE(Split(x, "###")(0)) & "'")))
                                        End Select
                                End Select
                            End If
                        Case Operadores.Campos.HACE
                            Dim intervalo As DateInterval
                            Dim valor As Double = CType(condicion.Valores(1), Double)
                            Select Case condicion.Valores(0)
                                Case 0 'hora
                                    intervalo = DateInterval.Hour
                                Case 1, 2 'dia, semanas (que multiplicaremos el valor por 7
                                    intervalo = DateInterval.Day
                                    valor = valor * 7
                                Case 3 'mes
                                    intervalo = DateInterval.Month
                                Case Else 'años
                                    intervalo = DateInterval.Year
                            End Select
                            Dim fechaRelativa As Date = DateAdd(intervalo, (-1) * valor, Now.Date)
                            clausula = String.Format(clausula, ">= Convert(date,'" & Format(fechaRelativa, "MM-dd-yyyy HH:mm:ss") & "',110)")
                        Case Operadores.Campos.PERIODO
                            Dim fechaPeriodo As Date
                            Select Case CType(condicion.Valores(0), Integer)
                                Case 0 'hoy
                                    fechaPeriodo = Now.Date
                                Case 1 'semana
                                    fechaPeriodo = DateAdd(DateInterval.Day, (-1) * IIf(Now.DayOfWeek = DayOfWeek.Sunday, 6, Now.DayOfWeek - 1), Now.Date)
                                Case 2 'mes
                                    fechaPeriodo = New Date(Now.Year, Now.Month, 1)
                                Case 3 'trimestre
                                    fechaPeriodo = New Date(Now.Year, (((Now.Month - 1) \ 3) * 3) + 1, 1)
                                Case 4 'semestre
                                    fechaPeriodo = New Date(Now.Year, (((Now.Month - 1) \ 6) * 6) + 1, 1)
                                Case Else 'año
                                    fechaPeriodo = New Date(Now.Year, 1, 1)
                            End Select
                            clausula = String.Format(clausula, ">= Convert(date,'" & Format(fechaPeriodo, "MM-dd-yyyy HH:mm:ss") & "',110)")
                        Case Else
                            Select Case condicion.Operador
                                Case Operadores.Campos.IGUAL
                                    clausula = String.Format(clausula, "= {0}")
                                Case Operadores.Campos.DISTINTO
                                    clausula = String.Format(clausula, "<> {0}")
                                Case Operadores.Campos.MAYOR
                                    clausula = String.Format(clausula, "> {0}")
                                Case Operadores.Campos.MAYORIGUAL
                                    clausula = String.Format(clausula, ">= {0}")
                                Case Operadores.Campos.MENOR
                                    clausula = String.Format(clausula, "< {0}")
                                Case Operadores.Campos.MENORIGUAL
                                    clausula = String.Format(clausula, "<= {0}")
                            End Select
                            If condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoFecha Then
                                clausula = String.Format(clausula, "Convert(date,'" & Format(CType(condicion.Valores(0), Date), "MM-dd-yyyy HH:mm:ss") & "',110)")
                            ElseIf condicion.TipoCampo = TiposDeDatos.TipoGeneral.TipoNumerico Then
                                clausula = String.Format(clausula, condicion.Valores(0))
                            Else
                                clausula = String.Format(clausula, "'" & strToSQLLIKE(condicion.Valores(0)) & "'")
                            End If
                    End Select
                End If
                'Añadimos las cláusula a la sentencia
                sentenciaWhere.Append(clausula)
            Next
            If AbtasUsted Is Nothing Then AbtasUsted = True
            If Participo Is Nothing Then Participo = True
            If Pendientes Is Nothing Then Pendientes = True
            If PendientesDevolucion Is Nothing Then PendientesDevolucion = True
            If Trasladadas Is Nothing Then Trasladadas = True
            If ObservadorSustitucion Is Nothing Then ObservadorSustitucion = True
            Return sentenciaWhere.ToString()
        End Function
        Private Shared Function BuscarProveedorArticulo(ByVal FiltroCondiciones As List(Of EscenarioFiltroCondicion),
                                                    ByVal CamposVista As List(Of Escenario_Campo)) As Integer
            Dim instanciaDesgloseMaterialProveedor As Integer = 0
            Dim instanciaDesgloseMaterialArticulo As Integer = 0
            For Each condicion As EscenarioFiltroCondicion In FiltroCondiciones
                If condicion.EsCampoGeneral AndAlso condicion.IdCampo = CamposGeneralesVisor.PROVEEDOR Then instanciaDesgloseMaterialProveedor = 1
                If condicion.EsCampoGeneral AndAlso condicion.IdCampo = CamposGeneralesVisor.ARTICULO Then instanciaDesgloseMaterialArticulo = 2
            Next
            If CamposVista IsNot Nothing Then
                For Each Campo As Escenario_Campo In CamposVista
                    If Campo.EsCampoGeneral AndAlso Campo.Id = CamposGeneralesVisor.PROVEEDOR Then instanciaDesgloseMaterialProveedor = 1
                    If Campo.EsCampoGeneral AndAlso Campo.Id = CamposGeneralesVisor.ARTICULO Then instanciaDesgloseMaterialArticulo = 2
                Next
            End If
            Return instanciaDesgloseMaterialProveedor + instanciaDesgloseMaterialArticulo
        End Function
        Private Sub Establecer_Orden_Columnas_Grid_Solicitudes(ByVal Orden As String)
            whdgSolicitudes.GridView.Behaviors.Sorting.SortedColumns.Clear()

            For Each columnaOrdenada As String In Orden.Split(",")
                If Not columnaOrdenada = "" AndAlso Not whdgSolicitudes.GridView.Columns.Item(Split(columnaOrdenada, " ")(0)) Is Nothing Then
                    whdgSolicitudes.GridView.Behaviors.Sorting.SortedColumns.Add(Split(columnaOrdenada, " ")(0),
                    IIf(Split(columnaOrdenada, " ")(1) = "DESC", Infragistics.Web.UI.SortDirection.Descending, Infragistics.Web.UI.SortDirection.Ascending))
                End If
            Next
        End Sub
        ''' <summary>
        ''' Da el Formato Numero a usar en Excel para q las columnas numericas sean reconocidas como numero y no como texto
        ''' </summary>
        ''' <returns>Formato Numero a usar en Excel</returns>
        ''' <remarks>Llamada desde:whdgExcelExporter_GridRecordItemExported ; Tiempo maximo:0</remarks>
        Private Function FormatoNumeroExcel() As String
            'El excel espera "#,###.00", no el formato de usuario. El excel admite lo q admite por el regional setting
            Dim FormatoNum As String = "#,###"
            For i As Integer = 1 To FSPMUser.NumberFormat.NumberDecimalDigits
                If i = 1 Then
                    FormatoNum = FormatoNum & "."
                End If
                FormatoNum = FormatoNum & "0"
            Next
            Return FormatoNum
        End Function
        ''' <summary>
        ''' Strip HTML tags.
        ''' </summary>
        Private Function StripTags(ByVal html As String) As String
            ' Remove HTML tags.
            Return Regex.Replace(html, "<.*?>", "")
        End Function
#End Region
#Region "WebMethods"
        <Services.WebMethod(EnableSession:=True)>
        <Script.Services.ScriptMethod()>
        Public Shared Function Obtener_Escenarios_Usuario(ByVal iTipoVisor As Integer) As Object
            Try
                Dim PMPortalServer As Fullstep.PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
                Dim FSPMUser As Fullstep.PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
                Dim cEscenarios As Escenarios
                cEscenarios = PMPortalServer.Get_Escenarios
                Dim lCiaComp As Long = FSPMUser.CiaComp
                Dim serializer As New JavaScriptSerializer
                Dim oResultado As Object = cEscenarios.FSN_Get_Escenarios_Usuario(lCiaComp, FSPMUser.Idioma, iTipoVisor)
                Return serializer.Serialize(oResultado)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        <System.Web.Services.WebMethod(True),
		System.Web.Script.Services.ScriptMethod()>
		Public Shared Function Obtener_Numero_Pendientes(ByVal iTipoVisor As Integer, ByVal MostrarOtras As Boolean) As Object
			Try
				Dim PMPortalServer As Root = HttpContext.Current.Session("FS_Portal_Server")
				Dim FSPMUser As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")

				Dim condicionesPendientes As New List(Of EscenarioFiltroCondicion)
				Dim iEscenarioFiltroCondicion As New EscenarioFiltroCondicion
				With iEscenarioFiltroCondicion
					.IdCampo = CamposGeneralesVisor.ESTADO
					.EsCampoGeneral = True
					.Denominacion_BD = "ESTADO"
					.Operador = Operadores.Campos.ES
					.Valores.Add("1000")
				End With
				condicionesPendientes.Add(iEscenarioFiltroCondicion)
				Dim cInstancias As Instancias
				cInstancias = PMPortalServer.Get_Instancias
				Dim sentenciaWhereCamposGenerales As String = Generar_SentenciaWhere_CamposGenerales(condicionesPendientes)
				Dim ds As DataSet = cInstancias.DevolverSolicitudes(FSPMUser.CiaComp, FSPMUser.CodProveGS, FSPMUser.IdContacto, FSPMUser.Idioma, 0, Generar_SentenciaWhere_Filtro(condicionesPendientes),
														"", sentenciaWhereCamposGenerales, 1, 1, False,
														False, False, True, True, True, MostrarOtras, TipoVisor:=iTipoVisor)

				If ds.Tables(0).Rows.Count = 0 Then
					Return 0
				Else
					Return ds.Tables(0).Rows(0)("TOTALSOLICITUDES")
				End If
			Catch ex As Exception
				Throw ex
			End Try
		End Function
		<System.Web.Services.WebMethod(True),
        System.Web.Script.Services.ScriptMethod()>
        Public Shared Function Obtener_Escenario(ByVal IdEscenario As Integer, ByVal Edicion As Boolean, ByVal iTipoVisor As Integer) As Object
            Try
                Dim PMPortalServer As Root = HttpContext.Current.Session("FS_Portal_Server")
                Dim FSPMUser As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
                Dim cEscenarios As Escenarios
                cEscenarios = PMPortalServer.Get_Escenarios
                Dim lCiaComp As Long = FSPMUser.CiaComp
                Dim serializer As New JavaScriptSerializer
                Dim oResultado As Object = cEscenarios.FSN_Get_Escenario_Usuario(lCiaComp, FSPMUser.Idioma, iTipoVisor, IdEscenario)
                Return serializer.Serialize(oResultado)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' <summary>
        ''' Metodo que se llama desde cualquier campo de tipo lista para devolver los item para rellenar la lista
        ''' </summary>
        ''' <param name="Campo_Escenario">Info del campo de tipo lista para obtener los item</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <System.Web.Services.WebMethod(True),
        System.Web.Script.Services.ScriptMethod()>
        Public Shared Function Obtener_Opciones_Lista(ByVal Campo_Escenario As Object, ByVal Escenario As Object, ByVal ValorCampoPadre As String, ByVal iTipoVisor As Integer) As List(Of cn_fsItem)
            Try
                Dim PMPortalServer As Root = HttpContext.Current.Session("FS_Portal_Server")
                Dim FSPMUser As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
                Dim serializer As New JavaScriptSerializer
                Dim oCampo_Escenario As Escenario_Campo = serializer.Deserialize(Of Escenario_Campo)(Campo_Escenario)
                Dim oEscenario As Escenario = serializer.Deserialize(Of Escenario)(Escenario)
                Dim lOpcionesLista As New List(Of cn_fsItem)
                Dim iOpcionLista As cn_fsItem

                If oCampo_Escenario.EsCampoGeneral Then
                    Select Case oCampo_Escenario.Id
                        Case CamposGeneralesVisor.ORIGEN   'Departamento
                            Dim cDepartamentos As Departamentos
                            cDepartamentos = PMPortalServer.Get_Departamentos
                            cDepartamentos.LoadData(FSPMUser.CiaComp)
                            For Each departamento As DataRow In cDepartamentos.Data.Tables(0).Select("COD IS NOT NULL")
                                iOpcionLista = New cn_fsItem
                                iOpcionLista.value = departamento("COD").ToString
                                iOpcionLista.text = departamento("DEN").ToString
                                lOpcionesLista.Add(iOpcionLista)
                            Next
                        Case CamposGeneralesVisor.TIPOSOLICITUD
                            Dim cTiposSolicitud As Escenarios
                            cTiposSolicitud = PMPortalServer.Get_Escenarios
                            Dim idsSolicitud As String = String.Empty
                            For Each solicitudFormulario As String In oEscenario.SolicitudFormularioVinculados.Keys
                                idsSolicitud &= (IIf(idsSolicitud Is String.Empty, "", ",") & solicitudFormulario)
                            Next
                            lOpcionesLista = cTiposSolicitud.FSN_Get_Opciones_Lista_TipoSolicitud(FSPMUser.CiaComp, FSPMUser.Cod, FSPMUser.Idioma, idsSolicitud, iTipoVisor, FSPMUser.CodProveGS)
                        Case CamposGeneralesVisor.ESTADOHOMOLOGACION
                            Dim cOpcionesLista As Escenarios
                            cOpcionesLista = PMPortalServer.Get_Escenarios
                            lOpcionesLista = cOpcionesLista.FSN_Get_Opciones_Lista_EstadoHomologacion(FSPMUser.CiaComp, FSPMUser.Idioma, oEscenario.SolicitudFormularioVinculados)
                        Case Else
                            iOpcionLista = New cn_fsItem
                            iOpcionLista.value = ""
                            iOpcionLista.text = ""
                            lOpcionesLista.Add(iOpcionLista)
                    End Select
                Else
                    Select Case oCampo_Escenario.TipoCampoGS
                        Case TiposDeDatos.TipoCampoGS.FormaPago
                            Dim oFormasPago As FormasPago
                            oFormasPago = PMPortalServer.Get_FormasPago
                            oFormasPago.LoadData(FSPMUser.Idioma.ToString(), FSPMUser.CiaComp)
                            For Each formaPago As DataRow In oFormasPago.Data.Tables(0).Rows
                                iOpcionLista = New cn_fsItem
                                With iOpcionLista
                                    .value = formaPago("COD").ToString
                                    .text = formaPago("COD").ToString & " - " & formaPago("DEN").ToString
                                End With
                                lOpcionesLista.Add(iOpcionLista)
                            Next
                        Case TiposDeDatos.TipoCampoGS.Moneda
                            Dim oMonedas As Monedas
                            oMonedas = PMPortalServer.Get_Monedas
                            oMonedas.LoadData(FSPMUser.Idioma.ToString(), FSPMUser.CiaComp)
                            For Each moneda As DataRow In oMonedas.Data.Tables(0).Rows
                                iOpcionLista = New cn_fsItem
                                With iOpcionLista
                                    .value = moneda("COD").ToString
                                    .text = moneda("DEN").ToString
                                End With
                                lOpcionesLista.Add(iOpcionLista)
                            Next
                        Case TiposDeDatos.TipoCampoGS.Unidad, TiposDeDatos.TipoCampoGS.UnidadPedido
                            Dim oUnidades As Unidades
                            oUnidades = PMPortalServer.get_Unidades
                            oUnidades.LoadData(FSPMUser.Idioma.ToString(), FSPMUser.CiaComp)
                            For Each unidad As DataRow In oUnidades.Data.Tables(0).Rows
                                iOpcionLista = New cn_fsItem
                                With iOpcionLista
                                    .value = unidad("COD").ToString
                                    .text = unidad("COD").ToString & " - " & unidad("DEN").ToString
                                End With
                                lOpcionesLista.Add(iOpcionLista)
                            Next
                        Case TiposDeDatos.TipoCampoGS.Dest
                            Dim oDests As Destinos
                            oDests = PMPortalServer.Get_Destinos
                            oDests.LoadData(FSPMUser.Idioma.ToString(), FSPMUser.CiaComp, FSPMUser.AprobadorActual)
                            For Each destino As DataRow In oDests.Data.Tables(0).Rows
                                iOpcionLista = New cn_fsItem
                                With iOpcionLista
                                    .value = destino("COD").ToString
                                    .text = destino("COD").ToString & " - " & destino("DEN").ToString & " (" & destino("POB").ToString & ")"
                                End With
                                lOpcionesLista.Add(iOpcionLista)
                            Next
                        Case TiposDeDatos.TipoCampoGS.Departamento
                            Dim oDepartamentos As Departamentos
                            oDepartamentos = PMPortalServer.Get_Departamentos
                            oDepartamentos.LoadData(FSPMUser.CiaComp)
                            For Each departamento As DataRow In oDepartamentos.Data.Tables(0).Rows
                                If Not IsDBNull(departamento("COD")) AndAlso Not IsDBNull(departamento("COD")) Then
                                    iOpcionLista = New cn_fsItem
                                    With iOpcionLista
                                        .value = departamento("COD").ToString
                                        .text = departamento("COD").ToString & " - " & departamento("DEN").ToString
                                    End With
                                    lOpcionesLista.Add(iOpcionLista)
                                End If
                            Next
                        Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
                            Dim oOrganizacionesCompras As OrganizacionesCompras
                            oOrganizacionesCompras = PMPortalServer.Get_OrganizacionesCompras
                            oOrganizacionesCompras.LoadData(FSPMUser.CiaComp)
                            For Each orgCompras As DataRow In oOrganizacionesCompras.Data.Tables(0).Rows
                                iOpcionLista = New cn_fsItem
                                With iOpcionLista
                                    .value = orgCompras("COD").ToString
                                    .text = orgCompras("COD").ToString & " - " & orgCompras("DEN").ToString
                                End With
                                lOpcionesLista.Add(iOpcionLista)
                            Next
                        Case TiposDeDatos.TipoCampoGS.Centro
                            Dim oCentros As Centros
                            oCentros = PMPortalServer.Get_Centros
                            oCentros.LoadData(FSPMUser.CiaComp)
                            For Each centro As DataRow In oCentros.Data.Tables(0).Rows
                                iOpcionLista = New cn_fsItem
                                With iOpcionLista
                                    .value = centro("COD").ToString
                                    .text = centro("COD").ToString & " - " & centro("DEN").ToString
                                End With
                                lOpcionesLista.Add(iOpcionLista)
                            Next
                        Case TiposDeDatos.TipoCampoGS.Almacen
                            Dim oAlmac As Almacenes
                            oAlmac = PMPortalServer.Get_Almacenes
                            oAlmac.LoadData(FSPMUser.CiaComp)
                            For Each almacen As DataRow In oAlmac.Data.Tables(0).Rows
                                iOpcionLista = New cn_fsItem
                                With iOpcionLista
                                    .value = almacen("ID").ToString
                                    .text = almacen("COD").ToString & " - " & almacen("DEN").ToString
                                End With
                                lOpcionesLista.Add(iOpcionLista)
                            Next
                        Case TiposDeDatos.TipoCampoGS.ProveedorERP
                            Dim oProvesERP As CProveERPs
                            oProvesERP = PMPortalServer.Get_CProveERPs
                            Dim dsResultado As DataSet = oProvesERP.CargarProveedoresERPtoDS(FSPMUser.CiaComp)
                            For Each proveERP As DataRow In dsResultado.Tables(0).Rows
                                iOpcionLista = New cn_fsItem
                                With iOpcionLista
                                    .value = proveERP("COD").ToString
                                    .text = proveERP("COD").ToString & " - " & proveERP("DEN").ToString
                                End With
                                lOpcionesLista.Add(iOpcionLista)
                            Next
                        Case TiposDeDatos.TipoCampoGS.Pais
                            Dim cListaPaises As Paises
                            cListaPaises = PMPortalServer.Get_Paises
                            cListaPaises.LoadData(FSPMUser.Idioma, FSPMUser.CiaComp)
                            iOpcionLista = New cn_fsItem
                            iOpcionLista.value = ""
                            iOpcionLista.text = ""
                            lOpcionesLista.Add(iOpcionLista)
                            For Each pais As DataRow In cListaPaises.Data.Tables(0).Rows
                                iOpcionLista = New cn_fsItem
                                With iOpcionLista
                                    .value = pais("COD").ToString
                                    .text = pais("DEN").ToString
                                End With
                                lOpcionesLista.Add(iOpcionLista)
                            Next
                        Case TiposDeDatos.TipoCampoGS.Provincia
                            If ValorCampoPadre IsNot String.Empty Then
                                Dim cListaProvincias As Provincias
                                cListaProvincias = PMPortalServer.Get_Provincias
                                cListaProvincias.Pais = ValorCampoPadre
                                cListaProvincias.LoadData(FSPMUser.Idioma, FSPMUser.CiaComp)
                                iOpcionLista = New cn_fsItem
                                iOpcionLista.value = ""
                                iOpcionLista.text = ""
                                lOpcionesLista.Add(iOpcionLista)
                                For Each pais As DataRow In cListaProvincias.Data.Tables(0).Rows
                                    iOpcionLista = New cn_fsItem
                                    With iOpcionLista
                                        .value = pais("PAICOD").ToString & "#" & pais("COD").ToString
                                        .text = pais("DEN").ToString
                                    End With
                                    lOpcionesLista.Add(iOpcionLista)
                                Next
                            End If
                        Case TiposDeDatos.TipoCampoGS.TipoPedido
                            Dim oTiposPedido As TiposPedido
                            Dim dsTiposPedidos As DataSet
                            oTiposPedido = PMPortalServer.Get_TiposPedido
                            dsTiposPedidos = oTiposPedido.LoadData(FSPMUser.CiaComp, FSPMUser.Idioma)
                            For Each tipoPedido As DataRow In dsTiposPedidos.Tables(0).Rows
                                iOpcionLista = New cn_fsItem
                                With iOpcionLista
                                    .value = tipoPedido("COD").ToString
                                    .text = tipoPedido("DEN").ToString
                                End With
                                lOpcionesLista.Add(iOpcionLista)
                            Next
                        Case Else
                            If oCampo_Escenario.TipoCampo = TiposDeDatos.TipoGeneral.TipoBoolean Then
                                Dim Textos As DataTable
                                Dim oDict As Dictionary
                                oDict = PMPortalServer.Get_Dictionary
                                oDict.LoadData(TiposDeDatos.ModulosIdiomas.VisorSolicitudes, FSPMUser.Idioma)
                                Textos = oDict.Data.Tables(0)

                                Dim cOpcionesLista As Escenarios
                                cOpcionesLista = PMPortalServer.Get_Escenarios
                                lOpcionesLista = cOpcionesLista.Get_Opciones_Lista_Boolean(Textos)
                            Else
                                Dim cOpcionesLista As Escenarios
                                cOpcionesLista = PMPortalServer.Get_Escenarios
                                lOpcionesLista = cOpcionesLista.FSN_Get_Opciones_Lista(FSPMUser.CiaComp, FSPMUser.Idioma, oCampo_Escenario.Id, oCampo_Escenario.TipoCampo)
                            End If
                    End Select
                End If

                Return lOpcionesLista
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        <System.Web.Services.WebMethod(True),
        System.Web.Script.Services.ScriptMethod()>
        Public Shared Function Obtener_Solicitudes_Aprobar() As IList
            Try
                Dim FSPMUser As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
                Dim dtSolicitudes As DataTable = CType(HttpContext.Current.Cache("dsSolicitudes_" & FSPMUser.Cod), DataSet).Tables("SOLICITUDES_APROBAR")

                Return dtSolicitudes.Rows.OfType(Of DataRow).Select(Function(x) New With {Key .ID = x("ID"), .AccionAprobar = x("ACCION_APROBAR"), .Bloque = x("BLOQUE")}).ToList()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        <System.Web.Services.WebMethod(True),
        System.Web.Script.Services.ScriptMethod()>
        Public Shared Function Obtener_Solicitudes_Rechazar() As IList
            Try
                Dim FSPMUser As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
                Dim dtSolicitudes As DataTable = CType(HttpContext.Current.Cache("dsSolicitudes_" & FSPMUser.Cod), DataSet).Tables("SOLICITUDES_RECHAZAR")

                Return dtSolicitudes.Rows.OfType(Of DataRow).Select(Function(x) New With {Key .ID = x("ID"), .AccionRechazar = x("ACCION_RECHAZAR"), .Bloque = x("BLOQUE")}).ToList()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        <System.Web.Services.WebMethod(True),
    System.Web.Script.Services.ScriptMethod()>
        Public Shared Sub Procesar_Acciones(ByVal datosInstancias As String)
            Dim PMPortalServer As Root = HttpContext.Current.Session("FS_Portal_Server")
            Dim FSPMUser As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
            Dim serializer As New JavaScriptSerializer
            Dim oDatosInstanciaAprobar As Dictionary(Of String, String) = serializer.Deserialize(Of Dictionary(Of String, String))(datosInstancias)
            Dim cInstancia As Instancia
            cInstancia = PMPortalServer.Get_Instancia

            Dim Comentario As String
            Dim instanciasActualizarEnProceso As String = String.Empty
            Dim instanciasValidar As String = String.Empty

            Dim dsXML As New DataSet
            dsXML.Tables.Add("SOLICITUD")
            Dim drSolicitud As DataRow
            With dsXML.Tables("SOLICITUD").Columns
                .Add("TIPO_PROCESAMIENTO_XML")
                .Add("TIPO_DE_SOLICITUD")
                .Add("COMPLETO")
                .Add("INSTANCIA")
                .Add("CIACOMP")
                .Add("PROVE")
                .Add("COD")
                .Add("CODCIA")
                .Add("IDCIA")
                .Add("IDUSU")
                .Add("USUARIO_IDIOMA")
                .Add("USUARIO_EMAIL")
                .Add("PROVEGS")
                .Add("DENCIA")
                .Add("NIFCIA")
                .Add("NOMBRE")
                .Add("APELLIDOS")
                .Add("TELEFONO")
                .Add("FAX")
                .Add("ACCION")
                .Add("IDACCION")
                .Add("COMENTARIO")
                .Add("BLOQUE_ORIGEN")
                .Add("THOUSANFMT")
                .Add("DECIAMLFMT")
                .Add("PRECISIONFMT")
                .Add("DATEFMT")
                .Add("REFCULTURAL")
                .Add("TIPOEMAIL")
                .Add("IDTIEMPOPROC")
            End With

            For Each instancia As KeyValuePair(Of String, String) In oDatosInstanciaAprobar
                Comentario = HttpContext.Current.Session("COMENT" & instancia.Key)

                instanciasActualizarEnProceso &= IIf(instanciasActualizarEnProceso Is String.Empty, "", ",") & instancia.Key
                instanciasValidar &= IIf(instanciasValidar Is String.Empty, "", "#") & instancia.Key & "|" & instancia.Value & "|" & Comentario
            Next
            If Not instanciasActualizarEnProceso = "" Then
                Dim cInstancias As Instancias
                cInstancias = PMPortalServer.Get_Instancias
                cInstancias.Actualizar_En_Proceso2(FSPMUser.CiaComp, instanciasActualizarEnProceso, FSPMUser.CodProveGS)

                Dim xmlName As String
                Dim lIDTiempoProc As Long
                For Each info As String In Split(instanciasValidar, "#")
                    lIDTiempoProc = 0
                    dsXML.Tables("SOLICITUD").Rows.Clear()
                    cInstancia.ID = strToLong(Split(info, "|")(0))
                    cInstancia.ActualizarTiempoProcesamiento(FSPMUser.CiaComp, lIDTiempoProc, FSPMUser.Cod, strToLong(Split(info, "|")(1)))
                    drSolicitud = dsXML.Tables("SOLICITUD").NewRow
                    With drSolicitud
                        .Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.Portal_PM_AprobacionRechazoMultiple)
                        .Item("TIPO_DE_SOLICITUD") = CInt(TiposDeDatos.TipoDeSolicitud.Factura)
                        .Item("COMPLETO") = 1
                        .Item("INSTANCIA") = Split(info, "|")(0)
                        .Item("CIACOMP") = FSPMUser.CiaComp
                        .Item("PROVE") = FSPMUser.CodProve
                        .Item("COD") = FSPMUser.Cod
                        .Item("CODCIA") = FSPMUser.CodCia
                        .Item("IDCIA") = FSPMUser.IdCia
                        .Item("IDUSU") = FSPMUser.IdUsu
                        .Item("USUARIO_IDIOMA") = FSPMUser.Idioma
                        .Item("USUARIO_EMAIL") = FSPMUser.Email
                        .Item("PROVEGS") = FSPMUser.ObtenerProv(FSPMUser.CiaComp)
                        .Item("DENCIA") = FSPMUser.DenCia
                        .Item("NIFCIA") = FSPMUser.NIFCia
                        .Item("NOMBRE") = FSPMUser.Nombre
                        .Item("APELLIDOS") = FSPMUser.Apellidos
                        .Item("TELEFONO") = FSPMUser.Telefono
                        .Item("FAX") = FSPMUser.Fax
                        .Item("ACCION") = ""
                        .Item("IDACCION") = Split(info, "|")(2)
                        .Item("COMENTARIO") = Split(info, "|")(3)
                        .Item("BLOQUE_ORIGEN") = Split(info, "|")(1)
                        .Item("THOUSANFMT") = FSPMUser.ThousanFmt
                        .Item("DECIAMLFMT") = FSPMUser.DecimalFmt
                        .Item("PRECISIONFMT") = FSPMUser.PrecisionFmt
                        .Item("DATEFMT") = FSPMUser.DateFmt
                        .Item("TIPOEMAIL") = FSPMUser.TipoEmail
                        .Item("IDTIEMPOPROC") = lIDTiempoProc
                    End With
                    xmlName = FSPMUser.CodCia & "#" & FSPMUser.Cod & "#" & strToLong(Split(info, "|")(0)) & "#0"
                    dsXML.Tables("SOLICITUD").Rows.Add(drSolicitud)

                    cInstancia.ActualizarTiempoProcesamiento(FSPMUser.CiaComp, lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
                    If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
                        'Se hace con un StringWriter porque el método GetXml del dataset no incluye el esquema
                        Dim oSW As New System.IO.StringWriter()
                        dsXML.WriteXml(oSW, XmlWriteMode.WriteSchema)

                        Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
                        oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), xmlName)
                    Else
                        dsXML.WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#PORTAL.xml", XmlWriteMode.WriteSchema)
                        If File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml") Then _
                            File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
                        Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & "#PORTAL.xml",
                                          ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & xmlName & ".xml")
                    End If
                Next
                cInstancias = Nothing
            End If
        End Sub
        <System.Web.Services.WebMethod(True),
        System.Web.Script.Services.ScriptMethod()>
        Public Shared Function ObtenerTextoLargo(ByVal Instancia As Integer, ByVal Columna As String) As String
            Try
                Dim PMPortalServer As Root = HttpContext.Current.Session("FS_Portal_Server")
                Dim FSPMUser As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
                Dim cEscenarios As Escenarios
                cEscenarios = PMPortalServer.Get_Escenarios
                Dim serializer As New JavaScriptSerializer
                Dim IdCampoOrigen As Integer = CType(Split(Columna, "_")(Split(Columna, "_").Length - 1), Integer)
                Dim EsDesglose As Boolean = IIf(Split(Columna, "_").Length = 3, False, True)
                Return cEscenarios.FSN_Get_TextoLargo_Instancia(FSPMUser.CiaComp, Instancia, IdCampoOrigen, EsDesglose).ToString
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        <System.Web.Services.WebMethod(True),
		System.Web.Script.Services.ScriptMethod()>
		Public Shared Function Obtener_Numero_Solicitudes_Filtro(ByVal oEscenario As String, ByVal IdFiltro As Integer, ByVal iTipoVisor As Integer, ByVal MostrarOtras As Boolean) As Object
			Dim PMPortalServer As Root = HttpContext.Current.Session("FS_Portal_Server")
			Dim FSPMUser As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
			Dim serializer As New JavaScriptSerializer
			Dim cEscenario As Escenario = serializer.Deserialize(Of Escenario)(oEscenario)

			Dim cInstancias As Instancias
			cInstancias = PMPortalServer.Get_Instancias
			Dim IdFormulario As Integer = 0
			If cEscenario.SolicitudFormularioVinculados.Select(Function(x) x.Value.Key).Distinct().Count() = 1 Then
				IdFormulario = cEscenario.SolicitudFormularioVinculados.First.Value.Key
			End If

			Dim AbtasUsted, Participo, Pendientes, PendientesDevolucion, Trasladadas, Otras, ObservadorSustitucion As Nullable(Of Boolean)
			Dim FiltroDesglose As Boolean
			Dim sentenciaWhere As String = Generar_SentenciaWhere_Filtro(cEscenario.EscenarioFiltros(IdFiltro).FormulaCondiciones, FiltroDesglose)
			Dim sentenciaWhereCamposGenerales As String
			If cEscenario.EscenarioFiltros(IdFiltro).FormulaAvanzada Then
				sentenciaWhereCamposGenerales = ""
				AbtasUsted = True
				Participo = True
				Pendientes = True
				PendientesDevolucion = True
				Trasladadas = True
				ObservadorSustitucion = True
			Else
				sentenciaWhereCamposGenerales = Generar_SentenciaWhere_CamposGenerales(cEscenario.EscenarioFiltros(IdFiltro).FormulaCondiciones, AbtasUsted, Participo, Pendientes, PendientesDevolucion, Trasladadas, ObservadorSustitucion)
			End If

			Dim ds As DataSet = cInstancias.DevolverSolicitudes(FSPMUser.CiaComp, FSPMUser.CodProveGS, FSPMUser.IdContacto, FSPMUser.Idioma, IdFormulario,
											sentenciaWhere, "", sentenciaWhereCamposGenerales, 1, 1,
											BuscarProveedorArticulo(cEscenario.EscenarioFiltros(IdFiltro).FormulaCondiciones, Nothing),
											AbtasUsted, Participo, Pendientes, PendientesDevolucion, Trasladadas, MostrarOtras,
											FiltroDesglose:=FiltroDesglose, TipoVisor:=iTipoVisor)

			If ds.Tables(0).Rows.Count = 0 Then
				Return {0, cEscenario.Id, IdFiltro}
			Else
				Return {ds.Tables(0).Rows(0)("TOTALSOLICITUDES"), cEscenario.Id, IdFiltro}
			End If
		End Function
		<System.Web.Services.WebMethod(True),
	System.Web.Script.Services.ScriptMethod()>
		Public Shared Sub Obtener_Solicitudes_Ordenacion(ByVal data As Object, ByVal iTipoVisor As Integer, ByVal MostrarOtras As Boolean)
			Dim PMPortalServer As Root = HttpContext.Current.Session("FS_Portal_Server")
			Dim FSPMUser As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
			Dim serializer As New JavaScriptSerializer
			Dim oInfoFiltroVista As Object = serializer.Deserialize(Of Object)(data)
			Dim IdFormulario As Integer = serializer.Deserialize(Of Integer)(oInfoFiltroVista("IdFormulario"))
			Dim oFiltro As EscenarioFiltro = serializer.Deserialize(Of EscenarioFiltro)(oInfoFiltroVista("Filtro"))
			Dim oVista As EscenarioVista = serializer.Deserialize(Of EscenarioVista)(oInfoFiltroVista("Vista"))
			Dim lSolicitudesVinculadas As String = serializer.Deserialize(Of String)(oInfoFiltroVista("SolicitudFormularioVinculados"))
			Dim oPageNumber As Integer = serializer.Deserialize(Of Integer)(oInfoFiltroVista("Page"))
			Dim columNames() As String = {}
			Dim columNamesDesglose() As String = {}
			Dim ObtenerProcesos, ObtenerPedidos, VistaDesglose As Boolean
			Seleccionar_Columnas_Grid(oVista.Campos_Vista, columNames, columNamesDesglose, ObtenerProcesos, ObtenerPedidos, VistaDesglose)
			Dim cInstancias As Instancias
			cInstancias = PMPortalServer.Get_Instancias
			Dim AbtasUsted, Participo, Pendientes, PendientesDevolucion, Trasladadas, Otras, ObservadorSustitucion As Nullable(Of Boolean)
			Dim FiltroDesglose As Boolean
			Dim sentenciaWhere As String = Generar_SentenciaWhere_Filtro(oFiltro.FormulaCondiciones, FiltroDesglose)
			Dim sentenciaWhereCamposGenerales As String
			If oFiltro.FormulaAvanzada Then
				sentenciaWhereCamposGenerales = ""
				AbtasUsted = True
				Participo = True
				Pendientes = True
				PendientesDevolucion = True
				Trasladadas = True
				Otras = True
				ObservadorSustitucion = True
			Else
				sentenciaWhereCamposGenerales = Generar_SentenciaWhere_CamposGenerales(oFiltro.FormulaCondiciones, AbtasUsted, Participo, Pendientes, PendientesDevolucion, Trasladadas, ObservadorSustitucion, lSolicitudesVinculadas)
			End If

			Dim ds As DataSet = cInstancias.DevolverSolicitudes(FSPMUser.CiaComp, FSPMUser.CodProveGS, FSPMUser.IdContacto, FSPMUser.Idioma, IdFormulario,
								sentenciaWhere, oVista.Orden, sentenciaWhereCamposGenerales,
								oPageNumber, ConfigurationManager.AppSettings("PaginacionSolicitudes"), BuscarProveedorArticulo(oFiltro.FormulaCondiciones, oVista.Campos_Vista),
								AbtasUsted, Participo, Pendientes, PendientesDevolucion, Trasladadas, MostrarOtras, FiltroDesglose, VistaDesglose, iTipoVisor)
			Dim view As New DataView(ds.Tables(0))
			Dim dsResultado As New DataSet
			Dim dtSolicitudes As DataTable = view.ToTable("SOLICITUDES", True, columNames)
			dsResultado.Tables.Add(dtSolicitudes)
			If Not IdFormulario = 0 AndAlso columNamesDesglose.Count > 3 Then
				Dim listaDesgloses As List(Of String) = columNamesDesglose.Where(Function(x) Split(x, "_").Length > 1).Select(Function(x) Split(x, "_")(1)).Distinct().ToList
				Dim query = From Datos In ds.Tables(0).AsEnumerable()
							Where (listaDesgloses.Contains(Datos.Item("DESGLOSE").ToString))
							Select Datos Distinct
				Dim dtDesglose As DataTable = Nothing
				If query.Any Then
					dtDesglose = query.CopyToDataTable
				End If
				Dim viewDesglose As New DataView(dtDesglose)
				dsResultado.Tables.Add(viewDesglose.ToTable("DESGLOSE", True, columNamesDesglose))

				Dim parentColsVista(0) As DataColumn
				Dim childColsVista(0) As DataColumn
				parentColsVista(0) = dsResultado.Tables("SOLICITUDES").Columns("ID")
				childColsVista(0) = dsResultado.Tables("DESGLOSE").Columns("FORM_INSTANCIA")
				dsResultado.Relations.Add("SOLICITUD_DESGLOSE", parentColsVista, childColsVista, False)
			End If
			If Pendientes Then
				dsResultado.Tables.Add(New DataView(ds.Tables(1)).ToTable("SOLICITUDES_APROBAR", True, New String() {"ID", "ACCION_APROBAR", "BLOQUE"}))
				dsResultado.Tables.Add(New DataView(ds.Tables(2)).ToTable("SOLICITUDES_RECHAZAR", True, New String() {"ID", "ACCION_RECHAZAR", "BLOQUE"}))
			End If
			HttpContext.Current.Cache.Insert("dsSolicitudes_" & FSPMUser.Cod, dsResultado, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0), CacheItemPriority.BelowNormal, Nothing)
		End Sub
#End Region
	End Class
End Namespace