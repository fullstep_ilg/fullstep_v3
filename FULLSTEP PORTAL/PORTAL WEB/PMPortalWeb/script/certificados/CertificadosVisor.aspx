﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CertificadosVisor.aspx.vb" Inherits="Fullstep.PMPortalWeb.CertificadosVisor"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>visorCertificados</title>
	<script type="text/javascript" src="../../../common/formatos.js"></script>
	<script type="text/javascript" src="../../../common/menu.asp"></script>
	<script type="text/javascript">dibujaMenu(2)</script>
	<script type="text/javascript" src="../../js/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery/jquery-migrate.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			/*''' <summary>
			''' Iniciar la pagina.
			''' </summary>     
			''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/		    
		    if (accesoExterno == 0) { document.getElementById('tablemenu').style.display = 'block'; }
			$.getScript(rutanormal + 'js/json2.js');
		});
		function ExportarExcel() {
			/*''' <summary>
			''' Exporta a Excel
			''' </summary>
			''' <remarks>Llamada desde: onclick del div q contiene al link excel y a la img excel; Tiempo máximo:0,5</remarks>*/
			__doPostBack('Excel', '')
		}
		function ExportarPDF() {
			/*''' <summary>
			''' Exporta a Pdf
			''' </summary>
			''' <remarks>Llamada desde: onclick del div q contiene al link pdf y a la img pdf; Tiempo máximo:0,5</remarks>*/
			__doPostBack('PDF', '')
		}
		function wdgHDCertifs_CellClick(sender, e) {
			//''' <summary>
			//''' Responder al click de una columna. Para mostrar el detalle del certificado o de la persona del contacto.
			//''' </summary>
			//''' <param name="gridName">Nobre del grid</param>
			//''' <param name="cellId">Id de la celda pulsada</param>
			//''' <param name="button">Id de botón pulsado</param>        
			//''' <returns>Nada</returns>
			//''' <remarks>Llamada desde: sistema; Tiempo máximo:0</remarks>            
			if (e.getNewSelectedCells().get_length() == 1) {
				e.set_cancel(true);
				var row = e.getNewSelectedCells().getCell(0).get_row();
				switch (e.getNewSelectedCells().getCell(0).get_column().get_key()) {
				    case "NOMBRE":
						window.open("../_common/detallepersona.aspx?CodPersona=" + row.get_cellByColumnKey("PER").get_value(), "_blank", "top=150, left=60, width=450, height=220, resizable=yes,status=no,toolbar=no,menubar=no,location=no");
						break;
		default:
			$.ajax({
				type: "POST",
				url: rutanormal + 'script/certificados/CertificadosVisor.aspx/ComprobarEnProceso',
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify({ lCiaComp: lCiaComp, Instancia: row.get_cellByColumnKey("INSTANCIA").get_value() }),
				dataType: "json",
				async: false,
				success: function (msg) {
					var esAlta = row.get_cellByColumnKey("ESALTA").get_value();
					var certificado = row.get_cellByColumnKey("ID_ENCRYPTED").get_value();
					var instancia = row.get_cellByColumnKey("INSTANCIA_ENCRYPTED").get_value();
					var tipo = row.get_cellByColumnKey("DEN").get_value();
					var fecAlta = (row.get_cellByColumnKey("FECALTA").get_value() == null ? '' : row.get_cellByColumnKey("FECALTA").get_value());
					if (msg.d == 0) {
						var FechaLocal = new Date();
						otz = FechaLocal.getTimezoneOffset();
						window.open("../../script/certificados/extFrames.aspx?certificado=" + certificado + "&instancia=" + (esAlta==0?0:instancia) + "&Otz=" + otz.toString(), ((accesoExterno == 0)?"default_main":"_self"));
					} else {
					    window.open("../_common/QAenProceso.aspx?Instancia=" + instancia + "&Tipo=" + tipo + "&EsCertificado=1" + "&FecAlta=" + fecAlta, ((accesoExterno == 0) ? "_blank" : "_self"), "width=600,height=200,status=yes,resizable=no,top=200,left=200");
					}
				}
			});
			break;
				}
			}
		}
	</script>
</head>
<body>
	<iframe id="iframeWSServer" style="z-index: 106; left: 8px; visibility: hidden; position: absolute;
		top: 200px" name="iframeWSServer" src="<%=Application("RUTASEGURA")%>script/blank.htm">
	</iframe>
	<form id="form1" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"></asp:ScriptManager>
	<ig:WebExcelExporter ID="wdgExcelExporter" runat="server"></ig:WebExcelExporter>
	<ig:WebDocumentExporter ID="wdgPDFExporter" runat="server"></ig:WebDocumentExporter>
	<div style="margin-top: 10px;">
		<fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
	</div>
	<div class="CabeceraBotones" style="height: 22px; clear: both; line-height: 22px;">
        <div ID="divCertifPdteEnviar" runat="server" style="clear: both; float: left; padding-left: 10px;">
            <asp:Label ID="Label1" runat="server" CssClass="certificadoPendienteEnviar" Text="(*)"></asp:Label>
            <asp:Label ID="lblCertifPdteEnviar" runat="server"  CssClass="caption"></asp:Label>
        </div>

        <div ID="divSeleccionar" runat="server" style="clear: both; float: left; padding-left: 10px;">
            <asp:Label runat="server" ID="lblSubtitulo" CssClass="parrafo2" Text="DlblSubtitulo" Style="display:inline-block;"></asp:Label>
        </div>

		<div style="float: right;">
            
			<div onclick="ExportarExcel()" class="botonDerechaCabecera">
				<asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
				<asp:Label runat="server" ID="lblExcel" CssClass="fntLogin" Text="Excel"></asp:Label>
			</div>
			<div onclick="ExportarPDF()" class="botonDerechaCabecera">
				<asp:Image runat="server" ID="imgPDF" CssClass="imagenCabecera" />
				<asp:Label runat="server" ID="lblPDF" CssClass="fntLogin" Text="PDF"></asp:Label>
			</div>
		</div>
	</div>
	<div class="bordeSimple" style="overflow: hidden;clear: both;">
		<ig:WebHierarchicalDataGrid runat="server" ID="wdgHDCertifs" AutoGenerateColumns="false"
			Width="100%" ShowHeader="true" Height="700px">
			<Columns>
				<ig:UnboundField Key="IMAGE_ESTADO" CssClass="celdaImagenWebHierarchical" Width="20px" Header-CssClass="celdaImagenWebHierarchical"></ig:UnboundField>
				<ig:BoundDataField Key="ESTADO" DataFieldName="ESTADO" CssClass="Mano" Width="160px"></ig:BoundDataField>
				<ig:BoundDataField Key="FEC_SOLICITUD" DataFieldName="FEC_SOLICITUD" CssClass="Mano" Width="100px"></ig:BoundDataField>
				<ig:BoundDataField Key="FEC_LIM_CUMPLIM" DataFieldName="FEC_LIM_CUMPLIM" CssClass="Mano" Width="100px"></ig:BoundDataField>
				<ig:BoundDataField Key="INSTANCIA" DataFieldName="INSTANCIA"  CssClass="Mano" Width="70px"></ig:BoundDataField>
				<ig:BoundDataField Key="OBLIGATORIO" DataFieldName="OBLIGATORIO" CssClass="Mano" Width="40px"></ig:BoundDataField>
				<ig:BoundDataField Key="DEN" DataFieldName="DEN" CssClass="ManoBold"></ig:BoundDataField>
				<ig:BoundDataField Key="NOMBRE" DataFieldName="NOMBRE" CssClass="Enlace"></ig:BoundDataField>
				<ig:BoundDataField Key="ID_SOLIC" DataFieldName="ID_SOLIC" Hidden="true"></ig:BoundDataField>
				<ig:BoundDataField Key="ID" DataFieldName="ID" Hidden="true"></ig:BoundDataField>
				<ig:BoundDataField Key="FICHERO_ADJUNTO" DataFieldName="FICHERO_ADJUNTO" Hidden="true"></ig:BoundDataField>
				<ig:BoundDataField Key="PER" DataFieldName="PER" Hidden="true"></ig:BoundDataField>
				<ig:BoundDataField Key="EN_PROCESO" DataFieldName="EN_PROCESO" Hidden="true"></ig:BoundDataField>
				<ig:BoundDataField Key="FECALTA" DataFieldName="FECALTA" Hidden="true" CssClass="Mano"></ig:BoundDataField>
				<ig:BoundDataField Key="ESALTA" DataFieldName="ESALTA" Hidden="true"></ig:BoundDataField>
                <ig:UnboundField Key="ID_ENCRYPTED" Hidden="true"></ig:UnboundField>
                <ig:UnboundField Key="INSTANCIA_ENCRYPTED" Hidden="true"></ig:UnboundField>
			</Columns>
			<Behaviors>
				<ig:Selection Enabled="true" CellClickAction="Cell" RowSelectType="None" CellSelectType="Single" ColumnSelectType="None">
					<SelectionClientEvents CellSelectionChanging="wdgHDCertifs_CellClick" />
				</ig:Selection>
				<ig:Filtering Alignment="Top" Enabled="true" Visibility="Visible"></ig:Filtering>
				<ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>
				<ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
				<ig:ColumnMoving Enabled="false"></ig:ColumnMoving>
			</Behaviors>
			<GroupingSettings EnableColumnGrouping="True"></GroupingSettings>
		</ig:WebHierarchicalDataGrid>
	</div>
	</form>
</body>
</html>
