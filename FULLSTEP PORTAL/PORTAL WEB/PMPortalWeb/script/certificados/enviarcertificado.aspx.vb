Namespace Fullstep.PMPortalWeb
    Public Class enviarcertificado
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_PM_Server")
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_PM_User")
            Dim lCiaComp As Long = Request.Cookies("USU")("CIACOMP")
            Dim oError As Exception
            Dim sCertificados As String

            Dim sIdi As String = oUser.Idioma
            If sIdi = Nothing Then
                sIdi = ConfigurationSettings.AppSettings("idioma")
            End If

            Dim oCertificado As Fullstep.PMPortalServer.Certificado
            oCertificado = FSPMServer.Get_Certificado

            Dim bEsPrimeraGrabacion As Boolean = False

            If Request("Certificado") <> Nothing Then 'esta linea new
                oCertificado.Id = Request("Certificado")
                oCertificado.Prove = oUser.CodProve
                oCertificado.Load(lCiaComp, sIdi)
            Else 'new
                If Request("Error") = 0 Then
                    oCertificado.DesCrearCertificado(lCiaComp, oUser.IdCia, sCertificados, Request("IdCreate"), oUser.CodProve, Request("Solicitud"), True)

                    Response.Redirect("enviocertificadoko.aspx?Instancia=" & "&Certificado=" & "&Error=4")
                    oCertificado = Nothing
                    Exit Sub
                End If

                Dim ds As DataSet

                ds = GenerarDataset(Request)

                oError = oCertificado.CrearCertificado(lCiaComp, ds, sCertificados)

                If (oError Is Nothing) Then
                    oCertificado.Load(lCiaComp, sIdi)

                    oError = oCertificado.ActualizaInstanciaNula(oUser.IdCia, Request("IdCreate"))

                    If Not (oError Is Nothing) Then
                        'copia_campo cia=oUser.IdCia + instancia=null + idcreate=Request("IdCreate")
                        'copia_campo_adjun es por cia=oUser.IdCia + campo=copia_campo.id
                        'copia_adjun es por id autonumerico
                        'copia_linea_desglose es cia + linea + campopadre + campohijo de ds.
                        '   estos campo padre-hijo varian
                        'copia_linea_desglose_adjun es por id autonumerico

                        'Deshaz oCertificado.CrearCertificado
                        oCertificado.DesCrearCertificado(lCiaComp, oUser.IdCia, sCertificados, Request("IdCreate"), oUser.CodProve, Request("Solicitud"), False)

                        Response.Redirect("enviocertificadoko.aspx?Instancia=" & "&Certificado=" & "&Error=4")
                        oCertificado = Nothing
                        Exit Sub
                    End If

                    bEsPrimeraGrabacion = True
                Else
                    oCertificado.DesCrearCertificado(lCiaComp, oUser.IdCia, sCertificados, Request("IdCreate"), oUser.CodProve, Request("Solicitud"), True)

                    Response.Redirect("enviocertificadoko.aspx?Instancia=" & "&Certificado=" & "&Error=4")
                    oCertificado = Nothing
                    Exit Sub
                End If
            End If 'NEW

            Dim lVersion As Integer

            If Request("Version") <> Nothing Then
                lVersion = Request("Version")
                If lVersion <> Nothing Then
                    oCertificado.Version = Request("Version")
                End If
            End If

            Dim oExc As Exception
            Dim iRet As Integer = oCertificado.ValidarCertificado(lCiaComp)
            If iRet = 0 Then
                oExc = oCertificado.Enviar(lCiaComp, oUser.IdCia, oUser.CodProve, oUser.Email, oUser.Nombre, bEsPrimeraGrabacion, oUser.IdUsu)
            Else
                Response.Redirect("enviocertificadoko.aspx?Instancia=" & Request("Instancia") & "&Certificado=" & oCertificado.Id & "&Error=" & iRet)
                oCertificado = Nothing
                Exit Sub
            End If

            If oExc Is Nothing Then
                Response.Redirect("enviocertificadook.aspx?Instancia=" & Request("Instancia") & "&Certificado=" & oCertificado.Id)
            Else
                If bEsPrimeraGrabacion Then
                    oCertificado.DesCrearCertificado(lCiaComp, oUser.IdCia, sCertificados, Request("IdCreate"), oUser.CodProve, Request("Solicitud"), False)
                End If
                Response.Redirect("enviocertificadoko.aspx?Instancia=" & Request("Instancia") & "&Certificado=" & oCertificado.Id & "&Error=4")
            End If

            oCertificado = Nothing
        End Sub

        Private Function GenerarDataset(ByVal Request As System.Web.HttpRequest) As DataSet
            Dim oItem As System.Collections.Specialized.NameValueCollection
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_PM_User")
            Dim loop1 As Integer
            Dim arr1() As String
            Dim sRequest As String
            Dim oRequest As Object
            Dim dtNewRow As DataRow
            Dim DS As DataSet
            Dim dt As DataTable

            DS = New DataSet

            Dim oDTCertif As DataTable
            oDTCertif = DS.Tables.Add("TEMPNEWCERTIFICADO")
            oDTCertif.Columns.Add("TIPO_CERTIF", System.Type.GetType("System.Int32"))
            oDTCertif.Columns.Add("PROVE", System.Type.GetType("System.String"))
            oDTCertif.Columns.Add("FEC_DESPUB", System.Type.GetType("System.DateTime"))
            oDTCertif.Columns.Add("PETICIONARIO", System.Type.GetType("System.String"))
            oDTCertif.Columns.Add("CONTACTO", System.Type.GetType("System.String"))

            dtNewRow = oDTCertif.NewRow
            dtNewRow.Item("TIPO_CERTIF") = Request("Solicitud")
            dtNewRow.Item("PROVE") = oUser.CodProve
            dtNewRow.Item("FEC_DESPUB") = System.DBNull.Value
            dtNewRow.Item("PETICIONARIO") = System.DBNull.Value
            dtNewRow.Item("CONTACTO") = Request("Contacto")

            oDTCertif.Rows.Add(dtNewRow)

            Return DS

        End Function

    End Class

End Namespace
