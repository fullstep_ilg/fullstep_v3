<%@ Page Language="vb" AutoEventWireup="false" Codebehind="enviocertificadoKO.aspx.vb" Inherits="Fullstep.PMPortalWeb.enviocertificadoKO"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
		<title>enviocertificadoKO</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../../common/estilo.asp" type="text/css" rel="stylesheet">
		<script src="../../../common/formatos.js"></script>
		<script src="../../../common/menu.asp"></script>	
        <script type="text/javascript">
            /*''' <summary>
            ''' Iniciar la pagina.
            ''' </summary>     
            ''' <remarks>Llamada desde: page.onload; Tiempo m�ximo:0</remarks>*/
            function Init() {
                if (accesoExterno == 0) { document.getElementById('tablemenu').style.display = 'block'; }
            }
        </script>        	
	</head>
	<body onload="Init()">
		<script>dibujaMenu(2)</script>
		<form id="Form1" method="post" runat="server">
			<TABLE class="bordeadoAzul" id="tblCabecera" cellSpacing="2" cellPadding="1" width="100%"
				border="0">
				<tr>
					<td width="100%" colSpan="3"><asp:label id="lblNombreCerticado" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></td>
				</tr>
				<tr>
					<td colSpan="3">&nbsp;</td>
				</tr>
				<TR>
					<TD><asp:label id="lblId" runat="server" CssClass="captionBlue" Width="100%"></asp:label></TD>
					<TD><asp:label id="lblSolicitante" runat="server" CssClass="captionBlue" Width="100%"></asp:label></TD>
					<TD><asp:label id="lblFechaLimite" runat="server" CssClass="captionBlue" Width="100%"></asp:label></TD>
				</TR>
				<TR>
					<TD><asp:label id="txtId" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></TD>
					<TD><asp:label id="txtSolicitante" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></TD>
					<TD><asp:label id="txtFechaLimite" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></TD>
				</TR>
				<tr>
					<td colSpan="3">&nbsp;</td>
				</tr>
				<tr>
					<td width="100%" colSpan="3"><asp:label id="mensajeKo" runat="server" CssClass="error" Width="100%"></asp:label></td>
				</tr>
				<tr>
					<td colSpan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colSpan="3">
						<asp:label id="lblMotivo" runat="server" CssClass="parrafo" Width="100%"></asp:label>
					</td>
				</tr>
				<tr>
					<td colSpan="3"><li>
							<asp:label id="txtMotivo" runat="server" CssClass="parrafo" Width="100%"></asp:label></li></td>
				</tr>
			</TABLE>
		</form>
	</body>
</html>
