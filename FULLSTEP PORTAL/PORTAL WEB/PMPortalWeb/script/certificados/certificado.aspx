<%@ Page Language="vb" AutoEventWireup="false" Codebehind="certificado.aspx.vb" Inherits="Fullstep.PMPortalWeb.certificado" EnableSessionState="True" enableViewState="False"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
		<title>solicitudes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<link href="../../../common/estilo.asp" type="text/css" rel="stylesheet" />
		<script type="text/javascript" src="../../../common/formatos.js"></script>
		<script type="text/javascript" src="../../../common/menu.asp"></script>
		<script type="text/javascript" src="/ig_common/20043/scripts/ig_WebGrid.js"></script>
		<script type="text/javascript" src="../../js/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="../../js/jquery/jquery-migrate.min.js"></script>
		<script type="text/javascript" src="../../js/jquery/jquery.tmpl.min.js"></script>
	</head>
	<body onresize="resize()" onload="init()" onunload="if (wProgreso != null) OcultarEspera();wProgreso=null;">
		<script type="text/javascript">dibujaMenu(2)</script>
		<script type="text/javascript"><!--
			// 1.- Creamos el objeto xmlHttpRequest
			CreateXmlHttp();
			var bEnProceso = false;
			wProgreso = null;
			var tipoVersion = "0";
			function CreateXmlHttp() {
				/*''' <summary>
				''' Creaci�n del objeto para las llamdas ajax
				''' </summary>
				''' <remarks>Llamada desde: Esta javascript; Tiempo m�ximo: 0 </remarks>*/
				// Probamos con IE
				try {
					// Funcionar� para JavaScript 5.0
					xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch (e) {
					try {
						xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					catch (oc) {
						xmlHttp = null;
					}
				}
				// Si no se trataba de un IE, probamos con esto
				if (!xmlHttp && typeof XMLHttpRequest != "undefined") {
					xmlHttp = new XMLHttpRequest();
				}
				return xmlHttp;
			};
			function resize() {
				/*Descripcion: Le pone un ancho al desglose
				Llamada desde:carga de la pagina
				Tiempo ejecucion:0,1seg.*/
				for (i = 0; i < arrDesgloses.length; i++) {
				    sDiv = arrDesgloses[i].replace("tblDesglose", "divDesglose");
				    if (parseFloat(document.body.offsetWidth) >= 70) {
				        document.getElementById(sDiv).style.width = (parseFloat(document.body.offsetWidth) - 70) + "px";
				    }
				}
			};
			function localEval(s) {
			    eval(s);
			}
			function MostrarEspera() {
				/*Descripcion: Muestra los divs que indican que se esta realizando un proceso.
				Llamada desde:=Guardar() // Trasladar() 
				Tiempo ejecucion:0,1seg.*/
				wProgreso = true;
				document.getElementById("divForm2").style.display = 'none';
				document.getElementById("divForm3").style.display = 'none';
				document.getElementById("igtabuwtGrupos").style.display = 'none';

				i = 0;
				bSalir = false;
				while (bSalir == false) {
					if (document.getElementById("uwtGrupos_div" + i)) {
						document.getElementById("uwtGrupos_div" + i).style.visibility = 'hidden';
						i = i + 1;
					} else {
						bSalir = true;
					};
				};
				document.getElementById("lblProgreso").value = frmDetalle.cadenaespera.value;
				document.getElementById("divProgreso").style.display = '';
			}
			function OcultarEspera() {
				/*Descripcion: Oculta los divs que indican que se esta realizando un proceso y Muestra los grupos.
				Llamada desde:=HabilitarBotones y onunload
				Tiempo ejecucion:0,1seg.*/
				wProgreso = null;
				document.getElementById("divProgreso").style.display = 'none';
				document.getElementById("divForm2").style.display = '';
				document.getElementById("divForm3").style.display = '';
				if (document.getElementById("igtabuwtGrupos")) {
				    document.getElementById("igtabuwtGrupos").style.display = '';
				}
				i = 0;
				bSalir = false;
				while (bSalir == false) {
					if (document.getElementById("uwtGrupos_div" + i)) {
						document.getElementById("uwtGrupos_div" + i).style.visibility = 'visible';
						i = i + 1;
					} else {
						bSalir = true;
					};
				};
			}            
			function MontarSubmitGuardar(iVersion, iInstancia) {
				/*Descripcion:=Monta el formulario para el posterior guardado. (guardarInstancia.aspx)
				Llamada desde:resultadoProcesarAcciones()
				Tiempo ejecucion:1,5seg.*/
			    MontarFormularioSubmit();
			    document.forms["frmDetalle"].elements["Solicitud"].value = 0; /*AL GUARDAR CREAREMOS YA EL CERTIFICADO POR LO QUE NO NECESITAMOS LA SOLICITUD*/
				document.forms["frmSubmit"].elements["GEN_Enviar"].value = 0;
				document.forms["frmSubmit"].elements["GEN_Version"].value = iVersion;
				document.forms["frmSubmit"].elements["GEN_Instancia"].value = iInstancia;
				document.forms["frmSubmit"].elements["GEN_Accion"].value = "guardarcertificado";
				oFrm = MontarFormularioCalculados();
				sInner = oFrm.innerHTML;
				oFrm.innerHTML = "";
				document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner);
				document.forms["frmSubmit"].submit();
            }
            /*''' <summary>
            ''' Guardar el certificado
            ''' </summary>      
            ''' <remarks>Llamada desde: cmdGuardar; Tiempo m�ximo: 0</remarks>*/
			function Guardar() {
			    if (comprobarTextoLargo() == false)
			        return false;

	            var iVersion;
	            var iInstancia;
	            var itipoVersion;
                
				//deshabilitamos botones
	            if (document.forms["frmDetalle"].elements["cmdGuardar"])
	                if (document.forms["frmDetalle"].elements["cmdGuardar"].disabled)
	                    return false
	                else
	                    document.forms["frmDetalle"].elements["cmdGuardar"].disabled = true;
	            if (document.forms["frmDetalle"].elements["cmdEnviar"])
	                if (document.forms["frmDetalle"].elements["cmdEnviar"].disabled)
	                    return false
	                else
	                    document.forms["frmDetalle"].elements["cmdEnviar"].disabled = true;

				if (document.forms["frmDetalle"].elements["cmdImpExp"]) document.forms["frmDetalle"].elements["cmdImpExp"].disabled = true;
				if (document.forms["frmDetalle"].elements["cmdCalcular"]) document.forms["frmDetalle"].elements["cmdCalcular"].disabled = true;
				if (document.forms["frmDetalle"].elements["cmdCalcular2"]) document.forms["frmDetalle"].elements["cmdCalcular2"].disabled = true;

				iVersion = document.forms["frmDetalle"].elements["Version"].value;
				iInstancia = document.forms["frmDetalle"].elements["Instancia"].value;
				if (wProgreso == null) {
					wProgreso = true;
					MostrarEspera();
				};
				if (xmlHttp) {
				    itipoVersion = document.forms["frmDetalle"].elements["tipoVersion"].value;
					var params = "Certificado=" + document.forms["frmDetalle"].elements["Certificado"].value + "&Instancia=" + iInstancia + "&Version=" + iVersion + "&tipoVersion=" + itipoVersion;

					xmlHttp.open("POST", "../_common/controlarVersionQA.aspx", false);
					xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
					xmlHttp.send(params);
					
					resultadoProcesarAcciones(false, iInstancia, iVersion);
				} else {
					alert(arrTextosML[1]);
					HabilitarBotones()
				};
			};
			function MontarSubmitEnviar(iVersion, iInstancia) {
				/*Descripcion:=Monta el formulario para el posterior envio. (guardarInstancia.aspx)
				Llamada desde:resultadoProcesarAcciones()
				Tiempo ejecucion:1,5seg.*/

			    MontarFormularioSubmit();
			    //Por si por segunda vez entra aqui USANDO EL BROWSER HISTORY BACK. La 1ra vez se limpia la solicitud/versi�n y entonces la 2da no es por solicitud -> NO instancia_create, S� instancia_save.
			    document.forms["frmDetalle"].elements["Solicitud"].value = "0"
			    document.forms["frmDetalle"].elements["Version"].value = "-1234" //este numero se usa en guardarinstancia.aspx.vb y stored FSQA_LOADCORRESPONDENCIACAMPOS.

				document.forms["frmSubmit"].elements["GEN_Enviar"].value = 1
				document.forms["frmSubmit"].elements["GEN_Accion"].value = "enviarcertificado"
				document.forms["frmSubmit"].elements["GEN_Version"].value = iVersion
				document.forms["frmSubmit"].elements["GEN_Instancia"].value = iInstancia
				oFrm = MontarFormularioCalculados()
				sInner = oFrm.innerHTML
				oFrm.innerHTML = ""
				document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
				document.forms["frmSubmit"].submit()
			} 
		    /*''' <summary>
			''' Descripci�n:	funci�n que se ejecuta cuando el usuario pulsa el bot�n de Enviar Certificado
			''' 1� Validar los campos obligatorios del formulario
			''' 2� Obtener la versi�n y la instancia
			''' 3� Deshabilita los botones, muestra la capa de espera y llama a la funci�n MontarSubmitEnviar
			''' (los par�metros iVersion e iInstancia de MontarSubmitEnviar pueden ser "" cd el certificado es autom�tico
			''' por eso he a�adido a la cadena de llamada a esta funci�n unas comillas a los par�metros)		
			''' </summary>      
				''' <remarks>Llamada desde: cmdEnviar; Tiempo m�ximo: 0</remarks>*/
			function Enviar() {	
			    var respOblig = comprobarObligatorios();
				switch (respOblig) {
					case "":  //no falta ningun campo obligatorio
						break;
		            case "filas0": //no se han introducido filas en un desglose obligatorio
		                alert(arrTextosML[0]);
		                return false;
		                break;
		            default: //falta algun campo obligatorio
		                alert(arrTextosML[0] + '\n' + respOblig);
		                return false;
		                break;
				};

				if (comprobarTextoLargo() == false) return false;

				var iVersion;
				var iInstancia;
				var itipoVersion;

				iVersion = document.forms["frmDetalle"].elements["Version"].value;
				iInstancia = document.forms["frmDetalle"].elements["Instancia"].value;

				//deshabilitamos botones
				if (document.forms["frmDetalle"].elements["cmdGuardar"] && document.forms["frmDetalle"].elements["cmdGuardar"].disabled) return false
				else document.forms["frmDetalle"].elements["cmdGuardar"].disabled = true;

				if (document.forms["frmDetalle"].elements["cmdEnviar"] && document.forms["frmDetalle"].elements["cmdEnviar"].disabled) return false
				else document.forms["frmDetalle"].elements["cmdEnviar"].disabled = true;

				if (document.forms["frmDetalle"].elements["cmdImpExp"]) document.forms["frmDetalle"].elements["cmdImpExp"].disabled = true;
				if (document.forms["frmDetalle"].elements["cmdCalcular"]) document.forms["frmDetalle"].elements["cmdCalcular"].disabled = true;
				if (document.forms["frmDetalle"].elements["cmdCalcular2"]) document.forms["frmDetalle"].elements["cmdCalcular2"].disabled = true;

				if (wProgreso == null) {
					wProgreso = true;
					MostrarEspera();
				};
				if (xmlHttp) {
				    itipoVersion = document.forms["frmDetalle"].elements["tipoVersion"].value;
					var params = "Certificado=" + document.forms["frmDetalle"].elements["Certificado"].value + "&Instancia=" + iInstancia + "&Version=" + iVersion + "&tipoVersion=" + itipoVersion;

					xmlHttp.open("POST", "../_common/controlarVersionQA.aspx", false);
					xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
					xmlHttp.send(params);

					resultadoProcesarAcciones(true, iInstancia, iVersion);
				} else {
					alert(arrTextosML[1]);
					HabilitarBotones();
				};
			};
			function resultadoProcesarAcciones(bParam, iInstancia, iVersion) {
				/*''' <summary>
				''' Tras q se ejecute sincronamente controlarVersionQA.aspx controlamos sus resultados y obramos en 
				''' consecuencia.
				''' </summary>
				''' <param name="bParam">envias(1) o guardas (0)</param>  
				''' <param name="iInstancia">Instancia</param>  
				''' <param name="iVersion">Version</param>     
				''' <remarks>Llamada desde:Guardar     Enviar; Tiempo m�ximo: 0</remarks>*/
				var retorno;
				if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				    retorno = xmlHttp.responseText;

				    if (retorno == 1) { //en proceso
				        alert(arrTextosML[2]);
				        HabilitarBotones();
				        return
				    } else
				        if (retorno == 2) {//No se pueden llevar a cabo las modificaciones ya que mientras editaba el 
				            //certificado el peticionario lo ha renovado. Acceda de nuevo al certificado
				            alert(arrTextosML[3]);
				            HabilitarBotones();
				            return
				        } else
				            if (retorno == 3) {//No se pueden llevar a cabo las modificaciones ya que mientras editaba el 
				                //certificado el peticionario lo ha despublicado.
				                alert(arrTextosML[4]);
				                HabilitarBotones();
				                return
				            } else
				                if (retorno == 4) {//No se pueden llevar a cabo las modificaciones ya que mientras editaba el 
				                    //certificado el peticionario lo ha modificado y ha eliminado sus datos guardados sin enviar.
				                    // Acceda de nuevo al certificado
				                    alert(arrTextosML[5]);
				                    HabilitarBotones();
				                    return
				                } else
				                    if (retorno == 5) {//No se pueden llevar a cabo las modificaciones ya que mientras 
				                        //editaba el certificado el peticionario ha efectuado cambios sobre el. Acceda de nuevo 
				                        //al certificado.
				                        alert(arrTextosML[6]);
				                        HabilitarBotones();
				                        return
				                    };
				};

				if (bParam)
				    setTimeout("MontarSubmitEnviar('" + iVersion + "','" + iInstancia + "')", 100);
				else
				    setTimeout("MontarSubmitGuardar('" + iVersion + "','" + iInstancia + "')", 100);
			}
			function CalcularCamposCalculados() {
			    oFrm = MontarFormularioCalculados();
			    oFrm.submit();
			}
			function init() {
				/*''' <summary>
				''' Iniciar la pagina.
				''' </summary>     
				''' <remarks>Llamada desde: page.onload; Tiempo m�ximo:0</remarks>*/
			    if (accesoExterno == 0) {document.getElementById('tablemenu').style.display = 'block';}

				resize();
				AgruparEstilos();
			};
			function wddVersion_selectedIndexChanged(sender, eventArgs) {
				/*''' <summary>
				''' Evento que salta tras elegir una version en el combo. Carga los datos de la noConformidad en esa version
				''' </summary>
				''' <param name="sender">componente input</param>
				''' <param name="eventArgs">evento</param>    
				''' <remarks>Llamada desde=evento del dropdown; Tiempo maximo=0seg.</remarks>*/
				var aValue = sender.get_selectedItems()[0].get_value().split("##");

				var iInstancia = aValue[0];
				var iVersion = aValue[1];
				var iCertificado = aValue[2];

				var FechaLocal;
				FechaLocal = new Date();
				otz = FechaLocal.getTimezoneOffset();

				window.open("certificado.aspx?Instancia=" + iInstancia.toString() + "&Certificado=" + iCertificado.toString() + "&Version=" + iVersion.toString() + "&Otz=" + otz.toString(), "_self")
			}
			function HabilitarBotones() {
				/*''' <summary>
				''' Tras grabar los bts deben volver a estar accesibles y desaparecer el recuadro de Cargando...
				''' </summary>
				''' <remarks>Llamada desde: PonerEstado		FinalCarga; Tiempo m�ximo:0</remarks>*/
				if (document.forms["frmDetalle"].elements["cmdGuardar"]) document.forms["frmDetalle"].elements["cmdGuardar"].disabled = false;
				if (document.forms["frmDetalle"].elements["cmdEnviar"]) document.forms["frmDetalle"].elements["cmdEnviar"].disabled = false;
				if (document.forms["frmDetalle"].elements["cmdImpExp"]) document.forms["frmDetalle"].elements["cmdImpExp"].disabled = false;
				if (document.forms["frmDetalle"].elements["cmdCalcular"]) document.forms["frmDetalle"].elements["cmdCalcular"].disabled = false;
				if (document.forms["frmDetalle"].elements["cmdCalcular2"]) document.forms["frmDetalle"].elements["cmdCalcular2"].disabled = false;

				OcultarEspera();
			}  
			function PonerEstado(estado, guardadaenportal, ListaCalc) {
				/*''' <summary>
				''' Tras un guardado sin enviar no se hace ni submit ni se va a otra pantalla. Eso implica q haya datos q no estan 
				'''	actualizados en pantalla pero si en bbdd. Esta funci�n refleja estos cambios.
				''' </summary>
				''' <param name="estado">Texto multiidioma de guardado sin enviar</param>
				''' <param name="guardadaenportal">Texto multiidioma de guardado sin enviar/param>
				''' <param name="ListaCalc">Lista de pares que contiene los Id de los calculados y su valor ya calculado</param> 		
				''' <remarks>Llamada desde: GuardarInstancia.aspx; Tiempo m�ximo:0,1</remarks>*/			    
				document.getElementById("lblSinEnviar").value = estado;
				document.forms["frmDetalle"].elements["Solicitud"].value = 0; /*AL GUARDAR CREAREMOS YA EL CERTIFICADO POR LO QUE NO NECESITAMOS LA SOLICITUD*/
				var iVersion
				if (document.forms["frmDetalle"].elements["tipoVersion"].value == "3") iVersion = parseInt(document.forms["frmDetalle"].elements["Version"].value);				
				else iVersion = (document.forms["frmDetalle"].elements["Version"].value == '') ? 1 : parseInt(document.forms["frmDetalle"].elements["Version"].value) + 1;

				document.forms["frmDetalle"].elements["Version"].value = iVersion;
				document.forms["frmDetalle"].elements["tipoVersion"].value = 3;

				HabilitarBotones();

				var ParListaCalc = "";
				var ListaCalcId = "";
				var ListaCalcVal = "";

				while (ListaCalc != "") {
					ParListaCalc = ListaCalc.substring(0, ListaCalc.indexOf("#"))

					ListaCalcId = ParListaCalc.substring(0, ParListaCalc.indexOf("@"))
					ListaCalcVal = ParListaCalc.substring(ParListaCalc.indexOf("@") + 1)

					document.getElementById(ListaCalcId).value = ListaCalcVal;

					if (ListaCalc.indexOf("#")) ListaCalc = ListaCalc.substring(ListaCalc.indexOf("#") + 1);
				};

				wProgreso = true;
				var combo = $find('<%=wddVersion.ClientID %>');
				if (combo && combo.get_items().get_length()>0) {
					var item = combo.get_items().getItem(0)
					item.set_text(guardadaenportal);

					combo.set_activeItem(item, true)
					combo.set_visible(true);

					combo.selectItemByIndex(0, true, true);

					item._element.innerText = guardadaenportal;
				};
				wProgreso = null;
			};
			function cmdImpExp_onclick() {
				/*''' <summary>
				''' Exportar el certificado 
				''' </summary>
				''' <remarks>Llamada desde: bt cmdImpExp; Tiempo m�ximo:0</remarks>*/
				if (document.forms["frmDetalle"].elements["tipoVersion"].value == "3") {
					var valor;
					valor = parseInt(document.forms["frmDetalle"].elements["Version"].value) + 1;
					valor += '';
					window.open('../_common/impexp_sel.aspx?Instancia=' + document.getElementById("Instancia").value + '&Certificado=' + document.forms["frmDetalle"].elements["Certificado"].value + '&Version=' + valor + '&TipoImpExp=2', '_new', 'fullscreen=no,height=115,width=315,location=no,menubar=no,resizable=no,scrollbars=no,status=yes,titlebar=yes,toolbar=no,left=200,top=200');
				} else {
				    window.open('../_common/impexp_sel.aspx?Instancia=' + document.getElementById("Instancia").value + '&Certificado=' + document.forms["frmDetalle"].elements["Certificado"].value + '&Version=' + document.forms["frmDetalle"].elements["Version"].value + '&TipoImpExp=2', '_new', 'fullscreen=no,height=115,width=315,location=no,menubar=no,resizable=no,scrollbars=no,status=yes,titlebar=yes,toolbar=no,left=200,top=200');
				};
			};
			function FinalCarga() {
			    /*
			    ''' <summary>
			    ''' Mostrar u ocultar la label q indica q hay campos obligatorios a rellenar (caso normal) o q indica q no hay campos visibles y por eso no hay nada accesible (caso a controlar).
			    ''' </summary>
			    ''' <remarks>Llamada desde: html de la pagina ; Tiempo m�ximo: 0</remarks>*/
			    if (document.forms["frmDetalle"].elements["enproceso"].value != "1") HabilitarBotones();
			    if (document.getElementById("igtabuwtGrupos")) {//Caso Normal: hay grupos pq hay campos
			        if (arrObligatorios.length > 0 && document.getElementById("lblCamposObligatorios")) document.getElementById("lblCamposObligatorios").style.display = "";
			    } else { //Caso a controlar: No hay campos visibles.
			        document.getElementById("lblCamposObligatorios").style.display = "";
			    }
			};
		--></script>
		<form id="frmDetalle" method="post" runat="server">
			<asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
            <asp:ScriptReference Name="Common.Common.js" Assembly="AjaxControlToolkit" />
            </Scripts>
            </asp:ScriptManager>
			<input id="Instancia" type="hidden" name="Instancia" runat="server"/>
			<input id="Certificado" type="hidden" name="Certificado" runat="server"/>
			<input id="Version" type="hidden" name="Version" runat="server"/>
			<input id="VersionInicialCargada" type="hidden" name="VersionInicialCargada" runat="server"/>
			<input id="CertificadoEnviado" type="hidden" name="CertificadoEnviado" runat="server"/>
			<input id="Solicitud" type="hidden" name="Solicitud" runat="server"/>
			<input id="Contacto" type="hidden" name="Contacto" runat="server"/>
			<input id="tipoVersion" type="hidden" name="tipoVersion" runat="server"/>
			<input type="hidden" runat="server" id="diferenciaUTCServidor" name="diferenciaUTCServidor"/>
			<iframe id="iframeWSServer" style="z-index: 103; left: 8px; visibility: hidden; position: absolute;
				top: 200px" name="iframeWSServer" src="../blank.htm"></iframe>            
			<table id="tblCabecera" cellspacing="5" cellpadding="1"
				width="100%" border="0">
				<tr>
					<td width="100%">
						<table class="bordeadoAzul" id="tblCabeceraSolicitud" cellspacing="2" cellpadding="1" width="100%" border="0">
							<tbody>
								<tr>
									<td style="text-align:center;" colSpan="4">
										<asp:textbox id="lblSinEnviar" style="text-align:center" runat="server" CssClass="fntRed" BorderWidth="0" BorderStyle="None" Width="100%"></asp:textbox>
									</td>
								</tr>
								<tr>
									<td width="100%" colspan="4">
										<table cellspacing="0" cellpadding="0" width="100%" border="0">
											<tr>
												<td>
													<asp:label id="lblNombreCerticado" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label>
												</td>
												<td align="right" width="10%">
													<input class="boton" id="cmdCalcular" disabled onclick="return CalcularCamposCalculados()"
														type="button" value="Calcular" name="cmdCalcular" runat="server"/>
												</td>
												<td align="right" width="10%">
													<input class="boton" id="cmdGuardar" onclick="return Guardar();" type="button" value="DGuardar"
														name="cmdGuardar" runat="server" disabled/>
													<input class="boton" id="cmdCalcular2" onclick="return CalcularCamposCalculados()" type="button"
														value="dCalcular" name="cmdCalcular2" runat="server" disabled/>
												</td>
												<td align="right" width="10%">
													<input class="boton" id="cmdEnviar" onclick="return Enviar();" type="button" value="Enviar datos"
														name="cmdEnviar" runat="server" disabled/>
													</td>
												<td align="right" width="10%">
													<input class="boton" id="cmdImpExp" onclick="return cmdImpExp_onclick();" type="button"
														value="Imprimir" name="cmdImpExp" runat="server" disabled/>
													</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td width="10%"><asp:label id="lblId" runat="server" CssClass="captionBlue" Width="100%"></asp:label></td>
									<td width="20%"><asp:label id="lblSolicitante" runat="server" CssClass="captionBlue" Width="100%"></asp:label></td>
									<td width="37%"><asp:label id="lblFechaLimite" runat="server" CssClass="captionBlue" Width="100%"></asp:label></td>
									<td><asp:label id="lblRespuestas" runat="server" CssClass="captionBlue" Width="100%"></asp:label></td>
								</tr>
								<tr>
									<td width="10%"><asp:label id="txtId" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></td>
									<td width="20%"><asp:label id="txtSolicitante" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></td>
									<td width="37%"><asp:label id="txtFechaLimite" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></td>
									<td>
										 <ig:WebDropDown ID ="wddVersion" runat="server" EnableViewState="true" Height="22px" Width="328px" 
											EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" EnableClosingDropDownOnBlur="true">
											<ClientEvents SelectionChanged="wddVersion_selectedIndexChanged"/>
										</ig:WebDropDown>  
									</td>
								</tr>
								<tr>
									<td colspan="4"><asp:label id="lblCamposObligatorios" style="z-index: 102; display:none;" runat="server" CssClass="captionRed">Los campos marcados con (*) son de obligada cumplimentacion</asp:label></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</table>
			<div id="divProgreso">
				<table id="tblProgreso" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
					<tr style="height:50px">
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center" width="100%">
							<asp:textbox id="lblProgreso" style="TEXT-ALIGN: center" runat="server" CssClass="captionBlue"
								BorderWidth="0" BorderStyle="None" Width="100%">
								Su solicitud est� siendo tramitada. Espere unos instantes...
							</asp:textbox>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center" width="100%">
							<asp:image id="imgProgreso" runat="server" src="../_common/images/cursor-espera_grande.gif"></asp:image>
						</td>
					</tr>
				</table>
			</div>
			<div id="divForm2" style="float:left; width:100%;">
				<table cellspacing="0" cellpadding="0" style="width:100%;" border="0">
					<tr>
						<td valign="middle">
							<igtab:ultrawebtab id="uwtGrupos" runat="server" Width="100%" BorderWidth="1px" BorderStyle="Solid" 
								DisplayMode="Scrollable" CustomRules="padding:10px;" FixedLayout="True" 
								DummyTargetUrl=" " ThreeDEffect="False">
								<DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
									<Padding Left="20px" Right="10px"></Padding>
								</DefaultTabStyle>
								<RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter"></RoundedImage>
								<Tabs>
									<igtab:Tab Text="New Tab"></igtab:Tab>
									<igtab:Tab Text="New Tab"></igtab:Tab>
								</Tabs>
							</igtab:ultrawebtab>
						</td>
					</tr>
				</table>
				<div id="divDropDowns" style="z-index:103; visibility:hidden; position:absolute; top:300px;"></div>
				<div id="divCalculados" style="z-index:109; visibility:hidden; position:absolute; top:0px;" name="divCalculados"></div>
				<div id="divAlta" style="visibility:hidden"></div>
				<input id="cadenaespera" type="hidden" name="cadenaespera" runat="server"/> 
				<input id="enproceso" type="hidden" name="enproceso" runat="server"/>
				<input id="BotonCalcular" type="hidden" value="0" name="BotonCalcular" runat="server"/>
			</div>
		</form>
		<div id="divForm3" style="display:none;">
			<form id="frmCalculados" name="frmCalculados" action="../_common/recalcularimportes.aspx"
				method="post" target="fraPMPortalServer">
			</form>
			<form id="frmDesglose" name="frmDesglose" method="post" target="winDesglose"></form>
		</div>
		<script type="text/javascript">FinalCarga();</script>
	</body>
</html>
