﻿Imports Infragistics.Web.UI.GridControls
Imports Infragistics.Documents.Reports
Imports Infragistics.Documents.Reports.Report
Imports System.Text.RegularExpressions

Namespace Fullstep.PMPortalWeb
    Public Class CertificadosVisor
        Inherits FSPMPage

        Private bCertifPdteEnviar As Boolean
#Region "Properties"
        ''' <summary>
        ''' Devolver el Id de la compania
        ''' </summary>
        ''' <returns>Id de la compania</returns>
        ''' <remarks>Llamada desde: property oSolicitud property oInstancia property oCertificado; Tiempo máximo:0</remarks>
        Protected ReadOnly Property lCiaComp As Long
            Get
                lCiaComp = IdCiaComp
            End Get
        End Property
        Private _oCertificados As DataSet
        ''' <summary>
        ''' Crear el objeto con los Certificados para el proveedor. 
        ''' </summary>
        ''' <returns>DataSet con los Certificados</returns>
        ''' <remarks>Llamada desde: CargarGridCertificados ; Tiempo máximo:0,2</remarks>
        Protected ReadOnly Property oCertificados() As DataSet
            Get
                If _oCertificados Is Nothing Then
                    If IsPostBack Then
                        _oCertificados = CType(Cache("oCertificados_" & FSPMUser.IdCia & "_" & FSPMUser.Cod), DataSet)
                        If _oCertificados Is Nothing Then
                            Dim oCerts As Fullstep.PMPortalServer.Certificados
                            oCerts = FSPMServer.Get_Certificados
                            _oCertificados = oCerts.Load_Certificados(lCiaComp, FSPMUser.CodProveGS, FSPMUser.Idioma)
                            ConfigurarCertificados(_oCertificados)
							InsertarEnCache("oCertificados_" & FSPMUser.IdCia & "_" & FSPMUser.Cod, _oCertificados, Caching.CacheItemPriority.BelowNormal)
						End If
                    Else
                        Dim oCerts As Fullstep.PMPortalServer.Certificados
                        oCerts = FSPMServer.Get_Certificados
                        _oCertificados = oCerts.Load_Certificados(lCiaComp, FSPMUser.CodProveGS, FSPMUser.Idioma)
                        ConfigurarCertificados(_oCertificados)
						InsertarEnCache("oCertificados_" & FSPMUser.IdCia & "_" & FSPMUser.Cod, _oCertificados, Caching.CacheItemPriority.BelowNormal)
					End If
                End If

                Return _oCertificados
            End Get
        End Property
        Private Sub ConfigurarCertificados(ByRef ds As DataSet)
            ds.Tables(0).TableName = "CERTIFICADOS"

            ds.Tables("CERTIFICADOS").Columns.Add("ESTADO", System.Type.GetType("System.String"))
            ds.Tables("CERTIFICADOS").Columns.Add("ID_ENCRYPTED", System.Type.GetType("System.String"))
            ds.Tables("CERTIFICADOS").Columns.Add("INSTANCIA_ENCRYPTED", System.Type.GetType("System.String"))

			For Each row As DataRow In ds.Tables("CERTIFICADOS").Rows
				If DBNullToSomething(row("EN_PROCESO")) = "0" Then
					If row("PDTE_ENVIAR") Then
						row("ESTADO") = Textos(28) 'Pendiente de Enviar
					Else
						Select Case row("ID_TEXTO_ESTADO")
							Case 64
								row("ESTADO") = Textos(9) 'Pend cumplimentar
							Case 65
								row("ESTADO") = Textos(10) 'Vigente
							Case 66
								row("ESTADO") = Textos(20) 'Proximo a expirar
							Case 67
								row("ESTADO") = Textos(11) 'Expirado
							Case 80
								row("ESTADO") = Textos(26) 'Pendiente validar
							Case Else
								row("ESTADO") = Textos(27) 'No valido
						End Select
					End If
				Else
					row("ESTADO") = Textos(12) 'En proceso
				End If
			Next
		End Sub
#End Region
#Region "Inicializar pagina"
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
			ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.VisorCertificados

			CargarTextos()

            If Not IsPostBack Then
                FSNPageHeader.UrlImagenCabecera = "~/App_Themes/" & Page.Theme & "/images/certificado.png"

                HttpContext.Current.Cache.Remove("oCertificados_" & FSPMUser.IdCia & "_" & FSPMUser.Cod)

                ConfigurarGrid()
                CargarGridCertificados()
            Else
                CargarGridCertificados()
                Select Case Request("__EVENTTARGET")
                    Case "Excel"
                        ExportarExcel()
                    Case "PDF"
                        ExportarPDF()
                End Select
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutanormal") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutanormal", "var rutanormal ='" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "';", True)
            End If
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "lCiaComp") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "lCiaComp", "var lCiaComp =" & IdCiaComp & ";", True)
            End If
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)
            End If
        End Sub
        ''' <summary>
        ''' Carga el idioma correspondiente en los controles
        ''' </summary>
        ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
        Private Sub CargarTextos()
            FSNPageHeader.TituloCabecera = Textos(0)
            Me.lblSubtitulo.Text = Textos(1)

            Dim field As Infragistics.Web.UI.GridControls.BoundDataField
            field = wdgHDCertifs.Columns.FromKey("ESTADO")
            field.Header.Text = Textos(2)
            field = wdgHDCertifs.Columns.FromKey("FEC_SOLICITUD")
            field.Header.Text = Textos(3)
            field = wdgHDCertifs.Columns.FromKey("FEC_LIM_CUMPLIM")
            field.Header.Text = Textos(4)
            field = wdgHDCertifs.Columns.FromKey("INSTANCIA")
            field.Header.Text = Textos(5)
            field = wdgHDCertifs.Columns.FromKey("DEN")
            field.Header.Text = Textos(6)
            field = wdgHDCertifs.Columns.FromKey("NOMBRE")
            field.Header.Text = Textos(7)
            field = wdgHDCertifs.Columns.FromKey("OBLIGATORIO")
            field.Header.Text = Textos(23)

			wdgHDCertifs.GroupingSettings.EmptyGroupAreaText = Textos(22)

			lblCertifPdteEnviar.Text = Textos(29)
		End Sub
        Private Sub ConfigurarGrid()
            Dim ImageEstadoColumn As New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
            ImageEstadoColumn.ColumnKey = "IMAGE_ESTADO"
            ImageEstadoColumn.Enabled = False

            Dim fechaSolicitud As Infragistics.Web.UI.GridControls.BoundDataField
            Dim fechaLimiteCumplimentacion As Infragistics.Web.UI.GridControls.BoundDataField

            With wdgHDCertifs
                .Behaviors.Filtering.ColumnSettings.Add(ImageEstadoColumn)
                .GridView.Behaviors.Filtering.ColumnSettings.Add(ImageEstadoColumn)
                fechaSolicitud = .Columns("FEC_SOLICITUD")
                fechaLimiteCumplimentacion = .Columns("FEC_LIM_CUMPLIM")
            End With

            fechaSolicitud.DataFormatString = "{0:" & FSPMUser.DateFormat.ShortDatePattern & "}"
            fechaLimiteCumplimentacion.DataFormatString = "{0:" & FSPMUser.DateFormat.ShortDatePattern & "}"

            imgPDF.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/PDF.png"
            imgExcel.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/Excel.png"
        End Sub
#End Region
#Region "Grid"
        ''' <summary>
        ''' Carga la grid con los certificados
        ''' </summary>
        ''' <remarks>Llamada desde: page_load   cmdCargar_Click; Tiempo máximo:0,3</remarks>
        Private Sub CargarGridCertificados()
            With wdgHDCertifs
                .DataSource = oCertificados
                .DataMember = "CERTIFICADOS"
                .DataBind()
                If bCertifPdteEnviar Then
                    divCertifPdteEnviar.Visible = True
                    divSeleccionar.Visible = False
                Else
                    divCertifPdteEnviar.Visible = False
                    divSeleccionar.Visible = True
                End If
            End With
        End Sub
        ''' <summary>
        ''' Establecer el aspecto de una fila del grid 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">evento de sistema</param>       
        ''' <remarks>Llamada desde: sistema; Tiempo máximo:0 </remarks>
        Private Sub wdgHDCertifs_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdgHDCertifs.InitializeRow
            Dim cellEstadoIndex, cellImageEstadoIndex, cellLimCumplimIndex As Integer
            Dim items As Infragistics.Web.UI.GridControls.GridRecordItemCollection = e.Row.Items

			If DBNullToBoolean(e.Row.DataItem.Item.Row.Item("OBLIGATORIO")) Then
                With items(wdgHDCertifs.GridView.Columns.FromKey("OBLIGATORIO").Index)
                    .Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "Images/Asterisco.png'/>" '"No"
                    .CssClass = "itemCenterAligment"
                End With
            Else
                items(wdgHDCertifs.GridView.Columns.FromKey("OBLIGATORIO").Index).Text = "" ' "Sí"
            End If

            items(wdgHDCertifs.GridView.Columns.FromKey("ID_ENCRYPTED").Index).Value = Server.UrlEncode(Encrypter.Encrypt(DBNullToInt(e.Row.DataItem.Item.Row.Item("ID")), , True, Encrypter.TipoDeUsuario.Administrador))
            items(wdgHDCertifs.GridView.Columns.FromKey("INSTANCIA_ENCRYPTED").Index).Value = Server.UrlEncode(Encrypter.Encrypt(DBNullToInt(e.Row.DataItem.Item.Row.Item("INSTANCIA")), , True, Encrypter.TipoDeUsuario.Administrador))

            cellEstadoIndex = wdgHDCertifs.GridView.Columns.FromKey("ESTADO").Index
            cellImageEstadoIndex = wdgHDCertifs.GridView.Columns.FromKey("IMAGE_ESTADO").Index
			cellLimCumplimIndex = wdgHDCertifs.GridView.Columns.FromKey("FEC_LIM_CUMPLIM").Index
			If DBNullToSomething(e.Row.DataItem.Item.Row.Item("EN_PROCESO")) = "0" Then
				If e.Row.DataItem.Item.Row.Item("PDTE_ENVIAR") Then
					items(cellImageEstadoIndex).Text = "<img src='../_common/images/Ico_PendienteEnviar.png' style='text-align:center;'/>"
					items(cellEstadoIndex).CssClass = "certificadoPendienteEnviar"
					bCertifPdteEnviar = True
				Else
					Select Case e.Row.DataItem.Item.Row.Item("ID_TEXTO_ESTADO")
						Case 64
							items(cellImageEstadoIndex).Text = "<img src='../_common/images/Ico_Pendiente.gif' style='text-align:center;'/>"
							items(cellEstadoIndex).CssClass = "certificadoPendiente"
						Case 65
							If Not IsDBNull(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM")) Then
								items(cellLimCumplimIndex).Text = ""
							End If
							items(cellImageEstadoIndex).Text = "<img src='../_common/images/Ico_Vigente.gif' style='text-align:center;'/>"
							items(cellEstadoIndex).CssClass = "certificadoVigente"
						Case 66, 67
							items(cellImageEstadoIndex).Text = "<img src='../_common/images/Ico_Expirado.gif' style='text-align:center;'/>"
							items(cellEstadoIndex).CssClass = "certificadoExpirado"

							items(cellLimCumplimIndex).CssClass = "Mano"
							If Not IsDBNull(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM")) Then
								If CType(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM"), Date) < DateTime.Today Then
									items(cellLimCumplimIndex).CssClass = "ManoRed"
								End If
							End If
						Case 80
							items(cellImageEstadoIndex).Text = "<img src='../_common/images/Ico_PendienteValidar.png' style='text-align:center;'/>"
							items(cellEstadoIndex).CssClass = "certificadoPendienteValidar"

							items(cellLimCumplimIndex).CssClass = "Mano"
							If Not IsDBNull(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM")) Then
								If CType(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM"), Date) < DateTime.Today Then
									items(cellLimCumplimIndex).CssClass = "ManoRed"
								End If
							End If
						Case Else
							items(cellImageEstadoIndex).Text = "<img src='../_common/images/Ico_NoValidado.png' style='text-align:center;'/>"
							items(cellEstadoIndex).CssClass = "certificadoNoValidado"

							items(cellLimCumplimIndex).CssClass = "Mano"
							If Not IsDBNull(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM")) Then
								If CType(e.Row.DataItem.Item.Row.Item("FEC_LIM_CUMPLIM"), Date) < DateTime.Today Then
									items(cellLimCumplimIndex).CssClass = "ManoRed"
								End If
							End If
					End Select
				End If
			Else
				items(cellEstadoIndex).CssClass = "certificadoEnProceso"
			End If
		End Sub
#End Region
#Region "Botonera"
        ''' <summary>
        ''' Exporta a Excel
        ''' </summary>
        ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
        Private Sub ExportarExcel()
            Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
            fileName = fileName.Replace("+", "%20")

            With wdgHDCertifs
                .Columns.FromKey("IMAGE_ESTADO").Hidden = True
                .GridView.Columns.FromKey("IMAGE_ESTADO").Hidden = True
            End With
            With wdgExcelExporter
                .DownloadName = fileName
                .DataExportMode = DataExportMode.AllDataInDataSource

                .Export(wdgHDCertifs)
            End With
            With wdgHDCertifs
                .Columns.FromKey("IMAGE_ESTADO").Hidden = False
                .GridView.Columns.FromKey("IMAGE_ESTADO").Hidden = False
            End With
        End Sub
        Private Sub wdgExcelExporter_GridRecordItemExported(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GridRecordItemExportedEventArgs) Handles wdgExcelExporter.GridRecordItemExported
            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.VisorCertificados
            Select Case e.GridCell.Column.Key
                Case "OBLIGATORIO"
                    e.WorksheetCell.Value = IIf(e.GridCell.Value, Textos(24), Textos(25))
                Case Else
                    If e.GridCell.Column.Type = System.Type.GetType("System.DateTime") Then
                        e.WorksheetCell.CellFormat.FormatString = FSPMUser.DateFormat.ShortDatePattern
                    End If
            End Select
        End Sub
        Private Sub wdgExcelExporter_Exported(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.ExcelExportedEventArgs) Handles wdgExcelExporter.Exported
            e.Worksheet.Name = Regex.Replace(Textos(0), "[^\w\.@]", "_")
        End Sub
        ''' <summary>
        ''' Exporta a PDF
        ''' </summary>
        ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
        Private Sub ExportarPDF()
            Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
            fileName = fileName.Replace("+", "%20")
            wdgPDFExporter.DownloadName = fileName

            With wdgHDCertifs
                .Columns.FromKey("IMAGE_ESTADO").Hidden = True
                .GridView.Columns.FromKey("IMAGE_ESTADO").Hidden = True
            End With
            With wdgPDFExporter
                .EnableStylesExport = True

                .DataExportMode = DataExportMode.AllDataInDataSource
                .TargetPaperOrientation = PageOrientation.Landscape

                .Margins = PageMargins.GetPageMargins("Narrow")
                .TargetPaperSize = PageSizes.GetPageSize("A4")
                .Export(wdgHDCertifs)
            End With
            With wdgHDCertifs
                .Columns.FromKey("IMAGE_ESTADO").Hidden = False
                .GridView.Columns.FromKey("IMAGE_ESTADO").Hidden = False
            End With
        End Sub
        Private Sub wdgPDFExporter_GridRecordItemExporting(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.DocumentGridRecordItemExportingEventArgs) Handles wdgPDFExporter.GridRecordItemExporting
            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.VisorCertificados
            Select Case e.GridCell.Column.Key
                Case "OBLIGATORIO"
                    e.ExportValue = IIf(e.GridCell.Value, Textos(24), Textos(25))
                Case Else
                    Select Case e.GridCell.Column.Type
                        Case System.Type.GetType("System.DateTime")
                            If IsDBNull(e.ExportValue) Then
                                e.ExportValue = ""
                            Else
                                e.ExportValue = FormatDate(e.ExportValue, FSPMUser.DateFormat)
                            End If
                        Case Else
                    End Select
            End Select
        End Sub
#End Region
#Region "Page Methods"
        <System.Web.Services.WebMethod(True), _
        System.Web.Script.Services.ScriptMethod()> _
        Public Shared Function ComprobarEnProceso(ByVal lCiaComp As Integer, ByVal Instancia As Integer) As Integer
            Dim FSPMServer As Fullstep.PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
            Dim oInstancia As Fullstep.PMPortalServer.Instancia

            oInstancia = FSPMServer.Get_Instancia
            oInstancia.ID = CLng(Instancia)
            Return oInstancia.ComprobarEnProceso(lCiaComp)
        End Function
#End Region
    End Class
End Namespace