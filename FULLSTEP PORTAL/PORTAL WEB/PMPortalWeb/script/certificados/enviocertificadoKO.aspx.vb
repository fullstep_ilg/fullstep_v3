Namespace Fullstep.PMPortalWeb

    Partial Class enviocertificadoKO
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' <summary>
        ''' Pantalla de informaci�n de envio de certificado incorrectamente
        ''' </summary>
        ''' <param name="sender"> Propio del evento </param>
        ''' <param name="e"> Propio del evento </param>        
        ''' <remarks> Llamada desde: _common\guardarinstancia.aspx; Tiempo m�ximo = 0.1</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim oDict As Fullstep.PMPortalServer.Dictionary
            Dim oTextos As DataTable
            Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User
            Dim lCiaComp As Long = IdCiaComp
            oUser = Session("FS_Portal_User")

            Dim sIdi As String = Idioma
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If


            oDict = FSPMServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.EnvioCertificadoKO, sIdi)
            oTextos = oDict.Data.Tables(0)

            If Request("error") = 4 Then

            End If

            Dim oCertificado As Fullstep.PMPortalServer.Certificado


            Try
                oCertificado = FSPMServer.Get_Certificado
                oCertificado.Id = Request("Certificado")
                oCertificado.Prove = oUser.CodProve
                oCertificado.Load(lCiaComp, sIdi)
            Catch ex As Exception

            End Try

            Try
                If Page.IsPostBack Then Exit Sub

                Me.lblNombreCerticado.Text = oCertificado.TipoDen
            Catch ex As Exception

            End Try

            Me.lblId.Text = oTextos.Rows(0).Item(1)
            Me.lblSolicitante.Text = oTextos.Rows(1).Item(1)
            Me.lblFechaLimite.Text = oTextos.Rows(2).Item(1)

            Me.mensajeKo.Text = oTextos.Rows(3).Item(1)
            Try
                'Me.txtId.Text = IIf(oCertificado.Id = 0, "", oCertificado.Id)
                Me.txtId.Text = IIf(oCertificado.Instancia = 0, "", oCertificado.Instancia)
                Me.txtSolicitante.Text = oCertificado.NomSolicitante
                If Trim(Me.txtSolicitante.Text) = "" Then
                    Me.lblSolicitante.Visible = False
                End If

                If IsTime(oCertificado.FecDespub) Then
                    Me.txtFechaLimite.Visible = False
                    Me.lblFechaLimite.Visible = False
                Else
                    Me.txtFechaLimite.Text = modUtilidades.FormatDate(oCertificado.FecDespub, oUser.DateFormat)
                End If
            Catch ex As Exception

            End Try

            Me.lblMotivo.Text = oTextos.Rows(4).Item(1)
            Select Case Val(Request("error"))
                Case 1, 3
                    Me.txtMotivo.Text = oTextos.Rows(6).Item(1)
                Case 2
                    Me.txtMotivo.Text = oTextos.Rows(5).Item(1)
                Case 4
                    Me.txtMotivo.Text = oTextos.Rows(7).Item(1)
            End Select

            oCertificado = Nothing
            oTextos = Nothing
            oDict = Nothing

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)
            End If
        End Sub

    End Class
End Namespace
