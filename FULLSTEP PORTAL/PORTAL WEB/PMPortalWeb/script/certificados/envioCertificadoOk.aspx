<%@ Page Language="vb" AutoEventWireup="false" Codebehind="envioCertificadoOk.aspx.vb" Inherits="Fullstep.PMPortalWeb.envioCertificadoOk" %>
<%@ Register TagPrefix="uc1" TagName="menu" Src="../_common/menu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
		<title>devolucionOK</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../../common/estilo.asp" type="text/css" rel="stylesheet">
		<script src="../../../common/formatos.js"></script>
		<script src="../../../common/menu.asp"></script>	
        <script type="text/javascript">
            /*''' <summary>
            ''' Iniciar la pagina.
            ''' </summary>     
            ''' <remarks>Llamada desde: page.onload; Tiempo m�ximo:0</remarks>*/
            function Init() {
                if (accesoExterno == 0) { document.getElementById('tablemenu').style.display = 'block'; }
            }
        </script>        	
	</head>
	<body onload="Init()">
		<script>dibujaMenu(2)</script>
		<form id="Form1" method="post" runat="server">
			<table class="bordeadoAzul" id="tblCabecera" cellSpacing="2" cellPadding="1" width="100%" border="0">
				<tr>
					<td width="100%" colSpan="3"><asp:label id="lblNombreCerticado" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></td>
				</tr>
				<tr>
					<td colSpan="3">&nbsp;</td>
				</tr>
				<tr>
					<td><asp:label id="lblId" runat="server" CssClass="captionBlue" Width="100%"></asp:label></td>
					<td><asp:label id="lblSolicitante" runat="server" CssClass="captionBlue" Width="100%"></asp:label></td>
					<td><asp:label id="lblFechaLimite" runat="server" CssClass="captionBlue" Width="100%"></asp:label></td>
				</tr>
				<tr>
					<td><asp:label id="txtId" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></TD>
					<td><asp:label id="txtSolicitante" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></td>
					<td><asp:label id="txtFechaLimite" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></td>
				</tr>
				<tr>
					<td colSpan="3">&nbsp;</td>
				</tr>
				<tr>
					<td width="100%" colSpan="3">
                        <asp:Image runat="server" ID="imgAlerta" ImageUrl="~/images/Img_Ico_Alerta.gif" style="margin-right:2em;" />
                        <asp:label id="mensajeOk" runat="server" CssClass="fntLogin"></asp:label>
                        <asp:label id="mensajeOk2" runat="server" CssClass="certificadoPendienteEnviar"></asp:label>
                    </td>
				</tr>
			</table>
		</form>
	</body>
</html>
