Namespace Fullstep.PMPortalWeb
    Partial Class certificado
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        End Sub
        Protected WithEvents tblCabecera As System.Web.UI.HtmlControls.HtmlTable
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object
#End Region
        Private Const AltaScriptKey As String = "AltaIncludeScript"
        Private Const AltaFileName As String = "../_common/js/jsAlta.js?v="
        Private Const IncludeScriptFormat As String = ControlChars.CrLf &
    "<script type=""{0}"" src=""{1}""></script>"
        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf &
    "<script language=""{0}"">{1}</script>"
        Private m_sTextoAnulacion As String
        Private m_sMsgboxAccion(3) As String
#Region "Cache y Propiedades"
        ''' <summary>
        ''' Devolver el Id de la compania
        ''' </summary>
        ''' <returns>Id de la compania</returns>
        ''' <remarks>Llamada desde: property oSolicitud property oInstancia property oCertificado; Tiempo m�ximo:0</remarks>
        Protected ReadOnly Property lCiaComp As Long
            Get
                lCiaComp = IdCiaComp
            End Get
        End Property
        Private _oSolicitud As Fullstep.PMPortalServer.Solicitud
        ''' <summary>
        ''' Crear el objeto Solicitud para la pantalla. Es Request("TipoSolicit").
        ''' </summary>
        ''' <returns>Objeto Solicitud</returns>
        ''' <remarks>Llamada desde: PageLoad_Por_Solicitud PageInit_Por_Solicitud; Tiempo m�ximo:0,2</remarks>
        Protected ReadOnly Property oSolicitud() As Fullstep.PMPortalServer.Solicitud
            Get
                If _oSolicitud Is Nothing Then
                    If Me.IsPostBack Then
                        _oSolicitud = CType(Cache("oSolicitud_" & FSPMUser.IdCia & "_" & FSPMUser.Cod), Fullstep.PMPortalServer.Solicitud)
                        If _oSolicitud Is Nothing Then
                            _oSolicitud = FSPMServer.Get_Solicitud
                            _oSolicitud.ID = oCertificado.IDSolicitud

                            _oSolicitud.Load(lCiaComp, Idioma)
                            _oSolicitud.Formulario.Load(lCiaComp, Idioma, _oSolicitud.ID)

                            Me.InsertarEnCache("oSolicitud_" & FSPMUser.IdCia & "_" & FSPMUser.Cod, _oSolicitud)
                        End If
                    Else
                        _oSolicitud = FSPMServer.Get_Solicitud
                        _oSolicitud.ID = oCertificado.IDSolicitud

                        _oSolicitud.Load(lCiaComp, Idioma)
                        _oSolicitud.Formulario.Load(lCiaComp, Idioma, _oSolicitud.ID)

                        Me.InsertarEnCache("oSolicitud_" & FSPMUser.IdCia & "_" & FSPMUser.Cod, _oSolicitud)
                    End If
                End If
                Return _oSolicitud
            End Get
        End Property
        Private _oInstancia As Fullstep.PMPortalServer.Instancia
        ''' <summary>
        ''' Crear el objeto Instancia para la pantalla. 
        ''' Es Request("Instancia") (PageLoad_Por_Certificado PageInit_Por_Certificado) 
        ''' o el indicado por la solicitud (PageLoad_Por_Solicitud PageInit_Por_Solicitud).
        ''' </summary>
        ''' <returns>Objeto Instancia</returns>
        ''' <remarks>Llamada desde: PageLoad_Por_Certificado PageInit_Por_Certificado PageLoad_Por_Solicitud PageInit_Por_Solicitud; Tiempo m�ximo:0,2</remarks>
        Protected ReadOnly Property oInstancia() As Fullstep.PMPortalServer.Instancia
            Get
                If _oInstancia Is Nothing Then
                    If Me.IsPostBack Then
                        _oInstancia = CType(Cache("oInstancia_" & FSPMUser.IdCia & "_" & FSPMUser.Cod), Fullstep.PMPortalServer.Instancia)
                        If _oInstancia Is Nothing Then
                            _oInstancia = FSPMServer.Get_Instancia

                            If Request("Autom") = 0 Then
                                If Request("Instancia") = Nothing Or Request("Instancia") = "null" Or Request("Instancia") = "0" Then
                                    _oInstancia.ID = oCertificado.Instancia
                                Else
                                    Dim IdInstancia As Long
                                    Try
                                        IdInstancia = CLng(Encrypter.Encrypt(Request("Instancia"), , False, Encrypter.TipoDeUsuario.Administrador))
                                        _oInstancia.ID = IdInstancia
                                    Catch ex As Exception
                                        _oInstancia.ID = oCertificado.Instancia
                                    End Try
                                End If

                                _oInstancia.Load(lCiaComp, FSPMUser.Idioma.ToString)
                                _oInstancia.Version = oCertificado.Version
                            Else
                                _oInstancia.ID = oSolicitud.ID

                                _oInstancia.Load(lCiaComp, FSPMUser.Idioma.ToString)
                            End If

                            Dim bEnProceso As Boolean
                            Dim bRecuperarEnviada As Boolean

                            If (Request("version") <> Nothing) AndAlso (Request("version") <> 0) Then
                                bRecuperarEnviada = True
                            Else
                                bRecuperarEnviada = False

                                If _oInstancia.ComprobarEnProceso(lCiaComp) Then
                                    bEnProceso = True
                                End If
                            End If

                            _oInstancia.EnProceso = bEnProceso

                            If Not bEnProceso Then
                                _oInstancia.CargarCamposInstancia(lCiaComp, FSPMUser.IdCia, Idioma, , FSPMUser.CodProveGS, bRecuperarEnviada)
                            End If

                            Me.InsertarEnCache("oInstancia_" & FSPMUser.IdCia & "_" & FSPMUser.Cod, _oInstancia)
                        End If
                    Else
                        _oInstancia = FSPMServer.Get_Instancia

                        If Request("Autom") = 0 Then
                            If Request("Instancia") = Nothing Or Request("Instancia") = "null" Or Request("Instancia") = "0" Then
                                _oInstancia.ID = oCertificado.Instancia
                            Else
                                Dim IdInstancia As Long
                                Try
                                    IdInstancia = CLng(Encrypter.Encrypt(Request("Instancia"), , False, Encrypter.TipoDeUsuario.Administrador))
                                    _oInstancia.ID = IdInstancia
                                Catch ex As Exception
                                    _oInstancia.ID = oCertificado.Instancia
                                End Try
                            End If

                            _oInstancia.Load(lCiaComp, FSPMUser.Idioma.ToString)
                            _oInstancia.Version = oCertificado.Version
                        Else
                            _oInstancia.ID = oSolicitud.ID

                            _oInstancia.Load(lCiaComp, FSPMUser.Idioma.ToString)
                        End If

                        Dim bEnProceso As Boolean
                        Dim bRecuperarEnviada As Boolean

                        If (Request("version") <> Nothing) AndAlso (Request("version") <> 0) Then
                            bRecuperarEnviada = True
                        Else
                            bRecuperarEnviada = False

                            If _oInstancia.ComprobarEnProceso(lCiaComp) Then
                                bEnProceso = True
                            End If
                        End If

                        _oInstancia.EnProceso = bEnProceso

                        If Not bEnProceso Then
                            _oInstancia.CargarCamposInstancia(lCiaComp, FSPMUser.IdCia, Idioma, , FSPMUser.CodProveGS, bRecuperarEnviada)
                        End If

                        Me.InsertarEnCache("oInstancia_" & FSPMUser.IdCia & "_" & FSPMUser.Cod, _oInstancia)
                    End If
                End If
                Return _oInstancia
            End Get
        End Property
        Private _oCertificado As Fullstep.PMPortalServer.Certificado
        ''' <summary>
        ''' Crear el objeto Instancia para la pantalla. Es Request("Certificado").
        ''' </summary>
        ''' <returns>Objeto Certificado</returns>
        ''' <remarks>Llamada desde: PageLoad_Por_Certificado PageInit_Por_Certificado; Tiempo m�ximo:0,2</remarks>
        Protected ReadOnly Property oCertificado() As Fullstep.PMPortalServer.Certificado
            Get
                If _oCertificado Is Nothing Then
                    Dim IdCertificado As Long
                    Try
                        IdCertificado = CLng(Encrypter.Encrypt(Request("Certificado"), , False, Encrypter.TipoDeUsuario.Administrador))
                    Catch ex As Exception
                        IdCertificado = -1
                    End Try

                    If Me.IsPostBack Then
                        _oCertificado = CType(Cache("oCertificados_" & FSPMUser.IdCia & "_" & FSPMUser.Cod), Fullstep.PMPortalServer.Certificado)
                        If _oCertificado Is Nothing Then
                            _oCertificado = FSPMServer.Get_Certificado
                            _oCertificado.Id = IdCertificado
                            _oCertificado.Prove = FSPMUser.CodProve

                            _oCertificado.Load(lCiaComp, FSPMUser.Idioma.ToString)

                            If (Request("version") <> Nothing) AndAlso (Request("version") <> 0) Then _oCertificado.Version = Request("version")

                            Me.InsertarEnCache("oCertificados_" & FSPMUser.IdCia & "_" & FSPMUser.Cod, _oCertificado)
                        End If
                    Else
                        _oCertificado = FSPMServer.Get_Certificado
                        _oCertificado.Id = IdCertificado
                        _oCertificado.Prove = FSPMUser.CodProve

                        _oCertificado.Load(lCiaComp, FSPMUser.Idioma.ToString)

                        If (Request("version") <> Nothing) AndAlso (Request("version") <> 0) Then _oCertificado.Version = Request("version")

                        Me.InsertarEnCache("oCertificados_" & FSPMUser.IdCia & "_" & FSPMUser.Cod, _oCertificado)
                    End If
                End If
                Return _oCertificado
            End Get
        End Property
#End Region
#Region "Page Init"
        ''' <summary>
        ''' Precarga de pagina, crea los controles, es decir, tabs, desglose, campos -> entrys
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>      
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,3</remarks>
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            InitializeComponent()
        End Sub
#End Region
#Region "Inicio"
        ''' <summary>
        ''' carga de pagina
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>      
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,3</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Response.Expires = -1

            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Certificado

            Certificado.Value = oCertificado.Id
            Instancia.Value = oCertificado.Instancia
            Contacto.Value = Request("Contacto")

            CargarCampos()

            If Page.IsPostBack Then Exit Sub
            HttpContext.Current.Cache.Remove("oCertificado_" & FSPMUser.Cod)

            cmdGuardar.Value = Textos(3)

            Dim sClientTextVars As String
            sClientTextVars = ""
            sClientTextVars += "vdecimalfmt='" + FSPMUser.DecimalFmt + "';"
            sClientTextVars += "vthousanfmt='" + FSPMUser.ThousanFmt + "';"
            sClientTextVars += "vprecisionfmt='" + FSPMUser.PrecisionFmt + "';"
            sClientTextVars += "vDatefmt='" + FSPMUser.DateFormat.ShortDatePattern + "';"
            sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

            If oCertificado.Version > 0 Or oCertificado.Portal Or Not oCertificado.FecCumplimentacion = Date.MinValue Then
                Cargar_Certificado()
            Else
                Cargar_Solicitud()
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)
            End If
        End Sub
        Private Sub CargarCampos()
            'Si no tiene fecha de cumplimentacion y tampoco tiene versi�n de tipo 3 guardada en portal
            'cargamos los campos desde la solicitud, sino, lo cargamos de la versi�n guardada.
            'oCertificado.Portal es un boolen que dice si tiene versi�n de tipo 3 o no.
            'Tambien cargaremos los campos desde el certificado si ya existe versi�n
            If oCertificado.Version > 0 Or oCertificado.Portal Or Not oCertificado.FecCumplimentacion = Date.MinValue Then
                CargarCampos_Por_Certificado()
            Else
                CargarCampos_Por_Solicitud()
            End If
        End Sub
        ''' <summary>
        ''' Mostrar el detalle del certificado
        ''' Parte creaci�n de campos dinamicos  
        ''' </summary>
        ''' <remarks>Llamada desde: Page_load pagina
        ''' ; Tiempo m�ximo: 0,5</remarks>
        Private Sub CargarCampos_Por_Solicitud()
            Dim oGrupo As Fullstep.PMPortalServer.Grupo
            Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
            Dim oucCampos As Fullstep.PMPortalWeb.campos
            Dim oucDesglose As Fullstep.PMPortalWeb.desgloseControl
            Dim oRow As DataRow = Nothing
            Dim lIndex As Integer = 0
            Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden

            'Rellena el tab :
            uwtGrupos.Tabs.Clear()
            AplicarEstilosTab(uwtGrupos)

            If Not oSolicitud.Formulario.Grupos Is Nothing Then
                For Each oGrupo In oSolicitud.Formulario.Grupos.Grupos
                    oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

                    oInputHidden.ID = "txtPre_" + lIndex.ToString
                    oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                    Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                    Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                    oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(Idioma), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                    oTabItem.Key = lIndex.ToString
                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Hidden
                    uwtGrupos.Tabs.Add(oTabItem)
                    lIndex += 1

                    If oGrupo.DSCampos.Tables.Count > 0 Then
                        If oGrupo.NumCampos <= 1 Then
                            For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                                If oRow.Item("VISIBLE") = 1 Then
                                    Exit For
                                End If
                            Next
                            If oRow.Item("VISIBLE") = 0 Then
                                uwtGrupos.Tabs.Remove(oTabItem)
                            ElseIf DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Desglose Or oRow.Item("SUBTIPO") = Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoDesglose Then
                                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Hidden
                                oTabItem.ContentPane.UserControlUrl = "../_common/desglose.ascx"
                                oucDesglose = oTabItem.ContentPane.UserControl
                                oucDesglose.ID = oGrupo.Id.ToString
                                oucDesglose.Campo = oRow.Item("ID")
                                oucDesglose.TieneIdCampo = False
                                oucDesglose.Ayuda = DBNullToSomething(oRow.Item("AYUDA_" & Idioma))
                                oucDesglose.TabContainer = uwtGrupos.ClientID
                                oucDesglose.SoloLectura = ((oRow.Item("ESCRITURA") = 0) Or (oCertificado.PlazoLimCumplimSuperado And Not oCertificado.FueraPlazo))
                                oucDesglose.Solicitud = oSolicitud.ID
                                oucDesglose.EsQA = True
                                oucDesglose.InstanciaMoneda = oInstancia.Moneda
                                oucDesglose.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                                oInputHidden.Value = oucDesglose.ClientID
                            Else
                                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                                oTabItem.ContentPane.UserControlUrl = "../_common/campos.ascx"
                                oucCampos = oTabItem.ContentPane.UserControl
                                oucCampos.ID = oGrupo.Id.ToString
                                oucCampos.dsCampos = oGrupo.DSCampos
                                oucCampos.IdGrupo = oGrupo.Id
                                oucCampos.Idi = Idioma
                                oucCampos.TabContainer = uwtGrupos.ClientID
                                oucCampos.Solicitud = oSolicitud.ID
                                oucCampos.SoloLectura = (oCertificado.PlazoLimCumplimSuperado And Not oCertificado.FueraPlazo)
                                oucCampos.Instancia = -1
                                oucCampos.InstanciaMoneda = oInstancia.Moneda
                                oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                                oucCampos.Formulario = oInstancia.Solicitud.Formulario
                                oucCampos.ObjInstancia = oInstancia
                                oInputHidden.Value = oucCampos.ClientID
                            End If
                        Else
                            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                            oTabItem.ContentPane.UserControlUrl = "../_common/campos.ascx"
                            oucCampos = oTabItem.ContentPane.UserControl
                            oucCampos.ID = oGrupo.Id.ToString
                            oucCampos.dsCampos = oGrupo.DSCampos
                            oucCampos.IdGrupo = oGrupo.Id
                            oucCampos.Idi = Idioma
                            oucCampos.TabContainer = uwtGrupos.ClientID
                            oucCampos.Solicitud = oSolicitud.ID
                            oucCampos.SoloLectura = (oCertificado.PlazoLimCumplimSuperado And Not oCertificado.FueraPlazo)
                            oucCampos.Instancia = 0
                            oucCampos.InstanciaMoneda = oInstancia.Moneda
                            oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                            oucCampos.Formulario = oInstancia.Solicitud.Formulario
                            oucCampos.ObjInstancia = oInstancia
                            oInputHidden.Value = oucCampos.ClientID
                            Dim tabVisible As Boolean = False
                            For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                                If oRow.Item("VISIBLE") = 1 Then
                                    tabVisible = True
                                    Exit For
                                End If
                            Next
                            If tabVisible Then
                                oTabItem.Visible = True
                            Else
                                uwtGrupos.Tabs.Remove(oTabItem)
                            End If
                        End If
                    End If
                    Me.FindControl("frmDetalle").Controls.Add(oInputHidden)
                Next
                If uwtGrupos.Tabs.Count > 0 Then Me.FindControl("frmDetalle").Controls.Add(Fullstep.PMPortalWeb.CommonInstancia.InsertarCalendario(Idioma))
            End If
        End Sub
        ''' <summary>
        ''' Carga el detalle de un certificado q ha sido solicitado previamente.
        ''' Parte creaci�n de campos dinamicos 
        ''' </summary>        
        ''' <remarks>Llamada desde: Page_Load; Tiempo m�ximo: 0,1</remarks>
        Private Sub CargarCampos_Por_Certificado()
            Dim oGrupo As Fullstep.PMPortalServer.Grupo
            Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
            Dim oucCampos As Fullstep.PMPortalWeb.campos
            Dim oucDesglose As Fullstep.PMPortalWeb.desgloseControl
            Dim oRow As DataRow = Nothing
            Dim lIndex As Integer = 0
            Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden
            Dim bSoloLectura As Boolean
            Dim bRecuperarEnviada As Boolean

            If Not oInstancia.EnProceso Then
                Dim oAcciones As DataTable = Nothing
                If (Request("version") <> Nothing) AndAlso (Request("version") <> 0) Then
                    bRecuperarEnviada = True
                Else
                    bRecuperarEnviada = False
                End If
                If bRecuperarEnviada Or oInstancia.ID <> oCertificado.Instancia Then
                    'Esto ocurre cuando estamos cargando un certificado no activo del tipo de certificado actual
                    bSoloLectura = True
                End If
                'Rellena el tab :
                uwtGrupos.Tabs.Clear()
                AplicarEstilosTab(uwtGrupos)

                If Not oInstancia.Grupos.Grupos Is Nothing Then
                    For Each oGrupo In oInstancia.Grupos.Grupos
                        oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

                        oInputHidden.ID = "txtPre_" + lIndex.ToString
                        oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                        Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                        Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                        oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(Idioma), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                        oTabItem.Key = lIndex.ToString
                        oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Hidden
                        uwtGrupos.Tabs.Add(oTabItem)
                        lIndex += 1

                        If oGrupo.DSCampos.Tables.Count > 0 Then
                            If oGrupo.NumCampos <= 1 Then
                                For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                                    If oRow.Item("VISIBLE") = 1 Then
                                        Exit For
                                    End If
                                Next
                                If oRow.Item("VISIBLE") = 0 Then
                                    uwtGrupos.Tabs.Remove(oTabItem)
                                ElseIf DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Desglose Or oRow.Item("SUBTIPO") = Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoDesglose Then

                                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Hidden
                                    oTabItem.ContentPane.UserControlUrl = "../_common/desglose.ascx"
                                    oucDesglose = oTabItem.ContentPane.UserControl
                                    oucDesglose.ID = oGrupo.Id.ToString
                                    oucDesglose.Campo = oRow.Item("ID_CAMPO")
                                    oucDesglose.TabContainer = uwtGrupos.ClientID
                                    oucDesglose.Ayuda = DBNullToSomething(oRow.Item("AYUDA_" & Idioma))
                                    oucDesglose.TieneIdCampo = True
                                    oucDesglose.Instancia = oInstancia.ID
                                    oucDesglose.Version = oInstancia.Version
                                    oucDesglose.SoloLectura = bSoloLectura Or (oRow.Item("ESCRITURA") = 0)
                                    oInputHidden.Value = oucDesglose.ClientID
                                    oucDesglose.Acciones = oAcciones
                                    oucDesglose.EsQA = True
                                    oucDesglose.InstanciaMoneda = oInstancia.Moneda
                                    oucDesglose.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                                Else
                                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                                    oTabItem.ContentPane.UserControlUrl = "../_common/campos.ascx"
                                    oucCampos = oTabItem.ContentPane.UserControl
                                    oucCampos.Instancia = oInstancia.ID
                                    oucCampos.ID = oGrupo.Id.ToString
                                    oucCampos.dsCampos = oGrupo.DSCampos
                                    oucCampos.IdGrupo = oGrupo.Id
                                    oucCampos.Idi = Idioma
                                    oucCampos.TabContainer = uwtGrupos.ClientID
                                    oucCampos.Version = oInstancia.Version
                                    oucCampos.SoloLectura = bSoloLectura
                                    oucCampos.Acciones = oAcciones
                                    oucCampos.InstanciaMoneda = oInstancia.Moneda
                                    oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                                    oucCampos.Formulario = oInstancia.Solicitud.Formulario
                                    oucCampos.ObjInstancia = oInstancia
                                    oInputHidden.Value = oucCampos.ClientID
                                End If
                            Else 'Aqui ya se sabe q son visbles
                                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                                oTabItem.ContentPane.UserControlUrl = "../_common/campos.ascx"
                                oucCampos = oTabItem.ContentPane.UserControl
                                oucCampos.Instancia = oInstancia.ID
                                oucCampos.ID = oGrupo.Id.ToString
                                oucCampos.dsCampos = oGrupo.DSCampos
                                oucCampos.IdGrupo = oGrupo.Id
                                oucCampos.Idi = Idioma
                                oucCampos.TabContainer = uwtGrupos.ClientID
                                oucCampos.Version = oInstancia.Version
                                oucCampos.SoloLectura = bSoloLectura
                                oucCampos.Acciones = oAcciones
                                oucCampos.InstanciaMoneda = oInstancia.Moneda
                                oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                                oucCampos.Formulario = oInstancia.Solicitud.Formulario
                                oucCampos.ObjInstancia = oInstancia
                                oInputHidden.Value = oucCampos.ClientID
                            End If
                        End If
                        Me.FindControl("frmDetalle").Controls.Add(oInputHidden)
                    Next

                    If uwtGrupos.Tabs.Count > 0 Then Me.FindControl("frmDetalle").Controls.Add(Fullstep.PMPortalWeb.CommonInstancia.InsertarCalendario(Idioma))
                End If
            End If
        End Sub
        ''' <summary>
        ''' Mostrar el detalle del certificado
        ''' La creaci�n de campos dinamicos va en PageInit_Por_Solicitud 
        ''' </summary>
        ''' <remarks>Llamada desde: Page_load pagina
        ''' ; Tiempo m�ximo: 0,5</remarks>
        Private Sub Cargar_Solicitud()
            Solicitud.Value = oSolicitud.ID

            txtSolicitante.Text = DBNullToStr(oCertificado.NomSolicitante)
            cmdCalcular.Visible = False
            cmdCalcular2.Visible = False

            BotonCalcular.Value = 0
            cmdCalcular2.Visible = oSolicitud.Formulario.Grupos.Grupos.OfType(Of PMPortalServer.Grupo).Where(Function(x) x.DSCampos.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) y("TIPO") = TipoCampoPredefinido.Calculado).Any).Any()
            If cmdCalcular2.Visible Then BotonCalcular.Value = 1

            Version.Value = ""
            tipoVersion.Value = 1

            lblId.Text = Textos(0)
            lblSolicitante.Text = Textos(1)
            lblFechaLimite.Text = Textos(2)
            lblRespuestas.Text = Textos(5)
            cmdCalcular.Value = Textos(21)
            cmdCalcular2.Value = Textos(21)
            cmdEnviar.Value = Textos(4)
            cmdImpExp.Value = Textos(14)
            lblCamposObligatorios.Text = Textos(23)
            CertificadoEnviado.Value = 0

            'Tema de la cadena espera
            cadenaespera.Value = Textos(12)
            lblProgreso.Text = Textos(13)

            Dim sClientTexts As String
            sClientTexts = ""
            sClientTexts += "var arrTextosML = new Array();"
            sClientTexts += "arrTextosML[0] = '" + JSText(Textos(9)) + "';"
            sClientTexts += "arrTextosML[1] = '" + JSText(Textos(15)) + "';" 'Error Ajax
            sClientTexts += "arrTextosML[2] = '" + JSText(Textos(16)) + "';" 'El certificado est� 
            'siendo tramitado en estos momentos, y no se puede modificar. En breves instantes estar� disponible 
            'para su gesti�n.
            sClientTexts += "arrTextosML[3] = '" + JSText(Textos(17)) + "';" 'No se pueden llevar a 
            'cabo las modificaciones ya que mientras editaba el certificado el peticionario lo ha renovado. Acceda
            'de nuevo al certificado
            sClientTexts += "arrTextosML[4] = '" + JSText(Textos(18)) + "';" 'No se pueden llevar a 
            'cabo las modificaciones ya que mientras editaba el certificado el peticionario lo ha despublicado.
            sClientTexts += "arrTextosML[5] = '" + JSText(Textos(19)) + "';" 'No se pueden llevar a 
            'cabo las modificaciones ya que mientras editaba el certificado el peticionario lo ha modificado y ha 
            'eliminado sus datos guardados sin enviar. Acceda de nuevo al certificado.
            sClientTexts += "arrTextosML[6] = '" + JSText(Textos(20)) + "';" 'No se pueden llevar a 
            'cabo las modificaciones ya que mientras editaba el certificado el peticionario ha efectuado cambios 
            'sobre el. Acceda de nuevo al certificado.
            sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

            If Not Page.ClientScript.IsClientScriptBlockRegistered("varFila") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFila", "<script>var sFila = '" & JSText(Textos(22)) & "' </script>")

            wddVersion.Attributes.Add("style", "visibility: hidden")
            lblRespuestas.Text = lblRespuestas.Text + " 0"
            lblNombreCerticado.Text = oSolicitud.Den(Idioma)
            txtId.Text = oCertificado.Instancia

            If oSolicitud.Formulario.Grupos Is Nothing _
            OrElse oSolicitud.Formulario.Grupos.Grupos Is Nothing _
            OrElse uwtGrupos.Tabs.Count = 0 Then
                uwtGrupos.Visible = False
                cmdCalcular2.Visible = False
                cmdEnviar.Visible = False
                cmdGuardar.Visible = False

                lblCamposObligatorios.Text = Textos(24)
            ElseIf oCertificado.PlazoLimCumplimSuperado And Not oCertificado.FueraPlazo Then
                cmdCalcular2.Visible = False
                cmdEnviar.Visible = False
                cmdGuardar.Visible = False
            End If
            If IsDBNull(oCertificado.FecLimCumplim) OrElse (oCertificado.FecLimCumplim = "#12:00:00 AM#") Then 'Nulo o Nothing
                txtFechaLimite.Text = ""
            Else
                txtFechaLimite.Text = modUtilidades.FormatDate(oCertificado.FecLimCumplim, FSPMUser.DateFormat)
            End If

            cmdImpExp.Visible = False

            Dim includeScript As String
            Dim ilGMN1 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
            Dim ilGMN2 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
            Dim ilGMN3 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
            Dim ilGMN4 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN4
            Dim sScript As String

            sScript = ""
            sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
            sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
            sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
            sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)

            If Not Page.ClientScript.IsClientScriptBlockRegistered(AltaScriptKey) Then
                includeScript = String.Format(IncludeScriptFormat, "text/javascript", AltaFileName & ConfigurationManager.AppSettings("versionJs"))
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), AltaScriptKey, includeScript)
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
            End If

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutanormal", "<script>var rutanormal = '" & ConfigurationManager.AppSettings("ruta") & "' </script>")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")

        End Sub
        ''' <summary>
        ''' Carga el detalle de un certificado q ha sido solicitado previamente. 
        ''' La creaci�n de campos dinamicos va en PageInit_Por_Certificado
        ''' </summary>        
        ''' <remarks>Llamada desde: Page_Load; Tiempo m�ximo: 0,1</remarks>
        Private Sub Cargar_Certificado()
            Dim bSoloLectura As Boolean = False
            Dim bRecuperarEnviada As Boolean = False
            Dim iVersion As Long = 0

            If (Request("version") <> Nothing) AndAlso (Request("version") <> 0) Then
                bRecuperarEnviada = True
                iVersion = Request("Version")
            Else
                bRecuperarEnviada = False
            End If

            Version.Value = oCertificado.Version
            diferenciaUTCServidor.Value = oInstancia.DiferenciaUTCServidor

            cmdCalcular.Visible = False
            cmdCalcular2.Visible = False
            BotonCalcular.Value = 0
            cmdCalcular.Visible = oInstancia.Grupos.Grupos.OfType(Of PMPortalServer.Grupo).Where(Function(x) x.DSCampos.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) y("TIPO") = TipoCampoPredefinido.Calculado).Any).Any()
            If cmdCalcular.Visible Then BotonCalcular.Value = 1

            'Esto ocurre cuando estamos cargando un certificado no activo del tipo de certificado actual
            If bRecuperarEnviada Or oInstancia.ID <> oCertificado.Instancia Then bSoloLectura = True

            lblId.Text = Textos(0)
            lblSolicitante.Text = Textos(1)
            lblFechaLimite.Text = Textos(2)
            lblRespuestas.Text = Textos(5)
            cmdCalcular.Value = Textos(21)
            cmdCalcular2.Value = Textos(21)
            cmdEnviar.Value = Textos(4)
            cmdImpExp.Value = Textos(14)
            CertificadoEnviado.Value = IIf(bRecuperarEnviada = True, 1, 0)
            lblCamposObligatorios.Text = Textos(23)

            'Tema de la cadena espera
            cadenaespera.Value = Textos(12)
            If oInstancia.EnProceso = True Then
                enproceso.Value = "1"
                imgProgreso.Visible = False
                lblProgreso.Text = Textos(14)
            Else
                If oCertificado.Portal And Not bRecuperarEnviada Then
                    lblSinEnviar.Text = Textos(8)
                End If
                lblProgreso.Text = Textos(13)
            End If

            If bSoloLectura Then
                cmdCalcular.Visible = False
                cmdEnviar.Visible = False
                cmdImpExp.Visible = False
            Else
                cmdImpExp.Visible = Not (oCertificado.FecCumplimentacion = Date.MinValue)
            End If

            RellenarComboRespuestas(oInstancia.ID, iVersion, oCertificado.Portal)

            Dim sClientTexts As String
            sClientTexts = ""
            sClientTexts += "var arrTextosML = new Array();"
            sClientTexts += "arrTextosML[0] = '" + JSText(Textos(9)) + "';"
            sClientTexts += "arrTextosML[1] = '" + JSText(Textos(15)) + "';" 'Error Ajax
            sClientTexts += "arrTextosML[2] = '" + JSText(Textos(16)) + "';" 'El certificado est� 
            'siendo tramitado en estos momentos, y no se puede modificar. En breves instantes estar� disponible 
            'para su gesti�n.
            sClientTexts += "arrTextosML[3] = '" + JSText(Textos(17)) + "';" 'No se pueden llevar a 
            'cabo las modificaciones ya que mientras editaba el certificado el peticionario lo ha renovado. Acceda
            'de nuevo al certificado
            sClientTexts += "arrTextosML[4] = '" + JSText(Textos(18)) + "';" 'No se pueden llevar a 
            'cabo las modificaciones ya que mientras editaba el certificado el peticionario lo ha despublicado.
            sClientTexts += "arrTextosML[5] = '" + JSText(Textos(19)) + "';" 'No se pueden llevar a 
            'cabo las modificaciones ya que mientras editaba el certificado el peticionario lo ha modificado y ha 
            'eliminado sus datos guardados sin enviar. Acceda de nuevo al certificado.
            sClientTexts += "arrTextosML[6] = '" + JSText(Textos(20)) + "';" 'No se pueden llevar a 
            'cabo las modificaciones ya que mientras editaba el certificado el peticionario ha efectuado cambios 
            'sobre el. Acceda de nuevo al certificado.
            sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

            lblNombreCerticado.Text = oCertificado.TipoDen

            txtId.Text = oInstancia.ID
            txtSolicitante.Text = oCertificado.NomSolicitante

            tipoVersion.Value = oInstancia.TipoVersion
            VersionInicialCargada.Value = oInstancia.Version

            cmdEnviar.Visible = True
            If (Not IsTime(oCertificado.FecCumplimentacion)) AndAlso (oCertificado.Expirado = 0) Then
                'Vigente
            Else
                'Si no vigente. Se tiene en cuenta el plazo limite. Se ha a�adido el check fuera de plazo que permite env�ar si est� fuera de plazo de cumplimentaci�n

                If oCertificado.PlazoLimCumplimSuperado And Not oCertificado.FueraPlazo Then 'se te ha pasado el plazo
                    cmdEnviar.Visible = False
                End If
            End If

            If IsTime(oCertificado.FecLimCumplim) Then
                txtFechaLimite.Visible = False
                lblFechaLimite.Visible = False
            Else
                txtFechaLimite.Text = modUtilidades.FormatDate(oCertificado.FecLimCumplim, FSPMUser.DateFormat)
            End If

            If oInstancia.Grupos.Grupos Is Nothing Then
                uwtGrupos.Visible = False

                lblCamposObligatorios.Text = Textos(24)
                cmdCalcular2.Visible = False
                cmdCalcular.Visible = False
                cmdEnviar.Visible = False
                cmdGuardar.Visible = False
                cmdImpExp.Visible = False
            End If


            Dim includeScript As String
            Dim ilGMN1 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
            Dim ilGMN2 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
            Dim ilGMN3 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
            Dim ilGMN4 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN4
            Dim sScript As String

            sScript = ""
            sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
            sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
            sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
            sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)
            If Not Page.ClientScript.IsClientScriptBlockRegistered(AltaScriptKey) Then
                includeScript = String.Format(IncludeScriptFormat, "text/javascript", AltaFileName & ConfigurationManager.AppSettings("versionJs"))
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), AltaScriptKey, includeScript)
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered("varFila") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFila", "<script>var sFila = '" & JSText(Textos(22)) & "' </script>")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutanormal", "<script>var rutanormal = '" & ConfigurationManager.AppSettings("ruta") & "' </script>")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")
        End Sub
#End Region
        ''' <summary>
        ''' Carga el combo de versiones con la informacion de cuando se realizo la version del certificado
        ''' </summary>        
        ''' <param name="iInstancia">n� instancia</param>
        ''' <param name="iVersion">n� de version</param>
        ''' <param name="bPortal">Si es una version del portal</param>
        ''' <remarks>Llamada desde: Page_Load; Tiempo m�ximo: 0,3</remarks>
        Private Sub RellenarComboRespuestas(ByVal iInstancia As Integer, ByVal iVersion As Integer, ByVal bPortal As Boolean)
            Dim numRespuestas As Integer = 0
            Dim oDs As DataSet

            oDs = oCertificado.CargarRespuestas(lCiaComp, Idioma)

            Dim dt As New DataTable
            dt.Columns.Add("CLAVE", GetType(System.String))
            dt.Columns.Add("TEXTO", GetType(System.String))

            Dim indiceCombo As Integer = -1

            If oDs.Tables(0).Rows.Count = 0 OrElse (oInstancia.Grupos.Grupos Is Nothing) Then
                Me.wddVersion.Attributes.Add("style", "visibility: hidden")
            End If

            Dim sClientTextVars As String
            sClientTextVars = ""
            sClientTextVars += "vDatefmt='" + FSPMUser.DateFormat.ShortDatePattern + "';"
            sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

            Dim Index As Integer = 0

            Dim dFecha As Date
            Dim sQuien As String
            Dim sHoras As String
            Dim DifUTC As Integer = oInstancia.DiferenciaUTCServidor
            Dim DifOtz As Integer = Request.QueryString("Otz")

            oDs.Tables(0).Columns.Add("DEN", System.Type.GetType("System.String"))

            Dim oRow As DataRow
            Dim sInstancia As String
            Dim sCertificado As String = Server.UrlEncode(Encrypter.Encrypt(CStr(oCertificado.Id), , True, Encrypter.TipoDeUsuario.Administrador))

            oRow = oDs.Tables(0).NewRow
            oRow.Item("INSTANCIA") = oCertificado.Instancia
            oRow.Item("NUM_VERSION") = 0
            oRow.Item("DEN") = IIf(bPortal, Textos(7), Textos(10))
            oDs.Tables(0).Rows.InsertAt(oRow, 0)

            For Each row As DataRow In oDs.Tables(0).Rows
                Dim rowAdd As DataRow = dt.NewRow

                If row.Item("NUM_VERSION") = iVersion AndAlso row.Item("INSTANCIA") = iInstancia Then
                    indiceCombo = Index
                Else
                    Index = Index + 1
                End If

                sInstancia = Server.UrlEncode(Encrypter.Encrypt(CStr(row.Item("INSTANCIA")), , True, Encrypter.TipoDeUsuario.Administrador))

                If row.Item("NUM_VERSION") = 0 Then
                    rowAdd.Item("CLAVE") = sInstancia & "##"
                    rowAdd.Item("CLAVE") = rowAdd.Item("CLAVE") & CStr(row.Item("NUM_VERSION")) & "##"
                    rowAdd.Item("CLAVE") = rowAdd.Item("CLAVE") & sCertificado
                    rowAdd.Item("TEXTO") = row.Item("DEN")
                Else
                    If row.Item("TIPO") = 1 Then
                        numRespuestas += 1
                    End If

                    dFecha = row.Item("FECHA_MODIF")
                    dFecha = dFecha.AddMinutes(-1 * (DifUTC + DifOtz))

                    Dim sHoraMinuteAux As String = dFecha.Hour.ToString
                    If Len(sHoraMinuteAux) = 1 Then sHoraMinuteAux = "0" & sHoraMinuteAux
                    sHoras = " " & sHoraMinuteAux & ":"

                    sHoraMinuteAux = dFecha.Minute.ToString
                    If Len(sHoraMinuteAux) = 1 Then sHoraMinuteAux = "0" & sHoraMinuteAux
                    sHoras = sHoras & sHoraMinuteAux & " "

                    If Not IsDBNull(row.Item("PERSONA")) Then
                        sQuien = row.Item("PERSONA")
                    Else
                        sQuien = oCertificado.Prove & " " & row.Item("USUPORT_NOM")
                    End If

                    rowAdd.Item("CLAVE") = sInstancia & "##"
                    rowAdd.Item("CLAVE") = rowAdd.Item("CLAVE") & CStr(row.Item("NUM_VERSION")) & "##"
                    rowAdd.Item("CLAVE") = rowAdd.Item("CLAVE") & sCertificado

                    rowAdd.Item("TEXTO") = FormatDate(dFecha, FSPMUser.DateFormat) & sHoras & sQuien
                End If
                dt.Rows.Add(rowAdd)
            Next

            wddVersion.DataSource = dt
            wddVersion.TextField = "TEXTO"
            wddVersion.ValueField = "CLAVE"
            wddVersion.DataBind()
            wddVersion.SelectedItemIndex = indiceCombo

            Me.lblRespuestas.Text = Me.lblRespuestas.Text + " " + numRespuestas.ToString

            oDs = Nothing
        End Sub
    End Class
End Namespace