Namespace Fullstep.PMPortalWeb

    Partial Class noconformidad
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents tblCabecera As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents chkNotificacionesProveedor As System.Web.UI.WebControls.CheckBoxList
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object
        ''' <summary>
        ''' Devolver el Id de la compania
        ''' </summary>
        ''' <returns>Id de la compania</returns>
        ''' <remarks>Llamada desde: property oNoConformidad property oInstancia; Tiempo m�ximo:0</remarks>
        Protected ReadOnly Property lCiaComp As Long
            Get
                lCiaComp = IdCiaComp
            End Get
        End Property

        Private _oNoConformidad As Fullstep.PMPortalServer.NoConformidad
        ''' <summary>
        ''' Crear el objeto Instancia para la pantalla. Es Request("NoConformidad").
        ''' </summary>
        ''' <returns>Objeto NoConformidad</returns>
        ''' <remarks>Llamada desde: Page_Load Page_Init; Tiempo m�ximo:0,2</remarks>
        Protected ReadOnly Property oNoConformidad() As Fullstep.PMPortalServer.NoConformidad
            Get
                If _oNoConformidad Is Nothing Then

                    Dim IdNoConformidad As Long
                    Try
                        IdNoConformidad = CLng(Encrypter.Encrypt(Request("NoConformidad"), , False, Encrypter.TipoDeUsuario.Administrador))
                    Catch ex As Exception
                        IdNoConformidad = -1
                    End Try

                    If Me.IsPostBack Then
                        _oNoConformidad = CType(Cache("oNoConformidad_" & FSPMUser.IdCia & "_" & FSPMUser.Cod), Fullstep.PMPortalServer.NoConformidad)
                        If _oNoConformidad Is Nothing Then
                            _oNoConformidad = FSPMServer.Get_NoConformidad
                            _oNoConformidad.Id = IdNoConformidad
                            _oNoConformidad.Prove = FSPMUser.CodProve

                            If (Request("version") <> Nothing) AndAlso (Request("version") <> 0) Then
                                _oNoConformidad.Version = Request("version")
                            End If

                            _oNoConformidad.Load(lCiaComp, FSPMUser.Idioma.ToString)

                            Me.InsertarEnCache("oNoConformidad_" & FSPMUser.IdCia & "_" & FSPMUser.Cod, _oNoConformidad)
                        End If
                    Else
                        _oNoConformidad = FSPMServer.Get_NoConformidad
                        _oNoConformidad.Id = IdNoConformidad
                        _oNoConformidad.Prove = FSPMUser.CodProve

                        If (Request("version") <> Nothing) AndAlso (Request("version") <> 0) Then
                            _oNoConformidad.Version = Request("version")
                        End If

                        _oNoConformidad.Load(lCiaComp, FSPMUser.Idioma.ToString)

                        Me.InsertarEnCache("oNoConformidad_" & FSPMUser.IdCia & "_" & FSPMUser.Cod, _oNoConformidad)
                    End If
                End If
                Return _oNoConformidad
            End Get
        End Property

        Private _oInstancia As Fullstep.PMPortalServer.Instancia
        ''' <summary>
        ''' Crear el objeto Instancia para la pantalla. Es Request("Instancia").
        ''' </summary>
        ''' <returns>Objeto Instancia</returns>
        ''' <remarks>Llamada desde:Page_Load Page_Init; Tiempo m�ximo:0,2</remarks>
        Protected ReadOnly Property oInstancia() As Fullstep.PMPortalServer.Instancia
            Get
                If _oInstancia Is Nothing Then
                    If Me.IsPostBack Then
                        _oInstancia = CType(Cache("oInstancia_" & FSPMUser.IdCia & "_" & FSPMUser.Cod), Fullstep.PMPortalServer.Instancia)
                        If _oInstancia Is Nothing Then
                            _oInstancia = FSPMServer.Get_Instancia

                            If Request("Instancia") = Nothing Or Request("Instancia") = "null" Then
                                _oInstancia.ID = oNoConformidad.Instancia
                            Else
                                Dim IdInstancia As Long
                                Try
                                    IdInstancia = CLng(Encrypter.Encrypt(Request("Instancia"), , False, Encrypter.TipoDeUsuario.Administrador))

                                    _oInstancia.ID = IdInstancia
                                Catch ex As Exception
                                    _oInstancia.ID = oNoConformidad.Instancia
                                End Try
                            End If

                            _oInstancia.Load(lCiaComp, FSPMUser.Idioma.ToString)

                            _oInstancia.Version = oNoConformidad.Version

                            Dim bEnProceso As Boolean
                            Dim bRecuperarEnviada As Boolean

                            If (Request("version") <> Nothing) AndAlso (Request("version") <> 0) Then
                                bRecuperarEnviada = True
                            Else
                                bRecuperarEnviada = False

                                If _oInstancia.ComprobarEnProceso(lCiaComp) Then
                                    bEnProceso = True
                                End If
                            End If

                            If bRecuperarEnviada Or
                            (oNoConformidad.Estado = Fullstep.PMPortalServer.TipoEstadoNoConformidad.CierreNegativo) Or
                            (oNoConformidad.Estado = Fullstep.PMPortalServer.TipoEstadoNoConformidad.CierrePosEnPlazo) Or
                            (oNoConformidad.Estado = Fullstep.PMPortalServer.TipoEstadoNoConformidad.CierrePosFueraPlazo) Or
                            (oNoConformidad.Estado = Fullstep.PMPortalServer.TipoEstadoNoConformidad.CierrePosSinRevisar) Or
                            (oNoConformidad.Estado = Fullstep.PMPortalServer.TipoEstadoNoConformidad.CierreNegSinRevisar) Then
                                m_bSoloLectura = True
                            End If

                            _oInstancia.EnProceso = bEnProceso

                            If Not bEnProceso Then _oInstancia.CargarCamposInstancia(lCiaComp, FSPMUser.IdCia, Idioma, , FSPMUser.CodProveGS, bRecuperarEnviada)

                            Me.InsertarEnCache("oInstancia_" & FSPMUser.IdCia & "_" & FSPMUser.Cod, _oInstancia)
                        End If
                    Else
                        _oInstancia = FSPMServer.Get_Instancia

                        If Request("Instancia") = Nothing Or Request("Instancia") = "null" Then
                            _oInstancia.ID = oNoConformidad.Instancia
                        Else
                            Dim IdInstancia As Long
                            Try
                                IdInstancia = CLng(Encrypter.Encrypt(Request("Instancia"), , False, Encrypter.TipoDeUsuario.Administrador))

                                _oInstancia.ID = IdInstancia
                            Catch ex As Exception
                                _oInstancia.ID = oNoConformidad.Instancia
                            End Try
                        End If

                        _oInstancia.Load(lCiaComp, FSPMUser.Idioma.ToString)

                        _oInstancia.Version = oNoConformidad.Version

                        Dim bEnProceso As Boolean
                        Dim bRecuperarEnviada As Boolean

                        If (Request("version") <> Nothing) AndAlso (Request("version") <> 0) Then
                            bRecuperarEnviada = True
                        Else
                            bRecuperarEnviada = False

                            If _oInstancia.ComprobarEnProceso(lCiaComp) Then
                                bEnProceso = True
                            End If
                        End If

                        If bRecuperarEnviada Or
                        (oNoConformidad.Estado = Fullstep.PMPortalServer.TipoEstadoNoConformidad.CierreNegativo) Or
                        (oNoConformidad.Estado = Fullstep.PMPortalServer.TipoEstadoNoConformidad.CierrePosEnPlazo) Or
                        (oNoConformidad.Estado = Fullstep.PMPortalServer.TipoEstadoNoConformidad.CierrePosFueraPlazo) Or
                        (oNoConformidad.Estado = Fullstep.PMPortalServer.TipoEstadoNoConformidad.CierrePosSinRevisar) Or
                        (oNoConformidad.Estado = Fullstep.PMPortalServer.TipoEstadoNoConformidad.CierreNegSinRevisar) Then
                            m_bSoloLectura = True
                        End If

                        _oInstancia.EnProceso = bEnProceso

                        If Not bEnProceso Then _oInstancia.CargarCamposInstancia(lCiaComp, FSPMUser.IdCia, Idioma, , FSPMUser.CodProveGS, bRecuperarEnviada)

                        Me.InsertarEnCache("oInstancia_" & FSPMUser.IdCia & "_" & FSPMUser.Cod, _oInstancia)
                    End If
                End If
                Return _oInstancia
            End Get
        End Property

        ''' <summary>
        ''' Precarga de pagina, crea los controles, es decir, tabs, desglose, campos -> entrys
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>      
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,3</remarks>
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()

            Dim oGrupo As Fullstep.PMPortalServer.Grupo
            Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
            Dim oucCampos As Fullstep.PMPortalWeb.campos
            Dim oucDesglose As Fullstep.PMPortalWeb.desgloseControl
            Dim oRow As DataRow
            Dim oRowAccion As DataRow
            Dim lIndex As Integer = 0
            Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden

            If Not oInstancia.EnProceso Then
                diferenciaUTCServidor.Value = oInstancia.DiferenciaUTCServidor

                Me.ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.NoConformidad

                Dim oDSEstados As DataSet = oNoConformidad.CargarEstadosComentariosInternosAcciones(lCiaComp, Idioma, FSPMUser, Textos(19), Textos(20))
                Dim oAcciones As DataTable = oNoConformidad.CargarBotonesAprobarRechazarAcciones(Textos(15), Textos(16))

                'Rellena el tab :
                uwtGrupos.Tabs.Clear()
                AplicarEstilosTab(uwtGrupos)
                If Not oInstancia.Grupos.Grupos Is Nothing Then
                    For Each oGrupo In oInstancia.Grupos.Grupos
                        Dim blnHayAccion As Boolean 'para indicar si se tiene que mostrar o no el tab
                        If oNoConformidad.Acciones.Rows.Count = 0 Then
                            blnHayAccion = True
                        Else
                            For Each oRowAccion In oNoConformidad.Acciones.Rows
                                If oRowAccion.Item("GRUPO") = oGrupo.Id Then
                                    If oRowAccion.Item("SOLICITAR") = 1 Then
                                        blnHayAccion = True
                                        Exit For
                                    Else
                                        blnHayAccion = False
                                        Exit For
                                    End If
                                Else
                                    blnHayAccion = True
                                End If
                            Next
                        End If
                        If blnHayAccion Then
                            oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

                            oInputHidden.ID = "txtPre_" + lIndex.ToString
                            oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                            Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                            Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                            oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(Idioma), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                            oTabItem.Key = lIndex.ToString
                            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Hidden
                            uwtGrupos.Tabs.Add(oTabItem)
                            lIndex += 1

                            If oGrupo.DSCampos.Tables.Count > 0 Then
                                If oGrupo.NumCampos <= 1 Then
                                    For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                                        If oRow.Item("VISIBLE") = 1 Then
                                            Exit For
                                        End If

                                    Next
                                    If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Desglose Or oRow.Item("SUBTIPO") = Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoDesglose Then
                                        If oRow.Item("VISIBLE") = 0 Then
                                            uwtGrupos.Tabs.Remove(oTabItem)
                                        Else
                                            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Hidden
                                            oTabItem.ContentPane.UserControlUrl = "../_common/desglose.ascx"
                                            oucDesglose = oTabItem.ContentPane.UserControl
                                            oucDesglose.ID = oGrupo.Id.ToString
                                            oucDesglose.Campo = oRow.Item("ID_CAMPO")
                                            oucDesglose.TabContainer = uwtGrupos.ClientID
                                            oucDesglose.Ayuda = DBNullToSomething(oRow.Item("AYUDA_" & Idioma))
                                            oucDesglose.TieneIdCampo = True
                                            oucDesglose.Instancia = oInstancia.ID
                                            oucDesglose.Version = oInstancia.Version
                                            oucDesglose.SoloLectura = m_bSoloLectura Or (oRow.Item("ESCRITURA") = 0)
                                            oucDesglose.Acciones = oAcciones
                                            oucDesglose.Titulo = oRow.Item("DEN_" + Idioma)
                                            oucDesglose.DSEstados = oDSEstados
                                            oucDesglose.EsQA = True
                                            oucDesglose.InstanciaMoneda = oInstancia.Moneda
                                            oucDesglose.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                                            oInputHidden.Value = oucDesglose.ClientID
                                        End If
                                    Else
                                        oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                                        oTabItem.ContentPane.UserControlUrl = "../_common/campos.ascx"
                                        oucCampos = oTabItem.ContentPane.UserControl
                                        oucCampos.Instancia = oInstancia.ID
                                        oucCampos.ID = oGrupo.Id.ToString
                                        oucCampos.dsCampos = oGrupo.DSCampos
                                        oucCampos.IdGrupo = oGrupo.Id
                                        oucCampos.Idi = Idioma
                                        oucCampos.TabContainer = uwtGrupos.ClientID
                                        oucCampos.Version = oInstancia.Version
                                        oucCampos.SoloLectura = m_bSoloLectura
                                        oucCampos.DSEstados = oDSEstados
                                        oucCampos.Acciones = oAcciones
                                        oucCampos.InstanciaMoneda = oInstancia.Moneda
                                        oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                                        oucCampos.Formulario = oInstancia.Solicitud.Formulario
                                        oucCampos.ObjInstancia = oInstancia
                                        oInputHidden.Value = oucCampos.ClientID
                                    End If
                                Else
                                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Auto
                                    oTabItem.ContentPane.UserControlUrl = "../_common/campos.ascx"
                                    oucCampos = oTabItem.ContentPane.UserControl
                                    oucCampos.Instancia = oInstancia.ID
                                    oucCampos.ID = oGrupo.Id.ToString
                                    oucCampos.dsCampos = oGrupo.DSCampos
                                    oucCampos.IdGrupo = oGrupo.Id
                                    oucCampos.Idi = Idioma
                                    oucCampos.TabContainer = uwtGrupos.ClientID
                                    oucCampos.Version = oInstancia.Version
                                    oucCampos.SoloLectura = m_bSoloLectura
                                    oucCampos.Acciones = oAcciones
                                    oucCampos.DSEstados = oDSEstados
                                    oucCampos.InstanciaMoneda = oInstancia.Moneda
                                    oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                                    oucCampos.Formulario = oInstancia.Solicitud.Formulario
                                    oucCampos.ObjInstancia = oInstancia
                                    oInputHidden.Value = oucCampos.ClientID
                                End If
                            End If
                            Me.FindControl("frmDetalle").Controls.Add(oInputHidden)
                        End If
                    Next
                End If

                Me.FindControl("frmDetalle").Controls.Add(Fullstep.PMPortalWeb.CommonInstancia.InsertarCalendario(Idioma))
            End If

            '******************* Montar la tabla de acciones ****************************
            Dim iColumna As Integer
            Dim iFila As Integer
            Dim oTable As System.Web.UI.WebControls.Table
            Dim otblRow As System.Web.UI.WebControls.TableRow
            Dim otblCell As System.Web.UI.WebControls.TableCell
            Dim olabel As System.Web.UI.WebControls.Label
            Dim sFechaLimite As String
            If oNoConformidad.Es_Escalacion Then
                sFechaLimite = Textos(42)
            Else
                sFechaLimite = Left(Textos(21), Len(Textos(21)) - 1)
            End If

            If oNoConformidad.Acciones.Rows.Count > 0 Then
                iColumna = 1
                iFila = 0
                For Each oRow In oNoConformidad.Acciones.Rows
                    If (oRow.Item("SOLICITAR") = 1) Then
                        If iColumna = 1 Then 'a�ade una fila nueva a la tabla
                            otblRow = New System.Web.UI.WebControls.TableRow
                            iFila = iFila + 1
                        Else  'Se situa en la fila actual
                            otblRow = Me.tblAcciones.Rows.Item(iFila - 2)
                        End If

                        otblCell = New System.Web.UI.WebControls.TableCell

                        olabel = New System.Web.UI.WebControls.Label
                        olabel.Text = sFechaLimite & " "
                        If Not oNoConformidad.Es_Escalacion Then
                            Dim sCadena As String
                            If oRow.Item("DEN_ACCION") <> "" Then
                                sCadena = LCase(Left(oRow.Item("DEN_ACCION"), 1))
                                If Len(oRow.Item("DEN_ACCION")) > 1 Then sCadena = sCadena & Mid(oRow.Item("DEN_ACCION"), 2, Len(oRow.Item("DEN_ACCION")))
                                olabel.Text = olabel.Text & sCadena
                            End If
                        End If

                        olabel.CssClass = "captionBlue"
                        otblCell.Controls.Add(olabel)
                        otblCell.Height = Unit.Pixel(25)
                        otblRow.Cells.Add(otblCell)

                        If iColumna = 1 Then  'a�ade las filas
                            Me.tblAcciones.Rows.Add(otblRow)
                            otblRow = New System.Web.UI.WebControls.TableRow
                            iFila = iFila + 1
                        Else
                            otblRow = Me.tblAcciones.Rows.Item(iFila - 1)
                        End If

                        'Fecha l�mite:
                        otblCell = New System.Web.UI.WebControls.TableCell
                        Dim otblRow2 As System.Web.UI.WebControls.TableRow
                        Dim otblCell2 As System.Web.UI.WebControls.TableCell

                        oTable = New System.Web.UI.WebControls.Table
                        otblRow2 = New System.Web.UI.WebControls.TableRow
                        otblCell2 = New System.Web.UI.WebControls.TableCell


                        olabel = New System.Web.UI.WebControls.Label
                        olabel.Text = ""
                        If Not IsDBNull(oRow.Item("FECHA_LIMITE")) Then
                            olabel.Text = FormatDate(oRow.Item("FECHA_LIMITE"), FSPMUser.DateFormat)
                        End If
                        olabel.CssClass = "captionDarkBlue"
                        olabel.Width = Unit.Percentage(100)
                        otblCell.Controls.Add(olabel)
                        otblRow2.Cells.Add(otblCell2)

                        'a�ade la fila a la tabla de acciones
                        oTable.Rows.Add(otblRow2)
                        otblCell.Controls.Add(oTable)
                        otblRow.Cells.Add(otblCell)

                        If iColumna = 4 Then
                            iColumna = 1
                        Else
                            If iColumna = 1 Then
                                Me.tblAcciones.Rows.Add(otblRow)
                            End If
                            iColumna = iColumna + 1
                        End If
                    End If
                Next
            End If
        End Sub

#End Region

        Private Const AltaScriptKey As String = "AltaIncludeScript"
        Private Const AltaFileName As String = "../_common/js/jsAlta.js?v="
        Private Const IncludeScriptFormat As String = ControlChars.CrLf & _
    "<script type=""{0}"" src=""{1}""></script>"
        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script language=""{0}"">{1}</script>"
        Private m_sTextoAnulacion As String
        Private m_sMsgboxAccion(3) As String
        Private m_bSoloLectura As Boolean

        Public m_contador As Integer = 0

        ''' <summary>
        ''' Mostrar el detalle de una no conformidad
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">evento de sistema</param>           
        ''' <returns>Nada</returns>
        ''' <remarks>Llamada desde: noconformidades/NoConformidades11.asp   noconformidad/noconformidadesCerradas.aspx  
        ''' ; Tiempo m�ximo: 0,1</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Page.IsPostBack Then Exit Sub

            imgExpand.Attributes.Add("onclick", "return OcultarNotificaciones();")
            imgExpand.ImageUrl = "../../App_Themes/" & Me.Page.Theme & "/images/ig_menu_scrolldown.gif"
            imgCollapse.Attributes.Add("onclick", "return OcultarNotificaciones();")
            imgCollapse.ImageUrl = "../../App_Themes/" & Me.Page.Theme & "/images/ig_menu_scrollup.gif"

            Dim sClientTextVars As String
            sClientTextVars = ""
            sClientTextVars += "vdecimalfmt='" + FSPMUser.DecimalFmt + "';"
            sClientTextVars += "vthousanfmt='" + FSPMUser.ThousanFmt + "';"
            sClientTextVars += "vprecisionfmt='" + FSPMUser.PrecisionFmt + "';"
            sClientTextVars += "vDatefmt='" + FSPMUser.DateFormat.ShortDatePattern + "';"
            sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

            Me.ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.NoConformidad

            lblNotificaciones.Text = Textos(38)

            Me.NoConformidad.Value = oNoConformidad.Id

            Dim iVersion As Long

            Dim bRecuperarEnviada As Boolean
            If (Request("version") <> Nothing) AndAlso (Request("version") <> 0) Then
                iVersion = Request("Version")

                bRecuperarEnviada = True
            Else
                bRecuperarEnviada = False
            End If

            Dim oDScmdCalcular As DataSet
            Me.cmdCalcular.Visible = False
            Me.BotonCalcular.Value = 0

            'Hacer visible el boton de calcular
            cmdCalcular.Visible = oInstancia.Grupos.Grupos.OfType(Of PMPortalServer.Grupo).Where(Function(x) x.DSCampos.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) y("TIPO") = TipoCampoPredefinido.Calculado).Any).Any()
            If cmdCalcular.Visible Then BotonCalcular.Value = 1

            Me.Version.Value = oNoConformidad.Version
            Me.VersionInicialCargada.Value = oNoConformidad.Version

            Me.lblId.Text = Textos(0)
            Me.lblSolicitante.Text = Textos(1)
            If oNoConformidad.Es_Escalacion Then
                Me.lblFechaLimite.Text = Textos(41)
            Else
                Me.lblFechaLimite.Text = Textos(2)
            End If
            Me.lblFechaApliMejoras.Text = Textos(43)

            Me.lblRespuestas.Text = Textos(5)
            Me.cmdCalcular.Value = Textos(23)
            Me.cmdGuardar.Value = Textos(3)
            Me.cmdEnviar.Value = Textos(4)
            Me.lblEstado.Text = Textos(11)
            Me.cmdImpExp.Value = Textos(33)

            Select Case oNoConformidad.Estado
                Case 0 'Guardada
                    Me.txtEstado.Text = Textos(24)
                Case 1 'Abierta
                    Me.txtEstado.Text = Textos(28)
                Case 2 'Cierre eficaz dentro de plazo
                    Me.txtEstado.Text = Textos(25)
                Case 3 'Cierre eficaz fuera de plazo
                    Me.txtEstado.Text = Textos(26)
                Case 4 'Cierre no eficaz
                    Me.txtEstado.Text = Textos(27)
                Case 5, 6 'Pendiente de revisar cierre
                    Me.txtEstado.Text = Textos(29)
            End Select

            'Tema de la cadena espera
            Me.cadenaespera.Value = Textos(30)

            If _oInstancia.EnProceso Then
                Me.enproceso.Value = "1"
                Me.imgProgreso.Visible = False
                Me.lblProgreso.Text = Textos(32)
            Else
                If oNoConformidad.Portal And Not bRecuperarEnviada Then
                    Me.lblSinEnviar.Text = Textos(8)
                End If
                Me.lblProgreso.Text = Textos(31)
            End If

            If m_bSoloLectura Then
                Me.cmdCalcular.Visible = False
                Me.cmdGuardar.Visible = False
                Me.cmdEnviar.Visible = False
            End If

            Dim sClientTexts As String
            sClientTexts = ""
            sClientTexts += "var arrTextosML = new Array();"
            sClientTexts += "arrTextosML[0] = '" + JSText(Textos(9)) + "';"
            sClientTexts += "arrTextosML[1] = '" + JSText(Textos(34)) + "';" 'Error Ajax
            sClientTexts += "arrTextosML[2] = '" + JSText(Textos(35)) + "';" 'en proceso
            sClientTexts += "arrTextosML[3] = '" + JSText(Textos(36)) + "';" 'No se pueden llevar a cabo las 
            'modificaciones ya que mientras editaba la no conformidad el peticionario de la misma la ha 
            'modificado y ha eliminado sus datos guardados sin enviar. Acceda de nuevo a la no conformidad.
            sClientTexts += "arrTextosML[4] = '" + JSText(Textos(37)) + "';" 'No se pueden llevar a cabo
            ' las modificaciones ya que mientras editaba la no conformidad el peticionario de la no conformidad 
            'ha efectuado cambios sobre ella. Acceda de nuevo a la no conformidad
            sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

            If Not Page.ClientScript.IsClientScriptBlockRegistered("varFila") Then _
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFila", "<script>var sFila = '" & JSText(Textos(39)) & "' </script>")

            RellenarComboRespuestas(oInstancia.ID, iVersion, oNoConformidad.Portal)

            Me.tipoVersion.Value = oInstancia.TipoVersion

            Me.lblNombreCerticado.Text = oNoConformidad.TipoDen

            Me.txtId.Text = oInstancia.ID
            Me.txtSolicitante.Text = oNoConformidad.NomSolicitante
            Me.txtFechaLimite.Text = modUtilidades.FormatDate(oNoConformidad.FecLimCumplimentacion, FSPMUser.DateFormat)
            Me.txtFechaApliMejoras.Text = modUtilidades.FormatDate(oNoConformidad.FechaAplicPlan, FSPMUser.DateFormat)
            Me.Instancia.Value = oInstancia.ID

            '''
            Me.lblUnidadQA.Text = Textos(32)
            Me.lblCamposObligatorios.Text = Textos(40)
            Me.lblUnicoUnidadQA.Text = oNoConformidad.UnidadQADen
            '''

            Dim includeScript As String
            Dim ilGMN1 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
            Dim ilGMN2 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
            Dim ilGMN3 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
            Dim ilGMN4 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN4
            Dim sScript As String

            sScript = ""
            sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
            sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
            sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
            sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)
            If Not Page.ClientScript.IsClientScriptBlockRegistered(AltaScriptKey) Then
                includeScript = String.Format(IncludeScriptFormat, "text/javascript", AltaFileName & ConfigurationManager.AppSettings("versionJs"))
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), AltaScriptKey, includeScript)
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
            End If

            Load_Notificados_Proveedor(oNoConformidad.Id, oNoConformidad.Prove, lCiaComp)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "esNoconfEscalacion", "var esNoconfEscalacion=" & oNoConformidad.Es_Escalacion.ToString.ToLower & ";", True)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "<script>var accesoExterno=" & FSPMServer.AccesoServidorExterno.ToString.ToLower & "</script>")
        End Sub

        ''' <summary>
        ''' Cargar la combo de Versiones. Por cada fila, pone el formato de la fecha
        ''' </summary>
        ''' <param name="iInstancia">Identificador instancia</param>
        ''' <param name="iVersion">N� version</param>
        ''' <param name="bPortal">Si la versi�n se ha guardado desde el portal</param>
        ''' <remarks>Llamada desde= Databind (ugtxtVersion); Tiempo m�ximo=0seg.</remarks>
        Private Sub RellenarComboRespuestas(ByVal iInstancia As Integer, ByVal iVersion As Integer, ByVal bPortal As Boolean)
            Dim lCiaComp As Long = IdCiaComp
            Dim numRespuestas As Integer = 0
            Dim oDs As DataSet

            oDs = oNoConformidad.CargarRespuestas(lCiaComp, FSPMUser.Idioma.ToString)

            Dim dt As New DataTable
            dt.Columns.Add("CLAVE", GetType(System.String))
            dt.Columns.Add("TEXTO", GetType(System.String))

            Dim indiceCombo As Integer = -1

            If oDs.Tables(0).Rows.Count = 0 Then
                Me.wddVersion.Attributes.Add("style", "visibility: hidden")
            End If

            Dim Index As Integer = 0

            Dim dFecha As Date
            Dim sQuien As String
            Dim DifUTC As Integer = oInstancia.DiferenciaUTCServidor
            Dim DifOtz As Integer = Request.QueryString("Otz")

            oDs.Tables(0).Columns.Add("DEN", System.Type.GetType("System.String"))

            Dim oRow As DataRow
            Dim dtFechaModif As Date
            Dim sInstancia As String = Server.UrlEncode(Encrypter.Encrypt(CStr(iInstancia), , True, Encrypter.TipoDeUsuario.Administrador))
            Dim sNoConformidad As String = Server.UrlEncode(Encrypter.Encrypt(CStr(oNoConformidad.Id), , True, Encrypter.TipoDeUsuario.Administrador))

            If bPortal Then
                oRow = oDs.Tables(0).NewRow

                oRow.Item("INSTANCIA") = iInstancia
                oRow.Item("NUM_VERSION") = 0
                oRow.Item("DEN") = Textos(7)

                oDs.Tables(0).Rows.InsertAt(oRow, 0)
            Else
                oRow = oDs.Tables(0).NewRow

                oRow.Item("INSTANCIA") = iInstancia
                oRow.Item("NUM_VERSION") = 0
                oRow.Item("DEN") = Textos(10)

                oDs.Tables(0).Rows.InsertAt(oRow, 0)
            End If

            For Each row As DataRow In oDs.Tables(0).Rows
                Dim rowAdd As DataRow = dt.NewRow

                If row.Item("NUM_VERSION") = iVersion AndAlso row.Item("INSTANCIA") = iInstancia Then
                    indiceCombo = Index
                Else
                    Index = Index + 1
                End If

                If row.Item("NUM_VERSION") = 0 Then
                    rowAdd.Item("CLAVE") = sInstancia & "##"
                    rowAdd.Item("CLAVE") = rowAdd.Item("CLAVE") & CStr(row.Item("NUM_VERSION")) & "##"
                    rowAdd.Item("CLAVE") = rowAdd.Item("CLAVE") & sNoConformidad

                    rowAdd.Item("TEXTO") = row.Item("DEN")
                Else
                    If row.Item("TIPO") = 1 Then
                        numRespuestas += 1
                    End If

                    dFecha = row.Item("FECHA_MODIF")
                    dFecha = dFecha.AddMinutes(-1 * (DifUTC + DifOtz))

                    If Not IsDBNull(row.Item("PERSONA")) Then
                        sQuien = row.Item("PERSONA")
                        'ElseIf Not IsDBNull(row.Item("USUPORT_NOM")) Then
                        '    sQuien = row.Item("PROVEEDOR") & " (" & row.Item("USUPORT_NOM") & ")"
                    Else
                        sQuien = row.Item("USUPORT_NOM")
                    End If

                    rowAdd.Item("CLAVE") = sInstancia & "##"
                    rowAdd.Item("CLAVE") = rowAdd.Item("CLAVE") & CStr(row.Item("NUM_VERSION")) & "##"
                    rowAdd.Item("CLAVE") = rowAdd.Item("CLAVE") & sNoConformidad

                    rowAdd.Item("TEXTO") = FormatDate(dFecha, FSPMUser.DateFormat) & " " & sQuien '"g"
                End If
                dt.Rows.Add(rowAdd)
            Next

            wddVersion.DataSource = dt

            wddVersion.TextField = "TEXTO"
            wddVersion.ValueField = "CLAVE"

            wddVersion.DataBind()

            Me.wddVersion.SelectedItemIndex = indiceCombo

            Me.lblRespuestas.Text = Me.lblRespuestas.Text + " " + numRespuestas.ToString

            oDs = Nothing
        End Sub

        Private Sub Load_Notificados_Proveedor(ByVal lIdNoConformidad As Long, ByVal CodProve As String, ByVal lCiaComp As Long)
            Dim oNotificacionesNoConformidad As Fullstep.PMPortalServer.NoConformidad
            oNotificacionesNoConformidad = FSPMServer.Get_NoConformidad

            Dim Notificaciones As DataSet = oNotificacionesNoConformidad.Load_Notificados_Proveedor(lIdNoConformidad, CodProve, lCiaComp)
            Dim Contacto As ListItem

            With rptNotificacionesProveedor
                .DataSource = Notificaciones
                .DataBind()
            End With

        End Sub

        Private Sub rptNotificacionesProveedor_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptNotificacionesProveedor.ItemDataBound
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                m_contador += 1
            End If
        End Sub

    End Class
End Namespace
