Namespace Fullstep.PMPortalWeb
    Partial Class envionoconformidadOk
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' <summary>
        ''' Pantalla de informaci�n de envio de no conformidad correctamente
        ''' </summary>
        ''' <param name="sender"> Propio del evento </param>
        ''' <param name="e"> Propio del evento </param>        
        ''' <remarks> Llamada desde: _common\guardarinstancia.aspx  noconformidad\enviarnoconformidad.aspx; Tiempo m�ximo = 0.1</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim oDict As Fullstep.PMPortalServer.Dictionary
            Dim oTextos As DataTable
            Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User
            Dim lCiaComp As Long = IdCiaComp
            oUser = Session("FS_Portal_User")

            Dim sIdi As String = Idioma
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If


            oDict = FSPMServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.EnvioNoConformidadOk, sIdi)
            oTextos = oDict.Data.Tables(0)

            Dim oNoConformidad As Fullstep.PMPortalServer.NoConformidad

            oNoConformidad = FSPMServer.Get_NoConformidad
            oNoConformidad.Prove = oUser.CodProve
            oNoConformidad.Id = Request("NoConformidad")
            oNoConformidad.Load(lCiaComp, sIdi)

            If Page.IsPostBack Then Exit Sub

            Me.lblNombreCerticado.Text = oNoConformidad.TipoDen

            Me.lblId.Text = oTextos.Rows(0).Item(1)
            Me.lblSolicitante.Text = oTextos.Rows(1).Item(1)
            Me.lblFechaLimite.Text = oTextos.Rows(2).Item(1)

            Me.mensajeOk.Text = oTextos.Rows(3).Item(1)
            Me.txtId.Text = oNoConformidad.Instancia
            Me.txtSolicitante.Text = oNoConformidad.NomSolicitante
            Me.txtFechaLimite.Text = modUtilidades.FormatDate(oNoConformidad.FecLimCumplimentacion, oUser.DateFormat)

            oNoConformidad = Nothing
            oTextos = Nothing
            oDict = Nothing

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)
            End If
        End Sub

    End Class
End Namespace