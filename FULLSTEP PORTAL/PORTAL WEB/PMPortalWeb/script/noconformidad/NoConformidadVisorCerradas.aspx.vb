﻿Imports Infragistics.Web.UI.GridControls
Imports Infragistics.Documents.Reports
Imports Infragistics.Documents.Reports.Report

Namespace Fullstep.PMPortalWeb
    Public Class NoConformidadVisorCerradas
        Inherits FSPMPage

#Region "Property"
        ''' Revisado por: Jbg. Fecha: 01/12/2011
        ''' <summary>
        ''' Devolver el Id de la compania
        ''' </summary>
        ''' <returns>Id de la compania</returns>
        ''' <remarks>Llamada desde: property oSolicitud property oInstancia property oCertificado; Tiempo máximo:0</remarks>
        Protected ReadOnly Property lCiaComp As Integer
            Get
                lCiaComp = IdCiaComp
            End Get
        End Property
#End Region

#Region "Inicializar pagina"
        ''' Revisado por: Jbg. Fecha: 20/10/2011
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Me.ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.NoConformidades
            CargarTextos()
            CargaCombo()

            If Not IsPostBack Then
                Dim field As Infragistics.Web.UI.GridControls.BoundDataField = Me.wdgHDNoConfs.Columns.FromKey("FECALTA")
                Dim UserDateFmt As String = Replace(FSPMUser.DateFmt, "mm", "MM")
                field.DataFormatString = "{0:" & UserDateFmt & "}"

                CargarGrid(True)


                CargaImagenes()

                Me.wdgHDNoConfs.Behaviors.VirtualScrolling.RowCacheFactor = System.Configuration.ConfigurationManager.AppSettings("RowCacheFactor")
            ElseIf Request("__EVENTARGUMENT") = "," Then
                'Todas las acciones sobre el grid provocan postback y todas necesitan de databind. 
                'Existen eventos para wdgDatos_ColumnSorted y wdgDatos_DataFiltered
                'Pero no existe para el virtualscrolling
                CargarGrid(False)
            Else
                CargarGrid(False)
                Select Case Request("__EVENTTARGET")
                    Case "ExportarExcel"
                        ExportarExcel()
                    Case "ExportarPDF"
                        ExportarPDF()
                End Select
            End If

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)
            End If
        End Sub
        ''' Revisado por: Jbg. Fecha: 01/12/2011
        ''' <summary>
        ''' Carga el idioma correspondiente en los controles
        ''' </summary>
        ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
        Private Sub CargarTextos()
            'Textos:
            FSNPageHeader.TituloCabecera = Textos(0)
            Me.lblIdent.Text = Textos(1)
            Me.lblTipo.Text = Textos(2)
            Me.lblDesde.Text = Textos(3)

            Me.btnBuscar.Text = Textos(4)
            Me.btnLimpiar.Text = Textos(43)

            'Para el botón de cierre del popup
            Me.hCerrar.Value = Textos(20)

            'Caption de la grid de instancias:
            Dim field As Infragistics.Web.UI.GridControls.BoundDataField
            field = Me.wdgHDNoConfs.Columns.FromKey("ID_INS")
            field.Header.Text = Textos(7)
            field = Me.wdgHDNoConfs.Columns.FromKey("TIPO_NOCONF")
            field.Header.Text = Textos(8)
            field = Me.wdgHDNoConfs.Columns.FromKey("FECALTA")
            field.Header.Text = Textos(9)
            field = Me.wdgHDNoConfs.Columns.FromKey("ARTICULO")
            field.Header.Text = Textos(10)
            field = Me.wdgHDNoConfs.Columns.FromKey("CIERRE_DEN")
            field.Header.Text = Textos(11)
            Dim fieldc As Infragistics.Web.UI.GridControls.GridField
            fieldc = Me.wdgHDNoConfs.Columns.FromKey("COMENT")
            fieldc.Header.Text = Textos(19)
            field = Me.wdgHDNoConfs.Columns.FromKey("COMENT_ALTA")
            field.Header.Text = Textos(21)
            field = Me.wdgHDNoConfs.Columns.FromKey("COMENT_CIERRE")
            field.Header.Text = Textos(22)
            fieldc = Me.wdgHDNoConfs.Columns.FromKey("COMENT_IMPR")
            fieldc.Header.Text = Textos(19)
            fieldc = Me.wdgHDNoConfs.Columns.FromKey("FEC_CIERRE")
            fieldc.Header.Text = Textos(45)

            Dim fieldGridView As Infragistics.Web.UI.GridControls.BoundDataField
            fieldGridView = Me.wdgHDNoConfs.GridView.Columns.FromKey("ID_INS")
            fieldGridView.Header.Text = Textos(7)
            fieldGridView = Me.wdgHDNoConfs.GridView.Columns.FromKey("TIPO_NOCONF")
            fieldGridView.Header.Text = Textos(8)
            fieldGridView = Me.wdgHDNoConfs.GridView.Columns.FromKey("FECALTA")
            fieldGridView.Header.Text = Textos(9)
            fieldGridView = Me.wdgHDNoConfs.GridView.Columns.FromKey("ARTICULO")
            fieldGridView.Header.Text = Textos(10)
            fieldGridView = Me.wdgHDNoConfs.GridView.Columns.FromKey("CIERRE_DEN")
            fieldGridView.Header.Text = Textos(11)
            Dim fieldGridViewc As Infragistics.Web.UI.GridControls.GridField
            fieldGridViewc = Me.wdgHDNoConfs.GridView.Columns.FromKey("COMENT")
            fieldGridViewc.Header.Text = Textos(19)
            fieldGridView = Me.wdgHDNoConfs.GridView.Columns.FromKey("COMENT_ALTA")
            fieldGridView.Header.Text = Textos(21)
            fieldGridView = Me.wdgHDNoConfs.GridView.Columns.FromKey("COMENT_CIERRE")
            fieldGridView.Header.Text = Textos(22)
            fieldGridViewc = Me.wdgHDNoConfs.GridView.Columns.FromKey("COMENT_IMPR")
            fieldGridViewc.Header.Text = Textos(19)
            fieldGridViewc = Me.wdgHDNoConfs.GridView.Columns.FromKey("FEC_CIERRE")
            fieldGridViewc.Header.Text = Textos(45)

            Me.wdgHDNoConfs.GroupingSettings.EmptyGroupAreaText = Textos(12)

            'Exportar
            Me.lblPDF.Text = Textos(15)
            Me.lblExcel.Text = Textos(13)

            'Collapsible
            Me.lblBusqueda.Text = Textos(42)

            If Not IsPostBack Then
                Dim fieldSetting As Infragistics.Web.UI.GridControls.ColumnFilteringSetting

                fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
                fieldSetting.ColumnKey = "ID_INS"
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("ID_INS").BeforeFilterAppliedAltText = Textos(44)
                Me.wdgHDNoConfs.GridView.Behaviors.Filtering.ColumnSettings.Item("ID_INS").BeforeFilterAppliedAltText = Textos(44)

                fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
                fieldSetting.ColumnKey = "TIPO_NOCONF"
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("TIPO_NOCONF").BeforeFilterAppliedAltText = Textos(44)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("TIPO_NOCONF").BeforeFilterAppliedAltText = Textos(44)

                fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
                fieldSetting.ColumnKey = "FECALTA"
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("FECALTA").BeforeFilterAppliedAltText = Textos(44)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("FECALTA").BeforeFilterAppliedAltText = Textos(44)

                fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
                fieldSetting.ColumnKey = "ARTICULO"
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("ARTICULO").BeforeFilterAppliedAltText = Textos(44)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("ARTICULO").BeforeFilterAppliedAltText = Textos(44)

                fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
                fieldSetting.ColumnKey = "CIERRE_DEN"
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("CIERRE_DEN").BeforeFilterAppliedAltText = Textos(44)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("CIERRE_DEN").BeforeFilterAppliedAltText = Textos(44)

                fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
                fieldSetting.ColumnKey = "COMENT"
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("COMENT").BeforeFilterAppliedAltText = Textos(44)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("COMENT").BeforeFilterAppliedAltText = Textos(44)

                fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
                fieldSetting.ColumnKey = "COMENT_ALTA"
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("COMENT_ALTA").BeforeFilterAppliedAltText = Textos(44)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("COMENT_ALTA").BeforeFilterAppliedAltText = Textos(44)

                fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
                fieldSetting.ColumnKey = "COMENT_CIERRE"
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("COMENT_CIERRE").BeforeFilterAppliedAltText = Textos(44)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("COMENT_CIERRE").BeforeFilterAppliedAltText = Textos(44)

                fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
                fieldSetting.ColumnKey = "COMENT_IMPR"
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("COMENT_IMPR").BeforeFilterAppliedAltText = Textos(44)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("COMENT_IMPR").BeforeFilterAppliedAltText = Textos(44)

                fieldSetting = New Infragistics.Web.UI.GridControls.ColumnFilteringSetting
                fieldSetting.ColumnKey = "COMENT_ALTA"
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.GridView.Behaviors.Filtering.ColumnSettings.Add(fieldSetting)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("COMENT_ALTA").BeforeFilterAppliedAltText = Textos(44)
                Me.wdgHDNoConfs.Behaviors.Filtering.ColumnSettings.Item("COMENT_ALTA").BeforeFilterAppliedAltText = Textos(44)

            End If
        End Sub
        ''' Revisado por: Jbg. Fecha: 01/12/2011
        ''' <summary>
        ''' Carga el combo de los tipos de no conformidad
        ''' </summary>
        ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
        Private Sub CargaCombo()
            Dim oNoConformidades As Fullstep.PMPortalServer.NoConformidades
            oNoConformidades = FSPMServer.Get_NoConformidades()
            oNoConformidades.Load_Tipo_NoConformidades(lCiaComp, FSPMUser.Cod, Me.Idioma, 1, 3) 'Tipo de NoConformidades = 3

            oNoConformidades.Data.Tables(0).Rows(0).Item("DEN") = Textos(6)  '(Cualquier tipo)

            wddTipoSolicitud.DataSource = oNoConformidades.Data

            wddTipoSolicitud.TextField = "DEN"
            wddTipoSolicitud.ValueField = "ID"

            wddTipoSolicitud.DataBind()

            If Not Page.IsPostBack Then
                If wddTipoSolicitud.SelectedItemIndex = -1 Then
                    wddTipoSolicitud.SelectedItemIndex = 0 '(Cualquier tipo)
                End If

                hidTipoSol.Value = Me.wddTipoSolicitud.SelectedItemIndex
            Else
                wddTipoSolicitud.SelectedItemIndex = hidTipoSol.Value
            End If
        End Sub

        ''' Revisado por: Jbg. Fecha: 01/12/2011
        ''' <summary>
        ''' Carga las imagenes correspondiente en los controles
        ''' </summary>
        ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
        Private Sub CargaImagenes()
            FSNPageHeader.UrlImagenCabecera = "~/App_Themes/" & Page.Theme & "/images/noconformidad.png"

            imgPDF.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/PDF.png"
            imgExcel.ImageUrl = "~/App_Themes/" & Page.Theme & "/images/Excel.png"
        End Sub
#End Region

#Region "Grid NoConformidades"

        ''' Revisado por: Jbg. Fecha: 01/12/2011
        ''' <summary>
        ''' Establecer el aspecto de una fila del grid 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">evento de sistema</param>       
        ''' <remarks>Llamada desde: sistema; Tiempo máximo:0 </remarks>
        Private Sub wdgHDNoConfs_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdgHDNoConfs.InitializeRow
            Dim Index As Integer

            If Request("__EVENTTARGET") = "ExportarExcel" OrElse Request("__EVENTTARGET") = "ExportarPDF" Then
                Index = sender.Columns.FromKey("COMENT").Index
                e.Row.Items.Item(Index).Column.Hidden = True

                If Request("__EVENTTARGET") = "ExportarExcel" Then
                    Index = sender.Columns.FromKey("COMENT_ALTA").Index
                    e.Row.Items.Item(Index).Column.Hidden = False
                    Index = sender.Columns.FromKey("COMENT_CIERRE").Index
                    e.Row.Items.Item(Index).Column.Hidden = False
                Else
                    Index = sender.Columns.FromKey("COMENT_IMPR").Index
                    e.Row.Items.Item(Index).Column.Hidden = False
                End If
            End If

            'Inicializa la columna CIERRE
            Index = sender.Columns.FromKey("CIERRE").Index
            Dim IndexD As Integer = sender.Columns.FromKey("CIERRE_DEN").Index

            Select Case e.Row.Items.Item(Index).Value
                Case 2
                    e.Row.Items.Item(IndexD).CssClass = "noConformidadCierreOK"
                Case 3
                    e.Row.Items.Item(IndexD).CssClass = "noConformidadCierreOK"
                Case Else
                    e.Row.Items.Item(IndexD).CssClass = "noConformidadCierreNoOK"
            End Select

            'Añade el icono para ver el comentario  e.Row.Items.Item(sender.Columns.FromKey("COMENT").Index).Value
            If e.Row.Items.Item(sender.Columns.FromKey("COMENT_CIERRE").Index).Text <> "" _
            OrElse e.Row.Items.Item(sender.Columns.FromKey("COMENT_ALTA").Index).Text <> "" Then
                e.Row.Items.Item(sender.Columns.FromKey("COMENT").Index).Text = "<img src='../_common/images/coment.gif' style='text-align:center;'/>"

                Dim sTexto As String
                If e.Row.Items.Item(sender.Columns.FromKey("COMENT_ALTA").Index).Text <> "" Then
                    sTexto = Textos(21) & ": " & e.Row.Items.Item(sender.Columns.FromKey("COMENT_ALTA").Index).Text
                    If e.Row.Items.Item(sender.Columns.FromKey("COMENT_CIERRE").Index).Text <> "" Then
                        sTexto = sTexto & vbCrLf
                        sTexto = sTexto & Textos(22) & ": " & e.Row.Items.Item(sender.Columns.FromKey("COMENT_CIERRE").Index).Text
                    End If
                ElseIf e.Row.Items.Item(sender.Columns.FromKey("COMENT_CIERRE").Index).Text <> "" Then
                    sTexto = Textos(22) & ": " & e.Row.Items.Item(sender.Columns.FromKey("COMENT_CIERRE").Index).Text
                End If

                e.Row.Items.Item(sender.Columns.FromKey("COMENT_IMPR").Index).Text = sTexto
            End If

            e.Row.Items.Item(sender.Columns.FromKey("RCOMENT_CIERRE").Index).Text = Replace(JSText(e.Row.Items.Item(sender.Columns.FromKey("COMENT_CIERRE").Index).Text), "\n", "@VBCRLF@")
            e.Row.Items.Item(sender.Columns.FromKey("RCOMENT_ALTA").Index).Text = Replace(JSText(e.Row.Items.Item(sender.Columns.FromKey("COMENT_ALTA").Index).Text), "\n", "@VBCRLF@")

            e.Row.Items.Item(sender.Columns.FromKey("ID_NOCONF_ENCRYPTED").Index).Value = Server.UrlEncode(Encrypter.Encrypt(DBNullToInt(e.Row.DataItem.Item.Row.Item("ID_NOCONF")), , True, Encrypter.TipoDeUsuario.Administrador))
            e.Row.Items.Item(sender.Columns.FromKey("ID_INS_ENCRYPTED").Index).Value = Server.UrlEncode(Encrypter.Encrypt(DBNullToInt(e.Row.DataItem.Item.Row.Item("ID_INS")), , True, Encrypter.TipoDeUsuario.Administrador))
        End Sub
#End Region

#Region "Botonera"
        ''' Revisado por: Jbg. Fecha: 01/12/2011
        ''' <summary>
        ''' Exporta a Excel
        ''' </summary>
        ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
        Private Sub ExportarExcel()
            Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
            fileName = fileName.Replace("+", "%20")

            With wdgExcelExporter
                .DownloadName = fileName
                .DataExportMode = DataExportMode.AllDataInDataSource

                .Export(wdgHDNoConfs)
            End With
        End Sub
        ''' Revisado por: Jbg. Fecha: 01/12/2011
        ''' <summary>
        ''' Exporta a PDF
        ''' </summary>
        ''' <remarks>Llamada desde: Page_load; Tiempo máximo:0,1</remarks>
        Private Sub ExportarPDF()
            Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
            fileName = fileName.Replace("+", "%20")
            wdgPDFExporter.DownloadName = fileName

            With wdgPDFExporter
                .EnableStylesExport = True

                .DataExportMode = DataExportMode.AllDataInDataSource
                .TargetPaperOrientation = PageOrientation.Landscape

                .Margins = PageMargins.GetPageMargins("Narrow")
                .TargetPaperSize = PageSizes.GetPageSize("A4")
                .Export(wdgHDNoConfs)
            End With
        End Sub
#End Region

#Region "Carga datos"

        ''' <summary>
        ''' Nueva busqueda , nuevos parametros de busqueda
        ''' </summary>
        ''' <param name="sender">control</param>
        ''' <param name="e">evento de sistema</param>  
        ''' <remarks>Llamada desde: sistema; Tiempo mÃ¡ximo:0 </remarks>
        Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
            CargarGrid(True)
        End Sub

        ''' <summary>
        ''' Carga la grid con los certificados
        ''' </summary>
        ''' <remarks>Llamada desde: page_load   btnBuscar_Click; Tiempo mÃ¡ximo:0,3</remarks>
        Private Sub CargarGrid(ByVal bCargaCompleta As Boolean)
            If bCargaCompleta Then
                Dim oNoConfs As PMPortalServer.NoConformidades
                oNoConfs = FSPMServer.Get_NoConformidades

                Dim lId As Long
                lId = 0
                If txtID.Text <> Nothing Then
                    If IsNumeric(txtID.Text) Then
                        lId = txtID.Text
                    End If
                End If

                Dim lTipo As Long
                lTipo = 0
                If wddTipoSolicitud.SelectedItemIndex > 0 Then 'algo selecc y q no sea "Cualquier tipo"
                    lTipo = CLng(Me.wddTipoSolicitud.SelectedItem.Value)
                End If

                Dim oDS As DataSet
                If WDFecDesde.Value = Nothing Or IsTime(WDFecDesde.Value) Then
                    oDS = oNoConfs.Devolver_NoConformidades_Cerradas(lCiaComp, EmpresaPortal, NomPortal, FSPMUser.CodProve, lId, lTipo, Nothing, Idioma)
                Else
                    oDS = oNoConfs.Devolver_NoConformidades_Cerradas(lCiaComp, EmpresaPortal, NomPortal, FSPMUser.CodProve, lId, lTipo, DateToBDDate(WDFecDesde.Value), Idioma)
                End If

                wdgHDNoConfs.DataSource = oDS
                wdgHDNoConfs.GridView.DataSource = oDS
                wdgHDNoConfs.DataBind()

                InsertarEnCache("oNoConformidades_" & FSPMUser.IdCia & "_" & FSPMUser.Cod, oDS)
            Else
                wdgHDNoConfs.DataSource = CType(Cache("oNoConformidades_" & FSPMUser.IdCia & "_" & FSPMUser.Cod), DataSet)
                wdgHDNoConfs.DataBind()
            End If
            UpdDatos.Update()
        End Sub

#End Region
    End Class
End Namespace