Namespace Fullstep.PMPortalWeb
    Partial Class noconfdesglose
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar un desglose dado.
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">evento de sistema</param>      
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim idCampo As Integer = Request("campo")
            Dim sInput As String = Request("Input")
            Dim lInstancia As Integer = Request("instancia")
            Dim lVersion As Long = Request("Version")
            Dim lCiaComp As Long = IdCiaComp

            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Desglose

            INPUTDESGLOSE.Value = sInput
            Instancia.Value = lInstancia
           
            Dim oCampo As Fullstep.PMPortalServer.Campo
            Dim oInstancia As Fullstep.PMPortalServer.Instancia
            oInstancia = FSPMServer.Get_Instancia
            oInstancia.ID = lInstancia
            oInstancia.Version = lVersion
            oCampo = FSPMServer.Get_Campo()
            oCampo.Id = idCampo
            oCampo.LoadInst(lCiaComp, lInstancia, Idioma)

            lblTitulo.Text = Textos(0)
            lblTituloData.Text = oCampo.DenSolicitud(Idioma)

            If oCampo.DenGrupo(Idioma) = oCampo.DenGrupo(Idioma) Then
                lblSubTitulo.Text = oCampo.DenGrupo(Idioma)
            Else
                lblSubTitulo.Text = oCampo.DenGrupo(Idioma) + ";" + oCampo.Den(Idioma)
            End If
            cmdCalcular.Value = Textos(3)

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPM", "<script>var rutaPM = '" & ConfigurationManager.AppSettings("rutaPM") & "' </script>")

            InsertarCalendario()
            cmdGuardar.Text = Textos(2)
            cmdGuardar.Attributes.Add("onclick", "return actualizarCampoDesgloseYCierre()")

            Me.cmdCalcular.Visible = False
            If Request("BotonCalcular") <> Nothing Then
                If Request("BotonCalcular") = 1 Then
                    Me.cmdCalcular.Visible = True
                End If
            End If


            Dim oNoConformidad As Fullstep.PMPortalServer.NoConformidad = FSPMServer.Get_NoConformidad
            oNoConformidad.Id = Request("NoConformidad")
            oNoConformidad.Prove = FSPMUser.CodProve
            oNoConformidad.Version = lVersion
            oNoConformidad.Load(lCiaComp, Idioma)

            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.NoConformidad

            Dim oAcciones As DataTable
            Dim oDSEstados As DataSet

            If oCampo.Tipo = Fullstep.PMPortalServer.TipoCampoPredefinido.NoConformidad Then
                oAcciones = oNoConformidad.CargarBotonesAprobarRechazarAcciones(Textos(15), Textos(16))
                oDSEstados = oNoConformidad.CargarEstadosComentariosInternosAcciones(lCiaComp, Idioma, FSPMUser, Textos(19), Textos(20))
            End If

            Dim oucdesglose As Fullstep.PMPortalWeb.desgloseControl
            oucdesglose = Me.FindControl("desglose")
            With oucdesglose
                .Acciones = oAcciones
                .DSEstados = oDSEstados
                .Version = lVersion
                .EsQA = True
                .TipoSolicitud = oCampo.TipoSolicitud
                .InstanciaMoneda = oCampo.InstanciaMoneda
            End With
        End Sub
        Private Sub InsertarCalendario()
            Dim SharedCalendar As New Infragistics.WebUI.WebSchedule.WebCalendar
            With SharedCalendar
                .ClientSideEvents.InitializeCalendar = "initCalendarEvent"
                .ClientSideEvents.DateClicked = "calendarDateClickedEvent"
                With .Layout
                    .NextMonthImageUrl = "ig_cal_grayN.gif"
                    .ShowTitle = "False"
                    .PrevMonthImageUrl = "ig_cal_grayP.gif"
                    .FooterFormat = "Today: {0:d}"
                    .FooterStyle.CssClass = "igCalendarFooterStyle"
                    .TodayDayStyle.CssClass = "igCalendarSelectedDayStyle"
                    .OtherMonthDayStyle.CssClass = "igCalendarOtherMonthDayStyle"
                    .CalendarStyle.CssClass = "igCalendarStyle"
                    .TodayDayStyle.CssClass = "igCalendarTodayDayStyle"
                    .DayHeaderStyle.CssClass = "igCalendarDayHeaderStyle"
                    .TitleStyle.CssClass = "igCalendarTitleStyle"
                End With
            End With
            Me.FindControl("frmAlta").Controls.Add(SharedCalendar)
        End Sub
    End Class
End Namespace