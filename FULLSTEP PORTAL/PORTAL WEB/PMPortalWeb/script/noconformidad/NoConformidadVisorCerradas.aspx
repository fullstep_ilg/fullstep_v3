﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NoConformidadVisorCerradas.aspx.vb" Inherits="Fullstep.PMPortalWeb.NoConformidadVisorCerradas"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
    <title></title>
	<script type="text/javascript" src="../../../common/formatos.js"></script>
	<script type="text/javascript" src="../../../common/menu.asp"></script>
    <script src="../_common/js/AdjacentHTML.js" type="text/javascript"></script>
	<script type="text/javascript">dibujaMenu(2)</script>    
    <script type="text/javascript">
        /*''' <summary>
        ''' Iniciar la pagina.
        ''' </summary>     
        ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
        function Init() {
            if (accesoExterno == 0) {document.getElementById('tablemenu').style.display = 'block';}
        }
    </script>
</head>
<body onload="Init()">
    <script type="text/javascript"><!--
        /*''' <summary>
        ''' Reestablece los filtros sin tocar el grid
        ''' </summary>
        ''' <param name="sender">parametro sistema</param>
        ''' <remarks>Llamada desde: btnLimpiar ; Tiempo máximo: 0 </remarks>*/
        function Limpiar(sender) {
            document.getElementById("<%=txtID.ClientID%>").value = "";

            var dropdown = null;
            dropdown = $find("<%=wddTipoSolicitud.clientID%>");
            if (dropdown != null) {
                document.getElementById("<%=hidTipoSol.ClientID%>").value = "0";

                dropdown.selectItem(dropdown.get_items().getItem(0), true, true);
                dropdown.closeDropDown();
                dropdown.dispose;
            }

            var DataPicker = null;
            DataPicker = $find("<%=WDFecDesde.clientID%>");
            if (DataPicker != null) {
                DataPicker.set_value("", true);
            }
        }
        /*
        ''' <summary>
        ''' Exporta a Excel
        ''' </summary>
        ''' <remarks>Llamada desde: onclick del div q contiene al link excel y a la img excel; Tiempo máximo:0,5</remarks>
        */
        function ExportarExcel() {
            __doPostBack('ExportarExcel', '')
        }
        /*
        ''' <summary>
        ''' Exporta a Pdf
        ''' </summary>
        ''' <remarks>Llamada desde: onclick del div q contiene al link pdf y a la img pdf; Tiempo máximo:0,5</remarks>
        */
        function ExportarPDF() {
            __doPostBack('ExportarPDF', '')
        }


		//''' <summary>
		//''' Responder al click de una columna. Para mostrar el detalle de la no conformidad ó sus comentarios.
		//''' </summary>
        //''' <param name="sender">parametro de sistema</param>
        //''' <param name="e">parametro de sistema</param>     
		//''' <remarks>Llamada desde: sistema; Tiempo máximo:0</remarks>
        function wdgHDNoConfs_CellSelectionChanged(sender, e) {
            var Key = e.getSelectedCells().getItem(0).get_column().get_key();

            var IdNoConform = e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("ID_NOCONF_ENCRYPTED").get_value();
            var IdInstancia = e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("ID_INS_ENCRYPTED").get_value();
		    
		    if (Key == "COMENT") {
		        var Altura = 50
		        var comentarioApertura = e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("RCOMENT_ALTA").get_text()
		        if (comentarioApertura == null) {
		            comentarioApertura = ''
		        }

		        var comentarioCierre = e.getSelectedCells().getItem(0).get_row().get_cellByColumnKey("RCOMENT_CIERRE").get_text()
		        if (comentarioCierre == null) {
		            comentarioCierre = ''
		        }

		        if ((comentarioCierre != '') && (comentarioApertura != ''))
		            Altura = 480
		        else
		            if ((comentarioCierre != '') || (comentarioApertura != ''))
		                Altura = 250

		        if (Altura > 50) {
		            window.open("../_common/popUpComentario.aspx?textoApertura=" + comentarioApertura + "&textoCierre=" + comentarioCierre + "&boton=" + document.getElementById("hCerrar").value, "_blank", "width=450,height=" + Altura + ",status=yes,resizable=no,top=160,left=250")
		        }
		    }
		    else {
                window.open("../noconformidad/extFrames.aspx?noconformidad=" + IdNoConform + "&instancia=" + IdInstancia, "default_main")
		    }
		}

		/*''' <summary>
		''' Evento que salta tras elegir un tipo de solicitud en el combo.
		''' </summary>
		''' <param name="sender">componente input</param>
		''' <param name="eventArgs">evento</param>    
		''' <remarks>Llamada desde=evento del dropdown; Tiempo maximo=0seg.</remarks>*/
		function wddTipoSolicitud_selectedIndexChanged(sender, eventArgs) {
		    document.getElementById("hidTipoSol").value = sender.get_selectedItemIndex()
		}
--></script>

<form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
    </asp:ScriptManager>

	<ig:WebExcelExporter ID="wdgExcelExporter" runat="server"></ig:WebExcelExporter>
	<ig:WebDocumentExporter ID="wdgPDFExporter" runat="server" ExportMode="Download"></ig:WebDocumentExporter>

    <div style="margin-top:10px; clear:both;">
        <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>		
    </div>

    <asp:Panel ID="pnlCabeceraParametros" runat="server">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="PanelCabecera">
            <tr style="cursor: pointer; font-weight: bold; height: 15px; width: 100%;">
                <td valign="middle" align="left" style="padding-top: 5px">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding-top: 2px">
                                <asp:Image ID="imgExpandir" runat="server" SkinID="Expandir" />
                            </td>
                            <td style="vertical-align: top; padding-top: 2px">
                                <asp:Label ID="lblBusqueda" runat="server" Text="DSeleccione los criterios de búsqueda generales"
                                    Font-Bold="True" ForeColor="White"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel ID="pnlParametros" runat="server" CssClass="Rectangulo">
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td>
                    <asp:UpdatePanel ID="upBusqueda" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
						<table id="Table1" style="HEIGHT: 32px" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<tr>
								<td style="WIDTH: 11px"><asp:label id="lblIdent" runat="server" CssClass="captionBlue2">DId:</asp:label></td>
								<td style="WIDTH: 96px"><asp:textbox id="txtID" runat="server" CssClass="input2" Width="80px"></asp:textbox></td>
								<td style="WIDTH: 51px" align="right"><asp:label id="lblTipo" runat="server" CssClass="captionBlue2" Height="16px"></asp:label></td>
								<td style="WIDTH: 204px">
                                    <input id="hidTipoSol" type="hidden" runat="server" />
                                    <ig:WebDropDown ID ="wddTipoSolicitud" runat="server" EnableViewState="false" Height="22px" Width="200px" 
                                        EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" EnableClosingDropDownOnBlur="true">
                                        <ClientEvents SelectionChanged="wddTipoSolicitud_selectedIndexChanged"/>
                                    </ig:WebDropDown> 
                                </td>
								<td style="WIDTH: 54px" align="right"><asp:label id="lblDesde" runat="server" CssClass="captionBlue2"></asp:label></td>
								<td style="WIDTH: 172px">
                                    <ig:WebDatePicker ID="WDFecDesde" runat="server" SkinID="Calendario" Width="172px">
                                    </ig:WebDatePicker>
                                </td>
							</tr>
						</table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="1%">
                        <tr>
                            <td>
                                <fsn:FSNButton ID="btnBuscar" runat="server" Text="DBuscar"></fsn:FSNButton>
                            </td>
                            <td>
                                <fsn:FSNButton ID="btnLimpiar" runat="server" Text="DLimpiar" OnClientClick="Limpiar();return false;"></fsn:FSNButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" CollapseControlID="pnlCabeceraParametros"
        ExpandControlID="pnlCabeceraParametros" ImageControlID="imgExpandir" SuppressPostBack="true"
        TargetControlID="pnlParametros" Collapsed="true" SkinID="FondoRojo">
        </cc1:CollapsiblePanelExtender>

    <div class="CabeceraBotones" style="height:22px; clear:both; line-height:22px;">        
		<div style="float:right;">
			<div onclick="ExportarExcel()" class="botonDerechaCabecera">            
				<asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
				<asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" Text="Excel"></asp:Label>           
			</div>
			<div onclick="ExportarPDF()" class="botonDerechaCabecera">            
				<asp:Image runat="server" ID="imgPDF" CssClass="imagenCabecera" />
				<asp:Label runat="server" ID="lblPDF" CssClass="fntLogin2" Text="PDF"></asp:Label>           
			</div>
		</div>
    </div>        

    <asp:UpdatePanel ID="UpdDatos" ChildrenAsTriggers="false" EnableViewState="false" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="click" />
        </Triggers>
        <ContentTemplate>
            <div class="bordeSimple">
                <ig:WebHierarchicalDataGrid runat="server" ID="wdgHDNoConfs" AutoGenerateColumns="false" Width="100%" 
                ShowHeader="true" Height="700px" DataKeyFields="ID_NOCONF">
                    <Columns>            
                        <ig:BoundDataField Key="ID_NOCONF" DataFieldName="ID_NOCONF" Hidden="true"></ig:BoundDataField>
                        <ig:UnboundField Key="ID_NOCONF_ENCRYPTED" Hidden="true"></ig:UnboundField>
                        <ig:UnboundField Key="ID_INS_ENCRYPTED" Hidden="true"></ig:UnboundField>
                        <ig:BoundDataField Key="CIERRE" DataFieldName="CIERRE" Hidden="true"></ig:BoundDataField>
                        <ig:BoundDataField Key="COMENT_ALTA" DataFieldName="COMENT_ALTA" Hidden="true"></ig:BoundDataField>
                        <ig:BoundDataField Key="COMENT_CIERRE" DataFieldName="COMENT_CIERRE" Hidden="true"></ig:BoundDataField>
                        <ig:BoundDataField Key="RCOMENT_CIERRE" DataFieldName="RCOMENT_CIERRE" Hidden="true"></ig:BoundDataField>
                        <ig:BoundDataField Key="RCOMENT_ALTA" DataFieldName="RCOMENT_ALTA" Hidden="true"></ig:BoundDataField>
                        <ig:BoundDataField Key="COMENT_IMPR" DataFieldName="COMENT_IMPR" Hidden="true"></ig:BoundDataField>
                        <ig:BoundDataField Key="ID_INS" DataFieldName="ID_INS" Width="8%"></ig:BoundDataField>
                        <ig:BoundDataField Key="TIPO_NOCONF" DataFieldName="TIPO_NOCONF" Width="20%"></ig:BoundDataField>
                        <ig:BoundDataField Key="FECALTA" DataFieldName="FECALTA" Width="8%"></ig:BoundDataField>
                        <ig:BoundDataField Key="ARTICULO" DataFieldName="ARTICULO" Width="35%"></ig:BoundDataField>                        
                        <ig:BoundDataField Key="CIERRE_DEN" DataFieldName="CIERRE_DEN" Width="15%"></ig:BoundDataField>
                        <ig:BoundDataField Key="FEC_CIERRE" DataFieldName="FEC_CIERRE" Width="8%"></ig:BoundDataField>
                        <ig:BoundDataField Key="COMENT" DataFieldName="COMENT" CssClass ="ImagenBotonCabeceraCustomControl" Width="6%"></ig:BoundDataField>                        
                    </Columns>
                    <Behaviors>
                        <ig:Selection CellClickAction="Cell" CellSelectType="Single" ColumnSelectType="None" Enabled="true" RowSelectType="None">
                            <SelectionClientEvents CellSelectionChanged="wdgHDNoConfs_CellSelectionChanged" />
                        </ig:Selection>
                        <ig:Filtering Alignment="Top" Enabled="true" Visibility="Visible">
                        </ig:Filtering>
                        <ig:VirtualScrolling Enabled="true" ScrollingMode="Virtual" ThresholdFactor=".25"></ig:VirtualScrolling>
                        <ig:Sorting Enabled="true" SortingMode="Multi"></ig:Sorting>            
                        <ig:ColumnResizing Enabled="true">
                            <ColumnSettings>
                                <ig:ColumnResizeSetting ColumnKey="COMENT" EnableResize="false" />
                             </ColumnSettings>
                        </ig:ColumnResizing>
                        <ig:ColumnMoving Enabled="false"></ig:ColumnMoving>
                    </Behaviors>
                    <GroupingSettings EnableColumnGrouping="True">
                    </GroupingSettings>
                </ig:WebHierarchicalDataGrid>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>        
    <input type="hidden" name="hCerrar" id="hCerrar" runat="server">
</form>
</body>
</html>

