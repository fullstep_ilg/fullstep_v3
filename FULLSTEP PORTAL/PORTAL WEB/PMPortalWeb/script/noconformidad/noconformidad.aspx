<%@ Page Language="vb" AutoEventWireup="false" Codebehind="noconformidad.aspx.vb" Inherits="Fullstep.PMPortalWeb.noconformidad" EnableSessionState="True" enableViewState="False"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>solicitudes</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../../../common/estilo.asp" type="text/css" rel="stylesheet">
    <script src="../../../common/formatos.js"></script>
    <script src="../../../common/menu.asp"></script>
    <script src="/ig_common/20043/scripts/ig_WebGrid.js" type="text/javascript"></script>
    <script src="../_common/js/AdjacentHTML.js" type="text/javascript"></script>
    <script src="../../js/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../js/jquery/jquery.tmpl.min.js" type="text/javascript"></script>
</head>
<body onresize="resize()" onload="init()" onunload="if (wProgreso != null) OcultarEspera();wProgreso=null;">
    <script type="text/javascript">
	    CreateXmlHttp();

        /*''' <summary>
        ''' Crear el objeto para llamar con ajax a ComprobarVersionQa y EliminarVersionProveedor
        ''' </summary>
        ''' <remarks>Llamada desde: javascript ; Tiempo m�ximo: 0</remarks>*/	
	    function CreateXmlHttp() {
	        // Probamos con IE
	        try {
	            // Funcionar� para JavaScript 5.0
	            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	        }
	        catch (e) {
	            try {
	                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	            }
	            catch (oc) {
	                xmlHttp = null;
	            }
	        }
	        // Si no se trataba de un IE, probamos con esto
	        if (!xmlHttp && typeof XMLHttpRequest != "undefined") {
	            xmlHttp = new XMLHttpRequest();
	        }
	        return xmlHttp;
	    }	    
		wProgreso = null;
		/*Descripcion: Le pone un ancho al desglose
		Llamada desde:carga de la pagina
		Tiempo ejecucion:0,1seg.*/
		function resize() {
			for (i=0;i<arrDesgloses.length;i++) {
				sDiv = arrDesgloses[i].replace("tblDesglose","divDesglose")
				if (parseFloat(document.body.offsetWidth) >= 90) {
				    document.getElementById(sDiv).style.width = (parseFloat(document.body.offsetWidth) - 90) + "px"
				}
			}
		}		
		function localEval(s) {
			eval(s)
		}
		/*Descripcion: Muestra los divs que indican que se esta realizando un proceso.
		Llamada desde:=Guardar() // Enviar() 
		Tiempo ejecucion:0,1seg.*/
		function MostrarEspera() {
			wProgreso = true;
			
			document.getElementById("divForm2").style.display='none';
			document.getElementById("divForm3").style.display='none';
			document.getElementById("igtabuwtGrupos").style.display='none';
			
			i=0;
			bSalir = false;
			while (bSalir == false) {
				if (document.getElementById("uwtGrupos_div" + i)) {
					document.getElementById("uwtGrupos_div" + i).style.visibility='hidden';
					i = i+1;
				} else bSalir = true;				
			}
				
			document.getElementById("lblProgreso").value = frmDetalle.cadenaespera.value;
			document.getElementById("divProgreso").style.display='';
		}
		/*Descripcion: Oculta los divs que indican que se esta realizando un proceso y Muestra los grupos.
		Llamada desde:=HabilitarBotones y onunload
		Tiempo ejecucion:0,1seg.*/		
		function OcultarEspera() {
			wProgreso = null;
			
			document.getElementById("divProgreso").style.display='none';
			document.getElementById("divForm2").style.display='';
			document.getElementById("divForm3").style.display='';
			document.getElementById("igtabuwtGrupos").style.display='';
			
			i=0;
			bSalir = false;
			while (bSalir == false) {
				if (document.getElementById("uwtGrupos_div" + i)) {
					document.getElementById("uwtGrupos_div" + i).style.visibility='visible';
					i = i+1;
				} else bSalir = true;					
			}	
			return				
		}
		/* Descripcion:=Monta el formulario para el posterior guardado. (guardarInstancia.aspx)
		Llamada desde:resultadoProcesarAcciones()
		Tiempo ejecucion:1,5seg.*/			
		function MontarSubmitGuardar(iVersion,iInstancia) {
			MontarFormularioSubmit()
			
			/*Recogemos los notificados seleccionados*/
			var chkNotificadosProveedor=document.getElementsByName("chkNotificado");
			var cadenaIdsContactoProveedor="";
			
			for (i=0;i<chkNotificadosProveedor.length;i++){				
				if (chkNotificadosProveedor[i].checked){
					cadenaIdsContactoProveedor=cadenaIdsContactoProveedor + chkNotificadosProveedor[i].value + "#";
				}
			}

			document.forms["frmSubmit"].elements["GEN_NoConfContactosProveedor"].value=cadenaIdsContactoProveedor;			
			document.forms["frmSubmit"].elements["GEN_Enviar"].value = 0
			document.forms["frmSubmit"].elements["GEN_Version"].value = iVersion
			document.forms["frmSubmit"].elements["GEN_Instancia"].value = iInstancia
			document.forms["frmSubmit"].elements["GEN_Accion"].value ="guardarnoconformidad"
			oFrm = MontarFormularioCalculados()
			sInner = oFrm.innerHTML
			oFrm.innerHTML = ""
			document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
			document.forms["frmSubmit"].submit()
		}
		/*''' <summary>
		''' Guardar sin enviar una no conformidad
		''' </summary>   
		''' <remarks>Llamada desde: cmdGuardar ; Tiempo m�ximo: 0,1</remarks>*/		
		function Guardar() {
			if (comprobarTextoLargo()==false)
				return false
						
			var iVersion 
			var iInstancia
			var itipoVersion
			
			//deshabilitamos botones
			if (frmDetalle.cmdGuardar)
				frmDetalle.cmdGuardar.disabled = true;
			if (frmDetalle.cmdEnviar)	
				frmDetalle.cmdEnviar.disabled = true;
			if (frmDetalle.cmdImpExp)	
				frmDetalle.cmdImpExp.disabled = true;
			if (frmDetalle.cmdCalcular)
			    frmDetalle.cmdCalcular.disabled = true;		

            iVersion = document.forms["frmDetalle"].elements["Version"].value
            iInstancia = document.forms["frmDetalle"].elements["Instancia"].value                        			
			
			if (wProgreso == null) {	
				wProgreso = true;
				MostrarEspera();
			}
			
            if (xmlHttp) {  
				itipoVersion = document.forms["frmDetalle"].elements["tipoVersion"].value

                var params = "Instancia=" + iInstancia + "&Version=" + iVersion + "&tipoVersion=" + itipoVersion;
                
                xmlHttp.open("POST", "../_common/controlarVersionQA.aspx", false);
                xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                xmlHttp.send(params);
                
                resultadoProcesarAcciones(false,iInstancia,iVersion);                    
            }  else{
                alert(arrTextosML[1]);                
                HabilitarBotones();               
            }			
            return false;
		}
		/* Descripcion:=Monta el formulario para el posterior envio. (guardarInstancia.aspx)
		Llamada desde:resultadoProcesarAcciones()
		Tiempo ejecucion:1,5seg. */		
		function MontarSubmitEnviar(iVersion,iInstancia) {
			MontarFormularioSubmit()
			
			/*Recogemos los notificados seleccionados*/
			var chkNotificadosProveedor=document.getElementsByName("chkNotificado");
			var cadenaIdsContactoProveedor="";
			
			for (i=0;i<chkNotificadosProveedor.length;i++){				
				if (chkNotificadosProveedor[i].checked){
					cadenaIdsContactoProveedor=cadenaIdsContactoProveedor + chkNotificadosProveedor[i].value + "#";
				}
			}

			document.forms["frmSubmit"].elements["GEN_NoConfContactosProveedor"].value=cadenaIdsContactoProveedor;	
			document.forms["frmSubmit"].elements["GEN_Enviar"].value = 1
			document.forms["frmSubmit"].elements["GEN_Accion"].value ="enviarnoconformidad"
			document.forms["frmSubmit"].elements["GEN_Version"].value = iVersion
			document.forms["frmSubmit"].elements["GEN_Instancia"].value = iInstancia
			oFrm = MontarFormularioCalculados()
			sInner = oFrm.innerHTML
			oFrm.innerHTML = ""
			document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
			document.forms["frmSubmit"].submit()
		}
		/*''' <summary>
		''' Descripci�n:	funci�n que se ejecuta cuando el usuario pulsa el bot�n de Enviar datos
		''' 1� Validar los campos obligatorios del formulario
		''' 2� Obtener la versi�n y la instancia
		''' 3� Deshabilita los botones, muestra la capa de espera y llama a la funci�n MontarSubmitEnviar	
		''' </summary>      
		''' <remarks>Llamada desde: cmdEnviar; Tiempo m�ximo: 0</remarks>*/						
		function Enviar() {
		    var respOblig = comprobarObligatorios()
		    switch (respOblig) {
		        case "":  //no falta ningun campo obligatorio
		            break;
		        case "filas0": //no se han introducido filas en un desglose obligatorio
		            alert(arrTextosML[0])
		            return false
		            break;
		        default: //falta algun campo obligatorio
		            alert(arrTextosML[0] + '\n' + respOblig)
		            return false
		            break;
		    }

			if (comprobarTextoLargo()==false)
				return false

			var iVersion 
			var iInstancia
			var itipoVersion

			iVersion = document.forms["frmDetalle"].elements["Version"].value
			iInstancia = document.forms["frmDetalle"].elements["Instancia"].value						
			
			//deshabilitamos botones
			if (frmDetalle.cmdGuardar)
				frmDetalle.cmdGuardar.disabled = true;
			if (frmDetalle.cmdEnviar)	
				frmDetalle.cmdEnviar.disabled = true;
			if (frmDetalle.cmdImpExp)	
				frmDetalle.cmdImpExp.disabled = true;
			if (frmDetalle.cmdCalcular)	
				frmDetalle.cmdCalcular.disabled = true;	
			
			if (wProgreso == null) {	
				wProgreso = true;
				MostrarEspera()
			}
			
			if (xmlHttp) {           
				itipoVersion = document.forms["frmDetalle"].elements["tipoVersion"].value     
      
                var params = "Instancia=" + iInstancia + "&Version=" + iVersion + "&tipoVersion=" + itipoVersion;
                   
                xmlHttp.open("POST", "../_common/controlarVersionQA.aspx", false);
                xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                xmlHttp.send(params);
                
                resultadoProcesarAcciones(true,iInstancia,iVersion);                    
            } else{
                alert(arrTextosML[1]);                
                HabilitarBotones();               
            }
			return false;
		}	
        /*''' <summary>
        ''' Tras q se ejecute sincronamente controlarVersionQA.aspx controlamos sus resultados y obramos en 
        ''' consecuencia.
        ''' </summary>
        ''' <param name="bParam">envias(1) o guardas (0)</param>  
        ''' <param name="iInstancia">Instancia</param>  
        ''' <param name="iVersion">Version</param>     
        ''' <remarks>Llamada desde:Guardar     Enviar; Tiempo m�ximo: 0</remarks>*/		    
        function resultadoProcesarAcciones(bParam,iInstancia,iVersion) {      	          		
		    var retorno;
		    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {	
		        retorno = xmlHttp.responseText;
		
		        if (retorno == 1){ //en proceso
		            alert(arrTextosML[2])
		            HabilitarBotones();
		            return 
		        } else 		          
		            if (retorno == 2){//No se pueden llevar a cabo las modificaciones ya que mientras editaba la no 
		            //conformidad el peticionario de la misma la ha modificado y ha eliminado sus datos guardados 
		            //sin enviar. Acceda de nuevo a la no conformidad.
		                alert(arrTextosML[3])
		                HabilitarBotones();	                
		                return 		            
		            } else    
		                if (retorno == 3){//Si no es misma versi�n en funci�n del tipo mostraremos un  
		                //alert y el usuario no podr� guardar los cambios. Tipo Proveedor
		                    alert(arrTextosML[4])
		                    HabilitarBotones();	                    
		                    return 		            
		                }		    
		    }
            if (bParam)
			    setTimeout("MontarSubmitEnviar(" + iVersion + "," + iInstancia + ")",100)
	        else    
			    setTimeout("MontarSubmitGuardar(" + iVersion + "," + iInstancia + ")",100)
	    }		
		function CalcularCamposCalculados() {
		    oFrm = MontarFormularioCalculados()
		    oFrm.submit()
		}
	    /*''' <summary>
	    ''' Iniciar la pagina.
	    ''' </summary>     
	    ''' <remarks>Llamada desde: page.onload; Tiempo m�ximo:0</remarks>*/				
		function init() {
		    if (!accesoExterno) document.getElementById('tablemenu').style.display = 'block';
		    resize()
		}
		/*''' <summary>
		''' Evento que salta tras elegir una version en el combo. Carga los datos de la noConformidad en esa version
		''' </summary>
		''' <param name="sender">componente input</param>
		''' <param name="eventArgs">evento</param>    
		''' <remarks>Llamada desde=evento del dropdown; Tiempo maximo=0seg.</remarks>*/
		function wddVersion_selectedIndexChanged(sender, eventArgs) {
		    var aValue = sender.get_selectedItems()[0].get_value().split("##");
		    var iInstancia = aValue[0];
		    var iVersion = aValue[1];
		    var iNoConformidad = aValue[2];
		    var FechaLocal
		    FechaLocal = new Date()
		    otz = FechaLocal.getTimezoneOffset()

		    window.open("noconformidad.aspx?Instancia=" + iInstancia.toString() + "&NoConformidad=" + iNoConformidad.toString() + "&Version=" + iVersion.toString() + "&Otz=" + otz.toString(), "_self")
		}
		/*''' <summary>
		''' Tras grabar los bts deben volver a estar accesibles y desaparecer el recuadro de Cargando...
		''' </summary>
		''' <remarks>Llamada desde: PonerEstado		FinalCarga; Tiempo m�ximo:0</remarks>*/						
		function HabilitarBotones() {	
			if (frmDetalle.cmdGuardar)
				frmDetalle.cmdGuardar.disabled = false;
			if (frmDetalle.cmdEnviar)	
				frmDetalle.cmdEnviar.disabled = false;
			if (frmDetalle.cmdImpExp)
				frmDetalle.cmdImpExp.disabled = false;
			if (frmDetalle.cmdCalcular)
				frmDetalle.cmdCalcular.disabled = false;				
			OcultarEspera();
		}
        /*''' <summary>
		''' Tras un guardado sin enviar no se hace ni submit ni se va a otra pantalla. Eso implica q haya datos q no estan 
		'''	actualizados en pantalla pero si en bbdd. Esta funci�n habilita los botones y oculta la ventana de espera.
		'''	Despues actualiza en pantalla los calculados, por ultimo actualiza el combo de versiones.
		''' </summary>
		''' <param name="estado">Texto multiidioma de guardado sin enviar</param>
		''' <param name="guardadaenportal">Texto multiidioma de guardado sin enviar/param>
		''' <param name="ListaCalc">Lista de pares que contiene los Id de los calculados y su valor ya calculado</param> 		
		''' <remarks>Llamada desde: GuardarInstancia.aspx; Tiempo m�ximo:0,1</remarks>*/		
		function PonerEstado(estado,guardadaenportal,ListaCalc) {			
			document.getElementById("lblSinEnviar").value = estado;

			var iVersion
			if(document.forms["frmDetalle"].elements["tipoVersion"].value=="3"){		
				iVersion = parseInt(document.forms["frmDetalle"].elements["Version"].value);
			} else{
				iVersion = parseInt(document.forms["frmDetalle"].elements["Version"].value) + 1;
			}		
	
			document.forms["frmDetalle"].elements["Version"].value = iVersion;
			document.forms["frmDetalle"].elements["tipoVersion"].value=3;

			HabilitarBotones();	

			var ParListaCalc = "";
			var ListaCalcId = "";
			var ListaCalcVal = "";
			
			while (ListaCalc != "") {
				ParListaCalc= ListaCalc.substring (0,ListaCalc.indexOf ("#"))
				
				ListaCalcId= ParListaCalc.substring (0,ParListaCalc.indexOf ("@"))
				ListaCalcVal= ParListaCalc.substring (ParListaCalc.indexOf ("@")+1)
				
				document.getElementById(ListaCalcId).value = ListaCalcVal;
								
				if (ListaCalc.indexOf ("#"))
					ListaCalc= ListaCalc.substring (ListaCalc.indexOf ("#")+1)
			}

            var combo = $find('<%=wddVersion.ClientID %>');
            if (combo) {
                var item = combo.get_items().getItem(0)
                item.set_text(guardadaenportal);

                combo.set_activeItem(item, true)
                combo.set_visible(true);

                combo.selectItemByIndex(0, true, true)
                item._element.innerText = guardadaenportal
            }

			wProgreso = null;
		}		
		function FinalCarga() {	
			if (frmDetalle.enproceso.value != "1")
			    HabilitarBotones();
			if (arrObligatorios.length > 0)
			    if (document.getElementById("lblCamposObligatorios"))
			        document.getElementById("lblCamposObligatorios").style.display = ""
		}
	    /*''' <summary>
		''' Exportar el certificado 
		''' </summary>
		''' <remarks>Llamada desde: bt cmdImpExp; Tiempo m�ximo:0</remarks>*/				
		function cmdImpExp_onclick() {
		    if(document.forms["frmDetalle"].elements["tipoVersion"].value=="3"){
			    var valor;
			    valor = parseInt(document.forms["frmDetalle"].elements["Version"].value) + 1;
			    valor+='';			
			    window.open('../_common/impexp_sel.aspx?Instancia='+document.getElementById("Instancia").value+'&NoConformidad='+ document.forms["frmDetalle"].elements["NoConformidad"].value+'&Version='+ valor + '&TipoImpExp=3','_new','fullscreen=no,height=115,width=315,location=no,menubar=no,resizable=no,scrollbars=no,status=yes,titlebar=yes,toolbar=no,left=200,top=200');
		    }
		    else{
		        window.open('../_common/impexp_sel.aspx?Instancia=' + document.getElementById("Instancia").value + '&NoConformidad=' + document.forms["frmDetalle"].elements["NoConformidad"].value + '&Version=' + document.forms["frmDetalle"].elements["Version"].value + '&TipoImpExp=3', '_new', 'fullscreen=no,height=115,width=315,location=no,menubar=no,resizable=no,scrollbars=no,status=yes,titlebar=yes,toolbar=no,left=200,top=200');
			}
		}		
        function OcultarNotificaciones() {
            if (document.getElementById("pnlNotificaciones").style.display != "none") {
                document.getElementById("pnlNotificaciones").style.display = "none";
                document.getElementById("imgCollapse").style.display = "none";
                document.getElementById("imgExpand").style.display = "";
            } else {
                document.getElementById("pnlNotificaciones").style.display = "";
                document.getElementById("imgCollapse").style.display = "";
                document.getElementById("imgExpand").style.display = "none";                    
            
            }
            return false;
        }
    </script>
    <script>dibujaMenu(2)</script>
    <form id="frmDetalle" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Name="Common.Common.js" Assembly="AjaxControlToolkit" />
            </Scripts>
        </asp:ScriptManager>
        <input id="Notificar" type="hidden" name="Notificar" runat="server" />
        <input id="tipoVersion" type="hidden" name="tipoVersion" runat="server" />
        <input id="Instancia" type="hidden" name="Instancia" runat="server" />
        <input id="NoConformidad" type="hidden" name="NoConformidad" runat="server" />
        <input id="Version" type="hidden" name="Version" runat="server" />
        <input id="VersionInicialCargada" type="hidden" name="VersionInicialCargada" runat="server" />
        <iframe id="iframeWSServer" style="z-index: 103; position: absolute; visibility: hidden; top: 200px; left: 8px"
            name="iframeWSServer" src="../blank.htm"></iframe>
        <input type="hidden" runat="server" id="diferenciaUTCServidor" name="diferenciaUTCServidor" />
        <table id="tblCabecera" cellspacing="5" cellpadding="1"
            width="100%" border="0">
            <tr>
                <td width="100%">
                    <table class="bordeadoAzul" id="tblCabeceraSolicitud" cellspacing="2" cellpadding="1" width="100%"
                        border="0" runat="server">
                        <tr>
                            <td align="center" colspan="4">
                                <asp:TextBox ID="lblSinEnviar" Style="text-align: center" runat="server" Width="100%" BorderStyle="None"
                                    BorderWidth="0" CssClass="fntRed"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="4">
                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblNombreCerticado" runat="server" Width="100%" CssClass="captionDarkBlue"></asp:Label></td>
                                        <td align="left" width="10%">
                                            <input class="boton" id="cmdCalcular" disabled onclick="return CalcularCamposCalculados();"
                                                type="button" value="DCalcular" name="cmdCalcular" runat="server"><input id="BotonCalcular" type="hidden" value="0" name="BotonCalcular" runat="server"></td>
                                        <td align="right" width="10%">
                                            <input class="boton" id="cmdGuardar" disabled onclick="return Guardar();" type="button"
                                                value="DGuardar" name="cmdGuardar" runat="server"></td>
                                        <td align="right" width="10%">
                                            <input class="boton" id="cmdEnviar" disabled onclick="return Enviar();" type="button" value="Enviar datos"
                                                name="cmdEnviar" runat="server"></td>
                                        <td align="right" width="10%">
                                            <input language="javascript" class="boton" id="cmdImpExp" disabled onclick="return cmdImpExp_onclick()"
                                                type="button" value="Imprimir" name="cmdImpExp" runat="server"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <asp:Label ID="lblId" runat="server" Width="100%" CssClass="captionBlue"></asp:Label></td>
                            <td width="20%">
                                <asp:Label ID="lblSolicitante" runat="server" Width="100%" CssClass="captionBlue"></asp:Label></td>
                            <td width="37%">
                                <asp:Label ID="lblFechaLimite" runat="server" Width="100%" CssClass="captionBlue"></asp:Label></td>
                            <td>
                                <asp:Label ID="lblRespuestas" runat="server" Width="100%" CssClass="captionBlue"></asp:Label></td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <asp:Label ID="txtId" runat="server" Width="100%" CssClass="captionDarkBlue"></asp:Label></td>
                            <td width="20%">
                                <asp:Label ID="txtSolicitante" runat="server" Width="100%" CssClass="captionDarkBlue"></asp:Label></td>
                            <td width="37%">
                                <asp:Label ID="txtFechaLimite" runat="server" Width="100%" CssClass="captionDarkBlue"></asp:Label></td>
                            <td>
                                <ig:WebDropDown ID="wddVersion" runat="server" EnableViewState="false" Height="22px" Width="328px"
                                    EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" EnableClosingDropDownOnBlur="true">
                                    <ClientEvents SelectionChanged="wddVersion_selectedIndexChanged" />
                                </ig:WebDropDown>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblEstado" runat="server" CssClass="captionBlue"></asp:Label>&nbsp;
									<asp:Label ID="txtEstado" runat="server" CssClass="captionDarkBlue"></asp:Label></NOBR></td>
                            <td colspan="2">
                                <asp:Label ID="lblUnidadQA" runat="server" CssClass="captionBlue">DUnidad de Negocio</asp:Label>&nbsp;
									<asp:Label ID="lblUnicoUnidadQA" runat="server" CssClass="captionDarkBlue">DUnidad de Negocio</asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Table ID="tblAcciones" runat="server" Width="100%" CellPadding="0" CellSpacing="0"></asp:Table>
                            </td>
                            <td id="tdFechaApliMejoras">
                                <asp:Label ID="lblFechaApliMejoras" runat="server" style="display:inline-block;width:100%;" CssClass="captionBlue">D_Fecha de aplicaci�n del plan de mejoras_D</asp:Label>
                                <asp:Label ID="txtFechaApliMejoras" runat="server" style="display:inline-block;width:100%;" CssClass="captionDarkBlue">99/99/9999</asp:Label>
                            </td>
                            <td id="tdFechaLimite">

                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">
                                    <tr class="CollapsiblePanel">
                                        <td style="height: 20px">
                                            <asp:ImageButton runat="server" ID="imgExpand"></asp:ImageButton>
                                            <asp:ImageButton runat="server" ID="imgCollapse" Style="display: none"></asp:ImageButton>
                                        </td>
                                        <td style="width: 100%">
                                            <asp:Label runat="server" ID="lblNotificaciones" CssClass="CollapsiblePanelTitle">DNotificaciones</asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="pnlNotificaciones" style="display: none">
                                        <td colspan="2" style="width: 100%">
                                            <asp:Repeater runat="server" ID="rptNotificacionesProveedor">
                                                <HeaderTemplate>
                                                    <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">
                                                        <tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#iif((m_contador mod 3) =0,"</tr><tr> ","") %>
                                                    <td style="width: 33%">
                                                        <input type="checkbox" id="chkNotificado" name="chkNotificado" value="<%#DataBinder.Eval(Container.DataItem, "ID")%>" <%#iif(DataBinder.Eval(Container.DataItem,"SELECCIONADO") = "0"," ","checked") %>>
                                                        <%#DataBinder.Eval(Container.DataItem, "CONTACTO")%>
                                                    </td>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </tr>
														</table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lblCamposObligatorios" Style="z-index: 102; display: none;" runat="server" CssClass="captionRed">Los campos marcados con (*) son de obligada cumplimentacion</asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div id="divProgreso">
            <table id="tblProgreso" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
                <tr style="height: 50px">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:TextBox ID="lblProgreso" Style="text-align: center" runat="server" Width="100%" BorderStyle="None"
                            BorderWidth="0" CssClass="captionBlue">Su solicitud est� siendo tramitada. Espere unos instantes...</asp:TextBox></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" width="100%">
                        <asp:Image ID="imgProgreso" runat="server" src="../_common/images/cursor-espera_grande.gif"></asp:Image></td>
                </tr>
            </table>
        </div>
        <div id="divForm2" style="float: left; width: 100%; display: none;">
            <table cellspacing="3" cellpadding="3" width="100%" border="0">
                <tr>
                    <td width="100%">
                        <igtab:UltraWebTab ID="uwtGrupos" runat="server" Width="100%" BorderStyle="Solid" BorderWidth="1px"
                            DisplayMode="Scrollable" ThreeDEffect="False" DummyTargetUrl=" " FixedLayout="True" CustomRules="padding:10px;"
                            ImageDirectory=" ">
                            <ScrollButtons>
                                <DisabledStyle CssClass="uwtScrollButtons"></DisabledStyle>
                                <Style CssClass="uwtScrollButtons">
									</Style>
                            </ScrollButtons>
                            <DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
                                <Padding Left="20px" Right="20px"></Padding>
                            </DefaultTabStyle>
                            <RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter"></RoundedImage>
                            <Tabs>
                                <igtab:Tab Text="New Tab"></igtab:Tab>
                                <igtab:Tab Text="New Tab"></igtab:Tab>
                            </Tabs>
                        </igtab:UltraWebTab>
                    </td>
                </tr>
            </table>
            <div id="divDropDowns" style="z-index: 103; position: absolute; visibility: hidden; top: 300px"></div>
            <div id="divCalculados" style="z-index: 109; position: absolute; visibility: hidden; top: 0px"
                name="divCalculados">
            </div>
            <div id="divAlta" style="visibility: hidden"></div>
            <input id="cadenaespera" type="hidden" name="cadenaespera" runat="SERVER">
            <input id="enproceso" type="hidden" name="enproceso" runat="SERVER">            
        </div>
    </form>
    <div id="divForm3" style="display: none">
        <form id="frmCalculados" name="frmCalculados" action="../_common/recalcularimportes.aspx" method="post"
            target="fraPMPortalServer">
        </form>
        <form id="frmDesglose" name="frmDesglose" method="post" target="winDesglose"></form>
    </div>
    <script>FinalCarga();</script>
    <script type="text/javascript">  
        $(document).ready(function () {           
            if (esNoconfEscalacion) {
                $('#tblAcciones').parent().prop('colspan', '2');
                $('#lblFechaLimite').appendTo($('#tdFechaLimite'));
                $('#txtFechaLimite').appendTo($('#tdFechaLimite'));
            }
            else
            {
                $('#tdFechaLimite').hide();
                $('#tdFechaApliMejoras').hide();
            }
        })
     </script>

</body>
</html>
