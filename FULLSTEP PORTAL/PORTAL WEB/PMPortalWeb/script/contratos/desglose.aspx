<%@ Page Language="vb" AutoEventWireup="false" Codebehind="desglose.aspx.vb" Inherits="Fullstep.PMPortalWeb.contrdesglose" %>
<%@ Register TagPrefix="uc2" TagName="desglose" Src="../_common/desglose.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
		<title>desglose</title>
        <style type="text/css">
            html,body,form
            {
                width:99.5%;
                height:100%;
            }
        </style>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script type="text/javascript" src="/ig_common/20043/Scripts/ig_csom.js"></script>
		<script type="text/javascript" src="/ig_common/20043/scripts/ig_WebGrid_dom.js"></script>
		<script type="text/javascript" src="/ig_common/20043/scripts/ig_WebGrid_ie.js"></script>
		<script type="text/javascript" src="/ig_common/20043/scripts/ig_WebGrid_kb.js"></script>
		<script type="text/javascript" src="/ig_common/20043/scripts/ig_WebGrid_ie6.js"></script>
		<script type="text/javascript" src="/ig_common/20043/scripts/ig_WebGrid.js"></script>
        <script src="../../js/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="../../js/jquery/jquery.tmpl.min.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
	    
	
	function actualizarCampoDesgloseYCierre()
	{
		bMensajePorMostrar = document.getElementById("bMensajePorMostrar")

		if (bMensajePorMostrar.value != "1") {
			actualizarCampoDesglose()
			window.close() 		
		}else 
			bMensajePorMostrar.value = "0";
	}

	/* Revisado por: blp. Fecha: 11/10/2011
	''' <summary>
	''' Coge toda la informaci�n de los entrys y la mete/quita de los html de la pantalla q contiene al desglose.
	''' </summary>
	''' <remarks>Llamada desde: actualizarCampoDesgloseYCierre  CalcularDesgloseCalculados; Tiempo m�ximo: 0</remarks>*/
	function actualizarCampoDesglose()
	{
	p = window.opener
	var linea = new Array()
	oDesglose = p.fsGeneralEntry_getById(document.getElementById("INPUTDESGLOSE").value)
	//uwtGrupos__ctl0_147_fsentry2293__numRows

	oTbl = document.getElementById(arrDesgloses[0])
	sPre = arrDesgloses[0].replace("tblDesglose","")
	var iFilas = oTbl.rows.length -1
	var iMaxIndex = document.getElementById(sPre + "numRows").value
	for (i=1;i<=iMaxIndex;i++)
		{
		linea = new Array()
		if (document.getElementById(sPre + i.toString() + "_Deleted") || document.getElementById(sPre + "_" + i.toString() + "_Deleted") )
			{
			    for (oCampo in arrCampos) {
			        s = arrCampos[oCampo]
			        re = /fsentry/
			        while (s.search(re) >= 0) {
			            s = s.replace(re, "fsdsentry_" + i.toString() + "_")
			        }
			        oCampo = fsGeneralEntry_getById(s)

			        if (oCampo) {
			            if (oCampo.tipoGS == 118)
			                linea[linea.length] = s + "_*"
			            else
			                linea[linea.length] = s

			            if (oCampo.tipo == 8) {
			                k = linea.length
			                linea[k] = new Array()
			                linea[k][0] = document.getElementById(oCampo.id + "__hAct").value
			                linea[k][1] = document.getElementById(oCampo.id + "__hNew").value
			                linea[k][2] = oCampo.getValue();
			            }
			            else
			                if (oCampo.tipoGS == 118) {
			                    k = linea.length
			                    linea[k] = new Array()
			                    linea[k][0] = oCampo.getDataValue()
			                    linea[k][1] = oCampo.articuloGenerico;
			                    linea[k][2] = oCampo.DenArticuloModificado;
			                    linea[k][3] = oCampo.codigoArticulo;
			                    if (oCampo.idEntryUNI) {
			                        oUni = fsGeneralEntry_getById(oCampo.idEntryUNI)
			                        if ((!oUni) && (oCampo.UnidadDependent)) { //La Unidad est� oculta
			                            linea[linea.length] = oCampo.idEntryUNI
			                            linea[linea.length] = oCampo.UnidadDependent.value
			                        }
			                    }
			                    oMat = fsGeneralEntry_getById(oCampo.dependentfield)
			                    if (!oMat) //el material est� oculto
			                    {
			                        linea[linea.length] = oCampo.dependentfield
			                        linea[linea.length] = oCampo.Dependent.value
			                    }
			                }
			                else
			                    if ((oCampo.tipoGS == 119) || (oCampo.tipoGS == 104)) {
			                        linea[linea.length] = oCampo.getDataValue()
			                        if (oCampo.tipoGS == 104 || oCampo.tipoGS == 119) {
			                            oMat = fsGeneralEntry_getById(oCampo.dependentfield)
			                            if (!oMat) //el material est� oculto
			                            {
			                                linea[linea.length] = oCampo.dependentfield
			                                linea[linea.length] = oCampo.Dependent.value
			                            }
			                        }
			                        if (oCampo.idEntryUNI) {
			                            oUni = fsGeneralEntry_getById(oCampo.idEntryUNI)
			                            if ((!oUni) && (oCampo.UnidadDependent)) {
			                                linea[linea.length] = oCampo.idEntryUNI
			                                linea[linea.length] = oCampo.UnidadDependent.value
			                            }
			                        }
			                    }
			                    else
			                        if ((oCampo.tipoGS == 0) && (oCampo.intro == 1) && ((oCampo.tipo == 2) || (oCampo.tipo == 3)))
			                            linea[linea.length] = oCampo.getValue()
			                        else
			                            if (oCampo.tipo == 2) {
			                                if (oCampo.getDataValue() != null) {
			                                    if (oCampo.tipoGS == 125) {
			                                        linea[linea.length] = oCampo.getDataValue()
			                                    }
			                                    else
			                                        linea[linea.length] = oCampo.getDataValue().toString().replace(",", ".")
			                                } else
			                                    linea[linea.length] = oCampo.getDataValue()
			                            }
			                            else
			                                if ((oCampo.tipoGS == 0) && (oCampo.intro == 1) && ((oCampo.tipo == 1) || (oCampo.tipo == 5) || (oCampo.tipo == 6))) {
			                                k = linea.length
			                                linea[k] = new Array()
			                                linea[k][0] = oCampo.getDataValue()
			                                linea[k][1] = oCampo.getValue()
			                            }
			                            else {
			                                linea[linea.length] = oCampo.getDataValue()
			                            }

			        }
			    }
			    oDesglose.addDesgloseLine(linea)			    
			}
		else
			{
			for (oCampo in arrCampos)
				{
				s=arrCampos[oCampo]
				re=/fsentry/
				while (s.search(re)>=0)
					{
					s = s.replace(re,"fsdsentry_" + i.toString() + "_")
					}
				oCampo = fsGeneralEntry_getById(s)
				linea[linea.length]=s
				}
			
			oDesglose.removeDesgloseLine(linea)
			}
		
		}
	oNumRows = p.document.getElementById(oDesglose.id + "__numRows")
	oNumRows.value = iFilas
	oNumRows = p.document.getElementById(oDesglose.id + "__numTotRows")
	oNumRows.value = iMaxIndex
	//window.close()
	}
	
	function localEval(s)
	{
	eval(s)
	}
	
	function resize()
		{
		for (i=0;i<arrDesgloses.length;i++)
			{
			sDiv = arrDesgloses[i].replace("tblDesglose","divDesglose")
			document.getElementById(sDiv).style.width = (parseFloat(document.body.offsetWidth) - 40) + "px"
			document.getElementById(sDiv).style.height = (getWindowSize("Height") - 100) + "px"
			
			}
		}
		
	function CalcularDesgloseCalculados()
	{
		actualizarCampoDesglose()

		p = window.opener
		p.CalcularCamposCalculados()
		
	}
		</script>
	</head>
	<body onload="resize()">
        <script src="../_common/js/jsAlta.js?v=<%=ConfigurationManager.AppSettings("versionJs")%>" type="text/javascript"></script>
		<IFRAME id="iframeWSServer" style="Z-INDEX: 103; LEFT: 8px; VISIBILITY: hidden; POSITION: absolute; TOP: 200px"
			name="iframeWSServer" src="../blank.htm"></IFRAME>
		<form id="frmAlta" method="post" runat="server">
		    <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
			<input type="hidden" id="bMensajePorMostrar" name="bMensajePorMostrar" value="0">
			<INPUT id="Instancia" style="Z-INDEX: 101; LEFT: 624px; WIDTH: 128px; POSITION: absolute; TOP: 8px; HEIGHT: 22px"
				type="hidden" size="16" name="Instancia" runat="server">
			<table border="0" width="100%">
				<tr>
					<td width="70%">
						<asp:label id="lblTitulo" style="Z-INDEX: 101" runat="server" CssClass="captionBlue">Alta de solicitud tipo:</asp:label>
						<asp:label id="lblTituloData" runat="server" CssClass="captionDarkBlue" Width="488px">Alta de solicitud tipo:</asp:label>
					</td>
					<TD align="right"><INPUT class="boton" id="cmdCalcular" onclick="CalcularDesgloseCalculados()" type="button"
							value="Calcular" name="cmdCalcular" runat="server"></TD>
					<td align="right">
						<input class="boton" id="cmdGuardar" type="button" value="Button" runat="server" NAME="cmdGuardar"></td>
					</TD>
				</tr>
				<tr>
					<td colspan="3">
						<asp:label id="lblSubTitulo" style="Z-INDEX: 102" runat="server" CssClass="captionBlue">Grupo: Nombre del campo</asp:label>
					</td>
					<TD></TD>
				</tr>
				<tr>
					<td colspan="3">
						<uc2:desglose id="Desglose" runat="server"></uc2:desglose>
					</td>
					<TD></TD>
				</tr>
			</table>
			<div id="divDropDowns" style="Z-INDEX: 101;VISIBILITY: hidden;POSITION: absolute;TOP: 0px">
			</div>
			<input type="hidden" name="INPUTDESGLOSE" id="INPUTDESGLOSE" runat="server">
            <script type="text/javascript">
                if (window.onresize) {
                    window.onresize = resize;
                }
                else {
                    document.onresize = resize;
                }
            </script>
		</form>
	</body>
</html>
