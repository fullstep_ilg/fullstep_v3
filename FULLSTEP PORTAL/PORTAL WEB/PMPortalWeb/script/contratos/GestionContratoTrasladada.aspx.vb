﻿Imports Fullstep
Imports Fullstep.FSNWebControls

Namespace Fullstep.PMPortalWeb

    Partial Public Class GestionContratoTrasladada
        Inherits FSPMPage


#Region " Web Form Designer Generated Code "



        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private _oInstancia As PMPortalServer.Instancia
        ''' <summary>
        ''' Devolver el Id de la compania
        ''' </summary>
        ''' <returns>Id de la compania</returns>
        ''' <remarks>Llamada desde: property oSolicitud property oInstancia property oCertificado; Tiempo máximo:0</remarks>
        Protected ReadOnly Property lCiaComp As Long
            Get
                lCiaComp = IdCiaComp
            End Get
        End Property
        Protected ReadOnly Property oInstancia() As PMPortalServer.Instancia
            Get
                If _oInstancia Is Nothing Then
                    If Me.IsPostBack Then
                        _oInstancia = CType(Cache("oInstancia" & FSPMUser.Cod), PMPortalServer.Instancia)
                    Else
                        _oInstancia = FSPMServer.Get_Instancia
                        _oInstancia.ID = Request("Instancia")

                        'Carga los datos de la instancia:
                        _oInstancia.Load(lCiaComp, Idioma)
                        _oInstancia.Solicitud.Load(lCiaComp, Idioma)
                        _oInstancia.CargarCamposInstancia(lCiaComp, FSPMUser.IdCia, Idioma, , FSPMUser.CodProveGS, , True)
                        _oInstancia.DevolverEtapaActualTraslado(lCiaComp, Idioma)
                        Me.InsertarEnCache("oInstancia" & FSPMUser.Cod, _oInstancia)
                    End If
                End If
                Return _oInstancia
            End Get
        End Property

        Private _bSoloLectura As Boolean = False
        Protected Property bSoloLectura() As Boolean
            Get
                ViewState("bSoloLectura") = _bSoloLectura
                Return ViewState("bSoloLectura")
            End Get
            Set(ByVal value As Boolean)
                _bSoloLectura = value
            End Set
        End Property

        Private _oContrato As PMPortalServer.Contrato
        Protected ReadOnly Property oContrato() As PMPortalServer.Contrato
            Get
                If _oContrato Is Nothing Then
                    If Me.IsPostBack Then
                        _oContrato = CType(Cache("oContrato" & FSPMUser.Cod), PMPortalServer.Contrato)
                    Else
                        _oContrato = FSPMServer.Get_Contrato
                        _oContrato.ID = Request("Contrato")
                        hid_Contrato.Value = Request("Contrato")
                        _oContrato.Codigo = Request("Codigo")
                        hid_CodContrato.Value = Request("Codigo")
                        'Carga los datos del contrato:
                        _oContrato.Load(lCiaComp, Idioma)
                        hid_CodMoneda.Value = _oContrato.CodMoneda
                        Me.InsertarEnCache("oContrato" & FSPMUser.Cod, _oContrato)
                    End If
                End If
                Return _oContrato
            End Get
        End Property

#End Region

        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf &
    "<script language=""{0}"">{1}</script>"

        Private arrOrdenEstados(7) As String
        Private arrTextosEstados(7) As String

        ''' <summary>
        '''Muestra la pagina de que la instancia ha sido trasladada
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">evento de sistema</param>           
        ''' <remarks>Tiempo máximo: 0,3</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Response.Expires = -1

            CargarTextos()
            RegistrarScripts()

            If Page.IsPostBack Then Exit Sub

            Me.Bloque.Value = oInstancia.Etapa
            Me.Version.Value = oInstancia.Version
            Me.Instancia.Value = oInstancia.ID
            Me.txtPeticionario.Value = oInstancia.Peticionario
            Me.txtIdTipo.Value = oInstancia.Solicitud.ID
            Me.txtComent.Text = oInstancia.ComentarioEstado


            VisualizarOpcionesTraslado()
            VisualizarLinkFlujo()
            MostrarImporteContrato()

            CrearCamposDinamicos()


            CargarDatosContrato()
            ConfigurarCabecera()

            Me.FindControl("frmDetalle").Controls.Add(CommonAlta.InsertarCalendario(Me.FSPMUser.RefCultural))

        End Sub

        ''' <summary>
        ''' Configura la cabecera, muestra en el menu las posibles acciones
        ''' Activamos los botones que pueden aparecer en la pantalla
        ''' </summary>
        ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0,1seg.</remarks>
        Private Sub ConfigurarCabecera()

            FSNPageHeader.TextoBotonCalcular = Textos(17)
            FSNPageHeader.TextoBotonGuardar = Textos(19)
            FSNPageHeader.TextoBotonImpExp = Textos(20)
            FSNPageHeader.TextoBotonVolver = Textos(21)
            FSNPageHeader.TextoBotonEliminar = Textos(22)
            FSNPageHeader.TextoBotonTrasladar = Textos(31)
            FSNPageHeader.TextoBotonDevolver = Textos(32)


            FSNPageHeader.VisibleBotonImpExp = False
            FSNPageHeader.VisibleBotonGuardar = True
            FSNPageHeader.VisibleBotonTrasladar = False
            FSNPageHeader.VisibleBotonDevolver = True
            FSNPageHeader.VisibleBotonVolver = False

            FSNPageHeader.OnClientClickCalcular = "return CalcularCamposCalculados();return false;"
            FSNPageHeader.OnClientClickImpExp = "return cmdImpExp_onclick();return false;"
            FSNPageHeader.OnClientClickGuardar = "return Guardar();return false;"
            FSNPageHeader.OnClientClickTrasladar = "return Trasladar();return false;"
            FSNPageHeader.OnClientClickDevolver = "return Devolver();return false;"
            FSNPageHeader.OnClientClickVolver = "return Volver();return false;"

            mi_body.Attributes.Add("onload", "inicializar();")
            mi_body.Attributes.Add("onunload", "finalizar();")

            FSNPageHeader.TituloCabecera = oInstancia.Solicitud.Den(Idioma)
            FSNPageHeader.UrlImagenCabecera = "images/Contrato.png"

        End Sub

        ''' <summary>
        ''' Muestra las opciones de traslado
        ''' </summary>
        ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,2seg</remarks>
        Private Sub VisualizarOpcionesTraslado()
            If oInstancia.DestinatarioEst = FSPMUser.CodProve Then
                'Le han trasladado la solicitud y la tiene que cumplimentar:
                oInstancia.DevolverDatosTraslado(lCiaComp, FSPMUser.CodProveGS)
                HyperDetalle.Visible = False
                txtTraslado.Value = oInstancia.PersonaEst
            Else 'Es una solicitud trasladada a otra persona, no podrá hacer nada:
                oInstancia.DevolverDatosTraslado(lCiaComp, FSPMUser.CodProveGS)
                FSNPageHeader.VisibleBotonDevolver = False
                FSNPageHeader.VisibleBotonGuardar = False
                FSNPageHeader.VisibleBotonTrasladar = False
                FSNPageHeader.VisibleBotonCalcular = False
                txtTraslado.Value = oInstancia.DestinatarioEst
                bSoloLectura = True
            End If
        End Sub
        ''' <summary>
        ''' Nos muestra o no el link para poder ver el flujo por el que ha ido la instancia
        ''' </summary>
        ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,seg.</remarks>
        Private Sub VisualizarLinkFlujo()
            'hipervínculo para el detalle del workflow
            If oInstancia.Solicitud.Workflow = Nothing Then  'Si no hay workflow no muestra el hipervínculo
                Me.HyperDetalle.Visible = False
            Else
                If oInstancia.VerDetalleFlujo = True Then
                    HyperDetalle.Visible = True
                    Me.HyperDetalle.Attributes.Add("onclick", "javascript:window.open('../solicitudes/comentariosSolicitud.aspx?Instancia=" + CStr(oInstancia.ID) + "&Codigo=" & oContrato.Codigo & "','_blank', 'width=1000,height=560,status=yes,resizable=no,top=200,left=200')")
                Else
                    Me.HyperDetalle.Visible = False
                End If
            End If
        End Sub
        ''' <summary>
        ''' Registra las variables que vamos a utilizar en javascript
        ''' </summary>
        ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,1seg.</remarks>
        Private Sub RegistrarScripts()
            Dim sClientTextVars As String
            sClientTextVars = ""

            sClientTextVars += "vdecimalfmt='" + FSPMUser.DecimalFmt + "';"
            sClientTextVars += "vthousanfmt='" + FSPMUser.ThousanFmt + "';"
            sClientTextVars += "vprecisionfmt='" + FSPMUser.PrecisionFmt + "';"
            sClientTextVars = String.Format(IncludeScriptKeyFormat, "javascript", sClientTextVars)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "VarsUser", sClientTextVars)

            Dim sClientTexts As String
            sClientTexts = ""
            sClientTexts += "var arrTextosML = new Array();"
            sClientTexts += "arrTextosML[0] = '" + JSText(Textos(28)) + "';" '"Se detectó un contenido potencialmente peligroso. Por favor modifique el siguiente contenido: $$$"
            sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

            Dim ilGMN1 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
            Dim ilGMN2 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
            Dim ilGMN3 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
            Dim ilGMN4 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN4
            Dim sScript As String
            sScript = ""
            sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
            sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
            sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
            sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)

            If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
            End If

        End Sub
        Public Sub AplicarEstilosTab(ByRef oTab As Infragistics.WebUI.UltraWebTab.UltraWebTab)

            oTab.ImageDirectory = ConfigurationManager.AppSettings.Get("rutanormal") & "/App_Themes/" & Me.Page.Theme & "/images/"

            oTab.BorderColor = System.Drawing.Color.FromName(ObtenerValorPropiedad("#uwtDefaultTab", "border-color"))

        End Sub

        Private Function ObtenerValorPropiedad(ByVal sBloque As String, ByVal sProp As String) As String

            Dim sPath As String

            sPath = HttpContext.Current.Server.MapPath("../../App_Themes/" & Me.Page.Theme)

            Dim oFile As New System.IO.StreamReader(sPath & "\" & Me.Page.Theme & ".css")

            Dim sStyle As String = oFile.ReadToEnd()

            oFile.Close()
            oFile = Nothing

            Dim lIndex As Long
            Dim sAux As String

            lIndex = sStyle.IndexOf(sBloque)
            sAux = sStyle.Substring(lIndex, sStyle.Length - lIndex)
            lIndex = sAux.IndexOf("}")
            sAux = sAux.Substring(0, lIndex)

            lIndex = sAux.IndexOf(sProp)
            sAux = sAux.Substring(lIndex, sAux.Length - lIndex)
            lIndex = sAux.IndexOf(";")
            sAux = sAux.Substring(sAux.IndexOf(":") + 1, lIndex - sAux.IndexOf(":") - 1)

            Return sAux
        End Function



        ''' <summary>
        ''' Carga los campos de la instancia
        ''' </summary>
        ''' <remarks>Llamada desde:Page_Load; Tiempo máximo:(Depende de los campos de la isntancia)</remarks>
        Private Sub CrearCamposDinamicos()
            Dim oRow As DataRow
            Dim lIndex As Integer = 0
            Dim oInputHidden As System.Web.UI.HtmlControls.HtmlInputHidden
            Dim oDS As DataSet
            Dim oucCampos As PMPortalWeb.campos
            Dim oucDesglose As PMPortalWeb.desgloseControl

            Dim oGrupo As PMPortalServer.Grupo
            Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab

            'Rellena el tab :
            uwtGrupos.Tabs.Clear()
            AplicarEstilosTab(uwtGrupos)

            If Not oInstancia.Grupos.Grupos Is Nothing Then
                'Compruebo si hay que hacer visible el boton de calcular
                'Hacer visible el boton de calcular
                FSNPageHeader.VisibleBotonCalcular = oInstancia.Grupos.Grupos.OfType(Of PMPortalServer.Grupo).Where(Function(x) x.DSCampos.Tables(0).Rows.OfType(Of DataRow).Where(Function(y) y("TIPO") = TipoCampoPredefinido.Calculado).Any).Any()

                For Each oGrupo In oInstancia.Grupos.Grupos
                    oInputHidden = New System.Web.UI.HtmlControls.HtmlInputHidden

                    oInputHidden.ID = "txtPre_" + lIndex.ToString
                    oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
                    Dim Font As String = ObtenerValorPropiedad(".uwtDefaultTab", "font-family")
                    Dim Size As String = Replace(ObtenerValorPropiedad(".uwtDefaultTab", "font-size"), "pt", "")
                    oTabItem.Text = AjustarAnchoTextoPixels(oGrupo.Den(Idioma), AnchoDeTab, IIf(Font = "", "verdana", Font), IIf(Size = "", 8, CInt(Size)), False)
                    oTabItem.Key = lIndex.ToString
                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                    uwtGrupos.Tabs.Add(oTabItem)
                    lIndex += 1

                    If oGrupo.DSCampos.Tables.Count > 0 Then
                        If oGrupo.NumCampos <= 1 Then
                            For Each oRow In oGrupo.DSCampos.Tables(0).Rows
                                If oRow.Item("VISIBLE") = 1 Then
                                    Exit For
                                End If
                            Next
                            If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Desglose Or oRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoDesglose Then
                                If oRow.Item("VISIBLE") = 0 Then
                                    uwtGrupos.Tabs.Remove(oTabItem)
                                Else
                                    oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                                    oTabItem.ContentPane.UserControlUrl = "..\_common\desglose.ascx"
                                    oucDesglose = oTabItem.ContentPane.UserControl
                                    oucDesglose.ID = oGrupo.Id.ToString
                                    oucDesglose.Campo = oRow.Item("ID_CAMPO")
                                    oucDesglose.TabContainer = uwtGrupos.ClientID
                                    oucDesglose.Instancia = oInstancia.ID
                                    oucDesglose.Ayuda = DBNullToSomething(oRow.Item("AYUDA_" & Idioma))
                                    oucDesglose.TieneIdCampo = True
                                    oucDesglose.Version = oInstancia.Version
                                    oucDesglose.SoloLectura = bSoloLectura Or (oRow.Item("ESCRITURA") = 0)
                                    oucDesglose.PM = True
                                    oucDesglose.Titulo = DBNullToSomething(oRow.Item("DEN_" & Idioma.ToString))
                                    oucDesglose.InstanciaMoneda = oInstancia.Moneda
                                    oucDesglose.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                                    oInputHidden.Value = oucDesglose.ClientID
                                End If
                            Else
                                oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                                oTabItem.ContentPane.UserControlUrl = "..\_common\campos.ascx"
                                oucCampos = oTabItem.ContentPane.UserControl
                                oucCampos.Instancia = oInstancia.ID
                                oucCampos.ID = oGrupo.Id.ToString
                                oucCampos.dsCampos = oGrupo.DSCampos
                                oucCampos.IdGrupo = oGrupo.Id
                                oucCampos.Idi = Idioma
                                oucCampos.TabContainer = uwtGrupos.ClientID
                                oucCampos.Version = oInstancia.Version
                                oucCampos.SoloLectura = bSoloLectura
                                oucCampos.PM = True
                                oucCampos.InstanciaMoneda = oInstancia.Moneda
                                oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                                oucCampos.Formulario = oInstancia.Solicitud.Formulario
                                oucCampos.ObjInstancia = oInstancia
                                oInputHidden.Value = oucCampos.ClientID
                            End If

                        Else
                            oTabItem.ContentPane.Scrollable = Infragistics.WebUI.UltraWebTab.OverflowType.Visible
                            oTabItem.ContentPane.UserControlUrl = "..\_common\campos.ascx"
                            oucCampos = oTabItem.ContentPane.UserControl
                            oucCampos.Instancia = oInstancia.ID
                            oucCampos.ID = oGrupo.Id.ToString
                            oucCampos.dsCampos = oGrupo.DSCampos
                            oucCampos.IdGrupo = oGrupo.Id
                            oucCampos.Idi = Idioma
                            oucCampos.TabContainer = uwtGrupos.ClientID
                            oucCampos.Version = oInstancia.Version
                            oucCampos.SoloLectura = bSoloLectura
                            oucCampos.PM = True
                            oucCampos.InstanciaMoneda = oInstancia.Moneda
                            oucCampos.TipoSolicitud = oInstancia.Solicitud.TipoSolicit
                            oucCampos.Formulario = oInstancia.Solicitud.Formulario
                            oucCampos.ObjInstancia = oInstancia
                            oInputHidden.Value = oucCampos.ClientID
                        End If
                    End If
                    Me.FindControl("frmDetalle").Controls.Add(oInputHidden)

                Next
            End If
        End Sub

        ''' <summary>
        ''' Carga el idioma correspondiente el los controles
        ''' </summary>
        ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
        Private Sub CargarTextos()

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.GestionContratoTraslado

            lblLitFechaInicio.Text = Textos(0) & ":"  '"Fecha de inicio"
            lblLitFechaExpiracion.Text = Textos(1) & ":" '"Fecha de expiración"
            'lblLitProveedor.Text = "(*) " & Textos(2) '"Proveedor"
            lblLitContacto.Text = Textos(3) & ":"  '"Contacto"
            lblLitEmpresa.Text = Textos(4) & ":"  '"Empresa"
            lblLitMoneda.Text = Textos(5) & ":" '"Moneda"


            Me.cadenaespera.Value = Textos(23) '"Su solicitud está siendo tramitada. Espere unos instantes..."
            Me.lblProgreso.Text = Textos(24) '"Se está cargando la solicitud. Espere unos instantes..."
            HyperDetalle.Text = Textos(30)

            arrOrdenEstados(0) = TiposDeDatos.EstadosVisorContratos.Guardado
            arrOrdenEstados(1) = TiposDeDatos.EstadosVisorContratos.En_Curso_De_Aprobacion
            arrOrdenEstados(2) = TiposDeDatos.EstadosVisorContratos.Vigentes
            arrOrdenEstados(3) = TiposDeDatos.EstadosVisorContratos.Proximo_a_Expirar
            arrOrdenEstados(4) = TiposDeDatos.EstadosVisorContratos.Expirados
            arrOrdenEstados(5) = TiposDeDatos.EstadosVisorContratos.Rechazados
            arrOrdenEstados(6) = TiposDeDatos.EstadosVisorContratos.Anulados

            arrTextosEstados(0) = Textos(33) '"Guardados"
            arrTextosEstados(1) = Textos(34) '"En curso de aprobaciÃƒÂ³n"
            arrTextosEstados(2) = Textos(35) '"Vigentes"
            arrTextosEstados(3) = Textos(36) '"proximo a expirar"
            arrTextosEstados(4) = Textos(37) '"Expirados"
            arrTextosEstados(5) = Textos(38) '"Rechazados"
            arrTextosEstados(6) = Textos(39) '"Anulados"

            'Textos Panel info
            FSNPanelDatosPeticionario.Titulo = Textos(41) '"Peticionario"
            FSNPanelDatosPeticionario.SubTitulo = Textos(40) '"Informacion detallada"
        End Sub

        ''' <summary>
        ''' Carga la informacion del contrato en los campos
        ''' </summary>
        ''' <remarks>Llamada desde:page_load; Tiempo máximo:0,1seg.</remarks>
        Private Sub CargarDatosContrato()
            Dim sEstado As String
            Dim i As Byte
            If oContrato.Estado >= 0 Then
                For i = 0 To UBound(Me.arrOrdenEstados)
                    If oContrato.Estado = arrOrdenEstados(i) Then
                        sEstado = arrTextosEstados(i)
                        Exit For
                    End If
                Next
                If sEstado <> "" Then
                    sEstado = " (" & sEstado & ")"
                End If
            End If

            lblIDInstanciayEstado.Text = oContrato.Codigo & sEstado




            lblFechaInicio.Text = FormatDate(oContrato.FechaInicio, FSPMUser.DateFormat)
            lblProveedor.Text = oContrato.DenProve
            lblContacto.Text = oContrato.Contacto
            If Not oContrato.FechaFin Is Nothing Then
                lblFechaFin.Text = FormatDate(oContrato.FechaFin, FSPMUser.DateFormat)
            Else
                lblFechaFin.Visible = False
            End If
            lblEmpresa.Text = oContrato.Empresa
            lblMoneda.Text = oContrato.CodMoneda & " - " & oContrato.DenMoneda

            imgInfPeticionario.ImageUrl = "./images/info.gif"

            lblPeticionario.Text = oInstancia.NombrePeticionario
            lblFechaCreacion.Text = "(" & FormatDate(oInstancia.FechaAlta, FSPMUser.DateFormat) & ")"

            'Panel info
            imgInfPeticionario.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosPeticionario.AnimationClientID & "', event, '" & FSNPanelDatosPeticionario.DynamicPopulateClientID & "', '" & oInstancia.Peticionario & "'); return false;")
        End Sub

        ''' <summary>
        ''' Muestra o oculta el importe del contrato si se puso un campo de importe en el formulario
        ''' </summary>
        ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0seg.</remarks>
        Private Sub MostrarImporteContrato()
            If Not oInstancia.CampoImporte Is Nothing Then
                lblMoneda.Visible = False
                lblLitMoneda.Visible = False
                Me.lblLitImporte.Text = Textos(16) & ":"
                lblImporte.Text = FSNLibrary.FormatNumber(oInstancia.Importe, FSPMUser.NumberFormat) & " " & oInstancia.Moneda
                lblLitImporte.Visible = True
                lblImporte.Visible = True
            Else
                lblMoneda.Visible = True
                lblLitMoneda.Visible = True
                lblLitImporte.Visible = False
                Me.lblImporte.Visible = False
            End If
        End Sub

    End Class
End Namespace