﻿<%@ Page Language="vb" AutoEventWireup="false" Codebehind="GestionContratoTrasladada.aspx.vb" Inherits="Fullstep.PMPortalWeb.GestionContratoTrasladada" enableViewState="False"%>
<%@ Register TagPrefix ="cc1" Namespace ="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">>
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script src="/ig_common/20043/Scripts/ig_csom.js" type="text/javascript"></script>
		<script src="/ig_common/20043/scripts/ig_WebGrid_dom.js" type="text/javascript"></script>
		<script src="/ig_common/20043/scripts/ig_WebGrid_ie.js" type="text/javascript"></script>
		<script src="/ig_common/20043/scripts/ig_WebGrid_kb.js" type="text/javascript"></script>
		<script src="/ig_common/20043/scripts/ig_WebGrid_ie6.js" type="text/javascript"></script>
		<script src="/ig_common/20043/scripts/ig_WebGrid.js" type="text/javascript"></script>
		<script src="../_common/js/AdjacentHTML.js" type="text/javascript"></script>
		<script src="../../js/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
		<script src="../../js/jquery/jquery.tmpl.min.js" type="text/javascript"></script>
		<script src="../../../common/menu.asp"></script>
		<LINK href="../../../common/estilo.asp" type="text/css" rel="stylesheet">
		<script src="../../../common/formatos.js"></script>
		<style type="text/css">
        html,body,form
        {
            width:99.5%;
            height:100%;
        }
    </style>
		</head>
	<body runat="server" id="mi_body" onresize="resize()">
    <script src="../_common/js/jsAlta.js?v=<%=ConfigurationManager.AppSettings("versionJs")%>" type="text/javascript"></script>
	<script type="text/javascript">
	wbProgreso = null;
	
	//Descripcion:Muestra el contenido peligroso que ha efectuado un error al guardar.
	//Parametros entrada: 
	//contenido: Contenido del error
	//Llamada desde: errores.aspx.vb
	//Tiempo ejecucion:=0seg.
	function ErrorValidacion(contenido) {
		alert(arrTextosML[3].replace("$$$", contenido));	
		OcultarEspera()	    
	}
		
	/*Descripcion: Le pone un ancho al desglose
	  Llamada desde:carga de la pagina
	  Tiempo ejecucion:0,1seg.*/
	function resize()
	{
		for (i=0;i<arrDesgloses.length;i++)
		{
			sDiv = arrDesgloses[i].replace("tblDesglose","divDesglose")
			if (document.getElementById(sDiv))
				document.getElementById(sDiv).style.width = (parseFloat(document.body.offsetWidth) - 95) + "px"
		}
	}
	/*Descripcion: Evalua y ejecuta la instruccion que se le pasa commo parametro
	  parametro:
		s--> Instruccion a evaluar
	  Tiempo ejecucion:0,1seg.*/
	function localEval(s)
	{
	eval(s)
	}
	/*Descripcion: Muestra los divs que indican que se esta realizando un proceso.
	  Llamada desde:=Guardar() // Trasladar() // Devolver()
	  Tiempo ejecucion:0,1seg.*/
	function MostrarEspera()
		{
	        wbProgreso = true;

	        $("[id*='lnkBoton']").attr('disabled', 'disabled');
			
			document.getElementById("divForm2").style.display='none';
			document.getElementById("divForm3").style.display='none';
			document.getElementById("igtabuwtGrupos").style.display='none';
				
			i=0;
			bSalir = false;
			while (bSalir == false)
				{
				if (document.getElementById("uwtGrupos_div" + i))
					{
					document.getElementById("uwtGrupos_div" + i).style.visibility='hidden';
					i = i+1;
					}
				else
					{
					bSalir = true;
					}
				}
			
			document.getElementById("lblProgreso").value = frmDetalle.cadenaespera.value;
			document.getElementById("divProgreso").style.display='inline';	
			
	}

	//Estaba habilitando los botones antes de ocultar el progreso. Parecia q te dejaba dar a varios botones mientras estaba en progreso.
	function DarTiempoAOcultarProgreso() {
	    $("[id*='lnkBoton']").removeAttr('disabled');
	}

	/*Descripcion: Oculta los divs que indican que se esta realizando un proceso y Muestra los grupos.
	  Llamada desde:=HabilitarBotones // Finalizar()
	  Tiempo ejecucion:0,1seg.*/	
	function OcultarEspera()
		{
			wbProgreso = null;
			
			document.getElementById("divProgreso").style.display='none';
			document.getElementById("divForm2").style.display='inline';
			document.getElementById("divForm3").style.display = 'inline';
			document.getElementById("igtabuwtGrupos").style.display = '';

			setTimeout('DarTiempoAOcultarProgreso()', 250);
			
			i=0;
			bSalir = false;
			while (bSalir == false)
				{
				if (document.getElementById("uwtGrupos_div" + i))
					{
					document.getElementById("uwtGrupos_div" + i).style.visibility='visible';
					i = i+1;
					}
				else
					{
					bSalir = true;
					}
				}	
			return	
			
		}
			

	/*
	Descripcion:=Oculta los divs de espera
	Llamada desde:GuardarInstancia // Una vez que se carga la pagina
	Tiempo ejecucion:0,1seg.
	*/
	function HabilitarBotones()
	{	
		OcultarEspera();	             
	}
		

/*
	Descripcion:=Monta el formulario para el posterior guardado. (guardarInstancia.aspx)
	Llamada desde:opcion guardar del menu.
	Tiempo ejecucion:1,5seg.
*/		
function Guardar()
	{	 
		MostrarEspera()		
		setTimeout("MontarSubmitGuardar()",100)
		return false;		             
	}		

/*
	Descripcion:=Monta el formulario para el posterior traslado. (guardarInstancia.aspx)
	Llamada desde:opcion "Trasladar" del menu.
	Tiempo ejecucion:1,5seg.
*/		
function Trasladar()
	{
	MostrarEspera()
	
	setTimeout("MontarSubmitTrasladar()",100)
	return false;	
	}
	
/*
	Descripcion:=Monta el formulario para el posterior devolucion. (guardarInstancia.aspx)
	Llamada desde:opcion "Devolver" del menu.
	Tiempo ejecucion:1,5seg.
*/		
function Devolver()
{
	MostrarEspera()

	setTimeout("MontarSubmitDevolver()",100)
	return false;
}

/*
	Descripcion:=Monta el formulario para el posterior guardado. (guardarInstancia.aspx)
	Llamada desde:Guardar()
	Tiempo ejecucion:1,5seg.
*/	
function MontarSubmitGuardar()
{

	MontarFormularioSubmit(true)

	var frmSubmitElements = document.forms["frmSubmit"].elements;
	frmSubmitElements["GEN_AccionRol"].value = 0
	frmSubmitElements["GEN_Bloque"].value =document.forms["frmDetalle"].elements["Bloque"].value 

	frmSubmitElements["GEN_Enviar"].value = 0
	frmSubmitElements["GEN_Accion"].value ="guardarcontrato"

	
	CompletarFormulario()	
	
	oFrm = MontarFormularioCalculados()
	sInner = oFrm.innerHTML
	oFrm.innerHTML = ""
	document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
	document.forms["frmSubmit"].submit()
	return	false;	

}
	/*
	Descripcion:=Monta el formulario para el posterior devolucion. (guardarInstancia.aspx)
	Llamada desde:Devolver()
	Tiempo ejecucion:1,5seg.
	*/	
	function MontarSubmitDevolver()
		{

		MontarFormularioSubmit(true)
				
		var frmSubmitElements = document.forms["frmSubmit"].elements;
		frmSubmitElements["GEN_AccionRol"].value = 0
		frmSubmitElements["GEN_Bloque"].value =document.forms["frmDetalle"].elements["Bloque"].value 
		frmSubmitElements["GEN_Enviar"].value = 0
		frmSubmitElements["GEN_Accion"].value ="trasladadadevolvercontrato"
		
		CompletarFormulario()
		
		oFrm = MontarFormularioCalculados()
		sInner = oFrm.innerHTML
		oFrm.innerHTML = ""
		document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
		document.forms["frmSubmit"].submit()
		return false;

		}
/*
	Descripcion:=Monta el formulario para el posterior traslado. (guardarInstancia.aspx)
	Llamada desde:Trasladar()
	Tiempo ejecucion:1,5seg.
*/		
function MontarSubmitTrasladar()
{

	MontarFormularioSubmit(true)

	var frmSubmitElements = document.forms["frmSubmit"].elements;
	frmSubmitElements["GEN_Enviar"].value = 0
	frmSubmitElements["GEN_Accion"].value ="trasladadatrasladarcontrato"
	  
	frmSubmitElements["GEN_AccionRol"].value = 0
	frmSubmitElements["GEN_Bloque"].value =document.forms["frmDetalle"].elements["Bloque"].value 

				
	CompletarFormulario()
	
	oFrm = MontarFormularioCalculados()
	sInner = oFrm.innerHTML
	oFrm.innerHTML = ""
	document.forms["frmSubmit"].insertAdjacentHTML("beforeEnd", sInner)
	document.forms["frmSubmit"].submit()
	return

}

/*
	Descripcion:=Monta el formulario para realizar el calculo de los campos.
	Llamada desde:Opcion de menu "Calcular"
	Tiempo ejecucion:1,5seg.
*/		
	function CalcularCamposCalculados()
		{
		oFrm = MontarFormularioCalculados()
		oFrm.submit()
		return false;
		}
		

  /*Descripcion:=Iniciliza los tabs de los grupos
	Llamada desde:evento init de los tabls
	Tiempo ejecucion:0seg.*/
	function initTab(webTab)
	{
	   var cp = document.getElementById(webTab.ID + '_cp');
	   cp.style.minHeight = '300px';
	}	
	
  /*Descripcion:=Llama a la pagina para exportacion de datos
	Llamada desde:Option de menu "Impr./Exp."
	Tiempo ejecucion:0seg.*/      
	function cmdImpExp_onclick() {
		window.open('../seguimiento/impexp_sel.aspx?Instancia='+document.getElementById("Instancia").value + '&TipoImpExp=1&Contrato=' + document.forms["frmDetalle"].elements["hid_Contrato"].value,'_new','fullscreen=no,height=115,width=315,location=no,menubar=no,resizable=no,scrollbars=no,status=yes,titlebar=yes,toolbar=no,left=200,top=200');
		return false;

	}
	
	/*Descripcion:Funcion que oculta o muestra el panel con la informacion de las alertas
				  del contrato.
	Llamada desde:=Click imagenes.
	Tiempo ejecucion:=0seg.*/
	function OcultarAlertas() {
		if (document.getElementById("pnlAlertasTabla").style.display == "block") {
			document.getElementById("pnlAlertasTabla").style.display = "none";
			document.getElementById("imgCollapseAlertas").style.display = "none";
			document.getElementById("imgExpandAlertas").style.display = "block";            
		}else{
			document.getElementById("pnlAlertasTabla").style.display = "block";
			document.getElementById("imgCollapseAlertas").style.display = "block";
			document.getElementById("imgExpandAlertas").style.display = "none"; 
		}
	}	
	
	/*Descripcion:Despues del recalculo de los campos calculados en la pagina recalcularImportes.aspx.
				  se devuelve el valor
	Parametros entrada:=
		importeConFormato: Importe con el formato del usuario
		Importe: Importe (Numerico)
	Llamada desde:=Click Calcular.
	Tiempo ejecucion:=0seg.*/
	function ponerCalculados(importeConFormato, importe) {
		lblImporteV = document.getElementById('<%=lblImporte.clientID %>')
		if (lblImporteV) {
			lblImporteV.innerHTML = "Importe: " + importeConFormato;

		}
	}
	
	  /*Descripcion:=Añade el ID del contrato para el envio de la informacion de los campos a GuardarInstancia.aspx
	Llamada desde:MontarSubmitGuardar // MontarSubmitDevolver // MontarSubmitTrasladar
	Tiempo ejecucion:0seg.*/
	function CompletarFormulario() {
		document.forms["frmSubmit"].elements["ID_CONTRATO"].value = document.forms["frmDetalle"].elements["hid_Contrato"].value
        document.forms["frmSubmit"].elements["COD_CONTRATO"].value = document.forms["frmDetalle"].elements["hid_CodContrato"].value
	}
	
	//''' <summary>
	//''' funcion que inicializa los tabs
	//''' </summary>
	//''' <remarks>Llamada desde:=funcion que se ejecuta al cargar la pagina; Tiempo máximo:0,1</remarks>
	function inicializar()
	{
    document.getElementById('tablemenu').style.display = 'block';
	resize()
	AgruparEstilos()
	}
	
	//''' <summary>
	//''' funcion que inicializa los tabs
	//''' </summary>
	//''' <remarks>Llamada desde:=funcion que se ejecuta al descargar la pagina; Tiempo máximo:0,1</remarks>	
	function finalizar() {
		if (wbProgreso != null) 
			OcultarEspera();
		wbProgreso=null;
	}


//''' <summary>
//''' Vuelve a la pagina de seguimiento
//''' </summary>
//''' <remarks>Llamada desde:Al pinchar en volver; Tiempo máximo:0</remarks>
	function Volver() {
	    window.location = '../solicitudes/seguimientoSolicitudes.aspx';
		return false;
	}
		
	</script>
		<script type="text/javascript" >dibujaMenu(3)</script>
		<form id="frmDetalle" method="post" runat="server">
			<fsn:FSNPanelInfo ID="FSNPanelDatosPeticionario" runat="server" ServicePath="~/Consultas.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="0"></fsn:FSNPanelInfo>

			<asp:ScriptManager ID="ScriptManager1" runat="server">
				<Scripts>
					<asp:ScriptReference Name="ExtenderBase.BaseScripts.js" Assembly="AjaxControlToolkit" />
					<asp:ScriptReference Name="Compat.Timer.Timer.js" Assembly="AjaxControlToolkit" />
					<asp:ScriptReference Name="Animation.Animations.js" Assembly="AjaxControlToolkit" />
					<asp:ScriptReference Name="Animation.AnimationBehavior.js" Assembly="AjaxControlToolkit" />
					<asp:ScriptReference Name="PopupExtender.PopupBehavior.js" Assembly="AjaxControlToolkit" />
					<asp:ScriptReference Name="AutoComplete.AutoCompleteBehavior.js" Assembly="AjaxControlToolkit" />
				</Scripts> 
			</asp:ScriptManager>
			<IFRAME id="iframeWSServer" style="Z-INDEX: 109; LEFT: 8px; VISIBILITY: hidden; POSITION: absolute; TOP: 200px"
				name="iframeWSServer" src="../blank.htm"></IFRAME><INPUT id="Bloque" style="Z-INDEX: 112; LEFT: 8px; POSITION: absolute; TOP: 8px" type="hidden"
				name="Bloque" runat="server"><INPUT id="Instancia" style="Z-INDEX: 102; LEFT: 624px; WIDTH: 128px; POSITION: absolute; TOP: 8px; HEIGHT: 22px"
				type="hidden" size="16" name="Instancia" runat="server"><INPUT id="txtEnviar" style="Z-INDEX: 105; LEFT: 336px; POSITION: absolute; TOP: 16px"
				type="hidden" name="Enviar" runat="server">
				<INPUT id="Version" style="Z-INDEX: 106; LEFT: 336px; POSITION: absolute; TOP: 16px" type="hidden"
				name="Version" runat="server">
				<input id="hid_CodMoneda" type="hidden" runat="server" />
			
			<div>
				<table id="Table1" style="height:15%; width:100%; padding-bottom:15px;" cellspacing="0" cellpadding="1" border="0">
				<tr>
					<td colspan="7">
					<fsn:FSNPageHeader runat="server" ID="FSNPageHeader" UrlImagenCabecera="~/script/contratos/images/calendar-color.png">
					</fsn:FSNPageHeader>
					</td>				    
				</tr>
			 </table>
			 
				
				<div style="padding-left:15px;padding-bottom:15px">
	<asp:Panel ID="pnlContrato" runat="server"  BackColor="#f5f5f5" Font-Names="Arial" Width="95%" > 
		<table id="Table2" style="height:15%; width:100%; padding-bottom:15px;padding-left:5px" cellspacing="0" cellpadding="1" border="0">
		<tr>
			<td style="padding-top:5px; padding-bottom:5px;" class="fondoCabecera">
				<table id="Table3" style="width:100%;table-layout:fixed;padding-left:10px" border="0">
					<tr>
						<td style="width:50%">
							<asp:Label ID="lblIDInstanciayEstado" runat="server" CssClass="label" Font-Bold="true"></asp:Label> 
						</td>
						
						<td  style="width:50%" nowrap="nowrap">		                          	                            
							<asp:Label ID="lblProveedor" runat="server"  CssClass="label" Text="Proveedor" Font-Bold="true"></asp:Label>
						</td>
					</tr>
					<tr>
						<td rowspan="2" nowrap="nowrap">
							<table style="width:100%" border="0">
							<tr>
								<td style="width:120px">
									<asp:label id="lblLitFechaInicio" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px">Fecha de inicio</asp:label>
								</td>
								<td>
									<asp:Label ID="lblFechaInicio" runat="server" CssClass="label" Text="FechaInicio"></asp:Label>
								</td>
								
								<td>
									<asp:label id="lblLitEmpresa" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Text="Empresa"></asp:label>
								</td>
								<td>
									<asp:Label id="lblEmpresa" runat="server" CssClass="label" Text="Empresa"></asp:Label>
								</td>    		                    
							
							</tr>
							<tr>		                    
								<td  nowrap="nowrap" style="width:120px">
									<asp:label id="lblLitFechaExpiracion" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Text="Fecha de expiracion"></asp:label>
								</td>
								<td>
									<asp:Label ID="lblFechaFin" runat="server" CssClass="label" Text="FechaFin"></asp:Label>

								</td>
								
							   <td>
									<asp:label id="lblLitMoneda" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Text="Moneda"></asp:label>
									<asp:Label ID="lblLitImporte" runat="server"  CssClass="captionDarkGraySmall" Font-Size="12px" Visible="false"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblMoneda" runat="server" CssClass="label" Text="Moneda"></asp:Label>
									<asp:Label ID="lblImporte" runat="server" CssClass="label" Visible="false"></asp:Label>

								</td>
							</tr>

							</table>

						</td>
						<td nowrap="nowrap" colspan="2">
							<asp:Label ID="lblLitCreador" runat="server" Text="Creado por:" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label>

							
							<asp:Label ID="lblPeticionario" runat="server" CssClass="label"></asp:Label>
							<asp:Image ID="imgInfPeticionario" runat="server"/>
							<asp:Label ID="lblFechaCreacion" runat="server" Text="(01/01/0000)" CssClass="label"></asp:Label>

						</td>
						
					</tr>
					
					<tr>
						<td colspan="2">
							<asp:label id="lblLitContacto" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px" Text="Contacto"></asp:label>
							
							<asp:Label ID="lblContacto" runat="server" CssClass="label" Text="Contacto"></asp:Label>
						</td>	

					</tr>
				</table>
			</td>      
					   
		</tr>
	</table>
   
		
	<asp:hyperlink id="HyperDetalle" runat="server" Width="100%" CssClass="CaptionLink"></asp:hyperlink>

	</asp:Panel>
  
	</div>
  
				<cc1:DropShadowExtender TrackPosition="true" ID="DropShadowExtender1" runat="server" Opacity="0.5" Width="3" TargetControlID="pnlContrato" Rounded="true"> </cc1:DropShadowExtender>
	
			</div>
			
			<%--Texto traslado--%>
			<TABLE id="coment" cellSpacing="5" cellPadding="1" width="100%" border="0" height="15%">
				<TR height="25%">
					<TD vAlign="middle" colSpan="8" height="100%"><asp:textbox id="txtComent" runat="server" Width="100%" HEIGHT="100%" ReadOnly="True" TextMode="MultiLine"
							BackColor="#E0E0E0"></asp:textbox></TD>
				</TR>
			</TABLE>
			
			
			<div id="divProgreso" style="DISPLAY: inline">
				<table id="tblProgreso" cellSpacing="2" cellPadding="1" width="100%" border="0" runat="server">
					<tr style="HEIGHT: 50px">
						<td>&nbsp;</td>
					</tr>
					<TR>
						<td align="center" width="100%">
							<asp:textbox id="lblProgreso" runat="server" BorderStyle="None" BorderWidth="0" CssClass="captionBlue"
								Width="100%" style="TEXT-ALIGN: center">Su solicitud está siendo tramitada. Espere unos instantes...</asp:textbox>
						</td>
					</TR>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<TR>
						<td align="center" width="100%"><asp:image id="imgProgreso" runat="server" src="../_common/images/cursor-espera_grande.gif"></asp:image></td>
					</TR>
				</table>
			</div>
			<div id="divForm2" style="DISPLAY: none">
				<TABLE cellSpacing="3" cellPadding="1" width="100%" border="0">
					<TR>
						<TD width="100%" colSpan="4">
							<igtab:ultrawebtab id="uwtGrupos" runat="server" Width="100%" BorderStyle="Solid" BorderWidth="1px"
								CustomRules="padding:10px;" FixedLayout="True" DummyTargetUrl=" " ThreeDEffect="False" DisplayMode="Scrollable"
								>
								<DefaultTabStyle Height="24px" CssClass="uwtDefaultTab">
									<Padding Left="20px" Right="20px"></Padding>
								</DefaultTabStyle>
								<ClientSideEvents InitializeTabs="initTab" />
								<RoundedImage NormalImage="ig_tab_blueb2.gif" HoverImage="ig_tab_blueb1.gif" FillStyle="LeftMergedWithCenter"></RoundedImage>
							</igtab:ultrawebtab>
							<script>
								i=0;
								bSalir = false;
								while (bSalir == false)
									{
										if (document.getElementById("uwtGrupos_div" + i))
										{
										document.getElementById("uwtGrupos_div" + i).style.visibility='hidden';
										i = i+1;
										}
									else
										{
										bSalir = true;
										}
									}
							</script>
						</TD>
					</TR>
				</TABLE>
				<INPUT id="txtTraslado" style="Z-INDEX: 108; LEFT: 8px; WIDTH: 128px; POSITION: absolute; TOP: 8px; HEIGHT: 22px"
					type="hidden" size="16" name="CodPer" runat="server"> <INPUT id="txtIdTipo" style="Z-INDEX: 107; LEFT: 8px; POSITION: absolute; TOP: 8px" type="hidden"
					name="txtIdTipo" runat="server"> <INPUT id="txtPeticionario" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 8px"
					type="hidden" name="txtIdTipo" runat="server">
				<div id="divDropDowns" style="Z-INDEX: 103; VISIBILITY: hidden; POSITION: absolute; TOP: 300px"></div>
				<div id="divCalculados" style="Z-INDEX: 110; VISIBILITY: hidden; POSITION: absolute; TOP: 0px"
					name="divCalculados"></div>
				<div id="divAlta" style="VISIBILITY: hidden"></div>
				<input id="cadenaespera" type="hidden" name="cadenaespera" RUNAT="SERVER"> <input id="BotonCalcular" type="hidden" value="0" name="BotonCalcular" runat="server">
				<input id="hid_Contrato" type="hidden" runat="server" />
                <input id="hid_CodContrato" type="hidden" runat="server" />
			</div>
		</form>
		<div id="divForm3" style="DISPLAY: none">
			<form id="frmCalculados" name="frmCalculados" action="../_common/recalcularimportes.aspx?desde=contratos"
				method="post" target="fraPMPortalServer">
			</form>
			<form id="frmDesglose" name="frmDesglose" method="post" target="winDesglose">
			</form>
		</div>
		<script>HabilitarBotones();</script>
	</body>
</html>
