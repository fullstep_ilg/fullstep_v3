Namespace Fullstep.PMPortalWeb
    Partial Class campos
        Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected SharedCalendar As Infragistics.WebUI.WebSchedule.WebCalendar
        Protected WithEvents tblGeneral As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents cmdOk As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents cmdCancel As System.Web.UI.HtmlControls.HtmlInputButton

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
#Region "Properties"
		Private mdsCampos As DataSet
        Private msIdi As String
        Private moTextos As DataTable
        Private mlId As Long
        Private msTabContainer As String
        Private mlInstancia As Long
        'Enviar certificados sin que el cliente haya hecho 'Solicitar certificados'
        Private mlSolicitud As Long
        Private mlVersion As Long
        Private mbSoloLectura As Boolean
        Private moAcciones As DataTable
        Private moDSEstados As DataSet
        Private mlInstanciaEstado As Long
        Private msInstanciaMoneda As String
        Private bPM As Boolean
        Private msNombreContrato As String
        Private mlIdContrato As Long
        Private mlIdArchivoContrato As Long
        Private msTamanyoContrato As String = ""
        Private mTipoSolicitud As TiposDeDatos.TipoDeSolicitud
        Private _Formulario As PMPortalServer.Formulario
        Private _Instancia As PMPortalServer.Instancia

		Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"

        Property Instancia() As Long
            Get
                Return mlInstancia
            End Get
            Set(ByVal Value As Long)
                mlInstancia = Value
            End Set
        End Property
        Property Solicitud() As Long
            Get
                Return mlSolicitud
            End Get
            Set(ByVal Value As Long)
                mlSolicitud = Value
            End Set
        End Property
        Property Version() As Long
            Get
                Return mlVersion
            End Get
            Set(ByVal Value As Long)
                mlVersion = Value
            End Set
        End Property
        Property Acciones() As DataTable
            Get
                Return moAcciones
            End Get
            Set(ByVal Value As DataTable)
                moAcciones = Value
            End Set
        End Property
        Property DSEstados() As DataSet
            Get
                Return moDSEstados
            End Get
            Set(ByVal Value As DataSet)
                moDSEstados = Value
            End Set
        End Property

        Property dsCampos() As DataSet
            Get
                dsCampos = mdsCampos
            End Get
            Set(ByVal Value As DataSet)
                mdsCampos = Value
            End Set
        End Property
        Property Idi() As String
            Get
                Idi = msIdi
            End Get
            Set(ByVal Value As String)
                msIdi = Value
            End Set
        End Property
        Property TabContainer() As String
            Get
                TabContainer = msTabContainer
            End Get
            Set(ByVal Value As String)
                msTabContainer = Value
            End Set
        End Property
        Property IdGrupo() As Long
            Get
                IdGrupo = mlId
            End Get
            Set(ByVal Value As Long)
                mlId = Value
            End Set
        End Property
        Property NombreContrato() As String
            Get
                NombreContrato = msNombreContrato
            End Get
            Set(ByVal Value As String)
                msNombreContrato = Value
            End Set
        End Property
        Property PM() As Boolean
            Get
                Return bPM
            End Get
            Set(ByVal Value As Boolean)
                bPM = Value
            End Set
        End Property

        Property IdContrato() As Long
            Get
                IdContrato = mlIdContrato
            End Get
            Set(ByVal Value As Long)
                mlIdContrato = Value
            End Set
        End Property

        Property IdArchivoContrato() As Long
            Get
                IdArchivoContrato = mlIdArchivoContrato
            End Get
            Set(ByVal Value As Long)
                mlIdArchivoContrato = Value
            End Set
        End Property

        Property TamanyoContrato() As String
            Get
                TamanyoContrato = msTamanyoContrato
            End Get
            Set(ByVal Value As String)
                msTamanyoContrato = Value
            End Set
        End Property

        Property Textos() As DataTable
            Get
                Textos = moTextos
            End Get
            Set(ByVal Value As DataTable)
                moTextos = Value
            End Set
        End Property
        Property SoloLectura() As Boolean
            Get
                Return mbSoloLectura
            End Get
            Set(ByVal Value As Boolean)
                mbSoloLectura = Value

            End Set
        End Property

        Property InstanciaEstado() As Long
            Get
                Return mlInstanciaEstado
            End Get
            Set(ByVal Value As Long)
                mlInstanciaEstado = Value

            End Set
        End Property
        Property InstanciaMoneda() As String
            Get
                Return msInstanciaMoneda
            End Get
            Set(ByVal Value As String)
                msInstanciaMoneda = Value
            End Set
        End Property
        Property TipoSolicitud As TiposDeDatos.TipoDeSolicitud
            Get
                Return mTipoSolicitud
            End Get
            Set(ByVal Value As TiposDeDatos.TipoDeSolicitud)
                mTipoSolicitud = Value
            End Set
        End Property
        Property Formulario As PMPortalServer.Formulario
            Get
                Return _Formulario
            End Get
            Set(value As PMPortalServer.Formulario)
                _Formulario = value
            End Set
        End Property
        Property ObjInstancia As PMPortalServer.Instancia
            Get
                Return _Instancia
            End Get
            Set(value As PMPortalServer.Instancia)
                _Instancia = value
            End Set
        End Property
        Public Sub New(Optional ByVal lId As Long = Nothing, Optional ByRef oDS As DataSet = Nothing, Optional ByVal sIdi As String = Nothing)
            If lId <> Nothing Then
                mdsCampos = oDS
                msIdi = sIdi
                mlId = lId
            End If
        End Sub
#End Region
		''' Revisado por: blp. Fecha: 04/10/2011
		''' <summary>
		'''     Evento de sistema, carga de pantalla.
		'''     NOTA: Ahora aparte del dataentry este tambien inserta el javascript para el datecalendar, pq?
		'''     Pq si resulta q no hay ni un solo entry no se cargaba ese javascript y todos los Fullstep.
		'''     PMPortalWeb.CommonAlta.InsertarCalendario() no funcionaban y dejaban visible el calendario. 
		'''     NOTA: Ahora en 2008 los name q no creamos explicitamente usan $ para separar los trozos de los 
		'''     diferentes controles del q es hijo (ejemplo uwtGrupos$_ctl0x$128903$fsentry646101$_4__646180),
		'''     antes en 2003 se usaba _ y por ello resultaba q el id y el name eran iguales. Esto importa pq 
		'''     los request se hacen por nombre no por id. En 2003 se le pasaba el id y funcionaba, pero ya no. 
		''' </summary>
		''' <param name="sender">parametro de sistema</param>
		''' <param name="e">parametro de sistema</param>            
		''' <remarks>Llamada desde: certificado.aspx  noconformidad.aspx    detalleSolicitud.aspx   solicitudes.aspx;Tiempo m�ximo: 0</remarks>
		Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim otblRow As System.Web.UI.WebControls.TableRow
            Dim otblCell As System.Web.UI.WebControls.TableCell
            Dim oDSRow As System.Data.DataRow
            Dim oDSRowPortal As System.Data.DataRow

            Dim oDSRowAdjun As System.Data.DataRow
            Dim lCiaComp As Long = Session("FS_Portal_User").CiaComp
            Dim oUser As Fullstep.PMPortalServer.User
            Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")

            oUser = Session("FS_Portal_User")
            msIdi = oUser.Idioma

            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSPMServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DesgloseControl, msIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            Dim olabel As System.Web.UI.WebControls.Label
            Dim oimg As System.Web.UI.WebControls.HyperLink
            Dim imgFotos As New System.Web.UI.WebControls.Image

            Dim oFSEntry As Fullstep.DataEntry.GeneralEntry
            Dim sClase As String
            Dim sAdjun As String

			Dim oPaises As Fullstep.PMPortalServer.Paises
            Dim sCodPais As String

            Dim oArts As Fullstep.PMPortalServer.Articulos
            Dim oPres1 As Fullstep.PMPortalServer.PresProyectosNivel1
            Dim oPres2 As Fullstep.PMPortalServer.PresContablesNivel1
            Dim oPres3 As Fullstep.PMPortalServer.PresConceptos3Nivel1
            Dim oPres4 As Fullstep.PMPortalServer.PresConceptos4Nivel1
            Dim oProve As Fullstep.PMPortalServer.Proveedor
            Dim iTipoFecha As Fullstep.PMPortalServer.TiposDeDatos.TipoFecha
            Dim sKeyMaterial As String
            Dim sKeyArticulo As String
            Dim sKeyPais As String
            Dim sKeyProvincia As String
            Dim sKeyAlmacen As String
            Dim sKeyTipoRecepcion As String = "0"

            Dim oGMN1s As Fullstep.PMPortalServer.GruposMatNivel1
            Dim oGMN1 As Fullstep.PMPortalServer.GrupoMatNivel1
            Dim oGMN2 As Fullstep.PMPortalServer.GrupoMatNivel2
            Dim oGMN3 As Fullstep.PMPortalServer.GrupoMatNivel3
            Dim i As Integer

            Dim iNivel As Integer
            Dim lIdPresup As Integer
            Dim dPorcent As Decimal

            Dim sValor As String
            Dim sTexto As String
            Dim sDenominacion As String
            Dim sToolTip As String
            Dim arrMat(4) As String
            Dim oDS As DataSet
            Dim findCampo(0) As Object
            Dim idCampo As Long

            Dim bDepartamentoRelacionado As Boolean
            Dim bCentroRelacionado As Boolean
            Dim bAlmacenRelacionado As Boolean

            Dim sUnidadOrganizativa As String
            Dim sCodOrgCompras As String
            Dim sCodCentro As String
            Dim sIDCampoOrgCompras As String
            Dim sidDataEntryOrgCompras As String
            Dim sIDCampoPartida As String
            Dim sIDCentro As String     'para relacionar con el articulo
            Dim sIdDataEntryCentro As String
            Dim sIdCampoCentro As String

            Dim idEntryPROV, idEntryPREC As String
            Dim bArticuloGenerico As Boolean
            Dim sCodArticulo As String
            Dim nColumna As Integer = 1
            Dim lOrdenOrgCompras, lOrdenCentro As Long
            Dim sKeyUnidadOrganizativa As String
            Dim sKeyOrgComprasDataCheck As String
            Dim sKeyCentro As String
			Dim sIdDataentryMaterial As String = Nothing
			If dsCampos Is Nothing Then
                Dim oCampo As Fullstep.PMPortalServer.Campo = Session("FSWS_Campo")
                If oCampo IsNot Nothing Then
                    mdsCampos = oCampo.Data
                End If
            End If

            Dim arrCamposMaterial(0) As Long

            If Not Page.IsPostBack Then
                mdsCampos.Relations.Add("REL_CAMPO_CAMPO_HIJO", mdsCampos.Tables(0).Columns("CAMPO_HIJO"), mdsCampos.Tables(0).Columns("ID"), False)
                mdsCampos.Relations.Add("REL_CAMPO_CAMPO_PADRE", mdsCampos.Tables(0).Columns("CAMPO_PADRE"), mdsCampos.Tables(0).Columns("ID"), False)
            End If

            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeMaterialObligatorio", "<script>var sMensajeMaterialObligatorio = '" & oTextos.Rows.Item(21).Item(1) & "'</script>")

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechasDefecto") Then
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechasDefecto", "<script>var sMensajeFechaDefecto = '" & oTextos.Rows.Item(13).Item(1) & "'</script>")
            End If
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "FechaNoAnteriorAlSistema") Then
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "FechaNoAnteriorAlSistema", "<script>var sFechaNoAnteriorAlSistema = '" & oTextos.Rows.Item(20).Item(1) & "'</script>")
            End If
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeEliminarArticulosCambioMaterial") Then
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeEliminarArticulosCambioMaterial", "<script>var sMensajeEliminArtMat = '" & JSText(oTextos.Rows.Item(22).Item(1)) & "'</script>")
            End If
            sClase = "ugfilatabla"

            Dim bExisteCampoOrigen As Boolean = False
            Dim dcColumnaCampoOrigen As DataColumn
            dcColumnaCampoOrigen = mdsCampos.Tables(0).Columns("CAMPO_ORIGEN")

            If Not dcColumnaCampoOrigen Is Nothing Then bExisteCampoOrigen = True

			For Each oDSRow In mdsCampos.Tables(0).Rows
				'Guardamos los campos GS ocultos
				Dim sOculto As String
				Dim sOcultoClave As String
				If (oDSRow.Item("VISIBLE") = 0) Then
					If (modUtilidades.DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS"))) Then
						If modUtilidades.DBNullToSomething(oDSRow.Item("VALOR_NUM")) Is Nothing Then
							sOculto = oDSRow.Item("TIPO_CAMPO_GS") & "_" & oDSRow.Item("GRUPO") & "__" & oDSRow.Item("VALOR_TEXT")
						Else
							sOculto = oDSRow.Item("TIPO_CAMPO_GS") & "_" & oDSRow.Item("GRUPO") & "__" & oDSRow.Item("VALOR_NUM")
						End If
						If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
							sOcultoClave = "Ocultos" & oDSRow.Item("ID_CAMPO") & "_" & oDSRow.Item("TIPO_CAMPO_GS") & "_" & oDSRow.Item("GRUPO")
						Else
							sOcultoClave = "Ocultos" & oDSRow.Item("ID") & "_" & oDSRow.Item("TIPO_CAMPO_GS") & "_" & oDSRow.Item("GRUPO")
						End If
						If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), sOcultoClave) Then
							Page.ClientScript.RegisterStartupScript(Page.GetType(), sOcultoClave, "<script>arrGSOcultos[arrGSOcultos.length] = '" & sOculto & "'</script>")
						End If
					End If
				End If

				If modUtilidades.DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Material Then
					ReDim Preserve arrCamposMaterial(UBound(arrCamposMaterial) + 1)
					If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
						sKeyMaterial = "fsentry" + oDSRow.Item("ID_CAMPO").ToString
						arrCamposMaterial(UBound(arrCamposMaterial)) = oDSRow.Item("ID_CAMPO")
					Else
						sKeyMaterial = "fsentry" + oDSRow.Item("ID").ToString
						arrCamposMaterial(UBound(arrCamposMaterial)) = oDSRow.Item("ID")
					End If

				ElseIf (modUtilidades.DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras) And (oDSRow.Item("VISIBLE") = 0) Then
					sCodOrgCompras = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
					lOrdenOrgCompras = DBNullToSomething(oDSRow.Item("ORDEN"))
				ElseIf (modUtilidades.DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Centro) And (oDSRow.Item("VISIBLE") = 0) Then
					sCodCentro = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
					lOrdenCentro = DBNullToSomething(oDSRow.Item("ORDEN"))
				ElseIf modUtilidades.DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS")) = Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
					If Not mdsCampos.Tables(0).Columns("ID_CAMPO") Is Nothing Then
						sKeyArticulo = "fsentry" + oDSRow.Item("ID_CAMPO").ToString
					Else
						sKeyArticulo = "fsentry" + oDSRow.Item("ID").ToString
					End If
				End If

				If (oDSRow.Item("VISIBLE") = 1 And oDSRow.Item("SUBTIPO") <> Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoDesglose) Or (oDSRow.Item("VISIBLE") = 1 And oDSRow.Item("SUBTIPO") = Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoDesglose And oDSRow.Item("POPUP") = 1) Then
					If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
						findCampo(0) = oDSRow.Item("ID_CAMPO")
					Else
						findCampo(0) = oDSRow.Item("ID")
					End If

					If Not mdsCampos.Tables("PORTALCAMPOS") Is Nothing Then
						oDSRowPortal = mdsCampos.Tables("PORTALCAMPOS").Rows.Find(findCampo)

						If Not oDSRowPortal Is Nothing Then
							oDSRow.Item("VALOR_TEXT") = oDSRowPortal.Item("VALOR_TEXT")
							oDSRow.Item("VALOR_NUM") = oDSRowPortal.Item("VALOR_NUM")
							oDSRow.Item("VALOR_FEC") = oDSRowPortal.Item("VALOR_FEC")
							oDSRow.Item("VALOR_BOOL") = oDSRowPortal.Item("VALOR_BOOL")
						End If
					End If
					'Nombre del campo
					otblRow = New System.Web.UI.WebControls.TableRow
					otblCell = New System.Web.UI.WebControls.TableCell
					otblCell.Width = Unit.Percentage(40)
					olabel = New System.Web.UI.WebControls.Label
					If DBNullToSomething(oDSRow.Item("OBLIGATORIO")) = 1 Then
						olabel.Text = "(*)" & "" & oDSRow.Item("DEN_" & msIdi)
						olabel.Style("font-weight") = "bold"
					Else
						olabel.Text = "" & oDSRow.Item("DEN_" & msIdi)
					End If
					otblCell.Controls.Add(olabel)
					otblCell.CssClass = sClase
					otblRow.Cells.Add(otblCell)

					'Icono de ayuda
					otblCell = New System.Web.UI.WebControls.TableCell
					otblCell.Width = Unit.Percentage(1)
					otblCell.Attributes("valign") = "top"

					oimg = New System.Web.UI.WebControls.HyperLink
					oimg.NavigateUrl = "javascript:void(null)"

					imgFotos = New System.Web.UI.WebControls.Image
					imgFotos.ImageUrl = "images\help.gif"
					imgFotos.BorderWidth = Unit.Pixel(0)
					oimg.Controls.Add(imgFotos)

					If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
						oimg.Attributes("onclick") = "show_help(" + oDSRow.Item("ID_CAMPO").ToString + "," + mlInstancia.ToString + ")"
					Else
						oimg.Attributes("onclick") = "show_help(" + oDSRow.Item("ID").ToString + ",null," + mlSolicitud.ToString + ")"
					End If
					otblCell.Controls.Add(oimg)
					otblCell.CssClass = sClase
					otblRow.Cells.Add(otblCell)

					'Campo calculado
					otblCell = New System.Web.UI.WebControls.TableCell
					otblCell.Width = Unit.Percentage(1)
					If oDSRow.Item("TIPO") = 3 Then
						oimg = New System.Web.UI.WebControls.HyperLink

						imgFotos = New System.Web.UI.WebControls.Image
						imgFotos.ImageUrl = "../_common/images/sumatorio.gif"
						imgFotos.BorderWidth = Unit.Pixel(0)
						oimg.Controls.Add(imgFotos)

						oimg.NavigateUrl = "javascript:void(null)"
						If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
							oimg.Attributes("onclick") = "show_help(" + oDSRow.Item("ID_CAMPO").ToString + "," + mlInstancia.ToString + ")"
						Else
							oimg.Attributes("onclick") = "show_help(" + oDSRow.Item("ID").ToString + ",null," + mlSolicitud.ToString + ")"
						End If
						otblCell.Controls.Add(oimg)
					Else
						otblCell.Text = "&nbsp;"
					End If
					otblCell.CssClass = sClase
					otblRow.Cells.Add(otblCell)

					'Valor del campo
					otblCell = New System.Web.UI.WebControls.TableCell
					otblCell.Width = Unit.Percentage(53)
					oFSEntry = New Fullstep.DataEntry.GeneralEntry
					oFSEntry.Portal = True
					oFSEntry.Width = Unit.Percentage(100)

					If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
						oFSEntry.Tag = oDSRow.Item("ID_CAMPO")
						oFSEntry.ID = "fsentry" + oDSRow.Item("ID_CAMPO").ToString
						oFSEntry.CampoDef_CampoODesgHijo = oDSRow.Item("ID")
					Else
						oFSEntry.Tag = oDSRow.Item("ID")
						oFSEntry.ID = "fsentry" + oDSRow.Item("ID").ToString
					End If

					oFSEntry.Name = Me.UniqueID
					oFSEntry.IdContenedor = Me.ClientID
					'Cambio de las doble comillas para que en JavaScript no de error
					oDSRow.Item("DEN_" & msIdi) = oDSRow.Item("DEN_" & msIdi).ToString.Replace("'", "\'")
					oDSRow.Item("DEN_" & msIdi) = oDSRow.Item("DEN_" & msIdi).ToString.Replace("""", "\""")
					oFSEntry.Title = oDSRow.Item("DEN_" & msIdi)
					oFSEntry.WindowTitle = Me.Page.Title
					oFSEntry.Intro = oDSRow.Item("INTRO")
					oFSEntry.Idi = msIdi
					oFSEntry.TabContainer = msTabContainer
					oFSEntry.TipoGS = modUtilidades.DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS"))
					oFSEntry.ReadOnly = (oDSRow.Item("ESCRITURA") = 0) Or mbSoloLectura
					oFSEntry.ActivoSM = CType(Me.Page, FSPMPage).Acceso.gbAccesoFSSM

					''Comprobar Si tiene bloqueos de escritura Tarea (224)
					''---------------------------------------
					If Not oFSEntry.ReadOnly Then
						If oDSRow.GetChildRows("REL_CAMPO_BLOQUEO").Length > 0 Then
							Dim dsMonedas As DataSet
							Dim oMonedas As PMPortalServer.Monedas = FSPMServer.Get_Monedas
							Dim dCambioVigenteInstancia As Double = 1
							If Not msInstanciaMoneda Is Nothing Then
								oMonedas.LoadData("SPA", lCiaComp, msInstanciaMoneda, bAllDatos:=True)
								dCambioVigenteInstancia = oMonedas.Data.Tables(0).Rows(0)("EQUIV")
							End If

							oFSEntry.ReadOnly = ComprobarCondiciones(oDSRow.Item("ESCRITURA_FORMULA"), oDSRow.GetChildRows("REL_CAMPO_BLOQUEO"), mdsCampos, dCambioVigenteInstancia)
						End If
					End If

					oFSEntry.Obligatorio = (DBNullToSomething(oDSRow.Item("OBLIGATORIO")) = 1)
					oFSEntry.Calculado = (oDSRow.Item("TIPO") = 3 Or oDSRow.Item("SUBTIPO") = PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico) And Not IsDBNull(oDSRow.Item("ID_CALCULO"))
					oFSEntry.Orden = oDSRow.Item("ORDEN")
					oFSEntry.Grupo = oDSRow.Item("GRUPO")
					oFSEntry.DenGrupo = oDSRow.Item("DEN_GRUPO")
					oFSEntry.IdAtrib = DBNullToSomething(oDSRow.Item("ID_ATRIB_GS"))
					oFSEntry.ValErp = 0
					oFSEntry.nColumna = nColumna
					nColumna = nColumna + 1

					If bExisteCampoOrigen Then
						oFSEntry.Campo_Origen = DBNullToDbl(oDSRow.Item("CAMPO_ORIGEN"))
					End If

					If oDSRow.Item("INTRO") = 1 Then
						If (mdsCampos.Tables.Count = 1) And (DBNullToDbl(oDSRow.Item("CAMPO_HIJO")) = 0 And DBNullToDbl(oDSRow.Item("CAMPO_PADRE")) = 0) Then
							oFSEntry.Intro = 0
						Else
							oFSEntry.Lista = New DataSet
							oFSEntry.Lista.Merge(oDSRow.GetChildRows("REL_CAMPO_LISTA"))
						End If

						oFSEntry.idCampoListaEnlazada = DBNullToDbl(oDSRow.Item("ID"))

						'Si se trata de una lista comprobamos si tiene alguna lista enlazada                        
						If oDSRow.GetChildRows("REL_CAMPO_CAMPO_HIJO").GetLength(0) Then
							If mlInstancia > 0 Then
								oFSEntry.idCampoHijaListaEnlazada = DBNullToDbl(oDSRow.GetChildRows("REL_CAMPO_CAMPO_HIJO")(0)("ID_CAMPO"))
							Else
								oFSEntry.idCampoHijaListaEnlazada = DBNullToDbl(oDSRow.GetChildRows("REL_CAMPO_CAMPO_HIJO")(0)("ID"))
							End If
						End If

						'Si el campo padre de la lista es un campo de tipo material puede estar en cualquier otro grupo del formulario
						Dim bCampoPadreEncontrado As Boolean
						If oDSRow.GetChildRows("REL_CAMPO_CAMPO_PADRE").GetLength(0) Then
							bCampoPadreEncontrado = True
							If mlInstancia > 0 Then
								oFSEntry.idCampoPadreListaEnlazada = DBNullToDbl(oDSRow.GetChildRows("REL_CAMPO_CAMPO_PADRE")(0)("ID_CAMPO"))
							Else
								oFSEntry.idCampoPadreListaEnlazada = DBNullToDbl(oDSRow.GetChildRows("REL_CAMPO_CAMPO_PADRE")(0)("ID"))
							End If
						End If
						If Not bCampoPadreEncontrado Then
							If DBNullToDbl(oDSRow.Item("CAMPO_PADRE")) <> 0 Then
								If mlInstancia > 0 Then
									For Each oGrupo As PMPortalServer.Grupo In ObjInstancia.Grupos.Grupos
										If oGrupo.Id <> mlId Then
											Dim dvCampos As New DataView(oGrupo.DSCampos.Tables(0), "ID=" & oDSRow.Item("CAMPO_PADRE") & " AND TIPO=1 AND TIPO_CAMPO_GS=103", "", DataViewRowState.CurrentRows)

											If dvCampos.Count Then
												oFSEntry.idCampoPadreListaEnlazada = DBNullToDbl(dvCampos(0)("ID_CAMPO"))
												Exit For
											End If
										End If
									Next
								Else
									If _Formulario.Grupos Is Nothing Then _Formulario.Load(lCiaComp, msIdi, mlSolicitud, , FSPMServer.Get_Persona.Codigo)

									For Each oGrupo As PMPortalServer.Grupo In _Formulario.Grupos.Grupos
										If oGrupo.Id <> mlId Then
											Dim dvCampos As New DataView(oGrupo.DSCampos.Tables(0), "ID=" & oDSRow.Item("CAMPO_PADRE") & " AND TIPO=1 AND TIPO_CAMPO_GS=103", "", DataViewRowState.CurrentRows)
											If dvCampos.Count Then
												oFSEntry.idCampoPadreListaEnlazada = DBNullToDbl(dvCampos(0)("ID"))
												Exit For
											End If
										End If

									Next
								End If
							End If
						End If

						If mlInstancia > 0 Then oFSEntry.Instancia = mlInstancia
					End If

					oFSEntry.NumberFormat = oUser.NumberFormat
					oFSEntry.DateFormat = oUser.DateFormat
					oFSEntry.Tipo = oDSRow.Item("SUBTIPO")

					Select Case oFSEntry.Tipo
						Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoString, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoCorto, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoLargo
							If oFSEntry.Intro = 1 Then
								oFSEntry.Width = Unit.Percentage(100)
								If mlInstancia > 0 Then
									oFSEntry.Valor = modUtilidades.DBNullToSomething(oDSRow.Item("VALOR_NUM"))
									oFSEntry.Text = modUtilidades.DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
									If modUtilidades.DBNullToSomething(oDSRow.Item("VALOR_TEXT")) <> Nothing Then
										oFSEntry.Valor = modUtilidades.DBNullToSomething(oDSRow.Item("VALOR_NUM"))
										If oFSEntry.Lista.Tables.Count > 0 Then
											Dim oListaRows() As DataRow = oFSEntry.Lista.Tables(0).Select("ORDEN=" & oDSRow.Item("VALOR_NUM"))
											If oListaRows.Length > 0 Then
												oFSEntry.Text = modUtilidades.DBNullToSomething(oListaRows(0).Item("VALOR_TEXT_" & msIdi))
											End If
										End If
									End If
								Else
									If Not Page.IsPostBack Then
										oFSEntry.Valor = modUtilidades.DBNullToSomething(oDSRow.Item("VALOR_NUM"))
										oFSEntry.Text = modUtilidades.DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
									End If
								End If
							Else
								oFSEntry.Valor = modUtilidades.DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
								oFSEntry.Text = modUtilidades.DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
								oFSEntry.Formato = DBNullToSomething(oDSRow.Item("FORMATO"))
								If Not IsDBNull(oDSRow.Item("FORMATO")) Then oFSEntry.ErrMsgFormato = oTextos.Rows(19).Item(1)
							End If

							If (Len(oFSEntry.Text) > 80 AndAlso oFSEntry.Tipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio) Then
								oFSEntry.Width = Unit.Percentage(100)
							End If

						Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico
							oFSEntry.Width = Unit.Pixel(100)
							oFSEntry.Valor = modUtilidades.DBNullToSomething(oDSRow.Item("VALOR_NUM"))
							oFSEntry.Maximo = modUtilidades.DBNullToSomething(oDSRow.Item("MAXNUM"))
							oFSEntry.Minimo = modUtilidades.DBNullToSomething(oDSRow.Item("MINNUM"))

						Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoFecha
							iTipoFecha = modUtilidades.DBNullToSomething(oDSRow.Item("VALOR_NUM"))
							oFSEntry.Valor = modUtilidades.DevolverFechaRelativaA(iTipoFecha, DBNullToSomething(oDSRow.Item("VALOR_FEC")))
							oFSEntry.Maximo = modUtilidades.DBNullToSomething(oDSRow.Item("MAXFEC"))
							oFSEntry.Minimo = modUtilidades.DBNullToSomething(oDSRow.Item("MINFEC"))
							oFSEntry.FechaNoAnteriorAlSistema = DBNullToSomething(oDSRow.Item("FECHA_VALOR_NO_ANT_SIS"))
						Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoBoolean
							oFSEntry.Width = Unit.Pixel(50)
							oFSEntry.Valor = modUtilidades.DBNullToSomething(oDSRow.Item("VALOR_BOOL"))
							If Not IsDBNull(oDSRow.Item("VALOR_BOOL")) Then oFSEntry.Text = IIf(oDSRow.Item("VALOR_BOOL") = 1, oTextos.Rows(16).Item(1), oTextos.Rows(17).Item(1))
						Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoEditor
							oFSEntry.Valor = DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
							oFSEntry.Text = oTextos.Rows(15).Item(1)
						Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoArchivo
							Dim idAdjun As String
							Dim dSize As Double

							sAdjun = ""
							idAdjun = ""

							If msNombreContrato <> "" And oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.ArchivoContrato Then
								sAdjun = msNombreContrato
								oFSEntry.IdContrato = mlIdContrato
								oFSEntry.TamanyoContrato = msTamanyoContrato

								If oDSRowPortal Is Nothing Then
									For Each oDSRowAdjun In oDSRow.GetChildRows("REL_CAMPO_ADJUN")
										idAdjun = oDSRowAdjun.Item("ADJUN").ToString()
									Next
								Else
									For Each oDSRowAdjun In oDSRowPortal.GetChildRows("PREL_CAMPO_ADJUN")
										idAdjun = oDSRowAdjun.Item("ADJUN").ToString()
									Next
								End If
								If idAdjun = "" Or idAdjun Is Nothing Then
									idAdjun = mlIdArchivoContrato.ToString
								End If
								oFSEntry.IdArchivoContrato = idAdjun
								oFSEntry.Valor = idAdjun
							Else
								If oDSRowPortal Is Nothing Then
									For Each oDSRowAdjun In oDSRow.GetChildRows("REL_CAMPO_ADJUN")
										idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
										dSize = oDSRowAdjun.Item("DATASIZE") / 1024
										sAdjun += oDSRowAdjun.Item("NOM") + " (" + FormatNumber(dSize, oUser.NumberFormat) + " Kb.), "
									Next
								Else
									For Each oDSRowAdjun In oDSRowPortal.GetChildRows("PREL_CAMPO_ADJUN")
										idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
										dSize = oDSRowAdjun.Item("DATASIZE") / 1024
										sAdjun += oDSRowAdjun.Item("NOM") + " (" + FormatNumber(dSize, oUser.NumberFormat) + " Kb.), "
									Next
								End If
								If sAdjun <> "" Then
									sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
									idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)
								End If
								oFSEntry.Valor = idAdjun
							End If

							If Len(sAdjun) > 90 Then
								oFSEntry.Text = Mid(sAdjun, 1, 90) & "..."
							Else
								oFSEntry.Text = sAdjun
							End If

						Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoDesglose

							oFSEntry.InputStyle.CssClass = "Tipodesglose"
							oFSEntry.Width = Unit.Percentage(100)
							oFSEntry.ReadOnly = mbSoloLectura Or (oDSRow.Item("ESCRITURA") = 0)

							If sidDataEntryOrgCompras <> "" Then oFSEntry.idDataEntryDependent = sidDataEntryOrgCompras

							oFSEntry.Valor = oTextos.Rows(3).Item(1)

							Dim oCampo As Fullstep.PMPortalServer.Campo
							oCampo = FSPMServer.Get_Campo

							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								oCampo.Id = oDSRow.Item("ID_CAMPO")
							Else
								oCampo.Id = oDSRow.Item("ID")
							End If
							Dim bNuevoWorkflow As Boolean
							If mlInstancia > 0 Then
								oCampo.LoadInst(lCiaComp, mlInstancia, msIdi)
								If mTipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras OrElse mTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoExpress _
									OrElse mTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoNegociado OrElse mTipoSolicitud = TiposDeDatos.TipoDeSolicitud.Factura Then
									bNuevoWorkflow = True
								Else
									bNuevoWorkflow = False
								End If
								oDS = oCampo.LoadInstDesglose(lCiaComp, msIdi, mlInstancia, oUser.AprobadorActual, oUser.CodProveGS, mlVersion, bNuevoWorkflow)
							Else
								oCampo.Load(lCiaComp, msIdi, mlSolicitud)
								If mTipoSolicitud = TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras OrElse mTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoExpress _
									OrElse mTipoSolicitud = TiposDeDatos.TipoDeSolicitud.PedidoNegociado OrElse mTipoSolicitud = TiposDeDatos.TipoDeSolicitud.Factura Then
									bNuevoWorkflow = True
								Else
									bNuevoWorkflow = False
								End If
								If bNuevoWorkflow Then
									oDS = oCampo.LoadDesglose(lCiaComp, msIdi, oCampo.IdSolicitud, bNuevoWorkflow, oUser.CodProveGS, oUser.IdContacto)
								Else
									oDS = oCampo.LoadDesglose(lCiaComp, msIdi, oCampo.IdSolicitud)
								End If
							End If

							If Not (oCampo.Tipo = Fullstep.PMPortalServer.TipoCampoPredefinido.NoConformidad) Then
								moDSEstados = Nothing
								moAcciones = Nothing
							End If

							Dim oRow As DataRow
							Dim oNewRow As DataRow
							Dim sTblLineas As String

							If oDS.Tables.Count > 5 Then
								sTblLineas = "LINEAS_SIN"
							Else
								sTblLineas = "LINEAS"
							End If

							If Not moDSEstados Is Nothing Then
								For Each oRow In moDSEstados.Tables(0).Rows
									oDS.Tables(0).ImportRow(oRow)
									oDS.Tables(0).Rows(oDS.Tables(0).Rows.Count - 1).Item("ID") = oRow.Item("ID").ToString
								Next
								For Each oRow In moDSEstados.Tables(1).Rows

									oDS.Tables(1).ImportRow(oRow)
								Next
								idCampo = oCampo.IdCopiaCampo
								For Each oRow In moDSEstados.Tables(2).Select("CAMPO_PADRE= " & idCampo.ToString)

									oNewRow = oDS.Tables(sTblLineas).NewRow

									oNewRow.Item("LINEA") = oRow.Item("LINEA")
									oNewRow.Item("CAMPO_PADRE") = oRow.Item("CAMPO_PADRE")
									oNewRow.Item("CAMPO_HIJO") = oRow.Item("CAMPO_HIJO").ToString
									oNewRow.Item("VALOR_NUM") = oRow.Item("VALOR_NUM")
									oNewRow.Item("VALOR_TEXT") = oRow.Item("VALOR_TEXT")
									oDS.Tables(sTblLineas).Rows.Add(oNewRow)
								Next
							End If

							oFSEntry.Lista = oDS
					End Select

					Select Case oFSEntry.TipoGS
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.Material

							sValor = ""
							sKeyMaterial = oFSEntry.ClientID
							Dim sMat As String = modUtilidades.DBNullToSomething(oDSRow.Item("VALOR_TEXT"))

							Dim lLongCod As Integer

							For i = 1 To 4
								arrMat(i) = ""
							Next i

							i = 1
							While Trim(sMat) <> ""
								Select Case i
									Case 1
										lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
									Case 2
										lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
									Case 3
										lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
									Case 4
										lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN4
								End Select
								arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
								sMat = Mid(sMat, lLongCod + 1)
								i = i + 1
							End While
							sToolTip = ""
							sDenominacion = ""

							If arrMat(4) <> "" Then
								If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
									sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
								Else
									oGMN3 = FSPMServer.Get_GrupoMatNivel3
									oGMN3.GMN1Cod = arrMat(1)
									oGMN3.GMN2Cod = arrMat(2)
									oGMN3.Cod = arrMat(3)
									oGMN3.CargarTodosLosGruposMatDesde(lCiaComp, 1, msIdi, arrMat(4), , True)
									sDenominacion = oGMN3.GruposMatNivel4.Item(arrMat(4)).Den

									oGMN3 = Nothing
								End If
								sToolTip = sDenominacion & " (" & arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) & " - " & arrMat(4) & ")"
								If CType(Me.Page, FSPMPage).Acceso.gbMaterialVerTodosNiveles Then
									sValor = arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + arrMat(4) + " - " + sDenominacion
								Else
									sValor = arrMat(4) + " - " + sDenominacion
								End If

							ElseIf arrMat(3) <> "" Then
								If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
									sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
								Else
									oGMN2 = FSPMServer.Get_GrupoMatNivel2
									oGMN2.GMN1Cod = arrMat(1)
									oGMN2.Cod = arrMat(2)
									oGMN2.CargarTodosLosGruposMatDesde(lCiaComp, 1, msIdi, arrMat(3), , True)
									sDenominacion = oGMN2.GruposMatNivel3.Item(arrMat(3)).Den

									oGMN2 = Nothing
								End If
								sToolTip = sDenominacion & "(" & arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) & ")"
								If CType(Me.Page, FSPMPage).Acceso.gbMaterialVerTodosNiveles Then
									sValor = arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + sDenominacion
								Else
									sValor = arrMat(3) + " - " + sDenominacion
								End If

							ElseIf arrMat(2) <> "" Then
								If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
									sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
								Else
									oGMN1 = FSPMServer.Get_GrupoMatNivel1
									oGMN1.Cod = arrMat(1)
									oGMN1.CargarTodosLosGruposMatDesde(lCiaComp, 1, msIdi, arrMat(2), , True)
									sDenominacion = oGMN1.GruposMatNivel2.Item(arrMat(2)).Den

									oGMN1 = Nothing
								End If
								sToolTip = sDenominacion & "(" & arrMat(1) & " - " & arrMat(2) & ")"
								If CType(Me.Page, FSPMPage).Acceso.gbMaterialVerTodosNiveles Then
									sValor = arrMat(1) + " - " + arrMat(2) + " - " + sDenominacion
								Else
									sValor = arrMat(2) + " - " + sDenominacion
								End If

							ElseIf arrMat(1) <> "" Then
								If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
									sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
								Else
									oGMN1s = FSPMServer.Get_GruposMatNivel1
									oGMN1s.CargarTodosLosGruposMatDesde(lCiaComp, 1, msIdi, arrMat(1), , , True)
									sDenominacion = oGMN1s.Item(arrMat(1)).Den

									oGMN1s = Nothing
								End If
								sToolTip = sDenominacion & "(" & arrMat(1) & ")"
								sValor = arrMat(1) + " - " + sDenominacion
							End If

							oFSEntry.Width = Unit.Percentage(100)
							oFSEntry.Text = sValor
							oFSEntry.ToolTip = sToolTip
							oFSEntry.Denominacion = sDenominacion
							oFSEntry.AnyadirArt = oDSRow.Item("ANYADIR_ART")
							oFSEntry.SelCualquierMatPortal = oDSRow.Item("SEL_CUALQUIER_MAT_PORTAL")
							oFSEntry.NivelSeleccion = DBNullToInteger(oDSRow.Item("NIVEL_SELECCION"))
							If oFSEntry.NivelSeleccion <> 0 Then
								sIdDataentryMaterial = oFSEntry.IdContenedor & "_" & "fsentry" & oFSEntry.Tag
								Dim sscript As String
								sscript = "var sDataEntryMaterialFORM='" & sIdDataentryMaterial & "'"
								sscript = String.Format(IncludeScriptKeyFormat, "javascript", sscript)
								If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "VariablesMATERIALEntry" & oFSEntry.Tag) Then _
									 Page.ClientScript.RegisterStartupScript(Page.GetType(), "VariablesMATERIALEntry" & oFSEntry.Tag, sscript)
							End If


							'Comprobar si tiene alg�n campo de tipo lista como hija. Puede estar en cualquier grupo, 1� se busca en el grupo actual y despu�s en los otros
							Dim bListaHijaEncontrada As Boolean
							If oDSRow.GetChildRows("REL_CAMPO_CAMPO_HIJO").Count Then
								bListaHijaEncontrada = True
								If mlInstancia > 0 Then
									oFSEntry.idCampoHijaListaEnlazada = DBNullToDbl(oDSRow.GetChildRows("REL_CAMPO_CAMPO_HIJO")(0)("ID_CAMPO"))

								Else
									oFSEntry.idCampoHijaListaEnlazada = DBNullToDbl(oDSRow.GetChildRows("REL_CAMPO_CAMPO_HIJO")(0)("ID"))
								End If
							End If
							If Not bListaHijaEncontrada Then
								If mlInstancia > 0 Then
									For Each oGrupo As PMPortalServer.Grupo In ObjInstancia.Grupos.Grupos
										If oGrupo.Id <> mlId Then
											Dim dvCampos As New DataView(oGrupo.DSCampos.Tables(0), "CAMPO_PADRE=" & oFSEntry.Campo_Origen & " AND TIPO=0 AND INTRO=1 AND TIPO_CAMPO_GS IS NULL", "", DataViewRowState.CurrentRows)
											If dvCampos.Count Then
												oFSEntry.idCampoPadreListaEnlazada = DBNullToDbl(dvCampos(0)("ID_CAMPO"))
												Exit For
											End If
										End If
									Next
								Else
									If _Formulario.Grupos Is Nothing Then _Formulario.Load(lCiaComp, msIdi, mlSolicitud, , FSPMServer.Get_Persona.Codigo)

									For Each oGrupo As PMPortalServer.Grupo In _Formulario.Grupos.Grupos
										If oGrupo.Id <> mlId Then
											Dim dvCampos As New DataView(oGrupo.DSCampos.Tables(0), "CAMPO_PADRE=" & oFSEntry.Tag & " AND TIPO=0 AND INTRO=1 AND TIPO_CAMPO_GS IS NULL", "", DataViewRowState.CurrentRows)

											If dvCampos.Count Then
												oFSEntry.idCampoHijaListaEnlazada = DBNullToDbl(dvCampos(0)("ID"))
												Exit For
											End If
										End If
									Next
								End If
							End If

						Case PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo
							oFSEntry.Width = Unit.Percentage(100)

							oFSEntry.DependentField = sKeyMaterial
							oFSEntry.Intro = 1
							oFSEntry.InstanciaMoneda = msInstanciaMoneda
							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
									If oDSRow.Item("VALOR_TEXT_COD") = 0 Then
										oFSEntry.Text = oDSRow.Item("VALOR_TEXT")
									Else
										oFSEntry.Text = oDSRow.Item("VALOR_TEXT_DEN")
									End If
								Else
									oArts = FSPMServer.Get_Articulos
									Dim oArticulo As Fullstep.PMPortalServer.PropiedadesArticulo = oArts.Articulo_Get_Properties(lCiaComp, oDSRow.Item("VALOR_TEXT"))
									If oArticulo.ExisteArticulo Then
										oFSEntry.Text = oArticulo.Denominacion
										oFSEntry.TipoRecepcion = oArticulo.TipoRecepcion
										sKeyTipoRecepcion = oFSEntry.TipoRecepcion
									Else
										oFSEntry.Text = oDSRow.Item("VALOR_TEXT")
									End If
								End If
							End If
							oFSEntry.Denominacion = oFSEntry.Text
							If sidDataEntryOrgCompras <> "" Then
								oFSEntry.idDataEntryDependent = sidDataEntryOrgCompras
							End If
							If sIDCentro <> "" Then
								oFSEntry.idDataEntryDependent2 = oFSEntry.IdContenedor & "_" & "fsentry" & sIDCentro
							End If
							Dim sKeyUnidad As String
							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
									sKeyUnidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
								End If
							Else
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
									sKeyUnidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
								End If
							End If
							If sKeyUnidad <> "" Then oFSEntry.idEntryUNI = oFSEntry.IdContenedor & "_fsentry" & sKeyUnidad

							'Buscar la organizacion de compras y el centro
							Try
								oFSEntry.DependentValueDataEntry = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
								oFSEntry.DependentValueDataEntry2 = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))

								oFSEntry.UnidadOculta = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
							Catch ex As Exception
							End Try

							Try
								If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
									oFSEntry.IdMaterialOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
									oFSEntry.IdDenArticuloOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
								Else
									oFSEntry.IdMaterialOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID"))
									oFSEntry.IdDenArticuloOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID"))
								End If
							Catch ex As Exception
							End Try
							'idEntryPROV --> fsentry12345
							If Not idEntryPROV Is Nothing Then
								oFSEntry.idEntryPROV = oFSEntry.IdContenedor & "_" & idEntryPROV
							End If
							If Not idEntryPREC Is Nothing Then
								oFSEntry.idEntryPREC = oFSEntry.IdContenedor & "_" & idEntryPREC
							End If
						Case TiposDeDatos.TipoCampoGS.ProveedorERP
							'El campo Gs proveedor ERP dependera de 2 campos, el campo proveedor y el campo organizacion de compras para mostrar sus valores
							'El campo proveedor debera estar al mismo nivel que el campo proveedor ERP asi que en este caso tiene que estar en el mismo grupo
							Dim sKeyProveedorDeProveERP As String
							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
									sKeyProveedorDeProveERP = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
								End If
							Else
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
									sKeyProveedorDeProveERP = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
								End If
							End If

							'Si el campo proveedor esta a nivel de formulario el de organizacion de compras tambien debe estarlo, aunque no tiene que estar en el mismo grupo
							Dim sKeyOrgComprasDeProveERP As String
							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras).Length > 0 Then
									sKeyOrgComprasDeProveERP = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras)(0).Item("ID_CAMPO")
								End If
							Else
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras).Length > 0 Then
									sKeyOrgComprasDeProveERP = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras)(0).Item("ID")
								End If
							End If
							oFSEntry.idEntryPROV = oFSEntry.IdContenedor & "_fsentry" & sKeyProveedorDeProveERP
							oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_fsentry" & sKeyOrgComprasDeProveERP

							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								Dim oProvesERP As PMPortalServer.ProveedoresERP
								oProvesERP = FSPMServer.Get_ProveedoresERP

								Dim ds As DataSet = oProvesERP.CargarProveedoresERPtoDS(lCiaComp, , , oDSRow.Item("VALOR_TEXT"))
								For Each dr As DataRow In ds.Tables(0).Rows
									oFSEntry.Text = dr("COD") & " - " & dr("DEN")
									oFSEntry.Valor = dr("COD")
									Exit For
								Next
								ds = Nothing
								oProvesERP = Nothing
							End If

							oFSEntry.Intro = 1
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo
							Dim idCampoMat As Long
							Dim sValorMat As String
							idCampoMat = 0
							'OBTENEMOS EL VALOR DEL MAT OCULTO
							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								For Each i In arrCamposMaterial
									If i < oDSRow.Item("id_campo") Then
										If idCampoMat < i Then
											idCampoMat = i
										End If
									End If
								Next
								If idCampoMat > 0 Then
									Try
										sValorMat = mdsCampos.Tables(0).Select("ID_CAMPO = " & idCampoMat)(0).Item("VALOR_TEXT")

										oFSEntry.DependentValue = sValorMat
									Catch ex As Exception

									End Try
								End If
							Else
								For Each i In arrCamposMaterial
									If i < oDSRow.Item("ID") Then
										If idCampoMat < i Then
											idCampoMat = i
										End If
									End If
								Next
								If idCampoMat > 0 Then
									Try
										sValorMat = mdsCampos.Tables(0).Select("ID = " & idCampoMat)(0).Item("VALOR_TEXT")

										oFSEntry.DependentValue = sValorMat
									Catch ex As Exception

									End Try
								End If
							End If

							oFSEntry.Width = Unit.Percentage(100)

							oFSEntry.DependentField = sKeyMaterial
							oFSEntry.Intro = 1
							oFSEntry.InstanciaMoneda = msInstanciaMoneda
							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
								oFSEntry.Text = oDSRow.Item("VALOR_TEXT")
								oFSEntry.Denominacion = oFSEntry.Text

								oArts = FSPMServer.Get_Articulos
								Dim oArticulo As Fullstep.PMPortalServer.PropiedadesArticulo = oArts.Articulo_Get_Properties(lCiaComp, oDSRow.Item("VALOR_TEXT"))
								If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
									If oArticulo.ExisteArticulo Then
										oFSEntry.articuloGenerico = oArticulo.Generico
										oFSEntry.articuloCodificado = True
										oFSEntry.TipoRecepcion = oArticulo.TipoRecepcion
										sKeyTipoRecepcion = oFSEntry.TipoRecepcion
									End If
								Else
									If UBound(arrMat) = 0 Then
										oArts.GMN1 = sValorMat
									Else
										oArts.GMN1 = arrMat(0)
										If UBound(arrMat) > 0 Then
											oArts.GMN2 = arrMat(1)
										End If
										If UBound(arrMat) > 1 Then
											oArts.GMN3 = arrMat(2)
										End If
										If UBound(arrMat) > 2 Then
											oArts.GMN4 = arrMat(3)
										End If
									End If

									If oArticulo.ExisteArticulo Then
										oFSEntry.articuloCodificado = True
										oFSEntry.articuloGenerico = oArticulo.Generico
										oFSEntry.TipoRecepcion = oArticulo.TipoRecepcion
										sKeyTipoRecepcion = oFSEntry.TipoRecepcion
									Else
										oFSEntry.articuloCodificado = False
									End If
								End If
							Else
								oFSEntry.articuloCodificado = False
							End If

							oFSEntry.AnyadirArt = oDSRow.Item("ANYADIR_ART")
							bArticuloGenerico = oFSEntry.articuloGenerico
							If sidDataEntryOrgCompras <> "" Then
								oFSEntry.idDataEntryDependent = sidDataEntryOrgCompras
							End If
							If sIDCentro <> "" Then
								oFSEntry.idDataEntryDependent2 = oFSEntry.IdContenedor & "_" & "fsentry" & sIDCentro
							End If
							Dim sKeyUnidad As String
							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
									sKeyUnidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
								End If
							Else
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
									sKeyUnidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
								End If
							End If
							If sKeyUnidad <> "" Then oFSEntry.idEntryUNI = oFSEntry.IdContenedor & "_fsentry" & sKeyUnidad

							'Buscar la organizacion de compras y el centro
							Try
								oFSEntry.DependentValueDataEntry = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
								oFSEntry.DependentValueDataEntry2 = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))

								oFSEntry.UnidadOculta = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
							Catch ex As Exception
							End Try

							Try
								If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
									oFSEntry.IdMaterialOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
									oFSEntry.IdDenArticuloOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
								Else
									oFSEntry.IdMaterialOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID"))
									oFSEntry.IdDenArticuloOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID"))
								End If
							Catch ex As Exception
							End Try

							'idEntryPROV --> fsentry12345
							If Not idEntryPROV Is Nothing Then
								oFSEntry.idEntryPROV = oFSEntry.IdContenedor & "_" & idEntryPROV
							End If
							If Not idEntryPREC Is Nothing Then
								oFSEntry.idEntryPREC = oFSEntry.IdContenedor & "_" & idEntryPREC
							End If

						Case PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo
							oFSEntry.Width = Unit.Percentage(100)
							oFSEntry.DependentField = sKeyMaterial
							oFSEntry.Intro = 1
							oFSEntry.Valor = DBNullToStr(oDSRow.Item("VALOR_TEXT"))
							oFSEntry.Text = DBNullToStr(oDSRow.Item("VALOR_TEXT"))
							oFSEntry.InstanciaMoneda = msInstanciaMoneda
							oFSEntry.TipoRecepcion = sKeyTipoRecepcion
							If sCodArticulo <> "" Then
								oFSEntry.codigoArticulo = sCodArticulo
								sCodArticulo = ""
							End If
							Dim idCampoMat As Long
							Dim sValorMat As String

							idCampoMat = 0
							'OBTENEMOS EL VALOR DEL MAT OCULTO                            
							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								For Each i In arrCamposMaterial

									If i < oDSRow.Item("id_campo") Then
										If idCampoMat < i Then
											idCampoMat = i

										End If
									End If
								Next
								If idCampoMat > 0 Then
									Try
										sValorMat = mdsCampos.Tables(0).Select("ID_CAMPO = " & idCampoMat)(0).Item("VALOR_TEXT")
										oFSEntry.DependentValue = sValorMat
									Catch ex As Exception

									End Try
								End If
							Else
								For Each i In arrCamposMaterial
									If i < oDSRow.Item("ID") Then
										If idCampoMat < i Then
											idCampoMat = i
										End If
									End If
								Next
								If idCampoMat > 0 Then
									Try
										sValorMat = mdsCampos.Tables(0).Select("ID = " & idCampoMat)(0).Item("VALOR_TEXT")
										oFSEntry.DependentValue = sValorMat
									Catch ex As Exception

									End Try

								End If
							End If
							oFSEntry.articuloGenerico = bArticuloGenerico
							If sidDataEntryOrgCompras <> "" Then
								oFSEntry.idDataEntryDependent = sidDataEntryOrgCompras
							End If
							If sIDCentro <> "" Then
								oFSEntry.idDataEntryDependent2 = oFSEntry.IdContenedor & "_" & "fsentry" & sIDCentro
							End If
							Dim sKeyUnidad As String
							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
									sKeyUnidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
								End If
							Else
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
									sKeyUnidad = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
								End If
							End If
							If sKeyUnidad <> "" Then oFSEntry.idEntryUNI = oFSEntry.IdContenedor & "_fsentry" & sKeyUnidad

							'Buscar la organizacion de compras y el centro
							Try
								oFSEntry.DependentValueDataEntry = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
								oFSEntry.DependentValueDataEntry2 = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))

								oFSEntry.UnidadOculta = DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("VALOR_TEXT"))
							Catch ex As Exception
							End Try
							Try
								If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
									oFSEntry.IdMaterialOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
									oFSEntry.IdCodArticuloOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
								Else
									oFSEntry.IdMaterialOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID"))
									oFSEntry.IdCodArticuloOculta = oFSEntry.IdContenedor & "_fsentry" & DBNullToSomething(mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID"))
								End If
							Catch ex As Exception
							End Try

							'idEntryPROV --> fsentry12345
							If Not idEntryPROV Is Nothing Then
								oFSEntry.idEntryPROV = oFSEntry.IdContenedor & "_" & idEntryPROV
							End If
							If Not idEntryPREC Is Nothing Then
								oFSEntry.idEntryPREC = oFSEntry.IdContenedor & "_" & idEntryPREC
							End If

						Case PMPortalServer.TiposDeDatos.TipoCampoGS.Dest
							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								Dim oDests As PMPortalServer.Destinos
								oDests = FSPMServer.Get_Destinos()
								oDests.LoadData(msIdi, lCiaComp, oUser.AprobadorActual)

								oFSEntry.Text = TextoDelDropDown(oDests.Data, "COD", oDSRow.Item("VALOR_TEXT"), Not oFSEntry.ReadOnly, , , True)
								oFSEntry.ToolTip = oFSEntry.Text
								oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
							End If
							oFSEntry.Width = Unit.Percentage(100)
							oFSEntry.Intro = 1
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.Cantidad
							If sKeyTipoRecepcion = "1" Then
								oFSEntry.TipoRecepcion = 1
							End If
							If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeRecep") Then
								Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeRecep", "<script>var smensajeRecep = '" & oTextos.Rows.Item(18).Item(1) & "'</script>")
							End If
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad
							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								Dim oUnis As PMPortalServer.Unidades
								oUnis = FSPMServer.get_Unidades()
								oUnis.LoadData(msIdi, lCiaComp, , , True)

								oFSEntry.Text = TextoDelDropDown(oUnis.Data, "COD", oDSRow.Item("VALOR_TEXT"), Not oFSEntry.ReadOnly)
								oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
							End If

							oFSEntry.Width = Unit.Percentage(100)
							oFSEntry.Intro = 1
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.FormaPago
							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								Dim oPags As PMPortalServer.FormasPago
								oPags = FSPMServer.Get_FormasPago()
								oPags.LoadData(msIdi, lCiaComp)

								oFSEntry.Text = TextoDelDropDown(oPags.Data, "COD", oDSRow.Item("VALOR_TEXT"), Not oFSEntry.ReadOnly)
								oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
							End If
							oFSEntry.Width = Unit.Percentage(100)
							oFSEntry.Intro = 1
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.Moneda
							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								Dim oMons As PMPortalServer.Monedas
								oMons = FSPMServer.Get_Monedas
								oMons.LoadData(msIdi, lCiaComp)

								oFSEntry.Text = TextoDelDropDown(oMons.Data, "COD", oDSRow.Item("VALOR_TEXT"), Not oFSEntry.ReadOnly)
								oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
							End If
							oFSEntry.Width = Unit.Pixel(200)
							oFSEntry.Intro = 1

						Case PMPortalServer.TiposDeDatos.TipoCampoGS.Pais
							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								oPaises = FSPMServer.Get_Paises()
								oPaises.LoadData(msIdi, lCiaComp)

								oFSEntry.Text = TextoDelDropDown(oPaises.Data, "COD", oDSRow.Item("VALOR_TEXT"), Not oFSEntry.ReadOnly)
								sCodPais = oDSRow.Item("VALOR_TEXT")
								oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
							End If
							sKeyPais = oFSEntry.ClientID

							oFSEntry.Intro = 1

							'Relaci�n con Provincia
							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Provincia & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
									sKeyProvincia = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Provincia & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
								End If
							Else
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Provincia & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
									sKeyProvincia = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Provincia & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
								End If
							End If
							If Not sKeyProvincia Is Nothing Then
								oFSEntry.DependentField = "fsentry" + sKeyProvincia
							End If

						Case PMPortalServer.TiposDeDatos.TipoCampoGS.Provincia
							'Provincia es un campo relacionado por lo que la carga de datos se hace desde javascript(ajax)
							'Esta carga se hace �nicamente cuando el campo tiene un valor por defecto y hay que buscar la descripci�n del valor por defecto
							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								If Not String.IsNullOrEmpty(sCodPais) Then
									Dim oProvis As PMPortalServer.Provincias
									oProvis = FSPMServer.Get_Provincias()
									oProvis.Pais = DBNullToStr(sCodPais)
									oProvis.LoadData(msIdi, lCiaComp)
									'Como texto ponemos el c�digo y la descripci�n porque si no puede haber problemas al ser controles que cargan los datos desde javascript y tener Infragistics 11.1 limitaciones en ese sentido
									oFSEntry.Text = TextoDelDropDown(oProvis.Data, "COD", oDSRow.Item("VALOR_TEXT"), True, "PAICOD", sCodPais) 'sDenominacion
									oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
								End If
							End If

							If sKeyPais <> String.Empty Then _
								oFSEntry.MasterField = sKeyPais

							oFSEntry.Intro = 1
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.PRES1, PMPortalServer.TiposDeDatos.TipoCampoGS.Pres2, PMPortalServer.TiposDeDatos.TipoCampoGS.Pres3, PMPortalServer.TiposDeDatos.TipoCampoGS.Pres4
							oFSEntry.Width = Unit.Percentage(100)
							oFSEntry.Valor = modUtilidades.DBNullToSomething(oDSRow.Item("VALOR_TEXT"))
							If oFSEntry.Valor <> Nothing Then
								Dim arrPresupuestos() As String = oFSEntry.Valor.split("#")
								Dim oPresup As String
								Dim arrPresup(2) As String
								Dim iContadorPres As Integer
								Dim arrDenominaciones() As String
								If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") And Not DBNullToSomething(oDSRow.Item("VALOR_TEXT")) = Nothing Then
									arrDenominaciones = oDSRow.Item("VALOR_TEXT_DEN").ToString().Split("#")
									If oDSRow.Item("VALOR_TEXT_COD") Then
										oFSEntry.Tipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoPresBajaLog
									Else
										oFSEntry.Tipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio
									End If
									sDenominacion = oDSRow.Item("VALOR_TEXT_DEN")
								Else
									oFSEntry.Tipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio
								End If
								iContadorPres = 0

								sValor = ""
								sTexto = ""
								sDenominacion = ""
								For Each oPresup In arrPresupuestos
									iContadorPres = iContadorPres + 1
									arrPresup = oPresup.Split("_")
									iNivel = arrPresup(0)
									lIdPresup = arrPresup(1)
									dPorcent = Numero(arrPresup(2), ".", ",")
									If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") And Not DBNullToSomething(oDSRow.Item("VALOR_TEXT")) = Nothing Then
										sTexto = arrDenominaciones(iContadorPres - 1) & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); " & sTexto
										sValor += oPresup + "#"
									Else

										Select Case oFSEntry.TipoGS
											Case PMPortalServer.TiposDeDatos.TipoCampoGS.PRES1
												oPres1 = FSPMServer.Get_PresProyectosNivel1
												Select Case iNivel
													Case 1
														oPres1.LoadData(lCiaComp, lIdPresup)
													Case 2
														oPres1.LoadData(lCiaComp, Nothing, lIdPresup)
													Case 3
														oPres1.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
													Case 4
														oPres1.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
												End Select
												oDS = oPres1.Data
												oPres1 = Nothing
											Case PMPortalServer.TiposDeDatos.TipoCampoGS.Pres2
												oPres2 = FSPMServer.Get_PresContablesNivel1
												Select Case iNivel
													Case 1
														oPres2.LoadData(lCiaComp, lIdPresup)
													Case 2
														oPres2.LoadData(lCiaComp, Nothing, lIdPresup)
													Case 3
														oPres2.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
													Case 4
														oPres2.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
												End Select
												oDS = oPres2.Data
												oPres2 = Nothing
											Case PMPortalServer.TiposDeDatos.TipoCampoGS.Pres3
												oPres3 = FSPMServer.Get_PresConceptos3Nivel1
												Select Case iNivel
													Case 1
														oPres3.LoadData(lCiaComp, lIdPresup)
													Case 2
														oPres3.LoadData(lCiaComp, Nothing, lIdPresup)
													Case 3
														oPres3.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
													Case 4
														oPres3.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
												End Select
												oDS = oPres3.Data
												oPres3 = Nothing
											Case PMPortalServer.TiposDeDatos.TipoCampoGS.Pres4
												oPres4 = FSPMServer.Get_PresConceptos4Nivel1
												Select Case iNivel
													Case 1
														oPres4.LoadData(lCiaComp, lIdPresup)
													Case 2
														oPres4.LoadData(lCiaComp, Nothing, lIdPresup)
													Case 3
														oPres4.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
													Case 4
														oPres4.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
												End Select
												oDS = oPres4.Data
												oPres4 = Nothing
										End Select
										Dim iAnyo As Integer
										Dim sUon1 As String
										Dim sUon2 As String
										Dim sUon3 As String
										Dim bSinPermisos As Boolean

										If Not oDS Is Nothing Then
											If oDS.Tables(0).Rows.Count > 0 Then
												With oDS.Tables(0).Rows(0)
													'comprobamos que el valor por defecto cumpla la restricci�n del usuario. SI NO CUMPLE, NO APARECER�
													If Not oDS.Tables(0).Columns("ANYO") Is Nothing Then
														iAnyo = .Item("ANYO")

													End If
													sUon1 = DBNullToSomething(.Item("UON1"))
													sUon2 = DBNullToSomething(.Item("UON2"))
													sUon3 = DBNullToSomething(.Item("UON3"))
													bSinPermisos = False
													If sUon3 <> Nothing And oUser.UON3Aprobador <> Nothing Then
														If sUon3 <> oUser.UON3Aprobador Then
															'malo (el usuario no tiene permisos para asignar el presupuesto por defecto )
															bSinPermisos = True
														End If
													End If
													If sUon2 <> Nothing And oUser.UON2Aprobador <> Nothing Then
														If sUon2 <> oUser.UON2Aprobador Then

															bSinPermisos = True
														End If
													End If
													If sUon1 <> Nothing And oUser.UON1Aprobador <> Nothing Then
														If sUon1 <> oUser.UON1Aprobador Then

															bSinPermisos = True
														End If
													End If


													If Not bSinPermisos Then
														Dim sDenPresup As String
														If Not oDS.Tables(0).Columns("ANYO") Is Nothing Then
															sTexto += .Item("ANYO").ToString + " - "
															sDenPresup = .Item("ANYO").ToString + " - "
														End If

														If Not IsDBNull(.Item("PRES4")) Then
															sTexto += .Item("PRES4").ToString & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
															sDenPresup += .Item("PRES4").ToString()
														ElseIf Not IsDBNull(.Item("PRES3")) Then
															sTexto += .Item("PRES3").ToString & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
															sDenPresup += .Item("PRES3").ToString()
														ElseIf Not IsDBNull(.Item("PRES2")) Then
															sTexto += .Item("PRES2").ToString & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
															sDenPresup += .Item("PRES2").ToString()
														ElseIf Not IsDBNull(.Item("PRES1")) Then
															sTexto += .Item("PRES1").ToString & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
															sDenPresup += .Item("PRES1").ToString()
														End If
														If sDenominacion <> "" Then sDenominacion = "#" & sDenominacion
														sDenominacion = sDenPresup & sDenominacion
														sValor += oPresup + "#"
													End If

												End With
											End If
										End If
									End If
								Next
								If sValor <> Nothing Then
									sValor = Left(sValor, sValor.Length - 1)
								End If
								oFSEntry.Valor = sValor
								oFSEntry.Denominacion = sDenominacion
								If sTexto = "" Then
									If Not oFSEntry.ReadOnly Then
										oFSEntry.Text = oTextos.Rows(1).Item(1)
									End If
								Else
									oFSEntry.Text = sTexto
								End If
							Else
								If Not oFSEntry.ReadOnly Then
									oFSEntry.Text = oTextos.Rows(1).Item(1)
								End If
							End If
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.Proveedor
							If Not IsDBNull(oDSRow.Item("CARGAR_ULT_ADJ")) Then
								oFSEntry.CargarUltADJ = oDSRow.Item("CARGAR_ULT_ADJ")
								If oFSEntry.CargarUltADJ = True Then
									Dim mioFSDSEntry As Fullstep.DataEntry.GeneralEntry

									Dim sCampoCodArt, sCampoCodArtAnt As String
									Dim sCampoDenArt As String
									If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
										If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
											sCampoCodArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID_CAMPO")
										End If
										If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
											sCampoDenArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID_CAMPO")
										End If
										If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
											sCampoCodArtAnt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID_CAMPO")
										End If
									Else
										If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
											sCampoCodArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID")
										End If
										If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
											sCampoDenArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID")
										End If
										If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
											sCampoCodArtAnt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID")
										End If
									End If
									mioFSDSEntry = Me.FindControl("fsentry" & sCampoCodArt)
									If Not mioFSDSEntry Is Nothing Then
										mioFSDSEntry.WindowTitle = Me.Page.Title
										mioFSDSEntry.Portal = True
										mioFSDSEntry.idEntryPROV = oFSEntry.IdContenedor & "_" & oFSEntry.ClientID
									Else
										idEntryPROV = oFSEntry.ClientID
									End If
									mioFSDSEntry = Me.FindControl("fsentry" & sCampoDenArt)
									If Not mioFSDSEntry Is Nothing Then
										mioFSDSEntry.WindowTitle = Me.Page.Title
										mioFSDSEntry.idEntryPROV = oFSEntry.IdContenedor & "_" & oFSEntry.ClientID
									Else
										idEntryPROV = oFSEntry.ClientID
									End If
									mioFSDSEntry = Me.FindControl("fsentry" & sCampoCodArtAnt)
									If Not mioFSDSEntry Is Nothing Then
										mioFSDSEntry.WindowTitle = Me.Page.Title
										mioFSDSEntry.idEntryPROV = oFSEntry.IdContenedor & "_" & oFSEntry.ClientID
									Else
										idEntryPROV = oFSEntry.ClientID
									End If

								End If
							Else
								oFSEntry.CargarUltADJ = False
							End If
							If oFSEntry.Valor <> Nothing Then
								Dim CodProve As String = ""
								If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
									sDenominacion = oDSRow.Item("VALOR_TEXT_DEN")
									If ConfigurationManager.AppSettings("PYME") <> "" Then
										CodProve = Replace(oFSEntry.Valor.ToString(), ConfigurationManager.AppSettings("PYME"), "", 1, 1)
									End If
									If CodProve = "" Then
										CodProve = oFSEntry.Valor
									End If
								Else
									oProve = FSPMServer.get_Proveedor
									oProve.Cod = oFSEntry.Valor
									oProve.Load(msIdi, lCiaComp)
									sDenominacion = oProve.Den
									If ConfigurationManager.AppSettings("PYME") <> "" Then
										CodProve = oProve.Cod
										If InStr(1, oProve.Cod, ConfigurationManager.AppSettings("PYME")) = 1 Then
											CodProve = Replace(oProve.Cod, ConfigurationManager.AppSettings("PYME"), "", 1, 1)
										Else
											CodProve = oProve.Cod
										End If
									Else
										CodProve = oProve.Cod
									End If
								End If
								oFSEntry.Text = CodProve & " - " & sDenominacion.Replace("'", "")
								oFSEntry.Denominacion = sDenominacion
							Else
								If mlInstancia = 0 Then 'Si estoy en alta pone el proveedor que ha entrado
									oProve = FSPMServer.get_Proveedor
									oProve.Cod = oUser.CodProveGS
									oProve.Load(msIdi, lCiaComp)
									sDenominacion = oProve.Den
									oFSEntry.Valor = oProve.Cod
									oFSEntry.Text = oProve.Cod & " - " & sDenominacion.Replace("'", "")
									oFSEntry.Denominacion = sDenominacion
								Else
									If Not oFSEntry.ReadOnly Then 'SIN TRADUCIR
										oFSEntry.Text = oTextos.Rows(2).Item(1)
									End If
								End If
							End If
							Dim sql As String
							Dim sCampoDependent As String = ""
							sql = "(TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras & ") AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 "

							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								If mdsCampos.Tables(0).Select(sql).Length > 0 Then
									sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID_CAMPO")
								End If
							Else
								If mdsCampos.Tables(0).Select(sql).Length > 0 Then
									sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID")
								End If
							End If
							If sCampoDependent <> "" Then ' Relacionar la Organizacion de Compras con el Centro
								oFSEntry.DependentField = oFSEntry.IdContenedor & "_" & "fsentry" & sCampoDependent
							End If

						Case PMPortalServer.TiposDeDatos.TipoCampoGS.Persona
							If oFSEntry.Valor <> Nothing Then
								If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
									sDenominacion = DBNullToStr(oDSRow.Item("VALOR_TEXT_DEN"))
								Else
									Dim oPersona As Fullstep.PMPortalServer.Persona = FSPMServer.Get_Persona
									oPersona.LoadData(lCiaComp, oFSEntry.Valor.ToString)
									If Not oPersona.Codigo Is Nothing Then
										sDenominacion = oPersona.Denominacion(True)
									Else
										sDenominacion = ""
									End If
								End If
								oFSEntry.Text = sDenominacion
								oFSEntry.Text = sDenominacion
							End If
							oFSEntry.Intro = 0
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.Departamento
							If sKeyUnidadOrganizativa <> String.Empty Then _
								oFSEntry.MasterField = sKeyUnidadOrganizativa

							'Departamento es un campo relacionado por lo que la carga de datos se hace desde javascript(ajax)
							'Esta carga se hace �nicamente cuando el campo tiene un valor por defecto y hay que buscar la descripci�n del valor por defecto
							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								Dim oDepartamentos As PMPortalServer.Departamentos
								oDepartamentos = FSPMServer.Get_Departamentos
								If bDepartamentoRelacionado Then
									Dim vUnidadesOrganizativas(3) As String
									sUnidadOrganizativa = DBNullToStr(NothingToDBNull(sUnidadOrganizativa))
									vUnidadesOrganizativas = Split(sUnidadOrganizativa, " - ")
									Select Case UBound(vUnidadesOrganizativas)
										Case 0
											If sUnidadOrganizativa <> "" Then
												oDepartamentos.LoadData(lCiaComp, 0)
												oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
											End If
										Case 1
											oDepartamentos.LoadData(lCiaComp, 1, vUnidadesOrganizativas(0))
											oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
										Case 2
											oDepartamentos.LoadData(lCiaComp, 2, vUnidadesOrganizativas(0), vUnidadesOrganizativas(1))
											oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
										Case 3
											oDepartamentos.LoadData(lCiaComp, 3, vUnidadesOrganizativas(0), vUnidadesOrganizativas(1), vUnidadesOrganizativas(2))
											oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
									End Select

								Else
									oDepartamentos.LoadData(lCiaComp)
									oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)

								End If
								'Como texto ponemos el c�digo y la descripci�n porque si no puede haber problemas al ser controles que cargan los datos desde javascript y tener Infragistics 11.1 limitaciones en ese sentido
								oFSEntry.Text = TextoDelDropDown(oDepartamentos.Data, "COD", oDSRow.Item("VALOR_TEXT"), True)
								oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
							End If

							oFSEntry.Intro = 1

						Case PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras
							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								Dim oOrganizacionesCompras As PMPortalServer.OrganizacionesCompras
								oOrganizacionesCompras = FSPMServer.Get_OrganizacionesCompras
								oOrganizacionesCompras.LoadData(lCiaComp)

								oFSEntry.Text = TextoDelDropDown(oOrganizacionesCompras.Data, "COD", oDSRow.Item("VALOR_TEXT"), Not oFSEntry.ReadOnly)
								oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
							End If

							oFSEntry.Intro = 1
							lOrdenOrgCompras = oFSEntry.Orden
							Dim sCampoDependent As String
							Dim sql As String
							sql = "(TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =" & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & " OR TIPO_CAMPO_GS =" & PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo & ") AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 "

							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								If mdsCampos.Tables(0).Select(sql).Length > 0 Then
									sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID_CAMPO")
								End If
							Else
								If mdsCampos.Tables(0).Select(sql).Length > 0 Then
									sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID")
								End If
							End If
							If sCampoDependent <> "" Then
								oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry" & sCampoDependent
							End If

							sql = "(TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Centro & ") AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 "

							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								If mdsCampos.Tables(0).Select(sql).Length > 0 Then
									sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID_CAMPO")
								End If
							Else
								If mdsCampos.Tables(0).Select(sql).Length > 0 Then
									sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID")
								End If
							End If
							If sCampoDependent <> "" Then ' Relacionar la Organizacion de Compras con el Centro
								oFSEntry.idDataEntryDependent2 = oFSEntry.IdContenedor & "_" & "fsentry" & sCampoDependent
							End If

							sql = "(TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Proveedor & ") AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 "

							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								If mdsCampos.Tables(0).Select(sql).Length > 0 Then
									sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID_CAMPO")
								End If
							Else
								If mdsCampos.Tables(0).Select(sql).Length > 0 Then
									sCampoDependent = mdsCampos.Tables(0).Select(sql)(0).Item("ID")
								End If
							End If
							If sCampoDependent <> "" Then ' Relacionar la Organizacion de Compras con el Proveedor
								oFSEntry.DependentField = oFSEntry.IdContenedor & "_" & "fsentry" & sCampoDependent
							End If


							sidDataEntryOrgCompras = oFSEntry.IdContenedor & "_" & "fsentry" & oFSEntry.Tag

							sKeyOrgComprasDataCheck = oFSEntry.ClientID
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.Centro
							If sKeyOrgComprasDataCheck <> String.Empty Then _
								oFSEntry.MasterField = sKeyOrgComprasDataCheck

							'Centro es un campo relacionado por lo que la carga de datos se hace desde javascript(ajax)
							'Esta carga se hace �nicamente cuando el campo tiene un valor por defecto y hay que buscar la descripci�n del valor por defecto
							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								Dim oCentros As PMPortalServer.Centros
								oCentros = FSPMServer.Get_Centros
								sCodOrgCompras = DBNullToStr(NothingToDBNull(sCodOrgCompras))
								If sCodOrgCompras <> "" Then 'bCentroRelacionado
									oCentros.LoadData(lCiaComp, , sCodOrgCompras)
								Else
									oCentros.LoadData(lCiaComp)
								End If

								'Como texto ponemos el c�digo y la descripci�n porque si no puede haber problemas al ser controles que cargan los datos desde javascript y tener Infragistics 11.1 limitaciones en ese sentido
								oFSEntry.Text = TextoDelDropDown(oCentros.Data, "COD", oDSRow.Item("VALOR_TEXT"), True)

								oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
							End If

							Dim sScript As String
							''Variables de la ORGANIZACION DE COMPRAS (Carga las Varibles JS de la pag jsAlta.js)                          ''***************************************
							sScript = "var sCodOrgComprasFORM" & oFSEntry.Tag & "=null;var bCentroRelacionadoFORM" & oFSEntry.Tag & "=false;"

							sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
							If Not Page.ClientScript.IsStartupScriptRegistered("VariablesORGANIZACIONCOMPRAS" & oFSEntry.Tag) Then
								Page.ClientScript.RegisterStartupScript(Page.GetType, "VariablesORGANIZACIONCOMPRAS" & oFSEntry.Tag, sScript)
							End If

							oFSEntry.Intro = 1
							sIDCentro = oFSEntry.Tag
							lOrdenCentro = oFSEntry.Orden
							'Relacionar el Centro con el articulo, (Para cuando se elimine el centro que se elimine el articulo)
							Dim sCampoDepend As String
							Dim sql As String
							sql = "(TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =" & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & ") AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 "

							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								If mdsCampos.Tables(0).Select(sql).Length > 0 Then
									sCampoDepend = mdsCampos.Tables(0).Select(sql)(0).Item("ID_CAMPO")
								End If
							Else
								If mdsCampos.Tables(0).Select(sql).Length > 0 Then
									sCampoDepend = mdsCampos.Tables(0).Select(sql)(0).Item("ID")
								End If
							End If
							oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry" & sCampoDepend
							sIdDataEntryCentro = oFSEntry.IdContenedor & "_" & "fsentry" & oFSEntry.Tag
							oFSEntry.DependentValue = sCodOrgCompras

							'Relaci�n con Almacen
							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
									sKeyAlmacen = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
								End If
							Else
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oDSRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
									sKeyAlmacen = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
								End If
							End If
							If Not sKeyAlmacen Is Nothing Then
								oFSEntry.idDataEntryDependent3 = oFSEntry.IdContenedor & "_" & "fsentry" + sKeyAlmacen
							End If
							sKeyCentro = oFSEntry.ClientID
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.Almacen
							'Almac�n es un campo relacionado por lo que la carga de datos se hace desde javascript(ajax)
							'Esta carga se hace �nicamente cuando el campo tiene un valor por defecto y hay que buscar la descripci�n del valor por defecto
							If Not IsDBNull(oDSRow.Item("VALOR_NUM")) Then
								Dim oAlmacenes As PMPortalServer.Almacenes
								oAlmacenes = FSPMServer.Get_Almacenes
								If sIDCentro = "" Then
									oAlmacenes.LoadData(lCiaComp)
								Else
									oAlmacenes.LoadData(lCiaComp, , DBNullToStr(sCodCentro))
								End If
								'Como texto ponemos el c�digo y la descripci�n porque si no puede haber problemas al ser controles que cargan los datos desde javascript y tener Infragistics 11.1 limitaciones en ese sentido
								oFSEntry.Text = TextoDelDropDown(oAlmacenes.Data, "ID", oDSRow.Item("VALOR_NUM"), True)

								oFSEntry.Valor = oDSRow.Item("VALOR_NUM")
							End If
							oFSEntry.Intro = 1

							If sKeyCentro <> String.Empty Then _
								oFSEntry.MasterField = sKeyCentro
						Case TiposDeDatos.TipoCampoGS.Empresa
							If Not IsDBNull(oDSRow.Item("VALOR_NUM")) Then
								Dim oEmpresa As Fullstep.PMPortalServer.Empresa
								oEmpresa = FSPMServer.Get_Object(GetType(Fullstep.PMPortalServer.Empresa))
								oEmpresa.ID = oDSRow.Item("VALOR_NUM")
								oEmpresa.Load(lCiaComp, msIdi)
								If Not String.IsNullOrEmpty(oEmpresa.NIF) Then oFSEntry.Text = oEmpresa.NIF & " - " & oEmpresa.Den
								oEmpresa = Nothing

								oFSEntry.Valor = oDSRow.Item("VALOR_NUM")
							End If
							oFSEntry.Width = Unit.Percentage(100)
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.IniSuministro
							If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechas") Then
								Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechas", "<script>var sMensajeFecha = '" & oTextos.Rows.Item(10).Item(1) & "'</script>")
							End If
							Dim sIdCampo As String
							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.FinSuministro & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
									sIdCampo = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.FinSuministro & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
								End If
							Else
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.FinSuministro & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
									sIdCampo = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.FinSuministro & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
								End If
							End If
							oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_fsentry" & sIdCampo

						Case PMPortalServer.TiposDeDatos.TipoCampoGS.FinSuministro
							If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechas") Then
								Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechas", "<script>var sMensajeFecha = '" & oTextos.Rows.Item(10).Item(1) & "'</script>")
							End If
							Dim sIdCampo As String
							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.IniSuministro & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
									sIdCampo = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.IniSuministro & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
								End If
							Else
								If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.IniSuministro & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
									sIdCampo = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.IniSuministro & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
								End If
							End If
							oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_fsentry" & sIdCampo

						Case PMPortalServer.TiposDeDatos.TipoCampoGS.ImporteSolicitudesVinculadas
							oFSEntry.Tipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoCorto
							If ((mlInstanciaEstado = Fullstep.PMPortalServer.TipoEstadoSolic.SCPendiente) Or (mlInstanciaEstado = Fullstep.PMPortalServer.TipoEstadoSolic.SCAprobada) Or (mlInstanciaEstado = Fullstep.PMPortalServer.TipoEstadoSolic.SCCerrada)) Then
								Dim oInstancia As Fullstep.PMPortalServer.Instancia
								Dim aux As String
								oInstancia = FSPMServer.Get_Instancia
								oInstancia.ID = mlInstancia

								aux = oInstancia.DevolverImporteSolicitudesVinculadas(lCiaComp)
								oFSEntry.Text = FormatNumber(Replace(aux, ".", ","), oUser.NumberFormat)
								oFSEntry.Valor = aux
								oInstancia = Nothing
							Else
								oFSEntry.ReadOnly = True
							End If

						Case PMPortalServer.TiposDeDatos.TipoCampoGS.ImporteRepercutido
							If Not IsDBNull(oDSRow.Item("VALOR_NUM")) Then
								oFSEntry.Valor = oDSRow.Item("VALOR_NUM")
							End If
							If mlInstancia > 0 Then
								oFSEntry.MonRepercutido = oDSRow.Item("VALOR_TEXT")
								oFSEntry.CambioRepercutido = oDSRow.Item("CAMBIO")
							Else
								oFSEntry.MonRepercutido = oDSRow.Item("MONCEN")
								oFSEntry.CambioRepercutido = oDSRow.Item("CAMBIOCEN")
							End If

						Case PMPortalServer.TiposDeDatos.TipoCampoGS.RefSolicitud
							oFSEntry.ReadOnly = True

							If Not IsDBNull(oDSRow.Item("VALOR_NUM")) Then
								Dim oInstancia As Fullstep.PMPortalServer.Instancia
								Dim sTitulo As String
								oFSEntry.Valor = oDSRow.Item("VALOR_NUM")
								oInstancia = FSPMServer.Get_Instancia


								oInstancia.ID = oDSRow.Item("VALOR_NUM")
								sTitulo = oInstancia.DevolverTitulo(lCiaComp, msIdi)
								If sTitulo <> "" Then
									oFSEntry.Text = oDSRow.Item("VALOR_NUM") & " - " & sTitulo
								Else
									oFSEntry.Text = oDSRow.Item("VALOR_NUM")
								End If
								oInstancia = Nothing

							End If

							oFSEntry.Width = Unit.Percentage(100)
							If IsDBNull(oDSRow.Item("AVISOBLOQUEOIMPACUM")) Then
								oFSEntry.AvisoBloqueoRefSOL = -1
							Else
								oFSEntry.AvisoBloqueoRefSOL = oDSRow.Item("AVISOBLOQUEOIMPACUM")
							End If

						Case PMPortalServer.TiposDeDatos.TipoCampoGS.PrecioUnitario
							If Not IsDBNull(oDSRow.Item("CARGAR_ULT_ADJ")) Then
								oFSEntry.CargarUltADJ = oDSRow.Item("CARGAR_ULT_ADJ")
								If oFSEntry.CargarUltADJ = True Then
									Dim mioFSDSEntry As Fullstep.DataEntry.GeneralEntry
									Dim sCampoCodArt, sCampoCodArtAnt As String
									Dim sCampoDenArt As String
									If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
										If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
											sCampoCodArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID_CAMPO")
										End If
										If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
											sCampoDenArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID_CAMPO")
										End If
										If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
											sCampoCodArtAnt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID_CAMPO")
										End If
									Else
										If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
											sCampoCodArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID")
										End If
										If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
											sCampoDenArt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID")
										End If
										If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo).Length > 0 Then
											sCampoCodArtAnt = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oFSEntry.Grupo)(0).Item("ID")
										End If
									End If

									mioFSDSEntry = Me.FindControl("fsentry" & sCampoCodArt)
									If Not mioFSDSEntry Is Nothing Then
										mioFSDSEntry.WindowTitle = Me.Page.Title
										mioFSDSEntry.Portal = True
										mioFSDSEntry.idEntryPREC = oFSEntry.IdContenedor & "_" & oFSEntry.ClientID
									Else
										idEntryPREC = oFSEntry.ClientID
									End If
									mioFSDSEntry = Me.FindControl("fsentry" & sCampoDenArt)
									If Not mioFSDSEntry Is Nothing Then
										mioFSDSEntry.WindowTitle = Me.Page.Title
										mioFSDSEntry.idEntryPREC = oFSEntry.IdContenedor & "_" & oFSEntry.ClientID
									Else
										idEntryPREC = oFSEntry.ClientID
									End If
									mioFSDSEntry = Me.FindControl("fsentry" & sCampoCodArtAnt)
									If Not mioFSDSEntry Is Nothing Then
										mioFSDSEntry.WindowTitle = Me.Page.Title
										mioFSDSEntry.idEntryPREC = oFSEntry.IdContenedor & "_" & oFSEntry.ClientID
									Else
										idEntryPREC = oFSEntry.ClientID
									End If
								End If
							Else
								oFSEntry.CargarUltADJ = False
							End If

						Case TiposDeDatos.TipoCampoGS.UnidadOrganizativa
							sUnidadOrganizativa = ""
							sKeyUnidadOrganizativa = oFSEntry.ClientID

							If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Departamento & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
								Dim sKeyDepartamento As String = String.Empty
								If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
									sKeyDepartamento = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Departamento & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
								Else
									sKeyDepartamento = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Departamento & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
								End If
								oFSEntry.DependentField = "fsentry" & sKeyDepartamento
							End If

						Case PMPortalServer.TiposDeDatos.TipoCampoGS.CentroCoste
							If Not IsDBNull(oDSRow.Item("VER_UON")) Then
								oFSEntry.VerUON = oDSRow.Item("VER_UON")
							Else
								oFSEntry.VerUON = False
							End If
							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
								If InStrRev(oFSEntry.Valor, "#") >= 0 Then
									oFSEntry.Text = Right(oFSEntry.Valor, Len(oFSEntry.Valor) - InStrRev(oFSEntry.Valor, "#"))
								Else
									oFSEntry.Text = oFSEntry.Valor
								End If
								If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
									sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
									sDenominacion = oFSEntry.Text & " - " & sDenominacion
								End If

								oFSEntry.Text = sDenominacion
								oFSEntry.Denominacion = sDenominacion
							End If

						Case PMPortalServer.TiposDeDatos.TipoCampoGS.Partida
							oFSEntry.PRES5 = oDSRow.Item("PRES5")
							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
								If InStrRev(oFSEntry.Valor, "#") > 0 Then
									If InStrRev(oFSEntry.Valor, "|") > 0 Then 'si no est� en el nivel 1, si no m�s abajo
										oFSEntry.Text = Right(oFSEntry.Valor, Len(oFSEntry.Valor) - InStrRev(oFSEntry.Valor, "|"))
									Else
										oFSEntry.Text = Right(oFSEntry.Valor, Len(oFSEntry.Valor) - InStrRev(oFSEntry.Valor, "#"))
									End If
								Else
									oFSEntry.Text = oFSEntry.Valor
								End If
								If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
									sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
									sDenominacion = oFSEntry.Text & " - " & sDenominacion
								End If

								oFSEntry.Text = sDenominacion
								oFSEntry.Denominacion = sDenominacion
							End If
                            If Not IsDBNull(oDSRow.Item("CENTRO_COSTE")) Then
                                Dim sIdCampo As String
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.CentroCoste & " AND CAMPO_ORIGEN = " & oDSRow.Item("CENTRO_COSTE")).Length > 0 Then
                                        sIdCampo = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.CentroCoste & " AND CAMPO_ORIGEN = " & oDSRow.Item("CENTRO_COSTE"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    sIdCampo = oDSRow.Item("CENTRO_COSTE")
                                End If
                                oFSEntry.idEntryCC = oFSEntry.IdContenedor & "_fsentry" & sIdCampo
                            End If

                            sIDCampoPartida = oFSEntry.Tag
                        Case TiposDeDatos.TipoCampoGS.AnyoPartida
                            oFSEntry.Intro = 1

                            Dim sIdCampo As String = ""
                            If Not IsDBNull(oDSRow.Item("PARTIDAPRES")) Then
                                If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                                    If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Partida & " AND CAMPO_ORIGEN = " & oDSRow.Item("PARTIDAPRES")).Length > 0 Then
                                        sIdCampo = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Partida & " AND CAMPO_ORIGEN = " & oDSRow.Item("PARTIDAPRES"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    sIdCampo = oDSRow.Item("PARTIDAPRES")
                                End If
                                oFSEntry.idEntryPartidaPlurianual = oFSEntry.IdContenedor & "_fsentry" & sIdCampo
                                oFSEntry.EntryPartidaPlurianualCampo = 1
                            End If

                            If Not IsDBNull(oDSRow.Item("VALOR_NUM")) Then
                                oFSEntry.Valor = oDSRow.Item("VALOR_NUM")
                                oFSEntry.Text = oFSEntry.Valor
                            ElseIf sIdCampo <> "" Then
                                Dim mioFSDSEntry As DataEntry.GeneralEntry
                                mioFSDSEntry = Me.FindControl("fsentry" & sIdCampo)
                                If Not mioFSDSEntry Is Nothing Then
                                    Dim oAnyoPartida As PMPortalServer.Partida
                                    oAnyoPartida = FSPMServer.Get_Object(GetType(PMPortalServer.Partida))
                                    Dim dsAnyoPartida As DataSet = oAnyoPartida.DevolverDataAnyoPartida(lCiaComp, DBNullToStr(mioFSDSEntry.PRES5), DBNullToStr(NothingToDBNull(mioFSDSEntry.Valor)))
                                    If dsAnyoPartida.Tables(0).Rows.Count = 1 Then
                                        oFSEntry.Valor = dsAnyoPartida.Tables(0).Rows(0)("ANYO")
                                        oFSEntry.Text = oFSEntry.Valor
                                    End If
                                End If
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Activo
							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
								If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
									sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
								End If

								If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
									sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
									sDenominacion = oFSEntry.Valor & " - " & sDenominacion
								End If

								oFSEntry.Text = sDenominacion
								oFSEntry.Denominacion = sDenominacion
							End If

							If Not IsDBNull(oDSRow.Item("CENTRO_COSTE")) Then
								Dim sIdCampo As String
								If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
									If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.CentroCoste & " AND CAMPO_ORIGEN = " & oDSRow.Item("CENTRO_COSTE")).Length > 0 Then
										sIdCampo = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.CentroCoste & " AND CAMPO_ORIGEN = " & oDSRow.Item("CENTRO_COSTE"))(0).Item("ID_CAMPO")
									End If
								Else
									sIdCampo = oDSRow.Item("CENTRO_COSTE")
								End If
								oFSEntry.idEntryCC = oFSEntry.IdContenedor & "_fsentry" & sIdCampo
							End If
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.TipoPedido
							Dim oTiposPedido As PMPortalServer.TiposPedido
							Dim dsTiposPedidos As DataSet
							oTiposPedido = FSPMServer.Get_TiposPedido
							dsTiposPedidos = oTiposPedido.LoadData(lCiaComp, msIdi)
							'oFSEntry.Width = Unit.Pixel(300)
							oFSEntry.Width = Unit.Percentage(100)
							oFSEntry.ListaTiposPedido = dsTiposPedidos
							oFSEntry.Intro = 1
							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								oFSEntry.Valor = oDSRow.Item("VALOR_TEXT")
								If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
									sDenominacion = oFSEntry.Valor & " - " & PMPortalServer.Campo.ExtraerDescrIdioma(oDSRow.Item("VALOR_TEXT_DEN"), msIdi)
								Else
									If dsTiposPedidos.Tables(0).Rows.Count > 0 Then
										If dsTiposPedidos.Tables(0).Select("COD = '" & oDSRow.Item("VALOR_TEXT") & "'").Length > 0 Then
											sDenominacion = dsTiposPedidos.Tables(0).Select("COD = '" & oDSRow.Item("VALOR_TEXT") & "'")(0).Item("COD") & " - " & dsTiposPedidos.Tables(0).Select("COD = '" & oDSRow.Item("VALOR_TEXT") & "'")(0).Item("DEN").ToString
										End If
									Else
										sDenominacion = ""
									End If
								End If
								oFSEntry.Text = sDenominacion
								oFSEntry.Denominacion = sDenominacion

								If dsTiposPedidos.Tables(0).Rows.Count > 0 AndAlso dsTiposPedidos.Tables(0).Select("COD = '" & oDSRow.Item("VALOR_TEXT") & "'").Length > 0 Then
									oFSEntry.Concepto = dsTiposPedidos.Tables(0).Select("COD = '" & oDSRow.Item("VALOR_TEXT") & "'")(0).Item("CONCEPTO")
									oFSEntry.Almacenamiento = dsTiposPedidos.Tables(0).Select("COD = '" & oDSRow.Item("VALOR_TEXT") & "'")(0).Item("ALMACENAR")
									oFSEntry.Recepcion = dsTiposPedidos.Tables(0).Select("COD = '" & oDSRow.Item("VALOR_TEXT") & "'")(0).Item("RECEPCIONAR")
								End If
							End If
							oTiposPedido = Nothing

							Dim miPage As FSPMPage
							Dim moduloIdiomaAnterior As FSNLibrary.TiposDeDatos.ModulosIdiomas
							Dim arrConcepto As String()
							Dim arrAlmacenamiento As String()
							Dim arrRecepcion As String()

							miPage = CType(Page, FSPMPage)
							moduloIdiomaAnterior = miPage.ModuloIdioma
							miPage.ModuloIdioma = TiposDeDatos.ModulosIdiomas.BusquedaArticulos
							dsTiposPedidos.Tables(0).Columns(1).Caption = miPage.Textos(2)
							dsTiposPedidos.Tables(0).Columns(2).Caption = miPage.Textos(3)
							dsTiposPedidos.Tables(0).Columns(7).Caption = miPage.Textos(29)
							dsTiposPedidos.Tables(0).Columns(8).Caption = miPage.Textos(30)
							dsTiposPedidos.Tables(0).Columns(9).Caption = miPage.Textos(31)

							ReDim arrConcepto(2)
							arrConcepto(0) = miPage.Textos(32)
							arrConcepto(1) = miPage.Textos(33)
							arrConcepto(2) = miPage.Textos(20)

							ReDim arrAlmacenamiento(2)
							arrAlmacenamiento(0) = miPage.Textos(34)
							arrAlmacenamiento(1) = miPage.Textos(35)
							arrAlmacenamiento(2) = miPage.Textos(36)

							ReDim arrRecepcion(2)
							arrRecepcion(0) = miPage.Textos(37)
							arrRecepcion(1) = miPage.Textos(35)
							arrRecepcion(2) = miPage.Textos(36)

							For Each oDSRowTipoPedido As DataRow In dsTiposPedidos.Tables(0).Rows
								oDSRowTipoPedido.Item(7) = arrConcepto(oDSRowTipoPedido.Item(3))
								oDSRowTipoPedido.Item(8) = arrAlmacenamiento(oDSRowTipoPedido.Item(4))
								oDSRowTipoPedido.Item(9) = arrRecepcion(oDSRowTipoPedido.Item(5))
							Next

							miPage.ModuloIdioma = moduloIdiomaAnterior
					End Select
					If oDSRow.Item("TIPO") = 6 Then
						Dim bSinKeyArticulo As Boolean = True
						oFSEntry.Tabla_Externa = oDSRow.Item("TABLA_EXTERNA")
						If oFSEntry.Tipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoFecha AndAlso oFSEntry.Valor = New Date Then oFSEntry.Valor = Nothing
						If oDSRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
							If Not oFSEntry.Valor Is Nothing AndAlso Not IsDBNull(oDSRow.Item("VALOR_TEXT_DEN")) Then
								Dim sTxt As String = oDSRow.Item("VALOR_TEXT_DEN").ToString()
								Dim sTxt2 As String
								If sTxt.IndexOf("]#[") > 0 Then
									sTxt2 = sTxt.Substring(2, sTxt.IndexOf("]#[") - 2)
								Else
									sTxt2 = sTxt.Substring(2, sTxt.Length - 3)
								End If
								Select Case sTxt.Substring(0, 1)
									Case "s"
										oFSEntry.Text = sTxt2
									Case "n"
										oFSEntry.Text = Fullstep.FSNLibrary.FormatNumber(Numero(sTxt2, "."), oUser.NumberFormat)
									Case "d"
										oFSEntry.Text = CType(sTxt2, Date).ToString("d", oUser.DateFormat)
								End Select

								If sTxt.IndexOf("]#[") > 0 Then
									oFSEntry.Text = oFSEntry.Text & " "
									sTxt2 = sTxt.Substring(sTxt.IndexOf("]#[") + 3, sTxt.Length - sTxt.IndexOf("]#[") - 5)
									Select Case sTxt.Substring(sTxt.Length - 1, 1)
										Case "s"
											oFSEntry.Text = oFSEntry.Text & sTxt2
										Case "n"
											oFSEntry.Text = oFSEntry.Text & Fullstep.FSNLibrary.FormatNumber(Numero(sTxt2, "."), oUser.NumberFormat)
										Case "d"
											oFSEntry.Text = oFSEntry.Text & CType(sTxt2, Date).ToString("d", oUser.DateFormat)
									End Select
								End If
							End If
							If Not IsDBNull(oDSRow.Item("VALOR_TEXT_COD")) Then
								oFSEntry.codigoArticulo = oDSRow.Item("VALOR_TEXT_COD")
							End If
							Dim oTablaExterna As PMPortalServer.TablaExterna = FSPMServer.get_TablaExterna
							oTablaExterna.LoadDefTabla(lCiaComp, oFSEntry.Tabla_Externa, False)
							bSinKeyArticulo = oTablaExterna.ArtKey Is Nothing
						Else
							Dim oTablaExterna As PMPortalServer.TablaExterna = FSPMServer.get_TablaExterna
							oTablaExterna.LoadDefTabla(lCiaComp, oFSEntry.Tabla_Externa, False)
							oTablaExterna.LoadData(lCiaComp, False)
							If Not oFSEntry.Valor Is Nothing AndAlso oFSEntry.Valor <> "" Then
								oFSEntry.Text = oTablaExterna.DescripcionReg(lCiaComp, oFSEntry.Valor, msIdi, oUser.DateFormat, oUser.NumberFormat)
								oFSEntry.codigoArticulo = oTablaExterna.CodigoArticuloReg(oFSEntry.Valor)
								oFSEntry.Width = Unit.Percentage(100)
							End If
							bSinKeyArticulo = oTablaExterna.ArtKey Is Nothing
						End If
						oFSEntry.Denominacion = oFSEntry.Text
						oFSEntry.Width = Unit.Percentage(100)
						If sKeyArticulo Is Nothing Then
							For Each oRow As DataRow In mdsCampos.Tables(0).Rows
								If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
									If Not mdsCampos.Tables(0).Columns("ID_CAMPO") Is Nothing Then
										sKeyArticulo = "fsentry" + oRow.Item("ID_CAMPO").ToString
									Else
										sKeyArticulo = "fsentry" + oRow.Item("ID").ToString
									End If
									Exit For
								End If
							Next
						End If
						If Not (sKeyArticulo Is Nothing Or bSinKeyArticulo) Then _
							oFSEntry.DependentField = sKeyArticulo
					ElseIf oDSRow.Item("TIPO") = 7 Then
						oFSEntry.Servicio = oDSRow.Item("SERVICIO")
					End If

					If oDSRow.Item("INTRO") = 0 Then
						Select Case oFSEntry.Tipo
							Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoString, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio
								oFSEntry.InputStyle.CssClass = "TipoTextoMedio"

								If oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo Then
									oFSEntry.MaxLength = FSPMServer.LongitudesDeCodigos.giLongCodDENART
								Else
									If oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo Or oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
										oFSEntry.MaxLength = FSPMServer.LongitudesDeCodigos.giLongCodART
									Else
										If PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio And Not IsDBNull(oDSRow.Item("MAX_LENGTH")) Then
											oFSEntry.MaxLength = oDSRow.Item("MAX_LENGTH")
										Else
											oFSEntry.MaxLength = 800
										End If
									End If
								End If
								oFSEntry.ErrMsg = oTextos.Rows(11).Item(1)

							Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico
								oFSEntry.InputStyle.CssClass = "TipoNumero"
							Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoFecha
								oFSEntry.InputStyle.CssClass = "TipoFecha"
							Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoCorto
								If Not IsDBNull(oDSRow.Item("MAX_LENGTH")) Then
									oFSEntry.MaxLength = oDSRow.Item("MAX_LENGTH")
								Else
									oFSEntry.MaxLength = 100
								End If
								oFSEntry.InputStyle.CssClass = "TipoTextoCorto"
							Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoLargo
								oFSEntry.PopUpStyle = "../../App_Themes/" & Me.Page.Theme & "/" & Me.Page.Theme & ".css"
								oFSEntry.PUCaptionBoton = oTextos.Rows(4).Item(1)
								oFSEntry.InputStyle.CssClass = "TipoTextoLargo"
								If Not IsDBNull(oDSRow.Item("MAX_LENGTH")) Then
									oFSEntry.MaxLength = oDSRow.Item("MAX_LENGTH")
								Else
									oFSEntry.MaxLength = 4000
								End If
								oFSEntry.Width = Unit.Percentage(100)
								oFSEntry.ErrMsg = oTextos.Rows(11).Item(1)
							Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoArchivo
								oFSEntry.InputStyle.CssClass = "TipoArchivo"
								oFSEntry.ErrMsg = oTextos.Rows(11).Item(1)
							Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoBoolean
								oFSEntry.Lista = Fullstep.PMPortalWeb.CommonInstancia.CrearDataset(msIdi, oTextos.Rows(16).Item(1), oTextos.Rows(17).Item(1))
								oFSEntry.Intro = 1
						End Select
					End If

					If {PMPortalServer.TiposDeDatos.TipoCampoGS.Almacen, PMPortalServer.TiposDeDatos.TipoCampoGS.Empresa}.Contains(oFSEntry.TipoGS) Then
						oFSEntry.Width = Unit.Percentage(100)
					End If

					If oFSEntry.ReadOnly Then
						oFSEntry.LabelStyle.CssClass = "CampoSoloLectura"
						oFSEntry.InputStyle.CssClass = "trasparent"
					End If

					If Page.IsPostBack Then
						If (Request.Form.AllKeys.Contains(oFSEntry.SubmitKey)) Then
							oFSEntry.Valor = Request(oFSEntry.SubmitKey)
						End If
					End If

					otblCell.Controls.Add(oFSEntry)
					otblCell.CssClass = sClase
					otblRow.Cells.Add(otblCell)

					otblRow.Height = Unit.Pixel(20)

					Me.tblDesglose.Rows.Add(otblRow)

					If sClase = "ugfilatabla" Then
						sClase = "ugfilaalttabla"
					Else
						sClase = "ugfilatabla"
					End If

					Select Case oFSEntry.TipoGS
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.UnidadOrganizativa
							bDepartamentoRelacionado = True
							sUnidadOrganizativa = oFSEntry.Valor
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras
							bCentroRelacionado = True
							sCodOrgCompras = oFSEntry.Valor
							sIDCampoOrgCompras = oFSEntry.Tag
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.Centro
							bAlmacenRelacionado = True
							sCodCentro = oFSEntry.Valor
							sIdCampoCentro = oFSEntry.Tag
							'Por si la organizacion de Compras est� oculto actualizar si est� relacionado o no con el centro
							If lOrdenOrgCompras = lOrdenCentro - 1 Then
								bCentroRelacionado = True
							End If
						Case Else
							bDepartamentoRelacionado = False
							bCentroRelacionado = False
							bAlmacenRelacionado = False
					End Select
				Else
					'Para Cuando el CodArticulo No este visible
					Select Case modUtilidades.DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS"))
						Case PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo
							Dim idCampoMat As Long
							Dim sValorMat As String

							idCampoMat = 0

							'OBTENEMOS EL VALOR DEL MAT OCULTO
							If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
								For Each i In arrCamposMaterial
									If i < oDSRow.Item("id_campo") Then
										If idCampoMat < i Then
											idCampoMat = i
										End If
									End If
								Next
								If idCampoMat > 0 Then
									Try
										sValorMat = mdsCampos.Tables(0).Select("ID_CAMPO = " & idCampoMat)(0).Item("VALOR_TEXT")

									Catch ex As Exception

									End Try
								End If
							Else
								For Each i In arrCamposMaterial
									If i < oDSRow.Item("ID") Then
										If idCampoMat < i Then
											idCampoMat = i
										End If
									End If
								Next
								If idCampoMat > 0 Then
									Try
										sValorMat = mdsCampos.Tables(0).Select("ID = " & idCampoMat)(0).Item("VALOR_TEXT")

									Catch ex As Exception

									End Try
								End If
							End If

							For i = 1 To 4
								arrMat(i) = ""
							Next i

							Dim lLongCod As Long
							i = 1
							While Trim(sValorMat) <> ""
								Select Case i
									Case 1
										lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
									Case 2
										lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
									Case 3
										lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
									Case 4
										lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN4
								End Select
								arrMat(i) = Trim(Mid(sValorMat, 1, lLongCod))
								sValorMat = Mid(sValorMat, lLongCod + 1)
								i = i + 1
							End While

							If Not IsDBNull(oDSRow.Item("VALOR_TEXT")) Then
								sCodArticulo = oDSRow.Item("VALOR_TEXT")
								oArts = FSPMServer.Get_Articulos
								Dim oArticulo As Fullstep.PMPortalServer.PropiedadesArticulo = oArts.Articulo_Get_Properties(lCiaComp, oDSRow.Item("VALOR_TEXT"))
								If oArticulo.ExisteArticulo Then
									bArticuloGenerico = oArticulo.Generico
									sKeyTipoRecepcion = oArticulo.TipoRecepcion
								End If
								Dim bAnyadir_art As Boolean
								bAnyadir_art = oDSRow.Item("ANYADIR_ART")
								Dim idDataEntryMat As String

								If Not Me.ClientID Is Nothing Then
									idDataEntryMat = Me.ClientID & "_"
								End If

								If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
									idDataEntryMat = idDataEntryMat & "fsentry" & (oDSRow.Item("ID_CAMPO") - 1)
								Else
									idDataEntryMat = idDataEntryMat & "fsentry" & (oDSRow.Item("ID") - 1)
								End If
							End If
					End Select
				End If

				If oDSRow.Item("VISIBLE") = 1 And oDSRow.Item("SUBTIPO") = PMPortalServer.TiposDeDatos.TipoGeneral.TipoDesglose And oDSRow.Item("POPUP") = 0 Then

					If (DBNullToSomething(oDSRow.Item("OBLIGATORIO")) = 1) Then
						oFSEntry = New Fullstep.DataEntry.GeneralEntry
						oFSEntry.Portal = True
						oFSEntry.Width = Unit.Percentage(100)
						If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
							oFSEntry.Tag = oDSRow.Item("ID_CAMPO").ToString
							oFSEntry.ID = "OBLIG_" + Me.ID + "_" + oDSRow.Item("ID_CAMPO").ToString

							oFSEntry.CampoDef_CampoODesgHijo = 0 'Es un campo desglose, es decir, el padre
							oFSEntry.CampoDef_DesgPadre = oDSRow.Item("ID")
						Else
							oFSEntry.Tag = oDSRow.Item("ID").ToString
							oFSEntry.ID = "OBLIG_" + Me.ID + "_" + oDSRow.Item("ID").ToString
						End If
						oFSEntry.IdContenedor = Me.ClientID
						'Cambio de las doble comillas para que en JavaScript no de error
						oDSRow.Item("DEN_" & msIdi) = oDSRow.Item("DEN_" & msIdi).ToString.Replace("'", "\'")
						oDSRow.Item("DEN_" & msIdi) = oDSRow.Item("DEN_" & msIdi).ToString.Replace("""", "\""")
						oFSEntry.Title = oDSRow.Item("DEN_" & msIdi)
						oFSEntry.WindowTitle = Me.Page.Title
						oFSEntry.Intro = oDSRow.Item("INTRO")
						oFSEntry.Idi = msIdi
						oFSEntry.TabContainer = msTabContainer
						oFSEntry.TipoGS = modUtilidades.DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS"))
						oFSEntry.ReadOnly = (oDSRow.Item("ESCRITURA") = 0) Or mbSoloLectura
						oFSEntry.Obligatorio = True
						oFSEntry.Orden = oDSRow.Item("ORDEN")
						oFSEntry.Grupo = oDSRow.Item("GRUPO")
						oFSEntry.Tipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoDesgloseOblig
						oFSEntry.BaseDesglose = False
						oFSEntry.ActivoSM = CType(Me.Page, FSPMPage).Acceso.gbAccesoFSSM

						otblRow = New System.Web.UI.WebControls.TableRow
						otblCell = New System.Web.UI.WebControls.TableCell
						otblCell.Controls.Add(oFSEntry)
						otblRow.Cells.Add(otblCell)
						Me.tblDesglose.Rows.Add(otblRow)
					End If

					Dim oucDesglose As Fullstep.PMPortalWeb.desgloseControl = LoadControl("desglose.ascx")
					nColumna = nColumna + 1
					otblRow = New System.Web.UI.WebControls.TableRow
					otblCell = New System.Web.UI.WebControls.TableCell
					otblCell.Width = Unit.Percentage(100)
					otblCell.ColumnSpan = 4

					If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
						oucDesglose.ID = Me.ID + "_" + oDSRow.Item("ID_CAMPO").ToString
						oucDesglose.Campo = oDSRow.Item("ID_CAMPO")
						oucDesglose.TieneIdCampo = True
					Else
						oucDesglose.ID = Me.ID + "_" + oDSRow.Item("ID").ToString
						oucDesglose.TieneIdCampo = False
						oucDesglose.Campo = oDSRow.Item("ID")
					End If

					oucDesglose.TabContainer = Me.ClientID
					oucDesglose.Instancia = mlInstancia
					oucDesglose.Solicitud = mlSolicitud
					oucDesglose.Version = mlVersion
					oucDesglose.SoloLectura = mbSoloLectura Or (oDSRow.Item("ESCRITURA") = 0)
					oucDesglose.Acciones = moAcciones
					oucDesglose.DSEstados = moDSEstados
					If DBNullToSomething(oDSRow.Item("OBLIGATORIO")) = 1 Then
						oucDesglose.Titulo = "(*)" & "" & oDSRow.Item("DEN_" & msIdi)
					Else
						oucDesglose.Titulo = oDSRow.Item("DEN_" & msIdi)
					End If
					oucDesglose.Ayuda = DBNullToSomething(oDSRow.Item("AYUDA_" & msIdi))
					oucDesglose.CentroRelacionado = bCentroRelacionado
					oucDesglose.CodOrgCompras = sCodOrgCompras
					oucDesglose.CodCentro = sCodCentro
					oucDesglose.idCampoOrgCompras = sIDCampoOrgCompras
					oucDesglose.InstanciaMoneda = msInstanciaMoneda
                    oucDesglose.TipoSolicitud = mTipoSolicitud
                    If sIDCampoPartida Is Nothing Then
                        'Si el campo partida est� despu�s del desglose todav�a no se le ha dado valor a sIDCampoPartida                        
                        If Not mdsCampos.Tables(0).Columns("id_campo") Is Nothing Then
                            If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Partida & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                sIDCampoPartida = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Partida & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                            End If
                        Else
                            If mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Partida & " AND GRUPO = " & oDSRow.Item("GRUPO")).Length > 0 Then
                                sIDCampoPartida = mdsCampos.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Partida & " AND GRUPO = " & oDSRow.Item("GRUPO"))(0).Item("ID")
                            End If
                        End If
                    End If
                    oucDesglose.idCampoPartida = sIDCampoPartida
                    If sIDCampoOrgCompras <> "" Then
						oucDesglose.idDataEntryFORMOrgCompras = sidDataEntryOrgCompras
					End If
					If bCentroRelacionado Then
						oucDesglose.idDataEntryFORMCentro = sIdDataEntryCentro
						oucDesglose.idCampoCentro = sIdCampoCentro
					End If
					If TipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad Or TipoSolicitud = TiposDeDatos.TipoDeSolicitud.Certificado Then
						oucDesglose.EsQA = True
					End If
					otblCell.Controls.Add(oucDesglose)
					otblRow.Cells.Add(otblCell)
					otblCell.CssClass = sClase
					otblRow.Style("vertical-align") = "top"
					Me.tblDesglose.Rows.Add(otblRow)
				ElseIf oDSRow.Item("VISIBLE") = 1 And oDSRow.Item("SUBTIPO") = PMPortalServer.TiposDeDatos.TipoGeneral.TipoDesglose And oDSRow.Item("POPUP") = 1 Then
					Dim sScript As String

					If sidDataEntryOrgCompras <> "" Then
						sScript = "var sDataEntryOrgComprasFORM='" & sidDataEntryOrgCompras & "'"
						sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
						If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "VariablesORGANIZACIONCOMPRASEntry" & oFSEntry.Tag) Then _
						Page.ClientScript.RegisterStartupScript(Page.GetType(), "VariablesORGANIZACIONCOMPRASEntry" & oFSEntry.Tag, sScript)
					ElseIf sCodOrgCompras <> "" Then
						sScript = "var sCodOrgComprasFORM='" & sCodOrgCompras & "'"
						sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
						If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "VariablesORGANIZACIONCOMPRASEntry" & oFSEntry.Tag) Then _
						Page.ClientScript.RegisterStartupScript(Page.GetType(), "VariablesORGANIZACIONCOMPRASEntry" & oFSEntry.Tag, sScript)
					End If
					If sIdDataEntryCentro <> "" Then
						sScript = "var sDataEntryCentroFORM='" & sIdDataEntryCentro & "'"
						sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
						If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "VariablesCENTROEntry" & oFSEntry.Tag) Then _
						Page.ClientScript.RegisterStartupScript(Page.GetType(), "VariablesCENTROEntry" & oFSEntry.Tag, sScript)
					ElseIf sCodCentro <> "" Then
						sScript = "var sCodCentroFORM='" & sCodCentro & "'"
						sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
						If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "VariablesCENTROEntry" & oFSEntry.Tag) Then _
						Page.ClientScript.RegisterStartupScript(Page.GetType(), "VariablesCENTROEntry" & oFSEntry.Tag, sScript)
					End If
					If Not String.IsNullOrEmpty(sIdDataentryMaterial) Then
						sScript = "sDataEntryMaterialFORM='" & sIdDataentryMaterial & "'"
						sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
						If Not Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "VariablesMATERIALEntry" & oFSEntry.Tag) Then _
							 Page.ClientScript.RegisterStartupScript(Page.GetType(), "VariablesMATERIALEntry" & oFSEntry.Tag, sScript)
					End If
				End If
			Next

			Me.Page.ClientScript.RegisterClientScriptResource(GetType(Fullstep.DataEntry.GeneralEntry), "Fullstep.DataEntry.dateCalendar.js")
        End Sub

        ''' <summary>
        ''' Comprobac��n de bloqueos por campos
        ''' </summary>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="sFormula">Formula</param>
        ''' <param name="drCondiciones">Lista de bloqueos por campos</param>
        ''' <param name="dsCampos">Tabla Campos</param>
        ''' <returns>Si hay bloqueo o no</returns>
        ''' <remarks>Llamada desde:Page_load; Tiempo m�ximo:0,1</remarks>
        Private Function ComprobarCondiciones(ByVal sFormula As String, ByVal drCondiciones As DataRow(), ByVal dsCampos As DataSet, ByVal dCambioVigenteInstancia As Double) As Boolean
            Dim drCampo As DataRow
            Dim oValorCampo As Object
            Dim oValor As Object
            Dim iTipo As Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral
            Dim oCond As DataRow
            Dim i As Long
            Dim sVariables() As String
            Dim dValues() As Double
            Dim iEq As New USPExpress.USPExpression
            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
            Dim lCiaComp As Long = Session("FS_Portal_User").CiaComp
            Dim oInstancia As Fullstep.PMPortalServer.Instancia

            Dim sMoneda As String = ""
            Dim dCambioEnlace As Double

            Try
                oInstancia = FSWSServer.Get_Instancia

                If mlInstancia > 0 Then
                    oInstancia.ID = mlInstancia
                    oInstancia.Load(lCiaComp, msIdi)
                    oInstancia.Version = mlVersion
                End If

                i = 0

                For Each oCond In drCondiciones
                    ReDim Preserve sVariables(i)
                    ReDim Preserve dValues(i)

                    sVariables(i) = oCond.Item("COD")
                    dValues(i) = 0 'El valor ser� 1 o 0 dependiendo de si se cumple o no la condici�n

                    sMoneda = DBNullToStr(oCond.Item("MON"))
                    dCambioEnlace = DBNullToDbl(oCond.Item("CAMBIO"))

                    '<EXPRESION IZQUIEDA> <OPERADOR> <EXPRESION DERECHA>
                    'Primero evaluamos <EXPRESION IZQUIERDA> 
                    Select Case oCond.Item("TIPO_CAMPO")
                        Case 1 'Campo de formulario

                            If dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_DATO")).Length = 0 Then
                                If mlInstancia > 0 Then
                                    dsCampos = oInstancia.cargarCampo(lCiaComp, oCond.Item("CAMPO_DATO"))
                                End If
                            End If

                            If dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_DATO")).Length > 0 Then
                                Dim bDesglose As Boolean = False

                                drCampo = dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_DATO"))(0)

                                Select Case drCampo.Item("SUBTIPO")
                                    Case 2
                                        oValorCampo = DBNullToSomething(drCampo.Item("VALOR_NUM"))
                                    Case 3
                                        oValorCampo = DBNullToSomething(drCampo.Item("VALOR_FEC"))
                                    Case 4
                                        oValorCampo = DBNullToSomething(drCampo.Item("VALOR_BOOL"))
                                    Case Else
                                        oValorCampo = DBNullToStr(drCampo.Item("VALOR_TEXT"))
                                        Dim sPresup() As String
                                        Dim lIdPres As Integer

                                        If DBNullToSomething(drCampo.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.PRES1 Then
                                            sPresup = Split(drCampo.Item("VALOR_TEXT"), "_")
                                            lIdPres = CInt(sPresup(1))

                                            Dim oPres1 As Fullstep.PMPortalServer.PresProyectosNivel1 = FSWSServer.Get_PresProyectosNivel1
                                            Select Case CInt(sPresup(0))
                                                Case 1
                                                    oPres1.LoadData(lCiaComp, lIdPres)
                                                Case 2
                                                    oPres1.LoadData(lCiaComp, Nothing, lIdPres)
                                                Case 3
                                                    oPres1.LoadData(lCiaComp, Nothing, Nothing, lIdPres)
                                                Case 4
                                                    oPres1.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPres)
                                            End Select
                                            oValorCampo = oPres1.Data.Tables(0).Rows(0).Item("ANYO") + " - " + oPres1.Data.Tables(0).Rows(0).Item("PRES" & sPresup(0))
                                            oPres1 = Nothing
                                        ElseIf DBNullToSomething(drCampo.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Pres2 Then
                                            sPresup = Split(drCampo.Item("VALOR_TEXT"), "_")
                                            lIdPres = CInt(sPresup(1))

                                            Dim oPres2 As Fullstep.PMPortalServer.PresContablesNivel1 = FSWSServer.Get_PresContablesNivel1
                                            Select Case CInt(sPresup(0))
                                                Case 1
                                                    oPres2.LoadData(lCiaComp, lIdPres)
                                                Case 2
                                                    oPres2.LoadData(lCiaComp, Nothing, lIdPres)
                                                Case 3
                                                    oPres2.LoadData(lCiaComp, Nothing, Nothing, lIdPres)
                                                Case 4
                                                    oPres2.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPres)
                                            End Select
                                            oValorCampo = oPres2.Data.Tables(0).Rows(0).Item("ANYO") + " - " + oPres2.Data.Tables(0).Rows(0).Item("PRES" & sPresup(0))
                                            oPres2 = Nothing
                                        ElseIf DBNullToSomething(drCampo.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Pres3 Then
                                            sPresup = Split(drCampo.Item("VALOR_TEXT"), "_")
                                            lIdPres = CInt(sPresup(1))

                                            Dim oPres3 As Fullstep.PMPortalServer.PresConceptos3Nivel1 = FSWSServer.Get_PresConceptos3Nivel1
                                            Select Case CInt(sPresup(0))
                                                Case 1
                                                    oPres3.LoadData(lCiaComp, lIdPres)
                                                Case 2
                                                    oPres3.LoadData(lCiaComp, Nothing, lIdPres)
                                                Case 3
                                                    oPres3.LoadData(lCiaComp, Nothing, Nothing, lIdPres)
                                                Case 4
                                                    oPres3.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPres)
                                            End Select
                                            oValorCampo = oPres3.Data.Tables(0).Rows(0).Item("PRES" & sPresup(0))
                                            oPres3 = Nothing
                                        ElseIf DBNullToSomething(drCampo.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Pres4 Then
                                            sPresup = Split(drCampo.Item("VALOR_TEXT"), "_")
                                            lIdPres = CInt(sPresup(1))

                                            Dim oPres4 As Fullstep.PMPortalServer.PresConceptos4Nivel1 = FSWSServer.Get_PresConceptos4Nivel1
                                            Select Case CInt(sPresup(0))
                                                Case 1
                                                    oPres4.LoadData(lCiaComp, lIdPres)
                                                Case 2
                                                    oPres4.LoadData(lCiaComp, Nothing, lIdPres)
                                                Case 3
                                                    oPres4.LoadData(lCiaComp, Nothing, Nothing, lIdPres)
                                                Case 4
                                                    oPres4.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPres)
                                            End Select
                                            oValorCampo = oPres4.Data.Tables(0).Rows(0).Item("PRES" & sPresup(0))
                                            oPres4 = Nothing
                                        End If
                                End Select


                                iTipo = drCampo.Item("SUBTIPO")
                                If (sMoneda <> "" Or oCond.Item("TIPO_VALOR") = 6 Or oCond.Item("TIPO_VALOR") = 7) Then
                                    If dCambioVigenteInstancia <> 0 Then
                                        oValorCampo = oValorCampo / dCambioVigenteInstancia
                                    End If
                                End If
                            End If

                        Case 2 'Peticionario
                            If mlInstancia > 0 Then
                                oValorCampo = oInstancia.Peticionario
                            End If
                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio

                        Case 3 'Departamento del peticionario
                            If mlInstancia > 0 Then
                                Dim oPer As Fullstep.PMPortalServer.Persona = FSWSServer.Get_Persona
                                oPer.LoadData(lCiaComp, oInstancia.Peticionario)
                                oValorCampo = oPer.Departamento
                            End If

                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio
                        Case 4 'UON del peticionario

                            If mlInstancia > 0 Then
                                Dim oPer As Fullstep.PMPortalServer.Persona = FSWSServer.Get_Persona
                                oPer.LoadData(lCiaComp, oInstancia.Peticionario)
                                oValorCampo = oPer.UONs
                            End If
                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio

                        Case 5 'N� de procesos de compra abiertos
                            oValorCampo = 0
                            If mlInstancia > 0 Then
                                oInstancia.DevolverProcesosRelacionados()
                                oValorCampo = oInstancia.Procesos.Tables(0).Rows.Count
                            End If
                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico

                        Case 6 'Importe adj (LO DEVUELVE EN MONEDA CENTRAL...Y SE COMPARA CON MONEDA DEL FORMULARIO --->PENDIENTE DE CAMBIAR)

                            '"SELECT SUM(A.PREC) IMP_ADJ FROM AR_ITEM A INNER JOIN ITEM I ON A.ANYO=I.ANYO AND A.GMN1=I.GMN1 AND A.PROCE=I.PROCE AND A.ITEM=I.ID WHERE I.SOLICIT = @INSTANCIA "
                            oValorCampo = 0
                            If mlInstancia > 0 Then
                                oValorCampo = oInstancia.ObtenerImporteAdjudicado
                            End If
                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico

                        Case 7 'Importe de la solicitud de compra
                            oValorCampo = 0
                            If mlInstancia > 0 Then
                                oValorCampo = oInstancia.Importe
                            End If

                            If sMoneda <> "" AndAlso dCambioVigenteInstancia <> 0 Then
                                oValorCampo = oValorCampo / dCambioVigenteInstancia
                            End If
                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico
                        Case 10 'ImporteFactura
                            'Portal el DetalleFactura.aspx.vb no hace uso de Fullstep.PMPortalWeb.campos. 
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                        Case 11 'ImporteCostesPlanificados
                            'Portal el DetalleFactura.aspx.vb no hace uso de Fullstep.PMPortalWeb.campos. 
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                        Case 12 'ImporteCostesNoPlanificados
                            'Portal el DetalleFactura.aspx.vb no hace uso de Fullstep.PMPortalWeb.campos. 
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                        Case 13 'ImporteDctosPlanificados
                            'Portal el DetalleFactura.aspx.vb no hace uso de Fullstep.PMPortalWeb.campos. 
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                        Case 14 'ImporteDctosNoPlanificados
                            'Portal el DetalleFactura.aspx.vb no hace uso de Fullstep.PMPortalWeb.campos. 
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                        Case 15 'NumDiscrepanciasAbiertas
                            'Portal el DetalleFactura.aspx.vb no hace uso de Fullstep.PMPortalWeb.campos. 
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                    End Select

                    'Luego <EXPRESION DERECHA>
                    Select Case oCond.Item("TIPO_VALOR")
                        Case 1 'Campo de formulario
                            If dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_VALOR")).Length = 0 Then
                                If mlInstancia > 0 Then
                                    dsCampos = oInstancia.cargarCampo(lCiaComp, oCond.Item("CAMPO_VALOR"))
                                End If
                            End If

                            If dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_VALOR")).Length > 0 Then
                                drCampo = dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_VALOR"))(0)
                                Select Case drCampo.Item("SUBTIPO")
                                    Case 2
                                        oValor = DBNullToSomething(drCampo.Item("VALOR_NUM"))
                                    Case 3
                                        oValor = DBNullToSomething(drCampo.Item("VALOR_FEC"))
                                    Case 4
                                        oValor = DBNullToSomething(drCampo.Item("VALOR_BOOL"))
                                    Case Else
                                        oValor = DBNullToSomething(drCampo.Item("VALOR_TEXT"))
                                End Select
                                If (sMoneda <> "" Or oCond.Item("TIPO_CAMPO") = 6 Or oCond.Item("TIPO_CAMPO") = 7) Then
                                    If dCambioVigenteInstancia <> 0 Then
                                        oValor = oValor / dCambioVigenteInstancia
                                    End If
                                End If
                            End If
                        Case 2 'Peticionario
                            oValorCampo = oInstancia.Peticionario
                        Case 3 'Departamento del peticionario
                            Dim oPer As Fullstep.PMPortalServer.Persona = FSWSServer.Get_Persona
                            oPer.LoadData(lCiaComp, oInstancia.Peticionario)
                            oValorCampo = oPer.Departamento
                        Case 4 'UON del peticionario
                            Dim oPer As Fullstep.PMPortalServer.Persona = FSWSServer.Get_Persona
                            oPer.LoadData(lCiaComp, oInstancia.Peticionario)
                            oValorCampo = oPer.UONs
                        Case 5 'N� de procesos de compra abiertos
                            If mlInstancia = 0 Then
                                oValorCampo = 0
                            Else
                                oInstancia.DevolverProcesosRelacionados()
                                oValorCampo = oInstancia.Procesos.Tables(0).Rows.Count
                            End If
                        Case 6 'Importe adj (LO DEVUELVE EN MONEDA CENTRAL...Y SE COMPARA CON MONEDA DEL FORMULARIO --->PENDIENTE DE CAMBIAR)
                            If mlInstancia = 0 Then
                                oValorCampo = 0
                            Else
                                oValorCampo = oInstancia.ObtenerImporteAdjudicado
                            End If
                        Case 7 'Importe de la solicitud de compra
                            oValorCampo = oInstancia.Importe

                            If dCambioVigenteInstancia <> 0 Then
                                oValorCampo = oValorCampo / dCambioVigenteInstancia
                            End If
                        Case 10 'valor est�tico
                            Select Case iTipo
                                Case 2
                                    oValor = DBNullToSomething(oCond.Item("VALOR_NUM"))
                                Case 3
                                    oValor = DBNullToSomething(oCond.Item("VALOR_FEC"))
                                Case 4
                                    oValor = DBNullToSomething(oCond.Item("VALOR_BOOL"))
                                Case Else
                                    If oCond.Item("TIPO_CAMPO") = 4 Then 'UON DEL PETICIONARIO
                                        Dim oUON() As String
                                        Dim iIndice As Integer
                                        oValor = Nothing
                                        oUON = Split(oCond.Item("VALOR_TEXT"), "-")
                                        For iIndice = 0 To UBound(oUON)
                                            If oUON(iIndice) <> "" Then
                                                oValor = oValor & Trim(oUON(iIndice)) & "-"
                                            End If
                                        Next iIndice
                                        oValor = Left(oValor, Len(oValor) - 1)
                                    Else
                                        oValor = DBNullToSomething(oCond.Item("VALOR_TEXT"))
                                    End If
                            End Select
                            If sMoneda <> "" And dCambioEnlace <> 0 Then
                                oValor = oValor / dCambioEnlace
                            ElseIf (oCond.Item("TIPO_CAMPO") = 6 Or oCond.Item("TIPO_CAMPO") = 7) And dCambioVigenteInstancia <> 0 Then
                                oValor = oValor / dCambioVigenteInstancia
                            End If
                    End Select

                    'y por �ltimo con el OPERADOR obtenemos el valor

                    Select Case iTipo
                        Case 2, 3
                            If oValorCampo Is Nothing Then 'Condici�n vac�a
                                If oValor Is Nothing Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Else
                                Select Case oCond.Item("OPERADOR")
                                    Case ">"
                                        If oValorCampo > oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If

                                    Case "<"
                                        If oValorCampo < oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case ">="
                                        If oValorCampo >= oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case "<="
                                        If oValorCampo <= oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case "="
                                        If oValorCampo = oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case "<>"
                                        If oValorCampo <> oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                End Select
                            End If
                        Case 4
                            If oValorCampo Is Nothing Then 'Condici�n vac�a
                                If oValor Is Nothing Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Else
                                If oValorCampo = oValor Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            End If

                        Case Else
                            If oValorCampo Is Nothing Then 'Condici�n vac�a
                                If oValor Is Nothing Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Else
                                Select Case UCase(oCond.Item("OPERADOR"))
                                    Case "="
                                        If UCase(oValorCampo) = UCase(oValor) Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If

                                    Case "LIKE"
                                        If Left(oValor, 1) = "%" Then
                                            If Right(oValor, 1) = "%" Then
                                                oValor = oValor.ToString.Replace("%", "")
                                                If InStr(oValorCampo.ToString, oValor.ToString) > 0 Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            Else
                                                oValor = oValor.ToString.Replace("%", "")
                                                If oValorCampo.ToString.EndsWith(oValor.ToString) Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            End If
                                        Else
                                            If Right(oValor, 1) = "%" Then
                                                oValor = oValor.ToString.Replace("%", "")
                                                If oValorCampo.ToString.StartsWith(oValor.ToString) Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            Else
                                                If UCase(oValorCampo.ToString) = UCase(oValor.ToString) Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If

                                            End If
                                        End If

                                    Case "<>"
                                        If UCase(oValorCampo.ToString) <> UCase(oValor.ToString) Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                End Select
                            End If
                    End Select

                    i += 1


                Next
                oValor = Nothing
                Try
                    iEq.Parse(sFormula, sVariables)
                    oValor = iEq.Evaluate(dValues)

                Catch ex As USPExpress.ParseException

                End Try


            Catch e As Exception
                Dim a As String = e.Message
            Finally
                oInstancia = Nothing
            End Try


            Return (oValor > 0)

        End Function

        ''' Revisado por: blp. Fecha: 10/10/2011
        ''' <summary>
        ''' Carga la denominaci�n del combo
        ''' </summary>
        ''' <param name="Lista">Origen de datos</param>
        ''' <param name="ColCod">Columna por la q buscar</param>
        ''' <param name="Valor">Valor de codigo a buscar</param>
        ''' <param name="bTextoGuion">Si la denominaci�n es den � cod - den</param>
        ''' <param name="ColPais">Columna pais por la q buscar</param>
        ''' <param name="ValorPais">Valor de pais a buscar</param>
        ''' <returns>la denominaci�n del combo</returns>
        ''' <remarks>Llamada desde: Page_load; Tiempo m�ximo:0</remarks>
        Private Function TextoDelDropDown(ByVal Lista As DataSet, ByVal ColCod As String, ByVal Valor As String, ByVal bTextoGuion As Boolean _
            , Optional ByVal ColPais As String = "", Optional ByVal ValorPais As String = Nothing, Optional ByVal bPoblacion As Boolean = False) As String
            Dim oRowLista() As DataRow
            Dim Sql As String = ColCod & "=" & StrToSQLNULL(Valor)

            If ColPais <> "" Then
                Sql = ColPais & "=" & StrToSQLNULL(ValorPais) & " AND " & Sql
            End If

            If Lista IsNot Nothing Then
                oRowLista = Lista.Tables(0).Select(Sql)
                If oRowLista.Length > 0 Then
                    If bTextoGuion Then
                        If bPoblacion Then
                            Return ReturnToSpace(oRowLista(0).Item("COD")) & " - " & ReturnToSpace(oRowLista(0).Item("DEN")) & If(oRowLista(0).IsNull("POB"), "", " (" & ReturnToSpace(oRowLista(0).Item("POB")) & ")")
                        Else
                            Return ReturnToSpace(oRowLista(0).Item("COD")) & " - " & ReturnToSpace(oRowLista(0).Item("DEN"))
                        End If
                    Else
                        Dim sDen As String
                        sDen = ReturnToSpace(oRowLista(0).Item("DEN"))

                        Return sDen
                    End If
                Else
                    Return String.Empty
                End If
            Else
                'Ejemplo: Hay departamento pero no unidad organizativa???
                Return ""
            End If
        End Function

    End Class
End Namespace