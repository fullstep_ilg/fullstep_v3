Namespace Fullstep.PMPortalWeb.Common
    Partial Class atachedfiles
        Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private mlInstancia As Long

        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script DEFER=true language=""{0}"">{1}</script>"
        Private Const IncludeScriptFormat As String = ControlChars.CrLf & _
    "<script type=""{0}"" src=""{1}""></script>"

        Property Instancia() As Long
            Get
                Return mlInstancia
            End Get
            Set(ByVal Value As Long)
                mlInstancia = Value
            End Set
        End Property
        ''' <summary>
        ''' Carga de la pantalla
        ''' </summary>
        ''' <param name="sender">pantalla</param>
        ''' <param name="e">parametro de sistema</param>            
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim bReadOnly As Boolean = (Request("readOnly") = 1)
            Dim lCiaComp As Long = Session("FS_Portal_User").CiaComp
            Dim lCiaProv As Long = Session("FS_Portal_User").IdCia

            Dim sIdi As String = Session("FS_Portal_User").Idioma.ToString
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If

            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSPMServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DetalleCampoArchivo, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            Dim idCampoAux As Integer = CInt(Request("Campo"))
            Dim idAdjuntos As String = Request("Valor").Replace("xx", ",")
            Dim idAdjuntosNew As String = Request("Valor2").Replace("xx", ",")

            Dim oAdjuntos As Fullstep.PMPortalServer.Adjuntos
            oAdjuntos = FSPMServer.Get_Adjuntos

            If Request("Instancia") <> Nothing Then
                mlInstancia = Request("Instancia")
            End If

            Dim lSolicitud As Long

            If Request("Solicitud") <> Nothing Then
                lSolicitud = Request("Solicitud")
            End If

            Dim oCampo As Fullstep.PMPortalServer.Campo

            oCampo = FSPMServer.Get_Campo()
            oCampo.Id = idCampoAux
            IDCAMPO.Value = idCampoAux
            FSENTRY.Value = Request("Input")

            If mlInstancia > 0 Then
                oCampo.LoadInst(lCiaComp, mlInstancia, sIdi, lCiaProv)
            Else
                oCampo.Load(lCiaComp, sIdi, lSolicitud)
            End If

            If mlInstancia = Nothing Then
                oAdjuntos.Load(lCiaComp, Request("tipo"), idAdjuntos, idAdjuntosNew, sIdi)
            Else
                txtInstancia.Value = mlInstancia
                oAdjuntos.LoadInst(lCiaComp, Request("tipo"), idAdjuntos, idAdjuntosNew)
                'Si es una adjunto metido desde A�adir por valor por defecto. La informaci�n
                'esta en LINEA_DESGLOSE_ADJUN
                If oAdjuntos.Data.Tables(0).Rows.Count = 0 Then
                    oAdjuntos.Load(lCiaComp, Request("tipo"), idAdjuntos, idAdjuntosNew, sIdi)
                End If
            End If

            Dim oDS As DataSet
            oDS = oAdjuntos.Data

            lblTitulo.Text = oTextos.Rows(8).Item("TEXT_" & sIdi)
            lblTipo.Text = oCampo.DenSolicitud(sIdi)
            lblSubTitulo.Text = oCampo.DenGrupo(sIdi)
            lblNomCampo.Text = oCampo.Den(sIdi)
            cmdAnyadir.Value = oTextos.Rows(10).Item(1)

            uwgFiles.DataSource = oDS
            uwgFiles.DataBind()

            uwgFiles.Bands(0).Columns.FromKey("ID").Hidden = True
            uwgFiles.Bands(0).Columns.FromKey("TIPO").Hidden = True
            uwgFiles.Bands(0).Columns.FromKey("PER").Hidden = True

            uwgFiles.Bands(0).Columns.FromKey("FECALTA").Header.Caption = oTextos.Rows(1).Item("TEXT_" & sIdi)
            uwgFiles.Bands(0).Columns.FromKey("FECALTA").Width = Unit.Percentage(10)
            uwgFiles.Bands(0).Columns.FromKey("FECALTA").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
            uwgFiles.Bands(0).Columns.FromKey("NOMBRE").Header.Caption = oTextos.Rows(9).Item("TEXT_" & sIdi)
            uwgFiles.Bands(0).Columns.FromKey("NOMBRE").Width = Unit.Percentage(18)
            uwgFiles.Bands(0).Columns.FromKey("NOMBRE").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
            uwgFiles.Bands(0).Columns.FromKey("NOM").Header.Caption = oTextos.Rows(2).Item("TEXT_" & sIdi)
            uwgFiles.Bands(0).Columns.FromKey("NOM").Width = Unit.Percentage(18)
            uwgFiles.Bands(0).Columns.FromKey("NOM").CellMultiline = Infragistics.WebUI.UltraWebGrid.CellMultiline.Yes
            uwgFiles.Bands(0).Columns.FromKey("NOM").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
            uwgFiles.Bands(0).Columns.FromKey("DATASIZE").Header.Caption = oTextos.Rows(3).Item("TEXT_" & sIdi)
            uwgFiles.Bands(0).Columns.FromKey("DATASIZE").Width = Unit.Percentage(5)
            uwgFiles.Bands(0).Columns.FromKey("DATASIZE").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
            uwgFiles.Bands(0).Columns.FromKey("COMENT").Header.Caption = oTextos.Rows(4).Item("TEXT_" & sIdi)
            uwgFiles.Bands(0).Columns.FromKey("COMENT").Width = Unit.Percentage(40)
            uwgFiles.Bands(0).Columns.FromKey("COMENT").CellMultiline = Infragistics.WebUI.UltraWebGrid.CellMultiline.Yes
            uwgFiles.Bands(0).Columns.FromKey("COMENT").AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
            uwgFiles.Bands(0).Columns.FromKey("ELIMINAR").Header.Caption = oTextos.Rows(7).Item("TEXT_" & sIdi)
            uwgFiles.Bands(0).Columns.FromKey("ELIMINAR").Width = Unit.Percentage(3)
            uwgFiles.Bands(0).Columns.FromKey("SUSTITUIR").Header.Caption = oTextos.Rows(6).Item("TEXT_" & sIdi)
            uwgFiles.Bands(0).Columns.FromKey("SUSTITUIR").Width = Unit.Percentage(3)
            uwgFiles.Bands(0).Columns.FromKey("DESCARGAR").Header.Caption = oTextos.Rows(5).Item("TEXT_" & sIdi)
            uwgFiles.Bands(0).Columns.FromKey("DESCARGAR").Width = Unit.Percentage(3)
            uwgFiles.Bands(0).Columns.FromKey("NOM").Width = Unit.Percentage(40)
            uwgFiles.Bands(0).Columns.FromKey("IDIOMA").Hidden = True
            uwgFiles.Bands(0).Columns.FromKey("ADJUN").Hidden = True
            uwgFiles.Bands(0).Columns.FromKey("CAMPO").Hidden = True
            uwgFiles.Bands(0).Columns.FromKey("RUTA").Hidden = True
            uwgFiles.Bands(0).Columns.FromKey("PORTAL").Hidden = True
            uwgFiles.Bands(0).Columns.FromKey("COMENTSalto").Hidden = True
            If bReadOnly Then
                uwgFiles.Bands(0).Columns.FromKey("ELIMINAR").ServerOnly = True
                uwgFiles.Bands(0).Columns.FromKey("SUSTITUIR").ServerOnly = True
                Me.cmdAnyadir.Visible = False
            Else
                uwgFiles.Bands(0).Columns.FromKey("ELIMINAR").Move(11)
                uwgFiles.Bands(0).Columns.FromKey("SUSTITUIR").Move(11)
            End If
            uwgFiles.Bands(0).Columns.FromKey("DESCARGAR").Move(11)
            Me.uwgFiles.DataBind()

            Dim sScript As String
            Dim sClientId As String = Replace(Me.uwgFiles.ClientID, "_", "x")
            sScript = "document.all(""" + sClientId + "_addBox"").parentElement.style.height=""0px"""
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "startItKey", sScript)
        End Sub
        ''' <summary>
        ''' Inicializar cada linea del grid. Bien con un icono, bien en una columna oculta traduce los saltos de 
        ''' linea por su codigo javascript /n
        ''' </summary>
        ''' <param name="sender">parametro de sistema</param>
        ''' <param name="e">parametro de sistema</param>     
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub uwgFiles_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgFiles.InitializeRow
            e.Row.Cells.FromKey("ELIMINAR").Style.BackgroundImage = ConfigurationManager.AppSettings("RUTANORMAL") + "script/_common/images/eliminar.gif"
            e.Row.Cells.FromKey("DESCARGAR").Style.BackgroundImage = ConfigurationManager.AppSettings("RUTANORMAL") + "script/_common/images/descargar.gif"
            e.Row.Cells.FromKey("SUSTITUIR").Style.BackgroundImage = ConfigurationManager.AppSettings("RUTANORMAL") + "script/_common/images/sustituir.gif"

            e.Row.Cells.FromKey("COMENTSalto").Value = Replace(e.Row.Cells.FromKey("COMENTSalto").Value, vbCrLf, "/n")
        End Sub
    End Class
End Namespace