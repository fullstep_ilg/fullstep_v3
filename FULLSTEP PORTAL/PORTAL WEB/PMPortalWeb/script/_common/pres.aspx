<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pres.aspx.vb" Inherits="Fullstep.PMPortalWeb.pres" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
		<title>pres</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script>
		var bRespetar = false
		var arrNodes = new Array()
		
			function ugtxtAnyo_ValueChange(oEdit, oldValue, oEvent){

				oEvent.needPostBack=true
		}


			function txtCantidad_ValueChange(oEdit, oldValue, oEvent)
				{
				var p = window.parent
				
				var oTotal  = p.igedit_getById("txtTotal")
				

				var dCantidadTotal = oTotal.getValue()
				
				var dCantidad = oEdit.getValue()
				
				var dPorcent
				
				dPorcent = dCantidad * 100 / dCantidadTotal
				bRespetar=true
				var oldValue = igedit_getById("txtPorcent").getValue()
				
				igedit_getById("txtPorcent").setValue(dPorcent)
				bRespetar=false
				txtPorcent_ValueChange(igedit_getById("txtPorcent"),oldValue,oEvent)
				
				
				}
				


			function txtPorcent_ValueChange(oEdit, oldValue, oEvent){
			if (bRespetar)
				{
				return
				}

			var oTree = igtree_getTreeById("uwtPresupuestos")
			var oNode = oTree.getSelectedNode()
			
			if (!oNode)
				return
			
			var arrPres = new Array()
			while (oNode.getParent()!=null)
				{
				arrPres[arrPres.length] = oNode.getDataKey().split("__")[2]
				oNode = oNode.getParent()
				}
			var i = arrPres.length - 1
			var sPres1 = null
			var sPres2 = null
			var sPres3 = null
			var sPres4 = null
			sPres1 = arrPres[i]
			i--
			if (i>=0)
				{
				sPres2 = arrPres[i]
				i--
				if (i>=0)
					{
					sPres3 = arrPres[i]
					i--
					if (i>=0)
						{
						sPres4 = arrPres[i]
						i--
						}
					}
				}
			
			f = document.forms("frmPresupuestos")
			sUon1 = f.item("UON1").value
			if (sUon1=="")
				sUon1=null
			sUon2 = f.item("UON2").value
			if (sUon2=="")
				sUon2=null
			sUon3 = f.item("UON3").value
			if (sUon3=="")
				sUon3=null
			iAnyo = igedit_getById("ugtxtAnyo").getValue()		

			p = window.parent
			

			var i
			
			for (i in p.arrDistribuciones)
				{
				oDistr = p.arrDistribuciones[i]
				if (oDistr.uon1 == sUon1 && oDistr.uon2 == sUon2 && oDistr.uon3 == sUon3 && oDistr.anyo == iAnyo && oDistr.pres1 == sPres1 && oDistr.pres2 == sPres2 && oDistr.pres3 == sPres3 && oDistr.pres4 == sPres4)
					break;
					
				}
			var dPorcent = oEdit.getValue() / 100

			desmarcarNodo(oDistr.nodeId)
			if (dPorcent==0)
				p.arrDistribuciones.splice(i,1)	
			else
				oDistr.porcent=dPorcent
			p.visualizarDistribucion()

			if (igedit_getById("txtCantidad"))
				{
				bRespetar=true
				igedit_getById("txtCantidad").setMaxValue ((dPorcent + (1-p.dPorcentDistribuido))*f.item("CANTIDAD").value)
				igedit_getById("txtCantidad").setValue(dPorcent * f.item("CANTIDAD").value )
				bRespetar=false
				}
		
		
		}
		</script>
		<SCRIPT type="text/javascript"><!--

		

/*		function desmarcarHijos(oNode)
			{
			var arrHijos = oNode.getChildNodes()
			if (arrHijos.length==0)
				return
			for (oHijo in arrHijos)
				{
				oDiv = document.getElementById(arrHijos[oHijo].Id)
				for (oElem in oDiv.all)
					if (oDiv.all[oElem].tagName =="IMG")
						{
						if (oDiv.all[oElem].src.search("PresupuestosAsig")>0)
							{
							for (oarrNode in arrNodes)
								if (arrNodes[oarrNode][0]==arrHijos[oHijo].getDataKey())
									{
									bOk=true
									break;
									}
							arrHijos[oHijo].setText(arrNodes[oarrNode][3])
							oDiv.all[oElem].src = "images/SINEWAVE.gif"
							break
							}
							
						}
				desmarcarHijos(arrHijos[oHijo])
				}
			}

		function buscarNodo(sPres1, sPres2,sPres3,sPres4)
			{
			
			var oTree = igtree_getTreeById("uwtPresupuestos")
					
			var arrHijos = oTree.getNodes()
			
			var i
			var ret
				
			arrHijos = arrHijos[0].getChildNodes()
			var oPres1
			var oPres2
			var oPres3
			var oPres4
			for (i in arrHijos)
				{
				if (arrHijos[i].getDataKey().split("__")[1]==sPres1)
					{
					oPres1 = arrHijos[i]
					break;
					}
				}
			if (oPres1)
				{
				arrHijos = arrHijos[i].getChildNodes()
				
				for (i in arrHijos)
					{
					
					if (arrHijos[i].getDataKey().split("__")[1]==sPres2)
						{
						oPres2 = arrHijos[i]
						break;
						}
					}
				if (oPres2)
					{
					arrHijos = arrHijos[i].getChildNodes()
					
					for (i in arrHijos)
						{
						if (arrHijos[i].getDataKey().split("__")[1]==sPres3)
							{
							oPres3 = arrHijos[i]
							break;
							}
						}
					if (oPres3)
						{
						arrHijos = arrHijos[i].getChildNodes()
						
						for (i in arrHijos)
							{
							if (arrHijos[i].getDataKey().split("__")[1]==sPres4)
								{
								oPres4 = arrHijos[i]
								break;
								}
							}
						if (oPres4)
							ret = oPres4.Id
						else
							ret =  oPres3.Id
						}
					else
						ret = oPres2.Id
					}
				else
					ret = oPres1.Id
				}
			
			return ret
			
			}
*/


		function enlazarRamaADistribucion(idpres,inivel,nodeId,indexNode)
			{
			var i
			var oNode
			p=window.parent
			for (i in p.arrDistribuciones)
				{
				oDistr = p.arrDistribuciones[i]
				if (oDistr.nivel == inivel && oDistr.idpres == idpres)
						{
						oDistr.nodeId = nodeId
						oDistr.indexNode = indexNode
						}
					
				
				}

			}

				
		function uwtPresupuestos_AfterNodeSelectionChange(treeId, nodeId){
			var oNode = igtree_getNodeById(nodeId)
			var p = window.parent
			var f = document.forms("frmPresupuestos")
			
			if (oNode.getParent()==null)
				{
				otxtPresupuesto = igedit_getById("txtPresupuesto")
				otxtPresupuesto.setValue(null)
				otxtObjetivo = igedit_getById("txtObjetivo")
				otxtObjetivo.setValue(null)
				bRespetar = true
				if (igedit_getById("txtCantidad"))
					{
					igedit_getById("txtCantidad").setMaxValue ((1-p.dPorcentDistribuido)*f.item("CANTIDAD").value)
					igedit_getById("txtCantidad").setValue(0)
					}
				igedit_getById("txtPorcent").setMaxValue ((1-p.dPorcentDistribuido)*100)
				igedit_getById("txtPorcent").setValue(0)
				bRespetar = false
				return
				}
			
			
			var arrPres = new Array()
			while (oNode.getParent()!=null)
				{
				arrPres[arrPres.length] = oNode.getDataKey().split("__")[2]
				oNode = oNode.getParent()
				}
			var iDistr
			var i = arrPres.length - 1
			var sPres1 = null
			var sPres2 = null
			var sPres3 = null
			var sPres4 = null
			sPres1 = arrPres[i]
			iNivel = 1
			i--
			if (i>=0)
				{
				sPres2 = arrPres[i]
				iNivel = 2
				i--
				if (i>=0)
					{
					sPres3 = arrPres[i]
					iNivel = 3
					i--
					if (i>=0)
						{
						sPres4 = arrPres[i]
						iNivel = 4
						i--
						}
					}
				}
			var idPresup 
			
			oNode = igtree_getNodeById(nodeId)
			if (oNode.getParent()!=null)
				idPresup = oNode.getDataKey().split("__")[1]
			
			f = document.forms("frmPresupuestos")
			sUon1 = f.item("UON1").value
			if (sUon1=="")
				sUon1=null
			sUon2 = f.item("UON2").value
			if (sUon2=="")
				sUon2=null
			sUon3 = f.item("UON3").value
			if (sUon3=="")
				sUon3=null
			iAnyo = igedit_getById("ugtxtAnyo").getValue()
			dPorcent = igedit_getById("txtPorcent").getValue() / 100
			
			var opTree=p.igtree_getTreeById("uwtPresupuestosctl0ctl0uwtreeUons")
			var opNode = opTree.getSelectedNode()
					
			var sDenUon = opNode.getText()
			var bOk = false
			var oarrNode = oNode.getDataKey().split("__")[0]

			var sDenPres=""

			
			
			var oDistrSeleccionada
				
			var sCaso = ""
			if (p.arrDistribuciones.length==0)
			
				{
				oNode.setTag(oNode.getText())
				sDenPres = oNode.getTag()

				p.arrDistribuciones[p.arrDistribuciones.length] = new p.Distribucion( sUon1,sUon2,sUon3,iAnyo,sPres1,sPres2,sPres3,sPres4,1-p.dPorcentDistribuido,iNivel,idPresup,sDenUon,sDenPres,nodeId,oarrNode)
				oDistrSeleccionada = p.arrDistribuciones[p.arrDistribuciones.length - 1]
				p.dPorcentDistribuido = 1
				}
			else				
				{
				var bExiste = false
				var iPresupuestos = 0
				for (i in p.arrDistribuciones)
					{
					oDistr = p.arrDistribuciones[i]
					if (oDistr.uon1 == sUon1 && oDistr.uon2 == sUon2 && oDistr.uon3 == sUon3 && oDistr.anyo == iAnyo)
						{
						iPresupuestos++
						if (oDistr.pres1 == sPres1 && oDistr.pres2 ==sPres2 && oDistr.pres3 == sPres3 && oDistr.pres4 == sPres4)
							{
							oDistrSeleccionada = oDistr
							bExiste = true
							break
							//hemos seleccionado un nodo que ya estaba en las distribuciones. no hacemos nada
							}
						}
					}
				if (!bExiste)
					{
					for (i in p.arrDistribuciones)
						{
						oDistr = p.arrDistribuciones[i]
						if (oDistr.uon1 == sUon1 && oDistr.uon2 == sUon2 && oDistr.uon3 == sUon3 && oDistr.anyo == iAnyo)
							{
							if (oDistr.pres1==sPres1)
								if (oDistr.pres2==sPres2)
									if (oDistr.pres3==sPres3)
										{
										if (oDistr.pres4==null || sPres4==null)
											{
											iDistr=i
											sCaso = "intercambiarDistribucion"
											}
										}
									else
										{
										if (oDistr.pres3==null || sPres3==null)
											{
											iDistr=i
											sCaso = "intercambiarDistribucion"
											}
										}
								else
									if (oDistr.pres2==null || sPres2==null)
										{
										iDistr=i
										sCaso = "intercambiarDistribucion"
										}
							}
						}
					}
			
				if (!bExiste  && iPresupuestos==1 && sCaso=="") //
					{
					iDistr =null
					for (i in p.arrDistribuciones)
						{
						oDistr = p.arrDistribuciones[i]
						if (oDistr.uon1 == sUon1 && oDistr.uon2 == sUon2 && oDistr.uon3 == sUon3 && oDistr.anyo == iAnyo)
							{
							
							iDistr = i
							break; 
							}
						}
					if (iDistr!=null)
						if (p.dPorcentDistribuido<1)
							sCaso = "anyadirDistribucion"
						else
							sCaso="intercambiarDistribucion"
					else
						sCaso = "anyadirDistribucion"
					}


				if (!bExiste  && iPresupuestos>=1 && sCaso=="" && (p.dPorcentDistribuido<1) )//
					{
					iDistr =null
					sCaso = "anyadirDistribucion"
					}
				
				if (!bExiste && iPresupuestos==0)
					{
					sCaso = "anyadirDistribucion"
					}
					
				}
		switch (sCaso)
			{
			case "eliminarDistribucion":
				p.arrDistribuciones.splice(iDistr,1)	
				break;

			case "intercambiarDistribucion":
				dp=p.arrDistribuciones[iDistr].porcent
				desmarcarNodo(p.arrDistribuciones[iDistr].nodeId)
				p.arrDistribuciones.splice(iDistr,1)	

				oNode.setTag(oNode.getText())
				sDenPres = oNode.getTag()
				p.arrDistribuciones[p.arrDistribuciones.length] = new p.Distribucion( sUon1,sUon2,sUon3,iAnyo,sPres1,sPres2,sPres3,sPres4,dp,iNivel,idPresup,sDenUon,sDenPres,nodeId,oarrNode)
				oDistrSeleccionada = p.arrDistribuciones[p.arrDistribuciones.length - 1] 
				break;
			case "anyadirDistribucion":
				if (p.dPorcentDistribuido<1)
					{
					oNode.setTag(oNode.getText())
					sDenPres = oNode.getTag()
					p.arrDistribuciones[p.arrDistribuciones.length] = new p.Distribucion( sUon1,sUon2,sUon3,iAnyo,sPres1,sPres2,sPres3,sPres4,1-p.dPorcentDistribuido,iNivel,idPresup,sDenUon,sDenPres,nodeId,oarrNode)
					oDistrSeleccionada = p.arrDistribuciones[p.arrDistribuciones.length - 1] 
					p.dPorcentDistribuido = 1
					}
				break;
			}
			
		
		
					
		p.visualizarDistribucion()
				

		if (oDistrSeleccionada)
			{
			bRespetar = true
			
			igedit_getById("txtPorcent").setMaxValue ((oDistrSeleccionada.porcent + (1-p.dPorcentDistribuido))*100)
			igedit_getById("txtPorcent").setValue(oDistrSeleccionada.porcent*100)
			if (igedit_getById("txtCantidad"))
				{
				igedit_getById("txtCantidad").setMaxValue ((oDistrSeleccionada.porcent + (1-p.dPorcentDistribuido))*f.item("CANTIDAD").value)
				igedit_getById("txtCantidad").setValue(oDistrSeleccionada.porcent * f.item("CANTIDAD").value )
				}
			
			
			bRespetar = false

			}	
		else
			{
			bRespetar = true
			
			igedit_getById("txtPorcent").setMaxValue ((1-p.dPorcentDistribuido)*100)
			igedit_getById("txtPorcent").setValue(0)
			if (igedit_getById("txtCantidad"))
				{
				igedit_getById("txtCantidad").setMaxValue ((1-p.dPorcentDistribuido)*f.item("CANTIDAD").value)
				igedit_getById("txtCantidad").setValue(0)
				}
			bRespetar = false

			}	
				
			
		}
		
		function desmarcarNodo(nodeId)
			{
			oDiv = document.getElementById(nodeId)
			for (oElem in oDiv.all)
				if (oDiv.all[oElem].tagName =="IMG")
					if (oDiv.all[oElem].src.search("PresupuestosAsig")>0)
						{
						oDiv.all[oElem].src = "images/SINEWAVE.gif"
						oNode = igtree_getNodeById(nodeId)
						oNode.setText(oNode.getTag())
						break
						}			
			}
			
		
		function marcarNodo(treeId, nodeId, porcent,oarrNode){
			

			var oNode = igtree_getNodeById(nodeId)
			if (oNode.getParent()==null)
				{
				otxtPresupuesto = igedit_getById("txtPresupuesto")
				otxtPresupuesto.setValue(null)
				otxtObjetivo = igedit_getById("txtObjetivo")
				otxtObjetivo.setValue(null)
				return
				}
/*			var bOk = false
			for (oarrNode in arrNodes)
				if (arrNodes[oarrNode][0]==oNode.getDataKey())
					{
					bOk=true
					break;
					}
			if (bOk)
				{*/
				otxtPresupuesto = igedit_getById("txtPresupuesto")
				otxtPresupuesto.setValue(arrNodes[oarrNode][1])
				otxtObjetivo = igedit_getById("txtObjetivo")
				otxtObjetivo.setValue(arrNodes[oarrNode][2])
//				}
			if (oNode.getTag()==null)
				oNode.setTag(oNode.getText())

			oDiv = document.getElementById(nodeId)
			for (oElem in oDiv.all)
				if (oDiv.all[oElem].tagName =="IMG")
					if (oDiv.all[oElem].src.search("SINEWAVE")>0)
						{
						oDiv.all[oElem].src = "images/PresupuestosAsig.gif"
						oNode = igtree_getNodeById(nodeId)
						oNode.setText(oNode.getTag() + " (" + num2str(porcent*100,".",",",2) + "%)")
						break
						}
						
					
		}
		
		function visualizarDistribucion()
		{
		var p = window.parent
if (p.sDenUon)
	document.getElementById("lblUon").innerHTML=p.sDenUon

		igedit_getById("txtPorcent").setMaxValue (0)
		igedit_getById("txtPorcent").setValue(null)
		if (igedit_getById("txtCantidad"))
			{
			igedit_getById("txtCantidad").setMaxValue (0)
			igedit_getById("txtCantidad").setValue(null)
			}

		p.visualizarDistribucion()

		var f = document.forms("frmPresupuestos")
		sUon1 = f.item("UON1").value
		if (sUon1=="")
			sUon1=null
		sUon2 = f.item("UON2").value
		if (sUon2=="")
			sUon2=null
		sUon3 = f.item("UON3").value
		if (sUon3=="")
			sUon3=null
		var iAnyo = igedit_getById("ugtxtAnyo").getValue()
		
		var bHayOtras
		
		var i
		for (i in p.arrDistribuciones)
			{
			oDistr = p.arrDistribuciones[i]
			if (oDistr.uon1!=sUon1 || oDistr.uon2!=sUon2  || oDistr.uon3!=sUon3  || oDistr.anyo!=iAnyo)
				{ 
				bHayOtras = true
				break

				}			
			
			}

		if (bHayOtras)
			document.forms("frmPresupuestos").item("cmdOtras").style.visibility="visible"
		else
			document.forms("frmPresupuestos").item("cmdOtras").style.visibility="hidden"
		


		if (p.idDistrSeleccionada)
			{
			oDistr = p.arrDistribuciones[p.idDistrSeleccionada]
			oNode = igtree_getNodeById(oDistr.nodeId)
			oNode.setSelected(true)
			bRespetar = true
			
			igedit_getById("txtPorcent").setMaxValue ((oDistr.porcent + (1-p.dPorcentDistribuido))*100)
			igedit_getById("txtPorcent").setValue(oDistr.porcent*100)
			if (igedit_getById("txtCantidad"))
				{
				igedit_getById("txtCantidad").setMaxValue ((oDistr.porcent + (1-p.dPorcentDistribuido))*f.item("CANTIDAD").value)
				igedit_getById("txtCantidad").setValue(oDistr.porcent * f.item("CANTIDAD").value )
				}
			bRespetar = false
			p.idDistrSeleccionada=null
			}
		else
			for (i in p.arrDistribuciones)
				{
				oDistr = p.arrDistribuciones[i]
				if (oDistr.uon1 == sUon1 && oDistr.uon2 == sUon2 && oDistr.uon3 == sUon3 && oDistr.anyo == iAnyo)
					{
					var oNode = igtree_getNodeById(oDistr.nodeId)

					if (oNode)
						{
						oNode.setSelected(true)
						bRespetar = true
						
						igedit_getById("txtPorcent").setMaxValue ((oDistr.porcent + (1-p.dPorcentDistribuido))*100)
						igedit_getById("txtPorcent").setValue(oDistr.porcent*100)
						if (igedit_getById("txtCantidad"))
							{
							igedit_getById("txtCantidad").setMaxValue ((oDistr.porcent + (1-p.dPorcentDistribuido))*f.item("CANTIDAD").value)
							igedit_getById("txtCantidad").setValue(oDistr.porcent * f.item("CANTIDAD").value )
							}
						bRespetar = false
						break;
						}
						
					}
				}


		}
--></SCRIPT>
		<SCRIPT type="text/javascript"><!--
	function seleccionarPresupuesto(idPres)
		{
		var p = window.parent
		var oDistr = p.arrDistribuciones[idPres]
		p.idDistrSeleccionada = idPres
		
		oUon = p.marcarUON(oDistr.uon1,oDistr.uon2,oDistr.uon3)
		oUon.setSelected(true)
		var	oDiv = document.all("divPresupuestos")
		
		oDiv.style.visibility="hidden"


		var f = document.forms("frmPresupuestos")
		f.item("UON1").value = (oDistr.uon1==null?"":oDistr.uon1)
		f.item("UON2").value = (oDistr.uon2==null?"":oDistr.uon2)
		f.item("UON3").value = (oDistr.uon3==null?"":oDistr.uon3)

		igedit_getById("ugtxtAnyo").setValue(oDistr.anyo)
		f.item("DENUON").value = oDistr.denuon
		
		f.submit()

		}
	function mostrarPopupPresupuestos(oBot)
		{

		oDiv = document.all("divPresupuestos")
		if (oDiv.style.visibility=="visible")
			{
			oDiv.style.visibility="hidden"
			return
			}
		var i
		var p = window.parent

		var x = 0
		var y = 0
		var elem = oBot

		while(elem != null)
			{
			if(elem.offsetLeft != null) x += elem.offsetLeft;
			if(elem.offsetTop != null) y += elem.offsetTop;
			elem = elem.offsetParent;
			}
		
		var f = document.forms("frmPresupuestos")
		var sUon1 = f.item("UON1").value
		if (sUon1=="")
			sUon1=null
		var sUon2 = f.item("UON2").value
		if (sUon2=="")
			sUon2=null
		var sUon3 = f.item("UON3").value
		if (sUon3=="")
			sUon3=null
		var iAnyo = igedit_getById("ugtxtAnyo").getValue()
		
	
		var sHtml = ""
		sHtml += "<table class=MenuPopUP border=0>"
		for (i in p.arrDistribuciones)
			{
			oDistr = p.arrDistribuciones[i]
			if (oDistr.uon1!=sUon1 || oDistr.uon2!=sUon2  || oDistr.uon3!=sUon3  || oDistr.anyo!=iAnyo)
				{ 
				sMenu = oDistr.denuon + " - " + (oDistr.anyo>0?oDistr.anyo + " - ":"") + oDistr.denpres + "(" + num2str(oDistr.porcent * 100,".",",",2) + "%)"
				sHtml += "<tr><td ><a href='javascript:seleccionarPresupuesto(" + i + ")' onmouseover=\"this.parentElement.style.backgroundColor='darkblue';this.style.color='white'\" onmouseout=\"this.parentElement.style.backgroundColor='silver';this.style.color='black'\">" + sMenu + "</a></td></tr>" 
				}
			
			}		
		sHtml +="</table></div>"
		oDiv = document.all("divPresupuestos")
		oDiv.innerHTML = sHtml
		var w = oDiv.offsetWidth
		oDiv.style.width = w

		x = x - w + 15
		y = y + 15
		
		oDiv.style.top= y
		oDiv.style.left = x
		
		oDiv.style.visibility="visible"
		
		}
--></SCRIPT>
	</head>
	<body class="uwtDefaultTabGeneral" onload="visualizarDistribucion()" MS_POSITIONING="GridLayout">
		<form id="frmPresupuestos" method="post" runat="server">
			<table width="100%">
				<TR>
					<TD class="fondolinea" vAlign="bottom" colSpan="2" height="3"></TD>
				</TR>
			</table>
			<table class="EstadosFondo1" width="100%">
				<tr>
					<td vAlign="middle" width="90%"><asp:label id="lblUon" runat="server" cssClass="captionBlue">Label</asp:label></td>
					<td width="10%"><igtxt:webnumericedit id="ugtxtAnyo" runat="server" AutoPostBack="True">
							<CLIENTSIDEEVENTS ValueChange="ugtxtAnyo_ValueChange" Spin="ugtxtAnyo_Spin"></CLIENTSIDEEVENTS>
							<SPINBUTTONS Display="OnRight"></SPINBUTTONS>
						</igtxt:webnumericedit></td>
					<td><input id="cmdOtras" name="cmdOtras" runat="server" class="boton" style="WIDTH:15px" onclick="mostrarPopupPresupuestos(this)"
							type="button" value="...">
					</td>
				</tr>
			</table>
			<table width="100%">
				<TR>
					<TD class="fondolinea" vAlign="bottom" colSpan="2" height="3"></TD>
				</TR>
			</table>
			<table width="100%">
				<tr>
					<td><ignav:ultrawebtree id="uwtPresupuestos" runat="server" CssClass="igTreeEnTab" Height="275px" Width="100%">
							<SelectedNodeStyle ForeColor="White" BackColor="Navy"></SelectedNodeStyle>
							<NodeStyle Height="15px"></NodeStyle>
							<Levels>
								<ignav:Level Index="0"></ignav:Level>
								<ignav:Level Index="1"></ignav:Level>
							</Levels>
							<ClientSideEvents NodeClick="uwtPresupuestos_AfterNodeSelectionChange"></ClientSideEvents>
						</ignav:ultrawebtree></td>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<td style="WIDTH: 80px; HEIGHT: 23px"><asp:label id="lblPresupuesto" runat="server" CssClass="captionBlue">Presupuesto:</asp:label></td>
					<td style="WIDTH: 60px; HEIGHT: 23px"><igtxt:webnumericedit id="txtPresupuesto" runat="server" ReadOnly="True" CssClass="trasparent"></igtxt:webnumericedit></td>
					<td style="WIDTH: 200px; HEIGHT: 23px">&nbsp;
					</td>
					<td style="WIDTH: 80px; HEIGHT: 23px"><asp:label id="lblObjetivo" runat="server" CssClass="captionBlue">Objetivo:</asp:label></td>
					<td style="WIDTH: 60px; HEIGHT: 23px"><igtxt:webnumericedit id="txtObjetivo" runat="server" ReadOnly="True" CssClass="trasparent"></igtxt:webnumericedit></td>
					<td width="200"><asp:label id="lblpercent2" runat="server" CssClass="captionBlue">%</asp:label></td>
				</tr>
				<tr>
					<td><asp:label id="lblAsignar" runat="server" CssClass="captionBlue">Asignar:</asp:label></td>
					<td><igtxt:webnumericedit id="txtPorcent" runat="server" MinValue="0" MaxValue="100">
							<CLIENTSIDEEVENTS ValueChange="txtPorcent_ValueChange" Spin="txtPorcent_Spin"></CLIENTSIDEEVENTS>
							<SpinButtons Display="OnRight" Delta="10"></SpinButtons>
						</igtxt:webnumericedit></td>
					<td width="2" colSpan="2"><asp:label id="lblpercent1" runat="server" CssClass="captionBlue">%</asp:label></td>
					<td><igtxt:webnumericedit id="txtCantidad" runat="server" MaxValue="100" MinValue="0">
							<CLIENTSIDEEVENTS ValueChange="txtCantidad_ValueChange"></CLIENTSIDEEVENTS>
						</igtxt:webnumericedit></td>
				</tr>
			</table>
			<INPUT id="UON1" style="Z-INDEX: 100; LEFT: 672px; POSITION: absolute; TOP: 264px" type="hidden"
				name="UON1" runat="server"> <INPUT id="UON2" style="Z-INDEX: 101; LEFT: 696px; POSITION: absolute; TOP: 288px" type="hidden"
				name="UON2" runat="server"> <INPUT id="UON3" style="Z-INDEX: 103; LEFT: 720px; POSITION: absolute; TOP: 312px" type="hidden"
				name="UON3" runat="server"> <INPUT id="TIPO" style="Z-INDEX: 104; LEFT: 720px; POSITION: absolute; TOP: 312px" type="hidden"
				name="TIPO" runat="server"> <INPUT id="DENUON" style="Z-INDEX: 102; LEFT: 720px; POSITION: absolute; TOP: 312px" type="hidden"
				name="DENUON" runat="server"><INPUT id="CANTIDAD" style="Z-INDEX: 102; LEFT: 720px; POSITION: absolute; TOP: 312px"
				type="hidden" name="CANTIDAD" runat="server"><INPUT id="PRES" style="Z-INDEX: 102; LEFT: 720px; POSITION: absolute; TOP: 312px" type="hidden"
				name="PRES" runat="server">
			<div id="divPresupuestos" style='LEFT:100px;VISIBILITY:hidden;POSITION:absolute;TOP:20px'></div>
		</form>
	</body>
</html>
