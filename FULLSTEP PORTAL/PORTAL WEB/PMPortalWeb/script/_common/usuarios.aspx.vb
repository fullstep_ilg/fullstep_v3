Namespace Fullstep.PMPortalWeb
    Partial Class usuarios
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Protected WithEvents ugtxtUO_wdg As Infragistics.Web.UI.GridControls.WebDataGrid

        Private m_sCualquiera As String
        Private m_sUsuario As String
        Private m_lRol As Integer
        Private m_lInstancia As Integer
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim oUnidadesUO As Fullstep.PMPortalServer.UnidadesOrg
            Dim lCiaComp As Long = IdCiaComp

            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Usuarios
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaTheme", "<script>var rutaTheme = '" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "'</script>")
            'Textos de idiomas:
            Me.lblTitulo.Text = Textos(0)
            Me.lblUO.Text = Textos(1)
            Me.lblNombre.Text = Textos(2)
            Me.lblApe.Text = Textos(3)
            Me.lblDep.Text = Textos(4)
            Me.hypBuscar.Text = Textos(5)
            Me.lblResult.Text = Textos(6)
            Me.lblPulsar.Text = Textos(7)
            m_sCualquiera = Textos(8)
            m_sUsuario = Textos(9)
            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Paginador
            CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(0)
            CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(1)
            'Vuelvo a cargar el modulo de Usuarios
            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Usuarios

            If Request("idFilaGrid") <> Nothing Then
                FilaGrid.Value = Request("idFilaGrid")
                Me.lblTitulo.Text = Textos(11)
            Else
                FilaGrid.Value = ""
            End If
            If Request("Rol") <> Nothing Then
                m_lRol = CInt(Request("Rol"))
            Else
                m_lRol = Nothing
            End If
            If Request("Instancia") <> Nothing Then
                m_lInstancia = CInt(Request("Instancia"))
            Else
                m_lInstancia = Nothing
            End If

            If Request("IDControl") <> Nothing Then
                Me.lblTitulo.Text = Textos(11)
                Me.IDCONTROL.Value = Request("IdControl")
            End If

            If Request("idFilaGrid") Is Nothing AndAlso Request("IDControl") Is Nothing Then
                'guardarinstancia.aspx -> window.open("../_common/usuarios.aspx?idFilaGrid=" + cellId,
                'jsAlta.js -> window.open("../_common/usuarios.aspx?Valor=" + oEntry.getDataValue() + "&IDControl=" + oEntry.id + "&Campo=" + oEntry.idCampo,
                'RealizarAccion.aspx -> window.open ("../_common/usuarios.aspx?idFilaGrid=" + cellId,
                'Eoc -> No hay donde devolver datos luego se corta.
                Response.End()
            End If

            If Page.IsPostBack Then
                CargarGrid()
            End If
            'Carga los datos seleccionados:        
            If Not Page.IsPostBack Then
                With CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), System.Web.UI.WebControls.Image)
                    .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero_desactivado.gif"
                    .Attributes.Add("onClick", "return FirstPage();")
                    .Enabled = False
                End With
                With CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), System.Web.UI.WebControls.Image)
                    .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior_desactivado.gif"
                    .Attributes.Add("onClick", "return PrevPage();")
                    .Enabled = False
                End With
                Dim SoloUnaPagina As Boolean = True
                With CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), System.Web.UI.WebControls.Image)
                    .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
                    .Attributes.Add("onClick", "return NextPage();")
                    .Enabled = Not SoloUnaPagina
                End With
                With CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), System.Web.UI.WebControls.Image)
                    .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
                    .Attributes.Add("onClick", "return LastPage();")
                    .Enabled = Not SoloUnaPagina
                End With

                'Carga el combo de las unidades organizativas:
                oUnidadesUO = FSPMServer.get_UnidadesOrg
                oUnidadesUO.CargarUnidadesOrganizativas(lCiaComp, Idioma, "")

                Dim ugtxtUO_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = ugtxtUO_.Controls(0).Controls(0).FindControl("ugtxtUO_wdg")
                ugtxtUO_wdg.DataSource = oUnidadesUO.Data
                ugtxtUO_wdg.DataBind()
                ugtxtUO_wdg.Columns("COD_UON1").Hidden = True
                ugtxtUO_wdg.Columns("COD_UON2").Hidden = True
                ugtxtUO_wdg.Columns("COD_UON3").Hidden = True
                ugtxtUO_.TextField = "DEN"
                ugtxtUO_wdg.Rows(0).Items(3).Text = m_sCualquiera

                oUnidadesUO = Nothing

                ugtxtApe.Text = Request("APE")
                ugtxtNombre.Text = Request("NOMBRE")
                Dim SelectedRowIndex As Integer = IIf(Request("UO") = String.Empty, 0, CInt(Request("UO")))
                ugtxtUO_wdg.Behaviors.Selection.SelectedRows.Add(ugtxtUO_wdg.Rows(SelectedRowIndex))
                ugtxtUO_.Items(0).Text = DBNullToStr(ugtxtUO_wdg.Rows(SelectedRowIndex).Items(3).Value)
                ugtxtUO_.CurrentValue = DBNullToStr(ugtxtUO_wdg.Rows(SelectedRowIndex).Items(3).Text)

                'Carga la combo de departamentos
                If Request("UO") <> "" And Request("UO") <> Nothing Then
                    If ugtxtUO_wdg.Behaviors.Selection.SelectedRows.Count > 0 Then
                        With ugtxtUO_wdg.Behaviors.Selection.SelectedRows(0)
                            Dim sUON1 As String = IIf(.Items(0).Value Is System.DBNull.Value, Nothing, .Items(0).Value)
                            Dim sUON2 As String = IIf(.Items(1).Value Is System.DBNull.Value, Nothing, .Items(1).Value)
                            Dim sUON3 As String = IIf(.Items(2).Value Is System.DBNull.Value, Nothing, .Items(2).Value)

                            CargarDepartamentos(sUON1, sUON2, sUON3)
                        End With
                        If Request("DEP") <> Nothing And Request("DEP") <> "" Then
                            ugtxtDEP.SelectedItemIndex = Request("DEP")
                            If Not ugtxtDEP.SelectedItem Is Nothing Then ugtxtDEP.CurrentValue = ugtxtDEP.SelectedItem.Text
                        End If
                        If ugtxtDEP.SelectedItem IsNot Nothing Then
                            ugtxtDEP.SelectedItem.CssClass = "igdd_FullstepListItemB igdd_FullstepListItemSelected"
                        End If
                    End If
                End If
                'LLama a la b�squeda:
                hypBuscar_Click(sender, e)
            End If

        End Sub

        ''' <summary>
        ''' Carga el combo de Departamentos en funci�n de la Unidad organizativa
        ''' </summary>
        ''' <param name="UON1">Uon 1 del combo de Unidad organizativa</param>
        ''' <param name="UON2">Uon 2 del combo de Unidad organizativa</param>
        ''' <param name="UON3">Uon 3 del combo de Unidad organizativa</param>
        ''' <returns>Nada</returns>
        ''' <remarks>Llamada desde:ugtxtUO_SelectedRowChanged         Page_Load; Tiempo m�ximo:0,1</remarks>
        Private Function CargarDepartamentos(Optional ByVal UON1 As String = Nothing, Optional ByVal UON2 As String = Nothing, Optional ByVal UON3 As String = Nothing)
            Dim oDepartamentos As Fullstep.PMPortalServer.Departamentos
            Dim iNivel As Integer
            Dim lCiaComp As Long = IdCiaComp

            'Carga el combo del origen (los departamentos):
            oDepartamentos = FSPMServer.Get_Departamentos
            If Not UON3 = Nothing Then
                iNivel = 3
            ElseIf Not UON2 = Nothing Then
                iNivel = 2
            ElseIf Not UON1 = Nothing Then
                iNivel = 1
            Else
                iNivel = 0
            End If
            oDepartamentos.LoadData(lCiaComp, iNivel, UON1, UON2, UON3)

            ugtxtDEP.TextField = "DEN"
            ugtxtDEP.ValueField = "COD"
            ugtxtDEP.DataSource = oDepartamentos.Data
            ugtxtDEP.DataBind()

            ugtxtDEP.Items(0).Text = m_sCualquiera

            If ugtxtDEP.Items.Count = 1 Then
                ugtxtDEP.SelectedItemIndex = 0
            ElseIf ugtxtDEP.Items.Count = 2 AndAlso ugtxtDEP.Items(0).Visible = False Then
                ugtxtDEP.SelectedItemIndex = 1
                ugtxtDEP.CurrentValue = ugtxtDEP.SelectedItem.Text
            End If

            oDepartamentos = Nothing

        End Function

        Private Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
            hypBuscar_Click(sender, e)
        End Sub
        ''' <summary>
        ''' Responde al evento click del hyperlink haciendo una busqueda de usuarios para los filtros de pantalla. 
        ''' </summary>
        ''' <param name="sender">hyperlink hypBuscar</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,3</remarks>
        Private Sub hypBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hypBuscar.Click
            Dim sUON1 As String = Nothing
            Dim sUON2 As String = Nothing
            Dim sUON3 As String = Nothing
            Dim sDEP As String = Nothing
            Dim lCiaComp As Long = IdCiaComp

            Dim ugtxtUO_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = ugtxtUO_.Controls(0).Controls(0).FindControl("ugtxtUO_wdg")
            If ugtxtUO_wdg.Behaviors.Selection.SelectedRows.Count > 0 Then
                With ugtxtUO_wdg.Behaviors.Selection.SelectedRows(0)
                    sUON1 = IIf(.Items(0).Value Is System.DBNull.Value, Nothing, .Items(0).Value)
                    sUON2 = IIf(.Items(1).Value Is System.DBNull.Value, Nothing, .Items(1).Value)
                    sUON3 = IIf(.Items(2).Value Is System.DBNull.Value, Nothing, .Items(2).Value)
                End With
            End If

            If Not ugtxtDEP.SelectedItem Is Nothing Then
                If ugtxtDEP.SelectedItem.Text <> m_sCualquiera Then
                    sDEP = ugtxtDEP.SelectedItem.Value
                End If
            End If

            If ugtxtDEP.Items.Count = 0 Then
                sUON1 = ""
                sUON2 = ""
                sUON3 = ""
                sDEP = ""
            End If

            whdgUsuarios.Rows.Clear()
            whdgUsuarios.Columns.Clear()
            whdgUsuarios.GridView.Columns.Clear()

            If m_lRol <> Nothing Then
                Dim oPersonas As Fullstep.PMPortalServer.Participantes
                oPersonas = FSPMServer.Get_Participantes
                opersonas.Instancia = m_lInstancia
                opersonas.Rol = m_lRol

                oPersonas.LoadDataPersonas(lCiaComp, IIf(ugtxtNombre.Value = "", Nothing, ugtxtNombre.Value), IIf(ugtxtApe.Value = "", Nothing, ugtxtApe.Value), sUON1, sUON2, sUON3, sDEP)
                whdgUsuarios.DataSource = oPersonas.Data
                whdgUsuarios.DataMember = "Table"
                whdgUsuarios.DataKeyFields = "COD"
                CrearColumnas()
                whdgUsuarios.DataBind()
                ConfigurarGrid()
                Me.InsertarEnCache("oUsus_" & FSPMUser.Cod, oPersonas.Data)
            Else
                Dim oPersonas As Fullstep.PMPortalServer.Personas
                oPersonas = FSPMServer.Get_Personas
                oPersonas.LoadData(lciacomp, , IIf(ugtxtNombre.Value = "", Nothing, ugtxtNombre.Value), IIf(ugtxtApe.Value = "", Nothing, ugtxtApe.Value), sUON1, sUON2, sUON3, sDEP)
                whdgUsuarios.DataSource = oPersonas.Data
                whdgUsuarios.DataMember = "Table"
                whdgUsuarios.DataKeyFields = "COD"
                CrearColumnas()
                whdgUsuarios.DataBind()
                ConfigurarGrid()
                Me.InsertarEnCache("oUsus_" & FSPMUser.Cod, oPersonas.Data)
            End If
            UP_whdgUsuarios.Update()
        End Sub
        Private Sub CargarGrid()
            whdgUsuarios.Rows.Clear()
            whdgUsuarios.Columns.Clear()
            whdgUsuarios.GridView.Columns.Clear()

            whdgUsuarios.DataSource = CType(Cache("oUsus_" & FSPMUser.Cod), DataSet)
            whdgUsuarios.DataMember = "Table"
            whdgUsuarios.DataKeyFields = "COD"
            CrearColumnas()
            whdgUsuarios.DataBind()
            ConfigurarGrid()
        End Sub

        Private Sub CrearColumnas()
            Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
            Dim nombre As String

            For i As Integer = 0 To whdgUsuarios.DataSource.Tables(0).Columns.Count - 1
                campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
                nombre = whdgUsuarios.DataSource.Tables(0).Columns(i).ToString
                With campoGrid
                    .Key = nombre
                    .DataFieldName = nombre
                    .Header.Text = nombre
                End With
                whdgUsuarios.Columns.Add(campoGrid)
                whdgUsuarios.GridView.Columns.Add(campoGrid)
            Next
            AnyadirColumnaU("USUARIO")
        End Sub

        Private Sub AnyadirColumnaU(ByVal fieldname As String)
            Dim unboundField As New Infragistics.Web.UI.GridControls.UnboundField(True)

            unboundField.Key = fieldname
            whdgUsuarios.Columns.Add(unboundField)
            whdgUsuarios.GridView.Columns.Add(unboundField)
        End Sub

        Private Sub ConfigurarGrid()
            If m_lRol = Nothing Then
                whdgUsuarios.Columns("COD").Hidden = True
                whdgUsuarios.Columns("NOM").Hidden = True
                whdgUsuarios.Columns("APE").Hidden = True
                whdgUsuarios.Columns("UON1").Hidden = True
                whdgUsuarios.Columns("UON2").Hidden = True
                whdgUsuarios.Columns("UON3").Hidden = True
                whdgUsuarios.Columns("DEP").Hidden = True
                whdgUsuarios.Columns("NOM_UO").Hidden = True
                whdgUsuarios.Columns("SOL_COMPRA").Hidden = True
                whdgUsuarios.Columns("CAR").Hidden = True
                whdgUsuarios.Columns("TFNO").Hidden = True
                whdgUsuarios.Columns("FAX").Hidden = True
                whdgUsuarios.Columns("EMAIL").Hidden = True
                whdgUsuarios.Columns("USUCOD").Hidden = True
                whdgUsuarios.Columns("NOMBRE").Hidden = True
                whdgUsuarios.Columns("PYME").Hidden = True

                whdgUsuarios.Columns("NOM_DEP").Width = Unit.Percentage(30)
                whdgUsuarios.Columns("USUARIO").Width = Unit.Percentage(40)

                whdgUsuarios.Columns("NOM_DEP").Header.Text = lblDep.Text
                whdgUsuarios.Columns("USUARIO").Header.Text = m_sUsuario
            Else
                whdgUsuarios.Columns("PARTCOD").Hidden = True
                whdgUsuarios.Columns("NOM_DEP").Width = Unit.Percentage(30)
                whdgUsuarios.Columns("PARTNOM").Width = Unit.Percentage(40)
                whdgUsuarios.Columns("NOM_DEP").Header.Text = lblDep.Text
                whdgUsuarios.Columns("PARTNOM").Header.Text = m_sUsuario
            End If
        End Sub

        Private Sub whdgUsuarios_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgUsuarios.InitializeRow
            If m_lRol = Nothing Then
                e.Row.Items.FindItemByKey("USUARIO").Value = e.Row.Items.FindItemByKey("NOM").Value & " " & e.Row.Items.FindItemByKey("APE").Value
            End If
        End Sub

        ''' <summary>
        ''' Tras la carga del grid se "sincroniza" con el Custom Pager
        ''' </summary>
        ''' <param name="sender">control</param>
        ''' <param name="e">evento de sistema</param>  
        ''' <remarks>Llamada desde: sistema; Tiempo máximo:0 </remarks>
        Private Sub whdgUsuarios_DataBound(sender As Object, e As System.EventArgs) Handles whdgUsuarios.DataBound
            Dim pagerList As DropDownList = DirectCast(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
            pagerList.Items.Clear()
            For i As Integer = 1 To whdgUsuarios.GridView.Behaviors.Paging.PageCount
                pagerList.Items.Add(i.ToString())
            Next
            CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = whdgUsuarios.GridView.Behaviors.Paging.PageCount
            Dim SoloUnaPagina As Boolean = (whdgUsuarios.GridView.Behaviors.Paging.PageCount = 1)
            With CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), System.Web.UI.WebControls.Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
                .Enabled = Not SoloUnaPagina
            End With
            With CType(whdgUsuarios.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), System.Web.UI.WebControls.Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
                .Enabled = Not SoloUnaPagina
            End With
        End Sub
        ''' <summary>
        ''' M�todo que se lanza con la llamada a la funci�n loadItems desde javascript. 
        ''' </summary>
        ''' <param name="sender">Control que lanza el evento con la llamada a su m�todo loadItems en javascript</param>
        ''' <param name="e">par�metros del evento</param>
        ''' <remarks>Llamada desde usuario.aspx. Tiempo m�ximo inferior a 0,2 seg</remarks>
        Private Sub ugtxtDEP_ItemsRequested(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownItemsRequestedEventArgs) Handles ugtxtDEP.ItemsRequested
            Dim aUONS As String() = Split(e.Value, "@@")
            Dim sUON1 As String = IIf(aUONS(0) <> String.Empty, aUONS(0), Nothing)
            Dim sUON2 As String = IIf(aUONS(1) <> String.Empty, aUONS(1), Nothing)
            Dim sUON3 As String = IIf(aUONS(2) <> String.Empty, aUONS(2), Nothing)

            CargarDepartamentos(sUON1, sUON2, sUON3)
        End Sub

        ''' <summary>
        ''' M�todo que se lanza con el evento RowSelectionChanged del control
        ''' </summary>
        ''' <param name="sender">Control que lanza el evento</param>
        ''' <param name="e">par�metros del evento</param>
        ''' <remarks>Llamada desde usuario.aspx. Tiempo m�ximo inferior a 0,2 seg</remarks>
        Public Sub ugtxtUO_wdg_RowSelectionChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.SelectedRowEventArgs) Handles ugtxtUO_wdg.RowSelectionChanged
            Dim oSelectedRow As Infragistics.Web.UI.GridControls.GridRecord = CType(sender, Infragistics.Web.UI.GridControls.WebDataGrid).Behaviors.Selection.SelectedRows(0)
            Dim sUON1 As String = IIf(null2str(oSelectedRow.Items(0).Value) <> String.Empty, oSelectedRow.Items(0).Value, Nothing)
            Dim sUON2 As String = IIf(null2str(oSelectedRow.Items(1).Value) <> String.Empty, oSelectedRow.Items(1).Value, Nothing)
            Dim sUON3 As String = IIf(null2str(oSelectedRow.Items(2).Value) <> String.Empty, oSelectedRow.Items(2).Value, Nothing)

            CargarDepartamentos(sUON1, sUON2, sUON3)
            ugtxtDEP.Items(0).Text = m_sCualquiera
            whdgUsuarios.Rows.Clear()
            Me.UP_Uon.Update()
            Me.UP_whdgUsuarios.Update()
        End Sub

    End Class
End Namespace