﻿Namespace Fullstep.PMPortalWeb

    Partial Class Editor
        Inherits FSPMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

            'Le añadimos el CKFinder
            If Request("readOnly") <> "1" Then
                Dim FileBrowser1 As New CKFinder.FileBrowser
                FileBrowser1.BasePath = "/ckfinder/"
                FileBrowser1.SetupCKEditor(CKEditor1)
            End If

            RegistrarScripts()
        End Sub

        ''' <summary>
        ''' Registra en el aspx las funciones javascript para la configuración del editor, la carga del texto y su guardado
        ''' </summary>
        ''' <remarks>Llamada desde: Page_Load(); Tiempo máximo: 0,1 sg.</remarks>
        Sub RegistrarScripts()
            Dim sScript As String

            If Request("readOnly") <> "1" Then
                ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Editor
                sScript = "function handleCKEditorPost() {" & vbCrLf
                sScript = sScript & "p = window.opener" & vbCrLf
                sScript = sScript & "p.save_editor_text('" & Request("ID") & "', escape(CKEDITOR.instances.CKEditor1.getData()))" & vbCrLf
                sScript = sScript & "alert('" & Textos(0) & "')" & vbCrLf & "}" & vbCrLf
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "handleCKEditorPost", sScript, True)
            End If

            If Request("readOnly") <> "1" Then
                Dim sIdioma As String = "en"
                If Idioma = "SPA" Then
                    sIdioma = "es"
                ElseIf Idioma = "GER" Then
                    sIdioma = "de"
                ElseIf Idioma = "FRA" Then
                    sIdioma = "fr"
                End If
                sScript = "function ConfigurarIdioma() {" & vbCrLf & _
                    "CKEDITOR.config.defaultLanguage ='" & sIdioma & "';" & vbCrLf & _
                    "CKEDITOR.config.language ='" & sIdioma & "';" & vbCrLf & _
                    "CKEDITOR.config.scayt_sLang ='" & sIdioma & "';" & vbCrLf & _
                    "}" & vbCrLf
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ConfigurarIdioma", sScript, True)
            Else
                sScript = "function ConfigurarIdioma() {" & vbCrLf & _
                    "}" & vbCrLf
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ConfigurarIdioma", sScript, True)
            End If

            sScript = "function CargarTexto() {" & vbCrLf & _
                    "var valor;" & vbCrLf & _
                    "e = window.opener.document.getElementById('" & Request("ID") & "__tbl');" & vbCrLf & _
                    "if (e) {" & vbCrLf & _
                        "o = e.Object;" & vbCrLf & _
                        "valor = unescape(o.getDataValue());" & vbCrLf & _
                    "} else {" & vbCrLf & _
                        "e = window.opener.document.getElementById('" & Request("ID") & "');" & vbCrLf & _
                        "if (e) " & vbCrLf & _
                            "valor = unescape(e.value);" & vbCrLf & _
                    "}" & vbCrLf & vbCrLf & _
                    "if (e) {" & vbCrLf
            If Request("readOnly") <> "1" Then
                sScript = sScript & "CKEditor1 = document.getElementById('" & CKEditor1.ClientID & "');" & vbCrLf & _
                    "CKEditor1.value = valor;" & vbCrLf & _
                    "if (CKEditor1.value == null)" & vbCrLf & _
                        "CKEditor1.value = '';" & vbCrLf
            Else
                sScript = sScript & "document.getElementById('" & texto.ClientID & "').innerHTML = valor;" & vbCrLf
            End If
            sScript = sScript & "}" & vbCrLf
            sScript = sScript & "}" & vbCrLf
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "CargarTexto", sScript, True)

        End Sub
    End Class
End Namespace