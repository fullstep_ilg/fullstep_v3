<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UnidadesOrganizativas.aspx.vb" Inherits="Fullstep.PMPortalWeb.UnidadesOrganizativas" %>
<%@ Register TagPrefix="ignav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript">
/*''' <summary>
''' Seleccionar Unidad en la pantalla q llamo
''' </summary>
''' <remarks>Llamada desde:cmdAceptar; Tiempo m�ximo:0</remarks>*/		
function seleccionarUnidadOrg() {

	var oTree = igtree_getTreeById("uwtUnidadesOrganizativas")
	
	var oNode = oTree.getSelectedNode()
	if (oNode==null)
		{
		alert(arrTextosML[0])
		return false
		}
	var sDen= oNode.getText();	
	
	var sUON = new Array()
	
	var sUON0;
	var sUON1;
	var sUON2;
	var sUON3;
	
	
	
	var iNivel=-1
	while (oNode)
		{
		iNivel ++;
		sUON[iNivel]=oNode.getDataKey();
		oNode = oNode.getParent();
	
		}
	var j=0;
	for (i=iNivel;i>0;i--)	{
		eval("sUON" + i.toString() + "=sUON[" + j.toString() + "]");
		j++;
}
    var oidControl=document.getElementById("IDCONTROL")
    window.opener.UON_seleccionado(oidControl.value, sUON1, sUON2, sUON3, iNivel, sDen)
	window.close()
	
}
		</script>
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<ignav:ultrawebtree id="uwtUnidadesOrganizativas" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 16px"
				runat="server" Height="450px" Width="450px" CssClass="igTree">
				<SelectedNodeStyle ForeColor="White" BackColor="Navy"></SelectedNodeStyle>
				<Levels>
					<ignav:Level Index="0"></ignav:Level>
					<ignav:Level Index="1"></ignav:Level>
				</Levels>
			</ignav:ultrawebtree>
			<TABLE id="Table1" style="Z-INDEX: 109; LEFT: 8px; POSITION: absolute; TOP: 490px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD align="center"><INPUT class="boton" id="cmdAceptar" onclick="seleccionarUnidadOrg()" type="button" value="DSeleccionar"
							name="cmdAceptar" runat="server"></TD>
					<TD align="center"><INPUT class="boton" id="cmdCancelar" onclick="window.close()" type="button" value="DCancelar"
							name="cmdCancelar" runat="server"></TD>
				</TR>
			</TABLE>
			<INPUT runat="server" id="IDCAMPO" type="hidden" name="IDCAMPO"> <INPUT runat="server" id="IDCONTROL" type="hidden" name="IDCAMPO">
		</form>
	</body>
</html>
