Namespace Fullstep.PMPortalWeb

    Partial Class UnidadesOrganizativas
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        Private Const IncludeScriptFormat As String = ControlChars.CrLf & _
         "<script type=""{0}"" src=""{1}""></script>"
        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script language=""{0}"">{1}</script>"
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")

            Dim sValor As String = Request("Valor")
            Dim lCiaComp As Long = IdCiaComp
            Dim sIdi As String = oUser.Idioma

            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If

            'Textos de la p�gina:
            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSWSServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.UnidadesOrganizativas, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            cmdAceptar.Value = oTextos.Rows(1).Item(1)
            cmdCancelar.Value = oTextos.Rows(2).Item(1)


            IDCONTROL.Value = Request("IdControl")

            Dim oUnidadesOrg As Fullstep.PMPortalServer.UnidadesOrg
            oUnidadesOrg = FSWSServer.get_UnidadesOrg
            oUnidadesOrg.CargarTodasUnidadesOrganizativas(lCiaComp, sIdi)

            Me.uwtUnidadesOrganizativas.DataSource = oUnidadesOrg.Data.Tables(0).DefaultView
            Me.uwtUnidadesOrganizativas.Levels(0).RelationName = "REL_NIV0_NIV1"
            Me.uwtUnidadesOrganizativas.Levels(0).ColumnName = "DEN"
            Me.uwtUnidadesOrganizativas.Levels(0).LevelImage = "images/world.gif"

            Me.uwtUnidadesOrganizativas.Levels(1).LevelImage = "images/carpetaAzulOscuro.gif"
            Me.uwtUnidadesOrganizativas.Levels(1).RelationName = "REL_NIV1_NIV2"
            Me.uwtUnidadesOrganizativas.Levels(1).ColumnName = "DEN"
            Me.uwtUnidadesOrganizativas.Levels(1).LevelKeyField = "COD"

            Me.uwtUnidadesOrganizativas.Levels(2).LevelImage = "images/carpetaCommodityTxiki.gif"
            Me.uwtUnidadesOrganizativas.Levels(2).RelationName = "REL_NIV2_NIV3"
            Me.uwtUnidadesOrganizativas.Levels(2).ColumnName = "DEN"
            Me.uwtUnidadesOrganizativas.Levels(2).LevelKeyField = "COD"

            Me.uwtUnidadesOrganizativas.Levels(3).LevelImage = "images/carpetagris.gif"
            Me.uwtUnidadesOrganizativas.Levels(3).RelationName = "REL_NIV3_NIV4"
            Me.uwtUnidadesOrganizativas.Levels(3).ColumnName = "DEN"
            Me.uwtUnidadesOrganizativas.Levels(3).LevelKeyField = "COD"


            Me.uwtUnidadesOrganizativas.DataBind()
            Me.uwtUnidadesOrganizativas.DataKeyOnClient = True
            If Not IsNothing(uwtUnidadesOrganizativas.Nodes) Then
                uwtUnidadesOrganizativas.Nodes.Item(0).Expanded = True
            End If

            Dim sClientTexts As String
            sClientTexts = ""
            sClientTexts += "var arrTextosML = new Array();"
            sClientTexts += "arrTextosML[0] = '" + JSText(oTextos.Rows(3).Item(1)) + "';"
            sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
            Page.ClientScript.RegisterStartupScript(Me.GetType(),"TextosMICliente", sClientTexts)

            oTextos = Nothing
            oUnidadesOrg = Nothing

        End Sub

        Private Sub uwtUnidadesOrganizativas_NodeBound(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebNavigator.WebTreeNodeEventArgs) Handles uwtUnidadesOrganizativas.NodeBound

            Dim sValor As String = Request("Valor")
            Dim sUnidadOrg As Object = Split(sValor, " - ")
            Dim sUON1 As String
            Dim sUON2 As String
            Dim sUON3 As String
            Dim sDen As String
            Dim iNivel As Integer = UBound(sUnidadOrg)

            Select Case iNivel
                Case 0
                    sDen = sValor
                Case 1
                    sUON1 = sUnidadOrg(0)

                Case 2
                    sUON1 = sUnidadOrg(0)
                    sUON2 = sUnidadOrg(1)
                Case 3
                    sUON1 = sUnidadOrg(0)
                    sUON2 = sUnidadOrg(1)
                    sUON3 = sUnidadOrg(2)

            End Select

            Dim sScript As String

            If e.Node.Level = iNivel Then
                If iNivel = 3 Then
                    If e.Node.DataKey = sUON3 And e.Node.Parent.DataKey = sUON2 And e.Node.Parent.Parent.DataKey = sUON1 Then
                        e.Node.Parent.Expanded = True
                        e.Node.Parent.Parent.Expanded = True
                        sScript = "var oNode = igtree_getNodeById('uwtUnidadesOrganizativas" + e.Node.GetIdString + "');if (oNode) oNode.setSelected(true);"
                        Page.ClientScript.RegisterStartupScript(Me.GetType(),"node" + e.Node.DataKey.ToString(), "<script>" + sScript + "</script>")

                    End If

                ElseIf iNivel = 2 Then
                    If e.Node.DataKey = sUON2 And e.Node.Parent.DataKey = sUON1 Then
                        e.Node.Parent.Expanded = True
                        sScript = "var oNode = igtree_getNodeById('uwtUnidadesOrganizativas" + e.Node.GetIdString + "');if (oNode) oNode.setSelected(true);"
                        Page.ClientScript.RegisterStartupScript(Me.GetType(),"node" + e.Node.DataKey.ToString(), "<script>" + sScript + "</script>")

                    End If
                ElseIf iNivel = 1 Then
                    If e.Node.DataKey = sUON1 Then
                        sScript = "var oNode = igtree_getNodeById('uwtUnidadesOrganizativas" + e.Node.GetIdString + "');if (oNode) oNode.setSelected(true);"
                        Page.ClientScript.RegisterStartupScript(Me.GetType(),"node" + e.Node.DataKey.ToString(), "<script>" + sScript + "</script>")

                    End If
                ElseIf iNivel = 0 Then
                    If e.Node.Text = sDen Then
                        sScript = "var oNode = igtree_getNodeById('uwtUnidadesOrganizativas" + e.Node.GetIdString + "');if (oNode) oNode.setSelected(true);"
                        Page.ClientScript.RegisterStartupScript(Me.GetType(),"node" + e.Node.Text.ToString(), "<script>" + sScript + "</script>")

                    End If

                End If
            End If
        End Sub
    End Class

End Namespace