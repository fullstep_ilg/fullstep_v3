Namespace Fullstep.PMPortalWeb
    Partial Class materiales
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            Me.IDCONTROL.Value = Request("IdControl")
            Me.hid_txtMaterial.Value = Request("ClientId")
            If Request("readonly") = 1 Then
                Me.cmdAceptar.Visible = False
                Me.cmdCancelar.Visible = False
            End If

            CargarArbolMateriales()
        End Sub

        ''' blp: no pongo revisado por porque le corresponde a quien ha hecho cambios anteriormente en la funci�n
        ''' <summary>
        ''' Carga el Arbol de Materiales
        ''' </summary>
        ''' <remarks>Llamada desde:Page_Load; Tiempo m�ximo:0,1</remarks>
        Private Sub CargarArbolMateriales()
            Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim sIdi As String = Idioma
            Dim oGruposMaterial As Fullstep.PMPortalServer.GruposMaterial
            Dim lCiaComp As Long = IdCiaComp
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If

			oGruposMaterial = FSPMServer.get_GruposMaterial
            Dim iNivelSeleccion As Integer = Me.NivelSeleccion

            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSPMServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.BusquedaMateriales, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            Me.cmdAceptar.Value = oTextos.Rows(1).Item(1)
            Me.cmdCancelar.Value = oTextos.Rows(0).Item(1)

            Dim sScript As String
			Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
			Dim ilGMN1 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
            Dim ilGMN2 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
            Dim ilGMN3 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
            Dim ilGMN4 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN4
            sScript = ""
            sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
            sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
            sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
            sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TodosNiveles", "<script>var TodosNiveles = " & IIf(CType(Me.Page, FSPMPage).Acceso.gbMaterialVerTodosNiveles, 1, 0) & ";</script>")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "txtSeleccionarMaterial", "<script> var sSeleccionarMatNivel='" & oTextos.Rows(2).Item(1) & "'; var sSeleccionarMatNiveloSuperior='" & oTextos.Rows(3).Item(1) & "';</script>")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "sMat", "<script>var sMat='" & Request("Mat") & "'</script>")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NivelSeleccion", "<script>var NivelSeleccion = " & iNivelSeleccion & ";</script>")

            Dim sMat As String = Request("Mat")
            Dim bSelCualquierMatPortal As Boolean = Request("SelCualquierMatPortal")
            Dim arrMat(4) As String
            Dim lLongCod As Integer
            Dim i As Integer
            For i = 1 To 4
                arrMat(i) = ""
            Next i

            i = 1
            While Trim(sMat) <> ""
                Select Case i
                    Case 1
                        lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
                    Case 2
                        lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
                    Case 3
                        lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
                    Case 4
                        lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN4
                End Select
                arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
                sMat = Mid(sMat, lLongCod + 1)
                i = i + 1
            End While

            Dim sGMN1 As String = arrMat(1)
            Dim sGMN2 As String = arrMat(2)
            Dim sGMN3 As String = arrMat(3)
            Dim sGMN4 As String = arrMat(4)

            oGruposMaterial.LoadData(lCiaComp, IdCia, sGMN1, sGMN2, sGMN3, sGMN4, sIdi, bSelCualquierMatPortal, iNivelSeleccion)

            uwtMateriales.ClearAll()
			uwtMateriales.DataSource = oGruposMaterial.Data.Tables(0).DefaultView
			uwtMateriales.Levels(0).LevelImage = "./images/carpetaCommodityTxiki.gif"
			uwtMateriales.Levels(0).RelationName = "REL_NIV1_NIV2"
			uwtMateriales.Levels(0).ColumnName = "DEN_" + sIdi
			uwtMateriales.Levels(0).LevelKeyField = "COD"
			uwtMateriales.Levels(1).LevelImage = "./images/carpetaFamilia.gif"
			uwtMateriales.Levels(1).RelationName = "REL_NIV2_NIV3"
			uwtMateriales.Levels(1).ColumnName = "DEN_" + sIdi
			uwtMateriales.Levels(1).LevelKeyField = "COD"
			uwtMateriales.Levels(2).LevelImage = "./images/carpetaGrupo.gif"
			uwtMateriales.Levels(2).RelationName = "REL_NIV3_NIV4"
			uwtMateriales.Levels(2).ColumnName = "DEN_" + sIdi
			uwtMateriales.Levels(2).LevelKeyField = "COD"
			uwtMateriales.Levels(3).LevelImage = "./images/carpetaCerrada.gif"
			uwtMateriales.Levels(3).ColumnName = "DEN_" + sIdi
			uwtMateriales.Levels(3).LevelKeyField = "COD"
			uwtMateriales.DataBind()
			uwtMateriales.DataKeyOnClient = True
		End Sub

        ''' <summary>
        '''  Selecciona en el arbol el nodo q se le paso en la request
        ''' </summary>
        ''' <param name="sender">arbol uwtMateriales</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub uwtMateriales_NodeBound(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebNavigator.WebTreeNodeEventArgs) Handles uwtMateriales.NodeBound
            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")

            Dim sValor As String = Request("Valor")
            Dim arrMat(4) As String
            Dim lLongCod As Integer
            Dim i As Integer
            For i = 1 To 4
                arrMat(i) = ""
            Next i

            i = 1
            While Trim(sValor) <> ""
                Select Case i
                    Case 1
                        lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN1
                    Case 2
                        lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN2
                    Case 3
                        lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN3
                    Case 4
                        lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN4
                End Select
                arrMat(i) = Trim(Mid(sValor, 1, lLongCod))
                sValor = Mid(sValor, lLongCod + 1)
                i = i + 1
            End While


            Dim sGMN1 As String = arrMat(1)
            Dim sGMN2 As String = arrMat(2)
            Dim sGMN3 As String = arrMat(3)
            Dim sGMN4 As String = arrMat(4)
            Dim iNivel As Integer


            If sGMN4 <> Nothing Then
                iNivel = 4
            ElseIf sGMN3 <> Nothing Then
                iNivel = 3
            ElseIf sGMN2 <> Nothing Then
                iNivel = 2
            Else
                iNivel = 1
            End If
            Dim sScript As String

            If e.Node.Level = iNivel - 1 Then
                If iNivel = 4 Then
                    If e.Node.DataKey = sGMN4 And e.Node.Parent.DataKey = sGMN3 And e.Node.Parent.Parent.DataKey = sGMN2 And e.Node.Parent.Parent.Parent.DataKey = sGMN1 Then
                        e.Node.Parent.Expanded = True
                        e.Node.Parent.Parent.Expanded = True
                        e.Node.Parent.Parent.Parent.Expanded = True
                        sScript = "var oNode = igtree_getNodeById('uwtMateriales" + e.Node.GetIdString + "');if (oNode) oNode.setSelected(true);"
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "node" + e.Node.DataKey.ToString(), "<script>" + sScript + "</script>")

                    End If
                ElseIf iNivel = 3 Then
                    If e.Node.DataKey = sGMN3 And e.Node.Parent.DataKey = sGMN2 And e.Node.Parent.Parent.DataKey = sGMN1 Then
                        e.Node.Parent.Expanded = True
                        e.Node.Parent.Parent.Expanded = True
                        sScript = "var oNode = igtree_getNodeById('uwtMateriales" + e.Node.GetIdString + "');if (oNode) oNode.setSelected(true);"
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "node" + e.Node.DataKey.ToString(), "<script>" + sScript + "</script>")

                    End If

                ElseIf iNivel = 2 Then
                    If e.Node.DataKey = sGMN2 And e.Node.Parent.DataKey = sGMN1 Then
                        e.Node.Parent.Expanded = True
                        sScript = "var oNode = igtree_getNodeById('uwtMateriales" + e.Node.GetIdString + "');if (oNode) oNode.setSelected(true);"
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "node" + e.Node.DataKey.ToString(), "<script>" + sScript + "</script>")

                    End If
                ElseIf iNivel = 1 Then
                    If e.Node.DataKey = sGMN1 Then
                        sScript = "var oNode = igtree_getNodeById('uwtMateriales" + e.Node.GetIdString + "');if (oNode) oNode.setSelected(true);"
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "node" + e.Node.DataKey.ToString(), "<script>" + sScript + "</script>")

                    End If

                End If
            End If

        End Sub


        Public ReadOnly Property NivelSeleccion() As Integer
            Get
                If (Request.QueryString("NivelSeleccion") Is Nothing) Then
                    Return 0
                Else
                    Return Request.QueryString("NivelSeleccion")
                End If
            End Get
        End Property


    End Class
End Namespace