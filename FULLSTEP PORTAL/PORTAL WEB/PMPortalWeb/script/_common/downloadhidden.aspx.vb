Namespace Fullstep.PMPortalWeb
    Partial Class downloadhidden
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        Dim sPath As String

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            txtPath.Value = Server.UrlEncode(Request("path"))
            txtNombre.Value = Server.UrlEncode(Request("nombre"))
            idContrato.Value = Request("idContrato")
            idArchivoContrato.Value = DBNullToInteger(Request("idArchivoContrato"))
        End Sub

    End Class
End Namespace
