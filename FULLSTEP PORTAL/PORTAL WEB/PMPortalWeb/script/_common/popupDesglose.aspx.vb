Partial Class popupDesglose
    Inherits FSPMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Lblaniadir As System.Web.UI.WebControls.Label
    Protected WithEvents Lblquitar As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' Carga el popUp de desglose
    ''' </summary>
    ''' <param name="sender">del evento</param>
    ''' <param name="e">del evento</param>        
    ''' <remarks>Llamada desde; Tiempo m�ximo=0seg.</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
        Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
        Dim sIdi As String = oUser.Idioma

        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        If Not Me.IsPostBack Then
            Dim oDict As Fullstep.PMportalServer.Dictionary = FSPMServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.PopUpDesglose, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            Me.hypaniadir.Text = "" & oTextos.Rows(0).Item(1)
            Me.hypquitar.Text = "" & oTextos.Rows(1).Item(1)

            hypaniadir.Attributes.Add("OnClick", "javascript:aniadir(); return false;")
            hypquitar.Attributes.Add("OnClick", "javascript:quitar(); return false;")

        End If

    End Sub

End Class
