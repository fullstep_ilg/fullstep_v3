Namespace Fullstep.PMPortalWeb
    Partial Class tablaexternareg
        Inherits FSPMPage

        Public _msTitulo As String

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim lCiaComp As Long = IdCiaComp
            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
            Dim iTabla As Integer = CInt(Request.QueryString("tabla"))
            Dim oTablaExterna As Fullstep.PMPortalServer.TablaExterna = FSWSServer.get_TablaExterna
            oTablaExterna.LoadDefTabla(lCiaComp, iTabla, False)
            oTablaExterna.LoadData(lCiaComp, False)
            _msTitulo = oTablaExterna.Nombre
            Dim sTipoCampo As String
            For Each oRow As System.Data.DataRow In oTablaExterna.DefTabla.Tables(0).Rows
                If UCase(oRow.Item("CAMPO")) = UCase(oTablaExterna.PrimaryKey) Then
                    sTipoCampo = oRow.Item("TIPOCAMPO")
                    Exit For
                End If
            Next
            Dim oValor As Object
            Dim sValor As String = ""
            Select Case sTipoCampo
                Case "NUMERICO"
                    oValor = CType(Request.QueryString("valor"), Double)
                Case "FECHA"
                    oValor = CType(Request.QueryString("valor"), Date)
                Case "TEXTO"
                    oValor = CType(Request.QueryString("valor"), String)
            End Select
            If Not IsPostBack() Then
                Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & _
                    "<script language=""{0}"">{1}</script>"
                Dim sIdi As String = oUser.Idioma
                If sIdi = Nothing Then
                    sIdi = ConfigurationManager.AppSettings("idioma")
                End If
                Dim oDict As Fullstep.PMPortalServer.Dictionary = FSWSServer.Get_Dictionary()
                oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.TablaExterna, sIdi)
                Dim oTextos As DataTable = oDict.Data.Tables(0)
                cmdCerrar.Value = oTextos.Rows(2).Item(1)
                For Each oRow As System.Data.DataRow In oTablaExterna.Data.Tables(0).Rows
                    If oRow.Item(oTablaExterna.PrimaryKey) = oValor Then
                        For Each oColumn As System.Data.DataColumn In oTablaExterna.Data.Tables(0).Columns
                            If UCase(oColumn.ColumnName) <> UCase(oTablaExterna.ArtKey) Then
                                For Each oRow2 As System.Data.DataRow In oTablaExterna.DefTabla.Tables(0).Rows
                                    If UCase(oRow2.Item("CAMPO")) = UCase(oColumn.ColumnName) Then
                                        Dim oFila As New TableRow
                                        Dim oCeldaCab As New TableCell
                                        oCeldaCab.Text = oRow2.Item("NOMBRE")
                                        oCeldaCab.CssClass = "ugfilatablaCabecera"
                                        oFila.Cells.Add(oCeldaCab)
                                        Dim oCeldaDat As New TableCell
                                        oCeldaDat.Text = DBNullToSomething(oRow.Item(oColumn.ColumnName))
                                        oCeldaDat.CssClass = "ugfilatablaHist"
                                        oFila.Cells.Add(oCeldaDat)
                                        tblReg.Rows.Add(oFila)
                                        Exit For
                                    End If
                                Next
                            End If
                        Next
                        Exit For
                    End If
                Next

                Page.ClientScript.RegisterStartupScript(Me.GetType(),"redimwindow", String.Format(IncludeScriptKeyFormat, "javascript", "window.resizeTo(700," & (tblReg.Rows.Count * 25) + 150 & ")"))
            End If
        End Sub

    End Class
End Namespace
