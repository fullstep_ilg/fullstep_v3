﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Fullstep.PMWeb

    Partial Public Class detallesolicitudpadre

        '''<summary>
        '''Head1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Head1 As Global.System.Web.UI.HtmlControls.HtmlHead

        '''<summary>
        '''Form1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Form1 As Global.System.Web.UI.HtmlControls.HtmlForm

        '''<summary>
        '''lblIdentificador control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblIdentificador As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''txtIdentificador control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtIdentificador As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''lblTitulo control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblTitulo As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''txtTitulo control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtTitulo As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''lblImporteAprob control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblImporteAprob As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''txtImporteAprob control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtImporteAprob As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''lblImporteAcum control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblImporteAcum As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''txtImporteAcum control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtImporteAcum As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''cmdCerrar control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents cmdCerrar As Global.System.Web.UI.HtmlControls.HtmlInputButton
    End Class
End Namespace
