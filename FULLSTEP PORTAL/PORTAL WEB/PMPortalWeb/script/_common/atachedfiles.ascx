<%@ Control Language="vb" AutoEventWireup="false" Codebehind="atachedfiles.ascx.vb" Inherits="Fullstep.PMPortalWeb.Common.atachedfiles" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<SCRIPT type="text/javascript"><!--
var tipoOld
var idAdjuntoOld
var cellIdOld
/*
''' <summary>
''' Responder al evento click en una celda del grid bien sea SUSTITUIR, DESCARGAR o ELIMINAR
''' </summary>
''' <param name="gridName">Nombre del grid</param>
''' <param name="cellId">Id de la celda</param>        
''' <param name="button">Bot�n pulsado</param>    
''' <returns>Nada</returns>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
*/
		function uwgFiles_CellClickHandler(gridName, cellId, button){
			var columnClicked = igtbl_getColumnById(cellId)
			var row = igtbl_getRowById(cellId); 
			var IdAdjunto = row.getCellFromKey("ID").getValue();
			var tipo = row.getCellFromKey("TIPO").getValue();
			var portal = row.getCellFromKey("PORTAL").getValue();
			
			var nom = row.getCellFromKey("NOM").getValue()
			var coment = row.getCellFromKey("COMENTSalto").getValue()
			var input = document.all("<%=me.ClientID%>_FSENTRY").value 
			var instancia = ""
			if (document.all("<%=me.clientID%>_txtInstancia"))
			    instancia = document.all("<%=me.clientID%>_txtInstancia").value
		    var fecha = oRow.getCellFromKey("FECALTA").getValue()
			
			switch (columnClicked.Key)
				{
				case "SUSTITUIR":
					tipoOld=tipo
					idAdjuntoOld = IdAdjunto
					cellIdOld = cellId
					window.open("../_common/attachfile.aspx?instancia=" + instancia +"&id=" + IdAdjunto.toString() + "&tipo=" + tipo.toString() + "&Campo=" + document.all("<%=Me.ClientID%>_IDCAMPO").value +"&Input=" + document.all("<%=Me.ClientID%>_FSENTRY").value + "&Nombre=" + nom + "&Coment=" + coment, "_blank","width=600,height=275,status=no,resizable=no,top=200,left=200,menubar=yes")
					break;
			    case "DESCARGAR":
			        window.open("../_common/attach.aspx?instancia=" + instancia +"&id=" + IdAdjunto.toString() + "&tipo=" + tipo.toString()+ "&portal=" + portal.toString() + "&Campo=" + document.all("<%=Me.ClientID%>_IDCAMPO").value + "&Nombre=" + nom.toString() + "&fecha=" + fecha.toString() + "&esAltaFactura=0" , "_blank","width=600,height=275,status=no,resizable=yes,top=200,left=200,menubar=yes")
					break;
				case "ELIMINAR":
					p=window.opener
					oInput = p.fsGeneralEntry_getById(input)
					oInput.removeFile(tipo, IdAdjunto)
					
					fCambioNc(oInput,"",null)
					
					row.remove()
					break;
				}
			
			recorrerGrid()
			//Add code to handle your event here.
		}

function recorrerGrid()
{
var oGrid = igtbl_getGridById("<%=me.ClientID%>xuwgFiles")
var sAdjun = ""
for (i=0;i<oGrid.Rows.length;i++)
	{
	oRow = oGrid.Rows.getRow(i);
	sAdjun += oRow.getCellFromKey("NOM").getValue() + " (" +  oRow.getCellFromKey("DATASIZE").getValue().toString() + " kb.), "
	}
sAdjun = sAdjun.substr(0,sAdjun.length-2)
var input = document.all("<%=me.ClientID%>_FSENTRY").value 
oInput = p.fsGeneralEntry_getById(input)
oInput.setValue(sAdjun)
}
	
function anyadirAdjunto()
{

	window.open("../_common/attachfile.aspx?Campo=" + document.all("<%=me.ClientID%>_IDCAMPO").value +"&Input=" + document.all("<%=me.ClientID%>_FSENTRY").value,"_blank", "width=420,height=215,status=no,resizable=no,top=200,left=300")

}
/*
''' <summary>
''' A�adir en el grid el adjunto recien insertado.
''' </summary>
''' <param name="Id">Id en bbdd del adjunto</param>
''' <param name="nom">Nombre del adjunto</param>        
''' <param name="com">Comentario del adjunto</param>
''' <param name="datasize">tama�o del adjunto</param>
''' <param name="tipo">tipo de adjunto. 2 significa nuevo</param>
''' <param name="campo">Id en bbdd del campo fichero</param>
''' <param name="input">Id entry del campo fichero</param>
''' <param name="comsalto">Comentario del adjunto sin saltos de linea. Se han traducido por @VBCRLF@.</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:_common/attachfile.aspx; Tiempo m�ximo:0</remarks>
*/
function adjuntoAnyadido(Id,nom,com,datasize,tipo, campo, input,comsalto)
{
p=window.opener
oInput = p.fsGeneralEntry_getById(input)

if (cellIdOld)
	{
	oRow = igtbl_getRowById(cellIdOld); 
	oInput.removeFile(tipoOld, idAdjuntoOld)
	cellIdOld=null
	tipoOld = null
	idAdjuntoOld=null
	}
	
	
else
	oRow = igtbl_addNew("<%=me.ClientID%>xuwgFiles",0); 
	
	
oCell = oRow.getCellFromKey("TIPO")
oCell.setValue(tipo)
oCell = oRow.getCellFromKey("ID")
oCell.setValue(Id)
oCell = oRow.getCellFromKey("FECALTA")
oCell.setValue(new Date())
oCell = oRow.getCellFromKey("NOMBRE")
oCell.setValue("<%=Session("FS_Portal_User").Nombre%> <%=Session("FS_Portal_User").Apellidos%>")
oCell = oRow.getCellFromKey("NOM")
oCell.setValue(nom)
oCell = oRow.getCellFromKey("DATASIZE")
oCell.setValue(datasize)
oCell = oRow.getCellFromKey("COMENT")
while (com.toString().indexOf("&#39;") != -1){   
    com = com.toString().replace("&#39;","'");   
} 
while (com.toString().indexOf("&#34;") != -1){   
   com = com.toString().replace("&#34;",'"');   
}
oCell.setValue(com)
oCell = oRow.getCellFromKey("COMENTSalto")

while (comsalto.toString().indexOf("@VBCRLF@") != -1){   
    comsalto = comsalto.toString().replace("@VBCRLF@","/n");   
}  
oCell.setValue(comsalto)

oInput.addFile(Id)
recorrerGrid()

fCambioNc(oInput,"",null)
}

/*
''' <summary>
''' Las lineas rechazadas q cambien deben pasar a sin revisar
''' </summary>
''' <param name="oEdit">objeto q ha podido cambiar </param>
''' <param name="text">text del objeto</param>        
''' <param name="oEvent">Evento</param>    
''' <remarks>Llamada desde: uwgFiles_CellClickHandler	adjuntoAnyadido; Tiempo m�ximo: 0</remarks>*/
function fCambioNc (oEdit, text, oEvent) 
{
	if (oEdit.CtrlCambioRechazo)
	{
		try{
			var arrDesglose = oEdit.id.split("_")
		}
		catch(e){
			var arrDesglose = oEdit.ID.split("_") 					
		}	
		try{
			p=window.opener

			if (arrDesglose.length==7) 
			{
				var idGrupo = arrDesglose[3]
				var padre = arrDesglose[6]
				var index = arrDesglose[5]
				
				var desglose = arrDesglose[0] + "__" + arrDesglose[2] + "_" + idGrupo.toString() 
			}
			else
			{
				if (arrDesglose.length==9) 
				{
					var idGrupo = arrDesglose[3]
					var padre = arrDesglose[5]
					var index = arrDesglose[7]

					var desglose = arrDesglose[0] + "__" + arrDesglose[2] + "_" + idGrupo.toString()  + "_" + idGrupo.toString() + "_" + padre.toString()		
				}
				else
				{
					//4
					var idGrupo = ""
					var index = arrDesglose[2]
				
					var desglose = arrDesglose[0]			
				}
			}
					
			oEntry = p.fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + idGrupo.toString() + "1010")
			if (oEntry.getDataValue() == 3) //Rechazada
			{			
				oEntry.setValue(oEntry.NCSinRevisar)					
				oEntry.setDataValue(1)
				if (window.opener.document.getElementById(desglose + "_cmdFinAccion_" + index.toString()))
					window.opener.document.getElementById(desglose + "_cmdFinAccion_" + index.toString()).style.visibility = "visible"			
				//HA HABIDO UN CAMBIO DE ESTADO POR LO QUE HAY QUE NOTIFICAR
				if (window.opener.document.getElementById("Notificar"))
					window.opener.document.getElementById("Notificar").value = 1					
			}		
		}
		catch(e){}		
	}
}
-->
</SCRIPT>
<P></P>
<P>
	<TABLE id="Table1" width="100%">
		<TR>
			<TD style="WIDTH: 258px">
				<DIV title="  " style="DISPLAY: inline; WIDTH: 70px; HEIGHT: 15px" ms_positioning="FlowLayout"></DIV>
			</TD>
			<TD style="WIDTH: 506px"></TD>
			<TD colSpan="2"></TD>
		</TR>
		<tr>
			<td style="WIDTH: 258px"><asp:label id="lblTitulo" runat="server" CssClass="captionBlue">Alta de solicitud tipo:</asp:label></td>
			<td style="WIDTH: 506px"><asp:label id="lblTipo" runat="server" CssClass="captionDarkBlue">Tipo de solicitud</asp:label></td>
			<td colSpan="2"></td>
		</tr>
		<TR>
			<TD style="WIDTH: 258px; HEIGHT: 33px" width="258"><asp:label id="lblSubTitulo" runat="server" CssClass="captionBlue"> Grupo:</asp:label></TD>
			<td style="WIDTH: 506px"><asp:label id="lblNomCampo" runat="server" CssClass="captionDarkBlue">Nombre del campo</asp:label></td>
			<TD style="HEIGHT: 33px" align="right" width="20%"><asp:label id="lblDescargar" runat="server" CssClass="parrafo" Visible="False">Descargar todos</asp:label></TD>
			<TD style="HEIGHT: 33px" align="center" width="10%"><asp:imagebutton id="imgDescarga" runat="server" ImageUrl="images/DescargarEspecif.gif" Visible="False"></asp:imagebutton></TD>
		</TR>
	</TABLE>
</P>
<P><igtbl:ultrawebgrid id="uwgFiles" runat="server" Width="100%" Height="192px">
		<DisplayLayout AllowAddNewDefault="Yes" RowHeightDefault="20px" Version="4.00" HeaderClickActionDefault="SortSingle"
			BorderCollapseDefault="Separate" Name="xctl0xuwgFiles" AllowUpdateDefault="Yes">
			<AddNewBox Hidden="False">
				<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray" CustomRules="visibility:hidden;">

<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
</BorderDetails>

				</Style>
			</AddNewBox>
			<Pager>
				<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">

<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
</BorderDetails>

				</Style>
			</Pager>
			<HeaderStyleDefault Font-Size="XX-Small" Font-Names="Verdana" BackColor="WhiteSmoke" CssClass="VisorHead"></HeaderStyleDefault>
			<FrameStyle Width="100%" Height="192px" CssClass="VisorFrame"></FrameStyle>
			<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
				<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
			</FooterStyleDefault>
			<ClientSideEvents CellClickHandler="uwgFiles_CellClickHandler"></ClientSideEvents>
			<ActivationObject AllowActivation="False"></ActivationObject>
			<FixedHeaderStyleDefault CssClass="VisorHead"></FixedHeaderStyleDefault>
			<EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
			<RowStyleDefault BorderWidth="1px" Font-Size="XX-Small" Font-Names="Verdana" BorderColor="Gray" BorderStyle="Solid"
				CssClass="VisorRow">
				<Padding Left="3px"></Padding>
				<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
			</RowStyleDefault>
		</DisplayLayout>
		<Bands>
			<igtbl:UltraGridBand AllowUpdate="Yes" AllowAdd="Yes" RowSelectors="No">
				<HeaderStyle CssClass="CabeceraCampoArchivo"></HeaderStyle>
				<RowAlternateStyle CssClass="ugfilaalttabla"></RowAlternateStyle>
				<Columns>
					<igtbl:UltraGridColumn HeaderText="Descargar" Key="DESCARGAR" BaseColumnName="" AllowUpdate="No">
						<CellStyle BackgroundImage="../_common/images/descargar.gif" CustomRules="Background-repeat:no-repeat;background-position:center center"></CellStyle>
						<Footer Key="DESCARGAR"></Footer>
						<Header Key="DESCARGAR" Caption="Descargar"></Header>
					</igtbl:UltraGridColumn>
					<igtbl:UltraGridColumn HeaderText="Sustituir" Key="SUSTITUIR" BaseColumnName="" AllowUpdate="No">
						<CellStyle BackgroundImage="../_common/images/sustituir.gif" CustomRules="Background-repeat:no-repeat;background-position:center center"></CellStyle>
						<Footer Key="SUSTITUIR"></Footer>
						<Header Key="SUSTITUIR" Caption="Sustituir"></Header>
					</igtbl:UltraGridColumn>
					<igtbl:UltraGridColumn HeaderText="Eliminar" Key="ELIMINAR" BaseColumnName="" AllowUpdate="No">
						<CellStyle BackgroundImage="../_common/images/eliminar.gif" CustomRules="Background-repeat:no-repeat;background-position:center center"></CellStyle>
						<Footer Key="ELIMINAR"></Footer>
						<Header Key="ELIMINAR" Caption="Eliminar"></Header>
					</igtbl:UltraGridColumn>
				</Columns>
				<RowStyle CssClass="ugfilatabla"></RowStyle>
			</igtbl:UltraGridBand>
		</Bands>
	</igtbl:ultrawebgrid><INPUT id="cmdAnyadir" onclick="anyadirAdjunto()" type="button" value="A�adir archivo"
		name="cmdAnyadir" runat="server" class="boton">
    <INPUT id="IDCAMPO" type="hidden" name="IDCAMPO" runat="server">
    <INPUT id="txtInstancia" type="hidden" name="txtInstancia" runat="server">
    <INPUT id="FSENTRY" type="hidden" name="FSENTRY" runat="server">
</P>
