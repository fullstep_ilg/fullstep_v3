<%@ Page Language="vb" AutoEventWireup="false" Codebehind="popupTexto.aspx.vb" Inherits="PopUpTexto"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><%=Server.HTMLEncode(Request("titulo"))%></title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">

    <form id="Form1" method="post" runat="server">
	<TABLE WIDTH=100% HEIGHT = 100% BORDER = 0>
	<tr><td width=100% height=90%><TEXTAREA name=txtTextoMedio READONLY=true id=txtTextoMedio style='width:100%;height:97%;' type=text><%=Server.HTMLEncode(Request("texto"))%></TEXTAREA></td></tr>
	<tr><td align=center valign=center><input type=button value="<%=Request("boton")%>" onclick="window.close()" class="boton"></td></tr>
	</form>
  </body>
</html>
