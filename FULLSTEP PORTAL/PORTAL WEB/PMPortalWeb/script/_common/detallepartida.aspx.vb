Namespace Fullstep.PMPortalWeb
    Partial Class detallepartida
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim oPartida As Fullstep.PMPortalServer.Partida
            Dim ds As DataSet

            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Partidas

            Me.lblTitulo.Text = Request("Titulo")
            Me.lblCod.Text = Textos(9)
            Me.lblNombre.Text = Textos(10)
            Me.lblCentro.Text = Textos(1)
            Me.lblFechaInicio.Text = Textos(11)
            Me.lblFechaFin.Text = Textos(12)

            'carga los datos de la partida:
            oPartida = FSPMServer.Get_Partida
            ds = oPartida.BuscarDetallePartida(IdCiaComp, Request("PRES5"), Request("texto"), Idioma)

            If ds.Tables(0).Rows.Count > 0 Then
                lblCodBD.Text = ds.Tables(0).Rows(0).Item("COD")
                lblNombreBD.Text = DBNullToSomething(ds.Tables(0).Rows(0).Item("DEN"))
                lblFechaInicioBD.Text = FormatDate(ds.Tables(0).Rows(0).Item("FECINI"), FSPMUser.DateFormat)
                lblFechaFinBD.Text = FormatDate(ds.Tables(0).Rows(0).Item("FECFIN"), FSPMUser.DateFormat)
            End If
            If ds.Tables(1).Rows.Count > 0 Then
                lblCentroBD.Text = DBNullToSomething(ds.Tables(1).Rows(0).Item(0))
            End If

            ds = Nothing
            oPartida = Nothing
        End Sub

    End Class
End Namespace