﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Editor.aspx.vb" Inherits="Fullstep.PMPortalWeb.Editor" %>
<%@ Register assembly="CKEditor.NET" namespace="CKEditor.NET" tagprefix="CKEditor" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    
</head>
<body style="margin-top: 0px; margin-left: 0px; margin-right:0px">
<%If Request("readOnly") <> "1" Then%>
    <form id="form1" runat="server" action="javascript:handleCKEditorPost();void(0);">
    <div>
    <CKEditor:CKEditorControl ID="CKEditor1" runat="server" AutoPostBack="false" Height="500"></CKEditor:CKEditorControl>
    </div>
    </form>
<%Else%>
    <form id="form2" runat="server">
    <div runat="server" id="texto">
    </div>
    </form>
<%End If%>
</body>
</html>
<script type="text/javascript">
    ConfigurarIdioma();
    CargarTexto();
</script>