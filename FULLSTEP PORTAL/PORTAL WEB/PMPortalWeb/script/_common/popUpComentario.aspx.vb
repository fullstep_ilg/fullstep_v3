Partial Class popUpComentario
    Inherits FSPMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Protected WithEvents btnCerrar As System.Web.UI.HtmlControls.HtmlInputButton

    ''' <summary>
    ''' Mostrar los comentarios de apertura y cierre
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">evento de sistema</param>      
    ''' <returns>Nada</returns>
    ''' <remarks>Llamada desde: noConformidadesCerradas.aspx; Tiempo m�ximo:0,1</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
        Dim oDict As Fullstep.PMPortalServer.Dictionary
        Dim oTextos As DataTable
        Dim sIdi As String

        Dim sTextoApertura As String = Replace(Request("textoApertura"), "@VBCRLF@", vbCrLf)
        Dim sTextoCierre As String = Replace(Request("textoCierre"), "@VBCRLF@", vbCrLf)

        Me.TextoApertura.Value = sTextoApertura
        Me.TextoCierre.Value = sTextoCierre

        sIdi = Session("FS_Portal_User").Idioma.ToString
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        oDict = FSWSServer.Get_Dictionary()
        oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.NoConformidades, sIdi)
        oTextos = oDict.Data.Tables(0)

        'Textos:
        Me.lblComentarioApertura.Text = oTextos.Rows(21).Item(1)
        Me.lblComentarioCierre.Text = oTextos.Rows(22).Item(1)

        If Request("textoApertura") = Nothing Then
            Me.phApertura.Visible = False
        End If
        If Request("textoCierre") = Nothing Then
            Me.phCierre.Visible = False
        End If
    End Sub

End Class
