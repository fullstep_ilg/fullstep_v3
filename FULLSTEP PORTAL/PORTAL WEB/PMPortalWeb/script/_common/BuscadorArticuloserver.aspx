<%@ Page Language="vb" AutoEventWireup="false" Codebehind="BuscadorArticuloserver.aspx.vb" Inherits="Fullstep.PMPortalWeb.BuscadorArticuloserver"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script>	
	
	function rellenarCampos(idFSEntryCod,idFSEntryDen,sValor,bExiste,idFSEntryPrec,lPrecio,idFSEntryProv,sCodProv, sDenProv,idFSEntryUni, sUnidad,sDenUnidad,bGenerico,idfsEntryOrg,sCodOrg,sDenOrg,idfsEntryCentro,sCodCentro,sDenCentro) {
	var e = null;
	var oArt = null;	
	var oDen = null;
	var oPrecio = null;
	var oProveedor = null;	
	var oUnidad = null;
	var oOrg = null;
	var oCentro = null;
		
	p =window.parent.window.document;	
	idFSEntryCod = idFSEntryCod.replace("__t","");	
	e = p.getElementById(idFSEntryCod+"__tbl")
	if (e)
		oArt = e.Object;	
	
	idFSEntryDen = idFSEntryDen.replace("__t","");
	e = p.getElementById(idFSEntryDen+"__tbl");
	if (e)
		oDen = e.Object ;
	
	if ((sValor != null) && (sValor != "")) {
		oArt.articuloCodificado =  true;
		oArt.articuloGenerico = bGenerico;
		if ((oDen != null) && (oDen.tipoGS == 118)) {
			oDen.setValue(sValor);	
			oDen.articuloGenerico = bGenerico;
		}
	}
		
	/*	Si no existe el articulo y no tiene el check activado de Permitir a�adir nuevos articulos, 
	   Poner el codigo de articulo en blanco, visualizar mensaje		*/
	if ((!oArt.anyadir_art) && (!bExiste))
	{
		if ((oDen) && (oDen.tipoGS == 118))
			sValorAux = oDen.getDataValue();
		oArt.setValue("");		
		if ((oDen) && (oDen.tipoGS == 118)) {
			oDen.setValue(sValorAux);
			oDen.codigoArticulo="";
		}
		alert(sMensajeCOD);
		
	}else 
	{
		if ((oDen) && (oDen.tipoGS == 118))
			oDen.codigoArticulo = oArt.getDataValue();		
	
		//Cargar el valor del ULTIMO PRECIO ADJUDICADO
		if ((idFSEntryPrec) && (!bGenerico)) {
			idFSEntryPrec = idFSEntryPrec.replace("__t","");	
			e = p.getElementById(idFSEntryPrec+"__tbl")
			if (e)
				oPrecio = e.Object
			if (oPrecio)
				oPrecio.setValue(lPrecio);
		}
		
		//Cargar el valor del ULTIMO PROVEEDOR ADJUDICADO
		if ((idFSEntryProv) && (!bGenerico)) {
		    idFSEntryProv = idFSEntryProv.replace("__t","");	
			e = p.getElementById(idFSEntryProv+"__tbl")
			if (e)
				oProveedor = e.Object
			if (oProveedor) {
				if (sCodProv!='')
					oProveedor.setValue(sCodProv + " - " + sDenProv);
				else
					oProveedor.setValue('');
				oProveedor.setDataValue(sCodProv);
			}
		}
		
		//Cargar la Unidad del articulo seleccionado.
		if (idFSEntryUni) {
			idFSEntryUni = idFSEntryUni.replace("__t","");	
			e = p.getElementById(idFSEntryUni+"__tbl")
			if (e)
				oUnidad = e.Object
			if (oUnidad) {
				oUnidad.setValue(sDenUnidad);
				oUnidad.setDataValue(sUnidad);					
			}else
					//Si la Unidad esta oculta almacenarla en el atributo del articulo UnidadOculta el valor de la UNIDAD actualizado
					if (oArt) {			
						if (oArt.UnidadDependent) {
							oArt.UnidadDependent.value = sUnidad;
						}
					}else
						if (oDen)
							if (oDen.UnidadDependent) {
								oDen.UnidadDependent.value = sUnidad;
							}
		}	

		//Cargar la organizacion del Art�culo seleccionado
		if (idfsEntryOrg) {
			idfsEntryOrg = idfsEntryOrg.replace("__t","");	
			e = p.getElementById(idfsEntryOrg+"__tbl")
			if (e)
				oOrg = e.Object
			if (oOrg) {
				if (sCodOrg!='')
					oOrg.setValue(sCodOrg + " - " + sDenOrg);
				else
					oOrg.setValue('');
				oOrg.setDataValue(sCodOrg);
			}		
		}
		
		//Cargar el centro de organizacion del Art�culo seleccionado
		if (idfsEntryCentro) {
			idfsEntryCentro = idfsEntryCentro.replace("__t","");	
			e = p.getElementById(idfsEntryCentro+"__tbl")
			if (e)
				oCentro = e.Object
			if (oCentro) {
				if (sCodCentro!='')
					oCentro.setValue(sCodCentro + " - " + sDenCentro);
				else
					oCentro.setValue('');
				oCentro.setDataValue(sCodCentro);
			}		
		}			
	}
	
	if (p.getElementById("bMensajePorMostrar"))
		p.getElementById("bMensajePorMostrar").value="0"
	window.close();
	
	
	}
	
	function ponerCodigoArticulo (idFSEntryCod,idFSEntryDen,bGenerico,bExiste) {
	var e = null;
	var oArt = null;
	var oDen = null;	
	
	p =window.parent.window.document;
	
	idFSEntryCod = idFSEntryCod.replace("__t","");
	e = p.getElementById(idFSEntryCod+"__tbl");
	if (e)
		oArt = e.Object ;
	
	if (oArt) {
		if ((bExiste) && !(bGenerico)) {
			idFSEntryDen = idFSEntryDen.replace("__tden","");
			idFSEntryDen = idFSEntryDen.replace("__t","");
			e = p.getElementById(idFSEntryDen+"__tbl");
			if (e)
				oDen = e.Object ;
			if (oDen) 
				sValorAnt = oDen.getValue();			
			alert(sMensajeCOD);
			oArt.setValue("");					
			if (oDen)  {
				oDen.setValue(sValorAnt);
				oDen.codigoArticulo="";
			}
			
			
		}
	}
	if (p.getElementById("bMensajePorMostrar"))
		p.getElementById("bMensajePorMostrar").value="0"
	window.close();
	}
	
	function ponerMaterial (IDControl, sGMN1, sGMN2, sGMN3, sGMN4, sDen,TodosNiveles) {
	var sDenAux = '';	
	
	p =window.parent.window.document;
	
	idFSEntryMat= IDControl.replace("__t","");
	e = p.getElementById(idFSEntryMat+"__tbl");
	if (e)
		o = e.Object ;	
	
	s=""
	for (i=0;i<ilGMN1-sGMN1.length;i++)
		s+=" "
	
	sMat = s + sGMN1
	sDenAux +=  ' (' + sGMN1
	if (sGMN2)
	{
		s=""
		for (i=0;i<ilGMN2-sGMN2.length;i++)
			s+=" "
		sMat = sMat + s + sGMN2
        sDenAux += ' - ' + sGMN2
        if (TodosNiveles == 1) {
            sDenShort = sGMN1 + ' - ' + sGMN2 + ' - ' + sDen
        } else {
            sDenShort = sGMN2 + ' - ' + sDen
        }
	}
	else
		sDenShort = sGMN1 + ' - ' + sDen

	if (sGMN3)
	{
		s=""
		for (i=0;i<ilGMN3-sGMN3.length;i++)
			s+=" "
		sMat = sMat + s + sGMN3
        sDenAux += ' - ' + sGMN3
        if (TodosNiveles == 1) {
            sDenShort = sGMN1 + ' - ' + sGMN2 + ' - ' + sGMN3 + ' - ' + sDen
        } else {
            sDenShort = sGMN3 + ' - ' + sDen
        }
	}
	if (sGMN4)
	{
		s=""
		for (i=0;i<ilGMN4-sGMN4.length;i++)
			s+=" "
		sMat = sMat + s + sGMN4
        sDenAux += ' - ' + sGMN4
        if (TodosNiveles == 1) {
            sDenShort = sGMN1 + ' - ' + sGMN2 + ' - ' + sGMN3 + ' - ' + sGMN4 + ' - ' + sDen
        } else {
            sDenShort = sGMN4 + ' - ' + sDen
        }
		
	}
	sDenAux += ')'
	o.setDataValue(sMat)
	o.setValue(sDenShort)
	o.setToolTip(sDenAux)	
	}
	
	function ponerCodArt_Den_Blanco(idFSEntryCod,idFSEntryDen) {

	p =window.parent.window.document;
	//Poner el codigo de Articulo en blanco
	idFSEntryCod = idFSEntryCod.replace("__t","");	
	e = p.getElementById(idFSEntryCod+"__tbl")
	if (e)
		oEntry = e.Object
		
	if (oEntry) {
		oEntry.setValue("");	
	
	}
	//Poner la denominaci�n en blanco
	idFSEntryDen = idFSEntryDen.replace("__t","");	
	e = p.getElementById(idFSEntryDen+"__tbl")
	if (e)
		oEntry = e.Object
		
	if (oEntry) 
		oEntry.setValue("");	
	
	}
		</script>
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
		</form>
	</body>
</html>
