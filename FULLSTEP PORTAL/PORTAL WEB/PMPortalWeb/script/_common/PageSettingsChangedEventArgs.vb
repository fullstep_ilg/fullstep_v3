﻿Public Class PageSettingsChangedEventArgs
    Inherits System.EventArgs
    Public Property PageNumber() As Integer
        Get
            Return m_PageNumber
        End Get
        Set(ByVal value As Integer)
            m_PageNumber = Value
        End Set
    End Property
    Private m_PageNumber As Integer
    Public Property PageSize() As Integer
        Get
            Return m_PageSize
        End Get
        Set(ByVal value As Integer)
            m_PageSize = Value
        End Set
    End Property
    Private m_PageSize As Integer
    Public Property TotalNumberOfPages() As Integer
        Get
            Return m_TotalNumberOfPages
        End Get
        Set(ByVal value As Integer)
            m_TotalNumberOfPages = Value
        End Set
    End Property
    Private m_TotalNumberOfPages As Integer

    Public ReadOnly Property PageIndex() As Integer
        Get
            Return Me.PageNumber - 1
        End Get
    End Property

    ''' Revisado por: blp. Fecha:30/01/2012
    ''' <summary>
    ''' Método para crear instancia de la clase
    ''' </summary>
    ''' <remarks>Llamada desde la creación de la instancia. Máx. 0 seg.</remarks>
    Public Sub New()
    End Sub
End Class
