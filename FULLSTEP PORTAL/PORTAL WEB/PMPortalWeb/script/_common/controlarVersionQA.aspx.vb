Partial Class controlarVersionQA
    Inherits FSPMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    ''' <summary>
    ''' Carga de pantalla. Realmente lo q hace es q por ajax sincrono se desea saber si se puede o no 
    ''' grabar lo q esta editando el usuario en DetalleNoConformidad.aspx � DetalleCertificado.aspx.
    ''' </summary>
    ''' <param name="sender">pantalla</param>
    ''' <param name="e">parametro de sistema</param>     
    ''' <remarks>Llamada desde: DetalleNoConformidad.aspx DetalleCertificado.aspx; Tiempo m�ximo: 0,1</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Response.Expires = -1
        If Request("Certificado") Is Nothing Then
            ComprobacionesNoConform()
        Else
            ComprobacionesCertif()
        End If
    End Sub
    ''' <summary>
    ''' Se desea saber si se puede o no grabar lo q esta editando el usuario en DetalleNoConformidad.aspx
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo m�ximo:0,1</remarks>
    Private Sub ComprobacionesNoConform()
        Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
        Dim oInstancia As Fullstep.PMPortalServer.Instancia
        Dim lCiaComp As Long = IdCiaComp
        Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
        Dim sIdi As String = oUser.Idioma

        oInstancia = FSPMServer.Get_Instancia
        oInstancia.ID = Request("Instancia")

        oInstancia.ComprobarGrabacionNC(lCiaComp)

        If oInstancia.EnProceso = 1 Then
            Responder("1")
        ElseIf oInstancia.Version = Request("Version") AndAlso oInstancia.TipoVersion = Request("tipoVersion") Then
            Responder("0")
        Else 'Not (oInstancia.Version = Request("Version"))
            If Request("tipoVersion") = 3 Then
                Responder("2")
            Else
                Responder("3")
            End If
        End If
    End Sub
    ''' <summary>
    ''' Se desea saber si se puede o no grabar lo q esta editando el usuario en DetalleCertificado.aspx
    ''' </summary>
    ''' <remarks>Llamada desde:Page_Load; Tiempo m�ximo:0,1</remarks>
    Private Sub ComprobacionesCertif()
        Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
        Dim oInstancia As Fullstep.PMPortalServer.Instancia
        Dim lCiaComp As Long = IdCiaComp

        If Request("Instancia") = "" Then
            'Automatico
            Responder("0")
        Else
            oInstancia = FSPMServer.Get_Instancia
            oInstancia.ID = Request("Instancia")

            oInstancia.ComprobarGrabacionCertif(lCiaComp, Request("Certificado"))

            If oInstancia.EnProceso = 1 Then
                Responder("1")
            ElseIf Not oInstancia.CertifActivo Then
                Responder("2")
            ElseIf Not oInstancia.CertifPublicado Then
                Responder("3")
            ElseIf oInstancia.TipoVersion = 0 OrElse (oInstancia.Version = strToInt(Request("Version")) AndAlso oInstancia.TipoVersion = strToInt(Request("tipoVersion"))) Then
                Responder("0")
            Else 'Not (oInstancia.Version = Request("Version"))
                If Request("tipoVersion") = 3 Then
                    Responder("4")
                Else
                    Responder("5")
                End If
            End If
        End If
    End Sub
    ''' <summary>
    ''' Se ha hecho una llamada Ajax sincrona a esta pantalla, esta funci�n devuelve el resultado de la ejecuci�n
    ''' </summary>
    ''' <param name="Respuesta">Respuesta de la ejecuci�n</param>    
    ''' <remarks>Llamada desde: Page_Load; Tiempo m�ximo: 0</remarks>
    Private Sub Responder(ByVal Respuesta As String)
        Response.Clear()
        Response.ContentType = "text/plain"
        Response.Write(Respuesta)
        Response.End()
    End Sub
End Class