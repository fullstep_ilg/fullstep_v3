Imports ExpertPdf.HtmlToPdf
Imports Winista.Mime

Namespace CommonAlta
    Module Alta
        ''' <summary>
        ''' Carga el componente webCalendar.
        ''' </summary>
        ''' <param name="sIdi">Codigo Idioma. Idioma del usuario</param>      
        ''' <returns>Devuelve un componente de tipo WebCalendar configurado con el idioma del usuario</returns>
        ''' <remarks>Llamada desde=  GuardarInstancia.vb --> TrasladoUsu //NWAlta.aspx.vb -->Page_load
        '''                          alta.aspx.vb --> Page_load          //altaCertificado.aspx.vb--> Page_load
        '''                          DetalleCertificado.aspx.vb          //filtroCertificados.aspx.vb--> CargarCertificados
        '''                          AltaNoConformidad.aspx.vb           //NoConformidad.aspx.vb--> Page_load
        '''                          NoConformidadRevisor.aspx.vb        //NwDetalleSolicitud.aspx.vb --> Page_load
        '''                          NWSeguimiento.aspx.vb -->Page_load  // NWWorkflow.aspx.vb -->Page_load
        ''' ; Tiempo m�ximo=0seg.</remarks>

        Public Function InsertarCalendario(ByVal sIdioma As String) As Infragistics.WebUI.WebSchedule.WebCalendar

            Dim SharedCalendar As New Infragistics.WebUI.WebSchedule.WebCalendar

            With SharedCalendar
                .ClientSideEvents.InitializeCalendar = "initCalendarEvent"
                .ClientSideEvents.DateClicked = "calendarDateClickedEvent"
                With .Layout
                    .NextMonthImageUrl = "ig_cal_grayN.gif"
                    .ShowTitle = "False"
                    .PrevMonthImageUrl = "ig_cal_grayP.gif"
                    '.FooterFormat = "Today: {0:d}"
                    .FooterFormat = ""
                    .FooterStyle.CssClass = "igCalendarFooterStyle"
                    .TodayDayStyle.CssClass = "igCalendarSelectedDayStyle"
                    .OtherMonthDayStyle.CssClass = "igCalendarOtherMonthDayStyle"
                    .CalendarStyle.CssClass = "igCalendarStyle"
                    .TodayDayStyle.CssClass = "igCalendarTodayDayStyle"
                    .DayHeaderStyle.CssClass = "igCalendarDayHeaderStyle"
                    .TitleStyle.CssClass = "igCalendarTitleStyle"
                End With
            End With

            Return SharedCalendar
        End Function
    End Module
End Namespace

Namespace Fullstep.PMPortalWeb
    Module modUtilidades

        Public Function DBNullToSomething(Optional ByVal value As Object = Nothing) As Object
            If IsDBNull(value) Then
                Return Nothing
            Else
                Return value
            End If
        End Function

        Public Function DBNullToStr(Optional ByVal value As Object = Nothing) As String
            If IsDBNull(value) Then
                Return ""
            Else
                Return value
            End If
        End Function

        Public Function DBNullToDbl(Optional ByVal value As Object = Nothing) As Double
            If IsDBNull(value) Then
                Return 0
            ElseIf value Is Nothing Then
                Return 0
            Else
                Return CDbl(value)
            End If
        End Function

        ''' Revisado por: blp. Fecha: 12/12/2011
        ''' <summary>
        ''' Funci�n que devuelve el valor pasado si este no es null. Si es null devuelve 0
        ''' </summary>
        ''' <param name="valor">Valor a evaluar</param>
        ''' <returns>El valor o 0</returns>
        ''' <remarks>Llamada desde entregasPedidos.aspx.vb->whdgEntregas_InitializeRow. M�x. 0,1 seg.</remarks>
        Public Function DBNullToInt(ByVal valor As Object) As Integer
            Return IIf(IsDBNull(valor), 0, valor)
        End Function

        Public Function ValidFilename(ByVal sFileName As String)
            Dim s As String

            s = sFileName

            s = Replace(s, "\", "_")
            s = Replace(s, "/", "_")
            s = Replace(s, ":", "_")
            s = Replace(s, "*", "_")
            s = Replace(s, "?", "_")
            s = Replace(s, """", "_")
            s = Replace(s, ">", "_")
            s = Replace(s, "<", "_")
            s = Replace(s, "|", "_")
            s = Replace(s, "@", "_")


            ValidFilename = s


        End Function

        Public Function ReturnToSpace(ByVal sItem As String)
            Dim newsItem As String

            newsItem = Trim(sItem.Replace(vbCrLf, " "))
            Return newsItem

        End Function

        Public Function FormatNumber(ByVal dNumber As Double, ByVal oNumberFormat As System.Globalization.NumberFormatInfo) As String

            Return dNumber.ToString("N", oNumberFormat)

        End Function
        ''' <summary>
        ''' Devuelve un string con la fecha pasada en dDate formateada seg�n oDateFormat, por defecto aplica formato d:01/01/1900
        ''' </summary>
        ''' <param name="dDate">fecha a formatear</param>
        ''' <param name="oDateFormat">IFormatProvider</param>
        ''' <param name="sFormat">Format: d :08/17/2000; D :Thursday, August 17, 2000; f :Thursday, August 17, 2000 16:32; F :Thursday, August 17, 2000 16:32:32; g :08/17/2000 16:32;G :08/17/2000 16:32:32...
        '''</param>
        ''' <returns>String con la fecha</returns>
        ''' <remarks></remarks>
        Public Function FormatDate(ByVal dDate As Date, ByVal oDateFormat As System.Globalization.DateTimeFormatInfo, Optional ByVal sFormat As String = "d") As String
            Return dDate.ToString(sFormat, oDateFormat)
        End Function
        ''' <summary>
        ''' Convierte un texto para utilizarlo como cadena en javascript
        ''' </summary>
        ''' <param name="text">Texto a convertir</param>
        ''' <returns>Un String con el texto formateado para poderlo utilizar como cadena en javascript</returns>
        Function JSText(ByVal text As String)

            Dim t
            If text = Nothing Then
                JSText = ""
                Exit Function
            End If
            t = Replace(text, Chr(92), "\\")
            t = Replace(t, "'", "\'")
            t = Replace(t, """", "\""")
            t = Replace(t, Chr(9), "\t")
            t = Replace(t, Chr(13) & Chr(10), "\n")
            t = Replace(t, Chr(10), "\n")
            t = Replace(t, Chr(13), "\n")

            JSText = t


        End Function
        Function JSTextP(ByVal text)

            Dim t
            If text = Nothing Then
                JSTextP = ""
                Exit Function
            End If
            t = Replace(text, "'", "&#92;&#39;")
            t = Replace(t, """", "&#92;&#34;")
            t = Replace(t, Chr(13) & Chr(10), "\n")
            t = Replace(t, Chr(10), "\n")
            t = Replace(t, Chr(13), "\n")

            JSTextP = t


        End Function

        Function DevolverFechaRelativaA(ByVal iTipoFecha As Fullstep.PMPortalServer.TiposDeDatos.TipoFecha, ByVal dtFechaFija As Date) As Date
            If Not dtFechaFija = Nothing Then
                Return dtFechaFija
            End If
            Select Case iTipoFecha
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.FechaAlta
                    Return Now
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.UnDiaDespues
                    Return DateAdd(DateInterval.Day, 1, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.DosDiasDespues
                    Return DateAdd(DateInterval.Day, 2, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.TresDiasDespues
                    Return DateAdd(DateInterval.Day, 3, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.CuatroDiasDespues
                    Return DateAdd(DateInterval.Day, 4, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.CincoDiasDespues
                    Return DateAdd(DateInterval.Day, 5, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.SeisDiasDespues
                    Return DateAdd(DateInterval.Day, 6, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.UnaSemanaDespues
                    Return DateAdd(DateInterval.Day, 7, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.DosSemanaDespues
                    Return DateAdd(DateInterval.Day, 14, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.TresSemanaDespues
                    Return DateAdd(DateInterval.Day, 21, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.UnMesDespues
                    Return DateAdd(DateInterval.Month, 1, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.DosMesDespues
                    Return DateAdd(DateInterval.Month, 2, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.TresMesDespues
                    Return DateAdd(DateInterval.Month, 3, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.CuatroMesDespues
                    Return DateAdd(DateInterval.Month, 4, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.CincoMesDespues
                    Return DateAdd(DateInterval.Month, 5, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.SeisMesDespues
                    Return DateAdd(DateInterval.Month, 6, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.UnAnyoDespues
                    Return DateAdd(DateInterval.Year, 1, Today)
                Case Else
                    Return dtFechaFija
            End Select

        End Function
        Public Function VacioToNothing(ByVal s As Object)
            Dim o As Object = DBNullToSomething(s)
            If o Is Nothing Then Return Nothing
            If o.ToString() = "" Then
                Return Nothing
            Else
                Return o
            End If
        End Function
        Public Function igDateToDate(ByVal s As String) As DateTime

            Try
                If s = Nothing Or s = "" Then
                    Return Nothing
                End If
                Dim arr As Array = s.Split("-")

                Return DateSerial(arr(0), arr(1), arr(2))


            Catch ex As Exception
                Return Nothing

            End Try

        End Function
        Public Function DateToigDate(ByVal dt As DateTime) As String
            Try
                If dt = Nothing Then
                    Return ""
                End If
                '2005-7-14-12-7-14-244
                'anyo-m-di-ho-m-se-mil

                Dim s As String = Year(dt).ToString() + "-" + Month(dt).ToString() + "-" + Day(dt).ToString() + "-" + Hour(dt).ToString() + "-" + Minute(dt).ToString() + "-" + Second(dt).ToString()
                Return s

            Catch ex As Exception

                Return ""
            End Try
        End Function

        Public Function Numero(ByVal value As String, Optional ByVal sDecimalSeparator As String = "", Optional ByVal sThousanSeparator As String = "") As Double
            If value = Nothing Then
                Return Nothing
            Else
                If sDecimalSeparator <> "" Then
                    Try
                        value = value.Replace(sThousanSeparator, "")
                    Catch
                    End Try

                    value = value.Replace(sDecimalSeparator, System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)

                    Try
                        Return CDbl(value)
                    Catch
                        Return Nothing
                    End Try

                Else
                    If IsNumeric(value) Then
                        Return CDbl(value)
                    Else
                        Return Nothing
                    End If
                End If
            End If
        End Function
        Function JSNum(ByVal Num As Object) As String

            Dim t As String
            Try
                If IsDBNull(Num) Then
                    Return "null"
                End If
                t = Replace(Num.ToString(), ",", ".")
                Return t

            Catch ex As Exception

                Return "null"
            End Try


        End Function

        Function GenerarDataTableAcciones() As DataTable
            Dim dt As DataTable
            Dim dc As DataColumn
            dt = New DataTable("ACCIONES")
            dc = New DataColumn("ID")
            dc.DataType = System.Type.GetType("System.String")
            dt.Columns.Add(dc)
            dc = New DataColumn("IMAGEN")
            dc.DataType = System.Type.GetType("System.String")
            dt.Columns.Add(dc)
            dc = New DataColumn("TEXTO")
            dc.DataType = System.Type.GetType("System.String")
            dt.Columns.Add(dc)
            dc = New DataColumn("JAVASCRIPTEVENT")
            dc.DataType = System.Type.GetType("System.String")
            dt.Columns.Add(dc)
            dc = New DataColumn("TITULO")
            dc.DataType = System.Type.GetType("System.String")
            dt.Columns.Add(dc)
            dc = New DataColumn("WIDTH")
            dc.DataType = System.Type.GetType("System.Double")
            dt.Columns.Add(dc)
            Return dt
        End Function


        Public Function strToDBNull(Optional ByVal value As Object = Nothing) As Object
            If value = "" Then
                Return System.DBNull.Value
            Else
                Return value
            End If
        End Function

        Public Function StrToSQLNULL(ByVal StrThatCanBeEmpty As Object) As Object

            If IsDBNull(StrThatCanBeEmpty) Then
                StrToSQLNULL = "NULL"
            Else
                If StrThatCanBeEmpty = "" Then
                    StrToSQLNULL = "NULL"
                Else
                    StrToSQLNULL = "'" & DblQuote(CStr(StrThatCanBeEmpty)) & "'"
                End If
            End If

        End Function
        Public Function DblQuote(ByVal StrToDblQuote As String) As String

            DblQuote = Replace(StrToDblQuote, "'", "''")

        End Function
        Public Function DateToSQLDate(ByVal DateThatCanBeEmpty As Object) As Object

            Try
                If IsDBNull(DateThatCanBeEmpty) Then
                    Return "NULL"
                Else
                    If IsDate(DateThatCanBeEmpty) Then
                        Return "Convert(datetime,'" & Format(DateThatCanBeEmpty, "MM-dd-yyyy") & "',110)"
                    Else
                        If DateThatCanBeEmpty = "" Then
                            Return "NULL"
                        Else
                            Return "Convert(datetime,'" & Format(DateThatCanBeEmpty, "MM-dd-yyyy") & "',110)"
                        End If
                    End If

                End If

            Catch ex As Exception

                Return "NULL"

            End Try
        End Function
        Public Function DblToSQLFloat(ByVal DblToConvert As Object) As String

            Dim StrToConvert As String
            Dim sDecimal As String
            Dim sThousand As String

            Try

                If DblToConvert Is Nothing Then
                    DblToSQLFloat = "NULL"
                    Exit Function
                End If
                If IsDBNull(DblToConvert) Then
                    DblToSQLFloat = "NULL "
                    Exit Function
                End If


                If DblToConvert = "" Then
                    DblToSQLFloat = "NULL"
                    Exit Function
                End If
                StrToConvert = CStr(DblToConvert)
                StrToConvert = Replace(StrToConvert, ",", ".")
                DblToSQLFloat = StrToConvert
            Catch ex As Exception
                If IsNumeric(DblToConvert) Then
                    StrToConvert = CStr(DblToConvert)
                    StrToConvert = Replace(StrToConvert, ",", ".")
                    Return StrToConvert
                Else
                    Return "NULL"
                End If

            End Try



        End Function


        Public Sub AplicarEstilosTab(ByRef oTab As Infragistics.WebUI.UltraWebTab.UltraWebTab)

            'oTab.DefaultTabStyle.BackColor = Color.FromArgb(&HE5E5B5)

            oTab.ImageDirectory = ConfigurationManager.AppSettings("rutanormal") & "/App_Themes/" & CType(System.Web.HttpContext.Current.Handler, Page).Theme & "/images/"
            oTab.BorderColor = Color.FromName(ObtenerValorPropiedad("#uwtDefaultTab", "border-color"))




        End Sub

        Public Function ObtenerValorPropiedad(ByVal sBloque As String, ByVal sProp As String) As String

            Dim sPath As String

            sPath = HttpContext.Current.Server.MapPath("../../App_Themes/" & CType(System.Web.HttpContext.Current.Handler, Page).Theme)
            Dim oFile As New System.IO.StreamReader(sPath & "\" & CType(System.Web.HttpContext.Current.Handler, Page).Theme & ".css")



            Dim sStyle As String = oFile.ReadToEnd()

            oFile.Close()
            oFile = Nothing
            Dim lIndex As Long
            Dim sAux As String

            lIndex = sStyle.IndexOf(sBloque)

            sAux = sStyle.Substring(lIndex, sStyle.Length - lIndex)
            lIndex = sAux.IndexOf("}")

            sAux = sAux.Substring(0, lIndex)

            lIndex = sAux.IndexOf(sProp)
            sAux = sAux.Substring(lIndex, sAux.Length - lIndex)
            lIndex = sAux.IndexOf(";")
            sAux = sAux.Substring(sAux.IndexOf(":") + 1, lIndex - sAux.IndexOf(":") - 1)
            Return sAux


        End Function

        Public Function IsTime(ByVal DataAsked) As Boolean
            Dim s As String
            Dim pos As Integer

            s = CStr(DataAsked)

            If IsDate("01/01/1990 " & DataAsked) = False Then
                IsTime = False
                Exit Function
            End If

            IsTime = True

        End Function

        Public Function GenerateRandomPath() As String
            Dim s As String = ""
            Dim i As Integer
            Dim lowerbound As Integer = 65
            Dim upperbound As Integer = 90
            Randomize()
            For i = 1 To 10
                s = s & Chr(Int((upperbound - lowerbound + 1) * Rnd() + lowerbound))
            Next
            Return s

        End Function

        ''' <summary>
        ''' Convierte un fichero HTML en otro con formato PDF
        ''' </summary>
        ''' <param name="sPathHTML">Path y nombre del fichero HTML</param>
        ''' <param name="sPathPDF">Path donde se va a dejar y nombre</param>
        ''' <remarks>LLamada desde: impexp.aspx, flowDiagramExport.aspx, visorCertificados.aspx, noconformidadcerrada.aspx; Tiempo m�ximo: 0,1 sg.</remarks>
        Sub ConvertHTMLToPDF(ByVal sPathHTML As String, ByVal sPathPDF As String)
            'Pasamos el HTML a PDF utilizando el componente ExpertPdf (ephtmltopdf.dll)
            Dim pdfConverter As ExpertPdf.HtmlToPdf.PdfConverter = New PdfConverter()
            Dim pdfDocument As PdfDocument.Document

            pdfConverter.LicenseKey = "HzQtPyc/LCYvPygxLz8sLjEuLTEmJiYm"    ''Le pasamos la clave de licencia
            pdfConverter.PdfDocumentOptions.LeftMargin = 15
            pdfConverter.PdfDocumentOptions.RightMargin = 15
            pdfConverter.PdfDocumentOptions.TopMargin = 15
            pdfConverter.PdfDocumentOptions.BottomMargin = 15

            pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlFile(sPathHTML)
            pdfDocument.Save(sPathPDF)
            pdfDocument.Close()
        End Sub

        Public Function DateToBDDate(ByVal DateValue As Date) As String
            'Ponemos la fecha en formato dd/mm/yyyy pq en la BD se trata asi
            Dim DateFormat As System.Globalization.DateTimeFormatInfo
            Dim sCurrentCulture As String

            Try
                If IsDBNull(DateValue) Then
                    DateToBDDate = "NULL"
                Else


                    sCurrentCulture = System.Globalization.CultureInfo.CurrentCulture.Name
                    DateFormat = New System.Globalization.CultureInfo(sCurrentCulture, False).DateTimeFormat
                    DateFormat.ShortDatePattern = "dd/MM/yyyy"
                    DateToBDDate = FormatDate(DateValue, DateFormat)
                End If

            Catch ex As Exception

                Return "NULL"

            End Try
        End Function


#Region "FindMimeFromData"
        <System.Runtime.InteropServices.DllImport("urlmon.dll", CharSet:=System.Runtime.InteropServices.CharSet.Unicode, ExactSpelling:=True, SetLastError:=False)> _
        Function FindMimeFromData(ByVal pBC As IntPtr, <System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPWStr)> ByVal pwzUrl As String _
            , <System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPArray, ArraySubType:=System.Runtime.InteropServices.UnmanagedType.I1, SizeParamIndex:=3)> ByVal pBuffer As Byte() _
            , ByVal cbSize As Integer, <System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPWStr)> ByVal pwzMimeProposed As String, ByVal dwMimeFlags As Integer, ByRef ppwzMimeOut As IntPtr _
            , ByVal dwReserved As Integer) As Integer
        End Function

        ''' <summary>
        ''' Obtiene el mime-type
        ''' </summary>
        ''' <param name="file">fichero</param>
        ''' <returns>mime-type</returns>
        ''' <remarks>Llamada desde: attachFile.aspx attachFile.aspx; Tiempo maximo: 0</remarks>
        Public Function getMimeFromFile(ByVal filePost As HttpPostedFile, ByVal Extension As String) As Boolean
            Dim g_MimeTypes As MimeTypes = New MimeTypes(ConfigurationManager.AppSettings("rutaMime") + "\" & "mime-types.txt")

            Dim MaxContent As Integer = DirectCast(filePost.ContentLength, Integer)
            If MaxContent > 4096 Then
                MaxContent = 4096
            End If
            Dim buf As Byte() = New Byte(MaxContent) {}
            filePost.InputStream.Read(buf, 0, MaxContent)

            Dim fileData As SByte() = Winista.Mime.SupportUtil.ToSByteArray(buf)

            Dim oMimeType As MimeType = g_MimeTypes.GetMimeType(fileData)

            If (oMimeType Is Nothing) OrElse (g_MimeTypes.GetMimeType(LCase(Extension)) Is Nothing) Then
                'Return InStr(getMimeFromFileUrlMon(filePost, LCase(Extension)), LCase(Extension) & "#") > 0
                Return True
            Else
                Return g_MimeTypes.GetMimeType(LCase(Extension)).ToString = oMimeType.ToString
            End If
        End Function

#Region "UrlMon"
        ''' <summary>
        ''' Obtiene el mime-type
        ''' </summary>
        ''' <param name="file">fichero</param>
        ''' <returns>mime-type</returns>
        ''' <remarks>Llamada desde: attachFile.aspx attachFile.aspx; Tiempo maximo: 0</remarks>
        Private Function getMimeFromFileUrlMon(ByVal file As HttpPostedFile, ByVal Extension As String) As String
            Dim mimeout As IntPtr

            Dim MaxContent As Integer = DirectCast(file.ContentLength, Integer)
            If MaxContent > 4096 Then
                MaxContent = 4096
            End If

            Dim buf As Byte() = New Byte(MaxContent) {}
            file.InputStream.Read(buf, 0, MaxContent)
            Dim result As Integer = FindMimeFromData(IntPtr.Zero, file.FileName, buf, MaxContent, Nothing, 0, mimeout, 0)

            Dim mime As String
            If result <> 0 Then
                mime = ""
            Else
                mime = System.Runtime.InteropServices.Marshal.PtrToStringUni(mimeout)
            End If
            System.Runtime.InteropServices.Marshal.FreeCoTaskMem(mimeout)

            Return DameExtensionSegunMime(mime.ToLower(), Extension)
        End Function

        ''' <summary>
        '''  Obtiene Lista de extensiones para el mimetype
        ''' </summary>
        ''' <param name="mimeType">mimeType</param>
        ''' <param name="Extension">Extension</param>
        ''' <returns>Lista de extensiones para el mimetype</returns>
        ''' <remarks>Llamada desde: attachFile.aspx attachFile.aspx; Tiempo maximo: 0</remarks>
        Private Function DameExtensionSegunMime(ByVal mimeType As String, ByVal Extension As String) As String
            Dim Resul As String = ""

            If mimeType = "application/acad" Then Resul = Resul & ".dwg#"
            If mimeType = "application/arj" Then Resul = Resul & ".arj#"
            If mimeType = "application/base64" Then Resul = Resul & ".mm#"
            If mimeType = "application/base64" Then Resul = Resul & ".mme#"
            If mimeType = "application/binhex" Then Resul = Resul & ".hqx#"
            If mimeType = "application/binhex4" Then Resul = Resul & ".hqx#"
            If mimeType = "application/book" Then Resul = Resul & ".boo#"
            If mimeType = "application/book" Then Resul = Resul & ".book#"
            If mimeType = "application/cdf" Then Resul = Resul & ".cdf#"
            If mimeType = "application/clariscad" Then Resul = Resul & ".ccad#"
            If mimeType = "application/commonground" Then Resul = Resul & ".dp#"
            If mimeType = "application/drafting" Then Resul = Resul & ".drw#"
            If mimeType = "application/dsptype" Then Resul = Resul & ".tsp#"
            If mimeType = "application/dxf" Then Resul = Resul & ".dxf#"
            If mimeType = "application/ecmascript" Then Resul = Resul & ".js#"
            If mimeType = "application/envoy" Then Resul = Resul & ".evy#"
            If mimeType = "application/excel" Then Resul = Resul & ".xl#"
            If mimeType = "application/excel" Then Resul = Resul & ".xla#"
            If mimeType = "application/excel" Then Resul = Resul & ".xlb#"
            If mimeType = "application/excel" Then Resul = Resul & ".xlc#"
            If mimeType = "application/excel" Then Resul = Resul & ".xld#"
            If mimeType = "application/excel" Then Resul = Resul & ".xlk#"
            If mimeType = "application/excel" Then Resul = Resul & ".xll#"
            If mimeType = "application/excel" Then Resul = Resul & ".xlm#"
            If mimeType = "application/excel" Then Resul = Resul & ".xls#"
            If mimeType = "application/excel" Then Resul = Resul & ".xlt#"
            If mimeType = "application/excel" Then Resul = Resul & ".xlv#"
            If mimeType = "application/excel" Then Resul = Resul & ".xlw#"
            If mimeType = "application/fractals" Then Resul = Resul & ".fif#"
            If mimeType = "application/freeloader" Then Resul = Resul & ".frl#"
            If mimeType = "application/futuresplash" Then Resul = Resul & ".spl#"
            If mimeType = "application/gnutar" Then Resul = Resul & ".tgz#"
            If mimeType = "application/groupwise" Then Resul = Resul & ".vew#"
            If mimeType = "application/hlp" Then Resul = Resul & ".hlp#"
            If mimeType = "application/hta" Then Resul = Resul & ".hta#"
            If mimeType = "application/i-deas" Then Resul = Resul & ".unv#"
            If mimeType = "application/iges" Then Resul = Resul & ".iges#"
            If mimeType = "application/iges" Then Resul = Resul & ".igs#"
            If mimeType = "application/inf" Then Resul = Resul & ".inf#"
            If mimeType = "application/java" Then Resul = Resul & ".class#"
            If mimeType = "application/java-byte-code" Then Resul = Resul & ".class#"
            If mimeType = "application/javascript" Then Resul = Resul & ".js#"
            If mimeType = "application/lha" Then Resul = Resul & ".lha#"
            If mimeType = "application/lzx" Then Resul = Resul & ".lzx#"
            If mimeType = "application/macbinary" Then Resul = Resul & ".bin#"
            If mimeType = "application/mac-binary" Then Resul = Resul & ".bin#"
            If mimeType = "application/mac-binhex" Then Resul = Resul & ".hqx#"
            If mimeType = "application/mac-binhex40" Then Resul = Resul & ".hqx#"
            If mimeType = "application/mac-compactpro" Then Resul = Resul & ".cpt#"
            If mimeType = "application/marc" Then Resul = Resul & ".mrc#"
            If mimeType = "application/mbedlet" Then Resul = Resul & ".mbd#"
            If mimeType = "application/mcad" Then Resul = Resul & ".mcd#"
            If mimeType = "application/mime" Then Resul = Resul & ".aps#"
            If mimeType = "application/mspowerpoint" Then Resul = Resul & ".pot#"
            If mimeType = "application/mspowerpoint" Then Resul = Resul & ".pps#"
            If mimeType = "application/mspowerpoint" Then Resul = Resul & ".ppt#"
            If mimeType = "application/mspowerpoint" Then Resul = Resul & ".ppz#"
            If mimeType = "application/msword" Then Resul = Resul & ".doc#"
            If mimeType = "application/msword" Then Resul = Resul & ".dot#"
            If mimeType = "application/msword" Then Resul = Resul & ".w6w#"
            If mimeType = "application/msword" Then Resul = Resul & ".wiz#"
            If mimeType = "application/msword" Then Resul = Resul & ".word#"
            If mimeType = "application/mswrite" Then Resul = Resul & ".wri#"
            If mimeType = "application/netmc" Then Resul = Resul & ".mcp#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".a#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".arc#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".arj#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".bin#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".com#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".dump#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".exe#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".lha#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".lhx#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".lzh#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".lzx#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".o#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".psd#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".saveme#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".uu#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".zoo#"
            If mimeType = "application/oda" Then Resul = Resul & ".oda#"
            If mimeType = "application/pdf" Then Resul = Resul & ".pdf#"
            If mimeType = "application/pkcs10" Then Resul = Resul & ".p10#"
            If mimeType = "application/pkcs-12" Then Resul = Resul & ".p12#"
            If mimeType = "application/pkcs7-mime" Then Resul = Resul & ".p7c#"
            If mimeType = "application/pkcs7-mime" Then Resul = Resul & ".p7m#"
            If mimeType = "application/pkcs7-signature" Then Resul = Resul & ".p7s#"
            If mimeType = "application/pkcs-crl" Then Resul = Resul & ".crl#"
            If mimeType = "application/pkix-cert" Then Resul = Resul & ".cer#"
            If mimeType = "application/pkix-cert" Then Resul = Resul & ".crt#"
            If mimeType = "application/pkix-crl" Then Resul = Resul & ".crl#"
            If mimeType = "application/plain" Then Resul = Resul & ".text#"
            If mimeType = "application/postscript" Then Resul = Resul & ".ai#"
            If mimeType = "application/postscript" Then Resul = Resul & ".eps#"
            If mimeType = "application/postscript" Then Resul = Resul & ".ps#"
            If mimeType = "application/powerpoint" Then Resul = Resul & ".ppt#"
            If mimeType = "application/pro_eng" Then Resul = Resul & ".part#"
            If mimeType = "application/pro_eng" Then Resul = Resul & ".prt#"
            If mimeType = "application/ringing-tones" Then Resul = Resul & ".rng#"
            If mimeType = "application/rtf" Then Resul = Resul & ".rtf#"
            If mimeType = "application/rtf" Then Resul = Resul & ".rtx#"
            If mimeType = "application/sdp" Then Resul = Resul & ".sdp#"
            If mimeType = "application/sea" Then Resul = Resul & ".sea#"
            If mimeType = "application/set" Then Resul = Resul & ".set#"
            If mimeType = "application/sla" Then Resul = Resul & ".stl#"
            If mimeType = "application/smil" Then Resul = Resul & ".smi#"
            If mimeType = "application/smil" Then Resul = Resul & ".smil#"
            If mimeType = "application/solids" Then Resul = Resul & ".sol#"
            If mimeType = "application/sounder" Then Resul = Resul & ".sdr#"
            If mimeType = "application/step" Then Resul = Resul & ".step#"
            If mimeType = "application/step" Then Resul = Resul & ".stp#"
            If mimeType = "application/streamingmedia" Then Resul = Resul & ".ssm#"
            If mimeType = "application/toolbook" Then Resul = Resul & ".tbk#"
            If mimeType = "application/vda" Then Resul = Resul & ".vda#"
            If mimeType = "application/vnd.fdf" Then Resul = Resul & ".fdf#"
            If mimeType = "application/vnd.hp-hpgl" Then Resul = Resul & ".hgl#"
            If mimeType = "application/vnd.hp-hpgl" Then Resul = Resul & ".hpg#"
            If mimeType = "application/vnd.hp-hpgl" Then Resul = Resul & ".hpgl#"
            If mimeType = "application/vnd.hp-pcl" Then Resul = Resul & ".pcl#"
            If mimeType = "application/vnd.ms-excel" Then Resul = Resul & ".xlb#"
            If mimeType = "application/vnd.ms-excel" Then Resul = Resul & ".xlc#"
            If mimeType = "application/vnd.ms-excel" Then Resul = Resul & ".xll#"
            If mimeType = "application/vnd.ms-excel" Then Resul = Resul & ".xlm#"
            If mimeType = "application/vnd.ms-excel" Then Resul = Resul & ".xls#"
            If mimeType = "application/vnd.ms-excel" Then Resul = Resul & ".xlw#"
            If mimeType = "application/vnd.ms-pki.certstore" Then Resul = Resul & ".sst#"
            If mimeType = "application/vnd.ms-pki.pko" Then Resul = Resul & ".pko#"
            If mimeType = "application/vnd.ms-pki.seccat" Then Resul = Resul & ".cat#"
            If mimeType = "application/vnd.ms-pki.stl" Then Resul = Resul & ".stl#"
            If mimeType = "application/vnd.ms-powerpoint" Then Resul = Resul & ".pot#"
            If mimeType = "application/vnd.ms-powerpoint" Then Resul = Resul & ".ppa#"
            If mimeType = "application/vnd.ms-powerpoint" Then Resul = Resul & ".pps#"
            If mimeType = "application/vnd.ms-powerpoint" Then Resul = Resul & ".ppt#"
            If mimeType = "application/vnd.ms-powerpoint" Then Resul = Resul & ".pwz#"
            If mimeType = "application/vnd.ms-project" Then Resul = Resul & ".mpp#"
            If mimeType = "application/vnd.nokia.configuration-message" Then Resul = Resul & ".ncm#"
            If mimeType = "application/vnd.nokia.ringing-tone" Then Resul = Resul & ".rng#"
            If mimeType = "application/vnd.rn-realmedia" Then Resul = Resul & ".rm#"
            If mimeType = "application/vnd.rn-realplayer" Then Resul = Resul & ".rnx#"
            If mimeType = "application/vnd.wap.wmlc" Then Resul = Resul & ".wmlc#"
            If mimeType = "application/vnd.wap.wmlscriptc" Then Resul = Resul & ".wmlsc#"
            If mimeType = "application/vnd.xara" Then Resul = Resul & ".web#"
            If mimeType = "application/vocaltec-media-desc" Then Resul = Resul & ".vmd#"
            If mimeType = "application/vocaltec-media-file" Then Resul = Resul & ".vmf#"
            If mimeType = "application/wordperfect" Then Resul = Resul & ".wp#"
            If mimeType = "application/wordperfect" Then Resul = Resul & ".wp5#"
            If mimeType = "application/wordperfect" Then Resul = Resul & ".wp6#"
            If mimeType = "application/wordperfect" Then Resul = Resul & ".wpd#"
            If mimeType = "application/wordperfect6.0" Then Resul = Resul & ".w60#"
            If mimeType = "application/wordperfect6.0" Then Resul = Resul & ".wp5#"
            If mimeType = "application/wordperfect6.1" Then Resul = Resul & ".w61#"
            If mimeType = "application/x-123" Then Resul = Resul & ".wk1#"
            If mimeType = "application/x-aim" Then Resul = Resul & ".aim#"
            If mimeType = "application/x-authorware-bin" Then Resul = Resul & ".aab#"
            If mimeType = "application/x-authorware-map" Then Resul = Resul & ".aam#"
            If mimeType = "application/x-authorware-seg" Then Resul = Resul & ".aas#"
            If mimeType = "application/x-bcpio" Then Resul = Resul & ".bcpio#"
            If mimeType = "application/x-binary" Then Resul = Resul & ".bin#"
            If mimeType = "application/x-binhex40" Then Resul = Resul & ".hqx#"
            If mimeType = "application/x-bsh" Then Resul = Resul & ".bsh#"
            If mimeType = "application/x-bsh" Then Resul = Resul & ".sh#"
            If mimeType = "application/x-bsh" Then Resul = Resul & ".shar#"
            If mimeType = "application/x-bytecode.elisp (compiled elisp)" Then Resul = Resul & ".elc#"
            If mimeType = "application/x-bytecode.python" Then Resul = Resul & ".pyc#"
            If mimeType = "application/x-bzip" Then Resul = Resul & ".bz#"
            If mimeType = "application/x-bzip2" Then Resul = Resul & ".boz#"
            If mimeType = "application/x-bzip2" Then Resul = Resul & ".bz2#"
            If mimeType = "application/x-cdf" Then Resul = Resul & ".cdf#"
            If mimeType = "application/x-cdlink" Then Resul = Resul & ".vcd#"
            If mimeType = "application/x-chat" Then Resul = Resul & ".cha#"
            If mimeType = "application/x-chat" Then Resul = Resul & ".chat#"
            If mimeType = "application/x-cmu-raster" Then Resul = Resul & ".ras#"
            If mimeType = "application/x-cocoa" Then Resul = Resul & ".cco#"
            If mimeType = "application/x-compactpro" Then Resul = Resul & ".cpt#"
            If mimeType = "application/x-compress" Then Resul = Resul & ".z#"
            If mimeType = "application/x-compressed" Then Resul = Resul & ".gz#"
            If mimeType = "application/x-compressed" Then Resul = Resul & ".tgz#"
            If mimeType = "application/x-compressed" Then Resul = Resul & ".z#"
            If mimeType = "application/x-compressed" Then Resul = Resul & ".zip#"
            If mimeType = "application/x-conference" Then Resul = Resul & ".nsc#"
            If mimeType = "application/x-cpio" Then Resul = Resul & ".cpio#"
            If mimeType = "application/x-cpt" Then Resul = Resul & ".cpt#"
            If mimeType = "application/x-csh" Then Resul = Resul & ".csh#"
            If mimeType = "application/x-deepv" Then Resul = Resul & ".deepv#"
            If mimeType = "application/x-director" Then Resul = Resul & ".dcr#"
            If mimeType = "application/x-director" Then Resul = Resul & ".dir#"
            If mimeType = "application/x-director" Then Resul = Resul & ".dxr#"
            If mimeType = "application/x-dvi" Then Resul = Resul & ".dvi#"
            If mimeType = "application/x-elc" Then Resul = Resul & ".elc#"
            If mimeType = "application/x-envoy" Then Resul = Resul & ".env#"
            If mimeType = "application/x-envoy" Then Resul = Resul & ".evy#"
            If mimeType = "application/x-esrehber" Then Resul = Resul & ".es#"
            If mimeType = "application/x-excel" Then Resul = Resul & ".xla#"
            If mimeType = "application/x-excel" Then Resul = Resul & ".xlb#"
            If mimeType = "application/x-excel" Then Resul = Resul & ".xlc#"
            If mimeType = "application/x-excel" Then Resul = Resul & ".xld#"
            If mimeType = "application/x-excel" Then Resul = Resul & ".xlk#"
            If mimeType = "application/x-excel" Then Resul = Resul & ".xll#"
            If mimeType = "application/x-excel" Then Resul = Resul & ".xlm#"
            If mimeType = "application/x-excel" Then Resul = Resul & ".xls#"
            If mimeType = "application/x-excel" Then Resul = Resul & ".xlt#"
            If mimeType = "application/x-excel" Then Resul = Resul & ".xlv#"
            If mimeType = "application/x-excel" Then Resul = Resul & ".xlw#"
            If mimeType = "application/x-frame" Then Resul = Resul & ".mif#"
            If mimeType = "application/x-freelance" Then Resul = Resul & ".pre#"
            If mimeType = "application/x-gsp" Then Resul = Resul & ".gsp#"
            If mimeType = "application/x-gss" Then Resul = Resul & ".gss#"
            If mimeType = "application/x-gtar" Then Resul = Resul & ".gtar#"
            If mimeType = "application/x-gzip" Then Resul = Resul & ".gz#"
            If mimeType = "application/x-gzip" Then Resul = Resul & ".gzip#"
            If mimeType = "application/x-hdf" Then Resul = Resul & ".hdf#"
            If mimeType = "application/x-helpfile" Then Resul = Resul & ".help#"
            If mimeType = "application/x-helpfile" Then Resul = Resul & ".hlp#"
            If mimeType = "application/x-httpd-imap" Then Resul = Resul & ".imap#"
            If mimeType = "application/x-ima" Then Resul = Resul & ".ima#"
            If mimeType = "application/x-internett-signup" Then Resul = Resul & ".ins#"
            If mimeType = "application/x-inventor" Then Resul = Resul & ".iv#"
            If mimeType = "application/x-ip2" Then Resul = Resul & ".ip#"
            If mimeType = "application/x-java-class" Then Resul = Resul & ".class#"
            If mimeType = "application/x-java-commerce" Then Resul = Resul & ".jcm#"
            If mimeType = "application/x-javascript" Then Resul = Resul & ".js#"
            If mimeType = "application/x-koan" Then Resul = Resul & ".skd#"
            If mimeType = "application/x-koan" Then Resul = Resul & ".skm#"
            If mimeType = "application/x-koan" Then Resul = Resul & ".skp#"
            If mimeType = "application/x-koan" Then Resul = Resul & ".skt#"
            If mimeType = "application/x-ksh" Then Resul = Resul & ".ksh#"
            If mimeType = "application/x-latex" Then Resul = Resul & ".latex#"
            If mimeType = "application/x-latex" Then Resul = Resul & ".ltx#"
            If mimeType = "application/x-lha" Then Resul = Resul & ".lha#"
            If mimeType = "application/x-lisp" Then Resul = Resul & ".lsp#"
            If mimeType = "application/x-livescreen" Then Resul = Resul & ".ivy#"
            If mimeType = "application/x-lotus" Then Resul = Resul & ".wq1#"
            If mimeType = "application/x-lotusscreencam" Then Resul = Resul & ".scm#"
            If mimeType = "application/x-lzh" Then Resul = Resul & ".lzh#"
            If mimeType = "application/x-lzx" Then Resul = Resul & ".lzx#"
            If mimeType = "application/x-macbinary" Then Resul = Resul & ".bin#"
            If mimeType = "application/x-mac-binhex40" Then Resul = Resul & ".hqx#"
            If mimeType = "application/x-magic-cap-package-1.0" Then Resul = Resul & ".mc$#"
            If mimeType = "application/x-mathcad" Then Resul = Resul & ".mcd#"
            If mimeType = "application/x-meme" Then Resul = Resul & ".mm#"
            If mimeType = "application/x-midi" Then Resul = Resul & ".mid#"
            If mimeType = "application/x-midi" Then Resul = Resul & ".midi#"
            If mimeType = "application/x-mif" Then Resul = Resul & ".mif#"
            If mimeType = "application/x-mix-transfer" Then Resul = Resul & ".nix#"
            If mimeType = "application/xml" Then Resul = Resul & ".xml#"
            If mimeType = "application/x-mplayer2" Then Resul = Resul & ".asx#"
            If mimeType = "application/x-msexcel" Then Resul = Resul & ".xla#"
            If mimeType = "application/x-msexcel" Then Resul = Resul & ".xls#"
            If mimeType = "application/x-msexcel" Then Resul = Resul & ".xlw#"
            If mimeType = "application/x-mspowerpoint" Then Resul = Resul & ".ppt#"
            If mimeType = "application/x-navi-animation" Then Resul = Resul & ".ani#"
            If mimeType = "application/x-navidoc" Then Resul = Resul & ".nvd#"
            If mimeType = "application/x-navimap" Then Resul = Resul & ".map#"
            If mimeType = "application/x-navistyle" Then Resul = Resul & ".stl#"
            If mimeType = "application/x-netcdf" Then Resul = Resul & ".cdf#"
            If mimeType = "application/x-netcdf" Then Resul = Resul & ".nc#"
            If mimeType = "application/x-newton-compatible-pkg" Then Resul = Resul & ".pkg#"
            If mimeType = "application/x-nokia-9000-communicator-add-on-software" Then Resul = Resul & ".aos#"
            If mimeType = "application/x-omc" Then Resul = Resul & ".omc#"
            If mimeType = "application/x-omcdatamaker" Then Resul = Resul & ".omcd#"
            If mimeType = "application/x-omcregerator" Then Resul = Resul & ".omcr#"
            If mimeType = "application/x-pagemaker" Then Resul = Resul & ".pm4#"
            If mimeType = "application/x-pagemaker" Then Resul = Resul & ".pm5#"
            If mimeType = "application/x-pcl" Then Resul = Resul & ".pcl#"
            If mimeType = "application/x-pixclscript" Then Resul = Resul & ".plx#"
            If mimeType = "application/x-pkcs10" Then Resul = Resul & ".p10#"
            If mimeType = "application/x-pkcs12" Then Resul = Resul & ".p12#"
            If mimeType = "application/x-pkcs7-certificates" Then Resul = Resul & ".spc#"
            If mimeType = "application/x-pkcs7-certreqresp" Then Resul = Resul & ".p7r#"
            If mimeType = "application/x-pkcs7-mime" Then Resul = Resul & ".p7c#"
            If mimeType = "application/x-pkcs7-mime" Then Resul = Resul & ".p7m#"
            If mimeType = "application/x-pkcs7-signature" Then Resul = Resul & ".p7a#"
            If mimeType = "application/x-pointplus" Then Resul = Resul & ".css#"
            If mimeType = "application/x-portable-anymap" Then Resul = Resul & ".pnm#"
            If mimeType = "application/x-project" Then Resul = Resul & ".mpc#"
            If mimeType = "application/x-project" Then Resul = Resul & ".mpt#"
            If mimeType = "application/x-project" Then Resul = Resul & ".mpv#"
            If mimeType = "application/x-project" Then Resul = Resul & ".mpx#"
            If mimeType = "application/x-qpro" Then Resul = Resul & ".wb1#"
            If mimeType = "application/x-rtf" Then Resul = Resul & ".rtf#"
            If mimeType = "application/x-sdp" Then Resul = Resul & ".sdp#"
            If mimeType = "application/x-sea" Then Resul = Resul & ".sea#"
            If mimeType = "application/x-seelogo" Then Resul = Resul & ".sl#"
            If mimeType = "application/x-sh" Then Resul = Resul & ".sh#"
            If mimeType = "application/x-shar" Then Resul = Resul & ".sh#"
            If mimeType = "application/x-shar" Then Resul = Resul & ".shar#"
            If mimeType = "application/x-shockwave-flash" Then Resul = Resul & ".swf#"
            If mimeType = "application/x-sit" Then Resul = Resul & ".sit#"
            If mimeType = "application/x-sprite" Then Resul = Resul & ".spr#"
            If mimeType = "application/x-sprite" Then Resul = Resul & ".sprite#"
            If mimeType = "application/x-stuffit" Then Resul = Resul & ".sit#"
            If mimeType = "application/x-sv4cpio" Then Resul = Resul & ".sv4cpio#"
            If mimeType = "application/x-sv4crc" Then Resul = Resul & ".sv4crc#"
            If mimeType = "application/x-tar" Then Resul = Resul & ".tar#"
            If mimeType = "application/x-tbook" Then Resul = Resul & ".sbk#"
            If mimeType = "application/x-tbook" Then Resul = Resul & ".tbk#"
            If mimeType = "application/x-tcl" Then Resul = Resul & ".tcl#"
            If mimeType = "application/x-tex" Then Resul = Resul & ".tex#"
            If mimeType = "application/x-texinfo" Then Resul = Resul & ".texi#"
            If mimeType = "application/x-texinfo" Then Resul = Resul & ".texinfo#"
            If mimeType = "application/x-troff" Then Resul = Resul & ".roff#"
            If mimeType = "application/x-troff" Then Resul = Resul & ".t#"
            If mimeType = "application/x-troff" Then Resul = Resul & ".tr#"
            If mimeType = "application/x-troff-man" Then Resul = Resul & ".man#"
            If mimeType = "application/x-troff-me" Then Resul = Resul & ".me#"
            If mimeType = "application/x-troff-ms" Then Resul = Resul & ".ms#"
            If mimeType = "application/x-troff-msvideo" Then Resul = Resul & ".avi#"
            If mimeType = "application/x-ustar" Then Resul = Resul & ".ustar#"
            If mimeType = "application/x-visio" Then Resul = Resul & ".vsd#"
            If mimeType = "application/x-visio" Then Resul = Resul & ".vst#"
            If mimeType = "application/x-visio" Then Resul = Resul & ".vsw#"
            If mimeType = "application/x-vnd.audioexplosion.mzz" Then Resul = Resul & ".mzz#"
            If mimeType = "application/x-vnd.ls-xpix" Then Resul = Resul & ".xpix#"
            If mimeType = "application/x-vrml" Then Resul = Resul & ".vrml#"
            If mimeType = "application/x-wais-source" Then Resul = Resul & ".src#"
            If mimeType = "application/x-wais-source" Then Resul = Resul & ".wsrc#"
            If mimeType = "application/x-winhelp" Then Resul = Resul & ".hlp#"
            If mimeType = "application/x-wintalk" Then Resul = Resul & ".wtk#"
            If mimeType = "application/x-world" Then Resul = Resul & ".svr#"
            If mimeType = "application/x-world" Then Resul = Resul & ".wrl#"
            If mimeType = "application/x-wpwin" Then Resul = Resul & ".wpd#"
            If mimeType = "application/x-wri" Then Resul = Resul & ".wri#"
            If mimeType = "application/x-x509-ca-cert" Then Resul = Resul & ".cer#"
            If mimeType = "application/x-x509-ca-cert" Then Resul = Resul & ".crt#"
            If mimeType = "application/x-x509-ca-cert" Then Resul = Resul & ".der#"
            If mimeType = "application/x-x509-user-cert" Then Resul = Resul & ".crt#"
            If mimeType = "application/x-zip-compressed" Then Resul = Resul & ".zip#"
            If mimeType = "application/zip" Then Resul = Resul & ".zip#"
            If mimeType = "audio/aiff" Then Resul = Resul & ".aif#"
            If mimeType = "audio/aiff" Then Resul = Resul & ".aifc#"
            If mimeType = "audio/aiff" Then Resul = Resul & ".aiff#"
            If mimeType = "audio/basic" Then Resul = Resul & ".au#"
            If mimeType = "audio/basic" Then Resul = Resul & ".snd#"
            If mimeType = "audio/it" Then Resul = Resul & ".it#"
            If mimeType = "audio/make" Then Resul = Resul & ".funk#"
            If mimeType = "audio/make" Then Resul = Resul & ".my#"
            If mimeType = "audio/make" Then Resul = Resul & ".pfunk#"
            If mimeType = "audio/make.my.funk" Then Resul = Resul & ".pfunk#"
            If mimeType = "audio/mid" Then Resul = Resul & ".rmi#"
            If mimeType = "audio/midi" Then Resul = Resul & ".kar#"
            If mimeType = "audio/midi" Then Resul = Resul & ".mid#"
            If mimeType = "audio/midi" Then Resul = Resul & ".midi#"
            If mimeType = "audio/mod" Then Resul = Resul & ".mod#"
            If mimeType = "audio/mpeg" Then Resul = Resul & ".m2a#"
            If mimeType = "audio/mpeg" Then Resul = Resul & ".mp2#"
            If mimeType = "audio/mpeg" Then Resul = Resul & ".mpa#"
            If mimeType = "audio/mpeg" Then Resul = Resul & ".mpg#"
            If mimeType = "audio/mpeg" Then Resul = Resul & ".mpga#"
            If mimeType = "audio/mpeg3" Then Resul = Resul & ".mp3#"
            If mimeType = "audio/nspaudio" Then Resul = Resul & ".la#"
            If mimeType = "audio/nspaudio" Then Resul = Resul & ".lma#"
            If mimeType = "audio/s3m" Then Resul = Resul & ".s3m#"
            If mimeType = "audio/tsp-audio" Then Resul = Resul & ".tsi#"
            If mimeType = "audio/tsplayer" Then Resul = Resul & ".tsp#"
            If mimeType = "audio/vnd.qcelp" Then Resul = Resul & ".qcp#"
            If mimeType = "audio/voc" Then Resul = Resul & ".voc#"
            If mimeType = "audio/voxware" Then Resul = Resul & ".vox#"
            If mimeType = "audio/wav" Then Resul = Resul & ".wav#"
            If mimeType = "audio/x-adpcm" Then Resul = Resul & ".snd#"
            If mimeType = "audio/x-aiff" Then Resul = Resul & ".aif#"
            If mimeType = "audio/x-aiff" Then Resul = Resul & ".aifc#"
            If mimeType = "audio/x-aiff" Then Resul = Resul & ".aiff#"
            If mimeType = "audio/x-au" Then Resul = Resul & ".au#"
            If mimeType = "audio/x-gsm" Then Resul = Resul & ".gsd#"
            If mimeType = "audio/x-gsm" Then Resul = Resul & ".gsm#"
            If mimeType = "audio/x-jam" Then Resul = Resul & ".jam#"
            If mimeType = "audio/x-liveaudio" Then Resul = Resul & ".lam#"
            If mimeType = "audio/xm" Then Resul = Resul & ".xm#"
            If mimeType = "audio/x-mid" Then Resul = Resul & ".mid#"
            If mimeType = "audio/x-mid" Then Resul = Resul & ".midi#"
            If mimeType = "audio/x-midi" Then Resul = Resul & ".mid#"
            If mimeType = "audio/x-midi" Then Resul = Resul & ".midi#"
            If mimeType = "audio/x-mod" Then Resul = Resul & ".mod#"
            If mimeType = "audio/x-mpeg" Then Resul = Resul & ".mp2#"
            If mimeType = "audio/x-mpeg-3" Then Resul = Resul & ".mp3#"
            If mimeType = "audio/x-mpequrl" Then Resul = Resul & ".m3u#"
            If mimeType = "audio/x-nspaudio" Then Resul = Resul & ".la#"
            If mimeType = "audio/x-nspaudio" Then Resul = Resul & ".lma#"
            If mimeType = "audio/x-pn-realaudio" Then Resul = Resul & ".ra#"
            If mimeType = "audio/x-pn-realaudio" Then Resul = Resul & ".ram#"
            If mimeType = "audio/x-pn-realaudio" Then Resul = Resul & ".rm#"
            If mimeType = "audio/x-pn-realaudio" Then Resul = Resul & ".rmm#"
            If mimeType = "audio/x-pn-realaudio" Then Resul = Resul & ".rmp#"
            If mimeType = "audio/x-pn-realaudio-plugin" Then Resul = Resul & ".ra#"
            If mimeType = "audio/x-pn-realaudio-plugin" Then Resul = Resul & ".rmp#"
            If mimeType = "audio/x-pn-realaudio-plugin" Then Resul = Resul & ".rpm#"
            If mimeType = "audio/x-psid" Then Resul = Resul & ".sid#"
            If mimeType = "audio/x-realaudio" Then Resul = Resul & ".ra#"
            If mimeType = "audio/x-twinvq" Then Resul = Resul & ".vqf#"
            If mimeType = "audio/x-twinvq-plugin" Then Resul = Resul & ".vqe#"
            If mimeType = "audio/x-twinvq-plugin" Then Resul = Resul & ".vql#"
            If mimeType = "audio/x-vnd.audioexplosion.mjuicemediafile" Then Resul = Resul & ".mjf#"
            If mimeType = "audio/x-voc" Then Resul = Resul & ".voc#"
            If mimeType = "audio/x-wav" Then Resul = Resul & ".wav#"
            If mimeType = "chemical/x-pdb" Then Resul = Resul & ".pdb#"
            If mimeType = "chemical/x-pdb" Then Resul = Resul & ".xyz#"
            If mimeType = "drawing/x-dwf (old)" Then Resul = Resul & ".dwf#"
            If mimeType = "image/bmp" Then Resul = Resul & ".bm#"
            If mimeType = "image/bmp" Then Resul = Resul & ".bmp#"
            If mimeType = "image/cmu-raster" Then Resul = Resul & ".ras#"
            If mimeType = "image/cmu-raster" Then Resul = Resul & ".rast#"
            If mimeType = "image/fif" Then Resul = Resul & ".fif#"
            If mimeType = "image/florian" Then Resul = Resul & ".flo#"
            If mimeType = "image/florian" Then Resul = Resul & ".turbot#"
            If mimeType = "image/g3fax" Then Resul = Resul & ".g3#"
            If mimeType = "image/gif" Then Resul = Resul & ".gif#"
            If mimeType = "image/ief" Then Resul = Resul & ".ief#"
            If mimeType = "image/ief" Then Resul = Resul & ".iefs#"
            If mimeType = "image/jpeg" Then Resul = Resul & ".jfif#"
            If mimeType = "image/jpeg" Then Resul = Resul & ".jfif-tbnl#"
            If mimeType = "image/jpeg" Then Resul = Resul & ".jpe#"
            If mimeType = "image/jpeg" Then Resul = Resul & ".jpeg#"
            If mimeType = "image/jpeg" Then Resul = Resul & ".jpg#"
            If mimeType = "image/jutvision" Then Resul = Resul & ".jut#"
            If mimeType = "image/naplps" Then Resul = Resul & ".nap#"
            If mimeType = "image/naplps" Then Resul = Resul & ".naplps#"
            If mimeType = "image/pict" Then Resul = Resul & ".pic#"
            If mimeType = "image/pict" Then Resul = Resul & ".pict#"
            If mimeType = "image/pjpeg" Then Resul = Resul & ".jfif#"
            If mimeType = "image/pjpeg" Then Resul = Resul & ".jpe#"
            If mimeType = "image/pjpeg" Then Resul = Resul & ".jpeg#"
            If mimeType = "image/pjpeg" Then Resul = Resul & ".jpg#"
            If mimeType = "image/png" Then Resul = Resul & ".png#"
            If mimeType = "image/x-png" Then Resul = Resul & ".png#"
            If mimeType = "image/tiff" Then Resul = Resul & ".tif#"
            If mimeType = "image/tiff" Then Resul = Resul & ".tiff#"
            If mimeType = "image/vasa" Then Resul = Resul & ".mcf#"
            If mimeType = "image/vnd.dwg" Then Resul = Resul & ".dwg#"
            If mimeType = "image/vnd.dwg" Then Resul = Resul & ".dxf#"
            If mimeType = "image/vnd.dwg" Then Resul = Resul & ".svf#"
            If mimeType = "image/vnd.fpx" Then Resul = Resul & ".fpx#"
            If mimeType = "image/vnd.net-fpx" Then Resul = Resul & ".fpx#"
            If mimeType = "image/vnd.rn-realflash" Then Resul = Resul & ".rf#"
            If mimeType = "image/vnd.rn-realpix" Then Resul = Resul & ".rp#"
            If mimeType = "image/vnd.wap.wbmp" Then Resul = Resul & ".wbmp#"
            If mimeType = "image/vnd.xiff" Then Resul = Resul & ".xif#"
            If mimeType = "image/xbm" Then Resul = Resul & ".xbm#"
            If mimeType = "image/x-cmu-raster" Then Resul = Resul & ".ras#"
            If mimeType = "image/x-dwg" Then Resul = Resul & ".dwg#"
            If mimeType = "image/x-dwg" Then Resul = Resul & ".dxf#"
            If mimeType = "image/x-dwg" Then Resul = Resul & ".svf#"
            If mimeType = "image/x-icon" Then Resul = Resul & ".ico#"
            If mimeType = "image/x-jg" Then Resul = Resul & ".art#"
            If mimeType = "image/x-jps" Then Resul = Resul & ".jps#"
            If mimeType = "image/x-niff" Then Resul = Resul & ".nif#"
            If mimeType = "image/x-niff" Then Resul = Resul & ".niff#"
            If mimeType = "image/x-pcx" Then Resul = Resul & ".pcx#"
            If mimeType = "image/x-pict" Then Resul = Resul & ".pct#"
            If mimeType = "image/xpm" Then Resul = Resul & ".xpm#"
            If mimeType = "image/x-portable-anymap" Then Resul = Resul & ".pnm#"
            If mimeType = "image/x-portable-bitmap" Then Resul = Resul & ".pbm#"
            If mimeType = "image/x-portable-graymap" Then Resul = Resul & ".pgm#"
            If mimeType = "image/x-portable-greymap" Then Resul = Resul & ".pgm#"
            If mimeType = "image/x-portable-pixmap" Then Resul = Resul & ".ppm#"
            If mimeType = "image/x-quicktime" Then Resul = Resul & ".qif#"
            If mimeType = "image/x-quicktime" Then Resul = Resul & ".qti#"
            If mimeType = "image/x-quicktime" Then Resul = Resul & ".qtif#"
            If mimeType = "image/x-rgb" Then Resul = Resul & ".rgb#"
            If mimeType = "image/x-tiff" Then Resul = Resul & ".tif#"
            If mimeType = "image/x-tiff" Then Resul = Resul & ".tiff#"
            If mimeType = "image/x-windows-bmp" Then Resul = Resul & ".bmp#"
            If mimeType = "image/x-xbitmap" Then Resul = Resul & ".xbm#"
            If mimeType = "image/x-xbm" Then Resul = Resul & ".xbm#"
            If mimeType = "image/x-xpixmap" Then Resul = Resul & ".pm#"
            If mimeType = "image/x-xpixmap" Then Resul = Resul & ".xpm#"
            If mimeType = "image/x-xwd" Then Resul = Resul & ".xwd#"
            If mimeType = "image/x-xwindowdump" Then Resul = Resul & ".xwd#"
            If mimeType = "i-world/i-vrml" Then Resul = Resul & ".ivr#"
            If mimeType = "message/rfc822" Then Resul = Resul & ".mht#"
            If mimeType = "message/rfc822" Then Resul = Resul & ".mhtml#"
            If mimeType = "message/rfc822" Then Resul = Resul & ".mime#"
            If mimeType = "model/iges" Then Resul = Resul & ".iges#"
            If mimeType = "model/iges" Then Resul = Resul & ".igs#"
            If mimeType = "model/vnd.dwf" Then Resul = Resul & ".dwf#"
            If mimeType = "model/vrml" Then Resul = Resul & ".vrml#"
            If mimeType = "model/vrml" Then Resul = Resul & ".wrl#"
            If mimeType = "model/vrml" Then Resul = Resul & ".wrz#"
            If mimeType = "model/x-pov" Then Resul = Resul & ".pov#"
            If mimeType = "multipart/x-gzip" Then Resul = Resul & ".gzip#"
            If mimeType = "multipart/x-ustar" Then Resul = Resul & ".ustar#"
            If mimeType = "multipart/x-zip" Then Resul = Resul & ".zip#"
            If mimeType = "music/crescendo" Then Resul = Resul & ".mid#"
            If mimeType = "music/crescendo" Then Resul = Resul & ".midi#"
            If mimeType = "music/x-karaoke" Then Resul = Resul & ".kar#"
            If mimeType = "paleovu/x-pv" Then Resul = Resul & ".pvu#"
            If mimeType = "text/asp" Then Resul = Resul & ".asp#"
            If mimeType = "text/css" Then Resul = Resul & ".css#"
            If mimeType = "text/ecmascript" Then Resul = Resul & ".js#"
            If mimeType = "text/html" Then Resul = Resul & ".acgi#"
            If mimeType = "text/html" Then Resul = Resul & ".aspx#"
            If mimeType = "text/html" Then Resul = Resul & ".htm#"
            If mimeType = "text/html" Then Resul = Resul & ".html#"
            If mimeType = "text/html" Then Resul = Resul & ".htmls#"
            If mimeType = "text/html" Then Resul = Resul & ".htx#"
            If mimeType = "text/html" Then Resul = Resul & ".shtml#"
            If mimeType = "text/javascript" Then Resul = Resul & ".js#"
            If mimeType = "text/mcf" Then Resul = Resul & ".mcf#"
            If mimeType = "text/pascal" Then Resul = Resul & ".pas#"
            If mimeType = "text/plain" Then Resul = Resul & ".c#"
            If mimeType = "text/plain" Then Resul = Resul & ".c++#"
            If mimeType = "text/plain" Then Resul = Resul & ".cc#"
            If mimeType = "text/plain" Then Resul = Resul & ".com#"
            If mimeType = "text/plain" Then Resul = Resul & ".conf#"
            If mimeType = "text/plain" Then Resul = Resul & ".cxx#"
            If mimeType = "text/plain" Then Resul = Resul & ".def#"
            If mimeType = "text/plain" Then Resul = Resul & ".f#"
            If mimeType = "text/plain" Then Resul = Resul & ".f90#"
            If mimeType = "text/plain" Then Resul = Resul & ".for#"
            If mimeType = "text/plain" Then Resul = Resul & ".g#"
            If mimeType = "text/plain" Then Resul = Resul & ".h#"
            If mimeType = "text/plain" Then Resul = Resul & ".hh#"
            If mimeType = "text/plain" Then Resul = Resul & ".idc#"
            If mimeType = "text/plain" Then Resul = Resul & ".jav#"
            If mimeType = "text/plain" Then Resul = Resul & ".java#"
            If mimeType = "text/plain" Then Resul = Resul & ".list#"
            If mimeType = "text/plain" Then Resul = Resul & ".log#"
            If mimeType = "text/plain" Then Resul = Resul & ".lst#"
            If mimeType = "text/plain" Then Resul = Resul & ".m#"
            If mimeType = "text/plain" Then Resul = Resul & ".mar#"
            If mimeType = "text/plain" Then Resul = Resul & ".pl#"
            If mimeType = "text/plain" Then Resul = Resul & ".sdml#"
            If mimeType = "text/plain" Then Resul = Resul & ".text#"
            If mimeType = "text/plain" Then Resul = Resul & ".txt#"
            If mimeType = "text/plain" Then Resul = Resul & ".vb#"
            If mimeType = "text/richtext" Then Resul = Resul & ".rt#"
            If mimeType = "text/richtext" Then Resul = Resul & ".rtf#"
            If mimeType = "text/richtext" Then Resul = Resul & ".rtx#"
            If mimeType = "text/scriplet" Then Resul = Resul & ".wsc#"
            If mimeType = "text/sgml" Then Resul = Resul & ".sgm#"
            If mimeType = "text/sgml" Then Resul = Resul & ".sgml#"
            If mimeType = "text/tab-separated-values" Then Resul = Resul & ".tsv#"
            If mimeType = "text/uri-list" Then Resul = Resul & ".uni#"
            If mimeType = "text/uri-list" Then Resul = Resul & ".unis#"
            If mimeType = "text/uri-list" Then Resul = Resul & ".uri#"
            If mimeType = "text/uri-list" Then Resul = Resul & ".uris#"
            If mimeType = "text/vnd.abc" Then Resul = Resul & ".abc#"
            If mimeType = "text/vnd.fmi.flexstor" Then Resul = Resul & ".flx#"
            If mimeType = "text/vnd.rn-realtext" Then Resul = Resul & ".rt#"
            If mimeType = "text/vnd.wap.wml" Then Resul = Resul & ".wml#"
            If mimeType = "text/vnd.wap.wmlscript" Then Resul = Resul & ".wmls#"
            If mimeType = "text/webviewhtml" Then Resul = Resul & ".htt#"
            If mimeType = "text/x-asm" Then Resul = Resul & ".asm#"
            If mimeType = "text/x-asm" Then Resul = Resul & ".s#"
            If mimeType = "text/x-audiosoft-intra" Then Resul = Resul & ".aip#"
            If mimeType = "text/x-c" Then Resul = Resul & ".c#"
            If mimeType = "text/x-c" Then Resul = Resul & ".cc#"
            If mimeType = "text/x-c" Then Resul = Resul & ".cpp#"
            If mimeType = "text/x-component" Then Resul = Resul & ".htc#"
            If mimeType = "text/x-fortran" Then Resul = Resul & ".f#"
            If mimeType = "text/x-fortran" Then Resul = Resul & ".f77#"
            If mimeType = "text/x-fortran" Then Resul = Resul & ".f90#"
            If mimeType = "text/x-fortran" Then Resul = Resul & ".for#"
            If mimeType = "text/x-h" Then Resul = Resul & ".h#"
            If mimeType = "text/x-h" Then Resul = Resul & ".hh#"
            If mimeType = "text/x-java-source" Then Resul = Resul & ".jav#"
            If mimeType = "text/x-java-source" Then Resul = Resul & ".java#"
            If mimeType = "text/x-la-asf" Then Resul = Resul & ".lsx#"
            If mimeType = "text/x-m" Then Resul = Resul & ".m#"
            If mimeType = "text/xml" Then Resul = Resul & ".xml#"
            If mimeType = "text/x-pascal" Then Resul = Resul & ".p#"
            If mimeType = "text/x-script" Then Resul = Resul & ".hlb#"
            If mimeType = "text/x-script.csh" Then Resul = Resul & ".csh#"
            If mimeType = "text/x-script.elisp" Then Resul = Resul & ".el#"
            If mimeType = "text/x-script.guile" Then Resul = Resul & ".scm#"
            If mimeType = "text/x-script.ksh" Then Resul = Resul & ".ksh#"
            If mimeType = "text/x-script.lisp" Then Resul = Resul & ".lsp#"
            If mimeType = "text/x-script.perl" Then Resul = Resul & ".pl#"
            If mimeType = "text/x-script.perl-module" Then Resul = Resul & ".pm#"
            If mimeType = "text/x-script.phyton" Then Resul = Resul & ".py#"
            If mimeType = "text/x-script.rexx" Then Resul = Resul & ".rexx#"
            If mimeType = "text/x-script.scheme" Then Resul = Resul & ".scm#"
            If mimeType = "text/x-script.sh" Then Resul = Resul & ".sh#"
            If mimeType = "text/x-script.tcl" Then Resul = Resul & ".tcl#"
            If mimeType = "text/x-script.tcsh" Then Resul = Resul & ".tcsh#"
            If mimeType = "text/x-script.zsh" Then Resul = Resul & ".zsh#"
            If mimeType = "text/x-server-parsed-html" Then Resul = Resul & ".shtml#"
            If mimeType = "text/x-server-parsed-html" Then Resul = Resul & ".ssi#"
            If mimeType = "text/x-setext" Then Resul = Resul & ".etx#"
            If mimeType = "text/x-sgml" Then Resul = Resul & ".sgm#"
            If mimeType = "text/x-sgml" Then Resul = Resul & ".sgml#"
            If mimeType = "text/x-speech" Then Resul = Resul & ".spc#"
            If mimeType = "text/x-speech" Then Resul = Resul & ".talk#"
            If mimeType = "text/x-uil" Then Resul = Resul & ".uil#"
            If mimeType = "text/x-uuencode" Then Resul = Resul & ".uu#"
            If mimeType = "text/x-uuencode" Then Resul = Resul & ".uue#"
            If mimeType = "text/x-vcalendar" Then Resul = Resul & ".vcs#"
            If mimeType = "video/animaflex" Then Resul = Resul & ".afl#"
            If mimeType = "video/avi" Then Resul = Resul & ".avi#"
            If mimeType = "video/avs-video" Then Resul = Resul & ".avs#"
            If mimeType = "video/dl" Then Resul = Resul & ".dl#"
            If mimeType = "video/fli" Then Resul = Resul & ".fli#"
            If mimeType = "video/gl" Then Resul = Resul & ".gl#"
            If mimeType = "video/mpeg" Then Resul = Resul & ".m1v#"
            If mimeType = "video/mpeg" Then Resul = Resul & ".m2v#"
            If mimeType = "video/mpeg" Then Resul = Resul & ".mp2#"
            If mimeType = "video/mpeg" Then Resul = Resul & ".mp3#"
            If mimeType = "video/mpeg" Then Resul = Resul & ".mpa#"
            If mimeType = "video/mpeg" Then Resul = Resul & ".mpe#"
            If mimeType = "video/mpeg" Then Resul = Resul & ".mpeg#"
            If mimeType = "video/mpeg" Then Resul = Resul & ".mpg#"
            If mimeType = "video/msvideo" Then Resul = Resul & ".avi#"
            If mimeType = "video/quicktime" Then Resul = Resul & ".moov#"
            If mimeType = "video/quicktime" Then Resul = Resul & ".mov#"
            If mimeType = "video/quicktime" Then Resul = Resul & ".qt#"
            If mimeType = "video/vdo" Then Resul = Resul & ".vdo#"
            If mimeType = "video/vivo" Then Resul = Resul & ".viv#"
            If mimeType = "video/vivo" Then Resul = Resul & ".vivo#"
            If mimeType = "video/vnd.rn-realvideo" Then Resul = Resul & ".rv#"
            If mimeType = "video/vnd.vivo" Then Resul = Resul & ".viv#"
            If mimeType = "video/vnd.vivo" Then Resul = Resul & ".vivo#"
            If mimeType = "video/vosaic" Then Resul = Resul & ".vos#"
            If mimeType = "video/x-amt-demorun" Then Resul = Resul & ".xdr#"
            If mimeType = "video/x-amt-showrun" Then Resul = Resul & ".xsr#"
            If mimeType = "video/x-atomic3d-feature" Then Resul = Resul & ".fmf#"
            If mimeType = "video/x-dl" Then Resul = Resul & ".dl#"
            If mimeType = "video/x-dv" Then Resul = Resul & ".dif#"
            If mimeType = "video/x-dv" Then Resul = Resul & ".dv#"
            If mimeType = "video/x-fli" Then Resul = Resul & ".fli#"
            If mimeType = "video/x-gl" Then Resul = Resul & ".gl#"
            If mimeType = "video/x-isvideo" Then Resul = Resul & ".isu#"
            If mimeType = "video/x-motion-jpeg" Then Resul = Resul & ".mjpg#"
            If mimeType = "video/x-mpeg" Then Resul = Resul & ".mp2#"
            If mimeType = "video/x-mpeg" Then Resul = Resul & ".mp3#"
            If mimeType = "video/x-mpeq2a" Then Resul = Resul & ".mp2#"
            If mimeType = "video/x-ms-asf" Then Resul = Resul & ".asf#"
            If mimeType = "video/x-ms-asf" Then Resul = Resul & ".asx#"
            If mimeType = "video/x-ms-asf-plugin" Then Resul = Resul & ".asx#"
            If mimeType = "video/x-msvideo" Then Resul = Resul & ".avi#"
            If mimeType = "video/x-qtc" Then Resul = Resul & ".qtc#"
            If mimeType = "video/x-scm" Then Resul = Resul & ".scm#"
            If mimeType = "video/x-sgi-movie" Then Resul = Resul & ".movie#"
            If mimeType = "video/x-sgi-movie" Then Resul = Resul & ".mv#"
            If mimeType = "windows/metafile" Then Resul = Resul & ".wmf#"
            If mimeType = "www/mime" Then Resul = Resul & ".mime#"
            If mimeType = "x-conference/x-cooltalk" Then Resul = Resul & ".ice#"
            If mimeType = "xgl/drawing" Then Resul = Resul & ".xgz#"
            If mimeType = "xgl/movie" Then Resul = Resul & ".xmz#"
            If mimeType = "x-music/x-midi" Then Resul = Resul & ".mid#"
            If mimeType = "x-music/x-midi" Then Resul = Resul & ".midi#"
            If mimeType = "x-world/x-3dmf" Then Resul = Resul & ".3dm#"
            If mimeType = "x-world/x-3dmf" Then Resul = Resul & ".3dmf#"
            If mimeType = "x-world/x-3dmf" Then Resul = Resul & ".qd3#"
            If mimeType = "x-world/x-3dmf" Then Resul = Resul & ".qd3d#"
            If mimeType = "x-world/x-svr" Then Resul = Resul & ".svr#"
            If mimeType = "x-world/x-vrml" Then Resul = Resul & ".vrml#"
            If mimeType = "x-world/x-vrml" Then Resul = Resul & ".wrl#"
            If mimeType = "x-world/x-vrml" Then Resul = Resul & ".wrz#"
            If mimeType = "x-world/x-vrt" Then Resul = Resul & ".vrt#"
            If mimeType = "application/x-msdownload" Then Resul = Resul & ".exe#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".aspx#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".vb#"
            If mimeType = "application/octet-stream" Then Resul = Resul & ".txt#"

            If Resul = "" Then
                'No lo ha reconocido, a traves de la extensi�n miro si deber�a haberlo hecho. Es decir, .txt deber�a ser "text/plain"
                If EsMimeReconocibleSegunExtension(Extension) Then
                    Return "Fallido"
                Else
                    Return Extension & "#"
                End If
            Else
                Return Resul
            End If
        End Function

        ''' <summary>
        ''' Si segun la extensi�n el urlmon deber�a haberlo reconocido
        ''' </summary>
        ''' <param name="sExtension">Extension</param>
        ''' <returns>true/false</returns>
        ''' <remarks>Llamada desde: DameExtensionSegunMime; Tiempo maximo: 0</remarks>
        Private Function EsMimeReconocibleSegunExtension(ByVal sExtension As String) As Boolean
            If sExtension = ".3dm" Then Return True
            If sExtension = ".3dmf" Then Return True
            If sExtension = ".a" Then Return True
            If sExtension = ".aab" Then Return True
            If sExtension = ".aam" Then Return True
            If sExtension = ".aas" Then Return True
            If sExtension = ".abc" Then Return True
            If sExtension = ".acgi" Then Return True
            If sExtension = ".afl" Then Return True
            If sExtension = ".ai" Then Return True
            If sExtension = ".aif" Then Return True
            If sExtension = ".aifc" Then Return True
            If sExtension = ".aiff" Then Return True
            If sExtension = ".aim" Then Return True
            If sExtension = ".aip" Then Return True
            If sExtension = ".ani" Then Return True
            If sExtension = ".aos" Then Return True
            If sExtension = ".aps" Then Return True
            If sExtension = ".arc" Then Return True
            If sExtension = ".arj" Then Return True
            If sExtension = ".art" Then Return True
            If sExtension = ".asf" Then Return True
            If sExtension = ".asm" Then Return True
            If sExtension = ".asp" Then Return True
            If sExtension = ".asx" Then Return True
            If sExtension = ".au" Then Return True
            If sExtension = ".avi" Then Return True
            If sExtension = ".avs" Then Return True
            If sExtension = ".bcpio" Then Return True
            If sExtension = ".bin" Then Return True
            If sExtension = ".bm" Then Return True
            If sExtension = ".bmp" Then Return True
            If sExtension = ".boo" Then Return True
            If sExtension = ".book" Then Return True
            If sExtension = ".boz" Then Return True
            If sExtension = ".bsh" Then Return True
            If sExtension = ".bz" Then Return True
            If sExtension = ".bz2" Then Return True
            If sExtension = ".c" Then Return True
            If sExtension = ".c++" Then Return True
            If sExtension = ".cat" Then Return True
            If sExtension = ".cc" Then Return True
            If sExtension = ".ccad" Then Return True
            If sExtension = ".cco" Then Return True
            If sExtension = ".cdf" Then Return True
            If sExtension = ".cer" Then Return True
            If sExtension = ".cha" Then Return True
            If sExtension = ".chat" Then Return True
            If sExtension = ".class" Then Return True
            If sExtension = ".com" Then Return True
            If sExtension = ".conf" Then Return True
            If sExtension = ".cpio" Then Return True
            If sExtension = ".cpp" Then Return True
            If sExtension = ".cpt" Then Return True
            If sExtension = ".crl" Then Return True
            If sExtension = ".crt" Then Return True
            If sExtension = ".csh" Then Return True
            If sExtension = ".css" Then Return True
            If sExtension = ".cxx" Then Return True
            If sExtension = ".dcr" Then Return True
            If sExtension = ".deepv" Then Return True
            If sExtension = ".def" Then Return True
            If sExtension = ".der" Then Return True
            If sExtension = ".dif" Then Return True
            If sExtension = ".dir" Then Return True
            If sExtension = ".dl" Then Return True
            If sExtension = ".doc" Then Return True
            If sExtension = ".dot" Then Return True
            If sExtension = ".dp" Then Return True
            If sExtension = ".drw" Then Return True
            If sExtension = ".dump" Then Return True
            If sExtension = ".dv" Then Return True
            If sExtension = ".dvi" Then Return True
            If sExtension = ".dwf" Then Return True
            If sExtension = ".dwg" Then Return True
            If sExtension = ".dxf" Then Return True
            If sExtension = ".dxr" Then Return True
            If sExtension = ".el" Then Return True
            If sExtension = ".elc" Then Return True
            If sExtension = ".env" Then Return True
            If sExtension = ".eps" Then Return True
            If sExtension = ".es" Then Return True
            If sExtension = ".etx" Then Return True
            If sExtension = ".evy" Then Return True
            If sExtension = ".exe" Then Return True
            If sExtension = ".f" Then Return True
            If sExtension = ".f77" Then Return True
            If sExtension = ".f90" Then Return True
            If sExtension = ".fdf" Then Return True
            If sExtension = ".fif" Then Return True
            If sExtension = ".fli" Then Return True
            If sExtension = ".flo" Then Return True
            If sExtension = ".flx" Then Return True
            If sExtension = ".fmf" Then Return True
            If sExtension = ".for" Then Return True
            If sExtension = ".fpx" Then Return True
            If sExtension = ".frl" Then Return True
            If sExtension = ".funk" Then Return True
            If sExtension = ".g" Then Return True
            If sExtension = ".g3" Then Return True
            If sExtension = ".gif" Then Return True
            If sExtension = ".gl" Then Return True
            If sExtension = ".gsd" Then Return True
            If sExtension = ".gsm" Then Return True
            If sExtension = ".gsp" Then Return True
            If sExtension = ".gss" Then Return True
            If sExtension = ".gtar" Then Return True
            If sExtension = ".gz" Then Return True
            If sExtension = ".gzip" Then Return True
            If sExtension = ".h" Then Return True
            If sExtension = ".hdf" Then Return True
            If sExtension = ".help" Then Return True
            If sExtension = ".hgl" Then Return True
            If sExtension = ".hh" Then Return True
            If sExtension = ".hlb" Then Return True
            If sExtension = ".hlp" Then Return True
            If sExtension = ".hpg" Then Return True
            If sExtension = ".hpgl" Then Return True
            If sExtension = ".hqx" Then Return True
            If sExtension = ".hta" Then Return True
            If sExtension = ".htc" Then Return True
            If sExtension = ".htm" Then Return True
            If sExtension = ".html" Then Return True
            If sExtension = ".htmls" Then Return True
            If sExtension = ".htt" Then Return True
            If sExtension = ".htx" Then Return True
            If sExtension = ".ice" Then Return True
            If sExtension = ".ico" Then Return True
            If sExtension = ".idc" Then Return True
            If sExtension = ".ief" Then Return True
            If sExtension = ".iefs" Then Return True
            If sExtension = ".iges" Then Return True
            If sExtension = ".igs" Then Return True
            If sExtension = ".ima" Then Return True
            If sExtension = ".imap" Then Return True
            If sExtension = ".inf" Then Return True
            If sExtension = ".ins" Then Return True
            If sExtension = ".ip" Then Return True
            If sExtension = ".isu" Then Return True
            If sExtension = ".it" Then Return True
            If sExtension = ".iv" Then Return True
            If sExtension = ".ivr" Then Return True
            If sExtension = ".ivy" Then Return True
            If sExtension = ".jam" Then Return True
            If sExtension = ".jav" Then Return True
            If sExtension = ".java" Then Return True
            If sExtension = ".jcm" Then Return True
            If sExtension = ".jfif" Then Return True
            If sExtension = ".jfif-tbnl" Then Return True
            If sExtension = ".jpe" Then Return True
            If sExtension = ".jpeg" Then Return True
            If sExtension = ".jpg" Then Return True
            If sExtension = ".jps" Then Return True
            If sExtension = ".js" Then Return True
            If sExtension = ".jut" Then Return True
            If sExtension = ".kar" Then Return True
            If sExtension = ".ksh" Then Return True
            If sExtension = ".la" Then Return True
            If sExtension = ".lam" Then Return True
            If sExtension = ".latex" Then Return True
            If sExtension = ".lha" Then Return True
            If sExtension = ".lhx" Then Return True
            If sExtension = ".list" Then Return True
            If sExtension = ".lma" Then Return True
            If sExtension = ".log" Then Return True
            If sExtension = ".lsp" Then Return True
            If sExtension = ".lst" Then Return True
            If sExtension = ".lsx" Then Return True
            If sExtension = ".ltx" Then Return True
            If sExtension = ".lzh" Then Return True
            If sExtension = ".lzx" Then Return True
            If sExtension = ".m" Then Return True
            If sExtension = ".m1v" Then Return True
            If sExtension = ".m2a" Then Return True
            If sExtension = ".m2v" Then Return True
            If sExtension = ".m3u" Then Return True
            If sExtension = ".man" Then Return True
            If sExtension = ".map" Then Return True
            If sExtension = ".mar" Then Return True
            If sExtension = ".mbd" Then Return True
            If sExtension = ".mc$" Then Return True
            If sExtension = ".mcd" Then Return True
            If sExtension = ".mcf" Then Return True
            If sExtension = ".mcp" Then Return True
            If sExtension = ".me" Then Return True
            If sExtension = ".mht" Then Return True
            If sExtension = ".mhtml" Then Return True
            If sExtension = ".mid" Then Return True
            If sExtension = ".midi" Then Return True
            If sExtension = ".mif" Then Return True
            If sExtension = ".mime" Then Return True
            If sExtension = ".mjf" Then Return True
            If sExtension = ".mjpg" Then Return True
            If sExtension = ".mm" Then Return True
            If sExtension = ".mme" Then Return True
            If sExtension = ".mod" Then Return True
            If sExtension = ".moov" Then Return True
            If sExtension = ".mov" Then Return True
            If sExtension = ".movie" Then Return True
            If sExtension = ".mp2" Then Return True
            If sExtension = ".mp3" Then Return True
            If sExtension = ".mpa" Then Return True
            If sExtension = ".mpc" Then Return True
            If sExtension = ".mpe" Then Return True
            If sExtension = ".mpeg" Then Return True
            If sExtension = ".mpg" Then Return True
            If sExtension = ".mpga" Then Return True
            If sExtension = ".mpp" Then Return True
            If sExtension = ".mpt" Then Return True
            If sExtension = ".mpv" Then Return True
            If sExtension = ".mpx" Then Return True
            If sExtension = ".mrc" Then Return True
            If sExtension = ".ms" Then Return True
            If sExtension = ".mv" Then Return True
            If sExtension = ".my" Then Return True
            If sExtension = ".mzz" Then Return True
            If sExtension = ".nap" Then Return True
            If sExtension = ".naplps" Then Return True
            If sExtension = ".nc" Then Return True
            If sExtension = ".ncm" Then Return True
            If sExtension = ".nif" Then Return True
            If sExtension = ".niff" Then Return True
            If sExtension = ".nix" Then Return True
            If sExtension = ".nsc" Then Return True
            If sExtension = ".nvd" Then Return True
            If sExtension = ".o" Then Return True
            If sExtension = ".oda" Then Return True
            If sExtension = ".omc" Then Return True
            If sExtension = ".omcd" Then Return True
            If sExtension = ".omcr" Then Return True
            If sExtension = ".p" Then Return True
            If sExtension = ".p10" Then Return True
            If sExtension = ".p12" Then Return True
            If sExtension = ".p7a" Then Return True
            If sExtension = ".p7c" Then Return True
            If sExtension = ".p7m" Then Return True
            If sExtension = ".p7r" Then Return True
            If sExtension = ".p7s" Then Return True
            If sExtension = ".part" Then Return True
            If sExtension = ".pas" Then Return True
            If sExtension = ".pbm" Then Return True
            If sExtension = ".pcl" Then Return True
            If sExtension = ".pct" Then Return True
            If sExtension = ".pcx" Then Return True
            If sExtension = ".pdb" Then Return True
            If sExtension = ".pdf" Then Return True
            If sExtension = ".pfunk" Then Return True
            If sExtension = ".pfunk" Then Return True
            If sExtension = ".pgm" Then Return True
            If sExtension = ".pgm" Then Return True
            If sExtension = ".pic" Then Return True
            If sExtension = ".pict" Then Return True
            If sExtension = ".pkg" Then Return True
            If sExtension = ".pko" Then Return True
            If sExtension = ".pl" Then Return True
            If sExtension = ".plx" Then Return True
            If sExtension = ".pm" Then Return True
            If sExtension = ".pm4" Then Return True
            If sExtension = ".pm5" Then Return True
            If sExtension = ".png" Then Return True
            If sExtension = ".pnm" Then Return True
            If sExtension = ".pot" Then Return True
            If sExtension = ".pov" Then Return True
            If sExtension = ".ppa" Then Return True
            If sExtension = ".ppm" Then Return True
            If sExtension = ".pps" Then Return True
            If sExtension = ".ppt" Then Return True
            If sExtension = ".ppz" Then Return True
            If sExtension = ".pre" Then Return True
            If sExtension = ".prt" Then Return True
            If sExtension = ".ps" Then Return True
            If sExtension = ".psd" Then Return True
            If sExtension = ".pvu" Then Return True
            If sExtension = ".pwz" Then Return True
            If sExtension = ".py" Then Return True
            If sExtension = ".pyc" Then Return True
            If sExtension = ".qcp" Then Return True
            If sExtension = ".qd3" Then Return True
            If sExtension = ".qd3d" Then Return True
            If sExtension = ".qif" Then Return True
            If sExtension = ".qt" Then Return True
            If sExtension = ".qtc" Then Return True
            If sExtension = ".qti" Then Return True
            If sExtension = ".qtif" Then Return True
            If sExtension = ".ra" Then Return True
            If sExtension = ".ram" Then Return True
            If sExtension = ".ras" Then Return True
            If sExtension = ".rast" Then Return True
            If sExtension = ".rexx" Then Return True
            If sExtension = ".rf" Then Return True
            If sExtension = ".rgb" Then Return True
            If sExtension = ".rm" Then Return True
            If sExtension = ".rmi" Then Return True
            If sExtension = ".rmm" Then Return True
            If sExtension = ".rmp" Then Return True
            If sExtension = ".rng" Then Return True
            If sExtension = ".rnx" Then Return True
            If sExtension = ".roff" Then Return True
            If sExtension = ".rp" Then Return True
            If sExtension = ".rpm" Then Return True
            If sExtension = ".rt" Then Return True
            If sExtension = ".rtf" Then Return True
            If sExtension = ".rtx" Then Return True
            If sExtension = ".rv" Then Return True
            If sExtension = ".s" Then Return True
            If sExtension = ".s3m" Then Return True
            If sExtension = ".saveme" Then Return True
            If sExtension = ".sbk" Then Return True
            If sExtension = ".scm" Then Return True
            If sExtension = ".sdml" Then Return True
            If sExtension = ".sdp" Then Return True
            If sExtension = ".sdr" Then Return True
            If sExtension = ".sea" Then Return True
            If sExtension = ".set" Then Return True
            If sExtension = ".sgm" Then Return True
            If sExtension = ".sgml" Then Return True
            If sExtension = ".sh" Then Return True
            If sExtension = ".shar" Then Return True
            If sExtension = ".shtml" Then Return True
            If sExtension = ".sid" Then Return True
            If sExtension = ".sit" Then Return True
            If sExtension = ".skd" Then Return True
            If sExtension = ".skm" Then Return True
            If sExtension = ".skp" Then Return True
            If sExtension = ".skt" Then Return True
            If sExtension = ".sl" Then Return True
            If sExtension = ".smi" Then Return True
            If sExtension = ".smil" Then Return True
            If sExtension = ".snd" Then Return True
            If sExtension = ".snd" Then Return True
            If sExtension = ".sol" Then Return True
            If sExtension = ".spc" Then Return True
            If sExtension = ".spc" Then Return True
            If sExtension = ".spl" Then Return True
            If sExtension = ".spr" Then Return True
            If sExtension = ".sprite" Then Return True
            If sExtension = ".src" Then Return True
            If sExtension = ".ssi" Then Return True
            If sExtension = ".ssm" Then Return True
            If sExtension = ".sst" Then Return True
            If sExtension = ".step" Then Return True
            If sExtension = ".stl" Then Return True
            If sExtension = ".stp" Then Return True
            If sExtension = ".sv4cpio" Then Return True
            If sExtension = ".sv4crc" Then Return True
            If sExtension = ".svf" Then Return True
            If sExtension = ".svr" Then Return True
            If sExtension = ".swf" Then Return True
            If sExtension = ".t" Then Return True
            If sExtension = ".talk" Then Return True
            If sExtension = ".tar" Then Return True
            If sExtension = ".tbk" Then Return True
            If sExtension = ".tcl" Then Return True
            If sExtension = ".tcsh" Then Return True
            If sExtension = ".tex" Then Return True
            If sExtension = ".texi" Then Return True
            If sExtension = ".texinfo" Then Return True
            If sExtension = ".text" Then Return True
            If sExtension = ".tgz" Then Return True
            If sExtension = ".tif" Then Return True
            If sExtension = ".tiff" Then Return True
            If sExtension = ".tr" Then Return True
            If sExtension = ".tsi" Then Return True
            If sExtension = ".tsp" Then Return True
            If sExtension = ".tsv" Then Return True
            If sExtension = ".turbot" Then Return True
            If sExtension = ".txt" Then Return True
            If sExtension = ".uil" Then Return True
            If sExtension = ".uni" Then Return True
            If sExtension = ".unis" Then Return True
            If sExtension = ".unv" Then Return True
            If sExtension = ".uri" Then Return True
            If sExtension = ".uris" Then Return True
            If sExtension = ".ustar" Then Return True
            If sExtension = ".uu" Then Return True
            If sExtension = ".uue" Then Return True
            If sExtension = ".vcd" Then Return True
            If sExtension = ".vcs" Then Return True
            If sExtension = ".vda" Then Return True
            If sExtension = ".vdo" Then Return True
            If sExtension = ".vew" Then Return True
            If sExtension = ".viv" Then Return True
            If sExtension = ".vivo" Then Return True
            If sExtension = ".vmd" Then Return True
            If sExtension = ".vmf" Then Return True
            If sExtension = ".voc" Then Return True
            If sExtension = ".vos" Then Return True
            If sExtension = ".vox" Then Return True
            If sExtension = ".vqe" Then Return True
            If sExtension = ".vqf" Then Return True
            If sExtension = ".vql" Then Return True
            If sExtension = ".vrml" Then Return True
            If sExtension = ".vrt" Then Return True
            If sExtension = ".vsd" Then Return True
            If sExtension = ".vst" Then Return True
            If sExtension = ".vsw" Then Return True
            If sExtension = ".w60" Then Return True
            If sExtension = ".w61" Then Return True
            If sExtension = ".w6w" Then Return True
            If sExtension = ".wav" Then Return True
            If sExtension = ".wb1" Then Return True
            If sExtension = ".wbmp" Then Return True
            If sExtension = ".web" Then Return True
            If sExtension = ".wiz" Then Return True
            If sExtension = ".wk1" Then Return True
            If sExtension = ".wmf" Then Return True
            If sExtension = ".wml" Then Return True
            If sExtension = ".wmlc" Then Return True
            If sExtension = ".wmls" Then Return True
            If sExtension = ".wmlsc" Then Return True
            If sExtension = ".word" Then Return True
            If sExtension = ".wp" Then Return True
            If sExtension = ".wp5" Then Return True
            If sExtension = ".wp6" Then Return True
            If sExtension = ".wpd" Then Return True
            If sExtension = ".wq1" Then Return True
            If sExtension = ".wri" Then Return True
            If sExtension = ".wrl" Then Return True
            If sExtension = ".wrz" Then Return True
            If sExtension = ".wsc" Then Return True
            If sExtension = ".wsrc" Then Return True
            If sExtension = ".wtk" Then Return True
            If sExtension = ".xbm" Then Return True
            If sExtension = ".xdr" Then Return True
            If sExtension = ".xgz" Then Return True
            If sExtension = ".xif" Then Return True
            If sExtension = ".xl" Then Return True
            If sExtension = ".xla" Then Return True
            If sExtension = ".xlb" Then Return True
            If sExtension = ".xlc" Then Return True
            If sExtension = ".xld" Then Return True
            If sExtension = ".xlk" Then Return True
            If sExtension = ".xll" Then Return True
            If sExtension = ".xlm" Then Return True
            If sExtension = ".xls" Then Return True
            If sExtension = ".xlt" Then Return True
            If sExtension = ".xlv" Then Return True
            If sExtension = ".xlw" Then Return True
            If sExtension = ".xm" Then Return True
            If sExtension = ".xml" Then Return True
            If sExtension = ".xmz" Then Return True
            If sExtension = ".xpix" Then Return True
            If sExtension = ".xpm" Then Return True
            If sExtension = ".xpm" Then Return True
            If sExtension = ".xsr" Then Return True
            If sExtension = ".xwd" Then Return True
            If sExtension = ".xyz" Then Return True
            If sExtension = ".z" Then Return True
            If sExtension = ".zip" Then Return True
            If sExtension = ".zoo" Then Return True
            If sExtension = ".zsh" Then Return True

            Return False
        End Function
#End Region

#End Region

    End Module
End Namespace
