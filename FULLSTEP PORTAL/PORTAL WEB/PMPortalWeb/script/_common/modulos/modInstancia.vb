Namespace Fullstep.PMPortalWeb.CommonInstancia
    Module alta
        ''' <summary>
        ''' Carga el componente webCalendar.
        ''' </summary>
        ''' <param name="sIdi">Codigo Idioma. Idioma del usuario</param>       
        ''' <returns>Devuelve un componente de tipo WebCalendar configurado con el idioma del usuario</returns>
        ''' <remarks>Llamada desde= Certificado.aspx.vb (pageLoad_por_certificado;pageLoad_por_Solicitud)
        '''                         noConformidad.aspx.vb (Page_load)
        '''                         noConformidadCerradas.aspx.vb (page_load)
        '''                         detalleSolicitud.aspx.vb y solicitud.aspx.vb (page_load)
        ''' ; Tiempo m�ximo=0seg.</remarks>
        Public Function InsertarCalendario(ByVal sIdioma As String) As Infragistics.WebUI.WebSchedule.WebCalendar
            Dim SharedCalendar As New Infragistics.WebUI.WebSchedule.WebCalendar
            With SharedCalendar
                .ClientSideEvents.InitializeCalendar = "initCalendarEvent"
                .ClientSideEvents.DateClicked = "calendarDateClickedEvent"
                With .Layout
                    .NextMonthImageUrl = "ig_cal_grayN.gif"
                    .ShowTitle = "False"
                    .PrevMonthImageUrl = "ig_cal_grayP.gif"
                    '.FooterFormat = "Today: {0:d}"
                    .FooterFormat = ""
                    .FooterStyle.CssClass = "igCalendarFooterStyle"
                    .TodayDayStyle.CssClass = "igCalendarSelectedDayStyle"
                    .OtherMonthDayStyle.CssClass = "igCalendarOtherMonthDayStyle"
                    .CalendarStyle.CssClass = "igCalendarStyle"
                    .TodayDayStyle.CssClass = "igCalendarTodayDayStyle"
                    .DayHeaderStyle.CssClass = "igCalendarDayHeaderStyle"
                    .TitleStyle.CssClass = "igCalendarTitleStyle"
                End With
            End With

            Return SharedCalendar
        End Function
        Public Function InsertarDropDownUnidades(ByVal idEntry As String, ByVal _tabContainer As String, ByVal _lista As DataSet) As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
            Dim ogrid As Infragistics.WebUI.UltraWebGrid.UltraWebGrid

            ogrid = New Infragistics.WebUI.UltraWebGrid.UltraWebGrid

            ogrid.ID = "_" + idEntry + "_dd"
            ogrid.DisplayLayout = New Infragistics.WebUI.UltraWebGrid.UltraGridLayout
            ogrid.DisplayLayout.ClientSideEvents.InitializeLayoutHandler = "preInitGrid('" + _tabContainer + "',0,0);ddinitGridEvent"
            ogrid.DisplayLayout.ClientSideEvents.CellClickHandler = "ddcellClickEvent"
            ogrid.Bands.Add(New Infragistics.WebUI.UltraWebGrid.UltraGridBand)
            If _lista.Tables.Count > 0 Then
                ogrid.DataSource = _lista.Tables(0)
            End If
            ogrid.Height = Unit.Pixel(100)

            ogrid.DisplayLayout.Bands(0).CellClickAction = Infragistics.WebUI.UltraWebGrid.CellClickAction.RowSelect
            ogrid.Bands(0).RowSelectors = Infragistics.WebUI.UltraWebGrid.RowSelectors.No
            ogrid.Bands(0).RowStyle.CssClass = "igDropDownStyle"
            ogrid.CssClass = "igDropDown"
            ogrid.Bands(0).ColHeadersVisible = Infragistics.WebUI.UltraWebGrid.ShowMarginInfo.No

            ogrid.Visible = True
            ogrid.DataBind()
            Return ogrid
        End Function
        Public Function InsertarDropDownMonedas(ByVal idEntry As String, ByVal _tabContainer As String, ByVal _lista As DataSet) As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
            Dim ogrid As Infragistics.WebUI.UltraWebGrid.UltraWebGrid

            ogrid = New Infragistics.WebUI.UltraWebGrid.UltraWebGrid

            ogrid.ID = "_" + idEntry + "_dd"
            ogrid.DisplayLayout = New Infragistics.WebUI.UltraWebGrid.UltraGridLayout
            ogrid.DisplayLayout.ClientSideEvents.InitializeLayoutHandler = "preInitGrid('" + _tabContainer + "',0,0);ddinitGridEvent"
            ogrid.DisplayLayout.ClientSideEvents.CellClickHandler = "ddcellClickEvent"
            ogrid.Bands.Add(New Infragistics.WebUI.UltraWebGrid.UltraGridBand)
            If _lista.Tables.Count > 0 Then
                ogrid.DataSource = _lista.Tables(0)
            End If
            ogrid.Height = Unit.Pixel(100)

            ogrid.DisplayLayout.Bands(0).CellClickAction = Infragistics.WebUI.UltraWebGrid.CellClickAction.RowSelect
            ogrid.Bands(0).RowSelectors = Infragistics.WebUI.UltraWebGrid.RowSelectors.No
            ogrid.Bands(0).RowStyle.CssClass = "igDropDownStyle"
            ogrid.CssClass = "igDropDown"
            ogrid.Bands(0).ColHeadersVisible = Infragistics.WebUI.UltraWebGrid.ShowMarginInfo.No

            ogrid.Visible = True
            ogrid.DataBind()
            Return ogrid
        End Function
        Public Function InsertarDropDownFormasPago(ByVal idEntry As String, ByVal _tabContainer As String, ByVal _lista As DataSet) As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
            Dim ogrid As Infragistics.WebUI.UltraWebGrid.UltraWebGrid

            ogrid = New Infragistics.WebUI.UltraWebGrid.UltraWebGrid

            ogrid.ID = "_" + idEntry + "_dd"
            ogrid.DisplayLayout = New Infragistics.WebUI.UltraWebGrid.UltraGridLayout
            ogrid.DisplayLayout.ClientSideEvents.InitializeLayoutHandler = "preInitGrid('" + _tabContainer + "',0,0);ddinitGridEvent"
            ogrid.DisplayLayout.ClientSideEvents.CellClickHandler = "ddcellClickEvent"
            ogrid.Bands.Add(New Infragistics.WebUI.UltraWebGrid.UltraGridBand)
            If _lista.Tables.Count > 0 Then
                ogrid.DataSource = _lista.Tables(0)
            End If
            ogrid.Height = Unit.Pixel(100)

            ogrid.DisplayLayout.Bands(0).CellClickAction = Infragistics.WebUI.UltraWebGrid.CellClickAction.RowSelect
            ogrid.Bands(0).RowSelectors = Infragistics.WebUI.UltraWebGrid.RowSelectors.No
            ogrid.Bands(0).RowStyle.CssClass = "igDropDownStyle"
            ogrid.CssClass = "igDropDown"
            ogrid.Bands(0).ColHeadersVisible = Infragistics.WebUI.UltraWebGrid.ShowMarginInfo.No

            ogrid.Visible = True
            ogrid.DataBind()
            Return ogrid
        End Function
        Public Function InsertarDropDownDestinos(ByVal idEntry As String, ByVal _tabContainer As String, ByVal _lista As DataSet) As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
            Dim ogrid As Infragistics.WebUI.UltraWebGrid.UltraWebGrid

            ogrid = New Infragistics.WebUI.UltraWebGrid.UltraWebGrid

            ogrid.ID = "_" + idEntry + "_dd"
            ogrid.DisplayLayout = New Infragistics.WebUI.UltraWebGrid.UltraGridLayout
            ogrid.DisplayLayout.ClientSideEvents.InitializeLayoutHandler = "preInitGrid('" + _tabContainer + "',0,0);ddinitGridEvent"
            ogrid.DisplayLayout.ClientSideEvents.CellClickHandler = "ddcellClickEvent"
            ogrid.Bands.Add(New Infragistics.WebUI.UltraWebGrid.UltraGridBand)
            If _lista.Tables.Count > 0 Then
                ogrid.DataSource = _lista.Tables(0)
            End If
            ogrid.Height = Unit.Pixel(100)

            ogrid.DisplayLayout.Bands(0).CellClickAction = Infragistics.WebUI.UltraWebGrid.CellClickAction.RowSelect
            ogrid.Bands(0).RowSelectors = Infragistics.WebUI.UltraWebGrid.RowSelectors.No
            ogrid.Bands(0).RowStyle.CssClass = "igDropDownStyle"
            ogrid.CssClass = "igDropDown"
            ogrid.Bands(0).ColHeadersVisible = Infragistics.WebUI.UltraWebGrid.ShowMarginInfo.No

            ogrid.Visible = True
            ogrid.DataBind()
            Return ogrid
        End Function
        Public Function InsertarDropDownPaises(ByVal idEntry As String, ByVal _tabContainer As String, ByVal _lista As DataSet) As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
            Dim ogrid As Infragistics.WebUI.UltraWebGrid.UltraWebGrid

            ogrid = New Infragistics.WebUI.UltraWebGrid.UltraWebGrid

            ogrid.ID = "_" + idEntry + "_dd"
            ogrid.DisplayLayout = New Infragistics.WebUI.UltraWebGrid.UltraGridLayout

            ogrid.DisplayLayout.ClientSideEvents.InitializeLayoutHandler = "preInitGrid('" + _tabContainer + "',0,0);ddinitGridEvent"
            ogrid.DisplayLayout.ClientSideEvents.CellClickHandler = "ddcellClickEvent"
            ogrid.Bands.Add(New Infragistics.WebUI.UltraWebGrid.UltraGridBand)
            If _lista.Tables.Count > 0 Then
                ogrid.DataSource = _lista.Tables(0)
            End If
            ogrid.Height = Unit.Pixel(100)

            ogrid.DisplayLayout.Bands(0).CellClickAction = Infragistics.WebUI.UltraWebGrid.CellClickAction.RowSelect
            ogrid.Bands(0).RowSelectors = Infragistics.WebUI.UltraWebGrid.RowSelectors.No
            ogrid.Bands(0).RowStyle.CssClass = "igDropDownStyle"
            ogrid.CssClass = "igDropDown"
            ogrid.Bands(0).ColHeadersVisible = Infragistics.WebUI.UltraWebGrid.ShowMarginInfo.No

            ogrid.Visible = True
            ogrid.DataBind()
            Return ogrid
        End Function
        ''' <summary>
        ''' Los valores de todo combo se muestran en un DIV(cada combo el suyo) en un UltraWebGrid(cada combo el suyo)
        ''' </summary>
        ''' <param name="idEntry">Id entry del combo</param>
        ''' <param name="_tabContainer">Contenedor del combo</param>        
        ''' <param name="_lista">Valores del combo</param>
        ''' <param name="_tipo">Tipo de dato. Sin tipo, string,archivo,...</param> 
        ''' <param name="_Idi">Idioma</param>
        ''' <param name="EsDesglose">Si es un combo en un desglose o no</param>
        ''' <returns>Grid en donde se mostraran los valores del combo</returns>
        ''' <remarks>Llamada desde: campos.ascx/Page_Load   desglose.ascx/Page_Load; Tiempo m�ximo:0</remarks>
        Public Function InsertarDropDown(ByVal idEntry As String, ByVal _tabContainer As String, ByVal _lista As DataSet, ByVal _tipo As Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral, ByVal _Idi As String, Optional ByVal EsDesglose As Boolean = False) As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
            Dim ogrid As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
            On Error Resume Next
            ogrid = New Infragistics.WebUI.UltraWebGrid.UltraWebGrid

            ogrid.ID = "_" + idEntry + "_dd"

            ogrid.DisplayLayout = New Infragistics.WebUI.UltraWebGrid.UltraGridLayout
            ogrid.DisplayLayout.FrameStyle.CustomRules = "position:absolute;"

            Dim iPos As Integer
            Select Case _tipo
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoString, Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoCorto, Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoLargo, Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio
                    iPos = 4
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico
                    iPos = 3
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoFecha
                    iPos = 5
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoBoolean
                    iPos = 1
            End Select

            ogrid.DisplayLayout.ClientSideEvents.CellClickHandler = "ddcellClickEvent"
            ogrid.Bands.Add(New Infragistics.WebUI.UltraWebGrid.UltraGridBand)
            ogrid.DataSource = _lista.Tables(0)

            ogrid.Bands(0).RowSelectors = Infragistics.WebUI.UltraWebGrid.RowSelectors.No
            ogrid.Bands(0).RowStyle.CssClass = "igDropDownStyle"
            ogrid.CssClass = "igDropDown"
            ogrid.Bands(0).ColHeadersVisible = Infragistics.WebUI.UltraWebGrid.ShowMarginInfo.No

            ogrid.Visible = True
            ogrid.DataBind()
            Dim sKey As String
            Dim i As Integer
            If Not EsDesglose Then
                ogrid.Width = Unit.Pixel(530)
            End If

            Select Case _tipo
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoFecha
                    sKey = "VALOR_FEC"
                    ogrid.Bands(0).Columns.FromKey(sKey).Format = "dd/MM/yyyy"
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico
                    sKey = "VALOR_NUM"
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoCorto, Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoLargo, Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio, Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoString
                    sKey = "VALOR_TEXT_" & _Idi
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoBoolean
                    sKey = "TEXT_" & _Idi
                    ogrid.Width = Unit.Pixel(50)
            End Select
            For i = 0 To ogrid.Bands(0).Columns.Count - 1
                If ogrid.Bands(0).Columns(i).Key <> sKey Then
                    ogrid.Bands(0).Columns(i).Hidden = True
                Else
                    ogrid.Bands(0).Columns(i).Width = Unit.Percentage(100)
                    iPos = i
                End If
            Next
            ogrid.DisplayLayout.ClientSideEvents.InitializeLayoutHandler = "preInitGrid('" + _tabContainer + "',0," + iPos.ToString + ");ddinitGridEvent"
            ogrid.DisplayLayout.NoDataMessage = ""

            If _lista.Tables(0).Rows.Count > 12 Then
                ogrid.DisplayLayout.FrameStyle.Height = System.Web.UI.WebControls.Unit.Pixel((12.5 * 12) - 5)
            End If

            Return ogrid
        End Function
        ''' Revisado por: SRA. Fecha: 09/10/2012
        ''' <summary>
        ''' Crea el dataset para un combo de tipo boolean
        ''' </summary>
        ''' <param name="sIdi">Idioma en que tienen que ir los textos</param>
        ''' <returns>Dataset con una tabla que contiene los valores del combo booleano en el idioma solicitado</returns>
        ''' <remarks>Llamada desde campos.ascx.vb y desglose.ascx.vb. Tiempo m�x inferior a 0,1 seg</remarks>
        Public Function CrearDataset(ByVal sIdi As String, ByVal sTextoSi As String, ByVal sTextoNo As String) As DataSet
            Dim ds As New DataSet
            Dim oRow(1) As Object

            oRow(0) = "1"
            oRow(1) = sTextoSi
            ds.Tables.Add("BOOL")

            ds.Tables("BOOL").Columns.Add("value", System.Type.GetType("System.String"))
            ds.Tables("BOOL").Columns.Add("TEXT_" & sIdi, System.Type.GetType("System.String"))
            ds.Tables("BOOL").Rows.Add(oRow)

            oRow(0) = "0"
            oRow(1) = sTextoNo
            ds.Tables("BOOL").Rows.Add(oRow)
            Return ds
        End Function
        Private Function DevolverIdCampoActual(ByRef oDsPares As DataSet, ByVal IdCampoVersion As Long) As Long
            If oDsPares Is Nothing Then
                Return IdCampoVersion
            End If
            Dim find(0) As Object
            find(0) = IdCampoVersion
            Dim oRow As DataRow

            oRow = oDsPares.Tables(0).Rows.Find(find)
            Return oRow.Item("IDCAMPOACTUAL")
        End Function
    End Module
End Namespace