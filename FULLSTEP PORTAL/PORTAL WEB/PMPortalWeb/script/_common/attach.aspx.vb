Imports System.IO
Imports System.Security.Principal
Namespace Fullstep.PMPortalWeb
    Partial Class adjunto
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' <summary>
        ''' Descarga, a disco, del adjunto indicado en Request("Id") y apertura de pantalla con links para q el usuario reciba/acceda el adjunto 
        ''' </summary>
        ''' <param name="sender">pantalla</param>
        ''' <param name="e">parametro de sistema</param>            
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,2</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim lCiaComp As Long = IdCiaComp
            Dim sPath As String
            Dim FSWSServer As PMPortalServer.Root = Session("FS_Portal_Server")
            Dim arrPath() As String
            Dim wsAdjuntos As New FSN_Adjuntos.FSN_Adjuntos
            Dim file() As Byte
            If Request("idContrato") <> "" Then
                TratarAdjuntoContrato()
            Else
                Dim MiInstancia As Long
                Try
                    MiInstancia = CLng(Request("instancia"))
                Catch ex As Exception
                    MiInstancia = 0
                End Try

                Dim EsAltafactura As Boolean = IIf(Request("esAltaFactura") = "1", True, False)

                If Request("adjuntosNew") <> "" OrElse Request("adjuntos") <> "" Then
                    'Se descargan todos los adjuntos en un fichero comprimido
                    Dim NuevosAdjuntos As String()
                    Dim Adjuntos() As String
                    Dim Tipos() As String
                    Dim Nombres() As String
                    Dim Fechas() As String
                    Dim bSinCtrlNombre_DscgTodos As Boolean = False
                    Dim PathFile As String

                    Dim NombreFicheroZip As String

                    Tipos = Split(Request("tipo"), "xx")

                    If Request("Nombre") <> Nothing Then
                        Nombres = Split(Request("Nombre"), "@xx@")
                        Fechas = Split(Request("fecha"), "@xx@")
                    Else
                        bSinCtrlNombre_DscgTodos = True
                    End If

                    sPath = ConfigurationManager.AppSettings("temp") + "\" + FSNLibrary.modUtilidades.GenerateRandomPath() & "_" & FSPMUser.IdCia
                    NuevosAdjuntos = Split(Request("adjuntosNew"), ",")
                    Adjuntos = Split(Request("adjuntos"), ",")

                    For i As Short = 0 To NuevosAdjuntos.GetUpperBound(0)
                        Dim Tipo As String
                        If NuevosAdjuntos(i) = "" Then Exit For
                        Dim Nombre As String = ""
                        Dim Fecha As String = ""
                        Dim oAdjunto As PMPortalServer.Adjunto

                        oAdjunto = FSWSServer.Get_Adjunto()

                        oAdjunto.Id = NuevosAdjuntos(i)

                        'Buscamos el tipo del adjunto que tratamos
                        For x = 0 To Tipos.GetUpperBound(0)
                            Dim pos As Short = Tipos(x).IndexOf(",")
                            If Tipos(x).Substring(0, pos).ToString = NuevosAdjuntos(i).ToString Then
                                Tipo = Tipos(x).Substring(pos + 1)
                            End If
                        Next
                        If Not bSinCtrlNombre_DscgTodos Then
                            For x = 0 To Nombres.GetUpperBound(0)
                                Dim pos As Short = Nombres(x).IndexOf("@yy@")
                                If Nombres(x).Substring(0, pos).ToString = NuevosAdjuntos(i).ToString Then
                                    Nombre = Nombres(x).Substring(pos + 4)
                                End If
                            Next
                            For x = 0 To Fechas.GetUpperBound(0)
                                Dim pos As Short = Fechas(x).IndexOf("@yy@")
                                If Fechas(x).Substring(0, pos).ToString = NuevosAdjuntos(i).ToString Then
                                    Fecha = Fechas(x).Substring(pos + 4)
                                End If
                            Next
                        Else
                            Fecha = Now.ToString
                        End If

                        If oAdjunto.CtrlEsTuyo(lCiaComp, Tipo, Request("instancia"), FSPMUser.CodProve, Request("Campo"), Nombre, Fecha, bSinCtrlNombre_DscgTodos, EsAltafactura:=EsAltafactura) = False Then Exit Sub

                        If MiInstancia <= 0 Then
                            oAdjunto.LoadFromRequest(lCiaComp, Tipo)
                            PathFile = oAdjunto.SaveAdjunToDiskZip(lCiaComp, Tipo, sPath)
                            file = wsAdjuntos.LeerAdjuntoPortal(FSPMUser.Cod, FSPMUser.Password, oAdjunto.Id, Tipo, False)
                            BytesToDisk(file, PathFile)
                        Else
                            oAdjunto.LoadInstFromRequest(lCiaComp, Tipo)

                            'Si es una adjunto metido desde A�adir por valor por defecto. La informaci�n
                            'esta en LINEA_DESGLOSE_ADJUN
                            If oAdjunto.DataSize = -1000 Then
                                oAdjunto.LoadFromRequest(lCiaComp, Tipo)
                                PathFile = oAdjunto.SaveAdjunToDiskZip(lCiaComp, Tipo, sPath)
                            Else
                                PathFile = oAdjunto.SaveAdjunToDiskZip(lCiaComp, Tipo, sPath, True)
                            End If
                            file = wsAdjuntos.LeerAdjuntoPortal(FSPMUser.Cod, FSPMUser.Password, oAdjunto.Id, Tipo, True)
                            BytesToDisk(file, PathFile)
                        End If
                    Next

                    For i As Short = 0 To Adjuntos.GetUpperBound(0)
                        Dim Tipo As String
                        If Adjuntos(i) = "" Then Exit For
                        Dim Nombre As String = ""
                        Dim Fecha As String = ""
                        Dim oAdjunto As PMPortalServer.Adjunto

                        oAdjunto = FSWSServer.Get_Adjunto()

                        oAdjunto.Id = Adjuntos(i)

                        'Buscamos el tipo del adjunto que tratamos
                        For x = 0 To Tipos.GetUpperBound(0)
                            Dim pos As Short = Tipos(x).IndexOf(",")
                            If Tipos(x).Substring(0, pos).ToString = Adjuntos(i).ToString Then
                                Tipo = Tipos(x).Substring(pos + 1)
                            End If
                        Next
                        If Not bSinCtrlNombre_DscgTodos Then
                            For x = 0 To Nombres.GetUpperBound(0)
                                Dim pos As Short = Nombres(x).IndexOf("@yy@")
                                If Nombres(x).Substring(0, pos).ToString = Adjuntos(i).ToString Then
                                    Nombre = Nombres(x).Substring(pos + 4)
                                End If
                            Next
                            For x = 0 To Fechas.GetUpperBound(0)
                                Dim pos As Short = Fechas(x).IndexOf("@yy@")
                                If Fechas(x).Substring(0, pos).ToString = Adjuntos(i).ToString Then
                                    Fecha = Fechas(x).Substring(pos + 4)
                                End If
                            Next
                        Else
                            Fecha = Now.ToString
                        End If

                        If oAdjunto.CtrlEsTuyo(lCiaComp, Tipo, Request("instancia"), FSPMUser.CodProve, Request("Campo"), Nombre, Fecha, bSinCtrlNombre_DscgTodos, EsAltafactura:=EsAltafactura) = False Then Exit Sub

                        If MiInstancia <= 0 Then
                            oAdjunto.LoadFromRequest(lCiaComp, Tipo)
                            PathFile = oAdjunto.SaveAdjunToDiskZip(lCiaComp, Tipo, sPath)
                            file = wsAdjuntos.LeerAdjuntoPortal(FSPMUser.Cod, FSPMUser.Password, oAdjunto.Id, Tipo, False)
                            BytesToDisk(file, PathFile)
                        Else
                            oAdjunto.LoadInstFromRequest(lCiaComp, Tipo)

                            'Si es una adjunto metido desde A�adir por valor por defecto. La informaci�n
                            'esta en LINEA_DESGLOSE_ADJUN
                            If oAdjunto.DataSize = -1000 Then
                                oAdjunto.LoadFromRequest(lCiaComp, Tipo)
                                PathFile = oAdjunto.SaveAdjunToDiskZip(lCiaComp, Tipo, sPath)
                            Else
                                PathFile = oAdjunto.SaveAdjunToDiskZip(lCiaComp, Tipo, sPath, True)
                            End If
                            file = wsAdjuntos.LeerAdjuntoPortal(FSPMUser.Cod, FSPMUser.Password, oAdjunto.Id, Tipo, True)
                            BytesToDisk(file, PathFile)
                        End If
                    Next
                    NombreFicheroZip = FSNLibrary.modUtilidades.Comprimir(sPath, "*.*", sPath, True, False)
                    Dim FicheroZip As String() = Split(NombreFicheroZip, "##")
                    arrPath = sPath.Split("\")
                    Response.Redirect("../_common/download.aspx?path=" + arrPath(UBound(arrPath)) + "&nombre=" + FicheroZip(0) + "&datasize=" + FicheroZip(1))
                Else

                    'Solo se descarga un adjunto

                    Dim oAdjunto As PMPortalServer.Adjunto

                    oAdjunto = FSWSServer.Get_Adjunto()

                    oAdjunto.Id = Request("Id")

                    If InStr(Request("Nombre"), "@xx@") > 0 Then 'No deberia pasar. DescargarTodos NO se ve cuando solo hay un archivo.
                        Try
                            Dim Nombres() As String
                            Dim Fechas() As String
                            Dim pos As Short

                            Nombres = Split(Request("Nombre"), "@xx@")
                            Fechas = Split(Request("fecha"), "@xx@")

                            Dim Nombre As String = ""
                            Dim Fecha As String = ""

                            pos = Nombres(0).IndexOf("@yy@")
                            Nombre = Nombres(0).Substring(pos + 4)

                            pos = Fechas(0).IndexOf("@yy@")
                            Fecha = Fechas(0).Substring(pos + 4)

                            'No deberia pasar
                            If oAdjunto.CtrlEsTuyo(lCiaComp, Request("tipo"), Request("instancia"), FSPMUser.CodProve, Request("Campo"), Nombre, Fecha, False, EsAltafactura:=EsAltafactura) = False Then Exit Sub
                        Catch ex As Exception
                            'En la duda q descargue
                        End Try
                    Else
                        If oAdjunto.CtrlEsTuyo(lCiaComp, Request("tipo"), Request("instancia"), FSPMUser.CodProve, Request("Campo"), Request("Nombre"), Request("fecha"), False, EsAltafactura:=EsAltafactura) = False Then Exit Sub
                    End If

                    If MiInstancia <= 0 Then
                        oAdjunto.LoadFromRequest(lCiaComp, Request("tipo"))
                        file = wsAdjuntos.LeerAdjuntoPortal(FSPMUser.Cod, FSPMUser.Password, oAdjunto.Id, Request("tipo"), False)
                        sPath = BytesToDisk(file, , oAdjunto.Nom)
                    Else
                        oAdjunto.LoadInstFromRequest(lCiaComp, Request("tipo"))
                            'Si es una adjunto metido desde A�adir por valor por defecto. La informaci�n esta en LINEA_DESGLOSE_ADJUN
                            If oAdjunto.DataSize = -1000 Then
                                oAdjunto.LoadFromRequest(lCiaComp, Request("tipo"))
                                file = wsAdjuntos.LeerAdjuntoPortal(FSPMUser.Cod, FSPMUser.Password, oAdjunto.Id, 5, True)
                            Else
                                file = wsAdjuntos.LeerAdjuntoPortal(FSPMUser.Cod, FSPMUser.Password, oAdjunto.Id, Request("tipo"), True)
                            End If
                            sPath = BytesToDisk(file, , oAdjunto.Nom)
                        End If
                        arrPath = sPath.Split("\")
                        Response.Redirect("../_common/download.aspx?path=" + arrPath(UBound(arrPath) - 1) + "&nombre=" + Server.UrlEncode(arrPath(UBound(arrPath))) + "&datasize=" + oAdjunto.DataSize.ToString)
                    End If
                End If

        End Sub
        ''' <summary>
        ''' Graba el buffer de bytes en la ruta especificada por parametro
        ''' </summary>
        ''' <param name="buffer">buffer de bytes</param>
        ''' <param name="sPath">Ruta y nombre del fichero que se guardara en disco</param>
        ''' <param name="sNom">nombre del fichero que se guardara en disco</param>
        ''' <remarks></remarks>
        Private Function BytesToDisk(ByVal buffer As Byte(), Optional ByVal sPath As String = "", Optional ByVal sNom As String = "") As String
            If sPath <> "" Then
                'Si se graba en la carpeta que luego se comprimira en ZIP
                System.IO.File.WriteAllBytes(sPath, buffer)
            Else
                sPath = ConfigurationManager.AppSettings("temp") + "\" + modUtilidades.GenerateRandomPath() & "_" & FSPMUser.IdCia

                Dim oFolder As System.IO.Directory

                If Not oFolder.Exists(sPath) Then
                    oFolder.CreateDirectory(sPath)
                End If
                sPath += "\" + sNom
                System.IO.File.WriteAllBytes(sPath, buffer)
            End If
            Return sPath
        End Function

        Private Sub TratarAdjuntoContrato()
            Dim i As Integer
            Dim lCiaComp As Long = IdCiaComp
            Dim FSWSServer As PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oFolder As System.IO.Directory
            Dim oAdjunto As PMPortalServer.Adjunto

            oAdjunto = FSWSServer.Get_Adjunto()

            If Request("idArchivoContrato") <> "0" Then
                'PCA.ID=" & lIdArchivoContrato
                oAdjunto.Id = Request("idArchivoContrato")
                If oAdjunto.CtrlEsTuyo(lCiaComp, 2, Request("instancia"), FSPMUser.CodProve, Request("Campo"), Server.UrlDecode(Request("NombreAdjunto")), Request("fecha"), False, False, True, Request("EsTipo1"), False) = False Then Exit Sub
            Else
                'PCA.ID_CONTRATO=" & lIdContrato
                oAdjunto.Id = Request("idContrato")
                If oAdjunto.CtrlEsTuyo(lCiaComp, 2, Request("instancia"), FSPMUser.CodProve, Request("Campo"), Server.UrlDecode(Request("NombreAdjunto")), Request("fecha"), False, True, True, Request("EsTipo1"), False) = False Then Exit Sub
            End If

            Dim sFolder As String
            sFolder = FSNLibrary.modUtilidades.GenerateRandomPath() & "_" & FSPMUser.IdCia

            Dim impersonationContext As WindowsImpersonationContext
            Dim sPath As String = ConfigurationManager.AppSettings("temp") & "\" & sFolder

            If Not Directory.Exists(sPath) Then Directory.CreateDirectory(sPath)

            sPath = sPath & "\" & Server.UrlDecode(Request("NombreAdjunto"))
            FSWSServer.Impersonate()

            impersonationContext = RealizarSuplantacion(FSWSServer.ImpersonateLogin, FSWSServer.ImpersonatePassword, FSWSServer.ImpersonateDominio)

            Dim buffer As Byte()

            buffer = oAdjunto.LeerContratoAdjunto(lCiaComp, Request("idContrato"), Request("idArchivoContrato"))

            System.IO.File.WriteAllBytes(sPath, buffer)

            If Not impersonationContext Is Nothing Then
                impersonationContext.Undo()
            End If

            Response.Redirect("../_common/download.aspx?idArchivoContrato=" & Request("idArchivoContrato") & "&IdContrato=" & Request("idContrato") & "&path=" & sFolder & "&nombre=" & Request("NombreAdjunto") & "&datasize=" & Request("datasize") & "&Instancia=" & Request("Instancia"))
        End Sub
    End Class
End Namespace