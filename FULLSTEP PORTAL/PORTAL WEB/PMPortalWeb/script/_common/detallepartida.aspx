<%@ Page Language="vb" AutoEventWireup="false" Codebehind="detallepartida.aspx.vb" Inherits="Fullstep.PMPortalWeb.detallepartida" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body onload="window.focus();">
		<form id="form1" runat="server">
			<table cellspacing="1" cellpadding="4" width="100%" border="0">
				<TR>
					<td width="5%"><img src="images/candado.jpg"></td>
					<TD width="90%" align="center" nowrap><asp:label id="lblTitulo" runat="server" CssClass="titulo"></asp:label></TD>
					<TD width="5%"><img src="images/Bt_Cerrar.png" onclick="javascript:self.close();"></TD>
				</TR>
			</table>
			<table cellspacing="1" cellpadding="4" border="0" width="100%">
				<TR>
					<TD nowrap class="ugfilatablaCabecera"><asp:Label id="lblCod" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist"><asp:Label id="lblCodBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD nowrap class="ugfilatablaCabecera"><asp:Label id="lblNombre" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist"><asp:Label id="lblNombreBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD nowrap class="ugfilatablaCabecera"><asp:Label id="lblCentro" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist"><asp:Label id="lblCentroBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD nowrap class="ugfilatablaCabecera"><asp:Label id="lblFechaInicio" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist"><asp:Label id="lblFechaInicioBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD nowrap class="ugfilatablaCabecera"><asp:Label id="lblFechaFin" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist"><asp:Label id="lblFechaFinBD" runat="server"></asp:Label></TD>
				</TR>
			</table>
		</form>
	</body>
</html>
