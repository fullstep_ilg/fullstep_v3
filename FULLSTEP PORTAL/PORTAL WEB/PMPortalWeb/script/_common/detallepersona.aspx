<%@ Page Language="vb" AutoEventWireup="false" Codebehind="detallepersona.aspx.vb" Inherits="Fullstep.PMPortalWeb.detallepersona" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblPer" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" cellSpacing="1"
				cellPadding="1" width="100%" border="0" height="100%">
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblCod" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblCodBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblNombre" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblNombreBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblApe" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblApeBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblCargo" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblCargoBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblTfno" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblTfnoBD" runat="server">Label</asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblFax" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblFaxBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblMail" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblMailBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblDep" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblDepBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 100px">
						<asp:Label id="lblOrg" runat="server"></asp:Label></TD>
					<TD class="ugfilatablaHist">
						<asp:Label id="lblOrgBD" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD align="center" colSpan="2"><INPUT class="boton" id="cmdCerrar" onclick="window.close()" type="button" value="DCerrar"
							name="cmdCerrar" runat="server"></TD>
					<TD></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</html>
