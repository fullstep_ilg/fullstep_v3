<%@ Register TagPrefix="ignav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="uon.ascx.vb" Inherits="Fullstep.PMPortalWeb.uon" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<SCRIPT type="text/javascript"><!--

		function uwtreeUons_AfterNodeSelectionChange(treeId, nodeId){
			var oNode = igtree_getNodeById(nodeId)
			oTabs = igtab_getTabById("uwtPresupuestos")
			oTabs.Tabs["pres"].setVisible((oNode.getTag()==1))
			
			
		}
--></SCRIPT>
<ignav:UltraWebTree CssClass="igTreeEnTab" id="uwtreeUons" runat="server" Width="656px" Height="272px">
	<SelectedNodeStyle ForeColor="White" BackColor="Navy"></SelectedNodeStyle>
	<NodeStyle Height="15px"></NodeStyle>
	<Levels>
		<ignav:Level Index="0"></ignav:Level>
		<ignav:Level Index="1"></ignav:Level>
	</Levels>
	<ClientSideEvents AfterNodeSelectionChange="uwtreeUons_AfterNodeSelectionChange"></ClientSideEvents>
</ignav:UltraWebTree>
