Namespace Fullstep.PMPortalWeb

    Partial Class BuscadorArticuloserver
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Dim sArt As String = String.Empty
            Dim ArtfsEntry As String = String.Empty
            Dim DenfsEntry As String = String.Empty
            Dim MatfsEntry As String = String.Empty
            Dim oArts As Fullstep.PMPortalServer.Articulos
            Dim arrMat(4) As String
            Dim sDen As String
            Dim i As Byte
            Dim lLongCod As Long
            Dim sMat As String = String.Empty
            Dim desde As String = String.Empty
            Dim bGenerico As Boolean
            Dim bExiste As Boolean
            Dim msIdi As String = String.Empty
            Dim sGMN1 As String = String.Empty
            Dim sGMN2 As String = String.Empty
            Dim sGMN3 As String = String.Empty
            Dim sGMN4 As String = String.Empty
            Dim sDenGMN As String = String.Empty
            Dim sUnidad As String = String.Empty
            Dim sDenUnidad As String = String.Empty
            Dim sDenOrgCompras As String = String.Empty
            Dim sDenCentro As String = String.Empty
            Dim lCiaComp As Long = IdCiaComp

            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSPMServer.Get_Dictionary()

            Dim sPREC_Ult_Adj As String = String.Empty
            Dim sCODPROV_Ult_Adj As String = String.Empty
            Dim sDENPROV_Ult_Adj As String = String.Empty
            Dim PRECfsEntry As String = String.Empty
            Dim PROVfsEntry As String = String.Empty
            Dim UNIfsEntry As String = String.Empty
            Dim OrgfsEntry As String = String.Empty
            Dim CentrofsEntry As String = String.Empty
            Dim sInstanciaMoneda As String = "EUR"

            msIdi = FSPMUser.Idioma

            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.BusquedaNuevoCodArticulos, msIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            sMat = Request("idMaterial")
            sArt = Request("idArt")
            ArtfsEntry = Request("fsEntryArt")
            DenfsEntry = Request("fsEntryDen")

            If Not Request("desde") Is Nothing Then
                desde = Request("desde")
            End If
            If Not Request("fsEntryMat") Is Nothing Then
                MatfsEntry = Request("fsEntryMat")
            End If

            If Not Request("fsEntryPrec") Is Nothing And Request("fsEntryPrec") <> "" Then
                PRECfsEntry = Request("fsEntryPrec")
            End If
            If Not Request("fsEntryProv") Is Nothing And Request("fsEntryProv") <> "" Then
                PROVfsEntry = Request("fsEntryProv")
            End If
            If Not Request("fsEntryUni") Is Nothing And Request("fsEntryUni") <> "" Then
                UNIfsEntry = Request("fsEntryUni")
            End If
            If Not Request("fsEntryOrg") Is Nothing And Request("fsEntryOrg") <> "" Then
                OrgfsEntry = Request("fsEntryOrg")
            End If
            If Not Request("fsEntryCentro") Is Nothing And Request("fsEntryCentro") <> "" Then
                CentrofsEntry = Request("fsEntryCentro")
            End If
            Dim bCargarUltAdj As Boolean
            If PRECfsEntry <> "" Or PROVfsEntry <> "" Then
                bCargarUltAdj = True
                If Request("InstanciaMoneda") <> "" Then
                    sInstanciaMoneda = Request("InstanciaMoneda")
                End If
            End If

            Dim bUsar_OrgCompras As Boolean
            Dim sCodOrgCompras As String = Nothing
            Dim sCodCentro As String = Nothing

            bUsar_OrgCompras = Not (Request("CodOrgCompras") = "" Or Request("CodCentro") = "") 'Usar Organizacion de compras

            If bUsar_OrgCompras Then
                If Request("CodOrgCompras") <> "" Then  'Valor Inicial
                    sCodOrgCompras = IIf(Request("CodOrgCompras") = "-1", "", Request("CodOrgCompras"))
                End If

                If Request("CodCentro") <> "" Then
                    sCodCentro = IIf(Request("CodCentro") = "-1", "", Request("CodCentro"))
                End If
            End If

            oArts = FSPMServer.Get_Articulos
            Dim sMat2, sMat3 As String
            sMat3 = Replace(sMat, " ", "")
            If desde = "MATERIAL" Then

                For i = 1 To 4
                    arrMat(i) = ""
                Next i

                i = 1
                While Trim(sMat) <> ""
                    Select Case i
                        Case 1
                            lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
                        Case 2
                            lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
                        Case 3
                            lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
                        Case 4
                            lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN4
                    End Select
                    arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
                    sMat = Mid(sMat, lLongCod + 1)
                    i = i + 1
                End While

            End If


            oArts.LoadData(lCiaComp, sArt, , True, , , , msIdi, sInstanciaMoneda, bCargarUltAdj, sCodOrgCompras, sCodCentro, , , bUsar_OrgCompras)

            If oArts.Data.Tables(0).Rows.Count > 0 Then
                sDen = oArts.Data.Tables(0).Rows(0).Item("DEN")
                bGenerico = CBool(oArts.Data.Tables(0).Rows(0).Item("GENERICO"))
                sGMN1 = oArts.Data.Tables(0).Rows(0).Item("GMN1")
                sGMN2 = oArts.Data.Tables(0).Rows(0).Item("GMN2")
                sGMN3 = oArts.Data.Tables(0).Rows(0).Item("GMN3")
                sGMN4 = oArts.Data.Tables(0).Rows(0).Item("GMN4")
                sDenGMN = oArts.Data.Tables(0).Rows(0).Item("DENGMN")
                sUnidad = oArts.Data.Tables(0).Rows(0).Item("UNI")
                sDenUnidad = oArts.Data.Tables(0).Rows(0).Item("DENUNI")

                If bUsar_OrgCompras Then
                    If oArts.Data.Tables(0).Rows.Count > 1 Then
                        '1 art N org
                        sCodOrgCompras = ""
                        sDenOrgCompras = ""
                        sCodCentro = ""
                        sDenCentro = ""
                    Else
                        sCodOrgCompras = oArts.Data.Tables(0).Rows(0).Item("CODORGCOMPRAS")
                        sDenOrgCompras = oArts.Data.Tables(0).Rows(0).Item("DENORGCOMPRAS")
                        sCodCentro = oArts.Data.Tables(0).Rows(0).Item("CODCENTRO")
                        sDenCentro = oArts.Data.Tables(0).Rows(0).Item("DENCENTRO")
                    End If
                End If

                If bCargarUltAdj Then
                    sPREC_Ult_Adj = DBNullToStr(oArts.Data.Tables(0).Rows(0).Item("PREC_ULT_ADJ"))
                    sCODPROV_Ult_Adj = DBNullToStr(oArts.Data.Tables(0).Rows(0).Item("CODPROV_ULT_ADJ"))
                    sDENPROV_Ult_Adj = DBNullToStr(oArts.Data.Tables(0).Rows(0).Item("DENPROV_ULT_ADJ"))
                End If

                bExiste = True
            Else
                bExiste = False

            End If

            sMat2 = sGMN1 & sGMN2 & sGMN3 & sGMN4


            If desde = "ARTICULO" Then
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>ponerCodigoArticulo('" & ArtfsEntry & "','" & DenfsEntry & "'," & IIf(bGenerico, "true", "false") & "," & IIf(bExiste, "true", "false") & ")</script>")
                If Not Page.ClientScript.IsClientScriptBlockRegistered("MensajeCOD1") Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "MensajeCOD1", "<script>var sMensajeCOD = '" & oTextos.Rows.Item(0).Item(1) & "'</script>")
                End If
            Else
                If desde = "MATERIAL" And sMat2 <> sMat3 And bExiste Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>ponerCodArt_Den_Blanco('" & ArtfsEntry & "','" & DenfsEntry & "')</script>")
                Else

                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>rellenarCampos('" & ArtfsEntry & "','" & DenfsEntry & "','" & Replace(sDen, "\", "\\") & "'," & IIf(bExiste, "true", "false") & ",'" & PRECfsEntry & "','" & sPREC_Ult_Adj & "','" & PROVfsEntry & "','" & sCODPROV_Ult_Adj & "','" & sDENPROV_Ult_Adj & "','" & UNIfsEntry & "','" & sUnidad & "','" & sDenUnidad & "'," & IIf(bGenerico, "true", "false") & ",'" & OrgfsEntry & "','" & sCodOrgCompras & "','" & sDenOrgCompras & "','" & CentrofsEntry & "','" & sCodCentro & "','" & sDenCentro & "')</script>")
                    If Not Page.ClientScript.IsClientScriptBlockRegistered("MensajeCOD2") Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "MensajeCOD2", "<script>var sMensajeCOD = '" & oTextos.Rows.Item(1).Item(1) & "'</script>")
                    End If
                    If bExiste And MatfsEntry <> "" Then
                        If Not Page.ClientScript.IsClientScriptBlockRegistered("GMNs") Then
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "GMN1", "<script>var ilGMN1 = " & FSPMServer.LongitudesDeCodigos.giLongCodGMN1 & "</script>")
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "GMN2", "<script>var ilGMN2 = " & FSPMServer.LongitudesDeCodigos.giLongCodGMN2 & "</script>")
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "GMN3", "<script>var ilGMN3 = " & FSPMServer.LongitudesDeCodigos.giLongCodGMN3 & "</script>")
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "GMN4", "<script>var ilGMN4 = " & FSPMServer.LongitudesDeCodigos.giLongCodGMN4 & "</script>")
                        End If
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript2", "<script>ponerMaterial('" & MatfsEntry & "','" & sGMN1 & "','" & sGMN2 & "','" & sGMN3 & "','" & sGMN4 & "','" _
                                                                & sDenGMN & "','" & IIf(CType(Me.Page, FSPMPage).Acceso.gbMaterialVerTodosNiveles, 1, 0) & "')</script>")
                    End If
                End If
            End If
        End Sub
    End Class

End Namespace
