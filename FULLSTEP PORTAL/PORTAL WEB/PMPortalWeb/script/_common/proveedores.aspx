<%@ Page Language="vb" AutoEventWireup="false" Codebehind="proveedores.aspx.vb" Inherits="Fullstep.PMPortalWeb.proveedores" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
		<title>proveedores</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <style type="text/css">
            #whdgProveedores .igg_FullstepControl
            {
                border-width:0px 1px 1px 1px;
            }
        </style>
		<SCRIPT type="text/javascript">

        /*
        ''' <summary>
        ''' Devuelve a la pantalla q llamo a _common/provedores el proveedor seleccionado y cierra esta pantalla.
        ''' </summary>
        ''' <param name="gridName">parametro de sistema</param>
        ''' <param name="cellId">parametro de sistema</param>            
        '''	<param name="button">parametro de sistema</param> 
        ''' <returns>Nada</returns>
        ''' <remarks>Llamada desde: Sistema ;Tiempo m�ximo: 0</remarks>
        */
		function whdgProveedores_RowSelectionChanged(sender, e) {
		    var row = e.getSelectedRows().getItem(0);

		    var cell = row.get_cellByColumnKey("COD"); 
            var sProveCod= cell.get_value()
            cell = row.get_cellByColumnKey("DEN");
            var sProveDen = cell.get_value()
            cell = row.get_cellByColumnKey("NIF");
            var sCIF = cell.get_value()
            if (document.all("IDCONTROL") == "")
            {
                window.opener.prove_seleccionado(sCIF, sProveCod, sProveDen)
            }
            else 
            {
                window.opener.prove_seleccionado(document.all("IDCONTROL").value, sProveCod, sProveDen)
            }

            var sCod = document.all("VALORORGCOMPRAS").value

            var sDen = document.all("DENORGCOMPRAS").value
            var sDepen = document.all("IDDEPEN").value

            if ((document.all("IDDEPEN").value != "null") && (document.all("IDDEPEN").value != "oculto"))
				{
					if (window.opener.opener)
					{
						if ((window.opener.opener.sCodOrgComprasFORM) || (window.opener.opener.sDataEntryOrgComprasFORM))
						{
							window.opener.opener.cambiarOrganizacionCompras(sDepen,sCod,sDen)
						}
						else
						{
							window.opener.cambiarOrganizacionCompras(sDepen,sCod,sDen)
						}
					}
					else
					{
						window.opener.cambiarOrganizacionCompras(sDepen,sCod,sDen)
					}
				}			
			window.close()
			
		}
    </SCRIPT>
	</head>
	<body MS_POSITIONING="GridLayout">
    <script src="<%=ConfigurationManager.AppSettings("ruta")%>js/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="<%=ConfigurationManager.AppSettings("ruta")%>js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                /*Funciones para paginacion*/

                function AdministrarPaginador() {
                    var grid = $find("<%= whdgProveedores.ClientID %>");
                    var parentGrid = grid;
                    $('[id$=lblCount]').text(parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount());
                    if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() == 0) {
                        $('[id$=ImgBtnFirst]').attr('disabled', 'disabled');
                        $('[id$=ImgBtnFirst]').attr('src', rutaTheme + '/images/primero_desactivado.gif');
                        $('[id$=ImgBtnPrev]').attr('disabled', 'disabled');
                        $('[id$=ImgBtnPrev]').attr('src', rutaTheme + '/images/anterior_desactivado.gif');
                    } else {
                        $('[id$=ImgBtnFirst]').removeAttr('disabled');
                        $('[id$=ImgBtnFirst]').attr('src', rutaTheme + '/images/primero.gif');
                        $('[id$=ImgBtnPrev]').removeAttr('disabled');
                        $('[id$=ImgBtnPrev]').attr('src', rutaTheme + '/images/anterior.gif');
                    }
                    if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() == parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount() - 1) {
                        $('[id$=ImgBtnNext]').attr('disabled', 'disabled');
                        $('[id$=ImgBtnNext]').attr('src', rutaTheme + '/images/siguiente_desactivado.gif');
                        $('[id$=ImgBtnLast]').attr('disabled', 'disabled');
                        $('[id$=ImgBtnLast]').attr('src', rutaTheme + '/images/ultimo_desactivado.gif');
                    } else {
                        $('[id$=ImgBtnNext]').removeAttr('disabled');
                        $('[id$=ImgBtnNext]').attr('src', rutaTheme + '/images/siguiente.gif');
                        $('[id$=ImgBtnLast]').removeAttr('disabled');
                        $('[id$=ImgBtnLast]').attr('src', rutaTheme + '/images/ultimo.gif');
                    }
                }
                function whdgProveedores_PageIndexChanged(a, b, c, d) {
                    var grid = $find("<%= whdgProveedores.ClientID %>");
                    var parentGrid = grid;
                    var dropdownlist = $('[id$=PagerPageList]')[0];

                    dropdownlist.options[parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex()].selected = true;
                    AdministrarPaginador();
                }

                function IndexChanged() {
                    var dropdownlist = $('[id$=PagerPageList]')[0];
                    var grid = $find("<%= whdgProveedores.ClientID %>");
                    var parentGrid = grid.get_gridView();
                    var newValue = dropdownlist.selectedIndex;
                    parentGrid.get_behaviors().get_paging().set_pageIndex(newValue);
                    AdministrarPaginador();
                }
                function FirstPage() {
                    var grid = $find("<%= whdgProveedores.ClientID %>");
                    var parentGrid = grid;
                    parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(0);
                }
                function PrevPage() {
                    var grid = $find("<%= whdgProveedores.ClientID %>");
                    var parentGrid = grid;
                    var dropdownlist = $('[id$=PagerPageList]')[0];
                    if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() > 0) {
                        parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() - 1);
                    }
                }
                function NextPage() {
                    var grid = $find("<%= whdgProveedores.ClientID %>");
                    var parentGrid = grid;
                    var dropdownlist = $('[id$=PagerPageList]')[0];
                    if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() < parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount() - 1) {
                        parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() + 1);
                    }
                }
                function LastPage() {
                    var grid = $find("<%= whdgProveedores.ClientID %>");
                    var parentGrid = grid;
                    parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount() - 1);
                }
    </script>
		<form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="scrMng_presAsig" runat="server"></asp:ScriptManager>
			<asp:label id="lbltitulo" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server"
				CssClass="subtitulo">B&uacute;squeda de proveedor</asp:label>
			<TABLE id="Table1" style="Z-INDEX: 112; LEFT: 176px; POSITION: absolute; TOP: 40px" cellSpacing="0"
				cellPadding="0" width="300" align="center" border="0">
				<TR>
					<TD style="HEIGHT: 40px"><asp:label id="lblOrganizacionCompras" runat="server" CssClass="parrafo">Org Compras</asp:label></TD>
					<TD style="HEIGHT: 40px"> <ig:WebDropDown ID="ugtxtOrganizacionCompras" runat="server" Width="200px"
                DropDownContainerHeight="200px" DropDownContainerWidth="" DropDownAnimationType="EaseOut" 
                EnablePaging="False" PageSize="12">
                </ig:WebDropDown><asp:label id="lblOrganizacionComprasReadOnly" runat="server" CssClass="parrafo" Width="304px"></asp:label></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblCod" runat="server" CssClass="parrafo">C&oacute;digo</asp:label></TD>
					<TD><igtxt:webtextedit id="ugtxtCod" runat="server"></igtxt:webtextedit></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblDen" runat="server" CssClass="parrafo">Denominaci&oacute;n</asp:label></TD>
					<TD><igtxt:webtextedit id="ugtxtDen" runat="server"></igtxt:webtextedit></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblCIF" runat="server" CssClass="parrafo">CIF</asp:label></TD>
					<TD><igtxt:webtextedit id="ugtxtCIF" runat="server"></igtxt:webtextedit></TD>
				</TR>
			</TABLE>
			<asp:label id="Label2" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 152px" runat="server"
				CssClass="parrafo">Resultado de la b&uacute;squeda:</asp:label>
			<HR style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 176px" width="100%" color="gainsboro"
				noShade SIZE="4">
			<TABLE id="Table2" style="Z-INDEX: 105; LEFT: 624px; POSITION: absolute; TOP: 56px; HEIGHT: 27px"
				cellSpacing="0" cellPadding="0" width="88" border="0">
				<TR>
					<TD>
						<asp:LinkButton id="hypBuscar" runat="server" CssClass="parrafo">Buscar</asp:LinkButton></TD>
					<TD>
						<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="images/buscar.gif"></asp:ImageButton></TD>
				</TR>
			</TABLE>
			<asp:Label id="Label1" style="Z-INDEX: 106; LEFT: 8px; POSITION: absolute; TOP: 192px" runat="server"
				CssClass="parrafo">Pulse sobre el proveedor para seleccionarlo o haga una nueva b&uacute;squeda</asp:Label>
			<ig:WebHierarchicalDataGrid id="whdgProveedores" style="Z-INDEX: 107; POSITION: absolute; TOP: 224px" runat="server" Width="98%" Height="240px"
                AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="true">					
					<Behaviors>
                        <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Row" >
                            <SelectionClientEvents RowSelectionChanged= "whdgProveedores_RowSelectionChanged" />
                        </ig:Selection> 
                        <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true"  AnimationEnabled="true">
                            <FilteringClientEvents DataFiltered="AdministrarPaginador" /> 
                        </ig:Filtering> 
                        <ig:RowSelectors Enabled="false" RowNumbering="false"></ig:RowSelectors>
                        <ig:Paging Enabled="true" PagerAppearance="Top" PageSize="20">
                            <PagingClientEvents PageIndexChanged="whdgProveedores_PageIndexChanged" />
                            <PagerTemplate>
							    <div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
							        <div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
							            <div style="clear: both; float: left; margin-right: 5px;">
                                            <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-right:2px;" />
                                            <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" />
                                        </div>
                                        <div style="float: left; margin-right:5px;">
                                            <asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
                                            <asp:DropDownList ID="PagerPageList" runat="server" onchange="return IndexChanged()" Style="margin-right: 3px;" />
                                            <asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
                                            <asp:Label ID="lblCount" runat="server" />
                                        </div>
                                        <div style="float: left;">
                                            <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px;" />
                                            <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" />
                                        </div>
								    </div>   
                                 </div>  
                              </PagerTemplate>
                          </ig:Paging>
                          <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                    </Behaviors>					
			</ig:WebHierarchicalDataGrid>
			<INPUT runat="server" id="IDCAMPO" type="hidden" name="IDCAMPO"> <INPUT runat="server" id="IDCONTROL" type="hidden" name="IDCAMPO">
			<INPUT id="VISIBILIDAD" type="hidden" name="VISIBILIDAD" runat="server"> <INPUT id="VALORORGCOMPRAS" type="hidden" name="VALORORGCOMPRAS" runat="server">
			<INPUT id="IDDEPEN" type="hidden" name="IDDEPEN" runat="server"> <INPUT id="USAR_ORGCOMPRAS" type="hidden" name="USAR_ORGCOMPRAS" runat="server">
			<INPUT id="DENORGCOMPRAS" type="hidden" name="DENORGCOMPRAS" runat="server">
		</form>
	</body>
</html>
