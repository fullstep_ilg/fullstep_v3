﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorUsuarios.aspx.vb" Inherits=".BuscadorUsuarios" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <link href="../../../common/estilo.asp" type="text/css" rel="stylesheet"/>
<SCRIPT>
    //''' <summary>
    //''' Bankia RCM-907796 / Información Confidencial o interna en los mensajes ofrecidos por la aplicación. Si en el navegador pones la ruta te carga la pantalla con usuarios.
    //''' </summary>
    //''' <remarks>Llamada desde: este script ; Tiempo máximo: 0</remarks>
	function CtrlApertura() {
		if (window.opener) {
		} else {
		    document.location.href = "../../default.aspx";
		}
	}
	CtrlApertura();
</SCRIPT>
</head>
<body>
    <form id="form1" runat="server">
    <script type="text/javascript">
        </script>
    <asp:ScriptManager ID="scm1" runat="server"></asp:ScriptManager>
    <div>
        <fspm:BusquedaUsuarios id="Usuarios" runat="server"></fspm:BusquedaUsuarios>
        <input id="FilaGrid" type="hidden"	name="FilaGrid" runat="server"/>
        <input id="IDCONTROL" style="Z-INDEX: 109;" type="hidden" name="IDCONTROL" runat="server"/>
    </div>
    </form>
</body>
</html>
