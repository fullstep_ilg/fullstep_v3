Namespace Fullstep.PMPortalWeb

	Partial Class impexp_sel
		Inherits FSPMPage
		Private oUser As Fullstep.PMPortalServer.User
		Public Titulo As String

#Region " Web Form Designer Generated Code "
		'This call is required by the Web Form Designer.
		<System.Diagnostics.DebuggerStepThrough()>
		Private Sub InitializeComponent()
		End Sub

		'NOTE: The following placeholder declaration is required by the Web Form Designer.
		'Do not delete or move it.
		Private designerPlaceholderDeclaration As System.Object

		Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
			'CODEGEN: This method call is required by the Web Form Designer
			'Do not modify it using the code editor.
			InitializeComponent()
		End Sub
#End Region
		''' <summary>
		''' Visualizaci�n de la pantalla de elecci�n del tipo (PDF/HTML/XML) al que queremos exportar la noconformidad seleccionada
		''' </summary>
		''' <param name="sender">Propio del evento</param>
		''' <param name="e">Propio del evento</param>
		''' <remarks>Ventana de selecci�n del tipo (impexp_sel.aspx)
		''' Llamada desde el boton "Impr./Exp." de la pagina Noconformidad.aspx; Tiempo m�ximo = 0.0 seg</remarks>
		Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
			Instancia.Value = Request("Instancia")
			Certificado.Value = Request("Certificado")
			NoConformidad.Value = Request("NoConformidad")
			Version.Value = Request("Version")
			TipoImpExp.Value = Request("TipoImpExp")
			Observadores.Value = Request("Observadores")
			oUser = Session("FS_Portal_User")
			'Idioma
			Dim sIdi As String = oUser.Idioma
			If sIdi = Nothing Then sIdi = ConfigurationManager.AppSettings("idioma")

			'Textos
			Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
			Dim oDict As Fullstep.PMPortalServer.Dictionary = FSWSServer.Get_Dictionary()
			oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.NoConformidades, sIdi)
			Dim oTextos As DataTable = oDict.Data.Tables(0)

			chkFormato.Text = oTextos.Rows(23).Item("TEXT_" & sIdi)

			oTextos = Nothing
			oDict = Nothing
			FSWSServer = Nothing
		End Sub
	End Class
End Namespace