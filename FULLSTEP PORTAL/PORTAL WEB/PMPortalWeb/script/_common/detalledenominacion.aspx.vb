Partial Class detalledenominacion
    Inherits FSPMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
"<script language=""{0}"">{1}</script>"

    ''' <summary>
    ''' Cargar la pagina. 
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>        
    ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DetalleDenominacionPresupuestos

        Dim cadenatitulo = Textos(0)
        Dim sScriptTitulo

        sScriptTitulo = "var cadenatitulo = '" + cadenatitulo + "';"
        sScriptTitulo = String.Format(IncludeScriptKeyFormat, "javascript", sScriptTitulo)
        ClientScript.RegisterStartupScript(Page.GetType(), "scripttitulo", sScriptTitulo)

        Me.lblCaso1.Text = Textos(1)
        Me.lblDescripcionCaso1.Text = Textos(2)
        Me.lblCaso2.Text = Textos(3)
        Me.lblDescripcionCaso2.Text = Textos(4)
        Me.lblCaso3.Text = Textos(5)
        Me.lblDescripcionCaso3.Text = Textos(6)
    End Sub

End Class
