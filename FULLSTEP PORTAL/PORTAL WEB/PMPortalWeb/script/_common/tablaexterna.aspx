<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tablaexterna.aspx.vb" Inherits="Fullstep.PMPortalWeb.tablaexterna" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<title>
			<%=_msTitulo%>
		</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script type="text/javascript">
		
		function cerrarvent(){
			try{
				window.opener.wext=null;
			}
			catch(e){
			}
		}
		
		if (window.addEventListener) {
		    window.document.addEventListener("close", cerrarvent, false)
		} else {
		    if (window.attachEvent)
		        window.attachEvent("onclose", cerrarvent);
		}
		
		</script>
	</head>
	<body  MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<br>
			<igtbl:ultrawebgrid id="uwgTabla" runat="server" Height="380px" Width="660px">
				<DisplayLayout AllowSortingDefault="OnClient" RowHeightDefault="20px" Version="4.00" HeaderClickActionDefault="SortSingle"
					BorderCollapseDefault="Separate" AllowColSizingDefault="Free" Name="uwgTabla" CellClickActionDefault="RowSelect">
					<AddNewBox>                                                
						<igtbl:Style  BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray" >                                                
                            <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                            </BorderDetails>                        
						</igtbl:Style>
					</AddNewBox>
					<Pager>
						<igtbl:Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                            <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                            </BorderDetails>
						</igtbl:Style>
					</Pager>
					<FrameStyle Width="660px" BorderWidth="1px" Font-Size="8pt" Font-Names="Verdana" BorderColor="Black"
						BorderStyle="Solid" Height="380px"></FrameStyle>
					<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
						<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
					</FooterStyleDefault>
					<EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
					<RowStyleDefault BorderWidth="1px" BorderColor="Gray" BorderStyle="Solid">
						<Padding Left="3px"></Padding>
						<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
					</RowStyleDefault>
				</DisplayLayout>
				<Bands>
					<igtbl:UltraGridBand ColHeadersVisible="Yes" SelectTypeRow="Single" CellClickAction="RowSelect" RowSelectors="No">
						<HeaderStyle CssClass="cabecera"></HeaderStyle>
						<RowStyle Cursor="Hand"></RowStyle>
						<SelectedRowStyle ForeColor="White" BackColor="DarkBlue"></SelectedRowStyle>
					</igtbl:UltraGridBand>
				</Bands>
			</igtbl:ultrawebgrid>
			<br>
			<div style="text-align:center;">
				<asp:Button id="cmdAceptar" runat="server" Text="nAceptar" CssClass="boton"></asp:Button>
			</div>
			<input type="hidden" id="hddDataEntry" name="hddDataEntry" runat="server">
		</form>
	</body>
</html>
