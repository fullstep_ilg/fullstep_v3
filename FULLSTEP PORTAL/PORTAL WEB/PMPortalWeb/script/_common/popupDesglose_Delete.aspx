<%@ Page Language="vb" AutoEventWireup="false" Codebehind="popupDesglose_Delete.aspx.vb" Inherits="popupDesglose_Delete"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
		<title>popupDesglose_Delete</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<body>
        <script src="../_common/js/jsAlta.js?v=<%=ConfigurationManager.AppSettings("versionJs")%>" type="text/javascript"></script>
		<script type="text/javascript">
	  /*''' <summary>
		''' Elimina una linea de la pantalla desglose "padre"
		''' </summary>
		''' <remarks>Llamada desde: desglose.ascx; Tiempo m�ximo:0</remarks>*/
		function quitar(){			
			var p = window.parent
			var a =   "" + "<%=Request("celda")%>"
			var b =  "" + "<%=Request("ClientID")%>"
			var c = "" + "<%=Request("index")%>"		
			var d = "" + "<%=Request("idCampo")%>"
			p.deleteRow(p.document.getElementById(a).parentElement,b,c, d)		
		    var oFrame = p.document.all("<%=Request("FrameId")%>")
			oFrame.parentNode.removeChild(oFrame);
		}
		</script>

		<form id="Form1" method="post" runat="server">
			<TABLE cellpadding="0" cellspacing="0" class="cabeceraDesglose" id="Table2" style="Z-INDEX: 100; LEFT: 0px; WIDTH: 136px; BORDER-RIGHT-STYLE: double; BORDER-LEFT-STYLE: double; POSITION: absolute; TOP: 0px; HEIGHT: 8px; BORDER-BOTTOM-STYLE: double"
				border="0">
				<TR valign="bottom">
					<TD>
						<A id="cmdimgquitar" style="CURSOR: hand" onclick="javascript:quitar()" name="imgquitar">
							<IMG src="../_common/images/EliminarRegistro.gif" border="0" height="16" width="16"></A>
					</TD>
					<TD>
						<asp:LinkButton style="TEXT-DECORATION:none" CssClass="cabeceraDesglose" id="hypquitar" runat="server">Eliminar l�nea</asp:LinkButton></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</html>
