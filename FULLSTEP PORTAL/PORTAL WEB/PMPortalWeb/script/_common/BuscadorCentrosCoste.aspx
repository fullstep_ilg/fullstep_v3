﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorCentrosCoste.aspx.vb" Inherits=".BuscadorCentrosCoste" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <link href="../../../common/estilo.asp" type="text/css" rel="stylesheet"/>
    <script type="text/javascript" language="javascript">
        function seleccionarCentroCoste() {

            var oTree = $find("CentrosCoste_wdtCentros_Coste");

            var oNode = oTree.get_selectedNodes()[0]
            if (oNode == null) {
                alert(arrTextosML[0])
                return false
            }
            if (oNode.get_target() == null) {
                alert(arrTextosML[0])
                return false
            }

            var sDen = oNode.get_text();

            var sUON = new Array()

            var sUON0;
            var sUON1;
            var sUON2;
            var sUON3;
            var sUON4;

            var iNivel = -1
            while (oNode) {
                iNivel++;
                sUON[iNivel] = oNode.get_key()
                oNode = oNode.get_parentNode()
            }
            var j = 0;
            for (i = iNivel; i > 0; i--) {
                eval("sUON" + i.toString() + "=sUON[" + j.toString() + "]");
                j++;
            }
            var idControl = document.getElementById("idControl")
            var idHidControl = document.getElementById("idHidControl")
            window.opener.CentroCoste_seleccionado(idControl.value, idHidControl.value, sUON1, sUON2, sUON3, sUON4, sDen)
            window.close()

        }
</script>
</head>
<body>
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="scm1" runat="server"></asp:ScriptManager>
    <div>
        <fspm:BusquedaCentrosCoste id="CentrosCoste" runat="server"></fspm:BusquedaCentrosCoste>
        <input type="hidden" id="idControl" runat="server" />
        <input type="hidden" id="idHidControl" runat="server" />
    </div>
    </form>
</body>
</html>
