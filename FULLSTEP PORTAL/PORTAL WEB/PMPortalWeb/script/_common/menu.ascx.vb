Imports System.Security.Policy.Url

Namespace Fullstep.PMPortalWeb
    Partial Class MenuControl
        Inherits System.Web.UI.UserControl

        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script language=""{0}"">{1}</script>"

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")


            Dim oUrl As System.Security.Policy.Url

            Dim sIdi As String = oUser.Idioma
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If

            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSPMServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Menu)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            Dim oItem As Infragistics.WebUI.UltraWebNavigator.Item

            Me.uwmWS.Items.Clear()
            oItem = New Infragistics.WebUI.UltraWebNavigator.Item
            oItem.Text = oTextos.Rows(0).Item(1)
            oItem.TargetUrl = ConfigurationManager.AppSettings("rutanormal") & "script/login/" & Me.Page.Theme & "/" & sIdi & "/inicio.aspx"
            Me.uwmWS.Items.Add(oItem)
            oItem = Nothing
            oTextos = Nothing

            Dim sScript As String

            sScript = "var vDateFmt='" + oUser.DateFormat.ShortDatePattern + "'"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "variablesFormatoUsuario", sScript)
        End Sub

    End Class
End Namespace