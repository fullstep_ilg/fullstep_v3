﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="atachedfilesDesglose.aspx.vb" Inherits=".atachedfilesDesglose" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css" >
    .CapaTituloPanelInfo
    {
	    background-color:#282828;
    }
    .CapaSubTituloPanelInfo
    {
	    background-color:#424242;	 
    }
    .TituloPopUpPanelInfo
    {
	    font-family:Arial;
	    font-size:16px;
	    font-weight:bold;
	    color:#FFFFFF;
    }
    .SubTituloPopUpPanelInfo
    {
	    font-family:Arial;
	    font-size:12px;
	    font-weight:bold;
	    color:#FFFFFF;
    }
    </style>
    <style type="text/css" >@import url(../styles/<%#Me.Page.Theme%>.css ); 
	</style>
</head>
<body style="background-color: #FFFFFF;margin-top: 0px; margin-left: 0px" onload="window.focus();">
    <script src="../_common/js/jsAlta.js?v=<%=ConfigurationManager.AppSettings("versionJs")%>" type="text/javascript"></script>
    <form id="form1" runat="server">
    <div style='width:100%;z-index:10000; position:absolute;'>
    <div style='width:100%; height:35px;' class='CapaTituloPanelInfo'>&nbsp;</div>
    <div style='width:100%; height:25px;' class='CapaSubTituloPanelInfo'>&nbsp;</div></div>
    
    <table cellspacing="0" cellpadding="2" width="100%" border="0" style="z-index:10002; position:absolute;">
	    <tr style="height:35px;">
	        <td width="1%" style="background-color: Transparent;" rowspan="2"><asp:Image ID="imgAdjunto" runat="server" ImageUrl="images/adjuntos.gif"  /></td>
	        <td align="left"><asp:label id="lblTitulo" runat="server" CssClass="TituloPopUpPanelInfo"></asp:label></td>
	        <td id="cellImagenCerrar" align="right" valign="top" style="width:1%"></td>
        </tr>	    
	    <tr style="height:25px;">
	       <td align="left" valign="top"><asp:label id="lblSubtitulo" runat="server" CssClass="SubTituloPopUpPanelInfo"></asp:label></td>
	       <td></td>
	    </tr>
	    <tr>
	        <td colspan="3">
	            <asp:Table runat="server" id="tblcontenido" Width ="100%">
	            </asp:Table> 
	        </td>
	    </tr>
	</table>
    </form>
</body>
</html>
