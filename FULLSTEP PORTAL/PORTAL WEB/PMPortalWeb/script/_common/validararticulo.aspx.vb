Namespace Fullstep.PMPortalWeb

    Partial Class validararticulo
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & _
        "<script language=""{0}"">{1}</script>"
            Dim lCiaComp As Long = IdCiaComp

            Dim sCodArt As String = Request("codart")
            Dim sDenArt As String
            Dim sDenGMN As String
            Dim sUnidad, sDenUnidad As String

            Dim sMat As String = Request("codmat")
            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
            Dim i As Integer
            Dim sGMN1 As String
            Dim sGMN2 As String
            Dim sGMN3 As String
            Dim sGMN4 As String
            Dim iDesdeFila As Integer
            Dim sMatClientId As String = Request("MatClientId")
            Dim sArtClientId As String = Request("ArtClientId")

            Dim sRestric As String = Request("restric")
            Dim oArts As Fullstep.PMPortalServer.Articulos
            Dim arrMat(4) As String
            Dim arrRestric(4) As String
            Dim sIdi As String = oUser.Idioma
            Dim oDict As Fullstep.PMportalServer.Dictionary = FSWSServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.ValidarArticulo, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            Dim sPREC_Ult_Adj, sCODPROV_Ult_Adj, sDENPROV_Ult_Adj As String
            Dim PRECfsEntry, PROVfsEntry, UNIfsEntry As String
            Dim sInstanciaMoneda As String = "EUR"

            If Not Request("fsEntryPrec") Is Nothing And Request("fsEntryPrec") <> "" Then
                PRECfsEntry = Request("fsEntryPrec")
            End If
            If Not Request("fsEntryProv") Is Nothing And Request("fsEntryProv") <> "" Then
                PROVfsEntry = Request("fsEntryProv")
            End If
            If Not Request("fsEntryUni") Is Nothing And Request("fsEntryUni") <> "" Then
                UNIfsEntry = Request("fsEntryUni")
            End If

            Dim bCargarUltAdj As Boolean
            If PRECfsEntry <> "" Or PROVfsEntry <> "" Then
                bCargarUltAdj = True
                If Request("InstanciaMoneda") <> "" Then
                    sInstanciaMoneda = Request("InstanciaMoneda")
                End If
            End If

            Dim sClientTexts As String
            sClientTexts = ""
            sClientTexts += "var arrTextosML = new Array();"
            sClientTexts += "arrTextosML[0] = '" + JSText(oTextos.Rows(0).Item(1)) + "';"
            sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TodosNiveles", "<script>var TodosNiveles = " & IIf(CType(Me.Page, FSPMPage).Acceso.gbMaterialVerTodosNiveles, 1, 0) & ";</script>")

            Dim lLongCod As Integer

            For i = 1 To 4
                arrMat(i) = ""
                arrRestric(4) = ""
            Next i

            i = 1
            While Trim(sMat) <> ""
                Select Case i
                    Case 1
                        lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN1
                    Case 2
                        lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN2
                    Case 3
                        lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN3
                    Case 4
                        lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN4
                End Select
                arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
                sMat = Mid(sMat, lLongCod + 1)
                i = i + 1
            End While

            i = 1
            While Trim(sRestric) <> ""
                Select Case i
                    Case 1
                        lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN1
                    Case 2
                        lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN2
                    Case 3
                        lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN3
                    Case 4
                        lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN4
                End Select
                arrRestric(i) = Trim(Mid(sRestric, 1, lLongCod))
                sRestric = Mid(sRestric, lLongCod + 1)
                i = i + 1
            End While


            sGMN1 = arrRestric(1)
            sGMN2 = arrRestric(2)
            sGMN3 = arrRestric(3)
            sGMN4 = arrRestric(4)

            If sGMN1 = "" Then
                sGMN1 = arrMat(1)
            End If
            If sGMN2 = "" Then
                sGMN2 = arrMat(2)
            End If
            If sGMN3 = "" Then
                sGMN3 = arrMat(3)
            End If
            If sGMN4 = "" Then
                sGMN4 = arrMat(4)
            End If

            Dim oGMN1s As Fullstep.PMPortalServer.GruposMatNivel1
            Dim oGMN1 As Fullstep.PMPortalServer.GrupoMatNivel1
            Dim oGMN2 As Fullstep.PMPortalServer.GrupoMatNivel2
            Dim oGMN3 As Fullstep.PMPortalServer.GrupoMatNivel3
            Dim oGMN4 As Fullstep.PMPortalServer.GrupoMatNivel4
            Dim sValor As String

            If sGMN4 <> "" Then
                oGMN3 = FSWSServer.Get_GrupoMatNivel3
                oGMN3.GMN1Cod = sGMN1
                oGMN3.GMN2Cod = sGMN2
                oGMN3.Cod = sGMN3
                oGMN3.CargarTodosLosGruposMatDesde(lCiaComp, 1, sIdi, sGMN4, , True)
                sValor = oGMN3.GruposMatNivel4.Item(sGMN4).Den & " (" & sGMN1 & " - " & sGMN2 & " - " & sGMN3 & " - " & sGMN4 & ")"
                oGMN3 = Nothing

            ElseIf sGMN3 <> "" Then
                oGMN2 = FSWSServer.Get_GrupoMatNivel2
                oGMN2.GMN1Cod = sGMN1
                oGMN2.Cod = sGMN2
                oGMN2.CargarTodosLosGruposMatDesde(lCiaComp, 1, sIdi, sGMN3, , True)
                sValor = oGMN2.GruposMatNivel3.Item(sGMN3).Den & "(" & sGMN1 & " - " & sGMN2 & " - " & sGMN3 & ")"
                oGMN2 = Nothing

            ElseIf sGMN2 <> "" Then
                oGMN1 = FSWSServer.Get_GrupoMatNivel1
                oGMN1.Cod = sGMN1
                oGMN1.CargarTodosLosGruposMatDesde(lCiaComp, 1, sIdi, sGMN2, , True)
                sValor = oGMN1.GruposMatNivel2.Item(sGMN2).Den & "(" & sGMN1 & " - " & sGMN2 & ")"
                oGMN1 = Nothing

            ElseIf sGMN1 <> "" Then
                oGMN1s = FSWSServer.Get_GruposMatNivel1
                oGMN1s.CargarTodosLosGruposMatDesde(lCiaComp, 1, sIdi, sGMN1, , , True)
                sValor = oGMN1s.Item(sGMN1).Den & "(" & sGMN1 & ")"
                oGMN1s = Nothing
            End If

            oArts = FSWSServer.Get_Articulos

            oArts.LoadData(lCiaComp:=lCiaComp, sCod:=sCodArt, sper:=oUser.AprobadorActual, iDesdeFila:=iDesdeFila, iNumFilas:=1, sIdioma:=sIdi, bcoincid:=True, sMoneda:=sInstanciaMoneda, bCargarUltAdj:=bCargarUltAdj)


            Dim sScript As String

            Dim ilGMN1 As Integer = FSWSServer.LongitudesDeCodigos.giLongCodGMN1
            Dim ilGMN2 As Integer = FSWSServer.LongitudesDeCodigos.giLongCodGMN2
            Dim ilGMN3 As Integer = FSWSServer.LongitudesDeCodigos.giLongCodGMN3
            Dim ilGMN4 As Integer = FSWSServer.LongitudesDeCodigos.giLongCodGMN4
            sScript = ""
            sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
            sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
            sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
            sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
            sScript += "var sArtClientId = '" + JSText(sArtClientId) + "';"
            sScript += "var sMatClientId = '" + JSText(sMatClientId) + "';"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(),"LongitudesCodigosKey", sScript)

            If oArts.Data.Tables(0).Rows.Count = 0 Then
                'FATAL, NO EXISTE EL ART�CULO
                sScript = "articuloIncorrecto()"
                sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                Page.ClientScript.RegisterStartupScript(Me.GetType(),"errorAlValidar", sScript)

            Else
                'GENIAL, EL TIO LA HA CLAVADO

                Dim oDataRow As DataRow = oArts.Data.Tables(0).Rows(0)
                sGMN1 = oDataRow.Item("GMN1")
                sGMN2 = oDataRow.Item("GMN2")
                sGMN3 = oDataRow.Item("GMN3")
                sGMN4 = oDataRow.Item("GMN4")

                sCodArt = oDataRow.Item("COD")
                sDenArt = oDataRow.Item("DEN")
                sDenGMN = oDataRow.Item("DENGMN")
                sUnidad = oDataRow.Item("UNI")
                sDenUnidad = oDataRow.Item("DENUNI")

                If bCargarUltAdj Then
                    sPREC_Ult_Adj = DBNullToStr(oDataRow.Item("PREC_ULT_ADJ"))
                    sCODPROV_Ult_Adj = DBNullToStr(oDataRow.Item("CODPROV_ULT_ADJ"))
                    sDENPROV_Ult_Adj = DBNullToStr(oDataRow.Item("DENPROV_ULT_ADJ"))
                End If

                sScript = ""
                sScript += "var sGMN1 = '" + JSText(sGMN1) + "';"
                sScript += "var sGMN2 = '" + JSText(sGMN2) + "';"
                sScript += "var sGMN3 = '" + JSText(sGMN3) + "';"
                sScript += "var sGMN4 = '" + JSText(sGMN4) + "';"
                sScript += "var sCodArt = '" + JSText(sCodArt) + "';"
                sScript += "var sDenArt = '" + JSText(sDenArt) + "';"
                sScript += "var sDenGMN = '" + JSText(sDenGMN) + "';"
                sScript += "var sUNI = '" + JSText(sUnidad) + "';"
                sScript += "var sDenUNI = '" + JSText(sDenUnidad) + "';"
                If bCargarUltAdj Then
                    sScript += "var dPrecioUltADJ = '" + JSText(sPREC_Ult_Adj) + "';"
                    sScript += "var sCodPROV = '" + JSText(sCODPROV_Ult_Adj) + "';"
                    sScript += "var sDenPROV = '" + JSText(sDENPROV_Ult_Adj) + "';"
                End If
                sScript += "articuloValidado();"
                sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                Page.ClientScript.RegisterStartupScript(Me.GetType(),"validacionOK", sScript)

            End If


        End Sub

    End Class
End Namespace
