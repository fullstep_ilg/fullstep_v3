Namespace Fullstep.PMPortalWeb
    Partial Class detallepersona
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DetallePersona

            Me.lblCod.Text = Textos(0)
            Me.lblNombre.Text = Textos(1)
            Me.lblApe.Text = Textos(2)
            Me.lblCargo.Text = Textos(3)
            Me.lblTfno.Text = Textos(4)
            Me.lblFax.Text = Textos(5)
            Me.lblMail.Text = Textos(6)
            Me.lblDep.Text = Textos(7)
            Me.lblOrg.Text = Textos(8)
            cmdCerrar.Value = Textos(9)

            'carga los datos de la persona:
            Dim oPersona As Fullstep.PMPortalServer.Persona = FSPMServer.Get_Persona
            oPersona.LoadData(IdCiaComp, Request("CodPersona"), True)
            With oPersona
                lblCodBD.Text = .Codigo
                lblNombreBD.Text = .Nombre
                lblApeBD.Text = .Apellidos
                lblDepBD.Text = .DenDepartamento
                lblOrgBD.Text = .UONs
                lblCargoBD.Text = .Cargo
                lblTfnoBD.Text = .Telefono
                lblFaxBD.Text = .Fax
                lblMailBD.Text = .EMail
            End With
        End Sub

    End Class
End Namespace