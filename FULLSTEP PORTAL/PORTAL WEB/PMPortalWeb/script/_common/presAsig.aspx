<%@ Page Language="vb" AutoEventWireup="false" Codebehind="presAsig.aspx.vb" Inherits="Fullstep.PMPortalWeb.presAsig" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        <style type="text/css" >
            .SelectedRow
            {
                color:White;
                background-color:Blue;
            }
            #up_Asignaciones
            {
                height:99%;
                width:99%;
            }
            
        </style>
	</head>
	<body>
		<SCRIPT type="text/javascript" language="javascript">
		<!--
		var actualizarcombo = 0;
        var celdaId;
        var vEliminarTodas;

		
		function VerAyuda()
		{		    
			window.open("detalledenominacion.aspx","_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250")
		}

       

        /*''' <summary>
        ''' Seleccionar Presupuesto en la pantalla q llamo
        ''' </summary>
        ''' <remarks>Llamada desde:cmdAceptar; Tiempo m�ximo:0</remarks>*/
		function Aceptar()
		{				
				var oAsigPorcent =igedit_getById("txtAsignadoPorcent")
				var AsigPorcent = oAsigPorcent.getValue()
				
				if ((AsigPorcent < 100) && (arrDistribuciones.length > 0))
				{
					alert(cadenaerror100);
				}
				
				var sValor = ""
				var sTexto = ""
				for (var i in arrDistribuciones)
				{
					oDistr = arrDistribuciones[i]
					if (oDistr.anyo>0)
						sTexto += oDistr.anyo.toString() + " - " 
					switch(oDistr.nivel)
					{
						case 1:
							sTexto += oDistr.pres1
							break; 
						case 2:
							sTexto += oDistr.pres1 + " - " + oDistr.pres2
							break; 
						case 3:
							sTexto += oDistr.pres1 + " - " + oDistr.pres2 + " - " + oDistr.pres3
							break; 
						case 4:
							sTexto += oDistr.pres1 + " - " + oDistr.pres2 + " - " + oDistr.pres3 + " - " + oDistr.pres4
						break; 
					}
					sTexto += " (" + num2str(oDistr.porcent * 100,".",",",2) + "%);"
					sValor += oDistr.nivel.toString() + "_" + oDistr.idpres.toString() + "_" + num2str(oDistr.porcent,".",",",2).toString() + "#"
				}
			
				sValor = sValor.substr(0,sValor.length-1)
				
				var EsCabecera = 0;
				EsCabecera = document.getElementById("EsCabeceraDesglose").value;

				window.opener.pres_seleccionado(document.getElementById("IDCONTROL").value, sValor, sTexto, EsCabecera, <%=Request("Campo")%>)
				window.close()
		
		}
		
       

		function Buscar()
		{	
			var oDrop_

			if ($find("ugtxtunidadorganizativa") != null)
			{
				oDrop_ = $find("ugtxtunidadorganizativa")
				var oItem = oDrop_.get_selectedItem()
				var sValue = oItem.get_value()
				var sArray = sValue.split('@@')
				document.getElementById("UON1").value=sArray[0]
				if(sArray[1]==null){sArray[1]=''}
				document.getElementById("UON2").value=sArray[1]
				if(sArray[2]==null){sArray[2]=''}
				document.getElementById("UON3").value=sArray[2]
			}
            var oTipoPresupuesto=document.getElementById("TIPOPRESUPUESTO")
            var oAnyo=document.getElementById("Anyo")
            if ((oTipoPresupuesto.value == 1 ) || (oTipoPresupuesto.value == 2 ))
			{
				//prespuestos anuales
				oAnyo.value=igedit_getById("ugtxtAnyo").getValue()
			}
			else
			{
				oAnyo.value=0
			}

			var bUO =true
			if($find("ugtxtunidadorganizativa") != null){
			    sUO =oDrop_.get_currentValue()

				if (sUO=="")
					bUO=false
		    }
			
			  			
			if (bUO==false && (document.getElementById("txtCodigo").value=="") && (document.getElementById("txtDenominacion").value==""))
			{
				alert(cadenaerrorfiltrado)
			}
			else			
			{	
				if (( oTipoPresupuesto.value == 1 ) || ( oTipoPresupuesto.value == 2 ))
				{
					if (igedit_getById("ugtxtAnyo").getValue()!= null)
					{
						RellenarValor()				
						document.forms["frmPresupuestos"].submit()
					}
				}
				else
				{
						RellenarValor()				
						document.forms["frmPresupuestos"].submit()				
				}
			}
			
		}
	

	function RellenarValor()
	{
		
		var sValor=""
		for (var i in arrDistribuciones)
			{
				oDistr = arrDistribuciones[i]
					
				sValor += oDistr.nivel.toString() + "_" + oDistr.idpres.toString() + "_" + num2str(oDistr.porcent,".",",",2).toString() + "#"
			}
		sValor = sValor.substr(0,sValor.length-1)
		document.getElementById("Valor").value=sValor
	}
	
	var dPorcentDistribuido
	var arrDistribuciones = new Array()
	
	function Distribucion(uon1, uon2, uon3, anyo, pres1, pres2, pres3, pres4, porcent, nivel,idpres,denuon,denpres,nodeId,indexNode)
	{
	if (uon1!="")
		this.uon1 = uon1
	else
		this.uon1 = null
		
	if (uon2!="")
		this.uon2 = uon2
	else
		this.uon2 = null
	if (uon3!="")
		this.uon3 = uon3
	else
		this.uon3 = null
	this.anyo = anyo
	
	if (pres1!="")
		this.pres1 = pres1
	else
		this.pres1 = null
	if (pres2!="")
		this.pres2 = pres2
	else
		this.pres2 = null
	if (pres3!="")
		this.pres3 = pres3
	else
		this.pres3 = null
	if (pres4!="")
		this.pres4 = pres4
	else
		this.pres4 = null
	this.porcent = porcent
	this.nivel = nivel
	this.idpres = idpres
	this.denuon = denuon
	this.denpres = denpres
	this.nodeId = nodeId
	this.indexNode = indexNode
	}		
	

var PorcentageSelected=""
var ValorAsociadoSelected=""
var DenominacionSelected=""


function Quitar()
{
  if (ValorAsociadoSelected != "")
  {
	var f = document.forms["frmPresupuestos"]					
	var temp = 	ValorAsociadoSelected.split("_")

	for (var i in arrDistribuciones)
		{
			oDistr = arrDistribuciones[i]
			if (oDistr.idpres == temp[1] && oDistr.nivel==temp[0])
			  {
			  //Elimina del array
			  arrDistribuciones.splice(i,1)
			  break
			  }
		}
				
	//Quita el presupuesto asignado de las grids de asignaciones.
        	
	 oGrid= $find("wdg_Asignaciones")
     for (i=oGrid.get_rows().get_length()-1;i>=0;i--)
	{	
	    oRow=oGrid.get_rows().get_row(i)
        var column = oGrid.get_columns().get_columnFromKey("VALORASOCIADO");
        if (oRow.get_cellByColumn(column).get_value()==ValorAsociadoSelected)
        {
             oGrid.get_rows().remove(oRow,false);
             break
        }
	}

	//Actualiza los porcentajes
	CargarPorcentage()
	
	//Cuando quita un presupuesto de la grid se oculta la grid "peque"
	RestaurarGrids()
  }
}		

/*''' <summary>
''' A�ade el nodo seleccionado a la grid de asignaciones
''' </summary>
''' <remarks>Llamada desde:imganiadir; Tiempo m�ximo:0</remarks>*/
function Aniadir()
{
//
 var f = document.forms["frmPresupuestos"]		
 var oTree =igtree_getTreeById("uwtPresupuestos2")
 var oNode=oTree.getSelectedNode()
	
 if (oNode!=null) //Si no es el nodo ra�z podemos asignar
  {
  if (oNode.getParent()!=null) //Si no es el nodo ra�z y el valor es > 0 podemos asignar
  { 
	if (igedit_getById("txtPorcent").getValue() > 0)
	{
  
		//Obtenemos todos los datos necesarios para insertar:
		var dPorcent=igedit_getById("txtPorcent").getValue()
		var sDenPresAsig=""
		var sDenPres=oNode.getText()
        var oTipoPresupuesto=document.getElementById("TIPOPRESUPUESTO")
		if ((oTipoPresupuesto.value == 1) || (oTipoPresupuesto.value ==2))
		{
			sDenPresAsig+= "(" + igedit_getById("ugtxtAnyo").getText() + ") "	
			iAnyo=igedit_getById("ugtxtAnyo").getValue()
		}
		else	
		{
			iAnyo=0
		}

		while (oNode.getParent()!=null)
		{
			oNode = oNode.getParent()
			if (oNode.getDataKey()!=null)
			{	
				sDenPresAsig+= oNode.getDataKey().split("__")[1] + "-"
			}
		}	
		
		sDenPresAsig+= sDenPres	
			
		oNode=oTree.getSelectedNode()
		var nodeId=oNode.Id
		var idPresup = oNode.getDataKey().split("__")[0]
		var oarrNode = oNode.getIndex()
	    
		var arrPres = new Array()
		while (oNode.getParent()!=null)
		{
			arrPres[arrPres.length] = oNode.getDataKey().split("__")[1]
			oNode = oNode.getParent()
		}
		var i = arrPres.length - 1
		var sPres1 = null
		var sPres2 = null
		var sPres3 = null
		var sPres4 = null
		
		sPres1 = arrPres[i]
		iNivel = 1
		i--
		if (i>=0)
			{
			sPres2 = arrPres[i]
			iNivel = 2
			i--
			if (i>=0)
				{
				sPres3 = arrPres[i]
				iNivel = 3
				i--
				if (i>=0)
					{
					sPres4 = arrPres[i]
					iNivel = 4
					i--
					}
				}
			}
		
		//Obtiene los datos de la unidad organizativa
		sUon1 = document.getElementById("UON1").value
		if (sUon1=="" || sUon1=="#")
			sUon1=null
		sUon2 = document.getElementById("UON2").value
		if (sUon2=="" || sUon2=="#")
			sUon2=null
		sUon3 = document.getElementById("UON3").value
		if (sUon3=="" || sUon3=="#")
			sUon3=null
		
		if ($find("ugtxtunidadorganizativa") != null)
        {
			var oDrop_ = $find("ugtxtunidadorganizativa")
			var oItem = oDrop_.get_selectedItem()
			sDenUon = oItem.get_text()	
			if (sDenUon == null ) //Es vacio por lo que 
				sDenUon = document.getElementById("DENUON").value
		}
		else
			sDenUon = document.getElementById("DENUON").value
		  
		//*********************Asigna en el array ***************************************
		//NOTA1: Comprueba que el presupuesto no est� ya asignado en el array, si es as� no hace nada.
		var bYaExiste =false
		if (parseInt(dPorcent)<100)
		{
			for (var i in arrDistribuciones)
			{
				oDistr = arrDistribuciones[i]		
				if (oDistr.idpres == idPresup && oDistr.nivel==iNivel)
				{
					bYaExiste=true
					break
				}
			}
		}
		if (bYaExiste==false)
		{
            oGrid= $find("wdg_Asignaciones")
			//NOTA2: Si ya hab�a presupuestos asignados al 100% y se va a a�adir otro al 100% quitamos del array los anteriores:
			var dPorcentDistribuido=0
			for (var i in arrDistribuciones)
			{
				oDistr = arrDistribuciones[i]
				dPorcentDistribuido = dPorcentDistribuido + parseInt(num2str(oDistr.porcent * 100,".",",",2),10);		
			}
		
			if (dPorcentDistribuido==100)
			{ //Elimina del array de asignados y de las 2 grids
				for (i=arrDistribuciones.length-1;i>=0;i--)
				{
					arrDistribuciones.splice(i,1)
				}
				for (i=oGrid.get_rows().get_length()-1;i>=0;i--)
					{
					oRow=oGrid.get_rows().get_row(i)
                    oGrid.get_rows().remove(oRow,true);
					}
			}
			
			//asigna el nuevo presupuesto:
			arrDistribuciones[arrDistribuciones.length] = new Distribucion(sUon1,sUon2,sUon3,iAnyo,sPres1,sPres2,sPres3,sPres4,parseInt(dPorcent)/100,iNivel,idPresup,sDenUon,sDenPres,nodeId,oarrNode)
			
			//Inserta en las grids:
            var oNewRow = new Array(dPorcent, sDenPresAsig,iNivel + "_" + idPresup + "_" + dPorcent);
            oGrid.get_rows().add(oNewRow);
		    
			//Actualiza los porcentajes
			CargarPorcentage()
			
			//Cuando a�ade un presupuesto de la grid se oculta la grid "peque"
			RestaurarGrids()
		}
	}
  }
 }
}

function Cambiar()
{
  if (ValorAsociadoSelected != "")
  {
	var dPorcent=igedit_getById("ugtxtPorcentDetalle").getValue()
		
	if (dPorcent > 0) //Cuando quiere cambiar a 0 el valor lo hemos de quitar
	{	//Actualiza el array:
		var temp = 	ValorAsociadoSelected.split("_")
		for (var i in arrDistribuciones)
			{
				oDistr = arrDistribuciones[i]
				if (oDistr.idpres == temp[1] && oDistr.nivel==temp[0])
				{
				//Actualiza el array
				oDistr.porcent=parseInt(dPorcent)/100
				break
				}
			}
			
		//Actualiza las grids:
		var sNewValorAsociado=""
		sNewValorAsociado=temp[0] + "_" + temp[1] + "_" + dPorcent

		//Ahora lo cambia tb en la grid de asignaciones
        oGrid =$find("wdg_Asignaciones")
		for (i=0;i<=oGrid.get_rows().get_length() -1 ;i++)
			{
				oRow=oGrid.get_rows().get_row(i)
				if (oRow.get_cell(2).get_value()==ValorAsociadoSelected)
				{	
                    oRow.get_cell(0).set_value(dPorcent)
					oRow.get_cell(2).set_value(sNewValorAsociado)
					break
				}
			}	
			
		PorcentageSelected=dPorcent
		
		//Ahora actualiza los porcentajes
		CargarPorcentage()
		
		//Porcentaje de la caja de asignaci�n de detalle:
		
		var txtasignado=document.getElementById("txtAsignadoPorcent").value
		var txtpendiente=document.getElementById("txtPendientePorcent").value					
		var maximo = parseInt(PorcentageSelected,10) + parseInt(txtpendiente,10);
		igedit_getById("ugtxtPorcentDetalle").setMaxValue (maximo)										
		igedit_getById("ugtxtPorcentDetalle").setMinValue (0)
		igedit_getById("ugtxtPorcentDetalle").setValue(PorcentageSelected)	
		
		
		//Cuando cambia un presupuesto de la grid se oculta la grid "peque"
		RestaurarGrids()			
		ValorAsociadoSelected = ""	
	}	
	
  }
}

function MostrarDetalle()
{
	var temp = 	ValorAsociadoSelected.split("_")

			var textden=""
			var textuon = ""
							
			
			for (var i in arrDistribuciones)
			{
				oDistr = arrDistribuciones[i]
				
				if (oDistr.idpres == temp[1] && oDistr.nivel==temp[0])
				{
					textuon = cadenaunidadorganizativa + ": "
					if (oDistr.uon1 != null)					
						textuon +=  oDistr.uon1					
					if (oDistr.uon2 != null)
						textuon += "-" + oDistr.uon2
					if (oDistr.uon3 != null)
						textuon += "-" + oDistr.uon3
					if (oDistr.denuon != null)
					{					
						if (oDistr.uon1 != null) 
						  textuon += "-"
						  
						textuon += oDistr.denuon						
					}
										
					var txtasignado=document.getElementById("txtAsignadoPorcent").value
					var txtpendiente=document.getElementById("txtPendientePorcent").value
					var porcenasignadoaotros = txtasignado - PorcentageSelected					
					var maximo = parseInt(PorcentageSelected,10) + parseInt(txtpendiente,10);
					igedit_getById("ugtxtPorcentDetalle").setMaxValue (maximo)										
					igedit_getById("ugtxtPorcentDetalle").setMinValue (0)
					igedit_getById("ugtxtPorcentDetalle").setValue(PorcentageSelected)	

					var oDetDen=document.getElementById("lblDetDen")
                    var oDetUon=document.getElementById("lblDetUon")
                    if(oDetDen.innerText!=undefined)
                    {
                         oDetDen.innerText= DenominacionSelected
                         oDetUon.innerText= textuon
                    }else{
                        oDetDen.textContent= DenominacionSelected
                        oDetUon.textContent= textuon
                    }
					
					break;	
				}
				
			}
}

 /*
 Funcion que salta cuando se cambia de fila en el grid de asignaciones y muestra la tabla de detalle
 de la asignacion selecciona
 Sender: grid que desencadena el evento
 e: argumentos del grid
 */
 function wdg_Asignaciones_RowSelectionChanged(sender, e)
        {
            var oGrid = $find("wdg_Asignaciones");

            var oRow=e.getSelectedRows().getItem(0)
            document.getElementById("layergrid").style.height="150px"

            PorcentageSelected = oRow.get_cell(0).get_value()
			
			DenominacionSelected= oRow.get_cell(1).get_value()	
			
			ValorAsociadoSelected = oRow.get_cell(2).get_value()


            document.getElementById("tabladetalle").style.display="inline";
			
			MostrarDetalle();			
            	
        }
			

function MensajeSinElementos()
{
	alert(cadenaerrorsinpresupuestos)
}

function AlertaMultiplesUONS(sText)
{
	alert(sText);
}

function ugtxtPorcent_ValueChange(oEdit, oldValue, oEvent)
{
	var permitidos="0123456789"
	var valorporcentage = igedit_getById("txtPorcent").getValue()
	if (valorporcentage != null)
	{
		var permitido = 0;
		 
		for (i=0; i<= valorporcentage.length-1; i++)
		{
			permitido = 0;
			
			for (j=0; j<= permitidos.length-1; j++)
			{
				if (valorporcentage.charAt(i) == permitidos.charAt(j))
				{
					permitido = 1
					break;
				}
			}
			
			if (permitido == 0)
			{
				var oPendPorcent = igedit_getById("txtPendientePorcent").getValue()			
				if (oPendPorcent == 0)			
					igedit_getById("txtPorcent").setValue(100)
				else
					igedit_getById("txtPorcent").setValue(oPendPorcent)
				break;
			}
		}
	}
	else
	{
				var oPendPorcent = igedit_getById("txtPendientePorcent").getValue()			
				if (oPendPorcent == 0)			
					igedit_getById("txtPorcent").setValue(100)
				else
					igedit_getById("txtPorcent").setValue(oPendPorcent)
	}
}

function IsNumeric(valor) 
{ 
var log=valor.length; var sw="S"; 
	for (x=0; x<log; x++) 
	{ 
		v1=valor.substr(x,1); 
		v2 = parseInt(v1); 
		if (isNaN(v2)) 
		{ 
		sw= "N";
			} 
	} 
	if (sw=="S") 
	{
		return true;
	} 
	else 
	{
		return false; 
	} 
} 

		
function CargarPorcentage()
{
	dPorcentDistribuido = 0
	
    document.getElementById("Botones").style.display="inline"
	
	for (var i in arrDistribuciones)
	{
		oDistr = arrDistribuciones[i]
		dPorcentDistribuido = dPorcentDistribuido + parseInt(num2str(oDistr.porcent * 100,".",",",2),10);		
	}
		
	var oAsigPorcent =igedit_getById("txtAsignadoPorcent")
	oAsigPorcent.setValue (dPorcentDistribuido)
		
	var oPendPorcent = igedit_getById("txtPendientePorcent")
	oPendPorcent.setValue((100-dPorcentDistribuido))
	
	
	if (dPorcentDistribuido < 100)
	{
		igedit_getById("txtPorcent").setMaxValue (100-dPorcentDistribuido)
		igedit_getById("txtPorcent").setMinValue (0)
		igedit_getById("txtPorcent").setValue(100-dPorcentDistribuido)	
	}
	else
	{
		igedit_getById("txtPorcent").setMaxValue (100)
		
		if (arrDistribuciones.length==0)
			igedit_getById("txtPorcent").setMinValue (0)
		else
			igedit_getById("txtPorcent").setMinValue (100)
			
		igedit_getById("txtPorcent").setValue(100)		
	}		
	
}

function RestaurarGrids()
{
	//Cuando quita un presupuesto de la grid se oculta la grid "peque"
	document.getElementById("layergrid").style.height="250px"	
	document.getElementById("tabladetalle").style.display="none";
	var oGrid = $find("wdg_Asignaciones");

    //Quita las filas seleccionadas
    var selection = oGrid.get_behaviors().get_selection();
    var rows = selection.get_selectedRows();
    rows.remove(rows.getItem(0));
}

function CambiarValorCombo(valor)
{
	actualizarcombo = parseInt(valor,10);
}


function ActualizarCombo()
{
	oDrop = $find("ugtxtunidadorganizativa")
	oDrop.selectItemByIndex(actualizarcombo, true, true)
}

function ugtxtAnyo_ValueChange(oEdit, oldValue, oEvent)
{		
	var valoranyo = igedit_getById("ugtxtAnyo").getValue()	
	if (valoranyo != null && valoranyo !=0)
	{
		if (IsNumeric(valoranyo))
		{
			var dateVar = new Date()
			var anyo = dateVar.getFullYear() 
			if ((valoranyo < 1990) || (valoranyo >  anyo))
				igedit_getById("ugtxtAnyo").setValue(anyo)
		}		 
		else
		{
			igedit_getById("ugtxtAnyo").setValue(oldValue)
		}
	}
	else
	{
			var dateVar = new Date()
			var anyo = dateVar.getFullYear() 			
			igedit_getById("ugtxtAnyo").setValue(anyo)
	}
}

function ugtxtPorcentDetalle_ValueChange(oEdit, oldValue, oEvent)
{
	var valorporcentage = igedit_getById("ugtxtPorcentDetalle").getValue()
	if (valorporcentage == null)
	{
		igedit_getById("ugtxtPorcentDetalle").setValue(oldValue)
	}

}
	
function checkCombo(){
    if (actualizarcombo != 0) 
        ActualizarCombo();
}
--></SCRIPT>
		<form id="frmPresupuestos" method="post" runat="server">
            <asp:ScriptManager ID="scrMng_presAsig" runat="server"></asp:ScriptManager>
			<DIV id="Botones" style="DISPLAY: none" name="Botones"><IMG id="imganiadir" style="Z-INDEX: 116; LEFT: 376px; CURSOR: hand; POSITION: absolute; TOP: 312px"
					onclick="javascript:Aniadir()" alt="" src="../_common/images/aniadir.gif" name="imganiadir"><IMG id="imgquitar" style="Z-INDEX: 117; LEFT: 376px; CURSOR: hand; POSITION: absolute; TOP: 336px"
					onclick="javascript:Quitar()" alt="" src="../_common/images/quitar.gif" name="imgquitar">
			</DIV>
			<TABLE id="Table2" style="Z-INDEX: 111; LEFT: 24px; WIDTH: 752px; POSITION: absolute; TOP: 54px; HEIGHT: 56px"
				cellSpacing="1" cellPadding="1" width="752" border="0">
				<TR>
					<TD style="WIDTH: 177px; HEIGHT: 12px"><asp:label id="lblUnidadOrganizativa" runat="server" CssClass="parrafo" Width="160px"></asp:label></TD>
					<TD style="WIDTH: 253px; HEIGHT: 12px"><ig:WebDropDown ID="ugtxtunidadorganizativa" runat="server" Width="256px"
	                                DropDownContainerHeight="150px" DropDownContainerWidth="256px" EnableDropDownAsChild="false" DropDownAnimationType="EaseOut" 
	                                EnablePaging="False" PageSize="12">
                                    </ig:WebDropDown></TD>
					<TD style="WIDTH: 58px; HEIGHT: 12px"><asp:label id="lblAnyo" runat="server" CssClass="parrafo">A�o:</asp:label></TD>
					<TD style="WIDTH: 124px; HEIGHT: 12px"><igtxt:webnumericedit id="ugtxtAnyo" style="display: " runat="server" MinValue="1990" MaxValue="2100">
							<ClientSideEvents ValueChange="ugtxtAnyo_ValueChange"></ClientSideEvents>
							<SpinButtons Display="OnRight"></SpinButtons>
						</igtxt:webnumericedit></TD>
					<TD style="WIDTH: 38px; HEIGHT: 12px"><A onclick="Buscar()" href="#"><asp:label id="lblBuscar" runat="server" CssClass="parrafo">Buscar</asp:label></A></TD>
					<TD style="HEIGHT: 12px"><a id="cmdBuscarPres" style="CURSOR: hand" onclick="javascript:Buscar()" name="cmdBuscarPres"><IMG src="../_common/images/buscar.gif" border="0"></a></TD>
				</TR>
				<TR>
					<td style="HEIGHT: 20px" colSpan="1"><asp:label id="lblCodigo" runat="server" CssClass="parrafo" Width="176px" Height="16px">C�digo:</asp:label></td>
					<td style="WIDTH: 630px; HEIGHT: 20px" colSpan="5"><asp:textbox id="txtCodigo" style="LEFT: 100px" runat="server" Width="120px" Height="20px"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:label id="lblDenominacion" style="LEFT: 25px" runat="server" CssClass="parrafo" Width="104px">Denominaci�n:</asp:label><asp:textbox id="txtDenominacion" runat="server" Width="152px" Height="20px"></asp:textbox><A id="cmdBuscarPet" style="CURSOR: hand" onclick="javascript:VerAyuda()" name="cmdBuscarPet">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<IMG src="../_common/images/help.gif" border="0"></A></td>
				</TR>
				<tr>
					<td style="HEIGHT: 20px" colSpan="6"><asp:label id="lblResultado" runat="server" CssClass="captionBlackSmall" Width="200px" Visible="False">Resultado 
					de la busqueda:</asp:label></td>
				</tr>
				<tr>
					<td style="HEIGHT: 4px" colSpan="6">
						<HR id="lineares" style="DISPLAY: ; TOP: -5px; margin-bottom: 5px;" width="100%" color="gainsboro"
							noShade SIZE="4">
					</td>
				<tr>
					<td style="HEIGHT: 4px" colSpan="6"><asp:label id="lblSeleccionaArbol" runat="server" CssClass="captionBlackSmall" Width="288px"
							Height="22px" Visible="False">Seleccione el concepto prespuestario pulsando sobre una de las ramas del �rbol.</asp:label></td>
				</tr>
			</TABLE>
			<igtxt:webnumericedit id="txtPorcent" style="Z-INDEX: 115; LEFT: 368px; POSITION: absolute; TOP: 288px; diplay: none"
				runat="server" Width="69px" Visible="False" Nullable="False">
				<ClientSideEvents ValueChange="ugtxtPorcent_ValueChange"></ClientSideEvents>
				<SpinButtons Display="OnRight" Delta="10"></SpinButtons>
			</igtxt:webnumericedit><asp:label id="lbltitulo" style="Z-INDEX: 112; LEFT: 24px; POSITION: absolute; TOP: 20px" runat="server"
				CssClass="tituloDarkBlue" Width="448px">Busqueda de partidas contables</asp:label><asp:label id="lblUorganizativaRestringida" style="Z-INDEX: 121; LEFT: 208px; POSITION: absolute; TOP: 56px"
				runat="server" CssClass="captionBlue" Width="264px"> lblUorganizativaRestringida</asp:label><!--<br><br><br><br><br><br>-->
			<TABLE id="Table4" style="Z-INDEX: 114; LEFT: 478px; WIDTH: 298px; POSITION: absolute; TOP: 151px; HEIGHT: 24px"
				cellSpacing="1" cellPadding="1" width="302" border="0">
				<TR>
					<TD style="WIDTH: 110px; HEIGHT: 28px"><asp:label id="lblAsignado" runat="server" CssClass="captionBlue" Width="100px" Visible="False">Asignado:</asp:label></TD>
					<TD style="HEIGHT: 28px"><igtxt:webnumericedit id="txtAsignadoPorcent" runat="server" style="display: " Width="56px" Height="20px" Visible="False"
							ReadOnly="True"></igtxt:webnumericedit><asp:label id="Label1" style="POSITION: absolute; TOP: 7px; LEFT: 180px" runat="server" CssClass="captionBlue"
							Visible="False">%</asp:label></TD>
				</TR>
			</TABLE>
			<TABLE id="Table4" style="Z-INDEX: 113; LEFT: 478px; WIDTH: 298px; POSITION: absolute; TOP: 176px; HEIGHT: 24px"
				cellSpacing="1" cellPadding="1" width="302" border="0">
				<TR>
					<TD style="WIDTH: 110px"><asp:label id="lblPendiente" runat="server" CssClass="captionBlue" Width="100px" Visible="False">Pendiente:</asp:label></TD>
					<TD style="HEIGHT: 28px"><igtxt:webnumericedit id="txtPendientePorcent" runat="server" style="display: " Width="56px" Height="20px" Visible="False"
							ReadOnly="True"></igtxt:webnumericedit><asp:label id="Label3" style="POSITION: absolute; TOP: 7px; LEFT: 180px" runat="server" CssClass="captionBlue"
							Height="24" Visible="False">%</asp:label></TD>
				</TR>
			</TABLE>
			<!--<br>
			<br>
			<br>
			<br>
			<br>
			<br>-->
			<TABLE style="Z-INDEX: 118; LEFT: 24px; WIDTH: 752px; POSITION: absolute; TOP: 480px; HEIGHT: 30px"
				width="752">
				<TR>
					<TD style="WIDTH: 361px" align="right"><INPUT class="boton" id="cmdAceptar" onclick="Aceptar()" type="button" value="Aceptar"
							name="cmdAceptar" runat="server">
					</TD>
					<TD align="left"><INPUT class="boton" id="cmdCancelar" onclick="window.close()" type="button" value="Cancelar"
							name="cmdCancelar" runat="server">
					</TD>
				</TR>
			</TABLE>
			<ignav:ultrawebtree id="uwtPresupuestos2" style="Z-INDEX: 119; LEFT: 32px; POSITION: absolute; TOP: 200px; BACKGROUND-COLOR: #e1edff"
				runat="server" CssClass="igTree" Width="293px" Height="264px" Visible="False">
				<SelectedNodeStyle ForeColor="White" BackColor="Navy"></SelectedNodeStyle>
				<NodeStyle Height="15px"></NodeStyle>
				<Levels>
					<ignav:Level Index="0"></ignav:Level>
					<ignav:Level Index="1"></ignav:Level>
				</Levels>
			</ignav:ultrawebtree>
			<div id="layergrid" style="Z-INDEX: 124; LEFT: 480px; OVERFLOW: auto; WIDTH: 295px; POSITION: absolute; TOP: 210px; HEIGHT: 264px; TEXT-ALIGN: left">
            
                <asp:UpdatePanel ID="up_Asignaciones" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                <ContentTemplate>
                    <ig:WebDataGrid ID="wdg_Asignaciones" runat="server" Height="100%" OnRowsDeleting="wdg_Asignaciones_RowsDeleting"
                        AutoGenerateColumns="False" ShowFooter="false" BackColor="White" HeaderCaptionCssClass="parrafo" Width="100%" BorderStyle="Solid" DataKeyFields="VALORASOCIADO" 
                        BorderWidth="1px">
                        <Columns>
                            <ig:BoundDataField DataFieldName="PORCENTAGE" Key="PORCENTAGE" Hidden="false"></ig:BoundDataField>
					        <ig:BoundDataField DataFieldName="DESCRIPCION" Key="DESCRIPCION" Hidden="false"></ig:BoundDataField>
					        <ig:BoundDataField DataFieldName="VALORASOCIADO" Key="VALORASOCIADO" Hidden="true"></ig:BoundDataField>
                        </Columns>
                        <Behaviors>
                            <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Row" SelectedCellCssClass="SelectedRow">
                                <SelectionClientEvents RowSelectionChanged="wdg_Asignaciones_RowSelectionChanged" />
                            </ig:Selection>    
                            <ig:EditingCore AutoCRUD="false">
                                <Behaviors>
                                    <ig:RowDeleting />
                                    <ig:RowAdding>
                                    </ig:RowAdding>
                                    <ig:CellEditing></ig:CellEditing>
                                </Behaviors>
                            </ig:EditingCore>
                        </Behaviors>
                        
                    </ig:WebDataGrid>
                </ContentTemplate> 
                </asp:UpdatePanel> 
                </div>
			<div id="layergridPeque" style="DISPLAY: none; Z-INDEX: 123; LEFT: 480px; OVERFLOW: auto; WIDTH: 295px; POSITION: absolute; TOP: 200px; HEIGHT: 150px; TEXT-ALIGN: left">
                
                </DIV>
			<asp:label id="lblPorcen1" style="Z-INDEX: 122; LEFT: 440px; POSITION: absolute; TOP: 288px"
				runat="server" Width="8px" Visible="False" cssClass="captionBlue">%</asp:label>
			<div id="tabladetalle" style="BORDER-RIGHT: silver 1px solid; BORDER-TOP: silver 1px solid; DISPLAY: none; Z-INDEX: 120; LEFT: 480px; BORDER-LEFT: silver 1px solid; WIDTH: 294px; BORDER-BOTTOM: silver 1px solid; POSITION: absolute; TOP: 365px; HEIGHT: 107px">
            
                
				<DIV  id="DetDen" style="OVERFLOW-Y: auto; HEIGHT: 30px"
					name="DetDen"><span class="captionBlackSmall" id="lblDetDen"></span></DIV>
				<DIV class="captionBlackSmall" id="DetUon" style="OVERFLOW-Y: auto; HEIGHT: 45px"
					name="DetUon"><span class="captionBlackSmall" id="lblDetUon"></span></DIV>
                <table style="width:75%; height:auto; text-align:center">
                    <tr>
                        <td>
                            <igtxt:webnumericedit id="ugtxtPorcentDetalle" style="Z-INDEX: 101; display: "
					        runat="server" Width="80px" Height="19">
					        <ClientSideEvents ValueChange="ugtxtPorcentDetalle_ValueChange"></ClientSideEvents>
					        <SpinButtons Display="OnRight" Delta="10"></SpinButtons>
				            </igtxt:webnumericedit>
                        </td>
                        <td>
                             <asp:label id="lblPorcen2" style="Z-INDEX: 101;"
					        runat="server" Width="8px" Visible="False" cssClass="captionBlue">%</asp:label>
                        </td>
                        <td>
                            <input class="boton" id="cmdCambiar" style="Z-INDEX: 102;"
					        onclick="Cambiar()" type="button" value="Cambiar" name="cmdCambiar" runat="server"/>
                        </td>
                    </tr>
                </table>
        
                
            </div>
			<INPUT id="TIPO" style="Z-INDEX: 107; LEFT: 520px; POSITION: absolute; TOP: 512px" type="hidden"
				name="TIPO" RUNAT="SERVER"> <INPUT id="IDCONTROL" style="Z-INDEX: 109; LEFT: 32px; POSITION: absolute; TOP: 512px"
				type="hidden" name="IDCONTROL" RUNAT="SERVER"> <INPUT id="txtunidadorganizativa" style="Z-INDEX: 110; LEFT: 360px; POSITION: absolute; TOP: 512px"
				type="hidden" name="txtunidadorganizativa" RUNAT="SERVER"> <INPUT id="UON1" style="Z-INDEX: 106; LEFT: 312px; POSITION: absolute; TOP: 480px" type="hidden"
				name="UON1" RUNAT="SERVER"> <INPUT id="UON2" style="Z-INDEX: 108; LEFT: 192px; POSITION: absolute; TOP: 512px" type="hidden"
				name="UON2" RUNAT="SERVER"> <INPUT id="UON3" style="Z-INDEX: 105; LEFT: 312px; POSITION: absolute; TOP: 416px" type="hidden"
				name="UON3" RUNAT="SERVER"> <INPUT id="DENUON" style="Z-INDEX: 105; LEFT: 312px; POSITION: absolute; TOP: 416px" type="hidden"
				name="DENUON" RUNAT="SERVER"><INPUT id="Valor" style="Z-INDEX: 104; LEFT: 312px; POSITION: absolute; TOP: 448px" type="hidden"
				name="Valor" RUNAT="SERVER"> <INPUT id="Anyo" style="Z-INDEX: 102; LEFT: 312px; POSITION: absolute; TOP: 496px" type="hidden"
				value="0" name="Anyo" RUNAT="SERVER"><INPUT id="TIPOPRESUPUESTO" style="Z-INDEX: 103; LEFT: 312px; POSITION: absolute; TOP: 472px"
				type="hidden" value="0" name="TIPOPRESUPUESTO" RUNAT="SERVER"><INPUT id="USUARIORESTRINGIDO" style="Z-INDEX: 101; LEFT: 312px; POSITION: absolute; TOP: 432px"
				type="hidden" value="0" name="USUARIORESTRINGIDO" RUNAT="SERVER"><INPUT id="EsCabeceraDesglose" style="Z-INDEX: 101; LEFT: 312px; POSITION: absolute; TOP: 432px"
				type="hidden" value="0" name="EsCabeceraDesglose" RUNAT="SERVER">
		</form>
	</body>
</html>
<script type="text/javascript">
    Sys.Application.add_load(checkCombo);	
</script>