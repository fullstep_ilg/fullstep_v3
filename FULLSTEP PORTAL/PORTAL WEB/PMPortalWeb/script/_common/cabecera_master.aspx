﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="cabecera_master.aspx.vb" Inherits="Fullstep.PMPortalWeb.cabecera_master" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>" lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="cabecera" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
		<Scripts>
            <asp:ScriptReference Path="~/js/jquery/jquery.min.js" />
            <asp:ScriptReference Path="~/js/jquery/jquery-migrate.min.js" />
			<asp:ScriptReference Path="~/js/jquery/jquery.json.min.js" />
			<asp:ScriptReference Path="~/js/jquery/jquery.tmpl.min.js" />
			<asp:ScriptReference Path="~/js/jquery/jquery.ui.min.js" />
			<asp:ScriptReference Path="~/js/jquery/tmpl.min.js" /> 
			<asp:ScriptReference Path="~/ckeditor/ckeditor.js" />
			<asp:ScriptReference Path="~/js/cabecera_master.js" />
		</Scripts>
	</asp:ScriptManager>    
    </form>
</body>
</html>
