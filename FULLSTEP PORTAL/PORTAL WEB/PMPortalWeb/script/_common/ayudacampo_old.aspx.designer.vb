﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Fullstep.PMPortalWeb

    Partial Public Class ayudacampo

        '''<summary>
        '''Head1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Head1 As Global.System.Web.UI.HtmlControls.HtmlHead

        '''<summary>
        '''Form1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Form1 As Global.System.Web.UI.HtmlControls.HtmlForm

        '''<summary>
        '''tblAyuda control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tblAyuda As Global.System.Web.UI.HtmlControls.HtmlTable

        '''<summary>
        '''lblTitulo control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblTitulo As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''lblTipoSolicitud control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblTipoSolicitud As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''lblGrupo control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblGrupo As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''txtGrupo control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtGrupo As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''lblCampo control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblCampo As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''txtCampo control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtCampo As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''lblAyuda control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblAyuda As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''lblFormula control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblFormula As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''txtFormula control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtFormula As Global.System.Web.UI.WebControls.Label
    End Class
End Namespace
