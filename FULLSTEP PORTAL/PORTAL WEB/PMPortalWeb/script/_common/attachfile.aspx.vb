Namespace Fullstep.PMPortalWeb
    Partial Class attachfile
        Inherits FSPMPage


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents Label1 As System.Web.UI.WebControls.Label
        Protected WithEvents llblComent As System.Web.UI.WebControls.Label

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Carga de la pantalla
        ''' </summary>
        ''' <param name="sender">pantalla</param>
        ''' <param name="e">parametro de sistema</param>            
        ''' <returns>Nada, es un evento de sistema</returns>
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Me.ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.AttachFile

            Me.lblSeleccione.Text = Textos(0)
            Me.lblComent.Text = Textos(1)
            Me.cmdUpload.Value = Textos(3)

            Me.IDCAMPO.Value = Request("Campo")
            Me.FSENTRY.Value = Request("Input")


            If Request("EsContrato") = 1 Then
                If Request("idAdjuntoContrato") <> "" AndAlso Request("idAdjuntoContrato") <> "0" Then
                    Me.lblComent.Text = Textos(6) + " " + Server.UrlDecode(Request("Nombre"))
                End If
            Else
                If Request("ID") <> "" AndAlso Request("ID") <> "0" Then
                    Me.lblComent.Text = Textos(6) + Server.UrlDecode(Request("Nombre"))
                End If
            End If

            If Not IsPostBack() Then
                ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DesgloseControl
                txtComent.Attributes("onchange") = "return validarLengthyCortarStringLargo(this,500,'" & Textos(11) & "')"
            End If
        End Sub

        ''' <summary>
        ''' Grabar el adjunto y el comentario
        ''' </summary>
        ''' <param name="sender">bot�n Upload</param>
        ''' <param name="e">evento del bot�n</param>        
        ''' <returns>Nada</returns>
        ''' <remarks>Llamada desde: sistema (bot�n Upload); Tiempo m�ximo:0</remarks>
        Private Sub cmdUpload_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpload.ServerClick
            Dim lCiaComp As Long = IdCiaComp
            Dim COMENTSalto As String

            Dim oAdjun As Fullstep.PMPortalServer.Adjunto = FSPMServer.Get_Adjunto
            If Not Me.miFile.PostedFile Is Nothing And Me.miFile.PostedFile.ContentLength > 0 Then
                Dim fn As String = System.IO.Path.GetFileName(miFile.PostedFile.FileName)
                Dim vFolderPath As String = ConfigurationManager.AppSettings("TEMP")
                Dim SaveLocation As String = vFolderPath + "\" + fn
                Try
                    Dim ext As String = System.IO.Path.GetExtension(miFile.PostedFile.FileName)
                    If oAdjun.EstaenListaNegra(ext) Then
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>alert('" & Textos(7) & "');</script>")
                        oAdjun = Nothing
                        Exit Sub
                    End If

                    If Not getMimeFromFile(miFile.PostedFile, LCase(ext)) Then
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>alert('" & Textos(8) & "');</script>")
                        oAdjun = Nothing
                        Exit Sub
                    End If

                    miFile.PostedFile.SaveAs(SaveLocation)
                    oAdjun.Coment = Request("txtComent")
                    oAdjun.Prove = FSPMUser.CodProve
                    oAdjun.IdCia = FSPMUser.IdCia
                    oAdjun.IdUsu = FSPMUser.IdUsu

                    COMENTSalto = Replace(oAdjun.Coment, vbCrLf, "@VBCRLF@")

                    If Not Request("EsContrato") Is Nothing Then
                        Dim idArchivoContrato As Long
                        Dim arrAdjuntoContrato(1) As Long
                        arrAdjuntoContrato = oAdjun.Save_Adjun_Contrato_Wizard(lCiaComp, "", FSPMUser.CodProve, fn, vFolderPath, IIf(Request("idContrato"), Request("idContrato"), 0), oAdjun.Coment)
                        idArchivoContrato = arrAdjuntoContrato(0)
                        COMENTSalto = Replace(oAdjun.Coment, vbCrLf, "@VBCRLF@")
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>anyadirAdjunto('" + Request("entryid").ToString + "'," + idArchivoContrato.ToString + ",'" + Replace(fn, "'", "\'") + "','" + JSText(oAdjun.Coment) + "'," + JSNum(CLng(miFile.PostedFile.ContentLength)) + ",'" + JSText(COMENTSalto) + "','" + Request("origen").ToString + "','" & Request("IdContrato").ToString & "',1,'" + JSText(vFolderPath) + "')</script>")
                    Else
                        Dim objReturn As String()
                        Dim sReturn As String
                        Dim wsAdjuntos As New FSN_Adjuntos.FSN_Adjuntos
                        Dim file As Byte() = System.IO.File.ReadAllBytes(SaveLocation)
                        sReturn = wsAdjuntos.GrabarAdjuntoPortal(file, FSPMUser.Cod, FSPMUser.Password, FSPMUser.CodProve, fn, oAdjun.Coment, FSPMUser.Cod, miFile.PostedFile.ContentLength, FSPMUser.Idioma, Now)

                        If sReturn <> String.Empty Then
                            objReturn = Split(sReturn, "#")
                            oAdjun.Id = objReturn(0)
                            oAdjun.DataSize = objReturn(1)
                            COMENTSalto = Replace(oAdjun.Coment, vbCrLf, "@VBCRLF@")
                            Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>anyadirAdjunto('" + Request("entryid").ToString + "'," + oAdjun.Id.ToString + ",'" + Replace(oAdjun.Nom, "'", "\'") + "','" + JSText(oAdjun.Coment) + "'," + JSNum(CLng(oAdjun.DataSize / 1024)) + ",'" + JSText(COMENTSalto) + "','" + Request("origen").ToString + "','" & Request("IdContrato").ToString & "',0,'')</script>")
                        End If
                    End If
                Catch ex As Exception
                End Try
            End If
            oAdjun = Nothing

        End Sub
    End Class
End Namespace
