Namespace Fullstep.PMPortalWeb

    Partial Class ayudacampo
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here

            Dim sIdi As String = FSPMUser.Idioma
            If sIdi = Nothing Then
                sIdi = ConfigurationSettings.AppSettings("idioma")
            End If
            Dim idCampo As Integer = CInt(Request("Campo"))
            Dim lCiaComp As Long = Request.Cookies("USU")("CIACOMP")
            Dim lInstancia As Long

            If Request("Instancia") <> Nothing Then
                lInstancia = Request("Instancia")
            End If
            Dim lSolicitud As Long

            If Request("Solicitud") <> Nothing Then
                lSolicitud = Request("Solicitud")
            End If

            Dim oCampo As Fullstep.PMPortalServer.Campo


            oCampo = FSPMServer.Get_Campo()
            oCampo.Id = idCampo
            If lInstancia > 0 Then
                oCampo.LoadInst(lCiaComp, lInstancia, sIdi)
            Else
                oCampo.Load(lCiaComp, sIdi, lSolicitud)
            End If

            Me.lblTipoSolicitud.Text = oCampo.DenSolicitud(sIdi)
            Me.txtGrupo.Text = oCampo.DenGrupo(sIdi)
            Me.txtCampo.Text = oCampo.Den(sIdi)
            Me.lblAyuda.Text = oCampo.Ayuda(sIdi)

            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSPMServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.AyudaCampo, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            Me.lblTitulo.Text = oTextos.Rows(0).Item(1)
            Me.lblGrupo.Text = oTextos.Rows(1).Item(1)
            Me.lblCampo.Text = oTextos.Rows(2).Item(1)


            If oCampo.Formula <> Nothing Then
                Dim oSolicitud As Fullstep.PMPortalServer.Solicitud
                Dim oInstancia As Fullstep.PMPortalServer.Instancia
                Dim oDS As DataSet
                Dim oRow As DataRow
                Dim oCampoDesg As Fullstep.PMPortalServer.Campo
                Dim oDSDesglose As DataSet
                Dim oRowDesglose As DataRow

                Me.lblFormula.Text = oTextos.Rows(3).Item(1)

                If lInstancia = 0 Then

                    oSolicitud = FSPMServer.Get_Solicitud
                    oSolicitud.ID = oCampo.IdSolicitud
                    oSolicitud.Load(sIdi)
                    If oCampo.OrigenCalculo = Nothing And oCampo.CampoPadre = Nothing Then
                        oDS = oSolicitud.Formulario.LoadCamposCalculados(lCiaComp, oSolicitud.ID) ', fspmuser.CodPersona)
                    Else
                        oCampoDesg = FSPMServer.Get_Campo
                        If oCampo.CampoPadre <> Nothing Then
                            oCampoDesg.Id = oCampo.CampoPadre
                        Else
                            oCampoDesg.Id = oCampo.OrigenCalculo
                        End If

                        oDS = oCampoDesg.LoadCalculados(lCiaComp, oSolicitud.ID, FSPMUser.CodProve)

                    End If
                Else
                    oInstancia = FSPMServer.Get_Instancia
                    oInstancia.ID = lInstancia
                    oInstancia.Load(lCiaComp, sIdi)

                    If oCampo.OrigenCalculo = Nothing And oCampo.CampoPadre = Nothing Then

                        oDS = oInstancia.LoadCamposCalculados(lCiaComp)
                    Else

                        oCampoDesg = FSPMServer.Get_Campo
                        If oCampo.CampoPadre <> Nothing Then
                            oCampoDesg.Id = oCampo.CampoPadre
                        Else
                            oCampoDesg.Id = oCampo.OrigenCalculo

                        End If
                        oDS = oCampoDesg.LoadInstCalculados(lCiaComp, oInstancia.ID, FSPMUser.CodProve)

                    End If
                End If

                Dim sFormula As String = oCampo.Formula
                For Each oRow In oDS.Tables(0).Rows
                    sFormula = Replace(sFormula, oRow.Item("ID_CALCULO"), " " + oRow.Item("DEN_" + sIdi) + " ")
                Next

                Me.txtFormula.Text = sFormula
            Else
                Me.tblAyuda.Rows.RemoveAt(6)
                Me.tblAyuda.Rows.RemoveAt(6)
            End If

        End Sub

    End Class
End Namespace
