<%@ Page Language="vb" AutoEventWireup="false" Codebehind="materiales.aspx.vb" Inherits="Fullstep.PMPortalWeb.materiales" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
		<title>materiales</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script>


            function getNivel(oNode){
	            if (oNode.getParent()) {
	                return getNivel(oNode.getParent()) + 1;
	            } else {
	                return 1;
	            }
            }


	/*''' <summary>
	''' Seleccionar Material en la pantalla q llamo
	''' </summary>
	''' <remarks>Llamada desde:cmdAceptar; Tiempo m�ximo:0</remarks>*/
	function seleccionarMaterial()
	{
	var oTree = igtree_getTreeById("uwtMateriales")
	
	var oNode = oTree.getSelectedNode()
	if (oNode==null)
		{
		alert("hay que seleccionar un presupuesto")
		return false
		}
	var sGMNDen= oNode.getText().split(" - ")[1]
	var sGMN = new Array()
	
	var sGMN1
	var sGMN2
	var sGMN3
	var sGMN4
	var sDenAux
	var sMat
    var sDen

    if (getNivel(oNode) < NivelSeleccion && NivelSeleccion != 0) {
        if (!sMat) {
            alert(sSeleccionarMatNivel.replace("###", NivelSeleccion));
            return false;
        } else {
		    alert(sSeleccionarMatNiveloSuperior.replace("###", NivelSeleccion));
			return false;
			}
	    }


	var iNivel=0
	while (oNode)
		{
		iNivel ++
		sGMN[iNivel]=oNode.getDataKey()
		oNode = oNode.getParent()
	
		}
	j=1
	for (i=iNivel;i>0;i--)
		{
		eval("sGMN" + i.toString() + "=sGMN[" + j.toString() + "]")
		j++
}
        	p = window.opener
        	if (p.location.pathname.indexOf('DetalleFactura.aspx') > 0) {
        	    if (sGMN4) {
        	        sMat = sGMN1 + " " + sGMN2 + " " + sGMN3 + " " + sGMN4
        	        sDenAux = sGMN4 + " - " + sGMNDen
                }
        	    else {
        	        if (sGMN3) {
        	            sMat = sGMN1 + " " + sGMN2 + " " + sGMN3
        	            sDenAux = sGMN3 + " - " + sGMNDen
        	        }
        	        else {
        	            if (sGMN2) {
        	                sMat = sGMN1 + " " + sGMN2
        	                sDenAux = sGMN2 + " - " + sGMNDen
        	            }
        	            else {
        	                if (sGMN1) {
        	                    sMat = sGMN1
        	                    sDenAux = sGMN1 + " - " + sGMNDen
        	                }
        	                else {
        	                    sMat = ""
        	                    sDenAux = sGMNDen
        	                }
        	            }
        	        }
                }
               
        	    var sidTxtMaterial = document.getElementById("IDCONTROL").value
        	    var sidHidMaterial = document.getElementById("hid_txtMaterial").value
        	    var ohidMaterial = p.document.getElementById(sidHidMaterial)
        	    var otxtMaterial = p.document.getElementById(sidTxtMaterial)
        	    if (otxtMaterial) {
        	        ohidMaterial.value = sMat;
        	        otxtMaterial.value = sDenAux;
        	    }
        	} else {
        	    window.opener.mat_seleccionado(document.getElementById("IDCONTROL").value, sGMN1, sGMN2, sGMN3, sGMN4, iNivel, sGMNDen, TodosNiveles)
        	}
	window.close()
	

	}

		</script>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<ignav:ultrawebtree id="uwtMateriales" style="Z-INDEX: 101; LEFT: 16px; TOP: 16px"
				runat="server" Width="100%" CssClass="igTree" Height="496px">
				<SelectedNodeStyle ForeColor="White" BackColor="Navy"></SelectedNodeStyle>
				<Levels>
					<ignav:Level Index="0"></ignav:Level>
					<ignav:Level Index="1"></ignav:Level>
				</Levels>
			</ignav:ultrawebtree>
			<TABLE id="Table1" style="Z-INDEX: 109; LEFT: 8px; TOP: 512px" cellSpacing="1"
				cellPadding="1" width="100%" border="0">
				<TR>
					<TD align="center"><INPUT id="cmdAceptar" onclick="seleccionarMaterial()" type="button" value="Seleccionar"
							name="cmdAceptar" runat="server" class="boton"></TD>
					<TD align="center"><INPUT id="cmdCancelar" onclick="window.close()" type="button" value="Cancelar" name="cmdCancelar"
							runat="server" class="boton"></TD>
				</TR>
			</TABLE>
			<input runat="server" id="IDCAMPO" type="hidden" name="IDCAMPO"> <INPUT runat="server" id="IDCONTROL" type="hidden" name="IDCAMPO">
            <input runat="server" id="hid_txtMaterial" type="hidden"/>
		</form>
	</body>
</html>
