Namespace Fullstep.PMPortalWeb
    Partial Class downloadvisible
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")

            Dim oUser As Fullstep.PMPortalServer.User
            oUser = Session("FS_Portal_User")
            Me.txtFichero.Text = Request("nombre")
            Dim sIdi As String = Session("FS_Portal_User").Idioma
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If

            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSWSServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DownloadFile, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)
            Me.lblTitulo.Text = oTextos.Rows(0).Item(1)
            Me.lblFichero.Text = oTextos.Rows(1).Item(1)
            Me.lblSize.Text = oTextos.Rows(2).Item(1)
            Me.lblInstruc1.Text = oTextos.Rows(3).Item(1)
            Me.lblInstruc2.Text = oTextos.Rows(4).Item(1)

            Me.txtSize.Text = modUtilidades.FormatNumber(CDbl(Request("datasize")) / 1024, oUser.NumberFormat)

            Dim imgFotos As New System.Web.UI.WebControls.Image

            imgFotos = New System.Web.UI.WebControls.Image
            imgFotos.ImageUrl = "images/download.gif"
            imgFotos.BorderWidth = Unit.Pixel(0)
            lnkDownload1.Controls.Add(imgFotos)

            imgFotos = New System.Web.UI.WebControls.Image
            imgFotos.ImageUrl = "images/download.gif"
            imgFotos.BorderWidth = Unit.Pixel(0)
            lnkDownload2.Controls.Add(imgFotos)

            If InStr(Request("path"), "_") > 0 AndAlso Not Split(Request("path"), "_")(1) = FSPMUser.IdCia Then Exit Sub
            Me.lnkDownload1.NavigateUrl = ConfigurationManager.AppSettings("rutanormal") + "/download/" + Request("path") + "/" + Request("nombre")
            Me.lnkDownload2.NavigateUrl = ConfigurationManager.AppSettings("rutanormal") + "/download/" + Request("path") + "/" + Request("nombre")
        End Sub

    End Class
End Namespace