﻿
Namespace Fullstep.PMPortalWeb

    Partial Class AyudaCampo
        Inherits FSPMPage

        ''' <summary>
        ''' Carga de la pantalla
        ''' </summary>
        ''' <param name="sender">pantalla</param>
        ''' <param name="e">parametro de sistema</param>            
        ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim idCampo As Integer = CInt(Request("Campo"))
            Dim lInstancia As Long
            Dim lSolicitud As Long
            Dim oCampo As Fullstep.PMPortalServer.Campo

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.AyudaCampo

            If Request("Instancia") <> Nothing Then lInstancia = Request("Instancia")
            If Request("Solicitud") <> Nothing Then lSolicitud = Request("Solicitud")
            Dim lCiaComp As Long = IdCiaComp

            oCampo = FSPMServer.Get_Campo()
            oCampo.Id = idCampo
            If lInstancia > 0 Then
                oCampo.LoadInst(lCiaComp, lInstancia, Idioma)
            Else
                oCampo.Load(lCiaComp, Idioma, lSolicitud)
            End If

            Me.lblTitulo.Text = Textos(0) & " " & oCampo.DenSolicitud(Idioma)
            Me.lblSubtitulo.Text = Textos(1) & " " & oCampo.DenGrupo(Idioma) & " - " & Textos(2) & " " & oCampo.Den(Idioma)
            Me.lblAyuda.Text = VB2HTML(oCampo.Ayuda(Idioma))

            If oCampo.Formula <> Nothing Then
                Dim oSolicitud As Fullstep.PMPortalServer.Solicitud
                Dim oInstancia As Fullstep.PMPortalServer.Instancia
                Dim oDS As DataSet
                Dim oRow As DataRow
                Dim oCampoDesg As Fullstep.PMPortalServer.Campo

                If lInstancia = Nothing Then
                    oSolicitud = FSPMServer.Get_Solicitud
                    oSolicitud.ID = oCampo.IdSolicitud
                    oSolicitud.Load(lCiaComp, Idioma)
                    If oCampo.OrigenCalculo = Nothing And oCampo.CampoPadre = Nothing Then
                        oDS = oSolicitud.Formulario.LoadCamposCalculados(lCiaComp, oSolicitud.ID, FSPMUser.Idioma)
                    Else
                        oCampoDesg = FSPMServer.Get_Campo
                        If oCampo.CampoPadre <> Nothing Then
                            oCampoDesg.Id = oCampo.CampoPadre
                        Else
                            oCampoDesg.Id = oCampo.OrigenCalculo
                        End If
                        oDS = oCampoDesg.LoadCalculados(lCiaComp, oSolicitud.ID)
                    End If
                Else
                    oInstancia = FSPMServer.Get_Instancia
                    oInstancia.ID = lInstancia
                    oInstancia.Load(lCiaComp, Idioma)

                    If oCampo.OrigenCalculo = Nothing And oCampo.CampoPadre = Nothing Then
                        oDS = oInstancia.LoadCamposCalculados(lCiaComp)
                    Else
                        oCampoDesg = FSPMServer.Get_Campo
                        If oCampo.CampoPadre <> Nothing Then
                            oCampoDesg.Id = oCampo.CampoPadre
                        Else
                            oCampoDesg.Id = oCampo.OrigenCalculo
                        End If
                        oDS = oCampoDesg.LoadInstCalculados(lCiaComp, lInstancia, FSPMUser.CodProve)
                    End If
                End If

                Dim sFormula As String = oCampo.Formula
                For Each oRow In oDS.Tables(0).Rows
                    sFormula = Replace(sFormula, oRow.Item("ID_CALCULO"), " " + oRow.Item("DEN_" + Idioma.ToString) + " ")
                Next

                Me.hrTop.Visible = True
                Me.hrBottom.Visible = True
                Me.lblFormulaCalculo.Visible = True
                Me.lblFormula.Visible = True
                Me.lblFormulaCalculo.Text = Textos(3)
                Me.lblFormula.Text = sFormula
            Else
                Me.lblFormulaCalculo.Visible = False
                Me.lblFormula.Visible = False
            End If

        End Sub


    End Class

End Namespace