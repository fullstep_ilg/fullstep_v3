﻿Public Class BuscadorUsuarios
    Inherits FSPMPage
    Private lCiaComp As Long
    Private m_sCualquiera As String
    Private m_sUsuario As String
    Private m_lRol As Integer
    Private m_lInstancia As Integer

    ''' <summary>
    ''' Carga la pagina con los usuarios a traves del usercontrol wucUsuarios.ascx
    ''' </summary>
    ''' <param name="sender">pagina</param>
    ''' <param name="e">evento</param>
    ''' <remarks>Llamada desde: DetalleFactura.aspx    wucBusquedaAvanzadaFacturas.ascx; Tiempo máximo: 0,3</remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lCiaComp = IdCiaComp

        If Request("idFilaGrid") <> Nothing Then FilaGrid.Value = Request("idFilaGrid")
        If Request("Rol") <> Nothing Then m_lRol = CInt(Request("Rol"))
        If Request("Instancia") <> Nothing Then m_lInstancia = CInt(Request("Instancia"))
        If Request("IDControl") <> Nothing Then IDCONTROL.Value = Request("IdControl")

        If Request("idFilaGrid") Is Nothing AndAlso Request("idControl") Is Nothing Then
            'DetalleFactura.aspx -> window.open ("../_common/BuscadorUsuarios.aspx?Rol=" + Rol + "&Instancia=" + document.getElementById("Instancia").value + "&idFilaGrid=" + Rol  ,
            'VisorFacturas.aspx.vb ->  Busqueda.RutaBuscadorGestor = "../_common/BuscadorUsuarios.aspx"
            '       wucBusquedaAvanzadaFacturas.ascx.vb -> window.open('" & _RutaBuscadorGestor & If(InStr(_RutaBuscadorGestor, "?") > 0, "&", "?") & "Desde=IM&idControl=" & txtGestor.ClientID & "&idHidControl=" & hid_Gestor.ClientID & "',
            'Eoc -> No hay donde devolver datos luego se corta.
            Response.End()
        End If

        If Not Page.IsPostBack Then
            Dim oUnidadesUO As Fullstep.PMPortalServer.UnidadesOrg
            oUnidadesUO = FSPMServer.get_UnidadesOrg
            oUnidadesUO.CargarUnidadesOrganizativas(lCiaComp, Idioma, "")
            Usuarios.UnidadesOrgDs = oUnidadesUO.Data
            Usuarios.CargarUnidadesOrganizativas()
        End If

        ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Usuarios
        Usuarios.Textos = TextosModuloCompleto
    End Sub

    ''' <summary>
    ''' Carga la propiedad con los departamentos
    ''' </summary>
    ''' <param name="sUon1"></param>
    ''' <param name="sUon2"></param>
    ''' <param name="sUon3"></param>
    ''' <remarks></remarks>
    Private Sub CargarDepartamentos(ByVal sUon1 As String, ByVal sUon2 As String, ByVal sUon3 As String) Handles Usuarios.eventCargarDepartamentos
        Dim oDepartamentos As Fullstep.PMPortalServer.Departamentos
        Dim iNivel As Integer


        oDepartamentos = FSPMServer.Get_Departamentos
        If Not sUon3 = Nothing Then
            iNivel = 3
        ElseIf Not sUon2 = Nothing Then
            iNivel = 2
        ElseIf Not sUon1 = Nothing Then
            iNivel = 1
        Else
            iNivel = 0
        End If

        oDepartamentos.LoadData(lCiaComp, iNivel, sUon1, sUon2, sUon3, "")
        Usuarios.DepartamentosDt = oDepartamentos.Data.Tables(0)

    End Sub


    ''' <summary>
    ''' Procedimiento que carga el datatable de personas
    ''' </summary>
    ''' <param name="sNombre"></param>
    ''' <param name="sApe"></param>
    ''' <param name="sUon1"></param>
    ''' <param name="sUon2"></param>
    ''' <param name="sUon3"></param>
    ''' <param name="sDep"></param>
    ''' <remarks></remarks>
    Private Sub CargarPersonas(ByVal sNombre As String, ByVal sApe As String, ByVal sUon1 As String, ByVal sUon2 As String, ByVal sUon3 As String, ByVal sDep As String) Handles Usuarios.eventCargarPersonas
        Dim oPersonas As Fullstep.PMPortalServer.Personas
        oPersonas = FSPMServer.Get_Personas


        oPersonas.LoadData(lCiaComp, , sNombre, sApe, sUon1, sUon2, sUon3, sDep)
        If oPersonas.Data.Tables.Count > 0 Then
            Dim PK(0) As DataColumn
            PK(0) = oPersonas.Data.Tables(0).Columns("COD")
            oPersonas.Data.Tables(0).PrimaryKey = PK
            Usuarios.PersonasDs = oPersonas.Data
        End If
    End Sub

    
End Class