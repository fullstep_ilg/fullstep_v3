Imports System.Data.SqlClient
Imports System.Data.SqlTypes
Imports System.IO
Imports System.Security.Principal
Namespace Fullstep.PMPortalWeb
    Partial Class downloadfile
        Inherits FSPMPage

#Region "Variables Suplantacion"

        Private Const LOGON32_LOGON_INTERACTIVE As Integer = 2
        Private Const LOGON32_PROVIDER_DEFAULT As Integer = 0

        Declare Function LogonUserA Lib "advapi32.dll" (ByVal lpszUsername As String, _
                                ByVal lpszDomain As String, _
                                ByVal lpszPassword As String, _
                                ByVal dwLogonType As Integer, _
                                ByVal dwLogonProvider As Integer, _
                                ByRef phToken As IntPtr) As Integer

        Declare Auto Function DuplicateToken Lib "advapi32.dll" ( _
                                ByVal ExistingTokenHandle As IntPtr, _
                                ByVal ImpersonationLevel As Integer, _
                                ByRef DuplicateTokenHandle As IntPtr) As Integer

        Declare Auto Function RevertToSelf Lib "advapi32.dll" () As Long
        Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Long

#End Region

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' <summary>
        ''' Devolver el Id de la compania
        ''' </summary>
        ''' <returns>Id de la compania</returns>
        ''' <remarks>Llamada desde: property oSolicitud property oInstancia property oCertificado; Tiempo m�ximo:0</remarks>
        Protected ReadOnly Property lCiaComp As Long
            Get
                lCiaComp = IdCiaComp
            End Get
        End Property

        ''' <summary>
        ''' Carga la pantalla. Si el path pasado no es valido, no se descarga de nada
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server") 'Solo lo quiero para autenticar
            If FSWSServer Is Nothing Then Exit Sub

            'Si el proveedor que esta intentando descargar el adjunto es el mismo que tiene acceso/creo
            If Not Split(Request("src"), "_")(1) = FSPMUser.IdCia Then Exit Sub

            If Request("idContrato") <> "" Then
                TratarAdjuntoContrato()
            Else
                Dim mpath As String = ConfigurationManager.AppSettings("rutanormal")
                Dim servidor As String = Request.ServerVariables("SERVER_NAME")
                mpath = Replace(mpath, "http://" & servidor, "")
                mpath = Replace(mpath, "https://" & servidor, "")

                Dim sPath As String = mpath & "download/" '' en downloadhidden & Request("path") & "/" aqui no
                Dim RutaAceptada As String = Server.MapPath(sPath)

                Dim sFichero As String = Server.UrlDecode(RutaAceptada & Request("src") & "/") & Server.UrlDecode(Request("nombre"))
                If Not System.IO.File.Exists(sFichero) Then
                    sFichero = Server.UrlDecode(RutaAceptada & Request("src") & "/") & Server.UrlEncode(Request("nombre"))
                    If Not System.IO.File.Exists(sFichero) Then
                        sFichero = Server.UrlDecode(RutaAceptada & Request("src") & "/") & Request("nombre")
                    End If
                End If
                sFichero = Replace(sFichero, "../", "")
                sFichero = Replace(sFichero, "..\", "")
                If Not (Left(sFichero, Len(RutaAceptada)) = RutaAceptada) Then
                    Exit Sub
                End If

                TratarAdjunto(sFichero)
            End If
        End Sub

        ''' <summary>
        ''' Lee el fichero del adjunto de Contrato a descargar y llama a la funcion que lo descargara
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub TratarAdjuntoContrato()
            Dim oAdjunto As PMPortalServer.Adjunto
            oAdjunto = FSPMServer.Get_Adjunto
            Dim impersonationContext As WindowsImpersonationContext

            FSPMServer.Impersonate()
            impersonationContext = RealizarSuplantacion(FSPMServer.ImpersonateLogin, FSPMServer.ImpersonatePassword, FSPMServer.ImpersonateDominio)

            Dim buffer As Byte()
            buffer = oAdjunto.LeerContratoAdjunto(lCiaComp, Request("idContrato"), Request("idArchivoContrato"))
            MostrarAdjunto(buffer)

            If Not impersonationContext Is Nothing Then
                impersonationContext.Undo()
            End If

            oAdjunto = Nothing
        End Sub
        ''' <summary>
        ''' Lee el fichero del adjunto a descargar y llama a la funcion que lo descargara
        ''' </summary>
        ''' <param name="sFichero">fichero con ruta completa</param>
        ''' <remarks></remarks>
        Private Sub TratarAdjunto(ByVal sFichero As String)
            Dim oFileStream As System.IO.FileStream
            oFileStream = New System.IO.FileStream(sFichero, IO.FileMode.Open)

            Dim byteBuffer() As Byte
            ReDim byteBuffer(oFileStream.Length - 1)
            Call oFileStream.Read(byteBuffer, 0, Int32.Parse(oFileStream.Length.ToString()))

            oFileStream.Close()

            MostrarAdjunto(byteBuffer)
        End Sub

        ''' <summary>
        ''' descargara el fichero
        ''' </summary>
        ''' <param name="buffer">fichero</param>
        ''' <remarks>Llamada desde: TratarAdjunto TratarAdjuntoContrato ; Tiempo m�ximo: 0,2</remarks>
        Private Sub MostrarAdjunto(ByVal buffer As Byte())
            Call Me.Response.AddHeader("Content-Type", "application/octet-stream")
            Call Me.Response.AddHeader("Content-Disposition", "attachment;filename=""" & Request("fname") & """")
            Call Me.Response.BinaryWrite(buffer)
            Call Me.Response.End()
        End Sub
    End Class
End Namespace