<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ayudacampo_old.aspx.vb" Inherits="Fullstep.PMPortalWeb.ayudacampo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
		<title>ayudacampo</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE runat="server" id="tblAyuda" width="100%">
				<tr>
					<td>
						<asp:label id="lblTitulo" runat="server" CssClass="captionBlue"> Solicitud:</asp:label>
						<asp:label id="lblTipoSolicitud" runat="server" CssClass="captionDarkBlue">Tipo de solicitud</asp:label>
					</td>
				</tr>
				<tr>
					<td style="FONT-SIZE:2px">
						<HR width="100%" size="2" class="captionBlue">
					</td>
				</tr>
				<TR>
					<TD>
						<asp:label id="lblGrupo" runat="server" CssClass="captionBlue"> Grupo:</asp:label>
						<asp:label id="txtGrupo" runat="server" CssClass="captionDarkBlue"></asp:label>
					</TD>
				</TR>
				<TR>
					<TD>
						<asp:label id="lblCampo" runat="server" CssClass="captionBlue"> Campo:</asp:label>
						<asp:label id="txtCampo" runat="server" CssClass="captionDarkBlue"></asp:label>
					</TD>
				</TR>
				<TR>
					<TD>
						&nbsp;
					</TD>
				</TR>
				<TR>
					<TD>
						<asp:label id="lblAyuda" runat="server" CssClass="parrafo"> bla bla bla bla bla</asp:label>
					</TD>
				</TR>
				<TR>
					<TD>
						&nbsp;
					</TD>
				</TR>
				<TR>
					<TD style="BORDER-RIGHT: gray 2px solid; BORDER-TOP: gray 2px solid; BORDER-LEFT: gray 2px solid; BORDER-BOTTOM: gray 2px solid">
						<table>
							<tr>
								<td>
									<asp:label id="lblFormula" runat="server" CssClass="captionBlue">F�rmula del c�lculo</asp:label>
								</td>
							</tr>
							<tr>
								<td>
									<asp:label id="txtFormula" runat="server" CssClass="parrafo"> bla bla bla bla bla</asp:label>
								</td>
							</tr>
						</table>
					</TD>
				</TR>
				<tr>
					<td style="FONT-SIZE:2px">
						<HR width="100%" size="2" class="captionBlue">
					</td>
				</tr>
			</TABLE>
		</form>
	</body>
</html>
