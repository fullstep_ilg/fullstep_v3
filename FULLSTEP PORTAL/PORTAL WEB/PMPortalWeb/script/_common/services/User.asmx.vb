﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Fullstep

<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
 Public Class User
    Inherits System.Web.Services.WebService

    ''' <summary>
    ''' Devuelve el usuario conectado
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_Usuario_CN() As PMPortalServer.User
        Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
        Return CN_Usuario
    End Function
End Class