﻿Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);

/*
''' Revisado por: blp. Fecha: 31/10/2011
''' Función que se lanza en el momento en que se inicia una solicitud al servidor y muestra, cuando así se ha configurado un panel que indica que una carga está en curso
''' llamada desde: El evento de request
''' Máximo 0,1 seg.
*/
function beginReq(sender, args) {
    // shows the Popup
    var Emisor;
    var Comparacion;
    var Mostrar = 'si';
    if (String(args._postBackElement.id).search("wdgEntregas") != -1)
        if (ColCheck == 1) {
            Mostrar = 'no';
            ColCheck = 0;
        }
    if (String(args._postBackElement.id).search("ibExcel") != -1)
        Mostrar = 'no';
    if (String(args._postBackElement.id).search("ibPDF") != -1)
        Mostrar = 'no';

    if (Mostrar == 'si') {
    $find(ModalProgress).show();
    }
}

function endReq(sender, args) {
    //  shows the Popup 
    $find(ModalProgress).hide();
} 
