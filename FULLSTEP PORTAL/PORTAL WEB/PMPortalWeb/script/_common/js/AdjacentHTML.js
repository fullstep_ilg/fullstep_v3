﻿if (typeof HTMLElement != "undefined" && !HTMLElement.prototype.insertAdjacentElement) {
    /*
    ''' <summary>
    ''' En FireFox no se admite insertAdjacentHTML. Hay q "crear" el protipo de 
    ''' la función insertAdjacentHTML para q funcione. Esta insertAdjacentHTML
    ''' usa insertAdjacentElement para funcionar por ello también se debe crear el 
    ''' prototipo de insertAdjacentElement
    ''' </summary>
    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0</remarks>*/
    HTMLElement.prototype.insertAdjacentElement = function(where, parsedNode) {
        switch (where) {
            case 'beforeBegin':
                this.parentNode.insertBefore(parsedNode, this)
                break;
            case 'afterBegin':
                this.insertBefore(parsedNode, this.firstChild);
                break;
            case 'beforeEnd':
                this.appendChild(parsedNode);
                break;
            case 'afterEnd':
                if (this.nextSibling)
                    this.parentNode.insertBefore(parsedNode, this.nextSibling);
                else this.parentNode.appendChild(parsedNode);
                break;
        }
    }
    /*
    ''' <summary>
    ''' En FireFox no se admite insertAdjacentHTML. Hay q "crear" el protipo de 
    ''' la función insertAdjacentHTML para q funcione.
    ''' </summary>
    ''' <remarks>Llamada desde: sistema; Tiempo máximo:0</remarks>*/
    HTMLElement.prototype.insertAdjacentHTML = function(where, htmlStr) {
        var r = this.ownerDocument.createRange();
        var parsedHTML = r.createContextualFragment(htmlStr);
        this.insertAdjacentElement(where, parsedHTML)
    }
}
