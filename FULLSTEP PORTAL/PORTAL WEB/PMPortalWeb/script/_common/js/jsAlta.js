var sIdEntryMaterial;
var oGridsDesglose = new Array();
var arrEstadosInternos = new Array();
var arrFechasSuministros = new Array();
var arrGSOcultos = new Array();
var sDataEntryMaterialFORM;
//Revisado por: blp. Fecha: 10/10/2011
//''' <summary>
//''' Muestra un desglose popup
//''' </summary>
//''' <param name="text0">id del control hipervinculo</param>
//''' <param name="text1">Client Id de la pagina</param>
//''' <param name="text2">string ##newIndex## o numero de linea donde se encuentra el hipervinculo</param>
//''' <param name="text3">Id del desglose</param>
//''' <param name="tipo">tipo de desglose</param>
//''' <param name="habilitaCopiar">Si se podra copiar lineas o no</param>
//''' <remarks>Llamada desde:javascript introducido para responder al onclick de todo boton hipervinculo para mostrar desgloses; Tiempo m�ximo: 0</remarks>
function popupDesgloseClickEvent(text0, text1, text2, text3, tipo, habilitaCopiar, event) {
    var x = event.clientX + document.body.scrollLeft
    var y = event.clientY + document.body.scrollTop

    var scrollTop = getScrollTop()
    y = event.clientY - ((event.offsetY) ? event.offsetY : 0) + scrollTop
    scrollTop = null;


    if (tipo == 'dcha')
    { x = x - "136" + "px;" }

    if (habilitaCopiar > 0) {
        mostrarFRAME("ddpopupDesglose", x, y, 136, 24)
        var oddpopupDesglose = document.getElementById("ddpopupDesglose")
        if (oddpopupDesglose.src == undefined)
            oddpopupDesglose.location = "../_common/popupDesglose_Delete.aspx?FrameId=ddpopupDesglose&celda=" + text0 + "&ClientID=" + text1 + "&index=" + text2 + "&idCampo=" + text3
        else
            oddpopupDesglose.src = "../_common/popupDesglose_Delete.aspx?FrameId=ddpopupDesglose&celda=" + text0 + "&ClientID=" + text1 + "&index=" + text2 + "&idCampo=" + text3
    }
    else {
        mostrarFRAME("ddpopupDesglose", x, y, 136, 46)
        var oddpopupDesglose = document.getElementById("ddpopupDesglose")
        if (oddpopupDesglose.src == undefined)
            oddpopupDesglose.location = "../_common/popupDesglose.aspx?FrameId=ddpopupDesglose&celda=" + text0 + "&ClientID=" + text1 + "&index=" + text2 + "&idCampo=" + text3
        else
            oddpopupDesglose.src = "../_common/popupDesglose.aspx?FrameId=ddpopupDesglose&celda=" + text0 + "&ClientID=" + text1 + "&index=" + text2 + "&idCampo=" + text3
    }
}
//Revisado por: blp. Fecha: 10/10/2011
//''' <summary>
//''' A�ade una fila a un desglose. Si hay linea por defecto la linea nueva tiene los datos por defecto 
//''' rellenos, sino la linea va en blanco. Para Qa, el estado interno de la fila nueva es 1 (sin revisar).
//''' </summary>
//''' <param name="sRoot">nombre entry del desglose</param>
//''' <param name="idCampo">id de bbdd del desglose</param>        
//''' <param name="ProveCod">Cod de bbdd del proveedor</param>
//''' <param name="ProveDen">Descrip de bbdd del proveedor</param>  
//''' <param name="lIdContacto">id de bbdd del contacto del proveedor</param>
//''' <param name="sDenContacto">Descrip de bbdd del contacto del proveedor</param>  
//''' <remarks>Llamada desde:javascript introducido para responder al onclick de todo boton 'a�adir fila' de todo desglose
//'''; Tiempo m�ximo: 0</remarks>
function copiarFilaVacia(sRoot, idCampo, ProveCod, ProveDen, lIdContacto, sDenContacto) {

    var bMensajePorMostrar = document.getElementById("bMensajePorMostrar")
    if (bMensajePorMostrar)
        if (bMensajePorMostrar.value == "1") {
            bMensajePorMostrar.value = "0";
            return;
        }
    bMensajePorMostrar = null;

    var s
    //uwtGrupos__ctl0__ctl0
    var sClase = document.getElementById(sRoot + "_tblDesglose").rows[document.getElementById(sRoot + "_tblDesglose").rows.length - 1].cells[0].className
    if (sClase == "ugfilatablaDesglose")
        sClase = "ugfilaalttablaDesglose"
    else
        sClase = "ugfilatablaDesglose"

    var oTR = document.getElementById(sRoot + "_tblDesglose").insertRow(-1)

    if (document.getElementById(sRoot + "_numRows")) {
        lIndex = parseFloat(document.getElementById(sRoot + "_numRows").value) + 1
        document.getElementById(sRoot + "_numRows").value = lIndex
    }
    else {
        lIndex = document.getElementById(sRoot + "_tblDesglose").rows.length - 1
        s = "<input type=hidden name=" + sRoot + "_numRows id=" + sRoot + "_numRows value=" + lIndex.toString() + ">"
        document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", s)
    }

    if (document.getElementById(sRoot + "_numRowsGrid")) {
        lLineaGrid = parseFloat(document.getElementById(sRoot + "_numRowsGrid").value) + 1
        document.getElementById(sRoot + "_numRowsGrid").value = lLineaGrid
        lLineaGrid = lIndex;
    }
    else {
        lLineaGrid = document.getElementById(sRoot + "_tblDesglose").rows.length - 1
        s = "<input type=hidden name=" + sRoot + "_numRowsGrid id=" + sRoot + "_numRowsGrid value=" + lLineaGrid.toString() + ">"
        document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", s)
    }



    //Controlar las filas que se a�aden para optimizar MontarFormularioSubmit
    var ultimaFila = 0
    for (var indice in htControlFilas) {
        if (String(idCampo) == indice.substring(0, indice.indexOf("_"))) {
            if (parseInt(ultimaFila) < parseInt(htControlFilas[indice])) {
                ultimaFila = htControlFilas[indice]
            }
        }
    }

    htControlFilas[String(idCampo) + "_" + String(lIndex)] = String(parseInt(ultimaFila) + 1)
    ultimaFila = null;


    //A continuacion se obtiene lPrimeraLinea, que es la fila de donde se copian los datos
    var lPrimeraLinea
    if (lLineaGrid == 1) {
        lPrimeraLinea = lIndex
        var js = "<input type=hidden name=" + sRoot + "_numPrimeraLinea id=" + sRoot + "_numPrimeraLinea value=" + lIndex.toString() + ">"
        document.forms[0].insertAdjacentHTML("beforeEnd", js)
        js = null;
    }
    else {

        if (document.getElementById(sRoot + "_numPrimeraLinea"))
            lPrimeraLinea = document.getElementById(sRoot + "_numPrimeraLinea").value
        else
            lPrimeraLinea = 1
    }

    lPrimeraLinea = parseInt(lPrimeraLinea.toString(), 10)

    //En este array se guardan en cada fila el nuevo data entry y del que se va acoger el valor (_1_)
    var arraynuevafila = new Array()
    var insertarnuevoelemento = 0
    var VecesEntre = 0

    for (var i = 0; i < document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length; i++) {
        arraynuevafila[i] = new Array()
        for (var j = 0; j < 2; j++)
            arraynuevafila[i][j] = ""
    }



    for (i = 0; i < document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length; i++) {
        s = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].innerHTML
        var oTD = oTR.insertCell(-1)
        oTD.className = sClase
        if (document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].id)
            oTD.id = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].id + '_' + lIndex;

        ///////////////////
        var entryActual
        var stemp = s
        var stemp2
        var tempinicio
        if (stemp.search(' id="') > 0) { //En FireFox los ids vienen con comillas
            tempinicio = stemp.search(' id="')
            tempinicio = tempinicio + 5
        } else {
            tempinicio = stemp.search(" id=") //En IE los ids vienen sin comillas
            tempinicio = tempinicio + 4
        }
        stemp2 = stemp.substr(tempinicio, stemp.length - tempinicio)
        var tempfin = stemp2.search("__tbl")
        entryActual = stemp2.substr(0, tempfin)




        var re = "class=trasparent"
        if (s.search(re) >= 0) {
            var den = '';
            var valor = '';
            var inicio = s.search(sRoot + "_fsentry");
            if (inicio > 1) {
                var cadena = s.substr(inicio + sRoot.length + 8)
                var fin = cadena.indexOf("_")
                cadena = cadena.substring(0, fin)
                if (document.getElementById(sRoot + "_fsdsentry_" + lPrimeraLinea + "_" + cadena) == '[object]') {
                    etiqueta = document.getElementById(sRoot + "_fsdsentry_" + lPrimeraLinea + "_" + cadena).innerHTML;
                    inicio = etiqueta.indexOf("<SPAN>") + 6
                    if (inicio < 6) { //no existe <SPAN> sino <SPAN class="TipoArchivo">
                        inicio = etiqueta.indexOf("<SPAN")
                        fin = etiqueta.indexOf(">", inicio)
                        inicio = inicio + (fin - inicio) + 1; //etiqueta.indexOf("<SPAN ") + 24
                    }
                    fin = etiqueta.indexOf("</SPAN>");
                    den = etiqueta.substring(inicio, fin);
                    etiqueta = null;
                    if (den == '&nbsp;') {
                        den = '';
                        s = s.replace("<IMG src='" + rutanormal + "script/_common/images/trespuntos.gif' align=middle border=0 unselectable=\"on\">", "");
                        s = s.replace("BACKGROUND-IMAGE: url(" + rutanormal + "script/_common/images/trespuntos.gif);", "display: none;");
                    }
                }
                if (document.getElementById(sRoot + "_" + lPrimeraLinea + "_" + cadena) == '[object]') {
                    valor = document.getElementById(sRoot + "_" + lPrimeraLinea + "_" + cadena).value;
                }
            }

            if (valor != '') {
                if (s.search("__h type=hidden") >= 0)
                    s = s.replace("__h type=hidden", "__h type='hidden' value='" + valor + "'")
                if (s.search("__hAct type=hidden") >= 0) //para tipo archivo
                    s = s.replace("__hAct type=hidden", "__hAct type='hidden' value='" + valor + "'")
                if (s.search("__hNew type=hidden") >= 0) //para tipo archivo
                    s = s.replace("__hNew type=hidden", "__hNew type='hidden'")
            }
            if (den != '') {
                if (s.search("</TEXTAREA") >= 0)
                    s = s.replace("</TEXTAREA", den + "</TEXTAREA")
                else
                    s = s.replace("readOnly", "readOnly value='" + den + "'")

                while (s.search("type=hidden") >= 0)
                    s = s.replace("type=hidden", "type='hidden' value='" + den + "'")
                while (s.indexOf('[\"\"') >= 0)
                    s = s.replace('[\"\"', '[\"' + den + '\"')
            }

        }
        den = null;
        valor = null;
        inicio = null;
        cadena = null;
        ///////////////////	

        re = /fsentry/
        while (s.search(re) >= 0) {
            s = s.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }



        /////////////////////////////////////////////////////////
        // revisar si es necesario mantener este c�digo
        se = /<STYLE type=text\/css>/
        if (s.search(se) > 0) {
            while ((s.search(se) > 0)) {
                se = /<STYLE type=text\/css>/
                iComienzo = s.search(se)
                se = /<\/STYLE>/
                iFin = s.search(se)
                var sStyle = s.substr(iComienzo + 21, iFin - (iComienzo + 21))
                s = s.substr(0, iComienzo) + s.substr(iFin + 8)

                rllave = /{/
                rllaveCierre = /}/
                rpunto = /./
                sAux = sStyle

                while (sAux.search(rllave) > -1) {
                    sSelector = sAux.substr(0, sAux.search(rllave))
                    sStyleNew = sAux.substr(sAux.search(rllave) + 1, sAux.search(rllaveCierre) - sAux.search(rllave) - 1)
                    document.styleSheets[document.styleSheets.length - 1].addRule(sSelector, sStyleNew)
                    sAux = sAux.substr(sAux.search(rllaveCierre) + 2)
                }


            }
        }
        // Fin revisar
        /////////////////////////////////////////////////////////


        //Para evitar generar los contenidos de los webdropdown con los mismos IDs y a fin de que podamos localizar esos datos con m�s facilidad, vamos a editar los IDs
        var webdropdownReplace = '';
        var finID = s.search(".0:mkr:Target");
        if (finID > -1) {
            var extraer = s.substr(0, finID);
            var inicioID = extraer.lastIndexOf("x:");
            if (inicioID > -1) {
                extraer = extraer.substr(inicioID + 2, finID)
                webdropdownReplace = extraer;
                extraer = null;
            }
            inicioID = null;
        }
        finID = null;
        if (webdropdownReplace != '') {
            var numAleatorio = Aleatorio(1, parseInt(webdropdownReplace))
            re = new RegExp() // empty constructor
            re.compile(webdropdownReplace, 'gi')
            s = s.replace(re, parseInt(webdropdownReplace) + parseInt(numAleatorio))
            //Reemplazar la clase seleccionada para que no se seleccione nada por defecto al copiar l�nea vac�a
            re.compile('igdd_FullstepListItemSelected', 'gi')
            s = s.replace(re, parseInt(webdropdownReplace) + parseInt(numAleatorio))
            numAleatorio = null;
        }
        webdropdownReplace = null;

        var se = /<SCRIPT language=javascript>/
        var se2 = /<script language="javascript">/
        var arrJS = new Array()
        if ((s.search(se) > 0) || (s.search(se2) > 0)) {
            while (s.search(se) > 0) {
                var iComienzo = s.search(se) + 28
                se = /<\/SCRIPT>/
                var iFin = s.search(se)
                var sEval = s.substr(iComienzo, iFin - iComienzo)
                s = s.substr(0, iComienzo - 28) + s.substr(iFin + 9)
                arrJS[arrJS.length] = sEval
                sEval = null;
                se = /<SCRIPT language=javascript>/
            }
            while (s.search(se2) > 0) {
                var iComienzo = s.search(se2) + 30
                se = /<\/script>/
                var iFin = s.search(se)
                var sEval = s.substr(iComienzo, iFin - iComienzo)
                s = s.substr(0, iComienzo - 30) + s.substr(iFin + 9)
                arrJS[arrJS.length] = sEval
                sEval = null;
                se2 = /<script language="javascript">/
            }
            ni = /##newIndex##/g
            if (s.search(ni) > 0) {
                oTD.align = "center"
            }
            s = replaceAll(s, "##newIndex##", lIndex.toString())

            oTD.insertAdjacentHTML("beforeEnd", s)

            for (var k = 0; k < arrJS.length; k++) {
                eval(arrJS[k])
            }
        }
        else {
            ni = /##newIndex##/g
            if (s.search(ni) > 0) {
                oTD.align = "center"
            }
            s = replaceAll(s, "'##newIndex##'", lIndex.toString())
            oTD.insertAdjacentHTML("beforeEnd", s)
        }
        se = null;
        iComienzo = null;
        iFin = null;
        arrJS = null;
        ni = null;

        //Aqui es donde se introducen en el array los controles	
        stemp = s
        stemp2
        tempinicio
        if (stemp.search(' id="') > 0) { //En FireFox los ids vienen con comillas
            tempinicio = stemp.search(' id="')
            tempinicio = tempinicio + 5
        } else {
            tempinicio = stemp.search(" id=") //En IE los ids vienen sin comillas
            tempinicio = tempinicio + 4
        }
        stemp2 = stemp.substr(tempinicio, stemp.length - tempinicio)
        tempfin = stemp2.search("__tbl")
        var mientry = stemp2.substr(0, tempfin)

        var a = new Array();
        var entry1;
        var entryControl;
        var numero
        a = mientry.split("_")
        if ((a.length == 7) || (a.length == 4) || (a.length == 9)) {
            entry1 = mientry
            entryControl = mientry
            if (a.length == 7) //cuando no es popup
                numero = a[5]
            else
                if (a.length == 4) //Cuando es popup

                    numero = a[2]
                else
                    if (a.length == 9) //Cuando el desglose no es una ventana emergente
                        numero = a[7]

            numero = parseInt(numero.toString(), 10)

            var cadenaareemplazar = "_fsdsentry_" + numero + "_"
            entry1 = entry1.replace(cadenaareemplazar, "_fsdsentry_" + lPrimeraLinea.toString() + "_")

            arraynuevafila[insertarnuevoelemento][0] = mientry
            arraynuevafila[insertarnuevoelemento][1] = entry1

            var oContr;
            oContr = fsGeneralEntry_getById(arraynuevafila[insertarnuevoelemento][1])
            while (!oContr && VecesEntre == 0 && numero > lPrimeraLinea) {
                lPrimeraLinea = lPrimeraLinea + 1
                entry1 = entryControl.replace(cadenaareemplazar, "_fsdsentry_" + lPrimeraLinea.toString() + "_")
                arraynuevafila[insertarnuevoelemento][1] = entry1
                oContr = fsGeneralEntry_getById(arraynuevafila[insertarnuevoelemento][1])
            }

            var entryACopiar = entry1.replace("fsdsentry_" + lPrimeraLinea.toString() + "_", "fsentry")
            oContr = fsGeneralEntry_getById(entryACopiar)

            if (oContr) {
                if ((oContr.tipoGS != 119) && (oContr.tipoGS != 118) && (oContr.tipoGS != 104) && (oContr.tipoGS != 127) && (oContr.tipoGS != 121) && (oContr.tipoGS != 116) && (oContr.intro == 1) && (oContr.readOnly == false)) {
                    var param;
                    if (a.length == 9)
                        param = a[0] + '$_' + a[2] + '$' + a[3] + '$' + a[4] + '_' + a[5] + '$' + a[6] + '_' + a[7] + '_' + a[8] + '$_t';
                    else
                        if (a.length >= 6)
                            param = a[0] + '$_' + a[2] + '$' + a[3] + '$' + a[4] + '_' + a[5] + '$' + a[6] + '$_t';
                    if (a[0] == "ucDesglose")
                        param = a[0] + '$' + a[1] + '$_t';

                    var bClientBinding;
                    switch (oContr.tipoGS) {
                        case 101: //Forma pago
                        case 102: //Moneda
                        case 105: //Unidad
                        case 107: //Pais
                        case 108: //Provincia
                        case 109: //Destino
                        case 122: //Departamento
                        case 123: //Org. Compras
                        case 124: //Centro
                        case 125: //Almac�n
                        case 143: //Proveedor ERP
                            bClientBinding = true;
                            break;
                        default:
                            bClientBinding = false;
                            break;
                    }

                    if ((oContr.tipoGS == 0) && (oContr.tipo != 2) && (oContr.tipo != 3) && (oContr.tipo != 4))
                        bClientBinding = true;

                    var valores;
                    if (bClientBinding == false) {
                        var fin;
                        var codigo = document.documentElement.innerHTML;
                        var posicion = codigo.search("Infragistics.Web.UI.WebDropDown")
                        if (posicion > 0) {
                            codigo = codigo.substring(posicion)
                            posicion = codigo.search(entryACopiar)
                            if (posicion > 0) {
                                codigo = codigo.substring(posicion)
                                posicion = codigo.search("{'0':")
                                fin = codigo.search(']]}]')
                                if ((posicion > 0) && (fin > 0))
                                    valores = codigo.substring(posicion, fin + 3)
                            }
                        }
                        fin = null;
                        codigo = null;
                        posicion = null;
                    }

                    if (bClientBinding == true) {
                        //Cuidado: las clases en este caso son diferentes. Ejemplo: ig_FullstepDisabled en vez de ig_FullstepPMWebDisabled,...
                        Sys.Application.add_init(function () {
                            $create(Infragistics.Web.UI.WebDropDown, { "clientbindingprops": [[[, 1, 120, , , , , , , , 1, , 1, , 2, 0, , , 0, , 300, 120, , "", , , , 6, , 1, , , , , , , , , , 0, , , 1, 0, , , 0, , , , 1], { "c": { "dropDownInputFocusClass": "igdd_FullstepValueDisplayFocus", "dropDownButtonPressedImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonPressed.png", "_pi": "[\"" + InfraStylePath + "/Fullstep/images/ig_ajaxIndicator.gif\",,,\"ig_FullstepPMWebAjaxIndicator\",,1,,,,\"ig_FullstepPMWebAjaxIndicatorBlock\",,3,,3,\"Async post\"]", "pageCount": 0, "vse": 0, "textField": "Texto", "controlAreaHoverClass": "ig_FullstepPMWebHover igdd_FullstepControlHover", "controlAreaClass": "igdd_FullstepControlArea", "valueField": "Valor", "controlClass": "ig_FullstepPMWebControl igdd_FullstepControl", "dropDownInputHoverClass": "ig_FullstepPMWebHover igdd_FullstepValueDisplayHover", "dropDownItemDisabled": "igdd_FullstepListItemDisabled", "uid": mientry + "_t", "nullTextCssClass": "igdd_FullstepNullText", "controlAreaFocusClass": "igdd_FullstepControlFocus", "dropDownItemHover": "igdd_FullstepListItemHover", "dropDownButtonNormalImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButton.png", "dropDownInputClass": "igdd_FullstepValueDisplay", "controlDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepControlDisabled", "dropDownButtonDisabledImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonDisabled.png", "dropDownItemClass": "igdd_FullstepListItem", "pi": "[,,9,,1,1,,80,,,,3,,3,\"Async post\"]", "dropDownItemSelected": "igdd_FullstepListItemSelected", "ocs": 1, "dropDownButtonClass": "igdd_FullstepDropDownButton", "dropDownItemActiveClass": "igdd_FullstepListItemActive", "dropDownButtonHoverImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonHover.png", "dropDownInputDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepValueDisplayDisabled" } }], , [{}], ["InputKeyDown:WebDropDown_KeyDown", "DropDownOpening:WebDropDown_DataCheck", "SelectionChanged:WebDropDown_SelectionChanging", "Blur:WebDropDown_Focus", "ValueChanged:WebDropDown_valueChanged"], []], "id": mientry + "__t", "name": "_t" }, null, null, $get(mientry + "__t"));
                        });
                    } else {
                        Sys.Application.add_init(function () {
                            $create(Infragistics.Web.UI.WebDropDown, { "id": mientry + "__t", "name": "_t", "props": [[[, 1, 160, , , , , -1, , , 1, , 1, , 2, 0, , , 0, , , 0, , "", , , , 6, , 1, , , , , , , , 0, , 0, , -1, 0, 0, , 0, 0, , , ], { "c": { "controlDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepControlDisabled", "controlAreaClass": "igdd_FullstepControlArea", "controlAreaHoverClass": "ig_FullstepPMWebHover igdd_FullstepControlHover", "vse": 0, "dropDownInputHoverClass": "ig_FullstepPMWebHover igdd_FullstepValueDisplayHover", "dropDownItemHover": "igdd_FullstepListItemHover", "dropDownButtonNormalImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButton.png", "pi": "[]", "dropDownButtonHoverImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonHover.png", "dropDownButtonPressedImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonPressed.png", "dropDownInputClass": "igdd_FullstepValueDisplay", "dropDownItemClass": "igdd_FullstepListItem", "dropDownInputDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepValueDisplayDisabled", "controlClass": "ig_FullstepPMWebControl igdd_FullstepControl", "dropDownInputFocusClass": "igdd_FullstepValueDisplayFocus", "dropDownButtonDisabledImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonDisabled.png", "pageCount": 4, "controlAreaFocusClass": "igdd_FullstepControlFocus", "dropDownItemDisabled": "igdd_FullstepListItemDisabled", "ocs": 1, "nullTextCssClass": "igdd_FullstepNullText", "dropDownButtonClass": "igdd_FullstepDropDownButton", "uid": param, "dropDownItemSelected": "igdd_FullstepListItemSelected", "dropDownItemActiveClass": "igdd_FullstepListItemActive" } }], , eval("[" + valores + "]"), ["DropDownOpening:WebDropDown_DropDownOpening", "SelectionChanged:WebDropDown_SelectionChanging", "Blur:WebDropDown_Focus", "ValueChanged:WebDropDown_valueChanged"]] }, null, null, $get(mientry + "__t"));
                        });
                    }
                    Sys.Application.add_load(function () { $find(mientry + "__t").behavior.set_enableMovingTargetWithSource(false) })

                    param = null;
                    valores = null;

                }
            }
            entryACopiar = null;
            //las no conf grabadas tienen cols 1020 y 1010 que me obligan
            VecesEntre = 1

            insertarnuevoelemento = insertarnuevoelemento + 1
        }
        oContr = null;
        a = null;
        mientry = null;
        entry1 = null;
        entryControl = null;
        numero = null;

    }
    s = "<input type=hidden name=" + sRoot + "_" + lIndex + "_Deleted id=" + sRoot + "_" + lIndex + "_Deleted value='no'>"
    oTD.insertAdjacentHTML("beforeEnd", s)
    oTD = null;
    entryActual = null;
    stemp = null;
    stemp2 = null;
    tempinicio = null;
    tempfin = null;

    var iNumCeldas //indica el n�mero de celdas de la tabla 

    var iCeldaIni
    if (ProveCod) {
        iCeldaIni = 0;
        iNumCeldas = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length // si estamos a�adiendo un proveedor para emitirle un certificado est�n todas las celdas
    }
    else {
        iCeldaIni = 1;
        iNumCeldas = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length - 2 //si es una linea de desglose estan todas menos la �ltima que es el bot�n de eliminar
    }

    for (i = 0; i < iNumCeldas; i++) {
        re = /fsentry/
        //------------------------------------------------------------------------------------------------------------
        var s2 = eval("arrEntrys_" + idCampo + "[i]")
        if (!s2)
            continue;
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrCalculados") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrObligatorios") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrArticulos") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
            if (!(s2.search("arrArticulos\[iNumArts][0]=\"\"") > 0))
                s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrPres1") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrPres2") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrPres3") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrPres4") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }

        s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")

        if (s2.search(", 104,") > 0 || s2.search(", 108,") > 0 || s2.search(", 114,") > 0) {
            s2 = s2.replace(re, "fsdsentryx" + lIndex.toString() + "x") //en el caso del art�culo,de la provincia y del contacto del proveedor, la grid del dropdown es �nica para cada fila
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_") //tambien es �nico el campo dependiente (dependentfield)
        }
        eval(s2)
        //------------------------------------------------------------------------------------------------------------			
        if (s2.search("arrObligatorios") > 0) {
            var oEntry = fsGeneralEntry_getById(arrObligatorios[arrObligatorios.length - 1])
            oEntry.basedesglose = false
        }

    }

    //Esto ocurre al a�adir un proveedor al que emitir un certificado
    //A�ade los datos del proveedor:
    oEntry = fsGeneralEntry_getById(sRoot + "_fsdsentry_" + lIndex.toString() + "_PROVE")
    if (oEntry) {
        oEntry.setValue(ProveCod + ' - ' + ProveDen)
        oEntry.setDataValue(ProveCod)
    }

    //A�ade los datos del contacto:
    oEntry = fsGeneralEntry_getById(sRoot + "_fsdsentry_" + lIndex.toString() + "_CON")
    if (oEntry) {
        oEntry.setValue(sDenContacto)
        oEntry.setDataValue(lIdContacto)
    }

    //Se hace la asignacion de valores para la nueva fila   --------------------------------------------------------
    var o;
    var o1;
    //var sIDArt = '';var sIDDen = '';
    //var sIdPrecio = null;var sIdProveedor = null;var sIdUnidad=null;		

    for (i = 0; i < insertarnuevoelemento; i++) {
        o = fsGeneralEntry_getById(arraynuevafila[i][0])
        o1 = fsGeneralEntry_getById(arraynuevafila[i][1])
        if (o) {
            o.nColumna = i + iCeldaIni;
            o.nLinea = lLineaGrid;
            if (o.errMsg) {
                o.errMsg = o.errMsg.replace('\n', '\\n');
                o.errMsg = o.errMsg.replace('\n', '\\n');
            }

            //
            if (document.getElementById(sRoot + "_numRowsGrid").value == 1) {
                if (o.getDataValue() != '') {
                    o.PoseeDefecto = true
                    o.TextoValorDefecto = o.getValue()
                    if ((o.tipoGS == 8 || o.tipo == 8))
                        o.DataValorDefecto = o.getDataValue()[0] //devuelve un array en el caso de los ficheros
                    else
                        o.DataValorDefecto = o.getDataValue()
                }

                if (o1) {
                    if ((o.PoseeDefecto == false) && (o1.PoseeDefecto == true)) {
                        o.PoseeDefecto = o1.PoseeDefecto
                        o.TextoValorDefecto = o1.TextoValorDefecto
                        o.DataValorDefecto = o1.DataValorDefecto
                    }
                }
            } else {
                if (o1) {
                    o.PoseeDefecto = o1.PoseeDefecto
                    o.TextoValorDefecto = o1.TextoValorDefecto
                    o.DataValorDefecto = o1.DataValorDefecto
                }
            }
            //	

            if ((o.tipoGS == 8 || o.tipo == 8)) { //Fichero ADJUNTO				
                var vaciar = new Array();
                vaciar[0] = "";
                vaciar[1] = "";

                if (o.PoseeDefecto == true) {
                    o.setValue(o.TextoValorDefecto);
                    vaciar[0] = o.DataValorDefecto;
                    vaciar[1] = "";
                    //Si es un adjunto se tiene que generar un nuevo id para el adjunto/s copiados

                    CreateXmlHttp();
                    if (xmlHttp) {
                        var params = "accion=9&AdjuntosAct=" + vaciar[0] + "&AdjuntosNew=" + vaciar[1]

                        xmlHttp.open("POST", "../_common/GestionaAjax.aspx", false);
                        xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                        xmlHttp.send(params);

                        var retorno;
                        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                            retorno = xmlHttp.responseText;
                            vaciar[0] = "";
                            vaciar[1] = retorno;
                            o.setDataValue(vaciar)
                        }
                    }
                }
                else {
                    o.setValue('');
                    o.setDataValue(vaciar);
                }
            }
            else {
                switch (o.tipoGS) {
                    case 119: //Cod Articulo Nuevo 
                    case 104: //CodArt Antiguo
                    case 118:
                        switch (o.tipoGS) {
                            case 119: //Cod Articulo Nuevo 
                            case 104: //CodArt Antiguo
                                o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_")
                                break;
                            case 118:
                                o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_")
                                break;
                        }
                        if (o.idDataEntryDependent) {
                            if (o.idDataEntryDependent.search("fsentry_") < 0) { //La organizacion de compras de encuentra fuera del desglose
                                oEntry = fsGeneralEntry_getById(o.idDataEntryDependent)
                                if ((oEntry) && (oEntry.tipoGS == 123))
                                    oEntry.idDataEntryDependent = arraynuevafila[i][0];
                            }
                            o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        }
                        if (o.idDataEntryDependent2)
                            o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.idEntryPREC) {
                            o.idEntryPREC = o.idEntryPREC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        }
                        if (o.idEntryPROV) {
                            o.idEntryPROV = o.idEntryPROV.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        }
                        if (o.idEntryCANT)
                            o.idEntryCANT = o.idEntryCANT.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.idEntryUNI)
                            o.idEntryUNI = o.idEntryUNI.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.idEntryCC)
                            o.idEntryCC = o.idEntryCC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        break;
                    case 6:
                    case 7:
                        if (o.idDataEntryDependent)
                            o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        break;
                    case 100: //Proveedores
                        if (o.dependentfield)
                            o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.idDataEntryDependent2)
                            o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        break;
                    case 103:
                        if (o.idDataEntryDependent2)
                            o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        break;
                    case 123:
                        if (o.dependentfield)
                            o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.idDataEntryDependent)
                            o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.idDataEntryDependent2)//org-centro
                            o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        break;
                    case 107: //pais
                        if (o.dependentfield)
                            o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                        break;
                    case 124: //centro
                        if (o.idDataEntryDependent) {
                            o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            var oEntryArt = fsGeneralEntry_getById(o.idDataEntryDependent)
                            if ((oEntryArt) && (oEntryArt.idDataEntryDependent)) {
                                if (oEntryArt.idDataEntryDependent.search("fsentry_") < 0) { //La organizacion de compras de encuentra fuera del desglose
                                    oEntry = fsGeneralEntry_getById(oEntryArt.idDataEntryDependent)
                                    if ((oEntry) && (oEntry.tipoGS == 123))
                                        oEntry.idDataEntryDependent2 = arraynuevafila[i][0];
                                }
                            }
                        }
                        if (o.idDataEntryDependent3)
                            o.idDataEntryDependent3 = o.idDataEntryDependent3.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                        break;
                    case 45:
                        o.MonRepercutido = o.MonCentral;
                        o.CambioRepercutido = o.CambioCentral;
                        if (o.readOnly == false) {
                            var lnk = document.getElementById(o.dependentfield)
                            lnk.innerHTML = o.MonCentral;
                        }
                        break;
                    case 110:
                    case 111:
                    case 112:
                    case 113:
                        if (o.readOnly) {
                            if (o1) {
                                o.PoseeDefecto = true
                                o.TextoValorDefecto = o1.getValue()
                                o.DataValorDefecto = o1.getDataValue()
                            }
                        }
                        break;
                    case 105: //Unidad
                        if (o.idEntryCANT)
                            o.idEntryCANT = o.idEntryCANT.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        break;
                    case 4: //Cantidad
                        o.NumeroDeDecimales = o1.NumeroDeDecimales
                        o.UnidadOculta = o1.UnidadOculta
                        break;
                }

                if (o.PoseeDefecto == true) {
                    if ((o.TextoValorDefecto == "") && (o.DataValorDefecto != "") && ((o.tipo == 2) || (o.tipo == 3) || ((o.tipo == 5 || o.tipo == 6 || o.tipo == 7))) && (o.intro == 0)) {
                        o.TextoValorDefecto = o.DataValorDefecto
                    }

                    o.setValue(o.TextoValorDefecto);
                    o.setDataValue(o.DataValorDefecto);
                    //Nuevo campo/atrib en alta/desglose tienes Texto null Data valor
                    if (o.TextoValorDefecto == "") {
                        if ((o.tipo == 4) && (o.getValue() == "")) {
                            o.setValue(o.DataValorDefecto);
                            if (o.DataValorDefecto == "1")
                                o.setValue(sTextoSi);
                            else
                                o.setValue(sTextoNo);
                        }
                    }
                }
                else {
                    if (o.TextoValorDefecto == "") {
                        o.setValue('')
                    }
                    else {
                        if (o.TextoValorDefecto == null) {
                            o.setValue('')
                        }
                        else {
                            o.setValue(o.TextoValorDefecto);
                        }
                    }
                    o.setDataValue('');
                }

                if (o.tipoGS == 43) {
                    o.setValue(arrEstadosInternos[1][1]);
                    o.setDataValue(1);
                }
                if (o.tipoGS == 3000) {
                    o.setValue(o.nLinea);
                };
                if (o.readOnly == false) {
                    var sID = ''
                    if (o.Editor) {
                        if (o.Editor.ID) {
                            sID = o.Editor.ID
                        } else {
                            sID = o.Editor.id
                        }
                        if (sID != '' && sID != undefined) {
                            if (o.getDataValue() != '' && o.getDataValue() != null) {
                                copiarSeleccionWebDropDown(sID, o.getDataValue())
                            } else {
                                CambiarSeleccion(sID, "BORRAR")
                            }
                        }
                    }
                    sID = null;
                }
            }
            if (o.masterField && o.masterField != '')
                o.masterField = o.masterField.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");

        }
    }
}
// Revisado por: blp. Fecha: 10/10/2011
//''' <summary>	
//	Copia una fila seleccionada de un desglose en una fila nueva. Para Qa, el estado interno de la 
//	fila nueva es 1 (sin revisar).
//''' </summary>
//''' <param name="sRoot">Nombre entry del desglose</param> 
//''' <param name="index">Id de la fila seleccionada</param> 
//''' <param name="idCampo">Id de bbdd del desglose</param> 
//''' <param name="ProveCod">Cod de bbdd del proveedor</param> 
//''' <param name="ProveDen">Descrip de bbdd del proveedor</param> 
//''' <param name="lIdContacto">Id de bbdd del contacto del proveedor</param> 
//''' <param name="sDenContacto">Descrip de bbdd del contacto del proveedor</param> 
//''' <remarks>Llamada desde:javascript introducido para responder al onclick de todo boton 'copiar fila' 
//'''		de todo desglose; Tiempo m�ximo:0</remarks>
function copiarFilaSeleccionada(sRoot, index, idCampo, ProveCod, ProveDen, lIdContacto, sDenContacto) {
    var _arDependenceRelations = new Array()
    var bMensajePorMostrar = document.getElementById("bMensajePorMostrar")
    if (bMensajePorMostrar)
        if (bMensajePorMostrar.value == "1") {
            bMensajePorMostrar.value = "0";
            return;
        }
    bMensajePorMostrar = null;
    var s
    //uwtGrupos__ctl0__ctl0
    var sClase = document.getElementById(sRoot + "_tblDesglose").rows[document.getElementById(sRoot + "_tblDesglose").rows.length - 1].cells[0].className
    if (sClase == "ugfilatablaDesglose")
        sClase = "ugfilaalttablaDesglose"
    else
        sClase = "ugfilatablaDesglose"

    var oTR = document.getElementById(sRoot + "_tblDesglose").insertRow(-1)

    var lIndex;
    if (document.getElementById(sRoot + "_numRows")) {
        lIndex = parseFloat(document.getElementById(sRoot + "_numRows").value) + 1
        document.getElementById(sRoot + "_numRows").value = lIndex
    }
    else {
        lIndex = document.getElementById(sRoot + "_tblDesglose").rows.length - 1
        s = "<input type=hidden name=" + sRoot + "_numRows id=" + sRoot + "_numRows value=" + lIndex.toString() + ">"
        document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", s)
    }

    var lLineaGrid;
    if (document.getElementById(sRoot + "_numRowsGrid")) {
        lLineaGrid = parseFloat(document.getElementById(sRoot + "_numRowsGrid").value) + 1
        document.getElementById(sRoot + "_numRowsGrid").value = lLineaGrid
        lLineaGrid = lIndex;
    }
    else {
        lLineaGrid = document.getElementById(sRoot + "_tblDesglose").rows.length - 1
        s = "<input type=hidden name=" + sRoot + "_numRowsGrid id=" + sRoot + "_numRowsGrid value=" + lLineaGrid.toString() + ">"
        document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", s)
    }



    //Controlar las filas que se a�aden para optimizar MontarFormularioSubmit
    var ultimaFila = 0
    var filaNum;
    for (var indice in htControlFilas) {
        if (String(idCampo) == indice.substring(0, indice.indexOf("_"))) {
            filaNum = indice.substr(indice.lastIndexOf("_") + 1);
            if (parseInt(ultimaFila) < parseInt(filaNum)) {
                ultimaFila = htControlFilas[String(idCampo) + "_" + String(filaNum)];
            }
        };
    };

    htControlFilas[String(idCampo) + "_" + String(lIndex)] = String(parseInt(ultimaFila) + 1)
    ultimaFila = null;


    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
    //A continuacion se obtiene lPrimeraLinea, que es la fila de donde se copian los datos
    var lPrimeraLinea = index
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    //En este array se guardan en cada fila el nuevo data entry y del que se va acoger el valor (_1_)
    var arraynuevafila = new Array()
    var insertarnuevoelemento = 0
    for (var i = 0; i < document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length; i++) {
        arraynuevafila[i] = new Array()
        for (var j = 0; j < 2; j++)
            arraynuevafila[i][j] = ""
    }



    for (i = 0; i < document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length; i++) {
        s = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].innerHTML
        var oTD = oTR.insertCell(-1)
        oTD.className = sClase
        if (document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].id)
            oTD.id = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].id + '_' + lIndex;


        ///////////////////
        var sArchivo
        var entryActual
        var stemp = s
        var stemp2
        var tempinicio
        var tempfin
        if (stemp.search(' id="') > 0) { //En FireFox los ids vienen con comillas
            tempinicio = stemp.search(' id="')
            tempinicio = tempinicio + 5
        } else {
            tempinicio = stemp.search(" id=") //En IE los ids vienen sin comillas
            tempinicio = tempinicio + 4
        }
        stemp2 = stemp.substr(tempinicio, stemp.length - tempinicio)
        tempfin = stemp2.search("__tbl")
        entryActual = stemp2.substr(0, tempfin)

        var entryArchivo = entryActual;
        var re = /fsentry/
        while (entryArchivo.search(re) >= 0) {
            entryArchivo = entryArchivo.replace(re, "fsdsentry_" + lPrimeraLinea.toString() + "_")
        }
        var oContrAr = null;
        if (entryArchivo != '')
            oContrAr = fsGeneralEntry_getById(entryArchivo)
        entryArchivo = null;
        if (oContrAr) {
            if (oContrAr.tipo == 8) {
                sArchivo = s
                s = oContrAr.Element.parentNode.innerHTML
                while (s.search("fsdsentry_" + lPrimeraLinea.toString() + "_") >= 0) {
                    s = s.replace("fsdsentry_" + lPrimeraLinea.toString() + "_", "fsdsentry_" + lIndex.toString() + "_")
                }
            }
        }


        re = "class=trasparent"
        if (s.search(re) >= 0) {
            var den = '';
            var valor = '';
            var inicio = s.search(sRoot + "_fsentry");
            if (inicio > 1) {
                var cadena = s.substr(inicio + sRoot.length + 8)
                var fin = cadena.indexOf("_")
                cadena = cadena.substring(0, fin)
                if (document.getElementById(sRoot + "_fsdsentry_" + index + "_" + cadena) == '[object]') {
                    var etiqueta = document.getElementById(sRoot + "_fsdsentry_" + index + "_" + cadena).innerHTML;
                    inicio = etiqueta.indexOf("<SPAN>") + 6
                    fin = etiqueta.indexOf("</SPAN>");
                    den = etiqueta.substring(inicio, fin);
                    etiqueta = null;
                    if (den == '&nbsp;') {
                        den = '';
                        s = s.replace("<IMG src='" + rutanormal + "script/_common/images/trespuntos.gif' align=middle border=0 unselectable=\"on\">", "");
                        s = s.replace("BACKGROUND-IMAGE: url(" + rutanormal + "script/_common/images/trespuntos.gif);", "display: none;");
                    }
                }
                if (document.getElementById(sRoot + "_" + index + "_" + cadena) == '[object]') {
                    valor = document.getElementById(sRoot + "_" + index + "_" + cadena).value;
                }
                cadena = null;
                fin = null;
            }

            if (valor != '') {
                if (s.search("__h type=hidden") >= 0)
                    s = s.replace("__h type=hidden", "__h type='hidden' value='" + valor + "'")
            }
            if (den != '') {
                if (s.search("</TEXTAREA") >= 0)
                    s = s.replace("</TEXTAREA", den + "</TEXTAREA")
                else
                    s = s.replace("readOnly", "readOnly value='" + den + "'")
                while (s.search("type=hidden") >= 0)
                    s = s.replace("type=hidden", "type='hidden' value='" + den + "'")
                while (s.indexOf('[\"\"') >= 0)
                    s = s.replace('[\"\"', '[\"' + den + '\"')
            }
            den = null;
            valor = null;
            inicio = null;

        }
        ///////////////////	


        re = /fsentry/
        while (s.search(re) >= 0) {
            s = s.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }



        /////////////////////////////////////////////////////////
        // revisar si es necesario mantener este c�digo
        se = /<STYLE type=text\/css>/
        if (s.search(se) > 0) {
            while ((s.search(se) > 0)) {
                se = /<STYLE type=text\/css>/
                var iComienzo = s.search(se)
                se = /<\/STYLE>/
                var iFin = s.search(se)
                var sStyle = s.substr(iComienzo + 21, iFin - (iComienzo + 21))
                s = s.substr(0, iComienzo) + s.substr(iFin + 8)
                iComienzo = null;
                iFin = null;
                var rllave = /{/
                var rllaveCierre = /}/
                rpunto = /./
                sAux = sStyle
                sStyle = null;

                while (sAux.search(rllave) > -1) {
                    sSelector = sAux.substr(0, sAux.search(rllave))
                    sStyleNew = sAux.substr(sAux.search(rllave) + 1, sAux.search(rllaveCierre) - sAux.search(rllave) - 1)
                    document.styleSheets[document.styleSheets.length - 1].addRule(sSelector, sStyleNew)
                    sAux = sAux.substr(sAux.search(rllaveCierre) + 2)
                }
                rllave = null;
                rllaveCierre = null;
                sAux = null;

            }
        }
        // Fin revisar
        /////////////////////////////////////////////////////////

        /******** WEBDROPDOWNS RENDERIZADOS EN EL CLIENTE ****************/
        //Si el control tiene un control dependiente, a�adimos ambos a un array de maestros - dependientes q usaremos m�s tarde
        if (oContrAr) {
            if (oContrAr.dependentfield) {
                var oDependentField = fsGeneralEntry_getById(oContrAr.dependentfield)
                if (oDependentField) {
                    _arDependenceRelations[_arDependenceRelations.length] = new Array(oContrAr.id, oContrAr.dependentfield, oDependentField.idCampo)
                    oDependentField = null;
                }
            }
        }

        //Para evitar generar los contenidos de los webdropdown con los mismos IDs y a fin de que podamos localizar esos datos con m�s facilidad, vamos a editar los IDs
        var webdropdownReplace = '';
        var finID = s.search(".0:mkr:Target");
        if (finID > -1) {
            var extraer = s.substr(0, finID);
            var inicioID = extraer.lastIndexOf("x:");
            if (inicioID > -1) {
                extraer = extraer.substr(inicioID + 2, finID)
                webdropdownReplace = extraer;
                extraer = null;
            }
            inicioID = null;
        }
        finID = null;
        if (webdropdownReplace != '') {
            var numAleatorio = Aleatorio(1, parseInt(webdropdownReplace))
            var re = new RegExp() // empty constructor
            re.compile(webdropdownReplace, 'gi')
            s = s.replace(re, parseInt(webdropdownReplace) + parseInt(numAleatorio))
            numAleatorio = null;
        }
        webdropdownReplace = null;


        var se = /<SCRIPT language=javascript>/
        var se2 = /<script language="javascript">/
        var arrJS = new Array()
        if ((s.search(se) > 0) || (s.search(se2) > 0)) {
            while (s.search(se) > 0) {
                iComienzo = s.search(se) + 28
                se = /<\/SCRIPT>/
                iFin = s.search(se)
                var sEval = s.substr(iComienzo, iFin - iComienzo)
                s = s.substr(0, iComienzo - 28) + s.substr(iFin + 9)
                iComienzo = null;
                iFin = null;
                arrJS[arrJS.length] = sEval
                sEval = null;
                se = /<SCRIPT language=javascript>/
            }
            while (s.search(se2) > 0) {
                var iComienzo = s.search(se2) + 30
                se = /<\/script>/
                var iFin = s.search(se)
                var sEval = s.substr(iComienzo, iFin - iComienzo)
                s = s.substr(0, iComienzo - 30) + s.substr(iFin + 9)
                arrJS[arrJS.length] = sEval
                sEval = null;
                se2 = /<script language="javascript">/
            }
            ni = /##newIndex##/g
            if (s.search(ni) > 0) {
                oTD.align = "center"
            }
            s = replaceAll(s, "##newIndex##", lIndex.toString())

            oTD.insertAdjacentHTML("beforeEnd", s)

            for (k = 0; k < arrJS.length; k++) {
                eval(arrJS[k])
            }
        }
        else {
            ni = /##newIndex##/g
            if (s.search(ni) > 0) {
                oTD.align = "center"
            }
            s = replaceAll(s, "'##newIndex##'", lIndex.toString())
            oTD.insertAdjacentHTML("beforeEnd", s)
        }

        //Aqui es donde se introducen en el array los controles	
        var stemp = s
        var stemp2
        var tempinicio
        if (stemp.search(' id="') > 0) { //En FireFox los ids vienen con comillas
            tempinicio = stemp.search(' id="')
            tempinicio = tempinicio + 5
        } else {
            tempinicio = stemp.search(" id=") //En IE los ids vienen sin comillas
            tempinicio = tempinicio + 4
        }
        stemp2 = stemp.substr(tempinicio, stemp.length - tempinicio)
        tempfin = stemp2.search("__tbl")
        mientry = stemp2.substr(0, tempfin)

        var a = new Array();
        var entry1;
        entry1 = null;
        var numero
        a = mientry.split("_")
        var otmp;
        var o1tmp;
        if ((a.length == 7) || (a.length == 4) || (a.length == 9)) {
            entry1 = mientry
            if (a.length == 7) //cuando no es popup
                numero = a[5]
            else
                if (a.length == 4) //Cuando es popup

                    numero = a[2]
                else
                    if (a.length == 9) //Cuando el desglose no es una ventana emergente
                        numero = a[7]


            var cadenaareemplazar = "_fsdsentry_" + numero + "_"
            entry1 = entry1.replace(cadenaareemplazar, "_fsdsentry_" + lPrimeraLinea.toString() + "_")

            arraynuevafila[insertarnuevoelemento][0] = mientry
            arraynuevafila[insertarnuevoelemento][1] = entry1



            insertarnuevoelemento = insertarnuevoelemento + 1
        }

        if (entry1) {
            var entryACopiar = entry1.replace("fsdsentry_" + lPrimeraLinea.toString() + "_", "fsentry")
            oContr = fsGeneralEntry_getById(entryACopiar)

            if (oContr) {
                if ((oContr.tipoGS != 119) && (oContr.tipoGS != 118) && (oContr.tipoGS != 104) && (oContr.tipoGS != 127) && (oContr.tipoGS != 121) && (oContr.tipoGS != 116) && (oContr.intro == 1) && (oContr.readOnly == false)) {
                    var param;
                    if (a.length >= 6)
                        param = a[0] + '$_' + a[2] + '$' + a[3] + '$' + a[4] + '_' + a[5] + '$' + a[6] + '$_t';
                    if (a[0] == "ucDesglose")
                        param = a[0] + '$' + a[1] + '$_t';

                    var bClientBinding;
                    switch (oContr.tipoGS) {
                        case 101: //Forma pago
                        case 102: //Moneda
                        case 105: //Unidad
                        case 107: //Pais
                        case 108: //Provincia
                        case 109: //Destino
                        case 122: //Departamento
                        case 123: //Org. Compras
                        case 124: //Centro
                        case 125: //Almac�n
                        case 143: //Proveedor ERP
                            bClientBinding = true;
                            break;
                        default:
                            bClientBinding = false;
                            break;
                    }

                    if ((oContr.tipoGS == 0) && (oContr.tipo != 2) && (oContr.tipo != 3) && (oContr.tipo != 4))
                        bClientBinding = true;

                    var valores;
                    if (bClientBinding == false) {
                        var fin;
                        var codigo = document.documentElement.innerHTML;
                        var posicion = codigo.search("Infragistics.Web.UI.WebDropDown")
                        if (posicion > 0) {
                            codigo = codigo.substring(posicion)
                            posicion = codigo.search(entryACopiar)
                            if (posicion > 0) {
                                codigo = codigo.substring(posicion)
                                posicion = codigo.search("{'0':")
                                fin = codigo.search(']]}]')
                                if ((posicion > 0) && (fin > 0))
                                    valores = codigo.substring(posicion, fin + 3)
                            }
                        }

                        codigo = null;
                        fin = null;
                        posicion = null;
                    }

                    if (bClientBinding == true) {
                        //Cuidado: las clases en este caso son diferentes. Ejemplo: ig_FullstepDisabled en vez de ig_FullstepPMWebDisabled,...
                        Sys.Application.add_init(function () {
                            $create(Infragistics.Web.UI.WebDropDown, { "clientbindingprops": [[[, 1, 120, , , , , , , , 1, , 1, , 2, 0, , , 0, , 300, 120, , "", , , , 6, , 1, , , , , , , , , , 0, , , 1, 0, , , 0, , , , 1], { "c": { "dropDownInputFocusClass": "igdd_FullstepValueDisplayFocus", "dropDownButtonPressedImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonPressed.png", "_pi": "[\"" + InfraStylePath + "/Fullstep/images/ig_ajaxIndicator.gif\",,,\"ig_FullstepPMWebAjaxIndicator\",,1,,,,\"ig_FullstepPMWebAjaxIndicatorBlock\",,3,,3,\"Async post\"]", "pageCount": 0, "vse": 0, "textField": "Texto", "controlAreaHoverClass": "ig_FullstepPMWebHover igdd_FullstepControlHover", "controlAreaClass": "igdd_FullstepControlArea", "valueField": "Valor", "controlClass": "ig_FullstepPMWebControl igdd_FullstepControl", "dropDownInputHoverClass": "ig_FullstepPMWebHover igdd_FullstepValueDisplayHover", "dropDownItemDisabled": "igdd_FullstepListItemDisabled", "uid": mientry + "_t", "nullTextCssClass": "igdd_FullstepNullText", "controlAreaFocusClass": "igdd_FullstepControlFocus", "dropDownItemHover": "igdd_FullstepListItemHover", "dropDownButtonNormalImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButton.png", "dropDownInputClass": "igdd_FullstepValueDisplay", "controlDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepControlDisabled", "dropDownButtonDisabledImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonDisabled.png", "dropDownItemClass": "igdd_FullstepListItem", "pi": "[,,9,,1,1,,80,,,,3,,3,\"Async post\"]", "dropDownItemSelected": "igdd_FullstepListItemSelected", "ocs": 1, "dropDownButtonClass": "igdd_FullstepDropDownButton", "dropDownItemActiveClass": "igdd_FullstepListItemActive", "dropDownButtonHoverImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonHover.png", "dropDownInputDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepValueDisplayDisabled" } }], , [{}], ["InputKeyDown:WebDropDown_KeyDown", "DropDownOpening:WebDropDown_DataCheck", "SelectionChanged:WebDropDown_SelectionChanging", "Blur:WebDropDown_Focus", "ValueChanged:WebDropDown_valueChanged"], []], "id": mientry + "__t", "name": "_t" }, null, null, $get(mientry + "__t"));
                        });
                    } else {
                        Sys.Application.add_init(function () {
                            $create(Infragistics.Web.UI.WebDropDown, { "id": mientry + "__t", "name": "_t", "props": [[[, 1, 160, , , , , -1, , , 1, , 1, , 2, 0, , , 0, , , 0, , "", , , , 6, , 1, , , , , , , , 0, , 0, , -1, 0, 0, , 0, 0, , , ], { "c": { "controlDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepControlDisabled", "controlAreaClass": "igdd_FullstepControlArea", "controlAreaHoverClass": "ig_FullstepPMWebHover igdd_FullstepControlHover", "vse": 0, "dropDownInputHoverClass": "ig_FullstepPMWebHover igdd_FullstepValueDisplayHover", "dropDownItemHover": "igdd_FullstepListItemHover", "dropDownButtonNormalImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButton.png", "pi": "[]", "dropDownButtonHoverImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonHover.png", "dropDownButtonPressedImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonPressed.png", "dropDownInputClass": "igdd_FullstepValueDisplay", "dropDownItemClass": "igdd_FullstepListItem", "dropDownInputDisabledClass": "ig_FullstepPMWebDisabled igdd_FullstepValueDisplayDisabled", "controlClass": "ig_FullstepPMWebControl igdd_FullstepControl", "dropDownInputFocusClass": "igdd_FullstepValueDisplayFocus", "dropDownButtonDisabledImageUrl": InfraStylePath + "/Fullstep/images/igdd_DropDownButtonDisabled.png", "pageCount": 4, "controlAreaFocusClass": "igdd_FullstepControlFocus", "dropDownItemDisabled": "igdd_FullstepListItemDisabled", "ocs": 1, "nullTextCssClass": "igdd_FullstepNullText", "dropDownButtonClass": "igdd_FullstepDropDownButton", "uid": param, "dropDownItemSelected": "igdd_FullstepListItemSelected", "dropDownItemActiveClass": "igdd_FullstepListItemActive" } }], , eval("[" + valores + "]"), ["DropDownOpening:WebDropDown_DropDownOpening", "SelectionChanged:WebDropDown_SelectionChanging", "Blur:WebDropDown_Focus", "ValueChanged:WebDropDown_valueChanged"]] }, null, null, $get(mientry + "__t"));
                        });
                    }
                    Sys.Application.add_load(function () { $find(mientry + "__t").behavior.set_enableMovingTargetWithSource(false) })

                    if (oContrAr) {
                        var valorSeleccionado = oContrAr.getDataValue();
                        setTimeout('copiarSeleccionWebDropDown("' + mientry + '__t","' + JSText(valorSeleccionado) + '")', 500);
                        valorSeleccionado = null;
                    }
                    valores = null;
                    param = null;

                }
            }
            entryACopiar = null
        }
        sArchivo = null;
        entryActual = null;
        stemp = null;
        stemp2 = null;
        tempinicio = null;
        tempfin = null;
        re = null;
        oContrAr = null;
        se = null;
        oContr = null;
        campo = null;
        a = null;
        mientry = null;
        entry1 = null;
        numero = null;

    }
    sClase = null;
    sMovible = null;
    lPrimeraLinea = null;
    s = "<input type=hidden name=" + sRoot + "_" + lIndex + "_Deleted id=" + sRoot + "_" + lIndex + "_Deleted value='no'>"
    oTD.insertAdjacentHTML("beforeEnd", s)
    oTD = null;
    oTR = null;
    s = null;

    var iNumCeldas //indica el n�mero de celdas de la tabla 


    if (ProveCod) {
        iCeldaIni = 0;
        iNumCeldas = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length // si estamos a�adiendo un proveedor para emitirle un certificado est�n todas las celdas
    }
    else {
        iCeldaIni = 1;
        iNumCeldas = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length - 1 //si es una linea de desglose estan todas menos la �ltima que es el bot�n de eliminar
    }

    for (i = 0; i < iNumCeldas; i++) {

        re = /fsentry/

        var s2 = eval("arrEntrys_" + idCampo + "[i]")
        if (!s2)
            continue;
        if (s2.search("arrCalculados") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }
        if (s2.search("arrObligatorios") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }

        if (s2.search("arrArticulos") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
            if (!(s2.search("arrArticulos\[iNumArts][0]=\"\"") > 0))
                s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }

        if (s2.search("arrPres1") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }

        if (s2.search("arrPres2") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }

        if (s2.search("arrPres3") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }

        if (s2.search("arrPres4") > 0) {
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }

        s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")

        if (s2.search(", 104,") > 0 || s2.search(", 108,") > 0 || s2.search(", 114,") > 0) {
            s2 = s2.replace(re, "fsdsentryx" + lIndex.toString() + "x") //en el caso del art�culo,de la provincia y del contacto del proveedor, la grid del dropdown es �nica para cada fila
            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_") //tambien es �nico el campo dependiente (dependentfield)
        }
        eval(s2)

        if (s2.search("arrObligatorios") > 0) {
            var oEntry = fsGeneralEntry_getById(arrObligatorios[arrObligatorios.length - 1])
            oEntry.basedesglose = false
            oEntry = null;
        }
        s2 = null;
        re = null;
    }
    iNumCeldas = null;

    //Esto ocurre al a�adir un proveedor al que emitir un certificado
    //A�ade los datos del proveedor:
    oEntry = fsGeneralEntry_getById(sRoot + "_fsdsentry_" + lIndex.toString() + "_PROVE")
    if (oEntry) {
        oEntry.setValue(ProveCod + ' - ' + ProveDen)
        oEntry.setDataValue(ProveCod)
    }

    //A�ade los datos del contacto:
    oEntry = fsGeneralEntry_getById(sRoot + "_fsdsentry_" + lIndex.toString() + "_CON")
    if (oEntry) {
        oEntry.setValue(sDenContacto)
        oEntry.setDataValue(lIdContacto)
    }
    oEntry = null;

    //Se hace la asignacion de valores para la nueva fila   
    var o;
    var o1;
    //var sIDArt = '';var sIDDen = '';
    //var sIdPrecio = null;var sIdProveedor = null;var sIdUnidad=null;		

    for (i = 0; i < insertarnuevoelemento; i++) {
        o = fsGeneralEntry_getById(arraynuevafila[i][0])
        o1 = fsGeneralEntry_getById(arraynuevafila[i][1])
        if (o) {
            o.nColumna = i + iCeldaIni;
            o.nLinea = lLineaGrid;
            if (o.errMsg) {
                o.errMsg = o.errMsg.replace('\n', '\\n');
                o.errMsg = o.errMsg.replace('\n', '\\n');
            }
            //
            if (o1) {
                o.PoseeDefecto = o1.PoseeDefecto
                o.TextoValorDefecto = o1.TextoValorDefecto
                o.DataValorDefecto = o1.DataValorDefecto
            }
            //					
        }
        if ((o != null) && (o1 != null) && (lLineaGrid != 1)) {
            //esto tiene que estar antes del setValue, si no, coge los decimales de las opciones del usuario
            if (o.tipoGS == 4) { //Cantidad
                if (o1.NumeroDeDecimales == null)
                    o.Editor.decimals = 15;
                else
                    o.Editor.decimals = o1.NumeroDeDecimales;
            }


            //campo de tipo texto medio o largo
            if (o1.getValue() != "") // si no esta en blanco se le asigna el valor al campo de la linea copiada
                o.setValue(o1.getValue())

            if ((o.tipoGS == 0) && (o.intro == 1) && (o.readOnly == false)) {
                var dropDown = $find(o.Editor.id);
                if (dropDown && dropDown.set_currentValue) {
                    if (o1.getValue() == null) {
                        dropDown.set_currentValue('', true);
                        dropDown.set_selectedItemIndex(-1);
                    }
                    else {
                        copiarSeleccionWebDropDown(o.Editor.id, JSText(o1.getDataValue()))
                        dropDown.set_currentValue(o1.getValue(), true);
                    }
                }
                dropDown = null;
            }

            if ((o.tipoGS != 0) && (o.tipoGS != 119) && (o.tipoGS != 118) && (o.tipoGS != 104) && (o.tipoGS != 127) && (o.tipoGS != 121) && (o.tipoGS != 116) && (o.intro == 1) && (!o.readOnly)) {
                var dropDown = $find(o.Editor.id);
                if (dropDown && dropDown.set_currentValue) {
                    if (o1.getValue() == null || o1.getValue() == '') {
                        dropDown.set_currentValue('', true);
                        dropDown.set_selectedItemIndex(-1);
                        if (o.tipoGS == 109) {
                            document.getElementById("igtxt" + o.Editor2.ID).style.visibility = "hidden";
                        }
                    } else {
                        copiarSeleccionWebDropDown(o.Editor.id, JSText(o1.getDataValue()))
                        dropDown.set_currentValue(o1.getValue(), true);
                        if (o.tipoGS == 109) {
                            if (o1.getValue() != null) {
                                document.getElementById("igtxt" + o.Editor2.ID).style.visibility = "visible";

                            }

                        }
                    }
                }
                dropDown = null;
            }

            if (o.tipoGS == 105) { //Unidad
                if (o.idEntryCANT)
                    o.idEntryCANT = o.idEntryCANT.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
            }

            if (o.tipoGS == 4) {    //Cantidad
                o.NumeroDeDecimales = o1.NumeroDeDecimales
                o.UnidadOculta = o1.UnidadOculta
            }

            if ((o.tipoGS == 119) || (o.tipoGS == 118) || (o.tipoGS == 104)) {
                if (o.tipoGS == 118) { //Denominacion
                    o.codigoArticulo = o1.codigoArticulo;
                    //sIDDen = arraynuevafila[i][0];
                    o.articuloGenerico = o1.articuloGenerico;
                    if (o.Dependent)  //Cod Articulo Nuevo
                        o.Dependent.value = o1.Dependent.value;
                    if (o1.dependentfield) {
                        o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_")
                    }
                }
                else
                    if ((o.tipoGS == 119) || (o.tipoGS == 104)) {
                        if (o.Dependent)  //Cod Articulo Nuevo
                            o.Dependent.value = o1.Dependent.value;
                        //sIDArt = arraynuevafila[i][0]
                        if (o.tipoGS == 104) {
                            if (o1.dependentfield) {
                                o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_")
                            }
                        }
                    }
                if (o.idEntryPREC) {
                    o.idEntryPREC = o.idEntryPREC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                }
                if (o.idEntryPROV) {
                    o.idEntryPROV = o.idEntryPROV.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                }
                if (o.idEntryCANT) {
                    o.idEntryCANT = o.idEntryCANT.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                }
                if (o.idEntryUNI) {
                    o.idEntryUNI = o.idEntryUNI.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                }
                if (o.idEntryCC) {
                    o.idEntryCC = o.idEntryCC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                }
                if (o.idDataEntryDependent)
                    o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");

                if (o.idDataEntryDependent2)
                    o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");

                if (o.OrgComprasDependent)
                    o.OrgComprasDependent.value = o1.OrgComprasDependent.value;

                if (o.CentroDependent)
                    o.CentroDependent.value = o1.CentroDependent.value

                if (o.UnidadDependent)
                    o.UnidadDependent.value = o1.UnidadDependent.value
            }
            else
                if ((o.tipoGS == 9) || (o.tipoGS == 100)) {// Precio Unitario = 9 Y Proveedores = 100
                    o.cargarUltADJ = o1.cargarUltADJ;
                    //						if (o.cargarUltADJ == true) 
                    //							if (o.tipoGS == 9)
                    //								sIdPrecio = arraynuevafila[i][0]
                    //							else
                    //								sIdProveedor = arraynuevafila[i][0]
                    if (o.tipoGS == 100) {
                        if (o.dependentfield)
                            o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        if (o.idDataEntryDependent2)
                            o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");

                    }
                } else
                    if ((o.tipoGS == 6) || (o.tipoGS == 7)) {
                        if (o.idDataEntryDependent)
                            o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                    }
                    else
                        if (o.tipoGS == 103) {
                            if (o.idDataEntryDependent2)
                                o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        }
            if (o.tipoGS == 123) {
                if (o.dependentfield)
                    o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                if (o.idDataEntryDependent2)//org-centro
                    o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
            }
            if (o.tipoGS == 107) {//pais-provincia
                if (o.dependentfield)
                    o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
            }
            if (o.tipoGS == 124) {//centro-almacen
                if (o.idDataEntryDependent3)
                    o.idDataEntryDependent3 = o.idDataEntryDependent3.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
            }
            if ((o.tipoGS == 123) || (o.tipoGS == 124)) {
                if (o.idDataEntryDependent)
                    o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                o.setDataValue(o1.getDataValue())
            }
            else
                if ((o.tipoGS == 10) || (o.tipoGS == 12)) {
                    //o.setValue(0)	
                    //o.setDataValue(0);
                    o.setValue('')
                    o.setDataValue('');
                }
                else
                    if ((o.tipoGS == 11)) {
                        o.setValue('')
                        o.setDataValue('');
                    }
                    else
                        if ((o.tipoGS == 43)) {
                            o.setValue(arrEstadosInternos[1][1])
                            o.setDataValue(1);
                        }
                        else
                            if ((o.tipoGS == 1020)) {
                                o.setValue('')
                                o.setDataValue('');
                            }
                            else
                                if (o.tipoGS == 45) {
                                    o.setValue(o1.getValue())
                                    o.setDataValue(o1.getDataValue())
                                    o.MonRepercutido = o1.MonRepercutido
                                    o.CambioRepercutido = o1.CambioRepercutido
                                    if (o.readOnly == false) {
                                        var lnk = document.getElementById(o.dependentfield)
                                        lnk.innerText = o1.MonRepercutido;
                                        lnk = null;
                                    }
                                }
                                else
                                    if (o.tipoGS == 3000) {
                                        o.setValue(o.nLinea);
                                    }
                                    else
                                        if (o1.tipo == 8) {
                                            //Si es un adjunto se tiene que generar un nuevo id para el adjunto/s copiados
                                            var arrayAdjuntos = new Array();
                                            arrayAdjuntos[0] = "";
                                            arrayAdjuntos[1] = "";
                                            arrayAdjuntos = o1.getDataValue()
                                            CreateXmlHttp();
                                            if (xmlHttp && (arrayAdjuntos[0] != "" || arrayAdjuntos[1] != "")) {
                                                var params = "accion=8&AdjuntosAct=" + arrayAdjuntos[0] + "&AdjuntosNew=" + arrayAdjuntos[1]

                                                xmlHttp.open("POST", "../_common/GestionaAjax.aspx", false);
                                                xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                                                xmlHttp.send(params);

                                                var retorno;
                                                if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                                                    retorno = xmlHttp.responseText;
                                                    arrayAdjuntos[0] = "";
                                                    arrayAdjuntos[1] = retorno;
                                                    o.setDataValue(arrayAdjuntos)
                                                }
                                            } else
                                                o.setDataValue(arrayAdjuntos)
                                        } else
                                            o.setDataValue(o1.getDataValue())
        } else // else ---> if ((o != null)&&(o1 != null))
            if ((o != null) && (o1 == null) && (o.readOnly == true)) {
                o.setDataValue(o.getDataValue()); //si no, se guarda en blanco
            }

        if ((o.tipoGS == 8 || o.tipo == 8) && (lLineaGrid == 1)) { //Fichero ADJUNTO				
            var vaciar = new Array();
            vaciar[0] = "";
            vaciar[1] = "";
            o.setValue('');
            o.setDataValue(vaciar);
            vaciar = null;
        }
        else {
            if ((o.tipoGS == 119) || (o.tipoGS == 104) || (o.tipoGS == 118)) {
                o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_")
                if ((o.tipoGS == 119) || (o.tipoGS == 104)) {  //Cod Articulo Nuevo o CodArt Antiguo
                    //sIDArt = arraynuevafila[i][0]
                    if (o.tipoGS == 104)
                        o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_")

                }
                //					else
                //						if (o.tipoGS ==118)
                //							sIDDen = arraynuevafila[i][0]

                if (o.idDataEntryDependent) {
                    if (o.idDataEntryDependent.search("fsentry_") < 0) { //La organizacion de compras de encuentra fuera del desglose
                        oEntry = fsGeneralEntry_getById(o.idDataEntryDependent)
                        if ((oEntry) && (oEntry.tipoGS == 123))
                            oEntry.idDataEntryDependent = arraynuevafila[i][0];
                    }
                    o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                }
                if (o.idDataEntryDependent2)
                    o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                if (o.idEntryPREC) {
                    o.idEntryPREC = o.idEntryPREC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                }
                if (o.idEntryPROV) {
                    o.idEntryPROV = o.idEntryPROV.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                }
                if (o.idEntryCANT)
                    o.idEntryCANT = o.idEntryCANT.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");

                if (o.idEntryUNI)
                    o.idEntryUNI = o.idEntryUNI.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");

                if (o.idEntryCC) {
                    o.idEntryCC = o.idEntryCC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                }

            }
            else
                if ((o.tipoGS == 6) || (o.tipoGS == 7)) {
                    if (o.idDataEntryDependent)
                        o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                }
                else
                    if ((o.tipoGS == 9) || (o.tipoGS == 100)) {  // Precio Unitario = 9 Y Proveedores = 100
                        //								if (o.cargarUltADJ == true) 
                        //									if (o.tipoGS == 9)
                        //										sIdPrecio = arraynuevafila[i][0]
                        //									else
                        //										sIdProveedor = arraynuevafila[i][0]	
                        if (o.tipoGS == 100) {
                            if (o.dependentfield)
                                o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            if (o.idDataEntryDependent2)
                                o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        }
                    } else
                        if (o.tipoGS == 103) {
                            if (o.idDataEntryDependent2)
                                o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        }
            if (o.tipoGS == 123) {
                if (o.dependentfield)
                    o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                if (o.idDataEntryDependent2)//org-centro
                    o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
            }

            if (o.tipoGS == 107) {//pais-provincia
                if (o.dependentfield)
                    o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
            }
            if (o.tipoGS == 124) {//centro-almacen
                if (o.idDataEntryDependent3)
                    o.idDataEntryDependent3 = o.idDataEntryDependent3.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
            }

            if ((o.tipoGS == 123) || (o.tipoGS == 124)) {
                if (o.idDataEntryDependent)
                    o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");

                if (o.tipoGS == 124) {
                    var oEntryArt = fsGeneralEntry_getById(o.idDataEntryDependent)
                    if ((oEntryArt) && (oEntryArt.idDataEntryDependent)) {
                        if (oEntryArt.idDataEntryDependent.search("fsentry_") < 0) { //La organizacion de compras de encuentra fuera del desglose
                            oEntry = fsGeneralEntry_getById(oEntryArt.idDataEntryDependent)
                            if ((oEntry) && (oEntry.tipoGS == 123))
                                oEntry.idDataEntryDependent2 = arraynuevafila[i][0];
                            oEntry = null;
                        }
                    }
                    oEntryArt = null;
                }

            }


            if (lLineaGrid == 1) {
                o.setValue('');
                o.setDataValue('');
            }
        }
        if (o.masterField && o.masterField != '')
            o.masterField = o.masterField.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
        if (o.dependentfield && o.dependentfield != '') {
            //Se han usado tanto 'fsentry_' como con 'fsentry'. Ante la duda, controlamos ambos casos
            o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
            o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
        }
        o = null;
        o1 = null;
    }
    lIndex = null;
    lLineaGrid = null;
    Salta = null;
    arraynuevafila = null;
    insertarnuevoelemento = null;
    _arDependenceRelations = null;
    iCeldaIni = null;
}
//jpa Tarea 187 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//jpa viernes
function show_help(idcampo, instancia, solicitud) {
    var newWindow = window.open("../_common/ayudacampo.aspx?Campo=" + idcampo + "&Instancia=" + (instancia ? instancia : "") + "&Solicitud=" + (solicitud ? solicitud.toString() : ""), "_blank", "width=900,height=270,status=yes,resizable=no,top=200,left=200")
    newWindow.focus()
}
/*
''' <summary>
''' Abre la ventana para lo campos de tipo texto largo
''' </summary>
''' <param name="oTxt">control texto</param>
''' <param name="sValor">Texto</param>        
''' <param name="sIdi">Idioma</param>        
''' <param name="oEvent">Evento</param>             
''' <remarks>Llamada desde:dataentry de tipo texto largo; Tiempo m�ximo:0</remarks>*/
function show_popup(oTxt, sValor, sTextoAceptar, sTextoCancelar, sTextoCerrar, oEvent, windowTitle) {
    if ((ggWinPopUp == null) || (ggWinPopUp.closed)) {
        ggWinPopUp = window.open("../blank.htm", "winPopUp", "width=775,height=650,status=no,resizable=1,top=30,left=200");
        ggWinPopUp.focus()
        ggWinPopUp.opener = self;

        Build(oTxt, sValor, sTextoAceptar, sTextoCancelar, sTextoCerrar, windowTitle)
    }
}
/*
''' <summary>
''' Abre la ventana para lo campos de tipo texto medio
''' </summary>
''' <param name="oTxt"></param>
''' <param name="sValor">Texto</param>              
''' <param name="oEvent">Evento</param>            
''' <remarks>Llamada desde:dataentry de tipo texto medio
''' ; Tiempo m�ximo:0</remarks>
*/
function show_popup_texto_medio(oTxt, sValor, sTextoAceptar, sTextoCancelar, sTextoCerrar, oEvent, windowTitle) {
    //Descripci�n:	Abre la ventana de texto medio para un dataentry
    //Param. Entrada: 
    //	otxt		nombre del dataentry
    //	svalor		valor que teien asociado
    //	oEvent	    Evento producido
    //Param. Salida: -
    //LLamada:	javascript introducido para responder al onclick de todos los campos de texto medio
    //Tiempo:	0	
    if ((ggWinPopUp == null) || (ggWinPopUp.closed)) {
        CerrarDesplegables();
        ggWinPopUp = window.open("../blank.htm", "winPopUp", "width=350,height=215,status=no,resizable=yes,top=200,left=200");
        ggWinPopUp.focus()
        ggWinPopUp.opener = self;

        Build_texto_medio(oTxt, sValor, sTextoAceptar, sTextoCancelar, sTextoCerrar, windowTitle)
    }
}
function validar_fechas_acciones() {
    var fechasCorrectas = false;//true;
    //prompt ("codigo fuente", document.body.innerHTML)
    for (arrInput in arrInputs) {
        oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
        if (oEntry) {
            var oEntryLimite = null;
            var FechaLimite = null;
            var FechaInput = null;

            if (oEntry.idCampoPadre) //Este es un input perteneciente a un desglose
            {
                re = /fsdsentry/
                if (oEntry.id.search(re) >= 0) //y no es el input base del desglose
                {
                    if (oEntry.tipoGS == 36 || oEntry.tipoGS == 37) {
                        FechaInput = oEntry.getValue();
                        //document.getElementById("txtprueba").value = document.getElementById("txtprueba").value + oEntry.validationcontrol + "\r\n";
                        /*oEntryLimite = fsGeneralEntry_getById(oEntry.validationcontrol);
						if(oEntryLimite)
						{
							FechaLimite = oEntryLimite.getValue();
						}
						if(FechaInput != null && FechaLimite != null)
						{
							fechasCorrectas = fechasCorrectas && (FechaInput <= FechaLimite);
							//document.getElementById("txtprueba").value = document.getElementById("txtprueba").value + FechaInput + " <= " + FechaLimite + " = " + (FechaInput <= FechaLimite) + "\r\n";
						}*/
                    }
                }
            }
            else {
                if (oEntry.tipo == 9) //desglose
                {
                    for (k = 1 ; k <= document.getElementById(oEntry.id + "__numTotRows").value; k++) {
                        if (document.getElementById(oEntry.id + "__" + k.toString() + "__Deleted")) {
                            for (arrCampoHijo in oEntry.arrCamposHijos) {

                                var oHijoSrc = document.getElementById(oEntry.id + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                if (oHijoSrc) {
                                    if (oHijoSrc.tipoGS == 36 || oHijoSrc.tipoGS == 37) {
                                        var oHijo = document.getElementById(oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                        if (oHijo) {
                                            var sFecha = oHijo.value;
                                            if (sFecha) {
                                                var partesFecha = sFecha.split("-");
                                                FechaInput = new Date(partesFecha[0], (partesFecha[1] - 1), partesFecha[2], partesFecha[3], partesFecha[4], partesFecha[5]);
                                                document.getElementById("txtprueba").value = document.getElementById("txtprueba").value + FechaInput + "\r\n";
                                                /*oEntryLimite = fsGeneralEntry_getById("NoConfFECLIM_" + oEntry.idCampo);
												if(oEntryLimite)
												{
													FechaLimite = oEntryLimite.getValue();
												}
												if(FechaInput != null && FechaLimite != null)
												{
													fechasCorrectas = fechasCorrectas && (FechaInput <= FechaLimite);
													//document.getElementById("txtprueba").value = document.getElementById("txtprueba").value + "(" + sFecha + ")" + FechaInput + " <= " + FechaLimite + " = " + (FechaInput <= FechaLimite) + "\r\n";
												}*/
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return fechasCorrectas;
}
function FSDropDowns_MouseOver(gridName, id, objectType) {
    var grid = igtbl_getGridById(gridName);
    var cell = igtbl_getCellById(id);

    if (cell == null)
        return;
    igtbl_clearSelectionAll(gridName);
    igtbl_selectRow(gridName, cell.Row.Element.id);
}
function AgruparEstilos() {
    return
    s = document.body.innerHTML
    oStyle = document.styleSheets[document.styleSheets.length - 1]
    sStyle = oStyle.cssText
    iComienzo = s.search(sStyle)
    if (iComienzo == -1) {
        setTimeout("AgruparEstilos()", 100)
        return
    }
    iComienzo = s.search(sStyle)

    iFin = iComienzo + sStyle.length
    s = s.substr(iFin + 8, s.length)

    se = /<STYLE type=text\/css>/
    if (s.search(se) > 0) {
        while ((s.search(se) > 0)) {
            se = /<STYLE type=text\/css>/
            iComienzo = s.search(se)
            se = /<\/STYLE>/
            iFin = s.search(se)
            var sStyle = s.substr(iComienzo + 21, iFin - (iComienzo + 21))
            s = s.substr(0, iComienzo) + s.substr(iFin + 8)
            rllave = /{/
            rllaveCierre = /}/
            rpunto = /./
            sAux = sStyle
            while (sAux.search(rllave) > -1) {
                sSelector = sAux.substr(0, sAux.search(rllave))
                sSelector = replaceAll(sSelector, '\<\!--', '')

                sStyleNew = sAux.substr(sAux.search(rllave) + 1, sAux.search(rllaveCierre) - sAux.search(rllave) - 1)
                document.styleSheets[1].addRule(sSelector, sStyleNew)
                sAux = sAux.substr(sAux.search(rllaveCierre) + 2)
            }


        }
    }

}
//function ddNuevoArtsPreInitDropDown(param)
//function ddDenPreInitDropDown(param)
function ddArtsPreInitDropDown(param) {
    //fsdsentry_1_1792###uwtGrupos__ctl0__ctl0###uwtGrupos__ctl0__ctl0_fsdsentry_1_1793
    var oGrid = new Object()
    var arrParam = param.split("###")
    oGrid.ID = arrParam[1]
    oGrid.IdEntryMaterial = arrParam[0]
    oGrid.Editor = arrParam[2]
    oGridsDesglose[oGridsDesglose.length] = oGrid
}
var moEdit;
var motext;
var moEvent;
/* <summary>
''' Obtiene el entry del material a nivel de formulario del que depende el control actual. 
''' </summary>
'''<parameters name="oEdit">Entry actual</parameters>
''' <returns></returns>
*/
function getMaterialFormulario() {
    //si est� en el desglose
    if (window.opener) {
        if (window.opener.sDataEntryMaterialFORM)
            return window.opener.fsGeneralEntry_getById(window.opener.sDataEntryMaterialFORM);
    } else {
        if (sDataEntryMaterialFORM)
            return fsGeneralEntry_getById(sDataEntryMaterialFORM);
    }
};
function show_MensajeMaterialObligatorio() {
    p = window;
    if (window.opener) p = window.opener;
    alert(p.sMensajeMaterialObligatorio);
};
function ddArtsopenDropDownEvent(oEdit, text, oEvent) {
    oEdit = oEdit[0];
    moEdit = oEdit;
    motext = text;
    moEvent = oEvent;
    var nivelSeleccion = 0;

    var oEntryMaterial;
    oEntryMaterial = getMaterialFormulario();

        //uwtGruposxctl0xctl0xfsdsentryx1x1793xdd
    for (i = 0; i < oGridsDesglose.length; i++) {
        if (oGridsDesglose[i].Editor + "__t" == oEdit.id || oGridsDesglose[i].Editor + "__tden" == oEdit.id) vGrid = oGridsDesglose[i];
    }
    if (vGrid.Editor + "__tden" == oEdit.id)
        oEdit.id = vGrid.Editor + "__t";

    var sClientID;
    if (vGrid.Editor + "__tden" == oEdit.id) {
        sClientID = vGrid.Editor + "__t";
    } else sClientID = oEdit.id;
    
    sGridName = vGrid.ID;       
    sGridName += '_tblDesglose';

    sAux = oEdit.id
    if (sAux.search("fsdsentry") >= 0) {
        sInit = sAux.substr(0, sAux.search("fsdsentry"));
        sAux = sAux.substr(sAux.search("fsdsentry"), sAux.length);
        arrAux = sAux.split("_");
        lIndex = arrAux[1];
    } else {
        sInit = sAux.substr(0, sAux.search("fsentry"));
        lIndex = "-1";
    }

    var oGrid = document.getElementById(sGridName);
    o = fsGeneralEntry_getById(sInit + vGrid.IdEntryMaterial);
    var sMat = "";
    var sRead = "";
    var sMatId = "";

    //Nivel de seleccion de material 
    if (oEntryMaterial && oEntryMaterial.NivelSeleccion !== 0) 
        nivelSeleccion = oEntryMaterial.NivelSeleccion;
    
    
    if (oEntryMaterial && oEntryMaterial.NivelSeleccion != 0) {
        //si existe un campo mat fuera desglose con nivel de seleccion obligatorio se carga este material
        familiaMateriales = oEntryMaterial.dataValue;
        //si no se ha seleccionado ningun material
        if (!familiaMateriales || familiaMateriales == "") {
            show_MensajeMaterialObligatorio();
            return false;
        }
    }

    if (o) {
        sMat = o.getDataValue();
        sRead = o.readOnly ? "1" : "0";
        sMatId = o.id;
    } else sMat = oEdit.fsEntry.Dependent.value;

    if (sMat == "")
        if (oEdit.fsEntry.Restrict)
            sMat = oEdit.fsEntry.Restrict;
    var bCargarUltAdj = false;
    if (oEdit.fsEntry.idEntryPREC || oEdit.fsEntry.idEntryPROV)
        bCargarUltAdj = true;

    sCodOrgCompras = ""; sReadOrgCompras = "";
    sCodCentro = ""; sReadCentro = "";

    if (oEdit.fsEntry.idDataEntryDependent) {
        oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent);
        if (oOrgCompras) {
            sCodOrgCompras = oOrgCompras.getDataValue();
            if (sCodOrgCompras == "")
                sCodOrgCompras = "-1";
            sReadOrgCompras = (oOrgCompras.readOnly ? "1" : "0");
        }
    } else
        if (oEdit.fsEntry.OrgComprasDependent && oEdit.fsEntry.OrgComprasDependent.value) {
            sCodOrgCompras = oEdit.fsEntry.OrgComprasDependent.value;
            if (sCodOrgCompras == "")
                sCodOrgCompras = "-1";
            sReadOrgCompras = "1";
        } else {
            p = window.opener;
            if (p)
                if (p.sDataEntryOrgComprasFORM) { // Si antes que un desglose de pop-up hay un dataEntry ORGANIZACION COMPRAS
                    oOrgCompras = p.fsGeneralEntry_getById(p.sDataEntryOrgComprasFORM);
                    if (oOrgCompras) {
                        sCodOrgCompras = oOrgCompras.getDataValue();
                        if (sCodOrgCompras == "")
                            sCodOrgCompras = "-1";
                        sReadOrgCompras = (oOrgCompras.readOnly ? "1" : "0");
                    }
                }
        }

    if (oEdit.fsEntry.idDataEntryDependent2) {
        oCentro = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent2);
        if (oCentro) {
            sCodCentro = oCentro.getDataValue();
            if (sCodCentro == "")
                sCodCentro = "-1";
            sReadCentro = (oCentro.readOnly ? "1" : "0");
        }
    } else
        if (oEdit.fsEntry.CentroDependent && oEdit.fsEntry.CentroDependent.value) {
            sCodCentro = oEdit.fsEntry.CentroDependent.value;
            if (sCodCentro == "")
                sCodCentro = "-1";
            sReadCentro = "1";
        } else {
            p = window.opener;
            if (p) {
                if (p.sDataEntryCentroFORM) { // Si antes que un desglose de pop-up hay un dataEntry CENTRO
                    oCentro = p.fsGeneralEntry_getById(p.sDataEntryCentroFORM);
                    if (oCentro) {
                        sCodCentro = oCentro.getDataValue();
                        if (sCodCentro == "")
                            sCodCentro = "-1";
                        sReadCentro = oCentro.readOnly ? "1" : "0";
                    }
                } else
                    if (p.sCodCentroFORM) {
                        sCodCentro = p.sCodCentroFORM;
                        sReadCentro = "1";
                    }
            }
        }

    //altura ventana
    if (sCodOrgCompras == "" || sCodCentro == "")
        altura = 390;
    else
        altura = 430;

    CerrarDesplegables();
    var newWindow = window.open("../_common/articulosserver.aspx?mat=" + sMat + "&familiaMateriales=" + familiaMateriales + "&matro=" + sRead + "&Index=" + lIndex + "&TabContainer=" + sInit + "&ClientID=" + sClientID + "&CloneClientID=" + oEdit.id + "den&restric=" + (oEdit.fsEntry.Restric ? oEdit.fsEntry.Restric : "") + "&MatClientId=" + sMatId + "&InstanciaMoneda=" + oEdit.fsEntry.instanciaMoneda + "&CargarUltAdj=" + bCargarUltAdj + "&CodOrgCompras=" + sCodOrgCompras + "&OrgComprasRO=" + sReadOrgCompras + "&CodCentro=" + sCodCentro + "&CentroRO=" + sReadCentro + "&NivelSeleccion=" + nivelSeleccion, "_blank", "width=500,height=" + altura + ",status=yes,resizable=no,top=200,left=200")
    newWindow.focus();    
}

function DenArticulo_ValueChange(oEdit, text) {
    oEdit = oEdit[0];
    if (!oEdit.fsEntry.articuloGenerico) {
        idEntryCod = calcularIdDataEntryCelda(oEdit.fsEntry.id, oEdit.fsEntry.nColumna - 1, oEdit.fsEntry.nLinea, 0);
        if (idEntryCod) oFSEntryCod = fsGeneralEntry_getById(idEntryCod);
        if ((oFSEntryCod) && (oFSEntryCod.tipoGS == 119)) {
            if (!oFSEntryCod.getValue() == '') ddNuevoCodArticulo_ValueChange($('#' + idEntryCod + "__t"), oFSEntryCod.getValue());
        }
    }
};

function ddNuevoCodArticulo_ValueChange(oEdit, oldValue, oEvent) {
    oEdit = oEdit[0];
    VaciarCtrlsDependientes(oEdit);
    if (oEdit.fsEntry.getValue() == '')
        return;
    var oMat = null;
    if (document.getElementById("bMensajePorMostrar"))
        document.getElementById("bMensajePorMostrar").value = "1"
    var idEditorInput = oEdit.id + '_t';
    var oEditorInput = document.getElementById(idEditorInput);
    if (oEditorInput) {
        sValor = trim(oEditorInput.value);
        oEditorInput.title = sValor;
    }

    idEntryDen = calcularIdDataEntryCelda(oEdit.id, oEdit.fsEntry.nColumna + 1, oEdit.fsEntry.nLinea, 1)
    IDoMat = calcularIdDataEntryCelda(oEdit.id, oEdit.fsEntry.nColumna - 1, oEdit.fsEntry.nLinea, 1)


    if (IDoMat)
        oMat = fsGeneralEntry_getById(IDoMat);
    else
        IDoMat = "";
    if ((oMat) && (oMat.tipoGS == 103))
        sMat = oMat.getDataValue();
    else
        sMat = oEdit.fsEntry.Dependent.value;
    if (sMat == null) {
        sMat = ""
    }
    var idEntryPrec = null; var idEntryProv = null;

    //Obtener  el id del DataEntry del PRECIO si CARGAR_ULT_ADJ = 1
    if (oEdit.fsEntry.idEntryPREC != null) {
        idEntryPrec = oEdit.fsEntry.idEntryPREC;
        oPrecio = fsGeneralEntry_getById(idEntryPrec);
        if ((oPrecio) && (oPrecio.cargarUltADJ == true))
            idEntryPrec = oEdit.fsEntry.idEntryPREC;
        else
            idEntryPrec = "";
    }
    //Obtener  el id del DataEntry del PROVEEDOR si CARGAR_ULT_ADJ = 1
    if (oEdit.fsEntry.idEntryPROV != null) {
        idEntryProv = oEdit.fsEntry.idEntryPROV;
        oProveedor = fsGeneralEntry_getById(idEntryProv);
        if ((oProveedor) && (oProveedor.cargarUltADJ == true))
            idEntryProv = oEdit.fsEntry.idEntryPROV;
        else
            idEntryProv = "";
    }

    if (idEntryPrec == null)
        idEntryPrec = ""
    if (idEntryProv == null)
        idEntryProv = ""
    if (oEdit.fsEntry.idEntryUNI == null)
        idEntryUNI = ""
    else
        idEntryUNI = oEdit.fsEntry.idEntryUNI

    sCodOrgCompras = "";
    sCodCentro = "";
    var idEntryOrg = null;
    if (oEdit.fsEntry.idDataEntryDependent) {
        idEntryOrg = oEdit.fsEntry.idDataEntryDependent
        oOrgCompras = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent)
        if (oOrgCompras) {
            sCodOrgCompras = oOrgCompras.getDataValue()
        }
    } else
        if ((oEdit.fsEntry.OrgComprasDependent) && (oEdit.fsEntry.OrgComprasDependent.value)) {
            sCodOrgCompras = oEdit.fsEntry.OrgComprasDependent.value;
        } else {
            p = window.opener;
            if (p)
                if (p.sDataEntryOrgComprasFORM) { // Si antes que un desglose de pop-up hay un dataEntry ORGANIZACION COMPRAS
                    oOrgCompras = p.fsGeneralEntry_getById(p.sDataEntryOrgComprasFORM)
                    if (oOrgCompras) {
                        sCodOrgCompras = oOrgCompras.getDataValue()
                    }
                }
        }

    var idEntryCentro = null;
    if (oEdit.fsEntry.idDataEntryDependent2) {
        idEntryCentro = oEdit.fsEntry.idDataEntryDependent2
        oCentro = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent2)
        if (oCentro) {
            sCodCentro = oCentro.getDataValue()
        }
    } else
        if ((oEdit.fsEntry.CentroDependent) && (oEdit.fsEntry.CentroDependent.value)) {
            sCodCentro = oEdit.fsEntry.CentroDependent.value
        }
    var newWindow = window.open("../_common/BuscadorArticuloserver.aspx?idMaterial=" + sMat + "&idArt=" + sValor + "&fsEntryArt=" + oEdit.id + "&fsEntryDen=" + idEntryDen + "&fsEntryMat=" + IDoMat + "&fsEntryPrec=" + idEntryPrec + "&fsEntryProv=" + idEntryProv + "&InstanciaMoneda=" + oEdit.fsEntry.instanciaMoneda + "&fsEntryUni=" + idEntryUNI + "&fsEntryOrg=" + idEntryOrg + "&CodOrgCompras=" + sCodOrgCompras + "&fsEntryCentro=" + idEntryCentro + "&CodCentro=" + sCodCentro, "iframeWSServer")
    newWindow.focus();
}
/*
''' <summary>
''' Abre la ventana de monedas para un dataentry importe repercutido
''' </summary>
''' <param name="oEditIDMon">ClienId del dataentry</param>
''' <param name="oEditIDBt">ClienId del link</param>        
''' <remarks>Llamada desde:javascript introducido para responder al onclick de todos los campos de tipo importe repercutido
''' ; Tiempo m�ximo:0</remarks>
*/
function ddMonedaopenDropDownEvent(relativeElement, oEditIDMon, oEditIDBt) {
    CerrarDesplegables();

    var x = window.event.clientX - window.event.offsetX + document.body.scrollLeft - 1
    var y = window.event.clientY - window.event.offsetY + document.body.scrollTop + 16

    if (x + 200 > document.body.scrollWidth) {
        x = document.body.scrollWidth - 207
    }

    mostrarFRAME("ddMoneda", x, y, 200, 200)

    var oEditMon
    oEditMon = fsGeneralEntry_getById(oEditIDMon)

    ddMoneda.location = "../_common/LinkMonedas.aspx?FrameId=ddMoneda&EntryMoneda=" + oEditMon.id + "&EntryBoton=" + oEditIDBt
}
function ddCerrarMoneda() {
    // Cerrar desplegables de favoritos
    if (document.getElementById("ddMoneda")) {
        var oFrame = document.getElementById("ddMoneda")
        oFrame.parentNode.removeChild(oFrame);
    }
}
function ddCerrarpopupDesglose() {
    // Cerrar desplegables de favoritos
    if (document.getElementById("ddpopupDesglose")) {
        var oFrame = document.getElementById("ddpopupDesglose")
        oFrame.parentNode.removeChild(oFrame);
    }
}
/*
''' <summary>
''' Cerrar todos los combos abiertos
''' </summary>
''' <param name="evt">evento que provoco el cierre del combo</param>    
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Toda funci�n q despliega combos (ejemplo ddDestopenDropDownEvent), toda funci�n q
'''		despliega popup (ejemplo show_popup � show_popup_texto_medio) y PrepareControlHierarchy
'''	; Tiempo m�ximo:0</remarks>
*/
function CerrarDesplegables(evt) {
    try {
        ddCerrarpopupDesglose();
        ddCerrarMoneda();
        ddCloseDropDowns(evt);
        return true
    }
    catch (e) { }
}
function show_mat(oEntry) {
    o = oEntry.fsEntry;
    CerrarDesplegables();

    var sCodProve = "";
    nivelSeleccion = 0;
    
    //Restricciones de material
    var sRestric = o.Restric;
    if (!o.isDesglose && window.location.href.indexOf('articulosserver.aspx') === -1) {
        nivelSeleccion = o.NivelSeleccion;
    } else {
        //comprobar si hay un campo de material fuera del desglose
        oEntryMaterial = getMaterialFormulario();
        if (oEntryMaterial) {
            //si existe y tiene un nivel obligatorio, solo podemos seleccionar dicho nivel o descendientes
            if (oEntryMaterial.NivelSeleccion != 0) {
                sRestric = oEntryMaterial.dataValue;
                //si no tiene valor
                if (sRestric) {
                    nivelSeleccion = oEntryMaterial.NivelSeleccion;
                    o.SelCualquierMatPortal = true;
                } else {
                    show_MensajeMaterialObligatorio();
                    return false;
                }
            }
        }
    }

    if (oEntry.fsEntry.TipoSolicit == 5) { //Comprueba si se viene de Contrato
        if (document.getElementById("hid_Proveedor")) { //Comprueba si hay hidden de Proveedor (si ya hay proveedor seleccionado)
            sCodProve = document.getElementById("hid_Proveedor").value //Se obtiene el valor del proveedor
        }
    }

    var newWindow = window.open("../_common/materiales.aspx?Valor=" + o.getDataValue() + "&IdControl=" + o.id + "&Mat=" + sRestric + "&SelCualquierMatPortal=" + o.SelCualquierMatPortal + "&readonly=" + (o.readOnly ? "1" : "0") + "&NivelSeleccion=" + nivelSeleccion, "_blank", "width=550,height=550,status=yes,resizable=no,top=100,left=200")
    
    newWindow.focus();
}
function show_UnidadesOrganizativa(oEntry) {
    o = oEntry.fsEntry
    CerrarDesplegables();
    var newWidow = window.open("../_common/UnidadesOrganizativas.aspx?Valor=" + o.getDataValue() + "&IdControl=" + o.id, "_blank", "width=550,height=550,status=yes,resizable=no,top=100,left=200")
    newWidow.focus()

}
function show_pres(oTxt, sValor, oEvent) {
    o = oTxt.fsEntry
    var sValor = o.getDataValue()
    if (sValor == null)
        sValor = ""
    if (document.getElementById("Solicitud"))
        vSolicitud = document.getElementById("Solicitud").value
    else
        vSolicitud = ""

    if (document.getElementById("Instancia"))
        vInstancia = document.getElementById("Instancia").value
    else
        vInstancia = ""
    var dCantidad = ""
    var sTitulo = ""

    if (o.idCampoPadre) {

        var desglose = o.id.substr(0, o.id.search("_fsdsentry"))
        var index = o.id.substr(o.id.search("_fsdsentry_") + 11, o.id.search(o.idCampo) - o.id.search("_fsdsentry_") - 12)

        var idArticulo = null
        var idCantidad = null
        var idUnidad = null
        for (i = 0; i < document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells.length; i++) {
            s = document.getElementById(desglose + "_tblDesgloseHidden").rows[0].cells[i].innerHTML
            re = /fsentry/

            x = s.search(re)
            y = s.search("__tbl")

            id = s.substr(x + 7, y - x - 7)
            oEntry = fsGeneralEntry_getById(desglose + "_fsentry" + id)

            if (oEntry) {
                if (oEntry.tipoGS == 4)
                    idCantidad = oEntry.id
                if (oEntry.tipoGS == 104)
                    idArticulo = oEntry.id
                if (oEntry.tipoGS == 105)
                    idUnidad = oEntry.id
            }
        }

        if (idCantidad != null && idArticulo != null && idUnidad != null) {

            oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + fsGeneralEntry_getById(idArticulo).idCampo)
            sTitulo = oDSEntry.getValue()

            oDSEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + fsGeneralEntry_getById(idCantidad).idCampo)
            dCantidad = oDSEntry.getValue()
        }
    }
    CerrarDesplegables();
    var newWindow = window.open("../_common/presAsig.aspx?Valor=" + Var2Param(sValor) + "&IDPres=" + o.id + "&Campo=" + o.idCampo + "&Instancia=" + vInstancia.toString() + "&Solicitud=" + vSolicitud.toString() + "&Cantidad=" + dCantidad + "&Titulo=" + Var2Param(sTitulo), "_blank", "width=800,height= 520,status=yes,resizable=no,top=100,left=120")
    newWindow.focus();
}
/*  Revisado por: blp. Fecha: 10/10/2011
''' <summary>
''' M�todo que se lanza al seleccionar una organizaci�n de compras en una ventana emergente
''' Coloca el valor seleccionado en el campo de texto de Organizaci�n de compras
'''	y vac�a de contenido los campos dependientes caso de haberlos (centro y almac�n)
''' </summary>
''' <param name="sEntry">ID Entry del control a modificar</param> 
''' <param name="sCod">C�digo de la org de compras seleccionada</param> 
''' <param name="sDen">Denominaci�n de la org de compras seleccionada</param> 
''' <remarks>Llamada desde:proveedores.aspx; Tiempo m�ximo:0 </remarks>
*/
function cambiarOrganizacionCompras(sEntry, sCod, sDen) {
    if (sEntry != "null") {
        var oEntry = fsGeneralEntry_getById(sEntry)
        if (oEntry.getDataValue() != sCod) {
            oEntry.setDataValue(sCod)
            oEntry.setValue(sDen)
            ////El centro solo se cambia si hay cambio de organizacion de compras
            //centro
            if (oEntry.idDataEntryDependent2 != null) {
                //Vaciamos de contenido el control dependiente
                vaciarDropDown(oEntry.idDataEntryDependent2);
                var oCentro = fsGeneralEntry_getById(oEdit.idDataEntryDependent2)
                //Almacen
                if (oCentro.idDataEntryDependent3 != null) {
                    //Vaciamos de contenido el control dependiente
                    vaciarDropDown(oCentro.idDataEntryDependent3);
                }
            }
        }
    }

}
/*
''' <summary>
''' Mostrar una pantalla de selecci�n de proveedores. Se hara en funci�n de la organizaci�n de compras de haberla.
'''	Puede estar pero oculta, entonces se usa arrGSOcultos para sacarla.
''' </summary>
''' <param name="oEntry">Entry de tipo proveedor que provoco esta llamada</param> 
''' <returns>Nada. Llama a _common/proveedores.aspx y esta es la devuelve lo seleccionado</returns>
''' <remarks>Llamada desde:GeneralEntry.vb ; Tiempo m�ximo:0 </remarks>
*/
function show_proves(oEntry) {
    oEntry = oEntry.fsEntry;

    var oDepen;
    var visibilidad;
    var IdOrgCompras;

    if (oEntry.idDataEntryDependent) {
        oEntry.dependentfield = oEntry.idDataEntryDependent
    }

    if (oEntry.dependentfield) {
        oDepen = fsGeneralEntry_getById(oEntry.dependentfield)

        if (oDepen) {
            if (oDepen.readOnly == true) {
                visibilidad = "readOnly"
            }
            else {
                visibilidad = "visible"
            }
            valorOrgCompras = oDepen.getDataValue()

            sDepenId = oDepen.id
        }

    }
    else //El dependiente no existe (por no haber campo orgCompras) o es no visible.
        //Se comprobar� en la base 
    {
        valorOrgCompras = "null"
        visibilidad = "null"
        sDepenId = "null"

        if (oEntry.id.search("fsentry") > -1) {
            //No desglose
            re = /_fsentry/
            s = oEntry.id
            y = s.search(re)
            var x
            for (i = 0; i < y; i++) {
                if (s.charAt(i) == "_") {
                    x = i
                }
            }
            s2 = s.substring(x + 1, y)

            for (arrGSOculto in arrGSOcultos) {
                sBuscada = arrGSOcultos[arrGSOculto].substring(0, 3 + y - x)
                if (sBuscada == "123_" + s2)  //Es organizacion compras
                {
                    //sacar el valor

                    x = arrGSOcultos[arrGSOculto].search("__")
                    valor = arrGSOcultos[arrGSOculto].substr(x + 2)
                    sDepenId = "oculto"
                    valorOrgCompras = valor
                }
            }
        }
        else {
            //Desglose
            s2 = oEntry.idCampoPadre


            //Buscar el numero de Grupo (por si hay una organizacion de compras en el grupo fuera del desglose)
            re = s2 + "_fsdsentry"
            y1 = oEntry.id.search(re) - 1
            s3 = oEntry.id.substring(0, y1)
            x1 = s3.lastIndexOf("_")
            s4 = oEntry.id.substring(x1 + 1, y1)

            //Mirar si hay alguna organizaci�n de compra oculta en el grupo s4
            var bfueraDeDesglose = false
            if (s4 != "") {
                for (arrGSOculto in arrGSOcultos) {
                    sBuscada = arrGSOcultos[arrGSOculto].substring(0, s4.length + 4)
                    if (sBuscada == "123_" + s4)  //Es organizacion compras
                    {
                        x = arrGSOcultos[arrGSOculto].search("__")
                        valorOrgCompras = arrGSOcultos[arrGSOculto].substr(x + 2)
                        sDepenId = "oculto"
                        bfueraDeDesglose = true
                    }
                }
            }

            if (bfueraDeDesglose == false) {

                //Buscar Tambien el numero de linea
                re = /fsdsentry/
                x1 = oEntry.id.search(re) + 10
                y1 = oEntry.id.search(oEntry.idCampo) - 1
                iLinea = oEntry.id.substring(x1, y1)

                var sPrimerOculto = ""
                var bEncontrado = false
                var sPrimerOculto = ""
                for (arrGSOculto in arrGSOcultos) {
                    sBuscada = arrGSOcultos[arrGSOculto].substring(0, s2.length + 4)
                    if (sBuscada == "123_" + s2)  //Es organizacion compras
                    {
                        //Guardar el valor de la primera linea en caso de que sea necesario met�rselo
                        //a un campo de una linea a�adida
                        if ("1" == arrGSOcultos[arrGSOculto].substr(arrGSOcultos[arrGSOculto].search("___") + 3)) {
                            sPrimerOculto = arrGSOcultos[arrGSOculto]
                        }

                        if (iLinea == arrGSOcultos[arrGSOculto].substr(arrGSOcultos[arrGSOculto].search("___") + 3)) {
                            //sacar el valor
                            x = arrGSOcultos[arrGSOculto].search("__")
                            y = arrGSOcultos[arrGSOculto].search("___")
                            valor = arrGSOcultos[arrGSOculto].substring(x + 2, y)
                            sDepenId = "oculto"
                            valorOrgCompras = valor
                            bEncontrado = true
                        }

                    }
                }
                //Si bEncontrado = false se trata de alguna linea a�adida, luego se le da el valor de
                //la primera y se crea en el array de GSOcultos
                if (bEncontrado == false) {
                    if (sPrimerOculto != "") {
                        sNuevoOculto = sPrimerOculto.substring(0, sPrimerOculto.search("___") + 3) + iLinea
                        arrGSOcultos[arrGSOcultos.length] = sNuevoOculto

                        x = sNuevoOculto.search("__")
                        y = sNuevoOculto.search("___")
                        valor = sNuevoOculto.substring(x + 2, y)
                        sDepenId = "oculto"
                        valorOrgCompras = valor
                    }

                }
            } //Fin bFueraDeDesglose = False

        }

    }
    p = window.opener;
    if (p) {
        if (p.sDataEntryOrgComprasFORM) //Desglose no pop-up con organizaci�n de compras antes del desglose
        {
            //sDepenId = "oculto"
            //valorOrgCompras = valor	
            var oOrgCompras = p.fsGeneralEntry_getById(p.sDataEntryOrgComprasFORM)
            if (oOrgCompras) {
                valorOrgCompras = oOrgCompras.getDataValue()
                sDepenId = oOrgCompras.id
                if (oOrgCompras.readOnly == true) {
                    visibilidad = "readOnly"
                }
                else {
                    visibilidad = "visible"
                }
            }
        }
        else { //Comprobar si la organizaci�n de compras est� oculta
            if (p.sCodOrgComprasFORM) {
                sDepenId = "oculto"
                valorOrgCompras = p.sCodOrgComprasFORM
            }
        }
    }

    CerrarDesplegables();
    var newWindow = window.open("../_common/proveedores.aspx?Valor=" + oEntry.getDataValue() + "&IDControl=" + oEntry.id + "&IDDepen=" + sDepenId + "&Campo=" + oEntry.idCampo + "&ValorOrgCompras=" + valorOrgCompras + "&Visibilidad=" + visibilidad, "_blank", "width=750,height=475,status=yes,resizable=no,top=200,left=200")
    newWindow.focus();
}
function show_personas(oEntry) {
    oEntry = oEntry.fsEntry;

    CerrarDesplegables();
    var newWindow = window.open("../_common/usuarios.aspx?Valor=" + oEntry.getDataValue() + "&IDControl=" + oEntry.id + "&Campo=" + oEntry.idCampo, "_blank", "width=750,height=475,status=yes,resizable=no,top=200,left=200")
    newWindow.focus()
}
/*''' <summary>
''' muestra los centros de coste para que el usuario elija uno.
''' </summary>
''' <remarks>Llamada desde: GeneralEntry.vb/PrepareControlHierarchy; Tiempo m�ximo:0</remarks>*/
function show_centros_coste(oEntry) {
    oEntry = oEntry.fsEntry;

    CerrarDesplegables();
    var newWindow = window.open(rutaFS + "/pm/centrosCoste.aspx?Valor=" + escape(oEntry.getDataValue()) + "&IDControl=" + oEntry.id + "&Campo=" + oEntry.idCampo + "&VerUON=" + (oEntry.VerUON ? "1" : "0") + "&SessionId=" + sessionID, "_blank", "width=750,height=475,status=yes,resizable=no,top=200,left=200")
    newWindow.focus()
}
function show_atached_files(oEntry) {
    oEntry = oEntry.fsEntry

    if (document.getElementById("CertificadoEnviado"))
        vCertificadoEnviado = document.getElementById("CertificadoEnviado").value
    else
        vCertificadoEnviado = 0

    if (document.getElementById("Solicitud"))
        vSolicitud = document.getElementById("Solicitud").value
    else
        vSolicitud = ""

    if (document.getElementById("Instancia"))
        Instancia = document.getElementById("Instancia").value
    else
        Instancia = ""
    var inputFiles = document.getElementById(oEntry.id + "__hAct")

    sValor = inputFiles.value
    inputFiles = document.getElementById(oEntry.id + "__hNew")
    sValor2 = inputFiles.value
    if (oEntry.id.search("fsdsentry") > 0)
        tipo = 3
    else
        tipo = 1

    CerrarDesplegables();
    var newWindow = window.open("atachedfiles.aspx?Input=" + oEntry.id + "__hAct" + "&Valor=" + sValor + "&Valor2=" + sValor2 + "&Campo=" + oEntry.idCampo + "&tipo=" + tipo.toString() + "&Instancia=" + Instancia + "&Enviado=" + vCertificadoEnviado + "&Solicitud=" + vSolicitud.toString() + "&readOnly=" + (oEntry.readOnly ? "1" : "0"), "_blank", "width=750,height=330,status=yes,resizable=no,top=200,left=200")
    newWindow.focus()
}
var oWinDesglose
/*
''' <summary>
''' Abre la ventana de desglose. El nombre del entry pasa de estar en el id a estar en el name
''' </summary>
''' <param name="oEntry">el dataentry padre del desglose</param>     
''' <returns>Nada</returns>
''' <remarks>Llamada desde:Desde todos los entry que son desglose popup; Tiempo m�ximo:0</remarks>
*/
function show_desglose(oEntry) {

    oEntry = oEntry.fsEntry
    oDiv = oEntry.Editor

    oFrm = document.forms["frmDesglose"]

    sInner = oDiv.innerHTML
    oDiv.innerHTML = ""

    sInner = replaceAll(sInner, ":", "_")

    oFrm.innerHTML = sInner

    if (oFrm.elements["nomDesglose"])
        oFrm.elements["nomDesglose"].value = oEntry.name
    else
        oFrm.insertAdjacentHTML("beforeEnd", "<input id=nomDesglose type=hidden name =nomDesglose value=" + oEntry.name + ">")

    //En QA se actualiza tras cada grabaci�n el hidden Version. Desglose.ascx usa los entrys de cuando se entra por primera vez
    //bien en DetalleCertificado bien en DetalleNoConformidad por lo q hay q mantener un hidden VersionInicialCargada para q 
    //desglose.ascx funcione correctamente.
    if (document.getElementById("VersionInicialCargada"))
        vVersionInicial = document.getElementById("VersionInicialCargada").value
    else {
        if (document.getElementById("Version"))
            vVersionInicial = document.getElementById("Version").value
        else
            vVersionInicial = ""
    }

    if (document.getElementById("Version"))
        vVersionBdOCombo = document.getElementById("Version").value
    else
        vVersionBdOCombo = ""

    if (document.getElementById("Proveedor"))
        vProveedor = document.getElementById("Proveedor").value
    else
        vProveedor = ""

    if (document.getElementById("NoConformidad"))
        vNoConformidad = document.getElementById("NoConformidad").value
    else
        vNoConformidad = ""

    if (document.getElementById("FuerzaDesgloseLectura"))
        vFuerzaDesgloseLectura = document.getElementById("FuerzaDesgloseLectura").value
    else
        vFuerzaDesgloseLectura = ""

    if (document.getElementById("tipoVersion"))
        vtipoVersion = document.getElementById("tipoVersion").value
    else
        vtipoVersion = ""

    //COMPROBAR SI EL CAMPO ANTERIOR AL DESGLOSE ES DE TIPO ORGANIZACION DE COMPRAS
    //*****************************************************************************
    bCentroRelacionado = false;
    sCodOrgCompras = '';
    oFSEntryAnt = null;

    //idEntryAnt = calcularIdDataEntryAnt(oEntry.id,oEntry.orden - 1);		
    if (oEntry.orden > 1) {
        idEntryAnt = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 1, oEntry.nLinea, 1);
        if (idEntryAnt) {
            oFSEntryAnt = fsGeneralEntry_getById(idEntryAnt);

            if (oFSEntryAnt)
                if (oFSEntryAnt.tipoGS == 123) {
                    bCentroRelacionado = true;
                    sCodOrgCompras = oFSEntryAnt.getDataValue();
                }
        }
    }

    oInst = document.getElementById("Instancia")
    
    //Desglose popup cuando estas en caso version 0, debe salir por solicitud aunque tengas instancia
    var instanciaPort = 0
    if (vVersionInicial == "" && vtipoVersion == "1"){
        instanciaPort =oInst.value
        oInst = null;
    }
    if (document.getElementById("Solicitud"))
        vSolicitud = document.getElementById("Solicitud").value
    else
        vSolicitud = ""
    if (document.getElementById("BotonCalcular"))
        vCalcular = document.getElementById("BotonCalcular").value
    else
        vCalcular = 0
    if (oInst)
        if (oInst.value == "")
            oFrm.action = "desglose.aspx?Campo=" + oEntry.idCampo + "&Input=" + oEntry.id + "&SoloLectura=" + (oEntry.readOnly ? "1" : "0") + "&Solicitud=" + vSolicitud.toString() + "&BotonCalcular=" + vCalcular + "&CentroRelacionadoFORM=" + bCentroRelacionado + "&CodOrgComprasFORM=" + sCodOrgCompras
        else
            oFrm.action = "desglose.aspx?Campo=" + oEntry.idCampo + "&Input=" + oEntry.id + "&Instancia=" + oInst.value + "&Version=" + vVersionInicial + "&SoloLectura=" + (oEntry.readOnly ? "1" : "0") + "&NoConformidad=" + vNoConformidad + "&BotonCalcular=" + vCalcular + "&CentroRelacionadoFORM=" + bCentroRelacionado + "&CodOrgComprasFORM=" + sCodOrgCompras + "&VersionBd=" + vVersionBdOCombo
    else
        oFrm.action = "desglose.aspx?Campo=" + oEntry.idCampo + "&Input=" + oEntry.id + "&SoloLectura=" + (oEntry.readOnly ? "1" : "0") + "&Solicitud=" + vSolicitud.toString() + "&BotonCalcular=" + vCalcular + "&CentroRelacionadoFORM=" + bCentroRelacionado + "&CodOrgComprasFORM=" + sCodOrgCompras + "&instanciaPort=" + instanciaPort.toString()

    CerrarDesplegables();
    oWinDesglose = window.open("", "windesglose", "width=750,height=315,status=no,resizable=yes,top=200,left=200")
    oWinDesglose.focus()

    oFrm.target = "windesglose"
    oFrm.submit()

    sInner = oFrm.innerHTML
    oFrm.innerHTML = ""
    oDiv.innerHTML = sInner



}
/*
''' <summary>
''' Abre la ventana del editor.
''' </summary>
''' <param name="oEntry">el dataentry del campo tipo editor</param>     
''' <returns>Nada</returns>
''' <remarks>Llamada desde:Desde todos los entry que son tipo editor; Tiempo m�ximo:0</remarks>
*/
function show_editor(titulo, id, readOnly) {
    //window.open("../_common/editor.aspx?ID=" + oEntry.fsEntry.id + "&readOnly=" + (oEntry.fsEntry.readOnly ? "1" : "0"), "Editor", "width=910,height=600,status=yes,resizable=no,top=20,left=100")
    var newWindow = window.open("../_common/Editor.aspx?titulo=" + escape(titulo) + "&ID=" + id + "&readOnly=" + readOnly, "Editor", "width=910,height=600,status=yes,resizable=yes,top=20,left=100,scrollbars=yes")
    newWindow.focus()
}
/*
''' <summary>
''' Guarda el texto introducido en el editor y lo guarda en el dataentry
''' </summary>
''' <param name="IDControl">Id del dataentry tipo editor</param>     
''' <param name="texto">texto del editor</param>     
''' <returns>Nada</returns>
''' <remarks>Llamada desde:al pulsar el bot�n "guardar" del editor; Tiempo m�ximo:0</remarks>
*/
function save_editor_text(IDControl, texto) {
    o = fsGeneralEntry_getById(IDControl)
    o.setDataValue(texto)
}
function prove_seleccionado(IDControl, sProveCod, sProveDen) {

    o = fsGeneralEntry_getById(IDControl)
    o.setValue(sProveCod + ' - ' + sProveDen)
    o.setDataValue(sProveCod)
}
function prove_seleccionado3(IDControl, sProveCod, sProveDen, lConID, sConEmail) {

    //busca en oGridsDesglose la del participante
    for (i = 0; i < oGridsDesglose.length; i++) {
        if (!oGridsDesglose[i].IdEntryMaterial)
            continue
        sAux = oGridsDesglose[i].IdEntryMaterial
        re = /fsentry/
        sGridAux = sAux
        while (sGridAux.search(re) >= 0)
            sGridAux = sGridAux.replace(re, "")
        sGridAux = "_" + sGridAux + "_"

        if (IDControl.search(sAux) >= 0 || IDControl.search(sGridAux) > 0) {
            sAux = IDControl

            if (sAux.search("fsdsentry") >= 0) {
                sAux = sAux.substr(sAux.search("fsdsentry"), sAux.length)
                arrAux = sAux.split("_")
                lIndex = arrAux[1]
            }
            else {
                sAux = sAux.substr(sAux.search("fsentry"), sAux.length)
                lIndex = "-1"
            }


            vGrid = oGridsDesglose[i]
            sDropDown = vGrid.Editor
            oEntry = fsGeneralEntry_getById(vGrid.Editor)
            oEntry.setValue('')
            if (lIndex == "-1")
                sDropDown = sDropDown + "_dd"
            else
                sDropDown = sDropDown.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_") + "_dd"

            while (sDropDown.search("__") > 0)
                sDropDown = sDropDown.replace("__", "x")
            while (sDropDown.search("_") > 0)
                sDropDown = sDropDown.replace("_", "x")
            dropDownGrid = igtbl_getGridById(sDropDown)
            if (dropDownGrid) {
                dropDownGrid.sustituir = true;
            }
            break;
        }
    }

    o = fsGeneralEntry_getById(IDControl)
    o.setValue(sProveDen + ' (' + sConEmail + ')')
    o.setDataValue(sProveCod + '###' + lConID)

}
function usu_seleccionado(IDControl, sUsuCod, sUsuNom, sUsuEmail) {
    for (i = 0; i < oGridsDesglose.length; i++) {
        if (!oGridsDesglose[i].IdEntryMaterial)
            continue
        sAux = oGridsDesglose[i].IdEntryMaterial
        re = /fsentry/
        sGridAux = sAux
        while (sGridAux.search(re) >= 0)
            sGridAux = sGridAux.replace(re, "")
        sGridAux = "_" + sGridAux + "_"

        if (IDControl.search(sAux) >= 0 || IDControl.search(sGridAux) > 0) {
            sAux = IDControl

            if (sAux.search("fsdsentry") >= 0) {
                sAux = sAux.substr(sAux.search("fsdsentry"), sAux.length)
                arrAux = sAux.split("_")
                lIndex = arrAux[1]
            }
            else {
                sAux = sAux.substr(sAux.search("fsentry"), sAux.length)
                lIndex = "-1"
            }

            break;
        }
    }

    o = fsGeneralEntry_getById(IDControl)
    o.setValue(sUsuNom + ' (' + sUsuEmail + ')')
    o.setDataValue(sUsuCod)
}
function limpiarArt() {
}
/*''' <summary>
''' Rellenar un entry con el material seleccionado
''' </summary>
''' <param name="IDControl">Id del entry donde va el material seleccionado</param>
''' <param name="sGMN1">Gmn1 de material seleccionado</param>        
''' <param name="sGMN2">Gmn2 de material seleccionado</param>
''' <param name="sGMN3">Gmn3 de material seleccionado</param>  
''' <param name="sGMN4">Gmn4 de material seleccionado</param>
''' <param name="iNivel">Nivel de material seleccionado</param>  
''' <param name="sDen">Denominaci�n del material</param>
''' <remarks>Llamada desde: articulosserver.aspx/aceptar()  materiales.aspx/seleccionarMaterial()        
'''         validararticulo.aspx/articuloValidado(); Tiempo m�ximo:0</remarks>*/
/*Finalmente lo de cambiar el estado de rechazada a sin revisar tras un cambio en un campo no es en un campo 
cualquiera, es en los predefinidos de la acci�n, entre estos no esta el material/articulo/den. articulo*/
function mat_seleccionado(IDControl, sGMN1, sGMN2, sGMN3, sGMN4, iNivel, sDen, TodosNiveles) {
    var sCodArt = null;
    var idEntryCod = null;
    //Si el texto del nodo era, por ejemplo, C'a"ta >600mm. El oNode.getText() devuelve C'a"ta &gt;600mm. Lo q ha
    //de verse es > no &gt;
    sDen = replaceAll(sDen, "&gt;", ">");
    var sDenAux = sDen
    if (IDControl == "")
        return
    for (i = 0; i < oGridsDesglose.length; i++) {
        if (!oGridsDesglose[i].IdEntryMaterial)
            continue;
        sAux = oGridsDesglose[i].IdEntryMaterial
        re = /fsentry/;
        sGridAux = sAux;
        while (sGridAux.search(re) >= 0)
            sGridAux = sGridAux.replace(re, "");
        sGridAux = "_" + sGridAux + "_";
        if (IDControl.search(sAux) > 0 || IDControl.search(sGridAux) > 0) {
            sAux = IDControl;

            if (sAux.search("fsdsentry") >= 0) {
                sAux = sAux.substr(sAux.search("fsdsentry"), sAux.length)
                arrAux = sAux.split("_");
                lIndex = arrAux[1];
            }
            else {
                sAux = sAux.substr(sAux.search("fsentry"), sAux.length)
                lIndex = "-1";
            }

            vGrid = oGridsDesglose[i]
            sDropDown = vGrid.Editor
            oEntry = fsGeneralEntry_getById(vGrid.Editor);

            if (oEntry.tipoGS == 104 || oEntry.tipoGS == 119) {
                idEntryCod = vGrid.Editor
                idEntryDen = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna + 1, oEntry.nLinea, 1)
                sCodArt = oEntry.getDataValue();
                bAnyadir_art = oEntry.anyadir_art;
            }
        }
    }

    o = fsGeneralEntry_getById(IDControl);
    s = "";
    for (i = 0; i < ilGMN1 - sGMN1.length; i++)
        s += " ";

    sMat = s + sGMN1;

    if (TodosNiveles == 1) {
        sDenShort = sGMN1 + ' - ';
    } else {
        sDenShort = sGMN1 + ' - ' + sDen;
    }

    sDenAux += ' (' + sGMN1;
    if (sGMN2) {
        s = ""
        for (i = 0; i < ilGMN2 - sGMN2.length; i++)
            s += " "
        sMat = sMat + s + sGMN2
        sDenAux += ' - ' + sGMN2
        if (TodosNiveles == 1) {
            sDenShort += sGMN2 + ' - '
        } else {
            sDenShort = sGMN2 + ' - ' + sDen
        }
    }

    if (sGMN3) {
        s = ""
        for (i = 0; i < ilGMN3 - sGMN3.length; i++)
            s += " "
        sMat = sMat + s + sGMN3
        sDenAux += ' - ' + sGMN3
        if (TodosNiveles == 1) {
            sDenShort += sGMN3 + ' - '
        } else {
            sDenShort = sGMN3 + ' - ' + sDen
        }
    }
    if (sGMN4) {
        s = ""
        for (i = 0; i < ilGMN4 - sGMN4.length; i++)
            s += " "
        sMat = sMat + s + sGMN4
        sDenAux += ' - ' + sGMN4
        if (TodosNiveles == 1) {
            sDenShort += sGMN4 + ' - '
        } else {
            
            sDenShort = sGMN4 + ' - ' + sDen
        }       
    }
    sDenAux += ')'

    if (TodosNiveles == 1) {
        sDenShort += sDen
    }

    if (o.getDataValue() != sMat) {
        //si el campo material se encuentra en el desglose, confirmar� la eliminaci�n de los art�culos introducidos    
        if (!o.isDesglose && existeLineaDesgloseArticulos()) {
            var respuesta = confirm(sMensajeEliminArtMat);
            if (respuesta) {
                Borra_Los_Articulos();
            } else {
                return false;
            }
        }
        //Poner el NuevoCodArt en blanco
        var idEntryCod = calcularIdDataEntryCelda(o.id, o.nColumna + 1, o.nLinea, 0)
        if (idEntryCod) var oEntry = fsGeneralEntry_getById(idEntryCod)
        if ((oEntry != null) && (oEntry.tipoGS == 119)) {
            if (!oEntry.anyadir_art) {
                sCodArt = oEntry.getDataValue()
                oEntry.setValue("")
                oEntry.title = ""

                //Poner la Denominacion en Blanco
                oEntry = null;
                var idEntryDen = calcularIdDataEntryCelda(o.id, o.nColumna + 2, o.nLinea, 0)
                if (idEntryDen) oEntry = fsGeneralEntry_getById(idEntryDen)
                if ((oEntry != null) && (oEntry.tipoGS == 118)) {
                    if ((sCodArt != "") || (oEntry.getDataValue() == "")) {
                        oEntry.setValue("")
                        oEntry.title = ""
                    }
                }
            }
        }

        //Mirar si tiene alg�n campo de lista como hijo y ponerlo en blanco 
        VaciarCampoListaCampoMaterial(o);
    };


    o.setDataValue(sMat)
    o.setValue(sDenShort)
    o.setToolTip(sDenAux)
    limpiarArt()

    //Mirar si tiene alg�n campo de lista como hijo y ponerlo en blanco 
    VaciarCampoListaCampoMaterial(o);

    if ((sCodArt) && (idEntryCod))
        if (sCodArt != "") {
            var newWindow = window.open("../_common/BuscadorArticuloserver.aspx?idMaterial=" + sMat + "&idArt=" + sCodArt + "&fsEntryArt=" + idEntryCod + "&fsEntryDen=" + idEntryDen + "&desde=MATERIAL", "iframeWSServer")
            newWindow.focus()
        }
}

/*''' <summary>
''' En pedido negociado limpia los articulos por cambio de proveedor.
''' </summary>
''' <remarks>Llamada desde: BuscadorArticulos.aspx  Buscadorproveedores.aspx; Tiempo m�ximo: 0,2</remarks>*/
function Borra_Los_Articulos() {

    var NoRepetirPop = 0;
    borrar_Entrys(arrInputs);
    //Quien Borra puede ser un POPUP, en el padre hay q borrar los artic
    p = window.opener;
    if (p) {
        borrar_Entrys(p.arrInputs);
    }
};
function borrar_Entrys(arrInputs) {
    for (arrInput in arrInputs) {
        oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
        if (oEntry) {
            if (oEntry.idCampoPadre) //Este es un input perteneciente a un desglose
            {
                NoRepetirPop = oEntry.idCampoPadre;

                re = /fsdsentry/
                if (oEntry.id.search(re) >= 0) //y no es el input base del desglose
                {
                    if (oEntry.tipoGS == 119 || oEntry.tipoGS == 104 || oEntry.tipoGS == 118 || oEntry.tipoGS == 9 || oEntry.tipoGS == 105 || oEntry.tipoGS == 142 || oEntry.tipoGS == 103) { //Art nuevo//Art viejo////Precio Unitario//Unidad//Unidad de pedido//Material
                        oEntry.setValue("")
                        oEntry.setDataValue("")
                    }
                }
            } else {
                if (oEntry.tipo == 9 && oEntry.tipoGS != 136) { //desglose
                    for (k = 1; k <= document.getElementById(oEntry.id + "__numTotRows").value; k++) {
                        if (document.getElementById(oEntry.id + "__" + k.toString() + "__Deleted")) {
                            for (arrCampoHijo in oEntry.arrCamposHijos) {
                                var oHijoSrc = document.getElementById(oEntry.id + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                if (oHijoSrc) {
                                    var oHijo = document.getElementById(oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                    if (oHijo) {
                                        if (oHijoSrc.getAttribute("tipoGS") == 119 || oHijoSrc.getAttribute("tipoGS") == 104 || oHijoSrc.getAttribute("tipoGS") == 118 || oHijoSrc.getAttribute("tipoGS") == 9 || oHijoSrc.getAttribute("tipoGS") == 105 || oHijoSrc.getAttribute("tipoGS") == 142 || oHijoSrc.getAttribute("tipoGS") == 103) { //Art nuevo
                                            oHijo.value = "";
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else { //campo normal
                    if (oEntry.tipoGS == 119 || oEntry.tipoGS == 104 || oEntry.tipoGS == 118 || oEntry.tipoGS == 9 || oEntry.tipoGS == 105 || oEntry.tipoGS == 142 || oEntry.tipoGS == 103) { //Art nuevo//Art viejo////Precio Unitario//Unidad//Unidad de pedido//Material
                        oEntry.setValue("")
                        oEntry.setDataValue("")
                    }
                }
            }
        }
    }
}

function existeLineaDesgloseArticulos() {
    /*Comprueba si existe alguna linea del desglose con art�culos*/
    for (arrInput in arrInputs) {
        oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
        if (oEntry) {
            if (oEntry.isDesglose) //Input perteneciente al desglose
            {
                if (oEntry.tipoGS == 119 || oEntry.tipoGS == 104) {//Campos de articulos GS nuevo y viejo
                    if (oEntry.getValue() != '') return true;
                }
            }
            //desglose popup
            if (oEntry.arrCamposHijos) {
                for (arrCampoHijo in oEntry.arrCamposHijos) {
                    var oHijoSrc = document.getElementById(oEntry.id + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                    if (oHijoSrc) {
                        if (oHijoSrc.getAttribute("tipoGS") == 119 || oHijoSrc.getAttribute("tipoGS") == 104) {
                            for (k = 1; k <= document.getElementById(oEntry.id + "__numTotRows").value; k++) {
                                var oHijo = document.getElementById(oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo]);
                                if (oHijo.value != "") return true;
                            }
                        }
                    }
                }
            }
        }
    }
    return false;
};


function pres_seleccionado(IDControl, sValor, sTexto, CabeceraDesglose, DesgloseAsociado) {
    //DesgloseAsociado numero que me sirve para actualizar todos los dataentrys de presupuestos asociados
    //cuando es una cabecera la que abre la ventana de presupuestos
    var pertenece = -1

    if (CabeceraDesglose != 0) {
        //PRES1 = 110
        //Pres2 = 111
        //Pres3 = 112
        //Pres4 = 113			
        switch (CabeceraDesglose) {
            case '110':
                {
                    for (i = 0; i < arrPres1.length; i++) {
                        pertenece = -1
                        o = fsGeneralEntry_getById(arrPres1[i])
                        if (o != null) {
                            pertenece = arrPres1[i].indexOf(DesgloseAsociado)
                            if (pertenece != -1) {
                                o.setDataValue(sValor)
                                o.setValue(sTexto)
                            }
                        }
                    }
                    break;
                }

            case '111':
                {
                    for (i = 0; i < arrPres2.length; i++) {
                        pertenece = -1
                        o = fsGeneralEntry_getById(arrPres2[i])
                        if (o != null) {
                            pertenece = arrPres2[i].indexOf(DesgloseAsociado)
                            if (pertenece != -1) {
                                o.setDataValue(sValor)
                                o.setValue(sTexto)
                            }
                        }
                    }
                    break;
                }

            case '112':
                {
                    for (i = 0; i < arrPres3.length; i++) {
                        pertenece = -1
                        o = fsGeneralEntry_getById(arrPres3[i])
                        if (o != null) {
                            pertenece = arrPres3[i].indexOf(DesgloseAsociado)
                            if (pertenece != -1) {
                                o.setDataValue(sValor)
                                o.setValue(sTexto)
                            }
                        }
                    }

                    break;
                }

            case '113':
                {
                    for (i = 0; i < arrPres4.length; i++) {
                        pertenece = -1
                        o = fsGeneralEntry_getById(arrPres4[i])
                        if (o != null) {
                            pertenece = arrPres4[i].indexOf(DesgloseAsociado)
                            if (pertenece != -1) {
                                o.setDataValue(sValor)
                                o.setValue(sTexto)
                            }
                        }
                    }
                    break;
                }

        }
    }
    else {
        o = fsGeneralEntry_getById(IDControl)
        if (o != null) {
            o.setDataValue(sValor)
            o.setValue(sTexto)
        }
    }

}
// Revisado por: blp. Fecha: 10/10/2011
//''' <summary>
//''' Establece en pantalla la uon seleccionada
//''' </summary>
//''' <param name="IDControl">Control UOn donde establecer lo seleccionado</param>
//''' <param name="sUON1">UON nivel 1</param>
//''' <param name="sUON2">UON nivel 2</param>
//''' <param name="sUON3">UON nivel 3</param>
//''' <param name="iNivel">nivel</param>
//''' <param name="sDen">Denominacion de UON</param>
//''' <remarks>Llamada desde: script\_common\UnidadesOrganizativas.aspx; Tiempo m�ximo:0,2</remarks>
function UON_seleccionado(IDControl, sUON1, sUON2, sUON3, iNivel, sDen) {
    var sDenAux = '';

    if (sUON1) {
        sDenAux = sDenAux + sUON1;
        if (sUON2)
            sDenAux = sDenAux + ' - ' + sUON2;
        if (sUON3)
            sDenAux = sDenAux + ' - ' + sUON3;
        if (sDen)
            sDenAux = sDenAux + ' - ' + sDen;
    } else
        if (sDen)
            sDenAux = sDen;

    if (IDControl == "")
        return
    for (i = 0; i < oGridsDesglose.length; i++) {
        if (!oGridsDesglose[i].IdEntryMaterial)
            continue
        sAux = oGridsDesglose[i].IdEntryMaterial
        re = /fsentry/
        sGridAux = sAux
        while (sGridAux.search(re) >= 0)
            sGridAux = sGridAux.replace(re, "")
        sGridAux = "_" + sGridAux + "_"
        if (IDControl.search(sAux) > 0 || IDControl.search(sGridAux) > 0) {
            sAux = IDControl

            if (sAux.search("fsdsentry") >= 0) {
                sAux = sAux.substr(sAux.search("fsdsentry"), sAux.length)
                arrAux = sAux.split("_")
                lIndex = arrAux[1]
            }
            else {
                sAux = sAux.substr(sAux.search("fsentry"), sAux.length)
                lIndex = "-1"
            }


            vGrid = oGridsDesglose[i]
            sDropDown = vGrid.Editor
            oEntry = fsGeneralEntry_getById(vGrid.Editor)
            oEntry.setValue('')

            if (lIndex == "-1")
                sDropDown = sDropDown + "_dd"
            else
                sDropDown = sDropDown.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_") + "_dd"

            while (sDropDown.search("__") > 0)
                sDropDown = sDropDown.replace("__", "x")
            while (sDropDown.search("_") > 0)
                sDropDown = sDropDown.replace("_", "x")
            dropDownGrid = igtbl_getGridById(sDropDown)
            if (dropDownGrid) {
                dropDownGrid.sustituir = true;
            }
            break;
        }
    }

    var o = fsGeneralEntry_getById(IDControl)
    o.setDataValue(sDenAux)
    o.setValue(sDenAux)

    if (o.dependentfield) {
        //DEPARTAMENTO
        var oEntrySig = fsGeneralEntry_getById(o.dependentfield);
        if ((oEntrySig != null) && (oEntrySig.tipoGS == 122)) {
            //Vaciamos de contenido el control dependiente
            vaciarDropDown(o.dependentfield);
        }
        oEntrySig = null;
    }
    o = null;
}
//''' <summary>Borra del desglose la fila seleccionada</summary>
//''' <param name="e">Obj Celda</param>
//''' <param name="desglose">html del desglose</param>
//''' <param name="index">linea</param>
//''' <param name="idCampo">id del desglose</param>
//''' <remarks>Llamada desde: script\alta\desglose.ascx; Tiempo m�ximo:0,1</remarks>
function deleteRow(e, desglose, index, idCampo) {
    var filaOld
    var campo

    //Hay que comprobar que no est� asociada a un �tem o l�nea de pedido
    var result = ComprobarLineasAsociadas(idCampo, index);
    if (respuesta != "") {
        mostrarMensaje(respuesta);
        return;
    }
    for (var indice in htControlFilas) {
        filaOld = indice.substr(indice.lastIndexOf("_") + 1)
        campo = indice.substring(0, indice.indexOf("_"))
        if ((parseInt(filaOld) > index) && (campo == idCampo) && (htControlFilas[indice]) != 0) {
            htControlFilas[indice] = String(parseInt(htControlFilas[indice]) - 1)
        }

        if ((parseInt(filaOld) == index) && (campo == idCampo)) {
            htControlFilas[indice] = "0"
        }
    }

    for (arrInput in arrInputs) {
        if (arrInputs[arrInput].search(desglose + "_fsdsentry_" + index.toString() + "_") >= 0) {
            if (fsGeneralEntry_getById(arrInputs[arrInput]).tipoGS == 124) {
                if (fsGeneralEntry_getById(arrInputs[arrInput]).idDataEntryDependent2 != null) {
                    oOrgCompras = fsGeneralEntry_getById(fsGeneralEntry_getById(arrInputs[arrInput]).idDataEntryDependent2)
                    if (oOrgCompras.id.search("fsentry") > -1) // Org fuera del desglose
                    {
                        for (var indicefilas in htControlFilas) {
                            if (htControlFilas[indicefilas] != 0) {
                                if (oOrgCompras.idDataEntryDependent2.search("fsdsentry") >= 0) {
                                    sNew = oOrgCompras.idDataEntryDependent2.substr(0, oOrgCompras.idDataEntryDependent2.search("fsdsentry"))
                                    sNew2 = oOrgCompras.idDataEntryDependent2.substr(oOrgCompras.idDataEntryDependent2.search("fsdsentry"), oOrgCompras.idDataEntryDependent2.length)
                                    arrAux = sNew2.split("_")

                                    sCadenaFinal = sNew + arrAux[0] + "_" + htControlFilas[indicefilas] + "_" + arrAux[2]
                                    oOrgCompras.idDataEntryDependent2 = sCadenaFinal
                                    haylinea = 1
                                    break;
                                }

                            }
                        }

                        if (haylinea == 0) {
                            oOrgCompras.idDataEntryDependent2 = null
                        }
                    }
                }
            }

            arrInputs[arrInput] = "deleted"
        }

    }



    for (i = 0; i < arrCalculados.length; i++) {
        if (arrCalculados[i].search(desglose + "_fsdsentry_" + index.toString() + "_") >= 0) {
            arrCalculados[i] = "deleted"
        }
    }
    for (i = 0; i < arrObligatorios.length; i++) {
        if (arrObligatorios[i].search(desglose + "_fsdsentry_" + index.toString() + "_") >= 0) {
            arrObligatorios[i] = "deleted"
        }
    }

    var oTbl = e.parentElement.parentElement

    var oRow = e.parentElement

    oTbl.removeChild(oRow)

    if (document.getElementById(desglose + "_numRowsGrid")) {
        document.getElementById(desglose + "_numRowsGrid").value = parseFloat(document.getElementById(desglose + "_numRowsGrid").value) - 1;
    }

}
function ComprobarLineasAsociadas(campoDesglose, linea) {
    params = { idDesglose: campoDesglose, numLinea: linea }
    $.when($.ajax({
        type: "POST",
        url: '../../Consultas.asmx/Comprobar_Lineas',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        respuesta = msg.d;
    });
}
/* Revisado por: blp. Fecha: 10/10/2011
''' <summary>
''' Comprueba q los campos obligatorios esten rellenos y q los desgloses q deben tener al menos iuna linea la tengan
''' </summary>    
''' <returns>False si no esta correctamente relleno</returns>
''' <remarks>Llamada desde:certificados\certificado.aspx    noconformidad\noconformidad.aspx    solicitudes\detalleSolicitud.aspx   solicitudes\solicitudes.asp
; Tiempo m�ximo:0</remarks>*/
function comprobarObligatorios() {
    if (typeof (arrObligatorios) != "undefined") {
        for (var i = 0; i < arrObligatorios.length; i++) {
            if (arrObligatorios[i] != "deleted") {
                oEntry = fsGeneralEntry_getById(arrObligatorios[i])
                if (oEntry) {
                    if ((oEntry.tipoGS != 115) && (oEntry.tipoGS != 116)) { //si es campo participante proveedor o persona no se mira. Ya se mirar� en la funci�n ComprobarParticipantes.  
                        //si est� en rojo es que no est� validado y se borra
                        if (oEntry.Editor != null) {
                            if (oEntry.Editor.elem != null) {
                                if (oEntry.Editor.elem.className == "TipoTextoMedioRojo") {
                                    oEntry.setDataValue('');
                                    oEntry.setValue('');
                                    oEntry.Editor.elem.className = "TipoTextoMedio";
                                }
                            }
                        }

                        if (oEntry.basedesglose == false) {
                            if (oEntry.tipo == 9 || oEntry.tipo == 13 || oEntry.tipo == 14) {
                                if (oEntry.tipo == 13) {
                                    sDesglose = replaceAll(arrObligatorios[i], "OBLIG_", "")
                                    //uwtGrupos__ctl0x_922_922_9339_numRowsGrid
                                    numRows = document.getElementById(sDesglose + "_numRowsGrid").value
                                    if (numRows == 0)
                                        return 'filas0'
                                }
                                else {
                                    if (oEntry.tipoGS == 136) {
                                        if (sNumLineas == 0)
                                            return 'filas0'
                                    } else {
                                        if (oEntry.tipo == 14) {
                                            sDesglose = replaceAll(arrObligatorios[i], "_OBLDESG_", "")
                                            //uwtGrupos__ctl0x_922_922_9339_numRowsGrid
                                            numRows = document.getElementById(sDesglose + "_numRowsGrid").value
                                            if (numRows == 0)
                                                return 'filas0'

                                        }
                                        else {
                                            //uwtGrupos__ctl0x_922_fsentry9339__numRows
                                            numRows = document.getElementById(arrObligatorios[i] + "__numRows").value
                                            if (numRows == 0)
                                                return 'filas0'
                                        }
                                    }
                                }
                            }
                            else {
                                if (oEntry.tipoGS == 100 || oEntry.tipoGS == 110 || oEntry.tipoGS == 111 || oEntry.tipoGS == 112 || oEntry.tipoGS == 113 || oEntry.tipoGS == 132 || oEntry.tipoGS == 149)
                                    v = oEntry.getDataValue()
                                else
                                    if ((oEntry.tipoGS == 8) || (oEntry.tipo == 8)) {
                                        //v = oEntry.Editor.outerText
                                        var inputFiles;
                                        inputFiles = document.getElementById(oEntry.id + "__hNew")
                                        if (inputFiles.value != "")
                                            v = inputFiles.value
                                        else {
                                            inputFiles = document.getElementById(oEntry.id + "__hAct")
                                            v = inputFiles.value
                                        }
                                    } else {
                                        if ((oEntry.intro == 1) || (oEntry.tipo == 15))
                                            v = oEntry.getDataValue()
                                        else {
                                            if (oEntry.tipoGS == 0 || oEntry.tipoGS == 1 || oEntry.tipoGS == 2 || oEntry.tipoGS == 126 || oEntry.tipoGS == 133 || oEntry.tipoGS == 134
                                            || oEntry.tipoGS == 20 || oEntry.tipoGS == 23 || oEntry.tipoGS == 24 || oEntry.tipoGS == 25 || oEntry.tipoGS == 26 || oEntry.tipoGS == 30 || oEntry.tipoGS == 31
                                            || oEntry.tipoGS == 32 || oEntry.tipoGS == 35 || oEntry.tipoGS == 38 || oEntry.tipoGS == 39 || oEntry.tipoGS == 1020) {
                                                switch (oEntry.tipo) {
                                                    case 6: //tipo Texto medio y Texto Largo
                                                    case 7:
                                                        v = oEntry.getDataValue()
                                                        break;
                                                    default:
                                                        v = oEntry.Editor.elem.value
                                                        break;
                                                }
                                            } else
                                                v = oEntry.Editor.elem.value
                                        }
                                    }
                                if (v == null || v == "" || v == "null")
                                    if (oEntry.tipoGS == 104) {
                                        v = oEntry.EditorDen.elem.value
                                        if (v == null || v == "" || v == "null")
                                            if (oEntry.nLinea == 0)
                                                return oEntry.title.toString() + ' (' + oEntry.DenGrupo.toString() + ')'
                                            else
                                                return sFila + ' ' + oEntry.nLinea.toString() + ': ' + oEntry.title.toString() + ' (' + oEntry.DenDesgloseMat.toString() + ' - ' + oEntry.DenGrupo.toString() + ')'
                                    } else
                                        if (oEntry.nLinea == 0)
                                            return oEntry.title.toString() + '(' + oEntry.DenGrupo.toString() + ')'
                                        else
                                            return sFila + ' ' + oEntry.nLinea.toString() + ': ' + oEntry.title.toString() + ' (' + oEntry.DenDesgloseMat.toString() + ' - ' + oEntry.DenGrupo.toString() + ')'
                            }
                        }
                    }
                }
                else {
                    sDesglose = replaceAll(arrObligatorios[i], "OBLIG_", "")
                    if (document.getElementById(sDesglose + "_numRowsGrid")) {
                        //uwtGrupos__ctl0x_922_922_9339_numRowsGrid
                        numRows = document.getElementById(sDesglose + "_numRowsGrid").value
                        if (numRows == 0)
                            return 'filas0'
                    }
                    else {

                        sDesglose = replaceAll(arrObligatorios[i], "_OBLDESG_", "")
                        if (document.getElementById(sDesglose + "_numRowsGrid")) {
                            //uwtGrupos__ctl0x_922_922_9339_numRowsGrid
                            numRows = document.getElementById(sDesglose + "_numRowsGrid").value
                            if (numRows == 0)
                                return 'filas0'
                        }
                        else {
                            sDesglose = arrObligatorios[i]
                            sDesglose = sDesglose.split("__")
                            idCampo = sDesglose[sDesglose.length - 1]
                            sDesglose = replaceAll(arrObligatorios[i], "__" + idCampo, "")
                            var oEntryDesg = fsGeneralEntry_getById(sDesglose)
                            iNumRows = document.getElementById(sDesglose + "__numRows").value
                            iNumTotRows = document.getElementById(sDesglose + "__numTotRows").value
                            for (k = 1; k <= iNumTotRows; k++) {
                                if (document.getElementById(sDesglose + "__" + k.toString() + "__" + idCampo)) {
                                    v = document.getElementById(sDesglose + "__" + k.toString() + "__" + idCampo).value
                                    if (v == null || v == "" || v == "null") {
                                        if (document.getElementById(sDesglose + "__" + k.toString() + "__" + idCampo + "_hNew")) {
                                            v = document.getElementById(sDesglose + "__" + k.toString() + "__" + idCampo + "_hNew").value
                                            if (v == null || v == "" || v == "null") {
                                                return sFila + ' ' + k.toString() + ': ' + arrObligatoriosDen[i].toString() + ' (' + oEntryDesg.title.toString() + ' - ' + oEntryDesg.DenGrupo.toString() + ')'
                                            }
                                        }
                                        else
                                            return sFila + ' ' + k.toString() + ': ' + arrObligatoriosDen[i].toString() + ' (' + oEntryDesg.title.toString() + ' - ' + oEntryDesg.DenGrupo.toString() + ')'
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return ""
}
function copiarFila(sRoot, idCampo) {
    var s

    //uwtGrupos__ctl0__ctl0
    sClase = document.getElementById(sRoot + "_tblDesglose").rows[document.getElementById(sRoot + "_tblDesglose").rows.length - 1].cells[0].className
    if (sClase == "ugfilatablaDesglose")
        sClase = "ugfilaalttablaDesglose"
    else
        sClase = "ugfilatablaDesglose"

    oTR = document.getElementById(sRoot + "_tblDesglose").insertRow(-1)
    if (document.getElementById(sRoot + "_numRows")) {
        lIndex = parseFloat(document.getElementById(sRoot + "_numRows").value) + 1
        document.getElementById(sRoot + "_numRows").value = lIndex
    }
    else {
        lIndex = document.getElementById(sRoot + "_tblDesglose").rows.length - 1
        s = "<input type=hidden name=" + sRoot + "_numRows id=" + sRoot + "_numRows value=" + lIndex.toString() + ">"
        document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", s)
    }

    //Controlar las filas que se a�aden para optimizar MontarFormularioSubmit
    var ultimaFila = 0
    for (var indice in htControlFilas) {
        if (String(idCampo) == indice.substring(0, indice.indexOf("_"))) {
            if (parseInt(ultimaFila) < parseInt(htControlFilas[indice])) {
                ultimaFila = htControlFilas[indice]
            }
        }
        //alert(indice + htControlFilas[indice])
    }

    htControlFilas[String(idCampo) + "_" + String(lIndex)] = String(parseInt(ultimaFila) + 1)

    if (document.getElementById(sRoot + "_numRowsGrid")) {
        lLineaGrid = parseFloat(document.getElementById(sRoot + "_numRowsGrid").value) + 1
        document.getElementById(sRoot + "_numRowsGrid").value = lLineaGrid
    }
    else {
        lLineaGrid = document.getElementById(sRoot + "_tblDesglose").rows.length - 1
        s = "<input type=hidden name=" + sRoot + "_numRowsGrid id=" + sRoot + "_numRowsGrid value=" + lLineaGrid.toString() + ">"
        document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", s)
    }
    //A continuacion se obtiene lPrimeraLinea, que es la fila de donde se copian los datos
    if (lLineaGrid == 1) {
        lPrimeraLinea = lIndex
        js = "<input type=hidden name=" + sRoot + "_numPrimeraLinea id=" + sRoot + "_numPrimeraLinea value=" + lIndex.toString() + ">"
        document.forms[0].insertAdjacentHTML("beforeEnd", js)
    }
    else {

        if (document.getElementById(sRoot + "_numPrimeraLinea"))
            lPrimeraLinea = document.getElementById(sRoot + "_numPrimeraLinea").value
        else
            lPrimeraLinea = 1
    }



    //En este array se guardan en cada fila el nuevo data entry y del que se va acoger el valor (_1_)
    var arraynuevafila = new Array()
    var insertarnuevoelemento = 0
    for (var i = 0; i < document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length; i++) {
        arraynuevafila[i] = new Array()
        for (var j = 0; j < 2; j++)
            arraynuevafila[i][j] = ""
    }


    for (i = 0; i < document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length; i++) {
        s = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].innerHTML
        oTD = oTR.insertCell(-1)
        oTD.className = sClase
        re = /fsentry/
        while (s.search(re) >= 0) {
            s = s.replace(re, "fsdsentry_" + lIndex.toString() + "_")
        }

        se = /<STYLE type=text\/css>/
        if (s.search(se) > 0) {
            while ((s.search(se) > 0)) {
                se = /<STYLE type=text\/css>/
                iComienzo = s.search(se)
                se = /<\/STYLE>/
                iFin = s.search(se)
                var sStyle = s.substr(iComienzo + 21, iFin - (iComienzo + 21))
                s = s.substr(0, iComienzo) + s.substr(iFin + 8)

                rllave = /{/
                rllaveCierre = /}/
                rpunto = /./
                sAux = sStyle

                while (sAux.search(rllave) > -1) {
                    sSelector = sAux.substr(0, sAux.search(rllave))
                    sStyleNew = sAux.substr(sAux.search(rllave) + 1, sAux.search(rllaveCierre) - sAux.search(rllave) - 1)
                    document.styleSheets[document.styleSheets.length - 1].addRule(sSelector, sStyleNew)
                    sAux = sAux.substr(sAux.search(rllaveCierre) + 2)
                }
            }
        }

        var se = /<SCRIPT language=javascript>/
        var se2 = /<script language="javascript">/
        var arrJS = new Array()
        if ((s.search(se) > 0) || (s.search(se2) > 0)) {
            while (s.search(se) > 0) {
                iComienzo = s.search(se) + 28
                se = /<\/SCRIPT>/
                iFin = s.search(se)
                var sEval = s.substr(iComienzo, iFin - iComienzo)
                s = s.substr(0, iComienzo - 28) + s.substr(iFin + 9)
                arrJS[arrJS.length] = sEval
                se = /<SCRIPT language=javascript>/

            }

            while (s.search(se2) > 0) {
                var iComienzo = s.search(se2) + 30
                se = /<\/script>/
                var iFin = s.search(se)
                var sEval = s.substr(iComienzo, iFin - iComienzo)
                s = s.substr(0, iComienzo - 30) + s.substr(iFin + 9)
                arrJS[arrJS.length] = sEval
                sEval = null;
                se2 = /<script language="javascript">/
            }
            ni = /##newIndex##/g
            if (s.search(ni) > 0) {
                oTD.align = "center"
            }
            s = replaceAll(s, "##newIndex##", lIndex.toString())
            oTD.insertAdjacentHTML("beforeEnd", s)
            for (k = 0; k < arrJS.length; k++) {
                eval(arrJS[k])
            }
        }
        else {
            ni = /##newIndex##/g
            if (s.search(ni) > 0) {
                oTD.align = document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells[i].align
            }
            s = replaceAll(s, "'##newIndex##'", lIndex.toString())
            oTD.insertAdjacentHTML("beforeEnd", s)
        }

        //Aqui es donde se introducen en el array los controles	
        var stemp = s
        var stemp2
        var tempinicio
        if (stemp.search(' id="') > 0) { //En FireFox los ids vienen con comillas
            tempinicio = stemp.search(' id="')
            tempinicio = tempinicio + 5
        } else {
            tempinicio = stemp.search(" id=") //En IE los ids vienen sin comillas
            tempinicio = tempinicio + 4
        }
        stemp2 = stemp.substr(tempinicio, stemp.length - tempinicio)
        tempfin = stemp2.search("__tbl")
        mientry = stemp2.substr(0, tempfin)

        var a = new Array();
        var entry1;
        var numero
        a = mientry.split("_")
        var otmp;
        var o1tmp;
        if ((a.length == 7) || (a.length == 4) || (a.length == 9)) {
            entry1 = mientry
            if (a.length == 7) //cuando no es popup
                numero = a[5]
            else
                if (a.length == 4) //Cuando es popup

                    numero = a[2]
                else
                    if (a.length == 9) //Cuando el desglose no es una ventana emergente
                        numero = a[7]


            var cadenaareemplazar = "_" + numero + "_"
            //entry1 = entry1.replace(cadenaareemplazar, "_1_")
            entry1 = entry1.replace(cadenaareemplazar, "_" + lPrimeraLinea.toString() + "_")

            arraynuevafila[insertarnuevoelemento][0] = mientry
            arraynuevafila[insertarnuevoelemento][1] = entry1
            insertarnuevoelemento = insertarnuevoelemento + 1
        }

    }

    s = "<input type=hidden name=" + sRoot + "_" + lIndex + "_Deleted id=" + sRoot + "_" + lIndex + "_Deleted value='no'>"
    oTD.insertAdjacentHTML("beforeEnd", s)


    re = /fsentry/

    for (i = 0; i < document.getElementById(sRoot + "_tblDesgloseHidden").rows[0].cells.length; i++) {
        s2 = eval("arrEntrys_" + idCampo + "[i]")
        if (s2) {
            if (s2.search("arrCalculados") > 0) {
                s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
            }
            if (s2.search("arrObligatorios") > 0) {
                s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
            }

            if (s2.search("arrArticulos") > 0) {
                s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
                if (!(s2.search("arrArticulos\[iNumArts][0]=\"\"") > 0))
                    s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
            }

            if (s2.search("arrPres1") > 0) {
                s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
            }

            if (s2.search("arrPres2") > 0) {
                s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
            }

            if (s2.search("arrPres3") > 0) {
                s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
            }

            if (s2.search("arrPres4") > 0) {
                s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")
            }


            s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_")


            if (s2.search(", 104,") > 0 || s2.search(", 108,") > 0) {
                s2 = s2.replace(re, "fsdsentryx" + lIndex.toString() + "x") //en el caso del art�culo y de la provincia, la grid del dropdown es �nica para cada fila
                s2 = s2.replace(re, "fsdsentry_" + lIndex.toString() + "_") //tambien es �nico el campo dependiente (dependentfield)
            }
            eval(s2)
            if (s2.search("arrObligatorios") > 0) {
                oEntry = fsGeneralEntry_getById(arrObligatorios[arrObligatorios.length - 1])
                oEntry.basedesglose = false
            }
        }
    }

    /* //COMENTO ESTO, ESTO ES PARA COPIAR LOS VALORES DE LA ULTIMA FILA A LA FILA RECIEN CREADA
        lComienzoIdCampo = s2.search(sRoot + "_fsdsentry_" + lIndex.toString() + "_")
        lComienzoIdCampo = lComienzoIdCampo + (sRoot + "_fsdsentry_" + lIndex.toString() + "_").length
        s2 = s2.substr(lComienzoIdCampo,s2.length)
        lFinIdCampo = s2.search("\"")
        sIdCampo = s2.substr(0,lFinIdCampo)
        
        var lIndexBase = lIndex - 1
        
        while (lIndexBase > 0 && !fsGeneralEntry_getById(sRoot + "_fsdsentry_" + lIndexBase.toString() + "_" + sIdCampo))
            {
            lIndexBase --
            
            }
        oEntryUlt = fsGeneralEntry_getById(sRoot + "_fsdsentry_" + lIndexBase.toString() + "_" + sIdCampo)	
        if (oEntryUlt)
            {
            fsGeneralEntry_getById(sRoot + "_fsdsentry_" + lIndex.toString() + "_" + sIdCampo).setDataValue(oEntryUlt.getDataValue())
            fsGeneralEntry_getById(sRoot + "_fsdsentry_" + lIndex.toString() + "_" + sIdCampo).setValue(oEntryUlt.getValue())
            
            }
    */
    //Se hace la asignacion de valores para la nueva fila   
    var o;
    var o1;
    //var sIDArt = '';var sIDDen = '';
    //var sIdPrecio = null;var sIdProveedor = null;var sIdUnidad=null;

    for (i = 0; i < insertarnuevoelemento; i++) {
        o = fsGeneralEntry_getById(arraynuevafila[i][0])
        o1 = fsGeneralEntry_getById(arraynuevafila[i][1])
        if (o) {
            o.nColumna = i
            o.nLinea = lLineaGrid;
            if (o.errMsg) {
                o.errMsg = o.errMsg.replace('\n', '\\n');
                o.errMsg = o.errMsg.replace('\n', '\\n');
            }
        }
        if ((o != null) && (o1 != null) && (lLineaGrid != 1)) {
            if ((o.tipoGS == 8) || ((o.tipoGS == 0) && (o.tipo == 8)) || ((o.tipoGS == 40) && (o.tipo == 8)))
                o.setValue("")
            else
                o.setValue(o1.getValue())

            if ((o.tipoGS == 119) || (o.tipoGS == 118) || (o.tipoGS == 104)) {
                if (o.tipoGS == 118) { //Denominacion
                    o.codigoArticulo = o1.codigoArticulo;
                    //sIDDen = arraynuevafila[i][0];
                    o.articuloGenerico = o1.articuloGenerico;
                } else
                    if ((o.tipoGS == 119) || (o.tipoGS == 104)) {
                        if (o.Dependent)  //Cod Articulo Nuevo
                            o.Dependent.value = o1.Dependent.value;
                        sIDArt = arraynuevafila[i][0]
                        if (o.tipoGS == 104) {
                            if (o1.dependentfield) { //Cuando en un desglose de POP-UP hay un campo CodArtilo
                                o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_")
                            }
                        }
                    }
                if (o.idEntryPREC) {
                    o.idEntryPREC = o.idEntryPREC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                }
                if (o.idEntryPROV) {
                    o.idEntryPROV = o.idEntryPROV.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                }
                if (o.idEntryUNI)
                    o.idEntryUNI = o.idEntryUNI.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");

                if (o.idDataEntryDependent)
                    o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");

                if (o.idDataEntryDependent2)
                    o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");

                if (o.OrgComprasDependent)
                    o.OrgComprasDependent.value = o1.OrgComprasDependent.value;

                if (o.CentroDependent)
                    o.CentroDependent.value = o1.CentroDependent.value

            }
            else
                if ((o.tipoGS == 9) || (o.tipoGS == 100)) {// Precio Unitario = 9 Y Proveedores = 100
                    o.cargarUltADJ = o1.cargarUltADJ;
                    //						if (o.cargarUltADJ == true) 
                    //							if (o.tipoGS == 9)
                    //								sIdPrecio = arraynuevafila[i][0]
                    //							else
                    //								sIdProveedor = arraynuevafila[i][0]
                } else
                    //Si el entry es de tipo Fecha de Suministro introducirla en el array				
                    if ((o.tipoGS == 6) || (o.tipoGS == 7)) {
                        if (o.idDataEntryDependent)
                            o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");

                    } else
                        if ((o.tipoGS == 100) || (o.tipoGS == 123)) {
                            if (o.dependentfield)
                                o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                        }
            if ((o.tipoGS == 123) || (o.tipoGS == 124)) {
                if (o.idDataEntryDependent)
                    o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                o.setDataValue(o1.getDataValue())
            }

            else
                if ((o.tipoGS == 8) || ((o.tipoGS == 0) && (o.tipo == 8)) || ((o.tipoGS == 40) && (o.tipo == 8))) {
                    ret = new Array()
                    ret[0] = ""
                    ret[1] = ""

                    o.setDataValue(ret)
                }
                else
                    if (o.TablaExterna > 0) {
                        o.codigoArticulo = o1.codigoArticulo;
                        if (o.dependentfield != null)
                            o.dependentfield = o.dependentfield.replace(re, "fsdsentry_" + lIndex.toString() + "_");
                    }
                    else
                        o.setDataValue(o1.getDataValue())

        }// FIN --> if ((o != null)&&(o1 != null) && (lLineaGrid !=1))

        else // else ---> if ((o != null)&&(o1 != null))
            if (((o.tipoGS == 8) || ((o.tipoGS == 0) && (o.tipo == 8)) || ((o.tipoGS == 40) && (o.tipo == 8))) && (lLineaGrid == 1)) { //Fichero ADJUNTO
                //tipoGS=40	tipo=8	Documentaci�n de acciones para no conformidad
                var vaciar = new Array();
                vaciar[0] = "";
                vaciar[1] = "";
                o.setValue('');
                o.setDataValue(vaciar);
            } else {
                if ((o.tipoGS == 119) || (o.tipoGS == 104) || (o.tipoGS == 118)) {
                    if ((o.tipoGS == 119) || (o.tipoGS == 104)) {  //Cod Articulo Nuevo o CodArt Antiguo
                        sIDArt = arraynuevafila[i][0]
                        if (o.tipoGS == 104)
                            o.dependentfield = o.dependentfield.replace("fsentry", "fsdsentry_" + lIndex.toString() + "_");
                    }
                    //                    else
                    //						if (o.tipoGS ==118)
                    //							sIDDen = arraynuevafila[i][0]

                    if (o.idDataEntryDependent) {
                        if (o.idDataEntryDependent.search("fsentry_") < 0) { //La organizacion de compras de encuentra fuera del desglose
                            oEntry = fsGeneralEntry_getById(o.idDataEntryDependent)
                            if ((oEntry) && (oEntry.tipoGS == 123))
                                oEntry.idDataEntryDependent = arraynuevafila[i][0];
                        }
                        o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                    }
                    if (o.idDataEntryDependent2)
                        o.idDataEntryDependent2 = o.idDataEntryDependent2.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");

                    if (o.idEntryPREC) {
                        o.idEntryPREC = o.idEntryPREC.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                    }
                    if (o.idEntryPROV) {
                        o.idEntryPROV = o.idEntryPROV.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                    }
                    if (o.idEntryUNI)
                        o.idEntryUNI = o.idEntryUNI.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");


                }
                else
                    if ((o.tipoGS == 6) || (o.tipoGS == 7)) {
                        if (o.idDataEntryDependent)
                            o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                    }
                    else
                        if ((o.tipoGS == 9) || (o.tipoGS == 100)) {  // Precio Unitario = 9 Y Proveedores = 100
                            //						if (o.cargarUltADJ == true) 
                            //							if (o.tipoGS == 9)
                            //								sIdPrecio = arraynuevafila[i][0]
                            //							else
                            //								sIdProveedor = arraynuevafila[i][0]	
                        } else
                            //2	
                            if ((o.tipoGS == 100) || (o.tipoGS == 123)) {
                                if (o.dependentfield)
                                    o.dependentfield = o.dependentfield.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");
                            }
                if ((o.tipoGS == 123) || (o.tipoGS == 124)) {
                    if (o.idDataEntryDependent)
                        o.idDataEntryDependent = o.idDataEntryDependent.replace("fsentry_", "fsdsentry_" + lIndex.toString() + "_");

                    if (o.tipoGS == 124) {
                        oEntryArt = fsGeneralEntry_getById(o.idDataEntryDependent)
                        if ((oEntryArt) && (oEntryArt.idDataEntryDependent)) {
                            if (oEntryArt.idDataEntryDependent.search("fsentry_") < 0) { //La organizacion de compras de encuentra fuera del desglose
                                oEntry = fsGeneralEntry_getById(oEntryArt.idDataEntryDependent)
                                if ((oEntry) && (oEntry.tipoGS == 123))
                                    oEntry.idDataEntryDependent2 = arraynuevafila[i][0];
                            }
                        }
                    }

                }

                if (lLineaGrid == 1) {
                    o.setValue('');
                    o.setDataValue('');
                }
            }
    }// FIN --> for	   

}// FIN --> function copiarFila()
function TieneMasDecimalesQueEsteUsuario(num, dec, separador) {
    //separador 1-->","   2-->"."
    var nTmp = num

    var sNum = nTmp.toString()
    var rd = /\./
    var pDec
    if (separador == 1) pDec = sNum.search(",")
    else pDec = sNum.search(rd)

    if (pDec == -1) return (0)

    sDec = sNum.substr(pDec + 1, sNum.length)
    if (sDec.length > dec) {
        return (1)
    } else {
        return (0)
    };
};
function redondeacalc(num, dec, separador) {
    var nTmp = num
    var sNum = nTmp.toString()
    var rd = /\./
    var pDec
    if (separador == 1) pDec = sNum.search(",")
    else pDec = sNum.search(rd)

    if (pDec == -1) return (num)
    sDec = sNum.substr(pDec + 1, sNum.length)
    dec = (dec < sDec.length) ? dec : sDec.length

    //nTmp= parseFloat(nTmp)
    //nTmp = nTmp.replace(vthousanfmt, '')
    nTmp = nTmp.replace(",", '.')

    for (var i = 0; i < dec; i++) {
        nTmp = nTmp * 10
    }

    nTmp = Math.round(nTmp)


    for (var i = 0; i < dec; i++)
        nTmp = nTmp / 10

    return (nTmp)
};
function num2strcalc(num, vdecimalfmt, vthousanfmt, vprecisionfmt, separador) {
    var j
    if (num == 0) {
        return ("0")
    }
    if (num == null) {
        return ("")
    }

    var sNum
    var sDec
    var sInt
    var bNegativo
    var i

    bNegativo = false
    var vNum = redondeacalc(num, vprecisionfmt, separador)
    sNum = vNum.toString()
    if (sNum.charAt(0) == "-") {
        bNegativo = true
        sNum = sNum.substr(1, sNum.length)
    }
    rd = /\./
    pDec = sNum.search(rd)

    if (pDec == -1) {
        sInt = sNum
        sDec = ""
    } else {
        sInt = sNum.substr(0, pDec)
        sDec = sNum.substr(pDec + 1, 20)
        if (sDec.substr(sDec.length - 4, 4) == "999" + sDec.substr(sDec.length - 1, 1)) {
            var lDigito = 10 - parseFloat(sDec.substr(sDec.length - 1, 1))
            var sSum = "0."
            for (j = 1; j < sDec.length; j++)
                sSum += "0"
            sSum += lDigito.toString()


            sNum = (parseFloat(sInt + "." + sDec) + parseFloat(sSum)).toString()
            pDec = sNum.search(rd)

            if (pDec == -1) {
                sInt = sNum
                sDec = ""
            } else {
                sInt = sNum.substr(0, pDec)
                sDec = sNum.substr(pDec + 1, vprecisionfmt)
            }
        }
    }

    if (sInt.length > 3) {
        newValor = ""
        s = 0
        //los posicionamos correctamente
        for (i = sInt.length - 1; i >= 0; i--) {
            newValor = sInt.charAt(i) + newValor
            s++
        }
        sInt = newValor
    }

    var redondeo = sDec

    sDec = sDec.substr(0, vprecisionfmt >= 12 ? 10 : vprecisionfmt)

    while (sDec.charAt(sDec.length - 1) == "0")
        sDec = sDec.substr(0, sDec.length - 1)

    if (sDec != "" && sDec != "000000000000".substr(0, vprecisionfmt))
        //sNum= sInt + vdecimalfmt + sDec
        sNum = sInt + "." + sDec
    else sNum = sInt
    if (bNegativo == true) sNum = "-" + sNum

    var vprec = vprecisionfmt
    while (sNum == "0" && num != 0) {
        vprec++;
        sNum = num2strcalc(num, vdecimalfmt, vthousanfmt, vprec, separador)
    };
    return (sNum)
};
/*
''' <summary>
''' Crear la estructura javascript para realizar el calculo de calculados en RecalcularImportes.aspx
''' </summary>
''' <remarks>Llamada desde: certificado.aspx	noconformidad.aspx	detalleSolicitud.aspx	solicitudes.aspx; Tiempo m�ximo: 0</remarks>*/
function MontarFormularioCalculados() {
    var f = document.forms["frmAlta"]

    if (!f)
        f = document.forms["frmDetalle"]

    oFrm = document.getElementById("frmCalculados")
    oFrm.innerHTML = ""

    if (f.elements["Solicitud"]) {
        oFrm.insertAdjacentHTML("beforeEnd", "<INPUT id=CALC_Solicitud type=hidden NAME=CALC_Solicitud>")
        oFrm.elements["CALC_Solicitud"].value = f.elements["Solicitud"].value
    }
    if (f.elements["Instancia"]) {
        oFrm.insertAdjacentHTML("beforeEnd", "<INPUT id=CALC_Instancia type=hidden NAME=CALC_Instancia>")
        oFrm.elements["CALC_Instancia"].value = f.elements["Instancia"].value
    }
    if (f.elements["Version"]) {
        oFrm.insertAdjacentHTML("beforeEnd", "<INPUT id=CALC_NumVersion type=hidden NAME=CALC_NumVersion>")
        oFrm.elements["CALC_NumVersion"].value = f.elements["Version"].value
    }
    if (f.elements["SoloLectura"]) {
        oFrm.insertAdjacentHTML("beforeEnd", "<INPUT id=CALC_SoloLectura type=hidden NAME=CALC_SoloLectura>")
        oFrm.elements["CALC_SoloLectura"].value = f.elements["SoloLectura"].value
    }
    oFrm.insertAdjacentHTML("beforeEnd", "<input type=hidden name=CALC_txtNumDesgloses>")
    oFrm.elements["CALC_txtNumDesgloses"].value = arrDesgloses.length
    for (i = 0; f.elements["txtPre_" + i.toString()]; i++) {
        sGrupo = f.elements["txtPre_" + i.toString()].value
        sGrupo = sGrupo.split("_")
        sGrupo = sGrupo[sGrupo.length - 1]
        sInput = "<INPUT id=CALC_txtPre_" + sGrupo + " type=hidden name=CALC_txtPre_" + sGrupo + ">"
        oFrm.insertAdjacentHTML("beforeEnd", sInput)
        oFrm.elements["CALC_txtPre_" + sGrupo].value = f.elements["txtPre_" + i.toString()].value
    }

    if (typeof (arrDesgloses) != "undefined") {
        for (i = 0; i < arrDesgloses.length; i++) {
            sDesglose = replaceAll(arrDesgloses[i], "_tblDesglose", "")
            sNumRows = replaceAll(arrDesgloses[i], "_tblDesglose", "_numRows")
            sInput = "<input type=hidden name=CALC_nomDesglose_" + i.toString() + " value ='" + arrDesgloses[i] + "'>"
            oFrm.insertAdjacentHTML("beforeEnd", sInput)
            sInput = "<input type=hidden name=CALC_" + arrDesgloses[i] + ">"
            oFrm.insertAdjacentHTML("beforeEnd", sInput)
            oFrm.elements["CALC_" + arrDesgloses[i]].value = document.getElementById(sNumRows).value
        }
    }

    if (typeof (arrCalculados) != "undefined") {
        for (i = 0; i < arrCalculados.length; i++) {
            if (arrCalculados[i] != "deleted") {
                oEntry = fsGeneralEntry_getById(arrCalculados[i])
                if (oEntry) {
                    sInput = "<input type=hidden name=CALC_" + arrCalculados[i] + "  id=CALC_" + arrCalculados[i] + ">"
                    oFrm.insertAdjacentHTML("beforeEnd", sInput)
                    if (oEntry.readOnly) {
                        vValor = oEntry.dataValue
                        if (vValor == null) vValor = ""
                        else {
                            if (oEntry.dataValue.toString().toString().indexOf(',') != -1) vValor = oEntry.dataValue.replace(",", ".")
                            else vValor = oEntry.dataValue
                        }
                        document.forms["frmCalculados"].elements["CALC_" + arrCalculados[i]].value = vValor
                    } else {
                        vValor = oEntry.dataValue
                        if (vValor == null) {
                            vValor = oEntry.getValue()
                            if (vValor == null) vValor = ""
                        } else {
                            if (TieneMasDecimalesQueEsteUsuario(oEntry.dataValue, vprecisionfmt, 1)) {
                                formateado = num2strcalc(oEntry.dataValue, vdecimalfmt, vthousanfmt, vprecisionfmt, 1)
                                formateado2 = redondeacalc(oEntry.dataValue, vprecisionfmt, 1)
                                if ((formateado == oEntry.getValue()) || (formateado2 == oEntry.getValue())) {
                                    if (oEntry.dataValue.toString().toString().indexOf(',') != -1) vValor = oEntry.dataValue.replace(",", ".")
                                    else vValor = oEntry.dataValue
                                } else {
                                    if (oEntry.TablaExterna > 0) vValor = oEntry.dataValue.replace(",", ".")
                                    else
                                        if (oEntry.intro == 1) vValor = oEntry.dataValue.replace(",", ".")
                                        else vValor = oEntry.getValue()
                                }
                            } else {
                                if (oEntry.TablaExterna > 0) vValor = oEntry.dataValue.replace(",", ".")
                                else
                                    if (oEntry.intro == 1) vValor = oEntry.dataValue.replace(",", ".")
                                    else vValor = oEntry.getValue()
                            }
                        }

                        document.forms["frmCalculados"].elements["CALC_" + arrCalculados[i]].value = vValor
                    }
                }
                else {
                    sDesglose = arrCalculados[i]
                    sDesglose = sDesglose.split("__")
                    idCampo = sDesglose[sDesglose.length - 1]

                    sDesglose = replaceAll(arrCalculados[i], "__" + idCampo, "")
                    iNumRows = document.getElementById(sDesglose + "__numRows").value
                    iNumTotRows = document.getElementById(sDesglose + "__numTotRows").value
                    if (!document.forms["frmCalculados"].elements["CALC_" + sDesglose + "__numRows"]) {
                        sInput = "<input type=hidden name=CALC_" + sDesglose + "__numRows  id=CALC_" + sDesglose + "__numRows>"
                        oFrm.insertAdjacentHTML("beforeEnd", sInput)
                        document.forms["frmCalculados"].elements["CALC_" + sDesglose + "__numRows"].value = iNumRows
                    }
                    if (!document.forms["frmCalculados"].elements["CALC_" + sDesglose + "__numTotRows"]) {
                        sInput = "<input type=hidden name=CALC_" + sDesglose + "__numTotRows  id=CALC_" + sDesglose + "__numTotRows>"
                        oFrm.insertAdjacentHTML("beforeEnd", sInput)
                        document.forms["frmCalculados"].elements["CALC_" + sDesglose + "__numTotRows"].value = iNumTotRows
                    }

                    for (k = 1; k <= iNumTotRows; k++) {
                        if (document.getElementById(sDesglose + "__" + k.toString() + "__Deleted"))
                            if (document.getElementById(sDesglose + "__" + k.toString() + "__" + idCampo)) {
                                sInput = "<input type=hidden name=CALC_" + sDesglose + "__" + k.toString() + "__" + idCampo + "  id=CALC_" + sDesglose + "__" + k.toString() + "__" + idCampo + ">"
                                oFrm.insertAdjacentHTML("beforeEnd", sInput)
                                vValor = document.getElementById(sDesglose + "__" + k.toString() + "__" + idCampo).value
                                if (vValor == null) {
                                    vValor = ""
                                } else {
                                    vValor1 = document.getElementById(sDesglose + "__" + k.toString() + "__" + idCampo).defaultValue

                                    if (TieneMasDecimalesQueEsteUsuario(vValor1, vprecisionfmt, 2)) {
                                        formateado = num2strcalc(vValor1, vdecimalfmt, vthousanfmt, vprecisionfmt, 2)
                                        formateado2 = redondeacalc(vValor1, vprecisionfmt, 2)
                                        if ((formateado == vValor) || (formateado2 == vValor)) {
                                            vValor = vValor1
                                        }
                                    }
                                }
                                document.forms["frmCalculados"].elements["CALC_" + sDesglose + "__" + k.toString() + "__" + idCampo].value = vValor
                            }
                    }
                }
            }
        }
    }

    oFrm.insertAdjacentHTML("beforeEnd", "<input type=hidden name=CALC_MONEDA id=CALC_MONEDA>")
    var monedaInput = $.map(arrInputs, function (x) { if (fsGeneralEntry_getById(x) != null) { if (fsGeneralEntry_getById(x).tipoGS == 102 && !fsGeneralEntry_getById(x).basedesglose) return fsGeneralEntry_getById(x); } })[0];
    if ((monedaInput) && (monedaInput.dataValue != '') && (monedaInput.dataValue != null)) { document.forms["frmCalculados"].elements["CALC_MONEDA"].value = monedaInput.getDataValue(); }

    return oFrm
}
/*
''' <summary>
''' Crear un input tipo hidden con el nombre pasado en el param sInput y el valor pasado.
''' </summary>
''' <param name="sInput">nombre del hidden que se crea</param>
''' <param name="valor">valor del hidden que se crea</param>   
''' <returns>Nada</returns>
''' <remarks>Llamada desde: MontarFormularioSubmit; MontarFormularioCalculados, MontarFormularioAlta</remarks>*/
function anyadirInput(sInput, valor) {
    if ((valor == "null") || (valor == null))
        valor = ""

    var f = document.forms["frmSubmit"]
    if (!f.elements[sInput]) {
        f.insertAdjacentHTML("beforeEnd", "<INPUT type=hidden name=" + sInput + ">\n")
        f.elements[sInput].value = valor
    }
    else {
        if (sInput.search("LINDESG_") >= 0 && (f.elements[sInput].value != valor) && valor != "") {
            f.elements[sInput].value = valor
        }
    }
}
/*
''' <summary>
''' Crear los input necesarios para la grabaci�n en bbdd de solicitudes, no conformidades y certificados.
''' </summary>
''' <param name="guarda">Hay cambio de etapa o no. True no lo hay False lo hay</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:certificados\certificado.aspx    noconformidad\noconformidad.aspx    solicitudes\detalleSolicitud.aspx   solicitudes\solicitudes.aspx
; Tiempo m�ximo:1,0sg</remarks>
*/
function MontarFormularioSubmit(guarda, nuevoWorkflow, Mapper) {
    if (!document.forms["frmSubmit"]) {
        if (guarda == '' || guarda == undefined)
            document.body.insertAdjacentHTML("beforeEnd", "<form name=frmSubmit action=../_common/guardarinstancia.aspx target=fraPMPortalServer id=frmSubmit method=post style='visibility:hidden;position:absolute;top:0px;left:0px;width:1px;height:1px;'></form>")
        else //desde detallesolicitud.aspx	
            document.body.insertAdjacentHTML("beforeEnd", "<form name=frmSubmit action=../_common/guardarinstancia.aspx target=fraPMPortalServer id=frmSubmit method=post style='position:absolute;top:0px;left:0px;width:1px;height:1px;'></form>")
    } else document.forms["frmSubmit"].innerHTML = ""

    f = document.forms["frmDetalle"]
    if (!f)
        f = document.forms["frmAlta"]

    anyadirInput("GEN_MostrarAvisoMapper", true);
    //primero los ids generales de la instancia / version / certificado / no conformidad
    anyadirInput("GEN_AccionRol", "");
    anyadirInput("GEN_Rol", "");

    sInput = "GEN_Guardar";
    if (guarda == true || guarda == "true") anyadirInput(sInput, 1);
    else anyadirInput(sInput, 0);

    sInput = "GEN_Bloque";
    anyadirInput(sInput, "");
    //new
    sInput = "GEN_Solicitud";
    if (f.elements["Solicitud"])
        anyadirInput(sInput, f.elements["Solicitud"].value);
    sInput = "GEN_Contacto";
    if (f.elements["Contacto"])
        anyadirInput(sInput, f.elements["Contacto"].value);
    //new
    sInput = "GEN_Instancia";
    if (f.elements["Instancia"]) anyadirInput(sInput, f.elements["Instancia"].value);
    else if (f.elements["NEW_Instancia"]) anyadirInput(sInput, f.elements["NEW_Instancia"].value);
    sInput = "GEN_Certificado";
    if (f.elements["Certificado"])
        anyadirInput(sInput, f.elements["Certificado"].value);
    sInput = "GEN_Version"
    if (f.elements["Version"])
        anyadirInput(sInput, f.elements["Version"].value)
    sInput = "GEN_NoConformidad"
    if (f.elements["NoConformidad"])
        anyadirInput(sInput, f.elements["NoConformidad"].value)
    sInput = "GEN_Notificar"
    if (f.elements["Notificar"])
        anyadirInput(sInput, f.elements["Notificar"].value)

    sInput = "GEN_Enviar"
    anyadirInput(sInput, "0")
    sInput = "GEN_Accion"
    anyadirInput(sInput, "")

    //para los certificados y las no conformidades:
    sInput = "GEN_DespubDia"
    anyadirInput(sInput, "")
    sInput = "GEN_DespubMes"
    anyadirInput(sInput, "")
    sInput = "GEN_DespubAnyo"
    anyadirInput(sInput, "")

    //Para las no conformidades
    sInput = "GEN_NoConfProve"
    anyadirInput(sInput, "")
    sInput = "GEN_NoConfCon"
    anyadirInput(sInput, "")

    sInput = "GEN_NoConfContactosProveedor"
    anyadirInput(sInput, "")

    //Campos de contrato
    sInput = "ID_CONTRATO"
    anyadirInput(sInput, "")
    sInput = "COD_CONTRATO"
    anyadirInput(sInput, "")

    sInput = "PathContrato"
    anyadirInput(sInput, "")
    sInput = "NombreContrato"
    anyadirInput(sInput, "")
    sInput = "ProcesoContrato"
    anyadirInput(sInput, "")
    sInput = "ItemsContrato"
    anyadirInput(sInput, "")

    sInput = "GEN_EmpresaID"
    anyadirInput(sInput, "")
    sInput = "GEN_PROVE"
    anyadirInput(sInput, "")
    sInput = "GEN_ContactoID"
    anyadirInput(sInput, "")
    sInput = "GEN_CodMoneda"
    anyadirInput(sInput, "")
    sInput = "GEN_FechaIni" // dd/mm/aaaa --> hay que separarlo y convertirlo en date
    anyadirInput(sInput, "")
    sInput = "GEN_FechaFin"
    anyadirInput(sInput, "")
    sInput = "GEN_Alerta"
    anyadirInput(sInput, "")
    sInput = "GEN_RepetirEmail"
    anyadirInput(sInput, "")
    sInput = "GEN_AlertaImporteDesde"
    anyadirInput(sInput, "")
    sInput = "GEN_AlertaImporteHasta"
    anyadirInput(sInput, "")
    sInput = "GEN_Notificados"
    anyadirInput(sInput, "")

    //Mapper
    sInput = "PantallaMaper"
    anyadirInput(sInput, "")
    sInput = "DeDonde"
    anyadirInput(sInput, "")

    if (nuevoWorkflow == true) {
        if (guarda == false) return;
    }    

    //y luego todos los inputs
    for (arrInput in arrInputs) {
        oEntry = fsGeneralEntry_getById(arrInputs[arrInput])
        if (oEntry)
            if (oEntry.tipo != 16) {
                if (oEntry.idCampoPadre) //Este es un input perteneciente a un desglose
                {
                    re = /fsentry/
                    x = oEntry.id.search(re)
                    if (oEntry.id.search(re) >= 0) {
                        if (oEntry.tipoGS != 3000) {
                            anyadirInput("DESG_" + oEntry.idCampoPadre + "_" + oEntry.id.substr(x + 7) + "_0_1", "")
                            anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + oEntry.id.substr(x + 7) + "_0_1", oEntry.CampoDef_CampoODesgHijo)
                            anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + oEntry.id.substr(x + 7) + "_0_1", oEntry.CampoDef_DesgPadre)
                        }
                    }

                    re = /fsdsentry/
                    if (oEntry.tipoGS == 42) {
                        x = oEntry.id.search(re) + 10
                        //y=oEntry.id.search(oEntry.idCampo)+18					
                        y = x + 1
                    }
                    else {
                        x = oEntry.id.search(re) + 10
                        y = oEntry.id.search(oEntry.idCampo) - 1
                    }
                    if (oEntry.id.search(re) >= 0) //y no es el input base del desglose
                    {
                        iLinea = oEntry.id.substr(x, y - x)
                        iLineaOriginal = iLinea

                        //input de desglose formato: IdCampoPadre_IdCampoHijo_Linea
                        anyadirInput("DESGTIPOGS_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString(), oEntry.tipoGS);
                        if (Mapper == true) {
                            if (oEntry.calculado == true) { //identifica si es campo calculado 
                                sInput = "DESGCALC_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                anyadirInput(sInput, oEntry.calculado)
                                sInput = "DESGCALCTIT_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                anyadirInput(sInput, oEntry.title)
                            }

                            if ((oEntry.intro == 0 || oEntry.intro == 2) && oEntry.tipoGS == 0 && oEntry.IdAtrib != 0) { // atributos 
                                sInput = "DESGAT_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                anyadirInput(sInput, oEntry.IdAtrib)
                                sInput = "DESGERP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                anyadirInput(sInput, oEntry.ValErp)
                            }

                            if (oEntry.TablaExterna > 0) {
                                sInput = "DESGTABLAEXT_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                anyadirInput(sInput, oEntry.TablaExterna)
                            }
                        }
                        if (oEntry.intro == 1 && (oEntry.tipo == 1 || oEntry.tipo == 5 || oEntry.tipo == 6 || oEntry.tipo == 7) && (oEntry.tipoGS == 0 || oEntry.tipoGS == 46 || oEntry.tipoGS == 148 || oEntry.tipoGS == 150)) {
                            sInput = "DESG_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                            anyadirInput(sInput, oEntry.getValue())
                            sInput = "DESGID_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                            anyadirInput(sInput, oEntry.getDataValue())
                            sInput = "DESGCCDHNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                            anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                            sInput = "DESGCCDPNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                            anyadirInput(sInput, oEntry.CampoDef_DesgPadre)
                            if (Mapper == true) {
                                if (oEntry.IdAtrib != 0) {
                                    sInput = "DESGAT_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                    anyadirInput(sInput, oEntry.IdAtrib)
                                    sInput = "DESGERP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                    anyadirInput(sInput, oEntry.ValErp)
                                }
                            }
                        }
                        else {
                            sInput = "DESG_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()

                            if (oEntry.tipo == 8) {//archivo
                                anyadirInput(sInput, oEntry.getValue())
                                sInput = "DESGCCDHNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                sInput = "DESGCCDPNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                anyadirInput(sInput, oEntry.CampoDef_DesgPadre)
                            }
                            else
                                if (oEntry.tipoGS == 104)  //CodArticulo Soliciatudes anteriores
                                {
                                    var oMat
                                    if (oEntry.dependentfield != null) {
                                        oMat = fsGeneralEntry_getById(oEntry.dependentfield)
                                        if (!oMat) //el material est� oculto
                                        {
                                            sRe = "/fsdsentry_" + iLinea.toString() + "/"
                                            re = eval(sRe)
                                            x = oEntry.dependentfield.search(re) + sRe.length - 1
                                            y = oEntry.dependentfield.length - x
                                            idMat = oEntry.dependentfield.substr(x, y)
                                            sInputMat = "DESG_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6"
                                            anyadirInput(sInputMat, oEntry.Dependent.value)
                                        }
                                    }
                                    //Comprobar si la Unidad esta oculta
                                    if (oEntry.idEntryUNI != null) {
                                        oUni = fsGeneralEntry_getById(oEntry.idEntryUNI)
                                        if (!oUni) { //La Unidad est� oculta				
                                            sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                            re = eval(sRe)
                                            x = oEntry.idEntryUNI.search(re) + sRe.length - 1
                                            y = oEntry.idEntryUNI.length - x
                                            idUni = oEntry.idEntryUNI.substr(x, y)
                                            sInputUni = "DESG_" + oEntry.idCampoPadre + "_" + idUni + "_" + iLinea + "_6"
                                            anyadirInput(sInputUni, oEntry.UnidadDependent.value)
                                        }
                                    }

                                    if (oEntry.getDataValue() == null || oEntry.getDataValue() == "") {
                                        s = oEntry.getValue()
                                        s = s.replace(" - ", "")
                                        anyadirInput(sInput, s)
                                    }
                                    else
                                        anyadirInput(sInput, oEntry.getDataValue())

                                    sInput = "DESGCCDHNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                    anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                    sInput = "DESGCCDPNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                    anyadirInput(sInput, oEntry.CampoDef_DesgPadre)
                                }
                                else
                                    if (oEntry.tipoGS == 119)  //Nuevo Codigo de Articulo
                                    {
                                        var oMat
                                        if (oEntry.dependentfield != null) {
                                            oDen = null;
                                            idEntryDen = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna + 1, oEntry.nLinea, 1)
                                            if (idEntryDen)
                                                oDen = fsGeneralEntry_getById(idEntryDen)

                                            //si la denominaci�n no est� oculta, no hace nada con el material (ya se har� cuando se trate la denominaci�n)
                                            if ((!oDen) || ((oDen) && (oDen.tipoGS != 118))) //La denominacion esta oculta									
                                            {
                                                oMat = fsGeneralEntry_getById(oEntry.dependentfield)
                                                if (!oMat) //el material est� oculto
                                                {
                                                    sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                                    re = eval(sRe)
                                                    x = oEntry.IdMaterialOculta.search(re) + sRe.length - 1
                                                    y = oEntry.IdMaterialOculta.length - x
                                                    idMat = oEntry.IdMaterialOculta.substr(x, y)

                                                    sInputMat = "DESG_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6"
                                                    anyadirInput(sInputMat, oEntry.Dependent.value)
                                                    sInputMat = "DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6"
                                                    anyadirInput(sInputMat, -1000)
                                                    sInputMat = "DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6"
                                                    anyadirInput(sInputMat, oEntry.CampoDef_DesgPadre)
                                                }
                                            }


                                            if (idEntryDen)
                                                oDen = fsGeneralEntry_getById(idEntryDen)
                                            if (((!oDen) && (oEntry.tag != null)) || ((oDen) && (oDen.tipoGS != 118))) { //La denominacion esta oculta		
                                                sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                                re = eval(sRe)
                                                x = oEntry.IdDenArticuloOculta.search(re) + sRe.length - 1
                                                y = oEntry.IdDenArticuloOculta.length - x
                                                idDen = oEntry.IdDenArticuloOculta.substr(x, y)

                                                sInputDen = "DESG_" + oEntry.idCampoPadre + "_" + idDen + "_" + iLinea + "_6"
                                                anyadirInput(sInputDen, oEntry.tag)
                                                sInputDen = "DESGID_" + oEntry.idCampoPadre + "_" + idDen + "_" + iLinea + "_6"
                                                anyadirInput(sInputDen, oEntry.tag)
                                                sInputDen = "DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idDen + "_" + iLinea + "_6"
                                                anyadirInput(sInputDen, -1000)
                                                sInputDen = "DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idDen + "_" + iLinea + "_6"
                                                anyadirInput(sInputDen, oEntry.CampoDef_DesgPadre)
                                            }
                                        }
                                        //Comprobar si la Unidad esta oculta
                                        if (oEntry.idEntryUNI != null) {
                                            oUni = fsGeneralEntry_getById(oEntry.idEntryUNI)
                                            if (!oUni) { //La Unidad est� oculta				
                                                sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                                re = eval(sRe)
                                                x = oEntry.idEntryUNI.search(re) + sRe.length - 1
                                                y = oEntry.idEntryUNI.length - x
                                                idUni = oEntry.idEntryUNI.substr(x, y)
                                                sInputUni = "DESG_" + oEntry.idCampoPadre + "_" + idUni + "_" + iLinea + "_6"
                                                anyadirInput(sInputUni, oEntry.UnidadDependent.value)
                                                sInputUni = "DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idUni + "_" + iLinea + "_6"
                                                anyadirInput(sInputUni, -1000)
                                                sInputUni = "DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idUni + "_" + iLinea + "_6"
                                                anyadirInput(sInputUni, oEntry.CampoDef_DesgPadre)
                                            }
                                        }

                                        if (oEntry.getDataValue() == null || oEntry.getDataValue() == "") {
                                            s = oEntry.getValue()
                                            s = s.replace(" - ", "")
                                            anyadirInput(sInput, s)
                                        }
                                        else
                                            anyadirInput(sInput, oEntry.getDataValue())

                                        sInput = "DESGCCDHNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                        anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                        sInput = "DESGCCDPNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                        anyadirInput(sInput, oEntry.CampoDef_DesgPadre)
                                    }
                                    else
                                        if (oEntry.tipoGS == 110 || oEntry.tipoGS == 111 || oEntry.tipoGS == 112 || oEntry.tipoGS == 113) {
                                            sInput = "DESG_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_1"
                                            anyadirInput(sInput, oEntry.getDataValue())

                                            sInput = "DESGCCDHNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                            anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                            sInput = "DESGCCDPNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                            anyadirInput(sInput, oEntry.CampoDef_DesgPadre)
                                        }
                                        else
                                            if (oEntry.tipoGS == 42) {	//Estado actual	
                                                if (iLinea == 0) { iLinea = 1 }
                                                sInput = "DESG_" + oEntry.idCampoPadre + "_" + 1000 + "_" + iLinea + "_" + oEntry.idCampo + "_1"
                                                anyadirInput(sInput, oEntry.getDataValue())

                                                sInput = "DESGCCDHNP_" + oEntry.idCampoPadre + "_" + 1000 + "_" + iLinea + "_" + oEntry.idCampo + "_1"
                                                anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                                sInput = "DESGCCDPNP_" + oEntry.idCampoPadre + "_" + 1000 + "_" + iLinea + "_" + oEntry.idCampo + "_1"
                                                anyadirInput(sInput, oEntry.CampoDef_DesgPadre)
                                            }
                                            else
                                                if (oEntry.tipoGS == 118) {
                                                    oEntryCodArt = null;
                                                    idEntryCod = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 1, oEntry.nLinea, 1)
                                                    if (idEntryCod)
                                                        oEntryCodArt = fsGeneralEntry_getById(idEntryCod)
                                                    if ((!oEntryCodArt) || ((oEntryCodArt) && (oEntryCodArt.tipoGS != 119))) {

                                                        sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                                        re = eval(sRe)
                                                        x = oEntry.IdCodArticuloOculta.search(re) + sRe.length - 1
                                                        y = oEntry.IdCodArticuloOculta.length - x
                                                        idCod = oEntry.IdCodArticuloOculta.substr(x, y)

                                                        if ((!oEntry.articuloGenerico) && (oEntry.DenArticuloModificado)) {
                                                            sInputCodArt = "DESG_" + oEntry.idCampoPadre + "_" + idCod + "_" + iLinea + "_6"
                                                            anyadirInput(sInputCodArt, "nulo")

                                                        } else
                                                            if (oEntry.codigoArticulo) {
                                                                sInputCodArt = "DESG_" + oEntry.idCampoPadre + "_" + idCod + "_" + iLinea + "_6"
                                                                anyadirInput(sInputCodArt, oEntry.codigoArticulo)
                                                            }
                                                        sInputCodArt = "DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idCod + "_" + iLinea + "_6"
                                                        anyadirInput(sInputCodArt, -1000)
                                                        sInputCodArt = "DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idCod + "_" + iLinea + "_6"
                                                        anyadirInput(sInputCodArt, oEntry.CampoDef_DesgPadre)
                                                        if (oEntry.idEntryUNI != null) {
                                                            oUni = fsGeneralEntry_getById(oEntry.idEntryUNI)
                                                            if (!oUni) { //La Unidad est� oculta				
                                                                sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                                                re = eval(sRe)
                                                                x = oEntry.idEntryUNI.search(re) + sRe.length - 1
                                                                y = oEntry.idEntryUNI.length - x
                                                                idUni = oEntry.idEntryUNI.substr(x, y)
                                                                sInputUni = "DESG_" + oEntry.idCampoPadre + "_" + idUni + "_" + iLinea + "_6"
                                                                anyadirInput(sInputUni, oEntry.UnidadDependent.value)
                                                                sInputUni = "DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idUni + "_" + iLinea + "_6"
                                                                anyadirInput(sInputUni, -1000)
                                                                sInputUni = "DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idUni + "_" + iLinea + "_6"
                                                                anyadirInput(sInputUni, oEntry.CampoDef_DesgPadre)
                                                            }
                                                        }
                                                    }

                                                    //Comprobar si el Material esta oculto
                                                    var oMat
                                                    if (oEntry.dependentfield != null) {
                                                        oMat = fsGeneralEntry_getById(oEntry.dependentfield)
                                                        if (!oMat) //el material est� oculto
                                                        {
                                                            sRe = "/fsdsentry_" + iLineaOriginal.toString() + "/"
                                                            re = eval(sRe)
                                                            x = oEntry.IdMaterialOculta.search(re) + sRe.length - 1
                                                            y = oEntry.IdMaterialOculta.length - x
                                                            idMat = oEntry.IdMaterialOculta.substr(x, y)

                                                            sInputMat = "DESG_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6"
                                                            anyadirInput(sInputMat, oEntry.Dependent.value)
                                                            sInputMat = "DESGCCDHNP_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6"
                                                            anyadirInput(sInputMat, -1000)
                                                            sInputMat = "DESGCCDPNP_" + oEntry.idCampoPadre + "_" + idMat + "_" + iLinea + "_6"
                                                            anyadirInput(sInputMat, oEntry.CampoDef_DesgPadre)
                                                        }
                                                    }

                                                    anyadirInput(sInput, oEntry.getDataValue())

                                                    sInput = "DESGCCDHNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                                    anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                                    sInput = "DESGCCDPNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString()
                                                    anyadirInput(sInput, oEntry.CampoDef_DesgPadre)
                                                }
                                                else {
                                                    if (oEntry.tipoGS != 3000) {
                                                        anyadirInput("DESGCCDHNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString(), oEntry.CampoDef_CampoODesgHijo)
                                                        anyadirInput("DESGCCDPNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_" + oEntry.tipo.toString(), oEntry.CampoDef_DesgPadre)
                                                        if (oEntry.tipoGS == 45) { //Importe Repercutido
                                                            anyadirInput(sInput, oEntry.getDataValue())

                                                            sInputMon = "DMONREPER_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_1"
                                                            anyadirInput(sInputMon, oEntry.MonRepercutido)

                                                            sInputEquiv = "DEQREPER_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea + "_1"
                                                            anyadirInput(sInputEquiv, oEntry.CambioRepercutido)
                                                        }
                                                        else {
                                                            if (oEntry.TablaExterna > 0) {
                                                                if ((oEntry.tipo == 2) && (oEntry.getDataValue() != null))
                                                                    anyadirInput(sInput, oEntry.dataValue.replace(",", "."))
                                                                else
                                                                    anyadirInput(sInput, oEntry.getDataValue())
                                                            } else {
                                                                if ((oEntry.tipo == 2) && (oEntry.intro == 1)) {
                                                                    vValor = oEntry.getDataValue()
                                                                    if (vValor != null) {
                                                                        if (vValor.search(',')) {
                                                                            vValor = vValor.toString().replace(",", ".")
                                                                        }
                                                                    }
                                                                    anyadirInput(sInput, vValor)
                                                                } else
                                                                    anyadirInput(sInput, oEntry.getDataValue())
                                                            }
                                                        }
                                                    }
                                                }
                        }


                        //hay que obtener la l�nea con la que estaba guardado en  bd				
                        sPre = oEntry.id.substr(0, x - 10)
                        //Para cuando un desglose sea no emergente para que obtenga bien el n� de linea
                        //"uwtGrupos:_ctl0x:7690:7690_275575:_1_Linea" -->Para obtener bien la linea
                        //"uwtGrupos:_ctl0x:7690:7690:275575:_1_linea" -->Antes
                        sAux = sPre.split("_")
                        var sCampoHijo = null
                        if (sAux.length = 7) {
                            sCampoHijo = sAux[sAux.length - 2]
                            if (sCampoHijo)
                                sPre = sPre.substr(0, sPre.length - (sCampoHijo.length + 2))
                        }

                        sPre = replaceAll(sPre, "__", "::")
                        sPre = replaceAll(sPre, "_", "$")
                        sPre = replaceAll(sPre, "::", "$_")

                        var iLineaOld
                        if (!sCampoHijo) {
                            if (f.elements[sPre + "_" + iLinea + "_linea"])
                                iLineaOld = f.elements[sPre + "_" + iLinea + "_linea"].value
                            else
                                iLineaOld = null
                        } else {
                            if (f.elements[sPre + "_" + sCampoHijo + "$_" + iLineaOriginal + "_linea"])
                                iLineaOld = f.elements[sPre + "_" + sCampoHijo + "$_" + iLineaOriginal + "_linea"].value
                            else
                                iLineaOld = null
                        }

                        //input que dir� la l�nea de bd: IdCampoPadre_Linea
                        sInput = "LINDESG_" + oEntry.idCampoPadre + "_" + iLinea
                        anyadirInput(sInput, iLineaOld)

                        if (oEntry.tipo == 8) {

                            var inputFiles = document.getElementById(oEntry.id + "__hNew")
                            files = inputFiles.value.split("xx")
                            for (file in files) {
                                sInput = "ADJUNDESG_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea.toString() + "_2_" + file.toString()
                                anyadirInput(sInput, files[file])
                                if ((files[file].toString() != '') && ((files[file].toString() != 'null'))) {
                                    sInput = "ADJUNDESGCCDHNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea.toString() + "_2_" + files[file].toString()
                                    anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                    sInput = "ADJUNDESGCCDPNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea.toString() + "_2_" + files[file].toString()
                                    anyadirInput(sInput, oEntry.CampoDef_DesgPadre)
                                }
                            }
                            inputFiles = document.getElementById(oEntry.id + "__hAct")
                            files = inputFiles.value.split("xx")
                            for (file in files) {
                                sInput = "ADJUNDESG_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea.toString() + "_1_" + file.toString()
                                anyadirInput(sInput, files[file])
                                if ((files[file].toString() != '') && ((files[file].toString() != 'null'))) {
                                    sInput = "ADJUNDESGCCDHNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea.toString() + "_1_" + files[file].toString()
                                    anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                    sInput = "ADJUNDESGCCDPNP_" + oEntry.idCampoPadre + "_" + oEntry.idCampo + "_" + iLinea.toString() + "_1_" + files[file].toString()
                                    anyadirInput(sInput, oEntry.CampoDef_DesgPadre)
                                }
                            }
                        }
                    }
                }
                else {
                    if (oEntry.tipo == 9) //desglose
                    {
                        //Si el desglose no tiene lineas. No entra en el bucle. Luego no habra inputs -> Sin registros para tabla COPIA_DESGLOSE
                        if (f.elements[oEntry.id + "__numTotRows"].value == 0) {
                            for (arrCampoHijo in oEntry.arrCamposHijos) {
                                anyadirInput("DESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_0_1", "")
                                anyadirInput("DESGCCDPP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_0_1", oEntry.CampoDef_CampoODesgHijo)
                            }

                            var CampoDef;
                            for (arrCampoHijoDef in oEntry.arrCamposHijos_CampoDef) {
                                CampoDef = oEntry.arrCamposHijos_CampoDef[arrCampoHijoDef].split("xx")
                                anyadirInput("DESGCCDHP_" + oEntry.idCampo + "_" + CampoDef[0] + "_0_1", CampoDef[1])
                            }
                        }
                        //----
                        var i
                        for (k = 1; k <= f.elements[oEntry.id + "__numTotRows"].value; k++) {
                            oNoDeleted = f.elements[oEntry.id + "__" + k.toString() + "__Deleted"]

                            oLinea = f.elements[oEntry.id + "__" + k.toString() + "__linea"]
                            if (oLinea) {
                                //input que dir� la l�nea de bd: IdCampoPadre_<LineaOld>_Linea
                                sInput = "LINDESG_" + oEntry.idCampo + "_" + k.toString()
                                anyadirInput(sInput, oLinea.value)
                            }
                            var visibleArt = false;
                            for (arrCampoHijo in oEntry.arrCamposHijos) {
                                //encontrar el copia_campo_def
                                var CampoDef;
                                var CampoDefHijo;
                                for (arrCampoHijoDef in oEntry.arrCamposHijos_CampoDef) {
                                    CampoDef = oEntry.arrCamposHijos_CampoDef[arrCampoHijoDef].split("xx")
                                    if (CampoDef[0] == oEntry.arrCamposHijos[arrCampoHijo])
                                        CampoDefHijo = CampoDef[1]
                                }
                                //
                                oHijo = f.elements[oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo]]
                                if (oHijo) {
                                    //input de desglose formato: IdCampoPadre_IdCampoHijo_Linea
                                    oHijoSrc = f.elements[oEntry.id + "__" + oEntry.arrCamposHijos[arrCampoHijo]];
                                    if (oHijoSrc) {
                                        anyadirInput("DESGTIPOGS_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString(), oHijoSrc.getAttribute("tipoGS"));
                                        if (Mapper == true) {
                                            if (oHijoSrc.getAttribute("calculado") == "1") { //identifica si es campo calculado
                                                sInput = "DESGCALC_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                anyadirInput(sInput, oHijoSrc.getAttribute("calculado"))
                                                sInput = "DESGCALCTIT_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                anyadirInput(sInput, oHijoSrc.title)
                                            }

                                            if (oHijoSrc.getAttribute("IdAtrib") != 0) {
                                                sInput = "DESGAT_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                anyadirInput(sInput, oHijoSrc.getAttribute("IdAtrib"))
                                                sInput = "DESGERP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                anyadirInput(sInput, oHijoSrc.getAttribute("ValErp"))
                                            }

                                            if (oHijoSrc.getAttribute("TablaExterna") > 0) {
                                                sInput = "DESGTABLAEXT_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                anyadirInput(sInput, oHijoSrc.getAttribute("TablaExterna"))
                                            }
                                        }
                                        if (oHijoSrc.getAttribute("intro") == 1 && (oHijoSrc.getAttribute("tipo") == 1 || oHijoSrc.getAttribute("tipo") == 5 || oHijoSrc.getAttribute("tipo") == 6 || oHijoSrc.getAttribute("tipo") == 7) && (oHijoSrc.getAttribute("tipoGS") == 0 || oHijoSrc.getAttribute("tipoGS") == 46 || oHijoSrc.getAttribute("tipoGS") == 148 || oHijoSrc.getAttribute("tipoGS") == 150)) {
                                            sInput = "DESGID_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                            anyadirInput(sInput, oHijo.value)

                                            if (((oHijoSrc.getAttribute("tipo") == 1) || (oHijoSrc.getAttribute("tipo") == 5) || (oHijoSrc.getAttribute("tipo") == 6)) && (oHijoSrc.getAttribute("tipoGS") == 0)) {
                                                oDescrComboPopUp = f.elements[oHijo.id + "_DescrComboPopUp"]
                                                if (oDescrComboPopUp)
                                                    anyadirInput("DESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString(), oDescrComboPopUp.value)
                                                else//invisible
                                                    anyadirInput("DESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString(), oHijo.value)
                                            }
                                            else
                                                anyadirInput("DESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString(), oHijo.value)

                                            sInput = "DESGCCDHP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                            anyadirInput(sInput, CampoDefHijo)
                                            sInput = "DESGCCDPP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                            anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                        }
                                        else {
                                            sInput = "DESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                            oAdjun = f.elements[oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo] + "_hNew"]
                                            if (oAdjun) {
                                                oAdjunNombre = f.elements[oEntry.id + "__" + k.toString() + "__" + oEntry.arrCamposHijos[arrCampoHijo] + "_hNombre"]
                                                if (oAdjunNombre)
                                                    anyadirInput(sInput, oAdjunNombre.value)
                                                else
                                                    anyadirInput(sInput, "")
                                                files = oHijo.value.split("xx")
                                                for (file in files) {
                                                    sInput = "ADJUNDESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_1_" + file.toString()
                                                    anyadirInput(sInput, files[file])
                                                    if ((files[file].toString() != '') && ((files[file].toString() != 'null'))) {
                                                        sInput = "ADJUNDESGCCDHP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_1_" + files[file].toString()
                                                        anyadirInput(sInput, CampoDefHijo)

                                                        sInput = "ADJUNDESGCCDPP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_1_" + files[file].toString()
                                                        anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                                    }
                                                }
                                                files = oAdjun.value.split("xx")
                                                for (file in files) {
                                                    sInput = "ADJUNDESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_2_" + file.toString()
                                                    anyadirInput(sInput, files[file])
                                                    if ((files[file].toString() != '') && ((files[file].toString() != 'null'))) {
                                                        sInput = "ADJUNDESGCCDHP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_2_" + files[file].toString()
                                                        anyadirInput(sInput, CampoDefHijo)
                                                        sInput = "ADJUNDESGCCDPP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_2_" + files[file].toString()
                                                        anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                                    }
                                                }
                                                sInput = "DESGCCDHP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                anyadirInput(sInput, CampoDefHijo)
                                                sInput = "DESGCCDPP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                            }
                                            else
                                                if (oHijoSrc.getAttribute("tipoGS") == 110 || oHijoSrc.getAttribute("tipoGS") == 111 || oHijoSrc.getAttribute("tipoGS") == 112 || oHijoSrc.getAttribute("tipoGS") == 113) {
                                                    //los presupuestos 
                                                    sInput = "DESG_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_1"
                                                    anyadirInput(sInput, oHijo.value)

                                                    sInput = "DESGCCDHP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                    anyadirInput(sInput, CampoDefHijo)
                                                    sInput = "DESGCCDPP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                    anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                                }
                                                else
                                                    if (oHijoSrc.getAttribute("tipoGS") == 42) {	//DESG_8083_1000_1_6
                                                        sInput = "DESG_" + oEntry.idCampo + "_" + 1000 + "_" + k.toString() + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_1"
                                                        anyadirInput(sInput, oHijo.value)	//Estado actual		

                                                        sInput = "DESGCCDHP_" + oEntry.idCampo + "_" + 1000 + "_" + k.toString() + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_1"
                                                        anyadirInput(sInput, CampoDefHijo)
                                                        sInput = "DESGCCDPP_" + oEntry.idCampo + "_" + 1000 + "_" + k.toString() + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_1"
                                                        anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                                    }
                                                    else
                                                        if (oHijoSrc.getAttribute("tipoGS") == 119) //Codigo de Articulo (Nuevas Solicitudes)
                                                        {
                                                            anyadirInput(sInput, oHijo.value);
                                                            if ((oHijo.value != null) && (oHijo.value != "")) {
                                                                visibleArt = true;
                                                            }
                                                            sInput = "DESGCCDHP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                            anyadirInput(sInput, CampoDefHijo)
                                                            sInput = "DESGCCDPP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                            anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                                        }
                                                        else
                                                            if (oHijoSrc.getAttribute("tipoGS") == 118) {
                                                                if ((oHijo.codigoArticulo != null) && (oHijo.codigoArticulo != "")) {
                                                                    sInputCodArt = "DESG_" + oEntry.idCampo + "_" + (oEntry.arrCamposHijos[arrCampoHijo - 1]) + "_" + k.toString() + "_6"
                                                                    var fSubmit = document.forms["frmSubmit"] //Hay que obtener el input del Articulo que se va a pasar,
                                                                    if (!fSubmit.elements[sInputCodArt])		 // anteriormente se ha pasado el articulo aun estando invisible.
                                                                        anyadirInput(sInputCodArt, oHijo.codigoArticulo)
                                                                    else
                                                                        fSubmit.elements[sInputCodArt].value = oHijo.codigoArticulo
                                                                } else
                                                                    if ((!oHijo.articuloGenerico) && (oHijo.DenArticuloModificado) && (!visibleArt)) {
                                                                        sInputCodArt = "DESG_" + oEntry.idCampo + "_" + (oEntry.arrCamposHijos[arrCampoHijo - 1]) + "_" + k.toString() + "_6"
                                                                        var fSubmit = document.forms["frmSubmit"] //Hay que obtener el input del Articulo que se va a pasar,
                                                                        if (!fSubmit.elements[sInputCodArt])		 // anteriormente se ha pasado el articulo aun estando invisible.
                                                                            anyadirInput(sInputCodArt, "nulo")
                                                                        else
                                                                            fSubmit.elements[sInputCodArt].value = "nulo"
                                                                    }

                                                                anyadirInput(sInput, oHijo.value)
                                                                visibleArt = false;

                                                                sInput = "DESGCCDHP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                                anyadirInput(sInput, CampoDefHijo)
                                                                sInput = "DESGCCDPP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                                anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                                            }
                                                            else
                                                                if (oHijoSrc.tipoGS == 104) {
                                                                    anyadirInput(sInput, oHijo.value)

                                                                    sInput = "DESGCCDHP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                                    anyadirInput(sInput, CampoDefHijo)
                                                                    sInput = "DESGCCDPP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString()
                                                                    anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                                                }
                                                                else {
                                                                    if (oHijoSrc.getAttribute("tipoGS") != 3000) {
                                                                        anyadirInput("DESGCCDHP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString(), CampoDefHijo)
                                                                        anyadirInput("DESGCCDPP_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_" + oHijoSrc.getAttribute("tipo").toString(), oEntry.CampoDef_CampoODesgHijo)
                                                                        if (oHijoSrc.tipoGS == 45) { //Importe Repercutido
                                                                            anyadirInput(sInput, oHijo.value)

                                                                            oRepercutido = f.elements[oHijo.id + "_MonRepercutido"]
                                                                            sInputMon = "DMONREPER_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_1"
                                                                            if (oRepercutido)
                                                                                anyadirInput(sInputMon, oRepercutido.value)
                                                                            else
                                                                                anyadirInput(sInputMon, "EUR")//CompletarFormulario lo cambia a MONCEN. Lineas a�adidas y �copiadas? van , hay oRepercutido. El No Pop van todas????

                                                                            oRepercutido = f.elements[oHijo.id + "_CambioRepercutido"]
                                                                            sInputEquiv = "DEQREPER_" + oEntry.idCampo + "_" + oEntry.arrCamposHijos[arrCampoHijo] + "_" + k.toString() + "_1"
                                                                            if (oRepercutido)
                                                                                anyadirInput(sInputEquiv, oRepercutido.value)
                                                                            else
                                                                                anyadirInput(sInputEquiv, 1)//CompletarFormulario lo cambia a CAMBIOCEN. Lineas a�adidas y �copiadas? van , hay oRepercutido. El No Pop van todas???? 

                                                                            visibleArt = false;
                                                                        }
                                                                        else {
                                                                            if ((oHijoSrc.getAttribute("tipo") == 2) && (oHijoSrc.getAttribute("intro") == 1)) {
                                                                                vValor = oHijo.value
                                                                                if (vValor != null) {
                                                                                    if (vValor.search(',')) {
                                                                                        vValor = vValor.toString().replace(",", ".")
                                                                                    }
                                                                                }
                                                                                anyadirInput(sInput, vValor)
                                                                            } else {
                                                                                anyadirInput(sInput, oHijo.value)
                                                                            }
                                                                            visibleArt = false;
                                                                        }
                                                                    }
                                                                }

                                        }
                                    }

                                }
                            }
                        }
                    }
                    else {
                        if (oEntry.tipoGS != 115 && oEntry.tipoGS != 116 && oEntry.tipoGS != 117) anyadirInput("CAMPOTIPOGS_" + oEntry.idCampo, oEntry.tipoGS);
                        if (oEntry.tipo == 8) {

                            var inputFiles = document.getElementById(oEntry.id + "__hNew")
                            files = inputFiles.value.split("xx")
                            for (file in files) {
                                sInput = "ADJUN_" + oEntry.idCampo + "_2_" + file.toString()
                                anyadirInput(sInput, files[file])
                                if ((files[file].toString() != '') && ((files[file].toString() != 'null'))) {
                                    sInput = "ADJUNCCD_" + oEntry.idCampo + "_2_" + files[file].toString()
                                    anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                }
                            }
                            inputFiles = document.getElementById(oEntry.id + "__hAct")
                            files = inputFiles.value.split("xx")
                            for (file in files) {
                                if (oEntry.tipoGS == 135) { //Archivo de contrato
                                    sInput = "ADJUN_" + oEntry.idCampo + "_5_" + file.toString()
                                    anyadirInput(sInput, files[file])
                                    if ((files[file].toString() != '') && ((files[file].toString() != 'null'))) {
                                        sInput = "ADJUNCCD_" + oEntry.idCampo + "_5_" + files[file].toString()
                                        anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                    }
                                } else {
                                    sInput = "ADJUN_" + oEntry.idCampo + "_1_" + file.toString()
                                    anyadirInput(sInput, files[file])
                                    if ((files[file].toString() != '') && ((files[file].toString() != 'null'))) {
                                        sInput = "ADJUNCCD_" + oEntry.idCampo + "_1_" + files[file].toString()
                                        anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)

                                        comentFile = document.getElementById(oEntry.id + "__text" + files[file])
                                        if (comentFile) {
                                            if (comentFile.value != '') {
                                                sInput = "ADJUNCOMENT_" + oEntry.idCampo + "_1_" + files[file].toString()
                                                anyadirInput(sInput, comentFile.value)
                                            }
                                        }
                                    }
                                }
                            }
                            sInput = "CAMPO_" + oEntry.idCampo + "_" + oEntry.tipo.toString()
                            anyadirInput(sInput, oEntry.getValue())

                            sInput = "CAMPOCCD_" + oEntry.idCampo + "_" + oEntry.tipo.toString()
                            anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                        }
                        else
                            if (oEntry.idCampo == "PROVE" || oEntry.idCampo == "SOLICIT" || oEntry.idCampo == "CON")//proveedores a los que emitir el certificado
                            {
                                //uwtGrupos__ctl0xx__ctl0_fsdsentry_2_CON
                                re = /fsdsentry/
                                x = oEntry.id.search(re) + 10
                                y = oEntry.id.search(oEntry.idCampo) - 1

                                if (oEntry.id.search(re) >= 0 && oEntry.idCampo != "SOLICIT") //y no es el input base del desglose
                                {
                                    iFila = oEntry.id.substr(x, y - x)
                                    sSolicit = oEntry.id.substr(0, y) + "_SOLICIT"
                                    oSolicit = fsGeneralEntry_getById(sSolicit)
                                    if (oSolicit)
                                        if (oSolicit.getValue() == 1) {
                                            sInput = oEntry.idCampo + "_" + iFila.toString()
                                            anyadirInput(sInput, oEntry.getDataValue())
                                        }
                                }
                            }
                            else //campos simples
                            {
                                if (Mapper == true && oEntry.IdAtrib != 0) {
                                    sInput = "CAMPOGRUPO_" + oEntry.idCampo
                                    anyadirInput(sInput, oEntry.Grupo)
                                    sInput = "CAMPOORDEN_" + oEntry.idCampo
                                    anyadirInput(sInput, oEntry.orden)
                                    sInput = "CAMPOAT_" + oEntry.idCampo
                                    anyadirInput(sInput, oEntry.IdAtrib)
                                    sInput = "CAMPOERP_" + oEntry.idCampo
                                    anyadirInput(sInput, oEntry.ValErp)
                                }
                                sInput = "CAMPO_" + oEntry.idCampo + "_" + oEntry.tipo.toString()
                                if (oEntry.intro == 1 && (oEntry.tipo == 1 || oEntry.tipo == 5 || oEntry.tipo == 6 || oEntry.tipo == 7) && (oEntry.tipoGS == 0 || oEntry.tipoGS == 46 || oEntry.tipoGS == 148 || oEntry.tipoGS == 150)) {
                                    anyadirInput(sInput, oEntry.getValue())
                                    sInput = "CAMPOID_" + oEntry.idCampo + "_" + oEntry.tipo.toString()
                                    anyadirInput(sInput, oEntry.getDataValue())

                                    sInput = "CAMPOCCD_" + oEntry.idCampo
                                    anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                }
                                else
                                    if (oEntry.tipoGS == 104)  //CodArticulo Solicitudes anteriores
                                    {
                                        if (oEntry.dependentfield != null) {
                                            oMat = fsGeneralEntry_getById(oEntry.dependentfield)
                                            if (!oMat) //el material est� oculto
                                            {
                                                re = /fsentry/
                                                x = oEntry.dependentfield.search(re) + 7
                                                y = oEntry.dependentfield.length - x
                                                idMat = oEntry.dependentfield.substr(x, y)
                                                sInputMat = "CAMPO_" + idMat + "_6"
                                                anyadirInput(sInputMat, oEntry.Dependent.value)
                                                sInputH = "CAMPOCCD_" + idMat
                                                anyadirInput(sInputH, -1000)
                                            }
                                        }

                                        //Si la UNIDAD esta oculta									
                                        if (oEntry.idEntryUNI != null) {
                                            oUni = fsGeneralEntry_getById(oEntry.idEntryUNI)
                                            if (!oUni) { //La Unidad est� oculta										
                                                re = /fsentry/
                                                x = oEntry.idEntryUNI.search(re) + 7
                                                y = oEntry.idEntryUNI.length - x
                                                idUni = oEntry.idEntryUNI.substr(x, y)
                                                sInputUni = "CAMPO_" + idUni + "_6"
                                                anyadirInput(sInputUni, oEntry.UnidadDependent.value)
                                                sInputH = "CAMPOCCD_" + idUni
                                                anyadirInput(sInputH, -1000)
                                            }
                                        }
                                        if (oEntry.getDataValue() == null || oEntry.getDataValue() == "") {
                                            s = oEntry.getValue()
                                            s = s.replace(" - ", "")
                                            anyadirInput(sInput, s)
                                        }
                                        else
                                            anyadirInput(sInput, oEntry.getDataValue())
                                        if (Mapper == true) {
                                            sInput = "CAMPOGRUPO_" + oEntry.idCampo
                                            anyadirInput(sInput, oEntry.Grupo)
                                            sInput = "CAMPOORDEN_" + oEntry.idCampo
                                            anyadirInput(sInput, oEntry.orden)
                                        }
                                        sInput = "CAMPOCCD_" + oEntry.idCampo
                                        anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                    }
                                    else
                                        if (oEntry.tipoGS == 119) //CodArticulo Nuevas Solicitudes
                                        {
                                            if (oEntry.dependentfield != null) {
                                                oMat = fsGeneralEntry_getById(oEntry.dependentfield)
                                                if (!oMat) //el material est� oculto
                                                {
                                                    re = /fsentry/
                                                    x = oEntry.dependentfield.search(re) + 7
                                                    y = oEntry.dependentfield.length - x
                                                    idMat = oEntry.dependentfield.substr(x, y)
                                                    sInputMat = "CAMPO_" + idMat + "_6"
                                                    anyadirInput(sInputMat, oEntry.Dependent.value)
                                                    sInputH = "CAMPOCCD_" + idMat
                                                    anyadirInput(sInputH, -1000)
                                                    if (Mapper == true) {
                                                        anyadirInput("CAMPOGRUPO_" + idMat, oEntry.Grupo);
                                                        anyadirInput("CAMPOORDEN_" + idMat, oEntry.orden);
                                                    }
                                                }
                                                oDen = null;
                                                idEntryDen = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna + 1, oEntry.nLinea, 1)
                                                if (idEntryDen)
                                                    oDen = fsGeneralEntry_getById(idEntryDen)
                                                if ((!oDen) || ((oDen) && (oDen.tipoGS != 118)) && (oEntry.tag != null)) { //La denominacion esta oculta
                                                    re = /fsentry/
                                                    x = oEntry.dependentfield.search(re) + 7
                                                    y = oEntry.dependentfield.length - x
                                                    idDen = oEntry.dependentfield.substr(x, y)

                                                    sInputDen = "CAMPO_" + idDen + "_6"
                                                    anyadirInput(sInputDen, oEntry.tag)
                                                    sInputH = "CAMPOCCD_" + idDen
                                                    anyadirInput(sInputH, -1000)
                                                }
                                            }
                                            //Si la UNIDAD esta oculta									
                                            if (oEntry.idEntryUNI != null) {
                                                oUni = fsGeneralEntry_getById(oEntry.idEntryUNI)
                                                if (!oUni) { //La Unidad est� oculta										
                                                    re = /fsentry/
                                                    x = oEntry.idEntryUNI.search(re) + 7
                                                    y = oEntry.idEntryUNI.length - x
                                                    idUni = oEntry.idEntryUNI.substr(x, y)
                                                    sInputUni = "CAMPO_" + idUni + "_6"
                                                    anyadirInput(sInputUni, oEntry.UnidadDependent.value)
                                                    sInputH = "CAMPOCCD_" + idUni
                                                    anyadirInput(sInputH, -1000)
                                                }
                                            }
                                            if (oEntry.getDataValue() == null || oEntry.getDataValue() == "") {
                                                s = oEntry.getValue()
                                                s = s.replace(" - ", "")
                                                anyadirInput(sInput, s)
                                            }
                                            else
                                                anyadirInput(sInput, oEntry.getDataValue())
                                            if (Mapper == true) {
                                                anyadirInput("CAMPOGRUPO_" + oEntry.idCampo, oEntry.Grupo);
                                                anyadirInput("CAMPOORDEN_" + oEntry.idCampo, oEntry.orden);
                                            }
                                            sInput = "CAMPOCCD_" + oEntry.idCampo
                                            anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                        }
                                        else
                                            if (oEntry.tipoGS == 115 || oEntry.tipoGS == 116) //Participantes
                                            {
                                                if (oEntry.intro == 1) {
                                                    oRol = fsGeneralEntry_getById(oEntry.dependentfield)
                                                    sRol = oRol.getDataValue()
                                                    sInput = "PART_" + oEntry.tipoGS.toString() + "_" + sRol
                                                    anyadirInput(sInput, oEntry.getDataValue())
                                                }
                                                else {
                                                    anyadirInput("CAMPOTIPOGS_" + oEntry.idCampo, oEntry.tipoGS);
                                                    sInput = "CAMPO_" + oEntry.idCampo + "_" + oEntry.tipo.toString()
                                                    anyadirInput(sInput, oEntry.getDataValue())

                                                    sInput = "CAMPOCCD_" + oEntry.idCampo
                                                    anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                                }
                                            }
                                            else
                                                if (oEntry.tipoGS == 117) //Rol
                                                {
                                                }
                                                else

                                                    if (oEntry.tipoGS == 110 || oEntry.tipoGS == 111 || oEntry.tipoGS == 112 || oEntry.tipoGS == 113) {
                                                        //los presupuestos 
                                                        sInput = "CAMPO_" + oEntry.idCampo + "_1"
                                                        anyadirInput(sInput, oEntry.getDataValue())

                                                        sInput = "CAMPOCCD_" + oEntry.idCampo
                                                        anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                                        if (oEntry.tipoGS == 113 && Mapper == true) {
                                                            anyadirInput("CAMPOGRUPO_" + oEntry.idCampo, oEntry.Grupo);
                                                            anyadirInput("CAMPOORDEN_" + oEntry.idCampo, oEntry.orden);
                                                        }
                                                    }
                                                    else
                                                        if (oEntry.tipoGS == 118) //Denominacion Articulo									
                                                        {
                                                            oEntryCodArt = null;
                                                            idEntryCod = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 1, oEntry.nLinea, 1)
                                                            if (idEntryCod)
                                                                oEntryCodArt = fsGeneralEntry_getById(idEntryCod)
                                                            if ((!oEntryCodArt) || ((oEntryCodArt) && (oEntryCodArt.tipoGS != 119))) {
                                                                re = /fsentry/
                                                                x = oEntry.IdCodArticuloOculta.search(re) + 7
                                                                y = oEntry.IdCodArticuloOculta.length - x
                                                                idCod = oEntry.IdCodArticuloOculta.substr(x, y)

                                                                if ((oEntry.DenArticuloModificado) && (!oEntry.articuloGenerico)) {
                                                                    //Si el Codigo de Articulo esta oculto, se trata de un articulo NO Generico y se ha modificado la denominacion
                                                                    //eliminar el codigo de articulo
                                                                    sInputCodArt = "CAMPO_" + idCod + "_6"
                                                                    anyadirInput(sInputCodArt, null)
                                                                } else
                                                                    if (oEntry.codigoArticulo) {
                                                                        sInputCodArt = "CAMPO_" + idCod + "_6"
                                                                        anyadirInput(sInputCodArt, oEntry.codigoArticulo)
                                                                    }
                                                                sInputH = "CAMPOCCD_" + idCod
                                                                anyadirInput(sInputH, -1000)
                                                                //Si el Codigo de Articulo esta oculto, mirar Si la UNIDAD esta oculta									
                                                                if (oEntry.idEntryUNI != null) {
                                                                    oUni = fsGeneralEntry_getById(oEntry.idEntryUNI)
                                                                    if (!oUni) { //La Unidad est� oculta										
                                                                        re = /fsentry/
                                                                        x = oEntry.idEntryUNI.search(re) + 7
                                                                        y = oEntry.idEntryUNI.length - x
                                                                        idUni = oEntry.idEntryUNI.substr(x, y)
                                                                        sInputUni = "CAMPO_" + idUni + "_6"
                                                                        anyadirInput(sInputUni, oEntry.UnidadDependent.value)
                                                                        sInputH = "CAMPOCCD_" + idUni
                                                                        anyadirInput(sInputH, -1000)
                                                                    }
                                                                }
                                                            }
                                                            oEntryMat = null;
                                                            idEntryMat = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 2, oEntry.nLinea, 1)
                                                            if (idEntryMat)
                                                                oEntryMat = fsGeneralEntry_getById(idEntryMat)
                                                            if ((!oEntryMat) || ((oEntryMat) && (oEntryMat.tipoGS != 103))) {
                                                                re = /fsentry/
                                                                x = oEntry.IdMaterialOculta.search(re) + 7
                                                                y = oEntry.IdMaterialOculta.length - x
                                                                idMat = oEntry.IdMaterialOculta.substr(x, y)

                                                                //Si la Estructura del Material esta oculta...
                                                                sInputMat = "CAMPO_" + idMat + "_6"
                                                                anyadirInput(sInputMat, oEntry.Dependent.value)
                                                                sInputH = "CAMPOCCD_" + idMat
                                                                anyadirInput(sInputH, -1000)
                                                            }
                                                            anyadirInput(sInput, oEntry.getDataValue())

                                                            sInput = "CAMPOCCD_" + oEntry.idCampo
                                                            anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                                        }
                                                        else
                                                            if (oEntry.tipoGS == 128) { // RefSolicitud 
                                                                sInputIDRefSol = "GEN_IDREFSOL"
                                                                anyadirInput(sInputIDRefSol, oEntry.getDataValue())
                                                                sInputAvisoBloqueo = "GEN_AVISOBLOQUEO"
                                                                anyadirInput(sInputAvisoBloqueo, oEntry.avisoBloqueoRefSOL)

                                                                anyadirInput(sInput, oEntry.getDataValue())

                                                                sInput = "CAMPOCCD_" + oEntry.idCampo
                                                                anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)
                                                            }
                                                            else
                                                                if (oEntry.tipoGS == 45) { //Importe Repercutido
                                                                    anyadirInput(sInput, oEntry.getValue())

                                                                    sInput = "CAMPOCCD_" + oEntry.idCampo
                                                                    anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo)

                                                                    sInputMon = "MONREPER_" + oEntry.idCampo
                                                                    anyadirInput(sInputMon, oEntry.MonRepercutido)

                                                                    sInputEquiv = "EQREPER_" + oEntry.idCampo
                                                                    anyadirInput(sInputEquiv, oEntry.CambioRepercutido)
                                                                }
                                                                else {
                                                                    if (oEntry.tipoGS == 121) { //Unidad organizativa
                                                                        anyadirInput(sInput, oEntry.getValue());
                                                                    }
                                                                    else {
                                                                        if (oEntry.TablaExterna > 0) {
                                                                            if (Mapper == true) {
                                                                                anyadirInput("CAMPOTABLAEXT_" + oEntry.idCampo, oEntry.TablaExterna)
                                                                                anyadirInput("CAMPOGRUPO_" + oEntry.idCampo, oEntry.Grupo)
                                                                                anyadirInput("CAMPOORDEN_" + oEntry.idCampo, oEntry.orden)
                                                                            }
                                                                            if ((oEntry.tipo == 2) && (oEntry.getDataValue() != null))
                                                                                anyadirInput(sInput, oEntry.dataValue.replace(",", "."))
                                                                            else
                                                                                anyadirInput(sInput, oEntry.getDataValue())
                                                                        } else {
                                                                            if ((oEntry.tipo == 2) && (oEntry.intro == 1)) {
                                                                                vValor = oEntry.getDataValue()
                                                                                if (vValor != null) {
                                                                                    if (vValor.search(',')) {
                                                                                        vValor = vValor.toString().replace(",", ".")
                                                                                    }
                                                                                }
                                                                                anyadirInput(sInput, vValor)
                                                                            }
                                                                            else
                                                                                anyadirInput(sInput, oEntry.getDataValue())
                                                                        }
                                                                    }
                                                                    sInput = "CAMPOCCD_" + oEntry.idCampo;
                                                                    anyadirInput(sInput, oEntry.CampoDef_CampoODesgHijo);
                                                                }
                            }
                    }
                }
            }
    }

    //si es una no conformidad y ha solicitado acciones:
    for (arrAccion in arrAcciones) {
        oEntry = fsGeneralEntry_getById(arrAcciones[arrAccion])
        if (oEntry) {
            sInput = "ACCION_" + oEntry.idCampo + "_" + oEntry.tipo.toString() + "_" + oEntry.CampoDef_DesgPadre.toString()
            anyadirInput(sInput, oEntry.getDataValue())
        }
    }

}
function finalizarAccion(e, desglose, index, idCampo) {
    var arrDesglose = desglose.split("_")

    if (arrDesglose.length == 6)
        var idGrupo = arrDesglose[arrDesglose.length - 2]
    else
        var idGrupo = arrDesglose[arrDesglose.length - 1]

    if (idGrupo == desglose)
        idGrupo = ""


    oEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + idGrupo.toString() + "1010")
    var arrEstado
    for (arrEstado in arrEstadosInternos)
        if (arrEstadosInternos[arrEstado][0] == 4)
            break;

    oEntry.setValue(arrEstadosInternos[arrEstado][1])
    oEntry.setDataValue(4)

    if (document.getElementById(desglose + "_cmdFinAccion_" + index.toString()))
        document.getElementById(desglose + "_cmdFinAccion_" + index.toString()).style.visibility = "hidden"

    //HA HABIDO UN CAMBIO DE ESTADO POR LO QUE HAY QUE NOTIFICAR
    if (document.getElementById("Notificar"))
        document.getElementById("Notificar").value = 1
}
function ddFechasSuministro_ValueChange(oEdit, oldValue, oEvent) {
    if (oEdit.fsEntry.tipoGS == 6) {  //El campo es INICIO DE SUMINISTRO
        //Mirar si la fecha de FIN de SUMINISTRO es menor a la de Inicio		
        if (oEdit.fsEntry.idDataEntryDependent) {
            oFSEntryFFIN = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent)
            if ((oFSEntryFFIN) && (oFSEntryFFIN.tipoGS == 7)) {
                if (oFSEntryFFIN.getValue())
                    if (oEdit.fsEntry.getValue() > oFSEntryFFIN.getValue()) {
                        //Cargar el ALERT de la BBDD						
                        alert(sMensajeFecha)
                        oEdit.fsEntry.setValue("")
                        oEdit.fsEntry.setDataValue("")
                        oEdit.lastText = "" //Un bug en ig_edit.js obliga a vaciar el lastTExt o de lo contrario no se lanzan eventos cuando es necesario
                        oEdit.setText("");
                        oEdit.setValue("");
                        oEdit.setDate("");
                        oEdit.old = null;//Vaciamos la fecha e impedimos que en ig_edit.js se le de valor de nuevo, para que se lance el evento valueChange siempre que es necesario
                        window.focus();//Los cambios de LastText y old hacen que el evento spin del control (las flechas) puedan entrar en bucle. Hacemos un focus fuera del control para impedirlo.
                    }
            }
        }
    } else
        if (oEdit.fsEntry.tipoGS == 7) {
            //Mirar si la fecha de INICIO de SUMINISTRO es mayor a la de fin	
            if (oEdit.fsEntry.idDataEntryDependent) {
                oFSEntryFINI = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent)
                if ((oFSEntryFINI) && (oFSEntryFINI.tipoGS == 6))
                    if (oFSEntryFINI.getValue())
                        if (oFSEntryFINI.getValue() > oEdit.fsEntry.getValue()) {
                            //Cargar el ALERT de la BBDD							
                            alert(sMensajeFecha)
                            oEdit.fsEntry.setValue("")
                            oEdit.fsEntry.setDataValue("")
                            oEdit.lastText = "" //Un bug en ig_edit.js obliga a vaciar el lastTExt o de lo contrario no se lanzan eventos cuando es necesario
                            oEdit.setText("");
                            oEdit.setValue("");
                            oEdit.setDate("");
                            oEdit.old = null;//Vaciamos la fecha e impedimos que en ig_edit.js se le de valor de nuevo, para que se lance el evento valueChange siempre que es necesario
                            window.focus();//Los cambios de LastText y old hacen que el evento spin del control (las flechas) puedan entrar en bucle. Hacemos un focus fuera del control para impedirlo.
                        }
            }
        }
    ddFec_ValueChange(oEdit, oldValue, oEvent)
}
function fslimitar_den_art(oEdit, text, oEvent) {
    oEdit = oEdit[0];
    if (oEdit.fsEntry.articuloGenerico == true) {
        oEdit.fsEntry.setValue(oEdit.fsEntry.getValue().substr(oEdit.fsEntry.getValue(), oEdit.fsEntry.maxLength))
        oEdit.fsEntry.setDataValue(oEdit.fsEntry.getDataValue().substr(oEdit.fsEntry.getDataValue(), oEdit.fsEntry.maxLength))
    } else if (oEdit.fsEntry.articuloGenerico == false) {
        if (oEvent.keyCode == 9)
            return;

        idEntryCod = null;
        if (oEvent.keyCode != 8) {
            alert(sMensajeGenerico)
        }
        else {
            oEdit.fsEntry.setValue('');
            oEdit.fsEntry.setDataValue('');
        }
        oEdit.fsEntry.articuloGenerico = true;
        oEdit.fsEntry.codigoArticulo = "";
        idEntryCod = calcularIdDataEntryCelda(oEdit.fsEntry.id, oEdit.fsEntry.nColumna - 1, oEdit.fsEntry.nLinea, 1)
        if (idEntryCod)
            oFSEntryCod = fsGeneralEntry_getById(idEntryCod);
        if ((oFSEntryCod) && (oFSEntryCod.tipoGS == 119)) {
            oFSEntryCod.setValue('')
            oFSEntryCod.setDataValue('')
        }

    }
}
function show_DetalleSolicitudPadre(oEdit, oldValue, oEvent) {
    var idSolPadre = oEdit.fsEntry.getDataValue();
    CerrarDesplegables();
    var newWindow = window.open("../_common/detallesolicitudpadre.aspx?idSolPadre=" + idSolPadre, "_blank", "width=530,height=130,status=yes,resizable=no,top=200,left=200");
    newWindow.focus()
}
//muestra el detalle de la partida cuando el dataentry es de solo lectura
function show_detalle_partida(oEdit, oldValue, oEvent) {
    var pres5 = oEdit.fsEntry.PRES5;
    var texto = oEdit.fsEntry.getDataValue();
    var titulo = oEdit.fsEntry.title

    CerrarDesplegables();
    if (texto != '') {
        var newWindow = window.open("../_common/detallePartida.aspx?titulo=" + titulo + "&pres5=" + pres5 + "&texto=" + escape(texto), "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250")
        newWindow.focus()
    }
}
//muestra el detalle del activo cuando el dataentry es de solo lectura
function show_detalle_activo(oEdit, oldValue, oEvent) {
    var value = oEdit.fsEntry.getDataValue();

    CerrarDesplegables();
    if (value != '') {
        var newWindow = window.open("../_common/detalleactivo.aspx?codActivo=" + escape(value), "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
        newWindow.focus()
    }

}
/*  Revisado por: blp. Fecha: 10/10/2011
''' Si no hay elemento seleccionado, se quita el texto que haya en el cuado de texto del webdropdown para no confundir
''' sender: control que lanza el evento
''' e: argumentos del evento
''' Llamada desde: Evento blur de webdropdown. Max 0,1 seg
*/
function WebDropDown_Focus(sender, e) {
    var dropDownEntry = fsGeneralEntry_getById(sender._id);
    var dropDown = $find(sender._id);
    switch (dropDownEntry.tipoGS) {
        case 101: //Forma pago
        case 102: //Moneda
        case 105: //Unidad
        case 107: //Pais
        case 108: //Provincia
        case 109: //Destino
        case 122: //Departamento
        case 123: //Org. Compras
        case 124: //Centro
        case 125: //Almacen
        case 143: //Proveedor ERP
            if (dropDown) {
                //Estos casos cargan sus datos desde ajax al desplegar el combo.
                //Por lo que puede que no hayan cargado a�n los registros y por eso no hay nada seleccionado.
                //En este caso miramos si tiene value
                var sValue = dropDownEntry.getDataValue();
                if (sValue == null || sValue == "") {
                    dropDown.set_currentValue('', true);
                    dropDown.set_selectedItemIndex(-1);
                }
                sValue = null;
            }
            break;
        default:
            if (dropDown) {
                if (((dropDownEntry.tipoGS == 0) && (dropDownEntry.tipo == 5 || dropDownEntry.tipo == 6)) || (dropDownEntry.idCampoPadreListaEnlazada != 0)) { //Si es un dropdown hijo de listas hacemos lo mismo que con Prov, Dpto, Centro y Almac�n
                    var sValue = dropDownEntry.getDataValue();
                    if (sValue == null || sValue == "") {
                        dropDown.set_currentValue('', true);
                        dropDown.set_selectedItemIndex(-1);
                    }
                    sValue = null;
                } else {
                    //Si no hay elemento seleccionado, se quita el texto que haya en el cuado de texto del webdropdown para no confundir
                    var item = dropDown.get_items().getItem(dropDown.get_selectedItemIndex());
                    if (item == null) {
                        dropDown.set_currentValue('', true);
                        dropDown.set_selectedItemIndex(-1);
                    }
                    item = null;
                }
            }
            break;
    }
    dropDownEntry = null;
    dropDown = null;
}
// Revisado por: blp. Fecha: 10/10/2011
//''' <summary>
//Gets the new collection of selected drop-down items
//Only one item will be in the collection when single selection is enabled      
//''' </summary>
//''' <param name="sender">Origen del evento.</param>
//''' <param name="e">los datos del evento.</param>    
//''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
function WebDropDown_SelectionChanging(sender, e) {
    //Gets the new collection of selected drop-down items
    //Only one item will be in the collection when single selection is enabled          
    var newSelection = e.getNewSelection();

    if (newSelection[0] != null) {
        var text = newSelection[0].get_text();
        var index = newSelection[0].get_value();

        var oldSelection = e.getOldSelection();
        var indexOld = '';
        //Gets the index of the first drop-down item in the collection
        if (oldSelection[0]) {
            indexOld = oldSelection[0].get_value();
        }

        var oEdit = fsGeneralEntry_getById(sender._id)
        if (oEdit) {
            if (oEdit.tipoGS == 132) {//Tipo Pedido
                index = newSelection[0].get_element().childNodes.item(4).innerHTML
                if (oldSelection[0]) {
                    indexOld = oldSelection[0].get_element().childNodes.item(4).innerHTML
                }
            }

            oEdit.setDataValue(index);
            oEdit.setValue(text)

            var bCambiado = (index != indexOld)

            oEdit.oldDataValue = indexOld;

            switch (oEdit.tipoGS) {
                case 107: //Pa�s
                    if (oEdit.dependentfield != null) {
                        var ugtxtProvinciaID = oEdit.dependentfield;
                        //Vaciamos de contenido el control dependiente
                        vaciarDropDown(ugtxtProvinciaID);
                    }
                    break;
                case 109:
                    //Destino
                    if (oEdit.getValue() != null) document.getElementById("igtxt" + oEdit.Editor2.ID).style.visibility = "visible"
                    else document.getElementById("igtxt" + oEdit.Editor2.ID).style.visibility = "hidden"
                    break;
                case 122: //departamento
                    //Pongo el estilo normal del departamento pq ha cambiado y no esta de baja
                    //MALLLLLLLLLLLdocument.getElementById(oEdit.id + "__t_t").className = "TipoTextoMedio"
                    break;
                case 123: //Org Compras
                    try {
                        eval("sCodOrgComprasFORM" + oEdit.idCampo + "=" + "'" + oEdit.Hidden.value + "'");
                    } catch (err) { }

                    //Borrar el Proveedor, Articulo y la denominacion y El CENTRO
                    if (bCambiado) {
                        //Art�culo
                        if (oEdit.dependentfield != null) {
                            var oArticuloID = oEdit.dependentfield;
                            //Vaciamos de contenido el control dependiente
                            vaciarDropDown(oArticuloID);
                            oArticuloID = null;
                        }

                        //Borrar Proveedores
                        borrarProveedores(oEdit)
                        //Borrar Proveedores ERP
                        BorrarProveedoresERP(oEdit)
                        if (oEdit.idDataEntryDependent != null) {
                            var aOrgCompras = oEdit.id.split("_")
                            var aArt = oEdit.idDataEntryDependent.split("_")
                            if (aOrgCompras.length < aArt.length) {
                                BorrarArticulosCentrosEnDesglose(oEdit.idDataEntryDependent, oEdit.idDataEntryDependent2);
                            } else {
                                var oEntry = fsGeneralEntry_getById(oEdit.idDataEntryDependent)
                                if (oEntry != null) {
                                    oEntry.setValue("");
                                    oEntry.setDataValue("");
                                    if (oEntry.tipoGS == 119) {
                                        oEntry.OrgComprasDependent.value = index

                                        //Borrar la Denominacion
                                        var idDataEntryDen = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna + 1, oEntry.nLinea, 1)
                                        if (idDataEntryDen) {
                                            var oEntryDen = fsGeneralEntry_getById(idDataEntryDen)
                                            if ((oEntryDen) && (oEntryDen.tipoGS == 118)) {
                                                oEntryDen.setValue("");
                                                oEntryDen.setDataValue("");

                                                oEntryDen.OrgComprasDependent.value = index
                                            }
                                        }
                                    } else
                                        if (oEntry.tipoGS == 118) {
                                            oEntry.codigoArticulo = "";
                                            oEntry.OrgComprasDependent.value = index

                                            var idDataEntryCod = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 1, oEntry.nLinea, 1)
                                            if (idDataEntryCod) {
                                                var oEntryCod = fsGeneralEntry_getById(idDataEntryCod)
                                                if ((oEntryCod) && (oEntryCod.tipoGS == 119)) {
                                                    oEntryCod.setValue("");
                                                    oEntryCod.setDataValue("");

                                                    oEntryCod.OrgComprasDependent.value = index
                                                }
                                            }
                                        }
                                }
                                //si es webdropdown, vacias de contenido el control en pantalla
                                vaciarDropDown(oEdit.idDataEntryDependent);
                            }
                            aOrgCompras = null;
                        }
                        //CENTRO
                        if (oEdit.idDataEntryDependent2 != null) {
                            //if (oEdit.id.split("_").length != oEdit.idDataEntryDependent2.split("_").length) {
                            //Por el LoadItems parece q no hace ni caso window.open("../_common/CentroUnicoServer.aspx?IdOrgCompras=" + oGrid.oEdit.fsEntry.id + "&CodOrgCompras=" + oGrid.oEdit.fsEntry.getDataValue(), "iframeWSServer", "width=450,height=375,status=yes,resizable=no,top=200,left=200")
                            //}
                            var ugtxtCentroID = oEdit.idDataEntryDependent2;
                            //Vaciamos de contenido el control dependiente
                            vaciarDropDown(ugtxtCentroID);
                            ugtxtCentroID = null;

                            //Vaciar el control dependiente del control dependiente
                            var oCentro = fsGeneralEntry_getById(oEdit.idDataEntryDependent2)
                            if (oCentro) {
                                //Almac�n (depende de centro)
                                if (oCentro.idDataEntryDependent3 != null) {
                                    //Vaciamos de contenido el control dependiente
                                    vaciarDropDown(oCentro.idDataEntryDependent3);
                                }
                                oCentro = null;
                            }
                        }
                    }
                    break;
                case 124: //Centro
                    if (bCambiado) {
                        //Si ha cambiado el Centro borrar el ARTICULO relacionado
                        if (oEdit.idDataEntryDependent != null) {
                            oCentro = oEdit.id.split("_")
                            aArt = oEdit.idDataEntryDependent.split("_")
                            if (oCentro.length < aArt.length) {
                                BorrarArticulosCentrosEnDesglose(oEdit.idDataEntryDependent, null);
                            }
                            else {
                                oEntry = fsGeneralEntry_getById(oEdit.idDataEntryDependent)
                                if (oEntry != null) {
                                    var sCodOrgCompras = oEdit.Dependent.value;
                                    var sCodCentro = oEdit.Hidden.value;

                                    if (oEntry.tipoGS == 119) {
                                        idDataEntryDen = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna + 1, oEntry.nLinea, 1)
                                        oEntryDen = fsGeneralEntry_getById(idDataEntryDen)
                                        oEntry.CentroDependent.value = index
                                        if (oEntryDen != null) {
                                            oEntryDen.CentroDependent.value = index

                                            if ((oEntryDen.codigoArticulo != null) && (oEntryDen.codigoArticulo != "")) {
                                                //Llamamos de nuevo al buscador para que nos diga si para el nuevo Centro existe o no el articulo que tenemos
                                                //en caso negativo, borra el articulo y la denominaci�n
                                                var newWindow = window.open("../_common/BuscadorArticuloserver.aspx?&idArt=" + oEntryDen.codigoArticulo + "&fsEntryArt=" + oEntry.id + "&fsEntryDen=" + idDataEntryDen + "&CodOrgCompras=" + sCodOrgCompras + "&CodCentro=" + sCodCentro + "&desde=CENTRO", "iframeWSServer")
                                                newWindow.focus()
                                            }
                                        }
                                    }
                                    else {
                                        if (oEntry.tipoGS == 118) {
                                            idDataEntryCod = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 1, oEntry.nLinea, 1)
                                            oEntry.CentroDependent.value = index
                                            oEntryCod = fsGeneralEntry_getById(idDataEntryCod)
                                            if (oEntryCod)
                                                oEntryCod.CentroDependent.value = index
                                            if ((oEntry.codigoArticulo != null) && (oEntry.codigoArticulo != "")) {
                                                //Llamamos de nuevo al buscador para que nos diga si para el nuevo Centro existe o no el articulo que tenemos
                                                //en caso negativo, borra el articulo y la denominaci�n
                                                var newWindow = window.open("../_common/BuscadorArticuloserver.aspx?&idArt=" + oEntry.codigoArticulo + "&fsEntryArt=" + idDataEntryCod + "&fsEntryDen=" + oEntry.id + "&CodOrgCompras=" + sCodOrgCompras + "&CodCentro=" + sCodCentro + "&desde=CENTRO", "iframeWSServer")
                                                newWindow.focus()
                                            }
                                        }
                                    }
                                    sCodOrgCompras = null;
                                    sCodCentro = null;
                                }
                            }
                            oCentro = null;
                        }

                        if (oEdit.idDataEntryDependent3 != null) {
                            var ugtxtAlmacenID = oEdit.idDataEntryDependent3;
                            //Vaciamos de contenido el control dependiente
                            vaciarDropDown(ugtxtAlmacenID);
                            ugtxtAlmacenID = null;
                        }
                    }
                    break;
                case 125: //Almacen
                    break;
                case 132: //Tipo Pedido
                    var Datos = newSelection[0].get_element().childNodes.item(3).innerHTML

                    Datos = Datos.substr(Datos.length - 3)

                    var Concepto = Datos.substr(0, 1)
                    var Recepcion = Datos.substr(1, 1)
                    var Almacenamiento = Datos.substr(2)

                    Datos = null;

                    oEdit.Concepto = Concepto
                    oEdit.Almacenamiento = Almacenamiento
                    oEdit.Recepcion = Recepcion

                    var cadena = '\n';
                    for (var arrInput in arrInputs) {
                        var o = fsGeneralEntry_getById(arrInputs[arrInput])
                        if (o) {
                            if (o.tipoGS == 119) {
                                if (o.getDataValue() != '') {
                                    if (((Concepto == '0') && (o.Concepto == '1')) || ((Concepto == '1') && (o.Concepto == '0')) || ((Almacenamiento == '0') && (o.Almacenamiento == '1')) || ((Almacenamiento == '1') && (o.Almacenamiento == '0')) || ((Almacenamiento == '1') && (o.Almacenamiento == '0')) || ((Recepcion == '0') && (o.Recepcion == '1')) || ((Recepcion == '1') && (o.Recepcion == '0'))) {
                                        var sDenArticulo = '';
                                        if (o.idCampoPadre) { //est� en un desglose
                                            var oDen = null;
                                            var idEntryDen = calcularIdDataEntryCelda(o.id, o.nColumna + 1, o.nLinea, 1)
                                            if (idEntryDen)
                                                oDen = fsGeneralEntry_getById(idEntryDen)
                                            if (oDen) {
                                                sDenArticulo = ' - ' + oDen.getDataValue();
                                                oDen.setValue('');
                                                oDen.setDataValue('');
                                                oDen.codigoArticulo = '';
                                            }
                                            idEntryDen = null;
                                            oDen = null;
                                        }

                                        if (cadena.search('\n' + o.getDataValue() + sDenArticulo + '\n') == -1)
                                            cadena = cadena + o.getDataValue() + sDenArticulo + '\n'
                                        o.setValue('');
                                        o.setDataValue('');
                                    }
                                }
                            }
                        }
                        o = null;
                        sDenArticulo = null;
                    }
                    Concepto = null;
                    Recepcion = null;
                    Almacenamiento = null;
                    arrInput = null;

                    if (cadena != '\n')
                        alert(mensajeCambioTipoPedido + cadena)
                    cadena = null;
                    break;
            }
            oEntry = null;
            idDataEntryDen = null;
            oEntryDen = null;
            idDataEntryCod = null;
            oEntryCod = null;
            bCambiado = null;
        }

        text = null;
        index = null;
        oEdit = null;
        oldSelection = null;
        indexOld = null;
    } else {
        //Vaciamos de contenido el control
        vaciarDropDown(sender._id);
    }
    newSelection = null;
}
function BorrarProveedoresERP(Entry) {
    f = document.forms["frmDetalle"]
    if (!f)
        f = document.forms["frmAlta"]
    for (arrInput in arrInputs) {
        var oAux = fsGeneralEntry_getById(arrInputs[arrInput])
        if (oAux) {
            if (oAux.tipo == 9 && oAux.idDataEntryDependent == Entry.id)
                //desglose despu�s de un campo de OrgCompras
            {
                var i
                i = 0
                for (k = 1; k <= f.elements[oAux.id + "__numTotRows"].value; k++) {
                    for (arrCampoHijo in oAux.arrCamposHijos) {
                        oHijo = f.elements[oAux.id + "__" + k.toString() + "__" + oAux.arrCamposHijos[arrCampoHijo]]
                        if (oHijo) {
                            //input de desglose formato: IdCampoPadre_IdCampoHijo_Linea
                            oHijoSrc = f.elements[oAux.id + "__" + oAux.arrCamposHijos[arrCampoHijo]]
                            if (oHijoSrc.tipoGS == 143 && (oHijoSrc.idDataEntryDependent == Entry.id || oHijoSrc.idEntryPROV == Entry.id)) {
                                oHijo.value = ""
                            } else {
                                if (oHijoSrc.tipoGS == 143 && oHijoSrc.idDataEntryDependent == undefined) {
                                    if (sDataEntryOrgComprasFORM == Entry.id)
                                        oHijo.value = ""
                                }
                            }
                        }
                    }
                }
            }
            else {
                if ((oAux.idDataEntryDependent == Entry.id || oAux.idEntryPROV == Entry.id) && (oAux.tipoGS == 143)) {
                    oAux.setValue("")
                    oAux.setDataValue("")
                }
            }
        }
    }
}
/*  
''' M�todo para borrar el contenido del dataenttry proveedores cuando ha cambiado la organizaci�n de compras.
''' Entry: id del dataentry de la organizaci�n de compras
''' Llamada desde: jsalta.js --> ddshowDropDownCODmasDEN y WebDropDown_SelectionChanging
''' Tiempo m�x < 0,1 seg.
*/
function borrarProveedores(Entry) {
    f = document.forms["frmDetalle"]
    if (!f)
        f = document.forms["frmAlta"]
    for (arrInput in arrInputs) {
        var oAux = fsGeneralEntry_getById(arrInputs[arrInput])
        if (oAux) {
            if (oAux.tipo == 9 && oAux.idDataEntryDependent == Entry.id)
                //desglose despu�s de un campo de OrgCompras
            {
                var i
                i = 0
                for (k = 1; k <= f.elements[oAux.id + "__numTotRows"].value; k++) {
                    for (arrCampoHijo in oAux.arrCamposHijos) {
                        oHijo = f.elements[oAux.id + "__" + k.toString() + "__" + oAux.arrCamposHijos[arrCampoHijo]]
                        if (oHijo) {
                            //input de desglose formato: IdCampoPadre_IdCampoHijo_Linea
                            oHijoSrc = f.elements[oAux.id + "__" + oAux.arrCamposHijos[arrCampoHijo]]
                            if (oHijoSrc.tipoGS == 100) {
                                oHijo.value = ""
                            }
                        }
                    }
                }
            }
            else {
                if ((oAux.idDataEntryDependent == Entry.id) && (oAux.tipoGS == 100)) {
                    oAux.setValue("")
                    oAux.setDataValue("")
                }
            }
        }
    }
}
// The client event �InputKeyDown� takes two parameters sender and e
// sender  is the object which is raising the event
// e is the DropDownControlEventArgs
function WebDropDown_InputKeyDown(sender, eventArgs) {
    oCombo = $find(sender._id)
    oEdit = fsGeneralEntry_getById(sender._id)
    sComboPadre = ''
    if (oCombo) {
        //Obtener el combo padre para que si al no tener ningun elemento seleccionado no puedas escribir (LAS LISTAS ENLAZADAS SE ENCUENTRAN DE MOMENTO EN EL MISMO GRUPO)
        if ((oEdit.idCampoPadreListaEnlazada) && (oEdit.idCampoPadreListaEnlazada > 0)) {
            arrAux = sender._id.split("_")

            if (arrAux[1] == '')
                sComboPadre = arrAux[0] + '__' + arrAux[2] + '_' + arrAux[3] + "_" + 'fsentry' + oEdit.Grupo + '__t';   //uwtGrupos_ctl01_578_fsentry28277__t"
            else
                sComboPadre = arrAux[0] + '_' + arrAux[1] + '_' + arrAux[2] + "_" + 'fsentry' + oEdit.Grupo + '__t';   //uwtGrupos_ctl01_578_fsentry28277__t"

        }
        if (sComboPadre != '') {
            comboPadre = $find(sComboPadre)
            if ((comboPadre) && (comboPadre.get_currentValue() == ''))
                eventArgs.set_cancel(true);
        }
    }
}
function WebDropDown_DropDownOpening(sender, e) {
    var dropDown = $find(sender._id);
    dropDown._elements.DropDown.style.width = (dropDown._elements.Input.clientWidth + dropDown._elements.Button.clientWidth).toString() + "px"
    dropDown._elements.DropDownContents.style.Width = (dropDown._elements.Input.clientWidth + dropDown._elements.Button.clientWidth).toString() + "px"
}
function Solicitud_Padre_seleccionado(idDataEntry, sId, sDen) {
    //Obtener el DataEntry de Ref.Solicitud
    oFSEntry = fsGeneralEntry_getById(idDataEntry);
    if (oFSEntry) {
        oFSEntry.setDataValue(sId);
        if (sDen != null)
            sValor = sId + " - " + sDen;
        else
            sValor = sId;
        oFSEntry.setValue(sValor)

    }

}
var ventexterna = false;
function show_externa(oEdit, oldValue, oEvent) {
    ventexterna = true;
    var artcod = "";
    var sIndex = "-1";
    if (oEdit.fsEntry.idCampoPadre) {
        var o = oEdit.fsEntry;
        var desglose = o.id.substr(0, o.id.search("_fsdsentry"));
        sIndex = o.id.substring((desglose + "_fsdsentry_").length, o.id.search("_" + o.idCampo))
    }
    if (oEdit.fsEntry.dependentfield) {
        if (sIndex == "-1")
            artcod = fsGeneralEntry_getById(oEdit.fsEntry.dependentfield).getDataValue()
        else
            artcod = fsGeneralEntry_getById(oEdit.fsEntry.dependentfield.replace("fsentry", "fsdsentry_" + sIndex + "_")).getDataValue()
    }
    var vent = window.open("../_common/tablaexterna.aspx?tabla=" + oEdit.fsEntry.TablaExterna + "&valor=" + oldValue + "&idDataEntry=" + oEdit.fsEntry.id + "&art=" + artcod, "wtablaexterna", "width=700,height=450,status=yes,resizable=no,top=200,left=200");
    vent.focus();
}
function show_externa_ReadOnly(oEdit, oldValue, oEvent) {
    var newWindow = window.open("../_common/tablaexternareg.aspx?tabla=" + oEdit.fsEntry.TablaExterna + "&valor=" + oEdit.fsEntry.dataValue, "wtablaexterna", "width=700,height=275,status=yes,resizable=no,top=200,left=200");
    newWindow.focus()
}
function valor_original_externa(idDataEntry) {
    var oFSEntry = fsGeneralEntry_getById(idDataEntry);
    if (oFSEntry.tipo == 2 && oFSEntry.dataValue == null || oFSEntry.tipo == 3 && oFSEntry.dataValue == new Date('1/1/1900') || oFSEntry.getValue() == '') {
        oFSEntry.setValue('');
        oFSEntry.setDataValue('');
        oFSEntry.codigoArticulo = null;
    }
    else {
        var valorant = oFSEntry.dataValue;
        oFSEntry.setValue(oFSEntry.Hidden.value);
        oFSEntry.setDataValue(valorant);
    }
}
function valor_externa_seleccionado(idDataEntry, valor, texto, art) {
    if (document.getElementById("bMensajePorMostrar")) {
        if (document.getElementById("bMensajePorMostrar").value == "1") {
            document.getElementById("bMensajePorMostrar").value = "0";
        }
    }

    var oFSEntry = fsGeneralEntry_getById(idDataEntry);
    if (oFSEntry) {
        oFSEntry.setValue(texto);
        oFSEntry.setDataValue(valor);
        oFSEntry.codigoArticulo = ((art == '') ? null : art);
    }
}
function validar_externa(oEdit, oldValue, oEvent) {
    if (oEdit.fsEntry.getValue() != '' && !ventexterna) {
        var bvalidar = false;
        if (oEdit.fsEntry.tipo == 2 && oEdit.fsEntry.dataValue == null || oEdit.fsEntry.tipo == 3 && oEdit.fsEntry.dataValue == new Date('1/1/1900')) {
            if (oEdit.fsEntry.getValue() != '') bvalidar = true;
        }
        else {
            if (oEdit.fsEntry.getValue() != oEdit.fsEntry.Hidden.value) bvalidar = true;
        }
        if (bvalidar) {
            var artcod = "";
            var sIndex = "-1";
            if (oEdit.fsEntry.idCampoPadre) {
                var o = oEdit.fsEntry;
                var desglose = o.id.substr(0, o.id.search("_fsdsentry"));
                sIndex = o.id.substring((desglose + "_fsdsentry_").length, o.id.search("_" + o.idCampo))
            }
            if (oEdit.fsEntry.dependentfield) {
                if (sIndex == "-1")
                    artcod = fsGeneralEntry_getById(oEdit.fsEntry.dependentfield).getDataValue()
                else
                    artcod = fsGeneralEntry_getById(oEdit.fsEntry.dependentfield.replace("fsentry", "fsdsentry_" + sIndex + "_")).getDataValue()
            }
            if (document.getElementById("bMensajePorMostrar")) {
                document.getElementById("bMensajePorMostrar").value = "1"
            }
            var newWindow = window.open("../_common/tablaexternaserver.aspx?tabla=" + oEdit.fsEntry.TablaExterna + "&valor=" + oEdit.text + "&fsEntryID=" + oEdit.fsEntry.id + "&art=" + artcod, "iframeWSServer");
            newWindow.focus()
        }
    }
}
function errorval_externa(idDataEntry, mensaje) {
    alert(mensaje);

    if (document.getElementById("bMensajePorMostrar")) {
        if (document.getElementById("bMensajePorMostrar").value == "1") {
            document.getElementById("bMensajePorMostrar").value = "0";
        }
    }

    var oFSEntry = fsGeneralEntry_getById(idDataEntry);
    if (oFSEntry) {
        var valorant = oFSEntry.dataValue;
        oFSEntry.setValue(oFSEntry.Hidden.value);
        oFSEntry.setDataValue(valorant);
    }
}
/*Finalmente lo de cambiar el estado de rechazada a sin revisar tras un cambio en un campo no es en un campo 
cualquiera, es en los predefinidos de la acci�n, entre estos no esta el material/articulo/den. articulo*/
function VaciarMat_NuevoCodArt_Den(oEdit, text, oEvent) {
    if (oEvent.event.keyCode == 8 || oEvent.event.keyCode == 46 || oEvent.event.keyCode == 32) {
        //Poner el NuevoCodArt en blanco
        var idEntryCod = calcularIdDataEntryCelda(oEdit.fsEntry.id, oEdit.fsEntry.nColumna + 1, oEdit.fsEntry.nLinea, 1)
        if (idEntryCod)
            var oEntry = fsGeneralEntry_getById(idEntryCod)
        if ((oEntry != null) && (oEntry.tipoGS == 119) && !(oEntry.anyadir_art)) {
            oEntry.setValue("")
            oEntry.title = ""

            //if (oEntry.CtrlCambioRechazo == true)
            //{
            //	fCambioNc (oEdit,"",null)
            //}		

            //Poner la Denominacion en Blanco
            oEntry = null;
            var idEntryDen = calcularIdDataEntryCelda(oEdit.fsEntry.id, oEdit.fsEntry.nColumna + 2, oEdit.fsEntry.nLinea, 1)
            if (idEntryDen)
                oEntry = fsGeneralEntry_getById(idEntryDen)
            if ((oEntry != null) && (oEntry.tipoGS == 118)) {
                oEntry.setValue("")
                oEntry.title = ""
            }
        }

        //Mirar si tiene alg�n campo de lista como hijo y ponerlo en blanco 
        VaciarCampoListaCampoMaterial(oEdit.fsEntry);

        fsVaciaCombo(oEdit, text, oEvent)
    }
}
function VaciarCampoListaCampoMaterial(oEntry) {   
    var idEntryLista;
    var oLista;
    var p = window.opener;
    var arrAux = oEntry.id.split("_");
    if (arrAux[1] == '') idEntryLista = arrAux[0] + '__' + arrAux[2] + '_' + arrAux[3] + "_" + 'fsentry' + oEntry.idCampoHijaListaEnlazada + '__t'; //uwtGrupos_ctl01_578_fsentry28277__t";
    else idEntryLista = arrAux[0] + '_' + arrAux[1] + '_' + arrAux[2] + "_" + 'fsentry' + oEntry.idCampoHijaListaEnlazada + '__t'; //uwtGrupos_ctl01_578_fsentry28277__t";
    if (idEntryLista) oLista = fsGeneralEntry_getById(idEntryLista);
    //if (!oLista && p) { //el padre puede que est� en p, el window.opener
    //    oLista = p.fsGeneralEntry_getById(sIdPadre)
    //}
    if (!oLista) {
        //el padre puede que sea un campo de tipo material que est� en otro grupo
        for (arrInput in arrInputs) {
            var o = fsGeneralEntry_getById(arrInputs[arrInput])
            if (o) {
                if ((o.intro == 1) && (o.tipoGS == 0) && o.id.endsWith('fsentry' + oEntry.idCampoHijaListaEnlazada)) {
                    oLista = fsGeneralEntry_getById(arrInputs[arrInput] + '__t');
                }
            }
        }
    }
    if (oLista) {
        oLista.setValue("");
        oLista.title = "";
    }
}
//function fsVaciaComboOrgCompras (oEdit, text, oEvent)
//{
//if (oEvent.event.keyCode==8 ||oEvent.event.keyCode==46 || oEvent.event.keyCode==32)
//	{
//	
//	oEdit.setValue(null)
//	oEdit.fsEntry.setValue(null)
//	oEdit.fsEntry.setDataValue("")
//	if (oEdit.fsEntry.tipoGS == 123) { //Organizacion de Compras
//		try {
//			eval("sCodOrgComprasFORM" + oEdit.fsEntry.idCampo + "=" + "'" + "'");			
//		}catch (err) {}

//	}
//	if (oEdit.fsEntry.dependentfield)
//	{
//			oDependent =  fsGeneralEntry_getById(oEdit.fsEntry.dependentfield)
//			oDependent.setValue("")
//			oDependent.setDataValue("")
//	}
//	if (oEdit.fsEntry.idDataEntryDependent) {
//		
//		aOrgCompras = oEdit.fsEntry.id.split("_")
//		aArt = oEdit.fsEntry.idDataEntryDependent.split("_")
//		if (aOrgCompras.length < aArt.length) {
//			BorrarArticulosCentrosEnDesglose(oEdit.fsEntry.idDataEntryDependent,oEdit.fsEntry.idDataEntryDependent2);								
//		
//		}else {
//	
//	
//			var oEntry = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent)					
//			if (oEntry != null) {
//				oEntry.setValue("");
//				oEntry.setDataValue("");
//				if (oEntry.tipoGS == 119) {
//					//Borrar la Denominacion
//					var idDataEntryDen = calcularIdDataEntryCelda(oEntry.id,oEntry.nColumna + 1,oEntry.nLinea,1)
//					if  (idDataEntryDen) {
//						var oEntryDen = fsGeneralEntry_getById(idDataEntryDen)	
//						if ((oEntryDen) && (oEntryDen.tipoGS == 118)) {
//							oEntryDen.setValue("");
//							oEntryDen.setDataValue("");
//						}			
//					}			
//				}else 
//					if (oEntry.tipoGS == 118) {
//						oEntry.codigoArticulo = "";
//						var idDataEntryCod = calcularIdDataEntryCelda(oEntry.id,oEntry.nColumna -1,oEntry.nLinea,1)
//						if  (idDataEntryCod) {
//							var oEntryCod = fsGeneralEntry_getById(idDataEntryCod)	
//							if ((oEntryCod) && (oEntryCod.tipoGS == 119)) {
//								oEntryCod.setValue("");
//								oEntryCod.setDataValue("");
//							}
//						}
//					}
//					
//			}
//		}
//	}
//	}
//ddcloseDropDownEvent(oEdit, text, oEvent)
//if (!(oEvent.event.keyCode==16 || (oEvent.event.keyCode>=35 && oEvent.event.keyCode<=40) || oEvent.event.keyCode ==9))
//	oEvent.cancel = true
//}
/*  Revisado por: blp. Fecha: 10/10/2011
''' M�todo que vac�a los combos de art�culos y centro de cada l�nea de desglose cuando se le pasan los Id de los respectivos controles en la tabla oculta
''' idDataEntryInicialArticulo: Id del control de art�culo
''' idDataEntryInicialCentro: Id del control de centro
''' Llamada desde jsAlta.js
''' Tiempo m�x < 0,1 seg.*/
function BorrarArticulosCentrosEnDesglose(idDataEntryInicialArticulo, idDataEntryInicialCentro) {

    if (idDataEntryInicialArticulo)
        var oEntryInicialArt = fsGeneralEntry_getById(idDataEntryInicialArticulo)
    if (idDataEntryInicialCentro)
        var oEntryInicialCen = fsGeneralEntry_getById(idDataEntryInicialCentro)

    arrAux = idDataEntryInicialArticulo.split("_")
    tabla = arrAux[0] + "__" + arrAux[2] + "_" + arrAux[3] + "_" + arrAux[3] + "_" + arrAux[5] + "_tblDesglose"
    filas = (document.getElementById(tabla).rows.length) - 1
    for (nLinea = 1 ; nLinea <= filas; nLinea++) {
        var idDataEntry = calcularIdDataEntryCelda(idDataEntryInicialArticulo, oEntryInicialArt.nColumna, nLinea, 1)
        if (oEntryInicialCen)
            var idDataEntryCentro = calcularIdDataEntryCelda(idDataEntryInicialCentro, oEntryInicialCen.nColumna, nLinea, 1)
        if (idDataEntry) { //Borrar Articulo
            var oEntry = fsGeneralEntry_getById(idDataEntry)
            if (oEntry != null) {
                oEntry.setValue("");
                oEntry.setDataValue("");
                oEntry.CentroDependent.value = ""
                if (oEntry.tipoGS == 119) {
                    //Borrar la Denominacion
                    var idDataEntryDen = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna + 1, oEntry.nLinea, 1)
                    if (idDataEntryDen) {
                        var oEntryDen = fsGeneralEntry_getById(idDataEntryDen)
                        if ((oEntryDen) && (oEntryDen.tipoGS == 118)) {
                            oEntryDen.setValue("");
                            oEntryDen.setDataValue("");
                            oEntryDen.CentroDependent.value = ""
                        }
                    }
                } else
                    if (oEntry.tipoGS == 118) {
                        oEntry.codigoArticulo = "";
                        var idDataEntryCod = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 1, oEntry.nLinea, 1)
                        if (idDataEntryCod) {
                            var oEntryCod = fsGeneralEntry_getById(idDataEntryCod)
                            if ((oEntryCod) && (oEntryCod.tipoGS == 119)) {
                                oEntryCod.setValue("");
                                oEntryCod.setDataValue("");
                                oEntryCod.CentroDependent.value = "";
                            }
                        }
                    }
            }
            if (idDataEntryCentro) {
                //Vaciamos de contenido el control
                vaciarDropDown(idDataEntryCentro);
            }

        }

    }//fin for
}//fin function BorrarArticulosCentrosEnDesglose 
function fsVaciaComboCentros(oEdit, text, oEvent) {
    if (oEvent.event.keyCode == 8 || oEvent.event.keyCode == 46 || oEvent.event.keyCode == 32) {
        oEdit.setValue(null)
        oEdit.fsEntry.setValue(null)
        oEdit.fsEntry.setDataValue("")
        if (oEdit.fsEntry.idDataEntryDependent) {
            aCentro = oEdit.fsEntry.id.split("_")
            aArt = oEdit.fsEntry.idDataEntryDependent.split("_")
            if (aCentro.length < aArt.length) {
                BorrarArticulosCentrosEnDesglose(oEdit.fsEntry.idDataEntryDependent, null);
            } else {

                var oEntry = fsGeneralEntry_getById(oEdit.fsEntry.idDataEntryDependent)
                if (oEntry != null) {
                    oEntry.setValue("");
                    oEntry.setDataValue("");
                    if (oEntry.tipoGS == 119) {
                        //Borrar la Denominacion
                        var idDataEntryDen = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna + 1, oEntry.nLinea, 1)
                        if (idDataEntryDen) {
                            var oEntryDen = fsGeneralEntry_getById(idDataEntryDen)
                            if ((oEntryDen) && (oEntryDen.tipoGS == 118)) {
                                oEntryDen.setValue("");
                                oEntryDen.setDataValue("");
                            }
                        }
                    } else
                        if (oEntry.tipoGS == 118) {
                            oEntry.codigoArticulo = "";
                            var idDataEntryCod = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna - 1, oEntry.nLinea, 1)
                            if (idDataEntryCod) {
                                var oEntryCod = fsGeneralEntry_getById(idDataEntryCod)
                                if ((oEntryCod) && (oEntryCod.tipoGS == 119)) {
                                    oEntryCod.setValue("");
                                    oEntryCod.setDataValue("");
                                }
                            }
                        }
                }
            }
        }
    }
    ddcloseDropDownEvent(oEdit, text, oEvent)
    if (!(oEvent.event.keyCode == 16 || (oEvent.event.keyCode >= 35 && oEvent.event.keyCode <= 40) || oEvent.event.keyCode == 9))
        oEvent.cancel = true
}
/*  FUNCION PROBABLEMENTE NO USADA. REVISAR
    Revisado por: blp. Fecha: 10/10/2011
''' M�todo para establecer el valor del centro y vaciar el campo dependiente almac�n si lo hay
''' sIdOrgCompras: Id de la organizacion de compras de la que depende el centro
''' sValor: Cod del centro a seleccioner
''' sTexto: Texto a mostrar en el combo
''' Llamada desde CentroUnicoServer.aspx.vb.
''' Tiempo m�x < 0,1 seg.*/
function mostrarCentroUnico(sIdOrgCompras, sValor, sTexto) {

    oEntry = fsGeneralEntry_getById(sIdOrgCompras)

    idDataEntryInicialCentro = oEntry.idDataEntryDependent2
    if (idDataEntryInicialCentro) {
        var oEntryInicialCen = fsGeneralEntry_getById(idDataEntryInicialCentro)
    }
    else {
        idDataEntryInicialCentro = calcularIdDataEntryCelda(oEntry.id, oEntry.nColumna + 1, oEntry.nLinea, 1)
        if (idDataEntryInicialCentro) {
            var oEntryInicialCen = fsGeneralEntry_getById(idDataEntryInicialCentro)
        }
    }

    if (oEntryInicialCen) {
        var aOrgCompras = oEntry.id.split("_")
        var aCentro = oEntryInicialCen.id.split("_")

        if (aOrgCompras.length != aCentro.length) {
            arrAux = idDataEntryInicialCentro.split("_")
            tabla = arrAux[0] + "__" + arrAux[2] + "_" + arrAux[3] + "_" + arrAux[3] + "_" + arrAux[5] + "_tblDesglose"
            filas = (document.getElementById(tabla).rows.length) - 1
            for (nLinea = 1 ; nLinea <= filas; nLinea++) {

                var idDataEntryCentro = calcularIdDataEntryCelda(idDataEntryInicialCentro, oEntryInicialCen.nColumna, nLinea, 1)

                if (idDataEntryCentro) {
                    var oEntryCentro = fsGeneralEntry_getById(idDataEntryCentro)
                    if ((oEntryCentro != null) && (oEntryCentro.tipoGS == 124)) {
                        oEntryCentro.setValue(sTexto);
                        oEntryCentro.setDataValue(sValor);

                        eval("sCodOrgComprasFORM" + oEntry.idCampo + "='" + oEntry.getDataValue() + "';");

                        //ALMAC�N
                        if (oEntryCentro.idDataEntryDependent3 != null) {
                            //Vaciamos de contenido el control dependiente
                            vaciarDropDown(oEntryCentro.idDataEntryDependent3);
                        }
                    }
                }

            }//fin for
        }
        else {
            oEntryInicialCen.setValue(sTexto);
            oEntryInicialCen.setDataValue(sValor);

            //ALMAC�N
            if (oEntryInicialCen.idDataEntryDependent3 != null) {
                //Vaciamos de contenido el control dependiente
                vaciarDropDown(oEntryInicialCen.idDataEntryDependent3);
            }
        }
    }
}
function ControlLongitudStringLargo(oCampo, longitud) {
    if (oCampo.value.length >= longitud)
        return false
    else
        return true
}
/* <summary>
''' Valida la longitud de un dataentry de tipo texto
''' Octubre09: En el dataentry se establece el maxlength para texto corto y medio, por eso esta solo se usa para string largo.
''' </summary>
''' <param name="oCampo">control texto</param>
''' <param name="longitud">maxlength</param>        
''' <param name="msg">msg de error</param> 
''' <remarks>Llamada desde: Todos los dataentry de tipo texto medio,largo y string ; Tiempo m�ximo: instantaneo</remarks>
*/
function validarLengthStringLargo(oCampo, longitud, msg) {
    msg = msg.replace("\\n", "\n")
    msg = msg.replace("\\n", "\n")
    if (oCampo.value.length > longitud) {
        alert(msg + longitud)
        return false
    }
    else {
        return true
    }
}
/*
Valida la longitud de un dataentry de tipo texto y si no cumple la condici�n, corta el texto.
Llamadad desde: las cajas de texto de los dataentry medio y largo.
*/
function validarLengthyCortarStringLargo(oCampo, longitud, msg) {
    var retorno = validarLengthStringLargo(oCampo, longitud, msg);
    if (!retorno) {
        oCampo.value = oCampo.value.substr(0, longitud);
    }
    return retorno;
}
//INI FRAMES DESPLEGABLES
var gFRAMEid;
function mostrarFRAME(FRAMEid, x, y, width, height) {
    var fr = document.getElementById(FRAMEid)
    if (fr) {
        fr.parentNode.removeChild(fr);
    }
    document.body.insertAdjacentHTML("beforeEnd", "<iframe id='" + FRAMEid + "' frameborder='no' style='Z-INDEX: 2000; LEFT:" + x + "px; WIDTH: " + width + "px; POSITION: absolute; TOP: " + y + "px; HEIGHT: " + height + "px;'   scrolling='no'></iframe>")

    if (window.addEventListener) {
        window.document.addEventListener("mousedown", eliminarFRAME, false)
    } else {
        if (window.attachEvent) {
            window.document.attachEvent("onmousedown", eliminarFRAME)
            window.document.attachEvent("onblur", eliminarFRAME)
        }
    }

    var d = document.getElementById(FRAMEid);
    d.frameBorder = false
    if (d.addEventListener) {
        d.addEventListener("mouseover", FRAMEover, false)
        d.addEventListener("mouseout", FRAMEout, false)
    } else {
        if (d.attachEvent) {
            d.attachEvent("onmouseover", FRAMEover)
            d.attachEvent("onmouseout", FRAMEout)
        }
    }

    gFRAMEid = FRAMEid
}
function eliminarFRAME() {
    var d = document.getElementById(gFRAMEid);
    if (d) {

        d.parentNode.removeChild(d);
        if (window.document.removeEventListener)
            window.document.removeEventListener("mousedown", eliminarFRAME, false)
        else {
            if (window.document.detachEvent)
                window.document.detachEvent("onmousedown", eliminarFRAME)
        }
    }
}
function FRAMEover() {
    if (window.document.removeEventListener)
        window.document.removeEventListener("mousedown", eliminarFRAME, false)
    else {
        if (window.document.detachEvent)
            window.document.detachEvent("onmousedown", eliminarFRAME)
    }
}
function FRAMEout() {
    if (window.document.addEventListener)
        window.document.addEventListener("mousedown", eliminarFRAME, false)
    else {
        if (window.document.attachEvent)
            window.document.attachEvent("onmousedown", eliminarFRAME)
    }
}
//FIN FRAMES DESPLEGABLES
/*
''' <summary>
''' Validar fechas
''' </summary>
''' <param name="oEdit">Campo</param> 
''' <param name="oldValue">Valor que tenia el entry anteriormente</param> 
''' <param name="oEvent">Evento</param> 
''' <returns>cuando alguna de las fechas cambia</returns>
''' <remarks>Llamada desde:GeneralEntry.vb ; Tiempo m�ximo:0 </remarks>
*/
function ddFec_ValueChange(oEdit, oldValue, oEvent) {

    var a = new Array();
    var s
    if (oEdit.fsEntry.getValue() != null) {
        s = String(oEdit.fsEntry.getValue());
        var dFecha = new Date(s);
        var Anyo = dFecha.getFullYear();

        if (Anyo < 1900 || Anyo > 2100) {
            alert(sMensajeFechaDefecto)
            oEdit.fsEntry.setDataValue("");
            oEdit.fsEntry.setValue("");
            oEdit.setValue("");
            oEdit.setText("");
            oEdit.setDate("");
            oEdit.old = null;
        }
    }
}
/*
''' <summary>
''' Validar fechas
'''	''' </summary>
''' <param name="oEdit">Campo</param> 
''' <param name="oldValue">Valor que tenia el entry anteriormente</param> 
''' <param name="oEvent"></param> 
''' <returns>cuando la fecha es anterior a la fecha del sistema</returns>
''' <remarks>Llamada desde:GeneralEntry.vb ; Tiempo m�ximo:0 </remarks>
*/
function ddFechaNoAnteriorAlSistema_ValueChange(oEdit, oldValue, oEvent) {
    if (oEdit.fsEntry.getValue() != null) {
        var dFechaActual = new Date();
        var s;
        s = oEdit.fsEntry.getValue()
        var dFecha = new Date(s);
        if ((dFechaActual.getYear() > dFecha.getYear()) || ((dFechaActual.getYear() == dFecha.getYear()) && (dFechaActual.getMonth() > dFecha.getMonth())) || ((dFechaActual.getYear() == dFecha.getYear()) && (dFechaActual.getMonth() == dFecha.getMonth()) && (dFechaActual.getDate() > dFecha.getDate()))) {
            alert(sFechaNoAnteriorAlSistema)
            oEdit.fsEntry.setDataValue("");
            oEdit.fsEntry.setValue("");
            oEdit.setValue("");
            oEdit.setText("");
            oEdit.setDate("");
            oEdit.old = null;

        }

    }
};
/*
''' <summary>
''' Determinar si todos los campos texto medio y texto largo cumplen con su longitud maxima
''' </summary>      
''' <returns>Si todos los campos texto medio y texto largo cumplen con su longitud maxima</returns>
''' <remarks>Llamada desde: noconformidades.aspx	certificados,aspx; Tiempo m�ximo:0</remarks>*/
function comprobarTextoLargo() {
    for (arrInput in arrInputs) {
        oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
        if (oEntry) {
            if (oEntry.intro == 0) {
                if (oEntry.tipo == 6 || oEntry.tipo == 7) {
                    if (parseInt(oEntry.maxLength) > 0) {
                        if (oEntry.getValue().length > parseInt(oEntry.maxLength)) {
                            var msg = oEntry.errMsg;
                            msg = msg.replace("\\n", "\n");
                            msg = msg.replace("\\n", "\n");

                            alert(oEntry.title + '. ' + msg + oEntry.maxLength);
                            return false;
                        }
                    }
                }
            }
        }
    }

    return true;
}
/*''' <summary>
''' Funci�n que limita el n�mero de decimales a los propios de cada unidad en los controles webNumericEdit
''' </summary>
''' <param name="oEdit">objeto q ha podido cambiar </param>
''' <param name="newText">text del objeto</param>        
''' <param name="oEvent">Evento</param>  
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo: 0</remarks>*/
function limiteDecimalesTextChanged(oEdit, newText, oEvent) {
    var eventoPaste = false;
    var textoDecimales = newText;
    var avisoMaxDecimales = oEdit.fsEntry.errMsg
    var denUnidad = oEdit.fsEntry.UnidadOculta
    var numeroDecimales = oEdit.fsEntry.NumeroDeDecimales
    var separadorDecimales = oEdit.decimalSeparator

    if (numeroDecimales == null)
        numeroDecimales = 15
    oEdit.decimals = numeroDecimales

    if (oEvent) {
        if (oEvent.event) {
            if (oEvent.event.keyCode == 17)
                eventoPaste = true
        }
    }

    if (numeroDecimales >= 0 && separadorDecimales != null && textoDecimales != '') {
        if (textoDecimales.indexOf(separadorDecimales) > 0) {
            var textoEntero = textoDecimales.substr(0, textoDecimales.indexOf(separadorDecimales));
            textoDecimales = textoDecimales.slice(textoDecimales.indexOf(separadorDecimales) + 1)
            if (textoDecimales.length > numeroDecimales) {
                var old = oEdit.old;
                textoDecimales = textoDecimales.substr(0, numeroDecimales);
                oEdit.setText(textoEntero + separadorDecimales + textoDecimales);
                oEdit.lastText = oEdit.getText(); //Imprescindible modificar SIEMPRE el lastText para garantizar que ejecuta el evento TextChanged (si no, podr�a no modificarlo)
                oEdit.old = old;
                if (eventoPaste == true) {
                    alert(avisoMaxDecimales + ' ' + denUnidad + ':' + numeroDecimales);
                }
            }
            else {
                if (eventoPaste == true && textoDecimales.length == numeroDecimales) {
                    alert(avisoMaxDecimales + ' ' + denUnidad + ':' + numeroDecimales);
                }
            }
        }
        else {
            if (eventoPaste == true) {
                alert(avisoMaxDecimales + ' ' + denUnidad + ':' + numeroDecimales);
            }
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////
//                          AJAX
////////////////////////////////////////////////////////////////////////////////////////////////
var xmlHttp;
var mioEntryId;
/*''' <summary>
''' Funci�n que crea el objeto Ajax
''' </summary>
''' <remarks>Llamada desde: jsAlta.js; Tiempo m�ximo:0</remarks>*/
function CreateXmlHttp() {

    // Probamos con IE
    try {
        // Funcionar� para JavaScript 5.0
        xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e) {
        try {
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (oc) {
            xmlHttp = null;
        }
    }

    // Si no se trataba de un IE, probamos con esto
    if (!xmlHttp && typeof XMLHttpRequest != "undefined") {
        xmlHttp = new XMLHttpRequest();
    }

    return xmlHttp;
}
//Funci�n que envia la peticion Ajax
function envioInfo(oEntryID, Id, nom, com, datasize, comsalto, origen, IdContrato) {
    var entryId;
    entryId = oEntryID;
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");
    oEntry = fsGeneralEntry_getById(entryId)
    mioEntryId = oEntryID;
    var instancia = document.getElementById(oEntryID + '__hInstancia').value;
    var tipo = document.getElementById(oEntryID + '__hTipo').value
    var adjunto = document.getElementById(oEntryID + '__hAct').value
    var adjuntoNew = document.getElementById(oEntryID + '__hNew').value
    var nombre = document.getElementById(oEntryID + '__hNombre').value
    if (nombre == '') {
        nombre = nom;
    } else {
        nombre = nombre + '; ' + nom;
    }
    document.getElementById(oEntryID + '__hNombre').value = nombre
    var idCampo = document.getElementById(oEntryID + '__hCampo').value

    if (adjuntoNew == '') {
        adjuntoNew = Id;
        document.getElementById(oEntryID + '__hNew').value = adjuntoNew;
    } else {
        adjuntoNew = adjuntoNew + 'xx' + Id;
        document.getElementById(oEntryID + '__hNew').value = adjuntoNew;
    }
    var instanciaPort =0
    if (document.getElementById(oEntryID + '__hInstanciaPort')) {
        instanciaPort = document.getElementById(oEntryID + '__hInstanciaPort').value;
    }
    var instanciaPortF =0
    if (document.getElementById(oEntryID + '__hInstanciaPortF')) {
        instanciaPortF = document.getElementById(oEntryID + '__hInstanciaPortF').value;
    }

    // 1.- Creamos el objeto xmlHttpRequest
    CreateXmlHttp();

    // 2.- Definimos la llamada para hacer un simple GET.
    var ajaxRequest = '../_common/GestionaAjax.aspx?accion=1&EntryID=' + oEntryID + '&instancia=' + instancia + '&tipo=' + tipo + '&nombre=' + nombre + '&adjunto=' + adjunto + '&adjuntoNew=' + adjuntoNew + '&campo=' + idCampo + '&readOnly=' + oEntry.readOnly.toString() + '&origen=""&IdContrato=' + IdContrato.toString() + '&defecto=' + oEntry.DataValorDefecto + '&instanciaPort=' + instanciaPort + '&instanciaPortF=' + instanciaPortF;
    // 3.- Marcar qu� funci�n manejar� la respuesta
    xmlHttp.onreadystatechange = recogeInfo;

    // 4.- Enviar
    xmlHttp.open("GET", ajaxRequest, true);
    xmlHttp.send("");
}
//Funci�n que envia la peticion Ajax desde la pagina de detalle de adjuntos 
function envioInfoAdj(oEntryID, Id, nom, com, datasize, comsalto, origen) {
    var entryId;
    entryId = oEntryID;
    p = window.opener
    oEntry = p.fsGeneralEntry_getById(entryId)
    mioEntryId = oEntryID;

    var instancia = p.document.getElementById(oEntryID + '__hInstancia').value;
    var tipo = p.document.getElementById(oEntryID + '__hTipo').value
    var adjunto = p.document.getElementById(oEntryID + '__hAct').value
    var adjuntoNew = p.document.getElementById(oEntryID + '__hNew').value
    var nombre = p.document.getElementById(oEntryID + '__hNombre').value
    if (nombre == '') {
        nombre = nom;
    } else {
        nombre = nombre + '; ' + nom;
    }
    //p.document.getElementById(oEntryID + '__hNombre').value=nombre
    var idCampo = p.document.getElementById(oEntryID + '__hCampo').value

    if (adjuntoNew == '') {
        adjuntoNew = Id;
        //p.document.getElementById(oEntryID + '__hNew').value=adjuntoNew;
    } else {
        adjuntoNew = adjuntoNew + 'xx' + Id;
        //p.document.getElementById(oEntryID + '__hNew').value=adjuntoNew;
    }
    var instanciaPort = 0
    if (p.document.getElementById(oEntryID + '__hInstanciaPort')) {
        instanciaPort = p.document.getElementById(oEntryID + '__hInstanciaPort').value;
    }
    var instanciaPortF = 0
    if (p.document.getElementById(oEntryID + '__hInstanciaPortF')) {
        instanciaPortF = p.document.getElementById(oEntryID + '__hInstanciaPortF').value;
    }

    // 1.- Creamos el objeto xmlHttpRequest
    CreateXmlHttp();

    // 2.- Definimos la llamada para hacer un simple GET.S
    var ajaxRequest = '../_common/GestionaAjax.aspx?accion=2&EntryID=' + oEntryID + '&instancia=' + instancia + '&tipo=' + tipo + '&nombre=' + nombre + '&adjunto=' + adjunto + '&adjuntoNew=' + adjuntoNew + '&campo=' + idCampo + '&readOnly=' + oEntry.readOnly.toString() + '&origen=' + origen.toString() + '&defecto=' + oEntry.DataValorDefecto + '&instanciaPort=' + instanciaPort + '&instanciaPortF=' + instanciaPortF;

    // 3.- Marcar qu� funci�n manejar� la respuesta
    xmlHttp.onreadystatechange = recogeInfo;

    // 4.- Enviar
    xmlHttp.open("GET", ajaxRequest, true);
    xmlHttp.send("");
}
//Funci�n que envia la peticion Ajax
function envioInfoAdjuntoContrato(oEntryID, Id, nom, com, datasize, comsalto, origen, IdContrato, PathAdjunto) {
    var entryId;
    entryId = oEntryID;
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");
    oEntry = fsGeneralEntry_getById(entryId)
    mioEntryId = oEntryID;
    var instancia = document.getElementById(oEntryID + '__hInstancia').value;
    var tipo = document.getElementById(oEntryID + '__hTipo').value
    var adjunto = document.getElementById(oEntryID + '__hAct').value
    document.getElementById(oEntryID + '__hAct').value = ''
    var nombre = document.getElementById(oEntryID + '__hNombre').value
    if (nombre == '') {
        nombre = nom;
    } else {
        nombre = nombre + '; ' + nom;
    }
    document.getElementById(oEntryID + '__hNombre').value = nombre
    var Path_Contrato = document.getElementById('hid_pathContrato')
    if (Path_Contrato) {
        Path_Contrato.value = PathAdjunto
    }

    var Nom_Contrato = document.getElementById('hid_NombreContrato')
    if (Nom_Contrato) {
        Nom_Contrato.value = nom
    }

    var idCampo = document.getElementById(oEntryID + '__hCampo').value


    adjunto = Id;
    document.getElementById(oEntryID + '__hAct').value = adjunto;


    // 1.- Creamos el objeto xmlHttpRequest
    CreateXmlHttp();

    // 2.- Definimos la llamada para hacer un simple GET.
    var ajaxRequest = '../_common/GestionaAjax.aspx?idArchivoContrato=' + Id + '&EsContrato=1&accion=1&EntryID=' + oEntryID + '&instancia=' + instancia + '&tipo=' + tipo + '&nombre=' + nombre + '&adjunto=' + adjunto + '&adjuntoNew=&campo=' + idCampo + '&readOnly=' + oEntry.readOnly.toString() + '&origen=""&IdContrato=' + IdContrato.toString() + '&defecto=' + oEntry.DataValorDefecto + '&datasize=' + datasize + '&instanciaPort=0' 
    // 3.- Marcar qu� funci�n manejar� la respuesta
    xmlHttp.onreadystatechange = recogeInfo;

    // 4.- Enviar
    xmlHttp.open("GET", ajaxRequest, true);
    xmlHttp.send("");
}
//Funci�n que recoge la respuesta de la peticion Ajax
function recogeInfo() {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
        var div;
        while (mioEntryId.indexOf('$') >= 0)
            mioEntryId = mioEntryId.replace("$", "_");
        div = document.getElementById(mioEntryId + "_divcontenedor")
        div.innerHTML = xmlHttp.responseText;

        //recojo los nombres de todos los adjuntos y los asigno al editor del Entry
        if (document.getElementById(mioEntryId + '__hNombre')) {
            hid_NomAdjuntos = document.getElementById(mioEntryId + "__hidNombresAdj").value
            document.getElementById(mioEntryId + '__hNombre').value = hid_NomAdjuntos
        }
        xmlHttp.onreadystatechange = function () { };
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////
/*
''' <summary>
''' Funcion que abre la pantalla para adjuntar un fichero
''' </summary>    
''' <param name="instancia">instancia</param> 
''' <param name="adjuntos">adjuntos que tiene la solicitud</param> 
''' <param name="tipo">tipo del adjunto</param>  
''' <param name="entryId">Id del dataentry</param>  
''' <param name="Origen">origen desde donde se llama a la funcion</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function show_atach_file(instancia, adjuntos, tipo, entryId, origen, idContrato, nombreAdjunto) {
    var newWindow = window.open("../_common/attachfile.aspx?instancia=" + instancia + "&id=" + adjuntos.toString() + "&tipo=" + tipo.toString() + "&entryId=" + entryId.toString() + "&origen=" + origen.toString() + "&IdContrato=" + idContrato.toString() + "&nombre=" + encodeURIComponent(nombreAdjunto), "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200")
    newWindow.focus()
}
function show_atach_file_Contrato(instancia, idAdjuntoContrato, entryId, origen, idContrato, nombreAdjunto) {
    var newWindow = window.open("../_common/attachfile.aspx?EsContrato=1&instancia=" + instancia + "&idContrato=" + idContrato.toString() + "&entryId=" + entryId.toString() + "&origen=" + origen.toString() + "&nombre=" + encodeURIComponent(nombreAdjunto) + "&idAdjuntoContrato=" + idAdjuntoContrato.toString(), "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200")
    newWindow.focus()
}
/*
''' <summary>
''' Funcion que abre la pantalla de detalle para adjuntar un fichero o eliminar/sustituit los existentes
''' </summary>    
''' <param name="instancia">instancia</param> 
''' <param name="adjuntos">adjuntos que tiene la solicitud</param> 
''' <param name="tipo">tipo del adjunto</param>  
''' <param name="entryId">Id del dataentry</param>  
''' <param name="Origen">origen desde donde se llama a la funcion</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function show_atach_fileDesglose(instancia, adjuntos, adjuntosNew, tipo, entryId) {
    oInput = fsGeneralEntry_getById(entryId)
    var adjuntosAct = document.getElementById(entryId + '__hAct').value
    var adjuntosNew = document.getElementById(entryId + '__hNew').value
    var solicitud = document.getElementById('Solicitud');
    var idSolicitud
    if (solicitud)
        idSolicitud = solicitud.value
    else
        idSolicitud = 0
    var instanciaPort = 0
    if (document.getElementById(entryId + '__hInstanciaPort')) {
        instanciaPort = document.getElementById(entryId + '__hInstanciaPort').value;
    }
    var instanciaPortF = 0
    if (document.getElementById(entryId + '__hInstanciaPortF')) {
        instanciaPortF = document.getElementById(entryId + '__hInstanciaPortF').value;
    }
    var newWindow = window.open("../_common/atachedfilesDesglose.aspx?solicitud=" + idSolicitud.toString() + "&instancia=" + instancia + "&adjuntos=" + adjuntosAct + "&adjuntosNew=" + adjuntosNew.toString() + "&tipo=" + tipo.toString() + "&entryId=" + entryId.toString() + '&readOnly=' + oInput.readOnly.toString() + '&nombreCampo=' + encodeURI(oInput.title.toString()) + '&idCampo=' + oInput.idCampo.toString() + '&defecto=' + oInput.DataValorDefecto + '&instanciaPort=' + instanciaPort.toString() + '&instanciaPortF=' + instanciaPortF.toString(), "_blank", "width=800,height=300,status=yes,resizable=yes,top=200,left=250,scrollbars=yes") //"_blank","width=800,height=600,status=no,resizable=yes,top=200,left=200,scrollbars=yes")
    newWindow.focus()
}
/*
''' <summary>
''' Funcion que borra de la solicitud el adjunto indicado
''' </summary>    
''' <param name="idAdjunto">id del adjunto a eliminar</param> 
''' <param name="tipo">tipo del adjunto</param> 
''' <param name="instancia">instancia</param>  
''' <param name="entryId">Id del dataentry</param>  
''' <param name="Origen">origen desde donde se llama a la funcion</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function borrarAdjunto(idAdjunto, tipo, instancia, entryId, origen, idContrato) {
    mioEntryId = entryId;
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");
    oInput = fsGeneralEntry_getById(entryId)
    oInput.removeFile(tipo, idAdjunto)

    if (typeof (idContrato) == 'undefined')
        idContrato = 0;

    var instancia = document.getElementById(entryId + '__hInstancia').value;
    var tipo = document.getElementById(entryId + '__hTipo').value
    var adjunto = document.getElementById(entryId + '__hAct').value
    var adjuntoNew = document.getElementById(entryId + '__hNew').value
    var nombre = document.getElementById(entryId + '__hNombre').value
    var idCampo = document.getElementById(entryId + '__hCampo').value
    var instanciaPort = 0
    if (document.getElementById(entryId + '__hInstanciaPort')) {
        instanciaPort = document.getElementById(entryId + '__hInstanciaPort').value;
    }
    var instanciaPortF = 0
    if (document.getElementById(entryId + '__hInstanciaPortF')) {
        instanciaPortF = document.getElementById(entryId + '__hInstanciaPortF').value;
    }

    // 1.- Creamos el objeto xmlHttpRequest
    CreateXmlHttp();

    // 2.- Definimos la llamada para hacer un simple GET.
    var ajaxRequest = '../_common/GestionaAjax.aspx?accion=1&EntryID=' + mioEntryId + '&instancia=' + instancia + '&tipo=' + tipo + '&nombre=' + nombre + '&adjunto=' + adjunto + '&adjuntoNew=' + adjuntoNew + '&campo=' + idCampo + '&readOnly=' + oInput.readOnly.toString() + '&origen=' + origen.toString() + '&IdContrato=' + idContrato.toString() + '&defecto=' + oInput.DataValorDefecto + '&instanciaPort=' + instanciaPort.toString() + '&instanciaPortF=' + instanciaPortF.toString();

    // 3.- Marcar qu� funci�n manejar� la respuesta
    xmlHttp.onreadystatechange = recogeInfo;

    // 4.- Enviar
    xmlHttp.open("GET", ajaxRequest, true);
    xmlHttp.send("");
}
/*
''' <summary>
''' Funcion que borra de la solicitud el adjunto indicado(de contrato)
''' </summary>    
''' <param name="idAdjunto">id del adjunto a eliminar</param> 
''' <param name="tipo">tipo del adjunto</param> 
''' <param name="instancia">instancia</param>  
''' <param name="entryId">Id del dataentry</param>  
''' <param name="Origen">origen desde donde se llama a la funcion</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function borrarAdjuntoContrato(idAdjunto, tipo, instancia, entryId, origen, idContrato) {
    mioEntryId = entryId;
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");
    oInput = fsGeneralEntry_getById(entryId)
    oInput.removeFile(tipo, idAdjunto)

    if (typeof (idContrato) == 'undefined')
        idContrato = 0;

    //borramos el adjunto del contrato
    document.getElementById(entryId + '__hAct').value = ''

    var instancia = document.getElementById(entryId + '__hInstancia').value;
    var tipo = document.getElementById(entryId + '__hTipo').value
    var adjunto = document.getElementById(entryId + '__hAct').value
    var adjuntoNew = document.getElementById(entryId + '__hNew').value
    var nombre = document.getElementById(entryId + '__hNombre').value
    var idCampo = document.getElementById(entryId + '__hCampo').value
    var instanciaPort = 0
    if (document.getElementById(entryId + '__hInstanciaPort')) {
        instanciaPort = document.getElementById(entryId + '__hInstanciaPort').value;
    }
    var instanciaPortF =0
    if (document.getElementById(entryId + '__hInstanciaPortF')) {
        instanciaPortF = document.getElementById(entryId + '__hInstanciaPortF').value;
    }

    //borramos el adjunto del contrato
    document.getElementById(entryId + '__hAct').value = ''
    //borramos el adjunto del contrato
    document.getElementById(entryId + '__hNew').value = ''

    // 1.- Creamos el objeto xmlHttpRequest
    CreateXmlHttp();

    // 2.- Definimos la llamada para hacer un simple GET.
    var ajaxRequest = '../_common/GestionaAjax.aspx?Borrando=1&EsContrato=1&accion=1&EntryID=' + mioEntryId + '&instancia=' + instancia + '&tipo=' + tipo + '&nombre=' + nombre + '&adjunto=' + adjunto + '&adjuntoNew=' + adjuntoNew + '&campo=' + idCampo + '&readOnly=' + oInput.readOnly.toString() + '&origen=' + origen.toString() + '&IdContrato=' + idContrato.toString() + '&defecto=' + oInput.DataValorDefecto + '&instanciaPort=' + instanciaPort.toString() + '&instanciaPortF=' + instanciaPortF.toString();

    // 3.- Marcar qu� funci�n manejar� la respuesta
    xmlHttp.onreadystatechange = recogeInfo;

    // 4.- Enviar
    xmlHttp.open("GET", ajaxRequest, true);
    xmlHttp.send("");
}
/*
''' <summary>
''' Funcion que borra de la solicitud el adjunto indicado llamado desde la pagina de detalle de adjuntos
''' </summary>    
''' <param name="sololectura">indica si se mostrara la pantalla en modo de solo lectura</param>  
''' <param name="entryId">Id del dataentry</param>  
''' <param name="Origen">origen desde donde se llama a la funcion</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function borrarAdjuntoDetalle(entryId, sololectura, origen) {
    mioEntryId = entryId;
    var p = window.opener
    oInput = p.fsGeneralEntry_getById(entryId)
    var instancia = p.document.getElementById(entryId + '__hInstancia').value;
    var tipo = p.document.getElementById(entryId + '__hTipo').value
    var adjunto = p.document.getElementById(entryId + '__hAct').value
    var adjuntoNew = p.document.getElementById(entryId + '__hNew').value
    var nombre = p.document.getElementById(entryId + '__hNombre').value
    var idCampo = p.document.getElementById(entryId + '__hCampo').value
    var instanciaPort = 0
    if (p.document.getElementById(entryId + '__hInstanciaPort')) {
        instanciaPort = p.document.getElementById(entryId + '__hInstanciaPort').value;
    }
    var instanciaPortF =0
    if (p.document.getElementById(entryId + '__hInstanciaPortF')) {
        instanciaPortF = p.document.getElementById(entryId + '__hInstanciaPortF').value;
    }

    // 1.- Creamos el objeto xmlHttpRequest
    CreateXmlHttp();

    // 2.- Definimos la llamada para hacer un simple GET.
    var ajaxRequest = '../_common/GestionaAjax.aspx?accion=2&EntryID=' + entryId + '&instancia=' + instancia + '&tipo=' + tipo + '&nombre=' + nombre + '&adjunto=' + adjunto + '&adjuntoNew=' + adjuntoNew + '&campo=' + idCampo + '&readOnly=' + sololectura + '&origen=' + origen.toString() + '&defecto=' + oInput.DataValorDefecto + '&instanciaPort=' + instanciaPort.toString() + '&instanciaPortF=' + instanciaPortF.toString();

    // 3.- Marcar qu� funci�n manejar� la respuesta
    xmlHttp.onreadystatechange = recogeInfo;

    // 4.- Enviar
    xmlHttp.open("GET", ajaxRequest, true);
    xmlHttp.send("");
}
/*
''' <summary>
''' Funcion que sustituye un adjunto
''' </summary>    
''' <param name="idadjunto">id del adjunto que sera sustituido</param>  
''' <param name="tipo">tipo del adjunto que sera sustituido</param>  
''' <param name="Origen">origen desde donde se llama a la funcion</param>  
''' <param name="entryId">id del DataEntry</param>  
''' <param name="instancia">instancia de la solicitud</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function sustituirAdjunto(idAdjunto, tipo, instancia, entryId, origen, nombreAdjunto) {
    mioEntryId = entryId;
    oInput = fsGeneralEntry_getById(entryId)
    oInput.removeFile(tipo, idAdjunto)

    show_atach_file(instancia, idAdjunto, tipo, mioEntryId, origen, 0, nombreAdjunto)
}
/*
''' <summary>
''' Funcion que sustituye un adjunto
''' </summary>    
''' <param name="idadjunto">id del adjunto que sera sustituido</param>  
''' <param name="tipo">tipo del adjunto que sera sustituido</param>  
''' <param name="Origen">origen desde donde se llama a la funcion</param>  
''' <param name="entryId">id del DataEntry</param>  
''' <param name="instancia">instancia de la solicitud</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function sustituirAdjuntoContrato(idAdjunto, tipo, instancia, entryId, origen, nombreAdjunto) {
    mioEntryId = entryId;
    oInput = fsGeneralEntry_getById(entryId)
    oInput.removeFile(tipo, idAdjunto)

    show_atach_file_Contrato(instancia, idAdjunto, mioEntryId, origen, 0, nombreAdjunto)
}
/*
''' <summary>
''' Funcion que sustituye un adjunto en la pantalla de detalle
''' </summary>    
''' <param name="idadjunto">id del adjunto que sera sustituido</param>  
''' <param name="tipo">tipo del adjunto que sera sustituido</param>  
''' <param name="Origen">origen desde donde se llama a la funcion</param>  
''' <param name="entryId">id del DataEntry</param>  
''' <param name="instancia">instancia de la solicitud</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function sustituirAdjuntoDetalle(idAdjunto, tipo, instancia, entryId, origen, nombreAdjunto) {
    mioEntryId = entryId;
    var p;
    p = window.opener;
    oInput = p.fsGeneralEntry_getById(entryId)
    oInput.removeFile(tipo, idAdjunto)

    show_atach_file(instancia, idAdjunto, tipo, mioEntryId, origen, 0, nombreAdjunto)

}
/*
''' <summary>
''' Funcion que llama a la pantalla que descarga el adjunto
''' </summary>    
''' <param name="idadjunto">id del adjunto a descargar</param>  
''' <param name="tipo">tipo del adjunto que sera sustituido</param>  
''' <param name="instancia">instancia de la solicitud</param>  
''' <returns></returns>
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function descargarAdjunto(idAdjunto, tipo, instancia, entryId, Campo, Nombre, fecha, esAltaFactura) {
    var adjuntosNew
    var adjuntosAct
    var oadjuntosAct = document.getElementById(entryId + '__hAct')
    if (oadjuntosAct)
        adjuntosAct = oadjuntosAct.value
    else
        adjuntosAct = idAdjunto
    var oadjuntosNew = document.getElementById(entryId + '__hNew')
    if (oadjuntosNew)
        adjuntosNew = oadjuntosNew.value
    else
        adjuntosNew = ''

    if ((adjuntosNew != '' && adjuntosAct != '' && tipo == 3) || (adjuntosAct.indexOf('xx') != -1 && tipo == 3) || (adjuntosNew.indexOf('xx') != -1 && tipo == 3))
        descargarAdjuntos(adjuntosAct, adjuntosNew, instancia, tipo, entryId, Campo, Nombre, fecha, esAltaFactura)
    else {
        var newWindow = window.open("../_common/attach.aspx?instancia=" + instancia + "&id=" + idAdjunto.toString() + "&tipo=" + tipo.toString() + "&Campo=" + Campo.toString() + "&Nombre=" + Nombre + "&fecha=" + fecha.toString() + "&esAltaFactura=" + esAltaFactura.toString(), "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200")
        newWindow.focus()
    }
}
/*
''' <summary>
''' Funcion que llama a la pantalla que descarga un Zip con todos los adjuntos
''' </summary>    
''' <param name="idadjuntos">id de los adjuntos que estaban grabados en la solicitud</param>  
''' <param name="idadjuntosNew">id de los adjuntos que se han dado de alta</param>  
''' <param name="tipo">tipo del adjunto que sera sustituido</param>  
''' <param name="instancia">instancia de la solicitud</param>  
''' <returns></returns>
*/
function descargarAdjuntos(idAdjuntos, idAdjuntosNew, instancia, tipo, entryId, Campo, Nombre, fecha, esAltaFactura) {

    var adjuntosAct = document.getElementById(entryId + '__hAct').value
    var adjuntosNew = document.getElementById(entryId + '__hNew').value

    if (adjuntosNew == '' && adjuntosAct.indexOf('xx') == -1)
        if (tipo.indexOf(',3'))
            descargarAdjunto(adjuntosAct, 3, instancia, entryId, Campo, Nombre, fecha, esAltaFactura)
        else
            if (tipo.indexOf(',1'))
                descargarAdjunto(adjuntosAct, 1, instancia, entryId, Campo, Nombre, fecha, esAltaFactura)
            else
                descargarAdjunto(adjuntosAct, tipo, instancia, entryId, Campo, Nombre, fecha, esAltaFactura)
    else
        if (adjuntosAct == '' && adjuntosNew.indexOf('xx') == -1)
            descargarAdjunto(adjuntosNew, tipo, instancia, entryId, Campo, Nombre, fecha, esAltaFactura)
        else {
            var newWindow = window.open("../_common/attach.aspx?adjuntosNew=" + idAdjuntosNew.toString() + "&adjuntos=" + idAdjuntos.toString() + "&tipo=" + encodeURI(tipo.toString()) + "&instancia=" + instancia.toString() + "&Campo=" + Campo.toString() + "&esAltaFactura=" + esAltaFactura.toString(), "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200")
            newWindow.focus()
        }
}
/*
''' <summary>
''' Funcion que llama a la pantalla que descarga el adjunto de contrato
''' </summary>    
''' <param name="idContrato">idContrato</param>  
''' <param name="sNombreAdjunto">Nombre del adjunto</param>  
''' <remarks>Llamada desde: GeneralEntry.vb; Tiempo m�ximo:0</remarks>*/
function descargarAdjuntoContrato(instancia, idContrato, idArchivoContrato, sNombreAdjunto, datasize, Campo, fecha, EsTipo1) {
    var newWindow = window.open("../_common/attach.aspx?idContrato=" + idContrato.toString() + "&idArchivoContrato=" + idArchivoContrato + "&NombreAdjunto=" + encodeURIComponent(sNombreAdjunto.toString()) + "&datasize=" + datasize.toString() + "&instancia=" + instancia.toString() + "&Campo=" + Campo.toString() + "&fecha=" + fecha.toString() + "&EsTipo1=" + EsTipo1.toString() + "&esAltaFactura=0", "_blank", "width=600,height=275,status=no,resizable=yes,top=200,left=200")
    newWindow.focus()
}
/*
''' <summary>
''' Funcion que realiza la peticion Ajax para eliminar todos los adjuntos
''' </summary>    
''' <param name="entryId">id del DataEntry</param>  
''' <returns></returns>
*/
function eliminarAdjuntos(entryId, bConContrato) {
    mioEntryId = entryId;
    document.getElementById(entryId + '__hAct').value = '';
    document.getElementById(entryId + '__hNew').value = '';
    var instancia = document.getElementById(entryId + '__hInstancia').value;
    var tipo = document.getElementById(entryId + '__hTipo').value
    if (bConContrato == 1)
        var adjunto = -1
    else
        var adjunto = document.getElementById(entryId + '__hAct').value

    var adjuntoNew = document.getElementById(entryId + '__hNew').value

    var nombre = document.getElementById(entryId + '__hNombre').value
    var idCampo = document.getElementById(entryId + '__hCampo').value
    var instanciaPort = 0
    if (document.getElementById(entryId + '__hInstanciaPort')) {
        instanciaPort = document.getElementById(entryId + '__hInstanciaPort').value;
    }
    var instanciaPortF = 0
    if (document.getElementById(entryId + '__hInstanciaPortF')) {
        instanciaPortF = document.getElementById(entryId + '__hInstanciaPortF').value;
    }

    oEntry = fsGeneralEntry_getById(entryId)
    // 1.- Creamos el objeto xmlHttpRequest
    CreateXmlHttp();

    // 2.- Definimos la llamada para hacer un simple GET.
    var ajaxRequest = '../_common/GestionaAjax.aspx?accion=1&EntryID=' + entryId + '&instancia=' + instancia + '&tipo=' + tipo + '&nombre=' + nombre + '&adjunto=' + adjunto + '&adjuntoNew=' + adjuntoNew + '&campo=' + idCampo + '&readOnly=' + oEntry.readOnly.toString() + '&defecto=' + oEntry.DataValorDefecto + '&instanciaPort=' + instanciaPort.toString() + '&instanciaPortF=' + instanciaPortF.toString();

    // 3.- Marcar qu� funci�n manejar� la respuesta
    xmlHttp.onreadystatechange = recogeInfo;

    // 4.- Enviar
    xmlHttp.open("GET", ajaxRequest, true);
    xmlHttp.send("");
}
/*
''' <summary>
''' Funcion que aumenta el tama�o de la caja de texto con los comentarios de los adjuntos
''' </summary>    
''' <param name="entryId">id del DataEntry</param>  
''' <param name="idAdjunto">id del adjunto</param> 
''' <returns></returns>
*/
function aumentarTamanoCajaTexto(entryId, idAdjunto, readOnly) {
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");
    var cajaTexto;
    var hid_Texto;
    cajaTexto = document.getElementById(entryId + "__text" + idAdjunto)
    hid_Texto = document.getElementById(entryId + "__hidComent" + idAdjunto)

    cajaTexto.rows = 8
    if (readOnly == 1)
        cajaTexto.value = hid_Texto.value
    else
        cajaTexto.style.height = "200px"
    cajaTexto.style.Zindex = "99"
    cajaTexto.style.overflow = "scroll"
}
/*
''' <summary>
''' Funcion que aumenta el tama�o de la caja de texto con los comentarios de los adjuntos
''' </summary>    
''' <param name="entryId">id del DataEntry</param>  
''' <param name="idAdjunto">id del adjunto</param> 
''' <returns></returns>
*/
function reducirTamanoCajaTexto(entryId, idAdjunto, tipo, instancia, guardar, pos) {
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");
    var cajaTexto;
    cajaTexto = document.getElementById(entryId + "__text" + idAdjunto)
    cajaTexto.style.Zindex = "99"
    if (pos > 0)
        cajaTexto.value = cajaTexto.value.substring(0, pos) + " ...."
    else
        cajaTexto.style.height = "33px"
    cajaTexto.rows = 2
    cajaTexto.style.overflow = "hidden"
    if (guardar == 1) {
        // 1.- Creamos el objeto xmlHttpRequest
        CreateXmlHttp();

        // 2.- Definimos la llamada para hacer un simple GET.
        var ajaxRequest = '../_common/GestionaAjax.aspx?accion=3&tipo=' + tipo + '&instancia=' + instancia + '&adjunto=' + idAdjunto + '&coment=' + encodeURI(cajaTexto.value)

        // 3.- Marcar qu� funci�n manejar� la respuesta
        xmlHttp.onreadystatechange = recogeInfoAdjunto;

        // 4.- Enviar
        xmlHttp.open("GET", ajaxRequest, true);
        xmlHttp.send("");
    }

}
/*
''' <summary>
''' Funcion que aumenta el tama�o de la caja de texto con los comentarios de los adjuntos
''' </summary>    
''' <param name="entryId">id del DataEntry</param>  
''' <param name="idAdjunto">id del adjunto</param> 
''' <returns></returns>
*/
function reducirTamanoCajaTextoContrato(entryId, idAdjunto, tipo, instancia, guardar, pos, idArchivoContrato) {
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");
    var cajaTexto;
    cajaTexto = document.getElementById(entryId + "__text" + idAdjunto)
    cajaTexto.style.Zindex = "99"
    if (pos > 0)
        cajaTexto.value = cajaTexto.value.substring(0, pos) + " ...."
    else
        cajaTexto.style.height = "33px"
    cajaTexto.rows = 2
    cajaTexto.style.overflow = "hidden"
    if (guardar == 1) {
        // 1.- Creamos el objeto xmlHttpRequest
        CreateXmlHttp();

        // 2.- Definimos la llamada para hacer un simple GET.
        var ajaxRequest = '../_common/GestionaAjax.aspx?accion=3&tipo=5&instancia=' + instancia + '&adjunto=' + idAdjunto + '&idArchivoContrato=' + idArchivoContrato + '&coment=' + encodeURI(cajaTexto.value)

        // 3.- Marcar qu� funci�n manejar� la respuesta
        xmlHttp.onreadystatechange = recogeInfoAdjunto;

        // 4.- Enviar
        xmlHttp.open("GET", ajaxRequest, true);
        xmlHttp.send("");
    }

}
function recogeInfoAdjunto() {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {

    }
}
/*
''' <summary>
''' Funcion que aumenta el tama�o de la caja de texto del campo tipo medio o largo
''' </summary>    
''' <param name="entryId">id del DataEntry</param>  
''' <param name="numFilas">n�mero de filas que se va a aumentar</param> 
''' <returns></returns>
*/
function aumentarTamanoCampo(entryId, tipo, desglose) {
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");

    var cajaTexto;
    cajaTexto = document.getElementById(entryId + "__t")

    if (tipo == 6) {
        cajaTexto.rows = 3
        cajaTexto.style.height = "60px"
    } else {
        cajaTexto.rows = 8
        cajaTexto.style.height = "120px"
    }

    cajaTexto.style.zIndex = "99"
    cajaTexto.style.overflow = "scroll"

    if (desglose == true) {
        var pos = $common.getLocation(cajaTexto);
        var posx = pos.x;
        var posy = pos.y

        cajaTexto.style.position = "absolute";
        cajaTexto.style.left = posx + "px";
        cajaTexto.style.top = posy + "px";
    }
}
/*
''' <summary>
''' Funcion que aumenta el tama�o de la caja de texto con los comentarios de los adjuntos
''' </summary>    
''' <param name="entryId">id del DataEntry</param>  
''' <param name="idAdjunto">id del adjunto</param> 
''' <returns></returns>
*/
function reducirTamanoCampo(entryId, desglose) {
    while (entryId.indexOf('$') >= 0)
        entryId = entryId.replace("$", "_");

    var cajaTexto;
    cajaTexto = document.getElementById(entryId + "__t");
    cajaTexto.style.zIndex = 10;
    cajaTexto.style.height = "15px";
    cajaTexto.rows = 1;
    cajaTexto.style.overflow = "hidden";

    if (desglose == true) {
        cajaTexto.style.position = "relative";
        cajaTexto.style.left = "";
        cajaTexto.style.top = "-5px";
    }
}
/*  Revisado por: blp. Fecha: 06/10/2011
''' Funci�n que hace recupera datos de servidor y los inserta en el control seleccionado
''' sUrl: ruta de la funcion a usar
''' sData: par�metros que la funcion requiere
''' sIDDestino: Id del control que se desea actualizar
''' sSelectedItemIndex: �ndice del registro a mostrar seleccionado cuando se despliega el combo. NO USAR NUNCA AL MISMO TIEMPO QUE sSelectedItemValue
''' sSelectedItemValue: Valor del registro a mostrar seleccionado cuando se despliega el combo. 
'''                     NO USAR NUNCA AL MISMO TIEMPO QUE sSelectedItemIndex
'''                     Usar NULL para evitar que se use
''' Si se usan a la vez sSelectedItemIndex y sSelectedItemValue, tiene prioridad el sSelectedItemValue
''' sTemplate: Nombre del campo de datos que se usa en el template para rellenar el combo a trav�s de json. 
*/
function ajaxUpdate(sUrl, sData, sIDDestino, sSelectedItemIndex, sSelectedItemValue, tipoGS) {
    sData = '{"contextKey":"' + sData + "@@@" + tipoGS + '"}'

    $.ajax({
        type: "POST",
        url: sUrl,
        data: sData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            var dataSource;
            if (data.d != null) {
                dataSource = jQuery.parseJSON('[' + data.d + ']')
            }
            else {//No hay datos
                dataSource = jQuery.parseJSON('[{""Valor"":"""",""Texto"":""""}]')
            }
            var oControlDestino = $find(sIDDestino);
            oControlDestino.set_template('<li value=${Valor}>${Texto}</li>')
            oControlDestino.set_dataSource(dataSource);
            oControlDestino.dataBind();
            oControlDestino = null;
            //Si el control tiene valor seleccionado, lo seleccionamos
            if (sSelectedItemValue != null) {
                setTimeout('CambiarSeleccionValue("' + sIDDestino + '","' + sSelectedItemValue + '")', 200);
            }
        },
        failure: function () { alert("Error with AJAX callback"); }
    });
}
/*  Revisado por: blp. Fecha: 06/10/2011
''' M�todo que selecciona un registro concreto en el control del que se pasa el ID.
''' sIDControl: Id del control (generalmente webdropdown) en el que se va a seleccionar un registro concreto
''' sSelectedItemIndex: �ndice del elemento a seleccionar. Si es Undefined no se selecciona ninguno
''' Llamada desde ajaxUpdate(). Tiempo m�ximo inferior a 0,1 seg.
*/
function CambiarSeleccion(sIDControl, sSelectedItemIndex) {
    var oControlDestino = $find(sIDControl);
    if (oControlDestino) {
        if (isNaN(parseInt(sSelectedItemIndex))) {
            DeseleccionarDesactivarSeleccionado(sIDControl, true, true)
            oControlDestino.set_selectedItemIndex(-1);
            oControlDestino.set_currentValue('', true)
        }
        else {
            if (parseInt(sSelectedItemIndex) == -1) {
                DeseleccionarDesactivarSeleccionado(sIDControl, true, true)
            }
            oControlDestino.selectItemByIndex(sSelectedItemIndex, true, true);
        }
        oControlDestino = null;
    }
}
/*  Revisado por: blp. Fecha: 06/10/2011
''' M�todo que selecciona un registro concreto en el control a partir del valor seleccionado.
''' sIDControl: Id del control (generalmente webdropdown) en el que se va a seleccionar un registro concreto
''' sSelectedItemValue: Valor del elemento a seleccionar. Si es Undefined no se selecciona ninguno
''' Llamada desde ajaxUpdate() y WebDropDown_DataCheck() en jsALta.js. Tiempo m�ximo inferior a 0,1 seg.
*/
function CambiarSeleccionValue(sIDControl, sSelectedItemValue) {
    var oControlDestino = $find(sIDControl);
    if (sSelectedItemValue == null || sSelectedItemValue == 'undefined' || sSelectedItemValue == '') {
        DeseleccionarDesactivarSeleccionado(sIDControl, true, true)
        oControlDestino.set_selectedItemIndex(-1);
    }
    else {
        if (oControlDestino.get_dataSource) {
            var datos = oControlDestino.get_dataSource();
            var k;
            for (k = 0; k < datos.length; ++k) {
                if (datos[k].Text == sSelectedItemValue) {
                    var sSelectedItemIndex = k;
                    oControlDestino.selectItemByIndex(sSelectedItemIndex, true, true);
                    oControlDestino.set_currentValue(sSelectedItemValue, true)
                    break;
                }
            }
        }
    }
}
/*  Revisado por: blp. Fecha: 28/12/2012
''' M�todo que cierra el ajaxIndicator (la imagen de "Cargando") del webdropdown
''' Hemos asociado el m�todo al evento Focus del webdropdown porque es el que m�s se aproxima al momento en que queremos ocultar el ajaxIndicator
''' senderID: Id del Control webdropdown a cargar
''' Llamada desde: jsAlta.js
''' Tiempo m�ximo inferior a 1 seg.*/
function WebDropDown_CloseAjaxIndicatorID(senderID) {
    var oDropdown = $find(senderID);
    //Ocultamos el ajaxIndicator
    oDropdown.get_ajaxIndicator().hide();
    oDropdown = null;
}
function WebDropDown_KeyDown(sender, e) {
    if ((e.get_browserEvent().keyCode == 18) || (e.get_browserEvent().keyCode == 8)) {
        var dropdown = $find(sender._id);
        //Vaciar texto
        if (dropdown) {
            if (dropdown.set_currentValue)
                dropdown.set_currentValue('', true);
            DeseleccionarDesactivarSeleccionado(sender._id, true, true)
            dropdown.set_selectedItemIndex(-1);
        }

        var dropdownEntry = fsGeneralEntry_getById(sender._id)
        if (dropdownEntry) {
            dropdownEntry.setValue("");
            dropdownEntry.setDataValue("");
            dropdownEntry = null;
        }
        return
    }

    if (e.get_browserEvent().keyCode != 9) {
        var datosDropDown = sender.get_dataSource()
        if (!datosDropDown || datosDropDown.length == 0) {
            var oDropdown = $find(sender._id);
            oDropdown.openDropDown();
        }
        WebDropDown_DataCheck(sender, e)
    }
}
/*  Revisado por: blp. Fecha: 06/10/2011
''' M�todo que se lanza en el evento de despliegue del panel de un webdropdown y carga y muestra sus datos
''' sender: Control webdropdown que lanza el evento
''' e: argumentos del evento
''' Llamada desde: el evento de despliegue del panel del webdropdown de asquellos webdropdown a los que se haya asociado este m�todo al event
''' Tiempo m�ximo inferior a 1 seg.*/
function WebDropDown_DataCheck(sender, e) {
    WebDropDown_DropDownOpening(sender, e)
    var Editor = sender._id
    var oSenderGeneralEntry = fsGeneralEntry_getById(Editor)
    var sMasterField = oSenderGeneralEntry.masterField
    var sidCampoListaEnlazada = oSenderGeneralEntry.idCampoListaEnlazada
    var sidCampoPadreListaEnlazada = oSenderGeneralEntry.idCampoPadreListaEnlazada
    var sURL;

    switch (oSenderGeneralEntry.tipoGS) {
        case 101: //Forma pago
        case 102: //Moneda
        case 105: //Unidad
        case 107: //Pais
        case 108: //Provincia
        case 109: //Destino
        case 122: //Departamento
        case 123: //Organizaci�n de compras
        case 124: //Centro
        case 125: //Almac�n
        case 151: //A�o Partida  
        case 143: //Proveedor ERP
            sURL = '../../consultas.asmx/ObtenerDatos_DropDown'
            break;
        default: //las listas
            sURL = '../../consultas.asmx/Obtener_Lista'
            break;
    }
    debugger;
    //Miramos si el dropdown ya tiene datos
    var datosDropDown = sender.get_dataSource();
    //El combo no puede tener masterField y CampoPadreLista a la vez
    //por lo que vamos a unificar ambos elementos en un �nico valor para no duplicar el c�digo a la hora de recuperar los datos
    var sIdPadre = '';
    var sCampoListaEnlazada = '';
    if (sMasterField != '') {
        sIdPadre = getID(sMasterField, Editor);
    } else {
        if ((oSenderGeneralEntry.idCampoPadreListaEnlazada) && (oSenderGeneralEntry.idCampoPadreListaEnlazada > 0)) {
            var arrAux = sender._id.split("_")
            if (arrAux[1] == '')
                sIdPadre = arrAux[0] + '__' + arrAux[2] + '_' + arrAux[3] + "_" + 'fsentry' + oSenderGeneralEntry.idCampoPadreListaEnlazada + '__t';   //uwtGrupos_ctl01_578_fsentry28277__t"
            else
                sIdPadre = arrAux[0] + '_' + arrAux[1] + '_' + arrAux[2] + "_" + 'fsentry' + oSenderGeneralEntry.idCampoPadreListaEnlazada + '__t';   //uwtGrupos_ctl01_578_fsentry28277__t"
            arrAux = null;

            sCampoListaEnlazada = oSenderGeneralEntry.idCampoListaEnlazada + '#';
        }
    }

    if (sIdPadre != '') {
        var oPadre = fsGeneralEntry_getById(sIdPadre)
        if (!oPadre) {
            //el padre puede que sea un campo de tipo material que est� en otro grupo
            for (arrInput in arrInputs) {
                var o = fsGeneralEntry_getById(arrInputs[arrInput])
                if (o) {
                    if (o.tipoGS == 103 && arrInputs[arrInput].endsWith('fsentry' + oSenderGeneralEntry.idCampoPadreListaEnlazada)) oPadre = fsGeneralEntry_getById(arrInputs[arrInput] + '__t');
                }
            }
        }
        if (oPadre != null && oPadre != undefined) {
            if (oPadre.tipoGS == 103) {
                //En este caso se obtiene la lista de valores de otro m�todo distinto al de una lista con padre otra lista
                sURL = '../../consultas.asmx/Obtener_Lista_CampoPadre_Material';
            }
            var sPadreValue = oPadre.getDataValue();
            //Cuando hay listaEnlazada, el sPadreValue debe llevar no s�lo el valor seleccionado en el padre sino tb el id la lista y la instancia
            if (sCampoListaEnlazada != '') {
                if (oPadre.tipoGS == 103) {
                    sPadreValue = sCampoListaEnlazada + sPadreValue
                }
                else {
                    if (document.getElementById("Instancia")) {
                        var vInstancia = document.getElementById("Instancia").value
                    } else {
                        vInstancia = ""
                    }

                    //En los certificados, puede haber instancia, pero no haber registros en COPIA_CAMPO_VALOR_LISTA. 
                    //Estos casos tienen: Version = 0 y TipoVersion = 1
                    //Limpiamos la instancia para que haga la consulta en CAMPO_VALOR_LISTA
                    if (document.getElementById("Certificado")) { //Se trata de un certificado
                        if (EsCertificadoSinVersion())
                            vInstancia = ""
                    }

                    sPadreValue = sCampoListaEnlazada + sPadreValue + "#" + vInstancia
                    sPadreValue = sPadreValue + "#0#0"
                    vInstancia = null;
                }
            }
            var sPadreValueOld = oPadre.oldDataValue;
            var PadreValueNotNullNotEmpty = (sPadreValue != '' && sPadreValue != null)
            var PadreValueComp = (sPadreValue != sPadreValueOld)
            //Llamamos al update de datos del webdropdown (ajaxUpdate) en tres casos
            //1. Si no tenemos datos en el webdropdown
            //2. Si existe la variable de datos, es decir, se han cargado previamente pero hay Cero resultados (datosDropDown.length)
            // y adem�s el control maestro tiene valor distinto de vac�o y de null (PadreValueNotNullNotEmpty) (este caso es ineficiente porque vamos a acceder a bdd siempre en aquellos campos que devuelven cero resultados pero nos sirve para los campos generados al copiar o a�adir filas, en los que no hay datos pero aparecen como vac�os de datos)
            //3. Si existe la variable de datos (tenemos datos)
            // y adem�s el control maestro tiene valor distinto de vac�o y de null (PadreValueNotNullNotEmpty)
            // y adem�s en control maestro ha cambiado de valor (PadreValueComp)
            if (!datosDropDown || (datosDropDown.length == 0 && PadreValueNotNullNotEmpty) || (datosDropDown && PadreValueNotNullNotEmpty && PadreValueComp)) {
                sender.get_ajaxIndicator().setRelativeContainer(sender.get_element());
                //El ajaxindicator (la imagen de "Cargando") s�lo se muestra si el control q contiene el listado del dropdown est� visible, pero como no est� visible a tiempo, forzamos su visibilidad
                //$get(sender.get_ajaxIndicator()._rc.id).style.display = "block";
                //sender.get_ajaxIndicator() es lo mismo que sender._pi
                //_rc es el control del que depende el ajaxindicator
                //Ahora forzamos que se muestre el ajaxindicator
                sender.get_ajaxIndicator().show(sender)
                //Cargamos los datos en el dropdown
                ajaxUpdate(sURL, sPadreValue, Editor, '', null, oSenderGeneralEntry.tipoGS)
                oPadre.oldDataValue = sPadreValue;
                //Ocultamos el ajaxIndicator
                //No hay un evento adecuado para ocultarlo (ser�a lo ideal), lo que que usamos un setTimeOut
                setTimeout('WebDropDown_CloseAjaxIndicatorID("' + Editor + '")', 200);
            }
            sPadreValue = null;
            sPadreValueOld = null;
            PadreValueNotNullNotEmpty = null;
            PadreValueComp = null;
        }
        oPadre = null;
    } else {
        if (!datosDropDown || datosDropDown.length == 0 || oSenderGeneralEntry.tipoGS == 143 || oSenderGeneralEntry.tipoGS == 151) {
            if (sCampoListaEnlazada == '') {//Esta carga de datos es s�lo para ciertos casos de los campos que no son listas
                sender.get_ajaxIndicator().setRelativeContainer(sender.get_element());
                //El ajaxindicator (la imagen de "Cargando") s�lo se muestra si el control q contiene el listado del dropdown est� visible, pero como no est� visible a tiempo, forzamos su visibilidad
                //$get(sender.get_ajaxIndicator()._rc.id).style.display = "block";
                //Ahora forzamos que se muestre el ajaxindicator
                sender.get_ajaxIndicator().show(sender)
                //Cargamos los datos en el dropdown
                if (oSenderGeneralEntry.tipoGS == 0) {
                    var vInstancia = "";
                    if (document.getElementById("Instancia")) {
                        vInstancia = document.getElementById("Instancia").value

                        //En los certificados, puede haber instancia, pero no haber registros en COPIA_CAMPO_VALOR_LISTA. 
                        //Estos casos tienen: Version = 0 y TipoVersion = 1
                        //Limpiamos la instancia para que haga la consulta en CAMPO_VALOR_LISTA
                        if (document.getElementById("Certificado")) { //Se trata de un certificado
                            if (EsCertificadoSinVersion())
                                vInstancia = ""
                        }
                    }

                    if (vInstancia == "") {
                        sCampoValue = oSenderGeneralEntry.idCampo + "##"
                    } else {
                        if ((oSenderGeneralEntry.idCampoListaEnlazada != 0) && (oSenderGeneralEntry.idCampoListaEnlazada != undefined)) {
                            sCampoValue = oSenderGeneralEntry.idCampoListaEnlazada + "##" + vInstancia
                        } else {
                            sCampoValue = oSenderGeneralEntry.CampoDef_CampoODesgHijo + "##" + vInstancia
                        }
                    }

                    if ((oSenderGeneralEntry.IdAtrib == 0) || (oSenderGeneralEntry.IdAtrib == undefined)) {
                        sCampoValue = sCampoValue + "#0"
                    } else {
                        if (document.getElementById("Bloque")) {
                            sCampoValue = sCampoValue + "#" + document.getElementById("Bloque").value
                        } else {
                            sCampoValue = sCampoValue + "#0"
                        }
                    }

                    if ((oSenderGeneralEntry.IdAtrib == 0) || (oSenderGeneralEntry.IdAtrib == undefined)) {
                        sCampoValue = sCampoValue + "#0"
                    } else {
                        if (document.getElementById("Solicitud")) {
                            sCampoValue = sCampoValue + "#" + document.getElementById("Solicitud").value
                        } else {
                            sCampoValue = sCampoValue + "#0"
                        }
                    }

                    vInstancia = null;
                    ajaxUpdate(sURL, sCampoValue, Editor, '', null, oSenderGeneralEntry.tipoGS)
                } else {
                    switch (oSenderGeneralEntry.tipoGS) {
                        case 143: //Proveedor ERP
                            var sProveValue, sOrgComprasValue
                            var oOrgCompras

                            var oEntryProvErpId = oSenderGeneralEntry.id
                            if (oSenderGeneralEntry.idDataEntryDependent)
                                oOrgCompras = fsGeneralEntry_getById(oSenderGeneralEntry.idDataEntryDependent)
                            //Comprobaremos si tenemos que cambiar el nombre del DataEntry del proveedor
                            if (oEntryProvErpId.indexOf("fsdsentry") != -1) {
                                var arrEntryProvErp = new Array();
                                arrEntryProvErp = oEntryProvErpId.split("_")
                                /*Si el Dataentry que llama a la funcion es un dataentry de desglose(Proveedor ERP), se comprobara
                                si los 2 campos de los que depende(Proveedor y Org.compras) vienen con ids de desglose, el proveedor siempre 
                                tendra que estar al mismo nivel que el Proveedor ERP, la organizacion de compras no*/
                                if (oSenderGeneralEntry.idEntryPROV.indexOf("fsdsentry") == -1) {
                                    //Si el campo proveedor no tiene el id del desglose habra que ponerselo, esto sucede cuando se a�ade una fila en blanco en un desglose
                                    oSenderGeneralEntry.idEntryPROV = oSenderGeneralEntry.idEntryPROV.replace("fsentry", "fsdsentry_" + arrEntryProvErp[arrEntryProvErp.length - 2])
                                }
                                if (oOrgCompras == null || oOrgCompras == undefined) {
                                    //Si no hemos recuperado el dataentry con el id que viene en idDataEntryDependent, cambiaremos el id poniendole el id de un campo de desglose
                                    //para ver si lo encontramos en el desglose
                                    if (oSenderGeneralEntry.idDataEntryDependent) {
                                        oSenderGeneralEntry.idDataEntryDependent = oSenderGeneralEntry.idDataEntryDependent.replace("fsentry", "fsdsentry_" + arrEntryProvErp[arrEntryProvErp.length - 2])
                                        oOrgCompras = fsGeneralEntry_getById(oSenderGeneralEntry.idDataEntryDependent)
                                    } else {
                                        var p = window.opener
                                        //Si no ha recogido el Entry de organizacion de compras sera que el campo proveedor ERP se encuentra en un desglose popup
                                        if (p.sDataEntryOrgComprasFORM) {
                                            oOrgCompras = p.fsGeneralEntry_getById(p.sDataEntryOrgComprasFORM)
                                        }
                                    }

                                }
                            }
                            var oProve = fsGeneralEntry_getById(oSenderGeneralEntry.idEntryPROV)

                            sProveValue = '';
                            sOrgComprasValue = '';
                            if (oProve)
                                sProveValue = oProve.getDataValue();
                            if (oOrgCompras)
                                sOrgComprasValue = oOrgCompras.getDataValue();
                            ajaxUpdate(sURL, sProveValue + '###' + sOrgComprasValue, Editor, '', null, oSenderGeneralEntry.tipoGS)
                            break;
                        case 151:                            
                            var oEntry = null;

                            for (arrInput in arrInputs) {
                                oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
                                if (oEntry) if (oEntry.id == oSenderGeneralEntry.IdEntryPartidaPlurianual) {
                                    sIdPadre = oEntry.id + '__t';
                                    break;
                                }
                            }
                            var p = window.opener;
                            if (sIdPadre == '' && p) {
                                for (arrInput in p.arrInputs) {
                                    oEntry = p.fsGeneralEntry_getById(p.arrInputs[arrInput]);
                                    if (oEntry) if (oEntry.id == oSenderGeneralEntry.IdEntryPartidaPlurianual) {
                                        sIdPadre = oEntry.id + '__t';
                                        break;
                                    }
                                }
                            }
                            oEntry = null;

                            var oPadre = fsGeneralEntry_getById(sIdPadre)
                            sCampoPres0 = ' ';
                            sCampoValue = ' ';
                            if (oPadre) {
                                sCampoPres0 = oPadre.PRES5
                                sCampoValue = ' ' + oPadre.getDataValue();
                            }
                            ajaxUpdate(sURL, sCampoPres0 + "@@" + sCampoValue, Editor, '', null, oSenderGeneralEntry.tipoGS)
                            break;
                        default:
                            ajaxUpdate(sURL, '', Editor, '', null, oSenderGeneralEntry.tipoGS)
                    }

                }
                //Ocultamos el ajaxIndicator
                //No hay un evento adecuado para ocultarlo (ser�a lo ideal), lo que que usamos un setTimeOut
                setTimeout('WebDropDown_CloseAjaxIndicatorID("' + Editor + '")', 200);
            }
        }
    }

    //cuando haya datos, nos aseguramos de que, si el dropdown tiene un valor, est� realmente seleccionado
    var sSelectedDataValue = null;
    if (oSenderGeneralEntry.getDataValue() != null && oSenderGeneralEntry.getDataValue() != '' && oSenderGeneralEntry.getDataValue() != "undefined") {
        if (oSenderGeneralEntry.getValue() != null) {
            sSelectedDataValue = oSenderGeneralEntry.getValue();
        }
    }
    else {
        //cuando no hay entry porque el control es generado din�micamente desde javascript, controlamos si tiene algo en el cuadro de texto
        //y a partir de ese dato recuperamos el valor
        if (sender.get_currentValue() != null && sender.get_currentValue() != undefined) {
            sSelectedDataValue = sender.get_currentValue()
        }
    }
    if (sSelectedDataValue != null) {
        //Cuando acaba de haber una carga de datos desde ajax, esta funci�n s�lo funciona usando un timeout
        setTimeout('CambiarSeleccionValue("' + Editor + '","' + sSelectedDataValue + '")', 200);
    }
    Editor = null;
    oSenderGeneralEntry = null;
    sMasterField = null;
    sidCampoListaEnlazada = null;
    sidCampoPadreListaEnlazada = null;
    sURL = null;
    datosDropDown = null;
    sIdPadre = null;
    sCampoListaEnlazada = null;
    sSelectedDataValue = null;
}
//Puede que el certificado no tenga todavia versi�n, aunque si exista la instancia
function EsCertificadoSinVersion() {
    var version = -1;
    var tipoVersion = 0;

    if (document.getElementById("Version")) {
        version = document.getElementById("Version").value;
    }
    if (document.getElementById("tipoVersion")) {
        tipoVersion = document.getElementById("tipoVersion").value;
    }

    if (version == 0 && tipoVersion != 3) {
        return true;
    } else {
        return false;
    }
}
/*  Revisado por: blp. Fecha: 06/10/2011
''' Funci�n que devuelve un ID editado a partir de un fragmento de ID que queremos insertar en un ID completo
''' Si no se cumplen los criterios para insertarlo, se devuelve el ID a insertar recibido
''' IDInsertar: Fragmento de ID a insertar
''' IDCompleto: Id completo enb el que se inserta el fragmento
''' Llamada desde: WebDropDown_DataCheck. M�ximo < 0,1 seg
*/
function getID(IdInsertar, IdCompleto) {
    var IDEditado;
    var sSearchingText = "fsentry"
    if (IdCompleto.search(sSearchingText) == -1) {
        sSearchingText = "fsdsentry"
    }
    if (IdCompleto.search(sSearchingText) > -1) {
        var sInit = IdCompleto.substr(0, IdCompleto.search(sSearchingText));
        var sEnd = IdCompleto.substr(IdCompleto.search(sSearchingText), IdCompleto.length);
        IDEditado = sInit + IdInsertar + '__t';
    }
    return IDEditado
}
/*  Revisado por: blp. Fecha: 06/10/2011
''' Funci�n que vac�a el contenido del dropdown, los elementos seleccionados y los valores guardados en el entry 
''' dropdownID: Id del control a vaciar
''' Llamada desde: jsAlta.js. M�ximo < 0,1 seg
*/
function vaciarDropDown(dropdownID) {
    var dropdown = $find(dropdownID + '__t');
    //Vaciar texto
    if (dropdown) {
        if (dropdown.set_currentValue) {
            dropdown.set_currentValue('', true);
            dropdown.set_selectedItemIndex(-1);
            DeseleccionarDesactivarSeleccionado(dropdownID + '__t', true, true)
            var datos = dropdown.get_dataSource(); //As� se recuperan los datos cuando el control carga por jquery
            if (datos && datos.length > 0) { //Aqu� entra cuando el control tiene carga desde jquery (si no, oControl.get_dataSource() ser�a undefined)
                dropdown.set_dataSource(eval('[{"Valor":"","Texto":""}]'));
                dropdown.dataBind();
                datos = null;
            }
        }
        dropdown = null;
    }
    var dropdownEntry = fsGeneralEntry_getById(dropdownID)
    if (dropdownEntry) {
        dropdownEntry.setValue("");
        dropdownEntry.setDataValue("");
        dropdownEntry = null;
    }
}
/*  Revisado por: blp. Fecha: 06/10/2011
''' Busca el contenido del par�metro valorSeleccionado en los datos del webdropdown con ID idControlDestino. Si existe, lo selecciona 
''' dropdownID: Id del control en cuyos datos el que comprobar si existe el valorSeleccionado
''' valorSeleccionado: valor a buscar en el control y que se seleccionar� si se encuentra
''' Llamada desde: jsAlta.js. M�ximo < 0,1 seg
*/
function copiarSeleccionWebDropDown(idControlDestino, valorSeleccionado) {
    if (valorSeleccionado != '' && valorSeleccionado != 'undefined' && valorSeleccionado != null) {
        var oControl = $find(idControlDestino);
        if (oControl) {
            var datos = oControl.get_dataSource(); //As� se recuperan los datos cuando el control carga por jquery
            var k;
            if (datos) { //Aqu� entra cuando el control tiene carga desde jquery (si no, oControl.get_dataSource() ser�a undefined)
                if (datos.length > 0) {
                    for (k = 0; k < datos.length; ++k) {
                        if (datos[k].Text.substring(0, datos[k].Text.indexOf(" - ")) == valorSeleccionado) {
                            var iSelectedItemIndex = k;
                            CambiarSeleccion(oControl._id, iSelectedItemIndex);
                            break;
                        }
                    }
                } else {
                    var oControlEntry = fsGeneralEntry_getById(idControlDestino)
                    if (oControlEntry) {
                        var valorDefecto = oControlEntry.DataValorDefecto
                        if (valorSeleccionado == valorDefecto && oControlEntry.TextoValorDefecto != null) {
                            oControl.set_currentValue(oControlEntry.TextoValorDefecto, true)
                        }
                    }
                    oControlEntry = null;
                    valorDefecto = null;
                }
            }
            else { //cuando el control carga los datos desde servidor, o sea oControl.get_dataSource() es undefined
                datos = oControl.get_items(); //As� se recuperan los datos cuando el control carga desde servidor
                //Hay dos modos de recuperar los datos en esta situacion. controlaremos cu�ndo recuperarlos de una manera o de otra
                var modo = 1;
                var iLength = datos._items.length;
                if (datos.getLength() > datos._items.length) {
                    modo = 2;
                    iLength = datos.getLength();
                }
                if (datos && iLength > 0) {
                    for (var item = 0; item < iLength; item++) {
                        var dataValue = null;
                        var oCelda = null;
                        if (modo == 1) {
                            if (datos._items[item].get_element(0).children[0].rows != undefined)
                                oCelda = datos._items[item].get_element(0).children[0].rows[0].cells[0]

                            if (oCelda == null) { //tipoGS=0
                                dataValue = datos._items[item].get_value();
                            } else {
                                if (oCelda.innerText != undefined)
                                    dataValue = oCelda.innerText;
                                else
                                    dataValue = oCelda.textContent;
                            }
                        } else {
                            if (datos.getItem(item).get_element(0).children[0].rows != undefined)
                                oCelda = datos.getItem(item).get_element(0).children[0].rows[0].cells[0]

                            if (oCelda == null) { //tipoGS=0
                                dataValue = datos.getItem(item).get_value();
                            } else {
                                if (oCelda.innerText != undefined)
                                    dataValue = oCelda.innerText;
                                else
                                    dataValue = oCelda.textContent;
                            }
                        }
                        if (dataValue == valorSeleccionado) {
                            var iSelectedItemIndex = -1;
                            if (modo == 1) {
                                iSelectedItemIndex = datos._items[item].get_index();
                            } else {
                                iSelectedItemIndex = datos.getItem(item).get_index();
                            }
                            CambiarSeleccion(idControlDestino, iSelectedItemIndex);
                            break;
                        }
                    }
                }
            }
        }
    }
}
/*  Revisado por: blp. Fecha: 06/10/2011
''' Funci�n que devuelve un n�mero aleatorio dentro de un m�nimo y un m�ximo
''' Min: valor m�nimo
''' Max: valor m�ximo
''' Llamada desde: jsAlta.js. M�ximo < 0,1 seg
*/
function Aleatorio(Min, Max) {
    var random = parseInt(new String(Math.random()).split(".")[1].substring(0, new String(Max).length))
    while (random > Max && random < Min || random > Max && random > Min || random < Max && random < Min) {
        random = parseInt(new String(Math.random()).split(".")[1].substring(0, new String(Max).length))
    }
    return random
}
/*  Revisado por: blp. Fecha: 06/10/2011
''' Funci�n que devuelve la primera parte de un string en el que se busca el string textoBuscado.
''' Si no lo encuentra, devuelve el string completo
''' texto: texto en el que buscar el textoBuscado
''' textoBuscado: texto a bsucar
''' Llamada desde: webdropdown_datacheck. M�ximo < 0,1 seg
*/
function devolverCod(texto, textoBuscado) {
    sTexto = texto
    if (texto.indexOf(textoBuscado) > -1) {
        sTexto = texto.substr(0, texto.indexOf(textoBuscado))
    }
    return sTexto
}
/*
Revisado por: blp. Fecha: 03/10/2011
Funci�n que devuelve el alto o el ancho de la ventana
type: Valor del que depende que devuelva ancho o alto
Height: devuelve alto
Width: devuelve ancho
Llamada desde desglose.aspx --> resize()
*/
function getWindowSize(type) {
    var mySize = 0;
    if (type == "Height") {
        if (typeof (window.innerWidth) == 'number') {
            //Non-IE
            mySize = window.innerHeight;
        } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
            //IE 6+ in 'standards compliant mode'
            mySize = document.documentElement.clientHeight;
        } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
            //IE 4 compatible
            mySize = document.body.clientHeight;
        }
    }
    else {
        if (typeof (window.innerWidth) == 'number') {
            //Non-IE
            mySize = window.innerWidth;
        } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
            //IE 6+ in 'standards compliant mode'
            mySize = document.documentElement.clientWidth;
        } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
            //IE 4 compatible
            mySize = document.body.clientWidth;
        }
    }
    return mySize
}
/*
''' <summary>
''' Llamar Servicio Externo
''' </summary>
''' <param name="ClientID">Id del entry q es un servicio</param>
''' <param name="Instancia">Instancia en la q estas</param>        
''' <param name="Peticionario">se mete por compatibilidad del Entry Portal/FsnWeb. Pq aqui, Instancia>0 entonces se trae de bbdd el Instancia.Peticionario</param>   
''' <remarks>Llamada desde: GeneralEntry.vb ; Tiempo m�ximo: 0</remarks>*/
function llamarServicioExternoPorId(ClientID, Instancia, Peticionario) {
    var oEntry = fsGeneralEntry_getById(ClientID)
    llamarServicioExterno(oEntry, Instancia)
};
//function llamarServicioExternoPorEntry(oEntry) {
//     llamarServicioExterno(oEntry.fsEntry)
//};
/*
''' <summary>
''' Llamar Servicio Externo
''' </summary>
''' <param name="ClientID">Id del entry q es un servicio</param>
''' <param name="Instancia">Instancia en la q estas</param>   
''' <remarks>Llamada desde: llamarServicioExternoPorId ; Tiempo m�ximo: 0</remarks>*/
function llamarServicioExterno(oEdit, Instancia) {
    var respuesta;
    var campoId, lineaDesglose;
    var servicio = oEdit.Servicio;
    var valor = oEdit.getDataValue();
    if (oEdit.idLinea == "") { //Campo de fuera de desglose
        lineaDesglose = "0";
    } else {
        lineaDesglose = oEdit.idLinea;
    }
    if (oEdit.campo_origen == 0) { //Alta de formulario
        campoId = oEdit.idCampo;
    } else {
        campoId = oEdit.campo_origen;
    }
    //Saco la url y los parametros del servicio    
    params = {
        servicio: servicio,
        campoId: campoId,
        Instancia: Instancia,
        Peticionario: '',
        Empresa: 0,
        Proveedor: '',
        Contacto: 0,
        Moneda: '',
        FechaInicio: '',
        FechaExpiracion: '',
        Alerta: 0,
        RepetirEmail: 0,
        ImporteDesde: 0,
        ImporteHasta: 0
    }
    $.when($.ajax({
        type: "POST",
        url: '../../Consultas.asmx/Obtener_Servicio',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        respuesta = msg.d;
    });

    var sUrl = respuesta.Url;
    var oEntrada = respuesta.ParamsEntrada;
    var oSalida = respuesta.ParamsSalida;
    var Peticionario = respuesta.Peticionario;
    var nombreResult;
    for (var key in oSalida) {
        if (oSalida[key].IndError == 1) {
            nombreResult = oSalida[key].Den;
        }
    }

    //El campo servicio puede estar en campo, desglose no popup y desglose popup. Si es popup los param entrada pueden estar fuera desglose.
    var EsPopUp;
    EsPopUp = 0;
    p = window.opener;
    if (p) {
        EsPopUp = 1;
    }

    //llamar al webservice con el valor del par�metro de entrada introducido y recojo la respuesta
    params = "{"
    $.each($.map(oEntrada, function (parametro) {
        return parametro
    }), function () {
        params += "'" + this.Den + "':'";
        if (this.Directo == 1) { //Directo
            switch (this.Tipo) {
                case 2: //numero
                    params += this.valorNum;
                    break;
                case 3: //fecha
                    params += this.valorFec;
                    break;
                case 4: //boolean
                    params += this.valorBool;
                    break;
                case 6: //texto
                    params += this.valorText;
                    break;
            }
        }
        else { //No Directo
            switch (this.TipoCampo) {
                case 0:
                case 1: //0-1 campo Formulario
                    if (this.EsSubcampo == 0) {
                        //param entrada No en desglose Y Campo servicio S� en desglose -> buscar fuera.
                        lineaDesglose = "0";
                    } else {
                        lineaDesglose = oEdit.idLinea;
                    }
                    if (EsPopUp == 0 || this.EsSubcampo == 1) {
                        for (arrInput in arrInputs) {
                            if (arrInputs.hasOwnProperty(arrInput)) {
                                oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
                                if (oEntry) {
                                    Dato = DameDatoParamServicio(oEntry, lineaDesglose, this.Campo);
                                    if (Dato != '###NODATO###') {
                                        params += Dato;
                                        break;
                                    }
                                }
                            }
                        }
                    } else { //PopUp y param entrada es campo No de desglose.Buscar en padre.
                        for (arrInput in p.arrInputs) {
                            if (arrInputs.hasOwnProperty(arrInput)) {
                                oEntry = p.fsGeneralEntry_getById(p.arrInputs[arrInput]);
                                if (oEntry) {
                                    Dato = DameDatoParamServicio(oEntry, lineaDesglose, this.Campo);
                                    if (Dato != '###NODATO###') {
                                        params += Dato;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    break;
                case 2: //Peticionario
                    params += respuesta.ParamsCamposGenericos.Peticionario.valorText;
                    break;
                case 11: //Moneda
                    params += respuesta.ParamsCamposGenericos.Moneda.valorText;
                    break;
                case 12: //Proveedor
                    params += respuesta.ParamsCamposGenericos.Proveedor.valorText;
                    break;
                case 13: //Contacto
                    params += respuesta.ParamsCamposGenericos.Contacto.valorNum;
                    break;
                case 14: //Empresa
                    params += respuesta.ParamsCamposGenericos.Empresa.valorNum;
                    break;
                case 15: //Fecha de inicio
                    params += respuesta.ParamsCamposGenericos.FechaInicio.valorFec;
                    break;
                case 16: //Fecha de expiracion
                    params += respuesta.ParamsCamposGenericos.FechaExpiracion.valorFec;
                    break;
                case 17: //Mostrar alerta
                    params += respuesta.ParamsCamposGenericos.MostrarAlerta.valorNum;
                    break;
                case 18: //Enviar email
                    params += respuesta.ParamsCamposGenericos.RepetirMail.valorNum;
                    break;
                case 19: //Importe desde
                    params += respuesta.ParamsCamposGenericos.ImporteDesde.valorNum;
                    break;
                case 20: //Importe hasta
                    params += respuesta.ParamsCamposGenericos.ImporteHasta.valorNum;
                    break;
            }
        }
        params += "',";
    });

    params = params.substring(0, params.length - 1);
    params += "}";    
    $.when($.ajax({
        type: "POST",
        url: sUrl,
        contentType: "application/json; charset=utf-8",
        data: params,
        dataType: "json",
        async: false
    })).done(function (msg) {
        respuesta = msg.d;
    });

    if (respuesta[nombreResult] != "") {
        alert(respuesta[nombreResult]);
        oEdit.Editor.elem.className = "TipoTextoMedioRojo";
        for (var key in oSalida) {
            if (oSalida[key].IndError == 0) {
                var nomParam = oSalida[key].Den;
                var IDCampo = oSalida[key].Campo;

                //vaciar los valores de salida en sus correspondientes campos
                for (arrInput in arrInputs) {
                    if (arrInputs.hasOwnProperty(arrInput)) {
                        oEntry = fsGeneralEntry_getById(arrInputs[arrInput]);
                        if (oEntry) {
                            if (lineaDesglose == "0" || (lineaDesglose != "0" && oEntry.idLinea == lineaDesglose)) {
                                if (oEntry.campo_origen == 0) { //Alta de formulario
                                    if (oEntry.idCampo == IDCampo.toString()) {
                                        oEntry.setValue("");
                                        oEntry.setDataValue("");
                                    }
                                } else {
                                    if (oEntry.campo_origen == IDCampo.toString()) {
                                        oEntry.setValue("");
                                        oEntry.setDataValue("");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        //Cojo los valores y los guardo en los campos correspondientes
        oEdit.Editor.elem.className = "TipoTextoMedioVerde";
        for (var key in oSalida) {
            if (oSalida[key].IndError == 0) {
                var nomParam = oSalida[key].Den;
                var IDCampo = oSalida[key].Campo;
                var salidaParam = respuesta[nomParam];

                //meter los valores de salida en sus correspondientes campos
                //for (var i = 0; i < arrInputs.length; i++) {
                for (arrInput in arrInputs) {
                    if (arrInputs.hasOwnProperty(arrInput)) {
                        oEntry = fsGeneralEntry_getById(arrInputs[i]);
                        if (oEntry) {
                            if (lineaDesglose == "0" || (lineaDesglose != "0" && oEntry.idLinea == lineaDesglose)) {
                                if (oEntry.campo_origen == 0) { //Alta de formulario
                                    if (oEntry.idCampo == IDCampo.toString()) {
                                        oEntry.setValue(salidaParam);
                                        oEntry.setDataValue(salidaParam);
                                    }
                                } else {
                                    if (oEntry.campo_origen == IDCampo.toString()) {
                                        oEntry.setValue(salidaParam);
                                        oEntry.setDataValue(salidaParam);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
};
/*  Revisado por: blp. Fecha: 03/10/2011
''' M�todo que se lanza en el evento de texto cambiado en el webdropdown
''' En �l, comprobamos si la medida tomada es vaciar el contenido. 
''' Si es as�, comprobamos si hay alg�n elemento seleccionado y lo deseleccionamos 
''' y vaciamos el valor del control entry
''' sender: Control webdropdown que lanza el evento
''' e: argumentos del evento
''' Llamada desde: el evento
''' Tiempo m�ximo inferior a 1 seg.*/
function WebDropDown_valueChanged(sender, e) {
    if (e.getNewValue() == "") {
        if (sender.get_selectedItem() != null && sender.get_selectedItem() != undefined) {
            DeseleccionarDesactivarSeleccionado(sender._id, true, true)
            sender.set_selectedItemIndex(-1);
        }
        var dropdownEntry = fsGeneralEntry_getById(sender._id)
        if (dropdownEntry) {
            dropdownEntry.setValue("");
            dropdownEntry.setDataValue("");
            dropdownEntry = null;
            var oEdit = fsGeneralEntry_getById(sender._id);
            switch (oEdit.tipoGS) {
                case 109:
                    //Destinos
                    document.getElementById("igtxt" + oEdit.Editor2.ID).style.visibility = "hidden";
                    break;

            }
        }
    }
}
/*  Revisado por: blp. Fecha: 03/10/2011
''' M�todo que deselecciona y/o desactiva el elemento seleccionado, si lo hay
''' de modo que CAMBIA EL CSS del elemento seleccionado, que es lo que perseguimos con esta funci�n
''' idControl: ID del control a tratar
''' deseleccionar: si es true, deseleccionaremos el item
''' desactivar: si es true, desactivaremos el item
''' Llamada desde: CambiarSeleccion, CambiarSeleccionValue, WebDropDown_valueChanged
''' Tiempo m�ximo inferior a 0,1 seg.*/
function DeseleccionarDesactivarSeleccionado(idControl, deseleccionar, desactivar) {
    var oControl = $find(idControl)
    if (oControl) {
        if (oControl.get_selectedItem() != null && oControl.get_selectedItem() != undefined) {
            if (deseleccionar) {
                oControl.get_selectedItem().unselect()
            }
            if (desactivar) {
                oControl.get_selectedItem().inactivate()
            }
        }
    }
}
/*  Revisado por: blp. Fecha: 03/10/2011
''' La funci�n devuelve un array con los datos del doctype
''' Llamada desde:popupDesgloseClickEvent
''' M�x 0,1 seg
*/
function detectDoctype() {
    var re = /\s+(X?HTML)\s+([\d\.]+)\s*([^\/]+)*\//gi;
    var res = false;
    /*********************************************
    Just check for internet explorer.
    **********************************************/
    if (typeof document.namespaces != "undefined")
        res = document.all[0].nodeType == 8 ?
                    re.test(document.all[0].nodeValue) : false;
    else
        res = document.doctype != null ?
                    re.test(document.doctype.publicId) : false;
    if (res) {
        res = new Object();
        res['xhtml'] = RegExp.$1;
        res['version'] = RegExp.$2;
        res['importance'] = RegExp.$3;
        return res;
    } else {
        return null;
    }
}
/*  Revisado por: blp. Fecha: 03/10/2011
''' M�todo para calcular el scrolltop cuando cambiamos de doctype a XHTML
''' Devuelve la altura a la que se encuentra el elemento respecto del scroll en pantalla
''' Llamada desde:popupDesgloseClickEvent
''' M�x 0,1 seg
*/
function getScrollTop() {
    var scrollTop = 0
    if (self.pageYOffset) {
        // all except Explorer
        scrollTop = self.pageYOffset;
    }
    else if (document.documentElement && document.documentElement.scrollTop) {
        // Explorer 6 Strict
        scrollTop = document.documentElement.scrollTop;
    }
    else if (document.body) {
        // all other Explorers
        scrollTop = document.body.scrollTop;
    }
    return scrollTop
}
//funcion que decodifica unos caracteres especiales
function DecodeJSText(Entrada) {
    Entrada = replaceAll(Entrada, "&lt;", "<")
    Entrada = replaceAll(Entrada, "&gt;", ">")

    return Entrada
}
function fecha_invalida(oTxt, sValor, oEvent) {
    var Fecha = new Date(sValor.year, sValor.month - 1, sValor.day);
    if (Fecha > oTxt.getMaxValue()) {
        alert("Max = \n" + formatDate(oTxt.getMaxValue(), vDateFmt));
    }
}
function addZero(vNumber) {
    return ((vNumber < 10) ? "0" : "") + vNumber
}
function formatDate(vDate, vFormat) {
    var vDay = addZero(vDate.getDate());
    var vMonth = addZero(vDate.getMonth() + 1);
    var vYearLong = addZero(vDate.getFullYear());
    var vYearShort = addZero(vDate.getFullYear().toString().substring(3, 4));
    var vYear = (vFormat.indexOf("yyyy") > -1 ? vYearLong : vYearShort)
    var vHour = addZero(vDate.getHours());
    var vMinute = addZero(vDate.getMinutes());
    var vSecond = addZero(vDate.getSeconds());
    var vDateString = vFormat.replace(/dd/g, vDay).replace(/MM/g, vMonth).replace(/y{1,4}/g, vYear)
    vDateString = vDateString.replace(/hh/g, vHour).replace(/mm/g, vMinute).replace(/ss/g, vSecond)
    return vDateString
}
function MontarFormularioAlta() {
    if (document.forms["frmAlta"])
        oFrm = document.forms["frmAlta"]
    else
        oFrm = document.forms["frmDetalle"]


    for (i = 0; i < arrCamposDesglose.length; i++) //Array de campos de tipo desglose
    {
        oEntry = fsGeneralEntry_getById(arrCamposDesglose[i])
        oDiv = oEntry.Editor

        oFrmDesglose = document.forms["frmDesglose"]

        sInner = oDiv.innerHTML
        sInner = replaceAll(sInner, ":", "_")
        oDiv.innerHTML = sInner

        if (oFrm.elements["nomDesglose"])
            oFrm.elements["nomDesglose"].value = oEntry.id
        else
            oFrm.insertAdjacentHTML("beforeEnd", "<input id=nomDesglose type=hidden name =nomDesglose value=" + fsGeneralEntry_getById(arrCamposDesglose[i]).id + ">")
    }

    sInput = "<input type=hidden name=txtNumDesgloses>"
    document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", sInput)
    oFrm.elements["txtNumDesgloses"].value = arrDesgloses.length

    for (i = 0; i < arrDesgloses.length; i++) {
        sNumRows = replaceAll(arrDesgloses[i], "_tblDesglose", "_numRows")
        sInput = "<input type=hidden name=nomDesglose_" + i.toString() + " value ='" + arrDesgloses[i] + "'>"
        document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", sInput)
        sInput = "<input type=hidden name=" + arrDesgloses[i] + ">"
        document.getElementById("divAlta").insertAdjacentHTML("beforeEnd", sInput)
        oTbl = document.getElementById(arrDesgloses[i])
        oFrm.elements[arrDesgloses[i]].value = document.getElementById(sNumRows).value
    }
}
function comprobarSiParticipantes() {

    for (var i = 0; i < arrObligatorios.length; i++) {
        if (arrObligatorios[i] != "deleted") {
            oEntry = fsGeneralEntry_getById(arrObligatorios[i])
            if (oEntry) {
                if (oEntry.basedesglose == false) {
                    if (oEntry.tipo == 9) {
                        //uwtGrupos__ctl0x_922_fsentry9339__numRows
                        numRows = document.getElementById(arrObligatorios[i] + "__numRows").value
                        if (numRows == 0)
                            return false
                    }
                    else {
                        if (oEntry.tipoGS == 100 || oEntry.tipoGS == 102 || oEntry.tipoGS == 110 || oEntry.tipoGS == 111 || oEntry.tipoGS == 112 || oEntry.tipoGS == 113 || oEntry.tipoGS == 116)
                            v = oEntry.getDataValue()
                        else
                            if (oEntry.Editor) {
                                if ((oEntry.tipoGS == 8) || (oEntry.tipo == 8)) {
                                    return false
                                }
                                if (oEntry.Editor.elem)
                                    v = oEntry.Editor.elem.value
                                else
                                    v = null
                                if (v == null || v == "")
                                    if (oEntry.tipoGS == 104) {
                                        v = oEntry.EditorDen.elem.value
                                        if (v == null || v == "")
                                            return false
                                    }
                                    else
                                        if (oEntry.tipoGS == 115 || oEntry.tipoGS == 116) {
                                            if (oEntry.intro == 1) {
                                                return true
                                            }
                                            else {
                                                return true
                                            }
                                        }
                                        else
                                            if (oEntry.tipoGS == 117) //Rol
                                            {
                                            }
                                            else

                                                if (oEntry.tipoGS == 110 || oEntry.tipoGS == 111 || oEntry.tipoGS == 112 || oEntry.tipoGS == 113) {
                                                    return false

                                                }
                                                else
                                                    return false
                            }
                            else
                                return false
                    }
                }
            }
            else {

                sDesglose = replaceAll(arrObligatorios[i], "OBLIG_", "")
                if (document.getElementById(sDesglose + "_numRowsGrid")) {
                    //uwtGrupos__ctl0x_922_922_9339_numRowsGrid
                    numRows = document.getElementById(sDesglose + "_numRowsGrid").value
                    if (numRows == 0)
                        return false
                }
                else {

                    sDesglose = replaceAll(arrObligatorios[i], "_OBLDESG_", "")
                    if (document.getElementById(sDesglose + "_numRowsGrid")) {

                        //uwtGrupos__ctl0x_922_922_9339_numRowsGrid
                        numRows = document.getElementById(sDesglose + "_numRowsGrid").value
                        if (numRows == 0)
                            return false
                    }
                    else {

                        sDesglose = arrObligatorios[i]
                        sDesglose = sDesglose.split("__")
                        idCampo = sDesglose[sDesglose.length - 1]
                        sDesglose = replaceAll(arrObligatorios[i], "__" + idCampo, "")
                        iNumRows = document.getElementById(sDesglose + "__numRows").value
                        iNumTotRows = document.getElementById(sDesglose + "__numTotRows").value
                        for (k = 1; k <= iNumTotRows; k++) {
                            if (document.getElementById(sDesglose + "__" + k.toString() + "__" + idCampo)) {
                                v = document.getElementById(sDesglose + "__" + k.toString() + "__" + idCampo).value
                                if (v == null || v == "")
                                    return false

                            }
                        }
                    }
                }
            }
        }
    }
    return true
}
/*
''' <summary>
''' Las lineas rechazadas q cambien deben pasar a sin revisar
''' </summary>
''' <param name="oEdit">objeto q ha podido cambiar </param>
''' <param name="text">text del objeto</param>        
''' <param name="oEvent">Evento</param>        
''' <remarks>Llamada desde: mat_seleccionado	validarLengthStringLargo	dateCalendar.js/calendarDateClickedEvent
'''		dropdrown.js/ddcellClickEvent; Tiempo m�ximo: 0</remarks>*/
function fCambioNc(oEdit, text, oEvent) {
    if (oEdit.fsEntry.tipo == 3) {
        if (!ddFec_ValueChange(oEdit, oEdit.old, oEvent))
            return false
    }
    var Comparar = 11
    try {
        var arrDesglose = oEdit.ID.split("_")
    }
    catch (e) {
        try {
            var arrDesglose = oEdit.id.split("_")
        }
        catch (e) {
            var arrDesglose = oEdit.split("_")
            Comparar = 9
        }
    }

    try {

        if (arrDesglose.length == Comparar) {
            var idGrupo = arrDesglose[3]
            var padre = arrDesglose[5]
            var index = arrDesglose[7]

            var desglose = arrDesglose[0] + "__" + arrDesglose[2] + "_" + idGrupo.toString() + "_" + idGrupo.toString() + "_" + padre.toString()
        }
        else {
            if (arrDesglose.length == 9) {
                var idGrupo = arrDesglose[3]
                var index = arrDesglose[5]

                var desglose = arrDesglose[0] + "__" + arrDesglose[2] + "_" + idGrupo.toString()
            }
            else {
                //6
                var idGrupo = ""
                var index = arrDesglose[2]

                var desglose = arrDesglose[0]
            }
        }

        oEntry = fsGeneralEntry_getById(desglose + "_fsdsentry_" + index.toString() + "_" + idGrupo.toString() + "1010")

        if (oEntry.getDataValue() == 3) //Rechazada
        {
            var arrEstado
            for (arrEstado in arrEstadosInternos)
                if (arrEstadosInternos[arrEstado][0] == 1)
                    break;

            oEntry.setValue(arrEstadosInternos[arrEstado][1])
            oEntry.setDataValue(1)

            if (document.getElementById(desglose + "_cmdFinAccion_" + index.toString()))
                document.getElementById(desglose + "_cmdFinAccion_" + index.toString()).style.visibility = "visible"

            //HA HABIDO UN CAMBIO DE ESTADO POR LO QUE HAY QUE NOTIFICAR
            if (document.getElementById("Notificar"))
                document.getElementById("Notificar").value = 1
        }
    }
    catch (e) { }

}
//Muestra la pantalla de detalle de factura al pulsar desde el campo de Factura(CampoGS=138)
function show_detalle_factura(idEntry) {
    var oEntry = fsGeneralEntry_getById(idEntry)
    if (oEntry) {
        if (oEntry.Dependent.value != -1) {
            var newWindow = window.open("../facturas/DetalleFactura.aspx?ID=" + oEntry.Dependent.value + "&DesdePM=1", "Factura", "width=910,height=600,status=yes,resizable=yes,top=20,left=100,scrollbars=yes")
            newWindow.focus()
        }
    }
}
/*''' <summary>
''' Abre la ventana del detalle de un destino para un dataentry de tipo destino
''' </summary>
''' <param name="oEdit">Dataentry</param>
''' <param name="oldValue">Valor que tenia el entry anteriormente</param> 
''' <param name="oEvent">Evento click</param>         
''' <remarks>Llamada desde:javascript introducido para responder al onclick de todos los campos de tipo Destino; Tiempo m�ximo:0</remarks>*/
function showDetalleDestino(oEdit, text, oEvent) {
    var valor;
    var popup = $('#PnlDestInfo');
    if (oEdit == null) {
        valor = text;
    }
    else {
        var oEntry = oEdit.fsEntry;
        valor = oEntry.getDataValue();
    };
    var params = { sID: valor }
    $.when($.ajax({
        type: "POST",
        url: '../../Consultas.asmx/CargarDatosDestinos',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function (msg) {
        var rDestino = msg.d[0];
        var rTextos = msg.d[1];
        $.when($.get('../../script/_common/html/DetalleDestino.htm', function (detalleDestPopUp) {
            $('body').append(detalleDestPopUp);
        })).done(function () {
            //Poner textos de label
            $('#lblDestInfoDir').text(rTextos[0]);
            $('#lblDestInfoCP').text(rTextos[1]);
            $('#lblDestInfoPob').text(rTextos[2]);
            $('#lblDestInfoProv').text(rTextos[3]);
            $('#lblDestInfoPais').text(rTextos[4]);
            $('#lblDestInfoTfno').text(rTextos[5]);
            $('#lblDestInfoFAX').text(rTextos[6]);
            $('#lblDestInfoEmail').text(rTextos[7]);
            $('#lblTitPpalDen').text(rTextos[8]);
            $('#lblSubTit').text(rTextos[9]);
            //Poner valores del destino
            $('#lblDestInfoTituloCod').text(rDestino.Den);
            $('#txtDestInfoDir').text(rDestino.Direccion);
            $('#txtDestInfoCP').text(rDestino.CP);
            $('#txtDestInfoPob').text(rDestino.Poblacion);
            $('#txtDestInfoProv').text(rDestino.Provincia);
            $('#txtDestInfoPais').text(rDestino.Pais);
            $('#txtDestInfoTfno').text(rDestino.Tfno);
            $('#txtDestInfoFAX').text(rDestino.FAX);
            $('#txtDestInfoEmail').text(rDestino.Email);
            $('#ImgDestInfoCerrar').live('click', function () {
                $('#PnlDestInfo').hide();
                $('#popupFondo').hide();
            });
            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();
            $('#PnlDestInfo').css('height', '280px');
            $('#PnlDestInfo').css('width', '300px');
            $('#PnlDestInfo').css('overflow', 'none');
            CentrarPopUp($('#PnlDestInfo'));
            $('#PnlDestInfo').show();
        });
    });
};
function factura_keydown(oEdit, text, oEvent) {
    if (oEvent.event.keyCode == 8 || oEvent.event.keyCode == 46 || oEvent.event.keyCode == 32) {
        if (oEdit.fsEntry) {
            var oEntry = oEdit.fsEntry
            if (oEntry) {
                oEntry.setValue('')
                oEntry.setDataValue('')
                oEntry.Dependent.value = -1
            }
        }
    }
}
function Cantidad_ValueChange(oEdit, oldValue, oEvent) {

    var cant = 0;
    if (oEdit.fsEntry.TipoRecepcion == "1") {
        var smensaje1, smensaje2;
        cant = oEdit.fsEntry.getValue();
        if (cant > 1) {
            oEdit.fsEntry.setValue('1');
            alert(smensajeRecep);
        }
    }
}
/*
''' <summary>
''' Comprobar si el entry q se poporciona es el campo de donde se quiere sacar el param de entrada
''' </summary>
''' <param name="oEntry">Entry donde busco</param>
''' <param name="lineaDesglose">Linea donde busco</param>        
''' <param name="CampoId">Id del Campo que busco</param>   
''' <remarks>Llamada desde: llamarServicioExterno ; Tiempo m�ximo: 0</remarks>*/
function DameDatoParamServicio(oEntry, lineaDesglose, CampoId) {
    var Dato;

    Dato = '###NODATO###'

    if (lineaDesglose == "0" || (lineaDesglose != "0" && oEntry.idLinea == lineaDesglose)) {
        if (oEntry.campo_origen == 0) { //Alta de formulario
            if (oEntry.idCampo == CampoId) {
                Dato = oEntry.getDataValue();
            }
        } else {
            if (oEntry.campo_origen == CampoId) {
                Dato = oEntry.getDataValue();
            }
        }
    }

    return Dato
}
/* Valida el formato de un dataentry de tipo texto corto (sin recordar valores) */
function validarFormatoTextoCorto(oEdit, oldValue, oEvent) {
    var urlregex = new RegExp(oEdit.fsEntry.Formato, "i");
    if (urlregex.test(oEdit.fsEntry.getValue())) {
        oEdit.elem.className = "TipoTextoMedioVerde";
    } else {
        oEdit.elem.className = "TipoTextoMedioRojo";
        alert(oEdit.fsEntry.title + '. ' + oEdit.fsEntry.errMsgFormato);
    }
}
/* Valida el formato de un dataentry de tipo texto corto (con recordar valores) y texto medio y largo */
function validarFormato(oCampo, formato, msg) {
    var urlregex = new RegExp(formato, "i");
    if (urlregex.test(oCampo.value)) {
        oCampo.className = "TipoTextoMedioVerde";
    }
    else {
        oCampo.className = "TipoTextoMedioRojo";
        alert(msg);
    }
};
//<summary>Busca el valor de un campo (primera coincidencia) a nivel de formulario</summary>
//<param name="tipoCampo">Tipo de campo GS a buscar</param>
function buscarCampoEnFormulario(tipoCampo) {
    var p = window;
    if (window.opener) p = window.opener;
    for (arrInput in p.arrInputs) {
        oEntry = p.fsGeneralEntry_getById(p.arrInputs[arrInput]);
        if (oEntry) {
            if (!oEntry.isDesglose) //Input perteneciente al formulario
            {
                if (!oEntry.basedesglose) { //Si isDesglose es false pero basedesglose es true es parte del desglose_hidden
                    if (oEntry.tipoGS == tipoCampo) {
                        return oEntry;
                    }
                }
            }
        }
    }
};
function show_companies(oEntry, text, event) {
    var newWindow = window.open(rutanormal + "script/_common/BuscadorEmpresas.aspx?IdControl=" + oEntry[0].id + "&opener=desdeSolicitud", "_blank", "width=835,height=635,status=yes,resizable=yes,top=0,left=150,scrollbars=yes")
    newWindow.focus();
};
function SeleccionarEmpresaSolicitud(idControl, idEmpresa, nombreEmpresa, nifEmpresa) {
    o = fsGeneralEntry_getById(idControl);
    o.setValue(nifEmpresa + ' - ' + nombreEmpresa);
    o.setDataValue(idEmpresa);
};
function onEmpresaItemSelected(sender, e) {
    if (e._text != null) {
        var oEdit = $('#' + sender._element.id.substring(0, sender._element.id.length - 2));
        oEdit[0].fsEntry.setValue(e._text);
        oEdit[0].fsEntry.setDataValue(e._value);
    }
};
//Funci�n que recoloca la posicion de la caja de valores del autocompleteextender en caso de scroll de la pagina 
function resetPosition(object, args) {
    var tb = object._element;
    var tbposition = findPositionWithScrolling2(tb);

    var xposition = tbposition[0];
    var yposition = tbposition[1] + 17; // 17 = textbox height + a few pixels spacing

    var ex = object._completionListElement;
    if (ex) {
        $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
    }
};
//Funci�n que retorna la posicion en la pagina del objeto
function findPositionWithScrolling2(oElement) {
    if (typeof (oElement.offsetParent) != 'undefined') {
        var originalElement = oElement;
        var oOffsetParent;
        //CALIDAD
        for (var posX = 0, posY = 0; oElement; oElement = oElement.parentElement) {
            if (oElement == oOffsetParent) {
                posX += oElement.offsetLeft;
                posY += oElement.offsetTop;
            }
            if (oElement != originalElement && oElement != document.body && oElement != document.documentElement) {
                posX -= oElement.scrollLeft;
                posY -= oElement.scrollTop;
            }
            if (oElement.offsetParent && oElement.offsetParent != '') oOffsetParent = oElement.offsetParent;
        }
        return [posX, posY];

    } else {
        return [oElement.x, oElement.y];
    }
};