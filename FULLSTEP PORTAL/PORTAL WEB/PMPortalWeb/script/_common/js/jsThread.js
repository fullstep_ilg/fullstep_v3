﻿function OcultaCol(key) {
    var column,i
    var grid = $find("wdg");

    for (i = 0; i < grid.get_columns().get_length(); i++) {
        column = grid.get_columns().get_column(i)
        if (column.get_key().split("_")[0] == key) {
            column.set_hidden(false)
        }
    }
    OcultarModalCargando()      
}
