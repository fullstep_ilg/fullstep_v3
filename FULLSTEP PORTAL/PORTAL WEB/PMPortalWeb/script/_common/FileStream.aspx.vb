﻿Imports System.Data.SqlClient
Imports System.Data.SqlTypes
Imports System.IO
Imports System.Security.Principal

Public Class FileStream
    'Inherits System.Web.UI.Page
    Inherits FSPMPage

    Dim anyo As Long
    Dim gmn1 As String
    Dim proce As Long
    Dim lCiaComp As Long
    Dim op As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        anyo = Request("anyo")
        gmn1 = Request("gmn1")
        proce = Request("proce")
        lCiaComp = Request("lCiaComp")
        op = Request("op")
        DescargarFileStream(anyo, gmn1, proce, lCiaComp, op)
    End Sub



    ''' <summary>
    ''' Procedimiento que descarga un fichero almacenado en SQL en formato fileStream
    ''' </summary>
    ''' <param name="anyo">Año del proceso</param>
    ''' <param name="gmn1">Grupo de Material del Nivel 1 del proceso</param>
    ''' <param name="proce">Identificador del proceso</param>
    ''' <param name="lCiaComp">Identificador de compañía en el portal</param>
    ''' <remarks>
    ''' Llamada desde: 
    ''' Tiempo máximo: </remarks>
    Private Sub DescargarFileStream(ByVal anyo As Long, ByVal gmn1 As String, ByVal proce As Long, ByVal lCiaComp As Long, ByVal op As String, Optional ByVal file As String = "")
        ' Los adjuntos se encuentran en la Base de datos en formato filestream
        ' Para acceder a su descarga es necesaria la autenticación windows, por lo que hay que realizar Impersonalizacion-Suplantación

        ' Contexto de Suplantación
        Dim oAdjunto As Fullstep.PMPortalServer.Adjunto
        oAdjunto = FSPMServer.Get_Adjunto
        Dim impersonationContext As WindowsImpersonationContext
        FSPMServer.Impersonate()
        impersonationContext = RealizarSuplantacion(FSPMServer.ImpersonateLogin, FSPMServer.ImpersonatePassword, FSPMServer.ImpersonateDominio)

        Dim buffer As Byte()
        Dim filename As String

        Dim oDict As Fullstep.PMPortalServer.Dictionary = FSPMServer.Get_Dictionary()
        Dim msIdi As String

        msIdi = FSPMUser.Idioma

        oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.FileStream, msIdi)
        Dim oTextos As DataTable = oDict.Data.Tables(0)

        Select Case LCase(op)
            Case "reglassubasta"
                'Se obtiene el fichero en forma de buffer y además se obtiene el nombre del archivo (byref filename)
                buffer = oAdjunto.LeerReglasSubastaProce(anyo, gmn1, proce, lCiaComp, filename)
                If buffer Is Nothing Then
                    Call Me.Response.Write("<script>alert('" & oTextos.Rows.Item(0).Item(1) & "')</script>")
                    'No hay documentos disponibles para su descarga
                    Exit Sub
                End If
            Case Else
                buffer = Nothing
        End Select

        'y se manda al explorador
        MostrarAdjunto(buffer, filename)

        'Fin de la impersonalización
        If Not impersonationContext Is Nothing Then
            impersonationContext.Undo()
        End If

        oAdjunto = Nothing

    End Sub


    ''' <summary>
    ''' Procedimiento que envía un archivo en forma de buffer al explorador
    ''' </summary>
    ''' <param name="buffer">Buffer de bytes del archivo</param>
    ''' <param name="filename">Nombre para su desacrga a disco</param>
    ''' <remarks>
    ''' Llamada desde: DescargarFileStream
    ''' Tiempo máximo: 0</remarks>

    Private Sub MostrarAdjunto(ByVal buffer As Byte(), ByVal filename As String)

        If buffer.Length <> 0 Then
            Call Me.Response.AddHeader("Content-Type", "application/octet-stream")
            Call Me.Response.AddHeader("Content-Disposition", "attachment;filename=" & filename)
            Call Me.Response.BinaryWrite(buffer)
            Call Me.Response.End()
        End If

    End Sub



End Class