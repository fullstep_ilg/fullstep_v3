﻿Public Class BuscadorCentrosCoste
    Inherits FSPMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CentrosCoste.urlImagenCerrar = "~/App_Themes/" & Me.Page.Theme & "/images/Cerrar.gif"
        CentrosCoste.urlImagenCentrosCoste = System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "UserControls/images/centrocoste.JPG"
        idHidControl.Value = Request("idHidControl")
        idControl.Value = Request("idControl")
        If Not IsPostBack Then
            CargarDatosCentrosCoste()

            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.CentrosCoste
            CentrosCoste.Textos = TextosModuloCompleto
        End If
    End Sub
    ''' <summary>
    ''' Pasa al userControl de centros de coste los datos de los centros de coste
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarDatosCentrosCoste()
        Dim oCentros_SM As Fullstep.PMPortalServer.Centros_SM
        oCentros_SM = FSPMServer.Get_Centros_SM

        CentrosCoste.DatosTreeView = oCentros_SM.LoadData(IdCiaComp, "", Me.FSPMUser.Idioma)

    End Sub
End Class