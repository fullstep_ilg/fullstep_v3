Partial Class LinkMonedas
    Inherits FSPMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    ''' <summary>
    ''' Cargar la pagina. 
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>        
    ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
        Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
        Dim sIdi As String = oUser.Idioma
        Dim lCiaComp As Long = IdCiaComp

        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        '''''''''
        Dim oMonedas As Fullstep.PMPortalServer.Monedas = FSPMServer.Get_Monedas
        oMonedas.LoadData(sIdi, lCiaComp, , , True, True)

        Dim oGrid As Infragistics.WebUI.UltraWebGrid.UltraWebGrid
        oGrid = Me.uwgMoneda

        oGrid.DataSource = oMonedas.Data
        oGrid.DataBind()

        If oGrid.Bands.Count = 0 Then
            oGrid.Bands.Add(New Infragistics.WebUI.UltraWebGrid.UltraGridBand)
        End If

        Me.uwgMoneda.Columns.FromKey("COD").Hidden = True
        Me.uwgMoneda.Columns.FromKey("EQUIV").Hidden = True
        Me.uwgMoneda.Columns.FromKey("FECACT").Hidden = True
    End Sub

End Class
