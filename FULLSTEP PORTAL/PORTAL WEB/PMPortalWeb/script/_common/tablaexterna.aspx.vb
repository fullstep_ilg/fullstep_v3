Imports Infragistics.WebUI.UltraWebGrid

Namespace Fullstep.PMPortalWeb
    Partial Class tablaexterna
        Inherits FSPMPage

        Private _mlId As Long
        Private _msArt As String
        Private _moTablaExterna As Fullstep.PMPortalServer.TablaExterna
        Private _msIdi As String
        Public _msTitulo As String
        Private Const cdAnchoCaracter As Decimal = 7.5
        Private Const ciAnchoGrid As Integer = 680
        Private Const ciAnchoGridScroll As Integer = 663
        Private Const ciNumFilasScroll As Integer = 17
        Private lCiaComp As Long

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            lCiaComp = IdCiaComp
            _mlId = CLng(Request.QueryString("tabla"))
            If Request.QueryString("art").ToString() <> "" Then _
                _msArt = Request.QueryString("art").ToString()

            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")

            _moTablaExterna = FSWSServer.get_TablaExterna
            _moTablaExterna.LoadDefTabla(lCiaComp, _mlId, _msArt, True)
            _moTablaExterna.LoadData(lCiaComp, _msArt)

            _msTitulo = _moTablaExterna.Nombre

            _msIdi = oUser.Idioma
            If _msIdi = Nothing Then
                _msIdi = ConfigurationManager.AppSettings("idioma")
            End If

            If Not IsPostBack() Then
                Dim oDict As Fullstep.PMPortalServer.Dictionary = FSWSServer.Get_Dictionary()
                oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.TablaExterna, _msIdi)
                Dim oTextos As DataTable = oDict.Data.Tables(0)
                cmdAceptar.Text = oTextos.Rows(0).Item(1)

                Me.hddDataEntry.Value = Request.QueryString("idDataEntry")

                Dim oGrid As UltraWebGrid
                oGrid = Me.uwgTabla
                oGrid.DataSource = _moTablaExterna.Data
                oGrid.DataBind()
                FormatGrid(oGrid)
                SeleccionarValor(oGrid)
            End If
        End Sub
        ''' <summary>
        ''' Acepta la asignaci�n de Tablas Externas 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,2</remarks>
        Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
            Dim sValor As String = ""
            Dim sScript As String
            Dim sMostrar As String
            Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & _
                "<script language=""{0}"">{1}</script>"
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
            For Each oRow As UltraGridRow In Me.uwgTabla.Rows
                If oRow.Selected Then
                    For i As Integer = 0 To oRow.Cells.Count - 1
                        If UCase(oRow.Cells(i).Column.BaseColumnName) = UCase(_moTablaExterna.PrimaryKey) Then
                            sValor = oRow.Cells(i).Value.ToString()
                            sMostrar = _moTablaExterna.DescripcionReg(lCiaComp, oRow.Cells(i).Value, _msIdi, oUser.DateFormat, oUser.NumberFormat)
                            Exit For
                        End If
                    Next
                    Exit For
                End If
            Next
            sScript = "window.opener.valor_externa_seleccionado(document.forms[0].hddDataEntry.value, '" & JSText(sValor) & "', '" & JSText(sMostrar) & "', '" & JSText(IIf(_msArt Is Nothing, "", _msArt)) & "');" & ControlChars.CrLf _
                & "window.close();"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(),"ValorTablaExterna", sScript)
        End Sub

        ''' <summary>
        ''' Formatear el grid indicado
        ''' </summary>
        ''' <param name="oGrid">grid a formatear</param>
        ''' <remarks>Llamada desde:Page_Load; Tiempo m�ximo:0,1</remarks>
        Private Sub FormatGrid(ByVal oGrid As UltraWebGrid)
            Dim i As Integer
            Dim iAnchoGrid As Integer = 0
            Dim iAnchoGrid2 As Integer = 0
            Dim oColumn As UltraGridColumn
            Dim iMaxAncho As Integer
            oGrid.Width = Unit.Pixel(ciAnchoGrid + 2)
            oGrid.DisplayLayout.StationaryMargins = StationaryMargins.Header
            oGrid.DisplayLayout.UseFixedHeaders = True
            iMaxAncho = IIf(_moTablaExterna.DefTabla.Tables(1).Rows(0).Item("NUMFILAS") > ciNumFilasScroll, ciAnchoGridScroll, ciAnchoGrid)
            For Each oRow As System.Data.DataRow In _moTablaExterna.DefTabla.Tables(0).Rows
                For i = 0 To oGrid.Columns.Count - 1
                    If UCase(oGrid.Columns(i).BaseColumnName) = UCase(oRow.Item("CAMPO")) Then Exit For
                Next
                With oGrid.Columns(i)
                    If UCase(.BaseColumnName) = UCase(_moTablaExterna.ArtKey) Then
                        .Hidden = True
                    Else
                        .Header.Caption = oRow.Item("NOMBRE")
                        Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
                        If oRow.Item("TIPOCAMPO") = "FECHA" Then
                            .Format = oUser.DateFormat.ShortDatePattern
                            .Width = Unit.Pixel(CInt(oRow.Item("LONGITUD_CAMPO") * cdAnchoCaracter))
                        Else
                            .Width = Unit.Pixel(CInt(oRow.Item("LONGITUD_NOMBRE") * cdAnchoCaracter))
                        End If
                        If oRow.Item("TIPOCAMPO") = "NUMERICO" Then
                            .CellStyle.HorizontalAlign = HorizontalAlign.Right
                            .CellStyle.Padding.Right = Unit.Pixel(4)
                            If InStr(UCase(_moTablaExterna.Data.Tables(0).Columns(oRow.Item("CAMPO")).DataType.Name), "INT") > 0 Then
                                .Format = "#,##0"
                            Else
                                .Format = "#,##0.00"
                            End If
                        End If
                        iAnchoGrid += .Width.Value
                    End If
                End With

            Next
            If iAnchoGrid > iMaxAncho Then GoTo Ajustar

            For Each oColumn In oGrid.Columns
                If Not oColumn.Hidden Then
                    Dim oRow As System.Data.DataRow
                    For Each oRow In _moTablaExterna.DefTabla.Tables(0).Rows
                        If UCase(oRow.Item("CAMPO")) = UCase(oColumn.BaseColumnName) Then Exit For
                    Next
                    If oRow.Item("TIPOCAMPO") = "FECHA" Then
                        oColumn.Tag = oRow.Item("LONGITUD_CAMPO")
                    Else
                        oColumn.Tag = CInt(_moTablaExterna.DefTabla.Tables(1).Rows(0).Item("LONG_" & oColumn.BaseColumnName) * cdAnchoCaracter)
                    End If
                    If CInt(oColumn.Tag) < oColumn.Width.Value Then oColumn.Tag = oColumn.Width.Value
                    iAnchoGrid2 = iAnchoGrid2 + CInt(oColumn.Tag)
                End If
            Next
            If iAnchoGrid2 <= iMaxAncho Then
                iAnchoGrid = 0
                For Each oColumn In oGrid.Columns
                    If Not oColumn.Hidden Then
                        oColumn.Width = Unit.Pixel(CInt(oColumn.Tag))
                        iAnchoGrid += oColumn.Width.Value
                    End If
                Next
            Else
                Do While iAnchoGrid2 > iMaxAncho
                    Dim iCampos As Integer = 0
                    Dim iLongCampos As Integer = 0
                    For Each oColumn In oGrid.Columns
                        If Not oColumn.Hidden And CInt(oColumn.Tag) > oColumn.Width.Value Then
                            iCampos += 1
                            iLongCampos += CInt(oColumn.Tag)
                        End If
                    Next
                    If iCampos = 0 Then Exit Do
                    Dim dRec As Decimal = (iAnchoGrid2 - iMaxAncho) / iLongCampos
                    iAnchoGrid2 = 0
                    For Each oColumn In oGrid.Columns
                        If Not oColumn.Hidden Then
                            If CInt(CInt(oColumn.Tag) - (dRec * CInt(oColumn.Tag))) <= oColumn.Width.Value Then
                                oColumn.Tag = oColumn.Width.Value
                                iAnchoGrid2 += oColumn.Width.Value
                            Else
                                oColumn.Tag = CInt(CInt(oColumn.Tag) - (dRec * CInt(oColumn.Tag)))
                                iAnchoGrid2 += CInt(oColumn.Tag)
                            End If
                        End If
                    Next
                Loop
                iAnchoGrid = 0
                For Each oColumn In oGrid.Columns
                    If Not oColumn.Hidden Then
                        oColumn.Width = Unit.Pixel(CInt(oColumn.Tag))
                        iAnchoGrid += oColumn.Width.Value
                    End If
                Next
                If iAnchoGrid > iMaxAncho Then GoTo Ajustar
                Exit Sub
            End If
            If iAnchoGrid < iMaxAncho Then
                iAnchoGrid2 = 0
                For Each oRow As System.Data.DataRow In _moTablaExterna.DefTabla.Tables(0).Rows
                    For i = 0 To oGrid.Columns.Count - 1
                        If UCase(oGrid.Columns(i).BaseColumnName) = UCase(oRow.Item("CAMPO")) Then Exit For
                    Next
                    With oGrid.Columns(i)
                        If Not .Hidden Then
                            .Tag = CInt(oRow.Item("LONGITUD_CAMPO") * cdAnchoCaracter)
                            If CInt(.Tag) < .Width.Value Then .Tag = .Width.Value
                            iAnchoGrid2 += CInt(.Tag)
                        End If
                    End With
                Next
                If iAnchoGrid2 > iMaxAncho Then
                    Do While iAnchoGrid2 - iMaxAncho > 1
                        Dim iCampos As Integer = 0
                        Dim iLongCampos As Integer = 0
                        For Each oColumn In oGrid.Columns
                            If Not oColumn.Hidden And CInt(oColumn.Tag) > oColumn.Width.Value Then
                                iCampos += 1
                                iLongCampos += CInt(oColumn.Tag)
                            End If
                        Next
                        If iCampos = 0 Then Exit Do
                        Dim dRec As Decimal = (iAnchoGrid2 - iMaxAncho) / iLongCampos
                        iAnchoGrid2 = 0
                        For Each oColumn In oGrid.Columns
                            If Not oColumn.Hidden Then
                                If CInt(CInt(oColumn.Tag) - (dRec * CInt(oColumn.Tag))) <= oColumn.Width.Value Then
                                    oColumn.Tag = oColumn.Width.Value
                                    iAnchoGrid2 += oColumn.Width.Value
                                Else
                                    oColumn.Tag = CInt(CInt(oColumn.Tag) - (dRec * CInt(oColumn.Tag)))
                                    iAnchoGrid2 += CInt(oColumn.Tag)
                                End If
                            End If
                        Next
                    Loop
                    If iAnchoGrid2 > iMaxAncho Then
                        For Each oColumn In oGrid.Columns
                            If Not oColumn.Hidden Then
                                oColumn.Tag = CInt(oColumn.Tag) + iMaxAncho - iAnchoGrid2
                                iAnchoGrid2 = iMaxAncho
                                Exit For
                            End If
                        Next
                    End If
                End If
                iAnchoGrid = 0
                For Each oColumn In oGrid.Columns
                    If Not oColumn.Hidden Then
                        oColumn.Width = Unit.Pixel(CInt(oColumn.Tag))
                        iAnchoGrid += oColumn.Width.Value
                    End If
                Next
            End If

Ajustar:
            For Each oColumn In oGrid.Columns
                If Not oColumn.Hidden Then
                    oColumn.Width = Unit.Pixel(CInt(oColumn.Width.Value * iMaxAncho / iAnchoGrid))
                End If
            Next

        End Sub

        Private Sub SeleccionarValor(ByVal oGrid As UltraWebGrid)
            Dim sValorAnt As String = Trim(Request.QueryString("valor").ToString())
            Dim oRowReg As DataRow = _moTablaExterna.BuscarRegistro(lCiaComp, sValorAnt, _msArt, False)
            'Dim oValorKey As Object = _moTablaExterna.BuscarRegistro(lCiaComp, sValorAnt, False)
            Dim oValorKey As Object
            If Not oRowReg Is Nothing Then oValorKey = oRowReg.Item(_moTablaExterna.PrimaryKey)
            Dim iFilaSel As Integer = 0
            Dim sScript As String
            Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & _
                "<script language=""{0}"">{1}</script>"
            If Not oValorKey Is Nothing Then
                Dim iColClave As Integer
                For Each oCol As UltraGridColumn In oGrid.Columns
                    If UCase(oCol.BaseColumnName) = UCase(_moTablaExterna.PrimaryKey) Then
                        iColClave = oCol.Index
                        Exit For
                    End If
                Next
                For Each oRow As UltraGridRow In oGrid.Rows
                    If oRow.Cells(iColClave).Value = oValorKey Then
                        oRow.Selected = True
                        iFilaSel = oRow.Index
                    End If
                Next
            End If
            If iFilaSel >= ciNumFilasScroll Then
                sScript = "function scrollsel(){" & ControlChars.CrLf & _
                    "igtbl_getGridById('" & oGrid.ClientID & "').scrElem.scrollTop=" & _
                    (iFilaSel - ciNumFilasScroll) * 20 & ";" & ControlChars.CrLf & _
                    "}" & ControlChars.CrLf
                sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(),"scrollsel", sScript)
                sScript = "window.attachEvent('onload',scrollsel);" & ControlChars.CrLf
            End If
            sScript = sScript & "window.opener.valor_original_externa(document.forms[0].hddDataEntry.value);"
            sScript = sScript & ControlChars.CrLf & "window.opener.ventexterna=false;"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(),"ValorTablaExternaAct", sScript)
        End Sub

    End Class
End Namespace