<%@ Page Language="vb" AutoEventWireup="false" Codebehind="recalcularimportes.aspx.vb" Inherits="recalcularimportes" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
		<title>recalcularimportes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script>


  /*''' <summary>
	''' Tras calcular los campos calculados hay q establecerlos en pantalla
	''' </summary>
	''' <param name="campo">Id del campo a estableccer</param>
	''' <param name="valor">Valor a establecer</param>        
	''' <param name="tipo">El campo a actualizar esta entre: 1-los campos 2- un desglose</param>   
	''' <remarks>Llamada desde: recalcularImportes.aspx; Tiempo m�ximo:0</remarks>*/		
	function ponerValorCampoCalculado(campo,valor, tipo)
	{
	    p = window.parent.frames["fraPMPortalMain"];
	
	if (tipo==1)
		{
		oEntry = p.fsGeneralEntry_getById(campo)
		if (oEntry)
			{
			oEntry.setValue(valor)
			}
		}
	else
		{
		if (p.document.getElementById(campo))
			{
			p.document.getElementById(campo).value = valor
			
			if (p.oWinDesglose!=null)
			  {
				if (!p.oWinDesglose.closed)
				{
				var sAux
				sAux=campo
				sAux = sAux.split("__")
				sAux="Desglose_fsdsentry_" + sAux[sAux.length-2] + "_" + sAux[sAux.length-1] 			
				oEntry=p.oWinDesglose.fsGeneralEntry_getById(sAux)
				if (oEntry)
					{				
					oEntry.setValue(valor)
					}
				}
			  }
			
			}
		
		}
		

	}
	
	
		</script>
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
		</form>
	</body>
</html>
