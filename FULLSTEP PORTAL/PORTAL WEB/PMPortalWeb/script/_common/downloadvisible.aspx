<%@ Page Language="vb" AutoEventWireup="false" Codebehind="downloadvisible.aspx.vb" Inherits="Fullstep.PMPortalWeb.downloadvisible" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:label id="lblTitulo" runat="server" Height="24px" Width="100%" CssClass="subtitulo">DOWNLOADING FILE...</asp:label>
			<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="100%">
				<TR>
					<TD style="FONT-SIZE: 1px; HEIGHT: 1px" class="linea1" colSpan="2" height="1">&nbsp;
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 150px"><asp:label id="lblFichero" runat="server" CssClass="parrafo">File name:</asp:label></TD>
					<TD><asp:label id="txtFichero" runat="server" CssClass="parrafo">NombreDelFichero.EXT</asp:label></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblSize" runat="server" CssClass="parrafo">File size:</asp:label></TD>
					<TD><asp:label id="txtSize" runat="server" CssClass="parrafo">1.000 Kb.</asp:label></TD>
				</TR>
				<TR>
					<TD style="FONT-SIZE: 1px; HEIGHT: 1px" class="linea2" colSpan="2" height="1">&nbsp;
					</TD>
				</TR>
			</TABLE>
			<br>
			<asp:label id="lblInstruc1" runat="server" CssClass="parrafo">Download should begin in a few seconds. If the download does not start automatically, or you have any problems, click on the following link to re-start download:</asp:label><asp:hyperlink id="lnkDownload1" runat="server" Target="_self">HyperLink</asp:hyperlink><br>
			<br>
			<asp:Label id="lblInstruc2" runat="server" CssClass="parrafo">If you wish to save the file to disk, instead of opening the file, right click on the following link and select Save Target as...</asp:Label>
			<asp:HyperLink id="lnkDownload2" runat="server" Target="_self">HyperLink</asp:HyperLink></form>
	</body>
</html>
