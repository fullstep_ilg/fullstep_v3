Namespace Fullstep.PMPortalWeb

    Partial Class proveedores
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            Dim lCiaComp As Long = IdCiaComp
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaTheme", "<script>var rutaTheme = '" & ConfigurationManager.AppSettings("ruta") & "App_Themes/" & Page.Theme & "'</script>")
            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Paginador
            CType(whdgProveedores.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblPage"), Label).Text = Textos(0)
            CType(whdgProveedores.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblOF"), Label).Text = Textos(1)
            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.BusquedaArticulos
            Me.lblOrganizacionCompras.Text = Textos(7) 'Org Compras
            'Vuelvo a cargar el modulo de Proveedores
            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Proveedores
            'Textos de idiomas:
            Me.lbltitulo.Text = Textos(1) 'B�squeda de proveedor
            Me.lblCod.Text = Textos(8) 'C�digo
            Me.lblDen.Text = Textos(9) 'Denominaci�n
            Me.lblCIF.Text = Textos(10) 'CIF
            Me.hypBuscar.Text = Textos(7) 'Buscar
            Me.Label2.Text = Textos(5) 'Resultado de la b�squeda:
            Me.Label1.Text = Textos(6) 'Pulse sobre el proveedor para seleccionarlo o haga una nueva b�squeda

            If Page.IsPostBack Then
                CargarGrid()
            End If
            If Not Page.IsPostBack Then   'Inicializaci�n del Grid
                With CType(whdgProveedores.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnFirst"), System.Web.UI.WebControls.Image)
                    .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/primero_desactivado.gif"
                    .Attributes.Add("onClick", "return FirstPage();")
                    .Enabled = False
                End With
                With CType(whdgProveedores.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnPrev"), System.Web.UI.WebControls.Image)
                    .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/anterior_desactivado.gif"
                    .Attributes.Add("onClick", "return PrevPage();")
                    .Enabled = False
                End With
                Dim SoloUnaPagina As Boolean = True
                With CType(whdgProveedores.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), System.Web.UI.WebControls.Image)
                    .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
                    .Attributes.Add("onClick", "return NextPage();")
                    .Enabled = Not SoloUnaPagina
                End With
                With CType(whdgProveedores.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), System.Web.UI.WebControls.Image)
                    .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
                    .Attributes.Add("onClick", "return LastPage();")
                    .Enabled = Not SoloUnaPagina
                End With

                Dim oDS = New DataSet
                Dim oTable As DataTable = oDS.Tables.Add("TEMP")
                oTable.Columns.Add("COD", System.Type.GetType("System.String"))
                oTable.Columns.Add("DEN", System.Type.GetType("System.String"))
                oTable.Columns.Add("NIF", System.Type.GetType("System.String"))

                hypBuscar_Click(sender, e)
            End If

            If Request("IdControl") = Nothing Then
                If Page.IsPostBack = False Then
                    Me.ugtxtCIF.Text = Request("CIF")
                    Me.ugtxtCod.Text = Request("COD")
                    Me.ugtxtDen.Text = Request("DEN")
                    hypBuscar_Click(sender, e)
                End If
            Else
                Me.IDCONTROL.Value = Request("IdControl")
            End If

            Me.VISIBILIDAD.Value = Request("Visibilidad")
            Me.VALORORGCOMPRAS.Value = Request("ValorOrgCompras")
            Me.IDDEPEN.Value = Request("IDDepen")

            If (Me.IDDEPEN.Value <> "null") Then
                'Usar OrgCompras
                Dim oOrganizacionesCompras As Fullstep.PMPortalServer.OrganizacionesCompras = FSPMServer.Get_OrganizacionesCompras()
                oOrganizacionesCompras.LoadData(lCiaComp)

                ugtxtOrganizacionCompras.DataSource = From datos In oOrganizacionesCompras.Data.Tables(0) _
                                                      Select New With {.COD = datos("COD"), .DEN = datos("COD") & " - " & datos("DEN")}
                Me.ugtxtOrganizacionCompras.ValueField = "COD"
                Me.ugtxtOrganizacionCompras.TextField = "DEN"
                ugtxtOrganizacionCompras.DataBind()
                If ugtxtOrganizacionCompras.Items.Count > 0 Then Me.ugtxtOrganizacionCompras.SelectedItemIndex = 0
                For Each oItem As Infragistics.Web.UI.ListControls.DropDownItem In Me.ugtxtOrganizacionCompras.Items
                    If oItem.Value = Me.VALORORGCOMPRAS.Value Then
                        Me.ugtxtOrganizacionCompras.SelectedItemIndex = oItem.Index
                        Me.lblOrganizacionComprasReadOnly.Text = oItem.Text
                        Me.DENORGCOMPRAS.Value = oItem.Text
                        Exit For
                    End If
                Next

                If Me.VISIBILIDAD.Value = "visible" Then
                    Me.ugtxtOrganizacionCompras.Visible = True
                    Me.lblOrganizacionComprasReadOnly.Visible = False
                ElseIf Me.VISIBILIDAD.Value = "readOnly" Then
                    Me.ugtxtOrganizacionCompras.Visible = False
                    Me.lblOrganizacionComprasReadOnly.Visible = True
                ElseIf Me.VISIBILIDAD.Value = "null" And Me.IDDEPEN.Value = "oculto" Then
                    Me.ugtxtOrganizacionCompras.Visible = False
                    Me.lblOrganizacionComprasReadOnly.Visible = True
                End If
            Else
                'No uSar OrgCompras
                Me.lblOrganizacionCompras.Visible = False
                Me.lblOrganizacionComprasReadOnly.Visible = False
                Me.ugtxtOrganizacionCompras.Visible = False
            End If

        End Sub

        Private Sub CrearColumnas()
            Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
            Dim nombre As String

            whdgProveedores.Rows.Clear()
            whdgProveedores.Columns.Clear()
            whdgProveedores.GridView.Columns.Clear()
            For i As Integer = 0 To whdgProveedores.DataSource.Tables(0).Columns.Count - 1
                campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
                nombre = whdgProveedores.DataSource.Tables(0).Columns(i).ToString
                With campoGrid
                    .Key = nombre
                    .DataFieldName = nombre
                    .Header.Text = nombre
                End With
                whdgProveedores.Columns.Add(campoGrid)
                whdgProveedores.GridView.Columns.Add(campoGrid)
            Next
        End Sub

        Private Sub ConfigurarGrid()
            Me.whdgProveedores.Columns("COD").Header.Text = Textos(8) '"C�digo"
            Me.whdgProveedores.Columns("COD").Width = Unit.Percentage(15)
            Me.whdgProveedores.Columns("DEN").Header.Text = Textos(9) '"Denominaci�n"
            Me.whdgProveedores.Columns("DEN").Width = Unit.Percentage(60)
            Me.whdgProveedores.Columns("NIF").Header.Text = Textos(10) '"CIF"
            Me.whdgProveedores.Columns("NIF").Width = Unit.Percentage(25)
        End Sub

        Private Sub CargarGrid()
            Me.whdgProveedores.DataSource = CType(Cache("oProve_" & FSPMUser.Cod), DataSet)
            CrearColumnas()
            Me.whdgProveedores.DataBind()
            ConfigurarGrid()
        End Sub
        ''' <summary>
        ''' Responde al evento click del hyperlink haciendo una busqueda de proveedores para los filtros de pantalla. 
        ''' </summary>
        ''' <param name="sender">hyperlink hypBuscar</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,3</remarks>
        Private Sub hypBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hypBuscar.Click
            Dim lCiaComp As Long = IdCiaComp
            Dim oProves As Fullstep.PMPortalServer.Proveedores
            oProves = FSPMServer.get_Proveedores

            If (Me.IDDEPEN.Value <> "null") AndAlso Not (Me.IDDEPEN.Value.Equals("")) Then
                Dim sCodOrgCompras As String = ugtxtOrganizacionCompras.SelectedItem.Value
                If sCodOrgCompras <> Nothing Then
                    oProves.LoadData(Idioma, lCiaComp, Me.ugtxtCod.Value, Me.ugtxtDen.Value, Me.ugtxtCIF.Value, FSPMUser.CodProveGS, sCodOrgCompras)
                Else
                    oProves.LoadData(Idioma, lCiaComp, Me.ugtxtCod.Value, Me.ugtxtDen.Value, Me.ugtxtCIF.Value, FSPMUser.CodProveGS, "")
                End If
            Else
                oProves.LoadData(Idioma, lCiaComp, Me.ugtxtCod.Value, Me.ugtxtDen.Value, Me.ugtxtCIF.Value, FSPMUser.CodProveGS)
            End If

            If (Me.IDDEPEN.Value <> "null") Then
                If ugtxtOrganizacionCompras.SelectedItemIndex <> -1 Then
                    Me.VALORORGCOMPRAS.Value = ugtxtOrganizacionCompras.SelectedItem.Value
                    Me.DENORGCOMPRAS.Value = ugtxtOrganizacionCompras.SelectedItem.Text
                End If
            End If

            Me.whdgProveedores.DataSource = oProves.Data
            CrearColumnas()
            Me.whdgProveedores.DataBind()
            Me.InsertarEnCache("oProve_" & FSPMUser.Cod, oProves.Data)

            Dim i As Integer
            With Me.whdgProveedores
                For i = .Columns.Count - 1 To 0 Step -1
                    If .Columns(i).Key = "COD" Then
                        .Columns(i).Width = Unit.Percentage(15)
                    ElseIf .Columns(i).Key = "DEN" Then
                        .Columns(i).Width = Unit.Percentage(60)
                    ElseIf .Columns(i).Key = "NIF" Then
                        .Columns(i).Width = Unit.Percentage(25)
                    Else
                        .Columns.RemoveAt(i)
                    End If

                Next
                .Columns("COD").Header.Text = Textos(8) '"C�digo"
                .Columns("DEN").Header.Text = Textos(9) '"Denominaci�n"
                .Columns("NIF").Header.Text = Textos(10) '"CIF"
            End With
        End Sub

        Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
            hypBuscar_Click(sender, e)
        End Sub

        Private Sub whdgProveedores_DataBound(sender As Object, e As System.EventArgs) Handles whdgProveedores.DataBound
            Dim pagerList As DropDownList = DirectCast(whdgProveedores.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("PagerPageList"), DropDownList)
            pagerList.Items.Clear()
            For i As Integer = 1 To whdgProveedores.GridView.Behaviors.Paging.PageCount
                pagerList.Items.Add(i.ToString())
            Next
            CType(whdgProveedores.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblCount"), Label).Text = whdgProveedores.GridView.Behaviors.Paging.PageCount
            Dim SoloUnaPagina As Boolean = (whdgProveedores.GridView.Behaviors.Paging.PageCount = 1)
            With CType(whdgProveedores.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnNext"), System.Web.UI.WebControls.Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/siguiente" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
                .Enabled = Not SoloUnaPagina
            End With
            With CType(whdgProveedores.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("ImgBtnLast"), System.Web.UI.WebControls.Image)
                .ImageUrl = "~/App_Themes/" & Page.Theme & "/images/ultimo" & IIf(SoloUnaPagina, "_desactivado", "") & ".gif"
                .Enabled = Not SoloUnaPagina
            End With
        End Sub
    End Class
End Namespace
