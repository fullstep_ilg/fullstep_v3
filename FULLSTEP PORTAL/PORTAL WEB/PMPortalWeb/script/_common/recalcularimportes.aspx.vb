Imports Fullstep.PMPortalWeb.modUtilidades
Partial Class recalcularimportes
    Inherits FSPMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' Calcular los campos calculados de desgloses y campos
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">evento de sistema</param>           
    ''' <remarks>Llamada desde: certificado.aspx    noconformidad.aspx  detalleSolicitud.aspx   solicitudes.aspx; Tiempo m�ximo: 0,3</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
        Dim oInstancia As Fullstep.PMPortalServer.Instancia
        Dim oSolicitud As Fullstep.PMPortalServer.Solicitud
        Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
        Dim lIndexGrupos As Integer = 0
        Dim oDSRow As DataRow
        Dim sPre As String
        Dim lCiaComp As Long = IdCiaComp
        Dim oDS As DataSet
        Dim oDSAux As DataSet
        Dim oDSDesglosePadreVisible As DataSet
        Dim oDSRowDesglosePadreVisible As DataRow
        Dim lInstancia As Integer
        Dim bQA As Boolean = False

        If String.IsNullOrEmpty(Request("CALC_Instancia")) Or strToInt(Request("CALC_NumVersion")) = 0 Then
            lInstancia = Nothing

            oSolicitud = FSPMServer.Get_Solicitud
            oSolicitud.ID = Request("CALC_Solicitud")
            oSolicitud.Load(lCiaComp, Idioma)
            bQA = {TiposDeDatos.TipoDeSolicitud.Certificado, TiposDeDatos.TipoDeSolicitud.NoConformidad}.Contains(oSolicitud.TipoSolicit)
            oDS = oSolicitud.Formulario.LoadCamposCalculados(lCiaComp, oSolicitud.ID, oUser.Idioma, Not bQA, FSPMUser.CodProveGS, FSPMUser.IdContacto)
        Else
            lInstancia = Request("CALC_Instancia")

            oInstancia = FSPMServer.Get_Instancia
            oInstancia.ID = Request("CALC_Instancia")
            oInstancia.Load(lCiaComp, Idioma)
            oInstancia.Version = strToInt(Request("CALC_NumVersion"))
			bQA = {TiposDeDatos.TipoDeSolicitud.Certificado, TiposDeDatos.TipoDeSolicitud.NoConformidad}.Contains(oInstancia.Solicitud.TipoSolicit)
			oSolicitud = FSPMServer.Get_Solicitud

			oDS = oInstancia.LoadCamposCalculados(lCiaComp, oUser.CodProveGS, Not bQA)
		End If

        Dim oDSDesglose As DataSet
        Dim oDSRowDesglose As DataRow
        Dim oDSRowDesgloseOculto As DataRow
        Dim sVariables() As String
        Dim sVariablesDesglose() As String
        Dim dValues() As Double
        Dim dValuesDesglose() As Double
        Dim dValuesTotalDesglose() As Double
        Dim dTotalDesglose As Double
        Dim i As Integer
        Dim iDesglose As Integer

        ReDim sVariables(oDS.Tables(0).Rows.Count - 1)
        ReDim dValues(oDS.Tables(0).Rows.Count - 1)

        Dim iTipoGS As Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS
        Dim iTipo As Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral
        Dim sRequest As String
        Dim vValor As Object
        Dim iTipoGSDesglose As Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS
        Dim iTipoDesglose As Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral
        Dim iEq As New USPExpress.USPExpression

        i = 0
        Dim noinsertar As Integer
        Dim iIndices = 0
        Dim bEstaInvisibleElDesglose As Boolean

        noinsertar = 0
        For Each oDSRow In oDS.Tables(0).Rows
            bEstaInvisibleElDesglose = False
            sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())
            sVariables(i) = oDSRow.Item("ID_CALCULO")

            iTipoGS = DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS"))
            iTipo = oDSRow.Item("SUBTIPO")
            If lInstancia = Nothing Then
                sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID").ToString()
            Else
                sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO").ToString()
            End If

            If (oDSRow.Item("ESCRITURA") = 1 AndAlso Not (lInstancia = Nothing)) OrElse (oDSRow.Item("VISIBLE") = 1 AndAlso lInstancia = Nothing) Then
                vValor = Numero(VacioToNothing(Request(sRequest)), ".", ",")
            Else
                vValor = oDSRow.Item("VALOR_NUM")
            End If

            If oDSRow.Item("TIPO") = Fullstep.PMPortalServer.TipoCampoPredefinido.Calculado Then
                If Not IsDBNull(oDSRow.Item("ORIGEN_CALC_DESGLOSE")) Then
                    Dim oCampo As Fullstep.PMPortalServer.Campo
                    oCampo = FSPMServer.Get_Campo
                    If lInstancia = Nothing Then
                        oCampo.Id = oDSRow.Item("ORIGEN_CALC_DESGLOSE")
                        oDSDesglose = oCampo.LoadCalculados(lCiaComp, oSolicitud.ID, FSPMUser.CodProveGS, FSPMUser.IdContacto)
                        oDSDesglosePadreVisible = oSolicitud.Formulario.LoadDesglosePadreVisible(lCiaComp, oSolicitud.ID, oCampo.Id, Nothing, FSPMUser.CodProveGS, FSPMUser.IdContacto)
                    Else
                        oCampo.Id = DBNullToInt(oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE"))
                        oDSDesglose = oCampo.LoadInstCalculados(lCiaComp, oInstancia.ID, oUser.CodProve)
                        If {TiposDeDatos.TipoDeSolicitud.Certificado, TiposDeDatos.TipoDeSolicitud.NoConformidad}.Contains(oInstancia.Solicitud.TipoSolicit) Then
                            oDSAux = oSolicitud.Formulario.LoadCamposCalculados(lCiaComp, oInstancia.Solicitud.ID, oUser.Idioma)
                            For Each oDSRowAux As DataRow In oDSAux.Tables(0).Rows
                                If oDSRow.Item("ID") = oDSRowAux.Item("ID") Then
                                    oDSDesglosePadreVisible = oSolicitud.Formulario.LoadDesglosePadreVisible(lCiaComp, oInstancia.Solicitud.ID, oDSRowAux.Item("ORIGEN_CALC_DESGLOSE"))
                                    Exit For
                                End If
                            Next

                        Else
                            oDSDesglosePadreVisible = oInstancia.LoadDesglosePadreVisible(lCiaComp, oCampo.Id, , oUser.CodProve)
                        End If
                    End If

                    ReDim sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
                    ReDim dValuesDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
                    ReDim dValuesTotalDesglose(oDSDesglose.Tables(0).Rows.Count - 1)

                    dTotalDesglose = 0

                    Dim iNumRows As Integer
                    Dim lineasReal As Integer()
                    Dim bPopUp As Boolean
                    Dim bDesgloseEmergente As Boolean

                    iNumRows = 0
                    bEstaInvisibleElDesglose = True
                    If oDSDesglosePadreVisible.Tables(0).Rows.Count > 0 Then
                        For Each oDSRowDesglosePadreVisible In oDSDesglosePadreVisible.Tables(0).Rows
                            If oDSRowDesglosePadreVisible.Item("VISIBLE") = 1 Then
                                bEstaInvisibleElDesglose = False
                                Exit For
                            End If
                        Next
                    End If

                    If bEstaInvisibleElDesglose = False Then 'If oDSRow.Item("VISIBLE") = 1 Then
                        sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString())
                        iNumRows = Request("CALC_" + sPre + "_tblDesglose") 'Filas de desglose que salen en pesta�a
                        bPopUp = False
                        bDesgloseEmergente = False
                        If iNumRows = Nothing Then
                            'El desglose es de tipo popup
                            If lInstancia = Nothing Then
                                iNumRows = Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "_tblDesglose")
                                bDesgloseEmergente = False
                                If iNumRows = Nothing Then
                                    bDesgloseEmergente = True
                                    iNumRows = Request("CALC_" + sPre + "_fsentry" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "__numRows")
                                End If
                            Else
                                'Desglose no Emergente "CALC_uwtGrupos__ctl0xx_5491_5491_133510_tblDesglose"
                                iNumRows = Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_tblDesglose")
                                Dim conta As Integer = 1
                                If iNumRows <> Nothing Then
                                    bDesgloseEmergente = False
                                    ReDim lineasReal(iNumRows)
                                    For numLinea As Integer = 1 To iNumRows
                                        For campoCalc As Integer = 0 To oDSDesglose.Tables(0).Rows.Count - 1
                                            If Not Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + numLinea.ToString + "_" + oDSDesglose.Tables(0).Rows(campoCalc).Item("ID_CAMPO").ToString) Is Nothing Then
                                                lineasReal(conta) = numLinea
                                                Exit For
                                            End If
                                        Next
                                        conta += 1
                                    Next
                                Else
                                    bDesgloseEmergente = True
                                    iNumRows = Request("CALC_" + sPre + "_fsentry" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "__numTotRows")
                                    ReDim lineasReal(iNumRows)
                                    For numLinea As Integer = 1 To iNumRows
                                        For campoCalc As Integer = 0 To oDSDesglose.Tables(0).Rows.Count - 1
                                            If Not Request("CALC_" + sPre + "_fsentry" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "__" + numLinea.ToString + "__" + oDSDesglose.Tables(0).Rows(campoCalc).Item("ID_CAMPO").ToString) Is Nothing Then
                                                lineasReal(conta) = numLinea
                                                Exit For
                                            End If
                                        Next
                                        conta += 1
                                    Next
                                End If
                            End If
                            bPopUp = True
                        Else
                            If lInstancia <> Nothing Then
                                ReDim lineasReal(iNumRows)
                                Dim conta As Integer = 1
                                For numLinea As Integer = 1 To iNumRows
                                    For campoCalc As Integer = 0 To oDSDesglose.Tables(0).Rows.Count - 1
                                        If Not Request("CALC_" + sPre + "_fsdsentry_" + numLinea.ToString + "_" + oDSDesglose.Tables(0).Rows(campoCalc).Item("ID_CAMPO").ToString) Is Nothing Then
                                            lineasReal(conta) = numLinea
                                            Exit For
                                        End If
                                    Next
                                    conta += 1
                                Next
                            End If
                        End If
                    Else
                        iNumRows = 0
                        If oDSDesglose.Tables(1).Rows.Count > 0 Then
                            For Each oDSRowDesgloseOculto In oDSDesglose.Tables(1).Rows
                                If oDSRowDesgloseOculto.Item("Linea") > iNumRows Then
                                    iNumRows = oDSRowDesgloseOculto.Item("Linea")
                                    bEstaInvisibleElDesglose = True
                                End If
                            Next
                        End If
                    End If

                    Dim NumEliminadas As Integer
                    Dim k As Integer
                    Dim EliminadoporNothing As Boolean

                    NumEliminadas = 0
                    EliminadoporNothing = False
                    For k = 1 To iNumRows
                        If lInstancia = Nothing OrElse (lineasReal IsNot Nothing AndAlso lineasReal.Contains(k)) Then
                            iDesglose = 0
                            For Each oDSRowDesglose In oDSDesglose.Tables(0).Rows
                                sVariablesDesglose(iDesglose) = oDSRowDesglose.Item("ID_CALCULO")

                                iTipoGSDesglose = DBNullToSomething(oDSRowDesglose.Item("TIPO_CAMPO_GS"))
                                iTipoDesglose = oDSRow.Item("SUBTIPO")

                                vValor = Numero(VacioToNothing(Request(sRequest)), ".", ",")

                                Dim keysDesglose(2) As DataColumn
                                Dim findDesglose(2) As Object
                                Dim oRowDesglose As DataRow
                                vValor = Nothing

                                'Condiciones para coger el valor de los campos tipo "CALC_":
                                'Si estamos en el alta y el campo est� visible
                                'Si no estamos en el alta y el campo es de escritura
                                'En los dem�s casos se coge el valor de VALOR_NUM.
                                If (iTipoGSDesglose = TiposDeDatos.TipoCampoGS.PrecioUnitario AndAlso oDSRowDesglose.Item("VISIBLE") = 1) OrElse
                                    (((oDSRowDesglose.Item("ESCRITURA") = 1 AndAlso Not (lInstancia = Nothing)) OrElse (oDSRowDesglose.Item("VISIBLE") = 1 AndAlso lInstancia = Nothing)) AndAlso Not bEstaInvisibleElDesglose) Then
                                    If lInstancia = Nothing Then
                                        If bPopUp Then
                                            If bDesgloseEmergente Then
                                                sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ORIGEN_CALC_DESGLOSE") & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID")
                                            Else 'CALC_uwtGrupos__ctl0xx_623_623_10549_fsdsentry_1_10551
                                                sRequest = "CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID").ToString
                                            End If
                                        Else 'CALC_uwtGrupos__ctl0x_146_fsdsentry_1_2283
                                            sRequest = "CALC_" + sPre + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID").ToString()
                                        End If
                                    Else
                                        If bPopUp Then
                                            If bDesgloseEmergente Then
                                                sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE") & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID_CAMPO")

                                            Else 'CALC_uwtGrupos__ctl0xx_5491_5491_133510_fsdsentry_1_133486
                                                sRequest = "CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID_CAMPO").ToString
                                            End If

                                        Else 'CALC_uwtGrupos__ctl0x_146_fsdsentry_1_2283
                                            sRequest = "CALC_" + sPre + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID_CAMPO").ToString()
                                        End If
                                    End If

                                    vValor = Numero(VacioToNothing(Request(sRequest)), ".", ",")
                                    If Request(sRequest) Is Nothing Then
                                        EliminadoporNothing = True
                                    End If
                                Else
                                    keysDesglose(0) = oDSDesglose.Tables(1).Columns("LINEA")
                                    keysDesglose(1) = oDSDesglose.Tables(1).Columns("CAMPO_PADRE")
                                    keysDesglose(2) = oDSDesglose.Tables(1).Columns("CAMPO_HIJO")

                                    oDSDesglose.Tables(1).PrimaryKey = keysDesglose
                                    Dim iLinea As Integer
                                    iLinea = k
                                    findDesglose(0) = (k).ToString()
                                    If lInstancia = Nothing Then
                                        findDesglose(1) = oDSRow.Item("ORIGEN_CALC_DESGLOSE")
                                        findDesglose(2) = oDSRowDesglose.Item("ID").ToString()
                                    Else
                                        findDesglose(1) = oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE")
                                        findDesglose(2) = oDSRowDesglose.Item("ID_CAMPO").ToString()
                                    End If
                                    oRowDesglose = oDSDesglose.Tables(1).Rows.Find(findDesglose)
                                    While oRowDesglose Is Nothing And iLinea > 0
                                        iLinea -= 1
                                        findDesglose(0) = iLinea

                                        oRowDesglose = oDSDesglose.Tables(1).Rows.Find(findDesglose)
                                    End While

                                    If Not oRowDesglose Is Nothing Then
                                        vValor = DBNullToSomething(oRowDesglose.Item("VALOR_NUM"))
                                    End If
                                End If

                                If oDSRowDesglose.Item("TIPO") = Fullstep.PMPortalServer.TipoCampoPredefinido.Calculado Then
                                    dValuesDesglose(iDesglose) = 0
                                Else
                                    dValuesDesglose(iDesglose) = Numero(vValor)
                                End If
                                dValuesTotalDesglose(iDesglose) += dValuesDesglose(iDesglose)
                                iDesglose += 1
                            Next
                            iDesglose = 0

                            For Each oDSRowDesglose In oDSDesglose.Tables(0).Rows
                                If oDSRowDesglose.Item("TIPO") = Fullstep.PMPortalServer.TipoCampoPredefinido.Calculado Then
                                    Try
                                        iEq.Parse(oDSRowDesglose.Item("FORMULA"), sVariablesDesglose)
                                        dValuesDesglose(iDesglose) = iEq.Evaluate(dValuesDesglose)
                                        If lInstancia = Nothing Then
                                            If bPopUp Then
                                                If bDesgloseEmergente Then
                                                    Page.ClientScript.RegisterStartupScript(Me.GetType(), oDSRowDesglose.Item("ID").ToString() + "_" + iIndices.ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_fsentry" & oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID").ToString() + "'," + JSNum(dValuesDesglose(iDesglose)) + ",2);</script>")
                                                    iIndices += 1
                                                Else
                                                    Page.ClientScript.RegisterStartupScript(Me.GetType(), oDSRowDesglose.Item("ID").ToString() + "_" + iIndices.ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_" & oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString() & "_" & oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString & "_fsdsentry_" & (k).ToString() & "_" & oDSRowDesglose.Item("ID").ToString() + "'," + JSNum(dValuesDesglose(iDesglose)) + ",1);</script>")
                                                    iIndices += 1
                                                End If
                                            Else
                                                Page.ClientScript.RegisterStartupScript(Me.GetType(), oDSRowDesglose.Item("ID").ToString() + "_" + iIndices.ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_fsdsentry_" & (k).ToString() & "_" & oDSRowDesglose.Item("ID").ToString() + "'," + JSNum(dValuesDesglose(iDesglose)) + ",1);</script>")
                                                iIndices += 1
                                            End If
                                        Else
                                            If bPopUp Then
                                                If bDesgloseEmergente Then
                                                    Page.ClientScript.RegisterStartupScript(Me.GetType(), oDSRowDesglose.Item("ID").ToString() + "_" + iIndices.ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID_CAMPO").ToString() + "'," + JSNum(dValuesDesglose(iDesglose)) + ",2);</script>")
                                                    iIndices += 1
                                                Else
                                                    Page.ClientScript.RegisterStartupScript(Me.GetType(), oDSRowDesglose.Item("ID").ToString() + "_" + iIndices.ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_" & oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString() & "_" & oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString & "_fsdsentry_" & (k).ToString() & "_" & oDSRowDesglose.Item("ID_CAMPO").ToString() + "'," + JSNum(dValuesDesglose(iDesglose)) + ",1);</script>")
                                                    iIndices += 1
                                                End If
                                            Else
                                                Page.ClientScript.RegisterStartupScript(Me.GetType(), oDSRowDesglose.Item("ID").ToString() + "_" + iIndices.ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_fsdsentry_" & (k).ToString() & "_" & oDSRowDesglose.Item("ID_CAMPO").ToString() + "'," + JSNum(dValuesDesglose(iDesglose)) + ",1);</script>")
                                                iIndices += 1
                                            End If
                                        End If
                                        dValuesTotalDesglose(iDesglose) += dValuesDesglose(iDesglose)
                                    Catch ex As USPExpress.ParseException
                                    Catch ex0 As Exception
                                    End Try
                                End If
                                iDesglose += 1
                            Next
                            ReDim Preserve sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count)
                            ReDim Preserve dValuesDesglose(oDSDesglose.Tables(0).Rows.Count)

                            sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count) = oDSRow.Item("FORMULA") + " * 1"

                            Try
                                iEq.Parse(oDSRow.Item("FORMULA"), sVariablesDesglose)

                                dValuesDesglose(oDSDesglose.Tables(0).Rows.Count) = iEq.Evaluate(dValuesDesglose)
                                dTotalDesglose += dValuesDesglose(oDSDesglose.Tables(0).Rows.Count)
                                ReDim sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
                                ReDim dValuesDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
                            Catch ex As USPExpress.ParseException
                            Catch ex0 As Exception
                            End Try
                        End If
                    Next

                    sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())
                    If Not IsDBNull(oDSRow.Item("FORMULA")) And iNumRows > 0 Then
                        Try
                            dTotalDesglose = iEq.Evaluate(dValuesTotalDesglose)
                            dValues(i) = dTotalDesglose
                            If lInstancia = Nothing Then
                                Page.ClientScript.RegisterStartupScript(Me.GetType(), oDSRow.Item("ID").ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_fsentry" & oDSRow.Item("ID").ToString() + "'," + JSNum(dValues(i)) + ",1);</script>")
                            Else
                                Page.ClientScript.RegisterStartupScript(Me.GetType(), oDSRow.Item("ID").ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO").ToString() + "'," + JSNum(dValues(i)) + ",1);</script>")
                            End If
                        Catch ex As USPExpress.ParseException
                        Catch ex0 As Exception
                        End Try
                    End If
                Else
                    dValues(i) = 0
                End If
            Else
                If IsDBNull(vValor) Then
                    dValues(i) = 0
                Else
                    dValues(i) = Numero(vValor)
                End If
            End If
            i += 1
        Next
        i = 0
        For Each oDSRow In oDS.Tables(0).Rows
            sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())
            If oDSRow.Item("TIPO") = Fullstep.PMPortalServer.TipoCampoPredefinido.Calculado Then
                Try
                    iEq.Parse(oDSRow.Item("FORMULA"), sVariables)
                    dValues(i) = iEq.Evaluate(dValues)
                    oDSRow.Item("VALOR_NUM") = dValues(i)
                    If lInstancia = Nothing Then
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), oDSRow.Item("ID").ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_fsentry" & oDSRow.Item("ID").ToString() + "'," + JSNum(dValues(i)) + ",1);</script>")
                    Else
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), oDSRow.Item("ID").ToString(), "<script>ponerValorCampoCalculado('" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO").ToString() + "'," + JSNum(dValues(i)) + ",1);</script>")

                    End If
                Catch ex As USPExpress.ParseException
                Catch ex0 As Exception
                End Try
            End If
            i += 1
        Next
    End Sub
End Class