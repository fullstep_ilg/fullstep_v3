<%@ Page Language="vb" AutoEventWireup="false" Codebehind="attachfile.aspx.vb" Inherits="Fullstep.PMPortalWeb.attachfile" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<script>
		/*
		''' <summary>
        ''' A�adir en la pantalla de adjuntos el adjunto recien insertado y cerrar.
        ''' </summary>
        ''' <param name="Id">Id en bbdd del adjunto</param>
        ''' <param name="nom">Nombre del adjunto</param>        
        ''' <param name="com">Comentario del adjunto</param>
        ''' <param name="datasize">tama�o del adjunto</param>
        ''' <param name="comsalto">Comentario del adjunto sin saltos de linea. Se han traducido por @VBCRLF@.</param>  
        ''' <returns>Nada</returns>
        ''' <remarks>Llamada desde:Submit1_ServerClick; Tiempo m�ximo:0</remarks>
		*/
		    function anyadirAdjunto(entryId, Id, nom, com, datasize, comsalto, origen, idContrato, EsContrato, PathAdjunto) {
		        if (origen.indexOf('atachedfilesDesglose.aspx') >= 0) {
		            p = window.opener
		            j = p.opener
		            j.envioInfo(entryId, Id, nom, com, datasize, comsalto, origen, 0)
		            p.envioInfoAdj(entryId, Id, nom, com, datasize, comsalto, origen)
		        } else {
		            p = window.opener
		            if (EsContrato == 1)
		                p.envioInfoAdjuntoContrato(entryId, Id, nom, com, datasize, comsalto, origen, idContrato, PathAdjunto)
		            else
		                p.envioInfo(entryId, Id, nom, com, datasize, comsalto, origen, idContrato)
		        }
		        window.close()
		    }

		    /*
            Valida la longitud de un dataentry de tipo texto y si no cumple la condici�n, corta el texto.
            Llamadad desde: las cajas de texto de los dataentry medio y largo.
            */
		    function validarLengthyCortarStringLargo(oCampo, longitud, msg) {
		        var retorno = validarLengthStringLargo(oCampo, longitud, msg);
		        if (!retorno) {
		            oCampo.value = oCampo.value.substr(0, longitud);
		        }
		        return retorno;
		    };
		    /* <summary>
            ''' Valida la longitud de un dataentry de tipo texto
            ''' </summary>
            ''' <returns>Nada</returns>
            ''' <remarks>Llamada desde: Todos los dataentry de tipo texto medio,largo y string ; Tiempo m�ximo: instantaneo</remarks>
            */
		    function validarLengthStringLargo(oCampo, longitud, msg) {
		        msg = msg.replace("\\n", "\n")
		        msg = msg.replace("\\n", "\n")
		        if (oCampo.value.length > longitud) {
		            alert(msg + longitud)
		            return false
		        } else return true
		    };
		</script>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<table style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 16px" width="100%" border="0">
				<TR>
					<TD width="100%"><asp:label id="lblSeleccione" runat="server" CssClass="captionBlue">Seleccione el fichero a adjuntar</asp:label></TD>
				</TR>
				<TR>
					<TD align="center"><INPUT class="boton" id="miFile" style="WIDTH: 376px; HEIGHT: 22px" type="file" size="43"
							name="miFile" runat="server">
					</TD>
				</TR>
				<TR>
					<TD><asp:label id="lblComent" runat="server" CssClass="CaptionBlue"> Comentarios</asp:label></TD>
				</TR>
				<TR>
					<TD align="center"><asp:textbox id="txtComent" runat="server" TextMode="MultiLine" Rows="4" Height="88px" Width="376px"></asp:textbox></TD>
				</TR>
				<TR>
					<TD align="center"><INPUT class="boton" id="cmdUpload" type="submit" value="Upload" name="cmdUpload" runat="server">
					</TD>
				</TR>
			</table>
			<input id="IDCAMPO" type="hidden" name="IDCAMPO" runat="server"> <input id="FSENTRY" type="hidden" name="FSENTRY" runat="server">
		</form>
	</body>
</html>
