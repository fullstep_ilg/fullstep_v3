Namespace Fullstep.PMPortalWeb
    Partial Class desgloseControl
        Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub
#End Region
        Private mlId As Long
        Private msTabContainer As String
        Private mlInstancia As Long
        Private mlVersion As Long
        Private mlVersionCert As Long
        Private mbSoloLectura As Boolean
        Private moTextos As DataTable
        Private mlSolicitud As Long
        Private moAcciones As DataTable
        Private moDSEstados As DataSet
        Private msTitulo As String
        Private bCentroRelacionado As Boolean
        Private sCodOrgCompras As String
        Private sIDCampoOrgCompras As String
        Private sidDataEntryFORMOrgCompras As String
        Private sCodCentro As String
        Private sIDCampoCentro As String
        Private sidDataEntryFORMCentro As String
        Private sCopiar_Eliminar As String
        Private bPM As Boolean
        Private msAyuda As String
        Private mbTieneIdCampo As Boolean
        Private mTipoSolicitud As TiposDeDatos.TipoDeSolicitud
        Private msInstanciaMoneda As String
        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
#Region "propertys"
        Property Titulo() As String
            Get
                Return msTitulo
            End Get
            Set(ByVal Value As String)
                msTitulo = Value
            End Set
        End Property
        Property PM() As Boolean
            Get
                Return bPM
            End Get
            Set(ByVal Value As Boolean)
                bPM = Value
            End Set
        End Property
        Property Solicitud() As Long
            Get
                Return mlSolicitud
            End Get
            Set(ByVal Value As Long)
                mlSolicitud = Value
            End Set
        End Property
        Property Instancia() As Long
            Get
                Return mlInstancia
            End Get
            Set(ByVal Value As Long)
                mlInstancia = Value
            End Set
        End Property
        Property Version() As Long
            Get
                Return mlVersion
            End Get
            Set(ByVal Value As Long)
                mlVersion = Value
            End Set
        End Property
        ''' <summary>
        ''' Los certificados, una vez q hay instancia, para determinar si los campos del desglose son visible/editables usan la version
        ''' de la tabla certificado. Tras un guardado en mlVersion tienes la version q hab�a al entrar en el detallecertificado pero en
        ''' bbdd tienes otra versi�n (puedes grabar n veces sin hacer postback). Esta propiedad es para contener la versi�n de bbdd y 
        ''' pasarla al oCampo.LoadInstDesglose.
        ''' </summary>
        ''' <remarks>Llamada desde: Page_load; Tiempo:0</remarks>
        Property VersionCert() As Long
            Get
                Return mlVersionCert
            End Get
            Set(ByVal Value As Long)
                mlVersionCert = Value
            End Set
        End Property
        Property Campo() As Long
            Get
                Return mlId
            End Get
            Set(ByVal Value As Long)
                mlId = Value
            End Set
        End Property
        Property Acciones() As DataTable
            Get
                Return moAcciones
            End Get
            Set(ByVal Value As DataTable)
                moAcciones = Value
            End Set
        End Property
        Property DSEstados() As DataSet
            Get
                Return moDSEstados
            End Get
            Set(ByVal Value As DataSet)
                moDSEstados = Value
            End Set
        End Property
        Property TabContainer() As String
            Get
                Return msTabContainer
            End Get
            Set(ByVal Value As String)
                msTabContainer = Value
            End Set
        End Property
        Property SoloLectura() As Boolean
            Get
                Return mbSoloLectura
            End Get
            Set(ByVal Value As Boolean)
                mbSoloLectura = Value

            End Set
        End Property
        Property CentroRelacionado() As Boolean
            Get
                Return bCentroRelacionado
            End Get
            Set(ByVal Value As Boolean)
                bCentroRelacionado = Value
            End Set
        End Property
        Property CodOrgCompras() As String
            Get
                Return sCodOrgCompras
            End Get
            Set(ByVal Value As String)
                sCodOrgCompras = Value
            End Set
        End Property
        Property CodCentro() As String
            Get
                Return sCodCentro
            End Get
            Set(ByVal Value As String)
                sCodCentro = Value
            End Set
        End Property
        Property idCampoOrgCompras() As String
            Get
                Return sIDCampoOrgCompras
            End Get
            Set(ByVal Value As String)
                sIDCampoOrgCompras = Value
            End Set
        End Property
        Property idCampoCentro() As String
            Get
                Return sIDCampoCentro
            End Get
            Set(ByVal Value As String)
                sIDCampoCentro = Value
            End Set
        End Property
        Property idDataEntryFORMOrgCompras() As String
            Get
                Return sidDataEntryFORMOrgCompras
            End Get
            Set(ByVal Value As String)
                sidDataEntryFORMOrgCompras = Value
            End Set
        End Property
        Property idDataEntryFORMCentro() As String
            Get
                Return sidDataEntryFORMCentro
            End Get
            Set(ByVal Value As String)
                sidDataEntryFORMCentro = Value
            End Set
        End Property
        Private _sidCampoPartida As String
        Property idCampoPartida() As String
            Get
                Return _sidCampoPartida
            End Get
            Set(ByVal Value As String)
                _sidCampoPartida = Value
            End Set
        End Property
        Private _esQA As Boolean = False
        Public Property EsQA() As Boolean
            Get
                Return _esQA
            End Get
            Set(ByVal value As Boolean)
                _esQA = value
            End Set
        End Property
        Property Ayuda() As String
            Get
                Return msAyuda
            End Get
            Set(ByVal Value As String)
                msAyuda = Value
            End Set
        End Property
        Property TieneIdCampo() As Boolean
            Get
                Return mbTieneIdCampo
            End Get
            Set(ByVal Value As Boolean)
                mbTieneIdCampo = Value

            End Set
        End Property
        Property TipoSolicitud As TiposDeDatos.TipoDeSolicitud
            Get
                Return mTipoSolicitud
            End Get
            Set(ByVal Value As TiposDeDatos.TipoDeSolicitud)
                mTipoSolicitud = Value
            End Set
        End Property
        Property InstanciaMoneda() As String
            Get
                Return msInstanciaMoneda
            End Get
            Set(ByVal Value As String)
                msInstanciaMoneda = Value
            End Set
        End Property
#End Region
        ''' Revisado por: blp. Fecha: 10/10/2011
        ''' <summary>
        ''' Cargar un desglose dado.
        ''' NOTA: Ahora en 2008 los name q no creamos explicitamente usan $ para separar los trozos de los 
        ''' diferentes controles del q es hijo (ejemplo uwtGrupos$_ctl0x$128903$fsentry646101$_4__646180),
        ''' antes en 2003 se usaba _ y por ello resultaba q el id y el name eran iguales. Esto importa pq 
        ''' los request se hacen por nombre no por id. En 2003 se le pasaba el id y funcionaba, pero ya no.
        ''' NOTA: Una vez dada de alta la solicitud. Si una columna es readonly (salvo fechas ,material, art,
        ''' den art, presup, unidad, org compras y centro) se debe mostrar como si fuera un entry pero sin crear 
        ''' el entry realmente. Se crea un html con un bot�n tres puntos si hay la celda tiene valor y el tipo de 
        ''' campo de la columna usa bot�n tres puntos.
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">evento de sistema</param>      
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            If Page.IsPostBack Then
                Exit Sub
            End If

            Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User
            Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
            Dim sScript As String
            Dim sScriptFechasSuministro As String
            Dim sIdi As String = Session("FS_Portal_User").Idioma.ToString

            oUser = Session("FS_Portal_User")

            Dim sTblLineas As String
            Dim sTblAdjuntos As String
            Dim sTblEstados As String
            Dim bPopUp As Boolean
            Dim sClase As String
            Dim oHidNumRows As New System.Web.UI.HtmlControls.HtmlInputHidden
            Dim idCampo As Long
            Dim oCampo As Fullstep.PMPortalServer.Campo
            Dim lCiaComp As Long = Session("FS_Portal_User").CiaComp
            Dim lCiaProv As Long = Session("FS_Portal_User").IdCia
            Dim oDS As DataSet
            Dim oDsDefecto As DataSet
            Dim oDSPres As DataSet
            Dim oRow As DataRow
            Dim oRowDesglose As DataRow
            Dim oDSRowAdjun As DataRow
            Dim oRowAcc As DataRow
            Dim oNewRow As DataRow
            Dim oFSEntry As Fullstep.DataEntry.GeneralEntry
            Dim oFSDSEntry As Fullstep.DataEntry.GeneralEntry
            Dim oFSDSEntryDefecto As Fullstep.DataEntry.GeneralEntry
            Dim iTipoGS As Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS
            Dim iTipo As Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral
            Dim oArts As Fullstep.PMPortalServer.Articulos
            Dim oPres1 As Fullstep.PMPortalServer.PresProyectosNivel1
            Dim oPres2 As Fullstep.PMPortalServer.PresContablesNivel1
            Dim oPres3 As Fullstep.PMPortalServer.PresConceptos3Nivel1
            Dim oPres4 As Fullstep.PMPortalServer.PresConceptos4Nivel1
            Dim oProve As Fullstep.PMPortalServer.Proveedor
            Dim sKeyMaterial As String
            Dim sKeyArticulo As String
            Dim arrCamposMaterial(0) As Long
            Dim arrMat(4) As String
            Dim sRestrictMat As String
            Dim sKeyPais As String
            Dim sKeyProveedor As String
            Dim sCodPais As String
            Dim oGMN1s As Fullstep.PMPortalServer.GruposMatNivel1
            Dim oGMN1 As Fullstep.PMPortalServer.GrupoMatNivel1
            Dim oGMN2 As Fullstep.PMPortalServer.GrupoMatNivel2
            Dim oGMN3 As Fullstep.PMPortalServer.GrupoMatNivel3
            Dim iNivel As Integer
            Dim lIdPresup As Integer
            Dim dPorcent As Decimal
            Dim arrPresupuestos() As String
            Dim oPresup As String
            Dim arrPresup(2) As String
            Dim sLblUO As String
            Dim sTexto As String
            Dim sValor As String
            Dim sValorMat As String
            Dim iContadorPres As Integer
            Dim sToolTip As String
            Dim sDenominacion As String
            Dim iAnyo As Integer
            Dim sUon1 As String
            Dim sUon2 As String
            Dim sUon3 As String
            Dim bSinPermisos As Boolean
            Dim dSize As Double
            Dim ilGMN1 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
            Dim ilGMN2 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
            Dim ilGMN3 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
            Dim ilGMN4 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN4
            Dim bCentroRelacionadoFORM As Boolean
            Dim sCodOrgComprasFORM As String
            Dim sCodArticulo As String
            Dim miPage As FSPMPage = CType(Page, FSPMPage)

            If Not IsNothing(Request("CentroRelacionadoFORM")) Then
                bCentroRelacionadoFORM = Request("CentroRelacionadoFORM")
            ElseIf bCentroRelacionado = True Then
                bCentroRelacionadoFORM = True
                sCodOrgComprasFORM = sCodOrgCompras
            End If

            If Not IsNothing(Request("CodOrgComprasFORM")) Then
                sCodOrgComprasFORM = Request("CodOrgComprasFORM")
            End If

            Dim otblRow As System.Web.UI.HtmlControls.HtmlTableRow
            Dim otblCell As System.Web.UI.HtmlControls.HtmlTableCell
            Dim otblCellData As System.Web.UI.HtmlControls.HtmlTableCell
            Dim otblRowData As System.Web.UI.HtmlControls.HtmlTableRow
            Dim oRowHeader As System.Web.UI.HtmlControls.HtmlTableRow
            Dim oCellHeader As System.Web.UI.HtmlControls.HtmlTableCell
            Dim oDivHeader As System.Web.UI.HtmlControls.HtmlControl
            Dim oSpanHeader As System.Web.UI.HtmlControls.HtmlContainerControl
            Dim oImgAyuda As System.Web.UI.WebControls.HyperLink
            Dim oHidden As System.Web.UI.HtmlControls.HtmlInputHidden
            Dim oImg As System.Web.UI.WebControls.HyperLink
            Dim imgFotos As New System.Web.UI.WebControls.Image
            Dim lContador As Integer = 0
            Dim iLineas As Integer = 0
            Dim iMaxIndex As Integer
            Dim i As Integer
            Dim nomDesglose As String = Request("nomDesglose")
            Dim keys(3) As DataColumn
            Dim findTheseVals(3) As Object
            Dim oIndexColumn As DataColumn
            Dim FechaLimite As Date = Nothing
            Dim dtFechaLimite As Date
            Dim NAdjuntos As Integer
            Dim AuxNAdjuntos As String
            Dim AuxNAdjunto As String
            Dim bEstaAdjun As Boolean
            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSPMServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DesgloseControl, sIdi)

            Dim oTextos As DataTable = oDict.Data.Tables(0)
            Dim sCodOrgCompras2 As String
            Dim sCodCentro2 As String
            Dim sCodPais2 As String = "" '''''''''''''''<---------------
            Dim sKeyProvincia As String
            Dim sKeyAlmacen As String
            Dim sIDCentro2 As String '''''''''''''<---------------------
            Dim sKeyUnidadOrganizativa As String
            Dim sKeyOrgComprasDataCheck As String
            Dim sKeyCentro As String
            Dim sKeyTipoRecepcion As String = "0"

            cmdAnyadir.Value = oTextos.Rows(0).Item(1)
            sCopiar_Eliminar = ""

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "TextosBooleanos") Then
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "TextosBooleanos", "<script>var sTextoSi = '" & oTextos.Rows(16).Item(1) & "';var sTextoNo = '" & oTextos.Rows(17).Item(1) & "';</script>")
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "InfraStylePath") Then
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "InfraStylePath", "<script>var InfraStylePath = '" & ConfigurationManager.AppSettings("InfraStylePath") & "';</script>")
            End If

            If mlId = Nothing Then
                idCampo = Request("campo")
                bPopUp = True
            Else
                idCampo = mlId
                bPopUp = False
            End If

            If mlInstancia = Nothing Then
                mlInstancia = Request("Instancia")
            End If

            If mlSolicitud = Nothing Then
                mlSolicitud = Request("Solicitud")
            End If

            oHidNumRows.ID = "numRows"

            Dim oHidNumRowsGrid As New System.Web.UI.HtmlControls.HtmlInputHidden
            oHidNumRowsGrid.ID = "numRowsGrid"

            If Request("SoloLectura") = "1" Then
                mbSoloLectura = True
            End If

            cmdAnyadir.Attributes("onclick") = "copiarFilaVacia('" + ClientID + "','" + idCampo.ToString + "', null, null, null, null)"
            cmdAnyadir.Visible = Not mbSoloLectura

            oCampo = FSPMServer.Get_Campo()
            oCampo.Id = idCampo

            Dim bNuevoWorkflow As Boolean
            Dim bEsNC As Boolean = False
            Dim bFnValorDefecto As Boolean
            Dim sOrden As String
            Dim pag As FSPMPage = Me.Page
            Dim CopiaCampoDef_Padre As Long

            bFnValorDefecto = False
            sOrden = "ORDEN"

            If mlInstancia > 0 Then
                bFnValorDefecto = True
                oCampo.LoadInst(lCiaComp, mlInstancia, sIdi, lCiaProv)

                If {TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras,
                    TiposDeDatos.TipoDeSolicitud.Contrato,
                    TiposDeDatos.TipoDeSolicitud.Otros,
                    TiposDeDatos.TipoDeSolicitud.PedidoExpress,
                    TiposDeDatos.TipoDeSolicitud.PedidoNegociado,
                    TiposDeDatos.TipoDeSolicitud.Factura,
                    TiposDeDatos.TipoDeSolicitud.Encuesta,
                    TiposDeDatos.TipoDeSolicitud.SolicitudQA}.Contains(mTipoSolicitud) Then
                    bNuevoWorkflow = True
                Else
                    bNuevoWorkflow = False
                    sOrden = "ORDEN1"
                    bEsNC = (mTipoSolicitud = TiposDeDatos.TipoDeSolicitud.NoConformidad)
                End If

                Dim bSacarNumLinea As Boolean = {TiposDeDatos.TipoDeSolicitud.SolicitudDeCompras, TiposDeDatos.TipoDeSolicitud.PedidoExpress, TiposDeDatos.TipoDeSolicitud.PedidoNegociado}.Contains(mTipoSolicitud)

                oDS = oCampo.LoadInstDesglose(lCiaComp, sIdi, mlInstancia, oUser.AprobadorActual, oUser.CodProveGS, mlVersion, bNuevoWorkflow, True, mlVersionCert, bSacarNumLinea)

                oDsDefecto = oDS.Copy

                CopiaCampoDef_Padre = oCampo.Id

                If oDS.Tables.Count > 7 And bNuevoWorkflow = False Then
                    sTblLineas = "LINEAS_SIN"
                    sTblAdjuntos = "ADJUNTOS_SIN"
                    sTblEstados = "ESTADO_SIN"
                Else
                    sTblLineas = "LINEAS"
                    sTblAdjuntos = "ADJUNTOS"
                    sTblEstados = "ESTADO"
                End If
            Else
                If bPopUp Then bFnValorDefecto = True

                oCampo.Load(lCiaComp, sIdi, mlSolicitud)
                If EsQA Then
                    oDS = oCampo.LoadDesglose(lCiaComp, sIdi, oCampo.IdSolicitud)
                Else
                    oDS = oCampo.LoadDesglose(lCiaComp, sIdi, oCampo.IdSolicitud, True, oUser.CodProveGS, oUser.IdContacto)
                End If
                oDsDefecto = oDS.Copy

                sOrden = "ORDEN1"

                bEsNC = False 'Certificado

                sTblLineas = "LINEAS"
                sTblAdjuntos = "ADJUNTOS"
                sTblEstados = "ESTADO"
            End If

            If Not moAcciones Is Nothing AndAlso oCampo.Tipo = Fullstep.PMPortalServer.TipoCampoPredefinido.NoConformidad Then
            Else
                moAcciones = Nothing
                moDSEstados = Nothing
            End If

            If oDS.Tables.Count > 0 Then
                oDS.Tables(0).Columns.Add(New DataColumn("PRE_ID_CAMPO_NC", System.Type.GetType("System.Int32")))
                For Each oRow In oDS.Tables(0).Rows
                    oRow.Item("PRE_ID_CAMPO_NC") = 0
                Next
                If oDS.Tables.Contains("LINEAS") Then
                    oDS.Tables("LINEAS").Columns.Add(New DataColumn("PRE_ID_CAMPO_HIJO_NC", System.Type.GetType("System.Int32")))
                    For Each oRow In oDS.Tables("LINEAS").Rows
                        oRow.Item("PRE_ID_CAMPO_HIJO_NC") = 0
                    Next
                End If
            End If
            If Not moDSEstados Is Nothing Then
                If Not moDSEstados.Tables(0).Columns.Contains("PRE_ID_CAMPO_NC") Then
                    moDSEstados.Tables(0).Columns.Add(New DataColumn("PRE_ID_CAMPO_NC", System.Type.GetType("System.Int32")))
                End If
                For Each oRow In moDSEstados.Tables(0).Rows
                    oDS.Tables(0).ImportRow(oRow)
                    If bPopUp Then
                        oDS.Tables(0).Rows(oDS.Tables(0).Rows.Count - 1).Item("ID") = oRow.Item("ID").ToString
                        oDS.Tables(0).Rows(oDS.Tables(0).Rows.Count - 1).Item("PRE_ID_CAMPO_NC") = 0
                    Else
                        oDS.Tables(0).Rows(oDS.Tables(0).Rows.Count - 1).Item("PRE_ID_CAMPO_NC") = oCampo.IdGrupo.ToString
                        oDS.Tables(0).Rows(oDS.Tables(0).Rows.Count - 1).Item("ID") = oRow.Item("ID").ToString
                    End If
                Next
                For Each oRow In moDSEstados.Tables(1).Rows
                    oDS.Tables(1).ImportRow(oRow)
                Next
                If Not moDSEstados.Tables(2).Columns.Contains("PRE_ID_CAMPO_HIJO_NC") Then
                    moDSEstados.Tables(2).Columns.Add(New DataColumn("PRE_ID_CAMPO_HIJO_NC", System.Type.GetType("System.Int32")))
                End If
                If oDS.Tables(sTblLineas).Rows.Count > 0 Then
                    For Each oRow In moDSEstados.Tables(2).Select("CAMPO_PADRE= " & idCampo.ToString)
                        oNewRow = oDS.Tables(sTblLineas).NewRow
                        oNewRow.Item("LINEA") = oRow.Item("LINEA")
                        oNewRow.Item("CAMPO_PADRE") = oRow.Item("CAMPO_PADRE")
                        If bPopUp Then
                            oNewRow.Item("PRE_ID_CAMPO_HIJO_NC") = 0
                            oNewRow.Item("CAMPO_HIJO") = oRow.Item("CAMPO_HIJO").ToString
                        Else
                            oNewRow.Item("PRE_ID_CAMPO_HIJO_NC") = oCampo.IdGrupo.ToString
                            oNewRow.Item("CAMPO_HIJO") = oRow.Item("CAMPO_HIJO").ToString
                        End If
                        oNewRow.Item("VALOR_NUM") = oRow.Item("VALOR_NUM")
                        oNewRow.Item("VALOR_TEXT") = oRow.Item("VALOR_TEXT")
                        oNewRow.Item("VALOR_FEC") = oRow.Item("VALOR_FEC")
                        oDS.Tables(sTblLineas).Rows.Add(oNewRow)
                    Next
                End If
            End If

            Dim sSoloEliminar As String
            If oCampo.LineasPreconf > 0 Then
                tblGeneral.Rows.RemoveAt(1)
                sSoloEliminar = "Si"
            Else
                sSoloEliminar = "No"
            End If

            If oCampo.GMN1 <> Nothing Then
                sRestrictMat = oCampo.GMN1 + (Space(ilGMN1 - oCampo.GMN1.Length))
            End If

            If oCampo.GMN2 <> Nothing Then
                sRestrictMat += oCampo.GMN2 + (Space(ilGMN2 - oCampo.GMN2.Length))
            End If

            If oCampo.GMN3 <> Nothing Then
                sRestrictMat += oCampo.GMN3 + (Space(ilGMN3 - oCampo.GMN3.Length))
            End If

            If oCampo.GMN4 <> Nothing Then
                sRestrictMat += oCampo.GMN4 + (Space(ilGMN4 - oCampo.GMN4.Length))
            End If



            'Obtener la Fecha Limite para las acciones de la NoConformidad:
            If Not moDSEstados Is Nothing Then
                Dim oRowFECLIM() As DataRow = moDSEstados.Tables(2).Select("CAMPO_PADRE=" & idCampo.ToString & " AND CAMPO_HIJO=" & Fullstep.PMPortalServer.IdsFicticios.FechaLimite)
                If (Not oRowFECLIM Is Nothing) And (oRowFECLIM.Length > 0) Then
                    FechaLimite = DBNullToSomething(oRowFECLIM(0).Item("VALOR_FEC"))
                End If
            End If


            oRowHeader = New System.Web.UI.HtmlControls.HtmlTableRow
            otblRow = New System.Web.UI.HtmlControls.HtmlTableRow

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "claveArrayEntrys" + idCampo.ToString) Then
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "claveArrayEntrys" + idCampo.ToString, "<script>var arrEntrys_" + idCampo.ToString + " = new Array()</script>")
            End If
            Dim distinctCounts As IEnumerable(Of Int32)
            Dim lineasReal As Integer()

            If bPopUp Then
                iLineas = Request(nomDesglose + "$_numRows")
                iMaxIndex = Request(nomDesglose + "$_numTotRows")
                ReDim lineasReal(iMaxIndex - 1)
                Dim contador As Integer = 0
                For campoCalc As Integer = 1 To iMaxIndex
                    If Not Request(nomDesglose + "$_" + campoCalc.ToString + "__Deleted") Is Nothing Then
                        lineasReal(contador) = campoCalc
                        contador += 1
                    End If
                Next
                iLineas = contador
            Else
                If oDS.Tables(0).Rows.Count > 0 Then
                    iLineas = 0
                    If oDS.Tables(sTblLineas).Rows.Count > 0 Then
                        distinctCounts = From row In oDS.Tables(sTblLineas)
                                         Select row.Field(Of Int32)("LINEA")
                                         Order By "LINEA" Ascending
                                         Distinct
                        iLineas = distinctCounts.Count
                        iMaxIndex = oDS.Tables(sTblLineas).Select("", "LINEA DESC")(0).Item("LINEA")
                        ReDim lineasReal(iLineas - 1)
                        For contador As Integer = 0 To iLineas - 1
                            lineasReal(contador) = distinctCounts(contador)
                        Next
                        For Each oRow In oDS.Tables(sTblLineas).Rows
                            If Not (oRow.Item("CAMPO_HIJO").ToString <> oCampo.IdGrupo.ToString & Fullstep.PMPortalServer.IdsFicticios.FechaLimite) Then
                                dtFechaLimite = DBNullToSomething(oRow.Item("VALOR_FEC"))
                            End If
                        Next
                    Else
                        iMaxIndex = 0
                    End If
                    oDS.Tables(0).Rows(0).Item("LINEA") = iLineas
                End If

            End If

            'Para optimizar la velocidad de ejecuci�n de MontarFormularioSubmit en jsAlta.js
            Dim sJavaScript As String

            Dim conta As Integer = 0
            For i = 1 To iMaxIndex
                If lineasReal.Contains(i) Then
                    conta += 1
                    sJavaScript = "<script>htControlFilas[""" & idCampo & "_" & CStr(i) & """]=""" & CStr(conta) & """;</script>"
                    If Not Page.ClientScript.IsStartupScriptRegistered("ControlFilas" & idCampo & "_" & CStr(i)) Then _
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "ControlFilas" & idCampo & "_" & CStr(i), sJavaScript)
                End If
            Next

            Dim vLineasColumnas(iLineas) As Integer

            'Obtener la Fecha Limite para las acciones de la NoConformidad:
            If Not moDSEstados Is Nothing Then
                If dtFechaLimite <> Nothing Then
                    FechaLimite = dtFechaLimite
                End If
            End If

            keys(0) = oDS.Tables(sTblLineas).Columns("LINEA")
            keys(1) = oDS.Tables(sTblLineas).Columns("CAMPO_PADRE")
            keys(2) = oDS.Tables(sTblLineas).Columns("CAMPO_HIJO")
            keys(3) = oDS.Tables(2).Columns("PRE_ID_CAMPO_HIJO_NC")

            oIndexColumn = New DataColumn("INDEX")
            oIndexColumn.DataType = System.Type.GetType("System.Int32")
            oDS.Tables(sTblLineas).PrimaryKey = keys
            oDS.Tables(sTblLineas).Columns.Add(oIndexColumn)

            Me.tblDesglose.Rows.Add(oRowHeader)
            Dim sIdCampoDesglose As String = ""
            Dim sIdCampoHijoDesglose As String = ""
            For Each oRow In oDS.Tables(2).Rows
                If oRow.Item("PRE_ID_CAMPO_HIJO_NC") = 0 Then
                    sIdCampoHijoDesglose = oRow.Item("CAMPO_HIJO").ToString
                Else
                    sIdCampoHijoDesglose = oRow.Item("PRE_ID_CAMPO_HIJO_NC").ToString + oRow.Item("CAMPO_HIJO").ToString
                End If
                'Guardamos los campos GS ocultos
                Dim sOculto As String
                Dim oRow2 As DataRow
                For Each oRow2 In oDS.Tables(0).Rows
                    If (modUtilidades.DBNullToSomething(oRow.Item("TIPO_CAMPO_GS"))) Then

                        If (oRow2.Item("VISIBLE") = 0) And sIdCampoHijoDesglose = sIdCampoDesglose Then
                            Dim sLinea As String = oRow.Item("LINEA")
                            If modUtilidades.DBNullToSomething(oRow.Item("VALOR_NUM")) Is Nothing Then
                                sOculto = oRow.Item("TIPO_CAMPO_GS") & "_" & oRow.Item("CAMPO_PADRE") & "__" & oRow.Item("VALOR_TEXT") & "___" & oRow.Item("LINEA")
                            Else
                                sOculto = oRow.Item("TIPO_CAMPO_GS") & "_" & oRow.Item("CAMPO_PADRE") & "__" & oRow.Item("VALOR_NUM") & "___" & oRow.Item("LINEA")
                            End If
                            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "OcultosDes" & oRow.Item("TIPO_CAMPO_GS") & "_" & oRow.Item("CAMPO_PADRE") & "_" & sIdCampoHijoDesglose & "_" & oRow.Item("LINEA")) Then
                                Page.ClientScript.RegisterStartupScript(Page.GetType(), "OcultosDes" & oRow.Item("TIPO_CAMPO_GS") & "_" & oRow.Item("CAMPO_PADRE") & "_" & sIdCampoHijoDesglose & "_" & oRow.Item("LINEA"), "<script>arrGSOcultos[arrGSOcultos.length] = '" & sOculto & "'</script>")
                            End If
                        End If
                    End If
                Next
            Next

            If Not mbSoloLectura Then
                oCellHeader = New System.Web.UI.HtmlControls.HtmlTableCell
                oCellHeader.ID = "COL_Copiar_Eliminar"
                oCellHeader.InnerHtml = sCopiar_Eliminar
                oCellHeader.Attributes("class") = "cabeceraDesglose"
                oRowHeader.Cells.Add(oCellHeader)

                otblCell = New System.Web.UI.HtmlControls.HtmlTableCell
                oImg = New System.Web.UI.WebControls.HyperLink

                imgFotos = New System.Web.UI.WebControls.Image
                imgFotos.ImageUrl = "../_common/images/Flecha_Dcha.GIF"
                imgFotos.BorderWidth = Unit.Pixel(0)
                oImg.Controls.Add(imgFotos)

                oImg.NavigateUrl = "javascript:void(null)"
                oImg.Attributes("onclick") = "popupDesgloseClickEvent(this.id,'" + Me.ClientID + "','##newIndex##'," + idCampo.ToString() + ", 'izda' " + "," + oCampo.LineasPreconf.ToString() + ",event)"
                oImg.ID = "'##newIndex##'_cmdCopiar_Eliminar"

                otblCellData = New System.Web.UI.HtmlControls.HtmlTableCell
                otblCell.ID = "celdaCopiar_Eliminar"
                otblCell.Controls.Add(oImg)
                otblCell.Align = "center"
                otblRow.Cells.Add(otblCell)

            End If

            Dim sCodPaisDefecto As String = ""
            Dim sCodOrgCompraDefecto As String = ""
            Dim sCodCentroDefecto As String = ""
            Dim sCodUnidadOrgDefecto As String = ""

            If Not bFnValorDefecto Then
                CtrlHayDatosPorDefecto(oDS, oCampo, EsQA, sCodPaisDefecto, sCodOrgCompraDefecto, sCodCentroDefecto, sCodUnidadOrgDefecto, oUser.CodProveGS, oUser.IdContacto)
            Else
                CtrlHayDatosPorDefecto(oDsDefecto, oCampo, EsQA, sCodPaisDefecto, sCodOrgCompraDefecto, sCodCentroDefecto, sCodUnidadOrgDefecto, oUser.CodProveGS, oUser.IdContacto)
            End If
            Dim bAyuda As Boolean
            For Each oRow In oDS.Tables(0).Rows
                bAyuda = False 'Inicialmente no se muestra el icono de ayuda, se comprobara posteriormente
                'El Id del campo en el caso de los campos de desglose de acciones Comentario/Estado Interno lleva por delante el c�digo del grupo
                If oRow.Item("PRE_ID_CAMPO_NC") <> 0 Then
                    sIdCampoDesglose = oRow.Item("PRE_ID_CAMPO_NC").ToString + oRow.Item("ID").ToString
                Else
                    sIdCampoDesglose = oRow.Item("ID").ToString
                End If
                If modUtilidades.DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Material Then
                    sKeyMaterial = "fsentry" + sIdCampoDesglose
                    ReDim Preserve arrCamposMaterial(UBound(arrCamposMaterial) + 1)
                    arrCamposMaterial(UBound(arrCamposMaterial)) = oRow.Item("ID")

                ElseIf modUtilidades.DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
                    sKeyArticulo = "fsentry" + sIdCampoDesglose
                End If

                If oRow.Item("VISIBLE") = 1 Then
                    iTipo = oRow.Item("SUBTIPO")
                    iTipoGS = Fullstep.PMPortalWeb.DBNullToSomething(oRow.Item("TIPO_CAMPO_GS"))

                    'Cabecera de la columna
                    oDivHeader = New System.Web.UI.HtmlControls.HtmlGenericControl("DIV")
                    oDivHeader.Style.Item("width") = ObtenerWidth(iTipo, iTipoGS, (oRow.Item("ESCRITURA") = 0)).ToString + "px"

                    oCellHeader = New System.Web.UI.HtmlControls.HtmlTableCell
                    oCellHeader.ID = "COL_" + sIdCampoDesglose

                    Dim oLbl As System.Web.UI.HtmlControls.HtmlGenericControl
                    oLbl = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
                    If DBNullToSomething(oRow.Item("OBLIGATORIO")) = 1 Then
                        oLbl.InnerHtml = "(*)" + oRow.Item("DEN_" & sIdi)
                        oLbl.Style("font-weight") = "bold"
                    ElseIf DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.IdsFicticios.NumLinea Then
                        Dim modIdiomaAux As TiposDeDatos.ModulosIdiomas
                        modIdiomaAux = miPage.ModuloIdioma
                        miPage.ModuloIdioma = PMPortalServer.TiposDeDatos.ModulosIdiomas.GestionTraslado

                        oLbl.InnerHtml = miPage.Textos(33)
                        oRow.Item("DEN_" & sIdi) = miPage.Textos(33)
                        oLbl.Style("font-weight") = "bold"
                        miPage.ModuloIdioma = modIdiomaAux
                    Else
                        oLbl.InnerHtml = DBNullToSomething(oRow.Item("DEN_" & sIdi))
                    End If

                    If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.PrecioUnitario Then
                        oLbl.Attributes.Add("nombreMoneda", "")
                        oLbl.InnerHtml = oLbl.InnerHtml & " (" & If(mlInstancia > 0, msInstanciaMoneda, "") & ")"
                    End If

                    oRow.Item("DEN_" & sIdi) = oRow.Item("DEN_" & sIdi).ToString.Replace("'", "\'")
                    oRow.Item("DEN_" & sIdi) = oRow.Item("DEN_" & sIdi).ToString.Replace("""", "\""")

                    'Comprobaremos si se le tiene que poner la imagen de ayuda en la cabecera
                    'La comprobacion con "PRE_ID_CAMPO_NC" es debida a campos de No conformidades(campos de desglose de acciones Comentario/Estado Interno)
                    'que no tienen id en COPIA_CAMPO y por lo tanto no tendran ayuda, ya que no se le puede configurar la ayuda en el dise�o de formularios
                    If (Not IsDBNull(oRow.Item("AYUDA_" & sIdi)) OrElse Not IsDBNull(oRow.Item("FORMULA"))) AndAlso oRow.Item("PRE_ID_CAMPO_NC") = 0 Then
                        oImgAyuda = New System.Web.UI.WebControls.HyperLink
                        oImgAyuda.ImageUrl = "images\help.gif"
                        oImgAyuda.NavigateUrl = "javascript:void(null)"
                        If mlInstancia > 0 Then
                            oImgAyuda.Attributes("onclick") = "show_help(" + sIdCampoDesglose + "," + mlInstancia.ToString + ")"
                        Else
                            oImgAyuda.Attributes("onclick") = "show_help(" + sIdCampoDesglose + ",null," + mlSolicitud.ToString + ")"
                        End If
                        bAyuda = True
                    End If

                    If oRow.Item("TIPO") = 3 Then
                        oImg = New System.Web.UI.WebControls.HyperLink

                        imgFotos = New System.Web.UI.WebControls.Image
                        imgFotos.ImageUrl = "../_common/images/sumatorio.gif"
                        imgFotos.BorderWidth = Unit.Pixel(0)
                        oImg.Controls.Add(imgFotos)

                        oImg.NavigateUrl = "javascript:void(null)"

                        If mlInstancia > 0 Then
                            oImg.Attributes("onclick") = "show_help(" + sIdCampoDesglose + "," + mlInstancia.ToString + ")"
                        Else
                            oImg.Attributes("onclick") = "show_help(" + sIdCampoDesglose + ",null," + mlSolicitud.ToString + ")"
                        End If

                        Dim oTblSum As System.Web.UI.HtmlControls.HtmlTable
                        Dim oTblRowSum As System.Web.UI.HtmlControls.HtmlTableRow
                        Dim oTblCellSum As System.Web.UI.HtmlControls.HtmlTableCell

                        oTblSum = New System.Web.UI.HtmlControls.HtmlTable
                        oTblSum.Border = 0
                        oTblSum.CellPadding = 0
                        oTblSum.CellSpacing = 0
                        oTblSum.Width = "100%"
                        oTblRowSum = New System.Web.UI.HtmlControls.HtmlTableRow
                        oTblCellSum = New System.Web.UI.HtmlControls.HtmlTableCell
                        oTblCellSum.Width = "10%"
                        oTblCellSum.Controls.Add(oImg)
                        oTblRowSum.Controls.Add(oTblCellSum)
                        oTblSum.Controls.Add(oTblRowSum)
                        oTblCellSum = New System.Web.UI.HtmlControls.HtmlTableCell
                        oTblCellSum.Controls.Add(oLbl)
                        oTblCellSum.Attributes("class") = "cabeceraDesglose"
                        oTblRowSum.Controls.Add(oTblCellSum)

                        If bAyuda Then
                            'Se mete el icono de ayuda en la cabecera del campo
                            oTblCellSum = New System.Web.UI.HtmlControls.HtmlTableCell
                            oTblCellSum.Width = "5%"
                            oTblCellSum.Style("align") = "right"
                            oTblCellSum.Controls.Add(oImgAyuda)
                            oTblRowSum.Controls.Add(oTblCellSum)
                        End If
                        oDivHeader.Controls.Add(oTblSum)
                    Else
                        If bAyuda Then
                            Dim oTblSum As System.Web.UI.HtmlControls.HtmlTable
                            Dim oTblRowSum As System.Web.UI.HtmlControls.HtmlTableRow
                            Dim oTblCellSum As System.Web.UI.HtmlControls.HtmlTableCell

                            oTblSum = New System.Web.UI.HtmlControls.HtmlTable
                            oTblSum.Border = 0
                            oTblSum.CellPadding = 0
                            oTblSum.CellSpacing = 0
                            oTblSum.Width = "100%"
                            oTblRowSum = New System.Web.UI.HtmlControls.HtmlTableRow
                            oTblSum.Controls.Add(oTblRowSum)
                            oTblCellSum = New System.Web.UI.HtmlControls.HtmlTableCell
                            oTblCellSum.Controls.Add(oLbl)
                            oTblCellSum.Attributes("class") = "cabeceraDesglose"
                            oTblRowSum.Controls.Add(oTblCellSum)

                            'Se mete el icono de ayuda en la cabecera del campo
                            oTblCellSum = New System.Web.UI.HtmlControls.HtmlTableCell
                            oTblCellSum.Width = "5%"
                            oTblCellSum.Style("align") = "right"
                            oTblCellSum.Controls.Add(oImgAyuda)
                            oTblRowSum.Controls.Add(oTblCellSum)

                            oDivHeader.Controls.Add(oTblSum)
                        Else
                            oDivHeader.Controls.Add(oLbl)
                        End If

                    End If
                    oCellHeader.Controls.Add(oDivHeader)



                    iTipo = oRow.Item("SUBTIPO")
                    iTipoGS = Fullstep.PMPortalWeb.DBNullToSomething(oRow.Item("TIPO_CAMPO_GS"))

                    oCellHeader.Attributes("class") = "cabeceraDesglose"
                    oRowHeader.Cells.Add(oCellHeader)

                    otblCell = New System.Web.UI.HtmlControls.HtmlTableCell

                    oFSEntry = New Fullstep.DataEntry.GeneralEntry
                    oFSEntry.Portal = True
                    oFSEntry.ID = "fsentry" + sIdCampoDesglose
                    If mlInstancia > 0 Then
                        oFSEntry.CampoDef_CampoODesgHijo = oRow.Item("COPIA_CAMPO_DEF")
                        oFSEntry.CampoDef_DesgPadre = CopiaCampoDef_Padre
                    End If
                    oFSEntry.IdCampoPadre = idCampo
                    oFSEntry.TipoGS = iTipoGS
                    oFSEntry.Tipo = iTipo
                    oFSEntry.Intro = oRow.Item("INTRO")



                    'Cambio de las doble comillas para que en JavaScript no de error
                    If DBNullToSomething(oRow.Item("DEN_" & sIdi)) Is Nothing Then
                        oRow.Item("DEN_" & sIdi) = ""
                    Else
                        oRow.Item("DEN_" & sIdi) = HttpUtility.JavaScriptStringEncode(oRow.Item("DEN_" & sIdi))
                    End If

                    oFSEntry.Title = oRow.Item("DEN_" & sIdi)
                    oFSEntry.WindowTitle = Me.Page.Title

                    oFSEntry.Tag = sIdCampoDesglose
                    oFSEntry.DenGrupo = DBNullToStr(oRow.Item("DEN_GRUPO"))
                    oFSEntry.IsGridEditor = False
                    oFSEntry.IdContenedor = Me.ClientID
                    oFSEntry.TabContainer = msTabContainer
                    oFSEntry.ReadOnly = (oRow.Item("ESCRITURA") = 0)
                    oFSEntry.Obligatorio = (DBNullToSomething(oRow.Item("OBLIGATORIO")) = 1)
                    oFSEntry.BaseDesglose = True
                    oFSEntry.Calculado = (oRow.Item("TIPO") = 3 Or oRow.Item("SUBTIPO") = PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico) And Not IsDBNull(oRow.Item("ID_CALCULO"))
                    oFSEntry.Desglose = True
                    oFSEntry.NumberFormat = oUser.NumberFormat
                    oFSEntry.DateFormat = oUser.DateFormat
                    oFSEntry.Idi = sIdi
                    oFSEntry.IdAtrib = DBNullToSomething(oRow.Item("ID_ATRIB_GS"))
                    If oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.ArchivoEspecific Then
                        oFSEntry.Width = Unit.Percentage(ObtenerWidth(oFSEntry.Tipo, oFSEntry.TipoGS, oFSEntry.ReadOnly))
                    Else
                        oFSEntry.Width = Unit.Pixel(ObtenerWidth(oFSEntry.Tipo, oFSEntry.TipoGS, oFSEntry.ReadOnly))
                    End If
                    oFSEntry.Formato = DBNullToSomething(oRow.Item("FORMATO"))
                    miPage.ModuloIdioma = PMPortalServer.TiposDeDatos.ModulosIdiomas.DesgloseControl
                    oFSEntry.ErrMsgFormato = miPage.Textos(19)
                    oFSEntry.ActivoSM = CType(Me.Page, FSPMPage).Acceso.gbAccesoFSSM


                    Select Case oFSEntry.Tipo
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoString, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio
                            oFSEntry.InputStyle.CssClass = "TipoTextoMedio"

                            If oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo Then
                                oFSEntry.MaxLength = FSPMServer.LongitudesDeCodigos.giLongCodDENART
                            Else
                                If oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo Or oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
                                    oFSEntry.MaxLength = FSPMServer.LongitudesDeCodigos.giLongCodART
                                Else
                                    If PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio And Not IsDBNull(oRow.Item("MAX_LENGTH")) Then
                                        oFSEntry.MaxLength = oRow.Item("MAX_LENGTH")
                                    Else
                                        oFSEntry.MaxLength = 800
                                    End If
                                End If
                            End If
                            oFSEntry.ErrMsg = oTextos.Rows(11).Item(1)

                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico
                            oFSEntry.InputStyle.CssClass = "TipoNumero"
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoFecha
                            oFSEntry.InputStyle.CssClass = "TipoFecha"
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoCorto
                            If Not IsDBNull(oRow.Item("MAX_LENGTH")) Then
                                oFSEntry.MaxLength = oRow.Item("MAX_LENGTH")
                            Else
                                oFSEntry.MaxLength = 100
                            End If
                            oFSEntry.InputStyle.CssClass = "TipoTextoCorto"
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoLargo
                            oFSEntry.PopUpStyle = "../../App_Themes/" & Me.Page.Theme & "/" & Me.Page.Theme & ".css"
                            oFSEntry.PUCaptionBoton = oTextos.Rows(4).Item(1)
                            If Not IsDBNull(oRow.Item("MAX_LENGTH")) Then
                                oFSEntry.MaxLength = oRow.Item("MAX_LENGTH")
                            Else
                                oFSEntry.MaxLength = 4000
                            End If
                            oFSEntry.ErrMsg = oTextos.Rows(11).Item(1)

                            oFSEntry.InputStyle.CssClass = "TipoTextoLargo"
                            oFSEntry.ButtonStyle.CssClass = "TipoTextoLargoBoton"
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoArchivo
                            oFSEntry.InputStyle.CssClass = "TipoArchivo"
                            'oFSEntry.Width = Unit.Pixel(0)
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoBoolean
                            oFSEntry.Lista = Fullstep.PMPortalWeb.CommonInstancia.CrearDataset(sIdi, oTextos.Rows(16).Item(1), oTextos.Rows(17).Item(1))
                            'Me.Controls.Add(Fullstep.PMPortalWeb.CommonInstancia.alta.InsertarDropDown(oFSEntry.ID, msTabContainer, oFSEntry.Lista, oFSEntry.Tipo, sIdi, True))
                            oFSEntry.Intro = 1
                        Case TiposDeDatos.TipoGeneral.TipoEditor
                            oFSEntry.Text = oTextos.Rows(15).Item(1)
                    End Select
                    Select Case oFSEntry.Tipo
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoString, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoCorto, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoLargo
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico
                            oFSEntry.Maximo = modUtilidades.DBNullToSomething(oRow.Item("MAXNUM"))
                            oFSEntry.Minimo = modUtilidades.DBNullToSomething(oRow.Item("MINNUM"))
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoFecha
                            oFSEntry.Maximo = modUtilidades.DBNullToSomething(oRow.Item("MAXFEC"))
                            oFSEntry.Minimo = modUtilidades.DBNullToSomething(oRow.Item("MINFEC"))
                            oFSEntry.FechaNoAnteriorAlSistema = modUtilidades.DBNullToSomething(oRow.Item("FECHA_VALOR_NO_ANT_SIS"))
                    End Select
                    If oRow.Item("INTRO") = 1 Then
                        If oDS.Tables.Count = 1 Then
                            oFSEntry.Intro = 0
                        Else
                            oFSEntry.Lista = New DataSet
                            oFSEntry.Lista.Merge(oRow.GetChildRows("REL_CAMPO_LISTA"))
                        End If
                    End If
                    Select Case oFSEntry.TipoGS
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Material

                            oFSEntry.SelCualquierMatPortal = oRow.Item("SEL_CUALQUIER_MAT_PORTAL")  'Sandra

                            oFSEntry.Valor = sRestrictMat
                            Dim sMat As String = sRestrictMat
                            Dim lLongCod As Integer

                            For i = 1 To 4
                                arrMat(i) = ""
                            Next i

                            i = 1
                            While Trim(sMat) <> ""
                                Select Case i
                                    Case 1
                                        lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
                                    Case 2
                                        lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
                                    Case 3
                                        lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
                                    Case 4
                                        lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN4
                                End Select
                                arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
                                sMat = Mid(sMat, lLongCod + 1)
                                i = i + 1
                            End While

                            sToolTip = ""
                            sDenominacion = ""
                            If arrMat(4) <> "" Then
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") _
                                AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                Else
                                    oGMN3 = FSPMServer.Get_GrupoMatNivel3
                                    oGMN3.GMN1Cod = arrMat(1)
                                    oGMN3.GMN2Cod = arrMat(2)
                                    oGMN3.Cod = arrMat(3)
                                    oGMN3.CargarTodosLosGruposMatDesde(lCiaComp, 1, sIdi, arrMat(4), , True)
                                    sDenominacion = oGMN3.GruposMatNivel4.Item(arrMat(4)).Den
                                    oGMN3 = Nothing
                                End If
                                sToolTip = sDenominacion & " (" & arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) & " - " & arrMat(4) & ")"
                                If CType(Me.Page, FSPMPage).Acceso.gbMaterialVerTodosNiveles Then
                                    sValor = arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + arrMat(4) + " - " + sDenominacion
                                Else
                                    sValor = arrMat(4) + " - " + sDenominacion
                                End If
                            ElseIf arrMat(3) <> "" Then
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") _
                                AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                Else
                                    oGMN2 = FSPMServer.Get_GrupoMatNivel2
                                    oGMN2.GMN1Cod = arrMat(1)
                                    oGMN2.Cod = arrMat(2)
                                    oGMN2.CargarTodosLosGruposMatDesde(lCiaComp, 1, sIdi, arrMat(3), , True)
                                    sDenominacion = oGMN2.GruposMatNivel3.Item(arrMat(3)).Den

                                    oGMN2 = Nothing
                                End If
                                sToolTip = sDenominacion & "(" & arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) & ")"
                                If CType(Me.Page, FSPMPage).Acceso.gbMaterialVerTodosNiveles Then
                                    sValor = arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + sDenominacion
                                Else
                                    sValor = arrMat(3) + " - " + sDenominacion
                                End If
                            ElseIf arrMat(2) <> "" Then
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") _
                                AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                Else
                                    oGMN1 = FSPMServer.Get_GrupoMatNivel1
                                    oGMN1.Cod = arrMat(1)
                                    oGMN1.CargarTodosLosGruposMatDesde(lCiaComp, 1, sIdi, arrMat(2), , True)
                                    sDenominacion = oGMN1.GruposMatNivel2.Item(arrMat(2)).Den

                                    oGMN1 = Nothing
                                End If
                                sToolTip = sDenominacion & "(" & arrMat(1) & " - " & arrMat(2) & ")"
                                If CType(Me.Page, FSPMPage).Acceso.gbMaterialVerTodosNiveles Then
                                    sValor = arrMat(1) + " - " + arrMat(2) + " - " + sDenominacion
                                Else
                                    sValor = arrMat(2) + " - " + sDenominacion
                                End If

                            ElseIf arrMat(1) <> "" Then
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") _
                                AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                Else
                                    oGMN1s = FSPMServer.Get_GruposMatNivel1
                                    oGMN1s.CargarTodosLosGruposMatDesde(lCiaComp, 1, sIdi, arrMat(1), , , True)
                                    sDenominacion = oGMN1s.Item(arrMat(1)).Den

                                    oGMN1s = Nothing
                                End If
                                sToolTip = sDenominacion & "(" & arrMat(1) & ")"
                                sValor = arrMat(1) + " - " + sDenominacion
                            End If
                            oFSEntry.Text = sValor
                            oFSEntry.ToolTip = sToolTip
                            oFSEntry.Denominacion = sDenominacion

                            sKeyMaterial = oFSEntry.ClientID
                            oFSEntry.Restric = sRestrictMat



                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo, PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo, PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo
                            If oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
                                'oFSEntry.Width = Unit.Pixel(200)
                                oFSEntry.AnyadirArt = oRow.Item("ANYADIR_ART")
                            Else
                                oFSEntry.Width = Unit.Pixel(400)
                            End If
                            oFSEntry.Restric = sRestrictMat
                            oFSEntry.DependentField = sKeyMaterial
                            oFSEntry.Intro = 1
                            oFSEntry.InstanciaMoneda = msInstanciaMoneda

                            If idDataEntryFORMOrgCompras <> "" Then ' Tiene un campo Organizacion de compras antes que el desglose!!!
                                oFSEntry.idDataEntryDependent = idDataEntryFORMOrgCompras
                            Else
                                Dim sIDOrgCompras As String
                                If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                    If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1").Length > 0 Then
                                        sIDOrgCompras = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1")(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1").Length > 0 Then
                                        sIDOrgCompras = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1")(0).Item("ID")
                                    End If
                                End If
                                If sIDOrgCompras <> "" Then
                                    oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry_" & sIDOrgCompras
                                End If
                            End If

                            Dim sIDCentro As String
                            If idDataEntryFORMCentro <> "" Then ' Tiene un campo Centro antes que el desglose!!!
                                oFSEntry.idDataEntryDependent2 = idDataEntryFORMCentro
                            Else
                                If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                    If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                                        sIDCentro = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                    End If
                                Else
                                    If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                                        sIDCentro = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Centro & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                                    End If
                                End If
                                If sIDCentro <> "" Then
                                    oFSEntry.idDataEntryDependent2 = oFSEntry.IdContenedor & "_" & "fsentry_" & sIDCentro
                                End If
                            End If

                            Dim sKeyUnidad As String
                            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                                    sKeyUnidad = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                End If
                            Else
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                                    sKeyUnidad = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                                End If
                            End If
                            If sKeyUnidad <> "" Then oFSEntry.idEntryUNI = oFSEntry.IdContenedor & "_" & "fsentry_" & sKeyUnidad


                            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oRow.Item("GRUPO")).Length Then
                                If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                    oFSEntry.IdMaterialOculta = oFSEntry.IdContenedor & "_" & "fsentry_" & DBNullToSomething(oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
                                Else
                                    oFSEntry.IdMaterialOculta = oFSEntry.IdContenedor & "_" & "fsentry_" & DBNullToSomething(oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Material & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID"))
                                End If
                            End If
                            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oRow.Item("GRUPO")).Length Then
                                If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                    oFSEntry.IdCodArticuloOculta = oFSEntry.IdContenedor & "_" & "fsentry_" & DBNullToSomething(oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
                                Else
                                    oFSEntry.IdCodArticuloOculta = oFSEntry.IdContenedor & "_" & "fsentry_" & DBNullToSomething(oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID"))
                                End If
                            ElseIf oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oRow.Item("GRUPO")).Length Then
                                If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                    oFSEntry.IdCodArticuloOculta = oFSEntry.IdContenedor & "_" & "fsentry_" & DBNullToSomething(oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
                                Else
                                    oFSEntry.IdCodArticuloOculta = oFSEntry.IdContenedor & "_" & "fsentry_" & DBNullToSomething(oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID"))
                                End If
                            End If
                            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oRow.Item("GRUPO")).Length Then
                                If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                    oFSEntry.IdDenArticuloOculta = oFSEntry.IdContenedor & "_" & "fsentry_" & DBNullToSomething(oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO"))
                                Else
                                    oFSEntry.IdDenArticuloOculta = oFSEntry.IdContenedor & "_" & "fsentry_" & DBNullToSomething(oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID"))
                                End If
                            End If


                            'Relaci�n Con el proveedor
                            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_ULT_ADJ=1").Length > 0 Then
                                    sKeyProveedor = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_ULT_ADJ=1")(0).Item("ID_CAMPO")
                                End If
                            Else
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_ULT_ADJ=1").Length > 0 Then
                                    sKeyProveedor = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_ULT_ADJ=1")(0).Item("ID")
                                End If
                            End If

                            If sKeyProveedor <> "" Then oFSEntry.idEntryPROV = oFSEntry.IdContenedor & "_" & "fsentry_" & sKeyProveedor

                            'Relaci�n Con el Precio
                            Dim sKeyPrecio As String
                            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.PrecioUnitario & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_ULT_ADJ=1").Length > 0 Then
                                    sKeyPrecio = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.PrecioUnitario & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_ULT_ADJ=1")(0).Item("ID_CAMPO")
                                End If
                            Else
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.PrecioUnitario & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_ULT_ADJ=1").Length > 0 Then
                                    sKeyPrecio = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.PrecioUnitario & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 AND CARGAR_ULT_ADJ=1")(0).Item("ID")
                                End If
                            End If
                            If sKeyPrecio <> "" Then oFSEntry.idEntryPREC = oFSEntry.IdContenedor & "_" & "fsentry_" & sKeyPrecio
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.ProveedorERP
                            'El campo Gs proveedor ERP dependera de 2 campos, el campo proveedor y el campo organizacion de compras para mostrar sus valores

                            If bFnValorDefecto Then
                                Dim oProvesERP As PMPortalServer.ProveedoresERP
                                oProvesERP = FSPMServer.Get_ProveedoresERP

                                oFSEntry.Lista = oProvesERP.CargarProveedoresERPtoDS(lCiaComp)
                            End If

                            Dim sIDOrgCompras As String = String.Empty
                            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                                    sIDOrgCompras = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                End If
                            Else
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                                    sIDOrgCompras = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                                End If
                            End If
                            If sIDOrgCompras = "" AndAlso idDataEntryFORMOrgCompras <> "" Then ' Tiene un campo Organizacion de compras antes que el desglose a nivel de formulario, viene de campos.ascx Then
                                oFSEntry.idDataEntryDependent = idDataEntryFORMOrgCompras
                            ElseIf sIDOrgCompras <> "" Then 'El campo organizacion de compras estara en el propio desglose
                                oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry_" & sIDOrgCompras
                            End If

                            'Si el campo proveedor ERP esta a nivel de desglose el campo Proveedor tiene que estar tambien a nivel de desglose
                            Dim sIDProveedorDeProveERP As String = String.Empty
                            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                                    sIDProveedorDeProveERP = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                End If
                            Else
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                                    sIDProveedorDeProveERP = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                                End If
                            End If
                            If sIDProveedorDeProveERP <> "" Then
                                oFSEntry.idEntryPROV = oFSEntry.IdContenedor & "_" & "fsentry_" & sIDProveedorDeProveERP
                            End If
                            oFSEntry.Intro = 1
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Dest
                            If bFnValorDefecto Then
                                Dim oDests As PMPortalServer.Destinos
                                oDests = FSPMServer.Get_Destinos()
                                oDests.LoadData(sIdi, lCiaComp, oUser.AprobadorActual)

                                oFSEntry.Lista = oDests.Data
                            End If
                            oFSEntry.Intro = 1
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Cantidad
                            If sKeyTipoRecepcion = "1" Then
                                oFSEntry.Text = 1
                            End If
                            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeRecep") Then
                                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeRecep", "<script>var smensajeRecep = '" & oTextos.Rows.Item(18).Item(1) & "'</script>")
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad
                            If bFnValorDefecto Then
                                Dim oUnis As PMPortalServer.Unidades
                                oUnis = FSPMServer.get_Unidades()
                                oUnis.LoadData(sIdi, lCiaComp, , , False)

                                oFSEntry.Lista = oUnis.Data
                            End If
                            oFSEntry.Intro = 1
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.FormaPago
                            If bFnValorDefecto Then
                                Dim oPags As PMPortalServer.FormasPago
                                oPags = FSPMServer.Get_FormasPago()
                                oPags.LoadData(sIdi, lCiaComp)

                                oFSEntry.Lista = oPags.Data
                            End If
                            oFSEntry.Intro = 1
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Moneda
                            If bFnValorDefecto Then
                                Dim oMons As PMPortalServer.Monedas
                                oMons = FSPMServer.Get_Monedas
                                oMons.LoadData(sIdi, lCiaComp)

                                oFSEntry.Lista = oMons.Data
                            End If
                            oFSEntry.Intro = 1
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Pais
                            sKeyPais = oFSEntry.ID
                            If bFnValorDefecto Then
                                Dim oPaises2 As PMPortalServer.Paises
                                oPaises2 = FSPMServer.Get_Paises()
                                oPaises2.LoadData(sIdi, lCiaComp)

                                oFSEntry.Lista = oPaises2.Data
                                oPaises2 = Nothing
                            End If
                            oFSEntry.Intro = 1

                            'Relaci�n con Provincia
                            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Provincia & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                                    sKeyProvincia = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Provincia & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                End If
                            Else
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Provincia & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                                    sKeyProvincia = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Provincia & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                                End If
                            End If
                            If Not sKeyProvincia Is Nothing Then
                                oFSEntry.DependentField = "fsentry" & sKeyProvincia
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Provincia
                            'oFSEntry.DependentField = sKeyPais

                            'Rellenamos lista de datos del control s�lo para que pueda coger el DEN del valor por defecto
                            'en la funci�n CargarValoresDefecto. All� vaciamos la lista porque un webdropdown 11.1 con carga desde javascript habilitada no puede cargarse en servidor, 
                            'Los datos salen mal en pantalla
                            If bFnValorDefecto _
                                AndAlso (Not oFSEntry.ReadOnly) Then
                                If sCodPaisDefecto <> Nothing Then
                                    Dim oProvis As PMPortalServer.Provincias
                                    oProvis = FSPMServer.Get_Provincias()
                                    oProvis.Pais = DBNullToStr(sCodPaisDefecto)
                                    oProvis.LoadData(sIdi, lCiaComp)

                                    oFSEntry.Lista = oProvis.Data
                                End If
                            End If

                            If sKeyPais <> String.Empty Then _
                                oFSEntry.MasterField = sKeyPais

                            oFSEntry.Intro = 1

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.PRES1, PMPortalServer.TiposDeDatos.TipoCampoGS.Pres2, PMPortalServer.TiposDeDatos.TipoCampoGS.Pres3, PMPortalServer.TiposDeDatos.TipoCampoGS.Pres4
                            ''oFSEntry.Width = Unit.Pixel(200)
                            If Not oFSEntry.ReadOnly Then
                                oFSEntry.Text = oTextos.Rows(1).Item(1)
                            End If

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Subtipo


                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.ImporteRepercutido
                            oFSEntry.Texto_Defecto = DBNullToSomething(oRow.Item("VALOR_NUM"))

                            oFSEntry.MonCentral = oRow.Item("MONCEN")
                            oFSEntry.CambioCentral = oRow.Item("CAMBIOCEN")

                            oFSEntry.MonRepercutido = oFSEntry.MonCentral
                            oFSEntry.CambioRepercutido = oFSEntry.CambioCentral

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Proveedor
                            If idDataEntryFORMOrgCompras <> "" Then ' Tiene un campo Organizacion de compras antes que el desglose!!!
                                oFSEntry.idDataEntryDependent = idDataEntryFORMOrgCompras
                            End If
                            If Not oFSEntry.ReadOnly Then
                                oFSEntry.Text = oTextos.Rows(2).Item(1)
                            End If
                            If Not IsDBNull(oRow.Item("CARGAR_ULT_ADJ")) Then
                                oFSEntry.CargarUltADJ = oRow.Item("CARGAR_ULT_ADJ")
                            Else
                                oFSEntry.CargarUltADJ = False
                            End If
                            Dim sCampoDepend As String = ""
                            Dim sql As String
                            sql = "TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 "

                            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                If oDS.Tables(0).Select(sql).Length > 0 Then
                                    sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID_CAMPO")
                                End If
                            Else
                                If oDS.Tables(0).Select(sql).Length > 0 Then
                                    sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID")
                                End If
                            End If
                            If sCampoDepend <> "" Then
                                oFSEntry.DependentField = oFSEntry.IdContenedor & "_" & "fsentry_" & sCampoDepend
                            End If

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.EstadoNoConf
                            Dim oEsts As PMPortalServer.EstadosNoConf
                            oEsts = FSPMServer.get_EstadosNoConf()
                            oEsts.LoadData(lCiaComp, mlInstancia, "", sIdi)
                            oFSEntry.Lista = oEsts.Data
                            oFSEntry.Intro = 1
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Fec_inicio, PMPortalServer.TiposDeDatos.TipoCampoGS.Fec_cierre
                            If (Not FechaLimite = Nothing) Then
                                oFSEntry.Maximo = FechaLimite
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.EstadoInternoNoConf
                            oFSEntry.Valor = 1
                            oFSEntry.Text = oTextos.Rows(5).Item(1)

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.UnidadOrganizativa
                            oFSEntry.Width = Unit.Pixel(200)
                            oFSEntry.Intro = 1
                            oFSEntry.Orden = oRow.Item(sOrden)
                            sKeyUnidadOrganizativa = oFSEntry.ID

                            If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Departamento & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                                Dim sKeyDepartamento As String = String.Empty
                                If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                    sKeyDepartamento = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Departamento & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                Else
                                    sKeyDepartamento = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Departamento & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                                End If
                                oFSEntry.DependentField = "fsentry" & sKeyDepartamento
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Departamento
                            oFSEntry.Intro = 1
                            'Rellenamos lista de datos del control s�lo para que pueda coger el DEN del valor por defecto
                            'en la funci�n CargarValoresDefecto. All� vaciamos la lista porque un webdropdown 11.1 con carga desde javascript habilitada no puede cargarse en servidor, 
                            'Los datos salen mal en pantalla
                            If bFnValorDefecto _
                                AndAlso (Not oFSEntry.ReadOnly) Then
                                Dim oDepartamentos As PMPortalServer.Departamentos
                                oDepartamentos = FSPMServer.Get_Departamentos

                                If sCodUnidadOrgDefecto <> "" Then
                                    Dim vUnidadesOrganizativas(3) As String
                                    vUnidadesOrganizativas = Split(sCodUnidadOrgDefecto, " - ")

                                    Select Case UBound(vUnidadesOrganizativas)
                                        Case 0
                                            oDepartamentos.LoadData(lCiaComp, 0)
                                            oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                        Case 1
                                            oDepartamentos.LoadData(lCiaComp, 1, vUnidadesOrganizativas(0))
                                            oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                        Case 2
                                            oDepartamentos.LoadData(lCiaComp, 2, vUnidadesOrganizativas(0), vUnidadesOrganizativas(1))
                                            oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                        Case 3
                                            oDepartamentos.LoadData(lCiaComp, 3, vUnidadesOrganizativas(0), vUnidadesOrganizativas(1), vUnidadesOrganizativas(2))
                                            oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                    End Select

                                Else
                                    oDepartamentos.LoadData(lCiaComp)
                                    oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                End If
                                oFSEntry.Lista = oDepartamentos.Data
                            End If
                            oFSEntry.Orden = oRow.Item(sOrden)

                            If sKeyUnidadOrganizativa <> String.Empty Then _
                                oFSEntry.MasterField = sKeyUnidadOrganizativa

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras
                            If bFnValorDefecto Then
                                Dim oOrganizacionesCompras As PMPortalServer.OrganizacionesCompras
                                oOrganizacionesCompras = FSPMServer.Get_OrganizacionesCompras
                                oOrganizacionesCompras.LoadData(lCiaComp)

                                oFSEntry.Lista = oOrganizacionesCompras.Data
                            End If

                            oFSEntry.Intro = 1
                            oFSEntry.Orden = oRow.Item(sOrden)
                            Dim sCampoDepend As String
                            Dim sql As String
                            sql = "(TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =" & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & ") AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 "

                            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                If oDS.Tables(0).Select(sql).Length > 0 Then
                                    sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID_CAMPO")
                                End If
                            Else
                                If oDS.Tables(0).Select(sql).Length > 0 Then
                                    sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID")
                                End If
                            End If
                            oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry_" & sCampoDepend

                            sql = "TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Proveedor & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 "

                            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                If oDS.Tables(0).Select(sql).Length > 0 Then
                                    sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID_CAMPO")
                                End If
                            Else
                                If oDS.Tables(0).Select(sql).Length > 0 Then
                                    sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID")
                                End If
                            End If
                            If sCampoDepend <> "" Then
                                oFSEntry.DependentField = oFSEntry.IdContenedor & "_" & "fsentry_" & sCampoDepend
                            End If

                            sql = "(TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Centro & ") AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 "
                            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then ' Relacionar la Organizacion de Compras con el Centro
                                If oDS.Tables(0).Select(sql).Length > 0 Then
                                    sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID_CAMPO")
                                End If
                            Else
                                If oDS.Tables(0).Select(sql).Length > 0 Then
                                    sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID")
                                End If
                            End If
                            If sCampoDepend <> "" Then
                                oFSEntry.idDataEntryDependent2 = oFSEntry.IdContenedor & "_" & "fsentry_" & sCampoDepend
                            End If
                            sKeyOrgComprasDataCheck = oFSEntry.ID

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Centro
                            'Rellenamos lista de datos del control s�lo para que pueda coger el DEN del valor por defecto
                            'en la funci�n CargarValoresDefecto. All� vaciamos la lista porque un webdropdown 11.1 con carga desde javascript habilitada no puede cargarse en servidor, 
                            'Los datos salen mal en pantalla
                            If bFnValorDefecto _
                                AndAlso (Not oFSEntry.ReadOnly) Then

                                Dim oCentros As PMPortalServer.Centros
                                oCentros = FSPMServer.Get_Centros
                                If sCodOrgCompraDefecto <> "" Then
                                    oCentros.LoadData(lCiaComp, , DBNullToStr(sCodOrgCompraDefecto))
                                Else
                                    oCentros.LoadData(lCiaComp)
                                End If

                                oFSEntry.Lista = oCentros.Data
                            End If

                            If sKeyOrgComprasDataCheck <> String.Empty Then _
                                oFSEntry.MasterField = sKeyOrgComprasDataCheck

                            oFSEntry.Intro = 1
                            oFSEntry.Orden = oRow.Item(sOrden)
                            sIDCentro2 = oFSEntry.Tag

                            sScript = ""
                            ''Variables de la ORGANIZACION DE COMPRAS (Carga las Varibles JS de la pag jsAlta.js)
                            ''***************************************
                            sScript += "var bCentroRelacionadoFORM" & sIDCampoOrgCompras & " = " + IIf(bCentroRelacionadoFORM, "true", "false") + ";"
                            If Not IsNothing(sCodOrgComprasFORM) Then
                                sScript += "var sCodOrgComprasFORM" & sIDCampoOrgCompras & " = '" + sCodOrgComprasFORM.ToString() + "' ;"
                            Else
                                sScript += "var sCodOrgComprasFORM" & sIDCampoOrgCompras & " ;"
                            End If
                            oFSEntry.DependentField = sIDCampoOrgCompras
                            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                            If Not Page.ClientScript.IsStartupScriptRegistered("VariablesORGANIZACIONCOMPRAS" & oFSEntry.Tag) Then
                                Page.ClientScript.RegisterStartupScript(Page.GetType, "VariablesORGANIZACIONCOMPRAS" & oFSEntry.Tag, sScript)
                            End If

                            If Not sidDataEntryFORMOrgCompras Is Nothing Then
                                oFSEntry.idDataEntryDependent2 = sidDataEntryFORMOrgCompras
                            End If
                            'Relacionar el Centro con el articulo, (Para cuando se elimine el centro que se elimine el articulo)
                            Dim sCampoDepend As String
                            Dim sql As String
                            sql = "(TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo & " OR TIPO_CAMPO_GS =" & PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo & ") AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 "

                            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                If oDS.Tables(0).Select(sql).Length > 0 Then
                                    sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID_CAMPO")
                                End If
                            Else
                                If oDS.Tables(0).Select(sql).Length > 0 Then
                                    sCampoDepend = oDS.Tables(0).Select(sql)(0).Item("ID")
                                End If
                            End If
                            oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry_" & sCampoDepend

                            'Relaci�n con Almacen
                            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                                    sKeyAlmacen = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                End If
                            Else
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oRow.Item("GRUPO") & " AND VISIBLE = 1 ").Length > 0 Then
                                    sKeyAlmacen = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Almacen & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                                End If
                            End If
                            If Not sKeyAlmacen Is Nothing Then
                                oFSEntry.idDataEntryDependent3 = oFSEntry.IdContenedor & "_" & "fsentry" & sKeyAlmacen
                            End If

                            sKeyCentro = oFSEntry.ID

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Almacen
                            'Rellenamos lista de datos del control s�lo para que pueda coger el DEN del valor por defecto
                            'en la funci�n CargarValoresDefecto. All� vaciamos la lista porque un webdropdown 11.1 con carga desde javascript habilitada no puede cargarse en servidor, 
                            'Los datos salen mal en pantalla
                            If bFnValorDefecto AndAlso (Not oFSEntry.ReadOnly) Then
                                Dim oAlmacenes As PMPortalServer.Almacenes
                                oAlmacenes = FSPMServer.Get_Almacenes
                                If sCodCentroDefecto = "" Then
                                    oAlmacenes.LoadData(lCiaComp)
                                Else
                                    oAlmacenes.LoadData(lCiaComp, , DBNullToStr(sCodCentroDefecto))
                                End If

                                oFSEntry.Lista = oAlmacenes.Data
                            End If

                            If sKeyCentro <> String.Empty Then _
                                oFSEntry.MasterField = sKeyCentro

                            oFSEntry.Intro = 1
                            oFSEntry.Orden = oRow.Item(sOrden)

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Empresa
                            'Rellenamos lista de datos del control s�lo para que pueda coger el DEN del valor por defecto
                            'en la funci�n CargarValoresDefecto. All� vaciamos la lista porque un webdropdown 11.1 con carga desde javascript habilitada no puede cargarse en servidor, 
                            'Los datos salen mal en pantalla
                            If bFnValorDefecto AndAlso (Not oFSEntry.ReadOnly) Then
                                Dim oEmpresas As PMPortalServer.Empresas
                                oEmpresas = FSPMServer.Get_Empresas
                                Dim dt As DataTable
                                dt = oEmpresas.ObtenerEmpresas(lCiaComp, sIdi)

                                Dim ds As New DataSet
                                ds.Tables.Add(dt)
                                oFSEntry.Lista = ds
                            End If

                            oFSEntry.Intro = 1
                            oFSEntry.Orden = oRow.Item(sOrden)

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.IniSuministro
                            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechas") Then
                                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechas", "<script>var sMensajeFecha = '" & oTextos.Rows.Item(10).Item(1) & "'</script>")
                            End If

                            Dim sCampoFinSuministro As String
                            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.FinSuministro & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                                    sCampoFinSuministro = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.FinSuministro & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                End If
                            Else
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.FinSuministro & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                                    sCampoFinSuministro = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.FinSuministro & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                                End If
                            End If
                            If sCampoFinSuministro <> "" Then
                                oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry_" & sCampoFinSuministro
                            End If

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.FinSuministro

                            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechas") Then
                                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechas", "<script>var sMensajeFecha = '" & oTextos.Rows.Item(10).Item(1) & "'</script>")
                            End If
                            Dim sCampoIniSuministro As String
                            If Not oDS.Tables(0).Columns("id_campo") Is Nothing Then
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.IniSuministro & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                                    sCampoIniSuministro = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.IniSuministro & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID_CAMPO")
                                End If
                            Else
                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.IniSuministro & " AND GRUPO = " & oRow.Item("GRUPO")).Length > 0 Then
                                    sCampoIniSuministro = oDS.Tables(0).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.IniSuministro & " AND GRUPO = " & oRow.Item("GRUPO"))(0).Item("ID")
                                End If
                            End If
                            If sCampoIniSuministro <> "" Then
                                oFSEntry.idDataEntryDependent = oFSEntry.IdContenedor & "_" & "fsentry_" & sCampoIniSuministro
                            End If

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.PrecioUnitario

                            If Not IsDBNull(oRow.Item("CARGAR_ULT_ADJ")) Then
                                oFSEntry.CargarUltADJ = oRow.Item("CARGAR_ULT_ADJ")
                            Else
                                oFSEntry.CargarUltADJ = False
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.CentroCoste
                            If Not IsDBNull(oRow.Item("VER_UON")) Then
                                oFSEntry.VerUON = oRow.Item("VER_UON")
                            Else
                                oFSEntry.VerUON = False
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Partida
                            oFSEntry.PRES5 = oRow.Item("PRES5")
                            If Not IsDBNull(oRow.Item("CENTRO_COSTE")) Then
                                oFSEntry.idEntryCC = oRow.Item("CENTRO_COSTE")
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Activo
                            If Not IsDBNull(oRow.Item("CENTRO_COSTE")) Then
                                oFSEntry.idEntryCC = oRow.Item("CENTRO_COSTE")
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.AnyoPartida
                            oFSEntry.Intro = 1
                            If Not IsDBNull(oRow.Item("PARTIDAPRES")) Then
                                Dim Quien As String = "ID"
                                If Not oDS.Tables(0).Columns("COPIA_CAMPO_DEF") Is Nothing Then Quien = "COPIA_CAMPO_DEF"

                                If oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Partida & " AND " & Quien & " = " & oRow.Item("PARTIDAPRES")).Length = 0 Then
                                    'Es un campo
                                    If Not oDS.Tables(0).Columns("COPIA_CAMPO_DEF") Is Nothing Then
                                        oFSEntry.idEntryPartidaPlurianual = oFSEntry.TabContainer & "_" & "fsentry" & idCampoPartida
                                    Else
                                        oFSEntry.idEntryPartidaPlurianual = oFSEntry.TabContainer & "_" & "fsentry" & oRow.Item("PARTIDAPRES")
                                    End If
                                    oFSEntry.EntryPartidaPlurianualCampo = 1
                                Else 'esta en desglose
                                    If Not oDS.Tables(0).Columns("COPIA_CAMPO_DEF") Is Nothing Then
                                        oFSEntry.idEntryPartidaPlurianual = oFSEntry.IdContenedor & "_" & "fsentry" & oDS.Tables(0).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.Partida & " AND COPIA_CAMPO_DEF = " & oRow.Item("PARTIDAPRES"))(0).Item("ID")
                                    Else
                                        oFSEntry.idEntryPartidaPlurianual = oFSEntry.IdContenedor & "_" & "fsentry" & oRow.Item("PARTIDAPRES")
                                    End If
                                    oFSEntry.EntryPartidaPlurianualCampo = 0
                                End If
                            End If

                    End Select
                    If oRow.Item("TIPO") = 6 Then
                        Dim bSinKeyArticulo As Boolean = True
                        oFSEntry.Tabla_Externa = oRow.Item("TABLA_EXTERNA")
                        oFSEntry.Width = Unit.Pixel(250)
                        If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") _
                        AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                            oFSEntry.Text = oRow.Item("VALOR_TEXT_DEN")
                            If Not IsDBNull(oRow.Item("VALOR_TEXT_COD")) Then
                                oFSEntry.codigoArticulo = oRow.Item("VALOR_TEXT_COD")
                                bSinKeyArticulo = False
                            End If
                        Else
                            Dim oTablaExterna As PMPortalServer.TablaExterna = FSPMServer.get_TablaExterna
                            oTablaExterna.LoadDefTabla(lCiaComp, oFSEntry.Tabla_Externa)
                            oTablaExterna.LoadData(lCiaComp, False)
                            If Not oFSEntry.Valor Is Nothing Then
                                oFSEntry.Text = oTablaExterna.DescripcionReg(lCiaComp, oFSEntry.Valor, sIdi, oUser.DateFormat, oUser.NumberFormat)
                                oFSEntry.codigoArticulo = oTablaExterna.CodigoArticuloReg(oFSEntry.Valor)
                            End If
                            bSinKeyArticulo = oTablaExterna.ArtKey Is Nothing
                        End If
                        oFSEntry.Denominacion = oFSEntry.Text
                        If sKeyArticulo Is Nothing Then
                            For Each oRowaux As DataRow In oDS.Tables(0).Rows
                                If DBNullToSomething(oRowaux.Item("TIPO_CAMPO_GS")) = Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
                                    If Not oDS.Tables(0).Columns("ID_CAMPO") Is Nothing Then
                                        sKeyArticulo = "fsentry" + oRowaux.Item("ID_CAMPO").ToString
                                    Else
                                        sKeyArticulo = "fsentry" + oRowaux.Item("ID").ToString
                                    End If
                                    Exit For
                                End If
                            Next
                        End If
                        If Not (sKeyArticulo Is Nothing Or bSinKeyArticulo) Then _
                            oFSEntry.DependentField = sKeyArticulo
                    ElseIf oRow.Item("TIPO") = 7 Then
                        oFSEntry.Servicio = oRow.Item("SERVICIO")
                    End If
                    If oFSEntry.Intro = 1 _
                    AndAlso Not (oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.Dest) _
                    AndAlso Not (oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.CodArticulo) _
                    AndAlso Not (oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.DenArticulo) _
                    AndAlso Not (oFSEntry.TipoGS = TiposDeDatos.TipoCampoGS.NuevoCodArticulo) Then
                        'destino 250px
                        'CodArticulo    DenArticulo     400px
                        'resto combos
                        Select Case oFSEntry.Tipo
                            Case TiposDeDatos.TipoGeneral.TipoNumerico, TiposDeDatos.TipoGeneral.TipoFecha
                                oFSEntry.Width = Unit.Pixel(120)
                            Case TiposDeDatos.TipoGeneral.TipoBoolean
                                oFSEntry.Width = Unit.Pixel(50)
                            Case Else
                                Select Case oFSEntry.TipoGS
                                    Case TiposDeDatos.TipoCampoGS.FormaPago
                                        oFSEntry.Width = Unit.Pixel(200)
                                    Case Else
                                        oFSEntry.Width = Unit.Pixel(300)
                                End Select
                        End Select
                    End If

                    If oFSEntry.ReadOnly Then
                        oFSEntry.LabelStyle.CssClass = "CampoSoloLectura"
                        oFSEntry.InputStyle.CssClass = "trasparent"
                    End If
                    otblCell.Controls.Add(oFSEntry)
                    otblRow.Cells.Add(otblCell)
                    If Not Page.ClientScript.IsStartupScriptRegistered(oFSEntry.ClientID) Then _
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), oFSEntry.ClientID, "<script>arrEntrys_" + idCampo.ToString + "[arrEntrys_" + idCampo.ToString + ".length] = '" + oFSEntry.InitScript + "';</script>")
                    '####################################################################################################################################
                    'inicio desglose
                    Dim contaLinea As Integer = 0
                    For i = 1 To iMaxIndex
                        If lineasReal.Contains(i) Then
                            contaLinea += 1
                            If contaLinea Mod 2 = 0 Then
                                sClase = "ugfilaalttablaDesglose"
                            Else
                                sClase = "ugfilatablaDesglose"
                            End If

                            Try
                                otblRowData = Me.tblDesglose.Rows(contaLinea)

                            Catch ex As Exception
                                otblRowData = New System.Web.UI.HtmlControls.HtmlTableRow
                                Me.tblDesglose.Rows.Add(otblRowData)
                                '187 --------------------------------------------------------------------------------
                                If Not mbSoloLectura Then
                                    oImg = New System.Web.UI.WebControls.HyperLink

                                    imgFotos = New System.Web.UI.WebControls.Image
                                    imgFotos.ImageUrl = "../_common/images/Flecha_Dcha.GIF"
                                    imgFotos.BorderWidth = Unit.Pixel(0)
                                    oImg.Controls.Add(imgFotos)

                                    oImg.NavigateUrl = "javascript:void(null)"
                                    oImg.Attributes("onclick") = "popupDesgloseClickEvent(this.id,'" + Me.ClientID + "'," + i.ToString() + "," + idCampo.ToString() + ", 'izda' ," + oCampo.LineasPreconf.ToString() + ",event)"
                                    oImg.ID = "cmdCopiar_Eliminar_Izda" + i.ToString + "_" + sIdCampoDesglose
                                    otblCellData = New System.Web.UI.HtmlControls.HtmlTableCell
                                    otblCellData.Controls.Add(oImg)
                                    otblCellData.Attributes("class") = sClase
                                    otblCellData.Align = "center"
                                    otblRowData.Cells.Add(otblCellData)
                                    vLineasColumnas(contaLinea - 1) += 1
                                End If
                                'fin 187 --------------------------------------------------------------------------------
                            End Try

                            oFSDSEntry = New Fullstep.DataEntry.GeneralEntry
                            oFSDSEntry.Desglose = True
                            oFSDSEntry.DesgloseCambiaWidth = True
                            oFSDSEntry.Portal = True
                            oFSDSEntry.ID = "fsdsentry_" + i.ToString + "_" + sIdCampoDesglose
                            oFSDSEntry.CampoDef_CampoODesgHijo = oFSEntry.CampoDef_CampoODesgHijo
                            oFSDSEntry.CampoDef_DesgPadre = oFSEntry.CampoDef_DesgPadre
                            oFSDSEntry.DropDownGridID = oFSEntry.ID
                            oFSDSEntry.IdCampoPadre = oFSEntry.IdCampoPadre
                            oFSDSEntry.TipoGS = iTipoGS
                            oFSDSEntry.Width = oFSEntry.Width
                            oFSDSEntry.Tipo = iTipo
                            oFSDSEntry.Intro = oFSEntry.Intro
                            oFSDSEntry.Tag = oFSEntry.Tag
                            oFSDSEntry.Servicio = oFSEntry.Servicio
                            oFSDSEntry.Formato = oFSEntry.Formato

                            oFSDSEntry.Title = oFSEntry.Title
                            oFSDSEntry.WindowTitle = Me.Page.Title
                            oFSDSEntry.DenDesgloseMat = DBNullToStr(Me.Titulo)
                            oFSDSEntry.DenGrupo = oFSEntry.DenGrupo
                            oFSDSEntry.IsGridEditor = False
                            oFSDSEntry.IdContenedor = oFSEntry.IdContenedor
                            oFSDSEntry.TabContainer = oFSEntry.TabContainer
                            oFSDSEntry.ReadOnly = oFSEntry.ReadOnly
                            oFSDSEntry.ActivoSM = CType(Me.Page, FSPMPage).Acceso.gbAccesoFSSM
                            oFSDSEntry.Restric = oFSEntry.Restric
                            oFSDSEntry.SelCualquierMatPortal = oFSEntry.SelCualquierMatPortal

                            If bEsNC = True AndAlso CtrlPortalCambioEstado(iTipoGS) Then
                                oFSDSEntry.bSaltaCambioRechazo = True
                                oFSDSEntry.NCSinRevisar = oTextos.Rows(5).Item(1)
                            End If

                            ''*****************************************************************************
                            ''Comprobar Si tiene bloqueos de escritura Tarea (224)
                            ''---------------------------------------
                            If Not oFSDSEntry.ReadOnly Then
                                If oRow.GetChildRows("REL_CAMPO_BLOQUEO").Length > 0 Then
                                    Dim dsMonedas As DataSet
                                    Dim oMonedas As PMPortalServer.Monedas = FSPMServer.Get_Monedas
                                    Dim dCambioVigenteInstancia As Double = 1
                                    If Not msInstanciaMoneda Is Nothing Then
                                        oMonedas.LoadData("SPA", lCiaComp, msInstanciaMoneda, bAllDatos:=True)
                                        dCambioVigenteInstancia = oMonedas.Data.Tables(0).Rows(0)("EQUIV")
                                    End If

                                    Dim bCumple As Boolean
                                    bCumple = ComprobarCondiciones(oRow.Item("ESCRITURA_FORMULA"), oRow.GetChildRows("REL_CAMPO_BLOQUEO"), i, dCambioVigenteInstancia)

                                    oFSDSEntry.ReadOnly = bCumple
                                    If bCumple Then

                                        oFSDSEntry.LabelStyle.CssClass = "CampoSoloLectura"
                                        oFSDSEntry.InputStyle.CssClass = "trasparent"
                                        If (oFSDSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoGeneral.TipoArchivo) Or (oFSEntry.Tipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoArchivo) Then
                                            oFSDSEntry.InputStyle.Font.Size = System.Web.UI.WebControls.FontUnit.Parse("10px")
                                        End If
                                    Else
                                        oFSDSEntry.InputStyle.CopyFrom(oFSEntry.InputStyle)
                                    End If
                                Else
                                    oFSDSEntry.InputStyle.CopyFrom(oFSEntry.InputStyle)
                                End If
                            Else
                                oFSDSEntry.InputStyle.CopyFrom(oFSEntry.InputStyle)
                            End If

                            ''*****************************************************************************

                            oFSDSEntry.Obligatorio = oFSEntry.Obligatorio
                            oFSDSEntry.BaseDesglose = False

                            oFSDSEntry.Maximo = oFSEntry.Maximo
                            oFSDSEntry.Minimo = oFSEntry.Minimo
                            oFSDSEntry.Lista = oFSEntry.Lista
                            oFSDSEntry.DependentField = Replace(oFSEntry.DependentField, "fsentry", "fsdsentry_" + i.ToString + "_")
                            oFSDSEntry.ValidationControl = oFSEntry.ValidationControl
                            'oFSDSEntry.InputStyle.CopyFrom(oFSEntry.InputStyle)
                            oFSDSEntry.MaxLength = oFSEntry.MaxLength
                            oFSDSEntry.PopUpStyle = oFSEntry.PopUpStyle
                            oFSDSEntry.PUCaptionBoton = oFSEntry.PUCaptionBoton
                            oFSDSEntry.ButtonStyle.CopyFrom(oFSEntry.ButtonStyle)
                            oFSDSEntry.Restric = oFSEntry.Restric
                            oFSDSEntry.Calculado = oFSEntry.Calculado
                            oFSDSEntry.NumberFormat = oUser.NumberFormat
                            oFSDSEntry.DateFormat = oUser.DateFormat
                            oFSDSEntry.Idi = oFSEntry.Idi
                            oFSDSEntry.Orden = oFSEntry.Orden
                            oFSDSEntry.AnyadirArt = oFSEntry.AnyadirArt
                            oFSDSEntry.IdAtrib = oFSEntry.IdAtrib
                            oFSDSEntry.CargarUltADJ = oFSEntry.CargarUltADJ
                            oFSDSEntry.ErrMsg = oFSEntry.ErrMsg
                            oFSDSEntry.FechaNoAnteriorAlSistema = oFSEntry.FechaNoAnteriorAlSistema
                            oFSDSEntry.nLinea = i
                            oFSDSEntry.nColumna = vLineasColumnas(contaLinea - 1)
                            vLineasColumnas(contaLinea - 1) = vLineasColumnas(contaLinea - 1) + 1
                            oFSDSEntry.Tabla_Externa = oFSEntry.Tabla_Externa
                            oFSDSEntry.idEntryPartidaPlurianual = oFSEntry.idEntryPartidaPlurianual
                            oFSDSEntry.EntryPartidaPlurianualCampo = oFSEntry.EntryPartidaPlurianualCampo

                            If Not (oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Material OrElse
                                    oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.PRES1 OrElse
                                    oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Pres2 OrElse
                                    oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Pres3 OrElse
                                    oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Pres4) Then
                                If oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.EstadoNoConf Then
                                    If mlInstancia > 0 Then
                                        If Not oDS.Tables(sTblEstados) Is Nothing Then
                                            If oDS.Tables(sTblEstados).Rows.Count > 0 AndAlso oDS.Tables(sTblEstados).Rows.Count > (i - 1) Then
                                                If Not IsDBNull(oDS.Tables(sTblEstados).Rows.Item(i - 1).Item("ESTADO")) Then
                                                    oFSDSEntry.Text = oDS.Tables(sTblEstados).Rows.Item(i - 1).Item("ESTADO")
                                                    oFSDSEntry.Texto_Defecto = oFSEntry.Text
                                                End If
                                            End If
                                        End If
                                    End If
                                ElseIf oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.ImporteRepercutido Then
                                    oFSDSEntry.Texto_Defecto = oFSEntry.Texto_Defecto

                                    oFSDSEntry.MonCentral = oFSEntry.MonCentral
                                    oFSDSEntry.CambioCentral = oFSEntry.CambioCentral

                                    oFSDSEntry.MonRepercutido = oFSEntry.MonCentral
                                    oFSDSEntry.CambioRepercutido = oFSEntry.CambioCentral
                                Else
                                    If oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo Or oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo Then
                                        oFSDSEntry.InstanciaMoneda = msInstanciaMoneda
                                        oFSDSEntry.ToolTip = oFSEntry.ToolTip
                                    End If
                                    oFSDSEntry.Valor = oFSEntry.Valor
                                    oFSDSEntry.Text = oFSEntry.Text
                                End If
                            End If

                            'MasterField
                            oFSDSEntry.MasterField = oFSEntry.MasterField.Replace("fsentry", "fsdsentry_" + i.ToString + "_")

                            otblCellData = New System.Web.UI.HtmlControls.HtmlTableCell
                            otblCellData.Controls.Add(oFSDSEntry)
                            otblCellData.Attributes("class") = sClase
                            otblRowData.Cells.Add(otblCellData)

                            If oFSDSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Proveedor Then
                                If idDataEntryFORMOrgCompras <> "" Then
                                    oFSDSEntry.idDataEntryDependent = oFSEntry.idDataEntryDependent
                                Else
                                    oFSDSEntry.idDataEntryDependent = Replace(oFSEntry.idDataEntryDependent, "fsentry_", "fsdsentry_" + i.ToString + "_")
                                End If
                                oFSDSEntry.DependentField = Replace(oFSEntry.DependentField, "fsentry_", "fsdsentry_" + i.ToString + "_")
                            End If

                            If oFSDSEntry.TipoGS = TiposDeDatos.TipoCampoGS.ProveedorERP Then
                                If idDataEntryFORMOrgCompras <> "" Then
                                    oFSDSEntry.idDataEntryDependent = oFSEntry.idDataEntryDependent
                                Else
                                    oFSDSEntry.idDataEntryDependent = Replace(oFSEntry.idDataEntryDependent, "fsentry_", "fsdsentry_" + i.ToString + "_")
                                End If
                                oFSDSEntry.idEntryPROV = Replace(oFSEntry.idEntryPROV, "fsentry_", "fsdsentry_" + i.ToString + "_")
                            End If

                            If oFSDSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.IniSuministro Or oFSDSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.FinSuministro Then
                                oFSDSEntry.idDataEntryDependent = Replace(oFSEntry.idDataEntryDependent, "fsentry", "fsdsentry_" + i.ToString)

                            ElseIf oFSDSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras _
                            Or oFSDSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Centro Then
                                oFSDSEntry.idDataEntryDependent = Replace(oFSEntry.idDataEntryDependent, "fsentry", "fsdsentry_" + i.ToString)
                                If oFSDSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras Then
                                    oFSDSEntry.DependentField = Replace(oFSEntry.DependentField, "fsentry_", "fsdsentry_" + i.ToString + "_")
                                    oFSDSEntry.idDataEntryDependent2 = Replace(oFSEntry.idDataEntryDependent2, "fsentry_", "fsdsentry_" + i.ToString + "_")
                                End If
                                If oFSDSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Centro Then
                                    'Relacionar la organizacion de compras con el primer Centro del Desglose  
                                    If sIDCampoOrgCompras <> "" Then
                                        Dim mioFSDSEntry As Fullstep.DataEntry.GeneralEntry
                                        mioFSDSEntry = Me.Parent.FindControl("fsentry" & sIDCampoOrgCompras)
                                        If Not mioFSDSEntry Is Nothing Then
                                            mioFSDSEntry.idDataEntryDependent2 = oFSDSEntry.ClientID
                                            mioFSDSEntry.Portal = True
                                        End If
                                    End If
                                    If CodOrgCompras <> "" Then
                                        oFSDSEntry.DependentValue = CodOrgCompras
                                    Else
                                        If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString).Length > 0 Then
                                            oFSDSEntry.DependentValue = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString)(0).Item("VALOR_TEXT"))
                                        End If
                                    End If
                                    'Relacionar el Centro con la Organizacion de Compras fuera del desglose (Si lo hubiera)
                                    oFSDSEntry.idDataEntryDependent2 = Replace(oFSEntry.idDataEntryDependent2, "fsentry", "fsdsentry_" + i.ToString + "_")
                                    'Relacionar el Centro con el almacen (Si lo hubiera)
                                    oFSDSEntry.idDataEntryDependent3 = Replace(oFSEntry.idDataEntryDependent3, "fsentry", "fsdsentry_" + i.ToString + "_")
                                End If

                            ElseIf oFSDSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo Or oFSDSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo Or oFSDSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo Then
                                If idDataEntryFORMOrgCompras <> "" Then
                                    oFSDSEntry.idDataEntryDependent = oFSEntry.idDataEntryDependent
                                    Dim mioFSDSEntry As Fullstep.DataEntry.GeneralEntry
                                    mioFSDSEntry = Me.Parent.FindControl("fsentry" & sIDCampoOrgCompras)
                                    If Not mioFSDSEntry Is Nothing Then
                                        mioFSDSEntry.idDataEntryDependent = oFSDSEntry.ClientID
                                        mioFSDSEntry.Portal = True
                                    End If
                                Else
                                    oFSDSEntry.idDataEntryDependent = Replace(oFSEntry.idDataEntryDependent, "fsentry_", "fsdsentry_" + i.ToString + "_")
                                End If

                                If idDataEntryFORMCentro <> "" Then
                                    oFSDSEntry.idDataEntryDependent2 = oFSEntry.idDataEntryDependent2
                                    Dim mioFSDSEntry As Fullstep.DataEntry.GeneralEntry
                                    mioFSDSEntry = Me.Parent.FindControl("fsentry" & sIDCampoCentro)
                                    If Not mioFSDSEntry Is Nothing Then
                                        mioFSDSEntry.idDataEntryDependent = oFSDSEntry.ClientID
                                        mioFSDSEntry.Portal = True
                                    End If
                                Else
                                    oFSDSEntry.idDataEntryDependent2 = Replace(oFSEntry.idDataEntryDependent2, "fsentry_", "fsdsentry_" + i.ToString + "_")
                                End If

                                If Not (oFSEntry.idEntryUNI Is System.DBNull.Value) Then
                                    oFSDSEntry.idEntryUNI = Replace(oFSEntry.idEntryUNI, "fsentry_", "fsdsentry_" + i.ToString + "_")
                                End If
                                If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString).Length > 0 Then
                                    oFSDSEntry.UnidadOculta = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString)(0).Item("VALOR_TEXT"))
                                End If

                                If Not (oFSEntry.IdMaterialOculta Is System.DBNull.Value) Then
                                    oFSDSEntry.IdMaterialOculta = Replace(oFSEntry.IdMaterialOculta, "fsentry_", "fsdsentry_" + i.ToString + "_")
                                End If
                                If Not (oFSEntry.IdCodArticuloOculta Is System.DBNull.Value) Then
                                    oFSDSEntry.IdCodArticuloOculta = Replace(oFSEntry.IdCodArticuloOculta, "fsentry_", "fsdsentry_" + i.ToString + "_")
                                End If
                                If Not (oFSEntry.IdDenArticuloOculta Is System.DBNull.Value) Then
                                    oFSDSEntry.IdDenArticuloOculta = Replace(oFSEntry.IdDenArticuloOculta, "fsentry_", "fsdsentry_" + i.ToString + "_")
                                End If

                                'Buscar la organizacion de compras y el centro
                                If CodOrgCompras <> "" Then
                                    oFSDSEntry.DependentValueDataEntry = CodOrgCompras
                                Else
                                    If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString).Length > 0 Then
                                        oFSDSEntry.DependentValueDataEntry = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString)(0).Item("VALOR_TEXT"))
                                    End If
                                End If
                                If CodCentro <> "" Then
                                    oFSDSEntry.DependentValueDataEntry2 = CodCentro 'Centro Fuera del desglose (Oculto)
                                Else
                                    If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Centro & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString).Length > 0 Then
                                        oFSDSEntry.DependentValueDataEntry2 = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Centro & " AND CAMPO_PADRE = " & oFSEntry.IdCampoPadre & " AND LINEA = " & i.ToString)(0).Item("VALOR_TEXT"))
                                    End If
                                End If

                                oFSDSEntry.idEntryPROV = Replace(oFSEntry.idEntryPROV, "fsentry_", "fsdsentry_" + i.ToString + "_")
                                oFSDSEntry.idEntryPREC = Replace(oFSEntry.idEntryPREC, "fsentry_", "fsdsentry_" + i.ToString + "_")
                                oFSDSEntry.idEntryCC = Replace(oFSEntry.idEntryCC, "fsentry_", "fsdsentry_" + i.ToString + "_")
                            ElseIf oFSDSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Partida Or oFSDSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Activo Then
                                oFSDSEntry.idEntryCC = Replace(oFSEntry.idEntryCC, "fsentry_", "fsdsentry_" + i.ToString + "_")
                            End If
                        End If
                    Next
                    'fin desglose
                    '####################################################################################################################################
                    '####################################################################################################################################

                    'este bloque va a rellenar el datatable "LINEAS" con lo que venga del request 
                    Dim bDato As Boolean
                    Dim iIndex As Integer

                    For i = 1 To iMaxIndex
                        If lineasReal.Contains(i) Then
                            If (bPopUp _
                        And (Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose) <> Nothing _
                        Or Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_hnew") <> Nothing)) Then
                                bDato = True
                                iIndex = i
                            Else
                                'If bPopUp Then
                                '    bDato = False
                                'Else
                                bDato = True
                                iIndex = i
                                'End If
                            End If

                            If bDato Then
                                findTheseVals(0) = i
                                findTheseVals(1) = oCampo.IdCopiaCampo
                                If oCampo.IdCopiaCampo = Nothing Then
                                    findTheseVals(1) = oCampo.Id
                                Else
                                    findTheseVals(1) = oCampo.IdCopiaCampo
                                End If
                                findTheseVals(2) = oRow.Item("ID")
                                findTheseVals(3) = oRow.Item("PRE_ID_CAMPO_NC")
                                oRowDesglose = oDS.Tables(sTblLineas).Rows.Find(findTheseVals)

                                If oRowDesglose Is Nothing Then
                                    oRowDesglose = oDS.Tables(sTblLineas).NewRow
                                    oRowDesglose.Item("LINEA") = i
                                    If oCampo.IdCopiaCampo = Nothing Then
                                        oRowDesglose.Item("CAMPO_PADRE") = oCampo.Id
                                    Else
                                        oRowDesglose.Item("CAMPO_PADRE") = oCampo.IdCopiaCampo
                                    End If
                                    oRowDesglose.Item("TIPO_CAMPO_GS") = oRow.Item("TIPO_CAMPO_GS")
                                    oRowDesglose.Item("CAMPO_HIJO") = oRow.Item("ID")
                                    oRowDesglose.Item("PRE_ID_CAMPO_HIJO_NC") = oRow.Item("PRE_ID_CAMPO_NC")
                                    oDS.Tables(sTblLineas).Rows.Add(oRowDesglose)
                                End If
                                'uwtGrupos__ctl0_147_fsentry2293__1__2309
                                oRowDesglose.Item("INDEX") = iIndex
                                If bPopUp _
                                And (Not Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose) Is Nothing _
                                Or Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_hnew") <> Nothing) Then
                                    sValor = Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose)

                                    Select Case oFSEntry.Tipo
                                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoString, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoCorto, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoLargo, PMPortalServer.TiposDeDatos.TipoGeneral.TipoEditor
                                            If sValor <> "" AndAlso sValor <> "null" Then
                                                oRowDesglose.Item("VALOR_TEXT") = sValor
                                            End If
                                            If oFSEntry.Intro = 1 And oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.SinTipo Then
                                                oRowDesglose.Item("VALOR_NUM") = Numero(sValor, oUser.DecimalFmt, oUser.ThousanFmt)
                                            ElseIf oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.EstadoInternoNoConf Then
                                                oRowDesglose.Item("VALOR_NUM") = Numero(sValor, oUser.DecimalFmt, oUser.ThousanFmt)
                                            ElseIf oFSEntry.TipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.ProveedorERP Then
                                                If sValor = String.Empty OrElse sValor = "null" Then
                                                    oRowDesglose.Item("VALOR_TEXT_DEN") = DBNull.Value
                                                    oRowDesglose.Item("VALOR_TEXT") = DBNull.Value
                                                End If
                                            End If
                                            If IsDBNull(oRow.Item("TIPO_CAMPO_GS")) And oRow.Item("INTRO") = 1 Then
                                                oRowDesglose.Item("VALOR_NUM") = Numero(sValor, oUser.DecimalFmt, oUser.ThousanFmt)
                                            End If
                                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico
                                            'Preguntar pq me viene como numerico los de los presupuestos
                                            If Not IsDBNull(oRow.Item("TIPO_CAMPO_GS")) Then
                                                If oRow.Item("TIPO_CAMPO_GS") = PMPortalServer.TiposDeDatos.TipoCampoGS.PRES1 Or oRow.Item("TIPO_CAMPO_GS") = PMPortalServer.TiposDeDatos.TipoCampoGS.Pres2 Or oRow.Item("TIPO_CAMPO_GS") = PMPortalServer.TiposDeDatos.TipoCampoGS.Pres3 Or oRow.Item("TIPO_CAMPO_GS") = PMPortalServer.TiposDeDatos.TipoCampoGS.Pres4 Then
                                                    If sValor <> "" Then
                                                        oRowDesglose.Item("VALOR_TEXT") = sValor
                                                    End If
                                                End If
                                            End If

                                            'Pregunta pq puede resultar q lo q no es nothing en _MonRepercutido
                                            If Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose) <> "null" AndAlso Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose) <> "" Then
                                                If Not IsNumeric(Right(sValor, 1)) Then
                                                    '1,234.00 EUR   1,234.50 EUR
                                                    '1.234,00 EUR   1.234,50 EUR
                                                    Dim sAux As String = Mid(sValor, 1, Len(sValor) - (FSPMServer.LongitudesDeCodigos.giLongCodMON + 1))
                                                    If oUser.DecimalFmt = "." Then
                                                        '1,234.00    1,234.50
                                                        sAux = Replace(sAux, ",", "")
                                                        '1234.00    1234.50
                                                    Else '","
                                                        '1.234,00    1.234,50
                                                        sAux = Replace(Replace(sAux, ".", ""), ",", ".")
                                                        '1234.00    1234.50
                                                    End If
                                                    oRowDesglose.Item("VALOR_NUM") = Numero(sAux, ".", ",")
                                                Else
                                                    oRowDesglose.Item("VALOR_NUM") = Numero(sValor, ".", ",")
                                                End If
                                            End If

                                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoFecha
                                            oRowDesglose.Item("VALOR_FEC") = igDateToDate(sValor)
                                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoBoolean
                                            oRowDesglose.Item("VALOR_BOOL") = sValor
                                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoArchivo
                                            oRowDesglose.Item("VALOR_TEXT") = sValor + "####" + Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_hNew")
                                    End Select
                                End If
                            End If

                            If (bPopUp _
                            AndAlso DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = PMPortalServer.TiposDeDatos.TipoCampoGS.ImporteRepercutido _
                            AndAlso Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_MonRepercutido") <> Nothing _
                            AndAlso Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_MonRepercutido") <> "") Then
                                oRowDesglose.Item("VALOR_TEXT") = Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_MonRepercutido")
                                oRowDesglose.Item("CAMBIO") = Numero(Request(nomDesglose + "$_" + (i).ToString + "__" + sIdCampoDesglose + "_CambioRepercutido"), ".", ",")
                            End If
                        End If
                    Next
                    ' fin del bloque
                End If
                lContador += 1
            Next

            'Tarea 2227
            'Este orden es necesario para construir luego en el orden correcto el desglose cuando se trata de un desglose que se abre en ventana emergente.
            'Si no, en vez de cargarse l�nea a l�nea se cargar�a campo a campo y eso distorsionar�a los valores entre campos dependientes
            oDS.Tables(2).DefaultView.Sort = "LINEA ASC"

            Dim contanlinea As Integer = 0
            For i = 1 To iMaxIndex
                If lineasReal.Contains(i) Then
                    contanlinea += 1
                    If contanlinea Mod 2 = 0 Then
                        sClase = "ugfilaalttablaDesglose"
                    Else
                        sClase = "ugfilatablaDesglose"
                    End If

                    otblRowData = Me.tblDesglose.Rows(contanlinea)

                    'ACCIONES EN LAS FILAS GRABADAS
                    If Not mbSoloLectura Then
                        If Not moAcciones Is Nothing Then
                            For Each oRowAcc In moAcciones.Rows
                                Dim oControl As Control
                                If Not IsDBNull(oRowAcc.Item("IMAGEN")) Then
                                    oImg = New System.Web.UI.WebControls.HyperLink

                                    imgFotos = New System.Web.UI.WebControls.Image
                                    imgFotos.ImageUrl = oRowAcc.Item("IMAGEN")
                                    imgFotos.BorderWidth = Unit.Pixel(0)
                                    oImg.Controls.Add(imgFotos)

                                    oImg.NavigateUrl = "javascript:void(null)"
                                    oImg.Attributes("onclick") = oRowAcc.Item("JAVASCRIPTEVENT") + "(this.parentElement,'" + Me.ClientID + "'," + i.ToString() + "," + idCampo.ToString() + ")"
                                    oControl = oImg
                                Else
                                    Dim oBoton As HtmlInputButton = New HtmlInputButton
                                    oBoton.Value = DBNullToSomething(oRowAcc.Item("TEXTO"))
                                    oBoton.Attributes("onclick") = oRowAcc.Item("JAVASCRIPTEVENT") + "(this.parentElement,'" + Me.ClientID + "'," + i.ToString() + "," + idCampo.ToString() + ")"
                                    oBoton.Attributes.Add("class", "boton")
                                    oControl = oBoton
                                End If
                                oControl.ID = oRowAcc.Item("ID") + "_" + i.ToString()
                                otblCellData = New System.Web.UI.HtmlControls.HtmlTableCell
                                otblCellData.Controls.Add(oControl)
                                otblCellData.Attributes("class") = sClase
                                otblCellData.Align = "center"
                                otblRowData.Cells.Add(otblCellData)
                            Next
                        End If
                    End If

                    If Not mbSoloLectura Then
                        otblCellData = New System.Web.UI.HtmlControls.HtmlTableCell
                        otblCellData.ID = "celdaCopiar_Eliminar" + i.ToString

                        oImg = New System.Web.UI.WebControls.HyperLink

                        imgFotos = New System.Web.UI.WebControls.Image
                        imgFotos.ImageUrl = "../_common/images/Flecha_Izda.GIF"
                        imgFotos.BorderWidth = Unit.Pixel(0)
                        oImg.Controls.Add(imgFotos)

                        oImg.NavigateUrl = "javascript:void(null)"
                        oImg.Attributes("onclick") = "popupDesgloseClickEvent(this.id,'" + Me.ClientID + "'," + i.ToString() + "," + idCampo.ToString() + ", 'dcha' ," + oCampo.LineasPreconf.ToString() + ",event)"
                        oImg.ID = "cmdCopiar_Eliminar" + i.ToString

                        otblCellData.Controls.Add(oImg)
                        otblCellData.Attributes("class") = sClase
                        otblCellData.Align = "center"
                        otblRowData.Cells.Add(otblCellData)
                        vLineasColumnas(contanlinea - 1) += 1
                    End If
                    'finlinea
                    '##################################################################################################
                    'fin jpa Tarea 178
                    oHidden = New System.Web.UI.HtmlControls.HtmlInputHidden
                    oHidden.ID = "_" + i.ToString + "_Deleted"
                    oHidden.Name = "_" + i.ToString + "_Deleted"
                    oHidden.Value = "no"
                    otblCellData.Controls.Add(oHidden)

                    oHidden = New System.Web.UI.HtmlControls.HtmlInputHidden
                    oHidden.ID = "_" + i.ToString + "_linea"
                    oHidden.Name = "_" + i.ToString + "_linea"
                    oHidden.Value = i
                    otblCellData.Controls.Add(oHidden)
                End If
            Next

            If Not mbSoloLectura Then
                If Not moAcciones Is Nothing Then
                    For Each oRowAcc In moAcciones.Rows
                        'CABECERA PARA LAS ACCIONES
                        oDivHeader = New System.Web.UI.HtmlControls.HtmlGenericControl("DIV")
                        oCellHeader = New System.Web.UI.HtmlControls.HtmlTableCell
                        oCellHeader.ID = "COL_" + oRowAcc.Item("ID")
                        oSpanHeader = New System.Web.UI.HtmlControls.HtmlGenericControl("SPAN")
                        oSpanHeader.InnerHtml = oRowAcc.Item("TITULO")

                        If DBNullToSomething(oRowAcc.Item("WIDTH")) > 0 Then
                            oDivHeader.Style.Item("width") = oRowAcc.Item("WIDTH").ToString + "px"
                            'oCellHeader.Width = oRowAcc.Item("WIDTH")
                        End If
                        oCellHeader.Attributes("class") = "cabeceraDesglose"
                        oDivHeader.Controls.Add(oSpanHeader)
                        oCellHeader.Controls.Add(oDivHeader)
                        oRowHeader.Cells.Add(oCellHeader)

                        'ACCIONES EN LA FILA OCULTA
                        Dim oControl As Control
                        If Not IsDBNull(oRowAcc.Item("IMAGEN")) Then
                            oImg = New System.Web.UI.WebControls.HyperLink

                            imgFotos = New System.Web.UI.WebControls.Image
                            imgFotos.ImageUrl = oRowAcc.Item("IMAGEN")
                            imgFotos.BorderWidth = Unit.Pixel(0)
                            oImg.Controls.Add(imgFotos)

                            oImg.NavigateUrl = "javascript:void(null)"
                            oImg.Attributes("onclick") = oRowAcc.Item("JAVASCRIPTEVENT") + "(this.parentElement,'" + Me.ClientID + "','##newIndex##'," + idCampo.ToString() + ")"
                            oControl = oImg
                        Else
                            Dim oBoton As HtmlInputButton = New HtmlInputButton
                            oBoton.Value = DBNullToSomething(oRowAcc.Item("TEXTO"))
                            oBoton.Attributes("onclick") = oRowAcc.Item("JAVASCRIPTEVENT") + "(this.parentElement,'" + Me.ClientID + "','##newIndex##'," + idCampo.ToString() + ")"
                            oBoton.Attributes.Add("class", "boton")
                            oControl = oBoton
                        End If
                        oControl.ID = oRowAcc.Item("ID") + "_'##newIndex##'"
                        otblCell = New System.Web.UI.HtmlControls.HtmlTableCell
                        otblCell.Controls.Add(oControl)
                        otblCell.Attributes("class") = sClase
                        otblCell.Align = "center"
                        otblRow.Cells.Add(otblCell)
                    Next
                End If
            End If

            'jpa Tarea 187 --------------------------------------------------------------------------------
            'la primera cabecera de tblDesgloseHidden
            'ESTO ES LO QUE ESTABA
            If Not mbSoloLectura Then
                oCellHeader = New System.Web.UI.HtmlControls.HtmlTableCell
                oCellHeader.ID = "COL_Copiar_Eliminar_Izda"
                oCellHeader.InnerHtml = sCopiar_Eliminar
                oCellHeader.Attributes("class") = "cabeceraDesglose"
                oRowHeader.Cells.Add(oCellHeader)

                otblCell = New System.Web.UI.HtmlControls.HtmlTableCell
                oImg = New System.Web.UI.WebControls.HyperLink

                imgFotos = New System.Web.UI.WebControls.Image
                imgFotos.ImageUrl = "../_common/images/Flecha_Izda.GIF"
                imgFotos.BorderWidth = Unit.Pixel(0)
                oImg.Controls.Add(imgFotos)

                oImg.NavigateUrl = "javascript:void(null)"
                oImg.Attributes("onclick") = "popupDesgloseClickEvent(this.id,'" + Me.ClientID + "','##newIndex##'," + idCampo.ToString() + ", 'dcha' ," + oCampo.LineasPreconf.ToString() + ",event)"
                oImg.ID = "'##newIndex##'_cmdCopiar_Eliminar_Izda"
                otblCellData = New System.Web.UI.HtmlControls.HtmlTableCell
                otblCell.Controls.Add(oImg)
                otblCell.Align = "center"
                otblRow.Cells.Add(otblCell)
            End If
            'fin jpa Tarea 187 --------------------------------------------------------------------------------

            tblDesgloseHidden.Rows.Add(otblRow)

            oHidNumRows.Value = iMaxIndex
            oHidNumRowsGrid.Value = iLineas
            Controls.Add(oHidNumRows)
            Controls.Add(oHidNumRowsGrid)

            'Si hay acciones, debemos ordenarlas por fecha de inicio:
            If Not moAcciones Is Nothing Then
                'Obtengo el ID del Campo Fecha de Inicio
                Dim lIDFechaInicio As Long
                Dim aAUX As DataRow()
                aAUX = oDS.Tables(0).Select("TIPO_CAMPO_GS=" & Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Fec_inicio)
                If (aAUX.Length > 0) Then
                    lIDFechaInicio = CLng(aAUX(0).Item("ID"))
                    'Obtengo un array de DataRows ordenadas por la Fecha de Inicio
                    Dim aOrdenadosPorFecha As DataRow()
                    aOrdenadosPorFecha = oDS.Tables(sTblLineas).Select("CAMPO_HIJO=" & lIDFechaInicio & " AND INDEX IS NOT NULL", "VALOR_FEC ASC")
                    'Creo un Array de Arrays de DataRow para tener todas las l�neas de Accion enteras ordenadas por Fecha:
                    Dim aLineas As ArrayList = New ArrayList(aOrdenadosPorFecha.Length)
                    For Each dr As DataRow In aOrdenadosPorFecha
                        aLineas.Add(oDS.Tables(sTblLineas).Select("LINEA=" & dr.Item("LINEA").ToString))
                    Next
                    'Modifico el INDEX de cada linea:
                    For j As Integer = 0 To aLineas.Count - 1
                        For Each dr As DataRow In aLineas(j)
                            dr.Item("INDEX") = lineasReal(j)
                        Next
                    Next
                End If
            End If

            Dim sAdjun As String
            Dim bArticuloGenerico As Boolean = True
            Dim bYaControlado As Boolean = False

            'Tarea 2227
            'Pasamos el defaultview (que hemos ordenado previamente por l�nea) a table
            'De este modo sabemos que el desglose se cargar� l�nea a l�nea, no por columnas
            Dim oDSTableLINEAS As DataTable = oDS.Tables(sTblLineas).DefaultView.ToTable()

            For Each oRow In oDSTableLINEAS.Rows
                'For Each oRow In oDS.Tables(sTblLineas).Rows
                'En el caso de los campos Estado Interno/comentario del deglose de acciones de un ano conf el id del campo el el cod de grupo y el id del campo
                If oRow.Item("PRE_ID_CAMPO_HIJO_NC") = 0 Then
                    sIdCampoHijoDesglose = oRow.Item("CAMPO_HIJO").ToString
                Else
                    sIdCampoHijoDesglose = oRow.Item("PRE_ID_CAMPO_HIJO_NC").ToString + oRow.Item("CAMPO_HIJO").ToString
                End If
                sValor = ""
                sValorMat = ""

                bYaControlado = False

                Try
                    oFSDSEntry = Me.tblDesglose.FindControl("fsdsentry_" + oRow.Item("INDEX").ToString + "_" + sIdCampoHijoDesglose)
                Catch
                    oFSDSEntry = Nothing
                End Try
                If Not oFSDSEntry Is Nothing Then

                    iTipo = oFSDSEntry.Tipo
                    iTipoGS = oFSDSEntry.TipoGS

                    If (Not IsDBNull(oRow.Item("VALOR_TEXT")) Or Not IsDBNull(oRow.Item("VALOR_NUM")) Or Not IsDBNull(oRow.Item("VALOR_FEC")) Or Not IsDBNull(oRow.Item("VALOR_BOOL"))) Then
                    Else
                        bYaControlado = True
                    End If

                    iTipo = oFSDSEntry.Tipo
                    iTipoGS = oFSDSEntry.TipoGS
                    Select Case iTipo
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoString, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoCorto, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoLargo, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio
                            oFSDSEntry.Valor = DBNullToSomething(oRow.Item("VALOR_TEXT"))
                            oFSDSEntry.Text = oFSDSEntry.Valor
                            If oFSDSEntry.Valor <> Nothing Then
                                If iTipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.SinTipo And oFSDSEntry.Intro = 1 Then
                                    'If oFSDSEntry.Lista.Tables.Count > 0 Then '27-11
                                    Dim oRowLista() As DataRow
                                    If DBNullToSomething(oRow.Item("VALOR_NUM").ToString) <> Nothing Then

                                        oRowLista = oFSDSEntry.Lista.Tables(0).Select("ID = " + oRow.Item("CAMPO_HIJO").ToString + " and ORDEN=" + oRow.Item("VALOR_NUM").ToString)
                                    Else
                                        oRowLista = oFSDSEntry.Lista.Tables(0).Select("ID = " + oRow.Item("CAMPO_HIJO").ToString + " and VALOR_TEXT_" & sIdi & " =" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))

                                    End If
                                    If oRowLista.Length > 0 Then
                                        oFSDSEntry.Text = oRowLista(0).Item("VALOR_TEXT_" & sIdi)
                                        oFSDSEntry.Valor = oRow.Item("VALOR_NUM")
                                    End If
                                End If
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico
                            If DBNullToSomething(oRow.Item("TIPO_CAMPO_GS")) = TiposDeDatos.IdsFicticios.NumLinea Then
                                oFSDSEntry.Valor = oFSDSEntry.nLinea
                                oFSDSEntry.Text = oFSDSEntry.nLinea
                            Else
                                oFSDSEntry.Valor = DBNullToSomething(oRow.Item("VALOR_NUM"))
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoFecha
                            Dim iTipoFecha As Fullstep.PMPortalServer.TiposDeDatos.TipoFecha

                            If iTipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.SinTipo And oFSDSEntry.Intro = 1 Then
                                oFSDSEntry.Valor = FormatDate(DBNullToSomething(oRow.Item("VALOR_FEC")), oUser.DateFormat)
                            Else
                                iTipoFecha = DBNullToSomething(modUtilidades.DBNullToSomething(oRow.Item("VALOR_NUM")))
                                oFSDSEntry.Valor = modUtilidades.DevolverFechaRelativaA(iTipoFecha, DBNullToSomething(oRow.Item("VALOR_FEC")))
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoArchivo
                            Dim idAdjun As String
                            If mlId = Nothing Then
                                Dim sRequestAdjun As String
                                Dim idAdjuntos As String
                                Dim idAdjuntosNew As String
                                sAdjun = ""
                                idAdjun = ""

                                If IsDBNull(oRow.Item("VALOR_TEXT")) OrElse (oRow.Item("VALOR_TEXT") = "undefined") _
                                    OrElse (Split(oRow.Item("VALOR_TEXT"), "####")(0) = String.Empty AndAlso Split(oRow.Item("VALOR_TEXT"), "####")(1) = String.Empty) Then
                                    sRequestAdjun = ""
                                Else
                                    sRequestAdjun = oRow.Item("VALOR_TEXT")

                                    idAdjuntos = Split(sRequestAdjun, "####")(0)
                                    idAdjuntosNew = Split(sRequestAdjun, "####")(1)

                                    idAdjuntos = Replace(idAdjuntos, "xx", ",")
                                    idAdjuntosNew = Replace(idAdjuntosNew, "xx", ",")

                                    Dim oAdjuntos As Fullstep.PMPortalServer.Adjuntos
                                    oAdjuntos = FSPMServer.Get_Adjuntos
                                    Dim oAdjuntosFormCampo As Fullstep.PMPortalServer.Adjuntos
                                    oAdjuntosFormCampo = FSPMServer.Get_Adjuntos

                                    If mlInstancia = Nothing Then
                                        oAdjuntos.Load(lCiaComp, 3, idAdjuntos, idAdjuntosNew)
                                    Else
                                        oAdjuntos.LoadInst(lCiaComp, 3, idAdjuntos, idAdjuntosNew)
                                        ' Guardas sin enviar un popup donde has a�adido una linea con adjunto por defecto. La 2da vez q lo 
                                        ' abres (sin salirte de la pantalla por eso es 2da vez) en bbdd esta perfecto pero en page_load tienes 
                                        ' en idAdjuntos el id de form_campo no de copia_linea_desglose_adjun y no sale el texto.
                                        If Not String.IsNullOrEmpty(idAdjuntos) Then
                                            NAdjuntos = DameNumAdjuntos(idAdjuntos)
                                            NAdjuntos = NAdjuntos + IIf(idAdjuntosNew <> "", DameNumAdjuntos(idAdjuntosNew), 0)
                                        End If

                                        If NAdjuntos > oAdjuntos.Data.Tables(0).Rows.Count Then
                                            AuxNAdjuntos = idAdjuntos & ","

                                            While AuxNAdjuntos <> ""
                                                bEstaAdjun = False

                                                AuxNAdjunto = Left(AuxNAdjuntos, InStr(AuxNAdjuntos, ",") - 1)


                                                For Each oDSRowAdjun In oAdjuntos.Data.Tables(0).Rows
                                                    If oDSRowAdjun.Item("ID").ToString() = AuxNAdjunto Then
                                                        bEstaAdjun = True
                                                        Exit For
                                                    End If
                                                Next

                                                If bEstaAdjun = False Then
                                                    oAdjuntosFormCampo.Load(lCiaComp, 3, AuxNAdjunto, "")
                                                    For Each oDSRowAdjun In oAdjuntosFormCampo.Data.Tables(0).Rows
                                                        idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                                                        sAdjun += oDSRowAdjun.Item("NOM") + " (" + FormatNumber(DBNullToSomething(oDSRowAdjun.Item("DATASIZE")), oUser.NumberFormat) + " Kb.), "
                                                    Next
                                                End If

                                                AuxNAdjuntos = Mid(AuxNAdjuntos, InStr(AuxNAdjuntos, ",") + 1)
                                            End While
                                            'Los q estan en idAdjuntosNew no son un problema pq nunca son de form_campo
                                        End If
                                    End If

                                    For Each oDSRowAdjun In oAdjuntos.Data.Tables(0).Rows
                                        idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                                        sAdjun += oDSRowAdjun.Item("NOM") + " (" + FormatNumber(DBNullToSomething(oDSRowAdjun.Item("DATASIZE")), oUser.NumberFormat) + " Kb.), "
                                    Next

                                    If sAdjun <> "" Then
                                        sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                                        idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)
                                    End If
                                End If

                                oFSDSEntry.Valor = sRequestAdjun
                                oFSDSEntry.Text = AjustarAnchoTextoPixels(sAdjun, 150, "Verdana", 12)
                            Else
                                sAdjun = ""
                                idAdjun = ""
                                For Each oDSRowAdjun In oDS.Tables(sTblAdjuntos).Rows
                                    If oRow("CAMPO_PADRE") = oDSRowAdjun("CAMPO_PADRE") AndAlso oRow("CAMPO_HIJO") = oDSRowAdjun("CAMPO_HIJO") AndAlso oRow("LINEA") = oDSRowAdjun("LINEA") Then
                                        idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"

                                        dSize = DBNullToSomething(oDSRowAdjun.Item("DATASIZE")) / 1024
                                        sAdjun += oDSRowAdjun.Item("NOM") + " (" + FormatNumber(dSize, oUser.NumberFormat) + " Kb.), "
                                    End If
                                Next
                                If sAdjun <> "" Then
                                    sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                                    idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)

                                End If
                                oFSDSEntry.Valor = idAdjun
                                oFSDSEntry.Text = AjustarAnchoTextoPixels(sAdjun, 150, "Verdana", 12)
                            End If

                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoBoolean
                            oFSDSEntry.Valor = DBNullToSomething(oRow.Item("VALOR_BOOL"))
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoEditor
                            oFSDSEntry.Valor = DBNullToSomething(oRow.Item("VALOR_TEXT"))
                            If oFSEntry.ReadOnly Then
                                oFSDSEntry.Text = oTextos.Rows(15).Item(1)
                            Else
                                oFSDSEntry.Text = oTextos.Rows(14).Item(1)
                            End If
                    End Select

                    Select Case iTipoGS
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") _
                                AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    If oRow.Item("VALOR_TEXT_COD") = 0 Then
                                        oFSDSEntry.Valor = Nothing
                                        oFSDSEntry.Text = oRow.Item("VALOR_TEXT")
                                    Else
                                        oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                        oFSDSEntry.Text = oRow.Item("VALOR_TEXT_DEN")
                                    End If
                                Else
                                    oArts = FSPMServer.Get_Articulos
                                    Dim oArticulo As Fullstep.PMPortalServer.PropiedadesArticulo = oArts.Articulo_Get_Properties(lCiaComp, oRow.Item("VALOR_TEXT"))
                                    If oArticulo.ExisteArticulo Then
                                        oFSDSEntry.Valor = oArticulo.Codigo
                                        oFSDSEntry.Text = oArticulo.Denominacion
                                        oFSDSEntry.TipoRecepcion = oArticulo.TipoRecepcion
                                        sKeyTipoRecepcion = oFSDSEntry.TipoRecepcion
                                    Else
                                        oFSDSEntry.Text = oRow.Item("VALOR_TEXT")
                                        oFSDSEntry.Valor = Nothing
                                    End If
                                End If
                                oFSDSEntry.Denominacion = oFSDSEntry.Text
                                oFSDSEntry.Width = Unit.Pixel(400)
                                Dim idCampoMat As Long

                                idCampoMat = 0
                                For Each i In arrCamposMaterial
                                    If i < oRow.Item("CAMPO_HIJO") Then
                                        If idCampoMat < i Then
                                            idCampoMat = i
                                        End If
                                    End If
                                Next
                                If idCampoMat > 0 Then
                                    Try
                                        sValorMat = oDS.Tables(sTblLineas).Select("CAMPO_HIJO = " & idCampoMat & " AND LINEA = " & oRow.Item("LINEA"))(0).Item("VALOR_TEXT")
                                        oFSDSEntry.DependentValue = sValorMat
                                    Catch ex As Exception

                                    End Try
                                End If
                            End If
                            If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA")).Length > 0 Then
                                oFSDSEntry.UnidadOculta = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA"))(0).Item("VALOR_TEXT"))
                            End If

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Text = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.ToolTip = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.Denominacion = oFSDSEntry.Text
                                oFSDSEntry.Width = Unit.Pixel(200)
                                oArts = FSPMServer.Get_Articulos
                                Dim oArticulo As Fullstep.PMPortalServer.PropiedadesArticulo = oArts.Articulo_Get_Properties(lCiaComp, oRow.Item("VALOR_TEXT"))
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                    If oArticulo.ExisteArticulo Then
                                        oFSDSEntry.TipoRecepcion = oArticulo.TipoRecepcion
                                        oFSDSEntry.articuloGenerico = oArticulo.Generico
                                        oFSDSEntry.articuloCodificado = True
                                        sKeyTipoRecepcion = oFSDSEntry.TipoRecepcion
                                    End If
                                Else
                                    If oArticulo.ExisteArticulo Then
                                        oFSDSEntry.articuloGenerico = oArticulo.Generico
                                        oFSDSEntry.TipoRecepcion = oArticulo.TipoRecepcion
                                        sKeyTipoRecepcion = oFSDSEntry.TipoRecepcion
                                    Else
                                        oFSDSEntry.articuloGenerico = True
                                    End If
                                End If
                                bArticuloGenerico = oFSDSEntry.articuloGenerico

                                Dim idCampoMat As Long

                                idCampoMat = 0
                                For Each i In arrCamposMaterial
                                    If i < oRow.Item("CAMPO_HIJO") Then
                                        If idCampoMat < i Then
                                            idCampoMat = i
                                        End If
                                    End If
                                Next
                                If idCampoMat > 0 Then
                                    Try
                                        sValorMat = oDS.Tables(sTblLineas).Select("CAMPO_HIJO = " & idCampoMat & " AND LINEA = " & oRow.Item("LINEA"))(0).Item("VALOR_TEXT")
                                        oFSDSEntry.DependentValue = sValorMat
                                    Catch ex As Exception

                                    End Try
                                End If
                            Else
                                bArticuloGenerico = True
                            End If
                            If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA")).Length > 0 Then
                                oFSDSEntry.UnidadOculta = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA"))(0).Item("VALOR_TEXT"))
                            End If

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Text = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.ToolTip = oRow.Item("VALOR_TEXT")

                                oFSDSEntry.articuloGenerico = bArticuloGenerico
                                oFSDSEntry.TipoRecepcion = sKeyTipoRecepcion
                                If sCodArticulo <> "" Then
                                    oFSDSEntry.codigoArticulo = sCodArticulo
                                    sCodArticulo = ""
                                End If

                                Dim idCampoMat As Long
                                idCampoMat = 0
                                For Each i In arrCamposMaterial
                                    If i < oRow.Item("CAMPO_HIJO") Then
                                        If idCampoMat < i Then
                                            idCampoMat = i
                                        End If
                                    End If
                                Next
                                If idCampoMat > 0 Then
                                    Try
                                        sValorMat = oDS.Tables(sTblLineas).Select("CAMPO_HIJO = " & idCampoMat & " AND LINEA = " & oRow.Item("LINEA"))(0).Item("VALOR_TEXT")
                                        oFSDSEntry.DependentValue = sValorMat
                                    Catch ex As Exception

                                    End Try
                                End If
                            End If
                            If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA")).Length > 0 Then
                                oFSDSEntry.UnidadOculta = DBNullToSomething(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad & " AND CAMPO_PADRE = " & oRow.Item("CAMPO_PADRE") & " AND LINEA = " & oRow.Item("LINEA"))(0).Item("VALOR_TEXT"))
                            End If

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Dest
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Text = TextoDelDropDown(oFSDSEntry.Lista, "COD", oRow.Item("VALOR_TEXT"), Not oFSDSEntry.ReadOnly, , , True)
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Cantidad
                            If sKeyTipoRecepcion = "1" Then
                                oFSDSEntry.TipoRecepcion = 1
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Text = TextoDelDropDown(oFSDSEntry.Lista, "COD", oRow.Item("VALOR_TEXT"), Not oFSDSEntry.ReadOnly)
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.FormaPago
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Text = TextoDelDropDown(oFSDSEntry.Lista, "COD", oRow.Item("VALOR_TEXT"), Not oFSDSEntry.ReadOnly)
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Moneda
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Text = TextoDelDropDown(oFSDSEntry.Lista, "COD", oRow.Item("VALOR_TEXT"), Not oFSDSEntry.ReadOnly)
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Pais
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Text = TextoDelDropDown(oFSDSEntry.Lista, "COD", oRow.Item("VALOR_TEXT"), Not oFSDSEntry.ReadOnly)
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                sCodPais = oRow.Item("VALOR_TEXT")
                            End If
                            sKeyPais = oFSDSEntry.ID
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Provincia
                            'Provincia es un campo relacionado por lo que la carga de datos se hace desde javascript(ajax)
                            'Esta carga se hace �nicamente cuando el campo tiene un valor por defecto y hay que buscar la descripci�n del valor por defecto
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                If sCodPais <> Nothing Then
                                    Dim oProvis As PMPortalServer.Provincias
                                    oProvis = FSPMServer.Get_Provincias()
                                    oProvis.Pais = DBNullToStr(sCodPais)
                                    oProvis.LoadData(sIdi, lCiaComp)
                                    If sKeyPais <> String.Empty Then _
                                        oFSDSEntry.MasterField = sKeyPais
                                    'Como texto ponemos el c�digo y la descripci�n porque si no puede haber problemas al ser controles que cargan los datos desde javascript y tener Infragistics 11.1 limitaciones en ese sentido
                                    oFSDSEntry.Text = TextoDelDropDown(oProvis.Data, "COD", oRow.Item("VALOR_TEXT"), True, "PAICOD", sCodPais) 'sDenominacion
                                    oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                End If
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.PRES1, PMPortalServer.TiposDeDatos.TipoCampoGS.Pres2, PMPortalServer.TiposDeDatos.TipoCampoGS.Pres3, PMPortalServer.TiposDeDatos.TipoCampoGS.Pres4
                            'oFSDSEntry.Width = Unit.Pixel(200)
                            oFSDSEntry.Valor = modUtilidades.DBNullToSomething(oRow.Item("VALOR_TEXT"))
                            If DBNullToSomething(oRow.Item("VALOR_TEXT")) <> Nothing Then
                                Dim arrDenominaciones() As String
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") And Not DBNullToSomething(oRow.Item("VALOR_TEXT")) = Nothing _
                                AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    arrDenominaciones = oRow.Item("VALOR_TEXT_DEN").ToString().Split("#")
                                    If oRow.Item("VALOR_TEXT_COD") Then
                                        oFSDSEntry.InputStyle.CssClass = IIf(oFSDSEntry.ReadOnly, "StringTachadotrasparent", "StringTachado")
                                    Else
                                        oFSDSEntry.InputStyle.CssClass = IIf(oFSDSEntry.ReadOnly, "trasparent", "TipoTextoMedio")
                                    End If
                                Else
                                    oFSDSEntry.InputStyle.CssClass = IIf(oFSDSEntry.ReadOnly, "trasparent", "TipoTextoMedio")
                                End If
                                sValor = ""
                                sTexto = ""
                                sDenominacion = ""
                                iContadorPres = 0
                                arrPresupuestos = oRow.Item("VALOR_TEXT").split("#")
                                For Each oPresup In arrPresupuestos
                                    iContadorPres = iContadorPres + 1
                                    arrPresup = oPresup.Split("_")
                                    iNivel = arrPresup(0)
                                    lIdPresup = arrPresup(1)
                                    dPorcent = Numero(arrPresup(2), ".", ",")
                                    If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") And Not DBNullToSomething(oRow.Item("VALOR_TEXT")) = Nothing _
                                    AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                        sTexto = arrDenominaciones(iContadorPres - 1) & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); " & sTexto
                                        sValor += oPresup + "#"
                                    Else
                                        Select Case iTipoGS
                                            Case PMPortalServer.TiposDeDatos.TipoCampoGS.PRES1
                                                oPres1 = FSPMServer.Get_PresProyectosNivel1
                                                Select Case iNivel
                                                    Case 1
                                                        oPres1.LoadData(lCiaComp, lIdPresup)
                                                    Case 2
                                                        oPres1.LoadData(lCiaComp, Nothing, lIdPresup)
                                                    Case 3
                                                        oPres1.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
                                                    Case 4
                                                        oPres1.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
                                                End Select
                                                oDSPres = oPres1.Data
                                                oPres1 = Nothing
                                            Case PMPortalServer.TiposDeDatos.TipoCampoGS.Pres2
                                                oPres2 = FSPMServer.Get_PresContablesNivel1
                                                Select Case iNivel
                                                    Case 1
                                                        oPres2.LoadData(lCiaComp, lIdPresup)
                                                    Case 2
                                                        oPres2.LoadData(lCiaComp, Nothing, lIdPresup)
                                                    Case 3
                                                        oPres2.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
                                                    Case 4
                                                        oPres2.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
                                                End Select
                                                oDSPres = oPres2.Data
                                                oPres2 = Nothing
                                            Case PMPortalServer.TiposDeDatos.TipoCampoGS.Pres3
                                                oPres3 = FSPMServer.Get_PresConceptos3Nivel1
                                                Select Case iNivel
                                                    Case 1
                                                        oPres3.LoadData(lCiaComp, lIdPresup)
                                                    Case 2
                                                        oPres3.LoadData(lCiaComp, Nothing, lIdPresup)
                                                    Case 3
                                                        oPres3.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
                                                    Case 4
                                                        oPres3.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
                                                End Select
                                                oDSPres = oPres3.Data
                                                oPres3 = Nothing
                                            Case PMPortalServer.TiposDeDatos.TipoCampoGS.Pres4
                                                oPres4 = FSPMServer.Get_PresConceptos4Nivel1
                                                Select Case iNivel
                                                    Case 1
                                                        oPres4.LoadData(lCiaComp, lIdPresup)
                                                    Case 2
                                                        oPres4.LoadData(lCiaComp, Nothing, lIdPresup)
                                                    Case 3
                                                        oPres4.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
                                                    Case 4
                                                        oPres4.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
                                                End Select
                                                oDSPres = oPres4.Data
                                                oPres4 = Nothing
                                        End Select

                                        If Not oDSPres Is Nothing Then
                                            sLblUO = ""
                                            If oDSPres.Tables(0).Rows.Count > 0 Then
                                                With oDSPres.Tables(0).Rows(0)
                                                    'comprobamos que el valor por defecto cumpla la restricci�n del usuario. SI NO CUMPLE, NO APARECER�
                                                    If Not oDSPres.Tables(0).Columns("ANYO") Is Nothing Then
                                                        iAnyo = .Item("ANYO")

                                                    End If
                                                    Dim sDenPresup As String
                                                    sUon1 = DBNullToSomething(.Item("UON1"))
                                                    sUon2 = DBNullToSomething(.Item("UON2"))
                                                    sUon3 = DBNullToSomething(.Item("UON3"))
                                                    bSinPermisos = False
                                                    If sUon3 <> Nothing And oUser.UON3Aprobador <> Nothing Then
                                                        If sUon3 <> oUser.UON3Aprobador Then
                                                            'malo (el usuario no tiene permisos para asignar el presupuesto por defecto )
                                                            bSinPermisos = True
                                                        End If
                                                    End If
                                                    If sUon2 <> Nothing And oUser.UON2Aprobador <> Nothing Then
                                                        If sUon2 <> oUser.UON2Aprobador Then

                                                            bSinPermisos = True
                                                        End If
                                                    End If
                                                    If sUon1 <> Nothing And oUser.UON1Aprobador <> Nothing Then
                                                        If sUon1 <> oUser.UON1Aprobador Then

                                                            bSinPermisos = True
                                                        End If
                                                    End If

                                                    If Not bSinPermisos Then
                                                        If Not oDS.Tables(0).Columns("ANYO") Is Nothing Then
                                                            sTexto += .Item("ANYO").ToString + " - "
                                                            sDenPresup = .Item("ANYO").ToString() + " - "
                                                        End If

                                                        If Not IsDBNull(.Item("PRES4")) Then
                                                            sTexto += .Item("PRES4").ToString & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
                                                            sDenPresup += .Item("PRES4").ToString
                                                        ElseIf Not IsDBNull(.Item("PRES3")) Then
                                                            sTexto += .Item("PRES3").ToString & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
                                                            sDenPresup += .Item("PRES3").ToString
                                                        ElseIf Not IsDBNull(.Item("PRES2")) Then
                                                            sTexto += .Item("PRES2").ToString & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
                                                            sDenPresup += .Item("PRES2").ToString
                                                        ElseIf Not IsDBNull(.Item("PRES1")) Then
                                                            sTexto += .Item("PRES1").ToString & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
                                                            sDenPresup += .Item("PRES1").ToString
                                                        End If

                                                        If sDenominacion <> "" Then sDenominacion = "#" & sDenominacion
                                                        sDenominacion = sDenPresup & sDenominacion

                                                        sValor += oPresup + "#"
                                                    End If
                                                End With
                                            End If
                                        End If
                                    End If
                                Next
                                If sValor <> Nothing Then
                                    sValor = Left(sValor, sValor.Length - 1)
                                End If
                                oFSDSEntry.Valor = sValor
                                oFSDSEntry.Denominacion = sDenominacion
                                If sTexto = "" Then
                                    If Not oFSDSEntry.ReadOnly Then
                                        oFSDSEntry.Text = oTextos.Rows(1).Item(1)
                                        oFSDSEntry.Texto_Defecto = oFSDSEntry.Text
                                        bYaControlado = True
                                    End If
                                Else
                                    oFSDSEntry.Text = sTexto
                                End If
                            Else
                                If Not oFSDSEntry.ReadOnly Then
                                    oFSDSEntry.Text = oTextos.Rows(1).Item(1)
                                    oFSDSEntry.Texto_Defecto = oFSDSEntry.Text
                                    bYaControlado = True
                                End If
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Proveedor
                            If DBNullToSomething(oRow.Item("VALOR_TEXT")) <> Nothing Then
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") _
                                AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = oRow.Item("VALOR_TEXT_DEN")
                                Else
                                    oProve = FSPMServer.get_Proveedor
                                    oProve.Cod = oRow.Item("VALOR_TEXT")
                                    oProve.Load(sIdi, lCiaComp)
                                    sDenominacion = oProve.Den
                                End If
                                oFSDSEntry.Text = oRow.Item("VALOR_TEXT") + " - " + sDenominacion.Replace("'", "")
                                oFSDSEntry.Denominacion = sDenominacion
                            ElseIf Not oFSDSEntry.ReadOnly Then
                                oFSDSEntry.Text = oTextos.Rows(2).Item(1)
                                oFSDSEntry.Texto_Defecto = oFSDSEntry.Text
                                bYaControlado = True
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Material
                            If DBNullToSomething(oRow.Item("VALOR_TEXT")) <> Nothing Then
                                Dim sMat As String = oRow.Item("VALOR_TEXT")
                                '27-11Dim arrMat(4) As String
                                Dim lLongCod As Integer
                                'oFSDSEntry.Width = Unit.Pixel(150)
                                sValorMat = sMat

                                For i = 1 To 4
                                    arrMat(i) = ""
                                Next i

                                i = 1
                                While Trim(sMat) <> ""
                                    Select Case i
                                        Case 1
                                            lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
                                        Case 2
                                            lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
                                        Case 3
                                            lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
                                        Case 4
                                            lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN4
                                    End Select
                                    arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
                                    sMat = Mid(sMat, lLongCod + 1)
                                    i = i + 1
                                End While

                                sToolTip = ""
                                If arrMat(4) <> "" Then
                                    If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") _
                                    AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                    Else
                                        oGMN3 = FSPMServer.Get_GrupoMatNivel3
                                        oGMN3.GMN1Cod = arrMat(1)
                                        oGMN3.GMN2Cod = arrMat(2)
                                        oGMN3.Cod = arrMat(3)

                                        oGMN3.CargarTodosLosGruposMatDesde(lCiaComp, 1, sIdi, arrMat(4), , True)
                                        sDenominacion = oGMN3.GruposMatNivel4.Item(arrMat(4)).Den

                                        oGMN3 = Nothing
                                    End If
                                    sToolTip = sDenominacion & " (" & arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) & " - " & arrMat(4) & ")"
                                    If CType(Me.Page, FSPMPage).Acceso.gbMaterialVerTodosNiveles Then
                                        sValor = arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) & " - " & arrMat(4) + " - " + sDenominacion
                                    Else
                                        sValor = arrMat(4) + " - " + sDenominacion
                                    End If

                                ElseIf arrMat(3) <> "" Then
                                    If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") _
                                    AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                    Else
                                        oGMN2 = FSPMServer.Get_GrupoMatNivel2
                                        oGMN2.GMN1Cod = arrMat(1)
                                        oGMN2.Cod = arrMat(2)
                                        oGMN2.CargarTodosLosGruposMatDesde(lCiaComp, 1, sIdi, arrMat(3), , True)
                                        sDenominacion = oGMN2.GruposMatNivel3.Item(arrMat(3)).Den

                                        oGMN2 = Nothing
                                    End If
                                    sToolTip = sDenominacion & "(" & arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) & ")"
                                    If CType(Me.Page, FSPMPage).Acceso.gbMaterialVerTodosNiveles Then
                                        sValor = arrMat(1) & " - " & arrMat(2) & " - " & arrMat(3) + " - " + sDenominacion
                                    Else
                                        sValor = arrMat(3) + " - " + sDenominacion
                                    End If
                                ElseIf arrMat(2) <> "" Then
                                    If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") _
                                    AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                    Else
                                        oGMN1 = FSPMServer.Get_GrupoMatNivel1
                                        oGMN1.Cod = arrMat(1)
                                        oGMN1.CargarTodosLosGruposMatDesde(lCiaComp, 1, sIdi, arrMat(2), , True)
                                        sDenominacion = oGMN1.GruposMatNivel2.Item(arrMat(2)).Den

                                        oGMN1 = Nothing
                                    End If
                                    sToolTip = sDenominacion & "(" & arrMat(1) & " - " & arrMat(2) & ")"
                                    If CType(Me.Page, FSPMPage).Acceso.gbMaterialVerTodosNiveles Then
                                        sValor = arrMat(1) & " - " & arrMat(2) + " - " + sDenominacion
                                    Else
                                        sValor = arrMat(2) + " - " + sDenominacion
                                    End If
                                ElseIf arrMat(1) <> "" Then
                                    If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") _
                                    AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                        sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                    Else
                                        oGMN1s = FSPMServer.Get_GruposMatNivel1
                                        oGMN1s.CargarTodosLosGruposMatDesde(lCiaComp, 1, sIdi, arrMat(1), , , True)
                                        sDenominacion = oGMN1s.Item(arrMat(1)).Den

                                        oGMN1s = Nothing
                                    End If
                                    sToolTip = sDenominacion & "(" & arrMat(1) & ")"
                                    sValor = arrMat(1) + " - " + sDenominacion
                                End If
                                oFSDSEntry.Text = sValor
                                oFSDSEntry.ToolTip = sToolTip
                                oFSDSEntry.Denominacion = sDenominacion
                                oFSDSEntry.Width = Unit.Pixel(200)
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.EstadoNoConf
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                If Not IsDBNull(oRow.Item("INDEX")) Then
                                    oFSDSEntry.Valor = oFSDSEntry.Lista.Tables(0).Rows(oRow.Item("INDEX")).Item("COD")
                                    oFSDSEntry.Text = Mid(oFSDSEntry.Lista.Tables(0).Rows(oRow.Item("INDEX")).Item("DEN"), 6)
                                End If
                            End If

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.EstadoInternoNoConf
                            If DBNullToSomething(oRow.Item("VALOR_NUM")) > 0 Then
                                If ((oRow.Item("VALOR_NUM") = 3) Or (oRow.Item("VALOR_NUM") = 4) Or (oRow.Item("VALOR_NUM") = 5) Or mbSoloLectura) Then
                                    '' Ahora mismo si es 1-SIN REVISAR ,2-APROBADA SE DEJA VISIBLE
                                    Dim oBoton As HtmlInputButton = Me.FindControl("cmdFinAccion" + "_" + oRow.Item("INDEX").ToString)
                                    If Not oBoton Is Nothing Then
                                        oBoton.Style.Add("visibility", "hidden")
                                    End If
                                End If

                                oFSDSEntry.Valor = oRow.Item("VALOR_NUM")
                                oFSDSEntry.Text = oTextos.Rows(oRow.Item("VALOR_NUM") + 4).Item(1)
                            End If

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.UnidadOrganizativa
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.Text = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.Denominacion = oFSDSEntry.Text
                            End If
                            sKeyUnidadOrganizativa = oFSDSEntry.ID
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Departamento
                            If sKeyUnidadOrganizativa <> String.Empty Then _
                                oFSDSEntry.MasterField = sKeyUnidadOrganizativa

                            'Departamento es un campo relacionado por lo que la carga de datos se hace desde javascript(ajax)
                            'Esta carga se hace �nicamente cuando el campo tiene un valor por defecto y hay que buscar la descripci�n del valor por defecto
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                Dim sUnidadOrganizativa2 As String = ""
                                If oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa).Length > 0 Then
                                    sUnidadOrganizativa2 = DBNullToStr(oDS.Tables(2).Select("TIPO_CAMPO_GS = " & TiposDeDatos.TipoCampoGS.UnidadOrganizativa)(0).Item("VALOR_TEXT"))
                                End If

                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")

                                Dim oDepartamento As PMPortalServer.Departamento
                                oDepartamento = FSPMServer.Get_Departamento

                                If sUnidadOrganizativa2 <> "" Then
                                    Dim oDepartamentos As PMPortalServer.Departamentos
                                    oDepartamentos = FSPMServer.Get_Departamentos

                                    Dim vUnidadesOrganizativas(3) As String
                                    vUnidadesOrganizativas = Split(sUnidadOrganizativa2, " - ")

                                    Select Case UBound(vUnidadesOrganizativas)
                                        Case 0
                                            oDepartamentos.LoadData(lCiaComp, 0)
                                            oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                        Case 1
                                            oDepartamentos.LoadData(lCiaComp, 1, vUnidadesOrganizativas(0))
                                            oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                        Case 2
                                            oDepartamentos.LoadData(lCiaComp, 2, vUnidadesOrganizativas(0), vUnidadesOrganizativas(1))
                                            oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                        Case 3
                                            oDepartamentos.LoadData(lCiaComp, 3, vUnidadesOrganizativas(0), vUnidadesOrganizativas(1), vUnidadesOrganizativas(2))
                                            oDepartamentos.Data.Tables(0).Rows.RemoveAt(0)
                                    End Select
                                    'Como texto ponemos el c�digo y la descripci�n porque si no puede haber problemas al ser controles que cargan los datos desde javascript y tener Infragistics 11.1 limitaciones en ese sentido
                                    oFSDSEntry.Text = TextoDelDropDown(oDepartamentos.Data, "COD", oRow.Item("VALOR_TEXT"), True)
                                End If
                            End If

                            oFSDSEntry.Denominacion = sDenominacion

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras
                            sCodOrgCompras2 = ""
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                sCodOrgCompras2 = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.Text = TextoDelDropDown(oFSDSEntry.Lista, "COD", oRow.Item("VALOR_TEXT"), Not oFSDSEntry.ReadOnly)
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                            End If
                            sKeyOrgComprasDataCheck = oFSDSEntry.ID
                        Case TiposDeDatos.TipoCampoGS.ProveedorERP
                            If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                'Si viene de una instancia tendra VALOR_TEXT_DEN
                                oFSDSEntry.Text = oRow.Item("VALOR_TEXT") & " - " & oRow.Item("VALOR_TEXT_DEN")
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                            Else
                                'Si viene por defecto el valor del formulario y no tiene VALOR_TEXT_DEN(que vendria solo cuando ya es una instancia)
                                If DBNullToStr(oRow.Item("VALOR_TEXT")) <> String.Empty Then
                                    Dim oProvesERP As PMPortalServer.ProveedoresERP
                                    oProvesERP = FSPMServer.Get_ProveedoresERP
                                    Dim ds As DataSet = oProvesERP.CargarProveedoresERPtoDS(lCiaComp, , , oRow.Item("VALOR_TEXT"))
                                    For Each dr As DataRow In ds.Tables(0).Rows
                                        oFSDSEntry.Text = dr("COD") & " - " & dr("DEN")
                                        oFSDSEntry.Valor = dr("COD")
                                        Exit For
                                    Next
                                    ds = Nothing
                                    oProvesERP = Nothing
                                End If
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Centro
                            If sKeyOrgComprasDataCheck <> String.Empty Then _
                                oFSDSEntry.MasterField = sKeyOrgComprasDataCheck
                            sCodCentro2 = ""
                            'Centro es un campo relacionado por lo que la carga de datos se hace desde javascript(ajax)
                            'Esta carga se hace �nicamente cuando el campo tiene un valor por defecto y hay que buscar la descripci�n del valor por defecto
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                sCodCentro2 = oRow.Item("VALOR_TEXT")
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                If sCodOrgCompras2 <> "" Then
                                    Dim oCentros As PMPortalServer.Centros
                                    oCentros = FSPMServer.Get_Centros
                                    oCentros.LoadData(lCiaComp, , sCodOrgCompras2)
                                    oFSDSEntry.Text = TextoDelDropDown(oCentros.Data, "COD", oRow.Item("VALOR_TEXT"), True)
                                End If
                            End If
                            sKeyCentro = oFSDSEntry.ID
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Almacen
                            If sKeyCentro <> String.Empty Then _
                                oFSDSEntry.MasterField = sKeyCentro
                            'Almac�n es un campo relacionado por lo que la carga de datos se hace desde javascript(ajax)
                            'Esta carga se hace �nicamente cuando el campo tiene un valor por defecto y hay que buscar la descripci�n del valor por defecto
                            If Not IsDBNull(oRow.Item("VALOR_NUM")) Then
                                oFSDSEntry.Valor = oRow.Item("VALOR_NUM")
                                If sCodCentro2 <> "" Then
                                    Dim oAlmacenes As PMPortalServer.Almacenes
                                    oAlmacenes = FSPMServer.Get_Almacenes
                                    oAlmacenes.LoadData(lCiaComp, , sCodCentro2)
                                    oFSDSEntry.Text = TextoDelDropDown(oAlmacenes.Data, "ID", oRow.Item("VALOR_NUM"), True)
                                End If
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Empresa
                            If Not IsDBNull(oRow.Item("VALOR_NUM")) Then
                                Dim oEmpresa As Fullstep.PMPortalServer.Empresa
                                oEmpresa = FSPMServer.Get_Object(GetType(Fullstep.PMPortalServer.Empresa))
                                oEmpresa.ID = oRow.Item("VALOR_NUM")
                                oEmpresa.Load(lCiaComp, sIdi)
                                If Not String.IsNullOrEmpty(oEmpresa.NIF) Then oFSDSEntry.Text = oEmpresa.NIF & " - " & oEmpresa.Den
                                oEmpresa = Nothing

                                oFSDSEntry.Valor = oRow.Item("VALOR_NUM")
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.IniSuministro
                            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechas") Then
                                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechas", "<script>var sMensajeFecha = '" & oTextos.Rows.Item(10).Item(1) & "'</script>")
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.FinSuministro
                            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "MensajeFechas") Then
                                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechas", "<script>var sMensajeFecha = '" & oTextos.Rows.Item(10).Item(1) & "'</script>")
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.ImporteRepercutido
                            If Not IsDBNull(oRow.Item("VALOR_NUM")) Then
                                oFSDSEntry.Valor = oRow.Item("VALOR_NUM")
                                oFSDSEntry.Text = oRow.Item("VALOR_NUM")
                            End If
                            oFSDSEntry.MonRepercutido = oRow.Item("VALOR_TEXT")
                            oFSDSEntry.CambioRepercutido = oRow.Item("CAMBIO")
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.CentroCoste
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                If InStrRev(oFSDSEntry.Valor, "#") > 0 Then
                                    oFSDSEntry.Text = Right(oFSDSEntry.Valor, Len(oFSDSEntry.Valor) - InStrRev(oFSDSEntry.Valor, "#"))
                                Else
                                    oFSDSEntry.Text = oFSDSEntry.Valor
                                End If
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                    sDenominacion = oFSDSEntry.Text & " - " & sDenominacion
                                End If

                                oFSDSEntry.Text = sDenominacion
                                oFSDSEntry.Denominacion = sDenominacion
                            End If

                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Partida
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")
                                If InStrRev(oFSDSEntry.Valor, "#") > 0 Then
                                    If InStrRev(oFSDSEntry.Valor, "|") > 0 Then 'si no est� en el nivel 1, si no m�s abajo
                                        oFSDSEntry.Text = Right(oFSDSEntry.Valor, Len(oFSDSEntry.Valor) - InStrRev(oFSDSEntry.Valor, "|"))
                                    Else
                                        oFSDSEntry.Text = Right(oFSDSEntry.Valor, Len(oFSDSEntry.Valor) - InStrRev(oFSDSEntry.Valor, "#"))
                                    End If
                                Else
                                    oFSDSEntry.Text = oFSDSEntry.Valor
                                End If
                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                    sDenominacion = oFSDSEntry.Text & " - " & sDenominacion
                                End If

                                oFSDSEntry.Text = sDenominacion
                                oFSDSEntry.Denominacion = sDenominacion
                            End If
                        Case PMPortalServer.TiposDeDatos.TipoCampoGS.Activo
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                oFSDSEntry.Valor = oRow.Item("VALOR_TEXT")

                                If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") AndAlso Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    sDenominacion = PMPortalServer.Campo.ExtraerDescrIdioma(oRow.Item("VALOR_TEXT_DEN"), sIdi)
                                    sDenominacion = oFSDSEntry.Valor & " - " & sDenominacion
                                End If
                                oFSDSEntry.Text = sDenominacion
                                oFSDSEntry.Denominacion = sDenominacion
                            End If
                    End Select
                    If oFSDSEntry.Tabla_Externa > 0 Then
                        oFSDSEntry.Width = Unit.Pixel(250)
                        If Not oFSDSEntry.Valor Is Nothing Then
                            If oRow.Table.Columns.Contains("VALOR_TEXT_DEN") Then
                                If Not IsDBNull(oRow.Item("VALOR_TEXT_DEN")) Then
                                    Dim sTxt As String = oRow.Item("VALOR_TEXT_DEN").ToString()
                                    Dim sTxt2 As String
                                    If sTxt.IndexOf("]#[") > 0 Then
                                        sTxt2 = sTxt.Substring(2, sTxt.IndexOf("]#[") - 2)
                                    Else
                                        sTxt2 = sTxt.Substring(2, sTxt.Length - 3)
                                    End If
                                    Select Case sTxt.Substring(0, 1)
                                        Case "s"
                                            oFSDSEntry.Text = sTxt2
                                        Case "n"
                                            oFSDSEntry.Text = Fullstep.FSNLibrary.FormatNumber(Numero(sTxt2, "."), oUser.NumberFormat)
                                        Case "d"
                                            oFSDSEntry.Text = CType(sTxt2, Date).ToString("d", oUser.DateFormat)
                                    End Select
                                    If sTxt.IndexOf("]#[") > 0 Then
                                        oFSDSEntry.Text = oFSDSEntry.Text & " "
                                        sTxt2 = sTxt.Substring(sTxt.IndexOf("]#[") + 3, sTxt.Length - sTxt.IndexOf("]#[") - 5)
                                        Select Case sTxt.Substring(sTxt.Length - 1, 1)
                                            Case "s"
                                                oFSDSEntry.Text = oFSDSEntry.Text & oFSEntry.Text & sTxt2
                                            Case "n"
                                                oFSDSEntry.Text = oFSDSEntry.Text & Fullstep.FSNLibrary.FormatNumber(Numero(sTxt2, "."), oUser.NumberFormat)
                                            Case "d"
                                                oFSDSEntry.Text = oFSDSEntry.Text & CType(sTxt2, Date).ToString("d", oUser.DateFormat)
                                        End Select
                                    End If
                                End If
                                If Not IsDBNull(oRow.Item("VALOR_TEXT_COD")) Then oFSDSEntry.codigoArticulo = oRow.Item("VALOR_TEXT_COD")
                            Else
                                Dim oTablaExterna As PMPortalServer.TablaExterna = FSPMServer.get_TablaExterna
                                oTablaExterna.LoadDefTabla(lCiaComp, oFSDSEntry.Tabla_Externa, False)
                                oTablaExterna.LoadData(lCiaComp, False)
                                oFSDSEntry.Text = oTablaExterna.DescripcionReg(oFSDSEntry.Valor, sIdi, oUser.DateFormat, oUser.NumberFormat)
                                oFSDSEntry.codigoArticulo = oTablaExterna.CodigoArticuloReg(oFSDSEntry.Valor)
                            End If
                            oFSDSEntry.Denominacion = oFSDSEntry.Text
                        End If
                    End If

                    If DBNullToSomething(oRow.Item("INDEX")) = 1 Then
                        oFSEntry = Me.tblDesglose.FindControl("fsentry" + sIdCampoHijoDesglose)
                        If oFSEntry.TipoGS <> PMPortalServer.TiposDeDatos.TipoCampoGS.EstadoInternoNoConf And oFSEntry.ID <> "fsentry" + oCampo.IdGrupo.ToString + CInt(Fullstep.PMPortalServer.IdsFicticios.Comentario).ToString Then
                            oFSEntry.Valor = oFSDSEntry.Valor
                            oFSEntry.Text = oFSDSEntry.Text
                        End If
                    End If


                Else
                    Try
                        Dim dRowCampo() As DataRow
                        dRowCampo = oDS.Tables(0).Select("ID= " & oRow.Item("CAMPO_HIJO"))
                        If dRowCampo.Length > 0 Then
                            'Articulo oculto y con articulo en la linea
                            If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                sCodArticulo = oRow.Item("VALOR_TEXT")
                                oArts = FSPMServer.Get_Articulos
                                Dim oArticulo As Fullstep.PMPortalServer.PropiedadesArticulo = oArts.Articulo_Get_Properties(lCiaComp, sCodArticulo)
                                If oArticulo.ExisteArticulo Then
                                    bArticuloGenerico = oArticulo.Generico
                                End If
                            Else
                                bArticuloGenerico = True
                            End If
                        End If
                        dRowCampo = Nothing
                    Catch ex As Exception
                    End Try
                End If

                ''--------DEFECTO---------
                If DBNullToSomething(oRow.Item("INDEX")) = 1 Then
                    oFSDSEntryDefecto = oFSDSEntry
                Else
                    oFSDSEntryDefecto = Me.tblDesglose.FindControl("fsdsentry_1_" + sIdCampoHijoDesglose)
                End If

                If bFnValorDefecto = False AndAlso Not bYaControlado Then
                    oFSDSEntry.Posee_Defecto = ((Not (oFSDSEntryDefecto.Valor Is Nothing)) Or (Not (oFSDSEntryDefecto.Text Is Nothing)))

                    oFSDSEntry.Valor_Defecto = oFSDSEntryDefecto.Valor
                    If iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoArchivo Then
                        oFSDSEntry.Valor_Defecto = Strings.Replace(oFSDSEntryDefecto.Valor_Defecto, "####", "")
                    End If

                    oFSDSEntry.Texto_Defecto = IIf(oFSDSEntryDefecto.Text Is Nothing, "", oFSDSEntryDefecto.Text)

                    oFSDSEntry.MonCentral = oFSDSEntryDefecto.MonCentral
                    oFSDSEntry.CambioCentral = oFSDSEntryDefecto.CambioCentral
                End If
                ''--------DEFECTO---------
            Next

            If bFnValorDefecto Then
                Try
                    CargarValoresDefecto(lCiaComp, oDsDefecto, sIdi, iLineas)
                Catch ex As Exception
                End Try
            End If

            'Borramos las listas de datos de los controles del desglose que se cargan en cliente
            '(las listas de datos de los controles de valores por defecto ya las hemos borrado dentro de la funci�n CargarValoresDefecto)
            limpiarListadeCamposConCargaEnCliente(oDS, iLineas)

            If msTitulo <> Nothing Then
                oRowHeader = New System.Web.UI.HtmlControls.HtmlTableRow
                oCellHeader = New System.Web.UI.HtmlControls.HtmlTableCell

                Dim oLblTitulo As New Label

                oLblTitulo.Text = msTitulo
                oLblTitulo.Style.Add("margin-right", "15px")
                oLblTitulo.Style.Add("vertical-align", "top")
                oCellHeader.Controls.Add(oLblTitulo)

                oCellHeader.ColSpan = Me.tblDesglose.Rows(0).Cells.Count

                oCellHeader.Style("font-weight") = "bold"
                oCellHeader.ID = "TituloDesglose"

                If msAyuda <> "" Then 'Si el campo padre del desglose tiene ayuda se mete el icono
                    Dim ImgAyuda As System.Web.UI.WebControls.HyperLink

                    ImgAyuda = New System.Web.UI.WebControls.HyperLink
                    ImgAyuda.ImageUrl = "images\help.gif"
                    ImgAyuda.NavigateUrl = "javascript:void(null)"
                    ImgAyuda.Attributes.Add("margin-left", "15px")
                    If TieneIdCampo Then
                        ImgAyuda.Attributes("onclick") = "show_help(" + Campo.ToString + "," + mlInstancia.ToString + ")"
                    Else
                        ImgAyuda.Attributes("onclick") = "show_help(" + Campo.ToString + "," & IIf(mlInstancia > 0, mlInstancia.ToString & ",", " null,") + mlSolicitud.ToString + ")"
                    End If
                    oCellHeader.Controls.Add(ImgAyuda)
                End If

                oRowHeader.Cells.Add(oCellHeader)
                oCellHeader.Attributes("class") = "cabeceradesglose"
                tblDesglose.Rows.Insert(0, oRowHeader)
            End If

            sScriptFechasSuministro = String.Format(IncludeScriptKeyFormat, "javascript", sScriptFechasSuministro)
            If Not Page.ClientScript.IsStartupScriptRegistered("arrFechasDesgloseAUX") Then _
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "arrFechasDesgloseAUX", sScriptFechasSuministro)

            oDS = Nothing
            sScript = "var sMensajeGenerico = '" + JSText(oTextos.Rows(12).Item(1)) + "';"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            If Not Page.ClientScript.IsStartupScriptRegistered("MensajeGenericos") Then _
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "MensajeGenericos", sScript)

            If Not Page.ClientScript.IsClientScriptBlockRegistered("MensajeFechasDefecto") Then
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "MensajeFechasDefecto", "<script>var sMensajeFechaDefecto = '" & oTextos.Rows.Item(13).Item(1) & "'</script>")
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "FechaNoAnteriorAlSistema") Then
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "FechaNoAnteriorAlSistema", "<script>var sFechaNoAnteriorAlSistema = '" & oTextos.Rows.Item(20).Item(1) & "'</script>")
            End If
            sScript = ""
            sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
            sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
            sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
            sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            If Not Page.ClientScript.IsStartupScriptRegistered("LongitudesCodigosKey") Then Page.ClientScript.RegisterStartupScript(Page.GetType(), "LongitudesCodigosKey", sScript)

            sScript = ""
            sScript += "arrEstadosInternos[1] = new Array();arrEstadosInternos[1][0]=1;arrEstadosInternos[1][1]='" + oTextos.Rows(5).Item(1) + "';"
            sScript += "arrEstadosInternos[2] = new Array();arrEstadosInternos[2][0]=2;arrEstadosInternos[2][1]='" + oTextos.Rows(6).Item(1) + "';"
            sScript += "arrEstadosInternos[3] = new Array();arrEstadosInternos[3][0]=3;arrEstadosInternos[3][1]='" + oTextos.Rows(7).Item(1) + "';"
            sScript += "arrEstadosInternos[4] = new Array();arrEstadosInternos[4][0]=4;arrEstadosInternos[4][1]='" + oTextos.Rows(8).Item(1) + "';"
            sScript += "arrEstadosInternos[5] = new Array();arrEstadosInternos[5][0]=5;arrEstadosInternos[5][1]='" + oTextos.Rows(9).Item(1) + "';"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            If Not Page.ClientScript.IsStartupScriptRegistered("arrEstadosInternos") Then _
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "arrEstadosInternos", sScript)

            sScript = ""
            sScript += "var lIdEstadoActual = '" + CInt(Fullstep.PMPortalServer.IdsFicticios.EstadoActual).ToString + "';"
            sScript += "var lIdComentario = '" + CInt(Fullstep.PMPortalServer.IdsFicticios.Comentario).ToString + "';"
            sScript += "var lIdEstadoInterno = '" + CInt(Fullstep.PMPortalServer.IdsFicticios.EstadoInterno).ToString + "';"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            If Not Page.ClientScript.IsStartupScriptRegistered("idsFicticios") Then _
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "idsFicticios", sScript)

            sScript = "var vDateFmt='" + oUser.DateFormat.ShortDatePattern + "'"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            If Not Page.ClientScript.IsStartupScriptRegistered("variablesFormatoUsuario") Then _
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "variablesFormatoUsuario", sScript)

            If Not Page.ClientScript.IsClientScriptBlockRegistered("claveArrayDesgloses") Then
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "claveArrayDesgloses", "<script>var arrDesgloses = new Array()</script>")
            End If

            If Not Page.ClientScript.IsStartupScriptRegistered(Me.tblDesglose.ClientID) Then _
                Page.ClientScript.RegisterStartupScript(Page.GetType(), Me.tblDesglose.ClientID, "<script>arrDesgloses[arrDesgloses.length] = '" + Me.tblDesglose.ClientID + "';</script>")
        End Sub
        ''' <summary>
        ''' Obtien el ancho de un entry seg�n su tipo y tipo campo gs
        ''' </summary>
        ''' <param name="iTipo">Tipo de control, es decir, numero, archivo,....</param>
        ''' <param name="iTipoGs">Tipo campo gs del entry</param>
        ''' <returns>Ancho</returns>
        ''' <remarks>Llamada desde:Page_load; Tiempo m�ximo:0</remarks>
        Private Function ObtenerWidth(ByVal iTipo As Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral, ByVal iTipoGs As Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS, ByVal bReadOnly As Boolean) As String
            Select Case iTipoGs
                Case PMPortalServer.TiposDeDatos.TipoCampoGS.ArchivoEspecific
                    Return 100
                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Material
                    Return 200
                Case PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo
                    Return 400
                Case PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo
                    Return 200
                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Dest
                    Return 250
                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad
                    Return 200
                Case PMPortalServer.TiposDeDatos.TipoCampoGS.FormaPago
                    Return 200
                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Moneda
                    Return 200
                Case PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo
                    Return 200
                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Pais
                    Return 200
                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Provincia
                    Return 200
                Case PMPortalServer.TiposDeDatos.TipoCampoGS.PRES1, PMPortalServer.TiposDeDatos.TipoCampoGS.Pres2, PMPortalServer.TiposDeDatos.TipoCampoGS.Pres3, PMPortalServer.TiposDeDatos.TipoCampoGS.Pres4
                    Return 200
                Case TiposDeDatos.IdsFicticios.NumLinea
                    Return 65
                Case Else
                    Select Case iTipo
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoArchivo
                            Return 100
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoBoolean
                            Return 50
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoCheckBox
                            Return 30
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoFecha
                            Return 80
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoLargo, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio, PMPortalServer.TiposDeDatos.TipoGeneral.TipoString
                            Return 250 * CDbl(IIf(bReadOnly, 1.5, 1)) 'el general entry lo pon�a a 250 cuando readonly y desglose. Ahora 250 *1.5=375px
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoCorto
                            Return 100 * CDbl(IIf(bReadOnly, 1.5, 1)) 'El general entry coge lo q le pongas aqui
                            'Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio, PMPortalServer.TiposDeDatos.TipoGeneral.TipoString
                            '    Return IIf(bReadOnly, 250, 150) 'el general entry lo pone a 250 cuando readonly y desglose
                        Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico
                            Return 100
                        Case Else
                            Return 100

                    End Select
            End Select

        End Function
        ''' <summary>
        ''' Comprobac��n de bloqueos por campos
        ''' </summary>
        ''' <param name="lInstancia">Instancia</param>
        ''' <param name="sFormula">Formula</param>
        ''' <param name="drCondiciones">Lista de bloqueos por campos</param>
        ''' <param name="nLinea">Linea</param>
        ''' <returns>Si hay bloqueo o no</returns>
        ''' <remarks>Llamada desde:Page_load; Tiempo m�ximo:0</remarks>
        Private Function ComprobarCondiciones(ByVal sFormula As String, ByVal drCondiciones As DataRow(), ByVal nLinea As Long, ByVal dCambioVigenteInstancia As Double) As Boolean
            Dim drCampo As DataRow
            Dim dsCampos As DataSet
            Dim oValorCampo As Object
            Dim oValor As Object
            Dim iTipo As Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral
            Dim oCond As DataRow
            Dim i As Long
            Dim sVariables() As String
            Dim dValues() As Double
            Dim iEq As New USPExpress.USPExpression
            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
            Dim sIdi As String = Session("FS_Portal_User").Idioma.ToString
            Dim lCiaComp As Long = Session("FS_Portal_User").CiaComp
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If
            Dim oInstancia As Fullstep.PMPortalServer.Instancia

            Dim sMoneda As String = ""
            Dim dCambioEnlace As Double

            Try
                oInstancia = FSWSServer.Get_Instancia

                If mlInstancia > 0 Then
                    oInstancia.ID = mlInstancia
                    oInstancia.Load(lCiaComp, sIdi)
                    oInstancia.Version = mlVersion
                End If

                i = 0

                For Each oCond In drCondiciones
                    ReDim Preserve sVariables(i)
                    ReDim Preserve dValues(i)

                    sVariables(i) = oCond.Item("COD")
                    dValues(i) = 0 'El valor ser� 1 o 0 dependiendo de si se cumple o no la condici�n

                    sMoneda = DBNullToStr(oCond.Item("MON"))
                    dCambioEnlace = DBNullToDbl(oCond.Item("CAMBIO"))

                    '<EXPRESION IZQUIEDA> <OPERADOR> <EXPRESION DERECHA>
                    'Primero evaluamos <EXPRESION IZQUIERDA> 
                    Select Case oCond.Item("TIPO_CAMPO")
                        Case 1 'Campo de formulario

                            If mlInstancia > 0 Then
                                dsCampos = oInstancia.cargarCampo(lCiaComp, oCond.Item("CAMPO_DATO"))
                            End If

                            If dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_DATO")).Length > 0 Then
                                Dim bDesglose As Boolean = False
                                drCampo = dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_DATO"))(0)
                                If drCampo.Item("ES_SUBCAMPO") = 1 Then
                                    If mlInstancia > 0 Then
                                        oValorCampo = oInstancia.cargarCampoDesglose(lCiaComp, oCond.Item("CAMPO_DATO"), nLinea, Session("FS_Portal_Server"))
                                    End If
                                    bDesglose = True
                                End If
                                If Not bDesglose Then
                                    Select Case drCampo.Item("SUBTIPO")
                                        Case 2
                                            oValorCampo = DBNullToSomething(drCampo.Item("VALOR_NUM"))
                                        Case 3
                                            oValorCampo = DBNullToSomething(drCampo.Item("VALOR_FEC"))
                                        Case 4
                                            oValorCampo = DBNullToSomething(drCampo.Item("VALOR_BOOL"))
                                        Case Else
                                            oValorCampo = DBNullToStr(drCampo.Item("VALOR_TEXT"))

                                            Dim sPresup() As String
                                            Dim lIdPres As Integer

                                            If DBNullToSomething(drCampo.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.PRES1 Then
                                                sPresup = Split(drCampo.Item("VALOR_TEXT"), "_")
                                                lIdPres = CInt(sPresup(1))

                                                Dim oPres1 As Fullstep.PMPortalServer.PresProyectosNivel1 = FSWSServer.Get_PresProyectosNivel1
                                                Select Case CInt(sPresup(0))
                                                    Case 1
                                                        oPres1.LoadData(lCiaComp, lIdPres)
                                                    Case 2
                                                        oPres1.LoadData(lCiaComp, Nothing, lIdPres)
                                                    Case 3
                                                        oPres1.LoadData(lCiaComp, Nothing, Nothing, lIdPres)
                                                    Case 4
                                                        oPres1.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPres)
                                                End Select
                                                oValorCampo = oPres1.Data.Tables(0).Rows(0).Item("ANYO") + " - " + oPres1.Data.Tables(0).Rows(0).Item("PRES" & sPresup(0))
                                                oPres1 = Nothing
                                            ElseIf DBNullToSomething(drCampo.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Pres2 Then
                                                sPresup = Split(drCampo.Item("VALOR_TEXT"), "_")
                                                lIdPres = CInt(sPresup(1))

                                                Dim oPres2 As Fullstep.PMPortalServer.PresContablesNivel1 = FSWSServer.Get_PresContablesNivel1
                                                Select Case CInt(sPresup(0))
                                                    Case 1
                                                        oPres2.LoadData(lCiaComp, lIdPres)
                                                    Case 2
                                                        oPres2.LoadData(lCiaComp, Nothing, lIdPres)
                                                    Case 3
                                                        oPres2.LoadData(lCiaComp, Nothing, Nothing, lIdPres)
                                                    Case 4
                                                        oPres2.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPres)
                                                End Select
                                                oValorCampo = oPres2.Data.Tables(0).Rows(0).Item("ANYO") + " - " + oPres2.Data.Tables(0).Rows(0).Item("PRES" & sPresup(0))
                                                oPres2 = Nothing
                                            ElseIf DBNullToSomething(drCampo.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Pres3 Then
                                                sPresup = Split(drCampo.Item("VALOR_TEXT"), "_")
                                                lIdPres = CInt(sPresup(1))

                                                Dim oPres3 As Fullstep.PMPortalServer.PresConceptos3Nivel1 = FSWSServer.Get_PresConceptos3Nivel1
                                                Select Case CInt(sPresup(0))
                                                    Case 1
                                                        oPres3.LoadData(lCiaComp, lIdPres)
                                                    Case 2
                                                        oPres3.LoadData(lCiaComp, Nothing, lIdPres)
                                                    Case 3
                                                        oPres3.LoadData(lCiaComp, Nothing, Nothing, lIdPres)
                                                    Case 4
                                                        oPres3.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPres)
                                                End Select
                                                oValorCampo = oPres3.Data.Tables(0).Rows(0).Item("PRES" & sPresup(0))
                                                oPres3 = Nothing
                                            ElseIf DBNullToSomething(drCampo.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Pres4 Then
                                                sPresup = Split(drCampo.Item("VALOR_TEXT"), "_")
                                                lIdPres = CInt(sPresup(1))

                                                Dim oPres4 As Fullstep.PMPortalServer.PresConceptos4Nivel1 = FSWSServer.Get_PresConceptos4Nivel1
                                                Select Case CInt(sPresup(0))
                                                    Case 1
                                                        oPres4.LoadData(lCiaComp, lIdPres)
                                                    Case 2
                                                        oPres4.LoadData(lCiaComp, Nothing, lIdPres)
                                                    Case 3
                                                        oPres4.LoadData(lCiaComp, Nothing, Nothing, lIdPres)
                                                    Case 4
                                                        oPres4.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPres)
                                                End Select
                                                oValorCampo = oPres4.Data.Tables(0).Rows(0).Item("PRES" & sPresup(0))
                                                oPres4 = Nothing
                                            End If

                                    End Select

                                End If
                                iTipo = drCampo.Item("SUBTIPO")
                                If (sMoneda <> "" Or oCond.Item("TIPO_VALOR") = 6 Or oCond.Item("TIPO_VALOR") = 7) Then
                                    If dCambioVigenteInstancia <> 0 Then
                                        oValorCampo = oValorCampo / dCambioVigenteInstancia
                                    End If
                                End If
                            End If

                        Case 2 'Peticionario
                            If mlInstancia > 0 Then
                                'Comprueba si la solicitud es de no conformidad o certificado.Esto habr� que quitarlo cuando
                                'se haga el workflow para certificados y no conformidades
                                oValorCampo = oInstancia.Peticionario
                            End If
                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio

                        Case 3 'Departamento del peticionario
                            If mlInstancia > 0 Then
                                Dim oPer As Fullstep.PMPortalServer.Persona = FSWSServer.Get_Persona
                                oPer.LoadData(lCiaComp, oInstancia.Peticionario)
                                oValorCampo = oPer.Departamento
                            End If

                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio
                        Case 4 'UON del peticionario

                            If mlInstancia > 0 Then
                                Dim oPer As Fullstep.PMPortalServer.Persona = FSWSServer.Get_Persona
                                oPer.LoadData(lCiaComp, oInstancia.Peticionario)
                                oValorCampo = oPer.UONs
                            End If

                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio

                        Case 5 'N� de procesos de compra abiertos
                            oValorCampo = 0
                            If mlInstancia > 0 Then
                                oInstancia.DevolverProcesosRelacionados()
                                oValorCampo = oInstancia.Procesos.Tables(0).Rows.Count
                            End If
                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico

                        Case 6 'Importe adj (LO DEVUELVE EN MONEDA CENTRAL...Y SE COMPARA CON MONEDA DEL FORMULARIO --->PENDIENTE DE CAMBIAR)
                            oValorCampo = 0
                            If mlInstancia > 0 Then
                                oValorCampo = oInstancia.ObtenerImporteAdjudicado
                            End If
                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico

                        Case 7 'Importe de la solicitud de compra
                            oValorCampo = 0
                            If mlInstancia > 0 Then
                                oValorCampo = oInstancia.Importe
                            End If
                            If dCambioVigenteInstancia <> 0 Then
                                oValorCampo = oValorCampo / dCambioVigenteInstancia
                            End If
                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico
                        Case 10 'ImporteFactura
                            'Portal el DetalleFactura.aspx.vb no hace uso de Fullstep.PMPortalWeb.desglosecontrol.
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                        Case 11 'ImporteCostesPlanificados
                            'Portal el DetalleFactura.aspx.vb no hace uso de Fullstep.PMPortalWeb.desglosecontrol.
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                        Case 12 'ImporteCostesNoPlanificados
                            'Portal el DetalleFactura.aspx.vb no hace uso de Fullstep.PMPortalWeb.desglosecontrol.
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                        Case 13 'ImporteDctosPlanificados
                            'Portal el DetalleFactura.aspx.vb no hace uso de Fullstep.PMPortalWeb.desglosecontrol.
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                        Case 14 'ImporteDctosNoPlanificados
                            'Portal el DetalleFactura.aspx.vb no hace uso de Fullstep.PMPortalWeb.desglosecontrol.
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                        Case 15 'NumDiscrepanciasAbiertas
                            'Portal el DetalleFactura.aspx.vb no hace uso de Fullstep.PMPortalWeb.desglosecontrol.
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                    End Select

                    'Luego <EXPRESION DERECHA>

                    Select Case oCond.Item("TIPO_VALOR")
                        Case 1 'Campo de formulario
                            If mlInstancia > 0 Then
                                dsCampos = oInstancia.cargarCampo(lCiaComp, oCond.Item("CAMPO_VALOR"))
                            End If

                            If dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_VALOR")).Length > 0 Then
                                Dim bDesglose As Boolean = False
                                drCampo = dsCampos.Tables(0).Select("ID = " & oCond.Item("CAMPO_VALOR"))(0)
                                If drCampo.Item("ES_SUBCAMPO") = 1 Then
                                    If mlInstancia > 0 Then
                                        oValor = oInstancia.cargarCampoDesglose(lCiaComp, oCond.Item("CAMPO_VALOR"), nLinea, Session("FS_Portal_Server"))
                                    End If
                                    bDesglose = True
                                End If
                                If Not bDesglose Then
                                    Select Case drCampo.Item("SUBTIPO")
                                        Case 2
                                            oValor = DBNullToSomething(drCampo.Item("VALOR_NUM"))
                                        Case 3
                                            oValor = DBNullToSomething(drCampo.Item("VALOR_FEC"))
                                        Case 4
                                            oValor = DBNullToSomething(drCampo.Item("VALOR_BOOL"))
                                        Case Else
                                            oValor = DBNullToSomething(drCampo.Item("VALOR_TEXT"))
                                    End Select
                                End If
                                If (sMoneda <> "" Or oCond.Item("TIPO_CAMPO") = 6 Or oCond.Item("TIPO_CAMPO") = 7) Then
                                    If dCambioVigenteInstancia <> 0 Then
                                        oValor = oValor / dCambioVigenteInstancia
                                    End If
                                End If
                            End If
                        Case 2 'Peticionario
                            oValorCampo = oInstancia.Peticionario
                        Case 3 'Departamento del peticionario
                            Dim oPer As Fullstep.PMPortalServer.Persona = FSWSServer.Get_Persona
                            oPer.LoadData(lCiaComp, oInstancia.Peticionario)
                            oValorCampo = oPer.Departamento
                        Case 4 'UON del peticionario
                            Dim oPer As Fullstep.PMPortalServer.Persona = FSWSServer.Get_Persona
                            oPer.LoadData(lCiaComp, oInstancia.Peticionario)
                            oValorCampo = oPer.UONs
                        Case 5 'N� de procesos de compra abiertos
                            If mlInstancia = 0 Then
                                oValorCampo = 0
                            Else
                                oInstancia.DevolverProcesosRelacionados()
                                oValorCampo = oInstancia.Procesos.Tables(0).Rows.Count
                            End If
                        Case 6 'Importe adj (LO DEVUELVE EN MONEDA CENTRAL...Y SE COMPARA CON MONEDA DEL FORMULARIO --->PENDIENTE DE CAMBIAR)
                            If mlInstancia = 0 Then
                                oValorCampo = 0
                            Else
                                oValorCampo = oInstancia.ObtenerImporteAdjudicado
                            End If
                        Case 7 'Importe de la solicitud de compra
                            oValorCampo = oInstancia.Importe

                            If dCambioVigenteInstancia <> 0 Then
                                oValorCampo = oValorCampo / dCambioVigenteInstancia
                            End If
                        Case 10 'valor est�tico
                            Select Case iTipo
                                Case 2
                                    oValor = DBNullToSomething(oCond.Item("VALOR_NUM"))
                                Case 3
                                    oValor = DBNullToSomething(oCond.Item("VALOR_FEC"))
                                Case 4
                                    oValor = DBNullToSomething(oCond.Item("VALOR_BOOL"))
                                Case Else
                                    If oCond.Item("TIPO_CAMPO") = 4 Then 'UON DEL PETICIONARIO
                                        Dim oUON() As String
                                        Dim iIndice As Integer
                                        oValor = Nothing
                                        oUON = Split(oCond.Item("VALOR_TEXT"), "-")
                                        For iIndice = 0 To UBound(oUON)
                                            If oUON(iIndice) <> "" Then
                                                oValor = oValor & Trim(oUON(iIndice)) & "-"
                                            End If
                                        Next iIndice
                                        oValor = Left(oValor, Len(oValor) - 1)
                                    Else
                                        oValor = DBNullToSomething(oCond.Item("VALOR_TEXT"))
                                    End If
                            End Select
                            If sMoneda <> "" And dCambioEnlace <> 0 Then
                                oValor = oValor / dCambioEnlace
                            ElseIf (oCond.Item("TIPO_CAMPO") = 6 Or oCond.Item("TIPO_CAMPO") = 7) And dCambioVigenteInstancia <> 0 Then
                                oValor = oValor / dCambioVigenteInstancia
                            End If
                    End Select

                    'y por �ltimo con el OPERADOR obtenemos el valor

                    Select Case iTipo
                        Case 2, 3
                            If oValorCampo Is Nothing Then 'Condici�n vac�a
                                If oValor Is Nothing Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Else
                                Select Case oCond.Item("OPERADOR")
                                    Case ">"
                                        If oValorCampo > oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If

                                    Case "<"
                                        If oValorCampo < oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case ">="
                                        If oValorCampo >= oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case "<="
                                        If oValorCampo <= oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case "="
                                        If oValorCampo = oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case "<>"
                                        If oValorCampo <> oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                End Select
                            End If
                        Case 4
                            If oValorCampo Is Nothing Then 'Condici�n vac�a
                                If oValor Is Nothing Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Else
                                If oValorCampo = oValor Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            End If
                        Case Else
                            If oValorCampo Is Nothing Then 'Condici�n vac�a
                                If oValor Is Nothing Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Else
                                Select Case UCase(oCond.Item("OPERADOR"))
                                    Case "="
                                        If UCase(oValorCampo) = UCase(oValor) Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If

                                    Case "LIKE"
                                        If Left(oValor, 1) = "%" Then
                                            If Right(oValor, 1) = "%" Then
                                                oValor = oValor.ToString.Replace("%", "")
                                                If InStr(oValorCampo.ToString, oValor.ToString) > 0 Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            Else
                                                oValor = oValor.ToString.Replace("%", "")
                                                If oValorCampo.ToString.EndsWith(oValor.ToString) Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            End If
                                        Else
                                            If Right(oValor, 1) = "%" Then
                                                oValor = oValor.ToString.Replace("%", "")
                                                If oValorCampo.ToString.StartsWith(oValor.ToString) Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            Else
                                                If UCase(oValorCampo.ToString) = UCase(oValor.ToString) Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If

                                            End If
                                        End If

                                    Case "<>"
                                        If UCase(oValorCampo.ToString) <> UCase(oValor.ToString) Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                End Select
                            End If
                    End Select

                    i += 1


                Next
                oValor = Nothing
                Try
                    iEq.Parse(sFormula, sVariables)
                    oValor = iEq.Evaluate(dValues)

                Catch ex As USPExpress.ParseException

                End Try


            Catch e As Exception
                Dim a As String = e.Message
            Finally
                oInstancia = Nothing
            End Try


            Return (oValor > 0)

        End Function
        ''' Revisado por: blp. Fecha: 10/10/2011
        ''' <summary>
        ''' Carga los valores por defecto desde la bbdd
        ''' </summary>
        ''' <param name="lCiaComp">codigo de compania</param>
        ''' <param name="oDs">dataset con la info de bbdd</param>      
        ''' <param name="sIdi">idioma</param>
        ''' <param name="NumLineas">numero de lineas del desglose</param>   
        ''' <remarks>Llamada desde: Page_load; Tiempo m�ximo: instantaneo</remarks>
        Private Sub CargarValoresDefecto(ByVal lCiaComp As Integer, ByVal oDs As DataSet, ByVal sIdi As String, ByVal NumLineas As Integer)
            Dim oFSDSEntry As Fullstep.DataEntry.GeneralEntry
            Dim oRow As DataRow
            Dim Lineas As Integer
            Dim i As Integer
            Dim sTexto As String
            Dim sValor As String
            Dim sValorMat As String
            Dim iContadorPres As Integer
            Dim sAdjun As String
            Dim idAdjun As String
            Dim sCodPais As String
            Dim iTipoGS As Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS
            Dim iTipo As Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral
            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
            Dim arrMat(4) As String
            Dim iNivel As Integer
            Dim lIdPresup As Integer
            Dim dPorcent As Decimal
            Dim arrPresupuestos() As String
            Dim oPresup As String
            Dim arrPresup(2) As String
            Dim oPres1 As Fullstep.PMPortalServer.PresProyectosNivel1
            Dim oPres2 As Fullstep.PMPortalServer.PresContablesNivel1
            Dim oPres3 As Fullstep.PMPortalServer.PresConceptos3Nivel1
            Dim oPres4 As Fullstep.PMPortalServer.PresConceptos4Nivel1
            Dim oDSPres As DataSet
            Dim iAnyo As Integer
            Dim sUon1 As String
            Dim sUon2 As String
            Dim sUon3 As String
            Dim bSinPermisos As Boolean

            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSWSServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DesgloseControl, sIdi)

            Dim oTextos As DataTable = oDict.Data.Tables(0)
            Dim oGMN1s As Fullstep.PMPortalServer.GruposMatNivel1
            Dim oGMN1 As Fullstep.PMPortalServer.GrupoMatNivel1
            Dim oGMN2 As Fullstep.PMPortalServer.GrupoMatNivel2
            Dim oGMN3 As Fullstep.PMPortalServer.GrupoMatNivel3
            Dim sIdCampoHijoDesglose As String
            Dim oRowLista() As DataRow
            Dim DeDonde As Integer = 5
            Try
                Lineas = oDs.Tables(DeDonde).Rows.Count
            Catch ex As Exception
                DeDonde = 2
            End Try

            For Each oRow In oDs.Tables(DeDonde).Rows
                sValor = ""

                sIdCampoHijoDesglose = oRow.Item("CAMPO_HIJO").ToString

                For Lineas = 1 To NumLineas
                    oFSDSEntry = Me.tblDesglose.FindControl("fsdsentry_" & Lineas.ToString & "_" + sIdCampoHijoDesglose)

                    If Not oFSDSEntry Is Nothing Then
                        oFSDSEntry.Portal = True
                        oFSDSEntry.Posee_Defecto = False
                        oFSDSEntry.Valor_Defecto = System.DBNull.Value
                        oFSDSEntry.Texto_Defecto = System.DBNull.Value
                        iTipo = oFSDSEntry.Tipo
                        iTipoGS = oFSDSEntry.TipoGS

                        If (Not IsDBNull(oRow.Item("VALOR_TEXT")) Or Not IsDBNull(oRow.Item("VALOR_NUM")) Or Not IsDBNull(oRow.Item("VALOR_FEC")) Or Not IsDBNull(oRow.Item("VALOR_BOOL")) Or iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoArchivo Or iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoEditor Or iTipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo Or iTipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo Or iTipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo) Then

                            Select Case iTipo
                                Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoString, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoCorto, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoLargo, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio, PMPortalServer.TiposDeDatos.TipoGeneral.TipoEditor

                                    oFSDSEntry.Valor_Defecto = DBNullToSomething(oRow.Item("VALOR_TEXT"))

                                    If oFSDSEntry.Valor_Defecto <> Nothing Then

                                        oFSDSEntry.Posee_Defecto = True

                                        If iTipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.SinTipo And oFSDSEntry.Intro = 1 Then
                                            If oFSDSEntry.Lista.Tables.Count > 0 Then
                                                If DBNullToSomething(oRow.Item("VALOR_NUM").ToString) <> Nothing Then
                                                    oRowLista = oFSDSEntry.Lista.Tables(0).Select("ID = " + oRow.Item("CAMPO_HIJO").ToString + " and ORDEN=" + oRow.Item("VALOR_NUM").ToString)
                                                Else
                                                    oRowLista = oFSDSEntry.Lista.Tables(0).Select("ID = " + oRow.Item("CAMPO_HIJO").ToString + " and VALOR_TEXT_" & sIdi & " =" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))
                                                End If
                                                If oRowLista.Length > 0 Then
                                                    oFSDSEntry.Texto_Defecto = oRowLista(0).Item("VALOR_TEXT_" & sIdi)
                                                    oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_NUM")
                                                Else
                                                    oFSDSEntry.Texto_Defecto = oFSDSEntry.Valor_Defecto
                                                    oFSDSEntry.Valor_Defecto = Nothing
                                                End If
                                            Else
                                                oFSDSEntry.Texto_Defecto = oFSDSEntry.Valor_Defecto
                                                oFSDSEntry.Valor_Defecto = Nothing
                                            End If
                                        End If
                                    End If

                                Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico
                                    oFSDSEntry.Valor_Defecto = DBNullToSomething(oRow.Item("VALOR_NUM"))
                                    If oFSDSEntry.Valor_Defecto <> Nothing OrElse oFSDSEntry.Valor_Defecto = 0 Then oFSDSEntry.Posee_Defecto = True

                                Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoFecha
                                    Dim iTipoFecha As Fullstep.PMPortalServer.TiposDeDatos.TipoFecha

                                    iTipoFecha = DBNullToSomething(modUtilidades.DBNullToSomething(oRow.Item("VALOR_NUM")))
                                    oFSDSEntry.Valor_Defecto = modUtilidades.DevolverFechaRelativaA(iTipoFecha, DBNullToSomething(oRow.Item("VALOR_FEC")))
                                    If oFSDSEntry.Valor_Defecto <> Nothing Then oFSDSEntry.Posee_Defecto = True

                                Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoArchivo
                                    sAdjun = ""
                                    idAdjun = ""
                                    For Each oDSRowAdjun As DataRow In oRow.GetChildRows("REL_LINEA_ADJUNTO_DEFECTO")
                                        idAdjun += oDSRowAdjun.Item("ID").ToString() + "xx"
                                        sAdjun += oDSRowAdjun.Item("NOM") + " (" + FormatNumber(CDbl(DBNullToSomething(oDSRowAdjun.Item("DATASIZE")) / 1024), oUser.NumberFormat) + " Kb.), "
                                    Next
                                    If sAdjun <> "" Then
                                        sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
                                        idAdjun = idAdjun.Substring(0, idAdjun.Length - 2)
                                    End If
                                    oFSDSEntry.Valor_Defecto = idAdjun
                                    oFSDSEntry.Texto_Defecto = AjustarAnchoTextoPixels(sAdjun, 150, "Verdana", 12)

                                    If oFSDSEntry.Valor_Defecto <> "" Then oFSDSEntry.Posee_Defecto = True

                                Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoBoolean
                                    oFSDSEntry.Valor_Defecto = DBNullToSomething(oRow.Item("VALOR_BOOL"))
                                    If oFSDSEntry.Valor_Defecto <> Nothing Then oFSDSEntry.Posee_Defecto = True
                            End Select

                            Select Case iTipoGS
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.CodArticulo
                                    If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                        Dim oArts As Fullstep.PMPortalServer.Articulos
                                        oArts = FSWSServer.Get_Articulos
                                        Dim oArticulo As Fullstep.PMPortalServer.PropiedadesArticulo = oArts.Articulo_Get_Properties(lCiaComp, oRow.Item("VALOR_TEXT"))
                                        If oArticulo.ExisteArticulo Then
                                            oFSDSEntry.Valor_Defecto = oArticulo.Codigo
                                            oFSDSEntry.Texto_Defecto = oArticulo.Denominacion
                                        Else
                                            oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT")
                                            oFSDSEntry.Valor_Defecto = Nothing
                                        End If
                                    End If
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.NuevoCodArticulo
                                    If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                        oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT")
                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                    End If
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.DenArticulo
                                    If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                        oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT")
                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                    End If
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Dest
                                    If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                        oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))

                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                        oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN") & If(IsDBNull(oRowLista(0).Item("POB")), "", " (" & oRowLista(0).Item("POB") & ")")
                                    End If
                                    oFSDSEntry.Lista = Nothing
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Unidad
                                    If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                        oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))

                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                        oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN")
                                    End If
                                    oFSDSEntry.Lista = Nothing
                                Case TiposDeDatos.TipoCampoGS.ProveedorERP
                                    If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                        If (oFSDSEntry.Lista IsNot Nothing) Then
                                            oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oFSDSEntry.Valor_Defecto))
                                            If oRowLista.Length > 0 Then _
                                                oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN")
                                        End If
                                    End If
                                    'Vaciamos el contenido porque s�lo lo necesit�bamos aqu� y har�a fallar la carga de datos desde ajax para los webdropdown 11.1
                                    oFSDSEntry.Lista = Nothing
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.FormaPago
                                    If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                        oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))

                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                        oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN")
                                    End If
                                    oFSDSEntry.Lista = Nothing
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Moneda
                                    If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                        oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))

                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                        oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN")
                                    End If
                                    oFSDSEntry.Lista = Nothing
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Pais
                                    If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                        oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))
                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                        oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN")
                                        sCodPais = oRow.Item("VALOR_TEXT")
                                    End If
                                    oFSDSEntry.Lista = Nothing
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Provincia
                                    If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                        If (oFSDSEntry.Lista IsNot Nothing) Then
                                            oRowLista = oFSDSEntry.Lista.Tables(0).Select("PAICOD=" + StrToSQLNULL(sCodPais) + " AND COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))
                                            If oRowLista.Length > 0 Then _
                                                oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN") 'oProvis.Data.Tables(0).Rows(0).Item("DEN")
                                        End If
                                    End If
                                    'Vaciamos el contenido porque s�lo lo necesit�bamos aqu� y har�a fallar la carga de datos desde ajax para los webdropdown 11.1
                                    oFSDSEntry.Lista = Nothing
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.PRES1, PMPortalServer.TiposDeDatos.TipoCampoGS.Pres2, PMPortalServer.TiposDeDatos.TipoCampoGS.Pres3, PMPortalServer.TiposDeDatos.TipoCampoGS.Pres4
                                    oFSDSEntry.Valor_Defecto = modUtilidades.DBNullToSomething(oRow.Item("VALOR_TEXT"))
                                    If DBNullToSomething(oRow.Item("VALOR_TEXT")) <> Nothing Then
                                        sValor = ""
                                        sTexto = ""
                                        iContadorPres = 0
                                        arrPresupuestos = oRow.Item("VALOR_TEXT").split("#")
                                        For Each oPresup In arrPresupuestos
                                            iContadorPres = iContadorPres + 1
                                            arrPresup = oPresup.Split("_")
                                            iNivel = arrPresup(0)
                                            lIdPresup = arrPresup(1)
                                            'Los presupuestos siempre llevan el porcentaje como 0.xx salvo cuando es 100% que llevan 1
                                            dPorcent = Numero(arrPresup(2), ".", "")

                                            Select Case iTipoGS
                                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.PRES1
                                                    oPres1 = FSWSServer.Get_PresProyectosNivel1
                                                    Select Case iNivel
                                                        Case 1
                                                            oPres1.LoadData(lCiaComp, lIdPresup)
                                                        Case 2
                                                            oPres1.LoadData(lCiaComp, Nothing, lIdPresup)
                                                        Case 3
                                                            oPres1.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
                                                        Case 4
                                                            oPres1.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
                                                    End Select
                                                    oDSPres = oPres1.Data
                                                    oPres1 = Nothing
                                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Pres2
                                                    oPres2 = FSWSServer.Get_PresContablesNivel1
                                                    Select Case iNivel
                                                        Case 1
                                                            oPres2.LoadData(lCiaComp, lIdPresup)
                                                        Case 2
                                                            oPres2.LoadData(lCiaComp, Nothing, lIdPresup)
                                                        Case 3
                                                            oPres2.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
                                                        Case 4
                                                            oPres2.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
                                                    End Select
                                                    oDSPres = oPres2.Data
                                                    oPres2 = Nothing
                                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Pres3
                                                    oPres3 = FSWSServer.Get_PresConceptos3Nivel1
                                                    Select Case iNivel
                                                        Case 1
                                                            oPres3.LoadData(lCiaComp, lIdPresup)
                                                        Case 2
                                                            oPres3.LoadData(lCiaComp, Nothing, lIdPresup)
                                                        Case 3
                                                            oPres3.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
                                                        Case 4
                                                            oPres3.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
                                                    End Select
                                                    oDSPres = oPres3.Data
                                                    oPres3 = Nothing
                                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Pres4
                                                    oPres4 = FSWSServer.Get_PresConceptos4Nivel1
                                                    Select Case iNivel
                                                        Case 1
                                                            oPres4.LoadData(lCiaComp, lIdPresup)
                                                        Case 2
                                                            oPres4.LoadData(lCiaComp, Nothing, lIdPresup)
                                                        Case 3
                                                            oPres4.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
                                                        Case 4
                                                            oPres4.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
                                                    End Select
                                                    oDSPres = oPres4.Data
                                                    oPres4 = Nothing
                                            End Select

                                            If Not oDSPres Is Nothing Then

                                                If oDSPres.Tables(0).Rows.Count > 0 Then

                                                    With oDSPres.Tables(0).Rows(0)
                                                        'comprobamos que el valor por defecto cumpla la restricci�n del usuario. SI NO CUMPLE, NO APARECER�
                                                        If Not oDSPres.Tables(0).Columns("ANYO") Is Nothing Then
                                                            iAnyo = .Item("ANYO")

                                                        End If
                                                        sUon1 = DBNullToSomething(.Item("UON1"))
                                                        sUon2 = DBNullToSomething(.Item("UON2"))
                                                        sUon3 = DBNullToSomething(.Item("UON3"))
                                                        bSinPermisos = False
                                                        If sUon3 <> Nothing And oUser.UON3Aprobador <> Nothing Then
                                                            If sUon3 <> oUser.UON3Aprobador Then
                                                                'malo (el usuario no tiene permisos para asignar el presupuesto por defecto )
                                                                bSinPermisos = True
                                                            End If
                                                        End If
                                                        If sUon2 <> Nothing And oUser.UON2Aprobador <> Nothing Then
                                                            If sUon2 <> oUser.UON2Aprobador Then

                                                                bSinPermisos = True
                                                            End If
                                                        End If
                                                        If sUon1 <> Nothing And oUser.UON1Aprobador <> Nothing Then
                                                            If sUon1 <> oUser.UON1Aprobador Then

                                                                bSinPermisos = True
                                                            End If
                                                        End If


                                                        If Not bSinPermisos Then
                                                            If Not oDs.Tables(0).Columns("ANYO") Is Nothing Then
                                                                sTexto += .Item("ANYO").ToString + " - "
                                                            End If

                                                            If Not IsDBNull(.Item("PRES4")) Then
                                                                sTexto += .Item("PRES4").ToString & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
                                                            ElseIf Not IsDBNull(.Item("PRES3")) Then
                                                                sTexto += .Item("PRES3").ToString & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
                                                            ElseIf Not IsDBNull(.Item("PRES2")) Then
                                                                sTexto += .Item("PRES2").ToString & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
                                                            ElseIf Not IsDBNull(.Item("PRES1")) Then
                                                                sTexto += .Item("PRES1").ToString & " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
                                                            End If
                                                            sValor += oPresup + "#"
                                                        End If
                                                    End With
                                                End If
                                            End If
                                        Next
                                        If sValor <> Nothing Then
                                            sValor = Left(sValor, sValor.Length - 1)
                                        End If
                                        oFSDSEntry.Valor_Defecto = sValor
                                        If sTexto = "" Then
                                            If Not oFSDSEntry.ReadOnly Then
                                                oFSDSEntry.Texto_Defecto = oTextos.Rows(1).Item(1)
                                            End If
                                        Else
                                            oFSDSEntry.Texto_Defecto = sTexto
                                        End If
                                    Else
                                        If Not oFSDSEntry.ReadOnly Then
                                            oFSDSEntry.Texto_Defecto = oTextos.Rows(1).Item(1)
                                        End If
                                    End If

                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Proveedor
                                    If DBNullToSomething(oRow.Item("VALOR_TEXT")) <> Nothing Then
                                        Dim oProve As PMPortalServer.Proveedor
                                        oProve = FSWSServer.get_Proveedor
                                        oProve.Cod = oRow.Item("VALOR_TEXT")
                                        oProve.Load(sIdi, lCiaComp)
                                        oFSDSEntry.Texto_Defecto = oProve.Cod + " - " + oProve.Den.ToString.Replace("'", "")
                                    ElseIf Not oFSDSEntry.ReadOnly Then
                                        oFSDSEntry.Texto_Defecto = oTextos.Rows(2).Item(1)
                                    End If
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Material
                                    Dim sMat As String = DBNullToSomething(oRow.Item("VALOR_TEXT"))
                                    Dim lLongCod As Integer
                                    sValorMat = sMat

                                    For i = 1 To 4
                                        arrMat(i) = ""
                                    Next i

                                    i = 1
                                    While Trim(sMat) <> ""
                                        Select Case i
                                            Case 1
                                                lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN1
                                            Case 2
                                                lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN2
                                            Case 3
                                                lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN3
                                            Case 4
                                                lLongCod = FSWSServer.LongitudesDeCodigos.giLongCodGMN4
                                        End Select
                                        arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
                                        sMat = Mid(sMat, lLongCod + 1)
                                        i = i + 1
                                    End While

                                    If arrMat(4) <> "" Then
                                        oGMN3 = FSWSServer.Get_GrupoMatNivel3
                                        oGMN3.GMN1Cod = arrMat(1)
                                        oGMN3.GMN2Cod = arrMat(2)
                                        oGMN3.Cod = arrMat(3)

                                        oGMN3.CargarTodosLosGruposMatDesde(lCiaComp, 1, sIdi, arrMat(4), , True)
                                        If CType(Me.Page, FSPMPage).Acceso.gbMaterialVerTodosNiveles Then
                                            sValor = arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + arrMat(4) + " - " + oGMN3.GruposMatNivel4.Item(arrMat(4)).Den
                                        Else
                                            sValor = arrMat(4) + " - " + oGMN3.GruposMatNivel4.Item(arrMat(4)).Den
                                        End If
                                        oGMN3 = Nothing

                                    ElseIf arrMat(3) <> "" Then
                                        oGMN2 = FSWSServer.Get_GrupoMatNivel2
                                        oGMN2.GMN1Cod = arrMat(1)
                                        oGMN2.Cod = arrMat(2)
                                        oGMN2.CargarTodosLosGruposMatDesde(lCiaComp, 1, sIdi, arrMat(3), , True)
                                        If CType(Me.Page, FSPMPage).Acceso.gbMaterialVerTodosNiveles Then
                                            sValor = arrMat(1) + " - " + arrMat(2) + " - " + arrMat(3) + " - " + oGMN2.GruposMatNivel3.Item(arrMat(3)).Den
                                        Else
                                            sValor = arrMat(3) + " - " + oGMN2.GruposMatNivel3.Item(arrMat(3)).Den
                                        End If
                                        oGMN2 = Nothing

                                    ElseIf arrMat(2) <> "" Then
                                        oGMN1 = FSWSServer.Get_GrupoMatNivel1
                                        oGMN1.Cod = arrMat(1)
                                        oGMN1.CargarTodosLosGruposMatDesde(lCiaComp, 1, sIdi, arrMat(2), , True)
                                        If CType(Me.Page, FSPMPage).Acceso.gbMaterialVerTodosNiveles Then
                                            sValor = arrMat(1) + " - " + arrMat(2) + " - " + oGMN1.GruposMatNivel2.Item(arrMat(2)).Den
                                        Else
                                            sValor = arrMat(2) + " - " + oGMN1.GruposMatNivel2.Item(arrMat(2)).Den
                                        End If
                                        oGMN1 = Nothing

                                    ElseIf arrMat(1) <> "" Then
                                        oGMN1s = FSWSServer.Get_GruposMatNivel1
                                        oGMN1s.CargarTodosLosGruposMatDesde(lCiaComp, 1, sIdi, arrMat(1), , , True)
                                        sValor = arrMat(1) + " - " + oGMN1s.Item(arrMat(1)).Den
                                        oGMN1s = Nothing
                                    End If
                                    oFSDSEntry.Texto_Defecto = sValor
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.EstadoNoConf
                                    If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                        Dim oInstancia As Fullstep.PMPortalServer.Instancia
                                        oInstancia = FSWSServer.Get_Instancia
                                        oInstancia.ID = mlInstancia
                                        Dim oDSListaEstados As DataSet = oInstancia.CargarTiposEstadoAccion(lCiaComp)
                                        Dim lIndexEstado As Integer
                                        If Not IsDBNull(oRow.Item("INDEX")) Then
                                            If lIndexEstado <> Nothing Then
                                                oFSDSEntry.Valor_Defecto = oDSListaEstados.Tables(0).Rows(lIndexEstado).Item("COD")
                                                oFSDSEntry.Texto_Defecto = Mid(oDSListaEstados.Tables(0).Rows(lIndexEstado).Item("DEN_" & sIdi), 6)
                                            Else
                                                Dim oEsts As Fullstep.PMPortalServer.EstadosNoConf
                                                oEsts = FSWSServer.get_EstadosNoConf()
                                                oEsts.LoadData(lCiaComp, mlInstancia, oRow.Item("VALOR_TEXT"), sIdi)
                                                If oEsts.Data.Tables(0).Rows.Count > 0 Then
                                                    oFSDSEntry.Valor_Defecto = oEsts.Data.Tables(0).Rows(0).Item("COD")
                                                    oFSDSEntry.Texto_Defecto = oEsts.Data.Tables(0).Rows(0).Item("DEN_" & sIdi)
                                                End If
                                            End If
                                        End If
                                    End If
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.EstadoInternoNoConf
                                    If DBNullToSomething(oRow.Item("VALOR_NUM")) > 0 Then
                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_NUM")
                                        oFSDSEntry.Texto_Defecto = oTextos.Rows(oRow.Item("VALOR_NUM") + 4).Item(1)
                                    Else
                                        oFSDSEntry.Texto_Defecto = oTextos.Rows(5).Item(1)
                                    End If
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Persona
                                    If DBNullToSomething(oRow.Item("VALOR_TEXT")) <> Nothing Then
                                        Dim oPersona As PMPortalServer.Persona = FSWSServer.Get_Persona
                                        oPersona.LoadData(lCiaComp, oRow.Item("VALOR_TEXT").ToString)
                                        If Not oPersona.Codigo Is Nothing Then oFSDSEntry.Texto_Defecto = oPersona.Denominacion(True)
                                    End If
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.UnidadOrganizativa
                                    If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                        oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT")
                                    End If
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Departamento
                                    If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                        If (oFSDSEntry.Lista IsNot Nothing) Then
                                            oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))
                                            If oRowLista.Length > 0 Then _
                                                oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN") 'oDepartamento.Data.Tables(0).Rows(0).Item("COD").ToString & " - " & oDepartamento.Data.Tables(0).Rows(0).Item("DEN").ToString
                                        End If
                                    End If
                                    'Vaciamos el contenido porque s�lo lo necesit�bamos aqu� y har�a fallar la carga de datos desde ajax para los webdropdown 11.1
                                    oFSDSEntry.Lista = Nothing

                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.OrganizacionCompras
                                    If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                        'Mostrar la informacion referente a la Organizacion de Compras
                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                        oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))
                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                        oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN") 'oOrganizacionCompras.Data.Tables(0).Rows(0).Item("COD").ToString & " - " & oOrganizacionCompras.Data.Tables(0).Rows(0).Item("DEN").ToString
                                    End If
                                    oFSDSEntry.Lista = Nothing
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Centro
                                    If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                                        'Mostrar la informacion referente al CENTRO
                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_TEXT")
                                        If (oFSDSEntry.Lista IsNot Nothing) Then
                                            oRowLista = oFSDSEntry.Lista.Tables(0).Select("COD=" + StrToSQLNULL(oRow.Item("VALOR_TEXT").ToString))
                                            If oRowLista.Length > 0 Then _
                                                oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN") 'oCentro.Data.Tables(0).Rows(0).Item("COD").ToString & " - " & oCentro.Data.Tables(0).Rows(0).Item("DEN").ToString
                                        End If
                                    End If
                                    'Vaciamos el contenido porque s�lo lo necesit�bamos aqu� y har�a fallar la carga de datos desde ajax para los webdropdown 11.1
                                    oFSDSEntry.Lista = Nothing
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Almacen
                                    If Not IsDBNull(oRow.Item("VALOR_NUM")) Then
                                        'Mostrar la informacion referente al ALMACEN
                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_NUM")
                                        If (oFSDSEntry.Lista IsNot Nothing) Then
                                            oRowLista = oFSDSEntry.Lista.Tables(0).Select("ID=" + StrToSQLNULL(oRow.Item("VALOR_NUM").ToString))
                                            'oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN") 'oAlmacen.Data.Tables(0).Rows(0).Item("COD").ToString & " - " & oAlmacen.Data.Tables(0).Rows(0).Item("DEN").ToString
                                            If oRowLista.Length > 0 Then _
                                                oFSDSEntry.Texto_Defecto = "(" & oRow.Item("VALOR_NUM") & ") - " & oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN") 'oAlmacen.Data.Tables(0).Rows(0).Item("COD").ToString & " - " & oAlmacen.Data.Tables(0).Rows(0).Item("DEN").ToString
                                        End If
                                    End If
                                    'Vaciamos el contenido porque s�lo lo necesit�bamos aqu� y har�a fallar la carga de datos desde ajax para los webdropdown 11.1
                                    oFSDSEntry.Lista = Nothing
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.Empresa
                                    If Not IsDBNull(oRow.Item("VALOR_NUM")) Then
                                        'Mostrar la informacion referente al ALMACEN
                                        oFSDSEntry.Valor_Defecto = oRow.Item("VALOR_NUM")
                                        If (oFSDSEntry.Lista IsNot Nothing) Then
                                            oRowLista = oFSDSEntry.Lista.Tables(0).Select("ID=" + StrToSQLNULL(oRow.Item("VALOR_NUM").ToString))
                                            If oRowLista.Length > 0 Then _
                                                oFSDSEntry.Texto_Defecto = "(" & oRow.Item("VALOR_NUM") & ") - " & oRow.Item("VALOR_TEXT") & " - " & oRowLista(0).Item("DEN") 'oAlmacen.Data.Tables(0).Rows(0).Item("COD").ToString & " - " & oAlmacen.Data.Tables(0).Rows(0).Item("DEN").ToString
                                        End If
                                    End If
                                    'Vaciamos el contenido porque s�lo lo necesit�bamos aqu� y har�a fallar la carga de datos desde ajax para los webdropdown 11.1
                                    oFSDSEntry.Lista = Nothing
                                Case PMPortalServer.TiposDeDatos.TipoCampoGS.ProveedorAdj
                                    If DBNullToSomething(oRow.Item("VALOR_TEXT")) <> Nothing Then
                                        Dim oProve As PMPortalServer.Proveedor = FSWSServer.get_Proveedor
                                        oProve.Cod = oRow.Item("VALOR_TEXT")
                                        oProve.Load(sIdi, lCiaComp)
                                        oFSDSEntry.Texto_Defecto = oProve.Cod + " - " + oProve.Den.ToString.Replace("'", "")
                                    End If
                            End Select

                            If oFSDSEntry.Tabla_Externa > 0 Then
                                If Not oFSDSEntry.Valor_Defecto Is Nothing Then
                                    Dim oTablaExterna As Fullstep.PMPortalServer.TablaExterna = FSWSServer.get_TablaExterna
                                    oTablaExterna.LoadDefTabla(lCiaComp, oFSDSEntry.Tabla_Externa, False)
                                    oTablaExterna.LoadData(False)
                                    oFSDSEntry.Texto_Defecto = oTablaExterna.DescripcionReg(oFSDSEntry.Valor_Defecto, sIdi, oUser.DateFormat, oUser.NumberFormat)
                                End If
                            End If
                        End If

                        If iTipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.ImporteRepercutido Then
                            If Not IsDBNull(oRow.Item("VALOR_NUM")) Then
                                oFSDSEntry.Texto_Defecto = oRow.Item("VALOR_NUM")
                                oFSDSEntry.Posee_Defecto = True
                            End If

                            oFSDSEntry.MonCentral = oRow.Item("MONCEN")
                            oFSDSEntry.CambioCentral = oRow.Item("CAMBIOCEN")
                        End If
                    End If
                Next
            Next
            'En el load hemos recuperado datos para las listas de provincia, departamento, centro y almac�n
            'Son campos dependientes de otros campos, cuyos datos se cargan en los webdropdown desde ajax pero necesit�bamos esa carga para
            'poder coger el valor por defecto en esta funci�n (si es que hay alguna l�nea por defecto). 
            'Es necesario vaciar posteriormente las listas porque la versi�n 11.1 del webdropdown carga mal los datos desde ajax si primero se cargan desde servidor
            'Dado que no podemos estar seguros de que se hayan vaciado, lo comprobamos de nuevo aqu�
            limpiarListadeCamposConCargaEnCliente(oDs, NumLineas)
        End Sub
        ''' <summary>
        ''' Solo los para los campos elegidos del desglose-acciones de la no conformidad de portal hay q controlar
        ''' los cambios de valor para deshacer el rechazo de la acci�n. 
        ''' Campos indicados en: FSGE: Modif. traslado incidencia Demo Ederlan / 2009 / 4
        ''' </summary>
        ''' <param name="iTipoGS">tipo campo gs del entry</param>
        ''' <returns>True si es uno de los elegidos</returns>
        ''' <remarks>Llamada desde; Tiempo m�ximo</remarks>
        Private Function CtrlPortalCambioEstado(ByVal iTipoGS As Integer) As Boolean
            CtrlPortalCambioEstado = False

            If iTipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Accion OrElse iTipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Fec_inicio _
            OrElse iTipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Fec_cierre OrElse iTipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Responsable _
            OrElse iTipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Observaciones OrElse iTipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.Documentaci�n _
            OrElse iTipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.EstadoNoConf OrElse iTipoGS = PMPortalServer.TiposDeDatos.TipoCampoGS.EstadoInternoNoConf Then
                CtrlPortalCambioEstado = True
            End If
        End Function
        ''' <summary>
        ''' Guardas sin enviar un popup donde has a�adido una linea con adjunto por defecto. La 2da vez q lo 
        ''' abres (sin salirte de la pantalla por eso es 2da vez) en bbdd esta perfecto pero en page_load tienes 
        ''' en idAdjuntos el id de form_campo no de copia_linea_desglose_adjun y no sale el texto.
        ''' </summary>
        ''' <param name="IdAdjuntos">Lista de Ids de copia_linea_desglose_adjun o form_campo</param>     
        ''' <returns>Numero de adjuntos a cargar en page_load</returns>
        ''' <remarks>Llamada desde: Page_load; Tiempo m�ximo: 0,1</remarks>
        Private Function DameNumAdjuntos(ByVal IdAdjuntos As String) As Integer
            Dim NAdjuntos As Integer

            NAdjuntos = 1

            While InStr(IdAdjuntos, ",")
                NAdjuntos = NAdjuntos + 1

                IdAdjuntos = Mid(IdAdjuntos, InStr(IdAdjuntos, ",") + 1)
            End While

            Return NAdjuntos
        End Function
        ''' Revisado por: blp. Fecha: 10/10/2011
        ''' <summary>
        ''' Carga la denominaci�n del combo
        ''' </summary>
        ''' <param name="Lista">Origen de datos</param>
        ''' <param name="ColCod">Columna por la q buscar</param>
        ''' <param name="Valor">Valor de codigo a buscar</param>
        ''' <param name="bTextoGuion">Si la denominaci�n es den � cod - den</param>
        ''' <param name="ColPais">Columna pais por la q buscar</param>
        ''' <param name="ValorPais">Valor de pais a buscar</param>
        ''' <returns>la denominaci�n del combo</returns>
        ''' <remarks>Llamada desde: Page_load; Tiempo m�ximo:0</remarks>
        Private Function TextoDelDropDown(ByVal Lista As DataSet, ByVal ColCod As String, ByVal Valor As String, ByVal bTextoGuion As Boolean,
                Optional ByVal ColPais As String = "", Optional ByVal ValorPais As String = Nothing, Optional ByVal bPoblacion As Boolean = False) As String
            Dim oRowLista() As DataRow
            Dim Sql As String = ColCod & "=" & StrToSQLNULL(Valor)

            If ColPais <> "" Then
                Sql = ColPais & "=" & StrToSQLNULL(ValorPais) & " AND " & Sql
            End If

            If Lista IsNot Nothing Then
                oRowLista = Lista.Tables(0).Select(Sql)
                If oRowLista.Length > 0 Then
                    If bTextoGuion Then
                        If bPoblacion Then
                            Return oRowLista(0).Item("COD") & " - " & oRowLista(0).Item("DEN") & If(IsDBNull(oRowLista(0).Item("POB")), "", " (" & oRowLista(0).Item("POB") & ")")
                        Else
                            Return oRowLista(0).Item("COD") & " - " & oRowLista(0).Item("DEN")
                        End If
                    Else
                        Return oRowLista(0).Item("DEN")
                    End If
                Else
                    Return String.Empty
                End If
            Else
                'Ejemplo: Hay departamento pero no unidad organizativa???
                Return ""
            End If
        End Function
        ''' Revisado por: blp. Fecha: 10/10/2011
        ''' <summary>
        ''' Carga los datos por defecto de los combos relacionados para usarlos en las lineas a�adidas.
        ''' Por javascript se hace el create dropdown para hacerlo se necesita los posibles valores y los valores se obtienen una unica vez
        ''' y los mete en oFsEntry.Lista. 
        ''' As� q si, por ejemplo pais-provincia, en el oFsEntry.Lista de la provincia se cargan todas, las lineas a�adidas tendran todas
        ''' pero si en el oFsEntry.Lista se cargan las de el pais X, las lineas a�adidas seran las del pais X y si por defecto tienes marcado
        ''' pais X esos son los datos q debe tener el webdropdown.
        ''' </summary>
        ''' <param name="Datos">Datos del desglose leido de bbdd. Antes q los popup se actualizen.</param>
        ''' <param name="oCampo">objeto desglose</param> 
        ''' <param name="bQA">si es qa o no</param> 
        ''' <param name="sCodPaisDefecto">Pais por defecto en lineas a�adidas</param>
        ''' <param name="sCodOrgCompraDefecto">Organizaci�n de Compras por defecto en lineas a�adidas</param>
        ''' <param name="sCodCentroDefecto">Centr por defecto en lineas a�adidas</param>
        ''' <param name="sCodUnidadOrgDefecto">Unidad Organizativa por defecto en lineas a�adidas</param>
        ''' <remarks>Llamada desde: Page_Load; Tiempo maximo: 0</remarks>
        Private Sub CtrlHayDatosPorDefecto(ByVal Datos As DataSet, ByVal oCampo As PMPortalServer.Campo, ByVal bQA As Boolean _
                                             , ByRef sCodPaisDefecto As String, ByRef sCodOrgCompraDefecto As String, ByRef sCodCentroDefecto As String, ByRef sCodUnidadOrgDefecto As String,
                                             Optional ByVal sProve As String = Nothing, Optional ByVal idUsu As Integer = Nothing)

            Dim DeDonde As Integer = 5
            Dim Lineas As Integer

            Try
                Lineas = Datos.Tables(DeDonde).Rows.Count
            Catch ex As Exception
                DeDonde = 2
                Dim sIdi As String = Session("FS_Portal_User").Idioma.ToString
                'Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
                Dim lCiaComp As Long = Session("FS_Portal_User").CiaComp

                If bQA Then
                    Datos = oCampo.LoadDesglose(lCiaComp, sIdi, oCampo.IdSolicitud)
                Else
                    Datos = oCampo.LoadDesglose(lCiaComp, sIdi, oCampo.IdSolicitud, True, sProve, idUsu)
                End If
            End Try

            sCodPaisDefecto = ""
            sCodOrgCompraDefecto = ""
            sCodCentroDefecto = ""
            sCodUnidadOrgDefecto = ""

            For Each oRow In Datos.Tables(DeDonde).Rows
                If DBNullToSomething(oRow.Item("LINEA")) = 1 Then
                    If Not IsDBNull(oRow.Item("VALOR_TEXT")) Then
                        Select Case DBNullToInteger(oRow.Item("TIPO_CAMPO_GS"))
                            Case TiposDeDatos.TipoCampoGS.Pais
                                sCodPaisDefecto = oRow.Item("VALOR_TEXT")
                            Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
                                sCodOrgCompraDefecto = oRow.Item("VALOR_TEXT")
                            Case TiposDeDatos.TipoCampoGS.Centro
                                sCodCentroDefecto = oRow.Item("VALOR_TEXT")
                            Case TiposDeDatos.TipoCampoGS.UnidadOrganizativa
                                sCodUnidadOrgDefecto = oRow.Item("VALOR_TEXT")
                        End Select
                    End If
                Else
                    Exit For
                End If
            Next
        End Sub
        ''' Revisado por: blp. Fecha: 15/11/2012
        ''' <summary>
        ''' Borra el contenido de la lista de los controles que se cargan desde servidor, porque la lista la han recibido �nicamente para recuperar el DEN del valor que tiene o el campo por defecto o el campo del desglose
        ''' </summary>
        ''' <param name="oDSCampos">Dataset con los campos</param>
        ''' <param name="iLineas"></param>
        ''' <remarks>
        ''' Llamada desde Load para borrar las listas de los controles que aparecen en el desglose
        ''' Llamada desde CargarValoresDefecto para borrar las listas de los controles que guardan los valores por defecto (y que est�n ocultos, se usan al a�adir l�nea nueva)
        ''' M�ximo: 0,1 seg.
        ''' </remarks>
        Private Sub limpiarListadeCamposConCargaEnCliente(ByRef oDSCampos As DataSet, ByVal iLineas As Integer)

            'En el load hemos recuperado datos para las listas de provincia, departamento, centro y almac�n
            'Son campos dependientes de otros campos, cuyos datos se cargan en los webdropdown desde ajax pero necesit�bamos esa carga para
            'poder coger el valor por defecto en esta funci�n (si es que hay alguna l�nea por defecto). 
            'Es necesario vaciar posteriormente las listas porque la versi�n 11.1 del webdropdown carga mal los datos desde ajax si primero se cargan desde servidor
            'Dado que no podemos estar seguros de que se hayan vaciado, lo comprobamos de nuevo aqu�
            If oDSCampos.Tables(0) IsNot Nothing _
            AndAlso oDSCampos.Tables(0).Columns("ID") IsNot Nothing _
            AndAlso oDSCampos.Tables(0).Columns("TIPO_CAMPO_GS") IsNot Nothing Then
                For Each oFila As DataRow In oDSCampos.Tables(0).Rows
                    Dim oFSDSEntry As New Fullstep.DataEntry.GeneralEntry
                    Dim Lineas As Integer = 0 'Para que siempre entre al menos una vez.
                    Do While Lineas <= iLineas
                        Try
                            If Lineas = 0 Then
                                oFSDSEntry = Me.tblDesglose.FindControl("fsentry" & oFila("ID"))
                            Else
                                oFSDSEntry = Me.tblDesglose.FindControl("fsdsentry_" & Lineas.ToString & "_" & oFila("ID"))
                            End If

                        Catch
                            oFSDSEntry = Nothing
                        End Try

                        If Not oFSDSEntry Is Nothing Then
                            Select Case null2str(oFila("TIPO_CAMPO_GS"))
                                Case TiposDeDatos.TipoCampoGS.Pais, TiposDeDatos.TipoCampoGS.Provincia,
                                        TiposDeDatos.TipoCampoGS.Departamento,
                                        TiposDeDatos.TipoCampoGS.Centro,
                                        TiposDeDatos.TipoCampoGS.Almacen,
                                        TiposDeDatos.TipoCampoGS.Moneda,
                                        TiposDeDatos.TipoCampoGS.FormaPago,
                                        TiposDeDatos.TipoCampoGS.Unidad,
                                        TiposDeDatos.TipoCampoGS.OrganizacionCompras,
                                        TiposDeDatos.TipoCampoGS.Empresa
                                    If oFSDSEntry.Lista IsNot Nothing AndAlso oFSDSEntry.MasterField <> String.Empty Then
                                        oFSDSEntry.Lista = Nothing
                                    End If
                            End Select
                        End If
                        Lineas += 1
                    Loop
                Next
            End If
        End Sub
    End Class
End Namespace