﻿Namespace Fullstep.PMPortalWeb
    Public Class cabecera_master
        Inherits FSPMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.Menu
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutanormal") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutanormal", "<script>var rutanormal = '" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "'</script>")
            End If
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "segundosNotificacionesCN") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "segundosnNotificacionesCN", _
                    "var segundosNotificacionesCN=" & CType(ConfigurationManager.AppSettings("segundosNotificacionesCN"), Integer) * 1000 & ";", True)
            End If
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosTiempoRelativo") Then
                Dim sVariableJavascriptTextosTiempoRelativo As String = "var TextosTiempoRelativo = new Array();"
                sVariableJavascriptTextosTiempoRelativo &= "TextosTiempoRelativo[0]='" & JSText(Textos(1)) & "';"
                sVariableJavascriptTextosTiempoRelativo &= "TextosTiempoRelativo[1]='" & JSText(Textos(2)) & "';"
                sVariableJavascriptTextosTiempoRelativo &= "var unidadesTiempo = {" & _
                "1:'" & JSText(Textos(3)) & "'" & ",2:'" & JSText(Textos(4)) & "'" & ",3:'" & JSText(Textos(5)) & "'" & _
                ",4:'" & JSText(Textos(6)) & "'" & ",5:'" & JSText(Textos(7)) & "'" & ",6:'" & JSText(Textos(8)) & "'" & _
                ",11:'" & JSText(Textos(9)) & "'" & ",12:'" & JSText(Textos(10)) & "'" & ",13:'" & JSText(Textos(11)) & "'" & _
                ",14:'" & JSText(Textos(12)) & "'" & ",15:'" & JSText(Textos(13)) & "'" & ",16:'" & JSText(Textos(14)) & "'" & "};"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosTiempoRelativo", sVariableJavascriptTextosTiempoRelativo, True)
            End If

            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Colaboracion
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
                Dim sVariableJavascriptTextos As String = "var Textos = new Array();"
                Dim sVariableJavascriptTextosCKEditor As String = "var TextosCKEditor = new Array();"

                For i As Integer = 1 To 73
                    sVariableJavascriptTextos &= "Textos[" & i & "]='" & JSText(Textos(i)) & "';"
                    If i = 20 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[1]='" & JSText(Textos(i)) & "';"
                    If i = 21 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[2]='" & JSText(Textos(i)) & "';"
                Next

                For i As Integer = 83 To 85
                    sVariableJavascriptTextos &= "Textos[" & i - 9 & "]='" & JSText(Textos(i)) & "';"
                Next

                For i As Integer = 74 To 89
                    sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 71 & "]='" & JSText(Textos(i)) & "';"
                Next
                For i As Integer = 90 To 121

                    sVariableJavascriptTextos &= "Textos[" & i - 13 & "]='" & JSText(Textos(i)) & "';"
                Next
                For i As Integer = 142 To 143
                    sVariableJavascriptTextos &= "Textos[" & i - 33 & "]='" & JSText(Textos(i)) & "';"
                Next
                For i As Integer = 144 To 147
                    sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 125 & "]='" & JSText(Textos(i)) & "';"
                Next
                For i As Integer = 148 To 151
                    sVariableJavascriptTextos &= "Textos[" & i - 37 & "]='" & JSText(Textos(i)) & "';"
                Next
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextos & sVariableJavascriptTextosCKEditor, True)
            End If

            ScriptManager1.Services.Add(New ServiceReference(ConfigurationManager.AppSettings("rutanormal") & "script/facturas/services/Facturas.asmx"))
            Me.ScriptManager1.EnablePageMethods = True
        End Sub
    End Class
End Namespace