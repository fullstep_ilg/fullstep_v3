Namespace Fullstep.PMPortalWeb
    Partial Class pres
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        Private msValor As String
        Private mlContadorNodes As Long = 0
        Private mDTPresup As DataTable


        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Me.Page.ClientScript.RegisterClientScriptResource(GetType(Fullstep.DataEntry.GeneralEntry), "Fullstep.DataEntry.formatos.js")
            'Put user code to initialize the page here
            If Request("UON1") = Nothing Then
                Exit Sub
            End If

            mlContadorNodes = 0
            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Presupuestos

            Me.lblAsignar.Text = Textos(15)
            Me.lblObjetivo.Text = Textos(14)
            Me.lblPresupuesto.Text = Textos(13)
            Me.cmdOtras.Attributes.Add("title", Textos(16))

            Dim iAnyo As Object
            iAnyo = Request("ugtxtAnyo")
            If iAnyo <> Nothing Then
                iAnyo = Numero(iAnyo, ".", ",")
            Else
                iAnyo = Nothing
            End If

            Dim bConCantidad As Boolean

            Dim dCantidad As Decimal

            bConCantidad = (Request("CANTIDAD") <> Nothing)

            If bConCantidad Then
                dCantidad = Numero(Request("CANTIDAD"), ".", ",")
                If dCantidad = 0 Then
                    bConCantidad = False
                End If
            End If

            If bConCantidad Then
                Me.txtCantidad.Visible = True
            Else
                Me.txtCantidad.Visible = False
            End If
            Dim iTipo As Integer = Request("TIPO")
            Me.TIPO.Value = iTipo
            Dim sUon1 As String
            Dim sUon2 As String
            Dim sUon3 As String
            If Page.IsPostBack Then
                sUon1 = Request.Form("UON1")
                sUon2 = Request.Form("UON2")
                sUon3 = Request.Form("UON3")
            Else
                sUon1 = Request.QueryString("UON1")
                sUon2 = Request.QueryString("UON2")
                sUon3 = Request.QueryString("UON3")
            End If

            Me.UON1.Value = sUon1

            Me.UON2.Value = sUon2

            Me.UON3.Value = sUon3

            Dim sDen As String = Request("DENUON")
            Me.DENUON.Value = sDen
            Me.lblUon.Text = sDen
            Me.txtObjetivo.Value = Nothing
            Me.txtObjetivo.Text = Nothing
            Me.txtPresupuesto.Value = Nothing
            Me.txtPresupuesto.Text = Nothing

            msValor = Request("PRES")

            Dim arrPresupuestos() As String = msValor.Split("#")
            Dim arrPresup As String
            Dim arrPresupuesto() As String
            Dim iNivel As Integer
            Dim lIdPresup As Integer
            Dim dPorcent As Double

            mDTPresup = New DataTable("PRESUP")
            Dim oRow As DataRow

            mDTPresup.Columns.Add("PRESUP", System.Type.GetType("System.Int32"))
            mDTPresup.Columns.Add("NIVEL", System.Type.GetType("System.Int32"))
            mDTPresup.Columns.Add("PORCENT", System.Type.GetType("System.Decimal"))

            If msValor <> Nothing Then
                For Each arrPresup In arrPresupuestos
                    arrPresupuesto = arrPresup.Split("_")

                    iNivel = arrPresupuesto(0)
                    lIdPresup = arrPresupuesto(1)
                    dPorcent = Numero(arrPresupuesto(2), ".", ",")
                    oRow = mDTPresup.NewRow
                    oRow.Item("PRESUP") = lIdPresup
                    oRow.Item("NIVEL") = iNivel
                    oRow.Item("PORCENT") = dPorcent
                    mDTPresup.Rows.Add(oRow)



                Next


            End If

            CargarPresupuestos(iTipo, iAnyo, sUon1, sUon2, sUon3, Idioma, Page.IsPostBack)
        End Sub
        Private Sub ugtxtAnyo_ValueChange(ByVal sender As System.Object, ByVal e As Infragistics.WebUI.WebDataInput.ValueChangeEventArgs) Handles ugtxtAnyo.ValueChange

        End Sub

        ''' <summary>
        ''' Cargar los Presupuestos
        ''' </summary>
        ''' <param name="iTipo">Tipo de presupuesto</param>
        ''' <param name="iAnyo">A�o de presupuesto</param>
        ''' <param name="sUon1">Unidad organizativa de nivel 1</param>
        ''' <param name="sUon2">Unidad organizativa de nivel 2</param>
        ''' <param name="sUon3">Unidad organizativa de nivel 3</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="bIsPostback">Page.IsPostBack</param>
        ''' <remarks>Llamada desde: Page_Load ; Tiempo m�ximo: 0,2</remarks>
        Private Sub CargarPresupuestos(ByVal iTipo As Integer, ByVal iAnyo As Integer, ByVal sUon1 As String, ByVal sUon2 As String, ByVal sUon3 As String, ByVal sIdi As String, ByVal bIsPostback As Boolean)
            Dim lCiaComp As Long = IdCiaComp
            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUnidades As Fullstep.PMPortalServer.UnidadesOrg
            oUnidades = FSWSServer.get_UnidadesOrg
            oUnidades.CargarPresupuestos(lCiaComp, iTipo, iAnyo, sUon1, sUon2, sUon3, sIdi)


            If Not bIsPostback Then
                Dim sDen As String
                If oUnidades.Data.Tables(1).Rows.Count = 0 Then
                    Me.UON3.Value = Nothing
                    sUon3 = Nothing
                    oUnidades.CargarPresupuestos(lCiaComp, iTipo, iAnyo, sUon1, sUon2, sUon3, sIdi)
                    If oUnidades.Data.Tables(1).Rows.Count = 0 Then
                        sUon2 = Nothing
                        Me.UON2.Value = Nothing
                        oUnidades.CargarPresupuestos(lCiaComp, iTipo, iAnyo, sUon1, sUon2, sUon3, sIdi)

                    End If

                End If


            End If

            Try
                iAnyo = oUnidades.Data.Tables(1).Rows(0).Item("ANYO")

            Catch ex As Exception

            End Try

            Me.ugtxtAnyo.Value = iAnyo
            If iAnyo = Nothing Then
                Me.ugtxtAnyo.Style.Add("visibility", "hidden")
            End If
            Me.uwtPresupuestos.ClearAll()

            Me.uwtPresupuestos.DataSource = oUnidades.Data.Tables(0).DefaultView

            Me.uwtPresupuestos.Levels(0).RelationName = "REL_NIV0_NIV1"
            Me.uwtPresupuestos.Levels(0).ColumnName = "CDEN"
            Me.uwtPresupuestos.Levels(0).LevelImage = "images/RaizPresCon.gif"
            Me.uwtPresupuestos.Levels(1).RelationName = "REL_NIV1_NIV2"
            Me.uwtPresupuestos.Levels(1).ColumnName = "CDEN"
            Me.uwtPresupuestos.Levels(1).LevelKeyField = "IDCOD"

            Me.uwtPresupuestos.Levels(1).LevelImage = "images/SINEWAVE.gif"
            Me.uwtPresupuestos.Levels(2).RelationName = "REL_NIV2_NIV3"
            Me.uwtPresupuestos.Levels(2).ColumnName = "CDEN"
            Me.uwtPresupuestos.Levels(2).LevelKeyField = "IDCOD"
            Me.uwtPresupuestos.Levels(2).LevelImage = "images/SINEWAVE.gif"
            Me.uwtPresupuestos.Levels(3).RelationName = "REL_NIV3_NIV4"
            Me.uwtPresupuestos.Levels(3).ColumnName = "CDEN"
            Me.uwtPresupuestos.Levels(3).LevelKeyField = "IDCOD"
            Me.uwtPresupuestos.Levels(3).LevelImage = "images/SINEWAVE.gif"
            Me.uwtPresupuestos.Levels(4).ColumnName = "CDEN"
            Me.uwtPresupuestos.Levels(4).LevelKeyField = "IDCOD"
            Me.uwtPresupuestos.Levels(4).LevelImage = "images/SINEWAVE.gif"
            Me.uwtPresupuestos.DataKeyOnClient = True
            Me.uwtPresupuestos.DataBind()
        End Sub

        Private Sub uwtPresupuestos_NodeBound(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebNavigator.WebTreeNodeEventArgs) Handles uwtPresupuestos.NodeBound
            Dim oNode As Infragistics.WebUI.UltraWebNavigator.Node
            Dim ds As DataSet
            Dim oRows() As DataRow
            Dim oRowsPres() As DataRow
            Dim sScript As String

            oNode = e.Node

            If oNode.Level = 0 Then
                oNode.Expanded = True
                Exit Sub
            End If
            ds = sender.datasource.dataviewmanager.dataset
            oRows = ds.Tables(oNode.Level).Select("IDCOD= '" & oNode.DataKey & "'")
            sScript = ""
            If oRows.Length > 0 Then
                sScript = "arrNodes[arrNodes.length] = new Array('" + oNode.DataKey + "'," + JSNum(oRows(0).Item("IMP")) + "," + JSNum(oRows(0).Item("OBJ")) + ")"
            End If

            oRowsPres = mDTPresup.Select("PRESUP= " & oNode.DataKey.SPLIT("_")(0) & " AND NIVEL = " & oNode.Level)
            If oRowsPres.Length > 0 Then
                sScript += ";enlazarRamaADistribucion(" + JSNum(oRowsPres(0).Item("PRESUP")) + "," + JSNum(oRowsPres(0).Item("NIVEL")) + ",'uwtPresupuestos" + oNode.GetIdString() + "'," + JSNum(mlContadorNodes) + ")"
                While Not oNode.Parent.Parent Is Nothing
                    oNode = oNode.Parent
                    oNode.Expanded = True
                End While
                oNode = e.Node

            End If

            If sScript <> Nothing Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "node" + oNode.DataKey.ToString(), "<script>" + sScript + "</script>")
            End If

            oNode.DataKey = mlContadorNodes.ToString + "__" + oNode.DataKey
            mlContadorNodes += 1
        End Sub
    End Class
End Namespace