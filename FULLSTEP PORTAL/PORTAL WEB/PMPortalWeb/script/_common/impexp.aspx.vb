Imports System.IO
Imports System.Xml
Imports System.Xml.Xsl

Namespace Fullstep.PMPortalWeb

	Partial Class impexp
		Inherits FSPMPage

		Private oInstancia As Fullstep.PMPortalServer.Instancia
		Private oCertificado As Fullstep.PMPortalServer.Certificado
		Private oNoConformidad As Fullstep.PMPortalServer.NoConformidad
		Private oUser As Fullstep.PMPortalServer.User
		Private FSWSServer As Fullstep.PMPortalServer.Root

#Region " Web Form Designer Generated Code "
		'This call is required by the Web Form Designer.
		<System.Diagnostics.DebuggerStepThrough()>
		Private Sub InitializeComponent()
		End Sub

		'NOTE: The following placeholder declaration is required by the Web Form Designer.
		'Do not delete or move it.
		Private designerPlaceholderDeclaration As System.Object

		Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
			'CODEGEN: This method call is required by the Web Form Designer
			'Do not modify it using the code editor.
			InitializeComponent()
		End Sub
#End Region

		''' <summary>
		''' Pantalla de generaci�n del archivo del tipo seleccionado para exportar (PDF/HTML/XML)
		''' </summary>
		''' <param name="sender"> Propio del evento </param>
		''' <param name="e"> Propio del evento </param>
		''' <remarks>Informaci�n sobre la Noconformidad/Certificado en el formato seleccionado, que va a ser descargada en la pagina download.aspx
		''' Llamada desde impexp_sel.aspx; Tiempo m�ximo = 0.9062 seg (dependiendo del volumen de datos)</remarks>
		Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
			'Objetos para montar el documento XML
			Dim oXmlDoc As XmlDocument
			Dim oXmlEmt, oXMLDatoElemento, oXMLGrupos, oXMLGrupo, oXMLCampos, oXMLCampo, oXMLValorCampo, oXMLAcciones As XmlElement
			Dim oXMLAccion, oXMLDesglose, oXMLFilaDesglose, oXMLColumnaDesglose, oXMLValorColumnaDesglose, oXMLDatoElementoDesglose As XmlElement
			Dim oXMLDatoTexto, oXMLDatoTextoDesglose As XmlText
			oXMLDatoTexto = Nothing
			Dim sDato, sDenDato As String 'Para la denominaci�n en los casos de tipo GS.
			'Objetos para obtener la informaci�n del documento
			Dim iVersion As Long
			Dim oGrupo As Fullstep.PMPortalServer.Grupo
			Dim oCampo As Fullstep.PMPortalServer.Campo
			Dim oFilaCampo, oFilaDesglose, oAdjunto, oRow As DataRow
			Dim oDesglose, oDSLista As DataSet
			Dim sAdjun, lEstado As String
			Dim iColumna, iFila, iNumColumnas, iNumFilas As Long
			Dim iCampoColumna() As Long ''?
			Dim iTipoColumna() As Integer
			Dim iTipoGSColumna() As Integer
			Dim sDenColumna() As String
			Dim bVisibleColumna() As Boolean
			Dim bIntroColumna() As Boolean
			Dim lTablaExterna() As Long
			Dim bGrupoVisible, bCampoVisible As Boolean
			Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
			Dim lCiaComp As Long = IdCiaComp

			'Inicializar variables
			lEstado = String.Empty
			oXMLValorCampo = Nothing

			'Idioma
			oUser = Session("FS_Portal_User")
			Dim sIdi As String = oUser.Idioma
			If sIdi = Nothing Then sIdi = ConfigurationManager.AppSettings("idioma")

			'Textos
			Dim oDict As Fullstep.PMPortalServer.Dictionary = FSWSServer.Get_Dictionary()
			oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.ImpExp, sIdi)
			Dim oTextos As DataTable = oDict.Data.Tables(0)
			Dim bObservador As Boolean = (Request("Observadores") = "1")

			oInstancia = FSWSServer.Get_Instancia

			If Request("TipoImpExp") = "1" Then
				oInstancia.ID = Request("Instancia")
			ElseIf Request("TipoImpExp") = "2" Then
				oCertificado = FSWSServer.Get_Certificado
				oCertificado.Id = Request("Certificado")
				oCertificado.Prove = oUser.CodProve
				oCertificado.Load(lCiaComp, sIdi)

				If Request("Version") <> Nothing Then
					iVersion = Request("Version")
					If iVersion <> Nothing Then
						oCertificado.Version = iVersion
						oInstancia.ID = Request("Instancia")
					End If
				Else
					oInstancia.ID = oCertificado.Instancia
				End If

				oInstancia.Version = oCertificado.Version

				If Not CtrlVulnerabilidad(lCiaComp, oCertificado.Id, oUser.CodProve, Request("Instancia"), True) Then Exit Sub
			ElseIf Request("TipoImpExp") = "3" Then
				oNoConformidad = FSWSServer.Get_NoConformidad
				oNoConformidad.Id = Request("NoConformidad")
				oNoConformidad.Prove = oUser.CodProve
				oNoConformidad.Load(lCiaComp, sIdi, True)

				oInstancia.ID = oNoConformidad.Instancia

				If Request("Version") <> Nothing Then
					iVersion = Request("Version")
					If iVersion <> Nothing Then
						oNoConformidad.Version = iVersion
					End If
				End If

				oInstancia.Version = oNoConformidad.Version

				If Not CtrlVulnerabilidad(lCiaComp, oNoConformidad.Id, oUser.CodProve, Request("Instancia"), False) Then Exit Sub
			End If

			'Cargamos datos de instancia
			oInstancia.Load(IdCiaComp, sIdi, True)
			oInstancia.Solicitud.Load(IdCiaComp, sIdi)

			If oInstancia.EnProceso Then
				Page.ClientScript.RegisterStartupScript(Me.GetType(), "NoImp", "<script>alert('" & oTextos.Rows(0).Item(1) & "');window.close();window.parent.close();</script>")
				Exit Sub
			End If

			oUser = Session("FS_Portal_User")
			oInstancia.CargarCamposInstancia(lCiaComp, oUser.IdCia, sIdi, oInstancia.AprobadorActual, oUser.CodProveGS, False, Not {"2", "3"}.Contains(Request("TipoImpExp")))

			oDSLista = oInstancia.CargarTiposEstadoAccion(lCiaComp)

			'Documento XML con los datos de la instancia
			oXmlDoc = New XmlDocument
			oXmlEmt = oXmlDoc.CreateElement("DOCUMENTO")
			oXmlDoc.AppendChild(oXmlEmt)

			oXMLDatoElemento = oXmlDoc.CreateElement("TIPO")
			oXmlEmt.AppendChild(oXMLDatoElemento)
			If Request("TipoImpExp") = "1" Then
				oXMLDatoTexto = oXmlDoc.CreateTextNode(oInstancia.Solicitud.Den(sIdi))
			ElseIf Request("TipoImpExp") = "2" Then
				oXMLDatoTexto = oXmlDoc.CreateTextNode(oCertificado.TipoDen)
			ElseIf Request("TipoImpExp") = "3" Then
				oXMLDatoTexto = oXmlDoc.CreateTextNode(oNoConformidad.TipoDen)
			End If
			oXMLDatoElemento.AppendChild(oXMLDatoTexto)
			oXMLDatoElemento = Nothing
			oXMLDatoTexto = Nothing

			oXMLDatoElemento = oXmlDoc.CreateElement("ID")
			oXmlEmt.AppendChild(oXMLDatoElemento)
			oXMLDatoTexto = oXmlDoc.CreateTextNode(oInstancia.ID)
			oXMLDatoElemento.AppendChild(oXMLDatoTexto)
			oXMLDatoElemento = Nothing
			oXMLDatoTexto = Nothing

			oXMLDatoElemento = oXmlDoc.CreateElement("PETICIONARIO")
			oXmlEmt.AppendChild(oXMLDatoElemento)
			If Request("TipoImpExp") = "1" Then
				oXMLDatoTexto = oXmlDoc.CreateTextNode(oInstancia.NombrePeticionario)
			ElseIf Request("TipoImpExp") = "2" Then
				oXMLDatoTexto = oXmlDoc.CreateTextNode(oCertificado.Peticionario)
			ElseIf Request("TipoImpExp") = "3" Then
				oXMLDatoTexto = oXmlDoc.CreateTextNode(oNoConformidad.Peticionario)
			End If
			oXMLDatoElemento.AppendChild(oXMLDatoTexto)
			oXMLDatoElemento = Nothing
			oXMLDatoTexto = Nothing

			oXMLDatoElemento = oXmlDoc.CreateElement("FECHA")
			oXmlEmt.AppendChild(oXMLDatoElemento)
			If Request("TipoImpExp") = "1" Then
				oXMLDatoTexto = oXmlDoc.CreateTextNode(FormatDateTime(oInstancia.FechaAlta, DateFormat.ShortDate))
			ElseIf Request("TipoImpExp") = "2" Then
				oXMLDatoTexto = oXmlDoc.CreateTextNode(FormatDateTime(oCertificado.FecPub, DateFormat.ShortDate))
			ElseIf Request("TipoImpExp") = "3" Then
				oXMLDatoTexto = oXmlDoc.CreateTextNode(FormatDateTime(oInstancia.FechaAlta, DateFormat.ShortDate))
			End If
			oXMLDatoElemento.AppendChild(oXMLDatoTexto)
			oXMLDatoElemento = Nothing
			oXMLDatoTexto = Nothing

			'Estado
			If Request("TipoImpExp") = "3" Then
				Select Case oNoConformidad.Estado
					Case 0 'Guardada
						lEstado = oTextos.Rows(1).Item(1)
					Case 1 'Abierta
						lEstado = oTextos.Rows(2).Item(1)
					Case 2 'Cierre eficaz dentro de plazo
						lEstado = oTextos.Rows(3).Item(1)
					Case 3 'Cierre eficaz fuera de plazo
						lEstado = oTextos.Rows(4).Item(1)
					Case 4 'Cierre no eficaz
						lEstado = oTextos.Rows(5).Item(1)
					Case 5, 6 'Pendiente de revisar cierre
						lEstado = oTextos.Rows(6).Item(1)
				End Select
			Else
				Select Case oInstancia.Estado
					Case 0
						lEstado = oTextos.Rows(1).Item("TEXT_" & sIdi)
					Case 2, 100
						lEstado = oTextos.Rows(7).Item("TEXT_" & sIdi)
					Case 7, 101
						lEstado = oTextos.Rows(8).Item("TEXT_" & sIdi)
					Case 6, 102
						lEstado = oTextos.Rows(9).Item("TEXT_" & sIdi)
					Case 8, 103
						lEstado = oTextos.Rows(10).Item("TEXT_" & sIdi)
					Case 104
						lEstado = oTextos.Rows(11).Item("TEXT_" & sIdi)
					Case Else
						lEstado = oTextos.Rows(12).Item("TEXT_" & sIdi)
				End Select
			End If

			oXMLDatoElemento = oXmlDoc.CreateElement("ESTADO")
			oXmlEmt.AppendChild(oXMLDatoElemento)
			oXMLDatoTexto = oXmlDoc.CreateTextNode(lEstado)
			oXMLDatoElemento.AppendChild(oXMLDatoTexto)
			oXMLDatoElemento = Nothing
			oXMLDatoTexto = Nothing

			'Aprobador actual
			If Not oInstancia.AprobadorActual Is Nothing Then
				oXMLDatoElemento = oXmlDoc.CreateElement("APROBADOR_ACTUAL")
				oXmlEmt.AppendChild(oXMLDatoElemento)
				oXMLDatoTexto = oXmlDoc.CreateTextNode(oInstancia.NombreAprobadorActual)
				oXMLDatoElemento.AppendChild(oXMLDatoTexto)
				oXMLDatoElemento = Nothing
				oXMLDatoTexto = Nothing
			End If
			'Importe
			If Request("TipoImpExp") = "1" Then
				If IsNumeric(oInstancia.Importe) Then
					oXMLDatoElemento = oXmlDoc.CreateElement("IMPORTE")
					oXmlEmt.AppendChild(oXMLDatoElemento)
					oXMLDatoTexto = oXmlDoc.CreateTextNode(FSNLibrary.FormatNumber(oInstancia.Importe, oUser.NumberFormat))
					oXMLDatoElemento.AppendChild(oXMLDatoTexto)
					oXMLDatoElemento = Nothing
					oXMLDatoTexto = Nothing
				End If
			End If
			'Si es una no conformidad muestra tb la fecha l�mite de resoluci�n,unida de negocio, proveedor, contacto, comentario de alta y comentario de cierre
			If Request("TipoImpExp") = "3" Then
				oXMLDatoElemento = oXmlDoc.CreateElement("FEC_LIM")
				oXmlEmt.AppendChild(oXMLDatoElemento)
				oXMLDatoTexto = oXmlDoc.CreateTextNode(FormatDateTime(oNoConformidad.FecLimCumplimentacion, DateFormat.ShortDate))
				oXMLDatoElemento.AppendChild(oXMLDatoTexto)
				oXMLDatoElemento = Nothing
				oXMLDatoTexto = Nothing

				oXMLDatoElemento = oXmlDoc.CreateElement("UNQA")
				oXmlEmt.AppendChild(oXMLDatoElemento)
				oXMLDatoTexto = oXmlDoc.CreateTextNode(oNoConformidad.UnidadQADen)
				oXMLDatoElemento.AppendChild(oXMLDatoTexto)
				oXMLDatoElemento = Nothing
				oXMLDatoTexto = Nothing

				oXMLDatoElemento = oXmlDoc.CreateElement("COMMENT_ALTA")
				oXmlEmt.AppendChild(oXMLDatoElemento)
				If Request("Formato") = "XML" Then
					oXMLDatoTexto = oXmlDoc.CreateTextNode(oNoConformidad.ComentAlta)
					oXMLDatoElemento.AppendChild(oXMLDatoTexto)
				Else
					oXMLDatoElemento.AppendChild(oXmlDoc.CreateCDataSection(oNoConformidad.ComentAltaBR))
				End If

				oXMLDatoElemento = Nothing
				oXMLDatoTexto = Nothing

				oXMLDatoElemento = oXmlDoc.CreateElement("COMMENT_CIERRE")
				oXmlEmt.AppendChild(oXMLDatoElemento)

				'En el mail q se manda a Notificados de Proveedor: Va este comentario luego, aqui debe estar
				oNoConformidad.CargarComentarioCierre(lCiaComp)

				If Request("Formato") = "XML" Then
					oXMLDatoTexto = oXmlDoc.CreateTextNode(oNoConformidad.ComentCierre)
					oXMLDatoElemento.AppendChild(oXMLDatoTexto)
				Else
					oXMLDatoElemento.AppendChild(oXmlDoc.CreateCDataSection(oNoConformidad.ComentCierreBR))
				End If

				oXMLDatoElemento = Nothing
				oXMLDatoTexto = Nothing

				'Genera la tabla de acciones:
				If oNoConformidad.Acciones.Rows.Count > 0 Then
					oXMLAcciones = oXmlDoc.CreateElement("ACCIONES")
					oXmlEmt.AppendChild(oXMLAcciones)

					For Each oRow In oNoConformidad.Acciones.Rows
						If oRow.Item("SOLICITAR") = 1 Then
							oXMLAccion = oXmlDoc.CreateElement("ACCION")
							oXMLAcciones.AppendChild(oXMLAccion)

							oXMLDatoElemento = oXmlDoc.CreateElement("NOM_ACCION")
							oXMLAccion.AppendChild(oXMLDatoElemento)
							oXMLDatoTexto = oXmlDoc.CreateTextNode(oRow.Item("DEN_ACCION"))
							oXMLDatoElemento.AppendChild(oXMLDatoTexto)
							oXMLDatoElemento = Nothing
							oXMLDatoTexto = Nothing

							oXMLDatoElemento = oXmlDoc.CreateElement("FECLIM_ACCION")
							oXMLAccion.AppendChild(oXMLDatoElemento)
							If IsDBNull(oRow.Item("FECHA_LIMITE")) Then
								oXMLDatoTexto = oXmlDoc.CreateTextNode(oTextos.Rows(13).Item(1))
							Else
								oXMLDatoTexto = oXmlDoc.CreateTextNode(oTextos.Rows(13).Item(1) & " " & FormatDateTime(oRow.Item("FECHA_LIMITE"), DateFormat.ShortDate))
							End If
							oXMLDatoElemento.AppendChild(oXMLDatoTexto)
							oXMLDatoElemento = Nothing
							oXMLDatoTexto = Nothing
						End If
					Next
				End If
			End If

			'Grupos
			If Not oInstancia.Grupos.Grupos Is Nothing Then

				oXMLGrupos = oXmlDoc.CreateElement("GRUPOS")
				oXmlEmt.AppendChild(oXMLGrupos)

				'Por cada grupo vamos a recorrer sus campos
				For Each oGrupo In oInstancia.Grupos.Grupos
					'Si todos los campos del grupo no van a ser visibles, no se mostrar� el grupo.
					bGrupoVisible = False
					For Each oFilaCampo In oGrupo.DSCampos.Tables(0).Rows
						If oFilaCampo.Item("VISIBLE") = 1 Then
							If Request("TipoImpExp") = "3" And DBNullToSomething(oFilaCampo.Item("TIPO_CAMPO_GS")) = Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Acciones Then
								For Each oRow In oNoConformidad.Acciones.Rows
									If oRow.Item("COPIA_CAMPO") = oFilaCampo.Item("ID_CAMPO") Then
										If oRow.Item("SOLICITAR") = 1 Then
											bGrupoVisible = True
										End If
										Exit For
									End If
								Next
							Else
								bGrupoVisible = True
							End If

							If bGrupoVisible = True Then Exit For
						End If
					Next

					If bGrupoVisible = True Then 'Primero la cabecera del grupo
						oXMLGrupo = oXmlDoc.CreateElement("GRUPO")
						oXMLGrupos.AppendChild(oXMLGrupo)

						oXMLDatoElemento = oXmlDoc.CreateElement("NOMBRE")
						oXMLGrupo.AppendChild(oXMLDatoElemento)
						oXMLDatoTexto = oXmlDoc.CreateTextNode(oGrupo.Den(sIdi))
						oXMLDatoElemento.AppendChild(oXMLDatoTexto)
						oXMLDatoElemento = Nothing
						oXMLDatoTexto = Nothing

						If oGrupo.NumCampos >= 1 Then
							oXMLCampos = oXmlDoc.CreateElement("CAMPOS")
							oXMLGrupo.AppendChild(oXMLCampos)

							'Recorremos sus campos
							For Each oFilaCampo In oGrupo.DSCampos.Tables(0).Rows 'Cabecera del campo
								bCampoVisible = True
								If oFilaCampo.Item("VISIBLE") = 1 Then
									If Request("TipoImpExp") = "3" Then  'Si es una no conformidad y un campo de tipo acci�n comprueba que est� solicitado:
										If DBNullToSomething(oFilaCampo.Item("TIPO_CAMPO_GS")) = Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Acciones Then
											For Each oRow In oNoConformidad.Acciones.Rows
												If oRow.Item("COPIA_CAMPO") = oFilaCampo.Item("ID_CAMPO") Then
													If oRow.Item("SOLICITAR") = 0 Then
														bCampoVisible = False
													End If
													Exit For
												End If
											Next
										End If
									End If
								Else
									bCampoVisible = False
								End If

								If bCampoVisible = True Then
									oXMLCampo = oXmlDoc.CreateElement("CAMPO")
									oXMLCampos.AppendChild(oXMLCampo)

									oXMLDatoElemento = oXmlDoc.CreateElement("NOMBRE")
									oXMLCampo.AppendChild(oXMLDatoElemento)
									oXMLDatoTexto = oXmlDoc.CreateTextNode(oFilaCampo.Item("DEN_" & sIdi))
									oXMLDatoElemento.AppendChild(oXMLDatoTexto)

									oXMLDatoElemento = Nothing
									oXMLDatoTexto = Nothing

									'Si no es desglose, saldr� directamente el valor
									If oFilaCampo.Item("SUBTIPO") < 9 Then
										oXMLValorCampo = oXmlDoc.CreateElement("VALOR")
										oXMLCampo.AppendChild(oXMLValorCampo)
										oXMLDatoElemento = oXmlDoc.CreateElement("VALOR_DATO")
										oXMLValorCampo.AppendChild(oXMLDatoElemento)
									End If

									If oFilaCampo.Item("TIPO") = 6 Then
										Dim sCampoValor As String
										Select Case oFilaCampo.Item("SUBTIPO")
											Case 2
												sCampoValor = "VALOR_NUM"
											Case 3
												sCampoValor = "VALOR_FEC"
											Case 4
												sCampoValor = "VALOR_BOOL"
											Case Else
												sCampoValor = "VALOR_TEXT"
										End Select
										Dim oTablaExterna As PMPortalServer.TablaExterna = FSWSServer.get_TablaExterna
										oTablaExterna.LoadDefTabla(lCiaComp, oFilaCampo.Item("TABLA_EXTERNA"), False)
										oTablaExterna.LoadData(lCiaComp)
										If Not IsDBNull(oFilaCampo.Item(sCampoValor)) Then
											sDato = oTablaExterna.DescripcionReg(lCiaComp, oFilaCampo.Item(sCampoValor), sIdi, oUser.DateFormat, oUser.NumberFormat)
										Else
											sDato = ""
										End If
										oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
									Else
										Select Case oFilaCampo.Item("SUBTIPO")
											Case 0  'Sin tipo
												If Not (TypeOf (oFilaCampo.Item("VALOR_TEXT")) Is DBNull) Then
													sDato = oFilaCampo.Item("VALOR_TEXT")
												Else
													sDato = ""
												End If
												If Not TypeOf (oFilaCampo.Item("TIPO_CAMPO_GS")) Is DBNull Then
													If (CInt(oFilaCampo.Item("TIPO_CAMPO_GS") >= 100 And CInt(oFilaCampo.Item("TIPO_CAMPO_GS")) <= 105)) Or (CInt(oFilaCampo.Item("TIPO_CAMPO_GS")) >= 107 And CInt(oFilaCampo.Item("TIPO_CAMPO_GS")) <= 113) Then

														sDenDato = DenGS(sDato, oFilaCampo.Item("TIPO_CAMPO_GS"), sIdi)

														oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
														oXMLDatoElemento.AppendChild(oXMLDatoTexto)
														oXMLDatoElemento = Nothing
														oXMLDatoTexto = Nothing
														oXMLDatoElemento = oXmlDoc.CreateElement("VALOR_DEN")
														oXMLValorCampo.AppendChild(oXMLDatoElemento)

														oXMLDatoTexto = oXmlDoc.CreateTextNode(sDenDato)
													Else
														oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
													End If
												Else
													oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
												End If
											Case 1, 5, 6, 7  'Textos
												If Not (TypeOf (oFilaCampo.Item("VALOR_TEXT")) Is DBNull) Then
													sDato = oFilaCampo.Item("VALOR_TEXT")
												Else
													sDato = ""
												End If

												If Not TypeOf (oFilaCampo.Item("TIPO_CAMPO_GS")) Is DBNull Then
													If (CInt(oFilaCampo.Item("TIPO_CAMPO_GS")) >= 100 _
														And CInt(oFilaCampo.Item("TIPO_CAMPO_GS")) <= 105) Or (CInt(oFilaCampo.Item("TIPO_CAMPO_GS")) >= 107 _
														And CInt(oFilaCampo.Item("TIPO_CAMPO_GS")) <= 113) Then
														sDenDato = DenGS(sDato, oFilaCampo.Item("TIPO_CAMPO_GS"), sIdi)

														oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
														oXMLDatoElemento.AppendChild(oXMLDatoTexto)
														oXMLDatoElemento = Nothing
														oXMLDatoTexto = Nothing
														oXMLDatoElemento = oXmlDoc.CreateElement("VALOR_DEN")
														oXMLValorCampo.AppendChild(oXMLDatoElemento)

														oXMLDatoTexto = oXmlDoc.CreateTextNode(sDenDato)
													Else
														oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
													End If
												Else
													oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
												End If
											Case 8 ' Archivos
												sAdjun = ""

												For Each oAdjunto In oFilaCampo.GetChildRows("REL_CAMPO_ADJUN")
													sAdjun += oAdjunto.Item("NOM") + " (" + FormatNumber((oAdjunto.Item("DATASIZE") / 1024), oUser.NumberFormat) + " Kb.), "
												Next
												If sAdjun <> "" Then
													sAdjun = sAdjun.Substring(0, sAdjun.Length - 2)
												End If
												If Len(sAdjun) > 90 Then
													sDato = Mid(sAdjun, 1, 90) & "..."
												Else
													sDato = sAdjun
												End If
												oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
											Case 2  'Num�ricos
												If Not (TypeOf (oFilaCampo.Item("VALOR_NUM")) Is DBNull) Then
													sDato = oFilaCampo.Item("VALOR_NUM")
												Else
													If (CInt(DBNullToSomething(oFilaCampo.Item("TIPO_CAMPO_GS"))) >= 110 And CInt(DBNullToSomething(oFilaCampo.Item("TIPO_CAMPO_GS"))) <= 113) Then
														sDato = DBNullToStr(oFilaCampo.Item("VALOR_TEXT"))
													Else
														sDato = ""
													End If
												End If
												If Not TypeOf (oFilaCampo.Item("TIPO_CAMPO_GS")) Is DBNull Then
													If (CInt(oFilaCampo.Item("TIPO_CAMPO_GS")) >= 100 _
														And CInt(oFilaCampo.Item("TIPO_CAMPO_GS")) <= 105) Or (CInt(oFilaCampo.Item("TIPO_CAMPO_GS")) >= 107 _
														And CInt(oFilaCampo.Item("TIPO_CAMPO_GS")) <= 113) Then
														sDenDato = DenGS(sDato, oFilaCampo.Item("TIPO_CAMPO_GS"), sIdi)

														oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)

														oXMLDatoElemento.AppendChild(oXMLDatoTexto)
														oXMLDatoElemento = Nothing
														oXMLDatoTexto = Nothing
														oXMLDatoElemento = oXmlDoc.CreateElement("VALOR_DEN")
														oXMLValorCampo.AppendChild(oXMLDatoElemento)

														oXMLDatoTexto = oXmlDoc.CreateTextNode(sDenDato)
													ElseIf CInt(oFilaCampo.Item("TIPO_CAMPO_GS")) = Fullstep.PMPortalDatabaseServer.TiposDeDatos.TipoCampoGS.ImporteRepercutido Then
														sDenDato = DBNullToStr(oFilaCampo.Item("VALOR_TEXT"))

														If sDato = "" Then
															sDenDato = ""
															oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
														Else
															oXMLDatoTexto = oXmlDoc.CreateTextNode(modUtilidades.FormatNumber(sDato, oUser.NumberFormat))
														End If

														oXMLDatoElemento.AppendChild(oXMLDatoTexto)
														oXMLDatoElemento = Nothing
														oXMLDatoTexto = Nothing
														oXMLDatoElemento = oXmlDoc.CreateElement("VALOR_DEN")
														oXMLValorCampo.AppendChild(oXMLDatoElemento)

														oXMLDatoTexto = oXmlDoc.CreateTextNode(sDenDato)
													Else
														If sDato <> "" Then
															oXMLDatoTexto = oXmlDoc.CreateTextNode(modUtilidades.FormatNumber(sDato, oUser.NumberFormat))
														Else
															oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
														End If
													End If
												Else
													If sDato <> "" Then
														oXMLDatoTexto = oXmlDoc.CreateTextNode(modUtilidades.FormatNumber(sDato, oUser.NumberFormat))
													Else
														oXMLDatoTexto = oXmlDoc.CreateTextNode(sDato)
													End If
												End If
											Case 3  ' Fechas
												If Not (TypeOf (oFilaCampo.Item("VALOR_FEC")) Is DBNull) Then
													oXMLDatoTexto = oXmlDoc.CreateTextNode(modUtilidades.FormatDate(oFilaCampo.Item("VALOR_FEC"), oUser.DateFormat))
												Else
													oXMLDatoTexto = oXmlDoc.CreateTextNode("")
												End If
											Case 4  ' Booleanos
												If Not (TypeOf (oFilaCampo.Item("VALOR_BOOL")) Is DBNull) Then
													If oFilaCampo.Item("VALOR_BOOL") = 1 Then
														oXMLDatoTexto = oXmlDoc.CreateTextNode(oTextos.Rows(14).Item(1))
													Else
														oXMLDatoTexto = oXmlDoc.CreateTextNode(oTextos.Rows(15).Item(1))
													End If
												Else
													oXMLDatoTexto = oXmlDoc.CreateTextNode("")
												End If
											Case 9  ' Desgloses
												oXMLDesglose = oXmlDoc.CreateElement("DESGLOSE")
												oXMLCampo.AppendChild(oXMLDesglose)

												oCampo = FSWSServer.Get_Campo()
												oCampo.Id = oFilaCampo.Item("ID")

												'Cargamos DataSet con cabecera y l�neas de desglose
												Dim bNuevoWorkflow As Boolean
												If Request("TipoImpExp") = "1" Then
													bNuevoWorkflow = True
												Else
													bNuevoWorkflow = False
												End If
												oDesglose = oCampo.LoadInstDesglose(lCiaComp, sIdi, oInstancia.ID, oUser.AprobadorActual, oUser.CodProveGS, oInstancia.Version)

												'En la tabla 0 est�n las cabeceras
												'Pasamos los datos a un array para tenerlos en el procesado
												'de las filas
												If Request("TipoImpExp") = "3" Then
													If Not oNoConformidad.Data Is Nothing Then
														'Carga las cabeceras de estado y estado interno
														Dim oNewRow As DataRow
														Dim oDTEstados As DataTable = oDesglose.Tables(0)
														oNewRow = oDTEstados.NewRow
														oNewRow.Item("ID") = Fullstep.PMPortalServer.IdsFicticios.EstadoInterno
														oNewRow.Item("GRUPO") = oGrupo.Id
														oNewRow.Item("DEN_" & sIdi) = oTextos.Rows(16).Item(1) 'Estado interno
														oNewRow.Item("SUBTIPO") = Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio
														oNewRow.Item("INTRO") = 0
														oNewRow.Item("VISIBLE") = 1
														oNewRow.Item("TIPO_CAMPO_GS") = Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.EstadoInternoNoConf
														oDTEstados.Rows.Add(oNewRow)

														oNewRow = oDTEstados.NewRow
														oNewRow.Item("ID") = Fullstep.PMPortalServer.IdsFicticios.Comentario
														oNewRow.Item("GRUPO") = oGrupo.Id
														oNewRow.Item("DEN_" & sIdi) = oTextos.Rows(17).Item(1) 'Comentario
														oNewRow.Item("SUBTIPO") = Fullstep.PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoLargo
														oNewRow.Item("INTRO") = 0
														oNewRow.Item("VISIBLE") = 1
														oDTEstados.Rows.Add(oNewRow)

														'Valores de estado actual y estado interno:
														Dim oDTLineasDesglose As DataTable = oDesglose.Tables(2)
														For Each oRow In oNoConformidad.EstadosComentariosInternosAcciones.Rows
															If oRow.Item("CAMPO_PADRE") = oCampo.IdCopiaCampo Then
																oNewRow = oDTLineasDesglose.NewRow
																oNewRow.Item("CAMPO_PADRE") = oRow.Item("CAMPO_PADRE")
																oNewRow.Item("LINEA") = oRow.Item("LINEA")
																oNewRow.Item("CAMPO_HIJO") = Fullstep.PMPortalServer.IdsFicticios.EstadoActual
																oNewRow.Item("VALOR_TEXT") = oRow.Item("ESTADO")
																For iFila = 0 To oDSLista.Tables(0).Rows.Count - 1
																	If oDSLista.Tables(0).Rows(iFila).Item("COD") = DBNullToSomething(oRow.Item("ESTADO")) Then
																		oNewRow.Item("VALOR_TEXT") = oNewRow.Item("VALOR_TEXT") & "-" & oDSLista.Tables(0).Rows(iFila).Item("DEN_" & sIdi)
																		Exit For
																	End If
																Next
																oDTLineasDesglose.Rows.Add(oNewRow)

																oNewRow = oDTLineasDesglose.NewRow
																oNewRow.Item("CAMPO_PADRE") = oRow.Item("CAMPO_PADRE")
																oNewRow.Item("LINEA") = oRow.Item("LINEA")
																oNewRow.Item("CAMPO_HIJO") = Fullstep.PMPortalServer.IdsFicticios.EstadoInterno
																oNewRow.Item("VALOR_NUM") = oRow.Item("ESTADO_INT")

																Select Case oRow.Item("ESTADO_INT")
																	Case 0, Fullstep.PMPortalServer.TipoEstadoAcciones.SinRevisar
																		oNewRow.Item("VALOR_TEXT") = oTextos.Rows(18).Item(1)
																	Case Fullstep.PMPortalServer.TipoEstadoAcciones.Aprobada
																		oNewRow.Item("VALOR_TEXT") = oTextos.Rows(19).Item(1)
																	Case Fullstep.PMPortalServer.TipoEstadoAcciones.Rechazada
																		oNewRow.Item("VALOR_TEXT") = oTextos.Rows(20).Item(1)
																	Case Fullstep.PMPortalServer.TipoEstadoAcciones.FinalizadaSinRevisar
																		oNewRow.Item("VALOR_TEXT") = oTextos.Rows(21).Item(1)
																	Case Fullstep.PMPortalServer.TipoEstadoAcciones.FinalizadaRevisada
																		oNewRow.Item("VALOR_TEXT") = oTextos.Rows(22).Item(1)
																End Select
																oDTLineasDesglose.Rows.Add(oNewRow)

																oNewRow = oDTLineasDesglose.NewRow
																oNewRow.Item("CAMPO_PADRE") = oRow.Item("CAMPO_PADRE")
																oNewRow.Item("LINEA") = oRow.Item("LINEA")
																oNewRow.Item("CAMPO_HIJO") = Fullstep.PMPortalServer.IdsFicticios.Comentario
																oNewRow.Item("VALOR_TEXT") = oRow.Item("COMENT")

																oDTLineasDesglose.Rows.Add(oNewRow)
															End If
														Next
													End If
												End If

												iNumColumnas = oDesglose.Tables(0).Rows.Count

												ReDim iCampoColumna(iNumColumnas)
												ReDim iTipoColumna(iNumColumnas)
												ReDim iTipoGSColumna(iNumColumnas)
												ReDim sDenColumna(iNumColumnas)
												ReDim bVisibleColumna(iNumColumnas)
												ReDim bIntroColumna(iNumColumnas)
												ReDim lTablaExterna(iNumColumnas)

												iColumna = 0

												For Each oFilaDesglose In oDesglose.Tables(0).Rows
													iCampoColumna(iColumna) = oFilaDesglose.Item("ID")

													iTipoColumna(iColumna) = oFilaDesglose.Item("SUBTIPO")

													If Not TypeOf (oFilaDesglose.Item("TIPO_CAMPO_GS")) Is DBNull Then
														iTipoGSColumna(iColumna) = oFilaDesglose.Item("TIPO_CAMPO_GS")
													Else
														iTipoGSColumna(iColumna) = 0
													End If

													sDenColumna(iColumna) = DBNullToStr(oFilaDesglose.Item("DEN_" & sIdi))

													If oFilaDesglose.Item("VISIBLE") = 1 Then
														bVisibleColumna(iColumna) = True
													Else
														bVisibleColumna(iColumna) = False
													End If

													If oFilaDesglose.Item("INTRO") = 1 Then
														bIntroColumna(iColumna) = True
													Else
														bIntroColumna(iColumna) = False
													End If

													iColumna = iColumna + 1
												Next

												'Preparando el procesado de las filas del desglose
												'Problema en gestamp cuando habia desglose con 0 lineas cascaba
												If oDesglose.Tables(0).Rows.Count = 0 Then
													'le pongo valor 1 para que me genere la cabecera del desglose a pesar de que no tenga lineas
													iNumFilas = 1
												Else
													If Not (TypeOf (oDesglose.Tables(0).Rows(0).Item("LINEA")) Is System.DBNull) Then
														iNumFilas = oDesglose.Tables(0).Rows(0).Item("LINEA")
													Else
														iNumFilas = 1
													End If
												End If

												Dim KeyFields(2) As DataColumn
												KeyFields(0) = oDesglose.Tables(2).Columns("LINEA")
												KeyFields(1) = oDesglose.Tables(2).Columns("CAMPO_PADRE")
												KeyFields(2) = oDesglose.Tables(2).Columns("CAMPO_HIJO")

												oDesglose.Tables(2).PrimaryKey = KeyFields

												ReDim KeyFields(1)

												KeyFields(0) = oDesglose.Tables(1).Columns("ID")
												KeyFields(1) = oDesglose.Tables(1).Columns("ORDEN")

												oDesglose.Tables(1).PrimaryKey = KeyFields

												'Por cada fila, buscamos el valor de aquellas columnas visibles
												'Si hay valor lo ponemos, si no ponemos solo la cabecera y un valor vac�o
												For iFila = 0 To iNumFilas - 1
													oXMLFilaDesglose = oXmlDoc.CreateElement("FILA_DESGLOSE")
													oXMLDesglose.AppendChild(oXMLFilaDesglose)

													For iColumna = 0 To iNumColumnas - 1
														If bVisibleColumna(iColumna) = True Then 'Cabecera
															oXMLColumnaDesglose = oXmlDoc.CreateElement("COLUMNA_DESGLOSE")
															oXMLFilaDesglose.AppendChild(oXMLColumnaDesglose)

															oXMLDatoElementoDesglose = oXmlDoc.CreateElement("NOMBRE")
															oXMLColumnaDesglose.AppendChild(oXMLDatoElementoDesglose)
															oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDenColumna(iColumna))
															oXMLDatoElementoDesglose.AppendChild(oXMLDatoTextoDesglose)
															oXMLDatoElementoDesglose = Nothing
															oXMLDatoTextoDesglose = Nothing

															oXMLValorColumnaDesglose = oXmlDoc.CreateElement("VALOR")
															oXMLColumnaDesglose.AppendChild(oXMLValorColumnaDesglose)
															oXMLDatoElementoDesglose = oXmlDoc.CreateElement("VALOR_DATO")
															oXMLValorColumnaDesglose.AppendChild(oXMLDatoElementoDesglose)

															'Datos ...
															Dim SearchFields(2)
															SearchFields(0) = iFila + 1
															If oCampo.IdCopiaCampo = Nothing Then
																SearchFields(1) = oCampo.Id
															Else
																SearchFields(1) = oCampo.IdCopiaCampo
															End If

															If iTipoGSColumna(iColumna) = Fullstep.PMPortalDatabaseServer.TiposDeDatos.TipoCampoGS.EstadoNoConf Then
																SearchFields(2) = Fullstep.PMPortalServer.IdsFicticios.EstadoActual
															Else
																SearchFields(2) = iCampoColumna(iColumna)
															End If

															oFilaDesglose = oDesglose.Tables(2).Rows.Find(SearchFields)

															If oFilaDesglose Is Nothing Then
																If Request("TipoImpExp") = "3" _
																AndAlso oNoConformidad.EstadosComentariosInternosAcciones.Rows.Count = 0 _
																AndAlso iCampoColumna(iColumna) = Fullstep.PMPortalServer.IdsFicticios.EstadoInterno Then
																	oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(oTextos.Rows(18).Item(1))
																Else
																	sDato = ""
																	oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
																End If
																oXMLDatoElementoDesglose.AppendChild(oXMLDatoTextoDesglose)
															Else
																If lTablaExterna(iColumna) = 0 Then
																	Select Case iTipoColumna(iColumna)
																		Case 0  ' Sin tipo
																			If Not (TypeOf (oFilaDesglose.Item("VALOR_TEXT")) Is DBNull) Then
																				sDato = oFilaDesglose.Item("VALOR_TEXT")
																			Else
																				sDato = ""
																			End If

																			If iTipoGSColumna(iColumna) <> 0 Then
																				If (iTipoGSColumna(iColumna) >= 100 _
																					And iTipoGSColumna(iColumna) <= 105) Or (iTipoGSColumna(iColumna) >= 107 _
																					And iTipoGSColumna(iColumna) <= 113) Then
																					sDenDato = DenGS(sDato, iTipoGSColumna(iColumna), sIdi)

																					oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
																					oXMLDatoElementoDesglose.AppendChild(oXMLDatoTextoDesglose)
																					oXMLDatoElementoDesglose = Nothing
																					oXMLDatoTextoDesglose = Nothing
																					oXMLDatoElementoDesglose = oXmlDoc.CreateElement("VALOR_DEN")
																					oXMLValorColumnaDesglose.AppendChild(oXMLDatoElementoDesglose)

																					oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDenDato)
																				Else
																					oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)

																				End If
																			Else
																				oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
																			End If
																		Case 1, 5, 6, 7, 8  ' Textos (Incluye adjuntos porque el nombre de los archivos est� en el texto)
																			If bIntroColumna(iColumna) = False Then
																				If Not (TypeOf (oFilaDesglose.Item("VALOR_TEXT")) Is DBNull) Then
																					sDato = oFilaDesglose.Item("VALOR_TEXT")
																				Else
																					sDato = ""
																				End If
																				If iTipoGSColumna(iColumna) <> 0 Then

																					If (iTipoGSColumna(iColumna) >= 100 And iTipoGSColumna(iColumna) <= 105) Or (iTipoGSColumna(iColumna) >= 107 And iTipoGSColumna(iColumna) <= 113) Then

																						sDenDato = DenGS(sDato, iTipoGSColumna(iColumna), sIdi)

																						oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
																						oXMLDatoElementoDesglose.AppendChild(oXMLDatoTextoDesglose)
																						oXMLDatoElementoDesglose = Nothing
																						oXMLDatoTextoDesglose = Nothing
																						oXMLDatoElementoDesglose = oXmlDoc.CreateElement("VALOR_DEN")
																						oXMLValorColumnaDesglose.AppendChild(oXMLDatoElementoDesglose)

																						oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDenDato)
																					Else
																						oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
																					End If
																				Else
																					oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
																				End If
																			Else
																				Dim oDrValorLista As DataRow
																				Dim SearchValorLista(1)

																				If Not (TypeOf (oFilaDesglose.Item("VALOR_NUM")) Is DBNull) Then
																					SearchValorLista(0) = oFilaDesglose.Item("CAMPO_HIJO")
																					SearchValorLista(1) = CInt(oFilaDesglose.Item("VALOR_NUM"))

																					oDrValorLista = oDesglose.Tables(1).Rows.Find(SearchValorLista)

																					If oDrValorLista Is Nothing Then
																						sDato = ""
																					Else
																						sDato = oDrValorLista.Item("VALOR_TEXT_" & sIdi)
																					End If
																				Else
																					sDato = ""
																				End If

																				If iTipoGSColumna(iColumna) <> 0 Then
																					If (iTipoGSColumna(iColumna) >= 100 _
																						And iTipoGSColumna(iColumna) <= 105) Or (iTipoGSColumna(iColumna) >= 107 _
																						And iTipoGSColumna(iColumna) <= 113) Then
																						sDenDato = DenGS(sDato, iTipoGSColumna(iColumna), sIdi)

																						oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
																						oXMLDatoElementoDesglose.AppendChild(oXMLDatoTextoDesglose)
																						oXMLDatoElementoDesglose = Nothing
																						oXMLDatoTextoDesglose = Nothing
																						oXMLDatoElementoDesglose = oXmlDoc.CreateElement("VALOR_DEN")
																						oXMLValorColumnaDesglose.AppendChild(oXMLDatoElementoDesglose)

																						oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDenDato)
																					Else
																						oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
																					End If
																				Else
																					oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
																				End If
																			End If
																		Case 2  ' Num�ricos
																			If Not (TypeOf (oFilaDesglose.Item("VALOR_NUM")) Is DBNull) Then
																				sDato = oFilaDesglose.Item("VALOR_NUM")
																			Else
																				If (iTipoGSColumna(iColumna) >= 110 And iTipoGSColumna(iColumna) <= 113) Then
																					sDato = DBNullToStr(oFilaDesglose.Item("VALOR_TEXT"))
																				Else
																					sDato = ""
																				End If
																			End If

																			If iTipoGSColumna(iColumna) <> 0 Then
																				If (iTipoGSColumna(iColumna) >= 100 _
																					And iTipoGSColumna(iColumna) <= 105) Or (iTipoGSColumna(iColumna) >= 107 _
																					And iTipoGSColumna(iColumna) <= 113) Then
																					sDenDato = DenGS(sDato, iTipoGSColumna(iColumna), sIdi)

																					oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)

																					oXMLDatoElementoDesglose.AppendChild(oXMLDatoTextoDesglose)
																					oXMLDatoElementoDesglose = Nothing
																					oXMLDatoTextoDesglose = Nothing
																					oXMLDatoElementoDesglose = oXmlDoc.CreateElement("VALOR_DEN")
																					oXMLValorColumnaDesglose.AppendChild(oXMLDatoElementoDesglose)

																					oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDenDato)
																				ElseIf iTipoGSColumna(iColumna) = Fullstep.PMPortalDatabaseServer.TiposDeDatos.TipoCampoGS.ImporteRepercutido Then
																					sDenDato = DBNullToStr(oFilaDesglose.Item("VALOR_TEXT"))

																					If sDato = "" Then
																						sDenDato = ""
																						oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
																					Else
																						oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(modUtilidades.FormatNumber(sDato, oUser.NumberFormat))
																					End If

																					oXMLDatoElementoDesglose.AppendChild(oXMLDatoTextoDesglose)
																					oXMLDatoElementoDesglose = Nothing
																					oXMLDatoTextoDesglose = Nothing
																					oXMLDatoElementoDesglose = oXmlDoc.CreateElement("VALOR_DEN")
																					oXMLValorColumnaDesglose.AppendChild(oXMLDatoElementoDesglose)

																					oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDenDato)
																				Else
																					If sDato <> "" Then
																						oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(modUtilidades.FormatNumber(sDato, oUser.NumberFormat))
																					Else
																						oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
																					End If
																				End If
																			Else
																				If sDato <> "" Then
																					oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(modUtilidades.FormatNumber(sDato, oUser.NumberFormat))
																				Else
																					oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
																				End If
																			End If
																		Case 3  ' Fechas
																			If Not (TypeOf (oFilaDesglose.Item("VALOR_FEC")) Is DBNull) Then
																				oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(modUtilidades.FormatDate(oFilaDesglose.Item("VALOR_FEC"), oUser.DateFormat))
																			Else
																				oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode("")
																			End If
																		Case 4  ' Booleanos
																			If Not (TypeOf (oFilaDesglose.Item("VALOR_BOOL")) Is DBNull) Then
																				If oFilaDesglose.Item("VALOR_BOOL") = 1 Then
																					oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(oTextos.Rows(14).Item(1))
																				Else
																					oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(oTextos.Rows(15).Item(1))
																				End If
																			Else
																				oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode("")
																			End If
																	End Select
																Else
																	Dim sCampoValor As String
																	Select Case iTipoColumna(iColumna)
																		Case 2
																			sCampoValor = "VALOR_NUM"
																		Case 3
																			sCampoValor = "VALOR_FEC"
																		Case 4
																			sCampoValor = "VALOR_BOOL"
																		Case Else
																			sCampoValor = "VALOR_TEXT"
																	End Select
																	Dim oTablaExterna As PMPortalServer.TablaExterna = FSWSServer.get_TablaExterna
																	oTablaExterna.LoadDefTabla(lCiaComp, lTablaExterna(iColumna), False)
																	oTablaExterna.LoadData(lCiaComp)
																	If Not IsDBNull(oFilaCampo.Item(sCampoValor)) Then
																		sDato = oTablaExterna.DescripcionReg(lCiaComp, oFilaCampo.Item(sCampoValor), sIdi, oUser.DateFormat, oUser.NumberFormat)
																	Else
																		sDato = ""
																	End If
																	oXMLDatoTextoDesglose = oXmlDoc.CreateTextNode(sDato)
																End If

																oXMLDatoElementoDesglose.AppendChild(oXMLDatoTextoDesglose)
															End If
														End If
														'Siguiente columna
													Next
													'Siguiente fila
												Next
										End Select
									End If

									If oFilaCampo.Item("SUBTIPO") < 9 Then
										oXMLDatoElemento.AppendChild(oXMLDatoTexto)
										oXMLDatoElemento = Nothing
										oXMLDatoTexto = Nothing
									End If
								End If
								'Siguiente campo del grupo
							Next
						End If
					End If
					'Siguiente grupo de la instancia
				Next
			End If
			Select Case Request("Formato")
				Case "XML"
					Dim sPath As String
					Dim sBytes As Long
					sPath = ConfigurationManager.AppSettings("temp") + "\" + GenerateRandomPath() & "_" & FSPMUser.IdCia
					If Not Directory.Exists(sPath) Then Directory.CreateDirectory(sPath)

					sPath += "\" + CStr(oInstancia.ID) + ".xml"
					oXmlDoc.Save(sPath)

					Dim ofile As New System.IO.FileInfo(sPath)

					sBytes = ofile.Length

					Dim arrPath() As String = sPath.Split("\")
					Response.Redirect("../_common/download.aspx?path=" + arrPath(UBound(arrPath) - 1) + "&nombre=" + Server.UrlEncode(arrPath(UBound(arrPath))) + "&datasize=" + CStr(sBytes))
				Case "HTML"
					Dim oXslDoc As New XslTransform
					If Request("Desgloses") = "TABLA" Then
						If Request("TipoImpExp") = "1" Then
							oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & sIdi & "_impexptab_Portal.xslt")
						ElseIf Request("TipoImpExp") = "2" Then
							oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & sIdi & "_impexpcerttab_Portal.xslt")
						ElseIf Request("TipoImpExp") = "3" Then
							oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & sIdi & "_impexpnoconftab_Portal.xslt")
						End If
					Else
						If Request("TipoImpExp") = "1" Then
							oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & sIdi & "_impexplin_Portal.xslt")
						ElseIf Request("TipoImpExp") = "2" Then
							oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & sIdi & "_impexpcertlin_Portal.xslt")
						ElseIf Request("TipoImpExp") = "3" Then
							oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & sIdi & "_impexpnoconflin_Portal.xslt")
						End If
					End If
					'Pasamos el HTML a una String                    
					xmlPres.Document = oXmlDoc
					xmlPres.Transform = oXslDoc

					'RUE a�adido para que se pueda elegir entre abrirlo y guardarlo
					'Pasamos el HTML a una String
					Dim oStringWriter As New System.IO.StringWriter
					Dim oHtmlTextWriter As New System.Web.UI.HtmlTextWriter(oStringWriter)

					oHtmlTextWriter.WriteLine("<HTML>")
					oHtmlTextWriter.WriteLine("<HEAD>")
					oHtmlTextWriter.WriteLine("<meta http-equiv=""Content-Type"" content=""text/html; charset=windows-1252"">")
					oHtmlTextWriter.WriteLine("</HEAD>")
					oHtmlTextWriter.WriteLine("<BODY>")

					xmlPres.RenderControl(oHtmlTextWriter)

					oHtmlTextWriter.WriteLine("</BODY>")
					oHtmlTextWriter.WriteLine("</HTML>")

					'Guardamos el HTML en un archivo
					Dim sPath As String
					Dim sBytes As Long

					sPath = ConfigurationManager.AppSettings("temp") + "\" + GenerateRandomPath() & "_" & FSPMUser.IdCia
					Dim oFolder As System.IO.Directory
					If Not oFolder.Exists(sPath) Then
						oFolder.CreateDirectory(sPath)
					End If

					sPath += "\" + CStr(oInstancia.ID) + ".html"

					Dim oStreamWriter As New System.IO.StreamWriter(sPath, False, System.Text.Encoding.UTF8)
					oStreamWriter.Write(oStringWriter.ToString)

					oStreamWriter.Close()

					Dim arrPath() As String = sPath.Split("\")
					Response.Redirect("../_common/download.aspx?path=" + arrPath(UBound(arrPath) - 1) + "&nombre=" + Server.UrlEncode(arrPath(UBound(arrPath))) + "&datasize=" + CStr(sBytes))
				Case "PDF"
					Dim oXslDoc As New XslTransform
					If Request("Desgloses") = "TABLA" Then
						If Request("TipoImpExp") = "1" Then
							oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & sIdi & "_impexptab_Portal.xslt")
						ElseIf Request("TipoImpExp") = "2" Then
							oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & sIdi & "_impexpcerttab_Portal.xslt")
						ElseIf Request("TipoImpExp") = "3" Then
							oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & sIdi & "_impexpnoconftab_Portal.xslt")
						End If
					Else
						If Request("TipoImpExp") = "1" Then
							oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & sIdi & "_impexplin_Portal.xslt")
						ElseIf Request("TipoImpExp") = "2" Then
							oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & sIdi & "_impexpcertlin_Portal.xslt")
						ElseIf Request("TipoImpExp") = "3" Then
							oXslDoc.Load(ConfigurationManager.AppSettings("xslTemplates") & "\" & sIdi & "_impexpnoconflin_Portal.xslt")
						End If
					End If

					xmlPres.Document = oXmlDoc
					xmlPres.Transform = oXslDoc

					'Pasamos el HTML a una String
					Dim oStringWriter As New System.IO.StringWriter
					Dim oHtmlTextWriter As New System.Web.UI.HtmlTextWriter(oStringWriter)

					oHtmlTextWriter.WriteLine("<HTML>")
					oHtmlTextWriter.WriteLine("<HEAD>")
					oHtmlTextWriter.WriteLine("<meta http-equiv=""Content-Type"" content=""text/html; charset=windows-1252"">")
					oHtmlTextWriter.WriteLine("</HEAD>")
					oHtmlTextWriter.WriteLine("<BODY>")

					xmlPres.RenderControl(oHtmlTextWriter)

					oHtmlTextWriter.WriteLine("</BODY>")
					oHtmlTextWriter.WriteLine("</HTML>")

					'Guardamos el HTML en un archivo
					Dim sPathBase As String
					Dim sPathHTML As String
					Dim sPathPDF As String
					Dim sBytes As Long
					Dim oFolder As System.IO.Directory

					sPathBase = ConfigurationManager.AppSettings("temp") + "\" + GenerateRandomPath() & "_" & FSPMUser.IdCia
					If Not oFolder.Exists(sPathBase) Then
						oFolder.CreateDirectory(sPathBase)
					End If
					sPathHTML = sPathBase + "\" + CStr(oInstancia.ID) + ".html"
					sPathPDF = sPathBase + "\" + CStr(oInstancia.ID) + ".pdf"

					Dim oStreamWriter As New System.IO.StreamWriter(sPathHTML, False, System.Text.Encoding.UTF8)
					oStreamWriter.Write(oStringWriter.ToString)
					oStreamWriter.Close()

					'Pasamos el HTML a PDF
					ConvertHTMLToPDF(sPathHTML, sPathPDF)

					'Ponemos el PDF a descargar
					Dim ofile As New System.IO.FileInfo(sPathPDF)
					sBytes = ofile.Length
					Dim arrPath2() As String = sPathPDF.Split("\")
					Response.Redirect("../_common/download.aspx?path=" + arrPath2(UBound(arrPath2) - 1) + "&nombre=" + Server.UrlEncode(arrPath2(UBound(arrPath2))) + "&datasize=" + CStr(sBytes))
			End Select
		End Sub

		''' <summary>
		''' Funci�n para completar el codigo de GS con su descripci�n
		''' </summary>
		''' <param name="sDato">Dato del que vamos a sacar la denominacion</param>
		''' <param name="tipo">Tipo dato del GS</param>   
		''' <param name="Idi">Idioma</param> 
		''' <returns> Devuelve el dato denominaci�n del GS </returns>
		''' <remarks> Llamada desde del Page_Load() de impexp.aspx; Tiempo m�ximo = 0.1 seg</remarks>
		Private Function DenGS(ByRef sDato As String, ByVal tipo As Integer, ByVal Idi As String) As String
			'Funci�n para completar el c�digo de GS con su descripci�n
			Dim oUser As Fullstep.PMPortalServer.User
			Dim lCiaComp As Long = IdCiaComp

			oUser = Session("FS_Portal_User")

			Dim sDen As String
			'Definimos estas variables est�ticas para cuando venga despu�s de tener el material
			'con el c�digo del art�culo a por la denominaci�n...

			Static sCodGMN1 As String
			Static sCodGMN2 As String
			Static sCodGMN3 As String
			Static sCodGMN4 As String

			'Definimos esta variable est�tica para cuando venga a por la provincia despu�s de tener
			'el pa�s...
			Static sCodPais As String
			'Acci�n
			Try
				If (tipo >= 100) And (sDato <> "") Then
					Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
					Select Case tipo
						Case 100 'Proveedor
							Dim oProve As Fullstep.PMPortalServer.Proveedor = FSWSServer.get_Proveedor()
							oProve.Cod = sDato
							sDen = oProve.Den
						Case 101 'Forma de pago
							Dim oFormaPago As Fullstep.PMPortalServer.FormasPago = FSWSServer.Get_FormasPago

							oFormaPago.LoadData(Idi, lCiaComp, sDato)
							sDen = oFormaPago.Data.Tables(0).Rows(0).Item("DEN")
						Case 102 'Moneda
							Dim oMoneda As Fullstep.PMPortalServer.Monedas = FSWSServer.Get_Monedas

							oMoneda.LoadData(Idi, lCiaComp, sDato)
							sDen = oMoneda.Data.Tables(0).Rows(0).Item("DEN")
						Case 103 'Grupo de material... Hay que extraer los c�digos
							'Primero mirar las longitudes en DIC
							'Luego extraerlos de sDato
							'Y luego... obtener la denominaci�n de GMN4.
							Dim ilGMN1 As Integer = FSWSServer.LongitudesDeCodigos.giLongCodGMN1
							Dim ilGMN2 As Integer = FSWSServer.LongitudesDeCodigos.giLongCodGMN2
							Dim ilGMN3 As Integer = FSWSServer.LongitudesDeCodigos.giLongCodGMN3
							Dim ilGMN4 As Integer = FSWSServer.LongitudesDeCodigos.giLongCodGMN4

							sCodGMN1 = ""
							sCodGMN2 = ""
							sCodGMN3 = ""
							sCodGMN4 = ""

							sCodGMN1 = Trim(Left(sDato, ilGMN1))

							If Len(sDato) > ilGMN1 Then
								sCodGMN2 = Trim(Mid(sDato, ilGMN1 + 1, ilGMN2))

								If Len(sDato) > ilGMN1 + ilGMN2 Then
									sCodGMN3 = Trim(Mid(sDato, ilGMN1 + ilGMN2 + 1, ilGMN3))

									If Len(sDato) > ilGMN1 + ilGMN2 + ilGMN3 Then
										sCodGMN4 = Trim(Mid(sDato & Space(Len(sDato) - (ilGMN1 + ilGMN2 + ilGMN3 + ilGMN4)), ilGMN1 + ilGMN2 + ilGMN3 + 1, ilGMN4)) 'GMN4

										Dim oGMN3 As Fullstep.PMPortalServer.GrupoMatNivel3 = FSWSServer.Get_GrupoMatNivel3
										oGMN3.GMN1Cod = sCodGMN1
										oGMN3.GMN2Cod = sCodGMN2
										oGMN3.Cod = sCodGMN3
										oGMN3.CargarTodosLosGruposMatDesde(lCiaComp, 1, Idi, sCodGMN4, , True)
										sDen = oGMN3.GruposMatNivel4.Item(sCodGMN4).Den
									Else ' GMN3
										Dim oGMN2 As Fullstep.PMPortalServer.GrupoMatNivel2 = FSWSServer.Get_GrupoMatNivel2

										oGMN2.GMN1Cod = sCodGMN1
										oGMN2.Cod = sCodGMN2
										oGMN2.CargarTodosLosGruposMatDesde(lCiaComp, 1, Idi, sCodGMN3, , True)
										sDen = oGMN2.GruposMatNivel3.Item(sCodGMN3).Den
									End If
								Else 'GMN2
									Dim oGMN1 As Fullstep.PMPortalServer.GrupoMatNivel1 = FSWSServer.Get_GrupoMatNivel1

									oGMN1.Cod = sCodGMN1
									oGMN1.CargarTodosLosGruposMatDesde(lCiaComp, 1, Idi, sCodGMN2, , True)
									sDen = oGMN1.GruposMatNivel2.Item(sCodGMN2).Den
								End If
							Else ' GMN1
								Dim ogmn1 As Fullstep.PMPortalServer.GruposMatNivel1 = FSWSServer.Get_GruposMatNivel1

								ogmn1.CargarTodosLosGruposMatDesde(lCiaComp, 1, Idi, sCodGMN1, , True)
								sDen = ogmn1.Item(sCodGMN1).Den
							End If
						Case 104 ' Art�culo
							Dim oArt As Fullstep.PMPortalServer.Articulos = FSWSServer.Get_Articulos

							oArt.GMN1 = sCodGMN1
							oArt.GMN2 = sCodGMN2
							oArt.GMN3 = sCodGMN3
							oArt.GMN4 = sCodGMN4
							oArt.LoadData(lCiaComp, sDato, , True)

							sDen = oArt.Data.Tables(0).Rows(0).Item("DEN")
						Case 105 ' Unidad
							Dim oUnidad As Fullstep.PMPortalServer.Unidades = FSWSServer.get_Unidades

							oUnidad.LoadData(Idi, lCiaComp, sDato)
							sDen = oUnidad.Data.Tables(0).Rows(0).Item("DEN")
						Case 107 ' Pais
							sCodPais = ""

							Dim oPais As Fullstep.PMPortalServer.Paises = FSWSServer.Get_Paises

							oPais.LoadData(Idi, lCiaComp, sDato)
							sDen = oPais.Data.Tables(0).Rows(0).Item("DEN")
							sCodPais = sDato
						Case 108 ' Provincia
							Dim oProvincia As Fullstep.PMPortalServer.Provincias = FSWSServer.Get_Provincias

							oProvincia.Pais = sCodPais
							oProvincia.LoadData(Idi, lCiaComp, sDato)

							sDen = oProvincia.Data.Tables(0).Rows(0).Item("DEN")
						Case 109 ' Destino
							Dim oDestino As Fullstep.PMPortalServer.Destino = FSWSServer.Get_Destino

							oDestino.Cod = sDato
							sDen = oDestino.Den
						Case 110, 111, 112, 113 ' Presupuesto
							Dim oPres As Object

							Select Case tipo
								Case 110
									oPres = FSWSServer.Get_PresProyectosNivel1
								Case 111
									oPres = FSWSServer.Get_PresContablesNivel1
								Case 112
									oPres = FSWSServer.Get_PresConceptos3Nivel1
								Case 113
									oPres = FSWSServer.Get_PresConceptos4Nivel1
							End Select

							Dim arrPresupuestos() As String = sDato.Split("#")
							Dim oPresup As String
							Dim arrPresup(2) As String
							Dim iNivel As Integer
							Dim lIdPresup As Long
							Dim dPorcent As Double
							Dim oDs As DataSet

							sDen = ""
							For Each oPresup In arrPresupuestos
								arrPresup = oPresup.Split("_")
								iNivel = arrPresup(0)
								lIdPresup = arrPresup(1)
								dPorcent = Numero(arrPresup(2), ".", ",")

								Select Case iNivel
									Case 1
										oPres.LoadData(lIdPresup, , , )
									Case 2
										oPres.LoadData(Nothing, lIdPresup, , )
									Case 3
										oPres.LoadData(Nothing, Nothing, lIdPresup, )
									Case 4
										oPres.LoadData(Nothing, Nothing, Nothing, lIdPresup)
								End Select
								oDs = oPres.Data

								Dim iAnyo As Integer
								Dim sUon1 As String
								Dim sUon2 As String
								Dim sUon3 As String

								If Not oDs Is Nothing Then
									If oDs.Tables(0).Rows.Count > 0 Then
										With oDs.Tables(0).Rows(0)
											'comprobamos que el valor por defecto cumpla la restricci�n del usuario. SI NO CUMPLE, NO APARECER�
											If Not oDs.Tables(0).Columns("ANYO") Is Nothing Then iAnyo = .Item("ANYO")

											sUon1 = DBNullToSomething(.Item("UON1"))
											sUon2 = DBNullToSomething(.Item("UON2"))
											sUon3 = DBNullToSomething(.Item("UON3"))

											Dim bSeleccionado As Boolean = True
											Dim sPorcentaje As String
											Dim sDenPres As String
											For i As Integer = 4 To 1 Step -1 'PRESi:
												If Not IsDBNull(.Item("PRES" & i)) Then
													If bSeleccionado Then
														sPorcentaje = " (" & dPorcent.ToString("0.00%", oUser.NumberFormat) & "); "
														sDenPres = " - " & .Item("DEN").ToString
													Else
														sPorcentaje = " - "
														sDenPres = ""
													End If
													sDen = .Item("PRES" & i).ToString & sDenPres & sPorcentaje & sDen
													bSeleccionado = False
												End If
											Next
											If Not oDs.Tables(0).Columns("ANYO") Is Nothing Then
												sDen = .Item("ANYO").ToString & " - " & sDen
											End If

											bSeleccionado = True
											Dim sUONs As String = ""
											Dim sDenUON As String = ""
											For i As Integer = 3 To 1 Step -1
												'UONi
												If Not IsDBNull(.Item("UON" & i)) Then
													If bSeleccionado Then
														sDenUON = " - " & .Item("UONDEN").ToString
													Else
														sDenUON = " - "
													End If
													sUONs = .Item("UON" & i).ToString & sDenUON & sUONs
													bSeleccionado = False
												End If
											Next
											sDen = "( " & sUONs & " ) " & sDen
										End With
									End If
								End If
							Next
							sDato = ""
					End Select
					DenGS = sDen
				End If
			Catch ex As Exception
				'Si hay alg�n problema para obtener el dato de denominaci�n de GS,
				'no se incluye.
				DenGS = ""
			End Try
		End Function

		''' <summary>
		''' Control Vulnerabilidad ImpExp
		''' </summary>
		''' <param name="lCiaComp">Compania</param>
		''' <param name="IdQa">Id certif/No Conf</param>
		''' <param name="Proveedor">Proveedor</param>
		''' <param name="Instancia">Instancia</param>
		''' <param name="EsCertificado">1->certif/0->No Conf</param>
		Private Function CtrlVulnerabilidad(ByVal lCiaComp As Long, ByVal IdQa As Long, ByVal Proveedor As String, ByVal Instancia As Long, ByVal EsCertificado As Boolean) As Boolean
			Return oInstancia.CtrlVulnerabilidad_ImpExp(lCiaComp, IdQa, Proveedor, Instancia, EsCertificado)
		End Function
	End Class
End Namespace