﻿Imports Fullstep
Namespace Fullstep.PMPortalWeb
    Public Class attachfileEmail
        Inherits FSPMPage

        Public Titulo As String
        Private Ruta As String
        Private mbFaltaPComa As Boolean

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            ModuloIdioma = PMPortalServer.TiposDeDatos.ModulosIdiomas.AttachFile
            Ruta = Request("Ruta")

            'Titulo = Textos(0)
            Me.lblSeleccione.Text = Textos(0)
            Me.cmdUpload.Value = Textos(3)

            Dim att As String = Request("att")
            mbFaltaPComa = False
            If att <> "" AndAlso Right(att, 1) <> ";" Then
                mbFaltaPComa = True
                att = att & ";"
            End If
        End Sub

        ''' <summary>
        ''' Grabar el adjunto y el comentario
        ''' </summary>
        ''' <param name="sender">botón Upload</param>
        ''' <param name="e">evento del botón</param>        
        ''' <returns>Nada</returns>
        ''' <remarks>Llamada desde: sistema (botón Upload); Tiempo máximo:0</remarks>
        Private Sub cmdUpload_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpload.ServerClick
            Dim lOldTimeOut As Long = Server.ScriptTimeout
            Dim sAdjun As String = ""

            Dim ext As String = System.IO.Path.GetExtension(miFile.PostedFile.FileName)
            Dim oAdjun As Fullstep.PMPortalServer.Adjunto = FSPMServer.Get_Adjunto
            If oAdjun.EstaenListaNegra(ext) Then
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>alert('" & Textos(7) & "');</script>")
                oAdjun = Nothing
                Exit Sub
            End If
            oAdjun = Nothing

            If Not getMimeFromFile(miFile.PostedFile, LCase(ext)) Then
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>alert('" & Textos(8) & "');</script>")
                Exit Sub
            End If

            If mbFaltaPComa Then
                sAdjun = ";"
            End If

            sAdjun = sAdjun & System.IO.Path.GetFileName(miFile.PostedFile.FileName)

            If miFile.PostedFile.FileName <> "" Then
                Dim sFich As String = System.IO.Path.GetFileName(miFile.PostedFile.FileName)
                Dim sTemp As String = ConfigurationManager.AppSettings("temp") & "\" & Ruta & "\"

                miFile.PostedFile.SaveAs(sTemp & sFich)

                sAdjun = sAdjun & ";"
            End If

            Server.ScriptTimeout = 1800

            Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>anyadirAdjunto('" + sAdjun + "')</script>")

            Server.ScriptTimeout = lOldTimeOut
        End Sub
    End Class
End Namespace
