Namespace Fullstep.PMPortalWeb
    Partial Class articulosserver
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "
		'This call is required by the Web Form Designer.
		<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents cmdBuscar As System.Web.UI.HtmlControls.HtmlInputButton

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
		Private Const iNumFilas As Integer = 50
		Private bCargarUltAdj As Boolean
        Private bUsar_OrgCompras As Boolean

        ''' Revisado por: blp. Fecha: 24/01/2012
        ''' <summary>
        ''' Cargar la pagina. En el postback usa los filtros indicados para buscar art�culos.
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim i As Integer
            Dim sGMN1 As String
            Dim sGMN2 As String
            Dim sGMN3 As String
            Dim sGMN4 As String
			Dim sMat As String = IIf(String.IsNullOrEmpty(Request("mat")), Request("FamiliaMateriales"), Request("mat"))
			Dim iDesdeFila As Integer
            Dim sCodOrgCompras As String = Nothing
            Dim OrgComprasRO As Boolean
            Dim sCodCentro As String = Nothing
            Dim CentroRO As Boolean

			txtNumFilas.Value = iNumFilas
			txtMatClientID.Value = Request("MatClientId")
			txtArtClientID.Value = Request("ClientId")
			hidArtClientID.Value = Request("idHidControl")
			EsCampoGeneral.Value = If(String.IsNullOrEmpty(Request.QueryString("EsCampoGeneral")), "0", If(Request.QueryString("EsCampoGeneral"), "1", "0"))
			idControlArtCod.Value = Request("IdControlCodArt")
			idControlArtDen.Value = Request("IdControlDenArt")
			hTipoCampoGS.Value = Request("TipoCampoGS")

			ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.BusquedaArticulos

            Dim bMatRO As Boolean = (Request("matro") = "1")
            Dim sRestric As String = Request("restric")
            Dim sInstanciaMoneda As String = Request("InstanciaMoneda")
			If sInstanciaMoneda = "" Then sInstanciaMoneda = "EUR"

			If Request("CargarUltAdj") <> "" Then bCargarUltAdj = Request("CargarUltAdj")

			bUsar_OrgCompras = Not (Request("CodOrgCompras") = "" Or Request("CodCentro") = "") 'Usar Organizacion de compras

			Title = Textos(0)
			lblArticulo.Text = Textos(4)
			lblCod.Text = Textos(2)
			lblDen.Text = Textos(3)
			lblMaterial.Text = Textos(1)
			Submit1.Value = Textos(5)
			cmdAceptar.Value = Textos(6)
			If bUsar_OrgCompras Then
				lblOrgCompras.Text = Textos(7)
				lblCentro.Text = Textos(8)
			End If

            If bUsar_OrgCompras Then
                If Request("txtCodOrgCompras") <> "" Then
                    sCodOrgCompras = Request("txtCodOrgCompras")
                    OrgComprasRO = "0"
                ElseIf Request("CodOrgCompras") <> "" Then  'Valor Inicial
                    sCodOrgCompras = IIf(Request("CodOrgCompras") = "-1", "", Request("CodOrgCompras"))
                    OrgComprasRO = Request("OrgComprasRO") = "1"
                End If

                If Request("txtCodCentro") <> "" Then
                    sCodCentro = Request("txtCodCentro")
                    CentroRO = "0"
                ElseIf Request("CodCentro") <> "" Then
                    sCodCentro = IIf(Request("CodCentro") = "-1", "", Request("CodCentro"))
                    CentroRO = Request("CentroRO") = "1"
                End If
            End If

			Dim sCod As String = Request("cod")
            Dim sDen As String = Request("den")

            If Page.IsPostBack Then
                sMat = Request("txtmaterial$_h")
                sCod = Request("txtCod$_t")
                sDen = Request("txtDen$_t")
                iDesdeFila = Request("txtDesdeFila")
            End If
            Dim sMatOrig As String = sMat

			txtCod.Portal = True
			txtCod.Valor = sCod
			txtDen.Portal = True
			txtDen.Valor = sDen

			Dim oArts As Fullstep.PMPortalServer.Articulos
            Dim lCiaComp As Long = Session("FS_Portal_User").CiaComp
			Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
			Dim arrMat(4) As String
			Dim arrRestric(4) As String
			Dim lLongCod As Integer

            For i = 1 To 4
                arrMat(i) = ""
                arrRestric(4) = ""
            Next i

            i = 1
            While Trim(sMat) <> ""
                Select Case i
                    Case 1
                        lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
                    Case 2
                        lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
                    Case 3
                        lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
                    Case 4
                        lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN4
                End Select
                arrMat(i) = Trim(Mid(sMat, 1, lLongCod))
                sMat = Mid(sMat, lLongCod + 1)
                i = i + 1
            End While
            i = 1
            While Trim(sRestric) <> ""
                Select Case i
                    Case 1
                        lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
                    Case 2
                        lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
                    Case 3
                        lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
                    Case 4
                        lLongCod = FSPMServer.LongitudesDeCodigos.giLongCodGMN4
                End Select
                arrRestric(i) = Trim(Mid(sRestric, 1, lLongCod))
                sRestric = Mid(sRestric, lLongCod + 1)
                i = i + 1
            End While

			sGMN1 = arrRestric(1)
            sGMN2 = arrRestric(2)
            sGMN3 = arrRestric(3)
            sGMN4 = arrRestric(4)

			If sGMN1 = "" Then sGMN1 = arrMat(1)
			If sGMN2 = "" Then sGMN2 = arrMat(2)
			If sGMN3 = "" Then sGMN3 = arrMat(3)
			If sGMN4 = "" Then sGMN4 = arrMat(4)

			Dim oGMN1s As Fullstep.PMportalServer.GruposMatNivel1
            Dim oGMN1 As Fullstep.PMportalServer.GrupoMatNivel1
            Dim oGMN2 As Fullstep.PMportalServer.GrupoMatNivel2
            Dim oGMN3 As Fullstep.PMportalServer.GrupoMatNivel3
            Dim sValor As String = String.Empty

            If sGMN4 <> "" Then
				oGMN3 = FSPMServer.Get_GrupoMatNivel3
				oGMN3.GMN1Cod = sGMN1
				oGMN3.GMN2Cod = sGMN2
				oGMN3.Cod = sGMN3
				oGMN3.CargarTodosLosGruposMatDesde(lCiaComp, 1, Idioma, sGMN4, , True)
				sValor = oGMN3.GruposMatNivel4.Item(sGMN4).Den & " (" & sGMN1 & " - " & sGMN2 & " - " & sGMN3 & " - " & sGMN4 & ")"
				oGMN3 = Nothing
			ElseIf sGMN3 <> "" Then
				oGMN2 = FSPMServer.Get_GrupoMatNivel2
				oGMN2.GMN1Cod = sGMN1
				oGMN2.Cod = sGMN2
				oGMN2.CargarTodosLosGruposMatDesde(lCiaComp, 1, Idioma, sGMN3, , True)
				sValor = oGMN2.GruposMatNivel3.Item(sGMN3).Den & "(" & sGMN1 & " - " & sGMN2 & " - " & sGMN3 & ")"
				oGMN2 = Nothing
			ElseIf sGMN2 <> "" Then
				oGMN1 = FSPMServer.Get_GrupoMatNivel1
				oGMN1.Cod = sGMN1
				oGMN1.CargarTodosLosGruposMatDesde(lCiaComp, 1, Idioma, sGMN2, , True)
				sValor = oGMN1.GruposMatNivel2.Item(sGMN2).Den & "(" & sGMN1 & " - " & sGMN2 & ")"
				oGMN1 = Nothing
			ElseIf sGMN1 <> "" Then
				oGMN1s = FSPMServer.Get_GruposMatNivel1
				oGMN1s.CargarTodosLosGruposMatDesde(lCiaComp, 1, Idioma, sGMN1, , , True)
				sValor = oGMN1s.Item(sGMN1).Den & "(" & sGMN1 & ")"
				oGMN1s = Nothing
			End If

			txtMaterial.Portal = True
			txtMaterial.Restric = Request("restric")
			txtMaterial.Valor = modUtilidades.DBNullToSomething(sMatOrig)
			txtMaterial.Text = sValor
			txtMaterial.ToolTip = Server.HtmlEncode(sValor)
			txtMaterial.ReadOnly = bMatRO
			If bMatRO Then txtMaterial.InputStyle.CssClass = "trasparent"

			oArts = FSPMServer.Get_Articulos

            oArts.GMN1 = sGMN1
            oArts.GMN2 = sGMN2
            oArts.GMN3 = sGMN3
            oArts.GMN4 = sGMN4

            If bUsar_OrgCompras Then
				If OrgComprasRO = True Then
					ugtxtOrganizacionCompras.Visible = False
					Dim oOrganizacionCompras As Fullstep.PMPortalServer.OrganizacionCompras
					oOrganizacionCompras = FSPMServer.Get_OrganizacionCompras
					oOrganizacionCompras.Cod = sCodOrgCompras
					oOrganizacionCompras.LoadData(lCiaComp)
					If oOrganizacionCompras.Data.Tables(0).Rows.Count > 0 Then
						lblOrgComprasNombre.Text = oOrganizacionCompras.Data.Tables(0).Rows(0).Item(0) & " - " & oOrganizacionCompras.Data.Tables(0).Rows(0).Item(1)
					Else
						lblOrgComprasNombre.Text = ""
					End If
					oOrganizacionCompras = Nothing
				Else
					lblOrgComprasNombre.Visible = False
					'Carga el combo de ORGANIZACION DE COMPRAS
					Dim oOrganizacionesCompras As Fullstep.PMPortalServer.OrganizacionesCompras
					oOrganizacionesCompras = FSPMServer.Get_OrganizacionesCompras
					oOrganizacionesCompras.LoadData(lCiaComp)

					Dim ugtxtOrganizacionCompras_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg")
					ugtxtOrganizacionCompras_wdg.DataSource = oOrganizacionesCompras.Data
					ugtxtOrganizacionCompras_wdg.DataBind()

					If Not Page.IsPostBack And sCodOrgCompras <> "-1" Then
						For Each oRow_ As Infragistics.Web.UI.GridControls.GridRecord In ugtxtOrganizacionCompras_wdg.Rows
							If (oRow_.Items(0).Value = sCodOrgCompras) Then
								ugtxtOrganizacionCompras_wdg.Behaviors.Selection.SelectedRows.Add(oRow_)
								ugtxtOrganizacionCompras.Items(0).Text = oRow_.Items(1).Value
								ugtxtOrganizacionCompras.Items(0).Value = oRow_.Items(0).Value
								Exit For
							End If
						Next
					End If
					ugtxtOrganizacionCompras.TextField = "DEN"
					ugtxtOrganizacionCompras.ValueField = "COD"

					oOrganizacionesCompras = Nothing
				End If

				If CentroRO = True Then
                    ugtxtCentros.Visible = False
                    Dim oCentro As Fullstep.PMPortalServer.Centro
                    oCentro = FSPMServer.Get_Centro
                    oCentro.Cod = sCodCentro
                    oCentro.LoadData(lCiaComp)
                    If oCentro.Data.Tables(0).Rows.Count > 0 Then
                        lblCentroNombre.Text = oCentro.Data.Tables(0).Rows(0).Item(0) & " - " & oCentro.Data.Tables(0).Rows(0).Item(1)
                    Else
                        lblCentroNombre.Visible = False
                    End If
                    oCentro = Nothing
                Else
                    lblCentroNombre.Visible = False
                    'Carga el combo de CENTROS
                    Dim oCentros As Fullstep.PMPortalServer.Centros
                    oCentros = FSPMServer.Get_Centros
                    If sCodCentro <> "" And sCodCentro <> "-1" And sCodOrgCompras = "-1" Then
                        oCentros.LoadData(lCiaComp)
                    Else
                        oCentros.LoadData(lCiaComp, sCodOrgCompras)
                    End If

                    Dim ugtxtCentros_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg")
                    ugtxtCentros_wdg.DataSource = oCentros.Data
                    ugtxtCentros_wdg.DataBind()

                    If Not Page.IsPostBack And sCodCentro <> "-1" Then
                        For Each oRow_ As Infragistics.Web.UI.GridControls.GridRecord In ugtxtCentros_wdg.Rows
                            If (oRow_.Items(0).Value = sCodOrgCompras) Then
                                ugtxtCentros_wdg.Behaviors.Selection.SelectedRows.Add(oRow_)
                                ugtxtCentros.Items(0).Text = oRow_.Items(1).Value
                                ugtxtCentros.Items(0).Value = oRow_.Items(0).Value
                                Exit For
                            End If
                        Next
                    End If

                    ugtxtCentros.TextField = "DEN"
                    ugtxtCentros.ValueField = "COD"

                    ugtxtCentros.ClientEvents.ValueChanging = "ugtxtCentros_ValueChanging"

                    oCentros = Nothing
                End If
            Else
                ugtxtOrganizacionCompras.Visible = False
                lblOrgCompras.Visible = False
                lblOrgComprasNombre.Visible = False
                ugtxtCentros.Visible = False
                lblCentro.Visible = False
                lblCentroNombre.Visible = False
            End If

            If Not Page.IsPostBack Or (bUsar_OrgCompras And (Request("txtCodOrgCompras") <> "" Or Request("txtCodCentro") <> "")) Or Not bUsar_OrgCompras Or (bUsar_OrgCompras And (OrgComprasRO = "1" Or CentroRO = "1")) Then
                txtCodOrgCompras.Value = ""
                txtCodCentro.Value = ""

				oArts.LoadData(lCiaComp:=lCiaComp, sCod:=sCod, sDen:=sDen, sPer:=FSPMUser.AprobadorActual, iDesdeFila:=iDesdeFila, iNumFilas:=iNumFilas,
							   sIdioma:=Idioma, sMoneda:=sInstanciaMoneda, bCargarUltAdj:=bCargarUltAdj, sCodOrgCompras:=sCodOrgCompras, sCodCentro:=sCodCentro,
							   sCodPri:=txtCodPri.Value, sCodUlt:=txtCodUlt.Value, CodProve:=If(String.IsNullOrEmpty(idControlArtCod.Value), Nothing, FSPMUser.CodProveGS))

				If oArts.Data.Tables.Count > 0 Then
                    If oArts.Data.Tables(0).Rows.Count > 0 Then
						txtCodUlt.Value = oArts.Data.Tables(0).Rows(oArts.Data.Tables(0).Rows.Count - 1).Item("COD")
						txtCodPri.Value = oArts.Data.Tables(0).Rows(0).Item("COD")
					End If
                End If

				Dim sIdEntry As String = Request("ClientId")
				sIdEntry = Replace(sIdEntry, "__t", "")
				sIdEntry = Replace(sIdEntry, "__", "_")

				wdg_Articulos.DataSource = oArts.Data
				wdg_Articulos.DataKeyFields = "COD"
				wdg_Articulos.DataBind()
				wdg_Articulos.Height = Unit.Pixel(200)

				If iDesdeFila = 0 Then Me.ibFlechaArriba.Visible = False

				If iNumFilas > oArts.Data.Tables(0).Rows.Count Then ibFlechaAbajo.Visible = False

				txtDesdeFila.Value = iDesdeFila
			Else
				wdg_Articulos.Visible = False
				cmdAceptar.Visible = False
				ibFlechaArriba.Visible = False
				ibFlechaAbajo.Visible = False
			End If

			Select Case txtCod.Tipo
                Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoString, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio
                    txtCod.MaxLength = 100
                Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoCorto
                    txtCod.MaxLength = 25
                Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoLargo
                    txtCod.MaxLength = 4000
            End Select

			Select Case txtDen.Tipo
                Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoString, PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio
                    txtDen.MaxLength = 100
                Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoCorto
                    txtDen.MaxLength = 100
                Case PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoLargo
                    txtDen.MaxLength = 4000
            End Select


            Dim sScript As String
			sScript = String.Format(IncludeScriptKeyFormat, "javascript", "var sNombreGrid='" + Replace(Me.wdg_Articulos.ClientID, "_", "x") + "'")
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "ArticulosLlamadaKey", sScript)

            Dim ilGMN1 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN1
            Dim ilGMN2 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN2
            Dim ilGMN3 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN3
            Dim ilGMN4 As Integer = FSPMServer.LongitudesDeCodigos.giLongCodGMN4

            sScript = ""
            sScript += "var ilGMN1 = " + ilGMN1.ToString() + ";"
            sScript += "var ilGMN2 = " + ilGMN2.ToString() + ";"
            sScript += "var ilGMN3 = " + ilGMN3.ToString() + ";"
            sScript += "var ilGMN4 = " + ilGMN4.ToString() + ";"
            sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "LongitudesCodigosKey", sScript)

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TodosNiveles", "<script>var TodosNiveles = " & IIf(CType(Me.Page, FSPMPage).Acceso.gbMaterialVerTodosNiveles, 1, 0) & ";</script>")

            'Nivel Seleccion familia de materiales
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NivelSeleccion", "<script>var NivelSeleccion = " & If(String.IsNullOrEmpty(Request("NivelSeleccion")), 0, Request("NivelSeleccion")) & ";</script>")


        End Sub

        ''' <summary>
        ''' Carga de los centros relacionados con la Organizaci�n
        ''' </summary>
        ''' <param name="sender">combo</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub ugtxtOrganizacionCompras_SelectedRowChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles ugtxtOrganizacionCompras.SelectionChanged
            'Carga el combo de CENTROS
            Dim oCentros As Fullstep.PMPortalServer.Centros
            Dim lCiaComp As Long = Session("FS_Portal_User").CiaComp
            oCentros = FSPMServer.Get_Centros
            oCentros.LoadData(lCiaComp, , ugtxtOrganizacionCompras.SelectedValue)

            Dim ugtxtCentros_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg")
            ugtxtCentros_wdg.DataSource = oCentros.Data
            ugtxtCentros_wdg.DataBind()
            ugtxtCentros.TextField = "DEN"
            ugtxtCentros.ValueField = "COD"

            ugtxtCentros.ClientEvents.ValueChanging = "ugtxtCentros_ValueChanging"

            oCentros = Nothing
        End Sub

        ''' <summary>
        ''' Evento del control webdropdown lanzado desde javascript con la funcion loadItems
        ''' </summary>
        ''' <param name="sender">control webdropdown</param>
        ''' <param name="e">par�metros del evento</param>
        ''' <remarks></remarks>
        Private Sub ugtxtCentros_ItemsRequested(ByVal sender As Object, ByVal e As Infragistics.Web.UI.ListControls.DropDownItemsRequestedEventArgs) Handles ugtxtCentros.ItemsRequested
            Dim oCentros As Fullstep.PMPortalServer.Centros
            Dim lCiaComp As Long = Session("FS_Portal_User").CiaComp
            oCentros = FSPMServer.Get_Centros
            oCentros.LoadData(lCiaComp, , e.Value)

            ugtxtCentros.Items(0).Text = String.Empty
            ugtxtCentros.Items(0).Value = String.Empty

            Dim ugtxtCentros_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg")
            ugtxtCentros_wdg.DataSource = oCentros.Data
            ugtxtCentros_wdg.DataBind()
            AddHandler ugtxtCentros_wdg.PreRender, AddressOf ugtxtCentros_wdg_onPreRender
            ugtxtCentros.TextField = "DEN"
            ugtxtCentros.ValueField = "COD"

            ugtxtCentros.ClientEvents.ValueChanging = "ugtxtCentros_ValueChanging"

            oCentros = Nothing
        End Sub

        ''' <summary>
        ''' Creamos un prerender para deseleccionar el valor seleccionado en el webdatagrid ugtxtCentros_wdg cuando se cambie la selecci�n en ugtxtOrganizacionCompras
        ''' </summary>
        ''' <param name="sender">control que lanza el evento (ugtxtCentros_wdg)</param>
        ''' <param name="e">par�metros del evento</param>
        ''' <remarks>Llamada desde Buscador articulos.aspx.vb. Tiempo m�ximo inferior a 0,2 seg</remarks>
        Private Sub ugtxtCentros_wdg_onPreRender(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ugtxtCentros_wdg As Infragistics.Web.UI.GridControls.WebDataGrid = ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg")
            ugtxtCentros_wdg.Behaviors.Selection.SelectedRows.Clear()
        End Sub

		Private Sub wdg_Articulos_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdg_Articulos.InitializeRow
			For Each column As Infragistics.Web.UI.GridControls.GridRecordItem In e.Row.Items
				e.Row.Items.FindItemByKey(column.Column.Key).Column.Hidden = True
			Next

			e.Row.Items.FindItemByKey("COD").Column.Hidden = False
			e.Row.Items.FindItemByKey("DEN").Column.Hidden = False

			e.Row.Items.FindItemByKey("COD").Column.Width = Unit.Pixel(150)

			If bUsar_OrgCompras Then
				e.Row.Items.FindItemByKey("CODORGCOMPRAS").Column.Hidden = False
				e.Row.Items.FindItemByKey("DENORGCOMPRAS").Column.Hidden = False
				e.Row.Items.FindItemByKey("CODCENTRO").Column.Hidden = False
				e.Row.Items.FindItemByKey("DENCENTRO").Column.Hidden = False
			End If

			If bCargarUltAdj Then
				If e.Row.Items.FindItemByKey("GENERICO").Text = "1" Then
					e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Text = ""
				ElseIf Not e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Value Is Nothing Then
					If Not IsDBNull(e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Value) Then
						e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Text = Replace(e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Text, ".", ",")
						e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Text = FSNLibrary.FormatNumber(e.Row.Items.FindItemByKey("PREC_ULT_ADJ").Text, FSPMUser.NumberFormat)
					End If
				End If
			End If
		End Sub
	End Class
End Namespace
