<%@ Register TagPrefix="ignav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics.WebUI.UltraWebNavigator.v4.3, Version=4.3.20043.1094, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="menu.ascx.vb" Inherits="Fullstep.PMPortalWeb.MenuControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<ignav:ultrawebmenu id="uwmWS" runat="server" ScrollImageBottom="ig_menu_scrolldown.gif" ScrollImageTopDisabled="ig_menu_scrollup_disabled.gif"
	ScrollImageBottomDisabled="ig_menu_scrolldown_disabled.gif" Cursor="Default" ScrollImageTop="ig_menu_scrollup.gif"
	SubMenuImage="ig_menuTri.gif" ItemSpacingTop="0" CssClass="ugMenu" Width="100%">
	<ItemStyle CssClass="ugMenuItem"></ItemStyle>
	<DisabledStyle ForeColor="LightGray"></DisabledStyle>
	<HoverItemStyle Cursor="Default" CssClass="ugMenuItemHover"></HoverItemStyle>
	<IslandStyle Cursor="Default" BorderWidth="1px" Font-Size="8pt" Font-Names="MS Sans Serif" BorderStyle="Outset"
		ForeColor="Black" BackColor="LightGray"></IslandStyle>
	<ExpandEffects ShadowColor="LightGray"></ExpandEffects>
	<SeparatorStyle BackgroundImage="ig_menuSep.gif" CssClass="SeparatorClass" CustomRules="background-repeat:repeat-x; "></SeparatorStyle>
	<Levels>
		<ignav:Level Index="0"></ignav:Level>
	</Levels>
	<Items>
		<ignav:Item Text="Inicio" AccessKey="i"></ignav:Item>
		<ignav:Item Text="Alta de solicitudes" AccessKey="l"></ignav:Item>
		<ignav:Item Text="Seguimiento" AccessKey="s"></ignav:Item>
		<ignav:Item Text="Aprobaci&#243;n" AccessKey="a"></ignav:Item>
		<ignav:Item Text="Cumplimentaci&#243;n" AccessKey="c"></ignav:Item>
		<ignav:Item Text="Opciones" AccessKey="o"></ignav:Item>
	</Items>
</ignav:ultrawebmenu>
