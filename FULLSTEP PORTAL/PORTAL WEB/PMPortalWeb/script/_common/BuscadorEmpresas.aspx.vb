﻿Imports Fullstep
Public Class BuscadorEmpresas
    Inherits FSPMPage

    Private oFsServer As Fullstep.PMPortalServer.Root
    Private lCiaComp As Long

    ''' <summary>
    ''' Evento que carga la pagina
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        oFsServer = Me.FSPMServer
        lCiaComp = Session("FS_Portal_User").CiaComp
        If Not IsPostBack Then
            CargarPaises()
        End If
        CargarTextos()

    End Sub
    ''' <summary>
    ''' Carga las empresas con las opciones de filtro de busqueda y se mete en cache
    ''' </summary>
    ''' <remarks>Llamada desde:DevolverEmpresas()  ; Tiempo máximo:1seg.</remarks>
    Private Sub CargarEmpresas() Handles BusquedaEmpresas.eventBtnBuscarClick

        Dim oEmpresas As PMPortalServer.Empresas

        oEmpresas = oFsServer.Get_Empresas

        Dim dt As DataTable = oEmpresas.ObtenerEmpresas(lCiaComp, Idioma)

        BusquedaEmpresas.Empresas = dt
        BusquedaEmpresas.CargarEmpresas()

        oEmpresas = Nothing

    End Sub

    Private Sub CargarTextos()
        ModuloIdioma = PMPortalServer.TiposDeDatos.ModulosIdiomas.BusquedaEmpresas
        BusquedaEmpresas.Textos = TextosModuloCompleto
    End Sub



    ''' <summary>
    ''' Proceso que carga la propiedad del usercontrol con los paises
    ''' </summary>
    ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,1seg.</remarks>
    Private Sub CargarPaises()
        Dim oPaises As PMPortalServer.Paises
        oPaises = FSPMServer.Get_Paises

        oPaises.LoadData(Idioma, lCiaComp)

        BusquedaEmpresas.Paises = oPaises.Data.Tables(0)

        oPaises = Nothing
    End Sub

    ''' <summary>
    ''' Carga la propiedad del usercontrol con las provincias de un determinado pais
    ''' </summary>
    ''' <param name="sCodPais">Codigo del pais</param>
    ''' <remarks>Llamada desde=wddSubtipo_ItemsRequested; Tiempo máximo=0seg.</remarks>
    Private Sub CargarProvincias(ByVal sCodPais As String) Handles BusquedaEmpresas.eventProvinciasItemRequested
        Dim oProvincias As PMPortalServer.Provincias
        oProvincias = FSPMServer.Get_Provincias

        oProvincias.Pais = sCodPais
        oProvincias.LoadData(Idioma, lCiaComp)

        BusquedaEmpresas.Provincias = oProvincias.Data.Tables(0)

        BusquedaEmpresas.CargarComboProvincias()

        oProvincias = Nothing
    End Sub
End Class