<%@ Page Language="vb" AutoEventWireup="false" Codebehind="usuarios.aspx.vb" Inherits="Fullstep.PMPortalWeb.usuarios"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        <style type="text/css">
            #whdgUsuarios .igg_FullstepControl
            {
                border-width:0px 1px 1px 1px;
            }
        </style>
		<SCRIPT type="text/javascript">

		function whdgUsuarios_RowSelectionChanged(sender, e) {
		    
		    var row = e.getSelectedRows().getItem(0);
		    //var columnClicked = igtbl_getColumnById(cellId)

		    var filaGrid = document.all("FilaGrid").value
		    if (filaGrid == "") {
		        var IDControl = document.all("IDCONTROL").value
		        if (IDControl == "") {
		            cell = row.get_cellByColumnKey("COD"); //c�digo
		            var sUsuCod = cell.get_value()
		            cell = row.get_cellByColumnKey("NOM");   //nombre
		            var sNombre = cell.get_value()
		            cell = row.get_cellByColumnKey("APE");   //apellidos
		            var sApe = cell.get_value()
		            cell = row.get_cellByColumnKey("NOM_DEP"); //nombre de departamento
		            var sDep = cell.get_value()
		            cell = row.get_cellByColumnKey("NOM_UO");  //UON
		            var sUON = cell.get_value()
		            window.opener.SelectedPostBack(row.get_cellByColumnKey("UON1").get_value() + ";" + row.get_cellByColumnKey("UON2").get_value() + ";" + row.get_cellByColumnKey("UON3").get_value() + ";", row.get_cellByColumnKey("DEP").get_value(), sNombre, sApe, sUsuCod)
		            //window.opener.usu_seleccionado_traslado(sUsuCod, sNombre, sApe, sDep,sUON)

		        }
		        else {
		            var cell = row.get_cellByColumnKey("COD");  //c�digo
		            var sUsuCod = cell.get_value()
		            cell = row.get_cellByColumnKey("NOM");   //nombre
		            var sNombre = cell.get_value()
		            cell = row.get_cellByColumnKey("APE");   //apellidos
		            var sApe = cell.get_value()
		            cell = row.get_cellByColumnKey("EMAIL");   //email
		            var sEmail = cell.get_value()
		            window.opener.usu_seleccionado(IDControl, sUsuCod, sNombre + ' ' + sApe, sEmail)
		        }
		    }
		    else {
		        //se llama desde NWrealizarAccion
		        var cell = row.get_cellByColumnKey("COD");  //persona
		        var sUsuCod = cell.get_value()
		        cell = row.get_cellByColumnKey("NOM");   //nombre
		        var sNombre = cell.get_value()
		        cell = row.get_cellByColumnKey("APE");   //Apellido
		        var sApe = cell.get_value()
		        window.opener.usu_seleccionado(sUsuCod, sNombre, sApe, filaGrid)
		    }
		    window.close()
		}

		//''' <summary>
		//''' Bankia RCM-907796 / Informaci�n Confidencial o interna en los mensajes ofrecidos por la aplicaci�n. Si en el navegador pones la ruta te carga la pantalla con usuarios.
		//''' </summary>
		//''' <remarks>Llamada desde: este script ; Tiempo m�ximo: 0</remarks>
		function CtrlApertura() {
		    if (window.opener) {
		    } else {
		        document.location.href = "../../default.aspx";
		    }
		}

		CtrlApertura();
</SCRIPT>
	</head>
	<body MS_POSITIONING="GridLayout">
    <script src="<%=ConfigurationManager.AppSettings("ruta")%>js/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="<%=ConfigurationManager.AppSettings("ruta")%>js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            /*Funciones para paginacion*/

            function AdministrarPaginador() {
                var grid = $find("<%= whdgUsuarios.ClientID %>");
                var parentGrid = grid;
                $('[id$=lblCount]').text(parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount());
                if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() == 0) {
                    $('[id$=ImgBtnFirst]').attr('disabled', 'disabled');
                    $('[id$=ImgBtnFirst]').attr('src', rutaTheme + '/images/primero_desactivado.gif');
                    $('[id$=ImgBtnPrev]').attr('disabled', 'disabled');
                    $('[id$=ImgBtnPrev]').attr('src', rutaTheme + '/images/anterior_desactivado.gif');
                } else {
                    $('[id$=ImgBtnFirst]').removeAttr('disabled');
                    $('[id$=ImgBtnFirst]').attr('src', rutaTheme + '/images/primero.gif');
                    $('[id$=ImgBtnPrev]').removeAttr('disabled');
                    $('[id$=ImgBtnPrev]').attr('src', rutaTheme + '/images/anterior.gif');
                }
                if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() == parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount() - 1) {
                    $('[id$=ImgBtnNext]').attr('disabled', 'disabled');
                    $('[id$=ImgBtnNext]').attr('src', rutaTheme + '/images/siguiente_desactivado.gif');
                    $('[id$=ImgBtnLast]').attr('disabled', 'disabled');
                    $('[id$=ImgBtnLast]').attr('src', rutaTheme + '/images/ultimo_desactivado.gif');
                } else {
                    $('[id$=ImgBtnNext]').removeAttr('disabled');
                    $('[id$=ImgBtnNext]').attr('src', rutaTheme + '/images/siguiente.gif');
                    $('[id$=ImgBtnLast]').removeAttr('disabled');
                    $('[id$=ImgBtnLast]').attr('src', rutaTheme + '/images/ultimo.gif');
                }
            }
            function whdgUsuarios_PageIndexChanged(a, b, c, d) {
                var grid = $find("<%= whdgUsuarios.ClientID %>");
                var parentGrid = grid;
                var dropdownlist = $('[id$=PagerPageList]')[0];

                dropdownlist.options[parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex()].selected = true;
                AdministrarPaginador();
            }

            function IndexChanged() {
                var dropdownlist = $('[id$=PagerPageList]')[0];
                var grid = $find("<%= whdgUsuarios.ClientID %>");
                var parentGrid = grid.get_gridView();
                var newValue = dropdownlist.selectedIndex;
                parentGrid.get_behaviors().get_paging().set_pageIndex(newValue);
                AdministrarPaginador();
            }
            function FirstPage() {
                var grid = $find("<%= whdgUsuarios.ClientID %>");
                var parentGrid = grid;
                parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(0);
            }
            function PrevPage() {
                var grid = $find("<%= whdgUsuarios.ClientID %>");
                var parentGrid = grid;
                var dropdownlist = $('[id$=PagerPageList]')[0];
                if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() > 0) {
                    parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() - 1);
                }
            }
            function NextPage() {
                var grid = $find("<%= whdgUsuarios.ClientID %>");
                var parentGrid = grid;
                var dropdownlist = $('[id$=PagerPageList]')[0];
                if (parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() < parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount() - 1) {
                    parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(parentGrid.get_gridView().get_behaviors().get_paging().get_pageIndex() + 1);
                }
            }
            function LastPage() {
                var grid = $find("<%= whdgUsuarios.ClientID %>");
                var parentGrid = grid;
                parentGrid.get_gridView().get_behaviors().get_paging().set_pageIndex(parentGrid.get_gridView().get_behaviors().get_paging().get_pageCount() - 1);
            }
    </script>      
		<form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="scrMng_usu" runat="server"></asp:ScriptManager>
			<P>
				<asp:Label id="lblTitulo" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server"
					CssClass="subtitulo" Width="584px"></asp:Label></P>
			<P>
            <asp:UpdatePanel ID="UP_whdgUsuarios" runat="server" UpdateMode="Conditional" >
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="hypBuscar" EventName="Click" />
                </Triggers>
                <ContentTemplate >
				<ig:WebHierarchicalDataGrid id="whdgUsuarios" style="Z-INDEX: 105; POSITION: absolute; TOP: 224px;" runat="server"  Width="98%" Height="250px"
                        AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="true">
					<Behaviors>
                        <ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Row" >
                            <SelectionClientEvents RowSelectionChanged= "whdgUsuarios_RowSelectionChanged" />
                        </ig:Selection> 
                        <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true"  AnimationEnabled="true">
                            <FilteringClientEvents DataFiltered="AdministrarPaginador" /> 
                        </ig:Filtering> 
                        <ig:RowSelectors Enabled="false" RowNumbering="false"></ig:RowSelectors>
                        <ig:Paging Enabled="true" PagerAppearance="Top" PageSize="20">
                            <PagingClientEvents PageIndexChanged="whdgUsuarios_PageIndexChanged" />
                            <PagerTemplate>
							    <div class="CabeceraBotones" style="clear:both; float:left; width:100%;"> 
							        <div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
							            <div style="clear: both; float: left; margin-right: 5px;">
                                            <asp:Image ID="ImgBtnFirst" runat="server" OnClientClick="FirstPage()" style="margin-right:2px;" />
                                            <asp:Image ID="ImgBtnPrev" runat="server" OnClientClick="PrevPage()" />
                                        </div>
                                        <div style="float: left; margin-right:5px;">
                                            <asp:Label ID="lblPage" runat="server" Text="dPage" Style="margin-right: 3px;"></asp:Label>
                                            <asp:DropDownList ID="PagerPageList" runat="server" onchange="return IndexChanged()" Style="margin-right: 3px;" />
                                            <asp:Label ID="lblOF" runat="server" Text="dOf" Style="margin-right: 3px;"></asp:Label>
                                            <asp:Label ID="lblCount" runat="server" />
                                        </div>
                                        <div style="float: left;">
                                            <asp:Image ID="ImgBtnNext" runat="server" OnClientClick="NextPage()" style="margin-right:2px;" />
                                            <asp:Image ID="ImgBtnLast" runat="server" OnClientClick="LastPage()" />
                                        </div>
								    </div>   
                                 </div>  
                              </PagerTemplate>
                          </ig:Paging>
                          <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                    </Behaviors>
				</ig:WebHierarchicalDataGrid>
                </ContentTemplate>
            </asp:UpdatePanel>
            </P>
			<asp:Label id="lblResult" style="Z-INDEX: 102; LEFT: 16px; POSITION: absolute; TOP: 144px"
				runat="server" CssClass="parrafo" Width="528px">Resultado de la b�squeda:</asp:Label>
            <INPUT id="IDCONTROL" type="hidden" name="IDCONTROL" runat="server"/>
			<TABLE id="Table1" style="Z-INDEX: 107; LEFT: 112px; WIDTH: 391px; POSITION: absolute; TOP: 40px; HEIGHT: 64px"
				cellSpacing="0" cellPadding="0" width="391" align="center" border="0">
				<TR>
					<TD>
						<asp:Label id="lblUO" runat="server" CssClass="parrafo" Width="100%"></asp:Label></TD>
					<TD>
						<ig:WebDropDown ID="ugtxtUO_" 
                        runat="server" Width="200px" EnableClosingDropDownOnBlur="true" EnableClosingDropDownOnSelect="true" 
                        DropDownContainerWidth="345" CurrentValue="">          
                            <Items>
                                <ig:DropDownItem>
                                </ig:DropDownItem>
                            </Items>
                            <ItemTemplate>
                                <ig:WebDataGrid ID="ugtxtUO_wdg" runat="server"
                                    AutoGenerateColumns="false" Width="100%" ShowHeader="false" EnableAjaxViewState="true" EnableDataViewState="true" OnRowSelectionChanged="ugtxtUO_wdg_RowSelectionChanged" EnableAjax="False">
                                    <Columns>
                                        <ig:BoundDataField DataFieldName="COD_UON1" Key="COD_UON1" Width="0%">
                                        </ig:BoundDataField>
                                        <ig:BoundDataField DataFieldName="COD_UON2" Key="COD_UON2" Width="0%">
                                        </ig:BoundDataField>
                                        <ig:BoundDataField DataFieldName="COD_UON3" Key="COD_UON3" Width="0%">
                                        </ig:BoundDataField>
                                        <ig:BoundDataField DataFieldName="DEN" Key="DEN" Width="100%">
                                        </ig:BoundDataField>
                                    </Columns>
                                    <Behaviors>
                                        <ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Row">
                                            <SelectionClientEvents RowSelectionChanged="WebDataGrid_RowSelectionChanged" />
                                            <AutoPostBackFlags RowSelectionChanged="true" />
                                        </ig:Selection>
                                        <ig:RowSelectors Enabled="false" />
                                    </Behaviors>
                                </ig:WebDataGrid>
                            </ItemTemplate>
                            <ClientEvents />
                        </ig:WebDropDown></TD>
				</TR>
				<TR>
					<TD>
						<asp:Label id="lblDep" runat="server" CssClass="parrafo" Width="100%"></asp:Label></TD>
					<TD>
                        <asp:UpdatePanel ID="UP_Uon" runat="server" UpdateMode="Conditional">
                            <Triggers >
                                <asp:AsyncPostBackTrigger ControlID="ugtxtUO_" EventName="SelectionChanged" />
                            </Triggers>
                            <ContentTemplate>
						        <ig:WebDropDown ID="ugtxtDEP" runat="server" Width="200px" EnableClosingDropDownOnSelect="true"
						        DropDownContainerHeight="200px" DropDownContainerWidth="" DropDownAnimationType="EaseOut" 
						        EnablePaging="False" PageSize="12" CurrentValue="">
							        <ClientEvents SelectionChanged="selectedIndexChanged" />
						        </ig:WebDropDown>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </TD>
				</TR>
				<TR>
					<TD>
						<asp:Label id="lblNombre" runat="server" CssClass="parrafo" Width="100%"></asp:Label></TD>
					<TD>
						<igtxt:WebTextEdit id="ugtxtNombre" runat="server" Width="160px"></igtxt:WebTextEdit></TD>
				</TR>
				<TR>
					<TD>
						<asp:Label id="lblApe" runat="server" Width="100%" CssClass="parrafo"></asp:Label></TD>
					<TD>
						<igtxt:WebTextEdit id="ugtxtApe" runat="server" Width="160px"></igtxt:WebTextEdit></TD>
				</TR>
			</TABLE>
			<TABLE id="Table2" style="Z-INDEX: 106; LEFT: 536px; POSITION: absolute; TOP: 64px; HEIGHT: 27px"
				cellSpacing="0" cellPadding="0" width="88" border="0">
				<TR>
					<TD>
						<asp:LinkButton id="hypBuscar" runat="server" CssClass="parrafo">Buscar</asp:LinkButton></TD>
					<TD>
						<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="images/buscar.gif"></asp:ImageButton></TD>
				</TR>
			</TABLE>
			<HR style="Z-INDEX: 103; POSITION: absolute; TOP: 168px; HEIGHT: 4px"
				width="98%" color="gainsboro" noShade SIZE="4">
			<asp:Label id="lblPulsar" style="Z-INDEX: 104; LEFT: 16px; POSITION: absolute; TOP: 192px"
				runat="server" CssClass="parrafo" Width="672px">Pulse sobre el proveedor para seleccionarlo o haga una nueva b�squeda</asp:Label>
			<INPUT id="FilaGrid" style="Z-INDEX: 108; LEFT: 632px; POSITION: absolute; TOP: 40px" type="hidden"
				name="FilaGrid" runat="server">
		</form>
        <script type="text/javascript">
            // The client event �RowSelectionChanged� takes two parameters sender and e
            // sender  is the object which is raising the event
            // e is the RowSelectionChangedEventArgs

            function WebDataGrid_RowSelectionChanged(sender, e) {

                //Gets the selected rows collection of the WebDataGrid
                var selectedRows = e.getSelectedRows();
                //Gets the row that is selected from the selected rows collection
                var row = selectedRows.getItem(0);
                //Gets the second cell object in the row
                //In this case it is ProductName cell 
                var cell;
                cell = row.get_cell(3);

                //Gets reference to the WebDropDown
                var dropdown = null;
                dropdown = $find("<%= ugtxtUO_.clientID %>");

                //Gets the text in the cell
                var text = cell.get_text();
                if (dropdown != null) {
                    //Sets the text of the value display to the product name of the selected row
                    dropdown.set_currentValue(text, true);
                    dropdown.closeDropDown()
                    dropdown = null;
                }
            }

            function selectedIndexChanged(sender, e) {
                var newSelectedItem = e.getNewSelection()[0];
                var oldSelectedItem = e.getOldSelection()[0];
                if (newSelectedItem != null) {
                    newSelectedItem._element.className = 'igdd_FullstepListItem igdd_FullstepListItemB igdd_FullstepListItemSelected';
                    newSelectedItem = null;
                }
                if (oldSelectedItem != null) {
                    oldSelectedItem._element.className = 'igdd_FullstepListItem igdd_FullstepListItemB';
                    oldSelectedItem = null;
                }
            }
        </script>
	</body>
</html>
