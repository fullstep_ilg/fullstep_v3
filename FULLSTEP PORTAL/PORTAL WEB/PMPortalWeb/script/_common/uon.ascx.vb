Namespace Fullstep.PMPortalWeb
    Partial Class uon
        Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private moUnidades As Fullstep.PMportalServer.UnidadesOrg
        Private mbSeleccionarUON As Boolean




        Property Unidades() As Fullstep.PMportalServer.UnidadesOrg
            Get
                Unidades = moUnidades
            End Get
            Set(ByVal Value As Fullstep.PMportalServer.UnidadesOrg)
                moUnidades = Value
            End Set
        End Property
        Property SeleccionarUon() As Boolean
            Get
                Return mbSeleccionarUON
            End Get
            Set(ByVal Value As Boolean)
                mbSeleccionarUON = Value
            End Set
        End Property

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here

            Me.uwtreeUons.DataSource = moUnidades.Data

            Me.uwtreeUons.DataSource = moUnidades.Data.Tables(0).DefaultView

            Me.uwtreeUons.Levels(0).RelationName = "REL_UON0_UON1"
            Me.uwtreeUons.Levels(0).ColumnName = "DEN"
            Me.uwtreeUons.Levels(1).RelationName = "REL_UON1_UON2"
            Me.uwtreeUons.Levels(1).ColumnName = "UON1DEN"
            Me.uwtreeUons.Levels(1).LevelKeyField = "UON1COD"
            Me.uwtreeUons.Levels(1).LevelImage = "images/carpetaCommodityTxiki.gif"
            Me.uwtreeUons.Levels(2).RelationName = "REL_UON2_UON3"
            Me.uwtreeUons.Levels(2).ColumnName = "UON2DEN"
            Me.uwtreeUons.Levels(2).LevelKeyField = "UON2COD"
            Me.uwtreeUons.Levels(2).LevelImage = "images/carpetaFamilia.gif"
            Me.uwtreeUons.Levels(3).ColumnName = "UON3DEN"
            Me.uwtreeUons.Levels(3).LevelKeyField = "UON3COD"
            Me.uwtreeUons.Levels(3).LevelImage = "images/carpetaGrupo.gif"


            Me.uwtreeUons.DataBind()
            Me.uwtreeUons.DataKeyOnClient = True
            Me.uwtreeUons.ExpandAll()


        End Sub

        ''' <summary>
        '''  Selecciona en el arbol los nodos correspondientes a aprobadores
        ''' </summary>
        ''' <param name="sender">arbol</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub uwtreeUons_NodeBound(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebNavigator.WebTreeNodeEventArgs) Handles uwtreeUons.NodeBound
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")


            Dim oNode As Infragistics.WebUI.UltraWebNavigator.Node
            Dim ds As DataSet
            Dim oRows() As DataRow
            oNode = e.Node
            ds = sender.datasource.dataviewmanager.dataset
            Select Case oNode.Level
                Case 0
                    If ds.Tables(0).Rows(0).Item("EX_PRES") > 0 Then
                        oNode.ImageUrl = "images/MundoConPresupuespuestos.gif"
                        oNode.Tag = 1
                    End If
                    Exit Sub
                Case 1
                    oRows = ds.Tables(oNode.Level).Select("UON1COD= '" & oNode.DataKey & "'")
                Case 2
                    oRows = ds.Tables(oNode.Level).Select("UON1COD= '" & oNode.Parent.DataKey & "' AND UON2COD= '" & oNode.DataKey & "'")
                Case 3
                    oRows = ds.Tables(oNode.Level).Select("UON1COD= '" & oNode.Parent.Parent.DataKey & "' AND UON2COD= '" & oNode.Parent.DataKey & "' AND UON3COD= '" & oNode.DataKey & "'")

            End Select
            If oRows.Length > 0 Then
                If oRows(0).Item("EX_PRES") = 1 Then
                    oNode.ImageUrl = "images/carpetaGrisPres3.gif"
                    oNode.Tag = 1
                    If mbSeleccionarUON Then


                        If oUser.UON1Aprobador <> Nothing Then
                            Dim sScript As String
                            sScript = "seleccionarNodo('" & oNode.GetIdString() & "')"
                            Page.ClientScript.RegisterStartupScript(Me.GetType(),"node" + oNode.DataKey.ToString(), "<script>" + sScript + "</script>")

                        End If
                    End If

                End If
            End If
        End Sub
    End Class
End Namespace