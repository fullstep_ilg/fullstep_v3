<%@ Page Language="vb" AutoEventWireup="false" Codebehind="articulosserver.aspx.vb" Inherits="Fullstep.PMPortalWeb.articulosserver" enableViewState="False" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
	<title></title>
	<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
	<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
	<meta content="JavaScript" name="vs_defaultClientScript"/>
	<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
	<script type="text/javascript">
		//''' Revisado por: blp. Fecha: 20/01/2012
		//''' <summary>
		//''' A�ade los datos (Org. Compras/Centro/material/articulo/denominaci�n) del item seleccionado
		//''' </summary>
		//''' <returns>Nada</returns>
		//''' <remarks>Llamada desde:Desde el boton aceptar de esta pagina; Tiempo m�ximo:0</remarks>	
		function aceptar() {
			p = window.opener;
			var oGrid = $find("wdg_Articulos");
			var oRow = oGrid.get_behaviors().get_selection().get_selectedRows().getItem(0);
			if (!oRow) return;
			var otxtArticulo;
			var sCod = oRow.get_cellByColumnKey("COD").get_value();
			var sDen = oRow.get_cellByColumnKey("DEN").get_value();

			if (p.location.pathname.indexOf('VisorFacturas.aspx') > 0 || p.location.pathname.indexOf('DetalleFactura.aspx') > 0) {				
				//Cambiamos el contenido del Art�culo en la ventana original
				var sidTxtArticulo = document.getElementById("txtArtClientID").value;
				var sidHidArticulo = document.getElementById("hidArtClientID").value;
				var ohidArticulo = p.document.getElementById(sidHidArticulo);
				otxtArticulo = p.document.getElementById(sidTxtArticulo);
				if (otxtArticulo) {
					ohidArticulo.value = sCod;
					otxtArticulo.value = sDen;
				}
			} else { //<%-- Dos comportamientos diferentes: Si el origen de la p�gina es entregasPedidos.aspx o el resto--%>
				if (p.location.pathname.indexOf('entregasPedidos.aspx') > 0) {					
					//Cambiamos el contenido del Art�culo en la ventana original
					var otxtArticulowme = p.$find("txtwmeArticulo");
					otxtArticulo = p.$get("txtArticulo");
					if (otxtArticulo) otxtArticulowme.set_Text(sCod);					
				} else {
					if (p.location.pathname.toLowerCase().indexOf('visorsolicitudes.aspx') > 0) {
						var codArt = p.document.getElementById(document.getElementById("idControlArtCod").value);
						if (codArt) codArt.value = sCod;
						destinoResultado = p.document.getElementById(document.getElementById("idControlArtDen").value);
						switch (parseInt(document.getElementById("hTipoCampoGS").value)) {
							case 104:
							case 119: //codigo articulo
								destinoResultado.value = sCod + (document.getElementById("EsCampoGeneral").value == 0 ? '' : ' - ' + sDen);
								break;
							case 118: //denominacion articulo
								destinoResultado.value = sDen;
								break;
						}
						window.close();
					} else {
						pp = p.opener;
						sDataEntryOrgComprasFORM = null; sDataEntryCentroFORM = null; sCodOrgComprasOculto = ''; sCodCentroOculto = '';
						if (pp) {
							sDataEntryOrgComprasFORM = pp.sDataEntryOrgComprasFORM;
							sDataEntryCentroFORM = pp.sDataEntryCentroFORM;

							if (pp.sCodOrgComprasFORM) sCodOrgComprasOculto = pp.sCodOrgComprasFORM;	
							if (pp.sCodCentroFORM) sCodCentroOculto = pp.sCodCentroFORM;							
						}						

						sGMN1 = oRow.get_cellByColumnKey("GMN1").get_value();
						sGMN2 = oRow.get_cellByColumnKey("GMN2").get_value();
						sGMN3 = oRow.get_cellByColumnKey("GMN3").get_value();
						sGMN4 = oRow.get_cellByColumnKey("GMN4").get_value();
						sCod = oRow.get_cellByColumnKey("COD").get_value();
						sDen = oRow.get_cellByColumnKey("DEN").get_value();
						sGMNDen = oRow.get_cellByColumnKey("DENGMN").get_value();
						bArticuloGenerico = oRow.get_cellByColumnKey("GENERICO").get_value();
						sUnidades = oRow.get_cellByColumnKey("UNI").get_value();
						sDenUnidad = oRow.get_cellByColumnKey("DENUNI").get_value();
						oArt = p.fsGeneralEntry_getById(document.getElementById("txtArtClientID").value);

						if (oArt.idEntryPREC || oArt.idEntryPROV) {
							lPrecioUltADJ = oRow.get_cellByColumnKey("PREC_ULT_ADJ").get_value();
							sCODPROVUltADJ = oRow.get_cellByColumnKey("CODPROV_ULT_ADJ").get_value();
							sDENPROVUltADJ = oRow.get_cellByColumnKey("DENPROV_ULT_ADJ").get_value();
						}
						var sCodOrgCompras = ''; var sDenOrgCompras = '';
						var sCodCentro = ''; var sDenCentro = ''; bUsar_OrgCompras = false;
						if ((oArt.idDataEntryDependent || oArt.OrgComprasDependent.value !== '' || sDataEntryOrgComprasFORM || sCodOrgComprasOculto != '') && (oArt.idDataEntryDependent2 || oArt.CentroDependent.value !== '' || sDataEntryCentroFORM || sCodCentroOculto !== '')) {
							bUsar_OrgCompras = true;
							sCodOrgCompras = oRow.get_cellByColumnKey("CODORGCOMPRAS").get_value();
							sDenOrgCompras = oRow.get_cellByColumnKey("DENORGCOMPRAS").get_value();
							sCodCentro = oRow.get_cellByColumnKey("CODCENTRO").get_value();
							sDenCentro = oRow.get_cellByColumnKey("DENCENTRO").get_value();
						} else
							bUsar_OrgCompras = false;

						oArt.articuloGenerico = bArticuloGenerico;
						if (oArt.tipoGS === 118) {// Denominacion
							oArt.setValue(sDen);
							oArt.setDataValue(sDen);
							oArt.DenArticuloModificado = false;
							oFSEntryCod = null;
							idEntryCod = p.calcularIdDataEntryCelda(document.getElementById("txtArtClientID").value, oArt.nColumna - 1, oArt.nLinea, 1);
							if (idEntryCod)
								oFSEntryCod = p.fsGeneralEntry_getById(idEntryCod);
							if ((oFSEntryCod) && (oFSEntryCod.tipoGS === 119)) {
								oFSEntryCod.articuloGenerico = bArticuloGenerico;
								oFSEntryCod.setValue(sCod);
								oFSEntryCod.setDataValue(sCod);
							} else oArt.codigoArticulo = sCod;
						} else
							if (oArt.tipoGS === 119) { // NuevoCodArt		
								oArt.setValue(sCod);
								oArt.setDataValue(sCod);
								oFSEntryDen = null;
								idEntryDen = p.calcularIdDataEntryCelda(document.getElementById("txtArtClientID").value, oArt.nColumna + 1, oArt.nLinea, 1);
								if (idEntryDen)
									oFSEntryDen = p.fsGeneralEntry_getById(idEntryDen);

								if (oFSEntryDen && oFSEntryDen.tipoGS === 118) {
									oFSEntryDen.setValue(sDen);
									oFSEntryDen.setDataValue(sDen);
									oFSEntryDen.DenArticuloModificado = false;
								} else oArt.tag = sDen;
							} else { //Codigo Articulo (104)	
								oArt.setValue(sCod + " - " + sDen);
								oArt.setDataValue(sCod);
							}

						if ((oArt.idEntryPREC) && (!bArticuloGenerico)) {
							if (lPrecioUltADJ === null)
								lPrecioUltADJ = '';

							if (oArt.idEntryPREC !== '') {
								oPrecio = p.fsGeneralEntry_getById(oArt.idEntryPREC);
								if (oPrecio) {
									oPrecio.setValue(lPrecioUltADJ);
									oPrecio.setDataValue(lPrecioUltADJ);
								}
							}
						}

						sCodProv = document.getElementById("sCodProv").value;
						sDenProv = document.getElementById("sDenProv").value;
						lPrecio = document.getElementById("lPrecio").value;
						if ((oArt.idEntryPROV) && (!bArticuloGenerico)) {
							if (sCODPROVUltADJ === null)
								sCODPROVUltADJ = '';
							if (sDENPROVUltADJ === null)
								sDENPROVUltADJ = '';
							if (oArt.idEntryPROV !== '') {
								oProveedor = p.fsGeneralEntry_getById(oArt.idEntryPROV);
								if (oProveedor)
									if (sCodProv !== '' && lPrecioUltADJ === lPrecio) { //La 2� Parte del AND es para cndo viene de seleccionar una Ultima adjudicacion pero cambia de columna
										oProveedor.setDataValue(sCodProv);
										oProveedor.setValue(sCodProv + " - " + sDenProv);
									} else {
										oProveedor.setDataValue(sCODPROVUltADJ);
										if (sCODPROVUltADJ === '') oProveedor.setValue('');
										else oProveedor.setValue(sCODPROVUltADJ + " - " + sDENPROVUltADJ);
									}
							}
						}

						if (oArt.idEntryUNI)
							if (oArt.idEntryUNI !== '') {
								oUnidad = p.fsGeneralEntry_getById(oArt.idEntryUNI);
								if (oUnidad) {
									oUnidad.setDataValue(sUnidades);
									oUnidad.setValue(sDenUnidad);
								} else {
									oArt.UnidadDependent.value = sUnidades;
								}
							}

						if (bUsar_OrgCompras === true) {
							if (oArt.idDataEntryDependent) {
								oOrganizacionCompras = p.fsGeneralEntry_getById(oArt.idDataEntryDependent);

								if (oOrganizacionCompras && oOrganizacionCompras.tipoGS === 123) {
									oOrganizacionCompras.setDataValue(sCodOrgCompras);
									oOrganizacionCompras.setValue(sCodOrgCompras + ' - ' + sDenOrgCompras);
								}
							} else {
								if (sDataEntryOrgComprasFORM) {
									oOrganizacionCompras = pp.fsGeneralEntry_getById(sDataEntryOrgComprasFORM);
									if (oOrganizacionCompras && oOrganizacionCompras.tipoGS === 123) {
										oOrganizacionCompras.setDataValue(sCodOrgCompras);
										oOrganizacionCompras.setValue(sCodOrgCompras + ' - ' + sDenOrgCompras);
									}
								}
							}

							if (oArt.idDataEntryDependent2 || sDataEntryCentroFORM) {
								if (oArt.idDataEntryDependent2) oCentro = p.fsGeneralEntry_getById(oArt.idDataEntryDependent2);
								else oCentro = pp.fsGeneralEntry_getById(sDataEntryCentroFORM);

								if (oCentro && oCentro.tipoGS == 124 && sCodCentro) {
									oCentro.setDataValue(sCodCentro);
									oCentro.setValue(sCodCentro + " - " + sDenCentro);
								}
							}

						}
						var s;
						s = "";
						for (i = 0; i < ilGMN1 - sGMN1.length; i++)
							s += " ";

						sMat = s + sGMN1;
						if (sGMN2) {
							s = "";
							for (i = 0; i < ilGMN2 - sGMN2.length; i++)
								s += " ";
							sMat = sMat + s + sGMN2;
						}

						if (sGMN3) {
							s = "";
							for (i = 0; i < ilGMN3 - sGMN3.length; i++)
								s += " ";
							sMat = sMat + s + sGMN3;
						}

						if (sGMN4) {
							s = "";
							for (i = 0; i < ilGMN4 - sGMN4.length; i++)
								s += " ";
							sMat = sMat + s + sGMN4;
						}

						oArt.Dependent.value = sMat;
						if (oArt.tipoGS === 119) { // NuevoCodArt
							if (oFSEntryDen && oFSEntryDen.tipoGS === 118) {
								oFSEntryDen.Dependent.value = sMat;
							}
						}
						
						p.mat_seleccionado(document.getElementById("txtMatClientID").value, sGMN1, sGMN2, sGMN3, sGMN4, 4, sGMNDen, TodosNiveles);
					}					
				}
			}
			window.close();
		}			
	</script>
	<style type="text/css">
		.SelectedRow {
			color: White;
			background-color: Blue;
		}

		#up_Articulos {
			height: 99%;
			width: 99%;
		}

		#wdg_Articulos {
			background-image: none;
		}

		.Articulos_body {
			cursor: pointer;
		}
	</style>
	</head>
	<body>
        <script src="../_common/js/jsAlta.js?v=<%=ConfigurationManager.AppSettings("versionJs")%>" type="text/javascript"></script>
		<form id="frmArticulos" name="frmArticulos" method="post" runat="server">
            <asp:ScriptManager ID="scrMng_presAsig" runat="server"></asp:ScriptManager>
			<input id="txtDesdeFila" type="hidden" name="txtDesdeFila" runat="server"/> 
			<input id="txtNumFilas" type="hidden" name="txtNumFilas" runat="server"/>
			<input id="txtArtClientID" type="hidden" name="txtArtClientID" runat="server"/> 
			<input id="txtMatClientID" type="hidden" name="txtMatClientID" runat="server"/>
			<input id="lPrecio" type="hidden" name="lPrecio"/>
			<input id="sCodProv" type="hidden" name="sCodProv"/>
            <input id="hidArtClientID" type="hidden" runat="server" />
			<input id="sDenProv" type="hidden" name="sDenProv"/> 
			<input id="txtCodOrgCompras" type="hidden" name="txtCodOrgCompras" runat="server"/>
			<input id="txtCodCentro" type="hidden" name="txtCodCentro" runat="server"/> 
			<input id="txtCodPri" type="hidden" name="txtCodPri" runat="server"/>
			<input id="txtCodUlt" type="hidden" name="txtCodUlt" runat="server"/>
			<input id="EsCampoGeneral" type="hidden" name="desde" runat="server" />
			<input id="idControlArtCod" type="hidden" name="idControlArtCod" runat="server" />
			<input id="idControlArtDen" type="hidden" name="idControlArtDen" runat="server" />
			<input id="hTipoCampoGS" type="hidden" name="hTipoCampoGS" runat="server" />
			<div style="margin-top:0.5em;">
				<asp:Label id="lblOrgCompras" runat="server" CssClass="Negrita Texto10" style="display:inline-block; width:50%;">DOrg</asp:Label>
				<asp:Label id="lblCentro" runat="server" CssClass="Negrita Texto10" style="display:inline-block;">DCe</asp:Label>
			</div>
			<div style="display:inline-block; width:50%;">
				<asp:Label id="lblOrgComprasNombre" runat="server" CssClass="parrafo"></asp:Label>
				<ig:WebDropDown ID="ugtxtOrganizacionCompras" runat="server" Width="100%"
					EnableClosingDropDownOnBlur="true" 
					EnableClosingDropDownOnSelect="true"
					DropDownContainerWidth="100%">
                    <Items>
                        <ig:DropDownItem></ig:DropDownItem>
                    </Items>
                    <ItemTemplate>
                        <ig:WebDataGrid ID="ugtxtOrganizacionCompras_wdg" runat="server"
                            AutoGenerateColumns="false" Width="100%" ShowHeader="false">
                            <Columns>
                                <ig:BoundDataField DataFieldName="COD" Key="COD" Width="20%"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="DEN" Key="DEN" Width="80%"></ig:BoundDataField>
                            </Columns>
                            <Behaviors>
                                <ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Row">
                                    <SelectionClientEvents RowSelectionChanged="WebDataGrid_RowSelectionChanged" />
                                </ig:Selection>
                            </Behaviors>
                        </ig:WebDataGrid>
                    </ItemTemplate>
                </ig:WebDropDown>                
			</div>
			<div style="display:inline-block; width:49%;">
				<asp:Label id="lblCentroNombre" runat="server" CssClass="parrafo"></asp:Label>
				<ig:WebDropDown ID="ugtxtCentros" runat="server" Width="100%" 
					EnableClosingDropDownOnBlur="true" 
					EnableClosingDropDownOnSelect="true"
					DropDownContainerWidth="100%">
                    <Items>
                        <ig:DropDownItem></ig:DropDownItem>
                    </Items>
                    <ItemTemplate>
                        <ig:WebDataGrid ID="ugtxtCentros_wdg" runat="server"
                            AutoGenerateColumns="false" Width="100%" ShowHeader="false">
                            <Columns>
                                <ig:BoundDataField DataFieldName="COD" Key="COD" Width="20%"></ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="DEN" Key="DEN" Width="80%"></ig:BoundDataField>
                            </Columns>
                            <Behaviors>
                                <ig:Selection RowSelectType="Single" Enabled="True" CellClickAction="Row">
                                    <SelectionClientEvents RowSelectionChanged="WebDataGrid_RowSelectionChanged" />
                                </ig:Selection>
                            </Behaviors>
                        </ig:WebDataGrid>
                    </ItemTemplate>
                </ig:WebDropDown>                
			</div>
			<div>
				<asp:Label id="lblMaterial" runat="server" CssClass="Negrita Texto10" style="display:block;">Material</asp:Label>
				<fsde:generalentry id="txtMaterial" runat="server" Width="100%" Tag="Material" Tipo="TipoString" TipoGS="Material">
					<InputStyle Width="100%" CssClass="TipoTextoMedio CajaTexto"></InputStyle>
				</fsde:generalentry>
			</div>
			<div>
				<asp:Label id="lblCod" runat="server" CssClass="Negrita Texto10" style="display:inline-block; width:20%;">C�digo</asp:Label>
				<asp:Label id="lblDen" runat="server" CssClass="Negrita Texto10">Denominaci�n</asp:Label>
			</div>
			<div style="display:inline-block; width:20%;">
				<fsde:generalentry id="txtCod" runat="server" Width="100%" Tag="COD" Tipo="TipoTextoCorto">
					<InputStyle CssClass="TipoTextoMedio CajaTexto"></InputStyle>
				</fsde:generalentry>
			</div>
			<div style="display:inline-block; width:79%;">
				<fsde:generalentry id="txtDen" runat="server" Width="100%" Tag="DEN" Tipo="TipoTextoCorto">
					<InputStyle CssClass="TipoTextoMedio CajaTexto"></InputStyle>
				</fsde:generalentry>
			</div>
			<div style="line-height:20px; margin-top:0.5em;">
				<asp:Label id="lblArticulo" runat="server" CssClass="Negrita Texto10" style="display:inline;">Art�culo</asp:Label>
				<div style="display:inline-block; position:absolute; right:0.5em;">
					<input class="boton" id="Submit1" onclick="buscar()" type="submit" value="Buscar" name="cmdBuscar" runat="server"/>
				</div>
			</div>
			<div style="display:inline-block; width:100%; margin-top:0.5em; vertical-align:top;">
				<ig:WebDataGrid ID="wdg_Articulos" runat="server" Height="100%" Width="100%" 
					AutoGenerateColumns="true"
					ShowFooter="false" 
					BackColor="White" 
					HeaderCaptionCssClass="parrafo" 
					BorderStyle="Solid" 
					DataKeyFields="VALORASOCIADO" 
					BorderWidth="1px" 
					ShowHeader="false" 
					ItemCssClass="Articulos_body" >                                
					<Behaviors>
						<ig:Selection  RowSelectType="Single" Enabled="True" CellClickAction="Row" SelectedCellCssClass="SelectedRow"></ig:Selection>    
					</Behaviors>                                
				</ig:WebDataGrid>
			</div>
			<div style="display:inline-block; width:8%; margin-top:0.5em; padding-left:0.5em;">
				<asp:ImageButton ID="ibFlechaArriba" ImageUrl="images/flechaarriba.gif" runat="server" onClientClick="arriba()" style="display:block; margin-bottom:0.3em;"></asp:ImageButton>
				<asp:ImageButton ID="ibFlechaAbajo" ImageUrl="images/flechaabajo.gif" runat="server" onClientClick="abajo()" style="display:block;"></asp:ImageButton>
			</div>
			<div style="text-align:center;">
				<input class="boton" id="cmdAceptar" onclick="aceptar()" type="button" value="Aceptar" name="cmdAceptar" runat="server"/>
			</div>
		</form>
        <script type="text/javascript">
            // The client event ‘RowSelectionChanged’ takes two parameters sender and e
            // sender  is the object which is raising the event
            // e is the RowSelectionChangedEventArgs
            function WebDataGrid_RowSelectionChanged(sender, e) {
                //Gets the selected rows collection of the WebDataGrid
                var selectedRows = e.getSelectedRows();
                //Gets the row that is selected from the selected rows collection
                var row = selectedRows.getItem(0);
                //Gets the second cell object in the row
                //In this case it is ProductName cell 
                var cell = row.get_cell(1);
                //Gets the text in the cell
                var text = cell.get_text();

                //Gets reference to the WebDropDown
                var dropdown = null;
                if (sender._id.indexOf('<%= ugtxtOrganizacionCompras.clientID %>', 0) != -1) {
                    dropdown = $find("<%= ugtxtOrganizacionCompras.clientID %>");
                }
                else if (sender._id.indexOf('<%= ugtxtCentros.clientID %>', 0) != -1) {
                    dropdown = $find("<%= ugtxtCentros.clientID %>");
                }
                if (dropdown != null) {
                    //Sets the text of the value display to the product name of the selected row
                    dropdown.set_currentValue(text, true);

                    dropdown.closeDropDown()

                    if (sender._id.indexOf('<%= ugtxtOrganizacionCompras.clientID %>', 0) != -1) {
                        var CodCell = row.get_cell(0);
                        //Gets the text in the cell
                        var Cod = CodCell.get_text();
                        ugtxtOrganizacionComprasSelectionChanged(Cod);
                    }
                }
            }
            function ugtxtOrganizacionComprasValueChanging(sender, e) {
                e.set_cancel(true);
            }
            // The client event ‘ValueChanging' takes two parameters sender and e
            // sender  is the object which is raising the event
            // e is the DropDownEditEventArgs
            function ugtxtCentros_ValueChanging(sender, e) {
                e.set_cancel(true);
            }
            function ugtxtOrganizacionComprasSelectionChanged(cod) {
                var ugtxtCentros = $find("<%= ugtxtCentros.clientID %>");
                //Vaciar texto
                ugtxtCentros.set_currentValue('', true);
                //Cargar nuevos datos
                ugtxtCentros.loadItems(cod);
            }
            function buscar() {
                document.getElementById("txtDesdeFila").value = 0
                if (document.getElementById("txtCodPri"))
                    document.getElementById("txtCodPri").value = ""
                if (document.getElementById("txtCodUlt"))
                    document.getElementById("txtCodUlt").value = ""

                try {
                    if ($find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0) != null) {
                        var oWdg_Value = $find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cell(0).get_text();
                    }
                    else {
                        oWdg_Value = devolverValueCombos(1)
                    }
                    document.getElementById("txtCodOrgCompras").value = oWdg_Value;
                } catch (err) { }
                try {
                    if ($find('<%= ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0) != null) {
                        oWdg_Value = $find('<%= ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cell(0).get_text();
                    }
                    else {
                        oWdg_Value = devolverValueCombos(2)
                    }
                    document.getElementById("txtCodCentro").value = oWdg_Value;
                } catch (err) { }
            }
            function arriba() {
                var iDesdeFila = parseFloat(document.getElementById("txtDesdeFila").value)

                var iNumFilas = parseFloat(document.getElementById("txtNumFilas").value)

                if (iDesdeFila - iNumFilas < 0)
                    iDesdeFila = 0
                else
                    iDesdeFila -= iNumFilas

                document.getElementById("txtDesdeFila").value = iDesdeFila
                if (document.getElementById("txtCodUlt"))
                    document.getElementById("txtCodUlt").value = ""

                try {
                    if ($find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0) != null) {
                        var oWdg_Value = $find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cell(0).get_text();
                    }
                    else {
                        oWdg_Value = devolverValueCombos(1);
                    }
                    document.getElementById("txtCodOrgCompras").value = oWdg_Value;
                } catch (err) { }
                try {
                    if ($find('<%= ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0) != null) {
                        oWdg_Value = $find('<%= ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cell(0).get_text();
                    }
                    else {
                        oWdg_Value = devolverValueCombos(2)
                    }
                    document.getElementById("txtCodCentro").value = oWdg_Value;
                } catch (err) { }
            }

            //funcion que devuelve el value de los combos en funcion del texto que tenga en el input en ese momento
            //tipoCombo: 1 si es el de organizacion de compra, 2 si es el de centro
            function devolverValueCombos(tipoCombo) {
                var cmb;
                var gridCmb;
                if (tipoCombo == 1) {
                    var cmb = $find('<%=ugtxtOrganizacionCompras.clientID %>')
                    var gridCmb = $find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>')
                } else {
                    var cmb = $find('<%=ugtxtCentros.clientID %>')
                    var gridCmb = $find('<%= ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg").clientID %>')
                }
                var CurrentText=cmb.get_currentValue() //Recogemos el texto actual del input del combo
                var cmbItemText;
                for (i = 0; i <= gridCmb.get_rows().get_length() - 1; i++) {
                    cmbItemText = gridCmb.get_rows().get_row(i).get_cellByColumnKey("DEN").get_value()
                    if (cmbItemText == CurrentText) {
                        return gridCmb.get_rows().get_row(i).get_cellByColumnKey("COD").get_value()
                    }
                }
                return ''
            }
            function abajo() {
                var iDesdeFila = parseFloat(document.getElementById("txtDesdeFila").value)

                var iNumFilas = parseFloat(document.getElementById("txtNumFilas").value)

                iDesdeFila += iNumFilas

                document.getElementById("txtDesdeFila").value = iDesdeFila
                if (document.getElementById("txtCodPri"))
                    document.getElementById("txtCodPri").value = ""
                try {
                    if ($find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0) != null) {
                        var oWdg_Value = $find('<%= ugtxtOrganizacionCompras.Controls(0).Controls(0).FindControl("ugtxtOrganizacionCompras_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cell(0).get_text();
                    }
                    else {
                        oWdg_Value = devolverValueCombos(1)
                    }
                    document.getElementById("txtCodOrgCompras").value = oWdg_Value;
                } catch (err) { }
                try {
                    if ($find('<%= ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0) != null) {
                        oWdg_Value = $find('<%= ugtxtCentros.Controls(0).Controls(0).FindControl("ugtxtCentros_wdg").clientID %>').get_behaviors().get_selection().get_selectedRows(0).getItem(0).get_cell(0).get_text();
                    }
                    else {
                        oWdg_Value = devolverValueCombos(2);
                    }
                    document.getElementById("txtCodCentro").value = oWdg_Value;
                } catch (err) { }
            }
        </script>
	</body>
</html>
