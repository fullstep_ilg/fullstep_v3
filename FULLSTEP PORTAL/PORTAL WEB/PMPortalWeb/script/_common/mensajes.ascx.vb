Namespace Fullstep.PMPortalWeb
    Partial Class mensajesControl
        Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents cmdAnyadir As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents tblDesglose As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents tblDesgloseHidden As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents tblGeneral As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents divDesglose As System.Web.UI.HtmlControls.HtmlGenericControl

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private mlId As Long
        Private msTabContainer As String

        Private mlNoConformidad As Long
        Private mlInstancia As Long
        Private mlVersion As Long
        Private moTextos As DataTable

        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script language=""{0}"">{1}</script>"


        Property NoConformidad() As Long
            Get
                Return mlNoConformidad
            End Get
            Set(ByVal Value As Long)
                mlNoConformidad = Value
            End Set
        End Property

        Property Instancia() As Long
            Get
                Return mlInstancia
            End Get
            Set(ByVal Value As Long)
                mlInstancia = Value
            End Set
        End Property
        Property Version() As Long
            Get
                Return mlVersion
            End Get
            Set(ByVal Value As Long)
                mlVersion = Value
            End Set
        End Property


        Property TabContainer() As String
            Get
                Return msTabContainer
            End Get
            Set(ByVal Value As String)
                msTabContainer = Value
            End Set
        End Property

        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            If Page.IsPostBack Then
                Exit Sub
            End If

            Dim FSPMServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User
            Dim lCiaComp As Long = Session("FS_Portal_User").CiaComp


            Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
            Dim sScript As String


            Dim sIdi As String = Session("FS_Portal_User").Idioma.ToString
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If
            oUser = Session("FS_Portal_User")



            Dim oDict As Fullstep.PMPORTALServer.Dictionary = FSPMServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DesgloseControl, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)


            If mlInstancia = Nothing Then
                mlInstancia = Request("Instancia")
            End If

            If mlNoConformidad = Nothing Then
                mlNoConformidad = Request("NoConformidad")
            End If

            Dim oNoConformidad As Fullstep.PMPortalServer.NoConformidad


            oNoConformidad.Id = mlNoConformidad
            oNoConformidad.Load(lCiaComp, sIdi)








        End Sub

    End Class
End Namespace