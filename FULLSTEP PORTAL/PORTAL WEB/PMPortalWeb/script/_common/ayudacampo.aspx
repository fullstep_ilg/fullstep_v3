﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AyudaCampo.aspx.vb" Inherits="Fullstep.PMPortalWeb.AyudaCampo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css" >
    .CapaTituloPanelInfo
    {
	    background-color:#282828;
    }
    .CapaSubTituloPanelInfo
    {
	    background-color:#424242;	 
    }
    .TituloPopUpPanelInfo
    {
	    font-family:Arial;
	    font-size:16px;
	    font-weight:bold;
	    color:#FFFFFF;
    }
    .SubTituloPopUpPanelInfo
    {
	    font-family:Arial;
	    font-size:12px;
	    font-weight:bold;
	    color:#FFFFFF;
    }
    </style>
</head>
<body style="background-color: #FFFFFF;margin-top: 0px; margin-left: 0px" onload="window.focus();">
    <form id="form1" runat="server">
		<asp:Image ID="imgCalendar" runat="server" ImageUrl="~/script/_common/images/calendar.png" style="position:absolute; top:5px;left:5px; max-width:70px;"/>
		<div style='min-height: 35px;' class='CapaTituloPanelInfo'>
			<asp:Label ID="lblTitulo" runat="server" CssClass="TituloPopUpPanelInfo" style="display:inline-block;padding-top:5px;margin-left:85px;"></asp:Label>
		</div>
		<div style='min-height: 25px;' class='CapaSubTituloPanelInfo'>
			<asp:Label ID="lblSubtitulo" runat="server" CssClass="SubTituloPopUpPanelInfo" style="display:inline-block;padding-top:5px;margin-left:85px;"></asp:Label>
		</div>
		
		<asp:Label ID="lblAyuda" runat="server" style="display:inline-block; margin:1em;"></asp:Label>
		<hr id="hrTop" visible="false" runat="server" />
		<asp:Label ID="lblFormulaCalculo" runat="server" CssClass="Etiqueta" style="display:inline-block; margin:0em 1em;"></asp:Label>
		<asp:Label ID="lblFormula" runat="server"></asp:Label>
		<hr id="hrBottom" visible="false" runat="server" />
    </form>
</body>
</html>

