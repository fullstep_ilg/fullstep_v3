<%@ Page Language="vb" AutoEventWireup="false" Codebehind="detallesolicitudpadre.aspx.vb" Inherits="Fullstep.PMWeb.detallesolicitudpadre" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblDetalle" style="Z-INDEX: 101; LEFT: 8px; WIDTH: 520px; POSITION: absolute; TOP: 8px; HEIGHT: 173px"
				cellSpacing="1" cellPadding="1" border="0">
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 30%; HEIGHT: 1px"><asp:label id="lblIdentificador" runat="server" Width="100%">DIdentificador</asp:label></TD>
					<TD class="ugfilatablaHist" style="WIDTH: 70%; HEIGHT: 1px"><asp:label id="txtIdentificador" runat="server" Width="100%">txtIdentificador</asp:label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 30%; HEIGHT: 1px"><asp:label id="lblTitulo" runat="server" Width="100%">DTitulo</asp:label></TD>
					<TD class="ugfilatablaHist" style="WIDTH: 70%; HEIGHT: 1px"><asp:label id="txtTitulo" runat="server" Width="100%">txtTitulo</asp:label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 30%; HEIGHT: 1px"><asp:label id="lblImporteAprob" runat="server" Width="100%">DImporte aprobado</asp:label></TD>
					<TD class="ugfilatablaHist" style="WIDTH: 70%; HEIGHT: 1px"><asp:label id="txtImporteAprob" runat="server" Width="100%">txtImporteAprob</asp:label></TD>
				</TR>
				<TR>
					<TD class="ugfilatablaCabecera" style="WIDTH: 30%; HEIGHT: 1px"><asp:label id="lblImporteAcum" runat="server" Width="100%">DImporte acumulado</asp:label></TD>
					<TD class="ugfilatablaHist" style="WIDTH: 70%; HEIGHT: 1px"><asp:label id="txtImporteAcum" runat="server" Width="100%">txtImporteAcum</asp:label></TD>
				</TR>
				<tr>
					<td style="HEIGHT: 1px" colSpan="2"></td>
				</tr>
				<TR>
					<TD style="HEIGHT: 61px" vAlign="top" align="center" colSpan="2"><INPUT class="boton" id="cmdCerrar" onclick="javascript:window.close()" type="button" value="Cerrar"
							name="cmdCerrar" runat="server"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</html>
