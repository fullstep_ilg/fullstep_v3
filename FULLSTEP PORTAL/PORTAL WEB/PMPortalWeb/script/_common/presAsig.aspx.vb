Namespace Fullstep.PMPortalWeb
    Partial Class presAsig
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script language=""{0}"">{1}</script>"

        Private bMostrarArbolyGrid As Boolean = False
        Private cadenaerrormultiplesUO1 As String
        Private cadenaerrormultiplesUO2 As String
        Private DS As DataSet
        Private oDGElement As DataTable
        Private dtNewRow As DataRow
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Me.Page.ClientScript.RegisterClientScriptResource(GetType(Fullstep.DataEntry.GeneralEntry), "Fullstep.DataEntry.formatos.js")
            'Put user code to initialize the page here
            Dim FSWSServer As Fullstep.PMPortalServer.Root = FSPMServer
            Dim oUser As Fullstep.PMPortalServer.User = FSPMUser
            Dim lCiaComp As Long = IdCiaComp
            Dim sIdi As String = Idioma
            Dim lInstancia As Long
            Dim lSolicitud As Long
            Dim iTipo As Integer ' tipo de prespuesto
            Dim mcadenapresupuestos As String 'cadena que guarda la descripcion del tipo de presupuesto
            Dim CabeceraDesglose As String


            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Presupuestos

            Dim cadenaunidadorganizativa = Textos(17)
            Dim cadenaerror100 As String = Textos(26)
            Dim cadenaerrorfiltrado As String = Textos(27)
            Dim cadenaerrorsinpresupuestos As String = Textos(28)
            cadenaerrormultiplesUO1 = Textos(29)
            cadenaerrormultiplesUO2 = Textos(30)

            Dim sScriptErrores
            sScriptErrores += "var cadenaunidadorganizativa = '" + cadenaunidadorganizativa + "';"
            sScriptErrores += "var cadenaerror100 = '" + cadenaerror100 + "';"
            sScriptErrores += "var cadenaerrorfiltrado = '" + cadenaerrorfiltrado + "';"
            sScriptErrores += "var cadenaerrorsinpresupuestos = '" + cadenaerrorsinpresupuestos + "';"

            sScriptErrores = String.Format(IncludeScriptKeyFormat, "javascript", sScriptErrores)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "scripterrores", sScriptErrores)

            Me.lblUnidadOrganizativa.Text = Textos(17)
            Me.lblAnyo.Text = Textos(7)
            Me.lblAsignado.Text = Textos(11)
            Me.lblBuscar.Text = Textos(18)
            Me.lblCodigo.Text = Textos(19)
            Me.lblDenominacion.Text = Textos(20)
            Me.lblPendiente.Text = Textos(12)
            Me.lblResultado.Text = Textos(21)
            Me.lblSeleccionaArbol.Text = Textos(22)
            Me.cmdCambiar.Value = Textos(23)
            Me.cmdAceptar.Value = Textos(2)
            Me.cmdCancelar.Value = Textos(3)


            Dim oCampo As Fullstep.PMPortalServer.Campo
            Dim oUnidades As Fullstep.PMPortalServer.UnidadesOrg
            Dim oUnidadesCadenaPresupuesto As Fullstep.PMPortalServer.UnidadesOrg 'La utilizo para obtener la cadena de BD del tipo de presupuesto seleccionado

            Dim idCampo As Integer = CInt(Request("Campo"))

            If Request("Solicitud") <> Nothing Then
                lSolicitud = Request("Solicitud")
            End If

            If Request("Instancia") <> Nothing Then
                lInstancia = Request("Instancia")
            End If

            oCampo = FSWSServer.Get_Campo()
            oCampo.Id = idCampo

            If lInstancia > 0 Then
                oCampo.LoadInst(lCiaComp, lInstancia, sIdi)
            Else
                oCampo.Load(lCiaComp, sIdi, lSolicitud)
            End If

            Select Case oCampo.IdTipoCampoGS
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.PRES1
                    iTipo = 1
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Pres2
                    iTipo = 2
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Pres3
                    iTipo = 3
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Pres4
                    iTipo = 4
            End Select

            'Como ya se el tipo del presupuesto voy a obtener la cadena asociada a dicho tipo de presupuesto
            oUnidadesCadenaPresupuesto = FSWSServer.get_UnidadesOrg()
            oUnidadesCadenaPresupuesto.CargarCadenaPresupuesto(lCiaComp, iTipo, sIdi)
            mcadenapresupuestos = oUnidadesCadenaPresupuesto.Data.Tables(0).Rows(0).Item(0)

            Dim sScriptTitulo

            sScriptTitulo += "var cadenatitulo = '" + Textos(24) + " " + mcadenapresupuestos + "';"
            sScriptTitulo = String.Format(IncludeScriptKeyFormat, "javascript", sScriptTitulo)
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "scripttitulo", sScriptTitulo)

            Me.lbltitulo.Text = Textos(24) + " " + mcadenapresupuestos
            Dim anyo As Integer = 0
            Me.TIPOPRESUPUESTO.Value = iTipo 'Al campo oculto de mi formulario le pongo el tipo del presupuesto

            ''Los presupuestos de tipo 1 y 2 son anuales por lo tanto tienen campo de a�o
            If (iTipo = 1) Or (iTipo = 2) Then
                Me.ugtxtAnyo.Visible = True
                Me.lblAnyo.Visible = True
                Dim rightNow As DateTime = DateTime.Now
                Dim s As String

                s = rightNow.ToString()
                anyo = rightNow.Year()

                Me.ugtxtAnyo.NullText = anyo

                If (Request.Form("Anyo") <> Nothing) Then
                    Me.ugtxtAnyo.Value = Request.Form("Anyo")
                    anyo = Int32.Parse(Request.Form("Anyo").ToString)
                Else
                    Me.ugtxtAnyo.Value = Numero(anyo, "", ".")
                End If

            Else
                'Aqui se deberian de ocultar
                Me.ugtxtAnyo.Visible = False
                Me.lblAnyo.Visible = False
                Me.ugtxtAnyo.ValueText = 0
                anyo = 0
            End If

            Dim idControl = Request("IDPres")
            Me.IDCONTROL.Value = idControl


            Dim sUon1 As String
            Dim sUon2 As String
            Dim sUon3 As String
            Dim Valor As String 'Campo que recoge los presupuestos asignados.

            Dim sScript As String = ""





            If Page.IsPostBack = True Then
                Valor = Request.Form("Valor")
                Me.EsCabeceraDesglose.Value = Request.Form("EsCabeceraDesglose")
            Else
                Valor = Request("Valor")
                If (Request("EsCabecera") <> Nothing) Then
                    Me.EsCabeceraDesglose.Value = Request("EsCabecera")
                End If

            End If

            Dim pKey(0) As DataColumn
            DS = New DataSet
            oDGElement = DS.Tables.Add("TEMP")
            oDGElement.Columns.Add("PORCENTAGE", System.Type.GetType("System.Int32"))
            oDGElement.Columns.Add("DESCRIPCION", System.Type.GetType("System.String"))
            oDGElement.Columns.Add("VALORASOCIADO", System.Type.GetType("System.String"))
            pKey(0) = oDGElement.Columns("VALORASOCIADO")
            oDGElement.PrimaryKey = pKey

            If Valor <> "" Then
                Dim arrPresupuestos() As String = Valor.Split("#")
                Dim arrPresup As String
                Dim arrPresupuesto() As String
                Dim iNivel As Integer
                Dim lIdPresup As Integer
                Dim dPorcent As Double
                Dim iAnyo As Integer
                Dim sPres1 As String
                Dim sPres2 As String
                Dim sPres3 As String
                Dim sPres4 As String

                Dim sDenUon As String
                Dim sDenPres As String


                Dim oPres1 As Fullstep.PMPortalServer.PresProyectosNivel1
                Dim oPres2 As Fullstep.PMPortalServer.PresContablesNivel1
                Dim oPres3 As Fullstep.PMPortalServer.PresConceptos3Nivel1
                Dim oPres4 As Fullstep.PMPortalServer.PresConceptos4Nivel1
                Dim oDS As DataSet

                Valor = ""

                Dim valorasociado As String
                Dim primerelemento As Boolean = True
                Dim DescripcionCompleta As String
                For Each arrPresup In arrPresupuestos
                    arrPresupuesto = arrPresup.Split("_")

                    sUon1 = Nothing
                    sUon2 = Nothing
                    sUon3 = Nothing
                    sDenUon = Nothing
                    sPres1 = Nothing
                    sPres2 = Nothing
                    sPres3 = Nothing
                    sPres4 = Nothing
                    sDenPres = Nothing
                    iAnyo = Nothing
                    DescripcionCompleta = Nothing

                    iNivel = arrPresupuesto(0)
                    lIdPresup = arrPresupuesto(1)
                    dPorcent = Numero(arrPresupuesto(2), ".", ",")


                    'A partir del id del presupuesto y el nivel obtenemos los c�digos de unidad organizativa y de presupuesto en el dataset DS
                    Select Case oCampo.IdTipoCampoGS
                        Case Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.PRES1
                            iTipo = 1

                            oPres1 = FSWSServer.Get_PresProyectosNivel1
                            Select Case iNivel
                                Case 1
                                    oPres1.LoadData(lCiaComp, lIdPresup)
                                Case 2
                                    oPres1.LoadData(lCiaComp, Nothing, lIdPresup)
                                Case 3
                                    oPres1.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
                                Case 4
                                    oPres1.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
                            End Select
                            oDS = oPres1.Data
                            iAnyo = oDS.Tables(0).Rows(0).Item("ANYO")
                            oPres1 = Nothing
                        Case Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Pres2
                            iTipo = 2
                            oPres2 = FSWSServer.Get_PresContablesNivel1
                            Select Case iNivel
                                Case 1
                                    oPres2.LoadData(lCiaComp, lIdPresup)
                                Case 2
                                    oPres2.LoadData(lCiaComp, Nothing, lIdPresup)
                                Case 3
                                    oPres2.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
                                Case 4
                                    oPres2.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
                            End Select
                            oDS = oPres2.Data
                            iAnyo = oDS.Tables(0).Rows(0).Item("ANYO")
                            oPres2 = Nothing
                        Case Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Pres3
                            iTipo = 3
                            oPres3 = FSWSServer.Get_PresConceptos3Nivel1
                            Select Case iNivel
                                Case 1
                                    oPres3.LoadData(lCiaComp, lIdPresup)
                                Case 2
                                    oPres3.LoadData(lCiaComp, Nothing, lIdPresup)
                                Case 3
                                    oPres3.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
                                Case 4
                                    oPres3.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
                            End Select
                            oDS = oPres3.Data
                            oPres3 = Nothing
                        Case Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Pres4
                            iTipo = 4
                            oPres4 = FSWSServer.Get_PresConceptos4Nivel1
                            Select Case iNivel
                                Case 1
                                    oPres4.LoadData(lCiaComp, lIdPresup)
                                Case 2
                                    oPres4.LoadData(lCiaComp, Nothing, lIdPresup)
                                Case 3
                                    oPres4.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
                                Case 4
                                    oPres4.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
                            End Select
                            oDS = oPres4.Data
                            oPres4 = Nothing
                    End Select
                    If Not oDS Is Nothing Then
                        With oDS.Tables(0).Rows(0)

                            If (iTipo = 1) Or (iTipo = 2) Then
                                DescripcionCompleta = "(" & .Item("ANYO") & ") "
                            End If

                            If Not IsDBNull(.Item("UON1")) Then
                                sUon1 = .Item("UON1")
                                If Not IsDBNull(.Item("UON2")) Then
                                    sUon2 = .Item("UON2")
                                    If Not IsDBNull(.Item("UON3")) Then
                                        sUon3 = .Item("UON3")
                                    End If
                                End If
                            End If
                            sDenPres = DBNullToSomething(.Item("DEN"))
                            sDenUon = DBNullToSomething(.Item("UONDEN"))
                            If Not IsDBNull(.Item("PRES4")) Then
                                sPres4 = .Item("PRES4")
                                sPres3 = .Item("PRES3")
                                sPres2 = .Item("PRES2")
                                sPres1 = .Item("PRES1")
                                DescripcionCompleta += sPres1 + "-" + sPres2 + "-" + sPres3 + "-" + sPres4

                            ElseIf Not IsDBNull(.Item("PRES3")) Then
                                sPres3 = .Item("PRES3")
                                sPres2 = .Item("PRES2")
                                sPres1 = .Item("PRES1")
                                DescripcionCompleta += sPres1 + "-" + sPres2 + "-" + sPres3

                            ElseIf Not IsDBNull(.Item("PRES2")) Then
                                sPres2 = .Item("PRES2")
                                sPres1 = .Item("PRES1")
                                DescripcionCompleta += sPres1 + "-" + sPres2

                            ElseIf Not IsDBNull(.Item("PRES1")) Then
                                sPres1 = .Item("PRES1")
                                DescripcionCompleta += sPres1
                            End If

                            oDS = Nothing
                        End With
                        Dim bSinPermisos As Boolean
                        bSinPermisos = False


                        sScript += "arrDistribuciones[arrDistribuciones.length]= new Distribucion('" + JSText(sUon1) + "','" + JSText(sUon2) + "','" + JSText(sUon3) + "'," + JSNum(iAnyo) + ",'" + JSText(sPres1) + "','" + JSText(sPres2) + "','" + JSText(sPres3) + "','" + JSText(sPres4) + "'," + JSNum(dPorcent) + "," + JSNum(iNivel) + "," + JSNum(lIdPresup) + ",'" + JSText(sDenUon) + "','" + JSText(sDenPres) + "');"
                        If primerelemento Then
                            Valor += arrPresup
                            Me.Valor.Value = arrPresup
                            primerelemento = False
                        Else
                            Valor += "#" + arrPresup
                            Me.Valor.Value += "#" + arrPresup
                        End If

                        If Not IsPostBack Then
                            dtNewRow = oDGElement.NewRow
                            dtNewRow.Item("PORCENTAGE") = dPorcent * 100
                            DescripcionCompleta += "-" + sDenPres
                            dtNewRow.Item("DESCRIPCION") = DescripcionCompleta
                            valorasociado = arrPresupuesto(0) + "_" + arrPresupuesto(1) + "_" + arrPresupuesto(2)
                            dtNewRow.Item("VALORASOCIADO") = valorasociado
                            oDGElement.Rows.Add(dtNewRow)
                        Else
                            Me.wdg_Asignaciones.Rows.Clear()
                        End If


                    End If

                Next

                sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "distribucionPresupuestos", sScript)

                If Valor <> Nothing Then
                    Valor = Left(Valor, Valor.Length - 1)
                End If
            Else
                Me.wdg_Asignaciones.Rows.Clear()
            End If

            'Cambiamos el formato del campo A�o para que no salga el separador de miles
            Dim ci As System.Globalization.CultureInfo = New System.Globalization.CultureInfo(System.Threading.Thread.CurrentThread.CurrentCulture.LCID)
            Dim numberInfo As System.Globalization.NumberFormatInfo = ci.NumberFormat.CurrentInfo.Clone

            numberInfo.NumberGroupSeparator = ""
            ugtxtAnyo.NumberFormat = numberInfo

            If Not IsPostBack Then
                Me.wdg_Asignaciones.DataSource = oDGElement
                Me.wdg_Asignaciones.DataBind()
                Session("DTPress") = oDGElement

                Me.wdg_Asignaciones.Columns("PORCENTAGE").Header.Text = "%"
                Me.wdg_Asignaciones.Columns("PORCENTAGE").Width = Unit.Percentage(10)
                Me.wdg_Asignaciones.Columns("DESCRIPCION").Header.Text = mcadenapresupuestos + Textos(25)
            Else
                Me.wdg_Asignaciones.DataSource = Session("DTPress")
                Me.wdg_Asignaciones.DataBind()
            End If



            If Not Page.IsPostBack Then

                oUnidades = FSWSServer.get_UnidadesOrg()
                'Siempre Se carga la combo, no hay tema de usuario restringido
                lblUorganizativaRestringida.Visible = False
                ugtxtunidadorganizativa.Visible = True
                oUnidades.CargarUnidadesConPresupuestos(lCiaComp, iTipo, anyo, sIdi)

                Me.ugtxtunidadorganizativa.TextField = "DEN"
                Me.ugtxtunidadorganizativa.ValueField = "COD"
                Me.ugtxtunidadorganizativa.DataSource = From datos In oUnidades.Data.Tables(0)
                                                        Select New With {.DEN = datos.Item("den"), .COD = datos.Item(0) & "@@" & datos.Item(1) & "@@" & datos.Item(2)}
                Me.ugtxtunidadorganizativa.DataBind()

                Dim i As Integer
                Dim cad As String

                Dim tempuseruon1 As String = "#"
                Dim tempuseruon2 As String = "#"
                Dim tempuseruon3 As String = "#"

                For Each oItem As Infragistics.Web.UI.ListControls.DropDownItem In Me.ugtxtunidadorganizativa.Items
                    Dim oCod As String() = Split(oItem.Value, "@@")
                    If oCod.Length = 3 Then
                        If (oCod(0) = tempuseruon1) And (oCod(1) = tempuseruon2) And (oCod(2) = tempuseruon3) Then
                            ugtxtunidadorganizativa.SelectedItemIndex = oItem.Index
                            Exit For
                        End If
                    End If
                Next

                CargarMisPresupuestos(iTipo, anyo, tempuseruon1, tempuseruon2, tempuseruon3, sIdi, "", "")
                Me.UON1.Value = tempuseruon1
                Me.UON2.Value = tempuseruon2
                Me.UON3.Value = tempuseruon3

            Else

                Dim codigo As String
                codigo = Request.Form("txtCodigo")
                Dim currLoc1 As Integer

                Dim StringLength1 As Integer
                Dim tmpChar1 As String
                'Reemplazo los * por % para la busqueda por denominacion
                StringLength1 = Len(codigo)
                For currLoc1 = 1 To StringLength1
                    tmpChar1 = Mid(codigo, currLoc1, 1)
                    If InStr("*", tmpChar1) Then
                        Mid(codigo, currLoc1, 1) = "%"
                    End If
                Next

                Dim denominacion As String
                denominacion = Request.Form("txtDenominacion")

                Dim currLoc As Integer
                Dim StringLength As Integer
                Dim tmpChar As String
                'Reemplazo los * por % para la busqueda por denominacion
                StringLength = Len(denominacion)
                For currLoc = 1 To StringLength
                    tmpChar = Mid(denominacion, currLoc, 1)
                    If InStr("*", tmpChar) Then
                        Mid(denominacion, currLoc, 1) = "%"
                    End If
                Next

                CargarMisPresupuestos(iTipo, anyo, Me.UON1.Value, Me.UON2.Value, Me.UON3.Value, sIdi, codigo, denominacion)
            End If

        End Sub
        ''' <summary>
        ''' Carga los presupuestos disponibles seg�n los par�metros pasados.
        ''' </summary>
        ''' <param name="iTipo">Tipo de presupuesto</param>
        ''' <param name="iAnyo">A�o del presupuesto</param>        
        ''' <param name="sUon1">Unidad organizativa de nivel 1</param>        
        ''' <param name="sUon2">Unidad organizativa de nivel 2</param>        
        ''' <param name="sUon3">Unidad organizativa de nivel 3</param>        
        ''' <param name="Codigo">C�digo introducido en la b�squeda</param>        
        ''' <param name="Denominacion">Denominaci�n introducida en la b�squeda</param>        
        ''' <remarks>Llamada desde: Page_Load; Tiempo m�ximo: 0 sg.</remarks>
        Private Sub CargarMisPresupuestos(ByVal iTipo As Integer, ByVal iAnyo As Integer, ByVal sUon1 As String, ByVal sUon2 As String, ByVal sUon3 As String, ByVal sIdi As String, ByVal Codigo As String, ByVal Denominacion As String)
            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUnidades As Fullstep.PMPortalServer.UnidadesOrg
            Dim lCiaComp As Long = IdCiaComp
            Dim bEsUONbase As Boolean = False
            Dim iEsUONVacia As Integer = 0
            oUnidades = FSWSServer.get_UnidadesOrg

            If sUon1 = "null" And sUon2 = "null" And sUon3 = "null" Then
                iEsUONVacia = 1
            ElseIf sUon1 = "" And sUon2 = "" And sUon3 = "" Then
                iEsUONVacia = 1
            End If

            If sUon1 = "#" Or sUon1 = "null" Then sUon1 = ""
            If sUon2 = "#" Or sUon2 = "null" Then sUon2 = ""
            If sUon3 = "#" Or sUon3 = "null" Then sUon3 = ""

            oUnidades.CargarPresupuestos(lCiaComp, iTipo, iAnyo, sUon1, sUon2, sUon3, sIdi, Codigo, Denominacion, iEsUONVacia)

            Dim iTablas As Integer
            For iTablas = 0 To oUnidades.Data.Tables.Count() - 1
                If (oUnidades.Data.Tables(iTablas).Rows.Count > 0) Then
                    bMostrarArbolyGrid = True
                End If
            Next

            If (sUon1 = "") And (sUon2 = "") And (sUon3 = "") Then
                Dim ArrayUON As String()
                ReDim ArrayUON(0)
                Dim ArrayDENUON As String()
                ReDim ArrayDENUON(0)
                Dim UONBase As Boolean
                UONBase = False
                Dim iElemento As Integer
                Dim iElementoArray As Integer
                Dim sTemp As String = ""
                Dim sTempUON1 As String = ""
                Dim sTempUON2 As String = ""
                Dim sTempUON3 As String = ""
                Dim sTempDEN As String = ""
                Dim bEstaenelArray As Boolean = False

                For iTablas = 1 To oUnidades.Data.Tables.Count() - 1
                    If (oUnidades.Data.Tables(iTablas).Rows.Count > 0) Then
                        For iElemento = 0 To oUnidades.Data.Tables(iTablas).Rows.Count - 1
                            sTemp = ""
                            sTempDEN = ""
                            sTempUON1 = "#"
                            sTempUON2 = "#"
                            sTempUON3 = "#"
                            If (DBNullToSomething(oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON1")) <> Nothing) Then
                                sTemp = oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON1")
                                sTempUON1 = oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON1")
                                If (DBNullToSomething(oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON2")) <> Nothing) Then
                                    sTemp += "-" + oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON2")
                                    sTempUON2 = oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON2")
                                    If (DBNullToSomething(oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON3")) <> "") Then
                                        sTemp += "-" + oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON3")
                                        sTempUON3 = oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("UON3")
                                    End If
                                End If
                            End If

                            If (DBNullToSomething(oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("DEN_UON")) <> Nothing) Then
                                If (sTemp = "") Then
                                    sTempDEN += oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("DEN_UON")
                                Else
                                    sTempDEN += "-" + oUnidades.Data.Tables(iTablas).Rows(iElemento).Item("DEN_UON")
                                End If


                            End If


                            For iElementoArray = 0 To ArrayUON.Length - 1
                                If (ArrayUON.GetValue(iElementoArray) = sTemp) Then
                                    bEstaenelArray = True
                                    Exit For
                                End If
                            Next

                            If (bEstaenelArray = False Or (sTemp = "" And UONBase = False)) Then
                                If (sTemp = "") Then
                                    UONBase = True
                                End If

                                ArrayUON.SetValue(sTemp, ArrayUON.Length - 1)
                                ReDim Preserve ArrayUON(ArrayUON.Length)

                                ArrayDENUON.SetValue(sTempDEN, ArrayDENUON.Length - 1)

                                ReDim Preserve ArrayDENUON(ArrayDENUON.Length)

                            End If

                            bEstaenelArray = False
                            sTemp = ""
                        Next

                    End If
                Next

                Dim sText As String = ""

                If (ArrayUON.Length > 2) And (iEsUONVacia = 1) Then 'Comparo contra 2 pq tengo reservada una linea de mas
                    sText += cadenaerrormultiplesUO1 + "\n"
                    For iElementoArray = 0 To ArrayUON.Length - 2
                        sText += ArrayUON.GetValue(iElementoArray) + ArrayDENUON.GetValue(iElementoArray) + "\n"
                    Next
                    sText += "\n" + cadenaerrormultiplesUO2 + "\n"
                    Dim sScriptAlert
                    sScriptAlert += "AlertaMultiplesUONS('" + JSText(sText) + "'); "

                    sScriptAlert = String.Format(IncludeScriptKeyFormat, "javascript", sScriptAlert)
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "multiplesuons", sScriptAlert)

                Else
                    Me.uwtPresupuestos2.ClearAll()
                    Me.txtPorcent.Visible = True
                    Me.lblSeleccionaArbol.Visible = True
                    Me.uwtPresupuestos2.Visible = True
                    Me.lblAsignado.Visible = True
                    Me.txtAsignadoPorcent.Visible = True
                    Me.lblResultado.Visible = True
                    Me.lblPendiente.Visible = True
                    Me.txtPendientePorcent.Visible = True
                    Me.Label1.Visible = True
                    Me.Label3.Visible = True
                    Me.lblPorcen1.Visible = True
                    Me.lblPorcen2.Visible = True

                    Me.uwtPresupuestos2.DataSource = oUnidades.Data.Tables(0).DefaultView

                    Me.uwtPresupuestos2.Levels(0).RelationName = "REL_NIV0_NIV1"
                    Me.uwtPresupuestos2.Levels(0).ColumnName = "CDEN"
                    Me.uwtPresupuestos2.Levels(0).LevelImage = "images/RaizPresCon.gif"
                    Me.uwtPresupuestos2.Levels(1).RelationName = "REL_NIV1_NIV2"
                    Me.uwtPresupuestos2.Levels(1).ColumnName = "CDEN"
                    Me.uwtPresupuestos2.Levels(1).LevelKeyField = "IDCOD"
                    Me.uwtPresupuestos2.Levels(1).LevelImage = "images/SINEWAVE.gif"
                    Me.uwtPresupuestos2.Levels(2).RelationName = "REL_NIV2_NIV3"
                    Me.uwtPresupuestos2.Levels(2).ColumnName = "CDEN"
                    Me.uwtPresupuestos2.Levels(2).LevelKeyField = "IDCOD"
                    Me.uwtPresupuestos2.Levels(2).LevelImage = "images/SINEWAVE.gif"
                    Me.uwtPresupuestos2.Levels(3).RelationName = "REL_NIV3_NIV4"
                    Me.uwtPresupuestos2.Levels(3).ColumnName = "CDEN"
                    Me.uwtPresupuestos2.Levels(3).LevelKeyField = "IDCOD"
                    Me.uwtPresupuestos2.Levels(3).LevelImage = "images/SINEWAVE.gif"
                    Me.uwtPresupuestos2.Levels(4).ColumnName = "CDEN"
                    Me.uwtPresupuestos2.Levels(4).LevelKeyField = "IDCOD"
                    Me.uwtPresupuestos2.Levels(4).LevelImage = "images/SINEWAVE.gif"
                    Me.uwtPresupuestos2.DataKeyOnClient = True
                    Me.uwtPresupuestos2.DataBind()
                    Dim sScript
                    sScript += "CargarPorcentage()"
                    sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "cargarporcentage", sScript)
                    If uwtPresupuestos2.Nodes(0).Nodes.Count = 1 Then
                        Me.uwtPresupuestos2.ExpandAll()
                    Else
                        Me.uwtPresupuestos2.Nodes(0).Expand(False)
                    End If

                    Me.DENUON.Value = sTempDEN
                    If (sTempUON1 = "#") Then
                        Me.UON1.Value = ""
                    Else
                        Me.UON1.Value = sTempUON1
                    End If
                    If (sTempUON1 = "#") Then
                        Me.UON2.Value = ""
                    Else
                        Me.UON2.Value = sTempUON2
                    End If
                    If (sTempUON1 = "#") Then
                        Me.UON3.Value = ""
                    Else
                        Me.UON3.Value = sTempUON3
                    End If

                    If (ArrayUON.Length <= 2) Then 'Cuando la UO no es la base la longitud es 2 y cuando es la base la longitud es 1
                        'hay una unica UO y la vamos a poner seleccionada en el combo
                        For Each oItem As Infragistics.Web.UI.ListControls.DropDownItem In Me.ugtxtunidadorganizativa.Items
                            Dim oCod As String() = Split(oItem.Value, "@@")
                            If oCod.Length = 3 Then
                                If (oCod(0) = sTempUON1) And (oCod(1) = sTempUON2) And (oCod(2) = sTempUON3) Then
                                    Dim sScriptActualizarIndexCombo
                                    sScriptActualizarIndexCombo = " CambiarValorCombo ( " + oItem.Index.ToString() + "); "
                                    sScriptActualizarIndexCombo = String.Format(IncludeScriptKeyFormat, "javascript", sScriptActualizarIndexCombo)
                                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "scriptactualizarindexcombo", sScriptActualizarIndexCombo)
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                End If
            Else

                Dim Valor As String 'Campo importante
                If (Request.Form("Tipo") <> Nothing) Then
                    Valor = Request.Form("Valor")
                Else
                    Valor = Request("Valor")
                End If

                If (bMostrarArbolyGrid) Then
                    Me.uwtPresupuestos2.ClearAll()
                    Me.txtPorcent.Visible = True
                    Me.lblSeleccionaArbol.Visible = True
                    Me.uwtPresupuestos2.Visible = True
                    Me.lblAsignado.Visible = True
                    Me.txtAsignadoPorcent.Visible = True
                    Me.lblResultado.Visible = True
                    Me.lblPendiente.Visible = True
                    Me.txtPendientePorcent.Visible = True
                    Me.Label1.Visible = True
                    Me.Label3.Visible = True
                    Me.lblPorcen1.Visible = True
                    Me.lblPorcen2.Visible = True

                    Me.uwtPresupuestos2.DataSource = oUnidades.Data.Tables(0).DefaultView

                    Me.uwtPresupuestos2.Levels(0).RelationName = "REL_NIV0_NIV1"
                    Me.uwtPresupuestos2.Levels(0).ColumnName = "CDEN"
                    Me.uwtPresupuestos2.Levels(0).LevelImage = "images/RaizPresCon.gif"
                    Me.uwtPresupuestos2.Levels(1).RelationName = "REL_NIV1_NIV2"
                    Me.uwtPresupuestos2.Levels(1).ColumnName = "CDEN"
                    Me.uwtPresupuestos2.Levels(1).LevelKeyField = "IDCOD"
                    Me.uwtPresupuestos2.Levels(1).LevelImage = "images/SINEWAVE.gif"
                    Me.uwtPresupuestos2.Levels(2).RelationName = "REL_NIV2_NIV3"
                    Me.uwtPresupuestos2.Levels(2).ColumnName = "CDEN"
                    Me.uwtPresupuestos2.Levels(2).LevelKeyField = "IDCOD"
                    Me.uwtPresupuestos2.Levels(2).LevelImage = "images/SINEWAVE.gif"
                    Me.uwtPresupuestos2.Levels(3).RelationName = "REL_NIV3_NIV4"
                    Me.uwtPresupuestos2.Levels(3).ColumnName = "CDEN"
                    Me.uwtPresupuestos2.Levels(3).LevelKeyField = "IDCOD"
                    Me.uwtPresupuestos2.Levels(3).LevelImage = "images/SINEWAVE.gif"
                    Me.uwtPresupuestos2.Levels(4).ColumnName = "CDEN"
                    Me.uwtPresupuestos2.Levels(4).LevelKeyField = "IDCOD"
                    Me.uwtPresupuestos2.Levels(4).LevelImage = "images/SINEWAVE.gif"
                    Me.uwtPresupuestos2.DataKeyOnClient = True
                    Me.uwtPresupuestos2.DataBind()
                    Dim sScript
                    sScript += "CargarPorcentage()"
                    sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "cargarporcentage", sScript)
                    If uwtPresupuestos2.Nodes(0).Nodes.Count = 1 Then
                        Me.uwtPresupuestos2.ExpandAll()
                    Else
                        Me.uwtPresupuestos2.Nodes(0).Expand(False)
                    End If

                Else
                    'No hay que mostrar elementos en el arbol
                    If (Valor = "") Then
                        Me.txtPorcent.Visible = False
                        Me.lblSeleccionaArbol.Visible = False
                        Me.uwtPresupuestos2.Visible = False
                        Me.lblAsignado.Visible = False
                        Me.txtAsignadoPorcent.Visible = False
                        Me.lblResultado.Visible = False
                        Me.lblPendiente.Visible = False
                        Me.txtPendientePorcent.Visible = False
                        Me.Label1.Visible = False
                        Me.Label3.Visible = False
                        Me.lblPorcen1.Visible = False
                        Me.lblPorcen2.Visible = False
                    Else
                        Me.txtPorcent.Visible = True
                        Me.lblSeleccionaArbol.Visible = True
                        Me.uwtPresupuestos2.Visible = True
                        Me.lblAsignado.Visible = True
                        Me.txtAsignadoPorcent.Visible = True
                        Me.lblResultado.Visible = True
                        Me.lblPendiente.Visible = True
                        Me.txtPendientePorcent.Visible = True
                        Me.Label1.Visible = True
                        Me.Label3.Visible = True
                        Me.lblPorcen1.Visible = True
                        Me.lblPorcen2.Visible = True
                    End If

                    Dim sScript
                    sScript += "Botones.style.display='inline';MensajeSinElementos()"
                    sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "cargarporcentage", sScript)

                End If
            End If

            'Si s�lo existe un presupuesto en el �rbol lo seleccionamos
            Dim oNode As Infragistics.WebUI.UltraWebNavigator.Node
            Dim i As Integer
            oNode = Me.uwtPresupuestos2.Nodes(0) 'Nodo ra�z.
            If oNode.Nodes.Count = 1 Then 'si s�lo hay una rama de nivel 1
                If oNode.FirstNode.Nodes.Count = 0 Then 'Si no tiene ramas de nivel 2 selecciona la rama de nivel 1
                    uwtPresupuestos2.SelectedNode = oNode.FirstNode
                ElseIf oNode.FirstNode.Nodes.Count = 1 Then 'tiene una rama de nivel 2
                    If oNode.FirstNode.FirstNode.Nodes.Count = 0 Then ' selecciona la rama de nivel 2   
                        uwtPresupuestos2.SelectedNode = oNode.FirstNode.FirstNode
                    ElseIf oNode.FirstNode.FirstNode.Nodes.Count = 1 Then ' tiene una rama de nivel 3
                        If oNode.FirstNode.FirstNode.FirstNode.Nodes.Count = 0 Then ' selecciona la rama de nivel 3
                            uwtPresupuestos2.SelectedNode = oNode.FirstNode.FirstNode.FirstNode
                        ElseIf oNode.FirstNode.FirstNode.FirstNode.Nodes.Count = 1 Then ' tiene una rama de nivel 4
                            uwtPresupuestos2.SelectedNode = oNode.FirstNode.FirstNode.FirstNode.FirstNode 'Seleccina la rama de nivel 4
                        End If
                    End If
                End If
            End If
        End Sub
        ''' <summary>
        ''' evento que salta cuando se elimina una fila al grid de asignaciones
        ''' </summary>
        ''' <param name="sender">grid de asignaciones</param>
        ''' <param name="e">>valores de la fila a eliminar</param>
        ''' <remarks></remarks>
        Protected Sub wdg_Asignaciones_RowsDeleting(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowDeletingEventArgs)
            oDGElement = Session("DTPress")
            If Not oDGElement.Rows.Find(e.Row.DataKey) Is Nothing Then
                oDGElement.Rows.Remove(oDGElement.Rows.Find(e.Row.DataKey))
            End If
            Session("DTPress") = oDGElement
            wdg_Asignaciones.DataSource = oDGElement
            wdg_Asignaciones.DataBind()
        End Sub
        ''' <summary>
        ''' evento que salta cuando se a�ade una fila al grid de asignaciones
        ''' </summary>
        ''' <param name="sender">grid de asignaciones</param>
        ''' <param name="e">valores de la nueva fila</param>
        ''' <remarks></remarks>
        Private Sub wdg_Asignaciones_RowAdding(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowAddingEventArgs) Handles wdg_Asignaciones.RowAdding
            oDGElement = Session("DTPress")
            dtNewRow = oDGElement.NewRow
            dtNewRow.Item("PORCENTAGE") = e.Values.Item("PORCENTAGE")
            dtNewRow.Item("DESCRIPCION") = e.Values.Item("DESCRIPCION")
            dtNewRow.Item("VALORASOCIADO") = e.Values.Item("VALORASOCIADO")
            oDGElement.Rows.Add(dtNewRow)
            Session("DTPress") = oDGElement
            wdg_Asignaciones.DataSource = oDGElement
            wdg_Asignaciones.DataBind()
        End Sub
        ''' <summary>
        ''' evento que salta cuando se actualiza alguna celda del grid de asignaciones
        ''' </summary>
        ''' <param name="sender">grid de asignaciones</param>
        ''' <param name="e">valores de la fila</param>
        ''' <remarks></remarks>
        Private Sub wdg_Asignaciones_RowUpdating(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowUpdatingEventArgs) Handles wdg_Asignaciones.RowUpdating
            oDGElement = Session("DTPress")
            dtNewRow = oDGElement.Rows.Find(e.Row.DataKey)
            dtNewRow.Item("PORCENTAGE") = e.Values.Item("PORCENTAGE")
            dtNewRow.Item("DESCRIPCION") = e.Values.Item("DESCRIPCION")
            dtNewRow.Item("VALORASOCIADO") = e.Values.Item("VALORASOCIADO")
            Session("DTPress") = oDGElement
            wdg_Asignaciones.DataSource = oDGElement
            wdg_Asignaciones.DataBind()
        End Sub
    End Class
End Namespace