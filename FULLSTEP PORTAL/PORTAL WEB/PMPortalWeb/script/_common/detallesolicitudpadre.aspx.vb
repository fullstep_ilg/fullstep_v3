Namespace Fullstep.PMWeb

    Partial Class detallesolicitudpadre
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
            Dim lCiaComp As Long = IdCiaComp
            Dim sIdi As String = oUser.Idioma
            Dim idSolPadre As Integer
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If
            If Request("idSolPadre") = "" Then
                idSolPadre = 0
            Else
                idSolPadre = Request("idSolPadre")
            End If

            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSWSServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DetalleSeguimientoPadre, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)


            Me.lblIdentificador.Text = oTextos.Rows(1).Item(1)  'Identificador
            Me.lblTitulo.Text = oTextos.Rows(2).Item(1)         'T�tulo
            Me.lblImporteAprob.Text = oTextos.Rows(3).Item(1)    'Importe aprobado
            Me.lblImporteAcum.Text = oTextos.Rows(4).Item(1)   'Importe acumulado
            Me.cmdCerrar.Value = oTextos.Rows(5).Item(1)      'Cerrar

            If idSolPadre > 0 Then
                Dim oInstancias As Fullstep.PMPortalServer.Instancias
                oInstancias = FSWSServer.Get_Instancias
                oInstancias.DevolverSolicitudesPadre(lCiaComp, sIdi, idSolPadre)
                If oInstancias.Data.Tables(0).Rows.Count > 0 Then
                    Me.txtIdentificador.Text = idSolPadre
                    Me.txtTitulo.Text = oInstancias.Data.Tables(0).Rows(0).Item(1)
                    Me.txtImporteAprob.Text = oInstancias.Data.Tables(0).Rows(0).Item(3)
                    Me.txtImporteAcum.Text = Me.txtImporteAprob.Text - oInstancias.Data.Tables(0).Rows(0).Item(4)
                End If
                oInstancias = Nothing
            Else
                Me.txtIdentificador.Text = idSolPadre
                Me.txtTitulo.Text = ""
                Me.txtImporteAprob.Text = 0
                Me.txtImporteAcum.Text = 0

            End If

        End Sub

    End Class
End Namespace