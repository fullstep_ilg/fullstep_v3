Partial Class QAEnProceso
    Inherits FSPMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Protected WithEvents lblProveedor As System.Web.UI.WebControls.Label
    Protected WithEvents lblProveedorBD As System.Web.UI.WebControls.Label
    Protected Titulo As String

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    ''' <summary>
    ''' Cargar la pagina. 
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>        
    ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sIdi As String
        Dim oDict As Fullstep.PMPortalServer.Dictionary
        Dim oTextos As DataTable
        Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
        Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
        Dim lCiaComp As Long = IdCiaComp

        oUser = Session("FS_Portal_User")
        oUser.LoadUserData(oUser.Cod, lCiaComp)

        sIdi = oUser.Idioma
        If sIdi = Nothing Then
            sIdi = ConfigurationManager.AppSettings("idioma")
        End If

        oDict = FSWSServer.Get_Dictionary()
        oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.VisorCertificados, sIdi)

        oTextos = oDict.Data.Tables(0)

        Dim sInstancia As String
        Try
            sInstancia = Encrypter.Encrypt(Request("Instancia"), , False, Encrypter.TipoDeUsuario.Administrador)
        Catch ex As Exception
            Exit Sub
        End Try

        Dim sFecAlta As String = Request("FecAlta")
        Dim sProveedor As String = Request("Proveedor")
        Dim Tipo As String = Request("Tipo")

        lblId.Text = oTextos.Rows(15).Item(1)
        lblFecha.Text = oTextos.Rows(14).Item(1)
        'lblProveedor.Text = oTextos.Rows(13).Item(1)
        lblTipo.Text = oTextos.Rows(16).Item(1)
        btnCerrar.Value = oTextos.Rows(17).Item(1)
        lblEnProceso.Text = oTextos.Rows(18).Item(1)
        Titulo = oTextos.Rows(19).Item(1)

        lblIdBD.Text = sInstancia

        If sFecAlta <> "" Then
            lblFechaBD.Text = Fullstep.PMPortalWeb.modUtilidades.FormatDate(sFecAlta, oUser.DateFormat)
        End If
        'lblProveedorBD.Text = sProveedor
        lblTipoBD.Text = Tipo
    End Sub

End Class
