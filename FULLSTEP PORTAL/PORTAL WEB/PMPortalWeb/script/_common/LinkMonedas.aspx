<%@ Page Language="vb" AutoEventWireup="false" Codebehind="LinkMonedas.aspx.vb" Inherits="LinkMonedas" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
		<title>LinkMonedas</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body class="EstadosFondo2">
		<SCRIPT type="text/javascript"><!--

	function uwgMoneda_CellClickHandler(gridName, cellId, button){	 
		//Add code to handle your event here.
		var row = igtbl_getRowById(cellId); 
		//var columnClicked = igtbl_getColumnById(cellId)
		var cellCOD = row.getCell(0)
		var cellDEN = row.getCell(1)
		var cellEQUIV = row.getCell(2); 
		var p = window.parent
		var oEntry = p.fsGeneralEntry_getById("<%=Request("EntryMoneda")%>")

		oEntry.MonRepercutido=cellCOD.getValue()
		oEntry.CambioRepercutido=cellEQUIV.getValue()

		var oEntryBt = p.document.all("<%=Request("EntryBoton")%>")
		oEntryBt.innerText=cellCOD.getValue()

		var oFrame = p.document.all("<%=Request("FrameId")%>")
		oFrame.parentNode.removeChild(oFrame);
		//p.ddglobalListenerWasCreated = false	
	}
--></SCRIPT>

		<form id="Form1" method="post" runat="server">
			<table style="Z-INDEX: 101; LEFT: 0px; WIDTH: 200px; POSITION: absolute; TOP: 0px; HEIGHT: 200px"
				class="cabeceraSolicitud igTreeEnTab">
				<tr>
					<td>
						<igtbl:ultrawebgrid id="uwgMoneda" style="Z-INDEX: 102; LEFT: 0px; POSITION: absolute; TOP: 0px" runat="server"
							Width="192px" Height="192px">
							<DisplayLayout RowHeightDefault="20px" Version="4.00" GridLinesDefault="None" BorderCollapseDefault="Separate"
								Name="uwgMoneda">
								<AddNewBox>
									<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">

<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
</BorderDetails>

									</Style>
								</AddNewBox>
								<Pager>
									<Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">

<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
</BorderDetails>

									</Style>
								</Pager>
								<HeaderStyleDefault BorderStyle="Solid" BackColor="LightGray">
									<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
								</HeaderStyleDefault>
								<FrameStyle Width="192px" Cursor="Hand" BorderWidth="1px" Font-Size="8pt" BorderStyle="None"
									Height="192px"></FrameStyle>
								<FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
									<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
								</FooterStyleDefault>
								<ClientSideEvents CellClickHandler="uwgMoneda_CellClickHandler"></ClientSideEvents>
								<EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
								<RowStyleDefault BorderWidth="1px" BorderColor="Gray" BorderStyle="Solid">
									<Padding Left="3px"></Padding>
									<BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
								</RowStyleDefault>
							</DisplayLayout>
							<Bands>
								<igtbl:UltraGridBand ColHeadersVisible="No" CellClickAction="RowSelect" RowSelectors="No"></igtbl:UltraGridBand>
							</Bands>
						</igtbl:ultrawebgrid>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
