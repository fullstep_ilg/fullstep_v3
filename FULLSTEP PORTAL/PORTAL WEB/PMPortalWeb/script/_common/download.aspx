<%@ Page Language="vb" AutoEventWireup="false" Codebehind="download.aspx.vb" Inherits="Fullstep.PMPortalWeb.download" EnableTheming="false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
</head>	
    <body>
        <form id="form1" runat="server">
		    <iframe width="100%" style="display:block"  id="fraWSDownloadHidden" scrolling="yes"   src="<%="downloadhidden.aspx?" + Request.QueryString.tostring %>" marginheight="0" marginwidth="0" frameborder="0"   framespacing="0" height="0%"></iframe>
		    <iframe width="100%" style="display:block" id="fraWSDownloadVisible" scrolling="auto"  src="<%="downloadvisible.aspx?" + Request.QueryString.tostring %>" marginwidth="0" marginheight="0" frameborder="0" framespacing="0" height="100%"></iframe>
        </form>
    </body>
</html>
