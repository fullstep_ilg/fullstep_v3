<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="guardarinstancia.aspx.vb" Inherits="Fullstep.PMPortalWeb.guardarinstancia" %>

<!DOCTYPE HTML PUBLIC>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>guardarinstancia</title>
	<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
	<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
	<meta content="JavaScript" name="vs_defaultClientScript" />
	<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
	<link href="../../../common/estilo.asp" type="text/css" rel="stylesheet" />
	<script src="../../../common/formatos.js"></script>
	<script src="../../../common/menu.asp"></script>
	<script type="text/javascript">
		/*''' <summary>
		''' Iniciar la pagina.
		''' </summary>     
		''' <remarks>Llamada desde: page.onload; Tiempo m�ximo:0</remarks>*/
		function Init() {
			if (accesoExterno == 0) { document.getElementById('tablemenu').style.display = 'block'; }
		}
	</script>
</head>
<body onload="Init()">
	<script type="text/javascript">
		//Muestra el detalle del tipo de solicitud
		function DetalleTipoSolicitud(IdSolicitud) {
			window.open("../solicitudes/solicitud.aspx?Solicitud=" + IdSolicitud, "_blank", "width=700,height=450,status=no,resizable=no,top=100,left=100");
			return false;
		}
		function montarFormularioAvisos() {
			if (!document.forms["frmAvisos"])
				document.body.insertAdjacentHTML("beforeEnd", "<form name='frmAvisos' action='../solicitudes/avisosprecondiciones.aspx' target='ventanaAvisos' id='frmSubmit' method='post' style='visibility:hidden;position:absolute;top:0;left:0'></form>")
			else
				document.forms["frmAvisos"].innerHTML = ""

			var oLabel;
			for (i = 0; i < document.forms["Form1"].length; i++) {
				oLabel = document.forms["Form1"][i]
				if (oLabel.name != null) {
					if (oLabel.name.substring(0, oLabel.name.length - 1) == "Aviso") {
						anyadirInput(oLabel.name, oLabel.value)
					}
				}
			}
		}
		/*
		''' <summary>
		''' Crear un input tipo hidden con el nombre pasado en el param sInput y el valor pasado.
		''' </summary>
		''' <param name="sInput">nombre del hidden que se crea</param>
		''' <param name="valor">valor del hidden que se crea</param>   
		''' <returns>Nada</returns>*/
		function anyadirInput(sInput, valor) {
			if ((valor == "null") || (valor == null))
				valor = ""

			var f = document.forms["frmAvisos"]
			if (!f.elements[sInput]) {
				f.insertAdjacentHTML("beforeEnd", "<INPUT type=hidden name=" + sInput + ">\n")
				f.elements[sInput].value = valor
			}
		}
		function enviarAvisos() {
			window.open('../solicitudes/avisosprecondiciones.aspx', 'ventanaAvisos', 'height=200,width=400,status=yes,toolbar=no,menubar=no,location=no');
			document.forms['frmAvisos'].submit();
		}
		function ControlImporte(titulo, importeAcumulado, importeSolicitud, indicebloqueoAviso) {
			sTexto = arrTextosML[indicebloqueoAviso] + '\n' + '"' + titulo + '"' + '\n' + arrTextosML[2] + '=' + importeAcumulado + '\n' + arrTextosML[3] + '=' + importeSolicitud;
			alert(sTexto);
		}
		function ProblemaSave(sTexto) {
			alert(sTexto);
		}
		function validarLength(e, l) {
			if (e.value.length > l)
				return false
			else
				return true
		}
		function textCounter(e, l) {
			if (e.value.length > l)
				e.value = e.value.substring(0, l);
		}
		/*''' <summary>
		''' Deshabilitar Botones para q no se pueda dar a grabar varias veces
		''' </summary>
		''' <remarks>Llamada desde: cmdAceptar  cmdCancelar; Tiempo m�ximo:0</remarks>*/
		function DeshabilitarBotones() {
			if (document.forms["Form1"].elements["cmdAceptar"])
				document.forms["Form1"].elements["cmdAceptar"].disabled = true;
			if (document.forms["Form1"].elements["cmdCancelar"])
				document.forms["Form1"].elements["cmdCancelar"].disabled = true;
		}
		function MostrarFSWSMain() {
			parent.document.getElementById('fraWS').rows = "*,0";
		}
		function MostrarFSWSServer() {
			parent.document.getElementById('fraWS').rows = "0,*";
		}
		function BuscarPeticionario() {
			var Cod = document.forms["Form1"].elements["hPeticionario"].value
			window.open("../_common/detallepersona.aspx?CodPersona=" + Cod.toString(), "_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250")
		}
		function usu_seleccionado(sUsuCod, sNombre, sApe, cellId) {
			var grid = $find("<%=wdgtxtRoles.ClientId%>");
			var row = grid.get_behaviors().get_selection().get_selectedCells().getItem(0).get_row();

			row.get_cellByColumnKey("PER").set_value(sUsuCod)
			row.get_cellByColumnKey("NOMBRE").set_value(sNombre + ' ' + sApe)
		}
		//Abre el buscador de proveedores para elegir el destinatario de la acci�n
		function BuscadorProveedores() {
			window.open("../_common/proveedores.aspx?idFilaGrid=" + cellId, "_blank", "width=750,height=475,status=yes,resizable=no,top=200,left=200")
		}
		//Abre el buscador de usuarios para elegir el destinatario de la acci�n
		//Par�metros de entrada:    cellId: id de la celda
		function BuscadorUsuarios(cellId) {
			window.open("../_common/usuarios.aspx?idFilaGrid=" + cellId, "_blank", "width=750,height=475,status=yes,resizable=no,top=200,left=200")
		}
		//Muestra el detalle de la persona
		//Par�metro de entrada: Cod de la persona
		function VerDetallePersona(Per) {
			window.open("../_common/detallepersona.aspx?CodPersona=" + Per, "_blank", "width=450,height=300,status=yes,resizable=no,top=200,left=250");
		}
		function Deshacer_En_Proceso() {
			$.ajax({
				type: "POST",
				url: rutanormal + 'script/_common/guardarinstancia.aspx/Deshacer_En_Proceso',
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify({ idInstancia: $('#hInstancia').val(), idBloque: $('#hBloque').val() }),
				async: true,
				error: function (a, b) {
					alert(a);
					alert(b);
				}
			});
		}
		function ConfirmarAvisoValidacionMapper() {
			window.parent.fraPMPortalMain.document.forms["frmSubmit"].elements["GEN_MostrarAvisoMapper"].value = false;
			window.parent.fraPMPortalMain.document.forms["frmSubmit"].submit();
		}
	</script>
	<input id="Notificar" runat="server" type="hidden" name="Notificar" />
	<form id="frmSubmit" style="left: 0px; visibility: hidden; position: absolute; top: 0px"
		name="frmAvisos" action="../solicitudes/avisosprecondiciones.aspx" method="post" target="ventanaAvisos">
	</form>
	<form id="Form1" method="post" runat="server">
		<script type="text/javascript">    			
		    switch (Number(tipoVisor)) {
		        case 1:
		        case 4:
		            dibujaMenu(3);
		            break;
			    case 5:
			        dibujaMenu(9);
			        break;
		        case 2:
		        case 3:
			    case 6:
			    case 7:
			        dibujaMenu(2);
			        break;
			    default:
			        dibujaMenu(99);
			}
		</script>
		<fsn:FSNPanelInfo ID="FSNPanelDatosPeticionario" runat="server" ServicePath="~/Consultas.asmx" ServiceMethod="Obtener_DatosPersona" TipoDetalle="2"></fsn:FSNPanelInfo>
		<fsn:FSNPanelInfo ID="FSNPanelDatosProveedor" runat="server" ServicePath="~/Consultas.asmx" ServiceMethod="Obtener_DatosProveedor" TipoDetalle="1"></fsn:FSNPanelInfo>
		<asp:ScriptManager ID="ScriptManager1" runat="server">
		</asp:ScriptManager>
		<br />
		<table width="100%" cellpadding="0" border="0">
			<tr>
				<td>
					<fsn:FSNPageHeader ID="FSNPageHeader" UrlImagenCabecera="images/SolicitudesPMSmall.jpg" runat="server"></fsn:FSNPageHeader>
				</td>
			</tr>
		</table>
		<br />
		<input id="hSolicitud" type="hidden" name="hSolicitud" runat="server" />
		<input id="hInstancia" type="hidden" name="hInstancia" runat="server" />
		<input type="hidden" id="Contrato" runat="server" />
		<input type="hidden" id="CodContrato" runat="server" />
		<input id="hAccionId" type="hidden" name="hAccionId" runat="server" />
		<input id="hBloque" type="hidden" name="hBloque" runat="server" />
		<input id="hBloqueDestino" type="hidden" name="hBloqueDestino" runat="server" />
		<input id="hPeticionario" type="hidden" name="hPeticionario" runat="server" />
		<input id="hGuardar" type="hidden" name="hGuardar" runat="server" />
		<input id="hAccion" type="hidden" name="hAccion" runat="server" />
		<input id="RolActual" type="hidden" name="RolActual" runat="server" />
		<div id="ContenedorAvisos" runat="server"></div>
		<!------------------------------------------------->
		<div id="divCabecera" style="padding-left: 15px; padding-bottom: 15px; display: none" runat="server">
			<asp:Panel ID="pnlCabecera" runat="server" BackColor="#f5f5f5" Font-Names="Arial" Width="95%">
				<table style="height: 15%; width: 100%; padding-bottom: 15px; padding-left: 5px" cellspacing="0" cellpadding="1" border="0">
					<tr>
						<td style="padding-top: 5px; padding-bottom: 5px;" class="fondoCabecera">
							<table style="width: 100%; padding-left: 10px" border="0">
								<tr>
									<td colspan="6">
										<asp:Label ID="lblIDInstanciayEstado" runat="server" CssClass="label" Font-Bold="true"></asp:Label></td>
								</tr>
								<tr>
									<td>
										<asp:Label ID="lblLitCreadoPor" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
									<td>
										<asp:Label ID="lblCreadoPor" runat="server" CssClass="label"></asp:Label>
										<asp:Image ID="imgInfCreadoPor" ImageUrl="images/info.gif" runat="server" /></td>
									<td>
										<asp:Label ID="lblLitEstado" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
									<td>
										<asp:Label ID="lblEstado" runat="server" CssClass="label"></asp:Label></td>
									<td></td>
								</tr>
								<tr>
									<td>
										<asp:Label ID="lblLitFecAlta" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
									<td>
										<asp:Label ID="lblFecAlta" runat="server" CssClass="label"></asp:Label></td>
									<td>
										<asp:Label ID="lblLitTipo" runat="server" CssClass="captionDarkGraySmall" Font-Size="12px"></asp:Label></td>
									<td>
										<asp:Label ID="lblTipo" runat="server" CssClass="label"></asp:Label>
										<asp:Image ID="imgInfTipo" ImageUrl="images/info.gif" runat="server" /></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</asp:Panel>
		</div>

		<cc1:DropShadowExtender TrackPosition="true" ID="DropShadowExtender1" runat="server" Opacity="0.5" Width="3" TargetControlID="pnlCabecera" Rounded="true">
		</cc1:DropShadowExtender>
		<!---------------------------------------------------->
		<div id="divDevolucion" style="display: none" runat="server">
			<input id="cadenaespera" type="hidden" name="cadenaespera" runat="server" />
			<input id="hDestinatario" type="hidden" name="hDestinatario" runat="server" />

			<table id="Table1" style="z-index: 101; left: 16px;"
				cellspacing="1" cellpadding="1" width="100%" border="1">
				<tr>
					<td>
						<table id="tblDatos" cellspacing="1" cellpadding="1" width="100%" border="0"
							runat="server">
							<tr height="15%">
								<td width="20%">
									<asp:Label ID="lblOrigen" runat="server" CssClass="captionGris" Width="100%"></asp:Label></td>
								<td width="80%">
									<asp:Label ID="lblUsuario" runat="server" CssClass="captionGris" Width="100%"></asp:Label></td>
							</tr>
							<tr height="4%">
								<td colspan="2">
									<asp:Label ID="lblComent" runat="server" CssClass="fntLogin" Width="100%"></asp:Label></td>
								<td></td>
							</tr>
							<tr height="20%">
								<td colspan="2">
									<asp:TextBox ID="txtComentarioDevolucion" runat="server" Width="100%" TextMode="MultiLine" Rows="8" Height="100%"></asp:TextBox></td>
								<td></td>
							</tr>
							<tr>
								<td valign="middle" align="center" colspan="2">
									<table id="tblBotones" style="width: 100px; height: 20px" cellspacing="1" cellpadding="1"
										border="0">
										<tr>
											<td>
												<input class="boton" id="cmdAceptarDevolucion" type="button" value="DAceptar" name="cmdAceptarDevolucion" runat="server" /></td>
											<td>
												<input class="boton" id="cmdCancelarDevolucion" type="button" value="DCancelar" onclick="DeshabilitarBotones(); MostrarFSWSMain(); parent.fraPMPortalMain.HabilitarBotones();" name="cmdCancelarDevolucion" runat="server" /></td>
										</tr>
									</table>
								</td>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<div id="divRealizarAccion" style="display: none" runat="server">
			<table id="tblGeneral" style="z-index: 101; left: 16px;"
				cellspacing="1" cellpadding="1" width="95%" border="0" runat="server">
				<tr>
					<td style="height: 15px" valign="bottom">
						<asp:Label ID="lblEtapas" runat="server" Width="100%" CssClass="fntLogin">lblEtapas</asp:Label></td>
				</tr>
				<tr>
					<td style="height: 67px">
						<asp:ListBox ID="lstEtapas" runat="server" Width="100%" CssClass="fntLogin"></asp:ListBox></td>
				</tr>
				<tr>
					<td style="height: 26px" valign="bottom">
						<asp:Label ID="lblRoles" runat="server" Width="100%" CssClass="fntLogin">lblRoles</asp:Label></td>
				</tr>
				<tr>
					<td>
						<ig:WebDataGrid ID="wdgtxtRoles" runat="server" Width="100%"
							AutoGenerateBands="false" AutoGenerateColumns="false" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="true">
							<Behaviors>
								<ig:Selection RowSelectType="single" Enabled="true" CellClickAction="Cell"></ig:Selection>
							</Behaviors>
						</ig:WebDataGrid>
					</td>
				</tr>
				<tr>
					<td style="height: 26px" valign="bottom">
						<asp:Label ID="lblComentarios" runat="server" Width="100%" CssClass="fntLogin">DComentarios para el gestor:</asp:Label></td>
				</tr>
				<tr>
					<td style="height: 100px">
						<textarea id="txtComentario" name="txtComentario" runat="server" style='width: 100%; height: 100px'
							onkeydown='textCounter(this,4000)' onkeyup='textCounter(this,4000)' onchange='return validarLength(this,4000)'
							onkeypress='return validarLength(this,4000)'></textarea></td>
				</tr>
				<tr>
					<td style="height: 65px" align="center">
						<table id="tblBotones" cellspacing="1" cellpadding="1" width="100%" border="0">
							<tr>
								<td align="center">
									<input class="boton" id="cmdAceptar" type="button" onclick="DeshabilitarBotones();" value="Aceptar"
										name="cmdAceptar" runat="server"/>
								</td>
								<td align="left">
									<input class="boton" id="cmdCancelar" type="button" onclick="DeshabilitarBotones(); Deshacer_En_Proceso(); MostrarFSWSMain(); parent.fraPMPortalMain.HabilitarBotones();" value="Cancelar"
										name="cmdCancelar" runat="server" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>
