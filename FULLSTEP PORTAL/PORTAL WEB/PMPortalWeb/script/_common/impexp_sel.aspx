<%@ Page Language="vb" AutoEventWireup="false" Codebehind="impexp_sel.aspx.vb" Inherits="Fullstep.PMPortalWeb.impexp_sel" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
	<head id="Head1" runat="server">
	<title></title>
	<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
	<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
	<meta content="JavaScript" name="vs_defaultClientScript">
	<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	<script type="text/javascript" id="clientEventHandlersJS">
		/*''' <summary>
		''' Imprimir/Exportar la pantalla (certificado.aspx � noconformidad.aspx) q ha llamado a esta pantalla (impexp_sel.aspx)
		''' </summary>
		''' <param name="formato">Explicaci�npar�metro1</param>       
		''' <remarks>Llamada desde: Los 3 botones para imprimir/exportar; Tiempo m�ximo: 0,3</remarks>*/
		function cmd_onclick(formato) {
			var desgloses = "";
			if (formato == "XML") {
				desgloses = "TABLA"
			} else {
				if (document.getElementById("chkFormato").checked==false)
					desgloses = "TABLA"
				else		
					desgloses = "LISTADO"
			}
			window.open('impexp.aspx?Instancia=' + document.getElementById("Instancia").value + '&Certificado=' + document.getElementById("Certificado").value + '&NoConformidad=' + document.getElementById("NoConformidad").value + '&Version=' + document.getElementById("Version").value + '&TipoImpExp=' + document.getElementById("TipoImpExp").value + '&Observadores=' + document.getElementById("Observadores").value + '&Formato=' + formato + '&Desgloses=' + desgloses,'_blank','fullscreen=no,height=200,width=500,location=no,menubar=no,resizable=yes,scrollbars=yes,status=yes,titlebar=yes,toolbar=no,left=250,top=250');
		}
	</script>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<input id="Instancia" type="hidden" name="Instancia" runat="server"/> 
			<input id="NoConformidad" type="hidden" name="NoConformidad" runat="server"/>
			<input id="TipoImpExp" type="hidden" name="TipoImpExp" runat="server"/> 
			<input id="Version" type="hidden" name="Version" runat="server"/>
			<input id="Certificado" type="hidden" name="Certificado" runat="server"/> 
			<input id="Observadores" type="hidden" name="Observadores" runat="server"/>
			<input class="boton" id="cmdHTML" style="Z-INDEX: 101; LEFT: 128px; WIDTH: 60px; POSITION: absolute; TOP: 24px"
				onclick="return cmd_onclick('HTML')" type="button" size="20" value="HTML" name="cmdHTML"/>
			<input class="boton" id="cmdPDF" style="Z-INDEX: 102; LEFT: 32px; WIDTH: 60px; POSITION: absolute; TOP: 24px"
				onclick="return cmd_onclick('PDF')" type="button" value="PDF" name="cmdPDF"/>
			<input class="boton" id="cmdXML" style="Z-INDEX: 103; LEFT: 224px; WIDTH: 60px; POSITION: absolute; TOP: 24px"
				onclick="return cmd_onclick('XML')" type="button" value="XML" name="cmdXML"/>
			<asp:checkbox id="chkFormato" style="Z-INDEX: 104; LEFT: 32px; POSITION: absolute; TOP: 64px"
				runat="server" Text="Mostrar desgloses en listados" Font-Size="XX-Small" Font-Names="Tahoma"
				Height="16px" Width="168px"></asp:checkbox>
		</form>
	</body>
</html>