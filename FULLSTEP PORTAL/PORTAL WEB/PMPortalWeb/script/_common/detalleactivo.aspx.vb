Namespace Fullstep.PMPortalWeb
    Partial Class detalleactivo
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
            Dim lCiaComp As Long = IdCiaComp
            Dim sIdi As String = Idioma
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If

            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSWSServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Activos, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            Me.lblTitulo.Text = oTextos.Rows(16).Item(1)
            Me.lblCodigo.Text = oTextos.Rows(1).Item(1) & ":"
            Me.lblDenominacion.Text = oTextos.Rows(2).Item(1) & ":"
            Me.lblCentro.Text = oTextos.Rows(10).Item(1) & ":"
            Me.lblEmpresa.Text = oTextos.Rows(4).Item(1) & ":"

            
            If Len(Request("codActivo")) > 0 Then
                Dim oActivo As Fullstep.PMPortalServer.Activo
                oActivo = FSWSServer.Get_Activo
                Dim ds As DataSet = oActivo.DetalleActivo(lCiaComp, sIdi, Request("codActivo"))
                If ds.Tables(0).Rows.Count > 0 Then
                    lblCodigoBD.Text = ds.Tables(0).Rows(0).Item("COD")
                    lblDenominacionBD.Text = DBNullToSomething(ds.Tables(0).Rows(0).Item("DEN"))
                    lblCentroBD.Text = DBNullToSomething(ds.Tables(0).Rows(0).Item("CENTRO_SM"))
                    If Len(DBNullToSomething(ds.Tables(0).Rows(0).Item("DEN_CENTRO"))) > 0 Then
                        lblCentroBD.Text = lblCentroBD.Text & " - " & ds.Tables(0).Rows(0).Item("DEN_CENTRO")
                    End If
                    lblEmpresaBD.Text = DBNullToSomething(ds.Tables(0).Rows(0).Item("DEN_EMP"))
                End If
            End If
        End Sub

    End Class
End Namespace
