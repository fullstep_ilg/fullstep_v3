Namespace Fullstep.PMPortalWeb
    Partial Class tablaexternaserver
        Inherits FSPMPage


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim lCiaComp As Long = IdCiaComp
            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
            Dim iTabla As Integer = CInt(Request.QueryString("tabla"))
            Dim sArt As String = String.Empty
            If Request.QueryString("art").ToString() <> "" Then _
                sArt = Request.QueryString("art").ToString()
            Dim oTablaExterna As Fullstep.PMPortalServer.TablaExterna = FSWSServer.get_TablaExterna
            oTablaExterna.LoadDefTabla(lCiaComp, iTabla, sArt, True)
            oTablaExterna.LoadData(lCiaComp, sArt)
            Dim oValorKey As Object = oTablaExterna.BuscarRegistro(Request.QueryString("valor").ToString(), True)
            Dim sValor As String
            Dim sValorMostrar As String
            Dim sScript As String
            Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & _
                "<script language=""{0}"">{1}</script>"
            Dim sIdi As String = oUser.Idioma
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If
            If Not oValorKey Is Nothing Then
                Dim sTipoClave As String
                Dim sTipoMostrar As String
                For Each oRow As DataRow In oTablaExterna.DefTabla.Tables(0).Rows
                    If UCase(oRow.Item("CAMPO")) = UCase(oTablaExterna.PrimaryKey) Then
                        sTipoClave = oRow.Item("TIPOCAMPO")
                        Exit For
                    End If
                Next
                If sTipoClave = "FECHA" Then
                    sValor = FormatDate(CType(oValorKey, Date), oUser.DateFormat)
                Else
                    sValor = oValorKey
                End If
                sValorMostrar = oTablaExterna.DescripcionReg(oValorKey, sIdi, oUser.DateFormat, oUser.NumberFormat)
                sScript = "window.parent.valor_externa_seleccionado('" & JSText(Request.QueryString("fsEntryID")) & "', '" & JSText(sValor) & "', '" & JSText(sValorMostrar) & "', '" & JSText(IIf(sArt Is Nothing, "", sArt)) & "');"
                sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                Page.ClientScript.RegisterStartupScript(Me.GetType(),"ValorTablaExternaAct", sScript)
            Else
                Dim sMensaje As String = ""
                Dim oDict As Fullstep.PMPortalServer.Dictionary = FSWSServer.Get_Dictionary()
                oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.TablaExterna, sIdi)
                Dim oTextos As DataTable = oDict.Data.Tables(0)
                sMensaje = oTablaExterna.Nombre & ControlChars.CrLf & _
                    oTextos.Rows(1).Item(1)
                sScript = "window.parent.errorval_externa('" & JSText(Request.QueryString("fsEntryID")) & "', '" & JSText(sMensaje) & "');"
                sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                Page.ClientScript.RegisterStartupScript(Me.GetType(),"ValorTablaExternaAct", sScript)
            End If
        End Sub

    End Class
End Namespace