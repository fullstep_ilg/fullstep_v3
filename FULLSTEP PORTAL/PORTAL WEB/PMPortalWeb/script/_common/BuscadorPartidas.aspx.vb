﻿Public Class BuscadorPartidas
    Inherits FSPMPage

    Private sPartida As String
    Private sCentroCoste As String
    Private sCentroCosteDen As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sCentroCoste = Request("CentroCoste")
        sCentroCosteDen = Request("CentroCosteDen")
        Partidas.CentroCoste = sCentroCoste
        Partidas.CentroCosteDen = sCentroCosteDen
        Partidas.urlImagenBuscarCentro = System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "UserControls/images/lupa_15x14.gif"
        Partidas.urlImagenCentroCosteSmall = System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "UserControls/images/centrocoste_small.JPG"
        Partidas.urlImagenCentroCoste = System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "UserControls/images/centrocoste.JPG"
        Partidas.urlImagenCandado = System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "UserControls/images/candado.jpg"

        'Dim ci As System.Globalization.CultureInfo = New System.Globalization.CultureInfo(System.Threading.Thread.CurrentThread.CurrentCulture.LCID)
        'Dim dateTimeCI As System.Globalization.DateTimeFormatInfo = ci.DateTimeFormat.CurrentInfo.Clone


        'dateTimeCI.FullDateTimePattern = FSPMUser.DateFormat.ShortDatePattern
        'dateTimeCI.ShortDatePattern = FSPMUser.DateFormat.ShortDatePattern

        'ci.DateTimeFormat = dateTimeCI
        'Partidas.Culture = ci

        idControlPartida.Value = Request("idHidPartida")
        idControlPartidaDen.Value = Request("idTxtPartida")
        idControlCentroCoste.Value = Request("idTxtCentroCoste")
        idControlCentroCosteDen.Value = Request("idHidCentroCoste")
        
        sPartida = Request("Partida")
        If Not IsPostBack Then
            CargarPartidas()
            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSPMServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Partidas, Idioma)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            Partidas.Textos = oTextos
        End If
    End Sub

    ''' <summary>
    ''' Pasa al userControl de partidas los datos de las partidas
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CargarPartidas()
        Dim oPress5 As Fullstep.PMPortalServer.PRES5
        oPress5 = FSPMServer.Get_Press5
        Partidas.DatosTreeView = oPress5.Partidas_LoadData(IdCiaComp, sPartida, "", Me.Idioma, Partidas.CentroCoste, Partidas.NoVigentes, Partidas.FechaInicio, Partidas.FechaHasta)
    End Sub

    Private Sub BuscarPartidas() Handles Partidas.eventBuscarPartidas
        CargarPartidas()
    End Sub
End Class