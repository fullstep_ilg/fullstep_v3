Partial Class ComprobarEnProceso
    Inherits FSPMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    ''' <summary>
    ''' Cargar la pagina. 
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>        
    ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oInstancia As Fullstep.PMPortalServer.Instancia
        Dim iEnProceso As Integer
        Dim sInstancia As String = Request("Instancia")
        Dim sID As String = Request("ID")
        Dim sTipo As String = Request("Tipo")
        Dim sFecAlta As String = Request("FECALTA")
        Dim sTipoSolicitud As String = Request("TipoSolicitud")
        Dim sContrato As String = Request("Contrato")
        Dim sCodContrato As String = Request("Codigo")

        oInstancia = FSPMServer.Get_Instancia
        oInstancia.ID = CLng(sInstancia)
        iEnProceso = oInstancia.ComprobarEnProceso(IdCiaComp)

        Dim sScript As String
        sScript = "<script>window.parent.mostrarVentana(" & iEnProceso.ToString() & ",'" & sID & "','" & sInstancia & "','" & sTipo & "','" & sFecAlta & "','" & sTipoSolicitud & "','" & sCodContrato & "','" & sContrato & "'" & ")</script>"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EnProceso", sScript)
    End Sub

End Class
