﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BuscadorEmpresas.aspx.vb" Inherits=".BuscadorEmpresas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
    <title></title>
    <link href="../../../common/estilo.asp" type="text/css" rel="stylesheet"/>
</head>

<body>
    <form id="Form1" runat="server">   
        <asp:ScriptManager runat="server" ID="scriptManager1"></asp:ScriptManager>
        <div>
            <fspm:BusquedaEmpresas id="BusquedaEmpresas" runat="server"></fspm:BusquedaEmpresas>
            <input type="hidden" id="idControl" runat="server" />
            <input type="hidden" id="idHidControl" runat="server" />
        </div>
    </form>
</body>
</html>
