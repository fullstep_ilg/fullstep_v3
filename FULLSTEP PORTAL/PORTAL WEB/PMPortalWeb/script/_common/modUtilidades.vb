Namespace Fullstep.PMPortalWeb
    Module modUtilidades
        Public Function DBNullToSomething(Optional ByVal value As Object = Nothing) As Object
            If IsDBNull(value) Then
                Return Nothing
            Else
                Return value
            End If
        End Function

        Public Function DBNullToStr(Optional ByVal value As Object = Nothing) As String
            If IsDBNull(value) Then
                Return ""
            Else
                Return value
            End If
        End Function

        Public Function ValidFilename(ByVal sFileName As String)
            Dim s As String

            s = sFileName

            s = Replace(s, "\", "_")
            s = Replace(s, "/", "_")
            s = Replace(s, ":", "_")
            s = Replace(s, "*", "_")
            s = Replace(s, "?", "_")
            s = Replace(s, """", "_")
            s = Replace(s, ">", "_")
            s = Replace(s, "<", "_")
            s = Replace(s, "|", "_")
            s = Replace(s, "@", "_")
            s = Replace(s, "#", "_")

            ValidFilename = s


        End Function

        Public Function FormatNumber(ByVal dNumber As Double, ByVal oNumberFormat As System.Globalization.NumberFormatInfo) As String

            Return dNumber.ToString("N", oNumberFormat)

        End Function

        Public Function FormatDate(ByVal dDate As Date, ByVal oDateFormat As System.Globalization.DateTimeFormatInfo) As String

            Return dDate.ToString("d", oDateFormat)

        End Function

        Function JSText(ByVal text As String)

            Dim t
            If text = Nothing Then
                JSText = ""
                Exit Function
            End If
            t = Replace(text, "'", "&#39;")
            t = Replace(t, """", "&#34;")
            t = Replace(t, Chr(13) & Chr(10), "\n")
            t = Replace(t, Chr(10), "\n")
            t = Replace(t, Chr(13), "\n")

            JSText = t


        End Function
        Function JSTextP(ByVal text)

            Dim t
            If text = Nothing Then
                JSTextP = ""
                Exit Function
            End If
            t = Replace(text, "'", "&#92;&#39;")
            t = Replace(t, """", "&#92;&#34;")
            t = Replace(t, Chr(13) & Chr(10), "\n")
            t = Replace(t, Chr(10), "\n")
            t = Replace(t, Chr(13), "\n")

            JSTextP = t


        End Function

        Function DevolverFechaRelativaA(ByVal iTipoFecha As Fullstep.PMPortalServer.TiposDeDatos.TipoFecha, ByVal dtFechaFija As Date) As Date
            If Not dtFechaFija = Nothing Then
                Return dtFechaFija
            End If
            Select Case iTipoFecha
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.FechaAlta
                    Return Now
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.UnDiaDespues
                    Return DateAdd(DateInterval.Day, 1, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.DosDiasDespues
                    Return DateAdd(DateInterval.Day, 2, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.TresDiasDespues
                    Return DateAdd(DateInterval.Day, 3, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.CuatroDiasDespues
                    Return DateAdd(DateInterval.Day, 4, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.CincoDiasDespues
                    Return DateAdd(DateInterval.Day, 5, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.SeisDiasDespues
                    Return DateAdd(DateInterval.Day, 6, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.UnaSemanaDespues
                    Return DateAdd(DateInterval.Day, 7, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.DosSemanaDespues
                    Return DateAdd(DateInterval.Day, 14, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.TresSemanaDespues
                    Return DateAdd(DateInterval.Day, 21, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.UnMesDespues
                    Return DateAdd(DateInterval.Month, 1, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.DosMesDespues
                    Return DateAdd(DateInterval.Month, 2, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.TresMesDespues
                    Return DateAdd(DateInterval.Month, 3, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.CuatroMesDespues
                    Return DateAdd(DateInterval.Month, 4, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.CincoMesDespues
                    Return DateAdd(DateInterval.Month, 5, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.SeisMesDespues
                    Return DateAdd(DateInterval.Month, 6, Today)
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoFecha.UnAnyoDespues
                    Return DateAdd(DateInterval.Year, 1, Today)
                Case Else
                    Return dtFechaFija
            End Select

        End Function
        Public Function VacioToNothing(ByVal s As Object)
            Dim o As Object = DBNullToSomething(s)
            If o Is Nothing Then Return Nothing
            If o.ToString() = "" Then
                Return Nothing
            Else
                Return o
            End If
        End Function
        Public Function igDateToDate(ByVal s As String) As DateTime

            Try
                If s = Nothing Or s = "" Then
                    Return Nothing
                End If
                Dim arr As Array = s.Split("-")

                Return DateSerial(arr(0), arr(1), arr(2))


            Catch ex As Exception
                Return Nothing

            End Try

        End Function
        Public Function DateToigDate(ByVal dt As DateTime) As String
            Try
                If dt = Nothing Then
                    Return ""
                End If
                '2005-7-14-12-7-14-244
                'anyo-m-di-ho-m-se-mil

                Dim s As String = Year(dt).ToString() + "-" + Month(dt).ToString() + "-" + Day(dt).ToString() + "-" + Hour(dt).ToString() + "-" + Minute(dt).ToString() + "-" + Second(dt).ToString()
                Return s

            Catch ex As Exception

                Return ""
            End Try
        End Function

        Public Function Numero(ByVal value As String, Optional ByVal sDecimalSeparator As String = "", Optional ByVal sThousanSeparator As String = "") As Double
            If value = Nothing Then
                Return Nothing
            Else
                If sDecimalSeparator <> "" Then
                    Try
                        value = value.Replace(sThousanSeparator, "")
                    Catch
                    End Try

                    value = value.Replace(sDecimalSeparator, System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    Return CDbl(value)
                Else
                    If IsNumeric(value) Then
                        Return CDbl(value)
                    Else
                        Return Nothing
                    End If
                End If
            End If
        End Function
        Function JSNum(ByVal Num As Double) As String

            Dim t As String
            Try
                If IsDBNull(Num) Then
                    Return "null"
                End If
                t = Replace(Num.ToString(), ",", ".")
                Return t

            Catch ex As Exception

                Return "null"
            End Try


        End Function

    End Module
End Namespace
