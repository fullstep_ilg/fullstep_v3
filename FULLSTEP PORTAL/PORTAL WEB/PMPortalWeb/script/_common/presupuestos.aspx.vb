Namespace Fullstep.PMPortalWeb
    Partial Class presupuestos
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents UltraWebTab1 As Infragistics.WebUI.UltraWebTab.UltraWebTab

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region


        Private mlId As Long

        Property Campo() As Long
            Get
                Return mlId
            End Get
            Set(ByVal Value As Long)
                mlId = Value
            End Set
        End Property

        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script language=""{0}"">{1}</script>"
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Me.Page.ClientScript.RegisterClientScriptResource(GetType(Fullstep.DataEntry.GeneralEntry), "Fullstep.DataEntry.formatos.js")
            'Put user code to initialize the page here
            Dim FSWSServer As Fullstep.PMPortalServer.Root = Session("FS_Portal_Server")
            Dim oUser As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
            Dim lCiaComp As Long = IdCiaComp
            Dim sIdi As String = Idioma

            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If
            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSWSServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Presupuestos, sIdi)
            Dim oTextos As DataTable = oDict.Data.Tables(0)

            Me.lblAsignado.Text = oTextos.Rows(11).Item(1)
            Me.lblPendiente.Text = oTextos.Rows(12).Item(1)
            Me.lblTotal.Text = oTextos.Rows(10).Item(1)
            Me.uwtPresupuestos.Tabs.FromKey("pres").Text = oTextos.Rows(9).Item(1)
            Me.cmdAceptar.Value = oTextos.Rows(2).Item(1)
            Me.cmdCancelar.Value = oTextos.Rows(3).Item(1)

            Dim oCampo As Fullstep.PMPortalServer.Campo
            Dim oUnidades As Fullstep.PMPortalServer.UnidadesOrg

            'oUser.LoadUONPersona(lCiaComp)


            Dim lInstancia As Integer
            Dim lSolicitud As Integer

            Dim idCampo As Integer = CInt(Request("Campo"))
            Dim iTipo As Integer

            If Request("Solicitud") <> Nothing Then
                lSolicitud = Request("Solicitud")
            End If

            If Request("Instancia") <> Nothing Then
                lInstancia = Request("Instancia")
            End If


            oCampo = FSWSServer.Get_Campo()
            oCampo.Id = idCampo

            If lInstancia > 0 Then
                oCampo.LoadInst(lCiaComp, lInstancia, sIdi)
            Else
                oCampo.Load(lCiaComp, sIdi, lSolicitud)
            End If

            Select Case oCampo.IdTipoCampoGS
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.PRES1
                    iTipo = 1
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Pres2
                    iTipo = 2
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Pres3
                    iTipo = 3
                Case Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Pres4
                    iTipo = 4
            End Select

            oUnidades = FSWSServer.get_UnidadesOrg()
            oUnidades.CargarArbolUnidadesConPresupuestos(lCiaComp, iTipo, oUser.AprobadorActual)

            Dim idControl = Request("IDPres")

            Me.IDCONTROL.Value = idControl



            Dim bConCantidad As Boolean

            Dim dCantidad As Decimal


            bConCantidad = (Request("CANTIDAD") <> Nothing)

            If bConCantidad Then
                dCantidad = Numero(Request("CANTIDAD"), ".", ",")
                If dCantidad = 0 Then
                    bConCantidad = False
                End If
            End If



            If bConCantidad Then
                Me.lblTotal.Visible = True
                Me.txtTotal.Visible = True
                Me.txtAsignado.Visible = True
                Me.txtPendiente.Visible = True
                Me.txtTotal.NumberFormat = oUser.NumberFormat
                Me.txtPendiente.NumberFormat = oUser.NumberFormat
                Me.txtAsignado.NumberFormat = oUser.NumberFormat
                Me.txtTotal.Value = dCantidad

            Else

                Me.tblPresup.Rows(0).Cells.RemoveAt(7)
                Me.tblPresup.Rows(0).Cells.RemoveAt(3)
                Me.tblPresup.Rows(0).Cells.RemoveAt(1)
                Me.tblPresup.Rows(0).Cells.RemoveAt(0)
                Me.tblPresup.Rows(0).Cells(2).Width = "30%"
                Me.tblPresup.Rows(0).Cells(5).Width = "30%"

                Me.lblTotal.Visible = False
                Me.txtTotal.Visible = False
                Me.txtAsignado.Visible = False
                Me.txtPendiente.Visible = False

            End If




            If bConCantidad Then
                Me.lblTitulo.Text = oCampo.DenSolicitud(sIdi) & "&nbsp;&nbsp;&nbsp;&nbsp;" & Request("TITULO")

            Else
                Me.lblTitulo.Text = oCampo.DenSolicitud(sIdi)

            End If

            Me.TIPO.Value = iTipo
            Dim Valor As String = Request("Valor")


            Dim sScript As String
            sScript = ""



            Dim bMostrarUON As Boolean = False


            If Valor <> "" Then
                Dim arrPresupuestos() As String = Valor.Split("#")
                Dim arrPresup As String
                Dim arrPresupuesto() As String
                Dim iNivel As Integer
                Dim lIdPresup As Integer
                Dim dPorcent As Double
                Dim iAnyo As Integer
                Dim sUon1 As String
                Dim sUon2 As String
                Dim sUon3 As String
                Dim sPres1 As String
                Dim sPres2 As String
                Dim sPres3 As String
                Dim sPres4 As String

                Dim sDenUon As String
                Dim sDenPres As String

                Dim oPres1 As Fullstep.PMPortalServer.PresProyectosNivel1
                Dim oPres2 As Fullstep.PMPortalServer.PresContablesNivel1
                Dim oPres3 As Fullstep.PMPortalServer.PresConceptos3Nivel1
                Dim oPres4 As Fullstep.PMPortalServer.PresConceptos4Nivel1
                Dim oDS As DataSet
                Valor = ""



                For Each arrPresup In arrPresupuestos
                    arrPresupuesto = arrPresup.Split("_")



                    sUon1 = Nothing
                    sUon2 = Nothing
                    sUon3 = Nothing
                    sDenUon = Nothing
                    sPres1 = Nothing
                    sPres2 = Nothing
                    sPres3 = Nothing
                    sPres4 = Nothing
                    iAnyo = Nothing
                    sDenPres = Nothing



                    iNivel = arrPresupuesto(0)
                    lIdPresup = arrPresupuesto(1)
                    dPorcent = Numero(arrPresupuesto(2), ".", ",")


                    'A partir del id del presupuesto y el nivel obtenemos los c�digos de unidad organizativa y de presupuesto en el dataset DS
                    Select Case oCampo.IdTipoCampoGS
                        Case Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.PRES1
                            iTipo = 1

                            oPres1 = FSWSServer.Get_PresProyectosNivel1
                            Select Case iNivel
                                Case 1
                                    oPres1.LoadData(lCiaComp, lIdPresup)
                                Case 2
                                    oPres1.LoadData(lCiaComp, Nothing, lIdPresup)
                                Case 3
                                    oPres1.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
                                Case 4
                                    oPres1.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
                            End Select
                            oDS = oPres1.Data
                            iAnyo = oDS.Tables(0).Rows(0).Item("ANYO")
                            oPres1 = Nothing
                        Case Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Pres2
                            iTipo = 2
                            oPres2 = FSWSServer.Get_PresContablesNivel1
                            Select Case iNivel
                                Case 1
                                    oPres2.LoadData(lCiaComp, lIdPresup)
                                Case 2
                                    oPres2.LoadData(lCiaComp, Nothing, lIdPresup)
                                Case 3
                                    oPres2.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
                                Case 4
                                    oPres2.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
                            End Select
                            oDS = oPres2.Data
                            iAnyo = oDS.Tables(0).Rows(0).Item("ANYO")
                            oPres2 = Nothing
                        Case Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Pres3
                            iTipo = 3
                            oPres3 = FSWSServer.Get_PresConceptos3Nivel1
                            Select Case iNivel
                                Case 1
                                    oPres3.LoadData(lCiaComp, lIdPresup)
                                Case 2
                                    oPres3.LoadData(lCiaComp, Nothing, lIdPresup)
                                Case 3
                                    oPres3.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
                                Case 4
                                    oPres3.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
                            End Select
                            oDS = oPres3.Data
                            oPres3 = Nothing
                        Case Fullstep.PMPortalServer.TiposDeDatos.TipoCampoGS.Pres4
                            iTipo = 4
                            oPres4 = FSWSServer.Get_PresConceptos4Nivel1
                            Select Case iNivel
                                Case 1
                                    oPres4.LoadData(lCiaComp, lIdPresup)
                                Case 2
                                    oPres4.LoadData(lCiaComp, Nothing, lIdPresup)
                                Case 3
                                    oPres4.LoadData(lCiaComp, Nothing, Nothing, lIdPresup)
                                Case 4
                                    oPres4.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPresup)
                            End Select
                            oDS = oPres4.Data
                            oPres4 = Nothing
                    End Select
                    If Not oDS Is Nothing Then
                        With oDS.Tables(0).Rows(0)

                            If Not IsDBNull(.Item("UON1")) Then
                                sUon1 = .Item("UON1")
                                If Not IsDBNull(.Item("UON2")) Then
                                    sUon2 = .Item("UON2")
                                    If Not IsDBNull(.Item("UON3")) Then
                                        sUon3 = .Item("UON3")
                                    End If
                                End If
                            End If
                            sDenPres = DBNullToSomething(.Item("DEN"))
                            sDenUon = DBNullToSomething(.Item("UONDEN"))
                            If Not IsDBNull(.Item("PRES4")) Then
                                sPres4 = .Item("PRES4")
                                sPres3 = .Item("PRES3")
                                sPres2 = .Item("PRES2")
                                sPres1 = .Item("PRES1")

                            ElseIf Not IsDBNull(.Item("PRES3")) Then
                                sPres3 = .Item("PRES3")
                                sPres2 = .Item("PRES2")
                                sPres1 = .Item("PRES1")
                            ElseIf Not IsDBNull(.Item("PRES2")) Then
                                sPres2 = .Item("PRES2")
                                sPres1 = .Item("PRES1")
                            ElseIf Not IsDBNull(.Item("PRES1")) Then
                                sPres1 = .Item("PRES1")
                            End If

                            oDS = Nothing
                        End With
                        Dim bSinPermisos As Boolean
                        bSinPermisos = False
                        If sUon3 <> Nothing And oUser.UON3Aprobador <> Nothing Then
                            If sUon3 <> oUser.UON3Aprobador Then
                                'malo (el usuario no tiene permisos para asignar el presupuesto por defecto )
                                bSinPermisos = True
                            End If
                        Else
                            If sUon3 <> Nothing Then
                                bMostrarUON = True
                            End If
                        End If
                        If sUon2 <> Nothing And oUser.UON2Aprobador <> Nothing Then
                            If sUon2 <> oUser.UON2Aprobador Then

                                bSinPermisos = True
                            End If
                        Else
                            If sUon2 <> Nothing Then
                                bMostrarUON = True
                            End If
                        End If
                        If sUon1 <> Nothing And oUser.UON1Aprobador <> Nothing Then
                            If sUon1 <> oUser.UON1Aprobador Then

                                bSinPermisos = True
                            End If
                        End If
                        If Not bSinPermisos Then
                            'sErrorScript = "alert('" +  jstext("
                            sScript += "arrDistribuciones[arrDistribuciones.length]= new Distribucion('" + JSText(sUon1) + "','" + JSText(sUon2) + "','" + JSText(sUon3) + "'," + JSNum(iAnyo) + ",'" + JSText(sPres1) + "','" + JSText(sPres2) + "','" + JSText(sPres3) + "','" + JSText(sPres4) + "'," + JSNum(dPorcent) + "," + JSNum(iNivel) + "," + JSNum(lIdPresup) + ",'" + JSText(sDenUon) + "','" + JSText(sDenPres) + "');"
                            Valor += arrPresup + "#"
                        End If
                    End If
                Next


                sScript = String.Format(IncludeScriptKeyFormat, "javascript", sScript)
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "distribucionPresupuestos", sScript)
                If Valor <> Nothing Then
                    Valor = Left(Valor, Valor.Length - 1)
                End If

            End If
            Dim oTabItem As Infragistics.WebUI.UltraWebTab.Tab
            oTabItem = New Infragistics.WebUI.UltraWebTab.Tab
            Me.uwtPresupuestos.Tabs.Insert(0, oTabItem)
            oTabItem.Text = oTextos.Rows(4).Item(1)
            oTabItem.Key = "uon"
            oTabItem.ContentPane.UserControlUrl = "uon.ascx"
            Dim oUon As Fullstep.PMPortalWeb.uon


            oUon = oTabItem.ContentPane.UserControl
            oUon.Unidades = oUnidades
            oUon.SeleccionarUon = (bMostrarUON = False)

            If oUser.UON1Aprobador <> Nothing And Not bMostrarUON Then
                oTabItem.Visible = False
                oTabItem = Me.uwtPresupuestos.Tabs.FromKey("pres")
                oTabItem.Visible = True
                oTabItem.ContentPane.TargetUrl = "pres.aspx?TIPO=" & iTipo.ToString & "&PRES=" & Server.UrlEncode(Valor) & "&UON1=" & oUser.UON1Aprobador & "&UON2=" & oUser.UON2Aprobador & "&UON3=" & oUser.UON3Aprobador


            End If



        End Sub

    End Class
End Namespace
