﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Paginador.ascx.vb" Inherits="Fullstep.PMPortalWeb.Paginador" %>
    <script type="text/javascript">
            /*<summary>
            Provoca el postback para hacer la esportación a excel
            </summary>
            <remarks>Llamada desde: Icono de exportar a Excel</remarks>
            --%>*/
        function MostrarExcelPaginador() {            
                __doPostBack("", "Excel");
            }

            /*<summary>
            Provoca el postback para hacer la exportación a Pdf
            </summary>
            <remarks>Llamada desde: Icono de exportar a Pdf</remarks>
            --%>*/
            function MostrarPdfPaginador() {
                __doPostBack("", "Pdf");
            }
    </script>

<asp:UpdatePanel ID="upPaginador" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
<ContentTemplate>
    <div style="float:left; padding:3px 0px 2px 0px; margin-left:10px;">
        <table width="310px" cellpadding="2" border="0" cellspacing="0">
            <tr>
                <td>&nbsp;</td><td>&nbsp;</td>
                <td>
                <asp:LinkButton runat="server" ID="btnFirstPage" CommandArgument="first" OnClick="changePage" style="text-align:right;">
                    <asp:Image id="imFirstPage" runat=server style="border:0px"/>
                </asp:LinkButton>
                </td>
                <td>
                <asp:LinkButton runat="server" ID="btnPreviousPage" CommandArgument="prev" OnClick="changePage" style="text-align:right;">
                    <asp:Image id="imPreviousPage" runat=server style="border:0px"/>
                </asp:LinkButton>
                </td>
                <td><asp:Label cssClass="common" ID="lblPagina" runat="server" Text="DPágina"></asp:Label></td>
                <td><asp:DropDownList ID="ddlPage" runat="server" CausesValidation="False" AutoPostBack="True" OnSelectedIndexChanged="changePage"></asp:DropDownList></td>
                <td align="left" style="width:50px">
                    <asp:Label cssClass="common" ID="lblPagTot" runat="server" Text="D1" ></asp:Label>
                </td>
                <td style="width:60px">
                    <asp:Label cssClass="common" ID="lblTotal" runat="server" Text="D(Total )"></asp:Label>
                </td>
                <td><asp:LinkButton runat="server" ID="btnNextPage" CommandArgument="next" OnClick="changePage" style="text-align:right;">
                    <asp:Image id="imNextPage" runat=server style="border:0px"/>
                </asp:LinkButton>
                </td>
                <td><asp:LinkButton runat="server" ID="btnLastPage" CommandArgument="last" OnClick="changePage" style="text-align:right;">
                    <asp:Image id="imLastPage" runat=server style="border:0px"/>
                </asp:LinkButton>
                </td>

            </tr>
        </table>
    </div>
    <div style="height:22px; float:right; line-height:22px;">
        
            <div class="botonDerechaCabecera" ID="ibExcel" runat="server" style="background-repeat:no-repeat;height:20px;" onclick="MostrarExcelPaginador();return false;">
                <span ID="litExcel" runat="server" style="margin-left:20px;">Excel</span>
            </div>
  
            <div ID="ibPDF" class="botonDerechaCabecera" runat="server" style="background-repeat:no-repeat;height:20px;" onclick="MostrarPdfPaginador();return false;">
                <span ID="litPDF" runat="server" style="margin-left:20px;">Pdf</span>
            </div>
        
    </div>
    <div style="clear:both;"></div>
</ContentTemplate>
</asp:UpdatePanel>