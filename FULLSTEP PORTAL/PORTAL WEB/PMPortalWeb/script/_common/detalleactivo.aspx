<%@ Page Language="vb" AutoEventWireup="false" Codebehind="detalleactivo.aspx.vb" Inherits="Fullstep.PMPortalWeb.detalleactivo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="form1" runat="server">
			<table cellspacing="1" cellpadding="4" width="100%" border="0">
				<tr>
					<td width="5%">
						<img src="images/dolar.JPG">
					</td>
					<td width="90%" align="center" nowrap>
						<asp:Label ID="lblTitulo" runat="server" CssClass="titulo"></asp:Label>
					</td>
					<td width="5%">
						<img src="images/Bt_Cerrar.png" onclick="javascript:self.close();">
					</td>
				</tr>
			</table>
			<table cellspacing="1" cellpadding="4" width="100%" border="0">
				<tr>
					<td style="WIDTH:35%" class="ugfilatablaCabecera">
						<asp:Label ID="lblCodigo" runat="server"></asp:Label>
					</td>
					<td class="ugfilatablaHist">
						<asp:Label ID="lblCodigoBD" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td style="WIDTH:35%" class="ugfilatablaCabecera">
						<asp:Label ID="lblDenominacion" runat="server"></asp:Label>
					</td>
					<td class="ugfilatablaHist">
						<asp:Label ID="lblDenominacionBD" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td style="WIDTH:35%" class="ugfilatablaCabecera">
						<asp:Label ID="lblCentro" runat="server"></asp:Label>
					</td>
					<td class="ugfilatablaHist">
						<asp:Label ID="lblCentroBD" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td style="WIDTH:35%" class="ugfilatablaCabecera">
						<asp:Label ID="lblEmpresa" runat="server"></asp:Label>
					</td>
					<td class="ugfilatablaHist">
						<asp:Label ID="lblEmpresaBD" runat="server"></asp:Label>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
