﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Fullstep.PMPortalWeb

    Partial Public Class desgloseControl

        '''<summary>
        '''tblGeneral control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tblGeneral As Global.System.Web.UI.HtmlControls.HtmlTable

        '''<summary>
        '''divDesglose control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divDesglose As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''tblTituloDesglose control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tblTituloDesglose As Global.System.Web.UI.HtmlControls.HtmlTable

        '''<summary>
        '''tblDesglose control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tblDesglose As Global.System.Web.UI.HtmlControls.HtmlTable

        '''<summary>
        '''cmdAnyadir control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents cmdAnyadir As Global.System.Web.UI.HtmlControls.HtmlInputButton

        '''<summary>
        '''tblDesgloseHidden control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tblDesgloseHidden As Global.System.Web.UI.HtmlControls.HtmlTable
    End Class
End Namespace
