<%@ Page Language="vb" AutoEventWireup="false" Codebehind="presupuestos.aspx.vb" Inherits="Fullstep.PMPortalWeb.presupuestos" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script>
		/*''' <summary>
		''' Seleccionar Presupuesto en la pantalla q llamo
		''' </summary>
		''' <remarks>Llamada desde:cmdAceptar; Tiempo m�ximo:0</remarks>*/
		function Aceptar()
		{
		
		var sValor = ""
		var sTexto = ""
		for (var i in arrDistribuciones)
		
			{
			oDistr = arrDistribuciones[i]
			if (oDistr.anyo>0)
				sTexto += oDistr.anyo.toString() + " - " 
			switch(oDistr.nivel)
				{
				case 1:
					sTexto += oDistr.pres1
					break; 
				case 2:
					sTexto += oDistr.pres2
					break; 
				case 3:
					sTexto += oDistr.pres3
					break; 
				case 4:
					sTexto += oDistr.pres4
					break; 
				}
			sTexto += " (" + num2str(oDistr.porcent * 100,".",",",2) + "%);"
			sValor += oDistr.nivel.toString() + "_" + oDistr.idpres.toString() + "_" + num2str(oDistr.porcent,".",",",2).toString() + "#"
			}
		
		sValor = sValor.substr(0,sValor.length-1)

		

		window.opener.pres_seleccionado(document.forms("Form1").item("IDCONTROL").value, sValor, sTexto)
		window.close()

		
		
		}
		function montarValorPres()
			{
				
			var sValor = ""

			for (var i in arrDistribuciones)
			
				{
				oDistr = arrDistribuciones[i]
				sValor += oDistr.nivel.toString() + "_" + oDistr.idpres.toString() + "_" + num2str(oDistr.porcent,".",",",2).toString() + "#"
				}
			
			sValor = sValor.substr(0,sValor.length-1)
			
			return sValor
		
			}
		
		</script>
		<script>
	
	var arrDistribuciones = new Array()
	var idDistrSeleccionada = null
	var dCantidad 
	var dPorcentDistribuido
	
	
	
	function Distribucion(uon1, uon2, uon3, anyo, pres1, pres2, pres3, pres4, porcent, nivel,idpres,denuon,denpres,nodeId,indexNode)
	{
	
	if (uon1!="")
		this.uon1 = uon1
	else
		this.uon1 = null
		
	if (uon2!="")
		this.uon2 = uon2
	else
		this.uon2 = null
	if (uon3!="")
		this.uon3 = uon3
	else
		this.uon3 = null
	this.anyo = anyo
	
	if (pres1!="")
		this.pres1 = pres1
	else
		this.pres1 = null
	if (pres2!="")
		this.pres2 = pres2
	else
		this.pres2 = null
	if (pres3!="")
		this.pres3 = pres3
	else
		this.pres3 = null
	if (pres4!="")
		this.pres4 = pres4
	else
		this.pres4 = null
	this.porcent = porcent
	this.nivel = nivel
	this.idpres = idpres
	this.denuon = denuon
	this.denpres = denpres
	this.nodeId = nodeId
	this.indexNode = indexNode
	}

	function desmarcarHijos(oNode)
		{
		var arrHijos = oNode.getChildNodes()
		if (arrHijos.length==0)
			return
		for (oHijo in arrHijos)
			{
			oDiv = document.getElementById(arrHijos[oHijo].Id)
			for (oElem in oDiv.all)
				if (oDiv.all[oElem].tagName =="IMG")
					{
					if (oDiv.all[oElem].src.search("CarpetaGrisAsig")>0)
						{
						oDiv.all[oElem].src = "images/carpetaGrisPres3.gif"
						break
						}
						
					}
			desmarcarHijos(arrHijos[oHijo])
			}
		}

	
	function marcarNodeUON(oNode)
	{
	oDiv = document.getElementById(oNode.Id)
	for (oElem in oDiv.all)
		if (oDiv.all[oElem].tagName =="IMG")
			{
			if (oDiv.all[oElem].src.search("carpetaGrisPres3.gif")>0  )
				{
				oDiv.all[oElem].src = "images/CarpetaGrisAsig.gif"
				break
				}
			if (oDiv.all[oElem].src.search("Mundo")>0  )
				{
				oDiv.all[oElem].src = "images/MundoAsignado.gif"
				break
				}
			}
	}

	function marcarUON(suon1, suon2, suon3)
	{
	
	var oTree=igtree_getTreeById("uwtPresupuestosctl0ctl0uwtreeUons")
		
	var arrHijos = oTree.getNodes()
	var oUON1
	var oUON2
	var oUON3
	var i
	var oRet
		
	arrHijos = arrHijos[0].getChildNodes()
	
	for (i in arrHijos)
		{
		if (arrHijos[i].getDataKey()==suon1)
			{
			oUON1 = arrHijos[i]
			break;
			}
		}
	if (oUON1)
		{
		arrHijos = arrHijos[i].getChildNodes()
		
		for (i in arrHijos)
			{
			
			if (arrHijos[i].getDataKey()==suon2)
				{
				oUON2 = arrHijos[i]
				break;
				}
			}
		if (oUON2)
			{
			arrHijos = arrHijos[i].getChildNodes()
			
			for (i in arrHijos)
				{
				if (arrHijos[i].getDataKey()==suon3)
					{
					oUON3 = arrHijos[i]
					break;
					}
				}
			if (oUON3)
				{
				oRet = oUON3
				marcarNodeUON(oUON3)
				}
			else
				{
				oRet = oUON2
				marcarNodeUON(oUON2)
				}
			}
		else
			{
			oRet = oUON1
			marcarNodeUON(oUON1)
			}
		}
	else
		{
		oRet = oTree.getNodes()[0]
		marcarNodeUON(oTree.getNodes()[0])
		}
	return oRet
	}
	
	function visualizarDistribucion()
		{
			var oTabs = igtab_getTabById("uwtPresupuestos")
			var oTree
			oDoc = oTabs.Tabs["pres"].getTargetUrlDocument()
			var p = oDoc.parentWindow
			var f = oDoc.forms.item("frmPresupuestos")

			oTree=igtree_getTreeById("uwtPresupuestosctl0ctl0uwtreeUons")
			if (oTree)
			{
			oNodes  = oTree.getNodes()
			if (oNodes.length>0)
				desmarcarHijos(oNodes[0])
			}
			
			
			oTree = p.igtree_getTreeById("uwtPresupuestos")
//			if (oTree)
//			{
//			oNodes  = oTree.getNodes()
//			if (oNodes.length>0)
//				p.desmarcarHijos(oNodes[0])
//			}
			var dPorcentUON = 0
			dPorcentDistribuido=0
			for (var i in arrDistribuciones)
				{
				oDistr= arrDistribuciones[i]
				
				marcarUON(oDistr.uon1, oDistr.uon2, oDistr.uon3)
				
				dPorcentDistribuido += oDistr.porcent
				sUon1 = f.item("UON1").value
				if (sUon1=="")
					sUon1=null
				sUon2 = f.item("UON2").value
				if (sUon2=="")
					sUon2=null
				sUon3 = f.item("UON3").value
				if (sUon3=="")
					sUon3=null

				if (sUon1== oDistr.uon1 && sUon2== oDistr.uon2 && sUon3== oDistr.uon3 && p.igedit_getById("ugtxtAnyo").getValue() == oDistr.anyo)
					{

					dPorcentUON += oDistr.porcent
					
					oTree = p.igtree_getTreeById("uwtPresupuestos")
					oNodes  = oTree.getNodes()

					oRaiz = oNodes[0]
					var oPres1 = null
					var oPres2 = null
					var oPres3 = null
					var oPres4 = null
					arrHijos = oRaiz.getChildNodes()
					for (i in arrHijos)
						{
						if (arrHijos[i].getDataKey().split("__")[2]==oDistr.pres1)
							{
							oPres1 = arrHijos[i]
							break;
							}
						}
					
					if (oPres1)
						{
						arrHijos2 = oPres1.getChildNodes()
						for (i in arrHijos2)
							{
							if (arrHijos2[i].getDataKey().split("__")[2]==oDistr.pres2)
								{
								oPres2 = arrHijos2[i]
								break;
								}
							}
						if (oPres2)
							{
							arrHijos3 = oPres2.getChildNodes()
							for (i in arrHijos3)
								{
								if (arrHijos3[i].getDataKey().split("__")[2]==oDistr.pres3)
									{
									oPres3 = arrHijos3[i]
									break;
									}
								}				
							if (oPres3)
								{		
								arrHijos4 = oPres3.getChildNodes()
								for (i in arrHijos4)
									{
									if (arrHijos4[i].getDataKey().split("__")[2]==oDistr.pres4)
										{
										oPres4 = arrHijos4[i]
										break;
										}
									}	
								if (oPres4)
									{
									p.marcarNodo(oTree.Id, oPres4.Id, oDistr.porcent,oDistr.indexNode)
									}
								else
									{
									p.marcarNodo(oTree.Id, oPres3.Id, oDistr.porcent,oDistr.indexNode)
									}
								}
							else
								{
								p.marcarNodo(oTree.Id, oPres2.Id, oDistr.porcent,oDistr.indexNode)
								}							
							}
						else
							{
							p.marcarNodo(oTree.Id, oPres1.Id, oDistr.porcent,oDistr.indexNode)
							}							
						}
					}
				}
		var oAsigPorcent =igedit_getById("txtAsignadoPorcent")
		oAsigPorcent.setValue (dPorcentDistribuido * 100)
		
		var oPendPorcent = igedit_getById("txtPendientePorcent")
		oPendPorcent.setValue((1-dPorcentDistribuido) * 100)
		
		var oTotal = igedit_getById("txtTotal")
		if (oTotal)
			{
			dTotal = oTotal.getValue()
			oAsig = igedit_getById("txtAsignado")
			oAsig.setValue(dTotal * dPorcentDistribuido)
			oPend = igedit_getById("txtPendiente")
			oPend.setValue(dTotal * (1-dPorcentDistribuido))
			
			}
		}
	
		</script>
		<SCRIPT type="text/javascript"><!--
var sDenUon
function seleccionarNodo(idNode)
	{
	
	var oNode = igtree_getNodeById("uwtPresupuestosctl0ctl0uwtreeUons" + idNode)
	oNode.setSelected(true)
	
	var oTabs = igtab_getTabById("uwtPresupuestos")
	var oTab = oTabs.Tabs["pres"]
		var oTree=igtree_getTreeById("uwtPresupuestosctl0ctl0uwtreeUons")
		
		if (!oNode)
			{
			return false
			}
			
		
		var sDen = oNode.getText()
		oDoc = oTab.getTargetUrlDocument()
		
		
		
		if (oNode.getParent())
			sDenUon = oNode.getDataKey() + " - " + sDen
		else
			sDenUon = sDen

	}
function init()
{

}
		function uwtPresupuestos_AfterSelectedTabChange(oWebTab, oTab, oEvent){
		if (oTab.getIndex()==0)
			return
			
		var oTree=igtree_getTreeById("uwtPresupuestosctl0ctl0uwtreeUons")
		var oNode = oTree.getSelectedNode()
		
		if (!oNode)
			{
			oEvent.cancel=true
			return false
			}
			
		var sDen = oNode.getText()
		oDoc = oTab.getTargetUrlDocument()
		p = oDoc.parentWindow
		f = oDoc.forms.item("frmPresupuestos")
		var iNivel = 0

		var arrUONS = new Array()
		while (oNode.getParent())
			{
			iNivel++
			arrUONS[arrUONS.length]=oNode.getDataKey()
			oNode = oNode.getParent()				
			}
		
		iNivel = arrUONS.length -1
		
		var sUon1 =null
		var sUon2 =null
		var sUon3 =null
		
		if (arrUONS.length>0)
			{
			sUon1 = arrUONS[iNivel]
			iNivel--
			if (arrUONS[iNivel])
				sUon2 = arrUONS[iNivel]
			iNivel--
			if (arrUONS[iNivel])
				sUon3 = arrUONS[iNivel]
			f.item("DENUON").value = arrUONS[0] + " - " + sDen
			}
		else
			f.item("DENUON").value = sDen

		if (f.item("UON1").value!=""  || f.item("UON2").value!=""  || f.item("UON3").value!="" )
			if (f.item("UON1").value==(sUon1==null?"":sUon1) && f.item("UON2").value==(sUon2==null?"":sUon2) && f.item("UON3").value==(sUon3==null?"":sUon3) )
				return
				
	
		f.item("PRES").value = montarValorPres()
		f.item("UON1").value = (sUon1==null?"":sUon1)
		f.item("UON2").value = (sUon2==null?"":sUon2)
		f.item("UON3").value = (sUon3==null?"":sUon3)
		f.item("TIPO").value = document.getElementById("TIPO").value
		oTotal = igedit_getById("txtTotal")
		if (oTotal)
			f.item("CANTIDAD").value = oTotal.getValue()
		
		oAnyo = p.igedit_getById("ugtxtAnyo")
		
		var oDistr
		var iAnyo = 0
		for (var i in arrDistribuciones)
			{
			oDistr= arrDistribuciones[i]
			if (oDistr.uon1 == sUon1 && oDistr.uon2 == sUon2 && oDistr.uon3 == sUon3)
				if (iAnyo<oDistr.anyo)
					iAnyo = oDistr.anyo
				

			}		

		oAnyo.setValue(iAnyo)

		f.submit()	
		}

--></SCRIPT>
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE  STYLE="MARGIN-BOTTOM: 7px" cellSpacing="1" cellPadding="1" width="100%" border="0">
				<tr>
					<td width="100%"><asp:label id="lblTitulo" runat="server" CssClass="titulo">Label</asp:label></td>
				</tr>
			</TABLE>
			
			<table id="tblPresup"  runat="server" cellSpacing="1"  STYLE="MARGIN-BOTTOM: 7px" cellPadding="1" width="100%" border="0">
				<TR>
					<TD width="10%"><asp:label id="lblTotal" runat="server" CssClass="captionBlue">Abierto:</asp:label></TD>
					<TD width="20%"><igtxt:webnumericedit id="txtTotal" runat="server" ReadOnly="True"></igtxt:webnumericedit></TD>
					<TD width="12%"><asp:label id="lblAsignado" runat="server" CssClass="captionBlue">Asignado:</asp:label></TD>
					<TD width="10%"><igtxt:webnumericedit id="txtAsignado" runat="server" ReadOnly="True"></igtxt:webnumericedit></TD>
					<TD width="5%"><igtxt:webnumericedit id="txtAsignadoPorcent" runat="server" Width="40px" ReadOnly="True"></igtxt:webnumericedit></TD>
					<TD width="10%"><asp:label id="lblpercent1" runat="server" CssClass="captionBlue">%</asp:label></TD>
					<TD width="13%"><asp:label id="lblPendiente" runat="server" CssClass="captionBlue">Pendiente:</asp:label></TD>
					<TD width="10%"><igtxt:webnumericedit id="txtPendiente" runat="server" ReadOnly="True"></igtxt:webnumericedit></TD>
					<TD width="5%"><igtxt:webnumericedit id="txtPendientePorcent" runat="server" Width="40px" ReadOnly="True"></igtxt:webnumericedit></TD>
					<TD width="5%"><asp:label id="lblpercent2" runat="server" CssClass="captionBlue">%</asp:label></TD>
				</TR>
			</table>
			<table cellSpacing="1" cellPadding="1" width="100%" border="0">
				<tr>
					<TD width="100%"><igtab:ultrawebtab id="uwtPresupuestos" runat="server" Width="100%" Height="425px">
							<ClientSideEvents BeforeSelectedTabChange="uwtPresupuestos_AfterSelectedTabChange"></ClientSideEvents>
							<DefaultTabStyle Height="20px" CssClass="uwtDefaultTab">
								<Padding Left="20px" Right="20px"></Padding>
							</DefaultTabStyle>
							<Tabs>
								<igtab:Tab Key="pres" Text="Seleccione el presupuesto" Visible="False">
									<ContentPane TargetUrl="pres.aspx" BorderStyle="None"></ContentPane>
								</igtab:Tab>
							</Tabs>
						</igtab:ultrawebtab></TD>
				</tr>
			</table>
			<TABLE width="100%">
				<tr>
					<td align="center"><INPUT class="boton" id="cmdAceptar" onclick="Aceptar()" type="button" value="Aceptar"
							name="cmdAceptar" runat="server">
					</td>
					<td align="center"><INPUT class="boton" id="cmdCancelar" onclick="window.close()" type="button" value="Cancelar"
							name="cmdCancelar" runat="server">
					</td>
				</tr>
			</TABLE>
			<INPUT id="TIPO" type="hidden" RUNAT="SERVER"> <INPUT id="IDCONTROL" type="hidden" name="IDCONTROL" runat="server">
		</form>
	</body>
</html>
