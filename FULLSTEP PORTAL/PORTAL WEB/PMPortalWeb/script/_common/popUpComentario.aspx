<%@ Page Language="vb" AutoEventWireup="false" Codebehind="popUpComentario.aspx.vb" Inherits="popUpComentario" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
    <title></title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">
	<input type="hidden" id="TextoApertura" NAME="TextoApertura" runat="server"> <input type="hidden" id="TextoCierre" NAME="TextoCierre" runat="server">
	<asp:PlaceHolder ID="phApertura" Runat="server">
	<TABLE WIDTH=100% HEIGHT = 200px BORDER = 0>
	<tr><td><asp:Label id="lblComentarioApertura" runat="server" class="captionBlue"></asp:Label></td></tr>
	<tr><td width=100% height=90%><TEXTAREA name=txtComentarioApertura READONLY id=txtComentarioApertura style='width:100%;height:200px;' type=text><%=Server.HTMLEncode(TextoApertura.value)%></TEXTAREA></td></tr>
	</table>
	</asp:PlaceHolder>
	<asp:PlaceHolder ID="phCierre" Runat="server">
	<TABLE WIDTH=100% HEIGHT = 200px BORDER = 0>
	<tr><td><asp:Label id="lblComentarioCierre" runat="server" class="captionBlue"></asp:Label></td></tr>
	<tr><td width=100% height=90%><TEXTAREA name=txtComentarioCierre READONLY id=txtComentarioCierre style='width:100%;height:200px;' type=text><%=Server.HTMLEncode(TextoCierre.value)%></TEXTAREA></td></tr>
	</table>
	</asp:PlaceHolder>
	<center><input type=button  id="btnCerrar" value="<%=Request("boton")%>" onclick="window.close()" class="boton"></center>
  </body>
</html>
