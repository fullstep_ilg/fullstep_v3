Imports System.IO
Imports System.Threading
Imports Fullstep.PMPortalServer

Namespace Fullstep.PMPortalWeb
    Partial Class guardarinstancia
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        End Sub
        Protected WithEvents Button2 As System.Web.UI.HtmlControls.HtmlInputButton
        Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
        Private designerPlaceholderDeclaration As System.Object
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            InitializeComponent()
        End Sub
#End Region
        Dim ListaDesgloseDefecto As String
        Dim ListaAdjuntosDefecto() As String
        Private ds As DataSet
		Private dsMapper As DataSet
		Private dsRoles As New DataSet
        Private lSolicitud As Long
        Private lCertificado As Long
        Private lVersionCertificado As Long
        Private lNoConformidad As Long
        Private lInstancia As Long
        Private lBloque As Long
        Private sAccion As String
        Private iAccion As Integer
        Private ArrayGruposPadre() As Long
        Private ArrayGrupos() As Long
        Private oInstancia As PMPortalServer.Instancia
        Private oSolicitud As PMPortalServer.Solicitud
        Private blnAlta As Boolean
        Private sRolPorWebService As String
        Private nombreXML As String
        ''' <summary>
        ''' Realiza la acci�n
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Page.IsPostBack Then Exit Sub

            If Not Request("GEN_Solicitud") Is Nothing Then lSolicitud = strToLong(Request("GEN_Solicitud").ToString)
            oSolicitud = FSPMServer.Get_Solicitud()
            If Not lSolicitud = 0 Then
                oSolicitud.ID = lSolicitud
                oSolicitud.Load(IdCiaComp, Idioma)
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered("rutaFacturas") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaFacturas", "<script>var rutaFacturas=" & IIf(oSolicitud.TipoSolicit = PMPortalServer.TiposDeDatos.TipoDeSolicitud.Factura, True.ToString.ToLower(), False.ToString.ToLower()) & ";</script>")

            hSolicitud.Value = lSolicitud
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutanormal") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutanormal", "var rutanormal ='" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "';", True)
            End If

            ReDim ListaAdjuntosDefecto(0)

            sAccion = Request("GEN_Accion")

            If sAccion = "enviarcertificado" Or sAccion = "guardarcertificado" Or sAccion = "guardarnoconformidad" Or sAccion = "enviarnoconformidad" Then
                GuardarSinWorkflow()
            Else
                Dim blnOk As Boolean
                blnOk = GuardarConWorkflow()
                If Not sAccion = "guardarsolicitud" Then
                    If blnOk Then Page.ClientScript.RegisterStartupScript(Me.GetType(), "MostrarFSWSServer", "<script>MostrarFSWSServer();</script>")
                Else
                    GuardarConWorkflow_ProcesarXML()
                End If

                If (Not Contrato.Value = "") AndAlso (sAccion = "guardarsolicitud") Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "CerrarEspera", "<script>if (window.parent.fraPMPortalMain.wbProgreso != null) window.parent.fraPMPortalMain.finalizar();</script>")
                Else
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "CerrarEspera", "<script>if (window.parent.fraPMPortalMain.wProgreso != null) window.parent.fraPMPortalMain.wProgreso.close();</script>")
                End If
            End If

            'Establecer el tipo de visor del que procede para seleccionar la pesta�a de men� (oInstancia se carga en GuardarSinWorkflow y GuardarConWorkflow)            
            If Not Page.ClientScript.IsClientScriptBlockRegistered("tipoVisor") Then
                Dim iTipoSolicit As Integer = 0
                If Not IsNothing(oInstancia) AndAlso Not IsNothing(oInstancia.Solicitud) Then
                    iTipoSolicit = DevolverTipoVisor(oInstancia.Solicitud.TipoSolicit)
                End If

                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "tipoVisor", "<script>var tipoVisor=" & iTipoSolicit & ";</script>")
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)
            End If
        End Sub
        Private Function DevolverTipoVisor(ByVal TipoSolicitud As TiposDeDatos.TipoDeSolicitud) As TipoVisor
            Dim iTipoVisor As TipoVisor
            Select Case TipoSolicitud
                Case TiposDeDatos.TipoDeSolicitud.Certificado
                    iTipoVisor = TipoVisor.Certificados
                Case TiposDeDatos.TipoDeSolicitud.Factura
                    iTipoVisor = TipoVisor.Facturas
                Case TiposDeDatos.TipoDeSolicitud.Contrato
                    iTipoVisor = TipoVisor.Contratos
                Case TiposDeDatos.TipoDeSolicitud.Encuesta
                    iTipoVisor = TipoVisor.Encuestas
                Case TiposDeDatos.TipoDeSolicitud.NoConformidad
                    iTipoVisor = TipoVisor.NoConformidades
                Case TiposDeDatos.TipoDeSolicitud.SolicitudQA
                    iTipoVisor = TipoVisor.SolicitudesQA
                Case Else
                    iTipoVisor = TipoVisor.Solicitudes
            End Select
            Return iTipoVisor
        End Function

        ''' <summary>
        ''' Crear un dataset con N tablas temporales (N pq QA difiere de PM) q contenga todos los campos a grabar
        ''' </summary>
        ''' <param name="Request">todos los campos a grabar en forma de request</param>
        ''' <param name="oDSPares">dataset traducci�n copia_campo Id viejo/Id nuevo</param>
        ''' <param name="sAccion">string con lo q se esta haciendo. guardarnoconformidad, enviarcertificado, etc</param>     
        ''' <param name="lSolicitud">Solicitud</param>
        ''' <param name="lInstancia">Instancia</param>
        ''' <returns>dataset explicado en Descripci�n  </returns>
        ''' <remarks>Llamada desde:esta pantalla GuardarSinWorkflow GuardarConWorkflow;</remarks>
        Private Function GenerarDataset(ByVal Request As System.Web.HttpRequest, ByVal oDSPares As DataSet, ByVal oDSListaEstados As DataSet, ByVal sAccion As String, ByVal lSolicitud As Long, ByVal lInstancia As Long) As DataSet
            Dim oItem As System.Collections.Specialized.NameValueCollection
            Dim loop1 As Integer
            Dim arr1() As String
            Dim DS As DataSet
            Dim dt As DataTable
            Dim oDTDesglose As DataTable
            Dim keys(0) As DataColumn
            Dim find(0) As Object
            Dim findDesgloseAdjun(1) As Object
            Dim keysDesglose(2) As DataColumn
            Dim keysAdjun(0) As DataColumn
            Dim keysDesgloseAdjun(1) As DataColumn
            Dim findDesglose(2) As Object
            Dim keysAcc(1) As DataColumn
            Dim findAcc(1) As Object
            Dim keysVinc(1) As DataColumn
            Dim findVinc(1) As Object
            Dim bEsSolicitud As Boolean = (lInstancia = 0)
            Dim ControlPrimerGuardadoCert As Boolean = True
            Dim bEsVersion0 As Boolean = False

            If lCertificado <> Nothing AndAlso lVersionCertificado <= 1 Then
                ControlPrimerGuardadoCert = False
                'Puede ocurrir q instancia.ult_version=1 y certificado.num_version=0
                'Hay dos casos:
                '   Solicitar, Guardas, cierras, entras y Guardar/Enviar    -> los id de campo estan bien, NO bEsVersion0
                '   Solicitar, Guardas y Guardar/Enviar                     -> los id son los de solicitud, definitivamente S� bEsVersion0
                bEsVersion0 = True
                'Como saber q no has cerrado, usando el primer id q use DevolverGrupo para ver si es la instancia o no
            End If

            DS = New DataSet

            dt = DS.Tables.Add("TEMP")

            dt.Columns.Add("CC_ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("CCD_ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("CAMPO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("VALOR_TEXT", System.Type.GetType("System.String"))
            dt.Columns.Add("VALOR_NUM", System.Type.GetType("System.Double"))
            dt.Columns.Add("VALOR_FEC", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("VALOR_BOOL", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ES_SUBCAMPO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("SUBTIPO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("TIPOGS", System.Type.GetType("System.Int32"))
            dt.Columns.Add("CAMBIO", System.Type.GetType("System.Double"))
            dt.Columns.Add("SAVE_VALUES", System.Type.GetType("System.Int32"))
            dt.Columns.Add("IDATRIB", System.Type.GetType("System.Int32"))
            dt.Columns.Add("VALERP", System.Type.GetType("System.Int32"))
            dt.Columns.Add("GRUPO", System.Type.GetType("System.Int32"))
            If Not bEsSolicitud Then dt.Columns.Add("CAMPO_DEF", System.Type.GetType("System.Int32"))

            keys(0) = dt.Columns("CAMPO")
            dt.PrimaryKey = keys

            Dim dtAdjun As DataTable = DS.Tables.Add("TEMPADJUN")

            dtAdjun.Columns.Add("CC_ID", System.Type.GetType("System.Int32"))
            dtAdjun.Columns.Add("CAMPO", System.Type.GetType("System.Int32"))
            dtAdjun.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dtAdjun.Columns.Add("TIPO", System.Type.GetType("System.Int32"))
            dtAdjun.Columns.Add("COMENT", System.Type.GetType("System.String"))
            If Not bEsSolicitud Then dtAdjun.Columns.Add("CAMPO_DEF", System.Type.GetType("System.Int32"))

            keysAdjun(0) = dtAdjun.Columns("ID")
            dtAdjun.PrimaryKey = keysAdjun

            oDTDesglose = DS.Tables.Add("TEMPDESGLOSE")

            oDTDesglose.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            oDTDesglose.Columns.Add("CC_ID_PADRE", System.Type.GetType("System.Int32"))
            oDTDesglose.Columns.Add("CC_ID_HIJO", System.Type.GetType("System.Int32"))
            oDTDesglose.Columns.Add("CAMPO_PADRE", System.Type.GetType("System.Int32"))
            oDTDesglose.Columns.Add("CAMPO_HIJO", System.Type.GetType("System.Int32"))
            oDTDesglose.Columns.Add("VALOR_TEXT", System.Type.GetType("System.String"))
            oDTDesglose.Columns.Add("VALOR_NUM", System.Type.GetType("System.Double"))
            oDTDesglose.Columns.Add("VALOR_FEC", System.Type.GetType("System.DateTime"))
            oDTDesglose.Columns.Add("VALOR_BOOL", System.Type.GetType("System.Int32"))
            oDTDesglose.Columns.Add("CAMBIO", System.Type.GetType("System.Double"))
            oDTDesglose.Columns.Add("LINEA_OLD", System.Type.GetType("System.Int32"))
            oDTDesglose.Columns.Add("SAVE_VALUES", System.Type.GetType("System.Int32"))
            oDTDesglose.Columns.Add("TIPOGS", System.Type.GetType("System.Int32"))
            oDTDesglose.Columns.Add("IDATRIB", System.Type.GetType("System.Int32"))
            oDTDesglose.Columns.Add("VALERP", System.Type.GetType("System.Int32"))
            oDTDesglose.Columns.Add("TABLA_EXTERNA", System.Type.GetType("System.Int32"))
            oDTDesglose.Columns.Add("CALCULADO", System.Type.GetType("System.Boolean"))
            oDTDesglose.Columns.Add("CALCULADO_TIT", System.Type.GetType("System.String"))
            If Not bEsSolicitud Then
                oDTDesglose.Columns.Add("CAMPO_PADRE_DEF", System.Type.GetType("System.Int32"))
                oDTDesglose.Columns.Add("CAMPO_HIJO_DEF", System.Type.GetType("System.Int32"))
            End If
            oDTDesglose.Columns("LINEA_OLD").AllowDBNull = True

            keysDesglose(0) = oDTDesglose.Columns("LINEA")
            keysDesglose(1) = oDTDesglose.Columns("CAMPO_PADRE")
            keysDesglose(2) = oDTDesglose.Columns("CAMPO_HIJO")
            oDTDesglose.PrimaryKey = keysDesglose

            Dim dtDesgloseAdjun As DataTable = DS.Tables.Add("TEMPDESGLOSEADJUN")

            dtDesgloseAdjun.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            dtDesgloseAdjun.Columns.Add("CC_ID_PADRE", System.Type.GetType("System.Int32"))
            dtDesgloseAdjun.Columns.Add("CC_ID_HIJO", System.Type.GetType("System.Int32"))
            dtDesgloseAdjun.Columns.Add("CAMPO_PADRE", System.Type.GetType("System.Int32"))
            dtDesgloseAdjun.Columns.Add("CAMPO_HIJO", System.Type.GetType("System.Int32"))
            dtDesgloseAdjun.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dtDesgloseAdjun.Columns.Add("TIPO", System.Type.GetType("System.Int32"))
            If Not bEsSolicitud Then
                dtDesgloseAdjun.Columns.Add("CAMPO_PADRE_DEF", System.Type.GetType("System.Int32"))
                dtDesgloseAdjun.Columns.Add("CAMPO_HIJO_DEF", System.Type.GetType("System.Int32"))
            End If

            keysDesgloseAdjun(0) = dtDesgloseAdjun.Columns("ID")
            keysDesgloseAdjun(1) = dtDesgloseAdjun.Columns("LINEA")
            dtDesgloseAdjun.PrimaryKey = keysDesgloseAdjun

            Dim oDTCertif As DataTable
            oDTCertif = DS.Tables.Add("TEMPCERTIFICADO")
            oDTCertif.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            oDTCertif.Columns.Add("PROVE", System.Type.GetType("System.String"))
            oDTCertif.Columns.Add("CONTACTO", System.Type.GetType("System.Int32"))
            oDTCertif.Columns.Add("FEC_DESPUB", System.Type.GetType("System.DateTime"))
            oDTCertif.Columns.Add("PETICIONARIO", System.Type.GetType("System.String"))
            oDTCertif.Columns("CONTACTO").AllowDBNull = True

            Dim oDTNoConf As DataTable
            oDTNoConf = DS.Tables.Add("TEMPNOCONFORMIDAD")
            oDTNoConf.Columns.Add("ID", System.Type.GetType("System.Int32"))
            oDTNoConf.Columns.Add("PROVE", System.Type.GetType("System.String"))
            oDTNoConf.Columns.Add("CONTACTO", System.Type.GetType("System.Int32"))
            oDTNoConf.Columns.Add("FEC_LIM_RESOL", System.Type.GetType("System.DateTime"))
            oDTNoConf.Columns.Add("PETICIONARIO", System.Type.GetType("System.String"))
            oDTNoConf.Columns.Add("UNIDADQA", System.Type.GetType("System.Int32"))
            oDTNoConf.Columns("CONTACTO").AllowDBNull = True

            Dim oDTNoConfAccion As DataTable
            oDTNoConfAccion = DS.Tables.Add("TEMPNOCONF_SOLICIT_ACC")
            oDTNoConfAccion.Columns.Add("CC_ID", System.Type.GetType("System.Int32"))
            oDTNoConfAccion.Columns.Add("COPIA_CAMPO", System.Type.GetType("System.Int32"))
            oDTNoConfAccion.Columns.Add("SOLICITAR", System.Type.GetType("System.Int32"))
            oDTNoConfAccion.Columns.Add("FECHA_LIMITE", System.Type.GetType("System.DateTime"))
            If Not bEsSolicitud Then oDTNoConfAccion.Columns.Add("CAMPO_DEF", System.Type.GetType("System.Int32"))
            keys(0) = oDTNoConfAccion.Columns("COPIA_CAMPO")
            oDTNoConfAccion.PrimaryKey = keys

            Dim oDTNoConfEstadosAccion As DataTable
            oDTNoConfEstadosAccion = DS.Tables.Add("TEMPNOCONF_ACC")
            oDTNoConfEstadosAccion.Columns.Add("CC_ID", System.Type.GetType("System.Int32"))
            oDTNoConfEstadosAccion.Columns.Add("CAMPO_PADRE", System.Type.GetType("System.Int32"))
            oDTNoConfEstadosAccion.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            oDTNoConfEstadosAccion.Columns.Add("ESTADO", System.Type.GetType("System.String"))
            oDTNoConfEstadosAccion.Columns.Add("ESTADO_INT", System.Type.GetType("System.Int32"))
            oDTNoConfEstadosAccion.Columns.Add("COMENT", System.Type.GetType("System.String"))
            oDTNoConfEstadosAccion.Columns.Add("LINEA_OLD", System.Type.GetType("System.Int32"))
            oDTNoConfEstadosAccion.Columns("LINEA_OLD").AllowDBNull = True
            If Not bEsSolicitud Then oDTNoConfEstadosAccion.Columns.Add("CAMPO_DEF", System.Type.GetType("System.Int32"))
            keysAcc(0) = oDTNoConfEstadosAccion.Columns("CAMPO_PADRE")
            keysAcc(1) = oDTNoConfEstadosAccion.Columns("LINEA")
            oDTNoConfEstadosAccion.PrimaryKey = keysAcc

            Dim oDTPMParticipantes As DataTable
            oDTPMParticipantes = DS.Tables.Add("TEMP_PARTICIPANTES")
            oDTPMParticipantes.Columns.Add("ROL", System.Type.GetType("System.Int32"))
            oDTPMParticipantes.Columns.Add("TIPO", System.Type.GetType("System.Int16"))
            oDTPMParticipantes.Columns.Add("PER", System.Type.GetType("System.String"))
            oDTPMParticipantes.Columns.Add("PROVE", System.Type.GetType("System.String"))
            oDTPMParticipantes.Columns.Add("CON", System.Type.GetType("System.Int32"))

            keys(0) = oDTPMParticipantes.Columns("ROL")
            oDTPMParticipantes.PrimaryKey = keys

            'DEMO Mahou / 2016 / 32: Se incluye unicamente para q Instancia.CompletarFormulario lo rellene. Desde portal este campo NO es normal q se vea.
            Dim oDTVincular As DataTable
            oDTVincular = DS.Tables.Add("VINCULAR")
            oDTVincular.Columns.Add("CC_ID", System.Type.GetType("System.Int32"))
            oDTVincular.Columns.Add("DESGLOSEVINCULADA", System.Type.GetType("System.Int32"))
            oDTVincular.Columns.Add("LINEAVINCULADA", System.Type.GetType("System.Int16"))
            oDTVincular.Columns.Add("INSTANCIAORIGEN", System.Type.GetType("System.Int32"))
            oDTVincular.Columns.Add("DESGLOSEORIGEN", System.Type.GetType("System.Int32"))
            oDTVincular.Columns.Add("LINEAORIGEN", System.Type.GetType("System.Int16"))
            keysVinc(0) = oDTVincular.Columns("DESGLOSEVINCULADA")
            keysVinc(1) = oDTVincular.Columns("LINEAVINCULADA")
            oDTVincular.PrimaryKey = keysVinc

            Dim oDTMoverVincular As DataTable
            oDTMoverVincular = DS.Tables.Add("MOVERVINCULAR")
            oDTMoverVincular.Columns.Add("DESGLOSE", System.Type.GetType("System.Int32"))
            oDTMoverVincular.Columns.Add("LINEA_ILV", System.Type.GetType("System.Int16"))
            oDTMoverVincular.Columns.Add("INSTANCIADESTINO", System.Type.GetType("System.Int32"))
            'Fin: DEMO Mahou / 2016 / 32

            Dim sRequest As String
            Dim oRequest As Object
            Dim dtNewRow As DataRow
            Dim dtNewRowAcc As DataRow
            Dim iTipo As TiposDeDatos.TipoGeneral

            ReDim ArrayGruposPadre(0)
            ReDim ArrayGrupos(0)

            oItem = Request.Form
            arr1 = oItem.AllKeys

            For loop1 = 0 To arr1.GetUpperBound(0)
                sRequest = arr1(loop1).ToString

                oRequest = sRequest.Split("_")

                If oRequest(0) = "CAMPO" Or oRequest(0) = "CAMPOID" Or oRequest(0) = "DESG" Or oRequest(0) = "DESGID" Or oRequest(0) = "PART" _
                OrElse oRequest(0) = "CAMPOCCD" OrElse oRequest(0) = "ADJUNCCD" OrElse oRequest(0) = "DESGCCDHP" OrElse oRequest(0) = "DESGCCDPP" _
                OrElse oRequest(0) = "DESGCCDPNP" OrElse oRequest(0) = "DESGCCDHNP" OrElse oRequest(0) = "ADJUNDESGCCDHP" _
                OrElse oRequest(0) = "ADJUNDESGCCDPP" OrElse oRequest(0) = "ADJUNDESGCCDHNP" OrElse oRequest(0) = "ADJUNDESGCCDPNP" _
                OrElse oRequest(0) = "ADJUNCOMENT" Then
                    iTipo = oRequest(UBound(oRequest))
                End If

                Select Case oRequest(0)
                    Case "PROVE"
                        dtNewRow = oDTCertif.NewRow
                        dtNewRow.Item("LINEA") = oRequest(1)
                        dtNewRow.Item("PROVE") = Request(sRequest)
                        If Request("GEN_DespubAnyo") = "null" Then
                            dtNewRow.Item("FEC_DESPUB") = DBNull.Value
                        Else
                            Dim D As Date
                            D = New Date(Request("GEN_DespubAnyo"), Request("GEN_DespubMes"), Request("GEN_DespubDia"))
                            dtNewRow.Item("FEC_DESPUB") = D
                        End If
                        oDTCertif.Rows.Add(dtNewRow)
                    Case "PART"
                        dtNewRow = oDTPMParticipantes.NewRow
                        dtNewRow.Item("ROL") = CInt(oRequest(2))
                        Dim iTipoGS As TiposDeDatos.TipoCampoGS = CInt(oRequest(1))
                        dtNewRow.Item("TIPO") = iTipoGS
                        If iTipoGS = TiposDeDatos.TipoCampoGS.Persona Then
                            dtNewRow.Item("PER") = strToDBNull(Request(sRequest))
                            dtNewRow.Item("PROVE") = DBNull.Value
                            dtNewRow.Item("CON") = DBNull.Value
                        Else
                            Dim sAux As String = Request(sRequest)
                            Dim sProve As String
                            Dim lCon As Integer
                            If InStr(sAux, "###") > 0 Then
                                sProve = Left(sAux, InStr(sAux, "###") - 1)
                                lCon = CInt(Right(sAux, Len(sAux) - Len(sProve) - 3))
                            Else
                                sProve = ""
                                lCon = 0
                            End If
                            dtNewRow.Item("PER") = DBNull.Value
                            dtNewRow.Item("PROVE") = strToDBNull(sProve)
                            dtNewRow.Item("CON") = lCon
                        End If
                        oDTPMParticipantes.Rows.Add(dtNewRow)
                    Case "GEN"
                    Case "DMONREPER"
                        find(0) = oRequest(1)
                        dtNewRow = dt.Rows.Find(find)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dt.NewRow
                            dtNewRow.Item("CAMPO") = oRequest(1)
                            dtNewRow.Item("ES_SUBCAMPO") = 0
                            dt.Rows.Add(dtNewRow)
                        End If

                        find(0) = oRequest(2)
                        dtNewRow = dt.Rows.Find(find)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dt.NewRow
                            dtNewRow.Item("CAMPO") = oRequest(2)
                            dtNewRow.Item("ES_SUBCAMPO") = 1
                            dt.Rows.Add(dtNewRow)
                        End If

                        findDesglose(0) = oRequest(3)
                        findDesglose(1) = oRequest(1)
                        findDesglose(2) = oRequest(2)
                        dtNewRow = oDTDesglose.Rows.Find(findDesglose)
                        If dtNewRow Is Nothing Then
                            dtNewRow = oDTDesglose.NewRow
                        End If

                        dtNewRow.Item("LINEA") = oRequest(3)
                        dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                        dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

                        dtNewRow.Item("VALOR_TEXT") = strToDBNull(Request(sRequest))
                    Case "DEQREPER"
                        find(0) = oRequest(1)
                        dtNewRow = dt.Rows.Find(find)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dt.NewRow
                            dtNewRow.Item("CAMPO") = oRequest(1)
                            dtNewRow.Item("ES_SUBCAMPO") = 0
                            dt.Rows.Add(dtNewRow)
                        End If

                        find(0) = oRequest(2)
                        dtNewRow = dt.Rows.Find(find)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dt.NewRow
                            dtNewRow.Item("CAMPO") = oRequest(2)
                            dtNewRow.Item("ES_SUBCAMPO") = 1
                            dt.Rows.Add(dtNewRow)
                        End If

                        findDesglose(0) = oRequest(3)
                        findDesglose(1) = oRequest(1)
                        findDesglose(2) = oRequest(2)
                        dtNewRow = oDTDesglose.Rows.Find(findDesglose)
                        If dtNewRow Is Nothing Then
                            dtNewRow = oDTDesglose.NewRow
                        End If

                        dtNewRow.Item("LINEA") = oRequest(3)
                        dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                        dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

                        dtNewRow.Item("CAMBIO") = Numero(Request(sRequest), ".", ",")
                    Case "MONREPER"
                        find(0) = oRequest(1)
                        dtNewRow = dt.Rows.Find(find)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dt.NewRow
                        End If
                        dtNewRow.Item("CAMPO") = oRequest(1)
                        dtNewRow.Item("VALOR_TEXT") = strToDBNull(Request(sRequest))
                    Case "EQREPER"
                        find(0) = oRequest(1)
                        dtNewRow = dt.Rows.Find(find)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dt.NewRow
                        End If
                        dtNewRow.Item("CAMPO") = oRequest(1)
                        dtNewRow.Item("CAMBIO") = Numero(Request(sRequest), ".", ",")
                    Case "CAMPO"
                        'CAMPO_17046_7
                        find(0) = oRequest(1)
                        dtNewRow = dt.Rows.Find(find)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dt.NewRow
                        End If

                        dtNewRow.Item("CAMPO") = oRequest(1)
                        If Request(sRequest) <> Nothing Then
                            Select Case iTipo
                                Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo
                                    dtNewRow.Item("VALOR_TEXT") = strToDBNull(Request(sRequest))
                                Case TiposDeDatos.TipoGeneral.TipoNumerico
                                    If Not IsNumeric(Right(Request(sRequest), 1)) Then
                                        '1,234.00 EUR   1,234.50 EUR
                                        '1.234,00 EUR   1.234,50 EUR
                                        Dim sAux As String = Mid(Request(sRequest), 1, Len(Request(sRequest)) - (FSPMServer.LongitudesDeCodigos.giLongCodMON + 1))
                                        If FSPMUser.DecimalFmt = "." Then
                                            '1,234.00    1,234.50
                                            sAux = Replace(sAux, ",", "")
                                            '1234.00    1234.50
                                        Else '","
                                            '1.234,00    1.234,50
                                            sAux = Replace(Replace(sAux, ".", ""), ",", ".")
                                            '1234.00    1234.50
                                        End If
                                        dtNewRow.Item("VALOR_NUM") = Numero(sAux, ".", ",")
                                    Else
                                        dtNewRow.Item("VALOR_NUM") = Numero(Request(sRequest), ".", ",")
                                    End If
                                Case TiposDeDatos.TipoGeneral.TipoBoolean
                                    If Request(sRequest) = "" Then
                                        dtNewRow.Item("VALOR_BOOL") = System.DBNull.Value
                                    Else
                                        dtNewRow.Item("VALOR_BOOL") = Numero(Request(sRequest))
                                    End If
                                Case TiposDeDatos.TipoGeneral.TipoFecha
                                    If igDateToDate(Request(sRequest)) <> Nothing Then
                                        dtNewRow.Item("VALOR_FEC") = igDateToDate(Request(sRequest))
                                    End If
                                Case TiposDeDatos.TipoGeneral.TipoEditor
                                    dtNewRow.Item("VALOR_TEXT") = strToDBNull(Request(sRequest))
                            End Select
                        End If
                        dtNewRow.Item("SUBTIPO") = iTipo
                        dtNewRow.Item("ES_SUBCAMPO") = 0
                        If dtNewRow.RowState = DataRowState.Detached Then
                            dt.Rows.Add(dtNewRow)
                        End If
                    Case "CAMPOID"
                        'PARA LOS CAMPOS DE TIPO TEXTO Y INTRODUCCI�N LISTA HAY QUE METER EL INDICE DEL ELEMENTO SELECCIONADO EN VALOR_NUM
                        If iTipo = TiposDeDatos.TipoGeneral.TipoString Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoCorto Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoLargo Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio Then
                            find(0) = oRequest(1)
                            dtNewRow = dt.Rows.Find(find)
                            If dtNewRow Is Nothing Then
                                dtNewRow = dt.NewRow
                            End If

                            dtNewRow.Item("CAMPO") = oRequest(1)

                            Select Case iTipo
                                Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo
                                    If Request(sRequest) = "" Then
                                        dtNewRow.Item("VALOR_NUM") = System.DBNull.Value
                                    Else
                                        dtNewRow.Item("VALOR_NUM") = Numero(Request(sRequest), ".", ",")
                                    End If
                            End Select
                            dtNewRow.Item("ES_SUBCAMPO") = 0

                        End If
                        If dtNewRow.RowState = DataRowState.Detached Then
                            dt.Rows.Add(dtNewRow)
                        End If
                    Case "CAMPOAT"
                        find(0) = oRequest(1)
                        dtNewRow = dt.Rows.Find(find)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dt.NewRow
                        End If
                        dtNewRow.Item("CAMPO") = oRequest(1)
                        dtNewRow.Item("IDATRIB") = Request(sRequest)

                        If dtNewRow.RowState = DataRowState.Detached Then
                            dt.Rows.Add(dtNewRow)
                        End If
                    Case "CAMPOERP"
                        'Validaci�n ERP (llamada externa a la BAPI de SAP para comprobar si el valor existe):
                        find(0) = oRequest(1)
                        dtNewRow = dt.Rows.Find(find)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dt.NewRow
                        End If
                        dtNewRow.Item("CAMPO") = oRequest(1)
                        dtNewRow.Item("VALERP") = Request(sRequest)

                        If dtNewRow.RowState = DataRowState.Detached Then
                            dt.Rows.Add(dtNewRow)
                        End If
                    Case "CAMPOTIPOGS"
                        find(0) = oRequest(1)
                        dtNewRow = dt.Rows.Find(find)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dt.NewRow
                        End If

                        dtNewRow.Item("CAMPO") = oRequest(1)
                        dtNewRow.Item("TIPOGS") = Numero(Request(sRequest))

                        If dtNewRow.RowState = DataRowState.Detached Then
                            dt.Rows.Add(dtNewRow)
                        End If
                    Case "CAMPOGRUPO"
                        find(0) = oRequest(1)
                        dtNewRow = dt.Rows.Find(find)
                        If dtNewRow Is Nothing Then
                            dtNewRow = dt.NewRow
                        End If
                        dtNewRow.Item("CAMPO") = oRequest(1)
                        dtNewRow.Item("GRUPO") = Request(sRequest)

                        If dtNewRow.RowState = DataRowState.Detached Then
                            dt.Rows.Add(dtNewRow)
                        End If
                    Case "CAMPOCCD"
                        If Not bEsSolicitud Then
                            find(0) = oRequest(1)
                            dtNewRow = dt.Rows.Find(find)
                            If dtNewRow Is Nothing Then
                                dtNewRow = dt.NewRow
                            End If

                            dtNewRow.Item("CAMPO") = oRequest(1)
                            dtNewRow.Item("CAMPO_DEF") = Numero(Request(sRequest))

                            If dtNewRow.RowState = DataRowState.Detached Then
                                dt.Rows.Add(dtNewRow)
                            End If
                        End If
                    Case "DESG"
                        'DESG_17048_1000_3_6
                        'Me he encontrado con un 
                        '   - sRequest	"DESG_1312436_ctl0x_20253_20253_1312436_fsdsentry_4_1312443_3_6"
                        '     este campo de desglose es una unidad de medida invisible asociada a un articulo. 
                        '     El oRequest(2) que deber�a contener el campo hijo es ctl0x!!!. Me temo que peta.
                        '   - Si el desglose no es editable no pasa nada pq 
                        '     sRequest  "DESG_1312541_1312545_2_6"
                        If Not IsNumeric(oRequest(2)) Then
                            oRequest(2) = oRequest(8)
                        End If
                        'fin DESG_1312436_ctl0x_20253_20253_1312436_fsdsentry_4_1312443_3_6

                        Dim sGrupo As String = ""

                        If (Request("GEN_Instancia") <> Nothing) Then
                            If Not ControlPrimerGuardadoCert Then
                                ControlPrimerGuardadoCert = True

                                Dim oCertificado As PMPortalServer.Certificado
                                oCertificado = FSPMServer.Get_Certificado
                                oCertificado.Instancia = lInstancia

                                Dim lCiaComp As Long = IdCiaComp

                                bEsVersion0 = oCertificado.ControlPrimerGuardadoCert(lCiaComp, oRequest(1), lInstancia)
                            ElseIf blnAlta Then
                                bEsVersion0 = True
                            End If
                            sGrupo = DevolverGrupo(oRequest(1), True, bEsVersion0)
                        End If

                        Dim sAux As String
                        sAux = Replace(oRequest(2), sGrupo, "")
                        If (sAccion = "guardarnoconformidad" OrElse sAccion = "enviarnoconformidad") _
                        AndAlso (sAux = CInt(PMPortalServer.IdsFicticios.EstadoActual).ToString Or sAux = CInt(PMPortalServer.IdsFicticios.EstadoInterno).ToString Or sAux = CInt(PMPortalServer.IdsFicticios.Comentario).ToString Or sAux = CInt(PMPortalServer.IdsFicticios.FechaLimite).ToString) Then
                            If sAux <> CInt(PMPortalServer.IdsFicticios.FechaLimite).ToString Then
                                oRequest(2) = sAux
                                findAcc(0) = oRequest(1)
                                findAcc(1) = oRequest(3)
                                dtNewRow = oDTNoConfEstadosAccion.Rows.Find(findAcc)
                                If dtNewRow Is Nothing Then
                                    dtNewRow = oDTNoConfEstadosAccion.NewRow
                                    dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                                    dtNewRow.Item("LINEA") = oRequest(3)
                                End If
                                Select Case oRequest(2)
                                    Case PMPortalServer.IdsFicticios.EstadoActual
                                        dtNewRow.Item("CAMPO_PADRE") = oRequest(4)
                                        dtNewRow.Item("LINEA") = oRequest(3)
                                        dtNewRow.Item("ESTADO") = Request(sRequest) 'Directamente estamos recuperando el c�digo por que de primeras se mete el c�digo, es solo cuando seleccionaas de la lista cuando se le mete el �ndice.                                
                                    Case PMPortalServer.IdsFicticios.EstadoInterno
                                        If Numero(Request(sRequest), ".", ",") <> 0 Then
                                            dtNewRow.Item("ESTADO_INT") = Numero(Request(sRequest), ".", ",")
                                        Else
                                            dtNewRow.Item("ESTADO_INT") = 1
                                        End If
                                    Case PMPortalServer.IdsFicticios.Comentario
                                        dtNewRow.Item("COMENT") = Request(sRequest)
                                End Select
                                If dtNewRow.RowState = DataRowState.Detached Then
                                    oDTNoConfEstadosAccion.Rows.Add(dtNewRow)
                                End If
                            End If
                        Else
                            If (sAccion = "guardarnoconformidad" OrElse sAccion = "enviarnoconformidad") _
                            AndAlso (sAux = CInt(PMPortalServer.IdsFicticios.EstadoActual).ToString) Then
                                oRequest(2) = sAux
                                findAcc(0) = oRequest(1)
                                findAcc(1) = oRequest(3)
                                dtNewRow = oDTNoConfEstadosAccion.Rows.Find(findAcc)
                                If dtNewRow Is Nothing Then
                                    dtNewRow = oDTNoConfEstadosAccion.NewRow
                                    dtNewRow.Item("CAMPO_PADRE") = oRequest(4)
                                    dtNewRow.Item("LINEA") = oRequest(3)
                                End If
                                'nzg Select Case oRequest(2)
                                '    Case Fullstep.PMPortalServer.IdsFicticios.EstadoActual
                                dtNewRow.Item("ESTADO") = Request(sRequest) 'Directamente estamos recuperando el c�digo por que de primeras se mete el c�digo, es solo cuando seleccionaas de la lista cuando se le mete el �ndice.
                                dtNewRow.Item("ESTADO_INT") = 1 'SIN REVISAR                            
                                'End Select
                                If dtNewRow.RowState = DataRowState.Detached Then
                                    oDTNoConfEstadosAccion.Rows.Add(dtNewRow)
                                End If
                            Else
                                find(0) = oRequest(1)
                                dtNewRow = dt.Rows.Find(find)
                                If dtNewRow Is Nothing Then
                                    dtNewRow = dt.NewRow
                                    dtNewRow.Item("CAMPO") = oRequest(1)
                                    dtNewRow.Item("ES_SUBCAMPO") = 0
                                    dtNewRow.Item("SUBTIPO") = TiposDeDatos.TipoGeneral.TipoDesglose
                                    dt.Rows.Add(dtNewRow)
                                End If

                                find(0) = oRequest(2)
                                dtNewRow = dt.Rows.Find(find)
                                If dtNewRow Is Nothing Then
                                    dtNewRow = dt.NewRow
                                    dtNewRow.Item("CAMPO") = oRequest(2)
                                    dtNewRow.Item("ES_SUBCAMPO") = 1
                                    dt.Rows.Add(dtNewRow)
                                End If

                                findDesglose(0) = oRequest(3)
                                findDesglose(1) = oRequest(1)
                                findDesglose(2) = oRequest(2)
                                dtNewRow = oDTDesglose.Rows.Find(findDesglose)
                                If dtNewRow Is Nothing Then
                                    dtNewRow = oDTDesglose.NewRow
                                End If

                                dtNewRow.Item("LINEA") = oRequest(3)
                                dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                                dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
                                If Request(sRequest) <> Nothing Then
                                    Select Case iTipo
                                        Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo
                                            If Request(sRequest) = "nulo" Then
                                                dtNewRow.Item("VALOR_TEXT") = System.DBNull.Value
                                            Else
                                                dtNewRow.Item("VALOR_TEXT") = Request(sRequest)
                                            End If
                                        Case TiposDeDatos.TipoGeneral.TipoNumerico
                                            If Not IsNumeric(Right(Request(sRequest), 1)) Then
                                                '1,234.00 EUR   1,234.50 EUR
                                                '1.234,00 EUR   1.234,50 EUR
                                                sAux = Mid(Request(sRequest), 1, Len(Request(sRequest)) - (FSPMServer.LongitudesDeCodigos.giLongCodMON + 1))
                                                If FSPMUser.DecimalFmt = "." Then
                                                    '1,234.00    1,234.50
                                                    sAux = Replace(sAux, ",", "")
                                                    '1234.00    1234.50
                                                Else '","
                                                    '1.234,00    1.234,50
                                                    sAux = Replace(Replace(sAux, ".", ""), ",", ".")
                                                    '1234.00    1234.50
                                                End If
                                                dtNewRow.Item("VALOR_NUM") = Numero(sAux, ".", ",")
                                            Else
                                                dtNewRow.Item("VALOR_NUM") = Numero(Request(sRequest), ".", ",")
                                            End If
                                        Case TiposDeDatos.TipoGeneral.TipoBoolean
                                            If Request(sRequest) = "" Then
                                                dtNewRow.Item("VALOR_BOOL") = System.DBNull.Value
                                            Else
                                                dtNewRow.Item("VALOR_BOOL") = Numero(Request(sRequest))
                                            End If
                                        Case TiposDeDatos.TipoGeneral.TipoFecha
                                            If igDateToDate(Request(sRequest)) <> Nothing Then
                                                dtNewRow.Item("VALOR_FEC") = igDateToDate(Request(sRequest))
                                            End If
                                        Case TiposDeDatos.TipoGeneral.TipoEditor
                                            dtNewRow.Item("VALOR_TEXT") = strToDBNull(Request(sRequest))
                                    End Select

                                End If
                                If dtNewRow.RowState = DataRowState.Detached Then
                                    oDTDesglose.Rows.Add(dtNewRow)
                                End If
                            End If
                        End If
                    Case "DESGID"
                        'DESGID_17048_17053_2_6:
                        If iTipo = TiposDeDatos.TipoGeneral.TipoString Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoCorto Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoLargo Or iTipo = TiposDeDatos.TipoGeneral.TipoTextoMedio Then
                            findDesglose(0) = oRequest(3)
                            findDesglose(1) = oRequest(1)
                            findDesglose(2) = oRequest(2)
                            dtNewRow = oDTDesglose.Rows.Find(findDesglose)
                            If dtNewRow Is Nothing Then
                                dtNewRow = oDTDesglose.NewRow
                            End If

                            dtNewRow.Item("LINEA") = oRequest(3)
                            dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                            dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
                            Select Case iTipo
                                Case TiposDeDatos.TipoGeneral.TipoString, TiposDeDatos.TipoGeneral.TipoTextoCorto, TiposDeDatos.TipoGeneral.TipoTextoLargo, TiposDeDatos.TipoGeneral.TipoTextoMedio, TiposDeDatos.TipoGeneral.TipoArchivo
                                    dtNewRow.Item("VALOR_NUM") = IIf(Request(sRequest) = "", System.DBNull.Value, Numero(Request(sRequest), ".", ","))
                            End Select

                            If Request(sRequest) <> "" Then
								dtNewRow.Item("VALOR_TEXT") = DameDescripcion(0, dtNewRow.Item("VALOR_NUM"), dtNewRow.Item("CAMPO_HIJO"), oInstancia, If(blnAlta OrElse bEsVersion0, False, True), Idioma)
							Else
                                If IsDBNull(dtNewRow.Item("VALOR_TEXT")) Then
                                    dtNewRow.Item("VALOR_TEXT") = ""
                                End If
                            End If

                            If dtNewRow.RowState = DataRowState.Detached Then
                                oDTDesglose.Rows.Add(dtNewRow)
                            End If
                        End If
                    Case "DESGTIPOGS"
                        Dim sAuxRequest2 As String = oRequest(2).ToString
                        sAuxRequest2 = Replace(oRequest(2), DevolverGrupo(oRequest(1), False), "")

                        If (sAccion = "guardarnoconformidad" OrElse sAccion = "enviarnoconformidad") And (sAuxRequest2 = CInt(PMPortalServer.IdsFicticios.EstadoActual).ToString _
                        Or sAuxRequest2 = CInt(PMPortalServer.IdsFicticios.EstadoInterno).ToString _
                        Or sAuxRequest2 = CInt(PMPortalServer.IdsFicticios.Comentario).ToString) Then
                            oRequest(2) = sAuxRequest2
                        Else
                            'emitirnoconformidad   
                            If sAuxRequest2 = CInt(PMPortalServer.IdsFicticios.EstadoActual).ToString Then
                                oRequest(2) = sAuxRequest2
                            End If
                        End If

                        findDesglose(0) = oRequest(3)
                        findDesglose(1) = oRequest(1)
                        findDesglose(2) = oRequest(2)
                        dtNewRow = oDTDesglose.Rows.Find(findDesglose)
                        If dtNewRow Is Nothing Then
                            dtNewRow = oDTDesglose.NewRow
                        End If
                        dtNewRow.Item("LINEA") = oRequest(3)
                        dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                        dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

                        dtNewRow.Item("TIPOGS") = Numero(Request(sRequest))

                        If dtNewRow.RowState = DataRowState.Detached Then
                            oDTDesglose.Rows.Add(dtNewRow)
                        End If
                    Case "DESGAT"
                        findDesglose(0) = oRequest(3)
                        findDesglose(1) = oRequest(1)
                        findDesglose(2) = oRequest(2)
                        dtNewRow = oDTDesglose.Rows.Find(findDesglose)
                        If dtNewRow Is Nothing Then
                            dtNewRow = oDTDesglose.NewRow
                        End If
                        dtNewRow.Item("LINEA") = oRequest(3)
                        dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                        dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

                        dtNewRow.Item("IDATRIB") = Request(sRequest)

                        If dtNewRow.RowState = DataRowState.Detached Then
                            oDTDesglose.Rows.Add(dtNewRow)
                        End If
                    Case "DESGERP"
                        findDesglose(0) = oRequest(3)
                        findDesglose(1) = oRequest(1)
                        findDesglose(2) = oRequest(2)
                        dtNewRow = oDTDesglose.Rows.Find(findDesglose)
                        If dtNewRow Is Nothing Then
                            dtNewRow = oDTDesglose.NewRow
                        End If
                        dtNewRow.Item("LINEA") = oRequest(3)
                        dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                        dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

                        dtNewRow.Item("VALERP") = Request(sRequest)

                        If dtNewRow.RowState = DataRowState.Detached Then
                            oDTDesglose.Rows.Add(dtNewRow)
                        End If
                    Case "DESGTABLAEXT"
                        If Not Request(sRequest) = "" Then
                            findDesglose(0) = oRequest(3)
                            findDesglose(1) = oRequest(1)
                            findDesglose(2) = oRequest(2)
                            dtNewRow = oDTDesglose.Rows.Find(findDesglose)
                            If dtNewRow Is Nothing Then
                                dtNewRow = oDTDesglose.NewRow
                            End If
                            dtNewRow.Item("LINEA") = oRequest(3)
                            dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                            dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
                            dtNewRow.Item("TABLA_EXTERNA") = Request(sRequest)
                        End If

                        If dtNewRow.RowState = DataRowState.Detached Then
                            oDTDesglose.Rows.Add(dtNewRow)
                        End If
                    Case "DESGCALC"
                        If Not Request(sRequest) = "" Then
                            findDesglose(0) = oRequest(3)
                            findDesglose(1) = oRequest(1)
                            findDesglose(2) = oRequest(2)
                            dtNewRow = oDTDesglose.Rows.Find(findDesglose)
                            If dtNewRow Is Nothing Then
                                dtNewRow = oDTDesglose.NewRow
                            End If
                            dtNewRow.Item("LINEA") = oRequest(3)
                            dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                            dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
                            dtNewRow.Item("CALCULADO") = True
                        End If

                        If dtNewRow.RowState = DataRowState.Detached Then
                            oDTDesglose.Rows.Add(dtNewRow)
                        End If
                    Case "DESGCALCTIT"
                        If Not Request(sRequest) = "" Then
                            findDesglose(0) = oRequest(3)
                            findDesglose(1) = oRequest(1)
                            findDesglose(2) = oRequest(2)
                            dtNewRow = oDTDesglose.Rows.Find(findDesglose)
                            If dtNewRow Is Nothing Then
                                dtNewRow = oDTDesglose.NewRow
                            End If
                            dtNewRow.Item("LINEA") = oRequest(3)
                            dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                            dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
                            dtNewRow.Item("CALCULADO_TIT") = Request(sRequest)
                        End If
                        If dtNewRow.RowState = DataRowState.Detached Then
                            oDTDesglose.Rows.Add(dtNewRow)
                        End If
                    Case "DESGCCDPP", "DESGCCDPNP"
                        If Not bEsSolicitud Then
                            Dim sAuxRequest2 As String = oRequest(2).ToString
                            sAuxRequest2 = Replace(oRequest(2), DevolverGrupo(oRequest(1), False), "")

                            If (sAccion = "guardarnoconformidad" OrElse sAccion = "enviarnoconformidad") _
                            And (sAuxRequest2 = CInt(PMPortalServer.IdsFicticios.EstadoActual).ToString _
                            Or sAuxRequest2 = CInt(PMPortalServer.IdsFicticios.EstadoInterno).ToString _
                            Or sAuxRequest2 = CInt(PMPortalServer.IdsFicticios.Comentario).ToString) Then
                                oRequest(2) = sAuxRequest2
                            End If
                            findDesglose(0) = oRequest(3)
                            findDesglose(1) = oRequest(1)
                            findDesglose(2) = oRequest(2)
                            dtNewRow = oDTDesglose.Rows.Find(findDesglose)
                            If dtNewRow Is Nothing Then
                                dtNewRow = oDTDesglose.NewRow
                            End If
                            dtNewRow.Item("LINEA") = oRequest(3)
                            dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                            dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

                            dtNewRow.Item("CAMPO_PADRE_DEF") = Numero(Request(sRequest))

                            If dtNewRow.RowState = DataRowState.Detached Then
                                oDTDesglose.Rows.Add(dtNewRow)
                            End If
                        End If
                    Case "DESGCCDHP", "DESGCCDHNP"
                        If Not bEsSolicitud Then
                            Dim sAuxRequest2 As String = oRequest(2).ToString
                            sAuxRequest2 = Replace(oRequest(2), DevolverGrupo(oRequest(1), False), "")

                            If (sAccion = "guardarnoconformidad" OrElse sAccion = "enviarnoconformidad") _
                            And (sAuxRequest2 = CInt(PMPortalServer.IdsFicticios.EstadoActual).ToString _
                            Or sAuxRequest2 = CInt(PMPortalServer.IdsFicticios.EstadoInterno).ToString _
                            Or sAuxRequest2 = CInt(PMPortalServer.IdsFicticios.Comentario).ToString) Then
                                oRequest(2) = sAuxRequest2
                            End If
                            findDesglose(0) = oRequest(3)
                            findDesglose(1) = oRequest(1)
                            findDesglose(2) = oRequest(2)
                            dtNewRow = oDTDesglose.Rows.Find(findDesglose)
                            If dtNewRow Is Nothing Then
                                dtNewRow = oDTDesglose.NewRow
                            End If
                            dtNewRow.Item("LINEA") = oRequest(3)
                            dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                            dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

                            dtNewRow.Item("CAMPO_HIJO_DEF") = Numero(Request(sRequest))

                            If dtNewRow.RowState = DataRowState.Detached Then
                                oDTDesglose.Rows.Add(dtNewRow)
                            End If
                        End If

                    Case "ADJUN"
                        'ADJUN_17063_2
                        If Request(sRequest) <> Nothing Then
                            find(0) = Numero(Request(sRequest))
                            dtNewRow = dtAdjun.Rows.Find(find)
                            If dtNewRow Is Nothing Then
                                dtNewRow = dtAdjun.NewRow
                            End If

                            dtNewRow.Item("CAMPO") = oRequest(1)
                            dtNewRow.Item("TIPO") = oRequest(2)
                            dtNewRow.Item("ID") = Request(sRequest)
                            dtAdjun.Rows.Add(dtNewRow)

                            If dtNewRow.RowState = DataRowState.Detached Then
                                dtAdjun.Rows.Add(dtNewRow)
                            End If
                        End If
                    Case "ADJUNCCD"
                        If Not bEsSolicitud Then
                            find(0) = oRequest(3)
                            dtNewRow = dtAdjun.Rows.Find(find)
                            If dtNewRow Is Nothing Then
                                dtNewRow = dtAdjun.NewRow
                            End If

                            dtNewRow.Item("CAMPO") = oRequest(1)
                            dtNewRow.Item("TIPO") = oRequest(2)
                            dtNewRow.Item("ID") = oRequest(3)
                            dtNewRow.Item("CAMPO_DEF") = Numero(Request(sRequest))

                            If dtNewRow.RowState = DataRowState.Detached Then
                                dtAdjun.Rows.Add(dtNewRow)
                            End If
                        End If
                    Case "ADJUNCOMENT"
                        If Request(sRequest) <> Nothing Then
                            find(0) = oRequest(3)
                            dtNewRow = dtAdjun.Rows.Find(find)
                            If dtNewRow Is Nothing Then
                                dtNewRow = dtAdjun.NewRow
                                dtNewRow.Item("ID") = oRequest(3)
                            End If
                            dtNewRow.Item("COMENT") = Request(sRequest)

                            If dtNewRow.RowState = DataRowState.Detached Then
                                dtAdjun.Rows.Add(dtNewRow)
                            End If
                        End If
                    Case "ADJUNDESG"
                        'ADJUNDESG_17064_17079_1_2
                        If Request(sRequest) <> Nothing Then

                            findDesgloseAdjun(0) = Numero(Request(sRequest))
                            findDesgloseAdjun(1) = oRequest(3)
                            dtNewRow = dtDesgloseAdjun.Rows.Find(findDesgloseAdjun)
                            If dtNewRow Is Nothing Then
                                dtNewRow = dtDesgloseAdjun.NewRow
                            End If

                            dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                            dtNewRow.Item("CAMPO_HIJO") = oRequest(2)

                            dtNewRow.Item("LINEA") = oRequest(3)

                            Dim Lista As String = CreaListaAdjuntosDefecto(lSolicitud, oRequest(1))
                            If Strings.InStr(Lista, "##" & CStr(Request(sRequest)) & "##", CompareMethod.Text) <> 0 Then
                                'Los nuevos son tipo 1. �ya se ha creado en bbdd el registro tabla ADJUN?
                                'Y si me creo tipo 3 desde aqui y 
                                'que el stored cree en ADJUN y update 163## por 12345##
                                dtNewRow.Item("TIPO") = 3
                            Else
                                dtNewRow.Item("TIPO") = oRequest(4)
                            End If

                            dtNewRow.Item("ID") = Request(sRequest)

                            If dtNewRow.RowState = DataRowState.Detached Then
                                dtDesgloseAdjun.Rows.Add(dtNewRow)
                            End If
                        End If
                    Case "ADJUNDESGCCDPP", "ADJUNDESGCCDPNP"
                        If Not bEsSolicitud Then
                            findDesgloseAdjun(0) = oRequest(5)
                            findDesgloseAdjun(1) = oRequest(3)
                            dtNewRow = dtDesgloseAdjun.Rows.Find(findDesgloseAdjun)
                            If dtNewRow Is Nothing Then
                                dtNewRow = dtDesgloseAdjun.NewRow
                            End If
                            dtNewRow.Item("LINEA") = oRequest(3)
                            dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                            dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
                            'TIPO lo hace Case "ADJUNDESG"
                            dtNewRow.Item("ID") = oRequest(5)

                            dtNewRow.Item("CAMPO_PADRE_DEF") = Numero(Request(sRequest))

                            If dtNewRow.RowState = DataRowState.Detached Then
                                dtDesgloseAdjun.Rows.Add(dtNewRow)
                            End If
                        End If
                    Case "ADJUNDESGCCDHP", "ADJUNDESGCCDHNP"
                        If Not bEsSolicitud Then
                            findDesgloseAdjun(0) = oRequest(5)
                            findDesgloseAdjun(1) = oRequest(3)
                            dtNewRow = dtDesgloseAdjun.Rows.Find(findDesgloseAdjun)
                            If dtNewRow Is Nothing Then
                                dtNewRow = dtDesgloseAdjun.NewRow
                            End If
                            dtNewRow.Item("LINEA") = oRequest(3)
                            dtNewRow.Item("CAMPO_PADRE") = oRequest(1)
                            dtNewRow.Item("CAMPO_HIJO") = oRequest(2)
                            'TIPO lo hace Case "ADJUNDESG"
                            dtNewRow.Item("ID") = oRequest(5)

                            dtNewRow.Item("CAMPO_HIJO_DEF") = Numero(Request(sRequest))

                            If dtNewRow.RowState = DataRowState.Detached Then
                                dtDesgloseAdjun.Rows.Add(dtNewRow)
                            End If
                        End If
                    Case "ACCION"
                        find(0) = oRequest(2)
                        dtNewRow = oDTNoConfAccion.Rows.Find(find)
                        If dtNewRow Is Nothing Then
                            dtNewRow = oDTNoConfAccion.NewRow
                        End If

                        dtNewRow.Item("COPIA_CAMPO") = oRequest(2)
                        'If Request(sRequest) <> Nothing Then
                        If oRequest(1) = "NoConfFECLIM" Then
                            If igDateToDate(Request(sRequest)) <> Nothing Then
                                dtNewRow.Item("FECHA_LIMITE") = igDateToDate(Request(sRequest))
                            End If
                        Else
                            If Request(sRequest) = True Then
                                dtNewRow.Item("SOLICITAR") = 1
                            Else
                                dtNewRow.Item("SOLICITAR") = 0
                            End If
                        End If
                        If Not bEsSolicitud Then dtNewRow.Item("CAMPO_DEF") = oRequest(4)
                        If dtNewRow.RowState = DataRowState.Detached Then
                            oDTNoConfAccion.Rows.Add(dtNewRow)
                        End If
                End Select
            Next loop1

            'volvemos a recorrernos el request para actualizar las l�neas de desglose con la correspondiente l�nea en bd
            For loop1 = 0 To arr1.GetUpperBound(0)
                sRequest = arr1(loop1).ToString

                oRequest = sRequest.Split("_")

                Select Case oRequest(0)
                    Case "CON"
                        Dim dRowCertif() As DataRow
                        dRowCertif = oDTCertif.Select("LINEA= " + oRequest(1).ToString)
                        dtNewRow = dRowCertif(0)
                        If Request(sRequest) = "" Then
                            dtNewRow.Item("CONTACTO") = System.DBNull.Value
                        Else
                            dtNewRow.Item("CONTACTO") = Request(sRequest)
                        End If
                    Case "LINDESG"
                        'LINDESG_17048_2: 2
                        If Request(sRequest) <> Nothing Then
                            Dim oLineas As DataRow()

                            oLineas = oDTDesglose.Select("LINEA =" & oRequest(2).ToString() & " AND CAMPO_PADRE = " & oRequest(1).ToString())
                            For Each dtNewRow In oLineas
                                dtNewRow.Item("LINEA_OLD") = Request(sRequest)
                            Next
                            oLineas = oDTNoConfEstadosAccion.Select("LINEA =" & oRequest(2).ToString() & " AND CAMPO_PADRE = " & oRequest(1).ToString())
                            For Each dtNewRow In oLineas
                                dtNewRow.Item("LINEA_OLD") = Request(sRequest)
                            Next
                        End If
                    Case "DESGCCDHP", "DESGCCDHNP"
                        If Not bEsSolicitud Then
                            Dim sAuxRequest2 As String = oRequest(2).ToString
                            sAuxRequest2 = Replace(oRequest(2), DevolverGrupo(oRequest(1), False), "")

                            If (sAccion = "guardarnoconformidad" OrElse sAccion = "enviarnoconformidad") _
                            And (sAuxRequest2 = PMPortalServer.IdsFicticios.EstadoInterno _
                            OrElse sAuxRequest2 = PMPortalServer.IdsFicticios.Comentario) Then
                                findDesglose(0) = oRequest(3)
                                findDesglose(1) = oRequest(1)
                                findDesglose(2) = sAuxRequest2
                                dtNewRow = oDTDesglose.Rows.Find(findDesglose)

                                'Solo en, los siguientes casos
                                '   TiposDeDatos.IdsFicticios.EstadoActual
                                '   TiposDeDatos.IdsFicticios.EstadoInterno
                                '   TiposDeDatos.IdsFicticios.Comentario
                                'que no importa q no hagan nada en dt pq no son datos de copia_linea_desglose sino 
                                'de noconf_solict_acc
                                Dim lPadre As Long = dtNewRow("CAMPO_PADRE_DEF")

                                Dim oLineas As DataRow()
                                oLineas = oDTNoConfEstadosAccion.Select("LINEA =" & oRequest(3).ToString() & " AND CAMPO_PADRE = " & oRequest(1).ToString())
                                For Each dtNewRowAcc In oLineas
                                    dtNewRowAcc.Item("CAMPO_DEF") = lPadre
                                Next

                                If sAuxRequest2 = CStr(PMPortalServer.IdsFicticios.EstadoActual) Then
                                    dtNewRow.Item("CAMPO_HIJO_DEF") = CStr(PMPortalServer.IdsFicticios.EstadoActual)
                                End If

                                find(0) = oRequest(1)
                                dtNewRow = dt.Rows.Find(find)
                                dtNewRow.Item("CAMPO_DEF") = lPadre
                            ElseIf (sAccion = "guardarnoconformidad" OrElse sAccion = "enviarnoconformidad") _
                            And (sAuxRequest2 = PMPortalServer.IdsFicticios.FechaLimite) Then
                                'Solo en, los siguientes casos
                                '   TiposDeDatos.IdsFicticios.FechaLimite
                                'que no importa q no hagan nada en dt pq no son datos de copia_linea_desglose sino 
                                'de noconf_acc. No hay entrys solo label-> arrAcciones sin nada -> no hay fechas-> como antes, a partir de version anterior se graba OK.  
                            ElseIf (sAccion = "guardarnoconformidad" OrElse sAccion = "enviarnoconformidad") _
                            And (sAuxRequest2 = CStr(PMPortalServer.IdsFicticios.EstadoActual)) Then
                                findDesglose(0) = oRequest(3)
                                findDesglose(1) = oRequest(1)
                                findDesglose(2) = sAuxRequest2
                                dtNewRow = oDTDesglose.Rows.Find(findDesglose)

                                Dim lHijo As Long = dtNewRow("CAMPO_HIJO_DEF")
                                Dim lPadre As Long = dtNewRow("CAMPO_PADRE_DEF")

                                dtNewRow.Item("CAMPO_HIJO_DEF") = CStr(PMPortalServer.IdsFicticios.EstadoActual)

                                find(0) = oRequest(1)
                                dtNewRow = dt.Rows.Find(find)
                                dtNewRow.Item("CAMPO_DEF") = lPadre
                            Else
                                findDesglose(0) = oRequest(3)
                                findDesglose(1) = oRequest(1)
                                findDesglose(2) = oRequest(2)
                                dtNewRow = oDTDesglose.Rows.Find(findDesglose)

                                Dim lHijo As Long = dtNewRow("CAMPO_HIJO_DEF")
                                Dim lPadre As Long = dtNewRow("CAMPO_PADRE_DEF")

                                find(0) = oRequest(2)
                                dtNewRow = dt.Rows.Find(find)
                                dtNewRow.Item("CAMPO_DEF") = lHijo

                                find(0) = oRequest(1)
                                dtNewRow = dt.Rows.Find(find)
                                dtNewRow.Item("CAMPO_DEF") = lPadre
                            End If
                        End If
                End Select
            Next

            'REVISAR SI LO SIGUIENTE ES NECESARIO
            Dim oTable As DataTable

            oTable = DS.Tables("TEMP")
            For Each dtNewRow In oTable.Rows
                If Not bEsSolicitud AndAlso lVersionCertificado = -1234 Then dtNewRow.Item("CAMPO_DEF") = DevolverIdCampoActual(oDSPares, dtNewRow.Item("CAMPO"), False, dtNewRow.Item("CAMPO_DEF"))
                dtNewRow.Item("CAMPO") = DevolverIdCampoActual(oDSPares, dtNewRow.Item("CAMPO"))
            Next

            oTable = DS.Tables("TEMPDESGLOSE")
            For Each dtNewRow In oTable.Rows
                If Not bEsSolicitud AndAlso lVersionCertificado = -1234 Then
                    dtNewRow.Item("CAMPO_PADRE_DEF") = DevolverIdCampoActual(oDSPares, dtNewRow.Item("CAMPO"), False, dtNewRow.Item("CAMPO_PADRE_DEF"))
                    dtNewRow.Item("CAMPO_HIJO_DEF") = DevolverIdCampoActual(oDSPares, dtNewRow.Item("CAMPO"), False, dtNewRow.Item("CAMPO_HIJO_DEF"))
                End If
                dtNewRow.Item("CAMPO_PADRE") = DevolverIdCampoActual(oDSPares, dtNewRow.Item("CAMPO_PADRE"))
                dtNewRow.Item("CAMPO_HIJO") = DevolverIdCampoActual(oDSPares, dtNewRow.Item("CAMPO_HIJO"))
            Next

            oTable = DS.Tables("TEMPADJUN")
            For Each dtNewRow In oTable.Rows
                If Not bEsSolicitud AndAlso lVersionCertificado = -1234 Then dtNewRow.Item("CAMPO_DEF") = DevolverIdCampoActual(oDSPares, dtNewRow.Item("CAMPO"), False, dtNewRow.Item("CAMPO_DEF"))
                dtNewRow.Item("CAMPO") = DevolverIdCampoActual(oDSPares, dtNewRow.Item("CAMPO"))
            Next

            oTable = DS.Tables("TEMPDESGLOSEADJUN")
            For Each dtNewRow In oTable.Rows
                If Not bEsSolicitud AndAlso lVersionCertificado = -1234 Then
                    dtNewRow.Item("CAMPO_PADRE_DEF") = DevolverIdCampoActual(oDSPares, dtNewRow.Item("CAMPO"), False, dtNewRow.Item("CAMPO_PADRE_DEF"))
                    dtNewRow.Item("CAMPO_HIJO_DEF") = DevolverIdCampoActual(oDSPares, dtNewRow.Item("CAMPO"), False, dtNewRow.Item("CAMPO_HIJO_DEF"))
                End If
                dtNewRow.Item("CAMPO_PADRE") = DevolverIdCampoActual(oDSPares, dtNewRow.Item("CAMPO_PADRE"))
                dtNewRow.Item("CAMPO_HIJO") = DevolverIdCampoActual(oDSPares, dtNewRow.Item("CAMPO_HIJO"))
            Next

            Return DS
        End Function
        ''' <summary>
        ''' Como al guardar no refresca pero si graba en bbdd, el id del campo en pantalla y en bbdd no tienen
        ''' pq coincidir. 
        ''' </summary>
        ''' <param name="oDSPares">dataset traducci�n copia_campo Id viejo/Id nuevo</param>
        ''' <param name="IdCampoVersion">id del campo en bbdd</param>        
        ''' <param name="SacarCampoID">true Devolver Id Campo Actual. false Devolver Id Campo Def</param>  
        ''' <param name="IdCampoDef">id del copia_campo_def en bbdd</param> 
        ''' <returns>Correspondencia campo en pantalla</returns>
        ''' <remarks>Llamada desde: GenerarDataset; Tiempo m�ximo: 0,1</remarks>
        Private Function DevolverIdCampoActual(ByRef oDsPares As DataSet, ByVal IdCampoVersion As Long, Optional ByVal SacarCampoID As Boolean = True, Optional ByVal IdCampoDef As Long = 0) As Long
            If oDsPares Is Nothing Then
                Return IdCampoVersion
            End If
            Dim find(0) As Object

            find(0) = IdCampoVersion
            Dim oRow As DataRow

            oRow = oDsPares.Tables(0).Rows.Find(find)

            If oRow Is Nothing Then
                If SacarCampoID Then
                    Return IdCampoVersion
                Else
                    Return IdCampoDef
                End If
            End If

            If SacarCampoID Then
                Return oRow.Item("IDCAMPOACTUAL")
            Else
                Return oRow.Item("ID_CAMPO_DEF")
            End If
        End Function
        ''' <summary>
        ''' Como al guardar no refresca pero si graba en bbdd, el id del campo en pantalla y en bbdd no tienen
        ''' pq coincidir. 
        ''' </summary>
        ''' <param name="IdCampoVersion">id del campo en bbdd</param>        
        ''' <returns>Correspondencia campo en pantalla</returns>
        ''' <remarks>Llamada desde: GenerarDataset; Tiempo m�ximo: 0,1</remarks>
        Private Function DevolverIdCampoActualReverse(ByVal oDsParesReverse As DataSet, ByVal IdCampoVersion As Long) As Long
            If oDsParesReverse Is Nothing Then
                Return IdCampoVersion
            End If

            Dim find(0) As Object

            find(0) = IdCampoVersion
            Dim oRow As DataRow

            oRow = oDsParesReverse.Tables(0).Rows.Find(find)

            If oRow Is Nothing Then
                Return IdCampoVersion
            End If

            Return oRow.Item("IDCAMPOVERSION")
        End Function
        ''' <summary>
        ''' Devuelve desde bbdd o desde el array q contiene relacion campopadre-grupodesglose, el grupo en q esta un desglose 
        ''' </summary>
        ''' <param name="idCampoPadre">campo padre del desglose</param>
        ''' <param name="bDesdeBd">desde bbdd o desde el array</param>
        ''' <param name="bEsVersion0">los certificados solicitados por el service, solo son registro en Instancia y poco mas. No hay registros en copia_campo,copia_xxx,etc</param>     ' 
        ''' <returns>el grupo en q esta un desglose </returns>
        ''' <remarks>Llamada desde: GenerarDatset; Tiempo maximo: 0,1</remarks>
        Private Function DevolverGrupo(ByVal idCampoPadre As Integer, Optional ByVal bDesdeBd As Boolean = True, Optional ByVal bEsVersion0 As Boolean = False) As String
            If bDesdeBd Then
                Dim oCampo As PMPortalServer.Campo
                oCampo = FSPMServer.Get_Campo()

                If bEsVersion0 Then
                    oCampo.IdGrupo = idCampoPadre
                Else
                    Dim lCiaComp As Long = IdCiaComp
                    Dim lInstancia As Long = Request("GEN_Instancia")

                    oCampo.Id = idCampoPadre
                    oCampo.LoadInst(lCiaComp, lInstancia, Idioma)
                End If

                Dim bEsta As Boolean = False
                For i As Integer = 0 To UBound(ArrayGruposPadre)
                    If ArrayGruposPadre(i) = idCampoPadre Then
                        bEsta = True
                        Exit For
                    End If
                Next
                If Not bEsta Then
                    ReDim Preserve ArrayGruposPadre(UBound(ArrayGruposPadre) + 1)
                    ArrayGruposPadre(UBound(ArrayGruposPadre)) = idCampoPadre
                    ReDim Preserve ArrayGrupos(UBound(ArrayGrupos) + 1)
                    ArrayGrupos(UBound(ArrayGrupos)) = oCampo.IdGrupo.ToString
                End If

                Return oCampo.IdGrupo.ToString
                oCampo = Nothing
            Else
                For i As Integer = 0 To UBound(ArrayGruposPadre)
                    If ArrayGruposPadre(i) = idCampoPadre Then
                        Return ArrayGrupos(i)
                    End If
                Next
            End If
        End Function
        ''' <summary>
        ''' Funci�n graba certificados y no conformidades
        ''' </summary>
        ''' <remarks>Llamada desde:el evento Load de guardarinstancia.aspx.vb; Tiempo m�ximo: 4 sec aprox.</remarks>
        Private Sub GuardarSinWorkflow()
            Dim m_sMsgboxAccion(3) As String
            Dim sContacto As String = String.Empty
            Dim sGuardada As String = String.Empty
            Dim ListaCalc As String = String.Empty
            Dim sGuardadaEnPortal As String = String.Empty
            Dim lVersion As Long
            Dim oCertificado As PMPortalServer.Certificado
            Dim oNoConformidad As PMPortalServer.NoConformidad
            Dim oDSPares, oDSParesReverse As DataSet

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.GuardarInstancia

            m_sMsgboxAccion(1) = Textos(0) 'Imposible aprobar:
            m_sMsgboxAccion(2) = Textos(1) 'Imposible trasladar:

            If Not Request("GEN_Solicitud") Is Nothing Then lSolicitud = strToLong(Request("GEN_Solicitud").ToString)
            If Not Request("GEN_Contacto") Is Nothing Then sContacto = Request("GEN_Contacto").ToString
            lInstancia = strToLong(Request("GEN_Instancia").ToString)
            If Not Request("GEN_Certificado") Is Nothing Then lCertificado = strToLong(Request("GEN_Certificado").ToString)
            lVersion = strToLong(Request("GEN_Version").ToString)
            If Not Request("GEN_NoConformidad") Is Nothing Then lNoConformidad = strToLong(Request("GEN_NoConformidad"))
            sAccion = Request("GEN_Accion").ToString
            If Request("GEN_Notificar") IsNot Nothing Then
                Notificar.Value = (Request("GEN_Notificar").ToString = "1")
            Else
                Notificar.Value = False
            End If

            If Not lCertificado = 0 Then
                oCertificado = FSPMServer.Get_Certificado
                oCertificado.Id = lCertificado
                oCertificado.Prove = FSPMUser.CodProve
                oCertificado.Load(IdCiaComp, Idioma)
                If Not lVersion = 0 Then oCertificado.Version = lVersion

                oDSPares = oCertificado.CargarCorrespondenciaCampos(IdCiaComp, True, oDSParesReverse)
                lVersionCertificado = lVersion
            End If

            If Not lNoConformidad = 0 Then
                oNoConformidad = FSPMServer.Get_NoConformidad
                oNoConformidad.Id = lNoConformidad
                If Not lVersion = 0 Then oNoConformidad.Version = lVersion
            End If

            If Not lSolicitud = 0 Then
                oSolicitud = FSPMServer.Get_Solicitud
                oSolicitud.ID = lSolicitud
                oSolicitud.Load(IdCiaComp, Idioma)
            End If

            oInstancia = FSPMServer.Get_Instancia
            oInstancia.ID = lInstancia
            oInstancia.Actualizar_En_proceso(IdCiaComp, 1)

            oInstancia.Solicitud = oSolicitud
            oInstancia.Load(IdCiaComp, Idioma)

            If Not lCertificado = 0 Then oInstancia.Version = oCertificado.Version

            Dim oDSListaEstados As DataSet = oInstancia.CargarTiposEstadoAccion(IdCiaComp)
            ds = GenerarDataset(Request, oDSPares, oDSListaEstados, sAccion, lSolicitud, lInstancia)

            If Not lSolicitud = 0 Then
                ds = oSolicitud.CompletarFormulario(IdCiaComp, ds)
            Else
                ds = oInstancia.CompletarFormulario(IdCiaComp, ds, FSPMUser.CodProveGS, FSPMUser.AprobadorActual, False)
            End If

            CompletarDataSetCamposCalculados(ds, Idioma, ListaCalc, lVersion, oDSPares, oDSParesReverse)

            Dim sComentAltaNoConf As String
            If Request("GEN_NoConfComentAlta") <> Nothing Then sComentAltaNoConf = Request("GEN_NoConfComentAlta")

            'Aqu� haremos las validaciones 
            If sAccion = "enviarcertificado" Then
                If oCertificado.EnvioConjunto And oCertificado.FecCumplimentacion = Date.MinValue Then
                    If oCertificado.HayCertificadosPendientesCumplimentar(IdCiaComp) Then
                        sAccion = "guardarCertificadoPdteEnviar"
                    ElseIf oCertificado.HayCertificadosPendientesEnviar(IdCiaComp) Then
                        sAccion = "enviarCertificadoYtodosPdtesdeEnviar"
                    Else
                        sAccion = "enviarCertificadosPortal"
                    End If
                Else
                    sAccion = "enviarCertificadosPortal"
                End If
            End If

            If sAccion = "enviarnoconformidad" Then sAccion = "enviarNoConformidadesPortal"

            Select Case sAccion
                Case "altaCertificadoPortal", "enviarCertificadosPortal", "guardarCertificadoPdteEnviar", "enviarCertificadoYtodosPdtesdeEnviar"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EnvioCertificadoOk", "<script>window.open(""" & "../certificados/EnvioCertificadoOk.aspx?Instancia=" & oInstancia.ID.ToString &
                                                   "&Certificado=" & oCertificado.Id.ToString & "&Accion=" & sAccion & """,""fraPMPortalMain"")</script>")

                Case "enviarNoConformidadesPortal"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EnvioNoConformidadOk", "<script>window.open(""" & "../noconformidad/EnvioNoConformidadOk.aspx?Instancia=" & oInstancia.ID.ToString &
                                                   "&NoConformidad=" & oNoConformidad.Id.ToString & """,""fraPMPortalMain"")</script>")

                Case "guardarcertificado", "guardarnoconformidad"
                    ModuloIdioma = TiposDeDatos.ModulosIdiomas.Certificado

                    sGuardadaEnPortal = Textos(7)
                    sGuardada = Textos(8)
            End Select

            GuardarQA_Thread()

            If sAccion = "guardarcertificado" Or sAccion = "guardarnoconformidad" Then _
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerEstado", "<script>window.parent.fraPMPortalMain.PonerEstado('" & sGuardada & "','" & sGuardadaEnPortal & "','" & ListaCalc & "');</script>")

            oInstancia = Nothing
        End Sub
        ''' <summary>
        ''' Funci�n graba solicitudes
        ''' </summary>
        ''' <remarks>Llamada desde:el evento Load de guardarinstancia.aspx.vb; Tiempo m�ximo: 4 sec aprox.</remarks>
        Private Function GuardarConWorkflow() As Boolean
            Dim lCiaComp As Long = IdCiaComp
            Dim lVersion As Long
            Dim dTotal As Double
            Dim bEnviar As Boolean
            Dim oDSPares As DataSet
            Dim lIDRefSol As Long
            Dim bMostrarMensaje As Boolean = False
            Dim AvisoBloqueo As PMPortalServer.TipoAvisoBloqueo

            If Not Request("GEN_Solicitud") Is Nothing Then lSolicitud = strToLong(Request("GEN_Solicitud").ToString)
            lInstancia = strToLong(Request("GEN_Instancia"))
            hInstancia.Value = lInstancia
            Contrato.Value = Request("ID_CONTRATO")
            CodContrato.Value = Request("COD_CONTRATO")
            lVersion = Request("GEN_Version")
            lBloque = Request("GEN_Bloque")
            hBloque.Value = lBloque
            If Not Request("GEN_Rol") Is Nothing Then RolActual.Value = strToLong(Request("GEN_Rol").ToString)

            iAccion = Request("GEN_AccionRol")
            hAccionId.Value = iAccion
            hAccion.Value = Request("GEN_Accion")
            hGuardar.Value = Request("GEN_Guardar")

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.GuardarInstancia
            If lVersion = 0 Then blnAlta = True

            oInstancia = FSPMServer.Get_Instancia
            oInstancia.ID = lInstancia
            'Mahou San Miguel / 2017 / 93: Se estan quedando en proceso muchas solicitudes que contestan los proveedores desde portal. 
            'Puede ser que no est�n confirmando los env�os, y se quedan en proceso. Hay que dajr como etaba antes, que hasta que no confirmen no se ponga en proceso, ojo, solo en el portal. 
            'oInstancia.Actualizar_En_proceso(lCiaComp, 1, hBloque.Value)

            If Not lSolicitud = 0 Then
                oSolicitud = FSPMServer.Get_Solicitud()
                oSolicitud.ID = lSolicitud
                oSolicitud.Load(lCiaComp, Idioma)
            End If

            If Not lInstancia = 0 Then oInstancia.Load(lCiaComp, Idioma, True)

            If Request("GEN_Guardar") = 1 Or sAccion = "devolversolicitud" Or iAccion <> Nothing Then
                Dim oDSListaEstados As DataSet = oInstancia.CargarTiposEstadoAccion(lCiaComp)
                ds = GenerarDataset(Request, oDSPares, oDSListaEstados, sAccion, lSolicitud, lInstancia)
                If Not lSolicitud = 0 Then
                    ds = oSolicitud.CompletarFormulario(lCiaComp, ds, True, False, FSPMUser.CodProveGS, FSPMUser.IdContacto)
                Else
                    ds = oInstancia.CompletarFormulario(lCiaComp, ds, FSPMUser.CodProveGS, FSPMUser.AprobadorActual, True)
                End If
                dsMapper = ds
            End If

            If Request("GEN_AVISOBLOQUEO") = "" Then
                AvisoBloqueo = PMPortalServer.TipoAvisoBloqueo.SinAsignar
            Else
                AvisoBloqueo = Request("GEN_AVISOBLOQUEO")
            End If

            If Request("GEN_IDREFSOL") = "" Then
                lIDRefSol = 0
            Else
                lIDRefSol = CDbl(Request("GEN_IDREFSOL"))
            End If
            '********************************************************************************
            'RECALCULAMOS LOS CAMPOS CALCULADOS POR SI LAS FLY
            Dim oDS As DataSet
            Dim oDSDesglose As DataSet
            Dim bCalcular As Boolean
            Dim oDSRow As DataRow
            Dim oRow As DataRow
            Dim dt As DataTable
            dt = ds.Tables("TEMP")

            Dim oDSRowDesglose As DataRow
            Dim oDSRowDesgloseOculto As DataRow
            Dim sPre As String
            Dim sRequest As String
            Dim iTipoGS As TiposDeDatos.TipoCampoGS
            Dim iTipo As TiposDeDatos.TipoGeneral
            Dim iTipoGSDesglose As TiposDeDatos.TipoCampoGS
            Dim iTipoDesglose As TiposDeDatos.TipoGeneral
            Dim vValor As Object
            Dim oCampo As PMPortalServer.Campo
            Dim k As Integer
            Dim iLinea As Integer

            If blnAlta Then
                If IsNothing(oSolicitud) Then
                    oSolicitud = FSPMServer.Get_Solicitud()
                    oSolicitud.ID = Request("CALC_Solicitud")
                    oSolicitud.Load(lCiaComp, Idioma)
                End If
                oDS = oSolicitud.Formulario.LoadCamposCalculados(lCiaComp, oSolicitud.ID, Idioma, True, FSPMUser.CodProveGS, FSPMUser.IdContacto)
            Else
                If IsNothing(oInstancia) Then
                    oInstancia.ID = Request("CALC_Instancia")
                    oInstancia.Load(lCiaComp, Idioma, True)
                End If
                oInstancia.Version = Request("CALC_NumVersion")
                oDS = oInstancia.LoadCamposCalculados(lCiaComp, FSPMUser.CodProveGS)
            End If

            Dim findDesglose(2) As Object
            Dim find(0) As Object
            Dim oRowDesglose As DataRow
            Dim keysDesglose(2) As DataColumn
            Dim oDTDesglose As DataTable
            oDTDesglose = ds.Tables("TEMPDESGLOSE")

            Dim sVariables() As String
            Dim sVariablesDesglose() As String
            Dim dValues() As Double
            Dim dValuesDesglose() As Double
            Dim dValuesTotalDesglose() As Double
            Dim i As Integer
            Dim iDesglose As Integer

            ReDim sVariables(oDS.Tables(0).Rows.Count - 1)
            ReDim dValues(oDS.Tables(0).Rows.Count - 1)

            Dim Eliminados As Integer
            Eliminados = 0
            Dim iEq As New USPExpress.USPExpression

            i = 0
            Dim oRowWorkflow As DataRow

            Dim noinsertar As Integer
            noinsertar = 0

            Dim bEstaInvisibleElDesglose As Boolean
            For Each oDSRow In oDS.Tables(0).Rows
                bEstaInvisibleElDesglose = False

                sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())

                sVariables(i) = oDSRow.Item("ID_CALCULO")

                iTipoGS = DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS"))
                iTipo = oDSRow.Item("SUBTIPO")
                If blnAlta Then
                    sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID").ToString()
                Else
                    sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO").ToString()
                End If
                If oDSRow.Item("VISIBLE") = 1 Then
                    vValor = Numero(VacioToNothing(Request(sRequest)), ".", ",")
                Else
                    vValor = DBNullToSomething(oDSRow.Item("VALOR_NUM"))
                End If

                If oDSRow.Item("TIPO") = TipoCampoPredefinido.Calculado Then
                    If Not IsDBNull(oDSRow.Item("ORIGEN_CALC_DESGLOSE")) Then
                        oCampo = FSPMServer.Get_Campo
                        If blnAlta Then
                            oCampo.Id = oDSRow.Item("ORIGEN_CALC_DESGLOSE")
                            oDSDesglose = oCampo.LoadCalculados(lCiaComp, oSolicitud.ID, FSPMUser.CodProveGS, FSPMUser.IdContacto)
                        Else
                            oCampo.Id = DBNullToInt(oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE"))
                            oDSDesglose = oCampo.LoadInstCalculados(lCiaComp, lInstancia, FSPMUser.CodProveGS)
                        End If

                        ReDim sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
                        ReDim dValuesDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
                        ReDim dValuesTotalDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
                        bCalcular = False

                        Dim dTotalDesglose As Double

                        dTotalDesglose = 0

                        Dim iNumRows As Integer
                        iNumRows = 0
                        Dim lineasReal As Integer()
                        Dim bPopUp As Boolean
                        Dim bDesgloseEmergente As Boolean

                        'Compruebo si el campo desglose es visible
                        bEstaInvisibleElDesglose = True
                        If oDSDesglose.Tables(0).Rows.Count > 0 Then
                            For Each oDSRowDesglosePadreVisible In oDSDesglose.Tables(0).Rows
                                If oDSRowDesglosePadreVisible.Item("VISIBLE") = 1 Then
                                    bEstaInvisibleElDesglose = False
                                    Exit For
                                End If
                            Next
                        End If

                        If not bEstaInvisibleElDesglose Then
                            sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString())
                            iNumRows = Request("CALC_" + sPre + "_tblDesglose") 'Filas de desglose que salen en pesta�a
                            bPopUp = False
                            If iNumRows = Nothing Then
                                'El desglose es de tipo popup
                                If blnAlta Then
                                    iNumRows = Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "_tblDesglose")
                                    bDesgloseEmergente = False
                                    If iNumRows = Nothing Then
                                        bDesgloseEmergente = True
                                        iNumRows = Request("CALC_" + sPre + "_fsentry" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "__numRows")
                                    End If
                                Else
                                    'Desglose no Emergente "CALC_uwtGrupos__ctl0xx_5491_5491_133510_tblDesglose"
                                    iNumRows = Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_tblDesglose")
                                    Dim conta As Integer = 1
                                    If iNumRows <> Nothing Then
                                        bDesgloseEmergente = False
                                        ReDim lineasReal(iNumRows)
                                        For numLinea As Integer = 1 To iNumRows
                                            For campoCalc As Integer = 0 To oDSDesglose.Tables(0).Rows.Count - 1
                                                If Not Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + numLinea.ToString + "_" + oDSDesglose.Tables(0).Rows(campoCalc).Item("ID_CAMPO").ToString) Is Nothing Then
                                                    lineasReal(conta) = numLinea
                                                    Exit For
                                                End If
                                            Next
                                            conta += 1
                                        Next
                                    Else
                                        bDesgloseEmergente = True
                                        iNumRows = Request("CALC_" + sPre + "_fsentry" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "__numTotRows")
                                        ReDim lineasReal(iNumRows)
                                        For numLinea As Integer = 1 To iNumRows
                                            For campoCalc As Integer = 0 To oDSDesglose.Tables(0).Rows.Count - 1
                                                If Not Request("CALC_" + sPre + "_fsentry" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "__" + numLinea.ToString + "__" + oDSDesglose.Tables(0).Rows(campoCalc).Item("ID_CAMPO").ToString) Is Nothing Then
                                                    lineasReal(conta) = numLinea
                                                    Exit For
                                                End If
                                            Next
                                            conta += 1
                                        Next
                                    End If
                                End If
                                bPopUp = True
                            Else
                                If Not blnAlta Then
                                    ReDim lineasReal(iNumRows)
                                    Dim conta As Integer = 1
                                    For numLinea As Integer = 1 To iNumRows
                                        For campoCalc As Integer = 0 To oDSDesglose.Tables(0).Rows.Count - 1
                                            If Not Request("CALC_" + sPre + "_fsdsentry_" + numLinea.ToString + "_" + oDSDesglose.Tables(0).Rows(campoCalc).Item("ID_CAMPO").ToString) Is Nothing Then
                                                lineasReal(conta) = numLinea
                                                Exit For
                                            End If
                                        Next
                                        conta += 1
                                    Next
                                End If
                            End If
                        Else
                            iNumRows = 0
                            If oDSDesglose.Tables(1).Rows.Count > 0 Then
                                For Each oDSRowDesgloseOculto In oDSDesglose.Tables(1).Rows
                                    If oDSRowDesgloseOculto.Item("Linea") > iNumRows Then
                                        iNumRows = oDSRowDesgloseOculto.Item("Linea")
                                        bEstaInvisibleElDesglose = True
                                    End If
                                Next
                            End If
                        End If

                        Dim NumEliminadas As Integer
                        NumEliminadas = 0
                        Dim EliminadoporNothing As Boolean
                        EliminadoporNothing = False

                        For k = 1 To iNumRows
                            If blnAlta OrElse lineasReal.Contains(k) Then
                                EliminadoporNothing = False
                                iDesglose = 0
                                For Each oDSRowDesglose In oDSDesglose.Tables(0).Rows
                                    sVariablesDesglose(iDesglose) = oDSRowDesglose.Item("ID_CALCULO")
                                    bCalcular = True

                                    iTipoGSDesglose = DBNullToSomething(oDSRowDesglose.Item("TIPO_CAMPO_GS"))
                                    iTipoDesglose = oDSRow.Item("SUBTIPO")
									vValor = Nothing

                                    If oDSRowDesglose.Item("VISIBLE") = 1 And Not bEstaInvisibleElDesglose Then
                                        If blnAlta Then
                                            If bPopUp Then
                                                If bDesgloseEmergente Then
                                                    sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ORIGEN_CALC_DESGLOSE") & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID")
                                                Else
                                                    'CALC_uwtGrupos__ctl0xx_623_623_10549_fsdsentry_1_10551
                                                    sRequest = "CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID").ToString
                                                End If
                                            Else
                                                'CALC_uwtGrupos__ctl0x_146_fsdsentry_1_2283
                                                sRequest = "CALC_" + sPre + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID").ToString()
                                            End If
                                        Else
                                            If bPopUp Then
                                                If bDesgloseEmergente Then
                                                    sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE") & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID_CAMPO")
                                                Else
                                                    'CALC_uwtGrupos__ctl0xx_5491_5491_133510_fsdsentry_1_133486
                                                    sRequest = "CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID_CAMPO").ToString
                                                End If
                                            Else
                                                'CALC_uwtGrupos__ctl0x_146_fsdsentry_1_2283
                                                sRequest = "CALC_" + sPre + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID_CAMPO").ToString()
                                            End If
                                        End If
                                        vValor = Numero(VacioToNothing(Request(sRequest)), ".", ",")

                                        If Request(sRequest) Is Nothing Then
                                            EliminadoporNothing = True
                                        End If
                                    Else
                                        keysDesglose(0) = oDSDesglose.Tables(1).Columns("LINEA")
                                        keysDesglose(1) = oDSDesglose.Tables(1).Columns("CAMPO_PADRE")
                                        keysDesglose(2) = oDSDesglose.Tables(1).Columns("CAMPO_HIJO")

                                        oDSDesglose.Tables(1).PrimaryKey = keysDesglose
                                        iLinea = k
                                        findDesglose(0) = (k).ToString()
                                        If blnAlta Then
                                            findDesglose(1) = oDSRow.Item("ORIGEN_CALC_DESGLOSE")
                                            findDesglose(2) = oDSRowDesglose.Item("ID").ToString()
                                        Else
                                            findDesglose(1) = oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE")
                                            findDesglose(2) = oDSRowDesglose.Item("ID_CAMPO").ToString()
                                        End If
                                        oRowDesglose = oDSDesglose.Tables(1).Rows.Find(findDesglose)
                                        While oRowDesglose Is Nothing And iLinea > 0
                                            iLinea -= 1
                                            findDesglose(0) = iLinea

                                            oRowDesglose = oDSDesglose.Tables(1).Rows.Find(findDesglose)
                                        End While
                                        If Not oRowDesglose Is Nothing Then
                                            vValor = DBNullToSomething(oRowDesglose.Item("VALOR_NUM"))
                                        End If
                                    End If

                                    If oDSRowDesglose.Item("TIPO") = TipoCampoPredefinido.Calculado Then
                                        dValuesDesglose(iDesglose) = 0
                                    Else
                                        dValuesDesglose(iDesglose) = modUtilidades.Numero(vValor)
                                    End If
                                    dValuesTotalDesglose(iDesglose) += dValuesDesglose(iDesglose)
                                    iDesglose += 1
                                Next
                                iDesglose = 0
                                For Each oDSRowDesglose In oDSDesglose.Tables(0).Rows
                                    If oDSRowDesglose.Item("TIPO") = TipoCampoPredefinido.Calculado Then
                                        Try
                                            If Not EliminadoporNothing Then
                                                iEq.Parse(oDSRowDesglose.Item("FORMULA"), sVariablesDesglose)
                                                dValuesDesglose(iDesglose) = iEq.Evaluate(dValuesDesglose)
                                                iLinea = k
                                                findDesglose(0) = (k - NumEliminadas).ToString()
                                                If blnAlta Then
                                                    findDesglose(1) = oDSRow.Item("ORIGEN_CALC_DESGLOSE")
                                                    findDesglose(2) = oDSRowDesglose.Item("ID").ToString()
                                                Else
                                                    findDesglose(1) = oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE")
                                                    findDesglose(2) = oDSRowDesglose.Item("ID_CAMPO").ToString()
                                                End If
                                                oRowDesglose = Nothing
                                                oRowDesglose = oDTDesglose.Rows.Find(findDesglose)

                                                If Not oRowDesglose Is Nothing Then
                                                    oRowDesglose.Item("VALOR_NUM") = dValuesDesglose(iDesglose)
                                                    dValuesTotalDesglose(iDesglose) += dValuesDesglose(iDesglose)
                                                Else
                                                    'En la tabla temporal
                                                    If oDSRow.Item("VISIBLE") = 0 Then
                                                        dValuesTotalDesglose(iDesglose) += dValuesDesglose(iDesglose)

                                                    Else
                                                        ' NumEliminadas += 1
                                                    End If

                                                End If
                                            Else
                                                NumEliminadas += 1
                                            End If
                                        Catch ex As USPExpress.ParseException
                                        Catch ex0 As Exception
                                        End Try
                                    End If
                                    iDesglose += 1
                                Next
                                ReDim Preserve sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count)
                                ReDim Preserve dValuesDesglose(oDSDesglose.Tables(0).Rows.Count)

                                sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count) = oDSRow.Item("FORMULA") + " * 1"
                                iEq.Parse(oDSRow.Item("FORMULA"), sVariablesDesglose)

                                dValuesDesglose(oDSDesglose.Tables(0).Rows.Count) = iEq.Evaluate(dValuesDesglose)
                                dTotalDesglose += dValuesDesglose(oDSDesglose.Tables(0).Rows.Count)
                            End If
                        Next

                        If Not IsDBNull(oDSRow.Item("FORMULA")) Then
                            Try
                                If bCalcular = True Then
                                    If oDSRow.Item("ES_SUBCAMPO") = 0 Then
                                        dTotalDesglose = iEq.Evaluate(dValuesTotalDesglose)
                                    End If

                                    dValues(i) = dTotalDesglose
                                End If

                                If blnAlta Then
                                    find(0) = oDSRow.Item("ID")
                                Else
                                    find(0) = oDSRow.Item("ID_CAMPO")
                                End If
                                oRow = dt.Rows.Find(find)
                                If Not oRow Is Nothing Then
                                    oRow.Item("VALOR_NUM") = dValues(i)
                                End If
                            Catch ex As USPExpress.ParseException
                            Catch ex0 As Exception
                            End Try
                        End If
                        If (oDSRow.Item("ES_IMPORTE_TOTAL") = 1 AndAlso oDSRow.Item("ES_SUBCAMPO") = 0) Then
                            dTotal = dValues(i)
                        End If
                    Else
                        dValues(i) = 0
                    End If
                Else
                    dValues(i) = modUtilidades.Numero(vValor)
                    If (oDSRow.Item("ES_IMPORTE_TOTAL") = 1 AndAlso oDSRow.Item("ES_SUBCAMPO") = 0) Then
                        dTotal = dValues(i)
                    End If
                End If
                i += 1
            Next
            i = 0

            For Each oDSRow In oDS.Tables(0).Rows
                sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())
                If oDSRow.Item("TIPO") = PMPortalServer.TipoCampoPredefinido.Calculado Then
                    If blnAlta Then
                        find(0) = oDSRow.Item("ID")
                    Else
                        find(0) = oDSRow.Item("ID_CAMPO")
                    End If

                    oRow = dt.Rows.Find(find)
                    Try
                        iEq.Parse(oDSRow.Item("FORMULA"), sVariables)
                        dValues(i) = iEq.Evaluate(dValues)
                        If Not oRow Is Nothing Then
                            oRow.Item("VALOR_NUM") = dValues(i)
                        End If
                    Catch ex As USPExpress.ParseException
                    Catch ex0 As Exception
                    End Try

                    If DBNullToSomething(oDSRow.Item("ES_IMPORTE_TOTAL") AndAlso DBNullToSomething(oDSRow.Item("ES_SUBCAMPO")) = 0) = 1 Then
                        oRowWorkflow = oRow
                    End If
                End If
                i += 1
            Next

            If Not oRowWorkflow Is Nothing Then
                dTotal = DBNullToSomething(oRowWorkflow.Item("VALOR_NUM"))
            End If

            Session("Importe") = dTotal

            '********* Precondiciones *******************************************************
            Dim bBloquearPorPrecondiciones As Boolean = False
            If iAccion <> Nothing Then
                Dim dsPrecondiciones As DataSet
                If blnAlta Then
                    dsPrecondiciones = oSolicitud.ObtenerPrecondicionesAccion(lCiaComp, iAccion, Idioma)
                Else
                    dsPrecondiciones = oInstancia.ObtenerPrecondicionesAccion(lCiaComp, iAccion, Idioma)
                End If

                Dim dtAvisos As DataTable
                dtAvisos = ComprobarPrecondiciones(lCiaComp, oInstancia, dsPrecondiciones, ds)

                If dtAvisos.Rows.Count > 0 Then
                    'Mostrar los mensajes en un popup
                    Dim j As Integer = 1
                    For Each drAviso As DataRow In dtAvisos.Rows
                        Dim oLabel As HtmlInputHidden = New HtmlInputHidden
                        oLabel.Name = "Aviso"
                        oLabel.ID = "Aviso" & j
                        oLabel.Value = drAviso.Item("TIPO") & "|" & drAviso.Item("COD") & "|" & drAviso.Item("DEN")
                        ContenedorAvisos.Controls.Add(oLabel)
                        j = j + 1
                    Next
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "init", "<script>montarFormularioAvisos(); enviarAvisos();</script>")

                    bBloquearPorPrecondiciones = dtAvisos.Select("TIPO = 2").Length > 0
                    If bBloquearPorPrecondiciones Then
                        'Mahou San Miguel / 2017 / 93:
                        'oInstancia.Actualizar_En_proceso(lCiaComp, 0, hBloque.Value)
                        'oInstancia.Actualizar_En_proceso(lCiaComp, 0)

                        Return False
                    End If
                End If
            End If
            '********** Control de Integraci�n de art�culos **************************
            'Para sacar el ERP lo hace a partir de un campo OrgCompras si el par�metro gUsar_OrgCompras est� activo por lo que quien no use este par�metro tendr�a que sacarlo de otro sitio.
            If iAccion <> 0 Then 'SOLO PARA LOS CAMBIOS DE ETAPA
                If hGuardar.Value = 1 Then
                    'comprobar que la accion no sea de rechazo definitivo o anulaci�n
                    If Not (oInstancia.AccionSinControlDisponible(IdCiaComp, iAccion)) Then
                        Dim oIntegracion As PMPortalServer.Integracion
                        oIntegracion = FSPMServer.Get_Object(GetType(PMPortalServer.Integracion))
                        'Creamos dos conjuntos para almacenar los c�digos de art�culos diferentes presentes en la solicitud y as� no realizar comprobaciones
                        ' sobre integraci�n de un mismo art�culo.
                        Dim aArticulosNoIntegrados As New HashSet(Of String)
                        Dim aArticulosIntegrados As New HashSet(Of String)
                        Dim texto As String = ""
                        Dim sOrgCompras As String = ""
                        Dim drOrgCompras() As DataRow
                        'comprobar organizaci�n de compras

                        If Acceso.gbUsar_OrgCompras Then
                            'comprobar si existe campo del tipo organizaci�n de compras
                            ' cabecera
                            drOrgCompras = ds.Tables("TEMP").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND VALOR_TEXT IS NOT NULL")
                            If drOrgCompras.Length > 0 Then
                                sOrgCompras = DBNullToSomething(drOrgCompras(0).Item("VALOR_TEXT"))
                            End If

                            ' desglose
                            If sOrgCompras = "" Then
                                drOrgCompras = ds.Tables("TEMPDESGLOSE").Select("TIPOGS=" & TiposDeDatos.TipoCampoGS.OrganizacionCompras & " AND VALOR_TEXT IS NOT NULL")
                                If drOrgCompras.Length > 0 Then
                                    sOrgCompras = DBNullToSomething(drOrgCompras(0).Item("VALOR_TEXT"))
                                End If
                            End If

                            If sOrgCompras <> "" Then
                                'Extraer erp a partir de organizacion de compras
                                Dim sErp As String = oIntegracion.getERPFromOrgCompras(IdCiaComp, sOrgCompras)
                                If oIntegracion.hayIntegracionPM(IdCiaComp, sErp) Then
                                    If oIntegracion.hayIntegracionArticulos(IdCiaComp, sErp) Then
                                        For Each oDSRow In ds.Tables("TEMP").Rows
                                            If DBNullToSomething(oDSRow.Item("TIPOGS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
                                                'si el campo tipo articulo tiene contenido 
                                                If DBNullToStr(oDSRow.Item("VALOR_TEXT")) <> "" Then
                                                    'verificamos que no haya sido comprobado ya
                                                    If Not aArticulosNoIntegrados.Contains(oDSRow.Item("VALOR_TEXT")) AndAlso
                                                        Not aArticulosIntegrados.Contains(oDSRow.Item("VALOR_TEXT")) Then
                                                        If DBNullToStr(oDSRow.Item("VALOR_TEXT")) <> "" Then
                                                            If Not oIntegracion.estaIntegradoArticulo(IdCiaComp, oDSRow.Item("VALOR_TEXT"), sErp) Then
                                                                aArticulosNoIntegrados.Add(oDSRow.Item("VALOR_TEXT"))
                                                            Else
                                                                aArticulosIntegrados.Add(oDSRow.Item("VALOR_TEXT"))
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        Next

                                        For Each oDSRow In ds.Tables("TEMPDESGLOSE").Rows
                                            If DBNullToSomething(oDSRow("TIPOGS")) = TiposDeDatos.TipoCampoGS.NuevoCodArticulo Then
                                                'si el campo tipo articulo tiene contenido y no se encuentra entre los ya comprobados
                                                If DBNullToStr(oDSRow.Item("VALOR_TEXT")) <> "" Then
                                                    If Not aArticulosNoIntegrados.Contains(oDSRow.Item("VALOR_TEXT")) AndAlso
                                                        Not aArticulosIntegrados.Contains(oDSRow.Item("VALOR_TEXT")) Then
                                                        If (DBNullToStr(oDSRow.Item("VALOR_TEXT")) <> "") Then
                                                            If Not oIntegracion.estaIntegradoArticulo(IdCiaComp, oDSRow.Item("VALOR_TEXT"), sErp) Then
                                                                aArticulosNoIntegrados.Add(oDSRow.Item("VALOR_TEXT"))
                                                            Else
                                                                aArticulosIntegrados.Add(oDSRow.Item("VALOR_TEXT"))
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        Next
                                        If aArticulosNoIntegrados.Count > 0 Then
                                            Dim arts As String = String.Join(",", aArticulosNoIntegrados.ToArray)
                                            Dim textoIntegracion = Replace(Textos(7), "\n", Chr(10)) & arts  ' "�Imposible realizar acci�n! \n Los siguientes art�culos no se encuentran correctamente integrados : " 
                                            Page.ClientScript.RegisterStartupScript(Me.GetType(), "init", "<script>alert('" & JSText(textoIntegracion) & "');</script>")
                                            Return False
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            '*****************************************************************************
            If Request("GEN_Guardar") = 1 Or Request("GEN_Accion") = "devolversolicitud" Then
                bEnviar = (Request("GEN_Enviar") = 1)

                If lIDRefSol <> 0 Then
                    Dim oInstancias As PMPortalServer.Instancias
                    oInstancias = FSPMServer.Get_Instancias
                    oInstancias.DevolverSolicitudesPadre(lCiaComp, Idioma, lIDRefSol)

                    If oInstancias.Data.Tables(0).Rows.Count > 0 Then
                        '(Disponible - ImporteInstancia)
                        If (oInstancias.Data.Tables(0).Rows(0).Item(4) - dTotal) < 0 Then
                            Dim sClientTexts As String
                            Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"
                            sClientTexts = ""
                            sClientTexts += "var arrTextosML = new Array();"
                            sClientTexts += "arrTextosML[0] = '" + JSText(Textos(2)) + "';" '�Imposible realizar la acci�n! Se ha superado el importe acumulado para la solicitud
                            sClientTexts += "arrTextosML[1] = '" + JSText(Textos(3)) + "';" '�Atenci�n! Se ha superado el importe acumulado para la solicitud
                            sClientTexts += "arrTextosML[2] = '" + JSText(Textos(4)) + "';" 'Importe acumulado
                            sClientTexts += "arrTextosML[3] = '" + JSText(Textos(5)) + "';" 'Importe solicitud

                            sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
                            Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

                            bMostrarMensaje = True
                            Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript", "<script>ControlImporte('" & oInstancias.Data.Tables(0).Rows(0).Item(1) & "', " & (oInstancias.Data.Tables(0).Rows(0).Item(3) - oInstancias.Data.Tables(0).Rows(0).Item(4) + dTotal) & "," & oInstancias.Data.Tables(0).Rows(0).Item(3) & "," & AvisoBloqueo & ")</script>")
                        End If
                    End If
                    oInstancias = Nothing
                End If
                If (bMostrarMensaje And AvisoBloqueo = PMPortalServer.TipoAvisoBloqueo.Bloqueo And sAccion = "") Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "HabilitarBotones", "<script>if (window.parent.fraPMPortalMain != null) window.parent.fraPMPortalMain.HabilitarBotones();</script>")
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "CerrarEspera", "<script>if (window.parent.fraPMPortalMain.wProgreso != null) window.parent.fraPMPortalMain.wProgreso.close();</script>")
                End If
            End If

            If blnAlta And lInstancia = 0 Then 'SI ES UNA ALTA Y NO HEMOS CREADO LA INSTANCIA, LA CREAMOS
                oInstancia.Solicitud = oSolicitud
                oInstancia.PeticionarioProve = FSPMUser.CodProveGS
                oInstancia.PeticionarioProveContacto = FSPMUser.IdContacto
                oInstancia.Create_Prev(lCiaComp)
                lInstancia = oInstancia.ID
                hInstancia.Value = lInstancia
                oInstancia.FechaAlta = Now
                oInstancia.Actualizar_En_proceso(lCiaComp, 1, lBloque)
            Else
                oInstancia.ID = lInstancia
            End If

            GuardarWorkflow_GenerarXML()

            Select Case sAccion
                Case "devolversolicitud", "trasladadadevolvercontrato"
                    If oInstancia.Estado = PMPortalServer.TipoEstadoSolic.Anulada Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "init", "<script>window.open(""" & "imposibleaccion.aspx?Instancia=" & oInstancia.ID & "&Accion=5" & """,""fraPMPortalMain"")</script>")
                    Else
                        PrepararConfirmacionDevolucion()
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "Devolucion", "<script>divRealizarAccion.style.display='none';divDevolucion.style.display='inline';</script>")
                    End If
                Case Else  'Realiza la acci�n:
                    If Not (bMostrarMensaje And AvisoBloqueo = PMPortalServer.TipoAvisoBloqueo.Bloqueo And sAccion = "") Then
                        If Not PrepararConfirmacionRealizarAccion() Then Return False
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "RealizarAccion", "<script>divRealizarAccion.style.display='inline';divDevolucion.style.display='none';</script>")
                    End If
            End Select
            Return True
        End Function
        Private m_sIdiListaPart As String
        Private m_sIdiMsgbox As String
        Private m_sIdiSeleccioneRol As String
        ''' <summary>Carga los textos de pantalla y el grid de roles para confirmar/cancelar la ejecuci�n de una acci�n</summary>
        ''' <remarks>Llamada desde:GuardarConWorkflow; Tiempo m�ximo:0,2</remarks>
        Private Function PrepararConfirmacionRealizarAccion() As Boolean
            If Not sAccion = "guardarsolicitud" Then 'ES UNA ACCION CUALQUIERA
                ModuloIdioma = TiposDeDatos.ModulosIdiomas.RealizarAccion

                'Textos de idiomas:
                lblLitFecAlta.Text = Textos(1)
                lblLitCreadoPor.Text = Textos(2)
                If oSolicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.Factura Then
                    lblEtapas.Text = Textos(17)
                Else
                    lblEtapas.Text = Textos(3)
                End If
                m_sIdiSeleccioneRol = Textos(5)
                lblComentarios.Text = Textos(8)
                cmdAceptar.Value = Textos(9)
                cmdCancelar.Value = Textos(10)
                Session("m_sIdiMsgbox") = Textos(13)

                'Carga los datos de la instancia:
                oInstancia.ID = lInstancia
                oInstancia.Load(IdCiaComp, Idioma, True)
                oInstancia.Solicitud.Load(IdCiaComp, Idioma)
                hPeticionario.Value = oInstancia.Peticionario

                'Carga la acci�n:
                Dim oAccion As PMPortalServer.Accion
                oAccion = FSPMServer.Get_Accion
                oAccion.Id = iAccion
                oAccion.CargarAccion(IdCiaComp, Idioma)
                If Contrato.Value <> "" Then
                    FSNPageHeader.TituloCabecera = CodContrato.Value & " - " & Textos(11) & " " & oAccion.Den
                Else
                    FSNPageHeader.TituloCabecera = oInstancia.ID & " - " & Textos(11) & " " & oAccion.Den
                End If
                If oAccion.TipoRechazo = PMPortalServer.TipoRechazoAccion.RechazoDefinitivo Or oAccion.TipoRechazo = PMPortalServer.TipoRechazoAccion.RechazoTemporal Then
                    lblRoles.Text = Textos(14)
                Else
                    lblRoles.Text = Textos(4)
                End If
                oInstancia.Importe = Session("Importe")

                'Comprueba las etapas por las que pasar�:
                Dim oEtapas As DataSet
                Dim oRow As DataRow
                If blnAlta Then
                    oEtapas = oInstancia.DevolverSiguientesEtapasForm(IdCiaComp, iAccion, FSPMUser.CodProveGS, Idioma, ds, hBloque.Value, RolActual.Value, sRolPorWebService)
                Else
                    oEtapas = oInstancia.DevolverSiguientesEtapas(IdCiaComp, iAccion, FSPMUser.CodProve, Idioma, hBloque.Value, RolActual.Value)
                End If

                '********************* MAPPER **************************
                Dim MapperStr As String
                Dim iNumError As Integer
				Dim oErrorMapper As Object = Nothing
				Dim TextosGS As PMPortalServer.TextosGS
				If Not String.IsNullOrEmpty(Request("PantallaMaper")) AndAlso CBool(Request("PantallaMaper")) AndAlso CBool(Request("GEN_MostrarAvisoMapper")) Then 'SOLO CUANDO PROCEDA
					Dim strBloqueDestino As String = ""
					If (oEtapas.Tables.Count > 0) AndAlso (oEtapas.Tables.Count > 1) Then
						For Each miRow As DataRow In oEtapas.Tables(0).Rows
							strBloqueDestino = strBloqueDestino & miRow.Item("BLOQUE") & ","
						Next
						strBloqueDestino = IIf(Right(strBloqueDestino, 1) = ",", Left(strBloqueDestino, Len(strBloqueDestino) - 1), strBloqueDestino)
					End If

					oErrorMapper = oInstancia.ControlMapper(IdCiaComp, FSPMServer, dsMapper, Idioma, Val(Request("GEN_AccionRol")), strBloqueDestino, blnAlta, oInstancia.Peticionario, oInstancia.PeticionarioProve, oInstancia.PeticionarioProveContacto)
					If Not oErrorMapper Is Nothing Then
						TextosGS = FSPMServer.Get_Object(GetType(PMPortalServer.TextosGS))
						iNumError = CInt(oErrorMapper.iNumError)
						If iNumError = -100 Then
							MapperStr = oErrorMapper.strError
						Else
							MapperStr = TextosGS.MensajeError(IdCiaComp, MapperModuloMensaje.PedidoDirecto, iNumError, Idioma, oErrorMapper.strError)
						End If

						oInstancia.Actualizar_En_proceso(IdCiaComp, 0, strToLong(Request("GEN_Bloque").ToString))
						oInstancia.Actualizar_En_proceso(IdCiaComp, 0)

						If oErrorMapper.tipoValidacionIntegracion = TipoValidacionIntegracion.Mensaje_Bloqueo Then
							Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "MapperAlert",
																		"alert('" & JSText(MapperStr) & "'); if (window.parent.fraWSMain != null) window.parent.fraWSMain.HabilitarBotones();", True)
						Else 'Aviso
							Dim ModuloIdiomaAux As TiposDeDatos.ModulosIdiomas = ModuloIdioma
							ModuloIdioma = TiposDeDatos.ModulosIdiomas.GuardarInstancia
							MapperStr = oErrorMapper.strError & Chr(10) & Textos(8)
							ModuloIdioma = ModuloIdiomaAux
							Dim sJavaScript As String
							sJavaScript = "if(window.parent.fraPMPortalMain!= null && typeof(window.parent.fraPMPortalMain.OcultarEspera)=='function') window.parent.fraPMPortalMain.OcultarEspera();"
							sJavaScript &= "if (confirm('" & JSText(MapperStr) & "')) ConfirmarAvisoValidacionMapper();"
							sJavaScript &= "if (window.parent.fraWSMain!= null) window.parent.fraWSMain.HabilitarBotones();"
							Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "MapperAlert", sJavaScript, True)
						End If
						Return False
					End If
				End If
				'********************* RolPorWebService *****************
				Dim RolPorWebService As Boolean = False
                If Not sRolPorWebService = "-1" Then
                    If Not (sAccion = "guardarsolicitud" OrElse sAccion = "guardarcontrato") Then
                        Dim ContadorParalelo As Integer = 0 ' 0-> no etapas paralelo. Eoc-> etapas paralelo.
                        Dim sBloqueRolPorWebService() As String
                        sBloqueRolPorWebService = Split(sRolPorWebService, ",")
                        Dim sArrRolPorWebService() As String
                        Dim lRolPorWebService As String

                        If (oEtapas.Tables.Count > 1) Then
                            For Each row As DataRow In oEtapas.Tables(0).Rows
                                hBloqueDestino.Value = row.Item("BLOQUE")
                                lRolPorWebService = -1
                                For i As Integer = 0 To UBound(sBloqueRolPorWebService)
                                    sArrRolPorWebService = Split(sBloqueRolPorWebService(i), "@")
                                    If sArrRolPorWebService(0) = CStr(row.Item("BLOQUE")) Then
                                        lRolPorWebService = CLng(sArrRolPorWebService(1))
                                        Exit For
                                    End If
                                Next
                                If lRolPorWebService > -1 Then
                                    RolPorWebService = True
                                    GuardarConWorkflow_ProcesarXML()
                                    hBloqueDestino.Value = ""

                                    Dim HaIdoMalWebService As Boolean = Instancia_LlamadaWebService(lRolPorWebService, row.Item("BLOQUE"), ContadorParalelo)
                                    ContadorParalelo = ContadorParalelo + 1
                                    If HaIdoMalWebService Then
                                        Session.Remove(nombreXML)
                                        oInstancia.Actualizar_En_proceso(IdCiaComp, 0, hBloque.Value)
                                        oInstancia.Actualizar_En_proceso(IdCiaComp, 0)
                                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AbrirEnfraWSMain", "<script>AbrirEnfraWSMain(""" & "../workflow/accionInvalida.aspx?Instancia=" & lInstancia & "&Accion=" & iAccion & "&Version=" & Request("GEN_Version") & """)</script>")
                                        Return False
                                    End If
                                Else
                                    hBloqueDestino.Value = ""
                                End If
                            Next
                        End If
                    End If
                End If
                If oEtapas.Tables.Count > 0 Then
                    For Each oRow In oEtapas.Tables(0).Rows
                        hBloqueDestino.Value = hBloqueDestino.Value & oRow.Item("BLOQUE") & " "
                    Next

                    lstEtapas.DataSource = oEtapas.Tables(0)
                    lstEtapas.DataTextField = "DEN"
                    lstEtapas.DataBind()

                    If oEtapas.Tables(0).Rows(0).Item("TIPO") = 2 Then   'Si llega a la etapa de fin:
                        Page.EnableViewState = False
                        lblRoles.Visible = False
                        tblGeneral.Rows.RemoveAt(3)
                        lblComentarios.Text = String.Format("{0}:", Textos(15))
                    Else
                        If oEtapas.Tables(0).Rows(0).Item("TIPO") = 1 And Len(oInstancia.Peticionario) <> 0 Then
                            Page.EnableViewState = False
                            lblRoles.Visible = False
                            tblGeneral.Rows.RemoveAt(3)
                            lblComentarios.Text = String.Format("{0}:", Textos(15))
                        Else
                            wdgtxtRoles.Rows.Clear()
                            wdgtxtRoles.Columns.Clear()
                            wdgtxtRoles.DataSource = oEtapas.Tables(1)
                            CrearColumnas()
                            wdgtxtRoles.DataBind()
                            ConfigurarGrid()

                            wdgtxtRoles.Columns("DEN").Header.Text = Textos(6)
                            wdgtxtRoles.Columns("NOMBRE").Header.Text = If(oInstancia.Solicitud.TipoSolicit = TiposDeDatos.TipoDeSolicitud.Factura, Replace(Textos(7), " PM ", " IM "), Textos(7))

                            m_sIdiListaPart = Textos(12)
                        End If
                    End If
                Else
                    If oAccion.Tipo = 0 And oAccion.TipoRechazo = 0 And oAccion.Guardar = True Then
                        sAccion = "guardarsolicitud"
                        hAccion.Value = sAccion
                    Else
                        'No provoca ning�n cambio de etapa, as� que quita del formulario las filas correspondientes a los cambios de etapas y formularios:
                        Dim x As Integer
                        For x = 6 To 2 Step -1
                            tblGeneral.Rows(x).Visible = False
                        Next x
                    End If
                End If
                oEtapas = Nothing
            Else 'HEMOS PULSADO EN GUARDAR
                If blnAlta Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "PonerIdyEstado", "<script>window.parent.fraPMPortalMain.PonerIdSolicitud(" & lInstancia & ");</script>")
                Else
                    'Al guardar una solicitud ya guardada anteriormente (NWDetalleSolicitud) cambiando la moneda, oInstancia.Importe vale distinto que ViewState("Importe"). Se hace oInstancia.Importe = ViewState("Importe") cuando la acci�n es distinta de "guardarsolicitud" y "guardarcontrato" (�?).
                    'ViewState("Importe") es lo que se manda en el XML como importe de la solicitud.
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "HabilitarBotones", "<script>window.parent.fraPMPortalMain.HabilitarBotones(); if(window.parent.fraWSMain.lblImporte) window.parent.fraWSMain.lblImporte.innerHTML='" & FSNLibrary.FormatNumber(ViewState("Importe"), FSPMUser.NumberFormat) & " " & oInstancia.Moneda & "';</script>")
                End If
            End If
            Return True
        End Function
        ''' <summary>
        ''' Carga los textos de pantalla y el grid de roles para confirmar/cancelar la ejecuci�n de una devoluci�n
        ''' </summary>
        ''' <remarks>Llamada desde:GuardarConWorkflow; Tiempo m�ximo:0,2</remarks>
        Private Sub PrepararConfirmacionDevolucion()

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.Devolucion

            'Textos de idiomas:
            lblLitCreadoPor.Text = Textos(11) & ":"
            lblLitFecAlta.Text = Textos(12) & ":"
            lblLitTipo.Text = Textos(16) & ":"
            lblOrigen.Text = Textos(5)
            lblComent.Text = Textos(6)
            cmdAceptarDevolucion.Value = Textos(7)
            cmdCancelarDevolucion.Value = Textos(8)
            cadenaespera.Value = Textos(10)

            'Textos Panel info
            FSNPanelDatosPeticionario.Titulo = Textos(14) '"Peticionario"
            FSNPanelDatosPeticionario.SubTitulo = Textos(15) '"Informacion detallada"

            'Carga los datos de la instancia:
            oInstancia.ID = lInstancia
            oInstancia.Load(IdCiaComp, Idioma, True)
            oInstancia.Solicitud.Load(IdCiaComp, Idioma)

            If Not FSPMUser.VerDetallePersona(oInstancia.ID, IdCiaComp).Rows(0)("VER_DETALLE_PER") Then
                imgInfCreadoPor.Visible = False
                lblLitCreadoPor.Visible = False
                lblCreadoPor.Visible = False
            Else
                If oInstancia.PeticionarioProve = "" Then
                    imgInfCreadoPor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosPeticionario.AnimationClientID & "', event, '" & FSNPanelDatosPeticionario.DynamicPopulateClientID & "', '" & oInstancia.Peticionario & "'); return false;")
                Else
                    imgInfCreadoPor.Attributes.Add("onclick", "FSNMostrarPanel('" & FSNPanelDatosProveedor.AnimationClientID & "', event, '" & FSNPanelDatosProveedor.DynamicPopulateClientID & "', '" & oInstancia.PeticionarioProve & "'); return false;")
                End If
            End If

            'Comprueba si el proveedor es rol activo o traslado activo en la solicitud:
            oInstancia.DevolverEtapaActual(IdCiaComp, Idioma, FSPMUser.CodProveGS)
            hBloqueDestino.Value = oInstancia.InstanciaBloque
            oInstancia.DevolverDatosTraslado(IdCiaComp, FSPMUser.CodProveGS)

            'Datos de la instancia:
            FSNPageHeader.TituloCabecera = Textos(0) & ": " & If(oInstancia.Den(Idioma) = "", oInstancia.Solicitud.Den(Idioma), oInstancia.Den(Idioma))
            If CodContrato.Value <> "" Then
                lblIDInstanciayEstado.Text = CodContrato.Value
            Else
                lblIDInstanciayEstado.Text = oInstancia.ID
            End If

            If oInstancia.PeticionarioProve = "" Then
                lblCreadoPor.Text = oInstancia.NombrePeticionario
            Else
                lblCreadoPor.Text = oInstancia.PeticionarioProve + " (" + oInstancia.NombrePeticionario + ")"
            End If
            lblFecAlta.Text = modUtilidades.FormatDate(oInstancia.FechaAlta, FSPMUser.DateFormat)
            lblTipo.Text = oInstancia.Solicitud.Codigo & " - " & oInstancia.Solicitud.Den(Idioma)
            imgInfTipo.Attributes.Add("onclick", "DetalleTipoSolicitud(" & oInstancia.Solicitud.ID & ")")
            hPeticionario.Value = oInstancia.Peticionario
            lblUsuario.Text = oInstancia.NombrePersonaEst
            hDestinatario.Value = oInstancia.NombrePersonaEst

            Page.ClientScript.RegisterStartupScript(Me.GetType(), "DivCabecera", "<script>divCabecera.style.display='inline';</script>")
        End Sub
        Private Function DameDescripcion(ByVal IdTabla As Short, ByVal IdLista As Integer, ByVal Campo As Integer, ByVal oInstancia As PMPortalServer.Instancia, ByVal bSave As Boolean, ByVal Idioma As String) As String
            Dim bEsta As Boolean
            Dim dtCombosUso As DataSet
            Dim Indice As Integer = 0
            Dim iCampoLista() As Integer = Nothing
            Dim iIdLista() As Integer = Nothing
            Dim sTextoLista() As String
            DameDescripcion = String.Empty
            Try
                bEsta = False
                If Not iCampoLista Is Nothing Then
                    For i As Integer = 0 To UBound(iCampoLista)
                        If iCampoLista(i) = Campo Then
                            bEsta = True
                            Exit For
                        End If
                    Next
                    Indice = UBound(iCampoLista) + 1
                Else
                    Indice = 0
                End If

                If Not bEsta Then
                    dtCombosUso = oInstancia.CargaComboMapper(IdCiaComp, bSave, Campo, Idioma)

                    For Each oRow As DataRow In dtCombosUso.Tables(IdTabla).Rows
                        ReDim Preserve iCampoLista(Indice)
                        iCampoLista(Indice) = Campo

                        ReDim Preserve iIdLista(Indice)
                        iIdLista(Indice) = oRow("ORDEN")

                        ReDim Preserve sTextoLista(Indice)
                        sTextoLista(Indice) = oRow("VALOR_TEXT")

                        Indice = Indice + 1
                    Next
                End If

                For i As Integer = 0 To UBound(iCampoLista)
                    If iCampoLista(i) = Campo AndAlso iIdLista(i) = IdLista Then
                        DameDescripcion = sTextoLista(i)
                        Exit Function
                    End If
                Next
            Catch ex As Exception
                Return String.Empty
            End Try
        End Function
        Private Sub CrearColumnas()
            Dim campoGrid As Infragistics.Web.UI.GridControls.BoundDataField
            Dim nombre As String

            For i As Integer = 0 To wdgtxtRoles.DataSource.Columns.Count - 1
                campoGrid = New Infragistics.Web.UI.GridControls.BoundDataField(True)
                nombre = wdgtxtRoles.DataSource.Columns(i).ToString
                With campoGrid
                    .Key = nombre
                    .DataFieldName = nombre
                    .CssClass = "alturaFila"
                    .Header.Text = nombre
                    .Header.CssClass = "cabecera"
                End With
                wdgtxtRoles.Columns.Add(campoGrid)
            Next
            AnyadirColumnaU("SEL")
        End Sub
        Private Sub AnyadirColumnaU(ByVal fieldname As String)
            Dim unboundField As New Infragistics.Web.UI.GridControls.UnboundField(True)

            With unboundField
                .Key = fieldname
                .CssClass = "alturaFila"
                .Header.Text = fieldname
                .Header.CssClass = "cabecera"
            End With
            wdgtxtRoles.Columns.Add(unboundField)
        End Sub
        Private Sub ConfigurarGrid()
            wdgtxtRoles.Columns("SEL").VisibleIndex = 7
            'Oculta las columnas:
            wdgtxtRoles.Columns("ROL").Hidden = True
            wdgtxtRoles.Columns("PER").Hidden = True
            wdgtxtRoles.Columns("PROVE").Hidden = True
            wdgtxtRoles.Columns("COMO_ASIGNAR").Hidden = True
            wdgtxtRoles.Columns("CON").Hidden = True
            wdgtxtRoles.Columns("TIPO").Hidden = True
            wdgtxtRoles.Columns("GESTOR_FACTURA").Hidden = True
            wdgtxtRoles.Columns("ASIGNAR").Hidden = True

            wdgtxtRoles.Columns("DEN").Width = Unit.Percentage(35%)
            wdgtxtRoles.Columns("NOMBRE").Width = Unit.Percentage(65%)
            wdgtxtRoles.Columns("SEL").Width = Unit.Pixel(25)
        End Sub
        Private Sub wdgtxtRoles_InitializeRow(sender As Object, e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles wdgtxtRoles.InitializeRow
            If e.Row.Items.FindItemByKey("ASIGNAR").Value = 1 AndAlso DBNullToSomething(e.Row.Items.FindItemByKey("PROVE").Value) = Nothing AndAlso DBNullToSomething(e.Row.Items.FindItemByKey("PER").Value) = Nothing Then
                e.Row.Items.FindItemByKey("SEL").Text = "<input type=""button"" class=""btnSel"" value = ""..."""
                If DBNullToSomething(e.Row.Items.FindItemByKey("TIPO").Value) = "2" AndAlso DBNullToSomething(e.Row.Items.FindItemByKey("PROVE").Value) = Nothing Then
                    e.Row.Items.FindItemByKey("SEL").Text = e.Row.Items.FindItemByKey("SEL").Text & " onclick=""BuscadorProveedores(" & e.Row.Items.FindItemByKey("ROL").Value & ")"">"
                Else
                    e.Row.Items.FindItemByKey("SEL").Text = e.Row.Items.FindItemByKey("SEL").Text & " onclick=""BuscadorUsuarios(" & e.Row.Items.FindItemByKey("ROL").Value & ")"">"
                End If
            Else
                e.Row.Items.FindItemByKey("SEL").Text = "<input type=""button"" class=""btnSel"" value = ""   """
                If DBNullToSomething(e.Row.Items.FindItemByKey("PER").Value) <> Nothing Then
                    e.Row.Items.FindItemByKey("SEL").Text = e.Row.Items.FindItemByKey("SEL").Text & " onclick=""VerDetallePersona('" & e.Row.Items.FindItemByKey("PER").Value & "')"">"
                End If
            End If

            If DBNullToDbl(e.Row.Items.FindItemByKey("COMO_ASIGNAR").Value) = 3 Then  'Es una lista de participantes
                e.Row.Items.FindItemByKey("NOMBRE").Text = m_sIdiListaPart
            ElseIf DBNullToDbl(e.Row.Items.FindItemByKey("COMO_ASIGNAR").Value) = 1 And DBNullToStr(e.Row.Items.FindItemByKey("PER").Value) = Nothing And DBNullToStr(e.Row.Items.FindItemByKey("PROVE").Value) = Nothing Then
                e.Row.Items.FindItemByKey("NOMBRE").Text = m_sIdiSeleccioneRol
                e.Row.Items.FindItemByKey("NOMBRE").CssClass = "fntRequired"
            End If
        End Sub
        Public Function ComprobarPrecondiciones(ByVal lCiaComp As Long, ByVal oInstancia As PMPortalServer.Instancia, ByVal dsPrecondiciones As DataSet, ByVal dsCampos As DataSet) As DataTable
            Dim dtAvisos As DataTable = New DataTable

            dtAvisos.Columns.Add("TIPO", Type.GetType("System.Int32"))
            dtAvisos.Columns.Add("COD", Type.GetType("System.String"))
            dtAvisos.Columns.Add("DEN", Type.GetType("System.String"))

            Dim bCumple As Boolean
            If dsPrecondiciones.Tables.Count > 0 Then
                For Each drPrecondicion As DataRow In dsPrecondiciones.Tables(0).Rows
                    bCumple = ComprobarCondiciones(lCiaComp, oInstancia, drPrecondicion.Item("FORMULA"), drPrecondicion.GetChildRows("PRECOND_CONDICIONES"), dsCampos)
                    If bCumple Then
                        Dim newRow As DataRow = dtAvisos.NewRow
                        newRow.Item("TIPO") = drPrecondicion.Item("TIPO")
                        newRow.Item("COD") = drPrecondicion.Item("COD")
                        newRow.Item("DEN") = drPrecondicion.Item("DEN")
                        dtAvisos.Rows.Add(newRow)
                    End If
                Next
            End If
            Return dtAvisos
        End Function
        ''' <summary>
        ''' Comprobac��n de bloqueos por campos
        ''' </summary>
        ''' <param name="oInstancia">Instancia</param>
        ''' <param name="sFormula">Formula</param>
        ''' <param name="drCondiciones">Lista de bloqueos por campos</param>
        ''' <param name="dsCampos">Tabla Campos</param>
        ''' <returns>Si hay bloqueo o no</returns>
        ''' <remarks>Llamada desde:ComprobarPrecondiciones; Tiempo m�ximo:0,1</remarks>
        Public Function ComprobarCondiciones(ByVal lCiaComp As Long, ByVal oInstancia As PMPortalServer.Instancia, ByVal sFormula As String, ByVal drCondiciones As DataRow(), ByVal dsCampos As DataSet) As Boolean
            Dim drCampo As DataRow
            Dim oValorCampo As Object
            Dim oValor As Object
            Dim iTipo As TiposDeDatos.TipoGeneral
            Dim oDSFormula As New DataSet
            Dim oDSCond As New DataSet
            Dim oCond As DataRow
            Dim i As Long
            Dim sVariables() As String
            Dim dValues() As Double
            Dim iEq As New USPExpress.USPExpression
            Dim sMoneda As String
            Dim dCambioEnlace As Double
            Dim dCambioInstancia As Double

            Try
                'Cambio de moneda 
                dCambioInstancia = oInstancia.Cambio

                i = 0
                For Each oCond In drCondiciones
                    ReDim Preserve sVariables(i)
                    ReDim Preserve dValues(i)

                    sVariables(i) = oCond.Item("COD")
                    dValues(i) = 0 'El valor ser� 1 o 0 dependiendo de si se cumple o no la condici�n

                    sMoneda = DBNullToStr(oCond.Item("MON"))

                    dCambioEnlace = DBNullToDbl(oCond.Item("CAMBIO"))
                    '<EXPRESION IZQUIEDA> <OPERADOR> <EXPRESION DERECHA>

                    'Primero evaluamos <EXPRESION IZQUIERDA> 
                    Select Case oCond.Item("TIPO_CAMPO")
                        Case 1 'Campo de formulario
                            If dsCampos.Tables(0).Select("CAMPO = " & oCond.Item("CAMPO")).Length > 0 Then
                                drCampo = dsCampos.Tables(0).Select("CAMPO = " & oCond.Item("CAMPO"))(0)

                                Select Case drCampo.Item("SUBTIPO")
                                    Case 2
                                        oValorCampo = CDec(DBNullToSomething(drCampo.Item("VALOR_NUM")))
                                    Case 3
                                        oValorCampo = DBNullToSomething(drCampo.Item("VALOR_FEC"))
                                    Case 4
                                        oValorCampo = DBNullToSomething(drCampo.Item("VALOR_BOOL"))
                                    Case Else
                                        oValorCampo = DBNullToStr(drCampo.Item("VALOR_TEXT"))

                                        If oValorCampo <> "" Then
                                            Dim sPresup() As String
                                            Dim lIdPres As Integer
                                            Dim dsPres As DataSet
                                            Dim drPres As DataRow

                                            dsPres = oInstancia.cargarCampo(lCiaComp, oCond.Item("CAMPO"), True)
                                            If dsPres.Tables(0).Select("CCID = " & oCond.Item("CAMPO")).Length > 0 Then
                                                drPres = dsPres.Tables(0).Select("CCID = " & oCond.Item("CAMPO"))(0)

                                                If DBNullToSomething(drPres.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.PRES1 Then
                                                    sPresup = Split(drCampo.Item("VALOR_TEXT"), "_")
                                                    lIdPres = CInt(sPresup(1))

                                                    Dim oPres1 As PMPortalServer.PresProyectosNivel1 = FSPMServer.Get_PresProyectosNivel1
                                                    Select Case CInt(sPresup(0))
                                                        Case 1
                                                            oPres1.LoadData(lCiaComp, lIdPres)
                                                        Case 2
                                                            oPres1.LoadData(lCiaComp, Nothing, lIdPres)
                                                        Case 3
                                                            oPres1.LoadData(lCiaComp, Nothing, Nothing, lIdPres)
                                                        Case 4
                                                            oPres1.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPres)
                                                    End Select
                                                    oValorCampo = oPres1.Data.Tables(0).Rows(0).Item("ANYO") + " - " + oPres1.Data.Tables(0).Rows(0).Item("PRES" & sPresup(0))
                                                    oPres1 = Nothing
                                                ElseIf DBNullToSomething(drPres.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Pres2 Then
                                                    sPresup = Split(drCampo.Item("VALOR_TEXT"), "_")
                                                    lIdPres = CInt(sPresup(1))

                                                    Dim oPres2 As PMPortalServer.PresContablesNivel1 = FSPMServer.Get_PresContablesNivel1
                                                    Select Case CInt(sPresup(0))
                                                        Case 1
                                                            oPres2.LoadData(lCiaComp, lIdPres)
                                                        Case 2
                                                            oPres2.LoadData(lCiaComp, Nothing, lIdPres)
                                                        Case 3
                                                            oPres2.LoadData(lCiaComp, Nothing, Nothing, lIdPres)
                                                        Case 4
                                                            oPres2.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPres)
                                                    End Select
                                                    oValorCampo = oPres2.Data.Tables(0).Rows(0).Item("ANYO") + " - " + oPres2.Data.Tables(0).Rows(0).Item("PRES" & sPresup(0))
                                                    oPres2 = Nothing
                                                ElseIf DBNullToSomething(drPres.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Pres3 Then
                                                    sPresup = Split(drCampo.Item("VALOR_TEXT"), "_")
                                                    lIdPres = CInt(sPresup(1))

                                                    Dim oPres3 As PMPortalServer.PresConceptos3Nivel1 = FSPMServer.Get_PresConceptos3Nivel1
                                                    Select Case CInt(sPresup(0))
                                                        Case 1
                                                            oPres3.LoadData(lCiaComp, lIdPres)
                                                        Case 2
                                                            oPres3.LoadData(lCiaComp, Nothing, lIdPres)
                                                        Case 3
                                                            oPres3.LoadData(lCiaComp, Nothing, Nothing, lIdPres)
                                                        Case 4
                                                            oPres3.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPres)
                                                    End Select
                                                    oValorCampo = oPres3.Data.Tables(0).Rows(0).Item("PRES" & sPresup(0))
                                                    oPres3 = Nothing
                                                ElseIf DBNullToSomething(drPres.Item("TIPO_CAMPO_GS")) = TiposDeDatos.TipoCampoGS.Pres4 Then
                                                    sPresup = Split(drCampo.Item("VALOR_TEXT"), "_")
                                                    lIdPres = CInt(sPresup(1))

                                                    Dim oPres4 As PMPortalServer.PresConceptos4Nivel1 = FSPMServer.Get_PresConceptos4Nivel1
                                                    Select Case CInt(sPresup(0))
                                                        Case 1
                                                            oPres4.LoadData(lCiaComp, lIdPres)
                                                        Case 2
                                                            oPres4.LoadData(lCiaComp, Nothing, lIdPres)
                                                        Case 3
                                                            oPres4.LoadData(lCiaComp, Nothing, Nothing, lIdPres)
                                                        Case 4
                                                            oPres4.LoadData(lCiaComp, Nothing, Nothing, Nothing, lIdPres)
                                                    End Select
                                                    oValorCampo = oPres4.Data.Tables(0).Rows(0).Item("PRES" & sPresup(0))
                                                    oPres4 = Nothing
                                                End If
                                            End If
                                        End If
                                End Select
                                iTipo = drCampo.Item("SUBTIPO")
                                If (sMoneda <> "" Or oCond.Item("TIPO_VALOR") = 6 Or oCond.Item("TIPO_VALOR") = 7) Then
                                    If dCambioInstancia <> 0 Then
                                        oValorCampo = CDec(oValorCampo / dCambioInstancia)
                                    End If
                                End If
                            End If
                        Case 2 'Peticionario
                            oValorCampo = oInstancia.Peticionario
                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio
                        Case 3 'Departamento del peticionario
                            Dim oPer As PMPortalServer.Persona = FSPMServer.Get_Persona
                            oPer.LoadData(lCiaComp, oInstancia.Peticionario)
                            oValorCampo = oPer.Departamento
                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio
                        Case 4 'UON del peticionario
                            Dim oPer As PMPortalServer.Persona = FSPMServer.Get_Persona
                            oPer.LoadData(lCiaComp, oInstancia.Peticionario)
                            oValorCampo = oPer.UONs
                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoTextoMedio
                        Case 5 'N� de procesos de compra abiertos
                            If oInstancia.ID = 0 Then
                                oValorCampo = 0
                            Else
                                oInstancia.DevolverProcesosRelacionados()
                                oValorCampo = oInstancia.Procesos.Tables(0).Rows.Count
                            End If
                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico
                        Case 6 'Importe adj (LO DEVUELVE EN MONEDA CENTRAL...Y SE COMPARA CON MONEDA DEL FORMULARIO --->PENDIENTE DE CAMBIAR)
                            If oInstancia.ID = 0 Then
                                oValorCampo = 0
                            Else
                                oValorCampo = oInstancia.ObtenerImporteAdjudicado
                            End If
                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico
                        Case 7 'Importe de la solicitud de compra
                            oValorCampo = oInstancia.Importe

                            If sMoneda <> "" Then
                                oValorCampo = CDec(oValorCampo / dCambioInstancia)
                            End If
                            iTipo = PMPortalServer.TiposDeDatos.TipoGeneral.TipoNumerico
                        Case 10 'ImporteFactura
                            'Portal no guarda Facturas usando esta pantalla. 
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                        Case 11 'ImporteCostesPlanificados
                            'Portal no guarda Facturas usando esta pantalla. 
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                        Case 12 'ImporteCostesNoPlanificados
                            'Portal no guarda Facturas usando esta pantalla. 
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                        Case 13 'ImporteDctosPlanificados
                            'Portal no guarda Facturas usando esta pantalla. 
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                        Case 14 'ImporteDctosNoPlanificados
                            'Portal no guarda Facturas usando esta pantalla. 
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                        Case 15 'NumDiscrepanciasAbiertas
                            'Portal no guarda Facturas usando esta pantalla. 
                            'Lo unico q se hace en portal con facturas esta en script\facturas\DetalleFactura.aspx.vb
                    End Select

                    'Luego <EXPRESION DERECHA>
                    Select Case oCond.Item("TIPO_VALOR")
                        Case 1 'Campo de formulario
                            If dsCampos.Tables(0).Select("CAMPO = " & oCond.Item("CAMPO_VALOR")).Length > 0 Then
                                drCampo = dsCampos.Tables(0).Select("CAMPO = " & oCond.Item("CAMPO_VALOR"))(0)

                                Select Case drCampo.Item("SUBTIPO")
                                    Case 2
                                        oValor = CDec(DBNullToSomething(drCampo.Item("VALOR_NUM")))
                                    Case 3
                                        oValor = DBNullToSomething(drCampo.Item("VALOR_FEC"))
                                    Case 4
                                        oValor = DBNullToSomething(drCampo.Item("VALOR_BOOL"))
                                    Case Else
                                        oValor = DBNullToSomething(drCampo.Item("VALOR_TEXT"))
                                End Select
                                If (sMoneda <> "" Or oCond.Item("TIPO_CAMPO") = 6 Or oCond.Item("TIPO_CAMPO") = 7) Then
                                    If dCambioInstancia <> 0 Then
                                        oValor = CDec(oValor / dCambioInstancia)
                                    End If
                                End If
                            End If
                        Case 2 'Peticionario
                            oValorCampo = oInstancia.Peticionario
                        Case 3 'Departamento del peticionario
                            Dim oPer As PMPortalServer.Persona = FSPMServer.Get_Persona
                            oPer.LoadData(lCiaComp, oInstancia.Peticionario)
                            oValorCampo = oPer.Departamento
                        Case 4 'UON del peticionario
                            Dim oPer As PMPortalServer.Persona = FSPMServer.Get_Persona
                            oPer.LoadData(lCiaComp, oInstancia.Peticionario)
                            oValorCampo = oPer.UONs
                        Case 5 'N� de procesos de compra abiertos
                            If oInstancia.ID = 0 Then
                                oValorCampo = 0
                            Else
                                oInstancia.DevolverProcesosRelacionados()
                                oValorCampo = oInstancia.Procesos.Tables(0).Rows.Count
                            End If
                        Case 6 'Importe adj (LO DEVUELVE EN MONEDA CENTRAL...Y SE COMPARA CON MONEDA DEL FORMULARIO --->PENDIENTE DE CAMBIAR)
                            If oInstancia.ID = 0 Then
                                oValorCampo = 0
                            Else
                                oValorCampo = oInstancia.ObtenerImporteAdjudicado
                            End If
                        Case 7 'Importe de la solicitud de compra
                            oValorCampo = oInstancia.Importe

                            If dCambioInstancia <> 0 Then
                                oValorCampo = CDec(oValorCampo / dCambioInstancia)
                            End If
                        Case 10 'valor est�tico
                            Select Case iTipo
                                Case 2
                                    oValor = CDec(DBNullToSomething(oCond.Item("VALOR_NUM")))
                                Case 3
                                    oValor = DBNullToSomething(oCond.Item("VALOR_FEC"))
                                Case 4
                                    oValor = DBNullToSomething(oCond.Item("VALOR_BOOL"))
                                Case Else
                                    If oCond.Item("TIPO_CAMPO") = 4 Then 'UON DEL PETICIONARIO
                                        Dim oUON() As String
                                        Dim iIndice As Integer
                                        oValor = Nothing
                                        oUON = Split(oCond.Item("VALOR_TEXT"), "-")
                                        For iIndice = 0 To UBound(oUON)
                                            If oUON(iIndice) <> "" Then
                                                oValor = oValor & Trim(oUON(iIndice)) & "-"
                                            End If
                                        Next iIndice
                                        oValor = Left(oValor, Len(oValor) - 1)
                                    Else
                                        oValor = DBNullToStr(oCond.Item("VALOR_TEXT"))
                                    End If
                            End Select
                            If sMoneda <> "" And dCambioEnlace <> 0 Then
                                oValor = CDec(oValor / dCambioEnlace)
                            ElseIf (oCond.Item("TIPO_CAMPO") = 6 Or oCond.Item("TIPO_CAMPO") = 7) And dCambioInstancia <> 0 Then
                                oValor = CDec(oValor / dCambioInstancia)
                            End If
                    End Select

                    'y por �ltimo con el OPERADOR obtenemos el valor
                    Select Case iTipo
                        Case 2, 3
                            If oValorCampo Is Nothing Then 'Condici�n vac�a
                                If oValor Is Nothing Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Else
                                Select Case oCond.Item("OPERADOR")
                                    Case ">"
                                        If oValorCampo > oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case "<"
                                        If oValorCampo < oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case ">="
                                        If oValorCampo >= oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case "<="
                                        If oValorCampo <= oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case "="
                                        If oValorCampo = oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case "<>"
                                        If oValorCampo <> oValor Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                End Select
                            End If
                        Case 4
                            If oValorCampo Is Nothing Then 'Condici�n vac�a
                                If oValor Is Nothing Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Else
                                If oValorCampo = oValor Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            End If
                        Case Else
                            If oValorCampo Is Nothing Then 'Condici�n vac�a
                                If oValor Is Nothing Then
                                    dValues(i) = 1
                                Else
                                    dValues(i) = 0
                                End If
                            Else
                                Select Case UCase(oCond.Item("OPERADOR"))
                                    Case "="
                                        If UCase(oValorCampo) = UCase(oValor) Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                    Case "LIKE"
                                        If Left(oValor, 1) = "*" Then
                                            If Right(oValor, 1) = "*" Then
                                                oValor = oValor.ToString.Replace("*", "")
                                                If InStr(oValorCampo.ToString, oValor.ToString) >= 0 Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            Else
                                                oValor = oValor.ToString.Replace("*", "")
                                                If oValorCampo.ToString.EndsWith(oValor.ToString) Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            End If
                                        Else
                                            If Right(oValor, 1) = "*" Then
                                                oValor = oValor.ToString.Replace("*", "")
                                                If oValorCampo.ToString.StartsWith(oValor.ToString) Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            Else
                                                If UCase(oValorCampo.ToString) = UCase(oValor.ToString) Then
                                                    dValues(i) = 1
                                                Else
                                                    dValues(i) = 0
                                                End If
                                            End If
                                        End If
                                    Case "<>"
                                        If UCase(oValorCampo.ToString) <> UCase(oValor.ToString) Then
                                            dValues(i) = 1
                                        Else
                                            dValues(i) = 0
                                        End If
                                End Select
                            End If
                    End Select
                    i += 1
                Next
                oValor = Nothing
                Try
                    iEq.Parse(sFormula, sVariables)
                    oValor = iEq.Evaluate(dValues)
                Catch ex As USPExpress.ParseException
                Catch ex0 As Exception
                End Try
            Catch e As Exception
                Dim a As String = e.Message
            End Try
            Return (oValor > 0)
        End Function
        ''' <summary>
        ''' Crea una lista de id de adjuntos en forma de string #DIdDesgl##IdAdj##IdAdj##
        ''' </summary>
        ''' <param name="Solicitud">Solicitud</param>
        ''' <param name="CampoPadre">Id del desglose</param>
        ''' <returns>string #DIdDesgl##IdAdj##IdAdj##</returns>
        ''' <remarks>Llamada desde:GenerarDataSet; Tiempo m�ximo:0,1</remarks>
        Private Function CreaListaAdjuntosDefecto(ByVal Solicitud As Long, ByVal CampoPadre As Long) As String
            Dim lCiaComp As Long = IdCiaComp
            Dim Indice As Integer

            Try
                CreaListaAdjuntosDefecto = "#D" & CStr(CampoPadre) & "##"

                If Strings.InStr(ListaDesgloseDefecto, CreaListaAdjuntosDefecto, CompareMethod.Text) <> 0 Then

                    For i As Integer = 0 To UBound(ListaAdjuntosDefecto)
                        If Strings.InStr(ListaAdjuntosDefecto(i), CreaListaAdjuntosDefecto, CompareMethod.Text) <> 0 Then
                            Return ListaAdjuntosDefecto(i)
                        End If
                    Next
                Else
                    ListaDesgloseDefecto = ListaDesgloseDefecto & CreaListaAdjuntosDefecto
                    Indice = UBound(ListaAdjuntosDefecto) + 1
                    ReDim Preserve ListaAdjuntosDefecto(Indice)
                End If

                Dim ds As DataSet = FSPMServer.Load_AdjuntosDefecto(lCiaComp, Solicitud, CampoPadre)

                For Each row As DataRow In ds.Tables(0).Rows
                    'Crea string #DIdDesgl##IdAdj##IdAdj##
                    CreaListaAdjuntosDefecto = CreaListaAdjuntosDefecto & row(0) & "##"
                Next

                ListaAdjuntosDefecto(Indice) = CreaListaAdjuntosDefecto

                Return CreaListaAdjuntosDefecto
            Catch ex As Exception
                CreaListaAdjuntosDefecto = "#D" & CStr(CampoPadre) & "##"
            End Try
        End Function
        '***************************************************************************************************
        '*** HHF 25/03/2009
        '*** Descripci�n: Funci�n que crea la primera versi�n del xml con los datos recogidos del formulario
        '*** Par�metros de salida: No tiene
        '*** Llamada desde: la funci�n GuardarSinWorkflow y GuardarConWorkflow de guardarinstancia.aspx.vb
        '*** Tiempo m�ximo: 0 sec            
        '**************************************************************************************************
        Private Sub GuardarWorkflow_GenerarXML()
            Try
                'quitamos los car�cteres no v�lidos
                Dim sXMLName As String = ValidFilename(FSPMUser.CodCia & "#" & FSPMUser.Cod & "#" & lInstancia & "#" & lBloque)

                'le ponemos la P para saber que es de portal
                sXMLName = sXMLName & "#PORTAL.xml"
                ds.WriteXml(ConfigurationManager.AppSettings("rutaXMLPendiente") & "\" & sXMLName, XmlWriteMode.WriteSchema)
            Catch ex As Exception
                Dim oFileW As System.IO.StreamWriter
                oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & "\PM_P_log_" & FSPMUser.Cod & ".txt", True)
                oFileW.WriteLine()
                oFileW.WriteLine("Error GuardarWorkflow_GenerarXML: " & ex.Message & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
                oFileW.Close()
            End Try
        End Sub
        '***************************************************************************************************
        '*** ILG 20/05/2016
        '*** Descripci�n: Funci�n que crea la primera versi�n del xml con los datos recogidos del formulario
        '*** Par�metros de salida: No tiene
        '*** Llamada desde: la funci�n GuardarSinWorkflow y GuardarConWorkflow de guardarinstancia.aspx.vb
        '*** Tiempo m�ximo: 0 sec            
        '***************************************************************************************************
        Private Sub GuardarQA_Thread()
            Dim sPer As String = FSPMUser.Cod
            Dim sXMLName As String
            Dim intFileId As Long = 0
            Dim lCiaComp As Long = IdCiaComp

            Try
                sXMLName = ValidFilename(FSPMUser.CodCia & "#" & FSPMUser.Cod & "#" & lInstancia & "#0")

                Dim lIDTiempoProc As Long
                oInstancia.ActualizarTiempoProcesamiento(lCiaComp, lIDTiempoProc, FSPMUser.CodCia)

                Dim sProveGS As String = FSPMUser.ObtenerProv(lCiaComp)
                If ds.Tables.Contains("SOLICITUD") Then Exit Sub
                ds.Tables.Add("SOLICITUD")
                With ds.Tables("SOLICITUD").Columns
                    .Add("TIPO_PROCESAMIENTO_XML")
                    .Add("TIPO_DE_SOLICITUD")
                    .Add("COMPLETO")
                    .Add("SOLICITUD")
                    .Add("INSTANCIA")
                    .Add("CERTIFICADO")
                    .Add("NOCONFORMIDAD")
                    .Add("CIACOMP")
                    .Add("PROVE")
                    .Add("COD")
                    .Add("CODCIA")
                    .Add("IDCIA")
                    .Add("IDUSU")
                    .Add("ACCION")
                    .Add("NOMBRE")
                    .Add("USUARIO_EMAIL")
                    .Add("USUARIO_IDIOMA")
                    .Add("PROVEGS")
                    .Add("NOTIFICAR")
                    .Add("IDTIEMPOPROC")
                End With
                Dim drSolicitud As DataRow
                drSolicitud = ds.Tables("SOLICITUD").NewRow
                With drSolicitud
                    .Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.Portal)
                    .Item("TIPO_DE_SOLICITUD") = CInt(oInstancia.Solicitud.TipoSolicit)
                    .Item("COMPLETO") = 1
                    .Item("SOLICITUD") = lSolicitud
                    .Item("INSTANCIA") = lInstancia
                    .Item("CERTIFICADO") = lCertificado
                    .Item("NOCONFORMIDAD") = lNoConformidad
                    .Item("CIACOMP") = lCiaComp
                    .Item("PROVE") = FSPMUser.CodProve
                    .Item("COD") = FSPMUser.Cod
                    .Item("CODCIA") = FSPMUser.CodCia
                    .Item("IDCIA") = FSPMUser.IdCia
                    .Item("IDUSU") = FSPMUser.IdUsu
                    .Item("ACCION") = sAccion
                    .Item("NOMBRE") = FSPMUser.Nombre & " " & FSPMUser.Apellidos
                    .Item("USUARIO_EMAIL") = FSPMUser.Email
                    .Item("USUARIO_IDIOMA") = FSPMUser.Idioma.ToString
                    .Item("PROVEGS") = sProveGS
                    .Item("NOTIFICAR") = IIf(Notificar.Value, 1, 0)
                    .Item("IDTIEMPOPROC") = lIDTiempoProc
                End With
                ds.Tables("SOLICITUD").Rows.Add(drSolicitud)

                oInstancia.ActualizarTiempoProcesamiento(lCiaComp, lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
                If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
                    'Se hace con un StringWriter porque el m�todo GetXml del dataset no incluye el esquema
                    Dim oSW As New System.IO.StringWriter()
                    ds.WriteXml(oSW, XmlWriteMode.WriteSchema)

                    Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
                    oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), sXMLName)
                Else
                    'le ponemos la P para saber que es de portal
                    ds.WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & "#PORTAL.xml", XmlWriteMode.WriteSchema)
                    If File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml") Then _
                        File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml")
                    FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & "#PORTAL.xml",
                                      ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml")
                End If
            Catch ex As Exception
                Dim oFileW As System.IO.StreamWriter
                oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & "\QA_P_log_" & sPer & ".txt", True)
                oFileW.WriteLine()
                oFileW.WriteLine("Error GuardarQA_Thread: " & ex.Message & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
                oFileW.Close()
            End Try
        End Sub
        ''' <summary>
        ''' Realizar el calculo de campos calculados
        ''' </summary>
        ''' <param name="ds">dataset con tablas cmpos y desglose</param>
        ''' <param name="sIdi">Idioma</param>
        ''' <param name="ListaCalc">lista campos calculados</param>
        ''' <param name="lVersion">Version de la instancia</param>
        ''' <param name="oDSPares">Traduccion de ID de campos si estas en guardados sin enviar. A partir de IDCAMPOVERSION</param>
        ''' <param name="oDSParesReverse">Traduccion de ID de campos si estas en guardados sin enviar. A partir de IDCAMPOACTUAL</param>
        ''' <remarks>Llamada desde: GuardarSinWorkFlow; Tiempo maximo:0,2 sg</remarks>
        Private Sub CompletarDataSetCamposCalculados(ByRef ds As DataSet, ByVal sIdi As String, ByRef ListaCalc As String,
                    ByVal lVersion As Integer, ByVal oDSPares As DataSet, ByVal oDSParesReverse As DataSet)
            '********************************************************************************
            'RECALCULAMOS LOS CAMPOS CALCULADOS POR SI LAS FLY
            Dim lCiaComp As Long = IdCiaComp
            Dim oDS As DataSet
            Dim oDSDesglose As DataSet
            Dim bCalcular As Boolean
            Dim dTotal As Double
            Dim oDSRow As DataRow
            Dim oRow As DataRow
            Dim dt As DataTable
            Dim oDSRowDesglose As DataRow
            Dim oDSRowDesgloseOculto As DataRow
            Dim sPre As String
            Dim sPreCalc As String
            Dim sRequest As String
            Dim iTipoGS As TiposDeDatos.TipoCampoGS
            Dim iTipo As TiposDeDatos.TipoGeneral
            Dim iTipoGSDesglose As TiposDeDatos.TipoCampoGS
            Dim iTipoDesglose As TiposDeDatos.TipoGeneral
            Dim vValor As Object
            Dim oCampo As PMPortalServer.Campo
            Dim k As Integer
            Dim iLinea As Integer
            dt = ds.Tables("TEMP")

            If lVersion = 0 Then
                oDS = oSolicitud.Formulario.LoadCamposCalculados(lCiaComp, oSolicitud.ID, FSPMUser.Idioma)
            Else
                oDS = oInstancia.LoadCamposCalculados(lCiaComp, FSPMUser.CodProveGS, False)

                For Each row As DataRow In oDS.Tables(0).Rows
                    row.Item("ID_CAMPO") = DevolverIdCampoActual(oDSPares, row.Item("ID_CAMPO"))
                Next
            End If

            Dim findDesglose(2) As Object
            Dim find(0) As Object
            Dim oRowDesglose As DataRow
            Dim keysDesglose(2) As DataColumn
            Dim oDTDesglose As DataTable
            Dim sVariables() As String
            Dim sVariablesDesglose() As String
            Dim dValues() As Double
            Dim dValuesDesglose() As Double
            Dim dValuesTotalDesglose() As Double
            Dim i As Integer
            Dim iDesglose As Integer

            oDTDesglose = ds.Tables("TEMPDESGLOSE")

            ReDim sVariables(oDS.Tables(0).Rows.Count - 1)
            ReDim dValues(oDS.Tables(0).Rows.Count - 1)

            Dim Eliminados As Integer
            Eliminados = 0
            Dim iEq As New USPExpress.USPExpression

            i = 0
            Dim oRowWorkflow As DataRow
            Dim noinsertar As Integer
            Dim bEstaInvisibleElDesglose As Boolean
            Dim oDSDesglosePadreVisible As DataSet

            noinsertar = 0
            For Each oDSRow In oDS.Tables(0).Rows
                bEstaInvisibleElDesglose = False

                sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())

                sVariables(i) = oDSRow.Item("ID_CALCULO")

                iTipoGS = DBNullToSomething(oDSRow.Item("TIPO_CAMPO_GS"))
                iTipo = oDSRow.Item("SUBTIPO")
                If lVersion = 0 Then
                    sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID").ToString()
                Else
                    sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO").ToString()
                End If
                If oDSRow.Item("VISIBLE") = 1 Then
                    If (Not lVersion = 0) AndAlso Request(sRequest) Is Nothing Then
                        sRequest = "CALC_" + sPre & "_fsentry" & DevolverIdCampoActualReverse(oDSParesReverse, oDSRow.Item("ID_CAMPO")).ToString()
                    End If

                    vValor = Numero(VacioToNothing(Request(sRequest)), ".", ",")
                Else
                    vValor = DBNullToSomething(oDSRow.Item("VALOR_NUM"))
                End If

                If oDSRow.Item("TIPO") = PMPortalServer.TipoCampoPredefinido.Calculado Then
                    If Not IsDBNull(oDSRow.Item("ORIGEN_CALC_DESGLOSE")) Then
                        oCampo = FSPMServer.Get_Campo
                        If lVersion = 0 Then
                            oCampo.Id = oDSRow.Item("ORIGEN_CALC_DESGLOSE")
                            oDSDesglose = oCampo.LoadCalculados(lCiaComp, oSolicitud.ID)
                            oDSDesglosePadreVisible = oSolicitud.Formulario.LoadDesglosePadreVisible(lCiaComp, oSolicitud.ID, oCampo.Id, Nothing, FSPMUser.CodProveGS, FSPMUser.IdContacto)
                        Else
                            oCampo.Id = DBNullToInt(oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE"))
                            oDSDesglose = oCampo.LoadInstCalculados(lCiaComp, lInstancia, FSPMUser.CodProve)

                            oSolicitud = FSPMServer.Get_Solicitud
                            oSolicitud.ID = oInstancia.Solicitud.ID
                            oSolicitud.Load(lCiaComp, sIdi)

                            Dim oDSAux As DataSet = oSolicitud.Formulario.LoadCamposCalculados(lCiaComp, oSolicitud.ID, FSPMUser.Idioma)
                            For Each oDSRowAux As DataRow In oDSAux.Tables(0).Rows
                                If oDSRow.Item("ID") = oDSRowAux.Item("ID") Then
                                    oDSDesglosePadreVisible = oSolicitud.Formulario.LoadDesglosePadreVisible(lCiaComp, oSolicitud.ID, oDSRowAux.Item("ORIGEN_CALC_DESGLOSE"))
                                    Exit For
                                End If
                            Next
                        End If

                        ReDim sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
                        ReDim dValuesDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
                        ReDim dValuesTotalDesglose(oDSDesglose.Tables(0).Rows.Count - 1)
                        bCalcular = False

                        Dim dTotalDesglose As Double = 0
                        Dim iNumRows As Integer = 0
                        Dim bPopUp As Boolean
                        Dim bDesgloseEmergente As Boolean

                        'Compruebo si el campo desglose es visible                    
                        bEstaInvisibleElDesglose = True
                        If oDSDesglosePadreVisible.Tables(0).Rows.Count > 0 Then
                            For Each oDSRowDesglosePadreVisible In oDSDesglosePadreVisible.Tables(0).Rows
                                If oDSRowDesglosePadreVisible.Item("VISIBLE") = 1 Then
                                    bEstaInvisibleElDesglose = False
                                    Exit For
                                End If
                            Next
                        End If

                        'Miro como si fuera visible
                        'si es visible y tiene lineas me dara el numero de lineas
                        If bEstaInvisibleElDesglose = False Then
                            sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString())
                            iNumRows = Request("CALC_" + sPre + "_tblDesglose") 'Filas de desglose que salen en pesta�a
                            bPopUp = False
                            If iNumRows = Nothing Then
                                'El desglose es de tipo popup
                                If lVersion = 0 Then
                                    iNumRows = Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "_tblDesglose")
                                    bDesgloseEmergente = False
                                    If iNumRows = Nothing Then
                                        bDesgloseEmergente = True
                                        iNumRows = Request("CALC_" + sPre + "_fsentry" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "__numTotRows")
                                    End If
                                Else
                                    'Desglose no Emergente "CALC_uwtGrupos__ctl0xx_5491_5491_133510_tblDesglose"
                                    iNumRows = Request("CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_tblDesglose")
                                    bDesgloseEmergente = False
                                    If iNumRows = Nothing Then
                                        bDesgloseEmergente = True
                                        iNumRows = Request("CALC_" + sPre + "_fsentry" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "__numTotRows")
                                    End If
                                End If
                                bPopUp = True
                            End If
                        Else
                            'si el numero de lineas es 0 miro para ver si es pq el desglose esta oculto
                            If oDSDesglose.Tables(1).Rows.Count > 0 Then
                                For Each oDSRowDesgloseOculto In oDSDesglose.Tables(1).Rows
                                    If oDSRowDesgloseOculto.Item("Linea") > iNumRows Then
                                        iNumRows = oDSRowDesgloseOculto.Item("Linea")
                                        bEstaInvisibleElDesglose = True
                                    End If
                                Next
                            End If
                        End If

                        Dim NumEliminadas As Integer
                        NumEliminadas = 0
                        Dim EliminadoporNothing As Boolean
                        EliminadoporNothing = False
                        Dim MirarEliminadoporNothing As Boolean

                        Dim distinctCounts As System.Collections.Generic.IEnumerable(Of Int32) = Nothing
                        Dim iMaxIndex As Integer = 0
                        If oDSDesglose.Tables(1).Rows.Count > 0 Then
                            iMaxIndex = oDSDesglose.Tables(1).Select("", "LINEA DESC")(0).Item("LINEA")
                            distinctCounts = From row In oDSDesglose.Tables(1)
                                             Select row.Field(Of Int32)("LINEA")
                                             Distinct
                        End If
                        For k = 1 To iMaxIndex
                            If distinctCounts.Contains(k) Then
                                EliminadoporNothing = False
                                MirarEliminadoporNothing = True

                                iDesglose = 0
                                For Each oDSRowDesglose In oDSDesglose.Tables(0).Rows
                                    sVariablesDesglose(iDesglose) = oDSRowDesglose.Item("ID_CALCULO")
                                    bCalcular = True

                                    iTipoGSDesglose = DBNullToSomething(oDSRowDesglose.Item("TIPO_CAMPO_GS"))
                                    iTipoDesglose = oDSRow.Item("SUBTIPO")

                                    If oDSRowDesglose.Item("VISIBLE") = 1 And Not bEstaInvisibleElDesglose Then
                                        'Si es visible y el desglose esta visible
                                        If lVersion = 0 Then
                                            If bPopUp Then
                                                If bDesgloseEmergente Then
                                                    sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ORIGEN_CALC_DESGLOSE") & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID")
                                                Else
                                                    'CALC_uwtGrupos__ctl0xx_623_623_10549_fsdsentry_1_10551
                                                    sRequest = "CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID").ToString
                                                End If
                                            Else
                                                'CALC_uwtGrupos__ctl0x_146_fsdsentry_1_2283
                                                sRequest = "CALC_" + sPre + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID").ToString()
                                            End If
                                        Else
                                            If bPopUp Then
                                                If bDesgloseEmergente Then
                                                    sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE") & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID_CAMPO")

                                                Else
                                                    'CALC_uwtGrupos__ctl0xx_5491_5491_133510_fsdsentry_1_133486
                                                    sRequest = "CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID_CAMPO").ToString
                                                End If
                                            Else
                                                'CALC_uwtGrupos__ctl0x_146_fsdsentry_1_2283
                                                sRequest = "CALC_" + sPre + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID_CAMPO").ToString()
                                            End If
                                        End If
                                        vValor = Numero(VacioToNothing(Request(sRequest)), ".", ",")

                                        If MirarEliminadoporNothing Then
                                            MirarEliminadoporNothing = False
                                            If Request(sRequest) Is Nothing Then
                                                EliminadoporNothing = True
                                            End If
                                        End If
                                    Else

                                        keysDesglose(0) = oDSDesglose.Tables(1).Columns("LINEA")
                                        keysDesglose(1) = oDSDesglose.Tables(1).Columns("CAMPO_PADRE")
                                        keysDesglose(2) = oDSDesglose.Tables(1).Columns("CAMPO_HIJO")

                                        oDSDesglose.Tables(1).PrimaryKey = keysDesglose
                                        iLinea = k
                                        findDesglose(0) = (k).ToString()
                                        If lVersion = 0 Then
                                            findDesglose(1) = oDSRow.Item("ORIGEN_CALC_DESGLOSE")
                                            findDesglose(2) = oDSRowDesglose.Item("ID").ToString()
                                        Else
                                            findDesglose(1) = oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE")
                                            findDesglose(2) = oDSRowDesglose.Item("ID_CAMPO").ToString()
                                        End If
                                        oRowDesglose = oDSDesglose.Tables(1).Rows.Find(findDesglose)
                                        While oRowDesglose Is Nothing And iLinea > 0
                                            iLinea -= 1
                                            findDesglose(0) = iLinea

                                            oRowDesglose = oDSDesglose.Tables(1).Rows.Find(findDesglose)
                                        End While
                                        If Not oRowDesglose Is Nothing Then
                                            vValor = DBNullToSomething(oRowDesglose.Item("VALOR_NUM"))
                                        End If

                                    End If

                                    If oDSRowDesglose.Item("TIPO") = TipoCampoPredefinido.Calculado Then
                                        dValuesDesglose(iDesglose) = 0
                                    Else
                                        dValuesDesglose(iDesglose) = modUtilidades.Numero(vValor)
                                    End If
                                    dValuesTotalDesglose(iDesglose) += dValuesDesglose(iDesglose)
                                    iDesglose += 1

                                Next
                                iDesglose = 0
                                For Each oDSRowDesglose In oDSDesglose.Tables(0).Rows
                                    If oDSRowDesglose.Item("TIPO") = TipoCampoPredefinido.Calculado Then
                                        Try
                                            If Not EliminadoporNothing Then
                                                iEq.Parse(oDSRowDesglose.Item("FORMULA"), sVariablesDesglose)
                                                dValuesDesglose(iDesglose) = iEq.Evaluate(dValuesDesglose)
                                                iLinea = k
                                                findDesglose(0) = (k - NumEliminadas).ToString()
                                                If lVersion = 0 Then
                                                    findDesglose(1) = oDSRow.Item("ORIGEN_CALC_DESGLOSE")
                                                    findDesglose(2) = oDSRowDesglose.Item("ID").ToString()
                                                Else
                                                    findDesglose(1) = oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE")
                                                    findDesglose(2) = oDSRowDesglose.Item("ID_CAMPO").ToString()
                                                End If
                                                oRowDesglose = Nothing
                                                oRowDesglose = oDTDesglose.Rows.Find(findDesglose)

                                                If Not oRowDesglose Is Nothing Then
                                                    oRowDesglose.Item("VALOR_NUM") = dValuesDesglose(iDesglose)
                                                    dValuesTotalDesglose(iDesglose) += dValuesDesglose(iDesglose)

                                                    ''''
                                                    If lVersion = 0 Then
                                                        If bPopUp Then
                                                            If bDesgloseEmergente Then
                                                                sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ORIGEN_CALC_DESGLOSE") & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID")
                                                            Else
                                                                'CALC_uwtGrupos__ctl0xx_623_623_10549_fsdsentry_1_10551
                                                                sRequest = "CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID").ToString & "__t_t"
                                                            End If
                                                        Else
                                                            'CALC_uwtGrupos__ctl0x_146_fsdsentry_1_2283
                                                            sRequest = "CALC_" + sPre + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID").ToString() & "__t_t"
                                                        End If
                                                    Else
                                                        If bPopUp Then
                                                            If bDesgloseEmergente Then
                                                                sRequest = "CALC_" + sPre & "_fsentry" & oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE") & "__" & (k).ToString() & "__" & oDSRowDesglose.Item("ID_CAMPO")
                                                            Else
                                                                'CALC_uwtGrupos__ctl0xx_5491_5491_133510_fsdsentry_1_133486
                                                                sRequest = "CALC_" + sPre + "_" + oDSRow.Item("GRUPO_ORIGEN_CALC_DESGLOSE").ToString + "_" + oDSRow.Item("ID_CAMPO_ORIGEN_CALC_DESGLOSE").ToString + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID_CAMPO").ToString & "__t_t"
                                                            End If
                                                        Else
                                                            'CALC_uwtGrupos__ctl0x_146_fsdsentry_1_2283
                                                            sRequest = "CALC_" + sPre + "_fsdsentry_" + (k).ToString() + "_" + oDSRowDesglose.Item("ID_CAMPO").ToString() & "__t_t"
                                                        End If
                                                    End If
                                                    If oDSRow.Item("VISIBLE") Then
                                                        ListaCalc = ListaCalc & Replace(sRequest, "CALC_", "") & "@" & CStr(oRowDesglose.Item("VALOR_NUM")) & "#"
                                                    End If
                                                Else
                                                    'En la tabla temporal
                                                    If oDSRow.Item("VISIBLE") = 0 Then
                                                        dValuesTotalDesglose(iDesglose) += dValuesDesglose(iDesglose)
                                                    End If
                                                End If
                                            Else
                                                NumEliminadas += 1
                                            End If
                                        Catch ex As USPExpress.ParseException
                                        Catch ex0 As Exception
                                        End Try
                                    End If
                                    iDesglose += 1
                                Next

                                ReDim Preserve sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count)
                                ReDim Preserve dValuesDesglose(oDSDesglose.Tables(0).Rows.Count)

                                Try
                                    sVariablesDesglose(oDSDesglose.Tables(0).Rows.Count) = oDSRow.Item("FORMULA") + " * 1"
                                    iEq.Parse(oDSRow.Item("FORMULA"), sVariablesDesglose)

                                    dValuesDesglose(oDSDesglose.Tables(0).Rows.Count) = iEq.Evaluate(dValuesDesglose)
                                    dTotalDesglose += dValuesDesglose(oDSDesglose.Tables(0).Rows.Count)
                                Catch ex As USPExpress.ParseException
                                    'Se captura y no hace nada para que siga con el calculo
                                Catch e As Exception
                                    'Se captura y no hace nada para que siga con el calculo
                                End Try
                            End If
                        Next

                        If Not IsDBNull(oDSRow.Item("FORMULA")) Then
                            Try
                                If bCalcular = True Then
                                    If oDSRow.Item("ES_SUBCAMPO") = 0 Then
                                        dTotalDesglose = iEq.Evaluate(dValuesTotalDesglose)
                                    End If

                                    dValues(i) = dTotalDesglose
                                End If

                                If lVersion = 0 Then
                                    find(0) = oDSRow.Item("ID")
                                Else
                                    find(0) = oDSRow.Item("ID_CAMPO")
                                End If

                                oRow = dt.Rows.Find(find)
                                If Not oRow Is Nothing Then
                                    oRow.Item("VALOR_NUM") = dValues(i)

                                    ''''
                                    sPreCalc = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())
                                    If lVersion = 0 Then
                                        sRequest = "CALC_" + sPreCalc & "_fsentry" & oDSRow.Item("ID").ToString() & "__t_t"
                                    Else
                                        sRequest = "CALC_" + sPreCalc & "_fsentry" & oDSRow.Item("ID_CAMPO").ToString() & "__t_t"
                                    End If
                                    If oDSRow.Item("VISIBLE") Then
                                        If (Not lVersion = 0) AndAlso Request(Replace(sRequest, "__t_t", "")) Is Nothing Then
                                            sRequest = "CALC_" + sPreCalc & "_fsentry" & DevolverIdCampoActualReverse(oDSParesReverse, oDSRow.Item("ID_CAMPO")).ToString() & "__t_t"

                                            If Request(Replace(sRequest, "__t_t", "")) Is Nothing Then
                                                ''Sin el __t_t es campo de pantalla, si no es guardado los id coinciden. Sino, se hace DevolverIdCampoActual. �Ambas sRequest estan mal? Pues me quedo el codigo original.
                                                ''Esto se podra quitar cuando los calc de desglose certif esten ok.
                                                sRequest = "CALC_" + sPreCalc & "_fsentry" & oDSRow.Item("ID_CAMPO").ToString() & "__t_t"
                                            End If
                                        End If
                                        ListaCalc = ListaCalc & Replace(sRequest, "CALC_", "") & "@" & CStr(oRow.Item("VALOR_NUM")) & "#"
                                    End If
                                End If
                            Catch ex As USPExpress.ParseException
                            Catch ex0 As Exception
                            End Try
                        End If
                    Else
                        dValues(i) = 0
                    End If
                Else
                    dValues(i) = modUtilidades.Numero(vValor)
                    If (oDSRow.Item("ES_IMPORTE_TOTAL") = 1 AndAlso oDSRow.Item("ES_SUBCAMPO") = 0) Then
                        dTotal = dValues(i)
                    End If
                End If
                i += 1
            Next
            i = 0

            For Each oDSRow In oDS.Tables(0).Rows
                sPre = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())
                If oDSRow.Item("TIPO") = PMPortalServer.TipoCampoPredefinido.Calculado Then
                    If lVersion = 0 Then
                        find(0) = oDSRow.Item("ID")
                    Else
                        find(0) = oDSRow.Item("ID_CAMPO")
                    End If

                    oRow = dt.Rows.Find(find)

                    Try
                        iEq.Parse(oDSRow.Item("FORMULA"), sVariables)
                        dValues(i) = iEq.Evaluate(dValues)
                        If Not oRow Is Nothing Then
                            oRow.Item("VALOR_NUM") = dValues(i)

                            ''''
                            sPreCalc = Request("CALC_txtPre_" + oDSRow.Item("GRUPO").ToString())
                            If lVersion = 0 Then
                                sRequest = "CALC_" + sPreCalc & "_fsentry" & oDSRow.Item("ID").ToString() & "__t_t"
                            Else
                                sRequest = "CALC_" + sPreCalc & "_fsentry" & oDSRow.Item("ID_CAMPO").ToString() & "__t_t"
                            End If
                            If oDSRow.Item("VISIBLE") Then
                                If (Not lVersion = 0) AndAlso Request(Replace(sRequest, "__t_t", "")) Is Nothing Then
                                    sRequest = "CALC_" + sPreCalc & "_fsentry" & DevolverIdCampoActualReverse(oDSParesReverse, oDSRow.Item("ID_CAMPO")).ToString() & "__t_t"
                                End If
                                ListaCalc = ListaCalc & Replace(sRequest, "CALC_", "") & "@" & CStr(oRow.Item("VALOR_NUM")) & "#"
                            End If
                        End If
                    Catch ex As USPExpress.ParseException
                    Catch ex0 As Exception
                    End Try

                    If (DBNullToSomething(oDSRow.Item("ES_IMPORTE_TOTAL")) = 1 AndAlso DBNullToSomething(oDSRow.Item("ES_SUBCAMPO")) = 0) Then
                        oRowWorkflow = oRow
                    End If
                End If
                i += 1
            Next

            If Not oRowWorkflow Is Nothing Then
                dTotal = DBNullToSomething(oRowWorkflow.Item("VALOR_NUM"))
            End If
        End Sub
        '''************************************************************************************************
        ''' Revisado por: Jbg. Fecha: 17/10/2011
        ''' Descripci�n: Recuperar el xml previamente creado en GuardarWorkflow_GenerarXML(), genera 
        ''' el dataset con los datos necesarios, dejar el nuevo xml en la carpeta y hace la llamada al 
        ''' webservice para que lo procese.
        ''' Par�metros de entrada: No tiene
        ''' Par�metros de salida: No tiene
        ''' Llamada desde: la funci�n GuardarConWorkflow de guardarinstancia.aspx.vb
        ''' Tiempo m�ximo: 0 sec            
        '''************************************************************************************************
        Private Sub GuardarConWorkflow_ProcesarXML()
            Dim sXMLName As String
            Dim oFileW As System.IO.StreamWriter
            Dim lCiaComp As Long = IdCiaComp
            Dim oXMLFolder As DirectoryInfo = New DirectoryInfo(ConfigurationManager.AppSettings("rutaXMLPendiente"))
            Try
                'B�squeda del XML a meter en el DataSet
                sXMLName = ValidFilename(FSPMUser.CodCia & "#" & FSPMUser.Cod & "#" & oInstancia.ID & "#" & hBloque.Value)
                nombreXML = sXMLName

                Dim lIDTiempoProc As Long
                oInstancia.ActualizarTiempoProcesamiento(lCiaComp, lIDTiempoProc, FSPMUser.CodCia)

                Dim sProveGS As String = FSPMUser.ObtenerProv(lCiaComp)

                ds = New DataSet
                Session.Remove(nombreXML)
                ds.ReadXml(oXMLFolder.FullName & "\" & sXMLName & "#PORTAL.xml")

                If ds.Tables.Contains("SOLICITUD") Then Exit Sub
                ds.Tables.Add("SOLICITUD")
                With ds.Tables("SOLICITUD").Columns
                    .Add("TIPO_PROCESAMIENTO_XML")
                    .Add("TIPO_DE_SOLICITUD")
                    .Add("COMPLETO")
                    .Add("SOLICITUD")
                    .Add("FORMULARIO")
                    .Add("WORKFLOW")
                    .Add("INSTANCIA")
                    .Add("PETICIONARIO_PROVE")
                    .Add("PETICIONARIO_PROVECON")
                    .Add("ROL_ACTUAL")
                    .Add("CIACOMP")
                    .Add("PROVE")
                    .Add("COD")
                    .Add("CODCIA")
                    .Add("DENCIA")
                    .Add("NIFCIA")
                    .Add("IDCIA")
                    .Add("IDUSU")
                    .Add("NOMBRE")
                    .Add("APELLIDOS")
                    .Add("TELEFONO")
                    .Add("USUARIO_EMAIL")
                    .Add("FAX")
                    .Add("USUARIO_IDIOMA")
                    .Add("PROVEGS")
                    .Add("IMPORTE", System.Type.GetType("System.Double"))
                    .Add("ACCION")
                    .Add("IDACCION")
                    .Add("GUARDAR")
                    .Add("DEVOLUCION_COMENTARIO")
                    .Add("COMENTARIO")
                    .Add("BLOQUE_ORIGEN")
                    .Add("BLOQUES_DESTINO")
                    .Add("NOTIFICAR")
                    .Add("FACTURA")
                    .Add("IDTIEMPOPROC")
                End With
                Dim drSolicitud As DataRow
                drSolicitud = ds.Tables("SOLICITUD").NewRow
                With drSolicitud
                    .Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.Portal)
                    .Item("TIPO_DE_SOLICITUD") = CInt(oInstancia.Solicitud.TipoSolicit)
                    .Item("COMPLETO") = 1
                    .Item("SOLICITUD") = IIf(hSolicitud.Value = 0, String.Empty, hSolicitud.Value)
                    .Item("FORMULARIO") = CInt(oInstancia.Solicitud.Formulario.Id)
                    .Item("WORKFLOW") = CInt(oInstancia.Solicitud.Workflow)
                    .Item("INSTANCIA") = hInstancia.Value
                    .Item("PETICIONARIO_PROVE") = oInstancia.PeticionarioProve
                    .Item("PETICIONARIO_PROVECON") = oInstancia.PeticionarioProveContacto
                    .Item("ROL_ACTUAL") = RolActual.Value
                    .Item("CIACOMP") = lCiaComp
                    .Item("PROVE") = FSPMUser.CodProve
                    .Item("COD") = FSPMUser.Cod
                    .Item("CODCIA") = FSPMUser.CodCia
                    .Item("DENCIA") = FSPMUser.DenCia
                    .Item("NIFCIA") = FSPMUser.NIFCia
                    .Item("IDCIA") = FSPMUser.IdCia
                    .Item("IDUSU") = FSPMUser.IdContacto
                    .Item("NOMBRE") = FSPMUser.Nombre
                    .Item("APELLIDOS") = FSPMUser.Apellidos
                    .Item("TELEFONO") = FSPMUser.Telefono
                    .Item("USUARIO_EMAIL") = FSPMUser.Email
                    .Item("FAX") = FSPMUser.Fax.ToString
                    .Item("USUARIO_IDIOMA") = FSPMUser.Idioma.ToString
                    .Item("PROVEGS") = sProveGS
                    .Item("IMPORTE") = Session("Importe")
                    .Item("ACCION") = hAccion.Value
                    .Item("IDACCION") = hAccionId.Value
                    .Item("GUARDAR") = hGuardar.Value
                    .Item("DEVOLUCION_COMENTARIO") = txtComentarioDevolucion.Text
                    .Item("COMENTARIO") = txtComentario.Value
                    .Item("BLOQUE_ORIGEN") = hBloque.Value
                    .Item("BLOQUES_DESTINO") = hBloqueDestino.Value
                    .Item("NOTIFICAR") = Notificar.Value
                    .Item("IDTIEMPOPROC") = lIDTiempoProc
                End With
                ds.Tables("SOLICITUD").Rows.Add(drSolicitud)

                If Not dsRoles.Tables("ROLES") Is Nothing Then ds.Tables.Add(dsRoles.Tables("ROLES").Copy)

                'Borrar XML incompleto
                File.Delete(oXMLFolder.FullName & "\" & sXMLName & "#PORTAL.xml")
                Session(nombreXML) = ds

                oInstancia.ActualizarTiempoProcesamiento(lCiaComp, lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
                'Cargar XML completo
                If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
                    'Se hace con un StringWriter porque el m�todo GetXml del dataset no incluye el esquema
                    Dim oSW As New System.IO.StringWriter()
                    ds.WriteXml(oSW, XmlWriteMode.WriteSchema)

                    Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
                    oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), sXMLName)
                Else
                    ds.WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & "#PORTAL.xml", XmlWriteMode.WriteSchema)
                    If File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml") Then _
                        File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml")
                    FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & "#PORTAL.xml",
                                    ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml")
                End If
            Catch ex As Exception
                oFileW = New System.IO.StreamWriter(ConfigurationManager.AppSettings("logtemp") & "\PM_P_log_" & FSPMUser.Cod & ".txt", True)
                oFileW.WriteLine()
                oFileW.WriteLine("Error GuardarConWorkflow_ProcesarXML: " & ex.Message() & ". Hora del error: " & Format(System.DateTime.Now, "dd/MM/yyyy HH:mm:ss"))
                oFileW.Close()
            End Try
        End Sub
#Region "BOTONES CANCELAR/ACEPTAR"
        Private Sub cmdCancelarDevolucion_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancelarDevolucion.ServerClick
            Response.Redirect("../solicitudes/detalleSolicitud.aspx?Instancia=" & Request("hInstancia"))
        End Sub
        ''' <summary>
        ''' Realiza la acci�n
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub cmdAceptar_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptar.ServerClick
            'Realiza la acci�n:
            Dim bEjecutarAccion As Boolean
            Dim i As Integer
            Dim lCiaComp As Long = IdCiaComp

            bEjecutarAccion = True

            '1� Comprueba que se hayan introducido todos los roles que estaban vac�os:
            For i = 0 To wdgtxtRoles.Rows.Count - 1
                If DBNullToStr(wdgtxtRoles.Rows(i).Items.FindItemByKey("ASIGNAR").Value) AndAlso
                    DBNullToStr(wdgtxtRoles.Rows(i).Items.FindItemByKey("PER").Value) = "" AndALSO _
                    DBNullToStr(wdgtxtRoles.Rows(i).Items.FindItemByKey("PROVE").Value) = "" AndALSO _
                    DBNullToDbl(wdgtxtRoles.Rows(i).Items.FindItemByKey("COMO_ASIGNAR").Value) <> 3 Then
                    bEjecutarAccion = False
                    Exit For
                End If
            Next

            If bEjecutarAccion = False Then
                Dim sClientTexts As String
                Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"

                sClientTexts = "alert(""" & JSText(Session("m_sIdiMsgbox")) & """); history.back();"
                sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)
            Else
                '1� Genera un dataset con los roles a los que se ha introducido persona o prove:
                Dim oRow As DataRow
                dsRoles.Tables.Add("ROLES")
                dsRoles.Tables(0).Columns.Add("ROL")
                dsRoles.Tables(0).Columns.Add("PER")
                dsRoles.Tables(0).Columns.Add("PROVE")
                dsRoles.Tables(0).Columns.Add("CON")
                dsRoles.Tables(0).Columns.Add("TIPO")

                For i = 0 To wdgtxtRoles.Rows.Count - 1
                    If wdgtxtRoles.Rows(i).Items.FindItemByKey("SEL").Value = "..." Then
                        oRow = dsRoles.Tables(0).NewRow
                        oRow.Item("ROL") = wdgtxtRoles.Rows(i).Items.FindItemByKey("ROL").Value
                        oRow.Item("PER") = wdgtxtRoles.Rows(i).Items.FindItemByKey("PER").Value
                        oRow.Item("PROVE") = wdgtxtRoles.Rows(i).Items.FindItemByKey("PROVE").Value
                        oRow.Item("CON") = wdgtxtRoles.Rows(i).Items.FindItemByKey("CON").Value
                        oRow.Item("TIPO") = wdgtxtRoles.Rows(i).Items.FindItemByKey("TIPO").Value
                        dsRoles.Tables(0).Rows.Add(oRow)
                    End If
                Next

                oInstancia = FSPMServer.Get_Instancia
                oInstancia.ID = hInstancia.Value
                oInstancia.Load(IdCiaComp, Idioma, True)

                'Mahou San Miguel / 2017 / 93: Se estan quedando en proceso muchas solicitudes que contestan los proveedores desde portal. 
                'Puede ser que no est�n confirmando los env�os, y se quedan en proceso. Hay que dajr como etaba antes, que hasta que no confirmen no se ponga en proceso, ojo, solo en el portal. 
                oInstancia.Actualizar_En_proceso(lCiaComp, 1, hBloque.Value)

                GuardarConWorkflow_ProcesarXML()

                Response.Redirect("../solicitudes/accionRealizadaOK.aspx?Instancia=" & hInstancia.Value & "&Accion=" & hAccionId.Value & "&BloqueDestino=" & hBloqueDestino.Value & "&TipoVisor=" & DevolverTipoVisor(oInstancia.Solicitud.TipoSolicit))
            End If
        End Sub
        ''' <summary>
        ''' Para aceptar una devoluci�n
        ''' </summary>
        ''' <param name="sender">boton</param>
        ''' <param name="e">Evento de sistema</param>  
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
        Private Sub cmdAceptarDevolucion_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAceptarDevolucion.ServerClick
            Dim lCiaComp As Long = IdCiaComp

            'Carga los datos de la instancia:
            oInstancia = FSPMServer.Get_Instancia
            oInstancia.ID = hInstancia.Value
            oInstancia.Load(IdCiaComp, Idioma)

            'Si la instancia ha sido anulada por el peticionario no se puede trasladar:
            If oInstancia.Estado = PMPortalServer.TipoEstadoSolic.Anulada Then
                Response.Redirect("imposibleaccion.aspx?Instancia=" & oInstancia.ID & "&Accion=5")
                Exit Sub
            End If

            'Mahou San Miguel / 2017 / 93: Se estan quedando en proceso muchas solicitudes que contestan los proveedores desde portal. 
            'Puede ser que no est�n confirmando los env�os, y se quedan en proceso. Hay que dajr como etaba antes, que hasta que no confirmen no se ponga en proceso, ojo, solo en el portal. 
            oInstancia.Actualizar_En_proceso(lCiaComp, 1, hBloque.Value)

            GuardarConWorkflow_ProcesarXML()

            oInstancia = Nothing

            Response.Redirect("../solicitudes/devolucionOK.aspx?Instancia=" & hInstancia.Value & "&Destinatario=" & hDestinatario.Value)
        End Sub
#End Region
#Region "Page Methods"
        <Services.WebMethod(True)>
        <Script.Services.ScriptMethod()>
        Public Shared Sub Deshacer_En_Proceso(ByVal idInstancia As Long, ByVal idBloque As Long)
            Dim FSPMServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_User").CiaComp
            Dim oInstancia As PMPortalServer.Instancia
            oInstancia = FSPMServer.Get_Instancia
            oInstancia.ID = idInstancia
            oInstancia.Actualizar_En_proceso(lCiaComp, 0, idBloque)
            oInstancia.Actualizar_En_proceso(lCiaComp, 0)
        End Sub
#End Region
#Region "Llamada a WebService"
        ''' <summary>
        ''' Gestamp tiene 100 etapas a las q ir desde peticionario en varios flujos. Se quitan para q solo sea una etapa y un web 
        ''' service desde integraci�n nos dice cuales son las personas de autoasignaci�n.
        ''' </summary>
        ''' <param name="lRolPorWebService">Rol para 100 etapas.</param>
        ''' <param name="BloqueDestino">esa unica etapa q antes eran 100</param>
        ''' <param name="ContadorParalelo">Por si tiene q ir a varias etapas, los participantes cambian de uno a otro.</param>  
        ''' <returns>Si ha podido o no devolver una lista de personas separadas por #</returns>
        ''' <remarks>Llamada desde: PrepararConfirmacionRealizarAccion</remarks>
        Private Function Instancia_LlamadaWebService(ByVal lRolPorWebService As Long, ByVal BloqueDestino As Long, ByVal ContadorParalelo As Integer) As Boolean
            Dim dsXML As DataSet = CType(Session(nombreXML), DataSet)
            dsXML.WriteXml(ConfigurationManager.AppSettings("rutaXMLPendiente") & "\" & nombreXML, XmlWriteMode.WriteSchema)

            Dim oValorCampo As Object
            oValorCampo = LlamarWSServicio(ConfigurationManager.AppSettings("UrlParticipantesExterno"), "oXMLDocCadena", ConfigurationManager.AppSettings("rutaXMLPendiente") & "\" & nombreXML,
                                           "DevolverAprobadorResponse", "oXMLDocInstancia", "", "oXMLDocBLoque", BloqueDestino)

            'oValorCampo sera una lista de personas separadas por #. Ejemplo: 37HA#C227K#
            Dim oDTPMParticipantes As DataTable
            If ContadorParalelo = 0 Then
                oDTPMParticipantes = ds.Tables.Add("TEMP_PARTICIPANTES_EXTERNOS")
                oDTPMParticipantes.Columns.Add("ID", System.Type.GetType("System.Int32"))
                oDTPMParticipantes.Columns.Add("ROL", System.Type.GetType("System.Int32"))
                oDTPMParticipantes.Columns.Add("PER", System.Type.GetType("System.String"))
                'esto es un caso muy concreto de gestamp, nunca habra proveedor implicado
            Else
                oDTPMParticipantes = ds.Tables("TEMP_PARTICIPANTES_EXTERNOS")
            End If

            Dim keys(1) As DataColumn
            keys(0) = oDTPMParticipantes.Columns("ROL")
            keys(1) = oDTPMParticipantes.Columns("PER")
            oDTPMParticipantes.PrimaryKey = keys

            Dim dtNewRow As DataRow
            Dim sAux() As String = Split(oValorCampo, "#")
            For i As Integer = 0 To UBound(sAux) - 1 Step 1
                dtNewRow = oDTPMParticipantes.NewRow
                dtNewRow.Item("ID") = i + 1
                dtNewRow.Item("ROL") = lRolPorWebService
                dtNewRow.Item("PER") = sAux(i)
                oDTPMParticipantes.Rows.Add(dtNewRow)
            Next
            Session(nombreXML) = ds
            Return (oValorCampo = "")
        End Function
        ''' <summary>Ejecuta la llamada a un webservice q no incluimos en el proyecto.</summary>
        ''' <param name="sUrl">url del servicio con la funci�n a realizar. Ejemplo:http://localhost/FSNWebService_31900_9/GES_Tallent_WS.asmx/DevolverAprobador </param>
        ''' <param name="nomEntradaXml">Nombre de la variable q usa el webservice a realizar". En caso de webmethod para gestamp Tarea 3400, el xml</param>
        ''' <param name="valorXml">Valor de la variable q usa el webservice a realizar. En caso de webmethod para gestamp Tarea 3400, el Xml (ruta+nombre)</param>
        ''' <param name="nomResult">Nombre de la variable del webservice donde se deja el resultado</param>
        ''' <param name="nomEntradaInstancia">Nombre de la variable Instancia q usa el webservice a realizar.</param>
        ''' <param name="valorInstancia">Instancia q usa el webservice a realizar.</param> 
        ''' <param name="nomEntradaBloque">Nombre de la variable "Bloque destino q usa el webservice a realizar"</param>
        ''' <param name="valorBloque">Bloque q usa el webservice a realizar</param>  
        ''' <returns>Lo q responde el webservice</returns>
        ''' <remarks>Llamada desde: Instancia_LlamadaWebService</remarks>
        Private Function LlamarWSServicio(ByVal sUrl As String, ByVal nomEntradaXml As String, ByVal valorXml As String, ByVal nomResult As String, Optional ByVal nomEntradaInstancia As String = "",
                                          Optional ByVal valorInstancia As String = "", Optional ByVal nomEntradaBloque As String = "", Optional ByVal valorBloque As String = "") As String
            Dim valorResult As String = String.Empty
            Dim url, metodo, sSoapAction, sXml As String
            Dim num As Integer

            num = sUrl.IndexOf(".asmx")
            url = sUrl.Substring(0, num + 5)
            metodo = sUrl.Substring(num + 6)
            sSoapAction = "http://tempuri.org/" + metodo

            sXml = "<?xml version=""1.0"" encoding=""utf-8""?>"
            sXml = sXml + "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
            sXml = sXml + "<soap:Body>"
            sXml = sXml + "<" + metodo + " xmlns=""http://tempuri.org/"">"
            sXml = sXml + "<" + nomEntradaXml + ">" + valorXml + "</" + nomEntradaXml + ">"
            If nomEntradaInstancia <> "" Then sXml = sXml + "<" + nomEntradaInstancia + ">" + valorInstancia + "</" + nomEntradaInstancia + ">"
            If nomEntradaBloque <> "" Then sXml = sXml + "<" + nomEntradaBloque + ">" + valorBloque + "</" + nomEntradaBloque + ">"
            sXml = sXml + "</" + metodo + ">"
            sXml = sXml + "</soap:Body>"
            sXml = sXml + "</soap:Envelope>"

            Dim targetURI As New Uri(url)
            Dim req As System.Net.HttpWebRequest = DirectCast(System.Net.WebRequest.Create(targetURI), System.Net.HttpWebRequest)
            req.Headers.Add("SOAPAction", sSoapAction)
            req.ContentType = "text/xml; charset=""utf-8"""
            req.Accept = "text/xml"
            req.Method = "POST"
            Dim stm As Stream = req.GetRequestStream
            Dim stmw As New StreamWriter(stm)
            stmw.Write(sXml)
            stmw.Close()
            stm.Close()

            'Se realiza la llamada al WS
            Dim response As System.Net.WebResponse
            response = req.GetResponse()
            Dim responseStream As Stream = response.GetResponseStream

            Dim sr As New StreamReader(responseStream)
            Dim soapResult As String
            'Recogemos la respuesta del WS
            soapResult = sr.ReadToEnd
            Dim xmlResponse As New System.Xml.XmlDocument
            xmlResponse.LoadXml(soapResult)
            Dim xmlResult As System.Xml.XmlNode
            xmlResult = xmlResponse.GetElementsByTagName(nomResult).Item(0)
            If Not xmlResult Is Nothing Then
                'Recojo el valor del indicador de error
                valorResult = xmlResult.InnerText
            End If
            Return valorResult
        End Function
#End Region
    End Class
End Namespace
