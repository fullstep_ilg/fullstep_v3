Partial Class CentroUnicoServer
    Inherits FSPMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    ''' <summary>
    ''' Cargar la pagina. 
    ''' </summary>
    ''' <param name="sender">Pagina</param>
    ''' <param name="e">Evento de sistema</param>        
    ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oCentros As Fullstep.PMPortalServer.Centros
        Dim sCodOrgCompras As String = Request("CodOrgCompras")
        Dim sIdOrgCompras As String = Request("IdOrgCompras")

        oCentros = FSPMServer.Get_Centros
        oCentros.LoadData(IdCiaComp, , sCodOrgCompras)

        Dim sValor As String = String.Empty
        Dim sTexto As String = String.Empty
        Dim sScript As String

        If (oCentros.Data.Tables(0).Rows.Count) = 1 Then
            'Poner el Centro en el dataentry
            sValor = oCentros.Data.Tables(0).Rows(0).Item(0)
            sTexto = oCentros.Data.Tables(0).Rows(0).Item(0) & " - " & oCentros.Data.Tables(0).Rows(0).Item(1)
        End If
        sScript = "<script>window.parent.mostrarCentroUnico('" & sIdOrgCompras & "','" & sValor & "','" & sTexto & "');</script>"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "UnicoCentroKey", sScript)
    End Sub

End Class
