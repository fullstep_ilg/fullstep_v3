<%@ Page Language="vb" AutoEventWireup="false" Codebehind="QAEnProceso.aspx.vb" Inherits="QAEnProceso"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table id="tblComent" style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 8px" height="100%"
				cellSpacing="1" cellPadding="1" width="100%" border="0">
				<tr height="10%">
					<td>
						<table class="cabeceraSolicitud" id="tblCabecera" style="WIDTH: 100%; HEIGHT: 75px" cellSpacing="1"
							cellPadding="1" width="100%" border="0">
							<tr>
								<td style="WIDTH: 15%"><asp:label id="lblId" runat="server" CssClass="captionBlue" Width="100%"></asp:label></td>
								<td style="WIDTH: 352px"><asp:label id="lblIdBD" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></td>
								<td style="WIDTH: 20%"><asp:label id="lblFecha" runat="server" CssClass="captionBlue" Width="100%">lblFecha</asp:label></td>
								<td><asp:label id="lblFechaBD" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></td>
							</tr>
							<tr>
								<td style="WIDTH: 15%"></td>
								<td style="WIDTH: 352px"></td>
								<td style="WIDTH: 15%"></td>
								<td style="WIDTH: 352px"></td>
							</tr>
							<tr>
								<td style="WIDTH: 10%"><asp:label id="lblTipo" runat="server" CssClass="captionBlue" Width="100%">lblTipo</asp:label></td>
								<td colspan="3"><asp:label id="lblTipoBD" runat="server" CssClass="captionDarkBlue" Width="100%"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<p>
							<table width="90%" align="center" cellpadding="4">
								<tr>
									<td><img src="images/Icono_Error_Amarillo_40x40.gif"></td>
									<td><asp:Label id="lblEnProceso" runat="server" CssClass="SinDatos"></asp:Label></td>
								</tr>
							</table>
						</p>
					</td>
				</tr>
				<tr>
					<td align="center"><input type="button" id="btnCerrar" onclick="window.close()" class="boton" runat="server"
							NAME="btnCerrar"></td>
				</tr>
			</table>
		</form>
	</body>
</html>
