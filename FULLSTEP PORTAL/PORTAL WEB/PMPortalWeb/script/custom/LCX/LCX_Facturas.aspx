﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LCX_Facturas.aspx.vb" Inherits="Fullstep.PMPortalWeb.VisorFacturasLCX"%>
<%@ Register TagPrefix="pag" Src="~/script/_common/Paginador.ascx" TagName="Paginador"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
	    <title></title>
	    <meta http-equiv="expires" content="0"/>	
	    <script type="text/javascript" src="../../../../common/formatos.js"></script>
	    <script type="text/javascript" src="../../../../common/menu.asp"></script>		
        <script type="text/javascript">if (accesoExterno == 1) dibujaMenu("LCX/LCX_Facturas.aspx");</script>
        <script src="../../../js/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="../../../js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="../../../js/jquery/jquery.tmpl.min.js" type="text/javascript"></script>
                        
        <script type="text/javascript">
            /*''' <summary>
            ''' Iniciar la pagina.
            ''' </summary>     
            ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
        	function Init() {
        		document.getElementById('tablemenu').style.display = 'block';
        		$("#pnlParametros").hide();
        	};
            /*Descripcion:= Funcion que carga los valores del buscador de empresas en la caja de texto
            Parametros:=
            idEmpresa --> Id Empresa
            nombreEmpresa --> Nombre empresa
            Llamada desde:= BuscadorEmpresas.aspx --> aceptar()*/
            function SeleccionarEmpresa(idEmpresa, nombreEmpresa, nifEmpresa) {
            	oIdEmpresa = $("[id$=hidNifEmpresa]")[0]
            	if (oIdEmpresa)
            		oIdEmpresa.value = nifEmpresa;

            	otxtEmpresa = $("[id$=txtEmpresa]")[0]
            	if (otxtEmpresa)
            		otxtEmpresa.value = nombreEmpresa;
            };
            function eliminarEmpresa() {
            	$("[id$=hidNifEmpresa]")[0].value = "";
            	$("[id$=txtEmpresa]")[0].value = "";
            };
            /*<summary>
            Provoca el postback para hacer la esportación a excel
            </summary>
            <remarks>Llamada desde: Icono de exportar a Excel</remarks>
            --%>*/
            function MostrarExcel() {
            	__doPostBack("", "Excel");
            };
            /*<summary>Provoca el postback para hacer la exportación a Pdf</summary>
            <remarks>Llamada desde: Icono de exportar a Pdf</remarks>--%>*/
            function MostrarPdf() {
            	__doPostBack("", "Pdf");
            };
            function checkCollapseStatus() {
            	$("[id$=pnlParametros]").toggle();
            };
            function CtrlFecDesdeFactura() {
            	if ($("[id$=dtFechaFacturaDesde]")[0].title == "") {
            		alert(sMensajeFecDesde);
            		return false;
            	} else return true;
            };
        </script>
    </head>
<body class="common" onload="Init()">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization ="true"></asp:ScriptManager>

        <div style="margin-top: 10px;position:relative">
		    <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>
            <div style="float:right;color:Red" id="divfechaActualizacion" runat="server">
                <asp:Label id="lblFechaUltimaActualizacion" runat="server" >DFecha última actualización facturas : </asp:Label><span runat="server" ID="sUltimFechaActualizacion">##/##/##</span>
            </div>
	    </div>
        <div style="clear:both;"></div>
        <div class="ighg_FullstepGroupArea">                
            <asp:updatepanel ID="upBusqueda" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                        <div style="display:inline;width:150px;padding:2px;height:22px">			                
                            <div style="float: left;height:22px;padding:2px">
                                <asp:Label ID="lblCifEmpresa" runat="server">dCIF Empresa:</asp:Label>
                            </div>
                            <div style="float: left;padding:2px">
                                <ig:WebDropDown ID="wddCifEmpresa" runat="server" EnableViewState="true" Height="22px"
                                    Width="328px" EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true"
                                     EnableCustomValues = "false" >
                                    <AutoPostBackFlags SelectionChanged="On"/>
                                </ig:WebDropDown>
                            </div>
                            <div style="float: left;height:22px;padding:2px">
                                <asp:Label ID="lblEmpresa" runat="server">dEmpresa:</asp:Label>
                            </div>
                            <div style="float: left;padding:2px">
                                <ig:WebDropDown ID="wddEmpresa" runat="server" EnableViewState="true" Height="22px"
                                    Width="328px" EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true"
                                     EnableCustomValues = "false">
                                    <AutoPostBackFlags SelectionChanged="On" />
                                </ig:WebDropDown>
                            </div>
                            <div style="float: left;height:22px;padding:2px">
                                <asp:Label ID="lblNumeroFactura" runat="server">dNúmero Factura:</asp:Label>
                            </div>
                            <div style="float: left;height:22px;padding:2px">
                                <asp:TextBox ID="txtNumeroFactura" runat="server" Width="195px"></asp:TextBox>
                            </div>
                            <div style="float: left;height:22px;padding:2px">
                                <fsn:FSNButton ID="btnBuscar" runat="server" Alineacion="Right" OnClientClick="return CtrlFecDesdeFactura()">DBuscar</fsn:FSNButton>
                            </div>
                        </div>    
                        <div style="clear:both;"></div>
                </ContentTemplate>
            </asp:updatepanel>
        </div>        

        <asp:Panel runat="server" ID="pnlBusquedaAvanzada" Style="cursor: pointer" Font-Bold="True"
            Height="20px" Width="100%" CssClass="PanelCabecera" onClick="checkCollapseStatus();">
            <asp:Image ID="imgExpandir" runat="server" SkinID="Contraer" />
            <asp:Label ID="lblBusquedaAvanzada" runat="server" Text=" DSeleccione los criterios de búsqueda generales"
            Font-Bold="True" ForeColor="White"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="panelParametros" runat="server">
            <asp:updatepanel ID="upBusquedaAvanzada" runat="server" UpdateMode="Conditional"> 
                <ContentTemplate>
                    <asp:Panel id="pnlParametros" class="Rectangulo ighg_FullstepGroupArea" runat="server" style="display:block;">
		                <table style="height:60px" width="95%">
                            <tr>
                                <td><asp:label id="lblFechaFacturaDesde" Text="dFechaFactura (desde hasta)" runat="server" /></td>
					            <td><ig:WebDatePicker runat="server" ID="dtFechaFacturaDesde" SkinID="Calendario" Width="90%" ></ig:WebDatePicker></td>
                                <td> - </td>
                                <td><ig:WebDatePicker runat="server" ID="dtFechaFacturaHasta"  SkinID="Calendario" Width="90%" ></ig:WebDatePicker></td>
                                <td><asp:label id="lblEstadoFactura" Text="dEstadoFactura" runat="server" /></td>
                                <td>
                                    <ig:WebDropDown ID ="wddEstadoFactura" runat="server" EnableViewState="true" Height="22px" Width="328px" 
						                EnableMultipleSelection="false" EnableClosingDropDownOnSelect="true" EnableClosingDropDownOnBlur="true">
					                </ig:WebDropDown>  
                                </td>
                                <td><asp:CheckBox id="cbMisFacturas" runat="server" Text="dMis Facturas" Checked="true" Visible="false" /> </td>
                                <td><asp:CheckBox id="cbSuplidos" runat="server" Text="dSuplidos" Checked="false" Visible="false" /> </td>
				            </tr>					
				            <tr>
					            <td><asp:label id="lblFechaAbonoHasta" Text="dFecha abono (desde-hasta)" runat="server" /></td>
                                <td><ig:WebDatePicker runat="server" ID="dtFechaAbonoDesde"  SkinID="Calendario" Width="90%" ></ig:WebDatePicker></td>
                                <td> - </td>
                                <td><ig:WebDatePicker runat="server" ID="dtFechaAbonoHasta"  SkinID="Calendario" Width="90%" ></ig:WebDatePicker></td>
                                <td><asp:label id="lblClave" Text="dClave Factura" runat="server" /></td>
                                <td><asp:TextBox ID="txtClave" runat="server" Width="328px"  ></asp:TextBox></td>
                            </tr>
                            <tr>
					            <td><asp:label id="LblImporteDesdeHasta" Text="dImporte (desde-hasta)" Width="200px"  runat="server" /></td>
					            <td><ig:WebNumericEditor ID="txtImporteDesde" runat="server" Width="90%" ></ig:WebNumericEditor></td>
                                <td> - </td>
                                <td><ig:WebNumericEditor ID="txtImporteHasta" runat="server" Width="90%" ></ig:WebNumericEditor></td>
				            </tr>                                                        
			            </table>
                        <table width="80%" border="0" cellpadding="4" cellspacing="0">
			                <tr>
				                <td width="60px">
					                <fsn:FSNButton ID="btnBuscarBusquedaAvanzada" runat="server" Alineacion="left" OnClientClick="return CtrlFecDesdeFactura()">dBuscar</fsn:FSNButton>
				                </td>
				                <td width="60px">
					                <fsn:FSNButton ID="btnLimpiarBusquedaAvanzada" runat="server" Alineacion="Right">dLimpiar</fsn:FSNButton>
				                </td>
				                <td></td>
			                </tr>
		                </table>
	                </asp:Panel>
                </ContentTemplate>
            </asp:updatepanel>
        </asp:Panel>

        <cc1:CollapsiblePanelExtender ID="cpeBusquedaAvanzada" runat="server" CollapseControlID="pnlBusquedaAvanzada"
            ExpandControlID="pnlBusquedaAvanzada" SuppressPostBack="true" TargetControlID="panelParametros"
            ImageControlID="imgExpandir" SkinID="FondoRojo" Collapsed="true">
        </cc1:CollapsiblePanelExtender>
                
        <asp:UpdatePanel runat="server" ID="upFacturas" ChildrenAsTriggers="false" UpdateMode="Conditional">
            <ContentTemplate>
                <table>
                    <tr>
                        <td>
                            <ig:WebHierarchicalDataGrid ID="whdgFacturas" runat="server" AutoGenerateColumns="false" Width="100%" ShowHeader="true" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="false">
                                <Columns>
                                    <ig:BoundDataField Key="NUM_FACT" DataFieldName="NUM_FACT"></ig:BoundDataField>
                                    <ig:BoundDataField Key="SUPLIDO" DataFieldName="SUPLIDO"></ig:BoundDataField>
                                    <ig:BoundDataField Key="NOM_EMP" DataFieldName="NOM_EMP"></ig:BoundDataField>
                                    <ig:BoundDataField Key="FECHA" DataFieldName="FECHA" DataFormatString ="{0:d}"></ig:BoundDataField>
                                    <ig:BoundDataField Key="FECHA_REGISTRO" DataFieldName="FECHA_REGISTRO" DataFormatString ="{0:d}"></ig:BoundDataField>
                                    <ig:BoundDataField Key="CLAVE" DataFieldName="CLAVE"></ig:BoundDataField>
                                    <ig:BoundDataField Key="IMPORTE" DataFieldName="IMPORTE" DataFormatString="{0:C}" CssClass ="CeldaNumericaDataGrid">
                                    </ig:BoundDataField>
                                    <ig:BoundDataField Key="SITUACION" DataFieldName="SITUACION"></ig:BoundDataField>
                                    <ig:BoundDataField Key="CANAL" DataFieldName="CANAL"></ig:BoundDataField>
                                    <ig:BoundDataField Key="CUENTA_ABONO" DataFieldName="CUENTA_ABONO"></ig:BoundDataField>
                                    <ig:BoundDataField Key="FECHA_ABONO" DataFieldName="FECHA_ABONO" DataFormatString ="{0:d}"></ig:BoundDataField>                                    
                                </Columns>
                                <Behaviors >
                                    <ig:Activation Enabled="true"></ig:Activation>
                                    <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                                    <ig:Paging Enabled="true" PagerAppearance="Both">
                                        <PagerTemplate>
                                            <pag:Paginador id="wdgPaginador" runat="server"  />
                                        </PagerTemplate>
                                    </ig:Paging>
                                    <ig:Sorting Enabled="true" SortingMode="Multi" >                         
                                    </ig:Sorting>
                                    <ig:Filtering Alignment="Top" Visibility="Visible" Enabled="true" AnimationEnabled="true">
                                    </ig:Filtering>
                                </Behaviors>
                                <GroupingSettings EnableColumnGrouping="True" GroupAreaVisibility="Visible" />
                            </ig:WebHierarchicalDataGrid>
                        </td>
                    </tr>
                </table>                
            </ContentTemplate>
    </asp:UpdatePanel>
        <ig:WebDocumentExporter ID ="WebDocumentExporter1" runat="server"></ig:WebDocumentExporter>
        <ig:WebExcelExporter ID ="WebExcelExporter1" runat="server"></ig:WebExcelExporter>
    </form>
</body>
</html>