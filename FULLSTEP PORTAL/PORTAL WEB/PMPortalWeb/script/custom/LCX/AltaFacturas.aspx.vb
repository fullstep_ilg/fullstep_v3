﻿Imports System.Xml
Imports System.Linq
Imports System.Text
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml.Schema

Namespace Fullstep.PMPortalWeb
    Public Class AltaFacturas
        Inherits FSPMPage
#Region "Constantes"
        Private Const UECountyCodes As String = "AT|BE|BG|CY|HR|CZ|DK|EE|FI|FR|DE|GR|HU|IE|IT|LV|LT|LU|MT|NL|PL|PT|RO|SK|SI|SE|GB"
        Private Const CountryISOCodes As String = "AD|AND#AE|ARE#AF|AFG#AG|ATG#AI|AIA#AL|ALB#AM|ARM#AN|ANT#AO|AGO#AQ|ATA#AR|ARG#AS|ASM#AT|AUT#AU|AUS#AW|ABW#AX|ALA#AZ|AZE#" & _
            "BA|BIH#BB|BRB#BD|BGD#BE|BEL#BF|BFA#BG|BGR#BH|BHR#BI|BDI#BJ|BEN#BL|BLM#BM|BMU#BN|BRN#BO|BOL#BR|BRA#BS|BHS#BT|BTN#BV|BVT#BW|BWA#BY|BLR#BZ|BLZ#" & _
            "CA|CAN#CC|CCK#CF|CAF#CG|COG#CH|CHE#CI|CIV#CK|COK#CL|CHL#CM|CMR#CN|CHN#CO|COL#CR|CRI#CU|CUB#CV|CPV#CX|CXR#CY|CYP#CZ|CZE#" & _
            "DE|DEU#DJ|DJI#DK|DNK#DM|DMA#DO|DOM#DZ|DZA#" & _
            "EC|ECU#EE|EST#EG|EGY#EH|ESH#ER|ERI#ES|ESP#ET|ETH#" & _
            "FI|FIN#FJ|FJI#FK|KLK#FM|FSM#FO|FRO#FR|FRA#" & _
            "GA|GAB#GB|GBR#GD|GRD#GE|GEO#GF|GUF#GG|GGY#GH|GHA#GI|GIB#GL|GRL#GM|GMB#GN|GIN#GP|GLP#GQ|GNQ#GR|GRC#GS|SGS#GT|GTM#GU|GUM#GW|GNB#GY|GUY#" & _
            "HK|HKG#HM|HMD#HN|HND#HR|HRV#HT|HTI#HU|HUN#" & _
            "ID|IDN#IE|IRL#IL|ISR#IM|IMN#IN|IND#IO|IOT#IQ|IRQ#IR|IRN#IS|ISL#IT|ITA#" & _
            "JE|JEY#JM|JAM#JO|JOR#JP|JPN#" & _
            "KE|KEN#KG|KGZ#KH|KHM#KI|KIR#KM|COM#KN|KNA#KP|PRK#KR|KOR#KW|KWT#KY|CYM#KZ|KAZ#" & _
            "LA|LAO#LB|LBN#LC|LCA#LI|LIE#LK|LKA#LR|LBR#LS|LSO#LT|LTU#LU|LUX#LV|LVA#LY|LBY#" & _
            "MA|MAR#MC|MCO#MD|MDA#ME|MNE#MG|MDG#MH|MHL#MK|MKD#ML|MLI#MM|MMR#MN|MNG#MO|MAC#MQ|MTQ#MR|MRT#MS|MSR#MT|MLT#MU|MUS#MV|MDV#MW|MWI#MX|MEX#MY|MYS#MZ|MOZ#" & _
            "NA|NAM#NC|NCL#NE|NER#NF|NFK#NG|NGA#NI|NIC#NL|NLD#NO|NOR#NP|NPL#NR|NRU#NU|NIU#NZ|NZL#" & _
            "OM|OMN#" & _
            "PA|PAN#PE|PER#PF|PYF#PG|PNG#PH|PHL#PK|PAK#PL|POL#PM|SPM#PN|PCN#PR|PRI#PS|PSE#PT|PRT#PW|PLW#PY|PRY#" & _
            "QA|QAT#" & _
            "RE|REU#RO|ROU#RS|SRB#RU|RUS#RW|RWA#" & _
            "SA|SAU#SB|SLB#SC|SYC#SD|SDN#SE|SWE#SG|SGP#SH|SHN#SI|SVN#SJ|SJM#SK|SVK#SL|SLE#SM|SMR#SN|SEN#SO|SOM#SR|SUR#ST|STP#SV|SLV#SY|SYR#SZ|SWZ#" & _
            "TC|TCA#TD|TCD#TF|ATF#TG|TGO#TH|THA#TH|TZA#TJ|TJK#TK|TKL#TL|TLS#TM|TKM#TN|TUN#TO|TON#TR|TUR#TT|TTO#TV|TUV#TW|TWN#" & _
            "UA|UKR#UG|UGA#US|USA#UY|URY#UZ|UZB#" & _
            "VA|VAT#VC|VCT#VE|VEN#VG|VGB#VI|VIR#VN|VNM#VU|VUT#" & _
            "WF|WLF#WS|WSM#" & _
            "YE|YEM#YT|MYT#" & _
            "ZA|ZAF"
        Private ValidacionCorrecta As Boolean = True
#End Region
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

            Dim _FSPMServer As PMPortalServer.Root
            _FSPMServer = Session("FS_Portal_Server")

            If IsNothing(_FSPMServer) Then
                Response.Redirect(ConfigurationManager.AppSettings("rutanormal") & "/../../../", False)
                Exit Sub
            End If

            If Not IsPostBack Then
                CargarTextosPantalla()

                'Primero obtenemos la info del proveedor y ver si tenemos toda la informacion necesaria.
                'Puede pasar que siendo persona fisica no tengamos guardados el nombre, primer apellido y segundo apellido
                Dim iProveedor As CProveedorLCX = ObtenerInfoProveedor()
                'Reemplazamos las almoadillas por la denominacion de la compañia
                txtCondiciones.Text = Replace(Replace(Textos(12), "<br/>", Chr(13) & Chr(10) & Chr(13) & Chr(10)), "#####", FSPMUser.DenCia)

                pnlCondiciones.Visible = Not iProveedor.AceptacionCondiciones
                pnlNIFUsu.Visible = String.IsNullOrEmpty(FSPMUser.NIFUsu)
                pnlDatosPersonaFisica.Visible = (iProveedor.PersonType = "F" _
                        AndAlso (String.IsNullOrEmpty(iProveedor.PersonName) _
                             OrElse String.IsNullOrEmpty(iProveedor.PersonFirstName) _
                             OrElse String.IsNullOrEmpty(iProveedor.PersonLastName)))

                If Not pnlCondiciones.Visible AndAlso Not pnlDatosPersonaFisica.Visible AndAlso Not pnlNIFUsu.Visible Then
                    Dim xmlDoc As XmlDocument
                    xmlDoc = Obtener_XML_Proveedor(iProveedor)
                    If xmlDoc IsNot Nothing Then Llamada_URL_Externa(xmlDoc)
                End If
            End If
        End Sub
        Private Sub CargarTextosPantalla()
            Me.ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.AltaFacturasLCX
            lblTituloPantalla.Text = JSText(Textos(0))
            lblInfoCondiciones.Text = Textos(1)
            txtCondiciones.Text = Replace(Textos(12), "<br/>", Chr(13) & Chr(10) & Chr(13) & Chr(10))
            lblNombre.Text = "(*) " & JSText(Textos(2)) & ":"
            lblPrimerApellido.Text = "(*) " & JSText(Textos(3)) & ":"
            lblSegundoApellido.Text = "(*) " & JSText(Textos(4)) & ":"
            chkLeido.Text = JSText(Textos(10))
            lblSolicitudInfo.Text = JSText(Textos(11))
            btnAceptar.Text = JSText(Textos(8))
            btnCancelar.Text = JSText(Textos(9))
            lblNIFUsuInfo.Text = JSText(Textos(13))
            lblNIFUsu.Text = "(*) " & JSText(Textos(14)) & ":"

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
                Dim sVariableJavascriptTextosPantalla As String = "var TextosPantalla = new Array();"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[0]='" & JSText(Textos(5)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[1]='" & JSText(Textos(6)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[2]='" & JSText(Textos(7)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[3]='" & JSText(Textos(15)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[4]='" & JSText(Textos(16)) & "';"
                sVariableJavascriptTextosPantalla &= "TextosPantalla[5]='" & JSText(Textos(17)) & "';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextosPantalla, True)
            End If
        End Sub
        Private Function Obtener_XML_Proveedor(ByVal iProveedor As CProveedorLCX) As XmlDocument
            ' Create XmlWriterSettings.
            Dim settings As XmlWriterSettings = New XmlWriterSettings()
            settings.Indent = True

            ' Create XmlWriter.
            Dim xmlDoc As New XmlDocument
            Using writer As XmlWriter = xmlDoc.CreateNavigator.AppendChild
                ' Begin writing.
                writer.WriteStartDocument()
                writer.WriteStartElement("ProviderModelRequest") ' Root.
                With iProveedor
                    writer.WriteStartElement("ProviderCompanyActivities")
                    For Each dr As DataRow In iProveedor.Activities.Rows
                        writer.WriteStartElement("Activity")
                        writer.WriteElementString("ActivityName", dr("DEN_" & FSPMUser.Idioma).ToString)
                        writer.WriteEndElement()
                    Next
                    writer.WriteEndElement()
                    writer.WriteElementString("ProviderPersonType", .PersonType)
                    writer.WriteElementString("ProviderCIFType", .CIFType)
                    writer.WriteElementString("ProviderCIF", .CIF)
                    writer.WriteElementString("ProviderCompanyName", .CompanyName)
                    writer.WriteElementString("ProviderPersonName", .PersonName)
                    writer.WriteElementString("ProviderPersonFirstName", .PersonFirstName)
                    writer.WriteElementString("ProviderPersonLastName", .PersonLastName)
                    writer.WriteElementString("ProviderPhone", .Phone)
                    writer.WriteElementString("ProviderResidenceType", .ResidenceType)
                    writer.WriteElementString("ProviderCountryName", .CountryName)
                    writer.WriteElementString("ProviderCountryCode3", .CountryCode3)
                    writer.WriteElementString("ProviderAddress", .Address)
                    writer.WriteElementString("ProviderPostalCode", .PostalCode)
                    writer.WriteElementString("ProviderTown", .Town)
                    writer.WriteElementString("ProviderProvince", .Province)
                    writer.WriteElementString("ProviderUserEmail", .UserEmail)
                    writer.WriteElementString("ProviderUserNIF", FSPMUser.NIFUsu)
                    Select Case FSPMUser.Idioma.ToString
                        Case "ENG"
                            writer.WriteElementString("ProviderUserLanguage", "ENG")
                        Case "GER"
                            writer.WriteElementString("ProviderUserLanguage", "CAT")
                        Case Else
                            writer.WriteElementString("ProviderUserLanguage", "ESP")
                    End Select
                End With
                writer.WriteEndElement()

                ' End document.
                writer.WriteEndDocument()
            End Using
            ' Create an XML declaration. 
            Dim xmldecl As XmlDeclaration
            xmldecl = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", Nothing)

            ' Add the new node to the document.
            Dim root As XmlElement = xmlDoc.DocumentElement
            xmlDoc.InsertBefore(xmldecl, root)

            'Antes de devolver el XML Validaremos con el schema que sea valido
            '(no es muy necesario debido a que hemos creado el XML siguiendo el patron del schema, pero porsi)
            xmlDoc.Schemas.Add("", System.AppDomain.CurrentDomain.BaseDirectory() & "script\custom\LCX\Schemas\LCXProvider.xsd")
            Dim eventHandler As ValidationEventHandler = New ValidationEventHandler(AddressOf ValidationEventHandler)
            xmlDoc.Validate(eventHandler)
            If ValidacionCorrecta Then
                Return xmlDoc
            Else
                Return Nothing
            End If
        End Function
        Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As ValidationEventArgs)
            Select Case e.Severity
                Case XmlSeverityType.Error
                    ValidacionCorrecta = False
                Case XmlSeverityType.Warning
                    ValidacionCorrecta = False
            End Select
        End Sub
        Private Function ObtenerInfoProveedor() As CProveedorLCX
            Dim oProve As Fullstep.PMPortalServer.Proveedor
            oProve = FSPMServer.get_Proveedor
            oProve.Cod = FSPMUser.CodProveGS
            oProve.Load(FSPMUser.Idioma, FSPMUser.CiaComp)

            Dim infoProveedor As New CProveedorLCX
            With infoProveedor
                .CompanyName = FSPMUser.DenCia
                Dim infoPersonaFisica As Dictionary(Of String, String) = Obtener_Info_PersonaFisica()
                .PersonName = infoPersonaFisica("NOMBRE")
                .PersonFirstName = infoPersonaFisica("PRIMER_APELLIDO")
                .PersonLastName = infoPersonaFisica("SEGUNDO_APELLIDO")
                .AceptacionCondiciones = infoPersonaFisica("ACEPTACION_CONDICIONES")
                .Phone = FSPMUser.Telefono
                .ResidenceType = TipoResidencia(oProve.PaiCod)
                .CountryName = oProve.PaiDen
                .CountryCode3 = Obtener_CodigoISO3_Pais(oProve.PaiCod)
                .Address = oProve.Dir
                .PostalCode = oProve.CP
                .Town = oProve.Pob
                .Province = oProve.ProviDen
                .UserEmail = FSPMUser.Email
                .Activities = oProve.LoadActivities_NivelSuperior(FSPMUser.Idioma, FSPMUser.IdCia)
                'La Caixa Proveedores / 2016 / 269: No se debe validar el nif si el país de la cia no es España. Así mismo si no es España siempre se debe pasar "j" en el parámetro ProviderPersonType. 
                .CIFType = TipoDocumentoIdentificativo(FSPMUser.NIFCia)
                .PersonType = IIf(.CIFType = "CIF" OrElse (Not .ResidenceType = "R"), "J", "F")
                hidValidableNif.Value = IIf(.ResidenceType = "R", 1, 0)
                .CIF = FSPMUser.NIFCia
            End With
            Return infoProveedor
        End Function
        Public Sub Llamada_URL_Externa(ByVal xmlDoc As XmlDocument)
            'Codificacion de la informacion del XML en UTF8 y posteriormente en base 64            
            Dim xmlUTF8 As Byte() = System.Text.Encoding.UTF8.GetBytes(xmlDoc.InnerXml.ToString)
            Dim Base64StrData As String = Convert.ToBase64String(xmlUTF8)
            'Codificacion en caracteres validos de URL
            Dim paramNew As String = HttpUtility.UrlEncode(Base64StrData)
            'Creacion de parametros para el envio
            Dim parametros As New Dictionary(Of String, String)
            parametros.Add("Param", paramNew)
            Dim parametrosConcatenados As String = ConcatParams(parametros)
            'Codificacion del mensaje en UTF8
            Dim postBytes As Byte() = System.Text.Encoding.UTF8.GetBytes(parametrosConcatenados)
            'Enviar por POST al servicio de verificacion la peticion (XML)
            Dim _ServicioVerificacionURI As String = ConfigurationManager.AppSettings("URLPROVIMAD")
            Dim wr As System.Net.WebRequest = System.Net.WebRequest.Create(_ServicioVerificacionURI)
            With wr
                .Method = "POST"
                .ContentType = "application/x-www-form-urlencoded"
                .ContentLength = postBytes.Length
            End With

            'Envio de parametros
            Dim newStream As System.IO.Stream = wr.GetRequestStream
            newStream.Write(postBytes, 0, postBytes.Length)

            'Procesamiento de la respuesta
            Using resp As System.Net.HttpWebResponse = wr.GetResponse
                If resp IsNot Nothing Then
                    If resp.StatusCode = Net.HttpStatusCode.OK Then
                        Using answerReader = New System.IO.StreamReader(resp.GetResponseStream)
                            Dim responsePOST = answerReader.ReadToEnd
                            If responsePOST IsNot Nothing AndAlso Not String.IsNullOrEmpty(responsePOST) Then
                                'FALTA GESTIONAR LOS ERRORES QUE DA EL SERVICIO, QUE MOSTRAR AL USUARIO???
                                If Not Left(responsePOST, 5).ToLower = "error" AndAlso Not Left(responsePOST, 5).ToLower = "catch" Then
                                    Response.Redirect(responsePOST, False)
                                Else
                                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "claveStartUpScript2", "MostrarError('" & responsePOST & "')", True)
                                End If
                            End If
                            answerReader.Close()
                        End Using
                    End If
                End If
                newStream.Close()
                resp.Close()
            End Using
        End Sub
        Private Function TipoDocumentoIdentificativo(ByVal Documento As String) As String
            Dim letraInicio As String = Left(Documento, 1)
            If {"X", "Y", "Z"}.Contains(letraInicio) Then
                Return "NIE"
            ElseIf Not IsNumeric(letraInicio) Then
                Return "CIF"
            Else
                Return "NIF"
            End If
        End Function
        Private Function Obtener_Info_PersonaFisica() As Dictionary(Of String, String)
            Dim oProve As Fullstep.PMPortalServer.Proveedor
            oProve = FSPMServer.get_Proveedor
            Return (oProve.Obtener_Info_PersonaFisica(FSPMUser.IdCia))
        End Function
        Private Function TipoResidencia(ByVal ISOPais As String) As String
            If ISOPais = "ES" Then
                Return "R"
            ElseIf UECountyCodes.Split("|").Contains(ISOPais) Then
                Return "U"
            Else
                Return "E"
            End If
        End Function
        Private Function Obtener_CodigoISO3_Pais(ByVal CodigoISOPais2 As String) As String
            Dim lCodigosISO As New Dictionary(Of String, String)
            For Each pais As String In CountryISOCodes.Split("#")
                lCodigosISO(pais.Split("|")(0)) = pais.Split("|")(1)
            Next
            If lCodigosISO.ContainsKey(CodigoISOPais2) Then
                Return lCodigosISO(CodigoISOPais2)
            Else
                Return ""
            End If
        End Function
        Private Function ConcatParams(ByVal parameters As Dictionary(Of String, String)) As String
            Dim FirstParam As Boolean = True
            Dim Parametros As StringBuilder = Nothing

            If parameters IsNot Nothing Then
                Parametros = New StringBuilder
                For Each param As KeyValuePair(Of String, String) In parameters
                    Parametros.Append(IIf(FirstParam, "", "&"))
                    Parametros.Append(param.Key & "=" & HttpUtility.UrlEncode(param.Value))
                    FirstParam = False
                Next
            End If
            Return Parametros.ToString
        End Function
        Private Class CProveedorLCX
            Private _personType As String
            Public Property PersonType() As String
                Get
                    Return _personType
                End Get
                Set(ByVal value As String)
                    _personType = value
                End Set
            End Property
            Private _cifType As String
            Public Property CIFType() As String
                Get
                    Return _cifType
                End Get
                Set(ByVal value As String)
                    _cifType = value
                End Set
            End Property
            Private _cif As String
            Public Property CIF() As String
                Get
                    Return _cif
                End Get
                Set(ByVal value As String)
                    _cif = value
                End Set
            End Property
            Private _companyName As String
            Public Property CompanyName() As String
                Get
                    Return _companyName
                End Get
                Set(ByVal value As String)
                    _companyName = value
                End Set
            End Property
            Private _personName As String
            Public Property PersonName() As String
                Get
                    Return _personName
                End Get
                Set(ByVal value As String)
                    _personName = value
                End Set
            End Property
            Private _personFirstName As String
            Public Property PersonFirstName() As String
                Get
                    Return _personFirstName
                End Get
                Set(ByVal value As String)
                    _personFirstName = value
                End Set
            End Property
            Private _personLastName As String
            Public Property PersonLastName() As String
                Get
                    Return _personLastName
                End Get
                Set(ByVal value As String)
                    _personLastName = value
                End Set
            End Property
            Private _aceptacionCondiciones As Boolean
            Public Property AceptacionCondiciones() As Boolean
                Get
                    Return _aceptacionCondiciones
                End Get
                Set(ByVal value As Boolean)
                    _aceptacionCondiciones = value
                End Set
            End Property
            Private _phone As String
            Public Property Phone() As String
                Get
                    Return _phone
                End Get
                Set(ByVal value As String)
                    _phone = value
                End Set
            End Property
            Private _residenceType As String
            Public Property ResidenceType() As String
                Get
                    Return _residenceType
                End Get
                Set(ByVal value As String)
                    _residenceType = value
                End Set
            End Property
            Private _countryName As String
            Public Property CountryName() As String
                Get
                    Return _countryName
                End Get
                Set(ByVal value As String)
                    _countryName = value
                End Set
            End Property
            Private _countryCode3 As String
            Public Property CountryCode3() As String
                Get
                    Return _countryCode3
                End Get
                Set(ByVal value As String)
                    _countryCode3 = value
                End Set
            End Property
            Private _address As String
            Public Property Address() As String
                Get
                    Return _address
                End Get
                Set(ByVal value As String)
                    _address = value
                End Set
            End Property
            Private _postalCode As String
            Public Property PostalCode() As String
                Get
                    Return _postalCode
                End Get
                Set(ByVal value As String)
                    _postalCode = value
                End Set
            End Property
            Private _town As String
            Public Property Town() As String
                Get
                    Return _town
                End Get
                Set(ByVal value As String)
                    _town = value
                End Set
            End Property
            Private _province As String
            Public Property Province() As String
                Get
                    Return _province
                End Get
                Set(ByVal value As String)
                    _province = value
                End Set
            End Property
            Private _userEmail As String
            Public Property UserEmail() As String
                Get
                    Return _userEmail
                End Get
                Set(ByVal value As String)
                    _userEmail = value
                End Set
            End Property
            Private _activities As DataTable
            Public Property Activities() As DataTable
                Get
                    Return _activities
                End Get
                Set(ByVal value As DataTable)
                    _activities = value
                End Set
            End Property
        End Class
        Private Sub btnAceptar_Click(sender As Object, e As System.EventArgs) Handles btnAceptar.Click
            Dim oProve As Fullstep.PMPortalServer.Proveedor
            oProve = FSPMServer.get_Proveedor
            oProve.Guardar_Info_PersonaFisica(FSPMUser.IdCia, FSPMUser.Cod, txtNombre.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, txtNIFUsu.Text)
            If Not String.IsNullOrEmpty(txtNIFUsu.Text) Then FSPMUser.NIFUsu = txtNIFUsu.Text
            Dim iProveedor As CProveedorLCX = ObtenerInfoProveedor()
            Dim xmlDoc As XmlDocument
            xmlDoc = Obtener_XML_Proveedor(iProveedor)
            If xmlDoc IsNot Nothing Then Llamada_URL_Externa(xmlDoc)
        End Sub
    End Class
End Namespace