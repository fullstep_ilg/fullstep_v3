﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AltaFacturas.aspx.vb" Inherits="Fullstep.PMPortalWeb.AltaFacturas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="../../../../common/formatos.js"></script>
    <script src="../../../js/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../../js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function ValidarDatos() {
            if (document.getElementById("chkLeido")!==null && !document.getElementById("chkLeido").checked) {
                alert(TextosPantalla[3]);
                return false;
            }
            if (document.getElementById("txtNombre") !== null && document.getElementById("txtNombre").value == '') {
                alert(TextosPantalla[0]);
                return false;
            }
            if (document.getElementById("txtPrimerApellido") !== null && document.getElementById("txtPrimerApellido").value == '') {
                alert(TextosPantalla[1]);
                return false;
            }
            if (document.getElementById("txtSegundoApellido") !== null && document.getElementById("txtSegundoApellido").value == '') {
                alert(TextosPantalla[2]);
                return false;
            }
            if (document.getElementById("txtNIFUsu") !== null) {
                if (document.getElementById("txtNIFUsu").value == '') {
                    alert(TextosPantalla[4]);
                    return false;
                }
                if (document.getElementById("hidValidableNif").value == 1) {
                    if (!validarNIF_NIE(document.getElementById("txtNIFUsu").value)) {
                        alert(TextosPantalla[5]);
                        return false;
                    }
                }
            };
            MaximizarPopUp();
            $('#pantalla').hide();
            $('#popupFondo').css('height', $(document).height());
            $('#popupFondo').show();            
        };
        function MaximizarPopUp() {
            window.resizeTo(screen.width, screen.height);
            window.moveTo(0, 0);
        };
        function RedimensionarPopUp() {
            window.resizeTo(900, 600);
        };
        function MostrarError(error) {
            $('#btnAceptar').hide();
            alert(error);
        };  
    </script>
</head>
<body class="common" style="padding:1em;" onload="RedimensionarPopUp()">
    <form id="form1" runat="server">
        <div id="pantalla">
            <asp:Image runat="server" ID="imgLogo" SkinID="Logo" style="margin-bottom:1.5em;" />               
            <asp:Label runat="server" ID="lblTituloPantalla" CssClass="tituloDarkBlue" style="display:block;"></asp:Label>
            <asp:Panel runat="server" ID="pnlCondiciones">
                <asp:Label runat="server" ID="lblInfoCondiciones" CssClass="Texto12 Negrita" style="display:block; margin:0.5em 0em;"></asp:Label>
                <asp:TextBox runat="server" ID="txtCondiciones" TextMode="MultiLine" ReadOnly="true" style="border:solid 0.2em lightgray; width:100%; height:15em; display:block;"></asp:TextBox>
                <asp:CheckBox runat="server" ID="chkLeido" CssClass="Normal" style="cursor:pointer; display:block;" />
            </asp:Panel>        
            <asp:Panel runat="server" ID="pnlNIFUsu" style="margin-top:1.5em;">
                <asp:Label runat="server" ID="lblNIFUsuInfo" CssClass="Normal"></asp:Label>
                <div style="margin:0.2em 0em;">
                    <asp:Label runat="server" ID="lblNIFUsu" CssClass="captionBlue" style="display:inline-block; width:15em;"></asp:Label>
                    <asp:TextBox runat="server" ID="txtNIFUsu" style="width:15em;"></asp:TextBox>
                    <asp:HiddenField runat="server" ID="hidValidableNif" />
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlDatosPersonaFisica">
                <asp:Label runat="server" ID="lblSolicitudInfo" CssClass="Normal" style="display:block; margin:1em 0em;"></asp:Label>                                 
                <div style="margin:0.2em 0em;">
                    <asp:Label runat="server" ID="lblNombre" CssClass="captionBlue" style="display:inline-block; width:15em;"></asp:Label>
                    <asp:TextBox runat="server" ID="txtNombre" style="width:15em;"></asp:TextBox>
                </div>
                <div style="margin:0.2em 0em;">
                    <asp:Label runat="server" ID="lblPrimerApellido" CssClass="captionBlue" style="display:inline-block; width:15em;"></asp:Label>
                    <asp:TextBox runat="server" ID="txtPrimerApellido" style="width:15em;"></asp:TextBox>
                </div>
                <div style="margin:0.2em 0em;">
                    <asp:Label runat="server" ID="lblSegundoApellido" CssClass="captionBlue" style="display:inline-block; width:15em;"></asp:Label>
                    <asp:TextBox runat="server" ID="txtSegundoApellido" style="width:15em;"></asp:TextBox>
                </div>
            </asp:Panel>        
            <div style="text-align:center; margin:1em 0em;">            
                <asp:Button runat="server" ID="btnAceptar" CssClass="botonRedondeado" OnClientClick="return ValidarDatos();" Text="DAceptar" />
                <asp:Button runat="server" ID="btnCancelar" CssClass="botonRedondeado" OnClientClick="window.close();" Text="DCancelar" />
            </div>    
        </div>
        <div id="popupFondo" class="FondoModal" style="display:none; position:absolute; width:100%; top:0; left:0;">
            <img src="../../../images/CargandoMuro.gif" style="position:fixed; top:50%; left:50%; transform:translate(-50%, -50%);" />
        </div>
    </form>
</body>
</html>
