﻿Imports Fullstep.PMPortalServer
Imports Infragistics.Web.UI.GridControls
Imports Infragistics.Web.UI.ListControls
Imports System.Collections.Generic
Imports Fullstep.PMPortalDatabaseServer.TiposDeDatos

Namespace Fullstep.PMPortalWeb
	Public Class VisorFacturasLCX
		Inherits FSPMPage

		Private oFacturas As LCX_Facturas
		Private _facturas As List(Of LCX_Factura)
		Private _PaginadorBottom As Paginador
		Private _PaginadorTop As Paginador
		Private _empresas As DataTable
		Private filtro As FiltroFactura
		Private ReadOnly Property Facturas() As List(Of LCX_Factura)
			Get
				If _facturas Is Nothing Then
					If HttpContext.Current.Cache("facturasLCX_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString) IsNot Nothing Then
						_facturas = HttpContext.Current.Cache("facturasLCX_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString)
					Else
						_facturas = oFacturas.cargarTodasLasFacturas(getFiltroBusqueda)
						Me.InsertarEnCache("facturasLCX_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString, _facturas)
					End If
				End If
				Return _facturas
			End Get
		End Property
		Public Function getFiltroBusqueda() As FiltroFactura
			filtro = New FiltroFactura

			filtro.idioma = FSPMUser.Idioma
			If txtNumeroFactura.Text <> "" Then filtro.numFactura = txtNumeroFactura.Text
			If dtFechaFacturaDesde.Text <> "" Then filtro.fechaDesde = dtFechaFacturaDesde.Value
			If dtFechaFacturaHasta.Text <> "" Then filtro.fechaHasta = dtFechaFacturaHasta.Value
			If cbMisFacturas.Checked Then
				filtro.misFacturas = True
			Else
				filtro.misFacturas = False
			End If
			If cbSuplidos.Checked Then
				filtro.suplidos = True
			Else
				filtro.suplidos = False
			End If
			If dtFechaAbonoDesde.Text <> "" Then filtro.fechaAbonoDesde = dtFechaAbonoDesde.Value
			If dtFechaAbonoHasta.Text <> "" Then
				filtro.fechaAbonoHasta = Me.dtFechaAbonoHasta.Value
			Else
				filtro.fechaAbonoHasta = Nothing
			End If
			If txtImporteDesde.Text <> "" Then filtro.importeDesde = txtImporteDesde.Text
			If txtImporteHasta.Text <> "" Then filtro.importeHasta = txtImporteHasta.Text
			filtro.nif_prove = FSPMUser.NIFCia
			If wddCifEmpresa.SelectedValue <> "" Then filtro.nif_empresa = Me.wddCifEmpresa.SelectedValue
			If txtImporteDesde.Text <> "" Then filtro.importeDesde = CDbl(txtImporteDesde.Text)
			If txtImporteHasta.Text <> "" Then filtro.importeHasta = CDbl(txtImporteHasta.Text)
			If wddEstadoFactura.SelectedValue <> "" Then filtro.Estado.EstadoVisor = wddEstadoFactura.SelectedValue
			If txtClave.Text <> "" Then filtro.idClave = Me.txtClave.Text

			Return filtro
		End Function
		''' <summary>
		''' Empresas a mostrar en los combos
		''' </summary>
		''' <value></value>
		''' <returns>datatable con empresas</returns>
		''' <remarks></remarks>
		Private ReadOnly Property Empresas As DataTable
			Get
				If _empresas Is Nothing Then
					Dim oEmpresas As Empresas = FSPMServer.Get_Empresas
					_empresas = oEmpresas.ObtenerEmpresas(FSPMUser.CiaComp, FSPMUser.Idioma)
				End If
				Return _empresas
			End Get
		End Property
		Public Function getUltimaFechaActualizacion() As Date
			Dim max As Date
			For Each f As LCX_Factura In Facturas
				If f.FECHAACT.Value > max Then
					max = f.FECHAACT
				End If
			Next
			Return max
		End Function
		''' <summary>
		''' Borra todos los datos cacheados o almacenados en el objeto facturas
		''' </summary>
		''' <remarks></remarks>
		Private Sub BorrarCacheFacturas()
			If HttpContext.Current.Cache("facturasLCX_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString) IsNot Nothing Then _
					HttpContext.Current.Cache.Remove("facturasLCX_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString)
			_facturas = Nothing
		End Sub
		Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
			Dim _FSPMServer As PMPortalServer.Root
			_FSPMServer = Session("FS_Portal_Server")

			If IsNothing(_FSPMServer) Then
				Response.Redirect(ConfigurationManager.AppSettings("rutanormal") & "/../../../", False)
				Exit Sub
			End If

			_PaginadorBottom = TryCast(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerBottom.FindControl("wdgPaginador"), Paginador)
			AddHandler _PaginadorBottom.PageChanged, AddressOf _Paginador_PageChanged

			_PaginadorTop = TryCast(Me.whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("wdgPaginador"), Paginador)
			AddHandler _PaginadorTop.PageChanged, AddressOf _Paginador_PageChanged

			_PaginadorTop.MostrarBotonExcel = True
			_PaginadorTop.MostrarBotonPdf = True

			whdgFacturas.GridView.Behaviors.Paging.PageSize = ConfigurationManager.AppSettings("PaginacionFacturasLCX")
		End Sub
		Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
			Dim _FSPMServer As PMPortalServer.Root
			_FSPMServer = Session("FS_Portal_Server")

			If IsNothing(_FSPMServer) Then
				Response.Redirect(ConfigurationManager.AppSettings("rutanormal") & "/../../../", False)
				Exit Sub
			End If

			oFacturas = FSPMServer.Get_FacturasLCX
			ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorFacturasLCX
			CargarTextos()
			InicializarPaginador()

			If Me.cbSuplidos.Checked Then
				whdgFacturas.GridView.Columns("SUPLIDO").Hidden = False
			Else
				whdgFacturas.GridView.Columns("SUPLIDO").Hidden = True
			End If

			If Not IsPostBack Then
				InicializacionControles()
				BorrarCacheFacturas()
				cargarComboNifs()
				cargarComboDenEmpresas()
				cargarComboEstados()
				cargarGridFacturas()
				dtFechaFacturaDesde.Value = Today.AddMonths(-13)
				Dim fechaUltima As Date = getUltimaFechaActualizacion.ToString("d")
				If fechaUltima = New Date() Then
					divfechaActualizacion.Visible = False
				Else
					divfechaActualizacion.Visible = True
					sUltimFechaActualizacion.InnerText = getUltimaFechaActualizacion.ToString("d")
				End If
				'Los checks de Mis facturas y Suplidos única y exclusivamente aparecerán en el caso de que existan suplidos en las facturas.
				If oFacturas.haySuplidos(getFiltroBusqueda) Then
					cbMisFacturas.Visible = True
					cbSuplidos.Visible = True
				End If
			Else
				'Si hemos pulsado algunos botones del paginador,el boton buscar o limpiar gestionamos en el propio
				'evento la carga de grid
				Select Case Request("__EVENTARGUMENT")
					Case "Excel"
						Exportar(1)
						Exit Sub
					Case "Pdf"
						Exportar(2)
						Exit Sub
				End Select
			End If

			If Not Page.ClientScript.IsClientScriptBlockRegistered("MensajeCOD1") Then
				Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "MensajeCOD1", "<script>var sMensajeFecDesde = '" & Textos(26) & "'</script>")
			End If
		End Sub
		''' <summary>
		''' Inicializa los componentes y funciones javascript de los elementos
		''' </summary>
		''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
		Private Sub InicializacionControles()
			FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "images/Factura.png"
		End Sub
		Private Sub cargarGridFacturas()
			whdgFacturas.DataSource = Facturas
			whdgFacturas.GridView.DataSource = whdgFacturas.DataSource
			whdgFacturas.DataKeyFields = "ID"
			whdgFacturas.DataBind()
			whdgFacturas.GridView.DataBind()
		End Sub
		''' <summary>
		''' Incicializa los valores del paginador en función del número de registros totales
		''' y lo coloca en una página determinada
		''' </summary>
		''' <param name="pag">página actual del paginador</param>
		''' <remarks></remarks>
		Private Sub InicializarPaginador(Optional pag As Integer = 0)
			Dim numRegistros As Long = 0
			If whdgFacturas.GridView.Behaviors.Filtering.Filtered Then
				whdgFacturas.GridView.Behaviors.Paging.Enabled = False
				numRegistros = whdgFacturas.GridView.Rows.Count
				whdgFacturas.GridView.Behaviors.Paging.Enabled = True
			Else
				If Facturas.Count > 0 Then
					numRegistros = Facturas.Count
				End If
			End If
			_PaginadorBottom.SetContext(numRegistros, ConfigurationManager.AppSettings("PaginacionFacturasLCX"))
			_PaginadorTop.SetContext(numRegistros, ConfigurationManager.AppSettings("PaginacionFacturasLCX"))
			whdgFacturas.GridView.Behaviors.Paging.PageIndex = pag
		End Sub
		''' Revisado por: blp. Fecha: 30/01/2012
		''' <summary>
		''' Método que actualiza el contador de páginas del paginador
		''' </summary>
		''' <param name="sender">Control que lanza el evento PageChanged</param>
		''' <param name="e">argumentos del evento</param>
		''' <remarks>Llamada desde evento PageChanged. Máximo: 0,1 seg.</remarks>
		Private Sub _Paginador_PageChanged(ByVal sender As Object, ByVal e As PageSettingsChangedEventArgs)
			'Actualizamos tb el otro paginador
			Dim oPagSender As Paginador = TryCast(sender, Paginador)
			If oPagSender IsNot Nothing Then
				If oPagSender.ClientID = Me._PaginadorTop.ClientID Then
					_PaginadorBottom.PageNumber = Me._PaginadorTop.PageNumber
					_PaginadorBottom.ActualizarPaginador()
				Else
					_PaginadorTop.PageNumber = Me._PaginadorBottom.PageNumber
					_PaginadorTop.ActualizarPaginador()
				End If
			End If
			whdgFacturas.Behaviors.Paging.PageIndex = _PaginadorTop.PageNumber() - 1
			whdgFacturas.GridView.Behaviors.Paging.PageIndex = _PaginadorTop.PageNumber() - 1
			cargarGridFacturas()
			upFacturas.Update()
		End Sub
		''' Revisado por: blp. Fecha:27/01/2012
		''' <summary>
		''' Método que se lanza al ordenar por una columna en el grid whdgFacturas. Actualiza el paginador y el grid para mostrar los cambios.
		''' </summary>
		''' <param name="sender">El control que lanza el evento</param>
		''' <param name="e">argumentos del evento</param>
		''' <remarks>Llamada desde el evento de filtrado. Máx 1 seg.</remarks>
		Private Sub whdgFacturas_ColumnSorted(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.SortingEventArgs) Handles whdgFacturas.ColumnSorted
			_PaginadorBottom.PageNumber = 1
			_PaginadorTop.PageNumber = 1
			InicializarPaginador()
			cargarGridFacturas()
			upFacturas.Update()
			CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerBottom.FindControl("wdgPaginador").FindControl("upPaginador"), UpdatePanel).Update()
			CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("wdgPaginador").FindControl("upPaginador"), UpdatePanel).Update()
		End Sub
		Private Sub whdgFacturas_DataFiltering(sender As Object, e As Infragistics.Web.UI.GridControls.FilteringEventArgs) Handles whdgFacturas.DataFiltering
			InicializarPaginador()
			cargarGridFacturas()
			Dim columna = e.ColumnFilters(0).ColumnKey
			upFacturas.Update()
		End Sub
		''' <summary>
		''' Tras la carga del grid se "sincroniza" con el Custom Pager
		''' </summary>
		''' <param name="sender">control</param>
		''' <param name="e">evento de sistema</param>  
		''' <remarks>Llamada desde: sistema; Tiempo máximo:0 </remarks>
		Private Sub whdgFacturas_GroupedColumnsChanged(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GroupedColumnsChangedEventArgs) Handles whdgFacturas.GroupedColumnsChanged
			InicializarPaginador()
			Me.cargarGridFacturas()
			upFacturas.Update()
		End Sub
		''' <summary>
		''' Configura la grid para su posterior exportación. Exporta la Grid a una hoja excel o a Pdf
		''' </summary>
		''' <param name="iTipoExportacion"> Tipo Exportacion (Excel = 1 // Pdf = 2)</param>
		''' <remarks>Llamada desde:Page_load; Tiempo ejecucion:0,3seg.</remarks>
		Private Sub Exportar(ByVal iTipoExportacion As Byte)
			cargarGridFacturas()
			whdgFacturas.GridView.Behaviors.Paging.Enabled = False
			If iTipoExportacion = 1 Then 'Excel
				WebExcelExporter1.DownloadName = Date.Now.ToShortDateString & "_" & Textos(0)
				WebExcelExporter1.Export(whdgFacturas.GridView)

			Else 'PDF
				With WebDocumentExporter1
					.TargetPaperOrientation = Infragistics.Documents.Reports.Report.PageOrientation.Landscape
					.DownloadName = Date.Now.ToShortDateString & "_" & Textos(0)
					.Format = Infragistics.Documents.Reports.Report.FileFormat.PDF
					.EnableStylesExport = True
					.DataExportMode = DataExportMode.AllDataInDataSource
					.CustomFont = New System.Drawing.Font("Arial, Verdana", 4, System.Drawing.GraphicsUnit.Pixel)
					.Margins = Infragistics.Documents.Reports.Report.PageMargins.GetPageMargins("Narrow")
					.TargetPaperSize = Infragistics.Documents.Reports.Report.PageSizes.GetPageSize("A4")
					.Export(whdgFacturas.GridView)
				End With
			End If
		End Sub
		Private Sub btnBuscar_Click(sender As Object, e As System.EventArgs) Handles btnBuscar.Click
			buscarFacturas()
			upFacturas.Update()
		End Sub
		Private Sub btnBuscarBusquedaAvanzada_Click(sender As Object, e As System.EventArgs) Handles btnBuscarBusquedaAvanzada.Click
			pnlBusquedaAvanzada.Visible = True
			buscarFacturas()
			upFacturas.Update()
		End Sub
		Private Sub buscarFacturas()
			BorrarCacheFacturas()
			InicializarPaginador()
			cargarGridFacturas()
		End Sub
		Private Sub btnLimpiarBusquedaAvanzada_Click(sender As Object, e As System.EventArgs) Handles btnLimpiarBusquedaAvanzada.Click
			dtFechaFacturaDesde.Value = Today.AddMonths(-13)
			dtFechaFacturaHasta.Value = ""
			dtFechaAbonoDesde.Value = ""
			dtFechaAbonoHasta.Value = ""
			txtImporteDesde.Text = ""
			txtImporteHasta.Text = ""
			wddEstadoFactura.SelectedValue = ""
			wddEstadoFactura.CurrentValue = ""
			txtClave.Text = ""
			upBusquedaAvanzada.Update()
		End Sub
		Private Sub CargarTextos()
			FSNPageHeader.TituloCabecera = Textos(0)
			whdgFacturas.Columns("NUM_FACT").Header.Text = Textos(1)
			lblNumeroFactura.Text = Me.whdgFacturas.Columns("NUM_FACT").Header.Text & " :"
			whdgFacturas.Columns("SUPLIDO").Header.Text = Textos(23)
			whdgFacturas.Columns("NOM_EMP").Header.Text = Textos(12)
			whdgFacturas.Columns("FECHA").Header.Text = Textos(2)
			whdgFacturas.Columns("FECHA_REGISTRO").Header.Text = Textos(3)
			whdgFacturas.Columns("IMPORTE").Header.Text = Textos(5)
			whdgFacturas.Columns("CANAL").Header.Text = Textos(6)
			whdgFacturas.Columns("CUENTA_ABONO").Header.Text = Textos(7)
			whdgFacturas.Columns("FECHA_ABONO").Header.Text = Textos(8)
			whdgFacturas.Columns("SITUACION").Header.Text = Textos(9)
			lblEstadoFactura.Text = Me.whdgFacturas.Columns("SITUACION").Header.Text & " :"
			btnBuscar.Text = Textos(10)
			btnBuscarBusquedaAvanzada.Text = Me.btnBuscar.Text
			btnLimpiarBusquedaAvanzada.Text = Textos(11)
			lblEmpresa.Text = Textos(12) & " :"
			lblCifEmpresa.Text = Textos(4) & " :"
			lblFechaFacturaDesde.Text = Textos(13) & " :"
			lblFechaAbonoHasta.Text = Textos(14) & " :"
			LblImporteDesdeHasta.Text = Textos(15) & " :"
			lblFechaUltimaActualizacion.Text = Textos(16) & " : "
			lblClave.Text = Textos(17) & " :"
			whdgFacturas.Columns("CLAVE").Header.Text = Textos(17)
			cbMisFacturas.Text = Textos(25)
			cbSuplidos.Text = Textos(24)

			lblBusquedaAvanzada.Text = Textos(18)
			whdgFacturas.GroupingSettings.EmptyGroupAreaText = Textos(19)

			upFacturas.Update()
		End Sub
		''' <summary>
		''' Dado que es una página personalizada, en la que la moneda no viene dada por la del usuario, en lugar de modificar FSPMPage para modificar la cultura de la moneda sobreescribimos aquí el método
		''' 
		''' En el fondo en lo q se traduce "página personalizada" es: q no salta la InitializeCulture de FSPMPage tras q se carge el usuario, solo antes de q se carge. 
		''' Por ejemplo, CertificadosVisor si q entra en InitializeCulture de FSPMPage con el usuario cargado, la 2da vez (1ra sin estar cargado).
		''' Esto es lo q reproduce esta función: evento inteno lanza InitializeCulture, FSPMPage carga el usuario y algun evento interno lanza de nuevo el InitializeCulture.
		''' </summary>
		''' <remarks></remarks>
		Protected Overrides Sub InitializeCulture()
			MyBase.InitializeCulture()

			If IsNothing(Me.FSPMUser) Then 'Si esta relleno se hace cargo el FSPMPage
				Dim IPDir As String = Request.ServerVariables("LOCAL_ADDR").ToString
				Dim PersistID As String = Server.UrlDecode(HttpContext.Current.Request.Cookies("SESPP").Value)

				Dim FSPMServer As New Fullstep.PMPortalServer.Root
				Dim oUser As Fullstep.PMPortalServer.User = FSPMServer.Login(Server.UrlDecode(Request.Cookies("USU_SESIONID").Value), IPDir, PersistID)

				If Not IsNothing(oUser) Then
					oUser.LoadUserData(oUser.Cod, oUser.CiaComp)

					Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo(oUser.RefCultural)
					Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalDigits = oUser.NumberFormat.NumberDecimalDigits
					Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator = oUser.NumberFormat.NumberDecimalSeparator
					Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyGroupSeparator = oUser.NumberFormat.NumberGroupSeparator
					Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol = "€"

					Threading.Thread.CurrentThread.CurrentUICulture = Threading.Thread.CurrentThread.CurrentCulture
				End If
			End If
		End Sub
		Private Sub cargarComboNifs()
			Dim item As New DropDownItem
			item.Value = ""
			item.Text = ""
			wddCifEmpresa.Items.Add(item)
			For Each oEmpresa As DataRow In Empresas.Rows
				item = New DropDownItem
				item.Value = oEmpresa("NIF")
				item.Text = oEmpresa("NIF")

				Me.wddCifEmpresa.Items.Add(item)
			Next
			wddCifEmpresa.SelectedValue = ""
		End Sub
		Private Sub cargarComboDenEmpresas()
			Dim item As New DropDownItem
			item.Value = ""
			item.Text = ""
			wddEmpresa.Items.Add(item)
			For Each oEmpresa As DataRow In Empresas.Rows
				item = New DropDownItem
				item.Text = oEmpresa("DEN")
				item.Value = oEmpresa("NIF")

				wddEmpresa.Items.Add(item)
			Next
			wddEmpresa.SelectedValue = ""
		End Sub
		Private Sub cargarComboEstados()
			'En el combo de estados solo cargamos los estados de visor
			Dim item As New DropDownItem
			item.Value = ""
			item.Text = ""
			wddEstadoFactura.Items.Add(item)
			item = New DropDownItem
			item.Text = Textos(20)
			item.Value = lcx_estado_factura_visor.pendiente
			wddEstadoFactura.Items.Add(item)
			item = New DropDownItem
			item.Text = Textos(21)
			item.Value = lcx_estado_factura_visor.anulada
			wddEstadoFactura.Items.Add(item)
			item = New DropDownItem
			item.Text = Textos(22)
			item.Value = lcx_estado_factura_visor.pagada
			wddEstadoFactura.Items.Add(item)

			wddEstadoFactura.SelectedValue = ""
		End Sub
		Private Sub wddEmpresa_SelectionChanged(sender As Object, e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles wddEmpresa.SelectionChanged
			wddCifEmpresa.SelectedValue = wddEmpresa.SelectedValue
			If wddEmpresa.SelectedValue = "" Then
				wddCifEmpresa.CurrentValue = ""
			End If
			Me.upBusqueda.Update()
		End Sub
		Private Sub wddCifEmpresa_SelectionChanged(sender As Object, e As Infragistics.Web.UI.ListControls.DropDownSelectionChangedEventArgs) Handles wddCifEmpresa.SelectionChanged
			wddEmpresa.SelectedValue = wddCifEmpresa.SelectedValue
			If wddCifEmpresa.SelectedValue = "" Then
				wddEmpresa.CurrentValue = ""
			End If
			Me.upBusqueda.Update()
		End Sub
	End Class
End Namespace