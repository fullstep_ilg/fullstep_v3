﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PPL_Facturas.aspx.vb" Inherits="Fullstep.PMPortalWeb.VisorFacturasPPL" %>
<%@ Register TagPrefix="pag" Src="~/script/_common/Paginador.ascx" TagName="Paginador"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<meta http-equiv="expires" content="0"/>	
	<script type="text/javascript" src="../../../../common/formatos.js"></script>
	<script type="text/javascript" src="../../../../common/menu.asp"></script>		
    <script type="text/javascript">dibujaMenu("PPL/PPL_Facturas.aspx");</script>
    <script src="../../../js/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../../js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="../../../js/jquery/jquery.tmpl.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        // <summary>Iniciar la pagina.</summary>     
        // <remarks>Llamada desde: page.onload</remarks>
        function Init() {
            document.getElementById('tablemenu').style.display = 'block';           
        }        
    </script>
</head>
<body class="common" onload="Init()">
    <style type="text/css">
        .celdaImagenWebHierarchical{
	        padding-left:2px!important;
	        border-right:solid 0px transparent!important;	
        }
        .whdgExpansionColumnHidden{
            padding: 0 !important;
            display: none;
            margin-left:0.5em;
        }
        /*---------------*/
        /*COLORES ESTADOS*/
        /*---------------*/
        .EnProcesoAutorizacion {  
            color: #FFA500;                                          
        }
        .EnProcesoPago {
            color:#FFD700;            
        }
        .Devuelta {
            color:#ff0000;            
        }
        .Pagada {
            color: #32CD32;            
        }
        .PagadaAdministraciones {
            color: #006400;            
        }     
        .TextoEstado{
            font-weight:bold;             
        } 
        .TextoConsulta{
            font-size: 12px;
	        color: #AB0020;
	        font-family: Verdana;
        } 
    </style>

    <script type="text/javascript">
        // <summary>Provoca el postback para hacer la esportación a excel</summary>
        // <remarks>Llamada desde: Icono de exportar a Excel</remarks>        
        function MostrarExcel() {
            //__doPostBack("", "Excel");
        }
        // <summary>Limpia los filtros de búsqueda de facturas</summary>
        // <remarks>Llamada desde: Click botón cmdLimpiar</remarks>
        function limpiarFiltros() {
            document.getElementById("selEstado").value = "";
            document.getElementById("txtNFactura").value = "";
            document.getElementById("selSociedad").value = "";
            document.getElementById("txtLote").value = "";
            $find("dteDesdeFecFac").set_value("", true);
            $find("dteDesdeFecFac").set_value("", true);
            $find("dteDesdeFecRecep").set_value("", true);
            $find("dteDesdeFecPago").set_value("", true);
            $find("dteHastaFecFac").set_value("", true);
            $find("dteHastaFecRecep").set_value("", true);
            $find("dteHastaFecPago").set_value("", true);            
        }
        function cargarFacturas() {
            __doPostBack(document.getElementById("btnBuscar").id);           
        }
    </script>
    <form id="form1" runat="server" style="text-align:center">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization ="true"></asp:ScriptManager>

        <div id="Cabecera" style="display:inline-block;width:95%;margin-top:5px;margin-bottom:5px">
            <fsn:FSNPageHeader ID="FSNPageHeader" runat="server" TituloCabecera="DConsulta facturación de proveedores">                
            </fsn:FSNPageHeader>            
        </div>        
        
        <fieldset id="Filtros" style="width:93%; text-align:left; margin-bottom:10px; margin:auto;" class="bordeSimple" runat="server">
            <legend id="lgFiltros" runat="server" class="titulo">Filtros</legend>            
            <div style="display:inline-block;">
                <div style="text-align:left; margin-top:2em;">
                    <span ID="lblEstado" runat="server" style="display:inline-block; width:6em;">DEstado</span>
                    <select ID="selEstado" runat="server" style="width:15em;"></select>
                    <span ID="lblNFactura" runat="server" style="display:inline-block; width:6em; margin-left:2em;">DNº Factura</span>
                    <input type="text" ID="txtNFactura" runat="server" style="width:15em;">                    
                </div>
                <div style="text-align:left;margin-top:1em;">
                    <span ID="lblSociedad" runat="server" style="display:inline-block; width:6em;">DSociedad</span>
                    <select ID="selSociedad" runat="server" style="width:15em"></select>
                    <span ID="lblLote" runat="server" style="display:inline-block; width:6em; margin-left:2em;">DLote</span>
                    <input type="text" ID="txtLote" runat="server" style="width:15em;">                    
                </div>
            </div>
            <div style="display:inline-block;text-align:right;float:right;">
                <span ID="lblFecFac" runat="server" style="display:inline-block; width:10em; margin-left:7em; text-align:left;">DFecha Factura</span>
                <span ID="lblFecRecep" runat="server" style="width:10em; display:inline-block; margin-right:1em; text-align:left;">DFecha Recepción</span>
                <span ID="lblFecPago" runat="server" style="width:7em; display:inline-block; margin-left:1em; text-align:left;">DFecha Pago</span>
                <div style="margin-top:1em;">
                    <span ID="lblDesde" runat="server" style="width:5em; width:4em;display:inline-block;text-align:left;">DDesde</span>
                    <igpck:WebDatePicker runat="server" ID="dteDesdeFecFac" SkinID="Calendario" style="width:10em;margin-left:1em;display:inline-block;"></igpck:WebDatePicker>                    
                    <igpck:WebDatePicker runat="server" ID="dteDesdeFecRecep" SkinID="Calendario" style="width:12em;margin-left:2em;display:inline-block;"></igpck:WebDatePicker>   
                    <igpck:WebDatePicker runat="server" ID="dteDesdeFecPago" SkinID="Calendario" style="width:10em;margin-left:2em;display:inline-block;"></igpck:WebDatePicker>                       
                </div>  
                <div style="margin-top:1em;">
                    <span ID="lblHasta" runat="server" style="width:5em; width:4em;display:inline-block;text-align:left;">DHasta</span>
                    <igpck:WebDatePicker runat="server" ID="dteHastaFecFac" SkinID="Calendario" style="width:10em;margin-left:1em;display:inline-block;"></igpck:WebDatePicker>                    
                    <igpck:WebDatePicker runat="server" ID="dteHastaFecRecep" SkinID="Calendario" style="width:12em;margin-left:2em;display:inline-block;"></igpck:WebDatePicker>   
                    <igpck:WebDatePicker runat="server" ID="dteHastaFecPago" SkinID="Calendario" style="width:10em;margin-left:2em;display:inline-block;"></igpck:WebDatePicker>                        
                </div>              
            </div>
            <div style="margin-top:2em;text-align:right;">
                <fsn:FSNButton id="btnLimpiar" text="DLimpiar" runat="server" OnClientClick="limpiarFiltros(); return false;"/>
                <fsn:FSNButton id="btnBuscar" text="DBuscar" runat="server" style="margin-right:1em;" OnClientClick="cargarFacturas(); return false;"/>                
            </div>
        </fieldset>   

        <div style="text-align:left;width:95%;margin-left:auto;margin-right:auto;margin-top:1em;margin-bottom:0.5em;">
            <span id="lblFacturasDe" runat="server" class="titulo">DFacturas de ...</span>
        </div>

        <div id="Facturas" style="display:inline-block;width:95%">
            <asp:UpdatePanel runat="server" ID="upFacturas" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>                    
                    <ig:WebHierarchicalDataGrid ID="whdgFacturas" runat="server" AutoGenerateColumns="false" Width="100%" ShowHeader="true" EnableAjax="true" EnableAjaxViewState="false" EnableDataViewState="false" 
                        ExpansionColumnCss="whdgExpansionColumnHidden">
                        <Columns>
                            <ig:BoundDataField Key="ID" DataFieldName="ID"  Hidden="true" Width="20px"></ig:BoundDataField>
                            <ig:BoundDataField Key="IdEstadoWebProveedor" DataFieldName="IdEstadoWebProveedor" Hidden="true" Width="20px"></ig:BoundDataField>                            
                            <ig:BoundDataField Key="NumFactura" DataFieldName="NumFactura" Width="110px"></ig:BoundDataField>
                            <ig:BoundDataField Key="Fecha" DataFieldName="Fecha" DataFormatString ="{0:d}" Width="80px"></ig:BoundDataField>
                            <ig:BoundDataField Key="Sociedad" DataFieldName="Sociedad" Width="100px"></ig:BoundDataField>
                            <ig:BoundDataField Key="Total" DataFieldName="Total" CssClass ="CeldaNumericaDataGrid" Width="80px"></ig:BoundDataField>
                            <ig:UnboundField Key="ImgEstadoWebProveedor" Width="20px" CssClass ="celdaImagenWebHierarchical" Header-CssClass="celdaImagenWebHierarchical"></ig:UnboundField>
                            <ig:BoundDataField Key="EstadoWebProveedorDen" DataFieldName="EstadoWebProveedorDen" Width="150px"></ig:BoundDataField>
                            <ig:BoundDataField Key="FechaPago" DataFieldName="FechaPago" DataFormatString ="{0:d}" Width="80px"></ig:BoundDataField>
                            <ig:BoundDataField Key="Lote" DataFieldName="Lote" Width="100px"></ig:BoundDataField>
                            <ig:BoundDataField Key="FechaEntrada" DataFieldName="FechaEntrada" DataFormatString ="{0:d}" Width="80px"></ig:BoundDataField>  
                            <ig:BoundDataField Key="FechaEstado" DataFieldName="FechaEstado" DataFormatString ="{0:d}" Width="80px"></ig:BoundDataField>                                                                                                                                              
                            <ig:BoundDataField Key="Pedido" DataFieldName="Pedido" Width="150px"></ig:BoundDataField> 
                            <ig:BoundDataField Key="FechaVencimiento" DataFieldName="FechaVencimiento" DataFormatString ="{0:d}" Width="80px"></ig:BoundDataField>                                     
                            <ig:BoundDataField Key="Nominal" DataFieldName="Nominal" CssClass ="CeldaNumericaDataGrid" Width="80px"></ig:BoundDataField>
                            <ig:BoundDataField Key="Impuestos" DataFieldName="Impuestos" CssClass ="CeldaNumericaDataGrid" Width="80px"></ig:BoundDataField>
                            <ig:BoundDataField Key="Reparto" DataFieldName="Reparto" Width="50px"></ig:BoundDataField>
                        </Columns>
                        <Behaviors >                                        
                            <ig:Activation Enabled="true"></ig:Activation>                                        
                            <ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
                            <ig:Paging Enabled="true" PagerAppearance="Both">
                                <PagerTemplate>
                                    <pag:Paginador id="wdgPaginador" runat="server"  />
                                </PagerTemplate>
                            </ig:Paging >
                            <ig:VirtualScrolling Enabled="false"></ig:VirtualScrolling>
                            <ig:Sorting Enabled="true" SortingMode="Multi" >                         
                            </ig:Sorting>                            
                        </Behaviors>                                
                    </ig:WebHierarchicalDataGrid>                                   
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div style="margin-top:2em; margin-bottom:3em; text-align:left; display:inline-block; width:95%"><span id="txtConsulta" runat="server" class="TextoConsulta">DPara cualquier consulta o aclaración ...</span></div>        
        <ig:WebExcelExporter ID ="WebExcelExporter1" runat="server"></ig:WebExcelExporter>
    </form>
</body>
</html>
