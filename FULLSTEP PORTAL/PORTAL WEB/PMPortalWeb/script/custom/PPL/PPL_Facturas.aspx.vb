﻿Imports Fullstep.PMPortalServer
Imports Infragistics.Web.UI.GridControls

Namespace Fullstep.PMPortalWeb
    Public Class VisorFacturasPPL
        Inherits FSPMPage

        Private Enum EstadosFacturas_PPL
            EnProcesoAutorizacion = 1
            EnProcesoPago = 2
            Devuelta = 3
            Pagada = 4
            PagadaAdministraciones = 5
        End Enum

        Private _Facturas As List(Of PPL_Factura)
        Private _EstadosFacturas As List(Of Object)
        Private _Sociedades As List(Of String)
        Private _PaginadorBottom As Paginador
        Private _PaginadorTop As Paginador

        Private ReadOnly Property Facturas() As List(Of PPL_Factura)
            Get
                If _Facturas Is Nothing Then
                    If HttpContext.Current.Cache("FacturasPPL_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString) IsNot Nothing Then
                        _Facturas = HttpContext.Current.Cache("FacturasPPL_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString)
                    Else
                        Dim iEstado As Integer
                        If selEstado.Value <> String.Empty Then iEstado = EstadosFacturas.Find(Function(o As Object) (o.Den = selEstado.Value)).Id

                        Dim oFacturas As PPL_Facturas = FSPMServer.Get_FacturasPPL
                        _Facturas = oFacturas.cargarTodasLasFacturas(New With {.NIFCia = FSPMUser.NIFCia,
                                                                               .Idioma = FSPMUser.Idioma,
                                                                               .Estado = iEstado,
                                                                               .NumFactura = txtNFactura.Value,
                                                                               .Sociedad = selSociedad.Value,
                                                                               .Lote = txtLote.Value,
                                                                               .FecFacturaDesde = dteDesdeFecFac.Value,
                                                                               .FecFacturaHasta = dteHastaFecFac.Value,
                                                                               .FecRecepDesde = dteDesdeFecRecep.Value,
                                                                               .FecRecepHasta = dteHastaFecRecep.Value,
                                                                               .FecPagoDesde = dteDesdeFecPago.Value,
                                                                               .FecPagoHasta = dteHastaFecPago.Value})
                        Me.InsertarEnCache("FacturasPPL_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString, _Facturas)
                    End If
                End If
                Return _Facturas
            End Get
        End Property
        Private ReadOnly Property EstadosFacturas As List(Of Object)
            Get
                If _EstadosFacturas Is Nothing Then
                    If HttpContext.Current.Cache("EstadosFacturasPPL_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString) IsNot Nothing Then
                        _EstadosFacturas = HttpContext.Current.Cache("EstadosFacturasPPL_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString)
                    Else
                        Dim oFacturas As PPL_Facturas = FSPMServer.Get_FacturasPPL
                        _EstadosFacturas = oFacturas.devolverEstadosFacturas(FSPMUser.Idioma)
                        Me.InsertarEnCache("EstadosFacturasPPL_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString, _EstadosFacturas)
                    End If
                End If
                Return _EstadosFacturas
            End Get
        End Property
        Private ReadOnly Property Sociedades As List(Of String)
            Get
                If _Sociedades Is Nothing Then
                    If HttpContext.Current.Cache("SociedadesPPL_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString) IsNot Nothing Then
                        _Sociedades = HttpContext.Current.Cache("SociedadesPPL_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString)
                    Else
                        Dim oFacturas As PPL_Facturas = FSPMServer.Get_FacturasPPL
                        _Sociedades = oFacturas.devolverSociedades(FSPMUser.NIFCia)
                        Me.InsertarEnCache("SociedadesPPL_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString, _Sociedades)
                    End If
                End If
                Return _Sociedades
            End Get
        End Property
#Region "Page events"
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorFacturasPPL

            CargarTextos()
            ConfigurarControlesFecha()

            If Not Me.IsPostBack Then
                BorrarCacheFacturas()
                InicializarPaginador()
                CargarComboEstados()
                CargarComboSociedades()
                CargarFacturas()
            Else
                If Request("__EVENTTARGET") = btnBuscar.ClientID Then
                    BorrarCacheFacturas()
                    InicializarPaginador()
                    _PaginadorBottom.PageNumber = 1
                    _PaginadorBottom.ActualizarPaginador()
                    _PaginadorTop.PageNumber = 1
                    _PaginadorTop.ActualizarPaginador()
                    CargarFacturas()
                ElseIf Request("__EVENTARGUMENT") = "Excel" Then
                    Exportar()
                End If
            End If
        End Sub
        Private Sub VisorFacturasPPL_Init(sender As Object, e As EventArgs) Handles Me.Init
            _PaginadorBottom = TryCast(Me.whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerBottom.FindControl("wdgPaginador"), Paginador)
            AddHandler Me._PaginadorBottom.PageChanged, AddressOf _Paginador_PageChanged

            _PaginadorTop = TryCast(Me.whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("wdgPaginador"), Paginador)
            AddHandler Me._PaginadorTop.PageChanged, AddressOf _Paginador_PageChanged

            _PaginadorTop.MostrarBotonExcel = True

            whdgFacturas.GridView.Behaviors.Paging.PageSize = ConfigurationManager.AppSettings("PaginacionFacturas")
        End Sub
#End Region
        ''' <summary>Configura la grid para su posterior exportación. Exporta la Grid a una hoja excel</summary>        
        ''' <remarks>Llamada desde:Page_load; Tiempo ejecucion:0,3seg.</remarks>
        Private Sub Exportar()
            CargarFacturas()
            whdgFacturas.GridView.Behaviors.Paging.Enabled = False

            WebExcelExporter1.DownloadName = Date.Now.ToShortDateString & "_" & Textos(0)
            WebExcelExporter1.Export(whdgFacturas.GridView)
        End Sub
        ''' <summary>Borra todas las facturas cacheadas</summary>
        ''' <remarks>>Llamada desde: Form_Load</remarks>
        Private Sub BorrarCacheFacturas()
            If HttpContext.Current.Cache("FacturasPPL_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString) IsNot Nothing Then _
                    HttpContext.Current.Cache.Remove("FacturasPPL_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString)
            _Facturas = Nothing
        End Sub
        ''' <summary>Carga los textos de la página</summary>
        ''' <remarks>Llamada desde: Page_Load</remarks>
        Private Sub CargarTextos()
            With whdgFacturas
                FSNPageHeader.TituloCabecera = Textos(0)
                lgFiltros.InnerText = Textos(1)
                lblEstado.InnerText = Textos(2)
                lblNFactura.InnerText = Textos(4)
                lblSociedad.InnerText = Textos(3)
                lblLote.InnerText = Textos(5)
                lblFecFac.InnerText = Textos(8)
                lblFecRecep.InnerText = Textos(9)
                lblFecPago.InnerText = Textos(10)
                lblDesde.InnerText = Textos(6)
                lblHasta.InnerText = Textos(7)
                lblFacturasDe.InnerText = Textos(11) & " " & FSPMUser.NIFCia & " - " & FSPMUser.DenCia
                .Columns("NumFactura").Header.Text = Textos(4)
                .Columns("Fecha").Header.Text = Textos(8)
                .Columns("Sociedad").Header.Text = Textos(3)
                .Columns("Total").Header.Text = Textos(14)
                .Columns("EstadoWebProveedorDen").Header.Text = Textos(2)
                .Columns("FechaPago").Header.Text = Textos(10)
                .Columns("Lote").Header.Text = Textos(5)
                .Columns("FechaEntrada").Header.Text = Textos(9)
                .Columns("FechaEstado").Header.Text = Textos(15)
                .Columns("Pedido").Header.Text = Textos(16)
                .Columns("FechaVencimiento").Header.Text = Textos(17)
                .Columns("Nominal").Header.Text = Textos(18)
                .Columns("Impuestos").Header.Text = Textos(19)
                .Columns("Reparto").Header.Text = Textos(20)
                btnBuscar.Text = Textos(12)
                btnLimpiar.Text = Textos(21)
                txtConsulta.InnerText = Textos(22)
            End With
        End Sub
        ''' <summary>Carga el combo de estados de factura</summary>
        ''' <remarks>Llamada desde: Form_Load</remarks>
        Private Sub CargarComboEstados()
            selEstado.Items.Insert(0, String.Empty)
            Dim i As Integer = 1
            For Each oEstado As Object In EstadosFacturas
                selEstado.Items.Insert(i, oEstado.Den)
                i += 1
            Next
        End Sub
        ''' <summary>Carga el combo de sociedades</summary>
        ''' <remarks>Llamada desde: Form_Load</remarks>
        Private Sub CargarComboSociedades()
            selSociedad.Items.Insert(0, String.Empty)
            Dim i As Integer = 1
            For Each sSoc As String In Sociedades
                selSociedad.Items.Insert(i, sSoc)
                i += 1
            Next
        End Sub
        ''' <summary>Configura los controles de fecha con el formato de fecha del usuario</summary>
        ''' <remarks>Llamada desde: Form_Load</remarks>
        Private Sub ConfigurarControlesFecha()
            dteDesdeFecFac.DisplayModeFormat = FSPMUser.DateFormat.ShortDatePattern
            dteDesdeFecFac.EditModeFormat = FSPMUser.DateFormat.ShortDatePattern
            dteDesdeFecRecep.DisplayModeFormat = FSPMUser.DateFormat.ShortDatePattern
            dteDesdeFecRecep.EditModeFormat = FSPMUser.DateFormat.ShortDatePattern
            dteDesdeFecPago.DisplayModeFormat = FSPMUser.DateFormat.ShortDatePattern
            dteDesdeFecPago.EditModeFormat = FSPMUser.DateFormat.ShortDatePattern
            dteHastaFecFac.DisplayModeFormat = FSPMUser.DateFormat.ShortDatePattern
            dteHastaFecFac.EditModeFormat = FSPMUser.DateFormat.ShortDatePattern
            dteHastaFecRecep.DisplayModeFormat = FSPMUser.DateFormat.ShortDatePattern
            dteHastaFecRecep.EditModeFormat = FSPMUser.DateFormat.ShortDatePattern
            dteHastaFecPago.DisplayModeFormat = FSPMUser.DateFormat.ShortDatePattern
            dteHastaFecPago.EditModeFormat = FSPMUser.DateFormat.ShortDatePattern
        End Sub
        ''' <summary>
        ''' Incicializa los valores del paginador en función del número de registros totales
        ''' y lo coloca en una página determinada
        ''' </summary>
        ''' <param name="pag">página actual del paginador</param>
        ''' <remarks></remarks>
        Private Sub InicializarPaginador(Optional pag As Integer = 0)
            Dim numRegistros As Long = 0
            If Facturas.Count > 0 Then numRegistros = Facturas.Count

            _PaginadorBottom.SetContext(numRegistros, ConfigurationManager.AppSettings("PaginacionFacturas"))
            _PaginadorTop.SetContext(numRegistros, ConfigurationManager.AppSettings("PaginacionFacturas"))
            whdgFacturas.GridView.Behaviors.Paging.PageIndex = pag
        End Sub
        ''' <summary>Método que actualiza el contador de páginas del paginador</summary>
        ''' <param name="sender">Control que lanza el evento PageChanged</param>
        ''' <param name="e">argumentos del evento</param>
        ''' <remarks>Llamada desde evento PageChanged. Máximo: 0,1 seg.</remarks>
        Private Sub _Paginador_PageChanged(ByVal sender As Object, ByVal e As PageSettingsChangedEventArgs)
            'Actualizamos tb el otro paginador
            Dim oPagSender As Paginador = TryCast(sender, Paginador)
            If oPagSender IsNot Nothing Then
                If oPagSender.ClientID = _PaginadorTop.ClientID Then
                    _PaginadorBottom.PageNumber = _PaginadorTop.PageNumber
                    _PaginadorBottom.ActualizarPaginador()
                Else
                    _PaginadorTop.PageNumber = _PaginadorBottom.PageNumber
                    _PaginadorTop.ActualizarPaginador()
                End If
            End If
            whdgFacturas.Behaviors.Paging.PageIndex = _PaginadorTop.PageNumber() - 1
            whdgFacturas.GridView.Behaviors.Paging.PageIndex = _PaginadorTop.PageNumber() - 1
            CargarFacturas()
            InicializarPaginador(whdgFacturas.Behaviors.Paging.PageIndex)
            upFacturas.Update()
        End Sub
        ''' <summary>Carga las facturas en el grid</summary>
        Private Sub CargarFacturas()
            whdgFacturas.DataSource = Facturas
            whdgFacturas.GridView.DataSource = whdgFacturas.DataSource
            whdgFacturas.DataKeyFields = "ID"
            whdgFacturas.DataBind()
            whdgFacturas.GridView.DataBind()
        End Sub

        Private Sub whdgFacturas_ColumnSorted(sender As Object, e As SortingEventArgs) Handles whdgFacturas.ColumnSorted
            _PaginadorBottom.PageNumber = 1
            _PaginadorTop.PageNumber = 1
            InicializarPaginador()
            CargarFacturas()
            upFacturas.Update()
            CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerBottom.FindControl("wdgPaginador").FindControl("upPaginador"), UpdatePanel).Update()
            CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("wdgPaginador").FindControl("upPaginador"), UpdatePanel).Update()
        End Sub

        Private Sub whdgFacturas_InitializeRow(sender As Object, e As RowEventArgs) Handles whdgFacturas.InitializeRow
            Dim oFactura As PPL_Factura = e.Row.DataItem.Item

            If oFactura.Total.HasValue Then
                e.Row.Items(whdgFacturas.GridView.Columns.FromKey("Total").Index).Text = FSNLibrary.FormatNumber(oFactura.Total, FSPMUser.NumberFormat) & " €"
            Else
                e.Row.Items(whdgFacturas.GridView.Columns.FromKey("Total").Index).Text = ""
            End If
            If oFactura.Nominal.HasValue Then
                e.Row.Items(whdgFacturas.GridView.Columns.FromKey("Nominal").Index).Text = FSNLibrary.FormatNumber(oFactura.Nominal, FSPMUser.NumberFormat) & " €"
            Else
                e.Row.Items(whdgFacturas.GridView.Columns.FromKey("Nominal").Index).Text = ""
            End If
            If oFactura.Impuestos.HasValue Then
                e.Row.Items(whdgFacturas.GridView.Columns.FromKey("Impuestos").Index).Text = FSNLibrary.FormatNumber(oFactura.Impuestos, FSPMUser.NumberFormat) & " €"
            Else
                e.Row.Items(whdgFacturas.GridView.Columns.FromKey("Impuestos").Index).Text = ""
            End If
            Dim sImg As String = String.Empty
            Dim sEstiloTexto As String = String.Empty
            Select Case oFactura.IdEstadoWebProveedor
                Case EstadosFacturas_PPL.EnProcesoAutorizacion
                    sImg = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_EnProcesoAutorizacion.gif" & "'/>"
                    sEstiloTexto = "EnProcesoAutorizacion"
                Case EstadosFacturas_PPL.EnProcesoPago
                    sImg = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_EnProcesoPago.gif" & "'/>"
                    sEstiloTexto = "EnProcesoPago"
                Case EstadosFacturas_PPL.Devuelta
                    sImg = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Devuelta.gif" & "'/>"
                    sEstiloTexto = "Devuelta"
                Case EstadosFacturas_PPL.Pagada
                    sImg = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_Pagada.gif" & "'/>"
                    sEstiloTexto = "Pagada"
                Case EstadosFacturas_PPL.PagadaAdministraciones
                    sImg = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("ruta") & "App_themes/" & Page.Theme & "/images/Ico_PagadaAdministraciones.gif" & "'/>"
                    sEstiloTexto = "PagadaAdministraciones"
            End Select
            e.Row.Items(whdgFacturas.GridView.Columns.FromKey("ImgEstadoWebProveedor").Index).Text = sImg
            e.Row.Items(whdgFacturas.GridView.Columns.FromKey("EstadoWebProveedorDen").Index).CssClass = "TextoEstado " & sEstiloTexto
        End Sub
    End Class
End Namespace