Namespace Fullstep.PMPortalWeb
    Partial Class login
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Dim UserName As String = Request("txtUSU")
            Dim FSPMServer As New Fullstep.PMPortalServer.Root
            Dim oUser As Fullstep.PMPortalServer.User
            Dim lCiaComp As Long = IdCiaComp
            Dim IPDir As String = Request.ServerVariables("LOCAL_ADDR").ToString
            Dim PersistID As String = HttpContext.Current.Request.Cookies("SESPP").ToString

            Session.Clear()
            oUser = FSPMServer.Login(Request.Cookies("USU_SESIONID").Value, IPDir, PersistID)
            If oUser Is Nothing Then
                Response.Redirect(ConfigurationManager.AppSettings("rutanormal"), False)
            Else
                oUser.LoadUserData(UserName, lCiaComp)
                Session("FS_Portal_User") = oUser
                Session("FS_Portal_Server") = FSPMServer
                HttpContext.Current.User = Session("FS_Portal_User")
                Response.Redirect("../frames.aspx", False)
            End If
        End Sub

    End Class
End Namespace