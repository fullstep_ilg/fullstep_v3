<%@ Page Language="vb" AutoEventWireup="false" Codebehind="login.aspx.vb" Inherits="Fullstep.PMPortalWeb.fulldemo.login.spa.login" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="estilos.txt" rel="stylesheet" type="text/css">
			<style type="text/css"> .subtit { font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 9px; font-style: normal; font-weight: normal; color: #FFFFFF; text-decoration: none}
	.titulo { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-weight: bold; color: #6D93A2; text-decoration: none}
	.subtitulo { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-style: normal; font-weight: normal; color: #6D93A2; text-decoration: none}
	.textos { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-weight: normal; color: #666666; text-decoration: none}
	.registro { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-style: normal; font-weight: bold; color: #666666; text-decoration: none}
	.subtit { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; font-style: normal; font-weight: normal; color: #FFFFFF; text-decoration: none}
	.formulario { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; font-style: normal; font-weight: normal; color: #b70202; text-decoration: none}
	INPUT {font-family:"Arial";font-size:10px;}
	a:link {text-decoration:none;color:#DA1F26}
	a:visited {text-decoration:none;color:#666666}
	a:active {text-decoration:none;color:#DA1F26}
	a:hover { color: #DA1F26; text-decoration: none}
	body { margin-left: 5px; margin-top: 0px; }
	</style>
			<script language="JavaScript" type="text/JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function ventanaLogin (IDI){

   window.open ("registro.asp?Idioma="+IDI,"","width=700,height=400,resizable=yes")

}
function ventanaSecundaria (URL){

   window.open(URL,"ventana1","width=641,height=400,scrollbars=NO")

}

function recuerdePWD()
{
   window.open("recuerdo.asp","_blank","width=641,height=280,scrollbars=NO")
}
//-->
			</script>
	</head>
	<body onLoad="MM_preloadImages('../images/manuales2_r.gif','../images/imagen%20_chica.jpg','../images/imagen%20_carros.jpg','../images/imagen%20_mundo.jpg','../images/imagen_cuadro.gif','../images/deutsch_r.gif','../images/english_r.gif','../images/contact_r.gif','../images/entrar2f_spa.gif','../images/case%20studies_r.gif','../images/contacto2_r.gif','../images/espa�ol_r.gif','../images/english_r.gif');MM_preloadImages('../images/aviso%20legal_r.gif')">
		<form name="frmLogin" id="frmLogin" method="post" action="../../login.aspx">
			<input type="hidden" runat=server id="IdOrden" name="IdOrden" value="<%=IdOrden%>">
			<input type="hidden" runat=server id="CiaComp" name="CiaComp" value="<%=CiaComp%>">
			<table width="795" border="0" align="center" cellpadding="0" cellspacing="0">
				<!--DWLayoutTable-->
				<tr>
					<td height="89"><img src="../images/logo.jpg" name="Image5" width="150" height="89" id="Image5"></td>
					<td colspan="3" align="right" valign="bottom"><table width="145" border="0" align="right" cellpadding="0" cellspacing="0">
							<tr>
								<td><a href="javascript:ventanaSecundaria('../../../App_Themes/fulldemo/public/manuales.htm')"
										onMouseOver="MM_swapImage('Image8','','../images/manuales2_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../images/manuales2.gif" name="Image8" height="10" border="0" id="Image8"></a></td>
								<td><a href="mailto:compras@fullstep.com" onMouseOver="MM_swapImage('Image2','','../images/contacto2_r.gif',1)"
										onMouseOut="MM_swapImgRestore()"><img src="../images/contacto2.gif" name="Image2" height="10" border="0" id="Image2"></a></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" align="center"><img src="../images/linearoja.gif" width="155" height="1"></td>
					<td colspan="3"><img src="../images/linearoja.gif" width="645" height="1"></td>
				</tr>
				<tr>
					<td width="161" height="19"></td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td height="145" valign="top"><img src="../images/imagen%20_mundo.jpg" name="Image9" width="145" height="145" border="0"
							usemap="default.asp#Map" id="Image9"></td>
					<td colspan="3" rowspan="2" valign="top"><table width="634" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="20">&nbsp;</td>
								<td align="left"><img src="../images/portal.gif" height="29"></td>
							</tr>
							<tr>
								<td height="223"></td>
								<td width="634" rowspan="2"><table width="638" border="0" cellspacing="0" cellpadding="0">
										<!--DWLayoutTable-->
										<tr>
											<td height="19" colspan="2" align="right"><div align="left"><span class="textos"><br>
													</span></div>
											</td>
											<td align="right">&nbsp;</td>
											<td><div align="center"></div>
											</td>
										</tr>
										<tr>
											<td width="350" rowspan="3" valign="top"><font color="#666666" size="1" face="Verdana" class="textos">Como 
													compa��a especializada en el �rea de compras y aprovisionamiento, en FULLSTEP 
													trabajamos para conseguir una relaci�n fluida y eficiente entre proveedores y 
													compradores.<br>
													<br>
													Para ello ponemos a disposici�n de nuestros proveedores un canal de 
													comunicaci�n con el departamento de compras c�modo, transparente y �gil, 
													garantizando una privacidad total en la gesti�n de los procesos de compras. 
													Todo ello avalado por la s�lida experiencia de un equipo de profesionales 
													altamente cualificados.<br>
													<br>
													Nuestra participaci�n en grandes proyectos a nivel europeo ha supuesto la 
													mejora e implantaci�n de nuevos sistemas de compras en compa��as l�deres en su 
													sector: Campofr�o Alimentaci�n, Ficosa, Uni�n Fenosa, Grupo PRISA, Lafarge 
													Asland, Cooperativa Mondrag�n, Grupo Log�stico Santos, etc. </font>
											</td>
											<td width="5" height="14"></td>
											<td width="4" rowspan="3" align="right"></td>
											<td align="left" bgcolor="#e4e4e4"><div align="center"><img src="../images/proveedores_r.gif" alt="introduzca sus claves de acceso" name="Image4"
														width="150" height="14" border="0" id="Image4">
												</div>
											</td>
										</tr>
										<tr>
											<td height="133">&nbsp;</td>
											<td align="left" bgcolor="#e4e4e4"><div align="center">
													<table width="173" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td width="16">&nbsp;</td>
															<td width="67" class="formulario">Compa��a</td>
															<td width="80">
																<input id="txtCia" name="txtCIA" maxlength="20" size="10">
															</td>
															<td width="10">&nbsp;</td>
														</tr>
														<tr>
															<td width="16">&nbsp;</td>
															<td width="67" class="formulario">C�d.Usuario</td>
															<td width="80">
																<input name="txtUSU" maxlength="20" size="10">
															</td>
															<td width="10">&nbsp;</td>
														</tr>
														<tr>
															<td width="16">&nbsp;</td>
															<td width="67" class="formulario">Contrase�a</td>
															<td width="80">
																<input name="txtPWD" type="password" maxlength="20" size="10">
															</td>
															<td width="10">&nbsp;</td>
														</tr>
														<tr>
															<td width="16">&nbsp;</td>
															<td width="67">&nbsp;</td>
															<td width="80"><div align="center">
																	<input type="hidden" name="cmdEntrar" value="Entrar"> <input type="hidden" name="txtEntrar" value="Entrar">
																	<input type="image" name="imgEntrar" value="Entrar" src="../images/entrar2_spa.gif" WIDTH="50" HEIGHT="20">
																</div>
															</td>
														</tr>
														<tr>
															<td colspan="3" align="center">
																<a class="formulario" href="javascript:void(null)" onclick="recuerdePWD()"><u>�Olvid� 
																		sus claves de acceso?</u></a>
															</td>
															<td width="10">&nbsp;</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
										<tr>
											<td height="74">&nbsp;</td>
											<td align="left" bgcolor="#e4e4e4"><table width="275" cellspacing="0" cellpadding="0">
													<tr>
														<td width="273"><div align="left" class="textos">
																<div align="center">�Tiene alguna duda?
																	<br>
																	Ll�menos al 912 962 000</div>
															</div>
														</td>
													</tr>
													<tr>
														<td height="37"><div align="left" class="registro">
																<div align="center" class="textogris"><a href="javascript:ventanaLogin('SPA')">solicitar 
																		registro </a>
																</div>
															</div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="161" height="107" valign="top"><table width="155" cellspacing="0" cellpadding="0">
							<tr>
								<td width="10">&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td><a href="eng/login.asp" onMouseOver="MM_swapImage('Image11','','../images/english_r.gif',1)"
										onMouseOut="MM_swapImgRestore()"><img src="../images/english.gif" name="Image11" width="64" height="12" border="0" id="Image11"></a></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td><a href="ger/login.asp" onMouseOver="MM_swapImage('Image21','','../images/deutsch_r.gif',1)"
										onMouseOut="MM_swapImgRestore()"><img src="../images/deutsch.gif" name="Image21" width="64" height="12" border="0" id="Image21"></a></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="35" colspan="3" align="center"><div align="right"><a href="javascript:ventanaSecundaria('../../../App_Themes/fulldemo/public/aviso legal.htm')"
								onMouseOver="MM_swapImage('Image3','','../images/aviso%20legal_r.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="../images/aviso%20legal.gif" name="Image3" width="77" height="10" border="0"
									id="Image3"></a></div>
					</td>
				</tr>
				<tr>
					<td width="161" height="40" bgcolor="#e4e4e4" class="subtitulo"><div align="left">FULLSTEP 
							NETWORKS, S.L.<br>
							Antonio de Cabez�n, 83, 4�
							<br>
							28034 Madrid (Espa�a)</div>
					</td>
					<td width="303" valign="top" bgcolor="#e4e4e4" class="subtitulo"><div align="center"><img src="../images/slogan.gif" WIDTH="241" HEIGHT="40"></div>
					</td>
					<td width="330" bgcolor="#e4e4e4" class="subtitulo"><div align="left">Internet explorer 
							5.0 o superior requerido.<br>
							Para conseguir la �ltima versi�n,<a href="http://www.microsoft.com/windows/ie_intl/es/default.mspx" target="_blank">
								pulse aqu�</a>.<br>
						</div>
					</td>
				</tr>
				<tr>
					<td height="20" colspan="4" bgcolor="#6d93a2" class="registro">
						<div align="center"><img src="../images/copyright.gif" width="260" height="14"></div>
						<div align="center">
						</div>
					</td>
				</tr>
			</table>
			<map name="Map">
				<area shape="RECT" coords="72,3,145,74" alt="�alguna duda? ll�menos al 912 962 000" onMouseOver="MM_swapImage('Image9','','../images/imagen%20_chica.jpg',1)"
					onMouseOut="MM_swapImgRestore()">
				<area shape="RECT" coords="2,1,73,72" onMouseOver="MM_swapImage('Image9','','../images/imagen%20_carros.jpg',1)"
					onMouseOut="MM_swapImgRestore()">
				<area shape="RECT" coords="1,73,72,144" onMouseOver="MM_swapImage('Image9','','../images/imagen%20_mundo.jpg',1)"
					onMouseOut="MM_swapImgRestore()">
				<area shape="RECT" coords="73,73,146,143" onMouseOver="MM_swapImage('Image9','','../images/imagen_cuadro.gif',1)"
					onMouseOut="MM_swapImgRestore()">
			</map>
		</form>
	</body>
</html>
