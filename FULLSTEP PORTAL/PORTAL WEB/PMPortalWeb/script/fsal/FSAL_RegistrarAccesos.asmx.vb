﻿Imports System.Configuration
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Threading

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class FSAL_RegistrarAccesos
    Inherits System.Web.Services.WebService
    Private m_PMPortalServer As Fullstep.PMPortalServer.FSAL
    Private m_sErrorFile_FSAL = ConfigurationManager.AppSettings("logtemp") & "\log_FSALErrores.txt"

    <WebMethod()>
    Public Sub FSALRegistrarAccesos(Tipo As String, Producto As String, fechapet As String, fecha As String, ByVal pagina As String, ByVal iPost As String, ByVal iP As String, ByVal sUsuCod As String, ByVal sPaginaOrigen As String, ByVal sNavegador As String, ByVal IdRegistro As String, ByVal sProveCod As String, ByVal sQueryString As String, ByVal sNormalizarFechas As String)

        Try
            Dim iTipo As Int16
            Dim iPostBack As Int16

            If Tipo.Split(",").Length > 1 Then
                Tipo = Tipo.Split(",")(0)
            End If
            If IsNumeric(Tipo) Then
                iTipo = CShort(Tipo)
            Else
                iTipo = 1
            End If

            If iPost.Split(",").Length > 1 Then
                iPost = iPost.Split(",")(0)
            End If
            If IsNumeric(iPost) Then
                iPostBack = CShort(iPost)
            Else
                iPostBack = 0
            End If

            fechapet = fechapet.Split(".")(0) & "." & Format(CInt(fechapet.Split(".")(1)), "000")
            fecha = fecha.Split(".")(0) & "." & Format(CInt(fecha.Split(".")(1)), "000")

            If sNormalizarFechas = "1" Then   ''las llamadas que traen fechas de cliente hay que normalizarlas respecto a las del servidor
                Dim dtFechaServer As DateTime
                Dim dtFechaPet As DateTime
                Dim dtFecha As DateTime
                Dim dDiffMsIniFinCli As Double

                dtFechaPet = DateTime.ParseExact(fechapet, "yyyy-MM-dd HH:mm:ss.fff", System.Globalization.CultureInfo.CurrentCulture)
                dtFecha = DateTime.ParseExact(fecha, "yyyy-MM-dd HH:mm:ss.fff", System.Globalization.CultureInfo.CurrentCulture)

                dDiffMsIniFinCli = dtFecha.Subtract(dtFechaPet).TotalMilliseconds * -1

                dtFechaServer = DateTime.UtcNow
                dtFecha = dtFechaServer
                dtFechaPet = dtFecha.AddMilliseconds(dDiffMsIniFinCli)

                fechapet = Format(dtFechaPet, "yyyy-MM-dd HH:mm:ss") & "." & Format(dtFechaPet.Millisecond, "000")
                fecha = Format(dtFecha, "yyyy-MM-dd HH:mm:ss") & "." & Format(dtFecha.Millisecond, "000")
            End If

            m_PMPortalServer = New Fullstep.PMPortalServer.FSAL
            m_PMPortalServer.RegistrarAcceso(iTipo, Producto, fechapet, fecha, pagina, iPostBack, iP, sUsuCod, sPaginaOrigen, sNavegador, IdRegistro, sProveCod, sQueryString)

        Catch ex As Exception

            Dim sMensaje As String = "Error WebService de PORTAL, Sub FSALRegistrarAccesos(): " & ex.Message
            EscribirError(sMensaje)

        End Try

    End Sub

    <WebMethod()>
    Public Sub FSALActualizarAccesos(Tipo As String, ByVal IdRegistro As String, fechapet As String, ByVal fecha As String, ByVal sNormalizarFechas As String, ByVal pagina As String, ByVal iP As String)

        Try
            Dim iTipo As Int16

            If Tipo.Split(",").Length > 1 Then
                Tipo = Tipo.Split(",")(0)
            End If
            If IsNumeric(Tipo) Then
                iTipo = CShort(Tipo)
            Else
                iTipo = 3
            End If

            fechapet = fechapet.Split(".")(0) & "." & Format(CInt(fechapet.Split(".")(1)), "000")
            fecha = fecha.Split(".")(0) & "." & Format(CInt(fecha.Split(".")(1)), "000")

            If sNormalizarFechas = "1" Then   ''las llamadas que traen fechas de cliente hay que normalizarlas respecto a las del servidor
                Dim dtFechaServer As DateTime
                Dim dtFechaPet As DateTime
                Dim dtFecha As DateTime
                Dim dDiffMsIniFinCli As Double

                dtFechaPet = DateTime.ParseExact(fechapet, "yyyy-MM-dd HH:mm:ss.fff", System.Globalization.CultureInfo.CurrentCulture)
                dtFecha = DateTime.ParseExact(fecha, "yyyy-MM-dd HH:mm:ss.fff", System.Globalization.CultureInfo.CurrentCulture)

                dDiffMsIniFinCli = dtFecha.Subtract(dtFechaPet).TotalMilliseconds * -1

                dtFechaServer = DateTime.UtcNow
                dtFecha = dtFechaServer
                dtFechaPet = dtFecha.AddMilliseconds(dDiffMsIniFinCli)

                fechapet = Format(dtFechaPet, "yyyy-MM-dd HH:mm:ss") & "." & Format(dtFechaPet.Millisecond, "000")
                fecha = Format(dtFecha, "yyyy-MM-dd HH:mm:ss") & "." & Format(dtFecha.Millisecond, "000")
            End If

            Dim sIP As String

            If Not IsNothing(iP) And iP <> "" Then
                sIP = iP
            Else
                sIP = HttpContext.Current.Request.UserHostAddress
            End If

            m_PMPortalServer = New Fullstep.PMPortalServer.FSAL
            m_PMPortalServer.ActualizarAcceso(iTipo, fechapet, fecha, IdRegistro, pagina, sIP)

        Catch ex As Exception

            Dim sMensaje As String = "Error WebService de PORTAL, Sub FSALActualizarAccesos(): " & ex.Message
            EscribirError(sMensaje)

        End Try

    End Sub

    ''' <summary>
    ''' Escribe un fichero log con los errores controlados que se han producido durante la ejecucion del WebService
    ''' </summary>
    ''' <param name="sMensaje">Mensaje del error controlado que hay que escribir en el log</param>
    ''' <remarks>Llamada desde=FSALRegistrarAccesos, FSALActualizarAccesos.</remarks>
    Private Sub EscribirError(ByVal sMensaje As String)
        Dim oFileW As System.IO.StreamWriter
        Dim bExisteLog As Boolean = System.IO.File.Exists(m_sErrorFile_FSAL)

        Try
            oFileW = New System.IO.StreamWriter(m_sErrorFile_FSAL, True)
            If Not bExisteLog Then
                oFileW.WriteLine("Fichero de Errores del Registro de Accesos FSAL")
                oFileW.WriteLine("===============================================")
            End If
            oFileW.WriteLine(Format(Now(), "yyyy/MM/dd HH:mm:ss") & " => " & sMensaje)
            oFileW.Close()
        Catch ex As Exception
            'NO vamos a registrar los posibles errores de acceso al log de errores del WebService por su continua y multiple ejecucion
            Exit Sub
        End Try

    End Sub

End Class