﻿Imports System.Web
Imports System.Web.Services

Imports System.Collections.Generic
Imports System.Configuration
Imports System.IO
Imports System.Linq
Imports System.Web.Script.Serialization
Imports Fullstep

Public Class FileTransferHandler
    Implements System.Web.IHttpHandler, System.Web.SessionState.IReadOnlySessionState
    Private ReadOnly js As New JavaScriptSerializer()
    Public ReadOnly Property TempStorage As String
        Get
            Return ConfigurationManager.AppSettings("temp") & "\"
        End Get
    End Property
    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub ProcessRequest(context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.AddHeader("Pragma", "no-cache")
        context.Response.AddHeader("Cache-Control", "private, no-cache")

        HandleMethod(context)
    End Sub

    ' Handle request based on method
    Private Sub HandleMethod(ByVal context As HttpContext)
        Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")        'Solo lo quiero para autenticar
        If PMPortalServer Is Nothing Then
            context.Response.ClearHeaders()
            context.Response.Redirect(ConfigurationManager.AppSettings("rutanormal") & "/../../../", False)
            Exit Sub
        End If

        Select Case context.Request.HttpMethod
            Case "HEAD", "GET"
                If GivenFilename(context) Then
                    DeliverFile(context)
                Else
                    ListCurrentFiles(context)
                End If
            Case "POST", "PUT"
                Try
                    If context.Request("type") = "del" Then
                        DeleteFile(context)
                    Else
                        UploadFile(context)
                    End If
                Catch ex2 As HttpRequestValidationException
                    Throw ex2
                Catch ex As Exception
                End Try
            Case "OPTIONS"
                ReturnOptions(context)
                Exit Select
            Case Else
                context.Response.ClearHeaders()
                context.Response.StatusCode = 405
        End Select
    End Sub

    Private Sub ReturnOptions(ByVal context As HttpContext)
        context.Response.AddHeader("Allow", "DELETE,GET,HEAD,POST,PUT,OPTIONS")
        context.Response.StatusCode = 200
    End Sub

    ' Delete file from the server
    Private Sub DeleteFile(ByVal context As HttpContext)
        Dim filePath = TempStorage & context.Request("f")
        If File.Exists(filePath) Then
            File.Delete(filePath)
        End If
    End Sub

    ' Upload file to the server
    Private Sub UploadFile(ByVal context As HttpContext)
        Dim statuses = New List(Of FilesStatus)()
        Dim headers = context.Request.Headers

        If String.IsNullOrEmpty(headers("X-File-Name")) Then
            UploadWholeFile(context, statuses)
        Else
            UploadPartialFile(headers("X-File-Name"), context, statuses)
        End If

        WriteJsonIframeSafe(context, statuses)
    End Sub

    ' Upload partial file
    Private Sub UploadPartialFile(ByVal fileName As String, ByVal context As HttpContext, ByVal statuses As List(Of FilesStatus))
        If context.Request.Files.Count <> 1 Then
            Throw New HttpRequestValidationException("Attempt to upload chunked file containing more than one fragment per request")
        End If
        Dim inputStream = context.Request.Files(0).InputStream
        Dim fullName = TempStorage & Path.GetFileName(fileName)

        Using fs = New System.IO.FileStream(fullName, FileMode.Append, FileAccess.Write)
            Dim buffer = New Byte(1023) {}

            Dim l = inputStream.Read(buffer, 0, 1024)
            While l > 0
                fs.Write(buffer, 0, l)
                l = inputStream.Read(buffer, 0, 1024)
            End While
            fs.Flush()
            fs.Close()
        End Using
        statuses.Add(New FilesStatus(New FileInfo(fullName)))
    End Sub

    ' Upload entire file
    Private Sub UploadWholeFile(ByVal context As HttpContext, ByVal statuses As List(Of FilesStatus))
        Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
        Dim oAdjun As Fullstep.PMPortalServer.Adjunto = PMPortalServer.Get_Adjunto

        For i As Integer = 0 To context.Request.Files.Count - 1
            Dim file = context.Request.Files(i)
            Dim RandomDirectory As String = modUtilidades.GenerateRandomPath()

            While IO.Directory.Exists(TempStorage & "\" & RandomDirectory)
                RandomDirectory = modUtilidades.GenerateRandomPath()
            End While

            IO.Directory.CreateDirectory(TempStorage & "\" & RandomDirectory)

            Dim ext As String = System.IO.Path.GetExtension(file.FileName)
            If oAdjun.EstaenListaNegra(ext) Then
                Throw New HttpRequestValidationException("Attempt to upload file of not admitable extension")
            End If

            'If InStr(PMPortalWeb.modUtilidades.getMimeFromFile(file, LCase(ext)), LCase(ext) & "#") = 0 Then
            If Not PMPortalWeb.modUtilidades.getMimeFromFile(file, LCase(ext)) Then
                Throw New HttpRequestValidationException("Attempt to upload file with changed extension")

            End If

            file.SaveAs(TempStorage & RandomDirectory & "\" & Path.GetFileName(file.FileName))

            Dim fullName As String = Path.GetFileName(file.FileName)
            statuses.Add(New FilesStatus(RandomDirectory, fullName, file.ContentType, file.ContentLength))
        Next
    End Sub

    Private Sub WriteJsonIframeSafe(ByVal context As HttpContext, ByVal statuses As List(Of FilesStatus))
        context.Response.AddHeader("Vary", "Accept")
        Try
            If context.Request("HTTP_ACCEPT").Contains("application/json") Then
                context.Response.ContentType = "application/json"
            Else
                context.Response.ContentType = "text/plain"
            End If
        Catch
            context.Response.ContentType = "text/plain"
        End Try

        Dim jsonObj = js.Serialize(statuses.ToArray())
        context.Response.Write(jsonObj)
    End Sub

    Private Function GivenFilename(ByVal context As HttpContext) As Boolean
        Return Not String.IsNullOrEmpty(context.Request("f"))
    End Function

    ''' <summary>
    ''' Procedimiento que envía un archivo en forma de buffer al explorador
    ''' </summary>
    ''' <param name="context">HttpContext</param>
    ''' <remarks>Llamada desde: HandleMethod ; Tiempo máximo: 0,2</remarks>
    Private Sub DeliverFile(ByVal context As HttpContext)
        Dim filename As String = context.Request("f")
        Dim filePath As String
        Dim DGuid As String = context.Request("f")
        Dim byteBuffer() As Byte

        Select Case context.Request("t")
            Case 0, 1, 2
                Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
                Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
                Dim ocnMensajes As PMPortalServer.cnMensajes
                ocnMensajes = PMPortalServer.Get_Object(GetType(Fullstep.PMPortalServer.cnMensajes))
                filename = ocnMensajes.Obtener_Nombre_Adjunto(CN_Usuario.CiaComp, DGuid)
                byteBuffer = ocnMensajes.CN_Mensajes_GetAdjuntoMensaje(CN_Usuario.CiaComp, DGuid)
            Case Else
                filename = DGuid
                filePath = Replace(filename, "../", "")
                filePath = TempStorage & Replace(filePath, "..\", "")
                Dim input As System.IO.FileStream = New System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Dim buffer(input.Length - 1) As Byte
                input.Read(buffer, 0, buffer.Length)
                input.Close()
                byteBuffer = buffer
        End Select
        If context.Request("t") = 0 Then
            Call context.Response.AddHeader("Content-Type", "application/octet-stream")
            Call context.Response.AddHeader("Content-Disposition", "attachment;filename=" & context.Request("f"))
            Call context.Response.BinaryWrite(byteBuffer)
            Call context.Response.End()
        Else
            context.Response.ContentType = context.Request("c")
            context.Response.BinaryWrite(byteBuffer)
        End If
    End Sub
    Private Sub ListCurrentFiles(ByVal context As HttpContext)
        Dim files = New DirectoryInfo(TempStorage).GetFiles("*", SearchOption.TopDirectoryOnly).Where(Function(f) Not f.Attributes.HasFlag(FileAttributes.Hidden)).[Select](Function(f) New FilesStatus(f)).ToArray()
        Dim jsonObj As String = js.Serialize(files)
        context.Response.AddHeader("Content-Disposition", "inline, filename=""files.json""")
        context.Response.Write(jsonObj)
        context.Response.ContentType = "application/json"
    End Sub
End Class
