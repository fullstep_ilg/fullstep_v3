﻿Public Class cn_UserProfile
    Inherits FSPMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Colaboracion

        If Not IsPostBack Then
            With FSNPageHeader
                .TituloCabecera = Textos(0) 'Colaboración
            End With
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutas") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutas", "<script>" & _
                "var rutanormal = '" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "';" & _
                "var rutaFSNWeb = '" & System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "';</script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "LimiteMensajesCargados") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "LimiteMensajesCargados", "<script>var LimiteMensajesCargados =" & System.Configuration.ConfigurationManager.AppSettings("LimiteMensajesCargados") & "</script>")
        End If
        Dim sVariableJavascriptTextos As String
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
            sVariableJavascriptTextos = "var Textos = new Array();"
            Dim sVariableJavascriptTextosCKEditor As String = "var TextosCKEditor = new Array();"
            For i As Integer = 1 To 73
                sVariableJavascriptTextos &= "Textos[" & i & "]='" & JSText(Textos(i)) & "';"
                If i = 20 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[1]='" & JSText(Textos(i)) & "';"
                If i = 21 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[2]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 83 To 85
                sVariableJavascriptTextos &= "Textos[" & i - 9 & "]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 74 To 89
                sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 71 & "]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 90 To 121
                sVariableJavascriptTextos &= "Textos[" & i - 13 & "]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 142 To 143
                sVariableJavascriptTextos &= "Textos[" & i - 33 & "]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 144 To 147
                sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 125 & "]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 148 To 151
                sVariableJavascriptTextos &= "Textos[" & i - 37 & "]='" & JSText(Textos(i)) & "';"
            Next
            sVariableJavascriptTextos &= "Textos[115]='" & JSText(Textos(155)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextos & sVariableJavascriptTextosCKEditor, True)
        End If

        ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.PerfilUsuarioCN
        sVariableJavascriptTextos = "<script>var TextosUser = new Array();"
        For i = 1 To 15
            sVariableJavascriptTextos &= "TextosUser[" & i & "]='" & JSText(Textos(i)) & "';"
        Next
        sVariableJavascriptTextos &= "TextosUser[16]='" & JSText(Textos(29)) & "';"   'Aceptar
        sVariableJavascriptTextos &= "TextosUser[17]='" & JSText(Textos(30)) & "';"   'Cancelar
        sVariableJavascriptTextos &= "TextosUser[18]='" & JSText(Textos(31)) & "';"   'Cancelar
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosUser", sVariableJavascriptTextos & "</script>")

        Dim Usuario As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
        With Usuario
            With FSNPageHeader
                .TituloCabecera = Textos(0) 'Colaboración
            End With
            lblTitulo.Text = Textos(15)  'Perfil de usuario de la red de colaboración
            lblNombreEtiq.Text = Textos(16)  'Nombre:
            lblNombre.Text = .Nombre
            lblFotoEtiq.Text = Textos(17)  'Foto:
            lblModifFoto.Text = Textos(28)  'Modificar foto perfil
            lblNotificaciones.Text = Textos(18)  'Notificaciones ví­a email
            chkNotificarNuevaActividad.Text = Textos(19)  'Recibir un email con el resumen de la nueva actividad en la red de colaboración
            chkNotificarNuevaActividad.Checked = .CNNotificarResumenActividad
            lblConfiguraciones.Text = Textos(21)  'Opciones de configuración
            chkMostrarOcultosActividad.Text = Textos(22)  'Volver a mostrar automáticamente los mensajes ocultos en los que haya actividad
            chkMostrarOcultosActividad.Checked = .CNConfiguracionDesocultar
        End With
    End Sub
End Class