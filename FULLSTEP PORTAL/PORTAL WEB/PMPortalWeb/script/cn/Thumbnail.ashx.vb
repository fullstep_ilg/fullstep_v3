﻿Imports System.Web
Imports System.Web.Services
Imports System.IO
Imports System.Web.SessionState
Imports Fullstep

Public Class Thumbnail
    Implements System.Web.IHttpHandler, IRequiresSessionState, IReadOnlySessionState
    Public ReadOnly Property TempRoot As String
        Get
            Return ConfigurationManager.AppSettings("temp") & "\"
        End Get
    End Property
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim filename As String
        Dim byteBuffer() As Byte
        Select Case context.Request("t")
            Case 0
                Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
                If context.Request("u") Is Nothing Then
                    If CN_Usuario.CNTieneImagen Then
                        Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
                        Dim oMensajes As PMPortalServer.cnMensajes = PMPortalServer.Get_Object(GetType(PMPortalServer.cnMensajes))
                        byteBuffer = oMensajes.CN_Mensajes_GetImagenUsuario(CN_Usuario.CiaComp, CN_Usuario.CNGuidImagenUsu)
                    Else
                        filename = HttpContext.Current.Server.MapPath("~/images/FSCN_NO_USU_IMAGE.png")
                        Dim input As System.IO.FileStream = New System.IO.FileStream(filename, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                        Dim buffer(input.Length - 1) As Byte
                        input.Read(buffer, 0, buffer.Length)
                        input.Close()
                        byteBuffer = buffer
                    End If
                Else
                    If context.Request("u") Is String.Empty Then
                        filename = HttpContext.Current.Server.MapPath("~/images/FSCN_NO_IMAGE.png")
                        Dim input As System.IO.FileStream = New System.IO.FileStream(filename, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                        Dim buffer(input.Length - 1) As Byte
                        input.Read(buffer, 0, buffer.Length)
                        input.Close()
                        byteBuffer = buffer
                    Else
                        Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
                        Dim dataDir As String = ConfigurationManager.AppSettings("temp") & "\users"
                        Dim oMensajes As PMPortalServer.cnMensajes = PMPortalServer.Get_Object(GetType(PMPortalServer.cnMensajes))
                        byteBuffer = oMensajes.CN_Mensajes_GetImagenUsuario(CN_Usuario.CiaComp, context.Request("u"))
                    End If
                End If
            Case 1, 2
                Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
                'los dos parametros. Falta comprobar que ahora guarda y recupera los adjuntos.
                Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
                Dim oMensajes As PMPortalServer.cnMensajes = PMPortalServer.Get_Object(GetType(PMPortalServer.cnMensajes))
                byteBuffer = oMensajes.CN_Mensajes_GetAdjuntoMensaje(CN_Usuario.CiaComp, context.Request("f"))
            Case Else
                filename = Replace(context.Request("f"), "../", "")
                filename = TempRoot & Replace(filename, "..\", "")
                Dim input As System.IO.FileStream = New System.IO.FileStream(filename, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Dim buffer(input.Length - 1) As Byte
                input.Read(buffer, 0, buffer.Length)
                input.Close()
                byteBuffer = buffer
        End Select
        If byteBuffer IsNot Nothing Then
            context.Response.ContentType = context.Request("c")
            context.Response.BinaryWrite(byteBuffer)
        End If
    End Sub
    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
End Class