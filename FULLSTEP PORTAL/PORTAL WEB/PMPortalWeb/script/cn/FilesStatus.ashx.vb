﻿Imports System.IO

Public Class FilesStatus
    Public ReadOnly Property HandlerPath() As String
        Get
            Return ConfigurationManager.AppSettings("rutanormal") & "script/cn/"
        End Get
    End Property
    Public ReadOnly Property TempURL() As String
        Get
            Return ConfigurationManager.AppSettings("download")
        End Get
    End Property
    Public Property group() As String
        Get
            Return m_group
        End Get
        Set(ByVal value As String)
            m_group = value
        End Set
    End Property
    Private m_group As String
    Public Property name() As String
        Get
            Return m_name
        End Get
        Set(ByVal value As String)
            m_name = value
        End Set
    End Property
    Private m_name As String
    Public Property type() As String
        Get
            Return m_type
        End Get
        Set(ByVal value As String)
            m_type = value
        End Set
    End Property
    Private m_type As String
    Public Property size() As Integer
        Get
            Return m_size
        End Get
        Set(ByVal value As Integer)
            m_size = value
        End Set
    End Property
    Private m_size As Integer
    Public Property progress() As String
        Get
            Return m_progress
        End Get
        Set(ByVal value As String)
            m_progress = value
        End Set
    End Property
    Private m_progress As String
    Public Property url() As String
        Get
            Return m_url
        End Get
        Set(ByVal value As String)
            m_url = value
        End Set
    End Property
    Private m_url As String
    Public Property thumbnail_url() As String
        Get
            Return m_thumbnail_url
        End Get
        Set(ByVal value As String)
            m_thumbnail_url = value
        End Set
    End Property
    Private m_thumbnail_url As String
    Public Property delete_url() As String
        Get
            Return m_delete_url
        End Get
        Set(ByVal value As String)
            m_delete_url = value
        End Set
    End Property
    Private m_delete_url As String
    Public Property delete_type() As String
        Get
            Return m_delete_type
        End Get
        Set(ByVal value As String)
            m_delete_type = value
        End Set
    End Property
    Private m_delete_type As String
    Public Property [error]() As String
        Get
            Return m_error
        End Get
        Set(ByVal value As String)
            m_error = value
        End Set
    End Property
    Private m_error As String
    Public Property tipoadjunto() As Integer
        Get
            Return m_tipoadjunto
        End Get
        Set(ByVal value As Integer)
            m_tipoadjunto = value
        End Set
    End Property
    Private m_tipoadjunto As Integer
    Public Property filesize() As String
        Get
            Return m_filesize
        End Get
        Set(ByVal value As String)
            m_filesize = value
        End Set
    End Property
    Private m_filesize As String
    Public Property fileCopyURL() As String
        Get
            Return m_fileCopyURL
        End Get
        Set(ByVal value As String)
            m_fileCopyURL = value
        End Set
    End Property
    Private m_fileCopyURL As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal fileInfo As FileInfo)
        SetValuesInfo(fileInfo.Name, CInt(fileInfo.Length))
    End Sub
    Public Sub New(ByVal RandomDirectory As String, ByVal fileName As String, _
                   ByVal ContentType As String, ByVal fileLength As Integer)
        SetValues(RandomDirectory, fileName, ContentType, fileLength)
    End Sub
    Private Sub SetValuesInfo(ByVal fileName As String, ByVal fileLength As Integer)
        name = fileName
        type = "image/png"
        size = fileLength
        progress = "1.0"
        url = HandlerPath & "FileTransferHandler.ashx?f=" & fileName
        thumbnail_url = HandlerPath & "Thumbnail.ashx?f=" & fileName
        delete_url = HandlerPath & "FileTransferHandler.ashx?f=" & fileName
        delete_type = "DELETE"
    End Sub
    Private Sub SetValues(ByVal RandomDirectory As String, ByVal fileName As String, _
                          ByVal ContentType As String, ByVal fileLength As Integer)
        name = fileName
        type = ContentType

        size = fileLength
        progress = "1.0"
        url = HandlerPath & "FileTransferHandler.ashx?f=" & RandomDirectory & "/" & fileName & "&c=" & ContentType
        Select Case ContentType
            Case "image/bmp", "image/gif", "image/jpg", "image/jpeg", "image/png", "image/x-png", "image/tif", "image/tiff", "image/pjpeg"
                thumbnail_url = HandlerPath & "Thumbnail.ashx?f=" & RandomDirectory & "/" & fileName & "&c=" & ContentType
                tipoadjunto = 1
            Case "video/mp4"
                thumbnail_url = TempURL & RandomDirectory & "/" & fileName
                tipoadjunto = 2
            Case Else
                Select Case fileName.Split(".")(fileName.Split(".").Length - 1)
                    Case "xls", "xlsx"
                        thumbnail_url = ConfigurationManager.AppSettings("rutanormal") & "images/excelAttach.png"
                        tipoadjunto = 11
                    Case "doc", "docx"
                        thumbnail_url = ConfigurationManager.AppSettings("rutanormal") & "images/wordAttach.png"
                        tipoadjunto = 12
                    Case "pdf"
                        thumbnail_url = ConfigurationManager.AppSettings("rutanormal") & "images/pdfAttach.png"
                        tipoadjunto = 13
                    Case Else
                        thumbnail_url = ConfigurationManager.AppSettings("rutanormal") & "images/Attach.png"
                        tipoadjunto = 10
                End Select
        End Select
        fileCopyURL = ConfigurationManager.AppSettings("temp") & "\" & RandomDirectory & "\" & fileName
        delete_url = HandlerPath & "FileTransferHandler.ashx?f=" & RandomDirectory & "\" & fileName & "&type=del"
        delete_type = "POST"

        Dim CN_Usuario As Fullstep.PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
        If (fileLength >= 1000000000) Then
            filesize = modUtilidades.FormatNumber(fileLength / 1000000000, CN_Usuario.NumberFormat)
        ElseIf fileLength >= 1000000 Then
            filesize = modUtilidades.FormatNumber(fileLength / 1000000, CN_Usuario.NumberFormat)
        Else
            filesize = modUtilidades.FormatNumber(fileLength / 1000, CN_Usuario.NumberFormat)
        End If
    End Sub
End Class