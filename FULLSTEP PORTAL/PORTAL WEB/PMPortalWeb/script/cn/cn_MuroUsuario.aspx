﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="cn_MuroUsuario.aspx.vb" Inherits=".cn_MuroUsuario" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<script src="../../../common/menu.asp"></script>
</head>
<body class="bodyMuroUsuario">
	<script type="text/javascript">dibujaMenu(8)</script>
	<form id="form1" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server">
		<Scripts>
			<asp:ScriptReference Path="~/js/jquery/jquery.min.js" />
            <asp:ScriptReference Path="~/js/jquery/jquery-migrate.min.js" />
			<asp:ScriptReference Path="~/js/jquery/jquery.json.min.js" />
			<asp:ScriptReference Path="~/js/jquery/jquery.tmpl.min.js" />
			<asp:ScriptReference Path="~/js/jquery/jquery.ui.min.js" />
			<asp:ScriptReference Path="~/js/jquery/tmpl.min.js" /> 
			<asp:ScriptReference Path="~/ckeditor/ckeditor.js" />
			<asp:ScriptReference Path="~/script/cn/js/cn_MuroUsuario_init.js" />
		</Scripts>
	</asp:ScriptManager>
	<div id="divCabecera" style="clear:both; position:relative; float:left; width:100%; margin-top:15px;">
		<fsn:FSNPageHeader ID="FSNPageHeader" runat="server" UrlImagenCabecera="~/images/ColaboracionHeader.png" />
		<div style="position:absolute; width:100%; bottom:2px;">
            <div id="lnkAyudaBuscador" class="Link" style="float:right; margin-right:20px; margin-left:5px;">
				<span class="Link TextoAyuda Texto12 Negrita Subrayado" style="line-height:20px;">?</span>
			</div>
			<img id="imgBuscador" src="../../images/Buscador.png" class="Link Image20" style="float:right;"/>
			<input id="txtBuscador" type="text" class="CajaTexto" style="float:right; width:300px; padding-left:2px; padding-right:2px;" />             
		</div>
	</div>	
		
	<div id="divMenu" class="divMenu"></div>

	<div id="divPrincipal" class="divPrincipal">
		<div id="divNotificaciones" class="SeparadorSup" style="display:none; clear:both; float:left; width:100%; height:40px; text-align:center; padding:5px 0px 5px 0px;">
			<div id="divPanelNotificaciones" class="Link">
				<span class="Texto12">DNotificaciones</span>
			</div>
		</div>
		<div id="divMuro" class="divMuro">
            <div id="divOpcionesOtrosMensajes" class="divOpcionesOtrosMensajes" style="display:none;">
			    <div id="divHistorico" style="float:left; padding:5px; display:none;">
					<span id="lblHistorico" class="Link"></span>
				</div>
				<div id="divSiguientes" style="float:left; padding:5px;">
					<span id="lblSiguientes" class="Link"></span>
				</div>
				<div id="divOcultos" style="display:none; float:right; padding:5px;">
					<span id="lblOcultos" class="Link"></span>
				</div>
		    </div>
        </div>
	</div>

	<div id="divResultadosBusqueda" class="divPrincipal" style="display:none;">
		<div class="SeparadorInf" style="clear:both; float:left; width:100%; padding:15px 0px 15px 15px;">
			<span id="lblTituloResultadoBusqueda" class="Texto14 Negrita">DResultados de la busqueda</span>
		</div>
		<div style="clear:both; float:left; width:100%; padding:15px 0px 15px 15px;">
			<span id="lblInfoResultadosBusqueda">DSe ha encontrado un total de</span>
			<span id="lblNumResultadosBusqueda" class="Negrita">## mensajes</span>
			<span id="lblObjetivoResultadosBusqueda">Dpara</span>
			<span id="lblTextoResultadosBusqueda" class="Negrita"></span>
		</div>
		<div class="FondoHeader" style="clear:both; float:left; width:100%; padding:3px 0px 3px 15px;">
			<span id="lblTipoOrdenacion" class="Negrita" style="padding-right:5px;">DOrdenar por:</span>
			<span id="lblTipoOrdenacionFecha" style="padding-right:5px;">DMás reciente (por defecto)</span>|<span id="lblTipoOrdenacionCoincidencias" class="Subrayado Link"  style="padding-left:5px;">DCoincidencias</span>
		</div>
		<div id="divMensajesResultadosBusqueda" class="divMuro"></div>
	</div>

	<div id="divCargando" style="display:none; float:left; width:75%; margin-top:30px; text-align:center;">
		<img alt="" src="../../images/CargandoMuro.gif" />
	</div>
	</form>
	<div id="fileupload" style="display:none;">
		<form id="formFileupload" method="post" enctype="multipart/form-data">
			<div class="fileupload-buttonbar">
				<label class="fileinput-button">                    
					<input type="file" name="files[]" multiple="multiple" />
				</label>
				<div class="fileupload-progressbar"></div>
			</div>
		</form>
	</div>
</body>
</html>
