﻿Namespace Fullstep.PMPortalWeb
    Partial Class downloadattach
        Inherits FSPMPage
        ''' <summary>
        ''' Descarga un fichero en CN
        ''' </summary>
        ''' <param name="sender">pantalla</param>
        ''' <param name="e">parametro de sistema</param>  
        ''' <remarks>Llamada desde: cn_mensajes_ui.js; Tiempo maximo:0</remarks>
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Dim oFileStream As System.IO.FileStream
            Dim sFichero As String
            Dim filename As String
            Dim byteBuffer() As Byte

            Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")

            'Solo lo quiero para autenticar
            If PMPortalServer Is Nothing Then Exit Sub

            If Request.QueryString("e") Is Nothing Then
                Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
                Dim ocnMensajes As Fullstep.PMPortalServer.cnMensajes
                ocnMensajes = FSPMServer.Get_Object(GetType(Fullstep.PMPortalServer.cnMensajes))
                filename = ocnMensajes.Obtener_Nombre_Adjunto(FSPMUser.CiaComp, Request.QueryString("f"))
                byteBuffer = ocnMensajes.CN_Mensajes_GetAdjuntoMensaje(FSPMUser.CiaComp, Request.QueryString("f"))
            Else
                sFichero = Server.UrlDecode(Request.QueryString("f"))
                sFichero = Replace(sFichero, "../", "")
                sFichero = Replace(sFichero, "..\", "")
                Dim RutaAceptada As String = ConfigurationManager.AppSettings("temp")
                If Not (Left(sFichero, Len(RutaAceptada)) = RutaAceptada) Then
                    Exit Sub
                End If

                filename = Split(Replace(sFichero, ConfigurationManager.AppSettings("temp") & "\", ""), "\")(1)
                Dim input As System.IO.FileStream = New System.IO.FileStream(sFichero, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Dim buffer(input.Length - 1) As Byte
                input.Read(buffer, 0, buffer.Length)
                input.Close()
                byteBuffer = buffer
            End If
            If byteBuffer IsNot Nothing Then
                Response.ClearContent()
                Response.ClearHeaders()
                Response.Clear()
                Call Me.Response.AddHeader("Content-Type", "application/octet-stream")
                Call Me.Response.AddHeader("Content-Disposition", "attachment; filename=""" & filename & """")
                Call Me.Response.BinaryWrite(byteBuffer)
                Call Me.Response.End()
            End If
        End Sub
    End Class
End Namespace