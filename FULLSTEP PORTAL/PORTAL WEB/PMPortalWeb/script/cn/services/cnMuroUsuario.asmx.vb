﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Fullstep
Imports System.Collections.Generic
Imports System.IO
Imports Irony.Parsing
Imports System.Globalization

<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class cnMuroUsuario
    Inherits FSPMPage
#Region "Carga inicial"
    ''' <summary>
    ''' Obtener Mensajes Usuario
    ''' </summary>
    ''' <param name="tipo">tipo mensaje</param>
    ''' <param name="megusta">megusta el mensaje o no</param>
    ''' <param name="pagina">numero pagina  mostrar</param>
    ''' <param name="timezoneoffset">diferencia horario usuario y servidor</param>
    ''' <returns>Objeto mensajes</returns>
    ''' <remarks>Llamada desde: script\cn\js\cn_menu.js/function CargarMensajes ; Tiempo máximo: 0,2</remarks>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function ObtenerMensajesUsuario(ByVal tipo As Integer, ByVal megusta As Boolean, ByVal pagina As Integer, _
                                           ByVal timezoneoffset As Double, ByVal historico As Boolean, _
                                           ByVal discrepancias As Boolean, ByVal factura As Integer, ByVal linea As Integer) As List(Of cnMensaje)
        Dim PMPortalServerRoot As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
        Dim ocnMensajes As PMPortalServer.cnMensajes
        Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
        Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_User").CiaComp

        ocnMensajes = PMPortalServerRoot.Get_Object(GetType(PMPortalServer.cnMensajes))

        Dim LimiteMensajesCargados As String = CType(System.Configuration.ConfigurationManager.AppSettings("LimiteMensajesCargados"), Integer)
        Dim oMensajes As New List(Of cnMensaje)
        With CN_Usuario
            oMensajes = ocnMensajes.ObtenerMensajesUsuario(lCiaComp, .CodProveGS, .IdContacto, .Idioma, .DateFormat.ShortDatePattern, .NumberFormat, tipo, _
                                                           megusta, LimiteMensajesCargados, pagina, timezoneoffset, discrepancias, factura, linea)
        End With

        Return oMensajes
    End Function
#End Region
#Region "Notificaciones"
    ''' <summary>
    ''' Comprueba los mensajes nuevos del usuario conectado
    ''' </summary>
    ''' <returns>Objeto contadores</returns>
    ''' <remarks>Llamada desde: PMPortalWeb\js\cabecera_master.js\function VerificarNotificacionesCN    PMPortalWeb\script\cn\js\cn_menu.js\function Obtener_Contadores_Notificaciones
    ''' ; Tiempo máximo: 0,2</remarks>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Comprobar_Notificaciones() As List(Of cn_fsItem)
        Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
        Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
        Dim ocnMensajes As PMPortalServer.cnMensajes
        Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_User").CiaComp

        ocnMensajes = PMPortalServer.Get_Object(GetType(PMPortalServer.cnMensajes))

        Dim oContadores As New List(Of cn_fsItem)
        With CN_Usuario
            oContadores = ocnMensajes.Comprobar_Notificaciones(lCiaComp, .CodProveGS, .IdContacto)
        End With
        Return oContadores
    End Function
    ''' <summary>
    ''' Obtiene el mensaje urgente que se debe mostrar en el pop up
    ''' </summary>
    ''' <param name="idMensaje">El id del mensaje que se quiere recuperar</param>
    ''' <param name="timezoneoffset">diferencia horario usuario y servidor</param>
    ''' <remarks>Llamada desde: PMPortalWeb\js\cabecera_master.js\function ObtenerMensajeUrgente ; Tiempo máximo: 0,2</remarks>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_Mensaje(ByVal idMensaje As Integer, ByVal timezoneoffset As Double) As cnMensaje
        Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
        Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
        Dim ocnMensajes As PMPortalServer.cnMensajes
        Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_User").CiaComp

        ocnMensajes = PMPortalServer.Get_Object(GetType(PMPortalServer.cnMensajes))

        Dim oMensaje As New cnMensaje
        With CN_Usuario
            oMensaje = ocnMensajes.Obtener_Mensaje(lCiaComp, .CodProveGS, .IdContacto, .Idioma, .DateFormat.ShortDatePattern, _
                                                   .NumberFormat, idMensaje, timezoneoffset)
        End With
        Return oMensaje
    End Function
    ''' <summary>
    ''' marcar como Leido un Mensaje Urgente
    ''' </summary>
    ''' <param name="idMensaje">Id de mensaje</param>
    ''' <param name="timezoneoffset">diferencia horario usuario y servidor</param>
    ''' <returns>Objeto mensaje</returns>
    ''' <remarks>Llamada desde: \PMPortalWeb\js\cabecera_master.js\function MensajeUrgenteLeido ; Tiempo máximo: 0,2</remarks>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function MensajeUrgente_Leido(ByVal idMensaje As Integer, ByVal timezoneoffset As Double) As cnMensaje
        Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
        Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
        Dim ocnMensajes As PMPortalServer.cnMensajes
        Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_User").CiaComp

        ocnMensajes = PMPortalServer.Get_Object(GetType(PMPortalServer.cnMensajes))

        Dim oMensaje As New cnMensaje
        With CN_Usuario
            oMensaje = ocnMensajes.MensajeUrgente_Leido(lCiaComp, .CodProveGS, .IdContacto, .Idioma, .DateFormat.ShortDatePattern, _
                                                        .NumberFormat, idMensaje, timezoneoffset)
        End With
        Return oMensaje
    End Function
#End Region
#Region "Otras funciones"
    ''' <summary>
    ''' Oculta o muestra el mensaje seleccionado
    ''' </summary>
    ''' <param name="idMensaje">Id de mensaje</param>
    ''' <param name="ocultar">Ocultar/Mostrar</param>
    ''' <remarks>Llamada desde: PMPortalWeb\script\cn\js\cn_mensajes_ui.js ; Tiempo máximo: 0,2</remarks>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Sub OcultarMostrarMensaje(ByVal idMensaje As Integer, ByVal ocultar As Boolean)
        Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
        Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
        Dim ocnMensajes As PMPortalServer.cnMensajes
        Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_User").CiaComp

        ocnMensajes = PMPortalServer.Get_Object(GetType(PMPortalServer.cnMensajes))

        With CN_Usuario
            ocnMensajes.OcultarMostrarMensaje(lCiaComp, .CodProveGS, .IdContacto, idMensaje, ocultar)
        End With
    End Sub
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Comprobar_HayMensajesHistoricos() As Boolean
        Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
        Dim ocn_Data As PMPortalServer.cnData
        ocn_Data = PMPortalServer.Get_Object(GetType(PMPortalServer.cnData))

        Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FSN_User")
        Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_User").CiaComp
        With CN_Usuario
            Return ocn_Data.Comprobar_HayMensajesHistoricos(lCiaComp, .CodProve, .IdContacto)
        End With
    End Function
#End Region
#Region "Respuestas"
    ''' <summary>
    ''' Crea un objeto tipo respuesta para añadirlo a pantalla directamente y seguir despues guardando en base de datos.
    ''' </summary>
    ''' <remarks></remarks>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function GenerarRespuesta(ByVal idMensaje As Integer, ByVal idRespuesta As Integer, ByVal mensaje As Object, ByVal citado As Object, ByVal adjuntos As Object) As cnMensaje
        Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")

        Dim oMensajeHTML As New cnMensaje

        With oMensajeHTML
            .Categoria = New cnCategoria
            .InfoUsuario = New cnUsuario
            .MeGusta = New cnMeGusta

            .Respuestas = New List(Of cnRespuesta)
            Dim iRespuesta As New cnRespuesta
            With iRespuesta
                .IdRespuesta = 0
                .Leido = True
                .EnProceso = "xr" & idRespuesta & "rx"
                .IdMensaje = idMensaje
                .Contenido = mensaje("contenido")
                .FechaAlta = Date.Now
                .InfoUsuario = New cnUsuario
                .InfoUsuario.Cod = CN_Usuario.Cod
                .InfoUsuario.Nombre = CN_Usuario.Nombre
                .InfoUsuario.SituacionUO = CN_Usuario.DepDenominacion
                .InfoUsuario.ImagenUrl = ConfigurationManager.AppSettings("rutanormal") & "script/cn/Thumbnail.ashx?t=0&u=" & CN_Usuario.CNGuidImagenUsu
                If Not citado.Count = 0 Then
                    .UsuarioCitado = New cnUsuario
                    .UsuarioCitado.Nombre = citado("nombreUsu")
                    .UsuarioCitado.SituacionUO = citado("situacionUO")
                End If
                .IdMensajeCitado = IIf(idRespuesta = 0, Nothing, idRespuesta)
                .MeGusta = New cnMeGusta
                .MeGusta.MeGusta = False
                .Visible = True

                Dim adjunto As cnAdjunto
                .Adjuntos = New List(Of cnAdjunto)
                For Each item As Object In adjuntos
                    If Not item.Count = 0 Then
                        adjunto = New cnAdjunto
                        With adjunto
                            .EnProceso = "*" & idRespuesta & "*"
                            .Nombre = item("nombre")
                            .Size = modUtilidades.Numero(item("size"), CN_Usuario.NumberFormat.NumberDecimalSeparator)
                            .SizeUnit = item("sizeunit")
                            .SizeToString = modUtilidades.FormatNumber(.Size, CN_Usuario.NumberFormat) & " " & .SizeUnit
                            .TipoAdjunto = item("tipoadjunto")
                            .Url = item("url")
                            Select Case .TipoAdjunto
                                Case 1
                                    .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "script/cn/Thumbnail.ashx?f=" & _
                                        Replace(.Url, ConfigurationManager.AppSettings("temp"), "")
                                Case 10
                                    .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "images/Attach.png"
                                Case 11
                                    .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "images/excelAttach.png"
                                Case 12
                                    .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "images/wordAttach.png"
                                Case 13
                                    .Thumbnail_Url = ConfigurationManager.AppSettings("rutanormal") & "images/pdfAttach.png"
                            End Select
                        End With
                        .Adjuntos.Add(adjunto)
                    End If
                Next
            End With
            .Respuestas.Add(iRespuesta)
        End With

        Return oMensajeHTML
    End Function
    ''' <summary>
    ''' Guarda la respuesta al mensaje en base de datos
    ''' </summary>
    ''' <param name="idMensaje">Id de mensaje</param>
    ''' <param name="idRespuesta">Id Respuesta de mensaje</param>
    ''' <param name="mensaje">mensaje</param>
    ''' <param name="citado">mensaje citado o no</param>
    ''' <param name="adjuntos">adjuntos de mensaje</param>
    ''' <remarks>Llamada desde: PMPortalWeb\script\cn\js\cn_ui_respuestas.js ; Tiempo máximo: 0,2</remarks>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function ResponderMensaje(ByVal idMensaje As Integer, ByVal idRespuesta As Integer, ByVal mensaje As Object, _
                ByVal citado As Object, ByVal adjuntos As Object, ByVal imagenesCKEditor As Object) As Integer
        Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
        Dim ocnMensajes As PMPortalServer.cnMensajes
        Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
        Dim oRespuesta As New cnRespuesta

        ocnMensajes = PMPortalServer.Get_Object(GetType(PMPortalServer.cnMensajes))
        Dim Contenido As String = mensaje("contenido").ToString
        For Each item As Object In imagenesCKEditor
            item("guidID") = Guid.NewGuid.ToString
            Contenido = Replace(Contenido, HttpUtility.HtmlEncode(item("href")), "Thumbnail.ashx?f=" & item("guidID") & "&t=1")
        Next
        With oRespuesta
            .IdMensaje = idMensaje
            .FechaAlta = DateAdd(DateInterval.Minute, CType(mensaje("timezoneOffset"), Integer), DateTime.Parse(mensaje("fechaAlta"), CN_Usuario.DateFormat))
            .InfoUsuario = New cnUsuario
            .InfoUsuario.ProveCod = CN_Usuario.CodProveGS
            .InfoUsuario.Con = CN_Usuario.IdContacto
            .Contenido = Contenido
            .MeGusta = New cnMeGusta
            .MeGusta.MeGusta = False
            .IdMensajeCitado = IIf(idRespuesta = 0, Nothing, idRespuesta)

            Dim adjunto As cnAdjunto
            .Adjuntos = New List(Of cnAdjunto)
            For Each item As Object In adjuntos
                If Not item.Count = 0 Then
                    adjunto = New cnAdjunto
                    With adjunto
                        .Nombre = item("nombre")
                        .Size = modUtilidades.Numero(item("size"), CN_Usuario.NumberFormat.NumberDecimalSeparator)
                        .SizeUnit = item("sizeunit")
                        .SizeToString = modUtilidades.FormatNumber(.Size, CN_Usuario.NumberFormat) & " " & .SizeUnit
                        .TipoAdjunto = item("tipoadjunto")
                        .Url = item("url")
                    End With
                    .Adjuntos.Add(adjunto)
                End If
            Next
        End With

        Dim dtAdjuntos As New DataTable
        dtAdjuntos.Columns.Add("ID", GetType(System.Int64))
        dtAdjuntos.Columns.Add("NOMBRE", GetType(System.String))
        dtAdjuntos.Columns.Add("GUID", GetType(System.String))
        dtAdjuntos.Columns.Add("SIZE", GetType(System.Double))
        dtAdjuntos.Columns.Add("SIZEUNIT", GetType(System.String))
        dtAdjuntos.Columns.Add("TIPOADJUNTO", GetType(System.Int64))
        dtAdjuntos.Columns.Add("URL", GetType(System.String))
        Try
            Dim rAdjunto As DataRow
            If Not oRespuesta.Adjuntos.Count = 0 Then
                For Each iAdjunto As cnAdjunto In oRespuesta.Adjuntos
                    rAdjunto = dtAdjuntos.NewRow
                    With rAdjunto
                        .Item("NOMBRE") = iAdjunto.Nombre
                        .Item("GUID") = Guid.NewGuid.ToString & "." & Split(iAdjunto.Nombre, ".")(Split(iAdjunto.Nombre, ".").Length - 1)
                        iAdjunto.Guid = .Item("GUID")
                        .Item("SIZE") = iAdjunto.Size
                        .Item("SIZEUNIT") = iAdjunto.SizeUnit
                        .Item("TIPOADJUNTO") = iAdjunto.TipoAdjunto
                        .Item("URL") = iAdjunto.Url
                    End With
                    dtAdjuntos.Rows.Add(rAdjunto)
                Next
            End If
            For Each item As Object In imagenesCKEditor
                rAdjunto = dtAdjuntos.NewRow
                With rAdjunto
                    .Item("NOMBRE") = ""
                    .Item("GUID") = item("guidID")
                    .Item("SIZE") = 0
                    .Item("SIZEUNIT") = ""
                    .Item("TIPOADJUNTO") = 0
                    .Item("URL") = System.Configuration.ConfigurationManager.AppSettings("temp") & "\" & item("path") & "\" & item("filename")
                End With
                dtAdjuntos.Rows.Add(rAdjunto)
            Next
        Catch ex As Exception
            Throw ex
        End Try
        Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_User").CiaComp
        Dim IdRespuestaNueva As Integer = ocnMensajes.InsertarRespuesta(lCiaComp, oRespuesta, dtAdjuntos)
        Return IdRespuestaNueva
    End Function
    ''' <summary>
    ''' marcar un "MeGusta"
    ''' </summary>
    ''' <param name="idMensaje">Id de mensaje</param>
    ''' <param name="idRespuesta">Id Respuesta de mensaje</param>
    ''' <param name="timezoneoffset">diferencia horario usuario y servidor</param>
    ''' <remarks>Llamada desde: PMPortalWeb\script\cn\js\cn_ui_meGusta.js ; Tiempo máximo: 0,2</remarks>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function MeGustaMensaje(ByVal idMensaje As Integer, ByVal idRespuesta As Integer, ByVal timezoneoffset As Double) As cnMensaje
        Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
        Dim ocnMensajes As PMPortalServer.cnMensajes
        Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
        Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_User").CiaComp

        ocnMensajes = PMPortalServer.Get_Object(GetType(PMPortalServer.cnMensajes))

        With CN_Usuario
            Return ocnMensajes.InsertarMeGusta(lCiaComp, .CodProveGS, .IdContacto, .Idioma, .DateFormat.ShortDatePattern, _
                                               .NumberFormat, idMensaje, idRespuesta, timezoneoffset)
        End With
    End Function
#End Region
#Region "Buscador"
    ''' <summary>
    ''' Busqueda entre los mensajes
    ''' </summary>
    ''' <param name="tipoOrdenacion">tipo Ordenacion</param>
    ''' <param name="textoBusqueda">texto Busqueda </param>
    ''' <param name="timezoneoffset">diferencia horario usuario y servidor</param>
    ''' <returns>objeto mensajes</returns>
    ''' <remarks>Llamada desde: PMPortalWeb\script\cn\js\cn_funciones.js ; Tiempo máximo: 0,2</remarks>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function ObtenerResultadosBusqueda(ByVal tipoOrdenacion As Integer, ByVal textoBusqueda As String, _
                                              ByVal timezoneoffset As Double) As Object

        Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
        Dim ocnMensajes As PMPortalServer.cnMensajes
        Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
        Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_User").CiaComp

        Dim _grammar As New SearchGrammar
        Dim _language As LanguageData
        Dim _parser As Parser
        Dim _parseTree As ParseTree

        _language = New LanguageData(_grammar)
        _parser = New Parser(_language)
        Try
            _parser.Parse(textoBusqueda.ToLower, "<source>")
        Catch ex As Exception
            Throw
        Finally
            _parseTree = _parser.Context.CurrentParseTree
        End Try

        Dim iRunner = TryCast(_grammar, ICanRunSample)
        Dim args = New RunSampleArgs(_language, textoBusqueda.ToLower, _parseTree)
        Dim stringFTS As String = iRunner.RunSample(args)

        ocnMensajes = PMPortalServer.Get_Object(GetType(PMPortalServer.cnMensajes))
        Dim oMensajes As New List(Of cnMensaje)
        With CN_Usuario
            oMensajes = ocnMensajes.ObtenerResultadosBusqueda(lCiaComp, .CodProveGS, .IdContacto, .Idioma, CultureInfo.CreateSpecificCulture(CN_Usuario.refCultural).LCID, _
                                        .DateFormat.ShortDatePattern, .NumberFormat, tipoOrdenacion, stringFTS, timezoneoffset)
        End With

        If oMensajes.Count > 0 Then
            Dim oMensajesYPalabras As New List(Of Object)
            Dim listaPalabras As New List(Of String)
            For Each item As Irony.Parsing.Token In args.ParsedSample.Tokens
                If item.Terminal.ToString = "Phrase" Then
                    listaPalabras.Add(Replace(item.Text, """", ""))
                ElseIf item.Terminal.ToString = "Term" Then
                    listaPalabras.Add(item.Text)
                End If
            Next

            oMensajesYPalabras.Add(oMensajes)
            oMensajesYPalabras.Add(listaPalabras)

            Return oMensajesYPalabras
        Else
            Return oMensajes
        End If
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function AyudaBuscador()
        Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
        Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
        Dim oTextos As DataTable = HttpContext.Current.Cache("Textos_" & CN_Usuario.Idioma.ToString & "_" & Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Colaboracion).Tables(0)

        Dim oAyudaBuscador As New cn_fsTreeViewItem
        Dim oTextoAyudaBuscador As cn_fsTreeViewItem
        With oAyudaBuscador
            .value = oTextos.Rows(122).Item(1)
            .text = oTextos.Rows(123).Item(1)
            .children = New List(Of cn_fsTreeViewItem)
            For i As Integer = 124 To 140 Step 2
                oTextoAyudaBuscador = New cn_fsTreeViewItem
                oTextoAyudaBuscador.value = oTextos.Rows(i).Item(1)
                oTextoAyudaBuscador.text = oTextos.Rows(i + 1).Item(1)
                .children.Add(oTextoAyudaBuscador)
            Next
        End With

        Return oAyudaBuscador
    End Function
#End Region
#Region "Comprobar Permisos"
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Comprobar_PermisoConsulta_Identificador(ByVal TipoEntidad As Integer, ByVal Identificador As Integer) As String
        Dim PMPortalServerRoot As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
        Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")

        Dim ruta As String = String.Empty
        Dim ocn_Data As PMPortalServer.cnData
        ocn_Data = PMPortalServerRoot.Get_Object(GetType(PMPortalServer.cnData))
        Select Case TipoEntidad
            Case 1 'SOLICITUD
                Dim oSol As Dictionary(Of String, String) = ocn_Data.Comprobar_PermisoConsulta_Solicitud(CN_Usuario.CiaComp, CN_Usuario.CodProveGS, _
                                                      CN_Usuario.Idioma, Identificador)
                If Not oSol.Count = 0 Then
                    ruta = ConfigurationManager.AppSettings("rutanormal") & "script/solicitudes/detalleSolicitud.aspx?Instancia=" & CType(oSol("instanciaSol"), Integer)
                End If
            Case 2 'CERTIFICADO
                Dim oCert As Dictionary(Of String, Integer) = ocn_Data.Comprobar_PermisoConsulta_Certificado(CN_Usuario.CiaComp, CN_Usuario.CodProve, Identificador)
                If Not oCert.Count = 0 Then
                    ruta = ConfigurationManager.AppSettings("rutanormal") & "script/certificados/extFrames.aspx?certificado=" & oCert("idCert") & "&instancia=" & oCert("instanciaCert")
                End If
            Case 3 'NOCONFORMIDAD
                Dim oNC As Dictionary(Of String, Integer) = ocn_Data.Comprobar_PermisoConsulta_NoConformidad(CN_Usuario.CiaComp, CN_Usuario.CodProveGS, Identificador)
                If Not oNC.Count = 0 Then
                    ruta = ConfigurationManager.AppSettings("rutanormal") & "script/noconformidad/extFrames.aspx?noconformidad=" & oNC("idNC") & "&instancia=" & oNC("instanciaNC")
                End If
            Case 4
            Case 5 'CONTRATO
                Dim oContrato As Dictionary(Of String, String) = ocn_Data.Comprobar_PermisoConsulta_Solicitud(CN_Usuario.CiaComp, CN_Usuario.CodProveGS, _
                                                      CN_Usuario.Idioma, Identificador)
                If Not oContrato.Count = 0 Then
                    ruta = ConfigurationManager.AppSettings("rutanormal") & _
                        "script/contratos/GestionContrato.aspx?Instancia=" & CType(oContrato("instanciaSol"), Integer) & "&Codigo=" & oContrato("codContrato") & "&Contrato=" & oContrato("contrato")
                End If
            Case 7
                Dim oFact As Dictionary(Of String, Integer) = ocn_Data.Comprobar_PermisoConsulta_Factura(CN_Usuario.CiaComp, CN_Usuario.CodProveGS, _
                                                        CN_Usuario.IdContacto, CN_Usuario.Idioma, Identificador)
                If Not oFact.Count = 0 Then
                    ruta = ConfigurationManager.AppSettings("rutanormal") & "script/facturas/DetalleFactura.aspx?ID=" & oFact("idFact")
                End If
            Case Else
                Return Nothing
        End Select
        Return IIf(ruta = String.Empty, Nothing, ruta)
    End Function
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Comprobar_PermisoConsulta_FichaCalidad(ByVal ProveCod As String) As String
        Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
        Return IIf(Not ProveCod = CN_Usuario.CodProveGS, Nothing, ConfigurationManager.AppSettings("rutanormal") & "script/variablescalidad/PuntuacionesProveedor.aspx")
    End Function
#End Region
#Region "UserProfile"
    ''' <summary>
    ''' Guarda la foto en el directorio y, en base de datos, las notificaciones en el Perfil de Usuario.
    ''' </summary>
    ''' <param name="pathFoto">Path de la foto del perfil de usuario</param>
    ''' <param name="userProfile">Lista de notificaciones del perfil de usuario</param>
    ''' <remarks></remarks>
    ''' <revisado>JVS 08/02/2012</revisado>
    <Services.WebMethod(EnableSession:=True)> _
    <Script.Services.ScriptMethod()> _
    Public Sub UpdateUserProfile(ByVal pathFoto As String, ByVal userProfile As Object)
        Try
            Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
            Dim bNotificarResumenActividad As Boolean = IIf(userProfile("notificarResumenActividad") = 1, True, False)
            Dim bConfiguracionDesocultar As Boolean = IIf(userProfile("configuracionDesocultar") = 1, True, False)

            If pathFoto <> "" Then
                '1.-si hay foto guardamos la foto en bd
                '2.-cambiar las propiedades que tiene el usuario.
                Dim sNombreImagen As String = CreaImagenThubmnail(pathFoto, 80)
                CN_Usuario.CN_Actualizar_Imagen_Usuario(CN_Usuario.CiaComp, sNombreImagen, CN_Usuario.CodProveGS, CN_Usuario.IdContacto, CN_Usuario.CNGuidImagenUsu)
                CN_Usuario.CNGuidImagenUsu = CN_Usuario.CNGuidImagenUsu
            End If

            CN_Usuario.CNNotificarResumenActividad = bNotificarResumenActividad
            CN_Usuario.CNConfiguracionDesocultar = bConfiguracionDesocultar
            CN_Usuario.CN_Update_Opciones_Usuario(CN_Usuario.CiaComp, CN_Usuario.CodProveGS, CN_Usuario.IdContacto, bNotificarResumenActividad, bConfiguracionDesocultar)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Crea thumbnail de la foto de Usuario.
    ''' </summary>
    ''' <param name="pathOrigen">Path origen de la foto del perfil de usuario</param>
    ''' <param name="width">Anchura de la foto</param>
    ''' <param name="bSinFoto">No se habÃƒÆ’Ã‚Â­a seleccionado foto previamente</param>
    ''' <param name="sViejoGuid">Guid de la foto</param>
    ''' <remarks></remarks>
    ''' <revisado>JVS 14/02/2012</revisado>
    Private Function CreaImagenThubmnail(ByVal pathOrigen As String, ByVal width As Integer) As String
        Try
            ' Declaraciones
            Dim sNuevoGuid As String
            Dim objImage, objThumbnail As System.Drawing.Image
            Dim strUserImagePath, strImageFilename As String
            Dim shtWidth, shtHeight As Short
            Dim sExtension As String
            Dim sGuid As String
            ' Obtener path de la carpeta de imÃƒÆ’Ã‚Â¡genes en el servidor
            strUserImagePath = System.Configuration.ConfigurationManager.AppSettings("temp") & "\"
            sExtension = Mid(Trim(pathOrigen), (InStrRev(Trim(pathOrigen), ".") + 1))
            ' Recuperar nombre de fichero para redimensionar de la lista de parÃƒÆ’Ã‚Â¡metros
            sNuevoGuid = Guid.NewGuid.ToString
            strImageFilename = strUserImagePath & sNuevoGuid
            ' Recupera el fichero
            objImage = Drawing.Image.FromFile(pathOrigen)
            ' Recuperar ancho de la lista de parÃƒÆ’Ã‚Â¡metros
            If width = Nothing Then
                shtWidth = objImage.Width
            ElseIf width < 1 Then
                shtWidth = 100
            Else
                shtWidth = width
            End If
            ' CÃƒÆ’Ã‚Â¡lculo de una altura proporcional a la anchura
            shtHeight = objImage.Height / (objImage.Width / shtWidth)
            ' Crear thumbnail
            objThumbnail = objImage.GetThumbnailImage(shtWidth, shtHeight, Nothing, System.IntPtr.Zero)
            ' Enviar al cliente
            objThumbnail.Save(strImageFilename, FormatoImagen(sExtension))
            ' Liberar
            objImage.Dispose()
            objThumbnail.Dispose()

            Return strImageFilename
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sExt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function FormatoImagen(ByVal sExt As String) As System.Drawing.Imaging.ImageFormat
        Select Case sExt.ToLower
            Case "bmp"
                Return System.Drawing.Imaging.ImageFormat.MemoryBmp
            Case "emf"
                Return System.Drawing.Imaging.ImageFormat.Emf
            Case "exif"
                Return System.Drawing.Imaging.ImageFormat.Exif
            Case "gif"
                Return System.Drawing.Imaging.ImageFormat.Gif
            Case "ico"
                Return System.Drawing.Imaging.ImageFormat.Icon
            Case "jpg", "jpeg"
                Return System.Drawing.Imaging.ImageFormat.Jpeg
            Case "png"
                Return System.Drawing.Imaging.ImageFormat.Png
            Case "tiff"
                Return System.Drawing.Imaging.ImageFormat.Tiff
            Case "wmf"
                Return System.Drawing.Imaging.ImageFormat.Wmf
            Case Else
                Return System.Drawing.Imaging.ImageFormat.Jpeg
        End Select
    End Function
#End Region
End Class