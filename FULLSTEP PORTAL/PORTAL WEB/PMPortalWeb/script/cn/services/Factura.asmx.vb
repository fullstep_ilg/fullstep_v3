﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Fullstep
Imports System.Web
Imports System.Text
Imports System.IO
Imports System.Globalization
Imports Irony.Parsing
Imports System.Collections.Generic

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Script.Services.ScriptService()> _
<ToolboxItem(False)> _
Public Class Factura
    Inherits System.Web.Services.WebService

#Region "Discrepancias"
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Function Obtener_Datos_NuevaDiscrepancia_Proveedor(ByVal factura As Integer, ByVal linea As Integer) As List(Of cn_fsItem)
        Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
        Dim ocnInfo As PMPortalServer.cnDiscrepancias
        ocnInfo = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
        Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")

        Dim lCiaComp As Long = CN_Usuario.CiaComp
        Dim info As List(Of cn_fsItem)

        With CN_Usuario
            info = ocnInfo.Obtener_Datos_NuevaDiscrepancia_Proveedor(factura, linea, lCiaComp, .Idioma)
        End With

        Return info
    End Function
    <Services.WebMethod(True)> _
    <Script.Services.ScriptMethod()> _
    Public Sub InsertarDiscrepancia(ByVal tipo As Integer, ByVal mensaje As Object, ByVal categoria As Object, _
                                    ByVal para As Object, ByVal adjuntos As Object, ByVal imagenesCKEditor As Object, _
                                    ByVal factura As Integer, ByVal linea As Integer)
        Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
        Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
        Dim oMensaje As New cnMensaje
        Dim Contenido As String = mensaje("contenido").ToString
        For Each item As Object In imagenesCKEditor
            item("guidID") = Guid.NewGuid.ToString
            Contenido = Replace(Contenido, HttpUtility.HtmlEncode(item("href")), ConfigurationManager.AppSettings("rutaFS") & "cn/" & "Thumbnail.ashx?f=" & _
                                            item("guidID") & "&t=1")
        Next
        With oMensaje
            .IdMensaje = 0
            .FechaAlta = DateAdd(DateInterval.Minute, CType(mensaje("timezoneOffset"), Integer), DateTime.Parse(mensaje("fechaAlta"), CN_Usuario.DateFormat))
            .EnProceso = "x" & mensaje("id") & "x"
            .Categoria = New cnCategoria
            .InfoUsuario = New cnUsuario
            .MeGusta = New cnMeGusta
            .Categoria.Id = categoria("id")
            .Categoria.Denominacion = categoria("denominacion")
            .InfoUsuario.ProveCod = CN_Usuario.CodProveGS
            .InfoUsuario.Con = CN_Usuario.IdContacto
            .Titulo = mensaje("titulo")
            .Contenido = Contenido
            .Tipo = CType(tipo, Integer)
            If .Tipo = 2 Then
                .Fecha = DateAdd(DateInterval.Minute, CType(mensaje("timezoneOffset"), Integer), DateTime.Parse(mensaje("fecha") & " " & mensaje("hora"), CN_Usuario.DateFormat))
                Threading.Thread.CurrentThread.CurrentCulture = New CultureInfo(CN_Usuario.Idioma)
                .MesCorto = StrConv(MonthName(CType(.Fecha, DateTime).Month, True), VbStrConv.ProperCase)
                .Dia = CType(.Fecha, DateTime).Day
                .Hora = CType(.Fecha, DateTime).ToString("t")
                Dim FechaHora As DateTime
                Dim TimeZoneOffSet As Double = CType(mensaje("timezoneOffset"), Integer)
                FechaHora = TimeValue((Math.Abs(TimeZoneOffSet) \ 60) & ":" & Math.Round((((-TimeZoneOffSet / 60) - (-TimeZoneOffSet \ 60)) * 60), 2))
                .Cuando = CType(.Fecha, DateTime).ToString("dddd, dd MMMM yyyy HH:mm", CultureInfo.CreateSpecificCulture(CN_Usuario.Idioma)) & _
                    " (GMT" & IIf(TimeZoneOffSet = 0, "", IIf(TimeZoneOffSet > 0, " -", " +") & FechaHora.ToString("hh:mm")) & ")"
                .Donde = mensaje("donde")
            End If

            .Para = New cnMensajePara
            ObtenerPara_Mensaje(.Para, para)

            Dim adjunto As cnAdjunto
            .Adjuntos = New List(Of cnAdjunto)
            For Each item As Object In adjuntos
                If Not item.Count = 0 Then
                    adjunto = New cnAdjunto
                    With adjunto
                        .Nombre = item("nombre")
                        .Size = modUtilidades.Numero(item("size"), CN_Usuario.NumberFormat.NumberDecimalSeparator)
                        .SizeUnit = item("sizeunit")
                        .SizeToString = modUtilidades.FormatNumber(.Size, CN_Usuario.NumberFormat) & " " & .SizeUnit
                        .TipoAdjunto = item("tipoadjunto")
                        .Url = item("url")
                    End With
                    .Adjuntos.Add(adjunto)
                End If
            Next
        End With

        Dim ocnDiscrepancias As PMPortalServer.cnDiscrepancias
        ocnDiscrepancias = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))

        ocnDiscrepancias.InsertarDiscrepancia(CN_Usuario.CiaComp, oMensaje, factura, linea)
        For Each iAdjunto As cnAdjunto In oMensaje.Adjuntos
            File.Copy(iAdjunto.Url, System.Configuration.ConfigurationManager.AppSettings("rutaCNArchivos") & "\adjuntos\" & iAdjunto.Guid, False)
        Next

        For Each item As Object In imagenesCKEditor
            File.Copy(System.Configuration.ConfigurationManager.AppSettings("temp") & "\" & item("path") & "\" & item("filename"), _
                      System.Configuration.ConfigurationManager.AppSettings("rutaCNArchivos") & "\adjuntos\" & item("guidID"), False)
        Next
    End Sub
#End Region
#Region "Carga inicial"
    

    ' ''' <summary>
    ' ''' Obtener la categoría Facturas > Discrepancias
    ' ''' </summary>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    '<Services.WebMethod(True)> _
    '<Script.Services.ScriptMethod()> _
    'Public Function Obtener_Categoria_Discrepancias() As cn_fsItem
    '    Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
    '    Dim ocnInfo As PMPortalServer.cnDiscrepancias
    '    ocnInfo = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
    '    Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

    '    Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
    '    Dim item As cn_fsItem

    '    With CN_Usuario
    '        item = ocnInfo.Obtener_Categoria_Discrepancias(lCiaComp, .Idioma)
    '    End With

    '    Return item
    'End Function
    ' ''' <summary>
    ' ''' Determina a quien enviar la discrepancia (gestor / receptor de la línea de factura)
    ' ''' </summary>
    ' ''' <returns>GestorReceptor</returns>
    ' ''' <remarks>JVS 20/06/2012</remarks>
    '<Services.WebMethod(True)> _
    '<Script.Services.ScriptMethod()> _
    'Public Function Para_Gestor_Receptor_Discrep_LineaFactura() As String
    '    Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
    '    Dim ocnInfo As PMPortalServer.cnDiscrepancias
    '    ocnInfo = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
    '    Dim sGestorReceptor As String
    '    Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

    '    sGestorReceptor = ocnInfo.Para_Gestor_Receptor_Discrep_LineaFactura(lCiaComp)
    '    Return sGestorReceptor
    'End Function
    ' ''' <summary>
    ' ''' Obtener el gestor de la línea de factura
    ' ''' </summary>
    ' ''' <param name="Factura">Id. de factura</param>
    ' ''' <param name="Linea">Linea de factura</param>
    ' ''' <returns>Gestor</returns>
    ' ''' <remarks>JVS 19/06/2012</remarks>
    '<Services.WebMethod(True)> _
    '<Script.Services.ScriptMethod()> _
    'Public Function Obtener_Gestor_LineaFactura(ByVal Factura As Integer, ByVal Linea As Integer) As cn_fsItem
    '    Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
    '    Dim ocnInfo As PMPortalServer.cnDiscrepancias
    '    ocnInfo = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
    '    Dim item As cn_fsItem
    '    Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

    '    item = ocnInfo.Obtener_Gestor_LineaFactura(lCiaComp, Factura, Linea)
    '    Return item
    'End Function
    ' ''' <summary>
    ' ''' Obtener el receptor de la línea de factura
    ' ''' </summary>
    ' ''' <param name="Factura">Id. de factura</param>
    ' ''' <param name="Linea">Linea de factura</param>
    ' ''' <returns>Receptor</returns>
    ' ''' <remarks>JVS 19/06/2012</remarks>
    '<Services.WebMethod(True)> _
    '<Script.Services.ScriptMethod()> _
    'Public Function Obtener_Receptor_LineaFactura(ByVal Factura As Integer, ByVal Linea As Integer) As cn_fsItem
    '    Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
    '    Dim ocnInfo As PMPortalServer.cnDiscrepancias
    '    ocnInfo = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
    '    Dim item As cn_fsItem
    '    Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

    '    item = ocnInfo.Obtener_Receptor_LineaFactura(lCiaComp, Factura, Linea)
    '    Return item
    'End Function
    ' ''' <summary>
    ' ''' Obtener los datos de la línea de factura
    ' ''' </summary>
    ' ''' <param name="IdFactura">Id. de factura</param>
    ' ''' <param name="IdLinea">Linea de factura</param>
    ' ''' <returns>Objeto línea de factura</returns>
    ' ''' <remarks>JVS 19/06/2012</remarks>
    '<Services.WebMethod(True)> _
    '<Script.Services.ScriptMethod()> _
    'Public Function Obtener_Datos_LineaFactura(ByVal IdFactura As Integer, ByVal IdLinea As Integer) As Object
    '    Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
    '    Dim ocnInfo As PMPortalServer.cnDiscrepancias
    '    ocnInfo = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
    '    Dim item As Object
    '    Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

    '    item = ocnInfo.Obtener_Datos_LineaFactura(lCiaComp, IdFactura, IdLinea)
    '    Return item
    'End Function
    ' ''' <summary>
    ' ''' Obtener el proveedor de la factura
    ' ''' </summary>
    ' ''' <param name="Factura">Id. de factura</param>  
    ' ''' <returns>Proveedor</returns>
    ' ''' <remarks>JVS 22/06/2012</remarks>
    '<Services.WebMethod(True)> _
    '<Script.Services.ScriptMethod()> _
    'Public Function Obtener_Proveedor_Factura(ByVal Factura As Integer) As cn_fsItem
    '    Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
    '    Dim ocnInfo As PMPortalServer.cnDiscrepancias
    '    ocnInfo = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
    '    Dim item As cn_fsItem
    '    'Dim lCiaComp As Long = HttpContext.Current.Request.Cookies("USU")("CIACOMP")
    '    Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

    '    item = ocnInfo.Obtener_Proveedor_Factura(lCiaComp, Factura)
    '    Return item
    'End Function
    ' ''' <summary>
    ' ''' Obtener las líneas de la factura
    ' ''' </summary>
    ' ''' <param name="Factura">Id. de factura</param>  
    ' ''' <returns>Líneas de factura</returns>
    ' ''' <remarks>JVS 22/06/2012</remarks>
    '<Services.WebMethod(True)> _
    '<Script.Services.ScriptMethod()> _
    'Public Function Obtener_Lineas_Factura(ByVal Factura As Integer) As Object
    '    Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
    '    Dim ocnInfo As PMPortalServer.cnDiscrepancias
    '    ocnInfo = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
    '    Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
    '    Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

    '    Dim oLineas As New List(Of PMPortalServer.cnLineaFactura)
    '    With CN_Usuario
    '        oLineas = ocnInfo.Obtener_Lineas_Factura(lCiaComp, .Idioma, Factura)
    '    End With
    '    Return oLineas
    'End Function
    ' ''' <summary>
    ' ''' Guarda en base de datos la discrepancia.
    ' ''' </summary>
    ' ''' <remarks></remarks>
    '<Services.WebMethod(True)> _
    '<Script.Services.ScriptMethod()> _
    'Public Function InsertarDiscrepancia(ByVal idMensaje As Integer, ByVal factura As Integer, ByVal linea As Integer) As Integer
    '    Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
    '    Dim ocnInfo As PMPortalServer.cnDiscrepancias
    '    ocnInfo = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
    '    Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
    '    Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

    '    Dim oLineas As New List(Of PMPortalServer.cnLineaFactura)
    '    Dim IdDiscrepancia As Integer = ocnInfo.InsertarDiscrepancia(lCiaComp, idMensaje, factura, linea)

    '    Return IdDiscrepancia
    'End Function
    ' ''' <summary>
    ' ''' 
    ' ''' </summary>
    ' ''' <param name="tipo"></param>
    ' ''' <param name="pagina"></param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    '<Services.WebMethod(EnableSession:=True)> _
    '<Script.Services.ScriptMethod()> _
    'Public Function ObtenerDiscrepanciasLineaFactura(ByVal factura As Integer, ByVal linea As Integer, ByVal tipo As Integer, ByVal grupo As Integer, ByVal categoria As Integer, _
    '                                       ByVal megusta As Boolean, ByVal pagina As Integer, ByVal timezoneoffset As Double, _
    '                                       ByVal historico As Boolean) As List(Of cnMensaje)
    '    Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
    '    Dim ocnMensajes As PMPortalServer.cnMensajes
    '    Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")

    '    ocnMensajes = PMPortalServer.Get_Object(GetType(PMPortalServer.cnMensajes))

    '    Dim LimiteMensajesCargados As String = CType(System.Configuration.ConfigurationManager.AppSettings("LimiteMensajesCargados"), Integer)
    '    Dim oMensajes As New List(Of cnMensaje)
    '    Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

    '    With CN_Usuario
    '        oMensajes = ocnMensajes.ObtenerDiscrepanciasLineaFactura(lCiaComp, .ProveedorGS, .IdContacto, .ContactoGS, .Idioma, .DateFormat.ShortDatePattern, .NumberFormat, factura, linea, tipo, grupo, categoria, _
    '                                                       megusta, LimiteMensajesCargados, pagina, timezoneoffset, historico)
    '    End With

    '    Return oMensajes
    'End Function
#End Region
#Region "Otras funciones"
    Private Sub ObtenerPara_Mensaje(ByRef ParaMensaje As cnMensajePara, ByVal para As Object)
        Dim row As DataRow
        For Each item As Object In para
            Select Case item.key
                Case "UON"
                    For Each iUON As Object In item.value
                        If ParaMensaje.Para_UON Is Nothing Then
                            ParaMensaje.Para_UON = New DataTable

                            ParaMensaje.Para_UON.Columns.Add("UON1", GetType(System.String))
                            ParaMensaje.Para_UON.Columns.Add("UON2", GetType(System.String))
                            ParaMensaje.Para_UON.Columns.Add("UON3", GetType(System.String))
                            ParaMensaje.Para_UON.Columns.Add("DEP", GetType(System.String))
                            ParaMensaje.Para_UON.Columns.Add("USU", GetType(System.String))
                        End If
                        If iUON("UON1") Is Nothing AndAlso iUON("UON2") Is Nothing AndAlso iUON("UON3") Is Nothing AndAlso iUON("DEP") Is Nothing AndAlso iUON("USU") Is Nothing Then
                            ParaMensaje.Para_UON_UON0 = True
                        Else
                            row = ParaMensaje.Para_UON.NewRow
                            row("UON1") = iUON("UON1")
                            row("UON2") = iUON("UON2")
                            row("UON3") = iUON("UON3")
                            row("DEP") = iUON("DEP")
                            row("USU") = iUON("USU")

                            ParaMensaje.Para_UON.Rows.Add(row)
                        End If
                    Next
                Case "PROVE"
                    Dim contactoProveedor As DataRow
                    For Each iProve As Object In item.value
                        If iProve("TIPOPROVE") IsNot Nothing Then
                            Select Case iProve("TIPOPROVE")
                                Case 1
                                    ParaMensaje.Para_Prove_Tipo = 1
                                Case 2
                                    ParaMensaje.Para_Prove_Tipo = 2
                                Case 3
                                    ParaMensaje.Para_Prove_Tipo = 3
                                Case 4
                                    ParaMensaje.Para_Prove_Tipo = 4
                                Case 5
                                    ParaMensaje.Para_Prove_Tipo = 5
                                    ParaMensaje.Para_Material_QA = iProve("MATQA")
                                Case 6
                                    ParaMensaje.Para_Prove_Tipo = 6
                                    ParaMensaje.Para_Material_GMN1 = iProve("GMN1")
                                    ParaMensaje.Para_Material_GMN2 = iProve("GMN2")
                                    ParaMensaje.Para_Material_GMN3 = iProve("GMN3")
                                    ParaMensaje.Para_Material_GMN4 = iProve("GMN4")
                                Case Else
                                    If ParaMensaje.Para_Prove_Con Is Nothing Then
                                        ParaMensaje.Para_Prove_Con = New DataTable
                                        ParaMensaje.Para_Prove_Con.Columns.Add("PROVE", GetType(System.String))
                                        ParaMensaje.Para_Prove_Con.Columns.Add("CON", GetType(System.Int64))
                                    End If
                                    contactoProveedor = ParaMensaje.Para_Prove_Con.NewRow
                                    contactoProveedor("PROVE") = iProve("CODIGOPROVE")
                                    contactoProveedor("CON") = iProve("IDCONTACTO")

                                    ParaMensaje.Para_Prove_Con.Rows.Add(contactoProveedor)
                            End Select
                        End If
                    Next
                Case "PROCECOMPRAS"
                    For Each iProceCompra As Object In item.value
                        If iProceCompra("TIPOPARAPROCECOMPRA") IsNot Nothing Then
                            With ParaMensaje
                                Select Case iProceCompra("TIPOPARAPROCECOMPRA")
                                    Case "Todos"
                                        .Para_ProcesoCompra_Compradores = True
                                        .Para_ProcesoCompra_Invitados = True
                                        .Para_ProcesoCompra_Proveedores = True
                                        .Para_ProcesoCompra_Responsable = True
                                    Case "Responsable"
                                        .Para_ProcesoCompra_Responsable = True
                                    Case "Invitados"
                                        .Para_ProcesoCompra_Invitados = True
                                    Case "Compradores"
                                        .Para_ProcesoCompra_Compradores = True
                                    Case "Proveedores"
                                        .Para_ProcesoCompra_Proveedores = True
                                End Select
                                .Para_ProceCompra_Anyo = CType(iProceCompra("ANYO"), Integer)
                                .Para_ProceCompra_GMN1 = iProceCompra("GMN1")
                                .Para_ProceCompra_Cod = CType(iProceCompra("COD"), Integer)
                            End With
                        End If
                    Next
                Case "ESTRUCCOMPRAS"
                    Dim estrucCompras As DataRow
                    For Each iEstrucCompras As Object In item.value
                        If iEstrucCompras("TIPOESTRUCCOMPRAS") IsNot Nothing Then
                            Select Case iEstrucCompras("TIPOESTRUCCOMPRAS")
                                Case 3
                                    ParaMensaje.Para_EstructuraCompras_MaterialGS = True
                                    ParaMensaje.Para_Material_GMN1 = iEstrucCompras("GMN1")
                                    ParaMensaje.Para_Material_GMN2 = iEstrucCompras("GMN2")
                                    ParaMensaje.Para_Material_GMN3 = iEstrucCompras("GMN3")
                                    ParaMensaje.Para_Material_GMN4 = iEstrucCompras("GMN4")
                                Case Else
                                    If ParaMensaje.Para_EstructuraCompras_Equipo Is Nothing Then
                                        ParaMensaje.Para_EstructuraCompras_Equipo = New DataTable
                                        ParaMensaje.Para_EstructuraCompras_Equipo.Columns.Add("EQP", GetType(System.String))
                                        ParaMensaje.Para_EstructuraCompras_Equipo.Columns.Add("USU", GetType(System.String))
                                    End If
                                    estrucCompras = ParaMensaje.Para_EstructuraCompras_Equipo.NewRow
                                    With estrucCompras
                                        Select Case iEstrucCompras("TIPOESTRUCCOMPRAS")
                                            Case "1"
                                                estrucCompras("EQP") = iEstrucCompras("CODIGO")
                                                estrucCompras("USU") = Nothing
                                            Case "2"
                                                estrucCompras("EQP") = Nothing
                                                estrucCompras("USU") = iEstrucCompras("CODIGO")
                                        End Select
                                    End With
                                    ParaMensaje.Para_EstructuraCompras_Equipo.Rows.Add(estrucCompras)
                            End Select
                        End If
                    Next
                Case "GRUPO"
                    For Each iGrupo As Object In item.value
                        If iGrupo("TIPOGRUPO") Is Nothing Then
                            Dim idGrupo As DataRow
                            If ParaMensaje.Para_Grupos_Grupos Is Nothing Then
                                ParaMensaje.Para_Grupos_Grupos = New DataTable
                                ParaMensaje.Para_Grupos_Grupos.Columns.Add("GRUPO", GetType(System.Int64))
                            End If
                            idGrupo = ParaMensaje.Para_Grupos_Grupos.NewRow
                            idGrupo("GRUPO") = iGrupo("IDGRUPO")

                            ParaMensaje.Para_Grupos_Grupos.Rows.Add(idGrupo)
                        Else
                            Select Case iGrupo("TIPOGRUPO")
                                Case "EP"
                                    ParaMensaje.Para_Grupo_EP = True
                                Case "GS"
                                    ParaMensaje.Para_Grupo_GS = True
                                Case "PM"
                                    ParaMensaje.Para_Grupo_PM = True
                                Case "QA"
                                    ParaMensaje.Para_Grupo_QA = True
                                Case "SM"
                                    ParaMensaje.Para_Grupo_SM = True
                            End Select
                        End If
                    Next
            End Select
        Next
    End Sub
#End Region
End Class