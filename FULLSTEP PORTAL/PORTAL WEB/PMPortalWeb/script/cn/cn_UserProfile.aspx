﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="cn_UserProfile.aspx.vb" Inherits=".cn_UserProfile" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../../common/menu.asp"></script>
</head>
<body class="bodyMuroUsuario">
    <script type="text/javascript">dibujaMenu(8)</script>
    <form id="form1" runat="server">    
        <asp:ScriptManager ID="ScriptManager1" runat="server">
		    <Scripts>
			    <asp:ScriptReference Path="~/js/jquery/jquery.min.js" />
                <asp:ScriptReference Path="~/js/jquery/jquery-migrate.min.js" />
			    <asp:ScriptReference Path="~/js/jquery/jquery.json.min.js" />
			    <asp:ScriptReference Path="~/js/jquery/jquery.tmpl.min.js" />
			    <asp:ScriptReference Path="~/js/jquery/jquery.ui.min.js" />
			    <asp:ScriptReference Path="~/js/jquery/tmpl.min.js" />
			    <asp:ScriptReference Path="~/script/cn/js/cn_UserProfile_init.js" />
		    </Scripts>
	    </asp:ScriptManager>
        <div id="divCabecera" style="clear: both; float: left; width: 100%; margin-top:15px;">
            <fsn:FSNPageHeader ID="FSNPageHeader" runat="server" UrlImagenCabecera="~/images/ColaboracionHeader.png" />
        </div>
        <!-- Menú izquierda -->
        <div id="divMenu" class="divMenu"></div>
        <div id="divPrincipal" class="divPrincipal" style="display:none;">
            <div id="divTituloMain" class="SeparadorInf" style="clear:both; float:left; margin:15px 0px 15px 10px;">
                <asp:Label ID="lblTitulo" runat="server" CssClass="Texto14 Negrita TextoResaltado"></asp:Label>
            </div>
            <div style="clear:both; float:left; margin-top:5px; margin-left:10px;">
                <asp:Label ID="lblNombreEtiq" runat="server" Text="DNombre:" CssClass="Texto12"></asp:Label>
                <asp:Label ID="lblNombre" runat="server" CssClass="Texto12"></asp:Label>
            </div>        
            <div id="divImagenAdjuntos" style="clear:both; float:left; margin-top:5px; margin-left:10px;">    
                <div id="divFotoEtiq" style="float:left;">
                    <asp:Label ID="lblFotoEtiq" runat="server" Text="DFoto:" CssClass="Texto12"></asp:Label>
                </div>
                <div class="fileupload-content">
                    <div id="tablaadjuntos" class="files" usu="true" style="float:left; margin-left:5px;">
                        <div class="preview" style="float:left; width:85px; text-align:center;">
                            <asp:Image ID="imgFoto" runat="server" CssClass="LoggedUserImage" />
                        </div>
                    </div>
                    <div class="fileupload-progressbar"></div>
                </div>
                <div id="Perfil" style="clear:both; float:left; margin-top:5px; height:25px;">
                    <div id="ModificarFotoPerfil" class="OtrasOpciones">
                        <asp:Label ID="lblModifFoto" runat="server" Text="DModificar la foto de perfil" CssClass="Texto12 Subrayado"></asp:Label>
                    </div>
                </div>      
            </div>        
            <div id="divTituloNotificaciones" class="SeparadorInf" style="clear:both; float:left; margin-top:10px; margin-left:10px;">
                <img alt="" id="imgNotificaciones" />
                <asp:Label ID="lblNotificaciones" runat="server" Text="DNotificaciones vía email" CssClass="Texto12 Negrita"></asp:Label>
            </div>
            <div id="divNotif" style="clear:both; float:left; margin-top:5px; margin-left:10px;">
                <asp:CheckBox ID="chkNotificarNuevaActividad" runat="server" CssClass="Texto12" style="clear:both; float:left;" />
            </div>
            <div id="divConfig" class="SeparadorInf" style="clear:both; float:left; margin-top:10px; margin-left:10px;">
                <img alt="" id="imgConfiguraciones" />
                <asp:Label ID="lblConfiguraciones" runat="server" Text="DConfiguraciones" CssClass="Texto12 Negrita"></asp:Label>
            </div>
            <div style="clear:both; float:left; margin-top:5px; margin-left:10px;">
                <asp:CheckBox ID="chkMostrarOcultosActividad" runat="server" CssClass="Texto12" style="clear:both; float:left;" />
            </div>
            <div id="divBotones" style="clear:both; float:left; padding-right:5%; margin-top:10px; margin-left:10px;">
                <div id="btnAceptarUserProfile" class="botonRedondeado" style="float:left;">
                    <span id="lblUserProfileAceptar" class="Texto12 TextoClaro"></span>
                </div>
                <div id="btnCancelarUserProfile" class="botonRedondeado" style="float:left; margin-left:10px;">
                    <span id="lblUserProfileCancelar" class="Texto12 TextoClaro"></span>
                </div>
                <div style="float:left; margin-left:10px;">
                    <span id="lblPerfilGuardadoOK" class="Texto14 Negrita TextoAlternativo" style="display:none;"></span>
                </div>
            </div>        
        </div>
    </form>
    <div id="fileupload" style="display:none;">
		<form id="formFileupload" method="post" enctype="multipart/form-data">
			<div class="fileupload-buttonbar">
				<label class="fileinput-button">                    
					<input type="file" name="files[]" multiple="multiple" />
				</label>
				<div class="fileupload-progressbar"></div>
			</div>
		</form>
	</div>
</body>
</html>
