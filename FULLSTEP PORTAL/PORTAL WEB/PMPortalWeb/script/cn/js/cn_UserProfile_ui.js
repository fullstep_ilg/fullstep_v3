﻿//CN_USERPROFILE_UI.JS
$('#btnAceptarUserProfile').live('click', function () {
    $('#divCargando').show();
    var CNNotificarResumenActividad = ($('[id$=chkNotificarNuevaActividad]').attr('checked')) ? 1 : 0;
    var CNConfiguracionDesocultar = ($('[id$=chkMostrarOcultosActividad]').attr('checked')) ? 1 : 0;
    var pathFoto = ($('.template-download').attr('data-url'));
    GuardarUserProfileBD(pathFoto, CNNotificarResumenActividad, CNConfiguracionDesocultar);    
});
$('#btnCancelarUserProfile').live('click', function () {
    document.location.href = rutanormal + 'script/cn/cn_MuroUsuario.aspx?tipo=1&pagina=1&scrollpagina=0';
});
$('#ModificarFotoPerfil').live('mouseenter', function () {
    $('#ModificarFotoPerfil').addClass('Seleccionable');
    $('#fileupload [type=file]').css('width', $('#ModificarFotoPerfil').outerWidth());
    $('#fileupload [type=file]').css('height', $('#ModificarFotoPerfil').outerHeight());
    $('#fileupload [type=file]').css('left', $('#ModificarFotoPerfil').position().left);
    $('#fileupload [type=file]').css('top', $('#ModificarFotoPerfil').position().top);
    $('#fileupload [type=file]').css('right', '');
});
var GuardadoOK;
function GuardarUserProfileBD(path, notificarResumenActividad, configuracionDesocultar) {
    var params = '';
    var userProfile = '';
    //USER PROFILE
    var userProfile =
    {
        notificarResumenActividad: '' + notificarResumenActividad + '',
        configuracionDesocultar: '' + configuracionDesocultar + ''
    };
    if (path == undefined) {
        path = '';
    };
    params = { pathFoto: path, userProfile: userProfile };
    $.when($.ajax({
        type: "POST",
        url: rutanormal + 'script/cn/services/cnMuroUsuario.asmx/UpdateUserProfile',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: false
    })).done(function () {
        $('#lblPerfilGuardadoOK').show();
        $('#lblPerfilGuardadoOK').text(TextosUser[18]);
        $('#divMenu #imgUsuImagen').attr('src', rutanormal + 'script/cn/Thumbnail.ashx?t=0&d=' + new Date());
        $('#divCargando').hide();
        GuardadoOK = setTimeout(function () { clearTimeout(GuardadoOK); $('#lblPerfilGuardadoOK').hide(); }, 3000);
    });
};