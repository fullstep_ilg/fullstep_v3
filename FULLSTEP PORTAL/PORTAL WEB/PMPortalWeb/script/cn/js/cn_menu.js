﻿//CN_MENU.JS
var usuario;
function CargarMenu() {
    $.when($.ajax({
        type: "POST",
        url: rutanormal + 'script/_common/services/User.asmx/Obtener_Usuario_CN',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        usuario = msg.d;
        $('#divMenu #imgUsuImagen').one('load', function () {
            $('#divNombreUsuario').tmpl().appendTo('#divLoggedUser');
            $('#lblUsuNombre').text(usuario.Nombre + ' ' + usuario.Apellidos);
        });
        $('#divMenu #imgUsuImagen').attr('src', rutanormal + 'script/cn/Thumbnail.ashx?t=0&d=' + new Date());         
    });
    $('#divUsuImagen,#divUsuNombre').live('click', function () {
        window.location.href = rutanormal + 'script/cn/cn_UserProfile.aspx';
    });
    $('#lblGruposTitulo').text(Textos[9]);
    $('#lblGrupos').text(Textos[10]);
    $('#Grupos').attr('title', Textos[10]);
    $('#lblAdminGrupos').text(Textos[11]);
    $('#AdminGrupos').attr('title', Textos[11]);
    $('#divGrupos').show();
    $('#lblCategoriasTitulo').text(Textos[12]);
    $('#lblCategorias').text(Textos[13]);
    $('#Categorias').attr('title', Textos[13]);
    $('#lblAdminCategorias').text(Textos[14]);
    $('#AdminCategorias').attr('title', Textos[14]);
    $('#divCategorias').show();
    $('#lblMensajesTitulo').text(Textos[1]);
    $('#lblMiContenido').text(Textos[2]);
    $('#MiContenido').attr('title', Textos[2]);
    $('#lblTemas').text(Textos[3]);
    $('#Temas').attr('title', Textos[3]);
    $('#lblEventos').text(Textos[4]);
    $('#Eventos').attr('title', Textos[4]);
    $('#lblNoticias').text(Textos[5]);
    $('#Noticias').attr('title', Textos[5]);
    $('#lblPreguntas').text(Textos[6]);
    $('#Preguntas').attr('title', Textos[6]);
    $('#lblMensajesUrgentes').text(Textos[7]);
    $('#MensajesUrgentes').attr('title', Textos[7]);
    $('#lblMeGusta').text(Textos[8]);
    $('#MeGusta').attr('title', Textos[8]);
    $('#divMensajes').show();

    $('.MenuItemMenuOpcion').live('click', function () {
        var tipo, grupo, categoria, megusta;
        megusta = false;
        $('.MenuItemMenuOpcion').removeClass('MenuItemMenuOpcionSeleccionado');
        switch (this.id.replace('Option', '')) {
            case "MiContenido":
                tipo = 0;
                break;
            case "Temas":
                tipo = 1;
                break;
            case "Eventos":
                tipo = 2;
                break;
            case "Noticias":
                tipo = 3;
                break;
            case "Preguntas":
                tipo = 4;
                break;
            case "MensajesUrgentes":
                tipo = 5;
                break;
            case "MeGusta":
                tipo = 6;
                megusta = true;
                break;
            case "AdminCategorias":
            case "AdminGrupos":
            case "Grupos":
            case "Categorias":
            default:
                break;
        }

        $(this).addClass('MenuItemMenuOpcionSeleccionado');

        if (tipo != undefined) {
            if (typeof paginaMuroUsuario == 'undefined') {
                location.href("cn_MuroUsuario.aspx?tipoMensajes=" + tipo + "&pagina=1&scrollpagina=0");
            } else {
                CargarMensajes(tipo, megusta, 1, false);
            }
        }
    });
}

function FijarMenuTrasScroll() {
    var TopPageHeader = $('#divCabecera').offset().top;

    if ($(window).scrollTop() > TopPageHeader + 43) {
        $('#divMenu').addClass('divFijarArribaMenu');
        $('.divPrincipal').addClass('divPosicionarTrasFijacion');
    } else {
        $('#divMenu').removeClass('divFijarArribaMenu');
        $('.divPrincipal').removeClass('divPosicionarTrasFijacion');
    }
}

function FijarOpcionesNuevoMensajeTrasScroll() {
    var TopPageHeader = $('#divCabecera').offset().top;

    if ($(window).scrollTop() > TopPageHeader + 43) {
        if ($('#divNuevoMensaje').css('display') == 'none') {
            $('#divNuevosMensajes').css({ 'width': $('#divNuevosMensajes').width() });
            $('#divNuevosMensajes').addClass('divFijarArribaNuevoMensaje');
            $('#divMuro').css({ 'margin-top': $('#divNuevosMensajes').height() + 20 });
        }
    } else {
        $('#divNuevosMensajes').removeClass('divFijarArribaNuevoMensaje');
        $('#divMuro').css({ 'margin-top': 0 });
    }
}

function CargarMensajes(tipo, megusta, pagina,historico) {
    $('#divMuro ul').remove();
    $('#divSiguientes').hide();

    $('#divMuro').attr('tipoMensajes', tipo);
    $('#divSiguientes').attr('pagina', '1');

    FijarMenuTrasScroll();
    FijarOpcionesNuevoMensajeTrasScroll();

    //=================================
    //  CARGAR CONTADORES MENSAJE MENU
    //=================================
    //Obtener_Contadores_Notificaciones();

    $('#divMuro').cn_ObtenerMensajesUsuario({
        WebMethodURL: rutanormal + 'script/cn/services/cnMuroUsuario.asmx/ObtenerMensajesUsuario',
        tipo: tipo,
        megusta: megusta,
        pagina: pagina,
        historico: historico
    });
}

function Obtener_Contadores_Notificaciones() {
    $.when($.ajax({
        type: "POST",
        url: rutanormal + 'script/cn/services/cnMuroUsuario.asmx/Comprobar_Notificaciones',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    })).done(function (msg) {
        $('#lnkNotificacionesCN').hide();     
        $('#AlertNotificacionCNMiContenido').hide();
        $('#AlertNotificacionCNTemas').hide();
        $('#AlertNotificacionCNEventos').hide();
        $('#AlertNotificacionCNNoticias').hide();
        $('#AlertNotificacionCNPreguntas').hide();
        $.each(msg.d, function () {
            switch (this.value) {
                case "0":
                    $('#NumeroAlertNotificacionCNMiContenido').text(parseInt(this.text));
                    $('#AlertNotificacionCNMiContenido').show();
                    break;
                case "1":
                    $('#NumeroAlertNotificacionCNTemas').text(parseInt(this.text));
                    $('#AlertNotificacionCNTemas').show();
                    break;
                case "2":
                    $('#NumeroAlertNotificacionCNEventos').text(parseInt(this.text));
                    $('#AlertNotificacionCNEventos').show();
                    break;
                case "3":
                    $('#NumeroAlertNotificacionCNNoticias').text(parseInt(this.text));
                    $('#AlertNotificacionCNNoticias').show();
                    break;
                case "4":
                    $('#NumeroAlertNotificacionCNPreguntas').text(parseInt(this.text));
                    $('#AlertNotificacionCNPreguntas').show();
                    break;
            }
        })
    });
}