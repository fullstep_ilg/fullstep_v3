﻿//CN_MENSAJES_UI.JS
$('[id^=msgContenido_] a').live('click', function () {
    if ($(this).attr('type') == 'entidad') {
        var tipoEntidad = $(this).attr('href').split("#")[0];
        var identificador = $(this).attr('href').split("#")[1];
        var MethodURL, param;
        switch (tipoEntidad) {
//            case 'PC': 
//                MethodURL = '/Comprobar_PermisoConsulta_ProceCompra'; 
//                param = { Anyo: $(this).attr('href').split("#")[1], GMN1: $(this).attr('href').split("#")[2], Proce: $(this).attr('href').split("#")[3] }; 
//                break; 
            case 'SC':
                MethodURL = '/Comprobar_PermisoConsulta_Identificador';
                param = { TipoEntidad: 1, Identificador: identificador };
                break;
            case 'CE':
                MethodURL = '/Comprobar_PermisoConsulta_Identificador';
                param = { TipoEntidad: 2, Identificador: identificador };
                break;
            case 'NC':
                MethodURL = '/Comprobar_PermisoConsulta_Identificador';
                param = { TipoEntidad: 3, Identificador: identificador };
                break;
            case 'CO':
                MethodURL = '/Comprobar_PermisoConsulta_Identificador';
                param = { TipoEntidad: 5, Identificador: identificador };
                break;
            case 'FP':
                MethodURL = '/Comprobar_PermisoConsulta_FichaCalidad';
                param = { ProveCod: identificador };
                break;
            case 'FA':
                MethodURL = '/Comprobar_PermisoConsulta_Identificador';
                param = { TipoEntidad: 7, Identificador: identificador.split('/')[0] };
                break;
        }
        var FechaLocal, url, targetUrl;
        $.when($.ajax({
            type: "POST",
            url: rutanormal + 'script/cn/services/cnMuroUsuario.asmx' + MethodURL,
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true
        })).done(function (msg) {
            if (msg.d) {
                switch (tipoEntidad) {
                    case 'PC':
                        break;                    
                    case 'CE':
                        var FechaLocal = new Date();
                        otz = FechaLocal.getTimezoneOffset();
                        window.open(msg.d + "&Otz=" + otz.toString(), "default_main");
                    case 'SC':
                    case 'CO':
                    case 'NC':
                    case 'FP':
                    case 'FA':
                        window.open(msg.d, "default_main");
                        break;
                }
            } else {
                alert(Textos[115]);
                return false;
            }
        });
        return false;
    }
});
$('.msgAdjuntoImage').live('click', function () {
    if (!$(this).hasClass('Link')) return false;
    var adjun = $(this).attr('data-url');
    if ($("#popupThumbnail").length == 0) {
        $.when($.get(rutanormal + 'script/cn/html/_PopUp_Thumbnail.htm', function (popUp) {
            $('body').append(popUp);
        })).done(function () {
            $.get(rutanormal + 'script/cn/html/_thumbnail.htm', function (templateThumbnail) {
                $('#popupThumbnail').append(templateThumbnail);
                $('#divAdjuntoThumbnail').append('<img id="msgAdjuntoThumbnail" src="' + adjun + '" style="max-height:100%; max-width:100%;" />');
                $('#popupFondo').css('height', $(document).height());
                $('#popupFondo').show();
                $('#popupThumbnail').show();
                var width = 100;
                while ($('#popupThumbnail').outerHeight() < $('#msgAdjuntoThumbnail').outerHeight()+30) {
                    $('#msgAdjuntoThumbnail').css('max-width', width - 1 + '%');
                    width = width - 1;
                }
                CentrarPopUp($('#popupThumbnail'));
            });
            $('#btnCerrarPopUpThumbnail').live('click', function () {
                $('#popupFondo').hide();
                $('#popupThumbnail').hide();
            });
        });
    } else {
        $('#divAdjuntoThumbnail').empty();
        $('#divAdjuntoThumbnail').append('<img id="msgAdjuntoThumbnail" src="' + adjun + '" style="max-height:100%;max-width:100%;"/>');        
        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        $('#popupThumbnail').show();
        var width = 100;
        while ($('#popupThumbnail').outerHeight() < $('#msgAdjuntoThumbnail').outerHeight()+30) {
            $('#msgAdjuntoThumbnail').css('max-width', width - 1 + '%');
            width = width - 1;
        }
        CentrarPopUp($('#popupThumbnail'));
    }
    return false;
});
$('[id^=msgAdjunVideo_] img').live('mouseenter', function () {
    if ($(this).closest('.template-download').find('.video-js-box').is(':visible')) {
        $(this).css('margin-left', '-160px');
    } else {
        $(this).css('margin-left', '-80px');
    }
});
$('[id^=msgAdjunVideo_] img').live('mouseleave', function () {
    if ($(this).closest('.template-download').find('.video-js-box').is(':visible')) {
        $(this).css('margin-left', '-160px');
    } else {
        $(this).css('margin-left', '0px');
    }
});
$('[id^=msgAdjunVideo_] img').live('click', function () {
    if ($(this).closest('.template-download').find('.video-js-box').is(':visible')) {
        $(this).css('margin-left', '-80px');
        $(this).closest('.template-download').find('.video-js-box').hide();
        if (jQuery.support.html5Clone) {
            $(this).closest('.template-download').find('video').get(0).pause();
        }
    } else {
        $(this).css('margin-left', '-160px');
        $(this).closest('.template-download').find('.video-js-box').show();
        if (jQuery.support.html5Clone) {
            $(this).closest('.template-download').find('video').get(0).play();
        }
    }
});
$('[id^=msgDescargar_]').live('click', function () {
    var filename = $(this).attr('data-file');
    var eparameter = ($(this).attr('EnProceso') == '' ? '' : '&e=1');
    window.location.href = rutanormal + 'script/cn/downloadattach.aspx?f=' + filename + eparameter;
});
$('[id^=VerRespuestas_]').live('click', function () {
    if ($(this).parents("#divMensajeUrgente").length > 0) $('#divMensajeUrgente').css('max-height', '');
    var idMensaje = $(this).attr('id').replace('VerRespuestas_', '');
    $.each($('[id^=msgRespuesta_' + idMensaje + '_]'), function () {
        $(this).show();
    });

    $('#divVerTodasRespuestas_' + idMensaje).hide();
    if ($(this).parents("#divMensajeUrgente").length > 0) {
        CentrarPopUp($('#popupMensajeUrgente'));
        $('#divMensajeUrgente').css('max-height', $('#popupMensajeUrgente').height() - 85);
    }
});
$('[id^=msgOcultarMensaje_]').live('click', function () {
    var idMensaje = $(this).attr('id').replace('msgOcultarMensaje_', '');
    $('#msg_' + idMensaje).hide();
    $('#msgOcultarMensaje_' + idMensaje).addClass('Ocultar');
    $('#msgMostrarMensaje_' + idMensaje).removeClass('Ocultar');
    $('#divOcultos').show();
    $.ajax({
        type: "POST",
        url: rutanormal + 'script/cn/services/cnMuroUsuario.asmx/OcultarMostrarMensaje',
        data: JSON.stringify({ idMensaje: idMensaje, ocultar: true }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    });
});
$('[id^=msgMostrarMensaje_]').live('click', function () {
    var idMensaje = $(this).attr('id').replace('msgMostrarMensaje_', '');
    $('#msgMostrarMensaje_' + idMensaje).addClass('Ocultar');
    $('#msgOcultarMensaje_' + idMensaje).removeClass('Ocultar');
    $.ajax({
        type: "POST",
        url: rutanormal + 'script/cn/services/cnMuroUsuario.asmx/OcultarMostrarMensaje',
        data: JSON.stringify({ idMensaje: idMensaje, ocultar: false }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    });
});
$('[id^=msgLink_]').live('click', function () {
    window.open($(this).attr('data-file'),'link');
});
$('.divMuro ul li').live('mouseenter mouseleave', function () {
    var idMensaje = $(this).attr('id').replace('msg_', '');
    if ($('#msgMostrarMensaje_' + idMensaje).hasClass('Ocultar')) $('#msgOcultarMensaje_' + idMensaje).toggleClass('Ocultar');
});