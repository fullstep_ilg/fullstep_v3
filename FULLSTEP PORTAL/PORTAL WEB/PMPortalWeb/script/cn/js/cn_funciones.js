﻿function OpcionesOtrosMensajes(MostrarSiguientes, MostrarOcultos) {
    if (MostrarSiguientes || MostrarOcultos) {
        $('#divOpcionesOtrosMensajes').show();
        if (MostrarSiguientes) {
            $('#divSiguientes').show();
            $('#lblSiguientes').text(Textos[68]);
        } else { $('#divSiguientes').hide(); }
        if (MostrarOcultos) {
            $('#divOcultos').show();
            $('#lblOcultos').text(Textos[69]);
        }
    } else {
        $('#divOpcionesOtrosMensajes').hide();
    }
}

function Obtener_ResultadosBusqueda() {
    var textoBuscado = $('#txtBuscador').val();
    if (textoBuscado == '') return false;
    var tipoOrdenacion = ($('#lblTipoOrdenacionCoincidencias').hasClass('Link')) ? 0 : 1;
    $('#divPrincipal').hide();
    $('#divCargando').show();
    var divResultados = $('#divMensajesResultadosBusqueda');
    divResultados.empty();
    var cn = { posts: [] };
    var timeoffset = new Date().getTimezoneOffset();
    $.when($.ajax({
        type: "POST",
        url: rutanormal + 'script/cn/services/cnMuroUsuario.asmx/ObtenerResultadosBusqueda',
        data: JSON.stringify({ tipoOrdenacion: tipoOrdenacion, textoBusqueda: textoBuscado, timezoneoffset: timeoffset }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    })).done(function (msg) {
        if (msg.d && msg.d.length > 0) {
            var mensajesBuscador = msg.d[0];
            var palabrasBuscador = msg.d[1]; 
            $.each(mensajesBuscador, function () {
                this.FechaAlta = eval("new " + this.FechaAlta.slice(1, -1));
                this.FechaActualizacion = eval("new " + this.FechaActualizacion.slice(1, -1));

                this.FechaAltaRelativa = relativeTime(this.FechaAlta);
                this.FechaActualizacionRelativa = relativeTime(this.FechaActualizacion);

                if (this.Respuestas) {
                    $.each(this.Respuestas, function () {
                        this.FechaAlta = eval("new " + this.FechaAlta.slice(1, -1));
                        this.FechaActualizacion = eval("new " + this.FechaActualizacion.slice(1, -1));

                        this.FechaAltaRelativa = relativeTime(this.FechaAlta);
                        this.FechaActualizacionRelativa = relativeTime(this.FechaActualizacion);
                    });
                }

                cn.posts.push(this);
            });
            // Crear lista para los post
            var ul = $('<ul>').appendTo(divResultados);

            $('#itemMensaje').tmpl(cn.posts).appendTo(ul);

            $('[id^=lblTituloResultadoBusqueda]').text(Textos[101]);
            $('[id^=lblInfoResultadosBusqueda]').text(Textos[102]);
            $('[id^=lblNumResultadosBusqueda]').text(Textos[103].replace('##', cn.posts.length));
            $('[id^=lblObjetivoResultadosBusqueda]').text(Textos[104]);
            $('[id^=lblTipoOrdenacion]').text(Textos[105]);
            $('[id^=lblTipoOrdenacionFecha]').text(Textos[106]);
            $('[id^=lblTipoOrdenacionCoincidencias]').text(Textos[107]);
            $('[id^=lblTextoResultadosBusqueda]').text(textoBuscado);

            $('#divCargando').hide();
            $('#divResultadosBusqueda').show();
            if (cn.posts.length > 0) {
                $.each(palabrasBuscador, function () {
                    divResultados.highlight(this);
                });
                divResultados.show();
            }
            /*====================================
            =========== TEMPLATES INFO ME GUSTA
            =====================================*/
            $('[id^=msgTmplMeGusta_]').text(Textos[85]);
            $.each($('[id^=msgTmplLeGusta_]'), function () {
                $(this).text(Textos[86].replace('###', $(this).text()));
            });
            $.each($('[id^=msgTmplLeGustaMeGusta_]'), function () {
                $(this).text(Textos[87].replace('###', $(this).text()));
            });
            $.each($('[id^=msgTmplMeGustaLesGusta_]'), function () {
                $(this).html(Textos[88].replace('@@@', $(this).html().replace('$$$', Textos[91])));
            });
            $.each($('[id^=msgTmplLeGustaMeGustaLesGusta_]'), function () {
                $(this).html(Textos[89].replace('@@@', $(this).html().replace($(this).text().replace($(this).children('span').text(), ''), '').replace('$$$', Textos[91])).replace('###', $(this).text().replace($(this).children('span').text(), '')));
            });
            $.each($('[id^=msgTmplLesGusta_]'), function () {
                $(this).html(Textos[90].replace('@@@', $(this).html().replace($(this).text().replace($(this).children('span').text(), ''), '').replace('$$$', Textos[91])).replace('###', $(this).text().replace($(this).children('span').text(), '')));
            });
            /*====================================
            =========== FIN TEMPLATES INFO ME GUSTA
            =====================================*/
            $('[id^=msgAdjuntoRespuesta_] span').text(Textos[28]);
            $('[id^=msgMeGusta_]').text(Textos[8]);
            $('[id^=msgResponder_]').text(Textos[77]);
            $('[id^=msgNuevaRespuestaDefault_]').val(Textos[84]);
            $('[id^=msgDescargar__]').text(Textos[79]);
            $('[id^=btnResponder_]').text(Textos[20]);
            $('[id^=btnResponder_]').attr('title', Textos[20]);
            $.each($('[id^=VerRespuestas_]'), function () {
                $(this).text(Textos[92].replace('###', $(this).text()) + '>>');
            });
        }
    });
}