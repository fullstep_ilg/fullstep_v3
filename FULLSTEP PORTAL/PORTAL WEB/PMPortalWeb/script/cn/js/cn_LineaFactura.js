﻿function LineaFactura(factura, linea)
{
    this.factura = factura;
    this.linea = linea;
    this.pedido = null;
    this.albaran = null;

    //Metodos de LineaFactura
    this.cargarDatos = cargarDatosFactura;

}


function cargarDatosFactura() {debugger;
    $.when($.ajax({
        type: "POST",
        url: rutanormal + 'script/cn/services/Factura.asmx/Obtener_Datos_LineaFactura',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ idFactura: (this.factura == 0 ? null : this.factura), idLinea: (this.linea == 0 ? null : this.linea) }),
        dataType: "json",
        async: false
    })).done(function (msg) {
        var data = msg.d;
        if (data.PedidoFs != null) {
            this.pedido = data.PedidoFs;
            this.albaran = data.Albaran;
        }
    });
}


