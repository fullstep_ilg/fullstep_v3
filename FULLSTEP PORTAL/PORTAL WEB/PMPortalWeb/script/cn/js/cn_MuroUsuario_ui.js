﻿$('#divOcultos').live('click', function () {
    $('[id^=msg_]').show();
    $('#divOcultos').hide();
});
$('#divNotificaciones').live('click', function () {
    CargarMensajes(0, false, 1, false);
    $('#divNotificaciones').hide();
});
$('#imgBuscador').live('click', function () {
    Obtener_ResultadosBusqueda(0);
});
$('#lblTipoOrdenacionFecha').live('click', function () {
    if (!$(this).hasClass('Link')) return false;
    $(this).removeClass('Link');
    $(this).removeClass('Subrayado');
    $('#lblTipoOrdenacionCoincidencias').addClass('Link');
    $('#lblTipoOrdenacionCoincidencias').addClass('Subrayado');
    Obtener_ResultadosBusqueda();
});
$('#lblTipoOrdenacionCoincidencias').live('click', function () {
    if (!$(this).hasClass('Link')) return false;
    $(this).removeClass('Link');
    $(this).removeClass('Subrayado');
    $('#lblTipoOrdenacionFecha').addClass('Link');
    $('#lblTipoOrdenacionFecha').addClass('Subrayado');
    Obtener_ResultadosBusqueda();
});
$('#txtBuscador').bind(($.browser.opera ? 'keypress' : 'keydown'), function (event) {
    var code;
    if (event.keyCode) code = event.keyCode;
    else if (event.which) code = event.which;

    switch (code) {
        case KEY.RETURN:
            Obtener_ResultadosBusqueda(0);
            break;
        default:
            break;
    }
});
$('#lnkAyudaBuscador').live('click', function () {
    if ($('#popupAyudaBuscador').length == 0) {
        $.get(rutanormal + 'script/cn/html/_PopUp_AyudaBuscador.htm', function (ayudaBuscador) {
            $('body').append(ayudaBuscador);

            $('#lblAyudaBuscador').text(Textos[108]);

            $.when($.ajax({
                type: "POST",
                url: rutanormal + 'script/cn/services/cnMuroUsuario.asmx/AyudaBuscador',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                error: function (xhr, status, error) {
                    alert(xhr.responseText);
                }
            })).done(function (msg) {
                $('#tituloAyudaBuscador').tmpl(msg.d).appendTo($('#tblAyudaBuscador'));
                $('#infoAyudaBuscador').tmpl(msg.d.children).appendTo($('#tblAyudaBuscador'));

                CentrarPopUp($('#popupAyudaBuscador'));
                $('#popupFondo').css('height', $(document).height());
                $('#popupFondo').show();
                $('#popupAyudaBuscador').show();
            });

            $('#btnCerrarPopUpAyudaBuscador').live('click', function () {
                $('#popupFondo').hide();
                $('#popupAyudaBuscador').hide();
            });
        });
    } else {
        CentrarPopUp($('#popupAyudaBuscador'));
        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();
        $('#popupAyudaBuscador').show();
    }
});