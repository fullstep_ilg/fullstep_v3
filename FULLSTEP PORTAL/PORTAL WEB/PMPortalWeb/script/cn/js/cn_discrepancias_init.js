﻿var paginaDiscrepancias = true;
$(document).ready(function () {
    $('#tablemenu').show();

    $.getScript(rutanormal + 'js/jquery/plugins/jquery.highlight.js');
    $.getScript(rutanormal + 'script/cn/js/cn_MuroUsuario_ui.js');
    $.getScript(rutanormal + 'script/cn/js/cn_mensajes_ui.js');
    $.getScript(rutanormal + 'js/jquery/plugins/jquery.iframe-transport.js');
    $.getScript(rutanormal + 'js/jquery/plugins/video.js', function () { VideoJS.setupAllWhenReady(); });
    $.getScript(rutanormal + 'script/cn/js/cn_ui_respuestas.js');
    $.getScript(rutanormal + 'script/cn/js/cn_ui_meGusta.js');
    $.getScript(rutanormal + 'ckeditor/ckeditor.js');

    $.getScript(rutanormal + 'script/cn/js/cn_funciones.js').done(function () {
        $.get(rutanormal + 'script/cn/html/_menu_cn.htm', function (menu) {
            menu = menu.replace(/src="/gi, 'src="' + rutanormal);
            $('#divMenu').append(menu);
            $('#divCargando').show();
            $('#divMenu #imgUsuImagen').attr('src', rutanormal + 'script/cn/Thumbnail.ashx?t=0');

            $.getScript(rutanormal + 'script/cn/js/cn_menu.js').done(function () {
                $.getScript(rutanormal + 'script/cn/js/cn_mensajes.js').done(function () {
                    $.get('html/_mensajes.tmpl.htm', function (templates) {
                        $('body').append(templates);
                        $.getScript(rutanormal + 'js/jsUtilities.js').done(function () {
                            CargarMensajes(0, false, 1);
                        });
                    });
                });

                $.getScript(rutanormal + 'js/jquery/plugins/jquery.fileupload.js').done(function () {
                    $.getScript(rutanormal + 'js/jquery/plugins/jquery.fileupload-ui.js').done(function () {
                        $.when($.get(rutanormal + 'script/cn/html/_adjuntos.tmpl.htm', function (templates) {
                            $('body').append(templates);
                        })).done(function () {
                            $('#fileupload').fileupload();
                            $('#fileupload').show();
                            $('#fileupload [type=file]').css('right', '');
                            $('#fileupload [type=file]').css('left', -10000);
                            $('#fileupload [type=file]').css('top', -10000);
                            $('#fileupload [type=file]').live('mouseleave', function () {
                                $('#fileupload [type=file]').css('left', -10000);
                                $('#fileupload [type=file]').css('top', -10000);
                                $('.Seleccionable').removeClass('Seleccionable');
                            });
                        });
                    });
                });
            });
        });
    });
});