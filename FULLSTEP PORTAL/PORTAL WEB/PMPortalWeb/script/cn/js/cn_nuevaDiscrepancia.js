﻿//CN_NUEVADISCREPANCIA.JS
//=========================
//  MENU NUEVO MENSAJE
//=========================
var timeoutNuevoMensaje;
var guardando = false;
var CKEDITOR_BASEPATH = rutanormal + 'ckeditor/';
$('[id^=btnAceptarNuevoMensaje]').live('click', function () {
    if (ComprobarDatosDiscrepanciaNueva()) {
        GuardarDiscrepancia();
    }
    $('#divNuevoMensajeParaMaster').attr('new', '');
});
$('[id^=btnCancelarNuevoMensaje]').live('click', function (event) {    
    var Master = ($(this).attr('id').indexOf('Master') >= 0 ? true : false);
    MostrarFormularioNuevaDiscrepancia(false, Master);
    $('#divNuevoMensajePara').attr('new', '');
    return false;
});
//=========================
//  FUNCIONES NUEVO MENSAJE
//=========================
function ComprobarDatosDiscrepanciaNueva() {
    if (CKEDITOR.instances['CKENuevoMensajeMaster'].getData() == '') {
        alert(Textos[93]);
        return false;
    }

    if ($('#txtSelCategoriaMaster').attr('itemValue') == undefined || $('#txtSelCategoriaMaster').attr('itemValue') == '') {
        alert(Textos[94]);
        return false;
    }

    if ($('#divListaParaMaster .ItemPara').length == 0) {
        alert(Textos[95]);
        return false;
    }    
    return true;
}
function GuardarDiscrepancia() {
    var params = ''; var tipo=1; var mensaje = ''; var categoria = '';
    var para = {}; var fechaEvento; var idNuevo=0;    
    var fechaAlta = new Date();
    var fechaAltaMensaje = UsuMask.replace('dd', fechaAlta.getDate()).replace('MM', fechaAlta.getMonth() + 1).replace('yyyy', fechaAlta.getFullYear());
    fechaAltaMensaje += ' ' + fechaAlta.getHours() + ':' + fechaAlta.getMinutes() + ':' + fechaAlta.getSeconds();
    var editor = CKEDITOR.instances['CKENuevoMensajeMaster'];
    var CKEditorImages = [];
    if (editor.mode == 'source') {
        guardando = true;
        editor.setMode('wysiwyg');
        return false;
    }
    $.each(editor.document.getElementsByTag('img').$, function () {
        if ($(this).attr("type") == "CKEditorImage") CKEditorImages.push({ href: $(this).attr('src'), path: '' + $(this).attr('id') + '', filename: '' + $(this).attr('filename') + '', guidID: '' });
    });
    //modifico las url de las imagenes de los link a entidades. Inicialmente se guardaban
    //con la ruta inicial, por lo que en portal podia aparecer la ruta de FSNWeb. Por tanto,
    //si existe la sustituyo por la de portal
    var contenido = $('<div>' + editor.getData() + '</div>');
    $.each($('a[type=entidad] img', contenido), function () {
        img = $(this);
        img = img.attr('src', img.attr('src').replace(rutanormal, ''));
        $('img', $(this)).replaceWith(img);
    });
    //MENSAJE   
    var mensaje =
    {
        id: '' + idNuevo + '',
        fechaAlta: '' + fechaAltaMensaje + '',
        titulo: $('#txtNuevoMensajeTituloMaster').hasClass('textoDefecto') ? '' : '' + $('#txtNuevoMensajeTituloMaster').val() + '',
        contenido: '' + contenido.html() + '',
        fecha: '',
        hora: '',
        timezoneOffset: fechaAlta.getTimezoneOffset(),
        donde: ''
    }
    //CATEGORIA
    categoria =
    {
        id: $('#txtSelCategoriaMaster').attr('itemValue'),
        denominacion: '' + $('#txtSelCategoriaMaster').val() + ''
    };

    //ADJUNTOS
    var listaAdjuntos = [];
    $.each($('#divListaAdjuntosMaster #tablaadjuntos').children(), function () {
        listaAdjuntos.push({
            nombre: $(this).attr('data-name'),
            size: $(this).attr('data-size'),
            sizeunit: $(this).attr('data-sizeunit'),
            tipoadjunto: $(this).attr('data-tipo'),
            url: $(this).attr('data-url')
        });
    });

    var paraMensaje = ObtenerParaDiscrepancia();
    params = { tipo: tipo, mensaje: mensaje, categoria: categoria, para: paraMensaje, adjuntos: listaAdjuntos, imagenesCKEditor: CKEditorImages, factura: idFactura, linea: linea };
    
    MostrarFormularioNuevaDiscrepancia(false, true);
    
    $('#imgDiscrepancias_' + linea).show();
    $('#imgDiscrepanciasCerradas_' + linea).hide();
    $.ajax({
        type: "POST",
        url: rutanormal + 'script/facturas/services/Facturas.asmx/InsertarDiscrepancia',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(params),
        dataType: "json",
        async: true    
    });
}
function ObtenerParaDiscrepancia() {
    //PARA
    var UON0, UON1, UON2, UON3, DEP, USU;
    var tipoProve, codigoProve, idContacto, idMatQA, gmn1, gmn2, gmn3, gmn4, prove;
    var anyo, cod, proceCompras, num;
    var tipoEstrucCompras, codigoEstrucCompras;
    var tipoGrupo, idGrupo, grupo;
    var para = { UON: [], PROVE: [], PROCECOMPRAS: [], ESTRUCCOMPRAS: [], GRUPO: [] };
    $.each($('#divListaParaMaster .ItemPara'), function () {
        switch ($(this).attr("id").split("-")[0]) {
            case 'itemParaUON':
                UON0 = true;
                UON1 = null;
                UON2 = null;
                UON3 = null;
                DEP = null;
                USU = null;
                for (i = 1; i < $(this).attr("id").split("-").length; i++) {
                    switch ($(this).attr("id").split("-")[i].split("_")[0]) {
                        case "UON1":
                            UON0 = false;
                            UON1 = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "UON2":
                            UON0 = false;
                            UON2 = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "UON3":
                            UON0 = false;
                            UON3 = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "DEP":
                            UON0 = false;
                            DEP = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                        case "USU":
                            UON0 = false;
                            USU = $(this).attr("id").split("-")[i].split("_")[1];
                            break;
                    }
                }
                if (UON0) {
                    codigo = { UON1: null, UON2: null, UON3: null, DEP: null, USU: null };
                } else {
                    codigo = { UON1: UON1, UON2: UON2, UON3: UON3, DEP: DEP, USU: USU };
                }
                para.UON.push(codigo)
                break;
            case "itemParaProve":
                tipoProve = null; codigoProve = null; idContacto = null; idMatQA = null;
                gmn1 = null; gmn2 = null; gmn3 = null; gmn4 = null;
                switch ($(this).attr("id").split("-")[1]) {
                    case "Todos":
                        tipoProve = 1;
                        break;
                    case "Calidad":
                        tipoProve = 2;
                        break;
                    case "Potenciales":
                        tipoProve = 3;
                        break;
                    case "Reales":
                        tipoProve = 4;
                        break;
                    case "MatQA":
                        tipoProve = 5;
                        idMatQA = $(this).attr("id").split("-")[2]
                        break;
                    case "MatGS":
                        tipoProve = 6;
                        num = $(this).attr("id").split("-").length;
                        switch (true) {
                            case (num > 5):
                                gmn4 = $(this).attr("id").split("-")[5];
                            case (num > 4):
                                gmn3 = $(this).attr("id").split("-")[4];
                            case (num > 3):
                                gmn2 = $(this).attr("id").split("-")[3];
                            case (num > 2):
                                gmn1 = $(this).attr("id").split("-")[2];
                                break;
                        }
                        break;
                    case "Uno":
                        tipoProve = 0;
                        idContacto = $(this).attr('id').split('-')[$(this).attr('id').split('-').length - 1];
                        codigoProve = $(this).attr('id').replace('itemParaProve-Uno-', '').replace('-' + idContacto, '');
                        break;
                }
                prove = { TIPOPROVE: tipoProve, CODIGOPROVE: codigoProve, IDCONTACTO: idContacto, MATQA: idMatQA, GMN1: gmn1, GMN2: gmn2, GMN3: gmn3, GMN4: gmn4 };
                para.PROVE.push(prove);
                break;
            case "itemParaProceCompras":
                anyo = $(this).attr("id").split("-")[2];
                gmn1 = $(this).attr("id").split("-")[3];
                cod = $(this).attr("id").split("-")[4];
                proceCompras = { ANYO: anyo, GMN1: gmn1, COD: cod, TIPOPARAPROCECOMPRA: $(this).attr("id").split("-")[1] };
                para.PROCECOMPRAS.push(proceCompras);
                break;
            case "itemParaEstrucCompras":
                tipoEstrucCompras = null; codigoEstrucCompras = null;
                gmn1 = null; gmn2 = null; gmn3 = null; gmn4 = null;
                //TIPO: 1-Equipo de compra, 2-Usuario
                switch ($(this).attr("id").split("-")[1]) {
                    case 'Eqp':
                        tipoEstrucCompras = 1;
                        codigoEstrucCompras = $(this).attr("id").split("-")[2].split("_")[1];
                        break;
                    case 'Usu':
                        tipoEstrucCompras = 2;
                        codigoEstrucCompras = $(this).attr("id").split("-")[3].split("_")[1];
                        break;
                    case 'MatGS':
                        tipoEstrucCompras = 3;
                        num = $(this).attr("id").split("-").length;
                        switch (true) {
                            case (num > 5):
                                gmn4 = $(this).attr("id").split("-")[5];
                            case (num > 4):
                                gmn3 = $(this).attr("id").split("-")[4];
                            case (num > 3):
                                gmn2 = $(this).attr("id").split("-")[3];
                            case (num > 2):
                                gmn1 = $(this).attr("id").split("-")[2];
                                break;
                        }
                        break;
                }
                estrucCompras = { TIPOESTRUCCOMPRAS: tipoEstrucCompras, CODIGO: codigoEstrucCompras, GMN1: gmn1, GMN2: gmn2, GMN3: gmn3, GMN4: gmn4 };
                para.ESTRUCCOMPRAS.push(estrucCompras);
                break;
            case "itemParaGrupo":
                tipoGrupo = null, idGrupo = null;
                switch ($(this).attr("id").split("-")[1]) {
                    case "EP":
                        tipoGrupo = 'EP';
                        break;
                    case "GS":
                        tipoGrupo = 'GS';
                        break;
                    case "PM":
                        tipoGrupo = 'PM';
                        break;
                    case "QA":
                        tipoGrupo = 'QA';
                        break;
                    case "SM":
                        tipoGrupo = 'SM';
                        break;
                    default:
                        idGrupo = $(this).attr("id").split("-")[2];
                        break;
                }
                grupo = { TIPOGRUPO: tipoGrupo, IDGRUPO: idGrupo };
                para.GRUPO.push(grupo);
                break;
        }
    });
    return para;
}
//==============================================
//============== DISCREPANCIAS =================
//==============================================
function MostrarFormularioNuevaDiscrepancia(show, MasterPage, idFactura, numFactura, linea, pedido, albaran) {
    var Master = (MasterPage ? 'Master' : '');
    $('#divSelCategoria' + Master).hide();
    if (show) {        
        $('#fileupload').attr('mensaje', '');

        $('#divNuevoMensaje' + Master).show();
        $('#divBotones' + Master).show();

        $('#divNuevoMensajeDefecto' + Master).hide();

        $('#tablaadjuntos').remove();
        $('#divListaAdjuntos' + Master).append('<div id="tablaadjuntos" class="files"></div>');

        $('#btnAceptarNuevoMensaje' + Master).attr('IdMensaje', 0);
                
        $('#divNuevoMensajeFechaHora' + Master).hide();
        if ($('#txtNuevoMensajeTitulo' + Master).hasClass('textoDefecto')) $('#txtNuevoMensajeTitulo' + Master).removeClass('textoDefecto');
        $('#divCabeceraDiscrepancia' + Master).css('display', '');
        
        var editor = CKEDITOR.instances['CKENuevoMensaje' + Master];
        if (editor) {
            editor.destroy(true);
        }
        var CodigoEntidadColaboracion = { identificador: idFactura + '/' + linea,
            texto: TextosCKEditor[28] + ' ' + numFactura + ' - ' + TextosCKEditor[29] + ' ' + linea + ' (' + TextosCKEditor[18] + ': ' + pedido + (albaran == '' ? '' : ' ' + TextosCKEditor[30] + ' ' + albaran) + ')'
        };
        editor = CKEDITOR.replace('CKENuevoMensaje' + Master, { customConfig: rutanormal + 'ckeditor/configCN.js',
            on: { instanceReady: function (ev) { MostrarCrearDiscrepancia(CodigoEntidadColaboracion); ImageUploadPlugin(ev.editor.name); },
                mode: function (ev) { if (guardando) { guardando = false; timeoutNuevoMensaje = setTimeout(function () { clearTimeout(timeoutNuevoMensaje); GuardarDiscrepancia() }, 50); } }
            }
        });
        scayt_READY = setInterval(function () { SCAYTready('CKENuevoMensaje' + Master) }, 500);

        $('#txtNuevoMensajeTitulo' + Master).val(TextosCKEditor[31] + ' ' + CodigoEntidadColaboracion.texto);
             
        if ($('#btnAceptarNuevoMensaje' + Master).attr('IdMensaje') == '0') {
            $('#divNuevoMensajeParaOpciones' + Master).empty();
            $('#divParaDestinatarios' + Master).empty();
            $.get(rutanormal + 'script/cn/html/_mensaje_para.htm', function (paraOpciones) {
                paraOpciones = paraOpciones.replace(/src="/gi, 'src="' + rutanormal);
                $('#divNuevoMensajeParaOpciones' + Master).append(paraOpciones);
                $.each($('#divNuevoMensajeParaOpciones' + Master + ' [id]'), function () {
                    $(this).attr('id', $(this).attr('id') + Master);
                });
                $('#divParaOpciones' + Master).hide();
                $('#divListaPara' + Master).attr('usu', '');
                if ($('#divNuevoMensajePara' + Master).attr('new') == '') {
                    CargarDatos_NuevaDiscrepancia(idFactura, linea, Master);
                }
            });
        }
    } else {
        var editor = CKEDITOR.instances['CKENuevoMensaje' + Master];
        if (editor) { editor.destroy(true) };

        $('#btnAceptarNuevoMensaje' + Master).removeAttr('IdMensaje');

        $('#divListaAdjuntos' + Master + ' #tablaadjuntos').empty();

        $('#txtSelCategoria' + Master).removeAttr("itemValue")
        $('#txtSelCategoria' + Master).val('');

        $('#divListaPara' + Master + ' div').remove();
      
        if (MasterPage) {
            $('#popupFondo').hide();
            $('#popupNuevoMensaje').hide();
        } else {
            $('#divNuevoMensaje').hide();
            $('#divBotones').hide();

            $('#divNuevoMensajeDefecto').show();
        }
        $('#txtNuevoMensajeTitulo' + Master).val('');        
    }
    $('[id^=NuevoMensajeAdjunto]').live('mouseenter', function () {
        $(this).addClass('Seleccionable');
        $('#fileupload [type=file]').css('width', $(this).outerWidth());
        $('#fileupload [type=file]').css('height', $(this).outerHeight());
        if ($(this).attr('id').indexOf('Master') == 0) {
            $('#fileupload [type=file]').css('left', $(this).position().left);
            $('#fileupload [type=file]').css('top', $(this).position().top);
        } else {
            $('#fileupload [type=file]').css('left', $(this).offset().left);
            $('#fileupload [type=file]').css('top', $(this).offset().top);
        }
        $('#fileupload [type=file]').css('z-index', 10000);
        $('#fileupload [type=file]').css('right', '');
    });  
    
    return false;
}
function MostrarCrearDiscrepancia(CodigoEntidadColaboracion) {    
    CentrarPopUpMain($('#popupNuevoMensaje'));
    $('#popupNuevoMensaje').show();
    $('#divNuevoMensaje').hide();
    $('#divBotones').hide();
    $('#divNuevoMensajeDefecto').show();
    
    CKEDITOR.plugins.registered.FSDiscrepancia.setSelectedValue(CodigoEntidadColaboracion);
}
function CargarDatos_NuevaDiscrepancia(factura, linea, Master) {
    $.when($.ajax({
        type: "POST",
        url: rutanormal + 'script/facturas/services/Facturas.asmx/Obtener_Datos_NuevaDiscrepancia_Proveedor',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ factura: factura, linea: linea }),
        dataType: "json",
        async: false
    })).done(function (msg) {
        var data = msg.d;

        if (data != '' && data != null) {
            $('#txtSelCategoriaMaster').attr('itemValue', data[0].value);
            $('#txtSelCategoriaMaster').val(data[0].text);

            $.each(data, function (item) {
                if(item!=0)
                    $('#divListaPara' + Master).prepend('<div class="ItemPara" id="' + this.value + '">' + this.text + '</div>');
            });            
        }
    });
}
/* TECLAS POSIBLES PULSADAS PARA SU POSIBLE TRATAMIENTO */
var KEY = {
    UP: 38,
    DOWN: 40,
    DEL: 46,
    TAB: 9,
    RETURN: 13,
    ESC: 27,
    COMMA: 188,
    PAGEUP: 33,
    PAGEDOWN: 34,
    BACKSPACE: 8
};

