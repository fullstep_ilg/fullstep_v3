﻿$(document).ready(function () {    
    $("#formFileupload").attr("action", rutanormal + 'script/cn/FileTransferHandler.ashx');
    $('#tablemenu').show();
    $('#imgNotificaciones').attr('src', rutanormal + 'images/Notificaciones.png');
    $('#imgConfiguraciones').attr('src', rutanormal + 'images/configuracion.png');
    $.getScript(rutanormal + 'script/cn/js/cn_UserProfile_ui.js');
    $('#divCargando').hide();
    EstablecerTextosProfile();
    $.get(rutanormal + 'script/cn/html/_menu_cn.htm', function (menu) {
        menu = menu.replace(/src="/gi, 'src="' + rutanormal);
        $('#divMenu').append(menu);
        $.getScript(rutanormal + 'script/cn/js/cn_menu.js').done(function () { CargarMenu(); });
        $('#imgFoto').attr('src', rutanormal + 'script/cn/Thumbnail.ashx?t=0');
        $('#divPrincipal').show();
    });
    $.getScript(rutanormal + 'js/jquery/plugins/jquery.iframe-transport.js');
    $.getScript(rutanormal + 'js/jquery/plugins/jquery.fileupload.js').done(function () {
        $.getScript(rutanormal + 'js/jquery/plugins/jquery.fileupload-ui.js').done(function () {
            $.when($.get(rutanormal + 'script/cn/html/_adjuntos.tmpl.htm', function (templates) {
                $('body').append(templates);
            })).done(function () {
                $('#fileupload').fileupload({ removePrevious: true, acceptFileTypes: /(\.|\/)(gif|jpe?g|png|tiff|bmp|ico)$/i });
                $('#fileupload').show();
                $('#fileupload [type=file]').css('right', '');
                $('#fileupload [type=file]').css('left', -10000);
                $('#fileupload [type=file]').css('top', -10000);
                $('#fileupload [type=file]').live('mouseleave', function () {
                    $('#fileupload [type=file]').css('left', -10000);
                    $('#fileupload [type=file]').css('top', -10000);
                    $('.Seleccionable').removeClass('Seleccionable');
                });
            });
        });
    });
});
function EstablecerTextosProfile() {
    $('#lblUserProfileAceptar').text(TextosUser[16]);
    $('#btnAceptarUserProfile').attr('title', TextosUser[16]);
    $('#lblUserProfileCancelar').text(TextosUser[17]);
    $('#btnCancelarUserProfile').attr('title', TextosUser[17]);
}