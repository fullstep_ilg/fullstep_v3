﻿Public Class cn_MuroUsuario
    Inherits FSPMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Colaboracion

        If Not IsPostBack Then
            With FSNPageHeader
                .TituloCabecera = Textos(0) 'Colaboración
            End With
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutas") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutas", "<script>" & _
                "var rutanormal = '" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "';" & _
                "var rutaFSNWeb = '" & System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "';" & _
                "</script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "LimiteMensajesCargados") Then
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "LimiteMensajesCargados", "<script>var LimiteMensajesCargados =" & System.Configuration.ConfigurationManager.AppSettings("LimiteMensajesCargados") & "</script>")
        End If

        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
            Dim sVariableJavascriptTextos As String = "var Textos = new Array();"
            Dim sVariableJavascriptTextosCKEditor As String = "var TextosCKEditor = new Array();"

            For i As Integer = 1 To 73
                sVariableJavascriptTextos &= "Textos[" & i & "]='" & JSText(Textos(i)) & "';"
                If i = 20 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[1]='" & JSText(Textos(i)) & "';"
                If i = 21 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[2]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 83 To 85
                sVariableJavascriptTextos &= "Textos[" & i - 9 & "]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 74 To 89
                sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 71 & "]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 90 To 121
                sVariableJavascriptTextos &= "Textos[" & i - 13 & "]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 142 To 143
                sVariableJavascriptTextos &= "Textos[" & i - 33 & "]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 144 To 147
                sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 125 & "]='" & JSText(Textos(i)) & "';"
            Next
            For i As Integer = 148 To 151
                sVariableJavascriptTextos &= "Textos[" & i - 37 & "]='" & JSText(Textos(i)) & "';"
            Next
            sVariableJavascriptTextos &= "Textos[115]='" & JSText(Textos(155)) & "';"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextos & sVariableJavascriptTextosCKEditor, True)
        End If

        Dim Usuario As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
        If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "DateFormatUsu") Then
            With Usuario
                Dim sVariablesJavascript As String = "var UsuMask = '" & .DateFormat.ShortDatePattern & "';"
                sVariablesJavascript = sVariablesJavascript & "var UsuMaskSeparator='" & .DateFormat.DateSeparator & "';"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "DateFormatUsu", sVariablesJavascript, True)
            End With
        End If
    End Sub

End Class