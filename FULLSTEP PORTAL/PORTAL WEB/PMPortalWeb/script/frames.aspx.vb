Namespace Fullstep.PMPortalWeb
    Partial Class frames
        Inherits FSPMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' <summary>
        ''' Cargar la pagina. 
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            Dim sIdi As String = Session("FS_Portal_User").Idioma.ToString
            If sIdi = Nothing Then
                sIdi = ConfigurationManager.AppSettings("idioma")
            End If
            Dim str As String = "<frame id=fraPMPortalHeader src='" & ConfigurationManager.AppSettings("rutanormal") & "App_Themes/" & Me.Page.Theme & "/top.htm' scrolling=no noresize>		<frame id=fraPMPortalMain src='" & ConfigurationManager.AppSettings("rutanormal") & "script/login/" & Me.Page.Theme & "/" & sIdi & "/inicio.aspx' ><frame name=fraPMPortalServer id=fraPMPortalServer src='blank.htm' >"
            Me.fraWS.InnerHtml = str

        End Sub

    End Class
End Namespace
