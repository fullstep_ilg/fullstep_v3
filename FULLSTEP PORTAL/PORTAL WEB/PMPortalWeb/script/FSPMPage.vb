﻿Imports System.Web.Caching
Imports Fullstep
Imports Fullstep.PMPortalServer

Public Class FSPMPage
    Inherits System.Web.UI.Page

    Private _FSPMUser As PMPortalServer.User
    Private _FSPMServer As PMPortalServer.Root
    Private _Idioma As String
    Private _ModuloIdioma As TiposDeDatos.ModulosIdiomas
    Private _Textos As DataSet

    ''' <summary>
    ''' Retorna el Usuario para la aplicación
    ''' </summary>
    ''' <returns>Usuario de la aplicación</returns>
    ''' <remarks>Llamada desde:cualquier lugar en el q interese conocer el usuario; Tiempo máximo:0</remarks>
    Public ReadOnly Property FSPMUser() As PMPortalServer.User
        Get
            If _FSPMUser Is Nothing Then
                _FSPMUser = Session("FS_Portal_User")
            End If

            Return _FSPMUser
        End Get
    End Property
    ''' <summary>
    ''' Retorna el Root (para acceder a bbdd) para la aplicación
    ''' </summary>
    ''' <returns>Root de la aplicación</returns>
    ''' <remarks>Llamada desde:cualquier lugar en el q interese acceder a bbdd; Tiempo máximo:0</remarks>
    Public ReadOnly Property FSPMServer() As PMPortalServer.Root
        Get
            If _FSPMServer Is Nothing Then
                _FSPMServer = Session("FS_Portal_Server")
            End If
            Return _FSPMServer
        End Get
    End Property

    Public ReadOnly Property Acceso() As FSNLibrary.TiposDeDatos.ParametrosGenerales
        Get
            Dim CiaComp As String = HttpUtility.UrlDecode(Session("FS_Portal_User").CiaComp)
            FSPMServer.GetAccessType(CiaComp)
            Return FSPMServer.TipoAcceso
        End Get
    End Property
    Public ReadOnly Property FSNAcceso() As FSNLibrary.TiposDeDatos.ParametrosGenerales
        Get
            Return Me.FSPMServer.TipoAcceso
        End Get
    End Property
    Public ReadOnly Property TextosModuloCompleto() As DataTable
        Get
            If _Textos Is Nothing Then
                _Textos = CType(HttpContext.Current.Cache("Textos_" & Me.FSPMUser.Idioma.ToString() & "_" & Me.ModuloIdioma), DataSet)
            End If
            If _Textos Is Nothing Then
                Dim FSPMDict As PMPortalServer.Dictionary = FSPMServer.Get_Dictionary()
                FSPMDict.LoadData(ModuloIdioma, Idioma)
                _Textos = FSPMDict.Data
                Me.InsertarEnCache("Textos_" & Me.FSPMUser.Idioma.ToString() & "_" & Me.ModuloIdioma, _Textos)
            End If
            Return _Textos.Tables(0)
        End Get
    End Property

    ''' <summary>
    ''' Idioma del usuario conectado
    ''' </summary>
    ''' <returns>Idioma del usuario conectado</returns>
    ''' <remarks>Llamada desde: cualquier lugar en el q interese conocer el idioma; Tiempo máximo:0</remarks>
    Public ReadOnly Property Idioma() As String
        Get
            If _Idioma Is Nothing Then
                If Not IsNothing(FSPMUser) Then
                    _Idioma = FSPMUser.Idioma.ToString()
                End If
                If _Idioma Is Nothing Then
                    _Idioma = ConfigurationManager.AppSettings("idioma")
                End If
            End If
            Return _Idioma
        End Get
    End Property

    ''' <summary>
    ''' Para hacer el multilingüismo en las paginas. Establece la pagina a traducir.
    ''' </summary>
    ''' <returns>pagina a traducir</returns>
    ''' <remarks>Llamada desde: Con el tiempo todos los page_load; Tiempo máximo:0</remarks>
    Public Property ModuloIdioma() As PMPortalServer.TiposDeDatos.ModulosIdiomas
        Get
            Return _ModuloIdioma
        End Get
        Set(ByVal Value As PMPortalServer.TiposDeDatos.ModulosIdiomas)
            If _ModuloIdioma <> Value Then _Textos = Nothing
            _ModuloIdioma = Value
        End Set
    End Property

    ''' <summary>
    ''' Para hacer el multilingüismo en las paginas.
    ''' </summary>
    ''' <returns>Textos multiidioma</returns>
    ''' <remarks>Llamada desde: Con el tiempo todos los page_load; Tiempo máximo:0</remarks>
    Public ReadOnly Property Textos(ByVal iTexto As Integer) As String
        Get
            If _Textos Is Nothing Then
                _Textos = CType(HttpContext.Current.Cache("Textos_" & Me.FSPMUser.Idioma.ToString() & "_" & Me.ModuloIdioma), DataSet)
            End If
            If _Textos Is Nothing Then
                Dim FSPMDict As PMPortalServer.Dictionary = FSPMServer.Get_Dictionary()
                FSPMDict.LoadData(ModuloIdioma, Idioma)
                _Textos = FSPMDict.Data
                Me.InsertarEnCache("Textos_" & Me.FSPMUser.Idioma.ToString() & "_" & Me.ModuloIdioma, _Textos)
            End If
            Return _Textos.Tables(0).Rows(iTexto).Item(1)
        End Get
    End Property

    ''' <summary>
    ''' Retorna el ScriptManager para la aplicación
    ''' </summary>
    ''' <returns>El ScriptManager establecido</returns>
    ''' <remarks>Llamada desde:--; Tiempo máximo:0</remarks>
    Public ReadOnly Property ScriptMgr() As System.Web.UI.ScriptManager
        Get
            Return ScriptManager.GetCurrent(Me.Page)
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = System.Configuration.ConfigurationManager.AppSettings.Item("TituloVentanas_" & Idioma)
    End Sub

    ''' Revisado por: Jbg. Fecha: 17/10/2011
    ''' <summary>
    ''' Precarga de la pagina
    ''' </summary>
    ''' <param name="sender">Origen del evento.</param>
    ''' <param name="e">Clase EventArgs que no contiene datos de evento.</param>
    ''' <remarks>Se produce al principio de la inicialización de la página.</remarks>
    Private Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        If Request.Cookies("USU_SESIONID") Is Nothing Then
            'La llamada se ha realizado desde el FSPM
            Response.Redirect(ConfigurationManager.AppSettings("rutanormal") & "/../../../", False)

        Else
            Dim FSPMServer As New Fullstep.PMPortalServer.Root
            FSPMServer.Load_AccesoServidorExterno()
            If FSPMServer.AccesoServidorExterno Then
                'Si está activo el acceso por servidor externo FSPMServer ya se ha establecido en Consultas.asmx.LoginDesdeAsp
                FSPMServer = Session("FS_Portal_Server")
                FSPMServer.AccesoServidorExterno = True
            Else
                FSPMServer = New Fullstep.PMPortalServer.Root
            End If

            Dim Username As String
            Dim oUser As Fullstep.PMPortalServer.User

            Dim IPDir As String = Request.ServerVariables("LOCAL_ADDR").ToString
            Dim PersistID As String = Server.UrlDecode(HttpContext.Current.Request.Cookies("SESPP").Value)

            If Not Session("FS_Portal_User_ID") Is Nothing Then
                If Not (Session("FS_Portal_User_ID") = Server.UrlDecode(Request.Cookies("USU_SESIONID").Value)) Then
                    'Hay q logear pq alguien se esta logeando en el "mismo" IE, una pestaña con x,x,x , cierras esta y en otra pestaÃ±a y,x,x ó z,z,z

                    oUser = FSPMServer.Login(Server.UrlDecode(Request.Cookies("USU_SESIONID").Value), IPDir, PersistID)
                    If oUser Is Nothing Then
                        If FSPMServer.AccesoServidorExterno Then
                            Response.End()
                        Else
                            Dim sRuta As String = ConfigurationManager.AppSettings("rutanormal")
                            sRuta = Replace(sRuta, "PMPortal", "login")
                            Response.Redirect(sRuta & ConfigurationManager.AppSettings("custom") & "/login.asp", False)
                        End If

                        Exit Sub
                    Else
                        Username = oUser.Cod
                        oUser.LoadUserData(Username, oUser.CiaComp)
                        FSPMServer.Load_LongitudesDeCodigos(oUser.CiaComp)
                        Session("FS_Portal_User") = oUser
                        Session("FS_Portal_Server") = FSPMServer
                        HttpContext.Current.User = Session("FS_Portal_User")

                        Session("FS_Portal_User_ID") = Server.UrlDecode(Request.Cookies("USU_SESIONID").Value)

                        Exit Sub
                    End If
                Else
                    'Todo OK. 
                    'Comprobar cambio de idioma/ opciones de usuario
                    If Not Request.Cookies("USU_Idioma") Is Nothing OrElse Not Request.Cookies("USU_TIPOMAIL") Is Nothing _
                    OrElse Not Request.Cookies("USU_DECIMALFMT") Is Nothing OrElse Not Request.Cookies("USU_THOUSANFMT") Is Nothing _
                    OrElse Not Request.Cookies("USU_PRECISIONFMT") Is Nothing OrElse Not Request.Cookies("USU_DATEFMT") Is Nothing Then
                        Dim bHayCambio As Boolean = False

                        oUser = Session("FS_Portal_User")
                        If oUser.Idioma <> Request.Cookies("USU_Idioma").Value Then
                            oUser.Idioma = Request.Cookies("USU_Idioma").Value
                            bHayCambio = True
                        End If
                        If oUser.TipoEmail <> Request.Cookies("USU_TIPOMAIL").Value Then
                            oUser.TipoEmail = Request.Cookies("USU_TIPOMAIL").Value
                            bHayCambio = True
                        End If
                        If oUser.DecimalFmt <> Server.UrlDecode(Request.Cookies("USU_DECIMALFMT").Value) Then
                            oUser.DecimalFmt = Server.UrlDecode(Request.Cookies("USU_DECIMALFMT").Value)
                            oUser.NumberFormat.NumberDecimalSeparator = Server.UrlDecode(Request.Cookies("USU_DECIMALFMT").Value)
                            bHayCambio = True
                        End If
                        If oUser.ThousanFmt <> Server.UrlDecode(Request.Cookies("USU_THOUSANFMT").Value) Then
                            oUser.ThousanFmt = Server.UrlDecode(Request.Cookies("USU_THOUSANFMT").Value)
                            oUser.NumberFormat.NumberGroupSeparator = Server.UrlDecode(Request.Cookies("USU_THOUSANFMT").Value)
                            bHayCambio = True
                        End If
                        If oUser.PrecisionFmt <> Request.Cookies("USU_PRECISIONFMT").Value Then
                            oUser.PrecisionFmt = Request.Cookies("USU_PRECISIONFMT").Value
                            oUser.NumberFormat.NumberDecimalDigits = Request.Cookies("USU_PRECISIONFMT").Value
                            bHayCambio = True
                        End If
                        If oUser.DateFmt <> Server.UrlDecode(Request.Cookies("USU_DATEFMT").Value) Then
                            oUser.DateFmt = Server.UrlDecode(Request.Cookies("USU_DATEFMT").Value)
                            If Not Server.UrlDecode(Request.Cookies("USU_DATEFMT").Value) = "" Then
                                oUser.DateFormat.ShortDatePattern = Replace(Server.UrlDecode(Request.Cookies("USU_DATEFMT").Value), "mm", "MM")
                            Else
                                oUser.DateFormat.ShortDatePattern = "dd/MM/yyyy"
                            End If
                            bHayCambio = True
                        End If

                        If bHayCambio Then
                                Session("FS_Portal_User") = oUser
                                HttpContext.Current.User = Session("FS_Portal_User")

                                Exit Sub
                            End If
                        End If
                    End If
            Else
                oUser = FSPMServer.Login(Server.UrlDecode(Request.Cookies("USU_SESIONID").Value), IPDir, PersistID)
                If oUser Is Nothing Then
                    Dim sRuta As String = ConfigurationManager.AppSettings("rutanormal")
                    sRuta = Replace(sRuta, "PMPortal", "login")
                    Response.Redirect(sRuta & ConfigurationManager.AppSettings("custom") & "/login.asp", False)

                    Exit Sub
                Else
                    Username = oUser.Cod
                    oUser.LoadUserData(Username, oUser.CiaComp)
                    FSPMServer.Load_LongitudesDeCodigos(oUser.CiaComp)
                    Session("FS_Portal_User") = oUser
                    Session("FS_Portal_Server") = FSPMServer
                    HttpContext.Current.User = Session("FS_Portal_User")

                    Session("FS_Portal_User_ID") = Server.UrlDecode(Request.Cookies("USU_SESIONID").Value)
                End If
            End If

            'Al compartir Entry entre Producto y Portal, se hace necesario para este dato al Entry
            Session("FS_Portal_CiaComp") = oUser.CiaComp
        End If
    End Sub

    Private _IdCiaComp As Nullable(Of Long)
    ''' <summary>
    ''' Devolver el Id de la compañía
    ''' </summary>
    ''' <returns>Id de la compania</returns>
    ''' <remarks>Llamada desde: script\_common\guardarinstancia.aspx.vb script\variablescalidad\PuntuacionesProveedor.aspx; Tiempo máximo:0</remarks>
    Public ReadOnly Property IdCiaComp() As Long
        Get
            If _IdCiaComp Is Nothing Then
                _IdCiaComp = Session("FS_Portal_User").CiaComp
            End If

            Return _IdCiaComp
        End Get
    End Property

    Private _lIdCia As Nullable(Of Long)
    ''' <summary>
    ''' Devolver el Id del proveedor
    ''' </summary>
    ''' <returns>Id de la compania</returns>
    ''' <remarks>Llamada desde: property oSolicitud property oInstancia property oCertificado; Tiempo máximo:0</remarks>
    Public ReadOnly Property IdCia() As Long
        Get
            If _lIdCia Is Nothing Then
                _lIdCia = Session("FS_Portal_User").IdCia
            End If

            Return _lIdCia
        End Get
    End Property

    ''' <summary>
    ''' actualizar culture 
    ''' </summary>
    ''' <remarks>Llamada desde: Sistema es un Overrides; timepo máximo:0sg</remarks>
    Protected Overrides Sub InitializeCulture()
        MyBase.InitializeCulture()

        If Not IsNothing(Me.FSPMUser) Then
            'si el usuario ha cambiado de idioma recargamos datos para actualizar culture 
            If (Not IsNothing(Request.Cookies("CultureChange")) AndAlso Request.Cookies("CultureChange").Value = 1) Then
                If (IsNothing(Request.Cookies("CultureChangeNet"))) Then
                    Me.FSPMUser.LoadUserData(Me.FSPMUser.Cod, FSPMUser.CiaComp)
                    Response.Cookies("CultureChangeNet").Value = 0
                End If
            End If
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo(Me.FSPMUser.RefCultural)
            Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat = Me.FSPMUser.DateFormat
            Threading.Thread.CurrentThread.CurrentCulture.NumberFormat = Me.FSPMUser.NumberFormat

            Threading.Thread.CurrentThread.CurrentUICulture = Threading.Thread.CurrentThread.CurrentCulture
        Else
            'Sin esto, VisorOfertas.aspx y EntregasPedidos.aspx no coge la cultura nada mas entrar. 
            '¿FSPMUser nothing? No veo diferencia con certificados.aspx q lo coge nada mas entrar
            Threading.Thread.CurrentThread.CurrentUICulture = Threading.Thread.CurrentThread.CurrentCulture
        End If
    End Sub

    ''' <summary>Inserta un objeto en la caché con un tiempo de expiración que se obtiene del web.config</summary>
    ''' <param name="key">clave</param>
    ''' <param name="value">objeto a insertar</param>
    ''' <param name="priority">prioridad</param>
    Public Sub InsertarEnCache(ByVal key As String, ByVal value As Object, Optional ByVal priority As CacheItemPriority = CacheItemPriority.Default)
        HttpContext.Current.Cache.Insert(key, value, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, ConfigurationManager.AppSettings("TiempoExpiracionCacheMin"), 0), priority, Nothing)
    End Sub
    Public ReadOnly Property EmpresaPortal() As Boolean
        Get
            Return Request.Cookies("EMPRESAPORTAL").Value
        End Get
    End Property
    Public ReadOnly Property NomPortal() As String
        Get
            Return Request.Cookies("NOM_PORTAL").Value.ToString
        End Get
    End Property
End Class
