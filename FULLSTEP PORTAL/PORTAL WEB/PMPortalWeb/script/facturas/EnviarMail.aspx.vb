﻿Imports System.IO

Namespace Fullstep.PMPortalWeb

    Public Class EnviarMail
        Inherits FSPMPage

        Public Titulo As String
        Private Const IncludeScriptKeyFormat As String = ControlChars.CrLf & _
    "<script language=""{0}"">{1}</script>"

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lbltitulo As System.Web.UI.WebControls.Label
        Protected WithEvents ugtxtTexto As System.Web.UI.WebControls.TextBox

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' <summary>
        ''' Cargar la pagina en envio de mail a proveedores.
        ''' Para="" implica error. Modo de actuar: si en Cco solo una dirección ent (Para=Cco)(limpia Cco) sino (Para=Email usuario conectado)
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.MailProveedores

            Me.lbltitulo.Text = If(Request("NumeroFactura") <> "", Textos(16), Textos(0))
            Titulo = Me.lbltitulo.Text
            Me.btnEnviar.Text = Textos(1)
            Me.lblPara.Text = Textos(2)
            Me.lblCC.Text = Textos(3)
            Me.lblCCO.Text = Textos(11)
            Me.lblAsunto.Text = Textos(4)
            Me.cmdAdjuntar.Value = Textos(5)
            lblInfo.Text = Textos(12)

            btnEnviar.Attributes.Add("onclick", "return ComprobarDatosMail();")
            '
            Dim sClientTexts As String
            sClientTexts = ""
            sClientTexts += "var arrTextosML = new Array();"
            sClientTexts += "arrTextosML[0] = '" + JSText(Textos(6)) + " ';"   'Seleccione alguno
            sClientTexts += "arrTextosML[1] = '" + JSText(Textos(7)) + " ';"   'Problemas con el servidor
            sClientTexts += "arrTextosML[2] = '" + JSText(Textos(8)) + " ';"   'Problemas con los adjuntos
            sClientTexts += "arrTextosML[3] = '" + JSText(Textos(9)) + " ';"   'notificador. Consulte con FULLSTEP
            sClientTexts += "arrTextosML[4] = '" + JSText(Textos(10)) + " ';"   'Problemas desconocidos
            sClientTexts += "arrTextosML[5] = '" + JSText(Textos(13)) + " ';"   'El campo Para no puede estar vacio
            sClientTexts += "arrTextosML[6] = '" + JSText(Textos(14)) + " ';"   'La dirección ##### introducida no es correcta
            sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
            ClientScript.RegisterStartupScript(Me.GetType(), "TextosMICliente", sClientTexts)

            If Not Page.IsPostBack Then
                If Request("NumeroFactura") <> "" Then Me.ugtxtAsunto.Text = Replace(Textos(15), "#####", Request("NumeroFactura"))

                If Me.ParaExtendido.Value = "" Then
                    If InStr(Me.CCOExtendido.Value, ",", CompareMethod.Text) > 0 Then
                        If Len(Me.CCOExtendido.Value) = InStr(Me.CCOExtendido.Value, ",", CompareMethod.Text) Then
                            Me.ParaExtendido.Value = Me.CCOExtendido.Value
                            Me.CCOExtendido.Value = ""
                        Else
                            Me.ParaExtendido.Value = FSPMUser.Email
                        End If
                    Else
                        Me.ParaExtendido.Value = Me.CCOExtendido.Value
                        Me.CCOExtendido.Value = ""
                    End If
                End If

                Me.AdjuntoExtendido.Value = ""

                Me.txtRuta.Value = GenerateRandomPath()

                Dim sTemp As String = ConfigurationManager.AppSettings("temp") & "\" & Me.txtRuta.Value

                If Not Directory.Exists(sTemp) Then
                    Directory.CreateDirectory(sTemp)
                End If
            End If

        End Sub

        Private Function PreparaAdjuntos() As String
            Dim Adjuntos As String = Me.AdjuntoExtendido.Value

            Dim sTemp As String = ConfigurationManager.AppSettings("temp") & "\" & Me.txtRuta.Value & "\"

            'Dim oFichero As System.IO.FileInfo
            Dim sPath As String
            Dim sRes As String = ""

            While InStr(Adjuntos, ";", CompareMethod.Binary)
                sPath = Left(Adjuntos, InStr(Adjuntos, ";", CompareMethod.Binary) - 1)
                'No comprobamos si el fichero exista pq no funciona el los servidores el Exists por un 
                'problema de permisos.  Los ficheros se acaban de guardar así que estarán.(EPB 09/11/2006)
                'Fichero = New System.IO.FileInfo(sPath)
                'If oFichero.Exists = True Then
                sRes = sRes & sPath & ";"
                'End If
                'oFichero = Nothing

                Adjuntos = Right(Adjuntos, Adjuntos.Length - InStr(Adjuntos, ";", CompareMethod.Binary))
            End While

            Return sRes
        End Function

        '''Revisado por: Jbg; Fecha: 24/10/2011
        ''' <summary>
        ''' Envia un correo
        ''' </summary>
        ''' <param name="sender">Pagina</param>
        ''' <param name="e">Evento de sistema</param>        
        ''' <remarks>Llamada desde:sistema; Tiempo máximo:0,1</remarks>
        Private Sub btnEnviar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnviar.Click
            Dim sClientTexts As String
            Dim IncludeScriptKeyFormat As String = ControlChars.CrLf & "<script language=""{0}"">{1}</script>"

            If Me.AdjuntoExtendido.InnerText <> "" Then
                If Right(Me.AdjuntoExtendido.InnerText, 1) = ";" Then
                Else
                    Me.AdjuntoExtendido.InnerText = Me.AdjuntoExtendido.InnerText + ";"
                End If
            End If

            Dim lIdInstancia As Long = If(Request("Instancia") Is Nothing, 0, Request("Instancia"))
            Dim oEmail As PMPortalServer.Email
            oEmail = FSPMServer.Get_Object(GetType(PMPortalServer.Email))
            oEmail.EnvioMail(IdCiaComp, FSPMUser.Email, ugtxtAsunto.Text, ParaExtendido.InnerText, ugtxtTexto.Text, False, True, FSPMUser.CodProve, lIdInstancia, CCExtendido.InnerText, CCOExtendido.InnerText, Me.AdjuntoExtendido.InnerText, Me.txtRuta.Value)

            sClientTexts = ""
            sClientTexts += "window.close()"
            sClientTexts = String.Format(IncludeScriptKeyFormat, "javascript", sClientTexts)
            Page.ClientScript.RegisterStartupScript(Me.GetType(),"TextoOk", sClientTexts)
        End Sub

    End Class
End Namespace