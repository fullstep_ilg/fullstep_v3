﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="VisorFacturas.aspx.vb" Inherits="Fullstep.PMPortalWeb.VisorFacturas" %>
<%@ Register TagPrefix="pag" Src="~/script/_common/Paginador.ascx" TagName="Paginador"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head runat="server">
	<title></title>
	<link href="../../../common/estilo.asp" type="text/css" rel="stylesheet"/>
	<script src="../../../common/formatos.js" type="text/javascript"></script>
	<script src="../../js/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
	<script src="../../../common/menu.asp" type="text/javascript"></script>
	<script type="text/javascript">dibujaMenu(9)</script>
	<script type="text/javascript">
		$(document).ready(function () {
			/*''' <summary>
			''' Iniciar la pagina.
			''' </summary>     
			''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
		    $("#formFileupload").attr("action", rutanormal + 'script/cn/FileTransferHandler.ashx');
		    if (accesoExterno == 0) {document.getElementById('tablemenu').style.display = 'block';}

			$.getScript(rutanormal + 'script/facturas/js/visorFacturas_init.js');
		});
	</script>
	<style type="text/css">
		.seguimiento
		{
			color: Red;
		}
		#divContenedor div
		{
			display:inline;
			float:left;
			width:100%;
			height:100%
		}
	</style>
</head>
<body>
	<form id="form1" runat="server">
		<script language="javascript" type="text/javascript">
			/*
			Descripcion:= Funcion que carga los valores del buscador de empresas en la caja de texto
			Parametros:=
			idEmpresa --> Id Empresa
			nombreEmpresa --> Nombre empresa
			Llamada desde:= BuscadorEmpresas.aspx --> aceptar()
			*/
			function SeleccionarEmpresa(idEmpresa, nombreEmpresa, nifEmpresa) {
				oIdEmpresa = $("[id$=hidEmpresa]")[0]
				if (oIdEmpresa)
					oIdEmpresa.value = idEmpresa;

				otxtEmpresa = $("[id$=txtEmpresa]")[0]
				if (otxtEmpresa)
					otxtEmpresa.value = nombreEmpresa;
			}
			//Recoge el ID del proveedor seleccionado con el autocompletar
			function selected_Empresa(sender, e) {
				oIdEmpresa = $("[id$=hidEmpresa]")[0]
				if (oIdEmpresa)
					oIdEmpresa.value = e._value;
			}
			//funcion que abre el detalle de comentarios de una instancia
			//idInstancia: id de la instancia
			function AbrirDetalle(idInstancia) {
				window.open("../solicitudes/comentariosSolicitud.aspx?Instancia=" + idInstancia, "_blank", "width=1000,height=560,status=yes,resizable=no,top=200,left=200")
			}
			function AbrirBuscadorArticulos() {
				/*<%-- 
				Actualmente no es necesario pasar ninguno de los siguientes valores a la página articulosserver.
				mat = Material preseleccionado
				matro = 1: Material como sólo lectura, 0: No
				Index = No se usa en articulosserver
				TabContainer = No se usa en articulosserver
				ClientID = Id del control que contiene el artículo en esta página
				restric = string con los valores de GMN1, GMN2, GMN3 y GMN4 preseleccionados colocados uno tras otro
				MatClientId = Id del control que contiene el material en esta página
				InstanciaMoneda = Moneda por la que filtrar cuando se recupera el último precio adjudicado para el artículo
				CargarUltAdj = Indicador de si carga o no el último precio adjudicado para el artículo
				CodOrgCompras, OrgComprasRO, CodCentro, CentroRO -> Parámetros relacionados con las Organizaciones de compras y Centros de coste que no son necesarios en este caso.
				Origen -> Valor que nos permite saber si la página se solicitó desde entregasPedidos.
				--%>*/
				window.open("../_common/articulosserver.aspx?mat=&matro=0&Index=0&TabContainer=&ClientID=&restric=&MatClientId=&InstanciaMoneda=&CargarUltAdj=false&CodOrgCompras=&OrgComprasRO=&CodCentro=&CentroRO=&Origen=VisorFacturas", "_blank", "width=500,height=400,status=yes,resizable=no,top=200,left=200")
			}
			/*
			Se lanza al pulsar una celda de la grid. Dependiendo de la celda pulsada, realizará una acción u otra.
			*/
			function onCellSelectionChanged(sender, e) {
				var row = e.getNewSelectedCells().getCell(0).get_row();
				e.set_cancel(true);
				//--------------------------
				//SEGUIMIENTO DE FACTURAS   
				//--------------------------
				switch (e.getNewSelectedCells().getCell(0).get_column().get_key()) {
					case "SEG": //SEGUIMIENTO DE FACTURAS 
						var nuevo_valor_seg;
						if (e.getNewSelectedCells().getCell(0).get_value() == "0") {
							nuevo_valor_seg = 1;
							row.get_element().style.color = "red";
							e.getNewSelectedCells().getCell(0).set_value(nuevo_valor_seg, '');
							e.getNewSelectedCells().getCell(0)._element.innerHTML = '<IMG src="' + rutaPortal + 'App_themes/' + sTema + '/images/seg_selec.gif">'
						} else {
							nuevo_valor_seg = 0;
							row.get_element().style.color = "black";
							e.getNewSelectedCells().getCell(0).set_value(nuevo_valor_seg, '');
							e.getNewSelectedCells().getCell(0)._element.innerHTML = '<IMG src="' + rutaPortal + 'App_themes/' + sTema + '/images/seg_noselec.gif">'
						}
			            Fullstep.PMPortalWeb.Consultas.Monitorizar(row.get_cellByColumnKey("INSTANCIA").get_value(), nuevo_valor_seg, row.get_index());
						break;
					case "PAGOS": //DETALLE DE LOS PAGOS   
						var olblTitulos = $("[id$=lblTituloPagos]")[0];
						olblTitulos.innerText = row.get_cellByColumnKey("NUM_FACTURA").get_value();
						Fullstep.PMPortalWeb.Consultas.Obtener_DatosDetallepagos(row.get_cellByColumnKey("ID_FACTURA").get_value(), OnGetPagosComplete);
						break;
					case "DEN_ESTADO": //MOTIVO DE ANULACION
						if (row.get_cellByColumnKey("ANULADO").get_value() == "1")
						    FSNMostrarPanel(sFSNPanelMotivoAnulacion_AnimationClientID, event, sFSNPanelMotivoAnulacion_DynamicPopulateClientID, row.get_cellByColumnKey("ID_FACTURA").get_value());
						else {
						    factura = row.get_cellByColumnKey("ID_FACTURA").get_value();
						    Fullstep.PMPortalWeb.Consultas.ComprobarEnProceso(row.get_cellByColumnKey("INSTANCIA").get_value(), OnGetEnProcesoComplete);
                        }
						break;
					case "TIENE_DISCREP":
						if (row.get_cellByColumnKey("TIENE_DISCREP").get_value() == 1) {
						    idFactura = row.get_cellByColumnKey("ID_FACTURA").get_value();
						    numFactura = row.get_cellByColumnKey("NUM_FACTURA").get_value();
						    MostrarFormularioDiscrepanciasFactura(idFactura, numFactura);
						}
						break;
                    case "SITUACION_ACTUAL":
                        break
					default:
						factura = row.get_cellByColumnKey("ID_FACTURA").get_value();
						Fullstep.PMPortalWeb.Consultas.ComprobarEnProceso(row.get_cellByColumnKey("INSTANCIA").get_value(), OnGetEnProcesoComplete);
						break;
				}
			}

			/* 
			Una vez obtenido el detalle de los pagos de la factura, muestra el modalpopup.
			Llamada desde: onCellSelectionChanged
			*/
			function OnGetPagosComplete(result) {
				$get('divPagosContent').innerHTML = result;
				popUpShowed = $find('mpePagos')
				popUpShowed.show();
				popUpShowed._backgroundElement.style.zIndex += 10;
				popUpShowed._foregroundElement.style.zIndex += 10;
			}

			/* 
			Una vez comprobado si está o no en proceso te redirige a la página de detalle o te muestra el aviso de que está en proceso.
			Llamada desde: onCellSelectionChanged
			*/
			function OnGetEnProcesoComplete(result) {
				if (result == 0)
					window.open("DetalleFactura.aspx?ID=" + factura, "default_main")
				else
					FSNMostrarPanel(sFSNPanelEnProceso_AnimationClientID, event, sFSNPanelEnProceso_DynamicPopulateClientID, 7, null, null, x, y);
			}

			/*
			Obtiene las posiciones del cursor, necesario para el FireFox
			*/
			var x = 0
			var y = 0
			function Posicion(event) {
				x = event.clientX;
				y = event.clientY;

			}

			/*
			''' <summary>
			''' Exporta a Excel
			''' </summary>
			''' <remarks>Llamada desde: onclick del div q contiene al link excel y a la img excel; Tiempo máximo:0,5</remarks>
			*/
			function ExportarExcel() {
				__doPostBack('ExportarExcel', '')
			}

			/*
			''' <summary>
			''' Exporta a Pdf
			''' </summary>
			''' <remarks>Llamada desde: onclick del div q contiene al link pdf y a la img pdf; Tiempo máximo:0,5</remarks>
			*/
			function ExportarPDF() {
				__doPostBack('ExportarPDF', '')
			}

			/*<summary>
		Método necesario para ordenar las columnas del grid
		</summary>
		<remarks>Llamada desde: evento ColumnSorting del grid. Máx. 0,1 seg.</remarks>
		--%>*/
		function wdgFacturas_Sorting_ColumnSorting(grid, args) {
			var sorting = grid.get_behaviors().get_sorting();
			var col = args.get_column();
			var sortedCols = sorting.get_sortedColumns();
			for (var x = 0; x < sortedCols.length; ++x) {
				if (col.get_key() == sortedCols[x].get_key()) {
					args.set_clear(false);
					break;
				}
			}
		}

		//funcion que salta cuando se cambia el centro de coste, al meter un codigo validara si es valido o no
		//oTxtCentro: caja de texto del centro de coste
		function CentroCosteChange(oTxtCentro) {

			var sCentroCoste = oTxtCentro.value

			$.ajax({
				type: "POST",
				url: rutaPortal + 'Consultas.asmx/Buscar_CentroCoste',
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify({ CentroCoste: sCentroCoste }),
				dataType: "json",
				async: false,
				success: function (json_data) {
					var ohidCentroCoste = $("[id$=hid_CentroCostes]")[0]
					if (json_data.d.ExisteCentroCoste) {
						ohidCentroCoste.value = json_data.d.CodCentroCoste
						oTxtCentro.value = json_data.d.DenCentroCoste
						oTxtCentro.style.backgroundColor = "green"
					} else {
						oTxtCentro.style.backgroundColor = "red"
						oTxtCentro.value = ''
						ohidCentroCoste.value=''
					}
				}
			})
		}
	</script>
	<asp:ScriptManager ID="scm1" runat="server" EnablePageMethods="true" >
		<Scripts>
			<asp:ScriptReference Path="~/script/_common/js/jquery.tmpl.js" />
		</Scripts>
		<Services>
			<asp:ServiceReference Path="~/Consultas.asmx" />
		</Services>
	</asp:ScriptManager>
	<div style="width:100%; float:left">
		<fsn:FSNPageHeader ID="FSNPageHeader" runat="server">
		</fsn:FSNPageHeader>
	</div>
	<div style="float:left; display:inline; padding-right:15px; padding-top:10px"><asp:Label ID="lblEmpresa" runat="server"></asp:Label></div>
	<div style="float:left; display:inline; padding-right:15px; padding-top:10px"><table style="background-color:White;width:150px;border:solid 1px #BBBBBB" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<asp:TextBox ID="txtEmpresa" runat="server" Width="195px" BorderWidth="0px"></asp:TextBox>
					</td>
					<td style="border-left:solid 1px #BBBBBB;padding-left:2px;padding-right:0px">
						<asp:ImageButton id="imgEmpresaLupa" runat="server" SkinID="BuscadorLupa" /> 
					</td>
				</tr>                        
				<asp:HiddenField ID="hidEmpresa" runat="server" />
				
			</table>
	  </div>
	  <div style="float:left; display:inline; padding-right:15px; padding-top:10px"><asp:Label ID="lblNumFactura" runat="server"></asp:Label></div>
	  <div style="float:left; display:inline; padding-right:15px; padding-top:10px"><asp:TextBox ID="txtNumeroFactura" runat="server" Width="120px" BackColor="White"></asp:TextBox></div> 
	  <div style="float:left; display:inline; padding-right:15px; padding-top:10px"><fsn:FSNButton ID="btnBuscarIdentificador" runat="server" Alineacion="Left"></fsn:FSNButton></div> 
	  <div style="float:left; display:inline; padding-right:15px;">
	  <!-- Columna en la que está la alerta de facturas pendientes -->
			<asp:UpdatePanel ID="upAlerta" runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<asp:PlaceHolder ID="phAlerta" runat="server" Visible="false">
						<div>
							<table cellpadding="2" cellspacing="0" border="0">
								<tr>
									<td rowspan="3">
										<asp:Image ID="imgAlerta" runat="server" Height="35" Width="35" />
									</td>
									<td align="left">
										<asp:Label ID="lblFacturasPendientes" runat="server" CssClass="Etiqueta"></asp:Label>
									</td>
								</tr>
							</table>
						</div>
					</asp:PlaceHolder>
				</ContentTemplate>
			</asp:UpdatePanel>
	 </div> 
	 <div style="clear:both">
	  <asp:Panel ID="pnlCabeceraParametros" runat="server" Width="99%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-top: 5px;
			padding-bottom: 5px">
			<tr>
				<td valign="middle" align="left" style="padding-top: 5px">
					<asp:Panel runat="server" ID="pnlBusquedaAvanzada" Style="cursor: pointer" Font-Bold="True"
						Height="20px" Width="100%" CssClass="PanelCabecera">
						<table border="0">
							<tr>
								<td>
									<asp:Image ID="imgExpandir" runat="server" SkinID="Contraer" />
								</td>
								<td style="vertical-align: top">
									<asp:Label ID="lblBusquedaAvanzada" runat="server" 
										Font-Bold="True" ForeColor="White"></asp:Label>
								</td>
							</tr>
						</table>
					</asp:Panel>
				</td>
			</tr>
		</table>
	</asp:Panel>
	</div>
	<asp:Panel ID="pnlParametros" runat="server" CssClass="Rectangulo" Width="100%">
		 <asp:UpdatePanel ID="upBusquedaAvanzada" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
			<ContentTemplate>
				<fspm:BusquedaAvanzadaFac id="Busqueda" runat="server"></fspm:BusquedaAvanzadaFac>
				<table width="80%" border="0" cellpadding="4" cellspacing="0">
					<tr>
						<td width="60px">
							<fsn:FSNButton ID="btnBuscarBusquedaAvanzada" runat="server" Alineacion="left"></fsn:FSNButton>
						</td>
						<td width="60px">
							<fsn:FSNButton ID="btnLimpiarBusquedaAvanzada" runat="server" Alineacion="Right"></fsn:FSNButton>
						</td>
						<td>
						</td>
					</tr>
				</table>
			</ContentTemplate>
		</asp:UpdatePanel> 
	</asp:Panel>
	
	<cc1:CollapsiblePanelExtender runat="server" CollapseControlID="pnlBusquedaAvanzada"
		ExpandControlID="pnlBusquedaAvanzada" SuppressPostBack="true" TargetControlID="pnlParametros"
		ImageControlID="imgExpandir" SkinID="FondoRojo" Collapsed="true">
	</cc1:CollapsiblePanelExtender>
	<div onmouseover="Posicion(event)">
		<asp:UpdatePanel ID="upFacturas" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnBuscarIdentificador" EventName="Click" />
		        <asp:AsyncPostBackTrigger ControlID="btnBuscarBusquedaAvanzada" EventName="Click" />
	        </Triggers>
			<ContentTemplate>
				<ig:WebHierarchicalDataGrid ID="whdgFacturas" Visible="true" runat="server" AutoGenerateColumns="false" ShowHeader="true" EnableAjax="false" EnableAjaxViewState="false" EnableDataViewState="false" Width="100%">
				<Behaviors>
				<ig:Selection CellClickAction="Cell" CellSelectType="Single" SelectionClientEvents-CellSelectionChanging="onCellSelectionChanged"></ig:Selection>
							<ig:Filtering Alignment="Top" Enabled="true" Visibility="Visible" EnableInheritance="true">
				   
							</ig:Filtering>
				
							<ig:ColumnResizing Enabled="true"></ig:ColumnResizing>
							<ig:ColumnMoving Enabled="true"></ig:ColumnMoving>
							<ig:Sorting Enabled="true" SortingMode="Multi">
								<SortingClientEvents ColumnSorting="wdgFacturas_Sorting_ColumnSorting" />        
							</ig:Sorting>
							<ig:Paging Enabled="true" PagerAppearance="Top">
								<PagerTemplate>
										<div id="divContenedor" style="display:inline; float:left; width:30%; height:100%">
											<pag:Paginador id="wdgPaginador" runat="server" />
										</div>
										<div id="divFechaDesde" runat="server" style="display:inline; float:left; width:50%; height:100%">
											<asp:Label cssClass="common" ID="lblFechaDesde" style="position: relative; top: -20%; text-align:center;" runat="server"></asp:Label>
											<asp:Image ID="imgCalendarioFecDesde" runat="server" />
											<asp:Label cssClass="common" ID="lblExceptoFechaDesde" style="position: relative; top: -20%; text-align:center;" runat="server"></asp:Label>
										</div>
										<div id="divBotonesExportar" style="display:inline;float:right; height:100%">
											<div style="height:22px; float:right; line-height:22px;">
												<div onclick="ExportarExcel();" class="botonDerechaCabecera">
													<div ID="divExcel" runat="server" style="background-repeat:no-repeat;height:20px;">
														<span ID="litExcel" runat="server" style="margin-left:20px;"></span>
													</div>
												</div>
												<div onclick="ExportarPDF();" class="botonDerechaCabecera">            
													<div ID="divPdf" runat="server" style="background-repeat:no-repeat;height:20px;">
														<span ID="litPDF" runat="server" style="margin-left:20px;"></span>
													</div>
												</div>
											</div>
										</div>
								</PagerTemplate>
							</ig:Paging>
				</Behaviors>
				</ig:WebHierarchicalDataGrid>
			</ContentTemplate> 
		</asp:UpdatePanel>
	</div>

	<asp:Button ID="btnPagos" runat="server" Style="display: none" />
	<ajx:ModalPopupExtender ID="mpePagos" runat="server" Enabled="True" PopupControlID="panPagos" TargetControlID="btnPagos" CancelControlID="imgCerrarPagos" />
	<asp:Panel ID="panPagos" runat="server" Style="display: none" Width="600px" CssClass="modalPopup">
		<div id="Cabecera"><table width="100%">
		<tr><td align="left"><asp:Image ID="imgPagos" runat="server" /></td><td><asp:Label ID="lblLitTituloPagos" runat="server" CssClass="RotuloGrande"></asp:Label>&nbsp;<asp:Label ID="lblTituloPagos" runat="server" CssClass="RotuloGrande"></asp:Label></td>
			<td align="right""><asp:ImageButton id="imgCerrarPagos" runat="server" /></td></tr></table>        
		</div>
		<p><br /></p>
		<div id="divPagosContent"></div>
		<p><br /></p>
	</asp:Panel>
	 
	 <fsn:FSNPanelInfo ID="FSNPanelMotivoAnulacion" runat="server" ServicePath="~/Consultas.asmx" ServiceMethod="Obtener_MotivoAnulacion" TipoDetalle="0"></fsn:FSNPanelInfo>
	 <fsn:FSNPanelInfo ID="FSNPanelEnProceso" runat="server" ServicePath="~/Consultas.asmx" ServiceMethod="Obtener_TextoEnProceso" TipoDetalle="4" Height="120px" Width="370px"></fsn:FSNPanelInfo>
	<ig:WebDocumentExporter ID ="whdgPDFExporter" runat="server"></ig:WebDocumentExporter>
	<ig:WebExcelExporter ID ="whdgExcelExporter" runat="server"></ig:WebExcelExporter>
	</form>
	<div id="fileupload" style="display:none;">
		<form id="formFileupload" method="post" enctype="multipart/form-data">
			<div class="fileupload-buttonbar">
				<label class="fileinput-button">                    
					<input type="file" name="files[]" multiple="multiple" />
				</label>
				<div class="fileupload-progressbar"></div>
			</div>
		</form>
	</div>
</body>
</html>
