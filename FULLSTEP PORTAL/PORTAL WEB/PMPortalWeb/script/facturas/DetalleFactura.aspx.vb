﻿Imports Fullstep
Imports System.IO
Imports System.Threading

Namespace Fullstep.PMPortalWeb
    Public Class DetalleFactura
        Inherits FSPMPage


#Region "PROPIEDADES"
        Private oFsServer As PMPortalServer.Root
        Private _oFactura As PMPortalServer.Factura
        Private lCiaComp As Long
        Private m_desdePM As Boolean = False 'Si se llama desde el campo Factura(Gs=138) de una solicitud de PM
        ''' <summary>
        ''' Propiedad que carga los datos de la factura
        ''' </summary>
        Protected ReadOnly Property oFactura() As PMPortalServer.Factura
            Get
                If _oFactura Is Nothing Then
                    If Me.IsPostBack Then
                        _oFactura = CType(Cache("oFactura" & FSPMUser.IdCia & FSPMUser.Cod), PMPortalServer.Factura)
                    Else
                        _oFactura = FSPMServer.Get_Object(GetType(PMPortalServer.Factura))
                        _oFactura.ID = Request("ID")
                        _oFactura.Load(lCiaComp, Idioma)
                        Me.InsertarEnCache("oFactura" & FSPMUser.IdCia & FSPMUser.Cod, _oFactura)
                        Me.InsertarEnCache("dsFactura" & FSPMUser.IdCia & FSPMUser.Cod, _oFactura.DatosFactura)
                    End If
                End If
                Return _oFactura
            End Get
        End Property
        Private _oInstancia As PMPortalServer.Instancia
        ''' <summary>
        ''' Propiedad que carga los datos de la instancia
        ''' </summary>
        Protected ReadOnly Property oInstancia() As PMPortalServer.Instancia
            Get
                If _oInstancia Is Nothing Then
                    If Me.IsPostBack Then
                        _oInstancia = CType(Cache("oInstancia" & FSPMUser.IdCia & FSPMUser.Cod), PMPortalServer.Instancia)
                    Else
                        _oInstancia = FSPMServer.Get_Object(GetType(PMPortalServer.Instancia))
                        _oInstancia.ID = _oFactura.Instancia
                        _oInstancia.Load(lCiaComp, Idioma)
                        'mirar si hace falta solicitud.load para quitarlo
                        If Not _oInstancia.Solicitud Is Nothing Then
                            _oInstancia.Solicitud.Load(lCiaComp, Idioma)
                        End If

                        _oInstancia.CargarCumplimentacionFactura(lCiaComp, , FSPMUser.CodProve)

                        If Not _oInstancia Is Nothing Then Me.InsertarEnCache("oInstancia" & FSPMUser.IdCia & FSPMUser.Cod, _oInstancia)
                    End If
                End If
                Return _oInstancia
            End Get
        End Property
#End Region
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            oFsServer = Me.FSPMServer
            lCiaComp = IdCiaComp

            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DetalleFactura

            If Request("DesdePM") = 1 Then
                m_desdePM = True
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosDiscrepancia") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosDiscrepancia", "<script>var TextosDiscrepancia = new Array(); TextosDiscrepancia[1]= '" & Textos(33) & "'</script>")
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosValidacion") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosValidacion", "<script>var TextosValidacion = '" & Textos(34) & "'</script>")
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextoValidacionDiscrepancia") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextoValidacionDiscrepancia", "<script>var TextoValidacionDiscrepancia = '" & Textos(35) & "'</script>")
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextoTolerancia") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextoTolerancia", "<script>var TextoTolerancia = '" & Textos(36) & "';</script>")
            End If

            If IsPostBack Then
                Select Case Request("__EVENTTARGET")
                    Case "EjecutarAccion"
                        ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DetalleFactura
                        ConfigurarCabeceraMenu()
                        GenerarScripts()

                        'Validación para saber si el proveedor tiene facturas rectificativas pendientes de emitir.
                        If (oInstancia.ControlMapperFactura(lCiaComp, oFactura.IdEmpresa, oFactura.CodProve, oFactura.ID, CLng(Request("__EVENTARGUMENT"))) Is Nothing) Then
                            RealizarAccion(CLng(Request("__EVENTARGUMENT")))
                        End If
                End Select
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutanormal") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutanormal", "<script>var rutanormal = '" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "';" & _
                    "var rutaFSNWeb='" & System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "';</script>")
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "LimiteMensajesCargados") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "LimiteMensajesCargados", "<script>var LimiteMensajesCargados =" & System.Configuration.ConfigurationManager.AppSettings("LimiteMensajesCargados") & "</script>")
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumFactura") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumFactura", "<script>var NumFactura ='" & oFactura.NumeroFactura & "';</script>")
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "CosteGenerico") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "CosteGenerico", "<script>var CosteGenerico ='" & oFactura.IdCosteGenerico & "';</script>")
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "DescuentoGenerico") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "DescuentoGenerico", "<script>var DescuentoGenerico ='" & oFactura.IdDescuentoGenerico & "';</script>")
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "ToleranciaImporte") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ToleranciaImporte", "<script>var ToleranciaImporte = " & oFactura.ToleranciaImporte & ";</script>")
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "ToleranciaPorcentaje") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ToleranciaPorcentaje", "<script>var ToleranciaPorcentaje =" & oFactura.ToleranciaPorcentaje & ";</script>")
            End If

            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Colaboracion
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
                Dim sVariableJavascriptTextos As String = "var Textos = new Array();"
                Dim sVariableJavascriptTextosCKEditor As String = "var TextosCKEditor = new Array();"

                For i As Integer = 1 To 73
                    sVariableJavascriptTextos &= "Textos[" & i & "]='" & JSText(Textos(i)) & "';"
                    If i = 20 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[1]='" & JSText(Textos(i)) & "';"
                    If i = 21 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[2]='" & JSText(Textos(i)) & "';"
                Next

                For i As Integer = 83 To 85
                    sVariableJavascriptTextos &= "Textos[" & i - 9 & "]='" & JSText(Textos(i)) & "';"
                Next

                For i As Integer = 74 To 89
                    sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 71 & "]='" & Textos(i) & "';"
                Next

                For i As Integer = 90 To 120
                    sVariableJavascriptTextos &= "Textos[" & i - 13 & "]='" & JSText(Textos(i)) & "';"
                Next
                For i As Integer = 142 To 154
                    sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 123 & "]='" & JSText(Textos(i)) & "';"
                Next
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextos, True)
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosCKEditor", sVariableJavascriptTextosCKEditor, True)

                Dim Usuario As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
                If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "DateFormatUsu") Then
                    With Usuario
                        Dim sVariablesJavascript As String = "var UsuMask = '" & .DateFormat.ShortDatePattern & "';"
                        sVariablesJavascript = sVariablesJavascript & "var UsuMaskSeparator='" & .DateFormat.DateSeparator & "';"
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "DateFormatUsu", sVariablesJavascript, True)
                    End With
                End If
            End If

            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DetalleFactura
            If Not IsPostBack Then
                ConfigurarCabeceraMenu()
                GenerarScripts()

                DatosGenerales.Proveedor = oFactura.CodProve & " - " & oFactura.DenProve
                DatosGenerales.CodProveedor = oFactura.CodProve
                DatosGenerales.Empresa = oFactura.Empresa
                DatosGenerales.IdEmpresa = oFactura.IdEmpresa
                DatosGenerales.Importe = oFactura.Importe
                DatosGenerales.ImporteBruto = oFactura.ImporteBruto
                DatosGenerales.SumaCostesLineas = CalculoSumaCostesLinea()
                DatosGenerales.SumaDescuentosLineas = CalculoSumaDescuentosLinea()
                DatosGenerales.NumeroFactura = oFactura.NumeroFactura
                DatosGenerales.FechaFactura = oFactura.FechaFactura
                DatosGenerales.RetencionGarantia = oFactura.TotalRetencionGarantia
                DatosGenerales.NumeroERP = oFactura.NumeroERP
                DatosGenerales.FechaContabilizacion = oFactura.FechaContabilizacion
                DatosGenerales.Costes = oFactura.TotalCostes
                DatosGenerales.CodFormaPago = oFactura.CodFormaPago
                DatosGenerales.FormaPago = oFactura.FormaPago
                DatosGenerales.SituacionActual = If(oFactura.Estado = 0, Textos(14), If(oInstancia.DenEtapaActual = Nothing, oInstancia.CargarEstapaActual(IdCiaComp, Idioma), oInstancia.DenEtapaActual))
                DatosGenerales.Descuentos = oFactura.TotalDescuentos
                DatosGenerales.CodViaPago = oFactura.CodViaPago
                DatosGenerales.ViaPago = oFactura.ViaPago
                DatosGenerales.Observaciones = oFactura.Observaciones
                DatosGenerales.DataSourceCostes = oFactura.Costes
                DatosGenerales.DataSourceDescuentos = oFactura.Descuentos
                DatosGenerales.DataSourceRetencionGarantia = oFactura.RetencionGarantia
                DatosGenerales.Impuestos = oFactura.ImpuestosCabecera
                DatosGenerales.Instancia = oFactura.Instancia
                If m_desdePM Then
                    DatosGenerales.Editable = False
                Else
                    DatosGenerales.Editable = oInstancia.DetalleEditable
                End If
                DatosGenerales.LiteralNumeroERP = Acceso.nomPedERP(Idioma)
                DatosGenerales.PathServicio = ConfigurationManager.AppSettings("rutanormal") & "Consultas.asmx"

                If DatosGenerales.Editable Then
                    DatosGenerales.DataSourceFormasPago = DevolverFormasPago()
                    DatosGenerales.DataSourceViasPago = DevolverViasPago()

                    Dim oFacturas As PMPortalServer.Facturas
                    oFacturas = oFsServer.Get_Object(GetType(PMPortalServer.Facturas))
                    DatosGenerales.CostesFacturasProveedor = oFacturas.LoadCostesFacturas(lCiaComp, oFactura.CodProve)
                    DatosGenerales.DescuentosFacturasProveedor = oFacturas.LoadDescuentosFacturas(lCiaComp, oFactura.CodProve)
                    Lineas_Factura.CostesUtilizadosBuscador = oFacturas.LoadCostesLineaFacturas(lCiaComp, oFactura.CodProve)
                    Lineas_Factura.DescuentosUtilizadosBuscador = oFacturas.LoadDescuentosLineaFacturas(lCiaComp, oFactura.CodProve)

                    Lineas_Factura.Paises = CargarPaises()
                    Lineas_Factura.CodPaisProve = oFactura.CodPaisProve
                    If oFactura.CodPaisProve <> String.Empty Then
                        CargarProvincias(oFactura.CodPaisProve)
                    End If

                    DatosGenerales.RutaBuscadorArticulo = ConfigurationManager.AppSettings("rutanormal") & "script/_common/articulosserver.aspx"
                    DatosGenerales.RutaBuscadorMaterial = ConfigurationManager.AppSettings("rutanormal") & "script/_common/materiales.aspx"

                    Lineas_Factura.RutaBuscadorArticulo = ConfigurationManager.AppSettings("rutanormal") & "script/_common/articulosserver.aspx"
                    Lineas_Factura.RutaBuscadorMaterial = ConfigurationManager.AppSettings("rutanormal") & "script/_common/materiales.aspx"
                End If

                Lineas_Factura.PathServicio = ConfigurationManager.AppSettings("rutanormal") & "Consultas.asmx"
                Lineas_Factura.ImpuestosRepercutidos = oFactura.ImpuestosRepercutidos
                Lineas_Factura.ImpuestosRetenidos = oFactura.ImpuestosRetenidos
                Lineas_Factura.ImpuestosFactura = oFactura.ImpuestosFactura
                Lineas_Factura.PartidasDataTable = DevolverPartidas()
            Else
                Dim dtDescuentosGenerales As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("DESCUENTOS"), DataTable)
                Dim dtCostesGenerales As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("COSTES"), DataTable)
                If m_desdePM Then
                    DatosGenerales.Editable = False
                Else
                    DatosGenerales.Editable = oInstancia.DetalleEditable
                End If
                DatosGenerales.DataSourceCostes = dtCostesGenerales
                DatosGenerales.DataSourceDescuentos = dtDescuentosGenerales
                DatosGenerales.SumaCostesLineas = CalculoSumaCostesLinea()
                DatosGenerales.SumaDescuentosLineas = CalculoSumaDescuentosLinea()
                If CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable).Rows.Count > 0 Then
                    DatosGenerales.Costes = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable).Rows(0).Item("TOT_COSTES")
                    DatosGenerales.Descuentos = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable).Rows(0).Item("TOT_DESCUENTOS")
                    DatosGenerales.Importe = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable).Rows(0).Item("IMPORTE")
                    DatosGenerales.ImporteBruto = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable).Rows(0).Item("IMPORTE_BRUTO")
                End If
            End If

            ModuloIdioma = TiposDeDatos.ModulosIdiomas.DatosGeneralesFactura
            DatosGenerales.Textos = TextosModuloCompleto
            DatosGenerales.RutaImagenes = ConfigurationManager.AppSettings("rutanormal") & "/UserControls/images/"
            DatosGenerales.Moneda = oFactura.Moneda
            DatosGenerales.IdCosteGenerico = oFactura.IdCosteGenerico
            DatosGenerales.IdDescuentoGenerico = oFactura.IdDescuentoGenerico
            DatosGenerales.NumberFormat = FSPMUser.NumberFormat
            DatosGenerales.DateFormat = FSPMUser.DateFormat
            DatosGenerales.SufijoCache = FSPMUser.IdCia & FSPMUser.Cod

            Lineas_Factura.AccesoSM = Me.Acceso.gbAccesoFSSM
            Lineas_Factura.Textos = TextosModuloCompleto
            Lineas_Factura.RutaImagenes = ConfigurationManager.AppSettings("rutanormal") & "/UserControls/images/"
            If m_desdePM Then
                Lineas_Factura.Editable = False
            Else
                Lineas_Factura.Editable = oInstancia.DetalleEditable
            End If
            'Lineas_Factura.CostesPorLinea = Acceso.gbCostesPorLinea
            Lineas_Factura.Moneda = oFactura.Moneda
            Lineas_Factura.DataSource = oFactura.Lineas
            Lineas_Factura.IdFactura = oFactura.ID
            Lineas_Factura.NumFactura = oFactura.NumeroFactura
            Lineas_Factura.IdCosteGenerico = oFactura.IdCosteGenerico
            Lineas_Factura.IdDescuentoGenerico = oFactura.IdDescuentoGenerico
            Lineas_Factura.SufijoCache = FSPMUser.IdCia & FSPMUser.Cod
            Lineas_Factura.NumberFormat = FSPMUser.NumberFormat
            Lineas_Factura.MostrarLeyendaOtrosGestores = False 'En Portal siempre será falso.

            HistoricoNotificaciones.Textos = TextosModuloCompleto
            HistoricoNotificaciones.RutaImagenes = ConfigurationManager.AppSettings("rutanormal") & "/UserControls/images/"
            If Not IsPostBack Then HistoricoNotificaciones.DataSource = oFactura.Load_Historico_Notificaciones(lCiaComp, FSPMUser.CodProve)

            If Not IsPostBack Then
                GenerarEstructuraDataset()
                Dim cFacturas As New Fullstep.PMPortalWeb.Facturas
                cFacturas.ActualizarCostesDescuentosGenerales(oFactura.ID, oFactura.Importe, oFactura.TotalCostes, oFactura.TotalDescuentos, oFactura.ImporteBruto)
            End If
        End Sub

        ''' <summary>
        ''' Funcion que calcula la suma total de los costes de las lÃ­neas de la factura
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function CalculoSumaCostesLinea() As Double
            Dim Importe As Double = 0

            For Each drCoste As DataRow In oFactura.DatosFactura.Tables(11).Rows
                Importe = Importe + DBNullToDbl(drCoste("IMPORTE"))
            Next

            Return Importe
        End Function

        ''' <summary>
        ''' Funcion que calcula la suma total de los descuentos de las lÃ­neas de la factura
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function CalculoSumaDescuentosLinea() As Double
            Dim Importe As Double = 0

            For Each drDescuento As DataRow In oFactura.DatosFactura.Tables(13).Rows
                Importe = Importe + DBNullToDbl(drDescuento("IMPORTE"))
            Next

            Return Importe
        End Function

        ''' <summary>
        ''' Devuelve las formas de pago para cargarlas en el combo
        ''' </summary>
        ''' <returns>Un dataset con las formas de pago</returns>
        ''' <remarks>Llamada desde: Page_Load()</remarks>
        Function DevolverFormasPago() As DataSet
            Dim oPags As PMPortalServer.FormasPago
            oPags = FSPMServer.Get_FormasPago
            oPags.LoadData(Idioma, lCiaComp)
            DevolverFormasPago = oPags.Data
        End Function

        ''' <summary>
        ''' Devuelve las vías de pago para cargarlas en el combo
        ''' </summary>
        ''' <returns>Un dataset con las vías de pago</returns>
        ''' <remarks>Llamada desde: Page_Load()</remarks>
        Function DevolverViasPago() As DataSet
            Dim oViasPago As PMPortalServer.ViasPago
            oViasPago = oFsServer.Get_Object(GetType(PMPortalServer.ViasPago))
            oViasPago.LoadData(Idioma, lCiaComp)
            DevolverViasPago = oViasPago.Data
        End Function

        ''' <summary>
        ''' Devuelve las distintas partidas de la instalación
        ''' </summary>
        ''' <remarks></remarks>
        Private Function DevolverPartidas() As DataTable
            Dim oPress5 As Fullstep.PMPortalServer.PRES5
            oPress5 = oFsServer.Get_Press5

            Return oPress5.Partidas_LoadPartidasInstalacion(lCiaComp, Me.Idioma)
        End Function

        ''' <summary>
        ''' Proceso que carga la propiedad del usercontrol con los paises
        ''' </summary>
        ''' <remarks>Llamada desde:Page_load; Tiempo máximo:0,1seg.</remarks>
        Private Function CargarPaises()
            Dim oPaises As PMPortalServer.Paises
            oPaises = FSPMServer.Get_Paises

            oPaises.LoadData(Idioma, lCiaComp)

            CargarPaises = oPaises.Data.Tables(0)

            oPaises = Nothing
        End Function

        ''' <summary>
        ''' Carga la propiedad del usercontrol con las provincias de un determinado pais
        ''' </summary>
        ''' <param name="sCodPais">Codigo del pais</param>
        ''' <remarks>Llamada desde=wddSubtipo_ItemsRequested; Tiempo máximo=0seg.</remarks>
        Private Sub CargarProvincias(ByVal sCodPais As String) Handles Lineas_Factura.eventProvinciasItemRequested
            Dim oProvincias As PMPortalServer.Provincias
            oProvincias = FSPMServer.Get_Provincias

            oProvincias.Pais = sCodPais
            oProvincias.LoadData(Idioma, lCiaComp)

            Lineas_Factura.Provincias = oProvincias.Data.Tables(0)

            Lineas_Factura.CargarComboProvinciasImpuestos()

            oProvincias = Nothing
        End Sub

        ''' <summary>
        ''' Procedimiento que recalcula los diferentes importes en el resumen de la cabecera
        ''' </summary>
        ''' <param name="Tipo">Tipo de actualizacion:impuestos, costes o descuentos</param>
        ''' <param name="ImporteNetoLineasConCD">Importe Neto de las lineas con costes y descuentos incluidos, sin impuestos</param>
        ''' <remarks></remarks>
        Private Sub RecalcularImportesEnCabecera(ByVal Tipo As Byte, ByVal ImporteNetoLineasConCD As Double, ByVal dtImpuestos As DataTable, ByVal ImporteNetoLineasConCDYImpuestos As Double) Handles Lineas_Factura.EventoRecalcularImportes
            DatosGenerales.RecalcularImportesCabecera(Tipo, ImporteNetoLineasConCD, dtImpuestos, ImporteNetoLineasConCDYImpuestos)
        End Sub

        ''' <summary>
        ''' Carga la propiedad del usercontrol con los impuestos del buscador
        ''' </summary>
        ''' <param name="sCod">codigo del impuesto</param>
        ''' <param name="sDesc">descripcion del impuesto</param>
        ''' <param name="sPais">Pais</param>
        ''' <param name="sProv">provincia</param>
        ''' <param name="sCodArt">codigo de articulo</param>
        ''' <param name="sCodMat">codigo de material</param>
        ''' <remarks></remarks>
        Private Sub CargarImpuestosBuscador(ByVal sCod As String, ByVal sDesc As String, ByVal sPais As String, ByVal sProv As String, ByVal sCodArt As String, ByVal sCodMat As String, ByVal bRepercutido As Boolean, ByVal bRetenido As Boolean, ByVal iConceptoImpuesto As Nullable(Of TiposDeDatos.ConceptoImpuesto)) Handles Lineas_Factura.EventoBuscarImpuestos
            Dim oFacturas As PMPortalServer.Facturas
            oFacturas = oFsServer.Get_Object(GetType(PMPortalServer.Facturas))
            Lineas_Factura.ImpuestosBuscador = oFacturas.LoadImpuestosFacturas(lCiaComp, Idioma, sCod, sDesc, sPais, sProv, sCodArt, sCodMat, bRepercutido, bRetenido, iConceptoImpuesto)
        End Sub
        ''' <summary>
        ''' Carga la propiedad del usercontrol con los costes del buscador
        ''' </summary>
        ''' <param name="sCod">codigo del coste</param>
        ''' <param name="sDesc">descripcion del coste</param>
        ''' <param name="sCodArt">codigo de articulo</param>
        ''' <param name="sCodMat">codigo de material</param>
        ''' <param name="sLinea">codigo de linea</param>
        ''' <param name="sAlbaran">codigo de albaran</param>
        ''' <remarks></remarks>
        Private Sub CargarCostesBuscador(ByVal sCod As String, ByVal sDesc As String, ByVal sCodArt As String, ByVal sCodMat As String) Handles Lineas_Factura.EventoBuscarCostes
            Dim oFactura As PMPortalServer.Factura
            oFactura = oFsServer.Get_Object(GetType(PMPortalServer.Factura))
            Lineas_Factura.CostesBuscador = oFactura.Load_Atributos_BuscadorCostes(lCiaComp, sCod, sDesc, sCodArt, sCodMat)
        End Sub

        ''' <summary>
        ''' Carga la propiedad del usercontrol con los descuentos del buscador
        ''' </summary>
        ''' <param name="sCod">codigo del Descuento</param>
        ''' <param name="sDesc">descripcion del Descuento</param>
        ''' <param name="sCodArt">codigo de articulo</param>
        ''' <param name="sCodMat">codigo de material</param>
        ''' <param name="sLinea">codigo de linea</param>
        ''' <param name="sAlbaran">codigo de albaran</param>
        ''' <remarks></remarks>
        Private Sub CargarDescuentosBuscador(ByVal sCod As String, ByVal sDesc As String, ByVal sCodArt As String, ByVal sCodMat As String) Handles Lineas_Factura.EventoBuscarDescuentos
            Dim oFactura As PMPortalServer.Factura
            oFactura = oFsServer.Get_Object(GetType(PMPortalServer.Factura))
            Lineas_Factura.DescuentosBuscador = oFactura.Load_Atributos_BuscadorDescuentos(lCiaComp, sCod, sDesc, sCodArt, sCodMat)
        End Sub

        ''' <summary>
        ''' Configura la cabecera, muestra en el menu las posibles acciones
        ''' Activamos los botones que pueden aparecer en la pantalla
        ''' </summary>
        ''' <remarks>Llamada desde:=Page_load; Tiempo máximo:=0,1seg.</remarks>
        Private Sub ConfigurarCabeceraMenu()
            If m_desdePM Then
                FSNPageHeader.Visible = False
                Exit Sub
            End If

            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "images/Factura.png"
            FSNPageHeader.TituloCabecera = If(oFactura.Tipo = 1, Textos(11), Textos(12)) & " (" & oFactura.EstadoDen & ")"

            FSNPageHeader.VisibleBotonVolver = True
            FSNPageHeader.VisibleBotonMail = True
            'FSNPageHeader.VisibleBotonImpExp = True

            FSNPageHeader.TextoBotonVolver = Textos(0)
            FSNPageHeader.TextoBotonMail = Textos(1)
            'FSNPageHeader.TextoBotonImpExp = Textos(2)

            FSNPageHeader.OnClientClickVolver = "return Volver()"
            FSNPageHeader.OnClientClickMail = "return EnviarMail()"
            'FSNPageHeader.OnClientClickImpExp = "return cmdImpExp_onclick()"

            If oFactura.IdEFactura > 0 Then
                FSNPageHeader.VisibleBotonEFacturaPDF = True
                FSNPageHeader.VisibleBotonEFacturaXML = True
                FSNPageHeader.TextoBotonEFacturaPDF = Textos(9)
                FSNPageHeader.TextoBotonEFacturaXML = Textos(10)
                FSNPageHeader.OnClientClickEFacturaPDF = "return AbrirEFactura('PDF'," & oFactura.IdEFactura & ", '" & oFactura.NumeroFactura & "')"
                FSNPageHeader.OnClientClickEFacturaXML = "return AbrirEFactura('XML'," & oFactura.IdEFactura & ", '" & oFactura.NumeroFactura & "')"
            End If

            FSNPageHeader.TextoBotonEliminar = Textos(3)
            FSNPageHeader.TextoBotonGuardar = Textos(4)
            FSNPageHeader.TextoBotonAprobar = Textos(5)
            FSNPageHeader.TextoBotonRechazar = Textos(6)
            FSNPageHeader.TextoBotonAccion1 = Textos(7) 'Otras acciones

            FSNPageHeader.OnClientClickEliminar = "return EliminarInstancia()"
            FSNPageHeader.OnClientClickGuardar = "return Guardar()"

            Select Case oInstancia.Estado
                Case TipoEstadoSolic.Guardada   'Guardada
                    FSNPageHeader.VisibleBotonEliminar = True
                    'FSNPageHeader.VisibleBotonGuardar = True
                    FSNPageHeader.VisibleBotonAccion1 = True
                Case TipoEstadoSolic.Rechazada  'Rechazada    
                    FSNPageHeader.VisibleBotonAccion1 = True
                    FSNPageHeader.VisibleBotonEliminar = True
            End Select

            If oFactura.Estado = TipoEstadoSolic.Guardada Then
                'Si se esta en la etapa borrador pero se ha guardado la factura, el estado de la instancia sera 2(en curso) en vez de 0, hay que mirarlo por el estado
                'de la factura.
                FSNPageHeader.VisibleBotonEliminar = True
            End If

            ''''Carga las acciones posibles en el botón de Otras acciones
            'If oInstancia.Estado = TipoEstadoSolic.Guardada Or oInstancia.Estado = TipoEstadoSolic.Rechazada Then
            Dim oRol As PMPortalServer.Rol
            Dim oPopMenu As Infragistics.WebUI.UltraWebNavigator.UltraWebMenu
            Dim oItem As Infragistics.WebUI.UltraWebNavigator.Item

            oInstancia.DevolverEtapaActual(IdCiaComp, Idioma, FSPMUser.CodProveGS)

            hBloque.Value = oInstancia.Etapa
            RolActual.Value = oInstancia.RolActual

            If oInstancia.RolActual = 0 Or oInstancia.Etapa = 0 Then
                FSNPageHeader.VisibleBotonAccion1 = False
                FSNPageHeader.VisibleBotonGuardar = False
            Else
                oRol = FSPMServer.Get_Rol
                oRol.Id = oInstancia.RolActual
                oRol.Bloque = oInstancia.Etapa

                Dim oDSAcciones As DataSet
                If oInstancia.Estado = TipoEstadoSolic.Rechazada = True Then
                    oDSAcciones = oRol.CargarAcciones(lCiaComp, Idioma) '(Idioma, True, True)
                Else
                    oDSAcciones = oRol.CargarAcciones(lCiaComp, Idioma)
                End If

                oPopMenu = Me.uwPopUpAcciones
                Dim blnAprobar As Boolean = False
                Dim blnRechazar As Boolean = False
                Dim iAccionesRestarOtrasAcciones As Integer
                Dim iOtrasAcciones As Integer
                If oDSAcciones.Tables.Count > 0 Then
                    If oDSAcciones.Tables(0).Rows.Count > 1 Then
                        'Comprobamos si entre las acciones hay de tipo aprobar y rechazar
                        For Each oRow As DataRow In oDSAcciones.Tables(0).Rows
                            If DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                                blnAprobar = True
                                iAccionesRestarOtrasAcciones = iAccionesRestarOtrasAcciones + 1
                                If blnRechazar Then Exit For
                            ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                                blnRechazar = True
                                iAccionesRestarOtrasAcciones = iAccionesRestarOtrasAcciones + 1
                                If blnAprobar Then Exit For
                            End If
                        Next
                        iOtrasAcciones = oDSAcciones.Tables(0).Rows.Count - iAccionesRestarOtrasAcciones
                        Dim cont As Integer = 0
                        Dim bCambioEtapa As Byte
                        For Each oRow In oDSAcciones.Tables(0).Rows
                            Dim oAccion = FSPMServer.Get_Accion
                            Dim dsEnlaces As DataSet
                            dsEnlaces = oAccion.ObtenerEnlaces(oInstancia.Etapa, DBNullToInteger(oRow.Item("ACCION")), IdCiaComp)
                            If dsEnlaces.Tables(0).Rows.Count > 0 Then
                                bCambioEtapa = 1
                            Else
                                bCambioEtapa = 0
                            End If

                            'Comprueba las etapas por las que pasara:
                            If iOtrasAcciones > 3 AndAlso DBNullToSomething(oRow.Item("RA_APROBAR")) <> 1 AndAlso DBNullToSomething(oRow.Item("RA_RECHAZAR")) <> 1 Then
                                If IsDBNull(oRow.Item("DEN")) Then
                                    oItem = oPopMenu.Items.Add("&nbsp;")
                                Else
                                    If oRow.Item("DEN") = "" Then
                                        oItem = oPopMenu.Items.Add("&nbsp;")
                                    Else
                                        oItem = oPopMenu.Items.Add(DBNullToStr(oRow.Item("DEN")))
                                    End If
                                End If
                                oItem.TargetUrl = "javascript:EjecutarAccion(" + oRow.Item("ACCION").ToString() + ", " & DBNullToInteger(oRow.Item("APROB_DISCREPANCIAS")) & "," & bCambioEtapa & ")"
                            ElseIf DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                                FSNPageHeader.OnClientClickAprobar = "return EjecutarAccion(" + oRow.Item("ACCION").ToString() + ", " & DBNullToInteger(oRow.Item("APROB_DISCREPANCIAS")) & "," & bCambioEtapa & ")"
                            ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                                FSNPageHeader.OnClientClickRechazar = "return EjecutarAccion(" + oRow.Item("ACCION").ToString() + ", " & DBNullToInteger(oRow.Item("APROB_DISCREPANCIAS")) & "," & bCambioEtapa & ")"
                            Else
                                If cont = 0 Then
                                    FSNPageHeader.OnClientClickAccion1 = "return EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + ", " & DBNullToInteger(oRow.Item("APROB_DISCREPANCIAS")) & "," & bCambioEtapa & ")"
                                    FSNPageHeader.TextoBotonAccion1 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                                    FSNPageHeader.VisibleBotonAccion1 = True
                                    cont = 1
                                ElseIf cont = 1 Then
                                    FSNPageHeader.OnClientClickAccion2 = "return EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + ", " & DBNullToInteger(oRow.Item("APROB_DISCREPANCIAS")) & "," & bCambioEtapa & ")"
                                    FSNPageHeader.TextoBotonAccion2 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                                    FSNPageHeader.VisibleBotonAccion2 = True
                                    cont = 2
                                Else
                                    FSNPageHeader.OnClientClickAccion3 = "return EjecutarAccion(" + DBNullToSomething(oRow.Item("ACCION").ToString()) + ", " & DBNullToInteger(oRow.Item("APROB_DISCREPANCIAS")) & "," & bCambioEtapa & ")"
                                    FSNPageHeader.TextoBotonAccion3 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                                    FSNPageHeader.VisibleBotonAccion3 = True
                                End If
                            End If
                        Next
                        If blnAprobar Then
                            FSNPageHeader.VisibleBotonAprobar = True
                        End If
                        If blnRechazar Then
                            FSNPageHeader.VisibleBotonRechazar = True
                        End If
                        If iOtrasAcciones > 3 Then
                            FSNPageHeader.VisibleBotonAccion1 = True
                            FSNPageHeader.OnClientClickAccion1 = "igmenu_showMenu('" & uwPopUpAcciones.ClientID & "', event); return false;"
                        End If
                    Else
                        If oDSAcciones.Tables(0).Rows.Count = 0 Then
                            FSNPageHeader.VisibleBotonAccion1 = False
                        Else
                            Dim oRow As DataRow
                            oRow = oDSAcciones.Tables(0).Rows(0)
                            If DBNullToSomething(oRow.Item("RA_APROBAR")) = 1 Then
                                FSNPageHeader.VisibleBotonAccion1 = False
                                FSNPageHeader.VisibleBotonAprobar = True
                                FSNPageHeader.OnClientClickAprobar = " return EjecutarAccion(" + oRow.Item("ACCION").ToString() + ", " & DBNullToInteger(oRow.Item("APROB_DISCREPANCIAS")) & ",1)"
                            ElseIf DBNullToSomething(oRow.Item("RA_RECHAZAR")) = 1 Then
                                FSNPageHeader.VisibleBotonAccion1 = False
                                FSNPageHeader.VisibleBotonRechazar = True
                                FSNPageHeader.OnClientClickRechazar = "return EjecutarAccion(" + oRow.Item("ACCION").ToString() + ", " & DBNullToInteger(oRow.Item("APROB_DISCREPANCIAS")) & ",1)"
                            Else
                                FSNPageHeader.VisibleBotonAccion1 = True
                                FSNPageHeader.OnClientClickAccion1 = "return EjecutarAccion(" + oRow.Item("ACCION").ToString() + ", " & DBNullToInteger(oRow.Item("APROB_DISCREPANCIAS")) & ",1)"
                                FSNPageHeader.TextoBotonAccion1 = AcortarTexto(DBNullToStr(oRow.Item("DEN")), 20)
                            End If
                        End If
                    End If
                Else
                    FSNPageHeader.VisibleBotonAccion1 = False
                End If
            End If
        End Sub

        Sub GenerarScripts()
            Dim includeScript As String
            If Not Page.ClientScript.IsClientScriptBlockRegistered("Eliminar") Then
                includeScript = "function EliminarInstancia() {" & vbCrLf
                includeScript = includeScript & "if (confirm('" & Textos(8) & "') == true) {" & vbCrLf
                includeScript = includeScript & "window.open('EliminarFactura.aspx?Instancia=" & oInstancia.ID & "&Factura=" & oFactura.ID & "','_self');" & vbCrLf
                includeScript = includeScript & "}" & vbCrLf
                includeScript = includeScript & "return false;" & vbCrLf
                includeScript = includeScript & "}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Eliminar", includeScript, True)
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered("Volver") Then
                includeScript = "function Volver() {" & vbCrLf
                If Request("ORIGEN") = "entregasPedidos" Then
                    includeScript = includeScript & "window.open('../entregas/entregasPedidos.aspx','_self');" & vbCrLf
                    'Pruebas 31900.7 / 2013 / 285 Volver en lugar de volver al visor de entregas de pedidos me vuelve al visor de facturas
                Else
                    includeScript = includeScript & "window.open('VisorFacturas.aspx','_self');" & vbCrLf
                End If
                includeScript = includeScript & "return false;" & vbCrLf
                includeScript = includeScript & "}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Volver", includeScript, True)
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered("EnviarMail") Then
                includeScript = "function EnviarMail() {" & vbCrLf
                includeScript = includeScript & "window.open('EnviarMail.aspx?Instancia=" & oInstancia.ID & "&NumeroFactura=" & oFactura.NumeroFactura & "','_blank', 'width=692,height=330,status=yes,resizable=no,top=180,left=200');" & vbCrLf
                includeScript = includeScript & "return false;" & vbCrLf
                includeScript = includeScript & "}"
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "EnviarMail", includeScript, True)
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "NumberFormatUsu") Then
                With FSPMUser
                    Dim sVariablesJavascript As String = "var UsuNumberDecimalSeparator = '" & .NumberFormat.NumberDecimalSeparator & "';"
                    sVariablesJavascript = sVariablesJavascript & "var UsuNumberGroupSeparator='" & .NumberFormat.NumberGroupSeparator & "';"
                    sVariablesJavascript = sVariablesJavascript & "var UsuNumberNumDecimals='" & .NumberFormat.NumberDecimalDigits & "';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NumberFormatUsu", sVariablesJavascript, True)
                End With
            End If
        End Sub

        Sub BuscarCostes(ByVal sCod As String, ByVal sDen As String, ByVal sCodArt As String, ByVal sCodMat As String) Handles DatosGenerales.EventoBuscarCostes
            DatosGenerales.AtributosBuscadorCostes = oFactura.Load_Atributos_BuscadorCostes(lCiaComp, sCod, sDen, sCodArt, sCodMat)
        End Sub

        Sub BuscarDescuentos(ByVal sCod As String, ByVal sDen As String, ByVal sCodArt As String, ByVal sCodMat As String) Handles DatosGenerales.EventoBuscarDescuentos
            DatosGenerales.AtributosBuscadorDescuentos = oFactura.Load_Atributos_BuscadorDescuentos(lCiaComp, sCod, sDen, sCodArt, sCodMat)
        End Sub

        Sub CargarDetalleAlbaran(ByVal sAlbaran As String) Handles Lineas_Factura.EventoCargarDetalleAlbaran
            oFactura.LoadDetalleAlbaran(lCiaComp, sAlbaran)

            Lineas_Factura.AlbaranFechaRecepcion = oFactura.FechaAlbaran
            Lineas_Factura.AlbaranNumeroRecepcionERP = oFactura.NumRecepcionErp
            Lineas_Factura.CodReceptor = oFactura.CodReceptor
            Lineas_Factura.NombreReceptor = oFactura.NombreReceptor
            Lineas_Factura.Cargo = oFactura.Cargo
            Lineas_Factura.Departamento = oFactura.Departamento
            Lineas_Factura.Email = oFactura.Email
            Lineas_Factura.Telefono = oFactura.Telefono
            Lineas_Factura.Fax = oFactura.Fax
            Lineas_Factura.Organizacion = oFactura.Organizacion
            Lineas_Factura.CostesAlbaran = oFactura.CostesAlbaran
            Lineas_Factura.DescuentosAlbaran = oFactura.DescuentosAlbaran
        End Sub


        ''' <summary>
        ''' Genera la estructura que va a tener el dataset donde se guarda toda la información de la factura para guardarla en xml y que la procese el webservice.
        ''' </summary>
        ''' <remarks>LLamada desde: Page_Load()</remarks>
        Sub GenerarEstructuraDataset()
            Dim dt As DataTable
            Dim ds As DataSet
            Dim keys(0) As DataColumn

            ds = New DataSet

            dt = ds.Tables.Add("FACTURA")
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE_BRUTO", System.Type.GetType("System.Double"))
            dt.Columns.Add("PAG", System.Type.GetType("System.String"))
            dt.Columns.Add("VIA_PAG", System.Type.GetType("System.String"))
            dt.Columns.Add("OBS", System.Type.GetType("System.String"))
            dt.Columns.Add("TIPO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("TOT_COSTES", System.Type.GetType("System.Double"))
            dt.Columns.Add("TOT_DESCUENTOS", System.Type.GetType("System.Double"))
            dt.Columns.Add("FEC_CONTA", System.Type.GetType("System.DateTime"))

            keys(0) = dt.Columns("ID")
            dt.PrimaryKey = keys

            dt = ds.Tables.Add("LINEAS_FACTURA")
            dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("NUM", System.Type.GetType("System.Int32"))
            dt.Columns.Add("TOTAL_COSTES", System.Type.GetType("System.Double"))
            dt.Columns.Add("TOTAL_DCTOS", System.Type.GetType("System.Double"))
            dt.Columns.Add("TOTAL_IMP", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE_SIN_IMP", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("OBS", System.Type.GetType("System.String"))

            dt = ds.Tables.Add("COSTES")
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("DEN", System.Type.GetType("System.String"))
            dt.Columns.Add("OPERACION", System.Type.GetType("System.String"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("PLANIFICADO", System.Type.GetType("System.Int32"))

            dt = ds.Tables.Add("DESCUENTOS")
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("DEN", System.Type.GetType("System.String"))
            dt.Columns.Add("OPERACION", System.Type.GetType("System.String"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("PLANIFICADO", System.Type.GetType("System.Int32"))

            dt = ds.Tables.Add("LINEA_IMPUESTOS")
            dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("COD", System.Type.GetType("System.String"))
            dt.Columns.Add("DEN", System.Type.GetType("System.String"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("RETENIDO", System.Type.GetType("System.Int16"))
            dt.Columns.Add("COMENT", System.Type.GetType("System.String"))

            dt = ds.Tables.Add("COSTES_ALBARAN")
            dt.Columns.Add("ALBARAN", System.Type.GetType("System.String"))
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("DEN", System.Type.GetType("System.String"))
            dt.Columns.Add("OPERACION", System.Type.GetType("System.String"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("PLANIFICADO", System.Type.GetType("System.Int32"))

            dt = ds.Tables.Add("DESCUENTOS_ALBARAN")
            dt.Columns.Add("ALBARAN", System.Type.GetType("System.String"))
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("DEN", System.Type.GetType("System.String"))
            dt.Columns.Add("OPERACION", System.Type.GetType("System.String"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("PLANIFICADO", System.Type.GetType("System.Int32"))

            dt = ds.Tables.Add("LINEA_COSTES")
            dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("DEN", System.Type.GetType("System.String"))
            dt.Columns.Add("OPERACION", System.Type.GetType("System.String"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("ALBARAN", System.Type.GetType("System.String"))
            dt.Columns.Add("PLANIFICADO", System.Type.GetType("System.Int32"))

            dt = ds.Tables.Add("LINEA_DESCUENTOS")
            dt.Columns.Add("LINEA", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("DEN", System.Type.GetType("System.String"))
            dt.Columns.Add("OPERACION", System.Type.GetType("System.String"))
            dt.Columns.Add("VALOR", System.Type.GetType("System.Double"))
            dt.Columns.Add("IMPORTE", System.Type.GetType("System.Double"))
            dt.Columns.Add("ALBARAN", System.Type.GetType("System.String"))
            dt.Columns.Add("PLANIFICADO", System.Type.GetType("System.Int32"))

            dt = ds.Tables.Add("LINEAS_ELIMINADAS")
            dt.Columns.Add("LINEAS", System.Type.GetType("System.String"))

            dt = ds.Tables.Add("TEMP")
            dt.Columns.Add("CC_ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("CCD_ID", System.Type.GetType("System.Int32"))
            dt.Columns.Add("CAMPO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("VALOR_TEXT", System.Type.GetType("System.String"))
            dt.Columns.Add("VALOR_NUM", System.Type.GetType("System.Double"))
            dt.Columns.Add("VALOR_FEC", System.Type.GetType("System.DateTime"))
            dt.Columns.Add("VALOR_BOOL", System.Type.GetType("System.Int32"))
            dt.Columns.Add("ES_SUBCAMPO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("SUBTIPO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("TIPOGS", System.Type.GetType("System.Int32"))
            dt.Columns.Add("CAMBIO", System.Type.GetType("System.Double"))
            dt.Columns.Add("SAVE_VALUES", System.Type.GetType("System.Int32"))
            dt.Columns.Add("CAMPO_DEF", System.Type.GetType("System.Int32"))
            dt.Columns.Add("CONTACTOS", System.Type.GetType("System.String"))

            dt = ds.Tables.Add("TEMP_PARTICIPANTES")
            dt.Columns.Add("ROL", System.Type.GetType("System.Int32"))
            dt.Columns.Add("TIPO", System.Type.GetType("System.Int16"))
            dt.Columns.Add("PER", System.Type.GetType("System.String"))
            dt.Columns.Add("PROVE", System.Type.GetType("System.String"))
            dt.Columns.Add("CON", System.Type.GetType("System.String"))

            dt = ds.Tables.Add("ROLES")
            dt.Columns.Add("ROL", System.Type.GetType("System.Int32"))
            dt.Columns.Add("PER", System.Type.GetType("System.String"))
            dt.Columns.Add("PROVE", System.Type.GetType("System.String"))
            dt.Columns.Add("CON", System.Type.GetType("System.String"))
            dt.Columns.Add("TIPO", System.Type.GetType("System.Int32"))
            dt.Columns.Add("CAMPO", System.Type.GetType("System.Int32"))

            dt = ds.Tables.Add("SOLICITUD")
            With dt.Columns
                .Add("TIPO_PROCESAMIENTO_XML")
                .Add("TIPO_DE_SOLICITUD")
                .Add("COMPLETO")
                .Add("INSTANCIA")
                .Add("SOLICITUD")
                .Add("CIACOMP")
                .Add("PROVE")
                .Add("COD")
                .Add("CODCIA")
                .Add("DENCIA")
                .Add("NIFCIA")
                .Add("IDCIA")
                .Add("IDUSU")
                .Add("NOMBRE")
                .Add("APELLIDOS")
                .Add("TELEFONO")
                .Add("USUARIO_EMAIL")
                .Add("FAX")
                .Add("USUARIO_IDIOMA")
                .Add("PROVEGS")
                .Add("IMPORTE", System.Type.GetType("System.Double"))
                .Add("ACCION")
                .Add("IDACCION")
                .Add("GUARDAR")
                .Add("DEVOLUCION_COMENTARIO")
                .Add("COMENTARIO")
                .Add("BLOQUE_ORIGEN")
                .Add("BLOQUES_DESTINO")
                .Add("NOTIFICAR")
                .Add("FACTURA")
                .Add("IDTIEMPOPROC")
            End With

            Session("dsXMLFactura") = ds
        End Sub

        Private m_sIdiListaPart As String
        Private m_sIdiMsgbox As String
        Private m_sIdiSeleccioneRol As String
        ''' <summary>
        ''' Carga los textos de pantalla y el grid de roles para confirmar/cancelar la ejecución de una acción
        ''' </summary>
        ''' <remarks>Llamada desde:GuardarConWorkflow; Tiempo máximo:0,2</remarks>
        Sub RealizarAccion(ByVal lAccion As Long)
            Dim bMostrarConfirmacionAccion As Boolean = True

            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.RealizarAccion
            Session("EtapaFIN") = False

            'Textos de idiomas:
            Me.lblEtapas.Text = Textos(3)
            m_sIdiSeleccioneRol = Textos(5)
            Me.lblComentarios.Text = Textos(8)
            Me.btnAceptar.Text = Textos(9)
            Me.btnCancelar.Text = Textos(10)
            Session("m_sIdiMsgbox") = Textos(13)
            m_sIdiListaPart = Textos(12)

            'Carga la acción:
            Dim oAccion As Fullstep.PMPortalServer.Accion
            oAccion = FSPMServer.Get_Accion
            oAccion.Id = lAccion
            oAccion.CargarAccion(IdCiaComp, Idioma)
            oInstancia.Accion = lAccion

            imgCabeceraConfirmacion.Src = System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "images/Factura.png"
            lblCabeceraConfirmacion.Text = "Nº Factura: " & oFactura.NumeroFactura & " - " & Textos(11) & " " & oAccion.Den

            If oAccion.TipoRechazo = TipoRechazoAccion.RechazoDefinitivo Or oAccion.TipoRechazo = TipoRechazoAccion.RechazoTemporal Then
                lblRoles.Text = Textos(14)
            Else
                lblRoles.Text = Textos(4)
            End If

            'Lo metemos aquí (antes en GuardarThread) para pasarle el dataset a DevolverSiguientesEtapas
            InsertarLineas()
            InsertarImpuestos()
            InsertarCostesLinea()
            InsertarCostesAlbaran()
            InsertarDescuentosLinea()
            InsertarDescuentosAlbaran()

            'Comprueba las etapas por las que pasará:
            Dim oEtapas As DataSet
            oEtapas = oInstancia.DevolverSiguientesEtapas(IdCiaComp, lAccion, FSPMUser.CodProve, Idioma, hBloque.Value, RolActual.Value, CType(Session("dsXMLFactura"), DataSet))

            If oEtapas.Tables.Count > 0 Then
                If oEtapas.Tables.Count > 1 Then
                    lstEtapas.DataSource = oEtapas.Tables(0)
                    lstEtapas.DataTextField = "DEN"
                    lstEtapas.DataBind()

                    If oEtapas.Tables(0).Rows(0).Item("TIPO") = 2 Then   'Si llega a la etapa de fin:
                        Me.lblRoles.Visible = False
                        Me.tblGeneral.Rows(3).Visible = False
                        Me.lblComentarios.Text = String.Format("{0}:", Textos(15))
                        Session("EtapaFIN") = True
                    Else
                        Dim ds As New DataSet
                        ds.Tables.Add(oEtapas.Tables(1).Copy)
                        whdgRoles.DataSource = ds
                        whdgRoles.DataBind()
                    End If
                Else ' = 1
                    Exit Sub
                End If

            Else
                If oAccion.Tipo = 0 And oAccion.TipoRechazo = 0 And oAccion.Guardar = True Then
                    bMostrarConfirmacionAccion = False

                    Dim oThread As Thread = New Thread(AddressOf GuardarThread)
                    oThread.Start()
                Else
                    'No provoca ningún cambio de etapa, así que quita del formulario las filas
                    'correspondientes a los cambios de etapas y formularios:
                    Dim i As Integer
                    For i = 4 To 0 Step -1
                        Me.tblGeneral.Rows(i).Visible = False
                    Next i
                End If
            End If

            If bMostrarConfirmacionAccion Then
                If Not Page.ClientScript.IsClientScriptBlockRegistered("varMensaje") Then _
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varMensaje", "<script>var sMensaje = '" & JSText(Textos(13)) & "' </script>")
                ClientScript.RegisterStartupScript(Me.GetType(), "DevolverSiguientesEtapas", "<script>document.getElementById('" & divDevolverSiguientesEtapas.ClientID & "').style.display='inline';document.getElementById('" & divDetalleFactura.ClientID & "').style.display='none';</script>")
            End If

            oEtapas = Nothing
        End Sub

#Region "WebHierarchicalDataGrid ROLES"

        Private Sub whdgRoles_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgRoles.InitializeRow
            If e.Row.Items.FindItemByKey("GESTOR_FACTURA").Value = 0 AndAlso DBNullToSomething(e.Row.Items.FindItemByKey("PER").Value) = Nothing AndAlso DBNullToSomething(e.Row.Items.FindItemByKey("PROVE").Value) = Nothing Then
                e.Row.Items.FindItemByKey("SEL").Text = "..."
            End If

            If DBNullToInteger(e.Row.Items.FindItemByKey("COMO_ASIGNAR").Value) = 3 Then  'Es una lista de participantes
                e.Row.Items.FindItemByKey("NOMBRE").Text = m_sIdiListaPart
            ElseIf DBNullToInteger(e.Row.Items.FindItemByKey("COMO_ASIGNAR").Value) = 1 And DBNullToSomething(e.Row.Items.FindItemByKey("PER").Value) = Nothing And DBNullToSomething(e.Row.Items.FindItemByKey("PROVE").Value) = Nothing Then
                e.Row.Items.FindItemByKey("NOMBRE").Text = m_sIdiSeleccioneRol
                e.Row.Items.FindItemByKey("NOMBRE").CssClass = "fntRequired"
            End If
        End Sub

        Private Sub whdgRoles_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles whdgRoles.PreRender
            ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.RealizarAccion
            whdgRoles.Columns("DEN").Header.Text = Textos(6)
            whdgRoles.Columns("NOMBRE").Header.Text = Textos(7)
        End Sub
#End Region

        Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
            Dim oThread As Thread
            oThread = New Thread(AddressOf GuardarThread)
            oThread.Start()
            'Ir al visor de facturas
            Response.Redirect("VisorFacturas.aspx")
        End Sub

#Region "GENERAR XML Y LLAMAR AL WEBSERVICE"
        ''' <summary>
        ''' completa el XML que se enviará el servicio
        ''' </summary>
        ''' <remarks>Llamada desde: RealizarAccion, btnAceptar_Click ; Tiempo máximo: 0,5 sg</remarks>
        Private Sub GuardarThread()
            Try
                oInstancia.Actualizar_En_proceso(lCiaComp, 1, oInstancia.Etapa)

                Dim oAccion As Fullstep.PMPortalServer.Accion
                oAccion = FSPMServer.Get_Accion
                oAccion.Id = oInstancia.Accion
                oAccion.CargarAccion(IdCiaComp, Idioma)

                Dim sProveGS As String
                sProveGS = FSPMUser.ObtenerProv(lCiaComp)

                If Session("EtapaFIN") Then
                    InsertarFechaContabilizacion()
                End If
                If Session("LineasEliminadas") <> String.Empty Then
                    InsertarLineasEliminadas()
                End If
                Dim lIDTiempoProc As Long
                oInstancia.ActualizarTiempoProcesamiento(lCiaComp, lIDTiempoProc, FSPMUser.CodCia)
                Dim drSolicitud As DataRow
                drSolicitud = CType(Session("dsXMLFactura").Tables("SOLICITUD"), DataTable).NewRow
                With drSolicitud
                    .Item("TIPO_PROCESAMIENTO_XML") = CInt(TipoProcesamientoXML.Portal)
                    .Item("TIPO_DE_SOLICITUD") = CInt(TiposDeDatos.TipoDeSolicitud.AutoFactura)   'Solicitud de pedido
                    .Item("COMPLETO") = 1
                    .Item("INSTANCIA") = oInstancia.ID
                    .Item("SOLICITUD") = ""
                    .Item("CIACOMP") = lCiaComp
                    .Item("PROVE") = FSPMUser.CodProve
                    .Item("COD") = FSPMUser.Cod
                    .Item("CODCIA") = FSPMUser.CodCia
                    .Item("DENCIA") = FSPMUser.DenCia
                    .Item("NIFCIA") = FSPMUser.NIFCia
                    .Item("IDCIA") = FSPMUser.IdCia
                    .Item("IDUSU") = FSPMUser.IdUsu
                    .Item("NOMBRE") = FSPMUser.Nombre
                    .Item("APELLIDOS") = FSPMUser.Apellidos
                    .Item("TELEFONO") = FSPMUser.Telefono
                    .Item("USUARIO_EMAIL") = FSPMUser.Email
                    .Item("FAX") = IIf(FSPMUser.Fax = Nothing, "", FSPMUser.Fax) 'es el único campo que puede ser NULL
                    .Item("USUARIO_IDIOMA") = FSPMUser.Idioma
                    .Item("PROVEGS") = sProveGS
                    .Item("IMPORTE") = 0
                    .Item("ACCION") = ""
                    .Item("IDACCION") = oInstancia.Accion
                    .Item("GUARDAR") = 1
                    .Item("COMENTARIO") = Left(Me.txtComent.InnerText, 500)
                    .Item("BLOQUE_ORIGEN") = oInstancia.Etapa
                    .Item("FACTURA") = oFactura.ID
                    .Item("IDTIEMPOPROC") = lIDTiempoProc
                End With
                CType(Session("dsXMLFactura").Tables("SOLICITUD"), DataTable).Clear()
                CType(Session("dsXMLFactura").Tables("SOLICITUD"), DataTable).Rows.Add(drSolicitud)

                Dim sXMLName As String = FSPMUser.CodCia & "#" & FSPMUser.Cod & "#" & oInstancia.ID & "#" & oInstancia.Etapa
                oInstancia.ActualizarTiempoProcesamiento(lIDTiempoProc, iFecha:=TiempoProcesamiento.InicioDisco)
                'le ponemos la P para saber que es de portal
                If ConfigurationManager.AppSettings("SERVIDOR_TRATAMIENTO_INSTANCIAS") = "0" Then
                    'Se hace con un StringWriter porque el método GetXml del dataset no incluye el esquema
                    Dim oSW As New System.IO.StringWriter()
                    CType(Session("dsXMLFactura").Tables("SOLICITUD"), DataTable).WriteXml(oSW, XmlWriteMode.WriteSchema)

                    Dim oSrvTrataInst As New FSNWebServiceXML.TratamientoInstancias
                    oSrvTrataInst.TransferirXMLEnEpera(oSW.ToString(), sXMLName)
                Else
                    CType(Session("dsXMLFactura").Tables("SOLICITUD"), DataTable).WriteXml(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & "#PORTAL.xml", XmlWriteMode.WriteSchema)
                    If File.Exists(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml") Then _
                        File.Delete(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml")
                    FileSystem.Rename(ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & "#PORTAL.xml", _
                                      ConfigurationManager.AppSettings("rutaXMLEnEspera") & "\" & sXMLName & ".xml")
                End If
            Catch ex As Exception

            End Try
        End Sub
        ''' <summary>
        ''' Inserta en el dataset las líneas
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub InsertarLineas()
            Dim oDTLineas As DataTable = CType(Session("dsXMLFactura").Tables("LINEAS_FACTURA"), DataTable)
            Dim dtNewRow As DataRow
            Dim drRowLinea As DataRow

            oDTLineas.Clear()
            For Each drRowLinea In CType(Cache("dsFactura" & FSPMUser.IdCia & FSPMUser.Cod), DataSet).Tables(6).Rows
                If drRowLinea.RowState <> DataRowState.Deleted Then
                    dtNewRow = oDTLineas.NewRow
                    dtNewRow.Item("LINEA") = drRowLinea("LINEA")
                    dtNewRow.Item("NUM") = drRowLinea("NUM")
                    dtNewRow.Item("TOTAL_COSTES") = drRowLinea("TOTAL_COSTES")
                    dtNewRow.Item("TOTAL_DCTOS") = drRowLinea("TOTAL_DCTOS")
                    dtNewRow.Item("TOTAL_IMP") = drRowLinea("TOTAL_IMP")
                    dtNewRow.Item("IMPORTE_SIN_IMP") = drRowLinea("IMPORTE_SIN_IMP")
                    dtNewRow.Item("IMPORTE") = drRowLinea("IMPORTE")
                    dtNewRow.Item("OBS") = drRowLinea("OBS")
                    If dtNewRow.RowState = DataRowState.Detached Then
                        oDTLineas.Rows.Add(dtNewRow)
                    End If
                End If
            Next
        End Sub
        ''' <summary>
        ''' Inserta en el dataset los impuestos de las líneas
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub InsertarImpuestos()
            Dim oDTImpuestos As DataTable = CType(Session("dsXMLFactura").Tables("LINEA_IMPUESTOS"), DataTable)
            Dim dtNewRow As DataRow
            Dim drRowImpuesto As DataRow

            oDTImpuestos.Clear()
            For Each drRowImpuesto In CType(Cache("dsFactura" & FSPMUser.IdCia & FSPMUser.Cod), DataSet).Tables(10).Rows
                If drRowImpuesto.RowState <> DataRowState.Deleted AndAlso Not drRowImpuesto("VALOR") Is DBNull.Value Then
                    dtNewRow = oDTImpuestos.NewRow
                    dtNewRow.Item("LINEA") = drRowImpuesto("LINEA")
                    dtNewRow.Item("COD") = drRowImpuesto("COD")
                    dtNewRow.Item("DEN") = drRowImpuesto("DEN")
                    dtNewRow.Item("VALOR") = drRowImpuesto("VALOR")
                    dtNewRow.Item("IMPORTE") = drRowImpuesto("IMPORTE")
                    dtNewRow.Item("RETENIDO") = drRowImpuesto("RETENIDO")
                    dtNewRow.Item("COMENT") = drRowImpuesto("COMENT")
                    If dtNewRow.RowState = DataRowState.Detached Then
                        oDTImpuestos.Rows.Add(dtNewRow)
                    End If
                End If
            Next
        End Sub
        Public Sub InsertarCostesLinea()
            Dim oDTCostesLinea As DataTable = CType(Session("dsXMLFactura").Tables("LINEA_COSTES"), DataTable)
            Dim dtNewRow As DataRow
            Dim drRowCoste As DataRow

            oDTCostesLinea.Clear()
            For Each drRowCoste In CType(Cache("dsFactura" & FSPMUser.IdCia & FSPMUser.Cod), DataSet).Tables(11).Rows
                If drRowCoste.RowState <> DataRowState.Deleted AndAlso Not drRowCoste("VALOR") Is DBNull.Value Then
                    dtNewRow = oDTCostesLinea.NewRow
                    dtNewRow.Item("LINEA") = drRowCoste("LINEA")
                    dtNewRow.Item("ID") = drRowCoste("DEF_ATRIB_ID")
                    dtNewRow.Item("DEN") = drRowCoste("DEN")
                    dtNewRow.Item("OPERACION") = drRowCoste("OPERACION")
                    dtNewRow.Item("VALOR") = drRowCoste("VALOR")
                    dtNewRow.Item("IMPORTE") = drRowCoste("IMPORTE")
                    dtNewRow.Item("ALBARAN") = drRowCoste("ALBARAN")
                    dtNewRow.Item("PLANIFICADO") = DBNullToInt(drRowCoste("PLANIFICADO"))
                    If dtNewRow.RowState = DataRowState.Detached Then
                        oDTCostesLinea.Rows.Add(dtNewRow)
                    End If
                End If
            Next
        End Sub
        Public Sub InsertarCostesAlbaran()
            Dim oDTCostesAlbaran As DataTable = CType(Session("dsXMLFactura").Tables("COSTES_ALBARAN"), DataTable)
            Dim dtNewRow As DataRow
            Dim drRowCoste As DataRow

            oDTCostesAlbaran.Clear()
            For Each drRowCoste In CType(Cache("dsFactura" & FSPMUser.IdCia & FSPMUser.Cod), DataSet).Tables(12).Rows
                If drRowCoste.RowState <> DataRowState.Deleted AndAlso Not drRowCoste("VALOR") Is DBNull.Value Then
                    dtNewRow = oDTCostesAlbaran.NewRow
                    dtNewRow.Item("ALBARAN") = drRowCoste("ALBARAN")
                    dtNewRow.Item("ID") = drRowCoste("DEF_ATRIB_ID")
                    dtNewRow.Item("DEN") = drRowCoste("DEN")
                    dtNewRow.Item("OPERACION") = drRowCoste("OPERACION")
                    dtNewRow.Item("VALOR") = drRowCoste("VALOR")
                    dtNewRow.Item("IMPORTE") = drRowCoste("IMPORTE")
                    dtNewRow.Item("PLANIFICADO") = DBNullToInt(drRowCoste("PLANIFICADO"))
                    If dtNewRow.RowState = DataRowState.Detached Then
                        oDTCostesAlbaran.Rows.Add(dtNewRow)
                    End If
                End If
            Next
        End Sub
        Public Sub InsertarDescuentosLinea()
            Dim oDTDescuentosLinea As DataTable = CType(Session("dsXMLFactura").Tables("LINEA_DESCUENTOS"), DataTable)
            Dim dtNewRow As DataRow
            Dim drRowDescuento As DataRow

            oDTDescuentosLinea.Clear()
            For Each drRowDescuento In CType(Cache("dsFactura" & FSPMUser.IdCia & FSPMUser.Cod), DataSet).Tables(13).Rows
                If drRowDescuento.RowState <> DataRowState.Deleted AndAlso Not drRowDescuento("VALOR") Is DBNull.Value Then
                    dtNewRow = oDTDescuentosLinea.NewRow
                    dtNewRow.Item("LINEA") = drRowDescuento("LINEA")
                    dtNewRow.Item("ID") = drRowDescuento("DEF_ATRIB_ID")
                    dtNewRow.Item("DEN") = drRowDescuento("DEN")
                    dtNewRow.Item("OPERACION") = drRowDescuento("OPERACION")
                    dtNewRow.Item("VALOR") = drRowDescuento("VALOR")
                    dtNewRow.Item("IMPORTE") = drRowDescuento("IMPORTE")
                    dtNewRow.Item("ALBARAN") = drRowDescuento("ALBARAN")
                    dtNewRow.Item("PLANIFICADO") = DBNullToInt(drRowDescuento("PLANIFICADO"))
                    If dtNewRow.RowState = DataRowState.Detached Then
                        oDTDescuentosLinea.Rows.Add(dtNewRow)
                    End If
                End If
            Next
        End Sub
        Public Sub InsertarDescuentosAlbaran()
            Dim oDTDescuentosAlbaran As DataTable = CType(Session("dsXMLFactura").Tables("DESCUENTOS_ALBARAN"), DataTable)
            Dim dtNewRow As DataRow
            Dim drRowDescuento As DataRow

            oDTDescuentosAlbaran.Clear()
            For Each drRowDescuento In CType(Cache("dsFactura" & FSPMUser.IdCia & FSPMUser.Cod), DataSet).Tables(14).Rows
                If drRowDescuento.RowState <> DataRowState.Deleted AndAlso Not drRowDescuento("VALOR") Is DBNull.Value Then
                    dtNewRow = oDTDescuentosAlbaran.NewRow
                    dtNewRow.Item("ALBARAN") = drRowDescuento("ALBARAN")
                    dtNewRow.Item("ID") = drRowDescuento("DEF_ATRIB_ID")
                    dtNewRow.Item("DEN") = drRowDescuento("DEN")
                    dtNewRow.Item("OPERACION") = drRowDescuento("OPERACION")
                    dtNewRow.Item("VALOR") = drRowDescuento("VALOR")
                    dtNewRow.Item("IMPORTE") = drRowDescuento("IMPORTE")
                    dtNewRow.Item("PLANIFICADO") = DBNullToInt(drRowDescuento("PLANIFICADO"))
                    If dtNewRow.RowState = DataRowState.Detached Then
                        oDTDescuentosAlbaran.Rows.Add(dtNewRow)
                    End If
                End If
            Next
        End Sub
        Public Sub InsertarLineasEliminadas()
            Dim oDTLineasEliminadas As DataTable = CType(Session("dsXMLFactura").Tables("LINEAS_ELIMINADAS"), DataTable)
            Dim dtNewRow As DataRow

            dtNewRow = oDTLineasEliminadas.NewRow
            dtNewRow.Item("LINEAS") = Session("LineasEliminadas")
            oDTLineasEliminadas.Rows.Add(dtNewRow)
        End Sub
        Public Sub InsertarFechaContabilizacion()
            Dim oDT As DataTable = CType(Session("dsXMLFactura").Tables("FACTURA"), DataTable)
            If oDT.Rows.Count > 0 Then
                Dim dtRow As DataRow = oDT.Rows(0)
                dtRow.Item("FEC_CONTA") = Now
            End If
        End Sub
#End Region
    End Class
End Namespace
