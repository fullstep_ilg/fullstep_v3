﻿Imports Infragistics.Web.UI.GridControls
Imports System.Collections.ObjectModel

Namespace Fullstep.PMPortalWeb
    Public Class VisorFacturas
        Inherits FSPMPage

        Private _PaginadorBottom As PMPortalWeb.Paginador
        Private _PaginadorTop As PMPortalWeb.Paginador
        Private oFsServer As Fullstep.PMPortalServer.Root

        Private lCiaComp As Long
        Private dFechaDesdeConfigurada As Date
        Private NumFacturasPendientes As Long


        ''' <summary>
        ''' Dataset de Facturas
        ''' </summary>
        Private ReadOnly Property DatasetFacturas() As DataSet
            Get
                If HttpContext.Current.Cache("dsFacturas_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString) IsNot Nothing Then
                    Return HttpContext.Current.Cache("dsFacturas_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString)
                Else
                    Return ObtenerDataSetFacturas()
                End If
            End Get
        End Property

        ''' <summary>
        ''' Evento que se lanza al inicializarse la pagina
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            setCultureInfoUsuario()

            Me._PaginadorTop = TryCast(Me.whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("wdgPaginador"), PMPortalWeb.Paginador)
            AddHandler Me._PaginadorTop.PageChanged, AddressOf _Paginador_PageChanged
        End Sub

        ''' <summary>
        ''' Evento que se lanza al cargarse la pagina
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            oFsServer = Me.FSPMServer

            lCiaComp = Session("FS_Portal_User").CiaComp
            ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorFacturas

            ' Discrepancias
            ' LÃ­mite de mensajes cargados 
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "LimiteMensajesCargados") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "LimiteMensajesCargados", "<script>var LimiteMensajesCargados =" & System.Configuration.ConfigurationManager.AppSettings("LimiteMensajesCargados") & "</script>")
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "rutanormal") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutanormal", "<script>var rutanormal = '" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "';" & _
                    "var rutaFSNWeb='" & System.Configuration.ConfigurationManager.AppSettings("rutaFS") & "';</script>")
            End If

            Me.ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.Colaboracion
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "TextosPantalla") Then
                Dim sVariableJavascriptTextos As String = "var Textos = new Array();"
                Dim sVariableJavascriptTextosCKEditor As String = "var TextosCKEditor = new Array();"
                Dim sVariableJavascriptTextosDiscrepancias As String = "var TextosDiscrepancias = new Array();"
                For i As Integer = 1 To 73
                    sVariableJavascriptTextos &= "Textos[" & i & "]='" & JSText(Textos(i)) & "';"
                    If i = 20 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[1]='" & JSText(Textos(i)) & "';"
                    If i = 21 Then sVariableJavascriptTextosCKEditor &= "TextosCKEditor[2]='" & JSText(Textos(i)) & "';"
                Next

                For i As Integer = 83 To 85
                    sVariableJavascriptTextos &= "Textos[" & i - 9 & "]='" & JSText(Textos(i)) & "';"
                Next
                For i As Integer = 74 To 89
                    sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 71 & "]='" & Textos(i) & "';"
                Next
                For i As Integer = 90 To 121
                    sVariableJavascriptTextos &= "Textos[" & i - 13 & "]='" & JSText(Textos(i)) & "';"
                Next
                For i As Integer = 142 To 143
                    sVariableJavascriptTextos &= "Textos[" & i - 33 & "]='" & JSText(Textos(i)) & "';"
                Next
                For i As Integer = 144 To 147
                    sVariableJavascriptTextosCKEditor &= "TextosCKEditor[" & i - 125 & "]='" & Textos(i) & "';"
                Next
                For i As Integer = 148 To 150
                    sVariableJavascriptTextos &= "Textos[" & i - 37 & "]='" & JSText(Textos(i)) & "';"
                Next
                ModuloIdioma = TiposDeDatos.ModulosIdiomas.VisorFacturas
                sVariableJavascriptTextosDiscrepancias &= "TextosDiscrepancias[1]='" & JSText(Textos(8)) & "';"
                sVariableJavascriptTextosDiscrepancias &= "TextosDiscrepancias[2]='" & JSText(Textos(11)) & "';"
                sVariableJavascriptTextosDiscrepancias &= "TextosDiscrepancias[3]='" & JSText(Textos(14)) & "';"
                sVariableJavascriptTextosDiscrepancias &= "TextosDiscrepancias[4]='" & JSText(Textos(34)) & "';"
                For i As Integer = 55 To 69
                    sVariableJavascriptTextosDiscrepancias &= "TextosDiscrepancias[" & i - 50 & "]='" & JSText(Textos(i)) & "';"
                Next
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TextosPantalla", sVariableJavascriptTextos & sVariableJavascriptTextosCKEditor & sVariableJavascriptTextosDiscrepancias, True)
            End If

            'Recogemos la "fecha desde" configurada en el web.config
            Dim iMesesDesde As Byte = DBNullToInteger(System.Configuration.ConfigurationManager.AppSettings("MesesVisorFacturas"))
            dFechaDesdeConfigurada = DateAdd(DateInterval.Month, CDbl("-" & iMesesDesde.ToString), Now.Date)

            If Not IsPostBack Then
                'Establecemos en el campo desde de Fecha Factura de la busqueda avanzada la fecha configurada
                Busqueda.FechaFacturaDesde = dFechaDesdeConfigurada
                Busqueda.AccesoFSSM = Acceso.gbAccesoFSSM
                Busqueda.OblCodPedDir = (Acceso.gbOblCodPedDir OrElse Acceso.gbOblCodPedido)

                ObtenerDatosIntegracion()

                BorrarCacheFacturas()

                FSNPageHeader.TituloCabecera = Textos(0)
                FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "images/Factura.png"

                Busqueda.Textos = TextosModuloCompleto

                CargarTextos()

                Busqueda.EstadosFacturas = DevolverEstadosFacturas()

                Busqueda.PartidasDataTable = DevolverPartidas()

                CreateColumns()
            Else
                Select Case Request("__EVENTTARGET")
                    Case "ExportarExcel"
                        Exportar(1)
                    Case "ExportarPDF"
                        Exportar(2)
                End Select
            End If

            InicializacionControles()
            WriteScripts()

            CargarGridFacturas(False)

            '-------------------------------------------------------
            'MENSAJE DE AVISO DE FACTURAS PENDIENTES EN LA CABECERA
            '-------------------------------------------------------
            If NumFacturasPendientes > 0 Then
                If NumFacturasPendientes <> 1 Then
                    lblFacturasPendientes.Text = Replace(Textos(35), "###", NumFacturasPendientes)
                Else
                    lblFacturasPendientes.Text = Replace(Textos(70), "###", NumFacturasPendientes)
                End If
                phAlerta.Visible = True
            End If

            If Not (ScriptMgr.IsInAsyncPostBack AndAlso ScriptMgr.AsyncPostBackSourceElementID.Contains("whdgFacturas") AndAlso whdgFacturas.GridView.Behaviors.Filtering.Filtered) Then
                'El InicializarPaginador se lanza al filtrar los datos. Si lo lanzamos aquí tb deja de funcionar el recuento de resultados que se hace dentro de la función.
                'Por eso excluimos el caso concreto en que se filtra
                InicializarPaginador()
            End If

            Dim Usuario As Fullstep.PMPortalServer.User = Session("FS_Portal_User")
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "DateFormatUsu") Then
                With Usuario
                    Dim sVariablesJavascript As String = "var UsuMask = '" & .DateFormat.ShortDatePattern & "';"
                    sVariablesJavascript = sVariablesJavascript & "var UsuMaskSeparator='" & .DateFormat.DateSeparator & "';"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "DateFormatUsu", sVariablesJavascript, True)
                End With
            End If
            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)
            End If
        End Sub
        ''' <summary>
        ''' Establece las opciones de idioma/decimales/fechas/etc del usuario
        ''' </summary>
        ''' <remarks>Llamada desde Init. Máx. 0,1 seg.</remarks>
        Private Sub setCultureInfoUsuario()
            Me.ScriptMgr.EnableScriptGlobalization = True
            Me.ScriptMgr.EnableScriptLocalization = True
        End Sub

        ''' <summary>
        ''' Dar el formato del usuario a las columnas numéricas del grid
        ''' </summary>
        ''' <param name="field">Columna seleccionada</param>
        ''' <remarks>Llamada desde setCultureInfoUsuario. 0,1 seg.</remarks>
        Private Sub setNumberFormat(ByVal field As Infragistics.Web.UI.GridControls.BoundDataField)
            field.DataFormatString = "{0:n" & FSPMUser.NumberFormat.NumberDecimalDigits & "}"
        End Sub


        ''' <summary>
        ''' Dar el formato del usuario a las columnas de fecha del grid
        ''' </summary>
        ''' <param name="field">columna seleccionada</param>
        ''' <remarks>Llamada desde setCultureInfoUsuario. 0,1 seg.</remarks>
        Private Sub setDateFormat(ByVal field As Infragistics.Web.UI.GridControls.BoundDataField)
            field.DataFormatString = "{0:" & FSPMUser.DateFormat.ShortDatePattern.ToString & "}"
        End Sub


        ''' <summary>
        ''' Método que borra de la caché los datos de facturas
        ''' </summary>
        ''' <remarks>Llamada desde los botones btnBuscarGeneral y btnBusquedaAvanzada. Máx. 0,2 seg</remarks>
        Private Sub BorrarCacheFacturas()
            If HttpContext.Current.Cache("dsFacturas_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString) IsNot Nothing Then _
                HttpContext.Current.Cache.Remove("dsFacturas_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString)
        End Sub

        ''' <summary>
        ''' Procedimiento al que se le indica si se oculta o no los label y la imagen en la que se indica por que "fecha desde" se muestra los registros en el grid
        ''' </summary>
        ''' <param name="Ocultar">true si se muestra y false si se oculta</param>
        ''' <remarks></remarks>
        Private Sub MostrarLabelsFechaDesdePaginador(ByVal Mostrar As Boolean)

            DirectCast(Me.whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("divFechaDesde"), HtmlGenericControl).Visible = Mostrar
        End Sub

        ''' <summary>
        ''' Funcion que obtiene el Dataset con los datos de facturas
        ''' </summary>
        ''' <returns>dataset con las facturas</returns>
        ''' <remarks></remarks>
        Private Function ObtenerDataSetFacturas() As DataSet
            Dim Empresa As String
            Dim NumFactura As String
            Dim FecFacturaDesde As Date
            Dim FecFacturaHasta As Date
            Dim FecContaDesde As Date
            Dim FecContaHasta As Date
            Dim ImporteDesde As Double
            Dim ImporteHasta As Double
            Dim CentroCoste As String
            Dim Partidas As DataTable
            Dim Estado As String
            Dim AñoPedido As String
            Dim NumCesta As Long
            Dim NumOrden As Long
            Dim Articulo As String
            Dim Albaran As String
            Dim PedidoERP As String
            Dim NumFacturaSAP As String
            Dim Gestor As String
            Dim FacturaOriginal As Boolean
            Dim FacturaRectificativa As Boolean


            Empresa = hidEmpresa.Value
            NumFactura = txtNumeroFactura.Text
            FecFacturaDesde = Busqueda.FechaFacturaDesde

            'Actualizo el label que hay en el paginador del grid de facturas con la fecha
            DirectCast(Me.whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblFechaDesde"), Label).Text = Textos(48) & " " & FecFacturaDesde.ToShortDateString & " "
            If FecFacturaDesde = Nothing Then
                MostrarLabelsFechaDesdePaginador(False)
            Else
                MostrarLabelsFechaDesdePaginador(True)
            End If

            FecFacturaHasta = Busqueda.FechaFacturaHasta
            FecContaDesde = Busqueda.FechaContabilizacionDesde
            FecContaHasta = Busqueda.FechaContabilizacionHasta
            ImporteDesde = Busqueda.ImporteDesde
            ImporteHasta = Busqueda.ImporteHasta
            CentroCoste = Busqueda.CentroCosteHidden
            Partidas = Busqueda.Partidas
            Gestor = Busqueda.GestorHidden
            Estado = Busqueda.Estado
            AñoPedido = Busqueda.AnyoPedido
            NumCesta = Busqueda.NumCesta
            NumOrden = Busqueda.NumPedido
            Articulo = Busqueda.ArticuloHidden
            Albaran = Busqueda.NumeroAlbaran
            PedidoERP = Busqueda.PedidoERP
            NumFacturaSAP = Busqueda.NumeroFacturaSAP
            FacturaOriginal = Busqueda.FacturaOriginal
            FacturaRectificativa = Busqueda.FacturaRectificativa

            Dim f As Fullstep.PMPortalServer.Facturas = oFsServer.Get_Object(GetType(Fullstep.PMPortalServer.Facturas))
            Dim dsFacturas As DataSet = f.Visor_Facturas(lCiaComp, Me.FSPMUser.Idioma, Me.FSPMUser.CodProve, Me.FSPMUser.IdUsu, Empresa, NumFactura, FecFacturaDesde, FecFacturaHasta, FecContaDesde, FecContaHasta, ImporteDesde, ImporteHasta, CentroCoste, Partidas, Estado, AñoPedido, NumCesta, NumOrden, Articulo, Albaran, PedidoERP, NumFacturaSAP, Gestor, FacturaOriginal, FacturaRectificativa)

            NumFacturasPendientes = f.NumeroFacturasPendientes

            If Not dsFacturas Is Nothing Then Me.InsertarEnCache("dsFacturas_" & FSPMUser.Cod & "_" & FSPMUser.Idioma.ToString, dsFacturas, Caching.CacheItemPriority.BelowNormal)

            Return dsFacturas
        End Function

        ''' <summary>
        ''' Metodo que enlaza los datos de facturas con el grid
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub CargarGridFacturas(ByVal paraExportacion As Boolean, Optional ByVal dsFacturas As DataSet = Nothing)

            If dsFacturas Is Nothing Then
                whdgFacturas.DataSource = DatasetFacturas
            Else
                whdgFacturas.DataSource = dsFacturas
            End If
            whdgFacturas.DataMember = "FACTURAS"
            whdgFacturas.DataKeyFields = "ID_FACTURA"
            whdgFacturas.DataBind()

            If Not paraExportacion Then
                whdgFacturas.GridView.Behaviors.Paging.Enabled = True
            Else
                whdgFacturas.GridView.Behaviors.Paging.Enabled = False
            End If
        End Sub
        ''' <summary>
        ''' Inicializa los componentes y funciones javascript de los elementos
        ''' </summary>
        ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
        Private Sub InicializacionControles()
            imgEmpresaLupa.Attributes.Add("onClick", "javascript:window.open('../_common/BuscadorEmpresas.aspx','_blank','width=800,height=600, location=no,menubar=no,toolbar=no,resizable=no,addressbar=no,scrollbars =no');return false;")
            txtEmpresa.Attributes.Add("onkeydown", "javascript:return eliminarEmpresa(event)")

            Busqueda.RutaBuscadorCentroCoste = "../_common/BuscadorCentrosCoste.aspx"
            Busqueda.RutaBuscadorArticulo = "../_common/articulosserver.aspx"
            Busqueda.RutaBuscadorPartidas = "../_common/BuscadorPartidas.aspx"
            Busqueda.RutaBuscadorGestor = "../_common/BuscadorUsuarios.aspx"
            Busqueda.RutaWsAutoComplete = System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "/Consultas.asmx"

            imgAlerta.ImageUrl = ConfigurationManager.AppSettings("rutanormal") & "images/Img_Ico_Alerta.gif"
            imgCerrarPagos.ImageUrl = ConfigurationManager.AppSettings("rutanormal") & "images/Bt_Cerrar.png"
            imgPagos.ImageUrl = ConfigurationManager.AppSettings("rutanormal") & "images/Pedido.png"
            Me.imgPagos.ImageUrl = ConfigurationManager.AppSettings("rutanormal") & "App_Themes/" & Me.Page.Theme & "/images/Pedido.png"
            Me.imgCerrarPagos.ImageUrl = ConfigurationManager.AppSettings("rutanormal") & "App_Themes/" & Me.Page.Theme & "/images/Bt_Cerrar.png"
            DirectCast(Me.whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("imgCalendarioFecDesde"), WebControls.Image).ImageUrl = ConfigurationManager.AppSettings("rutanormal") & "App_Themes/" & Me.Page.Theme & "/images/Calendario.png"
            DirectCast(Me.whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("divExcel"), HtmlGenericControl).Style("background-image") = ConfigurationManager.AppSettings("rutanormal") & "App_Themes/" & Me.Page.Theme & "/images/excel.png"
            DirectCast(Me.whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("divPDF"), HtmlGenericControl).Style("background-image") = ConfigurationManager.AppSettings("rutanormal") & "App_Themes/" & Me.Page.Theme & "/images/PDF.png"

            'setNumberFormat(Me.whdgFacturas.Columns.FromKey("IMPORTE"))

            setDateFormat(Me.whdgFacturas.Columns.FromKey("FECHA"))
            setDateFormat(Me.whdgFacturas.Columns.FromKey("FEC_CONTA"))
        End Sub

        ''' <summary>
        ''' Inicializa javascript de los elementos
        ''' </summary>
        ''' <remarks>Llamada desde:page_load; Tiempo máximo:0seg.</remarks>
        Private Sub WriteScripts()

            'Funcion que sirve para eliminar el proveedor
            If Not Page.ClientScript.IsClientScriptBlockRegistered("eliminarEmpresa") Then
                Dim sScript As String = "function eliminarEmpresa(event) {" & vbCrLf & _
                " if ((event.keyCode >= 35) && (event.keyCode <= 40)) { " & vbCrLf & _
                        "  " & vbCrLf & _
                " } else { " & vbCrLf & _
                    " if ((event.keyCode == 8) || (event.keyCode == 46)) { " & vbCrLf & _
                        " campoEmpresa = document.getElementById('" & txtEmpresa.ClientID & "')" & vbCrLf & _
                        " if (campoEmpresa) { campoEmpresa.value = ''; } " & vbCrLf & _
                        " campoHiddenEmpresa = document.getElementById('" & hidEmpresa.ClientID & "')" & vbCrLf & _
                        " if (campoHiddenEmpresa) { campoHiddenEmpresa.value = ''; } " & vbCrLf & _
                " } " & vbCrLf & _
                "  " & vbCrLf & _
                " } }" & vbCrLf
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "eliminarEmpresa", sScript, True)
            End If

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "rutaPortal", "<script>var rutaPortal = '" & ConfigurationManager.AppSettings("rutanormal") & "' </script>")

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varTema", "<script>var sTema = '" & Page.Theme & "' </script>")

            If Not Page.ClientScript.IsClientScriptBlockRegistered("varFSNPanelMotivoAnulacion_AnimationClientID") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFSNPanelMotivoAnulacion_AnimationClientID", "<script>var sFSNPanelMotivoAnulacion_AnimationClientID = '" & FSNPanelMotivoAnulacion.AnimationClientID & "' </script>")

            If Not Page.ClientScript.IsClientScriptBlockRegistered("varFSNPanelMotivoAnulacion_DynamicPopulateClientID") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFSNPanelMotivoAnulacion_DynamicPopulateClientID", "<script>var sFSNPanelMotivoAnulacion_DynamicPopulateClientID = '" & FSNPanelMotivoAnulacion.DynamicPopulateClientID & "' </script>")

            If Not Page.ClientScript.IsClientScriptBlockRegistered("varFSNPanelEnProceso_AnimationClientID") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFSNPanelEnProceso_AnimationClientID", "<script>var sFSNPanelEnProceso_AnimationClientID = '" & FSNPanelEnProceso.AnimationClientID & "' </script>")

            If Not Page.ClientScript.IsClientScriptBlockRegistered("varFSNPanelEnProceso_DynamicPopulateClientID") Then _
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "varFSNPanelEnProceso_DynamicPopulateClientID", "<script>var sFSNPanelEnProceso_DynamicPopulateClientID = '" & FSNPanelEnProceso.DynamicPopulateClientID & "' </script>")
        End Sub

        ''' <summary>
        ''' Procedimiento que carga los textos de la pantalla
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub CargarTextos()
            lblEmpresa.Text = String.Format("{0}:", Textos(16))
            lblNumFactura.Text = String.Format("{0}:", Textos(2))

            btnBuscarIdentificador.Text = Textos(4)
            btnBuscarBusquedaAvanzada.Text = Textos(4)
            btnLimpiarBusquedaAvanzada.Text = Textos(18)
            lblBusquedaAvanzada.Text = Textos(5)
            lblLitTituloPagos.Text = Textos(26)

            DirectCast(Me.whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("lblExceptoFechaDesde"), Label).Text = Textos(50)
            DirectCast(Me.whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("litExcel"), HtmlGenericControl).InnerText = Textos(24)
            DirectCast(Me.whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("litPDF"), HtmlGenericControl).InnerText = Textos(25)
        End Sub

        ''' <summary>
        ''' Obtendremos si hay integracion o no y se establecera en variables de sesion
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub ObtenerDatosIntegracion()
            Dim oIntegracion As Fullstep.PMPortalServer.Integracion
            oIntegracion = oFsServer.Get_Object(GetType(Fullstep.PMPortalServer.Integracion))

            Session("HayIntegracionFacturas") = False
            If oIntegracion.HayIntegracion(lCiaComp, TablasIntegracion.Facturas) Then
                Session("HayIntegracionFacturas") = True
            End If

            Session("HayIntegracionPagos") = False
            If oIntegracion.HayIntegracion(lCiaComp, TablasIntegracion.Pagos, SentidoIntegracion.Entrada) Then
                Session("HayIntegracionPagos") = True
            End If
        End Sub

        ''' <summary>
        ''' Devuelve las distintas partidas de la instalación
        ''' </summary>
        ''' <remarks></remarks>
        Private Function DevolverPartidas() As DataTable
            Dim oPress5 As Fullstep.PMPortalServer.PRES5
            oPress5 = oFsServer.Get_Press5

            Return oPress5.Partidas_LoadPartidasInstalacion(lCiaComp, Me.Idioma)
        End Function

        ''' <summary>
        ''' Devuelve los distintos estados en los que se puede encontrar una factura
        ''' </summary>
        ''' <remarks></remarks>
        Private Function DevolverEstadosFacturas() As DataTable
            Dim oFacturas As Fullstep.PMPortalServer.Facturas
            oFacturas = oFsServer.Get_Object(GetType(Fullstep.PMPortalServer.Facturas))

            Return oFacturas.LoadEstados(lCiaComp, Me.Idioma)
        End Function

        ''' <summary>
        ''' Método que se lanza al ordenar por una columna en el grid de Facturas. Actualiza el paginador y el grid para mostrar los cambios.
        ''' </summary>
        ''' <param name="sender">El control que lanza el evento</param>
        ''' <param name="e">argumentos del evento</param>
        ''' <remarks>Llamada desde el evento de filtrado. Máx 1 seg.</remarks>
        Private Sub whdgFacturas_ColumnSorted(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.SortingEventArgs) Handles whdgFacturas.ColumnSorted
            InicializarPaginador()
            upFacturas.Update()
        End Sub
        ''' <summary>
        ''' Método que se lanza al aplicar filtros en el grid de facturas. Actualiza el paginador y el grid para mostrar los cambios.
        ''' </summary>
        ''' <param name="sender">El control que lanza el evento</param>
        ''' <param name="e">argumentos del evento</param>
        ''' <remarks>Llamada desde el evento de filtrado. Máx 1 seg.</remarks>
        Private Sub whdgFacturas_DataFiltered(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.FilteredEventArgs) Handles whdgFacturas.DataFiltered
            InicializarPaginador()
            upFacturas.Update()

            CType(whdgFacturas.GridView.Behaviors.Paging.PagerTemplateContainerTop.FindControl("wdgPaginador").FindControl("upPaginador"), UpdatePanel).Update()

        End Sub

        ''' <summary>
        ''' Metodo que inicializa cada fila del grid
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub whdgFacturas_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.RowEventArgs) Handles whdgFacturas.InitializeRow
            Dim UrlImage As String
            Dim sNombreCss As String = "itemSeleccionable"
            Dim sNombreCssEstadoFacturas As String = "itemSeleccionable" 'Para no perder los estilos si le tengo que poner en rojo
            Dim oData As DataRow = e.Row.DataItem.Item.Row

            If e.Row.Items.FindItemByKey("ICONO").Value = 0 Then
                e.Row.Items.FindItemByKey("ICONO").Text = ""
            Else
                e.Row.Items.FindItemByKey("ICONO").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "App_themes/" & Page.Theme & "/images/Img_Ico_Alerta_Peq.gif" & "' style='text-align:center;'/>"
            End If

            If Session("HayIntegracionPagos") Then
                If e.Row.Items.FindItemByKey("PAGOS").Value = 0 Then
                    e.Row.Items.FindItemByKey("PAGOS").Text = ""
                Else
                    e.Row.Items.FindItemByKey("PAGOS").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "App_themes/" & Page.Theme & "/images/aprobado.gif" & "' style='text-align:center;'/>"
                    e.Row.Items.FindItemByKey("PAGOS").CssClass = "itemSeleccionable"
                End If
            End If

            If e.Row.Items.FindItemByKey("SEG").Value = "1" Then
                sNombreCss = "ManoRed"
                e.Row.Items.FindItemByKey("SEG").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "App_themes/" & Page.Theme & "/images/seg_selec.gif" & "' style='text-align:center;' />"
            Else
                e.Row.Items.FindItemByKey("SEG").Text = "<img src='" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "App_themes/" & Page.Theme & "/images/seg_noselec.gif" & "' style='text-align:center;' />"
            End If

            If e.Row.Items.FindItemByKey("TIPO").Value = "1" Then
                e.Row.Items.FindItemByKey("TIPO").Text = Textos(36)
            Else
                e.Row.Items.FindItemByKey("TIPO").Text = Textos(37)
            End If

            If e.Row.Items.FindItemByKey("EN_PROCESO").Value = "1" Then
                e.Row.Items.FindItemByKey("SITUACION_ACTUAL").Text = Textos(54)
                e.Row.Items.FindItemByKey("DEN_ESTADO").Text = Textos(54)
            Else
                If e.Row.Items.FindItemByKey("ETAPA_PETICIONARIO").Value = "1" Then
                    e.Row.Items.FindItemByKey("SITUACION_ACTUAL").Text = "<a style='color:red;' onclick=javascript:AbrirDetalle(" & e.Row.Items.FindItemByKey("INSTANCIA").Value & ")>" & UCase(Textos(49)) & "</a>"
                Else
                    e.Row.Items.FindItemByKey("SITUACION_ACTUAL").Text = "<a onclick=javascript:AbrirDetalle(" & e.Row.Items.FindItemByKey("INSTANCIA").Value & ")>" & e.Row.Items.FindItemByKey("SITUACION_ACTUAL").Value & "</a>"
                End If

                If e.Row.Items.FindItemByKey("ANULADO").Value = "1" Then
                    UrlImage = System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "App_themes/" & Page.Theme & "/images/Ico_Expirado.gif"
                    e.Row.Items.FindItemByKey("DEN_ESTADO").Text = "<img src='" & UrlImage & "' style='text-align:center;vertical-align:middle;'/> <u>" & e.Row.Items.FindItemByKey("DEN_ESTADO").Value & "</u>"
                    e.Row.Items.FindItemByKey("DEN_ESTADO").CssClass = sNombreCss
                Else
                    If e.Row.Items.FindItemByKey("ESTADO").Value < 100 Then
                        UrlImage = System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "App_themes/" & Page.Theme & "/images/Ico_Pendiente.gif"
                    Else
                        UrlImage = System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "App_themes/" & Page.Theme & "/images/Ico_Vigente.gif"
                    End If

                    If e.Row.Items.FindItemByKey("ETAPA_PETICIONARIO").Value = "1" Then
                        sNombreCssEstadoFacturas = "ManoRed"
                        e.Row.Items.FindItemByKey("DEN_ESTADO").Text = "<img src='" & UrlImage & "' style='text-align:center;vertical-align:middle;'/> " & Textos(49)
                    Else
                        e.Row.Items.FindItemByKey("DEN_ESTADO").Text = "<img src='" & UrlImage & "' style='text-align:center;vertical-align:middle;'/> " & e.Row.Items.FindItemByKey("DEN_ESTADO").Value
                    End If
                End If
            End If

            e.Row.Items.FindItemByKey("IMPORTE").Text = FormatNumber(e.Row.Items.FindItemByKey("IMPORTE").Value, FSPMUser.NumberFormat) & " " & e.Row.Items.FindItemByKey("MON").Value

            e.Row.Items.FindItemByKey("NUM_FACTURA").CssClass = sNombreCss
            e.Row.Items.FindItemByKey("EMPRESA").CssClass = sNombreCss
            e.Row.Items.FindItemByKey("FECHA").CssClass = sNombreCss
            e.Row.Items.FindItemByKey("FEC_CONTA").CssClass = sNombreCss
            If Session("HayIntegracionFacturas") Then
                e.Row.Items.FindItemByKey("NUM_ERP").CssClass = sNombreCss
            End If
            e.Row.Items.FindItemByKey("IMPORTE").CssClass = sNombreCss & " itemRightAlignment"
            e.Row.Items.FindItemByKey("DEN_ESTADO").CssClass = sNombreCssEstadoFacturas 'sNombreCss
            e.Row.Items.FindItemByKey("SITUACION_ACTUAL").CssClass = sNombreCss
            e.Row.Items.FindItemByKey("TIPO").CssClass = sNombreCss
            e.Row.Items.FindItemByKey("SEG").CssClass = sNombreCss

            If Not CType(e.Row.DataItem.Item.Row.Item("TIENE_DISCREP"), Boolean) Then
                e.Row.Items.FindItemByKey("TIENE_DISCREP").Text = ""
            Else
                If CType(e.Row.DataItem.Item.Row.Item("TIENE_DISCREP_ABTAS"), Boolean) Then
                    e.Row.Items.FindItemByKey("TIENE_DISCREP").Text = "<img id='imgDiscrepancias_" & CType(e.Row.DataItem.Item.Row.Item("ID_FACTURA"), Integer) & "' src='" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "/images/Discrepancia.png" & "' class='Link' style='text-align:center;'/>"
                    e.Row.Items.FindItemByKey("TIENE_DISCREP").Text &= "<img id='imgDiscrepanciasCerradas_" & CType(e.Row.DataItem.Item.Row.Item("ID_FACTURA"), Integer) & "' src='" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "/images/Discrepancias_cerradas.png" & "' class='Link' style='text-align:center; display:none;'/>"
                Else
                    e.Row.Items.FindItemByKey("TIENE_DISCREP").Text = "<img id='imgDiscrepancias_" & CType(e.Row.DataItem.Item.Row.Item("ID_FACTURA"), Integer) & "' src='" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "/images/Discrepancia.png" & "' class='Link' style='text-align:center; display:none;'/>"
                    e.Row.Items.FindItemByKey("TIENE_DISCREP").Text &= "<img id='imgDiscrepanciasCerradas_" & CType(e.Row.DataItem.Item.Row.Item("ID_FACTURA"), Integer) & "' src='" & System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "/images/Discrepancias_cerradas.png" & "' class='Link' style='text-align:center;'/>"
                End If
            End If
        End Sub

        ''' <summary>
        ''' Metodo que crea las columnas del grid de facturas
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub CreateColumns()
            AddColumn("ICONO", "", 3)

            AddColumn("ID_FACTURA", "", 0, False)
            AddColumn("NUM_FACTURA", Textos(30), 12)
            AddColumn("EMPRESA", Textos(16), 15)
            AddColumn("FECHA", Textos(32), 8)
            AddColumn("FEC_CONTA", Textos(33), 8)

            If Session("HayIntegracionFacturas") Then
                AddColumn("NUM_ERP", Textos(14), 8)
            End If

            AddColumn("IMPORTE", Textos(34), 8)
            AddColumn("DEN_ESTADO", Textos(38), 12)
            AddColumn("SITUACION_ACTUAL", Textos(39), 10)
            AddColumn("TIPO", "Tipo", 5)

            If Session("HayIntegracionPagos") Then
                AddColumn("PAGOS", Textos(41), 5)
            End If
            AddColumn("SEG", "", 3)

            AddColumn("ESTADO", "", 0, False)
            AddColumn("ANULADO", "", 0, False)
            AddColumn("INSTANCIA", "", 0, False)
            AddColumn("TRASLADADA", "", 0, False)
            AddColumn("EN_PROCESO", "", 0, False)
            AddColumn("ETAPA_PETICIONARIO", "", 0, False) 'Indica si esta en la etapa peticionario, sera etapa BORRADOR
            AddColumn("MON", "", 0, False)

            AddColumn("TIENE_DISCREP", "", 0, FSPMUser.AccesoCN) 'Tiene discrepancias
        End Sub

        ''' <summary>
        ''' Metodo que crea cada columna del grid de facturas
        ''' </summary>
        ''' <param name="fieldName"></param>
        ''' <param name="headerText"></param>
        ''' <param name="Visible"></param>
        ''' <remarks></remarks>
        Private Sub AddColumn(ByVal fieldName As String, ByVal headerText As String, ByVal Width As Integer, Optional ByVal Visible As Boolean = True)
            Dim field As New BoundDataField(True)
            field.Key = fieldName
            field.DataFieldName = fieldName
            field.Header.Text = headerText
            field.Hidden = Not Visible
            field.Width = Unit.Percentage(Width)
            Me.whdgFacturas.Columns.Add(field)
        End Sub

        ''' <summary>
        ''' Configura la grid para su posterior exportación. Exporta la Grid a una hoja excel o a Pdf
        ''' </summary>
        ''' <param name="iTipoExportacion"> Tipo Exportacion (Excel = 1 // Pdf = 2)</param>
        ''' <remarks>Llamada desde:Page_load; Tiempo ejecucion:0,3seg.</remarks>
        Private Sub Exportar(ByVal iTipoExportacion As Byte)
            CargarGridFacturas(True)

            With whdgFacturas
                .Columns.FromKey("ICONO").Hidden = True
                .Columns.FromKey("SEG").Hidden = True
                .Columns.FromKey("TIENE_DISCREP").Hidden = True
                .GridView.Columns.FromKey("ICONO").Hidden = True
                .GridView.Columns.FromKey("SEG").Hidden = True
                .GridView.Columns.FromKey("TIENE_DISCREP").Hidden = True
            End With

            Dim fileName As String = HttpUtility.UrlEncode(FSNPageHeader.TituloCabecera)
            fileName = Date.Now.ToShortDateString & "_" & fileName.Replace("+", "_")

            If iTipoExportacion = 1 Then 'Excel
                whdgExcelExporter.DownloadName = fileName & ".xls"
                whdgExcelExporter.Export(Me.whdgFacturas.GridView)

            Else 'PDF
                With whdgPDFExporter
                    .TargetPaperOrientation = Infragistics.Documents.Reports.Report.PageOrientation.Landscape
                    .DownloadName = fileName & ".pdf"
                    .Format = Infragistics.Documents.Reports.Report.FileFormat.PDF
                    .EnableStylesExport = True
                    .DataExportMode = DataExportMode.AllDataInDataSource
                    .CustomFont = New System.Drawing.Font("Arial, Verdana", 4, System.Drawing.GraphicsUnit.Pixel)
                    .Margins = Infragistics.Documents.Reports.Report.PageMargins.GetPageMargins("Narrow")
                    .TargetPaperSize = Infragistics.Documents.Reports.Report.PageSizes.GetPageSize("A4")
                    .Export(Me.whdgFacturas)
                End With
            End If
        End Sub

        <System.Web.Services.WebMethodAttribute(), _
       System.Web.Script.Services.ScriptMethodAttribute()> _
        Public Shared Function DevolverTexto(ByVal contextKey As String) As String
            Return contextKey
        End Function

        Private Sub btnBuscarBusquedaAvanzada_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarBusquedaAvanzada.Click
            Dim dsFacturas As DataSet

            dsFacturas = ObtenerDataSetFacturas()
            CargarGridFacturas(False, dsFacturas)
            InicializarPaginador()
            upFacturas.Update()
        End Sub
        ''' <summary>
        ''' Limpia los campos de la busqueda avanzada
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub btnLimpiarBusquedaAvanzada_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarBusquedaAvanzada.Click
            'Recorremos los controles del panel y los vamos limpiando según el tipo
            VaciarControlesRecursivo(pnlParametros)
            Busqueda.Estado = 0 'Incidencia 34 de que al limpiar se queda el estado elegido
            upBusquedaAvanzada.Update()
        End Sub

        Private Sub btnBuscarIdentificador_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarIdentificador.Click
            Dim dsFacturas As DataSet

            dsFacturas = ObtenerDataSetFacturas()
            CargarGridFacturas(False, dsFacturas)
            InicializarPaginador()
            upFacturas.Update()
        End Sub



        ''' <summary>
        ''' Método que recorre de manera recursiva el control recibido y vacía las selecciones de todos los controles
        ''' </summary>
        ''' <param name="oControlBase">Control a recorrer</param>
        ''' <remarks>Llamada desde btnLimpiar_Click</remarks>
        Private Sub VaciarControlesRecursivo(ByVal oControlBase As Object)
            For Each oControl In oControlBase.Controls
                Select Case oControl.GetType().ToString
                    Case GetType(DropDownList).ToString
                        If CType(oControl, DropDownList).SelectedItem IsNot Nothing Then _
                        CType(oControl, DropDownList).SelectedItem.Selected = False
                    Case GetType(TextBox).ToString
                        CType(oControl, TextBox).Text = String.Empty
                    Case GetType(Infragistics.Web.UI.EditorControls.WebDatePicker).ToString
                        CType(oControl, Infragistics.Web.UI.EditorControls.WebDatePicker).Value = Nothing
                    Case GetType(Infragistics.Web.UI.EditorControls.WebNumericEditor).ToString
                        CType(oControl, Infragistics.Web.UI.EditorControls.WebNumericEditor).Value = Nothing
                    Case GetType(System.Web.UI.WebControls.HiddenField).ToString
                        'No tiene que borrar los hidden donde se indica el codigo de cada raiz de partida presupuestaria
                        If Not CType(oControl, System.Web.UI.WebControls.HiddenField).ClientID.Contains("hid_Pres5") Then
                            CType(oControl, System.Web.UI.WebControls.HiddenField).Value = String.Empty
                        End If
                    Case Else
                        If oControl.Controls IsNot Nothing AndAlso oControl.Controls.Count > 0 Then
                            VaciarControlesRecursivo(oControl)
                        End If
                End Select
            Next
        End Sub


#Region "Paginador"

        ''' <summary>
        ''' Inicializamos el paginador
        ''' </summary>
        ''' <remarks>Llamada desde load y botones de búsqueda. Máx. 0,1 seg.</remarks>
        Private Sub InicializarPaginador()
            Dim numRegistros As Integer = 0
            If whdgFacturas.GridView.Behaviors.Filtering.Filtered Then
                whdgFacturas.GridView.Behaviors.Paging.Enabled = False
                numRegistros = whdgFacturas.GridView.Rows.Count
                whdgFacturas.GridView.Behaviors.Paging.Enabled = True
            Else
                If DatasetFacturas IsNot Nothing AndAlso DatasetFacturas.Tables("FACTURAS") IsNot Nothing AndAlso DatasetFacturas.Tables("FACTURAS").Rows.Count > 0 Then _
                    numRegistros = DatasetFacturas.Tables(0).Rows.Count
            End If
            Me._PaginadorTop.SetContext(numRegistros, ConfigurationManager.AppSettings("PaginacionFacturas"))
            Me.whdgFacturas.GridView.Behaviors.Paging.PageIndex = 0
        End Sub


        ''' <summary>
        ''' Método que actualiza el contador de páginas del paginador
        ''' </summary>
        ''' <param name="sender">Control que lanza el evento PageChanged</param>
        ''' <param name="e">argumentos del evento</param>
        ''' <remarks>Llamada desde evento PageChanged. Máximo: 0,1 seg.</remarks>
        Private Sub _Paginador_PageChanged(ByVal sender As Object, ByVal e As PageSettingsChangedEventArgs)
            If (e.TotalNumberOfPages - 1) < e.PageIndex Then
                Me.whdgFacturas.GridView.Behaviors.Paging.PageIndex = 0
            Else
                Me.whdgFacturas.GridView.Behaviors.Paging.PageIndex = e.PageIndex
            End If

            'Actualizamos tb el otro paginador
            Dim oPagSender As PMPortalWeb.Paginador = TryCast(sender, PMPortalWeb.Paginador)
            If oPagSender IsNot Nothing Then
                If oPagSender.ClientID = Me._PaginadorTop.ClientID Then
                    'Me._PaginadorBottom.PageNumber = e.PageNumber
                    'Me._PaginadorBottom.ActualizarPaginador()
                Else
                    Me._PaginadorTop.PageNumber = e.PageNumber
                    Me._PaginadorTop.ActualizarPaginador()
                End If
            End If
            upFacturas.Update()
        End Sub
#End Region

        Private Sub whdgExcelExporter_GridRecordItemExported(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.GridRecordItemExportedEventArgs) Handles whdgExcelExporter.GridRecordItemExported
            e.WorksheetCell.CellFormat.Font.Bold = Infragistics.Documents.Excel.ExcelDefaultableBoolean.False
            e.WorksheetCell.CellFormat.Font.Color = Drawing.Color.Black

            Select Case e.GridCell.Column.Key
                Case "PAGOS"
                    e.WorksheetCell.Value = If(e.GridCell.Value > 0, Textos(43), Textos(44))
                Case "TIPO"
                    e.WorksheetCell.Value = If(e.GridCell.Value = 1, Textos(36), Textos(37))
                Case "DEN_ESTADO"
                    If e.GridCell.Row.DataItem.Item("EN_PROCESO") = "1" Then
                        e.WorksheetCell.Value = Textos(54)
                    Else
                        If e.GridCell.Row.DataItem.Item("ETAPA_PETICIONARIO") = "1" Then
                            e.WorksheetCell.Value = UCase(Textos(49))
                        Else
                            e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item("DEN_ESTADO")
                        End If
                    End If
                Case "SITUACION_ACTUAL"
                    If e.GridCell.Row.DataItem.Item("EN_PROCESO") = "1" Then
                        e.WorksheetCell.Value = Textos(54)
                    Else
                        If e.GridCell.Row.DataItem.Item("ETAPA_PETICIONARIO") = "1" Then
                            e.WorksheetCell.Value = UCase(Textos(49))
                        Else
                            e.WorksheetCell.Value = e.GridCell.Row.DataItem.Item("SITUACION_ACTUAL")
                        End If
                    End If
                Case "IMPORTE"
                    e.WorksheetCell.Value = FSNLibrary.FormatNumber(e.GridCell.Row.DataItem.Item("IMPORTE"), FSPMUser.NumberFormat) & " " & e.GridCell.Row.DataItem.Item("MON")
                    e.WorksheetCell.CellFormat.Alignment = Infragistics.Documents.Excel.HorizontalCellAlignment.Right
                Case Else
                    Select Case e.GridCell.Column.Type
                        Case System.Type.GetType("System.DateTime")
                            e.WorksheetCell.CellFormat.FormatString = FSPMUser.DateFormat.ShortDatePattern
                        Case Else
                    End Select
            End Select
        End Sub

        Private Sub whdgPDFExporter_GridRecordItemExporting(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.DocumentGridRecordItemExportingEventArgs) Handles whdgPDFExporter.GridRecordItemExporting
            Select Case e.GridCell.Column.Key
                Case "PAGOS"
                    e.ExportValue = If(e.GridCell.Value > 0, Textos(43), Textos(44))
                Case "SITUACION_ACTUAL", "DEN_ESTADO"
                    e.ExportValue = IIf(Not IsDBNull(e.GridCell.Value), e.GridCell.Value.ToString & " ", "")
            End Select
        End Sub

        Private Sub whdgExcelExporter_RowExported(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.ExcelRowExportedEventArgs) Handles whdgExcelExporter.RowExported
            Dim objDict As Hashtable = HttpContext.Current.Session("Hast_Seg_" & FSPMUser.Cod & "_" & FSPMUser.CodCia)

            If Not e.GridRow Is Nothing Then
                If Not objDict Is Nothing AndAlso Not objDict.Item(CShort(e.GridRow.Index)) Is Nothing Then
                    If objDict.Item(CShort(e.GridRow.Index)) = 1 Then
                        e.Worksheet.Rows(e.CurrentRowIndex).CellFormat.Font.Color = Drawing.Color.Red
                    End If
                Else
                    If e.GridRow.Items.FindItemByKey("SEG").Value = 1 Then
                        e.Worksheet.Rows(e.CurrentRowIndex).CellFormat.Font.Color = Drawing.Color.Red
                    End If
                End If

            End If
        End Sub
    End Class
End Namespace