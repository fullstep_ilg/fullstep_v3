﻿Imports Fullstep
Namespace Fullstep.PMPortalWeb

    Public Class DetalleMail
        Inherits FSPMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            ModuloIdioma = PMPortalServer.TiposDeDatos.ModulosIdiomas.EnvioMails

            FSNPageHeader.UrlImagenCabecera = System.Configuration.ConfigurationManager.AppSettings("rutanormal") & "images/cab_enviar_email.gif"
            FSNPageHeader.TituloCabecera = Textos(2)

            lblPara.Text = String.Format("{0}:", Textos(0))
            lblAsunto.Text = String.Format("{0}:", Textos(1))

            If Request.QueryString("IdMail") <> 0 Then
                Dim IdMail As Long
                Try
                    IdMail = CLng(Encrypter.Encrypt(Request.QueryString("IdMail"), , False, Encrypter.TipoDeUsuario.Administrador))
                Catch ex As Exception
                    IdMail = -1
                End Try

                If IdMail = -1 Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(),"Cierra", "<script>window.close();</script>")
                Else
                    Dim oEmail As PMPortalServer.Email
                    oEmail = FSPMServer.Get_Object(GetType(PMPortalServer.Email))
                    oEmail.Load_Registro_Email(IdCiaComp, IdMail, FSPMUser.Cod)

                    Instancia.Value = oEmail.Instancia
                    Prove.Value = oEmail.Prove
                    isHTML.Value = oEmail.IsHTML

                    txtPara.Text = oEmail.Para

                    TipoEmail.Value = oEmail.TipoEMail
                    EntidadEmail.Value = oEmail.EntidadEMail

                    txtEmailHtml.ActiveMode = AjaxControlToolkit.HTMLEditor.ActiveModeType.Preview
                    txtEmailHtml.Content = oEmail.Cuerpo
                    txtAsunto.Text = oEmail.Asunto
                End If
            End If
        End Sub

    End Class
End Namespace