﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EnviarMail.aspx.vb" Inherits="Fullstep.PMPortalWeb.EnviarMail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<script type="text/javascript">
function NoEnter(event)
{
	if (event.keyCode==13)
	{
		return false
	}
	return true
}
/*
''' <summary>
''' Abre la pantalla de seleccion de archivo
''' </summary>
''' <remarks>Llamada desde: cmdAdjuntar onclick; Tiempo máximo:0</remarks>		
*/
function Adjuntar() {
    var adjuntos = document.forms["Form1"].elements["AdjuntoExtendido"].value
    var Ruta = document.forms["Form1"].elements["txtRuta"].value
    window.open("../_common/attachfileEMail.aspx?att=" + adjuntos + "&Ruta=" + Ruta, "_blank", "width=390,height=100,status=yes,resizable=no,top=280,left=400")
}
/*
''' <summary>
''' Adjunta un fichero al mail (solo cambia la label, lo bbdd va aparte)
''' </summary>
''' <param name="fich">fichero</param>
''' <remarks>Llamada desde: attachfileEMail.aspx ; Tiempo máximo:0</remarks>		
*/
function AdjuntarAttach(fich) {
    document.forms["Form1"].elements["AdjuntoExtendido"].value = document.forms["Form1"].elements["AdjuntoExtendido"].value + fich
}

function ComprobarDatosMail() {
    if (document.getElementById("ParaExtendido").value == "") {
        alert(arrTextosML[5]);
        return false;
    } else {
        var addresses = (document.getElementById("ParaExtendido").value + ";" + document.getElementById("CCExtendido").value + ";" + document.getElementById("CCOExtendido").value).split(";");
        for (i = 0; i < addresses.length; i++) {
            if (!ValidateMailAdress(addresses[i].replace(/^\s+/g, '').replace(/\s+$/g, ''))) {
                alert(arrTextosML[6].replace("#####", addresses[i]));
                return false
            }
        }
    }
    return true
}

function ValidateMailAdress(address) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (address != "") {
        if (reg.test(address) == false) {
            return false;
        }
    }
    return true;
}
</script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <div style="clear:both; float:left; margin-top:10px; margin-left:10px;">            
            <asp:Label runat="server" ID="lbltitulo" CssClass="subtitulo">DEnvio de emails a proveedores</asp:Label>
        </div>
        <div style="float:right; margin-top:10px;">
            <asp:Button runat="server" ID="btnEnviar" CssClass="boton" />
                       
        </div>
        <div style="clear:both; float:left; margin-top:3px; margin-left:10px; width:100%;">
            <asp:Label runat="server" id="lblInfo" CssClass="parrafo" Text="...."></asp:Label>
        </div>
        <div style="clear:both; float:left; margin-top:3px; padding-left:10px; line-height:20px; width:50px;">
            <asp:Label runat="server" ID="lblPara" CssClass="parrafo" style="width: 70px; height: 20px">DPara:</asp:Label>
        </div>
        <div style="float:left; margin-top:3px; padding-left:10px; line-height:20px;">
            <textarea runat="server" id="ParaExtendido" name="ParaExtendido" onkeypress="return NoEnter(event)" 
                style="width: 550px; height:15px" rows="1" cols="71"></textarea>
        </div>
        <div style="clear:both; float:left; margin-top:3px; padding-left:10px; line-height:20px; width:50px;">
            <asp:Label runat="server" ID="lblCC" CssClass="parrafo" style="width: 70px; height: 20px">DPara:</asp:Label>
        </div>
        <div style="float:left; margin-top:3px; padding-left:10px; line-height:20px;">
            <textarea runat="server" id="CCExtendido" name="CCExtendido" onkeypress="return NoEnter(event)" 
                style="width: 550px; height:15px" rows="1" cols="71"></textarea>            
        </div>
        <div style="clear:both; float:left; margin-top:3px; padding-left:10px; line-height:20px; width:50px;">
            <asp:Label runat="server" ID="lblCCO" CssClass="parrafo">DPara:</asp:Label>
        </div>
        <div style="float:left; margin-top:3px; padding-left:10px; line-height:20px;">
            <textarea runat="server" id="CCOExtendido" name="CCOExtendido" onkeypress="return NoEnter(event)"
                style="width: 550px; height:15px" rows="1" cols="71"></textarea>
        </div>
        <div style="clear:both; float:left; margin-top:3px; padding-left:10px; line-height:20px; width:50px">
            <asp:Label runat="server" id="lblAsunto" CssClass="parrafo">DAsunto</asp:Label>
        </div>
        <div style="float:left; margin-top:3px; padding-left:10px;">
            <asp:TextBox runat="server" id="ugtxtAsunto" MaxLength="500" Wrap="False" Width="550px"></asp:TextBox>
        </div>
        <div style="clear:both; float:left; margin-top:3px; padding-left:10px;">
            <input runat="server" id="cmdAdjuntar" name="cmdAdjuntar" type="button" value="Adjuntar..." onclick="Adjuntar()"
                style="width: 50px; height: 20px" class="boton" />
            <textarea runat="server" id="AdjuntoExtendido" name="AdjuntoExtendido" onkeypress="return NoEnter(event)"
                style="width: 510px; height:15px" rows="1" cols="71"></textarea>
        </div>
        <div style="clear:both; float:left; margin-top:3px; padding-left:10px; width:100%;">
            <asp:TextBox runat="server" id="ugtxtTexto" TextMode="MultiLine" MaxLength="2000" Width="660px" Height="144px" Rows="4"></asp:TextBox>
            <input id="txtRuta" style="z-index: 111; left: 320px; width: 160px; position: absolute;
                top: 8px; height: 24px" type="hidden" size="21" name="txtRuta" runat="server"/>
        </div>
        </form>
</body>
</html>
