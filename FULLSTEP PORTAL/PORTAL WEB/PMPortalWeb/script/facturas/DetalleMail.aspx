﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DetalleMail.aspx.vb" Inherits="Fullstep.PMPortalWeb.DetalleMail" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>FULLSTEP Networks</title>
</head>
<body style="width: 100%; height:100%; padding: 0px; margin-top: 10px; margin-left: 0px; background:#FFFFFF;">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <table cellpadding="0" cellspacing="0" border="0" style="width:100%; height:100%;">
            <tr>
                <td>
                    <fsn:FSNPageHeader runat="server" ID="FSNPageHeader"></fsn:FSNPageHeader>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0" style="width:100%; padding-left:10px; padding-top:10px;">                        
                        <tr>
                            <td>
                                <input id="Instancia" runat="server" value="0" type="hidden" name="Instancia"/>
                                <input id="Prove" runat="server" value="0" type="hidden" name="Prove"/>
                                <input id="isHTML" runat="server" value="0" type="hidden" name="isHTML"/>
                                <input id="TipoEmail" runat="server" value="0" type="hidden" name="TipoEmail"/>
                                <input id="EntidadEmail" runat="server" value="0" type="hidden" name="EntidadEmail"/>
                                <asp:Label runat="server" ID="lblPara" CssClass="parrafo">DPara:</asp:Label>
                            </td>
                            <td style="width:100%; padding-left:10px;">
                                <asp:TextBox runat="server" ID="txtPara" style="width:700px;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:5px;">
                                <asp:Label runat="server" ID="lblAsunto" CssClass="parrafo">DAsunto:</asp:Label>
                            </td>
                            <td style="width:100%; padding-left:10px; padding-top:5px;">
                                <asp:TextBox runat="server" ID="txtAsunto" style="width:700px;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding-top:15px;">
                                <ajxEditor:Editor ID="txtEmailHtml" runat="server" 
                                    style="width:100%; height:550px;"/>
                            </td>
                        </tr>
                    </table>                
                </td>
            </tr>            
        </table>
    </form>
</body>
</html>
