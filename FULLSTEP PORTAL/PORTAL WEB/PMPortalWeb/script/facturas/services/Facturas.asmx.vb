﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Fullstep
Imports System.Web
Imports System.Text
Imports System.IO
Imports System.Globalization
Imports Irony.Parsing
Imports System.Collections.Generic

Namespace Fullstep.PMPortalWeb
    ' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
    <System.Web.Script.Services.ScriptService()> _
    <System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
    <System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
    <ToolboxItem(False)> _
    Public Class Facturas
        Inherits System.Web.Services.WebService
        <WebMethod(True)> _
        Public Sub LimpiarCostesDescuentosGenerales()
            Dim oDT As DataTable
            oDT = CType(HttpContext.Current.Session("dsXMLFactura").Tables("COSTES"), DataTable)
            oDT.Rows.Clear()
            oDT = CType(HttpContext.Current.Session("dsXMLFactura").Tables("DESCUENTOS"), DataTable)
            oDT.Rows.Clear()
        End Sub
        <WebMethod(True)> _
        Public Sub InsertarCosteGeneral(ByVal lId As List(Of Long), ByVal sDen As List(Of String), ByVal sOperacion As List(Of String), ByVal dValor As List(Of Double), ByVal dImporte As List(Of Double))
            Dim oDTCoste As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("COSTES"), DataTable)
            For i = 0 To lId.Count - 1
                Dim dtNewRow As DataRow
                dtNewRow = oDTCoste.NewRow
                dtNewRow.Item("ID") = lId(i)
                dtNewRow.Item("DEN") = sDen(i)
                dtNewRow.Item("OPERACION") = sOperacion(i)
                dtNewRow.Item("VALOR") = dValor(i)
                dtNewRow.Item("IMPORTE") = dImporte(i)
                dtNewRow.Item("PLANIFICADO") = 0
                If dtNewRow.RowState = DataRowState.Detached Then
                    oDTCoste.Rows.Add(dtNewRow)
                End If
            Next

        End Sub
        <WebMethod(True)> _
        Public Sub InsertarDescuentoGeneral(ByVal lId As List(Of Long), ByVal sDen As List(Of String), ByVal sOperacion As List(Of String), ByVal dValor As List(Of Double), ByVal dImporte As List(Of Double))
            Dim oDTCoste As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("DESCUENTOS"), DataTable)
            For i = 0 To lId.Count - 1
                Dim dtNewRow As DataRow
                dtNewRow = oDTCoste.NewRow
                dtNewRow.Item("ID") = lId(i)
                dtNewRow.Item("DEN") = sDen(i)
                dtNewRow.Item("OPERACION") = sOperacion(i)
                dtNewRow.Item("VALOR") = dValor(i)
                dtNewRow.Item("IMPORTE") = dImporte(i)
                dtNewRow.Item("PLANIFICADO") = 0
                If dtNewRow.RowState = DataRowState.Detached Then
                    oDTCoste.Rows.Add(dtNewRow)
                End If
            Next

        End Sub
        <WebMethod(True)> _
        Public Sub InsertarDatosFactura(ByVal lId As Long, ByVal dImporte As Double, ByVal sCodFormaPago As String, ByVal sCodViaPago As String, ByVal sObservaciones As String, ByVal iTipo As Integer, ByVal dTotalCostes As Double, ByVal dTotalDescuentos As Double)
            Dim oDTFactura As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable)
            Dim dtNewRow As DataRow
            Dim bNewRow As Boolean = False

            dtNewRow = oDTFactura.Rows.Find(lId)
            If dtNewRow Is Nothing Then
                bNewRow = True
                dtNewRow = oDTFactura.NewRow
            End If
            dtNewRow.Item("ID") = lId
            dtNewRow.Item("IMPORTE") = dImporte
            dtNewRow.Item("PAG") = sCodFormaPago
            dtNewRow.Item("VIA_PAG") = sCodViaPago
            dtNewRow.Item("OBS") = sObservaciones
            dtNewRow.Item("TIPO") = iTipo
            dtNewRow.Item("TOT_COSTES") = dTotalCostes
            dtNewRow.Item("TOT_DESCUENTOS") = dTotalDescuentos

            If bNewRow AndAlso dtNewRow.RowState = DataRowState.Detached Then
                oDTFactura.Rows.Add(dtNewRow)
            End If
        End Sub

        <WebMethod(True)>
        Public Sub ActualizarCostesDescuentosGenerales(ByVal lId As Long, ByVal dImporte As Double, ByVal dTotalCostes As Double, ByVal dTotalDescuentos As Double, ByVal dImporteBruto As Double)
            Dim oDTFactura As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("FACTURA"), DataTable)
            Dim dtNewRow As DataRow
            Dim bNewRow As Boolean = False

            dtNewRow = oDTFactura.Rows.Find(lId)
            If dtNewRow Is Nothing Then
                bNewRow = True
                dtNewRow = oDTFactura.NewRow
            End If
            dtNewRow.Item("ID") = lId
            dtNewRow.Item("IMPORTE") = dImporte
            If dImporteBruto > 0 Then
                dtNewRow.Item("IMPORTE_BRUTO") = dImporteBruto
            End If
            dtNewRow.Item("TOT_COSTES") = dTotalCostes
            dtNewRow.Item("TOT_DESCUENTOS") = dTotalDescuentos

            If bNewRow AndAlso dtNewRow.RowState = DataRowState.Detached Then
                oDTFactura.Rows.Add(dtNewRow)
            End If
        End Sub

        <WebMethod(True)> _
        Public Sub InsertarRol(ByVal lRol As Long, ByVal sPer As String, ByVal sProve As String, ByVal sContactos As String, ByVal iTipo As Integer)
            Dim oDTRol As DataTable = CType(HttpContext.Current.Session("dsXMLFactura").Tables("ROLES"), DataTable)
            Dim dtNewRow As DataRow
            dtNewRow = oDTRol.NewRow
            dtNewRow.Item("ROL") = lRol
            dtNewRow.Item("PER") = sPer
            dtNewRow.Item("PROVE") = sProve
            dtNewRow.Item("CON") = sContactos
            dtNewRow.Item("TIPO") = iTipo
            dtNewRow.Item("CAMPO") = Nothing
            If dtNewRow.RowState = DataRowState.Detached Then
                oDTRol.Rows.Add(dtNewRow)
            End If
        End Sub
#Region "Validaciones"
        ''' <summary>
        ''' Comprueba que todas las líneas tienen impuestos
        ''' </summary>
        ''' <remarks></remarks>
        <WebMethod(True)> _
        Public Function ComprobarImpuestosEnLineas() As Boolean
            Dim drRowLinea As DataRow

            For Each drRowLinea In CType(HttpContext.Current.Cache("dsFactura" & HttpContext.Current.Session("FS_Portal_User").IdCia & HttpContext.Current.Session("FS_Portal_User").Cod), DataSet).Tables(6).Rows
                If drRowLinea.RowState <> DataRowState.Deleted Then
                    If CType(HttpContext.Current.Cache("dsFactura" & HttpContext.Current.Session("FS_Portal_User").IdCia & HttpContext.Current.Session("FS_Portal_User").Cod), DataSet).Tables(10).Select("LINEA = " & drRowLinea("LINEA")).Length = 0 Then
                        Return False
                    End If
                End If
            Next

            Return True
        End Function

        ''' <summary>
        ''' Comprueba que se cumple la tolerancia de la empresa
        ''' </summary>
        ''' <remarks></remarks>
        <WebMethod(True)> _
        Public Function ComprobarTolerancia(ByVal ToleranciaImporte As Double, ByVal ToleranciaPorcentaje As Double, ByVal dImporteTotal As Double, ByVal dCostesGenerales As Double) As Boolean
            Dim bCumpleTolerancia As Boolean = True
            If ToleranciaImporte > 0 OrElse ToleranciaPorcentaje > 0 Then
                Dim dTotalImporteParaTolerancia As Double = 0
                Dim dTotalCosteParaTolerancia As Double = 0

                Dim dtLineas As DataTable = CType(HttpContext.Current.Cache("dsFactura" & HttpContext.Current.Session("FS_Portal_User").IdCia & HttpContext.Current.Session("FS_Portal_User").Cod), DataSet).Tables(6)
                dtLineas.DefaultView.Sort = "ALBARAN ASC"
                Dim sAlbaran As String = String.Empty
                For Each drLinea As DataRow In dtLineas.Rows
                    If sAlbaran = String.Empty Then sAlbaran = drLinea("ALBARAN")
                    If drLinea("ALBARAN") <> sAlbaran Then
                        dTotalCosteParaTolerancia = dTotalCosteParaTolerancia + (dTotalImporteParaTolerancia * dCostesGenerales) / dImporteTotal
                        If dTotalCosteParaTolerancia > 0 Then
                            If ToleranciaImporte > 0 AndAlso ToleranciaImporte < dTotalCosteParaTolerancia Then
                                bCumpleTolerancia = False
                                Exit For
                            End If
                            If bCumpleTolerancia AndAlso ToleranciaPorcentaje > 0 Then
                                If (dTotalImporteParaTolerancia * ToleranciaPorcentaje / 100) < dTotalCosteParaTolerancia Then
                                    bCumpleTolerancia = False
                                    Exit For
                                End If
                            End If
                        End If
                        sAlbaran = drLinea("ALBARAN")
                        dTotalImporteParaTolerancia = 0
                        dTotalCosteParaTolerancia = 0
                    End If
                    dTotalImporteParaTolerancia = dTotalImporteParaTolerancia + drLinea("IMPORTE")
                    dTotalCosteParaTolerancia = dTotalCosteParaTolerancia + drLinea("TOTAL_COSTES")
                Next

                If bCumpleTolerancia Then
                    dTotalCosteParaTolerancia = dTotalCosteParaTolerancia + (dTotalImporteParaTolerancia * dCostesGenerales) / dImporteTotal
                    If dTotalCosteParaTolerancia > 0 Then
                        If ToleranciaImporte > 0 AndAlso ToleranciaImporte < dTotalCosteParaTolerancia Then
                            bCumpleTolerancia = False
                        End If
                        If bCumpleTolerancia AndAlso ToleranciaPorcentaje > 0 Then
                            If (dTotalImporteParaTolerancia * ToleranciaPorcentaje / 100) < dTotalCosteParaTolerancia Then
                                bCumpleTolerancia = False
                            End If
                        End If
                    End If
                End If
            End If
            Return bCumpleTolerancia
        End Function
#End Region
#Region "Discrepancias"
        <Services.WebMethod(True)> _
        <Script.Services.ScriptMethod()> _
        Public Function Obtener_Datos_NuevaDiscrepancia_Proveedor(ByVal factura As Integer, ByVal linea As Integer) As List(Of cn_fsItem)
            Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
            Dim ocnInfo As PMPortalServer.cnDiscrepancias
            ocnInfo = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
            Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")

            Dim lCiaComp As Long = CN_Usuario.CiaComp
            Dim info As List(Of cn_fsItem)

            With CN_Usuario
                info = ocnInfo.Obtener_Datos_NuevaDiscrepancia_Proveedor(factura, linea, lCiaComp, .Idioma)
            End With

            Return info
        End Function
        <Services.WebMethod(True)> _
        <Script.Services.ScriptMethod()> _
        Public Sub InsertarDiscrepancia(ByVal tipo As Integer, ByVal mensaje As Object, ByVal categoria As Object, _
                                    ByVal para As Object, ByVal adjuntos As Object, ByVal imagenesCKEditor As Object, _
                                    ByVal factura As Integer, ByVal linea As Integer)
            Try
                Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
                Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
                Dim oMensaje As New cnMensaje
                Dim Contenido As String = mensaje("contenido").ToString

                'Actualizo el numero de discrepancias
                Dim dtLineas As DataTable = CType(HttpContext.Current.Cache("dsFactura" & CN_Usuario.IdCia & CN_Usuario.Cod), DataSet).Tables(6)
                Dim drLineaFactura() As DataRow = dtLineas.Select("LINEA=" & linea)

                drLineaFactura(0)("TIENE_DISCREP") = DBNullToInt(drLineaFactura(0)("TIENE_DISCREP")) + 1
                drLineaFactura(0)("TIENE_DISCREP_ABTAS") = DBNullToInt(drLineaFactura(0)("TIENE_DISCREP_ABTAS")) + 1

                For Each item As Object In imagenesCKEditor
                    item("guidID") = Guid.NewGuid.ToString
                    Contenido = Replace(Contenido, HttpUtility.HtmlEncode(item("href")), ConfigurationManager.AppSettings("rutanormal") & "script/cn/" & "Thumbnail.ashx?f=" & _
                                                    item("guidID") & "&t=1")
                Next
                With oMensaje
                    .IdMensaje = 0
                    .FechaAlta = DateAdd(DateInterval.Minute, CType(mensaje("timezoneOffset"), Integer), DateTime.Parse(mensaje("fechaAlta"), CN_Usuario.DateFormat))
                    .EnProceso = "x" & mensaje("id") & "x"
                    .Categoria = New cnCategoria
                    .InfoUsuario = New cnUsuario
                    .MeGusta = New cnMeGusta
                    .Categoria.Id = categoria("id")
                    .Categoria.Denominacion = categoria("denominacion")
                    .InfoUsuario.ProveCod = CN_Usuario.CodProveGS
                    .InfoUsuario.Con = CN_Usuario.IdContacto
                    .Titulo = mensaje("titulo")
                    .Contenido = Contenido
                    .Tipo = CType(tipo, Integer)
                    If .Tipo = 2 Then
                        .Fecha = DateAdd(DateInterval.Minute, CType(mensaje("timezoneOffset"), Integer), DateTime.Parse(mensaje("fecha") & " " & mensaje("hora"), CN_Usuario.DateFormat))
                        .MesCorto = StrConv(MonthName(CType(.Fecha, DateTime).Month, True), VbStrConv.ProperCase)
                        .Dia = CType(.Fecha, DateTime).Day
                        .Hora = CType(.Fecha, DateTime).ToString("t")
                        Dim FechaHora As DateTime
                        Dim TimeZoneOffSet As Double = CType(mensaje("timezoneOffset"), Integer)
                        FechaHora = TimeValue((Math.Abs(TimeZoneOffSet) \ 60) & ":" & Math.Round((((-TimeZoneOffSet / 60) - (-TimeZoneOffSet \ 60)) * 60), 2))
                        .Cuando = CType(.Fecha, DateTime).ToString("dddd, dd MMMM yyyy HH:mm", CultureInfo.CreateSpecificCulture(CN_Usuario.Idioma)) & _
                            " (GMT" & IIf(TimeZoneOffSet = 0, "", IIf(TimeZoneOffSet > 0, " -", " +") & FechaHora.ToString("hh:mm")) & ")"
                        .Donde = mensaje("donde")
                    End If

                    .Para = New cnMensajePara
                    ObtenerPara_Mensaje(.Para, para)

                    Dim adjunto As cnAdjunto
                    .Adjuntos = New List(Of cnAdjunto)
                    For Each item As Object In adjuntos
                        If Not item.Count = 0 Then
                            adjunto = New cnAdjunto
                            With adjunto
                                .Nombre = item("nombre")
                                .Size = modUtilidades.Numero(item("size"), CN_Usuario.NumberFormat.NumberDecimalSeparator)
                                .SizeUnit = item("sizeunit")
                                .SizeToString = modUtilidades.FormatNumber(.Size, CN_Usuario.NumberFormat) & " " & .SizeUnit
                                .TipoAdjunto = item("tipoadjunto")
                                .Url = item("url")
                            End With
                            .Adjuntos.Add(adjunto)
                        End If
                    Next
                End With

                Dim dtAdjuntos As New DataTable
                dtAdjuntos.Columns.Add("ID", GetType(System.Int64))
                dtAdjuntos.Columns.Add("NOMBRE", GetType(System.String))
                dtAdjuntos.Columns.Add("GUID", GetType(System.String))
                dtAdjuntos.Columns.Add("SIZE", GetType(System.Double))
                dtAdjuntos.Columns.Add("SIZEUNIT", GetType(System.String))
                dtAdjuntos.Columns.Add("NUM", GetType(System.Int64))
                dtAdjuntos.Columns.Add("TIPOADJUNTO", GetType(System.Int64))
                dtAdjuntos.Columns.Add("URL", GetType(System.String))

                Try
                    If Not oMensaje.Adjuntos.Count = 0 Then
                        Dim rAdjunto As DataRow
                        Dim i As Integer = 0
                        For Each iAdjunto As cnAdjunto In oMensaje.Adjuntos
                            i += 1
                            rAdjunto = dtAdjuntos.NewRow
                            With rAdjunto
                                .Item("NOMBRE") = iAdjunto.Nombre
                                .Item("GUID") = Guid.NewGuid.ToString & "." & Split(iAdjunto.Nombre, ".")(Split(iAdjunto.Nombre, ".").Length - 1)
                                iAdjunto.Guid = .Item("GUID")
                                .Item("SIZE") = iAdjunto.Size
                                .Item("SIZEUNIT") = iAdjunto.SizeUnit
                                .Item("NUM") = i
                                .Item("TIPOADJUNTO") = iAdjunto.TipoAdjunto
                                .Item("URL") = iAdjunto.Url
                                iAdjunto.IdAdjunto = i
                            End With
                            dtAdjuntos.Rows.Add(rAdjunto)
                        Next
                    End If
                Catch ex As Exception
                    Throw ex
                End Try

                Dim ocnDiscrepancias As PMPortalServer.cnDiscrepancias
                ocnDiscrepancias = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
                ocnDiscrepancias.InsertarDiscrepancia(CN_Usuario.CiaComp, oMensaje, factura, linea, dtAdjuntos)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        ''' <summary>
        ''' Obtener las líneas de la factura
        ''' </summary>
        ''' <param name="Factura">Id. de factura</param>  
        ''' <returns>Proveedor</returns>
        ''' <remarks>JVS 22/06/2012</remarks>
        <Services.WebMethod(True)> _
        <Script.Services.ScriptMethod()> _
        Public Function Obtener_Lineas_Factura(ByVal Factura As Integer, ByVal Linea As Integer) As Object
            Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
            Dim ocnInfo As PMPortalServer.cnDiscrepancias
            ocnInfo = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
            Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_User").CiaComp

            Dim oLineas As New List(Of PMPortalServer.cnLineaFactura)
            With CN_Usuario
                oLineas = ocnInfo.Obtener_Lineas_Factura(lCiaComp, .Idioma, Factura, Linea, CN_Usuario.NumberFormat)
            End With
            Return oLineas
        End Function
        ''' <summary>
        ''' Obtener el gestor de la línea de factura
        ''' </summary>
        ''' <param name="Factura">Id. de factura</param>
        ''' <param name="Linea">Linea de factura</param>
        ''' <returns>Gestor</returns>
        ''' <remarks>JVS 19/06/2012</remarks>
        <Services.WebMethod(True)> _
        <Script.Services.ScriptMethod()> _
        Public Function Obtener_Gestor_LineaFactura(ByVal Factura As Integer, ByVal Linea As Integer) As cn_fsItem
            Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
            Dim ocnInfo As PMPortalServer.cnDiscrepancias
            ocnInfo = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
            Dim item As cn_fsItem
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

            'item = ocnInfo.Obtener_Gestor_LineaFactura(lCiaComp, Factura, Linea)
            Return item
        End Function
        ''' <summary>
        ''' Obtener el receptor de la línea de factura
        ''' </summary>
        ''' <param name="Factura">Id. de factura</param>
        ''' <param name="Linea">Linea de factura</param>
        ''' <returns>Receptor</returns>
        ''' <remarks>JVS 19/06/2012</remarks>
        <Services.WebMethod(True)> _
        <Script.Services.ScriptMethod()> _
        Public Function Obtener_Receptor_LineaFactura(ByVal Factura As Integer, ByVal Linea As Integer) As cn_fsItem
            Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
            Dim ocnInfo As PMPortalServer.cnDiscrepancias
            ocnInfo = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
            Dim item As cn_fsItem
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

            'item = ocnInfo.Obtener_Receptor_LineaFactura(lCiaComp, Factura, Linea)
            Return item
        End Function
        <Services.WebMethod(True)> _
        <Script.Services.ScriptMethod()> _
        Public Sub Cerrar_Discrepancia(ByVal IdDiscrepancia As Integer, ByVal linea As Integer)
            Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
            Dim ocnDiscrepancia As PMPortalServer.cnDiscrepancias
            Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
            ocnDiscrepancia = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_User").CiaComp

            'Actualizo el numero de discrepancias
            Dim dtLineas As DataTable = CType(HttpContext.Current.Cache("dsFactura" & CN_Usuario.IdCia & CN_Usuario.Cod), DataSet).Tables(6)
            Dim drLineaFactura() As DataRow = dtLineas.Select("LINEA=" & linea)

            drLineaFactura(0)("TIENE_DISCREP") = DBNullToInt(drLineaFactura(0)("TIENE_DISCREP")) - 1
            drLineaFactura(0)("TIENE_DISCREP_ABTAS") = DBNullToInt(drLineaFactura(0)("TIENE_DISCREP_ABTAS")) - 1

            ocnDiscrepancia.Cerrar_Discrepancias(lCiaComp, IdDiscrepancia:=IdDiscrepancia)
        End Sub
        <Services.WebMethod(True)> _
        <Script.Services.ScriptMethod()> _
        Public Sub Cerrar_Discrepancias_Linea(ByVal Factura As Integer, ByVal Linea As Integer)
            Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
            Dim ocnDiscrepancia As PMPortalServer.cnDiscrepancias
            ocnDiscrepancia = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
            Dim CN_Usuario As PMPortalServer.User = HttpContext.Current.Session("FS_Portal_User")
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_User").CiaComp
            'Actualizo el numero de discrepancias
            Dim dtLineas As DataTable = CType(HttpContext.Current.Cache("dsFactura" & CN_Usuario.IdCia & CN_Usuario.Cod), DataSet).Tables(6)
            Dim drLineaFactura() As DataRow = dtLineas.Select("LINEA=" & Linea)
            drLineaFactura(0)("TIENE_DISCREP") = 0
            drLineaFactura(0)("TIENE_DISCREP_ABTAS") = 0
            ocnDiscrepancia.Cerrar_Discrepancias(lCiaComp, Factura, Linea)
        End Sub
        <Services.WebMethod(True)> _
        <Script.Services.ScriptMethod()> _
        Public Function Comprobar_DiscrepanciasCerradas(ByVal IdFactura As Integer, ByVal Linea As Integer) As Integer
            Dim PMPortalServer As PMPortalServer.Root = HttpContext.Current.Session("FS_Portal_Server")
            Dim ocnDiscrepancia As PMPortalServer.cnDiscrepancias
            ocnDiscrepancia = PMPortalServer.Get_Object(GetType(PMPortalServer.cnDiscrepancias))
            Return ocnDiscrepancia.Comprobar_DiscrepanciasCerradas(HttpContext.Current.Session("FS_Portal_User").CiaComp, IdFactura, Linea)
        End Function
#End Region
#Region "Otras funciones"
        Private Sub ObtenerPara_Mensaje(ByRef ParaMensaje As cnMensajePara, ByVal para As Object)
            Dim row As DataRow
            For Each item As Object In para
                Select Case item.key
                    Case "UON"
                        For Each iUON As Object In item.value
                            If ParaMensaje.Para_UON Is Nothing Then
                                ParaMensaje.Para_UON = New DataTable

                                ParaMensaje.Para_UON.Columns.Add("UON1", GetType(System.String))
                                ParaMensaje.Para_UON.Columns.Add("UON2", GetType(System.String))
                                ParaMensaje.Para_UON.Columns.Add("UON3", GetType(System.String))
                                ParaMensaje.Para_UON.Columns.Add("DEP", GetType(System.String))
                                ParaMensaje.Para_UON.Columns.Add("USU", GetType(System.String))
                            End If
                            If iUON("UON1") Is Nothing AndAlso iUON("UON2") Is Nothing AndAlso iUON("UON3") Is Nothing AndAlso iUON("DEP") Is Nothing AndAlso iUON("USU") Is Nothing Then
                                ParaMensaje.Para_UON_UON0 = True
                            Else
                                row = ParaMensaje.Para_UON.NewRow
                                row("UON1") = iUON("UON1")
                                row("UON2") = iUON("UON2")
                                row("UON3") = iUON("UON3")
                                row("DEP") = iUON("DEP")
                                row("USU") = iUON("USU")

                                ParaMensaje.Para_UON.Rows.Add(row)
                            End If
                        Next
                    Case "PROVE"
                        Dim contactoProveedor As DataRow
                        For Each iProve As Object In item.value
                            If iProve("TIPOPROVE") IsNot Nothing Then
                                Select Case iProve("TIPOPROVE")
                                    Case 1
                                        ParaMensaje.Para_Prove_Tipo = 1
                                    Case 2
                                        ParaMensaje.Para_Prove_Tipo = 2
                                    Case 3
                                        ParaMensaje.Para_Prove_Tipo = 3
                                    Case 4
                                        ParaMensaje.Para_Prove_Tipo = 4
                                    Case 5
                                        ParaMensaje.Para_Prove_Tipo = 5
                                        ParaMensaje.Para_Material_QA = iProve("MATQA")
                                    Case 6
                                        ParaMensaje.Para_Prove_Tipo = 6
                                        ParaMensaje.Para_Material_GMN1 = iProve("GMN1")
                                        ParaMensaje.Para_Material_GMN2 = iProve("GMN2")
                                        ParaMensaje.Para_Material_GMN3 = iProve("GMN3")
                                        ParaMensaje.Para_Material_GMN4 = iProve("GMN4")
                                    Case Else
                                        If ParaMensaje.Para_Prove_Con Is Nothing Then
                                            ParaMensaje.Para_Prove_Con = New DataTable
                                            ParaMensaje.Para_Prove_Con.Columns.Add("PROVE", GetType(System.String))
                                            ParaMensaje.Para_Prove_Con.Columns.Add("CON", GetType(System.Int64))
                                        End If
                                        contactoProveedor = ParaMensaje.Para_Prove_Con.NewRow
                                        contactoProveedor("PROVE") = iProve("CODIGOPROVE")
                                        contactoProveedor("CON") = iProve("IDCONTACTO")

                                        ParaMensaje.Para_Prove_Con.Rows.Add(contactoProveedor)
                                End Select
                            End If
                        Next
                    Case "PROCECOMPRAS"
                        For Each iProceCompra As Object In item.value
                            If iProceCompra("TIPOPARAPROCECOMPRA") IsNot Nothing Then
                                With ParaMensaje
                                    Select Case iProceCompra("TIPOPARAPROCECOMPRA")
                                        Case "Todos"
                                            .Para_ProcesoCompra_Compradores = True
                                            .Para_ProcesoCompra_Invitados = True
                                            .Para_ProcesoCompra_Proveedores = True
                                            .Para_ProcesoCompra_Responsable = True
                                        Case "Responsable"
                                            .Para_ProcesoCompra_Responsable = True
                                        Case "Invitados"
                                            .Para_ProcesoCompra_Invitados = True
                                        Case "Compradores"
                                            .Para_ProcesoCompra_Compradores = True
                                        Case "Proveedores"
                                            .Para_ProcesoCompra_Proveedores = True
                                    End Select
                                    .Para_ProceCompra_Anyo = CType(iProceCompra("ANYO"), Integer)
                                    .Para_ProceCompra_GMN1 = iProceCompra("GMN1")
                                    .Para_ProceCompra_Cod = CType(iProceCompra("COD"), Integer)
                                End With
                            End If
                        Next
                    Case "ESTRUCCOMPRAS"
                        Dim estrucCompras As DataRow
                        For Each iEstrucCompras As Object In item.value
                            If iEstrucCompras("TIPOESTRUCCOMPRAS") IsNot Nothing Then
                                Select Case iEstrucCompras("TIPOESTRUCCOMPRAS")
                                    Case 3
                                        ParaMensaje.Para_EstructuraCompras_MaterialGS = True
                                        ParaMensaje.Para_Material_GMN1 = iEstrucCompras("GMN1")
                                        ParaMensaje.Para_Material_GMN2 = iEstrucCompras("GMN2")
                                        ParaMensaje.Para_Material_GMN3 = iEstrucCompras("GMN3")
                                        ParaMensaje.Para_Material_GMN4 = iEstrucCompras("GMN4")
                                    Case Else
                                        If ParaMensaje.Para_EstructuraCompras_Equipo Is Nothing Then
                                            ParaMensaje.Para_EstructuraCompras_Equipo = New DataTable
                                            ParaMensaje.Para_EstructuraCompras_Equipo.Columns.Add("EQP", GetType(System.String))
                                            ParaMensaje.Para_EstructuraCompras_Equipo.Columns.Add("USU", GetType(System.String))
                                        End If
                                        estrucCompras = ParaMensaje.Para_EstructuraCompras_Equipo.NewRow
                                        With estrucCompras
                                            Select Case iEstrucCompras("TIPOESTRUCCOMPRAS")
                                                Case "1"
                                                    estrucCompras("EQP") = iEstrucCompras("CODIGO")
                                                    estrucCompras("USU") = Nothing
                                                Case "2"
                                                    estrucCompras("EQP") = Nothing
                                                    estrucCompras("USU") = iEstrucCompras("CODIGO")
                                            End Select
                                        End With
                                        ParaMensaje.Para_EstructuraCompras_Equipo.Rows.Add(estrucCompras)
                                End Select
                            End If
                        Next
                    Case "GRUPO"
                        For Each iGrupo As Object In item.value
                            If iGrupo("TIPOGRUPO") Is Nothing Then
                                Dim idGrupo As DataRow
                                If ParaMensaje.Para_Grupos_Grupos Is Nothing Then
                                    ParaMensaje.Para_Grupos_Grupos = New DataTable
                                    ParaMensaje.Para_Grupos_Grupos.Columns.Add("GRUPO", GetType(System.Int64))
                                End If
                                idGrupo = ParaMensaje.Para_Grupos_Grupos.NewRow
                                idGrupo("GRUPO") = iGrupo("IDGRUPO")

                                ParaMensaje.Para_Grupos_Grupos.Rows.Add(idGrupo)
                            Else
                                Select Case iGrupo("TIPOGRUPO")
                                    Case "EP"
                                        ParaMensaje.Para_Grupo_EP = True
                                    Case "GS"
                                        ParaMensaje.Para_Grupo_GS = True
                                    Case "PM"
                                        ParaMensaje.Para_Grupo_PM = True
                                    Case "QA"
                                        ParaMensaje.Para_Grupo_QA = True
                                    Case "SM"
                                        ParaMensaje.Para_Grupo_SM = True
                                End Select
                            End If
                        Next
                End Select
            Next
        End Sub
#End Region
    End Class
End Namespace