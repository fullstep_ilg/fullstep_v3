﻿Imports Fullstep
Namespace Fullstep.PMPortalWeb
    Public Class EliminarFactura
        Inherits FSPMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            'Elimina la instancia:
            Dim oInstancia As PMPortalServer.Instancia
            oInstancia = FSPMServer.Get_Instancia
            oInstancia.ID = Request("Instancia")
            oInstancia.EliminarFactura(IdCiaComp, Request("Factura"))

            Page.ClientScript.RegisterStartupScript(Me.GetType(), "volver", "<script>EliminarOK('VisorFacturas.aspx')</script>")
        End Sub
    End Class
End Namespace