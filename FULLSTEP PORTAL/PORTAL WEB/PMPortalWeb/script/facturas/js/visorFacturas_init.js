﻿var scriptsLoaded = false;
var factura;
var CKEDITOR_BASEPATH = rutanormal + 'ckeditor/';
$.getScript(rutanormal + 'js/jquery/jquery.ui.min.js');
$.getScript(rutanormal + 'js/jsUtilities.js');
$.getScript(rutanormal + 'ckeditor/ckeditor.js');
function MostrarFormularioDiscrepanciasFactura(idFactura, numFactura) {
    factura = idFactura;
    if (scriptsLoaded) {
        LoadDiscrepancias(idFactura);
        $('#DiscrepancialblTituloPopUpNuevoMensaje').text(TextosDiscrepancias[19] + ' ' + numFactura);
    } else {
        LoadScripts();
        $.getScript(rutanormal + 'script/cn/js/cn_mensajes.js', function () {
            $.get(rutanormal + 'script/cn/html/_mensajes.tmpl.htm', function (templates) {
                $('body').append(templates);
                $('body').prepend('<div id="popupPantallaDiscrep" class="popupCN" style="display:none; position:absolute; z-index:1002; width:95%; max-height:80%; text-align:left; padding:5px; overflow:auto;"></div>');
                var cabecera = '<div id="btnCerrarPopUpDiscrepancias" class="CerrarPopUp" style="float:right; width:30px; height:30px;"></div>'
                cabecera = cabecera + '<div class="FondoHeader" style="float:left; margin-bottom:5px;"><img src="' + rutanormal + 'images/Discrepancia.png" style="float:left; margin-left:10px; max-height:25px;" />';
                cabecera = cabecera + '<span id="DiscrepancialblTituloPopUpNuevoMensaje" class="LabelTituloCabeceraCustomControl" style="margin-left:10px; line-height:25px;"></span></div>';
                cabecera = cabecera + '<div id="divDetalleDiscrepancias" style="clear:both; float:left; width:100%; overflow:auto;"></div>';
                $('#popupPantallaDiscrep').prepend(cabecera);
                $('#DiscrepancialblTituloPopUpNuevoMensaje').text(TextosDiscrepancias[19] + ' ' + numFactura);
                $.get(rutanormal + 'script/cn/html/_detalle_discrp_linea.tmpl.htm', function (templates) {
                    $('body').append(templates);
                    LoadDiscrepancias(idFactura);
                });
            });
        });
    }
}
function LoadScripts() {
    $.getScript(rutanormal + 'js/jquery/plugins/jquery.iframe-transport.js');
    $.getScript(rutanormal + 'js/jquery/plugins/video.js', function () {
        VideoJS.setupAllWhenReady();
    });
    $.getScript(rutanormal + 'js/jquery/plugins/autogrow.js');
    $.getScript(rutanormal + 'script/cn/js/cn_funciones.js'); 
    $.getScript(rutanormal + 'script/cn/js/cn_ui_meGusta.js');
    $.getScript(rutanormal + 'script/cn/js/cn_ui_respuestas.js');
    $.getScript(rutanormal + 'script/cn/js/cn_mensajes_ui.js');
    $.getScript(rutanormal + 'js/jquery/plugins/jquery.highlight.js');
    $.getScript(rutanormal + 'js/jquery/plugins/jquery.fileupload.js').done(function () {
        $.getScript(rutanormal + 'js/jquery/plugins/jquery.fileupload-ui.js').done(function () {
            $.when($.get(rutanormal + 'script/cn/html/_adjuntos.tmpl.htm', function (templates) {
                $('body').append(templates);
            })).done(function () {
                $('#fileupload').fileupload();
                $('#fileupload').show();
                $('#fileupload [type=file]').css('right', '');
                $('#fileupload [type=file]').css('left', -10000);
                $('#fileupload [type=file]').css('top', -10000);
                $('#fileupload [type=file]').live('mouseleave', function () {
                    $('#fileupload [type=file]').css('left', -10000);
                    $('#fileupload [type=file]').css('top', -10000);
                    $('.Seleccionable').removeClass('Seleccionable');
                });
            });
        });
    });
    $('#btnCerrarPopUpDiscrepancias').live('click', function () {
        $.when($.ajax({
            type: "POST",
            url: rutanormal + 'script/facturas/services/Facturas.asmx/Comprobar_DiscrepanciasCerradas',
            data: JSON.stringify({ IdFactura: factura, Linea: 0 }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true
        })).done(function (msg) {
            if (parseInt(msg.d) == 0) {
                $('#imgDiscrepancias_' + factura).hide();
                $('#imgDiscrepanciasCerradas_' + factura).show();
            } else {
                $('#imgDiscrepancias_' + factura).show();
                $('#imgDiscrepanciasCerradas_' + factura).hide();
            };
            $('#popupFondo').hide();
            $('#popupPantallaDiscrep').hide();
        });
    });
    $('[id^=btnCerrarDiscrepancia_]').live('click', function () {
        var IdDiscrepancia = $(this).attr('id').split('_')[1];
        $('[id^=lblDiscrepanciaAbierta_' + IdDiscrepancia + ']').hide();
        $('[id^=lblDiscrepanciaCerrada_' + IdDiscrepancia + ']').show();
        $('[id^=btnCerrarDiscrepancia_' + IdDiscrepancia + ']').hide();
        $.ajax({
            type: "POST",
            url: rutanormal + 'script/facturas/services/Facturas.asmx/Cerrar_Discrepancia',
            data: JSON.stringify({ IdDiscrepancia: IdDiscrepancia }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true
        });
    });
    $('[id^=divMostrarIncidenciasCerradas_]').live('click', function () {
        var IdLinea = $(this).attr('id').split('_')[1];
        $('#divMuro_' + IdLinea + ' li').show();
        $('#divMostrarIncidenciasCerradas_' + IdLinea).hide();
        CentrarPopUp($('#popupPantallaDiscrep'));
    });
    
    scriptsLoaded = true;
}
function LoadDiscrepancias(idFactura) {
    $('#divDetalleDiscrepancias').empty();
    var IdLinea;
    $.when($.ajax({
        type: "POST",
        url: rutanormal + 'script/facturas/services/Facturas.asmx/Obtener_Lineas_Factura',
        data: JSON.stringify({ Factura: idFactura, Linea:0 }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    })).done(function (msg) {
        lineasFactura = msg.d;
        var totLineas = lineasFactura.length;
        $.each(lineasFactura, function (index) {
            IdLinea = this.Id;
            $('#detalleDiscrpLinea').tmpl(this).appendTo($('#divDetalleDiscrepancias'));
            $('#divMuro_' + IdLinea).cn_ObtenerMensajesUsuario({
                WebMethodURL: rutanormal + 'script/cn/services/cnMuroUsuario.asmx/ObtenerMensajesUsuario',
                tipo: 0,
                grupo: 0,
                categoria: 0,
                megusta: false,
                pagina: 1,
                historico: false,
                discrepancias: true,
                factura: idFactura,
                linea: IdLinea,
                onDone: function () { ShowPopUpDiscrepancias(totLineas==index+1,IdLinea) }
            });
        });

    });
}
function ShowPopUpDiscrepancias(LoadComplete, idLinea) {
    if (LoadComplete) {
        $('[id^=lblDetalleLinea_]').text(TextosDiscrepancias[5]);
        $('[id^=lblHeaderCodigo_]').text(TextosDiscrepancias[6]);
        $('[id^=lblHeaderDenominacion_]').text(TextosDiscrepancias[7]);
        $('[id^=lblHeaderCantidad_]').text(TextosDiscrepancias[8]);
        $('[id^=lblHeaderPrecio_]').text(TextosDiscrepancias[9]);
        $('[id^=lblHeaderTotalCostes_]').text(TextosDiscrepancias[10]);
        $('[id^=lblHeaderTotalDescuentos_]').text(TextosDiscrepancias[11]);
        $('[id^=lblHeaderImporteNeto_]').text(TextosDiscrepancias[4]);
        $('[id^=lblHeaderObservaciones_]').text(TextosDiscrepancias[12]);
        $('[id^=lblHeaderPedidoFullstep_]').text(TextosDiscrepancias[13]);
        $('[id^=lblHeaderRefFactura_]').text(TextosDiscrepancias[2]);
        $('[id^=lblHeaderAlbaran_]').text(TextosDiscrepancias[1]);
        $('[id^=lblHeaderNumDocSAP_]').text(TextosDiscrepancias[3]);
        $('[id^=lblInfoLinea_]').text(TextosDiscrepancias[14]);
        $('[id^=lblDiscrepanciaAbierta_]').text('(' + TextosDiscrepancias[15] + ')');
        $('[id^=lblDiscrepanciaCerrada_]').text('(' + TextosDiscrepancias[16] + ')');
        $('[id^=lblCerrarDiscrepancia_]').text(TextosDiscrepancias[17]);
        $('[id^=lblMostrarIncidenciasCerradas_]').text(TextosDiscrepancias[18]);               

        $('#popupFondo').css('height', $(document).height());
        $('#popupFondo').show();        
        $('#popupPantallaDiscrep').show();
        if ($('#divMuro_' + idLinea + ' li:hidden').length > 0) {
            $('#divMostrarIncidenciasCerradas_' + idLinea).show();
        } else {
            $('#divMostrarIncidenciasCerradas_' + idLinea).hide();
        }
        CentrarPopUp($('#popupPantallaDiscrep'));
    }
}