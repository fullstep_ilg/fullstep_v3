﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DetalleFactura.aspx.vb" Inherits="Fullstep.PMPortalWeb.DetalleFactura" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<link href="../../../common/estilo.asp" type="text/css" rel="stylesheet"/>
	<script type="text/javascript" src="../../../common/formatos.js"></script>
	<script src="../../js/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
	<script src="../../../common/menu.asp"></script>
	<script type="text/javascript"> dibujaMenu(9)</script>
	<script type="text/javascript">
		$(document).ready(function () {
			/*''' <summary>
			''' Iniciar la pagina.
			''' </summary>     
			''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
			document.getElementById('tablemenu').style.display = 'block';

        });

    /*<summary>
    Obtiene las posiciones del cursor, necesario para el FireFox
    </summary>
    <remarks>Llamada desde: UltraWebGrid_MouseOverHadler; Tiempo=0seg.</remarks>
    */
    var x = 0
    var y = 0
    function Posicion(event) {
        x = event.clientX;
        y = event.clientY;

    }
	</script>
</head>
<body>
	<form id="form1" runat="server">
	<asp:ScriptManager ID="scm" runat="server" EnablePartialRendering="true">
		<Scripts>
			<asp:ScriptReference Path="~/js/jquery/jquery.min.js" />
            <asp:ScriptReference Path="~/js/jquery/jquery-migrate.min.js" />
			<asp:ScriptReference Path="~/js/jquery/jquery.json.min.js" />
			<asp:ScriptReference Path="~/js/jquery/jquery.tmpl.min.js" />
			<asp:ScriptReference Path="~/js/jquery/jquery.ui.min.js" />
			<asp:ScriptReference Path="~/js/jquery/tmpl.min.js" />
			<asp:ScriptReference Path="~/ckeditor/ckeditor.js" />
			<asp:ScriptReference Path="~/script/facturas/js/detalleFactura_init.js" />
		</Scripts>
		<Services>
			<asp:ServiceReference Path="~/Consultas.asmx" />
            <asp:ServiceReference Path="services/Facturas.asmx" />
		</Services>
	</asp:ScriptManager>
    <input id="hBloque" type="hidden" name="hBloque" runat="server"/>
    <input id="RolActual" type="hidden" name="RolActual" runat="server"/>
	<div id="divDetalleFactura" runat="server" onmouseover="Posicion(event)">
		<fsn:FSNPageHeader ID="FSNPageHeader" runat="server" VisibleBotonVolver="true">
		</fsn:FSNPageHeader>
		<div id="divAcciones" runat="server">
                    <ignav:UltraWebMenu ID="uwPopUpAcciones" runat="server" WebMenuTarget="PopupMenu"
				        SubMenuImage="ig_menuTri.gif" ScrollImageTop="ig_menu_scrollup.gif" Cursor="Default"
				        ScrollImageBottomDisabled="ig_menu_scrolldown_disabled.gif" ScrollImageTopDisabled="ig_menu_scrollup_disabled.gif"
				        ScrollImageBottom="ig_menu_scrolldown.gif">
				        <ItemStyle CssClass="ugMenuItem"></ItemStyle>
				        <DisabledStyle ForeColor="LightGray">
				        </DisabledStyle>
				        <HoverItemStyle Cursor="Hand" CssClass="ugMenuItemHover">
				        </HoverItemStyle>
				        <IslandStyle Cursor="Default" BorderWidth="1px" Font-Size="8pt" Font-Names="MS Sans Serif"
					        BorderStyle="Outset" ForeColor="Black" BackColor="LightGray">
				        </IslandStyle>
				        <ExpandEffects ShadowColor="LightGray"></ExpandEffects>
				        <TopSelectedStyle Cursor="Default">
				        </TopSelectedStyle>
				        <SeparatorStyle BackgroundImage="ig_menuSep.gif" CssClass="SeparatorClass" CustomRules="background-repeat:repeat-x; ">
				        </SeparatorStyle>
				        <Levels>
					        <ignav:Level Index="0"></ignav:Level>
				        </Levels>
			        </ignav:UltraWebMenu>
		</div>
		<p>
			<br />
		</p>
		<p>
			<br />
		</p>
		<fspm:DatosGeneralesFactura ID="DatosGenerales" runat="server"></fspm:DatosGeneralesFactura>
		<p>
			<br />
		</p>
		<fspm:Lineas_Factura ID="Lineas_Factura" runat="server"></fspm:Lineas_Factura>
		<p>
			<br />
		</p>
		<fspm:NotificacionesFactura ID="HistoricoNotificaciones" runat="server"></fspm:NotificacionesFactura>
		<p>
			<br />
		</p>
	</div>

    <div id="divDevolverSiguientesEtapas" style="display: none; width:100%; float:left " runat="server">
        <asp:UpdatePanel ID="upDevolverSiguientesEtapas" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
            <ContentTemplate>
            <div id="divPageHeader" style="clear:both;	float:left;	width:100%; position:relative; z-index:0;">
	            <div id="divPageHeaderBack" class="PageHeaderBackground"></div>
                <div id="divPageHeaderImagen" style="float:left; width:32px; height:30px; padding-left:10px; padding-right:5px;">
	            <img src="" alt="" id="imgCabeceraConfirmacion" style="max-width:32px;max-height:30px;" runat="server" />
	            </div>
                <div id="divPageHeaderTitulo" style="float:left; margin-top:15px; white-space:nowrap; overflow:hidden;">
                    <asp:label id="lblCabeceraConfirmacion" runat="server" CssClass="LabelTituloCabeceraCustomControl"></asp:label>
	            </div>
            </div>
            
            <table id="tblGeneral" style="float:left" width="97%" border="0" runat="server">
            <tr>
	            <td align="left"><asp:label id="lblEtapas" runat="server" CssClass="Etiqueta"></asp:label></td>
            </tr>
            <tr>
	            <td><asp:listbox id="lstEtapas" Width="100%" runat="server"></asp:listbox></td>
            </tr>
            <tr>
	            <td style="HEIGHT: 25px" valign="bottom" align="left"><asp:label id="lblRoles" runat="server" CssClass="Etiqueta"></asp:label></td>
            </tr>
            <tr>
            <td><ig:WebHierarchicalDataGrid id="whdgRoles" Width="100%" Height="60px" runat="server" AutoGenerateColumns="false" ShowFooter="false" ShowHeader="true" EnableAjax="true">
                <Columns>
                <ig:BoundDataField Key="DEN" DataFieldName="DEN" Width="35%"></ig:BoundDataField>
                <ig:BoundDataField Key="NOMBRE" DataFieldName="NOMBRE" Width="60%"></ig:BoundDataField>
                <ig:BoundDataField Key="ROL" DataFieldName="ROL" Hidden="true"></ig:BoundDataField>
                <ig:BoundDataField Key="PER" DataFieldName="PER" Hidden="true"></ig:BoundDataField>
                <ig:BoundDataField Key="PROVE" DataFieldName="PROVE" Hidden="true"></ig:BoundDataField>
                <ig:BoundDataField Key="COMO_ASIGNAR" DataFieldName="COMO_ASIGNAR" Hidden="true"></ig:BoundDataField>
                <ig:BoundDataField Key="CON" DataFieldName="CON" Hidden="true"></ig:BoundDataField>
                <ig:BoundDataField Key="TIPO" DataFieldName="TIPO" Hidden="true"></ig:BoundDataField>
                <ig:BoundDataField Key="GESTOR_FACTURA" DataFieldName="GESTOR_FACTURA" Hidden="true"></ig:BoundDataField>
                <ig:UnboundField Key="SEL" Hidden="false" Width="5%"></ig:UnboundField>
                </Columns>
                </ig:WebHierarchicalDataGrid>
            </td>
            </tr>
            <tr><td style="HEIGHT: 25px" valign="bottom" align="left"><asp:label id="lblComentarios" runat="server" Width="100%" CssClass="Etiqueta"></asp:label></TD>
            </tr>
            <tr><td><textarea onkeypress="return validarLength(this,500)" id="txtComent" onkeydown="textCounter(this,500)"
		            onkeyup="textCounter(this,500)" style="WIDTH: 100%; HEIGHT: 100px" name="txtComent" onchange="return validarLength(this,500)"
		            runat="server">
		            </textarea>
	            </td>
            </tr>
            <tr><td align="center">
                <table><tr>
                <td><fsn:FSNButton ID="btnAceptar" runat="server" Alineacion="Right" Text="Confirmar envÃ­o" OnClientClick="return Confirmar();"></fsn:FSNButton></td>
                <td><fsn:FSNButton ID="btnCancelar" runat="server" Alineacion="Left" Text="Cancelar" OnClientClick="return VolverAlDetalle()"></fsn:FSNButton></td>
                </tr></table>
            </td>
            </tr>
            </table>
            
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

	<script type="text/javascript">
        //---------------------------------------
    //INICIO ConfirmaciÃ³n de la acciÃ³n
    
    
    //Abre el buscador de usuarios para elegir el destinatario de la acciÃ³n
	//ParÃ¡metros de entrada:    Rol: Id del rol
	//En el parÃ¡metro FilaGrid le pasamos ahora el id del rol para encontrar la fila en la que estamos asignando el usuario
	function BuscadorUsuarios(Rol) {
    	window.open ("../_common/BuscadorUsuarios.aspx?Rol=" + Rol + "&Instancia=" + document.getElementById("Instancia").value + "&idFilaGrid=" + Rol  ,"_blank", "width=750,height=475,status=yes,resizable=no,top=200,left=200"); 
	}

    //Muestra el detalle de la persona
	//ParÃ¡metro de entrada: Cod de la persona
	function VerDetallePersona(Per){
	    window.open("../_common/detallepersona.aspx?CodPersona=" + Per.toString(),"_blank", "width=450,height=220,status=yes,resizable=no,top=200,left=250");
	}
	
	function VolverAlDetalle() {
        document.getElementById('<%=divDevolverSiguientesEtapas.ClientID%>').style.display='none';
        document.getElementById('<%=divDetalleFactura.ClientID%>').style.display='inline';
        
        return false;
    }


    function Confirmar() {
        var grid, gridview, rows, row;
        var ejecutar_accion = true;

        grid = $find("<%=whdgRoles.ClientID%>")
        if (grid) {
            gridview = grid.get_gridView();
            rows = gridview.get_rows();
            for (var i = 0; i < rows.get_length(); i++) {
                row = rows.get_row(i);

                prove = row.get_cell(4).get_value();
                per = row.get_cell(3).get_value();
                como_asignar = row.get_cell(5).get_value();
                gestor = row.get_cell(8).get_value();

                if ((gestor == 0) && (prove == "") && (per == "") && (como_asignar != 3)){
                    ejecutar_accion = false;
                    break;
                }
            }
        }

        if (ejecutar_accion == false) {
            for (var i = 0; i < rows.get_length(); i++) {
                row = rows.get_row(i);
            
                prove = row.get_cell(4).get_value();
                per = row.get_cell(3).get_value();

                if ((per == "") && (prove == ""))
                    row.get_cell(1)._element.className="fntRequired" //nombre
                else    
                    row.get_cell(1)._element.className=""
            }

            alert(sMensaje);
            return false;
        } else {
            return true;
        }
     }

     function validarLength(e,l)
	{
		if (e.value.length>l)
			return false
		else
			return true
	}

    function strToNum(strNum) {
        if (strNum == undefined || strNum == null || strNum == '')
            strNum = '0'
        
        while (strNum.indexOf(UsuNumberGroupSeparator) >= 0) {
            strNum = strNum.replace(UsuNumberGroupSeparator, "")
        }
        strNum = strNum.replace(UsuNumberDecimalSeparator, ".")
        return parseFloat(strNum)
    }
    
    function formatNumber(num) {
        if (num == undefined || num == null || num == '')
            num = 0
        num=parseFloat(num)
        var result = num.toFixed(UsuNumberNumDecimals);
        result = addSeparadorMiles(result.toString())
        var result1 = result.substring(0, result.length - parseInt(UsuNumberNumDecimals) - 1)
        var result2 = result.substring(result.length - parseInt(UsuNumberNumDecimals), result.length)
        result = result1 + UsuNumberDecimalSeparator + result2
        return result
    }

    function addSeparadorMiles(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + UsuNumberGroupSeparator + '$2');
        }
        var retorno = x1 + x2;
        return retorno
    }    

	function textCounter(e, l) 
	{ 
		if (e.value.length > l) 
			e.value = e.value.substring(0, l); 
	} 
    //FIN ConfirmaciÃ³n de la acciÃ³n
    //---------------------------------------


		function Guardar() {
			return false;
		}
		/*Descripcion:=Llama a la pagina para exportacion de datos
		Llamada desde:Option de menu "Impr./Exp."
		Tiempo ejecucion:0seg.*/
		function cmdImpExp_onclick() {
			return false;
		}

		//Descripción: Cuando se pulsa un botón para realizar una acción, se llama a esta función
		//Paramétros: id: id de la acción
		// bAprob_Discrep: 1 si se debe comprobar si tiene discrepancias abierta, 0 si no se tiene que comprobar
        // bCambioEtapa: 1 si la accion conlleva un cambio de etapa o 0 si no conlleva
		function EjecutarAccion(id, bAprob_Discrep,bCambioEtapa) {
            
           if(bAprob_Discrep==1){
                var bTieneDisdrepancias=false;

               //Se comprueba si tiene discrepancias abiertas
               $("[id*=imgDiscrepancias_]").each(function() { 
                    if($(this)[0].style.display!='none'){
                        alert(TextoValidacionDiscrepancia)
                        bTieneDisdrepancias=true
                        return false;
                    }  
                });
       
               if(bTieneDisdrepancias)
                return false
           }
            
           if(bCambioEtapa==1){
                Fullstep.PMPortalWeb.Facturas.ComprobarImpuestosEnLineas(function(result) {
                    if(result){
                        Fullstep.PMPortalWeb.Facturas.ComprobarTolerancia(ToleranciaImporte, ToleranciaPorcentaje,strToNum(document.getElementById("lblImporteTotal").innerHTML),strToNum(document.getElementById("lblCostesGenerales").innerHTML), function(result) {
                        if(result){
                            GuardarDatosFactura(id)
                        }else{
                            alert(TextoTolerancia);
                        }
                        });
                    }else{
                        alert(TextosValidacion)
                    }     
                });
           }else{
                GuardarDatosFactura(id)
           } 
       
            return false;
    }

    function GuardarDatosFactura(id){
        var tbl;
        var den, operacion, valor, importe;
        var ddl;
        var txt;
        var ArrDen=new Array();
        var ArrOperacion=new Array();   
        var ArrValor=new Array();   
        var ArrImporte=new Array();
        var ArrIdDefAtrib=new Array();           

        ddlFormaPago = document.getElementById("ddlFormasPago")
        ddlViaPago = document.getElementById("ddlViasPago")
        txt = document.getElementById("txtObservaciones")
        
        if (txt) {  //si el detalle de la factura es editable recogemos los datos por si ha habido cambios, si no, no habrÃ¡ habido cambios

            importeTotal = strToNum(document.getElementById("lblImporteTotal").innerHTML) 
            totalCostes = strToNum(document.getElementById("lblCostesGenerales").innerHTML) 
            totalDescuentos = strToNum(document.getElementById("lblDescuentosGenerales").innerHTML)  

            Fullstep.PMPortalWeb.Facturas.InsertarDatosFactura(<%=Request("ID")%>, importeTotal, ddlFormaPago.value, ddlViaPago.value, txt.value, 1, totalCostes, totalDescuentos, function(){
                Fullstep.PMPortalWeb.Facturas.LimpiarCostesDescuentosGenerales(function() {
                    //Costes Generales
                    tbl = document.getElementById("tblCostesGenerales");
                    if (tbl) {
                        for (var i = 0; i < tbl.tBodies[0].rows.length ; i++) {
                            if(tbl.tBodies[0].rows[i].children.item(0).textContent ==undefined)
                                den = tbl.tBodies[0].rows[i].children.item(0).innerText
                            else
                                den = tbl.tBodies[0].rows[i].children.item(0).textContent
                            ArrDen.push(den)   
                            operacion = tbl.tBodies[0].rows[i].children.item(1).firstChild.value
                            ArrOperacion.push(operacion)
                            valor = strToNum(tbl.tBodies[0].rows[i].children.item(2).firstChild.value)
                            ArrValor.push(valor)
                            if (tbl.tBodies[0].rows[i].children.item(3).firstChild.innerHTML == undefined) 
                                importe = strToNum(tbl.tBodies[0].rows[i].children.item(3).innerHTML)
                            else
                                importe = strToNum(tbl.tBodies[0].rows[i].children.item(3).firstChild.innerHTML)
                            ArrImporte.push(importe)
                            if (tbl.tBodies[0].rows[i].children.item(6) == null)
                                def_atrib_id = tbl.tBodies[0].rows[i].children.item(0).firstChild.value
                            else
                                def_atrib_id = tbl.tBodies[0].rows[i].children.item(6).firstChild.value
                            ArrIdDefAtrib.push(def_atrib_id)
                        }
                    }
                    Fullstep.PMPortalWeb.Facturas.InsertarCosteGeneral(ArrIdDefAtrib,ArrDen,ArrOperacion,ArrValor,ArrImporte,function(){
                        ArrDen.splice(0,ArrDen.length)
                        ArrOperacion.splice(0,ArrOperacion.length)
                        ArrValor.splice(0,ArrValor.length)
                        ArrImporte.splice(0,ArrImporte.length)
                        ArrIdDefAtrib.splice(0,ArrIdDefAtrib.length)
                        //Descuentos generales
                        tbl = document.getElementById("tblDescuentosGenerales");
                        if (tbl) {
                            for (var i = 0; i < tbl.tBodies[0].rows.length ; i++) {
                                if(tbl.tBodies[0].rows[i].children.item(0).textContent ==undefined)
                                    den = tbl.tBodies[0].rows[i].children.item(0).innerText
                                else
                                    den = tbl.tBodies[0].rows[i].children.item(0).textContent
                                ArrDen.push(den)   
                                operacion = tbl.tBodies[0].rows[i].children.item(1).firstChild.value
                                ArrOperacion.push(operacion)
                                valor = strToNum(tbl.tBodies[0].rows[i].children.item(2).firstChild.value)
                                ArrValor.push(valor)
                                if (tbl.tBodies[0].rows[i].children.item(3).firstChild.innerHTML == undefined) 
                                    importe = strToNum(tbl.tBodies[0].rows[i].children.item(3).innerHTML)
                                else
                                    importe = strToNum(tbl.tBodies[0].rows[i].children.item(3).firstChild.innerHTML)
                                ArrImporte.push(importe)
                                if (tbl.tBodies[0].rows[i].children.item(6) == null)
                                    def_atrib_id = tbl.tBodies[0].rows[i].children.item(0).firstChild.value
                                else
                                    def_atrib_id = tbl.tBodies[0].rows[i].children.item(6).firstChild.value
                                ArrIdDefAtrib.push(def_atrib_id)
                            }
                        }
                        Fullstep.PMPortalWeb.Facturas.InsertarDescuentoGeneral(ArrIdDefAtrib,ArrDen,ArrOperacion,ArrValor,ArrImporte,function(){
                            __doPostBack("EjecutarAccion", id);
                        })
                    })
                })            
            })      
        }else{
            __doPostBack("EjecutarAccion", id);
        }
        
    }

    function VerFlujo(instancia,NumFactura) {
        window.open("<%=System.Configuration.ConfigurationManager.AppSettings("rutanormal")%>script/solicitudes/comentariosSolicitud.aspx?Instancia=" + instancia + "&NumFactura=" + NumFactura,"_blank", "width=1000,height=560,status=yes,resizable=no,top=200,left=200")		
	    return false;
    }

    function AbrirEFactura(tipo, idEFactura, numFactura) {
        window.open("EFactura.aspx?Id=" + idEFactura + "&Tipo=" + tipo + "&Num=" + numFactura, "_blank", "width=800,height=700,status=no,resizable=yes,top=200,left=200,menubar=no,scrollbars=yes");
        return false;        
    }

    function VerEmailNotificacion(IdMail) {
		window.open("DetalleMail.aspx?IdMail=" + IdMail, "_blank", "width=800,height=700,status=no,resizable=no,top=200,left=200,menubar=no");
    }

	</script>
    <script type="text/javascript" src="../_common/js/jsUpdateProgress.js"></script>
    <script type="text/javascript">        var ModalProgress = '<%= ModalProgress.ClientID %>';</script>
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
			<div style="position: relative; top: 30%; text-align: center;">
				<asp:Image ID="ImgProgress" runat="server" SkinId="ImgProgress" />
                <asp:Label ID="LblProcesando" runat="server" Text="Cargando ..."></asp:Label>	
			</div>
		</asp:Panel>
		<ajx:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
			BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
	</form>
    <div id="fileupload" style="display:none;">
		<form id="formFileupload" method="post" enctype="multipart/form-data">
			<div class="fileupload-buttonbar">
				<label class="fileinput-button">                    
					<input type="file" name="files[]" multiple="multiple" />
				</label>
				<div class="fileupload-progressbar"></div>
			</div>
		</form>
	</div>
    
</body>
</html>
