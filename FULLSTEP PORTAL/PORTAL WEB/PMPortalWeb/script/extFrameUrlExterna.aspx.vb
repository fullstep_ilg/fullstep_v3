﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Net
Imports System.IO
Imports System.Text


Public Class extFrameUrlExterna
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim post As String
        Dim webURL As String
        Dim sUrlExterna As String = String.Empty

        post = Request.QueryString("Post")

        If Not IsNothing(post) Then
            post = Replace(post, "_", "&")
            webURL = Request.QueryString("UrlExterna")
            Response.Redirect(webURL + post)
        Else
            fraPMPortalMain.Attributes.Add("src", Request("UrlExterna"))
        End If

    End Sub

End Class