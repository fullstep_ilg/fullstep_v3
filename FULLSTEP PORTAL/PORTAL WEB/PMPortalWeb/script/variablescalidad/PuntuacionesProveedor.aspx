﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PuntuacionesProveedor.aspx.vb" Inherits="Fullstep.PMPortalWeb.PuntuacionesProveedor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Threading.Thread.CurrentThread.CurrentCulture.IetfLanguageTag %>">
<head id="Head1" runat="server">
	<title></title>
	<meta http-equiv="expires" content="0"/>	
	<script type="text/javascript" src="../../../common/formatos.js"></script>
	<script type="text/javascript" src="../../../common/menu.asp"></script>		
	<script type="text/javascript">dibujaMenu(2)</script>
    <script src="../../js/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../../js/jquery/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="../../js/jquery/jquery.tmpl.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        /*''' <summary>
        ''' Iniciar la pagina.
        ''' </summary>     
        ''' <remarks>Llamada desde: page.onload; Tiempo máximo:0</remarks>*/
        function Init() {
            if (accesoExterno == 0) { document.getElementById('tablemenu').style.display = 'block'; }
        }
    </script>
</head>
<body onload="Init()">
	<script type="text/javascript">
		//===========================================
		//CODIGO QUE SE EJECUTA TRAS CARGAR LA PAGINA
		//===========================================
		$(window).load(function() {
			var i
			var grid = $find("wdg");
			var filas = $("#OcultarFilasInicio").val().split("#");
			for (i = 1; i < filas.length;i++) {
			    grid.get_rows().get_row(filas[i]).get_element().style.display='none'
			}

			$("#tvUnidadesNegocio input[type='checkbox']").click(function (event) {
				var idUNQA = $($(this).next()[0]).attr('href')
				ConfigurarColumnasUNQA(idUNQA, !$(this).is(':checked'))
			});
		});

		//================================
		//INICIO FUNCIONALIDAD POPUP UNQA
		//================================
		function MostrarPopUpEstructuraOrganizativa() {
			$("select[name$='cboTipoColumna']").val("0")
			CargarConfiguracionUNQA()
			$find('ModalEstructuraOrganizativa').show();
		}
		function OcultarPopUpEstructuraOrganizativa() {
			$find('ModalEstructuraOrganizativa').hide();
		}

		function CargarConfiguracionUNQA() {
			var key;
			switch ($("select[name$='cboTipoColumna']").val()) {
				case "0":
					key="CALI";
					break;
				case "1":
					key="PUNT";
					break;
				case "2":
					key="FACT";
					break;
			}
			var grid = $find("wdg");
			for (i = 0; i < grid.get_columns().get_length(); i++) {
				column = grid.get_columns().get_column(i)
				if (column.get_key().split("_")[0] == key) {
					var ocultar = column.get_hidden()
					var id = column.get_key().split("_")[1]
					var checkbox = "#tvUnidadesNegocio span[href='" + id + "']"
					$("#" + $(checkbox).prev()[0].id).attr('checked', !ocultar);
				}
			}
			
		}

		function CambioTipoColumnaUNQA() {
			CargarConfiguracionUNQA()
		}        	               

		function ConfigurarColumnasUNQA(idUNQA, ocultar) {
			var column
			var grid = $find("wdg");

			var key;
			switch ($("select[name$='cboTipoColumna']").val()) {
				case "0":
					key = "CALI";
					break;
				case "1":
					key = "PUNT";
					break;
				case "2":
					key = "FACT";
					break;
			}
			column = grid.get_columns().get_columnFromKey(key + "_" + idUNQA).set_hidden(ocultar)			
		}
		//================================
		//FIN FUNCIONALIDAD POPUP UNQA
		//================================
		//================================
		//INICIO FUNCIONALIDAD BOTONES
		//================================
		function VerPuntuaciones() {
			MostrarColumnas('Puntuacion')
		}

		function VerCalificaciones() {
			MostrarColumnas('Calificacion')
		}
		function VerFechaActualizacion() {
			MostrarColumnas('FechaActualizacion')
		}		

		function MostrarColumnas(tipo) {
			MostrarModalCargando();
			var key;
			switch (tipo) {
				case "Calificacion":
					key = "CALI"
					break;
				case "Puntuacion":
					key = "PUNT"
					break;
				case "FechaActualizacion":
					key = "FACT"
					break;
			}

			$.getScript('../_common/js/jsThread.js', function() {
				OcultaCol(key);
			});
		}
		
		//================================
		//FIN FUNCIONALIDAD BOTONES
		//================================
		//=====================================
		//INICIO FUNCIONALIDAD BOTONES CABECERA
		//=====================================
		function OcultarColumna(key) {
			var grid = $find("wdg");
			var column
			column = grid.get_columns().get_columnFromKey(key)
			column.set_hidden(true)

			var UNQA_Id = key.split("_")[1];
			$(".CALI_" + UNQA_Id).show();
			$(".PUNT_" + UNQA_Id).show();
			$(".FACT_" + UNQA_Id).show();

			return false;
		}
		function MostrarSelector(IdUNQA,event) {
			var menu = $find("ContextMenu");
			var grid = $find("wdg");

			menu.getItems().getItem(0).set_key("CALI_" + IdUNQA)
			menu.getItems().getItem(1).set_key("PUNT_" + IdUNQA)
			menu.getItems().getItem(2).set_key("FACT_" + IdUNQA)               

			var selected = false;
			if (!grid.get_columns().get_columnFromKey("CALI_" + IdUNQA).get_hidden()) {
				menu.getItems().getItem(0).set_visible(false)
			} else {
				menu.getItems().getItem(0).set_visible(true)
			} 
			if (!grid.get_columns().get_columnFromKey("PUNT_" + IdUNQA).get_hidden()) {
				menu.getItems().getItem(1).set_visible(false)
			} else {
				menu.getItems().getItem(1).set_visible(true)
			} 
			if (!grid.get_columns().get_columnFromKey("FACT_" + IdUNQA).get_hidden()) {
				menu.getItems().getItem(2).set_visible(false)
			} else {
				menu.getItems().getItem(2).set_visible(true)
			} 
			
			menu.showAt(null, null, event);
			return false;
		}
		function MenuItem_Click(menu, eventArgs) {
			var grid = $find("wdg");
			var column
			column = grid.get_columns().get_columnFromKey(eventArgs.getItem().get_key())
			column.set_hidden(false)

			var UNQA_Id = eventArgs.getItem().get_key().split("_")[1];
			
			if (!grid.get_columns().get_columnFromKey("CALI_" + UNQA_Id).get_hidden()) {
			    if (!grid.get_columns().get_columnFromKey("PUNT_" + UNQA_Id).get_hidden()) {
			        if (!grid.get_columns().get_columnFromKey("FACT_" + UNQA_Id).get_hidden()) {
			            $('.CALI_' + UNQA_Id).hide();
			            $('.PUNT_' + UNQA_Id).hide();
			            $('.FACT_' + UNQA_Id).hide();
			        }
                }
            }
			return false;
		}        	

		function Guardar() {
			ObtenerConfiguracion()

			var hiddenUNQA = $("#UNQAConfig");
			var hiddenVARCAL = $("#VARCALConfig");
			PageMethods.GuardarConfiguracion(hiddenUNQA.val(), hiddenVARCAL.val())

        }

        function ExportarExcel() {
            ObtenerConfiguracion()

            __doPostBack('ExportarExcel', '')
        }

        function ExportarPDF() {
            ObtenerConfiguracion()

            __doPostBack('ExportarPDF', '')
        }
		//=====================================
		//FIN FUNCIONALIDAD BOTONES CABECERA
		//=====================================
		//====================================
		//INICIO FUNCIONALIDAD EXPAND/COLLAPSE
		//====================================
		function CollapseRows(index, imgCollapse, imgExpand) {
			var grid = $find("wdg");
            
			grid.get_rows().get_row(index).get_cellByColumnKey("EXPANDED").set_value(false)

			document.getElementById(imgCollapse).style.display='none'
			document.getElementById(imgExpand).style.display=''

			OcultarMostrarNivel(true, index)
			return false;
		}

		function ExpandRows(index, imgCollapse, imgExpand) {
			var grid = $find("wdg");

			grid.get_rows().get_row(index).get_cellByColumnKey("EXPANDED").set_value(true)

			document.getElementById(imgCollapse).style.display=''
			document.getElementById(imgExpand).style.display='none'

			var styleDiv = grid.get_rows().get_row(index).get_element().getElementsByTagName('div')[0].style.cssText
			OcultarMostrarNivel(false, index,styleDiv)

			return false;
		}

		function OcultarMostrarNivel(ocultar, index,styleDiv) {
			var grid = $find("wdg");

			var nivelPulsado = grid.get_rows().get_row(index).get_cellByColumnKey("NIVEL").get_value()
			var nivel
			var i
			
			for (i = index + 1; i < grid.get_rows().get_length(); i++) {
				nivel = grid.get_rows().get_row(i).get_cellByColumnKey("NIVEL").get_value()

				switch (nivel) {
					case nivelPulsado + 1:
						if (ocultar) {
						    grid.get_rows().get_row(i).get_element().style.display='none'
							grid.get_rows().get_row(i).get_cellByColumnKey("VISIBLE").set_value(false)
						} else {
			                grid.get_rows().get_row(i).get_element().style.display = ''
							grid.get_rows().get_row(i).get_cellByColumnKey("VISIBLE").set_value(true)
							grid.get_rows().get_row(i).get_element().getElementsByTagName('div')[0].style.cssText = styleDiv
						}
						if (grid.get_rows().get_row(i).get_cellByColumnKey("HASCHILDS").get_value() == true) {
							if (ocultar) {
								OcultarMostrarNivel(true, i, styleDiv)
							} else {
								if (grid.get_rows().get_row(i).get_cellByColumnKey("EXPANDED").get_value()) {
									OcultarMostrarNivel(false, i, styleDiv)
								} else {
									OcultarMostrarNivel(true, i, styleDiv)
								}
							}
						}
						break;

					case nivelPulsado:
						i = grid.get_rows().get_length();
						break;
				}
			}
		}
		//====================================
		//INICIO FUNCIONALIDAD EXPAND/COLLAPSE
		//====================================
		function MostrarModalCargando() {
			$find('ModalCargando').show();
		}

		function OcultarModalCargando() {
			$find('ModalCargando').hide();
		}        	

		function ObtenerConfiguracion() {
			var grid = $find("wdg");
			var i
			var column, row
			var hiddenUNQA = $("#UNQAConfig");
			hiddenUNQA.val("");

			for (i = 0; i < grid.get_columns().get_length(); i++) {
				column = grid.get_columns().get_column(i)

				switch (column.get_key()) {
					case "IMAGES":
					case "DEN":
						break;
					default:
						if (!column.get_hidden()) {
							hiddenUNQA.val(hiddenUNQA.val() + "#" + column.get_key() + "@" + column.get_width())
						}
				}
			}

			var hiddenVARCAL = $("#VARCALConfig");
			hiddenVARCAL.val("");
			for (i = 0; i < grid.get_rows().get_length(); i++) {
				row = grid.get_rows().get_row(i)
				hiddenVARCAL.val(hiddenVARCAL.val() + "#" + row.get_cellByColumnKey("NIVEL").get_value() + "_" + row.get_cellByColumnKey("ID").get_value() + "_" + row.get_cellByColumnKey("EXPANDED").get_value() + "_" + row.get_cellByColumnKey("VISIBLE").get_value());
			}
		}		
	</script>
	<form id="form1" runat="server">
		<asp:ScriptManager runat="server" ID="ScriptManager1" EnablePageMethods="true"></asp:ScriptManager>
	
		<ig:WebExcelExporter ID="wdgExcelExporter" runat="server"></ig:WebExcelExporter>
		<ig:WebDocumentExporter ID="wdgPDFExporter" runat="server"></ig:WebDocumentExporter>

		<ig:WebDataMenu runat="server" ID="ContextMenu" IsContextMenu="true"             
			BorderStyle="Solid" BorderWidth="1" BorderColor="#CCCCCC">
			<ClientEvents ItemClick="MenuItem_Click" />
			<Items>
				<ig:DataMenuItem Key="CALI" Text="DVer calificación" CssClass="Link" />
				<ig:DataMenuItem Key="PUNT" Text="DVer puntuación" CssClass="Link" />
				<ig:DataMenuItem Key="FACT" Text="DVer fecha actualización" CssClass="Link" />
			</Items>
		</ig:WebDataMenu>

        <div style="margin-top:10px;">
            <fsn:FSNPageHeader ID="FSNPageHeader" runat="server"></fsn:FSNPageHeader>		
        </div>

		<div class="CabeceraBotones" style="height:25px; clear:both; line-height:25px;">        
			<div style="float:left;">
				<div onclick="VerPuntuaciones()" class="botonIzquierdaCabecera">  
					<asp:Image runat="server" ID="imgPuntuacion" CssClass="imagenCabecera" />
					<asp:Label runat="server" ID="lblVerPuntuacion" CssClass="fntLogin2" Text="oVer puntuación"></asp:Label> 
				</div>
				<div onclick="VerCalificaciones()" class="botonIzquierdaCabecera">  
					<asp:Image runat="server" ID="imgCalificacion" CssClass="imagenCabecera" /> 
					<asp:Label runat="server" ID="lblVerCalificacion" CssClass="fntLogin2" Text="oVer calificación"></asp:Label> 
				</div>
				<div onclick="VerFechaActualizacion()" class="botonIzquierdaCabecera">      
					<asp:Image runat="server" ID="imgFechaActualizacion" CssClass="imagenCabecera" />
					<asp:Label runat="server" ID="lblVerFechaActualizacion" CssClass="fntLogin2" Text="oVer fecha de actualización"></asp:Label>
				</div>
				<div onclick="MostrarPopUpEstructuraOrganizativa()" class="botonIzquierdaCabecera" style="border:none;">  
					<asp:Image runat="server" ID="imgEstructuraOrganizativa" CssClass="imagenCabecera" />
					<asp:Label runat="server" ID="lblUON" CssClass="fntLogin2" Text="Estructura organizativa"></asp:Label>           
				</div>
			</div>
			<div style="float:right;">
				<div onclick="Guardar()" class="botonDerechaCabecera">     
					<asp:Image runat="server" ID="imgGuardar" CssClass="imagenCabecera" />
					<asp:Label runat="server" ID="lblGuardar" CssClass="fntLogin2" Text="Guardar"></asp:Label>           
				</div>
				<div onclick="ExportarExcel()" class="botonDerechaCabecera">            
					<asp:Image runat="server" ID="imgExcel" CssClass="imagenCabecera" />
					<asp:Label runat="server" ID="lblExcel" CssClass="fntLogin2" Text="Excel"></asp:Label>           
				</div>
				<div onclick="ExportarPDF()" class="botonDerechaCabecera">            
					<asp:Image runat="server" ID="imgPDF" CssClass="imagenCabecera" />
					<asp:Label runat="server" ID="lblPDF" CssClass="fntLogin2" Text="PDF"></asp:Label>           
				</div>
			</div>
		</div>

		<div class="bordeSimple" style="width:100%; overflow:auto;"> 
			<asp:HiddenField runat="server" ID="VariablesCalidadConfiguracionUsuario" Value="" />
			<asp:HiddenField runat="server" ID="ColumnasConfiguracionUsuario" Value="" />
			<asp:HiddenField runat="server" ID="ColumnasConfiguracionSistema" Value="" />
			<ig:WebDataGrid runat="server" ID="wdg" DataSourceID="odsPuntuacionesProveedor" 
				AutoGenerateColumns="False" Width="100%">
				<Columns>
					<ig:TemplateDataField Key="IMAGES" Width="20px">
						<ItemTemplate>
							<asp:ImageButton runat="server" ID="imgExpand" style="display:none;" />
							<asp:ImageButton runat="server" ID="imgCollapse" style="display:none;" />
						</ItemTemplate>
					</ig:TemplateDataField>
					<ig:BoundDataField Key="NIVEL" DataFieldName="NIVEL" Hidden="true"></ig:BoundDataField>
					<ig:BoundDataField Key="ID" DataFieldName="ID" Hidden="true"></ig:BoundDataField>
					<ig:BoundDataField Key="HASCHILDS" DataFieldName="HASCHILDS" Hidden="true"></ig:BoundDataField>					
					<ig:BoundDataField Key="EXPANDED" DataFieldName="EXPANDED" Hidden="true"></ig:BoundDataField>
					<ig:BoundDataField Key="VISIBLE" DataFieldName="VISIBLE" Hidden="true"></ig:BoundDataField>
					<ig:BoundDataField Key="DEN" DataFieldName="DEN" Width="300px"></ig:BoundDataField>
				</Columns>
				<Behaviors>                     
					<ig:ColumnResizing Enabled="true"></ig:ColumnResizing>                 
					<ig:ColumnFixing ShowFixButtons="false" ShowRightSeparator="true" AutoAdjustCells="true">                
						<ColumnSettings>                
							<ig:ColumnFixingSetting ShowFixButton="false" ColumnKey="IMAGES" EnableFixing="True" FixLocation="Left" />
							<ig:ColumnFixingSetting ShowFixButton="false" ColumnKey="DEN" EnableFixing="True" FixLocation="Left" />
						</ColumnSettings>
					</ig:ColumnFixing>
				</Behaviors>
				<Templates>
					<ig:ItemTemplate ID="wdgTemplate1" runat="server" TemplateID="Template1">
						<Template>
							<table cellpadding="2" cellspacing="0" style="width:100%;">
								<tr>
									<td style="width:100%;">
										<asp:Label runat="server" ID="lblUNQA" CssClass="HeaderGrid"></asp:Label>
									</td>
									<td>                                        
										<asp:ImageButton runat="server" ID="imgSelector" />
									</td>
									<td>
										<asp:ImageButton runat="server" ID="imgOcultar" />
									</td>
								</tr>
							</table>
						</Template>
					</ig:ItemTemplate>
				</Templates>
			</ig:WebDataGrid>
		</div>    
        <div class="CabeceraBotones" style="height:25px; clear:both; line-height:25px;">
        </div>
	
		<asp:HiddenField runat="server" ID="UNQAConfig" Value="" />
		<asp:HiddenField runat="server" id="VARCALConfig" Value="" />
		<asp:HiddenField runat="server" id="OcultarFilasInicio" Value="" />

		<asp:ObjectDataSource ID="odsPuntuacionesProveedor" runat="server" SelectMethod="GetPuntuacionesVariablesCalidad"
			TypeName="Fullstep.PMPortalServer.CVariablesCalidad">
			<SelectParameters>
				<asp:Parameter Name="lCiaComp" Type="Int32" />
				<asp:Parameter Name="sCodProve" Type="String" />
				<asp:Parameter Name="sIdioma" Type="String" />
				<asp:Parameter Name="sPyme" Type="String" />
			</SelectParameters>
		</asp:ObjectDataSource>

		<cc1:ModalPopupExtender ID="ModalEstructuraOrganizativa" runat="server"
			TargetControlID="btnHiddenPopUp"
			BackgroundCssClass="modalBackground" 
			PopupControlID="pnlPopUpEstructuraOrganizativa" 
			BehaviorID="ModalEstructuraOrganizativa"/>

		<asp:Button runat="server" ID="btnHiddenPopUp" style="display:none;"/>
		<asp:Panel runat="server" ID="pnlPopUpEstructuraOrganizativa" style="display:none;">
			<div class="PopUpEstructuraOrganizativa">
				<div style="clear:both; float:left;">
					<asp:Image runat="server" id="imgUON" />
				</div>
				<div style="float:left; line-height:40px; padding-left:15px;">
					<asp:Label runat="server" ID="lblUONPopUp" CssClass="titulo"></asp:Label>
				</div>
				<div style="float:right; cursor:pointer;" onclick="OcultarPopUpEstructuraOrganizativa()">
					<asp:Image runat="server" ID="imgOcultarPopUp" />
				</div>   
				<div style="clear:both; float:right; padding:5px;">
					<asp:DropDownList runat="server" ID="cboTipoColumna">
						<asp:ListItem Value="0" Text="DCalificación"/>
						<asp:ListItem Value="1" Text="DPuntuación"/>
						<asp:ListItem Value="2" Text="DFecha actualización"/>
					</asp:DropDownList>                    
				</div>
				<div class="bordeSimple" style="clear:both; max-height:250px; overflow:auto;">                
					<asp:TreeView runat="server" ID="tvUnidadesNegocio" ShowCheckBoxes="All"> 
						<NodeStyle CssClass="fntLogin" />                   
						<DataBindings>
							<asp:TreeNodeBinding DataMember="System.Data.DataRowView" TextField="DEN" ValueField="ID" 
									NavigateUrlField="ID" SelectAction="None" />
						</DataBindings>
					</asp:TreeView>
				</div>                    
			</div>
		</asp:Panel>
	
		<cc1:ModalPopupExtender ID="ModalCargando" runat="server"
			TargetControlID="btnHiddenCargando"
			BackgroundCssClass="modalBackground" 
			PopupControlID="pnlCargando" 
			BehaviorID="ModalCargando"/>
		
		<asp:Button runat="server" ID="btnHiddenCargando" style="display:none;"/>
		<asp:Panel runat="server" ID="pnlCargando" style="display:none;">
			<div class="updateProgress">
				<div style="position: relative; top: 30%; text-align: center;">
					<asp:Image ID="ImgProgress" runat="server" ImageUrl="~/script/_common/images/cargando.gif"/>
				</div>            
			</div>
		</asp:Panel>	
	</form>
</body>
</html>
