﻿Imports Infragistics.Web.UI.GridControls

Namespace Fullstep.PMPortalWeb
    Public Class PuntuacionesProveedor
        Inherits FSPMPage
        Private _Config_UNQA_Usu As String
        Public Property Config_UNQA_Usu() As String
            Get
                Return _Config_UNQA_Usu
            End Get
            Set(ByVal value As String)
                _Config_UNQA_Usu = value
            End Set
        End Property
        Private _Config_UNQA_Defecto As String
        Public Property Config_UNQA_Defecto() As String
            Get
                Return _Config_UNQA_Defecto
            End Get
            Set(ByVal value As String)
                _Config_UNQA_Defecto = value
            End Set
        End Property
        Private _Config_VarCal_Usu As String
        Public Property Config_VarCal_Usu() As String
            Get
                Return _Config_VarCal_Usu
            End Get
            Set(ByVal value As String)
                _Config_VarCal_Usu = value
            End Set
        End Property
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not Page.IsPostBack Then
                Dim sIdi As String = FSPMUser.Idioma
                If sIdi = Nothing Then
                    sIdi = ConfigurationManager.AppSettings("idioma")
                End If

                Me.ModuloIdioma = Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.VariablesCalidad

                FSNPageHeader.TituloCabecera = Textos(9)
                FSNPageHeader.UrlImagenCabecera = "../_common/images/Puntuaciones.gif"
                lblVerPuntuacion.Text = Textos(0)
                lblVerCalificacion.Text = Textos(1)
                lblVerFechaActualizacion.Text = Textos(2)
                lblUON.Text = Textos(3)
                lblGuardar.Text = Textos(4)
                lblExcel.Text = Textos(5)
                lblPDF.Text = Textos(6)
                lblUONPopUp.Text = Textos(3)

                With ContextMenu
                    .Items.Item(0).Text = Textos(1)
                    .Items.Item(1).Text = Textos(0)
                    .Items.Item(2).Text = Textos(2)
                End With
                Dim oUnidadesNeg As Fullstep.PMPortalServer.UnidadesNeg
                oUnidadesNeg = FSPMServer.get_UnidadesNeg()

                Dim dtConfiguraciones As DataTable = FSPMUser.ObtenerConfiguracionesCalificacion(FSPMUser.IdCia, FSPMUser.IdUsu)
                If Not dtConfiguraciones.Rows.Count = 0 Then
                    Config_UNQA_Usu = dtConfiguraciones.Rows(0)("UNQA_VISIBLE_TAMANO")
                    Config_VarCal_Usu = dtConfiguraciones.Rows(0)("VARCAL_VISIBLE")
                Else
                    dtConfiguraciones = FSPMUser.ObtenerConfiguracionesDefectoCalificacion(IdCiaComp)
                    Config_UNQA_Defecto = dtConfiguraciones.Rows(0)("NIVELES_DEFECTO_UNQA")
                End If

                Dim dtDefinicionesUNQA As DataTable = oUnidadesNeg.UnidadesNegocio_PuntuacionesProveedor(IdCiaComp, FSPMUser.CodProve, FSPMUser.Idioma, EmpresaPortal, NomPortal)

                CrearColumnasGrid(dtDefinicionesUNQA)

                With tvUnidadesNegocio
                    .DataSource = New HierarchicalDataSet(dtDefinicionesUNQA.DefaultView, "ID", "IDPADRE")
                    .DataBind()
                    .Attributes.Add("onSelectedIndexChange", "OpcionesUnidadesNegocio()")
                End With

                wdg.Behaviors.ColumnFixing.FixedColumns.Add(New FixedColumnInfo("IMAGES"))
                wdg.Behaviors.ColumnFixing.FixedColumns.Add(New FixedColumnInfo("DEN"))

                With cboTipoColumna
                    .Attributes.Add("onchange", "CambioTipoColumnaUNQA(); return false;")
                    .Items(0).Text = Textos(10)
                    .Items(1).Text = Textos(11)
                    .Items(2).Text = Textos(12)
                End With

                EstablecerRutasImagenesPantalla()
            Else
                Select Case Request("__EVENTTARGET")
                    Case "ExportarExcel"
                        ExportarExcel()
                    Case "ExportarPDF"
                        ExportarPDF()
                End Select
            End If

            If EmpresaPortal Then
                imgEstructuraOrganizativa.Visible = False
                lblUON.Visible = False
            End If

            If Not Page.ClientScript.IsClientScriptBlockRegistered(Me.GetType(), "accesoExterno") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "accesoExterno", "var accesoExterno =" & IIf(FSPMServer.AccesoServidorExterno, 1, 0) & ";", True)
            End If
        End Sub
        Private Sub EstablecerRutasImagenesPantalla()
            imgPuntuacion.ImageUrl = "~/App_Themes/" & Me.Page.Theme & "/images/Puntuacion.png"
            imgCalificacion.ImageUrl = "~/App_Themes/" & Me.Page.Theme & "/images/Calificacion.png"
            imgFechaActualizacion.ImageUrl = "~/App_Themes/" & Me.Page.Theme & "/images/Calendario.png"
            imgEstructuraOrganizativa.ImageUrl = "~/App_Themes/" & Me.Page.Theme & "/images/UON.png"
            imgGuardar.ImageUrl = "~/App_Themes/" & Me.Page.Theme & "/images/Guardar.png"
            imgPDF.ImageUrl = "~/App_Themes/" & Me.Page.Theme & "/images/PDF.png"
            imgExcel.ImageUrl = "~/App_Themes/" & Me.Page.Theme & "/images/Excel.png"

            imgUON.ImageUrl = "~/App_Themes/" & Me.Page.Theme & "/images/UONCabecera.png"
            imgOcultarPopUp.ImageUrl = "~/App_Themes/" & Me.Page.Theme & "/images/CerrarPopUp.png"
        End Sub
        Private Sub CrearColumnasGrid(ByVal DefinicionesUNQA As DataTable)
            Dim oView As DataView = odsPuntuacionesProveedor.Select
            Dim oTable As DataTable = oView.Table
            Dim UNQAColumn As BoundDataField
            Dim NivelUNQA As String
            Dim i As Integer

            With oTable
                For i = 0 To .Columns.Count - 1
                    If (Split(.Columns(i).Caption, "_")(0) = "PUNT" _
                        OrElse Split(.Columns(i).Caption, "_")(0) = "CALI" _
                        OrElse Split(.Columns(i).Caption, "_")(0) = "FACT") _
                        AndAlso DefinicionesUNQA.Rows.OfType(Of DataRow).Select(Function(x) x("ID")).ToList.Contains(CInt(Split(.Columns(i).Caption, "_")(1))) Then

                        UNQAColumn = New BoundDataField
                        UNQAColumn.Key = .Columns(i).Caption
                        UNQAColumn.DataFieldName = .Columns(i).Caption
                        UNQAColumn.Header.Text = DefinicionesUNQA.Select("ID=" & Split(.Columns(i).Caption, "_")(1))(0)("DEN")
                        UNQAColumn.Header.TemplateId = "Template1"
                        If Split(.Columns(i).Caption, "_")(0) = "PUNT" Then
                            UNQAColumn.CssClass = "CeldaNumericaDataGrid"
                        End If

                        If Config_UNQA_Usu Is Nothing Then
                            If Split(.Columns(i).Caption, "_")(0) = "CALI" Then
                                NivelUNQA = "," & DefinicionesUNQA.Select("ID=" & Split(.Columns(i).Caption, "_")(1))(0)("NIVEL") & ","
                                UNQAColumn.Hidden = IIf(("," & Config_UNQA_Defecto & ",").Contains(NivelUNQA), False, True)
                                UNQAColumn.Width = 150
                            Else
                                UNQAColumn.Hidden = True
                                UNQAColumn.Width = 150
                            End If
                        Else
                            If Config_UNQA_Usu.Contains("#" & UNQAColumn.Key & "@") Then
                                UNQAColumn.Hidden = False
                                UNQAColumn.Width = Replace(Split(Split(Config_UNQA_Usu, "#" & UNQAColumn.Key & "@")(1), "#")(0), "px", "")
                            Else
                                UNQAColumn.Hidden = True
                                UNQAColumn.Width = 150
                            End If
                        End If

                        wdg.Columns.Add(UNQAColumn)
                    End If
                Next
            End With
        End Sub
        Private Sub odsPuntuacionesProveedor_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsPuntuacionesProveedor.ObjectCreating
            e.ObjectInstance = FSPMServer.Get_Variables()
        End Sub
        Private Sub odsPuntuacionesProveedor_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsPuntuacionesProveedor.Selecting
            Dim oUser As Fullstep.PMPortalServer.User
            oUser = Session("FS_Portal_User")

            e.InputParameters.Item("lCiaComp") = IdCiaComp
            e.InputParameters.Item("sCodProve") = oUser.CodProve
            e.InputParameters.Item("sIdioma") = oUser.Idioma.ToString
            e.InputParameters.Item("EmpresaPortal") = EmpresaPortal
            e.InputParameters.Item("NomPortal") = NomPortal
            e.InputParameters.Item("sPyme") = ""
        End Sub
        ''' <summary>
        ''' Inicializar cada linea del grid 
        ''' </summary>
        ''' <param name="sender">parametro de sistema</param>
        ''' <param name="e">parametro de sistema</param>     
        ''' <remarks>Llamada desde:sistema; Tiempo máximo:0</remarks>
        Private Sub whdgPuntuacionesProveedor_InitializeRow(ByVal sender As Object, ByVal e As RowEventArgs) Handles wdg.InitializeRow
            Dim gridCellIndex As Integer
            For i As Integer = 0 To wdg.Columns.Count - 1
                With wdg.Columns(i)
                    If Split(.Key, "_")(0) = "PUNT" OrElse
                        Split(.Key, "_")(0) = "CALI" OrElse
                        Split(.Key, "_")(0) = "FACT" Then
                        With CType(.Header.TemplateContainer.FindControl("lblUNQA"), Label)
                            .Text = wdg.Columns(i).Header.Text
                            .ToolTip = wdg.Columns(i).Header.Text
                        End With

                        With CType(.Header.TemplateContainer.FindControl("imgSelector"), ImageButton)
                            .CssClass = wdg.Columns(i).Key
                            .Attributes.Add("onclick", "return MostrarSelector('" & Split(wdg.Columns(i).Key, "_")(1) & "',event);")
                            .ImageUrl = "~/App_Themes/" & Me.Page.Theme & "/images/selector.gif"
                        End With
                        With CType(.Header.TemplateContainer.FindControl("imgOcultar"), ImageButton)
                            .Attributes.Add("onclick", "return OcultarColumna('" & wdg.Columns(i).Key & "');")
                            .ImageUrl = "~/App_Themes/" & Me.Page.Theme & "/images/cerrar.gif"
                        End With

                        If Split(.Key, "_")(0) = "FACT" AndAlso wdg.Columns("DEN").Header.Text = "" Then
                            Dim ci As Globalization.CultureInfo
							ci = New Globalization.CultureInfo(FSPMUser.RefCultural)

							'Bucamos escribir el nombre del mes en el idioma del usuario. Esto lo da la funcion MonthName, 
							'pero los meses van de 0 a 11, por lo que al mes del calculo le restamos dos, 
							'uno porque la puntuacion calculada es la del mes anterior y otro porque para la funcion el numero
							'que debemos pasar es uno menos.
							Dim MonthNumber As Integer = Month(CType(e.Row.DataItem, DataRowView).Item(wdg.Columns(i).Key))
                            MonthNumber = IIf(MonthNumber = 1, 11, MonthNumber - 2)
                            Dim Anio As Integer = Year(CType(e.Row.DataItem, DataRowView).Item(wdg.Columns(i).Key))
                            Anio = IIf(MonthNumber = 11, Anio - 1, Anio)
                            wdg.Columns("DEN").Header.Text = Replace(Textos(7), "###", StrConv(ci.DateTimeFormat.MonthNames(MonthNumber), vbProperCase) & " " & Anio)
                        End If
                    End If
                End With
            Next

            Dim index As Integer = wdg.Columns.FromKey("DEN").Index
            Select Case CType(e.Row.DataItem, DataRowView).Item("NIVEL")
                Case 0
                    e.Row.Items(index).Text = e.Row.Items(index).Text
                Case 1
                    e.Row.Items(index).Text = "&nbsp;&nbsp;&nbsp;&nbsp;" & e.Row.Items(index).Text
                Case 2
                    e.Row.Items(index).Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & e.Row.Items(index).Text
                Case 3
                    e.Row.Items(index).Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & e.Row.Items(index).Text
                Case 4
                    e.Row.Items(index).Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & e.Row.Items(index).Text
                Case 5
                    e.Row.Items(index).Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & e.Row.Items(index).Text
            End Select

            Dim imgExpand As ImageButton = CType(e.Row.Items(wdg.Columns.FromKey("IMAGES").Index).FindControl("imgExpand"), ImageButton)
            Dim imgCollapse As ImageButton = CType(e.Row.Items(wdg.Columns.FromKey("IMAGES").Index).FindControl("imgCollapse"), ImageButton)

            With imgExpand
                .Attributes.Add("onclick", "return ExpandRows(" & e.Row.Index & ",'" & imgCollapse.ClientID & "','" & imgExpand.ClientID & "');")
                .ImageUrl = "~/App_Themes/" & Me.Page.Theme & "/images/ighg_Expand.gif"
            End With
            With imgCollapse
                .Attributes.Add("onclick", "return CollapseRows(" & e.Row.Index & ",'" & imgCollapse.ClientID & "','" & imgExpand.ClientID & "');")
                .ImageUrl = "~/App_Themes/" & Me.Page.Theme & "/images/ighg_Collapse.gif"
            End With

            Dim Expanded, Visible As Boolean
            Dim ConfigVisibilidad As String
            If Config_VarCal_Usu IsNot String.Empty AndAlso Config_VarCal_Usu IsNot Nothing Then
                With CType(e.Row.DataItem, DataRowView)
                    If Config_VarCal_Usu.Contains("#" & .Item("NIVEL") & "_" & .Item("ID") & "_") Then
                        ConfigVisibilidad = Split(Split(Config_VarCal_Usu, "#" & .Item("NIVEL") & "_" & .Item("ID") & "_")(1), "#")(0)
                        Expanded = CType(Split(ConfigVisibilidad, "_")(0), Boolean)
                        Visible = CType(Split(ConfigVisibilidad, "_")(1), Boolean)

                        If .Item("HASCHILDS") Then
                            .Item("EXPANDED") = Expanded
                            If Expanded Then
                                imgCollapse.Attributes.Item("style") = ""
                            Else
                                imgExpand.Attributes.Item("style") = ""
                            End If
                        End If

                        If Not Visible Then
                            e.Row.Items.Item(5).Text = "False"

                            If Not (OcultarFilasInicio.Value & "#").Contains("#" & e.Row.Index & "#") Then
                                OcultarFilasInicio.Value = OcultarFilasInicio.Value & "#" & e.Row.Index
                            End If
                        End If
                    End If
                End With
            Else
                If CType(e.Row.DataItem, DataRowView).Item("HASCHILDS") Then
                    imgCollapse.Attributes.Item("style") = ""
                End If
            End If
            For Each item As Object In wdg.Columns
                If item.Type = System.Type.GetType("System.DateTime") Then
                    gridCellIndex = wdg.Columns.FromKey(item.Key).Index
                    CType(e.Row.Items(gridCellIndex).Column, Infragistics.Web.UI.GridControls.BoundDataField).DataFormatString = "{0:" & FSPMUser.DateFormat.ShortDatePattern & "}"
                End If

                If Split(item.Key, "_")(0) = "PUNT" Then
                    gridCellIndex = wdg.Columns.FromKey(item.Key).Index
                    e.Row.Items(gridCellIndex).Text = FormatNumber(DBNullToDbl(e.Row.Items(gridCellIndex).Value), FSPMUser.NumberFormat)
                End If
            Next
        End Sub
        Private Sub ExportarExcel()
            Config_UNQA_Usu = UNQAConfig.Value
            Config_VarCal_Usu = VARCALConfig.Value

            ConfigurarGridExport()
            wdg.DataBind()

            Dim fileName As String = HttpUtility.UrlEncode("Excel")
            fileName = fileName.Replace("+", "%20")

            With wdgExcelExporter
                .DownloadName = fileName
                .DataExportMode = DataExportMode.DataInGridOnly

                .Export(wdg)
            End With
        End Sub
        Private Sub ExportarPDF()
            Config_UNQA_Usu = UNQAConfig.Value
            Config_VarCal_Usu = VARCALConfig.Value

            ConfigurarGridExport()

            wdg.DataBind()

            Dim fileName As String = HttpUtility.UrlEncode("PDF")
            fileName = fileName.Replace("+", "%20")
            wdgPDFExporter.DownloadName = fileName

            With wdgPDFExporter
                .EnableStylesExport = True

                .DataExportMode = DataExportMode.DataInGridOnly
                .TargetPaperOrientation = Infragistics.Documents.Reports.Report.PageOrientation.Landscape

                .Margins = Infragistics.Documents.Reports.Report.PageMargins.GetPageMargins("Narrow")
                .TargetPaperSize = Infragistics.Documents.Reports.Report.PageSizes.GetPageSize("A4")
                .Export(wdg)
            End With
        End Sub
        Private Sub wdgPDFExporter_RowExporting(ByVal sender As Object, ByVal e As Infragistics.Web.UI.GridControls.DocumentRowExportingEventArgs) Handles wdgPDFExporter.RowExporting
            If Not e.IsFooterRow AndAlso Not e.IsHeaderRow Then
                e.GridRow.Items.Item(6).Text = Replace(e.GridRow.Items.Item(6).Text, "&nbsp;", "")

                If e.GridRow.Items.Item(5).Text = "False" Then
                    e.Cancel = True
                End If
            End If
        End Sub
        Private Sub ConfigurarGridExport()
            For i As Integer = 0 To wdg.Columns.Count - 1
                With wdg.Columns(i)
                    If Split(.Key, "_")(0) = "PUNT" OrElse
                        Split(.Key, "_")(0) = "CALI" OrElse
                        Split(.Key, "_")(0) = "FACT" Then

                        If Config_UNQA_Usu.Contains("#" & .Key & "@") Then
                            .Hidden = False
                            .Width = Replace(Split(Split(Config_UNQA_Usu, "#" & .Key & "@")(1), "#")(0), "px", "")
                        Else
                            .Hidden = True
                            .Width = 150
                        End If
                    End If
                End With
            Next
        End Sub
        ''' <summary>
        ''' Guardar Configuracion de los campos en pantalla del visor
        ''' </summary>
        ''' <param name="UNQAConfig">Unidad QA </param>
        ''' <param name="VARCALConfig">Variable de calidad</param>
        ''' <remarks>Llamada desde: javacript de PuntuacionesProveedor.aspx ; Tiempo máximo: 0,2</remarks>
        <Services.WebMethod(EnableSession:=True)>
        <Script.Services.ScriptMethod()>
        Public Shared Sub GuardarConfiguracion(ByVal UNQAConfig As String, ByVal VARCALConfig As String)
            Dim oUser As Fullstep.PMPortalServer.User
            oUser = HttpContext.Current.Session("FS_Portal_User")

            oUser.GuardarConfiguracionesCalificacion(oUser.IdCia, oUser.IdUsu, UNQAConfig, VARCALConfig)
        End Sub
    End Class
End Namespace