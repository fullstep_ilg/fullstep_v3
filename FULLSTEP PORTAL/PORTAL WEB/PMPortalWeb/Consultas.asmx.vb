﻿Imports System.ComponentModel
Imports System.Text
Imports System.Web.Script.Services
Imports System.Web.Services

Namespace Fullstep.PMPortalWeb


    ' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
    ' <System.Web.Script.Services.ScriptService()> _
    <System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
    <System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
    <System.Web.Script.Services.ScriptService()> _
    <ToolboxItem(False)> _
    Public Class Consultas
        Inherits System.Web.Services.WebService

#Region "Persona"

        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Función que se encarga de rellenar los datos de detalle de una persona
        ''' </summary>
        ''' <param name="contextKey">Código de persona</param>
        ''' <returns>Un String con los datos de la persona en formato html</returns>
        <System.Web.Services.WebMethod(True)> _
        Public Function Obtener_DatosPersona(ByVal contextKey As System.String) As System.String
            Dim oFSUsuario As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim oFSServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)            

            Dim oPer As Fullstep.PMPortalServer.Persona = oFSServer.Get_Persona
            oPer.LoadData(Session("FS_Portal_User").CiaComp, contextKey, True) '..., , False  ;Evitar error del popup cuando el peticionario esta dado en bajalog=1

            Dim oDict As Fullstep.PMPortalServer.Dictionary = oFSServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DetallePersona, oFSUsuario.Idioma)
            Dim sr As New StringBuilder()
            With oPer
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(0).Item(1) & ":</b> " & .Codigo & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b> " & .Nombre & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b> " & .Apellidos & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(3).Item(1) & ":</b> " & .Cargo & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(4).Item(1) & ":</b> " & _
                            "<a class=""Normal"" href=""callto:" & .Telefono & """>" & .Telefono & "</a><br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(5).Item(1) & ":</b> " & .Fax & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(6).Item(1) & ":</b> " & _
                          "<a class=""Normal"" href=""mailto:" & .EMail & """>" & .EMail & "</a><br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(7).Item(1) & ":</b> " & .DenDepartamento & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(8).Item(1) & ":</b> " & .UONs & "<br/>")
            End With

            Return sr.ToString()
        End Function
        <System.Web.Services.WebMethod(True)> _
        Public Function CargarDatosDestinos(ByVal sID As System.String) As Object
            'Dim oPer As FSNServer.CPersona
            Dim FSNServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)
            Dim FSNUser As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim oDestino As Fullstep.PMPortalServer.Destino
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")
            Dim oCDestinos As Fullstep.PMPortalServer.Destinos = FSNServer.Get_Destinos()
            Dim dsDatosDestino As DataSet = oCDestinos.CargarDatosDestino(FSNUser.Idioma, sID, lCiaComp)
            Dim sr As New StringBuilder()

            'oPer.Cod = FSNUser.CodPersona
            'oPer.CargarDestinos(FSNUser.Idioma, sID)
            Dim oDict As Fullstep.PMPortalServer.Dictionary = FSNServer.Get_Dictionary()

            oDict.LoadData(PMPortalServer.TiposDeDatos.ModulosIdiomas.DetalleEmpresa, FSNUser.Idioma)

            Dim lTextos As New System.Collections.Generic.List(Of String)
            lTextos.Add(oDict.Data.Tables(0).Rows(2).Item(1) & ": ")
            lTextos.Add(oDict.Data.Tables(0).Rows(3).Item(1) & ": ")
            lTextos.Add(oDict.Data.Tables(0).Rows(4).Item(1) & ": ")
            lTextos.Add(oDict.Data.Tables(0).Rows(5).Item(1) & ": ")
            lTextos.Add(oDict.Data.Tables(0).Rows(6).Item(1) & ": ")
            lTextos.Add(oDict.Data.Tables(0).Rows(7).Item(1) & ": ")
            lTextos.Add(oDict.Data.Tables(0).Rows(8).Item(1) & ": ")
            lTextos.Add(oDict.Data.Tables(0).Rows(9).Item(1) & ": ")
            lTextos.Add(oDict.Data.Tables(0).Rows(11).Item(1) & ": ")
            lTextos.Add(oDict.Data.Tables(0).Rows(10).Item(1) & ": ")
            Return {oCDestinos.CargarUnDestino(FSNUser.Idioma, sID, lCiaComp), lTextos}
        End Function
#End Region


#Region "Detalle de pagos de una factura"
        ''' <summary>
        ''' FunciÃ³n que se encarga de rellenar los datos de detalle de un albarÃ¡n
        ''' </summary>
        ''' <param name="contextKey">AlbarÃ¡n</param>
        ''' <returns>Un String con los datos de un albarÃ¡n en formato html</returns>
        <System.Web.Services.WebMethod(True)> _
        Public Function Obtener_DatosDetallepagos(ByVal contextKey As Long) As System.String
            Dim FSNServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)
            Dim FSNUser As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")
            Dim oPagos As Fullstep.PMPortalServer.Pagos = FSNServer.Get_Object(GetType(Fullstep.PMPortalServer.Pagos))
            oPagos = oPagos.CargarPagos(lCiaComp, FSNUser.Idioma, , contextKey)

            Dim sr As New StringBuilder()
            sr.Append("<center><table width=80% border=0 cellspacing=0 cellpadding=5>")
            sr.Append("<tr bgcolor=#cccccc><th width=30% align=left>Numero pago</th><th width=20% align=left>Fecha emisión</th><th width=50% align=left>Estado</th></tr>")

            For Each pago As Fullstep.PMPortalServer.Pago In oPagos
                sr.Append("<tr>")
                sr.Append("<td align=left>" & pago.NumPago & "</td>")
                sr.Append("<td align=left>" & FormatDate(pago.Fecha, FSNUser.DateFormat) & "</td>")
                sr.Append("<td align=left>" & pago.EstadoDen & "</td>")
                sr.Append("</tr>")
            Next
            sr.Append("</table></center>")
            Return sr.ToString()
        End Function

#End Region


#Region "Monitorizar"
        ''' <summary>
        ''' Procedimiento que graba la monitorizaciÃ³n o no de una instancia
        ''' </summary>
        ''' <param name="lID">Identificador del contacto a modificar</param>
        ''' <param name="Proveedor">Proveedor</param>
        ''' <returns>Identificador del contacto a modificar</returns>
        ''' <remarks>Llamada desde: 2008/puntuacion/Contactos/wdgDatos_CellSelectionChanged; Tiempo mÃƒÂ¡ximo: 0,1 seg</remarks>
        <System.Web.Services.WebMethod(True)> _
        Public Sub Monitorizar(ByVal lInstancia As Long, ByVal iSeg As Short, ByVal Fila As Short)
            Dim oFSServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)
            Dim oFSUsuario As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")
            Dim cInstancia As Fullstep.PMPortalServer.Instancia
            Dim objDict As Hashtable = HttpContext.Current.Session("Hast_Seg_" & oFSUsuario.Cod & "_" & oFSUsuario.CodCia)
            If objDict Is Nothing Then objDict = New Hashtable


            objDict.Item(Fila) = iSeg
            HttpContext.Current.Session("Hast_Seg_" & oFSUsuario.Cod & "_" & oFSUsuario.CodCia) = objDict

            cInstancia = oFSServer.Get_Instancia
            cInstancia.ID = lInstancia
            cInstancia.ActualizarMonitorizacion(lCiaComp, iSeg, oFSUsuario.CodProve, oFSUsuario.IdUsu)
        End Sub
#End Region


#Region "Motivo anulaciÃ³n de una factura"
        ''' <summary>
        ''' FunciÃ³n que se encarga de rellenar los datos de detalle de un albarÃ¡n
        ''' </summary>
        ''' <param name="contextKey">AlbarÃ¡n</param>
        ''' <returns>Un String con los datos de un albarÃ¡n en formato html</returns>
        <System.Web.Services.WebMethod(True)> _
        Public Function Obtener_MotivoAnulacion(ByVal contextKey As Long) As System.String
            Dim FSNServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)
            Dim FSNUser As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")
            Dim oFactura As Fullstep.PMPortalServer.Factura = FSNServer.Get_Object(GetType(Fullstep.PMPortalServer.Factura))
            oFactura.ID = contextKey
            Return oFactura.DevolverMotivoAnulacion(lCiaComp)
        End Function
#End Region

#Region "EntregasPedido"

        ''' Revisado por: mpf. Fecha: 30/8/2012
        ''' <summary>
        ''' Función que se encarga de rellenar los datos de detalle de factura
        ''' </summary>
        ''' <param name="contextKey">Código de persona</param>
        ''' <returns>Un String con los datos de la persona en formato html</returns>
        <System.Web.Services.WebMethod(True)> _
        Public Function Obtener_DatosEntregaFactura(ByVal contextKey As Integer) As System.String
            Dim oFSUsuario As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim oFSServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)

            Dim oFact As Fullstep.PMPortalServer.Factura
            oFact = oFSServer.Get_Factura
            oFact.LoadDataFactura(Session("FS_Portal_User").CiaComp, contextKey, oFSUsuario.Idioma)

            Dim oDict As Fullstep.PMPortalServer.Dictionary = oFSServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.EntregaPedidos, oFSUsuario.Idioma)
            Dim sr As New StringBuilder()

            sr.Append("<center><table width=80% border=0 cellspacing=0 cellpadding=5>")
            sr.Append("<tr bgcolor=#cccccc><th width=30% align=left>" & oDict.Data.Tables(0).Rows(82).Item(1) & "</th><th width=20% align=left>" & oDict.Data.Tables(0).Rows(83).Item(1) & "</th><th width=50% align=left>" & oDict.Data.Tables(0).Rows(84).Item(1) & "</th></tr>")
            sr.Append("<tr>")
            sr.Append("<td align=left>" & oFact.Data.Tables(0).Rows(0).Item("NUM") & "</td>")
            sr.Append("<td align=left>" & FormatDate(oFact.Data.Tables(0).Rows(0).Item("FECHA"), oFSUsuario.DateFormat) & "</td>")
            sr.Append("<td align=left>" & oFact.Data.Tables(0).Rows(0).Item("DEN") & "</td>")
            sr.Append("</tr>")
            Return sr.ToString()

        End Function

#End Region

#Region "Datos Receptor"

        ''' Revisado por: mpf. Fecha: 31/8/2012
        ''' <summary>
        ''' Función que obtiene los datos del receptor
        ''' </summary>
        ''' <param name="contextKey">Código de persona</param>
        ''' <returns>Un String con los datos de la persona en formato html</returns>
        <System.Web.Services.WebMethod(True)> _
        Public Function Obtener_DatosReceptor(ByVal contextKey As System.String) As System.String
            Dim oFSUsuario As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim oFSServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)

            Dim oPer As Fullstep.PMPortalServer.Personas
            oPer = oFSServer.Get_Personas
            oPer.LoadDataReceptor(Session("FS_Portal_User").CiaComp, contextKey)

            Dim oDict As Fullstep.PMPortalServer.Dictionary = oFSServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DetallePersona, oFSUsuario.Idioma)
            Dim sr As New StringBuilder()
            With oPer.Data.Tables(0).Rows(0)
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b> " & .Item("APE") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b> " & .Item("NOM") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(3).Item(1) & ":</b> " & .Item("CAR") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(4).Item(1) & ":</b> " & .Item("TFNO") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(5).Item(1) & ":</b> " & .Item("FAX") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(6).Item(1) & ":</b> " & .Item("EMAIL") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(7).Item(1) & ":</b> " & .Item("DEPARTAMENTO") & "<br/>")
                sr.Append("<b>" & oDict.Data.Tables(0).Rows(8).Item(1) & ":</b> " & .Item("ORGANIZACION") & "<br/>")

            End With
            Return sr.ToString()

        End Function

#End Region

#Region "Busqueda Centros Coste"
        ''' <summary>
        ''' FunciÃ³n que se encarga de rellenar los datos de detalle de un albarÃ¡n
        ''' </summary>
        ''' <param name="contextKey">AlbarÃ¡n</param>
        ''' <returns>Un String con los datos de un albarÃ¡n en formato html</returns>
        <System.Web.Services.WebMethod(True)> _
        Public Function Buscar_CentroCoste(ByVal CentroCoste As String) As RespuestaJSon
            Dim objRespuesta As New RespuestaJSon
            Dim FSNServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)
            Dim FSNUser As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")
            Dim oCentro_Sm As Fullstep.PMPortalServer.Centros_SM = FSNServer.Get_Centros_SM
            Dim dtCentro As DataTable
            dtCentro = oCentro_Sm.Buscar_Centro(lCiaComp, CentroCoste, FSNUser.Idioma)

            If dtCentro.Rows.Count > 0 Then
                objRespuesta.ExisteCentroCoste = True
                objRespuesta.DenCentroCoste = dtCentro.Rows(0)("DEN")
                objRespuesta.CodCentroCoste = dtCentro.Rows(0)("COD")
            Else
                objRespuesta.ExisteCentroCoste = False
            End If

            Return objRespuesta
        End Function
#End Region

#Region "Proveedor"

        ''' <summary>
        ''' FunciÃ³n que se encarga de rellenar los datos de detalle de un proveedor
        ''' </summary>
        ''' <param name="contextKey">CÃ³digo de proveedor</param>
        ''' <returns>Un String con los datos del proveedor en formato html</returns>
        <System.Web.Services.WebMethod(True)> _
        Public Function Obtener_DatosProveedor(ByVal contextKey As System.String) As System.String
            Dim oFSUsuario As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim oFSServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)
            Dim oProve As Fullstep.PMPortalServer.Proveedor
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

            oProve = oFSServer.get_Proveedor
            oProve.Cod = contextKey
            oProve.Load(oFSUsuario.Idioma, lCiaComp)

            Dim oDict As Fullstep.PMPortalServer.Dictionary = oFSServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DetalleProveedor, oFSUsuario.Idioma)
            Dim sr As New StringBuilder()

            sr.Append("<b>" & oDict.Data.Tables(0).Rows(0).Item(1) & ":</b> " & oProve.Cod & "<br/>")
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b> " & oProve.Den & "<br/>")
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b> " & oProve.NIF & "<br/>")
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(3).Item(1) & ":</b> " & oProve.Dir & "<br/>")
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(4).Item(1) & ":</b> " & oProve.CP & "<br/>")
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(5).Item(1) & ":</b> " & oProve.Pob & "<br/>")
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(6).Item(1) & ":</b> " & oProve.PaiDen & "<br/>")
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(7).Item(1) & ":</b> " & oProve.ProviDen & "<br/>")
            sr.Append("<b>" & oDict.Data.Tables(0).Rows(8).Item(1) & ":</b> " & oProve.MonCod & " - " & DBNullToSomething(oProve.MonDen) & "<br/>")

            Return sr.ToString()
        End Function

#End Region

#Region "Servicio"
        ''' <summary>
        ''' Procedimiento que carga los datos del servicio
        ''' </summary>
        ''' <param name="servicio">Id del servicio</param>
        ''' <param name="campoId">Id del campo</param>
        ''' <param name="Instancia">Instancia</param>
        ''' <returns>JSonServicio</returns>
        ''' <remarks>Llamada desde:jsAlta.js/ ; Tiempo máximo: 0,2sg</remarks>
        <WebMethod(EnableSession:=True)>
        <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
        Public Function Obtener_Servicio(ByVal servicio As String, ByVal campoId As String, ByVal Instancia As Long, ByVal Peticionario As String, ByVal Empresa As Integer, ByVal Proveedor As String, ByVal Contacto As Integer,
                                         ByVal Moneda As String, ByVal FechaInicio As String, ByVal FechaExpiracion As String, ByVal Alerta As Integer, ByVal RepetirEmail As Integer, ByVal ImporteDesde As Double,
                                         ByVal ImporteHasta As Double) As JSonServicio
            Dim objJson As New JSonServicio
            Dim url As String
            Dim paramEnt As DataTable
            Dim paramSal As DataTable
            Dim i As Integer

            If HttpContext.Current.Session("FS_Portal_Server") Is Nothing Then
                '¿Como es posible?
                LoginDesdeAsp()
            End If

            Dim oFSUsuario As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim oFSServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)

            Dim oServicio As Fullstep.PMPortalServer.Servicio
            Dim oParam As Fullstep.PMPortalServer.ParametroServicio
            Dim paramsEnt As New Dictionary(Of String, Fullstep.PMPortalServer.ParametroServicio)
            Dim paramsSal As New Dictionary(Of String, Fullstep.PMPortalServer.ParametroServicio)
            Dim paramsCamposGenericos As New Dictionary(Of String, Fullstep.PMPortalServer.ParametroServicio)

            oServicio = oFSServer.Get_Object(GetType(Fullstep.PMPortalServer.Servicio))
            oServicio.LoadData(oFSUsuario.CiaComp, servicio, campoId, Instancia)
            url = oServicio.Data.Tables("URL").Rows(0).Item("URL")
            paramEnt = oServicio.Data.Tables("PARAM_ENTRADA")
            paramSal = oServicio.Data.Tables("PARAM_SALIDA")

            For i = 0 To paramEnt.Rows.Count - 1
                oParam = New Fullstep.PMPortalServer.ParametroServicio
                oParam.Den = paramEnt.Rows(i).Item("DEN")
                oParam.Directo = paramEnt.Rows(i).Item("DIRECTO")
                oParam.valorText = DBNullToStr(paramEnt.Rows(i).Item("VALOR_TEXT"))
                oParam.valorNum = DBNullToDbl(paramEnt.Rows(i).Item("VALOR_NUM"))
                oParam.valorFec = DateToigDate(DBNullToSomething(paramEnt.Rows(i).Item("VALOR_FEC")))
                oParam.valorBool = DBNullToInteger(paramEnt.Rows(i).Item("VALOR_BOOL"))
                oParam.Tipo = DBNullToInteger(paramEnt.Rows(i).Item("TIPO"))
                oParam.TipoCampo = DBNullToInteger(paramEnt.Rows(i).Item("TIPO_CAMPO"))
                oParam.EsSubcampo = DBNullToInteger(paramEnt.Rows(i).Item("ES_SUBCAMPO"))
                oParam.Campo = DBNullToInteger(paramEnt.Rows(i).Item("CAMPO"))
                paramsEnt(oParam.Den) = oParam
            Next

            For Each campoEntradaGenerico As Integer In {TipoCampo.Peticionario, TipoCampo.Moneda, TipoCampo.Proveedor, TipoCampo.Contacto,
                                                    TipoCampo.Empresa, TipoCampo.FechaInicio, TipoCampo.FechaExpiracion,
                                                    TipoCampo.MostrarAlerta, TipoCampo.EnviarMail, TipoCampo.ImporteDesde, TipoCampo.ImporteHasta}
                oParam = New Fullstep.PMPortalServer.ParametroServicio
                oParam.Den = ""
                oParam.Directo = 0
                oParam.TipoCampo = campoEntradaGenerico
                oParam.EsSubcampo = 0
                oParam.Campo = 0
                oParam.valorText = ""
                oParam.valorNum = 0
                oParam.valorFec = New Date()
                oParam.valorBool = False
                Select Case campoEntradaGenerico
                    Case TipoCampo.Peticionario
                        oParam.Den = "Peticionario"
                        oParam.Tipo = 6
                        oParam.valorText = If(oServicio.Data.Tables.Count > 3, oServicio.Data.Tables("PARAM_PETICIONARIO").Rows(0).Item("PETICIONARIO").ToString(), Peticionario)
                    Case TipoCampo.Moneda
                        oParam.Den = "Moneda"
                        oParam.Tipo = 6
                        oParam.valorText = If(oServicio.Data.Tables.Count > 4, oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("MON").ToString(), Moneda)
                    Case TipoCampo.Proveedor
                        oParam.Den = "Proveedor"
                        oParam.Tipo = 6
                        oParam.valorText = If(oServicio.Data.Tables.Count > 4, oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("PROVE").ToString(), Proveedor)
                    Case TipoCampo.Contacto
                        oParam.Den = "Contacto"
                        oParam.Tipo = 2
                        oParam.valorNum = If(oServicio.Data.Tables.Count > 4, DBNullToDbl(oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("CONTACTO")), Contacto)
                    Case TipoCampo.Empresa
                        oParam.Den = "Empresa"
                        oParam.Tipo = 6
                        oParam.valorNum = If(oServicio.Data.Tables.Count > 4, oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("EMPRESA").ToString(), Empresa)
                    Case TipoCampo.FechaInicio
                        oParam.Den = "FechaInicio"
                        oParam.Tipo = 3
                        oParam.valorFec = If(oServicio.Data.Tables.Count > 4, DBNullToSomething(oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("FECINICIO")), FechaInicio)
                    Case TipoCampo.FechaExpiracion
                        oParam.Den = "FechaExpiracion"
                        oParam.Tipo = 3
                        oParam.valorFec = If(oServicio.Data.Tables.Count > 4, DBNullToSomething(oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("FECFIN")), FechaExpiracion)
                    Case TipoCampo.MostrarAlerta
                        oParam.Den = "MostrarAlerta"
                        oParam.Tipo = 2
                        oParam.valorNum = If(oServicio.Data.Tables.Count > 4, DBNullToDbl(oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("ALERTA")), Alerta)
                    Case TipoCampo.EnviarMail
                        oParam.Den = "RepetirMail"
                        oParam.Tipo = 2
                        oParam.valorNum = If(oServicio.Data.Tables.Count > 4, DBNullToDbl(oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("REPETIR_EMAIL")), RepetirEmail)
                    Case TipoCampo.ImporteDesde
                        oParam.Den = "ImporteDesde"
                        oParam.Tipo = 2
                        oParam.valorNum = If(oServicio.Data.Tables.Count > 4, DBNullToDbl(oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("IMPORTE_DESDE")), ImporteDesde)
                    Case TipoCampo.ImporteHasta
                        oParam.Den = "ImporteHasta"
                        oParam.Tipo = 2
                        oParam.valorNum = If(oServicio.Data.Tables.Count > 4, DBNullToDbl(oServicio.Data.Tables("PARAMETROS_ENTRADA_GENERICOS").Rows(0).Item("IMPORTE_HASTA")), ImporteHasta)
                    Case Else
                        oParam.Den = ""
                        oParam.valorText = ""
                End Select
                paramsCamposGenericos(oParam.Den) = oParam
            Next

            For i = 0 To paramSal.Rows.Count - 1
                oParam = New Fullstep.PMPortalServer.ParametroServicio
                oParam.Den = paramSal.Rows(i).Item("DEN")
                oParam.IndError = paramSal.Rows(i).Item("INDICADOR_ERROR")
                oParam.Campo = paramSal.Rows(i).Item("CAMPO")
                paramsSal(oParam.Den) = oParam
            Next

            objJson.Url = url
            objJson.ParamsEntrada = paramsEnt
            objJson.ParamsSalida = paramsSal
            objJson.ParamsCamposGenericos = paramsCamposGenericos

            Return objJson
        End Function
        Public Class JSonServicio
            Private _url As String
            Public Property Url() As String
                Get
                    Return _url
                End Get
                Set(ByVal value As String)
                    _url = value
                End Set
            End Property
            Private _ParamsEntrada As Dictionary(Of String, Fullstep.PMPortalServer.ParametroServicio)
            Public Property ParamsEntrada() As Dictionary(Of String, Fullstep.PMPortalServer.ParametroServicio)
                Get
                    Return _ParamsEntrada
                End Get
                Set(ByVal value As Dictionary(Of String, Fullstep.PMPortalServer.ParametroServicio))
                    _ParamsEntrada = value
                End Set
            End Property
            Private _ParamsSalida As Dictionary(Of String, Fullstep.PMPortalServer.ParametroServicio)
            Public Property ParamsSalida() As Dictionary(Of String, Fullstep.PMPortalServer.ParametroServicio)
                Get
                    Return _ParamsSalida
                End Get
                Set(ByVal value As Dictionary(Of String, Fullstep.PMPortalServer.ParametroServicio))
                    _ParamsSalida = value
                End Set
            End Property
            Private _ParamsCamposGenericos As Dictionary(Of String, Fullstep.PMPortalServer.ParametroServicio)
            Public Property ParamsCamposGenericos() As Dictionary(Of String, Fullstep.PMPortalServer.ParametroServicio)
                Get
                    Return _ParamsCamposGenericos
                End Get
                Set(ByVal value As Dictionary(Of String, Fullstep.PMPortalServer.ParametroServicio))
                    _ParamsCamposGenericos = value
                End Set
            End Property
            Public Sub New()
            End Sub
        End Class
#End Region

#Region "Empresa"
        ''' <summary>
        ''' FunciÃ³n que se encarga de rellenar los datos de detalle de una empresa
        ''' </summary>
        ''' <param name="contextKey">ID empresa</param>
        ''' <returns>Un String con los datos de la empresa en formato html</returns>
        <System.Web.Services.WebMethod(True)>
        Public Function Obtener_DatosEmpresa(ByVal contextKey As System.String) As System.String
            Dim oFSUsuario As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim oFSServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

            Dim oEmpresa As Fullstep.PMPortalServer.Empresa
            oEmpresa = oFSServer.Get_Object(GetType(Fullstep.PMPortalServer.Empresa))
            oEmpresa.ID = contextKey

            oEmpresa.Load(lCiaComp, oFSUsuario.Idioma)

            Dim oDict As Fullstep.PMPortalServer.Dictionary = oFSServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DetalleEmpresa, oFSUsuario.Idioma)
            Dim sr As New StringBuilder()

            sr.Append("<table>")
            sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(0).Item(1) & ":</b></td><td>" & oEmpresa.Den & "</td></tr>")
            sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(1).Item(1) & ":</b></td><td>" & oEmpresa.NIF & "</td></tr>")
            sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(2).Item(1) & ":</b></td><td>" & oEmpresa.Dir & "</td></tr>")
            sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(3).Item(1) & ":</b></td><td>" & oEmpresa.CP & "</td></tr>")
            sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(4).Item(1) & ":</b></td><td>" & oEmpresa.Poblacion & "</td></tr>")
            sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(5).Item(1) & ":</b></td><td>" & oEmpresa.Pais & "</td></tr>")
            sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(6).Item(1) & ":</b></td><td>" & oEmpresa.Provincia & "</td></tr>")
            sr.Append("</table>")

            Return sr.ToString()
        End Function
        ''' <summary>Funcion que devuelve los valores de autocompletado de artículos</summary>
        ''' <param name="prefixText">texto ya introducido en la caja de texto</param>
        ''' <param name="count">nº de palabras que se devuelven</param>
        ''' <param name="contextKey">otros parámetros</param>
        ''' <returns>devuelve un array con los artículos que cumplen los filtros</returns>
        ''' <remarks>Llamada desde:GeneralEntry.vb</remarks>
        <System.Web.Services.WebMethod(True)>
        Public Function DevolverEmpresas(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
            Dim oFSUsuario As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim oFSServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

            Dim oEmpresas As Fullstep.PMPortalServer.Empresas
            oEmpresas = oFSServer.Get_Empresas

            Dim dtEmpresas As DataTable = oEmpresas.ObtenerEmpresas(lCiaComp, oFSUsuario.Idioma, Autocompletar:=prefixText)
            Dim lEmpresas As New List(Of String)
            If Not dtEmpresas Is Nothing AndAlso dtEmpresas.Rows.Count > 0 Then
                For Each oRow As DataRow In dtEmpresas.Rows
                    lEmpresas.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(oRow("NIF") & " - " & oRow("DEN"), oRow("ID")))
                Next
            End If

            Return lEmpresas.ToArray()
        End Function
#End Region

#Region "En Proceso"

        <System.Web.Services.WebMethod(True)>
        Public Function ComprobarEnProceso(ByVal contextKey As System.String) As System.String
            If HttpContext.Current.Session("FS_Portal_Server") Is Nothing Then
                LoginDesdeAsp()
            End If
            Dim oFSServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)
            Dim oInstancia As Fullstep.PMPortalServer.Instancia
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

            oInstancia = oFSServer.Get_Instancia
            oInstancia.ID = contextKey
            Return oInstancia.ComprobarEnProceso(lCiaComp)
        End Function

        <System.Web.Services.WebMethod(True)>
        Public Function LoginDesdeAsp()
            Dim FSPMServer As New Fullstep.PMPortalServer.Root
            Dim Username As String
            Dim oUser As Fullstep.PMPortalServer.User

            Dim IPDir As String = HttpContext.Current.Request.ServerVariables("LOCAL_ADDR").ToString
            Dim PersistID As String = Server.UrlDecode(HttpContext.Current.Request.Cookies("SESPP").Value)
            oUser = FSPMServer.Login(Server.UrlDecode(HttpContext.Current.Request.Cookies("USU_SESIONID").Value), IPDir, PersistID)

            If oUser Is Nothing Then
                Exit Function
            Else
                Username = oUser.Cod
                oUser.LoadUserData(Username, oUser.CiaComp)
                FSPMServer.Load_LongitudesDeCodigos(oUser.CiaComp)
                Session("FS_Portal_User") = oUser
                Session("FS_Portal_Server") = FSPMServer
                HttpContext.Current.User = Session("FS_Portal_User")
                Session("FS_Portal_User_ID") = Server.UrlDecode(HttpContext.Current.Request.Cookies("USU_SESIONID").Value)
                Session("FS_Portal_CiaComp") = oUser.CiaComp
                Exit Function
            End If
        End Function

        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Función que devuelve el texto indicando que la factura está en proceso.
        ''' </summary>
        ''' <param name="contextKey"></param>
        ''' <returns>Un String con el texto en el idioma del usuario</returns>
        <System.Web.Services.WebMethod(True)> _
        Public Function Obtener_TextoEnProceso(ByVal contextKey As System.String) As System.String
            Dim oFSUsuario As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim oFSServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)

            Dim oDict As Fullstep.PMPortalServer.Dictionary = oFSServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.PanelInfo, oFSUsuario.Idioma)
            Dim sr As New StringBuilder()

            sr.Append("&nbsp;&nbsp;&nbsp;" & oDict.Data.Tables(0).Rows(0).Item(1))

            Return sr.ToString()
        End Function
#End Region

#Region "WebDropdowns_Data"

        ''' Revisado por: sra. Fecha: 22/03/2013
        ''' <summary>
        ''' FunciÃ³n que devuelve un string formateado en modo JSON con los datos obtenidos
        ''' </summary>
        ''' <param name="contextKey">TipoGS y cod de pais, ... cuando son combos que dependen de otras. Ambos separados por tres arrobas</param>
        ''' <returns>String con las datos obtgenidos</returns>
        ''' <remarks>Llamada desde los webdropdown de campos del sistema. Tiempo mÃ¡ximo inferior a 0,5 seg.</remarks>
        <System.Web.Services.WebMethod(True)> _
        Public Function ObtenerDatos_DropDown(ByVal contextKey As System.String) As String
            Dim oFSUsuario As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim oFSServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

            Dim sResultado As String = String.Empty
            Dim dsResultado As DataSet = Nothing

            Dim sDatos As String() = Split(contextKey, "@@@")
            Dim iTipoGS As Integer = sDatos(1)

            Select Case iTipoGS
                Case TiposDeDatos.TipoCampoGS.Provincia
                    Dim sPaisCod As String = sDatos(0)
                    Dim oProvis As Fullstep.PMPortalServer.Provincias
                    oProvis = oFSServer.Get_Provincias
                    oProvis.Pais = DBNullToStr(sPaisCod)
                    oProvis.LoadData(oFSUsuario.Idioma.ToString(), lCiaComp)
                    dsResultado = oProvis.Data
                Case TiposDeDatos.TipoCampoGS.Almacen
                    Dim sAlmacenCod As String = sDatos(0)
                    Dim oAlmac As Fullstep.PMPortalServer.Almacenes
                    oAlmac = oFSServer.Get_Almacenes
                    If sAlmacenCod <> String.Empty Then
                        oAlmac.LoadData(lCiaComp, , sAlmacenCod)
                    Else
                        oAlmac.LoadData(lCiaComp)
                    End If
                    dsResultado = oAlmac.Data
                Case TiposDeDatos.TipoCampoGS.Centro
                    Dim sCentroCod As String = sDatos(0)
                    Dim oCentros As Fullstep.PMPortalServer.Centros
                    oCentros = oFSServer.Get_Centros
                    If sCentroCod <> String.Empty Then
                        oCentros.LoadData(lCiaComp, , sCentroCod)
                    Else
                        oCentros.LoadData(lCiaComp)
                    End If
                    dsResultado = oCentros.Data
                Case TiposDeDatos.TipoCampoGS.Departamento
                    Dim sDepartamentoCod As String = sDatos(0)
                    Dim oDepartamentos As Fullstep.PMPortalServer.Departamentos
                    oDepartamentos = oFSServer.Get_Departamentos
                    If sDepartamentoCod <> String.Empty Then
                        Dim vUnidadesOrganizativas(3) As String

                        vUnidadesOrganizativas = Split(sDepartamentoCod, " - ")
                        Select Case UBound(vUnidadesOrganizativas)
                            Case 0
                                If sDepartamentoCod <> "" Then oDepartamentos.LoadData(lCiaComp, 0)
                            Case 1
                                oDepartamentos.LoadData(lCiaComp, 1, vUnidadesOrganizativas(0))
                            Case 2
                                oDepartamentos.LoadData(lCiaComp, 2, vUnidadesOrganizativas(0), vUnidadesOrganizativas(1))
                            Case 3
                                oDepartamentos.LoadData(lCiaComp, 3, vUnidadesOrganizativas(0), vUnidadesOrganizativas(1), vUnidadesOrganizativas(2))
                        End Select
                    Else ''Mostrar todas los departamentos
                        oDepartamentos.LoadData(lCiaComp)
                    End If
                    dsResultado = oDepartamentos.Data
                Case TiposDeDatos.TipoCampoGS.Moneda
                    Dim oMonedas As Fullstep.PMPortalServer.Monedas
                    oMonedas = oFSServer.Get_Monedas
                    oMonedas.LoadData(oFSUsuario.Idioma.ToString(), lCiaComp)
                    dsResultado = oMonedas.Data
                Case TiposDeDatos.TipoCampoGS.FormaPago
                    Dim oFormasPago As Fullstep.PMPortalServer.FormasPago
                    oFormasPago = oFSServer.Get_FormasPago
                    oFormasPago.LoadData(oFSUsuario.Idioma.ToString(), lCiaComp)
                    dsResultado = oFormasPago.Data
                Case TiposDeDatos.TipoCampoGS.Unidad
                    Dim oUnidades As Fullstep.PMPortalServer.Unidades
                    oUnidades = oFSServer.get_Unidades
                    oUnidades.LoadData(oFSUsuario.Idioma.ToString(), lCiaComp)
                    dsResultado = oUnidades.Data
                Case TiposDeDatos.TipoCampoGS.OrganizacionCompras
                    Dim oOrganizacionesCompras As Fullstep.PMPortalServer.OrganizacionesCompras
                    oOrganizacionesCompras = oFSServer.Get_OrganizacionesCompras
                    oOrganizacionesCompras.LoadData(lCiaComp)
                    dsResultado = oOrganizacionesCompras.Data
                Case TiposDeDatos.TipoCampoGS.Pais
                    Dim oPaises As Fullstep.PMPortalServer.Paises
                    oPaises = oFSServer.Get_Paises
                    oPaises.LoadData(oFSUsuario.Idioma.ToString(), lCiaComp)
                    dsResultado = oPaises.Data
                Case TiposDeDatos.TipoCampoGS.Dest
                    Dim oDests As Fullstep.PMPortalServer.Destinos
                    oDests = oFSServer.Get_Destinos
                    oDests.LoadData(oFSUsuario.Idioma.ToString(), lCiaComp, oFSUsuario.AprobadorActual)
                    dsResultado = oDests.Data
                Case TiposDeDatos.TipoCampoGS.ProveedorERP
                    Dim sDatosProveERP As String() = Split(sDatos(0), "###")
                    Dim sProve As String = sDatosProveERP(0)
                    Dim sOrgCompras As String = sDatosProveERP(1)
                    Dim oProvesERP As Fullstep.PMPortalServer.ProveedoresERP
                    oProvesERP = oFSServer.Get_ProveedoresERP
                    dsResultado = oProvesERP.CargarProveedoresERPtoDS(lCiaComp, sProve, sOrgCompras)
            End Select

            For Each oRow As DataRow In dsResultado.Tables(0).Rows
                If oRow("COD") Is System.DBNull.Value AndAlso oRow("DEN") Is System.DBNull.Value AndAlso dsResultado.Tables(0).Rows.Count = 1 Then
                    sResultado = sResultado & "{""Valor"":"""",""Texto"":""""},"
                Else
                    If Not (oRow("COD") Is System.DBNull.Value AndAlso oRow("DEN") Is System.DBNull.Value) Then
                        If iTipoGS = TiposDeDatos.TipoCampoGS.Almacen Then
                            sResultado = sResultado & "{""Valor"":""" & HttpUtility.JavaScriptStringEncode(oRow("ID"))
                        Else
                            sResultado = sResultado & "{""Valor"":""" & HttpUtility.JavaScriptStringEncode(oRow("COD"))
                        End If
                        sResultado = sResultado & """,""Texto"":""" & HttpUtility.JavaScriptStringEncode(oRow("COD")) & " - " & HttpUtility.JavaScriptStringEncode(oRow("DEN"))
                        If iTipoGS = TiposDeDatos.TipoCampoGS.Dest Then
                            sResultado = sResultado & HttpUtility.JavaScriptStringEncode(If(IsDBNull(oRow("POB")), "", " (" & oRow("POB") & ")"))
                        End If
                        sResultado = sResultado & """},"
                    End If
                End If
            Next

            If Len(sResultado) > 0 Then
                sResultado = Left(sResultado, Len(sResultado) - 1)
            End If
            Return sResultado
        End Function


        ''' Revisado por: blp. Fecha: 04/10/2011
        ''' <summary>
        ''' Función que devuelve un string formateado en modo JSON con la lista seleccionada
        ''' </summary>
        ''' <param name="contextKey">Id de la lista y valor seleccionado en el campo padre</param>
        ''' <returns>String con la lista y nombre del campo a usar en el template para construir luego el combo a partir del json. Ambos separados por tres arrobas</returns>
        ''' <remarks>Llamada desde los webdropdown de almacenes. Tiempo máximo inferior a 1 seg.</remarks>
        <System.Web.Services.WebMethod(True)>
        Public Function Obtener_Lista(ByVal contextKey As System.String) As String
            Dim oFSUsuario As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim oFSServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")
            Dim arrAux() As String
            Dim iOrden As Integer = 0
            Dim lIdCampo As Long = 0
            Dim lInstancia As Long = 0
            Dim lBloque As Long = 0
            Dim lSolicitud As Long = 0
            Dim oCampo As Object
            Dim sResultado As String = String.Empty
            Dim sDatos As String() = Split(contextKey, "@@@")
            Dim sLista As String = sDatos(0)
            Dim sTemplate As String = sDatos(1)

            arrAux = Split(CType(contextKey, String), "#")

            If IsNumeric(arrAux(0)) Then
                lIdCampo = CLng(arrAux(0))
            End If

            If IsNumeric(arrAux(1)) Then
                iOrden = CInt(arrAux(1))
            End If

            If IsNumeric(arrAux(2)) Then
                lInstancia = CInt(arrAux(2))
            End If

            If IsNumeric(arrAux(3)) Then
                lBloque = CLng(arrAux(3))
            End If

            If IsNumeric(arrAux(4)) Then
                lSolicitud = CLng(arrAux(4))
            End If

            oCampo = oFSServer.Get_Campo
            'lIdCampo --> Id del campo del que queremos mostrar los datos (Lista hija)
            'iOrdenPadre --> Orden del indice de la lista de la lista padre

            Dim dsLista As DataSet = oCampo.DevolverListaEnlazada(lCiaComp, lIdCampo, iOrden, oFSUsuario.Idioma, lInstancia, lBloque, lSolicitud, oFSUsuario.CodProveGS)
            For Each oRow As DataRow In dsLista.Tables(0).Rows
                If oRow("ORDEN") Is System.DBNull.Value AndAlso oRow("DEN") Is System.DBNull.Value AndAlso dsLista.Tables(0).Rows.Count = 1 Then
                    sResultado = sResultado & "{""Valor"":"""",""Texto"":""""},"
                ElseIf oRow("ORDEN") = 0 AndAlso oRow("DEN") = String.Empty Then
                    'No vamos a mostrar el primer valor cuando esté vacío para que no se vea "0 - " en el desplegable...
                Else
                    If Not (oRow("ORDEN") Is System.DBNull.Value AndAlso oRow("DEN") Is System.DBNull.Value) Then _
                        sResultado = sResultado & "{""Valor"":""" & oRow("ORDEN") & """,""Texto"":""" & HttpUtility.JavaScriptStringEncode(oRow("DEN")) & """},"
                End If
            Next

            oCampo = Nothing

            If Len(sResultado) > 0 Then
                sResultado = Left(sResultado, Len(sResultado) - 1)
            End If
            Return sResultado
        End Function
        ''' <summary>FunciÃ³n que devuelve un string formateado en modo JSON con la lista seleccionada</summary>
        ''' <param name="contextKey">Id de la lista y valor seleccionado en el campo padre: IdCampo#GMNs@@@TipoGS</param>
        ''' <returns>String con la lista y nombre del campo a usar en el template para construir luego el combo a partir del json. Ambos separados por tres arrobas</returns>    
        <System.Web.Services.WebMethod(True)>
        Public Function Obtener_Lista_CampoPadre_Material(ByVal contextKey As System.String)
            Dim lIdCampo As Long = 0
            Dim sResultado As String = String.Empty

            Dim FSNServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)
            Dim FSNUser As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")

            Dim sDatos As String() = Split(contextKey, "@@@")
            Dim arrAux As String() = sDatos(0).Split("#")

            If IsNumeric(arrAux(0)) Then lIdCampo = CLng(arrAux(0))
            If arrAux(1) <> String.Empty Then
                Dim sGMN1 As String = String.Empty
                Dim sGMN2 As String = String.Empty
                Dim sGMN3 As String = String.Empty
                Dim sGMN4 As String = String.Empty

                Dim sMat = arrAux(1)
                Dim i As Integer = 1
                While sMat <> ""
                    Select Case i
                        Case 1
                            sGMN1 = sMat.Substring(0, FSNServer.LongitudesDeCodigos.giLongCodGMN1)
                            sMat = sMat.Substring(FSNServer.LongitudesDeCodigos.giLongCodGMN1)
                        Case 2
                            sGMN2 = sMat.Substring(0, FSNServer.LongitudesDeCodigos.giLongCodGMN2)
                            sMat = sMat.Substring(FSNServer.LongitudesDeCodigos.giLongCodGMN2)
                        Case 3
                            sGMN3 = sMat.Substring(0, FSNServer.LongitudesDeCodigos.giLongCodGMN3)
                            sMat = sMat.Substring(FSNServer.LongitudesDeCodigos.giLongCodGMN3)
                        Case 4
                            sGMN4 = sMat.Substring(0, FSNServer.LongitudesDeCodigos.giLongCodGMN4)
                            sMat = sMat.Substring(FSNServer.LongitudesDeCodigos.giLongCodGMN4)
                    End Select

                    i += 1
                End While

                Dim oCampo As Object = FSNServer.Get_Campo
                Dim dsLista As DataSet = oCampo.DevolverListaCampoPadreMaterial(lCiaComp, lIdCampo, sGMN1, sGMN2, sGMN3, sGMN4, FSNUser.Idioma)

                For Each oRow As DataRow In dsLista.Tables(0).Rows
                    If oRow("ORDEN") Is System.DBNull.Value AndAlso oRow("DEN") Is System.DBNull.Value AndAlso dsLista.Tables(0).Rows.Count = 1 Then
                        sResultado = sResultado & "{""Valor"":"""",""Texto"":""""},"
                    ElseIf oRow("ORDEN") = 0 AndAlso oRow("DEN") = String.Empty Then
                        'No vamos a mostrar el primer valor cuando estÃ© vacÃ­o para que no se vea "0 - " en el desplegable...
                    Else
                        If Not (oRow("ORDEN") Is System.DBNull.Value AndAlso oRow("DEN") Is System.DBNull.Value) Then
                            sResultado = sResultado & "{""Valor"":""" & oRow("ORDEN") & """,""Texto"":""" & HttpUtility.JavaScriptStringEncode(If(IsDBNull(oRow("DEN")), "", oRow("DEN"))) & """},"
                        End If
                    End If
                Next
            End If

            If Len(sResultado) > 0 Then sResultado = Left(sResultado, Len(sResultado) - 1)
            Return sResultado
        End Function
#End Region


#Region "Detalle de pedido"
        ''' <summary>
        ''' Función que se encarga de rellenar los datos de detalle de un pedido
        ''' </summary>
        ''' <param name="contextKey">ID de pedido</param>
        ''' <returns>Un String con los datos de un pedido en formato html</returns>
        <System.Web.Services.WebMethod(True)> _
        Public Function Obtener_DatosDetallePedido(ByVal contextKey As System.String) As System.String
            Dim oFSUsuario As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim oFSServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)
            Dim lCiaComp As Long = HttpContext.Current.Session("FS_Portal_CiaComp")
            Dim sParam() As String
            sParam = Split(contextKey, "#")

            Dim oPedido As Fullstep.PMPortalServer.Pedido
            oPedido = oFSServer.Get_Object(GetType(Fullstep.PMPortalServer.Pedido))
            oPedido.ID = sParam(0)
            oPedido.LoadDetalle(Session("FS_Portal_User").CiaComp, oFSUsuario.Idioma)

            Dim oDict As Fullstep.PMPortalServer.Dictionary = oFSServer.Get_Dictionary()
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.DetallePedido, oFSUsuario.Idioma)

            Dim sDenEstado As String = String.Empty
            Select Case oPedido.Estado
                Case TipoEstadoOrdenEntrega.PendienteDeAprobacion
                    sDenEstado = oDict.Data.Tables(0).Rows(0).Item(1)
                Case TipoEstadoOrdenEntrega.DenegadoParcialAprob
                    sDenEstado = oDict.Data.Tables(0).Rows(1).Item(1)
                Case TipoEstadoOrdenEntrega.EmitidoAlProveedor
                    sDenEstado = oDict.Data.Tables(0).Rows(2).Item(1)
                Case TipoEstadoOrdenEntrega.AceptadoPorProveedor
                    sDenEstado = oDict.Data.Tables(0).Rows(3).Item(1)
                Case TipoEstadoOrdenEntrega.EnCamino
                    sDenEstado = oDict.Data.Tables(0).Rows(4).Item(1)
                Case TipoEstadoOrdenEntrega.EnRecepcion
                    sDenEstado = oDict.Data.Tables(0).Rows(5).Item(1)
                Case TipoEstadoOrdenEntrega.RecibidoYCerrado
                    sDenEstado = oDict.Data.Tables(0).Rows(6).Item(1)
                Case TipoEstadoOrdenEntrega.Anulado
                    sDenEstado = oDict.Data.Tables(0).Rows(7).Item(1)
                Case TipoEstadoOrdenEntrega.RechazadoPorProveedor
                    sDenEstado = oDict.Data.Tables(0).Rows(8).Item(1)
                Case TipoEstadoOrdenEntrega.DenegadoTotalAprobador
                    sDenEstado = oDict.Data.Tables(0).Rows(9).Item(1)
            End Select

            Dim sr As New StringBuilder()
            sr.Append("<table cellpadding=3>")
            sr.Append("<tr><td colspan=5 align=center><span class=RotuloGrande>" & oPedido.NumPedidoCompleto & "</span></td></tr>")
            sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(10).Item(1) & "</b></td><td>" & oPedido.Peticionario & "</td></tr>")
            sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(11).Item(1) & ":</b></td><td>" & FormatDate(oPedido.Fecha, oFSUsuario.DateFormat) & "</td></tr>")
            sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(12).Item(1) & ":</b></td><td>" & sDenEstado & "</td></tr>")
            sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(13).Item(1) & ":</b></td><td>" & oPedido.DenTipoPedido & "</td></tr>")
            sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(14).Item(1) & ":</b></td><td>" & oPedido.NumPedErp & "</td></tr>")
            sr.Append("<tr><td><b>" & oDict.Data.Tables(0).Rows(15).Item(1) & ":</b></td><td>" & sParam(1).ToString.PadLeft(3, "0") & "</td></tr>")
            sr.Append("</table>")

            Return sr.ToString()
        End Function
#End Region

#Region "Lineas Asociadas"
        ' FunciÃ³n que comprueba si una linea estÃ¡ asociada a un item o linea de pedido
        <System.Web.Services.WebMethod(True)> _
        Public Function Comprobar_Lineas(ByVal idDesglose As String, ByVal numLinea As String) As System.String
            Dim oFSUsuario As Fullstep.PMPortalServer.User = CType(HttpContext.Current.Session("FS_Portal_User"), Fullstep.PMPortalServer.User)
            Dim oFSServer As Fullstep.PMPortalServer.Root = CType(HttpContext.Current.Session("FS_Portal_Server"), Fullstep.PMPortalServer.Root)
            Dim oInstancia As Fullstep.PMPortalServer.Instancia = oFSServer.Get_Object(GetType(Fullstep.PMPortalServer.Instancia))
            Dim oDict As Fullstep.PMPortalServer.Dictionary = oFSServer.Get_Dictionary()
            Dim sIdi As String = oFSUsuario.Idioma
            oDict.LoadData(Fullstep.PMPortalServer.TiposDeDatos.ModulosIdiomas.RealizarAccion, sIdi)

            Dim result As Boolean
            Dim mensaje As String = ""

            result = oInstancia.ComprobarLineasAsociadas(Session("FS_Portal_User").CiaComp, idDesglose, numLinea)
            If result = True Then
                mensaje = oDict.Data.Tables(0).Rows(17).Item(1)
            End If
            Return mensaje

        End Function
#End Region
    End Class

    Public Class RespuestaJSon
        Public ExisteCentroCoste As Boolean
        Public DenCentroCoste As String
        Public CodCentroCoste As String
    End Class
End Namespace