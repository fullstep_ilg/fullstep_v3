﻿/*
Numeric Formatted Plug-in for JQuery

Formatea los valores numéricos segun las opciones de formato definidas sin que ello afecte al valor numérico subyacente.
Al aceptar el foco dicho formato se convierte a modo edición, en este modo al escribir los caracteres (,) o (.) se consideran como el separador decimal definido.
Al salir del foco el formato se convierte nuevamente al definido (modo lectura).
Este plug-in no realiza validaciones de tipo numérico, utilizar siempre en conjunción con el plugin jquery.numeric().

Opciones de inicialización:
    - decimalSeparator : Separador decimal.
    - decimalDigits : Número de decimales.
    - groupSeparator : Separador de grupo (miles).
    - groupSizes : Array que define los tamaños de los grupos de derecha a izquierda (la última posición establecerá el tamaño indefinidamente).
    - alignment: Alineación del valor en el input (por defecto derecha).
    - prefix: Prefijo al valor mostrado.
    - suffix: Sufijo al valor mostrado (útil para mostrar moneda, porcentajes u otras unidades).

Métodos:
    - val : Consultar o establecer el valor numérico subyacente. Se debe utilizar siempre este método el lugar de <input>.val().
            Para consultar el valor numérico subyacente : var valor = <input>.numericFormatted('val');
            Para establecer el valor numérico subyacente : <input>.numericFormatted('val', <valor>);

    - formattedVal : Devuelve el valor formateado : var valor = <input>.numericFormatted('formattedVal');

Fullstep Networks 2014 (www.fullstep.com)
Version 1.0.0 (26/09/2014)
*/
(function ($) {

    //Valores por defecto
    var defaults = {
        decimalSeparator: ',',
        decimalDigits: 2,
        groupSeparator: '.',
        groupSizes: [3],
        alignment: 'right',
        prefix: '',
        suffix: ''
    };

    //Métodos propios del plug-in
    var methods = {
        init: function (options) {
            //Aplicamos las opciones especificadas
            var settings = $.extend({}, defaults, options);

            //Alineamos a la derecha si se requiere
            if (settings.alignment.toLowerCase().trim() == "right")
                $(this).css('text-align', 'right');

            //Guardamos las settings en el objeto
            $(this).data('numericFormatted.settings', settings);
        },
        val: function (value) {
            //Consultamos o establecemos el valor numérico
            if (arguments.length == 0) {
                //Devolvemos el valor
                return $(this).data("numericFormatted.rawValue");
            } else {
                //Establecemos el valor
                if (value == undefined)
                    $(this).removeData("numericFormatted.rawValue");
                else if (typeof value == 'number')
                    $(this).data("numericFormatted.rawValue", value);
                else return;

                //Cargamos las opciones
                var settings = $(this).data('numericFormatted.settings');

                //Mostramos el valor con formato de lectura
                $(this).val(getViewFormat(value, settings));
            }
        },
        formattedVal: function () {
            //Cargamos las opciones
            var settings = $(this).data('numericFormatted.settings');

            //Consultamos el valor en formato lectura                        
            return getViewFormat($(this).data("numericFormatted.rawValue"), settings);
        }
    };

    //Devuelve el mismo string en orden inverso
    function reverseStr(str) {
        return str.split('').reverse().join('');
    };

    //Establece el texto como formato de lectura
    function getViewFormat(rawValue, settings) {
        if (typeof rawValue !== 'number') return "";

        //Componemos el formato de lectura
        var formattedValue = rawValue.toFixed(settings.decimalDigits).replace(".", settings.decimalSeparator);
        var arrValue = formattedValue.split(settings.decimalSeparator);
        var intPart = arrValue[0];
        var formattedIntPart = "";
        $.each(settings.groupSizes, function (i, el) {
            //Si es el último valor los dígitos se agrupan por este tamaño indefinidamente					                                
            if (i == settings.groupSizes.length - 1) {
                formattedIntPart = jQuery.map(reverseStr(intPart).match(new RegExp(".{1," + el + "}", "g")), function (x) { return reverseStr(x); }).reverse().join(settings.groupSeparator)
                                    + (formattedIntPart.length > 0 ? settings.groupSeparator : "")
                                    + formattedIntPart;
            } else {
                //Añadimos el tramo al valor formateado
                formattedIntPart = intPart.slice(-el)
                                    + (formattedIntPart.length > 0 ? settings.groupSeparator : "")
                                    + formattedIntPart;
                //Cortamos la parte procesada
                intPart = intPart.substring(0, intPart.length - el);
            }
        });

        //Unimos la parte entera con la decimal
        if (arrValue.length > 1) formattedValue = formattedIntPart + settings.decimalSeparator + arrValue[1];

        //Añadimos prefijo y sufijo
        if (settings.prefix != '') formattedValue = settings.prefix + " " + formattedValue;
        if (settings.suffix != '') formattedValue += (" " + settings.suffix);

        return formattedValue;
    };

    //Establece el texto como formato de edicion
    function getEditFormat(rawValue, settings) {
        if (typeof rawValue !== 'number') return "";

        return rawValue.toString().replace(".", settings.decimalSeparator);
    };

    //Obtiene el valor numérico en base al formato de edición
    function getRawValue(editFormat, settings) {
        if (editFormat.length == 0) return undefined;

        return parseFloat(editFormat.replace(settings.decimalSeparator, "."));
    };

    //Definición del plug-in
    $.fn.numericFormatted = function (methodOrOptions) {
        //Dependiendo del parámetro establecemos opciones o llamamos a métodos propios                                
        if (methods[methodOrOptions]) {
            //Salimos si el selector de jquery no tiene elementos
            if ($(this).length == 0) return;
            //Llamada a un método del plug-in
            return methods[methodOrOptions].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
            //Aplicamos opciones (mediante el método "init")
            methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + methodOrOptions + ' does not exist on jQuery.decimalFormatted');
        }

        //Devolvemos todo el objeto
        return this.each(function () {
            var input = $(this);

            //Cargamos las opciones
            var settings = input.data('numericFormatted.settings');

            //Enlazamos los eventos                    
            if (!input.attr("readonly"))
                input
					.off("focus.decimalFormatted").on("focus.decimalFormatted", function () {
					    //Mostramos el valor en formato de edición
					    input.val(getEditFormat(input.data("numericFormatted.rawValue"), settings));
					    //Seleccionamos todo el texto
					    input.select().one('mouseup', function (e) { e.preventDefault(); });
					})
					.off("blur.decimalFormatted").on("blur.decimalFormatted", function () {
					    var rawValue = getRawValue(input.val(), settings);
					    //Almacenamos el valor numérico
					    if (rawValue === undefined) input.removeData("numericFormatted.rawValue")
					    else input.data("numericFormatted.rawValue", rawValue);

					    //Mostramos el valor en formato de lectura
					    input.val(getViewFormat(rawValue, settings));
					})
                    .off("keypress.decimalFormatted").on("keypress.decimalFormatted", function (e) {
                        //Reemplazamos los puntos y comas por el separador decimal
                        // '46' is the keyCode for '.'
                        // '44' is the keyCode for ','                     
                        if (e.keyCode == '46' || e.charCode == '46' || e.keyCode == '44' || e.charCode == '44') {
                            //Cancel the keypress
                            e.preventDefault();

                            //Si no debe haber decimales, no permitimos escribir el separador decimal
                            if (settings.decimalDigits == 0) return;

                            //Obtenemos el texto
                            var text = input.val();

                            //Si ya existe el separador decimal en el valor, salimos
                            if (text.indexOf(settings.decimalSeparator) != -1) return;

                            //(las funciones getSelectionStart, getSelectionEnd y setSelection son del plugin jquery.numeric())

                            //Memorizamos la posicion del cursor
                            var position = $.fn.getSelectionStart(this);

                            //Insertamos el separador decimal donde esté el cursor                           
                            input.val([text.slice(0, position), settings.decimalSeparator, text.slice($.fn.getSelectionEnd(this))].join(''));

                            //Posicionamos el cursor donde estaba + 1
                            $.fn.setSelection(this, position + 1, position + 1)
                        }
                    });
        });
    };

})(jQuery);