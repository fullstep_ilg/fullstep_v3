﻿Date.prototype.getCompleteDate = function () {
    var fechaCompleta;
    $.when($.ajax({
        type: "POST",
        url: rutanormal + 'script/cn/cnMuroUsuario.asmx/FechaEvento_Completa',
        data: JSON.stringify({ anio: this.getFullYear(), mes: this.getMonth()+1, dia: this.getDate(), hora: this.getHours(), minuto: this.getMinutes(), gmt: this.getTimezoneOffset() }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        fechaCompleta = msg.d;
    });
    return fechaCompleta;
}

Date.prototype.UsuShortPattern = function () {
    var yyyy = this.getFullYear().toString();
    var mm = ("0" + (this.getMonth() + 1)).slice(-2).toString(); // getMonth() is zero-based         
    var dd = ("0" + this.getDate()).slice(-2).toString();

    return UsuMask.replace('dd', dd).replace('MM', mm).replace('yyyy', yyyy);
};

Date.prototype.getEventMonthNameAbr = function () {
    var mes;
    $.when($.ajax({
        type: "POST",
        url: rutanormal + 'script/cn/cnMuroUsuario.asmx/NombreMesAbr',
        data: JSON.stringify({ mes: this.getMonth()+1 }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        mes = msg.d;
    });
    return mes;
}

function relativeTime(time) {
    var period = new Date(time);
    var delta = new Date() - period;
    var TextosTiempoRelativo = top.frames['fraCN'].TextosTiempoRelativo;
    var unidadesTiempo = top.frames['fraCN'].unidadesTiempo;
    if (delta <= 10000) {	// Less than 10 seconds ago
        return TextosTiempoRelativo[0];
    }

    var units = 0;

    var conversions = {
        1: 1000, 	// ms -> sec
        2: 60, 		// sec -> min
        3: 60, 		// min -> hour
        4: 24, 		// hour -> day
        5: 30, 		// day -> month (roughly)
        6: 12			// month -> year
    };

    for (var key in conversions) {
        if (delta < conversions[key]) {
            break;
        }
        else {
            units = parseInt(key);
            delta = delta / conversions[key];
        }
    }

    // Pluralize if necessary:
    delta = Math.floor(delta);
    if (delta !== 1) { units += 10 };

    return TextosTiempoRelativo[1].replace('###', [delta, unidadesTiempo[units]].join(' '));
}

function IsValidDate(Day, Mn, Yr) {
    var DateVal = Mn + "/" + Day + "/" + Yr;
    var dt = new Date(DateVal);

    if (dt.getDate() != Day) {
        return false;
    }
    else if (dt.getMonth() != Mn - 1) {
        //this is for the purpose JavaScript starts the month from 0
        return false;
    }
    else if (dt.getFullYear() != Yr) {
        return false;
    }

    return true;
}

/* TECLAS POSIBLES PULSADAS PARA SU POSIBLE TRATAMIENTO */
var KEY = {
    UP: 38,
    DOWN: 40,
    DEL: 46,
    TAB: 9,
    RETURN: 13,
    ESC: 27,
    COMMA: 188,
    PAGEUP: 33,
    PAGEDOWN: 34,
    BACKSPACE: 8
};

function CentrarPopUpMain(popup) {
    var punto = CentrarPopUpContenedor(window.parent.frames['fraPMPortalMain'] ? window.parent.frames['fraPMPortalMain'] : window.parent.frames['default_main'], popup.outerWidth(true), popup.outerHeight(true));
    popup.css('top', punto.y);
    popup.css('left', punto.x);
}
function CentrarPopUpMainAncho(popup) {
    var punto = CentrarPopUpContenedor(window.parent.frames['fraPMPortalMain'] ? window.parent.frames['fraPMPortalMain'] : window.parent.frames['default_main'], popup.outerWidth(true), popup.outerHeight(true));
    popup.css('left', punto.x);
}

function CentrarPopUp(popup) {
    var punto = window.center({ width: popup.outerWidth(true), height: popup.outerHeight(true) });
    popup.css('top', punto.y);
    popup.css('left', punto.x);
}

/*Función que calcula el centro de la ventana actual o de un rectángulo de
* las dimensiones que se le pasen, devuelve el vértice superior izquierdo*/
function CentrarPopUpContenedor(content, width, height) {
    var hWnd = { width: width, height: height };
    var _x = 0;
    var _y = 0;
    var offsetX = 0;
    var offsetY = 0;
    //IE
    if (!content.pageYOffset) {
        //strict mode
        if (!(content.document.documentElement.scrollTop == 0)) {
            offsetY = content.document.documentElement.scrollTop;
            offsetX = content.document.documentElement.scrollLeft;
        }
        //quirks mode
        else {
            offsetY = $('html', document).scrollTop();
            offsetX = $('html', document).scrollLeft();
        }
    }
    //w3c
    else {
        offsetX = content.pageXOffset;
        offsetY = content.pageYOffset;
    }
    _x = ((SizeCentrarPopUp(content).width - hWnd.width) / 2) + offsetX;
    _y = ((SizeCentrarPopUp(content).height - hWnd.height) / 2) + offsetY;
    return { x: _x, y: _y };
}

window.center = function () {
    var hWnd = (arguments[0] != null) ? arguments[0] : { width: 0, height: 0 };
    var _x = 0;
    var _y = 0;
    var offsetX = 0;
    var offsetY = 0;
    //IE
    if (!window.pageYOffset) {
        //strict mode
        if (!(document.documentElement.scrollTop == 0)) {
            offsetY = document.documentElement.scrollTop;
            offsetX = document.documentElement.scrollLeft;
        }
        //quirks mode
        else {
            offsetY = document.body.scrollTop;
            offsetX = document.body.scrollLeft;
        }
    }
    //w3c
    else {
        offsetX = window.pageXOffset;
        offsetY = window.pageYOffset;
    }
    _x = ((this.size().width - hWnd.width) / 2) + offsetX;
    _y = ((this.size().height - hWnd.height) / 2) + offsetY;
    return { x: _x, y: _y };
}

function SizeCentrarPopUp(content) {
    var w = 0;
    var h = 0;
    //IE
    if (!content.innerWidth) {
        //strict mode
        if (!(content.document.documentElement.clientWidth == 0)) {
            w = content.document.documentElement.clientWidth;
            h = content.document.documentElement.clientHeight;
        }
        //quirks mode
        else {
            w = content.document.body.clientWidth;
            h = content.document.body.clientHeight;
        }
    }
    //w3c
    else {
        w = content.innerWidth;
        h = content.innerHeight;
    }
    return { width: w, height: h };
}
/**
* Función que calcula el tamaño de la ventana
*
*/
window.size = function () {
    var w = 0;
    var h = 0;
    //IE
    if (!window.innerWidth) {
        //strict mode
        if (!(document.documentElement.clientWidth == 0)) {
            w = document.documentElement.clientWidth;
            h = document.documentElement.clientHeight;
        }
        //quirks mode
        else {
            w = document.body.clientWidth;
            h = document.body.clientHeight;
        }
    }
    //w3c
    else {
        w = window.innerWidth;
        h = window.innerHeight;
    }
    return { width: w, height: h };
}
function SCAYTready(instance) {
    var usuario = top.frames['fraCN'].user;
    if (CKEDITOR.plugins.scayt.isScaytReady(CKEDITOR.instances[instance])) {
        var lang;
        switch (usuario.Idioma_CodigoUniversal) {
            case 'es':
                lang = 'es_ES';
                break;
            case 'en':
                lang = 'en_US';
                break;
            case 'de':
                lang = 'de_DE';
                break;
            default:
                lang = 'es_ES';
                break;
        }
        CKEDITOR.plugins.scayt.instances[instance].setLang(lang);
        clearInterval(scayt_READY);
    }
}