﻿var CKEDITOR_BASEPATH = rutanormal + 'ckeditor/';
var doc;
var user;
$(document).ready(function () {
    $.getScript(rutanormal + 'js/json2.js', function () {
        Cargar_Opciones_CN();
    });
    $('#lnkNotificacionesCN', top.frames['fraTop'].document).live('click', function () {
        var comp = $('.colaboracion', top.frames['default_main'].document)[0];
        try { //in firefox
            comp.click();
            return;
        } catch (ex) { }
        try { // in chrome
            if (document.createEvent) {
                var e = document.createEvent('MouseEvents');
                e.initEvent('click', true, true);
                comp.dispatchEvent(e);
                return;
            }
        } catch (ex) { }
        try { // in IE
            if (document.createEventObject) {
                var evObj = document.createEventObject();
                comp.fireEvent("onclick", evObj);
                return;
            }
        } catch (ex) { }
    });
});

function Cargar_Opciones_CN() {
    $.when($.ajax({
        type: "POST",
        url: rutanormal + 'script/_common/services/User.asmx/Obtener_Usuario_CN',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
    })).done(function (msg) {
        user = msg.d;
        if (user.AccesoCN) {
            $.getScript(rutanormal + 'js/jquery/plugins/autogrow.js');
            $.getScript(rutanormal + 'js/jquery/plugins/jquery.iframe-transport.js');
            $.getScript(rutanormal + 'js/jquery/plugins/video.js', function () {
                VideoJS.setupAllWhenReady();
            });
            $.getScript(rutanormal + 'js/jsUtilities.js').done(function () {
                VerificarNotificacionesCN(false);
                if (segundosNotificacionesCN !== 0) {                    
                    setInterval(function () { VerificarNotificacionesCN(true) }, segundosNotificacionesCN);
                }
            });
        }
    });
}

function VerificarNotificacionesCN(mostrarDivNotificaciones) {
    if (typeof(top.frames['default_main'].document.frames)=='undefined' || top.frames['default_main'].document.frames.length == 0) {
        doc = top.frames['default_main'].document;
    } else {
        doc = top.frames['default_main'].document.frames[0].document;        
    }
    $.when($.ajax({
        type: "POST",
        url: rutanormal + 'script/cn/services/cnMuroUsuario.asmx/Comprobar_Notificaciones',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true
    })).done(function (msg) {
        var Notificaciones = 0;
        $.each(msg.d, function () {
            switch (this.value) {
                case "0":
                    Notificaciones = parseInt(this.text);
                    break;
                case "5":                    
                    if (!$('#popupMensajeUrgente', doc).is(':visible')) {
                        var idMensaje = parseInt(this.text);
                        $.get(rutanormal + 'script/cn/html/_mensajes.tmpl.htm', function (mensajes) {
                            $('body').append(mensajes);
                            $.getScript(rutanormal + 'script/cn/js/cn_mensajes_ui.js').done(function () {
                                ObtenerMensajeUrgente(idMensaje);
                            });
                        });
                    }
                    break;
                default:
                    break;
            }
        });
        if (Notificaciones > 0) {
            $('#NumeroAlertNotificacionCN', top.frames['fraTop'].document).text(Notificaciones);
            $('#lnkNotificacionesCN', top.frames['fraTop'].document).show();
            if ($('#divMuro', doc).length > 0 && mostrarDivNotificaciones) {
                $('#divPanelNotificaciones span', doc).text(Textos[82].replace('###', Notificaciones));
                $('#divNotificaciones', doc).show();
            }
        } else {
            $('#lnkNotificacionesCN', top.frames['fraTop'].document).hide();
        }
    });
}

function ObtenerMensajeUrgente(idMensaje) {
    var timeoffset = new Date().getTimezoneOffset();
    $.when($.ajax({
        type: "POST",
        url: rutanormal + 'script/cn/services/cnMuroUsuario.asmx/Obtener_Mensaje',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ idMensaje: idMensaje, timezoneoffset: timeoffset }),
        dataType: "json",
        async: true
    })).done(function (msg) {
        if ($('#popupMensajeUrgente', doc).length == 0) {
            $.get(rutanormal + 'script/cn/html/_PopUp_MensajeUrgente.htm', function (divmensaje) {
                $('body', doc).append(divmensaje);
                CargarMensajeUrgente(msg);
                $('#btnAceptarPopUpMensajeUrgente', doc).live('click', function () {
                    $('#popupFondo', doc).hide();
                    $('#popupMensajeUrgente', doc).hide();
                    MensajeUrgenteLeido($(this).attr('IdMensaje'));
                });
            });
        } else {
            CargarMensajeUrgente(msg);
        }
    });
}

function CargarMensajeUrgente(msg) {
    var cn = { posts: [] };
    var mensaje = msg.d;

    $('#btnAceptarPopUpMensajeUrgente', doc).attr('IdMensaje', mensaje.IdMensaje);

    mensaje.FechaAlta = eval("new " + mensaje.FechaAlta.slice(1, -1));
    mensaje.FechaActualizacion = eval("new " + mensaje.FechaActualizacion.slice(1, -1));

    mensaje.FechaAltaRelativa = relativeTime(mensaje.FechaAlta);
    mensaje.FechaActualizacionRelativa = relativeTime(mensaje.FechaActualizacion);

    if (mensaje.Respuestas) {
        $.each(mensaje.Respuestas, function () {
            this.FechaAlta = eval("new " + this.FechaAlta.slice(1, -1));
            this.FechaActualizacion = eval("new " + this.FechaActualizacion.slice(1, -1));

            this.FechaAltaRelativa = relativeTime(this.FechaAlta);
            this.FechaActualizacionRelativa = relativeTime(this.FechaActualizacion);
        });
    }
    cn.posts.push(mensaje);       

    $('#divMensajeUrgente', doc).empty();
    $('#divMensajeUrgente', doc).css('max-height', '');
    var ul = $('<ul>').appendTo($('#divMensajeUrgente', doc));
    $('#itemMensaje').tmpl(cn.posts).appendTo(ul);

    var heightContenido = $('body', doc).height();
    CentrarPopUpMain($('#popupMensajeUrgente', doc));
    $('#popupFondo', doc).css('height', $(doc).height());
    $('#popupFondo', doc).show();

    $('#imgMensajeUrgente', doc).attr('src', rutanormal + 'images/ColaboracionHeader.png');
    $('#divMensajeUrgente [id^=divOpcionesMensaje_]', doc).hide();
    $('#divNuevaRespuestaDefault_' + mensaje.IdMensaje, doc).hide();

    $('#btnAceptarPopUpMensajeUrgente', doc).text(Textos[20]);
    $('#divMensajeUrgente [id^=msgDescargar__]', doc).text(Textos[79]);
    $('#btnAceptarPopUpMensajeUrgente', doc).text(Textos[20]);
    $('#divMensajeUrgente [id^=msgDescargar__]', doc).text(Textos[79]);
    $('[id^=msgAdjuntoRespuesta_] span', doc).text(Textos[28]);
    $('[id^=msgMeGusta_]', doc).text(Textos[8]);
    $('[id^=msgResponder_]', doc).text(Textos[77]);
    $('[id^=msgDestinatarios_]', doc).text(Textos[78]);
    $('[id^=msgEditar_]', doc).text(Textos[83]);
    $('[id^=msgNuevaRespuestaDefault_]', doc).val(Textos[84]);
    $('[id^=msgDescargar__]', doc).text(Textos[79]);
    $('[id^=btnResponder_]', doc).text(Textos[20]);
    $('[id^=btnResponder_]', doc).attr('title', Textos[20]);
    $('[id^=btnCancelarResponder_]', doc).text(Textos[21]);
    $('[id^=btnCancelarResponder_]', doc).attr('title', Textos[21]);
    $('[id^=msgNoLeido]', doc).text(Textos[109]);
    $('[id^=msgInfoCategoria]', doc).text(Textos[110]);
    $.each($('[id^=VerRespuestas_]', doc), function () {
        $(this).text(Textos[92].replace('###', $(this).text()) + '>>');
    });

    $('#popupMensajeUrgente', doc).show();
    $('#divMensajeUrgente', doc).css('max-height', $('#popupMensajeUrgente', doc).height() - 85);

    $(top.frames['default_main'].document.frames[0]).scroll(function () {
        CentrarPopUpMain($('#popupMensajeUrgente', doc));
    });
}

function MensajeUrgenteLeido(idMensaje) {
    var timeoffset = new Date().getTimezoneOffset();
    $.when($.ajax({
        type: "POST",
        url: rutanormal + 'script/cn/services/cnMuroUsuario.asmx/MensajeUrgente_Leido',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ idMensaje: idMensaje, timezoneoffset: timeoffset }),
        dataType: "json",
        async: true
    })).done(function (msg) {
        $('#btnAceptarPopUpMensajeUrgente', doc).removeAttr('IdMensaje');
        if (msg && msg.d) {
            CargarMensajeUrgente(msg);
        }
    });
}