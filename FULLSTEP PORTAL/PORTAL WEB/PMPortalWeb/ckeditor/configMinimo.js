﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function (config) {
	config.toolbarCanCollapse = false;
	config.scayt_autoStartup = true;
	config.scayt_uiTabs = '1,1,0';
	config.extraPlugins = 'youtube';
	config.skin = 'moono';
	config.enterMode = 2;
	config.removePlugins = 'elementspath';
	config.height = '60px';
	config.toolbar = [
		['Source', 'Scayt', 'Bold', 'Italic', 'Underline', 'FontSize', 'TextColor',
		'BGColor', 'Copy', 'Paste', 'Youtube', 'Link', 'Image',
		'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'NumberedList', 'BulletedList']
	];
};
//Personalizamos las pestañas del pop up que aparece al querer adjuntar una imagen, link,...
CKEDITOR.on('dialogDefinition', function (ev) {
	// Take the dialog name and its definition from the event
	// data.
	var dialogName = ev.data.name;
	var dialogDefinition = ev.data.definition;

	// Check if the definition is from the dialog we're
	// interested on ("Link" dialog).
	switch (dialogName) {
	    case 'link':
	        dialogDefinition.removeContents('advanced');
	        break;
	}
});
function CKEditorPaste(editor) {
	editor.data.html = editor.data.html.replace(/id="msg/gi, 'id="');
};
function ImageUploadPlugin(editorName) {
    if ($('body #fileUploadCKEditorImage').length == 0) {
        $.get(rutanormal + 'ckeditor/html/_imageUpload.tmpl.htm', function (templates) {
            $('body').append(templates);
            $("#formFileUploadCKEditorImage").attr("action", rutanormal + 'script/cn/FileTransferHandler.ashx');
            $('#fileUploadCKEditorImage').fileupload({ CKEditorImageUpload: editorName });
            $('#fileUploadCKEditorImage').show();
            $('body #fileUploadCKEditorImage form label input[type=file]').attr('accept', 'image/*');

            $('#fileUploadCKEditorImage [type=file]').css('z-index', 10000);
            $('#fileUploadCKEditorImage [type=file]').css('right', '');

            $('.cke_button__image').live('mouseenter', function () {
                $('#fileUploadCKEditorImage').fileupload('CKEditorImageUploadValue', 'msgRespuestaNueva_' + $(this).parents('[id*=msgRespuestaNueva_]').attr('id').replace('cke_msgRespuestaNueva_', ''));
                $('#fileUploadCKEditorImage [type=file]').css('width', $(this).outerWidth());
                $('#fileUploadCKEditorImage [type=file]').css('height', $(this).outerHeight());
                $('#fileUploadCKEditorImage [type=file]').css('left', $(this).offset().left);
                $('#fileUploadCKEditorImage [type=file]').css('top', $(this).offset().top);                
            });
        });
    }
};
