﻿(function (editor) {
    var o = {
        exec: function (editor) {
            var selection = editor.getSelection();
            if (selection.getSelectedText() == '') {
                if ($('#popupBuscadorProcesoCompra').length == 0) {
                    $.when($.get(rutaFS + 'CN/html/_PopUp_Buscador_ProcesoCompra.htm', function (popUp) {
                        popUp = popUp.replace(/src="/gi, 'src="' + ruta);
                        $('#popup').prepend(popUp);
                    })).done(function () {
                        $('#popupFondo').css('height', $(document).height());
                        $('#lblTituloPopUpBuscadorProcesoCompra').text(TextosCKEditor[3]);
                        $('#lblBuscadorProceComprasAnyo').text(TextosCKEditor[12]);
                        $('#lblBuscadorProceComprasGMN1').text(TextosCKEditor[13]);
                        $('#lblBuscadorProceComprasCodDen').text(TextosCKEditor[14]);

                        var optionsBuscadorAnyo = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_ProceCompras_Anyos' };
                        var optionsBuscadorGMN = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, itemListWidth: 250, fsComboParents: 'cboBuscadorProceComprasAnyo', WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_ProceCompras_GMNs' };
                        var optionsBuscadorProcesos = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, fsComboParents: 'cboBuscadorProceComprasAnyo,cboBuscadorProceComprasGMN1', WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_ProceCompras_CodDen' };

                        $('#cboBuscadorProceComprasAnyo').fsCombo(optionsBuscadorAnyo);
                        $('#cboBuscadorProceComprasGMN1').fsCombo(optionsBuscadorGMN);
                        $('#cboBuscadorProceComprasCodDen').fsCombo(optionsBuscadorProcesos);
                        $('#cboBuscadorProceComprasAnyo').fsCombo('selectValue', new Date().getFullYear());
                        if ($('#cboBuscadorProceComprasAnyo').fsCombo('getSelectedValue') == '') {
                            $('#cboBuscadorProceComprasAnyo').fsCombo('selectItem', $('#cboBuscadorProceComprasAnyo').fsCombo('itemsCount'));
                            $('#cboBuscadorProceComprasGMN1').fsCombo('selectItem', 2);
                        }
                        MostrarPopUp();
                    });
                } else {
                    $('#cboBuscadorProceComprasAnyo').fsCombo('selectValue', '');
                    $('#cboBuscadorProceComprasGMN1').fsCombo('selectValue', '');
                    $('#cboBuscadorProceComprasCodDen').fsCombo('selectValue', '');
                    $('#popupBuscadorProcesoCompra').show();
                    MostrarPopUp();
                }
            } else {
                try {
                    var proceCompra = selection.getSelectedText().split("/");
                    $.when($.ajax({
                        type: "POST",
                        url: rutaFS + 'CN/App_Services/CN.asmx/Comprobar_Buscador_ProceCompra',
                        data: JSON.stringify({ Anio: proceCompra[0], GMN1: proceCompra[1], Cod: proceCompra[2] }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true
                    })).done(function (msg) {
                        if (msg.d) {
                            var item = {
                                identificador: msg.d.value,
                                texto: msg.d.text
                            }
                            var element = CKEDITOR.dom.element.createFromHtml('<a type="entidad" href="PC#' + item.identificador + '">' + $('#entidadProcesoCompra').tmpl(item).html().replace(/src="/gi, 'src="' + ruta) + '</a>&nbsp;');
                            editor.insertElement(element);
                        } else { return false; }
                    });
                }
                catch (e) {
                    return false;
                }
            }
        },
        crearCodificacionProcesoCompra: function (item) {
            var element = CKEDITOR.dom.element.createFromHtml('<a type="entidad" href="PC#' + item.identificador + '">' + $('#entidadProcesoCompra').tmpl(item).html().replace(/src="/gi, 'src="' + ruta) + '</a>&nbsp;');
            this.editorCK.insertElement(element);
        },
        setEditor: function (editor) {
            this.editorCK = editor;
        }
    };
    function MostrarPopUp() {
        $('#btnCancelarBusquedaPopUp').attr('popup', 'BuscadorProcesoCompra');
        CentrarPopUp($('#popup'));
        $('#popupFondo').css('z-index', 1003);
        $('#popupFondo').show();
        $('#popup').show();
    };
    CKEDITOR.plugins.add('FSProcesoCompra', {
        init: function (editor) {
            editor.addCommand('FSProcesoCompra', o);
            editor.ui.addButton('FSProcesoCompra', {
                label: TextosCKEditor[3],
                icon: this.path + 'FSProcesoCompra.png',
                command: 'FSProcesoCompra'
            });
            o.setEditor(editor);
        },
        setSelectedValue: function (item) {
            o.crearCodificacionProcesoCompra(item);
        }
    });
})();