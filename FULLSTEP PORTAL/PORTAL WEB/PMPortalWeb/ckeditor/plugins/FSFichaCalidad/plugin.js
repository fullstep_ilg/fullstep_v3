﻿(function (editor) {
    var o = {
        exec: function (editor) {
            var selection = editor.getSelection();
            if (selection.getSelectedText() == '') {
                if ($('#popupBuscadorFichaCalidad').length == 0) {
                    $.when($.get(rutaFS + 'CN/html/_PopUp_Buscador_FichaCalidad.htm', function (popUp) {
                        popUp = popUp.replace(/src="/gi, 'src="' + ruta);
                        $('#popup').prepend(popUp);
                    })).done(function () {
                        $('#popupFondo').css('height', $(document).height());
                        $('#lblTituloPopUpBuscadorFichaCalidad').text(TextosCKEditor[9]);
                        $('#lblPopUpBuscadorFichaCalidad').text(TextosCKEditor[10]);

                        var optionsBuscadorFichaCalidad = { valueField: "value", textField: "text", isDropDown: false, disableParentEmptyValue: false, listItemMaxHeight: 100, appendEmptyItem: true, autoComplete: true, itemListMaxHeight: 250, WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_BuscadorFichaCalidad_Proveedores' };
                        $('#cboBuscadorFichaCalidadProveedor').fsCombo(optionsBuscadorFichaCalidad);
                        MostrarPopUp();
                    });
                } else {
                    $('#cboBuscadorFichaCalidadProveedor').fsCombo('selectValue', '');
                    $('#popupBuscadorFichaCalidad').show();
                    MostrarPopUp();
                }
            } else {
                try {
                    var codProve = selection.getSelectedText();
                    $.when($.ajax({
                        type: "POST",
                        url: rutaFS + 'CN/App_Services/CN.asmx/Comprobar_Buscador_Proveedor',
                        data: JSON.stringify({ CodProve: codProve }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true
                    })).done(function (msg) {
                        if (msg.d) {
                            var item = {
                                identificador: msg.d.value,
                                texto: msg.d.text
                            }
                            var element = CKEDITOR.dom.element.createFromHtml('<a type="entidad" href="FP#' + item.identificador + '">' + $('#entidadFichaCalidad').tmpl(item).html().replace(/src="/gi, 'src="' + ruta) + '</a>&nbsp;');
                            editor.insertElement(element);
                        } else { return false; }
                    });
                }
                catch (e) {
                    return false;
                }
            }
        },
        crearCodificacionFichaCalidad: function (item) {
            var element = CKEDITOR.dom.element.createFromHtml('<a type="entidad" href="FP#' + item.identificador + '">' + $('#entidadFichaCalidad').tmpl(item).html().replace(/src="/gi, 'src="' + ruta) + '</a>&nbsp;');
            this.editorCK.insertElement(element);
        },
        setEditor: function (editor) {
            this.editorCK = editor;
        }
    };
    function MostrarPopUp() {
        $('#btnCancelarBusquedaPopUp').attr('popup', 'BuscadorFichaCalidad');
        CentrarPopUp($('#popup'));
        $('#popupFondo').css('z-index', 1003);
        $('#popupFondo').show();
        $('#popup').show();
        $('#cboBuscadorFichaCalidadProveedor').fsCombo('focus');
    };
    CKEDITOR.plugins.add('FSFichaCalidad', {
        init: function (editor) {
            editor.addCommand('FSFichaCalidad', o);
            editor.ui.addButton('FSFichaCalidad', {
                label: TextosCKEditor[9],
                icon: this.path + 'FSFichaCalidad.png',
                command: 'FSFichaCalidad'
            });
            o.setEditor(editor);
        },
        setSelectedValue: function (item) {
            o.crearCodificacionFichaCalidad(item);
        }
    });
})();