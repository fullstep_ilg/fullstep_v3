﻿(function (editor) {
    var o = {
        exec: function (editor) {
            var selection = editor.getSelection();
            if (selection.getSelectedText() == '') {
                if ($('#popupBuscadorSolicitudCompra').length == 0) {
                    $.when($.get(rutaFS + 'CN/html/_PopUp_Buscador_SolicitudCompra.htm', function (popUp) {
                        popUp = popUp.replace(/src="/gi, 'src="' + ruta);
                        $('#popup').prepend(popUp);
                    })).done(function () {
                        $('#popupFondo').css('height', $(document).height());
                        $('#lblTituloPopUpBuscadorSolicitudCompra').text(TextosCKEditor[5]);
                        $('#lblBuscadorSolicitudCompraTipo').text(TextosCKEditor[15]);
                        $('#lblBuscadorSolicitudCompraIdentificador').text(TextosCKEditor[16]);

                        var optionsBuscadorSolicitudCompraTipo = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, itemListWidth: 250, WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_BuscadorSolicitudes_Tipos' };
                        var optionsBuscadorSolicitudCompraIdentificador = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', disableParentEmptyValue: false, itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, fsComboParents: 'cboBuscadorSolicitudCompraTipo', WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_BuscadorSolicitudes_Identificador' };
                        $('#cboBuscadorSolicitudCompraTipo').fsCombo(optionsBuscadorSolicitudCompraTipo);
                        $('#cboBuscadorSolicitudCompraIdentificador').fsCombo(optionsBuscadorSolicitudCompraIdentificador);
                        MostrarPopUp();
                    });
                } else {
                    $('#cboBuscadorSolicitudCompraTipo').fsCombo('selectValue', '');
                    $('#cboBuscadorSolicitudCompraIdentificador').fsCombo('selectValue', '');
                    $('#popupBuscadorSolicitudCompra').show();
                    MostrarPopUp();
                }
            } else {
                try {
                    var identificador = parseInt(selection.getSelectedText());
                    $.when($.ajax({
                        type: "POST",
                        url: rutaFS + 'CN/App_Services/CN.asmx/Comprobar_Buscador_Identificador',
                        data: JSON.stringify({ TipoEntidad: 1, Identificador: identificador }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true
                    })).done(function (msg) {
                        if (msg.d) {
                            var item = {
                                identificador: msg.d.value,
                                texto: msg.d.text
                            }
                            var element = CKEDITOR.dom.element.createFromHtml('<a type="entidad" href="SC#' + item.identificador + '">' + $('#entidadSolicitudCompra').tmpl(item).html().replace(/src="/gi, 'src="' + ruta) + '</a>&nbsp;');
                            editor.insertElement(element);
                        } else { return false; }
                    });
                }
                catch (e) {
                    return false;
                }
            }
        },
        crearCodificacionSolicitudCompra: function (item) {
            var element = CKEDITOR.dom.element.createFromHtml('<a type="entidad" href="SC#' + item.identificador + '">' + $('#entidadSolicitudCompra').tmpl(item).html().replace(/src="/gi, 'src="' + ruta) + '</a>&nbsp;');
            this.editorCK.insertElement(element);
        },
        setEditor: function (editor) {
            this.editorCK = editor;
        }
    };
    function MostrarPopUp() {
        $('#btnCancelarBusquedaPopUp').attr('popup', 'BuscadorSolicitudCompra');
        CentrarPopUp($('#popup'));
        $('#popupFondo').css('z-index', 1003);
        $('#popupFondo').show();
        $('#popup').show();
    };
    CKEDITOR.plugins.add('FSSolicitudCompra', {
        init: function (editor) {
            editor.addCommand('FSSolicitudCompra', o);
            editor.ui.addButton('FSSolicitudCompra', {
                label: TextosCKEditor[5],
                icon: this.path + 'FSSolicitudCompra.png',
                command: 'FSSolicitudCompra'
            });
            o.setEditor(editor);
        },
        setSelectedValue: function (item) {
            o.crearCodificacionSolicitudCompra(item);
        }
    });
})();