﻿(function (editor) {
    var o = {
        exec: function (editor) {
            var selection = editor.getSelection();
            if (selection.getSelectedText() == '') {
                if ($('#popupBuscadorPedido').length == 0) {
                    $.when($.get(rutaFS + 'CN/html/_PopUp_Buscador_Pedido.htm', function (popUp) {
                        popUp = popUp.replace(/src="/gi, 'src="' + ruta);
                        $('#popup').prepend(popUp);
                        $.each($('#popupBuscadorPedido img'), function () {
                            $(this).attr('src', rutaFS + $(this).attr('src'));
                        });
                    })).done(function () {
                        $('#popupFondo').css('height', $(document).height());
                        $('#lblTituloPopUpBuscadorPedido').text(TextosCKEditor[11]);
                        $('#lblBuscadorPedidoAnyo').text(TextosCKEditor[12]);
                        $('#lblBuscadorPedidoNumCesta').text(TextosCKEditor[17]);
                        $('#lblBuscadorPedidoNumPedido').text(TextosCKEditor[18]);

                        var optionsBuscadorPedidoAnyo = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_Pedidos_Anyos' };
                        var optionsBuscadorPedidoNumCesta = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, fsComboParents: 'cboBuscadorPedidoAnyo', WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_Pedidos_Cestas' };
                        var optionsBuscadorPedidoNumPedido = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, fsComboParents: 'cboBuscadorPedidoAnyo,cboBuscadorPedidoNumCesta', WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_Pedidos_Pedidos' };
                        $('#cboBuscadorPedidoAnyo').fsCombo(optionsBuscadorPedidoAnyo);
                        $('#cboBuscadorPedidoNumCesta').fsCombo(optionsBuscadorPedidoNumCesta);
                        $('#cboBuscadorPedidoNumPedido').fsCombo(optionsBuscadorPedidoNumPedido);
                        MostrarPopUp();
                    });
                } else {
                    $('#cboBuscadorPedidoAnyo').fsCombo('selectValue', '');
                    $('#cboBuscadorPedidoNumCesta').fsCombo('selectValue', '');
                    $('#cboBuscadorPedidoNumPedido').fsCombo('selectValue', '');
                    $('#popupBuscadorPedido').show();
                    MostrarPopUp();
                }
            } else {
                try {
                    var pedido = selection.getSelectedText().split("/");
                    $.when($.ajax({
                        type: "POST",
                        url: rutaFS + 'CN/App_Services/CN.asmx/Comprobar_Buscador_Pedido',
                        data: JSON.stringify({ Anio: pedido[0], NumPedido: pedido[1], NumOrden: pedido[2] }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true
                    })).done(function (msg) {
                        if (msg.d) {
                            var item = {
                                identificador: msg.d.value,
                                texto: msg.d.text
                            }
                            var element = CKEDITOR.dom.element.createFromHtml('<a type="entidad" href="PE#' + item.identificador + '">' + $('#entidadPedido').tmpl(item).html().replace(/src="/gi, 'src="' + ruta) + '</a>&nbsp;');
                            editor.insertElement(element);
                        } else { return false; }
                    });
                }
                catch (e) {
                    return false;
                }
            }
        },
        crearCodificacionProcesoCompra: function (item) {
            var element = CKEDITOR.dom.element.createFromHtml('<a type="entidad" href="PE#' + item.identificador + '">' + $('#entidadPedido').tmpl(item).html().replace(/src="/gi, 'src="' + ruta) + '</a>&nbsp;');
            this.editorCK.insertElement(element);
        },
        setEditor: function (editor) {
            this.editorCK = editor;
        }
    };
    function MostrarPopUp() {
        $('#btnCancelarBusquedaPopUp').attr('popup', 'BuscadorPedido');
        CentrarPopUp($('#popup'));
        $('#popupFondo').css('z-index', 1003);
        $('#popupFondo').show();
        $('#popup').show();
    };
    CKEDITOR.plugins.add('FSPedido', {
        init: function (editor) {
            editor.addCommand('FSPedido', o);
            editor.ui.addButton('FSPedido', {
                label: TextosCKEditor[4],
                icon: this.path + 'FSPedido.png',
                command: 'FSPedido'
            });
            o.setEditor(editor);
        },
        setSelectedValue: function (item) {
            o.crearCodificacionProcesoCompra(item);
        }
    });
})();