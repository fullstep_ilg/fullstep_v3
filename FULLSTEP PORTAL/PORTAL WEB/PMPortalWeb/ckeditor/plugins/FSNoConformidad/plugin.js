﻿(function (editor) {
    var o = {
        exec: function (editor) {
            var selection = editor.getSelection();
            if (selection.getSelectedText() == '') {
                if ($('#popupBuscadorNoConformidad').length == 0) {
                    $.when($.get(rutaFS + 'CN/html/_PopUp_Buscador_NoConformidad.htm', function (popUp) {
                        popUp = popUp.replace(/src="/gi, 'src="' + ruta);
                        $('#popup').prepend(popUp);
                    })).done(function () {
                        $('#popupFondo').css('height', $(document).height());
                        $('#lblTituloPopUpBuscadorNoConformidad').text(TextosCKEditor[7]);
                        $('#lblBuscadorNoConformidadTipo').text(TextosCKEditor[15]);
                        $('#lblBuscadorNoConformidadProveedor').text(TextosCKEditor[10]);
                        $('#lblBuscadorNoConformidadIdentificador').text(TextosCKEditor[16]);

                        var optionsBuscadorNoConfTipo = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, itemListWidth: 250, WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_BuscadorNoConf_Tipos' };
                        var optionsBuscadorNoConfProveedor = { valueField: "value", textField: "text", isDropDown: false, MinimumPrefixLength: 1, disableParentEmptyValue: false, itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, itemListWidth: 250, WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_BuscadorNoConf_Proveedores' };
                        var optionsBuscadorNoConfIdentificador = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', parentDependant: true, itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, fsComboParents: 'cboBuscadorNoConformidadTipo,cboBuscadorNoConformidadProveedor', WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_BuscadorNoConf_Identificador' };
                        $('#cboBuscadorNoConformidadTipo').fsCombo(optionsBuscadorNoConfTipo);
                        $('#cboBuscadorNoConformidadProveedor').fsCombo(optionsBuscadorNoConfProveedor);
                        $('#cboBuscadorNoConformidadIdentificador').fsCombo(optionsBuscadorNoConfIdentificador);
                        MostrarPopUp();
                    });
                } else {
                    $('#cboBuscadorNoConformidadTipo').fsCombo('selectValue', '');
                    $('#cboBuscadorNoConformidadProveedor').fsCombo('selectValue', '');
                    $('#cboBuscadorNoConformidadIdentificador').fsCombo('selectValue', '');
                    $('#popupBuscadorNoConformidad').show();
                    MostrarPopUp();
                }
            } else {
                try {
                    var identificador = parseInt(selection.getSelectedText());
                    $.when($.ajax({
                        type: "POST",
                        url: rutaFS + 'CN/App_Services/CN.asmx/Comprobar_Buscador_Identificador',
                        data: JSON.stringify({ TipoEntidad: 3, Identificador: identificador }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true
                    })).done(function (msg) {
                        if (msg.d) {
                            var item = {
                                identificador: msg.d.value,
                                texto: msg.d.text
                            }
                            var element = CKEDITOR.dom.element.createFromHtml('<a type="entidad" href="NC#' + item.identificador + '">' + $('#entidadNoConformidad').tmpl(item).html().replace(/src="/gi, 'src="' + ruta) + '</a>&nbsp;');
                            editor.insertElement(element);
                        } else { return false; }
                    });
                }
                catch (e) {
                    return false;
                }
            }
        },
        crearCodificacionNoConformidad: function (item) {
            var element = CKEDITOR.dom.element.createFromHtml('<a type="entidad" href="NC#' + item.identificador + '">' + $('#entidadNoConformidad').tmpl(item).html().replace(/src="/gi, 'src="' + ruta) + '</a>&nbsp;');
            this.editorCK.insertElement(element);
        },
        setEditor: function (editor) {
            this.editorCK = editor;
        }
    };
    function MostrarPopUp() {
        $('#btnCancelarBusquedaPopUp').attr('popup', 'BuscadorNoConformidad');
        CentrarPopUp($('#popup'));
        $('#popupFondo').css('z-index', 1003);
        $('#popupFondo').show();
        $('#popup').show();
    };
    CKEDITOR.plugins.add('FSNoConformidad', {
        init: function (editor) {
            editor.addCommand('FSNoConformidad', o);
            editor.ui.addButton('FSNoConformidad', {
                label: TextosCKEditor[7],
                icon: this.path + 'FSNoConformidad.png',
                command: 'FSNoConformidad'
            });
            o.setEditor(editor);
        },
        setSelectedValue: function (item) {
            o.crearCodificacionNoConformidad(item);
        }
    });
})();