﻿(function (editor) {
    var o = {
        exec: function (editor) {
            var selection = editor.getSelection();
            if (selection.getSelectedText() == '') {
                if ($('#popupBuscadorContrato').length == 0) {
                    $.when($.get(rutaFS + 'CN/html/_PopUp_Buscador_Contrato.htm', function (popUp) {
                        popUp = popUp.replace(/src="/gi, 'src="' + ruta);
                        $('#popup').prepend(popUp);
                    })).done(function () {
                        $('#popupFondo').css('height', $(document).height());
                        $('#lblTituloPopUpBuscadorContrato').text(TextosCKEditor[6]);
                        $('#lblBuscadorContratoTipo').text(TextosCKEditor[15]);
                        $('#lblBuscadorContratoProveedor').text(TextosCKEditor[10]);
                        $('#lblBuscadorContratoIdentificador').text(TextosCKEditor[16]);

                        var optionsBuscadorContratoTipo = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, itemListWidth: 250, WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_BuscadorContrato_Tipos' };
                        var optionsBuscadorContratoProveedor = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', isDropDown: false, MinimumPrefixLength: 1, disableParentEmptyValue: false, itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, itemListWidth: 250, fsComboParents: 'cboBuscadorContratoTipo', WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_BuscadorContrato_Proveedor' };
                        var optionsBuscadorContratoIdentificador = { valueField: "value", textField: "text", dropDownImageURL: 'Flecha.png', parentDependant: true, itemListMaxHeight: 100, appendEmptyItem: true, autoComplete: true, fsComboParents: 'cboBuscadorContratoTipo,cboBuscadorContratoProveedor', WebMethodURL: rutaFS + 'CN/App_Services/CN.asmx/Obtener_BuscadorContrato_Identificador' };
                        $('#cboBuscadorContratoTipo').fsCombo(optionsBuscadorContratoTipo);
                        $('#cboBuscadorContratoProveedor').fsCombo(optionsBuscadorContratoProveedor);
                        $('#cboBuscadorContratoIdentificador').fsCombo(optionsBuscadorContratoIdentificador);
                        MostrarPopUp();
                    });
                } else {
                    $('#cboBuscadorContratoTipo').fsCombo('selectValue', '');
                    $('#cboBuscadorContratoProveedor').fsCombo('selectValue', '');
                    $('#cboBuscadorContratoIdentificador').fsCombo('selectValue', '');
                    $('#popupBuscadorContrato').show();
                    MostrarPopUp();
                }
            } else {
                try {
                    var identificador = parseInt(selection.getSelectedText());
                    $.when($.ajax({
                        type: "POST",
                        url: rutaFS + 'CN/App_Services/CN.asmx/Comprobar_Buscador_Identificador',
                        data: JSON.stringify({ TipoEntidad: 5, Identificador: identificador }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true
                    })).done(function (msg) {
                        if (msg.d) {
                            var item = {
                                identificador: msg.d.value,
                                texto: msg.d.text
                            }
                            var element = CKEDITOR.dom.element.createFromHtml('<a type="entidad" href="CO#' + item.identificador + '">' + $('#entidadContrato').tmpl(item).html().replace(/src="/gi, 'src="' + ruta) + '</a>&nbsp;');
                            editor.insertElement(element);
                        } else { return false; }
                    });
                }
                catch (e) {
                    return false;
                }
            }
        },
        crearCodificacionContrato: function (item) {
            var element = CKEDITOR.dom.element.createFromHtml('<a type="entidad" href="CO#' + item.identificador + '">' + $('#entidadContrato').tmpl(item).html().replace(/src="/gi, 'src="' + ruta) + '</a>&nbsp;');
            this.editorCK.insertElement(element);
        },
        setEditor: function (editor) {
            this.editorCK = editor;
        }
    };
    function MostrarPopUp() {
        $('#btnCancelarBusquedaPopUp').attr('popup', 'BuscadorContrato');
        CentrarPopUp($('#popup'));
        $('#popupFondo').css('z-index', 1003);
        $('#popupFondo').show();
        $('#popup').show();
    };
    CKEDITOR.plugins.add('FSContrato', {
        init: function (editor) {
            editor.addCommand('FSContrato', o);
            editor.ui.addButton('FSContrato', {
                label: TextosCKEditor[6],
                icon: this.path + 'FSContrato.png',
                command: 'FSContrato'
            });
            o.setEditor(editor);
        },
        setSelectedValue: function (item) {
            o.crearCodificacionContrato(item);
        }
    });
})();