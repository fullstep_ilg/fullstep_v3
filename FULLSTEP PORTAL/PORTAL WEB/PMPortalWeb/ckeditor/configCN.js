﻿CKEDITOR.editorConfig = function (config) {
	config.toolbarCanCollapse = false;
	config.scayt_autoStartup = true;
	config.scayt_uiTabs = '1,1,0';
	config.extraPlugins = 'youtube';
	config.skin = 'moono';
	config.enterMode = 2;
	config.removePlugins = 'elementspath';    
	config.height = '60px';
	config.toolbar = [
		['Source', 'Scayt', 'Bold', 'Italic', 'Underline', 'FontSize', 'TextColor',
		'BGColor', 'Copy', 'Paste', 'Youtube', 'Link', 'Image',
		'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'NumberedList', 'BulletedList'],
		[]
	];
	if (usuario.AccesoGS) config.toolbar[1][config.toolbar[1].length] = 'FSProcesoCompra';
	if (usuario.AccesoPM) {
		config.toolbar[1][config.toolbar[1].length] = 'FSSolicitudCompra';
		config.toolbar[1][config.toolbar[1].length] = 'FSContrato';
	}
	if (usuario.AccesoQA) {
		config.toolbar[1][config.toolbar[1].length] = 'FSNoConformidad';
		config.toolbar[1][config.toolbar[1].length] = 'FSCertificado';
		if (usuario.QAPuntuacionProveedores) config.toolbar[1][config.toolbar[1].length] = 'FSFichaCalidad';
	}
};
//Personalizamos las pestañas del pop up que aparece al querer adjuntar una imagen, link,...
CKEDITOR.on('dialogDefinition', function (ev) {
	// Take the dialog name and its definition from the event
	// data.
	var dialogName = ev.data.name;
	var dialogDefinition = ev.data.definition;

	// Check if the definition is from the dialog we're
	// interested on ("Link" dialog).
	switch (dialogName) {
		case 'link':
			dialogDefinition.removeContents('advanced');
			break;
	}
});
function CKEditorPaste(editor) {
    editor.data.dataValue = editor.data.dataValue.replace(/id="msg/gi, 'id="');
};
function ImageUploadPlugin(editorName) {
    if ($('body #fileUploadCKEditorImage').length == 0) {
        $.get(rutanormal + 'ckeditor/html/_imageUpload.tmpl.htm', function (templates) {
            $('body').append(templates);
            $("#formFileUploadCKEditorImage").attr("action", rutanormal + 'script/cn/FileTransferHandler.ashx');
            $('#fileUploadCKEditorImage').fileupload({ CKEditorImageUpload: editorName });
            $('#fileUploadCKEditorImage').show();

            $('#fileUploadCKEditorImage [type=file]').css('z-index', 10000);
            $('#fileUploadCKEditorImage [type=file]').css('right', '');

            $('.cke_button__image').live('mouseenter', function () {  
                $('#fileUploadCKEditorImage [type=file]').css('width', $(this).outerWidth());
                $('#fileUploadCKEditorImage [type=file]').css('height', $(this).outerHeight());
                if (editorName.indexOf('Master') == 0) {
                    $('#fileUploadCKEditorImage [type=file]').css('left', $(this).position().left);
                    $('#fileUploadCKEditorImage [type=file]').css('top', $(this).position().top);
                } else {
                    $('#fileUploadCKEditorImage [type=file]').css('left', $(this).offset().left);
                    $('#fileUploadCKEditorImage [type=file]').css('top', $(this).offset().top);
                }
            });            
        });
    }
};