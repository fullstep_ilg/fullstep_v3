VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGestorIdiomasP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private AdoCon As ADODB.Connection


Private Sub Class_Terminate()
    
    AdoCon.Close
    Set AdoCon = Nothing
    
End Sub

Public Sub Conectar(ByVal sInstancia As String)
Dim Servidor As String, basedatos As String
    
    
    
        
    On Error GoTo ErrorDeConexion
    
    Servidor = GetStringSetting("FULLSTEP PORTAL", sInstancia, "Conexion", "Servidor")
    basedatos = GetStringSetting("FULLSTEP PORTAL", sInstancia, "Conexion", "BaseDeDatos")
   
    Set AdoCon = New ADODB.Connection
    
    AdoCon.Open "Provider=SQLOLEDB.1; Auto Translate=FALSE;Server=" & Servidor & ";Database=" & basedatos & ";", LICDBLogin, LICDBContra
    AdoCon.CursorLocation = adUseClient
    
    
    Exit Sub
    
ErrorDeConexion:
    
        Err.Raise 1000, , "Imposible conectar a la BD"
        
End Sub

Public Function DevolverTextoAyuda(ByVal Modulo As Long) As ADODB.Recordset
Dim ador As ADODB.Recordset
Dim sConsulta As String
    
    sConsulta = "SELECT * FROM WEBTEXTHELP WHERE MODULO = " & Modulo
    
    Set ador = New ADODB.Recordset
    
    ador.Open sConsulta, AdoCon, adOpenForwardOnly, adLockReadOnly
    
    Set ador.ActiveConnection = Nothing
    Set DevolverTextoAyuda = ador
    
End Function


Public Function DevolverTextoModulo(ByVal Modulo As Long) As ADODB.Recordset
Dim ador As ADODB.Recordset
Dim sConsulta As String
    
    sConsulta = "SELECT * FROM WEBTEXT WHERE MODULO = " & Modulo
    
    Set ador = New ADODB.Recordset
    
    ador.Open sConsulta, AdoCon, adOpenForwardOnly, adLockReadOnly
    
    Set ador.ActiveConnection = Nothing
    Set DevolverTextoModulo = ador
    
End Function

''' <summary>
''' Extrae la contrase�a del fichero FSPSDB.LIC y la desencripta.
''' </summary>
''' <returns>La contrase�a desencriptada</returns>
''' <remarks>Llamada desde: frmPrincipal.Form_Load ; Tiempo m�ximo: 0,1</remarks>
Private Function LICDBContra() As String
    Dim fso As New FileSystemObject, fil As File, ts As TextStream
    Dim FechaHora As String
    Dim tmplogin As String
    Dim tmpcontra As String

    Set fil = fso.GetFile(App.Path & "\FSDB.LIC")
    Set ts = fil.OpenAsTextStream(ForReading)
    
    FechaHora = Format(ts.ReadLine, "DD\/MM\/YYYY HH\:NN\:SS")
    tmplogin = ts.ReadLine
    tmpcontra = ts.ReadLine
    ts.Close
        
    ''' Desencriptar contrase�a
    LICDBContra = DesEncriptar(tmpcontra, FechaHora)
End Function

''' <summary>
''' Extrae el login del fichero FSPSDB.LIC y lo desencripta.
''' </summary>
''' <returns>La contrase�a desencriptada</returns>
''' <remarks>Llamada desde: frmPrincipal.Form_Load ; Tiempo m�ximo: 0,1</remarks>
Private Function LICDBLogin() As String
    Dim fso As New FileSystemObject, fil As File, ts As TextStream
    Dim FechaHora As String
    Dim tmplogin As String
    Dim tmpcontra As String

    Set fil = fso.GetFile(App.Path & "\FSDB.LIC")
    Set ts = fil.OpenAsTextStream(ForReading)
    
    FechaHora = Format(ts.ReadLine, "DD\/MM\/YYYY HH\:NN\:SS")
    tmplogin = ts.ReadLine
    tmpcontra = ts.ReadLine
    ts.Close
        
    ''' Desencriptar login
    LICDBLogin = DesEncriptar(tmplogin, FechaHora)
End Function

''' <summary>
''' Funci�n que desencripta un string que se le pasa como par�metro.
''' </summary>
''' <param name="Dato">Dato a desencriptar.</param>
''' <param name="Fecha">Fecha para la desencriptaci�n</param>
''' <returns>El string desencriptado.</returns>
''' <remarks>Llamada desde: LICDBContra, LICDBLogin ; Tiempo m�ximo: 0,1</remarks>
Private Function DesEncriptar(ByVal Dato As String, Optional ByVal Fecha As Variant) As String
    Dim fechahoracrypt As Date
    Dim oCrypt2 As CCrypt2
        
    If IsMissing(Fecha) Then
        fechahoracrypt = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            fechahoracrypt = DateSerial(1974, 3, 5)
        Else
            fechahoracrypt = Fecha
        End If
    End If
    
    Set oCrypt2 = New CCrypt2
    
    oCrypt2.InBuffer = Dato
    oCrypt2.Codigo = ClaveSQLtod
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    oCrypt2.Decrypt
    DesEncriptar = oCrypt2.OutBuffer
End Function


