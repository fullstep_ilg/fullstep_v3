VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEspecificacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private moItem As CItem
Private moProceso As CProceso
Private moGrupo As CGrupo
Private miId As Integer
Private mvComentario As Variant
Private msNombre As String
Private mdFecact As Date
Private mvIndice As Variant
Private moConexion As CConexion
Private mlDataSize As Long 'Tamanyo del contenido en bytes del campo mvarData
Private moCia As CCompania

Public Property Let dataSize(ByVal l As Long)
    mlDataSize = l
End Property
Public Property Get dataSize() As Long
    dataSize = mlDataSize
End Property

Public Property Let Indice(ByVal vIndice As Variant)
    mvIndice = vIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvIndice
End Property

Public Property Let Fecha(ByVal Dat As Date)
    mdFecact = Dat
End Property
Public Property Get Fecha() As Date
    Fecha = mdFecact
End Property
Public Property Get Nombre() As String
    Nombre = msNombre
End Property
Public Property Let Nombre(ByVal vData As String)
    msNombre = vData
End Property
Public Property Get Comentario() As Variant
    Comentario = mvComentario
End Property
Public Property Let Comentario(ByVal vData As Variant)
    mvComentario = vData
End Property
Public Property Let id(ByVal i As Integer)
    miId = i
End Property
Public Property Get id() As Integer
    id = miId
End Property
Public Property Set Item(ByVal o As CItem)
    Set moItem = o
End Property
Public Property Get Item() As CItem
    Set Item = moItem
End Property
Public Property Set Proceso(ByVal oProce As CProceso)
    Set moProceso = oProce
End Property
Public Property Get Proceso() As CProceso
    Set Proceso = moProceso
End Property
Public Property Set Grupo(ByVal oGrupo As CGrupo)
    Set moGrupo = oGrupo
End Property
Public Property Get Grupo() As CGrupo
    Set Grupo = moGrupo
End Property

Public Property Set Cia(ByVal oCia As CCompania)
    Set moCia = oCia
End Property
Public Property Get Cia() As CCompania
    Set Cia = moCia
End Property

Friend Property Set Conexion(oCon As CConexion)
Set moConexion = oCon
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property

Private Sub Class_Initialize()
    
    mlDataSize = 0
End Sub

Private Sub Class_Terminate()
    Set moConexion = Nothing
End Sub



''' <summary>
''' Descarga las especificiones de proceso, grupo o item
''' </summary>
''' <param name="CiaComp">id de compania</param>
''' <param name="sComent">Comentario</param>
''' <param name="Path">Path del directorio donde se deja el fichero</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: solicitudesoferta\descargaespecificserver.asp; Tiempo m�ximo: 0,1</remarks>
Public Sub DescargarEspec(ByVal lCiaComp As Long, ByVal sComent As String, ByVal Path As String)
    Dim oFilePropReader As DSOleFile.PropertyReader
    Dim oDocProp As DSOleFile.DocumentProperties
    Dim sComentario As String
    Dim sFileName As String
    Dim oFSO As scripting.FileSystemObject
    
    'Si no existe el directorio lo crea el path con el c�digo del proveedor,hora,min,y segundos.
    Set oFSO = New scripting.FileSystemObject
    If oFSO.FolderExists(Path) = False Then
        oFSO.CreateFolder Path
    End If
    Set oFSO = Nothing
    
    '1� Abrimos el fichero para escritura binaria
    sFileName = Path & "\" & Nombre

    LeerAdjunto lCiaComp, mlDataSize, sFileName
        
    
    '2� Asigna comentarios a las especificaciones:llevar� la informaci�n del comentario asociada
    'a la especificaci�n (si lo tiene), y los datos que lo relacionan con el proceso,grupo o item.
    'Se ha a�adido el tema del OnError debido a que en algun caso guardar los ficheros sin las pesta�as de comentarios y eso provocaba que no se descargasen las especificaciones
    Set oFilePropReader = New DSOleFile.PropertyReader

    On Error GoTo Error:
    
    Set oDocProp = oFilePropReader.GetDocumentProperties(Path & "\" & Nombre)
    sComentario = sComent
    
    If Not moItem Is Nothing Then
        'Especificaci�n de item
        sComentario = sComentario & " " & moItem.ArticuloCod & "-" & moItem.descr
    ElseIf Not moGrupo Is Nothing Then
        'Especificaci�n de grupo
        sComentario = sComentario & " " & moGrupo.Codigo
    End If
    
    If Not IsNull(mvComentario) Then
        sComentario = sComentario & ":" & mvComentario
    End If
        
    oDocProp.Comments = sComentario
     
    'Cierra todos los objetos
    Set oDocProp = Nothing
    Set oFilePropReader = Nothing
    
Error:

End Sub


''' <summary>
''' Descarga archivo de tabla CIAS_ESP del Portal a trav�s de FSNWebService o especificaciones de proceso en GS y guarda en disco
''' </summary>
''' <param name="lCiaComp">id de compania compradora</param>
''' <param name="sPath">carpeta temporal</param>
''' <param name="sTarget">nombre fichero</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: solicitudesoferta\downloadespec2.asp, usuppal\downloadespec.asp; Tiempo m�ximo: 0,1</remarks>
''' Revisada EPB 31/08/2015
Public Function EscribirADisco(ByVal lCiaComp As Long, ByVal sPath As String, ByVal sTarget As String) As CTESError
    Dim oTESError As CTESError
    Dim sFileName As String
    Dim sTargetOriginal As String
    Dim sConsulta As String
    Dim adoRes As New adodb.Recordset
    Dim iNum As Integer
    
    Set oTESError = New CTESError
    oTESError.NumError = 0
        
    If Not moCia Is Nothing Then
        CrearArbol sPath
        oTESError.Arg1 = sTarget
        sTargetOriginal = sTarget
            
        sFileName = sPath & sTarget
        
        'Buscamos el ID de Especificacion del fichero
        sConsulta = "SELECT ID AS NUM FROM CIAS_ESP WITH(NOLOCK) WHERE CIA=" & moCia.id & " AND NOM='" & DblQuote(sTarget) & "'"
        adoRes.Open sConsulta, moConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        If adoRes.eof Then
            iNum = 1
        Else
            iNum = NullToDbl0(adoRes.Fields("NUM"))
        End If
        adoRes.Close
        Set adoRes = Nothing
    
        LeerAdjuntoCia iNum, moCia.id, mlDataSize, sFileName
    Else
        CrearArbol sPath
        oTESError.Arg1 = sTarget
        sTargetOriginal = sTarget
        
        sFileName = sPath & sTarget
        
        LeerAdjunto lCiaComp, mlDataSize, sFileName
    End If

fin:
    Set EscribirADisco = oTESError
    Exit Function
Error:
    On Error Resume Next
    oTESError.NumError = 200
    Resume fin
End Function
''' <summary>
''' Guardar adjunto en tabla ADJUN del Portal a trav�s de FSNWebService
''' </summary>
''' <param name="Cia">id de compania compradora</param>
''' <param name="sTemp">carpeta temporal</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: usuppal\uploadserver.asp; Tiempo m�ximo: 0,1</remarks>
''' Revisada EPB 31/08/2015
Public Function guardarEspecificacionCia(ByVal Cia As Long, ByVal sTemp As String) As CTESError
    Dim fl As Long
    Dim Chunk() As Byte
    Dim oTESError As CTESError
    Dim fso As Object
    Dim objDom As Object
    Dim objXmlHttp As Object
    Dim parser As Object
    Dim XmlBody As String
    Dim strUrl As String
    Dim strSoapAction As String
    Dim strResultado As String
    Dim strBase64 As String
    Dim nId As Long
    Dim sConsulta As String
    Dim adoRes As New adodb.Recordset
    Dim iNum As Integer
    Dim rutaWS As String
    Dim partesRutaAdjun() As String
    Dim nomAdjun As String
    
    Set oTESError = New CTESError
    
    oTESError.NumError = 0
    
    On Error GoTo Error
    
    partesRutaAdjun = Split(sTemp, "\")
    nomAdjun = partesRutaAdjun(UBound(partesRutaAdjun))
    
    ' Crea objetos DOMDocument y XMLHTTP
    Set objDom = CreateObject("MSXML2.DOMDocument")
    Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
    
    rutaWS = basUtilidades.ObtenerRutaWebService(moConexion)
    
    strUrl = rutaWS & "/FSN_Adjuntos.asmx"
    
   'Buscamos el numero de Especificacion maximo
    sConsulta = "SELECT MAX(ID) AS NUM FROM CIAS_ESP WHERE CIA=" & Cia
    adoRes.Open sConsulta, moConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    If adoRes.eof Then
        iNum = 1
    Else
        iNum = NullToDbl0(adoRes.Fields("NUM")) + 1
    End If
    adoRes.Close
    Set adoRes = Nothing
    nId = iNum
    
    Dim datafile As Integer
    Dim Chunks As Long
    Dim Fragment As Long
    Dim iChunk As Integer
    Dim vChunkSize As Variant
    Dim rutaFicheroTemporal As String
    
    rutaFicheroTemporal = ""
    
    'Obtener tama�o del chunk
    Dim oParam As CGestorParametros
    Set oParam = New CGestorParametros
    Set oParam.Conexion = moConexion
    vChunkSize = oParam.DevolverParametroInternoGS(Cia, "CHUNK_SIZE")
    If IsNull(vChunkSize) Or IsEmpty(vChunkSize) Then vChunkSize = giChunkSize
    
    datafile = 1
    'Abrimos el fichero para lectura binaria
    Open sTemp For Binary Access Read As datafile
    fl = LOF(datafile)    ' Length of data in file
    If fl = 0 Then
        Close datafile
    End If
    ' Se lo asignamos en bloques a la especificacion
    Chunks = fl \ vChunkSize
    Fragment = fl Mod vChunkSize
    
    If Fragment > 0 Then ' Si no ocupa  justo un chunk
        Chunks = Chunks + 1
    End If
    
    ReDim Chunk(vChunkSize - 1)
    
    'Pasar el archivo a carpeta temporal
    '-----------------------------------
    For iChunk = 1 To Chunks
        If iChunk = Chunks And Fragment > 0 Then
            ReDim Chunk(Fragment - 1)
        End If
        Get datafile, , Chunk()
        
        strBase64 = EncodeBase64(Chunk)
        strSoapAction = "http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos/GrabarCarpetaTemporal"
        
        XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
                "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
                "<soap:Body>" & _
                "<GrabarCarpetaTemporal xmlns=""http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos"">" & _
                "<Adjunto>" & strBase64 & "</Adjunto>" & _
                "<Nombre>" & msNombre & "</Nombre>" & _
                "<RutaTemporal>" & rutaFicheroTemporal & "</RutaTemporal>" & _
                "</GrabarCarpetaTemporal>" & _
                "</soap:Body>" & _
                "</soap:Envelope>"

        ' Carga XML
        objDom.async = True
        objDom.loadXML XmlBody
        ' Abre el webservice
        objXmlHttp.Open "POST", strUrl, True
        ' Crea headings
        objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
        objXmlHttp.setRequestHeader "SOAPAction", strSoapAction
        
        ' Envia XML command
        objXmlHttp.Send objDom.xml
        'Esperar hasta que se termine de ejecutar
        While Not objXmlHttp.ReadyState = 4
            DoEvents
        Wend
           
        Set parser = CreateObject("MSXML2.DOMDocument")
        strResultado = objXmlHttp.ResponseText
        parser.loadXML strResultado
        
        If InStr(strResultado, "GrabarCarpetaTemporalResult") Then
            strResultado = parser.LastChild.Text
        End If
        Dim rdo() As String
        If InStr(1, strResultado, "#") > 0 Then
            rdo = Split(strResultado, "#", , vbTextCompare)
            If rdo(0) = "1" Then
                rutaFicheroTemporal = rdo(1)
            End If
        End If
    Next
    
    Close datafile
    
    'Grabar el archivo en BD
    '-----------------------
    strSoapAction = "http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos/GrabarEspecificacionCia"
           
    XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
            "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
            "<soap:Body>" & _
            "<GrabarEspecificacionCia xmlns=""http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos"">" & _
            "<RutaTemporal>" & rutaFicheroTemporal & "</RutaTemporal>" & _
            "<Nombre>" & msNombre & "</Nombre>" & _
            "<Id>" & nId & "</Id>" & _
            "<Cia>" & Cia & "</Cia>" & _
            "<Coment>" & mvComentario & "</Coment>" & _
            "</GrabarEspecificacionCia>" & _
            "</soap:Body>" & _
            "</soap:Envelope>"
         
    ' Carga XML
    objDom.async = True
    objDom.loadXML XmlBody
    ' Abre el webservice
    objXmlHttp.Open "POST", strUrl, True
    ' Crea headings
    objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
    objXmlHttp.setRequestHeader "SOAPAction", strSoapAction
 
    ' Envia XML command
    objXmlHttp.Send objDom.xml
    'Esperar hasta que se termine de ejecutar
    While Not objXmlHttp.ReadyState = 4
        DoEvents
    Wend
       
    Set parser = CreateObject("MSXML2.DOMDocument")
    strResultado = objXmlHttp.ResponseText
    parser.loadXML strResultado
    
    If InStr(strResultado, "GrabarEspecificacionCiaResult") Then
        strResultado = parser.LastChild.Text
    End If
    
    ' Cierra object
    Set objXmlHttp = Nothing

    Dim ArrayAdjunto() As String
    ArrayAdjunto = Split(strResultado, "#", , vbTextCompare)
    miId = ArrayAdjunto(0)
    mlDataSize = ArrayAdjunto(1)

fin:
    Set guardarEspecificacionCia = oTESError
    Set fso = CreateObject("SCRIPTING.FILESYSTEMOBJECT")
    fso.DeleteFile (sTemp)
    Set fso = Nothing
    Exit Function
Error:
    If moConexion.AdoCon.Errors.Count > 0 Then
        Set oTESError = TratarError(moConexion.AdoCon.Errors)
    Else
        oTESError.NumError = TESOtroErrorNOBD
    End If

    Resume fin
End Function

''' <summary>
''' Eliminar una especificaci�n de la compania
''' </summary>
''' <param name="Cia">id de compania</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: usuppal/eliminarespec.asp; Tiempo m�ximo: 0,1</remarks>
Public Function EliminarCiaEsp(ByVal Cia As Long) As CTESError
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim sConsulta As String
    Dim TESError As CTESError
    
    On Error GoTo Error:
    
    Set TESError = New CTESError
    TESError.NumError = 0
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = moConexion.AdoCon
    adoComm.CommandType = adCmdText
    adoComm.Prepared = True
    
    sConsulta = "DELETE FROM CIAS_ESP WHERE CIA=? AND ID= ?"
    Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=Cia)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter(Type:=adSmallInt, Direction:=adParamInput, Value:=miId)
    adoComm.Parameters.Append adoParam
    
    adoComm.CommandText = sConsulta
    
    adoComm.Execute
    
    Set EliminarCiaEsp = TESError
    
    Set adoParam = Nothing
    Set adoComm = Nothing
    
    Exit Function
    
Error:
    
    If moConexion.AdoCon.Errors.Count > 0 Then
        Set TESError = basErrores.TratarError(moConexion.AdoCon.Errors)
        moConexion.AdoCon.Execute "SET XACT_ABORT ON"
    Else
        TESError.NumError = TESOtroErrorNOBD
    End If
    Set EliminarCiaEsp = TESError
    
    Set adoParam = Nothing
    Set adoComm = Nothing
End Function

''' <summary>
''' Modificar una especificaci�n de la compania
''' </summary>
''' <param name="Cia">id de compania</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: usuppal/eliminarespec.asp; Tiempo m�ximo: 0,1</remarks>
Public Function ModificarCiaEsp(ByVal Cia As Long) As CTESError
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim sConsulta As String
    Dim TESError As CTESError
    
    On Error GoTo Error:
    
    Set TESError = New CTESError
    TESError.NumError = 0
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = moConexion.AdoCon
    adoComm.CommandType = adCmdText
    adoComm.Prepared = True
    
    sConsulta = "UPDATE CIAS_ESP SET NOM= ?, COM= ? WHERE CIA= ? AND ID= ?"
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=300, Value:=msNombre)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=500, Value:=StrToVbNULL(mvComentario))
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=Cia)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter(Type:=adSmallInt, Direction:=adParamInput, Value:=miId)
    adoComm.Parameters.Append adoParam
    
    adoComm.CommandText = sConsulta
    
    adoComm.Execute
    
    Set ModificarCiaEsp = TESError
    
    Set adoParam = Nothing
    Set adoComm = Nothing
    
    Exit Function
    
Error:

    If moConexion.AdoCon.Errors.Count > 0 Then
        Set TESError = basErrores.TratarError(moConexion.AdoCon.Errors)
        moConexion.AdoCon.Execute "SET XACT_ABORT ON"
    Else
        TESError.NumError = TESOtroErrorNOBD
    End If
    Set ModificarCiaEsp = TESError
    
    Set adoParam = Nothing
    Set adoComm = Nothing
End Function
''' <summary>
''' Lee el especificaciones de proceso de BD GS, se usa el webservice de fsnweb
''' </summary>
''' <param name="lCiaComp">cia compradora</param>
''' <param name="Tamanyo">tama�o</param>
''' <param name="sFileName">nombre fichero</param>
''' <remarks>Llamada desde: CEspecificacion.EscribirADisco; CEspecificacion.DescargarEspec; Tiempo m�ximo: 0,0</remarks>
''' Revisado por: epb; Fecha: 31/08/2015
Private Sub LeerAdjunto(ByVal lCiaComp As Long, Optional ByVal Tamanyo As Long, Optional ByVal sFileName As String)
    Dim objDom As Object
    Dim objXmlHttp As Object
    Dim parser As Object
    Dim XmlBody As String
    Dim strUrl As String
    Dim strSoapAction As String
    Dim strResultado As String
    Dim Tipo As Integer
    Dim Grupo As Long
    Dim Proce As Long
    Dim Gmn1 As String
    Dim Anyo As Integer
    Dim rutaWS As String
    
    ' Crea objetos DOMDocument y XMLHTTP
    Set objDom = CreateObject("MSXML2.DOMDocument")
    Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
    
    rutaWS = basUtilidades.ObtenerRutaWebServiceGS(moConexion)
        
    If Not moItem Is Nothing Then
        Tipo = 2
        Grupo = moItem.id
        Proce = moProceso.Cod
        Gmn1 = moProceso.GMN1Cod
        Anyo = moProceso.Anyo
    ElseIf Not moGrupo Is Nothing Then
        Tipo = 1
        Grupo = moGrupo.id
        Proce = moProceso.Cod
        Gmn1 = moProceso.GMN1Cod
        Anyo = moProceso.Anyo
    Else
        Tipo = 0
        Proce = moProceso.Cod
        Gmn1 = moProceso.GMN1Cod
        Anyo = moProceso.Anyo
    End If
    
    strUrl = rutaWS & "/FSN_Adjuntos.asmx"
    strSoapAction = "http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos/LeerEspecificacion"
           
    Dim Chunks As Long
    Dim Fragment As Long
    Dim arrBytes() As Byte
    Dim datafile As Integer
    Dim ChunkNumber As Long
    Dim vChunkSize As Variant
    
    'Obtener tama�o del chunk
    Dim oParam As CGestorParametros
    Set oParam = New CGestorParametros
    Set oParam.Conexion = moConexion
    vChunkSize = oParam.DevolverParametroInternoGS(lCiaComp, "CHUNK_SIZE")
    If IsNull(vChunkSize) Or IsEmpty(vChunkSize) Then vChunkSize = giChunkSize
    
    Chunks = Tamanyo \ vChunkSize
    Fragment = Tamanyo Mod vChunkSize
    
    If Fragment > 0 Then ' Si no ocupa  justo un chunk
        Chunks = Chunks + 1
    End If

    datafile = 1
    
    'Abrimos el fichero para escritura binaria
    Open sFileName For Binary Access Write As datafile
    
    For ChunkNumber = 1 To Chunks
        XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
                "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
                "<soap:Body>" & _
                "<LeerEspecificacion xmlns=""http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos"">" & _
                "<Id>" & miId & "</Id>" & _
                "<Tipo>" & Tipo & "</Tipo>" & _
                "<Grupo>" & Grupo & "</Grupo>" & _
                "<Proce>" & Proce & "</Proce>" & _
                "<Gmn1>" & Gmn1 & "</Gmn1>" & _
                "<Anyo>" & Anyo & "</Anyo>" & _
                "<ChunkNumber>" & ChunkNumber & "</ChunkNumber>" & _
                "<ChunkSize>" & vChunkSize & "</ChunkSize>" & _
                "</LeerEspecificacion>" & _
                "</soap:Body>" & _
                "</soap:Envelope>"
    
        ' Carga XML
        objDom.async = True
        objDom.loadXML XmlBody
        ' Abre el webservice
        objXmlHttp.Open "POST", strUrl, True
        ' Crea headings
        objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
        objXmlHttp.setRequestHeader "SOAPAction", strSoapAction
     
        ' Envia XML command
        objXmlHttp.Send objDom.xml
        'Esperar hasta que se termine de ejecutar
        While Not objXmlHttp.ReadyState = 4
            DoEvents
        Wend
           
        Set parser = CreateObject("MSXML2.DOMDocument")
        strResultado = objXmlHttp.ResponseText
        parser.loadXML strResultado
        
        If InStr(strResultado, "LeerEspecificacion") Then
            strResultado = parser.LastChild.Text
            arrBytes = DecodeBase64(strResultado)
            Put datafile, , arrBytes
        End If
    Next
    Close datafile
    ' Cierra object
    Set objXmlHttp = Nothing
End Sub


''' <summary>
''' Lee el especificaciones de cia, se usa el webservice de portal si hay
''' </summary>
''' <param name="miId">id archivo</param>
''' <param name="lCiaComp">cia compradora</param>
''' <param name="Tamanyo">tama�o</param>
''' <param name="sFileName">nombre fichero</param>
''' <remarks>Llamada desde: CEspecificacion.EscribirADisco;  Tiempo m�ximo: 0,0</remarks>
''' Revisado por: epb; Fecha: 31/08/2015
Private Sub LeerAdjuntoCia(ByVal miId As Long, ByVal lCiaComp As Long, Optional ByVal Tamanyo As Long, Optional ByVal sFileName As String)
    Dim objDom As Object
    Dim objXmlHttp As Object
    Dim parser As Object
    Dim XmlBody As String
    Dim strUrl As String
    Dim strSoapAction As String
    Dim strResultado As String
    Dim rutaWS As String
    
    rutaWS = basUtilidades.ObtenerRutaWebService(moConexion)
       
    ' Crea objetos DOMDocument y XMLHTTP
    Set objDom = CreateObject("MSXML2.DOMDocument")
    Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
       
    strUrl = rutaWS & "/FSN_Adjuntos.asmx"
    strSoapAction = "http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos/LeerEspecificacionCia"
    
    Dim Chunks As Long
    Dim Fragment As Long
    Dim arrBytes() As Byte
    Dim datafile As Integer
    Dim ChunkNumber As Long
    Dim vChunkSize As Variant
    
    'Obtener tama�o del chunk
    Dim oParam As CGestorParametros
    Set oParam = New CGestorParametros
    Set oParam.Conexion = moConexion
    vChunkSize = oParam.DevolverParametroInternoGS(lCiaComp, "CHUNK_SIZE")
    If IsNull(vChunkSize) Or IsEmpty(vChunkSize) Then vChunkSize = giChunkSize
    
    Chunks = Tamanyo \ vChunkSize
    Fragment = Tamanyo Mod vChunkSize
    
    If Fragment > 0 Then ' Si no ocupa  justo un chunk
        Chunks = Chunks + 1
    End If

    datafile = 1
    
    'Abrimos el fichero para escritura binaria
    Open sFileName For Binary Access Write As datafile
           
    For ChunkNumber = 1 To Chunks
        XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
                "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
                "<soap:Body>" & _
                "<LeerEspecificacionCia xmlns=""http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos"">" & _
                "<Id>" & miId & "</Id>" & _
                "<Cia>" & lCiaComp & "</Cia>" & _
                "<ChunkNumber>" & ChunkNumber & "</ChunkNumber>" & _
                "<ChunkSize>" & vChunkSize & "</ChunkSize>" & _
                "</LeerEspecificacionCia>" & _
                "</soap:Body>" & _
                "</soap:Envelope>"
    
        ' Carga XML
        objDom.async = True
        objDom.loadXML XmlBody
        ' Abre el webservice
        objXmlHttp.Open "POST", strUrl, True
        ' Crea headings
        objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
        objXmlHttp.setRequestHeader "SOAPAction", strSoapAction
     
        ' Envia XML command
        objXmlHttp.Send objDom.xml
        'Esperar hasta que se termine de ejecutar
        While Not objXmlHttp.ReadyState = 4
            DoEvents
        Wend
           
        Set parser = CreateObject("MSXML2.DOMDocument")
        strResultado = objXmlHttp.ResponseText
        parser.loadXML strResultado
        
        If InStr(strResultado, "LeerEspecificacionCiaResult") Then
            strResultado = parser.LastChild.Text
            arrBytes = DecodeBase64(strResultado)
            Put datafile, , arrBytes
        End If
    Next
    
    Close datafile
    
    ' Cierra object
    Set objXmlHttp = Nothing
End Sub


