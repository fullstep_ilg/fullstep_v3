VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAtributoOfertado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' Variables privadas con la informacion de un valor

Private mvValor As Variant
Private moAtributo As CAtributo
Private moObjeto As Object
Private mvIndice As Long
Private mvTag As Boolean
Private mvImporteParcial As Variant
Private mbHayEscalados As Boolean
Private m_oEscalados As CEscalados ' Posibles escalados para costes/descuentos



Public Property Get Indice() As Long

    ''' * Objetivo: Devolver el indice del pais en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice del pais en la coleccion

    Indice = mvIndice
    
End Property
Public Property Let Indice(ByVal lInd As Long)

    ''' * Objetivo: Dar valor al indice del pais en la coleccion
    ''' * Recibe: Indice del pais en la coleccion
    ''' * Devuelve: Nada

    mvIndice = lInd
    
End Property


Public Property Let Tag(ByVal vData As Boolean)
    mvTag = vData
End Property

Public Property Get Tag() As Boolean
    Tag = mvTag
End Property


Public Property Let Valor(ByVal vData As Variant)
On Error Resume Next
    If IsEmpty(vData) Then
        mvValor = Null
        Exit Property
    End If
    If IsNull(vData) Then
        mvValor = Null
        Exit Property
    End If
    If Not moAtributo Is Nothing Then
        Select Case moAtributo.tipo
            Case 1:
                mvValor = CStr(vData)
            Case 2:
                mvValor = CDbl(vData)
            Case 3:
                mvValor = CDate(vData)
            Case 4:
                mvValor = CBool(vData)
        End Select
    Else
        mvValor = vData
    End If
                
If IsEmpty(mvValor) Then
    mvValor = Null
End If
    
  
End Property
Public Property Get Valor() As Variant

    Valor = mvValor
    
End Property

Public Property Set Atributo(ByRef oAtrib As CAtributo)

    Set moAtributo = oAtrib
        
End Property
Public Property Get Atributo() As CAtributo

    Set Atributo = moAtributo
    
End Property

Public Property Set Objeto(ByRef oObj As Object)

    Set moObjeto = oObj
    
End Property
Public Property Get Objeto() As Object

    Set Objeto = moObjeto
    
End Property


Public Property Let ImporteParcial(ByVal vData As Variant)
    mvImporteParcial = vData
End Property

Public Property Get ImporteParcial() As Variant

    ImporteParcial = mvImporteParcial
    
End Property


'DPD - Propiedades para el manejo de precios escalados
Public Property Get Escalados() As CEscalados
    Set Escalados = m_oEscalados
End Property

Public Property Set Escalados(ByVal oEscalados As CEscalados)
    Set m_oEscalados = oEscalados
End Property


Public Property Get HayEscalados() As Boolean
    HayEscalados = mbHayEscalados
End Property
Public Property Let HayEscalados(ByVal bValor As Boolean)
    mbHayEscalados = bValor
End Property

