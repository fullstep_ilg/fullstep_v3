VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CProvincia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    FaltaObjetoPais = 614
    
End Enum
Private mvarConexion As CConexion 'local copy
Private mvarIndice As Long 'local copy
Private mvarResultset
Private mvarCod As String 'local copy
Private mvarDen As String 'local copy
Private mvarPais As CPais 'local copy
Private mvarId As Integer

Public Property Let ID(ByVal Data As Integer)
    Let mvarId = Data
End Property
Public Property Get ID() As Integer
    ID = mvarId
End Property


Public Property Set Pais(ByVal vData As CPais)
    Set mvarPais = vData
End Property


Public Property Get Pais() As CPais
    Set Pais = mvarPais
End Property

Public Property Let Indice(ByVal vData As Long)
    mvarIndice = vData
End Property


Public Property Get Indice() As Long
    Indice = mvarIndice
End Property



Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property



Public Property Let Den(ByVal vData As String)
    mvarDen = vData
End Property


Public Property Get Den() As String
    Den = mvarDen
End Property



Public Property Let Cod(ByVal vData As String)
    mvarCod = vData
End Property


Public Property Get Cod() As String
    Cod = mvarCod
End Property


