VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CValoresPond"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mCol As Collection
Private m_oConexion As CConexion
'Private oValorPond As CValorPond
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property
Friend Property Set Conexion(ByVal con As CConexion)
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    Set Conexion = m_oConexion
    
End Property


Public Property Get Item(vntIndexKey As Variant) As CValorPond
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)

    Exit Property

NoSeEncuentra:
    Set Item = Nothing

End Property
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
       Set NewEnum = mCol.[_NewEnum]
End Property
Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub
Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property
Public Function Add(Optional ByVal IDAtributo As Long, Optional ByVal AnyoProce As Variant, Optional ByVal GMN1Proce As Variant, Optional ByVal CodProce As Variant, Optional ByVal CodGrupo As Variant, Optional ByVal IdOrden As Long, _
                    Optional ByVal ValorLista As Variant, Optional ByVal ValorPond As Variant, Optional ByVal ValorDesde As Variant, Optional ByVal ValorHasta As Variant, Optional ByVal FechaActualizacion As Date, Optional ByVal varIndice As Variant) As CValorPond
   
    Dim objnewmember As CValorPond
    
    Set objnewmember = New CValorPond
    Set objnewmember.Conexion = m_oConexion
    
'    If IsMissing(IDAtributo) Then
'        objnewmember.ID = Null
'    Else
        objnewmember.IDAtributo = IDAtributo
'    End If
    
    If IsMissing(AnyoProce) Then
        objnewmember.AnyoProce = Null
    Else
        objnewmember.AnyoProce = AnyoProce
    End If
    If IsMissing(GMN1Proce) Then
        objnewmember.GMN1Proce = Null
    Else
        objnewmember.GMN1Proce = GMN1Proce
    End If
    If IsMissing(CodProce) Then
        objnewmember.CodProce = Null
    Else
        objnewmember.CodProce = CodProce
    End If
    If IsMissing(CodGrupo) Then
        objnewmember.CodGrupo = Null
    Else
        objnewmember.CodGrupo = CodGrupo
    End If
    
    objnewmember.IdOrden = IdOrden
    
    If IsMissing(ValorLista) Then
        objnewmember.ValorLista = Null
    Else
        objnewmember.ValorLista = ValorLista
    End If
    If IsMissing(ValorPond) Then
        objnewmember.ValorPond = Null
    Else
        objnewmember.ValorPond = ValorPond
    End If
    If IsMissing(ValorDesde) Then
        objnewmember.ValorDesde = Null
    Else
        objnewmember.ValorDesde = ValorDesde
    End If
    If IsMissing(ValorHasta) Then
        objnewmember.ValorHasta = Null
    Else
        objnewmember.ValorHasta = ValorHasta
    End If
    
    
    objnewmember.FechaActualizacion = FechaActualizacion
    
    
    Set objnewmember.Conexion = m_oConexion
      
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        mCol.Add objnewmember, CStr(IDAtributo)
    End If
    
    
    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function
Private Sub Class_Initialize()

    Set mCol = New Collection

End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub



