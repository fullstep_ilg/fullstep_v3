VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPais"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''' *** Clase: CPais
''' *** Creacion: 26/10/1998 (Javier Arana)
''' *** Ultima revision: 18/01/1999 (Alfredo Magallon)

Option Explicit

''' Variables privadas con la informacion de un pais

Private mvarCod As String
Private mvarDen As String
Private mvarMoneda As CMoneda
Private mvarProvincias As CProvincias
Private mvarPortal As Boolean
Private mvarId As Integer
Private mvarIdioma As CIdioma
Private miValidNif As Integer

''' Conexion

Private mvarConexion As CConexion

''' Control de errores

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

''' Indice del pais en la coleccion

Private mvarIndice As Long

Friend Property Let Portal(ByVal Data As Boolean)
    mvarPortal = Data
End Property

Public Property Get Indice() As Long

    ''' * Objetivo: Devolver el indice del pais en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice del pais en la coleccion

    Indice = mvarIndice
    
End Property
Public Property Let Indice(ByVal lInd As Long)

    ''' * Objetivo: Dar valor al indice del pais en la coleccion
    ''' * Recibe: Indice del pais en la coleccion
    ''' * Devuelve: Nada

    mvarIndice = lInd
    
End Property
Public Property Set Moneda(ByVal vData As CMoneda)

     Set mvarMoneda = vData
    
End Property
Public Property Get Moneda() As CMoneda

      Set Moneda = mvarMoneda
    
End Property
Public Property Set Idioma(ByVal vData As CIdioma)

     Set mvarIdioma = vData
    
End Property
Public Property Get Idioma() As CIdioma

      Set Idioma = mvarIdioma
    
End Property
Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion del pais
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set mvarConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion del pais
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = mvarConexion
    
End Property
Public Property Set Provincias(ByVal vData As CProvincias)

    ''' * Objetivo: Dar valor a la variable privada Provincias
    ''' * Recibe: Provincias del pais
    ''' * Devuelve: Nada

    Set mvarProvincias = vData
    
End Property
Public Property Get Provincias() As CProvincias

    ''' * Objetivo: Devolver la variable privada Provincias
    ''' * Recibe: Nada
    ''' * Devuelve: Provincias del pais
    
    Set Provincias = mvarProvincias
    
End Property
Public Property Let Den(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada DEN
    ''' * Recibe: Denominacion del pais
    ''' * Devuelve: Nada
    
    mvarDen = vData
    
End Property
Public Property Get Den() As String

    ''' * Objetivo: Devolver la variable privada DEN
    ''' * Recibe: Nada
    ''' * Devuelve: Denominacion del pais

    Den = mvarDen
    
End Property

Public Property Let ValidNif(ByVal Data As Integer)
    
    Let miValidNif = Data
    
End Property
Public Property Get ValidNif() As Integer
    
    ValidNif = miValidNif
    
End Property
Public Property Let id(ByVal Data As Integer)
    
    Let mvarId = Data
    
End Property
Public Property Get id() As Integer
    
    id = mvarId
    
End Property
Public Property Let Cod(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo del pais
    ''' * Devuelve: Nada

    mvarCod = vData
    
End Property
Public Property Get Cod() As String

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo del pais

    Cod = mvarCod
    
End Property

Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar la memoria
    
    Set mvarMoneda = Nothing
    Set mvarProvincias = Nothing
  
    Set mvarConexion = Nothing
    
End Sub

''' <summary>
''' Cargar las provincias del pais
''' </summary>
''' <param name="CarIniCod">Caracteres por los q empieza el c�digo</param>
''' <param name="CarIniDen">Caracteres por los q empieza la denominaci�n</param>
''' <param name="CoincidenciaTotal">true usar =    false usar like </param>
''' <param name="OrdenadasPorDen">Ordenar por codigo o denominaci�n</param>
''' <param name="UsarIndice">Al crear la clase CProvincias cada registro identificarlo por COD � por numeros correlativos</param>
''' <remarks>Llamada desde: provincias.asp      registro.asp    detallecia.asp
'''     modifcompaniaclient.asp     modifcompaniaserver.asp; Tiempo m�ximo:0,1</remarks>
Public Sub CargarTodasLasProvincias(Optional ByVal CarIniCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadasPorDen As Boolean = False, Optional ByVal UsarIndice As Boolean = False)
    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim ador As ADODB.Recordset
    Dim sConsulta As String
    Dim lIndice As Long

    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPais.CargarTodasLasdivisiones", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
       
    Set adoComm = New ADODB.Command
    Set adoComm.ActiveConnection = mvarConexion.AdoCon
    adoComm.CommandType = adCmdText
       
    sConsulta = " DECLARE @IDIOMA INT "
    If Not mvarIdioma Is Nothing Then
        sConsulta = sConsulta & " SELECT @IDIOMA= ID FROM IDI WITH (NOLOCK) WHERE COD='" & mvarIdioma.Cod & "' "
    Else
        sConsulta = sConsulta & " SELECT @IDIOMA= ID FROM IDI WITH (NOLOCK) WHERE COD='ENG' "
    End If
    sConsulta = sConsulta & " SELECT PROVI.ID,PROVI.COD,PROVI_DEN.DEN FROM PROVI WITH(NOLOCK) "
    sConsulta = sConsulta & " LEFT JOIN PROVI_DEN WITH(NOLOCK) ON PROVI_DEN.PAI = PROVI.PAI AND PROVI_DEN.PROVI = PROVI.ID AND PROVI_DEN.IDIOMA=@IDIOMA"
    sConsulta = sConsulta & " WHERE PROVI.PAI= ?"
    
    Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=mvarId)
    adoComm.Parameters.Append adoParam
    
    If CarIniCod = "" And CarIniDen = "" Then
    Else
        If Not (CarIniCod = "") And Not (CarIniDen = "") Then
        
            If CoincidenciaTotal Then
                sConsulta = sConsulta & " AND COD= ?"
                sConsulta = sConsulta & " AND DEN= ?"
            Else
                sConsulta = sConsulta & " AND PROVI.COD LIKE ?"
                sConsulta = sConsulta & " AND PROVI_DEN.DEN LIKE ?"
                
                CarIniCod = CarIniCod & "%"
                CarIniDen = CarIniDen & "%"
            End If
            
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(CarIniCod), Value:=CarIniCod)
            adoComm.Parameters.Append adoParam
            
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(CarIniDen), Value:=CarIniDen)
            adoComm.Parameters.Append adoParam
            
        Else
            If Not (CarIniCod = "") Then
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & " AND PROVI.COD = ?"
                Else
                    sConsulta = sConsulta & " AND PROVI.COD LIKE ?"
                    CarIniCod = CarIniCod & "%"
                End If
                
                Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(CarIniCod), Value:=CarIniCod)
                adoComm.Parameters.Append adoParam
            Else
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & " AND PROVI_DEN.DEN = ?"
                Else
                    sConsulta = sConsulta & " AND PROVI_DEN.DEN LIKE ?"
                    CarIniDen = CarIniDen & "%"
                End If
            
                Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(CarIniDen), Value:=CarIniDen)
                adoComm.Parameters.Append adoParam
            End If
        End If
    End If
    
    If OrdenadasPorDen Then
        sConsulta = sConsulta & " ORDER BY PROVI_DEN.DEN,PROVI.COD"
    Else
        sConsulta = sConsulta & " ORDER BY PROVI.COD,PROVI_DEN.DEN"
    End If
        
    adoComm.CommandText = sConsulta
    Set ador = New ADODB.Recordset
        
    Set ador = adoComm.Execute
          
    If ador.eof Then
            
        ador.Close
        Set ador = Nothing
        Set mvarProvincias = Nothing
        Set mvarProvincias = New CProvincias
        Set mvarProvincias.Conexion = mvarConexion
          
    Else
         
        Set mvarProvincias = Nothing
        Set mvarProvincias = New CProvincias
        Set mvarProvincias.Conexion = mvarConexion
        
        mvarProvincias.Portal = True
        
        If UsarIndice Then
            
            lIndice = 0
            
            While Not ador.eof
                mvarProvincias.Add ador("COD"), ador("DEN"), lIndice, ador("ID")
                ador.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
                While Not ador.eof
                    mvarProvincias.Add ador("COD"), ador("DEN"), , ador("ID").Value
                    ador.MoveNext
                Wend
        End If
        
        ador.Close
        Set ador = Nothing
          
    End If

    Set adoParam = Nothing
    Set adoComm = Nothing
End Sub

Public Function DevolverLosCodigosDeProvincias() As TipoDatosCombo
   
   ''' ! Pendiente para cuando veamos lo de provincias
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
   
   If mvarProvincias Is Nothing Then
        DevolverLosCodigosDeProvincias = Codigos
        Exit Function
   End If
    
   ReDim Codigos.Cod(mvarProvincias.Count)
   ReDim Codigos.Den(mvarProvincias.Count)
   ReDim Codigos.id(mvarProvincias.Count)
    
   
    For iCont = 0 To mvarProvincias.Count - 1
        
        Codigos.Cod(iCont) = mvarProvincias.Item(iCont + 1).Cod
        Codigos.Den(iCont) = mvarProvincias.Item(iCont + 1).Den
        Codigos.id(iCont) = mvarProvincias.Item(iCont + 1).id
    Next

   DevolverLosCodigosDeProvincias = Codigos
   
End Function


