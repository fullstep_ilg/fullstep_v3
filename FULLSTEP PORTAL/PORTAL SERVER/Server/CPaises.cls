VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPaises"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
''' *** Clase: CMonedas
''' *** Creacion: 1/09/1998 (Javier Arana)
''' *** Ultima revision: 14/01/1999 (Alfredo Magallon)

''' Variables que almacenan las propiedades de los objetos

Private mCol As Collection
Private mvarEOF As Boolean
Private mvarConexion As CConexion
Private mvarPortal As Boolean

''' Control de errores

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Friend Property Let Portal(ByVal Data As Boolean)
    
    Let mvarPortal = Data
    
End Property

''' <summary>
''' Cargar la coleccion de paises
''' </summary>
''' <param name="sIdi">Idioma</param>
''' <param name="IdPais">Pais</param>
''' <param name="CaracteresInicialesCod">Caracteres por los q empieza el c�digo</param>
''' <param name="CaracteresInicialesDen">Caracteres por los q empieza la denominaci�n</param>
''' <param name="CoincidenciaTotal">true usar =    false usar like </param>
''' <param name="OrdenadasPorDen">Ordenar por denominaci�n o no</param>
''' <param name="OrdenadosPorMoneda">Ordenar por moneda o no</param>
''' <param name="UsarIndice">Al crear la clase cada registro identificarlo por PAIS.COD � por numeros correlativos</param>
''' <remarks>Llamada desde: registro.asp    detallecia.asp    modifcompaniaclient.asp; Tiempo m�ximo: 0,1</remarks>
Public Sub CargarTodosLosPaises(ByVal sIdi As String, Optional ByVal IdPais As Variant, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean = False, Optional ByVal OrdenadosPorDen As Boolean = False, Optional ByVal OrdenadosPorMoneda As Boolean = False, Optional ByVal UsarIndice As Boolean = False)
    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim ador As ADODB.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
   
    
    ''' Precondicion
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDivisiones.CargarTodasLasdivisiones", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    sIdi = EsIdiomaValido(sIdi)
    
    Set adoComm = New ADODB.Command
    Set adoComm.ActiveConnection = mvarConexion.AdoCon
    adoComm.CommandType = adCmdText
    
    ''' Generacion del SQL a partir de los parametros
    
    sConsulta = " DECLARE @IDIOMA INT "
    sConsulta = sConsulta & " SELECT @IDIOMA= ID FROM IDI WITH (NOLOCK) WHERE COD='" & sIdi & "' "

    If Not IsMissing(IdPais) Then
        sConsulta = sConsulta & " SELECT PAI.ID,PAI.COD AS PAICOD,PAI_DEN.DEN AS PAIDEN,PAI.MON AS PAIMON,MON.COD AS MONCOD, MON_DEN.DEN AS MONDEN,MON.FECACT AS MONFECACT,MON.ID AS MONID,IDI.ID AS IDIID,IDI.COD AS IDICOD,IDI.DEN AS IDIDEN, VALIDNIF"
        sConsulta = sConsulta & " FROM PAI WITH (NOLOCK) LEFT JOIN MON WITH(NOLOCK) ON PAI.MON=MON.ID "
        sConsulta = sConsulta & " LEFT JOIN MON_DEN WITH(NOLOCK) ON MON_DEN.MON=MON.ID AND MON_DEN.IDIOMA =@IDIOMA "
        sConsulta = sConsulta & " LEFT JOIN PAI_DEN WITH(NOLOCK) ON PAI_DEN.PAI=PAI.ID AND PAI_DEN.IDIOMA =@IDIOMA "
        sConsulta = sConsulta & " LEFT JOIN IDI WITH(NOLOCK) ON PAI_DEN.IDIOMA=IDI.ID "
        sConsulta = sConsulta & " WHERE PAI.ID= ?"
        
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=IdPais)
        adoComm.Parameters.Append adoParam
    Else
        If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
            sConsulta = sConsulta & " SELECT PAI.ID,PAI.COD AS PAICOD,PAI_DEN.DEN AS PAIDEN,PAI.MON AS PAIMON,MON.COD AS MONCOD, MON_DEN.DEN AS MONDEN,MON.FECACT AS MONFECACT,MON.ID AS MONID,IDI.ID AS IDIID,IDI.COD AS IDICOD,IDI.DEN AS IDIDEN, VALIDNIF"
            sConsulta = sConsulta & " FROM PAI WITH (NOLOCK)  LEFT JOIN MON WITH(NOLOCK) ON PAI.MON=MON.ID "
            sConsulta = sConsulta & " LEFT JOIN MON_DEN WITH(NOLOCK) ON MON_DEN.MON=MON.ID AND MON_DEN.IDIOMA =@IDIOMA "
            sConsulta = sConsulta & " LEFT JOIN PAI_DEN WITH(NOLOCK) ON PAI_DEN.PAI=PAI.ID AND PAI_DEN.IDIOMA =@IDIOMA "
            sConsulta = sConsulta & " LEFT JOIN IDI WITH(NOLOCK) ON PAI_DEN.IDIOMA=IDI.ID "
            

        Else
            If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
                
                If CoincidenciaTotal Then
                        sConsulta = sConsulta & " SELECT PAI.ID,PAI.COD AS PAICOD,PAI_DEN.DEN AS PAIDEN,PAI.MON AS PAIMON,MON.COD AS MONCOD, MON_DEN.DEN AS MONDEN,MON.FECACT AS MONFECACT,MON.ID AS MONID ,IDI.ID AS IDIID,IDI.COD AS IDICOD,IDI.DEN AS IDIDEN, VALIDNIF"
                        sConsulta = sConsulta & " FROM PAI WITH (NOLOCK) LEFT JOIN MON WITH(NOLOCK) ON PAI.MON = MON.ID"
                        sConsulta = sConsulta & " LEFT JOIN MON_DEN WITH(NOLOCK) ON MON_DEN.MON=MON.ID AND MON_DEN.IDIOMA =@IDIOMA "
                        sConsulta = sConsulta & " LEFT JOIN PAI_DEN WITH(NOLOCK) ON PAI_DEN.PAI=PAI.ID AND PAI_DEN.IDIOMA =@IDIOMA "
                        sConsulta = sConsulta & " LEFT JOIN IDI WITH(NOLOCK) ON PAI_DEN.IDIOMA=IDI.ID "
                        sConsulta = sConsulta & " WHERE PAI.COD =?"
                        sConsulta = sConsulta & " AND PAI_DEN.DEN=?"
                Else
                        sConsulta = sConsulta & " SELECT PAI.ID,PAI.COD AS PAICOD,PAI_DEN.DEN AS PAIDEN,PAI.MON AS PAIMON,MON.COD AS MONCOD, MON_DEN.DEN AS MONDEN,MON.FECACT AS MONFECACT,MON.ID AS MONID,IDI.ID AS IDIID,IDI.COD AS IDICOD,IDI.DEN AS IDIDEN, VALIDNIF"
                        sConsulta = sConsulta & " FROM PAI WITH (NOLOCK) LEFT JOIN MON WITH(NOLOCK) ON PAI.MON = MON.ID"
                        sConsulta = sConsulta & " LEFT JOIN MON_DEN WITH(NOLOCK) ON MON_DEN.MON=MON.ID AND MON_DEN.IDIOMA =@IDIOMA "
                        sConsulta = sConsulta & " LEFT JOIN PAI_DEN WITH(NOLOCK) ON PAI_DEN.PAI=PAI.ID AND PAI_DEN.IDIOMA =@IDIOMA "
                        sConsulta = sConsulta & " LEFT JOIN IDI WITH(NOLOCK) ON PAI_DEN.IDIOMA=IDI.ID "
                        sConsulta = sConsulta & " WHERE PAI.COD LIKE ?"
                        sConsulta = sConsulta & " AND PAI_DEN.DEN LIKE ?"
                        
                        CaracteresInicialesCod = CaracteresInicialesCod & "%"
                        CaracteresInicialesDen = CaracteresInicialesDen & "%"
                End If
                
                Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(CaracteresInicialesCod), Value:=CaracteresInicialesCod)
                adoComm.Parameters.Append adoParam
                
                Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(CaracteresInicialesDen), Value:=CaracteresInicialesDen)
                adoComm.Parameters.Append adoParam
            Else
                
                If Not (CaracteresInicialesCod = "") Then
                    
                    If CoincidenciaTotal Then
                    
                         sConsulta = sConsulta & " SELECT PAI.ID,PAI.COD AS PAICOD,PAI_DEN.DEN AS PAIDEN,PAI.MON AS PAIMON,MON.COD AS MONCOD, MON_DEN.DEN AS MONDEN,MON.FECACT AS MONFECACT,MON.ID AS MONID ,IDI.ID AS IDIID,IDI.COD AS IDICOD,IDI.DEN AS IDIDEN, VALIDNIF"
                         sConsulta = sConsulta & " FROM PAI WITH (NOLOCK) LEFT JOIN MON WITH(NOLOCK) ON PAI.MON = MON.ID"
                         sConsulta = sConsulta & " LEFT JOIN MON_DEN WITH(NOLOCK) ON MON_DEN.MON=MON.ID AND MON_DEN.IDIOMA =@IDIOMA "
                         sConsulta = sConsulta & " LEFT JOIN PAI_DEN WITH(NOLOCK) ON PAI_DEN.PAI=PAI.ID AND PAI_DEN.IDIOMA =@IDIOMA "
                         sConsulta = sConsulta & " LEFT JOIN IDI WITH(NOLOCK) ON PAI_DEN.IDIOMA=IDI.ID "
                         sConsulta = sConsulta & " WHERE PAI.COD = ?"
                    
                    Else
                        sConsulta = sConsulta & " SELECT PAI.ID,PAI.COD AS PAICOD,PAI_DEN.DEN AS PAIDEN,PAI.MON AS PAIMON,MON.COD AS MONCOD, MON_DEN.DEN AS MONDEN,MON.FECACT AS MONFECACT,MON.ID AS MONID ,IDI.ID AS IDIID,IDI.COD AS IDICOD,IDI.DEN AS IDIDEN, VALIDNIF"
                        sConsulta = sConsulta & " FROM PAI WITH (NOLOCK) LEFT JOIN MON WITH(NOLOCK) ON PAI.MON = MON.ID"
                        sConsulta = sConsulta & " LEFT JOIN MON_DEN WITH(NOLOCK) ON MON_DEN.MON=MON.ID AND MON_DEN.IDIOMA =@IDIOMA "
                        sConsulta = sConsulta & " LEFT JOIN PAI_DEN WITH(NOLOCK) ON PAI_DEN.PAI=PAI.ID AND PAI_DEN.IDIOMA =@IDIOMA "
                        sConsulta = sConsulta & " LEFT JOIN IDI WITH(NOLOCK) ON PAI_DEN.IDIOMA=IDI.ID "
                        sConsulta = sConsulta & " WHERE PAI.COD LIKE ?"
                        
                        CaracteresInicialesCod = CaracteresInicialesCod & "%"
                    End If
                    
                    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(CaracteresInicialesCod), Value:=CaracteresInicialesCod)
                    adoComm.Parameters.Append adoParam
                Else
                    
                    If CoincidenciaTotal Then
                        sConsulta = sConsulta & " SELECT PAI.ID,PAI.COD AS PAICOD,PAI_DEN.DEN AS PAIDEN,PAI.MON AS PAIMON,MON.COD AS MONCOD, MON_DEN.DEN AS MONDEN,MON.FECACT AS MONFECACT,MON.ID AS MONID ,IDI.ID AS IDIID,IDI.COD AS IDICOD,IDI.DEN AS IDIDEN, VALIDNIF"
                        sConsulta = sConsulta & " FROM PAI WITH (NOLOCK) LEFT JOIN MON WITH(NOLOCK) ON PAI.MON = MON.ID"
                        sConsulta = sConsulta & " LEFT JOIN MON_DEN WITH(NOLOCK) ON MON_DEN.MON=MON.ID AND MON_DEN.IDIOMA =@IDIOMA "
                        sConsulta = sConsulta & " LEFT JOIN PAI_DEN WITH(NOLOCK) ON PAI_DEN.PAI=PAI.ID AND PAI_DEN.IDIOMA =@IDIOMA "
                        sConsulta = sConsulta & " LEFT JOIN IDI WITH(NOLOCK) ON PAI_DEN.IDIOMA=IDI.ID "
                        sConsulta = sConsulta & " WHERE PAI_DEN.DEN = ?"
                    Else
                        sConsulta = sConsulta & " SELECT PAI.ID,PAI.COD AS PAICOD,PAI_DEN.DEN AS PAIDEN,PAI.MON AS PAIMON,MON.COD AS MONCOD, MON_DEN.DEN AS MONDEN,MON.FECACT AS MONFECACT,MON.ID AS MONID ,IDI.ID AS IDIID,IDI.COD AS IDICOD,IDI.DEN AS IDIDEN, VALIDNIF"
                        sConsulta = sConsulta & " FROM PAI WITH (NOLOCK) LEFT JOIN MON WITH(NOLOCK) ON PAI.MON = MON.ID"
                        sConsulta = sConsulta & " LEFT JOIN MON_DEN WITH(NOLOCK) ON MON_DEN.MON=MON.ID AND MON_DEN.IDIOMA =@IDIOMA "
                        sConsulta = sConsulta & " LEFT JOIN PAI_DEN WITH(NOLOCK) ON PAI_DEN.PAI=PAI.ID AND PAI_DEN.IDIOMA =@IDIOMA "
                        sConsulta = sConsulta & " LEFT JOIN IDI WITH(NOLOCK) ON PAI_DEN.IDIOMA=IDI.ID "
                        sConsulta = sConsulta & " WHERE PAI_DEN.DEN LIKE ?"
                        
                        CaracteresInicialesDen = CaracteresInicialesDen & "%"
                    End If
                    
                    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(CaracteresInicialesDen), Value:=CaracteresInicialesDen)
                    adoComm.Parameters.Append adoParam
                End If
            
            End If
                   
        End If
    End If
    
    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY PAIDEN"
    Else
        If OrdenadosPorMoneda Then
            sConsulta = sConsulta & " ORDER BY MONCOD"
        Else
            sConsulta = sConsulta & " ORDER BY PAICOD"
        End If
    End If
          
    ''' Creacion del Resultset
    
    Set ador = New ADODB.Recordset
    
    adoComm.CommandText = sConsulta
    Set ador = New ADODB.Recordset
        
    Set ador = adoComm.Execute
       
    If ador.eof Then
            
        ador.Close
        Set ador = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
          
    Else
        
        ''' Carga del Resultset en la coleccion
        
        Set mCol = Nothing
        Set mCol = New Collection
        
           If UsarIndice Then
                
                lIndice = 0
            
                While Not ador.eof
                
                    Me.Add ador("PAICOD").Value, ador("PAIDEN").Value, NullToStr(ador("MONCOD").Value), NullToStr(ador("MONDEN").Value), lIndice, ador("ID").Value, ador("MONID").Value, ador("IDIID").Value, ador("IDICOD").Value, ador("IDIDEN").Value, ador("VALIDNIF").Value
                    ador.MoveNext
                    lIndice = lIndice + 1
                    
                Wend
                
            Else
            
                While Not ador.eof
                
                    Me.Add ador("PAICOD").Value, ador("PAIDEN").Value, ador("MONCOD").Value, ador("MONDEN").Value, , ador("ID").Value, ador("MONID").Value, ador("IDIID").Value, ador("IDICOD").Value, ador("IDIDEN").Value, ador("VALIDNIF").Value
                    ador.MoveNext
                    
                Wend
                
            End If
        
        
        ador.Close
        Set ador = Nothing
        
    End If
    
    Set adoParam = Nothing
    Set adoComm = Nothing
End Sub

Public Function Add(ByVal Cod As String, ByVal Den As String, Optional ByVal MonCod As Variant, Optional ByVal MonDen As Variant, Optional ByVal varIndice As Variant, Optional ByVal PaiId As Integer, Optional ByVal MonId As Variant, Optional ByVal IdiomaId As Variant, Optional ByVal IdiomaCod As Variant, Optional ByVal IdiomaDen As Variant, Optional ByVal iValidNif As Integer) As CPais
    
   Dim oPais As CPais
    
    ''' Creacion de objeto pais
    
    Set oPais = New CPais
    
    ''' Paso de los parametros al nuevo objeto pais
    
    oPais.Cod = Cod
    oPais.Den = Den
    
    If Not IsMissing(PaiId) Then
        oPais.ID = PaiId
    End If
    
    
    Set oPais.Conexion = mvarConexion
    oPais.Portal = mvarPortal
    oPais.ValidNif = iValidNif
    
    If Not IsMissing(MonCod) And Not IsNull(MonCod) Then
    
        Set oPais.Moneda = New CMoneda
        Set oPais.Moneda.Conexion = mvarConexion
        
        oPais.Moneda.Cod = MonCod
        
        If Not IsMissing(MonDen) And Not IsNull(MonDen) Then
            oPais.Moneda.Den = MonDen
        End If
            
        If Not IsMissing(MonId) Then
            oPais.Moneda.ID = MonId
        End If
        
    End If
    
    If Not IsMissing(IdiomaCod) Then
        
        If Not IsNull(IdiomaCod) Then
        
            Set oPais.Idioma = New CIdioma
            
            oPais.Idioma.Cod = IdiomaCod
            oPais.Idioma.ID = IdiomaId
            oPais.Idioma.Den = IdiomaDen
            
            
        End If
        
    End If
    
    ''' Anyadir el objeto pais a la coleccion
    ''' Si no se especifica indice, se anyade al final
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
    
        oPais.Indice = varIndice
        mCol.Add oPais, CStr(varIndice)
        
    Else
        mCol.Add oPais, Cod
    End If
    
    Set Add = oPais
    
    Set oPais = Nothing

End Function
Public Property Get Item(vntIndexKey As Variant) As CPais

    ''' * Objetivo: Recuperar un pais de la coleccion
    ''' * Recibe: Indice del pais a recuperar
    ''' * Devuelve: Pais correspondiente
    
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set mvarConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = mvarConexion
    
End Property
Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property

Public Property Get eof() As Boolean

    ''' * Objetivo: Devolver variable privada EOF
    ''' * Recibe: Nada
    ''' * Devuelve: Variable privada EOF
    
    eof = mvarEOF
    
End Property
Public Sub Remove(vntIndexKey As Variant)
   
    ''' * Objetivo: Eliminar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a eliminar
    ''' * Devuelve: Nada
    
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    
    '*-*'
   
    
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
    Set mvarConexion = Nothing
    
End Sub
Public Function DevolverPais(ByVal Cod As String) As CPais

    ''' ! Pendiente de revision, segun tema de combos, etc.
    
    Dim oPais As CPais
    Dim iIndice As Integer
    On Error Resume Next
    
    For iIndice = 1 To mCol.Count
        
        Set oPais = mCol.Item(iIndice)
        If oPais.Cod = Cod Then
            Set DevolverPais = oPais
            Set oPais = Nothing
            Exit Function
        End If
    
    Next
    
    Set oPais = Nothing
    Set DevolverPais = Nothing

End Function
Public Function DevolverLosCodigos() As TipoDatosCombo
   
    ''' ! Pendiente de revision, segun tema de combos
    
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   ReDim Codigos.ID(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
        Codigos.ID(iCont) = mCol(iCont + 1).ID
    Next

   DevolverLosCodigos = Codigos
   
End Function

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property



