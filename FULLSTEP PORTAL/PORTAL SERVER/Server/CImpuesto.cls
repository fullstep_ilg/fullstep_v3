VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CImpuesto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private m_oConexion As CConexion

Private m_vIDImpuesto As Variant
Private m_vIDMat As Variant
Private m_vIDValor As Variant
Private m_vCod As Variant

Private m_vDescr As Variant

Private m_vGMN1 As Variant
Private m_vGMN2 As Variant
Private m_vGMN3 As Variant
Private m_vGMN4 As Variant
Private m_vConcepto As Variant

Private m_vArt As Variant
Private m_vValor As Variant
Private m_vVigencia As Variant
Private m_vRetenido As Variant
Private m_vTipo As Variant
Private m_vAtribID As Variant
Private m_vNivelCategoria As Variant

Private m_vImpuestoPaisProvincia As Variant

Private m_vCodPais As Variant
Private m_vCodProvincia As Variant
Private m_vDenPais As Variant
Private m_vDenProvincia As Variant

Private m_vHeredado As Variant
Private m_vGrupoCompatibilidad As Variant

Private m_vFecAct As Variant

''' <summary>
''' Obtener el valor de m_vIDImpuesto
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get IDImpuesto() As Variant
    IDImpuesto = m_vIDImpuesto
End Property

''' <summary>
''' Establecer el valor de m_vIDImpuesto
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let IDImpuesto(ByVal Data As Variant)
    m_vIDImpuesto = Data
End Property

''' <summary>
''' Obtener el valor de m_vGrupoCompatibilidad
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get GrupoCompatibilidad() As Variant
    GrupoCompatibilidad = m_vGrupoCompatibilidad
End Property

''' <summary>
''' Establecer el valor de m_vGrupoCompatibilidad
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let GrupoCompatibilidad(ByVal Data As Variant)
    m_vGrupoCompatibilidad = Data
End Property

''' <summary>
''' Obtener el valor de m_vIDMat
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get IDMat() As Variant
    IDMat = m_vIDMat
End Property

''' <summary>
''' Establecer el valor de m_vIDMat
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let IDMat(ByVal Data As Variant)
    m_vIDMat = Data
End Property

''' <summary>
''' Obtener el valor de m_vIDValor
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get IDValor() As Variant
    IDValor = m_vIDValor
End Property

''' <summary>
''' Establecer el valor de m_vIDValor
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let IDValor(ByVal Data As Variant)
    m_vIDValor = Data
End Property

''' <summary>
''' Obtener el valor de m_vCOD
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get CodImpuesto() As Variant
    CodImpuesto = m_vCod
End Property

''' <summary>
''' Establecer el valor de m_vCOD
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let CodImpuesto(ByVal Data As Variant)
    m_vCod = Data
End Property

''' <summary>
''' Obtener el valor de m_vDescr
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Descripcion() As Variant
    Descripcion = m_vDescr
End Property

''' <summary>
''' Establecer el valor de m_vDescr
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let Descripcion(ByVal Data As Variant)
    m_vDescr = Data
End Property

''' <summary>
''' Obtener el valor de m_vGMN1
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get GMN1Cod() As Variant
    GMN1Cod = m_vGMN1
End Property

''' <summary>
''' Establecer el valor de m_vGMN1
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let GMN1Cod(ByVal Data As Variant)
    m_vGMN1 = Data
End Property

''' <summary>
''' Obtener el valor de m_vGMN2
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get GMN2Cod() As Variant
    GMN2Cod = m_vGMN2
End Property

''' <summary>
''' Establecer el valor de m_vGMN2
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let GMN2Cod(ByVal Data As Variant)
    m_vGMN2 = Data
End Property

''' <summary>
''' Obtener el valor de m_vGMN3
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get GMN3Cod() As Variant
    GMN3Cod = m_vGMN3
End Property

''' <summary>
''' Establecer el valor de m_vGMN3
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let GMN3Cod(ByVal Data As Variant)
    m_vGMN3 = Data
End Property

''' <summary>
''' Obtener el valor de m_vGMN4
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get GMN4Cod() As Variant
    GMN4Cod = m_vGMN4
End Property

''' <summary>
''' Establecer el valor de m_vGMN4
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let GMN4Cod(ByVal Data As Variant)
    m_vGMN4 = Data
End Property

''' <summary>
''' Obtener el valor de m_vArt
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Art() As Variant
    Art = m_vArt
End Property

''' <summary>
''' Establecer el valor de m_vArt
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let Art(ByVal Data As Variant)
    m_vArt = Data
End Property

''' <summary>
''' Obtener el valor de m_vValor
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Valor() As Variant
    Valor = m_vValor
End Property

''' <summary>
''' Establecer el valor de m_vValor
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let Valor(ByVal Data As Variant)
    m_vValor = Data
End Property

''' <summary>
''' Obtener el valor de m_vRetenido
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Retenido() As Variant
    Retenido = m_vRetenido
End Property

''' <summary>
''' Establecer el valor de m_vRetenido
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let Retenido(ByVal Data As Variant)
    m_vRetenido = Data
End Property

''' <summary>
''' Obtener el valor de m_vVigencia
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Vigencia() As Variant
    Vigencia = m_vVigencia
End Property

''' <summary>
''' Establecer el valor de m_vVigencia
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let Vigencia(ByVal Data As Variant)
    m_vVigencia = Data
End Property

''' <summary>
''' Obtener el valor de m_vImpuestoPaisProvincia
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get ImpuestoPaisProvincia() As Variant
    ImpuestoPaisProvincia = m_vImpuestoPaisProvincia
End Property

''' <summary>
''' Establecer el valor de m_vImpuestoPais
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let ImpuestoPaisProvincia(ByVal Data As Variant)
    m_vImpuestoPaisProvincia = Data
End Property

''' <summary>
''' Obtener el valor de m_vCodPais
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get CodPais() As Variant
    CodPais = m_vCodPais
End Property

''' <summary>
''' Establecer el valor de m_vCodPais
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let CodPais(ByVal Data As Variant)
    m_vCodPais = Data
End Property

''' <summary>
''' Obtener el valor de m_vCodProvincia
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get CodProvincia() As Variant
    CodProvincia = m_vCodProvincia
End Property

''' <summary>
''' Establecer el valor de m_vCodProvincia
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let CodProvincia(ByVal Data As Variant)
    m_vCodProvincia = Data
End Property

''' <summary>
''' Obtener el valor de m_vDenPais
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get DenPais() As Variant
    DenPais = m_vDenPais
End Property

''' <summary>
''' Establecer el valor de m_vDenPais
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let DenPais(ByVal Data As Variant)
    m_vDenPais = Data
End Property

''' <summary>
''' Obtener el valor de m_vDenProvincia
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get DenProvincia() As Variant
    DenProvincia = m_vDenProvincia
End Property

''' <summary>
''' Establecer el valor de m_vDenProvincia
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let DenProvincia(ByVal Data As Variant)
    m_vDenProvincia = Data
End Property

''' <summary>
''' Obtener el valor de m_vHeredado
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Heredado() As Variant
    Heredado = m_vHeredado
End Property

''' <summary>
''' Establecer el valor de m_vHeredado
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let Heredado(ByVal Data As Variant)
    m_vHeredado = Data
End Property

''' <summary>
''' Obtener el valor de m_vFecAct
''' </summary>
''' <remarks>Llamada desde: Nadie, usan la privada IBaseDatos_AnyadirABaseDatos y IBaseDatos_FinalizarEdicionModificando ; Tiempo m�ximo: 0</remarks>
Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property
''' <summary>
''' Establecer el valor de m_vFecAct
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add ; Tiempo m�ximo: 0</remarks>
Public Property Let FecAct(ByVal Data As Variant)
    m_vFecAct = Data
End Property

''' <summary>
''' Obtener el valor de m_iTipo
''' </summary>
''' <remarks>Llamada desde: Nadie, usan la privada IBaseDatos_AnyadirABaseDatos y IBaseDatos_FinalizarEdicionModificando ; Tiempo m�ximo: 0</remarks>
Public Property Get Tipo() As Variant
    Tipo = m_vTipo
End Property
''' <summary>
''' Establecer el valor de m_iTipo
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add ; Tiempo m�ximo: 0</remarks>
Public Property Let Tipo(ByVal Data As Variant)
    m_vTipo = Data
End Property

''' <summary>
''' Obtener el valor de m_vAtribId
''' </summary>
''' <remarks>Llamada desde: Nadie, usan la privada IBaseDatos_AnyadirABaseDatos y IBaseDatos_FinalizarEdicionModificando ; Tiempo m�ximo: 0</remarks>
Public Property Get AtribID() As Variant
    AtribID = m_vAtribID
End Property
''' <summary>
''' Establecer el valor de m_vAtribId
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add ; Tiempo m�ximo: 0</remarks>
Public Property Let AtribID(ByVal Data As Variant)
    m_vAtribID = Data
End Property

''' <summary>
''' Obtener el valor de m_vNivelCategoria
''' </summary>
''' <remarks>Llamada desde: Nadie, usan la privada IBaseDatos_AnyadirABaseDatos y IBaseDatos_FinalizarEdicionModificando ; Tiempo m�ximo: 0</remarks>
Public Property Get NivelCategoria() As Variant
    NivelCategoria = m_vNivelCategoria
End Property
''' <summary>
''' Establecer el valor de m_vNivelCategoria
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add ; Tiempo m�ximo: 0</remarks>
Public Property Let NivelCategoria(ByVal Data As Variant)
    m_vNivelCategoria = Data
End Property

''' <summary>
''' Obtener el valor de m_vConcepto
''' </summary>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Concepto() As Variant
    Concepto = m_vConcepto
End Property

''' <summary>
''' Establecer el valor de m_vConcepto
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add; Tiempo m�ximo: 0</remarks>
Public Property Let Concepto(ByVal Data As Variant)
    m_vConcepto = Data
End Property

''' <summary>
''' Establecer el valor de m_oConexion
''' </summary>
''' <param name="Data">Valor de la propiedad</param>
''' <remarks>Llamada desde: CImpuestos/Add ; Tiempo m�ximo: 0</remarks>
Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property
''' <summary>
''' Obtener el valor de m_oConexion
''' </summary>
''' <remarks>Llamada desde: Nadie, usan la privada IBaseDatos_AnyadirABaseDatos y IBaseDatos_FinalizarEdicionModificando ; Tiempo m�ximo: 0</remarks>
Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

''' <summary>
''' destroys collection when this class is terminated
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub


