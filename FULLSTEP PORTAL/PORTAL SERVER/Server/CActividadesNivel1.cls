VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CActividadesNivel1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CActividadesNivel1 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean

Public Function Add(ByVal ID As Integer, ByVal Cod As String, ByVal Den As String, Optional ByVal varIndice As Variant, Optional ByVal varAsign As Variant, Optional ByVal varPendiente As Variant) As CActividadNivel1
    'create a new object
    Dim objnewmember As CActividadNivel1
    Dim sCod As String
    
    Set objnewmember = New CActividadNivel1
   
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    objnewmember.ID = ID
    
    Set objnewmember.Conexion = mvarConexion
    If Not IsMissing(varAsign) Then
        If varAsign Then
            objnewmember.Asignada = True
        Else
            objnewmember.Asignada = False
        End If
        
    End If
    
    If Not IsMissing(varPendiente) Then
        If varPendiente Then
            objnewmember.Pendiente = True
        Else
            objnewmember.Pendiente = False
        End If
    Else
            objnewmember.Pendiente = False
    End If
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        sCod = CStr(ID) & Mid$("                         ", 1, 10 - Len(CStr(ID)))
        mCol.Add objnewmember, sCod
    End If
      
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

Public Property Get Item(vntIndexKey As Variant) As CActividadNivel1
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()

        Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
    
    Next

   DevolverLosCodigos = Codigos
   
End Function

''' <summary>
''' Cargar todas las actividades
''' </summary>
''' <param name="CodIdioma">Idioma para los textos</param>
''' <param name="CaracteresInicialesCod">Caracteres por los q empieza el c�digo</param>
''' <param name="CaracteresInicialesDen">Caracteres por los q empieza la denominaci�n</param>
''' <param name="TipoBusqueda">-1 usar =    0 usar like     1 usar >=</param>
''' <param name="OrdenadosPorDen">Ordenar por denominaci�n o por c�digo</param>
''' <param name="UsarIndice">Al crear la clase cada registro identificarlo por ACT1.ID � por numeros correlativos</param>
''' <param name="PYME">c�digo de pyme de la instalaci�n</param>
''' <remarks>Llamada desde: actividades.asp; Tiempo m�ximo: 0,1</remarks>
''' Revisado por: jbg; Fecha: 29/10/2012
Public Sub CargarTodasLasActividades(ByVal CodIdioma As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal TipoBusqueda As Integer, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean, Optional ByVal PYME As String)
    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim ador As ADODB.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
    Dim sDen As String
    Dim oACN1 As CActividadNivel1

    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
    
    Set adoComm = New ADODB.Command
    Set adoComm.ActiveConnection = mvarConexion.AdoCon
    adoComm.CommandType = adCmdText
    
    sDen = "DEN_" & EsIdiomaValido(CodIdioma)
    
    sConsulta = "SELECT ID,COD," & sDen & " FROM ACT1 WITH (NOLOCK) "
    
    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
        'BEGIN DES_PRT_PORTAL_180
        If PYME <> "" Then
            sConsulta = sConsulta & " WHERE PYME =? "
            
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(PYME), Value:=PYME)
            adoComm.Parameters.Append adoParam
        End If
        'END DES_PRT_PORTAL_180
    Else
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
            
            Select Case TipoBusqueda
                
            Case -1
            
                sConsulta = sConsulta & " WHERE COD =? "
                sConsulta = sConsulta & " AND " & sDen & "=? "
                
            Case 0
                
                sConsulta = sConsulta & " WHERE COD LIKE ? "
                sConsulta = sConsulta & " AND " & sDen & " LIKE ? "
                           
                CaracteresInicialesCod = CaracteresInicialesCod & "%"
                CaracteresInicialesDen = CaracteresInicialesDen & "%"
            Case 1
                
                sConsulta = sConsulta & " WHERE COD >= ? "
                sConsulta = sConsulta & " AND " & sDen & ">= ? "
                            
            End Select
                    
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(CaracteresInicialesCod), Value:=CaracteresInicialesCod)
            adoComm.Parameters.Append adoParam
            
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(CaracteresInicialesDen), Value:=CaracteresInicialesDen)
            adoComm.Parameters.Append adoParam
                    
        Else
            
            If Not (CaracteresInicialesCod = "") Then
                
                Select Case TipoBusqueda
                
                Case -1
                
                    sConsulta = sConsulta & " WHERE COD =? "
                    
                Case 0
                        
                   sConsulta = sConsulta & " WHERE COD LIKE ?"
                    
                   CaracteresInicialesCod = CaracteresInicialesCod & "%"
                   
                Case 1
                        
                   sConsulta = sConsulta & " WHERE COD >= ? "
                                
                End Select
                
                Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(CaracteresInicialesCod), Value:=CaracteresInicialesCod)
                adoComm.Parameters.Append adoParam
                
            Else
                
                Select Case TipoBusqueda
                
                Case -1
    
                    sConsulta = sConsulta & " WHERE " & sDen & "=? "
                    
                Case 0
                
                    sConsulta = sConsulta & " WHERE DEN LIKE ? "
                    CaracteresInicialesDen = CaracteresInicialesDen & "%"
                      
                Case 1
                    sConsulta = sConsulta & " WHERE " & sDen & ">= ? "
                    
                End Select
                
                Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(CaracteresInicialesDen), Value:=CaracteresInicialesDen)
                adoComm.Parameters.Append adoParam
                
            End If
        
        End If
        'BEGIN DES_PRT_PORTAL_180
        If PYME <> "" Then
            sConsulta = sConsulta & " AND PYME =? "
            
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(PYME), Value:=PYME)
            adoComm.Parameters.Append adoParam
        End If
        'END DES_PRT_PORTAL_180
    End If
    
    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY " & sDen & " ,COD"
    Else
        sConsulta = sConsulta & " ORDER BY COD," & sDen
    End If
          
    adoComm.CommandText = sConsulta
    Set ador = New ADODB.Recordset
        
    Set ador = adoComm.Execute
        
    If ador.eof Then
    
        ador.Close
        Set ador = Nothing
        Set adoParam = Nothing
        Set adoComm = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        
        Exit Sub
          
    Else
        
        Set mCol = Nothing
        Set mCol = New Collection
            
        If UsarIndice Then
            
            lIndice = 0
            
            While Not ador.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Set oACN1 = Me.Add(ador("ID").Value, ador("COD").Value, ador(sDen).Value, lIndice)
                oACN1.CargaSiActividadesHijas
                ador.MoveNext
                lIndice = lIndice + 1
            Wend
            
        Else
            
            While Not ador.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Set oACN1 = Me.Add(ador("ID").Value, ador("COD").Value, ador(sDen).Value)
                oACN1.CargaSiActividadesHijas
                ador.MoveNext
            Wend
        End If
        
        ador.Close
        
        Set adoParam = Nothing
        Set adoComm = Nothing
        Set ador = Nothing
        Exit Sub
          
    End If
End Sub

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property


