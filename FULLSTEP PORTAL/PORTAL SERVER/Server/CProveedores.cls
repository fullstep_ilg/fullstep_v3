VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CProveedores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CProveedores **********************************
'*             Autor : Javier Arana
'*             Creada : 4/12/98
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property
Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Function Add(ByVal ID As Long, ByVal Cod As String, ByVal Den As String, Optional ByVal Direccion As Variant, Optional ByVal CP As Variant, _
Optional ByVal CodPais As Variant, Optional ByVal DenPais As Variant, Optional ByVal Poblacion As Variant, _
Optional ByVal CodMon As Variant, Optional ByVal DenMon As Variant, Optional ByVal CodProvi As Variant, _
Optional ByVal DenProvi As Variant, Optional ByVal varIndice As Variant, Optional ByVal NIF As Variant, _
Optional ByVal CodFullStep As Variant, Optional ByVal Estado As Integer, Optional ByVal Premium As Boolean) As CProveedor
    'create a new object
    Dim objnewmember As CProveedor
    
    
    Set objnewmember = New CProveedor
    
    With objnewmember
        
        .ID = ID
        .Cod = Cod
        .Den = Den
        Set .Conexion = mvarConexion
        
        
        If Not IsMissing(Poblacion) Then
            .Poblacion = Poblacion
        Else
            .Poblacion = Null
        End If
        
        If Not IsMissing(Direccion) Then
            .Direccion = Direccion
        Else
            .Direccion = Null
        End If
        
        
        If Not IsMissing(CP) Then
            .CP = CP
        Else
            .CP = Null
        End If
        
        
        If Not IsMissing(CodMon) Then
            .CodMon = CodMon
        Else
            .CodMon = Null
        End If
        
        If Not IsMissing(DenMon) Then
            .DenMon = DenMon
        Else
            .DenMon = Null
        End If
        
        
        If Not IsMissing(CodProvi) Then
            .CodProvi = CodProvi
        Else
            .CodProvi = Null
        End If
        
        If Not IsMissing(DenProvi) Then
            .DenProvi = DenProvi
        Else
            .DenProvi = Null
        End If
        
        
        If Not IsMissing(CodPais) Then
            .CodPais = CodPais
        Else
            .CodPais = Null
        End If
            
        If Not IsMissing(DenPais) Then
            .DenPais = DenPais
        Else
            .DenPais = Null
        End If
        
        
        If Not IsMissing(NIF) Then
            .NIF = NIF
        Else
            .NIF = Null
        End If
        
                
        If Not IsMissing(CodFullStep) Then
            .CodFullStep = CodFullStep
        Else
            .CodFullStep = Null
        End If
        
        .Estado = Estado
        If Not IsMissing(Premium) Then
            .Premium = Premium
        Else
            .Premium = False
        End If
       
    End With
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
       objnewmember.Indice = varIndice
       mCol.Add objnewmember, CStr(varIndice)
    Else
    'set the properties passed into the method
       mCol.Add objnewmember, CStr(Cod)
    End If
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Property Get Item(vntIndexKey As Variant) As CProveedor
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If
End Property

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo NoSeEncuentra:
    mCol.Remove vntIndexKey
    Exit Sub
NoSeEncuentra:
    
End Sub
