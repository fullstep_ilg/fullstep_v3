Attribute VB_Name = "basErrores"
Option Explicit


Public Function TratarError(ByVal adoErs As ADODB.Errors) As CTESError

Dim oError As CTESError
 
''' Rutina general de respuesta a errores.

Dim ID As Long
Dim Den As String
Dim ItemError1 As String, ItemError2 As String
Dim pos1 As Integer, pos2 As Integer

If adoErs.Count <> 0 Then
    ID = adoErs(adoErs.Count - 1).Number
    Den = adoErs(adoErs.Count - 1).Description
End If

Set oError = New CTESError

Select Case ID

Case 0

    oError.NumError = TESInfModificada
    oError.Arg1 = ""
    oError.Arg2 = ""
    
Case 515

    Beep
    pos1 = InStr(1, Den, "'")
    pos2 = InStr(pos1 + 1, Den, "'")
    ItemError1 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
    oError.NumError = TESFaltanDatos
    oError.Arg1 = FieldName(ItemError1)
   
Case 547
           
        pos1 = InStr(1, Den, "table") + 6
        pos2 = InStr(pos1 + 1, Den, "'")
        ItemError1 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
        pos1 = InStr(1, Den, "column") + 7
        pos2 = InStr(pos1 + 1, Den, "'")
        ItemError2 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
        
        
        If Mid(Den, 55, 3) = "INS" Or Mid(Den, 55, 3) = "UPD" Then
            oError.NumError = TESDatoEliminado
            oError.Arg1 = FieldName(ItemError1)
            oError.Arg2 = "Eliminado"
            
        Else
            oError.NumError = TESImposibleEliminar
            oError.Arg1 = TableName(ItemError1)
            oError.Arg2 = "DatRel"
        End If
    
    
Case 2627
    
    If InStr(1, Den, "PRIMARY") <> 0 Then
        
        oError.NumError = TESDatoDuplicado
        oError.Arg1 = "Codigo"
        
        If InStr(1, Den, "PK_USU_COM") Then
            oError.NumError = TESDatoDuplicado
            oError.Arg1 = "Comprador"
        End If
        
    
    Else
        
        pos1 = InStr(1, Den, "UC_") + 2
        pos2 = InStr(pos1 + 1, Den, "'")
        ItemError1 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
        oError.NumError = TESDatoDuplicado
        oError.Arg1 = FieldName(ItemError1)
    
    End If
    
Case 3621

        pos1 = InStr(1, Den, "table") + 6
        pos2 = InStr(pos1 + 1, Den, "'")
        ItemError1 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
        pos1 = InStr(1, Den, "column") + 7
        pos2 = InStr(pos1 + 1, Den, "'")
        ItemError2 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
        If Mid(Den, 55, 3) = "INS" Then
            MsgBox "No se puede insertar. El dato """ & FieldName(ItemError1) & """ ha sido eliminado.", vbExclamation, "Summit"
        Else
            MsgBox "No se puede eliminar o cambiar." & Chr(13) & "Existen datos relacionados en: " & TableName(ItemError1), vbExclamation, "Summit"
        End If
    
    
Case 50000
    
    If Mid(Den, 1, 5) = "37000" Then
        
        If InStr(1, Den, "Data in ART has been deleted by other user.") <> 0 Then
            oError.NumError = TESDatoEliminado
            oError.Arg1 = "Art�culo"
        Else
        
            If InStr(1, Den, "Data in table ITEM has been deleted by other user.") <> 0 Then
                oError.NumError = TESDatoEliminado
                oError.Arg1 = "Item"
            Else
                If InStr(1, Den, "Sum of distribution is greater than Cant.") <> 0 Then
                    oError.NumError = TESCantDistMayorQueItem
                Else
                    If InStr(1, Den, "Distribution is greater than 100.") <> 0 Then
                        oError.NumError = TESCantDistMayorQueItem
                    Else
                        If InStr(1, Den, "The offer has been procesed") <> 0 Then
                            oError.NumError = TESOfertaAdjudicada
                        Else
                            If InStr(1, Den, "Incorrect password") <> 0 Then
                                oError.NumError = TESCambioContrasenya
                            End If
                        End If
                    End If
                End If
            End If
        End If
    Else
    
        oError.NumError = TESImposibleEliminar
        oError.Arg1 = TableName(Mid(Den, 55, Len(Den) - 55 + 1))
    End If

Case -2147217873, -2147217900
    oError.NumError = TESErrorCodigoDuplicado
    oError.Arg1 = ID
    oError.Arg2 = Den
    
Case Else
    
    oError.NumError = TESOtroerror
    oError.Arg1 = ID
    oError.Arg2 = Den
    
End Select


Set TratarError = oError
    
End Function
'***************** DICCIONARIO DE DATOS ***************
 Function TableName(TableCode As String) As String

    Select Case TableCode
      
    ''' Mantenimiento
    Case "MON"
        TableName = "Monedas"
    Case "PAI"
        TableName = "Pa�ses"
    Case "PROVI"
        TableName = "Provincias"
    Case "UON1", "UON2", "UON3"
        TableName = "Estructura de la organizaci�n"
    Case "DEP"
        TableName = "Departamentos"
    Case "UON0_DEP", "UON1_DEP", "UON2_DEP", "UON3_DEP"
        TableName = "Departamentos"
    Case "PER"
        TableName = "Personas de la organizaci�n"
    Case "DEST"
        TableName = "Destinos"
    Case "GMN1", "GMN2", "GMN3", "GMN4"
        TableName = "Estructura de material"
    Case "ART1", "ART2", "ART3", "ART4"
        TableName = "Art�culos"
    Case "ART1_ADJ", "ART2_ADJ", "ART3_ADJ", "ART4_ADJ"
        TableName = "Ultimas adjudicaciones de art�culos"
    Case "UNI"
        TableName = "Unidades"
    Case "EQP"
        TableName = "Equipos de compra"
    Case "COM"
        TableName = "Comprador"
    Case "COM_GMN1", "COM_GMN2", "COM_GMN3", "COM_GMN4"
        TableName = "Relaci�n entre Comprador y materiales"
    Case "PROVE"
        TableName = "Proveedores"
    Case "PROVE_GMN1", "PROVE_GMN2", "PROVE_GMN3", "PROVE_GMN4"
        TableName = "Relaci�n entre proveedores y materiales"
    Case "PROVE_ART1", "PROVE_ART2", "PROVE_ART3", "PROVE_ART4"
        TableName = "Relaci�n entre proveedores y art�culos homologados."
    Case "CON"
        TableName = "Contactos de proveedores"
    Case "PROVE_GMN4", "PROVE_GMN3", "PROVE_GMN2", "PROVE_GMN1"
        TableName = "Proveedores / Materiales"
    Case "PROVE_EQP"
        TableName = "Proveedores / Equipos de compra"
    
    
    Case "PRES_MAT"
        TableName = "Presupuestos por material"
    
    Case "PROCE", "BLOQUEO_PROCE"
        TableName = "Procesos"
    
    Case "PROCE_ESP"
        TableName = "Especificaciones de procesos"
        
    Case "PROCE_PROVE"
        TableName = "Proveedores asignados"
    
    Case "PROCE_OFE"
        TableName = "Ofertas"
    
    Case "PROCE_OFE_WEB"
        TableName = "Ofertas recibidas a trav�s del web"
    Case "PROCE_PER"
        TableName = "Personas vinculadas a procesos"
        Case "PROCE_PROVE_POT"
        TableName = "Proveedores potenciales de procesos"
        
    Case "PROCE_PROVE_PET"
        TableName = "Comunicaciones a proveedores"
    
    Case "AR_ITEM", "AR_ITEM_MES", "AR_ITEM_MES_UON1", "AR_ITEM_MES_UON2", "AR_ITEM_MES_UON3"
        TableName = "Almac�n de resultados"
    
    Case "ITEM"
        TableName = "Items de proceso"
        
    Case "ITEM_ADJ"
        TableName = "Adjudicaciones de �tems de proceso"
    Case "ITEM_ADJREU"
        TableName = "Adjudicaciones de �tems en reuniones"
    Case "ITEM_ESP"
        TableName = "Especificaciones de �tems de proceso"
    Case "ITEM_OBJ"
        TableName = "Objetivos de �tems de proceso"
    Case "ITEM_OFE"
        TableName = "Ofertas de �tems de proceso"
    Case "ITEM_OFE_WEB"
        TableName = "Ofertas de �tems de proceso recibidas a trav�s del web"
    Case "ITEM_PRES"
        TableName = "Presupuestos de �tems de proceso"
    Case "ITEM_UON1", "ITEM_UON2", "ITEM_UON3"
        TableName = "Distribuci�n de �tem de proceso"
    Case "REU"
        TableName = "Reuniones"
    Case "REU_PROCE"
        TableName = "Procesos de reuniones"
           
    ''' Parametros
    
    Case "PARGEN_LIT", "PARGEN_DEF", "PARGEN_PORT", "PARGEN_INTERNO", "PARGEN_DOT", "PARGEN_GEST"
    
        TableName = "Configuraci�n general"
    Case "CAL"
        TableName = "Configuraci�n de las calificaciones de proveedores"
    Case "DIC"
        TableName = "Diccionario de datos"
    Case "FIRMAS"
        TableName = "Configuraci�n de firmas para adjudicaciones"
    Case "OFEEST"
        TableName = "Configuraci�n de estados de ofertas"
    Case "PAG"
        TableName = "Configuraci�n de formas de pago"
    Case "ROL"
        TableName = "Configuraci�n de relaciones persona / proceso"
        
    ''' Seguridad
    
    Case "ACC"
        TableName = "Propiedades de seguridad"
    Case "ACC_DEPEN"
        TableName = "Interdependencia entre propiedades de seguridad"
    Case "ADM"
        TableName = "Cuenta de administrador"
    Case "USU"
        TableName = "Usuarios"
    Case "USU_ACC"
        TableName = "Propiedades de seguridad de los usuarios"
    Case "PERF"
        TableName = "Perfiles de usuario"
    Case "PERF_ACC"
        TableName = "Propiedades de seguridad del perfil de usuario"
        
    Case "USU_COM"
        TableName = "Usuarios / Comprador"
    Case "USU_PER"
        TableName = "Usuarios / Personas de la organizacion"
    Case "SES"
        TableName = "Control de sesiones"
        
    Case Else
        TableName = "Tabla desconocida"
        
    End Select
        
    
End Function
Function FieldName(FieldCode As String)

    Select Case FieldCode
    
    ''' Mantenimiento
    
    Case "ID"
        FieldName = "Identificador"
    Case "COD"
        FieldName = "Codigo"
    Case "DEN"
        FieldName = "Denominacion"
    Case "CAMBIO"
        FieldName = "Cambio de moneda"
    Case "FECACT"
        FieldName = "Fecha de actualizacion"
    Case "MON"
        FieldName = "Moneda"
    Case "PAI"
        FieldName = "Pais"
    Case "EQP"
        FieldName = "Equipo de compra"
    Case "COM"
        FieldName = "Comprador"
    Case "DIR"
        FieldName = "Direccion"
    Case "CP"
        FieldName = "Codigo postal"
    Case "POB"
        FieldName = "Poblacion"
    Case "PROVI"
        FieldName = "Provincia"
    Case "RATING1"
        FieldName = "Calificacion 1"
    Case "RATING2"
        FieldName = "Calificacion 2"
    Case "RATING3"
        FieldName = "Calificacion 3"
    Case "OBS"
        FieldName = "Observaciones"
    Case "PROVE"
        FieldName = "Proveedor"
    Case "PAG"
        FieldName = "Forma de pago"
        
    ''' Gestion
    
    Case "PRE"
        FieldName = "Presupuesto"
    Case "ANYO"
        FieldName = "Anyo"
    Case "DESCR"
        FieldName = "Descripcion"
    Case "ANYODESCR"
        FieldName = "Descripcion de proceso en el anyo"
    Case "DEP"
        FieldName = "Departamento"
    Case "CODART"
        FieldName = "Codigo de articulo"
    Case "DENART"
        FieldName = "Denominacion de articulo"
    Case "ESPART"
        FieldName = "Especificaciones"
    Case "UNI"
        FieldName = "Unidad"
    Case "FECNEC"
        FieldName = "Fecha de necesidad"
    Case "TIPONEC"
        FieldName = "Tipo de necesidad"
    Case "TIPOOBJ"
        FieldName = "Tipo de objetivo"
    Case "CODPROVE"
        FieldName = "Codigo de proveedor"
    Case "DENPROVE"
        FieldName = "Denominacion de proveedor"
    Case "FECPETOFE"
        FieldName = "Fecha planificada para peticion de ofertas"
    Case "FECRECOFE"
        FieldName = "Fecha planificada para peticion de ofertas"
    Case "FECREUPRE"
        FieldName = "Fecha planificada para peticion de ofertas"
    Case "FECREUDEC"
        FieldName = "Fecha planificada para peticion de ofertas"
    Case "EST"
        FieldName = "Estado"
    Case "USU"
        FieldName = "Usuario"
    Case "FECINS"
        FieldName = "Fecha de insercion"
    Case "FECACT"
        FieldName = "Fecha de actualizacion"
    Case "PROCE"
        FieldName = "Proceso"
    Case "ANYOPL"
        FieldName = "Anyo planificado"
    Case "VOL"
        FieldName = "Volumen"
    Case "DEST"
        FieldName = "Destino"
    Case "FECEST"
        FieldName = "Fecha de estado"
    Case "FECENV"
        FieldName = "Fecha de envio"
    Case "FECOFE"
        FieldName = "Fecha de oferta"
    Case "FECVAL"
        FieldName = "Fecha de validez"
    Case "INV"
        FieldName = "Inversion"
    Case "PRODHO"
        FieldName = "Producto homologado S/N"
    Case "OFE"
        FieldName = "Oferta"
    Case "ITEM"
        FieldName = "Item"
    Case "FECREU"
        FieldName = "Fecha de reunion"
    Case "REF"
        FieldName = "Referencia"
    Case "REUPRE"
        FieldName = "Reunion preparatoria"
    Case "HORA"
        FieldName = "Hora"
    Case "TIPOREU"
        FieldName = "Tipo de reunion"
    Case "COME"
        FieldName = "Comentario"
    Case "REUDEC"
        FieldName = "Reunion de decision"
    Case "BAJA"
        FieldName = "Indicador de baja"
    Case "ANYOSADJ"
        FieldName = "Anyos adjudicados"
    Case "PROCE_PROVE"
        FieldName = "Proveedores asignados"
    ''' Informes
                
    Case "VOLCONT"
        FieldName = "Volumen contratado"
    Case "VOLCERR"
        FieldName = "Volumen cerrado"
    Case "NUMOFE"
        FieldName = "Numero de ofertas"
    Case "ANYORES"
        FieldName = "Anyo de resultados"
    Case "NUMPROCE"
        FieldName = "Numero de procesos"
    
    ''' Parametros
    
    Case "DENRATING1"
        FieldName = "Denominacion de calificacion 1"
    Case "DENRATING2"
        FieldName = "Denominacion de calificacion 2"
    Case "DENRATING3"
        FieldName = "Denominacion de calificacion 3"
    Case "DENDOT1"
        FieldName = "Denominacion plantilla peticion oferta planificada"
    Case "DOT1"
        FieldName = "Plantilla peticion oferta planificada"
    Case "DENDOT2"
        FieldName = "Denominacion plantilla peticion oferta planificada"
    Case "DOT2"
        FieldName = "Plantilla peticion oferta planificada"
    Case "DENDOT3"
        FieldName = "Denominacion plantilla peticion oferta anual"
    Case "DOT3"
        FieldName = "Plantilla peticion oferta anual"
    Case "DENDOT4"
        FieldName = "Denominacion plantilla peticion oferta anual"
    Case "DOT4"
        FieldName = "Plantilla peticion oferta anual"
    Case "PRODHO"
        FieldName = "Productos homologados por defecto S/N"
    Case "COMPDET"
        FieldName = "Comparativa detallada S/N"
    Case "COMPHIS"
        FieldName = "Comparativa con historico de precios S/N"
    Case "COMPHOM"
        FieldName = "Comparativa con informacion de homologacion S/N"
    Case "COMPOBS"
        FieldName = "Comparativa con observaciones S/N"
    Case "DENDOT5"
        FieldName = "Denominacion plantilla acta reunion preparatoria"
    Case "DOT5"
        FieldName = "Plantilla peticion acta reunion preparatoria"
    Case "DENDOT6"
        FieldName = "Denominacion plantilla acta reunion de decision"
    Case "DOT6"
        FieldName = "Plantilla peticion acta reunion de decision"
    Case "MINREUPRE"
        FieldName = "Minutos de presentacion en reunion preparatoria"
    Case "MINREUDEC"
        FieldName = "Minutos de presentacion en reunion de decision"
    Case "HORCOMREUPRE"
        FieldName = "Hora de comienzo de reuniones preparatorias"
    Case "HORCOMREUDEC"
        FieldName = "Hora de comienzo de reuniones de decision"
    Case "ODAPA"
        FieldName = "Orden del dia apaisado S/N"
    Case "FECACU"
        FieldName = "Fecha de inicio acumulados"
    
    ''' Seguridad
    
    Case "ADMPWD"
        FieldName = "Contrasenya del administrador"
    Case "ACC"
        FieldName = "Propiedad de seguridad del usuario"
    Case "PERF"
        FieldName = "Perfil de usuario"
    Case "PER"
        FieldName = "Persona de la organizacion"
    Case "TS"
        FieldName = "Fecha de actualizacion"
    Case "USU"
        FieldName = "Codigo de usuario"
        
    Case Else
        FieldName = "Campo desconocido"
    End Select
    
End Function


 

