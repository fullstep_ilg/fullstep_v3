VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSobre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

Private moProceso As CProceso

Private mlCod As Long
Private mdtFechaApertura As Variant
Private mdtFechaRealApertura As Variant
Private msUsuApertura As Variant
Private msObs As Variant
Private miEst As String


Private moConexion As CConexion

Private moGrupos As CGrupos


Public Property Set Proceso(ByVal oProce As CProceso)
    Set moProceso = oProce
End Property

Public Property Get Proceso() As CProceso
    Set Proceso = moProceso
End Property


Public Property Let Cod(ByVal lData As Long)
    mlCod = lData
End Property

Public Property Get Cod() As Long
    Cod = mlCod
End Property

Public Property Let FechaApertura(ByVal vData As Variant)
    mdtFechaApertura = vData
End Property

Public Property Get FechaApertura() As Variant
    FechaApertura = mdtFechaApertura
End Property

Public Property Let FechaRealApertura(ByVal vData As Variant)
    mdtFechaRealApertura = vData
End Property

Public Property Get FechaRealApertura() As Variant
    FechaRealApertura = mdtFechaRealApertura
End Property


Public Property Let UsuApertura(ByVal vData As Variant)
    msUsuApertura = vData
End Property

Public Property Get UsuApertura() As Variant
    UsuApertura = msUsuApertura
End Property


Public Property Let Obs(ByVal vData As Variant)
    msObs = vData
End Property

Public Property Get Obs() As Variant
    Obs = msObs
End Property

Public Property Let Est(ByVal iData As Integer)
    miEst = iData
End Property

Public Property Get Est() As Integer
    Est = miEst
End Property


Public Property Set Conexion(ByVal oCon As CConexion)
    Set moConexion = oCon
End Property

Public Property Get Conexion() As CConexion
    Set Conexion = moConexion
End Property

Public Property Set Grupos(ByVal oGrupos As CGrupos)
    Set moGrupos = oGrupos
End Property

Public Property Get Grupos() As CGrupos
    Set Grupos = moGrupos
End Property


