VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNoConformidades"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mCol As Collection
Private moConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
Set moConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub
Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set moConexion = Nothing
End Sub

Public Property Get Count() As Long

    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If

End Property

''' <summary>
''' Devolver las no conformidades de un Proveedor
''' </summary>
''' <param name="CiaComp">C�digo de compania</param>
''' <param name="Prove">Proveedor</param>
''' <param name="Idioma">Idioma</param>
''' <returns>las no conformidades de un Proveedor</returns>
''' <remarks>Llamada desde: NoConformidades11.asp; Tiempo m�ximo: 0,1</remarks>
Public Function DevolverNoConformidades(ByVal CiaComp As Long, ByVal Prove As String, ByVal Idioma As String, ByVal EmpresaPortal As Boolean, ByVal NomPortal As String) As ADODB.Recordset
Dim ador As ADODB.Recordset
Dim sConsulta As String
Dim sFSG As String

    Idioma = EsIdiomaValido(Idioma)
    sFSG = Fsgs(moConexion, CiaComp)

    sConsulta = "EXEC " & sFSG & "FSQA_GET_NOCONFORMIDADES_PROVE @PROVE=?,@IDI=?,@NOM_PORTAL=?"
    
    Dim adoCom As ADODB.Command
    
    Set adoCom = New ADODB.Command
    adoCom.ActiveConnection = moConexion.AdoCon
    adoCom.CommandType = adCmdText
    adoCom.CommandText = sConsulta
    Dim adoPar As ADODB.Parameter
    
    Set adoPar = adoCom.CreateParameter("PROVE", adVarChar, adParamInput, 50, Prove)
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("IDI", adVarChar, adParamInput, 50, Idioma)
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("NOM_PORTAL", adVarChar, adParamInput, 50, IIf(EmpresaPortal, NomPortal, Null))
    adoCom.Parameters.Append adoPar
    
    Set ador = New ADODB.Recordset
    ador.CursorType = adOpenDynamic
    Set ador = adoCom.Execute
        
    If ador.eof Then
        ador.Close
        Set ador = Nothing
        Set DevolverNoConformidades = Nothing
        Exit Function
    Else
    
        Dim Dt As ADODB.Recordset
        Set Dt = New ADODB.Recordset
        Dt.CursorType = adOpenDynamic
            
        Dt.Fields.Append "ID", adInteger
        Dt.Fields.Append "INSTANCIA", adInteger, , adFldIsNullable
        Dt.Fields.Append "FEC_LIM_RESOL", adDBTimeStamp, , adFldIsNullable
        Dt.Fields.Append "DEN", adWChar, 255, adFldIsNullable
        Dt.Fields.Append "PER", adVarChar, 20, adFldIsNullable
        Dt.Fields.Append "NOMBRE", adWChar, 255, adFldIsNullable
        Dt.Fields.Append "EMAIL", adVarChar, 100, adFldIsNullable
        Dt.Fields.Append "VERSION", adInteger, , adFldIsNullable
        Dt.Fields.Append "USU", adVarChar, 50, adFldIsNullable
        Dt.Fields.Append "DEP", adVarChar, 10, adFldIsNullable
        Dt.Fields.Append "UON1", adVarChar, 10, adFldIsNullable
        Dt.Fields.Append "UON2", adVarChar, 10, adFldIsNullable
        Dt.Fields.Append "UON3", adVarChar, 10, adFldIsNullable
        Dt.Fields.Append "TITULO", adVarWChar, 4000, adFldIsNullable
        Dt.Fields.Append "FECALTA", adDBTimeStamp, , adFldIsNullable
        Dt.Fields.Append "COMENT_ALTA", adVarWChar, 4000, adFldIsNullable
        Dt.Fields.Append "EN_PROCESO", adUnsignedTinyInt, , adFldIsNullable
        Dt.Fields.Append "ESTADO", adUnsignedTinyInt, , adFldIsNullable
        Dt.Fields.Append "FECHA", adDBTimeStamp, , adFldIsNullable
        
        Dt.Fields.Append "ID_ENCRIPTED", adVarChar, 200
        Dt.Fields.Append "INSTANCIA_ENCRIPTED", adVarChar, 200
        
        Dt.Open
    
        While Not ador.eof
            Dt.AddNew
        
            Dt.Fields("ID").Value = ador.Fields("ID").Value
            Dt.Fields("INSTANCIA").Value = ador.Fields("INSTANCIA").Value
            Dt.Fields("FEC_LIM_RESOL").Value = ador.Fields("FEC_LIM_RESOL").Value
            Dt.Fields("DEN").Value = ador.Fields("DEN").Value
            Dt.Fields("PER").Value = ador.Fields("PER").Value
            Dt.Fields("NOMBRE").Value = ador.Fields("NOMBRE").Value
            Dt.Fields("EMAIL").Value = ador.Fields("EMAIL").Value
            Dt.Fields("VERSION").Value = ador.Fields("VERSION").Value
            Dt.Fields("USU").Value = ador.Fields("USU").Value
            Dt.Fields("DEP").Value = ador.Fields("DEP").Value
            Dt.Fields("UON1").Value = ador.Fields("UON1").Value
            Dt.Fields("UON2").Value = ador.Fields("UON2").Value
            Dt.Fields("UON3").Value = ador.Fields("UON3").Value
            Dt.Fields("TITULO").Value = ador.Fields("TITULO").Value
            Dt.Fields("FECALTA").Value = ador.Fields("FECALTA").Value
            Dt.Fields("COMENT_ALTA").Value = ador.Fields("COMENT_ALTA").Value
            Dt.Fields("EN_PROCESO").Value = ador.Fields("EN_PROCESO").Value
            Dt.Fields("ESTADO").Value = ador.Fields("ESTADO").Value
            Dt.Fields("FECHA").Value = ador.Fields("FECHA").Value
            
            
            Dt.Fields("ID_ENCRIPTED").Value = EncriptarADM(ador.Fields("ID").Value)
            Dt.Fields("INSTANCIA_ENCRIPTED").Value = EncriptarADM(ador.Fields("INSTANCIA").Value)
            
            ador.MoveNext
        Wend
    
        'Devuelve un recordset desconectado
        ador.ActiveConnection = Nothing
        Set DevolverNoConformidades = Dt
    End If
    
End Function

