VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAdjunto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAdjunto **********************************
'*             Autor : Hilario Barrenkua
'*             Creada : 20/11/2000
'***************************************************************

Option Explicit
Option Base 0

Private mvarId As Variant
Private mvarAdjunID As Variant
Private mvarAdjunIDPortal As Variant

Private moConexion As CConexion

Private mvarNombre As String
Private mvarComentario As Variant
Private mlDataSize As Long

Private msOP As String

Private moObjeto As Object

Property Set Objeto(oObjeto As Object)
Set moObjeto = oObjeto
End Property
Property Get Objeto() As Object
Set Objeto = moObjeto
End Property



Public Property Get DataSize() As Long
    DataSize = mlDataSize
End Property


Public Property Let DataSize(ByVal vSize As Long)
    mlDataSize = vSize
End Property


Public Property Get Nombre() As String
    Nombre = mvarNombre
End Property
Public Property Let Nombre(ByVal vData As String)
    mvarNombre = vData
End Property

Public Property Get Operacion() As String
    Operacion = msOP
End Property
Public Property Let Operacion(ByVal sOp As String)
    msOP = sOp
End Property


Public Property Get Comentario() As Variant
    Comentario = mvarComentario
End Property
Public Property Let Comentario(ByVal vData As Variant)
    mvarComentario = vData
End Property
Public Property Let AdjunID(ByVal i As Variant)
On Error Resume Next
    If i = "" Then
        mvarAdjunID = Null
    Else
        mvarAdjunID = CLng(i)
    End If

If IsEmpty(mvarAdjunID) Then
    mvarAdjunID = Null
End If
End Property
Public Property Get AdjunID() As Variant
    AdjunID = mvarAdjunID
End Property
Public Property Let AdjunIDPortal(ByVal i As Variant)
    mvarAdjunIDPortal = i
End Property
Public Property Get AdjunIDPortal() As Variant
    AdjunIDPortal = mvarAdjunIDPortal
End Property
Public Property Let id(ByVal i As Variant)
    mvarId = i
End Property
Public Property Get id() As Variant
    id = mvarId
End Property
Friend Property Set Conexion(ByVal con As CConexion)
Set moConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property


Private Sub Class_Terminate()
    Set moConexion = Nothing
End Sub

''' <summary>
''' Guardar adjunto en tabla ADJUN del Portal a trav�s de FSNWebService
''' </summary>
''' <param name="lCiaComp">id de compania compradora</param>
''' <param name="sTemp">carpeta temporal</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: script\solicitudesoferta\uploadserver.asp; Tiempo m�ximo: 0,1</remarks>
''' Revisada EPB 31/08/2015
Public Function guardarAdjuntoPortal(ByVal lCiaComp As Long, ByVal sTemp As String) As CTESError
    Dim Fl As Long
    Dim Chunk() As Byte
    Dim oTESError As CTESError
    Dim fso As Object
    Dim objDom As Object
    Dim objXmlHttp As Object
    Dim parser As Object
    Dim XmlBody As String
    Dim strUrl As String
    Dim strSoapAction As String
    Dim strResultado As String
    Dim strBase64 As String
    Dim nId As Long
    Dim rutaWS As String
    Dim partesRutaAdjun() As String
    Dim nomAdjun As String
    
    Set oTESError = New CTESError
    
    oTESError.NumError = 0
    
    On Error GoTo Error
    
    partesRutaAdjun = Split(sTemp, "\")
    nomAdjun = partesRutaAdjun(UBound(partesRutaAdjun))
    
    rutaWS = basUtilidades.ObtenerRutaWebService(moConexion) 'Si hay ruta en BD Portal esa, sino la de GS
    
    ' Crea objetos DOMDocument y XMLHTTP
    Set objDom = CreateObject("MSXML2.DOMDocument")
    Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
    
    strUrl = rutaWS & "/FSN_Adjuntos.asmx"
    
    nId = 0
    
    Dim DataFile As Integer
    Dim Chunks As Long
    Dim Fragment As Long
    Dim iChunk As Integer
    Dim vChunkSize As Variant
    Dim rutaFicheroTemporal As String
    
    rutaFicheroTemporal = ""
    
    'Obtener tama�o del chunk
    Dim oParam As CGestorParametros
    Set oParam = New CGestorParametros
    Set oParam.Conexion = moConexion
    vChunkSize = oParam.DevolverParametroInternoGS(lCiaComp, "CHUNK_SIZE")
    If IsNull(vChunkSize) Or IsEmpty(vChunkSize) Then vChunkSize = giChunkSize
    
    DataFile = 1
    'Abrimos el fichero para lectura binaria
    Open sTemp For Binary Access Read As DataFile
    Fl = LOF(DataFile)    ' Length of data in file
    If Fl = 0 Then
        Close DataFile
    End If
    ' Se lo asignamos en bloques a la especificacion
    Chunks = Fl \ vChunkSize
    Fragment = Fl Mod vChunkSize
    
    If Fragment > 0 Then ' Si no ocupa  justo un chunk
        Chunks = Chunks + 1
    End If
    
    ReDim Chunk(vChunkSize - 1)

    For iChunk = 1 To Chunks
        If iChunk = Chunks And Fragment > 0 Then
            ReDim Chunk(Fragment - 1)
        End If
        Get DataFile, , Chunk()
        
        strBase64 = EncodeBase64(Chunk)
        strSoapAction = "http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos/GrabarCarpetaTemporal"
        
        XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
                "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
                "<soap:Body>" & _
                "<GrabarCarpetaTemporal xmlns=""http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos"">" & _
                "<Adjunto>" & strBase64 & "</Adjunto>" & _
                "<Nombre>" & nomAdjun & "</Nombre>" & _
                "<RutaTemporal>" & rutaFicheroTemporal & "</RutaTemporal>" & _
                "</GrabarCarpetaTemporal>" & _
                "</soap:Body>" & _
                "</soap:Envelope>"
        
       ' Carga XML
       objDom.async = True
       objDom.loadXML XmlBody
       ' Abre el webservice
       objXmlHttp.Open "POST", strUrl, True
       ' Crea headings
       objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
       objXmlHttp.setRequestHeader "SOAPAction", strSoapAction
    
       ' Envia XML command
       objXmlHttp.Send objDom.xml
       'Esperar hasta que se termine de ejecutar
       While Not objXmlHttp.ReadyState = 4
           DoEvents
       Wend
          
       Set parser = CreateObject("MSXML2.DOMDocument")
       strResultado = objXmlHttp.ResponseText
       parser.loadXML strResultado
       
       If InStr(strResultado, "GrabarCarpetaTemporalResult") Then
           strResultado = parser.LastChild.Text
       End If
       Dim rdo() As String
        If InStr(1, strResultado, "#") > 0 Then
            rdo = Split(strResultado, "#", , vbTextCompare)
            If rdo(0) = "1" Then
                rutaFicheroTemporal = rdo(1)
            End If
        End If
    Next
    
    Close DataFile
    
    'Grabar el archivo en BD
    '-----------------------
    strSoapAction = "http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos/GrabarAdjuntoOfertaPortal"
           
    XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
            "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
            "<soap:Body>" & _
            "<GrabarAdjuntoOfertaPortal xmlns=""http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos"">" & _
            "<RutaTemporal>" & rutaFicheroTemporal & "</RutaTemporal>" & _
            "<Nombre>" & nomAdjun & "</Nombre>" & _
            "<Id>" & nId & "</Id>" & _
            "<Cia>" & lCiaComp & "</Cia>" & _
            "</GrabarAdjuntoOfertaPortal>" & _
            "</soap:Body>" & _
            "</soap:Envelope>"
         
    ' Carga XML
    objDom.async = True
    objDom.loadXML XmlBody
    ' Abre el webservice
    objXmlHttp.Open "POST", strUrl, True
    ' Crea headings
    objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
    objXmlHttp.setRequestHeader "SOAPAction", strSoapAction
 
    ' Envia XML command
    objXmlHttp.Send objDom.xml
    'Esperar hasta que se termine de ejecutar
    While Not objXmlHttp.ReadyState = 4
        DoEvents
    Wend
       
    Set parser = CreateObject("MSXML2.DOMDocument")
    strResultado = objXmlHttp.ResponseText
    parser.loadXML strResultado
    
    If InStr(strResultado, "GrabarAdjuntoOfertaPortalResult") Then
        strResultado = parser.LastChild.Text
    End If
    
    ' Cierra object
    Set objXmlHttp = Nothing

    Dim ArrayAdjunto() As String
    ArrayAdjunto = Split(strResultado, "#", , vbTextCompare)
    mvarAdjunIDPortal = ArrayAdjunto(0)
    mlDataSize = ArrayAdjunto(1)
    
fin:
    Set guardarAdjuntoPortal = oTESError
    Set fso = CreateObject("SCRIPTING.FILESYSTEMOBJECT")
    fso.DeleteFile (sTemp)
    Set fso = Nothing
    Exit Function
Error:
    On Error Resume Next

    If moConexion.AdoCon.Errors.Count > 0 Then
        Set oTESError = TratarError(moConexion.AdoCon.Errors)
    Else
        oTESError.NumError = 200
    End If

    Resume fin
End Function



''' <summary>
''' Descarga archivo de tabla ADJUN del Portal a trav�s de FSNWebService y guarda en disco
''' </summary>
''' <param name="lCiaComp">id de compania compradora</param>
''' <param name="sPath">carpeta temporal</param>
''' <param name="sTarget">nombre fichero</param>
''' <param name="Tipo">Tipo=0 ADJUN, otro tablas de pedido </param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: script\solicitudesoferta\uploadserver.asp; Tiempo m�ximo: 0,1</remarks>
''' Revisada EPB 31/08/2015
Public Function EscribirADisco(ByVal lCiaComp As Long, ByVal sPath As String, ByVal sTarget As String, Optional ByVal Tipo As Integer = 0) As CTESError
Dim oTESError As CTESError
Dim adoStream As adodb.Stream
Dim i As Long
Dim offset As Long
Dim sTargetOriginal As String
Dim sFileName As String
Dim bFSGS As Boolean
Dim lIDAdjunto As Long

Set oTESError = New CTESError
oTESError.NumError = 0
Set adoStream = New adodb.Stream
adoStream.Open
adoStream.Type = adTypeBinary
i = 0
offset = 50000
CrearArbol sPath
oTESError.Arg1 = sTarget
sTargetOriginal = sTarget
sFileName = sPath & sTarget
If IsNull(mvarAdjunIDPortal) Then
    bFSGS = True
    lIDAdjunto = mvarAdjunID
Else
    bFSGS = False
    lIDAdjunto = mvarAdjunIDPortal
End If
If Tipo = 0 Then
    LeerAdjuntoPortal lIDAdjunto, lCiaComp, bFSGS, mlDataSize, sFileName
Else
    LeerAdjunto lIDAdjunto, lCiaComp, sFileName, mlDataSize, Tipo
End If
       

fin:
    Set EscribirADisco = oTESError
    Exit Function
Error:
    On Error Resume Next
    oTESError.NumError = 200
    Resume fin
End Function

'''''' <summary>
'''''' Proceso que llama al webService de GS porque los adjuntos de pedidos est�n en la BBDD de GS
'''''' </summary>
'''''' <remarks>Llamada desde=MostrarPaginaWeb; Tiempo m�ximo=0</remarks>
Public Sub LeerAdjunto(ByVal miId As Long, ByVal lCiaComp As Long, ByVal sFileName As String, ByVal Tamanyo As Long, ByVal iTipo As Integer)
Dim objDom As Object
Dim objXmlHttp As Object
Dim parser As Object
Dim XmlBody As String
Dim strUrl As String
Dim strSoapAction As String
Dim strResultado As String
Dim strSoapActionClase As String
Dim vChunkSize As Variant
Dim oParam As CGestorParametros

Set oParam = New CGestorParametros
Set oParam.Conexion = moConexion
Set objDom = CreateObject("MSXML2.DOMDocument")
Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
'La URL y par�metros tiene que ser la de la BBDD de GS porque las tablas a las que hace referencia este metodo son todas de GS
'De momento se tratan Pedidos (orden_entrega_adjun, lineas_pedido_adjun)
strUrl = basUtilidades.ObtenerRutaWebServiceGS(moConexion) & "/FSN_Adjuntos.asmx"
strSoapActionClase = "http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos"
vChunkSize = oParam.DevolverParametroInternoGS(lCiaComp, "CHUNK_SIZE")
        
 '----------------
 Dim Chunks As Long
 Dim Fragment As Long
 Dim arrBytes() As Byte
 Dim DataFile As Integer
 Dim ChunkNumber As Long

 Chunks = Tamanyo \ vChunkSize
 Fragment = Tamanyo Mod vChunkSize
 
 If Fragment > 0 Then ' Si no ocupa  justo un chunk
     Chunks = Chunks + 1
 End If

 DataFile = 1
 
 'Abrimos el fichero para escritura binaria
 Open sFileName For Binary Access Write As DataFile
 
 For ChunkNumber = 1 To Chunks
     XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
         "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
         "<soap:Body>" & _
         "<LeerAdjunto xmlns=""" & strSoapActionClase & """>" & _
         "<Id>" & miId & "</Id>" & _
         "<ChunkNumber>" & ChunkNumber & "</ChunkNumber>" & _
         "<ChunkSize>" & vChunkSize & "</ChunkSize>" & _
         "<Tipo>" & iTipo & "</Tipo>" & _
         "<ID_Rel></ID_Rel>" & _
         "</LeerAdjunto>" & _
         "</soap:Body>" & _
         "</soap:Envelope>"
    ' Carga XML
    objDom.async = True
    objDom.loadXML XmlBody
    ' Abre el webservice
    objXmlHttp.Open "POST", strUrl, True
    ' Crea headings
    strSoapAction = strSoapActionClase & "/LeerAdjunto"
    objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
    objXmlHttp.setRequestHeader "SOAPAction", strSoapAction
     
    ' Envia XML command
    objXmlHttp.Send objDom.xml
    'Esperar hasta que se termine de ejecutar
    While Not objXmlHttp.ReadyState = 4
        DoEvents
    Wend
       
    Set parser = CreateObject("MSXML2.DOMDocument")
    strResultado = objXmlHttp.ResponseText
    parser.loadXML strResultado
 
     If InStr(strResultado, "LeerAdjuntoResult") Then
         strResultado = parser.LastChild.Text
         arrBytes = DecodeBase64(strResultado)
         Put DataFile, , arrBytes
     End If
 Next
        
 Close DataFile

 ' Cierra object
 Set objXmlHttp = Nothing
End Sub





Public Function GrabarAdjunto(ByVal strPath As String, ByVal strPathTemp As String, ByVal lCiaComp As Long, ByVal iTipo As Integer, Optional ByVal ID_Rel As String = "") As String
Dim strSoapAction As String
Dim strSoapActionClase As String
Dim strUrl As String
Dim objDom As Object
Dim objXmlHttp As Object
Dim DataFile As Integer
Dim Fl As Long
Dim Chunks As Long
Dim Fragment As Long
Dim Chunk() As Byte
Dim iChunk As Long
Dim rutaFicheroTemporal As String
Dim strBase64 As String
Dim parser As Object
Dim strResultado As String
Dim XmlBody As String
Dim sNombre As String
Dim oFos As Scripting.FileSystemObject
Dim oFile As File
Dim DataSize As Long
Dim sIDs_Rel As String ' Cuando se tienen que pasar mas c�digos se pasan en esta variable con # como separador
Dim oParam As CGestorParametros

On Error GoTo Error:

Set oParam = New CGestorParametros
Set oParam.Conexion = moConexion
sNombre = Nombre
'Si no se pasa la ruta temporal es porque ya se ha copiado previemente
Set oFos = New Scripting.FileSystemObject
If strPathTemp = "" Then
    If oFos.FileExists(strPath) Then
        'Si se pasa con el fichero
        If Right(oFos.GetParentFolderName(strPath), 1) <> "\" Then
            strPathTemp = oFos.GetParentFolderName(strPath) & "\"
        Else
            strPathTemp = oFos.GetParentFolderName(strPath)
        End If
    Else
        strPathTemp = strPath
    End If
Else
    Set oFile = oFos.GetFile(strPath & sNombre)
    oFos.CopyFile strPath & sNombre, strPathTemp & sNombre
End If
' Crea objetos DOMDocument y XMLHTTP
Set objDom = CreateObject("MSXML2.DOMDocument")
Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
    
strUrl = basUtilidades.ObtenerRutaWebServiceGS(moConexion) & "/FSN_Adjuntos.asmx"
strSoapActionClase = "http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos"
'----------------------------------------

rutaFicheroTemporal = ""

On Error GoTo Error:
DataFile = 1
'Abrimos el fichero para lectura binaria
Open strPathTemp & sNombre For Binary Access Read As DataFile
Fl = LOF(DataFile)    ' Length of data in file
If Fl = 0 Then
    Close DataFile
    Exit Function
End If
DataSize = Fl
' Se lo asignamos en bloques a la especificacion
If oParam.DevolverParametroInternoGS(lCiaComp, "CHUNK_SIZE") = 0 Then
    Chunks = 1
    Fragment = 1
Else
    Chunks = Fl \ oParam.DevolverParametroInternoGS(lCiaComp, "CHUNK_SIZE")
    Fragment = Fl Mod oParam.DevolverParametroInternoGS(lCiaComp, "CHUNK_SIZE")
    
    If Fragment > 0 Then ' Si no ocupa  justo un chunk
        Chunks = Chunks + 1
    End If

ReDim Chunk(oParam.DevolverParametroInternoGS(lCiaComp, "CHUNK_SIZE") - 1)
End If
For iChunk = 1 To Chunks
    If iChunk = Chunks And Fragment > 0 Then
        ReDim Chunk(Fragment - 1)
    End If
    Get DataFile, , Chunk()
    
    strBase64 = EncodeBase64(Chunk)
        
    XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
        "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
        "<soap:Body>" & _
        "<GrabarCarpetaTemporal xmlns=""" & strSoapActionClase & """>" & _
        "<Adjunto>" & strBase64 & "</Adjunto>" & _
        "<Nombre>" & sNombre & "</Nombre>" & _
        "<RutaTemporal>" & rutaFicheroTemporal & "</RutaTemporal>"
    XmlBody = XmlBody & "</GrabarCarpetaTemporal>" & _
        "</soap:Body>" & _
        "</soap:Envelope>"
     
    ' Carga XML
    objDom.async = True
    objDom.loadXML XmlBody
    ' Abre el webservice
    objXmlHttp.Open "POST", strUrl, True
    ' Crea headings
    objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
    objXmlHttp.setRequestHeader "SOAPAction", strSoapActionClase & "/GrabarCarpetaTemporal"

    ' Envia XML command
    objXmlHttp.Send objDom.xml
    'Esperar hasta que se termine de ejecutar
    While Not objXmlHttp.ReadyState = 4
        DoEvents
    Wend
   
    Set parser = CreateObject("MSXML2.DOMDocument")
    strResultado = objXmlHttp.ResponseText
    parser.loadXML strResultado

    If InStr(strResultado, "GrabarCarpetaTemporalResult") Then
        strResultado = parser.LastChild.Text
    End If
    Dim rdo() As String
    If InStr(1, strResultado, "#") > 0 Then
        rdo = Split(strResultado, "#", , vbTextCompare)
        If rdo(0) = "1" Then
            rutaFicheroTemporal = rdo(1)
        End If
    End If
Next

Close DataFile
'----------------------------------------

strSoapAction = strSoapActionClase & "/GrabarAdjunto"

sNombre = Replace(sNombre, "&", "@#AND")
sNombre = Replace(sNombre, "'", "@#TILDE")

Select Case iTipo
    Case 26 'Firma Digital
        sIDs_Rel = ID_Rel
End Select

XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
        "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
        "<soap:Body>" & _
        "<GrabarAdjunto xmlns=""" & strSoapActionClase & """>" & _
        "<RutaTemporal>" & rutaFicheroTemporal & "</RutaTemporal>" & _
        "<Nombre>" & sNombre & "</Nombre>" & _
        "<Tipo>" & iTipo & "</Tipo>" & _
        "<ID_Rel>" & sIDs_Rel & "</ID_Rel>" & _
        "<Comentario></Comentario>" & _
        "<DataSize>" & DataSize & "</DataSize>" & _
        "</GrabarAdjunto>" & _
        "</soap:Body>" & _
        "</soap:Envelope>"

' Carga XML
objDom.async = True
objDom.loadXML XmlBody
' Abre el webservice
objXmlHttp.Open "POST", strUrl, True
' Crea headings
objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
objXmlHttp.setRequestHeader "SOAPAction", strSoapAction

' Envia XML command
objXmlHttp.Send objDom.xml
'Esperar hasta que se termine de ejecutar
While Not objXmlHttp.ReadyState = 4
    DoEvents
Wend
   
Set parser = CreateObject("MSXML2.DOMDocument")
strResultado = objXmlHttp.ResponseText
parser.loadXML strResultado

If InStr(strResultado, "GrabarAdjunto") Then
    strResultado = parser.LastChild.Text
End If

GrabarAdjunto = strResultado

' Cierra object
Set objXmlHttp = Nothing

Error:
    Close DataFile
End Function


''' <summary>
''' Lee el adjunto de oferta bien de Portal o bien de GS, en cada caso se debe llamar al correspondiente WebService
''' </summary>
''' <param name="miId">id adjunto</param>
''' <param name="lCiaComp">cia compradora</param>
''' <param name="bFSGS">si es a GS o portal</param>
''' <param name="Tamanyo">tama�o</param>
''' <param name="sFileName">nombre fichero</param>
''' <remarks>Llamada desde: CAdjunto.EscribirADisco; Tiempo m�ximo: 0,0</remarks>
''' Revisado por: epb; Fecha: 31/08/2015
Private Sub LeerAdjuntoPortal(ByVal miId As Long, ByVal lCiaComp As Long, ByVal bFSGS As Boolean, Optional ByVal Tamanyo As Long, Optional ByVal sFileName As String)
    Dim objDom As Object
    Dim objXmlHttp As Object
    Dim parser As Object
    Dim XmlBody As String
    Dim strUrl As String
    Dim strSoapAction As String
    Dim strResultado As String
    Dim rutaWS As String
    
    If bFSGS Then 'Lo va a leer el servicio de GS
        rutaWS = basUtilidades.ObtenerRutaWebServiceGS(moConexion)
    Else 'Lo va a leer el servicio de Portal si tiene
        rutaWS = basUtilidades.ObtenerRutaWebService(moConexion)
    End If
    ' Crea objetos DOMDocument y XMLHTTP
    Set objDom = CreateObject("MSXML2.DOMDocument")
    Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
       
    strUrl = rutaWS & "/FSN_Adjuntos.asmx"
    
    strSoapAction = "http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos/LeerAdjuntoOfertaPortal"
            
    Dim Chunks As Long
    Dim Fragment As Long
    Dim arrBytes() As Byte
    Dim DataFile As Integer
    Dim ChunkNumber As Long
    Dim vChunkSize As Variant
    
    'Obtener tama�o del chunk
    Dim oParam As CGestorParametros
    Set oParam = New CGestorParametros
    Set oParam.Conexion = moConexion
    vChunkSize = oParam.DevolverParametroInternoGS(lCiaComp, "CHUNK_SIZE")
    If IsNull(vChunkSize) Or IsEmpty(vChunkSize) Then vChunkSize = giChunkSize

    Chunks = Tamanyo \ vChunkSize
    Fragment = Tamanyo Mod vChunkSize
    
    If Fragment > 0 Then ' Si no ocupa  justo un chunk
        Chunks = Chunks + 1
    End If

    DataFile = 1
    
    'Abrimos el fichero para escritura binaria
    Open sFileName For Binary Access Write As DataFile
    
    For ChunkNumber = 1 To Chunks
        XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
                "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
                "<soap:Body>" & _
                "<LeerAdjuntoOfertaPortal xmlns=""http://tempuri.org/FSN_Adjuntos/FSN_Adjuntos"">" & _
                "<Id>" & miId & "</Id>" & _
                "<Cia>" & lCiaComp & "</Cia>" & _
                "<ChunkNumber>" & ChunkNumber & "</ChunkNumber>" & _
                "<ChunkSize>" & vChunkSize & "</ChunkSize>" & _
                "<FSGS>" & IIf(bFSGS, 1, 0) & "</FSGS>" & _
                "</LeerAdjuntoOfertaPortal>" & _
                "</soap:Body>" & _
                "</soap:Envelope>"
    
        ' Carga XML
        objDom.async = True
        objDom.loadXML XmlBody
        ' Abre el webservice
        objXmlHttp.Open "POST", strUrl, True
        ' Crea headings
        objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
        objXmlHttp.setRequestHeader "SOAPAction", strSoapAction
     
        ' Envia XML command
        objXmlHttp.Send objDom.xml
        'Esperar hasta que se termine de ejecutar
        While Not objXmlHttp.ReadyState = 4
            DoEvents
        Wend
           
        Set parser = CreateObject("MSXML2.DOMDocument")
        strResultado = objXmlHttp.ResponseText
        parser.loadXML strResultado
        
        If InStr(strResultado, "LeerAdjuntoOfertaPortalResult") Then
            strResultado = parser.LastChild.Text
            arrBytes = DecodeBase64(strResultado)
            Put DataFile, , arrBytes
        End If
    Next
    
    Close DataFile
    
    ' Cierra object
    Set objXmlHttp = Nothing
End Sub


