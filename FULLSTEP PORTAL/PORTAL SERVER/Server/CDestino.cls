VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDestino"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CEquipo **********************************
'*             Autor : Javier Arana
'*             Creada : 7/9/98
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarCod As String 'local copy
Private mvarDen As String 'local copy
Private mvarSinTransporte As Integer
Private mvarDir As String
Private mvarPob As String
Private mvarCP As Variant
Private mvarProvi As String
Private mvarPai As String
Private msUON1 As Variant
Private msUON2 As Variant
Private msUON3 As Variant
Private msUON1DEN As Variant
Private msUON2DEN As Variant
Private msUON3DEN As Variant

Private mvarIndice As Long 'Nos indica la posicion secuencial dentro de una Coleccion
Private mvarConexion As CConexion 'local copy


Public Property Get Indice() As Long
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal Ind As Long)
    mvarIndice = Ind
End Property


Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property



Public Property Let Den(ByVal vData As String)
    mvarDen = vData
End Property


Public Property Get Den() As String
    Den = mvarDen
End Property



Public Property Let Cod(ByVal vData As String)
    mvarCod = vData
End Property


Public Property Get Cod() As String
    Cod = mvarCod
End Property



Public Property Let SinTransporte(ByVal vData As Boolean)
    mvarSinTransporte = vData
End Property


Public Property Get SinTransporte() As Boolean
    SinTransporte = mvarSinTransporte
End Property

Public Property Let Dir(ByVal vData As String)
    mvarDir = vData
End Property


Public Property Get Dir() As String
    Dir = mvarDir
End Property



Public Property Let Pob(ByVal vData As String)
    mvarPob = vData
End Property


Public Property Get Pob() As String
    Pob = mvarPob
End Property

Public Property Let CP(ByVal vData As String)
    mvarCP = vData
End Property


Public Property Get CP() As String
    CP = mvarCP
End Property


Public Property Let Provi(ByVal vData As String)
    mvarProvi = vData
End Property


Public Property Get Provi() As String
    Provi = mvarProvi
End Property


Public Property Let Pai(ByVal vData As String)
    mvarPai = vData
End Property


Public Property Get Pai() As String
    Pai = mvarPai
End Property


Public Property Get UON1() As Variant
    UON1 = msUON1
End Property
Public Property Let UON1(ByVal vData As Variant)
    msUON1 = vData
End Property

Public Property Get UON2() As Variant
    UON2 = msUON2
End Property
Public Property Let UON2(ByVal vData As Variant)
    msUON2 = vData
End Property


Public Property Get UON3() As Variant
    UON3 = msUON3
End Property

Public Property Let UON3(ByVal vData As Variant)
    msUON3 = vData
End Property

Public Property Get UON1DEN() As Variant
    UON1DEN = msUON1DEN
End Property

Public Property Let UON1DEN(ByVal vData As Variant)
    msUON1DEN = vData
End Property

Public Property Get UON2DEN() As Variant
    UON2DEN = msUON2DEN
End Property

Public Property Let UON2DEN(ByVal vData As Variant)
    msUON2DEN = vData
End Property

Public Property Get UON3DEN() As Variant
    UON3DEN = msUON3DEN
End Property

Public Property Let UON3DEN(ByVal vData As Variant)
    msUON3DEN = vData
End Property

