VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUsuario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_sCod As String
Private m_vNombre As Variant 'local copy
Private m_vApellidos As Variant
Private m_vNif As Variant
Private m_vCargo As Variant 'local copy
Private m_vTfno As Variant 'local copy
Private m_vTfno2 As Variant 'local copy
Private m_vTfnoMovil As Variant
Private m_vFax As Variant 'local copy
Private m_vMail As Variant 'local copy
Private m_sCodDep As String
Private m_lIndice As Long
Private m_oConexion As CConexion
Private m_vIdi As Variant
Private m_udtEstProveedor As USU_FPEST
Private m_sCodEncriptado As String
Private m_sPWDEncriptada As String
Private m_vFecPwd As Variant
Private m_vCia As Variant
Private m_vCiaId As Variant
Private m_vId As Variant
Private m_bPrincipal As Boolean
Private m_vDecimalFMT As Variant
Private m_vThousanFMT As Variant
Private m_vDateFMT As Variant
Private m_iMostrarFMT As Integer
Private m_vPrecisionFMT As Variant
Private m_vMoneda As Variant
Private m_vEQUIV As Variant
Private m_vTipoMail As Variant
Private m_vCodMoneda As Variant

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property
Public Property Get Cod() As String
  Cod = m_sCod
End Property

Public Property Let Cod(ByVal vData As String)
    Let m_sCod = vData
End Property

Public Property Let CodDep(ByVal vData As String)
    m_sCodDep = vData
End Property


Public Property Get CodDep() As String
    CodDep = m_sCodDep
End Property

Public Property Let Mail(ByVal vData As Variant)
    m_vMail = vData
End Property


Public Property Get Mail() As Variant
    Mail = m_vMail
End Property
Public Property Let Fax(ByVal vData As Variant)
    m_vFax = vData
End Property


Public Property Get Fax() As Variant
    Fax = m_vFax
End Property

Public Property Let Tfno(ByVal vData As Variant)
    m_vTfno = vData
End Property


Public Property Get Tfno() As Variant
    Tfno = m_vTfno
End Property

Public Property Let Tfno2(ByVal vData As Variant)
    m_vTfno2 = vData
End Property


Public Property Get Tfno2() As Variant
    Tfno2 = m_vTfno2
End Property


Public Property Let Cargo(ByVal vData As Variant)
    m_vCargo = vData
End Property


Public Property Get Cargo() As Variant
    Cargo = m_vCargo
End Property

Public Property Let Nombre(ByVal vData As Variant)
    m_vNombre = vData
End Property


Public Property Get Nombre() As Variant
    Nombre = m_vNombre
End Property

Public Property Let Apellidos(ByVal vData As Variant)
    m_vApellidos = vData
End Property
Public Property Get Apellidos() As Variant
    Apellidos = m_vApellidos
End Property

Public Property Let Nif(ByVal vData As Variant)
    m_vNif = vData
End Property
Public Property Get Nif() As Variant
    Nif = m_vNif
End Property

Public Property Get Indice() As Long
    Indice = m_lIndice
End Property
Public Property Let Indice(ByVal Ind As Long)
    m_lIndice = Ind
End Property
Public Property Get id() As Variant
    id = m_vId
End Property
Public Property Let id(ByVal d As Variant)
    m_vId = d
End Property
Friend Property Get EstadoProveedor() As USU_FPEST
    EstadoProveedor = m_udtEstProveedor
End Property
Friend Property Let EstadoProveedor(ByVal Data As USU_FPEST)
    m_udtEstProveedor = Data
End Property


Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub
Public Property Get Idioma() As Variant
    Idioma = m_vIdi
End Property
Public Property Let Idioma(ByVal Ind As Variant)
    m_vIdi = Ind
End Property
Public Property Get Principal() As Boolean
    Principal = m_bPrincipal
End Property
Public Property Let Principal(ByVal bPrincipal As Boolean)
    m_bPrincipal = bPrincipal
End Property
Public Property Let CodEncriptado(ByVal vData As Variant)
    m_sCodEncriptado = vData
End Property


Public Property Get CodEncriptado() As Variant
    CodEncriptado = m_sCodEncriptado
End Property
Public Property Let PWDEncriptada(ByVal vData As Variant)
    m_sPWDEncriptada = vData
End Property

Public Property Get PWDEncriptada() As Variant
    PWDEncriptada = m_sPWDEncriptada
End Property
Public Property Let FecPWD(ByVal vData As Variant)
    m_vFecPwd = vData
End Property
Public Property Get FecPWD() As Variant
    FecPWD = m_vFecPwd
End Property
Public Property Get Cia() As Variant
    Cia = m_vCia
End Property
Public Property Let Cia(ByVal Data As Variant)
    m_vCia = Data
End Property
Public Property Get CiaId() As Variant
    CiaId = m_vCiaId
End Property
Public Property Let CiaId(ByVal Data As Variant)
    m_vCiaId = Data
End Property

Public Property Get TfnoMovil() As Variant
TfnoMovil = m_vTfnoMovil
End Property

Public Property Let TfnoMovil(ByVal vData As Variant)
m_vTfnoMovil = vData
End Property

Public Property Get DecimalFMT() As Variant
    DecimalFMT = m_vDecimalFMT
End Property

Public Property Let DecimalFMT(ByVal Data As Variant)
    m_vDecimalFMT = IIf(IsNull(Data), "", Data)
End Property

Public Property Get ThousanFMT() As Variant
    ThousanFMT = m_vThousanFMT
End Property

Public Property Let ThousanFMT(ByVal Data As Variant)
    m_vThousanFMT = IIf(IsNull(Data), "", Data)
End Property

Public Property Get DateFMT() As Variant
    DateFMT = m_vDateFMT
End Property

Public Property Let DateFMT(ByVal Data As Variant)
    m_vDateFMT = IIf(IsNull(Data), "", Data)
End Property


Public Property Get MostrarFMT() As Integer
    MostrarFMT = m_iMostrarFMT
End Property

Public Property Let MostrarFMT(ByVal Data As Integer)
    m_iMostrarFMT = Data
End Property

Public Property Get PrecisionFMT() As Variant
    PrecisionFMT = m_vPrecisionFMT
End Property

Public Property Let PrecisionFMT(ByVal Data As Variant)
    m_vPrecisionFMT = Data
End Property

Public Property Get Moneda() As Variant
    Moneda = m_vMoneda
End Property

Public Property Let Moneda(ByVal Data As Variant)
    m_vMoneda = Data
End Property

Public Property Get CodMoneda() As Variant
    CodMoneda = m_vCodMoneda
End Property

Public Property Let CodMoneda(ByVal Data As Variant)
    m_vCodMoneda = Data
End Property

Public Property Get EQUIV() As Variant
    EQUIV = m_vEQUIV
End Property

Public Property Let EQUIV(ByVal Data As Variant)
    m_vEQUIV = Data
End Property

Public Property Get TipoMail() As Variant
    TipoMail = m_vTipoMail
End Property

Public Property Let TipoMail(ByVal Data As Variant)
    If IsNull(Data) Then
        m_vTipoMail = 0
    Else
        m_vTipoMail = Data
    End If
End Property

