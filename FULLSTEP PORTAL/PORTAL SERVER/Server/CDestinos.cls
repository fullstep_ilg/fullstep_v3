VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDestinos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'********************* CDestinos ********************************
'*             Autor : Javier Arana
'*             Creada : 7/9/98
'****************************************************************

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion


Public Function Add(ByVal Cod As String, ByVal Den As String, _
                    Optional ByVal SinTransporte As Boolean, _
                    Optional ByVal Dir As Variant, _
                    Optional ByVal Pob As Variant, _
                    Optional ByVal CP As Variant, _
                    Optional ByVal Provi As Variant, _
                    Optional ByVal Pai As Variant, _
                    Optional ByVal varIndice As Variant, _
                    Optional ByVal sUON1 As Variant, Optional ByVal sUON2 As Variant, Optional ByVal sUON3 As Variant, _
                    Optional ByVal sUON1DEN As Variant, Optional ByVal sUON2DEN As Variant, Optional ByVal sUON3DEN As Variant) As CDestino
    'create a new object
    Dim objnewmember As CDestino
    Set objnewmember = New CDestino
    With objnewmember
        .Cod = Cod
        .Den = Den
        .SinTransporte = SinTransporte
    
        .Dir = IIf(IsMissing(Dir) Or IsNull(Dir), "", Dir)
        .Pob = IIf(IsMissing(Pob) Or IsNull(Pob), "", Pob)
        .CP = IIf(IsMissing(CP) Or IsNull(CP), "", CP)
        .Provi = IIf(IsMissing(Provi) Or IsNull(Provi), "", Provi)
        .Pai = IIf(IsMissing(Pai) Or IsNull(Pai), "", Pai)
        .UON1 = sUON1
        .UON2 = sUON2
        .UON3 = sUON3
        .UON1DEN = sUON1DEN
        .UON2DEN = sUON2DEN
        .UON3DEN = sUON3DEN
    
        Set .Conexion = mvarConexion
    
        If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
            .Indice = varIndice
            mCol.Add objnewmember, CStr(varIndice)
        Else
           
           mCol.Add objnewmember, Cod
        End If
    End With
    
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Property Get Item(vntIndexKey As Variant) As CDestino
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
On Error GoTo NoSeEncuentra
    
    mCol.Remove vntIndexKey
    Exit Sub
NoSeEncuentra:

End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
  
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

Public Function DevolverDestino(ByVal Cod As String) As CDestino
Dim oDest As CDestino
Dim iIndice As Integer
On Error Resume Next

For iIndice = 1 To mCol.Count
    
    Set oDest = mCol.Item(iIndice)
    If oDest.Cod = Cod Then
        Set DevolverDestino = oDest
        Set oDest = Nothing
        Exit Function
    End If

Next

Set oDest = Nothing
Set DevolverDestino = Nothing

End Function

''' <summary>
''' Cargar la colecci�n de destinos
''' </summary>
''' <param name="sIdi">Idioma</param>
''' <param name="CiaComp">C�digo de compania</param>
''' <param name="NumMaximo">Num Maximo de destinos a cargar</param>
''' <param name="CarIniCod">Codigo destino a cargar</param>
''' <param name="CarIniDen">Denominaci�n destino a cargar</param>
''' <param name="CoincidenciaTotal">Usar like o =</param>
''' <param name="OrdenadosPorDen">Orden por denominaci�n o codigo</param>
''' <param name="CiaComp">C�digo de compania</param>
''' <param name="UsarIndice">Al crear la clase cada registro identificarlo por codigo � por numeros correlativos</param>
''' <param name="bConUON">cargar informaci�n de uon o no cargar</param>
''' <remarks>Llamada desde: confirmoferta.asp   cargaritems.asp; Tiempo m�ximo: 0,1</remarks>
Public Sub CargarTodosLosDestinosDesde(ByVal sIdi As String, ByVal CiaComp As Long, ByVal NumMaximo As Integer, _
                                       Optional ByVal CarIniCod As String, _
                                       Optional ByVal CarIniDen As String, _
                                       Optional ByVal CoincidenciaTotal As Boolean, _
                                       Optional ByVal OrdenadosPorDen As Boolean, _
                                       Optional ByVal UsarIndice As Boolean, _
                                       Optional ByVal bConUON As Boolean, _
                                       Optional ByVal lIdLinea As Long = 0)
Dim lIndice As Long
Dim sConsulta As String

Dim sFSG As String

Dim adoComm As adodb.Command
Dim adoRS As adodb.Recordset
Dim adoParam As adodb.Parameter

Dim sUON1 As Variant
Dim sDenUon1 As Variant
Dim sUON2 As Variant
Dim sDenUon2 As Variant
Dim sUON3 As Variant
Dim sDenUon3 As Variant

sIdi = EsIdiomaValido(sIdi)
sFSG = Fsgs(mvarConexion, CiaComp)

Set adoComm = New adodb.Command
Set adoComm.ActiveConnection = mvarConexion.AdoCon
adoComm.CommandType = adCmdText
sConsulta = "EXEC " & sFSG & "SP_DEVOLVER_DESTINOS @COD=?, @DEN =?, @COINCIDE =?, @ORDERDEN =?, @NUMMAXIMO =?, @IDLINEA=?"

Set adoParam = adoComm.CreateParameter("COD", adVarChar, adParamInput, 50, IIf(CarIniCod = "", Null, CarIniCod))
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("DEN", adVarChar, adParamInput, 500, IIf(CarIniDen = "", Null, CarIniDen))
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("COINCIDE", adTinyInt, adParamInput, , IIf(CoincidenciaTotal, 1, 0))
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("ORDERDEN", adTinyInt, adParamInput, , IIf(OrdenadosPorDen, 1, 0))
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("NUMMAX", adInteger, adParamInput, , NumMaximo)
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("IDLINEA", adVarChar, adParamInput, 20, lIdLinea)
adoComm.Parameters.Append adoParam

If bConUON Then
    sConsulta = sConsulta & ", @CONUON=?"
    Set adoParam = adoComm.CreateParameter("CONUON", adTinyInt, adParamInput, , IIf(bConUON, 1, 0))
    adoComm.Parameters.Append adoParam
End If

sConsulta = sConsulta & ", @IDI=?"
Set adoParam = adoComm.CreateParameter("IDI", adVarChar, adParamInput, 20, sIdi)
adoComm.Parameters.Append adoParam


adoComm.CommandText = sConsulta

Set adoRS = adoComm.Execute


If adoRS.eof Then
        
    adoRS.Close
    Set adoRS = Nothing
    Set mCol = Nothing
    Set mCol = New Collection
    Exit Sub
      
Else
    Set mCol = Nothing
    Set mCol = New Collection
    lIndice = 0
    
    While Not adoRS.eof
    
        If bConUON Then
            If Not IsNull(adoRS.Fields("U0DEST").Value) Then
                sUON1 = Null
                sDenUon1 = Null
                sUON2 = Null
                sDenUon2 = Null
                sUON3 = Null
                sDenUon3 = Null
            Else
                If Not IsNull(adoRS.Fields("COD1U1").Value) Then
                    sUON1 = adoRS.Fields("COD1U1").Value
                    sDenUon1 = adoRS.Fields("DEN1U1").Value
                    sUON2 = Null
                    sDenUon2 = Null
                    sUON3 = Null
                    sDenUon3 = Null
                Else
                    If Not IsNull(adoRS.Fields("COD2U1").Value) Then
                        sUON1 = adoRS.Fields("COD2U1").Value
                        sDenUon1 = adoRS.Fields("DEN2U1").Value
                        sUON2 = adoRS.Fields("COD2U2").Value
                        sDenUon2 = adoRS.Fields("DEN2U2").Value
                        sUON3 = Null
                        sDenUon3 = Null
                    Else
                        sUON1 = adoRS.Fields("COD3U1").Value
                        sDenUon1 = adoRS.Fields("DEN3U1").Value
                        sUON2 = adoRS.Fields("COD3U2").Value
                        sDenUon2 = adoRS.Fields("DEN3U2").Value
                        sUON3 = adoRS.Fields("COD3U3").Value
                        sDenUon3 = adoRS.Fields("DEN3U3").Value
                    End If
                End If
            End If
        Else
            sUON1 = Null
            sDenUon1 = Null
            sUON2 = Null
            sDenUon2 = Null
            sUON3 = Null
            sDenUon3 = Null
        End If
        
        If UsarIndice Then
            Me.Add adoRS("COD").Value, adoRS("DEN").Value, adoRS("SINTRANS"), adoRS("DIR"), adoRS("POB"), adoRS("CP"), adoRS("PROVI"), adoRS("PAI"), lIndice, sUON1, sUON2, sUON3, sDenUon1, sDenUon2, sDenUon3
            lIndice = lIndice + 1
        Else
            Me.Add adoRS("COD").Value, adoRS("DEN").Value, adoRS("SINTRANS"), adoRS("DIR"), adoRS("POB"), adoRS("CP"), adoRS("PROVI"), adoRS("PAI"), , sUON1, sUON2, sUON3, sDenUon1, sDenUon2, sDenUon3
        End If
    
        adoRS.MoveNext
    Wend
    adoRS.Close
    Set adoRS = Nothing
     
End If

End Sub


''' <summary>
''' Cargar los datos de un Destino
''' </summary>
''' <param name="sIdi">Idioma</param>
''' <param name="CiaComp">C�digo de compania</param>
''' <param name="Destino">Destino</param>
''' <returns>los datos de un Destino</returns>
''' <remarks>Llamada desde: rptPedidoDetalle.asp    rptPedido.asp   rptPedidos.asp  imprimiroferta.asp  resultadopuja.asp; Tiempo m�ximo: 0,1</remarks>
Public Function CargarDatosDestino(ByVal sIdi As String, ByVal CiaComp As Long, ByVal Destino As String, Optional ByVal LineaPedido As String = "") As adodb.Recordset

Dim lIndice As Long
Dim sConsulta As String

Dim sFSG As String

Dim adoComm As adodb.Command
Dim adoRS As adodb.Recordset
Dim adoParam As adodb.Parameter
Dim lIdLinea As Long
If LineaPedido = "" Then
    lIdLinea = 0
Else
    lIdLinea = CLng(LineaPedido)
End If
sIdi = EsIdiomaValido(sIdi)
sFSG = Fsgs(mvarConexion, CiaComp)

Set adoComm = New adodb.Command
Set adoComm.ActiveConnection = mvarConexion.AdoCon
adoComm.CommandType = adCmdText
sConsulta = "EXEC " & sFSG & "SP_DEVOLVER_DESTINOS @COD=?, @COINCIDE =1, @IDI=?, @IDLINEA=?"
adoComm.CommandText = sConsulta

Set adoParam = adoComm.CreateParameter("COD", adVarChar, adParamInput, 50, Destino)
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("IDI", adVarChar, adParamInput, 20, sIdi)
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("IDLINEA", adVarChar, adParamInput, 20, lIdLinea)
adoComm.Parameters.Append adoParam
Set adoRS = adoComm.Execute
        
If adoRS.eof Then
    adoRS.Close
    Set adoRS = Nothing
    Set CargarDatosDestino = Nothing
    Exit Function
      
Else
    adoRS.ActiveConnection = Nothing
    Set CargarDatosDestino = adoRS
    
     
End If

End Function



