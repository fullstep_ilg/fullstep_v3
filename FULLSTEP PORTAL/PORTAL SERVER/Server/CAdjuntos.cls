VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAdjuntos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAdjuntos ********************************
'*             Autor : Hilario Barrenkua
'*             Creada : 20-11-2000
'****************************************************************



Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection 'Contendra los archivos adjuntos de una oferta
Private mvarConexion As CConexion
Private mvarSize As Long



Public Property Get Item(vntIndexKey As Variant) As CAdjunto
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Size() As Long
Size = mvarSize
End Property



Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

Public Function Add(ByVal oObjeto As Object, _
                    ByVal ID As Long, _
                    ByVal Nombre As String, _
                    ByVal dataSize As Long, _
                    Optional ByVal Comentario As Variant, _
                    Optional ByVal AdjunID As Variant, _
                    Optional ByVal IDPortal As Variant, _
                    Optional ByVal OP As Variant) As CAdjunto
    'create a new object
    Dim sCod As String
    Dim objnewmember As CAdjunto
    Set objnewmember = New CAdjunto
    Set objnewmember.Objeto = oObjeto
    
    objnewmember.Nombre = Nombre
    objnewmember.ID = ID
    
    If IsMissing(dataSize) Then
        objnewmember.dataSize = 0
    Else
        objnewmember.dataSize = dataSize
    End If
    
'    mvarSize = mvarSize + objnewmember.dataSize
    
    If IsMissing(AdjunID) Then
        objnewmember.AdjunID = Null
    Else
        objnewmember.AdjunID = AdjunID
    End If
    
    If IsMissing(Comentario) Then
        objnewmember.Comentario = Null
    Else
        objnewmember.Comentario = Comentario
    End If
    
    If IsMissing(IDPortal) Then
        objnewmember.AdjunIDPortal = Null
    Else
        objnewmember.AdjunIDPortal = IDPortal
    End If

    If IsMissing(OP) Then
        objnewmember.Operacion = "I"
    Else
        objnewmember.Operacion = OP
    End If
    
    Set objnewmember.Conexion = mvarConexion
    
    mCol.Add objnewmember, CStr(ID)
    
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Sub Remove(vntIndexKey As Variant)
    mvarSize = mvarSize - mCol(vntIndexKey).dataSize
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    mvarSize = 0
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'EspProceroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

