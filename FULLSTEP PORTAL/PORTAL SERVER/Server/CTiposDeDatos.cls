VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTiposDeDatos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Public Enum USU_FCEST
    FCESTSinFunction = 0
    FCESTSolicitada = 1
    FCESTNoActiva = 2
    FCESTActiva = 3
End Enum

Public Enum USU_FPEST
    FPESTSinFunction = 0
    FPESTSolicitada = 1
    FPESTNoActiva = 2
    FPESTActiva = 3
End Enum

Public Enum CIAS_FCEST
    FCESTSinFuncion = 0
    FCESTSolicitada = 1
    FCESTNoActiva = 2
    FCESTActiva = 3
End Enum


Public Enum CIAS_PORT_FPEST
        
    FPESTSinFuncion = 0
    FPESTSolicitada = 1
    FPESTInactiva = 2
    FPESTActiva = 3
    
End Enum

Public Enum REL_CIAS_EST
        
    ESTInactiva = 2
    ESTActiva = 3
    
End Enum

Public Enum TipoOrdenacionItems
    
    OrdItemPorCodArt = 1
    OrdItemPorDen = 2
    OrdItemPorDest = 3
    OrdItemPorUni = 4
    OrdItemPorCant = 5
    OrdItemPorPrec = 6
    OrdItemPorPres = 7
    OrdPorPago = 8
    OrdItemPorFecIni = 9
    OrdItemPorFecFin = 10
    
End Enum

Public Type TipoDatosCombo
   Cod() As Variant
   Den() As Variant
   id() As Variant
End Type

Public Enum ErroresSummit

    TESnoerror = 0
    TESUsuarioNoValido = 1
    TESDatoDuplicado = 2
    TESDatoEliminado = 3
    TESDatoNoValido = 4
    TESInfModificada = 5
    TESFaltanDatos = 6
    TESImposibleEliminar = 7
    tesCodigoProcesoCambiado = 8 'No es un error en si. Indica que al dar de alta el numero mostrado de proceso ha cambiado.
    TESAdjAntDenegada = 9        'No es un error en si. Indica que no se pueden dar de alta adjudicaciones anteiores al summit, porque ya hay nuevas.
    TESCantDistMayorQueItem = 10 ' La cantidad de distribucion supera a la del item.
    TESValidarSinItems = 20      ' Intentar validar un proceso sin items
    TESValidarYaValidado = 21    ' Intentar validar un proceso ya validado. El campo Arg1 contendra la fecha de validacion, y el Arg2 el codigo de usuario.
    TESValidarSelProveSinProve = 22
    TESValidarSelProveYaValidado = 23
    TESValidarConfItemsYaValidada = 24
    TESVolumenAdjDirSuperado = 25 'Volumen de adjudicaci�n directa superado
    TESOfertaAdjudicada = 26
    TESCierreSinPreAdjudicaciones = 27
    TESImposibleReabrir = 28
    TESAlGuardarAR_PROCE = 29
    TESCambioContrasenya = 30
    tesprocesocerrado = 31 ' Al intentar modificar o eliminar un proceso que ha pasado a estado cerrado.
    tesimposiblecambiarfecha = 32 'Al intentar cambiar la fecha de una reuni�n
    TESProcesoBloqueado = 33 'Al intentar iniciar la edicion del proceso bloqueandolo
    TESImposibleEliminarAsignacion = 34 'Al intentar desasignar un proveedore al que ya se le han enviado peticiones
    TESImposibleExcluirItem = 35 ' Al intentar excluir un item que ya tiene adjudicaciones
    TESImposibleAdjudicarAProveNoHom = 36 ' Al intentar validarycerrar un proceso adjudicando a proveedores no homologados
    TESImposibleModificarReunion = 37 'Al intentar modificar una reuni�n que ya ha tenido algun resultado. (No al a�adir procesos nuevos a esa reuni�n)
    TESImposibleAsignarProveedor = 38 'Al intentar asignar un proveedor que ya est� asignado
    TESInfActualModificada = 39 'Cuando iniciamos la edici�n de una fila y vemos que los datos actuales han cambiado
    'TESImposiblePreadjudicar = 40 'El proceso ya ha tenido resultados en una reuni�n. Ya no podemos preadjudicar
    TESMail = 40 'Para tratamiento de errores de envio de mail
    TESProvePortalDesautorizado = 41
    TESCiaPortalDesautorizada = 42
    TESImposibleAdjuntarArchivo = 43
    TESExcelIncorrecta = 45
    TESCostesDescOProcesoDistintosGS = 46 'Se han modificado los costes y descuentos pero se puede recalcular
    TESErrorRecalculoOferta = 47
    TESImposibleEnviarPedido = 48
    TESErrorEnviarPedido = 49
    TESCostesDescModificadosGS = 50 'Se han modificado los costes y descuentos y  no se puede recalcular
    TESProcesoEliminado = 51
    TESProcesoDespublicado = 52
    TESProcesoFecLimPasada = 53
    TESMonedaNoValida = 55
    
    
    TESOtroerror = 100
    TESOtroErrorNOBD = 200
    TESErrorSharePoint = 300
    TESErrorCodigoDuplicado = 301
    TESErrorAccesoGS = 400
    TESErrorConstPdfYOFirmaDig = 1000
    
    TESErrorErrorServicio = 10000
    TESErrorPWDCorta = 10001
    TESErrorPWDEdadMin = 10002
    TESErrorPWDCondicionesComplejidad = 10003
    TESErrorPWDHistorico = 10004
End Enum


Public Type ParametrosGenerales
    gbPremium As Boolean
    glMAXADJUN As Long
    gsServerName As String
    gsBDName As String
    gsGrupo As String
    gsDominio As String
    g_iNivelMinAct As Integer       'Nivel m�nimo de actividad
    g_sNomPortal As String
    g_sUrl As String
    g_sAdm As String
    g_sEmail As String
    g_iTipoEmail As Integer
    g_iNotifSolicReg As Integer
    g_iNotifSolicAct As Integer
    g_iNotifSolicComp As Integer
    
End Type


Public Enum TipoDefinicionDatoProceso
    NoDefinido = 0  ' No est�n definidos
    EnProceso = 1   ' A nivel de proceso
    EnGrupo = 2     ' A nivel de grupo
    EnItem = 3      ' A nivel de item
End Enum

Public Enum TiposDeAtributos
    TDAString = 1
    TDAEntero = 2
    TDAFecha = 3
    TDABoolean = 4
End Enum


Public Enum TipoDeEstadoOfertas
    TDENueva = 1
    TDESinEnviar = 2
    TDEEnviada = 3
    TDEEnviandose = 5
End Enum

Public Enum TipoDeCondicion
    TDCSuperaPrecioSalida = 1
    TDCSuperaPujaPropia = 2
    TDCBajadaMinimaProve = 3
    TDCBajadaMinima = 4
    TDCIgualaPujaExistente = 5
    TDCIgualaPujaPropia = 6

End Enum

Public Enum TModoEscalado
    ModoRangos = 0
    ModoDirecto = 1
End Enum

Public Enum TipoImputacionSC
    NoImputa = 0
    Opcional = 1
    Obligatorio = 2
End Enum

Public Enum TipoModoImputacionSM
    NivelCabecera = 0
    Nivellinea = 1
End Enum


'***********************************************************
' Tipos de datos para INTEGRACION
'***********************************************************

' Tipo de dato para las distintas entidades a integrar
Public Enum EntidadIntegracion
    Mon = 1
    Pai = 2
    Provi = 3
    Pag = 4
    Dest = 5
    Uni = 6
    art4 = 7
    Prove = 8
    Adj = 9
    Recibo = 10
    con = 101
    PED_Aprov = 11
    Rec_Aprov = 12
    PED_directo = 13
    Rec_Directo = 14
    Materiales = 15
    PresArt = 16
    
    SolicitudPM = 18
    
    Presupuestos1 = 19
    Presupuestos2 = 20
    Presupuestos3 = 21
    Presupuestos4 = 22
    Proceso = 23
    TablasExternas = 24
    ViaPag = 25
    
    PPM = 26
    TasasServicio = 27
    CargosProveedor = 28
    
    UnidadesOrganizativas = 29
    Usuarios = 30
    
    Activos = 31
    Facturas = 32
    Pagos = 33
    Pres5 = 34
    CentroCoste = 35
    Empresa = 36
    
    AsignacionProv = 37
    RecepcionOFE = 38
End Enum

' Indica el origen de un movimiento en el LOG
Public Enum OrigenIntegracion
    FSGSInt = 0
    FSGSReg = 1
    FSGSIntReg = 2
    ERP = 3
End Enum

' Indican los distintos tipos de destino de integraci�n cuando se exportan los cambios
Public Enum TipoDestinoIntegracion
    NTFS = 1
    FTP = 2
    WEBSERVICE = 3
    wcf = 4
End Enum

'Indica para cada entidad si se exportan, importan (o los dos) los cambios
Public Enum SentidoIntegracion
    salida = 1
    Entrada = 2
    EntradaSalida = 3
End Enum

Public Enum EstadoIntegracion
    PendienteTratar = 0
    PendienteAcuse = 1
    ConError = 3
    OK = 4
End Enum

Public Type TipoErrorSummit
    NumError As ErroresSummit
    Arg1 As Variant
    Arg2 As Variant
End Type

Public Enum TAtributoIntroduccion
    IntroLibre = 0
    Introselec = 1
End Enum
Public Enum TipoAplicarAPrecio
    TotalOferta = 0
    TotalGrupo = 1
    TotalItem = 2
    UnitarioItem = 3
End Enum

' TIPOS DE USUARIOS SUMMIT
Public Enum TipoDeUsuario
    Administrador = 1
    Comprador = 2
    Persona = 3
    Proveedor = 4
'    UsuarioExterno = 5
End Enum

'Indica los distintos estados de integraci�n de los PROCESOS
Public Enum EstadoIntegracionProce
    Pendiente = 0
    ParcialEnviado = 11
    ParcialError = 13
    ParcialOK = 14
    TotalEnviado = 21
    TotalError = 23
    TotalOK = 24
End Enum

Public Enum TipoPedido
    Directo = 0   'Ahora se llaman negociados
    Aprovisionamiento = 1  'Ahora se llaman de catalogo
    PedidosPM = 2  'Ahora se llaman directos
    PedidosERP = 3 'Ahora se llaman de ERP
    PedidosExpress = 4 'FSN_DES_GS_PRT_3278 - Pedidos generados desde solicitudes de tipo Pedido Express
    PedidoAbierto = 5
    ContraPedidoAbierto = 6
End Enum

Public Type ParametroIntegracion
    gaExportar As Boolean
    gaSoloImportar As Boolean
End Type

Public Enum TLog_Pedido
    Alta_Log_Pedido = 0
    Cabecera_LOG = 1
    Adjuntos_Cabecera = 2
    Atrib_Cabecera = 3
    CD_Cabecera = 4
    LineaPedido = 5
    CD_LineaPedido = 6
    Impuestos_Linea = 7
    Imputacion_GS = 8
    Imputacion_SM = 9
    Atrib_LineaPedido = 10
    PlanEntrega_LineaPedido = 11
    Nueva_LineaPedido = 12
    Eliminar_LineaPedido = 13
    Eliminar_CD_Cabecera = 14
    Adjuntos_Linea = 15
    Eliminar_CD_Linea = 16
    Eliminar_PlanEntrega_Linea = 17
    Deshacer_Eliminar_LineaPedido = 18
    Cantidad_Pedida_Linea = 19
End Enum
