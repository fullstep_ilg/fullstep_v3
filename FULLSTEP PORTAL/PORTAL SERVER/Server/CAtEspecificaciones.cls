VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAtEspecificaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private moConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
Set moConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property

Public Property Get Item(vntIndexKey As Variant) As CAtriEspecificacion
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get Count() As Long
If mCol Is Nothing Then
    Count = 0
Else
    Count = mCol.Count
End If
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
    Set NewEnum = mCol.[_NewEnum]
End Property

Public Function Add(ByVal ID As Integer, ByVal Cod As String, ByVal descr As String, _
                    ByVal Desc As String, ByVal Atributo As Integer, ByVal Interno As Integer, _
                    ByVal Pedido As Integer, ByVal Orden As Integer, _
                    Optional ByVal oProceso As CProceso, Optional ByVal oGrupo As CGrupo, _
                    Optional ByVal oItem As CItem, Optional ByVal Valor_num As Variant, _
                    Optional ByVal Valor_text As Variant, Optional ByVal Valor_fec As Variant, _
                    Optional ByVal Valor_bool As Variant, Optional ByVal tipo As Variant) As CAtriEspecificacion


    'create a new object
    Dim sCod As String
    Dim objnewmember As CAtriEspecificacion
    Set objnewmember = New CAtriEspecificacion
   
    'Obligatorios
    'id , cod , descr ,Desc , Atributo ,Interno, Pedido , Orden
    

    objnewmember.ID = ID
    objnewmember.Cod = Cod
    objnewmember.descr = descr
    objnewmember.Desc = Desc
    objnewmember.Atributo = Atributo
    objnewmember.Interno = Interno
    objnewmember.Pedido = Pedido
    objnewmember.Orden = Orden
    
    
    'Optionales
    'oProceso , oGrupo , oItem ,
    'Valor_num , Valor_text , Valor_fec As Variant, Valor_bool
    
    If Not IsMissing(oProceso) Then
        Set objnewmember.Proceso = oProceso
    End If
    
    If Not IsMissing(oGrupo) Then
        Set objnewmember.Grupo = oGrupo
    End If
    
    If Not IsMissing(oItem) Then
        Set objnewmember.Item = oItem
    End If
    
    
    If IsMissing(Valor_num) Then
        objnewmember.Valor_num = Null
    Else
        objnewmember.Valor_num = Valor_num
    End If
    
    If IsMissing(Valor_text) Then
        objnewmember.Valor_text = Null
    Else
        objnewmember.Valor_text = Valor_text
    End If
    
    If IsMissing(Valor_fec) Then
        objnewmember.Valor_fec = Null
    Else
        objnewmember.Valor_fec = Valor_fec
    End If
   
    If IsMissing(Valor_bool) Then
        objnewmember.Valor_bool = Null
    Else
        objnewmember.Valor_bool = Valor_bool
    End If
      
    If IsMissing(tipo) Then
        objnewmember.tipo = Null
    Else
        objnewmember.tipo = tipo
    End If
      
      
    mCol.Add objnewmember, CStr(Atributo)
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function
Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub
Private Sub Class_Initialize()
   
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set moConexion = Nothing
End Sub

''' <summary>
''' Devolver los atributos de especificaci�n
''' </summary>
''' <param name="lCiaComp">C�digo de compania</param>
''' <param name="iAnyo">A�o de proceso</param>
''' <param name="sGMN1">GMN1 de proceso</param>
''' <param name="iProce">Numero de proceso</param>
''' <param name="sGrupo">C�digo de grupo</param>
''' <param name="vAmbito">1 proceso 2 grupo 3 item </param>
''' <param name="lAtrib">id de atributo</param>
''' <param name="lItem">id de item</param>
''' <returns>Si ha habido error o no</returns>
''' <remarks>Llamada desde: atributoespecificacion.asp; Tiempo m�ximo:0,2</remarks>
Public Function DevolverTodosLosAtributosEspeDesde(ByVal lCiaComp As Long, ByVal iAnyo As Integer, _
ByVal sGMN1 As String, ByVal iProce As Long, _
Optional ByVal sGrupo As Variant, Optional ByVal vAmbito As Variant, _
Optional ByVal lAtrib As Variant, Optional ByVal lItem As Variant) As Boolean
    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim adoRes As ADODB.Recordset
    Dim adoCommPg As ADODB.Command
    Dim adoParamPg As ADODB.Parameter
    Dim sConsulta As String
    Dim sFSG As String
    
    Dim ador As ADODB.Recordset
    Dim sConsulta2 As String
     
    Dim iAmbito  As Integer
    'JORGE: si iAmbito=1 --> proceso; si no el resto
    
    On Error GoTo Error
    
    
    sFSG = Fsgs(moConexion, lCiaComp)
    Dim iGrupo As Long
    
    Set adoComm = New ADODB.Command
    
    Set adoComm.ActiveConnection = moConexion.AdoCon
    
    sConsulta = "EXEC " & "SP_DEVOLVER_PROCE_ATRIBESP_WEB @CIA_COMP=?, @ANYO=?, @GMN1=?, @PROCE=?, @AMBITO=?"
    
    Set adoParam = adoComm.CreateParameter("CIA_COMP", adSmallInt, adParamInput, , lCiaComp)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter("ANYO", adSmallInt, adParamInput, , iAnyo)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter("GMN1", adVarChar, adParamInput, 50, sGMN1)
    adoComm.Parameters.Append adoParam
    
    Set adoParam = adoComm.CreateParameter("PROCE", adInteger, adParamInput, , iProce)
    adoComm.Parameters.Append adoParam
    
    iAmbito = 1
    
    If Not IsMissing(vAmbito) Then
        iAmbito = vAmbito
        If iAmbito = 2 Then
            Set adoCommPg = New ADODB.Command
    
            Set adoCommPg.ActiveConnection = moConexion.AdoCon
            
            'CALIDAD Resulta q no se puede poner WITH (NOLOCK) en esta sentencia pq esta preguntando por tablas de otro servidor
            'De poner WITH(NOLOCK) dar�a el siguiente error:
            '   Number : -2147217900
            '   Description: Cannot specify an index or locking hint for a remote data source.
            sConsulta2 = "SELECT ID FROM " & sFSG & "PROCE_GRUPO PG "
            sConsulta2 = sConsulta2 & " WHERE ANYO= ? AND GMN1=? AND PROCE=? AND COD=? "
            Set ador = New ADODB.Recordset
            
            Set adoParamPg = adoCommPg.CreateParameter("ANYO", adSmallInt, adParamInput, , iAnyo)
            adoCommPg.Parameters.Append adoParamPg
            
            Set adoParamPg = adoCommPg.CreateParameter("GMN1", adVarChar, adParamInput, 50, StrToVbNULL(sGMN1))
            adoCommPg.Parameters.Append adoParamPg
            
            Set adoParamPg = adoCommPg.CreateParameter("PROCE", adInteger, adParamInput, , iProce)
            adoCommPg.Parameters.Append adoParamPg
            
            Set adoParamPg = adoCommPg.CreateParameter("GRUPO", adVarChar, adParamInput, 50, StrToVbNULL(sGrupo))
            adoCommPg.Parameters.Append adoParamPg
            
            adoCommPg.CommandType = adCmdText
            adoCommPg.CommandText = sConsulta2
            Set ador = adoCommPg.Execute
                    
            If ador.eof Then
                iGrupo = 0
            Else
                iGrupo = NullToDbl0(ador("ID").Value)
            End If
            
            Set adoParamPg = Nothing
            Set adoCommPg = Nothing
        End If
    End If
    
    Set ador = New ADODB.Recordset
    
    Set adoParam = adoComm.CreateParameter("AMBITO", adTinyInt, adParamInput, , iAmbito)
    adoComm.Parameters.Append adoParam
    
    
    If Not IsMissing(sGrupo) Then
        If iGrupo > 0 Then
            sGrupo = iGrupo
            Set adoParam = adoComm.CreateParameter("GRUPO", adVarChar, adParamInput, 50, sGrupo)
            adoComm.Parameters.Append adoParam
            sConsulta = sConsulta & ", @GRUPO=? "
        End If
    End If
    
    
    If Not IsMissing(lAtrib) Then
        Set adoParam = adoComm.CreateParameter("ATRIB", adInteger, adParamInput, , lAtrib)
        adoComm.Parameters.Append adoParam
        sConsulta = sConsulta & ", @ATRIB=? "
    End If
    
    If Not IsMissing(lItem) Then
        Set adoParam = adoComm.CreateParameter("ITEM", adInteger, adParamInput, , lItem)
        adoComm.Parameters.Append adoParam
        sConsulta = sConsulta & ", @ITEM=? "
    End If
    
    Dim oAtributo As CAtriEspecificacion
    
    adoComm.CommandType = adCmdText
    adoComm.CommandText = sConsulta
    Set adoRes = adoComm.Execute
    
    If Not adoRes.eof Then
        While Not adoRes.eof
            Set oAtributo = Me.Add(adoRes("ID").Value, adoRes("COD").Value, "" & adoRes("DESCR").Value, "" & adoRes("DEN").Value, adoRes("ATRIB").Value, adoRes("INTERNO").Value, adoRes("PEDIDO").Value, adoRes("ORDEN").Value)
            
            adoRes.MoveNext
        Wend
        
    End If
    
    
    DevolverTodosLosAtributosEspeDesde = True
        
fin:
    On Error Resume Next
    adoRes.Close
    Set adoComm = Nothing
    Set adoRes = Nothing
    Exit Function
    
    
Error:
    DevolverTodosLosAtributosEspeDesde = False
    Resume fin

End Function


