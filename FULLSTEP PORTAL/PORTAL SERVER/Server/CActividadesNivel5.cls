VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CActividadesNivel5"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CActividadesNivel5 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion




Public Function Add(ByVal ACN1 As Integer, ByVal ACN2 As Integer, ByVal ACN3 As Integer, ByVal ACN4 As Integer, ByVal ID As Integer, ByVal Cod As String, ByVal Den As String, Optional ByVal varIndice As Variant, Optional ByVal varAsign As Variant, Optional ByVal varPendiente As Variant) As CActividadNivel5
    'create a new object
    Dim objnewmember As CActividadNivel5
    Dim sCod As String
    
    Set objnewmember = New CActividadNivel5
   
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    objnewmember.ID = ID
    objnewmember.ACN1 = ACN1
    objnewmember.ACN2 = ACN2
    objnewmember.ACN3 = ACN3
    objnewmember.ACN4 = ACN4
    
    Set objnewmember.Conexion = mvarConexion
    If Not IsMissing(varAsign) Then
        If varAsign Then
            objnewmember.Asignada = True
        Else
            objnewmember.Asignada = False
        End If
        
    End If
    If Not IsMissing(varPendiente) Then
        If varPendiente Then
            objnewmember.Pendiente = True
        Else
            objnewmember.Pendiente = False
        End If
    Else
            objnewmember.Pendiente = False
    End If
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        sCod = CStr(ACN1) & Mid$("                         ", 1, 10 - Len(CStr(ACN1)))
        sCod = sCod & CStr(ACN2) & Mid$("                         ", 1, 10 - Len(CStr(ACN2)))
        sCod = sCod & CStr(ACN3) & Mid$("                         ", 1, 10 - Len(CStr(ACN3)))
        sCod = sCod & CStr(ACN4) & Mid$("                         ", 1, 10 - Len(CStr(ACN4)))
        
        sCod = sCod & CStr(ID) & Mid$("                         ", 1, 10 - Len(CStr(ID)))
        
        mCol.Add objnewmember, sCod
    End If
      
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

Public Property Get Item(vntIndexKey As Variant) As CActividadNivel5
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()

        Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
    
    Next

   DevolverLosCodigos = Codigos
   
End Function






