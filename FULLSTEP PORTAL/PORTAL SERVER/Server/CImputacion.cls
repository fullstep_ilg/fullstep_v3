VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CImputacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion

Private m_sPRES5Id As String
Private m_sPRES5 As String
Private m_sPRES1 As String
Private m_sPRES2 As String
Private m_sPRES3 As String
Private m_sPRES4 As String
Private m_sCC1 As String
Private m_sCC3 As String
Private m_sCC2 As String
Private m_sCC4 As String
Private m_sCC As Variant
Private m_iModo As Integer
Private m_sPRESDen As String
Private m_sCCDen As String
Private m_vEmpresa As Variant
Private m_sCodContrato As String
Private m_sCodCenCoste As String
Private m_sCodConcat As String
Private m_sCenSM As String
Private m_dFecInicio As Date
Private m_dFecFin As Date
Private m_bCerrado As Boolean
Private m_dCambio As Double
Private m_sMoneda As String
Private m_vGestor As Variant

Public Property Let Moneda(ByVal Dato As String)
    m_sMoneda = Dato
End Property
Public Property Get Moneda() As String
    Moneda = m_sMoneda
End Property

Public Property Let FecInicio(ByVal Dato As Date)
    m_dFecInicio = Dato
End Property
Public Property Get FecInicio() As Date
    FecInicio = m_dFecInicio
End Property
Public Property Let FecFin(ByVal Dato As Date)
    m_dFecFin = Dato
End Property
Public Property Get FecFin() As Date
    FecFin = m_dFecFin
End Property
Public Property Let Cerrado(ByVal Dato As Boolean)
    m_bCerrado = Dato
End Property
Public Property Get Cerrado() As Boolean
    Cerrado = m_bCerrado
End Property

Public Property Let Modo(ByVal Dato As Integer)
    m_iModo = Dato
End Property
Public Property Get Modo() As Integer
    Modo = m_iModo
End Property

Public Property Let PresDen(ByVal Dato As String)
    m_sPRESDen = Dato
End Property
Public Property Get PresDen() As String
    PresDen = m_sPRESDen
End Property
Public Property Let CCDen(ByVal Dato As String)
    m_sCCDen = Dato
End Property
Public Property Get CCDen() As String
    CCDen = m_sCCDen
End Property

Public Property Let PRES5(ByVal Dato As String)
    m_sPRES5 = Dato
End Property
Public Property Get PRES5() As String
    PRES5 = m_sPRES5
End Property

Public Property Let PRES1(ByVal Dato As String)
    m_sPRES1 = Dato
End Property
Public Property Get PRES1() As String
    PRES1 = m_sPRES1
End Property

Public Property Let PRES2(ByVal Dato As String)
    m_sPRES2 = Dato
End Property
Public Property Get PRES2() As String
    PRES2 = m_sPRES2
End Property

Public Property Let Pres3(ByVal Dato As String)
    m_sPRES3 = Dato
End Property
Public Property Get Pres3() As String
    Pres3 = m_sPRES3
End Property

Public Property Let Pres4(ByVal Dato As String)
    m_sPRES4 = Dato
End Property
Public Property Get Pres4() As String
    Pres4 = m_sPRES4
End Property

Public Property Let CC1(ByVal Dato As String)
    m_sCC1 = Dato
End Property
Public Property Get CC1() As String
    CC1 = m_sCC1
End Property

Public Property Let CC2(ByVal Dato As String)
    m_sCC2 = Dato
End Property
Public Property Get CC2() As String
    CC2 = m_sCC2
End Property

Public Property Let CC3(ByVal Dato As String)
    m_sCC3 = Dato
End Property
Public Property Get CC3() As String
    CC3 = m_sCC3
End Property

Public Property Let CC4(ByVal Dato As String)
    m_sCC4 = Dato
End Property
Public Property Get CC4() As String
    CC4 = m_sCC4
End Property


Public Property Let CentroCoste(ByVal Dato As Variant)
    m_sCC = Dato
End Property
Public Property Get CentroCoste() As Variant
    CentroCoste = m_sCC
End Property

Public Property Let Empresa(ByVal Dato As Variant)
    m_vEmpresa = Dato
End Property
Public Property Get Empresa() As Variant
    Empresa = m_vEmpresa
End Property

Public Property Let CodContrato(ByVal Dato As String)
    m_sCodContrato = Dato
End Property
Public Property Get CodContrato() As String
    CodContrato = m_sCodContrato
End Property

Public Property Let CodCenCoste(ByVal Dato As String)
    m_sCodCenCoste = Dato
End Property
Public Property Get CodCenCoste() As String
    CodCenCoste = m_sCodCenCoste
End Property

Public Property Let CenSM(ByVal Dato As String)
    m_sCenSM = Dato
End Property
Public Property Get CenSM() As String
    CenSM = m_sCenSM
End Property

Public Property Let Cambio(ByVal Dato As Double)
    m_dCambio = Dato
End Property
Public Property Get Cambio() As Double
    Cambio = m_dCambio
End Property

Public Property Let CodConcat(ByVal Dato As String)
    m_sCodConcat = Dato
End Property
Public Property Get CodConcat() As String
    CodConcat = m_sCodConcat
End Property

Public Property Let Gestor(ByVal Dato As Variant)
    m_vGestor = Dato
End Property
Public Property Get Gestor() As Variant
    Gestor = m_vGestor
End Property

Public Property Let PRES5Id(ByVal Dato As String)
    m_sPRES5Id = Dato
End Property
Public Property Get PRES5Id() As String
    PRES5Id = m_sPRES5Id
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
End Property

Private Sub Class_Terminate()
    On Error GoTo Error_Cls:
    
    Set m_oConexion = Nothing
    Exit Sub
Error_Cls:
    Resume Next
End Sub


''' <summary>
''' Acumula el importe pasado en el comprometido de la partida, no hay transacci�n pq estar� en la funci�n que la llame
''' </summary>
''' <param name="MonPedido">Moneda del pedido</param>
''' <param name="CambioPedido">Cambio de la moneda del pedido</param>
''' <param name="Importe">Importe a sumar</param>
''' <param name="iRecep">Si se llama desde recepciones</param>
''' <param name="LineaRecep">Id de la linea de recepci�n</param>
''' <param name="CantPedida">Cantidad pedida</param>
''' <param name="IdSolicit">Id de la instancia vinculada al pedido</param>
''' <param name="IdCampoSolicit">Id de copia_campo de la linea vinculada al pedido</param>
''' <param name="TipoRecepcion">Si se recepciona por cantidad=0 o por importe=1</param>
''' <returns>Objeto error si se produce</returns>
''' <remarks>Llamada desde: Clinea.ActualizarDisponible</remarks>

Public Function ActualizarImportesPartida(ByVal CiaComp As Long, ByVal bImpRecep As Boolean, ByVal MonPedido As String, ByVal iComprAcum As Integer, ByVal sIdioma As String, Optional ByVal Importe As Double, _
                Optional ByVal iRecep As Integer, Optional ByVal LineaRecep As Long, Optional ByVal CantPedida As Double, Optional ByVal ImportePedido As Double, _
                Optional IdSolicit As Long, Optional IdCampoSolicit As Long, Optional ByVal LineaPedido As Integer, Optional tipoRecepcion As Integer) As CTESError
    Dim oGestParam As CGestorParametros
    Dim sConsulta As String
    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim adoRS As New ADODB.Recordset
    Dim oImp As CImputacion
    Dim oError As CTESError
    Dim dblComprometido As Double
    Dim sMonCen As String
    Dim sFSG As String
    
    On Error GoTo Error
    
    Set oError = New CTESError
    oError.NumError = TESnoerror
    
    sFSG = Fsgs(m_oConexion, CiaComp)
    
    Set oGestParam = New CGestorParametros
    Set oGestParam.Conexion = m_oConexion
    sMonCen = oGestParam.DevolverParametroDefGS(CiaComp, "MONCEN")
    
    If (iRecep = 0 And iComprAcum = 1) Or (iRecep <> 0 And iComprAcum = 2) Then
        'Obtenemos la moneda de la partida si no est� cargada.
        'Antes se hacia de manera similar para el caso de la emision y la recepcion.
        'Por falta de horas para la tarea no se contempla liberar solicitado cuando acumula comprometido en la recepcion
        'Ahora para la emision llamamos al stored FSSM_ACTUALIZAR_IMPORTES_PARTIDA que gestionara tb el liberar el solicitado
        Set adoComm = New ADODB.Command
        If iRecep > 1 Then
            sConsulta = "SELECT COMP,MON,EQUIV,PRES5_IMPORTES.ID,LRI.CAMBIO FROM " & sFSG & "PRES5_IMPORTES PI WITH (NOLOCK) LEFT JOIN " & sFSG & "MON WITH (NOLOCK) ON PI.MON=MON.COD  "
            If bImpRecep Then
                sConsulta = sConsulta & " INNER JOIN " & sFSG & "LINEAS_RECEP_IMPUTACION LRI WITH (NOLOCK) ON LRI.LINEA_RECEP=? AND LRI.PRES5_IMP=PI.ID "
            Else
                sConsulta = sConsulta & " INNER JOIN " & sFSG & "LINEAS_PED_IMPUTACION LRI WITH (NOLOCK) ON LRI.LINEA=? AND LRI.PRES5_IMP=PI.ID "
            End If
            sConsulta = sConsulta & " WHERE PI.PRES0=?"
            Set adoParam = adoComm.CreateParameter("LINEA_RECEP", adInteger, adParamInput, , LineaRecep)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("PRES5", adVarChar, adParamInput, Len(m_sPRES5), m_sPRES5)
            adoComm.Parameters.Append adoParam

            Set adoComm.ActiveConnection = m_oConexion.AdoCon
            adoComm.CommandText = sConsulta
            adoComm.CommandType = adCmdText
            Set adoRS = adoComm.Execute

            If Not adoRS.eof Then
                oError.Arg1 = adoRS.Fields("ID").Value 'Para la recepci�n devuelve el id de pres5_importes
                dblComprometido = NullToDbl0(adoRS.Fields("COMP").Value)
                If IsNull(adoRS.Fields("MON").Value) Then
                    m_sMoneda = sMonCen
                    m_dCambio = 1
                Else
                    m_sMoneda = adoRS.Fields("MON").Value
                    m_dCambio = NullToDbl0(adoRS.Fields("CAMBIO").Value)
                    If m_dCambio = 0 Then 'S�lo cargamos el cambio de moneda si no estaba ya cargado
                        m_dCambio = adoRS.Fields("EQUIV").Value
                    End If
                End If
            Else
                m_sMoneda = sMonCen
                m_dCambio = 1
                adoRS.Close
                Set adoRS = Nothing
                Set adoComm = Nothing
                oError.Arg1 = Null
                Set ActualizarImportesPartida = oError
                Exit Function
            End If
            adoRS.Close
            Set adoRS = Nothing
            Set adoComm = Nothing

            'El importe llega en moneda central. En pantalla Recepciones/Seguimiento se ve en la moneda correspondiente pero llega
            'como en bbdd moneda central.
            'Los cambios son respecto moneda central. Luego si llega 250, son 250� y si la moneda comprometido es ADP(cambio 2), se graba 500ADP.
            dblComprometido = dblComprometido + CDec(Importe * m_dCambio)

            If Importe <> 0 And iRecep > 0 Then
                Set adoComm = New ADODB.Command

                sConsulta = "UPDATE " & sFSG & "PRES5_IMPORTES SET COMP=? WHERE ID=? "
                Set adoParam = adoComm.CreateParameter("COMP", adDouble, adParamInput, , dblComprometido)
                adoComm.Parameters.Append adoParam
                Set adoParam = adoComm.CreateParameter("ID", adInteger, adParamInput, , oError.Arg1)
                adoComm.Parameters.Append adoParam

                Set adoComm.ActiveConnection = m_oConexion.AdoCon
                adoComm.CommandText = sConsulta
                adoComm.CommandType = adCmdText

                adoComm.Execute
                If m_oConexion.AdoCon.Errors.Count > 0 Then GoTo Error
                Set adoComm = Nothing
                Set adoParam = Nothing
            End If
        Else
            Dim rs As ADODB.Recordset
            'Ejecutamos el stored FSSM_ACTUALIZAR_IMPORTES_PARTIDA que acumulara el comprometido y liberara el solicitado
            Set adoComm = New ADODB.Command
            adoComm.CommandType = adCmdStoredProc
            adoComm.CommandText = sFSG & "FSSM_ACTUALIZAR_IMPORTES_PARTIDA"
            Set adoComm.ActiveConnection = m_oConexion.AdoCon

            Set adoParam = adoComm.CreateParameter("IDIOMA", adVarWChar, adParamInput, 10, sIdioma) 'El numero de la linea de la solicitud de compra
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("PRES0", adVarWChar, adParamInput, 100, StrToVbNULL(m_sPRES5))
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("PRES1", adVarWChar, adParamInput, 100, StrToVbNULL(m_sPRES1))
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("PRES2", adVarWChar, adParamInput, 100, StrToVbNULL(m_sPRES2))
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("PRES3", adVarWChar, adParamInput, 100, StrToVbNULL(m_sPRES3))
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("PRES4", adVarWChar, adParamInput, 100, StrToVbNULL(m_sPRES4))
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("TIENECONTROLDISPONIBLE", adInteger, adParamInput, , 0)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("INSTANCIA", adInteger, adParamInput, , IdSolicit) 'El id de la instancia de la solicitud de compra
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("MONEDA", adVarWChar, adParamInput, 10, sMonCen)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("CANT_PEDIDA", adDouble, adParamInput, , IIf(tipoRecepcion = 0 And Not CantPedida = 0 And Not IdSolicit = 0, CantPedida, Null))
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("IMPORTE_PEDIDO", adDouble, adParamInput, , IIf(tipoRecepcion = 1 And Not ImportePedido = 0 And Not IdSolicit = 0, ImportePedido, Null))  'El importe que pasaremos esta en moneda central
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("IDCAMPOSOLICIT", adInteger, adParamInput, , IdCampoSolicit) 'El id de copia_campo de la linea de la solicitud de compra
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("SOLICITADO", adDouble, adParamInput, , Null)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("LINEAPEDIDO", adInteger, adParamInput, , LineaPedido)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("COMPROMETIDO", adDouble, adParamInput, , IIf(Importe = 0, Null, Importe)) 'El importe comprometido que pasaremos esta en moneda central
            adoComm.Parameters.Append adoParam
            
            'Definimos las variables que obtendremos como output
            Set adoParam = adoComm.CreateParameter("OK", adInteger, adParamInputOutput, , 0)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("ID_PRES5_IMPORTES", adInteger, adParamOutput)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("CODPARTIDA", adVarWChar, adParamOutput, 100, Null)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("DENPARTIDA", adVarWChar, adParamOutput, 300, Null)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("PRESUPUESTADO", adDouble, adParamOutput, , Null)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("DISPONIBLE", adDouble, adParamOutput, , Null)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("EQUIV", adDouble, adParamOutput, , Null)
            adoComm.Parameters.Append adoParam
            Set adoParam = adoComm.CreateParameter("MON", adVarWChar, adParamOutput, 10, Null)
            adoComm.Parameters.Append adoParam
                        
            adoComm.Execute
            If m_oConexion.AdoCon.Errors.Count > 0 Then GoTo Error
            oError.Arg1 = adoComm.Parameters("MON").Value
        End If
    End If
    
Salir:
    Set oGestParam = Nothing
    Set ActualizarImportesPartida = oError
    Exit Function
Error:
    If m_oConexion.AdoCon.Errors.Count > 0 Then
        Set oError = basErrores.TratarError(m_oConexion.AdoCon.Errors)
    Else
        oError.NumError = TESOtroErrorNOBD
    End If
    m_oConexion.GrabarError CiaComp, "CImputacion.ActualizarImportesPartida", Err.Number, Err.Description
    Resume Salir
End Function
