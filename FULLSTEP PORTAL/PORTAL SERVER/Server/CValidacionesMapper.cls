VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CValidacionesMapper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private moConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
Set moConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property


Private Sub Class_Terminate()
    Set moConexion = Nothing
End Sub

''' <summary>
''' Obtiene el nombre del componente de la mapper (FSGSMapper.dll)
''' </summary>
''' <param name=""></param>
''' <returns>Nombre de la mapper, obtenido a partir del primer registro de la tabla ERP</returns>
''' <remarks>Llamada desde: actividadesCia.asp; Tiempo m�ximo: 0,1</remarks>
Public Function ReadMapper() As Variant
Dim adoComm As ADODB.Command
Dim adoRes As ADODB.Recordset
Dim sConsulta As String
Dim sFSG As String
On Error GoTo Error

Dim sNomMapper As String



sFSG = FSGS(moConexion, 1)
Set adoComm = New ADODB.Command
Set adoComm.ActiveConnection = moConexion.AdoCon

adoComm.CommandText = "SELECT TOP 1 MAPPER FROM " & sFSG & " ERP WITH(NOLOCK) ORDER BY COD "
Set adoRes = adoComm.Execute

ReadMapper = adoRes("MAPPER").Value

adoRes.Close
Set adoRes = Nothing

Set adoComm = Nothing

fin:

Exit Function
Error:
Resume fin
Resume 0

End Function

''' <summary>
''' Obtiene el texto con el error a mostrar, de la tabla TEXTOS_GS (con m�dulo=5: ValidacionesPortal)
''' </summary>
''' <param name="Idioma">Idioma en que se mostrar� el mensaje</param>
''' <param name="iNUmError">Id del error a mostrar (TEXTOS_GS.ID)</param>
''' <returns>Mensaje de error</returns>
''' <remarks>Llamada desde: actividadesCia.asp; Tiempo m�ximo: 0,1</remarks>
Public Function DevolverTexto(ByVal Idioma As String, ByVal iNumError As Integer) As Variant
Dim adoComm As ADODB.Command
Dim adoRes As ADODB.Recordset
Dim sConsulta As String
Dim sFSG As String
On Error GoTo Error

Dim sTexto As String



sFSG = FSGS(moConexion, 1)
Set adoComm = New ADODB.Command
Set adoComm.ActiveConnection = moConexion.AdoCon

adoComm.CommandText = "SELECT TEXT_" & Idioma & " AS TEXTO FROM " & sFSG & " TEXTOS_GS WITH(NOLOCK) WHERE MODULO=5 AND ID= " & iNumError
Set adoRes = adoComm.Execute

DevolverTexto = adoRes("TEXTO").Value

adoRes.Close
Set adoRes = Nothing

Set adoComm = Nothing

fin:

Exit Function
Error:
Resume fin
Resume 0

End Function
