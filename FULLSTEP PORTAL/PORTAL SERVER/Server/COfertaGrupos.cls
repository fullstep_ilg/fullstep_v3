VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COfertaGrupos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* COfertaGrupos *****************************
'*             Autor : Hilario Barrenkua
'*             Creada : 15/05/2002
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection

Public Function Add(ByVal oOfe As COferta, ByVal oGrupo As CGrupo, Optional ByVal vObsAdjun As Variant, _
                    Optional ByVal vIndice As Variant) As COfertaGrupo



    'create a new object
    Dim objnewmember As COfertaGrupo
    
    Set objnewmember = New COfertaGrupo
   
    With objnewmember
        Set .Oferta = oOfe
        Set .Grupo = oGrupo
        Set .Conexion = oOfe.Conexion
        If Not IsMissing(vObsAdjun) Then
            .ObsAdjuntos = vObsAdjun
        Else
            .ObsAdjuntos = Null
        End If
        
        If IsMissing(vIndice) Then
            .Indice = mCol.Count
            mCol.Add objnewmember, CStr(oGrupo.Codigo)
            
        Else
            .Indice = vIndice
            mCol.Add objnewmember, CStr(.Indice)
        End If
        
    
    End With
    
        
      
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing
        
    
    
End Function

Public Property Get Item(vntIndexKey As Variant) As COfertaGrupo
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
On Error GoTo NoSeEncuentra:

    mCol.Remove vntIndexKey
    Exit Sub

NoSeEncuentra:
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
End Sub


