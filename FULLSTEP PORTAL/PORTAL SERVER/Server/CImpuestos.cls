VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CImpuestos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mCol As Collection
Private mvarConexion As CConexion

''' <summary>
''' Establecer el valor de mvarConexion
''' </summary>
''' <param name="con">Conexion</param>
''' <remarks>Llamada desde: CRaiz/generar_CImpuestos; Tiempo m�ximo: 0</remarks>
Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

''' <summary>
''' Obtener el valor de mvarConexion
''' </summary>
''' <remarks>Llamada desde: frmImpuestos; Tiempo m�ximo: 0</remarks>
Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

''' <summary>
''' Recuperar un Impuesto de la coleccion
''' </summary>
''' <param name="vntIndexKey">Indice del Impuesto a recuperar</param>
''' <returns>Unidad correspondiente</returns>
''' <remarks>Llamada desde: frmImpuestos/CargarGrid; Tiempo m�ximo: 0</remarks>
Public Property Get Item(vntIndexKey As Variant) As CImpuesto
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
       Set NewEnum = mCol.[_NewEnum]
End Property

''' <summary>
''' Eliminar un Impuesto de la coleccion
''' </summary>
''' <param name="vntIndexKey">Indice del Impuesto a eliminar</param>
''' <remarks>Llamada desde: frmImpuestos  ; Tiempo m�ximo: 0</remarks>
Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub

''' <summary>
''' Devolver Numero de elementos de la coleccion
''' </summary>
''' <returns>Numero de elementos de la coleccion</returns>
''' <remarks>Llamada desde: frmImpuestos ; Tiempo m�ximo: 0</remarks>
Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
End Property

''' <summary>
''' creates the collection when this class is created
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

''' <summary>
''' destroys collection when this class is terminated
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Class_Terminate()
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

''' <summary>A�ade un nuevo item a la colecci�n</summary>
''' <param name="IDMat">ID del Impuesto</param>
''' <param name="CodImpuesto">Codigo del Impuesto</param>
''' <param name="Descripcion">Descripcion del Impuesto</param>
''' <param name="IDImpuesto">ID del Impuesto</param>
''' <param name="IDValor">ID del Impuesto</param>
''' <param name="GMN1Cod">GMN1 del Impuesto</param>
''' <param name="GMN2Cod">GMN2 del Impuesto</param>
''' <param name="GMN3Cod">GMN3 del Impuesto</param>
''' <param name="GMN4Cod">GMN4 del Impuesto</param>
''' <param name="Art">Art�culo del Impuesto</param>
''' <param name="Valor">Valor del Impuesto</param>
''' <param name="Vigente">Vigencia del Impuesto</param>
''' <param name="IDPaisProvincia">Pais/Provincia del Impuesto</param>
''' <param name="Heredado">Si el Impuesto es heredado del padre</param>
''' <param name="Retenido">Si el Impuesto es Retenido</param>
''' <param name="CodPais">Cod Pais del Impuesto</param>
''' <param name="CodProvincia">Cod Provincia del Impuesto</param>
''' <param name="DenPais">Cod Pais del Impuesto</param>
''' <param name="DenProvincia">Cod Provincia del Impuesto</param>
''' <param name="FecAct">Fecha ultima actualizacion del Impuesto</param>
''' <returns>Objeto de tipo CImpuesto a�adido</returns>
''' <remarks>Llamada desde: CargarTodosLosImpuestos ; Tiempo m�ximo: 0</remarks>
Public Function Add(ByVal IDMat As Variant, ByVal CodImpuesto As Variant, ByVal Descripcion As Variant, Optional ByVal IDImpuesto As Variant _
, Optional ByVal IDValor As Variant, Optional ByVal GMN1Cod As Variant, Optional ByVal GMN2Cod As Variant, Optional ByVal GMN3Cod As Variant _
, Optional ByVal GMN4Cod As Variant, Optional ByVal Art As Variant, Optional ByVal Valor As Variant, Optional ByVal Vigente As Variant _
, Optional ByVal IDPaisProvincia As Variant, Optional ByVal Heredado As Variant, Optional ByVal Retenido As Variant _
, Optional ByVal CodPais As Variant, Optional ByVal CodProvincia As Variant, Optional ByVal DenPais As Variant _
, Optional ByVal DenProvincia As Variant, Optional ByVal FecAct As Variant, Optional ByVal Tipo As Variant, Optional ByVal AtribID As Variant, Optional ByVal NivelCategoria As Variant, Optional ByVal Concepto As Variant, Optional ByVal GrupoCompatibilidad As Variant) As CImpuesto
    Dim objnewmember As CImpuesto
    
    Set objnewmember = New CImpuesto
    Set objnewmember.Conexion = mvarConexion
    
    objnewmember.IDImpuesto = IDImpuesto
    objnewmember.IDMat = IDMat
    objnewmember.IDValor = IDValor
            
    If Not IsMissing(CodImpuesto) Then
        objnewmember.CodImpuesto = CodImpuesto
    Else
        objnewmember.CodImpuesto = Null
    End If
    
    If Not IsMissing(Descripcion) Then
        objnewmember.Descripcion = Descripcion
    Else
        objnewmember.Descripcion = Null
    End If
    
    If Not IsMissing(Tipo) Then
        objnewmember.Tipo = Tipo
    Else
        objnewmember.Tipo = Null
    End If
    
    If Not IsMissing(Concepto) Then
        objnewmember.Concepto = Concepto
    Else
        objnewmember.Concepto = Null
    End If
    
    If Not IsMissing(GrupoCompatibilidad) Then
        objnewmember.GrupoCompatibilidad = GrupoCompatibilidad
    Else
        objnewmember.GrupoCompatibilidad = Null
    End If

    
    If Not IsMissing(AtribID) Then
        objnewmember.AtribID = AtribID
    Else
        objnewmember.AtribID = Null
    End If
    If Not IsMissing(NivelCategoria) Then
        objnewmember.NivelCategoria = NivelCategoria
    Else
        objnewmember.NivelCategoria = Null
    End If
    
    If Not IsMissing(GMN1Cod) Then
        objnewmember.GMN1Cod = GMN1Cod
    Else
        objnewmember.GMN1Cod = Null
    End If
    
    If Not IsMissing(GMN2Cod) Then
        objnewmember.GMN2Cod = GMN2Cod
    Else
        objnewmember.GMN2Cod = Null
    End If
    
    If Not IsMissing(GMN3Cod) Then
        objnewmember.GMN3Cod = GMN3Cod
    Else
        objnewmember.GMN3Cod = Null
    End If
    
    If Not IsMissing(GMN4Cod) Then
        objnewmember.GMN4Cod = GMN4Cod
    Else
        objnewmember.GMN4Cod = Null
    End If
    
    If Not IsMissing(Art) Then
        objnewmember.Art = Art
    Else
        objnewmember.Art = Null
    End If
    
    If Not IsMissing(Valor) Then
        objnewmember.Valor = Valor
    Else
        objnewmember.Valor = Null
    End If
    
    If Not IsMissing(Vigente) Then
        objnewmember.Vigencia = Vigente
    Else
        objnewmember.Vigencia = 1
    End If
    
    If Not IsMissing(IDPaisProvincia) Then
        objnewmember.ImpuestoPaisProvincia = IDPaisProvincia
    Else
        objnewmember.ImpuestoPaisProvincia = Null
    End If
    
    If Not IsMissing(CodPais) Then
        objnewmember.CodPais = CodPais
    Else
        objnewmember.CodPais = Null
    End If
    
    If Not IsMissing(CodProvincia) Then
        objnewmember.CodProvincia = CodProvincia
    Else
        objnewmember.CodProvincia = Null
    End If
    
    If Not IsMissing(DenPais) Then
        objnewmember.DenPais = DenPais
    Else
        objnewmember.DenPais = Null
    End If
    
    If Not IsMissing(DenProvincia) Then
        objnewmember.DenProvincia = DenProvincia
    Else
        objnewmember.DenProvincia = Null
    End If
    
    If Not IsMissing(Heredado) Then
        objnewmember.Heredado = Heredado
    Else
        objnewmember.Heredado = 0
    End If
    
    If Not IsMissing(Retenido) Then
        objnewmember.Retenido = Retenido
    Else
        objnewmember.Retenido = 0
    End If
    
    If IsMissing(FecAct) Then
        objnewmember.FecAct = Null
    Else
        objnewmember.FecAct = FecAct
    End If
    
    If Not Existe(CStr(IDMat)) Then
        mCol.Add objnewmember, CStr(IDMat)
    Else
        mCol.Add objnewmember, CStr(IDMat) & objnewmember.CodImpuesto & objnewmember.Valor
    End If
    
    Set Add = objnewmember
    
    Set objnewmember = Nothing
End Function

Public Function Existe(ByVal id As Variant) As Boolean
    Dim o As CImpuesto
    On Error GoTo Err
    '
    Set o = mCol(CStr(id))
    
    Existe = True
    Exit Function
Err:
    Existe = False
End Function

Public Sub CargarImpuestoLineaDesglose(ByVal Linea As Long, ByVal Idioma As String, ByVal CiaComp As Long)
    Dim rs As New ADODB.Recordset
    Dim sConsulta As String
    
    Dim fldImpuesto As ADODB.Field
    Dim fldCod As ADODB.Field
    Dim fldDen As ADODB.Field
    Dim fldValor As ADODB.Field
    Dim fldIdValor As ADODB.Field
    Dim sFSG As String
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CImpuestos.CargarImpuestoLineaDesglose", "No se ha establecido la conexion"
        Exit Sub
    End If

    sFSG = Fsgs(mvarConexion, CiaComp)
    sConsulta = "SELECT I.ID IMPUESTO,I.COD,IDEN.DEN,LPI.VALOR,LPI.IMPUESTO IMPPP_VALOR"
    sConsulta = sConsulta & " FROM " & sFSG & " LINEAS_PEDIDO_IMPUESTO LPI WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN " & sFSG & " IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK) ON IMPPP.ID= LPI.IMPUESTO"
    sConsulta = sConsulta & " INNER JOIN " & sFSG & " IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPP.IMPUESTO_PAIPROVI = IMPPAI.ID"
    sConsulta = sConsulta & " INNER JOIN " & sFSG & " IMPUESTO I WITH(NOLOCK) ON IMPPAI.IMPUESTO = I.ID"
    sConsulta = sConsulta & " INNER JOIN " & sFSG & " IMPUESTO_DEN IDEN WITH(NOLOCK) ON IDEN.IMPUESTO = I.ID"
    sConsulta = sConsulta & " WHERE LPI.LINEA=" & Linea
    sConsulta = sConsulta & " AND IDEN.IDIOMA=" & StrToSQLNULL(Idioma)
    
    rs.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

    If rs.eof Then

        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub

    Else
        Set mCol = Nothing
        Set mCol = New Collection

        Set fldImpuesto = rs.Fields("IMPUESTO")
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        Set fldValor = rs.Fields("VALOR")
        Set fldIdValor = rs.Fields("IMPPP_VALOR")

        While Not rs.eof
            Add fldImpuesto.Value, fldCod.Value, fldDen.Value, fldImpuesto.Value, fldIdValor.Value, , , , , , fldValor.Value
            rs.MoveNext
        Wend

        rs.Close
        Set rs = Nothing
        Set fldImpuesto = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldValor = Nothing
        Set fldIdValor = Nothing

    End If
End Sub

Public Sub CargarImpuestoCostesLineaDesglose(ByVal Linea As Long, ByVal Idioma As String, ByVal CiaComp As Long, ByVal Coste As Long)
    Dim rs As New ADODB.Recordset
    Dim sConsulta As String
    
    Dim fldImpuesto As ADODB.Field
    Dim fldCod As ADODB.Field
    Dim fldDen As ADODB.Field
    Dim fldValor As ADODB.Field
    Dim fldIdValor As ADODB.Field
    Dim sFSG As String
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CImpuestos.CargarImpuestoCostesLineaDesglose", "No se ha establecido la conexion"
        Exit Sub
    End If

    sFSG = Fsgs(mvarConexion, CiaComp)
    sConsulta = "SELECT I.ID IMPUESTO,I.COD,IDEN.DEN,LPCDI.VALOR,LPCDI.IMPUESTO IMPPP_VALOR"
    sConsulta = sConsulta & " FROM " & sFSG & " LINEAS_PEDIDO_CD LPCD WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN " & sFSG & " LINEAS_PEDIDO_CD_IMPUESTO LPCDI WITH(NOLOCK) ON LPCDI.COSTE=LPCD.ID"
    sConsulta = sConsulta & " INNER JOIN " & sFSG & " IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK) ON IMPPP.ID= LPCDI.IMPUESTO"
    sConsulta = sConsulta & " INNER JOIN " & sFSG & " IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPP.IMPUESTO_PAIPROVI = IMPPAI.ID"
    sConsulta = sConsulta & " INNER JOIN " & sFSG & " IMPUESTO I WITH(NOLOCK) ON IMPPAI.IMPUESTO = I.ID"
    sConsulta = sConsulta & " INNER JOIN " & sFSG & " IMPUESTO_DEN IDEN WITH(NOLOCK) ON IDEN.IMPUESTO = I.ID"
        
    sConsulta = sConsulta & " WHERE LPCDI.LINEA=" & Linea
    sConsulta = sConsulta & " AND IDEN.IDIOMA=" & StrToSQLNULL(Idioma)
    sConsulta = sConsulta & " AND LPCD.ATRIB=" & Coste
    
    rs.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

    If rs.eof Then

        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub

    Else
        Set mCol = Nothing
        Set mCol = New Collection

        Set fldImpuesto = rs.Fields("IMPUESTO")
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        Set fldValor = rs.Fields("VALOR")
        Set fldIdValor = rs.Fields("IMPPP_VALOR")

        While Not rs.eof
            Add fldImpuesto.Value, fldCod.Value, fldDen.Value, fldImpuesto.Value, fldIdValor.Value, , , , , , fldValor.Value
            rs.MoveNext
        Wend

        rs.Close
        Set rs = Nothing
        Set fldImpuesto = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldValor = Nothing
        Set fldIdValor = Nothing

    End If
End Sub

Public Sub CargarImpuestoCostesCabeceraDesglose(ByVal Orden As Long, ByVal Idioma As String, ByVal CiaComp As Long, ByVal Coste As Long)
    Dim rs As New ADODB.Recordset
    Dim sConsulta As String
    
    Dim fldImpuesto As ADODB.Field
    Dim fldCod As ADODB.Field
    Dim fldDen As ADODB.Field
    Dim fldValor As ADODB.Field
    Dim fldIdValor As ADODB.Field
    Dim sFSG As String
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CImpuestos.CargarImpuestoCostesCabeceraDesglose", "No se ha establecido la conexion"
        Exit Sub
    End If
   
    sFSG = Fsgs(mvarConexion, CiaComp)
    sConsulta = "SELECT I.ID IMPUESTO,I.COD,IDEN.DEN,OECDI.VALOR,OECDI.IMPUESTO IMPPP_VALOR"
    sConsulta = sConsulta & " FROM " & sFSG & " ORDEN_ENTREGA_CD OECD WITH(NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN " & sFSG & " ORDEN_ENTREGA_CD_IMPUESTO OECDI WITH(NOLOCK) ON OECDI.COSTE =OECD.ID AND OECDI.ORDEN=OECD.ORDEN "
    sConsulta = sConsulta & " INNER JOIN " & sFSG & " IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK) ON IMPPP.ID= OECDI.IMPUESTO"
    sConsulta = sConsulta & " INNER JOIN " & sFSG & " IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPP.IMPUESTO_PAIPROVI = IMPPAI.ID"
    sConsulta = sConsulta & " INNER JOIN " & sFSG & " IMPUESTO I WITH(NOLOCK) ON IMPPAI.IMPUESTO = I.ID"
    sConsulta = sConsulta & " INNER JOIN " & sFSG & " IMPUESTO_DEN IDEN WITH(NOLOCK) ON IDEN.IMPUESTO = I.ID"
    sConsulta = sConsulta & " WHERE OECDI.ORDEN=" & Orden
    sConsulta = sConsulta & " AND IDEN.IDIOMA=" & StrToSQLNULL(Idioma)
    sConsulta = sConsulta & " AND OECD.ATRIB=" & Coste
    
    rs.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
       
    If rs.eof Then

        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub

    Else
        Set mCol = Nothing
        Set mCol = New Collection

        Set fldImpuesto = rs.Fields("IMPUESTO")
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        Set fldValor = rs.Fields("VALOR")
        Set fldIdValor = rs.Fields("IMPPP_VALOR")

        While Not rs.eof
            Me.Add fldImpuesto.Value, fldCod.Value, fldDen.Value, fldImpuesto.Value, fldIdValor.Value, , , , , , fldValor.Value
            rs.MoveNext
        Wend

        rs.Close
        Set rs = Nothing
        Set fldImpuesto = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldValor = Nothing
        Set fldIdValor = Nothing

    End If
End Sub
