VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CProveedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mvarIndice As Variant 'Nos indica la posicion secuencial dentro de una Coleccion
Private mvarCod As String 'local copy
Private mvarDen As String 'local copy
Private mvarNIF As Variant
Private mvarDireccion As Variant
Private mvarPoblacion As Variant
Private mvarCP As Variant
Private mvarCodMon As Variant
Private mvarCodPais As Variant
Private mvarCodProvi As Variant
Private mvarDenMon As Variant
Private mvarDenPais As Variant
Private mvarDenProvi As Variant
Private mvarId As Variant
Private mvarCodFullStep As Variant
Private mvarEstado As CIAS_PORT_FPEST
Private mvarPremium As Variant
Private mvarActividadesNivel1 As CActividadesNivel1
Private mvarActividadesNivel2 As CActividadesNivel2
Private mvarActividadesNivel3 As CActividadesNivel3
Private mvarActividadesNivel4 As CActividadesNivel4
Private mvarActividadesNivel5 As CActividadesNivel5
Private m_vCiaMaxAdjun As Variant
Private m_varBajaCalidad As Variant
Private m_varPanelCalidad As Variant
Private mvarAutofactura As Variant
'*******************************************************

Private mvarConexion As CConexion 'local copy
Public Property Get oActividadesNIvel1() As CActividadesNivel1
    Set oActividadesNIvel1 = mvarActividadesNivel1
End Property
Public Property Set oActividadesNIvel1(ByVal oActs As CActividadesNivel1)
    Set mvarActividadesNivel1 = oActs
End Property
Public Property Get oActividadesNIvel2() As CActividadesNivel2
    Set oActividadesNIvel2 = mvarActividadesNivel2
End Property
Public Property Set oActividadesNIvel2(ByVal oActs As CActividadesNivel2)
    Set mvarActividadesNivel2 = oActs
End Property
Public Property Get oActividadesNIvel3() As CActividadesNivel3
    Set oActividadesNIvel3 = mvarActividadesNivel3
End Property
Public Property Set oActividadesNIvel3(ByVal oActs As CActividadesNivel3)
    Set mvarActividadesNivel3 = oActs
End Property
Public Property Get oActividadesNIvel4() As CActividadesNivel4
    Set oActividadesNIvel4 = mvarActividadesNivel4
End Property
Public Property Set oActividadesNIvel4(ByVal oActs As CActividadesNivel4)
    Set mvarActividadesNivel4 = oActs
End Property
Public Property Get oActividadesNIvel5() As CActividadesNivel5
    Set oActividadesNIvel5 = mvarActividadesNivel5
End Property
Public Property Set oActividadesNIvel5(ByVal oActs As CActividadesNivel5)
    Set mvarActividadesNivel5 = oActs
End Property
Public Property Let Den(ByVal Den As String)
    mvarDen = Den
End Property
Public Property Get Den() As String
    Den = mvarDen
End Property
Public Property Get CP() As Variant
    CP = mvarCP
End Property
Public Property Let CP(ByVal Cod As Variant)
    mvarCP = Cod
End Property
Public Property Get CodMon() As Variant
    CodMon = mvarCodMon
End Property
Public Property Let CodMon(ByVal Cod As Variant)
    mvarCodMon = Cod
End Property
Public Property Get CodPais() As Variant
    CodPais = mvarCodPais
End Property
Public Property Let CodPais(ByVal Cod As Variant)
    mvarCodPais = Cod
End Property
Public Property Get CodProvi() As Variant
    CodProvi = mvarCodProvi
End Property
Public Property Let CodProvi(ByVal Cod As Variant)
    mvarCodProvi = Cod
End Property
Public Property Get DenProvi() As Variant
    DenProvi = mvarDenProvi
End Property
Public Property Let DenProvi(ByVal Cod As Variant)
    mvarDenProvi = Cod
End Property
Public Property Get DenPais() As Variant
    DenPais = mvarDenPais
End Property
Public Property Let DenPais(ByVal Cod As Variant)
    mvarDenPais = Cod
End Property
Public Property Get DenMon() As Variant
    DenMon = mvarDenMon
End Property
Public Property Let DenMon(ByVal Cod As Variant)
    mvarDenMon = Cod
End Property
Public Property Get Direccion() As Variant
    Direccion = mvarDireccion
End Property
Public Property Let Direccion(ByVal Cod As Variant)
    mvarDireccion = Cod
End Property
Public Property Get Poblacion() As Variant
    Poblacion = mvarPoblacion
End Property
Public Property Let Poblacion(ByVal Cod As Variant)
    mvarPoblacion = Cod
End Property
Public Property Get Cod() As String
    Cod = mvarCod
End Property
Public Property Let Cod(ByVal Cod As String)
    mvarCod = Cod
End Property

Public Property Get CodFullStep() As Variant
    CodFullStep = mvarCodFullStep
End Property
Public Property Let CodFullStep(ByVal Cod As Variant)
    mvarCodFullStep = Cod
End Property

Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal varInd As Variant)
    mvarIndice = varInd
End Property

Public Property Get Nif() As Variant
    Nif = mvarNIF
End Property
Public Property Let Nif(ByVal varNIF As Variant)
    mvarNIF = varNIF
End Property

Public Property Get Premium() As Variant
    Premium = mvarPremium
End Property
Public Property Let Premium(ByVal varPremium As Variant)
    mvarPremium = varPremium
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Public Property Get id() As Long
    id = mvarId
End Property
Public Property Let id(ByVal Data As Long)
    mvarId = Data
End Property
Public Property Get Estado() As CIAS_PORT_FPEST
    Estado = mvarEstado
End Property
Public Property Let Estado(ByVal Data As CIAS_PORT_FPEST)
    mvarEstado = Data
End Property
Public Property Get CiaMaxAdjun() As Variant
    CiaMaxAdjun = m_vCiaMaxAdjun
End Property
Public Property Let CiaMaxAdjun(ByVal Data As Variant)
    m_vCiaMaxAdjun = Data
End Property

Public Property Get BajaCalidad() As Variant
    BajaCalidad = m_varBajaCalidad
End Property
Public Property Let BajaCalidad(ByVal Data As Variant)
    m_varBajaCalidad = Data
End Property

Public Property Get PanelCalidad() As Variant
    PanelCalidad = m_varPanelCalidad
End Property
Public Property Let PanelCalidad(ByVal Data As Variant)
    m_varPanelCalidad = Data
End Property

Public Property Get Autofactura() As Variant
    Autofactura = mvarAutofactura
End Property
Public Property Let Autofactura(ByVal Data As Variant)
    mvarAutofactura = Data
End Property

Private Sub Class_Terminate()
    
    Set mvarConexion = Nothing
        
    
End Sub

