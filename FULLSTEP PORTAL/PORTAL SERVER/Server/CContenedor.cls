VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CContenedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


Private mvarCod As Variant
Private mvarDen As Variant
Private mvarObjeto As Object
Private mvarIndice As Long


Public Property Get Indice() As Integer

    ''' * Objetivo: Devolver el indice de la moneda en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice de la moneda en la coleccion

    Indice = mvarIndice
    
End Property
Public Property Let Indice(ByVal iInd As Integer)

    ''' * Objetivo: Dar valor al indice de la moneda en la coleccion
    ''' * Recibe: Indice de la moneda en la coleccion
    ''' * Devuelve: Nada

    mvarIndice = iInd
    
End Property

Public Property Let Cod(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo de la moneda
    ''' * Devuelve: Nada

    mvarCod = vData
    
End Property
Public Property Get Cod() As String

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo de la moneda

    Cod = mvarCod
    
End Property

Public Property Let Den(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo de la moneda
    ''' * Devuelve: Nada

    mvarDen = vData
    
End Property
Public Property Get Den() As String

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo de la moneda

    Den = mvarDen
    
End Property

Public Property Set Objeto(ByVal vData As Object)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo de la moneda
    ''' * Devuelve: Nada

    Set mvarObjeto = vData
    
End Property
Public Property Get Objeto() As Object

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo de la moneda

    Objeto = mvarObjeto
    
End Property


