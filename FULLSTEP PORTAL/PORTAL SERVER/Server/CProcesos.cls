VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CProcesos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CProcesos **********************************
'*             Autor : Javier Arana
'*             Creada : 8/2/98
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEof As Boolean
Private mvarSubasta As Integer
Private mvarNormales As Integer

Public Property Get Eof() As Boolean
    Eof = mvarEof
End Property
Friend Property Let Eof(ByVal b As Boolean)
    mvarEof = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CProceso
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property




Public Function Add(ByVal Anyo As Integer, ByVal Cod As Long, ByVal Gmn1 As String, _
Optional ByVal Den As String, Optional ByVal PermAdjDir As Boolean, _
Optional ByVal FecApe As Variant, Optional ByVal GMN2 As Variant, Optional ByVal GMN3 As Variant, _
Optional ByVal GMN4 As Variant, _
Optional ByVal Est As Integer, Optional ByVal PresGlob As Double, _
Optional ByVal FecNec As Date, Optional ByVal FecPres As Variant, _
Optional ByVal FecEnvPet As Variant, Optional ByVal FecLimOfe As Variant, _
Optional ByVal FecProxReu As Variant, Optional ByVal FecUltReu As Variant, _
Optional ByVal varIndice As Variant, Optional ByVal Ofertado As Boolean, _
Optional ByVal Mon As String, Optional ByVal Cambio As Double, _
Optional ByVal HoraEnReunion As Variant, Optional ByVal oResp As Object, _
Optional ByVal Reudec As Boolean, _
Optional ByVal FecReape As Variant, Optional ByVal Solicitud As Variant, _
Optional ByVal Subasta As Boolean, Optional ByVal VerProv As Boolean, _
Optional ByVal VerPuja As Boolean, Optional ByVal Superada As Boolean, _
Optional ByVal ObjNuevos As Boolean, Optional ByVal ObjCont As Integer, _
Optional ByVal NoOfe As Boolean, Optional ByVal FecNoOfe As Variant, _
Optional ByVal MotivoNoOfe As Variant, Optional ByVal Com As Variant, _
Optional ByVal Nom As Variant, Optional ByVal Ape As Variant, Optional ByVal objPublicados As Boolean) As CProceso
    
    'create a new object
    Dim sCod As String
    Dim objnewmember As CProceso
    Set objnewmember = New CProceso
   
    objnewmember.Anyo = Anyo
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    objnewmember.Ofertado = Ofertado ' Solo tiene sentido en el caso de cargar los procesos de un proveedor con accesoWEB.
    objnewmember.Cambio = Cambio
    objnewmember.Reudec = Reudec
    Set objnewmember.Conexion = mvarConexion
    
    
    If IsMissing(Gmn1) Then
        objnewmember.GMN1Cod = Null
    Else
        objnewmember.GMN1Cod = Gmn1
    End If
    If IsMissing(GMN2) Then
        objnewmember.GMN2Cod = Null
    Else
        objnewmember.GMN2Cod = GMN2
    End If
    If IsMissing(GMN3) Then
        objnewmember.GMN3Cod = Null
    Else
        objnewmember.GMN3Cod = GMN3
    End If
    If IsMissing(GMN4) Then
        objnewmember.GMN4Cod = Null
    Else
        objnewmember.GMN4Cod = GMN4
    End If
    

    objnewmember.MonCod = Mon
    
    If IsMissing(PresGlob) Then
        objnewmember.PresGlobal = Null
    Else
        objnewmember.PresGlobal = PresGlob
    End If
    objnewmember.PermitirAdjDirecta = PermAdjDir
    objnewmember.Estado = Est
    objnewmember.FechaApertura = FecApe
    If IsMissing(FecLimOfe) Then
        objnewmember.FechaMinimoLimOfertas = Null
    Else
        objnewmember.FechaMinimoLimOfertas = FecLimOfe
    End If
    If IsMissing(FecNec) Then
        objnewmember.FechaNecesidad = Null
    Else
            objnewmember.FechaNecesidad = FecNec
    End If
    If IsMissing(FecPres) Then
        objnewmember.FechaPresentacion = Null
    Else
            objnewmember.FechaPresentacion = FecPres
    End If
    If IsMissing(FecEnvPet) Then
        objnewmember.FechaPrimerEnvioPeticiones = Null
    Else
            objnewmember.FechaPrimerEnvioPeticiones = FecEnvPet
    End If
    If IsMissing(FecProxReu) Then
        objnewmember.FechaProximaReunion = Null
    Else
        objnewmember.FechaProximaReunion = FecProxReu
    End If
    If IsMissing(FecUltReu) Then
        objnewmember.FechaUltimaReunion = Null
    Else
        objnewmember.FechaUltimaReunion = FecUltReu
    End If
    If IsMissing(FecReape) Then
        objnewmember.FechaReapertura = Null
    Else
        objnewmember.FechaReapertura = FecReape
    End If
    If IsMissing(HoraEnReunion) Then
        objnewmember.HoraEnReunion = Null
    Else
        objnewmember.HoraEnReunion = HoraEnReunion
    End If
    
   
    If IsMissing(Solicitud) Then
        objnewmember.Solicitud = Null
    Else
        objnewmember.Solicitud = Solicitud
    End If
    
    'subastas
    objnewmember.Subasta = Subasta
    objnewmember.VerProveedor = VerProv
    objnewmember.VerDetallePujas = VerPuja
    objnewmember.Superada = Superada
    
    objnewmember.ObjetivosNuevos = ObjNuevos
    objnewmember.ObjetivosCont = ObjCont
    
    
    If IsMissing(NoOfe) Then
        objnewmember.NoOfe = False
    Else
        objnewmember.NoOfe = NoOfe
    End If
    
    If IsMissing(FecNoOfe) Then
        objnewmember.FechaNoOfe = Null
    Else
        objnewmember.FechaNoOfe = FecNoOfe
    End If
    
    If IsMissing(MotivoNoOfe) Then
        objnewmember.MotivoNoOfe = Null
    Else
        objnewmember.MotivoNoOfe = MotivoNoOfe
    End If
    
    
    If IsMissing(Com) Or IsNull(Com) Then
        Set objnewmember.Com = Nothing
    Else
        Set objnewmember.Com = New CPersona
        objnewmember.Com.Cod = Com
        objnewmember.Com.Nombre = Nom
        objnewmember.Com.Apellidos = Ape
    End If
    
    objnewmember.objetivosPublicados = objPublicados
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
    'set the properties passed into the method
       sCod = Gmn1 & Mid$("                         ", 1, 50 - Len(Gmn1))
       mCol.Add objnewmember, CStr(Anyo) & sCod & CStr(Cod)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo Error

    mCol.Remove vntIndexKey

Error:

End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    
    Set mCol = Nothing
    Set mvarConexion = Nothing
    
End Sub

Public Property Get Subasta() As Integer
    Subasta = mvarSubasta
End Property
Public Property Let Subasta(ByVal b As Integer)
    mvarSubasta = b
End Property

Public Property Get Normales() As Integer
    Normales = mvarNormales
End Property
Public Property Let Normales(ByVal b As Integer)
    mvarNormales = b
End Property

