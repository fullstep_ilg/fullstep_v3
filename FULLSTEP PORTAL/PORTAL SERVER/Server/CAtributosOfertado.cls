VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAtributosOfertado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection


Public Property Get Item(vntIndexKey As Variant) As CAtributoOfertado
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

''' <summary>
''' A�ade un elemento a la colecci�n de atributos ofertados
''' </summary>
''' <param name="vValor">valor del atributo</param>
''' <param name="oAtrib">objeto de tipo atributo</param>
''' <param name="vIndice">�ndice</param>
''' <param name="oObj">objeto</param>
''' <param name="vImporteParcial">importe parcial del atributo (c/d)</param>
''' <revisado>JVS 17/06/2011</revisado>
Public Function Add(ByVal vValor As Variant, ByVal oAtrib As CAtributo, Optional ByVal vIndice As Variant, Optional ByRef oObj As Object, Optional ByVal vImporteParcial As Variant) As CAtributoOfertado
        
    Dim objnewmember As CAtributoOfertado
    Set objnewmember = New CAtributoOfertado
   
    
    Set objnewmember.Atributo = oAtrib
    
    objnewmember.Valor = vValor
    
    
    Set objnewmember.Objeto = oObj
    
    If Not IsMissing(vImporteParcial) Then
        objnewmember.ImporteParcial = vImporteParcial
    Else
        objnewmember.ImporteParcial = Null
    End If
    
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        
        mCol.Add objnewmember, CStr(oAtrib.PAID)
    End If
    
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Sub Remove(vntIndexKey As Variant)
    
        mCol.Remove vntIndexKey
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    Set mCol = Nothing
End Sub


