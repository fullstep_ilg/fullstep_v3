VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEmpresas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private m_oConexion As CConexion


Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Function Add(ByVal lId As Long, ByVal sNif As String, ByVal sDen As String, Optional ByVal vDireccion As Variant, Optional ByVal vCP As Variant, _
Optional ByVal vCodPais As Variant, Optional ByVal vPoblacion As Variant, Optional ByVal vCodProvi As Variant, Optional ByVal bERP As Boolean, Optional ByVal oConexion As CConexion, Optional ByVal vDenProvi As Variant, Optional ByVal vDenPais As Variant) As CEmpresa

    
    'create a new object
    Dim objnewmember As CEmpresa
   
    Set objnewmember = New CEmpresa
    
    With objnewmember
    
        .id = lId
        .Nif = sNif
        .Den = sDen
        
        If Not IsMissing(vPoblacion) Then
            .Poblacion = vPoblacion
        Else
            .Poblacion = Null
        End If
        
        If Not IsMissing(vDireccion) Then
            .Direccion = vDireccion
        Else
            .Direccion = Null
        End If
        
        If Not IsMissing(vCP) Then
            .CP = vCP
        Else
            .CP = Null
        End If
         
        If Not IsMissing(vCodProvi) Then
            .CodProvi = vCodProvi
        Else
            .CodProvi = Null
        End If
         
        If Not IsMissing(vDenProvi) Then
            .DenProvi = vDenProvi
        Else
            .DenProvi = Null
        End If
        
        
        If Not IsMissing(vCodPais) Then
            .CodPais = vCodPais
        Else
            .CodPais = Null
        End If
            
        If Not IsMissing(vDenPais) Then
            .DenPais = vDenPais
        Else
            .DenPais = Null
        End If
            
        .ERP = bERP
        Set .Conexion = oConexion
    End With
        
        
    
            
    mCol.Add objnewmember, CStr(lId)
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

Public Property Get Item(vntIndexKey As Variant) As CEmpresa
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property


Public Property Get Count() As Long

    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If

End Property


Public Sub Remove(vntIndexKey As Variant)
On Error GoTo NoSeEncuentra:
    mCol.Remove vntIndexKey
    Exit Sub
NoSeEncuentra:
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
  

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

''' <summary>
''' Cargar todas las empresas de una compania
''' </summary>
''' <param name="lCiacomp">Codigo de compa�ia</param>
''' <param name="sIdi">Idioma</param>
''' <param name="lId">identificador de empresa</param>
''' <param name="Pyme">Pyme</param>
''' <remarks>Llamada desde: seguimclt.asp   detalleempresa.asp; Tiempo m�ximo: 0,1</remarks>
''' Revisado por: jbg; Fecha: 11/01/2013
Public Sub CargarTodasLasEmpresasDesde(ByVal lCiaComp As Long, ByVal sIdi As String, Optional ByVal lId As Long, Optional ByVal Pyme As String = "")

    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim adoRes As New ADODB.Recordset
    Dim sConsulta As String
    Dim sFSG As String

    sFSG = Fsgs(m_oConexion, lCiaComp)

    Set adoComm = New ADODB.Command
    Set adoComm.ActiveConnection = m_oConexion.AdoCon
    adoComm.CommandType = adCmdText

    sConsulta = "SELECT DISTINCT E.ID, E.NIF, E.DEN, E.DIR, E.CP, E.POB, E.PAI, E.PROVI, E.ERP, PDEN.DEN PAIDEN, PRDEN.DEN PROVIDEN "
    sConsulta = sConsulta & "  FROM " & sFSG & "EMP E WITH(NOLOCK) "
    sConsulta = sConsulta & " LEFT JOIN " & sFSG & "PAI P WITH(NOLOCK) ON E.PAI = P.COD "
    sConsulta = sConsulta & " LEFT JOIN " & sFSG & "PAI_DEN PDEN WITH(NOLOCK) ON P.COD = PDEN.PAI AND PDEN.IDIOMA=? "
    sConsulta = sConsulta & " LEFT JOIN " & sFSG & "PROVI PR WITH(NOLOCK) ON E.PAI = PR.PAI AND E.PROVI = PR.COD "
    sConsulta = sConsulta & " LEFT JOIN " & sFSG & "PROVI_DEN PRDEN WITH(NOLOCK) ON  PRDEN.PAI = PR.PAI AND PRDEN.PROVI = PR.COD AND PRDEN.IDIOMA=?"

    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sIdi), Value:=sIdi)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sIdi), Value:=sIdi)
    adoComm.Parameters.Append adoParam

    If Pyme <> "" Then
        sConsulta = sConsulta & " INNER JOIN (SELECT COALESCE(UON1.EMPRESA,UON2.EMPRESA,UON3.EMPRESA,UON4.EMPRESA) EMPRESA "
        sConsulta = sConsulta & "   FROM " & sFSG & "UON1 WITH(NOLOCK)  "
        sConsulta = sConsulta & "   INNER JOIN " & sFSG & "PYMES PG WITH(NOLOCK) ON UON1.PYME=PG.ID"
        sConsulta = sConsulta & "   LEFT JOIN " & sFSG & "UON2 WITH(NOLOCK) ON UON1.COD=UON2.UON1 AND UON2.EMPRESA IS NOT NULL"
        sConsulta = sConsulta & "   LEFT JOIN " & sFSG & "UON3 WITH(NOLOCK) ON UON1.COD=UON3.UON1 AND UON2.COD=UON3.UON2 AND UON3.EMPRESA IS NOT NULL"
        sConsulta = sConsulta & "   LEFT JOIN " & sFSG & "UON4 WITH(NOLOCK) ON UON1.COD=UON4.UON1 AND UON2.COD=UON4.UON2 AND UON3.COD=UON4.UON3 AND UON4.EMPRESA IS NOT NULL"
        sConsulta = sConsulta & "   WHERE PG.COD=?"
        sConsulta = sConsulta & " ) Z ON Z.EMPRESA=E.ID"
        
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(Pyme), Value:=Pyme)
        adoComm.Parameters.Append adoParam
    End If

    sConsulta = sConsulta & " WHERE 1 = 1 "

    If lId <> 0 Then
        sConsulta = sConsulta & " AND E.ID = ?"
        
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lId)
        adoComm.Parameters.Append adoParam
    End If
    
    sConsulta = sConsulta & " ORDER BY E.ID"

    Set adoRes = New ADODB.Recordset

    adoComm.CommandText = sConsulta
    Set adoRes = adoComm.Execute
  
    If adoRes.eof Then
            
        adoRes.Close
        Set adoRes = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
          
    Else
        
        Set mCol = Nothing
        Set mCol = New Collection
        While Not adoRes.eof
            Me.Add adoRes.Fields("ID").Value, adoRes.Fields("NIF").Value, adoRes.Fields("DEN").Value, adoRes.Fields("DIR").Value, adoRes.Fields("CP").Value, adoRes.Fields("PAI").Value, adoRes.Fields("POB").Value, adoRes.Fields("PROVI").Value, (adoRes.Fields("ERP").Value = 1), m_oConexion, adoRes.Fields("PROVIDEN").Value, adoRes.Fields("PAIDEN").Value
            adoRes.MoveNext
        Wend
        
    End If
    
    adoRes.Close
    Set adoRes = Nothing
    Set adoParam = Nothing
    Set adoComm = Nothing
End Sub


