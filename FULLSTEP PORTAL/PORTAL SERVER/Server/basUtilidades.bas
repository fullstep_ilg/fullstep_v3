Attribute VB_Name = "basUtilidades"
Option Explicit
Private Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As Long
Private Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long
Private Declare Function RegQueryValueExString Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, ByVal lpData As String, lpcbData As Long) As Long
Private Declare Function RegQueryValueExLong Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Long, lpcbData As Long) As Long
Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
Private Declare Function RegQueryValueExNULL Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, ByVal lpData As Long, lpcbData As Long) As Long
Public Const HKEY_CURRENT_USER = &H80000001
Public Const KEY_ALL_ACCESS = &H3F
Public Const ERROR_NONE = 0
Public Const REG_SZ As Long = 1
Public Const REG_DWORD As Long = 4

Public Declare Function GetTickCount Lib "kernel32" () As Long

''' <summary>
''' Generar un String de Alta Entropia para Gesti�n de sesiones mediante identificadores encriptados
''' </summary>
''' <param name="Dato">dato </param>
''' <returns>String de Alta Entropia</returns>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0,2</remarks>
Public Function GenerarStringAltaEntropia(ByVal Dato As String) As String
           
    GenerarStringAltaEntropia = Left(Encriptar(Dato, Now), 40)

End Function

Function StrToVbNULL(StrThatCanBeEmpty) As Variant
    
    If IsEmpty(StrThatCanBeEmpty) Then
        StrToVbNULL = Null
    Else
        If IsNull(StrThatCanBeEmpty) Then
            StrToVbNULL = Null
        Else
            If StrThatCanBeEmpty = "" Then
                StrToVbNULL = Null
            Else
                StrToVbNULL = StrThatCanBeEmpty
            End If
        End If
    End If
    
End Function
Public Function DblQuote(ByVal StrToDblQuote As String) As String

    DblQuote = Replace(StrToDblQuote, "'", "''")
    
End Function


Public Function NullToDbl0(ByVal ValueThatCanBeNull As Variant) As Variant
  
    If IsNull(ValueThatCanBeNull) Then
        NullToDbl0 = 0
    Else
        NullToDbl0 = ValueThatCanBeNull
    End If

End Function

''' <summary>
''' Si el Dato a traducir contiene algo lo devuelve como fecha, eoc devuelve null
''' </summary>
''' <param name="DateThatCanBeEmpty">Dato a traducir</param>
''' <returns>Fecha si DateThatCanBeEmpty contiene algo, null eoc</returns>
''' <remarks>Llamada desde: coferta.AlmacenarOfertaPortal; Tiempo m�ximo:0</remarks>
Function DateToVbDate(DateThatCanBeEmpty) As Variant

If IsEmpty(DateThatCanBeEmpty) Then
    DateToVbDate = Null
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToVbDate = Null
    Else
        If DateThatCanBeEmpty = "" Then
            DateToVbDate = Null
        Else
            DateToVbDate = DateThatCanBeEmpty
        End If
    End If
End If

End Function


Function DateToSQLDate(DateThatCanBeEmpty) As Variant


If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLDate = "NULL"
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLDate = "NULL"
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLDate = "NULL"
        Else
            DateToSQLDate = "Convert(datetime,'" & Format(DateThatCanBeEmpty, "mm-dd-yyyy") & "',110)"
        End If
    End If
End If

End Function
Function DateToSQLTimeDate(DateThatCanBeEmpty) As Variant


If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLTimeDate = "NULL"
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLTimeDate = "NULL"
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLTimeDate = "NULL"
        Else
            DateToSQLTimeDate = "Convert(datetime,'" & Format(DateThatCanBeEmpty, "mm-dd-yyyy hh:mm:ss") & "',110)"
        End If
    End If
End If

End Function

''' <summary>
''' Funci�n que encripta un string que se le pasa como par�metro.
''' </summary>
''' <param name="Dato">Dato a encriptar.</param>
''' <param name="Fecha">Fecha para la encriptaci�n</param>
''' <returns>El string encriptado.</returns>
''' <remarks>Llamada desde: CRaiz.Encriptar ; Tiempo m�ximo: 0,1</remarks>
Public Function Encriptar(ByVal Dato As String, Optional ByVal Fecha As Variant) As String
    Dim fechahoracrypt As Date
    Dim diacrypt
    Dim oCrypt2 As CCrypt2
        
    If IsMissing(Fecha) Then
        Fecha = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            Fecha = DateSerial(1974, 3, 5)
        End If
    End If
        
    diacrypt = Day(Fecha)
    
    fechahoracrypt = Fecha
    
    Set oCrypt2 = New CCrypt2
    
    oCrypt2.InBuffer = Dato
    If diacrypt Mod 2 = 0 Then
        oCrypt2.Codigo = ClavePar5Bytes
    Else
        oCrypt2.Codigo = ClaveImp5bytes
    End If
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    oCrypt2.Encrypt
    Encriptar = oCrypt2.OutBuffer
                            
    Set oCrypt2 = Nothing
End Function

''' <summary>
''' Funci�n que desencripta un string que se le pasa como par�metro.
''' </summary>
''' <param name="Dato">Dato a desencriptar.</param>
''' <param name="Fecha">Fecha para la desencriptaci�n</param>
''' <returns>El string desencriptado</returns>
''' <remarks>Llamada desde: LICDBContra, LICDBLogin  ; Tiempo m�ximo: 0,1</remarks>
Public Function DesEncriptarFSDB(ByVal Dato As String, ByVal Fecha As Variant) As String
    Dim oCrypt2 As CCrypt2

    Set oCrypt2 = New CCrypt2
    
    oCrypt2.InBuffer = Dato
    oCrypt2.Codigo = ClaveSQLtod
    oCrypt2.Fecha = Fecha
        
    oCrypt2.Decrypt
    
    DesEncriptarFSDB = oCrypt2.OutBuffer
    
    Set oCrypt2 = Nothing
    
End Function

''' <summary>
''' Funci�n que desencripta un string que se le pasa como par�metro.
''' </summary>
''' <param name="Dato">Dato a desencriptar.</param>
''' <param name="Fecha">Fecha para la desencriptaci�n</param>
''' <returns>El string desencriptado.</returns>
''' <remarks>Llamada desde:  CRaiz.DesEncriptar ; Tiempo m�ximo: 0,1</remarks>
Public Function DesEncriptar(ByVal Dato As String, Optional ByVal Fecha As Variant) As String
    Dim fechahoracrypt As Date
    Dim diacrypt
    Dim oCrypt2 As CCrypt2
    
    If IsMissing(Fecha) Then
        Fecha = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            Fecha = DateSerial(1974, 3, 5)
        End If
    End If
    diacrypt = Day(Fecha)

    fechahoracrypt = Fecha
    
    Set oCrypt2 = New CCrypt2
    
    oCrypt2.InBuffer = Dato
    If diacrypt Mod 2 = 0 Then
        oCrypt2.Codigo = ClavePar5Bytes
    Else
        oCrypt2.Codigo = ClaveImp5bytes
    End If
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    oCrypt2.Decrypt
    DesEncriptar = oCrypt2.OutBuffer
            
    Set oCrypt2 = Nothing
End Function

''' <summary>Realiza la encriptaci�n/desencriptaci�n del password del usuario</summary>
''' <param name="Seed">Semilla para encriptar � desencriptar</param>
''' <param name="Dato">Dato a encriptar � desencriptar</param>
''' <param name="Encriptando">Indica si se trata de una encriptaci�n � desencriptaci�n</param>
''' <param name="Fecha">Fecha para la encriptaci�n</param>
''' <param name="Quien">Si estas trabajando con 0-licencia FSDB.LIC 1-usuarios 2-licencia FSOS.LIC</param>
''' <param name="Tipo">Tipo De Usuario</param>
''' <returns>String con el dato encriptado/desencriptado</returns>
Public Function EncriptarAES(ByVal Seed As String, ByVal Dato As String, ByVal Encriptando As Boolean, _
    ByVal Fecha As Variant, ByVal Quien As Integer, Optional ByVal Tipo As TipoDeUsuario) As String
    Dim fechahoracrypt As Date
    Dim diacrypt
    Dim oCrypt2 As CCrypt2
    
    If IsMissing(Fecha) Then
        Fecha = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            Fecha = DateSerial(1974, 3, 5)
        End If
    End If
        
    fechahoracrypt = Fecha
    
    Set oCrypt2 = New CCrypt2
    
    oCrypt2.InBuffer = Dato
    
    If Quien = 0 Then
        oCrypt2.Codigo = ClaveSQLtod
    ElseIf Quien = 2 Then
        oCrypt2.Codigo = ClaveOBJtod
    Else
        diacrypt = Day(Fecha)
        
        If diacrypt Mod 2 = 0 Then
            If Tipo = 1 Then
                oCrypt2.Codigo = Seed & ClaveADMpar
            Else
                oCrypt2.Codigo = Seed & ClaveUSUpar
            End If
        Else
            If Tipo = 1 Then
                oCrypt2.Codigo = Seed & ClaveADMimp
            Else
                oCrypt2.Codigo = Seed & ClaveUSUimp
            End If
        End If
    End If
        
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    If Encriptando Then
        oCrypt2.Encrypt
    Else
        oCrypt2.Decrypt
    End If
    
    EncriptarAES = oCrypt2.OutBuffer
                            
    Set oCrypt2 = Nothing
End Function

''' <summary>
''' Funci�n que encripta un string que se le pasa como par�metro.
''' </summary>
''' <param name="Dato">Dato a encriptar.</param>
''' <param name="Fecha">Fecha para la encriptaci�n</param>
''' <returns>El string encriptado.</returns>
''' <remarks>Llamada desde: CRaiz.Encriptar ; Tiempo m�ximo: 0,1</remarks>
Public Function CompararHash(ByVal Salt As String, ByVal pwd As String, ByVal bdpwd As String) As Boolean
    Dim oCrypt2 As CCrypt2
    Set oCrypt2 = New CCrypt2

    CompararHash = oCrypt2.CompararHash(Salt, pwd, bdpwd)
    Set oCrypt2 = Nothing
End Function
Public Function GenerarSALT() As String
    Dim oCrypt2 As CCrypt2
    Set oCrypt2 = New CCrypt2

    GenerarSALT = oCrypt2.GenerarSALT
    Set oCrypt2 = Nothing
End Function
Public Function EncryptHash(ByVal Salt As String, ByVal pwd As String) As String
    Dim oCrypt2 As CCrypt2
    Set oCrypt2 = New CCrypt2

    EncryptHash = oCrypt2.EncryptHash(Salt, pwd)
    Set oCrypt2 = Nothing
End Function
Public Function SQLBinaryToBoolean(ByVal var As Variant) As Variant
    
    If IsNull(var) Then
        SQLBinaryToBoolean = False
    Else
        If var = 1 Then
            SQLBinaryToBoolean = True
        Else
            SQLBinaryToBoolean = False
        End If
    End If
    

End Function
Public Function BooleanToSQLBinary(ByVal b As Variant) As Variant
    If IsNull(b) Then
        BooleanToSQLBinary = "NULL"
        Exit Function
    End If
    
    If b Then
        BooleanToSQLBinary = 1
    Else
        BooleanToSQLBinary = 0
    End If
    
End Function

Public Function NullToStr(ByVal ValueThatCanBeNull As Variant) As String
  
    If IsNull(ValueThatCanBeNull) Then
        NullToStr = ""
    Else
        NullToStr = ValueThatCanBeNull
    End If

End Function
Function StrToSQLNULL(StrThatCanBeEmpty) As Variant
    
    If IsEmpty(StrThatCanBeEmpty) Then
        StrToSQLNULL = "NULL"
    Else
        If IsNull(StrThatCanBeEmpty) Then
            StrToSQLNULL = "NULL"
        Else
            If StrThatCanBeEmpty = "" Then
                StrToSQLNULL = "NULL"
            Else
                StrToSQLNULL = "'" & DblQuote(CStr(StrThatCanBeEmpty)) & "'"
            End If
        End If
    End If
    
End Function
Private Function QueryValueEx(ByVal lHkey As Long, ByVal szValueName As String, vValue As Variant) As Long
Dim cch As Long
Dim lrc As Long
Dim lType As Long
Dim lValue As Long
Dim sValue As String
On Error GoTo QueryValueExError
          
    ' Determine the size and type of data to be read
    lrc = RegQueryValueExNULL(lHkey, szValueName, 0&, lType, 0&, cch)
    If lrc <> ERROR_NONE Then Error 5
    Select Case lType
              ' For strings
        Case REG_SZ:
                sValue = String(cch, 0)
                lrc = RegQueryValueExString(lHkey, szValueName, 0&, lType, sValue, cch)
                If lrc = ERROR_NONE Then
                    vValue = Left$(sValue, cch - 1)
                Else
                    vValue = Empty
                End If
              ' For DWORDS
        Case REG_DWORD:
                lrc = RegQueryValueExLong(lHkey, szValueName, 0&, lType, lValue, cch)
                If lrc = ERROR_NONE Then vValue = lValue
        Case Else                  'all other data types not supported
                lrc = -1
    End Select
    
QueryValueExExit:
QueryValueEx = lrc
Exit Function
QueryValueExError:
          Resume QueryValueExExit

End Function
Public Function QueryValue(sKeyName As String, sValueName As String) As Variant
Dim lRetVal As Long         'result of the API functions
Dim hKey As Long         'handle of opened key
Dim vValue As Variant      'setting of queried value
    
    lRetVal = RegOpenKeyEx(HKEY_CURRENT_USER, sKeyName, 0, KEY_ALL_ACCESS, hKey)
    lRetVal = QueryValueEx(hKey, sValueName, vValue)
    RegCloseKey (hKey)
    QueryValue = vValue
    
End Function

Public Function DblToSQLFloat(DblToConvert) As String

    Dim StrToConvert As String
    Dim sDecimal As String
    Dim sThousand As String
        
    If IsEmpty(DblToConvert) Or IsNull(DblToConvert) Or DblToConvert = "" Then
        DblToSQLFloat = "NULL"
        Exit Function
    End If
    
    StrToConvert = CStr(DblToConvert)
    
    sDecimal = QueryValue("Control Panel\International", "sDecimal")
    sThousand = QueryValue("Control Panel\International", "sThousand")
    
    StrToConvert = Replace(StrToConvert, sThousand, "")
    StrToConvert = Replace(StrToConvert, sDecimal, ".")
    
    DblToSQLFloat = StrToConvert

End Function





Public Function Fsgs(oConn As CConexion, ByVal lCiaComp As Long) As String

Dim adoRes As adodb.Recordset
Dim sConsulta As String

Dim sSRV As Variant
Dim sBD As String


'Primero debemos obtener los datos de SERVIDOR,sBD,... de la CiaCompradora
sConsulta = "SELECT FSGS_SRV,FSGS_BD " & vbCrLf _
          & "  FROM CIAS WITH (NOLOCK) " & vbCrLf _
          & " WHERE ID=" & lCiaComp
Set adoRes = New adodb.Recordset
adoRes.Open sConsulta, oConn.AdoCon, adOpenForwardOnly, adLockReadOnly

If adoRes.eof Then
    adoRes.Close
    Set adoRes = Nothing
    Fsgs = ""
    Exit Function
End If

sSRV = adoRes("FSGS_SRV").Value
sBD = adoRes("FSGS_BD").Value

adoRes.Close
Set adoRes = Nothing

If IsNull(sSRV) Then
    Fsgs = sBD & ".dbo."
Else
    Fsgs = sSRV & "." & sBD & ".dbo."
End If




End Function

Public Function NumTransaccionesAbiertas(oCon As adodb.Connection) As Integer

Dim sConsulta As String
Dim adoRS As adodb.Recordset

sConsulta = "SELECT @@TRANCOUNT NUMTRAN"

    
Set adoRS = New adodb.Recordset
adoRS.Open sConsulta, oCon, adOpenForwardOnly, adLockReadOnly

NumTransaccionesAbiertas = adoRS.Fields("NUMTRAN").Value

adoRS.Close
Set adoRS = Nothing

End Function





Public Function Fecha(ByVal mFecha As Variant, DateFMT As String) As Variant



Dim datesep As String
Dim returnFecha As Variant
Dim mdia As Integer
Dim mmes As Integer
Dim manyo As Integer
Dim sHora As String


Dim iPosDia As Integer
Dim iPosMes As Integer
Dim iPosAnyo As Integer
Dim sFmt As String

iPosDia = InStr(DateFMT, "dd")
iPosMes = InStr(DateFMT, "mm")
iPosAnyo = InStr(DateFMT, "yyyy")



If IsNull(mFecha) Or mFecha = "" Then
    Fecha = Null
    Exit Function
End If
returnFecha = mFecha
datesep = Mid(DateFMT, 3, 1)
        
Dim sTmpDateFmt As String
sTmpDateFmt = DateFMT
Dim i As Integer


For i = Asc("A") To Asc("Z")
    sTmpDateFmt = Replace(sTmpDateFmt, Chr(i), "", , , vbTextCompare)
Next i

datesep = Left(sTmpDateFmt, 1)


        
        
If InStr(returnFecha, ":") Then
    sHora = Mid(returnFecha, InStr(returnFecha, " "), Len(returnFecha))
    returnFecha = Mid(returnFecha, 1, InStr(returnFecha, " "))
End If
        
mFecha = Split(returnFecha, datesep)


If iPosDia < iPosMes And iPosDia < iPosAnyo Then
    If iPosMes < iPosAnyo Then
    
        mdia = CLng(mFecha(0))
        mmes = CLng(mFecha(1))
        manyo = CLng(mFecha(2))
            
    Else
        mdia = CLng(mFecha(0))
        mmes = CLng(mFecha(2))
        manyo = CLng(mFecha(1))
            
    End If
Else
    If iPosMes < iPosDia And iPosMes < iPosAnyo Then
        If iPosDia < iPosAnyo Then
            mdia = CLng(mFecha(1))
            mmes = CLng(mFecha(0))
            manyo = CLng(mFecha(2))
        
        Else
            mdia = CLng(mFecha(2))
            mmes = CLng(mFecha(0))
            manyo = CLng(mFecha(1))
        End If
    Else
        If iPosAnyo < iPosDia And iPosAnyo < iPosMes Then
            If iPosDia < iPosMes Then
                mdia = CLng(mFecha(1))
                mmes = CLng(mFecha(2))
                manyo = CLng(mFecha(0))
            Else
                mdia = CLng(mFecha(2))
                mmes = CLng(mFecha(1))
                manyo = CLng(mFecha(0))
            End If
        End If
    End If
    
End If



On Error Resume Next
If sHora = "" Then
    returnFecha = DateSerial(manyo, mmes, mdia)
Else
    
    returnFecha = DateSerial(manyo, mmes, mdia)
    returnFecha = DateAdd("h", CInt(Mid(sHora, 1, InStr(sHora, ":") - 1)), returnFecha)
    returnFecha = DateAdd("n", CInt(Mid(sHora, InStr(sHora, ":") + 1, 2)), returnFecha)
    
        
End If

Fecha = returnFecha


End Function


Public Function Numero(ByVal Num As Variant, ByVal sDecimalFmt As String) As Variant

    Dim NumeroFinal As Variant
    Dim Indice As Integer
    Dim dividir As Variant
    Dim decimales As Boolean

    Dim Numerico As String
    On Error GoTo Error
    If IsNull(Num) Then
        Numero = Null
        Exit Function
    End If
    dividir = 1
    NumeroFinal = ""

    Num = Replace(Num, sDecimalFmt, ".")


    If Num = "" Then
        Numero = Null
        Exit Function
    End If
        
    For Indice = 1 To Len(Num)
        Select Case Mid(Num, Indice, 1)
            Case "."
                NumeroFinal = NumeroFinal
                decimales = True
            Case Else
                NumeroFinal = NumeroFinal & Mid(Num, Indice, 1)
                If decimales = True Then
                    dividir = dividir * 10
                End If
        End Select
    Next
    If decimales = True Then
        Numerico = CDec(NumeroFinal) / CDec(dividir)
    Else
        Numerico = NumeroFinal
    End If
    
    Numero = Numerico
fin:
    Exit Function
Error:
    Debug.Print Err.Description
    Numero = Null
    Resume fin
    Resume 0
                
End Function

''' <summary>
''' Funci�n que encripta/desencripta un string que se le pasa como par�metro.
''' </summary>
''' <param name="sCadena">Texto para la key del encrypter</param>
''' <param name="sDato">Texto Sobre a encriptar/desencriptar</param>
''' <param name="Encriptando">true Encriptando/ false Desencriptando</param>
''' <returns>Texto encriptado/desencriptado</returns>
''' <remarks>Llamada desde: CProceso.CargarConfiguracion; Tiempo m�ximo: 0,2</remarks>
Public Function EncriptarAperturaSobre(ByVal sCadena As String, ByVal sDato As String, ByVal Encriptando As Boolean) As String
  
    Dim oCrypt2 As CCrypt2
        
    Set oCrypt2 = New CCrypt2
    
    oCrypt2.InBuffer = sDato
    If Len(sCadena) Mod 2 = 0 Then
        oCrypt2.Codigo = sCadena & ClavePar5Bytes
    Else
        oCrypt2.Codigo = sCadena & ClaveImp5bytes
    End If
    oCrypt2.Fecha = DateSerial(1974, 3, 5)
    
    If Encriptando Then
        oCrypt2.Encrypt
    Else
        oCrypt2.Decrypt
    End If
            
    EncriptarAperturaSobre = oCrypt2.OutBuffer
    
    Set oCrypt2 = Nothing
    
End Function



Function ObtenerFmtDesdeFecha(ByVal sFec As String) As String

'En sFec tiene que venir la fecha 31/12/2003 en el orden que sea

Dim iPosDia As Integer
Dim iPosMes As Integer
Dim iPosAnyo As Integer
Dim sFmt As String
Dim sDatesep As String
Dim sTmpDateFmt As String

Dim i As Integer

iPosDia = InStr(sFec, "31")
iPosMes = InStr(sFec, "12")
iPosAnyo = InStr(sFec, "2003")
If iPosAnyo = 0 Then iPosAnyo = InStr(sFec, "03")
sTmpDateFmt = sFec
For i = Asc("0") To Asc("9")
    sTmpDateFmt = Replace(sTmpDateFmt, Chr(i), "", , , vbTextCompare)
Next i

sDatesep = Left(sTmpDateFmt, 1)


If iPosDia < iPosMes And iPosDia < iPosAnyo Then
    If iPosMes < iPosAnyo Then
        sFmt = "dd" & sDatesep & "mm" & sDatesep & "yyyy"
    Else
        sFmt = "dd" & sDatesep & "yyyy" & sDatesep & "mm"
    End If
Else
    If iPosMes < iPosDia And iPosMes < iPosAnyo Then
        If iPosDia < iPosAnyo Then
            sFmt = "mm" & sDatesep & "dd" & sDatesep & "yyyy"
        Else
            sFmt = "mm" & sDatesep & "yyyy" & sDatesep & "dd"
        End If
    Else
        If iPosAnyo < iPosDia And iPosAnyo < iPosMes Then
            If iPosDia < iPosMes Then
                sFmt = "yyyy" & sDatesep & "dd" & sDatesep & "mm"
            Else
                sFmt = "yyyy" & sDatesep & "mm" & sDatesep & "dd"
            End If
        End If
    End If
    
End If

ObtenerFmtDesdeFecha = sFmt
    

End Function

Public Sub CrearArbol(ByVal sPath As String)
Dim oFSO As scripting.FileSystemObject
Dim sUnidad As String

Dim sPathCreado As String

Dim sTemp As String

'Quitar Unidad

sUnidad = Left(sPath, 3)

sTemp = Right(sPath, Len(sPath) - 3)

sPathCreado = ""
Set oFSO = New scripting.FileSystemObject
While InStr(sTemp, "\") > 0
    If oFSO.FolderExists(sUnidad & sPathCreado & Left(sTemp, InStr(sTemp, "\"))) = False Then
        oFSO.CreateFolder sUnidad & sPathCreado & Left(sTemp, InStr(sTemp, "\"))
    End If
    sPathCreado = sPathCreado & Left(sTemp, InStr(sTemp, "\"))
    sTemp = Right(sTemp, Len(sTemp) - InStr(sTemp, "\"))
    
Wend

Set oFSO = Nothing

End Sub

''' <summary>
''' Funci�n que encripta/desencripta un string que se le pasa como par�metro.
''' </summary>
''' <param name="sCadena">Texto para la key del encrypter</param>
''' <param name="sDato">Texto Sobre a encriptar/desencriptar</param>
''' <param name="Encriptando">true Encriptando/ false Desencriptando</param>
''' <returns>Texto encriptado/desencriptado</returns>
''' <remarks>Llamada desde: CGestorParametros.CadAccesoFSGS  CGestorParametros.CadAccesoFSSM ; Tiempo m�ximo: 0,1</remarks>
Public Function EncriptarAcceso(ByVal sCadena As String, ByVal sDato As String, ByVal Encriptando As Boolean) As String
    Dim oCrypt2 As CCrypt2
    Dim fechahoracrypt As Date

    Set oCrypt2 = New CCrypt2
    
    oCrypt2.InBuffer = sDato
        
    If Len(sCadena) Mod 2 = 0 Then
        oCrypt2.Codigo = ClavePar5Bytes
    Else
        oCrypt2.Codigo = ClaveImp5bytes
    End If
    
    fechahoracrypt = DateSerial(1974, 3, 5)
    oCrypt2.Fecha = fechahoracrypt
    
    If Encriptando Then
        oCrypt2.Encrypt
    Else
        oCrypt2.Decrypt
    End If
    
    EncriptarAcceso = oCrypt2.OutBuffer
                
    Set oCrypt2 = Nothing
End Function

''' <summary>
''' Comprueba que el c�digo del idioma est� entre los idiomas v�lidos
''' </summary>
''' <param name="sIdi">C�digo de idioma</param>
''' <returns>sIdi si es un idioma v�lido, "ENG" si no es v�lido
'''     * Si intentan hacer SQL Injection o no es un idioma v�lido devolvemos el idioma "ENG" por defecto
''' </returns>
''' <remarks>Llamada desde: Todas las funciones y m�todos que se vayan adecuando a la normativa en cuanto a SQL Injection
'''     ccompania/modificarDatosCompania    cmonedas/CargarTodasLasMonedasDelPortalDesde     coferta/TransferirOferta
'''     cpaises/CargarTodosLosPaises        cunidadespedido/CargarDatosUnidad       craiz/DevolverCompaniasCompradoras
'''     craiz/DevolverProveedorEnCiaComp    craiz/ModificarDatosUsuario             craiz/DevolverCompaniasConPedidos
'''     craiz/DevolverCodPedido             craiz/DevolverCompaniasConSolicitudes   craiz/DevolverCompaniasConCertificados
'''     ccertificados/DevolverCertificados  csolicitudes/DevolverSolicitudes        csolicitudes/DevolverPendientesCertificados
'''     cproceso/CargarOfertaProveedor      cproceso/CargarProceso                  cproceso/CargarResultadoPuja
'''     cproceso/CargarAdorItems        cordenes/BuscarTodasOrdenes       corden/DevolverLineas     coferta/Enviar
'''     cnoconformidades/DevolverNoConformidades    cmonedas/CargarTodasLasMonedasDelPortalDesde
'''     cmonedas/CargarTodasLasMonedasDesde     cgrupo/CargarGrupo    cgrupo/CargarItems   cgrupo/CargarAdorItems
'''     cdestinos/CargarTodosLosDestinosDesde   cdestinos/CargarDatosDestino        ccompania/DevolverEstructuraActividades
'''     ccompania/DevolverEstructuraActividadesVisible      csolicitudes/DevolverPendientes cactividadnivel4/CargarTodasLasActividadesNivel5
'''     cactividadnivel3/CargarTodasLasActividadesNivel4    cactividadnivel2/CargarTodasLasActividadesNivel3
'''     cactividadnivel1/CargarTodasLasActividadesNivel2    cactividadesnivel1/CargarTodasLasActividades
'''; Tiempo m�ximo:1msg
''' </remarks>
Public Function EsIdiomaValido(ByVal sIdi As String) As String
    Select Case sIdi
    Case "ENG"
        EsIdiomaValido = "ENG"
    Case "SPA"
        EsIdiomaValido = "SPA"
    Case "GER"
        EsIdiomaValido = "GER"
    Case "FRA"
        EsIdiomaValido = "FRA"
    Case Else
        EsIdiomaValido = "ENG"
    End Select
End Function

''' <summary>
''' Si el Dato a traducir contiene algo lo devuelve como un double
''' </summary>
''' <param name="DblToConvert">Dato a traducir</param>
''' <returns>si DblToConvert contiene algo double, null eoc</returns>
''' <remarks>Llamada desde: coferta.AlmacenarOfertaPortal; Tiempo m�ximo:0</remarks>
Public Function DblToVbFloat(DblToConvert) As Variant
    If IsEmpty(DblToConvert) Or IsNull(DblToConvert) Or DblToConvert = "" Then
        DblToVbFloat = Null
        Exit Function
    End If
    
'    DblToVbFloat = CDbl(DblToSQLFloat(DblToConvert))
    DblToVbFloat = CDbl(DblToConvert)
End Function

''' <summary>
''' Si el Dato a traducir contiene algo lo devuelve como un entero 1 true 0 false, eoc devuelve null
''' </summary>
''' <param name="b">Dato a traducir</param>
''' <returns>si b contiene algo un entero 1 true 0 false, null eoc</returns>
''' <remarks>Llamada desde: coferta.AlmacenarOfertaPortal; Tiempo m�ximo:0</remarks>
Public Function BooleanToVbBinary(ByVal b As Variant) As Variant
    If IsEmpty(b) Or IsNull(b) Then
        BooleanToVbBinary = Null
        Exit Function
    End If
    
    If b Then
        BooleanToVbBinary = 1
    Else
        BooleanToVbBinary = 0
    End If
    
End Function
''' <summary>
''' Devuelve si vParametro contiene algo nulo
''' </summary>
''' <param name="vParametro">Parametro</param>
''' <returns>Si vParametro contiene algo nulo</returns>
''' <remarks>Llamada desde: Cexcel.GenerarHojaExcel ; Tiempo m�ximo: 0</remarks>
Public Function NoHayParametro(ByVal vParametro As Variant) As Boolean
    If (IsMissing(vParametro) Or IsEmpty(vParametro) Or IsNull(vParametro)) Then
        NoHayParametro = True
    ElseIf (vParametro = "") Then
        NoHayParametro = True
    Else
        NoHayParametro = False
    End If
End Function

''' <summary>
''' Subrutina de llamada al servicio de integraci�n.
''' </summary>
''' <param name="IDOrden">El ID del orden de entrega.</param>
''' <param name="idPedido">El ID del pedido.</param>
''' <param name="CiaComp">La compa��a.</param>
''' <param name="con">La conexi�n a la BD.</param>
''' <remarks>Llamada desde: COrden.AceptarPorProveedor.</remarks>
Public Sub LlamarFSIS(ByVal IDOrden As Long, ByVal idPedido As Long, ByVal CiaComp As Long, _
    ByVal con As CConexion)
Dim sConsulta, sFSG, sRuta As String
Dim rs As adodb.Recordset
Dim iTipoInt, iErp As Integer
Dim IntPedTrasAcepActivado As Boolean
Dim vNombreUsu, vContrasenyaUsu As Variant

On Error GoTo Error:
    sFSG = Fsgs(con, CiaComp)
    
    sConsulta = "SELECT TOP 1 ERP= CASE WHEN EO.ERP IS NULL THEN CASE when ES.ERP IS NULL THEN '0' ELSE ES.ERP  END ELSE EO.ERP END "
    sConsulta = sConsulta & "FROM " & sFSG & "ORDEN_ENTREGA OE WITH (NOLOCK) "
    sConsulta = sConsulta & "LEFT JOIN " & sFSG & "EMP E WITH(NOLOCK) on E.ID=OE.EMPRESA "
    sConsulta = sConsulta & "LEFT JOIN " & sFSG & "ERP_SOCIEDAD ES WITH(NOLOCK) ON ES.SOCIEDAD=E.SOCIEDAD "
    sConsulta = sConsulta & "LEFT JOIN (" & sFSG & "ERP_ORGCOMPRAS EO WITH (NOLOCK) INNER JOIN " & sFSG & "SOCIEDAD_ORGCOMPRAS SO WITH(NOLOCK) "
    sConsulta = sConsulta & "ON SO.ORGCOMPRAS=EO.ORGCOMPRAS) ON SO.SOCIEDAD = E.SOCIEDAD AND SO.ORGCOMPRAS=OE.ORGCOMPRAS "
    sConsulta = sConsulta & "WHERE OE.ID=" & IDOrden
    Set rs = New adodb.Recordset
    rs.Open sConsulta, con.AdoCon, adOpenForwardOnly, adLockReadOnly
    If Not rs.eof Then
        iErp = IIf(IsNull(rs.Fields("ERP")), 0, rs.Fields("ERP").Value)
    Else
        GoTo Error
    End If
    rs.Close
    
    If iErp > 0 Then

        sConsulta = "SELECT DESTINO_TIPO FROM " & sFSG & "TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE ERP=" & iErp & " AND TABLA IN ("
        sConsulta = sConsulta & EntidadIntegracion.PED_Aprov & ", " & EntidadIntegracion.PED_directo & ")"
        sConsulta = sConsulta & " AND ACTIVA=1 AND SENTIDO IN (" & SentidoIntegracion.salida & ", "
        sConsulta = sConsulta & SentidoIntegracion.EntradaSalida & ")"
        rs.Open sConsulta, con.AdoCon, adOpenForwardOnly, adLockReadOnly
        If Not rs.eof Then
            iTipoInt = rs.Fields("DESTINO_TIPO").Value
        End If
        rs.Close
        
        If iTipoInt = TipoDestinoIntegracion.wcf Then
            sConsulta = "SELECT LOG_GRAL.ID_TABLA, LOG_GRAL.TABLA, LOG_ORDEN_ENTREGA.EST FROM  " & sFSG & "LOG_GRAL WITH(NOLOCK)"
            sConsulta = sConsulta & " INNER JOIN  " & sFSG & "LOG_ORDEN_ENTREGA WITH(NOLOCK) ON LOG_GRAL.ID_TABLA=LOG_ORDEN_ENTREGA.ID"
            sConsulta = sConsulta & " AND LOG_GRAL.TABLA IN (" & EntidadIntegracion.PED_Aprov & ", "
            sConsulta = sConsulta & EntidadIntegracion.PED_directo & ") WHERE LOG_ORDEN_ENTREGA.ID_ORDEN_ENTREGA="
            sConsulta = sConsulta & IDOrden & " AND LOG_ORDEN_ENTREGA.ID_PEDIDO=" & idPedido & " AND LOG_GRAL.ERP=" & iErp
            sConsulta = sConsulta & " AND ESTADO=" & EstadoIntegracion.PendienteTratar
            sConsulta = sConsulta & " AND LOG_GRAL.ORIGEN IN (" & OrigenIntegracion.FSGSInt & ", " & OrigenIntegracion.FSGSIntReg
            sConsulta = sConsulta & ") ORDER BY LOG_GRAL.ID DESC"
            rs.Open sConsulta, con.AdoCon, adOpenForwardOnly, adLockReadOnly
            
            If rs.eof Then
                GoTo Error
            Else
                ''Llamamos siempre que el proveedor haya hecho el cambio de estado (aceptar o rechazar)
                Dim iServiceBindingType As Integer
                Dim iServiceSecurityMode As Integer
                Dim iClientCredentialType As Integer
                Dim iProxyCredentialType As Integer
                ObtenerCredencialesWCF rs.Fields("TABLA").Value, iErp, sFSG, con, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, vNombreUsu, vContrasenyaUsu
                sRuta = ObtenerRutaInstalacionFSIS(sFSG, con)
                If sRuta <> "" Then
                    If oFSIS Is Nothing Then
                        Set oFSIS = New CFSISService
                    End If
                    'Llamamos a FSIS.
                    oFSIS.Exportar rs.Fields("ID_TABLA").Value, rs.Fields("TABLA").Value, sRuta, iServiceBindingType, iServiceSecurityMode, iClientCredentialType, iProxyCredentialType, CStr(vNombreUsu), CStr(vContrasenyaUsu)
                End If
                rs.Close
            End If
        End If
    End If
    
    Set rs = Nothing
Exit Sub

Error:
    rs.Close
    Set rs = Nothing
End Sub

'''<summary>Funci�n que obtiene la ruta de instalaci�n del servicio WCF.</summary>
'''<returns>String con la ruta de instalaci�n del servicio WCF.</returns>
'''<remarks>Llamada desde: basUtilidades.LlamarFSIS.</remarks>
Private Function ObtenerRutaInstalacionFSIS(ByVal sFSG As String, ByVal con As CConexion) As String
    Dim adoRes As New adodb.Recordset
    Dim sConsulta As String
    
On Error GoTo Error:
    sConsulta = "SELECT TOP 1 RUTAXBAP FROM " & sFSG & "PARGEN_RUTAS WITH(NOLOCK) WHERE NOMBREPROY='FullstepIS'"
    adoRes.Open sConsulta, con.AdoCon, adOpenForwardOnly, adLockReadOnly
    If Not adoRes.eof Then
        ObtenerRutaInstalacionFSIS = IIf(IsNull(adoRes.Fields("RUTAXBAP")), "", adoRes.Fields("RUTAXBAP").Value)
    Else
        GoTo Error:
    End If
    
    adoRes.Close
    Set adoRes = Nothing
    Exit Function
    
Error:
    Set adoRes = Nothing
    ObtenerRutaInstalacionFSIS = ""
End Function

'''<summary>Funci�n que obtiene el nombre y la contrase�a desencriptada del usuario del cliente para poder autenticar el servicio WCF v�a Windows.</summary>
'''<param name="iEntidadIntegracion">La entidad a integrar.</param>
'''<param name="vNombreUsu">El nombre del usuario al que pertenece la contrase�a.</param>
'''<param name="vContrasenyaUsu">La contrase�a desencriptada del usuario.</param>
'''<remarks>Llamada desde: basUtilidades.LlamarFSIS.</remarks>
Public Sub ObtenerCredencialesWCF(ByVal iEntidadIntegracion As Integer, ByVal iErp As Integer, ByVal sFSG As String, ByVal con As CConexion, _
                    ByRef iServiceBindingType As Integer, ByRef iServiceSecurityMode As Integer, _
                    ByRef iClientCredentialType As Integer, ByRef iProxyCredentialType As Integer, _
                    ByRef vNombreUsu As Variant, ByRef vContrasenyaUsu As Variant)
   Dim sConsulta As String
   Dim adoRes As New adodb.Recordset
   Dim sPass As String
   Dim sFecPass As String
   
   On Error GoTo Error:
        sConsulta = "SELECT SERVICEBINDINGTYPE,SERVICESECURITYMODE,SERVICECLIENTCREDENTIALTYPE,SERVICEPROXYCREDENTIALTYPE,TIE.WCF_ORIG_USU,TIE.WCF_ORIG_PWD,TIE.WCF_ORIG_FECPWD "
        sConsulta = sConsulta & "FROM  " & sFSG & "PARGEN_INTEGRACION P WITH(NOLOCK) "
        sConsulta = sConsulta & "INNER JOIN " & sFSG & "TABLAS_INTEGRACION_ERP TIE WITH(NOLOCK) ON TIE.ERP=P.ERP "
        sConsulta = sConsulta & "WHERE TIE.TABLA=" & iEntidadIntegracion & " AND TIE.ERP=" & iErp
        adoRes.Open sConsulta, con.AdoCon, adOpenForwardOnly, adLockReadOnly
        
        If Not adoRes.eof Then
            iServiceBindingType = IIf(IsNull(adoRes.Fields("SERVICEBINDINGTYPE")), "", adoRes.Fields("SERVICEBINDINGTYPE").Value)
            iServiceSecurityMode = IIf(IsNull(adoRes.Fields("SERVICESECURITYMODE")), "", adoRes.Fields("SERVICESECURITYMODE").Value)
            iClientCredentialType = IIf(IsNull(adoRes.Fields("SERVICECLIENTCREDENTIALTYPE")), "", adoRes.Fields("SERVICECLIENTCREDENTIALTYPE").Value)
            iProxyCredentialType = IIf(IsNull(adoRes.Fields("SERVICEPROXYCREDENTIALTYPE")), "", adoRes.Fields("SERVICEPROXYCREDENTIALTYPE").Value)
            vNombreUsu = IIf(IsNull(adoRes.Fields("WCF_ORIG_USU")), "", adoRes.Fields("WCF_ORIG_USU").Value)
            sPass = IIf(IsNull(adoRes.Fields("WCF_ORIG_PWD")), "", adoRes.Fields("WCF_ORIG_PWD").Value)
            sFecPass = IIf(IsNull(adoRes.Fields("WCF_ORIG_FECPWD")), "", adoRes.Fields("WCF_ORIG_FECPWD").Value)
            'Obtenemos la semilla para desencriptar la contrase�a.
            vContrasenyaUsu = EncriptarAES(vNombreUsu, sPass, False, sFecPass, 1, TipoDeUsuario.Persona)
        Else
            GoTo Error
        End If
        adoRes.Close
        Set adoRes = Nothing
    Exit Sub
    
Error:
    adoRes.Close
    Set adoRes = Nothing
End Sub

''' <summary>
''' Funci�n que encripta un string que se le pasa como par�metro.
''' </summary>
''' <param name="Dato">Dato a encriptar.</param>
''' <returns>El string encriptado.</returns>
''' <remarks>Llamada desde: CRaiz.Encriptar ; Tiempo m�ximo: 0,1</remarks>
Public Function EncriptarADM(ByVal Dato As String) As String
    Dim fechahoracrypt As Date
    Dim diacrypt
    Dim oCrypt2 As CCrypt2

    fechahoracrypt = DateSerial(1974, 3, 5)
    
    Set oCrypt2 = New CCrypt2
    
    oCrypt2.InBuffer = Dato
    
    oCrypt2.Codigo = ClaveADMimp
    
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    oCrypt2.Encrypt
    
    EncriptarADM = oCrypt2.OutBuffer
                            
    Set oCrypt2 = Nothing
End Function

Public Function DecodeBase64(ByVal strData As String) As Byte()

    Dim objXML As Object
    Dim objNode As Object
   
    ' help from MSXML
    Set objXML = CreateObject("MSXML2.DOMDocument")
    Set objNode = objXML.createElement("b64")
    objNode.DataType = "bin.base64"
    objNode.Text = strData
    DecodeBase64 = objNode.nodeTypedValue
   
    ' thanks, bye
    Set objNode = Nothing
    Set objXML = Nothing

End Function

Public Function EncodeBase64(ByRef arrData() As Byte) As String
    Dim objXML As Object
    Dim objNode As Object
    
    Set objXML = CreateObject("MSXML2.DOMDocument")
   
    ' byte array to base64
    Set objNode = objXML.createElement("b64")
    objNode.DataType = "bin.base64"
    objNode.nodeTypedValue = arrData
    EncodeBase64 = objNode.Text

    ' thanks, bye
    Set objNode = Nothing
    Set objXML = Nothing
End Function

''' <summary>
''' Lee el par�metro de ruta del FSNWebService de BD Portal y si esta vacio de BD GS
''' </summary>
''' <param name="con">ADO conexion</param>
''' <remarks>Llamada desde: CAdjunto CEspecificacion; Tiempo m�ximo: 0,0</remarks>
''' Revisado por: epb; Fecha: 31/08/2015
Public Function ObtenerRutaWebService(ByVal con As CConexion) As String
    Dim adoRes As New adodb.Recordset
    Dim sConsulta As String
    Dim sRuta As String
    
    sConsulta = "SELECT URL_WEBSERVICE FROM PARGEN_INTERNO WITH(NOLOCK) WHERE ID=1"
    adoRes.Open sConsulta, con.AdoCon, adOpenForwardOnly, adLockReadOnly
    If Not adoRes.eof Then
        sRuta = IIf(IsNull(adoRes.Fields("URL_WEBSERVICE")), "", adoRes.Fields("URL_WEBSERVICE").Value)
    End If
    If sRuta = "" Then
        sRuta = ObtenerRutaWebServiceGS(con)
    End If
    ObtenerRutaWebService = sRuta
    
    adoRes.Close
    Set adoRes = Nothing

End Function
''' <summary>
''' Lee el par�metro de ruta del FSNWebService de BD de GS
''' </summary>
''' <param name="con">ADO conexion</param>
''' <remarks>Llamada desde: CAdjunto CEspecificacion; Tiempo m�ximo: 0,0</remarks>
''' Revisado por: epb; Fecha: 31/08/2015
Public Function ObtenerRutaWebServiceGS(ByVal con As CConexion) As String
    Dim adoRes As New adodb.Recordset
    Dim sConsulta As String
    Dim sFSG As String
    
On Error GoTo Error:
    sFSG = Fsgs(con, 1)
    sConsulta = "SELECT URL_WEBSERVICE FROM " & sFSG & "PARGEN_INTERNO WITH(NOLOCK) WHERE ID=1"
    adoRes.Open sConsulta, con.AdoCon, adOpenForwardOnly, adLockReadOnly
    If Not adoRes.eof Then
        ObtenerRutaWebServiceGS = IIf(IsNull(adoRes.Fields("URL_WEBSERVICE")), "", adoRes.Fields("URL_WEBSERVICE").Value)
    Else
        GoTo Error:
    End If
    
    adoRes.Close
    Set adoRes = Nothing
    Exit Function
    
Error:
    Set adoRes = Nothing
    ObtenerRutaWebServiceGS = ""
End Function

''' <summary>
''' Aplicamos un atributo a un precio.
''' </summary>
''' <param name="oAtrib">Atributo a aplicar</param>
''' <param name="dblValorNum">Valor n�merico a aplicar</param>
''' <param name="dlbImporte">Importe sobre el que aplicar el precio</param>
''' <returns>Nuevo importe con el atributo aplicado</returns>
''' <remarks>Llamada desde: RealizarRecalculoOferta</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Public Function AplicamosAtributo(ByVal oAtrib As CAtributo, ByVal dblImporte As Double, ByVal dblValorNum As Double) As Double
    'Aplicamos el atributo al importe pasado
    Select Case oAtrib.Operacion
        Case "+"
            dblImporte = dblImporte + dblValorNum
            
        Case "-"
            dblImporte = dblImporte - dblValorNum
            
        Case "/"
            If dblValorNum <> 0 Then dblImporte = dblImporte / dblValorNum
            
        Case "*"
            dblImporte = dblImporte * dblValorNum
            
        Case "+%"
            dblImporte = dblImporte + (dblImporte * (dblValorNum / 100))
            
        Case "-%"
            dblImporte = dblImporte - (dblImporte * (dblValorNum / 100))
    End Select
    
    AplicamosAtributo = dblImporte

End Function
