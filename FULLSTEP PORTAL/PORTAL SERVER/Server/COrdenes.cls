VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COrdenes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private moConexion As CConexion

Private mCol As Collection
Friend Property Set Conexion(ByVal oCon As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set moConexion = oCon
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = moConexion
    
End Property

''' <summary>
''' Buscar todas las ordenes de una compania
''' </summary>
''' <param name="CiaComp">C�digo de compania</param>
''' <param name="Idioma">Idioma</param>
''' <param name="Estado">Estado</param>
''' <param name="desdeId">orden de entrega >=</param>
''' <param name="CodAprobador">Aprobador</param>
''' <param name="CodArt">Codigo Art�culo</param>
''' <param name="DenArt">Denominaci�n Art�culo</param>
''' <param name="CodExt">Codigo externo Art�culo</param>
''' <param name="desdefecha">desde fecha</param>
''' <param name="hastafecha">hasta fecha</param>
''' <param name="ProveCod">Codigo Proveedor</param>
''' <param name="ProveDen">Denominaci�n Proveedor</param>
''' <param name="numpedprove">Codigo externo orden</param>
''' <param name="Anyo">A�o</param>
''' <param name="Pedido">Pedido</param>
''' <param name="Orden">Numero Orden de entrega</param>
''' <param name="iCargaMaximaLineas">maximo numero de ordenes a mostrar</param>
''' <param name="RecIncorrect">Recepci�n incorrecta</param>
''' <param name="Referencia">Numero pedido Erp</param>
''' <param name="CoincidenciaTotal">true se hace like %x% or x% or %x false se hace like %x%</param>
''' <param name="Empresa">Empresa</param>
''' <param name="Receptor">Receptor</param>
''' <param name="CodErp">Codigo Proveedor Erp</param>
''' <param name="Entidad">Entidad</param>
''' <param name="IDOrden">Id Orden de entrega</param>
''' <param name="tipo">(Tipo=1 aprovisionamiento) (Tipo=0 directos) eoc pedidos directos y de aprovisionamiento</param>
''' <param name="bSoloPedidosAbiertos">1 muestra �nicamente pedidos abiertos, 0 muestra pedidos no Abiertos</param>
''' <param name="sEstadosAbiertos">lista de estados de pedido abierto separadas de comas por los que se desea filtrar</param>
''' <param name="AnyoAbierto">A�o del pedido abierto del que proceden los pedidos a buscar</param>
''' <param name="PedidoAbierto">Pedido del pedido abierto del que proceden los pedidos a buscar</param>
''' <param name="Orden">Numero Orden de entrega del pedido abierto del que proceden los pedidos a buscar</param>
''' <returns>las ordenes de una compania</returns>
''' <remarks>Llamada desde: buscarpedidos.asp   pedidos11dir.asp    pedidos21.asp    pedidos22.asp  pedidos41.asp
'''       rptpedidodetalle.asp    rptpedidos.asp    rptpedido.asp    seguimclt41.asp; Tiempo m�ximo: 0,1</remarks>
Public Function BuscarTodasOrdenes(ByVal CiaComp As Long, ByVal Idioma As String, ByVal EmpresaPortal As Boolean, ByVal NomPortal As String, _
        Optional ByVal Estado As Long, Optional ByVal desdeId As Variant, Optional ByVal CodAprobador As Variant, _
        Optional ByVal CodAprovisionador As Variant, Optional ByVal CodArt As Variant, _
        Optional ByVal DenArt As Variant, Optional ByVal CodExt As Variant, _
        Optional ByVal desdefecha As Variant, Optional ByVal hastafecha As Variant, _
        Optional ByVal ProveCod As Variant, Optional ByVal ProveDen As Variant, _
        Optional ByVal numpedprove As Variant, Optional ByVal Anyo As Variant, _
        Optional ByVal Pedido As Variant, Optional ByVal Orden As Variant, _
        Optional ByVal iCargaMaximaLineas As Variant, Optional ByVal RecIncorrect As Long, _
        Optional ByVal Referencia As Variant, Optional ByVal CoincidenciaTotal As Integer, _
        Optional ByVal Empresa As Variant, Optional ByVal Receptor As Variant, _
        Optional ByVal CodErp As Variant, Optional ByVal Entidad As Variant, _
        Optional ByVal IdOrden As Variant, Optional ByVal Tipo As Variant, _
        Optional ByVal Pendientes As Integer = 0, Optional ByVal Recep As Integer = 0, _
        Optional ByVal CentroCoste As Variant, Optional ByVal PartidaPresup As Variant, _
        Optional ByVal bSoloPedidosAbiertos As Boolean, Optional ByVal sEstadosAbiertos As String, Optional ByVal AnyoAbierto As Variant, _
        Optional ByVal CestaAbierto As Variant, Optional ByVal NumPedAbierto As Variant) As adodb.Recordset
    Dim ador As adodb.Recordset
    Dim adoCom As adodb.Command
    Dim oParam As adodb.Parameter
    Dim BD As String

    sFSG = Fsgs(moConexion, CiaComp)
    Idioma = EsIdiomaValido(Idioma)
    
    'Ejecuta el stored procedure FSEP_DEVOLVER_TODAS_ORDENES
    Set adoCom = New adodb.Command
    Set adoCom.ActiveConnection = moConexion.AdoCon
    adoCom.CommandText = sFSG & "FSEP_DEVOLVER_TODAS_ORDENES"
    adoCom.CommandType = adCmdStoredProc
    
    If IsMissing(iCargaMaximaLineas) Or IsNull(iCargaMaximaLineas) Then
        Set oParam = adoCom.CreateParameter("MAXLINEAS", adInteger, adParamInput, , 10000)
    Else
        Set oParam = adoCom.CreateParameter("MAXLINEAS", adInteger, adParamInput, , iCargaMaximaLineas)
    End If
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("DESDEID", adVarChar, adParamInput, 50, desdeId)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("APROBADOR", adVarChar, adParamInput, 50, CodAprobador)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("APROVISIONADOR", adVarChar, adParamInput, 50, CodAprovisionador)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("ARTINT", adVarChar, adParamInput, 50, CodArt)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("ARTDEN", adVarChar, adParamInput, 500, DenArt)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("ARTEXT", adVarChar, adParamInput, 500, CodExt)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("DESDEFECHA", adVarChar, adParamInput, 10, desdefecha)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("HASTAFECHA", adVarChar, adParamInput, 10, hastafecha)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("PROVECOD", adVarChar, adParamInput, 50, ProveCod)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("PROVEDEN", adVarChar, adParamInput, 500, ProveDen)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("NUMPEDPROVE", adVarChar, adParamInput, 100, numpedprove)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("ANYO", adInteger, adParamInput, , Anyo)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("PEDIDO", adInteger, adParamInput, , Pedido)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("ORDEN", adInteger, adParamInput, , Orden)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("EST", adInteger, adParamInput, , Estado)
    adoCom.Parameters.Append oParam
    If IsMissing(IdOrden) Then
        Set oParam = adoCom.CreateParameter("ID", adInteger, adParamInput, , Null)
    Else
        Set oParam = adoCom.CreateParameter("ID", adInteger, adParamInput, , IdOrden)
    End If
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("RECINCORRECTO", adInteger, adParamInput, , RecIncorrect)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("COINCIDENCIA", adInteger, adParamInput, , CoincidenciaTotal)
    adoCom.Parameters.Append oParam
    If Not IsMissing(Referencia) Then
        Set oParam = adoCom.CreateParameter("REFERENCIA", adVarChar, adParamInput, 100, Referencia)
    Else
        Set oParam = adoCom.CreateParameter("REFERENCIA", adVarChar, adParamInput, 100, Null)
    End If
    adoCom.Parameters.Append oParam
    
    Set oParam = adoCom.CreateParameter("CAMPO1", adInteger, adParamInput, , Null)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("CAMPO2", adInteger, adParamInput, , Null)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("VALORCAMPO1", adVarChar, adParamInput, 150, Null)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("VALORCAMPO2", adVarChar, adParamInput, 150, Null)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("CATN1", adInteger, adParamInput, , Null)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("CATN2", adInteger, adParamInput, , Null)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("CATN3", adInteger, adParamInput, , Null)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("CATN4", adInteger, adParamInput, , Null)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("CATN5", adInteger, adParamInput, , Null)
    adoCom.Parameters.Append oParam
    
    If Not IsMissing(Empresa) Then
        Set oParam = adoCom.CreateParameter("EMPRESA", adInteger, adParamInput, , Empresa)
    Else
        Set oParam = adoCom.CreateParameter("EMPRESA", adInteger, adParamInput, , Null)
    End If
    adoCom.Parameters.Append oParam
    
    If Not IsMissing(Receptor) Then
        Set oParam = adoCom.CreateParameter("RECEPTOR", adVarChar, adParamInput, 100, Receptor)
    Else
        Set oParam = adoCom.CreateParameter("RECEPTOR", adVarChar, adParamInput, 100, Null)
    End If
    adoCom.Parameters.Append oParam
    If Not IsMissing(CodErp) Then
        Set oParam = adoCom.CreateParameter("COD_ERP", adVarChar, adParamInput, 100, CodErp)
    Else
        Set oParam = adoCom.CreateParameter("COD_ERP", adVarChar, adParamInput, 100, Null)
    End If
    adoCom.Parameters.Append oParam
    If Not IsMissing(Entidad) Then
        Set oParam = adoCom.CreateParameter("ENTIDAD", adVarChar, adParamInput, 100, Entidad)
    Else
        Set oParam = adoCom.CreateParameter("ENTIDAD", adVarChar, adParamInput, 100, "ORDEN")
    End If
    adoCom.Parameters.Append oParam
    If IsMissing(Tipo) Or IsNull(Tipo) Then
        Set oParam = adoCom.CreateParameter("TIPO", adInteger, adParamInput, , Null)  'Carga pedidos directos y de aprovisionamiento
    Else
        Set oParam = adoCom.CreateParameter("TIPO", adInteger, adParamInput, , Tipo)  'Carga s�lo pedidos (Tipo=1 aprovisionamiento) (Tipo=0 directos)
    End If
    adoCom.Parameters.Append oParam
    
    Set oParam = adoCom.CreateParameter("IDIOMA", adVarChar, adParamInput, 20, Idioma)
    adoCom.Parameters.Append oParam

    If Pendientes <> 0 Then
        Set oParam = adoCom.CreateParameter("PENDIENTES", adTinyInt, adParamInput, , 1)
        adoCom.Parameters.Append oParam
    Else
        Set oParam = adoCom.CreateParameter("PENDIENTES", adTinyInt, adParamInput, , 0)
        adoCom.Parameters.Append oParam
    End If
    If Recep <> 0 Then
        Set oParam = adoCom.CreateParameter("RECEP", adTinyInt, adParamInput, , 1)
        adoCom.Parameters.Append oParam
    Else
        Set oParam = adoCom.CreateParameter("RECEP", adTinyInt, adParamInput, , 0)
        adoCom.Parameters.Append oParam
    End If
    Set oParam = adoCom.CreateParameter("CCOSTE", adVarChar, adParamInput, 50, IIf(IsMissing(CentroCoste) Or IsNull(CentroCoste), Null, CentroCoste))
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("PPRESUP", adVarChar, adParamInput, 50, IIf(IsMissing(PartidaPresup) Or IsNull(PartidaPresup), Null, PartidaPresup))
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("SOLOABIERTOS", adTinyInt, adParamInput, , BooleanToSQLBinary(bSoloPedidosAbiertos))
    adoCom.Parameters.Append oParam
    
    Set oParam = adoCom.CreateParameter("ESTADOSABIERTOS", adVarChar, adParamInput, 100, sEstadosAbiertos)
    adoCom.Parameters.Append oParam
    
    Set oParam = adoCom.CreateParameter("ANYO_PED_ABIERTO", adInteger, adParamInput, , AnyoAbierto)
    adoCom.Parameters.Append oParam

    Set oParam = adoCom.CreateParameter("PEDIDO_PED_ABIERTO", adInteger, adParamInput, , CestaAbierto)
    adoCom.Parameters.Append oParam

    Set oParam = adoCom.CreateParameter("ORDEN_PED_ABIERTO", adInteger, adParamInput, , NumPedAbierto)
    adoCom.Parameters.Append oParam
    
    Set adoPar = adoCom.CreateParameter("NOM_PORTAL", adVarChar, adParamInput, 50, IIf(EmpresaPortal, NomPortal, Null))
    adoCom.Parameters.Append adoPar
    
    Set ador = adoCom.Execute

    If ador.eof Then
        ador.Close
        Set BuscarTodasOrdenes = Nothing
    Else
        Set adoCom.ActiveConnection = Nothing
        Set BuscarTodasOrdenes = ador
    End If
    
    Set ador = Nothing
    Set adoCom = Nothing
End Function

Public Function BuscarTodasIDOrdenes(ByVal CiaComp As Long, Optional ByVal Estado As Long, _
                    Optional ByVal CodAprobador As Variant, Optional ByVal CodAprovisionador As Variant, _
                    Optional ByVal CodArt As Variant, Optional ByVal DenArt As Variant, Optional ByVal CodExt As Variant, _
                    Optional ByVal desdefecha As Variant, Optional ByVal hastafecha As Variant, _
                    Optional ByVal ProveCod As Variant, Optional ByVal ProveDen As Variant, _
                    Optional ByVal numpedprove As Variant, Optional ByVal Anyo As Variant, _
                    Optional ByVal Pedido As Variant, Optional ByVal Orden As Variant) As adodb.Recordset


Dim ador As adodb.Recordset
Dim adoCom As adodb.Command
Dim oParam As adodb.Parameter
Dim BD As String

    sFSG = Fsgs(moConexion, CiaComp)


    'Ejecuta el stored procedure FSEP_DEVOLVER_TODAS_ID_ORDENES
    Set adoCom = New adodb.Command
    Set adoCom.ActiveConnection = moConexion.AdoCon
    adoCom.CommandText = sFSG & "FSEP_DEVOLVER_TODAS_ID_ORDENES"
    adoCom.CommandType = adCmdStoredProc
    
    Set oParam = adoCom.CreateParameter("MAXLINEAS", adInteger, adParamInput, , 10000)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("APROBADOR", adVarChar, adParamInput, 50, CodAprobador)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("APROVISIONADOR", adVarChar, adParamInput, 50, CodAprovisionador)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("ARTCOD", adVarChar, adParamInput, 50, CodArt)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("ARTDEN", adVarChar, adParamInput, 500, DenArt)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("ARTEXT", adVarChar, adParamInput, 500, CodExt)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("DESDEFECHA", adVarChar, adParamInput, 10, desdefecha)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("HASTAFECHA", adVarChar, adParamInput, 10, hastafecha)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("PROVECOD", adVarChar, adParamInput, 50, ProveCod)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("PROVEDEN", adVarChar, adParamInput, 500, ProveDen)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("NUMPEDPROVE", adVarChar, adParamInput, 100, numpedprove)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("ANYO", adInteger, adParamInput, , Anyo)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("PEDIDO", adInteger, adParamInput, , Pedido)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("ORDEN", adInteger, adParamInput, , Orden)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("EST", adInteger, adParamInput, , Estado)
    adoCom.Parameters.Append oParam
    
    adoCom.Prepared = True

    Set ador = adoCom.Execute

    If ador.eof Then
        ador.Close
        Set ador = Nothing
        Set adoCom = Nothing
        Set BuscarTodasIDOrdenes = Nothing
        Exit Function
    Else
        'Devuelve un recordset desconectado
        Set adoCom.ActiveConnection = Nothing
        Set adoCom = Nothing
        Set BuscarTodasIDOrdenes = ador
    End If


End Function


Public Function Add(ByVal id As Long, ByVal Pedido As Long, ByVal Anyo As Long, ByVal Prove As String, ByVal Num As Long, ByVal Est As Long, ByVal Tipo As Long, ByVal Fecha As Variant, ByVal Importe As Double, Optional ByVal NumExt As Variant, Optional ByVal OrdenEst As Variant, Optional ByVal NumPedido As Long, Optional ByVal varIndice As Variant, Optional ByVal bAbono As Boolean = False) As COrden
    'create a new object
    Dim objnewmember As COrden
    
    Set objnewmember = New COrden
   
    objnewmember.id = id
    objnewmember.PedidoId = Pedido
    objnewmember.Anyo = Anyo
    objnewmember.Prove = Prove
    objnewmember.Num = Num
    objnewmember.Est = Est
    objnewmember.Tipo = Tipo
    objnewmember.Fecha = Fecha
    objnewmember.Importe = Importe
    objnewmember.NumPedido = NumPedido
    objnewmember.Abono = bAbono
    If Not IsMissing(NumExt) And Not IsNull(NumExt) Then
        objnewmember.NumExt = CStr(NumExt)
    End If
    
    If Not IsMissing(OrdenEst) And Not IsNull(OrdenEst) Then
        objnewmember.OrdenEst = CInt(OrdenEst)
    End If
    
    Set objnewmember.Conexion = moConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        mCol.Add objnewmember, CStr(id)
    End If
      
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property

Private Sub Class_Initialize()
    ''' * Objetivo: Crear la coleccion
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    Set mCol = Nothing
    Set moConexion = Nothing
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property

''' <summary>Devuelve los centros de coste para los que el proveedor tiene pedidos</summary>
''' <param name="lCiaComp">CIA</param>
''' <param name="lIdProve">Id del proveedor en el portal</param>
''' <param name="sIdioma">Idioma</param>
''' <returns>string</returns>
''' <remarks>Llamada desde: seguimctl.asp; Tiempo m�ximo: 0,1</remarks>
''' <revision>LTG 22/08/2011</revision>

Public Function DevolverCentrosCostePedidosProveedor(ByVal lCiaComp As Long, ByVal lIdProve As Long, ByVal sIdioma As String) As adodb.Recordset
    Dim oRaiz As CRaiz
    Dim oComm As adodb.Command
    Dim oParam As adodb.Parameter
    Dim rsCentros As adodb.Recordset
    Dim sConsulta As String
    Dim sFSG  As String
    Dim sProveCod As String
    
    'Obtener el c�digo del proveedor del GS
    Set oRaiz = New CRaiz
    oRaiz.Conectar gInstancia
    sProveCod = oRaiz.DevolverCodProveedorGS(lIdProve, lCiaComp)
    
    sFSG = Fsgs(moConexion, lCiaComp)
    
    
    Set oComm = New adodb.Command
    With oComm
        Set .ActiveConnection = moConexion.AdoCon
        .CommandText = sFSG & "FSP_CARGARCENTROSCOSTEPROVE"
        .CommandType = adCmdStoredProc
        
        Set oParam = .CreateParameter("PROVE", adVarChar, adParamInput, 50, sProveCod)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("IDI", adVarChar, adParamInput, 20, sIdioma)
        .Parameters.Append oParam
    
        Set rsCentros = .Execute
    End With
        
    Set DevolverCentrosCostePedidosProveedor = rsCentros
    
    Set rsCentros = Nothing
    Set oParam = Nothing
    Set oComm = Nothing
    Set oRaiz = Nothing
End Function

''' <summary>Devuelve las partidas para las que el proveedor tiene pedidos</summary>
''' <param name="lCiaComp">CIA</param>
''' <param name="lIdProve">Id del proveedor en el portal</param>
''' <param name="sIdioma">Idioma</param>
''' <param name="sCentroCoste">Centro de Coste</param>
''' <returns>string</returns>
''' <remarks>Llamada desde: seguimctl.asp; Tiempo m�ximo: 0,1</remarks>
''' <revision>LTG 22/08/2011</revision>

Public Function DevolverPartidasPedidosProveedor(ByVal lCiaComp As Long, ByVal lIdProve As Long, ByVal sIdioma As String, Optional ByVal sCentroCoste As String) As adodb.Recordset
    Dim oRaiz As CRaiz
    Dim oComm As adodb.Command
    Dim oParam As adodb.Parameter
    Dim rsPartidas As adodb.Recordset
    Dim sConsulta As String
    Dim sFSG  As String
    Dim sProveCod As String
    
    'Obtener el c�digo del proveedor del GS
    Set oRaiz = New CRaiz
    oRaiz.Conectar gInstancia
    sProveCod = oRaiz.DevolverCodProveedorGS(lIdProve, lCiaComp)
    
    sFSG = Fsgs(moConexion, lCiaComp)
    
    Set oComm = New adodb.Command
    With oComm
        Set .ActiveConnection = moConexion.AdoCon
        .CommandText = sFSG & "FSP_CARGARPARTIDASPROVE"
        .CommandType = adCmdStoredProc
        
        Set oParam = .CreateParameter("PROVE", adVarChar, adParamInput, 50, sProveCod)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("IDI", adVarChar, adParamInput, 50, sIdioma)
        .Parameters.Append oParam
    
        Set rsPartidas = .Execute
    End With
        
    Set DevolverPartidasPedidosProveedor = rsPartidas
    
    Set rsPartidas = Nothing
    Set oParam = Nothing
    Set oComm = Nothing
    Set oRaiz = Nothing
End Function

''' <summary>Devuelve si la imputaci�n se realiza a nivel de cabecera de pedido o de l�neas (0 cabecera, 1 l�neas)</summary>
''' <returns>entero</returns>
''' <remarks>Llamada desde: seguimctl41.asp; Tiempo m�ximo: 0,1</remarks>

Public Function DevolverModoImputacion(ByVal lCiaComp As Long) As Integer
    Dim oComm As adodb.Command
    Dim rsIMP As adodb.Recordset
    Dim sConsulta As String
    Dim sFSG  As String
    
    sFSG = Fsgs(moConexion, lCiaComp)
       
    sConsulta = "SELECT TOP 1 IMPMODO"
    sConsulta = sConsulta & " FROM " & sFSG & "PARGEN_SM"

    Set oComm = New adodb.Command
    With oComm
        Set .ActiveConnection = moConexion.AdoCon
        .CommandType = adCmdText
        .CommandText = sConsulta
    
        Set rsIMP = .Execute
    End With
    
    If Not rsIMP.eof Then
        DevolverModoImputacion = rsIMP("IMPMODO").Value
    End If
    
    Set rsIMP = Nothing
    Set oComm = Nothing
End Function


''' <summary>
''' Carga el unico �rbol de la PARGEN_SM (desde Portal se supone q solo hay uno)
''' </summary>
''' <param name="lCiaComp">ID de la compania</param>
''' <param name="Pres5">Centro de coste</param>
''' <param name="ImpModoCabecera">Si imputa en Cabecera/Lineas</param>
''' <param name="Imputable">Si NoImputa � imputa (Opcional/Obligatorio)</param>
''' <remarks>Llamada desde:CExcel.GenerarHojaExcel</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Public Sub CargarElArbolSM(ByVal lCiaComp As Long, ByRef Pres5 As String, ByRef ImpModoCabecera As Boolean, ByRef Imputable As Boolean)

    Dim sConsulta As String
    Dim adoRes As adodb.Recordset
    Dim adoComm As adodb.Command
     
    Dim sFSG As String
        
    ''' Precondicion
    If moConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "COrdenes.CargarElArbolSM", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    Set adoComm = New adodb.Command
    
    sFSG = Fsgs(moConexion, lCiaComp)
    
    Set adoComm.ActiveConnection = moConexion.AdoCon
    adoComm.CommandText = "EXEC " & sFSG & "SP_DEVOLVER_PARGEN_SM"
    adoComm.CommandType = adCmdText
    
    Set adoRes = adoComm.Execute
    
    If adoRes.eof Then
        Pres5 = "''"
        ImpModoCabecera = True
        Imputable = False
    Else
        Pres5 = "'" & adoRes.Fields("PRES5") & "'"
        ImpModoCabecera = (adoRes.Fields("IMPMODO") = TipoModoImputacionSM.NivelCabecera)
        Imputable = (adoRes.Fields("IMP_PEDIDO") <> TipoImputacionSC.NoImputa)
    End If
    
    adoRes.Close
    Set adoRes = Nothing
                
End Sub



 
