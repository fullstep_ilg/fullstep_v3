VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUnidadesPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mCol As Collection
Private moConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
Set moConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub
Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set moConexion = Nothing
End Sub

Public Property Get Count() As Long

    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If

End Property

''' <summary>
''' Cargar la lista de unidades de articulo
''' </summary>
''' <param name="sIdioma">Idioma</param>
''' <param name="CiaComp">C�digo de compania</param>
''' <param name="cod">c�digo de unidad</param>
''' <returns>Lista de unidades de articulo</returns>
''' <remarks>Llamada desde: denominacionup.asp; Tiempo m�ximo: 0,1</remarks>
Public Function CargarDatosUnidad(ByVal sIdioma As String, ByVal CiaComp As Long, ByVal Cod As String) As ADODB.Recordset
    Dim ador As ADODB.Recordset
    Dim adoCom As ADODB.Command
    Dim adoPar As ADODB.Parameter
    Dim sConsulta As String
    Dim sFSG As String

    sFSG = FSGS(moConexion, CiaComp)
    
    sIdioma = EsIdiomaValido(sIdioma)
    
    'CALIDAD Resulta q no se puede poner WITH (NOLOCK) en esta sentencia pq esta preguntando por tablas de otro servidor
    'De poner WITH(NOLOCK) dar�a el siguiente error:
    '   Number : -2147217900
    '   Description: Cannot specify an index or locking hint for a remote data source.
    sConsulta = "SELECT UNI.COD,UNI_DEN.DEN AS DEN FROM " & sFSG & "UNI UNI LEFT JOIN " & sFSG & "UNI_DEN UNI_DEN ON UNI_DEN.UNI = UNI.COD AND UNI_DEN.IDIOMA='" & sIdioma & "' WHERE UNI.COD= ?"
    
    Set ador = New ADODB.Recordset
    Set adoCom = New ADODB.Command
            
    Set adoCom.ActiveConnection = moConexion.AdoCon
    
    Set adoPar = adoCom.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(Cod), Value:=Cod)
    adoCom.Parameters.Append adoPar
    
    adoCom.CommandText = sConsulta
    adoCom.CommandType = adCmdText
    Set ador = adoCom.Execute
            
    If ador.eof Then
        ador.Close
        Set ador = Nothing
        Set CargarDatosUnidad = Nothing
    Else
        ador.ActiveConnection = Nothing
        Set CargarDatosUnidad = ador
    End If

    Set adoPar = Nothing
    Set adoCom = Nothing
End Function


















