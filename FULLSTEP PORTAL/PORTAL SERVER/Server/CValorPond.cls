VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CValorPond"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private m_lIdAtributo As Long
Private m_vAnyoProce As Variant
Private m_vGMN1Proce  As Variant
Private m_vCodProce As Variant
Private m_vCodGrupo As Variant
Private m_lIdOrden As Long
Private m_vIndice As Variant
Private m_vValorLista As Variant
Private m_vValorPond As Variant
Private m_vValorDesde As Variant
Private m_vValorHasta As Variant
Private m_vFechaActualizacion As Variant

Private m_adores As adodb.Recordset

Private m_oConexion As CConexion
Private m_AdoRecordset As adodb.Recordset
Public Property Let IDAtributo(ByVal var As Long)
    m_lIdAtributo = var
End Property
Public Property Get IDAtributo() As Long
    IDAtributo = m_lIdAtributo
End Property
Public Property Let AnyoProce(ByVal var As Variant)
    m_vAnyoProce = var
End Property
Public Property Get AnyoProce() As Variant
    AnyoProce = m_vAnyoProce
End Property
Public Property Let GMN1Proce(ByVal var As Variant)
    m_vGMN1Proce = var
End Property
Public Property Get GMN1Proce() As Variant
    GMN1Proce = m_vGMN1Proce
End Property
Public Property Let CodProce(ByVal var As Variant)
    m_vCodProce = var
End Property
Public Property Get CodProce() As Variant
    CodProce = m_vCodProce
End Property
Public Property Let CodGrupo(ByVal var As Variant)
    m_vCodGrupo = var
End Property
Public Property Get CodGrupo() As Variant
    CodGrupo = m_vCodGrupo
End Property
Public Property Let IdOrden(ByVal var As Long)
    m_lIdOrden = var
End Property
Public Property Get IdOrden() As Long
    IdOrden = m_lIdOrden
End Property
Public Property Let Indice(ByVal varIndice As Variant)
    m_vIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property
Public Property Let ValorLista(ByVal var As Variant)
    m_vValorLista = var
End Property
Public Property Get ValorLista() As Variant
    ValorLista = m_vValorLista
End Property
Public Property Let ValorPond(ByVal var As Variant)
    m_vValorPond = var
End Property
Public Property Get ValorPond() As Variant
    ValorPond = m_vValorPond
End Property
Public Property Let ValorDesde(ByVal var As Variant)
    m_vValorDesde = var
End Property
Public Property Get ValorDesde() As Variant
    ValorDesde = m_vValorDesde
End Property
Public Property Let ValorHasta(ByVal var As Variant)
    m_vValorHasta = var
End Property
Public Property Get ValorHasta() As Variant
    ValorHasta = m_vValorHasta
End Property
Public Property Let FechaActualizacion(ByVal var As Variant)
    m_vFechaActualizacion = var
End Property
Public Property Get FechaActualizacion() As Variant
    FechaActualizacion = m_vFechaActualizacion
End Property
Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Private Sub Class_Terminate()
    
    Set m_AdoRecordset = Nothing
    Set m_oConexion = Nothing
   
End Sub
