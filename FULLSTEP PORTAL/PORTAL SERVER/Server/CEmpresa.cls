VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEmpresa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_lId As Long 'Nos indica la posicion secuencial dentro de una Coleccion
Private m_sNif As String
Private m_sDen As String
Private m_vDireccion As Variant
Private m_vPoblacion As Variant
Private m_vCP As Variant
Private m_vCodMon As Variant
Private m_vCodPais As Variant
Private m_vCodProvi As Variant
Private m_vDenPais As Variant
Private m_vDenProvi As Variant
Private m_bERP As Boolean

Private m_dtFecact As Date


Private m_oConexion As CConexion


'Private m_oUONS1 As CUnidadesOrgNivel1
'Private m_oUONS2 As CUnidadesOrgNivel2
'Private m_oUONS3 As CUnidadesOrgNivel3
'
'Public Property Get UONS1() As CUnidadesOrgNivel1
'Set UONS1 = m_oUONS1
'End Property
'
'Public Property Set UONS1(oUons1 As CUnidadesOrgNivel1)
'Set m_oUONS1 = oUons1
'End Property
'Public Property Get UONS2() As CUnidadesOrgNivel2
'Set UONS2 = m_oUONS2
'End Property
'
'Public Property Set UONS2(oUons2 As CUnidadesOrgNivel2)
'Set m_oUONS2 = oUons2
'End Property
'
'Public Property Get UONS3() As CUnidadesOrgNivel3
'Set UONS3 = m_oUONS3
'End Property
'
'Public Property Set UONS3(oUons3 As CUnidadesOrgNivel3)
'Set m_oUONS3 = oUons3
'End Property
'


Public Property Get Nif() As String
    Nif = m_sNif
End Property
Public Property Let Nif(ByVal sNif As String)
    m_sNif = sNif
End Property

Public Property Get Den() As String
    Den = m_sDen
End Property
Public Property Let Den(ByVal sDen As String)
    m_sDen = sDen
End Property

Public Property Get CP() As Variant
    CP = m_vCP
End Property
Public Property Let CP(ByVal Cod As Variant)
    m_vCP = Cod
End Property



Public Property Get ERP() As Boolean
    ERP = m_bERP
End Property
Public Property Let ERP(ByVal bERP As Boolean)
    m_bERP = bERP
End Property

Public Property Get CodPais() As Variant
    CodPais = m_vCodPais
End Property
Public Property Let CodPais(ByVal Cod As Variant)
    m_vCodPais = Cod
End Property
Public Property Get CodProvi() As Variant
    CodProvi = m_vCodProvi
End Property
Public Property Let CodProvi(ByVal Cod As Variant)
    m_vCodProvi = Cod
End Property

Public Property Get DenPais() As Variant
    DenPais = m_vDenPais
End Property
Public Property Let DenPais(ByVal Den As Variant)
    m_vDenPais = Den
End Property
Public Property Get DenProvi() As Variant
    DenProvi = m_vDenProvi
End Property
Public Property Let DenProvi(ByVal Den As Variant)
    m_vDenProvi = Den
End Property

Public Property Get Direccion() As Variant
    Direccion = m_vDireccion
End Property
Public Property Let Direccion(ByVal Cod As Variant)
    m_vDireccion = Cod
End Property
Public Property Get Poblacion() As Variant
    Poblacion = m_vPoblacion
End Property
Public Property Let Poblacion(ByVal Cod As Variant)
    m_vPoblacion = Cod
End Property

Public Property Get ID() As Long
    ID = m_lId
End Property
Public Property Let ID(ByVal lId As Long)
    m_lId = lId
End Property
Public Property Get FecAct() As Date
    FecAct = m_dtFecact
End Property
Public Property Let FecAct(ByVal var As Date)
    m_dtFecact = var
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Private Sub Class_Terminate()
    
    Set m_oConexion = Nothing
    
End Sub

