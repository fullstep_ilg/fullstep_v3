VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CItems"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CItems **********************************
'*             Autor : Javier Arana
'*             Creada : 22/2/98
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private moConexion As CConexion
Private mvarEOF As Boolean

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CItem
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set moConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

''' <summary>
''' Add de la coleccion de items
''' </summary>
''' <param name="oProce">Proceso</param><param name="oGrupo">Grupo</param><param name="id">Id</param>
''' <param name="Dest">Destino</param><param name="Uni">Unidad</param><param name="Pag">Pgo</param>
''' <param name="DenPag">Den de pago</param><param name="Cant">Cantidad</param><param name="FecIni">FecIni</param>
''' <param name="FecFin">Fecha inicio</param><param name="ArtiCod">ArtiCod</param><param name="descr">descr item</param>
''' <param name="Precio">precio</param><param name="Presupuesto">Presupuesto</param><param name="varIndice">Indice</param>
''' <param name="Confirmado">Confirmado</param><param name="Objetivo">Objetivo</param><param name="FecObj">fecha objetivo</param>
''' <param name="EspAdj">Hay especificaciones</param><param name="MinProveCod">MinProveCod</param><param name="MinProveDen">MinProveDen</param>
''' <param name="Usar">Especifica el precio alternativo que se utiliza en el �tem</param>
''' <returns>el elemento insertado</returns>
''' <remarks>Llamada desde: CargarItems; Tiempo m�ximo: 0,1</remarks>
''' <revisado>JVS 01/07/2011</revisado>
Public Function Add(ByVal oProce As CProceso, ByVal oGrupo As CGrupo, ByVal id As Integer, _
Optional ByVal Dest As Variant, Optional ByVal Uni As String, _
Optional ByVal Pag As String, Optional ByVal DenPag As String, _
Optional ByVal Cant As Variant, _
Optional ByVal FecIni As Date, Optional ByVal FecFin As Date, _
Optional ByVal ArtiCod As Variant, _
Optional ByVal descr As String, _
Optional ByVal Precio As Variant, Optional ByVal Presupuesto As Variant, _
Optional ByVal varIndice As Variant, Optional ByVal Confirmado As Boolean, _
Optional ByVal Objetivo As Variant, _
Optional ByVal FecObj As Variant, _
Optional ByVal EspAdj As Variant, _
Optional ByVal MinProveCod As Variant, Optional ByVal MinProveDen As Variant, Optional ByVal MinPrecio As Variant, _
Optional ByVal vCantidadOferta As Variant, Optional ByVal vPrecioOferta As Variant, _
Optional ByVal vPrecio2 As Variant, Optional ByVal vPrecio3 As Variant, _
Optional ByVal vNumAdjun As Variant, _
Optional ByVal vComent1 As Variant, Optional ByVal vComent2 As Variant, _
Optional ByVal vComent3 As Variant, Optional ByVal bPuja As Boolean, Optional ByVal vEsp As Variant, Optional ByVal vDenUni As Variant, _
Optional ByVal vObsAdjun As Variant, _
Optional ByVal EspAdjI As Variant, Optional ByVal vPosicionPuja As Variant, Optional ByVal vPrecioNeto As Variant, Optional ByVal vImporteNeto As Variant, _
Optional ByVal PrecSalida As Variant, Optional MinPujGanador As Variant, Optional MinPujProve As Variant, Optional vUsar As Variant, Optional iAnyoImputacion As Integer) As CItem
    
    'create a new object
    Dim sCod As String
    Dim objnewmember As CItem
    Set objnewmember = New CItem
    With objnewmember
    
        Set .Conexion = moConexion
        Set .Proceso = oProce
        Set .Grupo = oGrupo
        .id = id
        .UniCod = Uni
        
        If IsMissing(vDenUni) Then
           .UniDen = ""
        Else
            .UniDen = vDenUni
        End If
        
        .PagCod = Pag
        .PagDen = DenPag
        .DestCod = Dest
        .FechaInicioSuministro = FecIni
        If oProce.MostrarFinSum Then
            .FechaFinSuministro = FecFin
        End If
        .Cantidad = Cant
        .Confirmado = Confirmado
        
        Set .Conexion = moConexion
        
        If IsMissing(Objetivo) Then
            .Objetivo = Null
        Else
            .Objetivo = Objetivo
        End If
        
        If IsMissing(ArtiCod) Then
            .ArticuloCod = Null
        Else
            .ArticuloCod = ArtiCod
        End If
        
        If IsMissing(descr) Then
            .descr = Null
        Else
            .descr = descr
        End If
        
        If IsMissing(Precio) Then
            .Precio = Null
        Else
            .Precio = Precio
        End If
        
        If IsMissing(Presupuesto) Then
            .Presupuesto = Null
        Else
            .Presupuesto = Presupuesto
        End If
        
        If IsMissing(FecObj) Then
            .FechaObjetivo = Null
        Else
            .FechaObjetivo = FecObj
        End If
        
        If IsMissing(EspAdj) Then
            .EspAdj = Null
        Else
            .EspAdj = EspAdj
        End If
        
        'jpa Tarea 540
        If IsMissing(EspAdjI) Then
            .EspAdjI = Null
        Else
            .EspAdjI = EspAdjI
        End If
        'fin jpa Tarea 540
        
        
        If IsMissing(MinProveCod) Then
            .MinProveCod = Null
        Else
            .MinProveCod = MinProveCod
        End If
        
        If IsMissing(MinProveDen) Then
            .MinProveDen = Null
        Else
            .MinProveDen = MinProveDen
        End If
        
        If IsMissing(MinPrecio) Then
            .MinPrecio = Null
        Else
            .MinPrecio = MinPrecio
        End If
        
        If IsMissing(vEsp) Then
            .Esp = Null
        Else
            .Esp = vEsp
        End If
        
        If IsMissing(vPosicionPuja) Then
            .PosicionPuja = Null
        Else
            .PosicionPuja = vPosicionPuja
        End If
        
        
        If IsMissing(PrecSalida) Then
            .PrecioSalida = Null
        Else
            .PrecioSalida = PrecSalida
        End If
        
        If IsMissing(MinPujGanador) Then
            .MinPujGanador = Null
        Else
            .MinPujGanador = MinPujGanador
        End If
        
        If IsMissing(MinPujProve) Then
            .MinPujProve = Null
        Else
            .MinPujProve = MinPujProve
        End If
        
        If IsMissing(vUsar) Then
            .Usar = Null
        Else
            .Usar = vUsar
        End If
        
        

        
        
        If Not IsMissing(vCantidadOferta) Or Not IsMissing(vPrecioOferta) Or Not IsMissing(vPrecio2) Or Not IsMissing(vPrecio3) Or Not IsMissing(vPrecioNeto) Or Not IsMissing(vImporteNeto) Then
            Set .PrecioItem = New CPrecioItem
            With .PrecioItem
                If IsMissing(vCantidadOferta) Then
                    .Cantidad = Null
                Else
                    .Cantidad = vCantidadOferta
                End If
                If IsMissing(vPrecioOferta) Then
                    .PrecioOferta = Null
                    .PrecioGS = Null
                Else
                    .PrecioOferta = vPrecioOferta
                    .PrecioGS = vPrecioOferta
                    '.PosicionPuja = vPosicionPuja
                End If
                If IsMissing(vPrecio2) Then
                    .Precio2 = Null
                Else
                    .Precio2 = vPrecio2
                End If
                If IsMissing(vPrecio3) Then
                    .Precio3 = Null
                Else
                    .Precio3 = vPrecio3
                End If
                If Not IsMissing(vNumAdjun) Then
                    .NumAdjuntos = vNumAdjun
                End If
                If IsMissing(vComent1) Then
                    .Comentario1 = Null
                Else
                    .Comentario1 = vComent1
                End If
                
                If IsMissing(vComent2) Then
                    .Comentario2 = Null
                Else
                    .Comentario2 = vComent2
                End If
                
                If IsMissing(vComent3) Then
                    .Comentario3 = Null
                Else
                    .Comentario3 = vComent3
                End If
                
                If IsMissing(vPrecioNeto) Then
                    .PrecioNeto = Null
                Else
                    .PrecioNeto = vPrecioNeto
                    If Not IsNull(.Cantidad) Then
                        .ImporteBruto = .PrecioNeto * .Cantidad
                    Else
                        .ImporteBruto = .PrecioNeto * objnewmember.Cantidad
                    End If
                End If
                If IsMissing(vImporteNeto) Then
                    .ImporteNeto = Null
                Else
                    .ImporteNeto = vImporteNeto
                End If
                
                .ObsAdjuntos = vObsAdjun
            End With
        End If
        If IsMissing(iAnyoImputacion) Then
            .AnyoImputacion = Null
        Else
            .AnyoImputacion = iAnyoImputacion
        End If
        If bPuja Then
            Set .PujaItem = New CPrecioItem
            .PujaItem.PrecioOferta = vPrecioOferta
            '.PujaItem.PosicionPuja = vPosicionPuja
        End If
        If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
            .Indice = varIndice
            mCol.Add objnewmember, CStr(varIndice)
        Else
           mCol.Add objnewmember, CStr(id)
        End If
    
    End With
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Function AddItem(ByVal oItem As CItem)

    mCol.Add oItem

End Function



Public Sub Remove(vntIndexKey As Variant)
    
        mCol.Remove vntIndexKey
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
  

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'Itemroys collection when this class is terminated
    Set mCol = Nothing
    Set moConexion = Nothing
End Sub


