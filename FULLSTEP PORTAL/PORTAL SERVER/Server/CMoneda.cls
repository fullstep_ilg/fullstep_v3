VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMoneda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''' *** Clase: CMoneda
''' *** Creacion: 9/09/1998 (Javier Arana)
''' *** Ultima revision: 31/05/1999 (Alfredo Magallon)

Option Explicit


''' Variables privadas con la informacion de una moneda
Private mvarConexion As CConexion
Private mvarCod As String
Private mvarDen As String
Private mvarEQUIV As Double
'Private mvarEQ_ACT As Variant
Private mvarFecact As Variant
Private mvarId As Variant

''' Control de errores

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

''' Resultset para la moneda a editar
''' Indice de la moneda en la coleccion

Private mvarIndice As Long

Public Property Let Id(ByVal Data As Variant)
    Let mvarId = Data
End Property
Public Property Get Id() As Variant
    
    Id = mvarId
    
End Property

Public Property Let EQUIV(ByVal vData As Double)

    ''' * Objetivo: Dar valor a la variable privada EQUIV
    ''' * Recibe: Equivalencia de la moneda respecto a la central
    ''' * Devuelve: Nada
    
    mvarEQUIV = vData
    
End Property
Public Property Get EQUIV() As Double

    ''' * Objetivo: Devolver la variable privada EQUIV
    ''' * Recibe: Nada
    ''' * Devuelve: Equivalencia de la moneda respecto a la central

    EQUIV = mvarEQUIV
    
End Property
'Public Property Let EQ_ACT(ByVal vData As Variant)
'
'    ''' * Objetivo: Dar valor a la variable privada EQ_ACT
'    ''' * Recibe: Equivalencia actualizable S/N
'    ''' * Devuelve: Nada
'
'    mvarEQ_ACT = vData
'
'End Property
'Public Property Get EQ_ACT() As Variant
'
'    ''' * Objetivo: Devolver la variable privada EQUIV
'    ''' * Recibe: Nada
'    ''' * Devuelve: Equivalencia actualizable S/N
'
'    EQ_ACT = mvarEQ_ACT
'
'End Property
Public Property Let FECACT(ByVal vData As Variant)

    ''' * Objetivo: Dar valor a la variable privada FECACT
    ''' * Recibe: Fecha de ultima actualizacion de la moneda
    ''' * Devuelve: Nada

    mvarFecact = vData
    
End Property
Public Property Get FECACT() As Variant

    ''' * Objetivo: Devolver la variable privada FECACT
    ''' * Recibe: Nada
    ''' * Devuelve: Fecha de ultima actualizacion de la moneda

    FECACT = mvarFecact
    
End Property
Public Property Get Indice() As Integer

    ''' * Objetivo: Devolver el indice de la moneda en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice de la moneda en la coleccion

    Indice = mvarIndice
    
End Property
Public Property Let Indice(ByVal iInd As Integer)

    ''' * Objetivo: Dar valor al indice de la moneda en la coleccion
    ''' * Recibe: Indice de la moneda en la coleccion
    ''' * Devuelve: Nada

    mvarIndice = iInd
    
End Property
Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la moneda
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set mvarConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la moneda
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = mvarConexion
    
End Property
Public Property Let Den(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada DEN
    ''' * Recibe: Denominacion de la moneda
    ''' * Devuelve: Nada

    mvarDen = vData
    
End Property
Public Property Get Den() As String

    ''' * Objetivo: Devolver la variable privada DEN
    ''' * Recibe: Nada
    ''' * Devuelve: Denominacion de la moneda

    Den = mvarDen
    
End Property
Public Property Let Cod(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo de la moneda
    ''' * Devuelve: Nada

    mvarCod = vData
    
End Property
Public Property Get Cod() As String

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo de la moneda

    Cod = mvarCod
    
End Property

Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar la memoria
    
    Set mvarConexion = Nothing
    
End Sub


