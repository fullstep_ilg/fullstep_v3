VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGestorSeguridad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mvarConexion As CConexion

Public Property Set Conexion(ByVal vData As CConexion)
    
    Set mvarConexion = vData
    
End Property


Public Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Public Function ValidarUsuario(ByVal Cod As String, ByVal PWD As String, ByVal Portal As Integer) As CUsuario
Dim ador As ADODB.Recordset
Dim sConsulta As String
Dim oUsuario As CUsuario
Dim oCrypt1 As New CCrypt1
Dim pwdCrypt
Dim diacrypt
Dim oCrypt2 As New CCrypt2

    sConsulta = "SELECT USU.CIA,USU.ID,USU.COD,USU.PWD,USU.FECPWD,USU.APE,USU.NOM,USU.DEP,USU.CAR"
    sConsulta = sConsulta & ",USU.TFNO,USU.TFNO2,USU.FAX,USU.EMAIL,USU.IDI,USU.FCEST,USU_PORT.CIA,USU_PORT.USU,USU_PORT.FPEST FROM USU LEFT JOIN USU_PORT ON USU.ID=USU_PORT.USU AND USU_PORT.PORT=" & Portal
    sConsulta = sConsulta & " AND USU.COD='" & DblQuote(Cod) & "'"
    
    Set ador = New ADODB.Recordset
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenKeyset, adLockReadOnly
    
    If ador.EOF Then
    
        ador.Close
        Set ador = Nothing
        Set ValidarUsuario = Nothing
        Exit Function
    Else
        Set oUsuario = New CUsuario
        With oUsuario
        
            .Cod = ador("COD").Value
            .Cargo = ador("CAR").Value
            .CodDep = ador("DEP").Value
            .Fax = ador("FAX").Value
            .Mail = ador("EMAIL").Value
            .ID = ador("ID").Value
            .Nombre = ador("NOM").Value
            .Apellidos = ador("APE").Value
            .Tfno = ador("TFNO").Value
            .Tfno2 = ador("TFNO2").Value
            .PWD = ador("PWD").Value
            .FecPWD = ador("FECPWD").Value
            .Cia = ador("CIA").Value
            
            Set .Conexion = mvarConexion
            
            If IsNull(ador("FCEST").Value) Then
                .EstadoComprador = TESUPSinFuncion
            Else
                .EstadoComprador = ador("FCEST").Value
            End If
            
            If IsNull(ador("FPEST").Value) Then
                .EstadoProveedor = TESUPSinFuncion
            Else
                .EstadoProveedor = ador("FPEST").Value
            End If
            
            .Idioma = ador("IDI").Value
            
            If .EstadoComprador <> TESUPSinFuncion And .EstadoProveedor <> TESUPSinFuncion Then
                .TipoUsuario = TUCompradorYProveedor
            Else
                
                If .EstadoComprador <> TESUPSinFuncion Then
                    .TipoUsuario = TUComprador
                Else
                    .TipoUsuario = TUProveedor
                End If
            End If
            
        End With
        
        ador.Close
        Set ador = Nothing
        
        'Ahora debemos comparar las paswords
     
        'Primero encriptar
         ''' Encriptación de contraseña
        pwdCrypt = Encriptar(PWD, oUsuario.FecPWD)
        
        If pwdCrypt = oUsuario.PWD Then
            oUsuario.CodEncriptado = Encriptar(oUsuario.Cod, oUsuario.FecPWD)
            oUsuario.PWDEncriptada = pwdCrypt
            Set ValidarUsuario = oUsuario
        Else
            
            Set ValidarUsuario = Nothing
            Exit Function
            
        End If
        
        
    End If
    
    
    
End Function
