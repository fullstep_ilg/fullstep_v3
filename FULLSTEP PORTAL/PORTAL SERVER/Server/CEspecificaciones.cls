VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEspecificaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CEspecificaciones **********************************
'*             Autor : Javier Arana
'*             Creada : 2/3/98
'****************************************************************



Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection 'Contendra las especificaciones de un proceso o item
Private mvarConexion As CConexion

Public Property Get Item(vntIndexKey As Variant) As CEspecificacion
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

Public Function Add(ByVal Nombre As String, ByVal Fecha As Date, ByVal ID As Integer, Optional ByVal oProceso As CProceso, Optional ByVal oGrupo As CGrupo, Optional ByVal oItem As CItem, Optional ByVal Comentario As Variant, Optional ByVal varIndice As Variant, Optional ByVal mdSize As Double, Optional ByVal oCia As CCompania) As CEspecificacion
    'create a new object
    Dim sCod As String
    Dim objnewmember As CEspecificacion
    Set objnewmember = New CEspecificacion
   
    If Not IsMissing(oProceso) Then
        Set objnewmember.Proceso = oProceso
    End If
    
    If Not IsMissing(oGrupo) Then
        Set objnewmember.Grupo = oGrupo
    End If
    
    If Not IsMissing(oItem) Then
        Set objnewmember.Item = oItem
    End If
    
    If Not IsMissing(oCia) Then
        Set objnewmember.Cia = oCia
    End If
    
    objnewmember.ID = ID
    objnewmember.Nombre = Nombre
    If IsMissing(Comentario) Then
        objnewmember.Comentario = Null
    Else
        objnewmember.Comentario = Comentario
    End If
    
    objnewmember.Fecha = Fecha
    
    objnewmember.dataSize = mdSize
    
    Set objnewmember.Conexion = mvarConexion
    
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
    'set the properties passed into the method
       mCol.Add objnewmember, CStr(ID)
    End If
    
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
 

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'EspProceroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub




