VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCostesDescGrOfert"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion


Public Property Get Item(vntIndexKey As Variant) As CCosteDescGrOfert
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Function Add(ByVal vGrupo As Variant, ByVal iCosteDescuento As Long, Optional ByVal dImporteParcial As Variant) As CCosteDescGrOfert
        
    Dim objnewmember As CCosteDescGrOfert
    Set objnewmember = New CCosteDescGrOfert
   
    objnewmember.Grupo = vGrupo
    
    objnewmember.CosteDescuento = iCosteDescuento
    
    If IsNumeric(dImporteParcial) Then
        objnewmember.ImporteParcial = dImporteParcial
    Else
        objnewmember.ImporteParcial = 0
    End If

    mCol.Add objnewmember, vGrupo & "$$$" & CStr(iCosteDescuento)
     
    
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Sub Remove(vntIndexKey As Variant)
    
        mCol.Remove vntIndexKey
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    Set mCol = Nothing
End Sub



