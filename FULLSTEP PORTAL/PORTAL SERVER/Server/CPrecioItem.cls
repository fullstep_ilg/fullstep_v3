VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPrecioItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CPrecioItem **********************************
'*             Autor : Javier Arana
'*             Creada : 30/3/99
'***************************************************************

Option Explicit

Private m_lId As Long
Private m_vCantidad As Variant
Private m_vPrecioGS As Variant
Private m_vPrecioOferta As Variant
Private m_bPrecioOfertaValido As Boolean
Private m_vComent1 As Variant
Private m_vPrecio2 As Variant
Private m_vComent2 As Variant
Private m_vPrecio3 As Variant
Private m_vComent3 As Variant
Private m_vIndice As Variant
Private m_vObsAdjuntos As Variant
Private m_vPosicionPuja As Variant


Private m_oItem As CItem
Private m_oOfertaGrupo As COfertaGrupo
Private m_oAdjuntos As CAdjuntos
Private m_oAtributosOfertado As CAtributosOfertado

Private m_oCostesDescOfertados As CAtributosOfertado 'Valores de costes y descuentos del item
Private m_vPrecioNeto As Variant 'Precio neto = prec_valido, despues de aplicar costes/desc de precio unitario de �tem
Private m_vImporteBruto As Variant 'Importe bruto = prec_valido*cantidad
Private m_vImporteNeto As Variant 'Importe neto despues de aplicar costes/desc de total �tem

Private m_iNumAdjuntos As Integer


Private m_oConexion As CConexion

Public Property Let Indice(ByVal varIndice As Variant)
    m_vIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Public Property Let id(ByVal l As Long)
    m_lId = l
End Property
Public Property Get id() As Long
    id = m_lId
End Property
Public Property Let Cantidad(ByVal vDato As Variant)
    m_vCantidad = vDato
End Property
Public Property Get Cantidad() As Variant
    Cantidad = m_vCantidad
End Property


Public Property Let PrecioGS(ByVal vDato As Variant)
    m_vPrecioGS = vDato
End Property
Public Property Get PrecioGS() As Variant
    PrecioGS = m_vPrecioGS
End Property

Public Property Let PrecioOfertaValido(ByVal bDato As Boolean)
    m_bPrecioOfertaValido = bDato
End Property
Public Property Get PrecioOfertaValido() As Boolean
    PrecioOfertaValido = m_bPrecioOfertaValido
End Property

Public Property Let PrecioOferta(ByVal vDato As Variant)
    m_vPrecioOferta = vDato
End Property
Public Property Get PrecioOferta() As Variant
    PrecioOferta = m_vPrecioOferta
End Property

Public Property Let Comentario1(ByVal vDato As Variant)
    If IsEmpty(vDato) Then
        vDato = Null
    End If
    m_vComent1 = vDato
End Property
Public Property Get Comentario1() As Variant
    Comentario1 = m_vComent1
End Property
Public Property Let Precio2(ByVal vDato As Variant)
    m_vPrecio2 = vDato
End Property
Public Property Get Precio2() As Variant
    Precio2 = m_vPrecio2
End Property
Public Property Let Comentario2(ByVal vDato As Variant)
    If IsEmpty(vDato) Then
        vDato = Null
    End If
    m_vComent2 = vDato
End Property
Public Property Get Comentario2() As Variant
    Comentario2 = m_vComent2
End Property

Public Property Let Precio3(ByVal vDato As Variant)
    m_vPrecio3 = vDato
End Property
Public Property Get Precio3() As Variant
    Precio3 = m_vPrecio3
End Property
Public Property Let Comentario3(ByVal vDato As Variant)
    If IsEmpty(vDato) Then
        vDato = Null
    End If
    m_vComent3 = vDato
End Property
Public Property Get Comentario3() As Variant
    Comentario3 = m_vComent3
End Property

Public Property Let ObsAdjuntos(ByVal vDato As Variant)
    On Error Resume Next
    If vDato = "" Then
        m_vObsAdjuntos = Null
    Else
        m_vObsAdjuntos = vDato
    End If
End Property
Public Property Get ObsAdjuntos() As Variant
    ObsAdjuntos = m_vObsAdjuntos
End Property

Public Property Let NumAdjuntos(ByVal vDato As Variant)
On Error Resume Next
If IsNull(vDato) Then
    m_iNumAdjuntos = 0
Else
    m_iNumAdjuntos = CLng(vDato)
End If
If IsEmpty(m_iNumAdjuntos) Then
    m_iNumAdjuntos = 0
End If


End Property
Public Property Get NumAdjuntos() As Variant
    NumAdjuntos = m_iNumAdjuntos
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Set Item(ByVal oItem As CItem)
Set m_oItem = oItem
End Property

Public Property Get Item() As CItem
Set Item = m_oItem
End Property
Public Property Get Adjuntos() As CAdjuntos
    Set Adjuntos = m_oAdjuntos
End Property
Public Property Set Adjuntos(ByVal oAdjuntos As CAdjuntos)
    Set m_oAdjuntos = oAdjuntos
End Property

Public Property Set OfertaGrupo(ByVal oOfertaGrupo As COfertaGrupo)
Set m_oOfertaGrupo = oOfertaGrupo
End Property



Public Property Get OfertaGrupo() As COfertaGrupo
Set OfertaGrupo = m_oOfertaGrupo
End Property

Public Property Get AtributosOfertado() As CAtributosOfertado
    Set AtributosOfertado = m_oAtributosOfertado
End Property
Public Property Set AtributosOfertado(ByVal oAtributosOfertado As CAtributosOfertado)
    Set m_oAtributosOfertado = oAtributosOfertado
End Property

Public Property Let PosicionPuja(ByVal vDato As Variant)
    m_vPosicionPuja = vDato
End Property
Public Property Get PosicionPuja() As Variant
    PosicionPuja = m_vPosicionPuja
End Property

Public Property Get CostesDescOfertados() As CAtributosOfertado
    Set CostesDescOfertados = m_oCostesDescOfertados
End Property
Public Property Set CostesDescOfertados(ByVal oCostesDesc As CAtributosOfertado)
    Set m_oCostesDescOfertados = oCostesDesc
End Property

Public Property Let PrecioNeto(ByVal vDato As Variant)
    m_vPrecioNeto = vDato
End Property
Public Property Get PrecioNeto() As Variant
    PrecioNeto = m_vPrecioNeto
End Property

Public Property Let ImporteBruto(ByVal vDato As Variant)
    m_vImporteBruto = vDato
End Property
Public Property Get ImporteBruto() As Variant
    ImporteBruto = m_vImporteBruto
End Property

Public Property Let ImporteNeto(ByVal vDato As Variant)
    m_vImporteNeto = vDato
End Property
Public Property Get ImporteNeto() As Variant
    ImporteNeto = m_vImporteNeto
End Property

Private Sub Class_Terminate()
    Set m_oItem = Nothing
    Set m_oConexion = Nothing
    
End Sub




Public Function CargarAdjuntos(ByVal lCiaComp As Long) As Boolean
Dim adoRes As ADODB.Recordset
Dim adoComm As ADODB.Command
Dim adoParam As ADODB.Parameter
Dim sConsulta As String
Dim sFSG As String
Dim bSoloPortal As Boolean
Dim lIndice As Long

On Error GoTo Error


Set m_oAdjuntos = New CAdjuntos

If Not IsNull(m_oOfertaGrupo.Oferta.IDPortal) Then

    Set adoComm = New ADODB.Command
    
    Set adoComm.ActiveConnection = m_oConexion.AdoCon
    adoComm.CommandText = "EXEC SP_DEVOLVER_ADJUNTOS @ID = ?, @ITEM =?, @AMBITO = 3, @SOLOPORTAL = ? OUTPUT"
    adoComm.CommandType = adCmdText
    
    With m_oOfertaGrupo
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_oOfertaGrupo.Oferta.IDPortal)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_oItem.id)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter(Name:="SOLOPORTAL", Type:=adTinyInt, Direction:=adParamOutput)
        adoComm.Parameters.Append adoParam
    End With
    
    
    Set adoRes = adoComm.Execute
    
    bSoloPortal = (adoComm.Parameters("SOLOPORTAL").Value = 1)
    
    lIndice = 0
    While Not adoRes.eof
        m_oAdjuntos.Add oObjeto:=Me, id:=lIndice, AdjunID:=adoRes.Fields("IDGS").Value, Nombre:=adoRes.Fields("NOM").Value, dataSize:=adoRes("DATASIZE").Value, Comentario:=adoRes.Fields("COM").Value, IDPortal:=adoRes.Fields("IDPORTAL"), OP:="U"
        lIndice = lIndice + 1
    
        adoRes.MoveNext
    Wend
    
End If

If m_oAdjuntos.Count = 0 And Not bSoloPortal Then
    
    sFSG = FSGS(m_oConexion, lCiaComp)
    
    
    Set adoComm = New ADODB.Command
    
    Set adoComm.ActiveConnection = m_oConexion.AdoCon
    
    With m_oOfertaGrupo
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=.Grupo.Proceso.Anyo)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(.Grupo.Proceso.GMN1Cod), Value:=.Grupo.Proceso.GMN1Cod)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=.Grupo.Proceso.Cod)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_oItem.id)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(m_oOfertaGrupo.Oferta.Prove), Value:=m_oOfertaGrupo.Oferta.Prove)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_oOfertaGrupo.Oferta.Num)
        adoComm.Parameters.Append adoParam
    End With
    
    Set adoComm.ActiveConnection = m_oConexion.AdoCon
    adoComm.CommandText = "EXEC " & sFSG & "SP_DEVOLVER_ADJUNTOS @ANYO = ?, @GMN1 =?, @PROCE =?, @ITEM =?, @PROVE =?, @OFE =?"
    adoComm.CommandType = adCmdText
    
    Set adoRes = adoComm.Execute
    
    lIndice = 0
    While Not adoRes.eof
        m_oAdjuntos.Add oObjeto:=Me, id:=lIndice, AdjunID:=adoRes.Fields("ID").Value, Nombre:=adoRes.Fields("NOM").Value, dataSize:=adoRes("DATASIZE").Value, Comentario:=adoRes.Fields("COM").Value, IDPortal:=adoRes.Fields("IDPORTAL"), OP:="U"
        lIndice = lIndice + 1
    
        adoRes.MoveNext
    Wend
End If
    
fin:
On Error Resume Next
adoRes.Close
Set adoComm = Nothing
Set adoRes = Nothing
Exit Function


Error:
CargarAdjuntos = False
Resume fin
Resume 0
End Function



