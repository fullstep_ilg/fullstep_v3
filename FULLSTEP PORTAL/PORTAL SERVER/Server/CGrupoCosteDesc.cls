VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGrupoCosteDesc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' Variables privadas con la informacion de un grupo dentro del Coste / Descuento

Private mvGrupo As Variant
Private mvIndice As Long
'Private mdValor As Double
Private mdImporteParcial As Double


Public Property Get Indice() As Long

    ''' * Objetivo: Devolver el indice del grupo dentro del Coste / Descuento en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice del grupo dentro del Coste / Descuento en la coleccion

    Indice = mvIndice
    
End Property
Public Property Let Indice(ByVal lInd As Long)

    ''' * Objetivo: Dar valor al indice del grupo dentro del Coste / Descuento en la coleccion
    ''' * Recibe: Indice del grupo dentro del Coste / Descuento en la coleccion
    ''' * Devuelve: Nada

    mvIndice = lInd
    
End Property

Public Property Let Grupo(ByVal vData As Variant)

    mvGrupo = vData
    
End Property
Public Property Get Grupo() As Variant

    Grupo = mvGrupo
    
End Property

'Public Property Let Valor(ByVal dData As Double)
'
'    mdValor = dData
'
'End Property
'Public Property Get Valor() As Double
'
'    Valor = mdValor
'
'End Property

Public Property Let ImporteParcial(ByVal dData As Double)

    mdImporteParcial = dData
    
End Property
Public Property Get ImporteParcial() As Double

    ImporteParcial = mdImporteParcial
    
End Property

