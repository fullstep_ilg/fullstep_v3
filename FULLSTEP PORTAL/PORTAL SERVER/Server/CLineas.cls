VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLineas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' *** Clase: CLineas

''' Variables que almacenan las propiedades de los objetos

Private mCol As Collection
Private moConexion As CConexion

''' Control de errores

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum


Public Function Add(ByVal lId As Long, Optional ByVal lLineaCatalogo As Variant, Optional ByVal sUP As Variant _
                    , Optional ByVal dCantPed As Variant, Optional ByVal sDest As Variant _
                    , Optional ByVal bEntregaObl As Variant, Optional ByVal dFC As Variant _
                    , Optional ByVal dtFecEntrega As Variant, Optional ByVal sObs As Variant _
                    , Optional ByVal dCantRec As Variant, Optional ByVal ObsAdjun As Variant) As CLinea
    
    ''' * Objetivo: Anyadir una l�nea a la coleccion
    ''' * Recibe: Datos de la l�nea
    ''' * Devuelve: l�nea a�adida
    
    Dim objnewmember As CLinea
    
    ''' Creacion de objeto l�nea
    
    Set objnewmember = New CLinea
   
    ''' Paso de los parametros al nuevo objeto l�nea
    
    With objnewmember
        .Id = lId
        .LineaCatalogo = lLineaCatalogo
        If NoHayParametro(sUP) Then
            sUP = Null
        End If
        .UP = sUP
        If NoHayParametro(sDest) Then
            sDest = Null
        End If
        .Dest = sDest
        If NoHayParametro(dCantPed) Then
            dCantPed = Null
        End If
        .CantPed = dCantPed
        If NoHayParametro(dCantRec) Then
            dCantRec = Null
        End If
        .CantRec = dCantRec
        If NoHayParametro(bEntregaObl) Then
            bEntregaObl = Null
        End If
        .EntregaObl = bEntregaObl
        If NoHayParametro(dFC) Then
            dFC = Null
        End If
        .FC = dFC
        If NoHayParametro(dtFecEntrega) Then
            dtFecEntrega = Null
        End If
        .FecEntrega = dtFecEntrega
        If NoHayParametro(sObs) Then
            sObs = Null
        End If
        .obs = sObs

        
        If NoHayParametro(ObsAdjun) Then
            .ObsAdjun = Null
        Else
            .ObsAdjun = ObsAdjun
        End If
        
        Set .Conexion = moConexion
    End With
    
    mCol.Add objnewmember, CStr(lId)
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function
Public Property Get Item(vntIndexKey As Variant) As CLinea
Attribute Item.VB_MemberFlags = "200"

    ''' * Objetivo: Recuperar una l�nea de la coleccion
    ''' * Recibe: Indice de la l�nea a recuperar
    ''' * Devuelve: l�nea correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set moConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = moConexion
    
End Property
Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una l�nea de la coleccion
    ''' * Recibe: Indice de la l�nea a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    
   
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
    Set moConexion = Nothing
        
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property

