VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CConexion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mobj_adoConn As ADODB.Connection
Private mobj_adoErs As ADODB.Errors

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Friend Property Set AdoCon(ByVal vData As ADODB.Connection)
    Set mobj_adoConn = vData
End Property

Friend Property Get AdoCon() As ADODB.Connection
    Set AdoCon = mobj_adoConn
End Property

Public Property Set adoErs(ByVal vData As ADODB.Errors)
    Set mobj_adoErs = vData
End Property

Public Property Get adoErs() As ADODB.Errors
    Set adoErs = mobj_adoErs
End Property

Private Sub Class_Terminate()
    On Error Resume Next
    
    mobj_adoConn.Close
    Set mobj_adoConn = Nothing
End Sub

''' <summary>
''' Grabar un error en una comunicaci�n
''' </summary>
''' <param name="Pagina">Funci�n o pantalla q da el error</param>
''' <param name="NumError">Numero de Error VB. El Id de la tabla va en teserror.arg1</param>
''' <param name="Message">Mensaje del error VB</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmAdjAnya/cmdAceptar_Click frmObjAnya/cmdAceptar_Click frmOfePetAnya/cmdAceptar_Click
'''     basUtilidades/EnviarMensaje     basUtilidades/GrabarEnviarMensaje ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: jbg; Fecha: 13/06/2013
Public Function GrabarError(ByVal lCiaComp As Long, ByVal Pagina As String, _
        ByVal NumError As String, ByVal Message As String, Optional ByVal UsuConectado As String = "") As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim rs As ADODB.Recordset
    
    '********* Precondicion *******************
    If AdoCon Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConexion.GrabarError", "No se ha establecido la conexion"
    End If
    
    On Error GoTo Error:

    TESError.NumError = TESnoerror
    AdoCon.Execute "BEGIN TRANSACTION"
        
    Pagina = "Portal: " & Pagina

    AdoCon.Execute "INSERT INTO " & Fsgs(Me, lCiaComp) & "ERRORES (FEC_ALTA,PAGINA,USUARIO,EX_FULLNAME,EX_MESSAGE)" _
        & " VALUES(GETDATE()," & StrToSQLNULL(Pagina) & "," & StrToSQLNULL(UsuConectado) _
        & "," & StrToSQLNULL(NumError) & "," & StrToSQLNULL(Message) & ")"
        
    If AdoCon.Errors.Count > 0 Then GoTo Error
    
    Set rs = New ADODB.Recordset
    'Buscamos el numero de id maximo
    'CALIDAD: Sin WITH (NOLOCK) porque es para obtener el nuevo ID
    rs.Open "SELECT MAX(ID) AS NUM FROM " & Fsgs(Me, lCiaComp) & "ERRORES", AdoCon, adOpenForwardOnly, adLockReadOnly
    If rs.eof Then
        TESError.NumError = TESDatoEliminado
        AdoCon.Execute "ROLLBACK TRANSACTION"
        rs.Close
        Set rs = Nothing
        GrabarError = TESError
        Exit Function
    Else
        TESError.Arg1 = NullToDbl0(rs(0).Value)
        TESError.Arg2 = Message
    End If
    
    rs.Close
    Set rs = Nothing
    
    AdoCon.Execute "COMMIT TRANSACTION"
    
    GrabarError = TESError
    
    Exit Function
    
Error:
    basErrores.TratarError AdoCon.Errors
    
    AdoCon.Execute "ROLLBACK TRANSACTION"
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
End Function

