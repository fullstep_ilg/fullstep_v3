VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CEscalados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CEscalados **********************************
'*             Autor : DPD
'*             Creada : 26/07/2011
'***************************************************************

Option Explicit

Private m_oGrupo As CGrupo  ' Grupo de referencia (opcional)
Private m_oItem As CItem  ' �tem de referencia (opcional)
Private mCol As Collection ' Colecci�n de escalados
Private m_oConexion As CConexion ' Conexi�n de BBDD
Private m_lModo As TModoEscalado  'ModoRango(0)-> Rangos ModoDirecto(1)-> Cantidades Directas
Private m_sUniEsc As String ' Unidad del escalado


Public Property Get Item(vntIndexKey As Variant) As CEscalado
On Error GoTo NoSeEncuentra:
    Set Item = mCol(vntIndexKey)
    Exit Property
NoSeEncuentra:
    Set Item = Nothing
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get Modo() As TModoEscalado
    Modo = m_lModo
End Property

Public Property Let Modo(lParamModo As TModoEscalado)
    m_lModo = lParamModo
End Property

Public Property Get UnidadEscalado() As String
    UnidadEscalado = m_sUniEsc
End Property

Public Property Let UnidadEscalado(ByVal sParamUniEsc As String)
    m_sUniEsc = sParamUniEsc
End Property

Public Property Get Grupo() As CGrupo
    Set Grupo = m_oGrupo
End Property

Public Property Set Grupo(oParamGrupo As CGrupo)
    Set m_oGrupo = oParamGrupo
End Property

Public Property Get oItem() As CItem
    Set oItem = m_oItem
End Property

Public Property Set oItem(oParamItem As CItem)
    Set m_oItem = oParamItem
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub


'--<summary>
'--Crea un objeto de tipo Escalado y lo A�ade a la colecci�n.
'--</summary>
'--<param name="ID">Identificador</param>
'--<param name="vInicial">Valor de cantidad directo o Valor Inicial en el modo de rangos</param>
'--<param name="vFinal">Valor final de rango</param>
'--<param name="vPrecio">Precio</param>
'--<param name="vPrecioValido">Precio</param>
'--<param name="lIndex">Se utiliza para cargar los escalados de los diferentes grupos en el mismo orden y as� poder reacionar escalados en distintos grupos</param>
'--<returns>Devuelve el objeto escalado tras a�adirlo a la colecci�n</returns>
'--<remarks>Llamada desde frmPROCE frmPROCEEsc...</remarks>
'--<revision>DPD 12/08/11</revision>
Public Function Add _
        (ByVal id As Long, ByVal vInicial As Variant, Optional ByVal vFinal As Variant, Optional ByVal vPrecio As Variant, Optional ByVal vPrecioValido As Variant, Optional ByVal vValorNum As Variant, Optional ByVal vPrecioGS As Variant, Optional ByVal vPrecioOferta As Variant, Optional lIndex As Long) _
        As CEscalado
    
    
    Dim objnewmember As CEscalado
    Set objnewmember = New CEscalado
    objnewmember.id = id
    objnewmember.inicial = vInicial
    If Not IsMissing(vFinal) Then
        objnewmember.Final = vFinal
    End If
    If Not IsMissing(vPrecio) Then
        objnewmember.Precio = DblToVbFloat(vPrecio)
    End If
    If Not IsMissing(vPrecioValido) Then
        objnewmember.PrecioValido = DblToVbFloat(vPrecioValido)
    End If
    If Not IsMissing(vValorNum) Then
        objnewmember.ValorNum = vValorNum
    End If
    If Not IsMissing(vPrecioGS) Then
        objnewmember.PrecioGS = vPrecioGS
    End If
    If Not IsMissing(vPrecioOferta) Then
        objnewmember.PrecioOferta = vPrecioOferta
    End If
    
    
    If lIndex <> 0 Then
        objnewmember.Index = lIndex
    Else
        objnewmember.Index = Me.Count + 1
    End If
    If id = 0 Then ' Nuevo Escalado
        mCol.Add objnewmember, "N" & mCol.Count
    Else 'Escalado existente
        mCol.Add objnewmember, CStr(id)
    End If

    'Devuelve el objeto cEscalado
    Set Add = objnewmember
    Set objnewmember = Nothing
End Function

'--<summary>
'--Guarda el conjunto de escalados en la base de datos
'--</summary>
'--<remarks>Llamada desde; Tiempo m�ximo</remarks>
'--<revision>DPD 27/07/2011</revision>
Public Function Guardar() As Boolean

    Dim adoRes As New ADODB.Recordset
    Dim sConsulta As String
    Dim sIDsEscalados As String
    sIDsEscalados = ""
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        Guardar = False
        Exit Function
    End If
    
    Dim esc As CEscalado
    For Each esc In mCol
        If esc.id = 0 Then ' Nuevo Escalado
            If IsNull(esc.Final) Then
                sConsulta = "INSERT INTO PROCE_GR_ESC(ANYO,GMN1,PROCE,GRUPO,INICIO,FIN) VALUES(" & m_oGrupo.Proceso.Anyo & ",'" & m_oGrupo.Proceso.GMN1Cod & "'," & m_oGrupo.Proceso.Cod & "," & m_oGrupo.id & "," & DblToSQLFloat(esc.inicial) & ",null)"
            Else
                sConsulta = "INSERT INTO PROCE_GR_ESC(ANYO,GMN1,PROCE,GRUPO,INICIO,FIN) VALUES(" & m_oGrupo.Proceso.Anyo & ",'" & m_oGrupo.Proceso.GMN1Cod & "'," & m_oGrupo.Proceso.Cod & "," & m_oGrupo.id & "," & DblToSQLFloat(esc.inicial) & "," & DblToSQLFloat(esc.Final) & ")"
            End If
            m_oConexion.AdoCon.Execute sConsulta
            
            sConsulta = "Select @@identity"
            Set adoRes = m_oConexion.AdoCon.Execute(sConsulta)
            esc.id = adoRes(0).Value
        Else ' Modificar Escalado existente
            If IsNull(esc.Final) Then
                sConsulta = "UPDATE PROCE_GR_ESC SET INICIO=" & DblToSQLFloat(esc.inicial) & ",FIN =null WHERE id=" & esc.id
            Else
                sConsulta = "UPDATE PROCE_GR_ESC SET INICIO=" & DblToSQLFloat(esc.inicial) & ",FIN =" & DblToSQLFloat(esc.Final) & " WHERE id=" & esc.id
            End If
            m_oConexion.AdoCon.Execute sConsulta
        End If
        sIDsEscalados = sIDsEscalados & "," & esc.id
    Next
    
    'Comprobar si se han quitado escalados
    If Len(sIDsEscalados) > 0 Then
        sIDsEscalados = Mid(sIDsEscalados, 2, Len(sIDsEscalados))
    End If
    ' s�lo las del grupo
    sConsulta = "DELETE FROM PROCE_GR_ESC WHERE ANYO=" & m_oGrupo.Proceso.Anyo & " AND GMN1='" & m_oGrupo.Proceso.GMN1Cod & "' AND PROCE=" & m_oGrupo.Proceso.Cod & " AND GRUPO=" & m_oGrupo.id
    If Len(sIDsEscalados) > 0 Then
        'Quitamos los que ya no est�n en la colecci�n de escalados
        sConsulta = sConsulta & " AND ID NOT IN (" & sIDsEscalados & ")"
    End If
    m_oConexion.AdoCon.Execute sConsulta
    Guardar = Me.GuardarEstado
    
End Function


'--<summary>
'--Guarda el estado del grupo respecto al uso escalados en la base de datos
'--</summary>
'--<remarks>Llamada desde; Tiempo m�ximo</remarks>
'--<revision>DPD 02/08/2011</revision>
Public Function GuardarEstado() As Boolean

    Dim adoRes As New ADODB.Recordset
    Dim sConsulta As String
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        GuardarEstado = False
        Exit Function
    End If
    sConsulta = "UPDATE PROCE_GRUPO SET ESCALADOS=" & m_oGrupo.UsarEscalados & ",ESCALATIPO=" & m_oGrupo.TipoEscalados & ",ESCALAUNI='" & m_oGrupo.UnidadEscalado & "' WHERE ANYO=" & m_oGrupo.Proceso.Anyo & " AND GMN1='" & m_oGrupo.Proceso.GMN1Cod & "' AND PROCE=" & m_oGrupo.Proceso.Cod & " AND ID=" & m_oGrupo.id
    m_oConexion.AdoCon.Execute sConsulta
    
    GuardarEstado = True
End Function


'--<summary>
'--Vac�a la colecci�n de Escalados
'--</summary>
'--<remarks>Llamada desde; Tiempo m�ximo</remarks>
'--<revision>DPD 02/08/2011</revision>
Public Sub Clear()
    Dim esc As CEscalado
    For Each esc In mCol
        Me.Remove (CStr(esc.id))
    Next
End Sub


'--<summary>
'--Elimina de la colecci�n el elemento con �ndice especificado
'--</summary>
'--<param name="vntIndexKey">�ndice del elemento en la colecci�n</param>
'--<remarks>Llamada desde; Tiempo m�ximo</remarks>
'--<revision>DPD 02/08/2011</revision>
Public Sub Remove(vntIndexKey As Variant)
    On Error GoTo NoSeEncuentra
    mCol.Remove vntIndexKey
    Exit Sub
NoSeEncuentra:
End Sub


''' <summary>
''' Procedimiento que Borra de la base de datos los escalados de un grupo
''' </summary>
'''<param name="Anyo">Anyo del proceso</param>
'''<param name="GMN1">GMN1 del proceso</param>
'''<param name="PROCE">Codigo del proceso</param>
'''<param name="Grupo">Id del grupo</param>
''' <remarks>Llamada desde BorraEscaladosGrupos</remarks>
Public Sub BorrarEscaladoGrupo(ByVal Anyo As Long, ByVal Gmn1 As String, ByVal Proce As Long, ByVal Grupo As Long)

    Dim sConsulta As String
    
    sConsulta = "DELETE FROM ITEM_PRESESC WHERE ESC IN (SELECT ID FROM PROCE_GR_ESC WITH(NOLOCK) WHERE ANYO=" & Anyo & " AND GMN1='" & Gmn1 & "' AND PROCE=" & Proce & " AND GRUPO=" & Grupo & ")"
    m_oConexion.AdoCon.Execute sConsulta
    
    sConsulta = "DELETE FROM PROCE_GR_ESC WHERE ANYO=" & Anyo & " AND GMN1='" & Gmn1 & "' AND PROCE=" & Proce & " AND GRUPO=" & Grupo
    m_oConexion.AdoCon.Execute sConsulta

End Sub


'--<summary>
'--Comprueba si un valor es v�lido para el conjunto de escalados
'--</summary>
'--<param name="vParam">Valor a comprobar</param>
'--<optional param name="paramModo">Modo de escalado (si se omite se utiliza el que est� establecido internamente)</param>
'--<returns>True si es un valor v�lido false en caso contrario</returns>
'--<remarks>Llamada desde; Tiempo m�ximo</remarks>
'--<revision>DPD</revision>

Public Function ValorValido(vParam As Variant, Optional ByVal paramModo As Variant) As Boolean
    
    Dim lModoComp As Long
    If IsMissing(paramModo) Then lModoComp = Modo Else lModoComp = paramModo
    ValorValido = False
    If IsNull(vParam) Then
        ValorValido = True
        Exit Function
    End If
    
    If vParam = "" Then
        ValorValido = True
        Exit Function
    End If
    Dim oEscalado As CEscalado
    For Each oEscalado In mCol
        If lModoComp = 0 Then
            'Rango
            'If CDbl(vParam) > CDbl(oEscalado.Inicial) And CDbl(vParam) <= CDbl(oEscalado.Final) Then
            'DPD Ahora se permite una cantidad superior a cualquier escalado
            If CDbl(vParam) > CDbl(oEscalado.inicial) Then
                ValorValido = True
                Exit Function
            End If
            If CDbl(vParam) < CDbl(oEscalado.inicial) Then
                Exit Function
            End If
        Else
            'Cantidades directas
            If CDbl(vParam) = CDbl(oEscalado.inicial) Then
                ValorValido = True
                Exit Function
            End If
            If CDbl(vParam) < CDbl(oEscalado.inicial) Then
                Exit Function
            End If
        End If
    Next
End Function




'--<summary>
'--Guarda el conjunto de presupuestos - escalados en la base de datos
'--</summary>
'--<remarks>Llamada desde; Tiempo m�ximo</remarks>
'--<revision>DPD 27/07/2011</revision>
Public Function GuardarPresupuestos() As Boolean

    Dim adoRes As New ADODB.Recordset
    Dim sConsulta As String
    Dim sIDsEscalados As String
    sIDsEscalados = ""
    
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        GuardarPresupuestos = False
        Exit Function
    End If
    
    Dim esc As CEscalado
    For Each esc In mCol
        'Comprobar si ya existe el presupuesto para cambiarlo o inseratrlo
        sConsulta = "SELECT ESC FROM ITEM_PRESESC WITH(NOLOCK)"
        sConsulta = sConsulta & " WHERE ANYO= " & m_oItem.Proceso.Anyo
        sConsulta = sConsulta & " AND GMN1='" & m_oItem.Proceso.GMN1Cod & "'"
        sConsulta = sConsulta & " AND PROCE=" & m_oItem.Proceso.Cod
        sConsulta = sConsulta & " AND ITEM=" & m_oItem.id
        sConsulta = sConsulta & " AND ESC=" & esc.id
                        
        Set adoRes = m_oConexion.AdoCon.Execute(sConsulta)
        If adoRes.eof Then
            'Insertamos un nuevo prespupuesto escalado
            sConsulta = "INSERT INTO ITEM_PRESESC(ANYO,GMN1,PROCE,ITEM,ESC,PRESUNI) VALUES(" & m_oItem.Proceso.Anyo & ",'" & m_oItem.Proceso.GMN1Cod & "'," & m_oItem.Proceso.Cod & "," & m_oItem.id & "," & esc.id & "," & DblToSQLFloat(esc.Precio) & ")"
        Else
            'Modificamos el presupuesto escalado existente
            sConsulta = "UPDATE ITEM_PRESESC SET PRESUNI=" & DblToSQLFloat(esc.Precio) & " WHERE ANYO=" & m_oItem.Proceso.Anyo & " AND GMN1='" & m_oItem.Proceso.GMN1Cod & "' AND PROCE =" & m_oItem.Proceso.Cod & " AND ITEM=" & m_oItem.id & " AND ESC=" & esc.id
        End If
    
        m_oConexion.AdoCon.Execute sConsulta
        sIDsEscalados = sIDsEscalados & "," & esc.id
    Next
    GuardarPresupuestos = True
End Function



'--<summary>
'--Devuelve el presupuesto correspondiente a una cantidad dentro del conjunto de escalados
'--</summary>
'--<param name="Cantidad">Cantidad</param>
'--<returns>El presupuesto para una cantidad calculada seg�n escala directa o escala de rangos</returns>
'--<remarks>Llamada desde frmProce -> Editar Grid Campo Presupuesto</remarks>
'--<revision>DPD 08/08/2011</revision>

Function PresupuestoCantidad(ByVal Cantidad As Variant) As Variant
    Dim oEscalado As CEscalado
    PresupuestoCantidad = Null
    If IsNumeric(Cantidad) Then
        If Me.Modo = ModoDirecto Then
            For Each oEscalado In mCol
                If CDbl(oEscalado.inicial) = CDbl(Cantidad) Then
                    PresupuestoCantidad = oEscalado.Precio
                    Exit Function
                End If
                If CDbl(oEscalado.inicial) > CDbl(Cantidad) Then
                    Exit Function
                End If
            Next
        Else ' Modo Rangos
            For Each oEscalado In mCol
                'DPD Incidencia 21561
                'Permitimos culquier cantidad mayor que el inicio del primer escalado
                'Obtenemos el presupuesto asociado al mayor escalado que cuadre con la cantidad
                
                If CDbl(oEscalado.inicial) < CDbl(Cantidad) Then
                    If IsNumeric(oEscalado.Precio) Then
                        PresupuestoCantidad = oEscalado.Precio
                    End If
                Else
                    Exit Function
                End If
            
            Next
        End If
    End If
End Function



'--<summary>
'--Devuelve la posici�n del elemento con la clave indicada
'--</summary>
'--<param name="sKey">Clave de el</param>
'--<returns>la posici�n del elemento con la clave indicada. 0 en caso contrario</returns>
'--<remarks>Llamada desde; Tiempo m�ximo</remarks>
'--<revision>DPD 07/09/2011</revision>

Public Function Index(ByVal sKey As String) As Long
    Dim i As Long
    For i = 1 To Me.Count
        If Me.Item(i).id = sKey Then Exit For
    Next
    If Me.Item(i).id = sKey Then
        Index = i
    Else
        Index = 0
    End If
End Function







'--<summary>
'--Determina si un escalado es el que se aplica a una cantidad
'--</summary>
'--<param name="iEscalado">Escalado</param>
'--<param name="vCant">Cantidad</param>
'--<returns>True si el escalado se aplica a esa cantidad False en caso contrario</returns>
'--<remarks>Llamada desde CargarPrecios(); Tiempo m�ximo < 1s</remarks>
'--<revision>DPD 21/12/2011</revision>

Public Function Usar(ByVal idEscalado As String, ByVal vCant As Variant) As Boolean
    Dim oEscalado As CEscalado
    Dim idAux As Long
    Dim inicial As Variant
    Dim dblDif As Double
    Dim lIdEscMin As Long
    Dim lIdEscMax As Long
    Dim dblMin As Double
    Dim dblMax As Double
    Dim i As Integer
    
    If IsNumeric(vCant) Then
        If Not IsNull(Me.Item(1).Final) And Not IsEmpty(Me.Item(1).Final) Then    'Escalados por rangos
            dblMin = 0
            dblMax = 0
            For Each oEscalado In Me
                If lIdEscMin = 0 Then
                    lIdEscMin = oEscalado.id
                    dblMin = oEscalado.inicial
                Else
                    If CDbl(oEscalado.inicial) < dblMin Then
                        lIdEscMin = oEscalado.id
                        dblMin = oEscalado.inicial
                    End If
                End If
                If CDbl(oEscalado.Final) > dblMax Then
                    lIdEscMax = oEscalado.id
                    dblMax = oEscalado.Final
                End If
                
                If CDbl(oEscalado.inicial) <= CDbl(vCant) And CDbl(oEscalado.Final) >= CDbl(vCant) Then
                    Usar = (idEscalado = oEscalado.id)
                    Exit Function
                End If
            Next
                        
            If CDbl(vCant) < CDbl(dblMin) Then
                Usar = (idEscalado = lIdEscMin)
            ElseIf CDbl(vCant) > CDbl(dblMax) Then
                Usar = (idEscalado = lIdEscMax)
            End If
        Else ' Modo cantidades directas
            If Me.Item(CStr(idEscalado)) Is Nothing Then
                Usar = False
            Else
                If Me.Item(CStr(idEscalado)).inicial = vCant Then
                    Usar = True
                Else
                    'Se toma el escalado que presente una menor diferencia a la cantidad
                    i = 0
                    For Each oEscalado In Me
                        If i = 0 Then
                            idAux = oEscalado.id
                            dblDif = Abs(CDbl(oEscalado.inicial) - CDbl(vCant))
                        Else
                            If Abs(CDbl(oEscalado.inicial) - CDbl(vCant)) < dblDif Then
                                idAux = oEscalado.id
                                dblDif = Abs(CDbl(oEscalado.inicial) - CDbl(vCant))
                            End If
                        End If
                        
                        i = i + 1
                    Next
                
                    Usar = (idAux = idEscalado)
                End If
            End If
        End If
    Else
        Usar = False
    End If

End Function

