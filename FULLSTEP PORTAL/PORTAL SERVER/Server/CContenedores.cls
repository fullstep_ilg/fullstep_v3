VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CContenedores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mCol As Collection


Public Function Add(Optional ByVal Cod As Variant, Optional ByVal Den As Variant, Optional ByVal Obj As Object, Optional ByVal varIndice As Variant) As CContenedor
    
    Dim objnewmember As CContenedor
    
    Set objnewmember = New CContenedor
    
    If Not IsMissing(Cod) Then
        objnewmember.Cod = Cod
    End If
    
    If Not IsMissing(Den) Then
        objnewmember.Den = Den
    End If
    
    If Not IsMissing(varIndice) Then
        objnewmember.Indice = varIndice
    End If
    
    If Not IsMissing(Obj) Then
        Set objnewmember.Objeto = Obj
    End If
    
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        mCol.Add objnewmember, Cod
    End If
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function
Public Property Get Item(vntIndexKey As Variant) As CContenedor
  
On Error GoTo NoSeEncuentra:

    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get Count() As Long

   
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property
Public Sub Remove(vntIndexKey As Variant)
 
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()
 
   
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

       
    Set mCol = Nothing
    
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    Set NewEnum = mCol.[_NewEnum]

End Property

    




