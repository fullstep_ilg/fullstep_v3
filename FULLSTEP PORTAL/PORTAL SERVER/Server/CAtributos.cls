VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAtributos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private moConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
Set moConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property

Public Property Get Item(vntIndexKey As Variant) As CAtributo
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get Count() As Long
If mCol Is Nothing Then
    Count = 0
Else
    Count = mCol.Count
End If
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = mCol.[_NewEnum]
End Property

''' <summary>
''' A�ade un elemento a la colecci�n de atributos
''' </summary>
''' <param name="id">id</param>
''' <param name="PAID">PAID</param>
''' <param name="Cod">c�digo</param>
''' <param name="Den">denominaci�n</param>
''' <param name="ValorMin">valor m�nimo</param>
''' <param name="ValorMax">valor m�ximo</param>
''' <param name="Oblig">obligatorio</param>
''' <param name="bUsarIdProceAtrib">usar id proce atributo</param>
''' <param name="Operacion">operaci�n aritm�tica</param>
''' <param name="Aplicar">si se aplica el atributo</param>
''' <param name="vDescr">descripci�n</param>
''' <param name="Ambito">�mbito</param>
''' <param name="Grupos">colecci�n de grupos de c/d</param>
''' <param name="Alcance">alcance</param>
''' <revisado>JVS 17/06/2011</revisado>

Public Function Add(ByVal id As Variant, ByVal PAID As Variant, ByVal Cod As String, ByVal Den As String, _
                    ByVal Intro As Integer, ByVal tipo As TiposDeAtributos, Optional ByVal Valor As Variant, _
                    Optional ByVal ValorMin As Variant, Optional ByVal ValorMax As Variant, _
                    Optional ByVal Oblig As Boolean, Optional ByVal bUsarIdProceAtrib As Boolean = True, _
                    Optional ByVal Operacion As Variant, Optional ByVal Aplicar As Variant, Optional ByVal vDescr As Variant, Optional ByVal Ambito As Variant, _
                    Optional ByVal Grupos As CGruposCosteDesc, Optional ByVal Alcance As Variant) As CAtributo
    Dim objnewmember As CAtributo
    
    Set objnewmember = New CAtributo
   
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    objnewmember.Intro = Intro
    objnewmember.tipo = tipo
    objnewmember.id = id
    objnewmember.PAID = PAID
    objnewmember.Obligatorio = Oblig
    objnewmember.Descripcion = vDescr
    
    If Not IsMissing(Valor) Then
        objnewmember.Valor = Valor
    Else
        objnewmember.Valor = Null
    End If
    If Not IsMissing(ValorMin) Then
        objnewmember.ValorMin = ValorMin
    Else
        objnewmember.ValorMin = Null
    End If
    
    If Not IsMissing(ValorMax) Then
        objnewmember.ValorMax = ValorMax
    Else
        objnewmember.ValorMax = Null
    End If
    
    If IsMissing(Aplicar) Then
        Aplicar = Null
    End If
    If IsMissing(Operacion) Then
        Operacion = Null
    End If
    
    objnewmember.Aplicar = Aplicar
    objnewmember.Ambito = Ambito
    objnewmember.Operacion = Operacion
    objnewmember.Alcance = Alcance
    Set objnewmember.Grupos = Grupos

    
    If bUsarIdProceAtrib Then
        mCol.Add objnewmember, CStr(PAID)
    Else
        mCol.Add objnewmember, CStr(id)
    End If
    
    
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function


Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub
Private Sub Class_Initialize()
   
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set moConexion = Nothing
End Sub


Public Function DevolverTodosLosAtributosDesde(ByVal lCiaComp As Long, ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal iProce As Long, Optional ByVal sGrupo As Variant, Optional ByVal vAmbito As Variant, Optional ByVal lAtrib As Variant) As Boolean

Dim adoRes As ADODB.Recordset
Dim adoComm As ADODB.Command
Dim adoParam As ADODB.Parameter
Dim sConsulta As String
Dim sFSG As String
 
Dim iAmbito  As Integer


On Error GoTo Error


sFSG = Fsgs(moConexion, lCiaComp)
    

Set adoComm = New ADODB.Command

Set adoComm.ActiveConnection = moConexion.AdoCon


sConsulta = "EXEC " & sFSG & "SP_DEVOLVER_PROCE_ATRIB @ANYO=?, @GMN1=?, @PROCE=?, @AMBITO=?, @INTERNO = 0"

Set adoParam = adoComm.CreateParameter("ANYO", adSmallInt, adParamInput, , iAnyo)
adoComm.Parameters.Append adoParam

Set adoParam = adoComm.CreateParameter("GMN1", adVarChar, adParamInput, 50, sGMN1)
adoComm.Parameters.Append adoParam

Set adoParam = adoComm.CreateParameter("PROCE", adInteger, adParamInput, , iProce)
adoComm.Parameters.Append adoParam

iAmbito = 1

If Not IsMissing(vAmbito) Then
    iAmbito = vAmbito
End If

Set adoParam = adoComm.CreateParameter("AMBITO", adTinyInt, adParamInput, , iAmbito)
adoComm.Parameters.Append adoParam


If Not IsMissing(sGrupo) Then
    Set adoParam = adoComm.CreateParameter("GRUPO", adVarChar, adParamInput, 50, sGrupo)
    adoComm.Parameters.Append adoParam
    sConsulta = sConsulta & ", @GRUPO=? "
End If


If Not IsMissing(lAtrib) Then
    Set adoParam = adoComm.CreateParameter("ATRIB", adInteger, adParamInput, , lAtrib)
    adoComm.Parameters.Append adoParam
    sConsulta = sConsulta & ", @ATRIB=? "
End If




adoComm.CommandType = adCmdText
adoComm.CommandText = sConsulta
Set adoRes = adoComm.Execute

Dim vPAIDOld As Variant
Dim vPAIDNew As Variant
Dim vValor As Variant
Dim vValorMin As Variant
Dim vValorMax As Variant
Dim oAtributo As CAtributo

Set mCol = Nothing
Set mCol = New Collection

If Not adoRes.eof Then
    While Not adoRes.eof
        Select Case adoRes("TIPO").Value
            Case 1:
                vValorMin = Null
                vValorMax = Null
            Case 2:
                vValorMin = adoRes("MINNUM").Value
                vValorMax = adoRes("MAXNUM").Value
            Case 3:
                vValorMin = adoRes("MINFEC").Value
                vValorMax = adoRes("MAXFEC").Value
            Case 4:
                vValorMin = Null
                vValorMax = Null
        End Select
        Set oAtributo = Me.Add(adoRes("ATRIB").Value, adoRes("ID").Value, adoRes("COD").Value, adoRes("DEN").Value, adoRes("INTRO").Value, adoRes("TIPO").Value, , vValorMin, vValorMax, adoRes("OBLIG").Value, False, adoRes("OP_PREC").Value, adoRes("APLIC_PREC").Value, adoRes("DESCR").Value)
         
        vPAIDNew = adoRes("ID").Value
        vPAIDOld = vPAIDNew
        If adoRes("INTRO").Value = 1 Then
            Set oAtributo.Valores = New CValoresAtrib
            While Not adoRes.eof And vPAIDNew = vPAIDOld
                Select Case adoRes("TIPO").Value
                    Case 1:
                        oAtributo.Valores.Add adoRes("DALVALOR_TEXT").Value
                    Case 2:
                        oAtributo.Valores.Add adoRes("DALVALOR_NUM").Value
                    Case 3:
                        oAtributo.Valores.Add adoRes("DALVALOR_FEC").Value
                    Case 4:
                        oAtributo.Valores.Add adoRes("DALVALOR_BOOL").Value
                End Select
                vPAIDOld = vPAIDNew
                adoRes.MoveNext
                If Not adoRes.eof Then vPAIDNew = adoRes("ID").Value
            Wend
        Else
            adoRes.MoveNext
        End If
    Wend
    
End If


DevolverTodosLosAtributosDesde = True
    
fin:
On Error Resume Next
adoRes.Close
Set adoComm = Nothing
Set adoRes = Nothing
Exit Function


Error:
DevolverTodosLosAtributosDesde = False
Resume fin

End Function
