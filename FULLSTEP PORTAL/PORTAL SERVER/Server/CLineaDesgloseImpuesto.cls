VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLineaDesgImpuesto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private m_dBaseImponible As Double
Private m_stipoImpuesto As String
Private m_dtipovalor As Double

Private m_oConexion As CConexion

Public Property Get Key() As String
    Key = m_stipoImpuesto & " " & m_dtipovalor
End Property

Public Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Set Conexion(oConexion As CConexion)
    Set m_oConexion = oConexion
End Property

Public Function cuota() As Double
    cuota = BaseImponible * Me.TipoValor / 100
End Function

Public Property Get BaseImponible() As Double
    BaseImponible = m_dBaseImponible
End Property

Public Property Let BaseImponible(ByVal dBaseImponible As Double)
    m_dBaseImponible = dBaseImponible
End Property

Public Property Get TipoImpuesto() As String
    TipoImpuesto = m_stipoImpuesto
End Property

Public Property Let TipoImpuesto(ByVal stipoImpuesto As String)
    m_stipoImpuesto = stipoImpuesto
End Property

Public Property Get TipoValor() As Double
    TipoValor = m_dtipovalor
End Property

Public Property Let TipoValor(ByVal dtipovalor As Double)
    m_dtipovalor = dtipovalor
End Property

Public Function toString() As String
    toString = TipoImpuesto & " " & TipoValor & " % :" & cuota & vbCrLf & "Base imponible " & TipoImpuesto & " : " & BaseImponible
End Function


