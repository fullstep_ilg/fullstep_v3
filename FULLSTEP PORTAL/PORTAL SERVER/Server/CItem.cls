VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mlId As Long
Private mvArtCod As Variant
Private mvDescr As Variant
Private mvDestCod As Variant
Private mvUniCod As Variant
Private mvUniDen As Variant
Private mvPagCod As Variant
Private mvPagDen As Variant
Private mvCantidad As Variant
Private mvPrecio As Variant
Private mvPresupuesto As Variant
Private mvIndice As Variant
Private mvObjetivo As Variant
Private mvFecInicio As Variant
Private mvFecFin As Variant
Private mvFecObj As Variant
Private mvEsp As Variant
Private mvMinProveCod As Variant
Private mvMinProveDen As Variant
Private mvMinPrecio As Variant
Private mvEspAdj As Variant
Private mvEspAdjI As Variant
Private mbConfirmado As Boolean
Private mvObsAdjuntos As Variant
Private mvNumAdjuntos As Variant
'Private mdLimite As Variant
Private mdMinPujGanador As Variant
Private mvPosicionPuja As Variant


Private moConexion As CConexion


Private moProceso As CProceso
Private moGrupo As CGrupo
Private moEspecificaciones As CEspecificaciones
Private moAEspecificacionesI As CAtEspecificaciones
Private moPrecioItems As CPrecioItems
Private moPrecioItem As CPrecioItem
Private moPujaItem As CPrecioItem

'DPD Campos asociados al modo Subasta

Private mvPrecioSalida As Variant
Private mvMinPujProve As Variant
Private mvImporteCondicion As Variant

'Presupuestos por escalados
Private m_oEscalados As CEscalados ' Tabla de precios escalados
Private m_lUsarEscalados As Long ' Usar Escalados 1 si/ 0 no
Private m_lTipoEscalados As Long ' Tipo de Escalados 0 -> Cantidades directas / 1 -> Rangos

'/DPD
Private m_iAnyoImputacion As Integer
'JVS Campo para determinar el precio al que aplica
Private mvUsar As Variant

Public Property Let Indice(ByVal vIndice As Variant)
    mvIndice = vIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvIndice
End Property
Public Property Let FechaInicioSuministro(ByVal vDat As Variant)
    mvFecInicio = vDat
End Property
Public Property Get FechaInicioSuministro() As Variant
    FechaInicioSuministro = mvFecInicio
End Property

Public Property Let FechaObjetivo(ByVal vDat As Variant)
    mvFecObj = vDat
End Property
Public Property Get FechaObjetivo() As Variant
    FechaObjetivo = mvFecObj
End Property

Public Property Let FechaFinSuministro(ByVal vDat As Variant)
    mvFecFin = vDat
End Property
Public Property Get FechaFinSuministro() As Variant
    FechaFinSuministro = mvFecFin
End Property
Public Property Get Especificaciones() As CEspecificaciones
    Set Especificaciones = moEspecificaciones
End Property
Public Property Set Especificaciones(ByVal oEsps As CEspecificaciones)
    Set moEspecificaciones = oEsps
End Property

Public Property Get AEspecificacionesI() As CAtEspecificaciones
    Set AEspecificacionesI = moAEspecificacionesI
End Property
Public Property Set AEspecificacionesI(ByVal oAEspsI As CAtEspecificaciones)
    Set moAEspecificacionesI = oAEspsI
End Property

Public Property Let Esp(ByVal vDato As Variant)
    mvEsp = vDato
End Property
Public Property Get Esp() As Variant
    Esp = mvEsp
End Property
Public Property Let EspAdj(ByVal vDato As Variant)
    mvEspAdj = vDato
End Property
Public Property Get EspAdj() As Variant
    EspAdj = mvEspAdj
End Property
'jpa
Public Property Let EspAdjI(ByVal vDato As Variant)
    mvEspAdjI = vDato
End Property
Public Property Get EspAdjI() As Variant
    EspAdjI = mvEspAdjI
End Property

'fin jpa Tarea 540
Public Property Let DestCod(ByVal vDato As Variant)
    mvDestCod = vDato
End Property
Public Property Get DestCod() As Variant
    DestCod = mvDestCod
End Property
Public Property Let UniCod(ByVal vDato As Variant)
    mvUniCod = vDato
End Property
Public Property Get UniCod() As Variant
    UniCod = mvUniCod
End Property
Public Property Let UniDen(ByVal vDato As Variant)
    mvUniDen = vDato
End Property
Public Property Get UniDen() As Variant
    UniDen = mvUniDen
End Property
Public Property Let Confirmado(ByVal bDato As Boolean)
    mbConfirmado = bDato
End Property
Public Property Get Confirmado() As Boolean
    Confirmado = mbConfirmado
End Property

Public Property Let PagCod(ByVal vDato As Variant)
    mvPagCod = vDato
End Property
Public Property Get PagCod() As Variant
    PagCod = mvPagCod
End Property
Public Property Get PagDen() As Variant
    PagDen = mvPagDen
End Property
Public Property Let PagDen(ByVal vPagDen As Variant)
    mvPagDen = vPagDen
End Property
Public Property Get descr() As Variant
    descr = mvDescr
End Property
Public Property Let descr(ByVal vDescr As Variant)
    mvDescr = vDescr
End Property
Public Property Let id(ByVal lDato As Long)
    mlId = lDato
End Property
Public Property Get id() As Long
    id = mlId
End Property
Public Property Let Cantidad(ByVal vDato As Variant)
    mvCantidad = vDato
End Property
Public Property Get Cantidad() As Variant
    Cantidad = mvCantidad
End Property

Public Property Let Precio(ByVal v As Variant)
    mvPrecio = v
End Property
Public Property Get Precio() As Variant
    Precio = mvPrecio
End Property
Public Property Let Presupuesto(ByVal v As Variant)
    mvPresupuesto = v
End Property
Public Property Get Presupuesto() As Variant
    Presupuesto = mvPresupuesto
End Property
Public Property Let Objetivo(ByVal v As Variant)
    mvObjetivo = v
End Property
Public Property Get Objetivo() As Variant
Objetivo = mvObjetivo
End Property

Public Property Get ArticuloCod() As Variant
    ArticuloCod = mvArtCod
End Property
Public Property Let ArticuloCod(ByVal v As Variant)
    mvArtCod = v
End Property

Public Property Get MinProveCod() As Variant
    MinProveCod = mvMinProveCod
End Property
Public Property Let MinProveCod(ByVal v As Variant)
    mvMinProveCod = v
End Property
Public Property Get MinProveDen() As Variant
    MinProveDen = mvMinProveDen
End Property
Public Property Let MinProveDen(ByVal v As Variant)
    mvMinProveDen = v
End Property

Public Property Get MinPrecio() As Variant
    MinPrecio = mvMinPrecio
End Property
Public Property Let MinPrecio(ByVal vDato As Variant)
    mvMinPrecio = vDato
End Property

'Public Property Get Limite() As Variant
'    Limite = mdLimite
'End Property
'Public Property Let Limite(ByVal vDato As Variant)
'    mdLimite = vDato
'End Property

Public Property Get MinPujGanador() As Variant
    MinPujGanador = mdMinPujGanador
End Property
Public Property Let MinPujGanador(ByVal vDato As Variant)
    mdMinPujGanador = vDato
End Property

'JVS Propiedad de Usar
Public Property Get Usar() As Variant
    Usar = mvUsar
End Property
Public Property Let Usar(ByVal vDato As Variant)
    mvUsar = vDato
End Property



Public Property Set Conexion(ByVal oCon As CConexion)
Set moConexion = oCon
End Property

Public Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property


Public Property Set Proceso(ByVal oProce As CProceso)
    Set moProceso = oProce
End Property
Public Property Get Proceso() As CProceso
    Set Proceso = moProceso
End Property

Public Property Set Grupo(ByVal oGrupo As CGrupo)
    Set moGrupo = oGrupo
End Property
Public Property Get Grupo() As CGrupo
    Set Grupo = moGrupo
End Property

Public Property Set PrecioItems(ByVal oPrecios As CPrecioItems)
    Set moPrecioItems = oPrecios
End Property
Public Property Get PrecioItems() As CPrecioItems
    Set PrecioItems = moPrecioItems
End Property

Public Property Set PrecioItem(ByVal oPrecio As CPrecioItem)
    Set moPrecioItem = oPrecio
End Property
Public Property Get PrecioItem() As CPrecioItem
    Set PrecioItem = moPrecioItem
End Property

Public Property Set PujaItem(ByVal oPrecio As CPrecioItem)
    Set moPujaItem = oPrecio
End Property
Public Property Get PujaItem() As CPrecioItem
    Set PujaItem = moPujaItem
End Property

Public Property Let PosicionPuja(ByVal vDato As Variant)
    mvPosicionPuja = vDato
End Property
Public Property Get PosicionPuja() As Variant
    PosicionPuja = mvPosicionPuja
End Property



'DPD Propiedades asociados al modo Subasta

Public Property Let PrecioSalida(ByVal vDato As Variant)
    mvPrecioSalida = vDato
End Property
Public Property Get PrecioSalida() As Variant
    PrecioSalida = mvPrecioSalida
End Property

Public Property Let MinPujProve(ByVal vDato As Variant)
    mvMinPujProve = vDato
End Property
Public Property Get MinPujProve() As Variant
    MinPujProve = mvMinPujProve
End Property

'DPD Propiedades para el manejo de precios escalados
Public Property Get Escalados() As CEscalados
    Set Escalados = m_oEscalados
End Property

Public Property Set Escalados(ByVal oEscalados As CEscalados)
    Set m_oEscalados = oEscalados
End Property


Public Property Get UsarEscalados() As Long
    UsarEscalados = m_lUsarEscalados
End Property

Public Property Let UsarEscalados(ByVal lParamUsarEscalados As Long)
    m_lUsarEscalados = lParamUsarEscalados
End Property


Public Property Get TipoEscalados() As Long
    TipoEscalados = m_lTipoEscalados
End Property

Public Property Let TipoEscalados(ByVal lParamTipoEscalados As Long)
    m_lTipoEscalados = lParamTipoEscalados
End Property


Public Property Get AnyoImputacion() As Integer
    AnyoImputacion = m_iAnyoImputacion
End Property

Public Property Let AnyoImputacion(ByVal iAnyoImputacion As Integer)
    m_iAnyoImputacion = iAnyoImputacion
End Property






'''<summary>
'''Dato que se leer� en el portal para consultar el importe m�nimo de la condici�n
'''Puede ser el precio de salida o el de mejora de la puja m�nima
'''</summary>
Public Property Get ImporteCondicion() As Variant
    ImporteCondicion = mvImporteCondicion
End Property
Public Property Let ImporteCondicion(ByVal vValor As Variant)
    mvImporteCondicion = vValor
End Property


'/DPD




''' <summary>
''' Devuelve un recordset desconectado con la fecha, proveedor e importe de las pujas para el item indicado
''' </summary>
''' <returns>Recordset (fecha, proveedor, importe)</returns>
''' <remarks>Llamada desde: solicitudesoferta/detallepujas.asp</remarks>

Public Function DevolverDetallesPujas(ByVal lCiaComp As Long, ByVal sCodProve As String) As ADODB.Recordset
Dim adoRes As ADODB.Recordset
Dim adoComm As ADODB.Command
Dim adoParam As ADODB.Parameter

       
    On Error GoTo Error

    
    'Ejecutamos el store procedure SP_DEVOLVER_PUJAS_ITEM que enlaza con el SP_DEVOLVER_PUJAS_ITEM_WEB del GS
    Set adoComm = New ADODB.Command
    
    Set adoParam = adoComm.CreateParameter("CIA_COMP", adInteger, adParamInput, , lCiaComp)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("ANYO", adInteger, adParamInput, , moProceso.Anyo)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("GMN1", adVarChar, adParamInput, 50, moProceso.GMN1Cod)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("PROCE", adInteger, adParamInput, , moProceso.Cod)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("ITEM", adInteger, adParamInput, , mlId)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("PROVE", adVarChar, adParamInput, 50, sCodProve)
    adoComm.Parameters.Append adoParam
    
    Set adoComm.ActiveConnection = Proceso.Conexion.AdoCon
    adoComm.CommandType = adCmdStoredProc
    adoComm.CommandText = "SP_DEVOLVER_PUJAS_ITEM"
    adoComm.Prepared = True
    Set adoRes = adoComm.Execute
    
    If adoRes.eof Then
        adoRes.Close
        Set adoRes = Nothing
        Set DevolverDetallesPujas = Nothing
        Exit Function
    Else
        'Devuelve un recordset desconectado
        adoRes.ActiveConnection = Nothing
        Set DevolverDetallesPujas = adoRes
        
        Set adoComm = Nothing
    End If

fin:

    Exit Function
    
Error:

    Resume fin
End Function



Public Function CargarEspecificaciones(ByVal lCiaComp As Long, Optional ByVal vAnyo As Variant, Optional ByVal vGMN1Cod As Variant, Optional ByVal vProce As Variant, Optional ByVal vItem As Variant) As Boolean
Dim adoRes As ADODB.Recordset
Dim adoComm As ADODB.Command
Dim adoParam As ADODB.Parameter
Dim sConsulta As String
Dim sSRV As String
Dim sBD As String
Dim sFSG As String
       
    
On Error GoTo Error


If IsMissing(vAnyo) Then
    vAnyo = moProceso.Anyo
    vGMN1Cod = moProceso.GMN1Cod
    vProce = moProceso.Cod
Else
    Set moProceso = New CProceso
    moProceso.Anyo = vAnyo
    moProceso.GMN1Cod = vGMN1Cod
    moProceso.Cod = vProce
End If

If Not IsMissing(vItem) Then
    mlId = vItem
End If

sFSG = Fsgs(moConexion, lCiaComp)
    

Set adoComm = New ADODB.Command

Set adoComm.ActiveConnection = moConexion.AdoCon

Set adoParam = adoComm.CreateParameter("ANYO", adInteger, adParamInput, , vAnyo)
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("GMN1", adVarChar, adParamInput, 50, vGMN1Cod)
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("PROCE", adInteger, adParamInput, , vProce)
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("ITEM", adVarChar, adParamInput, 50, mlId)
adoComm.Parameters.Append adoParam



sConsulta = sFSG & "SP_DEVOLVER_ITEM_ESP_PORTAL"
adoComm.CommandType = adCmdStoredProc
adoComm.CommandText = sConsulta
Set adoRes = adoComm.Execute

Set moEspecificaciones = New CEspecificaciones
If Not adoRes.eof Then
           
    While Not adoRes.eof
        ' Dejamos en manos del programadoRes cuando asignar la conexion al obejto usuario
        moEspecificaciones.Add adoRes("NOM").Value, adoRes("FECACT").Value, adoRes("ID").Value, , , Me, adoRes("COM").Value, , adoRes("TAMANYO").Value
        adoRes.MoveNext
    Wend
    
End If

CargarEspecificaciones = True
    
fin:
On Error Resume Next
adoRes.Close
Set adoComm = Nothing
Set adoRes = Nothing
Exit Function


Error:
CargarEspecificaciones = False
Resume fin

End Function

Public Function CargarAEspecificacionesI(ByVal lCiaComp As Long, Optional ByVal vAnyo As Variant, Optional ByVal vGMN1Cod As Variant, Optional ByVal vProce As Variant, Optional ByVal vItem As Variant) As Boolean
Dim adoRes As ADODB.Recordset
Dim adoComm As ADODB.Command
Dim adoParam As ADODB.Parameter
Dim sConsulta As String
Dim sSRV As String
Dim sBD As String
'Dim sFSG As String
       
    
On Error GoTo Error


If IsMissing(vAnyo) Then
    vAnyo = moProceso.Anyo
    vGMN1Cod = moProceso.GMN1Cod
    vProce = moProceso.Cod
Else
    Set moProceso = New CProceso
    moProceso.Anyo = vAnyo
    moProceso.GMN1Cod = vGMN1Cod
    moProceso.Cod = vProce
End If

If Not IsMissing(vItem) Then
    mlId = vItem
End If

'sFSG = FSGS(moConexion, lCiaComp)
    

Set adoComm = New ADODB.Command

Set adoComm.ActiveConnection = moConexion.AdoCon

Set adoParam = adoComm.CreateParameter("CIA_COMP", adInteger, adParamInput, , lCiaComp)
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("ANYO", adInteger, adParamInput, , vAnyo)
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("GMN1", adVarChar, adParamInput, 50, vGMN1Cod)
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("PROCE", adInteger, adParamInput, , vProce)
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("ITEM", adInteger, adParamInput, , mlId)
adoComm.Parameters.Append adoParam

'sConsulta = sFSG & "SP_DEVOLVER_PROCE_ATRIBESP_ITEM"
sConsulta = "SP_DEVOLVER_PROCE_ATRIBESP_ITEM_WEB"
adoComm.CommandType = adCmdStoredProc
adoComm.CommandText = sConsulta
Set adoRes = adoComm.Execute

Set moAEspecificacionesI = New CAtEspecificaciones
If Not adoRes.eof Then
           
    While Not adoRes.eof
        ' Dejamos en manos del programadoRes cuando asignar la conexion al obejto usuario
        moAEspecificacionesI.Add adoRes("ID").Value, adoRes("COD").Value, "" & adoRes("DESCR").Value, "" & adoRes("DEN").Value, adoRes("ATRIB").Value, adoRes("INTERNO").Value, adoRes("PEDIDO").Value, adoRes("ORDEN").Value, , , , adoRes("VALOR_NUM").Value, adoRes("VALOR_TEXT").Value, adoRes("VALOR_FEC").Value, adoRes("VALOR_BOOL").Value, adoRes("TIPO").Value
        
        adoRes.MoveNext
    Wend
    
End If

CargarAEspecificacionesI = True
    
fin:
On Error Resume Next
adoRes.Close
Set adoComm = Nothing
Set adoRes = Nothing
Exit Function


Error:
CargarAEspecificacionesI = False
Resume fin

End Function



'--<summary>
'--Carga los presupuestos escalados para un item desde la base de datos
'--</summary>
'--<remarks>Llamada desde portal -> Cumpoferta.asp </remarks>
'--<revision>DPD 29/09/2011</revision>

Public Sub CargarPresEscalados(ByVal lCiaComp As Long)
Dim adoRes As ADODB.Recordset
Dim adoComm As ADODB.Command
Dim adoParam As ADODB.Parameter
Dim sConsulta As String
Dim sFSG As String
    sFSG = Fsgs(moConexion, lCiaComp)
    On Error GoTo Error

    sConsulta = sFSG & "FSP_OBT_ESCALADOS_ITEM"
        
    Set adoComm = New ADODB.Command
    
    Set adoParam = adoComm.CreateParameter("ANYO", adInteger, adParamInput, , moProceso.Anyo)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("GMN1", adVarChar, adParamInput, 50, moProceso.GMN1Cod)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("PROCE", adInteger, adParamInput, , moProceso.Cod)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("GRUPO", adInteger, adParamInput, , moGrupo.id)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("ITEM", adInteger, adParamInput, , mlId)
    adoComm.Parameters.Append adoParam
    
    
    Set adoParam = adoComm.CreateParameter("ESCALADOS", adTinyInt, adParamOutput)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter("ESCALATIPO", adTinyInt, adParamOutput)
    adoComm.Parameters.Append adoParam
    
    
    
    Set adoComm.ActiveConnection = Proceso.Conexion.AdoCon
'    adoComm.CommandType = adCmdText
    adoComm.CommandType = adCmdStoredProc
    adoComm.CommandText = sConsulta
    adoComm.Prepared = True
    Set adoRes = adoComm.Execute
    
        
    m_lUsarEscalados = adoComm.Parameters("ESCALADOS").Value
    m_lTipoEscalados = adoComm.Parameters("ESCALATIPO").Value
    
    
    If m_oEscalados Is Nothing Then Set m_oEscalados = New CEscalados
    
    
    m_oEscalados.Modo = m_lTipoEscalados
    
    
    If Not adoRes.eof Then
        m_oEscalados.Modo = 1
        While Not adoRes.eof
            If m_oEscalados.Item(CStr(adoRes.Fields("ID"))) Is Nothing Then
                If IsNull(adoRes.Fields("FIN")) Then
                    
                        m_oEscalados.Add adoRes.Fields("ID"), adoRes.Fields("INICIO"), Null
                    
                Else
                    m_oEscalados.Modo = 0
                    
                    m_oEscalados.Add adoRes.Fields("ID"), adoRes.Fields("INICIO"), adoRes.Fields("FIN") ', adoRes.Fields("PRESUNI")
                End If
                
            End If
            m_oEscalados.Item(CStr(adoRes.Fields("ID"))).Objetivo = adoRes.Fields("OBJ")
            adoRes.MoveNext
        Wend
    End If
    adoRes.Close
    
    Set adoRes = Nothing
fin:
    Exit Sub
Error:
    Resume fin
End Sub


