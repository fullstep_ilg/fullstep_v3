VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSession"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_sID As String
Private m_lCiaComp As Long
Private m_lCiaId As Long
Private m_sCiaCod As String
Private m_sCiaCodGs As String
Private m_lUsuId As Long
Private m_sUsuCod As String
Private m_sIPDir As String

Private m_vPyme As Variant
Private m_vUsuPpal As Variant
Private m_vPedidos As Variant
Private m_vCn As Variant
Private m_vPedPendientes As Variant
Private m_vPmSolicitudes As Variant
Private m_vCertificados As Variant
Private m_vNoConformidades As Variant
Private m_vNoConfNuevas As Variant
Private m_vBajaCalidad As Variant
Private m_vQaVarCal As Variant
Private m_vCSRFToken As Variant
Private m_vSolicitudesFsga As Variant
Private m_vIdOrden As Variant

Private m_iCadSesion As Integer
Private m_vFecIniSes As Variant
Private m_vFecFinSes As Variant
Private m_vActiva As Variant
Private m_vPersistID As Variant

Private m_iMostrarFMT As Integer
Private m_vDecimalFMT As Variant
Private m_vThousanFMT As Variant
Private m_vPrecisionFMT As Variant
Private m_vDateFMT As Variant
Private m_vTipoMail As Variant
Private m_vIdi As Variant
Private m_vAutofactura As Variant

Private m_vFecAct As Variant

Private m_oConexion As CConexion

Private b_CadPassword As Boolean
Private d_FecPWD As Date

Private m_oParametros As CGestorParametros
''' <summary>
''' Establecer la variable Id de sesion
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let id(ByVal vData As String)
    m_sID = vData
End Property
''' <summary>
''' Establecer la variable ID de la compa�ia compradora
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let CiaComp(ByVal vData As Long)
    m_lCiaComp = vData
End Property
''' <summary>
''' Establecer la variable ID de compa�ia proveedora
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let CiaId(ByVal vData As Long)
    m_lCiaId = vData
End Property
''' <summary>
''' Establecer la variable codigo de la compa�ia compradora
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let CiaCod(ByVal vData As String)
    m_sCiaCod = vData
End Property
''' <summary>
''' Establecer la variable codigo de la compa�ia compradora en la bbdd GS
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let CiaCodGs(ByVal vData As String)
    m_sCiaCodGs = vData
End Property
''' <summary>
''' Establecer la variable id de usuario
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let UsuId(ByVal vData As Long)
    m_lUsuId = vData
End Property
''' <summary>
''' Establecer la variable Codigo de usuario
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let UsuCod(ByVal vData As String)
    m_sUsuCod = vData
End Property
''' <summary>
''' Establecer la variable direcci�n IP desde la que se ha hecho la petici�n
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let IPDir(ByVal vData As String)
    m_sIPDir = vData
End Property
''' <summary>
''' Establecer la variable Pyme
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let PYME(ByVal vData As Variant)
    m_vPyme = vData
End Property
''' <summary>
''' Establecer la variable si es usuario principal o no
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let UsuPpal(ByVal vData As Variant)
    m_vUsuPpal = vData
End Property
''' <summary>
''' Establecer la variable Pedidos
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let Pedidos(ByVal vData As Variant)
    m_vPedidos = vData
End Property
''' <summary>
''' Establecer la variable Pedidos Pendientes
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let PedPendientes(ByVal vData As Variant)
    m_vPedPendientes = vData
End Property
''' <summary>
''' Establecer la variable si tiene PMSOLICITUDES la compa�ia o no
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let PmSolicitudes(ByVal vData As Variant)
    m_vPmSolicitudes = vData
End Property
''' <summary>
''' Establecer la variable si tiene CN la compa�ia o no
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let CN(ByVal vData As Variant)
    m_vCn = vData
End Property
''' <summary>
''' Establecer la variable si tiene certificados la compa�ia o no
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let Certificados(ByVal vData As Variant)
    m_vCertificados = vData
End Property
''' <summary>
''' Establecer la variable si tiene NoConformidades la compa�ia o no
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let NoConformidades(ByVal vData As Variant)
    m_vNoConformidades = vData
End Property
''' <summary>
''' Establecer la variable si puede crear NoConformidades la compa�ia o no
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let NoConfNuevas(ByVal vData As Variant)
    m_vNoConfNuevas = vData
End Property
''' <summary>
''' Establecer la variable si esta de baja en calidad o no
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let BajaCalidad(ByVal vData As Variant)
    m_vBajaCalidad = vData
End Property
''' <summary>
''' Establecer la variable Variables Calidad publicadas
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let QaVarCal(ByVal vData As Variant)
    m_vQaVarCal = vData
End Property
''' <summary>
''' Establecer la variable CSRFToken
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let CSRFToken(ByVal vData As Variant)
    m_vCSRFToken = vData
End Property
''' <summary>
''' Establecer la variable si tiene SolicitudesFsga la compa�ia o no
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let SolicitudesFsga(ByVal vData As Variant)
    m_vSolicitudesFsga = vData
End Property
''' <summary>
''' Establecer la variable Identificador de Orden
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let IDOrden(ByVal vData As Variant)
    m_vIdOrden = vData
End Property
''' <summary>
''' Establecer la variable caducidad sesion
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let CadSesion(ByVal vData As Integer)
    m_iCadSesion = vData
End Property
''' <summary>
''' Establecer la variable fecha hora inicio sesion
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let FecIniSes(ByVal vData As Variant)
    m_vFecIniSes = vData
End Property
''' <summary>
''' Establecer la variable fecha hora fin sesion
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let FecFinSes(ByVal vData As Variant)
    m_vFecFinSes = vData
End Property
''' <summary>
''' Establecer la variable si es sesion activa o no
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let Activa(ByVal vData As Variant)
    m_vActiva = vData
End Property
''' <summary>
''' Establecer la variable ID de sesion guardado en cooki persistente
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let PersistID(ByVal vData As Variant)
    m_vPersistID = vData
End Property
''' <summary>
''' Establecer la variable Fecha de actualizacion
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let FecAct(ByVal vData As Variant)
    m_vFecAct = vData
End Property
''' <summary>
''' Establecer la variable formato caracter decimales
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let DecimalFMT(ByVal Data As Variant)
    m_vDecimalFMT = IIf(IsNull(Data), "", Data)
End Property
''' <summary>
''' Establecer la variable formato caracter miles
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let ThousanFMT(ByVal Data As Variant)
    m_vThousanFMT = IIf(IsNull(Data), "", Data)
End Property
''' <summary>
''' Establecer la variable formato fechas
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let DateFMT(ByVal Data As Variant)
    m_vDateFMT = IIf(IsNull(Data), "", Data)
End Property
''' <summary>
''' Establecer la variable formato numeros
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let MostrarFMT(ByVal Data As Integer)
    m_iMostrarFMT = Data
End Property
''' <summary>
''' Establecer la variable numero decimales
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let PrecisionFMT(ByVal Data As Variant)
    m_vPrecisionFMT = Data
End Property
''' <summary>
''' Establecer la variable Tipo Mail usuario
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let TipoMail(ByVal Data As Variant)
    If IsNull(Data) Then
        m_vTipoMail = 0
    Else
        m_vTipoMail = Data
    End If
End Property
''' <summary>
''' Establecer la variable idioma usuario
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let Idioma(ByVal Ind As Variant)
    m_vIdi = Ind
End Property

''' <summary>
''' Devolver la variable Autofactura
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let Autofactura(ByVal Data As Variant)
    If IsNull(Data) Then
        m_vAutofactura = 0
    Else
        m_vAutofactura = Data
    End If
End Property

''' <summary>
''' Establecer la variable Conexion
''' </summary>
''' <param name="oCon">Conexion</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Set Conexion(ByVal oCon As CConexion)
    Set m_oConexion = oCon
End Property
''' <summary>
''' Establecer la variable Parametros
''' </summary>
''' <param name="oParametros">Parametros</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Set Parametros(ByVal oParametros As CGestorParametros)
    Set m_oParametros = oParametros
End Property

''' <summary>
''' Devolver la variable Id de sesion
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get id() As String
    id = m_sID
End Property
''' <summary>
''' Devolver la variable ID de la compa�ia compradora
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get CiaComp() As Long
    CiaComp = m_lCiaComp
End Property
''' <summary>
''' Devolver la variable ID de compa�ia proveedora
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get CiaId() As Long
    CiaId = m_lCiaId
End Property
''' <summary>
''' Devolver la variable codigo de la compa�ia compradora
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get CiaCod() As String
    CiaCod = m_sCiaCod
End Property
''' <summary>
''' Devolver la variable codigo de la compa�ia compradora en la bbdd GS
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get CiaCodGs() As String
    CiaCodGs = m_sCiaCodGs
End Property
''' <summary>
''' Devolver la variable id de usuario
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get UsuId() As Long
    UsuId = m_lUsuId
End Property
''' <summary>
''' Devolver la variable codigo de usuario
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get UsuCod() As String
    UsuCod = m_sUsuCod
End Property
''' <summary>
''' Devolver la variable direcci�n IP desde la que se ha hecho la petici�n
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get IPDir() As String
    IPDir = m_sIPDir
End Property
''' <summary>
''' Devolver la variable Pyme
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get PYME() As Variant
    PYME = m_vPyme
End Property
''' <summary>
''' Devolver la variable si es usuario principal o no
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get UsuPpal() As Variant
    UsuPpal = m_vUsuPpal
End Property
''' <summary>
''' Devolver la variable Pedidos
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get Pedidos() As Variant
    Pedidos = m_vPedidos
End Property
''' <summary>
''' Devolver la variable Pedidos Pendientes
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get PedPendientes() As Variant
    PedPendientes = m_vPedPendientes
End Property
''' <summary>
''' Devolver la variable si tiene PMSOLICITUDES la compa�ia o no
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get PmSolicitudes() As Variant
    PmSolicitudes = m_vPmSolicitudes
End Property
''' <summary>
''' Devolver la variable si tiene CN la compa�ia o no
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get CN() As Variant
    CN = m_vCn
End Property
''' <summary>
''' Devolver la variable si tiene Certificados la compa�ia o no
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get Certificados() As Variant
    Certificados = m_vCertificados
End Property
''' <summary>
''' Devolver la variable si tiene NoConformidades la compa�ia o no
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get NoConformidades() As Variant
    NoConformidades = m_vNoConformidades
End Property
''' <summary>
''' Devolver la variable si puede crear NoConformidades la compa�ia o no
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get NoConfNuevas() As Variant
    NoConfNuevas = m_vNoConfNuevas
End Property
''' <summary>
''' Devolver la variable si esta de baja en calidad o no
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get BajaCalidad() As Variant
    BajaCalidad = m_vBajaCalidad
End Property
''' <summary>
''' Devolver la variable Variables Calidad publicadas
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get QaVarCal() As Variant
    QaVarCal = m_vQaVarCal
End Property
''' <summary>
''' Devolver la variable CSRFToken
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get CSRFToken() As Variant
    CSRFToken = m_vCSRFToken
End Property
''' <summary>
''' Devolver la variable si tiene SolicitudesFsga la compa�ia o no
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get SolicitudesFsga() As Variant
    SolicitudesFsga = m_vSolicitudesFsga
End Property
''' <summary>
''' Devolver la variable Identificador de Orden
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get IDOrden() As Variant
    IDOrden = m_vIdOrden
End Property
''' <summary>
''' Devolver la variable caducidad sesion
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get CadSesion() As Integer
    CadSesion = m_iCadSesion
End Property
''' <summary>
''' Devolver la variable fecha hora inicio sesion
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get FecIniSes() As Variant
    FecIniSes = m_vFecIniSes
End Property
''' <summary>
''' Devolver la variable fecha hora fin sesion
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get FecFinSes() As Variant
    FecFinSes = m_vFecFinSes
End Property
''' <summary>
''' Devolver la variable si es sesion activa o no
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get Activa() As Variant
    Activa = m_vActiva
End Property
''' <summary>
''' Devolver la variable ID de sesion guardado en cooki persistente
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get PersistID() As Variant
    PersistID = m_vPersistID
End Property
''' <summary>
''' Devolver la variable Fecha de actualizacion
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property
''' <summary>
''' Devolver la variable Conexion
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property
''' <summary>
''' Devolver la variable Parametros
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get Parametros() As CGestorParametros
    Set Parametros = m_oParametros
End Property
''' <summary>
''' Devolver la variable Formato caracter decimales
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get DecimalFMT() As Variant
    DecimalFMT = m_vDecimalFMT
End Property
''' <summary>
''' Devolver la variable Formato caracter miles
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get ThousanFMT() As Variant
    ThousanFMT = m_vThousanFMT
End Property
''' <summary>
''' Devolver la variable Formato fechas
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get DateFMT() As Variant
    DateFMT = m_vDateFMT
End Property
''' <summary>
''' Devolver la variable Formato numeros
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get MostrarFMT() As Integer
    MostrarFMT = m_iMostrarFMT
End Property
''' <summary>
''' Devolver la variable Formato numero decimales
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get PrecisionFMT() As Variant
    PrecisionFMT = m_vPrecisionFMT
End Property
''' <summary>
''' Devolver la variable Tipo email usuario
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get TipoMail() As Variant
    TipoMail = m_vTipoMail
End Property
''' <summary>
''' Devolver la variable idioma usuario
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get Idioma() As Variant
    Idioma = m_vIdi
End Property

''' <summary>
''' Devolver la variable autofactura del usuario
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get Autofactura() As Variant
    Autofactura = m_vAutofactura
End Property


Public Property Let CadPassword(ByVal dateValue As Boolean)
    b_CadPassword = dateValue
End Property
Public Property Get CadPassword() As Boolean
    CadPassword = b_CadPassword
End Property

Public Property Let FecPWD(ByVal dateValue As Date)
    d_FecPWD = dateValue
End Property
Public Property Get FecPWD() As Date
    FecPWD = d_FecPWD
End Property

''' <summary>
''' Determinar si existe una sesi�n v�lida para el mismo usuario. Si lo es se comprobar� que la direcci�n IP sea la misma y que el
''' par�metro PersistID sea el mismo que SESION.PERSISTID.  si esto se cumple quiere decir que es el mismo usuario el que quiere hacer
''' logon y se lo permitimos.
''' </summary>
''' <param name="Cia">C�digo de compania</param>
''' <param name="UsuCod">C�digo de usuario</param>
''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
''' <param name="PersistID">Valor de la cookie persistente</param>
''' <returns>Si es sesi�n valida (activa y mismos ips o sin sesion en bbdd) o no (activa pero no de los mismos ips)</returns>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: Jbg. Fecha: 01/08/2012
Public Function PasaCtrlExistenciaSesionUsuario(ByVal Cia As String, ByVal UsuCod As String, ByVal IPDir As String, ByVal PersistID As String _
) As Integer
    Dim ador As ADODB.Recordset
    Dim sConsulta As String
    
    sConsulta = "SELECT ID,IPDIR,PERSISTID FROM SESION WITH(NOLOCK) WHERE CIACOD=" & StrToSQLNULL(Cia) & " AND USUCOD=" & StrToSQLNULL(UsuCod)
    sConsulta = sConsulta & " AND FECFINSES>GETDATE() AND ACTIVA=1"
    
    Set ador = New ADODB.Recordset
    ador.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    If ador.eof Then
        PasaCtrlExistenciaSesionUsuario = 2
        
    ElseIf ador("IPDIR").Value = IPDir And ador("PERSISTID").Value = PersistID Then
        PasaCtrlExistenciaSesionUsuario = 1
        
        sConsulta = "UPDATE SESION SET ACTIVA=0 WHERE ID=" & StrToSQLNULL(ador("ID").Value)
        m_oConexion.AdoCon.Execute sConsulta
        
        ador.Close
    Else
        PasaCtrlExistenciaSesionUsuario = 0
        
        ador.Close
    End If
    
    Set ador = Nothing
End Function

''' <summary>
''' Valida y Cargar las propiedades de sesion
''' </summary>
''' <param name="SesionId">Id de sesion</param>
''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
''' <param name="PersistID">Valor de la cookie persistente</param>
''' <param name="NoCargarSesion">True objeto sesion solo con ID. false objeto sesion cargas todo </param>
''' <param name="BToken">-1- no hacer nada 0- limpia token contra Cross Site Request Forgery 1- generar token contra Cross Site Request Forgery</param>
''' <returns>Si es sesi�n valida o no</returns>
''' <remarks>Llamada desde: CRaiz.ValidarSesion ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: Jbg. Fecha: 01/08/2012
Public Function CargarDatosSesion(ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal NoCargarSesion As Boolean, ByVal BToken As Integer) As Boolean
    Dim ador As ADODB.Recordset
    Dim sConsulta As String
    
    sConsulta = "SELECT CIACOMP,PYME,CIAID,CIACOD,CIACODGS,USUID,USUCOD,IPDIR,USUPPAL,PEDIDOS,PEDPENDIENTES,PMSOLICITUDES,CERTIFICADOS,NOCONFORMIDADES"
    sConsulta = sConsulta & ",NOCONFNUEVAS,FSGAIDUSU,IDORDEN,BAJACALIDAD,QAVARCAL,CSRFToken,FECINISES,FECFINSES,ACTIVA,PERSISTID,FECACT, A.CADSESION, CN, AUTOFACTURA "
    sConsulta = sConsulta & " FROM SESION WITH(NOLOCK)"
    sConsulta = sConsulta & " , (SELECT TOP 1 CADSESION FROM PARGEN_INTERNO WITH(NOLOCK)) A"
    sConsulta = sConsulta & " WHERE ID=" & StrToSQLNULL(SesionId) & " AND FECFINSES > GETDATE() AND ACTIVA=1"
    sConsulta = sConsulta & " AND IPDIR=" & StrToSQLNULL(IPDir) & " AND PERSISTID=" & StrToSQLNULL(PersistID)
    
    Set ador = New ADODB.Recordset
    ador.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        
    If ador.eof Then
        'sesion no valida
        CargarDatosSesion = False
        
        Set ador = Nothing
        
        Exit Function
    ElseIf NoCargarSesion Then
        m_sID = SesionId
        
        m_iCadSesion = ador("CADSESION").Value
    Else
        m_sID = SesionId
        m_lCiaComp = ador("CIACOMP").Value
        m_vPyme = ador("PYME").Value
        m_lCiaId = ador("CIAID").Value
        m_sCiaCod = ador("CIACOD").Value
        m_sCiaCodGs = NullToStr(ador("CIACODGS").Value)
        m_lUsuId = ador("USUID").Value
        m_sUsuCod = ador("USUCOD").Value
        m_sIPDir = ador("IPDIR").Value
                
        m_vUsuPpal = ador("USUPPAL").Value
        m_vPedidos = ador("PEDIDOS").Value
        m_vPedPendientes = ador("PEDPENDIENTES").Value
        m_vPmSolicitudes = ador("PMSOLICITUDES").Value
        m_vCertificados = ador("CERTIFICADOS").Value
        m_vNoConformidades = ador("NOCONFORMIDADES").Value
        m_vNoConfNuevas = ador("NOCONFNUEVAS").Value
        m_vSolicitudesFsga = ador("FSGAIDUSU").Value
        m_vIdOrden = ador("IDORDEN").Value
        m_vBajaCalidad = ador("BAJACALIDAD").Value
        m_vQaVarCal = ador("QAVARCAL").Value
        
        m_vFecIniSes = ador("FECINISES").Value
        m_vFecFinSes = ador("FECFINSES").Value
        m_vActiva = ador("ACTIVA").Value
        m_vPersistID = ador("PERSISTID").Value
        m_vCn = ador("CN").Value
        m_vAutofactura = ador("AUTOFACTURA").Value
        
        m_vFecAct = ador("FECACT").Value
        
        m_iCadSesion = ador("CADSESION").Value
        
        If Not (BToken = 1) Then
            m_vCSRFToken = ""
        Else
            m_vCSRFToken = Left(GenerarStringAltaEntropia(CStr(GetTickCount) & m_sUsuCod & m_sIPDir), 20)
        End If
    End If
    
    CargarDatosSesion = True
        
    ador.Close
        
    Set ador = Nothing
            
    sConsulta = "UPDATE SESION SET FECFINSES=DATEADD(minute," & CStr(m_iCadSesion) & ",GETDATE())"
    
    If (BToken = 0 Or BToken = 1) Then sConsulta = sConsulta & ", CSRFTOKEN=" & StrToSQLNULL(m_vCSRFToken)
    
    sConsulta = sConsulta & " WHERE ID=" & StrToSQLNULL(m_sID)
    
    m_oConexion.AdoCon.Execute sConsulta
    
End Function

''' <summary>
''' Crea una sesion con las propiedades dadas
''' </summary>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: Jbg. Fecha: 01/08/2012
Public Function CreaDatosSesion() As CTESError
    Dim TESError  As CTESError
    Dim sConsulta As String

    Set TESError = New CTESError
    TESError.NumError = TESnoerror
    
    On Error GoTo Error
    
    sConsulta = "INSERT INTO SESION(ID,CIACOMP,PYME,CIAID,CIACOD,CIACODGS,USUID,USUCOD,IPDIR,USUPPAL,PEDIDOS,PEDPENDIENTES,PMSOLICITUDES,CERTIFICADOS"
    sConsulta = sConsulta & ",NOCONFORMIDADES,NOCONFNUEVAS,FSGAIDUSU,IDORDEN,BAJACALIDAD,QAVARCAL,CSRFToken,FECINISES,FECFINSES,ACTIVA,PERSISTID,FECACT,CN,AUTOFACTURA)"
    sConsulta = sConsulta & "VALUES (" & StrToSQLNULL(m_sID) & "," & m_lCiaComp & "," & StrToSQLNULL(m_vPyme) & "," & m_lCiaId & "," & StrToSQLNULL(m_sCiaCod)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sCiaCodGs) & "," & m_lUsuId & "," & StrToSQLNULL(UsuCod)
    sConsulta = sConsulta & "," & StrToSQLNULL(m_sIPDir) & "," & m_vUsuPpal & "," & m_vPedidos & "," & m_vPedPendientes & "," & m_vPmSolicitudes
    sConsulta = sConsulta & "," & m_vCertificados & "," & m_vNoConformidades & "," & m_vNoConfNuevas & "," & m_vSolicitudesFsga & "," & m_vIdOrden
    sConsulta = sConsulta & "," & m_vBajaCalidad & "," & m_vQaVarCal & "," & StrToSQLNULL(m_vCSRFToken)
    sConsulta = sConsulta & ",GETDATE(),DATEADD(minute," & CStr(m_iCadSesion) & ",GETDATE()) ," & m_vActiva & "," & StrToSQLNULL(m_vPersistID) & ",GETDATE()," & m_vCn & "," & m_vAutofactura & ")"
    
    m_oConexion.AdoCon.Execute sConsulta
    
    Set CreaDatosSesion = TESError
    
    Exit Function
    
Error:
    Set TESError = basErrores.TratarError(m_oConexion.AdoCon.Errors)
    
    Set CreaDatosSesion = TESError
End Function

''' <summary>
''' Cerrar la sesion
''' </summary>
''' <param name="SesionId">Id de sesion</param>
''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
''' <param name="PersistID">Valor de la cookie persistente</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: craiz/DesconectarUsuario ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: Jbg. Fecha: 13/08/2012
Public Function DesconectarUsuario(ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String) As CTESError
    Dim TESError  As CTESError
    Dim sConsulta As String

    Set TESError = New CTESError
    TESError.NumError = TESnoerror
    
    On Error GoTo Error
    
    sConsulta = "UPDATE SESION SET ACTIVA=0 WHERE ID=" & StrToSQLNULL(SesionId)
    sConsulta = sConsulta & " AND IPDIR=" & StrToSQLNULL(IPDir) & " AND PERSISTID=" & StrToSQLNULL(PersistID)
    
    m_oConexion.AdoCon.Execute sConsulta
     
    Set DesconectarUsuario = TESError
    
    Exit Function
    
Error:
    Set TESError = basErrores.TratarError(m_oConexion.AdoCon.Errors)
    
    Set DesconectarUsuario = TESError
End Function

''' <summary>
''' Determinar si es CSRFToken valido o no
''' </summary>
''' <param name="SesionId">Id de sesion</param>
''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
''' <param name="PersistID">Valor de la cookie persistente</param>
''' <param name="CSRFToken">token contra Cross Site Request Forgery</param>
''' <returns>Si es CSRFToken valido o no</returns>
''' <remarks>Llamada desde: craiz/ValidarSesion ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: Jbg. Fecha: 21/11/2012
Public Function PasaCtrlCSRFToken(ByVal SesionId As String, ByVal IPDir As String, ByVal PersistID As String, ByVal CSRFToken As String _
) As Boolean
    Dim ador As ADODB.Recordset
    Dim sConsulta As String
    
    sConsulta = "SELECT 1 FROM SESION WITH(NOLOCK) WHERE ID=" & StrToSQLNULL(SesionId) & " AND IPDIR=" & StrToSQLNULL(IPDir)
    sConsulta = sConsulta & " AND PERSISTID=" & StrToSQLNULL(PersistID) & " AND CSRFTOKEN=" & StrToSQLNULL(CSRFToken)
    
    Set ador = New ADODB.Recordset
    ador.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    If (Not ador.eof) Then
        PasaCtrlCSRFToken = True
        ador.Close
    Else
        PasaCtrlCSRFToken = False
    End If
    
    Set ador = Nothing
    
    sConsulta = "UPDATE SESION SET CSRFTOKEN=NULL WHERE ID=" & StrToSQLNULL(SesionId)
    
    m_oConexion.AdoCon.Execute sConsulta
End Function

''' <summary>Obtiene los datos de la �ltima sesi�n activa de un usuario</summary>
''' <param name="Cia">C�digo de compania</param>
''' <param name="UsuCod">C�digo de usuario</param>
''' <param name="IPDir">Ip desde la que se realiza la petici�n</param>
''' <returns>objeto sesi�n con los datos de la �ltima sesi�n activa</returns>
''' <remarks>Llamada desde: CRaiz.ValidarLogin</remarks>

Public Sub ObtenerDatosUltimaSesionActivaUsuario(ByVal Cia As String, ByVal UsuCod As String, ByVal IPDir As String, ByRef oParametros As CGestorParametros)
    Dim sConsulta As String
    Dim oCom As ADODB.Command
    Dim oParam As ADODB.Parameter
    Dim rsDatos As ADODB.Recordset
    
    sConsulta = "SELECT TOP 1 S.ID,S.PERSISTID,S.CIACOMP,S.PYME,S.CIAID,S.CIACOD,S.CIACODGS,S.USUID,S.USUCOD,S.USUPPAL,S.IPDIR,S.PEDIDOS,S.PEDPENDIENTES,S.FSGAIDUSU,S.CN,S.PMSOLICITUDES,S.CERTIFICADOS,"
    sConsulta = sConsulta & "S.NOCONFNUEVAS,S.NOCONFORMIDADES,S.BAJACALIDAD,S.QAVARCAL,S.IDORDEN,U.MOSTRARFMT,U.DECIMALFMT,U.THOUSANFMT,U.PRECISIONFMT,U.DATEFMT,U.TIPOEMAIL,U.IDI,I.COD IDICOD,"
    sConsulta = sConsulta & "S.AUTOFACTURA "
    sConsulta = sConsulta & "FROM SESION S WITH (NOLOCK) "
    sConsulta = sConsulta & "INNER JOIN USU U WITH (NOLOCK) ON U.CIA=S.CIAID AND U.COD=S.USUCOD "
    sConsulta = sConsulta & "INNER JOIN IDI I WITH(NOLOCK) ON U.IDI=I.ID "
    sConsulta = sConsulta & "WHERE S.CIACOD=? AND S.USUCOD=? AND S.IPDIR=? AND S.ACTIVA=1 AND FECFINSES>GETDATE() "
    sConsulta = sConsulta & "ORDER BY FECACT DESC"
    
    Set oCom = New ADODB.Command
    With oCom
        Set .ActiveConnection = m_oConexion.AdoCon
        .CommandType = adCmdText
        .CommandText = sConsulta
        
        Set oParam = .CreateParameter("CIACOD", adVarChar, adParamInput, Len(Cia), Cia)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("USUCOD", adVarChar, adParamInput, Len(UsuCod), UsuCod)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("IPDIR", adVarChar, adParamInput, Len(IPDir), IPDir)
        .Parameters.Append oParam
        
        Set rsDatos = .Execute
    End With
    Set oCom = Nothing
    Set oParam = Nothing
    
    Set Parametros = oParametros
    If Not rsDatos Is Nothing Then
        If rsDatos.RecordCount > 0 Then
            id = rsDatos("ID")
            If Not IsNull(rsDatos("PERSISTID")) Then PersistID = rsDatos("PERSISTID")
            CiaComp = rsDatos("CIACOMP")
            If Not IsNull(rsDatos("PYME")) Then PYME = rsDatos("PYME")
            CiaId = rsDatos("CIAID")
            CiaCod = rsDatos("CIACOD")
            CiaCodGs = NullToStr(rsDatos("CIACODGS"))
            UsuId = rsDatos("USUID")
            UsuCod = rsDatos("USUCOD")
            UsuPpal = rsDatos("USUPPAL")
            IPDir = rsDatos("IPDIR")
            If Not IsNull(rsDatos("PEDIDOS")) Then Pedidos = rsDatos("PEDIDOS")
            If Not IsNull(rsDatos("PEDPENDIENTES")) Then PedPendientes = rsDatos("PEDPENDIENTES")
            If Not IsNull(rsDatos("FSGAIDUSU")) Then SolicitudesFsga = rsDatos("FSGAIDUSU")
            If Not IsNull(rsDatos("CN")) Then CN = rsDatos("CN")
            If Not IsNull(rsDatos("PMSOLICITUDES")) Then PmSolicitudes = rsDatos("PMSOLICITUDES")
            If Not IsNull(rsDatos("CERTIFICADOS")) Then Certificados = rsDatos("CERTIFICADOS")
            If Not IsNull(rsDatos("NOCONFNUEVAS")) Then NoConfNuevas = rsDatos("NOCONFNUEVAS")
            If Not IsNull(rsDatos("NOCONFORMIDADES")) Then NoConformidades = rsDatos("NOCONFORMIDADES")
            If Not IsNull(rsDatos("BAJACALIDAD")) Then BajaCalidad = rsDatos("BAJACALIDAD")
            If Not IsNull(rsDatos("QAVARCAL")) Then QaVarCal = rsDatos("QAVARCAL")
            If Not IsNull(rsDatos("IDORDEN")) Then IDOrden = rsDatos("IDORDEN")
            Activa = 1
            MostrarFMT = rsDatos("MOSTRARFMT")
            DecimalFMT = NullToStr(rsDatos("DECIMALFMT"))
            ThousanFMT = NullToStr(rsDatos("THOUSANFMT"))
            If DecimalFMT = "" Or ThousanFMT = "" Then
                DecimalFMT = ","
                ThousanFMT = "."
            End If
            PrecisionFMT = rsDatos("PRECISIONFMT")
            DateFMT = NullToStr(rsDatos("DATEFMT"))
            If DateFMT = "" Then DateFMT = "dd/mm/yyyy"
            TipoMail = rsDatos("TIPOEMAIL")
            Idioma = rsDatos("IDICOD")
            If Not IsNull(rsDatos("AUTOFACTURA")) Then Autofactura = rsDatos("AUTOFACTURA")
            CadSesion = Parametros.CaducidadSesion
        End If
    End If
End Sub

