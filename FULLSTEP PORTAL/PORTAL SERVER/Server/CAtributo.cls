VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAtributo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

Private moConexion As CConexion
Private mvId As Variant
Private mvPAId As Variant
Private msCod As String
Private msDen As String
Private miIntro As Integer
Private mudtTipo As TiposDeAtributos
Private mvValor As Variant
Private mvValorMin As Variant
Private mvValorMax As Variant
Private mbObligatorio As Boolean
Private msOperacion As Variant
Private miAplicar As Variant
Private moValores As CValoresAtrib
Private mvDescripcion As Variant
Private miAmbito As Variant
Private mdImporteParcial As Double
Private moGrupos As CGruposCosteDesc
Private mvAlcance As Variant 'Contendrá el campo PROCE_ATRIB.GRUPO

Private mbHayEscalados As Boolean
Private m_oEscalados As CEscalados ' Posibles escalados para costes/descuentos
Private m_oImpuestosCostes As CImpuestos
Private m_udtTipoIntroduccion As TAtributoIntroduccion
Private m_vPrecioFormula As Variant
Private m_vPrecioAplicarA As Variant
Private m_oListaPonderacion As CValoresPond
Private m_vValorNum As Variant
Private m_vIdCD As Variant
Private m_dblImporte As Double  'Costes/descuentos
Private m_bFacturar As Boolean 'Costes/descuentos

Friend Property Set Conexion(ByVal vData As CConexion)
    Set moConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = moConexion
End Property
Public Property Let id(ByVal vData As Variant)
    mvId = vData
End Property
Public Property Get id() As Variant
    id = mvId
End Property
Public Property Let PAID(ByVal vData As Variant)
    mvPAId = vData
End Property
Public Property Get PAID() As Variant
    PAID = mvPAId
End Property
Public Property Let Cod(ByVal sData As String)
    msCod = sData
End Property
Public Property Get Cod() As String
    Cod = msCod
End Property
Public Property Let Den(ByVal sData As String)
    msDen = sData
End Property
Public Property Get Den() As String
    Den = msDen
End Property
Public Property Let Intro(ByVal iValor As Integer)
    miIntro = iValor
End Property
Public Property Get Intro() As Integer
    Intro = miIntro
End Property

Public Property Let Tipo(ByVal udtData As TiposDeAtributos)
    mudtTipo = udtData
End Property
Public Property Get Tipo() As TiposDeAtributos
    Tipo = mudtTipo
End Property

Public Property Let Obligatorio(ByVal bOblig As Boolean)
    mbObligatorio = bOblig
End Property
Public Property Get Obligatorio() As Boolean
    Obligatorio = mbObligatorio
End Property

Public Property Let Operacion(ByVal sOp As Variant)
    msOperacion = sOp
End Property
Public Property Get Operacion() As Variant
    Operacion = msOperacion
End Property
Public Property Let Alcance(ByVal sOp As Variant)
    mvAlcance = sOp
End Property
Public Property Get Alcance() As Variant
    Alcance = mvAlcance
End Property

Public Property Let Aplicar(ByVal iAplic As Variant)
    miAplicar = iAplic
End Property
Public Property Get Aplicar() As Variant
    Aplicar = miAplicar
End Property
Public Property Let Ambito(ByVal iAmb As Variant)
    miAmbito = iAmb
End Property
Public Property Get Ambito() As Variant
    Ambito = miAmbito
End Property


Public Property Get Valor() As Variant
    Valor = mvValor
End Property
Public Property Let Valor(ByVal vData As Variant)
On Error Resume Next
Select Case mudtTipo
    Case 1:
        mvValor = CStr(vData)
    Case 2:
        mvValor = CDbl(vData)
    Case 3:
        mvValor = CDate(vData)
    Case 4:
        mvValor = CBool(vData)
End Select
                
If IsEmpty(mvValor) Then
    mvValor = Null
End If
End Property

Public Property Get ValorMin() As Variant
    ValorMin = mvValorMin
End Property
Public Property Let ValorMin(ByVal vData As Variant)
    mvValorMin = vData
End Property
Public Property Get ValorMax() As Variant
    ValorMax = mvValorMax
End Property
Public Property Let ValorMax(ByVal vData As Variant)
    mvValorMax = vData
End Property


Public Property Get Valores() As CValoresAtrib
    Set Valores = moValores
End Property

Public Property Set Valores(oData As CValoresAtrib)
    Set moValores = oData
End Property


Public Property Let Descripcion(ByVal sDescr As Variant)
    mvDescripcion = sDescr
End Property
Public Property Get Descripcion() As Variant
    Descripcion = mvDescripcion
End Property

Public Property Let ImporteParcial(ByVal vImporteParcial As Double)
    mdImporteParcial = vImporteParcial
End Property
Public Property Get ImporteParcial() As Double
    ImporteParcial = mdImporteParcial
End Property

Public Property Get Grupos() As CGruposCosteDesc
    Set Grupos = moGrupos
End Property

Public Property Set Grupos(oData As CGruposCosteDesc)
    Set moGrupos = oData
End Property

'DPD - Propiedades para el manejo de precios escalados
Public Property Get Escalados() As CEscalados
    Set Escalados = m_oEscalados
End Property

Public Property Set Escalados(ByVal oEscalados As CEscalados)
    Set m_oEscalados = oEscalados
End Property


Public Property Get HayEscalados() As Boolean
    HayEscalados = mbHayEscalados
End Property
Public Property Let HayEscalados(ByVal bValor As Boolean)
    mbHayEscalados = bValor
End Property
Public Property Set ImpuestosCostes(ByVal vData As CImpuestos)
    Set m_oImpuestosCostes = vData
End Property
Public Property Get ImpuestosCostes() As CImpuestos
    Set ImpuestosCostes = m_oImpuestosCostes
End Property
Public Property Get PrecioFormula() As Variant
    PrecioFormula = m_vPrecioFormula
End Property
Public Property Let PrecioFormula(ByVal vData As Variant)
    m_vPrecioFormula = vData
End Property
Public Property Let TipoIntroduccion(ByVal vData As TAtributoIntroduccion)
    m_udtTipoIntroduccion = vData
End Property
Public Property Get TipoIntroduccion() As TAtributoIntroduccion
    TipoIntroduccion = m_udtTipoIntroduccion
End Property
Public Property Set ListaPonderacion(ByVal vData As CValoresPond)
    Set m_oListaPonderacion = vData
End Property
Public Property Get ListaPonderacion() As CValoresPond
    Set ListaPonderacion = m_oListaPonderacion
End Property
Public Property Let ValorNum(ByVal vData As Variant)
    m_vValorNum = vData
End Property
Public Property Get ValorNum() As Variant
    ValorNum = m_vValorNum
End Property
Public Property Get IdCD() As Variant
    IdCD = m_vIdCD
End Property
Public Property Let IdCD(ByVal vData As Variant)
    m_vIdCD = vData
End Property

Public Property Get Importe() As Double
    Importe = m_dblImporte
End Property
Public Property Let Importe(ByVal vData As Double)
    m_dblImporte = vData
End Property

Public Property Get Facturar() As Boolean
    Facturar = m_bFacturar
End Property
Public Property Let Facturar(ByVal vData As Boolean)
    m_bFacturar = vData
End Property
Public Property Let PrecioAplicarA(ByVal vData As Variant)
    m_vPrecioAplicarA = vData
End Property
Public Property Get PrecioAplicarA() As Variant
    PrecioAplicarA = m_vPrecioAplicarA
End Property

Public Sub cargarImpuestos(IDLinea As Long, ByVal Idioma As String, ByVal CiaComp As String)
    Set ImpuestosCostes = Nothing
    Set ImpuestosCostes = New CImpuestos
    Set ImpuestosCostes.Conexion = moConexion
    ImpuestosCostes.CargarImpuestoCostesLineaDesglose IDLinea, Idioma, CiaComp, id
End Sub

Public Function EsCoste() As Boolean
    EsCoste = False
        
    Select Case m_vPrecioFormula
        Case "+"
            If NullToDbl0(m_vValorNum) >= 0 Then EsCoste = True
        Case "-"
            If NullToDbl0(m_vValorNum) < 0 Then EsCoste = True
        Case "/"
            If NullToDbl0(m_vValorNum) > 0 Then EsCoste = True
        Case "*"
            If NullToDbl0(m_vValorNum) >= 0 Then EsCoste = True
        Case "+%"
            If NullToDbl0(m_vValorNum) >= 0 Then EsCoste = True
        Case "-%"
            If NullToDbl0(m_vValorNum) < 0 Then EsCoste = True
    End Select
End Function
