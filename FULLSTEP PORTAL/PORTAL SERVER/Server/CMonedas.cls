VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMonedas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' *** Clase: CMonedas
''' *** Creacion: 31/08/1998 (Javier Arana)
''' *** Ultima revision: 31/05/1999 (Alfredo Magallon)

''' Variables que almacenan las propiedades de los objetos

Private mCol As Collection
Private mvarEOF As Boolean
Private mvarConexion As CConexion

''' Control de errores

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Function Add(ByVal Cod As String, ByVal Den As String, ByVal EQUIV As Double, Optional ByVal FecAct As Variant, Optional ByVal varIndice As Variant, Optional ByVal id As Variant) As CMoneda
    
    ''' * Objetivo: Anyadir una moneda a la coleccion
    ''' * Recibe: Datos de la moneda
    ''' * Devuelve: Moneda a�adida
    
    Dim objnewmember As CMoneda
    
    ''' Creacion de objeto moneda
    
    Set objnewmember = New CMoneda
   
    ''' Paso de los parametros al nuevo objeto moneda
    
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    objnewmember.EQUIV = EQUIV
    objnewmember.FecAct = FecAct
    Set objnewmember.Conexion = mvarConexion
    
    ''' Anyadir el objeto moneda a la coleccion
    ''' Si no se especifica indice, se anyade al final
    If Not IsMissing(id) Then
        objnewmember.id = id
    Else
        objnewmember.id = Null
    End If
    
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        mCol.Add objnewmember, Cod
    End If
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function
Public Property Get Item(vntIndexKey As Variant) As CMoneda

    ''' * Objetivo: Recuperar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a recuperar
    ''' * Devuelve: Moneda correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property
Public Property Get eof() As Boolean

    ''' * Objetivo: Devolver variable privada EOF
    ''' * Recibe: Nada
    ''' * Devuelve: Variable privada EOF
    
    eof = mvarEOF
    
End Property
Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set mvarConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = mvarConexion
    
End Property
Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    
   
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
    Set mvarConexion = Nothing
        
End Sub

''' <summary>
''' Cargar las monedas hasta un numero maximo desde un codigo o una denominacion determinadas
''' </summary>
''' <param name="sIdi">Idioma</param>
''' <param name="CiaComp">C�digo de compania</param>
''' <param name="NumMaximo">Num Maximo de destinos a cargar</param>
''' <param name="CarIniCod">Codigo destino a cargar</param>
''' <param name="CarIniDen">Denominaci�n destino a cargar</param>
''' <param name="OrdenadosPorDen">Orden por denominaci�n o codigo</param>
''' <param name="IncluirEqNoAct">incluir solo los equivalencia activa</param>
''' <param name="UsarIndice">Al crear la clase cada registro identificarlo por codigo � por numeros correlativos</param>
''' <param name="CoincidenciaTotal">Usar like o =</param>
''' <remarks>Llamada desde; Tiempo m�ximo</remarks>
Public Sub CargarTodasLasMonedasDesde(ByVal sIdi As String, ByVal CiaComp As Long, ByVal NumMaximo As Integer, _
                                      Optional ByVal CarIniCod As String, Optional ByVal CarIniDen As String, _
                                      Optional ByVal OrdenadosPorDen As Boolean, _
                                      Optional ByVal IncluirEqNoAct As Boolean = False, _
                                      Optional ByVal UsarIndice As Boolean, _
                                      Optional ByVal CoincidenciaTotal As Boolean)

    
Dim lIndice As Long
Dim sConsulta As String

Dim sFSG As String

Dim adoComm As adodb.Command
Dim adoRS As adodb.Recordset
Dim adoParam As adodb.Parameter

sIdi = EsIdiomaValido(sIdi)
sFSG = Fsgs(mvarConexion, CiaComp)

Set adoComm = New adodb.Command
Set adoComm.ActiveConnection = mvarConexion.AdoCon
adoComm.CommandType = adCmdText
sConsulta = "EXEC " & sFSG & "SP_DEVOLVER_MONEDAS @COD=?, @DEN =?, @COINCIDE =?, @ORDERDEN =?, @NUMMAXIMO =?,@IDI= ? "
adoComm.CommandText = sConsulta

Set adoParam = adoComm.CreateParameter("COD", adVarWChar, adParamInput, 50, IIf(CarIniCod = "", Null, CarIniCod))
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("DEN", adVarWChar, adParamInput, 500, IIf(CarIniDen = "", Null, CarIniDen))
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("COINCIDE", adTinyInt, adParamInput, , IIf(CoincidenciaTotal, 1, 0))
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("ORDERDEN", adTinyInt, adParamInput, , IIf(OrdenadosPorDen, 1, 0))
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("NUMMAX", adInteger, adParamInput, , NumMaximo)
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("IDI", adVarChar, adParamInput, 20, sIdi)
adoComm.Parameters.Append adoParam


Set adoRS = adoComm.Execute
        
If adoRS.eof Then
        
    adoRS.Close
    Set adoRS = Nothing
    Exit Sub
      
Else
            
    Set mCol = Nothing
    Set mCol = New Collection
        
    If UsarIndice Then
        lIndice = 0
        While Not adoRS.eof
            'Me.Add adoRS("COD").Value, adoRS("DEN").Value, adoRS("EQUIV").Value, SQLBinaryToBoolean(adoRS("EQ_ACT").Value), adoRS("FECACT").Value, lIndice
            Me.Add adoRS("COD").Value, adoRS("DEN").Value, adoRS("EQUIV").Value, adoRS("FECACT").Value, lIndice
            adoRS.MoveNext
            lIndice = lIndice + 1
        Wend
    Else
        While Not adoRS.eof
            'Me.Add adoRS("COD").Value, adoRS("DEN").Value, adoRS("EQUIV").Value, SQLBinaryToBoolean(adoRS("EQ_ACT").Value), adoRS("FECACT").Value
            Me.Add adoRS("COD").Value, adoRS("DEN").Value, adoRS("EQUIV").Value, adoRS("FECACT").Value
            adoRS.MoveNext
        Wend
    End If
    
    adoRS.Close
    Set adoRS = Nothing
      
End If
    
End Sub
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property

''' <summary>
''' Cargar las monedas hasta un numero maximo desde un codigo o una denominacion determinadas
''' </summary>
''' <param name="sIdi">Idioma</param>
''' <param name="NumMaximo">numero maximo de monedas a cargar</param>
''' <param name="OrdenadosPorDen">Ordenar por denominaci�n o por c�digo</param>
''' <remarks>Llamada desde: registro/registro.asp   usuppal/modifcompaniaclient.asp; Tiempo m�ximo: 0,1</remarks>
Public Sub CargarTodasLasMonedasDelPortalDesde(ByVal sIdi As String, ByVal NumMaximo As Integer, Optional ByVal OrdenadosPorDen As Boolean)
    Dim ador As adodb.Recordset
    Dim sConsulta As String
    Dim sPrimeraCond As String
    
    Dim lIndice As Long
    Dim iNumMon As Integer
    Dim SRV As String
    Dim BD As String
    Dim sFSG As String
    
    ''' Precondicion
            
    sIdi = EsIdiomaValido(sIdi)
    
    sConsulta = " DECLARE @IDIOMA INT "
    sConsulta = sConsulta & " SELECT @IDIOMA= ID FROM IDI WITH (NOLOCK) WHERE COD='" & sIdi & "' "
    sConsulta = sConsulta & " SELECT MON.ID,MON.COD,MON_DEN.DEN DEN,MON.EQUIV,MON.FECACT FROM MON WITH(NOLOCK) "
    sConsulta = sConsulta & " LEFT JOIN MON_DEN WITH (NOLOCK) ON MON_DEN.MON=MON.ID AND MON_DEN.IDIOMA=@IDIOMA"
    
    sConsulta = sConsulta & " WHERE EQUIV IS NOT NULL"
    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY ORDEN, DEN "
    Else
        sConsulta = sConsulta & " ORDER BY ORDEN, COD"
    End If
          
    ''' Crear el Resultset
    Set ador = New adodb.Recordset
    
    ador.MaxRecords = NumMaximo + 1
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        
    If ador.eof Then
            
        ador.Close
        Set ador = Nothing
        Exit Sub
          
    Else
        
        ''' Llenar la coleccion a partir del Resultset
        ''' teniendo en cuenta el maximo
        
        Set mCol = Nothing
        Set mCol = New Collection
        
            
        iNumMon = 0
        
        While Not ador.eof And iNumMon < NumMaximo
        
            'Me.Add ador("COD").Value, ador("DEN").Value, NullToDbl0(ador("EQUIV").Value), SQLBinaryToBoolean(ador("EQ_ACT").Value), ador("FECACT").Value, , ador("ID").Value
            Me.Add ador("COD").Value, ador("DEN").Value, NullToDbl0(ador("EQUIV").Value), ador("FECACT").Value, , ador("ID").Value
            ador.MoveNext
            iNumMon = iNumMon + 1
            
        Wend
        
        If Not ador.eof Then
            mvarEOF = False
        Else
            mvarEOF = True
        End If
            
        ador.Close
        Set ador = Nothing
    End If
End Sub
    

