VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CActividadNivel2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CActividadNivel2 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************


Option Explicit
Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarIndice As Long 'Nos indica la posicion secuencial dentro de una Coleccion
Private mvarCod As String
Private mvarDen As String
Private mvarId As Integer
Private mvarAsignada As Boolean

Private mvarACN1 As Integer
Private mvarActividadesNivel3 As CActividadesNivel3
Private mvarConexion As CConexion 'local copy
Private mvarPendiente As Variant

Private mbHayActividadesHijas As Boolean

Public Property Get Pendiente() As Boolean
    Pendiente = mvarPendiente
End Property
Public Property Let Pendiente(ByVal Ind As Boolean)
    mvarPendiente = Ind
End Property

Public Property Get ACN1() As Integer
    ACN1 = mvarACN1
End Property
Public Property Let ACN1(ByVal Ind As Integer)
    mvarACN1 = Ind
End Property
Public Property Get ID() As Integer
    ID = mvarId
End Property
Public Property Let ID(ByVal Ind As Integer)
    mvarId = Ind
End Property
Public Property Get Indice() As Long
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal Ind As Long)
    mvarIndice = Ind
End Property

Public Property Get Asignada() As Boolean
Asignada = mvarAsignada
End Property
Public Property Let Asignada(ByVal bAsignada As Boolean)
mvarAsignada = bAsignada
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Public Property Set ActividadesNivel3(ByVal vData As CActividadesNivel3)
    Set mvarActividadesNivel3 = vData
End Property


Public Property Get ActividadesNivel3() As CActividadesNivel3
    Set ActividadesNivel3 = mvarActividadesNivel3
End Property

Public Property Let Den(ByVal vData As String)
    mvarDen = vData
End Property


Public Property Get Den() As String
    Den = mvarDen
End Property



Public Property Let Cod(ByVal vData As String)
    mvarCod = vData
End Property


Public Property Get Cod() As String
    Cod = mvarCod
End Property


Private Sub Class_Terminate()
    
    Set mvarConexion = Nothing
    
    
End Sub

''' <summary>
''' Cargar todas las actividades de nivel 3
''' </summary>
''' <param name="CodIdioma">Idioma para los textos</param>
''' <param name="CaracteresInicialesCod">Caracteres por los q empieza el c�digo</param>
''' <param name="CaracteresInicialesDen">Caracteres por los q empieza la denominaci�n</param>
''' <param name="CoincidenciaTotal">true usar =    false usar >= </param>
''' <param name="OrdenadosPorDen">Ordenar por denominaci�n o por c�digo</param>
''' <param name="UsarIndice">Al crear la clase cada registro identificarlo por ACT1.ID � por numeros correlativos</param>
''' <remarks>Llamada desde: actividadesciaserver.asp modifactividadesserver.asp; Tiempo m�ximo: 0,1</remarks>
''' Revisado por: jbg; Fecha: 29/10/2012 </remarks>
Public Sub CargarTodasLasActividadesNivel3(ByVal CodIdioma As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean)
    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim ador As ADODB.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
    Dim iNumGrups As Integer
    Dim sDen As String
    Dim objHijo As CActividadNivel3
    
    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
    
    Set adoComm = New ADODB.Command
    Set adoComm.ActiveConnection = mvarConexion.AdoCon
    adoComm.CommandType = adCmdText
    
    sDen = "DEN_" & EsIdiomaValido(CodIdioma)
    
    sConsulta = "SELECT ACT3.ACT1,ACT3.ACT2,ACT3.ID,ACT3.COD,ACT3." & sDen & " FROM ACT3 WITH (NOLOCK) WHERE ACT3.ACT1= ? AND ACT3.ACT2= ?"
    
    Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=mvarACN1)
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=mvarId)
    adoComm.Parameters.Append adoParam
    
    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
    Else
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
            
            If Not CoincidenciaTotal Then
               sConsulta = sConsulta & " AND COD >=? "
               sConsulta = sConsulta & " AND " & sDen & " >=? """
            Else
               sConsulta = sConsulta & " AND COD =? "
               sConsulta = sConsulta & " AND " & sDen & " =? """
            End If
            
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(CaracteresInicialesCod), Value:=CaracteresInicialesCod)
            adoComm.Parameters.Append adoParam
            
            Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(CaracteresInicialesDen), Value:=CaracteresInicialesDen)
            adoComm.Parameters.Append adoParam
        Else
            
            If Not (CaracteresInicialesCod = "") Then
                
               If Not CoincidenciaTotal Then
                    sConsulta = sConsulta & "AND COD >=? "
                Else
                    sConsulta = sConsulta & "AND COD =? "
                End If
                
                Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(CaracteresInicialesCod), Value:=CaracteresInicialesCod)
                adoComm.Parameters.Append adoParam
            Else
                If Not CoincidenciaTotal Then
                    sConsulta = sConsulta & "AND " & sDen & " >=? """
                Else
                    sConsulta = sConsulta & "AND " & sDen & " =? """
                End If
                           
                Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(CaracteresInicialesDen), Value:=CaracteresInicialesDen)
                adoComm.Parameters.Append adoParam
            End If
        
        End If
               
    End If

    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY " & sDen & ",COD"
    Else
        sConsulta = sConsulta & " ORDER BY COD," & sDen
    End If
          
    Set ador = New ADODB.Recordset
            
    adoComm.CommandText = sConsulta
    Set ador = New ADODB.Recordset
            
    Set ador = adoComm.Execute
        
    If ador.eof Then
    
        ador.Close
        Set ador = Nothing
        Set adoParam = Nothing
        Set adoComm = Nothing
        Set mvarActividadesNivel3 = Nothing
        Set mvarActividadesNivel3 = New CActividadesNivel3
       
        Set mvarActividadesNivel3.Conexion = mvarConexion
        
        Exit Sub
          
    Else
        
        Set mvarActividadesNivel3 = Nothing
        Set mvarActividadesNivel3 = New CActividadesNivel3
        Set mvarActividadesNivel3.Conexion = mvarConexion
        
        If UsarIndice Then
        
            lIndice = 0
            iNumGrups = 0
            
            While Not ador.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Set objHijo = mvarActividadesNivel3.Add(ador("ACT1").Value, ador("ACT2").Value, ador("ID").Value, ador("COD").Value, ador(sDen).Value, lIndice)
                objHijo.CargaSiActividadesHijas
                ador.MoveNext
                lIndice = lIndice + 1
                iNumGrups = iNumGrups + 1
            Wend
            
        Else
            
            While Not ador.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                Set objHijo = mvarActividadesNivel3.Add(ador("ACT1").Value, ador("ACT2").Value, ador("ID").Value, ador("COD").Value, ador(sDen).Value)
                objHijo.CargaSiActividadesHijas
                ador.MoveNext
                iNumGrups = iNumGrups + 1
            Wend
        End If
        
        ador.Close
        Set ador = Nothing
        Set adoParam = Nothing
        Set adoComm = Nothing
        Exit Sub
          
    End If

End Sub

''' <summary>
''' Determinar si hay o no actividades hijas
''' </summary>
''' <remarks>Llamada desde: ccompania.DevolverEstructuraActividadesVisible ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: jbg; Fecha: 29/10/2012 </remarks>
Public Sub CargaSiActividadesHijas()
    Dim rs As ADODB.Recordset
    Dim sConsulta As String

    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    Set rs = New ADODB.Recordset
    
    sConsulta = "SELECT COUNT(1) AS CNT FROM ACT3 WITH(NOLOCK) WHERE ACT1=" & mvarACN1 & " AND ACT2=" & mvarId
    
    rs.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

    mbHayActividadesHijas = (rs("CNT") > 0)
End Sub

''' <summary>
''' Establecer si hay o no actividades hijas
''' </summary>
''' <remarks>Llamada desde: usuppal/actividades.asp ; Tiempo m�ximo: 0</remarks>
Public Property Let ExistenActividadesHijas(ByVal vData As Boolean)
    mbHayActividadesHijas = vData
End Property

''' <summary>
''' Devolver si hay o no actividades hijas
''' </summary>
''' <remarks>Llamada desde: usuppal/actividades.asp ; Tiempo m�ximo: 0</remarks>
Public Property Get ExistenActividadesHijas() As Boolean
    ExistenActividadesHijas = mbHayActividadesHijas
End Property
