VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAtriEspecificacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private moItem As CItem
Private moProceso As CProceso
Private moGrupo As CGrupo
Private mvIndice As Variant
Private moConexion As CConexion
Private sFSG As String
Private moCia As CCompania

Private miiid As Integer             'ID
Private miAtrib As Integer          'ATRIB (int, Not Null)
Private miInterno As Integer        'INTERNO (tinyint, Not Null)
Private miPedido As Integer         'PEDIDO (tinyint, Not Null)
Private miOrden As Integer          'ORDEN (smallint, Not Null)
Private miDesc As String
Private midescr As String
Private micod As String
Private mvValor_num As Variant      'VALOR_NUM (float, Null)
Private mvValor_text As Variant     'VALOR_TEXT (varchar(800), Null)
Private mvValor_fec As Variant      'VALOR_FEC (datetime, Null)
Private mvValor_bool As Variant     'VALOR_BOOL (tinyint, Null)
Private mvtipo As Variant
Public Property Let id(ByVal i As Integer)
    miiid = i
End Property
Public Property Get id() As Integer
    id = miiid
End Property
Public Property Let cod(ByVal i As String)
    micod = i
End Property
Public Property Get cod() As String
    cod = micod
End Property
Public Property Let descr(ByVal i As String)
    midescr = i
End Property
Public Property Get descr() As String
    descr = midescr
End Property
Public Property Let Desc(ByVal i As String)
    miDesc = i
End Property
Public Property Get Desc() As String
    Desc = miDesc
End Property

Public Property Let Atributo(ByVal i As Integer)
    miAtrib = i
End Property
Public Property Get Atributo() As Integer
    Atributo = miAtrib
End Property

Public Property Let Valor_num(ByVal i As Variant)
    mvValor_num = i
End Property
Public Property Get Valor_num() As Variant
    Valor_num = mvValor_num
End Property

Public Property Let Valor_text(ByVal i As Variant)
    mvValor_text = i
End Property
Public Property Get Valor_text() As Variant
    Valor_text = mvValor_text
End Property

Public Property Let Valor_fec(ByVal i As Variant)
    mvValor_fec = i
End Property
Public Property Get Valor_fec() As Variant
    Valor_fec = mvValor_fec
End Property

Public Property Let Valor_bool(ByVal i As Variant)
    mvValor_bool = i
End Property
Public Property Get Valor_bool() As Variant
    Valor_bool = mvValor_bool
End Property

Public Property Let tipo(ByVal i As Variant)
    mvtipo = i
End Property
Public Property Get tipo() As Variant
    tipo = mvtipo
End Property

Public Property Let Orden(ByVal i As Integer)
    miOrden = i
End Property
Public Property Get Orden() As Integer
    Orden = miOrden
End Property

Public Property Let Pedido(ByVal i As Integer)
    miPedido = i
End Property
Public Property Get Pedido() As Integer
    Pedido = miPedido
End Property

Public Property Let Interno(ByVal i As Integer)
    miInterno = i
End Property
Public Property Get Interno() As Integer
    Interno = miInterno
End Property

Public Property Let Indice(ByVal vIndice As Variant)
    mvIndice = vIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvIndice
End Property
Public Property Set Item(ByVal o As CItem)
    Set moItem = o
End Property
Public Property Get Item() As CItem
    Set Item = moItem
End Property
Public Property Set Proceso(ByVal oProce As CProceso)
    Set moProceso = oProce
End Property
Public Property Get Proceso() As CProceso
    Set Proceso = moProceso
End Property
Public Property Set Grupo(ByVal oGrupo As CGrupo)
    Set moGrupo = oGrupo
End Property
Public Property Get Grupo() As CGrupo
    Set Grupo = moGrupo
End Property

Public Property Set Cia(ByVal oCia As CCompania)
    Set moCia = oCia
End Property
Public Property Get Cia() As CCompania
    Set Cia = moCia
End Property

Friend Property Set Conexion(oCon As CConexion)
Set moConexion = oCon
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property

Private Sub Class_Terminate()
    Set moConexion = Nothing
End Sub






