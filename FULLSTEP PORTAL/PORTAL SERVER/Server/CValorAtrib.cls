VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CValorAtrib"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' Variables privadas con la informacion de un valor

Private mvValor As Variant
Private mvIndice As Long


Public Property Get Indice() As Long

    ''' * Objetivo: Devolver el indice del pais en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice del pais en la coleccion

    Indice = mvIndice
    
End Property
Public Property Let Indice(ByVal lInd As Long)

    ''' * Objetivo: Dar valor al indice del pais en la coleccion
    ''' * Recibe: Indice del pais en la coleccion
    ''' * Devuelve: Nada

    mvIndice = lInd
    
End Property

Public Property Let Valor(ByVal vData As Variant)

    mvValor = vData
    
End Property
Public Property Get Valor() As Variant

    Valor = mvValor
    
End Property

