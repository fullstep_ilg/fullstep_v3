VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CEscalado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CEscalado **********************************
'*             Autor : DPD
'*             Creada : 26/07/2011
'***************************************************************

Option Explicit

Private lId As Long
Private vInicial As Variant ' Dependiendo del modo contiene la cantidad inicial del rango o la cantidad directa
Private vFinal As Variant  ' Cantidad final del rango (s�lo en modo rango)
Private vPrecio As Variant ' Precio ( s�lo para �tems, no se define en en grupos) (bruto)
Private vPrecioValido As Variant ' Precio V�lido (neto)
'Variables para ofertas
Private vPrecioOferta As Variant  'Neto
Private vPrecioGS As Variant   'Bruto
Private vObjetivo As Variant
'Variables para ofertas
Private vValorNum As Variant ' S�lo para costes/descuentos

Private lIndex As Long ' �ndice para saber la posici�n que ocupa un escalado en un grupo y poder relacionarlo con otros grupos


Public Property Get id() As Long
    id = lId
End Property

Public Property Let id(ByVal lvar As Long)
    lId = lvar
End Property

Public Property Get Inicial() As Variant
    Inicial = vInicial
End Property

Public Property Let Inicial(ByVal vVar As Variant)
    vInicial = vVar
End Property

Public Property Get Final() As Variant
    Final = vFinal
End Property

Public Property Let Final(ByVal vVar As Variant)
    vFinal = vVar
End Property

Public Property Get Precio() As Variant
    Precio = vPrecio
End Property

Public Property Let Precio(ByVal vVar As Variant)
    vPrecio = vVar
End Property

Public Property Get PrecioValido() As Variant
    PrecioValido = vPrecioValido
End Property

Public Property Let PrecioValido(ByVal vVar As Variant)
    vPrecioValido = vVar
End Property


Public Property Get PrecioGS() As Variant
    PrecioGS = vPrecioGS
End Property

Public Property Let PrecioGS(ByVal vVar As Variant)
    vPrecioGS = vVar
End Property


Public Property Get PrecioOferta() As Variant
    PrecioOferta = vPrecioOferta
End Property

Public Property Let PrecioOferta(ByVal vVar As Variant)
    vPrecioOferta = vVar
End Property



Public Property Let ValorNum(ByVal vData As Variant)
    vValorNum = vData
End Property

Public Property Get ValorNum() As Variant
    ValorNum = vValorNum
End Property

Public Property Get Index() As Long
    Index = lIndex
End Property

Public Property Let Index(ByVal lParamIndex As Long)
    lIndex = lParamIndex
End Property

Public Property Get Objetivo() As Variant
    Objetivo = vObjetivo
End Property

Public Property Let Objetivo(ByVal vVar As Variant)
    vObjetivo = vVar
End Property

