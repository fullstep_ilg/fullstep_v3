VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPrecioItems"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CPrecioItems **********************************
'*             Autor : Javier Arana
'*             Creada : 30/3/99
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CPrecioItem
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
    Count = mCol.Count
End If


End Property

''' <summary>
''' A�adir un objeto de la clase CPrecioItem. CPrecioItem es una property de CItem.
''' </summary>
''' <param name="oItem">Item al q corresponde el objeto</param>
''' <param name="lID">Id del item</param>
''' <param name="vCant">Cantidad del item</param>
''' <param name="vPrecioOferta">Precio1 del item</param>
''' <param name="vPrecio2">Precio2 del item</param>
''' <param name="vPrecio3">Precio3 del item</param>
''' <param name="vIndice">Indice para el objeto</param>
''' <param name="vComent1">Comentario para Precio1 del item</param>
''' <param name="vComent2">Comentario para Precio2 del item</param>
''' <param name="vComent3">Comentario para Precio2 del item</param>
''' <param name="vPrecioGS">PrecioGs del item. La moneda de la oferta puede ser distinta de la del proceso</param>
''' <param name="bPrecioOfertaValido">Indice si el precio1 cumple con lo de bajada minima en la subasta</param>
''' <returns>Objeto de la clase CPrecioItem</returns>
''' <remarks>Llamada desde: solicitudesoferta/guardaroferta.asp; Tiempo m�ximo:0 </remarks>
Public Function Add(ByVal oItem As CItem, ByVal lID As Long, Optional ByVal vCant As Variant, _
                    Optional ByVal vPrecioOferta As Variant, Optional ByVal vPrecio2 As Variant, _
                    Optional ByVal vPrecio3 As Variant, Optional ByVal vIndice As Variant, _
                    Optional ByVal vComent1 As Variant, Optional ByVal vComent2 As Variant, _
                    Optional ByVal vComent3 As Variant, Optional ByVal vPrecioGS As Variant, _
                    Optional ByVal bPrecioOfertaValido As Boolean = True) As CPrecioItem
                    
    'create a new object
    Dim sCod As String
    Dim objnewmember As CPrecioItem
    Set objnewmember = New CPrecioItem
   
    Set objnewmember.Item = oItem
    objnewmember.ID = lID
    objnewmember.Cantidad = vCant
    Set objnewmember.Conexion = mvarConexion
        
    If IsMissing(vCant) Then
        objnewmember.Cantidad = Null
    Else
        objnewmember.Cantidad = vCant
    End If
    If IsMissing(vPrecioGS) Then
        objnewmember.PrecioGS = Null
    Else
        objnewmember.PrecioGS = vPrecioGS
    End If
    
    If IsMissing(vPrecioOferta) Then
        objnewmember.PrecioOferta = Null
    Else
        objnewmember.PrecioOferta = vPrecioOferta
    End If
    
    If IsMissing(bPrecioOfertaValido) Then
        objnewmember.PrecioOfertaValido = True
    Else
        objnewmember.PrecioOfertaValido = bPrecioOfertaValido
    End If
    
    
    If IsMissing(vPrecio2) Then
        objnewmember.Precio2 = Null
    Else
        objnewmember.Precio2 = vPrecio2
    End If
    
    If IsMissing(vPrecio3) Then
        objnewmember.Precio3 = Null
    Else
        objnewmember.Precio3 = vPrecio3
    End If
    
    If IsMissing(vComent1) Then
        objnewmember.Comentario1 = Null
    Else
        objnewmember.Comentario1 = vComent1
    End If
    
    If IsMissing(vComent2) Then
        objnewmember.Comentario2 = Null
    Else
        objnewmember.Comentario2 = vComent2
    End If
    
    If IsMissing(vComent3) Then
        objnewmember.Comentario3 = Null
    Else
        objnewmember.Comentario3 = vComent3
    End If
    
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
       mCol.Add objnewmember, CStr(lID)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Sub Remove(vntIndexKey As Variant)
    
        mCol.Remove vntIndexKey
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
  

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'Itemroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

Public Function BorrarEnModoIndice(ByVal iIndice As Integer)
Dim IndFor As Integer
Dim oItem As CItem

    For IndFor = iIndice To mCol.Count - 2
            
        mCol.Remove (CStr(IndFor))
        Set oItem = mCol.Item(CStr(IndFor + 1))
        mCol.Add oItem, CStr(IndFor)
        Set oItem = Nothing
        
    Next IndFor
    
    mCol.Remove CStr(IndFor)
    
End Function



