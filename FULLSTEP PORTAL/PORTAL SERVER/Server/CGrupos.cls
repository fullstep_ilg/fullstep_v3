VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGrupos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_oConexion As CConexion
Private m_bEOF As Boolean
Private mCol As Collection

''' <summary>
''' A�ade un elemento a la colecci�n de grupos
''' </summary>
''' <param name="oProce">proceso</param>
''' <param name="sCod">c�digo</param>
''' <param name="vIndice">�ndice</param>
''' <param name="sDen">denominaci�n</param>
''' <param name="vDescripcion">descripci�n</param>
''' <param name="bDefDestino">destino</param>
''' <param name="vDestCod">c�digo destino</param>
''' <param name="bDefFechasSum">fechas suministro</param>
''' <param name="vFechaInicioSuministro">fecha inicio suministro</param>
''' <param name="vFechaFinSuministro">fecha fin suministro</param>
''' <param name="bDefFormaPago">forma pago</param>
''' <param name="vPagCod">c�digo pago</param>
''' <param name="bDefEspecificaciones">especificaciones</param>
''' <param name="bDefEspecificacionesItem">especificaciones �tem</param>
''' <param name="bHayAtributos">si hay o no atributos</param>
''' <param name="vEsp">especif</param>
''' <param name="oEspecificaciones">colecci�n de especificaciones</param>
''' <param name="oItems">colecci�n de �tems</param>
''' <param name="oAtributos">colecci�n de atributos</param>
''' <param name="oCon">conexi�n</param>
''' <param name="vNumItems">n�mero items</param>
''' <param name="vSobre">sobre</param>
''' <param name="lId">id</param>
''' <param name="bHayCostesDesc">si hay o no c/d</param>
''' <revisado>JVS 17/06/2011</revisado>

Public Function Add(ByVal oProce As CProceso, ByVal sCod As String, Optional ByVal vIndice As Variant, Optional ByVal sDen As String, Optional ByVal vDescripcion As Variant, _
Optional ByVal bDefDestino As Boolean, Optional ByVal vDestCod As Variant, Optional ByVal bDefFechasSum As Boolean, _
Optional ByVal vFechaInicioSuministro As Variant, Optional ByVal vFechaFinSuministro As Variant, Optional ByVal bDefFormaPago As Boolean, _
Optional ByVal vPagCod As Variant, Optional ByVal bDefEspecificaciones As Boolean, Optional ByVal bDefEspecificacionesItem As Boolean, Optional ByVal bHayAtributos As Boolean, Optional ByVal vEsp As Variant, _
Optional ByVal oEspecificaciones As CEspecificaciones, _
Optional ByVal oItems As CItems, Optional ByVal oAtributos As CAtributos, Optional ByVal oCon As CConexion, Optional ByVal vNumItems As Variant, Optional ByVal vSobre As Variant, Optional ByVal lId As Long, Optional ByVal bHayCostesDesc As Boolean, Optional bHayEscalados As Boolean) As CGrupo
    
    'create a new object
'    Dim sCod As String
    Dim objnewmember As CGrupo
    Set objnewmember = New CGrupo
   
    Set objnewmember.Proceso = oProce
    objnewmember.id = lId
    objnewmember.Codigo = sCod
    objnewmember.Den = sDen
    
    If IsMissing(vDescripcion) Then
        objnewmember.Descripcion = Null
    Else
        objnewmember.Descripcion = vDescripcion
    End If
    
    objnewmember.DefDestino = bDefDestino
    
    If IsMissing(vDestCod) Then
        objnewmember.DestCod = Null
    Else
        objnewmember.DestCod = vDestCod
    End If
    
    objnewmember.DefFechasSum = bDefFechasSum
    
    If IsMissing(vFechaInicioSuministro) Then
        objnewmember.FechaInicioSuministro = Null
    Else
        objnewmember.FechaInicioSuministro = vFechaInicioSuministro
    End If
    
    If IsMissing(vFechaFinSuministro) Then
        objnewmember.FechaFinSuministro = Null
    Else
        objnewmember.FechaFinSuministro = vFechaFinSuministro
    End If
    
    objnewmember.DefFormaPago = bDefFormaPago
    
    If IsMissing(vPagCod) Then
        objnewmember.PagCod = Null
    Else
        objnewmember.PagCod = vPagCod
    End If
    
    
    objnewmember.DefEspecificaciones = bDefEspecificaciones
    objnewmember.DefEspecificacionesItem = bDefEspecificacionesItem
    
    objnewmember.HayAtributos = bHayAtributos
    objnewmember.HayCostesDescuentos = bHayCostesDesc
    
    If IsMissing(vEsp) Then
        objnewmember.Esp = Null
    Else
        objnewmember.Esp = vEsp
    End If
    
    If Not IsMissing(vNumItems) Then
        objnewmember.NumItems = vNumItems
    End If
    
    objnewmember.Sobre = vSobre
    Set objnewmember.Especificaciones = oEspecificaciones
    
    Set objnewmember.Items = oItems
    Set objnewmember.Atributos = oAtributos
    
    
    
    Set objnewmember.Conexion = oCon
    
       
       
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        mCol.Add objnewmember, sCod
    End If
    
    objnewmember.HayEscalados = bHayEscalados
    
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Function AddGrupo(oGrupo As CGrupo)
    mCol.Add oGrupo, oGrupo.Codigo
End Function
Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CGrupo
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo Error

    mCol.Remove vntIndexKey

Error:

End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub




