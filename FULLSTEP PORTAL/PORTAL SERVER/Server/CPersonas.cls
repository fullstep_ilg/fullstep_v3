VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPersonas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mCol As Collection
Private moConexion As CConexion
Friend Property Set Conexion(ByVal oCon As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set moConexion = oCon
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = moConexion
    
End Property
Public Function DevolverDatosPersona(ByVal lCiacomp As Long, ByVal Cod As String) As ADODB.Recordset
Dim ador As ADODB.Recordset
Dim adoCom As ADODB.Command
Dim oParam As ADODB.Parameter
Dim sFSGS As String
    Set adoCom = New ADODB.Command
    Set adoCom.ActiveConnection = moConexion.AdoCon
    
    sFSGS = FSGS(moConexion, lCiacomp)
    adoCom.CommandText = sFSGS & "FSEP_DEVOLVER_DATOS_PERSONA"
    adoCom.CommandType = adCmdStoredProc
    Set oParam = adoCom.CreateParameter("PER", adVarChar, adParamInput, 50, Cod)
    adoCom.Parameters.Append oParam

    adoCom.Prepared = True

    Set ador = adoCom.Execute

    If ador.eof Then
        ador.Close
        Set ador = Nothing
        Set DevolverDatosPersona = Nothing
        Exit Function
    Else
        'Devuelve un recordset desconectado
        ador.ActiveConnection = Nothing
        Set DevolverDatosPersona = ador
        Set ador = Nothing
    End If


End Function

Public Function Add(ByVal sCod As String, ByVal sNom As Variant, ByVal sApe As Variant, _
                    ByVal sCargo As Variant, ByVal sDep As Variant, ByVal sEmail As Variant, _
                    ByVal sTfno As Variant, Optional ByVal sTfno2 As Variant, Optional ByVal sFax As Variant, _
                    Optional ByVal sUON1 As Variant, Optional ByVal sUON2 As Variant, Optional ByVal sUON3 As Variant, _
                    Optional ByVal sUON1DEN As Variant, Optional ByVal sUON2DEN As Variant, Optional ByVal sUON3DEN As Variant, _
                    Optional ByVal sEqp As Variant, Optional ByVal sDepDen As Variant) As CPersona

    'create a new object
    Dim objnewmember As CPersona
    
    
    Set objnewmember = New CPersona
   
    With objnewmember
        .Cod = sCod
        .Nombre = sNom
        .Apellidos = sApe
        .Cargo = sCargo
        .Dep = sDep
        .Email = sEmail
        .Fax = sFax
        .Tfno = sTfno
        .Tfno2 = sTfno2
        .UON1 = sUON1
        .UON2 = sUON2
        .UON3 = sUON3
        .UON1DEN = sUON1DEN
        .UON2DEN = sUON2DEN
        .UON3DEN = sUON3DEN
        .Eqp = sEqp
        .DepDen = sDepDen
    End With
    
    mCol.Add objnewmember, sCod
      
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function


Public Property Get Item(vntIndexKey As Variant) As CPersona
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property



Public Sub Remove(vntIndexKey As Variant)
    
        mCol.Remove vntIndexKey
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()

    Set moConexion = Nothing

End Sub


Public Sub CargarTodasLasPersonas(ByVal lCiacomp As Long, Optional ByVal Cod As Variant)
Dim sconsulta As String
Dim sFSG As String
Dim adoComm As ADODB.Command
Dim adoRS As ADODB.Recordset
Dim adoParam As ADODB.Parameter

sFSG = FSGS(moConexion, lCiacomp)

Set adoComm = New ADODB.Command
Set adoComm.ActiveConnection = moConexion.AdoCon
adoComm.CommandType = adCmdText
sconsulta = "EXEC " & sFSG & "FSEP_DEVOLVER_DATOS_PERSONA"
If Not IsMissing(Cod) Then
    Set adoParam = adoComm.CreateParameter("PER", adVarChar, adParamInput, 50, Cod)
    adoComm.Parameters.Append adoParam
End If

adoComm.CommandText = sconsulta

Set adoRS = adoComm.Execute
        
If adoRS.eof Then
        
    adoRS.Close
    Set adoRS = Nothing
    Set mCol = New Collection
    Exit Sub
      
Else
    Set mCol = Nothing
    Set mCol = New Collection
    
    While Not adoRS.eof
        Me.Add adoRS.Fields("COD").Value, adoRS.Fields("NOM").Value, adoRS.Fields("APE").Value, _
               NullToStr(adoRS.Fields("CAR").Value), adoRS.Fields("DEP").Value, NullToStr(adoRS.Fields("EMAIL").Value), _
               NullToStr(adoRS.Fields("TFNO").Value), adoRS.Fields("TFNO2").Value, adoRS.Fields("FAX").Value, _
               adoRS.Fields("UON1").Value, adoRS.Fields("UON2").Value, adoRS.Fields("UON3").Value, _
               adoRS.Fields("DENU1").Value, adoRS.Fields("DENU2").Value, adoRS.Fields("DENU3").Value, _
               adoRS.Fields("EQP").Value, adoRS.Fields("DENDEP").Value
        adoRS.MoveNext
    Wend
    adoRS.Close
    
    Set adoRS = Nothing
     
End If

End Sub
