VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCompanias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean


Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Public Function Add(ByVal ID As Long, ByVal Cod As String, ByVal Den As String, Optional ByVal Direccion As Variant, Optional ByVal CP As Variant, _
Optional ByVal CodPais As Variant, Optional ByVal DenPais As Variant, Optional ByVal Poblacion As Variant, _
Optional ByVal CodMon As Variant, Optional ByVal DenMon As Variant, Optional ByVal CodProvi As Variant, _
Optional ByVal DenProvi As Variant, Optional ByVal NumProceNuevos As Variant, Optional ByVal NumProceActivos As Variant, Optional ByVal NumProceARevisar As Variant, Optional ByVal varIndice As Variant, Optional ByVal IDPortal As Variant, _
Optional ByVal Estado As Integer, Optional ByVal NumOrdenActivas As Variant, Optional ByVal NumOrdenNuevas As Variant, _
Optional ByVal NumCertActivas As Variant, Optional ByVal NumCertNuevas As Variant, _
Optional ByVal NumNoConfActivas As Variant, Optional ByVal NumNoConfNuevas As Variant) As CCompania
    'create a new object

Dim objnewmember As CCompania
    
    
    Set objnewmember = New CCompania
    
    With objnewmember
        
        .ID = ID
        .Cod = Cod
        .Den = Den
        
        Set .Conexion = mvarConexion
        
        If Not IsMissing(Poblacion) Then
            .Poblacion = Poblacion
        Else
            .Poblacion = Null
        End If
        
        If Not IsMissing(Direccion) Then
            .Direccion = Direccion
        Else
            .Direccion = Null
        End If
        
        
        If Not IsMissing(CP) Then
            .CP = CP
        Else
            .CP = Null
        End If
        
        
        If Not IsMissing(CodMon) Then
            .CodMon = CodMon
        Else
            .CodMon = Null
        End If
        
        If Not IsMissing(DenMon) Then
            .DenMon = DenMon
        Else
            .DenMon = Null
        End If
        
        
        If Not IsMissing(CodProvi) Then
            .CodProvi = CodProvi
        Else
            .CodProvi = Null
        End If
        
        If Not IsMissing(DenProvi) Then
            .DenProvi = DenProvi
        Else
            .DenProvi = Null
        End If
        
        
        If Not IsMissing(CodPais) Then
            .CodPais = CodPais
        Else
            .CodPais = Null
        End If
            
        If Not IsMissing(DenPais) Then
            .DenPais = DenPais
        Else
            .DenPais = Null
        End If
        
       
        If Not IsMissing(IDPortal) Then
            .IDPortal = IDPortal
        Else
            .IDPortal = Null
        End If
        
        '.Estado = Estado
       
        If Not IsMissing(NumProceActivos) Then
            .NumProceActivos = NumProceActivos
        Else
            .NumProceActivos = Null
        End If
        
        If Not IsMissing(NumProceNuevos) Then
            .NumProceNuevos = NumProceNuevos
        Else
            .NumProceNuevos = Null
        End If
            
        If Not IsMissing(NumProceARevisar) Then
            .NumProceARevisar = NumProceARevisar
        Else
            .NumProceARevisar = Null
        End If
        
        If Not IsMissing(NumOrdenActivas) Then
            .NumOrdenActivas = NumOrdenActivas
        Else
            .NumOrdenActivas = Null
        End If
        
        If Not IsMissing(NumOrdenNuevas) Then
            .NumOrdenNuevas = NumOrdenNuevas
        Else
            .NumOrdenNuevas = Null
        End If
        
        If Not IsMissing(NumCertActivas) Then
            .NumCertActivas = NumCertActivas
        Else
            .NumCertActivas = Null
        End If
        
        If Not IsMissing(NumCertNuevas) Then
            .NumCertNuevas = NumCertNuevas
        Else
            .NumCertNuevas = Null
        End If
             
#If VERSION >= 30600 Then
        If Not IsMissing(NumNoConfActivas) Then
            .NumNoConfActivas = NumNoConfActivas
        Else
            .NumNoConfActivas = Null
        End If
        
        If Not IsMissing(NumNoConfNuevas) Then
            .NumNoConfNuevas = NumNoConfNuevas
        Else
            .NumNoConfNuevas = Null
        End If
#End If
        
    End With
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        'set the properties passed into the method
        mCol.Add objnewmember, CStr(Cod)
    End If

    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Property Get Item(vntIndexKey As Variant) As CCompania
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
On Error GoTo NoSeEncuentra:
    mCol.Remove vntIndexKey
    Exit Sub
NoSeEncuentra:
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
  

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

