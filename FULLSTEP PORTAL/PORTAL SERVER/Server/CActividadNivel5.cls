VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CActividadNivel5"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit
Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarIndice As Long 'Nos indica la posicion secuencial dentro de una Coleccion
Private mvarCod As String
Private mvarDen As String
Private mvarId As Integer
Private mvarAsignada As Boolean

Private mvarACN1 As Integer
Private mvarACN2 As Integer
Private mvarACN3 As Integer
Private mvarACN4 As Integer
Private mvarConexion As CConexion 'local copy
Private mvarPendiente As Variant


Public Property Get Pendiente() As Boolean
    Pendiente = mvarPendiente
End Property
Public Property Let Pendiente(ByVal Ind As Boolean)
    mvarPendiente = Ind
End Property


Public Property Get ACN1() As Integer
    ACN1 = mvarACN1
End Property
Public Property Let ACN1(ByVal Ind As Integer)
    mvarACN1 = Ind
End Property
Public Property Get ACN2() As Integer
    ACN2 = mvarACN2
End Property
Public Property Let ACN2(ByVal Ind As Integer)
    mvarACN2 = Ind
End Property
Public Property Get ACN3() As Integer
    ACN3 = mvarACN3
End Property
Public Property Let ACN3(ByVal Ind As Integer)
    mvarACN3 = Ind
End Property
Public Property Get ACN4() As Integer
    ACN4 = mvarACN4
End Property
Public Property Let ACN4(ByVal Ind As Integer)
    mvarACN4 = Ind
End Property
Public Property Get ID() As Integer
    ID = mvarId
End Property
Public Property Let ID(ByVal Ind As Integer)
    mvarId = Ind
End Property
Public Property Get Indice() As Long
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal Ind As Long)
    mvarIndice = Ind
End Property

Public Property Get Asignada() As Boolean
Asignada = mvarAsignada
End Property
Public Property Let Asignada(ByVal bAsignada As Boolean)
mvarAsignada = bAsignada
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Public Property Let Den(ByVal vData As String)
    mvarDen = vData
End Property


Public Property Get Den() As String
    Den = mvarDen
End Property



Public Property Let Cod(ByVal vData As String)
    mvarCod = vData
End Property


Public Property Get Cod() As String
    Cod = mvarCod
End Property


Private Sub Class_Terminate()
    
    Set mvarConexion = Nothing
 
    
End Sub




