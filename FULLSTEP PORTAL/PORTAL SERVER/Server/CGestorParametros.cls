VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGestorParametros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CGestorParametros **********************************
'*             Autor : Hilario Barrenkua
'*             Creada : 05/01/2001
'* **************************************************************
Option Explicit

Private mvarConexion As CConexion 'local copy
Private mvarPremium As Variant
Private mvarMaxAdjun As Variant
Private mvarGrupo As Variant
Private mvarDominio As Variant
Private m_iNivelMinAct As Integer
Private mvarMaxAdjunCia As Variant

Private mvarNomPor As Variant
Private mvarUrl As Variant
Private mvarAdm As Variant
Private mvarTipoEmail As Variant
Private mvarEmail As String
Private mvarNotifSolicComp As Variant
Private mvarNotifSolicAct As Variant
Private mvarNotifSolicReg As Variant
Private mlPais As Long
Private mbUnaCompradora As Boolean
Private mlIdCiaCompradora As Variant
Private msFSBDCiaCompradora As Variant
Private msFSSRVCiaCompradora As Variant
Private mbAccesoFSSM As Boolean
Private msCadAccesoFSSM As String
' Control de acceso con licencia GS
Private mbAccesoFSGS As Boolean
Private msCadAccesoFSGS As String
' Control de acceso a facturas
Private mbAccesoFSFA As Boolean
Private msCadAccesoFSFA As String

Private mlMonCen As Long
Private msIdiCod  As String

Private mbCompruebaNif As Boolean
Private mbAutorizacionAutomatica As Boolean
Private mbActivarRegistroActiv As Boolean
'A�ADIDO PARA POLITICA DE ACEPTACION DE PEDIDO
Private mbImprimirPoliticaPedidos As Boolean
Private mbComprobarCodCia As Boolean
'A�adido para no mostrar resultado subasta
Private mbNoMostrarPopUp As Boolean
Private mbBloqModifProve As Boolean
Private mbVerNumeroProveedor As Boolean
Private mbDatosUsu As Boolean
Private mbDatosCia As Boolean
Private mbActivCia As Boolean
Private mbAdjunCia As Boolean
Private mbAdminUsu As Boolean
Private mbCompruebaTfno As Boolean
Private miCaducidadSesion As Integer
Private mbAccesoExterno As Boolean
Private mbEmpresaPortal As Boolean

Private iVigenciaMaximaPassword As Integer
Private iNumIntentosBloqueo As Integer
Private b_EncryptOneWay As Boolean

Private sUsuImpersonate As String
Private sPwdImpersonate As String
Private sDominioImpersonate As String
Private sURLPasswordChangeService As String

Private iMinSizePWD As Integer
Private iHistoricoPWD As Integer
Private iPubPortalPedAbiertos As Integer
Private m_iMaxActividades As Integer


Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property
Public Property Let Dominio(ByVal vData As Variant)
    mvarDominio = vData
End Property
Public Property Get Dominio() As Variant
    Dominio = mvarDominio
End Property

Public Property Let Grupo(ByVal vData As Variant)
    mvarGrupo = vData
End Property


Public Property Get Grupo() As Variant
    Grupo = mvarGrupo
End Property
Public Property Let Premium(ByVal vData As Variant)
    mvarPremium = vData
End Property


Public Property Get Premium() As Variant
    Premium = mvarPremium
End Property


Public Property Let MaxAdjun(ByVal vData As Variant)
    mvarMaxAdjun = vData
End Property


Public Property Get MaxAdjun() As Variant
    MaxAdjun = mvarMaxAdjun
End Property

Public Property Let NivelMinAct(ByVal vData As Integer)
    m_iNivelMinAct = vData
End Property

Public Property Get NivelMinAct() As Integer
    NivelMinAct = m_iNivelMinAct
End Property

Public Property Let MaxActividades(ByVal vData As Integer)
    m_iMaxActividades = vData
End Property

Public Property Get MaxActividades() As Integer
    MaxActividades = m_iMaxActividades
End Property

Public Property Let MaxAdjunCia(ByVal vData As Variant)
    mvarMaxAdjunCia = vData
End Property


Public Property Get MaxAdjunCia() As Variant
    MaxAdjunCia = mvarMaxAdjunCia
End Property

Public Property Let NomPor(ByVal vData As Variant)
    mvarNomPor = vData
End Property


Public Property Get NomPor() As Variant
    NomPor = mvarNomPor
End Property

Public Property Let Url(ByVal vData As Variant)
    mvarUrl = vData
End Property


Public Property Get Url() As Variant
    Url = mvarUrl
End Property

Public Property Let Adm(ByVal vData As Variant)
    mvarAdm = vData
End Property


Public Property Get Adm() As Variant
    Adm = mvarAdm
End Property

Public Property Let TipoEmail(ByVal vData As Variant)
    mvarTipoEmail = vData
End Property


Public Property Get TipoEmail() As Variant
    TipoEmail = mvarTipoEmail
End Property

Public Property Let Email(ByVal vData As Variant)
    mvarEmail = vData
End Property


Public Property Get Email() As Variant
    Email = mvarEmail
End Property

Public Property Let NotifSolicComp(ByVal vData As Variant)
    mvarNotifSolicComp = vData
End Property


Public Property Get NotifSolicComp() As Variant
    NotifSolicComp = mvarNotifSolicComp
End Property

Public Property Let NotifSolicAct(ByVal vData As Variant)
    mvarNotifSolicAct = vData
End Property


Public Property Get NotifSolicAct() As Variant
    NotifSolicAct = mvarNotifSolicAct
End Property

Public Property Let NotifSolicReg(ByVal vData As Variant)
   mvarNotifSolicReg = vData
End Property


Public Property Get NotifSolicReg() As Variant
    NotifSolicReg = mvarNotifSolicReg
End Property


Public Property Let Pais(ByVal lData As Long)
    mlPais = lData
End Property

Public Property Get Pais() As Long
    Pais = mlPais
End Property

Public Property Let UnaCompradora(ByVal bData As Boolean)
    mbUnaCompradora = bData
End Property

Public Property Get UnaCompradora() As Boolean
    UnaCompradora = mbUnaCompradora
End Property

Public Property Let IdCiaCompradora(ByVal vData As Variant)
    mlIdCiaCompradora = vData
End Property

Public Property Get IdCiaCompradora() As Variant
    IdCiaCompradora = mlIdCiaCompradora
End Property

Public Property Let FSBDCiaCompradora(ByVal vData As Variant)
    msFSBDCiaCompradora = vData
End Property

Public Property Get FSBDCiaCompradora() As Variant
    FSBDCiaCompradora = msFSBDCiaCompradora
End Property

Public Property Let FSSRVCiaCompradora(ByVal vData As Variant)
    msFSSRVCiaCompradora = vData
End Property

Public Property Get FSSRVCiaCompradora() As Variant
    FSSRVCiaCompradora = msFSSRVCiaCompradora
End Property


Public Property Get MonCen() As Long
    MonCen = mlMonCen
End Property

Public Property Let MonCen(ByVal lData As Long)
    mlMonCen = lData
End Property

Public Property Get IdiCod() As String
    IdiCod = msIdiCod
End Property

Public Property Let IdiCod(ByVal sData As String)
    msIdiCod = sData
End Property

Public Property Let CompruebaNIF(ByVal vData As Boolean)
    mbCompruebaNif = vData
End Property

Public Property Get CompruebaNIF() As Boolean
    CompruebaNIF = mbCompruebaNif
End Property
Public Property Let AutorizacionAutomatica(ByVal vData As Boolean)
    mbAutorizacionAutomatica = vData
End Property

Public Property Get AutorizacionAutomatica() As Boolean
    AutorizacionAutomatica = mbAutorizacionAutomatica
End Property


Public Property Let ActivarRegistroActiv(ByVal vData As Boolean)
    mbActivarRegistroActiv = vData
End Property

Public Property Get ActivarRegistroActiv() As Boolean
    ActivarRegistroActiv = mbActivarRegistroActiv
End Property

Public Property Let ImprimirPoliticaPedidos(ByVal vData As Boolean)
    mbImprimirPoliticaPedidos = vData
End Property

Public Property Get ImprimirPoliticaPedidos() As Boolean
    ImprimirPoliticaPedidos = mbImprimirPoliticaPedidos
End Property

Public Property Let ComprobarCodCia(ByVal vData As Boolean)
    mbComprobarCodCia = vData
End Property

Public Property Get ComprobarCodCia() As Boolean
    ComprobarCodCia = mbComprobarCodCia
End Property

Public Property Let NoMostrarPopUp(ByVal vData As Boolean)
    mbNoMostrarPopUp = vData
End Property

Public Property Get NoMostrarPopUp() As Boolean
    NoMostrarPopUp = mbNoMostrarPopUp
End Property

Public Property Let CadAccesoFSSM(ByVal vData As String)
    msCadAccesoFSSM = vData
    mbAccesoFSSM = (EncriptarAcceso("SM", NullToStr(vData), False) = "FULLSTEP SM")
End Property
Public Property Get CadAccesoFSSM() As String
    CadAccesoFSSM = msCadAccesoFSSM
End Property

Public Property Let AccesoFSSM(ByVal vData As Boolean)
    mbAccesoFSSM = vData
End Property
Public Property Get AccesoFSSM() As Boolean
    AccesoFSSM = mbAccesoFSSM
End Property

'Propiedades para validar el Acceso con licencia GS
Public Property Let CadAccesoFSGS(ByVal vData As String)
    msCadAccesoFSGS = vData
    mbAccesoFSGS = (EncriptarAcceso("GS", NullToStr(vData), False) = "FULLSTEP GS")
End Property
Public Property Get CadAccesoFSGS() As String
    CadAccesoFSGS = msCadAccesoFSGS
End Property

Public Property Let AccesoFSGS(ByVal vData As Boolean)
    mbAccesoFSGS = vData
End Property
Public Property Get AccesoFSGS() As Boolean
    AccesoFSGS = mbAccesoFSGS
End Property

'Propiedades para validar el acceso a Facturas
Public Property Let CadAccesoFSFA(ByVal vData As String)
    msCadAccesoFSFA = vData
    mbAccesoFSFA = (EncriptarAcceso("FA", NullToStr(vData), False) = "FULLSTEP FA")
End Property
Public Property Get CadAccesoFSFA() As String
    CadAccesoFSFA = msCadAccesoFSFA
End Property

Public Property Let AccesoFSFA(ByVal vData As Boolean)
    mbAccesoFSFA = vData
End Property
Public Property Get AccesoFSFA() As Boolean
    AccesoFSFA = mbAccesoFSFA
End Property


Public Property Let BloqModifProve(ByVal vData As Boolean)
    mbBloqModifProve = vData
End Property
Public Property Get BloqModifProve() As Boolean
    BloqModifProve = mbBloqModifProve
End Property

Public Property Get VerNumeroProveedor() As Boolean
    VerNumeroProveedor = mbVerNumeroProveedor
End Property
Public Property Let VerNumeroProveedor(ByVal vData As Boolean)
    mbVerNumeroProveedor = vData
End Property

Public Property Get DatosUsu() As Boolean
    DatosUsu = mbDatosUsu
End Property
Public Property Let DatosUsu(ByVal vData As Boolean)
    mbDatosUsu = vData
End Property

Public Property Get DatosCia() As Boolean
    DatosCia = mbDatosCia
End Property
Public Property Let DatosCia(ByVal vData As Boolean)
    mbDatosCia = vData
End Property

Public Property Get ActivCia() As Boolean
    ActivCia = mbActivCia
End Property
Public Property Let ActivCia(ByVal vData As Boolean)
    mbActivCia = vData
End Property

Public Property Get AdjunCia() As Boolean
    AdjunCia = mbAdjunCia
End Property
Public Property Let AdjunCia(ByVal vData As Boolean)
    mbAdjunCia = vData
End Property

Public Property Get AdminUsu() As Boolean
    AdminUsu = mbAdminUsu
End Property
Public Property Let AdminUsu(ByVal vData As Boolean)
    mbAdminUsu = vData
End Property

Public Property Get AccesoExterno() As Boolean
    AccesoExterno = mbAccesoExterno
End Property
Public Property Let AccesoExterno(ByVal vData As Boolean)
    mbAccesoExterno = vData
End Property
Public Property Get EmpresaPortal() As Boolean
    EmpresaPortal = mbEmpresaPortal
End Property
Public Property Let EmpresaPortal(ByVal vData As Boolean)
    mbEmpresaPortal = vData
End Property
Public Property Get CompruebaTfno() As Boolean
    CompruebaTfno = mbCompruebaTfno
End Property
Public Property Let CompruebaTfno(ByVal vData As Boolean)
    mbCompruebaTfno = vData
End Property
''' <summary>
''' Devolver la variable Caducidad Sesion
''' </summary>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Get CaducidadSesion() As Integer
    CaducidadSesion = miCaducidadSesion
End Property
''' <summary>
''' Establecer la variable Caducidad Sesion
''' </summary>
''' <param name="vData">valor de la variable</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
Public Property Let CaducidadSesion(ByVal vData As Integer)
    miCaducidadSesion = vData
End Property

Public Property Get VigenciaMaximaPassword() As Integer
    VigenciaMaximaPassword = iVigenciaMaximaPassword
End Property
Public Property Let VigenciaMaximaPassword(ByVal vData As Integer)
    iVigenciaMaximaPassword = vData
End Property

Public Property Get NumIntentosBloqueo() As Integer
    NumIntentosBloqueo = iNumIntentosBloqueo
End Property
Public Property Let NumIntentosBloqueo(ByVal vData As Integer)
    iNumIntentosBloqueo = vData
End Property

Public Property Let EncryptOneWay(ByVal Data As Boolean)
    b_EncryptOneWay = Data
End Property
Public Property Get EncryptOneWay() As Boolean
    EncryptOneWay = b_EncryptOneWay
End Property

Public Property Let UsuImpersonate(ByVal Data As String)
    sUsuImpersonate = Data
End Property
Public Property Get UsuImpersonate() As String
    UsuImpersonate = sUsuImpersonate
End Property

Public Property Let PwdImpersonate(ByVal Data As String)
    sPwdImpersonate = Data
End Property
Public Property Get PwdImpersonate() As String
    PwdImpersonate = sPwdImpersonate
End Property

Public Property Let DominioImpersonate(ByVal Data As String)
    sDominioImpersonate = Data
End Property
Public Property Get DominioImpersonate() As String
    DominioImpersonate = sDominioImpersonate
End Property

Public Property Let URLPasswordChangeService(ByVal Data As String)
    sURLPasswordChangeService = Data
End Property
Public Property Get URLPasswordChangeService() As String
    URLPasswordChangeService = sURLPasswordChangeService
End Property

Public Property Get MinSizePWD() As Integer
    MinSizePWD = iMinSizePWD
End Property
Public Property Let MinSizePWD(ByVal vData As Integer)
    iMinSizePWD = vData
End Property

Public Property Get HistoricoPWD() As Integer
    HistoricoPWD = iHistoricoPWD
End Property
Public Property Let HistoricoPWD(ByVal vData As Integer)
    iHistoricoPWD = vData
End Property
Public Property Get PubPortalPedAbiertos() As Integer
    PubPortalPedAbiertos = iPubPortalPedAbiertos
End Property
Public Property Let PubPortalPedAbiertos(ByVal vData As Integer)
    iPubPortalPedAbiertos = vData
End Property

''' <summary>Devuelve el valor de un campo de PARGEN_INTERNO de GS</summary>
''' <param name="lCiaComp">CIA</param>
''' <param name="sParamName">Nombre del campo</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>

Public Function DevolverParametroInternoGS(ByVal lCiaComp As Long, ByVal sParamName As String) As Variant
    Dim sFSG As String
    Dim sConsulta  As String
    Dim oComm As ADODB.Command
    Dim rsParam As ADODB.Recordset
    
    On Error GoTo Error
    
    sFSG = Fsgs(mvarConexion, lCiaComp)
    
    sConsulta = "SELECT " & sParamName & " FROM " & sFSG & "PARGEN_INTERNO"
    
    Set oComm = New ADODB.Command
    With oComm
        Set .ActiveConnection = mvarConexion.AdoCon
        .CommandType = adCmdText
        .CommandText = sConsulta
        
        Set rsParam = .Execute
    End With
        
    If Not rsParam Is Nothing Then
        If rsParam.RecordCount > 0 Then
            DevolverParametroInternoGS = rsParam(sParamName)
        End If
    End If
        
Salir:
    Set oComm = Nothing
    Exit Function
Error:
    If Err.Number = 1 Then
        Exit Function
    End If
    Resume Salir
End Function

''' <summary>Devuelve el valor de un campo de PARGEN_GEST de GS</summary>
''' <param name="lCiaComp">CIA</param>
''' <param name="sParamName">Nombre del campo</param>
''' <remarks>Llamada desde: </remarks>

Public Function DevolverParametroDefGS(ByVal lCiaComp As Long, ByVal sParamName As String) As Variant
    Dim sFSG As String
    Dim sConsulta  As String
    Dim oComm As ADODB.Command
    Dim rsParam As ADODB.Recordset
    
    On Error GoTo Error
    
    sFSG = Fsgs(mvarConexion, lCiaComp)
    sConsulta = "SELECT " & sParamName & " FROM " & sFSG & "PARGEN_DEF"
    
    Set oComm = New ADODB.Command
    With oComm
        Set .ActiveConnection = mvarConexion.AdoCon
        .CommandType = adCmdText
        .CommandText = sConsulta
        
        Set rsParam = .Execute
    End With
        
    If Not rsParam Is Nothing Then
        If rsParam.RecordCount > 0 Then DevolverParametroDefGS = rsParam(sParamName)
    End If
        
Salir:
    Set oComm = Nothing
    Exit Function
Error:
    If Err.Number = 1 Then
        Exit Function
    End If
    Resume Salir
End Function


''' <summary>Devuelve el valor de un campo de PARGEN_GEST de GS</summary>
''' <param name="lCiaComp">CIA</param>
''' <param name="sParamName">Nombre del campo</param>
''' <remarks>Llamada desde: </remarks>

Public Function DevolverParametroGestGS(ByVal lCiaComp As Long, ByVal sParamName As String) As Variant
    Dim sFSG As String
    Dim sConsulta  As String
    Dim oComm As ADODB.Command
    Dim rsParam As ADODB.Recordset
    
    On Error GoTo Error
    
    sFSG = Fsgs(mvarConexion, lCiaComp)
    sConsulta = "SELECT " & sParamName & " FROM " & sFSG & "PARGEN_GEST"
    
    Set oComm = New ADODB.Command
    With oComm
        Set .ActiveConnection = mvarConexion.AdoCon
        .CommandType = adCmdText
        .CommandText = sConsulta
        
        Set rsParam = .Execute
    End With
        
    If Not rsParam Is Nothing Then
        If rsParam.RecordCount > 0 Then DevolverParametroGestGS = rsParam(sParamName)
    End If
        
Salir:
    Set oComm = Nothing
    Exit Function
Error:
    If Err.Number = 1 Then
        Exit Function
    End If
    Resume Salir
End Function

''' <summary>Devuelve el valor de una ruta de la tabla PARGEN_RUTAS de la BD de GS</summary>
''' <param name="lCiaComp">CIA</param>
''' <param name="sNombreRuta">Nombre de la ruta</param>
''' <remarks>Llamada desde: </remarks>

Public Function DevolverRutaGS(ByVal lCiaComp As Long, ByVal sNombreRuta As String) As String
    Dim sFSG As String
    Dim sConsulta  As String
    Dim oComm As ADODB.Command
    Dim oParam As ADODB.Parameter
    Dim rsParam As ADODB.Recordset
    
    On Error GoTo Error
    
    sFSG = Fsgs(mvarConexion, lCiaComp)
    sConsulta = "SELECT RUTAXBAP FROM " & sFSG & "PARGEN_RUTAS WITH (NOLOCK) WHERE NOMBREPROY=?"
    
    Set oComm = New ADODB.Command
    With oComm
        Set .ActiveConnection = mvarConexion.AdoCon
        Set oParam = .CreateParameter("@NOMBREPROY", adVarChar, adParamInput, Len(sNombreRuta), sNombreRuta)
        .Parameters.Append oParam
        .CommandType = adCmdText
        .CommandText = sConsulta
        
        Set rsParam = .Execute
    End With
        
    If Not rsParam Is Nothing Then
        If rsParam.RecordCount > 0 Then DevolverRutaGS = rsParam("RUTAXBAP")
    End If
        
Salir:
    Set oComm = Nothing
    Set oParam = Nothing
    Exit Function
Error:
    If Err.Number = 1 Then
        Exit Function
    End If
    Resume Salir
End Function


''' <summary>Devuelve el valor de un campo de PARGEN_GEST de GS</summary>
''' <param name="lCiaComp">CIA</param>
''' <param name="iTabla">Tabla</param>
''' <remarks>Llamada desde: </remarks>

Public Function DevolverParametroIntegracion(ByVal lCiaComp As Long, ByVal iTabla As EntidadIntegracion) As ParametroIntegracion
    Dim sFSG As String
    Dim sConsulta  As String
    Dim oCom As ADODB.Command
    Dim oParam As ADODB.Parameter
    Dim rsParam As ADODB.Recordset
    Dim bActiva As Boolean
    Dim bExportar As Boolean
    Dim bSoloImportar
    Dim bSentidoEsNull As Boolean
    Dim adoRes As ADODB.Recordset
    
    On Error GoTo Error
    
    sFSG = Fsgs(mvarConexion, lCiaComp)
    
    bActiva = False
    bExportar = False
    bSoloImportar = True
    bSentidoEsNull = True
    
    Set oCom = New ADODB.Command
    With oCom
        sConsulta = "SELECT TABLA, ACTIVA, SENTIDO FROM " & sFSG & "TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=? ORDER BY ERP"
        
        Set .ActiveConnection = mvarConexion.AdoCon
        .CommandType = adCmdText
        .CommandText = sConsulta
        Set oParam = .CreateParameter(, adInteger, adParamInput, , iTabla)
        .Parameters.Append oParam
        
        Set adoRes = .Execute
    End With
    
    While Not adoRes.eof
        'Activa para alg�n ERP -> ACTIVA
        If adoRes("ACTIVA").Value Then bActiva = True
        
        If Not IsNull(adoRes("SENTIDO").Value) Then
            'Todos los sentidos de entrada -> SoloImportar
            'Alg�n sentido de salida o entrada/salida: Exportar
            bSentidoEsNull = False
            If (adoRes("SENTIDO").Value <> SentidoIntegracion.Entrada) Then
                bSoloImportar = False
                bExportar = True
            End If
        End If
        
        adoRes.MoveNext
    Wend
        
    If bActiva Then
        If bSentidoEsNull Then
            bSoloImportar = False
            bExportar = False
        End If
        DevolverParametroIntegracion.gaSoloImportar = bSoloImportar
        DevolverParametroIntegracion.gaExportar = bExportar
    End If
    
Salir:
    adoRes.Close
    Set adoRes = Nothing
    Set oCom = Nothing
    Set oParam = Nothing
    Exit Function
Error:
    If Err.Number = 1 Then
        Exit Function
    End If
    Resume Salir
End Function
