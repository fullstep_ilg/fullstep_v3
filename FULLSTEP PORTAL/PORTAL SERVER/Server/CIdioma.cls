VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CIdioma"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' Variables privadas con la informacion de un pais

Private mvarId As Integer
Private mvarCod As String
Private mvarDen As String
Public Property Let Den(ByVal vData As String)

    mvarDen = vData
    
End Property
Public Property Get Den() As String

    Den = mvarDen
    
End Property
Public Property Let ID(ByVal Data As Integer)
    
    Let mvarId = Data
    
End Property
Public Property Get ID() As Integer
    
    ID = mvarId
    
End Property
Public Property Let Cod(ByVal vData As String)

    mvarCod = vData
    
End Property
Public Property Get Cod() As String

    Cod = mvarCod
    
End Property
