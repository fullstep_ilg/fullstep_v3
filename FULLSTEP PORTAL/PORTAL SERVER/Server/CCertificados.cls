VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCertificados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mCol As Collection
Private moConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
Set moConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub
Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set moConexion = Nothing
End Sub

Public Property Get Count() As Long

    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If

End Property

''' <summary>
''' Devolver Certificados del proveedor
''' </summary>
''' <param name="CiaComp">C�digo de compania</param>
''' <param name="Prove">Proveedor</param>
''' <param name="Idioma">Idioma</param>
''' <returns>Certificados del proveedor</returns>
''' <remarks>Llamada desde: Certificados11.asp; Tiempo m�ximo: 0,1</remarks>
Public Function DevolverCertificados(ByVal CiaComp As Long, ByVal Prove As String, ByVal Idioma As String) As ADODB.Recordset
Dim ador As ADODB.Recordset
Dim sConsulta As String
Dim sFSG As String

    Dim adoCom As ADODB.Command
    
    Set adoCom = New ADODB.Command
    adoCom.ActiveConnection = moConexion.AdoCon

    sFSG = Fsgs(moConexion, CiaComp)

    Idioma = EsIdiomaValido(Idioma)

    adoCom.CommandType = adCmdStoredProc
    adoCom.CommandText = sFSG & "FSQA_GET_CERTIFICADOS_PROVE_MENU"

    
    Dim adoPar As ADODB.Parameter
    
    Set adoPar = adoCom.CreateParameter("PROVE", adVarChar, adParamInput, 50, Prove)
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("IDI", adVarChar, adParamInput, 20, Idioma)
    adoCom.Parameters.Append adoPar
    Set ador = adoCom.Execute
    
    If ador.eof Then
        ador.Close
        Set ador = Nothing
        Set DevolverCertificados = Nothing
        Exit Function
    Else
        If ador("NUMEROCERTIFICADOS") = 0 Then
            ador.Close
            Set ador = Nothing
            Set DevolverCertificados = Nothing
            Exit Function
        Else
            'Devuelve un recordset desconectado
            ador.ActiveConnection = Nothing
            Set DevolverCertificados = ador
        End If
        
    End If
    
End Function























