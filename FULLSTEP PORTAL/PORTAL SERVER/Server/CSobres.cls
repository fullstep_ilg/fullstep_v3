VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSobres"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mCol As Collection

Public Function Add(ByVal lCod As Long, ByVal iEst As Integer, ByVal dtFechaApertura As Variant, ByVal dtFechaRealApertura As Variant, _
                    ByVal sUsuApertura As Variant, ByVal sObs As Variant, oProce As CProceso, oCon As CConexion) As CSobre
    
    Dim objnewmember As CSobre
    Set objnewmember = New CSobre
   
    With objnewmember
        .Cod = lCod
        .Est = iEst
        .FechaApertura = dtFechaApertura
        .FechaRealApertura = dtFechaRealApertura
        .UsuApertura = sUsuApertura
        .Obs = sObs
        Set .Proceso = oProce
        Set .Conexion = oCon
    End With
       
       
    mCol.Add objnewmember, CStr(lCod)
    
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get Item(vntIndexKey As Variant) As CSobre
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo Error

    mCol.Remove vntIndexKey

Error:

End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set mCol = Nothing
End Sub




