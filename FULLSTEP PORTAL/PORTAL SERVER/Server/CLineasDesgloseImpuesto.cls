VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLineasDesgImpuesto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mCol As Collection
Private m_oConexion As CConexion

Public Function addLinea(oLinea As CLineaDesgImpuesto, Optional sKey As String) As CLineaDesgImpuesto
    If Len(sKey) = 0 Then
        mCol.Add oLinea
    Else
        mCol.Add oLinea, sKey
    End If
End Function

Public Function Add(TipoValor As Double, TipoImpuesto As String, BaseImponible As Double) As CLineaDesgImpuesto
    'create a new object
    Dim objnewmember As CLineaDesgImpuesto
    Set objnewmember = New CLineaDesgImpuesto

    'set the properties passed into the method
    objnewmember.TipoValor = TipoValor
    objnewmember.TipoImpuesto = TipoImpuesto
    objnewmember.BaseImponible = BaseImponible
    
    mCol.Add objnewmember, CStr(mCol.Count + 1)

    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

Public Property Get Item(vntIndexKey As Variant) As CLineaDesgImpuesto
  Set Item = mCol(vntIndexKey)
End Property

Public Property Get Count() As Long
    'used when retrieving the number of elements in the
    'collection. Syntax: Debug.Print x.Count
    Count = mCol.Count
End Property


Public Sub Remove(vntIndexKey As Variant)
    'used when removing an element from the collection
    'vntIndexKey contains either the Index or Key, which is why
    'it is declared as a Variant
    'Syntax: x.Remove(xyz)
    mCol.Remove vntIndexKey
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
    'creates the collection when this class is created
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Function Existe(Key As String)
    Dim o As CLineaDesgImpuesto  '
    On Error Resume Next
    '
    Set o = mCol.Item(Key)
    ' si se produce un error es que no existe ese elemento
    If Err.Number <> 0 Then
        Existe = False
    Else
        Existe = True
    End If
    Set o = Nothing
    
End Function

Public Function toString() As String
    toString = ""
    Dim oLinea As CLineaDesgImpuesto
    For Each oLinea In mCol
        toString = toString & oLinea.toString & vbCrLf
    Next
End Function

Public Sub cargarLineasDesgloseImpuestoOrden(ByVal IdOrden As Long, ByVal CiaComp As Long, ByVal Idioma As String)

    Dim ador As ADODB.Recordset
    Dim adoCom As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim sFSG As String
    
    sFSG = Fsgs(m_oConexion, CiaComp)

    Set adoCom = New ADODB.Command
    Set adoCom.ActiveConnection = m_oConexion.AdoCon
    adoCom.CommandText = sFSG & "FSGS_DESGLOSE_IMPUESTOS_ORDEN"
    adoCom.CommandType = adCmdStoredProc
    
    Set adoParam = adoCom.CreateParameter("ORDEN", adInteger, adParamInput, , IdOrden)
    adoCom.Parameters.Append adoParam
    Set adoParam = adoCom.CreateParameter("IDIOMA", adVarChar, adParamInput, 20, Idioma)
    adoCom.Parameters.Append adoParam
    

    Set ador = adoCom.Execute
    Set mCol = Nothing
    Set mCol = New Collection
        
    While Not ador.eof
        Add ador("TIPO").Value, ador("DEN").Value, ador("BI").Value
        ador.MoveNext
    Wend

    If ador.eof Then
        ador.Close
        Set ador = Nothing
        Set adoCom = Nothing
        Exit Sub
    Else
        ador.ActiveConnection = Nothing
        Set adoCom = Nothing
    End If
End Sub


Public Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Set Conexion(oConexion As CConexion)
    Set m_oConexion = oConexion
End Property

'''<summary>Devuelve el valor de impuestos total que es la suma de las cuotas de los impuestos de la orden</summary>
Public Function GetImporteImpuesto() As Double
    Dim oLineaDesglose As CLineaDesgImpuesto
    Dim impuesto As Double

    impuesto = 0
    For Each oLineaDesglose In mCol
        impuesto = impuesto + oLineaDesglose.cuota
    Next
    Set oLineaDesglose = Nothing
    
    GetImporteImpuesto = impuesto
End Function
