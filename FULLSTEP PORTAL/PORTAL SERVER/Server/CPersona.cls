VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPersona"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private msCod As String
Private msNom As Variant
Private msApe As String
Private msDep As Variant
Private msDepDen As Variant
Private msCargo As Variant
Private msEmail As Variant
Private msTfno As Variant
Private msTfno2 As Variant
Private msFax As Variant
Private msUON1 As Variant
Private msUON2 As Variant
Private msUON3 As Variant
Private msUON1DEN As Variant
Private msUON2DEN As Variant
Private msUON3DEN As Variant
Private msEqp As Variant
Private moConexion As CConexion


Public Property Get cod() As String
    cod = msCod
End Property
Public Property Let cod(ByVal vData As String)
    msCod = vData
End Property

Public Property Get Nombre() As Variant
    Nombre = msNom
End Property
Public Property Let Nombre(ByVal vData As Variant)
    msNom = vData
End Property

Public Property Get Apellidos() As String
    Apellidos = msApe
End Property
Public Property Let Apellidos(ByVal vData As String)
    msApe = vData
End Property

Public Property Get Dep() As Variant
    Dep = msDep
End Property
Public Property Let Dep(ByVal vData As Variant)
    msDep = vData
End Property

Public Property Get Cargo() As Variant
    Cargo = msCargo
End Property
Public Property Let Cargo(ByVal vData As Variant)
    msCargo = vData
End Property

Public Property Get Email() As Variant
    Email = msEmail
End Property
Public Property Let Email(ByVal vData As Variant)
    msEmail = vData
End Property

Public Property Get Tfno() As Variant
    Tfno = msTfno
End Property
Public Property Let Tfno(ByVal vData As Variant)
    msTfno = vData
End Property

Public Property Get Tfno2() As Variant
    Tfno2 = msTfno2
End Property
Public Property Let Tfno2(ByVal vData As Variant)
    msTfno2 = vData
End Property



Public Property Get Fax() As Variant
    Fax = msFax
End Property
Public Property Let Fax(ByVal vData As Variant)
    msFax = vData
End Property
Public Property Get UON1() As Variant
    UON1 = msUON1
End Property
Public Property Let UON1(ByVal vData As Variant)
    msUON1 = vData
End Property

Public Property Get UON2() As Variant
    UON2 = msUON2
End Property
Public Property Let UON2(ByVal vData As Variant)
    msUON2 = vData
End Property


Public Property Get UON3() As Variant
    UON3 = msUON3
End Property

Public Property Let UON3(ByVal vData As Variant)
    msUON3 = vData
End Property

Public Property Get UON1DEN() As Variant
    UON1DEN = msUON1DEN
End Property

Public Property Let UON1DEN(ByVal vData As Variant)
    msUON1DEN = vData
End Property

Public Property Get UON2DEN() As Variant
    UON2DEN = msUON2DEN
End Property

Public Property Let UON2DEN(ByVal vData As Variant)
    msUON2DEN = vData
End Property

Public Property Get UON3DEN() As Variant
    UON3DEN = msUON3DEN
End Property

Public Property Let UON3DEN(ByVal vData As Variant)
    msUON3DEN = vData
End Property

Public Property Get Eqp() As Variant
    Eqp = msEqp
End Property
Public Property Let Eqp(ByVal vData As Variant)
    msEqp = vData
End Property


Public Property Get DepDen() As Variant
    DepDen = msDepDen
End Property
Public Property Let DepDen(ByVal vData As Variant)
    msDepDen = vData
End Property


Public Property Set Conexion(oCon As CConexion)
Set moConexion = oCon
End Property

Public Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property

''' <summary>
''' Cargar los datos de una persona
''' </summary>
''' <param name="lCiaComp">C�digo de compania</param>
''' <remarks>Llamada desde: detallepersona.asp; Tiempo m�ximo: 0,1</remarks>
Public Sub CargarPersona(ByVal lCiaComp As Long)

    Dim adoCom As ADODB.Command
    Dim adoPar As ADODB.Parameter
    Dim ador As ADODB.Recordset
    Dim sConsulta As String
    Dim sFSG As String
    
    sFSG = FSGS(moConexion, lCiaComp)
    Set ador = New ADODB.Recordset
    Set ador.ActiveConnection = moConexion.AdoCon
    
    'CALIDAD Resulta q no se puede poner WITH (NOLOCK) en esta sentencia pq esta preguntando por tablas de otro servidor
    'De poner WITH(NOLOCK) dar�a el siguiente error:
    '   Number : -2147217900
    '   Description: Cannot specify an index or locking hint for a remote data source.
    sConsulta = "SELECT PER.COD,PER.NOM,PER.APE,PER.CAR,PER.TFNO,PER.TFNO2,PER.FAX," _
    & " PER.EMAIL,UON1.DEN AS UON1,UON2.DEN AS UON2,UON3.DEN AS UON3,DEP.DEN AS DEP" _
    & " FROM " & sFSG & "PER PER INNER JOIN " & sFSG & "DEP DEP ON DEP.COD=PER.DEP" _
    & " LEFT JOIN " & sFSG & "UON1 UON1 ON PER.UON1=UON1.COD" _
    & " LEFT JOIN " & sFSG & "UON2 UON2 ON PER.UON1=UON2.UON1 AND PER.UON2=UON2.COD" _
    & " LEFT JOIN " & sFSG & "UON3 UON3 ON PER.UON1=UON3.UON1 AND PER.UON2=UON2.COD AND PER.UON3=UON3.COD " _
    & " WHERE PER.COD = ?"
    
    Set adoCom = New ADODB.Command
    Set ador = New ADODB.Recordset
    
    Set adoCom.ActiveConnection = moConexion.AdoCon

    Set adoPar = adoCom.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=20, Value:=msCod)
    adoCom.Parameters.Append adoPar

    adoCom.CommandText = sConsulta
    adoCom.CommandType = adCmdText
    
    Set ador = adoCom.Execute

    Me.cod = ador("COD").Value
    Me.Nombre = ador("NOM").Value
    Me.Apellidos = ador("APE").Value
    Me.Email = ador("EMAIL").Value
    Me.Fax = ador("FAX").Value
    Me.Tfno = ador("TFNO").Value
    Me.Tfno2 = ador("TFNO2").Value
    Me.Cargo = ador("CAR").Value
    Me.DepDen = ador("DEP").Value
    Me.UON1DEN = ador("UON1").Value
    Me.UON2DEN = ador("UON2").Value
    Me.UON3DEN = ador("UON3").Value
    
    ador.Close
    Set ador = Nothing
    Set adoPar = Nothing
    Set adoCom = Nothing

End Sub


