VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEquipos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'********************* CEquipos **********************************
'*             Autor : Javier Arana
'*             Creada : 7/9/98
'****************************************************************

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean



Public Function Add(ByVal Cod As String, ByVal Den As String, Optional ByVal varIndice As Variant) As CEquipo
    'create a new object
    Dim objnewmember As CEquipo
    Set objnewmember = New CEquipo
    
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    Set objnewmember.Conexion = mvarConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
    'set the properties passed into the method
       mCol.Add objnewmember, Cod
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Property Get Item(vntIndexKey As Variant) As CEquipo
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
On Error GoTo NoSeEncuentra
    
    mCol.Remove vntIndexKey
    Exit Sub
NoSeEncuentra:

End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
  
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'Eqproys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

Public Function DevolverEquipo(ByVal Cod As String) As CEquipo
Dim oEqp  As CEquipo
Dim iIndice As Integer
On Error Resume Next

For iIndice = 1 To mCol.Count
    
    Set oEqp = mCol.Item(iIndice)
    If oEqp.Cod = Cod Then
        Set DevolverEquipo = oEqp
        Set oEqp = Nothing
        Exit Function
    End If

Next

Set oEqp = Nothing
Set DevolverEquipo = Nothing

End Function


Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Public Function DevolverLosCodigos() As TipoDatosCombo
   
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
    
    Next

   DevolverLosCodigos = Codigos
   
End Function




