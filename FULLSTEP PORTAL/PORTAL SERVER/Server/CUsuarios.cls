VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUsuarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mvarConexion As CConexion
Private mCol As Collection

Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Public Function Add(ByVal Cia As Long, ByVal ID As Integer, ByVal Cod As String, _
                    ByVal Nombre As String, ByVal Apellidos As String, _
                    ByVal EstadoFP As Variant, Optional ByVal pvTipoMail As Variant) As CUsuario
    
    ''' * Objetivo: Anyadir un usuario a la coleccion
    ''' * Recibe: Datos del Usuario
    ''' * Devuelve: Usuario anyadido
    
    Dim objnewmember As CUsuario
    
    ''' Creacion de objeto Usuario
    
    Set objnewmember = New CUsuario
   
   
    ''' Paso de los parametros al nuevo objeto Usuario

    objnewmember.Cia = Cia
    objnewmember.ID = ID
    objnewmember.Cod = Cod
    objnewmember.Nombre = Nombre
    objnewmember.Apellidos = Apellidos
    If IsNull(EstadoFP) Or IsMissing(EstadoFP) Then
        objnewmember.EstadoProveedor = 0
    Else
        objnewmember.EstadoProveedor = EstadoFP
    End If
    
    Set objnewmember.Conexion = mvarConexion
    
    mCol.Add objnewmember, CStr(ID)
    
    If IsNull(pvTipoMail) Or IsMissing(pvTipoMail) Then
        objnewmember.TipoMail = ""
    Else
        objnewmember.TipoMail = pvTipoMail
    End If
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function

Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar un Usuario de la coleccion
    ''' * Recibe: Indice del Usuario a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub

Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property

Public Property Get Item(vntIndexKey As Variant) As CUsuario

    ''' * Objetivo: Recuperar un usuario de la coleccion
    ''' * Recibe: Indice del usuario a recuperar
    ''' * Devuelve: usuario correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

