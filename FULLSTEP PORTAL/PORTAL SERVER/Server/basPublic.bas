Attribute VB_Name = "basPublic"
Public gInstancia As String

Public oIdsMail As Object
Public Const giChunkSize As Integer = 16384

Public Const ClavePar5Bytes = "agkag�"
Public Const ClaveImp5bytes = "h+hlL_"

Public Const ClaveSQLtod = "aldj�w"
Public Const ClaveOBJtod = "jldfad"
Public Const ClaveADMpar = "pruiop"
Public Const ClaveUSUpar = "agkag�"
Public Const ClaveUSUimp = "h+hlL_"

Public Const ClaveADMimp As String = "ljkdag"

Public oFSIS As Fullstep_FSNLibraryCOM.CFSISService

' CONSTANTES PARA EL LOG DE CAMBIOS E INTEGRACION
Public Const Accion_Alta            As String = "I"
Public Const Accion_Baja            As String = "D"
Public Const Accion_Modificacion    As String = "U"
Public Const Accion_CambioCodigo    As String = "C"
Public Const Accion_Reenvio         As String = "R"
Public Const Accion_CambioEstado    As String = "E"
Public Const Accion_Homologacion    As String = "H" 'Esta acci�n es exclusiva para PROVEEDORES, en la adjudicacion de procesos
Public Const Accion_CambioNivel4    As String = "4" 'Esta acci�n es exclusiva para ARTICULOS, implica una reubicaci�n del art�culo

Public Const NumeroVersion As String = "371"
