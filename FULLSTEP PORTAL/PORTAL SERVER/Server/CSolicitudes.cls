VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSolicitudes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mCol As Collection
Private moConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
Set moConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = moConexion
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub
Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set moConexion = Nothing
End Sub

Public Property Get Count() As Long

    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If

End Property

''' <summary>
''' Devolver las solicitudes pendientes
''' </summary>
''' <param name="CiaComp">C�digo de compania</param>
''' <param name="CiaId">Id de compania</param>
''' <param name="Usu">Usuario</param>
''' <returns>solicitudes pendientes</returns>
''' <remarks>Llamada desde: Pendientes.asp; Tiempo m�ximo: 0,1</remarks>
Public Function DevolverPendientes(ByVal CiaComp As Long, ByVal CiaId As Long, ByVal Usu As String, ByVal EmpresaPortal As Boolean, ByVal NomPortal As String) As ADODB.Recordset
    Dim ador As ADODB.Recordset
    Dim Str As String
    
    Str = "EXEC FSQA_SOLPENDIENTES @CIACOMP=?,@CIAPROVE=?,@USU=?,@NOM_PORTAL=?"
    
    Dim adoCom As ADODB.Command
    
    Set adoCom = New ADODB.Command
    adoCom.ActiveConnection = moConexion.AdoCon
    adoCom.CommandType = adCmdText
    Dim adoPar As ADODB.Parameter
    
    Set adoPar = adoCom.CreateParameter("CIACOMP", adInteger, adParamInput, , CiaComp)
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("CIAPROVE", adInteger, adParamInput, , CiaId)
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("USU", adVarChar, adParamInput, 20, Usu)
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("NOM_PORTAL", adVarChar, adParamInput, 50, IIf(EmpresaPortal, NomPortal, Null))
    adoCom.Parameters.Append adoPar
    
    adoCom.CommandText = Str
    Set ador = adoCom.Execute
        
    ador.ActiveConnection = Nothing
    Set DevolverPendientes = ador
        
End Function

''' <summary>
''' Devolver el NUMERO de solicitudes pendientes
''' </summary>
''' <param name="CiaComp">C�digo de compania</param>
 '' <param name="CodProve">C�digo del proveedor</param>
''' <returns>N� de solicitudes pendientes</returns>
''' <remarks>Llamada desde: Pendientes.asp; Tiempo m�ximo: 0,1</remarks>
Public Function DevolverPendientesSolicitudes(ByVal Cia As Long, ByVal CodProveGS As String, ByVal EmpresaPortal As Boolean, ByVal NomPortal As String) As ADODB.Recordset
    Dim ador As ADODB.Recordset
    Dim Str As String
        
    Str = "EXEC " & Fsgs(moConexion, Cia) & " FSPM_GET_SOLICITUDES_PDTES_PROVE @PRV=?, @NOM_PORTAL=?"
    
    Dim adoCom As ADODB.Command
    
    Set adoCom = New ADODB.Command
    adoCom.ActiveConnection = moConexion.AdoCon
    adoCom.CommandType = adCmdText
    adoCom.CommandText = Str
    Dim adoPar As ADODB.Parameter
        
    Set adoPar = adoCom.CreateParameter("PRV", adVarChar, adParamInput, 50, CodProveGS)
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("NOM_PORTAL", adVarChar, adParamInput, 50, IIf(EmpresaPortal, NomPortal, Null))
    adoCom.Parameters.Append adoPar
    
    Set ador = adoCom.Execute
        
    ador.ActiveConnection = Nothing
    Set DevolverPendientesSolicitudes = ador
End Function

Public Function DevolverNumSolicitudesPorTipo(ByVal Cia As Long, ByVal CodProveGS As String, ByVal sTipos As String) As Integer
    Dim ador As ADODB.Recordset
    Dim Str As String
        
    Str = "EXEC " & Fsgs(moConexion, Cia) & " FSN_SOLICITUDES_POR_TIPOS @PRV=?, @TIPOS=?"
    
    Dim adoCom As ADODB.Command
    
    Set adoCom = New ADODB.Command
    adoCom.ActiveConnection = moConexion.AdoCon
    adoCom.CommandType = adCmdText
    adoCom.CommandText = Str
    Dim adoPar As ADODB.Parameter
        
    Set adoPar = adoCom.CreateParameter("PRV", adVarChar, adParamInput, 50, CodProveGS)
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("TIPOS", adVarChar, adParamInput, 50, sTipos)
    adoCom.Parameters.Append adoPar
    
    Set ador = adoCom.Execute
        
    ador.ActiveConnection = Nothing
    DevolverNumSolicitudesPorTipo = ador("NUMSOLICITUDES").Value
End Function


Public Function DevolverNumSolicitudesPorTipo_Alta(ByVal Cia As Long, ByVal CodProveGS As String, ByVal sTipo As String, ByVal sIdi As String, ByVal idContacto As Long) As Integer
    Dim ador As ADODB.Recordset
    Dim Str As String
        
    Str = "EXEC " & Fsgs(moConexion, Cia) & " FSPM_LOADSOLICITUDES_ALTA_PORTAL @PROVE=?, @CONTACTO=?, @TIPO=?, @IDI=?"
    
    Dim adoCom As ADODB.Command
    
    Set adoCom = New ADODB.Command
    adoCom.ActiveConnection = moConexion.AdoCon
    adoCom.CommandType = adCmdText
    adoCom.CommandText = Str
    Dim adoPar As ADODB.Parameter
        
    Set adoPar = adoCom.CreateParameter("PROVE", adVarChar, adParamInput, 50, CodProveGS)
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("CONTACTO", adInteger, adParamInput, , idContacto)
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("TIPO", adInteger, adParamInput, , CInt(sTipo))
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("IDI", adVarChar, adParamInput, 3, sIdi)
    adoCom.Parameters.Append adoPar
    Set ador = adoCom.Execute
        
    ador.ActiveConnection = Nothing
    DevolverNumSolicitudesPorTipo_Alta = ador.RecordCount
End Function



''' <summary>
''' Devolver certificados pendientes del proveedor
''' </summary>
''' <param name="CiaComp">C�digo de compania</param>
''' <param name="Idioma">Idioma</param>
''' <param name="CodProve">proveedor</param>
''' <returns>Certificados pendientes del proveedor</returns>
''' <remarks>Llamada desde: pendientes.asp; Tiempo m�ximo: 0,1</remarks>
Public Function DevolverPendientesCertificados(ByVal CiaComp As Long, ByVal Idioma As String, ByVal CodProve As String) As ADODB.Recordset
    Dim ador As ADODB.Recordset
    Dim Str As String
    Dim sFSG As String
        
    Idioma = EsIdiomaValido(Idioma)
    sFSG = Fsgs(moConexion, CiaComp)

    Str = "EXEC " & sFSG & "FSQA_GET_NUM_CERTIFICADOS_PEND @CODPROVE=?"
    
    Dim adoCom As ADODB.Command
    
    Set adoCom = New ADODB.Command
    adoCom.ActiveConnection = moConexion.AdoCon
    adoCom.CommandType = adCmdText
    adoCom.CommandText = Str
    Dim adoPar As ADODB.Parameter
    
    Set adoPar = adoCom.CreateParameter("CODPROVE", adVarChar, adParamInput, 50, CodProve)
    adoCom.Parameters.Append adoPar

    Set ador = adoCom.Execute
        
    ador.ActiveConnection = Nothing
    Set DevolverPendientesCertificados = ador
End Function

Public Function DevolverPendientesOfertas(ByVal CiaComp As Long, ByVal CodProve As String, ByVal EmpresaPortal As Boolean, ByVal NomPortal As String) As ADODB.Recordset
    Dim ador As ADODB.Recordset
    Dim Str As String
    Dim sFSG As String

    sFSG = Fsgs(moConexion, CiaComp)

    Str = "EXEC " & sFSG & "FSQA_GET_NUM_OFERTAS_PEND @CODPROVE=?,@NOM_PORTAL=?"

    Dim adoCom As ADODB.Command

    Set adoCom = New ADODB.Command
    adoCom.ActiveConnection = moConexion.AdoCon
    adoCom.CommandType = adCmdText
    adoCom.CommandText = Str
    Dim adoPar As ADODB.Parameter

    Set adoPar = adoCom.CreateParameter("CODPROVE", adVarChar, adParamInput, 50, CodProve)
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("NOM_PORTAL", adVarChar, adParamInput, 50, IIf(EmpresaPortal, NomPortal, Null))
    adoCom.Parameters.Append adoPar
    
    Set ador = adoCom.Execute

    ador.ActiveConnection = Nothing
    Set DevolverPendientesOfertas = ador
End Function

''' <summary>
''' Devolver las solicitudes del proveedor
''' </summary>
''' <param name="CiaComp">C�digo de compania</param>
''' <param name="Prove">proveedor</param>
''' <param name="Idioma">Idioma</param>
''' <returns>Solicitudes del proveedor</returns>
''' <remarks>Llamada desde: solicitudes11.asp; Tiempo m�ximo: 0,1</remarks>
Public Function DevolverSolicitudes(ByVal CiaComp As Long, ByVal Prove As String, ByVal Idioma As String) As ADODB.Recordset
    Dim ador As ADODB.Recordset
    Dim sConsulta As String
    Dim sFSG As String

    Idioma = EsIdiomaValido(Idioma)
    sFSG = Fsgs(moConexion, CiaComp)
    
    sConsulta = "EXEC " & sFSG & "FSPM_GET_INSTANCIAS_PROVE @PROVE=?, @IDI = ?, @NUEVO_WORKLF = ?"
    
    Dim adoCom As ADODB.Command
    
    Set adoCom = New ADODB.Command
    adoCom.ActiveConnection = moConexion.AdoCon
    adoCom.CommandType = adCmdText
    adoCom.CommandText = sConsulta
    Dim adoPar As ADODB.Parameter
    
    Set adoPar = adoCom.CreateParameter("PROVE", adVarChar, adParamInput, 50, Prove)
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("IDI", adVarChar, adParamInput, 50, Idioma)
    adoCom.Parameters.Append adoPar
    
        Set adoPar = adoCom.CreateParameter("NUEVO_WORKLF", adTinyInt, adParamInput, , 1)
        adoCom.Parameters.Append adoPar
    
    Set ador = adoCom.Execute

    
    If ador.eof Then
        ador.Close
        Set ador = Nothing
        Set DevolverSolicitudes = Nothing
        Exit Function
    Else
        'Devuelve un recordset desconectado
        ador.ActiveConnection = Nothing
        Set DevolverSolicitudes = ador
    End If
    
End Function

Public Sub DevolverSolicitudesRequieren(ByVal CiaComp As Long, ByVal Prove As String, ByVal Idioma As String, ByRef Pendientes As Variant)
    Dim sConsulta As String
    Dim adoCom As ADODB.Command
    Dim adoPar As ADODB.Parameter
    Dim sFSG As String
    
    
    sFSG = Fsgs(moConexion, CiaComp)
    
    sConsulta = "EXEC " & sFSG & "FSPM_GETWORKFLOW_PROVE_REQUIERE @PROVE=?, @PENDIENTES=? OUTPUT"
    
    Set adoCom = New ADODB.Command
    adoCom.ActiveConnection = moConexion.AdoCon
    adoCom.CommandType = adCmdText
    adoCom.CommandText = sConsulta
    
    Set adoPar = adoCom.CreateParameter("PROVE", adVarChar, adParamInput, 50, Prove)
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("PENDIENTES", adInteger, adParamOutput)
    adoCom.Parameters.Append adoPar
    adoCom.Execute

    Pendientes = adoCom.Parameters("PENDIENTES").Value
    
End Sub

''' <summary>
''' Devolver el NUMERO de facturas pendientes
''' </summary>
''' <param name="CiaComp">C�digo de compania</param>
 '' <param name="CodProve">C�digo del proveedor</param>
''' <returns>N� de facturas pendientes</returns>
''' <remarks>Llamada desde: Pendientes.asp; Tiempo m�ximo: 0,1</remarks>
Public Function DevolverPendientesFacturas(ByVal Cia As Long, ByVal CodProve As String) As ADODB.Recordset
    Dim ador As ADODB.Recordset
    Dim Str As String
    Dim sFSG As String
        
    sFSG = Fsgs(moConexion, Cia)
    Str = "EXEC " & sFSG & "FSPM_GET_FACTURAS_PDTES_PROVE @PROVE=?"
    
    Dim adoCom As ADODB.Command
    
    Set adoCom = New ADODB.Command
    adoCom.ActiveConnection = moConexion.AdoCon
    adoCom.CommandType = adCmdText
    adoCom.CommandText = Str
    Dim adoPar As ADODB.Parameter
    
    Set adoPar = adoCom.CreateParameter("PROVE", adVarChar, adParamInput, 50, CodProve)
    adoCom.Parameters.Append adoPar
    
    Set ador = adoCom.Execute
        
    ador.ActiveConnection = Nothing
    Set DevolverPendientesFacturas = ador
End Function

