VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCrypt2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private sInBuffer As String
Private sOutBuffer As String
Private sCodigo As String
Private dFecha As Date

''' <summary>
''' Funci�n q establece el Texto a encriptar o desencriptar
''' </summary>
''' <param name="vNewValue">Texto a encriptar o desencriptar</param>
''' <remarks>Llamada desde: basUtilildades.Encriptar basUtilildades.DesEncriptar ; Tiempo m�ximo: 0</remarks>
Public Property Let InBuffer(vNewValue As String)
    sInBuffer = vNewValue
End Property

''' <summary>
''' Funci�n q devuelve el Texto encriptado o desencriptado
''' </summary>
''' <returns>Texto encriptado/desencriptado</returns>
''' <remarks>Llamada desde: basUtilildades.Encriptar basUtilildades.DesEncriptar ; Tiempo m�ximo: 0</remarks>
Public Property Get OutBuffer() As String
    OutBuffer = sOutBuffer
End Property

''' <summary>
''' Funci�n q establece el codigo q es parte de la Key de encriptaci�n
''' </summary>
''' <param name="vNewValue">Codigo</param>
''' <remarks>Llamada desde: basUtilildades.Encriptar basUtilildades.DesEncriptar ; Tiempo m�ximo: 0</remarks>
Public Property Let Codigo(vNewValue As String)
    sCodigo = vNewValue
End Property

''' <summary>
''' Funci�n q establece la fecha q es parte de la Key de encriptaci�n
''' </summary>
''' <remarks>Llamada desde: basUtilildades.Encriptar basUtilildades.DesEncriptar ; Tiempo m�ximo: 0</remarks>
Public Property Let Fecha(vNewValue As Date)
    dFecha = vNewValue
End Property

''' <summary>
''' Funci�n que encripta un string
''' </summary>
''' <remarks>Llamada desde: ; Tiempo m�ximo: 0</remarks>
Public Sub Encrypt()
    Dim ObjCrypt As FSNAES
    Set ObjCrypt = New FSNAES

    sOutBuffer = ObjCrypt.EncryptStringToString(dFecha, sCodigo, sInBuffer)
End Sub

''' <summary>
''' Funci�n que desencripta un string
''' </summary>
''' <remarks>Llamada desde: basUtilildades.Encriptar basUtilildades.DesEncriptar ; Tiempo m�ximo: 0</remarks>
Public Sub Decrypt()
    Dim ObjCrypt As FSNAES
    Set ObjCrypt = New FSNAES
    
    sOutBuffer = ObjCrypt.DecryptStringFromString(dFecha, sCodigo, sInBuffer)
End Sub


