Attribute VB_Name = "basFormatos"

Public Function VisualizacionNumero(Num, DecFmt, ThouFmt, PrecFmt) As Variant
Dim numeroLocal
Dim snumeroLocal
Dim decimalloc
Dim i
Dim ParteDecimal
Dim tmpParteDecimal
Dim quitarCeros
Dim newValor

    numeroLocal = 1 / 10

    snumeroLocal = CStr(numeroLocal)
    If InStr(snumeroLocal, ".") Then
        decimalloc = "."
    Else
        decimalloc = ","
    End If

    Num = Round(Num, PrecFmt)

    If IsNull(Num) Or Num = 0 Or Num = "" Then
        VisualizacionNumero = ""
        Exit Function
    End If

    ParteEntera = CStr(Fix(Num))
    sNum = CStr(Num)
    tmpParteDecimal = ""

    If InStr(sNum, decimalloc) > 0 Then
        ParteDecimal = Mid(sNum, InStr(sNum, decimalloc) + 1, PrecFmt)
    Else
        ParteDecimal = ""
    End If

    quitarCeros = True

    For i = Len(ParteDecimal) To 1 Step -1
        If Mid(ParteDecimal, i, 1) = "0" And quitarCeros Then
        Else
            tmpParteDecimal = Mid(ParteDecimal, i, 1) & tmpParteDecimal
            quitarCeros = False
        End If
    Next

    ParteDecimal = tmpParteDecimal
    newValor = ""
    s = 0
    For i = Len(ParteEntera) To 1 Step -1
        If s = 3 Then
            newValor = ThouFmt & newValor
            s = 0
        End If
        newValor = Mid(ParteEntera, i, 1) & newValor
        s = s + 1
    Next
    If ParteDecimal <> "" Then
        VisualizacionNumero = newValor & DecFmt & ParteDecimal
    Else
        VisualizacionNumero = newValor
    End If
End Function


    
'ESTA FUNCION DEVUELVE UNA CADENA CON UNA FECHA FORMATEADA DEPENDIENDO
'DE LOS PARAMETROS SIGUIENTE
'
'Fecha: la fecha a formatear
'datfmt: formato a aplicar a dicha fecha

Public Function VisualizacionFecha(Fecha, datFmt) As Variant

sepFecha = Mid(datFmt, 3, 1)
If IsNull(Fecha) Then
    VisualizacionFecha = ""
    Exit Function
End If
sDia = CStr(Day(Fecha))
sMes = CStr(Month(Fecha))
sUrte = CStr(Year(Fecha))


sDia = Left("00", 2 - Len(sDia)) & sDia
sMes = Left("00", 2 - Len(sMes)) & sMes

returnFecha = Replace(datFmt, "dd", sDia)
returnFecha = Replace(returnFecha, "mm", sMes)
returnFecha = Replace(returnFecha, "yyyy", sUrte)
VisualizacionFecha = returnFecha

End Function

Public Function DblQuote(ByVal StrToDblQuote As String) As String

    Dim Ind As Long
    Dim QPos As Long
    Dim pos As Long
    
    Dim TmpDblQuote As String
    
    TmpDblQuote = ""
    pos = 1
    
    Do While Len(StrToDblQuote) > 0
    
        QPos = InStr(pos, StrToDblQuote, "'")
        
        If QPos > 0 Then
            
            TmpDblQuote = TmpDblQuote & Mid(StrToDblQuote, pos, QPos - 1) & "''"
            StrToDblQuote = Right(StrToDblQuote, Len(StrToDblQuote) - QPos)
            
        Else
        
            TmpDblQuote = TmpDblQuote & StrToDblQuote
            Exit Do
            
        End If
            
    Loop
    
    DblQuote = TmpDblQuote
    
End Function

Public Function NullToStr(ByVal ValueThatCanBeNull As Variant) As String
  
    If IsNull(ValueThatCanBeNull) Then
        NullToStr = ""
    Else
        NullToStr = ValueThatCanBeNull
    End If

End Function
