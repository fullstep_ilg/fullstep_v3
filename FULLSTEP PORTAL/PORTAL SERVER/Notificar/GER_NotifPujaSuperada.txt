Le informamos que su puja ha sido superada por otro proveedor.


	Datos de la Compa��a proveedora
	------------------------------------------------------
  	C�digo:		@COD_CIA
  	Denominaci�n: 	@DEN_CIA

	Datos del proveedor 
	------------------------------------------------------
	Nombre:		@NOM_PROVE
  	Apellidos:	@APE_PROVE
  	Cargo:		@CAR_PROVE
  	Tel�fono:	@TFNO1_PROVE
	Tel�fono 2:	@TFNO2_PROVE
	Fax:		@FAX_PROVE
	E-mail:		@EMAIL_PROVE
	Departamento:	@DEP_PROVE

	Datos del proceso
	------------------------------------------------------
	A�o:			@ANYO_PROCE
	Gmn1:			@GMN1_PROCE
	C�digo:			@COD_PROCE
	Denominaci�n:		@DEN_PROCE
	Material de 1� nivel:	@COD_GMN1 - @DEN_GMN1
	Material de 2� nivel:	@COD_GMN2 - @DEN_GMN2
	Material de 3� nivel:	@COD_GMN3 - @DEN_GMN3
	Material de 4� nivel:	@COD_GMN4 - @DEN_GMN4
	Material:		@MATERIAL

	Datos de la subasta
	------------------------------------------------------
	Fecha de apertura:	@FEC_APE
	Hora de apertura:	@HORA_APE
	Fecha UTC de apertura:	@FEC_UTC_APE
	Hora UTC de apertura:	@HORA_UTC_APE
	Fecha de cierre:	@FEC_CIERRE
	Hora de cierre:		@HORA_CIERRE
	Fecha UTC de cierre:	@FEC_UTC_CIERRE
	Hora UTC de cierre:	@HORA_UTC_CIERRE

	Datos del comprador responsable 
	------------------------------------------------------
	Nombre: 	@NOM_COM
	Apellidos: 	@APE_COM
	E-mail:  	@EMAIL_COM
	Tel�fono: 	@TFNO1_COM
	Tel�fono 2:	@TFNO2_COM
	Fax:		@FAX_COM
	Cargo:		@CARGO_COM

	
<!--INICIO MODO PROCESO-->
	Proceso en el que su oferta ha sido superada 
	======================================================
<!-- Comienzo de subplantilla -->
	Proceso:		@COD_PROCE - @DEN_PROCE
	<!--INICIO PRECIO-->Importe:		@IMPORTE @COD_MON<!--FIN PRECIO-->
	<!--INICIO PROVE-->Proveedor que le
	ha superado la puja:	@COD_PROVE - @DEN_PROVE<!--FIN PROVE-->
<!-- Fin de subplantilla -->
<!--FIN MODO PROCESO-->
<!--INICIO MODO GRUPOS-->
	Grupos en los que su oferta ha sido superada 
	======================================================
<!-- Comienzo de subplantilla -->
	Grupo:			@COD_GRUPO - @DEN_GRUPO
	<!--INICIO PRECIO-->Importe:		@IMPORTE @COD_MON<!--FIN PRECIO-->
	<!--INICIO PROVE-->Proveedor que le
	ha superado la puja:	@COD_PROVE - @DEN_PROVE<!--FIN PROVE-->
<!-- Fin de subplantilla -->
<!--FIN MODO GRUPOS-->
<!--INICIO MODO ITEMS-->
	�tems en los que su oferta ha sido superada 
	======================================================
<!-- Comienzo de subplantilla -->
	Art�culo:		@COD_ART - @DEN_ART
	Destino:		@COD_DEST - @DEN_DEST
	Cantidad:		@CANT
	Unidad:			@COD_UNI - @DEN_UNI
	<!--INICIO PRECIO-->Importe:		@IMPORTE @COD_MON<!--FIN PRECIO-->
	<!--INICIO PROVE-->Proveedor que le
	ha superado la puja:	@COD_PROVE - @DEN_PROVE<!--FIN PROVE-->
<!-- Fin de subplantilla -->
<!--FIN MODO ITEMS-->