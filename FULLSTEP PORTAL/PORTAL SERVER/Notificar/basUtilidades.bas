Attribute VB_Name = "basUtilidades"
Option Explicit

''' <summary>
''' Funci�n que desencripta un string que se le pasa como par�metro.
''' </summary>
''' <param name="Dato">Dato a desencriptar.</param>
''' <param name="Fecha">Fecha para la desencriptaci�n</param>
''' <returns>El string desencriptado.</returns>
''' <remarks>Llamada desde:  CFSPNotificar.HTMLNotificacionAutUsuario   CFSPNotificar.TEXTONotificacionAutUsuario
''' CFSPNotificar.EnviarEmailUsuRecuerdoPWD     CFSPNotificar.LICDBContra      CFSPNotificar.LICDBLogin
'''; Tiempo m�ximo: 0,1</remarks>
Public Function DesEncriptar(ByVal Dato As String, Optional ByVal Fecha As Variant) As String
    Dim fechahoracrypt
    Dim diacrypt
    Dim oCrypt2 As CCrypt2
        
    If IsMissing(Fecha) Then
        Fecha = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            Fecha = DateSerial(1974, 3, 5)
        End If
    End If

    diacrypt = Day(Fecha)

    fechahoracrypt = Fecha

    Set oCrypt2 = New CCrypt2
    
    oCrypt2.InBuffer = Dato
    If diacrypt Mod 2 = 0 Then
        oCrypt2.Codigo = Clavepar5Bytes
    Else
        oCrypt2.Codigo = Claveimp5Bytes
    End If
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    oCrypt2.Decrypt
    DesEncriptar = oCrypt2.OutBuffer
            
    Set oCrypt2 = Nothing
End Function





Public Function ReplaceV(s1 As String, s2 As String, s3 As Variant) As String

ReplaceV = Replace(s1, s2, NullToStr(s3))



End Function

''' <summary>Devuelve la ubicaci�n de la BD del GS</summary>
''' <param name="oConn">Conexi�n</param>
''' <param name="lCiaComp">Compa��a</param>
''' <returns>string con la ubicaci�n de la BD del GS</returns>
''' <remarks>Llamada desde: Portal; Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 13/01/2012</revision>

Public Function FSGS(oConn As ADODB.Connection, ByVal lCiaComp As Long) As String
    Dim adoRes As ADODB.Recordset
    Dim sConsulta As String
    Dim sSRV As Variant
    Dim sBD As String
        
    'Primero debemos obtener los datos de SERVIDOR,sBD,... de la CiaCompradora
    sConsulta = "SELECT FSGS_SRV,FSGS_BD " & vbCrLf _
              & "  FROM CIAS WITH (NOLOCK) " & vbCrLf _
              & " WHERE ID=" & lCiaComp
    Set adoRes = New ADODB.Recordset
    adoRes.Open sConsulta, oConn, adOpenForwardOnly, adLockReadOnly
    
    If adoRes.EOF Then
        adoRes.Close
        Set adoRes = Nothing
        FSGS = ""
        Exit Function
    End If
    
    sSRV = adoRes("FSGS_SRV").Value
    sBD = adoRes("FSGS_BD").Value
    
    adoRes.Close
    Set adoRes = Nothing
    
    If IsNull(sSRV) Then
        FSGS = sBD & ".dbo."
    Else
        FSGS = sSRV & "." & sBD & ".dbo."
    End If
End Function


