Attribute VB_Name = "basPublic"
Option Explicit
Private Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As Long
Private Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long
Private Declare Function RegQueryValueExString Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, ByVal lpData As String, lpcbData As Long) As Long
Private Declare Function RegQueryValueExLong Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Long, lpcbData As Long) As Long
Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
Private Declare Function RegQueryValueExNULL Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, ByVal lpData As Long, lpcbData As Long) As Long
Public Const HKEY_CURRENT_USER = &H80000001
Public Const KEY_ALL_ACCESS = &H3F
Public Const ERROR_NONE = 0
Public Const REG_SZ As Long = 1
Public Const REG_DWORD As Long = 4

Public gsInstancia As String

Public Const ClavePar5Bytes = "agkag�"
Public Const ClaveImp5bytes = "h+hlL_"

Public Const ClaveSQLtod = "aldj�w"

Public Const NumeroVersion As String = "371"


Function StrToVbNULL(StrThatCanBeEmpty) As Variant
    
    If IsEmpty(StrThatCanBeEmpty) Then
        StrToVbNULL = Null
    Else
        If IsNull(StrThatCanBeEmpty) Then
            StrToVbNULL = Null
        Else
            If StrThatCanBeEmpty = "" Then
                StrToVbNULL = Null
            Else
                StrToVbNULL = StrThatCanBeEmpty
            End If
        End If
    End If
    
End Function
Public Function DblQuote(ByVal StrToDblQuote As String) As String

    DblQuote = Replace(StrToDblQuote, "'", "''")
    
End Function


Public Function NullToDbl0(ByVal ValueThatCanBeNull As Variant) As Variant
  
    If IsNull(ValueThatCanBeNull) Then
        NullToDbl0 = 0
    Else
        NullToDbl0 = ValueThatCanBeNull
    End If

End Function

Function DateToSQLDate(DateThatCanBeEmpty) As Variant


If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLDate = "NULL"
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLDate = "NULL"
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLDate = "NULL"
        Else
            DateToSQLDate = "Convert(datetime,'" & Format(DateThatCanBeEmpty, "mm-dd-yyyy") & "',110)"
        End If
    End If
End If

End Function
Function DateToSQLTimeDate(DateThatCanBeEmpty) As Variant


If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLTimeDate = "NULL"
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLTimeDate = "NULL"
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLTimeDate = "NULL"
        Else
            DateToSQLTimeDate = "Convert(datetime,'" & Format(DateThatCanBeEmpty, "mm-dd-yyyy hh:mm:ss") & "',110)"
        End If
    End If
End If

End Function



Public Function SQLBinaryToBoolean(ByVal var As Variant) As Variant
    
    If IsNull(var) Then
        SQLBinaryToBoolean = False
    Else
        If var = 1 Then
            SQLBinaryToBoolean = True
        Else
            SQLBinaryToBoolean = False
        End If
    End If
    

End Function
Public Function BooleanToSQLBinary(ByVal b As Variant) As Variant
    If IsNull(b) Then
        BooleanToSQLBinary = "NULL"
        Exit Function
    End If
    
    If b Then
        BooleanToSQLBinary = 1
    Else
        BooleanToSQLBinary = 0
    End If
    
End Function

Public Function NullToStr(ByVal ValueThatCanBeNull As Variant) As String
  
    If IsNull(ValueThatCanBeNull) Then
        NullToStr = ""
    Else
        NullToStr = ValueThatCanBeNull
    End If

End Function
Function StrToSQLNULL(StrThatCanBeEmpty) As Variant
    
    If IsEmpty(StrThatCanBeEmpty) Then
        StrToSQLNULL = "NULL"
    Else
        If IsNull(StrThatCanBeEmpty) Then
            StrToSQLNULL = "NULL"
        Else
            If StrThatCanBeEmpty = "" Then
                StrToSQLNULL = "NULL"
            Else
                StrToSQLNULL = "'" & DblQuote(CStr(StrThatCanBeEmpty)) & "'"
            End If
        End If
    End If
    
End Function
Private Function QueryValueEx(ByVal lHkey As Long, ByVal szValueName As String, vValue As Variant) As Long
Dim cch As Long
Dim lrc As Long
Dim lType As Long
Dim lValue As Long
Dim sValue As String
On Error GoTo QueryValueExError
          
    ' Determine the size and type of data to be read
    lrc = RegQueryValueExNULL(lHkey, szValueName, 0&, lType, 0&, cch)
    If lrc <> ERROR_NONE Then Error 5
    Select Case lType
              ' For strings
        Case REG_SZ:
                sValue = String(cch, 0)
                lrc = RegQueryValueExString(lHkey, szValueName, 0&, lType, sValue, cch)
                If lrc = ERROR_NONE Then
                    vValue = Left$(sValue, cch - 1)
                Else
                    vValue = Empty
                End If
              ' For DWORDS
        Case REG_DWORD:
                lrc = RegQueryValueExLong(lHkey, szValueName, 0&, lType, lValue, cch)
                If lrc = ERROR_NONE Then vValue = lValue
        Case Else                  'all other data types not supported
                lrc = -1
    End Select
    
QueryValueExExit:
QueryValueEx = lrc
Exit Function
QueryValueExError:
          Resume QueryValueExExit

End Function
Public Function QueryValue(sKeyName As String, sValueName As String) As Variant
Dim lRetVal As Long         'result of the API functions
Dim hKey As Long         'handle of opened key
Dim vValue As Variant      'setting of queried value
    
    lRetVal = RegOpenKeyEx(HKEY_CURRENT_USER, sKeyName, 0, KEY_ALL_ACCESS, hKey)
    lRetVal = QueryValueEx(hKey, sValueName, vValue)
    RegCloseKey (hKey)
    QueryValue = vValue
    
End Function
Public Function DblToSQLFloat(DblToConvert) As String

    Dim Ind As Long
    Dim SeekPos As Long
    Dim pos As Long
    
    Dim TmpFloat As String
    Dim StrToConvert As String
    
    Dim sDecimal As String
    Dim sThousand As String
        
    sDecimal = QueryValue("Control Panel\International", "sDecimal")
    sThousand = QueryValue("Control Panel\International", "sThousand")
        
    If IsEmpty(DblToConvert) Then
        DblToSQLFloat = "NULL"
        Exit Function
    End If
    
    If IsNull(DblToConvert) Then
        DblToSQLFloat = "NULL"
        Exit Function
    End If
    
    If DblToConvert = "" Then
        DblToSQLFloat = "NULL"
        Exit Function
    End If
    
    StrToConvert = CStr(DblToConvert)
    
    
    TmpFloat = ""
    pos = 1
    
    Do While Len(StrToConvert) > 0
    
        SeekPos = InStr(pos, StrToConvert, sThousand)
        
        If SeekPos > 0 Then
            
            TmpFloat = TmpFloat & Mid(StrToConvert, pos, SeekPos - 1)
            StrToConvert = Right(StrToConvert, Len(StrToConvert) - SeekPos)
            
        Else
        
            TmpFloat = TmpFloat & StrToConvert
            Exit Do
            
        End If
            
    Loop
    
    DblToSQLFloat = TmpFloat

    StrToConvert = DblToSQLFloat
    TmpFloat = ""
    pos = 1
    
    Do While Len(StrToConvert) > 0
    
        SeekPos = InStr(pos, StrToConvert, sDecimal)
        
        If SeekPos > 0 Then
            
            TmpFloat = TmpFloat & Mid(StrToConvert, pos, SeekPos - 1) & "."
            StrToConvert = Right(StrToConvert, Len(StrToConvert) - SeekPos)
            
        Else
        
            TmpFloat = TmpFloat & StrToConvert
            Exit Do
            
        End If
            
    Loop
    
    DblToSQLFloat = TmpFloat

End Function



Public Function NumTransaccionesAbiertas(oCon As ADODB.Connection) As Integer

Dim sConsulta As String
Dim adoRS As ADODB.Recordset

sConsulta = "SELECT @@TRANCOUNT NUMTRAN"

    
Set adoRS = New ADODB.Recordset
adoRS.Open sConsulta, oCon, adOpenForwardOnly, adLockReadOnly

NumTransaccionesAbiertas = adoRS.Fields("NUMTRAN").Value

adoRS.Close
Set adoRS = Nothing

End Function





Public Function Fecha(ByVal mFecha As Variant, DateFMT As String) As Variant



Dim datesep As String
Dim returnFecha As Variant
Dim mdia As Integer
Dim mmes As Integer
Dim manyo As Integer
Dim sHora As String

If IsNull(mFecha) Or mFecha = "" Then
    Fecha = Null
    Exit Function
End If
returnFecha = mFecha
datesep = Mid(DateFMT, 3, 1)
        
If InStr(returnFecha, ":") Then
    sHora = Mid(returnFecha, InStr(returnFecha, " "), Len(returnFecha))
    returnFecha = Mid(returnFecha, 1, InStr(returnFecha, " "))
End If
        
If Left(DateFMT, 1) = "d" Then
    mFecha = Split(returnFecha, datesep)
                
    mdia = CLng(mFecha(0))
    mmes = CLng(mFecha(1))
    manyo = CLng(mFecha(2))

Else
    mFecha = Split(returnFecha, datesep)
                
    mdia = CLng(mFecha(1))
    mmes = CLng(mFecha(0))
    manyo = CLng(mFecha(2))
End If


On Error Resume Next
If sHora = "" Then
    returnFecha = DateSerial(manyo, mmes, mdia)
Else
    
    returnFecha = DateSerial(manyo, mmes, mdia)
    returnFecha = DateAdd("h", CInt(Mid(sHora, 1, InStr(sHora, ":") - 1)), returnFecha)
    returnFecha = DateAdd("n", CInt(Mid(sHora, InStr(sHora, ":") + 1, 2)), returnFecha)
    
        
End If

Fecha = returnFecha


End Function


Public Function Numero(ByVal Num As Variant, ByVal sDecimalFmt As String) As Variant

    Dim NumeroFinal As Variant
    Dim Indice As Integer
    Dim dividir As Variant
    Dim decimales As Boolean

    Dim Numerico As String
    On Error GoTo Error
    If IsNull(Num) Then
        Numero = Null
        Exit Function
    End If
    dividir = 1
    NumeroFinal = ""

    Num = Replace(Num, sDecimalFmt, ".")


    If Num = "" Then
        Numero = Null
        Exit Function
    End If
        
    For Indice = 1 To Len(Num)
        Select Case Mid(Num, Indice, 1)
            Case "."
                NumeroFinal = NumeroFinal
                decimales = True
            Case Else
                NumeroFinal = NumeroFinal & Mid(Num, Indice, 1)
                If decimales = True Then
                    dividir = dividir * 10
                End If
        End Select
    Next
    If decimales = True Then
        Numerico = CDec(NumeroFinal) / CDec(dividir)
    Else
        Numerico = NumeroFinal
    End If
    
    Numero = Numerico
fin:
    Exit Function
Error:
    Debug.Print Err.Description
    Numero = Null
    Resume fin
    Resume 0
                
End Function

Public Function FSGS(ByRef oConn As ADODB.Connection, ByVal lCiaComp As Long) As String

Dim adoRes As ADODB.Recordset
Dim sConsulta As String

Dim sSRV As String
Dim sBD As String


'Primero debemos obtener los datos de SERVIDOR,sBD,... de la CiaCompradora
sConsulta = "SELECT FSGS_SRV,FSGS_BD " & vbCrLf _
          & "  FROM CIAS " & vbCrLf _
          & " WHERE ID=" & lCiaComp
Set adoRes = New ADODB.Recordset
adoRes.Open sConsulta, oConn, adOpenForwardOnly, adLockReadOnly

If adoRes.EOF Then
    adoRes.Close
    Set adoRes = Nothing
    FSGS = ""
    Exit Function
End If

sSRV = adoRes("FSGS_SRV").Value
sBD = adoRes("FSGS_BD").Value

adoRes.Close
Set adoRes = Nothing

FSGS = sSRV & "." & sBD & ".dbo."


End Function


