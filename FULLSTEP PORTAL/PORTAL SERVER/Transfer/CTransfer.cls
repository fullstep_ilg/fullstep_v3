VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTransfer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
''Private moConexion As FSPServer_31900_601.CConexion
Dim mAdoCon As ADODB.Connection

'Private msRutaAdjunFirmaDig As String

'Public Property Get RutaAdjunFirmaDig() As String
'    RutaAdjunFirmaDig = msRutaAdjunFirmaDig
'End Property
'Public Property Let RutaAdjunFirmaDig(ByVal sRutaAdjunFirmaDig As String)
'    msRutaAdjunFirmaDig = sRutaAdjunFirmaDig
'End Property

Public Sub Conectar(ByVal sInstancia As String)
Dim sServidor As String
Dim sBaseDeDatos As String

If sInstancia = "" Then
    Exit Sub
End If
gsInstancia = sInstancia
    
Set mAdoCon = New ADODB.Connection
sServidor = GetStringSetting("FULLSTEP PORTAL", gsInstancia, "Conexion", "Servidor")
sBaseDeDatos = GetStringSetting("FULLSTEP PORTAL", gsInstancia, "Conexion", "BaseDeDatos")
mAdoCon.Open "Provider=SQLOLEDB.1; Auto Translate=FALSE;Server=" & sServidor & ";Database=" & sBaseDeDatos & ";", LICDBLogin, LICDBContra
mAdoCon.CursorLocation = adUseClient
mAdoCon.CommandTimeout = 120

End Sub

''' <summary>
''' Grabar en bbdd los adjuntos de una oferta
''' </summary>
''' <param name="lCiaComp">C�digo de compania</param>
''' <param name="iAnyo">A�o de proceso</param>
''' <param name="sGMN1">GMN1 de proceso</param>
''' <param name="lProce">Numero de proceso</param>
''' <param name="sProve">Proveedor</param>
''' <param name="iOfe">Numero de oferta</param>
''' <remarks>Llamada desde: TransferirOferta; Tiempo m�ximo: 0,3</remarks>
Public Sub TransferirAdjuntosOferta(ByVal lCiaComp As Long, ByVal iAnyo As Integer, ByVal sGmn1 As String, ByVal lProce As Long, ByVal sProve As String, ByVal iOfe As Integer)

Dim adoRS As ADODB.Recordset
Dim adoRes As ADODB.Recordset
Dim adoCom As ADODB.Command
Dim adoComP As ADODB.Command
Dim adoComGS As ADODB.Command
Dim adoComIn As ADODB.Command
Dim adoPar As ADODB.Parameter
Dim sFSGS As String
Dim lIdPortal As Long
Dim lId As Long
Dim lAmbito As Integer
Dim sConsulta As String

On Error GoTo Error


sFSGS = FSGS(mAdoCon, lCiaComp)

Set adoCom = New ADODB.Command

adoCom.CommandType = adCmdText

adoCom.CommandText = "EXEC " & sFSGS & "SP_DEVOLVER_ADJUNTOS_TRANSFER @ANYO = ?, @GMN1=?, @PROCE=?, @PROVE = ?, @OFE =?"
adoCom.CommandType = adCmdText

Set adoCom.ActiveConnection = mAdoCon

Set adoPar = adoCom.CreateParameter("ANYO", adInteger, adParamInput, , iAnyo)
adoCom.Parameters.Append adoPar
Set adoPar = adoCom.CreateParameter("GMN1", adVarChar, adParamInput, 50, sGmn1)
adoCom.Parameters.Append adoPar
Set adoPar = adoCom.CreateParameter("PROCE", adInteger, adParamInput, , lProce)
adoCom.Parameters.Append adoPar
Set adoPar = adoCom.CreateParameter("PROVE", adVarChar, adParamInput, 50, sProve)
adoCom.Parameters.Append adoPar
Set adoPar = adoCom.CreateParameter("OFE", adInteger, adParamInput, , iOfe)
adoCom.Parameters.Append adoPar

Set adoRS = adoCom.Execute

While Not adoRS.EOF
    lIdPortal = adoRS.Fields("IDPORTAL")
    lId = adoRS.Fields("ID")
    lAmbito = adoRS.Fields("AMBITO")
    mAdoCon.Execute "BEGIN DISTRIBUTED TRAN"
    mAdoCon.Execute "SET XACT_ABORT ON"
    
    Set adoComP = New ADODB.Command
    adoComP.CommandText = sFSGS & "FSGS_ADJUNTOS_TRANSFERIRDEPORTAL"
    Set adoPar = adoComP.CreateParameter("@TIPO", adInteger, adParamInput, , lAmbito)
    adoComP.Parameters.Append adoPar
    Set adoPar = adoComP.CreateParameter("@ID", adInteger, adParamInput, , lId)
    adoComP.Parameters.Append adoPar
    Set adoPar = adoComP.CreateParameter("@IDPORTAL", adInteger, adParamInput, , lIdPortal)
    adoComP.Parameters.Append adoPar
    
    adoComP.CommandType = adCmdStoredProc
    Set adoComP.ActiveConnection = mAdoCon
    adoComP.Execute
    
    Set adoComP = Nothing
    mAdoCon.Execute "COMMIT TRAN"
    mAdoCon.Execute "SET XACT_ABORT OFF"
    adoRS.MoveNext
Wend

fin:
    On Error Resume Next
    adoRS.Close
    Set adoRS = Nothing
    
    Set adoComIn = Nothing
    Exit Sub

Error:
    On Error Resume Next
    Dim bGoto As Boolean
    If Err = 0 Then
        bGoto = True
    Else
        bGoto = False
    End If
    mAdoCon.Execute "ROLLBACK TRAN"
    mAdoCon.Execute "SET XACT_ABORT OFF"
    If bGoto Then
        GoTo fin
    Else
        Resume fin
    End If

End Sub

''' <summary>
''' Grabar en bbdd de Gs la oferta realizada en portal q esta guardada en bbdd portal
''' </summary>
''' <param name="sIdi">Idioma</param>
''' <param name="lCiaComp">C�digo de compania</param>
''' <param name="lId">Id de oferta portal</param>
''' <param name="PYME">C�digo de pyme</param>
''' <param name="bEsPujaSubasta">Indica si se trata de una puja de una subasta</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: coferta/TransferirOferta; Tiempo m�ximo: 0,5</remarks>
Public Sub TransferirOferta(ByVal lCiaComp As Long, ByVal lId As Long, ByVal sIdioma As String, ByVal PYME As String, ByVal msRutaAdjunFirmaDig As String, _
                            Optional ByVal bEsPujaSubasta As Boolean, Optional ByVal CodPortal As String)

    'ESTE PROCEDIMIENTO TIENE QUE SER IGUAL QUE LA FUNCI�N "TransferirOferta"
    'DEL COMPONENTE FSPServer_2x.COferta. Con la salvedad de que esto es un procedimiento y
    'en caso de error genera un evento en el log.
    
    Dim sConsulta As String
    Dim SQL As String
    Dim adoCom As ADODB.Command
    Dim adoPar As ADODB.Parameter
    Dim adoRes As ADODB.Recordset
    On Error GoTo Error
    Dim sFSGS As String
    Dim lIdGS As Long
    Dim fSQL As scripting.FileSystemObject
    Dim aSQL As scripting.TextStream
    Dim sFile As String
    Dim sProve As String
    Dim lAnyo As Long
    Dim sGmn1 As String
    Dim lProce As Long
    Dim lOfe As Long
    Dim sUsuProve As String
    Dim lCiaProve As Long
    Dim vFecRec As Variant
    Dim sDen As String
    Dim sFileOld As String
    Dim i As Integer
    Dim bCreandoElFichero As Boolean
    
    bCreandoElFichero = True
RepetirFichero_:
    Set fSQL = New FileSystemObject
    sFile = App.Path & "\sql" & Format(Now(), "dd_mm_yyyy_hh_nn_ss")
    sFileOld = sFile
    i = 1
    While fSQL.FileExists(sFile & ".sql")
        sFile = sFileOld & "_" & CStr(i)
        i = i + 1
    Wend
    sFile = sFile & ".sql"
    Set aSQL = fSQL.CreateTextFile(sFile)
    bCreandoElFichero = False
    sConsulta = "EXEC SP_DEVOLVER_OFERTA_PROVE @ID =?"
    'Ejecutamos el store procedure SP_DEVOLVER_OFERTA_PROVE
    Set adoCom = New ADODB.Command
    Set adoPar = adoCom.CreateParameter("ID", adInteger, adParamInput, , lId)
    adoCom.Parameters.Append adoPar
    Set adoCom.ActiveConnection = mAdoCon
    adoCom.CommandType = adCmdText
    adoCom.CommandText = sConsulta
    Set adoRes = adoCom.Execute
    Set adoCom = Nothing
    
    If adoRes.EOF Then
        adoRes.Close
        Set adoRes = Nothing
        Exit Sub
    End If
    
    SQL = ""
    
    Dim sObsAdjun  As Variant
    
    If adoRes.Fields("OBSADJUN").ActualSize > 0 Then
        sObsAdjun = adoRes.Fields("OBSADJUN").GetChunk(adoRes.Fields("OBSADJUN").ActualSize)
    Else
        sObsAdjun = Null
    End If
        
    SQL = SQL _
        & "    BEGIN TRANSACTION " & vbCrLf
        SQL = SQL & "    INSERT INTO P_PROCE_OFE (ANYO, GMN1, PROCE, PROVE, CIA_PROVE, USU, FECREC, FECVAL, MON, CAMBIO, OBS, OBSADJUN, NUMOBJ, ADJUN, PORTAL, NOMUSU,IMPORTE,IMPORTE_BRUTO"
        If Not bEsPujaSubasta Then
            SQL = SQL & ", ADJUN_FIRMA_DIGITAL) " & vbCrLf
        Else
            SQL = SQL & ") " & vbCrLf
        End If
        SQL = SQL & "    VALUES (" & adoRes("ANYO").Value & ",'" & DblQuote(adoRes("GMN1").Value) & "', " & adoRes("PROCE").Value & ",'" & DblQuote(adoRes("PROVE").Value) & "', " & vbCrLf _
        & "            " & adoRes("CIA_PROVE").Value & ",'" & DblQuote(adoRes("USU").Value) & "'," & DateToSQLTimeDate(adoRes("FECREC").Value) & ", " & DateToSQLDate(adoRes("FECVAL").Value) & ",'" & DblQuote(adoRes("MON").Value) & "'," & vbCrLf _
        & "            " & DblToSQLFloat(adoRes("CAMBIO").Value) & "," & StrToSQLNULL(adoRes("OBS").Value) & "," & StrToSQLNULL(sObsAdjun) & "," & DblToSQLFloat(adoRes("NUMOBJ").Value) & "," & DblToSQLFloat(adoRes("ADJUN").Value) & ",1,'" & DblQuote(adoRes("NOMUSU").Value) & "'," & vbCrLf _
        & "            " & DblToSQLFloat(adoRes("IMPORTEOFERTA").Value) & "," & DblToSQLFloat(adoRes("IMPORTEBRUTO").Value)
        
        
        If Not bEsPujaSubasta Then
            'Las pujas de subasta no llevan firma digital
            Dim oRaiz As Object
            Set oRaiz = CreateObject("FSPServer_" & NumeroVersion & ".cRaiz")
            oRaiz.Conectar gsInstancia
            Dim oAdjun As Object
            Set oAdjun = oRaiz.Generar_CAdjunto()
            oAdjun.Nombre = fSQL.GetFileName(msRutaAdjunFirmaDig)
            Call oAdjun.GrabarAdjunto(fSQL.GetParentFolderName(msRutaAdjunFirmaDig) & "\", "", lCiaComp, 26)
                            
            SQL = SQL & ", (SELECT MAX(ID) FROM P_ADJUN_FIRMA_DIGITAL))" & vbCrLf
        Else
            SQL = SQL & ")" & vbCrLf
        End If
        SQL = SQL & "    SET @ID = (SELECT MAX(ID) " & vbCrLf _
        & "                 FROM P_PROCE_OFE)" & vbCrLf _
        & "    COMMIT " & vbCrLf _
        & "    IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
    
    
    sProve = adoRes.Fields("PROVE").Value
    lAnyo = adoRes("ANYO").Value
    sGmn1 = adoRes("GMN1").Value
    lProce = adoRes("PROCE").Value
    lCiaProve = adoRes("CIA_PROVE").Value
    sUsuProve = adoRes("USU").Value
    vFecRec = adoRes("FECREC").Value
    adoRes.Close
    Set adoRes = Nothing
    
    aSQL.WriteLine (SQL)
    SQL = ""
    
    sConsulta = "EXEC SP_DEVOLVER_IMPORTES_PARCIALES_GRUPO @ID =?"
    
    
    Set adoCom = New ADODB.Command
    
    Set adoCom.ActiveConnection = mAdoCon
    
    adoCom.CommandText = sConsulta
    adoCom.CommandType = adCmdText
    
    Set adoPar = adoCom.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lId)
    adoCom.Parameters.Append adoPar
    
    
    Set adoRes = adoCom.Execute
    
    While Not adoRes.EOF
    
        SQL = SQL & "" _
            & "INSERT INTO P_OFE_GR_COSTESDESC (ID, GRUPO,  ATRIB_ID, IMPPARCIAL)" & vbCrLf _
                & "VALUES (@ID,'" & DblQuote(adoRes.Fields("GRUPO").Value) & "'," & adoRes.Fields("COSTEDESC").Value & "," & DblToSQLFloat(adoRes.Fields("IMPPARCIAL").Value) & ")" & vbCrLf _
                & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
    
        aSQL.WriteLine (SQL)
        SQL = ""
    adoRes.MoveNext
    Wend
    adoRes.Close
    Set adoRes = Nothing
    
    sConsulta = "EXEC SP_DEVOLVER_OFE_ATRIB @ID =?, @AMBITO =1"
    
    
    Set adoCom = New ADODB.Command
    
    Set adoCom.ActiveConnection = mAdoCon
    
    adoCom.CommandText = sConsulta
    adoCom.CommandType = adCmdText
    
    Set adoPar = adoCom.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lId)
    adoCom.Parameters.Append adoPar
    
    
    Set adoRes = adoCom.Execute
    While Not adoRes.EOF
    
        SQL = SQL & "" _
            & "INSERT INTO P_OFE_ATRIB (ID, ATRIB_ID, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL,IMPPARCIAL)" & vbCrLf _
                & "VALUES (@ID," & adoRes.Fields("ATRIB_ID") & "," & StrToSQLNULL(adoRes.Fields("VALOR_TEXT").Value) & ", " & vbCrLf _
                & "        " & DblToSQLFloat(adoRes.Fields("VALOR_NUM").Value) & ", " & DateToSQLDate(adoRes.Fields("VALOR_FEC").Value) & ", " & vbCrLf _
                & "        " & BooleanToSQLBinary(adoRes.Fields("VALOR_BOOL").Value) & "," & vbCrLf _
                & "        " & DblToSQLFloat(adoRes.Fields("IMPPARCIAL").Value) & ")" & vbCrLf _
                & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
    
    adoRes.MoveNext
    Wend
    adoRes.Close
    Set adoRes = Nothing
    aSQL.WriteLine (SQL)
    SQL = ""
    
    
    Set adoCom = New ADODB.Command
    Set adoCom.ActiveConnection = mAdoCon
    adoCom.CommandText = "EXEC SP_DEVOLVER_ADJUNTOS @ID = ?, @AMBITO = 1"
    adoCom.CommandType = adCmdText
    
    Set adoPar = adoCom.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lId)
    adoCom.Parameters.Append adoPar
    
    Set adoRes = adoCom.Execute
    
    While Not adoRes.EOF
        
        SQL = SQL & "INSERT INTO P_PROCE_OFE_ADJUN (ID,ADJUN, NOM,COM ,IDPORTAL,IDGS, DATASIZE)" & vbCrLf _
            & "VALUES (@ID," & adoRes.Fields("ADJUN").Value & ",'" _
            & DblQuote(adoRes.Fields("NOM").Value) & "'," _
            & StrToSQLNULL(adoRes.Fields("COM").Value) & "," _
            & DblToSQLFloat(adoRes.Fields("IDPORTAL").Value) & "," _
            & DblToSQLFloat(adoRes.Fields("IDGS").Value) & "," _
            & DblToSQLFloat(adoRes.Fields("DATASIZE").Value) & ")" & vbCrLf _
            & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
    
        adoRes.MoveNext
    Wend
    adoRes.Close
    Set adoRes = Nothing
    aSQL.WriteLine (SQL)
    SQL = ""
    
    
    Set adoCom = New ADODB.Command
    Set adoCom.ActiveConnection = mAdoCon
    adoCom.CommandText = "SELECT ID,GRUPO,OBSADJUN,ADJUN,IMPORTE,IMPORTE_BRUTO FROM GRUPO_OFE WHERE ID = ?"
    adoCom.CommandType = adCmdText
    
    Set adoPar = adoCom.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lId)
    adoCom.Parameters.Append adoPar
    
    Set adoRes = adoCom.Execute
    
    While Not adoRes.EOF
        
        SQL = SQL & "INSERT INTO P_GRUPO_OFE (ID,GRUPO,OBSADJUN,ADJUN,IMPORTE,IMPORTE_BRUTO)" & vbCrLf _
            & "VALUES (@ID,'" & DblQuote(adoRes.Fields("GRUPO").Value) & "'," & vbCrLf _
            & StrToSQLNULL(adoRes.Fields("OBSADJUN").Value) & "," & DblToSQLFloat(adoRes.Fields("ADJUN").Value) & "," & vbCrLf _
            & DblToSQLFloat(adoRes("IMPORTE").Value) & "," & DblToSQLFloat(adoRes("IMPORTE_BRUTO").Value) & ")" & vbCrLf _
            & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
    
        adoRes.MoveNext
    Wend
    adoRes.Close
    Set adoRes = Nothing
    aSQL.WriteLine (SQL)
    SQL = ""
    
    
    
    sConsulta = "EXEC SP_DEVOLVER_OFE_ATRIB @ID =?, @AMBITO =2"
    
    
    Set adoCom = New ADODB.Command
    
    Set adoCom.ActiveConnection = mAdoCon
    
    adoCom.CommandText = sConsulta
    adoCom.CommandType = adCmdText
    
    Set adoPar = adoCom.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lId)
    adoCom.Parameters.Append adoPar
    
    
    Set adoRes = adoCom.Execute
    
    While Not adoRes.EOF
    
        SQL = SQL & "" _
            & "INSERT INTO P_OFE_GR_ATRIB (ID, GRUPO, ATRIB_ID, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL)" & vbCrLf _
                & "VALUES (@ID,'" & DblQuote(adoRes.Fields("GRUPO").Value) & "'," & adoRes.Fields("ATRIB_ID") & "," & StrToSQLNULL(adoRes.Fields("VALOR_TEXT").Value) & ", " & vbCrLf _
                & "        " & DblToSQLFloat(adoRes.Fields("VALOR_NUM").Value) & ", " & DateToSQLDate(adoRes.Fields("VALOR_FEC").Value) & ", " & vbCrLf _
                & "        " & BooleanToSQLBinary(adoRes.Fields("VALOR_BOOL").Value) & ")" & vbCrLf _
                & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
    
    adoRes.MoveNext
    Wend
    adoRes.Close
    Set adoRes = Nothing
    
    
    aSQL.WriteLine (SQL)
    SQL = ""
    
    
    Set adoCom = New ADODB.Command
    Set adoCom.ActiveConnection = mAdoCon
    adoCom.CommandText = "EXEC SP_DEVOLVER_ADJUNTOS @ID = ?, @AMBITO = 2"
    adoCom.CommandType = adCmdText
    
    Set adoPar = adoCom.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lId)
    adoCom.Parameters.Append adoPar
    
    Set adoRes = adoCom.Execute
    
    While Not adoRes.EOF
        
        SQL = SQL & "INSERT INTO P_OFE_GR_ADJUN (ID, GRUPO, ADJUN, NOM,COM ,IDPORTAL,IDGS, DATASIZE)" & vbCrLf _
            & "VALUES (@ID,'" & DblQuote(adoRes.Fields("GRUPO").Value) & "'," _
            & adoRes.Fields("ADJUN").Value & vbCrLf _
            & ",'" & DblQuote(adoRes.Fields("NOM").Value) & "'," _
            & StrToSQLNULL(adoRes.Fields("COM").Value) & "," _
            & DblToSQLFloat(adoRes.Fields("IDPORTAL").Value) & "," _
            & DblToSQLFloat(adoRes.Fields("IDGS").Value) & "," _
            & DblToSQLFloat(adoRes.Fields("DATASIZE").Value) & ")" & vbCrLf _
            & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
    
        adoRes.MoveNext
    Wend
    adoRes.Close
    Set adoRes = Nothing
    
    aSQL.WriteLine (SQL)
    SQL = ""
    
    
    Set adoCom = New ADODB.Command
    Set adoCom.ActiveConnection = mAdoCon
    adoCom.CommandText = "SELECT ID, ITEM, GRUPO, PRECIO, CANTMAX, PRECIO2, PRECIO3, COMENT1, COMENT2, COMENT3, OBSADJUN, ADJUN,IMPORTE,PREC_VALIDO FROM ITEM_OFE WHERE ID = ?"
    adoCom.CommandType = adCmdText
    
    Set adoPar = adoCom.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lId)
    adoCom.Parameters.Append adoPar
    
    Set adoRes = adoCom.Execute
    
    While Not adoRes.EOF
        
        SQL = SQL & "INSERT INTO P_ITEM_OFE (ID, ITEM, GRUPO, PRECIO, CANTMAX, PRECIO2, PRECIO3, COMENT1, COMENT2, COMENT3, OBSADJUN, ADJUN,IMPORTE,PREC_VALIDO)" & vbCrLf _
            & "VALUES (@ID," & adoRes.Fields("ITEM").Value & ", " _
            & "'" & DblQuote(adoRes.Fields("GRUPO").Value) & "'," _
            & DblToSQLFloat(adoRes.Fields("PRECIO").Value) & "," _
            & DblToSQLFloat(adoRes.Fields("CANTMAX").Value) & "," _
            & DblToSQLFloat(adoRes.Fields("PRECIO2").Value) & "," _
            & DblToSQLFloat(adoRes.Fields("PRECIO3").Value) & "," _
            & StrToSQLNULL(adoRes.Fields("COMENT1").Value) & "," _
            & StrToSQLNULL(adoRes.Fields("COMENT2").Value) & "," _
            & StrToSQLNULL(adoRes.Fields("COMENT3").Value) & "," _
            & StrToSQLNULL(adoRes.Fields("OBSADJUN").Value) & ", " _
            & DblToSQLFloat(adoRes.Fields("ADJUN").Value) & "," _
            & DblToSQLFloat(adoRes("IMPORTE").Value) & "," _
            & DblToSQLFloat(adoRes("PREC_VALIDO").Value) & ")" & vbCrLf _
            & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
        
        aSQL.WriteLine (SQL)
        SQL = ""
        adoRes.MoveNext
    Wend
    adoRes.Close
    Set adoRes = Nothing
    
    ' Precios Escalados
    Set adoCom = New ADODB.Command
    Set adoCom.ActiveConnection = mAdoCon
    adoCom.CommandText = "SELECT ID, ITEM, ESC, PRECIO, PREC_VALIDO FROM ITEM_OFEESC with (nolock)  WHERE ID = ?"
    adoCom.CommandType = adCmdText
    Set adoPar = adoCom.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lId)
    adoCom.Parameters.Append adoPar
    Set adoRes = adoCom.Execute
    While Not adoRes.EOF
        SQL = SQL & "INSERT INTO P_ITEM_OFEESC (ID, ITEM, ESC, PRECIO, PREC_VALIDO)" & vbCrLf _
            & "VALUES (@ID," & adoRes.Fields("ITEM").Value & ", " _
            & adoRes.Fields("ESC").Value & ", " _
            & DblToSQLFloat(adoRes.Fields("PRECIO").Value) & "," _
            & DblToSQLFloat(adoRes.Fields("PREC_VALIDO").Value) & ")" & vbCrLf _
            & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
    
        aSQL.WriteLine (SQL)
        SQL = ""
        adoRes.MoveNext
    Wend
    adoRes.Close
    Set adoRes = Nothing
    ' Fin Precios Escalados
    
    
    sConsulta = "EXEC SP_DEVOLVER_OFE_ATRIB @ID =?, @AMBITO =3"
    
    
    Set adoCom = New ADODB.Command
    
    Set adoCom.ActiveConnection = mAdoCon
    
    adoCom.CommandText = sConsulta
    adoCom.CommandType = adCmdText
    
    Set adoPar = adoCom.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lId)
    adoCom.Parameters.Append adoPar
    
    
    Set adoRes = adoCom.Execute
    
    While Not adoRes.EOF
    
        SQL = SQL & "" _
            & "INSERT INTO P_OFE_ITEM_ATRIB (ID, ITEM, ATRIB_ID, GRUPO,  VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL)" & vbCrLf _
                & "VALUES (@ID," & adoRes.Fields("ITEM").Value & "," & adoRes.Fields("ATRIB_ID") & ",'" & DblQuote(adoRes.Fields("GRUPO").Value) & "'," & StrToSQLNULL(adoRes.Fields("VALOR_TEXT").Value) & ", " & vbCrLf _
                & "        " & DblToSQLFloat(adoRes.Fields("VALOR_NUM").Value) & ", " & DateToSQLDate(adoRes.Fields("VALOR_FEC").Value) & ", " & vbCrLf _
                & "        " & BooleanToSQLBinary(adoRes.Fields("VALOR_BOOL").Value) & ")" & vbCrLf _
                & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
    
        aSQL.WriteLine (SQL)
        SQL = ""
    adoRes.MoveNext
    Wend
    adoRes.Close
    Set adoRes = Nothing
    
    
    'Costes-Descuentos Escalados
    
    
    Set adoCom = New ADODB.Command
    Set adoCom.ActiveConnection = mAdoCon
    adoCom.CommandText = "SELECT ID, ITEM, ATRIB_ID, ESC, VALOR_NUM FROM OFE_ITEM_ATRIBESC with (nolock)  WHERE ID = ?"
    adoCom.CommandType = adCmdText
    Set adoPar = adoCom.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lId)
    adoCom.Parameters.Append adoPar
    Set adoRes = adoCom.Execute
    While Not adoRes.EOF
    
        SQL = SQL & "INSERT INTO P_OFE_ITEM_ATRIBESC (ID, ITEM, ATRIB_ID, ESC, VALOR_NUM)" & vbCrLf _
            & "VALUES (@ID," & adoRes.Fields("ITEM").Value & ", " _
            & adoRes.Fields("ATRIB_ID").Value & ", " _
            & adoRes.Fields("ESC").Value & "," _
            & DblToSQLFloat(adoRes.Fields("VALOR_NUM").Value) & ")" & vbCrLf _
            & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
    
        aSQL.WriteLine (SQL)
        SQL = ""
        adoRes.MoveNext
    Wend
    adoRes.Close
    Set adoRes = Nothing
    
    
    
    ' Fin Costes-Descuentos Escalados
    
    
    
    
    
    
    Set adoCom = New ADODB.Command
    Set adoCom.ActiveConnection = mAdoCon
    adoCom.CommandText = "EXEC SP_DEVOLVER_ADJUNTOS @ID = ?, @AMBITO = 3"
    adoCom.CommandType = adCmdText
    
    Set adoPar = adoCom.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lId)
    adoCom.Parameters.Append adoPar
    
    Set adoRes = adoCom.Execute
    
    While Not adoRes.EOF
        
        SQL = SQL & "INSERT INTO P_OFE_ITEM_ADJUN (ID, ITEM, ADJUN, NOM,COM ,IDPORTAL,IDGS, DATASIZE)" & vbCrLf _
            & "VALUES (@ID,'" & DblQuote(adoRes.Fields("ITEM").Value) & "'," _
            & adoRes.Fields("ADJUN").Value _
            & ",'" & DblQuote(adoRes.Fields("NOM").Value) & "'," _
            & StrToSQLNULL(adoRes.Fields("COM").Value) & "," _
            & DblToSQLFloat(adoRes.Fields("IDPORTAL").Value) & "," _
            & DblToSQLFloat(adoRes.Fields("IDGS").Value) & "," _
            & DblToSQLFloat(adoRes.Fields("DATASIZE").Value) & ")" & vbCrLf _
            & "IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
    
        adoRes.MoveNext
    Wend
    adoRes.Close
    Set adoRes = Nothing
    aSQL.WriteLine (SQL)
    SQL = ""
    
    
    SQL = SQL _
        & "ERROR_:"
    
    aSQL.WriteLine (SQL)
    SQL = ""
    aSQL.Close
    
    sFSGS = FSGS(mAdoCon, lCiaComp)
    
    Set adoCom = New ADODB.Command
    
    Set adoCom.ActiveConnection = mAdoCon
    
    Set aSQL = fSQL.OpenTextFile(sFile, ForReading)
    SQL = aSQL.ReadAll
    aSQL.Close
    Set aSQL = Nothing
    Set adoPar = adoCom.CreateParameter("stmt", adLongVarWChar, adParamInput, Len(SQL))
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("ID", adInteger, adParamOutput)
    adoCom.Parameters.Append adoPar
    adoCom.Parameters("stmt").AppendChunk SQL
    sConsulta = "EXEC " & sFSGS & "sp_executesql @stmt = ?, @params=N'"
    sConsulta = sConsulta & "@ID INT OUTPUT',"
    sConsulta = sConsulta & "@ID = ? OUTPUT"
    adoCom.CommandText = sConsulta
    adoCom.CommandType = adCmdText
    adoCom.CommandTimeout = 120
    adoCom.Execute
    
    lIdGS = adoCom.Parameters("ID").Value
    If mAdoCon.Errors.Count > 0 Then
        GoTo Error
    End If
    
    Set adoCom = New ADODB.Command
    
    Set adoCom.ActiveConnection = mAdoCon
    
    
    Set adoPar = adoCom.CreateParameter("ID", adInteger, adParamInput, , lIdGS)
    adoCom.Parameters.Append adoPar
    
    Set adoPar = adoCom.CreateParameter("NUM", adInteger, adParamOutput)
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("NUMNUE", adInteger, adParamOutput)
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("NUMAREV", adInteger, adParamOutput)
    adoCom.Parameters.Append adoPar
    Set adoPar = adoCom.CreateParameter("ERROR", adSmallInt, adParamOutput)
    adoCom.Parameters.Append adoPar
    
    Dim iContador As Integer
    iContador = 0
    
    sConsulta = "EXEC " & sFSGS & "SP_TRASPASAR_OFERTA @ID = ?, @NUM=? OUTPUT, @NUMNUE=? OUTPUT, @NUMAREV = ? OUTPUT, @ERROR = ? OUTPUT"
    adoCom.CommandText = sConsulta
    adoCom.CommandType = adCmdText
    adoCom.CommandTimeout = 120
    Dim bErrorAlTraspasar As Boolean
    
Repite_:
    adoCom.Execute
    
    
    If mAdoCon.Errors.Count > 0 Then
        GoTo Error
    End If
    
    Dim lNum As Long
    Dim lNumNue As Long
    Dim lNumARev As Long
    Dim iError As Integer
    
    lNum = adoCom.Parameters("NUM").Value
    lNumNue = adoCom.Parameters("NUMNUE").Value
    lNumARev = adoCom.Parameters("NUMAREV").Value
    iError = adoCom.Parameters("ERROR").Value
    
    
    If iError = 0 Then
        Set adoCom = New ADODB.Command
        
        Set adoCom.ActiveConnection = mAdoCon
        
        
        Set adoPar = adoCom.CreateParameter("CIACOMP", adInteger, adParamInput, , lCiaComp)
        adoCom.Parameters.Append adoPar
        Set adoPar = adoCom.CreateParameter("CIAPROVE", adVarChar, adParamInput, 50, sProve)
        adoCom.Parameters.Append adoPar
        
        Set adoPar = adoCom.CreateParameter("NUM", adInteger, adParamInput, , lNum)
        adoCom.Parameters.Append adoPar
        Set adoPar = adoCom.CreateParameter("NUMNUE", adInteger, adParamInput, , lNumNue)
        adoCom.Parameters.Append adoPar
        Set adoPar = adoCom.CreateParameter("NUMAREV", adInteger, adParamInput, , lNumARev)
        adoCom.Parameters.Append adoPar
        
        sConsulta = "EXEC SP_ACT_PROCE_PUB @CIA_COMP =?, @CIA_PROVE =?, @NUM_PUB =?, @NUM_PUB_SINOFE =?,@NUM_PUB_AREV =?"
        adoCom.CommandText = sConsulta
        adoCom.CommandType = adCmdText
        adoCom.Execute
        
        
        If mAdoCon.Errors.Count > 0 Then
            GoTo Error
        End If
        
        
        Set adoCom = New ADODB.Command
        
        Set adoCom.ActiveConnection = mAdoCon
        
        
        Set adoPar = adoCom.CreateParameter("ID", adInteger, adParamInput, , lId)
        adoCom.Parameters.Append adoPar
        
        sConsulta = "EXEC SP_ELIMINAR_OFERTA_PROVE @ID = ?"
        adoCom.CommandText = sConsulta
        adoCom.CommandType = adCmdText
        adoCom.Execute
        
        
        
        sConsulta = "EXEC " & sFSGS & "SP_DEVOLVER_OFERTA_PROVE @ANYO =?,@GMN1 =?,@PROCE =?,@PROVE =?,@IDIOMA =?"
        'Ejecutamos el store procedure SP_DEVOLVER_OFERTA_PROVE
        Set adoCom = New ADODB.Command
        Set adoPar = adoCom.CreateParameter("ANYO", adInteger, adParamInput, , lAnyo)
        adoCom.Parameters.Append adoPar
        Set adoPar = adoCom.CreateParameter("GMN1", adVarChar, adParamInput, 20, DblQuote(sGmn1))
        adoCom.Parameters.Append adoPar
        Set adoPar = adoCom.CreateParameter("PROCE", adInteger, adParamInput, , lProce)
        adoCom.Parameters.Append adoPar
        Set adoPar = adoCom.CreateParameter("PROVE", adVarChar, adParamInput, 50, sProve)
        adoCom.Parameters.Append adoPar
        Set adoPar = adoCom.CreateParameter("IDIOMA", adVarChar, adParamInput, 50, sIdioma)
        adoCom.Parameters.Append adoPar
        
        Set adoCom.ActiveConnection = mAdoCon
        adoCom.CommandType = adCmdText
        adoCom.CommandText = sConsulta
        
        
        Set adoRes = adoCom.Execute
        Set adoCom = Nothing
        
        lOfe = adoRes("OFE").Value
        adoRes.Close
    
        Set adoRes = Nothing
        
        
        
        TransferirAdjuntosOferta lCiaComp, lAnyo, sGmn1, lProce, sProve, lOfe
        
        
        Set adoCom = New ADODB.Command
        
        Set adoPar = adoCom.CreateParameter("ANYO", adInteger, adParamInput, , lAnyo)
        adoCom.Parameters.Append adoPar
        Set adoPar = adoCom.CreateParameter("GMN1", adVarChar, adParamInput, 50, sGmn1)
        adoCom.Parameters.Append adoPar
        Set adoPar = adoCom.CreateParameter("PROCE", adInteger, adParamInput, , lProce)
        adoCom.Parameters.Append adoPar
        Set adoPar = adoCom.CreateParameter("ACTIVA", adSmallInt, adParamOutput)
        adoCom.Parameters.Append adoPar
        Set adoPar = adoCom.CreateParameter("IDIOMA", adVarChar, adParamInput, 50, sIdioma)
        adoCom.Parameters.Append adoPar
        
        Set adoCom.ActiveConnection = mAdoCon
        adoCom.CommandType = adCmdText
        
        adoCom.CommandText = "EXEC " & sFSGS & "SP_DEVOLVER_PROCESO @ANYO =? ,@GMN1 =?,@PROCE =?,@ACTIVA =? OUTPUT,@IDIOMA =?"
        
        
        Set adoRes = adoCom.Execute
        sDen = adoRes("DEN").Value
        
        adoRes.Close
        
        'Incidencia: Pruebas3.0 2006    638
        'En Gestamp falla el notificador por un motivo exteno a nosotros, pero los proveedores cuando env�an una
        'oferta les sale que se guard� pero no se pudo enviar y se les queda como sin enviar anque en realidad la
        'oferta lleg� al GS correctamente.  Aunque falle el notificador, si se ha enviado la oferta deber�a quedar
        'como enviada.
        
        On Error GoTo ErrorNotificar
        
        Dim o As Object
        
        #If DEB Then
            Set o = CreateObject("FSPNotificar_" & NumeroVersion & ".CFSPNotificar")
        #Else
            Set o = CreateObject("FSPNotificar_" & NumeroVersion & ".CFSPNotificar")
            Set o = Nothing
            Set o = GetObject("queue:/new:FSPNotificar_" & NumeroVersion & ".CFSPNotificar")
        #End If
        
        o.Conectar ""
        
        o.Conectar gsInstancia
        
        o.EnviarEmailProveConfirmacionOferta lCiaComp, lCiaProve, sUsuProve, lAnyo, sGmn1, lProce, sProve, lOfe, vFecRec, sDen, PYME, CodPortal
        
        o.EnviarEmailCompResponsableyCompAsignado lCiaComp, lCiaProve, lAnyo, sGmn1, lProce, sDen, lOfe, vFecRec, sProve, sUsuProve, CodPortal
            
        Set o = Nothing
        
        
        
        If mAdoCon.Errors.Count > 0 Then
            GoTo ErrorNotificar
        End If
    Else
        
        Set adoCom = New ADODB.Command
        
        Set adoCom.ActiveConnection = mAdoCon
        
        
        Set adoPar = adoCom.CreateParameter("ERROR", adSmallInt, adParamInput, , iError)
        adoCom.Parameters.Append adoPar
        Set adoPar = adoCom.CreateParameter("ID", adInteger, adParamInput, , lId)
        adoCom.Parameters.Append adoPar
        
        sConsulta = "UPDATE PROCE_OFE SET ERROR=? WHERE ID = ?"
        adoCom.CommandText = sConsulta
        adoCom.CommandType = adCmdText
        adoCom.Execute
        
    End If
    
    
fin:
    On Error Resume Next
    fSQL.DeleteFile sFile, True
    adoRes.Close
    Set adoRes = Nothing
    
    Exit Sub
    
Error:
    Dim bGoto As Boolean
    Dim sDatosProceso  As String
    
    If Err.Number = 0 Then
        bGoto = True
    Else
        bGoto = False
    End If
    If bCreandoElFichero Then
        If bGoto Then
            GoTo RepetirFichero_
        Else
            Resume RepetirFichero_
        End If
    End If
    
    Dim sLog As String
    
    sLog = gsInstancia & ": N�mero error: " & Err.Number & ", descripci�n: " & Err.Description & vbCrLf
    
    Dim dhora As Date
    Dim numSeg As Integer
    
    If mAdoCon.Errors.Count > 0 Then
        If mAdoCon.Errors(0).NativeError = 1205 And iContador <= 20 Then
            dhora = Now
            numSeg = CInt(Rnd() * 10)
            numSeg = numSeg + 1
            dhora = DateAdd("s", numSeg, dhora)
            
            While dhora > Now
            
            Wend
       
            iContador = iContador + 1
            App.LogEvent sLog & " Intento n�mero:" & iContador
            If Err.Number = 0 Then
                GoTo Repite_
            Else
                Resume 0
            End If
            
        End If
        
        
        
        'Set TESError = basErrores.TratarError(mAdoCon.Errors)
        Dim adoErr As ADODB.Error
        For Each adoErr In mAdoCon.Errors
            sLog = sLog & "AdoError: " & adoErr.NativeError & ", Descripcion: " & adoErr.Description & vbCrLf
        Next
    Else
        'TESError.NumError = TESOtroErrorNOBD
    End If
    
    App.LogEvent sLog
    
    If bGoto Then
        GoTo fin
    Else
        Resume fin
    End If
    
    
    
ErrorNotificar:
    
    Dim bGotoN As Boolean
    If Err.Number = 0 Then
        bGotoN = True
    Else
        bGotoN = False
    End If
    
    Dim sLogN As String
    
    sLogN = gsInstancia & ": Envio as�ncrono de oferta error en la notificaci�n: " & Err.Number & ", descripci�n: " & Err.Description & vbCrLf
    
    'Incidencia: Pruebas3.0 2006    638
    'En Gestamp falla el notificador por un motivo exteno a nosotros, pero los proveedores cuando env�an una
    'oferta les sale que se guard� pero no se pudo enviar y se les queda como sin enviar anque en realidad la
    'oferta lleg� al GS correctamente.  Aunque falle el notificador, si se ha enviado la oferta deber�a quedar
    'como enviada.
    '----------------
    'TESError.NumError = TESMail <-- Esto viene de copiar lo que he hecho para sincrono
    'Lo comento por lo que veo en Error: de esta misma funcion
    '----------------
    'Con Mertxe al lado y viendo el script/solicitudesoferta/confirmoferta.asp
    'se decidio que el problema estaba pq entraba en la if del siguiente codigo
    '   sePudoEnviar = oOferta.ErrorEnvio
    '   if oOferta.errorEnvio = 0 and TESError.NumError<>0 then
    '        sePudoEnviar = TESError.NumError
    '   End If
    'Es decir, en que sePudoEnviar no era 0 (oOferta.ErrorEnvio es cero si lo ha hecho todo bien hasta llegar al
    'notificador) sino que era TESOtroErrorNOBD.
    
    App.LogEvent sLogN
    
    If bGotoN Then
        GoTo fin
    Else
        Resume fin
    End If
End Sub

Private Sub Class_Terminate()
On Error Resume Next
mAdoCon.Close
Set mAdoCon = Nothing

End Sub


''' <summary>
''' Extrae la contrase�a del fichero FSPSDB.LIC y la desencripta.
''' </summary>
''' <returns>La contrase�a desencriptada</returns>
''' <remarks>Llamada desde: frmPrincipal.Form_Load ; Tiempo m�ximo: 0,1</remarks>
Private Function LICDBContra() As String
    Dim fso As New FileSystemObject, fil As File, ts As TextStream
    Dim FechaHora As String
    Dim tmplogin As String
    Dim tmpcontra As String

    Set fil = fso.GetFile(App.Path & "\FSDB.LIC")
    Set ts = fil.OpenAsTextStream(ForReading)
    
    FechaHora = Format(ts.ReadLine, "DD\/MM\/YYYY HH\:NN\:SS")
    tmplogin = ts.ReadLine
    tmpcontra = ts.ReadLine
    ts.Close
        
    ''' Desencriptar contrase�a
    LICDBContra = DesEncriptar(tmpcontra, FechaHora)
End Function

''' <summary>
''' Extrae el login del fichero FSPSDB.LIC y lo desencripta.
''' </summary>
''' <returns>La contrase�a desencriptada</returns>
''' <remarks>Llamada desde: frmPrincipal.Form_Load ; Tiempo m�ximo: 0,1</remarks>
Private Function LICDBLogin() As String
    Dim fso As New FileSystemObject, fil As File, ts As TextStream
    Dim FechaHora As String
    Dim tmplogin As String
    Dim tmpcontra As String

    Set fil = fso.GetFile(App.Path & "\FSDB.LIC")
    Set ts = fil.OpenAsTextStream(ForReading)
    
    FechaHora = Format(ts.ReadLine, "DD\/MM\/YYYY HH\:NN\:SS")
    tmplogin = ts.ReadLine
    tmpcontra = ts.ReadLine
    ts.Close
        
    ''' Desencriptar login
    LICDBLogin = DesEncriptar(tmplogin, FechaHora)
End Function

''' <summary>
''' Funci�n que desencripta un string que se le pasa como par�metro.
''' </summary>
''' <param name="Dato">Dato a desencriptar.</param>
''' <param name="Fecha">Fecha para la desencriptaci�n</param>
''' <returns>El string desencriptado.</returns>
''' <remarks>Llamada desde: CTransfer.LICDBContra, CTransfer.LICDBLogin ; Tiempo m�ximo: 0,1</remarks>
Private Function DesEncriptar(ByVal Dato As String, Optional ByVal Fecha As Variant) As String
    Dim fechahoracrypt As Date
    Dim oCrypt2 As CCrypt2
    
    If IsMissing(Fecha) Then
        fechahoracrypt = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            fechahoracrypt = DateSerial(1974, 3, 5)
        Else
            fechahoracrypt = Fecha
        End If
    End If

    Set oCrypt2 = New CCrypt2
    
    oCrypt2.InBuffer = Dato
    oCrypt2.Codigo = ClaveSQLtod
    oCrypt2.Fecha = fechahoracrypt
                
    oCrypt2.Decrypt
    DesEncriptar = oCrypt2.OutBuffer
            
    Set oCrypt2 = Nothing
        
End Function

