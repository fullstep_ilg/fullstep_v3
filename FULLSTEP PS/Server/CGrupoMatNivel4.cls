VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGrupoMatNivel4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CGrupoMatNivel3 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarIndice As Long 'Nos indica la posicion secuencial dentro de una Coleccion
Private mvarCod As String
Private mvarDen As String
Private mvarGMN1Cod As String
Private mvarGMN2Cod As String
Private mvarGMN3Cod As String
Private mvarGMN1Den As String
Private mvarGMN2Den As String
Private mvarGMN3Den As String
Private mvarConexion As CConexion 'local copy



Public Property Get Indice() As Long
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal Ind As Long)
    mvarIndice = Ind
End Property


Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property



Public Property Let Den(ByVal vData As String)
    mvarDen = vData
End Property


Public Property Get Den() As String
    Den = mvarDen
End Property



Public Property Let Cod(ByVal vData As String)
    mvarCod = vData
End Property


Public Property Get Cod() As String
    Cod = mvarCod
End Property
Public Property Let GMN1Cod(ByVal vData As String)
    mvarGMN1Cod = vData
End Property


Public Property Get GMN1Cod() As String
    GMN1Cod = mvarGMN1Cod
End Property
Public Property Let GMN2Cod(ByVal vData As String)
    mvarGMN2Cod = vData
End Property


Public Property Get GMN2Cod() As String
    GMN2Cod = mvarGMN2Cod
End Property

Public Property Let GMN3Cod(ByVal vData As String)
    mvarGMN3Cod = vData
End Property


Public Property Get GMN3Cod() As String
    GMN3Cod = mvarGMN3Cod
End Property
Public Property Let GMN3Den(ByVal vData As String)
    mvarGMN3Den = vData
End Property


Public Property Get GMN3Den() As String
    GMN3Den = mvarGMN3Den
End Property
Public Property Let GMN2Den(ByVal vData As String)
    mvarGMN2Den = vData
End Property


Public Property Get GMN2Den() As String
    GMN2Den = mvarGMN2Den
End Property
Public Property Let GMN1Den(ByVal vData As String)
    mvarGMN1Den = vData
End Property


Public Property Get GMN1Den() As String
    GMN1Den = mvarGMN1Den
End Property

