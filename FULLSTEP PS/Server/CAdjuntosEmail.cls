VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAdjuntosEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAdjuntosEmail **********************************
'*             Autor :  Mertxe Martin
'*             Creada : 02/03/04
'****************************************************************

Option Explicit

Private m_oConexion As CConexion
Private mCol As Collection

Public Property Get Count() As Long

    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If

End Property

Public Property Get Item(vntIndexKey As Variant) As CAdjuntoEmail
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Function Add(ByVal oRegistro As CRegistroEmail, ByVal iID As Integer, ByVal sNombre As String, Optional ByVal dtFecha As Date, Optional ByVal vIndice As Variant, Optional ByVal lSize As Long) As CAdjuntoEmail
    'create a new object
Dim sCod As String
Dim objnewmember As CAdjuntoEmail
    
    Set objnewmember = New CAdjuntoEmail
   
    Set objnewmember.RegistroEmail = oRegistro
    objnewmember.ID = iID
    objnewmember.Nombre = sNombre
    objnewmember.Fecha = dtFecha
    
    objnewmember.DataSize = lSize
    
    Set objnewmember.Conexion = m_oConexion
    
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        'set the properties passed into the method
       mCol.Add objnewmember, CStr(iID)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub



