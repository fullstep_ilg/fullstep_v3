Attribute VB_Name = "basErrores"
Option Explicit

Public Function TratarErrorSPTS(ByVal oErs As ADODB.Recordset) As CTESError
    
    Dim oError As CTESError
    Set oError = New CTESError
    
        oError.NumError = TESErrorSharePoint
        oError.Arg1 = "Error Actualizar SharePoint Team Services"
        If Not (oErs.Fields.Count = 0) Then
            oError.Arg2 = oErs.Fields("DescrError")
        End If
    Set TratarErrorSPTS = oError
End Function
Public Function TratarError(ByVal adoErs As ADODB.Errors) As CTESError

Dim oError As CTESError
 
''' Rutina general de respuesta a errores.

Dim ID As Long
Dim Den As String
Dim ItemError1 As String, ItemError2 As String
Dim pos1 As Integer, pos2 As Integer
Dim bConDBO As Boolean

If adoErs.Count <> 0 Then
    ID = adoErs(0).NativeError
    Den = adoErs(0).Description
End If

Set oError = New CTESError

Select Case ID

Case 0

    oError.NumError = TESInfModificada
    oError.Arg1 = ""
    oError.Arg2 = ""
    
Case 515

    Beep
    pos1 = InStr(1, Den, "'")
    pos2 = InStr(pos1 + 1, Den, "'")
    ItemError1 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
    oError.NumError = TESFaltanDatos
    oError.Arg1 = FieldName(ItemError1)
   
Case 547
    bConDBO = True
    
    pos1 = InStr(1, Den, "table " & """" & "dbo.") + 6
    If pos1 = 6 Then
        bConDBO = False
        pos1 = InStr(1, Den, "table") + 6
    End If
    
    pos2 = InStr(pos1 + 1, Den, "'")
    
    If pos2 = 0 Or bConDBO Then
        pos1 = pos1 + 4
        pos2 = InStr(pos1 + 1, Den, """")
    End If
    
    ItemError1 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
    pos1 = InStr(1, Den, "column") + 7
    pos2 = InStr(pos1 + 1, Den, "'")
    If pos2 = 0 Then
        pos1 = pos1 + 4
        pos2 = InStr(pos1 + 1, Den, """")
    End If
    ItemError2 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
    
    
    If Mid(Den, 55, 3) = "INS" Or Mid(Den, 55, 3) = "UPD" Or Left(Den, 3) = "INS" Or Left(Den, 3) = "UPD" Then
        oError.NumError = TESDatoEliminado
        oError.Arg1 = FieldName(ItemError1)
        oError.Arg2 = "Eliminado"
        
    Else
        oError.NumError = TESImposibleEliminar
        oError.Arg1 = TableName(ItemError1)
        oError.Arg2 = "DatRel"
    End If
    
'Case 911
'    oError.NumError = TESConexionRemotoFailed
'    oError.Arg1 = Mid(Den, InStr(Den, "'"), InStr(Den, ".") - InStr(Den, "'"))
Case 2601
    'Cannot insert duplicate key row in object '%.*ls' with unique index '%.*ls'.
    If InStr(1, Den, "PRIMARY") <> 0 Then
        
        oError.NumError = TESDatoDuplicado
        oError.Arg1 = "Codigo"
        
        If InStr(1, Den, "PK_USU_COM") Then
            oError.NumError = TESDatoDuplicado
            oError.Arg1 = "Comprador"
        End If
        
    
    Else
        
        pos1 = InStr(1, Den, "IX_") + 2
        pos2 = InStr(pos1 + 1, Den, "'")
        If pos2 = 0 Then
            pos1 = pos1 + 4
            pos2 = InStr(pos1 + 1, Den, """")
        End If
        ItemError1 = Mid(Den, pos1 + 1, pos2 - pos1 - 4)
        oError.NumError = TESDatoDuplicado
        oError.Arg1 = FieldName(ItemError1)
    
    End If
    
'Case 2601
'        oError.NumError = TESDatoDuplicado
'        oError.Arg1 = "C�digo"
        
Case 2627
    
    If InStr(1, Den, "PRIMARY") <> 0 Then
        
        oError.NumError = TESDatoDuplicado
        oError.Arg1 = "Codigo"
        
        If InStr(1, Den, "PK_USU_COM") Then
            oError.NumError = TESDatoDuplicado
            oError.Arg1 = "Comprador"
        End If
    Else
        pos1 = InStr(1, Den, "UC_") + 2
        pos2 = InStr(pos1 + 1, Den, "'")
        ItemError1 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
        oError.NumError = TESDatoDuplicado
        oError.Arg1 = FieldName(ItemError1)
    
    End If
    
Case 3621
        'The statement has been terminated.
        bConDBO = True
        
        pos1 = InStr(1, Den, "table " & """" & "dbo.") + 6
        If pos1 = 6 Then
            bConDBO = False
            pos1 = InStr(1, Den, "table") + 6
        End If
        
        pos2 = InStr(pos1 + 1, Den, "'")
        
        If pos2 = 0 Or bConDBO Then
            pos1 = pos1 + 4
            pos2 = InStr(pos1 + 1, Den, """")
        End If
        
        ItemError1 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
        pos1 = InStr(1, Den, "column") + 7
        pos2 = InStr(pos1 + 1, Den, "'")
        If pos2 = 0 Then
            pos1 = pos1 + 4
            pos2 = InStr(pos1 + 1, Den, """")
        End If
        ItemError2 = Mid(Den, pos1 + 1, pos2 - pos1 - 1)
        
        If Mid(Den, 55, 3) = "INS" Then
            MsgBox "No se puede insertar. El dato """ & FieldName(ItemError1) & """ ha sido eliminado.", vbExclamation, "FULLSTEP PS"
        Else
            MsgBox "No se puede eliminar o modificar." & Chr(13) & "Existen datos relacionados en: " & TableName(ItemError1), vbExclamation, "FULLSTEP PS"
        End If
Case 8525
    oError.NumError = TESPaisYaRelacionadoEnGS
    oError.Arg1 = adoErs(1).Description
    
Case 50000
    
    If Mid(Den, 1, 5) = "37000" Then
        If InStr(1, Den, "Incorrect password") <> 0 Then
            oError.NumError = TESCambioContrasenya
        End If
    Else
        oError.NumError = TESImposibleEliminar
        
        pos1 = 55
        If InStr(1, Den, "dbo.") > 0 Then
            pos1 = pos1 + 4
        End If
        
        If Len(Den) > 55 Then
            oError.Arg1 = TableName(Mid(Den, pos1, Len(Den) - pos1 + 1))
        End If
    End If
Case 50009
    
    oError.NumError = TESProveYaRelacionadoEnGS
    oError.Arg1 = Den
    
Case Else
    
    oError.NumError = TESOtroerror
    oError.Arg1 = ID
    oError.Arg2 = Den
    
End Select


Set TratarError = oError
    
End Function
'***************** DICCIONARIO DE DATOS ***************
 Function TableName(TableCode As String) As String

    Select Case TableCode
      
    ''' Mantenimiento
    Case "MON"
        TableName = "Monedas"
    Case "PAI"
        TableName = "Pa�ses"
    Case "PROVI"
        TableName = "Provincias"
        TableName = "Personas de la organizaci�n"
    Case "CIAS"
        TableName = "Compa��as"
    Case "ACT1", "ACT2", "ACT3", "ACT4", "ACT5"
        TableName = "Estructura de Actividades"
    Case "CIAS_ACT1", "CIAS_ACT2", "CIAS_ACT3", "CIAS_ACT4", "CIAS_ACT5"
        TableName = "Estructura de Actividades de Compa��as"
    Case "CIAS_PORT"
        TableName = "Compa��as proveedoras"
    Case "USU"
        TableName = "Usuarios"
    Case "USU_PORT"
        TableName = "Usuarios proveedores"
    Case "CIAS_COM"
        TableName = "Comunicaciones con compa��as"
    Case "COM"
        TableName = "Tipos de comunicacion"
    Case "IDI"
        TableName = "Idiomas"
    Case "PORT"
        TableName = "Portales"
    Case "REL_CIAS"
        TableName = "Relacciones"
    Case "CIAS_MON"
        TableName = "Relaci�n con moneda central de compa��a compradora"
    Case "NOCONFORMIDAD"
        TableName = "No conformidades"
    Case "PM_ROL"
        TableName = "Roles en solicitudes de PM"
    Case Else
        TableName = "Tabla desconocida"
        
    End Select
        
    
End Function
Function FieldName(FieldCode As String)

    Select Case FieldCode
    
    ''' Mantenimiento
    
    Case "ID"
        FieldName = "Identificador"
    Case "COD"
        FieldName = "Codigo"
    Case "DEN"
        FieldName = "Denominacion"
    Case "CAMBIO"
        FieldName = "Cambio de moneda"
    Case "MON"
        FieldName = "Moneda"
    Case "PAI"
        FieldName = "Pais"
    Case "CIAS"
        FieldName = "Compa��a"
    Case "USU"
        FieldName = "Usuario"
    Case "DIR"
        FieldName = "Direccion"
    Case "CP"
        FieldName = "Codigo postal"
    Case "POB"
        FieldName = "Poblacion"
    Case "PROVI"
        FieldName = "Provincia"
    Case "USU"
        FieldName = "Usuario"
    Case "OBS"
        FieldName = "Observaciones"
    Case "PROVE"
        FieldName = "Proveedor"
    Case "TIPOCOM"
        FieldName = "Tipo de comunicaci�n"
    Case "FECHA"
        FieldName = "Fecha/Hora"
    Case "FCEST"
        FieldName = "Estado de la Funci�n compradora"
    Case "FPEST"
        FieldName = "Estado de la Funci�n proveedora"
    Case "IDI"
        FieldName = "Idioma"
    Case "TFNO", "TFNO2"
        FieldName = "Tel�fono"
    Case "FAX"
        FieldName = "Fax"
    Case "APE"
        FieldName = "Apellidos"
    Case "NOM"
        FieldName = "Nombre"
    Case "DEP"
        FieldName = "Departamento"
    Case "CAR"
        FieldName = "Cargo"
    Case "PWD"
        FieldName = "Password"
    Case "FECPWD"
        FieldName = "Fecha de Password"
    Case "COD"
        FieldName = "Codigo"
    Case "PORT"
        FieldName = "Portal"
    Case "FSGS_SRV"
        FieldName = "Servidor de FULLSTEP GS"
    Case "FSGS_BD"
        FieldName = "Base de datos de FULLSTEP GS"
    Case "NIF"
        FieldName = "N.I.F."
    Case "ACT1"
        FieldName = "Actividad nivel1"
    Case "ACT2"
        FieldName = "Actividad nivel2"
    Case "ACT3"
        FieldName = "Actividad nivel3"
    Case "ACT4"
        FieldName = "Actividad nivel4"
    Case "ACT5"
        FieldName = "Actividad nivel5"
    Case "CIA_ESP"
        FieldName = "Especificaci�n de compa��a"
    
    Case Else
        FieldName = "Campo desconocido"
    End Select
    
End Function



