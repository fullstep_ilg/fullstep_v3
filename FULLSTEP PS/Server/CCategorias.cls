VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCategorias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private mCol As Collection
Private m_varConexion As CConexion

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una categoria de la coleccion
    ''' * Recibe: Indice de la categoria a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property
Public Property Get Item(vntIndexKey As Variant) As CCategoria


    ''' * Objetivo: Recuperar una categoria de la coleccion
    ''' * Recibe: Indice de la categoria a recuperar
    ''' * Devuelve: categoria correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    Set NewEnum = mCol.[_NewEnum]

End Property


Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_varConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = m_varConexion
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set m_varConexion = Nothing
End Sub




''' <summary>A�ade un nuevo elemento a la colecci�n de categorias. </summary>
''' <param name="iId">Id de la categor�a</param>
''' <param name="sCodigo">Codigo de la categor�a</param>
''' <param name="oDenominaciones">Denominaciones de la categor�a</param>
''' <param name="lCosteH">Coste por horas de la categor�a</param>
''' <param name="bDact">True:categoria deshabilida, False:habilitada </param>
''' <returns>Una colecci�n de categor�as</returns>
''' <remarks>Llamada desde CCategor�as; Tiempo m�ximo=0seg.</remarks>

Public Function Add(ByVal lID As Long, ByVal sCodigo As String, ByVal oDenominaciones As CMultiidiomas, _
                    ByVal lCosteH As Long, ByVal bDact As Boolean, Optional ByVal iElim As Integer = 0, Optional ByVal iElimSis As Integer = 0) As CCategoria
                    
Dim objnewmember As CCategoria
Set objnewmember = New CCategoria

With objnewmember
    Set .Conexion = m_varConexion
    .ID = lID
    .Codigo = sCodigo
    Set .Denominaciones = oDenominaciones
    .CosteH = lCosteH
    .Dact = bDact
    .Elim = iElim
    .ElimSis = iElimSis
    
End With
    mCol.Add objnewmember, CStr(lID)
    
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function


''' <summary>Carga todas las categor�as existentes</summary>
''' <param name="iOrden">1:Las devuelve ordenadas por bajalog-c�digo;2:Ordenadas por bajalog-denominaci�n del idioma del usuario.</param>
''' <returns>Coleccion de categorias</returns>
''' <remarks>Llamada desde=frmProveBuscar.Proveseleccionado; Tiempo m�ximo=0seg.</remarks>

Public Function CargarTodasLasCategorias(ByVal iOrden As Integer)

Dim sBDGS As String
Dim sSRVGS As String
Dim sPrefijo As String
Dim adoCom As adodb.Command
Dim ador As adodb.Recordset
Dim oParam As adodb.Parameter
Dim lID As Long
Dim oCategoria As CCategoria
Dim oDenominaciones As CMultiidiomas
Dim oGestorParametros As CGestorParametros
Dim oIdiomas As CIdiomas
Dim oIdioma As CIdioma

On Error GoTo Error

    sSRVGS = g_udtParametrosGenerales.g_sFSSRVCiaCompradora
    sBDGS = g_udtParametrosGenerales.g_sFSBDCiaCompradora
    sPrefijo = sSRVGS & "." & sBDGS & ".dbo."
    
    Set adoCom = New adodb.Command
    Set adoCom.ActiveConnection = m_varConexion.AdoCon

    Set oParam = adoCom.CreateParameter("ORDEN", adInteger, adParamInput, , iOrden)
    adoCom.Parameters.Append oParam
    Set oParam = adoCom.CreateParameter("IDIOMA", adVarChar, adParamInput, 3, g_udtParametrosGenerales.g_sIdioma)
    adoCom.Parameters.Append oParam
    adoCom.CommandType = adCmdStoredProc
    adoCom.CommandText = sPrefijo & "FSGA_CARGAR_CATEGORIAS"
   ' adoCom.Prepared = True
    adoCom.CommandTimeout = 1800
    
    Set ador = adoCom.Execute
    
    Set mCol = Nothing
    Set mCol = New Collection
        
    'Carga todos los idiomas de la aplicaci�n:
    Set oGestorParametros = New CGestorParametros
    Set oGestorParametros.Conexion = m_varConexion
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, True)
        
    While Not ador.eof
        lID = ador.Fields("ID").Value
        If Me.Item(CStr(lID)) Is Nothing Then
            Set oDenominaciones = New CMultiidiomas
            Set oCategoria = Me.Add(lID, ador.Fields("CODIGO").Value, oDenominaciones, ador.Fields("COSTEH").Value, ador.Fields("DACT").Value)
            For Each oIdioma In oIdiomas
                oCategoria.Denominaciones.Add ador.Fields("CODIGO").Value, ador.Fields("DEN_" & oIdioma.Cod).Value, oIdioma.ID
            Next
            Set oDenominaciones = Nothing
            Set oCategoria = Nothing
        End If
        ador.MoveNext
    Wend
    
    ador.Close
    Set ador = Nothing
    Set adoCom = Nothing
    Set oIdiomas = Nothing
    Set oIdioma = Nothing

Exit Function

Error:
    If Err.Number = "-2147217900" Then
         Set mCol = Nothing
    End If

End Function


