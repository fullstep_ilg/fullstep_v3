VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMoneda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' Variables privadas con la informacion de una moneda
Private m_oConexion As CConexion

Private m_iId As Integer
Private m_sCod As String
Private m_oDen As CMultiidiomas
Private m_dblEQUIV As Double

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Property Let EQUIV(ByVal vData As Double)

    ''' * Objetivo: Dar valor a la variable privada EQUIV
    ''' * Recibe: Equivalencia de la moneda respecto a la central
    ''' * Devuelve: Nada
    
    m_dblEQUIV = vData
    
End Property
Public Property Get EQUIV() As Double

    ''' * Objetivo: Devolver la variable privada EQUIV
    ''' * Recibe: Nada
    ''' * Devuelve: Equivalencia de la moneda respecto a la central

    EQUIV = m_dblEQUIV
    
End Property
Public Property Get ID() As Integer

    ''' * Objetivo: Devolver el indice de la moneda en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice de la moneda en la coleccion

    ID = m_iId
    
End Property
Public Property Let ID(ByVal iInd As Integer)

    ''' * Objetivo: Dar valor al indice de la moneda en la coleccion
    ''' * Recibe: Indice de la moneda en la coleccion
    ''' * Devuelve: Nada

    m_iId = iInd
    
End Property
Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la moneda
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la moneda
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property
Public Property Get Denominaciones() As CMultiidiomas

    Set Denominaciones = m_oDen
    
End Property

Public Property Set Denominaciones(ByVal dato As CMultiidiomas)

    Set m_oDen = dato
    
End Property
Public Property Let Cod(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo de la moneda
    ''' * Devuelve: Nada

    m_sCod = vData
    
End Property
Public Property Get Cod() As String

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo de la moneda

    Cod = m_sCod
    
End Property

Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar la memoria
    
    Set m_oConexion = Nothing
    
End Sub

Public Function EliminarMoneda(ByVal oCias As CCias) As CTESError
Dim sConsulta As String
Dim oError As CTESError
Dim sfsg As String
Dim oCia As CCia
Dim lCiaComp As Long
Dim adoComm As adodb.Command
Dim adoParam As adodb.Parameter
Dim adoRes As adodb.Recordset


    On Error GoTo Error
    
    Set oError = New CTESError
    oError.NumError = 0
    
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.AdoCon

    Set adoParam = adoComm.CreateParameter("", adVarChar, adParamInput, 3, m_sCod)
    adoComm.Parameters.Append adoParam
    
    
    m_oConexion.AdoCon.Execute "BEGIN DISTRIBUTED TRANSACTION"
    m_oConexion.AdoCon.Execute "SET XACT_ABORT ON"
    
    If oCias.Count > 0 Then
        For Each oCia In oCias
        
            lCiaComp = oCia.ID
            sfsg = FSGS(m_oConexion, lCiaComp)
            sConsulta = "EXEC " & sfsg & "SP_EXECUTESQL N'UPDATE MON SET FSP_COD= NULL WHERE FSP_COD = @COD', N'@COD VARCHAR(50)', @COD = ?"
            adoComm.CommandType = adCmdText
            adoComm.CommandText = sConsulta
            adoComm.Execute
            
            If m_oConexion.AdoCon.Errors.Count > 0 Then
                GoTo Error
            End If
        Next
    End If
    
    m_oConexion.AdoCon.Execute "DELETE FROM MON WHERE ID=" & m_iId

    m_oConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    m_oConexion.AdoCon.Execute "COMMIT TRANSACTION"
    
    Set EliminarMoneda = oError
    
    Exit Function

Error:
    Set oError = basErrores.TratarError(m_oConexion.AdoCon.Errors)
    If NumTransaccionesAbiertas(m_oConexion.AdoCon) > 0 Then
        m_oConexion.AdoCon.Execute "ROLLBACK"
    End If
    m_oConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    Set EliminarMoneda = oError

End Function

''' <summary>
''' A�adir moneda
''' </summary>
''' <returns>Un error si lo hay</returns>
''' <remarks>Llamada desde: frmMon.sdbgMonedas_BeforeUpdate
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 13/01/2011</revision>
Public Function AnyadirMoneda() As CTESError
    Dim ador As adodb.Recordset
    Dim sConsulta As String
    Dim oError As CTESError
    Dim oMulti As CMultiidioma
    Dim sCod As String
    
    Set oError = New CTESError
    oError.NumError = 0
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "cMoneda.AnyadirMoneda", "No se ha establecido la conexion"
    End If
    ' *****************************************

On Error GoTo Error:

    m_oConexion.AdoCon.Execute "BEGIN TRANSACTION"
    m_oConexion.AdoCon.Execute "SET XACT_ABORT ON"
    
    'Selecciono el id de la comunicaci�n
    'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
    sConsulta = "SELECT MAX(ID) FROM MON"
    Set ador = New adodb.Recordset
    
    ador.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    If IsNull(ador(0).Value) Then
        m_iId = 1
    Else
        m_iId = ador(0).Value + 1
    End If
    
    ador.Close
    Set ador = Nothing

    'Realizo la inserci�n
    
    If Not m_oDen Is Nothing Then
        For Each oMulti In m_oDen
            sCod = sCod & "," & "DEN_" & oMulti.Cod
        Next
    End If
       
    sConsulta = "INSERT INTO MON (ID,COD" & sCod & ",EQUIV) VALUES "
    If m_dblEQUIV = 0 Then
        sConsulta = sConsulta & "(" & m_iId & ",'" & DblQuote(m_sCod) & "'"
        For Each oMulti In m_oDen
            sConsulta = sConsulta & "," & StrToSQLNULL(oMulti.Den)
        Next
        sConsulta = sConsulta & ",NULL)"
    Else
        sConsulta = sConsulta & "(" & m_iId & ",'" & DblQuote(m_sCod) & "'"
        For Each oMulti In m_oDen
            sConsulta = sConsulta & "," & StrToSQLNULL(oMulti.Den)
        Next
        sConsulta = sConsulta & "," & DblToSQLFloat(m_dblEQUIV) & ")"

    End If
    m_oConexion.AdoCon.Execute sConsulta
    
    m_oConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    m_oConexion.AdoCon.Execute "COMMIT TRANSACTION"
    
    Set AnyadirMoneda = oError
    Exit Function
    
Error:
    m_oConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    Set oError = basErrores.TratarError(m_oConexion.AdoCon.Errors)
    Set AnyadirMoneda = oError
    
End Function


Public Function ModificarMonedas() As CTESError
    Dim sConsulta As String
    Dim oError As CTESError
    Dim oMulti As CMultiidioma
    Dim sCod As String
    
    Set oError = New CTESError
    oError.NumError = 0

On Error GoTo Error:
    m_oConexion.AdoCon.Execute "BEGIN TRANSACTION"
    m_oConexion.AdoCon.Execute "SET XACT_ABORT ON"
    
    sConsulta = "UPDATE MON SET "
    For Each oMulti In m_oDen
        If sCod = "" Then
            sCod = sConsulta & " DEN_" & oMulti.Cod & "= " & StrToSQLNULL(oMulti.Den)
            sConsulta = sCod
        Else
            sConsulta = sConsulta & " ,DEN_" & oMulti.Cod & "= " & StrToSQLNULL(oMulti.Den)
        End If
    Next
    sCod = ""
    sConsulta = sConsulta & "," & " EQUIV=" & DblToSQLFloat(m_dblEQUIV)
    sConsulta = sConsulta & " WHERE ID=" & m_iId

    m_oConexion.AdoCon.Execute sConsulta
    
    m_oConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    m_oConexion.AdoCon.Execute "COMMIT TRANSACTION"
       
    Set ModificarMonedas = oError
    Exit Function
    
Error:
    m_oConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    Set oError = basErrores.TratarError(m_oConexion.AdoCon.Errors)
    
    Set ModificarMonedas = oError
    
End Function

Public Function CambiarCodigo(psNuevoCodigo As String) As CTESError
'*************************************************************************
'*** Descripci�n: Cambia el c�digo de la moneda seleccionada.          ***
'*** Par�metros:  ::>> CodigoNuevo, contiene el nuevo c�digo que ser�  ***
'***                   asignado.                                       ***
'*** Valor que devuelve: un CTESError que indicar� si se ha            ***
'***                     producido un error o no.                      ***
'*************************************************************************
    Dim TESError As New CTESError
    Dim cm As adodb.Command
    Dim par As adodb.Parameter

    TESError.NumError = TESnoerror

    ''' Precondicion

    If m_oConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CMoneda.EliminarDeBaseDatos", "No se ha establecido la conexion"
    End If

On Error GoTo Error:
    
    m_oConexion.AdoCon.Execute "BEGIN TRANSACTION"
    m_oConexion.AdoCon.Execute "SET XACT_ABORT ON"
            
    Set cm = New adodb.Command
    Set cm.ActiveConnection = m_oConexion.AdoCon

    Set par = cm.CreateParameter("PAR1", adVarChar, adParamInput, 50, Me.Cod)
    cm.Parameters.Append par
    Set par = cm.CreateParameter("PAR2", adVarChar, adParamInput, 50, psNuevoCodigo)
    cm.Parameters.Append par

    cm.CommandType = adCmdStoredProc
    cm.CommandText = "MON_COD"


    ''' Ejecutar la SP
    cm.Execute

    Set cm = Nothing

    m_sCod = psNuevoCodigo
    
    m_oConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    m_oConexion.AdoCon.Execute "COMMIT TRANSACTION"
        
    Set CambiarCodigo = TESError
    
    Exit Function
    
Error:

    If Not cm Is Nothing Then
       Set cm = Nothing
    End If
    
    Set CambiarCodigo = basErrores.TratarError(m_oConexion.AdoCon.Errors)

    m_oConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    
End Function


''' <summary>Devuelve los datos de la moneda</summary>
''' <returns>Si se ha producido error</returns>
''' <remarks>Llamada desde=frmCompanias.CargarRecursos,frmParametros.MostrarParametros
'''; Tiempo m�ximo=0seg.</remarks>
Public Function cargarMoneda() As CTESError
    Dim TESError As New CTESError
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim adoRes As adodb.Recordset
    Dim oDen As CMultiidiomas
    Dim oGestorParametros As CGestorParametros
    Dim oIdiomas As CIdiomas
    Dim oIdi As CIdioma
    Dim sCod As String
    Dim sCod2 As String ''MultiERP
    Dim sCod3 As String ''MultiERP
    Dim sConsulta As String
    
    'Carga todos los idiomas de la aplicaci�n:
    Set oGestorParametros = New CGestorParametros
    Set oGestorParametros.Conexion = m_oConexion
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, True)
        
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.AdoCon

    Set adoParam = adoComm.CreateParameter("", adInteger, adParamInput, , m_iId)
    adoComm.Parameters.Append adoParam

    adoComm.CommandType = adCmdText
    
    
    For Each oIdi In oIdiomas
        sCod = sCod & ", " & oIdi.Cod & ".DEN AS DEN_" & oIdi.Cod
        sCod2 = sCod2 & " LEFT JOIN MON_DEN " & oIdi.Cod & " WITH(NOLOCK) ON MON.ID = " & oIdi.Cod & ".MON AND " & oIdi.Cod & ".IDIOMA = " & oIdi.ID & " "
        sCod3 = sCod3 & "," & "TABLA.DEN_" & oIdi.Cod ''MultiERP
    Next
    

    
    sConsulta = "SELECT TABLA.COD" & sCod3 & ",TABLA.EQUIV FROM ( "
    sConsulta = sConsulta & " SELECT MON.COD" & sCod & ",MON.EQUIV FROM MON WITH (NOLOCK) "
    sConsulta = sConsulta & sCod2
    sConsulta = sConsulta & " WHERE MON.ID = ? ) AS TABLA"
    

    adoComm.CommandText = sConsulta
    

    Set adoRes = adoComm.Execute
    
    If adoRes.eof Then
        TESError.NumError = TESDatoEliminado
    Else
        m_sCod = adoRes.Fields("COD").Value
        
        Set oDen = Nothing
        Set oDen = New CMultiidiomas
        Set oDen.Conexion = m_oConexion
        For Each oIdi In oIdiomas
            oDen.Add oIdi.Cod, adoRes.Fields("DEN_" & oIdi.Cod).Value
        Next
        Set m_oDen = oDen
        m_dblEQUIV = adoRes.Fields("EQUIV").Value
        TESError.NumError = TESnoerror
    End If
    
    adoRes.Close
    Set adoRes = Nothing
    Set adoComm = Nothing

    Set cargarMoneda = TESError

End Function

''' <summary>
''' Devuelve las companias enlazadas
''' </summary>
''' <returns>CCias</returns>
''' <remarks>Llamada desde: frmMON/cmdEliminar_Click ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: Jbg. Fecha: 03/07/2012
Public Function Enlazada() As CCias
    Dim TESError As New CTESError
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    Dim adoResGS As adodb.Recordset
    Dim adoRes As adodb.Recordset
    Dim oCias As CCias
    Dim sConsulta As String
    
    Set oCias = New CCias
    
    
    Set adoComm = New adodb.Command
    Set adoComm.ActiveConnection = m_oConexion.AdoCon

    Set adoParam = adoComm.CreateParameter("", adVarChar, adParamInput, 3, m_sCod)
    adoComm.Parameters.Append adoParam

    Dim sfsg As String
    Dim lCiaComp As Long
    
    Set adoResGS = New adodb.Recordset
    
    
    adoResGS.Open "SELECT ID, DEN, COD FROM CIAS WITH (NOLOCK) WHERE FCEST=3 AND FSGS_SRV IS NOT NULL AND FSGS_BD IS NOT NULL", m_oConexion.AdoCon, adOpenForwardOnly
    While Not adoResGS.eof
        lCiaComp = adoResGS.Fields("ID").Value
        sfsg = FSGS(m_oConexion, lCiaComp)
        
        sConsulta = "EXEC " & sfsg & "SP_EXECUTESQL N'SELECT COUNT(1) NUMERO FROM MON WITH (NOLOCK) WHERE FSP_COD = @COD', N'@COD VARCHAR(10)', @COD = ?"
        adoComm.CommandType = adCmdText
        adoComm.CommandText = sConsulta
        Set adoRes = adoComm.Execute
    
        If adoRes.Fields("NUMERO").Value > 0 Then
            oCias.Add adoResGS.Fields("ID").Value, adoResGS("COD").Value, adoResGS("DEN").Value, 0, "", "", 3, 0
        End If
        adoRes.Close
        adoResGS.MoveNext
    Wend
    adoResGS.Close
    Set adoResGS = Nothing
    Set adoRes = Nothing
    Set adoComm = Nothing
    Set Enlazada = oCias

End Function
