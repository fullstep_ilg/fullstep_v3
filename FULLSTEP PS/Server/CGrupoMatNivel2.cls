VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGrupoMatNivel2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CGrupoMatNivel2 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************


Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarIndice As Long 'Nos indica la posicion secuencial dentro de una Coleccion
Private mvarCod As String
Private mvarDen As String
Private mvarGMN1Cod As String
Private mvarGMN1Den As String
Private mvarGruposMatNivel3 As CGruposMatNivel3
Private mvarConexion As CConexion 'local copy


Public Property Set GruposMatNivel3(ByVal vData As CGruposMatNivel3)
    Set mvarGruposMatNivel3 = vData
End Property


Public Property Get GruposMatNivel3() As CGruposMatNivel3
    Set GruposMatNivel3 = mvarGruposMatNivel3
End Property

Public Property Get Indice() As Long
    Indice = mvarIndice
End Property

Public Property Let Indice(ByVal Ind As Long)
    mvarIndice = Ind
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Public Property Let Den(ByVal vData As String)
    mvarDen = vData
End Property

Public Property Get Den() As String
    Den = mvarDen
End Property

Public Property Let Cod(ByVal vData As String)
    mvarCod = vData
End Property


Public Property Get Cod() As String
    Cod = mvarCod
End Property
Public Property Let GMN1Cod(ByVal vData As String)
    mvarGMN1Cod = vData
End Property


Public Property Get GMN1Cod() As String
    GMN1Cod = mvarGMN1Cod
End Property
Public Property Let GMN1Den(ByVal vData As String)
    mvarGMN1Den = vData
End Property


Public Property Get GMN1Den() As String
    GMN1Den = mvarGMN1Den
End Property

''' <summary>
''' Carga de todos los grupos de materiales de nivel 3 de la compa��a
''' </summary>
''' <param name="CiaComp">Compa��a</param>
''' <param optional name="CaracteresInicialesCod">Cod de busqueda</param>
''' <param optional name="CaracteresInicialesDen">Den de busqueda</param>
''' <param optional name="CoincidenciaTotal">Si la busqueda ha de coincidir con la den o el cod</param>
''' <param optional name="OrdenadosPorDen">Ordenacion por denominacion</param>
''' <param optional name="UsarIndice">Si se usa indice para la coleccion</param>
''' <remarks>Llamada desde: frmPROVEBuscar.sdbcGMN3_4Cod_DropDown;
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 13/01/2011</revision>
Public Sub CargarTodosLosGruposMat3DeLaCia(ByVal CiaComp As Long, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean)

'Dim ador As ADODB.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim iNumGrups As Integer

Dim sfsg As String
Dim BD As String
Dim SRV As String
Dim ador As adodb.Recordset
    
    
    'Primero debemos obtener los datos de SERVIDOR,BD,... de la CiaCompradora
    
    sConsulta = "SELECT FSGS_SRV,FSGS_BD FROM CIAS WITH (NOLOCK) WHERE ID=" & CiaComp
    Set ador = New adodb.Recordset
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    
    If ador.eof Then
        ador.Close
        Set ador = Nothing
        Set mvarGruposMatNivel3 = New CGruposMatNivel3
        Set mvarGruposMatNivel3.Conexion = mvarConexion
        Exit Sub
        
    End If
    
    SRV = ador("FSGS_SRV").Value
    BD = ador("FSGS_BD").Value
        
    ador.Close
    
    sfsg = SRV & "." & BD & ".dbo."
    
    sConsulta = "SELECT GMN1,GMN2,COD,FECACT,DEN_ENG,DEN_GER,DEN_SPA,QA_NOTIF_USU FROM " & sfsg & "GMN3 AS GMN3 WITH (NOLOCK) WHERE GMN1='" & DblQuote(mvarGMN1Cod) & "'AND GMN2='" & DblQuote(mvarCod) & "'"

If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
    
    
   
Else
    If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
        
        sConsulta = sConsulta & " AND COD >='" & DblQuote(CaracteresInicialesCod) & "'"
        sConsulta = sConsulta & " AND " & DevolverDenGMN & " >='" & DblQuote(CaracteresInicialesDen) & "'"
                    
    Else
        
        If Not (CaracteresInicialesCod = "") Then
            
                sConsulta = sConsulta & "AND COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            
        Else
            
                sConsulta = sConsulta & " AND " & DevolverDenGMN & " >='" & DblQuote(CaracteresInicialesDen) & "'"
            
        End If
    
    End If
           
End If

If OrdenadosPorDen Then
    sConsulta = sConsulta & " ORDER BY " & DevolverDenGMN & ",COD"
Else
    sConsulta = sConsulta & " ORDER BY COD," & DevolverDenGMN
End If
      
ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
      
If ador.eof Then
        
    ador.Close
    Set ador = Nothing
    Set mvarGruposMatNivel3 = New CGruposMatNivel3
    Set mvarGruposMatNivel3.Conexion = mvarConexion
    mvarGruposMatNivel3.eof = True
    Exit Sub
      
Else
    Set mvarGruposMatNivel3 = Nothing
    Set mvarGruposMatNivel3 = New CGruposMatNivel3
    Set mvarGruposMatNivel3.Conexion = mvarConexion
    
    iNumGrups = 0
    
    If UsarIndice Then
        
        lIndice = 0
        
        While Not ador.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            mvarGruposMatNivel3.Add ador("GMN1").Value, ador("GMN2").Value, ador("COD").Value, "", "", ador(DevolverDenGMN).Value, lIndice
            ador.MoveNext
            lIndice = lIndice + 1
            iNumGrups = iNumGrups + 1
        Wend
        
    Else
        
        While Not ador.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            mvarGruposMatNivel3.Add ador("GMN1").Value, ador("GMN2").Value, ador("COD").Value, "", "", ador(DevolverDenGMN).Value
            ador.MoveNext
             iNumGrups = iNumGrups + 1
        Wend
    End If
    ador.Close
    Set ador = Nothing
    Exit Sub
      
End If

End Sub

