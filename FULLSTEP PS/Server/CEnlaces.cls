VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEnlaces"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' Variables que almacenan las propiedades de los objetos

Private mCol As Collection
Private mvarEOF As Boolean
Private mvarConexion As CConexion

''' Control de errores

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Function Add(ByVal CiaCompID As Long, ByVal ProvePortalID As Long, Optional ByVal CiaCompCod As String, Optional ByVal CiaCompDen As String, Optional ByVal ProvePortalCod As String, Optional ByVal ProvePortalDen As String, Optional ByVal ProveGSCod As String, Optional ByVal ProveGSDen As String, Optional ByVal PremiumActivo As Boolean, Optional ByVal IndiceProve As Boolean, Optional ByVal Servidor As String, Optional ByVal baseDatos As String, Optional ByVal sNif As Variant) As CEnlace
    
    Dim objnewmember As CEnlace
    
    Set objnewmember = New CEnlace
   
    ''' Paso de los parametros al nuevo objeto moneda
    objnewmember.IDCiaCompradora = CiaCompID
    objnewmember.IDProveedorPortal = ProvePortalID
    If IsMissing(CiaCompCod) Then
        objnewmember.CodigoCiaCompradora = Null
    Else
        objnewmember.CodigoCiaCompradora = CiaCompCod
    End If
    If IsMissing(CiaCompDen) Then
        objnewmember.DenCiaCompradora = Null
    Else
        objnewmember.DenCiaCompradora = CiaCompDen
    End If
    If IsMissing(ProvePortalCod) Then
        objnewmember.CodigoProveedorPortal = Null
    Else
        objnewmember.CodigoProveedorPortal = ProvePortalCod
    End If
    If IsMissing(ProvePortalDen) Then
        objnewmember.DenProveedorPortal = Null
    Else
        objnewmember.DenProveedorPortal = ProvePortalDen
    End If
    If IsMissing(ProveGSCod) Then
        objnewmember.CodigoProveedorGS = Null
    Else
        objnewmember.CodigoProveedorGS = ProveGSCod
    End If
    If IsMissing(ProveGSDen) Then
        objnewmember.DenProveedorGS = Null
    Else
        objnewmember.DenProveedorGS = ProveGSDen
    End If
    If IsMissing(PremiumActivo) Then
        objnewmember.PremiumActivo = False
    Else
        objnewmember.PremiumActivo = PremiumActivo
    End If
    If IsMissing(Servidor) Then
        objnewmember.ServidorGS = Null
    Else
        objnewmember.ServidorGS = Servidor
    End If
    If IsMissing(baseDatos) Then
        objnewmember.BaseDatosGS = Null
    Else
        objnewmember.BaseDatosGS = baseDatos
    End If
    
    If IsMissing(sNif) Then
        objnewmember.NIF = Null
    Else
        objnewmember.NIF = sNif
    End If
    Set objnewmember.Conexion = mvarConexion
    
    ''' Anyadir el objeto enlace a la coleccion
    If IndiceProve Then
        mCol.Add objnewmember, CStr(ProvePortalID)
    Else
        mCol.Add objnewmember, CStr(CiaCompID)
    End If
    
    
    
    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function
Public Property Get Item(vntIndexKey As Variant) As CEnlace

    ''' * Objetivo: Recuperar un enlace de la coleccion
    ''' * Recibe: Indice del enlace a recuperar
    ''' * Devuelve: Enlace correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property
Public Property Get eof() As Boolean

    ''' * Objetivo: Devolver variable privada EOF
    ''' * Recibe: Nada
    ''' * Devuelve: Variable privada EOF
    
    eof = mvarEOF
    
End Property
Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set mvarConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = mvarConexion
    
End Property
Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    
   
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
    Set mvarConexion = Nothing
        
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property




