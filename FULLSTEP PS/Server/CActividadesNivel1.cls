VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CActividadesNivel1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CActividadesNivel1 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean


Public Sub GenerarEstructuraActividades(ByVal OrdPorDen As Boolean, Optional ByVal sIdioma As String)

Dim oACN1 As CActividadNivel1
Dim oACN2 As CActividadNivel2
Dim oACN3 As CActividadNivel3
Dim oACN4 As CActividadNivel4
Dim oACN5 As CActividadNivel5

Dim sConsulta As String
Dim ador As ADODB.Recordset

Dim sCod As String
Dim sDen As String

Set mCol = Nothing
Set mCol = New Collection
        
         sDen = "DEN_" & sIdioma
         
        ' Generamos la coleccion de grupos mat nivel1
        
        sConsulta = "SELECT ID,COD," & NullToStr(sDen) & " FROM ACT1 WITH (NOLOCK) "
        
        If OrdPorDen Then
            sConsulta = sConsulta & " ORDER BY " & NullToStr(sDen) & ""
        Else
            sConsulta = sConsulta & " ORDER BY COD"
        End If
        
        Set ador = New ADODB.Recordset
        
        ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
        If ador.eof Then
            
            Set ador = Nothing
            Exit Sub
        End If
        
        While Not ador.eof
            Set oACN1 = Me.Add(ador("ID").Value, ador("COD").Value, ador(sDen).Value)
            Set oACN1.ActividadesNivel2 = New CActividadesNivel2
            Set oACN1.ActividadesNivel2.Conexion = mvarConexion
            
            ador.MoveNext
            Set oACN1 = Nothing
        Wend
        
        ador.Close
        
        
        ' Generamos la coleccion de grupos mat nivel2
        
        sConsulta = "SELECT ACT1,ID,COD," & NullToStr(sDen) & " FROM ACT2 WITH (NOLOCK) "
        
        If OrdPorDen Then
            sConsulta = sConsulta & " ORDER BY " & NullToStr(sDen) & ""
        Else
            sConsulta = sConsulta & " ORDER BY COD"
        End If
        
        ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
        If ador.eof Then
            Set ador = Nothing
            Exit Sub
        End If
        
        While Not ador.eof
            sCod = CStr(ador("ACT1").Value) & Mid$("                         ", 1, 10 - Len(CStr(ador("ACT1").Value)))
            Set oACN1 = Me.Item(sCod)
            Set oACN2 = oACN1.ActividadesNivel2.Add(CStr(ador("ACT1").Value), ador("ID").Value, ador("COD").Value, ador(sDen).Value)
            ' Creo una nueva coleccion para cada uno
            Set oACN2.ActividadesNivel3 = New CActividadesNivel3
            Set oACN2.ActividadesNivel3.Conexion = mvarConexion
            
            ador.MoveNext
            Set oACN1 = Nothing
            Set oACN2 = Nothing
        Wend
        ador.Close
       
        
               
        ' Generamos la coleccion de grupos mat nivel3
        
        sConsulta = "SELECT ACT1,ACT2,ID,COD," & NullToStr(sDen) & " FROM ACT3 WITH (NOLOCK) "
        
        If OrdPorDen Then
            sConsulta = sConsulta & " ORDER BY " & NullToStr(sDen) & ""
        Else
            sConsulta = sConsulta & " ORDER BY COD"
        End If
        
        ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
        If ador.eof Then
            Set ador = Nothing
            Exit Sub
        End If
        
        While Not ador.eof
            sCod = CStr(ador("ACT1").Value) & Mid$("                         ", 1, 10 - Len(CStr(ador("ACT1").Value)))
            Set oACN1 = Me.Item(sCod)
            sCod = sCod & CStr(ador("ACT2").Value) & Mid$("                         ", 1, 10 - Len(CStr(ador("ACT2").Value)))
            Set oACN2 = oACN1.ActividadesNivel2.Item(sCod)
            Set oACN3 = oACN2.ActividadesNivel3.Add(ador("ACT1").Value, ador("ACT2").Value, ador("ID").Value, ador("COD").Value, ador(sDen).Value)
            ' Creo una nueva coleccion para cada uno
            Set oACN3.ActividadesNivel4 = New CActividadesNivel4
            Set oACN3.ActividadesNivel4.Conexion = mvarConexion
            
            ador.MoveNext
            Set oACN1 = Nothing
            Set oACN2 = Nothing
            Set oACN3 = Nothing
        Wend
        
        ador.Close
        
        
        ' Generamos la coleccion de grupos mat nivel4
        
        sConsulta = "SELECT ACT1,ACT2,ACT3,ID,COD," & NullToStr(sDen) & " FROM ACT4 WITH (NOLOCK) "
        
        If OrdPorDen Then
            sConsulta = sConsulta & " ORDER BY " & NullToStr(sDen) & ""
        Else
            sConsulta = sConsulta & " ORDER BY COD"
        End If
        
        ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        
        If ador.eof Then
           ador.Close
            Set ador = Nothing
            Exit Sub
        End If
        
        While Not ador.eof
            sCod = CStr(ador("ACT1").Value) & Mid$("                         ", 1, 10 - Len(CStr(ador("ACT1").Value)))
            Set oACN1 = Me.Item(sCod)
            sCod = sCod & CStr(ador("ACT2").Value) & Mid$("                         ", 1, 10 - Len(CStr(ador("ACT2").Value)))
            Set oACN2 = oACN1.ActividadesNivel2.Item(sCod)
            sCod = sCod & CStr(ador("ACT3").Value) & Mid$("                         ", 1, 10 - Len(CStr(ador("ACT3").Value)))
            Set oACN3 = oACN2.ActividadesNivel3.Item(sCod)
            Set oACN4 = oACN3.ActividadesNivel4.Add(ador("ACT1").Value, ador("ACT2").Value, ador("ACT3").Value, ador("ID").Value, ador("COD").Value, ador(sDen).Value)
            ' Creo una nueva coleccion para cada uno
            Set oACN4.ActividadesNivel5 = New CActividadesNivel5
            Set oACN4.ActividadesNivel5.Conexion = mvarConexion
            
            ador.MoveNext
            Set oACN1 = Nothing
            Set oACN2 = Nothing
            Set oACN3 = Nothing
        Wend
           
        ador.Close
        
        
        ' Generamos la coleccion de grupos mat nivel4
        
        sConsulta = "SELECT ACT1,ACT2,ACT3,ACT4,ID,COD," & NullToStr(sDen) & " FROM ACT5 WITH (NOLOCK) "
        
        If OrdPorDen Then
            sConsulta = sConsulta & " ORDER BY " & NullToStr(sDen) & ""
        Else
            sConsulta = sConsulta & " ORDER BY COD"
        End If
        
        ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
       
        If ador.eof Then
            ador.Close
            Set ador = Nothing
            Exit Sub
        End If
        
        While Not ador.eof
            sCod = CStr(ador("ACT1").Value) & Mid$("                         ", 1, 10 - Len(CStr(ador("ACT1").Value)))
            Set oACN1 = Me.Item(sCod)
            sCod = sCod & CStr(ador("ACT2").Value) & Mid$("                         ", 1, 10 - Len(CStr(ador("ACT2").Value)))
            Set oACN2 = oACN1.ActividadesNivel2.Item(sCod)
            sCod = sCod & CStr(ador("ACT3").Value) & Mid$("                         ", 1, 10 - Len(CStr(ador("ACT3").Value)))
            Set oACN3 = oACN2.ActividadesNivel3.Item(sCod)
            sCod = sCod & CStr(ador("ACT4").Value) & Mid$("                         ", 1, 10 - Len(CStr(ador("ACT4").Value)))
            Set oACN4 = oACN3.ActividadesNivel4.Item(sCod)
            oACN4.ActividadesNivel5.Add ador("ACT1").Value, ador("ACT2").Value, ador("ACT3").Value, ador("ACT4").Value, ador("ID").Value, ador("COD").Value, ador(sDen).Value
            ador.MoveNext
            Set oACN1 = Nothing
            Set oACN2 = Nothing
            Set oACN3 = Nothing
            Set oACN4 = Nothing
            
        Wend
           
        ador.Close
        Set ador = Nothing

End Sub
Public Function GenerarListadoEstructuraActividades(ByVal OrdPorDen As Boolean, Optional ByVal sIdioma As String, Optional ByVal iAct1 As Long, Optional ByVal iAct2 As Long, Optional ByVal iAct3 As Long, Optional ByVal iAct4 As Long, Optional ByVal iAct5 As Long) As ADODB.Recordset

Dim sConsulta As String
Dim ador As ADODB.Recordset

Dim sCod As String
Dim sDen As String

Set mCol = Nothing
Set mCol = New Collection
        
         sDen = "DEN_" & sIdioma
         
        ' Generamos un recordset con toda la estructura
        
        sConsulta = "SELECT ACT1.COD cod_act1,ACT1." & sDen & " den_act1,ACT2.COD cod_act2,ACT2." & sDen & " den_act2,ACT3.COD cod_act3,ACT3." & sDen & " den_act3,ACT4.COD cod_act4,ACT4." & sDen & " den_act4,ACT5.COD cod_act5,ACT5." & sDen & " den_act5 FROM ACT1 WITH (NOLOCK)"
        sConsulta = sConsulta & " LEFT JOIN ACT2 WITH (NOLOCK) ON ACT1.ID = ACT2.ACT1"
        sConsulta = sConsulta & " LEFT JOIN ACT3 WITH (NOLOCK) ON ACT2.ACT1 = ACT3.ACT1 AND ACT2.ID = ACT3.ACT2"
        sConsulta = sConsulta & " LEFT JOIN ACT4 WITH (NOLOCK) ON ACT3.ACT1 = ACT4.ACT1 AND ACT3.ACT2 = ACT4.ACT2 AND ACT3.ID = ACT4.ACT3"
        sConsulta = sConsulta & " LEFT JOIN ACT5 WITH (NOLOCK) ON ACT4.ACT1 = ACT5.ACT1 AND ACT4.ACT2 = ACT5.ACT2 AND ACT4.ACT3 = ACT5.ACT3 AND ACT4.ID = ACT5.ACT4"
        
        If iAct5 <> 0 Then
            sConsulta = sConsulta & " WHERE ACT1.ID = " & iAct1 & " AND ACT2.ID = " & iAct2 & " AND ACT3.ID =" & iAct3 & " AND ACT4.ID = " & iAct4 & " AND ACT5.ID = " & iAct5
        Else
            If iAct4 <> 0 Then
                sConsulta = sConsulta & " WHERE ACT1.ID = " & iAct1 & " AND ACT2.ID = " & iAct2 & " AND ACT3.ID =" & iAct3 & " AND ACT4.ID = " & iAct4
            Else
                If iAct3 <> 0 Then
                    sConsulta = sConsulta & " WHERE ACT1.ID = " & iAct1 & " AND ACT2.ID = " & iAct2 & " AND ACT3.ID =" & iAct3
                Else
                    If iAct2 <> 0 Then
                        sConsulta = sConsulta & " WHERE ACT1.ID = " & iAct1 & " AND ACT2.ID = " & iAct2
                    Else
                        If iAct1 <> 0 Then
                            sConsulta = sConsulta & " WHERE ACT1.ID = " & iAct1
                        End If
                    End If
                End If
            End If
        End If
            
        If OrdPorDen Then
            sConsulta = sConsulta & " ORDER BY ACT1." & sDen & ",ACT2." & sDen & ",ACT3." & sDen & ",ACT4." & sDen & ",ACT5." & sDen
        Else
            sConsulta = sConsulta & " ORDER BY ACT1.COD,ACT2.COD,ACT3.COD,ACT4.COD,ACT5.COD"
        End If
        
        Set ador = New ADODB.Recordset
        
        ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
        'ador.MoveFirst
        
        Set ador.ActiveConnection = Nothing
        
        Set GenerarListadoEstructuraActividades = ador
        


End Function

Public Function Add(ByVal Id As Integer, ByVal Cod As String, ByVal Den As String, Optional ByVal varIndice As Variant, Optional ByVal varAsign As Variant, Optional ByVal varPendiente As Variant, Optional ByVal FecAct As Date) As CActividadNivel1
    'create a new object
    Dim objnewmember As CActividadNivel1
    Dim sCod As String
    
    Set objnewmember = New CActividadNivel1
   
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    
    objnewmember.Id = Id
    If Not IsMissing(varAsign) Then
        If varAsign Then
            objnewmember.Asignada = True
        Else
            objnewmember.Asignada = False
        End If
        
    End If
    
    If Not IsMissing(FecAct) And Not IsNull(FecAct) Then
        objnewmember.FecAct = FecAct
    End If
    
    If Not IsMissing(varPendiente) Then
        If varPendiente Then
            objnewmember.Pendiente = True
        Else
            objnewmember.Pendiente = False
        End If
    Else
            objnewmember.Pendiente = False
    End If
    Set objnewmember.Conexion = mvarConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        sCod = CStr(Id) & Mid$("                         ", 1, 10 - Len(CStr(Id)))
        mCol.Add objnewmember, sCod
    End If
      
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

Public Property Get Item(vntIndexKey As Variant) As CActividadNivel1
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()

        Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub


Public Sub CargarTodasLasActividades(ByVal CodIdioma As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal TipoBusqueda As Integer, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean)
Dim ador As ADODB.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim sDen As String

'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************
sDen = "DEN_" & CodIdioma

sConsulta = "SELECT ID,COD," & sDen & " FROM ACT1 WITH (NOLOCK) "

If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
    
    
   
Else
    If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
        
        Select Case TipoBusqueda
            
        Case -1
        
            sConsulta = sConsulta & " WHERE COD ='" & basUtilidades.DblQuote(CaracteresInicialesCod) & "'"
            sConsulta = sConsulta & " AND " & sDen & "='" & basUtilidades.DblQuote(CaracteresInicialesDen) & "'"
            
        Case 0
            
            sConsulta = sConsulta & " WHERE COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
            sConsulta = sConsulta & " AND DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                       
        Case 1
            
            sConsulta = sConsulta & " WHERE COD >= '" & DblQuote(CaracteresInicialesCod) & "'"
            sConsulta = sConsulta & " AND " & sDen & ">= '" & DblQuote(CaracteresInicialesDen) & "'"
                        
        End Select
                    
    Else
        
        If Not (CaracteresInicialesCod = "") Then
            
            Select Case TipoBusqueda
            
            Case -1
            
                sConsulta = sConsulta & " WHERE COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                
            Case 0
                    
               sConsulta = sConsulta & " WHERE COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                
            Case 1
                    
               sConsulta = sConsulta & " WHERE COD >= '" & DblQuote(CaracteresInicialesCod) & "'"
                            
            End Select
            
        Else
            
            Select Case TipoBusqueda
            
            Case -1

                sConsulta = sConsulta & " WHERE " & sDen & "='" & DblQuote(CaracteresInicialesDen) & "'"
                
            Case 0
            
                sConsulta = sConsulta & " WHERE DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                  
            Case 1
                sConsulta = sConsulta & " WHERE " & sDen & ">= '" & DblQuote(CaracteresInicialesDen) & "'"
                
            End Select
            
        End If
    
    End If
           
End If

If OrdenadosPorDen Then
    sConsulta = sConsulta & " ORDER BY " & sDen & " ,COD"
Else
    sConsulta = sConsulta & " ORDER BY COD," & sDen
End If
      
Set ador = New ADODB.Recordset
        
ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
If ador.eof Then

    ador.Close
    Set ador = Nothing
    Set mCol = Nothing
    Set mCol = New Collection
    
    Exit Sub
      
Else
    
    Set mCol = Nothing
    Set mCol = New Collection
        
    If UsarIndice Then
        
        lIndice = 0
        
        While Not ador.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            Me.Add ador("ID").Value, ador("COD").Value, ador(sDen).Value, lIndice
            ador.MoveNext
            lIndice = lIndice + 1
        Wend
        
    Else
        
        While Not ador.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            Me.Add ador("ID").Value, ador("COD").Value, ador(sDen).Value
            ador.MoveNext
        Wend
    End If
    
    ador.Close
    Set ador = Nothing
    Exit Sub
      
End If
End Sub

Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

Public Function DevolverTodasLasActividades(ByVal CodIdioma As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal TipoBusqueda As Integer, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean) As ADODB.Recordset
Dim ador As ADODB.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim sDen As String

    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CActividadesNivel1.DevolverTodasLasActividades", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
    If CodIdioma <> "" Then
        sDen = "DEN_" & CodIdioma
    Else
         sDen = "DEN_" & g_udtParametrosGenerales.g_sIdioma
    End If
    sConsulta = "SELECT ID,COD," & sDen & " FROM ACT1 WITH (NOLOCK) "

    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
    
    
   
    Else
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
        
            Select Case TipoBusqueda
            
            Case -1
        
                sConsulta = sConsulta & " WHERE COD ='" & basUtilidades.DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND " & sDen & "='" & basUtilidades.DblQuote(CaracteresInicialesDen) & "'"
            
            Case 0
            
                sConsulta = sConsulta & " WHERE COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                sConsulta = sConsulta & " AND " & sDen & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                       
            Case 1
                
                sConsulta = sConsulta & " WHERE COD >= '" & DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND " & sDen & ">= '" & DblQuote(CaracteresInicialesDen) & "'"
                        
            End Select
                    
        Else
            
            If Not (CaracteresInicialesCod = "") Then
            
                Select Case TipoBusqueda
            
                Case -1
            
                    sConsulta = sConsulta & " WHERE COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                
                Case 0
                    
                    sConsulta = sConsulta & " WHERE COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                
                Case 1
                    
                    sConsulta = sConsulta & " WHERE COD >= '" & DblQuote(CaracteresInicialesCod) & "'"
                            
                End Select
            
            Else
            
                Select Case TipoBusqueda
            
                Case -1

                    sConsulta = sConsulta & " WHERE " & sDen & "='" & DblQuote(CaracteresInicialesDen) & "'"
                
                Case 0
            
                    sConsulta = sConsulta & " WHERE " & sDen & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                  
                Case 1
                    sConsulta = sConsulta & " WHERE " & sDen & ">= '" & DblQuote(CaracteresInicialesDen) & "'"
                
                End Select
            
            End If
    
        End If
           
    End If

    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY " & sDen & " ,COD"
    Else
        sConsulta = sConsulta & " ORDER BY COD," & sDen
    End If
      
    Set ador = New ADODB.Recordset
        
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    Set ador.ActiveConnection = Nothing
    
    If ador.RecordCount = 0 Then
        Set DevolverTodasLasActividades = Nothing
    Else
        Set DevolverTodasLasActividades = ador
    End If
    
End Function

Public Function EsPYME() As Boolean
Dim ador As ADODB.Recordset
Dim sConsulta As String

    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CActividadesNivel1.EsPYME", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
    
    sConsulta = "SELECT count(PYME) as iPYME from ACT1 WITH (NOLOCK) where PYME is not null"
    Set ador = New ADODB.Recordset
        
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    Set ador.ActiveConnection = Nothing
    
    If ador.Fields("iPYME").Value > 0 Then
        EsPYME = True
    Else
        EsPYME = False
    End If
End Function

