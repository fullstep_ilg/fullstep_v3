VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CActividadNivel3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CActividadNivel3 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************


Option Explicit

Implements IBaseDatos

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_iACN1 As Integer
Private m_iIndice As Long 'Nos indica la posicion secuencial dentro de una Coleccion
Private m_sCod As String
Private m_sDen As String
Private m_iId As Integer
Private m_bAsignada As Boolean
Private m_bPendiente As Boolean
Private m_oActividadesNivel4 As CActividadesNivel4
Private m_oConexion As CConexion 'local copy
Private m_vFecAct As Variant
Private m_iACN2 As Integer

Public Property Let FecAct(ByVal vData As Date)
    m_vFecAct = vData
End Property

Public Property Get FecAct() As Date
    FecAct = m_vFecAct
End Property
Public Property Get ACN1() As Integer
    ACN1 = m_iACN1
End Property
Public Property Let ACN1(ByVal Ind As Integer)
    m_iACN1 = Ind
End Property
Public Property Get ACN2() As Integer
    ACN2 = m_iACN2
End Property
Public Property Let ACN2(ByVal Ind As Integer)
    m_iACN2 = Ind
End Property

Public Property Get ID() As Integer
    ID = m_iId
End Property
Public Property Let ID(ByVal Ind As Integer)
    m_iId = Ind
End Property
Public Property Get Indice() As Long
    Indice = m_iIndice
End Property
Public Property Let Indice(ByVal Ind As Long)
    m_iIndice = Ind
End Property

Public Property Get Pendiente() As Boolean
    Pendiente = m_bPendiente
End Property
Public Property Let Pendiente(ByVal Ind As Boolean)
    m_bPendiente = Ind
End Property
Public Property Get Asignada() As Boolean
Asignada = m_bAsignada
End Property
Public Property Let Asignada(ByVal bAsignada As Boolean)
m_bAsignada = bAsignada
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Set ActividadesNivel4(ByVal vData As CActividadesNivel4)
    Set m_oActividadesNivel4 = vData
End Property


Public Property Get ActividadesNivel4() As CActividadesNivel4
    Set ActividadesNivel4 = m_oActividadesNivel4
End Property

Public Property Let Den(ByVal vData As String)
    m_sDen = vData
End Property


Public Property Get Den() As String
    Den = m_sDen
End Property



Public Property Let Cod(ByVal vData As String)
    m_sCod = vData
End Property


Public Property Get Cod() As String
    Cod = m_sCod
End Property


Private Sub Class_Terminate()
    
    Set m_oConexion = Nothing
   
    
End Sub


Public Sub CargarTodasLasActividadesNivel4(ByVal CodIdioma As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean)

Dim ador As ADODB.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim iNumGrups As Integer
Dim sDen As String

'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************

sDen = "DEN_" & CodIdioma

sConsulta = "SELECT ACT4.ACT1,ACT4.ACT2,ACT4.ACT3,ACT4.ID,ACT4.COD,ACT4." & sDen & " FROM ACT4 WITH (NOLOCK) WHERE ACT4.ACT1=" & m_iACN1 & " AND ACT4.ACT2=" & m_iACN2 & " AND ACT4.ACT3=" & m_iId

If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
   
Else
    If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
        
        If Not CoincidenciaTotal Then
           sConsulta = sConsulta & " AND COD >='" & DblQuote(CaracteresInicialesCod) & "'"
           sConsulta = sConsulta & " AND " & sDen & " >='" & DblQuote(CaracteresInicialesDen) & "'"
        Else
           sConsulta = sConsulta & " AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
           sConsulta = sConsulta & " AND " & sDen & " ='" & DblQuote(CaracteresInicialesDen) & "'"
        End If
    Else
        
        If Not (CaracteresInicialesCod = "") Then
            
           If Not CoincidenciaTotal Then
                sConsulta = sConsulta & "AND COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            Else
                sConsulta = sConsulta & "AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
            End If
            
        Else
            If Not CoincidenciaTotal Then
                sConsulta = sConsulta & "AND " & sDen & " >='" & DblQuote(CaracteresInicialesDen) & "'"
            Else
                sConsulta = sConsulta & "AND " & sDen & " ='" & DblQuote(CaracteresInicialesDen) & "'"
            End If
        End If
    
    End If
           
End If

If OrdenadosPorDen Then
    sConsulta = sConsulta & " ORDER BY " & sDen & ",COD"
Else
    sConsulta = sConsulta & " ORDER BY COD," & sDen
End If
      
Set ador = New ADODB.Recordset
        
ador.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
If ador.eof Then

    ador.Close
    Set ador = Nothing
    Set m_oActividadesNivel4 = Nothing
    Set m_oActividadesNivel4 = New CActividadesNivel4
   
    Set m_oActividadesNivel4.Conexion = m_oConexion
    
    Exit Sub
      
Else
    
    Set m_oActividadesNivel4 = Nothing
    Set m_oActividadesNivel4 = New CActividadesNivel4
    Set m_oActividadesNivel4.Conexion = m_oConexion
    
    If UsarIndice Then
        
        lIndice = 0
        iNumGrups = 0
        
        While Not ador.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            m_oActividadesNivel4.Add ador("ACT1").Value, ador("ACT2").Value, ador("ACT3").Value, ador("ID").Value, ador("COD").Value, ador(sDen).Value, lIndice
            ador.MoveNext
            lIndice = lIndice + 1
            iNumGrups = iNumGrups + 1
        Wend
        
    Else
        
        While Not ador.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            m_oActividadesNivel4.Add ador("ACT1").Value, ador("ACT2").Value, ador("ACT3").Value, ador("ID").Value, ador("COD").Value, ador(sDen).Value
            ador.MoveNext
            iNumGrups = iNumGrups + 1
        Wend
    End If
    

    Set ador = Nothing
    Exit Sub
      
End If

End Sub

Public Function DevolverTodasLasActividadesNivel4(ByVal idAct1 As Integer, ByVal idAct2 As Integer, ByVal idAct3 As Integer, ByVal CodIdioma As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean)

    Dim ador As ADODB.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
    Dim iNumGrups As Integer
    Dim sDen As String

    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************

    sDen = "DEN_" & CodIdioma
    m_iACN1 = idAct1
    m_iACN2 = idAct2
    m_iId = idAct3
    sConsulta = "SELECT ACT4.ACT1,ACT4.ACT2,ACT4.ACT3,ACT4.ID,ACT4.COD,ACT4." & sDen & " FROM ACT4 WITH (NOLOCK) WHERE ACT4.ACT1=" & m_iACN1 & " AND ACT4.ACT2=" & m_iACN2 & " AND ACT4.ACT3=" & m_iId

    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
   
    Else
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
        
            If Not CoincidenciaTotal Then
                sConsulta = sConsulta & " AND COD >='" & DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND " & sDen & " >='" & DblQuote(CaracteresInicialesDen) & "'"
            Else
                sConsulta = sConsulta & " AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND " & sDen & " ='" & DblQuote(CaracteresInicialesDen) & "'"
            End If
        Else
        
            If Not (CaracteresInicialesCod = "") Then
            
                If Not CoincidenciaTotal Then
                    sConsulta = sConsulta & "AND COD >='" & DblQuote(CaracteresInicialesCod) & "'"
                Else
                    sConsulta = sConsulta & "AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                End If
            
            Else
                If Not CoincidenciaTotal Then
                    sConsulta = sConsulta & "AND " & sDen & " >='" & DblQuote(CaracteresInicialesDen) & "'"
                Else
                    sConsulta = sConsulta & "AND " & sDen & " ='" & DblQuote(CaracteresInicialesDen) & "'"
                End If
            End If
    
        End If
           
    End If

    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY " & sDen & ",COD"
    Else
        sConsulta = sConsulta & " ORDER BY COD," & sDen
    End If
      
    Set ador = New ADODB.Recordset
        
    ador.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    Set DevolverTodasLasActividadesNivel4 = ador
End Function

''' <summary>Esta funci�n a�ade un registro en la tabla ACT3 </summary>
''' <param name="Cod">C�digo</param>
''' <param name="Den">Denominaci�n</param>
''' <param name="idioma">Idioma</param>
''' <returns>Error si lo hay</returns>
''' <remarks>Llamada desde: FSPSClient;
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 13/01/2012</revision>
Private Function IBaseDatos_AnyadirABaseDatos(Cod() As String, Den() As String, idioma As String) As CTESError
Dim TESError As CTESError
Dim sConsulta As String
Dim rs As New ADODB.Recordset
Dim i As Integer
Dim sCod As String
Dim sDen As String

Set TESError = New CTESError
TESError.NumError = TESnoerror
'********* Precondicion *******************
If m_oConexion Is Nothing Then

    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CActividadNivel1.AnyadirABaseDatos", "No se ha establecido la conexion"
End If
'******************************************

TESError.NumError = TESnoerror

On Error GoTo Error

    'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
    sConsulta = "SELECT MAX(ID) FROM ACT3 WHERE ACT1=" & m_iACN1 & " AND ACT2= " & m_iACN2 & ""
        
    rs.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
        m_iId = 1
    Else
        m_iId = NullToDbl0(rs.Fields(0).Value) + 1
    End If
    
    While i < UBound(Cod()) + 1
        If Cod(i) = idioma Then
            m_sDen = Den(i)
        End If
        sCod = sCod & "," & "DEN_" & Cod(i)
        sDen = sDen & ",'" & NullToStr(Den(i)) & "'"
        i = i + 1
    Wend
    
    sConsulta = "INSERT INTO ACT3 (ACT1,ACT2,ID,COD" & sCod & ") VALUES (" & m_iACN1 & "," & m_iACN2 & "," & m_iId & ",'" & NullToStr(m_sCod) & "'"
    sConsulta = sConsulta & sDen & ")"
    m_oConexion.AdoCon.Execute sConsulta
    
    rs.Close
    
    'CALIDAD Se obtiene la Fecha de Actualizaci�n con bloqueo para recuperar la verdadera �ltima fecha de actualizaci�n
    sConsulta = "SELECT FECACT FROM ACT3 WHERE ID= " & m_iId & " AND ACT1=" & m_iACN1 & " AND ACT2=" & m_iACN2 & ""
    
    rs.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

    
    m_vFecAct = rs.Fields(0).Value
    
    Set IBaseDatos_AnyadirABaseDatos = TESError
    Exit Function
    
Error:
    
    Set IBaseDatos_AnyadirABaseDatos = basErrores.TratarError(m_oConexion.AdoCon.Errors)

End Function


Private Function IBaseDatos_CambiarCodigo(CodigoNuevo As Variant) As CTESError
Dim TESError As CTESError
Dim cm As ADODB.Command
Dim par As ADODB.Parameter

Set TESError = New CTESError
TESError.NumError = TESnoerror
'********* Precondicion *******************
If m_oConexion Is Nothing Then

    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CActividadNivel3.CambiarCodigo", "No se ha establecido la conexion"
End If
'******************************************

TESError.NumError = TESnoerror

On Error GoTo Error

    Set cm = New ADODB.Command
    Set cm.ActiveConnection = m_oConexion.AdoCon

    Set par = cm.CreateParameter("ACT1", adInteger, adParamInput, , m_iACN1)
    cm.Parameters.Append par
    Set par = cm.CreateParameter("ACT2", adInteger, adParamInput, , m_iACN2)
    cm.Parameters.Append par
    Set par = cm.CreateParameter("ID", adInteger, adParamInput, , m_iId)
    cm.Parameters.Append par
    Set par = cm.CreateParameter("NEW", adVarChar, adParamInput, 50, NullToStr(CodigoNuevo))
    cm.Parameters.Append par

    cm.CommandType = adCmdStoredProc
    cm.CommandText = "ACT3_COD"

    ''' Ejecutar la SP
    cm.Execute

    Set cm = Nothing

    Set IBaseDatos_CambiarCodigo = TESError
    Exit Function
    
Error:
    
    If Not cm Is Nothing Then
       Set cm = Nothing
    End If
    
    Set IBaseDatos_CambiarCodigo = basErrores.TratarError(m_oConexion.AdoCon.Errors)

End Function

Private Function IBaseDatos_EliminarDeBaseDatos() As CTESError

Dim TESError As CTESError
Dim rs As New ADODB.Recordset
Dim sConsulta As String
Dim adoCom As ADODB.Command
Dim oParam As ADODB.Parameter
Dim sfsg As String

Set TESError = New CTESError
TESError.NumError = TESnoerror
'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CActividadesN1.EliminarDeBaseDatos", "No se ha establecido la conexion"
End If

On Error GoTo Error:
'*****************************************************
m_oConexion.AdoCon.Execute "BEGIN DISTRIBUTED TRAN"
m_oConexion.AdoCon.Execute "SET XACT_ABORT ON"

sConsulta = "SELECT FSGS_SRV,FSGS_BD,ID FROM CIAS WITH (NOLOCK) WHERE FCEST = 3 AND FSGS_SRV<>'NULL' AND FSGS_BD<>'NULL'"

rs.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

If Not rs.eof Then

    While Not rs.eof
    
        Set adoCom = New ADODB.Command
        Set adoCom.ActiveConnection = m_oConexion.AdoCon
        
        sfsg = rs(0).Value & "." & rs(1).Value & ".dbo."
    
        Set oParam = adoCom.CreateParameter("ACT1", adSmallInt, adParamInput, , m_iACN1)
        adoCom.Parameters.Append oParam
        Set oParam = adoCom.CreateParameter("ACT2", adSmallInt, adParamInput, , m_iACN2)
        adoCom.Parameters.Append oParam
        Set oParam = adoCom.CreateParameter("ACT3", adSmallInt, adParamInput, , m_iId)
        adoCom.Parameters.Append oParam
        Set oParam = adoCom.CreateParameter("ACT4", adSmallInt, adParamInput, , Null)
        adoCom.Parameters.Append oParam
        Set oParam = adoCom.CreateParameter("ACT5", adSmallInt, adParamInput, , Null)
        adoCom.Parameters.Append oParam
        adoCom.CommandType = adCmdStoredProc
        adoCom.CommandText = sfsg & "P_GMN4_ACT_MODIF"
        adoCom.Prepared = True
        adoCom.CommandTimeout = 1800
        
        adoCom.Execute
        
        rs.MoveNext
        Set adoCom = Nothing
    Wend

End If


sConsulta = "DELETE FROM CIAS_ACT3 WHERE ACT3=" & m_iId & " AND ACT1=" & m_iACN1 & " AND ACT2=" & m_iACN2 & " "
m_oConexion.AdoCon.Execute sConsulta

sConsulta = "DELETE FROM MCIAS_ACT3 WHERE ACT3=" & m_iId & " AND ACT1=" & m_iACN1 & " AND ACT2=" & m_iACN2 & ""
m_oConexion.AdoCon.Execute sConsulta

sConsulta = "DELETE FROM ACT3 WHERE ID=" & m_iId & " AND ACT1=" & m_iACN1 & " AND ACT2=" & m_iACN2 & ""
m_oConexion.AdoCon.Execute sConsulta
    
m_oConexion.AdoCon.Execute "SET XACT_ABORT OFF"
m_oConexion.AdoCon.Execute "COMMIT TRAN"

Set IBaseDatos_EliminarDeBaseDatos = TESError

rs.Close
Set rs = Nothing
    
Exit Function

Error:
    
    Set TESError = basErrores.TratarError(m_oConexion.AdoCon.Errors)
    m_oConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    Set IBaseDatos_EliminarDeBaseDatos = TESError
    
End Function

Private Function IBaseDatos_FinalizarEdicionModificando(Cod() As String, Den() As String, idioma As String) As CTESError
Dim TESError As CTESError
Dim sConsulta As String
Dim rs As New ADODB.Recordset
Dim sTotalStr As String
Dim sCod As String
Dim sDen As String
Dim i As Integer

Set TESError = New CTESError
TESError.NumError = TESnoerror
'********* Precondicion *******************
If m_oConexion Is Nothing Then

    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CActividadNivel3.FinalizarEdicionModificando", "No se ha establecido la conexion"
End If
'******************************************

TESError.NumError = TESnoerror

On Error GoTo Error

    While i < UBound(Cod()) + 1
        If Cod(i) = idioma Then
            m_sDen = Den(i)
        End If
        sCod = " DEN_" & Cod(i) & "="
        sDen = "'" & NullToStr(Den(i)) & "'" & ","
        sTotalStr = sTotalStr & sCod & sDen
        i = i + 1
    Wend
    
    sTotalStr = Left(sTotalStr, Len(sTotalStr) - 1)

    sConsulta = "UPDATE ACT3 SET" & "" & NullToStr(sTotalStr)
    sConsulta = sConsulta & " WHERE ID=" & m_iId & " AND ACT1=" & m_iACN1 & " AND ACT2=" & m_iACN2 & ""
    m_oConexion.AdoCon.Execute sConsulta
    
    
    Set IBaseDatos_FinalizarEdicionModificando = TESError
    
    Exit Function
    
Error:
    
    Set IBaseDatos_FinalizarEdicionModificando = basErrores.TratarError(m_oConexion.AdoCon.Errors)


End Function


Private Function IBaseDatos_IniciarEdicion(Optional ByVal Bloquear As Boolean, Optional ByVal UsuarioBloqueo As String, Optional ByVal sIdioma As String) As CTESError
Dim TESError As CTESError
Dim rs As New ADODB.Recordset
Dim sDen As String

Set TESError = New CTESError
TESError.NumError = TESnoerror
'******************* Precondicion *******************
If m_oConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CActividadNivel3.IniciarEdicion", "No se ha establecido la conexion"
End If
'*****************************************************
    
    sDen = "DEN_" & sIdioma
    
    rs.Open ("SELECT ID,COD,FECACT," & NullToStr(sDen) & " FROM ACT3 WITH (NOLOCK) WHERE ID=" & m_iId & " AND ACT1=" & m_iACN1 & " AND ACT2=" & m_iACN2 & ""), m_oConexion.AdoCon, adOpenStatic, adLockReadOnly
    
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 123 '
        Set IBaseDatos_IniciarEdicion = TESError
        Exit Function
    End If

    m_iId = rs("ID").Value
    m_sCod = rs("COD").Value
    m_vFecAct = rs("FECACT").Value
    m_sDen = rs(sDen).Value
    
    Set IBaseDatos_IniciarEdicion = TESError
    
End Function
Public Function DevolverDenominaciones(Cod() As String) As ADODB.Recordset

Dim ador As ADODB.Recordset
Dim sConsulta As String
Dim i As Integer
Dim sCod As String

    While i < UBound(Cod()) + 1
        sCod = sCod & " DEN_" & Cod(i) & ","
        i = i + 1
    Wend
    
    sCod = Left(sCod, Len(sCod) - 1)

sConsulta = "SELECT " & sCod & " FROM ACT3 WITH (NOLOCK) WHERE ID = " & m_iId & " AND ACT1=" & m_iACN1 & " AND ACT2=" & m_iACN2 & ""

Set ador = New ADODB.Recordset
    
ador.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
Set ador.ActiveConnection = Nothing

Set DevolverDenominaciones = ador

End Function
Public Function DevolverCiasAsignadas() As ADODB.Recordset
Dim sConsulta As String
Dim adoRecordset As ADODB.Recordset

Set adoRecordset = New ADODB.Recordset

sConsulta = "SELECT DISTINCT CIAS.COD,CIAS.DEN FROM CIAS WITH (NOLOCK) INNER JOIN CIAS_ACT3 WITH (NOLOCK) ON CIAS.ID=CIAS_ACT3.CIA"
sConsulta = sConsulta & " WHERE ACT1= " & m_iACN1 & "AND ACT2= " & m_iACN2 & ""
adoRecordset.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

Set adoRecordset.ActiveConnection = Nothing

If adoRecordset.RecordCount = 0 Then
    Set DevolverCiasAsignadas = Nothing
Else
    Set DevolverCiasAsignadas = adoRecordset
End If
End Function



