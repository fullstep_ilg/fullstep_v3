VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTiposDeDatos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public Enum ErroresSummit

    TESnoerror = 0
    TESUsuarioNoValido = 1
    TESDatoDuplicado = 2
    TESDatoEliminado = 3
    TESDatoNoValido = 4
    TESInfModificada = 5
    TESFaltanDatos = 6
    TESImposibleEliminar = 7
    tesCodigoProcesoCambiado = 8 'No es un error en si. Indica que al dar de alta el numero mostrado de proceso ha cambiado.
    
    TESConexionServidorFailed = 9 ' *** ERRORES EN SQLDMO (LINKED SERVER )
    TESDesconexionServidorFailed = 10
    TESLinkedServerCreateError = 11
    TESLinkedServerAddError = 12
    TESLinkedServerSetOptionsError = 13
    TESLinkedLoginCreateError = 14
    TESLinkedLoginAddError = 15
    TESConexionRemotoFailed = 16
    TESDesconexionRemotoFailed = 17
    TESRemotoConfigError = 18
    TESLinkedServerDeleteError = 19
    TESLinkedLoginDeleteError = 20
    TESOtroErrorEnLink = 21
    
    TESCambioContrasenya = 30
    tesprocesocerrado = 31 ' Al intentar modificar o eliminar un proceso que ha pasado a estado cerrado.
    tesimposiblecambiarfecha = 32 'Al intentar cambiar la fecha de una reuni�n
    TESProcesoBloqueado = 33 'Al intentar iniciar la edicion del proceso bloqueandolo
    TESImposibleEliminarAsignacion = 34 'Al intentar desasignar un proveedore al que ya se le han enviado peticiones
    TESImposibleExcluirItem = 35 ' Al intentar excluir un item que ya tiene adjudicaciones
    TESImposibleAdjudicarAProveNoHom = 36 ' Al intentar validarycerrar un proceso adjudicando a proveedores no homologados
    TESImposibleModificarReunion = 37 'Al intentar modificar una reuni�n que ya ha tenido algun resultado. (No al a�adir procesos nuevos a esa reuni�n)
    TESImposibleAsignarProveedor = 38 'Al intentar asignar un proveedor que ya est� asignado
    TESInfActualModificada = 39 'Cuando iniciamos la edici�n de una fila y vemos que los datos actuales han cambiado
    'TESImposiblePreadjudicar = 40 'El proceso ya ha tenido resultados en una reuni�n. Ya no podemos preadjudicar
    TESMail = 40 'Para tratamiento de errores de envio de mail
    TESProvePortalDesautorizado = 41
    TESciaportaldesautorizada = 42
    TESImposibleConectarALaCia = 43
    TESPaisYaRelacionadoEnGS = 44
    TESProveYaRelacionadoEnGS = 45
    TESNIFRepetido = 46
    TESNIFProveRepetido = 47
    TESTelfNoValido = 48
    TESEMailNoValido = 49
    
    TESASPUser = 72
    TESmailsmtp = 73
    TESRunAsUser = 74
    TESASPUserCuentaExiste = 75
    TESASPUserAddUser = 76
    TESASPUserPropiedades = 77
    TESASPUserRemoteGrupo = 78
    TESADSIExProgramaIni = 79
    TESADSIExHomeDir = 80
    TESASPUserCambiaPwd = 81
    TESASPUserRename = 82
    TESASPUserRemove = 83
    
    TESOtroerror = 100
    TESOtroErrorNoBD = 200
    TESErrorSharePoint = 300
        
    TESErrorErrorServicio = 10000
    TESErrorPWDCorta = 10001
    TESErrorPWDEdadMin = 10002
    TESErrorPWDCondicionesComplejidad = 10003
    TESErrorPWDHistorico = 10004
End Enum

Public Enum TipoOrdenacionEnlaces
    OrdPorCompCod = 1
    OrdPorCompDen = 2
    OrdPorProvePCod = 3
    OrdPorProvePDen = 4
    OrdPorProveGSCod = 5
    OrdPorPremActivo = 6

End Enum

Public Type TipoErrorSummit
    NumError As ErroresSummit
    Arg1 As Variant
    Arg2 As Variant
End Type

Public Type TipoResultadoConexion
    
    ID As Integer
    Cod As String
    Den As String
    Res As Boolean
    FSGS_SRV As String
    FSGS_BD As String
    
End Type


Public Type ParametrosGenerales
    g_bCollab As Boolean
    g_sServerName As String
    g_sBDName As String
    g_sGrupo As String
    g_sIPSrv As String
    g_sIPWeb As String
    g_sIPMapeo As String
    g_sServerGroup As String
    g_sWebSPTS As String
    
    
    g_sIPSrvDMO As String
    
    g_iNotifAut As Integer
    g_iNotifDesaut As Integer
    g_iNotifCambAct As Integer
    g_iNotifRegCias As Integer
    g_iConfirmAut As Integer
    g_iConfirmDesaut As Integer
    g_iConfirmCambAct As Integer
    g_iConfirmRegCias As Integer
        
    g_sNomPortal As String          'Nombre del portal
    g_sUrl As String                'URL
    g_iMonedaCentral As Integer     'Moneda central
    g_lCargaMaxima As Long
    
    g_sAdm As String
    g_sEmail As String
    g_iTipoEmail As Integer
    g_iNotifSolicReg As Integer
    g_iNotifSolicAct As Integer
    g_iNotifSolicComp As Integer
    g_sMailPath As String
    g_sRptPath As String
    
    
    g_bPremium As Boolean           'Soporta proveedores premium
    g_dblMaxAdjun As Double         'Tama�o max. adjuntos
    g_iNivelMinAct As Integer       'Nivel m�nimo de actividad
    g_sIdioma As String

    'Asuntos/Subjects de los mails
    g_sNotifAutSPA As String
    g_sNotifAutENG As String
    g_sNotifAutGER As String
    g_sNotifAutFRA As String
    g_sNotifDesautSPA As String
    g_sNotifDesautENG As String
    g_sNotifDesautGER As String
    g_sNotifDesautFRA As String
    g_sNotifConfirmCambActSPA As String
    g_sNotifConfirmCambActENG As String
    g_sNotifConfirmCambActGER As String
    g_sNotifRechazoCambActSPA As String
    g_sNotifRechazoCambActENG As String
    g_sNotifRechazoCambActGER As String
    g_sNotifConfirmRegCiasSPA As String
    g_sNotifConfirmRegCiasENG As String
    g_sNotifConfirmRegCiasGER As String
    g_sNotifRechazoRegCiasSPA As String
    g_sNotifRechazoRegCiasENG As String
    g_sNotifRechazoRegCiasGER As String
    g_sNotifSolicRegSPA As String
    g_sNotifSolicRegENG As String
    g_sNotifSolicRegGER As String
    g_sNotifSolicActSPA As String
    g_sNotifSolicActENG As String
    g_sNotifSolicActGER As String
    g_sNotifSolicCompSPA As String
    g_sNotifSolicCompENG As String
    g_sNotifSolicCompGER As String
    g_bUnaCompradora As Boolean
    g_lIdCiaCompradora As Long
    g_sFSBDCiaCompradora As String
    g_sFSSRVCiaCompradora As String
    g_sNotifDesbloqueoUsuarioSPA As String
    g_sNotifDesbloqueoUsuarioENG As String
    g_sNotifDesbloqueoUsuarioGER As String
    
    g_lPais As Long
    
    
    gbMostrarMail As Boolean    'Mostrar Mail antes de enviar
    gbAcuseRecibo As Boolean    'Pedir acuse de recibo
    gsSMTPServer As String
    giSMTPPort As Integer
    gbSMTPSSL As Boolean
    giSMTPAutent As Integer     '0 - No requiere, 1 - Basica, 2 - Segura
    gbSMTPBasicaGS As Boolean   'True - Basica cod y pwd GS, False - Basica otro cod y pwd
    gsSMTPUser   As String
    gsSMTPPwd   As String
    gbSMTPUsarCuenta   As Boolean ' 0 Cuenta com�n,  1 Cuenta del usuario
    gsSMTPCuentaMail   As String
    gsSMTPCC   As String
    gsSMTPCCO   As String
    gsSMTPRespuestaMail   As String
    giSMTPMetodoEntrega As Integer
    gsSMTPDominio   As String
    
    gbCompruebaNIF As Boolean
    gbBloqModifProve As Boolean
    gbSincronizacion As Boolean
    gbCompruebaTfno As Boolean
    g_dblMaxAdjunCias As Double         'Tama�o max. adjuntos compa��as
    g_bPymes As Boolean                 'Entorno On Demand o Pymes
    
    g_iIdIdioma As Integer
    
    g_bAccesoFSGA As Boolean    'Acceso funcionalidad FSGA
    g_iMaxActividades As Integer 'N�mero m�ximo de actividades
    g_sMARCADOR_CARPETA_PLANTILLAS As String
End Type

Public Enum AccionesPS
    ACCACT1Anya = 0
    ACCACT1Mod = 1
    ACCACT1Eli = 2
    ACCACT1Det = 3
    ACCACT2Anya = 4
    ACCACT2Mod = 5
    ACCACT2Eli = 6
    ACCACT2Det = 7
    ACCACT3Anya = 8
    ACCACT3Mod = 9
    ACCACT3Eli = 10
    ACCACT3Det = 11
    ACCACT4Anya = 12
    ACCACT4Mod = 13
    ACCACT4Eli = 14
    ACCACT4Det = 15
    ACCACT5Mod = 16
    ACCACT5Eli = 17
    ACCACT5Det = 18
    ACCACT5Anya = 19
End Enum

Public Enum TipoAccion
    Login_Usu = 10
    Cia_Sel = 20
    Cia_Anyadir = 30
    Cia_Eliminar = 40
    Cia_Modif_DatosGen = 50
    Cia_Modif_Acceso = 60
    Cia_Enlazar_GS = 70
    Cia_Desenlazar_GS = 80
    Cia_Modif_Cod = 90
    Cia_Modif_Activ = 100
    Cia_Modif_Obs = 110
    Cia_Archivo_Anyadir = 120
    Cia_Archivo_Eliminar = 130
    Cia_Archivo_Modif = 140
    Cia_Archivo_GuardarEnDisco = 150
    Cia_Archivo_Abrir = 160
    Usu_Anyadir = 170
    Usu_Eliminar = 180
    Usu_Modif_Datos = 190
    Usu_Modif_Acceso = 200
    Usu_Modif_Principal = 210
    Activ_Sel_Cia = 220
    Activ_Confirmar = 230
    Activ_Deshacer = 240
    RegMail_Buscar_Notif_Cia = 250
    RegMail_Ver_Notif_Cia = 260
    RegMail_Reenviar_Notif_Cia = 270
    Param_Acceso = 280
    Param_Modif = 290
    Cambio_Pwd = 300
End Enum

Public Enum TipoOrdenAccion
    None = 0
    col_fecha = 1
    Col_Usu = 2
    Col_Accion = 3
    Col_CiaCod = 4
    Col_CiaDen = 5
    Col_Dato = 6
End Enum
