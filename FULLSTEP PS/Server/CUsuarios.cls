VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUsuarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_oConexion As CConexion
Private m_col As Collection

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Function Add(ByVal Cia As Long, ByVal ID As Long, ByVal Cod As String, ByVal Nombre As String, ByVal Apellidos As String, ByVal idioma As Integer, ByVal IdiomaDen As String, ByVal EstadoFC As Integer, ByVal EstadoFP As Variant, ByVal SPTSID As Variant, ByVal EMAIL As Variant, Optional ByVal pvTipoMail As Variant, Optional ByVal fecpwd As Date, Optional ByVal lIDCategoria As Long, Optional ByVal bAccesoFSGA As Boolean = False, Optional sPWD_HASH As String, Optional bBLOQ As Boolean) As CUsuario
    
    ''' * Objetivo: Anyadir un usuario a la coleccion
    ''' * Recibe: Datos del Usuario
    ''' * Devuelve: Usuario anyadido
    
    Dim objnewmember As CUsuario
    
    ''' Creacion de objeto Usuario
    
    Set objnewmember = New CUsuario
   
    ''' Paso de los parametros al nuevo objeto Usuario
    objnewmember.Cia = Cia
    objnewmember.ID = ID
    objnewmember.Cod = Cod
    objnewmember.Nombre = Nombre
    objnewmember.Apellidos = Apellidos
    objnewmember.idioma = idioma
    objnewmember.IdiomaDen = IdiomaDen
    objnewmember.EstadoComp = EstadoFC
    If IsNull(EstadoFP) Or IsMissing(EstadoFP) Then
        objnewmember.EstadoProv = 0
    Else
        objnewmember.EstadoProv = EstadoFP
    End If
    If IsNull(SPTSID) Or IsMissing(SPTSID) Then
        objnewmember.IDSPTS = 0
    Else
        objnewmember.IDSPTS = SPTSID
    End If
    If IsNull(EMAIL) Or IsMissing(EMAIL) Then
        objnewmember.EMAIL = ""
    Else
        objnewmember.EMAIL = EMAIL
    End If

    objnewmember.fecpwd = fecpwd

    If IsNull(pvTipoMail) Or IsMissing(pvTipoMail) Then
        objnewmember.TipoMail = ""
    Else
        objnewmember.TipoMail = pvTipoMail
    End If
    If IsNull(lIDCategoria) Or IsMissing(lIDCategoria) Then
        objnewmember.IDCategoria = 0
    Else
        objnewmember.IDCategoria = lIDCategoria
    End If
    objnewmember.AccesoFSGA = bAccesoFSGA
    objnewmember.PWD_HASH = sPWD_HASH
    objnewmember.UsuarioBloqueado = bBLOQ
    
    Set objnewmember.Conexion = m_oConexion
    
    m_col.Add objnewmember, CStr(ID)
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function

Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar un Usuario de la coleccion
    ''' * Recibe: Indice del Usuario a eliminar
    ''' * Devuelve: Nada
   
    m_col.Remove vntIndexKey
    
End Sub

Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If m_col Is Nothing Then
        Count = 0
    Else
        Count = m_col.Count
    End If

End Property

Public Property Get Item(vntIndexKey As Variant) As CUsuario

    ''' * Objetivo: Recuperar un usuario de la coleccion
    ''' * Recibe: Indice del usuario a recuperar
    ''' * Devuelve: usuario correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = m_col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = m_col.[_NewEnum]

End Property

Public Function DevolverDatosUsuario(ByVal Cia As Long, ByVal Usu As Long) As adodb.Recordset
    Dim ador As adodb.Recordset
    Dim sConsulta As String
    
    sConsulta = "SELECT USU.ID,USU.COD,USU.APE,USU.NOM,USU.DEP,USU.CAR,USU.BLOQ,USU.TFNO,USU.TFNO2,USU.TFNO_MOVIL,PWD_HASH,"
    sConsulta = sConsulta & "USU.FAX,USU.EMAIL,IDI.ID AS IDIID,IDI.COD AS IDICOD,IDI.DEN AS IDIDEN,BLOQ,USU.NIF "
    sConsulta = sConsulta & "FROM USU WITH (NOLOCK) INNER JOIN IDI WITH (NOLOCK) ON USU.IDI=IDI.ID "
    sConsulta = sConsulta & "WHERE USU.ID = " & Usu & " And Usu.Cia = " & Cia
        
    Set ador = New adodb.Recordset
    
    ador.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    Set ador.ActiveConnection = Nothing
    Set DevolverDatosUsuario = ador
End Function

Public Sub CargarTodosLosUsuariosDeLaCiaDesde(ByVal NumMaximo As Integer, ByVal Cia As Long, Optional ByVal OrdenarPorDen As Boolean, Optional ByVal CarIniUsuCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean)

    ''' * Objetivo: Cargar los usuarios de una compañíahasta un numero maximo
    ''' * Objetivo: desde un codigo o una denominacion determinadas
    
    Dim ador As adodb.Recordset
    Dim sConsulta As String
    Dim sPrimeraCond As String
    Dim sPrimerOrden As String
    
    Dim lIndice As Long
    Dim iNumUsu As Integer
    
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        ' Err.Raise vbObjectError "CUsuarios.CargarTodosLosUsuariosDeLaCiaDesde", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    ''' Generacion del SQL a partir de los parametros
    sConsulta = "SELECT USU.*,USU_PORT.FPEST,IDI.DEN FROM USU WITH (NOLOCK) LEFT JOIN USU_PORT WITH (NOLOCK) ON USU.CIA = USU_PORT.CIA AND USU.ID = USU_PORT.USU AND USU.PORT = USU_PORT.PORT "
    sConsulta = sConsulta & " INNER JOIN IDI WITH (NOLOCK) ON USU.IDI = IDI.ID WHERE USU.CIA = " & Cia & " AND USU.PORT = " & basParametros.IdPortal
    If CarIniUsuCod = "" And CarIniDen = "" Then
    Else
        If Not (CarIniUsuCod = "") And Not (CarIniDen = "") Then
            If CoincidenciaTotal Then
                sConsulta = sConsulta & " AND COD ='" & DblQuote(CarIniUsuCod) & "'"
                sConsulta = sConsulta & " AND APE ='" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta = sConsulta & " AND COD >='" & DblQuote(CarIniUsuCod) & "'"
                sConsulta = sConsulta & " AND APE >='" & DblQuote(CarIniDen) & "'"
            End If
        Else
            If Not (CarIniUsuCod = "") Then
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & " AND COD ='" & DblQuote(CarIniUsuCod) & "'"
                Else
                    sConsulta = sConsulta & " AND COD >='" & DblQuote(CarIniUsuCod) & "'"
                End If
            Else
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & " AND APE ='" & DblQuote(CarIniDen) & "'"
                Else
                    sConsulta = sConsulta & " AND APE >='" & DblQuote(CarIniDen) & "'"
                End If
            End If
        End If
    End If
    Set ador = New adodb.Recordset
    Set m_col = Nothing
    Set m_col = New Collection
    ador.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    If ador.eof Then
        Set ador = Nothing
        Exit Sub
    End If
    iNumUsu = 0
    
    While Not ador.eof And iNumUsu < NumMaximo
            Me.Add Cia, ador("ID").Value, ador("COD").Value, ador("NOM").Value, ador("APE").Value, ador("IDI").Value, ador("DEN").Value, ador("FCEST").Value, ador("FPEST").Value, ador("SPTS_ID").Value, ador("EMAIL").Value
            iNumUsu = iNumUsu + 1
            ador.MoveNext
    Wend
    ador.Close
    Set ador = Nothing
    
End Sub

Private Sub Class_Initialize()
    Set m_col = New Collection
End Sub

Private Sub Class_Terminate()
    Set m_col = Nothing
    Set m_oConexion = Nothing
End Sub


