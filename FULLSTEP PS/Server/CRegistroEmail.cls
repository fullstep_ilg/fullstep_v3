VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRegistroEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mvarIndice As Long 'Nos indica la posicion secuencial dentro de una Coleccion
Private m_oConexion As CConexion
Private mvarId As Long
Private mvarCia As Long
Private mvarFecha As Date
Private mvarTipo As Integer
Private mvarSubject As Variant
Private mvarPara As Variant
Private mvarCC As Variant
Private mvarCCo As Variant
Private mvarResponderA As Variant
Private mvarCuerpo As Variant
Private mvarHayAdjuntos As Boolean
Private mvarAcuseRecibo As Integer
Private m_oAdjuntos As CAdjuntosEmail
Private mvarProducto As Integer
Private mvarUsuario As Variant 'Long en bbdd
Private mvarKO As Boolean
Private mvarTextoError As String

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Property Set Adjuntos(ByVal oAdjun As CAdjuntosEmail)
    Set m_oAdjuntos = oAdjun
End Property
Public Property Get Adjuntos() As CAdjuntosEmail
    Set Adjuntos = m_oAdjuntos
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get ID() As Long
     ID = mvarId
End Property
Public Property Let ID(ByVal ident As Long)
    mvarId = ident
End Property

Public Property Get Cia() As Long
     Cia = mvarCia
End Property
Public Property Let Cia(ByVal Comp As Long)
    mvarCia = Comp
End Property

Public Property Get Fecha() As Date
     Fecha = mvarFecha
End Property
Public Property Let Fecha(ByVal Fec As Date)
    mvarFecha = Fec
End Property

Public Property Get Tipo() As Integer
     Tipo = mvarTipo
End Property
Public Property Let Tipo(ByVal Data As Integer)
     mvarTipo = Data
End Property

Public Property Get AcuseRecibo() As Integer
     AcuseRecibo = mvarAcuseRecibo
End Property
Public Property Let AcuseRecibo(ByVal Data As Integer)
     mvarAcuseRecibo = Data
End Property

Public Property Get Subject() As Variant
     Subject = mvarSubject
End Property
Public Property Let Subject(ByVal Data As Variant)
     mvarSubject = Data
End Property

Public Property Get Para() As Variant
     Para = mvarPara
End Property
Public Property Let Para(ByVal Data As Variant)
     mvarPara = Data
End Property

Public Property Get CC() As Variant
     CC = mvarCC
End Property
Public Property Let CC(ByVal Data As Variant)
     mvarCC = Data
End Property

Public Property Get CCO() As Variant
     CCO = mvarCCo
End Property
Public Property Let CCO(ByVal Data As Variant)
     mvarCCo = Data
End Property

Public Property Get ResponderA() As Variant
     ResponderA = mvarResponderA
End Property
Public Property Let ResponderA(ByVal Data As Variant)
     mvarResponderA = Data
End Property

Public Property Get Cuerpo() As Variant
     Cuerpo = mvarCuerpo
End Property
Public Property Let Cuerpo(ByVal Data As Variant)
     mvarCuerpo = Data
End Property

Public Property Get HayAdjuntos() As Boolean
     HayAdjuntos = mvarHayAdjuntos
End Property
Public Property Let HayAdjuntos(ByVal Data As Boolean)
     mvarHayAdjuntos = Data
End Property

''' <summary>
''' A�adir comunicaci�n
''' Grabar un error en una comunicaci�n si Hay KO
''' </summary>
''' <param name="sTemp">Directorio temporal</param>
''' <param name="Pagina">Funci�n o pantalla q da el error</param>
''' <param name="NumError">Numero de Error VB. El Id de la tabla va en teserror.arg1</param>
''' <param name="Message">Mensaje del error VB</param>
''' <returns>Un error si lo hay</returns>
''' <remarks>Llamada desde: frmComunicacionesPorFechas.sdbgComunicacion_BeforeUpdate
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 18/01/2011</revision>
Public Function AnyadirComunicacion(ByVal sTemp As String, Optional ByVal Pagina As String = "" _
, Optional ByVal NumError As String = "", Optional ByVal Message As String = "") As TipoErrorSummit
    Dim ador As adodb.Recordset
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim oAdjun As CAdjuntoEmail
    Dim adoCom As adodb.Command
    Dim adoPar As adodb.Parameter
    Dim oStream As adodb.Stream
    Dim chunk As Variant
    Dim iAdjun As Integer
    Dim lIdError As Long
    Dim sFSGS As String
    
    TESError.NumError = TESnoerror
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRegistroEmail.AnyadirComunicacion", "No se ha establecido la conexion"
    End If
    ' *****************************************

On Error GoTo Error:

    m_oConexion.AdoCon.Execute "BEGIN TRANSACTION"
    m_oConexion.AdoCon.Execute "SET XACT_ABORT ON"
    
    'Selecciono el id de la comunicaci�n
        'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
    sConsulta = "SELECT MAX(ID) FROM REGISTRO_EMAIL WHERE CIA=" & mvarCia
    Set ador = New adodb.Recordset
    ador.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    If IsNull(ador(0).Value) Then
        mvarId = 1
    Else
        mvarId = ador(0).Value + 1
    End If
    
    ador.Close
    Set ador = Nothing

    lIdError = -1
    If mvarKO And (Pagina <> "") Then
        Set ador = New adodb.Recordset
        ador.Open "SELECT TOP 1 FSGS_SRV,FSGS_BD FROM CIAS WITH(NOLOCK) WHERE CIAS.FCEST=3 AND FSGS_BD IS NOT NULL", m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
        If Not ador.eof Then
            If IsNull(ador("FSGS_SRV").Value) Then
                sFSGS = ador("FSGS_BD").Value & ".dbo."
            Else
                sFSGS = ador("FSGS_SRV").Value & "." & ador("FSGS_BD").Value & ".dbo."
            End If
            ador.Close
            Set ador = Nothing
            
            Pagina = "FSPS: " & Pagina
        
            sConsulta = "INSERT INTO " & sFSGS & "ERRORES (FEC_ALTA,PAGINA,USUARIO,EX_FULLNAME,EX_MESSAGE)"
            sConsulta = sConsulta & " VALUES(GETDATE()," & StrToSQLNULL(Pagina) & "," & StrToSQLNULL(mvarUsuario) & "," & StrToSQLNULL(NumError) & "," & StrToSQLNULL(Message) & ")"
            
            m_oConexion.AdoCon.Execute sConsulta
            
            Set ador = New adodb.Recordset
            'Buscamos el numero de id maximo
            'CALIDAD: Sin WITH (NOLOCK) porque es para obtener el nuevo ID
            ador.Open "SELECT MAX(ID) AS NUM FROM " & sFSGS & "ERRORES", m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
            If ador.eof Then
            Else
                lIdError = NullToDbl0(ador(0).Value)
                ador.Close
            End If
            Set ador = Nothing
        End If
        
        TESError.Arg1 = lIdError
    End If
    

    'Realizo la inserci�n
    sConsulta = "INSERT INTO REGISTRO_EMAIL (CIA,ID,FECHA,SUBJECT,PARA,CC,CCO,DIR_RESPUESTA,TIPO,CUERPO,ACUSE_RECIBO,PRODUCTO,USU,KO,TEXTO_ERROR,ID_ERROR) VALUES "
    sConsulta = sConsulta & "(" & mvarCia & "," & mvarId & ",GETDATE()," & StrToSQLNULL(mvarSubject) & "," & StrToSQLNULL(mvarPara)
    sConsulta = sConsulta & "," & StrToSQLNULL(mvarCC) & "," & StrToSQLNULL(mvarCCo) & "," & StrToSQLNULL(mvarResponderA)
    sConsulta = sConsulta & "," & mvarTipo & "," & StrToSQLNULL(mvarCuerpo) & "," & mvarAcuseRecibo
    sConsulta = sConsulta & "," & mvarProducto & "," & StrToSQLNULL(mvarUsuario) & "," & IIf(mvarKO, 1, 0) & "," & StrToSQLNULL(mvarTextoError) & ""
    sConsulta = sConsulta & "," & IIf(lIdError = -1, "NULL", CStr(lIdError)) & ")"
    m_oConexion.AdoCon.Execute sConsulta
    
    
    Set oStream = New adodb.Stream
    'Si hay adjuntos los a�ado:
    If Not m_oAdjuntos Is Nothing Then
        For Each oAdjun In m_oAdjuntos
                        'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
            sConsulta = "SELECT MAX(ID) FROM REGISTRO_EMAIL_ADJUN WHERE CIA=" & mvarCia & " AND ID_REG= " & mvarId
            Set ador = New adodb.Recordset
            ador.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
            If IsNull(ador.Fields(0)) Then
                iAdjun = 1
            Else
                iAdjun = NullToDbl0(ador.Fields(0)) + 1
            End If
                       
            ador.Close
            Set ador = Nothing
            
            
            oStream.Open
            oStream.LoadFromFile (sTemp & oAdjun.Nombre)
        
            sConsulta = "INSERT INTO REGISTRO_EMAIL_ADJUN (ID_REG,CIA,ID,DATA,NOM) VALUES "
            sConsulta = sConsulta & "(" & mvarId & "," & mvarCia & "," & iAdjun & ",?,'" & DblQuote(oAdjun.Nombre)
            sConsulta = sConsulta & "')"
            Set adoCom = New adodb.Command
            Set adoCom.ActiveConnection = m_oConexion.AdoCon
            Set adoPar = adoCom.CreateParameter("DATA", adBinary, adParamInput, oAdjun.DataSize, Null)
            adoCom.Parameters.Append adoPar
            adoCom.CommandText = sConsulta
            
            chunk = oStream.ReadText()
            
            adoCom.Parameters.Item("DATA").AppendChunk (chunk)
            oStream.Close
            
            adoCom.Execute
            Set adoCom = Nothing
            Set adoPar = Nothing
        Next
    End If
    
    m_oConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    m_oConexion.AdoCon.Execute "COMMIT TRANSACTION"
    
    AnyadirComunicacion = TESError
    
    Exit Function
    
Error:
    m_oConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    AnyadirComunicacion = basErrores.TratarError(m_oConexion.AdoCon.Errors)
    
End Function

Private Sub Class_Terminate()
    Set m_oConexion = Nothing
    Set m_oAdjuntos = Nothing
End Sub



Public Sub CargarTodosLosAdjuntos(Optional ByVal bUsarIndice As Boolean)

'*******************************************************************************************
'*** Descripci�n: Carga en la propiedad Adjuntos los adjuntos del hist�rico.
'*** Par�metros:
'***              bUsarIndice, cargar la colecci�n de especificaciones con �ndice o no
'*******************************************************************************************

Dim adores As adodb.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim adofldNom As adodb.Field
Dim adofldFecAct As adodb.Field
Dim adofldId As adodb.Field

'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRegistroEmail.CargarTodosLosAdjuntos", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************

    sConsulta = "SELECT ID,NOM,FECACT"
    sConsulta = sConsulta & " FROM REGISTRO_EMAIL_ADJUN WITH (NOLOCK) "
    sConsulta = sConsulta & " WHERE CIA=" & mvarCia & " AND ID_REG=" & mvarId
    sConsulta = sConsulta & " ORDER BY FECACT"
    
    Set adores = New adodb.Recordset
    adores.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    If adores.eof Then
            
        adores.Close
        Set adores = Nothing
        Set m_oAdjuntos = Nothing
        Set m_oAdjuntos = New CAdjuntosEmail
        Set m_oAdjuntos.Conexion = m_oConexion
        Exit Sub
          
    Else
        
        Set m_oAdjuntos = Nothing
        Set m_oAdjuntos = New CAdjuntosEmail
        Set m_oAdjuntos.Conexion = m_oConexion
        
        Set adofldNom = adores.Fields("NOM")
        Set adofldFecAct = adores.Fields("FECACT")
        Set adofldId = adores.Fields("ID")
        
        If bUsarIndice Then
            
            lIndice = 0
                   
            While Not adores.eof
                m_oAdjuntos.Add Me, adofldId.Value, adofldNom.Value, adofldFecAct.Value, lIndice
                adores.MoveNext
                lIndice = lIndice + 1
            Wend
        
        Else
                   
            While Not adores.eof
                m_oAdjuntos.Add Me, adofldId.Value, adofldNom.Value, adofldFecAct.Value
                adores.MoveNext
            Wend
            
        End If
        
        adores.Close
        Set adores = Nothing
        Set adofldNom = Nothing
        Set adofldFecAct = Nothing
        Set adofldId = Nothing
          
    End If

End Sub

''' <summary>Carga los datos secundarios del mensaje</summary>
''' <remarks>Llamada desde: frmRegComunicaciones Tiempo m�ximo: <1 seg</remarks>

Public Sub CargarDetalleMensaje()
    Dim sConsulta As String
    Dim adoComm As adodb.Command
    Dim adoParam As Parameter
    Dim rs As adodb.Recordset

    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRegistroEmail.CargarTodosLosAdjuntos", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    sConsulta = "SELECT DIR_RESPUESTA,ACUSE_RECIBO,CUERPO FROM REGISTRO_EMAIL WITH (NOLOCK) WHERE CIA=? AND ID=?"
   
    Set adoComm = New adodb.Command
    With adoComm
        Set .ActiveConnection = m_oConexion.AdoCon
        .CommandText = sConsulta
        .CommandType = adCmdText
        .Prepared = True
        
        Set adoParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=mvarCia)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=mvarId)
        .Parameters.Append adoParam
        
        Set rs = .Execute
        If Not rs Is Nothing Then
            If rs.RecordCount > 0 Then
                mvarCuerpo = rs("CUERPO")
                mvarAcuseRecibo = rs("ACUSE_RECIBO")
                mvarResponderA = rs("DIR_RESPUESTA")
            End If
        End If
    End With
    
    rs.Close
    Set rs = Nothing
    Set adoParam = Nothing
    Set adoComm = Nothing
End Sub


Public Function CargarObjDetalleMensaje(ByVal lCia As Long, ByVal lID As Long) As CRegistroEmail
    Dim sConsulta As String
    Dim adoComm As adodb.Command
    Dim adoParam As Parameter
    Dim rs As adodb.Recordset

    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CRegistroEmail.CargarTodosLosAdjuntos", "No se ha establecido la conexion"
        Exit Function
    End If
        
        sConsulta = "SELECT RE.CIA,RE.ID,CONVERT(varchar(23),RE.FECHA,21) AS FECHA,ISNULL(RE.PARA,'') AS PARA,"
        sConsulta = sConsulta & " ISNULL(RE.Subject,'') AS SUBJECT,ISNULL(RE.CC,'') AS CC,ISNULL(RE.CCO,'') AS CCO,RE.TIPO,"
        sConsulta = sConsulta & " ISNULL(ADJUN.ADJUNTOS,0) AS ADJUNTOS"
        sConsulta = sConsulta & " FROM REGISTRO_EMAIL RE WITH (NOLOCK) "
        sConsulta = sConsulta & " LEFT JOIN (SELECT CIA,ID_REG,CASE WHEN COUNT(*)>0 THEN 1 ELSE 0 END AS ADJUNTOS"
        sConsulta = sConsulta & "            FROM REGISTRO_EMAIL_ADJUN REA WITH (NOLOCK) "
        sConsulta = sConsulta & "            GROUP BY REA.CIA,REA.ID_REG) ADJUN ON ADJUN.CIA = RE.CIA AND ADJUN.ID_REG=RE.ID"
        sConsulta = sConsulta & " WHERE RE.CIA=? AND RE.ID=?"
        
        
    Set adoComm = New adodb.Command
    With adoComm
        Set .ActiveConnection = m_oConexion.AdoCon
        .CommandText = sConsulta
        .CommandType = adCmdText
        .Prepared = True
        
        Set adoParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lCia)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lID)
        .Parameters.Append adoParam
        
        Set rs = .Execute
        If Not rs Is Nothing Then
            If rs.RecordCount > 0 Then
            
                 Dim objnewmember As CRegistroEmail
                 
                 ''' Creacion de objeto Comunicacion
                 
                 Set objnewmember = New CRegistroEmail
                
                 ''' Paso de los parametros al nuevo objeto comunicacion
                 objnewmember.Cia = rs("CIA").Value
                 objnewmember.ID = rs("ID").Value
                 Dim dtFecha As Date
                 dtFecha = CDate(Left(rs("FECHA").Value, Len(rs("FECHA").Value) - 4))
                 objnewmember.Fecha = dtFecha
                 objnewmember.Tipo = rs("TIPO").Value
                 objnewmember.Para = rs("PARA").Value
                 objnewmember.HayAdjuntos = IIf(rs("ADJUNTOS").Value > 0, True, False)
                 
                 If Not IsMissing(Subject) Then
                     objnewmember.Subject = rs("SUBJECT").Value
                 Else
                     objnewmember.Subject = Null
                 End If
                 
                 If Not IsMissing(CC) Then
                     objnewmember.CC = rs("CC").Value
                 Else
                     objnewmember.CC = Null
                 End If
                 
                 If Not IsMissing(CCO) Then
                     objnewmember.CCO = rs("CCO").Value
                 Else
                     objnewmember.CCO = Null
                 End If
                 
                 'If Not IsMissing(RespuestaA) Then
                 '    objnewmember.ResponderA = rs("PARA").Value
                 'Else
                 '    objnewmember.ResponderA = Null
                 'End If
                 
                 'If Not IsMissing(Cuerpo) Then
                 '    objnewmember.Cuerpo = rs("CUERPO").Value
                 'Else
                 '    objnewmember.Cuerpo = Null
                 'End If
                 
                 'objnewmember.AcuseRecibo = AcuseRecibo
                 'objnewmember.Producto = Producto
                 'objnewmember.Usuario = Usuario
                 'objnewmember.KO = KO
                 'objnewmember.TextoError = "" & TextoError
                 
                 Set objnewmember.Conexion = m_oConexion
                             
                 Set CargarObjDetalleMensaje = objnewmember
                 
                 Set objnewmember = Nothing

            End If
        End If
    End With
    
    rs.Close
    Set rs = Nothing
    Set adoParam = Nothing
    Set adoComm = Nothing
End Function

Public Property Get Producto() As Integer
     Producto = mvarProducto
End Property
Public Property Let Producto(ByVal Data As Integer)
     mvarProducto = Data
End Property

Public Property Get Usuario() As Variant
     Usuario = mvarUsuario
End Property
Public Property Let Usuario(ByVal Data As Variant)
     mvarUsuario = Data
End Property

Public Property Get KO() As Boolean
     KO = mvarKO
End Property
Public Property Let KO(ByVal Data As Boolean)
     mvarKO = Data
End Property

Public Property Get TextoError() As String
     TextoError = mvarTextoError
End Property
Public Property Let TextoError(ByVal Data As String)
     mvarTextoError = Data
End Property
