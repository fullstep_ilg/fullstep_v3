VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CProveedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarIndice As Variant 'Nos indica la posicion secuencial dentro de una Coleccion
Private mvarIdCiaComp As Long
Private mvarServidorGS As String
Private mvarBaseDatosGS As String
Private mvarCod As String 'local copy
Private mvarDen As String 'local copy
Private mvarNIF As Variant
Private mvarDireccion As Variant
Private mvarPoblacion As Variant
Private mvarCP As Variant
Private mvarCodMon As Variant
Private mvarCodPais As Variant
Private mvarCodProvi As Variant
Private mvarDenMon As Variant
Private mvarDenPais As Variant
Private mvarDenProvi As Variant
Private mvarObs As Variant
Private mvarHomologado As Variant

Private mvarConexion As CConexion 'local copy

' Variables para el interface de material asignado
Private mvarGruposMN1 As CGruposMatNivel1
Private mvarGruposMN2 As CGruposMatNivel2
Private mvarGruposMN3 As CGruposMatNivel3
Private mvarGruposMN4 As CGruposMatNivel4


Public Property Get Homologado() As Variant
    Homologado = mvarHomologado
End Property
Public Property Let Homologado(ByVal Valor As Variant)
    mvarHomologado = Valor
End Property

Public Property Get CP() As Variant
    CP = mvarCP
End Property
Public Property Let CP(ByVal Cod As Variant)
    mvarCP = Cod
End Property
Public Property Get Obs() As Variant
    Obs = mvarObs
End Property
Public Property Let Obs(ByVal Cod As Variant)
    mvarObs = Cod
End Property

Public Property Get codmon() As Variant
    codmon = mvarCodMon
End Property
Public Property Let codmon(ByVal Cod As Variant)
    mvarCodMon = Cod
End Property
Public Property Get CodPais() As Variant
    CodPais = mvarCodPais
End Property
Public Property Let CodPais(ByVal Cod As Variant)
    mvarCodPais = Cod
End Property
Public Property Get codprovi() As Variant
    codprovi = mvarCodProvi
End Property
Public Property Let codprovi(ByVal Cod As Variant)
    mvarCodProvi = Cod
End Property
Public Property Get denprovi() As Variant
    denprovi = mvarDenProvi
End Property
Public Property Let denprovi(ByVal Cod As Variant)
    mvarDenProvi = Cod
End Property
Public Property Get DenPais() As Variant
    DenPais = mvarDenPais
End Property
Public Property Let DenPais(ByVal Cod As Variant)
    mvarDenPais = Cod
End Property
Public Property Get denmon() As Variant
    denmon = mvarDenMon
End Property
Public Property Let denmon(ByVal Cod As Variant)
    mvarDenMon = Cod
End Property
Public Property Get Direccion() As Variant
    Direccion = mvarDireccion
End Property
Public Property Let Direccion(ByVal Cod As Variant)
    mvarDireccion = Cod
End Property
Public Property Get Poblacion() As Variant
    Poblacion = mvarPoblacion
End Property
Public Property Let Poblacion(ByVal Cod As Variant)
    mvarPoblacion = Cod
End Property

Public Property Get ServidorGS() As String
    ServidorGS = mvarServidorGS
End Property
Public Property Let ServidorGS(ByVal SRVGS As String)
    mvarServidorGS = SRVGS
End Property
Public Property Get BaseDatosGS() As String
    BaseDatosGS = mvarBaseDatosGS
End Property
Public Property Let BaseDatosGS(ByVal BDGS As String)
    mvarBaseDatosGS = BDGS
End Property

Public Property Get Cod() As String
    Cod = mvarCod
End Property
Public Property Let Cod(ByVal Cod As String)
    mvarCod = Cod
End Property

Public Property Get Den() As String
    Den = mvarDen
End Property
Public Property Let Den(ByVal Den As String)
    mvarDen = Den
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal varInd As Variant)
    mvarIndice = varInd
End Property

Public Property Get NIF() As Variant
    NIF = mvarNIF
End Property
Public Property Let NIF(ByVal varNIF As Variant)
    mvarNIF = varNIF
End Property
Public Property Get idCiaComp() As Variant
    idCiaComp = mvarIdCiaComp
End Property
Public Property Let idCiaComp(ByVal varIdCiaComp As Variant)
    mvarIdCiaComp = varIdCiaComp
End Property


Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Private Sub Class_Terminate()
    
    Set mvarConexion = Nothing
    
End Sub

''' <summary>
''' Carga de la estructura de materiales asignada al proveedor hasta el nivel 4
''' </summary>
''' <returns>Boolean</returns>
''' <remarks>Llamada desde: frmEnlazarComprador.sdbgProve_BeforeColUpdate;
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 13/01/2011</revision>
Public Function DevolverEstrMatDeProveedorEnCiaComp() As CGruposMatNivel1
' Esta funcion cargar� en las colecciones de materiales
' toda la estructura de materiales que llegu� hasta el nivel4, asignada al proveedor

Dim oGrupsMN1 As CGruposMatNivel1
Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4
Dim sConsulta As String
Dim ador As adodb.Recordset
Dim sCod1 As String
Dim sCod2 As String
Dim sCod3 As String
Dim sCod4 As String
Dim SRV As String
Dim BD As String
Dim sfsg As String


    'Primero debemos obtener los datos de SERVIDOR,BD,... de la CiaCompradora


    sfsg = mvarServidorGS & "." & mvarBaseDatosGS & ".dbo."

    Set oGrupsMN1 = Nothing
    Set oGrupsMN1 = New CGruposMatNivel1

' Generamos la coleccion de grupos mat nivel1

    sConsulta = "SELECT GMN1.COD,GMN1." & DevolverDenGMN & " FROM " & sfsg & "GMN1 AS GMN1 WITH(NOLOCK) INNER JOIN " & sfsg & "PROVE_GMN4 AS PROVE_GMN4 WITH(NOLOCK) ON GMN1.COD = PROVE_GMN4.GMN1 AND PROVE='" & DblQuote(mvarCod) & "'"
    sConsulta = sConsulta & " GROUP BY COD," & DevolverDenGMN

    Set ador = New adodb.Recordset
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly


    If ador.eof Then
        Set DevolverEstrMatDeProveedorEnCiaComp = oGrupsMN1
        Set oGrupsMN1 = Nothing
        ador.Close
        Set ador = Nothing
        Exit Function
    End If

    While Not ador.eof
        Set oGMN1 = oGrupsMN1.Add(ador("COD").Value, ador("DEN").Value)
        Set oGMN1.GruposMatNivel2 = New CGruposMatNivel2
        Set oGMN1.GruposMatNivel2.Conexion = mvarConexion

        ador.MoveNext
        Set oGMN1 = Nothing
    Wend

    ador.Close
    Set ador = Nothing


' Generamos la coleccion de grupos mat nivel2

    sConsulta = "SELECT GMN2.GMN1,GMN2.COD,GMN2." & DevolverDenGMN & " FROM " & sfsg & "GMN2 AS GMN2 WITH(NOLOCK) ," & sfsg & "PROVE_GMN4 AS PROVE_GMN4 WITH(NOLOCK) WHERE GMN2.GMN1 = PROVE_GMN4.GMN1 AND GMN2.COD = PROVE_GMN4.GMN2 AND PROVE='" & DblQuote(mvarCod) & "'"
    sConsulta = sConsulta & " GROUP BY GMN2.GMN1,GMN2.COD,GMN2." & DevolverDenGMN
    Set ador = New adodb.Recordset
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

    If ador.eof Then
        ador.Close
        Set ador = Nothing
        Set DevolverEstrMatDeProveedorEnCiaComp = oGrupsMN1
        Set oGrupsMN1 = Nothing
        Exit Function
    End If

    While Not ador.eof
        sCod1 = ador("GMN1").Value & Mid$("                         ", 1, 20 - Len(ador("GMN1").Value))
        Set oGMN1 = oGrupsMN1.Item(sCod1)
        Set oGMN2 = oGMN1.GruposMatNivel2.Add(ador("GMN1").Value, "", CStr(ador("COD").Value), ador("DEN").Value)
        ' Creo una nueva coleccion para cada uno
        Set oGMN2.GruposMatNivel3 = New CGruposMatNivel3
        Set oGMN2.GruposMatNivel3.Conexion = mvarConexion
        ador.MoveNext
        Set oGMN1 = Nothing
        Set oGMN2 = Nothing
    Wend
    ador.Close
    Set ador = Nothing

    ' Generamos la coleccion de grupos mat nivel3

    sConsulta = "SELECT GMN3.GMN1,GMN3.GMN2,GMN3.COD,GMN3." & DevolverDenGMN & " FROM " & sfsg & "GMN3 AS GMN3 WITH(NOLOCK) ," & sfsg & "PROVE_GMN4 AS PROVE_GMN4 WITH(NOLOCK) WHERE GMN3.GMN1 = PROVE_GMN4.GMN1 AND GMN3.GMN2=PROVE_GMN4.GMN2 AND GMN3.COD = PROVE_GMN4.GMN3 AND PROVE='" & DblQuote(mvarCod) & "'"
     sConsulta = sConsulta & " GROUP BY GMN3.GMN1,GMN3.GMN2,GMN3.COD,GMN3." & DevolverDenGMN
    Set ador = New adodb.Recordset
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

    If ador.eof Then
        ador.Close
        Set ador = Nothing
        Set DevolverEstrMatDeProveedorEnCiaComp = oGrupsMN1
        Set oGrupsMN1 = Nothing
        Exit Function
    End If

    While Not ador.eof
        sCod1 = ador("GMN1").Value & Mid$("                         ", 1, 20 - Len(ador("GMN1").Value))
        Set oGMN1 = oGrupsMN1.Item(sCod1)
        sCod2 = ador("GMN2").Value & Mid$("                         ", 1, 20 - Len(ador("GMN2").Value))
        Set oGMN2 = oGMN1.GruposMatNivel2.Item(sCod1 & sCod2)
        Set oGMN3 = oGMN2.GruposMatNivel3.Add(ador("GMN1").Value, ador("GMN2").Value, CStr(ador("COD").Value), "", "", ador("DEN").Value)
        ' Creo una nueva coleccion para cada uno
        Set oGMN3.GruposMatNivel4 = New CGruposMatNivel4
        Set oGMN3.GruposMatNivel4.Conexion = mvarConexion

        ador.MoveNext
        Set oGMN1 = Nothing
        Set oGMN2 = Nothing
        Set oGMN3 = Nothing
    Wend

    ador.Close
    Set ador = Nothing



    ' Generamos la coleccion de grupos mat nivel4

    sConsulta = "SELECT GMN4.GMN1,GMN4.GMN2,GMN4.GMN3,GMN4.COD,GMN4." & DevolverDenGMN & " FROM " & sfsg & "GMN4 AS GMN4 WITH(NOLOCK) ," & sfsg & "PROVE_GMN4 AS PROVE_GMN4 WITH(NOLOCK) WHERE GMN4.GMN1 = PROVE_GMN4.GMN1 AND GMN4.GMN2=PROVE_GMN4.GMN2 AND GMN4.GMN3=PROVE_GMN4.GMN3 AND GMN4.COD=PROVE_GMN4.GMN4 AND PROVE='" & DblQuote(mvarCod) & "'"
     sConsulta = sConsulta & " GROUP BY GMN4.GMN1,GMN4.GMN2,GMN4.GMN3,GMN4.COD,GMN4." & DevolverDenGMN
    Set ador = New adodb.Recordset
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

    If ador.eof Then
        ador.Close
        Set ador = Nothing
        Set DevolverEstrMatDeProveedorEnCiaComp = oGrupsMN1
        Set oGrupsMN1 = Nothing
        Exit Function
    End If

    While Not ador.eof
        sCod1 = ador("GMN1").Value & Mid$("                         ", 1, 20 - Len(ador("GMN1").Value))
        Set oGMN1 = oGrupsMN1.Item(sCod1)
        sCod2 = ador("GMN2") & Mid$("                         ", 1, 20 - Len(ador("GMN2").Value))
        Set oGMN2 = oGMN1.GruposMatNivel2.Item(sCod1 & sCod2)
        sCod3 = ador("GMN3") & Mid$("                         ", 1, 20 - Len(ador("GMN3").Value))
        Set oGMN3 = oGMN2.GruposMatNivel3.Item(sCod1 & sCod2 & sCod3)
        oGMN3.GruposMatNivel4.Add ador("GMN1").Value, ador("GMN2").Value, ador("GMN3").Value, "", "", "", CStr(ador("COD").Value), ador("DEN").Value
        ador.MoveNext
        Set oGMN1 = Nothing
        Set oGMN2 = Nothing
        Set oGMN3 = Nothing
    Wend

    ador.Close
    Set ador = Nothing




Set DevolverEstrMatDeProveedorEnCiaComp = oGrupsMN1
Set oGrupsMN1 = Nothing



End Function

''' <summary>
''' Devuelve colecci�n de grupos de materiales de nivel 1
''' </summary>
''' <param optional name="UsarIndice">Si se usa indice para la coleccion</param>
''' <returns>CGruposMatNivel1</returns>
''' <remarks>Llamada desde: frmPROVEMatPorProve.GenerarEstructuraDeMateriales;
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 13/01/2011</revision>
Public Function DevolverGruposMN1Asignados(Optional ByVal UsarIndice As Boolean = False) As CGruposMatNivel1

Dim adoRes As adodb.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim oGruposMN1 As CGruposMatNivel1
Dim sfsg As String

   'Primero debemos obtener los datos de SERVIDOR,BD,... de la CiaCompradora
    
    If IsNull(mvarServidorGS) Or mvarServidorGS = "" Or IsNull(mvarBaseDatosGS) Or mvarBaseDatosGS = "" Then
        Set DevolverGruposMN1Asignados = New CGruposMatNivel1
        Set DevolverGruposMN1Asignados.Conexion = mvarConexion
        Exit Function
    End If
    sfsg = mvarServidorGS & "." & mvarBaseDatosGS & ".dbo."
   
sConsulta = "SELECT GMN1.COD,GMN1." & DevolverDenGMN & " AS DEN FROM " & sfsg & "PROVE_GMN1 as PROVE_GMN1 WITH(NOLOCK) ," & sfsg & "GMN1 AS GMN1 WITH(NOLOCK) "
sConsulta = sConsulta & " WHERE PROVE_GMN1.PROVE='" & DblQuote(mvarCod) & "'"
sConsulta = sConsulta & " AND PROVE_GMN1.GMN1=GMN1.COD"
sConsulta = sConsulta & " ORDER BY " & DevolverDenGMN

Set adoRes = New adodb.Recordset
adoRes.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

      
If adoRes.eof Then
    
    Set oGruposMN1 = Nothing
    Set DevolverGruposMN1Asignados = New CGruposMatNivel1
    Set DevolverGruposMN1Asignados.Conexion = mvarConexion
      
Else
         
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = New CGruposMatNivel1
    Set oGruposMN1.Conexion = mvarConexion
    
    If UsarIndice Then
        lIndice = 0
        While Not adoRes.eof
            oGruposMN1.Add CStr(adoRes("COD").Value), adoRes("DEN").Value, lIndice
            adoRes.MoveNext
            lIndice = lIndice + 1
        Wend
    Else
        While Not adoRes.eof
            oGruposMN1.Add CStr(adoRes("COD").Value), adoRes("DEN").Value
            adoRes.MoveNext
        Wend
    End If
    
    
    Set DevolverGruposMN1Asignados = oGruposMN1
    Set oGruposMN1 = Nothing

End If
adoRes.Close
Set adoRes = Nothing

End Function

''' <summary>
''' Devuelve colecci�n de grupos de materiales de nivel 2
''' </summary>
''' <param optional name="UsarIndice">Si se usa indice para la coleccion</param>
''' <returns>CGruposMatNivel2</returns>
''' <remarks>Llamada desde: frmPROVEMatPorProve.GenerarEstructuraDeMateriales;
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 13/01/2011</revision>
Public Function DevolverGruposMN2Asignados(Optional ByVal UsarIndice As Boolean = False) As CGruposMatNivel2
Dim sfsg As String
Dim adoRes As adodb.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim oGruposMN2 As CGruposMatNivel2

'Primero debemos obtener los datos de SERVIDOR,BD,... de la CiaCompradora
    
    If IsNull(mvarServidorGS) Or mvarServidorGS = "" Or IsNull(mvarBaseDatosGS) Or mvarBaseDatosGS = "" Then
        Set DevolverGruposMN2Asignados = New CGruposMatNivel2
        Set DevolverGruposMN2Asignados.Conexion = mvarConexion
        Exit Function
    End If
    sfsg = mvarServidorGS & "." & mvarBaseDatosGS & ".dbo."

sConsulta = "SELECT GMN2.GMN1 , GMN2.COD, GMN2." & DevolverDenGMN & " AS DEN ,GMN1." & DevolverDenGMN & " as GMN1DEN  FROM " & sfsg & "PROVE_GMN2 AS PROVE_GMN2 WITH(NOLOCK) , " & sfsg & "GMN2 AS GMN2 WITH(NOLOCK) , " & sfsg & "GMN1 AS GMN1 WITH(NOLOCK) "
sConsulta = sConsulta & " WHERE PROVE_GMN2.PROVE='" & DblQuote(mvarCod) & "'"
sConsulta = sConsulta & " AND PROVE_GMN2.GMN1=GMN2.GMN1 AND PROVE_GMN2.GMN2=GMN2.COD AND GMN2.GMN1=GMN1.COD"
sConsulta = sConsulta & " ORDER BY GMN2." & DevolverDenGMN

Set adoRes = New adodb.Recordset
adoRes.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

      
If adoRes.eof Then
    
    Set oGruposMN2 = Nothing
    Set DevolverGruposMN2Asignados = New CGruposMatNivel2
    Set DevolverGruposMN2Asignados.Conexion = mvarConexion
      
Else
         
    Set oGruposMN2 = Nothing
    Set oGruposMN2 = New CGruposMatNivel2
    Set oGruposMN2.Conexion = mvarConexion
    
    If UsarIndice Then
        lIndice = 0
        While Not adoRes.eof
            oGruposMN2.Add adoRes("GMN1").Value, adoRes("GMN1DEN").Value, CStr(adoRes("COD").Value), adoRes("DEN").Value, lIndice
            adoRes.MoveNext
            lIndice = lIndice + 1
        Wend
    Else
        While Not adoRes.eof
            oGruposMN2.Add adoRes("GMN1").Value, adoRes("GMN1DEN").Value, CStr(adoRes("COD").Value), adoRes("DEN").Value
            adoRes.MoveNext
        Wend
    End If
    
    
    Set DevolverGruposMN2Asignados = oGruposMN2
    Set oGruposMN2 = Nothing

End If
adoRes.Close
Set adoRes = Nothing

End Function

''' <summary>
''' Devuelve colecci�n de grupos de materiales de nivel 3
''' </summary>
''' <param optional name="UsarIndice">Si se usa indice para la coleccion</param>
''' <returns>CGruposMatNivel3</returns>
''' <remarks>Llamada desde: frmPROVEMatPorProve.GenerarEstructuraDeMateriales;
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 13/01/2011</revision>
Public Function DevolverGruposMN3Asignados(Optional ByVal UsarIndice As Boolean = False) As CGruposMatNivel3
Dim sfsg As String
Dim adoRes As adodb.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim oGruposMN3 As CGruposMatNivel3

'Primero debemos obtener los datos de SERVIDOR,BD,... de la CiaCompradora
    
    If IsNull(mvarServidorGS) Or mvarServidorGS = "" Or IsNull(mvarBaseDatosGS) Or mvarBaseDatosGS = "" Then
        Set DevolverGruposMN3Asignados = New CGruposMatNivel3
        Set DevolverGruposMN3Asignados.Conexion = mvarConexion
        Exit Function
    End If
    sfsg = mvarServidorGS & "." & mvarBaseDatosGS & ".dbo."

sConsulta = "SELECT GMN3.GMN1,GMN3.GMN2,GMN3.COD,GMN3." & DevolverDenGMN & " AS DEN ,GMN2." & DevolverDenGMN & " AS GMN2DEN, GMN1." & DevolverDenGMN & " as GMN1DEN FROM " & sfsg & "PROVE_GMN3 AS PROVE_GMN3 WITH(NOLOCK) ," & sfsg & "GMN3 AS GMN3 WITH(NOLOCK) ," & sfsg & "GMN2 AS GMN2 WITH(NOLOCK) ," & sfsg & "GMN1 AS GMN1 WITH(NOLOCK) "
sConsulta = sConsulta & " WHERE PROVE_GMN3.PROVE='" & DblQuote(mvarCod) & "'"
sConsulta = sConsulta & " AND PROVE_GMN3.GMN1=GMN3.GMN1 AND PROVE_GMN3.GMN2=GMN3.GMN2 AND PROVE_GMN3.GMN3=GMN3.COD"
sConsulta = sConsulta & " AND GMN3.GMN1=GMN2.GMN1 AND GMN3.GMN2=GMN2.COD AND GMN3.GMN1=GMN1.COD"
sConsulta = sConsulta & " ORDER BY GMN3." & DevolverDenGMN

Set adoRes = New adodb.Recordset
adoRes.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

      
If adoRes.eof Then
    
    Set oGruposMN3 = Nothing
    Set DevolverGruposMN3Asignados = New CGruposMatNivel3
    Set DevolverGruposMN3Asignados.Conexion = mvarConexion
      
Else
         
    Set oGruposMN3 = Nothing
    Set oGruposMN3 = New CGruposMatNivel3
    Set oGruposMN3.Conexion = mvarConexion
    
    If UsarIndice Then
        lIndice = 0
        While Not adoRes.eof
            oGruposMN3.Add adoRes("GMN1").Value, adoRes("GMN2").Value, CStr(adoRes("COD").Value), adoRes("GMN1DEN").Value, adoRes("GMN2DEN").Value, adoRes("DEN").Value, lIndice
            adoRes.MoveNext
            lIndice = lIndice + 1
        Wend
    Else
        While Not adoRes.eof
            oGruposMN3.Add adoRes("GMN1").Value, adoRes("GMN2").Value, CStr(adoRes("COD").Value), adoRes("GMN1DEN").Value, adoRes("GMN2DEN").Value, adoRes("DEN").Value
            adoRes.MoveNext
        Wend
    End If
    
    
    Set DevolverGruposMN3Asignados = oGruposMN3
    Set oGruposMN3 = Nothing

End If
adoRes.Close
Set adoRes = Nothing

End Function

''' <summary>
''' Devuelve colecci�n de grupos de materiales de nivel 4
''' </summary>
''' <param optional name="UsarIndice">Si se usa indice para la coleccion</param>
''' <returns>CGruposMatNivel4</returns>
''' <remarks>Llamada desde: frmPROVEMatPorProve.GenerarEstructuraDeMateriales;
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 13/01/2011</revision>
Public Function DevolverGruposMN4Asignados(Optional ByVal UsarIndice As Boolean = False) As CGruposMatNivel4
Dim sfsg As String
Dim adoRes As adodb.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim oGruposMN4 As CGruposMatNivel4

'Primero debemos obtener los datos de SERVIDOR,BD,... de la CiaCompradora
    
    If IsNull(mvarServidorGS) Or mvarServidorGS = "" Or IsNull(mvarBaseDatosGS) Or mvarBaseDatosGS = "" Then
        Set DevolverGruposMN4Asignados = New CGruposMatNivel4
        Set DevolverGruposMN4Asignados.Conexion = mvarConexion
        Exit Function
    End If
    sfsg = mvarServidorGS & "." & mvarBaseDatosGS & ".dbo."

sConsulta = "SELECT GMN4.GMN1,GMN4.GMN2,GMN4.GMN3,GMN4.COD,GMN4." & DevolverDenGMN & " AS DEN ,GMN3." & DevolverDenGMN & " as GMN3DEN,GMN2." & DevolverDenGMN & " AS GMN2DEN,GMN1." & DevolverDenGMN & " AS GMN1DEN FROM " & sfsg & "PROVE_GMN4 AS PROVE_GMN4 WITH(NOLOCK) ," & sfsg & "GMN4 AS GMN4 WITH(NOLOCK) ," & sfsg & "GMN3 AS GMN3 ," & sfsg & "GMN2 AS GMN2 WITH(NOLOCK) ," & sfsg & "GMN1 AS GMN1 WITH(NOLOCK) "
sConsulta = sConsulta & " WHERE PROVE_GMN4.PROVE='" & DblQuote(mvarCod) & "'"
sConsulta = sConsulta & " AND PROVE_GMN4.GMN1=GMN4.GMN1 AND PROVE_GMN4.GMN2=GMN4.GMN2 AND PROVE_GMN4.GMN3=GMN4.GMN3 AND PROVE_GMN4.GMN4=GMN4.COD"
sConsulta = sConsulta & " AND GMN4.GMN1=GMN3.GMN1 AND GMN4.GMN2=GMN3.GMN2 AND GMN4.GMN3=GMN3.COD AND GMN4.GMN1=GMN2.GMN1 AND GMN4.GMN2=GMN2.COD AND GMN4.GMN1=GMN1.COD"
sConsulta = sConsulta & " ORDER BY GMN4." & DevolverDenGMN

Set adoRes = New adodb.Recordset
adoRes.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

      
If adoRes.eof Then
    
    Set oGruposMN4 = Nothing
    Set DevolverGruposMN4Asignados = New CGruposMatNivel4
    Set DevolverGruposMN4Asignados.Conexion = mvarConexion
      
Else
         
    Set oGruposMN4 = Nothing
    Set oGruposMN4 = New CGruposMatNivel4
    Set oGruposMN4.Conexion = mvarConexion
    
    If UsarIndice Then
        lIndice = 0
        While Not adoRes.eof
            oGruposMN4.Add adoRes("GMN1").Value, adoRes("GMN2").Value, adoRes("GMN3").Value, adoRes("GMN1DEN").Value, adoRes("GMN2DEN").Value, adoRes("GMN3DEN").Value, CStr(adoRes("COD").Value), adoRes("DEN").Value, lIndice
            adoRes.MoveNext
            lIndice = lIndice + 1
        Wend
    Else
        While Not adoRes.eof
            oGruposMN4.Add adoRes("GMN1").Value, adoRes("GMN2").Value, adoRes("GMN3").Value, adoRes("GMN1DEN").Value, adoRes("GMN2DEN").Value, adoRes("GMN3DEN").Value, CStr(adoRes("COD").Value), adoRes("DEN").Value
            adoRes.MoveNext
        Wend
    End If
    
    
    Set DevolverGruposMN4Asignados = oGruposMN4
    Set oGruposMN4 = Nothing

End If
adoRes.Close
Set adoRes = Nothing

End Function

''' <summary>Comprueba si existe un Proveedor con el mismo NIF.</summary>
''' <param name="bAdd">Indica si la comprobaci�n se produce en un alta o una modificaci�n de un Proveedor.</param>
''' <returns>Objeto de tipo CTESError con el error si se ha producido</returns>
''' <remarks>Llamada desde=CCias.ComprobarNIFRepetidoProvEnlazado; Tiempo m�ximo=0seg.</remarks>
''' Revisado por: Jbg. Fecha: 03/07/2012
Public Function ComprobarNIFRepetido() As CTESError
    Dim sConsulta As String
    Dim adoRes As adodb.Recordset
    Dim sBDGS As String
    Dim sSRVGS As String
    Dim sPrefijo As String
    Dim oError As CTESError
    
    On Error GoTo Error
    
    Set oError = New CTESError
    oError.NumError = TESnoerror
    
    sSRVGS = g_udtParametrosGenerales.g_sFSSRVCiaCompradora
    sBDGS = g_udtParametrosGenerales.g_sFSBDCiaCompradora
    sPrefijo = sSRVGS & "." & sBDGS & ".dbo."
    
    
    sConsulta = "SELECT COUNT(1) AS NumProv FROM " & sPrefijo & "PROVE WITH(NOLOCK) WHERE NIF='" & DblQuote(mvarNIF) & "'"
    sConsulta = sConsulta & " AND COD<>'" & DblQuote(mvarCod) & "'"
    If g_udtParametrosGenerales.g_bPymes Then
        sConsulta = sConsulta & " AND LEFT(COD,3)='" & Left(mvarCod, 3) & "'"
    End If
    Set adoRes = New adodb.Recordset
    adoRes.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    If Not adoRes.eof Then
        If adoRes("NumProv").Value > 0 Then
            oError.NumError = TESNIFProveRepetido
            oError.Arg1 = mvarNIF
        End If
    End If
    
fin:
    adoRes.Close
    Set adoRes = Nothing
    Set ComprobarNIFRepetido = oError
    Set oError = Nothing
    Exit Function
Error:
    On Error Resume Next
    If mvarConexion.AdoCon.Errors.Count > 0 Then
        Set oError = basErrores.TratarError(mvarConexion.AdoCon.Errors)
    Else
        oError.NumError = TESOtroerror
    End If
    Resume fin
End Function

