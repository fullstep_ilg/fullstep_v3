VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCategoria"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_varIndice As Variant 'Nos indica la posicion secuencial dentro de una Coleccion
Private m_lId As Integer    'Identificador de categor�a laboral
Private m_sCodigo As String 'C�digo de categor�a laboral
Private m_oDenominaciones As CMultiidiomas   'Denominaciones en todos los idiomas de la aplicaci�n de la categoria
Private m_lCosteH As Long   'Coste por hora de la categoria
Private m_bDact As Boolean  'Indica si una categoria est� o no deshabilitada
Private m_iElim As Integer  'Flag que indica si hay que eliminar la asignaci�n de la categor�a al proveedor (0 nada, 1 baja l�gica, 2 eliminar,3 deshacer baja)
Private m_iElimSis As Integer   'Flag que indica si hay que eliminar la categor�a de la tabla categor�as  (0 no, 1 baja l�gica, 2 eliminar, ,3 deshacer baja)

Private m_varConexion As CConexion 'local copy


Public Property Get Indice() As Variant
    Indice = m_varIndice
End Property
Public Property Let Indice(ByVal varInd As Variant)
    m_varIndice = varInd
End Property

Public Property Get ID() As Long
    ID = m_lId
End Property
Public Property Let ID(ByVal lID As Long)
    m_lId = lID
End Property

Public Property Get Codigo() As String
    Codigo = m_sCodigo
End Property
Public Property Let Codigo(ByVal sCodigo As String)
    m_sCodigo = sCodigo
End Property

Public Property Set Denominaciones(ByVal vData As CMultiidiomas)
    Set m_oDenominaciones = vData
End Property
Public Property Get Denominaciones() As CMultiidiomas
    Set Denominaciones = m_oDenominaciones
End Property

Public Property Get CosteH() As Long
    CosteH = m_lCosteH
End Property
Public Property Let CosteH(ByVal lCosteH As Long)
    m_lCosteH = lCosteH
End Property

Public Property Get Dact() As Boolean
    Dact = m_bDact
End Property
Public Property Let Dact(ByVal bDact As Boolean)
    m_bDact = bDact
End Property

Public Property Get Elim() As Integer
    Elim = m_iElim
End Property
Public Property Let Elim(ByVal iElim As Integer)
    m_iElim = iElim
End Property

Public Property Get ElimSis() As Integer
    ElimSis = m_iElimSis
End Property
Public Property Let ElimSis(ByVal iElimSis As Integer)
    m_iElimSis = iElimSis
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_varConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_varConexion
End Property

Private Sub Class_Terminate()
    Set m_varConexion = Nothing
End Sub

''' <summary>Mira si esa categor�a est� asociada a alg�n otro proveedor o se est� usando en el FSGA.</summary>
''' <param name="sProve">C�digo del proveedor</param>
''' <returns>True si est� asociada a alg�n usuario, False si no est� asociada a ninguno.</returns>
''' <remarks>Llamada desde frmCompanias; Tiempo m�ximo=0seg.</remarks>

Public Function EstaAsociadaOUsando(ByVal sProve As String) As Boolean
Dim sBDGS As String
Dim sSRVGS As String
Dim sPrefijo As String
Dim oComm As ADODB.Command
Dim oParam As ADODB.Parameter
Dim rsNumUsu As Recordset
Dim sConsulta As String
    
    sSRVGS = g_udtParametrosGenerales.g_sFSSRVCiaCompradora
    sBDGS = g_udtParametrosGenerales.g_sFSBDCiaCompradora
    sPrefijo = sSRVGS & "." & sBDGS & ".dbo."
    
    EstaAsociadaOUsando = False
    
    'Se mira si est� asociada a alg�n otro proveedor.
    sConsulta = "SELECT COUNT(ID_CAT_LAB) AS NUMUSU FROM " & sPrefijo & "FSGA_CAT_PROVE WITH (NOLOCK) WHERE ID_CAT_LAB=? AND PROVE<>?"
    
    Set oComm = New ADODB.Command
    With oComm
        Set .ActiveConnection = m_varConexion.AdoCon
        .CommandText = sConsulta
        .CommandType = adCmdText
        .Prepared = True
        
        Set oParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_lId)
        .Parameters.Append oParam
        Set oParam = .CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=20, Value:=sProve)
        .Parameters.Append oParam
        
        
        Set rsNumUsu = .Execute
        If Not rsNumUsu.eof Then
            If rsNumUsu("NUMUSU").Value > 0 Then
                EstaAsociadaOUsando = True
            End If
        End If
    End With

    'Si no est� asociada a ning�n otro proveedor se mira si se est� usando en el FSGA.
    If EstaAsociadaOUsando = False Then
    
        sConsulta = "SELECT COUNT(ID_CAT_LAB) AS NUMUSU FROM " & sPrefijo & "FSGA_USU_TARE WITH (NOLOCK) "
        sConsulta = sConsulta & " UNION "
        sConsulta = sConsulta & "SELECT COUNT(ID) AS NUMUSU FROM " & sPrefijo & "FSGA_USU WITH (NOLOCK) "
        sConsulta = sConsulta & " WHERE ID_CAT_LAB=?"
        
        Set oComm = New ADODB.Command
        With oComm
            Set .ActiveConnection = m_varConexion.AdoCon
            .CommandText = sConsulta
            .CommandType = adCmdText
            .Prepared = True
            
            Set oParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_lId)
            .Parameters.Append oParam
            
            Set rsNumUsu = .Execute
            If Not rsNumUsu.eof Then
                If rsNumUsu("NUMUSU").Value > 1 Then
                    EstaAsociadaOUsando = True
                End If
            End If
        End With
        
    End If
    
    Set oParam = Nothing
    Set oComm = Nothing
End Function


''' <summary>Mira si hay alg�n usuario de ese proveedor que tenga esta categoria asignada.</summary>
''' <param name="sProve">C�digo del proveedor</param>
''' <returns>True si est� asociada a alg�n usuario, False si no est� asociada a ninguno.</returns>
''' <remarks>Llamada desde frmCompanias; Tiempo m�ximo=0seg.</remarks>

Public Function EstaAsociadaAUsuariosDelProve(ByVal sProve As String) As Boolean
Dim sBDGS As String
Dim sSRVGS As String
Dim sPrefijo As String
Dim oComm As ADODB.Command
Dim oParam As ADODB.Parameter
Dim rsNumUsu As Recordset
Dim sConsulta As String
    
    sSRVGS = g_udtParametrosGenerales.g_sFSSRVCiaCompradora
    sBDGS = g_udtParametrosGenerales.g_sFSBDCiaCompradora
    sPrefijo = sSRVGS & "." & sBDGS & ".dbo."
    
    EstaAsociadaAUsuariosDelProve = False
    
    sConsulta = "SELECT COUNT(ID) AS NUMUSU FROM " & sPrefijo & "FSGA_USU WITH (NOLOCK) WHERE ID_CAT_LAB=? AND PROVE=?"
    
    Set oComm = New ADODB.Command
    With oComm
        Set .ActiveConnection = m_varConexion.AdoCon
        .CommandText = sConsulta
        .CommandType = adCmdText
        .Prepared = True
        
        Set oParam = .CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=m_lId)
        .Parameters.Append oParam
        Set oParam = .CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=20, Value:=sProve)
        .Parameters.Append oParam
        
        Set rsNumUsu = .Execute
        If Not rsNumUsu.eof Then
            If rsNumUsu("NUMUSU").Value > 0 Then
                EstaAsociadaAUsuariosDelProve = True
            End If
        End If
    End With
    
    Set oParam = Nothing
    Set oComm = Nothing
End Function

