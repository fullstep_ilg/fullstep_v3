VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cGestorInformes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mvarAdoConexionSummit As adodb.Connection
Private mvarAdoComando As adodb.Command

Private mvarConexion As CConexion


Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set mvarConexion = vData
    
End Property

Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = mvarConexion
    
End Property

Public Function DevolverVolumenAdjudicado(Optional ByVal FecDesde As Variant, Optional ByVal FecHasta As Variant, Optional ByVal CiaProv As Long, Optional ByVal TipoOrdenacion As Integer) As adodb.Recordset
    Dim ador As adodb.Recordset
    Dim sConsulta As String
    
    '''Carga los informes de volumen adjudicado
    If CiaProv <> 0 Then
        sConsulta = "SELECT SUM(VOLADJ.VOLPOR) AS VOLPOR,SUM(VOLADJ.VOLGS) AS VOLGS,MAX(VOLADJ.MONGS) AS MONGS,MAX(VOLADJ.MONPOR) AS MONPOR,MAX(CIAS_MON.MON_GS) As MGS,MAX(CIAS.DEN) AS DEN,MAX(CIAS.COD) AS COD,MAX(CIA_COMP) AS ID"
        sConsulta = sConsulta & " FROM VOLADJ WITH (NOLOCK) INNER JOIN CIAS WITH (NOLOCK) "
        sConsulta = sConsulta & " ON VOLADJ.CIA_COMP=CIAS.ID"
    Else
        sConsulta = "SELECT SUM(VOLADJ.VOLPOR) AS VOLPOR, SUM(VOLADJ.VOLGS) AS VOLGS,MAX(VOLADJ.MONGS) AS MONGS,MAX(VOLADJ.MONPOR) AS MONPOR,MAX(CIAS.DEN) AS DEN,MAX(CIAS.COD) AS COD,MAX(CIA_PROVE) AS ID"
        sConsulta = sConsulta & ",SUM(ISNULL(VOLADJ.VOLPOR,100)) AS VOLPORD"
        sConsulta = sConsulta & " FROM VOLADJ WITH (NOLOCK) INNER JOIN CIAS WITH (NOLOCK) "
        sConsulta = sConsulta & " INNER JOIN CIAS_PORT WITH (NOLOCK) ON CIAS.ID=CIAS_PORT.CIA AND CIAS.PORT=CIAS_PORT.PORT AND"
        sConsulta = sConsulta & " CIAS.PORT=0 AND (CIAS_PORT.FPEST=2 OR CIAS_PORT.FPEST=3)"
        sConsulta = sConsulta & " ON VOLADJ.CIA_PROVE=CIAS.ID"
    End If
    
    If Not IsMissing(FecDesde) Then
        sConsulta = sConsulta & " AND FECADJ>=" & DateToSQLDate(FecDesde)
    End If
    
    If Not IsMissing(FecHasta) Then
        sConsulta = sConsulta & " AND FECADJ <=" & DateToSQLDate(FecHasta)
    End If
    
    If CiaProv <> 0 Then
        'Acumulado por comprador para el proveedor especificado
        sConsulta = sConsulta & " AND CIA_PROVE=" & CiaProv
        sConsulta = sConsulta & " LEFT JOIN CIAS_MON WITH (NOLOCK) ON VOLADJ.CIA_COMP=CIAS_MON.CIA"
        sConsulta = sConsulta & " GROUP BY CIA_COMP"
    Else
        'Acumulado por proveedor para todos los proveedores con vol.adjudicado
        'sConsulta = sConsulta & " AND CIAS.FCEST=0"
        sConsulta = sConsulta & " GROUP BY CIA_PROVE"
    End If
    
    If TipoOrdenacion <> 0 Then
        Select Case TipoOrdenacion
            Case 1
                sConsulta = sConsulta & " ORDER BY DEN"
                'sConsulta = sConsulta & " ORDER BY 5"
            Case 2
                sConsulta = sConsulta & " ORDER BY VOLPOR"
                'sConsulta = sConsulta & " ORDER BY 1"
            Case 3
                sConsulta = sConsulta & " ORDER BY VOLGS"
                'sConsulta = sConsulta & " ORDER BY 2"
            Case 4
                sConsulta = sConsulta & " ORDER BY MONGS"
                'sConsulta = sConsulta & " ORDER BY 3"
        End Select
    Else
        sConsulta = sConsulta & " ORDER BY COD"
        'sConsulta = sConsulta & " ORDER BY 6"
    End If
    
    Set ador = New adodb.Recordset
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    Set ador.ActiveConnection = Nothing
    
    Set DevolverVolumenAdjudicado = ador
End Function

Public Function DevolverVolumenPorProcesos(Optional ByVal FecDesde As Variant, Optional ByVal FecHasta As Variant, Optional ByVal CiaProv As Long, Optional ByVal CiaComp As Long, Optional ByVal TipoOrdenacion As Integer) As adodb.Recordset
    Dim ador As adodb.Recordset
    Dim sConsulta As String
    Dim sfsg As String
    
    'Devuelve el detalle de los procesos
    If CiaComp = 0 Then
        'sConsulta = "SELECT VOLADJ.*,CIAS.DEN,CIAS.COD FROM VOLADJ INNER JOIN CIAS"
        'sConsulta = sConsulta & " ON VOLADJ.CIA_COMP=CIAS.ID AND "
        sConsulta = "SELECT SUM(VOLGS) AS VOLGS,SUM(VOLPOR) AS VOLPOR,MAX(MONGS) AS MONGS,MAX(CIAS.ID) as ID"
        sConsulta = sConsulta & ",MAX(CIAS_MON.MON_GS) AS MGS,MAX(CIAS.DEN) AS DEN,MAX(CIAS.COD) AS COD FROM VOLADJ WITH (NOLOCK) INNER JOIN CIAS WITH (NOLOCK) "
        sConsulta = sConsulta & " ON VOLADJ.CIA_COMP=CIAS.ID AND "
    Else

        sConsulta = "SELECT VOLADJ.* FROM VOLADJ WITH (NOLOCK) WHERE"
        
    End If
    
    If Not IsMissing(FecDesde) Then
        sConsulta = sConsulta & " FECADJ>=" & DateToSQLDate(FecDesde)
    End If
    
    If Not IsMissing(FecHasta) Then
        sConsulta = sConsulta & " AND FECADJ <=" & DateToSQLDate(FecHasta)
    End If

    If CiaProv <> 0 Then
        sConsulta = sConsulta & " AND CIA_PROVE =" & CiaProv
        'sConsulta = sConsulta & " LEFT JOIN CIAS_MON ON   VOLADJ.CIA_COMP=CIAS_MON.CIA"
    End If
    If CiaComp = 0 Then
            sConsulta = sConsulta & " LEFT JOIN CIAS_MON WITH (NOLOCK) ON   VOLADJ.CIA_COMP=CIAS_MON.CIA"
    End If
    If CiaComp <> 0 Then
        sConsulta = sConsulta & " AND CIA_COMP =" & CiaComp
    End If
    
    If CiaComp = 0 Then
        sConsulta = sConsulta & " GROUP BY CIA_COMP"
    End If
    
    'If TipoOrdenacion <> 0 Then
        Select Case TipoOrdenacion
            Case 0
                If CiaComp = 0 Then
                    sConsulta = sConsulta & " ORDER BY COD"
                Else
                    sConsulta = sConsulta & " ORDER BY ANYO,GMN1,PROCE"
                End If
            Case 1
                If CiaComp = 0 Then
                    sConsulta = sConsulta & " ORDER BY DEN"
                Else
                    sConsulta = sConsulta & " ORDER BY DENPROCE"
                End If
            Case 2
                sConsulta = sConsulta & " ORDER BY VOLPOR"
            Case 3
                sConsulta = sConsulta & " ORDER BY VOLGS"
            Case 5
                If CiaComp = 0 Then
                    sConsulta = sConsulta & " ORDER BY MONGS"
                Else
                    sConsulta = sConsulta & " ORDER BY FECADJ"
                End If
        End Select
        
    'End If
    
    Set ador = New adodb.Recordset
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    Set ador.ActiveConnection = Nothing
    
    Set DevolverVolumenPorProcesos = ador
End Function

''' <summary>
''' Recuperar los procesos publicados
''' </summary>
''' <param name="Cia">Compa��a</param>
''' <param name="Den">Denonimaci�n</param>
''' <param name="SRV">Servidor de BBDD</param>
''' <param name="BD">Base de Datos</param>
''' <returns>recordset</returns>
''' <remarks>Llamada desde: frmLstProcPub.ObtenerListado;
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 13/01/2011</revision>
Public Function DevolverProcesosPublicados(Cia As String, ID As Integer, Den As String, SRV As String, BD As String) As adodb.Recordset

    Dim ador As adodb.Recordset
    Dim sConsulta As String
    Dim sfsg As String
    
    On Error GoTo Error
    
    sfsg = SRV & "." & BD & ".dbo."
    
    
    sConsulta = "SELECT '" & Cia & "' as CIA, " & ID & " AS ID, '" & Den & "' as DEN,'" & SRV & "' as SRV, '" & BD & "' AS BD" _
        & "       ,PROCE_PROVE.ANYO AS PROCEANO, " _
        & "       PROCE_PROVE.GMN1 AS PROCEGMN1, " _
        & "       PROCE_PROVE.PROCE AS PROCE, " _
        & "       PROCE.DEN AS PROCEDEN, " _
        & "       PROCE_PROVE.FECPUB AS FECPUB, " _
        & "       PROCE_PROVE.PROVE AS PROVE, " _
        & "       PROVE.DEN AS PROVEDEN " _
        & "  FROM " & sfsg & "PROCE_PROVE PROCE_PROVE WITH (NOLOCK) INNER JOIN" _
        & "       " & sfsg & "PROCE PROCE WITH (NOLOCK) ON PROCE_PROVE.ANYO = PROCE.ANYO AND" _
        & "       PROCE_PROVE.GMN1 = PROCE.GMN1 AND" _
        & "       PROCE_PROVE.PROCE = PROCE.COD INNER JOIN " _
        & "       " & sfsg & "PROVE PROVE WITH (NOLOCK) ON PROCE_PROVE.PROVE = PROVE.COD" _
        & " WHERE (PROCE_PROVE.PUB = 1) "
    
    Set ador = New adodb.Recordset
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

    Set ador.ActiveConnection = Nothing
    
    Set DevolverProcesosPublicados = ador
    
    Exit Function
    
Error:
    If Err.Number = "-2147217900" Then
        Set ador = Nothing
    End If
End Function

Public Function DevolverHistoricoEmails(Optional ByVal IdCia As Long, Optional ByVal TipoOrdenacion As Integer) As adodb.Recordset
    Dim ador As adodb.Recordset
    Dim sConsulta As String
    
    sConsulta = "SELECT REGISTRO_EMAIL.FECHA,CIAS.COD AS COD_CIA ,CIAS.DEN AS DEN_CIA ,REGISTRO_EMAIL.PARA,REGISTRO_EMAIL.CC"
    sConsulta = sConsulta & ",REGISTRO_EMAIL.CCO,REGISTRO_EMAIL.DIR_RESPUESTA AS RESPUESTA_A,REGISTRO_EMAIL.SUBJECT,REGISTRO_EMAIL.TIPO,REGISTRO_EMAIL.ACUSE_RECIBO,REGISTRO_EMAIL.CIA,REGISTRO_EMAIL.ID"
    sConsulta = sConsulta & ",ADJUN.ADJUNTOS AS HAYADJUN"
    
    sConsulta = sConsulta & " FROM REGISTRO_EMAIL WITH (NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN CIAS WITH (NOLOCK) ON CIAS.ID = REGISTRO_EMAIL.CIA"
    
    'Adjuntos
    sConsulta = sConsulta & " LEFT JOIN (SELECT COUNT(*) AS ADJUNTOS,CIA,ID_REG FROM REGISTRO_EMAIL_ADJUN WITH (NOLOCK) GROUP BY REGISTRO_EMAIL_ADJUN.CIA,REGISTRO_EMAIL_ADJUN.ID_REG) ADJUN ON REGISTRO_EMAIL.CIA=ADJUN.CIA AND REGISTRO_EMAIL.ID=ADJUN.ID_REG"
    
    If IdCia <> 0 Then
        sConsulta = sConsulta & " WHERE REGISTRO_EMAIL.CIA = " & IdCia
    End If
    
    Select Case TipoOrdenacion
        Case 1  'Fecha
            sConsulta = sConsulta & " ORDER BY REGISTRO_EMAIL.FECHA DESC"
        Case 2 'Cod. cia
            sConsulta = sConsulta & " ORDER BY COD_CIA,REGISTRO_EMAIL.FECHA DESC"
        Case 3 'Den cia
            sConsulta = sConsulta & " ORDER BY DEN_CIA,REGISTRO_EMAIL.FECHA DESC"
        Case 4 'Para
            sConsulta = sConsulta & " ORDER BY REGISTRO_EMAIL.PARA,REGISTRO_EMAIL.FECHA DESC"
        Case 5 'Subject
            sConsulta = sConsulta & " ORDER BY REGISTRO_EMAIL.SUBJECT,REGISTRO_EMAIL.FECHA DESC"
        Case 6 'CC
            sConsulta = sConsulta & " ORDER BY REGISTRO_EMAIL.CC,REGISTRO_EMAIL.FECHA DESC"
        Case 7 'CCo
            sConsulta = sConsulta & " ORDER BY REGISTRO_EMAIL.CCO,REGISTRO_EMAIL.FECHA DESC"
        Case Else
            sConsulta = sConsulta & " ORDER BY REGISTRO_EMAIL.FECHA DESC"
    End Select
    
    Set ador = New adodb.Recordset
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    Set ador.ActiveConnection = Nothing
    
    Set DevolverHistoricoEmails = ador
    
End Function

Public Function DevolverAdjuntosEmails(Optional ByVal IdCia As Long) As adodb.Recordset
    Dim ador As adodb.Recordset
    Dim sConsulta As String
    
    sConsulta = "SELECT CIA,ID_REG,ID,NOM AS NOMBRE"
    sConsulta = sConsulta & " FROM REGISTRO_EMAIL_ADJUN WITH (NOLOCK) "
        
    If IdCia <> 0 Then
        sConsulta = sConsulta & " WHERE CIA = " & IdCia
    End If
    
    Set ador = New adodb.Recordset
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    Set ador.ActiveConnection = Nothing
    
    Set DevolverAdjuntosEmails = ador
End Function
