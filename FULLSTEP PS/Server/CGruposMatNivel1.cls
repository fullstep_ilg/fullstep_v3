VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGruposMatNivel1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CGruposMatNivel1 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************

'Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean

''' <summary>
''' Carga de todos los grupos de materiales de la compa��a
''' </summary>
''' <param name="CiaComp">Compa��a</param>
''' <param optional name="CaracteresInicialesCod">Cod de busqueda</param>
''' <param optional name="CaracteresInicialesDen">Den de busqueda</param>
''' <param optional name="CoincidenciaTotal">Si la busqueda ha de coincidir con la den o el cod</param>
''' <param optional name="OrdenadosPorDen">Ordenacion por denominacion</param>
''' <param optional name="UsarIndice">Si se usa indice para la coleccion</param>
''' <remarks>Llamada desde: frmPROVEBuscar.sdbcGMN1_4Cod_DropDown;
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 13/01/2011</revision>
Public Sub CargarTodosLosGruposMatDeLaCia(ByVal CiaComp As Long, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean)
Dim ador As adodb.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim sfsg As String
Dim BD As String
Dim SRV As String
    
    
    'Primero debemos obtener los datos de SERVIDOR,BD,... de la CiaCompradora
    
    sConsulta = "SELECT FSGS_SRV,FSGS_BD FROM CIAS WITH (NOLOCK) WHERE ID=" & CiaComp
    Set ador = New adodb.Recordset
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
   
    
    If ador.eof Then
        ador.Close
        Set ador = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
    End If
    
    SRV = ador("FSGS_SRV").Value
    BD = ador("FSGS_BD").Value
        
    ador.Close
    
    sfsg = SRV & "." & BD & ".dbo."
  
    sConsulta = "SELECT COD,FECACT,DEN_ENG,DEN_GER,DEN_SPA,QA_NOTIF_USU FROM " & sfsg & "GMN1 AS GMN1 WITH (NOLOCK) "

If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
    
    
   
Else
    If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
        
        If CoincidenciaTotal Then
            sConsulta = sConsulta & " WHERE COD ='" & basUtilidades.DblQuote(CaracteresInicialesCod) & "'"
            sConsulta = sConsulta & " AND " & DevolverDenGMN & "='" & basUtilidades.DblQuote(CaracteresInicialesDen) & "'"
        Else
            
            sConsulta = sConsulta & " WHERE COD >= '" & DblQuote(CaracteresInicialesCod) & "'"
            sConsulta = sConsulta & " AND " & DevolverDenGMN & " >= '" & DblQuote(CaracteresInicialesDen) & "'"
                        
        End If
                    
    Else
        
        If Not (CaracteresInicialesCod = "") Then
            
            If CoincidenciaTotal Then
            
                sConsulta = sConsulta & " WHERE COD ='" & DblQuote(CaracteresInicialesCod) & "'"
            Else
                    
               sConsulta = sConsulta & " WHERE COD >= '" & DblQuote(CaracteresInicialesCod) & "'"
                            
            End If
            
        Else
            
            If CoincidenciaTotal Then
            
                sConsulta = sConsulta & " WHERE " & DevolverDenGMN & "='" & DblQuote(CaracteresInicialesDen) & "'"
            Else
                sConsulta = sConsulta & " WHERE " & DevolverDenGMN & " >= '" & DblQuote(CaracteresInicialesDen) & "'"
                
            End If
            
        End If
    
    End If
           
End If

If OrdenadosPorDen Then
    sConsulta = sConsulta & " ORDER BY " & DevolverDenGMN & ",COD"
Else
    sConsulta = sConsulta & " ORDER BY COD," & DevolverDenGMN
End If
      
ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
         
If ador.eof Then
        
    ador.Close
    Set ador = Nothing
    Exit Sub
      
Else
    
    Set mCol = Nothing
    Set mCol = New Collection
        
    If UsarIndice Then
        
        lIndice = 0
        
        While Not ador.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            Me.Add ador("COD").Value, ador(DevolverDenGMN).Value, lIndice
            ador.MoveNext
            lIndice = lIndice + 1
        Wend
        
    Else
        
        While Not ador.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            Me.Add ador("COD").Value, ador(DevolverDenGMN).Value
            ador.MoveNext
        Wend
    End If
    
    ador.Close
    Set ador = Nothing
    Exit Sub
      
End If
End Sub


Public Function Add(ByVal Cod As String, ByVal Den As String, Optional ByVal varIndice As Variant) As CGrupoMatNivel1
    'create a new object
    Dim objnewmember As CGrupoMatNivel1
    Dim sCod As String
    
    Set objnewmember = New CGrupoMatNivel1
   
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    Set objnewmember.Conexion = mvarConexion
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        sCod = Cod & Mid$("                         ", 1, 20 - Len(Cod))
        mCol.Add objnewmember, sCod
    End If
      
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

Public Property Get Item(vntIndexKey As Variant) As CGrupoMatNivel1
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()

        Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

'Public Function DevolverLosCodigos() As TipoDatosCombo
   
'   Dim Codigos As TipoDatosCombo
'   Dim iCont As Integer
'   ReDim Codigos.Cod(mCol.Count)
'   ReDim Codigos.Den(mCol.Count)
   
'    For iCont = 0 To mCol.Count - 1
        
'        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
'        Codigos.Den(iCont) = mCol(iCont + 1).Den
    
'    Next

'   DevolverLosCodigos = Codigos
   
'End Function
Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

''' <summary>
''' Crea la estructura de materiales
''' </summary>
''' <param name="CiaComp">Compa��a</param>
''' <param optional name="OrdPorDen">Ordenacion por denominacion</param>
''' <remarks>Llamada desde: frmPROVEBuscar.EdicionMateriales;
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 13/01/2011</revision>
Public Sub GenerarEstructuraMateriales(ByVal CiaComp As Long, Optional ByVal OrdPorDen As Boolean)

Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4
Dim sConsulta As String
Dim sCod As String
Dim adoRes As adodb.Recordset

On Error GoTo Error
Set mCol = Nothing
Set mCol = New Collection

 'Primero debemos obtener los datos de SERVIDOR,BD,... de la CiaCompradora
    
    sConsulta = "SELECT FSGS_SRV,FSGS_BD FROM CIAS WITH (NOLOCK) WHERE ID=" & CiaComp
    Set adoRes = New adodb.Recordset
    
    adoRes.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
   
    
    If adoRes.eof Then
        adoRes.Close
        Set adoRes = Nothing
        Exit Sub
    End If
    If IsNull(adoRes("FSGS_SRV").Value) Or IsNull(adoRes("FSGS_BD").Value) Then
        adoRes.Close
        Set adoRes = Nothing
        Exit Sub
    End If
        
    sfsg = adoRes("FSGS_SRV").Value & "." & adoRes("FSGS_BD").Value & ".dbo."
    
    adoRes.Close
    Set adoRes = Nothing

        ' Generamos la coleccion de grupos mat nivel1
        
        sConsulta = "SELECT COD," & DevolverDenGMN & " FROM " & sfsg & "GMN1 WITH (NOLOCK) "
        If OrdPorDen Then
            sConsulta = sConsulta & " ORDER BY " & DevolverDenGMN
        Else
            sConsulta = sConsulta & " ORDER BY COD"
        End If
        Set adoRes = New adodb.Recordset
        adoRes.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        
        If adoRes.eof Then
            adoRes.Close
            Set adoRes = Nothing
            Exit Sub
        End If
        
        While Not adoRes.eof
            Set oGMN1 = Me.Add(adoRes("COD").Value, adoRes(DevolverDenGMN).Value)
            Set oGMN1.GruposMatNivel2 = New CGruposMatNivel2
            Set oGMN1.GruposMatNivel2.Conexion = mvarConexion
            
            adoRes.MoveNext
            Set oGMN1 = Nothing
        Wend
        
        adoRes.Close
        Set adoRes = Nothing
        
               
        ' Generamos la coleccion de grupos mat nivel2
        sConsulta = "SELECT GMN1,COD,FECACT,DEN_ENG,DEN_GER,DEN_SPA,QA_NOTIF_USU FROM " & sfsg & "GMN2 WITH (NOLOCK) "
        
        If OrdPorDen Then
            sConsulta = sConsulta & " ORDER BY " & DevolverDenGMN
        Else
            sConsulta = sConsulta & " ORDER BY COD"
        End If
        Set adoRes = New adodb.Recordset
        adoRes.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        
        If adoRes.eof Then
            adoRes.Close
            Set adoRes = Nothing
            Exit Sub
        End If
        
        While Not adoRes.eof
            sCod = adoRes("GMN1").Value & Mid$("                         ", 1, 20 - Len(adoRes("GMN1").Value))
            Set oGMN1 = Me.Item(sCod)
            Set oGMN2 = oGMN1.GruposMatNivel2.Add(adoRes("GMN1").Value, "", adoRes("COD").Value, adoRes(DevolverDenGMN).Value)
            ' Creo una nueva coleccion para cada uno
            Set oGMN2.GruposMatNivel3 = New CGruposMatNivel3
            Set oGMN2.GruposMatNivel3.Conexion = mvarConexion
            
            adoRes.MoveNext
            Set oGMN1 = Nothing
            Set oGMN2 = Nothing
        Wend
        adoRes.Close
        Set adoRes = Nothing
        
        ' Generamos la coleccion de grupos mat nivel3
        sConsulta = "SELECT GMN1,GMN2,COD,FECACT,DEN_ENG,DEN_GER,DEN_SPA,QA_NOTIF_USU FROM " & sfsg & "GMN3 WITH (NOLOCK) "
        
        If OrdPorDen Then
            sConsulta = sConsulta & " ORDER BY " & DevolverDenGMN
        Else
            sConsulta = sConsulta & " ORDER BY COD"
        End If
        Set adoRes = New adodb.Recordset
        adoRes.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        
        If adoRes.eof Then
            adoRes.Close
            Set adoRes = Nothing
            Exit Sub
        End If
        
        While Not adoRes.eof
            sCod = adoRes("GMN1").Value & Mid$("                         ", 1, 20 - Len(adoRes("GMN1").Value))
            Set oGMN1 = Me.Item(sCod)
            sCod = sCod & adoRes("GMN2").Value & Mid$("                         ", 1, 20 - Len(adoRes("GMN2").Value))
            Set oGMN2 = oGMN1.GruposMatNivel2.Item(sCod)
            Set oGMN3 = oGMN2.GruposMatNivel3.Add(adoRes("GMN1").Value, adoRes("GMN2").Value, adoRes("COD").Value, "", "", adoRes(DevolverDenGMN).Value)
            ' Creo una nueva coleccion para cada uno
            Set oGMN3.GruposMatNivel4 = New CGruposMatNivel4
            Set oGMN3.GruposMatNivel4.Conexion = mvarConexion
            
            adoRes.MoveNext
            Set oGMN1 = Nothing
            Set oGMN2 = Nothing
            Set oGMN3 = Nothing
        Wend
        
        adoRes.Close
        Set adoRes = Nothing
        
        ' Generamos la coleccion de grupos mat nivel4
        sConsulta = "SELECT GMN1,GMN2,GMN3,COD,FECACT,DEN_ENG,DEN_GER,DEN_SPA FROM " & sfsg & "GMN4 WITH (NOLOCK) "
        
        If OrdPorDen Then
            sConsulta = sConsulta & " ORDER BY " & DevolverDenGMN
        Else
            sConsulta = sConsulta & " ORDER BY COD"
        End If
        Set adoRes = New adodb.Recordset
        adoRes.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
            
        If adoRes.eof Then
            adoRes.Close
            Set adoRes = Nothing
            Exit Sub
        End If
        
        While Not adoRes.eof
            sCod = adoRes("GMN1").Value & Mid$("                         ", 1, 20 - Len(adoRes("GMN1").Value))
            Set oGMN1 = Me.Item(sCod)
            sCod = sCod & adoRes("GMN2").Value & Mid$("                         ", 1, 20 - Len(adoRes("GMN2").Value))
            Set oGMN2 = oGMN1.GruposMatNivel2.Item(sCod)
            sCod = sCod & adoRes("GMN3").Value & Mid$("                         ", 1, 20 - Len(adoRes("GMN3").Value))
            Set oGMN3 = oGMN2.GruposMatNivel3.Item(sCod)
            oGMN3.GruposMatNivel4.Add adoRes("GMN1").Value, adoRes("GMN2").Value, adoRes("GMN3").Value, "", "", "", CStr(adoRes("COD").Value), adoRes(DevolverDenGMN).Value
            adoRes.MoveNext
            Set oGMN1 = Nothing
            Set oGMN2 = Nothing
            Set oGMN3 = Nothing
        Wend
           
        adoRes.Close
        Set adoRes = Nothing
   
    Exit Sub
    
Error:
    If Err.Number = "-2147217900" Then
        Exit Sub
    End If

End Sub


