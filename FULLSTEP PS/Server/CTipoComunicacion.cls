VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTipoComunicacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit
Private mvarConexion As CConexion
Private mvarId As Integer
Private mvarCodigo As String
Private mvarDenominacion As String

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum


Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property



Public Property Get ID() As Integer
    ID = mvarId
End Property

Public Property Let ID(ByVal iden As Integer)
    mvarId = iden
End Property

Public Property Get Codigo() As String
    Codigo = mvarCodigo
End Property

Public Property Let Codigo(ByVal codig As String)
    mvarCodigo = codig
End Property

Public Property Get Denominacion() As String
    Denominacion = mvarDenominacion
End Property

Public Property Let Denominacion(ByVal apellid As String)
    mvarDenominacion = apellid
End Property

Public Function ModificarRegistro() As CTESError
Dim ador As ADODB.Recordset
Dim sConsulta As String
Dim oError As CTESError
    
    Set oError = New CTESError
    oError.NumError = 0

On Error GoTo Error:
    mvarConexion.AdoCon.Execute "BEGIN TRANSACTION"
    mvarConexion.AdoCon.Execute "SET XACT_ABORT ON"
    
    sConsulta = "UPDATE COM SET COD='" & DblQuote(mvarCodigo) & "',DEN='" & DblQuote(mvarDenominacion) & "'"
    sConsulta = sConsulta & " WHERE ID=" & mvarId
    
    mvarConexion.AdoCon.Execute sConsulta
    
    mvarConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    mvarConexion.AdoCon.Execute "COMMIT TRANSACTION"
    
    Set ModificarRegistro = oError
    Exit Function
    
Error:
    mvarConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    Set oError = basErrores.TratarError(mvarConexion.AdoCon.Errors)
    
    Set ModificarRegistro = oError
    
    
End Function
Public Function AnyadirRegistro() As CTESError
Dim ador As ADODB.Recordset
Dim sConsulta As String
Dim oError As CTESError
    
    Set oError = New CTESError
    oError.NumError = 0
    
    '********* Precondicion *******************
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CTipoComunicacion.AnyadirRegistro", "No se ha establecido la conexion"
    End If
    ' *****************************************

On Error GoTo Error:

    mvarConexion.AdoCon.Execute "BEGIN TRANSACTION"
    mvarConexion.AdoCon.Execute "SET XACT_ABORT ON"
    'Selecciono el id
    'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperación del mayor valor de la tabla
    sConsulta = "SELECT MAX(ID) FROM COM"
    Set ador = New ADODB.Recordset
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    If IsNull(ador(0).Value) Then
        mvarId = 1
    Else
        mvarId = ador(0).Value + 1
    End If
    
    ador.Close
    Set ador = Nothing
    
    sConsulta = "INSERT INTO COM (ID,COD,DEN) VALUES "
    sConsulta = sConsulta & "(" & mvarId & ",'" & DblQuote(mvarCodigo) & "','" & DblQuote(mvarDenominacion) & "')"
    mvarConexion.AdoCon.Execute sConsulta
    
    mvarConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    mvarConexion.AdoCon.Execute "COMMIT TRANSACTION"
    
    Set AnyadirRegistro = oError
    Exit Function
    
Error:
    mvarConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    Set oError = basErrores.TratarError(mvarConexion.AdoCon.Errors)
    Set AnyadirRegistro = oError
    
    
End Function


Public Function EliminarRegistro() As CTESError
Dim ador As ADODB.Recordset
Dim sConsulta As String
Dim oError As CTESError
Dim sfsg As String

    On Error GoTo Error
    
    Set oError = New CTESError
    oError.NumError = 0
    
    mvarConexion.AdoCon.Execute "BEGIN TRANSACTION"
    mvarConexion.AdoCon.Execute "SET XACT_ABORT ON"
    
    mvarConexion.AdoCon.Execute "DELETE FROM COM WHERE ID =" & mvarId
    
    mvarConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    mvarConexion.AdoCon.Execute "COMMIT TRAN"
    
    Set EliminarRegistro = oError
    
    Exit Function

Error:
    mvarConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    Set oError = basErrores.TratarError(mvarConexion.AdoCon.Errors)
    Set EliminarRegistro = oError
    
         
End Function


Private Sub Class_Terminate()
    Set mvarConexion = Nothing
End Sub
