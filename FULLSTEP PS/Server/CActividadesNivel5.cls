VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CActividadesNivel5"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CActividadesNivel5 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion

Public Function Add(ByVal ACN1 As Integer, ByVal ACN2 As Integer, ByVal ACN3 As Integer, ByVal ACN4 As Integer, ByVal ID As Integer, ByVal Cod As String, ByVal Den As String, Optional ByVal varIndice As Variant, Optional ByVal varAsign As Variant, Optional ByVal varPendiente As Variant, Optional ByVal FecAct As Date) As CActividadNivel5
    'create a new object
    Dim objnewmember As CActividadNivel5
    Dim sCod As String
    
    Set objnewmember = New CActividadNivel5
   
    objnewmember.Cod = Cod
    objnewmember.ID = ID
    objnewmember.Den = Den
    
    If Not IsMissing(varAsign) Then
        If varAsign Then
            objnewmember.Asignada = True
        Else
            objnewmember.Asignada = False
        End If
        
    End If
    
    If Not IsMissing(varPendiente) Then
        If varPendiente Then
            objnewmember.Pendiente = True
        Else
            objnewmember.Pendiente = False
        End If
    Else
            objnewmember.Pendiente = False
    End If
    objnewmember.ACN1 = ACN1
    objnewmember.ACN2 = ACN2
    objnewmember.ACN3 = ACN3
    objnewmember.ACN4 = ACN4
    
    If Not IsMissing(FecAct) And Not IsNull(FecAct) Then
        objnewmember.FecAct = FecAct
    End If
    
    Set objnewmember.Conexion = mvarConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        sCod = CStr(ACN1) & Mid$("                         ", 1, 10 - Len(CStr(ACN1)))
        sCod = sCod & CStr(ACN2) & Mid$("                         ", 1, 10 - Len(CStr(ACN2)))
        sCod = sCod & CStr(ACN3) & Mid$("                         ", 1, 10 - Len(CStr(ACN3)))
        sCod = sCod & CStr(ACN4) & Mid$("                         ", 1, 10 - Len(CStr(ACN4)))
        
        sCod = sCod & CStr(ID) & Mid$("                         ", 1, 10 - Len(CStr(ID)))
        
        mCol.Add objnewmember, sCod
    End If
      
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

Public Property Get Item(vntIndexKey As Variant) As CActividadNivel5
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()

        Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub
Public Function DevolverTodasLasActividades(ByVal CodIdioma As String, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal TipoBusqueda As Integer, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean)

Dim ador As adodb.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim sDen As String

    '********* Precondicion **************************************
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CActividadesNivel2.DevolverTodasLasActividades", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
    If CodIdioma <> "" Then
        sDen = "ACT5.DEN_" & CodIdioma
    Else
         sDen = "ACT5.DEN_" & g_udtParametrosGenerales.g_sIdioma
    End If

    sConsulta = "SELECT ACT5.ID,ACT5.COD," & sDen & ",ACT1.COD AS ACT1COD,ACT2.COD AS ACT2COD,ACT3.COD AS ACT3COD,ACT4.COD AS ACT4COD,ACT1.ID,ACT2.ID,ACT3.ID,ACT4.ID "
    sConsulta = sConsulta & " FROM ACT5 WITH (NOLOCK) INNER JOIN ACT1 WITH (NOLOCK) ON ACT5.ACT1=ACT1.ID INNER JOIN ACT2 WITH (NOLOCK) ON ACT5.ACT2=ACT2.ID AND ACT5.ACT1=ACT2.ACT1 "
    sConsulta = sConsulta & " INNER JOIN ACT3 WITH (NOLOCK) ON ACT5.ACT3=ACT3.ID AND ACT5.ACT1=ACT3.ACT1 AND ACT5.ACT2=ACT3.ACT2 INNER JOIN ACT4 WITH (NOLOCK) ON ACT5.ACT4=ACT4.ID AND ACT5.ACT1=ACT4.ACT1 AND ACT5.ACT2=ACT4.ACT2 AND ACT5.ACT3=ACT4.ACT3"

    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
    
    
   
    Else
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
        
            Select Case TipoBusqueda
            
            Case -1
        
                sConsulta = sConsulta & " WHERE ACT5.COD ='" & basUtilidades.DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND " & sDen & "='" & basUtilidades.DblQuote(CaracteresInicialesDen) & "'"
            
            Case 0
            
                sConsulta = sConsulta & " WHERE ACT5.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                sConsulta = sConsulta & " AND " & sDen & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                       
            Case 1
                
                sConsulta = sConsulta & " WHERE ACT5.COD >= '" & DblQuote(CaracteresInicialesCod) & "'"
                sConsulta = sConsulta & " AND " & sDen & ">= '" & DblQuote(CaracteresInicialesDen) & "'"
                        
            End Select
                    
        Else
            
            If Not (CaracteresInicialesCod = "") Then
            
                Select Case TipoBusqueda
            
                Case -1
            
                    sConsulta = sConsulta & " WHERE ACT5.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                
                Case 0
                    
                    sConsulta = sConsulta & " WHERE ACT5.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                
                Case 1
                    
                    sConsulta = sConsulta & " WHERE ACT5.COD >= '" & DblQuote(CaracteresInicialesCod) & "'"
                            
                End Select
            
            Else
            
                Select Case TipoBusqueda
            
                Case -1

                    sConsulta = sConsulta & " WHERE " & sDen & "='" & DblQuote(CaracteresInicialesDen) & "'"
                
                Case 0
            
                    sConsulta = sConsulta & " WHERE " & sDen & " LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                  
                Case 1
                    sConsulta = sConsulta & " WHERE " & sDen & ">= '" & DblQuote(CaracteresInicialesDen) & "'"
                
                End Select
            
            End If
    
        End If
           
    End If

    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY " & sDen & " ,ACT5.COD"
    Else
        sConsulta = sConsulta & " ORDER BY ACT5.COD," & sDen
    End If
      
    Set ador = New adodb.Recordset
        
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

    Set ador.ActiveConnection = Nothing
    
    If ador.RecordCount = 0 Then
        Set DevolverTodasLasActividades = Nothing
    Else
        Set DevolverTodasLasActividades = ador
    End If
    
    
End Function







