VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CIdioma"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Private mvarCod As String
Private mvarDen As String
Private mvarId As Integer

Public Property Get Cod() As String
Cod = mvarCod
End Property

Public Property Let Cod(ByVal Data As String)
Let mvarCod = Data
End Property

Public Property Get Den() As String
Den = mvarDen
End Property

Public Property Let Den(ByVal Data As String)
  Let mvarDen = Data
End Property

Public Property Get Id() As String
Id = mvarId
End Property

Public Property Let Id(ByVal Data As String)
  Let mvarId = Data
End Property


