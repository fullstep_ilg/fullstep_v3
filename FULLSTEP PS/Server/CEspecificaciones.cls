VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEspecificaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CEspecificaciones **********************************
'*             Autor :  Elena Mu�oz
'*             Creada : 11/12/01
'****************************************************************

Option Explicit

Private m_oConexion As CConexion
Private mCol As Collection

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property

Public Property Get Item(vntIndexKey As Variant) As CEspecificacion
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Function Add(ByVal sNombre As String, ByVal dtFecha As Date, ByVal iID As Integer, Optional ByVal oCia As CCia, Optional ByVal vComentario As Variant, Optional ByVal vIndice As Variant, Optional ByVal vSize As Variant) As CEspecificacion
    'create a new object
Dim sCod As String
Dim objnewmember As CEspecificacion
    
    Set objnewmember = New CEspecificacion
   
    If Not IsMissing(oCia) Then
        Set objnewmember.Cia = oCia
    End If
    
    objnewmember.ID = iID
    objnewmember.Nombre = sNombre
    If IsMissing(vComentario) Then
        objnewmember.Comentario = Null
    Else
        objnewmember.Comentario = vComentario
    End If
    
    objnewmember.Fecha = dtFecha
    
    If Not IsMissing(vSize) Then
        objnewmember.DataSize = vSize
    End If
    
    Set objnewmember.Conexion = m_oConexion
    
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        'set the properties passed into the method
       mCol.Add objnewmember, CStr(iID)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub


