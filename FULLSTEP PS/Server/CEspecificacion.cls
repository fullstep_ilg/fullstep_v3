VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEspecificacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CEspecificacion **********************************
'*             Autor :  Elena Mu�oz
'*             Creada : 11/12/01
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_vComentario As Variant
Private m_oCia As CCia
Private m_lDataSize As Long 'Tamanyo del contenido en bytes del campo mvarData
Private m_dtFecact As Date
Private m_iId As Integer
Private m_vIndice As Variant 'Nos indica la posicion secuencial dentro de una Coleccion
Private m_sNombre As String

Private m_oConexion As CConexion
Private m_adores As adodb.Recordset
Private m_adoComm As adodb.Command

'Private m_adores As ADODB.Recordset
'Private m_adoComm As ADODB.Command

Public Property Get Comentario() As Variant
    Comentario = m_vComentario
End Property

Public Property Let Comentario(ByVal vData As Variant)
    m_vComentario = vData
End Property

Public Property Set Cia(ByVal oCia As CCia)
    Set m_oCia = oCia
End Property

Public Property Get Cia() As CCia
    Set Cia = m_oCia
End Property

Public Property Get DataSize() As Long
    DataSize = m_lDataSize
End Property

Public Property Let DataSize(ByVal Data As Long)
    m_lDataSize = Data
End Property

Public Property Let Fecha(ByVal Dat As Date)
    m_dtFecact = Dat
End Property

Public Property Get Fecha() As Date
    Fecha = m_dtFecact
End Property

Public Property Let ID(ByVal i As Integer)
    m_iId = i
End Property

Public Property Get ID() As Integer
    ID = m_iId
End Property

Public Property Let Indice(ByVal vIndice As Variant)
    m_vIndice = vIndice
End Property

Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Public Property Get Nombre() As String
    Nombre = m_sNombre
End Property

Public Property Let Nombre(ByVal sNombre As String)
    m_sNombre = sNombre
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Function ComenzarLecturaData() As CTESError
Dim adoRes As adodb.Recordset
Dim sConsulta As String
Dim oError As CTESError

'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    Err.Raise vbObjectError + 613, "CEspecificacion.ComenzarLecturaData", "No se ha establecido la conexion"
    Exit Function
End If
'*************************************************************
        
    Set oError = New CTESError
    oError.NumError = 0
  
    If Not m_oCia Is Nothing Then
    
        'Comprobamos si existe a�n la compa��a
        sConsulta = "SELECT * FROM CIAS WITH (NOLOCK) WHERE ID=" & m_oCia.ID
        Set adoRes = New adodb.Recordset
        adoRes.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        If adoRes.eof Then
            adoRes.Close
            Set adoRes = Nothing
            oError.NumError = FSPSServer.ErroresSummit.TESDatoEliminado
            oError.Arg1 = "CIA"
            Set EliminarEspecificacion = oError
            Exit Function
        End If
        adoRes.Close
        Set adoRes = Nothing
    
        sConsulta = "EXEC SP_DEVOLVER_PROVE_ESP @IDPROVE=" & m_oCia.ID & ", @IDESP= ?"
        Set m_adoComm = New adodb.Command
        m_adoComm.ActiveConnection = m_oConexion.AdoCon
        m_adoComm.CommandText = sConsulta
        
        m_adoComm.Parameters.Item(0).Value = m_iId
        m_adoComm.CommandType = adCmdText
        Set m_adores = m_adoComm.Execute
    
        If m_adores.eof Then
            oError.NumError = FSPSServer.ErroresSummit.TESDatoEliminado
            oError.Arg1 = "CIA_ESP"
            m_lDataSize = 0
            m_adores.Close
            Set m_adores = Nothing
            Set m_adoComm = Nothing
            Set ComenzarLecturaData = oError
            Exit Function
        End If
    
        m_lDataSize = m_adores("TAMANYO").Value

    End If
        
    Set ComenzarLecturaData = oError
    Exit Function

Error:
    
    oError = basErrores.TratarError(m_oConexion.AdoCon)
    Set ComenzarLecturaData = oError
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
        Set m_adoComm = Nothing
    End If
    
End Function

Private Sub Class_Initialize()
    m_lDataSize = 0
End Sub

Private Sub Class_Terminate()
    Set m_adores = Nothing
    Set m_adoComm = Nothing
    Set m_oConexion = Nothing
End Sub

Public Function EliminarEspecificacion() As CTESError
Dim sConsulta As String
Dim oError As CTESError
Dim adoRes As adodb.Recordset

    On Error GoTo Error
    
    Set oError = New CTESError
    oError.NumError = 0
    
    m_oConexion.AdoCon.Execute "BEGIN TRANSACTION"
    m_oConexion.AdoCon.Execute "SET XACT_ABORT ON"
    
    If Not m_oCia Is Nothing Then
        sConsulta = "SELECT * FROM CIAS WITH (NOLOCK) WHERE ID=" & m_oCia.ID
    End If
    
    Set adoRes = New adodb.Recordset
    adoRes.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

    If adoRes.eof Then
        adoRes.Close
        Set adoRes = Nothing
        oError.NumError = FSPSServer.ErroresSummit.TESDatoEliminado
        oError.Arg1 = "CIA"
        Set EliminarEspecificacion = oError
        Exit Function
    End If
    
    adoRes.Close
    Set adoRes = Nothing
    
    If Not m_oCia Is Nothing Then
        m_oConexion.AdoCon.Execute "DELETE FROM CIAS_ESP WHERE ID=" & m_iId & " AND CIA=" & m_oCia.ID
    End If
    
    m_oConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    m_oConexion.AdoCon.Execute "COMMIT TRANSACTION"
    
    Set EliminarEspecificacion = oError
    
    Exit Function

Error:

    Set oError = basErrores.TratarError(m_oConexion.AdoCon.Errors)
    m_oConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    Set EliminarEspecificacion = oError

End Function


Public Function ModificarEspecificacion() As CTESError
Dim sConsulta As String
Dim oError As CTESError
Dim adoRes As adodb.Recordset
    
    Set oError = New CTESError
    oError.NumError = 0

On Error GoTo Error:
    
    m_oConexion.AdoCon.Execute "BEGIN TRANSACTION"
    m_oConexion.AdoCon.Execute "SET XACT_ABORT ON"
    
    If Not m_oCia Is Nothing Then
        
        sConsulta = "SELECT * FROM CIAS WITH (NOLOCK) WHERE ID=" & m_oCia.ID
    
        Set adoRes = New adodb.Recordset
        adoRes.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        If adoRes.eof Then
            adoRes.Close
            Set adoRes = Nothing
            oError.NumError = FSPSServer.ErroresSummit.TESDatoEliminado
            oError.Arg1 = "CIA"
            Set ModificarEspecificacion = oError
            Exit Function
        End If
        
        adoRes.Close
        Set adoRes = Nothing
    
        sConsulta = "UPDATE CIAS_ESP SET COM='" & NullToStr(m_vComentario) & "',NOM='" & DblQuote(m_sNombre) & "'"
        sConsulta = sConsulta & " WHERE CIA=" & m_oCia.ID & " AND ID=" & m_iId
        m_oConexion.AdoCon.Execute sConsulta
    
    End If
    
    m_oConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    m_oConexion.AdoCon.Execute "COMMIT TRANSACTION"
       
    Set ModificarEspecificacion = oError
    Exit Function
    
Error:
    Set oError = basErrores.TratarError(m_oConexion.AdoCon.Errors)
    m_oConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    Set ModificarEspecificacion = oError
    
End Function

''' <summary>
''' Lee el Adjunto
''' </summary>
''' <param name="lCiaComp">cia compradora</param>
''' <param name="Tamanyo">tama�o</param>
''' <param name="sFileName">nombre fichero</param>
''' <remarks>Llamada desde: frmCompanias; Tiempo m�ximo: 0,0</remarks>
Public Sub LeerAdjunto(ByVal lCiaComp As Long, ByVal lCia As Long, Optional ByVal Tamanyo As Long, Optional ByVal sFileName As String)
    Dim objDom As Object
    Dim objXmlHttp As Object
    Dim parser As Object
    Dim XmlBody As String
    Dim strUrl As String
    Dim strSoapAction As String
    Dim strResultado As String
    Dim rutaWS As String
    
    ' Crea objetos DOMDocument y XMLHTTP
    Set objDom = CreateObject("MSXML2.DOMDocument")
    Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
    
    rutaWS = basUtilidades.ObtenerRutaWebService(m_oConexion)

    strUrl = rutaWS & "/FSP_AdjuntosPortal.asmx"
    strSoapAction = "http://tempuri.org/FSP_AdjuntosPortal/AdjuntosPortal/LeerEspecificacionCia"
           
    Dim Chunks As Long
    Dim Fragment As Long
    Dim arrBytes() As Byte
    Dim datafile As Integer
    Dim ChunkNumber As Long
    Dim vChunkSize As Variant
    
    'Obtener tama�o del chunk
    Dim oParam As CGestorParametros
    Set oParam = New CGestorParametros
    Set oParam.Conexion = m_oConexion
    vChunkSize = oParam.DevolverParametroInternoGS(lCiaComp, "CHUNK_SIZE")
    If IsNull(vChunkSize) Or IsEmpty(vChunkSize) Then vChunkSize = giChunkSize
    
    Chunks = Tamanyo \ vChunkSize
    Fragment = Tamanyo Mod vChunkSize
    
    If Fragment > 0 Then ' Si no ocupa  justo un chunk
        Chunks = Chunks + 1
    End If

    datafile = 1
    
    'Abrimos el fichero para escritura binaria
    Open sFileName For Binary Access Write As datafile
    
    For ChunkNumber = 1 To Chunks
        XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
                "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
                "<soap:Body>" & _
                "<LeerEspecificacionCia xmlns=""http://tempuri.org/FSP_AdjuntosPortal/AdjuntosPortal"">" & _
                "<Id>" & m_iId & "</Id>" & _
                "<Cia>" & lCia & "</Cia>" & _
                "<ChunkNumber>" & ChunkNumber & "</ChunkNumber>" & _
                "<ChunkSize>" & vChunkSize & "</ChunkSize>" & _
                "</LeerEspecificacionCia>" & _
                "</soap:Body>" & _
                "</soap:Envelope>"
    
        ' Carga XML
        objDom.async = True
        objDom.loadXML XmlBody
        ' Abre el webservice
        objXmlHttp.Open "POST", strUrl, True
        ' Crea headings
        objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
        objXmlHttp.setRequestHeader "SOAPAction", strSoapAction
     
        ' Envia XML command
        objXmlHttp.Send objDom.xml
        'Esperar hasta que se termine de ejecutar
        While Not objXmlHttp.ReadyState = 4
            DoEvents
        Wend
           
        Set parser = CreateObject("MSXML2.DOMDocument")
        strResultado = objXmlHttp.ResponseText
        parser.loadXML strResultado
        
        If InStr(strResultado, "LeerEspecificacionCiaResult") Then
            strResultado = parser.LastChild.Text
            arrBytes = DecodeBase64(strResultado)
            Put datafile, , arrBytes
        End If
    Next
    Close datafile
    ' Cierra object
    Set objXmlHttp = Nothing
End Sub

''' <summary>
''' Guardar adjunto en tabla CIA_ESP del Portal a trav�s de FSNWebService
''' </summary>
''' <param name="Cia">id de compania compradora</param>
''' <param name="sTemp">carpeta temporal</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: usuppal\uploadserver.asp; Tiempo m�ximo: 0,1</remarks>
''' Revisada EPB 31/08/2015
Public Function guardarEspecificacionCia(ByVal lCiaComp As Long, ByVal Cia As Long, ByVal sTemp As String) As CTESError
    Dim adoComm As adodb.Command
    Dim adoPar As adodb.Parameter
    Dim F As Integer
    Dim fl As Long
    Dim chunk() As Byte
    Dim oTESError As CTESError
    Dim fso As Object
    Dim objDom As Object
    Dim objXmlHttp As Object
    Dim parser As Object
    Dim XmlBody As String
    Dim strUrl As String
    Dim strSoapAction As String
    Dim strResultado As String
    Dim strBase64 As String
    Dim nId As Long
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim adoRes As New adodb.Recordset
    Dim iNum As Integer
    Dim rutaWS As String
    Dim partesRutaAdjun() As String
    Dim nomAdjun As String
    
    Set oTESError = New CTESError
    
    oTESError.NumError = 0
    
    On Error GoTo Error
    
    partesRutaAdjun = Split(sTemp, "\")
    nomAdjun = partesRutaAdjun(UBound(partesRutaAdjun))
    
    ' Crea objetos DOMDocument y XMLHTTP
    Set objDom = CreateObject("MSXML2.DOMDocument")
    Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
    
    rutaWS = basUtilidades.ObtenerRutaWebService(m_oConexion)
    
    strUrl = rutaWS & "/FSP_AdjuntosPortal.asmx"
    
   'Buscamos el numero de Especificacion maximo
    sConsulta = "SELECT MAX(ID) AS NUM FROM CIAS_ESP WHERE CIA=" & Cia
    adoRes.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    If adoRes.eof Then
        iNum = 1
    Else
        iNum = NullToDbl0(adoRes.Fields("NUM")) + 1
    End If
    adoRes.Close
    Set adoRes = Nothing
    nId = iNum
    
    Dim datafile As Integer
    Dim sFileName As String
    Dim Chunks As Long
    Dim Fragment As Long
    Dim iChunk As Integer
    Dim vChunkSize As Variant
    Dim rutaFicheroTemporal As String
    
    rutaFicheroTemporal = ""
    
    'Obtener tama�o del chunk
    Dim oParam As CGestorParametros
    Set oParam = New CGestorParametros
    Set oParam.Conexion = m_oConexion
    vChunkSize = oParam.DevolverParametroInternoGS(lCiaComp, "CHUNK_SIZE")
    If IsNull(vChunkSize) Or IsEmpty(vChunkSize) Then vChunkSize = giChunkSize
    
    datafile = 1
    'Abrimos el fichero para lectura binaria
    Open sTemp For Binary Access Read As datafile
    fl = LOF(datafile)    ' Length of data in file
    If fl = 0 Then
        Close datafile
    End If
    ' Se lo asignamos en bloques a la especificacion
    Chunks = fl \ vChunkSize
    Fragment = fl Mod vChunkSize
    
    If Fragment > 0 Then ' Si no ocupa  justo un chunk
        Chunks = Chunks + 1
    End If
    
    ReDim chunk(vChunkSize - 1)
    
    'Pasar el archivo a carpeta temporal
    '-----------------------------------
    For iChunk = 1 To Chunks
        If iChunk = Chunks And Fragment > 0 Then
            ReDim chunk(Fragment - 1)
        End If
        Get datafile, , chunk()
        
        strBase64 = EncodeBase64(chunk)
        strSoapAction = "http://tempuri.org/FSP_AdjuntosPortal/AdjuntosPortal/GrabarEspecificacionCiaCarpetaTemporal"
        
        XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
                "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
                "<soap:Body>" & _
                "<GrabarEspecificacionCiaCarpetaTemporal xmlns=""http://tempuri.org/FSP_AdjuntosPortal/AdjuntosPortal"">" & _
                "<Especificacion>" & strBase64 & "</Especificacion>" & _
                "<Nombre>" & m_sNombre & "</Nombre>" & _
                "<RutaTemporal>" & rutaFicheroTemporal & "</RutaTemporal>" & _
                "</GrabarEspecificacionCiaCarpetaTemporal>" & _
                "</soap:Body>" & _
                "</soap:Envelope>"

        ' Carga XML
        objDom.async = True
        objDom.loadXML XmlBody
        ' Abre el webservice
        objXmlHttp.Open "POST", strUrl, True
        ' Crea headings
        objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
        objXmlHttp.setRequestHeader "SOAPAction", strSoapAction
        
        ' Envia XML command
        objXmlHttp.Send objDom.xml
        'Esperar hasta que se termine de ejecutar
        While Not objXmlHttp.ReadyState = 4
            DoEvents
        Wend
           
        Set parser = CreateObject("MSXML2.DOMDocument")
        strResultado = objXmlHttp.ResponseText
        parser.loadXML strResultado
        
        If InStr(strResultado, "GrabarEspecificacionCiaCarpetaTemporalResult") Then
            strResultado = parser.LastChild.Text
        End If
        Dim rdo() As String
        If InStr(1, strResultado, "#") > 0 Then
            rdo = Split(strResultado, "#", , vbTextCompare)
            If rdo(0) = "1" Then
                rutaFicheroTemporal = rdo(1)
            End If
        End If
    Next
    
    Close datafile
    
    'Grabar el archivo en BD
    '-----------------------
    strSoapAction = "http://tempuri.org/FSP_AdjuntosPortal/AdjuntosPortal/GrabarEspecificacionCiaBD"
           
    XmlBody = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
            "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _
            "<soap:Body>" & _
            "<GrabarEspecificacionCiaBD xmlns=""http://tempuri.org/FSP_AdjuntosPortal/AdjuntosPortal"">" & _
            "<RutaTemporal>" & rutaFicheroTemporal & "</RutaTemporal>" & _
            "<Nombre>" & m_sNombre & "</Nombre>" & _
            "<Id>" & nId & "</Id>" & _
            "<Cia>" & Cia & "</Cia>" & _
            "<Coment>" & m_vComentario & "</Coment>" & _
            "</GrabarEspecificacionCiaBD>" & _
            "</soap:Body>" & _
            "</soap:Envelope>"
         
    ' Carga XML
    objDom.async = True
    objDom.loadXML XmlBody
    ' Abre el webservice
    objXmlHttp.Open "POST", strUrl, True
    ' Crea headings
    objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
    objXmlHttp.setRequestHeader "SOAPAction", strSoapAction
 
    ' Envia XML command
    objXmlHttp.Send objDom.xml
    'Esperar hasta que se termine de ejecutar
    While Not objXmlHttp.ReadyState = 4
        DoEvents
    Wend
       
    Set parser = CreateObject("MSXML2.DOMDocument")
    strResultado = objXmlHttp.ResponseText
    parser.loadXML strResultado
    
    If InStr(strResultado, "GrabarEspecificacionCiaBDResult") Then
        strResultado = parser.LastChild.Text
    End If
    
    ' Cierra object
    Set objXmlHttp = Nothing

    Dim ArrayAdjunto() As String
    ArrayAdjunto = Split(strResultado, "#", , vbTextCompare)
    m_iId = ArrayAdjunto(0)
    m_lDataSize = ArrayAdjunto(1)

fin:
    Set guardarEspecificacionCia = oTESError
    Set fso = CreateObject("SCRIPTING.FILESYSTEMOBJECT")
    fso.DeleteFile (sTemp)
    Set fso = Nothing
    Exit Function
Error:
    If m_oConexion.AdoCon.Errors.Count > 0 Then
        Set oTESError = TratarError(m_oConexion.AdoCon.Errors)
    Else
        oTESError.NumError = TESOtroErrorNOBD
        
    End If

    Resume fin
End Function

