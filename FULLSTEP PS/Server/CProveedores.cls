VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CProveedores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private mCol As Collection
Private mvarConexion As CConexion

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una companya de la coleccion
    ''' * Recibe: Indice de la companya a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property
Public Property Get Item(vntIndexKey As Variant) As CProveedor


    ''' * Objetivo: Recuperar una companya de la coleccion
    ''' * Recibe: Indice de la companya a recuperar
    ''' * Devuelve: companya correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property


Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

Public Function Add(ByVal idCiaComp As Long, _
                    ByVal Cod As String, _
                    ByVal Den As String, _
                    ByVal BDGS As String, _
                    ByVal SRVGS As String, _
                    Optional ByVal NIF As Variant, _
                    Optional ByVal dir As Variant, _
                    Optional ByVal pob As Variant, _
                    Optional ByVal CP As Variant, _
                    Optional ByVal codmon As Variant, _
                    Optional ByVal codpai As Variant, _
                    Optional ByVal codprovi As Variant, _
                    Optional ByVal denmon As Variant, _
                    Optional ByVal denpai As Variant, _
                    Optional ByVal denprovi As Variant) As CProveedor
Dim objnewmember As CProveedor
Set objnewmember = New CProveedor

With objnewmember
    Set .Conexion = mvarConexion
    .idCiaComp = idCiaComp
    .Cod = Cod
    .Den = Den
    .BaseDatosGS = BDGS
    .ServidorGS = SRVGS
    
    If Not IsMissing(NIF) Then
        .NIF = NIF
    End If
    If Not IsMissing(dir) Then
        .Direccion = dir
    End If
    If Not IsMissing(pob) Then
        .Poblacion = pob
    End If
    If Not IsMissing(CP) Then
        .CP = CP
    End If
    If Not IsMissing(codmon) Then
        .codmon = codmon
    End If
    If Not IsMissing(codpai) Then
        .CodPais = codpai
    End If
    If Not IsMissing(codprovi) Then
        .codprovi = codprovi
    End If
    If Not IsMissing(denmon) Then
        .denmon = denmon
    End If
    If Not IsMissing(denpai) Then
        .DenPais = denpai
    End If
    If Not IsMissing(denprovi) Then
        .denprovi = denprovi
    End If
End With
    mCol.Add objnewmember, CStr(Cod)
    
    Set Add = objnewmember
    Set objnewmember = Nothing



End Function

''' <summary>
''' Devuelve recordset de proveedores
''' </summary>
''' <param name="NumMaximo">N�mero m�ximo de proveedores en el recordset</param>
''' <param name="CiaComp">Compa��a</param>
''' <param optional name="vCod">C�digo</param>
''' <param optional name="vDen">Denominaci�n</param>
''' <param optional name="vPais">Pa�s</param>
''' <param optional name="Provincia">Provincia</param>
''' <param optional name="vGMN1">GMN1</param>
''' <param optional name="vGMN2">GMN2</param>
''' <param optional name="vGMN3">GMN3</param>
''' <param optional name="vGMN4">GMN4</param>
''' <param optional name="bOrdenadosPorDen">Se ordena por Denominaci�n</param>
''' <returns>recordset</returns>
''' <remarks>Llamada desde: frmPROVEBuscar.sdbcProveCod_DropDown;
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 13/01/2011</revision>
Public Function devolverTodosLosProveedores(ByVal NumMaximo As Integer, ByVal CiaComp As Long, Optional ByVal vCod As String, Optional ByVal vDen As String, Optional ByVal vPais As String, Optional ByVal Provincia As String, Optional ByVal vGMN1 As String, Optional ByVal vGMN2 As String, Optional ByVal vGMN3 As String, Optional ByVal vGMN4 As String, Optional ByVal bOrdenadosPorDen As Boolean = False) As adodb.Recordset


Dim sConsulta As String
Dim sWhere As String
Dim lIndice As Long
Dim iNumGrups As Integer
Dim sfsg As String
Dim BD As String
Dim SRV As String
Dim ador As adodb.Recordset
    
    On Error GoTo Error
    
    'Primero debemos obtener los datos de SERVIDOR,BD,... de la CiaCompradora
    
    sConsulta = "SELECT FSGS_SRV,FSGS_BD FROM CIAS WITH (NOLOCK) WHERE ID=" & CiaComp
    Set ador = New adodb.Recordset
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        
    If ador.eof Then
        ador.Close
        Set ador = Nothing
        Set devolverTodosLosProveedores = Nothing
        Exit Function
    End If
    
    SRV = ador("FSGS_SRV").Value
    BD = ador("FSGS_BD").Value
        
    ador.Close
    Set ador = Nothing
    
    sfsg = SRV & "." & BD & ".dbo."
    
    
    sConsulta = "SELECT distinct PROVE.COD,PROVE.DEN "
    sConsulta = sConsulta & " FROM " & sfsg & "PROVE AS PROVE WITH (NOLOCK) LEFT JOIN " & sfsg & "PAI AS PAI WITH (NOLOCK) ON PROVE.PAI=PAI.COD"
    sConsulta = sConsulta & " LEFT JOIN " & sfsg & "PROVI AS PROVI WITH (NOLOCK) ON PROVE.PAI = PROVI.PAI AND PROVE.PROVI=PROVI.COD"
    sWhere = " WHERE 1=1 "
    
    If Not IsMissing(vCod) And vCod <> "" Then
        sWhere = sWhere & " AND PROVE.COD ='" & DblQuote(vCod) & "'"
    End If
    
    If Not IsMissing(vDen) And vDen <> "" Then
        sWhere = sWhere & " AND PROVE.DEN LIKE '" & DblQuote(vDen) & "%'"
    End If
    
    If Not IsMissing(vPais) And vPais <> "" Then
        sWhere = sWhere & " AND PROVE.PAI ='" & DblQuote(vPais) & "'"
    End If
    
    If Not IsMissing(Provincia) And Provincia <> "" Then
        sWhere = sWhere & " AND PROVE.PROVI ='" & DblQuote(Provincia) & "'"
    End If
    If Not IsMissing(vGMN1) And vGMN1 <> "" Then
        sConsulta = sConsulta & " INNER JOIN " & sfsg & "PROVE_GMN4 PROVE_GMN4 WITH (NOLOCK) ON PROVE_GMN4.PROVE = PROVE.COD AND PROVE_GMN4.GMN1='" & vGMN1 & "'"
        If Not IsMissing(vGMN2) And vGMN2 <> "" Then
            sConsulta = sConsulta & " AND PROVE_GMN4.GMN2='" & vGMN2 & "'"
            If Not IsMissing(vGMN3) And vGMN3 <> "" Then
                sConsulta = sConsulta & " AND PROVE_GMN4.GMN3 ='" & vGMN3 & "'"
                If Not IsMissing(vGMN4) And vGMN4 <> "" Then
                    sConsulta = sConsulta & " AND PROVE_GMN4.GMN4 ='" & vGMN4 & "'"
                End If
            End If
        End If
    End If
    sConsulta = sConsulta & sWhere
    
    If bOrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY PROVE.DEN "
    Else
        sConsulta = sConsulta & " ORDER BY PROVE.COD "
    End If
    Set ador = New adodb.Recordset
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    If ador.eof Then
    
        ador.Close
        Set ador = Nothing
        Set devolverTodosLosProveedores = Nothing
        Exit Function
    End If
    
    Set ador.ActiveConnection = Nothing
    
    Set devolverTodosLosProveedores = ador
    
    Exit Function
    
Error:
    If Err.Number = -2147217900 Then
        Set devolverTodosLosProveedores = Nothing
        Exit Function
    End If
End Function


''' <summary>Carga los proveedores en una coleccion</summary>
''' <param name="NumMaximo">Numero maximo de proveedores a Cargar</param>
''' <param name="CiaComp">Compa�ia</param>
''' <param name="vCod">Codigo de proveedor</param>
''' <param name="vDen">Denominacion de proveedor</param>
''' <param name="vPais">Pais</param>
''' <param name="Provincia">Provincia</param>
''' <param name="vGMN1">Filtro de material de nivel 1</param>
''' <param name="vGMN2">Filtro de material de nivel 2</param>
''' <param name="vGMN3">Filtro de material de nivel 3</param>
''' <param name="vGMN4">Filtro de material de nivel 4</param>
''' <returns>Coleccion de proveedores</returns>
''' <remarks>Llamada desde=frmProveBuscar.Proveseleccionado; Tiempo m�ximo=0seg.</remarks>
''' <revision>JVS 13/01/2011</revision>
Public Function cargarTodosProveedores(ByVal NumMaximo As Integer, ByVal CiaComp As Long, Optional ByVal vCod As String, Optional ByVal vDen As String, Optional ByVal vPais As String, Optional ByVal Provincia As String, Optional ByVal vGMN1 As String, Optional ByVal vGMN2 As String, Optional ByVal vGMN3 As String, Optional ByVal vGMN4 As String)


Dim sConsulta As String
Dim sWhere As String
Dim lIndice As Long
Dim iNumGrups As Integer
Dim sfsg As String
Dim BD As String
Dim SRV As String
Dim ador As adodb.Recordset
    
    'Primero debemos obtener los datos de SERVIDOR,BD,... de la CiaCompradora
    
    sConsulta = "SELECT FSGS_SRV,FSGS_BD FROM CIAS WITH (NOLOCK) WHERE ID=" & CiaComp
    Set ador = New adodb.Recordset
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        
    If ador.eof Then
        ador.Close
        Set ador = Nothing
        Exit Function
    End If
    
    SRV = ador("FSGS_SRV").Value
    BD = ador("FSGS_BD").Value
        
    ador.Close
    Set ador = Nothing
    
    sfsg = SRV & "." & BD & ".dbo."
    
    
    sConsulta = "SELECT DISTINCT PROVE.COD, PROVE.DEN, PROVE.NIF, PROVE.DIR, PROVE.CP," _
              & "       PROVE.POB, PROVE.PAI AS PAICOD, " _
              & "       PROVE.PROVI AS PROVICOD, PROVE.MON AS MONCOD, " _
              & "       MON_DEN.DEN AS MONDEN, PROVI_DEN.DEN AS PROVIDEN, " _
              & "       PAI_DEN.DEN AS PAIDEN FROM " & sfsg & "PROVE PROVE WITH (NOLOCK) LEFT OUTER JOIN " _
              & "       " & sfsg & "MON_DEN MON_DEN WITH (NOLOCK) ON PROVE.MON = MON_DEN.MON AND MON_DEN.IDIOMA='" & g_udtParametrosGenerales.g_sIdioma & "' LEFT OUTER JOIN " _
              & "       " & sfsg & "PROVI_DEN PROVI_DEN WITH (NOLOCK)  ON PROVE.PAI = PROVI_DEN.PAI AND " _
              & "       PROVE.PROVI = PROVI_DEN.PROVI AND PROVI_DEN.IDIOMA='" & g_udtParametrosGenerales.g_sIdioma & "' LEFT OUTER JOIN " _
              & "       " & sfsg & "PAI_DEN PAI_DEN WITH (NOLOCK) ON PROVE.PAI = PAI_DEN.PAI AND PAI_DEN.IDIOMA='" & g_udtParametrosGenerales.g_sIdioma & "' "
    
    
    If Not IsMissing(vGMN1) And vGMN1 <> "" Then
        sConsulta = sConsulta & " INNER JOIN " & sfsg & "PROVE_GMN4 PROVE_GMN4 WITH (NOLOCK) ON PROVE_GMN4.PROVE = PROVE.COD AND PROVE_GMN4.GMN1='" & vGMN1 & "'"
        If Not IsMissing(vGMN2) And vGMN2 <> "" Then
            sConsulta = sConsulta & " AND PROVE_GMN4.GMN2='" & vGMN2 & "'"
            If Not IsMissing(vGMN3) And vGMN3 <> "" Then
                sConsulta = sConsulta & " AND PROVE_GMN4.GMN3 ='" & vGMN3 & "'"
                If Not IsMissing(vGMN4) And vGMN4 <> "" Then
                    sConsulta = sConsulta & " AND PROVE_GMN4.GMN4 ='" & vGMN4 & "'"
                End If
            End If
        End If
    End If
    
    sWhere = " where 1 = 1 "
    
    If Not IsMissing(vCod) And vCod <> "" Then
        sWhere = sWhere & " AND PROVE.COD ='" & DblQuote(vCod) & "'"
    End If
    
    If Not IsMissing(vDen) And vDen <> "" Then
        sWhere = sWhere & " AND PROVE.DEN LIKE '" & DblQuote(vDen) & "%'"
    End If
    
    If Not IsMissing(vPais) And vPais <> "" Then
        sWhere = sWhere & " AND PROVE.PAI ='" & DblQuote(vPais) & "'"
    End If
    
    If Not IsMissing(Provincia) And Provincia <> "" Then
        sWhere = sWhere & " AND PROVE.PROVI ='" & DblQuote(Provincia) & "'"
    End If
    sConsulta = sConsulta & sWhere
    
    Set ador = New adodb.Recordset
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    If ador.eof Then
    
        ador.Close
        Set ador = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Function
    End If
    Dim iNumProve As Integer
    
    Set mCol = Nothing
    Set mCol = New Collection
        
    iNumProve = 0
    While Not ador.eof And iNumProve < NumMaximo
        Me.Add CiaComp, Trim(ador("cod").Value), ador("den").Value, BD, SRV, ador("nif").Value, ador("dir").Value, ador("pob").Value, ador("cp").Value, ador("moncod").Value, ador("paicod").Value, ador("provicod").Value, ador("monden").Value, ador("paiden").Value, ador("providen").Value
        
        ador.MoveNext
    Wend
    
    ador.Close
    Set ador = Nothing

End Function

