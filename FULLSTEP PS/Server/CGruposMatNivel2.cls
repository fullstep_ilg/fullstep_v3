VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGruposMatNivel2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CGruposMatNivel2 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean
Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

Public Function Add(ByVal GMN1Cod As String, ByVal GMN1Den As String, ByVal Cod As String, ByVal Den As String, Optional ByVal varIndice As Variant) As CGrupoMatNivel2
    'create a new object
    Dim objnewmember As CGrupoMatNivel2
    Dim sCod As String
    
    Set objnewmember = New CGrupoMatNivel2
   
    objnewmember.GMN1Cod = GMN1Cod
    objnewmember.GMN1Den = GMN1Den
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    Set objnewmember.Conexion = mvarConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
       sCod = GMN1Cod & Mid$("                         ", 1, 20 - Len(GMN1Cod))
       sCod = sCod & Cod & Mid$("                         ", 1, 20 - Len(Cod))
       mCol.Add objnewmember, sCod
    End If
    
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

Public Property Get Item(vntIndexKey As Variant) As CGrupoMatNivel2
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
   

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

'Public Function DevolverLosCodigos() As TipoDatosCombo
   
 '  Dim Codigos As TipoDatosCombo
 '  Dim iCont As Integer
 '  ReDim Codigos.Cod(mCol.Count)
 '  ReDim Codigos.Den(mCol.Count)
   
 '   For iCont = 0 To mCol.Count - 1
        
 '       Codigos.Cod(iCont) = mCol(iCont + 1).Cod
 '       Codigos.Den(iCont) = mCol(iCont + 1).Den
    
 '   Next

 '  DevolverLosCodigos = Codigos
   
'End Function



