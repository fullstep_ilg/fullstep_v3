VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGruposMatNivel4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CGruposMatNivel4 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private mvarConexion As CConexion
Private mvarEOF As Boolean
Public Property Get eof() As Boolean
    eof = mvarEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property

Public Function Add(ByVal GMN1Cod As String, ByVal GMN2Cod As String, ByVal GMN3Cod As String, ByVal GMN1Den As String, ByVal GMN2Den As String, ByVal GMN3Den As String, ByVal Cod As String, ByVal Den As String, Optional ByVal varIndice As Variant) As CGrupoMatNivel4
    'create a new object
    Dim objnewmember As CGrupoMatNivel4
    Dim sCod As String
    Set objnewmember = New CGrupoMatNivel4
   
    objnewmember.GMN1Cod = GMN1Cod
    objnewmember.GMN2Cod = GMN2Cod
    objnewmember.GMN3Cod = GMN3Cod
    objnewmember.GMN1Den = GMN1Den
    objnewmember.GMN2Den = GMN2Den
    objnewmember.GMN3Den = GMN3Den
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    Set objnewmember.Conexion = mvarConexion
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        sCod = GMN1Cod & Mid$("                         ", 1, 20 - Len(GMN1Cod))
        sCod = sCod & GMN2Cod & Mid$("                         ", 1, 20 - Len(GMN2Cod))
        sCod = sCod & GMN3Cod & Mid$("                         ", 1, 20 - Len(GMN3Cod))
        sCod = sCod & Cod & Mid$("                         ", 1, 20 - Len(Cod))
        mCol.Add objnewmember, sCod
    End If
    
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

Public Property Get Item(vntIndexKey As Variant) As CGrupoMatNivel4
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property


Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

