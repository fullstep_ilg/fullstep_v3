VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cComunicacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mvarIndice As Long 'Nos indica la posicion secuencial dentro de una Coleccion
Private mvarConexion As CConexion
Private mvarId As Integer
Private mvarCia As Long
Private mvarFecha As Date
Private mvarTipoCon As Variant
Private mvarDesCon As String
Private mvarUsu As Integer
Private mvarModo As String
Private mvarPort As Integer
Private mvarComent As Variant

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Public Property Get ID() As Integer
     ID = mvarId
End Property
Public Property Let ID(ByVal ident As Integer)
    mvarId = ident
End Property

Public Property Get Cia() As Long
     Cia = mvarCia
End Property
Public Property Let Cia(ByVal Comp As Long)
    mvarCia = Comp
End Property

Public Property Get Fecha() As Date
     Fecha = mvarFecha
End Property
Public Property Let Fecha(ByVal Fec As Date)
    mvarFecha = Fec
End Property

Public Property Get TipoComunicacion() As Variant
     TipoComunicacion = mvarTipoCon
End Property
Public Property Let TipoComunicacion(ByVal comunic As Variant)
     mvarTipoCon = comunic
End Property

Public Property Get DenComunicacion() As String
     DenComunicacion = mvarDesCon
End Property
Public Property Let DenComunicacion(ByVal DescripCom As String)
     mvarDesCon = DescripCom
End Property

Public Property Get Usuario() As Integer
     Usuario = mvarUsu
End Property
Public Property Let Usuario(ByVal Usu As Integer)
     mvarUsu = Usu
End Property

Public Property Get ModoComunica() As String
     ModoComunica = mvarModo
End Property
Public Property Let ModoComunica(ByVal ModoC As String)
     mvarModo = ModoC
End Property

Public Property Get Port() As Integer
     Port = mvarPort
End Property
Public Property Let Port(ByVal Puerto As Integer)
     mvarPort = Puerto
End Property

Public Property Get Comentario() As Variant
     Comentario = mvarComent
End Property
Public Property Let Comentario(ByVal Coment As Variant)
    mvarComent = Coment
End Property

Public Function EliminarComunicacion() As CTESError
Dim ador As ADODB.Recordset
Dim sConsulta As String
Dim oError As CTESError
Dim sfsg As String

    On Error GoTo Error
    
    Set oError = New CTESError
    oError.NumError = 0
    
    mvarConexion.AdoCon.Execute "BEGIN TRANSACTION"
    mvarConexion.AdoCon.Execute "SET XACT_ABORT ON"
    
    mvarConexion.AdoCon.Execute "DELETE FROM CIAS_COM WHERE CIA=" & mvarCia & " AND ID =" & mvarId & " AND PORT=" & basParametros.IdPortal

    mvarConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    mvarConexion.AdoCon.Execute "COMMIT TRANSACTION"
    
    Set EliminarComunicacion = oError
    
    Exit Function

Error:
    mvarConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    Set oError = basErrores.TratarError(mvarConexion.AdoCon.Errors)
    Set EliminarComunicacion = oError

End Function

Public Function ModificarDatosComunicacion(Optional ByVal SoloComentario As Boolean) As CTESError
    Dim ador As ADODB.Recordset
    Dim sConsulta As String
    Dim oError As CTESError
    
    Set oError = New CTESError
    oError.NumError = 0

On Error GoTo Error:
    mvarConexion.AdoCon.Execute "BEGIN TRANSACTION"
    mvarConexion.AdoCon.Execute "SET XACT_ABORT ON"
    
    sConsulta = "UPDATE CIAS_COM SET "
    
    If SoloComentario = True Then
        sConsulta = sConsulta & " COMENT = " & StrToSQLNULL(mvarComent)
    Else
        sConsulta = sConsulta & " FECHA = " & DateToSQLTimeDate(mvarFecha) & ",TIPOCOM = " & mvarTipoCon & ", COMENT = " & StrToSQLNULL(mvarComent)
    End If
    sConsulta = sConsulta & " WHERE ID=" & mvarId & " AND CIA=" & mvarCia
    
    mvarConexion.AdoCon.Execute sConsulta
    
    mvarConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    mvarConexion.AdoCon.Execute "COMMIT TRANSACTION"
       
    Set ModificarDatosComunicacion = oError
    Exit Function
    
    
Error:
    mvarConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    Set oError = basErrores.TratarError(mvarConexion.AdoCon.Errors)
    
    Set ModificarDatosComunicacion = oError
    
    
End Function

''' <summary>
''' A�adir comunicaci�n
''' </summary>
''' <returns>Error si lo hay</returns>
''' <remarks>Llamada desde: frmComunicacionCias.sdbgComunicacion_BeforeUpdate;
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 13/01/2011</revision>
Public Function AnyadirComunicacion() As CTESError
    Dim ador As ADODB.Recordset
    Dim sConsulta As String
    Dim oError As CTESError
    
    Set oError = New CTESError
    oError.NumError = 0
    
    '********* Precondicion *******************
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "cComunicacion.AnyadirComunicacion", "No se ha establecido la conexion"
    End If
    ' *****************************************

On Error GoTo Error:

    mvarConexion.AdoCon.Execute "BEGIN TRANSACTION"
    mvarConexion.AdoCon.Execute "SET XACT_ABORT ON"
    
    'Selecciono el id de la comunicaci�n
    'CALIDAD Sin WITH(NOLOCK) en el FROM para asegurar la recuperaci�n del mayor valor de la tabla
    sConsulta = "SELECT MAX(ID) FROM CIAS_COM WHERE CIA=" & mvarCia
    Set ador = New ADODB.Recordset
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    If IsNull(ador(0).Value) Then
        mvarId = 1
    Else
        mvarId = ador(0).Value + 1
    End If
    
    ador.Close
    Set ador = Nothing

    'Realizo la inserci�n
    sConsulta = "INSERT INTO CIAS_COM (CIA,ID,FECHA,TIPOCOM,PORT,COMENT) VALUES "
    sConsulta = sConsulta & "(" & mvarCia & "," & mvarId & "," & DateToSQLTimeDate(mvarFecha) & "," & mvarTipoCon & "," & basParametros.IdPortal & "," & StrToSQLNULL(mvarComent) & ")"
    mvarConexion.AdoCon.Execute sConsulta
    
    mvarConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    mvarConexion.AdoCon.Execute "COMMIT TRANSACTION"
    
    Set AnyadirComunicacion = oError
    Exit Function
    
Error:
    mvarConexion.AdoCon.Execute "SET XACT_ABORT OFF"
    Set oError = basErrores.TratarError(mvarConexion.AdoCon.Errors)
    Set AnyadirComunicacion = oError
    
    
End Function

Private Sub Class_Terminate()
    Set mvarConexion = Nothing
End Sub
