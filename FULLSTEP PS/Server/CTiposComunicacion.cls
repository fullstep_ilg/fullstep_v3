VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTiposComunicacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mvarConexion As CConexion
Private mCol As Collection

Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Public Function Add(ByVal ID As Integer, ByVal Cod As String, ByVal Den As String) As CTipoComunicacion
    
    ''' * Objetivo: Anyadir un usuario a la coleccion
    ''' * Recibe: Datos del Usuario
    ''' * Devuelve: Usuario anyadido
    
    Dim objnewmember As CTipoComunicacion
    
    ''' Creacion de objeto Usuario
    
    Set objnewmember = New CTipoComunicacion
   
    ''' Paso de los parametros al nuevo objeto Usuario
    objnewmember.ID = ID
    objnewmember.Codigo = Cod
    objnewmember.Denominacion = Den
    Set objnewmember.Conexion = mvarConexion
    
    mCol.Add objnewmember, CStr(ID)
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function

Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar un Usuario de la coleccion
    ''' * Recibe: Indice del Usuario a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub

Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property

Public Property Get Item(vntIndexKey As Variant) As CTipoComunicacion

    ''' * Objetivo: Recuperar un usuario de la coleccion
    ''' * Recibe: Indice del usuario a recuperar
    ''' * Devuelve: usuario correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property



Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

''' <summary>
''' Cargar todos los tipos de comunicacion
''' </summary>
''' <param name="NumMaximo">NumMaximo de resultados a cargar</param>
''' <param name="OrdenarPorDen">Si es ordenado por denominaci�n</param>
''' <param name="CarIniUsuCod">Codigo comunicacion</param>
''' <param name="CarIniDen">Denominacion comunicacion</param>
''' <param name="CoincidenciaTotal">Si se hace like o =</param>
''' <remarks>Llamada desde: frmCOM/cmdRestaurar_Click   frmCOM/sdbgTipoCom_BeforeUpdate     frmCOM/sdbgTipoCom_HeadClick; Tiempo m�ximo: 0</remarks>
''' Revisado por: Jbg. Fecha: 03/07/2012
Public Sub CargarTodosLosTiposDesde(ByVal NumMaximo As Integer, Optional ByVal OrdenarPorDen As Boolean, Optional ByVal CarIniUsuCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean)
    
    Dim ador As adodb.Recordset
    Dim sConsulta As String
    Dim sPrimeraCond As String
    Dim sPrimerOrden As String
    
    Dim lIndice As Long
    Dim iNum As Integer
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        ' Err.Raise vbObjectError "CUsuarios.CargarTodosLosUsuariosDeLaCiaDesde", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    ''' Generacion del SQL a partir de los parametros
    sConsulta = "SELECT ID,COD,DEN,FECACT FROM COM WITH (NOLOCK) "
    If CarIniUsuCod = "" And CarIniDen = "" Then
    Else
        If Not (CarIniUsuCod = "") And Not (CarIniDen = "") Then
            If CoincidenciaTotal Then
                sConsulta = sConsulta & " WHERE COD ='" & DblQuote(CarIniUsuCod) & "'"
                sConsulta = sConsulta & " AND DEN ='" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta = sConsulta & " WHERE COD >='" & DblQuote(CarIniUsuCod) & "'"
                sConsulta = sConsulta & " AND DEN >='" & DblQuote(CarIniDen) & "'"
            End If
        Else
            If Not (CarIniUsuCod = "") Then
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & " WHERE COD ='" & DblQuote(CarIniUsuCod) & "'"
                Else
                    sConsulta = sConsulta & " WHERE COD >='" & DblQuote(CarIniUsuCod) & "'"
                End If
            Else
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & " WHERE DEN ='" & DblQuote(CarIniDen) & "'"
                Else
                    sConsulta = sConsulta & " WHERE DEN >='" & DblQuote(CarIniDen) & "'"
                End If
            End If
        End If
    End If
    If OrdenarPorDen Then
        sConsulta = sConsulta & " ORDER BY DEN"
    Else
        sConsulta = sConsulta & " ORDER BY COD"
    End If
    Set ador = New adodb.Recordset
    Set mCol = Nothing
    Set mCol = New Collection
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    If ador.eof Then
        Set ador = Nothing
        Exit Sub
    End If
    iNum = 0
    
    While Not ador.eof And iNum < NumMaximo
            Me.Add ador("ID").Value, ador("COD").Value, ador("DEN").Value
            iNum = iNum + 1
            ador.MoveNext
    Wend
    ador.Close
    Set ador = Nothing
    
End Sub

''' <summary>
''' Devuelve en un Recordset todos los tipos de comunicacion
''' </summary>
''' <param name="Cod">Codigo comunicacion</param>
''' <param name="Den">Denominacion comunicacion</param>
''' <param name="CoincidenciaTotal">Si se hace like o =</param>
''' <param name="OrdenadoPorDen">Si es ordenado por denominaci�n</param>
''' <returns>Recordset</returns>
''' <remarks>Llamada desde: frmComunicacionesPorFechas/Validar      frmComunicacionCias/Validar ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: Jbg. Fecha: 03/07/2012
Public Function DevolverTiposDesde(Optional ByVal Cod As String, Optional ByVal Den As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadoPorDen As Boolean) As adodb.Recordset
Dim sConsulta As String
Dim ador As adodb.Recordset
    
    sConsulta = "SELECT ID,COD,DEN,FECACT FROM COM WITH (NOLOCK) WHERE 1=1 "
    
    If Cod <> "" Then
        If CoincidenciaTotal Then
            sConsulta = sConsulta & " AND COD = '" & DblQuote(Cod) & "'"
        Else
            sConsulta = sConsulta & " AND COD >= '" & DblQuote(Cod) & "'"
        End If
    End If
    
    If Den <> "" Then
        If CoincidenciaTotal Then
            sConsulta = sConsulta & " AND DEN = '" & DblQuote(Den) & "'"
        Else
            sConsulta = sConsulta & " AND DEN >= '" & DblQuote(Den) & "'"
        End If
    End If
    
    
    If OrdenadoPorDen Then
            sConsulta = sConsulta & " ORDER BY DEN"
    Else
            sConsulta = sConsulta & " ORDER BY COD"
    End If
    
    Set ador = New adodb.Recordset
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    Set ador.ActiveConnection = Nothing
    
    Set DevolverTiposDesde = ador
    
    
End Function

