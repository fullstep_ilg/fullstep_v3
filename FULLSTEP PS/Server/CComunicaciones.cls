VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cComunicaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mCol As Collection
Private mvarConexion As CConexion

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum


Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Public Function Add(ByVal Cia As Long, ByVal ID As Integer, ByVal Fecha As Date, ByVal TipoCom As Variant, ByVal DesCom As String, ByVal Coment As Variant) As cComunicacion
    
    ''' * Objetivo: Anyadir una comunicacion a la coleccion
    ''' * Recibe: Datos de la comunicacion
    ''' * Devuelve: Comunicacion anyadida
    
    Dim objnewmember As cComunicacion
    
    ''' Creacion de objeto Comunicacion
    
    Set objnewmember = New cComunicacion
   
    ''' Paso de los parametros al nuevo objeto comunicacion
    objnewmember.Cia = Cia
    objnewmember.ID = ID
    objnewmember.Fecha = Fecha
    objnewmember.TipoComunicacion = TipoCom
    objnewmember.DenComunicacion = DesCom
    'objnewmember.Usuario = Usu
    'objnewmember.ModoComunica = Modo
    'objnewmember.Port = Port
    objnewmember.Comentario = Coment
    
    Set objnewmember.Conexion = mvarConexion
    
    mCol.Add objnewmember, CStr(Cia) & "-" & CStr(ID)
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function


Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una comunicacion de la coleccion
    ''' * Recibe: Indice de la comunicacion a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property


Public Property Get Item(vntIndexKey As Variant) As cComunicacion

    ''' * Objetivo: Recuperar una comunicacion de la coleccion
    ''' * Recibe: Indice de la comunicacion a recuperar
    ''' * Devuelve: comunicacion correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

Public Function DevolverComboTiposComunicacion() As ADODB.Recordset
    Dim ador As ADODB.Recordset
    Dim sConsulta As String
    
    'Devuelve el c�digo y la descripci�n de los tipos de comunicaci�n
    'existentes
    
    Set ador = New ADODB.Recordset
    
    sConsulta = "SELECT ID,DEN FROM COM WITH (NOLOCK) "
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    
    Set ador.ActiveConnection = Nothing
    
    Set DevolverComboTiposComunicacion = ador

End Function

Function CargarComunicacionesDesde(Optional ByVal FecDesde As String, Optional ByVal FecHasta As String, Optional ByVal TipoOrdenacion As Integer) As ADODB.Recordset
    
    Dim ador As ADODB.Recordset
    Dim sConsulta As String
    Dim sPrimeraCond As String
    Dim sPrimerOrden As String
    
    Dim lIndice As Long
    Dim iNumC As Integer

    ''' Precondicion
    If mvarConexion Is Nothing Then
        ' Err.Raise vbObjectError "CCia.CargarTodasLasComunicaciones", "No se ha establecido la conexion"
        Exit Function
    End If
    
    ''' Generacion del SQL a partir de los parametros
    sConsulta = "SELECT CIAS_COM.*,CIAS.DEN AS CIADEN,COM.DEN AS TICOMDEN FROM CIAS_COM WITH (NOLOCK) LEFT JOIN COM WITH (NOLOCK) ON "
    sConsulta = sConsulta & "CIAS_COM.TIPOCOM = COM.ID INNER JOIN CIAS WITH (NOLOCK) ON CIAS.ID = CIAS_COM.CIA"
        
    If Not IsMissing(FecDesde) And Not IsNull(FecDesde) And Trim$(FecDesde) <> "" Then
        sConsulta = sConsulta & " WHERE FECHA >= " & DateToSQLDate(FecDesde)
        If Not IsMissing(FecHasta) And Not IsNull(FecHasta) And Trim$(FecHasta) <> "" Then
            sConsulta = sConsulta & " AND FECHA <= " & DateToSQLDate(FecHasta)
        End If
    Else
        If Not IsMissing(FecHasta) And Not IsNull(FecHasta) And Trim$(FecHasta) <> "" Then
            sConsulta = sConsulta & " WHERE FECHA <= " & DateToSQLDate(FecHasta)
        End If
    End If
    
    Select Case TipoOrdenacion
    Case 1
        sConsulta = sConsulta & " ORDER BY CIAS_COM.FECHA,CIADEN"
    Case 2
        sConsulta = sConsulta & " ORDER BY CIADEN,CIAS_COM.FECHA"
    Case 3
        sConsulta = sConsulta & " ORDER BY TICOMDEN,CIAS_COM.FECHA,CIADEN"
    Case Else
        sConsulta = sConsulta & " ORDER BY CIAS_COM.FECHA,CIADEN"
    End Select
    
    Set ador = New ADODB.Recordset
    
    Set mCol = Nothing
    Set mCol = New Collection
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    If ador.eof Then
        Set ador = Nothing
        Exit Function
    End If
    iNumC = 0
    
    While Not ador.eof
        Me.Add ador("CIA").Value, ador("ID").Value, ador("FECHA").Value, ador("TIPOCOM").Value, ador("TICOMDEN").Value, ador("COMENT")
        iNumC = iNumC + 1
        ador.MoveNext
    Wend
    ador.MoveFirst
    
    Set ador.ActiveConnection = Nothing
    Set CargarComunicacionesDesde = ador
End Function


