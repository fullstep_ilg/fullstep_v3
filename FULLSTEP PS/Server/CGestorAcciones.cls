VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGestorAcciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


Private mvarConexion As CConexion

Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexi�n
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set mvarConexion = vData
    
End Property

''' <summary>Genera un nuevo registro en la tabla LOG con los datos indicados</summary>
''' <param name="sCodUsu">Cod. usuario</param>
''' <param name="Acc">Id accion</param>
''' <param name="sDat_SPA">Datos</param>
''' <param name="sDat_ENG">Datos</param>
''' <param name="sDat_FRA">Datos</param>
''' <param name="sDat_GER">Datos</param>
''' <param name="Cia">cOMPA�IA</param>
''' <param name="RegMail">iD DE REGISTRO MAIL</param>
Public Sub RegistrarAccion(ByVal sCodUsu As String, ByVal Acc As TipoAccion, ByVal sDat_SPA As String, ByVal sDat_ENG As String, ByVal sDat_FRA As String, ByVal sDat_GER As String, Optional ByVal Cia As Long = 0, Optional ByVal RegMail As Long = 0)
    Dim oCom As adodb.Command
    Dim oParam As adodb.Parameter
    
    Set oCom = New adodb.Command
    With oCom
        Set .ActiveConnection = mvarConexion.AdoCon
        .CommandText = "FSP_REGISTRAR_ACCION"
        .CommandType = adCmdStoredProc
        Set oParam = .CreateParameter("@USU", adVarChar, adParamInput, Len(sCodUsu), sCodUsu)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@ACC", adInteger, adParamInput, , Acc)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@DAT_SPA", adVarChar, adParamInput, IIf(Len(sDat_SPA) = 0, 1, Len(sDat_SPA)), sDat_SPA)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@DAT_ENG", adVarChar, adParamInput, IIf(Len(sDat_ENG) = 0, 1, Len(sDat_ENG)), sDat_ENG)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@DAT_FRA", adVarChar, adParamInput, IIf(Len(sDat_FRA) = 0, 1, Len(sDat_FRA)), sDat_FRA)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@DAT_GER", adVarChar, adParamInput, IIf(Len(sDat_GER) = 0, 1, Len(sDat_GER)), sDat_GER)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@FEC", adDBDate, adParamInput, , Now)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@CIA", adInteger, adParamInput, , Cia)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("@REGMAIL", adInteger, adParamInput, , RegMail)
        .Parameters.Append oParam
        .Execute
    End With

    Set oCom = Nothing
    Set oParam = Nothing
End Sub

Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = mvarConexion
    
End Property



''' <summary>Obtiene los registros de comunicaciones con los filtros y ordenaci�n pasados</summary>
''' <param name="iPaginaMostrar">P�gina a mostrar</param>
''' <param name="IdCia">Compa��a</param>
''' <param name="dtFechaDesde">Fecha desde</param>
''' <param name="dtFechaHasta">Fecha hasta</param>
''' <param name="TipoOrdenacion">orden de lectura de los datos</param>
''' <returns>Un recordset con los datos.</returns>
''' <remarks>Llamada desde: frmRegComunicaciones Tiempo m�ximo: <1 seg</remarks>

Public Function CargarAccionesDesdeConPaginacion(ByVal iNumPagMostrar As Integer, ByVal bPaginacionSig As Boolean, _
        ByRef iNumPag As Long, Optional ByVal IdCia As Long, Optional ByVal vFechaDesde As Variant, Optional ByVal vFechaHasta As Variant, _
        Optional ByVal TipoOrdenacion As TipoOrdenAccion = TipoOrdenAccion.None, Optional ByVal sUsu As String) As adodb.Recordset
        
    Dim sWhere As String
    Dim sWherePaginacion As String
    Dim sOrderBy As String
    Dim sConsultaCuenta As String
    Dim sConsulta As String
    Dim rsCuenta As adodb.Recordset
    Dim rsReg As adodb.Recordset
    Dim lNumReg As Long
    Dim bHayPaginacion As Boolean
    Dim sOrden As String
    Dim sOrdenFecha As String
    Dim dtFecha As Date
    Dim sSort As String
    Dim iNumRegDevolver As Long
    
    Dim sWhereRegPag As String
    Dim sOrderRegPag As String
        
    ''' Precondicion
    If mvarConexion Is Nothing Then
        Exit Function
    End If
    
    bHayPaginacion = False
    
    'SELECT CUENTA
    sConsultaCuenta = "SELECT COUNT(*) AS NUMREG"
    sConsultaCuenta = sConsultaCuenta & " FROM LOG L WITH (NOLOCK) "
    
    'WHERE
    If IdCia <> 0 Then
        sWhere = "L.CIA = " & IdCia & " AND"
    End If
    
    
    If Not IsMissing(vFechaDesde) And IsDate(vFechaDesde) Then
    'restamos 1 dia para tener en cuenta las horas
        sWhere = sWhere & " L.FEC > " & DateToSQLTimeDate(DateAdd("d", -1, CDate(vFechaDesde))) & " AND"
    End If
    If Not IsMissing(vFechaHasta) And IsDate(vFechaHasta) Then
    'sumamos 1 dia para tener en cuenta las horas
        sWhere = sWhere & " L.FEC < " & DateToSQLTimeDate(DateAdd("d", 1, CDate(vFechaHasta))) & " AND"
    End If
    If sUsu <> "" Then
        sWhere = sWhere & " L.USU ='" & sUsu & "' AND"
    End If
    
    If sWhere <> vbNullString Then
        sWhere = " WHERE " & Left(sWhere, Len(sWhere) - 3)
    End If
    
    sConsultaCuenta = sConsultaCuenta & sWhere
   
    
    'Obtener el n� de registros de la consulta
    iNumPag = 0
    Set rsCuenta = New adodb.Recordset
    rsCuenta.Open sConsultaCuenta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    If rsCuenta.RecordCount > 0 Then
        lNumReg = rsCuenta("NUMREG")
        iNumPag = lNumReg \ g_udtParametrosGenerales.g_lCargaMaxima
        If lNumReg Mod g_udtParametrosGenerales.g_lCargaMaxima > 0 Then iNumPag = iNumPag + 1
        iNumRegDevolver = g_udtParametrosGenerales.g_lCargaMaxima
        
        sWhereRegPag = " WHERE a.ROWNUM BETWEEN " & iNumRegDevolver * (iNumPagMostrar - 1) & " AND " & iNumRegDevolver * iNumPagMostrar
              
        If iNumPag > 1 Then
            If iNumPagMostrar > 1 And iNumPagMostrar < iNumPag Then
                bHayPaginacion = True
            End If
        End If
        
        Select Case TipoOrdenacion
            Case TipoOrdenAccion.col_fecha
                sSort = " L.FEC DESC "
            Case TipoOrdenAccion.Col_Usu
                sSort = " L.USU ASC, L.FEC DESC "
            Case TipoOrdenAccion.Col_Accion
                sSort = " LAD.Den ASC, L.FEC DESC "
            Case TipoOrdenAccion.Col_CiaCod
                sSort = " C.COD ASC, L.FEC DESC "
            Case TipoOrdenAccion.Col_ciaden
                sSort = " C.DEN ASC, L.FEC DESC "
            Case TipoOrdenAccion.Col_Dato
                sSort = " L.DAT ASC, L.FEC DESC "
            Case Else
                    sSort = " L.FEC DESC "
        End Select
               
            sConsulta = "SELECT ID,USU,FECHA,ACCION_DEN,CIA_COD,CIA_DEN,ACCION_DAT, REGMAIL, CIA_ID  FROM ("
            If bPaginacionSig Then
                sConsulta = sConsulta & " SELECT ROW_NUMBER() OVER(ORDER BY " & sSort & "   ) AS ROWNUM,  L.ID, L.USU, "
            Else
                sConsulta = sConsulta & " SELECT  L.ID, L.USU, "
            End If
               
            sConsulta = sConsulta & " CONVERT(varchar(23), L.FEC,21) FECHA, LAD.DEN  ACCION_DEN,"
            sConsulta = sConsulta & " C.COD  CIA_COD, C.DEN  CIA_DEN, L.DAT_" & g_udtParametrosGenerales.g_sIdioma & " ACCION_DAT, "
            sConsulta = sConsulta & " REGMAIL, C.ID  CIA_ID  "
            sConsulta = sConsulta & " FROM LOG L WITH (NOLOCK) "
            sConsulta = sConsulta & " LEFT JOIN CIAS C WITH (NOLOCK) ON C.ID = L.CIA "
            sConsulta = sConsulta & " INNER JOIN LOG_ACC_DEN lad on L.ACC = LAD.ACC AND LAD.IDIOMA='" & g_udtParametrosGenerales.g_sIdioma & "'"
                    
            If sWhere <> vbNullString Then sConsulta = sConsulta & sWhere
            If bPaginacionSig Then
                sConsulta = sConsulta & ") a " & sWhereRegPag
            Else
                sConsulta = sConsulta & ") a "
            End If
                            
                'Obtener los registros
        Set rsReg = New adodb.Recordset
        rsReg.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        
        Set rsReg.ActiveConnection = Nothing
    End If
    
    Set CargarAccionesDesdeConPaginacion = rsReg
    
    Set rsCuenta = Nothing
    Set rsReg = Nothing
End Function

