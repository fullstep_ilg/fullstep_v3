VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGrupoMatNivel1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CGrupoMatNivel1 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************


Option Explicit


Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarIndice As Long 'Nos indica la posicion secuencial dentro de una Coleccion
Private mvarCod As String
Private mvarDen As String
Private mvarGruposMatNivel2 As CGruposMatNivel2
Private mvarConexion As CConexion 'local copy


Public Property Get Indice() As Long
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal Ind As Long)
    mvarIndice = Ind
End Property


Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Public Property Set GruposMatNivel2(ByVal vData As CGruposMatNivel2)
    Set mvarGruposMatNivel2 = vData
End Property


Public Property Get GruposMatNivel2() As CGruposMatNivel2
    Set GruposMatNivel2 = mvarGruposMatNivel2
End Property

Public Property Let Den(ByVal vData As String)
    mvarDen = vData
End Property


Public Property Get Den() As String
    Den = mvarDen
End Property



Public Property Let Cod(ByVal vData As String)
    mvarCod = vData
End Property


Public Property Get Cod() As String
    Cod = mvarCod
End Property


Private Sub Class_Terminate()
    
    Set mvarConexion = Nothing
   
End Sub

''' <summary>
''' Carga de todos los grupos de materiales de nivel 2 de la compa��a
''' </summary>
''' <param name="CiaComp">Compa��a</param>
''' <param optional name="CaracteresInicialesCod">Cod de busqueda</param>
''' <param optional name="CaracteresInicialesDen">Den de busqueda</param>
''' <param optional name="CoincidenciaTotal">Si la busqueda ha de coincidir con la den o el cod</param>
''' <param optional name="OrdenadosPorDen">Ordenacion por denominacion</param>
''' <param optional name="UsarIndice">Si se usa indice para la coleccion</param>
''' <remarks>Llamada desde: frmPROVEBuscar.sdbcGMN2_4Cod_DropDown;
''' Tiempo m�ximo: <1 seg </remarks>
''' <revision>JVS 13/01/2011</revision>
Public Sub CargarTodosLosGruposMat2DeLaCia(ByVal CiaComp As Long, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean)

'Dim ador As ADODB.Recordset
Dim sConsulta As String
Dim lIndice As Long
Dim iNumGrups As Integer
Dim sfsg As String
Dim BD As String
Dim SRV As String
Dim ador As adodb.Recordset
    
    'Primero debemos obtener los datos de SERVIDOR,BD,... de la CiaCompradora
    
    sConsulta = "SELECT FSGS_SRV,FSGS_BD FROM CIAS WITH (NOLOCK) WHERE ID=" & CiaComp
    Set ador = New adodb.Recordset
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        
    If ador.eof Then
        ador.Close
        Set ador = Nothing
        Set mvarGruposMatNivel2 = Nothing
        Set mvarGruposMatNivel2 = New CGruposMatNivel2
        Exit Sub
    End If
    
    SRV = ador("FSGS_SRV").Value
    BD = ador("FSGS_BD").Value
        
    ador.Close
    Set ador = Nothing
    
    sfsg = SRV & "." & BD & ".dbo."
    
    sConsulta = "SELECT GMN1,COD,FECACT,DEN_ENG,DEN_GER,DEN_SPA,QA_NOTIF_USU FROM " & sfsg & "GMN2 AS GMN2 WITH (NOLOCK) WHERE GMN2.GMN1='" & DblQuote(mvarCod) & "'"

If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
   
Else
    If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
        
        If Not CoincidenciaTotal Then
           sConsulta = sConsulta & " AND COD >='" & DblQuote(CaracteresInicialesCod) & "'"
           sConsulta = sConsulta & " AND " & DevolverDenGMN & " >='" & DblQuote(CaracteresInicialesDen) & "'"
        Else
           sConsulta = sConsulta & " AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
           sConsulta = sConsulta & " AND " & DevolverDenGMN & " ='" & DblQuote(CaracteresInicialesDen) & "'"
        End If
    Else
        
        If Not (CaracteresInicialesCod = "") Then
            
           If Not CoincidenciaTotal Then
                sConsulta = sConsulta & "AND COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            Else
                sConsulta = sConsulta & "AND COD ='" & DblQuote(CaracteresInicialesCod) & "'"
            End If
            
        Else
            If Not CoincidenciaTotal Then
                sConsulta = sConsulta & "AND " & DevolverDenGMN & " >='" & DblQuote(CaracteresInicialesDen) & "'"
            Else
                sConsulta = sConsulta & "AND " & DevolverDenGMN & " ='" & DblQuote(CaracteresInicialesDen) & "'"
            End If
        End If
    
    End If
           
End If

If OrdenadosPorDen Then
    sConsulta = sConsulta & " ORDER BY " & DevolverDenGMN & ",COD"
Else
    sConsulta = sConsulta & " ORDER BY COD," & DevolverDenGMN
End If

Set ador = New adodb.Recordset
ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
      

If ador.eof Then
        
    ador.Close
    Set ador = Nothing
    Set mvarGruposMatNivel2 = Nothing
    Set mvarGruposMatNivel2 = New CGruposMatNivel2
    
    Set mvarGruposMatNivel2.Conexion = mvarConexion
    
    Exit Sub
      
Else
    
    Set mvarGruposMatNivel2 = Nothing
    Set mvarGruposMatNivel2 = New CGruposMatNivel2
    Set mvarGruposMatNivel2.Conexion = mvarConexion
    
    If UsarIndice Then
        
        lIndice = 0
        iNumGrups = 0
        
        While Not ador.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            mvarGruposMatNivel2.Add ador("GMN1").Value, "", ador("COD").Value, ador(DevolverDenGMN).Value, lIndice
            ador.MoveNext
            lIndice = lIndice + 1
            iNumGrups = iNumGrups + 1
        Wend
        
    Else
        
        While Not ador.eof
            ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
            mvarGruposMatNivel2.Add ador("GMN1").Value, "", ador("COD").Value, ador(DevolverDenGMN).Value
            ador.MoveNext
            iNumGrups = iNumGrups + 1
        Wend
    End If
    
    ador.Close
    Set ador = Nothing
    Exit Sub
      
End If

End Sub


