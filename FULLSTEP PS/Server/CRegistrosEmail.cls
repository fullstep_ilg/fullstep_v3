VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRegistrosEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private mCol As Collection
Private mvarConexion As CConexion

'Variables para paginaci�n
Private m_lPrimerCIA As Long
Private m_lUltimoCIA As Long
Private m_lPrimerID As Long
Private m_lUltimoID As Long
Private m_iPrimerAdjuntos As Long
Private m_iUltimoAdjuntos As Long
Private m_sPrimerFecha As String
Private m_sUltimoFecha As String
Private m_sPrimerCiaCod As String
Private m_sUltimoCiaCod As String
Private m_sPrimerCiaDen As String
Private m_sUltimoCiaDen As String
Private m_vPrimerPara As Variant
Private m_vUltimoPara As Variant
Private m_vPrimerSubject As Variant
Private m_vUltimoSubject As Variant
Private m_vPrimerCC As Variant
Private m_vUltimoCC As Variant
Private m_vPrimerCCO As Variant
Private m_vUltimoCCO As Variant

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum


Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Public Function Add(ByVal Cia As Long, ByVal ID As Long, ByVal Fecha As Date, ByVal Tipo As Integer, _
                    Optional ByVal Subject As Variant, Optional ByVal Para As Variant, Optional ByVal CC As Variant, _
                    Optional ByVal CCO As Variant, Optional ByVal RespuestaA As Variant, Optional ByVal Cuerpo As Variant, _
                    Optional ByVal HayAdjuntos As Boolean, Optional ByVal AcuseRecibo As Integer, _
                    Optional ByVal Producto As Integer, Optional ByVal Usuario As Long, Optional ByVal KO As Boolean, _
                    Optional ByVal TextoError As String) As CRegistroEmail
    
    ''' * Objetivo: Anyadir una comunicacion a la coleccion
    ''' * Recibe: Datos de la comunicacion
    ''' * Devuelve: Comunicacion anyadida
    
    Dim objnewmember As CRegistroEmail
    
    ''' Creacion de objeto Comunicacion
    
    Set objnewmember = New CRegistroEmail
   
    ''' Paso de los parametros al nuevo objeto comunicacion
    objnewmember.Cia = Cia
    objnewmember.ID = ID
    objnewmember.Fecha = Fecha
    objnewmember.Tipo = Tipo
    objnewmember.Para = Para
    objnewmember.HayAdjuntos = HayAdjuntos
    
    If Not IsMissing(Subject) Then
        objnewmember.Subject = Subject
    Else
        objnewmember.Subject = Null
    End If
    
    If Not IsMissing(CC) Then
        objnewmember.CC = CC
    Else
        objnewmember.CC = Null
    End If
    
    If Not IsMissing(CCO) Then
        objnewmember.CCO = CCO
    Else
        objnewmember.CCO = Null
    End If
    
    If Not IsMissing(RespuestaA) Then
        objnewmember.ResponderA = RespuestaA
    Else
        objnewmember.ResponderA = Null
    End If
    
    If Not IsMissing(Cuerpo) Then
        objnewmember.Cuerpo = Cuerpo
    Else
        objnewmember.Cuerpo = Null
    End If
    
    objnewmember.AcuseRecibo = AcuseRecibo
    objnewmember.Producto = Producto
    objnewmember.Usuario = Usuario
    objnewmember.KO = KO
    objnewmember.TextoError = "" & TextoError
    
    Set objnewmember.Conexion = mvarConexion
    
    mCol.Add objnewmember, CStr(Cia) & "-" & CStr(ID)
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function


Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una comunicacion de la coleccion
    ''' * Recibe: Indice de la comunicacion a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property


Public Property Get Item(vntIndexKey As Variant) As CRegistroEmail

    ''' * Objetivo: Recuperar una comunicacion de la coleccion
    ''' * Recibe: Indice de la comunicacion a recuperar
    ''' * Devuelve: comunicacion correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property


Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set mCol = Nothing
    Set mvarConexion = Nothing
End Sub

''' <summary>Obtiene los registros de comunicaciones con los filtros y ordenaci�n pasados</summary>
''' <param name="IdCia">Compa��a</param>
''' <param name="dtFechaDesde">Fecha desde</param>
''' <param name="dtFechaHasta">Fecha hasta</param>
''' <param name="TipoOrdenacion">orden de lectura de los datos</param>
''' <returns>Un recordset con los datos.</returns>
''' <remarks>Llamada desde: frmRegComunicaciones Tiempo m�ximo: <1 seg</remarks>
''' <remarks>Revisado por: LTG  Fecha: 02/06/2011<remarks>

Function CargarComunicacionesDesde(Optional ByVal IdCia As Long, Optional ByVal vFechaDesde As Variant, Optional ByVal vFechaHasta As Variant, _
                                   Optional ByVal TipoOrdenacion As Integer) As ADODB.Recordset
    Dim ador As ADODB.Recordset
    Dim sConsulta As String
    Dim lIndice As Long
    Dim strWhere As String
    
    ''' Precondicion
    If mvarConexion Is Nothing Then
        Exit Function
    End If
    
    ''' Generacion del SQL a partir de los parametros
    sConsulta = "SELECT RE.CIA,RE.ID,RE.FECHA,RE.PARA,RE.SUBJECT,RE.CC,RE.CCO,RE.TIPO,RE.ACUSE_RECIBO,RE.DIR_RESPUESTA,"
    sConsulta = sConsulta & "CIAS.DEN AS CIADEN,CIAS.COD AS CIACOD,"
     'Adjuntos
    sConsulta = sConsulta & " CASE WHEN EXISTS (SELECT TOP 1 CIA"
    sConsulta = sConsulta & "            FROM REGISTRO_EMAIL_ADJUN REA WITH (NOLOCK) "
    sConsulta = sConsulta & "            WHERE REA.CIA=RE.CIA AND REA.ID_REG=RE.ID) THEN 1 ELSE 0 END AS ADJUNTOS"
    
    sConsulta = sConsulta & " FROM REGISTRO_EMAIL RE WITH (NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN CIAS WITH (NOLOCK) ON CIAS.ID = RE.CIA"
    
    If IdCia <> 0 Then
        'PRODUCTO=1 AND
        strWhere = "RE.CIA = " & IdCia & " AND"
    End If
    If Not IsMissing(vFechaDesde) And IsDate(vFechaDesde) Then
        strWhere = strWhere & " RE.FECHA >= " & DateToSQLTimeDate(CDate(vFechaDesde)) & " AND"
    End If
    If Not IsMissing(vFechaHasta) And IsDate(vFechaHasta) Then
        strWhere = strWhere & " RE.FECHA <= " & DateToSQLTimeDate(CDate(vFechaHasta)) & " AND"
    End If
    
    If strWhere <> vbNullString Then
        sConsulta = sConsulta & " WHERE " & Left(strWhere, Len(strWhere) - 3)
    End If
    
    Select Case TipoOrdenacion
        Case 1
            sConsulta = sConsulta & " ORDER BY ADJUN.ADJUNTOS,RE.FECHA DESC"
        Case 2
            sConsulta = sConsulta & " ORDER BY RE.FECHA DESC"
        Case 3
            sConsulta = sConsulta & " ORDER BY CIACOD,RE.FECHA DESC"
        Case 4
            sConsulta = sConsulta & " ORDER BY CIADEN,RE.FECHA DESC"
        Case 5
            sConsulta = sConsulta & " ORDER BY RE.PARA,RE.FECHA DESC"
        Case 6
            sConsulta = sConsulta & " ORDER BY RE.SUBJECT,RE.FECHA DESC"
        Case 7
            sConsulta = sConsulta & " ORDER BY RE.CC,RE.FECHA DESC"
        Case 8
            sConsulta = sConsulta & " ORDER BY RE.CCO,RE.FECHA DESC"
        Case Else
            sConsulta = sConsulta & " ORDER BY RE.FECHA DESC"
    End Select
    
    Set ador = New ADODB.Recordset
    
    Set mCol = Nothing
    Set mCol = New Collection
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    If ador.eof Then
        Set ador = Nothing
        Exit Function
    End If
    
    While Not ador.eof
        Me.Add ador("CIA").Value, ador("ID").Value, ador("FECHA").Value, ador("TIPO").Value, ador("SUBJECT").Value, ador("PARA").Value, ador("CC").Value, ador("CCO").Value, ador("DIR_RESPUESTA").Value, , IIf(ador("ADJUNTOS").Value > 0, True, False)
        ador.MoveNext
    Wend
    ador.MoveFirst
    
    Set ador.ActiveConnection = Nothing
    Set CargarComunicacionesDesde = ador
End Function

''' <summary>Obtiene los registros de comunicaciones con los filtros y ordenaci�n pasados</summary>
''' <param name="iPaginaMostrar">P�gina a mostrar</param>
''' <param name="IdCia">Compa��a</param>
''' <param name="dtFechaDesde">Fecha desde</param>
''' <param name="dtFechaHasta">Fecha hasta</param>
''' <param name="TipoOrdenacion">orden de lectura de los datos</param>
''' <returns>Un recordset con los datos.</returns>
''' <remarks>Llamada desde: frmRegComunicaciones Tiempo m�ximo: <1 seg</remarks>

Public Function CargarComunicacionesDesdeConPaginacion(ByVal iNumPagMostrar As Integer, ByVal bPaginacionSig As Boolean, _
        ByRef iNumPag As Long, Optional ByVal IdCia As Long, Optional ByVal vFechaDesde As Variant, Optional ByVal vFechaHasta As Variant, _
        Optional ByVal TipoOrdenacion As Integer) As ADODB.Recordset
    Dim sWhere As String
    Dim sWherePaginacion As String
    Dim sOrderBy As String
    Dim sConsultaCuenta As String
    Dim sConsulta As String
    Dim rsCuenta As ADODB.Recordset
    Dim rsReg As ADODB.Recordset
    Dim lNumReg As Long
    Dim bHayPaginacion As Boolean
    Dim sOrden As String
    Dim sOrdenFecha As String
    Dim dtFecha As Date
    Dim sSort As String
    Dim iNumRegDevolver As Long
    
    ''' Precondicion
    If mvarConexion Is Nothing Then
        Exit Function
    End If
    
    bHayPaginacion = False
    
    'SELECT CUENTA
    sConsultaCuenta = "SELECT COUNT(*) AS NUMREG"
    sConsultaCuenta = sConsultaCuenta & " FROM REGISTRO_EMAIL RE WITH (NOLOCK) "
    
    'WHERE
    If IdCia <> 0 Then
        sWhere = "RE.CIA = " & IdCia & " AND"
    End If
    If Not IsMissing(vFechaDesde) And IsDate(vFechaDesde) Then
        sWhere = sWhere & " RE.FECHA >= " & DateToSQLTimeDate(CDate(vFechaDesde)) & " AND"
    End If
    If Not IsMissing(vFechaHasta) And IsDate(vFechaHasta) Then
        sWhere = sWhere & " RE.FECHA <= " & DateToSQLTimeDate(CDate(vFechaHasta)) & " AND"
    End If
    If sWhere <> vbNullString Then
        sWhere = " WHERE " & Left(sWhere, Len(sWhere) - 3)
    End If
    
    sConsultaCuenta = sConsultaCuenta & sWhere
    
    'Obtener el n� de registros de la consulta
    iNumPag = 0
    Set rsCuenta = New ADODB.Recordset
    rsCuenta.Open sConsultaCuenta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    If rsCuenta.RecordCount > 0 Then
        lNumReg = rsCuenta("NUMREG")
        iNumPag = lNumReg \ g_udtParametrosGenerales.g_lCargaMaxima
        If lNumReg Mod g_udtParametrosGenerales.g_lCargaMaxima > 0 Then iNumPag = iNumPag + 1
        
        iNumRegDevolver = g_udtParametrosGenerales.g_lCargaMaxima
        
        If iNumPag > 1 Then
            If iNumPagMostrar > 1 And iNumPagMostrar < iNumPag Then
                bHayPaginacion = True
            End If
            
            If iNumPagMostrar = iNumPag And lNumReg Mod g_udtParametrosGenerales.g_lCargaMaxima > 0 Then
                iNumRegDevolver = lNumReg Mod g_udtParametrosGenerales.g_lCargaMaxima
            End If
        End If
        
        Set mCol = Nothing
        Set mCol = New Collection
        
        'SELECT
        'Se obtiene la fecha convertida a varchar para mantener los milisegundos. Aunque SQL guarda la fecha con milisegundos,
        'ADO, al devolver esta fecha en un campo de recordset la redondea a segundos, con lo que los filtros posteriores
        'por el campo FECHA (para la paginaci�n) pueden devolver registros repetidos
        sConsulta = "SELECT TOP " & iNumRegDevolver & " RE.CIA,RE.ID,CONVERT(varchar(23),RE.FECHA,21) AS FECHA,ISNULL(RE.PARA,'') AS PARA,"
        sConsulta = sConsulta & "ISNULL(RE.Subject,'') AS SUBJECT,ISNULL(RE.CC,'') AS CC,ISNULL(RE.CCO,'') AS CCO,RE.TIPO,"
        sConsulta = sConsulta & "CIAS.DEN AS CIADEN,CIAS.COD AS CIACOD,ISNULL(ADJUN.ADJUNTOS,0) AS ADJUNTOS"
        sConsulta = sConsulta & " FROM REGISTRO_EMAIL RE WITH (NOLOCK) "
        sConsulta = sConsulta & " INNER JOIN CIAS WITH (NOLOCK) ON CIAS.ID = RE.CIA "
        sConsulta = sConsulta & " LEFT JOIN (SELECT CIA,ID_REG,CASE WHEN COUNT(*)>0 THEN 1 ELSE 0 END AS ADJUNTOS"
        sConsulta = sConsulta & "            FROM REGISTRO_EMAIL_ADJUN REA WITH (NOLOCK) "
        sConsulta = sConsulta & "            GROUP BY REA.CIA,REA.ID_REG) ADJUN ON ADJUN.CIA = RE.CIA AND ADJUN.ID_REG=RE.ID"
    
        'ORDER BY
        If bHayPaginacion Then
            If bPaginacionSig Then
                sOrden = "ASC"
                sOrdenFecha = "DESC"
            Else
                sOrden = "DESC"
                sOrdenFecha = "ASC"
            End If
        Else
            If iNumPagMostrar = iNumPag Then
                sOrden = "DESC"
                sOrdenFecha = "ASC"
            Else
                sOrden = "ASC"
                sOrdenFecha = "DESC"
            End If
        End If
        Select Case TipoOrdenacion
            Case 1
                sSort = "ADJUNTOS,FECHA DESC"
                sOrderBy = " ORDER BY ADJUN.ADJUNTOS " & sOrden & ",RE.FECHA " & sOrdenFecha
                
                If bHayPaginacion Then
                    If bPaginacionSig Then
                        sWherePaginacion = " ((ISNULL(ADJUN.ADJUNTOS,0)>" & m_iUltimoAdjuntos & " OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(ADJUN.ADJUNTOS,0)=" & m_iUltimoAdjuntos & " AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA<CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(ADJUN.ADJUNTOS,0)=" & m_iUltimoAdjuntos & " AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lUltimoCIA & " AND RE.ID>" & m_lUltimoID & ") OR (RE.CIA>" & m_lUltimoCIA & ")))))"
                    Else
                        sWherePaginacion = " ((ISNULL(ADJUN.ADJUNTOS,0)<" & m_iPrimerAdjuntos & " OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(ADJUN.ADJUNTOS,0)=" & m_iPrimerAdjuntos & " AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA>CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(ADJUN.ADJUNTOS,0)=" & m_iPrimerAdjuntos & " AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lPrimerCIA & " AND RE.ID<" & m_lPrimerID & ") OR (RE.CIA<" & m_lPrimerCIA & ")))))"
                    End If
                End If
            Case 2
                sSort = "FECHA DESC"
                sOrderBy = " ORDER BY RE.FECHA " & sOrdenFecha
                
                If bHayPaginacion Then
                    If bPaginacionSig Then
                        sWherePaginacion = " ((RE.FECHA<CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lUltimoCIA & " AND RE.ID>" & m_lUltimoID & ") OR (RE.CIA>" & m_lUltimoCIA & "))))"
                    Else
                        sWherePaginacion = " ((RE.FECHA>CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lPrimerCIA & " AND RE.ID<" & m_lPrimerID & ") OR (RE.CIA<" & m_lPrimerCIA & "))))"
                    End If
                End If
            Case 3
                sSort = "CIACOD,FECHA DESC"
                sOrderBy = " ORDER BY CIACOD " & sOrden & ",RE.FECHA " & sOrdenFecha
                
                If bHayPaginacion Then
                    If bPaginacionSig Then
                        sWherePaginacion = " ((CIAS.COD>" & StrToSQLNULL(m_sUltimoCiaCod) & " OR "
                        sWherePaginacion = sWherePaginacion & " (CIAS.COD=" & StrToSQLNULL(m_sUltimoCiaCod) & " AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA<CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (CIAS.COD=" & StrToSQLNULL(m_sUltimoCiaCod) & " AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lUltimoCIA & " AND RE.ID>" & m_lUltimoID & ") OR (RE.CIA>" & m_lUltimoCIA & ")))))"
                    Else
                        sWherePaginacion = " ((CIAS.COD<" & StrToSQLNULL(m_sPrimerCiaCod) & " OR "
                        sWherePaginacion = sWherePaginacion & " (CIAS.COD=" & StrToSQLNULL(m_sPrimerCiaCod) & " AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA>CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (CIAS.COD=" & StrToSQLNULL(m_sPrimerCiaCod) & " AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lPrimerCIA & " AND RE.ID<" & m_lPrimerID & ") OR (RE.CIA<" & m_lPrimerCIA & ")))))"
                    End If
                End If
            Case 4
                sSort = "CIADEN,FECHA DESC"
                sOrderBy = " ORDER BY CIADEN " & sOrden & ",RE.FECHA " & sOrdenFecha
                
                If bHayPaginacion Then
                    If bPaginacionSig Then
                        sWherePaginacion = " ((CIAS.DEN>" & StrToSQLNULL(m_sUltimoCiaDen) & " OR "
                        sWherePaginacion = sWherePaginacion & " (CIAS.DEN=" & StrToSQLNULL(m_sUltimoCiaDen) & " AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA<CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (CIAS.DEN=" & StrToSQLNULL(m_sUltimoCiaDen) & " AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lUltimoCIA & " AND RE.ID>" & m_lUltimoID & ") OR (RE.CIA>" & m_lUltimoCIA & ")))))"
                    Else
                        sWherePaginacion = " ((CIAS.DEN<" & StrToSQLNULL(m_sPrimerCiaDen) & " OR "
                        sWherePaginacion = sWherePaginacion & " (CIAS.DEN=" & StrToSQLNULL(m_sPrimerCiaDen) & " AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA>CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (CIAS.DEN=" & StrToSQLNULL(m_sPrimerCiaDen) & " AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lPrimerCIA & " AND RE.ID<" & m_lPrimerID & ") OR (RE.CIA<" & m_lPrimerCIA & ")))))"
                    End If
                End If
            Case 5
                sSort = "PARA,FECHA DESC"
                sOrderBy = " ORDER BY ISNULL(RE.PARA,'') " & sOrden & ",RE.FECHA " & sOrdenFecha
                
                If bHayPaginacion Then
                    If bPaginacionSig Then
                        sWherePaginacion = " ((ISNULL(RE.PARA,'')>'" & NullToStr(m_vUltimoPara) & "' OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(RE.PARA,'')='" & NullToStr(m_vUltimoPara) & "' AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA<CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(RE.PARA,'')='" & NullToStr(m_vUltimoPara) & "' AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lUltimoCIA & " AND RE.ID>" & m_lUltimoID & ") OR (RE.CIA>" & m_lUltimoCIA & ")))))"
                    Else
                        sWherePaginacion = " ((ISNULL(RE.PARA,'')<'" & NullToStr(m_vPrimerPara) & "' OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(RE.PARA,'')='" & NullToStr(m_vPrimerPara) & "' AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA>CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(RE.PARA,'')='" & NullToStr(m_vPrimerPara) & "' AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lPrimerCIA & " AND RE.ID<" & m_lPrimerID & ") OR (RE.CIA<" & m_lPrimerCIA & ")))))"
                    End If
                End If
            Case 6
                sSort = "SUBJECT,FECHA DESC"
                sOrderBy = " ORDER BY ISNULL(RE.SUBJECT,'') " & sOrden & ",RE.FECHA " & sOrdenFecha
                
                If bHayPaginacion Then
                    If bPaginacionSig Then
                        sWherePaginacion = " ((ISNULL(RE.SUBJECT,'')>'" & NullToStr(m_vUltimoSubject) & "' OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(RE.SUBJECT,'')='" & NullToStr(m_vUltimoSubject) & "' AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA<CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(RE.SUBJECT,'')='" & NullToStr(m_vUltimoSubject) & "' AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lUltimoCIA & " AND RE.ID>" & m_lUltimoID & ") OR (RE.CIA>" & m_lUltimoCIA & ")))))"
                    Else
                        sWherePaginacion = " ((ISNULL(RE.SUBJECT,'')<'" & NullToStr(m_vPrimerSubject) & "' OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(RE.SUBJECT,'')='" & NullToStr(m_vPrimerSubject) & "' AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA>CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(RE.SUBJECT,'')='" & NullToStr(m_vPrimerSubject) & "' AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lPrimerCIA & " AND RE.ID<" & m_lPrimerID & ") OR (RE.CIA<" & m_lPrimerCIA & ")))))"
                    End If
                End If
            Case 7
                sSort = "CC,FECHA DESC"
                sOrderBy = " ORDER BY ISNULL(RE.CC,'') " & sOrden & ",RE.FECHA " & sOrdenFecha
                
                If bHayPaginacion Then
                    If bPaginacionSig Then
                        sWherePaginacion = " ((ISNULL(RE.CC,'')>'" & NullToStr(m_vUltimoCC) & "' OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(RE.CC,'')='" & NullToStr(m_vUltimoCC) & "' AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA<CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(RE.CC,'')='" & NullToStr(m_vUltimoCC) & "' AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lUltimoCIA & " AND RE.ID>" & m_lUltimoID & ") OR (RE.CIA>" & m_lUltimoCIA & ")))))"
                    Else
                        sWherePaginacion = " ((ISNULL(RE.CC,'')<'" & NullToStr(m_vPrimerCC) & "' OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(RE.CC,'')='" & NullToStr(m_vPrimerCC) & "' AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA>CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(RE.CC,'')='" & NullToStr(m_vPrimerCC) & "' AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lPrimerCIA & " AND RE.ID<" & m_lPrimerID & ") OR (RE.CIA<" & m_lPrimerCIA & ")))))"
                    End If
                End If
            Case 8
                sSort = "CCO,FECHA DESC"
                sOrderBy = " ORDER BY ISNULL(RE.CCO,'') " & sOrden & ",RE.FECHA " & sOrdenFecha
                
                If bHayPaginacion Then
                    If bPaginacionSig Then
                        sWherePaginacion = " ((ISNULL(RE.CCO,'')>'" & NullToStr(m_vUltimoCCO) & "' OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(RE.CCO,'')='" & NullToStr(m_vUltimoCCO) & "' AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA<CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(RE.CCO,'')='" & NullToStr(m_vUltimoCCO) & "' AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lUltimoCIA & " AND RE.ID>" & m_lUltimoID & ") OR (RE.CIA>" & m_lUltimoCIA & ")))))"
                    Else
                        sWherePaginacion = " ((ISNULL(RE.CCO,'')<'" & NullToStr(m_vPrimerCCO) & "' OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(RE.CCO,'')='" & NullToStr(m_vPrimerCCO) & "' AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA>CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (ISNULL(RE.CCO,'')='" & NullToStr(m_vPrimerCCO) & "' AND "
                        sWherePaginacion = sWherePaginacion & " RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lPrimerCIA & " AND RE.ID<" & m_lPrimerID & ") OR (RE.CIA<" & m_lPrimerCIA & ")))))"
                    End If
                End If
            Case Else
                sSort = "FECHA DESC"
                sOrderBy = " ORDER BY RE.FECHA " & sOrdenFecha
                
                If bHayPaginacion Then
                    If bPaginacionSig Then
                        sWherePaginacion = " ((RE.FECHA<CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sUltimoFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lUltimoCIA & " AND RE.ID>" & m_lUltimoID & ") OR (RE.CIA>" & m_lUltimoCIA & "))))"
                    Else
                        sWherePaginacion = " ((RE.FECHA>CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110)) OR "
                        sWherePaginacion = sWherePaginacion & " (RE.FECHA=CONVERT(datetime," & StrToSQLNULL(m_sPrimerFecha) & ",110) AND "
                        sWherePaginacion = sWherePaginacion & "((RE.CIA=" & m_lPrimerCIA & " AND RE.ID<" & m_lPrimerID & ") OR (RE.CIA<" & m_lPrimerCIA & "))))"
                    End If
                End If
        End Select
        If sWhere = vbNullString And sWherePaginacion <> vbNullString Then
            sWherePaginacion = " WHERE " & sWherePaginacion
        ElseIf sWhere <> vbNullString And sWherePaginacion <> vbNullString Then
            sWherePaginacion = " AND " & sWherePaginacion
        End If
        
        sConsulta = sConsulta & sWhere & sWherePaginacion & sOrderBy
        
        'Obtener los registros
        Set rsReg = New ADODB.Recordset
        rsReg.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        rsReg.Sort = sSort
        If rsReg.RecordCount > 0 Then
            rsReg.MoveLast
            m_lUltimoCIA = rsReg("CIA").Value
            m_lUltimoID = rsReg("ID").Value
            m_iUltimoAdjuntos = rsReg("ADJUNTOS").Value
            m_sUltimoFecha = rsReg("FECHA").Value
            m_sUltimoCiaCod = rsReg("CIACOD").Value
            m_sUltimoCiaDen = rsReg("CIADEN").Value
            m_vUltimoPara = rsReg("PARA").Value
            m_vUltimoSubject = rsReg("SUBJECT").Value
            m_vUltimoCC = rsReg("CC").Value
            m_vUltimoCCO = rsReg("CCO").Value

            rsReg.MoveFirst
            m_lPrimerCIA = rsReg("CIA").Value
            m_lPrimerID = rsReg("ID").Value
            m_iPrimerAdjuntos = rsReg("ADJUNTOS").Value
            m_sPrimerFecha = rsReg("FECHA").Value
            m_sPrimerCiaCod = rsReg("CIACOD").Value
            m_sPrimerCiaDen = rsReg("CIADEN").Value
            m_vPrimerPara = rsReg("PARA").Value
            m_vPrimerSubject = rsReg("SUBJECT").Value
            m_vPrimerCC = rsReg("CC").Value
            m_vPrimerCCO = rsReg("CCO").Value
            
            While Not rsReg.eof
                dtFecha = CDate(Left(rsReg("FECHA").Value, Len(rsReg("FECHA").Value) - 4))
                Me.Add rsReg("CIA").Value, rsReg("ID").Value, dtFecha, rsReg("TIPO").Value, rsReg("SUBJECT").Value, rsReg("PARA").Value, rsReg("CC").Value, rsReg("CCO").Value, , , IIf(rsReg("ADJUNTOS").Value > 0, True, False)
                rsReg.MoveNext
            Wend
            rsReg.MoveFirst
        End If
        
        Set rsReg.ActiveConnection = Nothing
    End If
    
    Set CargarComunicacionesDesdeConPaginacion = rsReg
    
    Set rsCuenta = Nothing
    Set rsReg = Nothing
End Function

