VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGestorParametros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CGestorParametros **********************************
'*             Autor : Elena Mu�oz
'*             Creada : 30/11/01
'* **************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion


Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property

Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property

''' <summary>Carga los parametros generales de la aplicacion </summary>
''' <remarks>Llamada desde=frmparametros.cmdAceptar_click;basSubmain.Main
'''; Tiempo m�ximo=0seg.</remarks>
''' <remarks>Revisado por: LTG  Fecha: 02/06/2011</remarks>

Public Function DevolverParametrosGenerales() As ParametrosGenerales
'***********************************************************
'*** Descripci�n: Devuelve los par�metros generales.     ***
'*** Par�metros:  --------                               ***
'*** Valor que devuelve: un udt ParametrosGenerales.     ***
'***********************************************************
    Dim adores As adodb.Recordset
    Dim sConsulta As String
    Dim sfsg As String
    Dim fecpwd As Variant
    Dim sPwd As Variant
    Dim sPrefijo As String
    
    'Carga la moneda central

    sConsulta = "SELECT MONCEN,CARGAMAX FROM PARGEN_DEF WITH (NOLOCK) "

    Set adores = New adodb.Recordset

    adores.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

    If Not adores.eof Then
        g_udtParametrosGenerales.g_iMonedaCentral = adores("MONCEN").Value
        g_udtParametrosGenerales.g_lCargaMaxima = adores("CARGAMAX").Value
    End If

    adores.Close
    
    sConsulta = "SELECT IDI.COD,IDI.ID FROM IDI WITH (NOLOCK) INNER JOIN PORT WITH (NOLOCK) ON IDI.ID=PORT.IDIDEF"
    
    adores.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

    If Not adores.eof Then
        g_udtParametrosGenerales.g_sIdioma = adores("COD").Value
        g_udtParametrosGenerales.g_iIdIdioma = adores("ID").Value
    End If
    
    adores.Close

    sConsulta = "SELECT PREMIUM,MAX_ADJUN,UNA_COMPRADORA,PAI,COMPRUEBANIF,BLOQMODIFPROVE,COMPRUEBATFNO, MARCADOR_CARPETA_PLANTILLAS FROM PARGEN_INTERNO WITH (NOLOCK) "

    adores.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    If Not adores.eof Then
        g_udtParametrosGenerales.g_bPremium = adores("PREMIUM").Value
        g_udtParametrosGenerales.g_dblMaxAdjun = adores("MAX_ADJUN").Value
        g_udtParametrosGenerales.g_bUnaCompradora = adores("UNA_COMPRADORA").Value
        g_udtParametrosGenerales.g_lPais = adores("PAI").Value
        g_udtParametrosGenerales.gbCompruebaNIF = (adores("COMPRUEBANIF").Value = 1)
        g_udtParametrosGenerales.gbBloqModifProve = (adores("BLOQMODIFPROVE").Value = 1)
        g_udtParametrosGenerales.gbCompruebaTfno = (adores("COMPRUEBATFNO").Value = 1)
        g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS = NullToStr(adores("MARCADOR_CARPETA_PLANTILLAS").Value)
    End If
    adores.Close
                
    
    sConsulta = "SELECT COLAB FROM PARGEN_INTERNO WITH (NOLOCK) "
    adores.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    If adores(0) Then
        'recuperamos los datos de la tabla PARGEN_COLAB
        g_udtParametrosGenerales.g_bCollab = True
        adores.Close
        sConsulta = "Select ID,SRVNAME,DBNAME,SRVGROUP,GRUPO,WEBSPTS from PARGEN_COLAB WITH (NOLOCK) "
        adores.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        If Not adores.eof Then
            If IsNull(adores("SRVNAME")) Then
                g_udtParametrosGenerales.g_sServerName = ""
            Else
                g_udtParametrosGenerales.g_sServerName = adores("SRVNAME").Value
            End If
            If IsNull(adores("DBNAME")) Then
                g_udtParametrosGenerales.g_sBDName = ""
            Else
                g_udtParametrosGenerales.g_sBDName = adores("DBNAME").Value
            End If
            If IsNull(adores("GRUPO")) Then
                g_udtParametrosGenerales.g_sGrupo = ""
            Else
                g_udtParametrosGenerales.g_sGrupo = adores("GRUPO").Value
            End If
            
            g_udtParametrosGenerales.g_sIPSrv = GetStringSetting("FULLSTEP PS", gInstancia, "Opciones", "IPSPTS")

            
            If IsNull(adores("SRVGROUP")) Then
                 g_udtParametrosGenerales.g_sServerGroup = ""
            Else
                g_udtParametrosGenerales.g_sServerGroup = adores("SRVGROUP").Value
            End If
            If IsNull(adores("WEBSPTS")) Then
                g_udtParametrosGenerales.g_sWebSPTS = ""
            Else
                g_udtParametrosGenerales.g_sWebSPTS = adores("WEBSPTS").Value
            End If
        End If
    Else
        g_udtParametrosGenerales.g_bCollab = False
    End If
    
    
    adores.Close
    sConsulta = "SELECT ID,NOTIF_AUT,NOTIF_DESAUT,NOTIF_CAMB_ACT,NOTIF_REGCIAS,CONFIRM_AUT,CONFIRM_DESAUT,CONFIRM_CAMB_ACT,"
    sConsulta = sConsulta & "CONFIRM_REGCIAS,NOM_PORTAL,URL,ADM,EMAIL,TIPOEMAIL,NOTIF_SOLICREG,NOTIF_SOLICACTIV,NOTIF_SOLICCOMP,"
    sConsulta = sConsulta & "NIVELMINACT,MAILPATH,RPTPATH,MAX_ADJUN_CIAS, MAX_NUM_ACTIVIDADES "
    sConsulta = sConsulta & " FROM PARGEN_GEST WITH (NOLOCK) "
    adores.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    If Not adores.eof Then
        If IsNull(adores("NOTIF_AUT")) Then
            g_udtParametrosGenerales.g_iNotifAut = ""
        Else
            g_udtParametrosGenerales.g_iNotifAut = adores("NOTIF_AUT")
        End If
        If IsNull(adores("NOTIF_DESAUT")) Then
            g_udtParametrosGenerales.g_iNotifDesaut = ""
        Else
            g_udtParametrosGenerales.g_iNotifDesaut = adores("NOTIF_DESAUT")
        End If
        If IsNull(adores("NOTIF_CAMB_ACT")) Then
            g_udtParametrosGenerales.g_iNotifCambAct = ""
        Else
            g_udtParametrosGenerales.g_iNotifCambAct = adores("NOTIF_CAMB_ACT")
        End If
        If IsNull(adores("NOTIF_REGCIAS")) Then
            g_udtParametrosGenerales.g_iNotifRegCias = ""
        Else
            g_udtParametrosGenerales.g_iNotifRegCias = adores("NOTIF_REGCIAS")
        End If
        If IsNull(adores("CONFIRM_AUT")) Then
             g_udtParametrosGenerales.g_iConfirmAut = ""
        Else
            g_udtParametrosGenerales.g_iConfirmAut = adores("CONFIRM_AUT")
        End If
        If IsNull(adores("CONFIRM_DESAUT")) Then
            g_udtParametrosGenerales.g_iConfirmDesaut = ""
        Else
            g_udtParametrosGenerales.g_iConfirmDesaut = adores("CONFIRM_DESAUT")
        End If
        If IsNull(adores("CONFIRM_CAMB_ACT")) Then
             g_udtParametrosGenerales.g_iConfirmCambAct = ""
        Else
            g_udtParametrosGenerales.g_iConfirmCambAct = adores("CONFIRM_CAMB_ACT")
        End If
        If IsNull(adores("CONFIRM_REGCIAS")) Then
            g_udtParametrosGenerales.g_iConfirmRegCias = ""
        Else
            g_udtParametrosGenerales.g_iConfirmRegCias = adores("CONFIRM_REGCIAS")
        End If
        g_udtParametrosGenerales.g_sNomPortal = adores("NOM_PORTAL").Value
        
        g_udtParametrosGenerales.g_sIPSrvDMO = GetStringSetting("FULLSTEP PS", gInstancia, "Opciones", "IPDMO")
        
        g_udtParametrosGenerales.g_sUrl = adores("URL").Value
        g_udtParametrosGenerales.g_iNivelMinAct = adores("NIVELMINACT").Value
        
        
         If IsNull(adores("NOM_PORTAL")) Then
                g_udtParametrosGenerales.g_sNomPortal = ""
            Else
                g_udtParametrosGenerales.g_sNomPortal = adores("NOM_PORTAL")
            End If
            If IsNull(adores("URL")) Then
                 g_udtParametrosGenerales.g_sUrl = ""
            Else
                g_udtParametrosGenerales.g_sUrl = adores("URL")
            End If
            If IsNull(adores("ADM")) Then
                g_udtParametrosGenerales.g_sAdm = ""
            Else
                g_udtParametrosGenerales.g_sAdm = adores("ADM")
            End If
            If IsNull(adores("EMAIL")) Then
                 g_udtParametrosGenerales.g_sEmail = ""
            Else
                g_udtParametrosGenerales.g_sEmail = adores("EMAIL")
            End If
            If IsNull(adores("TIPOEMAIL")) Then
                g_udtParametrosGenerales.g_iTipoEmail = ""
            Else
                g_udtParametrosGenerales.g_iTipoEmail = adores("TIPOEMAIL")
            End If
            If IsNull(adores("NOTIF_SOLICREG")) Then
                 g_udtParametrosGenerales.g_iNotifSolicReg = ""
            Else
                g_udtParametrosGenerales.g_iNotifSolicReg = adores("NOTIF_SOLICREG")
            End If
            If IsNull(adores("NOTIF_SOLICACTIV")) Then
                g_udtParametrosGenerales.g_iNotifSolicAct = ""
            Else
                g_udtParametrosGenerales.g_iNotifSolicAct = adores("NOTIF_SOLICACTIV")
            End If
            If IsNull(adores("NOTIF_SOLICCOMP")) Then
                g_udtParametrosGenerales.g_iNotifSolicComp = ""
            Else
                g_udtParametrosGenerales.g_iNotifSolicComp = adores("NOTIF_SOLICCOMP")
            End If
            
            If IsNull(adores("MAILPATH")) Then
                g_udtParametrosGenerales.g_sMailPath = App.Path & "\plantillas"
            Else
                g_udtParametrosGenerales.g_sMailPath = adores("MAILPATH")
            End If
            
            If IsNull(adores("RPTPATH")) Then
                g_udtParametrosGenerales.g_sRptPath = App.Path & "\RPT"
            Else
                g_udtParametrosGenerales.g_sRptPath = adores("RPTPATH")
            End If
            
            g_udtParametrosGenerales.g_dblMaxAdjunCias = adores("MAX_ADJUN_CIAS").Value
            g_udtParametrosGenerales.g_iMaxActividades = adores("MAX_NUM_ACTIVIDADES").Value
        
    End If
        
    adores.Close
    
    ' Ahora recuperamos los asuntos/subjects de los e-mails
    sConsulta = "SELECT SPA, ENG, GER, FRA FROM WEBTEXT WITH (NOLOCK) WHERE MODULO=73"
    adores.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    g_udtParametrosGenerales.g_sNotifAutSPA = adores.Fields.Item("SPA")
    g_udtParametrosGenerales.g_sNotifAutENG = adores.Fields.Item("ENG")
    g_udtParametrosGenerales.g_sNotifAutGER = adores.Fields.Item("GER")
    g_udtParametrosGenerales.g_sNotifAutFRA = adores.Fields.Item("FRA")
    adores.MoveNext
    g_udtParametrosGenerales.g_sNotifDesautSPA = adores.Fields.Item("SPA")
    g_udtParametrosGenerales.g_sNotifDesautENG = adores.Fields.Item("ENG")
    g_udtParametrosGenerales.g_sNotifDesautGER = adores.Fields.Item("GER")
    g_udtParametrosGenerales.g_sNotifDesautFRA = adores.Fields.Item("FRA")
    adores.MoveNext
    g_udtParametrosGenerales.g_sNotifConfirmCambActSPA = adores.Fields.Item("SPA")
    g_udtParametrosGenerales.g_sNotifConfirmCambActENG = adores.Fields.Item("ENG")
    g_udtParametrosGenerales.g_sNotifConfirmCambActGER = adores.Fields.Item("GER")
    adores.MoveNext
    g_udtParametrosGenerales.g_sNotifRechazoCambActSPA = adores.Fields.Item("SPA")
    g_udtParametrosGenerales.g_sNotifRechazoCambActENG = adores.Fields.Item("ENG")
    g_udtParametrosGenerales.g_sNotifRechazoCambActGER = adores.Fields.Item("GER")
    adores.MoveNext
    g_udtParametrosGenerales.g_sNotifConfirmRegCiasSPA = adores.Fields.Item("SPA")
    g_udtParametrosGenerales.g_sNotifConfirmRegCiasENG = adores.Fields.Item("ENG")
    g_udtParametrosGenerales.g_sNotifConfirmRegCiasGER = adores.Fields.Item("GER")
    adores.MoveNext
    g_udtParametrosGenerales.g_sNotifRechazoRegCiasSPA = adores.Fields.Item("SPA")
    g_udtParametrosGenerales.g_sNotifRechazoRegCiasENG = adores.Fields.Item("ENG")
    g_udtParametrosGenerales.g_sNotifRechazoRegCiasGER = adores.Fields.Item("GER")
    adores.MoveNext
    adores.MoveNext
    adores.MoveNext
    adores.MoveNext
    g_udtParametrosGenerales.g_sNotifDesbloqueoUsuarioSPA = adores.Fields.Item("SPA")
    g_udtParametrosGenerales.g_sNotifDesbloqueoUsuarioENG = adores.Fields.Item("ENG")
    g_udtParametrosGenerales.g_sNotifDesbloqueoUsuarioGER = adores.Fields.Item("GER")
    adores.Close
        
    sConsulta = "SELECT SPA, ENG, GER FROM WEBTEXT WITH (NOLOCK) WHERE MODULO=73"
    adores.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    g_udtParametrosGenerales.g_sNotifSolicRegSPA = adores.Fields.Item("SPA")
    g_udtParametrosGenerales.g_sNotifSolicRegENG = adores.Fields.Item("ENG")
    g_udtParametrosGenerales.g_sNotifSolicRegGER = adores.Fields.Item("GER")
    adores.MoveNext
    g_udtParametrosGenerales.g_sNotifSolicActSPA = adores.Fields.Item("SPA")
    g_udtParametrosGenerales.g_sNotifSolicActENG = adores.Fields.Item("ENG")
    g_udtParametrosGenerales.g_sNotifSolicActGER = adores.Fields.Item("GER")
    adores.MoveNext
    g_udtParametrosGenerales.g_sNotifSolicCompSPA = adores.Fields.Item("SPA")
    g_udtParametrosGenerales.g_sNotifSolicCompENG = adores.Fields.Item("ENG")
    g_udtParametrosGenerales.g_sNotifSolicCompGER = adores.Fields.Item("GER")
    
    adores.Close
    basParametros.g_udtParametrosGenerales.gbSincronizacion = False
    'Si trabajamos con una sola compradora recuperamos su ID
    If basParametros.g_udtParametrosGenerales.g_bUnaCompradora Then
        sConsulta = "SELECT ID,FSGS_SRV,FSGS_BD FROM CIAS WITH (NOLOCK) WHERE FCEST=3"
        adores.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        If Not adores.eof Then
            basParametros.g_udtParametrosGenerales.g_lIdCiaCompradora = adores("ID").Value
            basParametros.g_udtParametrosGenerales.g_sFSSRVCiaCompradora = adores("FSGS_SRV").Value
            basParametros.g_udtParametrosGenerales.g_sFSBDCiaCompradora = adores("FSGS_BD").Value
            adores.Close
            
            sfsg = basParametros.g_udtParametrosGenerales.g_sFSSRVCiaCompradora & "." & basParametros.g_udtParametrosGenerales.g_sFSBDCiaCompradora & ".dbo."
            sConsulta = "SELECT SINC_MAT,PYMES,ACCESO_FSGA FROM " & sfsg & "PARGEN_INTERNO WITH (NOLOCK)"
            adores.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
            If Not adores.eof Then
                g_udtParametrosGenerales.gbSincronizacion = SQLBinaryToBoolean(adores.Fields("SINC_MAT").Value)
                g_udtParametrosGenerales.g_bPymes = SQLBinaryToBoolean(adores("PYMES").Value)
                If g_udtParametrosGenerales.g_iNivelMinAct = 5 And g_udtParametrosGenerales.gbSincronizacion = True Then
                    g_udtParametrosGenerales.g_iNivelMinAct = 4
                End If
                g_udtParametrosGenerales.g_bAccesoFSGA = adores("ACCESO_FSGA").Value
            End If
            adores.Close
        Else
            adores.Close
        End If
    End If
    
    
    sConsulta = "SELECT SERVIDOR,PUERTO,SSL,AUTENTICACION,BASICA_GS,USU,PWD,USAR_CUENTA,CUENTAMAIL,MOSTRARMAIL,"
    sConsulta = sConsulta & "ACUSERECIBO,CC,CCO,RESPUESTAMAIL,FECPWD,METODO_ENTREGA,DOMINIO"
    sConsulta = sConsulta & " FROM PARGEN_MAIL WITH (NOLOCK) WHERE ID=1"
    Set adores = New adodb.Recordset
    adores.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    If Not adores.eof Then
        With g_udtParametrosGenerales
            .gsSMTPServer = NullToStr(adores("SERVIDOR").Value)
            .giSMTPPort = NullToDbl0(adores("PUERTO").Value)
            .gbSMTPSSL = SQLBinaryToBoolean(adores("SSL").Value)
            .giSMTPAutent = NullToDbl0(adores("AUTENTICACION").Value)
            .gbSMTPBasicaGS = SQLBinaryToBoolean(adores("BASICA_GS").Value)
            .gsSMTPUser = NullToStr(adores("USU").Value)
            .gsSMTPPwd = NullToStr(adores("PWD").Value)
            .gbSMTPUsarCuenta = SQLBinaryToBoolean(adores("USAR_CUENTA").Value)
            .gsSMTPCuentaMail = NullToStr(adores("CUENTAMAIL").Value)
            .gbMostrarMail = SQLBinaryToBoolean(adores("MOSTRARMAIL").Value)
            .gbAcuseRecibo = SQLBinaryToBoolean(adores("ACUSERECIBO").Value)
            .gsSMTPCC = NullToStr(adores("CC").Value)
            .gsSMTPCCO = NullToStr(adores("CCO").Value)
            .gsSMTPRespuestaMail = NullToStr(adores("RESPUESTAMAIL").Value)
            .giSMTPMetodoEntrega = NullToDbl0(adores("METODO_ENTREGA").Value)
            .gsSMTPDominio = NullToStr(adores("DOMINIO").Value)
            fecpwd = adores("FECPWD").Value
        End With
    End If
    adores.Close
    Set adores = Nothing

    If IsDate(fecpwd) Then
        sPwd = basUtilidades.DesEncriptar(g_udtParametrosGenerales.gsSMTPPwd, fecpwd)
        g_udtParametrosGenerales.gsSMTPPwd = sPwd
    End If
    
    Set adores = Nothing
    
    DevolverParametrosGenerales = g_udtParametrosGenerales

End Function

Public Function GuardarParametrosGenerales(udtParametrosGenerales As ParametrosGenerales, sInstancia As String, Optional ByVal iNivel As Byte, Optional ByVal sServidor As String, Optional ByVal sBaseDatos As String) As CTESError
'*****************************************************************************************************
'*** Descripci�n: Guarda los par�metros generales del PS en la base de datos y                     ***
'***              devuelve el tipo de error en caso de que este se produzca.                       ***
'***                                                                                               ***
'*** Par�metros: ::>> udtParametrosGenerales: es la estructura d�nde se guarda el valor de los     ***
'***                  par�metros generales del PS.                                                 ***
'***             ::>> sInstancia:                                                                  ***
'***             ::>> iNivel:                                                                      ***
'***                                                                                               ***
'*** Valor que devuelve: Una estructura de tipo CTESError que contendr� el error en caso de que    ***
'***                     este se produzca.                                                         ***
'*****************************************************************************************************
    Dim sConsulta As String
    Dim TESError As CTESError
    Dim bTransaccionencurso As Boolean
    Dim adoCom As adodb.Command
    Dim oParam As adodb.Parameter
    Dim adoRS As adodb.Recordset
    Dim iResultado As Integer
    
    Set TESError = New CTESError
        
On Error GoTo Error

    TESError.NumError = TESnoerror
    
    If sServidor = "" Or sBaseDatos = "" Then
        sServidor = GetStringSetting("FULLSTEP PS", sInstancia, "Conexion", "Servidor")
        sBaseDatos = GetStringSetting("FULLSTEP PS", sInstancia, "Conexion", "BaseDeDatos")
    End If
    
   
    m_oConexion.AdoCon.Execute "BEGIN TRANSACTION"
    bTransaccionencurso = True
    
    If iNivel <> 0 Then
        If udtParametrosGenerales.g_iNivelMinAct > iNivel Then
            Set adoCom = New adodb.Command
            Set adoCom.ActiveConnection = m_oConexion.AdoCon
            Set oParam = adoCom.CreateParameter("NIVEL", adSmallInt, adParamInput, , udtParametrosGenerales.g_iNivelMinAct)
            adoCom.Parameters.Append oParam
            Set oParam = adoCom.CreateParameter("RES", adSmallInt, adParamOutput)
            adoCom.Parameters.Append oParam
            Set adoCom.ActiveConnection = m_oConexion.AdoCon
            adoCom.CommandType = adCmdStoredProc
            adoCom.CommandText = "SP_CAMBIO_NIVEL_ACT_CIAS"
            adoCom.Prepared = True
            adoCom.CommandTimeout = 1800
            
            adoCom.Execute
                    
            iResultado = NullToDbl0(adoCom.Parameters(1).Value)
    
            Set adoCom = Nothing
                
            If iResultado <> 0 Then
                m_oConexion.AdoCon.Execute "ROLLBACK"
                TESError.NumError = iResultado
                Set GuardarParametrosGenerales = TESError
                Exit Function
            End If
        End If
    End If
    
    sConsulta = "UPDATE PARGEN_INTERNO SET"
    If udtParametrosGenerales.g_bPremium Then
        sConsulta = sConsulta & " PREMIUM=1"
    Else
        sConsulta = sConsulta & " PREMIUM=0"
    End If
    sConsulta = sConsulta & ",MAX_ADJUN=" & DblToSQLFloat(udtParametrosGenerales.g_dblMaxAdjun)
    
    If udtParametrosGenerales.g_bUnaCompradora Then
        sConsulta = sConsulta & ",UNA_COMPRADORA=1"
    Else
        sConsulta = sConsulta & ",UNA_COMPRADORA=0"
    End If
    
    sConsulta = sConsulta & " WHERE ID=1"
    m_oConexion.AdoCon.Execute sConsulta
    
    sConsulta = "UPDATE PARGEN_GEST SET"
    sConsulta = sConsulta & " NOTIF_AUT=" & udtParametrosGenerales.g_iNotifAut
    sConsulta = sConsulta & ",NOTIF_DESAUT=" & udtParametrosGenerales.g_iNotifDesaut
    sConsulta = sConsulta & ",NOTIF_CAMB_ACT=" & udtParametrosGenerales.g_iNotifCambAct
    sConsulta = sConsulta & ",NOTIF_REGCIAS=" & udtParametrosGenerales.g_iNotifRegCias
    sConsulta = sConsulta & ",CONFIRM_AUT=" & udtParametrosGenerales.g_iConfirmAut
    sConsulta = sConsulta & ",CONFIRM_DESAUT=" & udtParametrosGenerales.g_iConfirmDesaut
    sConsulta = sConsulta & ",CONFIRM_CAMB_ACT=" & udtParametrosGenerales.g_iConfirmCambAct
    sConsulta = sConsulta & ",CONFIRM_REGCIAS=" & udtParametrosGenerales.g_iConfirmRegCias
    sConsulta = sConsulta & ",NOTIF_SOLICREG=" & udtParametrosGenerales.g_iNotifSolicReg
    sConsulta = sConsulta & ",NOTIF_SOLICACTIV=" & udtParametrosGenerales.g_iNotifSolicAct
    sConsulta = sConsulta & ",NOTIF_SOLICCOMP=" & udtParametrosGenerales.g_iNotifSolicComp
    sConsulta = sConsulta & ",MAILPATH='" & DblQuote(udtParametrosGenerales.g_sMailPath) & "'"
    sConsulta = sConsulta & ",RPTPATH='" & DblQuote(udtParametrosGenerales.g_sRptPath) & "'"
    sConsulta = sConsulta & ",TIPOEMAIL=" & udtParametrosGenerales.g_iTipoEmail
    sConsulta = sConsulta & ",NIVELMINACT=" & udtParametrosGenerales.g_iNivelMinAct

    
    If udtParametrosGenerales.g_sNomPortal <> "" Then
        sConsulta = sConsulta & ",NOM_PORTAL=" & "'" & udtParametrosGenerales.g_sNomPortal & "' "
    End If
    
    If udtParametrosGenerales.g_sUrl <> "" Then
        sConsulta = sConsulta & ",URL=" & "'" & udtParametrosGenerales.g_sUrl & "' "
    End If
    
    If udtParametrosGenerales.g_sAdm <> "" Then
        sConsulta = sConsulta & ",ADM=" & "'" & udtParametrosGenerales.g_sAdm & "' "
    End If
    
    If udtParametrosGenerales.g_sEmail <> "" Then
        sConsulta = sConsulta & ",EMAIL=" & "'" & udtParametrosGenerales.g_sEmail & "' "
    End If
    
    sConsulta = sConsulta & ",MAX_ADJUN_CIAS=" & DblToSQLFloat(udtParametrosGenerales.g_dblMaxAdjunCias)
    sConsulta = sConsulta & ",MAX_NUM_ACTIVIDADES = " & udtParametrosGenerales.g_iMaxActividades
    sConsulta = sConsulta & " WHERE ID=1"
    
    m_oConexion.AdoCon.Execute sConsulta
    
    Set adoRS = New adodb.Recordset
    sConsulta = "SELECT IDI.ID FROM IDI WITH (NOLOCK) WHERE IDI.COD = '" & DblQuote(udtParametrosGenerales.g_sIdioma) & "'"
    
    adoRS.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly

    sConsulta = "UPDATE PORT SET IDIDEF = " & adoRS.Fields("ID").Value
    adoRS.Close
    
    m_oConexion.AdoCon.Execute sConsulta
    
    Dim dfechahoracrypt As Variant
    Dim sPassUsu As Variant
    
    
    sConsulta = "UPDATE PARGEN_MAIL SET TIPOCLTEMAIL=2 "
    sConsulta = sConsulta & ",SERVIDOR=" & StrToSQLNULL(udtParametrosGenerales.gsSMTPServer)
    sConsulta = sConsulta & ",PUERTO=" & udtParametrosGenerales.giSMTPPort
    sConsulta = sConsulta & ",SSL=" & BooleanToSQLBinary(udtParametrosGenerales.gbSMTPSSL)
    sConsulta = sConsulta & ",AUTENTICACION=" & udtParametrosGenerales.giSMTPAutent
    If udtParametrosGenerales.giSMTPAutent = 0 Then
        sConsulta = sConsulta & ",BASICA_GS=NULL"
        sConsulta = sConsulta & ",USU=NULL"
        sConsulta = sConsulta & ",PWD=NULL"
        sConsulta = sConsulta & ",FECPWD=NULL"
        sConsulta = sConsulta & ",DOMINIO=NULL"
    ElseIf udtParametrosGenerales.giSMTPAutent = 1 Then 'BASICA
        sConsulta = sConsulta & ",BASICA_GS=" & BooleanToSQLBinary(udtParametrosGenerales.gbSMTPBasicaGS)
        sConsulta = sConsulta & ",USU=" & StrToSQLNULL(udtParametrosGenerales.gsSMTPUser)
        If udtParametrosGenerales.gsSMTPPwd <> "" Then
            dfechahoracrypt = Now
            sPassUsu = basUtilidades.Encriptar(udtParametrosGenerales.gsSMTPPwd, dfechahoracrypt)
            sConsulta = sConsulta & ",PWD=" & StrToSQLNULL(sPassUsu)
            sConsulta = sConsulta & ",FECPWD=" & DateToSQLTimeDate(dfechahoracrypt)
        Else
            sConsulta = sConsulta & ",PWD=NULL"
            sConsulta = sConsulta & ",FECPWD=NULL"
        End If
        sConsulta = sConsulta & ",DOMINIO=NULL"
    Else
        sConsulta = sConsulta & ",BASICA_GS=NULL"
        sConsulta = sConsulta & ",USU=" & StrToSQLNULL(udtParametrosGenerales.gsSMTPUser)
        If udtParametrosGenerales.gsSMTPPwd <> "" Then
            dfechahoracrypt = Now
            sPassUsu = basUtilidades.Encriptar(udtParametrosGenerales.gsSMTPPwd, dfechahoracrypt)
            sConsulta = sConsulta & ",PWD=" & StrToSQLNULL(sPassUsu)
            sConsulta = sConsulta & ",FECPWD=" & DateToSQLTimeDate(dfechahoracrypt)
        Else
            sConsulta = sConsulta & ",PWD=NULL"
            sConsulta = sConsulta & ",FECPWD=NULL"
        End If
        sConsulta = sConsulta & ",DOMINIO=" & StrToSQLNULL(udtParametrosGenerales.gsSMTPDominio)
    End If
    sConsulta = sConsulta & ",USAR_CUENTA=" & BooleanToSQLBinary(udtParametrosGenerales.gbSMTPUsarCuenta)
    sConsulta = sConsulta & ",CUENTAMAIL=" & StrToSQLNULL(udtParametrosGenerales.gsSMTPCuentaMail)
    sConsulta = sConsulta & ",MOSTRARMAIL=" & BooleanToSQLBinary(udtParametrosGenerales.gbMostrarMail)
    sConsulta = sConsulta & ",ACUSERECIBO=" & BooleanToSQLBinary(udtParametrosGenerales.gbAcuseRecibo)
    sConsulta = sConsulta & ",CC=" & StrToSQLNULL(udtParametrosGenerales.gsSMTPCC)
    sConsulta = sConsulta & ",CCO=" & StrToSQLNULL(udtParametrosGenerales.gsSMTPCCO)
    sConsulta = sConsulta & ",RESPUESTAMAIL=" & StrToSQLNULL(udtParametrosGenerales.gsSMTPRespuestaMail)
    sConsulta = sConsulta & ",METODO_ENTREGA=" & IIf(IsNull(udtParametrosGenerales.giSMTPMetodoEntrega) Or IsEmpty(udtParametrosGenerales.giSMTPMetodoEntrega), 0, udtParametrosGenerales.giSMTPMetodoEntrega)
    
    sConsulta = sConsulta & " WHERE ID=1"
    m_oConexion.AdoCon.Execute sConsulta
    
    
    m_oConexion.AdoCon.Execute "COMMIT TRANSACTION"
    bTransaccionencurso = False
    
    
    g_udtParametrosGenerales = udtParametrosGenerales
                    
fin:
    Set GuardarParametrosGenerales = TESError
    Exit Function
Error:

    If m_oConexion.AdoCon.Errors.Count > 0 Then
        Set TESError = TratarError(m_oConexion.AdoCon.Errors)
    Else
           
        TESError.NumError = TESOtroerror
    End If
    If NumTransaccionesAbiertas(m_oConexion.AdoCon) > 0 Then
        m_oConexion.AdoCon.Execute "ROLLBACK"
    End If
    Resume fin

End Function


''' <summary>Carga los idiomas del portal </summary>
''' <param name="bOrdenDen">Si se ordena por denominacion</param>
''' <param name="bTodos">Devuelete todos</param>
''' <remarks>Llamada desde=frmMon.Form_load
''frmEstactDetalle.Form_Load
''frmLstMon.Form_Load
'''; Tiempo m�ximo=0seg.</remarks>
Public Function DevolverIdiomas(Optional ByVal bOrdenDen As Boolean, Optional ByVal bTodos As Boolean) As CIdiomas
    
    Dim rs As New adodb.Recordset
    Dim oIdiomas As CIdiomas
    Dim sConsulta As String
    Dim sFSP As String
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldId As adodb.Field
    
    sConsulta = "SELECT * FROM IDI WITH (NOLOCK)"
    If Not bTodos = True Then
        sConsulta = sConsulta & " WHERE APLICACION=1"
    End If
        
    If bOrdenDen = True Then
        sConsulta = sConsulta & " ORDER BY DEN"
    End If
        
    rs.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    Set oIdiomas = New CIdiomas
    
    If Not rs.eof Then
        Set fldId = rs.Fields(0)
        Set fldCod = rs.Fields(1)
        Set fldDen = rs.Fields(2)
            
        While Not rs.eof
            oIdiomas.Add fldCod.Value, fldDen.Value, fldId
            rs.MoveNext
        Wend
        
        Set fldCod = Nothing
        Set fldDen = Nothing
        
    End If
                        
  
    rs.Close
    Set rs = Nothing
    
    Set DevolverIdiomas = oIdiomas
    
End Function




''' <summary>Carga los idiomas del portal cargando en primer lugar el idioma del usuario.</summary>
''' <param name></param>
''' <remarks>Llamada desde=frmCompanias;Tiempo m�ximo=0seg.</remarks>

Public Function DevolverIdiomasPrimeroIdiUsu() As CIdiomas
    
    Dim rs As New adodb.Recordset
    Dim oIdiomas As CIdiomas
    Dim sConsulta As String
    Dim sFSP As String
    Dim fldCod As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldId As adodb.Field
        
    
    'Se carga primero el idioma del usuario.
    sConsulta = "SELECT ID,COD,DEN FROM IDI WITH (NOLOCK) WHERE ID=" & g_udtParametrosGenerales.g_iIdIdioma
        
    rs.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    Set oIdiomas = New CIdiomas
    
    If Not rs.eof Then
        Set fldId = rs.Fields(0)
        Set fldCod = rs.Fields(1)
        Set fldDen = rs.Fields(2)
        
        oIdiomas.Add fldCod.Value, fldDen.Value, fldId
        
        Set fldCod = Nothing
        Set fldDen = Nothing
    End If
    
    rs.Close
    
    'Se cargan el resto de idiomas.
    sConsulta = "SELECT ID,COD,DEN FROM IDI WITH (NOLOCK) WHERE ID<>" & g_udtParametrosGenerales.g_iIdIdioma
        
    rs.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        
    If Not rs.eof Then
        Set fldId = rs.Fields(0)
        Set fldCod = rs.Fields(1)
        Set fldDen = rs.Fields(2)
            
        While Not rs.eof
            oIdiomas.Add fldCod.Value, fldDen.Value, fldId
            rs.MoveNext
        Wend
        
        Set fldCod = Nothing
        Set fldDen = Nothing
        
    End If
                        
  
    rs.Close
    Set rs = Nothing
    
    Set DevolverIdiomasPrimeroIdiUsu = oIdiomas
    
End Function

''' <summary>Devuelve el valor de un campo de PARGEN_INTERNO de GS</summary>
''' <param name="lCiaComp">CIA</param>
''' <param name="sParamName">Nombre del campo</param>
''' <remarks>Llamada desde: CRaiz.ValidarLogin ; Tiempo m�ximo: 0</remarks>
''' Revisado por: jbg; Fecha: 24/02/2016
Public Function DevolverParametroInternoGS(ByVal lCiaComp As Long, ByVal sParamName As String) As Variant
    Dim sfsg As String
    Dim sConsulta  As String
    Dim oComm As adodb.Command
    Dim rsParam As adodb.Recordset
    
    On Error GoTo Error
    
    sfsg = FSGS(m_oConexion, lCiaComp)
    
    sConsulta = "SELECT " & sParamName & " FROM " & sfsg & "PARGEN_INTERNO WITH(NOLOCK)"
    
    Set oComm = New adodb.Command
    With oComm
        Set .ActiveConnection = m_oConexion.AdoCon
        .CommandType = adCmdText
        .CommandText = sConsulta
        
        Set rsParam = .Execute
    End With
        
    If Not rsParam Is Nothing Then
        If rsParam.RecordCount > 0 Then
            DevolverParametroInternoGS = rsParam(sParamName)
        End If
    End If
        
Salir:
    Set oComm = Nothing
    Exit Function
Error:
    If Err.Number = 1 Then
        Exit Function
    End If
    Resume Salir
End Function
