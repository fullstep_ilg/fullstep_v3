VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAdjuntoEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAdjuntoEmail **********************************
'*             Autor : Mertxe Martin
'*             Creada : 02/03/04
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oRegistroEmail As CRegistroEmail
Private m_iId As Integer

Private m_sNombre As String
Private m_lDataSize As Long 'Tamanyo del contenido en bytes del campo mvarData
Private m_dtFecact As Date

Private m_vIndice As Variant 'Nos indica la posicion secuencial dentro de una Coleccion

Private m_oConexion As CConexion
Private m_adores As ADODB.Recordset
Private m_adoComm As ADODB.Command

Public Property Set RegistroEmail(ByVal oRegistro As CRegistroEmail)
    Set m_oRegistroEmail = oRegistro
End Property

Public Property Get RegistroEmail() As CRegistroEmail
    Set RegistroEmail = m_oRegistroEmail
End Property

Public Property Get DataSize() As Long
    DataSize = m_lDataSize
End Property

Public Property Let DataSize(ByVal lSize As Long)
    m_lDataSize = lSize
End Property

Public Property Let Fecha(ByVal Dat As Date)
    m_dtFecact = Dat
End Property

Public Property Get Fecha() As Date
    Fecha = m_dtFecact
End Property

Public Property Let ID(ByVal i As Integer)
    m_iId = i
End Property

Public Property Get ID() As Integer
    ID = m_iId
End Property

Public Property Let Indice(ByVal vIndice As Variant)
    m_vIndice = vIndice
End Property

Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Public Property Get Nombre() As String
    Nombre = m_sNombre
End Property

Public Property Let Nombre(ByVal sNombre As String)
    m_sNombre = sNombre
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Function ComenzarLecturaData() As CTESError
Dim adoRes As ADODB.Recordset
Dim sConsulta As String
Dim oError As CTESError

'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    Err.Raise vbObjectError + 613, "CAdjuntoEmail.ComenzarLecturaData", "No se ha establecido la conexion"
    Exit Function
End If
'*************************************************************
        
    Set oError = New CTESError
    oError.NumError = 0
  
    If Not m_oRegistroEmail Is Nothing Then
    
        'Comprobamos si existe a�n la compa��a
        sConsulta = "SELECT * FROM REGISTRO_EMAIL WITH (NOLOCK) WHERE CIA=" & m_oRegistroEmail.Cia & " AND ID=" & m_oRegistroEmail.ID
        Set adoRes = New ADODB.Recordset
        adoRes.Open sConsulta, m_oConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
        If adoRes.eof Then
            adoRes.Close
            Set adoRes = Nothing
            oError.NumError = FSPSServer.ErroresSummit.TESDatoEliminado
            oError.Arg1 = "REGISTRO_EMAIL"
            Set ComenzarLecturaData = oError
            Exit Function
        End If
        adoRes.Close
        Set adoRes = Nothing
    
        sConsulta = "SELECT *, DATALENGTH(DATA) TAMANYO FROM REGISTRO_EMAIL_ADJUN WITH (NOLOCK) WHERE CIA=" & m_oRegistroEmail.Cia & " AND ID_REG=" & m_oRegistroEmail.ID & " AND ID= ?"
        
        Set m_adoComm = New ADODB.Command
        m_adoComm.ActiveConnection = m_oConexion.AdoCon
        m_adoComm.CommandText = sConsulta
        
        m_adoComm.Parameters.Item(0).Value = m_iId
        
        m_adoComm.CommandType = adCmdText
        Set m_adores = m_adoComm.Execute
    
        If m_adores.eof Then
            oError.NumError = FSPSServer.ErroresSummit.TESDatoEliminado
            oError.Arg1 = "REGISTRO_EMAIL_ADJUN"
            m_lDataSize = 0
            m_adores.Close
            Set m_adores = Nothing
            Set m_adoComm = Nothing
            Set ComenzarLecturaData = oError
            Exit Function
        End If
    
        m_lDataSize = m_adores("TAMANYO").Value

    End If
        
    Set ComenzarLecturaData = oError
    Exit Function

Error:
    
    oError = basErrores.TratarError(m_oConexion.AdoCon)
    Set ComenzarLecturaData = oError
    If Not m_adores Is Nothing Then
        m_adores.Close
        Set m_adores = Nothing
        Set m_adoComm = Nothing
    End If
    
End Function

Public Function ReadData(ByVal lInit As Long, ByVal lSize As Long) As Variant
'*****************************************************************************************
'*****  Init:  Inicio del bloque
'*****  lSize: Tama�o del bloque
'*******************************************************************************************

Dim TESError As TipoErrorSummit
Dim adoRes As ADODB.Recordset
Dim adoCommP As ADODB.Command
Dim adoPar As ADODB.Parameter
Dim sFSP As String


On Error GoTo Error

    TESError.NumError = TESnoerror
    Set adoCommP = New ADODB.Command
    
    adoCommP.CommandText = "SP_DOWNLOAD_ADJUNTO_EMAIL"
    
    Set adoPar = adoCommP.CreateParameter("CIA", adInteger, adParamInput, , m_oRegistroEmail.Cia)
    adoCommP.Parameters.Append adoPar
    Set adoPar = adoCommP.CreateParameter("ID_REG", adInteger, adParamInput, , m_oRegistroEmail.ID)
    adoCommP.Parameters.Append adoPar
    Set adoPar = adoCommP.CreateParameter("ADJUN", adInteger, adParamInput, , m_iId)
    adoCommP.Parameters.Append adoPar
    Set adoPar = adoCommP.CreateParameter("INIT", adInteger, adParamInput, , lInit)
    adoCommP.Parameters.Append adoPar
    Set adoPar = adoCommP.CreateParameter("SIZE", adInteger, adParamInput, , lSize)
    adoCommP.Parameters.Append adoPar
    
    adoCommP.CommandType = adCmdStoredProc
    Set adoCommP.ActiveConnection = m_oConexion.AdoCon
    Set adoRes = adoCommP.Execute
    
    ReadData = adoRes.Fields(0).GetChunk(adoRes.Fields(0).ActualSize)
    
fin:
    Exit Function

Error:

    ReadData = basErrores.TratarError(m_oConexion.AdoCon.Errors)
    Resume fin


    
End Function

Private Sub Class_Initialize()
    m_lDataSize = 0
End Sub

Private Sub Class_Terminate()
    Set m_adores = Nothing
    Set m_adoComm = Nothing
    Set m_oConexion = Nothing
End Sub

