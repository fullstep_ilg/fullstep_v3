VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CIdiomas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mCol As Collection
Private m_oConexion As CConexion

''' <summary>A�ade idiomas a la coleccion</summary>
''' <param name="Cod">Codigo del idioma</param>
''' <param name="Den">Denominacion del idioma</param>
''' <param name="Id">Id del idioma</param>
''' <returns>Recordset</returns>
''' <remarks>Llamada desde=Todas las cargas de idiomas de la aplicacion
'''; Tiempo m�ximo=0seg.</remarks>
Public Function Add(ByVal Cod As String, ByVal Den As String, ByVal ID As Integer) As CIdioma
   Dim objnewmember As CIdioma
   
   Set objnewmember = New CIdioma
    
   objnewmember.Cod = Cod
   objnewmember.Den = Den
   objnewmember.ID = ID
   
   mCol.Add objnewmember, Cod
   
End Function

Public Function DevolverCodigoDeIdioma(ByVal ID As Integer) As String
    Dim adores As adodb.Recordset
    Dim sConsulta As String

    sConsulta = "SELECT COD FROM IDI WITH(NOLOCK) WHERE ID=" & ID
    
    Set adores = New adodb.Recordset
    adores.Open sConsulta, Conexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    If Not adores.eof Then
        DevolverCodigoDeIdioma = adores.Fields("COD").Value
    End If
    adores.Close
    Set adores.ActiveConnection = Nothing
End Function

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property
Public Property Get Item(vntIndexKey As Variant) As CIdioma
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property
Public Sub Remove(vntIndexKey As Variant)
    
        mCol.Remove vntIndexKey
    
End Sub
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
    
    Set mCol = New Collection

End Sub
