VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMonedas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

''' Variables que almacenan las propiedades de los objetos

Private mCol As Collection
Private mvarEOF As Boolean
Private mvarConexion As CConexion

''' Control de errores

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

''' <summary>Devuelve monedas</summary>
''' <param name="Cia">Mumero maximo (para el top de la consulta)</param>
''' <param name="OrdenarPorDen">Ordenacion por denominacion</param>
''' <param name="OrdenarPorEQUIV">Ordenacion por equivalencia</param>
''' <param name="CoincidenciaTotal">Query por =</param>
''' <param name="idiColum">Idioma de la columna</param>
''' <returns>Recordset</returns>
''' <remarks>Llamada desde=frmMon.cmdRestaurar_click,frmMon.Form_Load,frmMon.sdbgMonedas_BeforeUpdate,frmMon.sdbgMonedas_HeadClick
'''; Tiempo m�ximo=0seg.</remarks>
Public Function CargarTodasLasMonedas(ByVal NumMaximo As Integer, Optional ByVal OrdenarPorDen As Boolean, Optional ByVal OrdenarPorEQUIV As Boolean, Optional ByVal CarIniUsuCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal IdiColum As String) As ADODB.Recordset
    Dim ador As ADODB.Recordset
    Dim sConsulta As String
    Dim sPrimeraCond As String
    Dim sPrimerOrden As String
    Dim iNumC As Integer
    Dim lIndice As Long
    Dim oDen As CMultiidiomas
    Dim oGestorParametros As CGestorParametros
    Dim oIdiomas As CIdiomas
    Dim oIdi As CIdioma
    Dim sCod As String
    Dim sCod2 As String
    Dim sCod3 As String
    
    ''' * Objetivo: Cargar la coleccion de monedas
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CMonedas.CargarTodasLasMonedas", "No se ha establecido la conexion"
        Exit Function
    End If
    
    'Carga todos los idiomas de la aplicaci�n:
    Set oGestorParametros = New CGestorParametros
    Set oGestorParametros.Conexion = mvarConexion
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, True)
        
    ''' Generacion de SQL a partir de los parametros
    
    For Each oIdi In oIdiomas
        sCod = sCod & ", " & oIdi.Cod & ".DEN AS DEN_" & oIdi.Cod
        sCod2 = sCod2 & " LEFT JOIN MON_DEN " & oIdi.Cod & " WITH(NOLOCK) ON MON.ID = " & oIdi.Cod & ".MON AND " & oIdi.Cod & ".IDIOMA = " & oIdi.ID & " "
        sCod3 = sCod3 & "," & "TABLA.DEN_" & oIdi.Cod ''MultiERP
    Next
    

    
    sConsulta = "SELECT TABLA.ID,TABLA.COD" & sCod3 & ",TABLA.EQUIV FROM ( "
    sConsulta = sConsulta & " SELECT MON.ID,MON.COD" & sCod & ",MON.EQUIV FROM MON WITH (NOLOCK) "
    sConsulta = sConsulta & sCod2
    sConsulta = sConsulta & " ) AS TABLA"
    
    'sConsulta = "SELECT * FROM MON WITH (NOLOCK) "
    If CarIniUsuCod = "" And CarIniDen = "" Then
    Else
        If Not (CarIniUsuCod = "") And Not (CarIniDen = "") Then
    
            If CoincidenciaTotal Then
                sConsulta = sConsulta & " WHERE" & sPrimeraCond
                sConsulta = sConsulta & " COD ='" & DblQuote(CarIniUsuCod) & "'"
                sConsulta = sConsulta & " AND DEN_" & g_udtParametrosGenerales.g_sIdioma & "='" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta = sConsulta & " WHERE" & sPrimeraCond
                sConsulta = sConsulta & " COD LIKE '" & DblQuote(CarIniUsuCod) & "%'"
                sConsulta = sConsulta & " DEN_" & g_udtParametrosGenerales.g_sIdioma & " LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
                        
        Else
            
            If Not (CarIniUsuCod = "") Then
                
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & " WHERE" & sPrimeraCond
                    sConsulta = sConsulta & " COD ='" & DblQuote(CarIniUsuCod) & "'"
                Else
                    sConsulta = sConsulta & " WHERE" & sPrimeraCond
                    sConsulta = sConsulta & " COD LIKE '" & DblQuote(CarIniUsuCod) & "%'"
                End If
                
            Else
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & " WHERE" & sPrimeraCond
                    sConsulta = sConsulta & " DEN_" & g_udtParametrosGenerales.g_sIdioma & "='" & DblQuote(CarIniDen) & "'"
                Else
                    sConsulta = sConsulta & " WHERE" & sPrimeraCond
                    sConsulta = sConsulta & " DEN_" & g_udtParametrosGenerales.g_sIdioma & " LIKE '" & DblQuote(CarIniDen) & "%'"
                End If
                
            End If
        
        End If
               
    End If
    
    
    If OrdenarPorDen Then
        If IdiColum = "" Then
            sConsulta = sConsulta & " ORDER BY " & sPrimerOrden & "DEN_" & g_udtParametrosGenerales.g_sIdioma
        Else
            sConsulta = sConsulta & " ORDER BY " & sPrimerOrden & "DEN_" & IdiColum
        End If
    Else
        If OrdenarPorEQUIV Then
            sConsulta = sConsulta & " ORDER BY " & sPrimerOrden & "EQUIV"
        Else
            sConsulta = sConsulta & " ORDER BY " & sPrimerOrden & "COD"
        End If
    End If
    
    ''' Creacion del Resultset
    
    Set ador = New ADODB.Recordset
    
    Set mCol = Nothing
    Set mCol = New Collection
    
    ador.Open sConsulta, mvarConexion.AdoCon, adOpenForwardOnly, adLockReadOnly
    If ador.eof Then
        Set ador = Nothing
        Exit Function
    End If
    iNumC = 0
    
    While Not ador.eof
        If IsNull(ador("EQUIV").Value) Then
        
            Set oDen = Nothing
            Set oDen = New CMultiidiomas
            Set oDen.Conexion = mvarConexion
            For Each oIdi In oIdiomas
                oDen.Add oIdi.Cod, ador.Fields("DEN_" & oIdi.Cod).Value
            Next
            Me.Add ador("ID").Value, ador("COD").Value, oDen, 0
            
        Else
            Set oDen = Nothing
            Set oDen = New CMultiidiomas
            Set oDen.Conexion = mvarConexion
            For Each oIdi In oIdiomas
                oDen.Add oIdi.Cod, ador.Fields("DEN_" & oIdi.Cod).Value
            Next
            Me.Add ador("ID").Value, ador("COD").Value, oDen, ador("EQUIV").Value
        End If
        
        iNumC = iNumC + 1
        ador.MoveNext
    Wend
    ador.MoveFirst
    
    Set ador.ActiveConnection = Nothing
    Set CargarTodasLasMonedas = ador
        
End Function
Public Function Add(ByVal ID As Integer, ByVal Cod As String, ByVal oDen As CMultiidiomas, ByVal EQUIV As Double) As CMoneda
    
    ''' * Objetivo: Anyadir una moneda a la coleccion
    ''' * Recibe: Datos de la moneda
    ''' * Devuelve: Moneda a�adida
    
    Dim objnewmember As CMoneda
    
    ''' Creacion de objeto moneda
    
    Set objnewmember = New CMoneda
   
    ''' Paso de los parametros al nuevo objeto moneda
    objnewmember.ID = ID
    objnewmember.Cod = Cod
    If Not IsMissing(oDen) Then
        Set objnewmember.Denominaciones = oDen
    Else
        Set objnewmember.Denominaciones = Nothing
    End If
    objnewmember.EQUIV = EQUIV
    
    Set objnewmember.Conexion = mvarConexion
    
    ''' Anyadir el objeto moneda a la coleccion
    mCol.Add objnewmember, CStr(ID)
    
    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function
Public Property Get Item(vntIndexKey As Variant) As CMoneda

    ''' * Objetivo: Recuperar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a recuperar
    ''' * Devuelve: Moneda correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property
Public Property Get eof() As Boolean

    ''' * Objetivo: Devolver variable privada EOF
    ''' * Recibe: Nada
    ''' * Devuelve: Variable privada EOF
    
    eof = mvarEOF
    
End Property
Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set mvarConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = mvarConexion
    
End Property
Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    
   
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
    Set mvarConexion = Nothing
        
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property

    


