Attribute VB_Name = "basUtilidades"
Function StrToVbNULL(StrThatCanBeEmpty) As Variant
    
    If IsEmpty(StrThatCanBeEmpty) Then
        StrToVbNULL = Null
    Else
        If IsNull(StrThatCanBeEmpty) Then
            StrToVbNULL = Null
        Else
            If StrThatCanBeEmpty = "" Then
                StrToVbNULL = Null
            Else
                StrToVbNULL = StrThatCanBeEmpty
            End If
        End If
    End If
    
End Function
Public Function DblQuote(ByVal StrToDblQuote As String) As String

    Dim Ind As Long
    Dim QPos As Long
    Dim pos As Long
    
    Dim TmpDblQuote As String
    
    TmpDblQuote = ""
    pos = 1
    
    Do While Len(StrToDblQuote) > 0
    
        QPos = InStr(pos, StrToDblQuote, "'")
        
        If QPos > 0 Then
            
            TmpDblQuote = TmpDblQuote & Mid(StrToDblQuote, pos, QPos - 1) & "''"
            StrToDblQuote = Right(StrToDblQuote, Len(StrToDblQuote) - QPos)
            
        Else
        
            TmpDblQuote = TmpDblQuote & StrToDblQuote
            Exit Do
            
        End If
            
    Loop
    
    DblQuote = TmpDblQuote
    
End Function


Public Function NullToDbl0(ByVal ValueThatCanBeNull As Variant) As Variant
  
    If IsNull(ValueThatCanBeNull) Then
        NullToDbl0 = 0
    Else
        NullToDbl0 = ValueThatCanBeNull
    End If

End Function


Function DateToSQLDate(DateThatCanBeEmpty) As Variant


If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLDate = "NULL"
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLDate = "NULL"
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLDate = "NULL"
        Else
            DateToSQLDate = "Convert(datetime,'" & Format(DateThatCanBeEmpty, "mm-dd-yyyy") & "',110)"
        End If
    End If
End If

End Function


Function DateToSQLTimeDate(DateThatCanBeEmpty) As Variant


If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLTimeDate = "NULL"
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLTimeDate = "NULL"
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLTimeDate = "NULL"
        Else
            DateToSQLTimeDate = "Convert(datetime,'" & Format(DateThatCanBeEmpty, "mm-dd-yyyy hh:mm:ss") & "',110)"
        End If
    End If
End If

End Function

''' <summary>
''' Encripta un texto
''' </summary>
''' <param name="Dato">texto a encriptar</param>
''' <param name="Fecha">Fecha para la encriptaci�n</param>
''' <returns>Texto encriptado</returns>
''' <remarks>Llamada desde: CGestorParametros.GuardarParametrosGenerales, CUsuario.ModificarDatosUsuario, CUsuario.AnyadirUsuario
''' ; Tiempo m�ximo: 0</remarks>
Public Function Encriptar(ByVal dato As String, Optional ByVal Fecha As Variant) As String
    Dim fechahoracrypt
    Dim diacrypt
    Dim oCrypt2 As CCrypt2

    If IsMissing(Fecha) Then
        fechahoracrypt = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            fechahoracrypt = DateSerial(1974, 3, 5)
        Else
            fechahoracrypt = Fecha
        End If
    End If
    
    diacrypt = Day(Fecha)

    Set oCrypt2 = New CCrypt2
    
    oCrypt2.InBuffer = dato
    If diacrypt Mod 2 = 0 Then
        oCrypt2.Codigo = ClavePar5Bytes
    Else
        oCrypt2.Codigo = ClaveImp5bytes
    End If
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    oCrypt2.Encrypt
    
    Encriptar = oCrypt2.OutBuffer
            
    Set oCrypt2 = Nothing
End Function
''' <summary>
''' Desencripta un texto
''' </summary>
''' <param name="Dato">texto a desencriptar</param>
''' <param name="Fecha">Fecha para la encriptaci�n</param>
''' <returns>Texto desencriptado</returns>
''' <remarks>Llamada desde: CGestorParametros.DevolverParametrosGenerales, CRaiz.ValidarAccesoObjetos, CRaiz.Conectar
''' , CRaiz.SincronizarUsuarios, CUsuario.AutorizarUsuario, CUsuario.ValidarUsuario, CCia.ModificarDatosCia
''' ; Tiempo m�ximo: 0</remarks>
Public Function DesEncriptar(ByVal dato As String, Optional ByVal Fecha As Variant) As String
    Dim fechahoracrypt
    Dim diacrypt
    Dim oCrypt2 As CCrypt2
            
    If IsMissing(Fecha) Then
        fechahoracrypt = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            fechahoracrypt = DateSerial(1974, 3, 5)
        Else
            fechahoracrypt = Fecha
        End If
    End If

    diacrypt = Day(Fecha)
    
    Set oCrypt2 = New CCrypt2
    
    oCrypt2.InBuffer = dato
    If diacrypt Mod 2 = 0 Then
        oCrypt2.Codigo = ClavePar5Bytes
    Else
        oCrypt2.Codigo = ClaveImp5bytes
    End If
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    oCrypt2.Decrypt
    DesEncriptar = oCrypt2.OutBuffer
            
    Set oCrypt2 = Nothing
End Function

Public Function SQLBinaryToBoolean(ByVal var As Variant) As Variant
    
    If IsNull(var) Then
        SQLBinaryToBoolean = False
    Else
        If var = 1 Then
            SQLBinaryToBoolean = True
        Else
            SQLBinaryToBoolean = False
        End If
    End If
    

End Function
Public Function BooleanToSQLBinary(ByVal b As Boolean) As Variant
    
    If b Then
        BooleanToSQLBinary = 1
    Else
        BooleanToSQLBinary = 0
    End If
    
End Function

Public Function NullToStr(ByVal ValueThatCanBeNull As Variant) As String
  
    If IsNull(ValueThatCanBeNull) Then
        NullToStr = ""
    Else
        NullToStr = ValueThatCanBeNull
    End If

End Function
Function StrToSQLNULL(StrThatCanBeEmpty) As Variant
    
    If IsEmpty(StrThatCanBeEmpty) Then
        StrToSQLNULL = "NULL"
    Else
        If IsNull(StrThatCanBeEmpty) Then
            StrToSQLNULL = "NULL"
        Else
            If StrThatCanBeEmpty = "" Then
                StrToSQLNULL = "NULL"
            Else
                StrToSQLNULL = "'" & DblQuote(CStr(StrThatCanBeEmpty)) & "'"
            End If
        End If
    End If
    
End Function
Public Function DblToSQLFloat(DblToConvert) As String

    Dim Ind As Long
    Dim SeekPos As Long
    Dim pos As Long
    
    Dim TmpFloat As String
    Dim StrToConvert As String
    
    Dim sDecimal As String
    Dim sThousand As String
        
    If IsEmpty(DblToConvert) Then
        DblToSQLFloat = "NULL"
        Exit Function
    End If
    
    If IsNull(DblToConvert) Then
        DblToSQLFloat = "NULL"
        Exit Function
    End If
    
    If DblToConvert = "" Then
        DblToSQLFloat = "NULL"
        Exit Function
    End If
    
    StrToConvert = CStr(DblToConvert)
    
    sThousand = "."
    
    sDecimal = ","
    
    TmpFloat = ""
    pos = 1
    
    Do While Len(StrToConvert) > 0
    
        SeekPos = InStr(pos, StrToConvert, sThousand)
        
        If SeekPos > 0 Then
            
            TmpFloat = TmpFloat & Mid(StrToConvert, pos, SeekPos - 1)
            StrToConvert = Right(StrToConvert, Len(StrToConvert) - SeekPos)
            
        Else
        
            TmpFloat = TmpFloat & StrToConvert
            Exit Do
            
        End If
            
    Loop
    
    DblToSQLFloat = TmpFloat

    StrToConvert = DblToSQLFloat
    TmpFloat = ""
    pos = 1
    
    Do While Len(StrToConvert) > 0
    
        SeekPos = InStr(pos, StrToConvert, sDecimal)
        
        If SeekPos > 0 Then
            
            TmpFloat = TmpFloat & Mid(StrToConvert, pos, SeekPos - 1) & "."
            StrToConvert = Right(StrToConvert, Len(StrToConvert) - SeekPos)
            
        Else
        
            TmpFloat = TmpFloat & StrToConvert
            Exit Do
            
        End If
            
    Loop
    
    DblToSQLFloat = TmpFloat

End Function



Function devolverIP() As String
Dim Socket As Object
On Error GoTo nohayIP

    Set Socket = CreateObject("MSWinsock.Winsock")
    devolverIP = Socket.LocalIP
    Set Socket = Nothing
    Exit Function
nohayIP:
    Set Socket = Nothing
    devolverIP = ""
End Function

Public Function NumTransaccionesAbiertas(oCon As adodb.Connection) As Integer

Dim sConsulta As String
Dim adoRS As adodb.Recordset

sConsulta = "SELECT @@TRANCOUNT NUMTRAN"

    
Set adoRS = New adodb.Recordset
adoRS.Open sConsulta, oCon, adOpenForwardOnly, adLockReadOnly

NumTransaccionesAbiertas = adoRS.Fields("NUMTRAN").Value

adoRS.Close
Set adoRS = Nothing

End Function



''' <summary>
''' Devuelve un string con la ruta del servidor GS
''' </summary>
''' <param name="oConn">Conexion</param>
''' <param name="lCiaComp">Compa�ia</param>
''' <returns>String con la ruta del servidor GS</returns>
''' <remarks>Llamada desde: Gran cantidad de clases q van a acceder al servidor GS ; Tiempo m�ximo: 0</remarks>
''' Revisado por: Jbg. Fecha: 03/07/2012
Public Function FSGS(oConn As CConexion, ByVal lCiaComp As Long) As String

Dim adoRes As adodb.Recordset
Dim sConsulta As String

Dim sSRV As Variant
Dim sBD As String


'Primero debemos obtener los datos de SERVIDOR,sBD,... de la CiaCompradora
sConsulta = "SELECT FSGS_SRV,FSGS_BD " & vbCrLf _
          & "  FROM CIAS WITH (NOLOCK) " & vbCrLf _
          & " WHERE ID=" & lCiaComp
Set adoRes = New adodb.Recordset
adoRes.Open sConsulta, oConn.AdoCon, adOpenForwardOnly, adLockReadOnly

If adoRes.eof Then
    adoRes.Close
    Set adoRes = Nothing
    FSGS = ""
    Exit Function
End If

sSRV = adoRes("FSGS_SRV").Value
sBD = adoRes("FSGS_BD").Value

adoRes.Close
Set adoRes = Nothing

If IsNull(sSRV) Then
    FSGS = sBD & ".dbo."
Else
    FSGS = sSRV & "." & sBD & ".dbo."
End If




End Function

Public Function DevolverDenGMN() As String
    If g_udtParametrosGenerales.gbSincronizacion = True Then
        DevolverDenGMN = "DEN_" & g_udtParametrosGenerales.g_sIdioma
    Else
        DevolverDenGMN = "DEN_SPA"
    End If
End Function

''' <summary>
''' Funci�n que encripta un string que se le pasa como par�metro.
''' </summary>
''' <param name="Dato">Dato a encriptar.</param>
''' <param name="Fecha">Fecha para la encriptaci�n</param>
''' <returns>El string encriptado.</returns>
''' <remarks>Llamada desde: CRaiz.Encriptar ; Tiempo m�ximo: 0,1</remarks>
Public Function CompararHash(ByVal Salt As String, ByVal pwd As String, ByVal bdpwd As String) As Boolean
    Dim oCrypt2 As CCrypt2
    Set oCrypt2 = New CCrypt2
    
    CompararHash = oCrypt2.CompararHash(Salt, pwd, bdpwd)
    Set oCrypt2 = Nothing
End Function

''' <summary>
''' Lee el par�metro de ruta del FSNWebService de BD Portal y si esta vacio de BD GS
''' </summary>
''' <param name="con">ADO conexion</param>
''' <remarks>Llamada desde: CAdjunto CEspecificacion; Tiempo m�ximo: 0,0</remarks>
''' Revisado por: epb; Fecha: 31/08/2015
Public Function ObtenerRutaWebService(ByVal con As CConexion) As String
    Dim adoRes As New adodb.Recordset
    Dim sConsulta As String
    Dim sRuta As String
    
    sConsulta = "SELECT URL_WEBSERVICE FROM PARGEN_INTERNO WITH(NOLOCK) WHERE ID=1"
    adoRes.Open sConsulta, con.AdoCon, adOpenForwardOnly, adLockReadOnly
    If Not adoRes.eof Then
        sRuta = IIf(IsNull(adoRes.Fields("URL_WEBSERVICE")), "", adoRes.Fields("URL_WEBSERVICE").Value)
    End If
    If sRuta = "" Then
        sRuta = ObtenerRutaWebServiceGS(con)
    End If
    ObtenerRutaWebService = sRuta
    
    adoRes.Close
    Set adoRes = Nothing

End Function

''' <summary>
''' Lee el par�metro de ruta del FSNWebService de BD de GS
''' </summary>
''' <param name="con">ADO conexion</param>
''' <remarks>Llamada desde: CAdjunto CEspecificacion; Tiempo m�ximo: 0,0</remarks>
''' Revisado por: epb; Fecha: 31/08/2015
Private Function ObtenerRutaWebServiceGS(ByVal con As CConexion) As String
    Dim adoRes As New adodb.Recordset
    Dim sConsulta As String
    Dim sFSG As String
    
    On Error GoTo Error:
    sFSG = FSGS(con, 1)
    sConsulta = "SELECT URL_WEBSERVICE FROM " & sFSG & "PARGEN_INTERNO WITH(NOLOCK) WHERE ID=1"
    adoRes.Open sConsulta, con.AdoCon, adOpenForwardOnly, adLockReadOnly
    If Not adoRes.eof Then
        ObtenerRutaWebServiceGS = IIf(IsNull(adoRes.Fields("URL_WEBSERVICE")), "", adoRes.Fields("URL_WEBSERVICE").Value)
    Else
        GoTo Error:
    End If
    
    adoRes.Close
    Set adoRes = Nothing
    Exit Function
    
Error:
    Set adoRes = Nothing
    ObtenerRutaWebServiceGS = ""
End Function

Public Function DecodeBase64(ByVal strData As String) As Byte()

    Dim objXML As Object
    Dim objNode As Object
   
    ' help from MSXML
    Set objXML = CreateObject("MSXML2.DOMDocument")
    Set objNode = objXML.createElement("b64")
    objNode.DataType = "bin.base64"
    objNode.Text = strData
    DecodeBase64 = objNode.nodeTypedValue
   
    ' thanks, bye
    Set objNode = Nothing
    Set objXML = Nothing

End Function

Public Function EncodeBase64(ByRef arrData() As Byte) As String
    Dim objXML As Object
    Dim objNode As Object
    
    Set objXML = CreateObject("MSXML2.DOMDocument")
   
    ' byte array to base64
    Set objNode = objXML.createElement("b64")
    objNode.DataType = "bin.base64"
    objNode.nodeTypedValue = arrData
    EncodeBase64 = objNode.Text

    ' thanks, bye
    Set objNode = Nothing
    Set objXML = Nothing
End Function
