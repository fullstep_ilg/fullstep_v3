Le notificamos que ha sido rechazada la solicitud de cambio de registro.


Datos de la compa�ia:
  C�digo: @CODCIA
  Denominaci�n: @DENCIA
  NIF de la cia: @NIF
  C.P.: @CP
  Poblaci�n: @POB

  Provincia: 
    C�digo: @CODPROVI
    Denominaci�n: @DENPROVI

  Pa�s:
    C�digo: @CODPAI
    Denominaci�n: @DENPAI

  Direcci�n web de la cia: @URL


  Cias compradoras en las que est� suscrito:

@CIAS_ACTUALES

  Solicitud rechazada. Cias compradoras en las que quer�a estar suscrito:�

@CIAS_NUEVAS

Datos del usuario que realiz� la solicitud de cambio:
  C�digo: @CODUSU
  Nombre: @NOMUSU
  Apellidos: @APEUSU
  Tel�fono 1: @TFNO1USU
  Tel�fono 2: @TFNO2USU
  Tel�fono m�vil: @MOVILUSU
  Fax: @FAXUSU
  E-mail: @EMAILUSU
  Cargo: @CARGOUSU
  Departamento: @DEPUSU


Un saludo


(Si desea recibir este email en HTML acceda al portal y modifique sus preferencias en el apartado de registro de usuario)
