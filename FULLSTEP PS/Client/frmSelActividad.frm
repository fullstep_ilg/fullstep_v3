VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmSelActividad 
   BackColor       =   &H00808000&
   Caption         =   "Selecci�n de Actividades"
   ClientHeight    =   4980
   ClientLeft      =   1485
   ClientTop       =   1935
   ClientWidth     =   5235
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSelActividad.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4980
   ScaleWidth      =   5235
   Begin VB.CommandButton cmdSeleccionar 
      Caption         =   "&Seleccionar"
      Height          =   345
      Left            =   1552
      TabIndex        =   1
      Top             =   4560
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   345
      Left            =   2677
      TabIndex        =   2
      Top             =   4560
      Width           =   1005
   End
   Begin MSComctlLib.TreeView tvwEstrAct 
      Height          =   4395
      Left            =   75
      TabIndex        =   0
      Top             =   75
      Width           =   5070
      _ExtentX        =   8943
      _ExtentY        =   7752
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelActividad.frx":0CB2
            Key             =   "Raiz2"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelActividad.frx":1104
            Key             =   "ACT"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmSelActividad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oActividadesNivel1 As CActividadesNivel1
' Variables para interactuar con otros forms
Public sOrigen As String

Private sIdiAct As String

Private Sub DevolverCodDen(ByVal Node As MSComctlLib.Node, ByRef Cod As String, Den As String)
Dim sTexto() As String

If Node Is Nothing Then Exit Sub

sTexto = Split(Node.Text, " - ")
Cod = sTexto(0)
Den = sTexto(1)

    
End Sub

Private Sub cmdSeleccionar_Click()
Dim oACN1 As CActividadNivel1
Dim oACN2 As CActividadNivel2
Dim oACN3 As CActividadNivel3
Dim oACN4 As CActividadNivel4
Dim oACN5 As CActividadNivel5
Dim Node As MSComctlLib.Node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim sCod5 As String
Dim scod As String
Dim sden As String
    
    Set Node = tvwEstrAct.SelectedItem
    
    If Node Is Nothing Then Exit Sub
    
    If Node.Tag = "Raiz" Then
        
        If sOrigen = "frmCiaBuscar" Then
                
                With frmCiaBuscar
                
                    .ACN1Seleccionada = Null
                    .ACN2Seleccionada = Null
                    .ACN3Seleccionada = Null
                    .ACN4Seleccionada = Null
                    .ACN5Seleccionada = Null
                    .lblActividad = ""
                    
                End With
            
            Unload Me
            
        End If
        
        Exit Sub
        
    End If
    
    If sOrigen = "frmCiaBuscar" Then
        
        Select Case Left(Node.Tag, 4)
        
            Case "ACN1"
                
                With frmCiaBuscar
                
                    .ACN1Seleccionada = DevolverId(Node)
                    .ACN2Seleccionada = Null
                    .ACN3Seleccionada = Null
                    .ACN4Seleccionada = Null
                    .ACN5Seleccionada = Null
                    .lblActividad = ""
                    scod1 = CStr(.ACN1Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.ACN1Seleccionada)))
                    
                    Set oACN1 = m_oActividadesNivel1.Item(scod1)
                    
                    .lblActividad = oACN1.Cod & " - " & oACN1.Den
                
                End With
                
            Case "ACN2"
                
                With frmCiaBuscar
                
                    .ACN1Seleccionada = DevolverId(Node.Parent)
                    .ACN2Seleccionada = DevolverId(Node)
                    .ACN3Seleccionada = Null
                    .ACN4Seleccionada = Null
                    .ACN5Seleccionada = Null
                
                    scod1 = CStr(.ACN1Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.ACN1Seleccionada)))
                    scod2 = scod1 & CStr(.ACN2Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.ACN2Seleccionada)))
                    
                    Set oACN2 = m_oActividadesNivel1.Item(scod1).ActividadesNivel2.Item(scod2)
                    
                    .lblActividad = oACN2.Cod & " - " & oACN2.Den
                
                End With
                
                
            Case "ACN3"
                
                With frmCiaBuscar
                
                    .ACN1Seleccionada = DevolverId(Node.Parent.Parent)
                    .ACN2Seleccionada = DevolverId(Node.Parent)
                    .ACN3Seleccionada = DevolverId(Node)
                    .ACN4Seleccionada = Null
                    .ACN5Seleccionada = Null
                
                    scod1 = CStr(.ACN1Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.ACN1Seleccionada)))
                    scod2 = scod1 & CStr(.ACN2Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.ACN2Seleccionada)))
                    scod3 = scod2 & CStr(.ACN3Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.ACN3Seleccionada)))
                    
                    Set oACN3 = m_oActividadesNivel1.Item(scod1).ActividadesNivel2.Item(scod2).ActividadesNivel3.Item(scod3)
                    
                    .lblActividad = oACN3.Cod & " - " & oACN3.Den
                
                End With
                
            Case "ACN4"
            
                With frmCiaBuscar
                
                    .ACN1Seleccionada = DevolverId(Node.Parent.Parent.Parent)
                    .ACN2Seleccionada = DevolverId(Node.Parent.Parent)
                    .ACN3Seleccionada = DevolverId(Node.Parent)
                    .ACN4Seleccionada = DevolverId(Node)
                    .ACN5Seleccionada = Null
                    scod1 = CStr(.ACN1Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.ACN1Seleccionada)))
                    scod2 = scod1 & CStr(.ACN2Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.ACN2Seleccionada)))
                    scod3 = scod2 & CStr(.ACN3Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.ACN3Seleccionada)))
                    scod4 = scod3 & CStr(.ACN4Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.ACN4Seleccionada)))
                    
                    Set oACN4 = m_oActividadesNivel1.Item(scod1).ActividadesNivel2.Item(scod2).ActividadesNivel3.Item(scod3).ActividadesNivel4.Item(scod4)
                    
                    .lblActividad = oACN4.Cod & " - " & oACN4.Den
                End With
                
            Case "ACN5"
                
                With frmCiaBuscar
                
                    .ACN1Seleccionada = DevolverId(Node.Parent.Parent.Parent.Parent)
                    .ACN2Seleccionada = DevolverId(Node.Parent.Parent.Parent)
                    .ACN3Seleccionada = DevolverId(Node.Parent.Parent)
                    .ACN4Seleccionada = DevolverId(Node.Parent)
                    .ACN5Seleccionada = DevolverId(Node)
                
                    scod1 = CStr(.ACN1Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.ACN1Seleccionada)))
                    scod2 = scod1 & CStr(.ACN2Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.ACN2Seleccionada)))
                    scod3 = scod2 & CStr(.ACN3Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.ACN3Seleccionada)))
                    scod4 = scod3 & CStr(.ACN4Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.ACN4Seleccionada)))
                    sCod5 = scod4 & CStr(.ACN5Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.ACN5Seleccionada)))
                    
                    Set oACN5 = m_oActividadesNivel1.Item(scod1).ActividadesNivel2.Item(scod2).ActividadesNivel3.Item(scod3).ActividadesNivel4.Item(scod4).ActividadesNivel5.Item(sCod5)
                    
                    frmCiaBuscar.lblActividad = oACN5.Cod & " - " & oACN5.Den
                
                End With
                
                
        End Select
        
        Unload Me
    
    End If
    'prueba
    If Node.Tag = "Raiz" Then
    
        If sOrigen = "frmLstCIAS" Then
                
                    With frmLstCIAS
                
                        .g_sACN1Seleccionad = ""
                        .g_sACN2Seleccionad = ""
                        .g_sACN3Seleccionad = ""
                        .g_sACN4Seleccionad = ""
                        .g_sACN5Seleccionad = ""
                        .lblActividad = ""
                    
                    End With
            
                Unload Me
        End If
        
        Exit Sub
    
    End If
    
        
    
    If sOrigen = "frmLstCIAS" Then
        
        Select Case Left(Node.Tag, 4)
        
            Case "ACN1"
                
                With frmLstCIAS
                
                    .g_sACN1Seleccionad = DevolverId(Node)
                    .g_sACN2Seleccionad = ""
                    .g_sACN3Seleccionad = ""
                    .g_sACN4Seleccionad = ""
                    .g_sACN5Seleccionad = ""
                    .lblActividad = ""
                    scod1 = CStr(.g_sACN1Seleccionad) & Mid$("                         ", 1, 10 - Len(CStr(.g_sACN1Seleccionad)))
                    
                    Set oACN1 = m_oActividadesNivel1.Item(scod1)
                    
                    .lblActividad = oACN1.Cod & " - " & oACN1.Den
                
                End With
                
            Case "ACN2"
                
                With frmLstCIAS
                
                    .g_sACN1Seleccionad = DevolverId(Node.Parent)
                    .g_sACN2Seleccionad = DevolverId(Node)
                    .g_sACN3Seleccionad = ""
                    .g_sACN4Seleccionad = ""
                    .g_sACN5Seleccionad = ""
                
                    scod1 = CStr(.g_sACN1Seleccionad) & Mid$("                         ", 1, 10 - Len(CStr(.g_sACN1Seleccionad)))
                    scod2 = scod1 & CStr(.g_sACN2Seleccionad) & Mid$("                         ", 1, 10 - Len(CStr(.g_sACN2Seleccionad)))
                    
                    Set oACN2 = m_oActividadesNivel1.Item(scod1).ActividadesNivel2.Item(scod2)
                    
                    .lblActividad = oACN2.Cod & " - " & oACN2.Den
                
                End With
                
                
            Case "ACN3"
                
                With frmLstCIAS
                
                    .g_sACN1Seleccionad = DevolverId(Node.Parent.Parent)
                    .g_sACN2Seleccionad = DevolverId(Node.Parent)
                    .g_sACN3Seleccionad = DevolverId(Node)
                    .g_sACN4Seleccionad = ""
                    .g_sACN5Seleccionad = ""
                
                    scod1 = CStr(.g_sACN1Seleccionad) & Mid$("                         ", 1, 10 - Len(CStr(.g_sACN1Seleccionad)))
                    scod2 = scod1 & CStr(.g_sACN2Seleccionad) & Mid$("                         ", 1, 10 - Len(CStr(.g_sACN2Seleccionad)))
                    scod3 = scod2 & CStr(.g_sACN3Seleccionad) & Mid$("                         ", 1, 10 - Len(CStr(.g_sACN3Seleccionad)))
                    
                    Set oACN3 = m_oActividadesNivel1.Item(scod1).ActividadesNivel2.Item(scod2).ActividadesNivel3.Item(scod3)
                    
                    .lblActividad = oACN3.Cod & " - " & oACN3.Den
                
                End With
                
            Case "ACN4"
            
                With frmLstCIAS
                
                    .g_sACN1Seleccionad = DevolverId(Node.Parent.Parent.Parent)
                    .g_sACN2Seleccionad = DevolverId(Node.Parent.Parent)
                    .g_sACN3Seleccionad = DevolverId(Node.Parent)
                    .g_sACN4Seleccionad = DevolverId(Node)
                    .g_sACN5Seleccionad = ""
                    scod1 = CStr(.g_sACN1Seleccionad) & Mid$("                         ", 1, 10 - Len(CStr(.g_sACN1Seleccionad)))
                    scod2 = scod1 & CStr(.g_sACN2Seleccionad) & Mid$("                         ", 1, 10 - Len(CStr(.g_sACN2Seleccionad)))
                    scod3 = scod2 & CStr(.g_sACN3Seleccionad) & Mid$("                         ", 1, 10 - Len(CStr(.g_sACN3Seleccionad)))
                    scod4 = scod3 & CStr(.g_sACN4Seleccionad) & Mid$("                         ", 1, 10 - Len(CStr(.g_sACN4Seleccionad)))
                    
                    Set oACN4 = m_oActividadesNivel1.Item(scod1).ActividadesNivel2.Item(scod2).ActividadesNivel3.Item(scod3).ActividadesNivel4.Item(scod4)
                    
                    .lblActividad = oACN4.Cod & " - " & oACN4.Den
                End With
                
            Case "ACN5"
                
                With frmLstCIAS
                
                    .g_sACN1Seleccionad = DevolverId(Node.Parent.Parent.Parent.Parent)
                    .g_sACN2Seleccionad = DevolverId(Node.Parent.Parent.Parent)
                    .g_sACN3Seleccionad = DevolverId(Node.Parent.Parent)
                    .g_sACN4Seleccionad = DevolverId(Node.Parent)
                    .g_sACN5Seleccionad = DevolverId(Node)
                
                    scod1 = CStr(.g_sACN1Seleccionad) & Mid$("                         ", 1, 10 - Len(CStr(.g_sACN1Seleccionad)))
                    scod2 = scod1 & CStr(.g_sACN2Seleccionad) & Mid$("                         ", 1, 10 - Len(CStr(.g_sACN2Seleccionad)))
                    scod3 = scod2 & CStr(.g_sACN3Seleccionad) & Mid$("                         ", 1, 10 - Len(CStr(.g_sACN3Seleccionad)))
                    scod4 = scod3 & CStr(.g_sACN4Seleccionad) & Mid$("                         ", 1, 10 - Len(CStr(.g_sACN4Seleccionad)))
                    sCod5 = scod4 & CStr(.g_sACN5Seleccionad) & Mid$("                         ", 1, 10 - Len(CStr(.g_sACN5Seleccionad)))
                    
                    Set oACN5 = m_oActividadesNivel1.Item(scod1).ActividadesNivel2.Item(scod2).ActividadesNivel3.Item(scod3).ActividadesNivel4.Item(scod4).ActividadesNivel5.Item(sCod5)
                    
                    frmLstCIAS.lblActividad = oACN5.Cod & " - " & oACN5.Den
                
                End With
                
                
        End Select
        
        Unload Me
    
    End If
    
    If sOrigen = "frmLstACT" Then
        
        With frmLstACT
            Set .g_oACN1Seleccionado = Nothing
            Set .g_oACN2Seleccionado = Nothing
            Set .g_oACN3Seleccionado = Nothing
            Set .g_oACN4Seleccionado = Nothing
            Set .g_oACN5Seleccionado = Nothing
            Select Case Left(Node.Tag, 4)
                Case "ACN1"
                    Set .g_oACN1Seleccionado = g_oRaiz.Generar_CActividadNivel1
                    .g_oACN1Seleccionado.Id = DevolverId(Node)
                    DevolverCodDen Node, scod, sden
                    .g_oACN1Seleccionado.Cod = scod
                    .g_oACN1Seleccionado.Den = sden
                 Case "ACN2"
                    Set .g_oACN1Seleccionado = g_oRaiz.Generar_CActividadNivel1
                    Set .g_oACN2Seleccionado = g_oRaiz.Generar_CActividadNivel2
                    DevolverCodDen Node.Parent, scod, sden
                    .g_oACN1Seleccionado.Cod = scod
                    .g_oACN2Seleccionado.Id = DevolverId(Node)
                    .g_oACN2Seleccionado.ACN1 = DevolverId(Node.Parent)
                    DevolverCodDen Node, scod, sden
                    .g_oACN2Seleccionado.Cod = scod
                    .g_oACN2Seleccionado.Den = sden
                Case "ACN3"
                    Set .g_oACN1Seleccionado = g_oRaiz.Generar_CActividadNivel1
                    Set .g_oACN2Seleccionado = g_oRaiz.Generar_CActividadNivel2
                    Set .g_oACN3Seleccionado = g_oRaiz.Generar_CActividadNivel3
                    DevolverCodDen Node.Parent.Parent, scod, sden
                    .g_oACN1Seleccionado.Cod = scod
                    DevolverCodDen Node.Parent, scod, sden
                    .g_oACN2Seleccionado.Cod = scod
                    .g_oACN3Seleccionado.Id = DevolverId(Node)
                    .g_oACN3Seleccionado.ACN1 = DevolverId(Node.Parent.Parent)
                    .g_oACN3Seleccionado.ACN2 = DevolverId(Node.Parent)
                    DevolverCodDen Node, scod, sden
                    .g_oACN3Seleccionado.Cod = scod
                    .g_oACN3Seleccionado.Den = sden
                    Set .g_oACN4Seleccionado = Nothing
                    Set .g_oACN5Seleccionado = Nothing
                Case "ACN4"
                    Set .g_oACN1Seleccionado = g_oRaiz.Generar_CActividadNivel1
                    Set .g_oACN2Seleccionado = g_oRaiz.Generar_CActividadNivel2
                    Set .g_oACN3Seleccionado = g_oRaiz.Generar_CActividadNivel3
                    Set .g_oACN4Seleccionado = g_oRaiz.Generar_CActividadNivel4
                    DevolverCodDen Node.Parent.Parent.Parent, scod, sden
                    .g_oACN1Seleccionado.Cod = scod
                    DevolverCodDen Node.Parent.Parent, scod, sden
                    .g_oACN2Seleccionado.Cod = scod
                    DevolverCodDen Node.Parent, scod, sden
                    .g_oACN3Seleccionado.Cod = scod
                    .g_oACN4Seleccionado.Id = DevolverId(Node)
                    .g_oACN4Seleccionado.ACN1 = DevolverId(Node.Parent.Parent.Parent)
                    .g_oACN4Seleccionado.ACN2 = DevolverId(Node.Parent.Parent)
                    .g_oACN4Seleccionado.ACN3 = DevolverId(Node.Parent)
                    DevolverCodDen Node, scod, sden
                    .g_oACN4Seleccionado.Cod = scod
                    .g_oACN4Seleccionado.Den = sden
                Case "ACN5"
                    Set .g_oACN1Seleccionado = g_oRaiz.Generar_CActividadNivel1
                    Set .g_oACN2Seleccionado = g_oRaiz.Generar_CActividadNivel2
                    Set .g_oACN3Seleccionado = g_oRaiz.Generar_CActividadNivel3
                    Set .g_oACN4Seleccionado = g_oRaiz.Generar_CActividadNivel4
                    Set .g_oACN5Seleccionado = g_oRaiz.Generar_CActividadNivel5
                    DevolverCodDen Node.Parent.Parent.Parent.Parent, scod, sden
                    .g_oACN1Seleccionado.Cod = scod
                    DevolverCodDen Node.Parent.Parent.Parent, scod, sden
                    .g_oACN2Seleccionado.Cod = scod
                    DevolverCodDen Node.Parent.Parent, scod, sden
                    .g_oACN3Seleccionado.Cod = scod
                    DevolverCodDen Node.Parent, scod, sden
                    .g_oACN4Seleccionado.Cod = scod
                    .g_oACN5Seleccionado.Id = DevolverId(Node)
                    .g_oACN5Seleccionado.ACN1 = DevolverId(Node.Parent.Parent.Parent.Parent)
                    .g_oACN5Seleccionado.ACN2 = DevolverId(Node.Parent.Parent.Parent)
                    .g_oACN5Seleccionado.ACN3 = DevolverId(Node.Parent.Parent)
                    .g_oACN5Seleccionado.ACN4 = DevolverId(Node.Parent)
                    DevolverCodDen Node, scod, sden
                    .g_oACN5Seleccionado.Cod = scod
                    .g_oACN5Seleccionado.Den = sden
            End Select
        End With
    End If
    
    Unload Me
    
End Sub

Private Sub cmdCAncelar_Click()

    Unload Me

End Sub

Private Sub Form_Load()
       
    Dim sCaption As String
    
    Me.Height = 5385
    Me.Width = 5355
    CargarRecursos
    Me.Top = frmMDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = frmMDI.ScaleWidth / 2 - Me.Width / 2
        
    DoEvents
        
    GenerarEstructuraActividades False
    
End Sub

Private Sub Form_Resize()
        
    If Me.Height < 1200 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    tvwEstrAct.Height = Me.Height - 1030
    tvwEstrAct.Width = Me.Width - 360
    cmdSeleccionar.Top = Me.Height - 825
    cmdCancelar.Top = cmdSeleccionar.Top
    cmdCancelar.Left = Me.Width / 2
    cmdSeleccionar.Left = cmdCancelar.Left - cmdSeleccionar.Width - 110
    
End Sub

Public Function DevolverCod(ByVal Node As MSComctlLib.Node) As Variant

If Node Is Nothing Then Exit Function

DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)

End Function
Public Function DevolverId(ByVal Node As MSComctlLib.Node) As Variant

If Node Is Nothing Then Exit Function

DevolverId = Val(Right(Node.Tag, Len(Node.Tag) - 4))

End Function

Private Sub GenerarEstructuraActividades(ByVal bOrdenadoPorDen As Boolean)

Dim oACN1 As CActividadNivel1
Dim oACN2 As CActividadNivel2
Dim oACN3 As CActividadNivel3
Dim oACN4 As CActividadNivel4
Dim oACN5 As CActividadNivel5

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim sCod5 As String

Dim nodx As MSComctlLib.Node

tvwEstrAct.Nodes.Clear

Set nodx = tvwEstrAct.Nodes.Add(, , "Raiz", sIdiAct, "Raiz2")
nodx.Tag = "Raiz"
nodx.Expanded = True

Set m_oActividadesNivel1 = g_oRaiz.Generar_CActividadesNivel1
m_oActividadesNivel1.GenerarEstructuraActividades bOrdenadoPorDen, g_udtParametrosGenerales.g_sIdioma


If m_oActividadesNivel1 Is Nothing Then Exit Sub
        For Each oACN1 In m_oActividadesNivel1
        
        scod1 = oACN1.Cod & Mid$("                         ", 1, 10 - Len(oACN1.Cod))
            
        Set nodx = tvwEstrAct.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, oACN1.Cod & " - " & oACN1.Den, "ACT")
        nodx.Tag = "ACN1" & oACN1.Id
        
        For Each oACN2 In oACN1.ActividadesNivel2
            scod2 = oACN2.Cod & Mid$("                         ", 1, 10 - Len(oACN2.Cod))
            Set nodx = tvwEstrAct.Nodes.Add("ACN1" & scod1, tvwChild, "ACN2" & scod1 & scod2, oACN2.Cod & " - " & oACN2.Den, "ACT")
            nodx.Tag = "ACN2" & oACN2.Id
            For Each oACN3 In oACN2.ActividadesNivel3
                scod3 = oACN3.Cod & Mid$("                         ", 1, 10 - Len(oACN3.Cod))
                Set nodx = tvwEstrAct.Nodes.Add("ACN2" & scod1 & scod2, tvwChild, "ACN3" & scod1 & scod2 & scod3, oACN3.Cod & " - " & oACN3.Den, "ACT")
                nodx.Tag = "ACN3" & oACN3.Id
                For Each oACN4 In oACN3.ActividadesNivel4
                    scod4 = oACN4.Cod & Mid$("                         ", 1, 10 - Len(oACN4.Cod))
                    Set nodx = tvwEstrAct.Nodes.Add("ACN3" & scod1 & scod2 & scod3, tvwChild, "ACN4" & scod1 & scod2 & scod3 & scod4, oACN4.Cod & " - " & oACN4.Den, "ACT")
                    nodx.Tag = "ACN4" & oACN4.Id ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                    For Each oACN5 In oACN4.ActividadesNivel5
                        sCod5 = oACN5.Cod & Mid$("                         ", 1, 10 - Len(oACN5.Cod))
                        Set nodx = tvwEstrAct.Nodes.Add("ACN4" & scod1 & scod2 & scod3 & scod4, tvwChild, "ACN5" & scod1 & scod2 & scod3 & scod4 & sCod5, oACN5.Cod & " - " & oACN5.Den, "ACT")
                        nodx.Tag = "ACN5" & oACN5.Id ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                    Next
                Next
            Next
    
        Next
    Next
       
        
Exit Sub

error:
    Set nodx = Nothing
    Resume Next
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_SELACTIVIDAD, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdSeleccionar.Caption = Ador(0).Value
        Ador.MoveNext
        sIdiAct = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub





