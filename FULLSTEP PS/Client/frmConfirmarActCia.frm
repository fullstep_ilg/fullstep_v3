VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConfirmarActCia 
   Caption         =   "Solicitudes de cambio de actividad"
   ClientHeight    =   6675
   ClientLeft      =   1650
   ClientTop       =   3825
   ClientWidth     =   9480
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmConfirmarActCia.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   6675
   ScaleWidth      =   9480
   Begin VB.Frame fraLegeng 
      BackColor       =   &H8000000E&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   75
      TabIndex        =   7
      Top             =   5715
      Width           =   9315
      Begin VB.Label Label2 
         BackColor       =   &H8000000E&
         Caption         =   "Las actividades marcadas con este s�mbolo son las que actualmente est�n asignadas a la compa��a."
         Height          =   270
         Left            =   525
         TabIndex        =   8
         Top             =   75
         Width           =   8400
      End
      Begin VB.Image Image2 
         Height          =   240
         Left            =   180
         Picture         =   "frmConfirmarActCia.frx":014A
         Top             =   75
         Width           =   240
      End
   End
   Begin VB.CommandButton cmdConfirmar 
      Caption         =   "&Confirmar"
      Height          =   345
      Left            =   75
      TabIndex        =   6
      Top             =   6240
      Width           =   1005
   End
   Begin VB.CommandButton cmdDeshacer 
      Caption         =   "&Deshacer"
      Height          =   345
      Left            =   1260
      TabIndex        =   5
      Top             =   6240
      Width           =   1005
   End
   Begin VB.Frame fraCias 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   75
      TabIndex        =   1
      Top             =   105
      Width           =   9315
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaCod 
         Height          =   285
         Left            =   1350
         TabIndex        =   2
         Top             =   210
         Width           =   1740
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   7064
         Columns(2).Caption=   "Denominaci�n"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FC"
         Columns(3).Name =   "FC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FP"
         Columns(4).Name =   "FP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   3069
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaDen 
         Height          =   285
         Left            =   3525
         TabIndex        =   3
         Top             =   210
         Width           =   5250
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5980
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "C�digo"
         Columns(2).Name =   "COD"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FC"
         Columns(3).Name =   "FC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FP"
         Columns(4).Name =   "FP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   9260
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
      End
      Begin VB.Label Label7 
         Caption         =   "Compa��a:"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   825
      End
   End
   Begin MSComctlLib.TreeView tvwEstrAct 
      Height          =   4665
      Left            =   75
      TabIndex        =   0
      Top             =   900
      Width           =   9315
      _ExtentX        =   16431
      _ExtentY        =   8229
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      Checkboxes      =   -1  'True
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   465
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   14
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfirmarActCia.frx":024C
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfirmarActCia.frx":06A0
            Key             =   "ACT"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfirmarActCia.frx":07B2
            Key             =   "ACTASIG"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfirmarActCia.frx":08C4
            Key             =   "ACTELIM"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfirmarActCia.frx":0C67
            Key             =   "ACTSINCAMB"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfirmarActCia.frx":1002
            Key             =   "Raiz2"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfirmarActCia.frx":1454
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfirmarActCia.frx":17A8
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfirmarActCia.frx":1AFC
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfirmarActCia.frx":1E50
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfirmarActCia.frx":21A4
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfirmarActCia.frx":24F8
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfirmarActCia.frx":284C
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmConfirmarActCia.frx":2BA0
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmConfirmarActCia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmConfirmarActCia
' Author    : gfa
' Date      : 12/12/2017
' Purpose   :
'---------------------------------------------------------------------------------------

Option Explicit

Private m_bMouseDown As Boolean
Private m_oCiaSeleccionada As CCia
Private m_bRespetarCombo As Boolean
Private m_bValidando As Boolean
Private m_oSelNode As MSComctlLib.Node
Private m_bSesionIniciada As Boolean
Private m_oActsN1 As CActividadesNivel1

Private m_oCias As CCias

Private sIdiAct As String
Private sIdiNoDisponible As String
Private sIdiSi As String
Private sIdiNo As String

Private m_sCarpeta_Plantilla As String
Private m_IdCia_Ant As Long
Private m_sLitRegistro(0 To 1) As String
Private Sub Form_Load()
    Me.Width = 9810
    Me.Height = 7080
    CargarRecursos
    m_bValidando = False

End Sub

Private Sub Form_Resize()
    If Me.Height < 2500 Then Exit Sub
    If Me.Width < 300 Then Exit Sub
    fraCias.Width = Me.Width - 285
    tvwEstrAct.Height = Me.Height - 2415
    tvwEstrAct.Width = Me.Width - 285
    fraLegeng.Top = tvwEstrAct.Top + tvwEstrAct.Height + 150
    fraLegeng.Width = Me.Width - 285
    cmdConfirmar.Top = fraLegeng.Height + fraLegeng.Top + 140
    cmdDeshacer.Top = cmdConfirmar.Top

End Sub

Private Sub sdbcCiaCod_Change()

    If Not m_bRespetarCombo Then
        m_bValidando = False
        m_bRespetarCombo = True
        sdbcCiaDen.Text = ""
        limpiarArbol
        m_bRespetarCombo = False
    End If
  
End Sub

Private Sub sdbcCiaCod_Click()
    If Not sdbcCiaCod.DroppedDown Then
        sdbcCiaCod = ""
        sdbcCiaDen = ""
    End If

End Sub

Private Sub sdbcCiaCod_CloseUp()
    If sdbcCiaCod.Value = "-1" Then
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaCod.Text = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaCod.Columns(2).Text
    m_bRespetarCombo = True
    sdbcCiaCod.Text = sdbcCiaCod.Columns(1).Text
    m_bRespetarCombo = False
    
    
    CiaSeleccionada
    m_bValidando = True
    
End Sub
''' <summary>
''' Despliega el combo ya cargado
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcCiaCod_DropDown()
    Dim i As Integer
    Dim Ador As Ador.Recordset
    Set m_oCias = Nothing
    Set m_oCias = g_oRaiz.Generar_CCias
    sdbcCiaCod.RemoveAll


    Set Ador = m_oCias.DevolverCiasDesde(g_iCargaMaximaCombos, sdbcCiaCod.Text, , False, False, , , , , , , , , , , , , , , , , , , , , True)

    
    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaCod.AddItem Ador("ID").Value & Chr(9) & Ador("COD").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("FCEST").Value & Chr(9) & Ador("FPEST").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcCiaCod.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    sdbcCiaCod.SelStart = 0
    sdbcCiaCod.SelLength = Len(sdbcCiaCod.Text)
    sdbcCiaCod.Refresh

End Sub

Private Sub CiaSeleccionada()
    Set m_oCias = Nothing
    DoEvents
    Screen.MousePointer = vbHourglass
    If Not sdbcCiaCod.Text = "" Then
        Set m_oCias = g_oRaiz.Generar_CCias
        m_oCias.CargarTodasLasCiasDesde 1, sdbcCiaCod.Columns(0).Value, , , True
    
        Set m_oCiaSeleccionada = Nothing
        Set m_oCiaSeleccionada = m_oCias.Item(CStr(sdbcCiaCod.Columns(0).Value))
        RellenarDatosCia
        tvwEstrAct.Visible = False
        
        GenerarEstructuraActividadesConsul False
        GenerarEstructuraActividadesCiaConsul
        
        tvwEstrAct.Nodes("Raiz").Selected = True
        Screen.MousePointer = vbDefault
        
        tvwEstrAct.Visible = True
        'Registrar accion de seleccion de compa�ia
        If m_IdCia_Ant <> m_oCiaSeleccionada.ID Then
            g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Activ_Sel_Cia, g_sLitRegAccion_SPA(8) & ": " & m_oCiaSeleccionada.Cod, _
                                                g_sLitRegAccion_ENG(8) & ": " & m_oCiaSeleccionada.Cod, _
                                                g_sLitRegAccion_FRA(8) & ": " & m_oCiaSeleccionada.Cod, _
                                                g_sLitRegAccion_GER(8) & ": " & m_oCiaSeleccionada.Cod, _
                                                m_oCiaSeleccionada.ID
            m_IdCia_Ant = m_oCiaSeleccionada.ID
        End If
    End If
    Set m_oCias = Nothing
    m_bRespetarCombo = False
End Sub

Public Sub GenerarEstructuraActividadesConsul(ByVal bOrdenadoPorDen As Boolean)
    Dim oActividadesNivel1 As CActividadesNivel1
    Dim oActividadesNivel2 As FSPSServer.CActividadesNivel2
    Dim oActividadesNivel3 As FSPSServer.CActividadesNivel3
    Dim oActividadesNivel4 As FSPSServer.CActividadesNivel4
    
    Dim oACN1 As CActividadNivel1
    Dim oACN2 As CActividadNivel2
    Dim oACN3 As CActividadNivel3
    Dim oACN4 As CActividadNivel4
    Dim oACN5 As CActividadNivel5
    
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    
    Dim nodx As MSComctlLib.Node

    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
    oActividadesNivel1.GenerarEstructuraActividades False, g_udtParametrosGenerales.g_sIdioma

    tvwEstrAct.Nodes.Clear

    Set nodx = tvwEstrAct.Nodes.Add(, , "Raiz", sIdiAct, "Raiz2")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    
    If oActividadesNivel1 Is Nothing Then Exit Sub
    For Each oACN1 In oActividadesNivel1
     
        scod1 = oACN1.ID & Mid$("                         ", 1, 10 - Len(CStr(oACN1.ID)))
            
        Set nodx = tvwEstrAct.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, oACN1.Cod & " - " & oACN1.Den, "ACT")
        nodx.Tag = "ACN1" & oACN1.ID
        
        For Each oACN2 In oACN1.ActividadesNivel2
            scod2 = oACN2.ID & Mid$("                         ", 1, 10 - Len(CStr(oACN2.ID)))
            Set nodx = tvwEstrAct.Nodes.Add("ACN1" & scod1, tvwChild, "ACN2" & scod1 & scod2, oACN2.Cod & " - " & oACN2.Den, "ACT")
            nodx.Tag = "ACN2" & oACN2.ID
            For Each oACN3 In oACN2.ActividadesNivel3
                scod3 = oACN3.ID & Mid$("                         ", 1, 10 - Len(CStr(oACN3.ID)))
                Set nodx = tvwEstrAct.Nodes.Add("ACN2" & scod1 & scod2, tvwChild, "ACN3" & scod1 & scod2 & scod3, oACN3.Cod & " - " & oACN3.Den, "ACT")
                nodx.Tag = "ACN3" & oACN3.ID
                For Each oACN4 In oACN3.ActividadesNivel4
                    scod4 = oACN4.ID & Mid$("                         ", 1, 10 - Len(CStr(oACN4.ID)))
                    Set nodx = tvwEstrAct.Nodes.Add("ACN3" & scod1 & scod2 & scod3, tvwChild, "ACN4" & scod1 & scod2 & scod3 & scod4, oACN4.Cod & " - " & oACN4.Den, "ACT")
                    nodx.Tag = "ACN4" & oACN4.ID ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                    For Each oACN5 In oACN4.ActividadesNivel5
                        scod5 = oACN5.ID & Mid$("                         ", 1, 10 - Len(CStr(oACN5.ID)))
                        Set nodx = tvwEstrAct.Nodes.Add("ACN4" & scod1 & scod2 & scod3 & scod4, tvwChild, "ACN5" & scod1 & scod2 & scod3 & scod4 & scod5, oACN5.Cod & " - " & oACN5.Den, "ACT")
                        nodx.Tag = "ACN5" & oACN5.ID ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                    Next
                Next
            Next
    
        Next
    Next
        
Exit Sub

error:
    Set nodx = Nothing
    Resume Next


End Sub

Private Sub GenerarEstructuraActividadesCiaConsul()

    Dim oACN1 As CActividadNivel1
    Dim oACN2 As CActividadNivel2
    Dim oACN3 As CActividadNivel3
    Dim oACN4 As CActividadNivel4
    Dim oACN5 As CActividadNivel5
    
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    Dim image As String
    Dim Image2 As String
    Dim Image3 As String
    Dim image4 As String
    
    
    Dim nodx As MSComctlLib.Node

    If m_oCiaSeleccionada Is Nothing Then Exit Sub
    
    'Se marcan las actividades que est�n seleccionadas para la compa��a
    
    Set m_oActsN1 = m_oCiaSeleccionada.devolverEstructuraActividadesPendiente()
    If Not m_oActsN1 Is Nothing Then
        For Each oACN1 In m_oActsN1
            scod1 = CStr(oACN1.ID) & Mid$("                         ", 1, 10 - Len(CStr(oACN1.ID)))
            If oACN1.Asignada Then
                image = "ACTASIG"
            Else
                image = "ACT"
            End If
            If oACN1.Pendiente Then
                tvwEstrAct.Nodes.Item("ACN1" & scod1).Checked = True
                MarcarTodosLosHijos tvwEstrAct.Nodes.Item("ACN1" & scod1)
            End If
            tvwEstrAct.Nodes.Item("ACN1" & scod1).image = image
            If oACN1.ActividadesNivel2.Count = 0 Then
            Else
                tvwEstrAct.Nodes.Item("ACN1" & scod1).Expanded = True
                For Each oACN2 In oACN1.ActividadesNivel2
                    scod2 = CStr(oACN2.ID) & Mid$("                         ", 1, 10 - Len(CStr(oACN2.ID)))
                    If oACN2.Asignada Then
                        image = "ACTASIG"
                    Else
                        image = "ACT"
                    End If
                    If oACN2.Pendiente Then
                        tvwEstrAct.Nodes.Item("ACN2" & scod1 & scod2).Checked = True
                        MarcarTodosLosHijos tvwEstrAct.Nodes.Item("ACN2" & scod1 & scod2)
                    End If
                    tvwEstrAct.Nodes.Item("ACN2" & scod1 & scod2).image = image
                    If oACN2.ActividadesNivel3.Count = 0 Then
                    Else
                        tvwEstrAct.Nodes.Item("ACN1" & scod1).Expanded = True
                        tvwEstrAct.Nodes.Item("ACN2" & scod1 & scod2).Expanded = True
                        For Each oACN3 In oACN2.ActividadesNivel3
                            scod3 = CStr(oACN3.ID) & Mid$("                         ", 1, 10 - Len(CStr(oACN3.ID)))
                            If oACN3.Asignada Then
                                image = "ACTASIG"
                            Else
                                image = "ACT"
                            End If
                            If oACN3.Pendiente Then
                                tvwEstrAct.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Checked = True
                                MarcarTodosLosHijos tvwEstrAct.Nodes.Item("ACN3" & scod1 & scod2 & scod3)
                            End If
                            tvwEstrAct.Nodes.Item("ACN3" & scod1 & scod2 & scod3).image = image
                            If oACN3.ActividadesNivel4.Count = 0 Then
                            Else
                                tvwEstrAct.Nodes.Item("ACN1" & scod1).Expanded = True
                                tvwEstrAct.Nodes.Item("ACN2" & scod1 & scod2).Expanded = True
                                tvwEstrAct.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Expanded = True
                                For Each oACN4 In oACN3.ActividadesNivel4
                                    scod4 = CStr(oACN4.ID) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.ID)))
                                    If oACN4.Asignada Then
                                        image = "ACTASIG"
                                    Else
                                        image = "ACT"
                                    End If
                                    If oACN4.Pendiente Then
                                        tvwEstrAct.Nodes.Item("ACN4" & scod1 & scod2 & scod3 & scod4).Checked = True
                                        MarcarTodosLosHijos tvwEstrAct.Nodes.Item("ACN4" & scod1 & scod2 & scod3 & scod4)
                                    End If
                                    tvwEstrAct.Nodes.Item("ACN4" & scod1 & scod2 & scod3 & scod4).image = image
                                    If oACN4.ActividadesNivel5.Count = 0 Then
                                    Else
                                        tvwEstrAct.Nodes.Item("ACN1" & scod1).Expanded = True
                                        tvwEstrAct.Nodes.Item("ACN2" & scod1 & scod2).Expanded = True
                                        tvwEstrAct.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Expanded = True
                                        tvwEstrAct.Nodes.Item("ACN4" & scod1 & scod2 & scod3 & scod4).Expanded = True
                                        For Each oACN5 In oACN4.ActividadesNivel5
                                            scod5 = CStr(oACN5.ID) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.ID)))
                                            If oACN5.Asignada Then
                                                image = "ACTASIG"
                                            Else
                                                image = "ACT"
                                            End If
                                            If oACN5.Pendiente Then
                                                tvwEstrAct.Nodes.Item("ACN5" & scod1 & scod2 & scod3 & scod4 & scod5).Checked = True
                                            End If
                                
                                            tvwEstrAct.Nodes.Item("ACN5" & scod1 & scod2 & scod3 & scod4 & scod5).image = image
                                        Next
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        Next
    End If
Exit Sub

error:
    Set nodx = Nothing
    Resume Next
End Sub

Private Sub limpiarArbol()
    
    Set m_oCiaSeleccionada = Nothing
    tvwEstrAct.Nodes.Clear

End Sub


Private Sub sdbcCiaCod_Validate(Cancel As Boolean)
    Dim Ador As Ador.Recordset

If m_bValidando Then Exit Sub
m_bValidando = True

    Set m_oCias = Nothing
    Set m_oCias = g_oRaiz.Generar_CCias

    If Trim(sdbcCiaCod.Text) = "" Then Exit Sub
    
    Set Ador = m_oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , True, , , , , , , , , , , , , , , , , , , , , , True)
    
    If Ador Is Nothing Then
        
        sdbcCiaCod = ""
        Exit Sub
        
    Else
        If Ador.EOF Then
            sdbcCiaCod.Value = ""
            Exit Sub
        Else
            If UCase(sdbcCiaCod.Text) <> UCase(Ador("COD").Value) Then
                sdbcCiaCod.Value = ""
                Exit Sub
            Else
                m_bRespetarCombo = True
                sdbcCiaDen.Text = Ador("DEN").Value
                sdbcCiaCod.Text = Ador("COD").Value
                sdbcCiaCod.Columns(0).Value = Ador("ID").Value
                CiaSeleccionada
                m_bRespetarCombo = False
            End If
        End If
        
        Ador.Close
        Set Ador = Nothing
        
    End If


End Sub

Private Sub sdbcCiaDen_Change()
   
If Not m_bRespetarCombo Then
        m_bValidando = False
        
        m_bRespetarCombo = True
        sdbcCiaCod.Text = ""
        limpiarArbol
        m_bRespetarCombo = False
End If

End Sub

Private Sub sdbcCiaDen_Click()
    If Not sdbcCiaDen.DroppedDown Then
        sdbcCiaDen = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaDen_CloseUp()
    If sdbcCiaDen.Columns(0).Value = "-1" Then
        sdbcCiaDen.Text = ""
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaDen.Value = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaDen.Columns(1).Text
    sdbcCiaCod.Text = sdbcCiaDen.Columns(2).Text
    sdbcCiaCod.Columns("ID").Value = sdbcCiaDen.Columns("ID").Value
    m_bRespetarCombo = False
    
    
    CiaSeleccionada
    m_bValidando = True

End Sub
''' <summary>
''' Despliega el combo ya cargado
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcCiaDen_DropDown()

    Dim i As Integer
    Dim Ador As Ador.Recordset
    Dim m_oCias As CCias
    Set m_oCias = g_oRaiz.Generar_CCias
    
    sdbcCiaDen.RemoveAll
    sdbcCiaDen.ReBind
    
    Set Ador = m_oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , sdbcCiaDen.Text, False, True, , , , , , , , , , , , , , , , , , , , , True)
    
    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaDen.AddItem Ador("ID").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("COD").Value & Chr(9) & Ador("FCEST").Value & Chr(9) & Ador("FPEST").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcCiaDen.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    sdbcCiaDen.SelStart = 0
    sdbcCiaDen.SelLength = Len(sdbcCiaDen.Text)
    sdbcCiaDen.Refresh
    

End Sub

Private Sub sdbcCiaDen_Validate(Cancel As Boolean)
Dim Ador As Ador.Recordset
If m_bValidando Then Exit Sub
m_bValidando = True

    If Trim(sdbcCiaDen.Text) = "" Then Exit Sub
    Set m_oCias = Nothing
    Set m_oCias = g_oRaiz.Generar_CCias
    
    Set Ador = m_oCias.DevolverCiasDesde(1, , sdbcCiaDen.Text, True, , , , , , , , , , , , , , , , , , , , , , True)
    
    If Ador Is Nothing Then
            
        sdbcCiaCod = ""
        Exit Sub
        
    Else
        If Ador.EOF Then
            sdbcCiaCod.Value = ""
            Exit Sub
        Else
            If UCase(sdbcCiaDen.Text) <> UCase(Ador("DEN").Value) Then
                sdbcCiaCod.Value = ""
                Exit Sub
            Else
                m_bRespetarCombo = True
                sdbcCiaDen.Text = Ador("DEN").Value
                sdbcCiaCod.Text = Ador("COD").Value
                sdbcCiaCod.Columns(0).Value = Ador("ID").Value
                CiaSeleccionada
                m_bRespetarCombo = False
            End If
        End If
        
        Ador.Close
        Set Ador = Nothing
        
    End If
    


End Sub


Private Sub tvwEstrAct_KeyUp(KeyCode As Integer, Shift As Integer)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
      
    If m_bMouseDown Then
        
        m_bMouseDown = False
        
        If m_oSelNode.Tag = "Raiz" Then
            m_oSelNode.Checked = False
            DoEvents
            Exit Sub
        End If
        
    If Mid$(m_oSelNode.Tag, 4, 1) < g_udtParametrosGenerales.g_iNivelMinAct And m_oSelNode.Checked = True Then
        m_oSelNode.Checked = False
        basMensajes.NivelActividades
        Exit Sub
    End If
            
        If m_oSelNode.Checked Then
            MarcarTodosLosHijos m_oSelNode
                           
        Else
            QuitarMarcaTodosSusPadres m_oSelNode
            QuitarMarcaTodosLosHijos m_oSelNode
                        
        End If
    
    End If

End Sub



Private Sub MarcarTodosLosHijos(ByVal nodx As MSComctlLib.Node)

Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node
Dim nod5 As MSComctlLib.Node

Select Case Left(nodx.Tag, 4)

    Case "ACN1"
                           
        Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            
            nod1.Checked = True
            Set nod2 = nod1.Child
            
            While Not nod2 Is Nothing
                nod2.Checked = True
                Set nod3 = nod2.Child
                
                While Not nod3 Is Nothing
                    nod3.Checked = True
                    Set nod4 = nod3.Child
                    
                    While Not nod4 Is Nothing
                        nod4.Checked = True
                        Set nod5 = nod4.Child
                        
                        While Not nod5 Is Nothing
                            nod5.Checked = True
                            Set nod5 = nod5.Next
                        Wend
                        
                        Set nod4 = nod4.Next
                        
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
            Set nod1 = nod1.Next
         Wend
            
    DoEvents
    Case "ACN2"
        
        Set nod2 = nodx.Child
        
        While Not nod2 Is Nothing
            nod2.Checked = True
            Set nod3 = nod2.Child
            While Not nod3 Is Nothing
                nod3.Checked = True
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod5 = nod4.Child
                    While Not nod5 Is Nothing
                        nod5.Checked = True
                        Set nod5 = nod5.Next
                    Wend
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            Set nod2 = nod2.Next
        Wend
    
    Case "ACN3"
            
            Set nod3 = nodx.Child
            While Not nod3 Is Nothing
                nod3.Checked = True
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod5 = nod4.Child
                    While Not nod5 Is Nothing
                        nod5.Checked = True
                        Set nod5 = nod5.Next
                    Wend
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            
            
    Case "ACN4"
            
                Set nod4 = nodx.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod5 = nod4.Child
                    While Not nod5 Is Nothing
                        nod5.Checked = True
                        Set nod5 = nod5.Next
                    Wend
                    Set nod4 = nod4.Next
                Wend
                
    Case "ACN5"
            
                Set nod5 = nodx.Child
                While Not nod5 Is Nothing
                    nod5.Checked = True
                    Set nod5 = nod5.Next
                Wend
            
                
End Select

End Sub


Private Sub QuitarMarcaTodosSusPadres(ByVal nodx As MSComctlLib.Node)
    
    Set nodx = nodx.Parent
    While Not nodx Is Nothing
        nodx.Checked = False
        Set nodx = nodx.Parent
    Wend
    DoEvents
End Sub

Private Sub QuitarMarcaTodosLosHijos(ByVal nodx As MSComctlLib.Node)

Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node
Dim nod5 As MSComctlLib.Node

Select Case Left(nodx.Tag, 4)

    Case "ACN1"
                           
        Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            
            nod1.Checked = False
            Set nod2 = nod1.Child
            
            While Not nod2 Is Nothing
                nod2.Checked = False
                Set nod3 = nod2.Child
                
                While Not nod3 Is Nothing
                    nod3.Checked = False
                    Set nod4 = nod3.Child
                    
                    While Not nod4 Is Nothing
                        nod4.Checked = False
                        Set nod5 = nod4.Child
                        
                        While Not nod5 Is Nothing
                            nod5.Checked = False
                            Set nod5 = nod5.Next
                        Wend
                        
                        Set nod4 = nod4.Next
                        
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
            Set nod1 = nod1.Next
         Wend
            
    DoEvents
    Case "ACN2"
        
        Set nod2 = nodx.Child
        
        While Not nod2 Is Nothing
            nod2.Checked = False
            Set nod3 = nod2.Child
            While Not nod3 Is Nothing
                nod3.Checked = False
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod5 = nod4.Child
                    While Not nod5 Is Nothing
                        nod5.Checked = False
                        Set nod5 = nod5.Next
                    Wend
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            Set nod2 = nod2.Next
        Wend
    
    Case "ACN3"
            
            Set nod3 = nodx.Child
            While Not nod3 Is Nothing
                nod3.Checked = False
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod5 = nod4.Child
                    While Not nod5 Is Nothing
                        nod5.Checked = False
                        Set nod5 = nod5.Next
                    Wend
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            
            
    Case "ACN4"
            
                Set nod4 = nodx.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod5 = nod4.Child
                    While Not nod5 Is Nothing
                        nod5.Checked = False
                        Set nod5 = nod5.Next
                    Wend
                    Set nod4 = nod4.Next
                Wend
                
    Case "ACN5"
            
                Set nod5 = nodx.Child
                While Not nod5 Is Nothing
                    nod5.Checked = False
                    Set nod5 = nod5.Next
                Wend
            
                
End Select

End Sub

Private Sub tvwEstrAct_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nod As MSComctlLib.Node
Dim tvhti As TVHITTESTINFO

                  
    If (Button = vbLeftButton) Then
        'Determinar si se ha pulsado sobre el nodo
        Set nod = tvwEstrAct.HitTest(X, Y)
        If Not nod Is Nothing Then
            'Determinar si se ha pulsado sobre la checkbox
            
            tvhti.pt.X = X / Screen.TwipsPerPixelX
            tvhti.pt.Y = Y / Screen.TwipsPerPixelY
            If (SendMessage(tvwEstrAct.hWnd, TVM_HITTEST, 0, tvhti)) Then
            
                If (tvhti.Flags And TVHT_ONITEMSTATEICON) Then
                 'El estado del nodo cambiar� al finalizar este procedimiento
                             
                m_bMouseDown = True
                Set m_oSelNode = nod
                             
                End If
            End If
        End If
    End If
    
End Sub

Private Sub tvwEstrAct_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
      
If m_bMouseDown Then
    
    m_bMouseDown = False
    
    If m_oSelNode.Tag = "Raiz" Then
        m_oSelNode.Checked = False
        DoEvents
        Exit Sub
    End If
    
    If Mid$(m_oSelNode.Tag, 4, 1) < g_udtParametrosGenerales.g_iNivelMinAct And m_oSelNode.Checked = True Then
        m_oSelNode.Checked = False
        basMensajes.NivelActividades
        Exit Sub
    End If
                    
    If m_oSelNode.Checked Then
        MarcarTodosLosHijos m_oSelNode
                       
    Else
        QuitarMarcaTodosSusPadres m_oSelNode
        QuitarMarcaTodosLosHijos m_oSelNode
                    
    End If
End If


End Sub

Private Sub tvwEstrAct_NodeCheck(ByVal Node As MSComctlLib.Node)
    If m_bMouseDown Then
        Exit Sub
    End If
    
    m_bMouseDown = True
    Set m_oSelNode = Node

End Sub
Private Function RegistrarCambiosSolicitud()


Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node
Dim nod5 As MSComctlLib.Node
Dim nodx As MSComctlLib.Node
Dim teserror As CTESError
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String
Dim iTipo As Integer
Dim sProviId As String


'If Not Validacion Then Exit Sub


Dim oActsN1Seleccionados As CActividadesNivel1
Dim oActsN2Seleccionados As CActividadesNivel2
Dim oActsN3Seleccionados As CActividadesNivel3
Dim oActsN4Seleccionados As CActividadesNivel4
Dim oActsN5Seleccionados As CActividadesNivel5

Set oActsN1Seleccionados = g_oRaiz.generar_CActividadesNivel1
Set oActsN2Seleccionados = g_oRaiz.Generar_CActividadesNivel2
Set oActsN3Seleccionados = g_oRaiz.Generar_CActividadesNivel3
Set oActsN4Seleccionados = g_oRaiz.Generar_CActividadesNivel4
Set oActsN5Seleccionados = g_oRaiz.Generar_CActividadesNivel5

'Screen.MousePointer = vbHourglass

If tvwEstrAct.Nodes.Count > 0 Then

    Set nodx = tvwEstrAct.Nodes(1)
       If nodx.Children = 0 Then Exit Function
        
       Set nod1 = nodx.Child
       
            While Not nod1 Is Nothing
                scod1 = DevolverId(nod1) & Mid$("                         ", 1, 10 - Len(DevolverId(nod1)))
                ' No asignado
                If Not nod1.Checked Then
                    
                    Set nod2 = nod1.Child
                    
                    While Not nod2 Is Nothing
                        scod2 = DevolverId(nod2) & Mid$("                         ", 1, 10 - Len(DevolverId(nod2)))
                        ' No asignado
                        If Not nod2.Checked Then
                
                            Set nod3 = nod2.Child
                            While Not nod3 Is Nothing
                                scod3 = DevolverId(nod3) & Mid$("                         ", 1, 10 - Len(DevolverId(nod3)))
                                If Not nod3.Checked Then
                                    
                                    Set nod4 = nod3.Child
                                    
                                    While Not nod4 Is Nothing
                                        
                                        scod4 = DevolverId(nod4) & Mid$("                         ", 1, 10 - Len(DevolverId(nod4)))
                                                    
                                        If Not nod4.Checked Then
                                            Set nod5 = nod4.Child
                                            
                                            While Not nod5 Is Nothing
                                                scod5 = DevolverId(nod5) & Mid$("                         ", 1, 10 - Len(DevolverId(nod5)))
                                                    
                                                If nod5.Checked Then
                                                    oActsN5Seleccionados.Add DevolverId(nod5.Parent.Parent.Parent.Parent), DevolverId(nod5.Parent.Parent.Parent), DevolverId(nod5.Parent.Parent), DevolverId(nod5.Parent), DevolverId(nod5), "", ""
                                                End If
                                            
                                                Set nod5 = nod5.Next
                                            Wend
                                            
                                            
                                        Else
                                            oActsN4Seleccionados.Add DevolverId(nod4.Parent.Parent.Parent), DevolverId(nod4.Parent.Parent), DevolverId(nod4.Parent), DevolverId(nod4), "", ""
                                        End If
                                        
                                        Set nod4 = nod4.Next
                                    Wend
                                Else
                                    oActsN3Seleccionados.Add DevolverId(nod3.Parent.Parent), DevolverId(nod3.Parent), DevolverId(nod3), "", ""
                                End If
                                
                                Set nod3 = nod3.Next
                            Wend
                        Else
                            oActsN2Seleccionados.Add DevolverId(nod2.Parent), DevolverId(nod2), "", ""
                        End If
                        
                        Set nod2 = nod2.Next
                    Wend
                    
                Else
                    oActsN1Seleccionados.Add DevolverId(nod1), "", ""
                End If
                Set nod1 = nod1.Next
            Wend
            Set m_oCiaSeleccionada.oActividadesNivel1 = Nothing
            Set m_oCiaSeleccionada.oActividadesNivel2 = Nothing
            Set m_oCiaSeleccionada.oActividadesNivel3 = Nothing
            Set m_oCiaSeleccionada.oActividadesNivel4 = Nothing
            Set m_oCiaSeleccionada.oActividadesNivel5 = Nothing
            Set m_oCiaSeleccionada.oActividadesNivel1 = oActsN1Seleccionados
            Set m_oCiaSeleccionada.oActividadesNivel2 = oActsN2Seleccionados
            Set m_oCiaSeleccionada.oActividadesNivel3 = oActsN3Seleccionados
            Set m_oCiaSeleccionada.oActividadesNivel4 = oActsN4Seleccionados
            Set m_oCiaSeleccionada.oActividadesNivel5 = oActsN5Seleccionados
            Set teserror = m_oCiaSeleccionada.RegistrarNuevaSolicitud()
    
            If teserror.NumError <> TESnoerror Then
              basErrores.TratarError teserror
              Screen.MousePointer = vbNormal
              RegistrarCambiosSolicitud = False
              Exit Function
            End If
  
  End If
  
  
    Set oActsN1Seleccionados = Nothing
    Set oActsN2Seleccionados = Nothing
    Set oActsN3Seleccionados = Nothing
    Set oActsN4Seleccionados = Nothing
    Set oActsN5Seleccionados = Nothing
    
    RegistrarCambiosSolicitud = True
    

End Function

Private Function AceptarActiv()
Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node
Dim nod5 As MSComctlLib.Node
Dim nodx As MSComctlLib.Node
Dim teserror As CTESError
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String
Dim iTipo As Integer
Dim sProviId As String


'If Not Validacion Then Exit Sub


Dim oActsN1Seleccionados As CActividadesNivel1
Dim oActsN2Seleccionados As CActividadesNivel2
Dim oActsN3Seleccionados As CActividadesNivel3
Dim oActsN4Seleccionados As CActividadesNivel4
Dim oActsN5Seleccionados As CActividadesNivel5

Set oActsN1Seleccionados = g_oRaiz.generar_CActividadesNivel1
Set oActsN2Seleccionados = g_oRaiz.Generar_CActividadesNivel2
Set oActsN3Seleccionados = g_oRaiz.Generar_CActividadesNivel3
Set oActsN4Seleccionados = g_oRaiz.Generar_CActividadesNivel4
Set oActsN5Seleccionados = g_oRaiz.Generar_CActividadesNivel5

'Screen.MousePointer = vbHourglass

If tvwEstrAct.Nodes.Count > 0 Then

    Set nodx = tvwEstrAct.Nodes(1)
       If nodx.Children = 0 Then Exit Function
        
       Set nod1 = nodx.Child
       
            While Not nod1 Is Nothing
                scod1 = DevolverId(nod1) & Mid$("                         ", 1, 10 - Len(DevolverId(nod1)))
                ' No asignado
                If Not nod1.Checked Then
                    
                    Set nod2 = nod1.Child
                    
                    While Not nod2 Is Nothing
                        scod2 = DevolverId(nod2) & Mid$("                         ", 1, 10 - Len(DevolverId(nod2)))
                        ' No asignado
                        If Not nod2.Checked Then
                
                            Set nod3 = nod2.Child
                            While Not nod3 Is Nothing
                                scod3 = DevolverId(nod3) & Mid$("                         ", 1, 10 - Len(DevolverId(nod3)))
                                If Not nod3.Checked Then
                                    
                                    Set nod4 = nod3.Child
                                    
                                    While Not nod4 Is Nothing
                                        
                                        scod4 = DevolverId(nod4) & Mid$("                         ", 1, 10 - Len(DevolverId(nod4)))
                                                    
                                        If Not nod4.Checked Then
                                            Set nod5 = nod4.Child
                                            
                                            While Not nod5 Is Nothing
                                                scod5 = DevolverId(nod5) & Mid$("                         ", 1, 10 - Len(DevolverId(nod5)))
                                                    
                                                If nod5.Checked Then
                                                    oActsN5Seleccionados.Add DevolverId(nod5.Parent.Parent.Parent.Parent), DevolverId(nod5.Parent.Parent.Parent), DevolverId(nod5.Parent.Parent), DevolverId(nod5.Parent), DevolverId(nod5), "", ""
                                                End If
                                            
                                                Set nod5 = nod5.Next
                                            Wend
                                            
                                            
                                        Else
                                            oActsN4Seleccionados.Add DevolverId(nod4.Parent.Parent.Parent), DevolverId(nod4.Parent.Parent), DevolverId(nod4.Parent), DevolverId(nod4), "", ""
                                        End If
                                        
                                        Set nod4 = nod4.Next
                                    Wend
                                Else
                                    oActsN3Seleccionados.Add DevolverId(nod3.Parent.Parent), DevolverId(nod3.Parent), DevolverId(nod3), "", ""
                                End If
                                
                                Set nod3 = nod3.Next
                            Wend
                        Else
                            oActsN2Seleccionados.Add DevolverId(nod2.Parent), DevolverId(nod2), "", ""
                        End If
                        
                        Set nod2 = nod2.Next
                    Wend
                    
                Else
                    oActsN1Seleccionados.Add DevolverId(nod1), "", ""
                End If
                Set nod1 = nod1.Next
            Wend
            Set m_oCiaSeleccionada.oActividadesNivel1 = Nothing
            Set m_oCiaSeleccionada.oActividadesNivel2 = Nothing
            Set m_oCiaSeleccionada.oActividadesNivel3 = Nothing
            Set m_oCiaSeleccionada.oActividadesNivel4 = Nothing
            Set m_oCiaSeleccionada.oActividadesNivel5 = Nothing
            Set m_oCiaSeleccionada.oActividadesNivel1 = oActsN1Seleccionados
            Set m_oCiaSeleccionada.oActividadesNivel2 = oActsN2Seleccionados
            Set m_oCiaSeleccionada.oActividadesNivel3 = oActsN3Seleccionados
            Set m_oCiaSeleccionada.oActividadesNivel4 = oActsN4Seleccionados
            Set m_oCiaSeleccionada.oActividadesNivel5 = oActsN5Seleccionados
            Set teserror = m_oCiaSeleccionada.confirmarCambios()
    
            If teserror.NumError <> TESnoerror Then
              basErrores.TratarError teserror
              Screen.MousePointer = vbNormal
              AceptarActiv = False
              Exit Function
            End If
  
  End If
  
  
    Set oActsN1Seleccionados = Nothing
    Set oActsN2Seleccionados = Nothing
    Set oActsN3Seleccionados = Nothing
    Set oActsN4Seleccionados = Nothing
    Set oActsN5Seleccionados = Nothing
    
    AceptarActiv = True
    
End Function

Private Function DevolverId(ByVal Node As MSComctlLib.Node) As String
    
    DevolverId = Right(Node.Tag, Len(Node.Tag) - 4)

End Function

Private Function Disponible(ByVal param As Variant) As Boolean
'**********************************************************************
'*** Descripci�n: Determina cu�ndo un valor est� disponible.        ***
'***                                                                ***
'*** Par�metros:  param ::>> argumento de entrada sobre el cu�l     ***
'***                         se har�n las comprobaciones sobre su   ***
'***                         disponibilidad.                        ***
'***                                                                ***
'*** Valor que devuelve: True  ::>> Si el valor pasado como         ***
'***                                par�metro no es nulo y ha sido  ***
'***                                inicializado, luego est�        ***
'***                                disponible.                     ***
'***                     False ::>> En caso contrario, el par�metro ***
'***                                no est� disponible.             ***
'***                                                                ***
'**********************************************************************
    If ((Not IsEmpty(param)) And (Not IsNull(param)) And (param <> "")) Then
        Disponible = True
    Else
        Disponible = False
    End If
End Function

Private Sub RellenarDatosCia()
'******************************************************************
'*** Descripci�n: Rellena los datos de la compa��a seleccionada.***
'***                                                            ***
'*** Par�metros:         -------------                          ***
'***                                                            ***
'*** Valor que devuelve: -------------                          ***
'******************************************************************
    Dim adores As Ador.Recordset
    
    If m_oCiaSeleccionada Is Nothing Then Exit Sub
    Set adores = m_oCias.DevolverDatosCia(sdbcCiaCod.Columns(0).Value)
    If ((Not adores Is Nothing) And (Not adores.EOF)) Then
        m_oCiaSeleccionada.codpai = adores("PAIID")
        m_oCiaSeleccionada.codprovi = adores("PROVIID")
        m_oCiaSeleccionada.idioma = adores("IDIID")
        m_oCiaSeleccionada.Cod = adores("COD")
        m_oCiaSeleccionada.Den = adores("DEN")
        m_oCiaSeleccionada.CodPos = adores("CP")
        m_oCiaSeleccionada.Coment = adores("COMENT")
        m_oCiaSeleccionada.Dir = adores("DIR")
        m_oCiaSeleccionada.NIF = adores("NIF")
        m_oCiaSeleccionada.Poblacion = adores("POB")
        m_oCiaSeleccionada.URLCia = adores("URLCIA")
        m_oCiaSeleccionada.VolFac = adores("VOLFACT")
        m_oCiaSeleccionada.CliRef1 = adores("CLIREF1")
        m_oCiaSeleccionada.CliRef2 = adores("CLIREF2")
        m_oCiaSeleccionada.CliRef3 = adores("CLIREF3")
        m_oCiaSeleccionada.CliRef4 = adores("CLIREF4")
        m_oCiaSeleccionada.Hom1 = adores("HOM1")
        m_oCiaSeleccionada.Hom1 = adores("HOM2")
        m_oCiaSeleccionada.Hom1 = adores("HOM3")
        m_oCiaSeleccionada.EstadoCompradora = adores("FCEST")
        m_oCiaSeleccionada.EstadoProveedora = adores("FPEST")
    End If
    adores.Close
    Set adores = Nothing
End Sub

Private Sub RellenarDatosUsuario(ByRef poUsuarioSeleccionado As CUsuario)
'******************************************************************
'*** Descripci�n: Rellena los datos del usuario seleccionado.   ***
'***                                                            ***
'*** Par�metros:  ::>> poUsuarioSeleccionado: el usuario cuyos  ***
'***                   datos ser�n introducidos. Param. por ref.***
'***                                                            ***
'*** Valor que devuelve: -------------                          ***
'******************************************************************
    Dim adores As Ador.Recordset
    
    Set adores = m_oCiaSeleccionada.Usuarios.DevolverDatosUsuario(m_oCiaSeleccionada.ID, poUsuarioSeleccionado.ID)
    If Not adores Is Nothing Then
        With poUsuarioSeleccionado
            .Cargo = NullToStr(adores("CAR").Value)
            .Departamento = NullToStr(adores("DEP").Value)
            .Email = NullToStr(adores("EMAIL").Value)
            .Fax = NullToStr(adores("FAX").Value)
            .Tfno1 = NullToStr(adores("TFNO").Value)
            .Tfno2 = NullToStr(adores("TFNO2").Value)
            .TfnoMovil = NullToStr(adores("TFNO_MOVIL").Value)
            If Not IsNull(adores("IDICOD").Value) Then
                .idioma = adores("IDIID").Value
                .Idiomaden = adores("IDIDEN").Value
            End If
        End With
    End If
    adores.Close
    Set adores = Nothing
End Sub

Private Function AlgunaActividad(ByVal psCadena As String, ByVal pEsHTML As Boolean) As Boolean
'**********************************************************************
'*** Descripci�n: Determina cu�ndo se ha agregado alguna actividad  ***
'***              a una lista.                                      ***
'***                                                                ***
'*** Par�metros:  psCadena ::>> argumento de entrada que contiene   ***
'***                            una lista en HTML o texto plano,    ***
'***                            que puede estar vac�a o no.         ***
'***                            Eso es lo que la funci�n detecta.   ***
'***                                                                ***
'***              pEsHTML  ::>> par�metro de tipo boolean que       ***
'***                            indica si la lista est� en formato  ***
'***                            HTML o en texto plano.              ***
'***                                                                ***
'*** Valor que devuelve: True  ::>> Si el valor pasado como         ***
'***                                par�metro contiene una lista    ***
'***                                con al menos una actividad.     ***
'***                                                                ***
'***                     False ::>> En caso contrario, el par�metro ***
'***                                contiene una lista HTML vac�a,  ***
'***                                sin ninguna actividad.          ***
'***                                                                ***
'**********************************************************************
    Dim vResultado As Variant
    
    Select Case pEsHTML
        Case True
            vResultado = InStr(1, UCase(psCadena), "<LI>", vbTextCompare)
            If ((vResultado > 0) And (Not IsNull(vResultado))) Then
                AlgunaActividad = True
            Else
                AlgunaActividad = False
            End If
        Case False
            vResultado = InStr(1, UCase(psCadena), "#", vbTextCompare)
            If ((vResultado > 0) And (Not IsNull(vResultado))) Then
                AlgunaActividad = True
            Else
                AlgunaActividad = False
            End If
    End Select
End Function

Private Function GenerarMensajeRechazoCambActHTML(poUsuario As CUsuario) As String
'**************************************************************************
'*** Descripci�n: Genera el cuerpo del e-mail en HTML a partir          ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSPSNotifRechazoCambAct.htm o                     ***
'***              ENG_FSPSNotifRechazoCambAct.htm dependi�ndo del       ***
'***              idioma.                                               ***
'***                                                                    ***
'*** Par�metros:  poUsuario, un objeto de tipo CUsuario del que         ***
'***              obtendremos datos relativos al usuario.               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
    Dim sConsulta As String
    Dim adores As Ador.Recordset
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim oUsuarios As CUsuarios
    Dim oCias As CCias
    Dim sCuerpoMensaje As String
    Dim sEstado As String
    Dim sFileName As String
    Dim sActividades As String
    Dim bFin As Boolean
    Dim bAlgunaActividad As Boolean
    Dim Ador As Ador.Recordset
    Dim sIdioma As String
    Dim oActividadesNivel1 As CActividadesNivel1
        
    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
    
    Select Case poUsuario.idioma
        Case 0
            sFileName = "ENG_"
            sIdioma = "ENG"
        Case 1
            sFileName = "SPA_"
            sIdioma = "SPA"
        Case 2
            sFileName = "GER_"
            sIdioma = "GER"
    End Select
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFIRMARACTCIA, sIdioma, 10)
    If Not Ador Is Nothing Then
        sIdiNoDisponible = Ador(0).Value
    End If
    Ador.Close
    Set Ador = Nothing
    
    sFileName = sFileName & "FSPSNotifRechazoCambAct.htm"

    Set oFos = New FileSystemObject
    
    If Not oFos.FileExists(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName) Then
        basMensajes.RutaPlantillasNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeRechazoCambActHTML = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName)
    
    
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll
    If oActividadesNivel1.EsPYME Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", Right(m_oCiaSeleccionada.Cod, Len(m_oCiaSeleccionada.Cod) - LEN_PYME))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", m_oCiaSeleccionada.Cod)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENCIA", m_oCiaSeleccionada.Den)
    If (Disponible(m_oCiaSeleccionada.NIF)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", m_oCiaSeleccionada.NIF)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.CodPos)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", m_oCiaSeleccionada.CodPos)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.Poblacion)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", m_oCiaSeleccionada.Poblacion)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.Dir)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", m_oCiaSeleccionada.Dir)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", sIdiNoDisponible)
    End If
    
    Set oCias = g_oRaiz.Generar_CCias
    Set adores = oCias.DevolverDatosCia(m_oCiaSeleccionada.ID)
    
    If (Not IsNull(adores.Fields.Item("PROVICOD"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", adores.Fields.Item("PROVICOD"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", sIdiNoDisponible)
    End If

    If (Not IsNull(adores.Fields.Item("PROVIDEN"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", adores.Fields.Item("PROVIDEN"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPAI", adores.Fields.Item("PAICOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPAI", adores.Fields.Item("PAIDEN"))
    If (Disponible(m_oCiaSeleccionada.URLCia)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", m_oCiaSeleccionada.URLCia)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", sIdiNoDisponible)
    End If
    Select Case m_oCiaSeleccionada.EstadoCompradora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FC", sEstado)
    Select Case m_oCiaSeleccionada.EstadoProveedora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FP", sEstado)
    Select Case m_oCiaSeleccionada.Premium
        Case 2
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PREMIUM", sEstado)

    If (Disponible(poUsuario.Cod)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", poUsuario.Cod)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Nombre)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", poUsuario.Nombre)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Apellidos)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", poUsuario.Apellidos)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Tfno1)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", poUsuario.Tfno1)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Tfno2)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", poUsuario.Tfno2)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.TfnoMovil)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", poUsuario.TfnoMovil)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Fax)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", poUsuario.Fax)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Email)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", poUsuario.Email)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Cargo)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", poUsuario.Cargo)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Departamento)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", poUsuario.Departamento)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", sIdiNoDisponible)
    End If

    'Comienzo del cambio para selecci�n de actividades de cualquier nivel
    
    '**************************************
    '*** Actividades actuales de la cia ***
    '**************************************
    'Sacamos de la base de datos las actividades de nivel 1 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct1(poUsuario.idioma)
    sActividades = "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & ">"
    While Not adores.EOF
        sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("DEN1")
        adores.MoveNext
    Wend
    adores.Close
    
    'Sacamos de la base de datos las actividades de nivel 2 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct2(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("DEN2")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Sacamos de la base de datos las actividades de nivel 3 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct3(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("DEN3")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Sacamos de la base de datos las actividades de nivel 4 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct4(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("DEN4")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Sacamos de la base de datos las actividades de nivel 5 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct5(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("COD5") & " - " & adores.Fields.Item("DEN5")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    sActividades = sActividades & "</UL>"
    adores.Close
    
    'Poner en el mail
    Select Case AlgunaActividad(sActividades, True)
        Case True
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_ACTUALES", sActividades)
        Case False
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_ACTUALES", sIdiNoDisponible)
    End Select
    
    '*****************************************
    '*** Actividades que se desean agregar ***
    '*****************************************
    
    'Nivel 1
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct1(poUsuario.idioma)
    sActividades = "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & ">"
    While Not adores.EOF
        sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("DEN1")
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 2
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct2(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("DEN2")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 3
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct3(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("DEN3")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 4
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct4(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("DEN4")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 5
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct5(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("COD5") & " - " & adores.Fields.Item("DEN5")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    sActividades = sActividades & "</UL>"
    adores.Close
    
    'Poner en el mail
    Select Case AlgunaActividad(sActividades, True)
        Case True
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_A_SUSCRIBIR", sActividades)
        Case False
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_A_SUSCRIBIR", sIdiNoDisponible)
    End Select
    
    '******************************************
    '*** Actividades que se desean eliminar ***
    '******************************************
    
    'Nivel 1
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct1(poUsuario.idioma)
    sActividades = "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & ">"
    While Not adores.EOF
        sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("DEN1")
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 2
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct2(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("DEN2")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 3
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct3(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("DEN3")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 4
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct4(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("DEN4")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 5
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct5(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("COD5") & " - " & adores.Fields.Item("DEN5")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    sActividades = sActividades & "</UL>"
    adores.Close
    
    'Poner en el mail
    Select Case AlgunaActividad(sActividades, True)
        Case True
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_A_ELIMINAR", sActividades)
        Case False
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_A_ELIMINAR", sIdiNoDisponible)
    End Select
    
    'Fin del cambio para selecci�n de actividades de cualquier nivel

    Set adores = Nothing
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing
    Set oUsuarios = Nothing
    Set oActividadesNivel1 = Nothing
    GenerarMensajeRechazoCambActHTML = sCuerpoMensaje
End Function

Private Function GenerarMensajeRechazoCambActTXT(poUsuario As CUsuario) As String
'**************************************************************************
'*** Descripci�n: Genera el cuerpo del e-mail en texto plano a partir   ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSPSNotifRechazoCambAct.txt o                     ***
'***              ENG_FSPSNotifRechazoCambAct.txt dependi�ndo del       ***
'***              idioma.                                               ***
'***                                                                    ***
'*** Par�metros:  poUsuario, un objeto de tipo CUsuario del que         ***
'***              obtendremos datos relativos al usuario.               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
    Dim sConsulta As String
    Dim adores As Ador.Recordset
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim oUsuarios As CUsuarios
    Dim oCias As CCias
    Dim sCuerpoMensaje As String
    Dim sEstado As String
    Dim sFileName As String
    Dim sActividades As String
    Dim bFin As Boolean
    Dim bAlgunaActividad As Boolean
    Dim Ador As Ador.Recordset
    Dim sIdioma As String
    Dim oActividadesNivel1 As CActividadesNivel1
        
    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
    
    Select Case poUsuario.idioma
        Case 0
            sFileName = "ENG_"
            sIdioma = "ENG"
        Case 1
            sFileName = "SPA_"
            sIdioma = "SPA"
        Case 2
            sFileName = "GER_"
            sIdioma = "GER"
    End Select
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFIRMARACTCIA, sIdioma, 10)
    If Not Ador Is Nothing Then
        sIdiNoDisponible = Ador(0).Value
    End If
    Ador.Close
    Set Ador = Nothing
    
    sFileName = sFileName & "FSPSNotifRechazoCambAct.txt"


    Set oFos = New FileSystemObject
    
    If Not oFos.FileExists(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName) Then
        basMensajes.RutaPlantillasNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeRechazoCambActTXT = ""
        
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll
    If oActividadesNivel1.EsPYME Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", Right(m_oCiaSeleccionada.Cod, Len(m_oCiaSeleccionada.Cod) - LEN_PYME))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", m_oCiaSeleccionada.Cod)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENCIA", m_oCiaSeleccionada.Den)
    If (Disponible(m_oCiaSeleccionada.NIF)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", m_oCiaSeleccionada.NIF)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.CodPos)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", m_oCiaSeleccionada.CodPos)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.Poblacion)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", m_oCiaSeleccionada.Poblacion)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.Dir)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", m_oCiaSeleccionada.Dir)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", sIdiNoDisponible)
    End If
    
    Set oCias = g_oRaiz.Generar_CCias
    Set adores = oCias.DevolverDatosCia(m_oCiaSeleccionada.ID)
    
    If (Not IsNull(adores.Fields.Item("PROVICOD"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", adores.Fields.Item("PROVICOD"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", sIdiNoDisponible)
    End If
    
    If (Not IsNull(adores.Fields.Item("PROVIDEN"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", adores.Fields.Item("PROVIDEN"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPAI", adores.Fields.Item("PAICOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPAI", adores.Fields.Item("PAIDEN"))
    If (Disponible(m_oCiaSeleccionada.URLCia)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", m_oCiaSeleccionada.URLCia)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", sIdiNoDisponible)
    End If
    Select Case m_oCiaSeleccionada.EstadoCompradora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FC", sEstado)
    Select Case m_oCiaSeleccionada.EstadoProveedora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FP", sEstado)
    Select Case m_oCiaSeleccionada.Premium
        Case 2
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PREMIUM", sEstado)

    If (Disponible(poUsuario.Cod)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", poUsuario.Cod)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Nombre)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", poUsuario.Nombre)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Apellidos)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", poUsuario.Apellidos)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Tfno1)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", poUsuario.Tfno1)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Tfno2)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", poUsuario.Tfno2)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.TfnoMovil)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", poUsuario.TfnoMovil)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Fax)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", poUsuario.Fax)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Email)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", poUsuario.Email)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Cargo)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", poUsuario.Cargo)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Departamento)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", poUsuario.Departamento)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", sIdiNoDisponible)
    End If

    'Comienzo del cambio para selecci�n de actividades de cualquier nivel
    
    '**************************************
    '*** Actividades actuales de la cia ***
    '**************************************
    'Sacamos de la base de datos las actividades de nivel 1 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct1(poUsuario.idioma)
    sActividades = ""
    While Not adores.EOF
        sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("DEN1") & vbCrLf
        adores.MoveNext
    Wend
    adores.Close
    
    'Sacamos de la base de datos las actividades de nivel 2 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct2(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("DEN2") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Sacamos de la base de datos las actividades de nivel 3 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct3(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("DEN3") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Sacamos de la base de datos las actividades de nivel 4 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct4(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("DEN4") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Sacamos de la base de datos las actividades de nivel 5 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct5(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("COD5") & " - " & adores.Fields.Item("DEN5") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Poner en el mail
    Select Case AlgunaActividad(sActividades, False)
        Case True
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_ACTUALES", sActividades)
        Case False
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_ACTUALES", "   " & sIdiNoDisponible)
    End Select
    
    '*****************************************
    '*** Actividades que se desean agregar ***
    '*****************************************
    
    'Nivel 1
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct1(poUsuario.idioma)
    sActividades = ""
    While Not adores.EOF
        sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("DEN1") & vbCrLf
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 2
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct2(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("DEN2") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 3
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct3(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("DEN3") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 4
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct4(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("DEN4") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 5
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct5(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("COD5") & " - " & adores.Fields.Item("DEN5") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Poner en el mail
    Select Case AlgunaActividad(sActividades, False)
        Case True
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_A_SUSCRIBIR", sActividades)
        Case False
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_A_SUSCRIBIR", "   " & sIdiNoDisponible)
    End Select
    
    '******************************************
    '*** Actividades que se desean eliminar ***
    '******************************************
    
    'Nivel 1
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct1(poUsuario.idioma)
    sActividades = ""
    While Not adores.EOF
        sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("DEN1") & vbCrLf
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 2
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct2(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("DEN2") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 3
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct3(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("DEN3") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 4
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct4(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("DEN4") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 5
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct5(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("COD5") & " - " & adores.Fields.Item("DEN5") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Poner en el mail
    Select Case AlgunaActividad(sActividades, False)
        Case True
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_A_ELIMINAR", sActividades)
        Case False
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_A_ELIMINAR", "   " & sIdiNoDisponible)
    End Select
    
    'Fin del cambio para selecci�n de actividades de cualquier nivel
    
    Set adores = Nothing
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing
    Set oUsuarios = Nothing
    Set oActividadesNivel1 = Nothing
    GenerarMensajeRechazoCambActTXT = sCuerpoMensaje
End Function

Private Function GenerarMensajeConfirmCambActTXT(poUsuario As CUsuario) As String
'**************************************************************************
'*** Descripci�n: Genera el cuerpo del e-mail en texto plano a partir   ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSPSNotifConfirmCambAct.txt o                     ***
'***              ENG_FSPSNotifConfirmCambAct.txt dependi�ndo del       ***
'***              idioma.                                               ***
'***                                                                    ***
'*** Par�metros:  poUsuario, un objeto de tipo CUsuario del que         ***
'***              obtendremos datos relativos al usuario.               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
    Dim sConsulta As String
    Dim adores As Ador.Recordset
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim oUsuarios As CUsuarios
    Dim oCias As CCias
    Dim sCuerpoMensaje As String
    Dim sEstado As String
    Dim sFileName As String
    Dim sActividades As String
    Dim bFin As Boolean
    Dim bAlgunaActividad As Boolean
    Dim Ador As Ador.Recordset
    Dim sIdioma As String
    Dim oActividadesNivel1 As CActividadesNivel1
        
    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
    
    Select Case poUsuario.idioma
        Case 0
            sFileName = "ENG_"
            sIdioma = "ENG"
        Case 1
            sFileName = "SPA_"
            sIdioma = "SPA"
        Case 2
            sFileName = "GER_"
            sIdioma = "GER"
    End Select
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFIRMARACTCIA, sIdioma, 10)
    If Not Ador Is Nothing Then
        sIdiNoDisponible = Ador(0).Value
    End If
    Ador.Close
    Set Ador = Nothing
    
    sFileName = sFileName & "FSPSNotifConfirmCambAct.txt"

    Set oFos = New FileSystemObject
    
    If Not oFos.FileExists(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName) Then
        basMensajes.RutaPlantillasNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeConfirmCambActTXT = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll
    If oActividadesNivel1.EsPYME Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", Right(m_oCiaSeleccionada.Cod, Len(m_oCiaSeleccionada.Cod) - LEN_PYME))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", m_oCiaSeleccionada.Cod)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENCIA", m_oCiaSeleccionada.Den)
    If (Disponible(m_oCiaSeleccionada.NIF)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", m_oCiaSeleccionada.NIF)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.CodPos)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", m_oCiaSeleccionada.CodPos)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.Poblacion)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", m_oCiaSeleccionada.Poblacion)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.Dir)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", m_oCiaSeleccionada.Dir)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", sIdiNoDisponible)
    End If
    
    Set oCias = g_oRaiz.Generar_CCias
    Set adores = oCias.DevolverDatosCia(m_oCiaSeleccionada.ID)
    
    If (Not IsNull(adores.Fields.Item("PROVICOD"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", adores.Fields.Item("PROVICOD"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", sIdiNoDisponible)
    End If

    If (Not IsNull(adores.Fields.Item("PROVIDEN"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", adores.Fields.Item("PROVIDEN"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPAI", adores.Fields.Item("PAICOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPAI", adores.Fields.Item("PAIDEN"))
    If (Disponible(m_oCiaSeleccionada.URLCia)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", m_oCiaSeleccionada.URLCia)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", sIdiNoDisponible)
    End If
    Select Case m_oCiaSeleccionada.EstadoCompradora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FC", sEstado)
    Select Case m_oCiaSeleccionada.EstadoProveedora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FP", sEstado)
    Select Case m_oCiaSeleccionada.Premium
        Case 2
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PREMIUM", sEstado)

    If (Disponible(poUsuario.Cod)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", poUsuario.Cod)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Nombre)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", poUsuario.Nombre)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Apellidos)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", poUsuario.Apellidos)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Tfno1)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", poUsuario.Tfno1)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Tfno2)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", poUsuario.Tfno2)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.TfnoMovil)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", poUsuario.TfnoMovil)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Fax)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", poUsuario.Fax)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Email)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", poUsuario.Email)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Cargo)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", poUsuario.Cargo)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Departamento)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", poUsuario.Departamento)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", sIdiNoDisponible)
    End If

    'Comienzo del cambio para selecci�n de actividades de cualquier nivel
    
    '****************************************
    '*** Actividades antes de los cambios ***
    '****************************************
    'Sacamos de la base de datos las actividades de nivel 1 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct1(poUsuario.idioma)
    sActividades = ""
    While Not adores.EOF
        sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("DEN1") & vbCrLf
        adores.MoveNext
    Wend
    adores.Close
    
    'Sacamos de la base de datos las actividades de nivel 2 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct2(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("DEN2") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Sacamos de la base de datos las actividades de nivel 3 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct3(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("DEN3") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Sacamos de la base de datos las actividades de nivel 4 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct4(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("DEN4") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Sacamos de la base de datos las actividades de nivel 5 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct5(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("COD5") & " - " & adores.Fields.Item("DEN5") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Poner en el mail
    Select Case AlgunaActividad(sActividades, False)
        Case True
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_ANTES", sActividades)
        Case False
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_ANTES", "   " & sIdiNoDisponible)
    End Select
    
    '*****************************************
    '*** Actividades que se desean agregar ***
    '*****************************************
    
    'Nivel 1
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct1(poUsuario.idioma)
    sActividades = ""
    While Not adores.EOF
        sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("DEN1") & vbCrLf
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 2
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct2(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("DEN2") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 3
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct3(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("DEN3") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 4
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct4(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("DEN4") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 5
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct5(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("COD5") & " - " & adores.Fields.Item("DEN5") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Poner en el mail
    Select Case AlgunaActividad(sActividades, False)
        Case True
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_A_SUSCRIBIR", sActividades)
        Case False
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_A_SUSCRIBIR", "   " & sIdiNoDisponible)
    End Select
    
    '******************************************
    '*** Actividades que se desean eliminar ***
    '******************************************
    
    'Nivel 1
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct1(poUsuario.idioma)
    sActividades = ""
    While Not adores.EOF
        sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("DEN1") & vbCrLf
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 2
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct2(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("DEN2") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 3
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct3(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("DEN3") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 4
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct4(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("DEN4") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 5
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct5(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("COD5") & " - " & adores.Fields.Item("DEN5") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Poner en el mail
    Select Case AlgunaActividad(sActividades, False)
        Case True
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_A_ELIMINAR", sActividades)
        Case False
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_A_ELIMINAR", "   " & sIdiNoDisponible)
    End Select
    
    '*********************************************
    '*** Actividades de la nueva configuraci�n ***
    '*********************************************
    
    'Nivel 1
    Set adores = m_oCiaSeleccionada.DevolverNuevaConfiguracionAct1(poUsuario.idioma)
    sActividades = ""
    While Not adores.EOF
        sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("DEN1") & vbCrLf
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 2
    Set adores = m_oCiaSeleccionada.DevolverNuevaConfiguracionAct2(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("DEN2") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 3
    Set adores = m_oCiaSeleccionada.DevolverNuevaConfiguracionAct3(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("DEN3") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 4
    Set adores = m_oCiaSeleccionada.DevolverNuevaConfiguracionAct4(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("DEN4") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 5
    Set adores = m_oCiaSeleccionada.DevolverNuevaConfiguracionAct5(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "   # " & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("COD5") & " - " & adores.Fields.Item("DEN5") & vbCrLf
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Poner en el mail
    Select Case AlgunaActividad(sActividades, False)
        Case True
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_ACTUALES", sActividades)
        Case False
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_ACTUALES", "   " & sIdiNoDisponible)
    End Select
    
    'Fin del cambio para selecci�n de actividades de cualquier nivel

    Set adores = Nothing
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing
    Set oUsuarios = Nothing
    Set oActividadesNivel1 = Nothing
    GenerarMensajeConfirmCambActTXT = sCuerpoMensaje
End Function

Private Function GenerarMensajeConfirmCambActHTML(poUsuario As CUsuario) As String
'**************************************************************************
'*** Descripci�n: Genera el cuerpo del e-mail en HTML a partir          ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSPSNotifConfirmCambAct.htm o                     ***
'***              ENG_FSPSNotifConfirmCambAct.htm dependi�ndo del       ***
'***              idioma.                                               ***
'***                                                                    ***
'*** Par�metros:  poUsuario, un objeto de tipo CUsuario del que         ***
'***              obtendremos datos relativos al usuario.               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
    Dim sConsulta As String
    Dim adores As Ador.Recordset
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim oUsuarios As CUsuarios
    Dim oCias As CCias
    Dim sCuerpoMensaje As String
    Dim sEstado As String
    Dim sFileName As String
    Dim sActividades As String
    Dim bFin As Boolean
    Dim bAlgunaActividad As Boolean
    Dim Ador As Ador.Recordset
    Dim sIdioma As String
    Dim oActividadesNivel1 As CActividadesNivel1
        
    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
    
    Select Case poUsuario.idioma
        Case 0
            sFileName = "ENG_"
            sIdioma = "ENG"
        Case 1
            sFileName = "SPA_"
            sIdioma = "SPA"
        Case 2
            sFileName = "GER_"
            sIdioma = "GER"
    End Select
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFIRMARACTCIA, sIdioma, 10)
    If Not Ador Is Nothing Then
        sIdiNoDisponible = Ador(0).Value
    End If
    Ador.Close
    Set Ador = Nothing
    
    sFileName = sFileName & "FSPSNotifConfirmCambAct.htm"

    Set oFos = New FileSystemObject
    
    If Not oFos.FileExists(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName) Then
        basMensajes.RutaPlantillasNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeConfirmCambActHTML = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll
    
    If oActividadesNivel1.EsPYME Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", Right(m_oCiaSeleccionada.Cod, Len(m_oCiaSeleccionada.Cod) - LEN_PYME))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", m_oCiaSeleccionada.Cod)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENCIA", m_oCiaSeleccionada.Den)
    If (Disponible(m_oCiaSeleccionada.NIF)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", m_oCiaSeleccionada.NIF)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.CodPos)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", m_oCiaSeleccionada.CodPos)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.Poblacion)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", m_oCiaSeleccionada.Poblacion)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.Dir)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", m_oCiaSeleccionada.Dir)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", sIdiNoDisponible)
    End If
    
    Set oCias = g_oRaiz.Generar_CCias
    Set adores = oCias.DevolverDatosCia(m_oCiaSeleccionada.ID)
    
    If (Not IsNull(adores.Fields.Item("PROVICOD"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", adores.Fields.Item("PROVICOD"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", sIdiNoDisponible)
    End If
    
    If (Not IsNull(adores.Fields.Item("PROVIDEN"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", adores.Fields.Item("PROVIDEN"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPAI", adores.Fields.Item("PAICOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPAI", adores.Fields.Item("PAIDEN"))
    If (Disponible(m_oCiaSeleccionada.URLCia)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", m_oCiaSeleccionada.URLCia)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", sIdiNoDisponible)
    End If
    Select Case m_oCiaSeleccionada.EstadoCompradora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FC", sEstado)
    Select Case m_oCiaSeleccionada.EstadoProveedora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FP", sEstado)
    Select Case m_oCiaSeleccionada.Premium
        Case 2
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PREMIUM", sEstado)

    If (Disponible(poUsuario.Cod)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", poUsuario.Cod)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Nombre)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", poUsuario.Nombre)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Apellidos)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", poUsuario.Apellidos)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Tfno1)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", poUsuario.Tfno1)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Tfno2)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", poUsuario.Tfno2)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.TfnoMovil)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", poUsuario.TfnoMovil)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Fax)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", poUsuario.Fax)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Email)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", poUsuario.Email)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Cargo)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", poUsuario.Cargo)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Departamento)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", poUsuario.Departamento)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", sIdiNoDisponible)
    End If
    
    'Comienzo del cambio para selecci�n de actividades de cualquier nivel
    
    '****************************************
    '*** Actividades antes de los cambios ***
    '****************************************
    'Sacamos de la base de datos las actividades de nivel 1 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct1(poUsuario.idioma)
    sActividades = "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & ">"
    While Not adores.EOF
        sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("DEN1")
        adores.MoveNext
    Wend
    adores.Close
    
    'Sacamos de la base de datos las actividades de nivel 2 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct2(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("DEN2")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Sacamos de la base de datos las actividades de nivel 3 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct3(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("DEN3")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Sacamos de la base de datos las actividades de nivel 4 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct4(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("DEN4")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Sacamos de la base de datos las actividades de nivel 5 y las ponemos en el mail
    Set adores = m_oCiaSeleccionada.DevolverActualesAct5(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("COD5") & " - " & adores.Fields.Item("DEN5")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    sActividades = sActividades & "</UL>"
    adores.Close
    
    'Poner en el mail
    Select Case AlgunaActividad(sActividades, True)
        Case True
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_ANTES", sActividades)
        Case False
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_ANTES", sIdiNoDisponible)
    End Select
    
    '*****************************************
    '*** Actividades que se desean agregar ***
    '*****************************************
    
    'Nivel 1
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct1(poUsuario.idioma)
    sActividades = "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & ">"
    While Not adores.EOF
        sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("DEN1")
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 2
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct2(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("DEN2")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 3
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct3(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("DEN3")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 4
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct4(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("DEN4")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 5
    Set adores = m_oCiaSeleccionada.DevolverAgregadasAct5(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("COD5") & " - " & adores.Fields.Item("DEN5")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    sActividades = sActividades & "</UL>"
    adores.Close
    
    'Poner en el mail
    Select Case AlgunaActividad(sActividades, True)
        Case True
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_A_SUSCRIBIR", sActividades)
        Case False
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_A_SUSCRIBIR", sIdiNoDisponible)
    End Select
    
    '******************************************
    '*** Actividades que se desean eliminar ***
    '******************************************
    
    'Nivel 1
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct1(poUsuario.idioma)
    sActividades = "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & ">"
    While Not adores.EOF
        sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("DEN1")
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 2
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct2(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("DEN2")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 3
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct3(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("DEN3")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 4
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct4(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("DEN4")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 5
    Set adores = m_oCiaSeleccionada.DevolverEliminadasAct5(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("COD5") & " - " & adores.Fields.Item("DEN5")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    sActividades = sActividades & "</UL>"
    adores.Close
    
    'Poner en el mail
    Select Case AlgunaActividad(sActividades, True)
        Case True
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_A_ELIMINAR", sActividades)
        Case False
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_A_ELIMINAR", sIdiNoDisponible)
    End Select
    
    '*********************************************
    '*** Actividades de la nueva configuraci�n ***
    '*********************************************
    
    'Nivel 1
    Set adores = m_oCiaSeleccionada.DevolverNuevaConfiguracionAct1(poUsuario.idioma)
    sActividades = "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & ">"
    While Not adores.EOF
        sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("DEN1")
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 2
    Set adores = m_oCiaSeleccionada.DevolverNuevaConfiguracionAct2(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("DEN2")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 3
    Set adores = m_oCiaSeleccionada.DevolverNuevaConfiguracionAct3(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("DEN3")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 4
    Set adores = m_oCiaSeleccionada.DevolverNuevaConfiguracionAct4(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("DEN4")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    adores.Close
    
    'Nivel 5
    Set adores = m_oCiaSeleccionada.DevolverNuevaConfiguracionAct5(poUsuario.idioma)
    bFin = False
    While ((Not adores.EOF) And (Not bFin))
        If (NullToStr(adores.Fields.Item("CIA")) = "") Then
            sActividades = sActividades & "<LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("COD5") & " - " & adores.Fields.Item("DEN5")
        Else
            bFin = True
        End If
        adores.MoveNext
    Wend
    sActividades = sActividades & "</UL>"
    adores.Close
    
    'Poner en el mail
    Select Case AlgunaActividad(sActividades, True)
        Case True
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_ACTUALES", sActividades)
        Case False
            sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES_ACTUALES", sIdiNoDisponible)
    End Select
    
    'Fin del cambio para selecci�n de actividades de cualquier nivel
    
    Set adores = Nothing
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing
    Set oUsuarios = Nothing
    Set oActividadesNivel1 = Nothing
    GenerarMensajeConfirmCambActHTML = sCuerpoMensaje
End Function

''' <summary>
''' Confirmar "Solicitud de cambio de actividad"
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdConfirmar_Click()
    Dim oError As CTESError
    Dim resp As Variant
    Dim oUsuarioSeleccionado As CUsuario
    Dim oUsuarios As CUsuarios
    Dim errormail As TipoErrorSummit
    Dim bSinUsuarioPrincipal As Boolean
    If m_oCiaSeleccionada Is Nothing Then Exit Sub
    
    resp = basMensajes.PreguntaConfirmarEstructuraAct
    If resp = vbYes Then
        Set oUsuarios = g_oRaiz.Generar_CUsuarios
        Set oUsuarioSeleccionado = g_oRaiz.Generar_CUsuario
        Set oUsuarios = m_oCiaSeleccionada.CargarTodosLosUsuariosDesde
        
        If IsNull(m_oCiaSeleccionada.UsuarioPrincipal) Then
            basMensajes.sinUsuarioPrincipal
            bSinUsuarioPrincipal = True
            For Each oUsuarioSeleccionado In oUsuarios
                Exit For
            Next
        Else
            Set oUsuarioSeleccionado = oUsuarios.Item((CStr(m_oCiaSeleccionada.UsuarioPrincipal)))
        End If
        RellenarDatosUsuario oUsuarioSeleccionado
        
        Dim sAsunto As String
        
        If ((Not m_bSesionIniciada) Or (g_oMailSender Is Nothing)) Then
            DoEvents
            IniciarSesionMail
            m_bSesionIniciada = True
        End If
            
        Select Case oUsuarioSeleccionado.idioma
            Case 0
                sAsunto = g_udtParametrosGenerales.g_sNotifConfirmCambActENG
            Case 1
                sAsunto = g_udtParametrosGenerales.g_sNotifConfirmCambActSPA
            Case 2
                sAsunto = g_udtParametrosGenerales.g_sNotifConfirmCambActger
        End Select
                    
        If Not RegistrarCambiosSolicitud Then
            Exit Sub
        End If
        g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Activ_Confirmar, g_sLitRegAccion_SPA(8) & ": " & m_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_SPA(23) & " " & g_sCodADM, _
                                                            g_sLitRegAccion_ENG(8) & ": " & m_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_ENG(23) & " " & g_sCodADM, _
                                                            g_sLitRegAccion_FRA(8) & ": " & m_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_FRA(23) & " " & g_sCodADM, _
                                                            g_sLitRegAccion_GER(8) & ": " & m_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_GER(23) & " " & g_sCodADM, _
                                                            m_oCiaSeleccionada.ID
        Dim sCuerpo As String
        If g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS <> "" Then
            m_oCiaSeleccionada.SacarRemitente
            m_sCarpeta_Plantilla = m_oCiaSeleccionada.CarpetaPlantilla
        End If
        Select Case oUsuarioSeleccionado.TipoMail
            Case 0
                sCuerpo = GenerarMensajeConfirmCambActTXT(oUsuarioSeleccionado)
            Case 1
                sCuerpo = GenerarMensajeConfirmCambActHTML(oUsuarioSeleccionado)
        End Select
                    
        If (AceptarActiv = True) Then
            If (g_udtParametrosGenerales.g_iNotifCambAct = 1) Then
                
                Dim sTo As String
                If (g_udtParametrosGenerales.g_iConfirmCambAct = 0) Or bSinUsuarioPrincipal Then
                    Set frmConfirNotif.g_oCiaSeleccionada = m_oCiaSeleccionada
                    frmConfirNotif.Show vbModal
                    sTo = frmConfirNotif.txtMail
                    If frmConfirNotif.bCancelar = True Then
                        Unload frmConfirNotif
                        Exit Sub
                    End If
                    Unload frmConfirNotif
                Else
                    sTo = oUsuarioSeleccionado.Email
                End If
                If sCuerpo <> "" Then
                    errormail = ComponerMensaje(sTo, sAsunto, sCuerpo, , "frmConfirmarActCia", m_oCiaSeleccionada.ID, SQLBinaryToBoolean(oUsuarioSeleccionado.TipoMail))
                    If errormail.NumError <> TESnoerror Then
                        If errormail.NumError <> TESnoerror Then
                            basMensajes.ErrorOriginal errormail
                        End If
                    End If
                End If
    
            End If
        End If
    ElseIf resp = vbNo Then
        Exit Sub
    End If
    
    Set m_oCiaSeleccionada = Nothing
    
    sdbcCiaCod.Text = ""
    
    limpiarArbol
    
    If (m_bSesionIniciada) Then
        FinalizarMailSender
    End If

End Sub

''' <summary>
''' Deshacer "Solicitud de cambio de actividad"
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdDeshacer_Click()
    
    Dim errormail As TipoErrorSummit
    Dim oError As CTESError
    Dim resp As Variant
    Dim oUsuarioSeleccionado As CUsuario
    Dim oUsuarios As CUsuarios

    If m_oCiaSeleccionada Is Nothing Then Exit Sub
    
    resp = basMensajes.PreguntaCancelCambEstructuraAct
    If resp = vbYes Then
        
        Dim sCuerpo As String
        Set oUsuarios = g_oRaiz.Generar_CUsuarios
        Set oUsuarioSeleccionado = g_oRaiz.Generar_CUsuario
        Set oUsuarios = m_oCiaSeleccionada.CargarTodosLosUsuariosDesde
        Set oUsuarioSeleccionado = oUsuarios.Item((CStr(m_oCiaSeleccionada.UsuarioPrincipal)))
        RellenarDatosUsuario oUsuarioSeleccionado
        If g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS <> "" Then
            m_oCiaSeleccionada.SacarRemitente
            m_sCarpeta_Plantilla = m_oCiaSeleccionada.CarpetaPlantilla
        End If
        Select Case oUsuarioSeleccionado.TipoMail
            Case 0
                sCuerpo = GenerarMensajeRechazoCambActTXT(oUsuarioSeleccionado)
            Case 1
                sCuerpo = GenerarMensajeRechazoCambActHTML(oUsuarioSeleccionado)
        End Select
        
        Set oError = m_oCiaSeleccionada.cancelarCambios
        If oError.NumError = TESnoerror Then
            g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Activ_Deshacer, g_sLitRegAccion_SPA(8) & ": " & m_oCiaSeleccionada.Cod, _
                                                g_sLitRegAccion_ENG(8) & ": " & m_oCiaSeleccionada.Cod, _
                                                g_sLitRegAccion_FRA(8) & ": " & m_oCiaSeleccionada.Cod, _
                                                g_sLitRegAccion_GER(8) & ": " & m_oCiaSeleccionada.Cod, _
                                                m_oCiaSeleccionada.ID
        End If
        Dim sAsunto As String
        
        If ((Not m_bSesionIniciada) Or (g_oMailSender Is Nothing)) Then
            DoEvents
            IniciarSesionMail
            m_bSesionIniciada = True
        End If
            
        Select Case oUsuarioSeleccionado.idioma
            Case 0
                sAsunto = g_udtParametrosGenerales.g_sNotifRechazoCambActENG
            Case 1
                sAsunto = g_udtParametrosGenerales.g_sNotifRechazoCambActSPA
            Case 2
                sAsunto = g_udtParametrosGenerales.g_sNotifRechazoCambActger
        End Select
            
    

        'PrepararMensajeConfirmacion oUsuarioSeleccionado
        Dim sTo As String
        
        If (g_udtParametrosGenerales.g_iConfirmCambAct = 0) Then
            Set frmConfirNotif.g_oCiaSeleccionada = m_oCiaSeleccionada
            frmConfirNotif.Show vbModal
            If frmConfirNotif.bCancelar = True Then
                Unload frmConfirNotif
                Exit Sub
            End If
            sTo = frmConfirNotif.txtMail
            Unload frmConfirNotif
            
            
        Else
            sTo = oUsuarioSeleccionado.Email

        End If
        
            
        If (g_udtParametrosGenerales.g_iNotifCambAct = 1) Then
        
            If sCuerpo <> "" Then
                errormail = ComponerMensaje(sTo, sAsunto, sCuerpo, , "frmConfirmarActCia", m_oCiaSeleccionada.ID, SQLBinaryToBoolean(oUsuarioSeleccionado.TipoMail))
                If errormail.NumError <> TESnoerror Then
                    If errormail.NumError <> TESnoerror Then
                        basMensajes.ErrorOriginal errormail
                    End If
                End If
            End If
    
        End If
        
        If (m_bSesionIniciada) Then
            FinalizarMailSender
        End If
        
    ElseIf resp = vbNo Then
        Exit Sub
    End If
    
    Set m_oCiaSeleccionada = Nothing
    
    sdbcCiaCod.Text = ""
    
    limpiarArbol
    

End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFIRMARACTCIA, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdConfirmar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdDeshacer.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label2.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label7.Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcCiaCod.Columns(1).Caption = Ador(0).Value
        Me.sdbcCiaDen.Columns(2).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcCiaCod.Columns(2).Caption = Ador(0).Value
        Me.sdbcCiaDen.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        sIdiAct = Ador(0).Value
        Ador.MoveNext
        sIdiNoDisponible = Ador(0).Value
        Ador.MoveNext
        sIdiSi = Ador(0).Value
        Ador.MoveNext
        sIdiNo = Ador(0).Value
        Ador.MoveNext
        m_sLitRegistro(0) = Ador(0).Value 'C�a
        Ador.MoveNext
        m_sLitRegistro(1) = Ador(0).Value 'Modificada selecci�n de actividades por usuario
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub


