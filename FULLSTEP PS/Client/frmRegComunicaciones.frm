VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmRegComunicaciones 
   Caption         =   "DHist�rico de emails enviados"
   ClientHeight    =   7095
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12135
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRegComunicaciones.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7095
   ScaleWidth      =   12135
   Begin FSPSClient.PaginacionGrid pgRegistro 
      Height          =   375
      Left            =   60
      TabIndex        =   15
      Top             =   1080
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   661
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgRegistro 
      Height          =   5580
      Left            =   60
      TabIndex        =   6
      Top             =   1470
      Width           =   12015
      _Version        =   196617
      DataMode        =   2
      RecordSelectors =   0   'False
      GroupHeaders    =   0   'False
      Col.Count       =   13
      stylesets.count =   3
      stylesets(0).Name=   "Sobre"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmRegComunicaciones.frx":0CB2
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmRegComunicaciones.frx":124C
      stylesets(2).Name=   "Adjunto"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmRegComunicaciones.frx":1268
      DividerType     =   0
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   13
      Columns(0).Width=   609
      Columns(0).Name =   "MSG"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   609
      Columns(1).Name =   "ADJUN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1799
      Columns(2).Caption=   "FECHA"
      Columns(2).Name =   "FECHA"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   2831
      Columns(3).Caption=   "COD_CIA"
      Columns(3).Name =   "COD_CIA"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3572
      Columns(4).Caption=   "DEN_CIA"
      Columns(4).Name =   "DEN_CIA"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   2805
      Columns(5).Caption=   "PARA"
      Columns(5).Name =   "PARA"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   2805
      Columns(6).Caption=   "ASUNTO"
      Columns(6).Name =   "ASUNTO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   2805
      Columns(7).Caption=   "CC"
      Columns(7).Name =   "CC"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   2805
      Columns(8).Caption=   "CCO"
      Columns(8).Name =   "CCO"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "ID_CIA"
      Columns(9).Name =   "ID_CIA"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "ID"
      Columns(10).Name=   "ID"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "ADJUNTOS"
      Columns(11).Name=   "ADJUNTOS"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "ACUSE_RECIBO"
      Columns(12).Name=   "ACUSE_RECIBO"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      _ExtentX        =   21193
      _ExtentY        =   9842
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraCias 
      Height          =   975
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   11985
      Begin VB.CommandButton cmdListado 
         Enabled         =   0   'False
         Height          =   285
         Left            =   11520
         Picture         =   "frmRegComunicaciones.frx":15BA
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   600
         Width           =   315
      End
      Begin VB.TextBox txtFecDesde 
         Height          =   285
         Left            =   1350
         TabIndex        =   11
         Top             =   210
         Width           =   1035
      End
      Begin VB.CommandButton cmdFecDesde 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2445
         Picture         =   "frmRegComunicaciones.frx":163F
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   210
         Width           =   315
      End
      Begin VB.TextBox txtFecHasta 
         Height          =   285
         Left            =   3725
         TabIndex        =   9
         Top             =   210
         Width           =   1035
      End
      Begin VB.CommandButton cmdFecHasta 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4875
         Picture         =   "frmRegComunicaciones.frx":1951
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   210
         Width           =   315
      End
      Begin VB.CommandButton cmdCargar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   11520
         Picture         =   "frmRegComunicaciones.frx":1C63
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   210
         Width           =   315
      End
      Begin VB.CommandButton cmdBuscar 
         Height          =   285
         Left            =   9050
         Picture         =   "frmRegComunicaciones.frx":1CEE
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   600
         Width           =   300
      End
      Begin FSPSClient.CiaSelector CiaSelector1 
         Height          =   285
         Left            =   3365
         TabIndex        =   1
         Top             =   600
         Width           =   300
         _ExtentX        =   529
         _ExtentY        =   503
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaCod 
         Height          =   285
         Left            =   1350
         TabIndex        =   3
         Top             =   600
         Width           =   1940
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   7064
         Columns(2).Caption=   "Denominaci�n"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FC"
         Columns(3).Name =   "FC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FP"
         Columns(4).Name =   "FP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   3422
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaDen 
         Height          =   285
         Left            =   3725
         TabIndex        =   4
         Top             =   600
         Width           =   5220
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5980
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "C�digo"
         Columns(2).Name =   "COD"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FC"
         Columns(3).Name =   "FC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FP"
         Columns(4).Name =   "FP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   9208
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
      End
      Begin VB.Label lblFechaDesde 
         Caption         =   "Fecha desde:"
         Height          =   195
         Left            =   240
         TabIndex        =   13
         Top             =   300
         Width           =   1095
      End
      Begin VB.Label lblFecHasta 
         Caption         =   "hasta:"
         Height          =   195
         Left            =   3135
         TabIndex        =   12
         Top             =   300
         Width           =   540
      End
      Begin VB.Label lblCia 
         Caption         =   "Compa��a:"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   600
         Width           =   1000
      End
   End
End
Attribute VB_Name = "frmRegComunicaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const cnMargen As Long = 60

Public m_bRespetarCombo As Boolean
Public g_oCiaSeleccionada As CCia

Private m_oRegistros As CRegistrosEmail
Private m_bCargarComboDesde As Boolean
Private m_oCias As CCias

Private sIdiAbrevPend As String
Private sIdiAbrevTodas As String
Private sIdiOrdenando As String

Private m_iOrden As Integer

Private Sub CiaSelector1_Click(ByVal opcion As PSSeleccion)
    sdbcCiaCod = ""
End Sub

Private Sub cmdBuscar_Click()
    frmCiaBuscar.g_sOrigen = "frmRegComunicaciones"
    frmCiaBuscar.WindowState = vbNormal
    frmCiaBuscar.Show 1
    
    'Actualizo el bot�n del ciaselector
    CiaSelector1.Seleccion = PSSeleccion.PSTodos
End Sub

Private Sub cmdCargar_Click()
Dim s As String
    CargarHistorico 1, True, m_iOrden
    If IsDate(txtFecDesde.Text) Then
        s = "Desde " & txtFecDesde.Text
    End If
    If IsDate(txtFecHasta.Text) Then
        s = s & " hasta " & txtFecHasta.Text
    End If
    If sdbcCiaCod.Text <> "" Then
        If s <> "" Then s = s & "; "
        s = s & "C�a: " & sdbcCiaCod.Text
    End If
    If s = "" Then s = "Ninguno"
    g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.RegMail_Buscar_Notif_Cia, g_sLitRegAccion_SPA(39) & ": " & s, g_sLitRegAccion_ENG(39) & ": " & s, _
                                                    g_sLitRegAccion_FRA(39) & ": " & s, g_sLitRegAccion_GER(39) & ": " & s
End Sub


Private Sub cmdFecDesde_Click()
    Set frmCalendar.frmDestination = frmRegComunicaciones
    Set frmCalendar.ctrDestination = txtFecDesde
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecDesde.Text <> "" Then
        frmCalendar.Calendar.Value = txtFecDesde.Text
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub

Private Sub cmdFecHasta_Click()
    Set frmCalendar.frmDestination = frmRegComunicaciones
    Set frmCalendar.ctrDestination = txtFecHasta
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecHasta.Text <> "" Then
        frmCalendar.Calendar.Value = txtFecHasta.Text
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub

Private Sub cmdListado_Click()
    'Genera un listado
    If Not g_oCiaSeleccionada Is Nothing Then
        frmLstRegComunic.optTodos.Value = False
        frmLstRegComunic.optSelec.Value = True
        
        frmLstRegComunic.sdbcCiaCod.Text = g_oCiaSeleccionada.Cod
        frmLstRegComunic.sdbcCiaCod_Validate False

    End If
    frmLstRegComunic.WindowState = vbNormal
    frmLstRegComunic.SetFocus
    
End Sub

Private Sub Form_Load()
   
    Me.Width = 12255
    Me.Height = 7605

    CargarRecursos

    cmdListado.Enabled = False
    
    Set m_oCias = g_oRaiz.Generar_CCias
    
    CiaSelector1.AbrevParaPendientes = sIdiAbrevPend
    CiaSelector1.AbrevParaTodos = sIdiAbrevTodas
    
    m_iOrden = 0
    
    CargarFiltrosDefecto
    CargarHistorico 1, True
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

''' <summary>Dimensiona y coloca los controles en pantalla</summary>
''' <remarks>Llamada desde Form_Resize</remarks>

Private Sub Arrange()
    If Me.Height < 1500 Then Exit Sub
    If Me.Width < 400 Then Exit Sub
    
    fraCias.Left = cnMargen
    fraCias.Width = Me.ScaleWidth - (2 * cnMargen)
    
    cmdCargar.Left = fraCias.Width - cmdCargar.Width - (2 * cnMargen)
    
    cmdListado.Left = fraCias.Width - cmdListado.Width - (2 * cnMargen)
    
    pgRegistro.Left = cnMargen
    
    ArrangeGrid
    
    sdbgRegistro.Columns("FECHA").Width = sdbgRegistro.Width * 0.084
    sdbgRegistro.Columns("COD_CIA").Width = sdbgRegistro.Width * 0.1335
    sdbgRegistro.Columns("DEN_CIA").Width = sdbgRegistro.Width * 0.1934
    sdbgRegistro.Columns("PARA").Width = sdbgRegistro.Width * 0.1322
    sdbgRegistro.Columns("ASUNTO").Width = sdbgRegistro.Width * 0.1322
    sdbgRegistro.Columns("CC").Width = sdbgRegistro.Width * 0.1322
    sdbgRegistro.Columns("CCO").Width = sdbgRegistro.Width * 0.1322
End Sub

''' <summary>Dimensiona y coloca en pantalla el grid</summary>
''' <remarks>Llamada desde Arrange y CargarHistorico</remarks>

Private Sub ArrangeGrid()
    sdbgRegistro.Top = IIf(pgRegistro.Visible, pgRegistro.Top + pgRegistro.Height + cnMargen, pgRegistro.Top)
    sdbgRegistro.Height = Me.ScaleHeight - IIf(pgRegistro.Visible, (pgRegistro.Top + pgRegistro.Height), (fraCias.Top + fraCias.Height)) - cnMargen
    sdbgRegistro.Left = cnMargen
    sdbgRegistro.Width = Me.ScaleWidth - (2 * cnMargen)
End Sub

Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_REGCOMUNIC, g_udtParametrosGenerales.g_sIdioma)
    
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        ador.MoveNext
        lblCia.Caption = ador(0).Value
        ador.MoveNext
        
        sdbgRegistro.Columns("FECHA").Caption = ador(0).Value
        ador.MoveNext
        sdbgRegistro.Columns("COD_CIA").Caption = ador(0).Value
        ador.MoveNext
        sdbgRegistro.Columns("DEN_CIA").Caption = ador(0).Value
        ador.MoveNext
        sdbgRegistro.Columns("PARA").Caption = ador(0).Value
        ador.MoveNext
        sdbgRegistro.Columns("ASUNTO").Caption = ador(0).Value
        ador.MoveNext
        sdbgRegistro.Columns("CC").Caption = ador(0).Value
        ador.MoveNext
        sdbgRegistro.Columns("CCO").Caption = ador(0).Value
        ador.MoveNext
        sIdiAbrevPend = ador(0).Value 'P
        ador.MoveNext
        sIdiAbrevTodas = ador(0).Value 'T
        ador.MoveNext
        sIdiOrdenando = ador(0).Value
        ador.MoveNext
        sdbcCiaCod.Columns("COD").Caption = ador(0).Value
        sdbcCiaDen.Columns("COD").Caption = ador(0).Value
        ador.MoveNext
        sdbcCiaCod.Columns("DEN").Caption = ador(0).Value
        sdbcCiaDen.Columns("DEN").Caption = ador(0).Value
        ador.MoveNext
        lblFechaDesde.Caption = ador(0).Value & ":"
        ador.MoveNext
        lblFecHasta.Caption = ador(0).Value & ":"
        ador.MoveNext
        pgRegistro.PageCaption = ador(0).Value
        
        ador.Close
    
    End If

   Set ador = Nothing

End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set m_oRegistros = Nothing
    Set g_oCiaSeleccionada = Nothing
    Set m_oCias = Nothing
End Sub

Private Sub pgRegistro_OnFirstClick()
    CargarHistorico 1, True, m_iOrden
End Sub

Private Sub pgRegistro_OnLastClick()
    CargarHistorico pgRegistro.NumPaginas, True, m_iOrden
End Sub

Private Sub pgRegistro_OnNextClick(ByVal iPaginaActual As Integer)
    CargarHistorico iPaginaActual, True, m_iOrden
End Sub

Private Sub pgRegistro_OnPreviousClick(ByVal iPaginaActual As Integer)
    CargarHistorico iPaginaActual, False, m_iOrden
End Sub

Private Sub sdbcCiaCod_DropDown()
    Dim i As Long
    Dim ador As ador.Recordset
    
    sdbcCiaCod.RemoveAll
    Set g_oCiaSeleccionada = Nothing
    
    Select Case CiaSelector1.Seleccion
        Case PSSeleccion.PSTodos
            'Carga todos
            If m_bCargarComboDesde Then
                Set ador = m_oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False, , , , , , , , , , , , , , 4)
            Else
                Set ador = m_oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, , , , , , , , , , , , , , 4)
            End If
            
        Case PSSeleccion.PSPendientes
            'Carga pendientes
            If m_bCargarComboDesde Then
                Set ador = m_oCias.DevolverCiasPendientesDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False)
            Else
                Set ador = m_oCias.DevolverCiasPendientesDesde(g_iCargaMaximaCombos, , , False, False)
            End If
    End Select
    
    If Not ador Is Nothing Then
        
        i = 1
        
        While Not ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaCod.AddItem ador("ID").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("FCEST").Value & Chr(9) & ador("FPEST").Value
            ador.MoveNext
        Wend
        
        If Not ador.EOF Then
            sdbcCiaCod.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        ador.Close
        Set ador = Nothing
    End If
    
    
    sdbcCiaCod.SelStart = 0
    sdbcCiaCod.SelLength = Len(sdbcCiaCod.Text)
    sdbcCiaCod.Refresh
    
End Sub

Private Sub sdbcCiaCod_InitColumnProps()
    
    sdbcCiaCod.DataField = "Column 1"
    sdbcCiaCod.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcCiaCod_Change()
    
    If Not m_bRespetarCombo Then
    
        m_bRespetarCombo = True
        sdbcCiaDen.Text = ""
                
        m_bRespetarCombo = False
        
        m_bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCiaCod_Click()
    
    If Not sdbcCiaCod.DroppedDown Then
        sdbcCiaCod = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaCod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaCod.Rows - 1
            bm = sdbcCiaCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaCod_CloseUp()

    If sdbcCiaCod.Value = "-1" Then
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaCod.Text = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaCod.Columns(2).Text
    m_bRespetarCombo = True
    sdbcCiaCod.Text = sdbcCiaCod.Columns(1).Text
    m_bRespetarCombo = False
    
    m_bCargarComboDesde = False
    
    CiaSeleccionada
    
    
End Sub

Private Sub sdbcCiaCod_Validate(Cancel As Boolean)
    Dim ador As ador.Recordset

    If Trim(sdbcCiaCod.Text) = "" Then
        Set g_oCiaSeleccionada = Nothing
        Exit Sub
    End If
    
    Select Case CiaSelector1.Seleccion
        Case PSSeleccion.PSTodos
            'Carga todos
            Set ador = m_oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , True, , , , , , , , , , , , , , , 4)
            
        Case PSSeleccion.PSPendientes
            'Carga pendientes
            Set ador = m_oCias.DevolverCiasPendientesDesde(1, sdbcCiaCod.Text, , True)
    End Select
    
    If ador Is Nothing Then
        
        sdbcCiaCod = ""
        Exit Sub
        
    Else
        If ador.EOF Then
            sdbcCiaCod.Value = ""
            Exit Sub
        Else
            If UCase(sdbcCiaCod.Text) <> UCase(ador("COD").Value) Then
                sdbcCiaCod.Value = ""
                Exit Sub
            Else
                m_bRespetarCombo = True
                sdbcCiaDen.Text = ador("DEN").Value
                sdbcCiaCod.Text = ador("COD").Value
                sdbcCiaCod.Columns(0).Value = ador("ID").Value
                CiaSeleccionada
                m_bRespetarCombo = False
                m_bCargarComboDesde = False
            End If
        End If
        
        ador.Close
        Set ador = Nothing
        
    End If
    
End Sub

Private Sub sdbcCiaDen_DropDown()
    Dim i As Long
    Dim ador As ador.Recordset
    
    sdbcCiaDen.RemoveAll
    sdbcCiaDen.ReBind
    Set g_oCiaSeleccionada = Nothing
    
    Select Case CiaSelector1.Seleccion
        Case PSSeleccion.PSTodos
            'Carga todos
            If m_bCargarComboDesde Then
                Set ador = m_oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True, , , , , , , , , , , , , , 4)
            Else
                Set ador = m_oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , , False, True, , , , , , , , , , , , , , 4)
            End If
            
        Case PSSeleccion.PSPendientes
            'Carga pendientes
            If m_bCargarComboDesde Then
                Set ador = m_oCias.DevolverCiasPendientesDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True)
            Else
                Set ador = m_oCias.DevolverCiasPendientesDesde(basParametros.g_iCargaMaximaCombos, , , False, True)
            End If
    End Select
    
    If Not ador Is Nothing Then
        
        i = 1
        
        While Not ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaDen.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("FCEST").Value & Chr(9) & ador("FPEST").Value
            ador.MoveNext
        Wend
        
        If Not ador.EOF Then
            sdbcCiaDen.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        ador.Close
        Set ador = Nothing
    End If
    
    
    sdbcCiaDen.SelStart = 0
    sdbcCiaDen.SelLength = Len(sdbcCiaDen.Text)
    sdbcCiaDen.Refresh
    
End Sub
Private Sub sdbcCiaDen_InitColumnProps()
    
    sdbcCiaDen.DataField = "Column 1"
    sdbcCiaDen.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcCiaDen_Change()
    
    If Not m_bRespetarCombo Then
    
        m_bRespetarCombo = True
        sdbcCiaCod.Text = ""
        
        m_bRespetarCombo = False
        m_bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCiaDen_Click()
    
    If Not sdbcCiaDen.DroppedDown Then
        sdbcCiaDen = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaDen_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaDen.Rows - 1
            bm = sdbcCiaDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaDen.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaDen_CloseUp()

    If sdbcCiaDen.Columns(0).Value = "-1" Then
        sdbcCiaDen.Text = ""
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaDen.Value = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaDen.Columns(1).Text
    sdbcCiaCod.Text = sdbcCiaDen.Columns(2).Text
    sdbcCiaCod.Columns("ID").Value = sdbcCiaDen.Columns("ID").Value
    m_bRespetarCombo = False
    
    m_bCargarComboDesde = False
    
    CiaSeleccionada
    
End Sub

Public Sub CiaSeleccionada()
    If Not sdbcCiaCod.Text = "" Then
        m_oCias.CargarTodasLasCiasDesde 1, sdbcCiaCod.Columns(0).Value, , , True
          
        Set g_oCiaSeleccionada = Nothing
        Set g_oCiaSeleccionada = m_oCias.Item(CStr(sdbcCiaCod.Columns(0).Value))
    End If
    
    m_bRespetarCombo = False
End Sub

''' <summary>Carga los filtros por defecto de la pantalla</summary>
''' <remarks>Llamada desde: Form_Load</remarks>

Private Sub CargarFiltrosDefecto()
    txtFecHasta.Text = Date
    txtFecDesde.Text = DateAdd("d", -6, Date)
End Sub

Private Sub CargarHistorico(ByVal iNumPagCargar As Integer, ByVal bPaginacionSig As Boolean, Optional ByVal iOrden As Integer)
    'Carga las comunicaciones asociadas a la cia seleccionada
    Dim ador As ador.Recordset
    Dim i As Long
    Dim iNumPag As Long
    Dim dtFecha As Date
    
    If ComprobarFiltros Then
        sdbgRegistro.RemoveAll
        
        If m_oRegistros Is Nothing Then Set m_oRegistros = g_oRaiz.Generar_CRegistrosEmail
        
        If g_oCiaSeleccionada Is Nothing Then
            Set ador = m_oRegistros.CargarComunicacionesDesdeConPaginacion(iNumPagCargar, bPaginacionSig, iNumPag, , txtFecDesde.Text, txtFecHasta.Text, iOrden)
        Else
            Set ador = m_oRegistros.CargarComunicacionesDesdeConPaginacion(iNumPagCargar, bPaginacionSig, iNumPag, g_oCiaSeleccionada.ID, txtFecDesde.Text, txtFecHasta.Text, iOrden)
        End If
        
        pgRegistro.PaginaActual = iNumPagCargar
        pgRegistro.NumPaginas = iNumPag
        pgRegistro.Visible = (iNumPag > 1)
        ArrangeGrid
        
        DoEvents
                
        If Not ador Is Nothing Then
            'Cargar el grid
            While Not ador.EOF
                dtFecha = CDate(Left(ador("FECHA").Value, Len(ador("FECHA").Value) - 4))
                sdbgRegistro.AddItem "" & Chr(9) & "" & Chr(9) & Format(dtFecha, "Short Date") & Chr(9) & ador("CIACOD").Value & Chr(9) & ador("CIADEN").Value & Chr(9) & ador("PARA").Value & Chr(9) & ador("SUBJECT").Value & Chr(9) & ador("CC").Value & Chr(9) & ador("CCO").Value & Chr(9) & ador("CIA").Value & Chr(9) & ador("ID").Value & Chr(9) & ador("ADJUNTOS").Value
                
                ador.MoveNext
            Wend
        End If
        Set ador = Nothing
        
        If sdbgRegistro.Rows = 0 Then
            cmdListado.Enabled = False
        Else
            cmdListado.Enabled = True
        End If
        
        If Me.sdbgRegistro.Visible = True Then
            sdbgRegistro.SetFocus
        End If
    End If
End Sub

Private Function ComprobarFiltros() As Boolean
    ComprobarFiltros = True
    
    If Trim(txtFecDesde.Text) <> "" Then
        If Not IsDate(txtFecDesde.Text) Then
            basMensajes.NoValido Left(lblFechaDesde.Caption, Len(lblFechaDesde.Caption) - 1)
            ComprobarFiltros = False
            
            txtFecDesde.SetFocus
            Exit Function
        End If
    End If
    
    If Trim(txtFecHasta.Text) <> "" Then
        If Not IsDate(txtFecHasta.Text) Then
            basMensajes.NoValido Left(lblFecHasta.Caption, Len(lblFecHasta.Caption) - 1)
            ComprobarFiltros = False
            
            txtFecDesde.SetFocus
            Exit Function
        End If
    End If
End Function

Private Sub sdbcCiaDen_Validate(Cancel As Boolean)
    If Trim(sdbcCiaDen.Text) = "" Then
        Set g_oCiaSeleccionada = Nothing
        Exit Sub
    End If
End Sub

Private Sub sdbgRegistro_DblClick()
    'Muestra el detalle del proceso:
    Dim oRegistro As CRegistroEmail
    
    If Not m_oRegistros.Item(CStr(sdbgRegistro.Columns("ID_CIA").Value) & "-" & CStr(sdbgRegistro.Columns("ID").Value)) Is Nothing Then
        Set oRegistro = m_oRegistros.Item(CStr(sdbgRegistro.Columns("ID_CIA").Value) & "-" & CStr(sdbgRegistro.Columns("ID").Value))
        
        If oRegistro Is Nothing Then Exit Sub
        
        Set frmMensajeDetalle.g_oRegistro = oRegistro
        g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.RegMail_Ver_Notif_Cia, g_sLitRegAccion_SPA(8) & ": " & sdbgRegistro.Columns("COD_CIA").Value & "; " & g_sLitRegAccion_SPA(36) & ": " & sdbgRegistro.Columns("ASUNTO").Value, _
                                                g_sLitRegAccion_ENG(8) & ": " & sdbgRegistro.Columns("COD_CIA").Value & "; " & g_sLitRegAccion_ENG(36) & ": " & sdbgRegistro.Columns("ASUNTO").Value, _
                                                g_sLitRegAccion_FRA(8) & ": " & sdbgRegistro.Columns("COD_CIA").Value & "; " & g_sLitRegAccion_FRA(36) & ": " & sdbgRegistro.Columns("ASUNTO").Value, _
                                                g_sLitRegAccion_GER(8) & ": " & sdbgRegistro.Columns("COD_CIA").Value & "; " & g_sLitRegAccion_GER(36) & ": " & sdbgRegistro.Columns("ASUNTO").Value, _
                                                sdbgRegistro.Columns("ID_CIA").Value, oRegistro.ID
        frmMensajeDetalle.g_sCodCia = sdbgRegistro.Columns("COD_CIA").Value
        frmMensajeDetalle.g_bLock = False
        frmMensajeDetalle.Show vbModal
        
        Set oRegistro = Nothing
    End If
    
End Sub

Private Sub sdbgRegistro_HeadClick(ByVal ColIndex As Integer)
    Dim sHeadCaption As String
    Dim bCambioOrden As Boolean
    
    'Ordena el hist�rico por la columna seleccionada
    sHeadCaption = sdbgRegistro.Columns(ColIndex).Caption
    sdbgRegistro.Columns(ColIndex).Caption = sIdiOrdenando
           
    bCambioOrden = False
    If m_iOrden <> ColIndex Then bCambioOrden = True
       
    Select Case ColIndex
        Case 1
            m_iOrden = 1
        Case 2
            m_iOrden = 2
        Case 3
            m_iOrden = 3
        Case 4
            m_iOrden = 4
        Case 5
            m_iOrden = 5
        Case 6
            m_iOrden = 6
        Case 7
            m_iOrden = 7
        Case 8
            m_iOrden = 8
    End Select
            
    If bCambioOrden Then
        'Si se cambia el criterio de ordenaci�n cargar la primera p�gina
        pgRegistro.PaginaActual = 1
        CargarHistorico pgRegistro.PaginaActual, True, m_iOrden
    End If
    
    sdbgRegistro.Columns(ColIndex).Caption = sHeadCaption
End Sub

Private Sub sdbgRegistro_RowLoaded(ByVal Bookmark As Variant)
    
    sdbgRegistro.Columns("MSG").CellStyleSet "Sobre"
    
    If sdbgRegistro.Columns("ADJUNTOS").Value >= "1" Then
        sdbgRegistro.Columns("ADJUN").CellStyleSet "Adjunto"
    Else
        sdbgRegistro.Columns("ADJUN").CellStyleSet "Normal"
    End If
    
End Sub


