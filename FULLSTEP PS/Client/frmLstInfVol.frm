VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstInfVol 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de volumen adjudicado general"
   ClientHeight    =   2550
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4275
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstInfVol.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2550
   ScaleWidth      =   4275
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdObtener 
      Caption         =   "Obtener"
      Height          =   375
      Left            =   2880
      TabIndex        =   8
      Top             =   2160
      Width           =   1335
   End
   Begin TabDlg.SSTab ssTabListado 
      Height          =   2115
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4215
      _ExtentX        =   7435
      _ExtentY        =   3731
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstInfVol.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Opciones"
      TabPicture(1)   =   "frmLstInfVol.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "frameOpt"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Orden"
      TabPicture(2)   =   "frmLstInfVol.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "frameOrden"
      Tab(2).ControlCount=   1
      Begin VB.Frame frameOrden 
         Height          =   1575
         Left            =   -74880
         TabIndex        =   10
         Top             =   360
         Width           =   3885
         Begin VB.OptionButton optVolPor 
            Caption         =   "Volumen en la moneda del portal"
            Height          =   255
            Left            =   360
            TabIndex        =   13
            Top             =   1100
            Width           =   2655
         End
         Begin VB.OptionButton optDen 
            Caption         =   "Denominaci�n compa��a proveedora"
            Height          =   255
            Left            =   360
            TabIndex        =   12
            Top             =   700
            Width           =   3015
         End
         Begin VB.OptionButton optCod 
            Caption         =   "C�digo compa��a proveedora"
            Height          =   255
            Left            =   360
            TabIndex        =   11
            Top             =   340
            Value           =   -1  'True
            Width           =   3135
         End
      End
      Begin VB.Frame frameOpt 
         Height          =   1575
         Left            =   -74880
         TabIndex        =   9
         Top             =   360
         Width           =   3885
         Begin VB.OptionButton optComp 
            Caption         =   "Desglose por compa��as compradoras"
            Height          =   255
            Left            =   360
            TabIndex        =   16
            Top             =   700
            Width           =   3255
         End
         Begin VB.OptionButton optSoloComp 
            Caption         =   "Desglose por compa��as proveedoras"
            Height          =   255
            Left            =   360
            TabIndex        =   15
            Top             =   340
            Value           =   -1  'True
            Width           =   3255
         End
         Begin VB.OptionButton optDetalle 
            Caption         =   "Desglose por procesos"
            Height          =   255
            Left            =   360
            TabIndex        =   14
            Top             =   1100
            Width           =   3255
         End
      End
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1575
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   3885
         Begin VB.CommandButton cmdCalFecApeDesde 
            Height          =   285
            Left            =   2520
            Picture         =   "frmLstInfVol.frx":0D06
            Style           =   1  'Graphical
            TabIndex        =   5
            TabStop         =   0   'False
            ToolTipText     =   "Mantenimiento"
            Top             =   360
            Width           =   315
         End
         Begin VB.TextBox txtFecDesde 
            Height          =   285
            Left            =   1380
            TabIndex        =   1
            Top             =   360
            Width           =   1110
         End
         Begin VB.CommandButton cmdCalFecApeHasta 
            Height          =   285
            Left            =   2520
            Picture         =   "frmLstInfVol.frx":1290
            Style           =   1  'Graphical
            TabIndex        =   4
            TabStop         =   0   'False
            ToolTipText     =   "Mantenimiento"
            Top             =   960
            Width           =   315
         End
         Begin VB.TextBox txtFecHasta 
            Height          =   285
            Left            =   1380
            TabIndex        =   2
            Top             =   960
            Width           =   1110
         End
         Begin VB.Label lblFecDesde 
            Caption         =   "Fec. Desde:"
            Height          =   255
            Left            =   240
            TabIndex        =   7
            Top             =   390
            Width           =   1065
         End
         Begin VB.Label lblFecHasta 
            Caption         =   "Fec. Hasta:"
            Height          =   225
            Left            =   240
            TabIndex        =   6
            Top             =   990
            Width           =   855
         End
      End
   End
End
Attribute VB_Name = "frmLstInfVol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean

Public DenMonCentral As String
Public bExcel As Boolean

Private sIdiFecDesde As String
Private sIdiFecHasta As String
Private sIdiComprador As String
Private sIdiMonGS As String
Private sIdiAdjGS As String
Private sIdiAdj As String
Private sIdiProce As String
Private sIdiFec As String
Private sIdiTotAdj As String

Private sIdiTxtTitulo As String
Private sIdiTxtFecDesde As String
Private sIdiTxtFecHasta As String
Private sIdiTxtProve As String
Private sIdiTxtDen As String
Private sIdiTxtPag As String
Private sIdiTxtDe As String

Private Sub cmdCalFecApeDesde_Click()
    'Muestra el formulario del calendario
    Set frmCalendar.frmDestination = frmLstInfVol
    Set frmCalendar.ctrDestination = txtFecDesde
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecDesde <> "" Then
        frmCalendar.Calendar.Value = Me.txtFecDesde
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
    
End Sub

Private Sub cmdCalFecApeHasta_Click()
        'Muestra el formulario del calendario
    
    Set frmCalendar.frmDestination = frmLstInfVol
    Set frmCalendar.ctrDestination = txtFecHasta
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecHasta <> "" Then
        frmCalendar.Calendar.Value = txtFecHasta
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub

Private Sub cmdObtener_Click()
    'Comprueba que se han introducido todos los par�metros
    If txtFecDesde = "" Then
        basMensajes.NoValida sIdiFecDesde
        Exit Sub
    End If
    
    If txtFecHasta = "" Then
        basMensajes.NoValida sIdiFecHasta
        Exit Sub
    End If

    If CDate(txtFecHasta.Text) < CDate(txtFecDesde.Text) Then
        basMensajes.FechaHastaMenor
        txtFecHasta.SetFocus
        Exit Sub
    End If
    
    If bExcel Then
        '''Obtiene la hoja excel
        ObtenerHojaExcel
    Else
        '''Obtiene el informe
        ObtenerInforme
    End If
    
End Sub

Private Sub Form_Load()
    CargarRecursos
    
    If basParametros.g_udtParametrosGenerales.g_bUnaCompradora = True Then
        optComp.Visible = False
        optDetalle.Top = 900
    End If
    
End Sub

Private Sub txtFecDesde_Validate(Cancel As Boolean)
    If Not IsDate(txtFecDesde.Text) And Not txtFecDesde.Text = "" Then
        basMensajes.NoValida (sIdiFecDesde)
        txtFecDesde.Text = ""
        Cancel = True
    End If
End Sub

Private Sub txtFecHasta_Validate(Cancel As Boolean)
    If Not IsDate(txtFecHasta.Text) And Not txtFecHasta.Text = "" Then
        basMensajes.NoValida (sIdiFecHasta)
        txtFecHasta.Text = ""
        Cancel = True
    End If
    
End Sub


Private Sub ObtenerInforme()
    Dim oReport As CRAXDRT.Report
    Dim oCRInformes As New CRInformes
    Dim pv As Preview
    Dim RepPath As String
    Dim oFos As FileSystemObject
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    'Comprueba que el el path sea correcto y que exista el informe
    If basParametros.g_sRptPath = "" Then
        basMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    RepPath = basParametros.g_sRptPath & "\rptVolGeneral.rpt"

    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        basMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set oFos = Nothing
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "FecDesde")).Text = DateToSQLDate(Me.txtFecDesde)
    oReport.FormulaFields(crs_FormulaIndex(oReport, "FecHasta")).Text = DateToSQLDate(Me.txtFecHasta)
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TOT")).Text = """" & DenMonCentral & """"
    
    'Dependiendo de si se ha seleccionado el detalle o no se suprime la secci�n de detalle
    If Me.optDetalle.Value = False Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "Sup")).Text = "'T'"
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "Sup")).Text = "'F'"
    End If
    
    'Si se selecciona desglose por compradores
    If optComp.Value = True Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "SupComp")).Text = "'F'"
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "SupComp")).Text = "'T'"
    End If
    
    If basParametros.g_udtParametrosGenerales.g_bUnaCompradora = True Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUnaComp")).Text = "'S'"
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUnaComp")).Text = "'N'"
    End If
    
    oCRInformes.ListadoVolGeneral oReport, txtFecDesde, txtFecHasta, optCod.Value, optDen.Value, optVolPor.Value
            
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = "'" & sIdiTxtTitulo & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecDesde")).Text = "'" & sIdiFecDesde & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecHasta")).Text = "'" & sIdiFecHasta & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProve")).Text = "'" & sIdiTxtProve & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDen")).Text = "'" & sIdiTxtDen & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAdj")).Text = "'" & sIdiAdj & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtComp")).Text = "'" & sIdiComprador & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMonGS")).Text = "'" & sIdiMonGS & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAdjGS")).Text = "'" & sIdiAdjGS & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProce")).Text = "'" & sIdiProce & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecha")).Text = "'" & sIdiFec & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTotalAdj")).Text = "'" & sIdiTotAdj & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = "'" & sIdiTxtPag & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = "'" & sIdiTxtDe & "'"
    
            
            
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
   
    Set pv = New Preview
    Unload Me
    pv.Hide
    pv.Caption = Me.Caption
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    pv.crViewer.ViewReport
    pv.Show
    
    Set oReport = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub ObtenerHojaExcel()
    Dim xlApp As Object
    Dim xlBook As Object
    Dim xlSheet As Object
    Dim sCelda As String
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim orange As Object
    Dim Ador As Ador.Recordset
    Dim i As Integer
    Dim intOrden As Integer
    Dim sVersion As String
    Dim intDetalle As Integer
    Dim intDetalle2 As Integer
    Dim adorDetalle As Ador.Recordset
    Dim adorDetalle2 As Ador.Recordset
    Dim sumVol As Double
    
    On Error GoTo error
    
    'Comprueba que el el path sea correcto y que exista el informe
    
    RepPath = Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, "") & "\infAdjucic.xlt"

    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        basMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass

    'Crear aplicaci�n excel
    Set xlApp = CreateObject("Excel.Application")
    
    sVersion = xlApp.Version
    
    Set xlBook = xlApp.Workbooks.Add(RepPath)
    
    Set xlSheet = xlBook.Sheets.Item(1)
        
    'Condiciones de fechas
    sCelda = "C" & 7
    xlSheet.Range(sCelda) = Me.txtFecDesde
    sCelda = "G" & 7
    xlSheet.Range(sCelda) = Me.txtFecHasta
    
    'Orden del informe
    If Me.optCod = True Then
        intOrden = 0
    ElseIf Me.optDen Then
        intOrden = 1
    Else
        intOrden = 2
    End If
    
    'DATOS
    i = 10
    sumVol = 0
    
    Set Ador = g_oGestorInformes.DevolverVolumenAdjudicado(txtFecDesde.Text, txtFecHasta.Text, , intOrden)
    If Not Ador.EOF Then
      While Not Ador.EOF
        sCelda = "A" & i
        xlSheet.Range(sCelda) = Ador("COD")
        Set orange = xlSheet.Range("A" & i & ":" & "B" & i)
        orange.borderaround , 2
        orange.interior.Color = RGB(245, 245, 200)
        orange.mergecells = True
        
        sCelda = "C" & i
        xlSheet.Range(sCelda) = Ador("DEN")
        Set orange = xlSheet.Range("C" & i & ":" & "N" & i)
        orange.borderaround , 2
        orange.interior.Color = RGB(245, 245, 200)
        orange.mergecells = True
        
        sCelda = "P" & i
        xlSheet.Range(sCelda) = NullToDbl0(Ador("VOLPOR"))
        xlSheet.Range(sCelda).horizontalalignment = 4
        xlSheet.Range(sCelda).NumberFormat = "general"
        Set orange = xlSheet.Range("O" & i & ":" & "P" & i)
        orange.borderaround , 2
        
        'comprueba si la compa��a proveedora tiene adjudicaciones de una cia cuya
        'moneda central no est� enlazada en el portal
        If Ador("VOLPOR") <> Ador("VOLPORD") Then
            'orange.interior.Color = vbYellow
            orange.interior.Color = RGB(245, 245, 200)
            xlSheet.Range(sCelda) = ""
        Else
            orange.interior.Color = RGB(245, 245, 200)
            sumVol = sumVol + NullToDbl0(Ador("VOLPOR"))
        End If
        orange.mergecells = True
        
        
        'Detalle
        If Me.optDetalle.Value = True Or Me.optComp.Value = True Then
            'Detalle
            Set adorDetalle = g_oGestorInformes.DevolverVolumenPorProcesos(Me.txtFecDesde, Me.txtFecHasta, Ador("ID"))
            
            If Not adorDetalle.EOF Then
                sCelda = "B" & i + 1
                xlSheet.Range(sCelda) = sIdiComprador
                Set orange = xlSheet.Range("B" & i + 1 & ":" & "F" & i + 1)
                orange.borderaround , 2
                orange.Font.Bold = True
                orange.interior.Color = RGB(192, 192, 192)
                orange.mergecells = True
                
                sCelda = "G" & i + 1
                xlSheet.Range(sCelda) = sIdiMonGS
                Set orange = xlSheet.Range("G" & i + 1 & ":" & "L" & i + 1)
                orange.borderaround , 2
                orange.Font.Bold = True
                orange.interior.Color = RGB(192, 192, 192)
                orange.mergecells = True
                
                sCelda = "N" & i + 1
                xlSheet.Range(sCelda) = sIdiAdjGS
                xlSheet.Range(sCelda).horizontalalignment = 4
                Set orange = xlSheet.Range("M" & i + 1 & ":" & "N" & i + 1)
                orange.borderaround , 2
                orange.Font.Bold = True
                orange.interior.Color = RGB(192, 192, 192)
                orange.mergecells = True
                
                sCelda = "P" & i + 1
                xlSheet.Range(sCelda) = sIdiAdj
                xlSheet.Range(sCelda).horizontalalignment = 4
                Set orange = xlSheet.Range("O" & i + 1 & ":" & "P" & i + 1)
                orange.borderaround , 2
                orange.Font.Bold = True
                orange.interior.Color = RGB(192, 192, 192)
                orange.mergecells = True
                
                intDetalle = i + 2
                While Not adorDetalle.EOF
                    sCelda = "B" & intDetalle
                    xlSheet.Range(sCelda) = adorDetalle("COD") & "-" & adorDetalle("DEN")
                    Set orange = xlSheet.Range("B" & intDetalle & ":" & "F" & intDetalle)
                    orange.borderaround , 2
                    orange.interior.Color = RGB(162, 250, 158)
                    orange.mergecells = True
                    
                    sCelda = "G" & intDetalle
                    xlSheet.Range(sCelda) = adorDetalle("MGS")
                    Set orange = xlSheet.Range("G" & intDetalle & ":" & "L" & intDetalle)
                    orange.borderaround , 2
                    orange.interior.Color = RGB(162, 250, 158)
                    orange.mergecells = True
                    
                    sCelda = "N" & intDetalle
                    xlSheet.Range(sCelda) = NullToDbl0(adorDetalle("VOLGS"))
                    xlSheet.Range(sCelda).horizontalalignment = 4
                    xlSheet.Range(sCelda).NumberFormat = "general"
                    Set orange = xlSheet.Range("M" & intDetalle & ":" & "N" & intDetalle)
                    orange.borderaround , 2
                    orange.interior.Color = RGB(162, 250, 158)
                    orange.mergecells = True
                    
                    sCelda = "P" & intDetalle
                    xlSheet.Range(sCelda) = NullToDbl0(adorDetalle("VOLPOR"))
                    xlSheet.Range(sCelda).horizontalalignment = 4
                    xlSheet.Range(sCelda).NumberFormat = "general"
                    Set orange = xlSheet.Range("O" & intDetalle & ":" & "P" & intDetalle)
                    orange.borderaround , 2
                    orange.interior.Color = RGB(162, 250, 158)
                    orange.mergecells = True
                    
                    'Procesos por comprador
                    If Me.optDetalle.Value = True Then
                        Set adorDetalle2 = g_oGestorInformes.DevolverVolumenPorProcesos(Me.txtFecDesde, Me.txtFecHasta, Ador("ID"), adorDetalle("ID"))
                        intDetalle2 = intDetalle + 1
                        If Not adorDetalle2.EOF Then
                            'Cabecera
                            sCelda = "C" & intDetalle2
                            xlSheet.Range(sCelda) = sIdiProce
                            Set orange = xlSheet.Range("C" & intDetalle2 & ":" & "F" & intDetalle2)
                            orange.borderaround , 2
                            orange.Font.Bold = True
                            orange.interior.Color = RGB(192, 192, 192)
                            orange.mergecells = True
                            
                            sCelda = "G" & intDetalle2
                            xlSheet.Range(sCelda) = sIdiFec
                            Set orange = xlSheet.Range("G" & intDetalle2 & ":" & "L" & intDetalle2)
                            orange.borderaround , 2
                            orange.Font.Bold = True
                            orange.interior.Color = RGB(192, 192, 192)
                            orange.mergecells = True
                            
                            sCelda = "N" & intDetalle2
                            xlSheet.Range(sCelda) = sIdiAdjGS
                            xlSheet.Range(sCelda).horizontalalignment = 4
                            Set orange = xlSheet.Range("M" & intDetalle2 & ":" & "N" & intDetalle2)
                            orange.borderaround , 2
                            orange.Font.Bold = True
                            orange.interior.Color = RGB(192, 192, 192)
                            orange.mergecells = True
                            
                            sCelda = "P" & intDetalle2
                            xlSheet.Range(sCelda) = sIdiAdj
                            xlSheet.Range(sCelda).horizontalalignment = 4
                            Set orange = xlSheet.Range("O" & intDetalle2 & ":" & "P" & intDetalle2)
                            orange.borderaround , 2
                            orange.Font.Bold = True
                            orange.interior.Color = RGB(192, 192, 192)
                            orange.mergecells = True
                            
                            intDetalle2 = intDetalle2 + 1
                            
                            While Not adorDetalle2.EOF
                                sCelda = "c" & intDetalle2
                                xlSheet.Range(sCelda) = adorDetalle2("ANYO") & "-" & adorDetalle2("GMN1") & "-" & adorDetalle2("PROCE") & "  " & adorDetalle2("DENPROCE")
                                Set orange = xlSheet.Range("c" & intDetalle2 & ":" & "F" & intDetalle2)
                                orange.borderaround , 2
                                orange.interior.Color = RGB(0, 255, 255)
                                orange.mergecells = True
                                
                                sCelda = "G" & intDetalle2
                                xlSheet.Range(sCelda) = adorDetalle2("FECADJ")
                                xlSheet.Range(sCelda).NumberFormat = "dd/mm/yyyy"
                                xlSheet.Range(sCelda).horizontalalignment = 2
                                Set orange = xlSheet.Range("G" & intDetalle2 & ":" & "L" & intDetalle2)
                                orange.borderaround , 2
                                orange.interior.Color = RGB(0, 255, 255)
                                orange.mergecells = True
                                
                                sCelda = "N" & intDetalle2
                                xlSheet.Range(sCelda) = NullToDbl0(adorDetalle2("VOLGS"))
                                xlSheet.Range(sCelda).horizontalalignment = 4
                                xlSheet.Range(sCelda).NumberFormat = "general"
                                Set orange = xlSheet.Range("M" & intDetalle2 & ":" & "N" & intDetalle2)
                                orange.borderaround , 2
                                orange.interior.Color = RGB(0, 255, 255)
                                orange.mergecells = True
                                
                                sCelda = "P" & intDetalle2
                                xlSheet.Range(sCelda) = NullToDbl0(adorDetalle2("VOLPOR"))
                                xlSheet.Range(sCelda).horizontalalignment = 4
                                xlSheet.Range(sCelda).NumberFormat = "general"
                                Set orange = xlSheet.Range("O" & intDetalle2 & ":" & "P" & intDetalle2)
                                orange.borderaround , 2
                                orange.interior.Color = RGB(0, 255, 255)
                                orange.mergecells = True
                                
                                intDetalle2 = intDetalle2 + 1
                                adorDetalle2.MoveNext
                            Wend
                            
                        End If
                        
                        Set adorDetalle2 = Nothing
                        intDetalle = intDetalle2
                    Else
                        intDetalle = intDetalle + 1
                    End If
                    
                    
                    adorDetalle.MoveNext
                Wend
                
            End If
            Set adorDetalle = Nothing
        End If
        
        Ador.MoveNext
        
        If Me.optDetalle.Value = False And Me.optComp = False Then
            i = i + 1
        Else
            i = intDetalle
        End If
      Wend
      
      'TOTALES
      sCelda = "A" & i + 1
      xlSheet.Range(sCelda) = sIdiTotAdj & " (" & DenMonCentral & ") : "
      Set orange = xlSheet.Range("A" & i + 1 & ":" & "N" & i + 1)
      orange.borderaround , 2
      orange.Font.Bold = True
      orange.interior.Color = RGB(192, 192, 192)
      orange.mergecells = True
      
      sCelda = "P" & i + 1
      xlSheet.Range(sCelda) = sumVol
      xlSheet.Range(sCelda).horizontalalignment = 4
      xlSheet.Range(sCelda).NumberFormat = "general"
      Set orange = xlSheet.Range("O" & i + 1 & ":" & "P" & i + 1)
      orange.borderaround , 2
      orange.Font.Bold = True
      orange.interior.Color = RGB(192, 192, 192)
      orange.mergecells = True
      
    End If
    Set Ador = Nothing
    
    Unload Me
    'Hace visible la hoja excel
    xlApp.Visible = True

    Screen.MousePointer = vbNormal
    
    Exit Sub
    
error:
    Screen.MousePointer = vbNormal
    'Cierro los objetos
    Set xlSheet = Nothing
    Set xlBook = Nothing
    xlApp.Quit
    Set xlApp = Nothing
End Sub



Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTINFVOL, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        sIdiTxtTitulo = Ador(0).Value
        Ador.MoveNext
        Me.cmdObtener.Caption = Ador(0).Value
        Ador.MoveNext
        Me.lblFecDesde.Caption = Ador(0).Value
        sIdiTxtFecDesde = Ador(0).Value
        Ador.MoveNext
        
        Me.lblFecHasta.Caption = Ador(0).Value
        sIdiTxtFecHasta = Ador(0).Value
        Ador.MoveNext
        
        Me.optCod.Caption = Ador(0).Value
        Ador.MoveNext
        Me.optComp.Caption = Ador(0).Value
        Ador.MoveNext
        Me.optDen.Caption = Ador(0).Value
        Ador.MoveNext
        Me.optDetalle.Caption = Ador(0).Value
        Ador.MoveNext
        Me.optSoloComp.Caption = Ador(0).Value
        Ador.MoveNext
        Me.optVolPor.Caption = Ador(0).Value
        Ador.MoveNext
        
        Me.ssTabListado.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        Me.ssTabListado.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        Me.ssTabListado.TabCaption(2) = Ador(0).Value
        Ador.MoveNext
        
        sIdiFecDesde = Ador(0).Value
        Ador.MoveNext
        sIdiFecHasta = Ador(0).Value
        Ador.MoveNext
        sIdiComprador = Ador(0).Value
        Ador.MoveNext
        sIdiMonGS = Ador(0).Value
        Ador.MoveNext
        sIdiAdjGS = Ador(0).Value
        Ador.MoveNext
        sIdiAdj = Ador(0).Value
        Ador.MoveNext
        sIdiProce = Ador(0).Value
        Ador.MoveNext
        sIdiFec = Ador(0).Value
        Ador.MoveNext
        sIdiTotAdj = Ador(0).Value
        Ador.MoveNext
        sIdiTxtProve = Ador(0).Value
        Ador.MoveNext
        sIdiTxtDen = Ador(0).Value
        Ador.MoveNext
        sIdiTxtPag = Ador(0).Value
        Ador.MoveNext
        sIdiTxtDe = Ador(0).Value
        
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub




