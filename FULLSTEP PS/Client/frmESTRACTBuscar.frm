VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmESTRACTBuscar 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Buscar material"
   ClientHeight    =   3720
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6825
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmESTRACTBuscar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3720
   ScaleWidth      =   6825
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdCargar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6390
      Picture         =   "frmESTRACTBuscar.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Cargar"
      Top             =   285
      Width           =   315
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cerrar"
      Height          =   315
      Left            =   3465
      TabIndex        =   4
      Top             =   3345
      Width           =   1290
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      Height          =   315
      Left            =   1905
      TabIndex        =   3
      Top             =   3345
      Width           =   1290
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   90
      ScaleHeight     =   585
      ScaleWidth      =   6105
      TabIndex        =   5
      Top             =   90
      Width           =   6165
      Begin VB.TextBox txtDen 
         Height          =   285
         Left            =   3300
         MaxLength       =   150
         TabIndex        =   1
         Top             =   120
         Width           =   2745
      End
      Begin VB.TextBox txtCod 
         Height          =   285
         Left            =   780
         MaxLength       =   50
         TabIndex        =   0
         Top             =   120
         Width           =   1155
      End
      Begin VB.Label lblCod 
         BackColor       =   &H00808000&
         Caption         =   "Código:"
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   45
         TabIndex        =   7
         Top             =   180
         Width           =   705
      End
      Begin VB.Label lblDen 
         BackColor       =   &H00808000&
         Caption         =   "Denominación:"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   2025
         TabIndex        =   6
         Top             =   180
         Width           =   1365
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgAct 
      Height          =   2220
      Left            =   120
      TabIndex        =   2
      Top             =   1035
      Width           =   6570
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   11
      stylesets.count =   2
      stylesets(0).Name=   "ActiveRow"
      stylesets(0).ForeColor=   16777215
      stylesets(0).BackColor=   8421376
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmESTRACTBuscar.frx":048C
      stylesets(1).Name=   "ActiveRowBlue"
      stylesets(1).ForeColor=   16777215
      stylesets(1).BackColor=   8388608
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmESTRACTBuscar.frx":04A8
      AllowUpdate     =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "ActiveRowBlue"
      Columns.Count   =   11
      Columns(0).Width=   1270
      Columns(0).Caption=   "ACT1"
      Columns(0).Name =   "ACT1"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16449500
      Columns(1).Width=   1058
      Columns(1).Caption=   "ACT2"
      Columns(1).Name =   "ACT2"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16449500
      Columns(2).Width=   1164
      Columns(2).Caption=   "ACT3"
      Columns(2).Name =   "ACT3"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   16449500
      Columns(3).Width=   1085
      Columns(3).Caption=   "ACT4"
      Columns(3).Name =   "ACT4"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   16449500
      Columns(4).Width=   1667
      Columns(4).Caption=   "ACT5"
      Columns(4).Name =   "ACT5"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   16449500
      Columns(5).Width=   4974
      Columns(5).Caption=   "Denominación"
      Columns(5).Name =   "DEN"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "ID"
      Columns(6).Name =   "ID1"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "ID2"
      Columns(7).Name =   "ID2"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "ID3"
      Columns(8).Name =   "ID3"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "ID4"
      Columns(9).Name =   "ID4"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "ID5"
      Columns(10).Name=   "ID5"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      _ExtentX        =   11589
      _ExtentY        =   3916
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmESTRACTBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public m_oACNs1Seleccionado As CActividadesNivel1
Public m_oACNs2Seleccionado As CActividadesNivel2
Public m_oACNs3Seleccionado As CActividadesNivel3
Public m_oACNs4Seleccionado As CActividadesNivel4
Public m_oACNs5Seleccionado As CActividadesNivel5



Private Sub cmdAceptar_Click()
Dim nodx As MSComctlLib.Node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String
Dim i As Integer
Dim j As Integer
On Error GoTo NoSeEncuentra

If Trim(sdbgAct.Columns(4).Value) <> "" Then
    j = 5
Else
    If Trim(sdbgAct.Columns(3).Value) <> "" Then
        j = 4
    Else
        If Trim(sdbgAct.Columns(2).Value) <> "" Then
            j = 3
        Else
            If Trim(sdbgAct.Columns(1).Value) <> "" Then
                j = 2
            Else
                If Trim(sdbgAct.Columns(0).Value) <> "" Then
                    j = 1
                End If
            End If
        End If
    End If
End If


Select Case j

Case 1
    
    scod1 = sdbgAct.Columns("ID1").Value & Mid$("                         ", 1, 10 - Len(sdbgAct.Columns("ID1").Value))
    Set nodx = frmActiv.tvwEstrAct.Nodes("ACN1" & scod1)
    nodx.Selected = True
    nodx.EnsureVisible
    frmActiv.ConfigurarInterfazSeguridad nodx
    'frmActiv.tvwEstrAct.SetFocus
Case 2
    
    scod1 = sdbgAct.Columns("ID1").Value & Mid$("                         ", 1, 10 - Len(sdbgAct.Columns("ID1").Value))
    scod2 = sdbgAct.Columns("ID2").Value & Mid$("                         ", 1, 10 - Len(sdbgAct.Columns("ID2").Value))
    Set nodx = frmActiv.tvwEstrAct.Nodes("ACN2" & scod1 & scod2)
    nodx.Selected = True
    nodx.EnsureVisible
    frmActiv.ConfigurarInterfazSeguridad nodx
    'frmActiv.tvwEstrAct.SetFocus
Case 3
    
    scod1 = sdbgAct.Columns("ID1").Value & Mid$("                         ", 1, 10 - Len(sdbgAct.Columns("ID1").Value))
    scod2 = sdbgAct.Columns("ID2").Value & Mid$("                         ", 1, 10 - Len(sdbgAct.Columns("ID2").Value))
    scod3 = sdbgAct.Columns("ID3").Value & Mid$("                         ", 1, 10 - Len(sdbgAct.Columns("ID3").Value))
    
    Set nodx = frmActiv.tvwEstrAct.Nodes("ACN3" & scod1 & scod2 & scod3)
    nodx.Selected = True
    nodx.EnsureVisible
    frmActiv.ConfigurarInterfazSeguridad nodx
    'frmActiv.tvwEstrAct.SetFocus

Case 4
    
    scod1 = sdbgAct.Columns("ID1").Value & Mid$("                         ", 1, 10 - Len(sdbgAct.Columns("ID1").Value))
    scod2 = sdbgAct.Columns("ID2").Value & Mid$("                         ", 1, 10 - Len(sdbgAct.Columns("ID2").Value))
    scod3 = sdbgAct.Columns("ID3").Value & Mid$("                         ", 1, 10 - Len(sdbgAct.Columns("ID3").Value))
    scod4 = sdbgAct.Columns("ID4").Value & Mid$("                         ", 1, 10 - Len(sdbgAct.Columns("ID4").Value))
    Set nodx = frmActiv.tvwEstrAct.Nodes("ACN4" & scod1 & scod2 & scod3 & scod4)
    nodx.Selected = True
    nodx.EnsureVisible
    frmActiv.ConfigurarInterfazSeguridad nodx
    
Case 5
    scod1 = sdbgAct.Columns("ID1").Value & Mid$("                         ", 1, 10 - Len(sdbgAct.Columns("ID1").Value))
    scod2 = sdbgAct.Columns("ID2").Value & Mid$("                         ", 1, 10 - Len(sdbgAct.Columns("ID2").Value))
    scod3 = sdbgAct.Columns("ID3").Value & Mid$("                         ", 1, 10 - Len(sdbgAct.Columns("ID3").Value))
    scod4 = sdbgAct.Columns("ID4").Value & Mid$("                         ", 1, 10 - Len(sdbgAct.Columns("ID4").Value))
    scod5 = sdbgAct.Columns("ID5").Value & Mid$("                         ", 1, 10 - Len(sdbgAct.Columns("ID5").Value))

    Set nodx = frmActiv.tvwEstrAct.Nodes("ACN5" & scod1 & scod2 & scod3 & scod4 & scod5)
    nodx.Selected = True
    nodx.EnsureVisible
    frmActiv.ConfigurarInterfazSeguridad nodx
       
End Select

Unload Me

frmActiv.tvwEstrAct.SetFocus

Exit Sub

NoSeEncuentra:
    'basMensajes.n
    Unload Me
    
End Sub

Private Sub cmdCancelar_Click()
Unload Me
End Sub

Private Sub cmdCargar_Click()
Dim Adors As Ador.Recordset

Screen.MousePointer = vbHourglass

sdbgAct.RemoveAll

       Set m_oACNs5Seleccionado = g_oRaiz.Generar_CActividadesNivel5
   
       Set m_oACNs4Seleccionado = g_oRaiz.Generar_CActividadesNivel4
    
       Set m_oACNs3Seleccionado = g_oRaiz.Generar_CActividadesNivel3
      
       Set m_oACNs2Seleccionado = g_oRaiz.Generar_CActividadesNivel2

       Set m_oACNs1Seleccionado = g_oRaiz.Generar_CActividadesNivel1
    
        
       Set Adors = m_oACNs1Seleccionado.DevolverTodasLasActividades(Trim(frmActiv.sdbcIdiCod.Text), Trim(txtCod.Text), Trim(txtDen.Text), False, False, False)
       CargarGrid "ACN1", Adors
       Set Adors = m_oACNs2Seleccionado.DevolverTodasLasActividades(Trim(frmActiv.sdbcIdiCod.Text), Trim(txtCod), Trim(txtDen), False, False, False)
       CargarGrid "ACN2", Adors
       Set Adors = m_oACNs3Seleccionado.DevolverTodasLasActividades(Trim(frmActiv.sdbcIdiCod.Text), Trim(txtCod), Trim(txtDen), False, False, False)
       CargarGrid "ACN3", Adors
       Set Adors = m_oACNs4Seleccionado.DevolverTodasLasActividades(Trim(frmActiv.sdbcIdiCod.Text), Trim(txtCod), Trim(txtDen), False, False, False)
       CargarGrid "ACN4", Adors
       Set Adors = m_oACNs5Seleccionado.DevolverTodasLasActividades(Trim(frmActiv.sdbcIdiCod.Text), Trim(txtCod), Trim(txtDen), False, False, False)
       CargarGrid "ACN5", Adors
       
       Set Adors = Nothing

Screen.MousePointer = vbNormal
    
End Sub

Private Sub Form_Load()

    Me.Left = frmMDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = frmMDI.ScaleHeight / 2 - Me.Height / 2
    CargarRecursos
End Sub

Private Sub CargarGrid(ByVal opcion As String, ByVal Adors As Ador.Recordset)
Dim oACN1 As CActividadNivel1
Dim oACN2 As CActividadNivel2
Dim oACN3 As CActividadNivel3
Dim oACN4 As CActividadNivel4
Dim oACN5 As CActividadNivel5

'sdbgAct.RemoveAll

Select Case opcion
    
    Case "ACN1"
        
        If Adors Is Nothing Then Exit Sub
        While Not Adors.EOF
           
           sdbgAct.AddItem Adors(1).Value & "" & Chr(9) & "" & Chr(9) & "" & Chr(9) & "" & Chr(9) & Chr(9) & Adors(2).Value & Chr(9) & Adors(0).Value
           Adors.MoveNext
        Wend
        
        Set m_oACNs1Seleccionado = Nothing
        Adors.Close
        
    Case "ACN2"
        
        If Adors Is Nothing Then Exit Sub
        While Not Adors.EOF
            sdbgAct.AddItem Adors(3).Value & Chr(9) & Adors(1).Value & Chr(9) & "" & Chr(9) & "" & Chr(9) & "" & Chr(9) & Adors(2).Value & Chr(9) & Adors(4).Value & Chr(9) & Adors(0).Value
            Adors.MoveNext
        Wend
        
        Set m_oACNs2Seleccionado = Nothing
        
    Case "ACN3"
        
        If Adors Is Nothing Then Exit Sub
        While Not Adors.EOF
            sdbgAct.AddItem Adors(3).Value & Chr(9) & Adors(4).Value & Chr(9) & Adors(1).Value & Chr(9) & "" & Chr(9) & "" & Chr(9) & Adors(2).Value & Chr(9) & Adors(5).Value & Chr(9) & Adors(6).Value & Chr(9) & Adors(0).Value
            Adors.MoveNext
        Wend
        
        Set m_oACNs3Seleccionado = Nothing
        
    Case "ACN4"
        
        If Adors Is Nothing Then Exit Sub
        While Not Adors.EOF
            sdbgAct.AddItem Adors(3).Value & Chr(9) & Adors(4).Value & Chr(9) & Adors(5).Value & Chr(9) & Adors(1).Value & Chr(9) & "" & Chr(9) & Adors(2).Value & Chr(9) & Adors(6).Value & Chr(9) & Adors(7).Value & Chr(9) & Adors(8).Value & Chr(9) & Adors(0).Value
            Adors.MoveNext
        Wend
        
        Set m_oACNs4Seleccionado = Nothing
        
    Case "ACN5"
        
        If Adors Is Nothing Then Exit Sub
        While Not Adors.EOF
            sdbgAct.AddItem Adors(3).Value & Chr(9) & Adors(4).Value & Chr(9) & Adors(5).Value & Chr(9) & Adors(6).Value & Chr(9) & Adors(1).Value & Chr(9) & Adors(2).Value & Chr(9) & Adors(7).Value & Chr(9) & Adors(8).Value & Chr(9) & Adors(9).Value & Chr(9) & Adors(10) & Chr(9) & Adors(0).Value & Chr(9) & Adors(0).Value
            Adors.MoveNext
        Wend
        
        Set m_oACNs5Seleccionado = Nothing

End Select


End Sub


Private Sub Form_Unload(Cancel As Integer)
    
    Set m_oACNs1Seleccionado = Nothing
    Set m_oACNs2Seleccionado = Nothing
    Set m_oACNs3Seleccionado = Nothing
    Set m_oACNs4Seleccionado = Nothing
    Set m_oACNs4Seleccionado = Nothing

End Sub


Private Sub sdbgAct_DblClick()
    
    cmdAceptar_Click
    
End Sub



Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTRACTBUSCAR, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.lblCod.Caption = Ador(0).Value
        Ador.MoveNext
        Me.lblDen.Caption = Ador(0).Value & ":"
        Me.sdbgAct.Columns(5).Caption = Ador(0).Value
        

        Ador.MoveNext
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub


