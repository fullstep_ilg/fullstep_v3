VERSION 5.00
Begin VB.Form frmCambContr 
   BackColor       =   &H00808000&
   Caption         =   "Cambio de Contrase�a"
   ClientHeight    =   2925
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4575
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCambContr.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2925
   ScaleWidth      =   4575
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picADM 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1600
      Left            =   75
      ScaleHeight     =   1545
      ScaleWidth      =   4395
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   760
      Width           =   4455
      Begin VB.TextBox txtAContrActual 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2175
         MaxLength       =   20
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   105
         Width           =   2100
      End
      Begin VB.TextBox txtAContrNueva 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2175
         MaxLength       =   20
         PasswordChar    =   "*"
         TabIndex        =   2
         Top             =   660
         Width           =   2100
      End
      Begin VB.TextBox txtAContrNuevaConf 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2175
         MaxLength       =   20
         PasswordChar    =   "*"
         TabIndex        =   3
         Top             =   1080
         Width           =   2100
      End
      Begin VB.Label lblPwdAct 
         BackColor       =   &H00808000&
         Caption         =   "Contrase�a actual:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   165
         Width           =   2100
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00FFFFFF&
         BorderWidth     =   2
         X1              =   75
         X2              =   4215
         Y1              =   540
         Y2              =   540
      End
      Begin VB.Label lblPwdNue 
         BackColor       =   &H00808000&
         Caption         =   "Nueva contrase�a:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   720
         Width           =   2100
      End
      Begin VB.Label lblConf 
         BackColor       =   &H00808000&
         Caption         =   "Confirmaci�n:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   1140
         Width           =   2100
      End
   End
   Begin VB.PictureBox picButtons 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   4575
      TabIndex        =   0
      Top             =   2430
      Width           =   4575
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   315
         Left            =   960
         TabIndex        =   4
         Top             =   60
         Width           =   1155
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   315
         Left            =   2220
         TabIndex        =   5
         Top             =   60
         Width           =   1215
      End
      Begin VB.Label lblPWDNovalida 
         Caption         =   "Label1"
         Height          =   135
         Index           =   4
         Left            =   0
         TabIndex        =   15
         Top             =   0
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.Label lblPWDNovalida 
         Caption         =   "Label1"
         Height          =   135
         Index           =   3
         Left            =   0
         TabIndex        =   14
         Top             =   0
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.Label lblPWDNovalida 
         Caption         =   "Label1"
         Height          =   135
         Index           =   2
         Left            =   0
         TabIndex        =   13
         Top             =   0
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.Label lblPWDNovalida 
         Caption         =   "Label1"
         Height          =   135
         Index           =   1
         Left            =   0
         TabIndex        =   12
         Top             =   0
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.Label lblPWDNovalida 
         Caption         =   "Label1"
         Height          =   135
         Index           =   0
         Left            =   3840
         TabIndex        =   11
         Top             =   120
         Visible         =   0   'False
         Width           =   375
      End
   End
   Begin VB.Label lblPasswordChange 
      BackColor       =   &H00808000&
      Caption         =   "Label1"
      ForeColor       =   &H00FFFFFF&
      Height          =   600
      Left            =   75
      TabIndex        =   10
      Top             =   120
      Width           =   4455
   End
End
Attribute VB_Name = "frmCambContr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private sPwdAct As String
Private sConfPwd As String
Private m_sPassUsu As String
Private m_sUsuActual As String
Private m_oUsuario As CUsuario
Public b_PasswordCaducada As Boolean
Private UsuImpersonate As String
Private PWDImpersonate As String
Private DominioImpersonate As String
Private URLPWDChange As String
Private Encrypt_One_Way As Boolean

''' <summary>
''' Acepta un cambio de contrase�a
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    Dim adors As ador.Recordset

    Dim dFecPWD, dNewFecusu As Date
    Dim usuNew, newpwd, usuCod, fecpwd, salt As String
        
    Dim oUsuario As CUsuario
    Set oUsuario = g_oRaiz.Generar_CUsuario
  
    If oUsuario.ValidarUsuario(basPublic.g_sCodADM, Me.txtAContrActual.Text) = 1 Then
        basMensajes.UsuarioNoAutorizado
        Set oUsuario = Nothing
        Exit Sub
    End If

    ''' Comprobaci�n de igualdad
    If Trim(txtAContrNueva.Text) = "" Then
        basMensajes.NoValido lblPwdNue.Caption
        txtAContrNueva.SetFocus
        Exit Sub
    End If
    
    If Trim(txtAContrNuevaConf.Text) = "" Then
        basMensajes.NoValido lblConf.Caption
        txtAContrNuevaConf.SetFocus
        Exit Sub
    End If
    
    If txtAContrNueva.Text <> txtAContrNuevaConf.Text Then
        basMensajes.NoValida lblPwdNue.Caption
        Exit Sub
    End If
    
    Dim PasswordChangeService As PasswordPolicy
    Set PasswordChangeService = New PasswordPolicy
    
    With PasswordChangeService
        .URLServicio = URLPWDChange
        .UsuImpersonate = UsuImpersonate
        .PWDImpersonate = PWDImpersonate
        .DominioImpersonate = DominioImpersonate
        
        .usuCod = basPublic.g_sCodADM
        .fecpwd = Date
        .UsuNewPWD = txtAContrNueva.Text
        
        Select Case .ValidarPasswordPoliticaSeguridad.TipoMensaje
            Case 0
                teserror.NumError = TESnoerror
                teserror.Arg1 = .Pwd
                If Encrypt_One_Way Then
                    teserror.Arg2 = .salt
                Else
                    teserror.Arg2 = .fecpwd
                End If
            Case 1
                teserror.NumError = TESErrorPWDCorta
                basMensajes.NoValida lblPWDNovalida(0).Caption
            Case 2
                teserror.NumError = TESErrorPWDEdadMin
                basMensajes.NoValida lblPWDNovalida(1).Caption
            Case 3
                teserror.NumError = TESErrorPWDCondicionesComplejidad
                basMensajes.NoValida lblPWDNovalida(2).Caption
            Case 4
                teserror.NumError = TESErrorPWDHistorico
                basMensajes.NoValida lblPWDNovalida(3).Caption
            Case Else
                teserror.NumError = TESErrorErrorServicio
                basMensajes.FalloEnConexionConServidor
        End Select
    End With

    If teserror.NumError = TESnoerror Then
        dNewFecusu = Date
               
        Screen.MousePointer = vbHourglass
        Set m_oUsuario = g_oRaiz.Generar_CUsuario
        teserror = m_oUsuario.CambiarLogin(basPublic.g_sCodADM, basPublic.g_sCodADM, teserror.Arg1, IIf(Encrypt_One_Way, Nothing, teserror.Arg2), IIf(Encrypt_One_Way, teserror.Arg2, ""))
        Screen.MousePointer = vbNormal
                
        If teserror.NumError = TESnoerror Then
            g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Cambio_Pwd, g_sLitRegAccion_SPA(5), g_sLitRegAccion_ENG(5), g_sLitRegAccion_FRA(5), g_sLitRegAccion_GER(5)
            basMensajes.DatosActualizadosCorrectamente
            b_PasswordCaducada = False
            Unload Me
        Else
            basMensajes.NoValido usuCod
        End If
    End If
End Sub

Private Sub cmdCancelar_Click()
    If b_PasswordCaducada Then
        End
    Else
        Unload Me
    End If
End Sub

Private Sub Form_Load()
    CargarRecursos
    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarRecursos()
    Dim ador As ador.Recordset
    On Error Resume Next
    Dim Historico_PWD, MinSize_PWD As Integer
    
    Dim oUsuario As CUsuario
    Set oUsuario = g_oRaiz.Generar_CUsuario
    Set ador = oUsuario.leer_parametrosgenerales
    UsuImpersonate = DesEncriptar(ador.Fields("USUIMPERSONATE").Value)
    PWDImpersonate = DesEncriptar(ador.Fields("PWDIMPERSONATE").Value)
    DominioImpersonate = DesEncriptar(ador.Fields("DOMINIOIMPERSONATE").Value)
    URLPWDChange = ador.Fields("URLPWDCHANGEWCF").Value
    Encrypt_One_Way = CBool(ador.Fields("ENCRYPT_ONE_WAY").Value)
    
    MinSize_PWD = ador.Fields("MIN_SIZE_PWD").Value
    Historico_PWD = ador.Fields("HISTORICO_PWD").Value
    
    ador.Close
    Set ador = Nothing
        
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CAMBCONTR, g_udtParametrosGenerales.g_sIdioma)
    If Not ador Is Nothing Then
        Me.cmdAceptar.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdCancelar.Caption = ador(0).Value
        ador.MoveNext
        Me.Caption = ador(0).Value
        ador.MoveNext
        Me.lblConf.Caption = ador(0).Value
        ador.MoveNext
        Me.lblPwdAct = ador(0).Value
        ador.MoveNext
        Me.lblPwdNue = ador(0).Value
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        Dim texto As String
        texto = ador(0).Value & vbCrLf
        ador.MoveNext
        texto = texto & ador(0).Value
        Me.lblPasswordChange.Caption = texto
        ador.MoveNext
        lblPWDNovalida(0).Caption = ador(0).Value
        ador.MoveNext
        lblPWDNovalida(1).Caption = ador(0).Value
        ador.MoveNext
        lblPWDNovalida(2).Caption = ador(0).Value & vbCrLf
        ador.MoveNext
        lblPWDNovalida(2).Caption = lblPWDNovalida(2).Caption & Replace(ador(0).Value, "###", MinSize_PWD) & vbCrLf
        ador.MoveNext
        lblPWDNovalida(2).Caption = lblPWDNovalida(2).Caption & ador(0).Value & vbCrLf
        ador.MoveNext
        lblPWDNovalida(2).Caption = lblPWDNovalida(2).Caption & ador(0).Value
        ador.MoveNext
        lblPWDNovalida(3).Caption = ador(0).Value & vbCrLf
        ador.MoveNext
        lblPWDNovalida(3).Caption = lblPWDNovalida(3).Caption & Replace(ador(0).Value, "###", Historico_PWD)
        ador.MoveNext
        lblPWDNovalida(4).Caption = ador(0).Value

        ador.Close
    End If
    If b_PasswordCaducada Then
        Me.Height = 3500
        picADM.Top = 760
    Else
        Me.Height = 2800
        picADM.Top = 60
    End If
    Set ador = Nothing
End Sub


Private Sub Form_Unload(Cancel As Integer)
    If b_PasswordCaducada Then
        End
    End If
End Sub

