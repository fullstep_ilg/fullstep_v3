VERSION 5.00
Begin VB.Form frmInitial 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Inicio de sesi�n"
   ClientHeight    =   3270
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4950
   ClipControls    =   0   'False
   Icon            =   "frmInitial.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3270
   ScaleWidth      =   4950
   StartUpPosition =   2  'CenterScreen
End
Attribute VB_Name = "frmInitial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Me.Width = frmLogin.Width
    Me.Height = frmLogin.Height
    CargarRecursos
End Sub



Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_INITIAL, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub

