VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmMON 
   Caption         =   "Monedas"
   ClientHeight    =   4200
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12945
   Icon            =   "frmMON.frx":0000
   LinkTopic       =   "frmMON2"
   MDIChild        =   -1  'True
   ScaleHeight     =   4200
   ScaleWidth      =   12945
   Begin SSDataWidgets_B.SSDBGrid sdbgMonedas 
      Height          =   3540
      Left            =   60
      TabIndex        =   4
      Top             =   60
      Width           =   12825
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   6
      stylesets.count =   1
      stylesets(0).Name=   "EqNoAct"
      stylesets(0).BackColor=   12648447
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmMON.frx":014A
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      BalloonHelp     =   0   'False
      MaxSelectedRows =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   185
      Columns.Count   =   6
      Columns(0).Width=   1323
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   3
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777152
      Columns(1).Width=   5662
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN1"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   50
      Columns(2).Width=   5662
      Columns(2).Caption=   "DEN2"
      Columns(2).Name =   "DEN2"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   5662
      Columns(3).Caption=   "DEN3"
      Columns(3).Name =   "DEN3"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3175
      Columns(4).Caption=   "Equivalencia"
      Columns(4).Name =   "EQUIV"
      Columns(4).Alignment=   1
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "Standard"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "ID"
      Columns(5).Name =   "ID"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   22622
      _ExtentY        =   6244
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   12945
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   3645
      Width           =   12945
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         Height          =   345
         Left            =   1320
         TabIndex        =   6
         Top             =   120
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdCodigo 
         Caption         =   "&C�digo"
         Height          =   345
         Left            =   3720
         TabIndex        =   8
         Top             =   105
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdlistado 
         Caption         =   "Listado"
         Height          =   345
         Left            =   1320
         TabIndex        =   7
         Top             =   120
         Width           =   1005
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   120
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   120
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2520
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   120
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   5355
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   105
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   120
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmMON"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Const LONGCOD = 3

''' Coleccion de monedas
Public m_oMonedas As CMonedas

''' Moneda en edicion
Private m_oMonedaEnEdicion As CMoneda

''' Control de errores
Private m_bModError As Boolean
Private m_bAnyaError As Boolean
Private m_bValError As Boolean

''' Posicion para UnboundReadData
Private m_lp As Long

Private m_bModoEdicion As Boolean
Private m_bAnyadir As Boolean
Private m_bCriterioCoincidenciaTotal  As Boolean
Public g_sCodigoNuevo As String

Private sIdiMonCod As String
Private sIdiMonEdicion As String
Private sIdiConsulta As String
Private sIdiMonConsulta As String
Private sIdiEdicion As String
Private sIdiCodigo As String
Private sIdiEquiv As String
Private sIdiOrdenando As String
Private sIdiLaMoneda As String

Private m_oIdiomas As CIdiomas


Private Sub Arrange()
    
    ''' * Objetivo: Ajustar el tama�o y posicion de los controles
    ''' * Objetivo: al tama�o del formulario
    
    If Height >= 1200 Then sdbgMonedas.Height = Height - 975
    If Width >= 250 Then sdbgMonedas.Width = Width - 250
    
    'sdbgMonedas.Columns(0).Width = sdbgMonedas.Width * 16 / 100
    'sdbgMonedas.Columns(1).Width = sdbgMonedas.Width * 52 / 100
    'sdbgMonedas.Columns(2).Width = sdbgMonedas.Width * 23 / 100
    sdbgMonedas.Columns(0).Width = sdbgMonedas.Width * 12 / 100
    sdbgMonedas.Columns(1).Width = sdbgMonedas.Width * 22 / 100
    sdbgMonedas.Columns(2).Width = sdbgMonedas.Width * 22 / 100
    sdbgMonedas.Columns(3).Width = sdbgMonedas.Width * 22 / 100
    sdbgMonedas.Columns(4).Width = sdbgMonedas.Width * 18 / 100
    
    cmdModoEdicion.Left = sdbgMonedas.Left + sdbgMonedas.Width - cmdModoEdicion.Width
    
End Sub

Private Sub cmdA�adir_Click()
    
    ''' * Objetivo: Situarnos en la fila de adicion
    
    sdbgMonedas.Scroll 0, sdbgMonedas.Rows - sdbgMonedas.Row
    
    If sdbgMonedas.VisibleRows > 0 Then
        
        If sdbgMonedas.VisibleRows >= sdbgMonedas.Rows Then
            
            If sdbgMonedas.VisibleRows = sdbgMonedas.Rows Then
    
                sdbgMonedas.Row = sdbgMonedas.Rows - 1
    
            Else
                sdbgMonedas.Row = sdbgMonedas.Rows
    
            End If
    
        Else
            
            sdbgMonedas.Row = sdbgMonedas.Rows - (sdbgMonedas.Rows - sdbgMonedas.VisibleRows) - 1
     
       End If
        
    End If
    
    m_bAnyadir = True
    sdbgMonedas.SetFocus
    
End Sub

Private Sub cmdCodigo_Click()
Dim oIdi As CIdioma
Dim i As Integer

    If sdbgMonedas.Rows = 0 Then Exit Sub
    If IsNull(sdbgMonedas.Bookmark) Then Exit Sub
    sdbgMonedas.SelBookmarks.Add sdbgMonedas.Bookmark
    frmMODCOD.Left = frmMON.Left + 500
    frmMODCOD.Top = frmMON.Top + 1000
    frmMODCOD.Caption = sIdiMonCod
    Set m_oMonedaEnEdicion = Nothing
    Set m_oMonedaEnEdicion = g_oRaiz.Generar_CMoneda
    m_oMonedaEnEdicion.Cod = sdbgMonedas.Columns(0).Text
    
    Set m_oMonedaEnEdicion.Denominaciones = g_oRaiz.Generar_CMultiidiomas
    For Each oIdi In m_oIdiomas
        m_oMonedaEnEdicion.Denominaciones.Add oIdi.Cod, ""
    Next
        
    For i = 1 To 3
        m_oMonedaEnEdicion.Denominaciones.Item(Mid(sdbgMonedas.Columns(i).Name, 5)).Den = sdbgMonedas.Columns(sdbgMonedas.Columns(i).Name).Value
    Next
    frmMODCOD.txtCodNue.MaxLength = LONGCOD
    frmMODCOD.txtCodAct.Text = m_oMonedaEnEdicion.Cod
    frmMODCOD.g_sfrmOrigen = "frmMON"
    Set frmMODCOD.g_oMonedaEd = m_oMonedaEnEdicion
    frmMODCOD.Show 1
    sdbgMonedas.Columns(0).Text = m_oMonedaEnEdicion.Cod
    sdbgMonedas.MoveFirst
    sdbgMonedas.Refresh
    sdbgMonedas.Bookmark = sdbgMonedas.SelBookmarks(0)
    sdbgMonedas.SelBookmarks.RemoveAll
    Set m_oMonedaEnEdicion = Nothing
End Sub

Private Sub cmdDeshacer_Click()
     
     ''' * Objetivo: Deshacer la edicion en la moneda actual
    
    sdbgMonedas.CancelUpdate
    sdbgMonedas.DataChanged = False
    
    m_bAnyadir = False
    
    If Not m_oMonedaEnEdicion Is Nothing Then
        Set m_oMonedaEnEdicion = Nothing
    End If
    
    If Not sdbgMonedas.IsAddRow Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
    Else
        cmdDeshacer.Enabled = False
    End If
    
End Sub

Private Sub cmdEliminar_Click()
Dim i As Integer
Dim iRespuesta As Integer
Dim oError As CTESError
Dim bEliminar As Boolean
Dim oCias As CCias
Dim oCia As CCia

Dim sCias As String


  
If sdbgMonedas.Rows = 0 Then Exit Sub
    
    If g_udtParametrosGenerales.g_iMonedaCentral = sdbgMonedas.Columns("ID").Value Then
        basMensajes.ImposibleEliminarMonedaCentral
        Exit Sub
    End If
    
    
    
    Screen.MousePointer = vbHourglass

    iRespuesta = basMensajes.PreguntaEliminar(sIdiLaMoneda & " " & sdbgMonedas.Columns(1).Value & " (" & sdbgMonedas.Columns(0).Value & ")")
        
    If iRespuesta = vbNo Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    
        
    sdbgMonedas.SelBookmarks.Add sdbgMonedas.Bookmark
  
    Set m_oMonedaEnEdicion = m_oMonedas.Item(CStr(sdbgMonedas.Columns("ID").Value))
    
    
    bEliminar = False
    Set oCias = m_oMonedaEnEdicion.Enlazada
    If oCias.Count > 0 Then
        For Each oCia In oCias
            sCias = sCias & oCia.Den & vbCrLf
        Next
        If basMensajes.PreguntarEliminarMonedaEnlazada(sCias) = vbYes Then
            bEliminar = True
        End If
    Else
        bEliminar = True
    End If
    
    If bEliminar Then
        Set oError = m_oMonedaEnEdicion.EliminarMoneda(oCias)
    
        If oError.numerror <> TESnoerror Then
            TratarError oError
            Screen.MousePointer = vbNormal
            sdbgMonedas.SetFocus
            Exit Sub
            
        Else
            sdbgMonedas.DeleteSelected
            DoEvents
            
        End If
    End If
        
        
    Screen.MousePointer = vbNormal
    Set m_oMonedaEnEdicion = Nothing

End Sub

Private Sub cmdListado_Click()
Load frmLstMon
frmLstMon.Visible = True
End Sub

Private Sub cmdModoEdicion_Click()
    
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    
    If Not m_bModoEdicion Then
                
        sdbgMonedas.AllowAddNew = True
        sdbgMonedas.AllowUpdate = True
        
        frmMON.Caption = sIdiMonEdicion
        
        cmdModoEdicion.Caption = sIdiConsulta
        
        cmdRestaurar.Visible = False
        cmdA�adir.Visible = True
        cmdEliminar.Visible = True
        
        cmdlistado.Visible = False
        cmdDeshacer.Visible = True
        cmdCodigo.Visible = True
        
        m_bModoEdicion = True
    
    Else
        
        If sdbgMonedas.DataChanged = True Then
            
          If sdbgMonedas.Col <> 4 Then
            v = sdbgMonedas.ActiveCell.Value
            sdbgMonedas.SetFocus
            sdbgMonedas.ActiveCell.Value = v
          End If
          
            m_bValError = False
            m_bAnyaError = False
            m_bModError = False
            
            sdbgMonedas.Update
            
            If m_bValError Or m_bAnyaError Or m_bModError Then
                Exit Sub
            End If
            
        End If
        
        sdbgMonedas.AllowAddNew = False
        sdbgMonedas.AllowUpdate = False
        
        cmdCodigo.Visible = False
        cmdA�adir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
        cmdRestaurar.Visible = True
        cmdlistado.Visible = True
   
        frmMON.Caption = sIdiMonConsulta
        
        cmdModoEdicion.Caption = sIdiEdicion

        cmdRestaurar_Click
        
        m_bModoEdicion = False
           
    End If
    
    sdbgMonedas.SetFocus
    
End Sub
Private Sub cmdRestaurar_Click()

    ''' * Objetivo: Restaurar el contenido de la grid
    ''' * Objetivo: desde la base de datos
    
    Me.Caption = sIdiMonConsulta
    
    Screen.MousePointer = vbHourglass
    
    Set m_oMonedas = Nothing
    
    Set m_oMonedas = g_oRaiz.generar_cmonedas
    m_oMonedas.CargarTodasLasMonedas 32000
    
    CargarGridMonedas
    
    m_bCriterioCoincidenciaTotal = False
    
    Screen.MousePointer = vbNormal
    
    sdbgMonedas.SetFocus
    m_bCriterioCoincidenciaTotal = False
    
End Sub
Private Sub Form_Load()

    ''' * Objetivo: Cargar las monedas e iniciar
    ''' * Objetivo: el formulario

    Set m_oIdiomas = g_oGestorParametros.DevolverIdiomas(False, True)
    
    CargarRecursos
    Set m_oMonedas = g_oRaiz.generar_cmonedas
    m_oMonedas.CargarTodasLasMonedas 32000
    
    Me.Width = 6585
    Me.Height = 4620
    Me.Top = 0
    Me.Left = 0
    
    Show
    DoEvents
    
    m_bModoEdicion = False
        
    m_bCriterioCoincidenciaTotal = False
    
    cmdRestaurar_Click
End Sub
Private Sub Form_Resize()
    
    ''' * Objetivo: Adecuar los controles
    
    Arrange
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    ''' * Objetivo: Descargar el formulario si no
    ''' * Objetivo: hay cambios pendientes
    
    Dim v As Variant
    
    If sdbgMonedas.DataChanged = True Then
    
        v = sdbgMonedas.ActiveCell.Value
        sdbgMonedas.SetFocus
        sdbgMonedas.ActiveCell.Value = v
        
        m_bValError = False
        m_bAnyaError = False
        m_bModError = False
                
        sdbgMonedas.Update
            
        If m_bValError Or m_bAnyaError Or m_bModError Then
            Cancel = True
        End If
        
    End If
    
    
    Set m_oMonedas = Nothing
    Set m_oMonedaEnEdicion = Nothing
       
End Sub


Private Sub sdbgMonedas_AfterInsert(RtnDispErrMsg As Integer)
    
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If m_bAnyaError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
    End If
    m_bAnyadir = False
End Sub
Private Sub sdbgMonedas_AfterUpdate(RtnDispErrMsg As Integer)

    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If Not m_bModoEdicion Then Exit Sub
    
    If m_bAnyaError = False And m_bModError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
    End If
    
End Sub

Private Sub sdbgMonedas_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = 0

End Sub

Private Sub sdbgMonedas_BeforeInsert(Cancel As Integer)
    Dim oError As CTESError
    
    If Not m_bModoEdicion Then Exit Sub
        
    m_bAnyadir = True
    m_bAnyaError = False
End Sub

Private Sub sdbgMonedas_BeforeUpdate(Cancel As Integer)

    Dim oError As CTESError
    Dim oMon As CMoneda
    Dim i As Integer
    Dim oIdi As CIdioma
        
    ''' * Objetivo: Validar los datos
    
    m_bValError = False
    m_bModError = False
    Cancel = False
    
    'comprueba los datos a insertar
    If Trim(sdbgMonedas.Columns(0).Value) = "" Then
        basMensajes.NoValido sIdiCodigo
        GoTo Salir
    End If
    
    For i = 1 To 3
        If Trim(sdbgMonedas.Columns(i).Value) = "" Then
    '        basMensajes.NoValida "Denominacion"
            basMensajes.NoValida sdbgMonedas.Columns(i).Caption
            GoTo Salir
        End If
    Next
   
    If Not IsNumeric(sdbgMonedas.Columns(4).Text) And sdbgMonedas.Columns(4).Text <> "" Then
        basMensajes.NoValida sIdiEquiv
        GoTo Salir
    End If
    
    m_oMonedas.CargarTodasLasMonedas 1, , , sdbgMonedas.Columns(0).Text, , True
    
    If m_oMonedas.Count > 0 Then
        For Each oMon In m_oMonedas
            If oMon.Id <> sdbgMonedas.Columns(5).Value Or (m_bAnyadir And sdbgMonedas.IsAddRow) Then
                basMensajes.DatoDuplicado sIdiCodigo
                GoTo Salir
            End If
        Next
            
    End If
    
    
    Set m_oMonedaEnEdicion = Nothing
    Set m_oMonedaEnEdicion = g_oRaiz.Generar_CMoneda
    
    m_oMonedaEnEdicion.Cod = sdbgMonedas.Columns(0).Text
    
    Set m_oMonedaEnEdicion.Denominaciones = g_oRaiz.Generar_CMultiidiomas
    For Each oIdi In m_oIdiomas
        m_oMonedaEnEdicion.Denominaciones.Add oIdi.Cod, ""
    Next
    
    For Each oIdi In m_oIdiomas
        m_oMonedaEnEdicion.Denominaciones.Item(oIdi.Cod).Den = sdbgMonedas.Columns("DEN_" & oIdi.Cod).Text
    Next
    
    If sdbgMonedas.Columns(4).Text = "" Then
        m_oMonedaEnEdicion.EQUIV = 0
    Else
        m_oMonedaEnEdicion.EQUIV = sdbgMonedas.Columns(4).Text
    End If
    
    
    If m_bAnyadir And sdbgMonedas.IsAddRow Then   'Inserci�n
        Set oError = m_oMonedaEnEdicion.AnyadirMoneda
    
        If oError.numerror <> 0 Then
            basErrores.TratarError oError
            m_bAnyaError = True
            Cancel = True
            sdbgMonedas.SetFocus
            Exit Sub
        Else
            sdbgMonedas.Columns(5).Value = m_oMonedaEnEdicion.Id
            m_bAnyadir = False
        End If
    
    Else  'Modificaci�n

        m_oMonedaEnEdicion.Id = sdbgMonedas.Columns(5).Value
    
        Set oError = m_oMonedaEnEdicion.ModificarMonedas
    
        If oError.numerror <> 0 Then
            basErrores.TratarError oError
            m_bModError = True
            Cancel = True
        End If
    End If
    
    m_oMonedas.CargarTodasLasMonedas 32000

    Exit Sub
    
Salir:
    Cancel = True
    m_bValError = Cancel
    sdbgMonedas.SetFocus
    'm_bAnyadir = False

        
End Sub

Private Sub sdbgMonedas_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    Dim oError As CTESError
    
    If cmdDeshacer.Enabled = False Then
    
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
    
    End If
    
    If m_bModoEdicion And Not sdbgMonedas.IsAddRow Then
    
        Set m_oMonedaEnEdicion = Nothing
        Set m_oMonedaEnEdicion = m_oMonedas.Item(CStr(sdbgMonedas.Bookmark))
    End If
    
End Sub
Private Sub sdbgMonedas_HeadClick(ByVal ColIndex As Integer)

    ''' * Objetivo: Ordenar el grid segun la columna
        
    Dim lCodigo As Integer
    Dim sCodigo As String
    Dim lDenominacion As Integer
    Dim sDenominacion As String
    Dim sHeadCaption As String
    
    
    If m_bModoEdicion Then Exit Sub

    Screen.MousePointer = vbHourglass
    
    sHeadCaption = sdbgMonedas.Columns(ColIndex).Caption
    sdbgMonedas.Columns(ColIndex).Caption = sIdiOrdenando
    
    ''' Volvemos a cargar las monedas, ordenadas segun la columna
    ''' en cuyo encabezado se ha hecho 'clic'.
    
    Set m_oMonedas = Nothing
    Set m_oMonedas = g_oRaiz.generar_cmonedas
    
    
            
    Select Case ColIndex
    Case 0
        m_oMonedas.CargarTodasLasMonedas 32000
    Case 1, 2, 3
        m_oMonedas.CargarTodasLasMonedas 32000, True, IdiColum:=Right(sdbgMonedas.Columns(ColIndex).Name, 3)
    Case 4
        m_oMonedas.CargarTodasLasMonedas 32000, , True
    End Select
                
           
    CargarGridMonedas
    sdbgMonedas.Columns(ColIndex).Caption = sHeadCaption

    Screen.MousePointer = vbNormal
End Sub



Private Sub sdbgMonedas_InitColumnProps()
    sdbgMonedas.Columns(4).NumberFormat = "#,##0.0#########"
End Sub

Private Sub sdbgMonedas_KeyPress(KeyAscii As Integer)

    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
  
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgMonedas.DataChanged = False Then
            
            sdbgMonedas.CancelUpdate
            sdbgMonedas.DataChanged = False
            
            If Not m_oMonedaEnEdicion Is Nothing Then
                'cancelar edicion
                Set m_oMonedaEnEdicion = Nothing
            End If
            
            If Not sdbgMonedas.IsAddRow Then
            
                cmdA�adir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
            
            Else
            
                cmdA�adir.Enabled = False
                cmdEliminar.Enabled = False
                cmdDeshacer.Enabled = True
            
            End If
                    
       End If
       
    End If
    
End Sub

Private Sub sdbgMonedas_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    
    If Not sdbgMonedas.IsAddRow Then
        
        sdbgMonedas.Columns(0).Locked = True
        
        If sdbgMonedas.DataChanged = False Then
            cmdA�adir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
        End If
    
    Else
        
        sdbgMonedas.Columns(0).Locked = False
        'sdbgMonedas.Columns(3).Value = 1
        
        If Not IsNull(LastRow) Then
            If Val(LastRow) <> Val(sdbgMonedas.Row) Then
                sdbgMonedas.Col = 0
                cmdDeshacer.Enabled = False
            End If
        End If
        
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        If sdbgMonedas.DataChanged = True Then cmdDeshacer.Enabled = True
    
    End If
    
    If sdbgMonedas.Columns(5).Value = basParametros.g_udtParametrosGenerales.g_iMonedaCentral Then
        sdbgMonedas.Columns(4).Locked = True
    Else
        sdbgMonedas.Columns(4).Locked = False
    End If
End Sub

Private Sub CargarGridMonedas()
    Dim oMoneda As CMoneda
    Dim sCadena As String
    Dim oIdi As CIdioma
    
    sdbgMonedas.RemoveAll
    
    If Not m_oMonedas Is Nothing Then
    
       For Each oMoneda In m_oMonedas
          If oMoneda.EQUIV = 0 Then
            sCadena = oMoneda.Cod & Chr(9) & oMoneda.Denominaciones.Item(g_udtParametrosGenerales.g_sIdioma).Den
            For Each oIdi In m_oIdiomas
                If oIdi.Cod <> g_udtParametrosGenerales.g_sIdioma Then
                    sCadena = sCadena & Chr(9) & oMoneda.Denominaciones.Item(oIdi.Cod).Den
                End If
            Next
            sCadena = sCadena & Chr(9) & "" & Chr(9) & oMoneda.Id
            sdbgMonedas.AddItem sCadena
          Else
            sCadena = oMoneda.Cod & Chr(9) & oMoneda.Denominaciones.Item(g_udtParametrosGenerales.g_sIdioma).Den
            For Each oIdi In m_oIdiomas
                If oIdi.Cod <> g_udtParametrosGenerales.g_sIdioma Then
                    sCadena = sCadena & Chr(9) & oMoneda.Denominaciones.Item(oIdi.Cod).Den
                End If
            Next
            sCadena = sCadena & Chr(9) & oMoneda.EQUIV & Chr(9) & oMoneda.Id
            sdbgMonedas.AddItem sCadena
          End If
       Next
    
    End If
End Sub

Private Sub sdbgMonedas_RowLoaded(ByVal Bookmark As Variant)
    
    'Si es la moneda central la pongo en amarillo
    If sdbgMonedas.Columns(5).Value = basParametros.g_udtParametrosGenerales.g_iMonedaCentral Then
        sdbgMonedas.Columns(4).CellStyleSet "EqNoAct"
    End If
    
End Sub



Private Sub CargarRecursos()
Dim ador As ador.Recordset
Dim k As Integer
Dim oIdi As CIdioma

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_MON, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdA�adir.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdCodigo.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdDeshacer.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdlistado.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdModoEdicion.Caption = ador(0).Value
        sIdiEdicion = ador(0).Value
        ador.MoveNext
        Me.cmdRestaurar.Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgMonedas.Columns(0).Caption = ador(0).Value
        sIdiCodigo = ador(0).Value
        ador.MoveNext
        Me.sdbgMonedas.Columns(1).Caption = ador(0).Value
        Me.sdbgMonedas.Columns(2).Caption = ador(0).Value
        Me.sdbgMonedas.Columns(3).Caption = ador(0).Value
        ador.MoveNext
        sIdiMonConsulta = ador(0).Value
        ador.MoveNext
        sIdiOrdenando = ador(0).Value
        ador.MoveNext
        cmdEliminar.Caption = ador(0).Value
        ador.MoveNext
        sIdiLaMoneda = ador(0).Value
       
        ador.Close
        
        
        'Se cargan los t�tulos de las columnas en los distintos idioma
        k = 1
        For Each oIdi In m_oIdiomas
            If oIdi.Cod = g_udtParametrosGenerales.g_sIdioma Then
                sdbgMonedas.Columns(k).Caption = sdbgMonedas.Columns(k).Caption & " (" & oIdi.Den & ")"
                sdbgMonedas.Columns(k).Name = "DEN_" & g_udtParametrosGenerales.g_sIdioma
                k = k + 1
                Exit For
            End If
        Next
    
        For Each oIdi In m_oIdiomas
            If oIdi.Cod <> g_udtParametrosGenerales.g_sIdioma Then
                sdbgMonedas.Columns(k).Caption = sdbgMonedas.Columns(k).Caption & " (" & oIdi.Den & ")"
                sdbgMonedas.Columns(k).Name = "DEN_" & oIdi.Cod
                k = k + 1
            End If
        Next

    End If
    
    Set ador = Nothing
           
End Sub


