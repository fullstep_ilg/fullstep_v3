VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstInfVolProv 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de volumen adjudicado por proveedor"
   ClientHeight    =   2490
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5370
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstInfVolProv.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2490
   ScaleWidth      =   5370
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdObtener 
      Caption         =   "Obtener"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3990
      TabIndex        =   0
      Top             =   2050
      Width           =   1335
   End
   Begin TabDlg.SSTab ssTabListado 
      Height          =   2000
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   5325
      _ExtentX        =   9393
      _ExtentY        =   3519
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstInfVolProv.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Opciones"
      TabPicture(1)   =   "frmLstInfVolProv.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "frameOpt"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Orden"
      TabPicture(2)   =   "frmLstInfVolProv.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "frameOrden"
      Tab(2).ControlCount=   1
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1455
         Left            =   120
         TabIndex        =   8
         Top             =   360
         Width           =   5055
         Begin VB.TextBox txtFecHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3450
            TabIndex        =   12
            Top             =   360
            Width           =   1110
         End
         Begin VB.CommandButton cmdCalFecApeHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4590
            Picture         =   "frmLstInfVolProv.frx":0D06
            Style           =   1  'Graphical
            TabIndex        =   11
            TabStop         =   0   'False
            ToolTipText     =   "Mantenimiento"
            Top             =   360
            Width           =   315
         End
         Begin VB.TextBox txtFecDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1025
            TabIndex        =   10
            Top             =   360
            Width           =   1110
         End
         Begin VB.CommandButton cmdCalFecApeDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2165
            Picture         =   "frmLstInfVolProv.frx":1290
            Style           =   1  'Graphical
            TabIndex        =   9
            TabStop         =   0   'False
            ToolTipText     =   "Mantenimiento"
            Top             =   360
            Width           =   315
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCia 
            Height          =   285
            Left            =   1025
            TabIndex        =   13
            Top             =   960
            Width           =   2535
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3307
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   8678
            Columns(2).Caption=   "Denominaci�n"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   4471
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblFecHasta 
            Caption         =   "Hasta:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   2760
            TabIndex        =   16
            Top             =   390
            Width           =   615
         End
         Begin VB.Label lblFecDesde 
            Caption         =   "Desde:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   195
            TabIndex        =   15
            Top             =   390
            Width           =   945
         End
         Begin VB.Label lblCia 
            Caption         =   "Proveedor:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   200
            TabIndex        =   14
            Top             =   990
            Width           =   975
         End
      End
      Begin VB.Frame frameOpt 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   -74880
         TabIndex        =   4
         Top             =   360
         Width           =   5055
         Begin VB.OptionButton optSoloComp 
            Caption         =   "Desglose por compa��as compradoras"
            Height          =   255
            Left            =   240
            TabIndex        =   18
            Top             =   360
            Value           =   -1  'True
            Width           =   4095
         End
         Begin VB.OptionButton optDetalle 
            Caption         =   "Desglose por procesos"
            Height          =   255
            Left            =   240
            TabIndex        =   17
            Top             =   840
            Width           =   3135
         End
      End
      Begin VB.Frame frameOrden 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1455
         Left            =   -74880
         TabIndex        =   2
         Top             =   360
         Width           =   5055
         Begin VB.OptionButton optVolGS 
            Caption         =   "Volumen en la moneda de GS"
            Height          =   325
            Left            =   3000
            TabIndex        =   7
            Top             =   360
            Width           =   1935
         End
         Begin VB.OptionButton optCod 
            Caption         =   "C�digo compa��a compradora"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   3
            Top             =   360
            Value           =   -1  'True
            Width           =   2535
         End
         Begin VB.OptionButton optDen 
            Caption         =   "Denominaci�n compa�ia compradora"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   450
            Left            =   240
            TabIndex        =   5
            Top             =   860
            Width           =   2655
         End
         Begin VB.OptionButton optVolPor 
            Caption         =   "Volumen en la moneda del portal"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   450
            Left            =   3000
            TabIndex        =   6
            Top             =   860
            Width           =   2000
         End
      End
   End
End
Attribute VB_Name = "frmLstInfVolProv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean
Private oCias As CCias

'C�digo y denominaci�n de la moneda central
Public DenMonCentral As String
Public bExcel As Boolean

Private sIdiFecDesde As String
Private sIdiFecHasta As String
Private sIdiProveedor As String
Private sIdiProce As String
Private sIdiFec As String
Private sIdiAdjGS As String
Private sIdiAdj As String
Private sIdiTotAdj As String


Private sIdiMonGS As String

Private sIdiTxtTitulo As String
Private sIdiTxtFecDesde As String
Private sIdiTxtFecHasta As String
Private sIdiTxtProve As String
Private sIdiTxtDen As String

Private sIdiTxtComprador As String
Private sIdiTxtMonGS As String
Private sIdiTxtPag As String
Private sIdiTxtDe As String


Private Sub cmdCalFecApeDesde_Click()
    'Muestra el formulario del calendario
    Set frmCalendar.frmDestination = frmLstInfVolProv
    Set frmCalendar.ctrDestination = txtFecDesde
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecDesde <> "" Then
        frmCalendar.Calendar.Value = Me.txtFecDesde
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
    
End Sub

Private Sub cmdCalFecApeHasta_Click()
        'Muestra el formulario del calendario
    
    Set frmCalendar.frmDestination = frmLstInfVolProv
    Set frmCalendar.ctrDestination = txtFecHasta
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecHasta <> "" Then
        frmCalendar.Calendar.Value = txtFecHasta
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub

Private Sub cmdObtener_Click()
    'Comprueba que se han introducido todos los par�metros
    If txtFecDesde = "" Then
        basMensajes.NoValida sIdiFecDesde
        Exit Sub
    End If
    
    If txtFecHasta = "" Then
        basMensajes.NoValida sIdiFecHasta
        Exit Sub
    End If
    
    If sdbcCia.Text = "" Then
        basMensajes.NoValido sIdiProveedor
        Exit Sub
    End If
    
    If CDate(txtFecHasta.Text) < CDate(txtFecDesde.Text) Then
        basMensajes.FechaHastaMenor
        txtFecHasta.SetFocus
        Exit Sub
    End If
    
    sdbcCia_Validate False
    
    If bExcel Then
        '''Obtiene la hoja excel
        ObtenerHojaExcel
    Else
        '''Obtiene el informe
        ObtenerInforme
    End If
End Sub

Private Sub Form_Load()
    
    Me.Height = 2850
    Me.Width = 5475
    CargarRecursos
    
    'Clase cias
    Set oCias = g_oRaiz.Generar_CCias
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oCias = Nothing
End Sub

Private Sub sdbcCia_DropDown()
    Dim i As Long
    Dim ador As ador.Recordset
    
    Screen.MousePointer = vbHourglass
    
    sdbcCia.RemoveAll

    If bCargarComboDesde Then
        Set ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCia.Text), , False, False, , , , , , , , , , , , , , 5)
    Else
        Set ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, , , , , , , , , , , , , , 5)
    End If
    
    If Not ador Is Nothing Then
        
        i = 1
        
        While Not ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCia.AddItem ador("ID").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("DEN").Value
            ador.MoveNext
        Wend
        
        If Not ador.EOF Then
            sdbcCia.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        
        ador.Close
        Set ador = Nothing
    End If
    
    
    sdbcCia.SelStart = 0
    sdbcCia.SelLength = Len(sdbcCia.Text)
    sdbcCia.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCia_InitColumnProps()
    sdbcCia.DataField = "Column 1"
    sdbcCia.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcCia_PositionList(ByVal Text As String)
        ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCia.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCia.Rows - 1
            bm = sdbcCia.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCia.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCia.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcCia_Click()
    If Not sdbcCia.DroppedDown Then
        sdbcCia = ""
    End If
End Sub

Private Sub sdbcCia_CloseUp()
    If sdbcCia.Value = "-1" Then
        sdbcCia.Text = ""
        Exit Sub
    End If
    
    If sdbcCia.Text = "" Then Exit Sub
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcCia_Change()
    If Not bRespetarCombo Then
        bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcCia_Validate(Cancel As Boolean)
    Dim ador As ador.Recordset

    If Trim(sdbcCia.Text) = "" Then Exit Sub
    
    Set ador = oCias.DevolverCiasDesde(1, sdbcCia.Text, , True)
    
    If ador Is Nothing Then
        sdbcCia = ""
        Exit Sub
    Else
        If ador.EOF Then
            sdbcCia.Value = ""
            Exit Sub
        Else
            If UCase(sdbcCia.Text) <> UCase(ador("COD").Value) Then
                sdbcCia.Value = ""
                Exit Sub
            Else
                bRespetarCombo = True
                sdbcCia.Text = ador("COD").Value
                sdbcCia.Columns(0).Value = ador("ID").Value
                sdbcCia.Columns(1).Value = ador("COD").Value
                sdbcCia.Columns(2).Value = ador("DEN").Value
                bRespetarCombo = False
                bCargarComboDesde = False
            End If
        End If
        
        ador.Close
        Set ador = Nothing
        
    End If
End Sub

Private Sub txtFecDesde_Validate(Cancel As Boolean)
    If Not IsDate(txtFecDesde.Text) And Not txtFecDesde.Text = "" Then
        basMensajes.NoValida (sIdiFecDesde)
        txtFecDesde.Text = ""
        Cancel = True
    End If
End Sub

Private Sub txtFecHasta_Validate(Cancel As Boolean)
    If Not IsDate(txtFecHasta.Text) And Not txtFecHasta.Text = "" Then
        basMensajes.NoValida (sIdiFecHasta)
        txtFecHasta.Text = ""
        Cancel = True
    End If
    
End Sub


Private Sub ObtenerInforme()
    Dim oReport As CRAXDRT.Report
    Dim oCRInformes As New CRInformes
    Dim pv As Preview
    Dim RepPath As String
    Dim oFos As FileSystemObject
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    'Comprueba que la ruta sea la correcta y que exista el informe
    If basParametros.g_sRptPath = "" Then
        basMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    RepPath = basParametros.g_sRptPath & "\rptVolProv.rpt"

    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        basMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    Set oFos = Nothing
    
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "FecDesde")).Text = DateToSQLDate(Me.txtFecDesde)
    oReport.FormulaFields(crs_FormulaIndex(oReport, "FecHasta")).Text = DateToSQLDate(Me.txtFecHasta)
    oReport.FormulaFields(crs_FormulaIndex(oReport, "Prov")).Text = """" & sdbcCia.Text & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "DenProv")).Text = """" & sdbcCia.Columns(2).Value & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TOT")).Text = """" & DenMonCentral & """"
    
    'Dependiendo de si se ha seleccionado el detalle o no se suprime la secci�n de detalle
    If Me.optDetalle.Value = False Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "SUP")).Text = "'T'"
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "SUP")).Text = "'F'"
    End If
    
    oCRInformes.ListadoVolProv oReport, txtFecDesde, txtFecHasta, sdbcCia.Columns(0).Value, optCod.Value, optDen.Value, optVolPor.Value, optVolGS.Value
                    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = "'" & sIdiTxtTitulo & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecDesde")).Text = "'" & sIdiFecDesde & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecHasta")).Text = "'" & sIdiFecHasta & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProve")).Text = "'" & sIdiTxtProve & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDen")).Text = "'" & sIdiTxtDen & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAdj")).Text = "'" & sIdiAdj & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtComp")).Text = "'" & sIdiTxtComprador & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMonGS")).Text = "'" & sIdiTxtMonGS & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAdjGS")).Text = "'" & sIdiAdjGS & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProce")).Text = "'" & sIdiProce & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecha")).Text = "'" & sIdiFec & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTotalAdj")).Text = "'" & sIdiTotAdj & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = "'" & sIdiTxtPag & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = "'" & sIdiTxtDe & "'"
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
   
    Set pv = New Preview
    Unload Me
    pv.Hide
    pv.Caption = Me.Caption
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    pv.crViewer.ViewReport
    pv.Show
    
    Set oReport = Nothing
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub ObtenerHojaExcel()
    Dim xlApp As Object
    Dim xlBook As Object
    Dim xlSheet As Object
    Dim sCelda As String
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim orange As Object
    Dim ador As ador.Recordset
    Dim i As Integer
    Dim intOrden As Integer
    Dim sVersion As String
    Dim intDetalle As Integer
    Dim adorDetalle As ador.Recordset
    Dim sumVol As Double
    
    On Error GoTo error
    
    'Comprueba que el el path sea correcto y que exista el informe
    
    RepPath = Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, "") & "\infAdjucicProv.xlt"

    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        basMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass

    'Crear aplicaci�n excel
    Set xlApp = CreateObject("Excel.Application")
    
    sVersion = xlApp.Version
    
    Set xlBook = xlApp.Workbooks.Add(RepPath)
    
    Set xlSheet = xlBook.Sheets.Item(1)
    
    'Condiciones de fechas
    sCelda = "C" & 7
    xlSheet.Range(sCelda) = xlSheet.Range(sCelda) & "  " & Me.txtFecDesde
    sCelda = "G" & 7
    xlSheet.Range(sCelda) = Me.txtFecHasta
    sCelda = "C" & 8
    xlSheet.Range(sCelda) = sdbcCia.Columns(1).Value & "-" & sdbcCia.Columns(2).Value
    
    'Orden del informe
    If Me.optCod = True Then
        intOrden = 0
    ElseIf Me.optDen Then
        intOrden = 1
    ElseIf Me.optVolPor Then
        intOrden = 2
    ElseIf Me.optVolGS Then
        intOrden = 3
    End If
    
    'DATOS
    i = 11
    sumVol = 0
    
    Set ador = g_oGestorInformes.DevolverVolumenAdjudicado(txtFecDesde.Text, txtFecHasta.Text, sdbcCia.Columns(0).Value, intOrden)
    If Not ador.EOF Then
      While Not ador.EOF
        sCelda = "A" & i
        xlSheet.Range(sCelda) = ador("COD")
        Set orange = xlSheet.Range("A" & i & ":" & "B" & i)
        orange.borderaround , 2
        orange.mergecells = True
        
        sCelda = "C" & i
        xlSheet.Range(sCelda) = ador("DEN")
        Set orange = xlSheet.Range("C" & i & ":" & "G" & i)
        orange.borderaround , 2
        orange.mergecells = True
        
        sCelda = "L" & i
        xlSheet.Range(sCelda) = ador("MGS")
        Set orange = xlSheet.Range("L" & i & ":" & "M" & i)
        orange.borderaround , 2
        orange.mergecells = True
        
        sCelda = "O" & i
        xlSheet.Range(sCelda) = NullToDbl0(ador("VOLGS"))
        xlSheet.Range(sCelda).horizontalalignment = 4
        xlSheet.Range(sCelda).NumberFormat = "general"
        Set orange = xlSheet.Range("N" & i & ":" & "O" & i)
        orange.borderaround , 2
        orange.mergecells = True
        
        sCelda = "Q" & i
        xlSheet.Range(sCelda) = NullToDbl0(ador("VOLPOR"))
        xlSheet.Range(sCelda).horizontalalignment = 4
        xlSheet.Range(sCelda).NumberFormat = "general"
        Set orange = xlSheet.Range("P" & i & ":" & "Q" & i)
        orange.borderaround , 2
        sumVol = sumVol + NullToDbl0(ador("VOLPOR"))
        orange.mergecells = True
        
        'Color de los datos
        Set orange = xlSheet.Range("A" & i & ":" & "Q" & i)
        orange.interior.Color = RGB(245, 245, 200)
        
        
        'Detalle
        If Me.optDetalle.Value = True Then
            'Detalle
            Set adorDetalle = g_oGestorInformes.DevolverVolumenPorProcesos(Me.txtFecDesde, Me.txtFecHasta, Me.sdbcCia.Columns(0).Value, ador("ID"))
            
            If Not adorDetalle.EOF Then
                sCelda = "B" & i + 1
                xlSheet.Range(sCelda) = sIdiProce
                Set orange = xlSheet.Range("B" & i + 1 & ":" & "G" & i + 1)
                orange.borderaround , 2
                orange.Font.Bold = True
                orange.interior.Color = RGB(192, 192, 192)
                orange.mergecells = True
                
                sCelda = "L" & i + 1
                xlSheet.Range(sCelda) = sIdiFec
                Set orange = xlSheet.Range("L" & i + 1 & ":" & "M" & i + 1)
                orange.borderaround , 2
                orange.Font.Bold = True
                orange.interior.Color = RGB(192, 192, 192)
                orange.mergecells = True
                
                sCelda = "O" & i + 1
                xlSheet.Range(sCelda) = sIdiAdjGS
                xlSheet.Range(sCelda).horizontalalignment = 4
                Set orange = xlSheet.Range("N" & i + 1 & ":" & "O" & i + 1)
                orange.borderaround , 2
                orange.Font.Bold = True
                orange.interior.Color = RGB(192, 192, 192)
                orange.mergecells = True
                
                sCelda = "Q" & i + 1
                xlSheet.Range(sCelda) = sIdiAdj
                xlSheet.Range(sCelda).horizontalalignment = 4
                Set orange = xlSheet.Range("P" & i + 1 & ":" & "Q" & i + 1)
                orange.borderaround , 2
                orange.Font.Bold = True
                orange.interior.Color = RGB(192, 192, 192)
                orange.mergecells = True
                
                intDetalle = i + 2
                While Not adorDetalle.EOF
                    sCelda = "B" & intDetalle
                    xlSheet.Range(sCelda) = adorDetalle("ANYO") & "-" & adorDetalle("GMN1") & "-" & adorDetalle("PROCE") & "   " & adorDetalle("DENPROCE")
                    Set orange = xlSheet.Range("B" & intDetalle & ":" & "G" & intDetalle)
                    orange.borderaround , 2
                    orange.interior.Color = RGB(162, 250, 158)
                    orange.mergecells = True
                    
                    sCelda = "L" & intDetalle
                    xlSheet.Range(sCelda) = CStr(adorDetalle("FECADJ"))
                    xlSheet.Range(sCelda).NumberFormat = "dd/mm/yyyy"
                    xlSheet.Range(sCelda).horizontalalignment = 2
                    Set orange = xlSheet.Range("L" & intDetalle & ":" & "M" & intDetalle)
                    orange.borderaround , 2
                    orange.interior.Color = RGB(162, 250, 158)
                    orange.mergecells = True
                    
                    sCelda = "O" & intDetalle
                    xlSheet.Range(sCelda) = NullToDbl0(adorDetalle("VOLGS"))
                    xlSheet.Range(sCelda).horizontalalignment = 4
                    xlSheet.Range(sCelda).NumberFormat = "general"
                    Set orange = xlSheet.Range("N" & intDetalle & ":" & "O" & intDetalle)
                    orange.borderaround , 2
                    orange.interior.Color = RGB(162, 250, 158)
                    orange.mergecells = True
                    
                    sCelda = "Q" & intDetalle
                    xlSheet.Range(sCelda) = NullToDbl0(adorDetalle("VOLPOR"))
                    xlSheet.Range(sCelda).horizontalalignment = 4
                    xlSheet.Range(sCelda).NumberFormat = "general"
                    Set orange = xlSheet.Range("P" & intDetalle & ":" & "Q" & intDetalle)
                    orange.borderaround , 2
                    orange.interior.Color = RGB(162, 250, 158)
                    orange.mergecells = True
                    
                    intDetalle = intDetalle + 1
                    adorDetalle.MoveNext
                Wend
                
            End If
            Set adorDetalle = Nothing
        End If
        
        ador.MoveNext
        
        If Me.optDetalle.Value = False Then
            i = i + 1
        Else
            i = intDetalle
        End If
      Wend
      
      'TOTALES
      sCelda = "A" & i + 1
      xlSheet.Range(sCelda) = sIdiTotAdj & " (" & DenMonCentral & "): "
      Set orange = xlSheet.Range("A" & i + 1 & ":" & "O" & i + 1)
      orange.borderaround , 2
      orange.Font.Bold = True
      orange.interior.Color = RGB(192, 192, 192)
      orange.mergecells = True
      
      sCelda = "Q" & i + 1
      xlSheet.Range(sCelda) = sumVol
      xlSheet.Range(sCelda).horizontalalignment = 4
      xlSheet.Range(sCelda).NumberFormat = "general"
      Set orange = xlSheet.Range("P" & i + 1 & ":" & "Q" & i + 1)
      orange.borderaround , 2
      orange.Font.Bold = True
      orange.interior.Color = RGB(192, 192, 192)
      orange.mergecells = True

    End If
    Set ador = Nothing
    
    Unload Me
    'Hace visible la hoja excel
    xlApp.Visible = True

    Screen.MousePointer = vbNormal
    
    Exit Sub
    
error:
    Screen.MousePointer = vbNormal
    'Cierro los objetos
    Set xlSheet = Nothing
    Set xlBook = Nothing
    xlApp.Quit
    Set xlApp = Nothing
End Sub




Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTINFVOLPROV, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        sIdiTxtTitulo = ador(0).Value
        ador.MoveNext
        Me.cmdObtener.Caption = ador(0).Value
        ador.MoveNext
        Me.lblCia.Caption = ador(0).Value
        sIdiTxtProve = ador(0).Value
        ador.MoveNext
        Me.lblFecDesde.Caption = ador(0).Value
        sIdiTxtFecDesde = ador(0).Value
        ador.MoveNext
        Me.lblFecHasta.Caption = ador(0).Value
        sIdiTxtFecHasta = ador(0).Value
        ador.MoveNext
        
        Me.optCod.Caption = ador(0).Value
        ador.MoveNext
        Me.optDen.Caption = ador(0).Value
        ador.MoveNext
        
        Me.optDetalle.Caption = ador(0).Value
        ador.MoveNext
        Me.optSoloComp.Caption = ador(0).Value
        ador.MoveNext
        Me.optVolGS.Caption = ador(0).Value
        ador.MoveNext
        Me.optVolPor.Caption = ador(0).Value
        ador.MoveNext
        Me.sdbcCia.Columns(1).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbcCia.Columns(2).Caption = ador(0).Value
        sIdiTxtDen = ador(0).Value
        ador.MoveNext
        
        Me.ssTabListado.TabCaption(0) = ador(0).Value
        ador.MoveNext
        Me.ssTabListado.TabCaption(1) = ador(0).Value
        ador.MoveNext
        Me.ssTabListado.TabCaption(2) = ador(0).Value
        ador.MoveNext
        
        sIdiFecDesde = ador(0).Value
        ador.MoveNext
        sIdiFecHasta = ador(0).Value
        ador.MoveNext
        sIdiProce = ador(0).Value
        ador.MoveNext
        sIdiFec = ador(0).Value
        ador.MoveNext
        sIdiAdjGS = ador(0).Value
        ador.MoveNext
        sIdiAdj = ador(0).Value
        ador.MoveNext
        sIdiTotAdj = ador(0).Value
        ador.MoveNext
        sIdiTxtComprador = ador(0).Value
        ador.MoveNext
        sIdiTxtMonGS = ador(0).Value
        ador.MoveNext
        sIdiTxtPag = ador(0).Value
        ador.MoveNext
        sIdiTxtDe = ador(0).Value
        
        
        
        ador.Close
    End If
    
    Set ador = Nothing
           
End Sub





