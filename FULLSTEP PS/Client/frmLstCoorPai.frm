VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstCoorPai 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de Coordinaci�n de Pa�ses"
   ClientHeight    =   2850
   ClientLeft      =   5595
   ClientTop       =   6375
   ClientWidth     =   5625
   Icon            =   "frmLstCoorPai.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   Picture         =   "frmLstCoorPai.frx":0CB2
   ScaleHeight     =   2850
   ScaleWidth      =   5625
   Begin VB.CommandButton cmdObtener 
      Caption         =   "&Obtener"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   4590
      TabIndex        =   0
      Top             =   2475
      Width           =   1005
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   2415
      Left            =   15
      TabIndex        =   1
      Top             =   0
      Width           =   5565
      _ExtentX        =   9816
      _ExtentY        =   4260
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabsPerRow      =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Opciones"
      TabPicture(0)   =   "frmLstCoorPai.frx":11F4
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame3"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      Begin VB.Frame Frame2 
         Caption         =   "Orden"
         Height          =   855
         Left            =   90
         TabIndex        =   8
         Top             =   1350
         Width           =   5370
         Begin VB.OptionButton opOrdCodF 
            Caption         =   "C�digo FULLSTEP"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   390
            TabIndex        =   10
            Top             =   375
            Value           =   -1  'True
            Width           =   1635
         End
         Begin VB.OptionButton opOrdDenF 
            Caption         =   "Denom. FULLSTEP"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   2805
            TabIndex        =   9
            Top             =   375
            Width           =   2085
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Compa��a"
         Height          =   855
         Left            =   90
         TabIndex        =   5
         Top             =   420
         Width           =   5370
         Begin SSDataWidgets_B.SSDBCombo sdbcCiaCod 
            Height          =   285
            Left            =   150
            TabIndex        =   6
            Top             =   360
            Width           =   1365
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   5
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   7064
            Columns(2).Caption=   "Denominaci�n"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "FC"
            Columns(3).Name =   "FC"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "FP"
            Columns(4).Name =   "FP"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            _ExtentX        =   2408
            _ExtentY        =   503
            _StockProps     =   93
            ForeColor       =   0
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCiaDen 
            Height          =   285
            Left            =   1500
            TabIndex        =   7
            Top             =   360
            Width           =   3690
            DataFieldList   =   "Column 0"
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   5
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5980
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Caption=   "C�digo"
            Columns(2).Name =   "COD"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "FC"
            Columns(3).Name =   "FC"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "FP"
            Columns(4).Name =   "FP"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            _ExtentX        =   6509
            _ExtentY        =   503
            _StockProps     =   93
            ForeColor       =   0
            BackColor       =   -2147483643
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Listado de Coordinacion de Pa�ses"
         Height          =   975
         Left            =   -74895
         TabIndex        =   2
         Top             =   420
         Width           =   4575
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   4
            Top             =   240
            Value           =   -1  'True
            Width           =   3840
         End
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   3
            Top             =   600
            Width           =   3750
         End
      End
   End
End
Attribute VB_Name = "frmLstCoorPai"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private sTitulo As String  'preview
Private FormulaRpt(1 To 2, 1 To 5) As String      'formulas textos RPT
Private IdCiaSeleccionada As Long
Private bCargarComboDesde As Boolean
Private bRespetarCombo As Boolean
Private oCias As CCias
Public g_adores_Ador As ador.Recordset
Public oCiaSeleccionada As CCia

Private sIdiCompania As String

Private sIdiTxtTitulo As String
Private sIdiTxtCodGS As String
Private sIdiTxtDenGS As String
Private sIdiTxtCodP As String
Private sIdiTxtDenP As String
Private sIdiTxtPag As String
Private sIdiTxtDe As String


Private Sub cmdObtener_Click()
If oCiaSeleccionada Is Nothing Then
        basMensajes.NoValida sIdiCompania
        Exit Sub
End If
oCiaSeleccionada.Id = IdCiaSeleccionada
If Not oCiaSeleccionada.ComprobarConectividadConCia Then
        basMensajes.ConexionIncorrecta
        Exit Sub
End If
''' * Objetivo: Obtener un listado de Coordinaci�n de pa�ses
ObtenerInforme
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oCiaSeleccionada = Nothing
    Set oCias = Nothing
End Sub

Private Sub sdbcCiaCod_DropDown()
    Dim i As Long
    Dim g_adores_Ador As ador.Recordset
    
    sdbcCiaCod.RemoveAll

    If bCargarComboDesde Then
        Set g_adores_Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False, True)
    Else
        Set g_adores_Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, True)
    End If
    
    If Not g_adores_Ador Is Nothing Then
        
        i = 1
        
        While Not g_adores_Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaCod.AddItem g_adores_Ador("ID").Value & Chr(9) & g_adores_Ador("COD").Value & Chr(9) & g_adores_Ador("DEN").Value & Chr(9) & g_adores_Ador("FCEST").Value
            g_adores_Ador.MoveNext
        Wend
        
        If Not g_adores_Ador.EOF Then
            sdbcCiaCod.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        g_adores_Ador.Close
        Set g_adores_Ador = Nothing
    End If
    
    
    sdbcCiaCod.SelStart = 0
    sdbcCiaCod.SelLength = Len(sdbcCiaCod.Text)
    sdbcCiaCod.Refresh
    
End Sub
Private Sub sdbcCiaCod_InitColumnProps()
    
    sdbcCiaCod.DataField = "Column 1"
    sdbcCiaCod.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcCiaCod_Change()

    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaDen.Text = ""
        IdCiaSeleccionada = -1
        bRespetarCombo = False
        bCargarComboDesde = True
        Set oCiaSeleccionada = Nothing
    End If
    
End Sub

Private Sub sdbcCiaCod_Click()
    
    If Not sdbcCiaCod.DroppedDown Then
        sdbcCiaCod = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaCod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaCod.Rows - 1
            bm = sdbcCiaCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaCod_CloseUp()

    If sdbcCiaCod.Value = "-1" Then
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaCod.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaCod.Columns(2).Text
    sdbcCiaCod.Text = sdbcCiaCod.Columns(1).Text
    
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
    IdCiaSeleccionada = sdbcCiaCod.Columns(0).Value
    Set oCiaSeleccionada = g_oRaiz.Generar_CCia
    oCiaSeleccionada.Id = IdCiaSeleccionada
    
    
End Sub

Public Sub sdbcCiaCod_Validate(Cancel As Boolean)

    Dim g_adores_Ador As ador.Recordset

    If sdbcCiaCod.Text = "" Then
        Exit Sub
    End If
    
    Set g_adores_Ador = oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , True, , True)
    
    If g_adores_Ador.RecordCount = 0 Then
        Set oCiaSeleccionada = Nothing
        sdbcCiaCod.Text = ""
        sdbcCiaDen.Text = ""
        Exit Sub
        
    Else
        bRespetarCombo = True
        sdbcCiaDen.Text = g_adores_Ador("DEN").Value
        bRespetarCombo = False
        bCargarComboDesde = False
        IdCiaSeleccionada = g_adores_Ador("ID").Value
        Set oCiaSeleccionada = g_oRaiz.Generar_CCia
        oCiaSeleccionada.Id = IdCiaSeleccionada
    End If
    g_adores_Ador.Close
    Set g_adores_Ador = Nothing

        
End Sub

Private Sub sdbcCiaDen_DropDown()
    Dim i As Long
    Dim g_adores_Ador As ador.Recordset
    
    sdbcCiaDen.RemoveAll

    If bCargarComboDesde Then
        Set g_adores_Ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True, True)
    Else
        Set g_adores_Ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , , False, True, True)
    End If
    
    If Not g_adores_Ador Is Nothing Then
        i = 1
        
        While Not g_adores_Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaDen.AddItem g_adores_Ador("ID").Value & Chr(9) & g_adores_Ador("DEN").Value & Chr(9) & g_adores_Ador("COD").Value & Chr(9) & g_adores_Ador("FCEST").Value
            g_adores_Ador.MoveNext
        Wend
        
        If Not g_adores_Ador.EOF Then
            sdbcCiaDen.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        g_adores_Ador.Close
        Set g_adores_Ador = Nothing
    End If
    
    
    sdbcCiaDen.SelStart = 0
    sdbcCiaDen.SelLength = Len(sdbcCiaDen.Text)
    sdbcCiaDen.Refresh
    
End Sub
Private Sub sdbcCiaDen_InitColumnProps()
    
    sdbcCiaDen.DataField = "Column 1"
    sdbcCiaDen.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcCiaDen_Change()

    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaCod.Text = ""
        IdCiaSeleccionada = -1
        bRespetarCombo = False
        bCargarComboDesde = True
        Set oCiaSeleccionada = Nothing
    End If
    
End Sub

Private Sub sdbcCiaDen_Click()
    
    If Not sdbcCiaDen.DroppedDown Then
        sdbcCiaDen = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaDen_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaDen.Rows - 1
            bm = sdbcCiaDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaDen.Columns(2).CellText(bm), 1, Len(Text))) Then
                sdbcCiaDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaDen_CloseUp()

    If sdbcCiaDen.Columns(0).Value = "-1" Then
        sdbcCiaDen.Text = ""
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaDen.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaDen.Columns(1).Text
    sdbcCiaCod.Text = sdbcCiaDen.Columns(2).Text
    bRespetarCombo = False
        
    bCargarComboDesde = False
    
    IdCiaSeleccionada = sdbcCiaDen.Columns(0).Value
    Set oCiaSeleccionada = g_oRaiz.Generar_CCia
    oCiaSeleccionada.Id = IdCiaSeleccionada
    
End Sub




Private Sub sdbcCiaDen_Validate(Cancel As Boolean)
    
    Dim g_adores_Ador As ador.Recordset

    If sdbcCiaDen.Text = "" Then
        Exit Sub
    End If
    
    Set g_adores_Ador = oCias.DevolverCiasDesde(1, , sdbcCiaDen.Text, True, , True)
    
    If g_adores_Ador.RecordCount = 0 Then
        Set oCiaSeleccionada = Nothing
        sdbcCiaCod.Text = ""
        sdbcCiaDen.Text = ""
        Exit Sub
        
    Else
        bRespetarCombo = True
        sdbcCiaCod.Text = g_adores_Ador("COD").Value
        bRespetarCombo = False
        bCargarComboDesde = False
        IdCiaSeleccionada = g_adores_Ador("ID").Value
        Set oCiaSeleccionada = g_oRaiz.Generar_CCia
        oCiaSeleccionada.Id = IdCiaSeleccionada
    End If
    g_adores_Ador.Close
    Set g_adores_Ador = Nothing

End Sub

Private Sub ObtenerInforme()
    Dim RecordSortFields(1 To 1) As String
    Dim oReport As CRAXDRT.Report
    Dim pv As Preview
    Dim oFos As FileSystemObject
    Dim RepPath As String
    Dim intOrden As Integer
    Dim i As Integer
    Dim oCia As CCia
       
    If crs_Connected = False Then
        Exit Sub
    End If
    
    If sdbcCiaDen = "" Then
        basMensajes.NoValida sIdiCompania
        Exit Sub
    End If
    
    If Not oCiaSeleccionada.ComprobarConectividadConCia Then
       basMensajes.ConexionIncorrecta
       Exit Sub
    End If
    
    If basParametros.g_sRptPath = "" Then
        basMensajes.RutaDeRPTNoValida
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    RepPath = basParametros.g_sRptPath & "\rptcoordpai.rpt"
        
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        basMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
   
    If sdbcCiaCod.Text = "" Then
       basMensajes.NoValida sIdiCompania
        Exit Sub
    End If
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    Screen.MousePointer = vbHourglass
    
'Orden del informe
    
    Set oCia = g_oRaiz.Generar_CCia
    oCia.Id = IdCiaSeleccionada
    'oCia.Id = sdbcCiaCod.Text
    Set g_adores_Ador = oCia.DevolverPaisesDeLaCiaDesde(32000, , , , opOrdDenF)
    oReport.Database.SetDataSource g_adores_Ador
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = "'" & sIdiTxtTitulo & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodGS")).Text = "'" & sIdiTxtCodGS & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDenGS")).Text = "'" & sIdiTxtDenGS & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodP")).Text = "'" & sIdiTxtCodP & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDenP")).Text = "'" & sIdiTxtDenP & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = "'" & sIdiTxtPag & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = "'" & sIdiTxtDe & "'"
    
    Set pv = New Preview
    pv.g_sOrigen = "frmLstCoorPai"
    pv.Hide
    pv.Caption = sTitulo 'Listado de coordinaci�n de Pa�ses
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    pv.crViewer.ViewReport
    pv.Show
    Unload Me
    Screen.MousePointer = vbNormal
End Sub
Private Sub Form_Load()

    Me.Width = 5745
    Me.Height = 3255
    CargarRecursos
    Set oCias = g_oRaiz.Generar_CCias
   
    
End Sub



Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTCOORPAI, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        sIdiTxtTitulo = ador(0).Value
        Frame1.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdObtener.Caption = ador(0).Value
        ador.MoveNext
        Me.Frame2.Caption = ador(0).Value
        ador.MoveNext
        Me.Frame3.Caption = ador(0).Value
        sIdiCompania = ador(0).Value
        
        ador.MoveNext
        Me.opOrdCod(0).Caption = ador(0).Value
        Me.sdbcCiaCod.Columns(1).Caption = ador(0).Value
        Me.sdbcCiaDen.Columns(2).Caption = ador(0).Value
        ador.MoveNext
        Me.opOrdCodF.Caption = ador(0).Value
        ador.MoveNext
        Me.sdbcCiaCod.Columns(2).Caption = ador(0).Value
        Me.sdbcCiaDen.Columns(1).Caption = ador(0).Value
        Me.opOrdDen.Caption = ador(0).Value
        ador.MoveNext
        Me.opOrdDenF.Caption = ador(0).Value
        ador.MoveNext
        Me.SSTab1.TabCaption(0) = ador(0).Value
        ador.MoveNext
        sIdiTxtCodGS = ador(0).Value
        ador.MoveNext
        sIdiTxtDenGS = ador(0).Value
        ador.MoveNext
        sIdiTxtCodP = ador(0).Value
        ador.MoveNext
        sIdiTxtDenP = ador(0).Value
        ador.MoveNext
        sIdiTxtPag = ador(0).Value
        ador.MoveNext
        sIdiTxtDe = ador(0).Value
        ador.MoveNext
        
        ador.Close
    End If
    
    Set ador = Nothing
           
End Sub

