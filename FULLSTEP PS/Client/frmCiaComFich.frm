VERSION 5.00
Begin VB.Form frmCiaComFich 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Comentario"
   ClientHeight    =   2085
   ClientLeft      =   15
   ClientTop       =   1860
   ClientWidth     =   4680
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCiaComFich.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2085
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   315
      Left            =   2355
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1680
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   1245
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   1680
      Width           =   1005
   End
   Begin VB.TextBox txtCom 
      Height          =   1215
      Left            =   120
      MaxLength       =   500
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   360
      Width           =   4455
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Fichero adjunto:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1335
   End
   Begin VB.Label lblFich 
      BackColor       =   &H00808000&
      Caption         =   "Fichero adjunto:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1450
      TabIndex        =   0
      Top             =   120
      Width           =   3135
   End
End
Attribute VB_Name = "frmCiaComFich"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_sOrigen As String

Private Sub cmdAceptar_Click()
    
    If g_sOrigen = "frmCompanias" Then
        frmCompanias.g_sComentario = txtCom.Text
    End If

    Unload Me

End Sub

Private Sub cmdCancelar_Click()
    
    If g_sOrigen = "frmCompanias" Then
        frmCompanias.g_bCancelarEsp = True
    End If
    
    Unload Me
    
End Sub

Private Sub Form_Activate()
    txtCom.SetFocus
End Sub

Private Sub Form_Load()
    CargarRecursos
    Me.Top = frmMDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = frmMDI.ScaleWidth / 2 - Me.Width / 2

End Sub
Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CIACOMFICH, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdAceptar.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdCancelar.Caption = ador(0).Value
        ador.MoveNext
        Me.Label1.Caption = ador(0).Value
        ador.Close
    End If
    
    Set ador = Nothing
           
End Sub

