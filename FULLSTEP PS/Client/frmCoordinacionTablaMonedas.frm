VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCoordinacionTablaMonedas 
   Caption         =   "Coordinaci�n de tabla de monedas"
   ClientHeight    =   3390
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9690
   Icon            =   "frmCoordinacionTablaMonedas.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   3390
   ScaleWidth      =   9690
   Begin VB.CommandButton Cmdlistado 
      Caption         =   "&Listado"
      Height          =   345
      Left            =   75
      TabIndex        =   4
      Top             =   3060
      Width           =   1005
   End
   Begin VB.CommandButton cmdModoEdicion 
      Caption         =   "&Edici�n"
      Height          =   345
      Left            =   8625
      TabIndex        =   3
      Top             =   3000
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddMonPortCod 
      Height          =   1545
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   4965
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2699
      Columns(0).Caption=   "C�digo en el portal"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5662
      Columns(1).Caption=   "Den.en el portal"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   8758
      _ExtentY        =   2725
      _StockProps     =   77
      ForeColor       =   0
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTabla 
      Height          =   2865
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   9570
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   4
      stylesets.count =   2
      stylesets(0).Name=   "Central"
      stylesets(0).BackColor=   16777088
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCoordinacionTablaMonedas.frx":0CB2
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmCoordinacionTablaMonedas.frx":0CCE
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   2990
      Columns(0).Caption=   "C�digo de FullStepGS"
      Columns(0).Name =   "COD_FSGS"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   3200
      Columns(1).Caption=   "Den. de FullStepGS"
      Columns(1).Name =   "DEN_FSGS"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   2725
      Columns(2).Caption=   "C�digo del portal"
      Columns(2).Name =   "COD_FSP"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "Den. del portal"
      Columns(3).Name =   "DEN_FSP"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   16880
      _ExtentY        =   5054
      _StockProps     =   79
      ForeColor       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddMonPortDen 
      Height          =   1545
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   4965
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5662
      Columns(0).Caption=   "Den.en el portal"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2699
      Columns(1).Caption=   "C�digo en el portal"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   8758
      _ExtentY        =   2725
      _StockProps     =   77
      ForeColor       =   0
      DataFieldToDisplay=   "Column 0"
   End
End
Attribute VB_Name = "frmCoordinacionTablaMonedas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public oCia As CCia
Private IdMonPortalSeleccionado As Integer
Private bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean
Private bHayErrores As Boolean
Private bRespetarDropdown As Boolean
Private oCias As CCias
Private bCentralSinEnlazar As Boolean

'Moneda central de GS para la Cia
Public Mon As String

Private sIdiEdicion As String
Private sIdiConsulta As String


Private Sub cmdListado_Click()

 ''' * Objetivo: Obtener un listado de provincias de un pa�s o paises
    
    frmLstCoorMon.WindowState = vbNormal
    Set frmLstCoorMon.oCiaSeleccionada = oCia
    If Not oCia Is Nothing Then
       frmLstCoorMon.sdbcCiaCod.Text = oCia.Cod
       frmLstCoorMon.sdbcCiaCod_Validate False
       
    End If
frmLstCoorMon.SetFocus

End Sub

Private Sub cmdModoEdicion_Click()
    
    If sdbgTabla.AllowUpdate Then
        'Pasar a modo consulta
        sdbgTabla.Update
        DoEvents
        
        If bCentralSinEnlazar = True Then
            basMensajes.EnlazarCentral
            bHayErrores = True
        End If
        
        If Not bHayErrores Then
            sdbgTabla.AllowUpdate = False
            cmdModoEdicion.Caption = sIdiEdicion
        End If
        
    Else
        'Pasar a modo edici�n
        sdbgTabla.AllowUpdate = True
        cmdModoEdicion.Caption = sIdiConsulta
    End If
    
End Sub

Private Sub Form_Load()
Dim i As Integer
    
    Width = 7335
    Height = 3930
    CargarRecursos
    sdbgTabla.Columns("COD_FSP").DropDownHwnd = sdbddMonPortCod.HWND
    sdbddMonPortCod.AddItem ""
    sdbgTabla.Columns("DEN_FSP").DropDownHwnd = sdbddMonPortDen.HWND
    sdbddMonPortDen.AddItem ""
    sdbgTabla.AllowUpdate = False
    cmdModoEdicion.Visible = True
    
    'Set oCias = g_oRaiz.Generar_CCias
    
End Sub


Private Sub Form_Resize()
    
    If Me.Height < 1000 Then Exit Sub
    If Me.Width < 1205 Then Exit Sub
    
    sdbgTabla.Width = Me.Width - 200
    sdbgTabla.Height = Me.Height - 1000
    
    sdbgTabla.Columns("COD_FSGS").Width = sdbgTabla.Width * 0.2
    sdbgTabla.Columns("DEN_FSGS").Width = sdbgTabla.Width * 0.3
    sdbgTabla.Columns("COD_FSP").Width = sdbgTabla.Width * 0.2
    sdbgTabla.Columns("DEN_FSP").Width = sdbgTabla.Width * 0.3 - 570
    cmdModoEdicion.Left = sdbgTabla.Width - 1005
    cmdModoEdicion.Top = Me.Height - 850
    cmdListado.Left = sdbgTabla.Left
    cmdListado.Top = Me.Height - 850
    
    
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set oCia = Nothing
End Sub

Private Sub sdbddMonPortCod_CloseUp()
    
    sdbgTabla.Columns("DEN_FSP").Value = sdbddMonPortCod.Columns("DEN").Value
    
End Sub

Private Sub sdbddMonPortCod_DropDown()
Dim ador As ador.Recordset
    
    If sdbgTabla.AllowUpdate = False Then
       sdbddMonPortCod.DroppedDown = False
    End If
    
    
    sdbddMonPortCod.RemoveAll
    
    Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, sdbgTabla.ActiveCell.Value, , False, True)
    
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbddMonPortCod.AddItem ador("COD").Value & Chr(9) & ador("DEN").Value
            ador.MoveNext
        
        Wend
        
        ador.Close
        Set ador = Nothing
        
    End If
    
        
        
    
End Sub

Private Sub sdbddMonPortCod_InitColumnProps()
    
    sdbddMonPortCod.DataFieldList = "Column 0"
    sdbddMonPortCod.DataFieldToDisplay = "Column 0"
    
    'sdbddArticulos.DataFieldList = "Column 0"
    'sdbddArticulos.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddMonPortCod_PositionList(ByVal Text As String)
      
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddMonPortCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddMonPortCod.Rows - 1
            bm = sdbddMonPortCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddMonPortCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddMonPortCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddMonPortCod_ValidateList(Text As String, RtnPassed As Integer)
Dim scod As String
Dim ador As ador.Recordset
    
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True
        sdbgTabla.Columns("DEN_FSP").Value = ""
    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddMonPortCod.Columns("COD").Value = Text Then
            RtnPassed = True
            sdbgTabla.Columns("DEN_FSP").Value = sdbddMonPortCod.Columns("DEN").Value
            Exit Sub
        Else
            Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, sdbgTabla.Columns("COD_FSP").Value, , True)
            
            If ador Is Nothing Then
                RtnPassed = 0
            Else
                If ador.EOF Then
                    RtnPassed = 0
                Else
                    sdbgTabla.Columns("COD_FSP").Value = ador("COD").Value
                    sdbgTabla.Columns("DEN_FSP").Value = ador("DEN").Value
                        
                    RtnPassed = 1
                End If
            End If
            
            ador.Close
            Set ador = Nothing
        End If
        
    End If
End Sub

Private Sub sdbddMonPortDen_CloseUp()
    
    
    sdbgTabla.Columns("COD_FSP").Value = sdbddMonPortDen.Columns("COD").Value
    
End Sub

Private Sub sdbddMonPortDen_DropDown()
    Dim ador As ador.Recordset
    
    If sdbgTabla.AllowUpdate = False Then
       sdbddMonPortDen.DroppedDown = False
    End If
    
    
    sdbddMonPortDen.RemoveAll
    
    Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbgTabla.ActiveCell.Value, , False)
    
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbddMonPortDen.AddItem ador("DEN").Value & Chr(9) & ador("COD").Value
            ador.MoveNext
        
        Wend
        ador.Close
        Set ador = Nothing
        
    End If
    
        
End Sub

Private Sub sdbddMonPortDen_InitColumnProps()

    sdbddMonPortDen.DataFieldList = "Column 0"
    sdbddMonPortDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbddMonPortDen_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddMonPortDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddMonPortDen.Rows - 1
            bm = sdbddMonPortCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddMonPortDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddMonPortDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddMonPortDen_ValidateList(Text As String, RtnPassed As Integer)
    Dim scod As String
    Dim ador As ador.Recordset
    
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True
        sdbgTabla.Columns("COD_FSP").Value = ""
    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddMonPortDen.Columns("DEN").Value = Text Then
            RtnPassed = True
            sdbgTabla.Columns("COD_FSP").Value = sdbddMonPortDen.Columns("COD").Value
            Exit Sub
        Else
            Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbgTabla.Columns("DEN_FSP").Value, True)
            
            If ador Is Nothing Then
                RtnPassed = True
                sdbgTabla.Columns("COD_FSP").Value = ""
                sdbgTabla.Columns("DEN_FSP").Value = ""
            Else
                If ador.EOF Then
                    RtnPassed = True
                    sdbgTabla.Columns("COD_FSP").Value = ""
                    sdbgTabla.Columns("DEN_FSP").Value = ""
                Else
                    sdbgTabla.Columns("COD_FSP").Value = ador("COD").Value
                    sdbgTabla.Columns("DEN_FSP").Value = ador("DEN").Value
                        
                    RtnPassed = 1
                End If
            End If
            
            ador.Close
            Set ador = Nothing
        End If
        
    End If
    
    
    
End Sub

Private Sub sdbgTabla_BeforeUpdate(Cancel As Integer)
Dim oError As CTESError

    'Enlaza
    If sdbgTabla.Columns("COD_FSP").Value <> "" And sdbgTabla.Columns("DEN_FSP").Value <> "" Then
        If sdbgTabla.Columns("COD_FSGS").Value = Mon Then  'Si va a enlazar la moneda central
            Set oError = oCia.EnlazarMoneda(sdbgTabla.Columns("COD_FSGS").Value, sdbgTabla.Columns("COD_FSP").Value, True, sdbgTabla.Columns("DEN_FSGS").Value)
            If oError.numerror = 0 Then
                bCentralSinEnlazar = False
            End If
        Else
            Set oError = oCia.EnlazarMoneda(sdbgTabla.Columns("COD_FSGS").Value, sdbgTabla.Columns("COD_FSP").Value)
        End If
        If oError.numerror <> 0 Then
            basErrores.TratarError oError
            bHayErrores = True
            Cancel = True
        Else
            bHayErrores = False
        End If
        
    Else  'Desenlaza
        If sdbgTabla.Columns("COD_FSP").Value = "" Then
        
           If sdbgTabla.Columns("COD_FSGS").Value = Mon And Not bCentralSinEnlazar Then
                'Si es la moneda central no se puede desenlazar
                basMensajes.DesenlazarCentral
                bHayErrores = True
                Cancel = True
                Exit Sub
           End If
           
           Set oError = oCia.EnlazarMoneda(sdbgTabla.Columns("COD_FSGS").Value, "")
           If oError.numerror <> 0 Then
                basErrores.TratarError (oError)
                bHayErrores = True
                Cancel = True
            Else
                sdbgTabla.Columns("DEN_FSP").Value = ""
                bHayErrores = False
            End If
        Else
            Cancel = True
            bHayErrores = True
        End If
    End If
    
End Sub

Private Sub sdbgTabla_Change()
    
    'If Not bRespetarDropdown Then
     '   sdbgTabla.Columns("COD_FSP").Value = ""
      '  sdbgTabla.Columns("DEN_FSP").Value = ""
    'End If
    
End Sub

Private Sub sdbgTabla_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyEscape Then
        sdbgTabla.CancelUpdate
        bHayErrores = False
    End If
     
End Sub


Private Sub sdbgTabla_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    'Si es la moneda central bloqueo la edici�n
    If sdbgTabla.Columns("COD_FSGS").Value = Mon And Not bCentralSinEnlazar Then
        sdbgTabla.Columns("COD_FSP").Locked = True
        sdbgTabla.Columns("DEN_FSP").Locked = True
        Me.sdbddMonPortCod.Enabled = False
        sdbddMonPortDen.Enabled = False
    Else
        sdbgTabla.Columns("COD_FSP").Locked = False
        sdbgTabla.Columns("DEN_FSP").Locked = False
        sdbddMonPortCod.Enabled = True
        sdbddMonPortDen.Enabled = True
    End If
End Sub

Private Sub sdbgTabla_RowLoaded(ByVal Bookmark As Variant)
    'La fila correspondiente a la moneda central de GS para
    'la compa��a se pone en azul
    If sdbgTabla.Columns("COD_FSGS").Value = Mon Then
        sdbgTabla.Columns(0).CellStyleSet "Central"
        sdbgTabla.Columns(1).CellStyleSet "Central"
        sdbgTabla.Columns(2).CellStyleSet "Central"
        sdbgTabla.Columns(3).CellStyleSet "Central"
        
        If sdbgTabla.Columns("COD_FSP").Value = "" Then
            bCentralSinEnlazar = True
        End If
        
    End If
End Sub

Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_COORDINACIONTABLAMONEDAS, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdListado.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdModoEdicion.Caption = ador(0).Value
        sIdiEdicion = ador(0).Value
        ador.MoveNext
        
        Me.sdbddMonPortCod.Columns(0).Caption = ador(0).Value
        Me.sdbddMonPortDen.Columns(1).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbddMonPortCod.Columns(1).Caption = ador(0).Value
        Me.sdbddMonPortDen.Columns(0).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgTabla.Columns(0).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgTabla.Columns(1).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgTabla.Columns(2).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgTabla.Columns(3).Caption = ador(0).Value
        ador.MoveNext
        sIdiConsulta = ador(0).Value
        ador.Close
    End If
    
    Set ador = Nothing
           
End Sub

