Attribute VB_Name = "basPublic"

Public g_oRaiz As CRaiz
Public g_oGestorInformes As CGestorInformes
Public g_sInstancia As String
Public g_sBaseDeDatos As String
Public g_sServidor As String

Public g_oGestorParametros As cGestorParametros
Public g_oMailSender As Email

Public g_oGestorIdiomas As FSPSIdiomas.CGestorIdiomas

Public Const ClavePar5Bytes = "agkag�"
Public Const ClaveImp5bytes = "h+hlL_"

Public Const LEN_PYME = 3

Public g_sCodADM As String
Public g_oGestorAcciones As CGestorAcciones

Public Const c_iNumLitRegAccion As Integer = 55
Public g_sLitRegAccion_SPA(0 To c_iNumLitRegAccion) As String
Public g_sLitRegAccion_ENG(0 To c_iNumLitRegAccion) As String
Public g_sLitRegAccion_FRA(0 To c_iNumLitRegAccion) As String
Public g_sLitRegAccion_GER(0 To c_iNumLitRegAccion) As String
