VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmActiv 
   Caption         =   "Actividades"
   ClientHeight    =   5595
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9000
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmActiv.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5595
   ScaleWidth      =   9000
   Begin VB.PictureBox picNavigate 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   90
      ScaleHeight     =   405
      ScaleWidth      =   6720
      TabIndex        =   0
      Top             =   5160
      Width           =   6720
      Begin VB.CommandButton cmdEli 
         Caption         =   "&Eliminar"
         Height          =   345
         Left            =   2160
         TabIndex        =   6
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdModif 
         Caption         =   "&Modificar"
         Height          =   345
         Left            =   1095
         TabIndex        =   5
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Height          =   345
         Left            =   3240
         TabIndex        =   4
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   345
         Left            =   4320
         TabIndex        =   3
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         Height          =   345
         Left            =   5400
         TabIndex        =   2
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         Height          =   345
         Left            =   0
         TabIndex        =   1
         Top             =   0
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcIdiCod 
      Height          =   285
      Left            =   930
      TabIndex        =   8
      Top             =   240
      Width           =   915
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1111
      Columns(1).Caption=   "C�digo"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3545
      Columns(2).Caption=   "Denominaci�n"
      Columns(2).Name =   "DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   1614
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcIdiDen 
      Height          =   285
      Left            =   1860
      TabIndex        =   9
      Top             =   240
      Width           =   2535
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3493
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1138
      Columns(2).Caption=   "C�digo"
      Columns(2).Name =   "COD"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   4471
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin MSComctlLib.TreeView tvwEstrAct 
      Height          =   4380
      Left            =   75
      TabIndex        =   10
      Top             =   690
      Width           =   8820
      _ExtentX        =   15558
      _ExtentY        =   7726
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmActiv.frx":014A
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmActiv.frx":059E
            Key             =   "ACT"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmActiv.frx":06B0
            Key             =   "ACTASIG"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmActiv.frx":07C2
            Key             =   "Raiz2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmActiv.frx":0C14
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmActiv.frx":0F68
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmActiv.frx":12BC
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmActiv.frx":1610
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmActiv.frx":1964
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmActiv.frx":1CB8
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmActiv.frx":200C
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmActiv.frx":2360
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblIdioma 
      Caption         =   "Idioma:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   7
      Top             =   240
      Width           =   1125
   End
End
Attribute VB_Name = "frmActiv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_bCargarComboDesde As Boolean
Private m_bRespetarCombo As Boolean
Public g_udtAccion As accionesps

Public g_oIBaseDatos As IBaseDatos
Public g_oACN1Seleccionado As CActividadNivel1
Public g_oACN2Seleccionado As CActividadNivel2
Public g_oACN3Seleccionado As CActividadNivel3
Public g_oACN4Seleccionado As CActividadNivel4
Public g_oACN5Seleccionado As CActividadNivel5
Public g_bCodigoCancelar As Boolean
Public g_sCodigoNuevo As String

Private sIdiCodigo As String
Private sIdiAnyaActN1 As String
Private sIdiAnyaActN2 As String
Private sIdiAnyaActN3 As String
Private sIdiAnyaActN4 As String
Private sIdiAnyaActN5 As String
Private sIdiModifActN1 As String
Private sIdiModifActN2 As String
Private sIdiModifActN3 As String
Private sIdiModifActN4 As String
Private sIdiModifActN5 As String
Private sIdiActividades As String
Private sIdiCargandoAct As String
Private sIdiActN1 As String
Private sIdiActN2 As String
Private sIdiActN3 As String
Private sIdiActN4 As String
Private sIdiActN5 As String
Private sIdiDetActN1 As String
Private sIdiDetActN2 As String
Private sIdiDetActN3 As String
Private sIdiDetActN4 As String
Private sIdiDetActN5 As String
Private sIdiActividad As String



Public Sub Ordenar(ByVal bOrdPorDen As Boolean)
    tvwEstrAct.Nodes.Clear
    GenerarEstrActividades bOrdPorDen, sdbcIdiCod.Text
End Sub

Public Sub cmdA�adir_Click()
Dim nodx As MSComctlLib.Node
    
    Set nodx = tvwEstrAct.SelectedItem
    
    If Not nodx Is Nothing Then
        
        Select Case Left(nodx.Tag, 4)
        
            Case "Raiz"
                        g_udtAccion = ACCACT1Anya
                        frmESTRACTDetalle.Caption = sIdiAnyaActN1
                        frmESTRACTDetalle.Show 1
            
            Case "ACN1"
                        g_udtAccion = ACCACT2Anya
                        frmESTRACTDetalle.Caption = sIdiAnyaActN2
                        frmESTRACTDetalle.Show 1
                        
            Case "ACN2"
                        g_udtAccion = ACCACT3Anya
                        frmESTRACTDetalle.Caption = sIdiAnyaActN3
                        frmESTRACTDetalle.Show 1
                           
            Case "ACN3"
                        g_udtAccion = ACCACT4Anya
                        frmESTRACTDetalle.Caption = sIdiAnyaActN4
                        frmESTRACTDetalle.Show 1

            Case "ACN4"
                        g_udtAccion = ACCACT5Anya
                        frmESTRACTDetalle.Caption = sIdiAnyaActN5
                        frmESTRACTDetalle.Show 1
        End Select
          
    End If
    
    Set nodx = Nothing
Me.tvwEstrAct.SetFocus

End Sub



Private Sub cmdBuscar_Click()
 frmESTRACTBuscar.Show 1
End Sub

Public Sub cmdEli_Click()
Dim nodx As MSComctlLib.Node
Dim teserror As CTESError
Dim iRespuesta As Integer
Dim bHayPres As Boolean
Dim bHayProve As Boolean
Dim bHayComp As Boolean
Dim adors As Ador.Recordset

Set nodx = tvwEstrAct.SelectedItem


If Not nodx Is Nothing Then
        
    Select Case Left(nodx.Tag, 4)
        
    Case "ACN1"
            
             
            g_udtAccion = ACCACT1Eli
            
            Set g_oACN1Seleccionado = Nothing
            Set g_oACN1Seleccionado = g_oRaiz.Generar_CActividadNivel1
            g_oACN1Seleccionado.Id = DevolverId(nodx)
         
            Set g_oIBaseDatos = g_oACN1Seleccionado
            
            Screen.MousePointer = vbHourglass
            Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
            Screen.MousePointer = vbNormal
            
            If teserror.numerror = TESnoerror Then
                
                iRespuesta = basMensajes.PreguntaEliminar(sIdiActividad & " " & CStr(g_oACN1Seleccionado.Cod) & " (" & g_oACN1Seleccionado.Den & ")")
                
                If iRespuesta = vbYes Then
                    'Mirar si existen compa�ias asignadas
                    Set adors = g_oACN1Seleccionado.DevolverCiasAsignadas
                    
                    If adors Is Nothing Then

                        Screen.MousePointer = vbHourglass
                        Set teserror = g_oIBaseDatos.EliminarDeBaseDatos
                        Screen.MousePointer = vbNormal
                        
                        If teserror.numerror <> TESnoerror Then
                            TratarError teserror
                            Exit Sub
                        Else
                            EliminaraACTEstr
                            Set g_oIBaseDatos = Nothing
                            Set g_oACN1Seleccionado = Nothing
                        End If
                    
                    Else
                    
                        iRespuesta = basMensajes.CompAsig(adors)
                        
                        If iRespuesta = vbYes Then
                        
                            Screen.MousePointer = vbHourglass
                            Set teserror = g_oIBaseDatos.EliminarDeBaseDatos
                            Screen.MousePointer = vbNormal
                            
                            If teserror.numerror <> TESnoerror Then
                                TratarError teserror
                                Exit Sub
                            Else
                                EliminaraACTEstr
                                Set g_oIBaseDatos = Nothing
                                Set g_oACN1Seleccionado = Nothing
                            End If
                        End If
                        
                        If iRespuesta = vbNo Then
                            Exit Sub
                        End If
                    End If
                    
                Else
                    Set g_oIBaseDatos = Nothing
                    Set g_oACN1Seleccionado = Nothing
                End If
            
            Else
                'Ha habido un error al iniciar la edicion
                
                TratarError teserror
                Set g_oIBaseDatos = Nothing
                Set g_oACN1Seleccionado = Nothing
                Exit Sub
            
            End If
    
    Case "ACN2"
            
             
            g_udtAccion = ACCACT2Eli
            
            Set g_oACN2Seleccionado = Nothing
            Set g_oACN2Seleccionado = g_oRaiz.Generar_CActividadNivel2
            g_oACN2Seleccionado.ACN1 = DevolverId(nodx.Parent)
            g_oACN2Seleccionado.Id = DevolverId(nodx)
         
            Set g_oIBaseDatos = g_oACN2Seleccionado
            
            Screen.MousePointer = vbHourglass
            Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
            Screen.MousePointer = vbNormal
            
            If teserror.numerror = TESnoerror Then
                iRespuesta = basMensajes.PreguntaEliminar(sIdiActividad & " " & CStr(g_oACN2Seleccionado.Cod) & " (" & g_oACN2Seleccionado.Den & ")")
                
                If iRespuesta = vbYes Then
                    'Mirar si existen compa�ias asignadas
                    Set adors = g_oACN2Seleccionado.DevolverCiasAsignadas

                    If adors Is Nothing Then

                        Screen.MousePointer = vbHourglass
                        Set teserror = g_oIBaseDatos.EliminarDeBaseDatos
                        Screen.MousePointer = vbNormal
                        
                        If teserror.numerror <> TESnoerror Then
                            TratarError teserror
                            Exit Sub
                        Else
                            EliminaraACTEstr
                            'RegistrarAccion ACCGMN1Eli, "Cod:" & CStr(g_oACN1Seleccionado.Cod)
                            Set g_oIBaseDatos = Nothing
                            Set g_oACN2Seleccionado = Nothing
                        End If
                    
                    Else

                        iRespuesta = basMensajes.CompAsig(adors)

                        If iRespuesta = vbYes Then

                            Screen.MousePointer = vbHourglass
                            Set teserror = g_oIBaseDatos.EliminarDeBaseDatos
                            Screen.MousePointer = vbNormal

                            If teserror.numerror <> TESnoerror Then
                                TratarError teserror
                                Exit Sub
                            Else
                                EliminaraACTEstr
                                'RegistrarAccion ACCGMN1Eli, "Cod:" & CStr(g_oACN1Seleccionado.Cod)
                                Set g_oIBaseDatos = Nothing
                                Set g_oACN2Seleccionado = Nothing
                            End If
                        End If
                        
                        If iRespuesta = vbNo Then
                            Exit Sub
                        End If


                    End If
                    
                Else
                    Set g_oIBaseDatos = Nothing
                    Set g_oACN2Seleccionado = Nothing
                End If
            
            Else
                'Ha habido un error al iniciar la edicion
                
                TratarError teserror
                Set g_oIBaseDatos = Nothing
                Set g_oACN2Seleccionado = Nothing
                Exit Sub
            
            End If
            
    Case "ACN3"
            
             
            g_udtAccion = ACCACT3Eli
            
            Set g_oACN3Seleccionado = Nothing
            Set g_oACN3Seleccionado = g_oRaiz.Generar_CActividadNivel3
            g_oACN3Seleccionado.ACN1 = DevolverId(nodx.Parent.Parent)
            g_oACN3Seleccionado.ACN2 = DevolverId(nodx.Parent)
            g_oACN3Seleccionado.Id = DevolverId(nodx)
         
            Set g_oIBaseDatos = g_oACN3Seleccionado
            
            Screen.MousePointer = vbHourglass
            Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
            Screen.MousePointer = vbNormal
            
            If teserror.numerror = TESnoerror Then
                
                iRespuesta = basMensajes.PreguntaEliminar(sIdiActividad & " " & CStr(g_oACN3Seleccionado.Cod) & " (" & g_oACN3Seleccionado.Den & ")")
                
                If iRespuesta = vbYes Then
                    'Mirar si existen compa�ias asignadas
                    Set adors = g_oACN3Seleccionado.DevolverCiasAsignadas

                    If adors Is Nothing Then

                        Screen.MousePointer = vbHourglass
                        Set teserror = g_oIBaseDatos.EliminarDeBaseDatos
                        Screen.MousePointer = vbNormal
                        
                        If teserror.numerror <> TESnoerror Then
                            TratarError teserror
                            Exit Sub
                        Else
                            EliminaraACTEstr
                            'RegistrarAccion ACCGMN1Eli, "Cod:" & CStr(g_oACN1Seleccionado.Cod)
                            Set g_oIBaseDatos = Nothing
                            Set g_oACN3Seleccionado = Nothing
                        End If
                    
                    Else
'
                        iRespuesta = basMensajes.CompAsig(adors)

                        If iRespuesta = vbYes Then

                            Screen.MousePointer = vbHourglass
                            Set teserror = g_oIBaseDatos.EliminarDeBaseDatos
                            Screen.MousePointer = vbNormal

                            If teserror.numerror <> TESnoerror Then
                                TratarError teserror
                                Exit Sub
                            Else
                                EliminaraACTEstr
'                                'RegistrarAccion ACCGMN1Eli, "Cod:" & CStr(g_oACN1Seleccionado.Cod)
                                Set g_oIBaseDatos = Nothing
                                Set g_oACN3Seleccionado = Nothing
                            End If
                        End If
'
                        If iRespuesta = vbNo Then
                            Exit Sub
                        End If


                    End If
                    
                Else
                    Set g_oIBaseDatos = Nothing
                    Set g_oACN3Seleccionado = Nothing
                End If
            
            Else
                'Ha habido un error al iniciar la edicion
                
                TratarError teserror
                Set g_oIBaseDatos = Nothing
                Set g_oACN3Seleccionado = Nothing
                Exit Sub
            
            End If
    
    Case "ACN4"
            
             
            g_udtAccion = ACCACT4Eli
            
            Set g_oACN4Seleccionado = Nothing
            Set g_oACN4Seleccionado = g_oRaiz.Generar_CActividadNivel4
            g_oACN4Seleccionado.ACN1 = DevolverId(nodx.Parent.Parent.Parent)
            g_oACN4Seleccionado.ACN2 = DevolverId(nodx.Parent.Parent)
            g_oACN4Seleccionado.ACN3 = DevolverId(nodx.Parent)
            g_oACN4Seleccionado.Id = DevolverId(nodx)
         
            Set g_oIBaseDatos = g_oACN4Seleccionado
            
            Screen.MousePointer = vbHourglass
            Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
            Screen.MousePointer = vbNormal
            
            If teserror.numerror = TESnoerror Then
                    
                iRespuesta = basMensajes.PreguntaEliminar(sIdiActividad & " " & CStr(g_oACN4Seleccionado.Cod) & " (" & g_oACN4Seleccionado.Den & ")")
                
                If iRespuesta = vbYes Then
                    'Mirar si existen compa�ias asignadas
                    Set adors = g_oACN4Seleccionado.DevolverCiasAsignadas
                    
                    If adors Is Nothing Then

                        Screen.MousePointer = vbHourglass
                        Set teserror = g_oIBaseDatos.EliminarDeBaseDatos
                        Screen.MousePointer = vbNormal
                        
                        If teserror.numerror <> TESnoerror Then
                            TratarError teserror
                            Exit Sub
                        Else
                            EliminaraACTEstr
                            'RegistrarAccion ACCGMN1Eli, "Cod:" & CStr(g_oACN1Seleccionado.Cod)
                            Set g_oIBaseDatos = Nothing
                            Set g_oACN4Seleccionado = Nothing
                        End If
                    
                    Else
                    
                        iRespuesta = basMensajes.CompAsig(adors)
                        
                        If iRespuesta = vbYes Then
                        
                            Screen.MousePointer = vbHourglass
                            Set teserror = g_oIBaseDatos.EliminarDeBaseDatos
                            Screen.MousePointer = vbNormal
                            
                            If teserror.numerror <> TESnoerror Then
                                TratarError teserror
                                Exit Sub
                            Else
                                EliminaraACTEstr
                                'RegistrarAccion ACCGMN1Eli, "Cod:" & CStr(g_oACN1Seleccionado.Cod)
                                Set g_oIBaseDatos = Nothing
                                Set g_oACN4Seleccionado = Nothing
                            End If
                        End If
                        
                        If iRespuesta = vbNo Then
                            Exit Sub
                        End If
                        
                    
                    End If
                    
                Else
                    Set g_oIBaseDatos = Nothing
                    Set g_oACN4Seleccionado = Nothing
                End If
            
            Else
                'Ha habido un error al iniciar la edicion
                
                TratarError teserror
                Set g_oIBaseDatos = Nothing
                Set g_oACN4Seleccionado = Nothing
                Exit Sub
            
            End If
            
    Case "ACN5"
            
             
            g_udtAccion = ACCACT5Eli
            
            Set g_oACN5Seleccionado = Nothing
            Set g_oACN5Seleccionado = g_oRaiz.Generar_CActividadNivel5
            g_oACN5Seleccionado.ACN1 = DevolverId(nodx.Parent.Parent.Parent.Parent)
            g_oACN5Seleccionado.ACN2 = DevolverId(nodx.Parent.Parent.Parent)
            g_oACN5Seleccionado.ACN3 = DevolverId(nodx.Parent.Parent)
            g_oACN5Seleccionado.ACN4 = DevolverId(nodx.Parent)
            g_oACN5Seleccionado.Id = DevolverId(nodx)
         
            Set g_oIBaseDatos = g_oACN5Seleccionado
            
            Screen.MousePointer = vbHourglass
            Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
            Screen.MousePointer = vbNormal
            
            If teserror.numerror = TESnoerror Then
                iRespuesta = basMensajes.PreguntaEliminar(sIdiActividad & " " & CStr(g_oACN5Seleccionado.Cod) & " (" & g_oACN5Seleccionado.Den & ")")
                
                If iRespuesta = vbYes Then
                    'Mirar si existen compa�ias asignadas
                    Set adors = g_oACN5Seleccionado.DevolverCiasAsignadas
                    
                    If adors Is Nothing Then

                        Screen.MousePointer = vbHourglass
                        Set teserror = g_oIBaseDatos.EliminarDeBaseDatos
                        Screen.MousePointer = vbNormal
                        
                        If teserror.numerror <> TESnoerror Then
                            TratarError teserror
                            Exit Sub
                        Else
                            EliminaraACTEstr
                            'RegistrarAccion ACCGMN1Eli, "Cod:" & CStr(g_oACN1Seleccionado.Cod)
                            Set g_oIBaseDatos = Nothing
                            Set g_oACN5Seleccionado = Nothing
                        End If
                    
                    Else
                    
                        iRespuesta = basMensajes.CompAsig(adors)
                        
                        If iRespuesta = vbYes Then
                        
                            Screen.MousePointer = vbHourglass
                            Set teserror = g_oIBaseDatos.EliminarDeBaseDatos
                            Screen.MousePointer = vbNormal
                            
                            If teserror.numerror <> TESnoerror Then
                                TratarError teserror
                                Exit Sub
                            Else
                                EliminaraACTEstr
                                'RegistrarAccion ACCGMN1Eli, "Cod:" & CStr(g_oACN1Seleccionado.Cod)
                                Set g_oIBaseDatos = Nothing
                                Set g_oACN5Seleccionado = Nothing
                            End If
                        End If
                        
                        If iRespuesta = vbNo Then
                            Exit Sub
                        End If
                        
                    
                    End If
                    
                Else
                    Set g_oIBaseDatos = Nothing
                    Set g_oACN5Seleccionado = Nothing
                End If
            
            Else
                'Ha habido un error al iniciar la edicion
                
                TratarError teserror
                Set g_oIBaseDatos = Nothing
                Set g_oACN5Seleccionado = Nothing
                Exit Sub
            
            End If

    End Select
    

End If
End Sub
Private Function EliminaraACTEstr()
Dim nod As Node
Dim nodSiguiente As MSComctlLib.Node

Set nod = tvwEstrAct.SelectedItem

    If Not nod.Previous Is Nothing Then
        Set nodSiguiente = nod.Previous
    Else
        If Not nod.Next Is Nothing Then
            Set nodSiguiente = nod.Next
            Else
                Set nodSiguiente = nod.Parent
        End If
    End If


    tvwEstrAct.Nodes.Remove (tvwEstrAct.SelectedItem.Index)


tvwEstrAct_NodeClick nodSiguiente
nodSiguiente.Selected = True
tvwEstrAct.SetFocus

End Function

Private Sub cmdListado_Click()

Dim nodx As MSComctlLib.Node
Dim scod As String
Dim sDen As String
    
    ''' * Objetivo: Obtener un listado, pasar a selecci�n de par�metros listados
    
    
    If Not frmMDI.mnuOrdCod.Checked Then
        frmLstACT.optOrdDen.Value = True
    End If
    frmLstACT.g_sIdioma = sdbcIdiCod.Text
    
    Set nodx = tvwEstrAct.SelectedItem
    If nodx Is Nothing Then
        frmLstACT.optTodos.Value = True
        Exit Sub
    Else
        If nodx.Tag = "Raiz" Then
            frmLstACT.optTodos.Value = True
            Exit Sub
        End If
        frmLstACT.optRama.Value = True
        Screen.MousePointer = vbHourglass
      
        Select Case Left(nodx.Tag, 4)
        Case "ACN1"
                            
                frmLstACT.g_oACN1Seleccionado.Id = DevolverId(nodx)
                DevolverCodDen nodx, scod, sDen
                frmLstACT.g_oACN1Seleccionado.Cod = scod
                frmLstACT.g_oACN1Seleccionado.Den = sDen
                Set frmLstACT.g_oACN2Seleccionado = Nothing
                Set frmLstACT.g_oACN3Seleccionado = Nothing
                Set frmLstACT.g_oACN4Seleccionado = Nothing
                Set frmLstACT.g_oACN5Seleccionado = Nothing
    
         Case "ACN2"
                DevolverCodDen nodx.Parent, scod, sDen
                frmLstACT.g_oACN1Seleccionado.Cod = scod
                frmLstACT.g_oACN2Seleccionado.Id = DevolverId(nodx)
                frmLstACT.g_oACN2Seleccionado.ACN1 = DevolverId(nodx.Parent)
                DevolverCodDen nodx, scod, sDen
                frmLstACT.g_oACN2Seleccionado.Cod = scod
                frmLstACT.g_oACN2Seleccionado.Den = sDen
    
                Set frmLstACT.g_oACN3Seleccionado = Nothing
                Set frmLstACT.g_oACN4Seleccionado = Nothing
                Set frmLstACT.g_oACN5Seleccionado = Nothing
                                        
            Case "ACN3"
                DevolverCodDen nodx.Parent.Parent, scod, sDen
                frmLstACT.g_oACN1Seleccionado.Cod = scod
                
                DevolverCodDen nodx.Parent, scod, sDen
                frmLstACT.g_oACN2Seleccionado.Cod = scod
    
                frmLstACT.g_oACN3Seleccionado.Id = DevolverId(nodx)
                frmLstACT.g_oACN3Seleccionado.ACN1 = DevolverId(nodx.Parent.Parent)
                frmLstACT.g_oACN3Seleccionado.ACN2 = DevolverId(nodx.Parent)
                DevolverCodDen nodx, scod, sDen
                frmLstACT.g_oACN3Seleccionado.Cod = scod
                frmLstACT.g_oACN3Seleccionado.Den = sDen
                Set frmLstACT.g_oACN4Seleccionado = Nothing
                Set frmLstACT.g_oACN5Seleccionado = Nothing
                            
            Case "ACN4"
                DevolverCodDen nodx.Parent.Parent.Parent, scod, sDen
                frmLstACT.g_oACN1Seleccionado.Cod = scod
                DevolverCodDen nodx.Parent.Parent, scod, sDen
                frmLstACT.g_oACN2Seleccionado.Cod = scod
                DevolverCodDen nodx.Parent, scod, sDen
                frmLstACT.g_oACN3Seleccionado.Cod = scod
                frmLstACT.g_oACN4Seleccionado.Id = DevolverId(nodx)
                frmLstACT.g_oACN4Seleccionado.ACN1 = DevolverId(nodx.Parent.Parent.Parent)
                frmLstACT.g_oACN4Seleccionado.ACN2 = DevolverId(nodx.Parent.Parent)
                frmLstACT.g_oACN4Seleccionado.ACN3 = DevolverId(nodx.Parent)
                DevolverCodDen nodx, scod, sDen
                frmLstACT.g_oACN4Seleccionado.Cod = scod
                frmLstACT.g_oACN4Seleccionado.Den = sDen
             
                Set frmLstACT.g_oACN5Seleccionado = Nothing
                
           
            Case "ACN5"
                DevolverCodDen nodx.Parent.Parent.Parent.Parent, scod, sDen
                frmLstACT.g_oACN1Seleccionado.Cod = scod
                DevolverCodDen nodx.Parent.Parent.Parent, scod, sDen
                frmLstACT.g_oACN2Seleccionado.Cod = scod
                DevolverCodDen nodx.Parent.Parent, scod, sDen
                frmLstACT.g_oACN3Seleccionado.Cod = scod
                DevolverCodDen nodx.Parent, scod, sDen
                frmLstACT.g_oACN4Seleccionado.Cod = scod
                frmLstACT.g_oACN5Seleccionado.Id = DevolverId(nodx)
                frmLstACT.g_oACN5Seleccionado.ACN1 = DevolverId(nodx.Parent.Parent.Parent.Parent)
                frmLstACT.g_oACN5Seleccionado.ACN2 = DevolverId(nodx.Parent.Parent.Parent)
                frmLstACT.g_oACN5Seleccionado.ACN3 = DevolverId(nodx.Parent.Parent)
                frmLstACT.g_oACN5Seleccionado.ACN4 = DevolverId(nodx.Parent)
                DevolverCodDen nodx, scod, sDen
                frmLstACT.g_oACN5Seleccionado.Cod = scod
                frmLstACT.g_oACN5Seleccionado.Den = sDen
        End Select
        

        frmLstACT.PonerActSeleccionada
        frmLstACT.WindowState = vbNormal
    End If
    
    frmLstACT.SetFocus
    
    Screen.MousePointer = vbNormal


End Sub

Public Sub cmdModif_Click()
Dim nodx As MSComctlLib.Node
Dim teserror As CTESError

Set nodx = tvwEstrAct.SelectedItem


If Not nodx Is Nothing Then
    
    Set teserror = New CTESError
        
    Select Case Left(nodx.Tag, 4)
        
    Case "ACN1"
            
            g_udtAccion = ACCACT1Mod
            
            Set g_oACN1Seleccionado = Nothing
            Set g_oACN1Seleccionado = g_oRaiz.Generar_CActividadNivel1
            g_oACN1Seleccionado.Id = DevolverId(nodx)
           
            Set g_oIBaseDatos = g_oACN1Seleccionado
            
            Screen.MousePointer = vbHourglass
            Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
            Screen.MousePointer = vbNormal
            
            If teserror.numerror = TESnoerror Then
 
                frmESTRACTDetalle.Caption = sIdiModifActN1
                frmESTRACTDetalle.txtCod = g_oACN1Seleccionado.Cod
                frmESTRACTDetalle.Show 1
            Else
                TratarError teserror
                Set g_oIBaseDatos = Nothing
                Set g_oACN1Seleccionado = Nothing
                Exit Sub
            End If
    
    Case "ACN2"
             
             g_udtAccion = ACCACT2Mod
            
            Set g_oACN2Seleccionado = Nothing
            Set g_oACN2Seleccionado = g_oRaiz.Generar_CActividadNivel2
            g_oACN2Seleccionado.Id = DevolverId(nodx)
            g_oACN2Seleccionado.ACN1 = DevolverId(nodx.Parent)
            
            Set g_oIBaseDatos = g_oACN2Seleccionado
            
            Screen.MousePointer = vbHourglass
            Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
            Screen.MousePointer = vbNormal
            
            If teserror.numerror = TESnoerror Then
            
                frmESTRACTDetalle.Caption = sIdiModifActN2
                frmESTRACTDetalle.txtCod = g_oACN2Seleccionado.Cod
                frmESTRACTDetalle.Show 1
            Else
                TratarError teserror
                Set g_oIBaseDatos = Nothing
                Set g_oACN2Seleccionado = Nothing
                Exit Sub
            End If
        
        Case "ACN3"
        
             g_udtAccion = ACCACT3Mod
            
            Set g_oACN3Seleccionado = Nothing
            Set g_oACN3Seleccionado = g_oRaiz.Generar_CActividadNivel3
            g_oACN3Seleccionado.Id = DevolverId(nodx)
            g_oACN3Seleccionado.ACN1 = DevolverId(nodx.Parent.Parent)
            g_oACN3Seleccionado.ACN2 = DevolverId(nodx.Parent)
            
            Set g_oIBaseDatos = g_oACN3Seleccionado
            
            Screen.MousePointer = vbHourglass
            Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
            Screen.MousePointer = vbNormal
            
            If teserror.numerror = TESnoerror Then
            
                frmESTRACTDetalle.Caption = sIdiModifActN3
                frmESTRACTDetalle.txtCod = g_oACN3Seleccionado.Cod
                frmESTRACTDetalle.Show 1
            Else
                TratarError teserror
                Set g_oIBaseDatos = Nothing
                Set g_oACN3Seleccionado = Nothing
                Exit Sub
            End If
        
        Case "ACN4"
             
            g_udtAccion = ACCACT4Mod
            
            Set g_oACN4Seleccionado = Nothing
            Set g_oACN4Seleccionado = g_oRaiz.Generar_CActividadNivel4
            g_oACN4Seleccionado.Id = DevolverId(nodx)
            g_oACN4Seleccionado.ACN1 = DevolverId(nodx.Parent.Parent.Parent)
            g_oACN4Seleccionado.ACN2 = DevolverId(nodx.Parent.Parent)
            g_oACN4Seleccionado.ACN3 = DevolverId(nodx.Parent)
           
            Set g_oIBaseDatos = g_oACN4Seleccionado
            
            Screen.MousePointer = vbHourglass
            Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
            Screen.MousePointer = vbNormal
            
            If teserror.numerror = TESnoerror Then
            
                frmESTRACTDetalle.Caption = sIdiModifActN4

                frmESTRACTDetalle.txtCod = g_oACN4Seleccionado.Cod
                frmESTRACTDetalle.Show 1
            Else
                TratarError teserror
                Set g_oIBaseDatos = Nothing
                Set g_oACN4Seleccionado = Nothing
                Exit Sub
            End If
            
        Case "ACN5"
             
            g_udtAccion = ACCACT5Mod
            
            Set g_oACN5Seleccionado = Nothing
            Set g_oACN5Seleccionado = g_oRaiz.Generar_CActividadNivel5
            g_oACN5Seleccionado.Id = DevolverId(nodx)
            g_oACN5Seleccionado.ACN1 = DevolverId(nodx.Parent.Parent.Parent.Parent)
            g_oACN5Seleccionado.ACN2 = DevolverId(nodx.Parent.Parent.Parent)
            g_oACN5Seleccionado.ACN3 = DevolverId(nodx.Parent.Parent)
            g_oACN5Seleccionado.ACN4 = DevolverId(nodx.Parent)
           
            Set g_oIBaseDatos = g_oACN5Seleccionado
            
            Screen.MousePointer = vbHourglass
            Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
            Screen.MousePointer = vbNormal
            
            If teserror.numerror = TESnoerror Then
            
                frmESTRACTDetalle.Caption = sIdiModifActN5

                frmESTRACTDetalle.txtCod = g_oACN5Seleccionado.Cod
                frmESTRACTDetalle.Show 1
            Else
                TratarError teserror
                Set g_oIBaseDatos = Nothing
                Set g_oACN5Seleccionado = Nothing
                Exit Sub
            End If
            
    End Select

    If Not nodx Is Nothing Then
        nodx.Selected = True
    End If
        
End If

Me.tvwEstrAct.SetFocus

Set nodx = Nothing

End Sub
Public Function DevolverId(ByVal Node As MSComctlLib.Node) As Variant

If Node Is Nothing Then Exit Function

Select Case Left(Node.Tag, 4)

Case "ACN1"
        
        DevolverId = Right(Node.Tag, Len(Node.Tag) - 4)
    
Case "ACN2"
    
        DevolverId = Right(Node.Tag, Len(Node.Tag) - 4)
    
Case "ACN3"
    
        DevolverId = Right(Node.Tag, Len(Node.Tag) - 4)
    
Case "ACN4"
        
        DevolverId = Right(Node.Tag, Len(Node.Tag) - 4)

Case "ACN5"
        
        DevolverId = Right(Node.Tag, Len(Node.Tag) - 4)
    
End Select

End Function
Private Sub DevolverCodDen(ByVal Node As MSComctlLib.Node, ByRef Cod As String, Den As String)
Dim sTexto() As String

If Node Is Nothing Then Exit Sub

sTexto = Split(Node.Text, " - ")
Cod = sTexto(0)
Den = sTexto(1)

    
End Sub

Private Sub cmdRestaurar_Click()

GenerarEstrActividades Not frmMDI.mnuOrdCod.Checked, sdbcIdiCod.Text

ConfigurarInterfazSeguridad tvwEstrAct.Nodes.Item("Raiz")

Me.tvwEstrAct.SetFocus

End Sub

Private Sub Form_Load()

    Me.Width = 9120
    Me.Height = 6000
    Me.Top = 400
    Me.Left = 200
    CargarRecursos
    
    cmdA�adir.Enabled = False
    cmdEli.Enabled = False
    cmdListado.Enabled = False
    cmdRestaurar.Enabled = False
    cmdModif.Enabled = False
    cmdBuscar.Enabled = False
    
    tvwEstrAct.Nodes.Clear
    
End Sub

Private Sub Form_Resize()
    
    If Me.Height < 1500 Then Exit Sub
    If Me.Width < 1205 Then Exit Sub
    
    If Height <= 1600 Then
        tvwEstrAct.Height = Height - 1600
    End If
    
    tvwEstrAct.Height = Me.Height - 1620
    tvwEstrAct.Width = Me.Width - 350
    picNavigate.Top = Me.Height - 850
    sdbcIdiDen.Top = tvwEstrAct.Top - 450
    sdbcIdiCod.Top = tvwEstrAct.Top - 450
End Sub


Private Sub sdbcIdiCod_Change()
 
    If Not m_bRespetarCombo Then
    
        m_bRespetarCombo = True
        sdbcIdiDen.Text = ""
    
        m_bCargarComboDesde = True
        
        sdbcIdiCod.Text = ""
        
    End If
    
    If sdbcIdiCod.Text = "" Then
        tvwEstrAct.Nodes.Clear
    End If
    
End Sub

Private Sub sdbcIdiCod_CloseUp()
       
    If sdbcIdiCod.Value = "..." Then
        sdbcIdiCod.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcIdiDen.Text = sdbcIdiCod.Columns(2).Text
    sdbcIdiCod.Text = sdbcIdiCod.Columns(1).Text

    m_bRespetarCombo = False
   
    
    m_bCargarComboDesde = False
    
    Me.Caption = sIdiCargandoAct
    GenerarEstrActividades Not frmMDI.mnuOrdCod.Checked, sdbcIdiCod.Text
    Me.Caption = sIdiActividades

    Me.tvwEstrAct.SetFocus
    cmdA�adir.Enabled = True
    cmdEli.Enabled = False
    cmdListado.Enabled = True
    cmdRestaurar.Enabled = True
    cmdBuscar.Enabled = True
    cmdModif.Enabled = False
    
    
End Sub

Private Sub sdbcIdiCod_DropDown()
Dim Ador As Ador.Recordset
    
   
    sdbcIdiCod.RemoveAll
    
    If m_bCargarComboDesde Then
        Set Ador = g_oRaiz.DevolverIdiomasDelPortalDesde(basParametros.g_iCargaMaximaCombos, sdbcIdiCod, , False)
    Else
        Set Ador = g_oRaiz.DevolverIdiomasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , , False)
    End If
    
    If Not Ador Is Nothing Then
        
        While Not Ador.EOF
        
            sdbcIdiCod.AddItem Ador("ID").Value & Chr(9) & Ador("COD").Value & Chr(9) & Ador("DEN").Value
            Ador.MoveNext
        
        Wend
        
        Ador.Close
        Set Ador = Nothing
        
    End If
    
        
End Sub

Private Sub sdbcIdiCod_InitColumnProps()
    
    sdbcIdiCod.DataFieldList = "Column 1"
    sdbcIdiCod.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcIdiCod_PositionList(ByVal Text As String)
Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcIdiCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcIdiCod.Rows - 1
            bm = sdbcIdiCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcIdiCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcIdiCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcIdiCod_Validate(Cancel As Boolean)
Dim scod As String
Dim Ador As Ador.Recordset
Dim nodeAct As MSComctlLib.Node

    
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
       
    If sdbcIdiCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    
     Set Ador = g_oRaiz.DevolverIdiomasDelPortalDesde(g_iCargaMaximaCombos, sdbcIdiCod.Text, , True, False)
    
    
    If Ador Is Nothing Then
        sdbcIdiCod.Text = ""
    Else
        If Ador.EOF Then
            sdbcIdiCod.Text = ""
        Else
            sdbcIdiDen.Text = Ador("DEN").Value
            If m_bRespetarCombo = True Then
                Me.Caption = sIdiCargandoAct
                GenerarEstrActividades Not frmMDI.mnuOrdCod.Checked, sdbcIdiCod.Text
                Me.Caption = sIdiActividades
            End If

            m_bRespetarCombo = False
            m_bCargarComboDesde = False
        End If
        Ador.Close
        Set Ador = Nothing
        Me.tvwEstrAct.SetFocus
        cmdA�adir.Enabled = True
        cmdEli.Enabled = False
        cmdListado.Enabled = True
        cmdRestaurar.Enabled = True
        cmdBuscar.Enabled = True
        cmdModif.Enabled = False
    End If
    
    
    
End Sub

Private Sub sdbcIdiDen_Change()
    
    If Not m_bRespetarCombo Then
    
        m_bRespetarCombo = True
        sdbcIdiCod.Text = ""
    
        m_bCargarComboDesde = True
        
        sdbcIdiDen = ""
        
    End If
End Sub

Private Sub sdbcIdiDen_CloseUp()
    
    If sdbcIdiDen.Value = "..." Then
        sdbcIdiDen.Text = ""
        Exit Sub
    End If
            
    m_bRespetarCombo = True
    sdbcIdiDen.Text = sdbcIdiDen.Columns(1).Text
    sdbcIdiCod.Text = sdbcIdiDen.Columns(2).Text
    sdbcIdiCod.Columns("ID").Value = sdbcIdiDen.Columns("ID").Value
    m_bRespetarCombo = False
    
    m_bCargarComboDesde = False
    
    Me.Caption = sIdiCargandoAct
    GenerarEstrActividades Not frmMDI.mnuOrdCod.Checked, sdbcIdiCod.Text
    Me.Caption = sIdiActividades
    
    cmdA�adir.Enabled = True
    cmdEli.Enabled = True
    cmdListado.Enabled = True
    cmdRestaurar.Enabled = True
    cmdBuscar.Enabled = True
    cmdModif.Enabled = False

End Sub




Private Sub sdbcIdiDen_DropDown()
Dim Ador As Ador.Recordset
    
    sdbcIdiDen.RemoveAll
    
    If m_bCargarComboDesde Then
        Set Ador = g_oRaiz.DevolverIdiomasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbcIdiDen, , True)
    Else
        Set Ador = g_oRaiz.DevolverIdiomasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , , , True)
    End If
    
    If Not Ador Is Nothing Then
        
        While Not Ador.EOF
        
            sdbcIdiDen.AddItem Ador("ID").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("COD").Value
            Ador.MoveNext
        
        Wend
        Ador.Close
        Set Ador = Nothing
        
    End If
    
End Sub

Private Sub sdbcIdiDen_InitColumnProps()
    
    sdbcIdiDen.DataFieldList = "Column 1"
    sdbcIdiDen.DataFieldToDisplay = "Column 1"
    
End Sub
Public Sub GenerarEstrActividades(ByVal bOrdenadoPorDen As Boolean, ByVal sIdioma As String)
    Dim Ador As Ador.Recordset
    
    Dim oActividadesNivel1 As CActividadesNivel1
    Dim oActividadesNivel2 As FSPSServer.CActividadesNivel2
    Dim oActividadesNivel3 As FSPSServer.CActividadesNivel3
    Dim oActividadesNivel4 As FSPSServer.CActividadesNivel4
    
    Dim oACN1 As CActividadNivel1
    Dim oACN2 As CActividadNivel2
    Dim oACN3 As CActividadNivel3
    Dim oACN4 As CActividadNivel4
    Dim oACN5 As CActividadNivel5
    
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    
    Dim nodeAct As MSComctlLib.Node
    
    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
        
    oActividadesNivel1.GenerarEstructuraActividades bOrdenadoPorDen, sIdioma

    tvwEstrAct.Nodes.Clear

    Set nodeAct = tvwEstrAct.Nodes.Add(, , "Raiz", sIdiActividades, "Raiz2")
    nodeAct.Tag = "Raiz"
    nodeAct.Expanded = True
    
    For Each oACN1 In oActividadesNivel1
     
        scod1 = oACN1.Id & Mid$("                         ", 1, 10 - Len(CStr(oACN1.Id)))
        
        Set nodeAct = tvwEstrAct.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, oACN1.Cod & " - " & oACN1.Den, "ACT")
        
        nodeAct.Tag = "ACN1" & oACN1.Id
        
        For Each oACN2 In oACN1.ActividadesNivel2
            
            scod2 = oACN2.Id & Mid$("                         ", 1, 10 - Len(CStr(oACN2.Id)))
            
            Set nodeAct = tvwEstrAct.Nodes.Add("ACN1" & scod1, tvwChild, "ACN2" & scod1 & scod2, oACN2.Cod & " - " & oACN2.Den, "ACT")
            
            nodeAct.Tag = "ACN2" & oACN2.Id
            
            For Each oACN3 In oACN2.ActividadesNivel3
            
                scod3 = oACN3.Id & Mid$("                         ", 1, 10 - Len(CStr(oACN3.Id)))
                
                Set nodeAct = tvwEstrAct.Nodes.Add("ACN2" & scod1 & scod2, tvwChild, "ACN3" & scod1 & scod2 & scod3, oACN3.Cod & " - " & oACN3.Den, "ACT")
                
                nodeAct.Tag = "ACN3" & oACN3.Id
                
                For Each oACN4 In oACN3.ActividadesNivel4
                    
                    scod4 = oACN4.Id & Mid$("                         ", 1, 10 - Len(CStr(oACN4.Id)))
                    
                    Set nodeAct = tvwEstrAct.Nodes.Add("ACN3" & scod1 & scod2 & scod3, tvwChild, "ACN4" & scod1 & scod2 & scod3 & scod4, oACN4.Cod & " - " & oACN4.Den, "ACT")
                    
                    nodeAct.Tag = "ACN4" & oACN4.Id
                    
                    For Each oACN5 In oACN4.ActividadesNivel5
                        
                        scod5 = oACN5.Id & Mid$("                         ", 1, 10 - Len(CStr(oACN5.Id)))
                        
                        Set nodeAct = tvwEstrAct.Nodes.Add("ACN4" & scod1 & scod2 & scod3 & scod4, tvwChild, "ACN5" & scod1 & scod2 & scod3 & scod4 & scod5, oACN5.Cod & " - " & oACN5.Den, "ACT")
                        
                        nodeAct.Tag = "ACN5" & oACN5.Id
                        
                    Next
                Next
            Next
    
        Next
    Next
        
Exit Sub

error:
    Set nodeAct = Nothing
    Resume Next


End Sub

Private Sub tvwEstrAct_Collapse(ByVal Node As MSComctlLib.Node)
    ConfigurarInterfazSeguridad Node
End Sub


Private Sub tvwEstrAct_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nodeAct As MSComctlLib.Node

If Button = 2 Then

    Set nodeAct = tvwEstrAct.SelectedItem
    
    If Not nodeAct Is Nothing Then
    
        Select Case Left(nodeAct.Tag, 4)
        
        Case "Raiz"
                    PopupMenu frmMDI.mnuEstrActRaiz
        
        Case "ACN1"
                    PopupMenu frmMDI.mnuEstrAct

        Case "ACN2"
                    PopupMenu frmMDI.mnuEstrAct
                    
                    
        Case "ACN3"
                    PopupMenu frmMDI.mnuEstrAct
                    

        Case "ACN4"
                    PopupMenu frmMDI.mnuEstrAct
                    
                    
        Case "ACN5"
                    PopupMenu frmMDI.mnuEstrAct

                     
        End Select
    
    End If

End If

End Sub

Public Sub ConfigurarInterfazSeguridad(ByVal nodeAct As MSComctlLib.Node)

'*************************************************************************

'***Descripcion: Habilitar o deshabilitar los botones y las opciones de los
'                popup menus

'*************************************************************************

Select Case Left(nodeAct.Tag, 4)
    
        
        Case "Raiz"
                    cmdModif.Enabled = False
                    cmdEli.Enabled = False
                    cmdA�adir.Enabled = True
        
        Case "ACN1"
                    frmMDI.mnuAnyadir.Enabled = True
                    cmdModif.Enabled = True
                    cmdA�adir.Enabled = True
                    If nodeAct.Children > 0 Then
                        frmMDI.mnuEliminar.Enabled = False
                        cmdEli.Enabled = False
                    Else
                        frmMDI.mnuEliminar.Enabled = True
                        cmdEli.Enabled = True
                    End If

        Case "ACN2"
                    frmMDI.mnuAnyadir.Enabled = True
                    cmdModif.Enabled = True
                    cmdA�adir.Enabled = True
                    If nodeAct.Children > 0 Then
                        frmMDI.mnuEliminar.Enabled = False
                        cmdEli.Enabled = False
                    Else
                        frmMDI.mnuEliminar.Enabled = True
                        cmdEli.Enabled = True
                    End If
                    
        Case "ACN3"
                    frmMDI.mnuAnyadir.Enabled = True
                    cmdModif.Enabled = True
                    cmdA�adir.Enabled = True
                    If nodeAct.Children > 0 Then
                        frmMDI.mnuEliminar.Enabled = False
                        cmdEli.Enabled = False
                    Else
                        frmMDI.mnuEliminar.Enabled = True
                        cmdEli.Enabled = True
                    End If

        Case "ACN4"
                    frmMDI.mnuAnyadir.Enabled = True
                    cmdModif.Enabled = True
                    cmdA�adir.Enabled = True
                    If nodeAct.Children > 0 Then
                        frmMDI.mnuEliminar.Enabled = False
                        cmdEli.Enabled = False
                    Else
                        frmMDI.mnuEliminar.Enabled = True
                        cmdEli.Enabled = True
                    End If
                    
        Case "ACN5"
                    frmMDI.mnuAnyadir.Enabled = False
                    cmdModif.Enabled = True
                    cmdA�adir.Enabled = False
                    frmMDI.mnuEliminar.Enabled = True
                    cmdEli.Enabled = True
                    
        
        End Select
    
    
    
    

End Sub

Public Sub tvwEstrAct_NodeClick(ByVal Node As MSComctlLib.Node)
ConfigurarInterfazSeguridad Node
End Sub

Public Sub CambiarCodigo()

    Dim teserror As CTESError
    
    Dim nodx As Node
    
    Set nodx = tvwEstrAct.SelectedItem
    
    Screen.MousePointer = vbHourglass
    
    Select Case Left(nodx.Tag, 4)
    
    Case "ACN1"
    
        Set g_oIBaseDatos = Nothing
        Set g_oACN1Seleccionado = Nothing
        Set g_oACN1Seleccionado = g_oRaiz.Generar_CActividadNivel1

        g_oACN1Seleccionado.Id = DevolverId(nodx)
    
        Set g_oIBaseDatos = g_oACN1Seleccionado
        
        Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
        
        If teserror.numerror <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            tvwEstrAct.SetFocus
            Exit Sub
        End If
        
        'g_oIBaseDatos.CancelarEdicion
                
        frmMODCOD.Caption = sIdiActN1
        frmMODCOD.txtCodNue.MaxLength = 2
        frmMODCOD.txtCodAct.Text = g_oACN1Seleccionado.Cod
        frmMODCOD.g_sfrmOrigen = "frmActiv"
        g_bCodigoCancelar = False
        Screen.MousePointer = vbNormal
        frmMODCOD.Show 1
            
        DoEvents
        
        If g_bCodigoCancelar = True Then Exit Sub
         
        If g_sCodigoNuevo = "" Then
            basMensajes.NoValido sIdiCodigo
            Exit Sub
        End If
        
        If UCase(g_sCodigoNuevo) = UCase(g_oACN1Seleccionado.Cod) Then
            basMensajes.NoValido sIdiCodigo
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        Set teserror = g_oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
        
        If teserror.numerror <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If

        ''' Actualizar datos
    
        cmdRestaurar_Click
                    
    Case "ACN2"
    
        Set g_oIBaseDatos = Nothing
        Set g_oACN2Seleccionado = Nothing
        Set g_oACN2Seleccionado = g_oRaiz.Generar_CActividadNivel2

        g_oACN2Seleccionado.Id = DevolverId(nodx)
        g_oACN2Seleccionado.ACN1 = DevolverId(nodx.Parent)
    
        Set g_oIBaseDatos = g_oACN2Seleccionado
        
        Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
        
        If teserror.numerror <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            tvwEstrAct.SetFocus
            Exit Sub
        End If
        
        'g_oIBaseDatos.CancelarEdicion
                
        frmMODCOD.Caption = sIdiActN2
        frmMODCOD.txtCodNue.MaxLength = 2
        frmMODCOD.txtCodAct.Text = g_oACN2Seleccionado.Cod
        frmMODCOD.g_sfrmOrigen = "frmActiv"
        g_bCodigoCancelar = False
        Screen.MousePointer = vbNormal
        frmMODCOD.Show 1
            
        DoEvents
        
        If g_bCodigoCancelar = True Then Exit Sub
         
        If g_sCodigoNuevo = "" Then
            basMensajes.NoValido sIdiCodigo
            Exit Sub
        End If
        
        If UCase(g_sCodigoNuevo) = UCase(g_oACN2Seleccionado.Cod) Then
            basMensajes.NoValido sIdiCodigo
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        Set teserror = g_oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
        
        If teserror.numerror <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If

        ''' Actualizar datos
        cmdRestaurar_Click
    
    Case "ACN3"
            
        Set g_oIBaseDatos = Nothing
        Set g_oACN3Seleccionado = Nothing
        Set g_oACN3Seleccionado = g_oRaiz.Generar_CActividadNivel3

        g_oACN3Seleccionado.Id = DevolverId(nodx)
        g_oACN3Seleccionado.ACN1 = DevolverId(nodx.Parent.Parent)
        g_oACN3Seleccionado.ACN2 = DevolverId(nodx.Parent)
    
        Set g_oIBaseDatos = g_oACN3Seleccionado
        
        Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
        If teserror.numerror <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            tvwEstrAct.SetFocus
            Exit Sub
        End If
        
        'g_oIBaseDatos.CancelarEdicion
                
        frmMODCOD.Caption = sIdiActN3
        frmMODCOD.txtCodNue.MaxLength = 2
        frmMODCOD.txtCodAct.Text = g_oACN3Seleccionado.Cod
        frmMODCOD.g_sfrmOrigen = "frmActiv"
        g_bCodigoCancelar = False
        Screen.MousePointer = vbNormal
        frmMODCOD.Show 1
            
        DoEvents
        
        If g_bCodigoCancelar = True Then Exit Sub
         
        If g_sCodigoNuevo = "" Then
            basMensajes.NoValido sIdiCodigo
            Exit Sub
        End If
        
        If UCase(g_sCodigoNuevo) = UCase(g_oACN3Seleccionado.Cod) Then
            basMensajes.NoValido sIdiCodigo
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        Set teserror = g_oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
        
        If teserror.numerror <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If

        ''' Actualizar datos
    
        cmdRestaurar_Click '
    
    Case "ACN4"

        Set g_oIBaseDatos = Nothing
        Set g_oACN4Seleccionado = Nothing
        Set g_oACN4Seleccionado = g_oRaiz.Generar_CActividadNivel4

        g_oACN4Seleccionado.Id = DevolverId(nodx)
        g_oACN4Seleccionado.ACN1 = DevolverId(nodx.Parent.Parent.Parent)
        g_oACN4Seleccionado.ACN2 = DevolverId(nodx.Parent.Parent)
        g_oACN4Seleccionado.ACN3 = DevolverId(nodx.Parent)
    
        Set g_oIBaseDatos = g_oACN4Seleccionado
        
        Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
        
        If teserror.numerror <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            tvwEstrAct.SetFocus
            Exit Sub
        End If
        
        'g_oIBaseDatos.CancelarEdicion
                
        frmMODCOD.Caption = sIdiActN4
        frmMODCOD.txtCodNue.MaxLength = 2
        frmMODCOD.txtCodAct.Text = g_oACN4Seleccionado.Cod
        frmMODCOD.g_sfrmOrigen = "frmActiv"
        g_bCodigoCancelar = False
        Screen.MousePointer = vbNormal
        frmMODCOD.Show 1
            
        DoEvents
        
        If g_bCodigoCancelar = True Then Exit Sub
         
        If g_sCodigoNuevo = "" Then
            basMensajes.NoValido sIdiCodigo
            Exit Sub
        End If
        
        If UCase(g_sCodigoNuevo) = UCase(g_oACN4Seleccionado.Cod) Then
            basMensajes.NoValido sIdiCodigo
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        Set teserror = g_oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
        
        If teserror.numerror <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If

        ''' Actualizar datos
    
        cmdRestaurar_Click
    
    Case "ACN5"
        Set g_oIBaseDatos = Nothing
        Set g_oACN5Seleccionado = Nothing
        Set g_oACN5Seleccionado = g_oRaiz.Generar_CActividadNivel5

        g_oACN5Seleccionado.Id = DevolverId(nodx)
        g_oACN5Seleccionado.ACN1 = DevolverId(nodx.Parent.Parent.Parent.Parent)
        g_oACN5Seleccionado.ACN2 = DevolverId(nodx.Parent.Parent.Parent)
        g_oACN5Seleccionado.ACN3 = DevolverId(nodx.Parent.Parent)
        g_oACN5Seleccionado.ACN4 = DevolverId(nodx.Parent)
    
        Set g_oIBaseDatos = g_oACN5Seleccionado
        
        Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
        
        If teserror.numerror <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            tvwEstrAct.SetFocus
            Exit Sub
        End If
        
        'g_oIBaseDatos.CancelarEdicion
                
        frmMODCOD.Caption = sIdiActN5
        frmMODCOD.txtCodNue.MaxLength = 2
        frmMODCOD.txtCodAct.Text = g_oACN5Seleccionado.Cod
        frmMODCOD.g_sfrmOrigen = "frmActiv"
        g_bCodigoCancelar = False
        Screen.MousePointer = vbNormal
        frmMODCOD.Show 1
            
        DoEvents
        
        If g_bCodigoCancelar = True Then Exit Sub
         
        If g_sCodigoNuevo = "" Then
            basMensajes.NoValido sIdiCodigo
            Exit Sub
        End If
        
        If UCase(g_sCodigoNuevo) = UCase(g_oACN5Seleccionado.Cod) Then
            basMensajes.NoValido sIdiCodigo
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        Set teserror = g_oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
        
        If teserror.numerror <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If

        ''' Actualizar datos
    
        cmdRestaurar_Click
    End Select
    
    Screen.MousePointer = vbNormal

End Sub

Public Sub mnuPopUpDetalle()
Dim nodx As MSComctlLib.Node
Dim teserror As CTESError

Set nodx = tvwEstrAct.SelectedItem


If Not nodx Is Nothing Then
        
    Screen.MousePointer = vbHourglass
    
    Select Case Left(nodx.Tag, 4)
        
    Case "ACN1"
            
            g_udtAccion = ACCACT1Det
            
            Set g_oACN1Seleccionado = Nothing
            Set g_oACN1Seleccionado = g_oRaiz.Generar_CActividadNivel1
                        
            g_oACN1Seleccionado.Id = DevolverId(nodx)
         
            Set g_oIBaseDatos = g_oACN1Seleccionado
            
            Screen.MousePointer = vbHourglass
            Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
            Screen.MousePointer = vbNormal
                       
            If teserror.numerror = TESnoerror Then
 
                frmESTRACTDetalle.Caption = sIdiDetActN1
                frmESTRACTDetalle.txtCod = g_oACN1Seleccionado.Cod
                'frmESTRACTDetalle.txtDen = g_oACN1Seleccionado.Den
                frmESTRACTDetalle.Show 1
            Else
                TratarError teserror
                Set g_oIBaseDatos = Nothing
                Set g_oACN1Seleccionado = Nothing
                Exit Sub
            End If
            
    Case "ACN2"
            
            g_udtAccion = ACCACT2Det
            
            Set g_oACN2Seleccionado = Nothing
            Set g_oACN2Seleccionado = g_oRaiz.Generar_CActividadNivel2
                        
            g_oACN2Seleccionado.Id = DevolverId(nodx)
            g_oACN2Seleccionado.ACN1 = DevolverId(nodx.Parent)
         
            Set g_oIBaseDatos = g_oACN2Seleccionado
            
            Screen.MousePointer = vbHourglass
            Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
            Screen.MousePointer = vbNormal
                       
            If teserror.numerror = TESnoerror Then
 
                frmESTRACTDetalle.Caption = sIdiDetActN2
                frmESTRACTDetalle.txtCod = g_oACN2Seleccionado.Cod
                'frmESTRACTDetalle.txtDen = g_oACN2Seleccionado.Den
                frmESTRACTDetalle.Show 1
            Else
                TratarError teserror
                Set g_oIBaseDatos = Nothing
                Set g_oACN3Seleccionado = Nothing
                Exit Sub
            End If
            
    Case "ACN3"
            
            g_udtAccion = ACCACT3Det
            
            Set g_oACN3Seleccionado = Nothing
            Set g_oACN3Seleccionado = g_oRaiz.Generar_CActividadNivel3
                        
            g_oACN3Seleccionado.Id = DevolverId(nodx)
            g_oACN3Seleccionado.ACN1 = DevolverId(nodx.Parent.Parent)
            g_oACN3Seleccionado.ACN2 = DevolverId(nodx.Parent)
         
            Set g_oIBaseDatos = g_oACN3Seleccionado
            
            Screen.MousePointer = vbHourglass
            Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
            Screen.MousePointer = vbNormal
                       
            If teserror.numerror = TESnoerror Then
 
                frmESTRACTDetalle.Caption = sIdiDetActN3
                frmESTRACTDetalle.txtCod = g_oACN3Seleccionado.Cod
                'frmESTRACTDetalle.txtDen = g_oACN3Seleccionado.Den
                frmESTRACTDetalle.Show 1
            Else
                TratarError teserror
                Set g_oIBaseDatos = Nothing
                Set g_oACN3Seleccionado = Nothing
                Exit Sub
            End If
            
    Case "ACN4"
            
            g_udtAccion = ACCACT4Det
            
            Set g_oACN4Seleccionado = Nothing
            Set g_oACN4Seleccionado = g_oRaiz.Generar_CActividadNivel4
                        
            g_oACN4Seleccionado.Id = DevolverId(nodx)
            g_oACN4Seleccionado.Id = DevolverId(nodx)
            g_oACN4Seleccionado.ACN1 = DevolverId(nodx.Parent.Parent.Parent)
            g_oACN4Seleccionado.ACN2 = DevolverId(nodx.Parent.Parent)
            g_oACN4Seleccionado.ACN3 = DevolverId(nodx.Parent)
         
            Set g_oIBaseDatos = g_oACN4Seleccionado
            
            Screen.MousePointer = vbHourglass
            Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
            Screen.MousePointer = vbNormal
                       
            If teserror.numerror = TESnoerror Then
 
                frmESTRACTDetalle.Caption = sIdiDetActN4
                frmESTRACTDetalle.txtCod = g_oACN4Seleccionado.Cod
                'frmESTRACTDetalle.txtDen = g_oACN4Seleccionado.Den
                frmESTRACTDetalle.Show 1
            Else
                TratarError teserror
                Set g_oIBaseDatos = Nothing
                Set g_oACN4Seleccionado = Nothing
                Exit Sub
            End If
            
    Case "ACN5"
            
            g_udtAccion = ACCACT5Det
            
            Set g_oACN5Seleccionado = Nothing
            Set g_oACN5Seleccionado = g_oRaiz.Generar_CActividadNivel5
                        
            g_oACN5Seleccionado.Id = DevolverId(nodx)
            g_oACN5Seleccionado.ACN1 = DevolverId(nodx.Parent.Parent.Parent.Parent)
            g_oACN5Seleccionado.ACN2 = DevolverId(nodx.Parent.Parent.Parent)
            g_oACN5Seleccionado.ACN3 = DevolverId(nodx.Parent.Parent)
            g_oACN5Seleccionado.ACN4 = DevolverId(nodx.Parent)
         
            Set g_oIBaseDatos = g_oACN5Seleccionado
            
            Screen.MousePointer = vbHourglass
            Set teserror = g_oIBaseDatos.IniciarEdicion(, , sdbcIdiCod.Text)
            Screen.MousePointer = vbNormal
                       
            If teserror.numerror = TESnoerror Then
 
                frmESTRACTDetalle.Caption = sIdiDetActN5
                frmESTRACTDetalle.txtCod = g_oACN5Seleccionado.Cod
                'frmESTRACTDetalle.txtDen = g_oACN5Seleccionado.Den
                frmESTRACTDetalle.Show 1
            Else
                TratarError teserror
                Set g_oIBaseDatos = Nothing
                Set g_oACN5Seleccionado = Nothing
                Exit Sub
            End If
            
    
    End Select
    
    Screen.MousePointer = vbNormal

End If

End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    

    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_ACTIV, g_udtParametrosGenerales.g_sIdioma)
    
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        sIdiActividades = Ador(0).Value
        Ador.MoveNext
        Me.lblIdioma.Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcIdiCod.Columns("COD").Caption = Ador(0).Value
        Me.sdbcIdiDen.Columns("COD").Caption = Ador(0).Value
        sIdiCodigo = Ador(0).Value
        Ador.MoveNext
        Me.sdbcIdiCod.Columns("DEN").Caption = Ador(0).Value
        Me.sdbcIdiDen.Columns("DEN").Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdA�adir.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdBuscar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdEli.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdListado.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdModif.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdRestaurar.Caption = Ador(0).Value
        Ador.MoveNext
        sIdiAnyaActN1 = Ador(0).Value
        Ador.MoveNext
        sIdiAnyaActN2 = Ador(0).Value
        Ador.MoveNext
        sIdiAnyaActN3 = Ador(0).Value
        Ador.MoveNext
        sIdiAnyaActN4 = Ador(0).Value
        Ador.MoveNext
        sIdiAnyaActN5 = Ador(0).Value
        Ador.MoveNext
        sIdiModifActN1 = Ador(0).Value
        Ador.MoveNext
        sIdiModifActN2 = Ador(0).Value
        Ador.MoveNext
        sIdiModifActN3 = Ador(0).Value
        Ador.MoveNext
        sIdiModifActN4 = Ador(0).Value
        Ador.MoveNext
        sIdiModifActN5 = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        sIdiCargandoAct = Ador(0).Value
        Ador.MoveNext
        sIdiActN1 = Ador(0).Value
        Ador.MoveNext
        sIdiActN2 = Ador(0).Value
        Ador.MoveNext
        sIdiActN3 = Ador(0).Value
        Ador.MoveNext
        sIdiActN4 = Ador(0).Value
        Ador.MoveNext
        sIdiActN5 = Ador(0).Value
        Ador.MoveNext
        sIdiDetActN1 = Ador(0).Value
        Ador.MoveNext
        sIdiDetActN2 = Ador(0).Value
        Ador.MoveNext
        sIdiDetActN3 = Ador(0).Value
        Ador.MoveNext
        sIdiDetActN4 = Ador(0).Value
        Ador.MoveNext
        sIdiDetActN5 = Ador(0).Value
        Ador.MoveNext
        sIdiActividad = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
    
    
        
End Sub


