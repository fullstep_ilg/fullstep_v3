Attribute VB_Name = "basMensajes"
Public Function CargarTextoMensaje(ByVal MensajeId As Long)
Dim Ador As Ador.Recordset
Dim sTexto As String

    On Error Resume Next
      
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(MENSAJES, g_udtParametrosGenerales.g_sIdioma, MensajeId)
    
    If Not Ador Is Nothing Then
        
        sTexto = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
    CargarTextoMensaje = sTexto
    
End Function


Public Sub ImposibleAutentificar()

    MsgBox CargarTextoMensaje(1), vbCritical + vbOKOnly, "FULLSTEP PS"
    
End Sub
Public Sub ImposibleConectarAlServidorDeColaboracion()
Dim sMensaje As String

    sMensaje = CargarTextoMensaje(2)
    
    MsgBox sMensaje, vbCritical + vbOKOnly, "FULLSTEP PS"
    
End Sub

Public Sub HoraIncorrecta()
    MsgBox CargarTextoMensaje(3), vbCritical, "FULLSTEP PS"
End Sub

Public Sub NumeroIncorrecto()
    MsgBox CargarTextoMensaje(6), vbCritical, "FULLSTEP PS"
End Sub

Public Sub IntervaloIncorrecto()
    MsgBox CargarTextoMensaje(9) & vbLf & CargarTextoMensaje(10), vbCritical, "FULLSTEP PS"
End Sub

Public Sub ImposibleActualizarSPTS()
    MsgBox CargarTextoMensaje(14) & vbCrLf & CargarTextoMensaje(15), vbExclamation, "FULLSTEP PS"
End Sub
Public Sub CiaDesautorizadaEnPortal()
    
    MsgBox CargarTextoMensaje(16), vbCritical, "FULLSTEP PS"
    
End Sub


Public Sub ImposibleAutorizarProvePortal()
    MsgBox CargarTextoMensaje(17), vbExclamation, "FULLSTEP PS"
End Sub


Public Sub ImposibleEnlazarProvePortal()
    MsgBox CargarTextoMensaje(18) & vbLf & CargarTextoMensaje(19), vbExclamation, "FULLSTEP PS"
End Sub

Public Function ImposibleActualizarParam(ByVal Causa As String)
MsgBox CargarTextoMensaje(20) & vbCrLf & Causa, vbExclamation, "FULLSTEP PS"

End Function

Public Sub ConfiguracionParametrosActualizada()
    
    MsgBox CargarTextoMensaje(21), vbInformation, "FULLSTEP PS"
    
End Sub
Public Sub ImposibleGuardarParametros()
    
    MsgBox CargarTextoMensaje(22), vbCritical, "FULLSTEP PS"

End Sub


Public Sub RutaDeRPTNoValida()
    MsgBox CargarTextoMensaje(23) & vbLf & CargarTextoMensaje(24), vbCritical, "FULLSTEP PS"
End Sub
Public Sub OtroError(ByVal Codigo As String, ByVal Descr As String)
    
    MsgBox CargarTextoMensaje(25) & vbLf & CargarTextoMensaje(26) & Codigo & vbLf & CargarTextoMensaje(27) & Descr & vbLf _
    & CargarTextoMensaje(28), vbExclamation, "FULLSTEP PS"
                  
End Sub

Public Sub UsuarioNoAutorizado()
    
    Beep
    MsgBox CargarTextoMensaje(29), vbCritical, "FULLSTEP PS"

End Sub


Public Sub NoValido(ByVal Campo As String)
    
    MsgBox CargarTextoMensaje(30) & Campo, vbExclamation + vbOKOnly, "FULLSTEP PS"

End Sub
Public Sub NoValida(ByVal Campo As String)
    
    MsgBox CargarTextoMensaje(30) & Campo, vbExclamation + vbOKOnly, "FULLSTEP PS"

End Sub
Public Sub FaltanDatos(ByVal Campo As String)
    
    MsgBox CargarTextoMensaje(31) & Campo, vbExclamation + vbOKOnly, "FULLSTEP PS"

End Sub


Public Sub DatoDuplicado(ByVal Dato As String)
    
     MsgBox CargarTextoMensaje(32) & Dato, vbExclamation + vbOKOnly, "FULLSTEP PS"


End Sub

Public Sub DatoEliminado(ByVal Dato As String)
    
    MsgBox CargarTextoMensaje(33) & Dato, vbExclamation + vbOKOnly, "FULLSTEP PS"

End Sub
Public Sub DatosActualesModificados()
    MsgBox CargarTextoMensaje(34), vbInformation + vbOKOnly, "FULLSTEP PS"
End Sub
Public Sub DatosModificados()
    
    MsgBox CargarTextoMensaje(35) & Chr(13) & CargarTextoMensaje(36) & Chr(13) & CargarTextoMensaje(37), vbExclamation + vbOKOnly, "FULLSTEP PS"

End Sub
Public Sub DatosActualizadosCorrectamente()
 
 MsgBox CargarTextoMensaje(38), vbInformation, "FULLSTEP"
End Sub
Public Sub ActualizacionCorrecta()
    
    MsgBox CargarTextoMensaje(39), vbExclamation + vbOKOnly, "FULLSTEP PS"

End Sub

Public Sub ImposibleEliDatRel(ByVal sDato As String)
    
    MsgBox CargarTextoMensaje(40) & sDato, vbCritical + vbOKOnly, "FULLSTEP PS"

End Sub

Public Sub ImposibleConectarBD()
    
    MsgBox CargarTextoMensaje(41), vbCritical + vbOKOnly, "FULLSTEP PS"
    
End Sub

Public Sub ImposibleEstablecerUsuPpal()

    MsgBox CargarTextoMensaje(42), vbInformation + vbOKOnly, "FULLSTEP PS"
End Sub

Public Function PreguntaEliminarCia() As Integer
Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(43) & vbLf & CargarTextoMensaje(44), vbYesNo + vbQuestion, "FULLSTEP PS")
    PreguntaEliminarCia = iRespuesta
    
End Function

Public Function PreguntaEliminarCiaComp() As Integer
Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(45) & vbLf & CargarTextoMensaje(46), vbYesNo + vbCritical + vbDefaultButton2, "FULLSTEP PS")
    PreguntaEliminarCiaComp = iRespuesta
    
End Function
Public Function PreguntaEliminarPremium() As Integer
Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(47) & vbLf & CargarTextoMensaje(46), vbYesNo + vbQuestion, "FULLSTEP PS")
    PreguntaEliminarPremium = iRespuesta
    
End Function

Public Function PreguntaEliminarEnlaceComp(ByVal Cia As Variant, ByVal Prove As String) As Integer
Dim iRespuesta As Integer

If IsNull(Cia) Then
    iRespuesta = MsgBox(CargarTextoMensaje(48) & Prove & "." & vbLf & CargarTextoMensaje(46), vbYesNo + vbQuestion, "FULLSTEP PS")
Else
    iRespuesta = MsgBox(CargarTextoMensaje(48) & Prove & "." & vbLf & CargarTextoMensaje(49) & Cia & vbLf & CargarTextoMensaje(46), vbYesNo + vbQuestion, "FULLSTEP PS")
End If
    PreguntaEliminarEnlaceComp = iRespuesta
    
End Function

Public Function PreguntaAnyadirEnlaceComp(ByVal Cia As String, ByVal Prove As String) As Integer
Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(50) & Prove & vbLf & CargarTextoMensaje(51) & Cia & vbLf & CargarTextoMensaje(46), vbYesNo + vbQuestion, "FULLSTEP PS")
    PreguntaAnyadirEnlaceComp = iRespuesta
    
End Function
Public Function PreguntaModificarEnlaceComp(ByVal Cia As String) As Integer
Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(52) & Cia & vbLf & CargarTextoMensaje(46), vbYesNo + vbQuestion, "FULLSTEP PS")
    PreguntaModificarEnlaceComp = iRespuesta
    
End Function

Public Function PreguntaEliminarEnlaceProve(ByVal CiaProve As String, ByVal Prove As String) As Integer
Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(53) & vbCrLf & CargarTextoMensaje(54) & CiaProve & vbCrLf & CargarTextoMensaje(55) & Prove & vbLf & CargarTextoMensaje(46), vbYesNo + vbQuestion, "FULLSTEP PS")
    PreguntaEliminarEnlaceProve = iRespuesta
    
End Function
Public Function PreguntaAnyadirEnlaceProve(ByVal Prove As String) As Integer
Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(56) & Prove & vbLf & CargarTextoMensaje(46), vbYesNo + vbQuestion, "FULLSTEP PS")
    PreguntaAnyadirEnlaceProve = iRespuesta
    
End Function
Public Function PreguntaModificarEnlaceProve(ByVal CiaProve As String) As Integer
Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(57) & CiaProve & vbLf & CargarTextoMensaje(46), vbYesNo + vbQuestion, "FULLSTEP PS")
    PreguntaModificarEnlaceProve = iRespuesta
    
End Function

Public Function PreguntaEliminar(ByVal Dato As String) As Integer
Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(58) & vbLf & Dato & vbLf & CargarTextoMensaje(46), vbYesNo + vbQuestion, "FULLSTEP PS")
    PreguntaEliminar = iRespuesta
    
End Function
Public Function CompAsig(ByVal Dato As Ador.Recordset) As Integer
Dim iRespuesta As Integer
Dim sMensaje As String
    
    i = 0
    
    While Not Dato.EOF
        sMensaje = sMensaje & Dato(0).Value & " - " & Dato(1) & vbLf
        Dato.MoveNext
    Wend
    
    iRespuesta = MsgBox(CargarTextoMensaje(59) & vbLf & sMensaje & vbLf & CargarTextoMensaje(46), vbYesNo + vbQuestion, "FULLSTEP PS")
    CompAsig = iRespuesta
    
    
End Function
Public Function PreguntaEliminarComunicacion(ByVal Dato As String, ByVal Dato1 As String) As Integer
Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(60) & Dato & vbCrLf & Dato1 & vbLf & CargarTextoMensaje(61) & CargarTextoMensaje(46), vbYesNo + vbQuestion, "FULLSTEP PS")
    PreguntaEliminarComunicacion = iRespuesta
    
End Function
Public Function PreguntaDesautorizadPrincipal() As Integer
Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(62) & vbLf & CargarTextoMensaje(46), vbYesNo + vbQuestion, "FULLSTEP PS")
    PreguntaDesautorizadPrincipal = iRespuesta
    
End Function
Public Function PreguntaDesautorizarPremium() As Integer
Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(63) & vbLf & CargarTextoMensaje(64), vbYesNo + vbQuestion, "FULLSTEP PS")
    PreguntaDesautorizarPremium = iRespuesta
    
End Function

Public Function PreguntaDesregistrarEnCompradora() As Integer
Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(65) & vbLf & CargarTextoMensaje(66), vbYesNo + vbQuestion, "FULLSTEP PS")
    PreguntaDesregistrarEnCompradora = iRespuesta
    
End Function

Public Sub AvisoLinkedServer()
    MsgBox CargarTextoMensaje(67) & vbLf & CargarTextoMensaje(68), vbInformation, "FULLSTEP PS"
End Sub

Public Sub CiaEliminada()
    MsgBox CargarTextoMensaje(69), vbOKOnly, "FULLSTEP PS"
End Sub

Public Sub SeleccioneFichero()
    MsgBox CargarTextoMensaje(70), vbExclamation + vbOKOnly, "FULLSTEP PS"
End Sub
Public Sub ErrorDeEscrituraEnDisco()
    MsgBox CargarTextoMensaje(71), vbCritical + vbOKOnly, "FULLSTEP PS"
End Sub
Public Sub UsuarioEliminado()
    MsgBox CargarTextoMensaje(72), vbOKOnly, "FULLSTEP PS"
End Sub

Public Sub ConexionCorrecta()
    
    MsgBox CargarTextoMensaje(73), vbOKOnly, "FULLSTEP PS"
End Sub

Public Sub ConexionIncorrecta()
    
    MsgBox CargarTextoMensaje(74), vbCritical, "FULLSTEP PS"
    
End Sub

Public Sub FalloEnConexionConServidor()
    MsgBox CargarTextoMensaje(75), vbCritical, "FULLSTEP PS"
End Sub
Public Sub FalloEnDesconexionConServidor()
    MsgBox CargarTextoMensaje(76), vbCritical, "FULLSTEP PS"
End Sub
Public Sub FalloAlCrearLinkedServer()
    MsgBox CargarTextoMensaje(77), vbCritical, "FULLSTEP PS"
End Sub
Public Sub FalloAlAnyadirLinkedServer()
    MsgBox CargarTextoMensaje(78), vbCritical, "FULLSTEP PS"
End Sub
Public Sub FalloEnSetOptions()
    MsgBox CargarTextoMensaje(79), vbCritical, "FULLSTEP PS"
End Sub
Public Sub FalloAlCrearLinkLogin()
    MsgBox CargarTextoMensaje(80), vbCritical, "FULLSTEP PS"
End Sub
Public Sub FalloAlAnyadirLinkLogin()
    MsgBox CargarTextoMensaje(81), vbCritical, "FULLSTEP PS"
End Sub
Public Sub FalloEnConexionConSrvRemoto(ByVal Servidor As String)
    MsgBox CargarTextoMensaje(82) & Servidor, vbCritical, "FULLSTEP PS"
End Sub
Public Sub FalloEnDesconexionConSrvRemoto(ByVal Servidor As String)
    MsgBox CargarTextoMensaje(83) & Servidor, vbCritical, "FULLSTEP PS"
End Sub
Public Sub FalloAlConfigurarSrvRemoto(ByVal Servidor As String)
    MsgBox CargarTextoMensaje(84) & Servidor, vbCritical, "FULLSTEP PS"
End Sub
Public Sub FalloAlBorrarLinkedServer(ByVal LnkServer As String)
    MsgBox CargarTextoMensaje(85) & LnkServer, vbCritical, "FULLSTEP PS"
End Sub
Public Sub FalloAlBorrarLinkedLogin(ByVal LnkServer As String)
    MsgBox CargarTextoMensaje(86) & LnkServer, vbCritical, "FULLSTEP PS"
End Sub
Public Sub OtroErrorDeSQLDMO(ByVal LnkServer As String)
    MsgBox CargarTextoMensaje(87) & LnkServer, vbCritical, "FULLSTEP PS"
End Sub
Public Sub LinkedServerCreadoCorrectamente(ByVal LnkServer As String)
    MsgBox CargarTextoMensaje(88) & LnkServer, vbExclamation, "FULLSTEP PS"
End Sub
Public Sub LinkedServerBorradoCorrectamente(ByVal LnkServer As String)
    MsgBox CargarTextoMensaje(89) & LnkServer, vbExclamation, "FULLSTEP PS"
End Sub

Public Sub LinkedServerCancelado()
    MsgBox CargarTextoMensaje(90) & vbCrLf & CargarTextoMensaje(91) & vbCrLf & "Consulte con el administrador del sistema.", vbExclamation, "FULLSTEP PS"
End Sub

Public Sub NivelActividades()
    MsgBox CargarTextoMensaje(92) & g_udtParametrosGenerales.g_iNivelMinAct & "", vbExclamation, "FULLSTEP PS"
End Sub

Public Sub DesenlazarCentral()
    MsgBox CargarTextoMensaje(93), vbExclamation, "FULLSTEP PS"
End Sub

Public Sub EnlazarCentral()
    MsgBox CargarTextoMensaje(94), vbExclamation, "FULLSTEP PS"
End Sub

Public Sub EnlazarCentralVacia()
    MsgBox CargarTextoMensaje(95), vbExclamation, "FULLSTEP PS"
End Sub

Public Sub CompradoraSinServidor()
    MsgBox CargarTextoMensaje(96), vbExclamation, "FULLSTEP PS"
End Sub
Public Sub ImposibleConectarAlServidorDeInformes()
    Dim sMensaje2 As String

    sMensaje = CargarTextoMensaje(97)
    sMensaje2 = CargarTextoMensaje(98)
    
    MsgBox sMensaje & vbLf & sMensaje2, vbCritical + vbOKOnly, "FULLSTEP PS"
    
End Sub

Public Function PreguntaModEnlaceMonCentral() As Integer
    Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(99) & vbLf & CargarTextoMensaje(46), vbYesNo + vbQuestion, "FULLSTEP PS")
    PreguntaModEnlaceMonCentral = iRespuesta
    
End Function
Public Function PreguntaConfirmarEstructuraAct() As Integer
    Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(100), vbQuestion + vbYesNo, "FULLSTEP PS")
    PreguntaConfirmarEstructuraAct = iRespuesta
    
End Function
Public Function PreguntaConfirmarRegistroCiasComp() As Integer
    Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(101), vbQuestion + vbYesNo, "FULLSTEP PS")
    PreguntaConfirmarRegistroCiasComp = iRespuesta
    
End Function
Public Function PreguntaDeshacerRegistroCiasComp() As Integer
    Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(102), vbQuestion + vbYesNo, "FULLSTEP PS")
    PreguntaDeshacerRegistroCiasComp = iRespuesta
    
End Function
Public Function PreguntaContinuarConfirmarRegistroCiasComp() As Integer
    Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(103), vbQuestion + vbYesNo, "FULLSTEP PS")
    PreguntaContinuarConfirmarRegistroCiasComp = iRespuesta
    
End Function
Public Function PreguntaCancelCambEstructuraAct() As Integer
    Dim iRespuesta As Integer
    
    iRespuesta = MsgBox(CargarTextoMensaje(104), vbQuestion + vbYesNo, "FULLSTEP PS")
    PreguntaCancelCambEstructuraAct = iRespuesta
    
End Function

Public Sub FechaHastaMenor()
    
    MsgBox CargarTextoMensaje(105), vbExclamation + vbOKOnly, "FULLSTEP PS"

End Sub


Public Sub SinProvePremium()
    
    MsgBox CargarTextoMensaje(106), vbExclamation, "FULLSTEP PS"

End Sub

Public Sub SeleccionarCia()
    MsgBox CargarTextoMensaje(107), vbExclamation, "FULLSTEP PS"
End Sub

Public Sub SeleccionarMaterial()
    MsgBox CargarTextoMensaje(108), vbExclamation, "FULLSTEP PS"
End Sub

Public Sub PaisYaRelacionado()
    MsgBox CargarTextoMensaje(109), vbExclamation, "FULLSTEP PS"
End Sub

Public Sub ProveYaRelacionado(ByVal sProve As String)
    MsgBox CargarTextoMensaje(110) & vbCrLf & CargarTextoMensaje(111) & sProve, vbExclamation, "FULLSTEP PS"
End Sub

Public Function PreguntaEliminarModerador(ByRef foros() As String) As Integer
Dim iRespuesta As Integer
Dim strforos As String
Dim i As Integer

    i = 0
    While i < UBound(foros)
        
        strforos = strforos & foros(i) & vbLf
        i = i + 1
    Wend
    
    iRespuesta = MsgBox(CargarTextoMensaje(112) & vbLf & strforos & vbLf & CargarTextoMensaje(56), vbYesNo + vbQuestion, "FULLSTEP PS")
    PreguntaEliminarModerador = iRespuesta
    
End Function
Public Sub MonSinEquivalencia()
    MsgBox CargarTextoMensaje(113), vbExclamation, "FULLSTEP PS"
End Sub
Public Sub ImposibleCambiarNivel()
    MsgBox CargarTextoMensaje(114), vbExclamation, "FULLSTEP PS"
End Sub



Public Sub ImposibleConectarDMO()
    MsgBox CargarTextoMensaje(115), vbCritical + vbOKOnly, "FULLSTEP PS"
End Sub

Public Sub CompradoraDuplicada()
    MsgBox CargarTextoMensaje(116) & sCiaComp & vbCrLf & CargarTextoMensaje(117), vbCritical, "FULLSTEP PS"
End Sub


Public Sub FaltanDatosServidorCorreo()
    MsgBox CargarTextoMensaje(118), vbCritical + vbOKOnly, "FULLSTEP PS"
End Sub

Public Sub RutaPlantillasNoValida()
    MsgBox CargarTextoMensaje(119), vbCritical + vbOKOnly, "FULLSTEP PS"
End Sub


Public Sub NifIncorrecto()
    MsgBox CargarTextoMensaje(120), vbCritical + vbOKOnly, "FULLSTEP PS"
End Sub

Public Function PreguntarEliminarMonedaEnlazada(ByVal sCompradora As String) As Integer
Dim iRespuesta As Integer
Dim sMensaje2 As String

Dim arrCompradoras As Variant
arrCompradoras = Split(sCompradora, vbCrLf)
If UBound(arrCompradoras) > 1 Then
    sMensaje2 = CargarTextoMensaje(121)
Else
    sMensaje2 = CargarTextoMensaje(123)
End If

    
iRespuesta = MsgBox(sMensaje2 & vbLf & sCompradora & vbLf & CargarTextoMensaje(122), vbYesNo + vbQuestion, "FULLSTEP PS")

PreguntarEliminarMonedaEnlazada = iRespuesta
    

End Function

Public Sub ImposibleEliminarMonedaCentral()
    MsgBox CargarTextoMensaje(124), vbCritical + vbOKOnly, "FULLSTEP PS"
End Sub

Public Sub DireccionCCNoValida()
    MsgBox CargarTextoMensaje(126), vbCritical + vbOKOnly, "FULLSTEP PS"
End Sub

Public Sub DireccionCCoNoValida()
    MsgBox CargarTextoMensaje(127), vbCritical + vbOKOnly, "FULLSTEP PS"
End Sub

Public Sub DireccionRespuestaNoValida()
    MsgBox CargarTextoMensaje(128), vbCritical + vbOKOnly, "FULLSTEP PS"
End Sub

Public Sub DireccionCorreoNoValida()
    MsgBox CargarTextoMensaje(129), vbCritical + vbOKOnly, "FULLSTEP PS"
End Sub


Public Sub errormail(ByVal Descr As String)
    
    MsgBox Descr, vbOKOnly, CargarTextoMensaje(125), , "FULLSTEP PS"
    
End Sub

''' <summary>
''' Muestra un mensaje de error u otro segun cual sea el indicado en TESErr.NumError
''' </summary>
''' <param name="TESErr">Error</param>
''' <remarks>Llamada desde: Multiples pantallas ; Tiempo m�ximo: 0,2</remarks>
Public Sub ErrorOriginal(TESErr As TipoErrorSummit)
    Dim bErrorEmail As Boolean
    Dim mail As String
    Dim Num As Integer
    Dim sAux As String
    Dim sMensaje2 As String
    
    bErrorEmail = True
    Select Case TESErr.NumError
        Case erroressummit.TESASPUser
            sMensaje = CargarTextoMensaje(130) & vbCrLf
        Case erroressummit.TESASPUserAddUser
            sMensaje = CargarTextoMensaje(131) & vbCrLf
        Case erroressummit.TESASPUserRemoteGrupo
            sMensaje = CargarTextoMensaje(132) & vbCrLf
        Case erroressummit.TESASPUserPropiedades
            sMensaje = CargarTextoMensaje(133) & vbCrLf
        Case erroressummit.TESADSIExProgramaIni
            sMensaje = CargarTextoMensaje(134) & vbCrLf
        Case erroressummit.TESADSIExHomeDir
            sMensaje = CargarTextoMensaje(135) & vbCrLf
        Case erroressummit.TESRunAsUser
            sMensaje = CargarTextoMensaje(136) & vbCrLf
        Case erroressummit.TESASPUserCambiaPwd
            sMensaje = CargarTextoMensaje(137) & vbCrLf
        Case erroressummit.TESASPUserRename
            sMensaje = CargarTextoMensaje(138) & vbCrLf
        Case erroressummit.TESASPUserRemove
            sMensaje = CargarTextoMensaje(139) & vbCrLf
        Case erroressummit.TESmailsmtp
            sMensaje = CargarTextoMensaje(140) & vbCrLf
            
            If TESErr.Arg1 = -1000000 Then
                mail = TESErr.Arg2
                Num = CInt(Mid(mail, InStr(mail, "###") + 3))
                mail = Mid(mail, 1, InStr(mail, "###") - 2)
            
                If Num = 1 Then
                    sMensaje = sMensaje & CargarTextoMensaje(170) & " " & mail & vbCrLf
                Else
                    sMensaje = sMensaje & CargarTextoMensaje(171) & " " & mail & vbCrLf
                End If
                sMensaje = sMensaje & CargarTextoMensaje(144)
            ElseIf TESErr.Arg1 < 0 Then
                mail = TESErr.Arg2
                
                sAux = Mid(mail, 1, InStr(mail, "|||") - 1)
                sMensaje2 = Mid(mail, InStr(mail, "|||") + 3)
                
                Num = CInt(Mid(sAux, InStr(sAux, "###") + 3))
                mail = Mid(sAux, 1, InStr(sAux, "###") - 2)
            
                If Num = 1 Then
                    sMensaje = sMensaje & "(1) " & CargarTextoMensaje(170) & " " & mail & vbCrLf
                Else
                    sMensaje = sMensaje & "(1) " & CargarTextoMensaje(171) & " " & mail & vbCrLf
                End If
                sMensaje = sMensaje & "(2) " & (-1 * TESErr.Arg1) & " - " & sMensaje2 & vbCrLf
                sMensaje = sMensaje & CargarTextoMensaje(141) & vbCrLf
                sMensaje = sMensaje & CargarTextoMensaje(144)
            End If
        Case Else
            bErrorEmail = False
            sMensaje = CargarTextoMensaje(142) & vbCrLf
    End Select

    If Not ((TESErr.NumError = erroressummit.TESmailsmtp) And (TESErr.Arg1 < 0)) Then
        sMensaje = sMensaje & TESErr.Arg1 & " - " & TESErr.Arg2 & vbCrLf
       
        If bErrorEmail = True Then
            sMensaje = sMensaje & CargarTextoMensaje(141) & vbCrLf
            sMensaje = sMensaje & CargarTextoMensaje(144)
        End If
    
    End If
    
    MsgBox sMensaje, vbCritical + vbOKOnly, "FULLSTEP PS"
    
End Sub


Public Sub SeleccioneCia()
    MsgBox CargarTextoMensaje(143), vbExclamation + vbOKOnly, "FULLSTEP PS"
End Sub


Public Function ExistenProveedoresVinculados(sProves As String) As Integer

Dim iRespuesta As Integer

sMensaje = CargarTextoMensaje(145) & vbCrLf & sProves & CargarTextoMensaje(66)
    
iRespuesta = MsgBox(sMensaje, vbYesNo + vbQuestion, "FULLSTEP PS")

ExistenProveedoresVinculados = iRespuesta
    

End Function

Public Sub sinUsuarioPrincipal()
MsgBox CargarTextoMensaje(146), vbExclamation + vbOKOnly, "FULLSTEP PS"
End Sub

Public Sub TamanyoIncorrecto()
    MsgBox CargarTextoMensaje(147), vbExclamation + vbOKOnly, "FULLSTEP PS"
End Sub

Public Sub ImposibleModificarTamanyo(Optional ByVal vSize As Variant)
    If Not IsMissing(vSize) Then
        MsgBox CargarTextoMensaje(147) & vbCrLf & CargarTextoMensaje(149) & ":" & vSize, vbExclamation + vbOKOnly, "FULLSTEP PS"
    Else
        MsgBox CargarTextoMensaje(147) & vbCrLf & CargarTextoMensaje(148), vbExclamation + vbOKOnly, "FULLSTEP PS"
    End If
End Sub

Public Sub ImposibleAdjuntarArchivo()
    MsgBox CargarTextoMensaje(150), vbExclamation + vbOKOnly, "FULLSTEP PS"
End Sub

'*******************************************************************************
'*** Descripci�n: Mensaje de NIF repetido en otra Cia.                     *****
'*** Par�metros: sNIF: NIF                                                 *****
'*** Valor que devuelve:                                                   *****
'*******************************************************************************

Public Sub NIFCiaRepetido(ByVal sNif As String)
    MsgBox CargarTextoMensaje(154), vbExclamation + vbOKOnly, "FULLSTEP PS"
End Sub

'*******************************************************************************
'*** Descripci�n: Mensaje de NIF repetido en Proveedor enlazado a Cia.     *****
'*** Par�metros: sNIF: NIF                                                 *****
'*** Valor que devuelve:                                                   *****
'*******************************************************************************

Public Sub NIFCiaProveRepetido(ByVal sNif As String)
    MsgBox CargarTextoMensaje(151), vbExclamation + vbOKOnly, "FULLSTEP PS"
End Sub

'*******************************************************************************
'*** Descripci�n: Mensaje de Tel�fono no v�lido                            *****
'*** Par�metros: sTelf: Tel�fono                                           *****
'*** Valor que devuelve:                                                   *****
'*******************************************************************************

Public Sub TelefonoNoValido(ByVal sTelf As String)
    MsgBox CargarTextoMensaje(152) & " " & sTelf, vbExclamation + vbOKOnly, "FULLSTEP PS"
End Sub

'*******************************************************************************
'*** Descripci�n: Mensaje de E-Mail no v�lido                              *****
'*** Par�metros: sEMail: E-Mail                                            *****
'*** Valor que devuelve:                                                   *****
'*******************************************************************************

Public Sub EMailNoValido(ByVal sEMail As String)
    MsgBox CargarTextoMensaje(153) & " " & sEMail, vbExclamation + vbOKOnly, "FULLSTEP PS"
End Sub

''' <summary>Mensaje que te pregunta si deseas dar de alta la categor�a laboral seleccionada que ahora mismo est� de baja l�gica.</summary>
''' <param name></param>
''' <returns>Si se acepta o no</returns>
''' <remarks>Llamada desde frmCompanias; Tiempo m�ximo=0seg.</remarks>

Public Function DarDeAltaLaCategoria() As Integer
Dim iRespuesta As Integer
Dim sMensaje As String
     
    sMensaje = CargarTextoMensaje(155)
    sMensaje = sMensaje & vbCrLf & CargarTextoMensaje(66)
    
    iRespuesta = MsgBox(sMensaje, vbQuestion + vbYesNo, "FULLSTEP PS")
    DarDeAltaLaCategoria = iRespuesta
    
End Function

''' <summary>Mensaje que te recuerda que la categor�a a dar de baja est� asociada a alg�n usuario del proveedor.</summary>
''' <param name="sCategoria">Denominaci�n de la categor�a.</param>
''' <returns>Si se acepta o no</returns>
''' <remarks>Llamada desde frmCompanias; Tiempo m�ximo=0seg.</remarks>

Public Function CategoriaAsociadaAUsusProve_BajaLog(ByVal sCategoria As String) As Integer
Dim iRespuesta As Integer
Dim sMensaje As String
     
    sMensaje = CargarTextoMensaje(156)
    sMensaje = sMensaje & vbCrLf & sCategoria & vbCrLf
    sMensaje = sMensaje & vbCrLf & CargarTextoMensaje(157)
    sMensaje = sMensaje & vbCrLf & CargarTextoMensaje(66)
    
    iRespuesta = MsgBox(sMensaje, vbQuestion + vbYesNo, "FULLSTEP PS")
    CategoriaAsociadaAUsusProve_BajaLog = iRespuesta
    
End Function

''' <summary>Se preguntar� al usuario si quiere dar una categor�a de baja l�gica en el sistema o por el contrario dejarla en el repositorio de categor�as laborales para usos posteriores.</summary>
''' <param name="sCategoria">Denominaci�n de la categor�a.</param>
''' <returns>Si se acepta o no</returns>
''' <remarks>Llamada desde frmCompanias; Tiempo m�ximo=0seg.</remarks>

Public Function Categoria_BajaLog(ByVal sCategoria As String) As Integer
Dim iRespuesta As Integer
Dim sMensaje As String
     
    sMensaje = CargarTextoMensaje(158)
    sMensaje = sMensaje & vbCrLf & CargarTextoMensaje(159) & vbCrLf
    sMensaje = sMensaje & vbCrLf & CargarTextoMensaje(160) & sCategoria
    
    iRespuesta = MsgBox(sMensaje, vbQuestion + vbYesNo, "FULLSTEP PS")
    Categoria_BajaLog = iRespuesta
    
End Function

''' <summary>Mensaje que te recuerda que la categor�a a eliminar est� asociada a alg�n usuario del proveedor.</summary>
''' <param name="sCategoria">Denominaci�n de la categor�a.</param>
''' <returns>Si se acepta o no</returns>
''' <remarks>Llamada desde frmCompanias; Tiempo m�ximo=0seg.</remarks>

Public Function CategoriaAsociadaAUsusProve_Eliminar(ByVal sCategoria As String) As Integer
Dim iRespuesta As Integer
Dim sMensaje As String
     
    sMensaje = CargarTextoMensaje(161)
    sMensaje = sMensaje & vbCrLf & sCategoria & vbCrLf
    sMensaje = sMensaje & vbCrLf & CargarTextoMensaje(162)
    sMensaje = sMensaje & vbCrLf & CargarTextoMensaje(66)
    
    iRespuesta = MsgBox(sMensaje, vbQuestion + vbYesNo, "FULLSTEP PS")
    CategoriaAsociadaAUsusProve_Eliminar = iRespuesta
    
End Function

''' <summary>Se preguntar� al usuario si quiere eliminar una categor�a en el sistema o por el contrario dejarla en el repositorio de categor�as laborales para usos posteriores.</summary>
''' <param name="sCategoria">Denominaci�n de la categor�a.</param>
''' <returns>Si se acepta o no</returns>
''' <remarks>Llamada desde frmCompanias; Tiempo m�ximo=0seg.</remarks>

Public Function Categoria_Eliminar(ByVal sCategoria As String) As Integer
Dim iRespuesta As Integer
Dim sMensaje As String
     
    sMensaje = CargarTextoMensaje(163)
    sMensaje = sMensaje & vbCrLf & CargarTextoMensaje(164) & vbCrLf
    sMensaje = sMensaje & vbCrLf & CargarTextoMensaje(165) & sCategoria
    
    iRespuesta = MsgBox(sMensaje, vbQuestion + vbYesNo, "FULLSTEP PS")
    Categoria_Eliminar = iRespuesta
    
End Function

'''''' <summary>Se informa al usuario de que esa categoria ya est� asignada al proveedor.</summary>
'''''' <param name="sCategoria">Denominaci�n de la categor�a.</param>
'''''' <returns>Si se acepta o no</returns>
'''''' <remarks>Llamada desde frmCompanias; Tiempo m�ximo=0seg.</remarks>
'''
'''Public Sub Categoria_Asignada(ByVal sCategoria As String)
'''Dim sMensaje As String
'''
'''    sMensaje = "La categor�a seleccionada ya est� asignada a este proveedor. "
'''
'''    iRespuesta = MsgBox(sMensaje, vbExclamation + vbOKOnly, "FULLSTEP PS")
'''
'''End Sub

''' <summary>Se informa se que el usuario que se pretende eliminar est� en proyecto de FSGA.</summary>
''' <param name></param>
''' <returns>Si se acepta o no</returns>
''' <remarks>Llamada desde frmCompanias; Tiempo m�ximo=0seg.</remarks>

Public Sub UusarioEnProyectosFSGA()
Dim sMensaje As String
     
    sMensaje = CargarTextoMensaje(166)
    
    iRespuesta = MsgBox(sMensaje, vbExclamation + vbOKOnly, "FULLSTEP PS")
    
End Sub

''' <summary>El c�digo de categoria es un campo obligatorio.</summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde frmCompanias; Tiempo m�ximo=0seg.</remarks>

Public Sub AvisoCodCategoria()
     
    sMensaje = CargarTextoMensaje(167)
   
    iRespuesta = MsgBox(sMensaje, vbExclamation + vbOKOnly, "FULLSTEP PS")
    
End Sub

''' <summary>La denominaci�n de la categoria es un campo obligatorio.</summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde frmCompanias; Tiempo m�ximo=0seg.</remarks>

Public Sub AvisoDenCategoria()
     
    sMensaje = CargarTextoMensaje(168)
    
    iRespuesta = MsgBox(sMensaje, vbExclamation + vbOKOnly, "FULLSTEP PS")
    
End Sub

''' <summary>El coste/hora de la categor�a es un campo n�merico obligatorio.</summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde frmCompanias; Tiempo m�ximo=0seg.</remarks>

Public Sub AvisoCosteHoraCategoria()
     
    sMensaje = CargarTextoMensaje(169)
    
    iRespuesta = MsgBox(sMensaje, vbExclamation + vbOKOnly, "FULLSTEP PS")
    
End Sub
