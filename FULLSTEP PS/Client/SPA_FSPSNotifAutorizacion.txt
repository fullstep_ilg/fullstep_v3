Le notificamos que ha sido autorizado en el portal de compras de @PORTAL.
Le adjuntamos los datos personales y de acceso con los que ha sio registrado.
@INICIOALTAUSUARIOPara poder acceder con su c�digo de compa��a y usuario es necesario que establezca una contrase�a a trav�s del siguiente enlace: 

@LINKALTA
@FINALTAUSUARIO

Datos de acceso:
  C�digo de cia: @CODCIA
  C�digo de usuario: @CODUSU


Datos de la cia:
  Cia registrada: @DENCIA
  NIF de la cia: @NIF
  C.P.: @CP
  Poblaci�n: @POB

  Provincia: 
    C�digo: @CODPROVI
    Denominaci�n: @DENPROVI

  Pa�s:
    C�digo: @CODPAI
    Denominaci�n: @DENPAI

  Moneda:
    C�digo: @CODMON
    Denominaci�n: @DENMON

  Idioma seleccionado para la cia:
    C�digo: @IDICOD
    Denominaci�n: @IDIDEN

  Direcci�n web de la cia: @URL

  Funci�n compradora autorizada: @FC
  Funci�n proveedora autorizada: @FP
  Funci�n Premium autorizada: @PREMIUM

  Actividades en las que se ha registrado:

@ACTIVIDADES

  Compa��as compradoras en las que est� registrada:

@SUSCRIPCIONCIAS


Datos del usuario:
  C�digo: @CODUSU
  Nombre: @NOMUSU
  Apellidos: @APEUSU
  Tel�fono 1: @TFNO1USU
  Tel�fono 2: @TFNO2USU
  Tel�fono m�vil: @MOVILUSU
  Fax: @FAXUSU
  E-mail: @EMAILUSU
  Cargo: @CARGOUSU
  Departamento: @DEPUSU

  Idioma:
    C�digo: @CODIDIUSU
    Denominaci�n: @DENIDIUSU


Un saludo

(Si desea recibir este email en HTML acceda al portal y modifique sus preferencias en el apartado de registro de usuario)
