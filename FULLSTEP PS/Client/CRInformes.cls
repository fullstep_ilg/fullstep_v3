VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRInformes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Sub ListadoVolGeneral(oReport As CRAXDRT.Report, FecDesde As Variant, FecHasta As Variant, OrdenarPorCod As Boolean, OrdenarPorDen As Boolean, OrdenarPorVol As Boolean)

    Dim Table As CRAXDRT.DatabaseTable
    Dim RecordSortFields As String
    Dim sSQL As String
    Dim sRecordSelectionFormula As String
    Dim GroupOrder1 As String
    
    If crs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
    Next
    
    'sSQL = ObtenerSQLVolumenGeneral(OrdenarPorCod, OrdenarPorDen, OrdenarPorVol)
    'oReport.SQLQueryString = sSQL
    
    'Condiciones de b�squeda
    sRecordSelectionFormula = ""
    sRecordSelectionFormula = "CDate({VOLADJ.FECADJ}) >= CDate('" & DblQuote(FecDesde) & "')"
    sRecordSelectionFormula = sRecordSelectionFormula & " AND CDate({VOLADJ.FECADJ}) <= CDate('" & DblQuote(FecHasta) & "')"
    oReport.RecordSelectionFormula = sRecordSelectionFormula
    
    
    'Orden del informe
    If OrdenarPorCod Then   'ordenado por c�digo
        RecordSortFields = "+{CIAS.COD}"
        GroupOrder1 = "{CIAS.COD}"
    End If
    
    If OrdenarPorDen = True Then   'ordenado por denominaci�n
        RecordSortFields = "+{CIAS.DEN}"
        GroupOrder1 = "{CIAS.DEN}"
    End If

    While oReport.RecordSortFields.Count > 0
        oReport.RecordSortFields.Delete 1
    Wend
    
    If OrdenarPorCod Or OrdenarPorDen Then
        oReport.GroupSortFields.Delete 1
        oReport.FormulaFields(crs_FormulaIndex(oReport, "ORDEN_GRUPO")).Text = GroupOrder1
        oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex(oReport, crs_SortTable(RecordSortFields))).Fields(crs_FieldIndex(oReport, crs_SortTable(RecordSortFields), crs_SortField(RecordSortFields))), crs_SortDirection(RecordSortFields)
    End If

    
End Sub

Public Sub ListadoVolProv(oReport As CRAXDRT.Report, FecDesde As Variant, FecHasta As Variant, Cia As Integer, OrdenarPorCod As Boolean, OrdenarPorDen As Boolean, OrdenarPorVol As Boolean, OrdenarPorVolgs As Boolean)

    Dim Table As CRAXDRT.DatabaseTable
    Dim RecordSortFields As String
    Dim sSQL As String
    Dim sRecordSelectionFormula As String
    Dim GroupOrder1 As String
    
    If crs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
                                                    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
    Next

    
    'Condiciones de b�squeda
    sRecordSelectionFormula = ""
    sRecordSelectionFormula = "CDate({VOLADJ.FECADJ}) >= CDate('" & DblQuote(FecDesde) & "')"
    sRecordSelectionFormula = sRecordSelectionFormula & " AND CDate({VOLADJ.FECADJ}) <= CDate('" & DblQuote(FecHasta) & "')"
    sRecordSelectionFormula = sRecordSelectionFormula & " AND {VOLADJ.CIA_PROVE} =" & Cia

    oReport.RecordSelectionFormula = sRecordSelectionFormula
    
    'Orden del informe
    If OrdenarPorCod Then
        RecordSortFields = "+{CIAS.COD}"
        GroupOrder1 = "{CIAS.COD}"
    End If
    
    If OrdenarPorDen = True Then
        RecordSortFields = "+{CIAS.DEN}"
        GroupOrder1 = "{CIAS.DEN}"
    End If
    
    If OrdenarPorVolgs = True Then
        RecordSortFields = "+{VOLADJ.VOLGS}"
    End If
    
    While oReport.RecordSortFields.Count > 0
        oReport.RecordSortFields.Delete 1
    Wend
    
    If OrdenarPorCod Or OrdenarPorDen Then
        oReport.GroupSortFields.Delete 1
        oReport.FormulaFields(crs_FormulaIndex(oReport, "ORDEN_GRUPO")).Text = GroupOrder1
        oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex(oReport, crs_SortTable(RecordSortFields))).Fields(crs_FieldIndex(oReport, crs_SortTable(RecordSortFields), crs_SortField(RecordSortFields))), crs_SortDirection(RecordSortFields)
        
    ElseIf OrdenarPorVolgs Then
        oReport.GroupSortFields.Delete 1
        oReport.GroupSortFields.Add oReport.SummaryFields.Item(1), crAscendingOrder
    End If
End Sub


