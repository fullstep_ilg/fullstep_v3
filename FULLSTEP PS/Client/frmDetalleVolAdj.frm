VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmDetalleVolAdj 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Detalle del volumen adjudicado a "
   ClientHeight    =   3285
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9690
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDetalleVolAdj.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3285
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin SSDataWidgets_B.SSDBGrid sdbgDetalle 
      Height          =   2710
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9495
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   6
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   6
      Columns(0).Width=   1720
      Columns(0).Caption=   "Comprador"
      Columns(0).Name =   "COMP"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   5477
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      Columns(2).Width=   3493
      Columns(2).Caption=   "Adjudicado "
      Columns(2).Name =   "ADJ"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "Standard"
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   16777215
      Columns(3).Width=   3493
      Columns(3).Caption=   "Adjudicado (Moneda GS)"
      Columns(3).Name =   "ADJGS"
      Columns(3).Alignment=   1
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).NumberFormat=   "Standard"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   16777215
      Columns(4).Width=   1931
      Columns(4).Caption=   "Moneda GS"
      Columns(4).Name =   "MONGS"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "ID"
      Columns(5).Name =   "ID"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   16748
      _ExtentY        =   4780
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTotales 
      Height          =   315
      Left            =   120
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   2880
      Width           =   9495
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   3
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmDetalleVolAdj.frx":0CB2
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4741885
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmDetalleVolAdj.frx":0CCE
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmDetalleVolAdj.frx":0CEA
      DividerType     =   1
      DividerStyle    =   2
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   503
      ExtraHeight     =   344
      Columns.Count   =   3
      Columns(0).Width=   7699
      Columns(0).Caption=   "PROV"
      Columns(0).Name =   "PROV"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   3519
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).Alignment=   1
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).NumberFormat=   "Standard"
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   8811
      Columns(2).Caption=   "ADJ"
      Columns(2).Name =   "ADJ"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      _ExtentX        =   16748
      _ExtentY        =   556
      _StockProps     =   79
      ForeColor       =   0
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmDetalleVolAdj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private DenMonCentral As String

Public FecDesde As Date
Public FecHasta As Date
Public CiaProv As Long
'Public CiaComp As Integer

Public strCodProv As String
Public strDenProv As String

Private sIdiTotalAdj As String

Private Sub Form_Load()
    Dim Ador As Ador.Recordset
    CargarRecursos
    
    'Obtiene el c�digo de la moneda central
    Set Ador = g_oRaiz.CodMonedaCentral
    DenMonCentral = Ador(1)
    sdbgDetalle.Columns(2).Caption = Me.sdbgDetalle.Columns(2).Caption & "(" & Ador(0) & ")"
    
    Ador.Close
    Set Ador = Nothing
   
End Sub


Public Sub CargarVolumen(Optional ByVal FecDesde As Date, Optional ByVal FecHasta As Date, Optional ByVal CiaProv As Long, Optional ByVal CiaComp As Long)
    Dim Ador As Ador.Recordset
    Dim sum As Double
    
    Screen.MousePointer = vbHourglass
    
    'Limpia los grids
    sdbgDetalle.RemoveAll
    sdbgTotales.RemoveAll
    
    Set Ador = g_oGestorInformes.DevolverVolumenPorProcesos(FecDesde, FecHasta, CiaProv, CiaComp, 1)
    If Not Ador Is Nothing Then
        sum = 0
        While Not Ador.EOF
            sdbgDetalle.AddItem Ador("COD") & Chr(9) & Ador("DEN") & Chr(9) & Ador("VOLPOR") & Chr(9) & Ador("VOLGS") & Chr(9) & Ador("MGS") & Chr(9) & Ador("ID")
            sum = sum + NullToDbl0(Ador("VOLPOR"))
            Ador.MoveNext
        Wend
        
        'Calcula el total
        sdbgTotales.AddItem "  " & sIdiTotalAdj & "  " & Chr(9) & sum & Chr(9) & DenMonCentral
    End If
    
    Ador.Close
    Set Ador = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgDetalle_DblClick()
    If sdbgDetalle.Rows < 1 Then Exit Sub
    
     'Carga el detalle
     frmDetalleProcAdj.CargarVolumen FecDesde, FecHasta, CiaProv, sdbgDetalle.Columns(5).Value
     
     'Caption del formulario
     frmDetalleProcAdj.Caption = frmDetalleProcAdj.Caption & strCodProv & " - " & strDenProv & " por " & sdbgDetalle.Columns(0).Value
     
     frmDetalleProcAdj.FecDesde = FecDesde
     frmDetalleProcAdj.FecHasta = FecHasta
     frmDetalleProcAdj.CiaComp = sdbgDetalle.Columns(5).Value
     frmDetalleProcAdj.CiaProv = CiaProv

     frmDetalleProcAdj.Show vbModal
     
End Sub

Private Sub sdbgDetalle_HeadClick(ByVal ColIndex As Integer)
    '''Ordena el grid seg�n la columna
    
    Dim Ador As Ador.Recordset
    
    Screen.MousePointer = vbHourglass
    
    'Limpia el grid
    sdbgDetalle.RemoveAll
    
    Set Ador = g_oGestorInformes.DevolverVolumenPorProcesos(FecDesde, FecHasta, CiaProv, , ColIndex)
    If Not Ador Is Nothing Then
        While Not Ador.EOF
            sdbgDetalle.AddItem Ador("COD") & Chr(9) & Ador("DEN") & Chr(9) & Ador("VOLPOR") & Chr(9) & Ador("VOLGS") & Chr(9) & Ador("MGS") & Chr(9) & Ador("ID")
            Ador.MoveNext
        Wend
    End If
    
    Ador.Close
    Set Ador = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_DETALLEVOLADJ, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgDetalle.Columns(0).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgDetalle.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgDetalle.Columns(2).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgDetalle.Columns(3).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgDetalle.Columns(4).Caption = Ador(0).Value
        Ador.MoveNext
        sIdiTotalAdj = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub

