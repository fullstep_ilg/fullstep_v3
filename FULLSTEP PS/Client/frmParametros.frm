VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmParametros 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Par�metros"
   ClientHeight    =   7395
   ClientLeft      =   1050
   ClientTop       =   3225
   ClientWidth     =   9390
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmParametros.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7395
   ScaleWidth      =   9390
   Begin TabDlg.SSTab sstabParam 
      Height          =   6705
      Left            =   105
      TabIndex        =   30
      TabStop         =   0   'False
      Top             =   135
      Width           =   9210
      _ExtentX        =   16245
      _ExtentY        =   11827
      _Version        =   393216
      Style           =   1
      Tabs            =   6
      Tab             =   4
      TabsPerRow      =   6
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Generales"
      TabPicture(0)   =   "frmParametros.frx":014A
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "fraGen1"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Gestion"
      TabPicture(1)   =   "frmParametros.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "picGestion"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Emails del portal shell"
      TabPicture(2)   =   "frmParametros.frx":0182
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraNotif"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Emails del Portal"
      TabPicture(3)   =   "frmParametros.frx":019E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraMail"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "Servidor de correo"
      TabPicture(4)   =   "frmParametros.frx":01BA
      Tab(4).ControlEnabled=   -1  'True
      Tab(4).Control(0)=   "fraOpciones(2)"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "Ruta listados"
      TabPicture(5)   =   "frmParametros.frx":01D6
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "picRutaRpt"
      Tab(5).ControlCount=   1
      Begin VB.PictureBox picRutaRpt 
         BorderStyle     =   0  'None
         Height          =   4995
         Left            =   -74880
         ScaleHeight     =   4995
         ScaleWidth      =   9060
         TabIndex        =   94
         Top             =   540
         Width           =   9060
         Begin VB.Frame fraRutaRpt 
            Caption         =   "Ruta para las plantillas RPT de listados"
            Height          =   2775
            Left            =   0
            TabIndex        =   95
            Top             =   0
            Width           =   8775
            Begin VB.CommandButton cmdRutaRpt 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   8355
               Picture         =   "frmParametros.frx":01F2
               Style           =   1  'Graphical
               TabIndex        =   98
               Top             =   315
               Width           =   315
            End
            Begin VB.DirListBox dirRutaRpt 
               Height          =   1890
               Left            =   105
               TabIndex        =   97
               Top             =   705
               Width           =   8565
            End
            Begin VB.TextBox txtRutaRPT 
               Height          =   315
               Left            =   105
               TabIndex        =   96
               Top             =   300
               Width           =   8160
            End
         End
      End
      Begin VB.Frame fraOpciones 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   6255
         Index           =   2
         Left            =   120
         TabIndex        =   64
         Top             =   300
         Width           =   8925
         Begin VB.TextBox txtSMTP 
            Height          =   285
            Index           =   1
            Left            =   1530
            TabIndex        =   91
            Top             =   570
            Width           =   1065
         End
         Begin VB.CheckBox chkCorreo 
            Caption         =   "Requiere cifrado SSL/TLS"
            Height          =   225
            Index           =   2
            Left            =   1530
            TabIndex        =   90
            Top             =   910
            Width           =   2775
         End
         Begin VB.TextBox txtSMTP 
            Height          =   285
            Index           =   0
            Left            =   1530
            TabIndex        =   89
            Top             =   240
            Width           =   3345
         End
         Begin VB.Frame fraOpciones 
            Height          =   1245
            Index           =   4
            Left            =   210
            TabIndex        =   84
            Top             =   1230
            Width           =   7155
            Begin VB.OptionButton optAutenticacion 
               Caption         =   "dAutenticaci�n basica"
               Height          =   195
               Index           =   0
               Left            =   360
               TabIndex        =   87
               Top             =   930
               Width           =   3030
            End
            Begin VB.OptionButton optAutenticacion 
               Caption         =   "dAutenticaci�n de windows integrada"
               Height          =   195
               Index           =   1
               Left            =   360
               TabIndex        =   86
               Top             =   600
               Width           =   3030
            End
            Begin VB.OptionButton optAutenticacion 
               Caption         =   "dAutenticaci�n Anonima"
               Height          =   195
               Index           =   2
               Left            =   360
               TabIndex        =   85
               Top             =   270
               Width           =   3030
            End
            Begin VB.Label lblSMTP 
               Caption         =   "DModo de autenticaci�n en servidor de correo"
               Height          =   225
               Index           =   10
               Left            =   120
               TabIndex        =   88
               Top             =   0
               Width           =   3405
            End
         End
         Begin VB.Frame fraOpciones 
            Caption         =   "Valores por defecto"
            Height          =   1500
            Index           =   5
            Left            =   210
            TabIndex        =   77
            Top             =   4410
            Width           =   7215
            Begin VB.TextBox txtCC 
               Height          =   285
               Left            =   2000
               TabIndex        =   80
               Top             =   300
               Width           =   5000
            End
            Begin VB.TextBox txtCCO 
               Height          =   285
               Left            =   2000
               TabIndex        =   79
               Top             =   685
               Width           =   5000
            End
            Begin VB.TextBox txtRespuesta 
               Height          =   285
               Left            =   2000
               TabIndex        =   78
               Top             =   1070
               Width           =   5000
            End
            Begin VB.Label lblSMTP 
               Caption         =   "CC:"
               Height          =   225
               Index           =   4
               Left            =   360
               TabIndex        =   83
               Top             =   360
               Width           =   975
            End
            Begin VB.Label lblSMTP 
               Caption         =   "CCo:"
               Height          =   225
               Index           =   5
               Left            =   360
               TabIndex        =   82
               Top             =   745
               Width           =   855
            End
            Begin VB.Label lblSMTP 
               Caption         =   "Enviar respuesta a:"
               Height          =   225
               Index           =   6
               Left            =   360
               TabIndex        =   81
               Top             =   1130
               Width           =   1535
            End
         End
         Begin VB.CheckBox chkMostrarMail 
            Caption         =   "DMostrar los mensajes antes de enviarlos"
            Height          =   225
            Left            =   210
            TabIndex        =   76
            Top             =   6045
            Width           =   3400
         End
         Begin VB.CheckBox chkAcuseRecibo 
            Caption         =   "DAcuse recibo"
            Height          =   225
            Left            =   5400
            TabIndex        =   75
            Top             =   6045
            Width           =   1935
         End
         Begin VB.Frame fraOpciones 
            Height          =   1800
            Index           =   3
            Left            =   210
            TabIndex        =   65
            Top             =   2550
            Width           =   7215
            Begin VB.TextBox txtSMTP 
               Height          =   285
               IMEMode         =   3  'DISABLE
               Index           =   4
               Left            =   2000
               TabIndex        =   69
               Top             =   300
               Width           =   5000
            End
            Begin VB.TextBox txtSMTP 
               Height          =   285
               IMEMode         =   3  'DISABLE
               Index           =   5
               Left            =   2000
               PasswordChar    =   "*"
               TabIndex        =   68
               Top             =   1380
               Width           =   5000
            End
            Begin VB.TextBox txtSMTP 
               Height          =   285
               Index           =   2
               Left            =   2000
               TabIndex        =   67
               Top             =   630
               Width           =   5000
            End
            Begin VB.TextBox txtSMTP 
               Height          =   285
               IMEMode         =   3  'DISABLE
               Index           =   3
               Left            =   2000
               PasswordChar    =   "*"
               TabIndex        =   66
               Top             =   990
               Width           =   5000
            End
            Begin VB.Label lblSMTP 
               Caption         =   "DCuenta de correo:"
               Height          =   225
               Index           =   9
               Left            =   330
               TabIndex        =   74
               Top             =   330
               Width           =   1485
            End
            Begin VB.Label lblSMTP 
               Caption         =   "DServicio plataforma"
               Height          =   225
               Index           =   8
               Left            =   90
               TabIndex        =   73
               Top             =   0
               Width           =   1605
            End
            Begin VB.Label lblSMTP 
               Caption         =   "DDominio:"
               Height          =   225
               Index           =   7
               Left            =   330
               TabIndex        =   72
               Top             =   1410
               Width           =   1125
            End
            Begin VB.Label lblSMTP 
               Caption         =   "Usuario:"
               Height          =   225
               Index           =   2
               Left            =   330
               TabIndex        =   71
               Top             =   705
               Width           =   1065
            End
            Begin VB.Label lblSMTP 
               Caption         =   "Contrase�a:"
               Height          =   225
               Index           =   3
               Left            =   330
               TabIndex        =   70
               Top             =   1050
               Width           =   1125
            End
         End
         Begin VB.Label lblSMTP 
            Caption         =   "Servidor:"
            Height          =   225
            Index           =   0
            Left            =   210
            TabIndex        =   93
            Top             =   300
            Width           =   1185
         End
         Begin VB.Label lblSMTP 
            Caption         =   "Puerto:"
            Height          =   225
            Index           =   1
            Left            =   210
            TabIndex        =   92
            Top             =   600
            Width           =   1185
         End
      End
      Begin VB.Frame fraMail 
         BorderStyle     =   0  'None
         Height          =   4545
         Left            =   -74850
         TabIndex        =   53
         Top             =   540
         Width           =   9015
         Begin VB.CheckBox chkMail1 
            Caption         =   "Notificaci�n al administrador del portal de una solicitud de registro"
            Height          =   375
            Left            =   135
            TabIndex        =   22
            Top             =   450
            Width           =   5895
         End
         Begin VB.CheckBox chkMail2 
            Caption         =   "Notificaci�n al administrador del portal de una solictitud de cambio de actividad"
            Height          =   375
            Left            =   135
            TabIndex        =   23
            Top             =   1185
            Width           =   6015
         End
         Begin VB.CheckBox chkMail3 
            Caption         =   "Notificaci�n al administrador del portal de una solicitud de cambio de suscripci�n a compa��as compradoras"
            Height          =   495
            Left            =   135
            TabIndex        =   24
            Top             =   1905
            Width           =   6015
         End
         Begin VB.CheckBox chkMail4 
            Caption         =   "Usar formato HTML para los emails"
            Height          =   255
            Left            =   3120
            TabIndex        =   25
            Top             =   3225
            Width           =   3375
         End
         Begin VB.Label Label5 
            Caption         =   "Activar el env�o de emails para los siguientes casos:"
            Height          =   255
            Left            =   0
            TabIndex        =   54
            Top             =   -30
            Width           =   6495
         End
         Begin VB.Line Line5 
            X1              =   0
            X2              =   8755
            Y1              =   330
            Y2              =   330
         End
      End
      Begin VB.Frame fraGen1 
         BorderStyle     =   0  'None
         Height          =   4320
         Left            =   -74985
         TabIndex        =   47
         Top             =   405
         Width           =   9045
         Begin VB.TextBox txtEmail 
            Height          =   285
            Left            =   2745
            TabIndex        =   5
            Top             =   3090
            Width           =   3735
         End
         Begin VB.TextBox txtAdm 
            Height          =   285
            Left            =   2745
            TabIndex        =   4
            Top             =   2460
            Width           =   3735
         End
         Begin VB.TextBox txtMonCen 
            BackColor       =   &H00C0FFFF&
            Height          =   285
            Left            =   2745
            Locked          =   -1  'True
            TabIndex        =   3
            Top             =   1830
            Width           =   3735
         End
         Begin VB.TextBox txtURL 
            Height          =   285
            Left            =   2745
            TabIndex        =   2
            Top             =   1185
            Width           =   3735
         End
         Begin VB.TextBox txtNomPor 
            Height          =   285
            Left            =   2760
            TabIndex        =   1
            Top             =   240
            Width           =   3735
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcIdiCod 
            Height          =   285
            Left            =   2745
            TabIndex        =   6
            Top             =   3705
            Width           =   3750
            DataFieldList   =   "Column 0"
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3545
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Caption=   "C�digo"
            Columns(2).Name =   "COD"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   6615
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblIdioma 
            Caption         =   "Idioma"
            Height          =   255
            Left            =   240
            TabIndex        =   56
            Top             =   3720
            Width           =   2295
         End
         Begin VB.Line Line1 
            X1              =   120
            X2              =   8755
            Y1              =   840
            Y2              =   840
         End
         Begin VB.Label Label8 
            Caption         =   "Email"
            Height          =   255
            Left            =   240
            TabIndex        =   52
            Top             =   3120
            Width           =   2295
         End
         Begin VB.Label Label7 
            Caption         =   "Administrador"
            Height          =   255
            Left            =   240
            TabIndex        =   51
            Top             =   2490
            Width           =   2295
         End
         Begin VB.Label Label6 
            Caption         =   "Moneda central"
            Height          =   255
            Left            =   240
            TabIndex        =   50
            Top             =   1860
            Width           =   2295
         End
         Begin VB.Label Label4 
            Caption         =   "URL"
            Height          =   255
            Left            =   240
            TabIndex        =   49
            Top             =   1230
            Width           =   2295
         End
         Begin VB.Label Label3 
            Caption         =   "Nombre del Portal"
            Height          =   255
            Left            =   240
            TabIndex        =   48
            Top             =   240
            Width           =   2415
         End
      End
      Begin VB.Frame fraNotif 
         BorderStyle     =   0  'None
         Height          =   5220
         Left            =   -74850
         TabIndex        =   46
         Top             =   510
         Width           =   8940
         Begin VB.Frame fraRutaPlantillas 
            Caption         =   "Ruta para las plantillas de emails"
            Height          =   2800
            Left            =   0
            TabIndex        =   57
            Top             =   2430
            Width           =   8895
            Begin VB.TextBox txtRuta 
               Height          =   315
               Left            =   105
               TabIndex        =   19
               Top             =   300
               Width           =   8280
            End
            Begin VB.CommandButton cmdDrive 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   8445
               Picture         =   "frmParametros.frx":02B1
               Style           =   1  'Graphical
               TabIndex        =   20
               Top             =   315
               Width           =   315
            End
            Begin VB.DirListBox dirPlantillas 
               Height          =   1890
               Left            =   105
               TabIndex        =   21
               Top             =   705
               Width           =   8655
            End
         End
         Begin VB.CheckBox chkOp1 
            Caption         =   "Sin confirmaci�n"
            Height          =   255
            Index           =   1
            Left            =   615
            TabIndex        =   12
            Top             =   690
            Width           =   2250
         End
         Begin VB.CheckBox chkOp2 
            Caption         =   "Notificaci�n de desautorizaci�n"
            Height          =   255
            Index           =   0
            Left            =   3480
            TabIndex        =   13
            Top             =   435
            Width           =   3510
         End
         Begin VB.CheckBox chkOp2 
            Caption         =   "Sin confirmaci�n"
            Height          =   255
            Index           =   1
            Left            =   3855
            TabIndex        =   14
            Top             =   690
            Width           =   2250
         End
         Begin VB.CheckBox chkOp3 
            Caption         =   "Notificaci�n de confirmaci�n/rechazo de cambios de actividades"
            Height          =   255
            Index           =   0
            Left            =   255
            TabIndex        =   15
            Top             =   1020
            Width           =   6435
         End
         Begin VB.CheckBox chkOp3 
            Caption         =   "Sin confirmaci�n"
            Height          =   255
            Index           =   1
            Left            =   615
            TabIndex        =   16
            Top             =   1365
            Width           =   2250
         End
         Begin VB.CheckBox chkOp4 
            Caption         =   "Notificaci�n de confirmaci�n/rechazo de cambios en el registro de compa��as"
            Height          =   255
            Index           =   0
            Left            =   255
            TabIndex        =   17
            Top             =   1695
            Width           =   6435
         End
         Begin VB.CheckBox chkOp4 
            Caption         =   "Sin confirmaci�n"
            Height          =   255
            Index           =   1
            Left            =   615
            TabIndex        =   18
            Top             =   2010
            Width           =   2250
         End
         Begin VB.CheckBox chkOp1 
            Caption         =   "Notificaci�n de autorizaci�n"
            Height          =   255
            Index           =   0
            Left            =   255
            TabIndex        =   11
            Top             =   435
            Width           =   2970
         End
         Begin VB.Line Line4 
            X1              =   0
            X2              =   8755
            Y1              =   360
            Y2              =   360
         End
         Begin VB.Label lblemailsadm 
            Caption         =   "Activar el envio de emails para los siguientes casos:"
            Height          =   255
            Left            =   0
            TabIndex        =   55
            Top             =   0
            Width           =   3855
         End
      End
      Begin VB.Frame fraNotif_ 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4215
         Left            =   -74880
         TabIndex        =   36
         Top             =   960
         Width           =   6495
         Begin VB.CheckBox chkOpcion1 
            Caption         =   "Notificaci�n de autorizaci�n"
            Height          =   375
            Index           =   0
            Left            =   480
            TabIndex        =   44
            Top             =   240
            Width           =   2415
         End
         Begin VB.CheckBox chkOpcion4 
            Caption         =   "Sin confirmaci�n"
            Height          =   255
            Index           =   1
            Left            =   840
            TabIndex        =   43
            Top             =   3480
            Width           =   1575
         End
         Begin VB.CheckBox chkOpcion4 
            Caption         =   "Notificaci�n de confirmaci�n/rechazo de cambios en el registro de compa��as"
            Height          =   255
            Index           =   0
            Left            =   480
            TabIndex        =   42
            Top             =   3120
            Width           =   5775
         End
         Begin VB.CheckBox chkOpcion3 
            Caption         =   "Sin confirmaci�n"
            Height          =   255
            Index           =   1
            Left            =   840
            TabIndex        =   41
            Top             =   2520
            Width           =   1575
         End
         Begin VB.CheckBox chkOpcion3 
            Caption         =   "Notificaci�n de confirmaci�n/rechazo de cambios de actividades"
            Height          =   255
            Index           =   0
            Left            =   480
            TabIndex        =   40
            Top             =   2160
            Width           =   5055
         End
         Begin VB.CheckBox chkOpcion2 
            Caption         =   "Sin confirmaci�n"
            Height          =   255
            Index           =   1
            Left            =   840
            TabIndex        =   39
            Top             =   1560
            Width           =   1575
         End
         Begin VB.CheckBox chkOpcion2 
            Caption         =   "Notificaci�n de desautorizaci�n"
            Height          =   255
            Index           =   0
            Left            =   480
            TabIndex        =   38
            Top             =   1200
            Width           =   2655
         End
         Begin VB.CheckBox chkOpcion1 
            Caption         =   "Sin confirmaci�n"
            Height          =   375
            Index           =   1
            Left            =   840
            TabIndex        =   37
            Top             =   600
            Width           =   1575
         End
      End
      Begin VB.PictureBox picGestion 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   4855
         Left            =   -74985
         ScaleHeight     =   4860
         ScaleWidth      =   9150
         TabIndex        =   31
         Top             =   420
         Width           =   9150
         Begin VB.TextBox txtMaxActividades 
            Height          =   285
            Left            =   4350
            TabIndex        =   61
            Top             =   4170
            Width           =   855
         End
         Begin VB.TextBox txtTamanyoAdjunCia 
            Height          =   285
            Left            =   250
            TabIndex        =   9
            Top             =   2680
            Width           =   2500
         End
         Begin VB.CheckBox chkPremium 
            Caption         =   "Este portal soporta proveedores premium"
            Height          =   255
            Left            =   250
            TabIndex        =   7
            Top             =   300
            Width           =   5295
         End
         Begin VB.TextBox txtTamanyoAdjun 
            Height          =   285
            Left            =   250
            TabIndex        =   8
            Top             =   1600
            Width           =   2500
         End
         Begin MSComctlLib.Slider Slider1 
            Height          =   435
            Left            =   300
            TabIndex        =   10
            Top             =   4180
            Width           =   2415
            _ExtentX        =   4260
            _ExtentY        =   767
            _Version        =   393216
            LargeChange     =   1
            Min             =   1
            Max             =   5
            SelStart        =   1
            Value           =   1
         End
         Begin VB.Label lblMaxActividades 
            Caption         =   "D_(En caso de no querer limitar, introducir 0)"
            Height          =   255
            Index           =   2
            Left            =   4560
            TabIndex        =   63
            Top             =   3840
            Width           =   3330
         End
         Begin VB.Label lblMaxActividades 
            Caption         =   "D_Actividades"
            Height          =   255
            Index           =   1
            Left            =   5250
            TabIndex        =   62
            Top             =   4200
            Width           =   1575
         End
         Begin VB.Label lblMaxActividades 
            Caption         =   "D_N�mero m�ximo de actividades permitido"
            Height          =   255
            Index           =   0
            Left            =   4380
            TabIndex        =   60
            Top             =   3600
            Width           =   3330
         End
         Begin VB.Label lblBytes2 
            Caption         =   "KBytes"
            Height          =   255
            Left            =   3300
            TabIndex        =   59
            Top             =   2680
            Width           =   1575
         End
         Begin VB.Label lblTamanyoCias 
            Caption         =   "DTama�o m�ximo permitido por defecto para la documentaci�n adjunta de compras"
            Height          =   255
            Left            =   255
            TabIndex        =   58
            Top             =   2280
            Width           =   8220
         End
         Begin VB.Label lblBytes 
            Caption         =   "KBytes"
            Height          =   255
            Left            =   3300
            TabIndex        =   35
            Top             =   1600
            Width           =   1575
         End
         Begin VB.Label lblTamanyo 
            Caption         =   "Tama�o m�ximo permitido para los datos adjuntos en el env�o de ofertas"
            Height          =   255
            Left            =   250
            TabIndex        =   34
            Top             =   1200
            Width           =   6300
         End
         Begin VB.Label lblNivel 
            Caption         =   "Nivel m�nimo del registro de actividades"
            Height          =   435
            Left            =   255
            TabIndex        =   33
            Top             =   3600
            Width           =   3330
         End
         Begin VB.Line Line2 
            X1              =   135
            X2              =   8755
            Y1              =   840
            Y2              =   840
         End
         Begin VB.Line Line3 
            X1              =   135
            X2              =   8755
            Y1              =   3280
            Y2              =   3280
         End
         Begin VB.Label lblActiv 
            Height          =   375
            Left            =   2900
            TabIndex        =   32
            Top             =   4180
            Width           =   500
         End
      End
      Begin VB.Label lblPrimera 
         Caption         =   "Activar el envio de emails para los siguientes casos:"
         Height          =   255
         Left            =   -74640
         TabIndex        =   45
         Top             =   600
         Width           =   3855
      End
      Begin VB.Line linSep_ 
         BorderWidth     =   2
         X1              =   -74880
         X2              =   -68880
         Y1              =   960
         Y2              =   960
      End
   End
   Begin VB.PictureBox picNavigate 
      BorderStyle     =   0  'None
      Height          =   465
      Left            =   120
      ScaleHeight     =   465
      ScaleWidth      =   9210
      TabIndex        =   0
      Top             =   6870
      Width           =   9210
      Begin VB.CommandButton cmdEdicion 
         Caption         =   "&Edici�n"
         Height          =   345
         Left            =   8205
         TabIndex        =   26
         Top             =   120
         Width           =   1005
      End
   End
   Begin VB.PictureBox picEdit 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   465
      Left            =   120
      ScaleHeight     =   465
      ScaleWidth      =   9225
      TabIndex        =   27
      TabStop         =   0   'False
      Top             =   6870
      Width           =   9225
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   345
         Left            =   3645
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Cancel          =   -1  'True
         Caption         =   "&Cancelar"
         Height          =   345
         Left            =   4785
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   0
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmParametros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Moneda central
Private m_sDenMonCentral As String
Private m_iNivel As Byte

Private m_bCargarComboDesde As Boolean
Public m_bRespetarCombo As Boolean
Private m_CargarComboDesde As Boolean

Private bRespetarControl As Boolean
Private bChangeTxt As Boolean

Private sIdiTamMax As String
Private sIdiIdioma  As String


Private Sub cmdAceptar_Click()
Dim udtParametrosGenerales As ParametrosGenerales
Dim oGestorParametros As cGestorParametros
Dim teserror As CTESError
        
    If Not VerificarParametros Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    PrepararParametros udtParametrosGenerales
        
    Set oGestorParametros = g_oRaiz.Generar_CGestorParametros
    Set teserror = oGestorParametros.GuardarParametrosGenerales(udtParametrosGenerales, g_sInstancia, m_iNivel, g_sServidor, g_sBaseDeDatos)
    If teserror.NumError = 1 Then
        Screen.MousePointer = vbNormal
        basMensajes.ImposibleCambiarNivel
    End If
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basMensajes.ImposibleGuardarParametros
        Exit Sub
    End If
    
    g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Param_Modif, g_sLitRegAccion_SPA(50), g_sLitRegAccion_ENG(50), g_sLitRegAccion_FRA(50), g_sLitRegAccion_GER(50)

    g_udtParametrosGenerales = oGestorParametros.DevolverParametrosGenerales()
    g_sPlantillasPath = g_udtParametrosGenerales.g_sMailPath
    g_sRptPath = g_udtParametrosGenerales.g_sRptPath
    
    SaveStringSetting "FULLSTEP PS", g_sInstancia, "Opciones", "Idioma", g_udtParametrosGenerales.g_sIdioma
    
    MostrarParametros
    
    ModoConsulta
        
    Screen.MousePointer = vbNormal
    

    basMensajes.ConfiguracionParametrosActualizada

End Sub
''' <summary>
''' Comprueba si los parametros estan bien o no
''' </summary>
''' <returns>si estan bien o no</returns>
''' <remarks>Llamada desde: cmdAceptar_Click ; Tiempo m�ximo: 0,2</remarks>
Private Function VerificarParametros() As Boolean

    VerificarParametros = False
    
    If Not IsNumeric(txtTamanyoAdjun.Text) Then
        NoValido sIdiTamMax
        Exit Function
    ElseIf txtTamanyoAdjun.Text < 0 Then
        NoValido sIdiTamMax
        Exit Function
    End If
    
    If Not IsNumeric(txtTamanyoAdjunCia.Text) Then
        NoValido lblTamanyoCias.Caption
        Exit Function
    ElseIf txtTamanyoAdjunCia.Text < 0 Then
        NoValido lblTamanyoCias.Caption
        Exit Function
    End If
    If Not IsNumeric(txtMaxActividades.Text) Then
        NoValido lblMaxActividades(0).Caption
        Exit Function
    ElseIf txtTamanyoAdjunCia.Text < 0 Then
        NoValido lblMaxActividades(0).Caption
        Exit Function
    End If
    
    If txtSMTP(0).Text = "" Or txtSMTP(1).Text = "" Then
        basMensajes.FaltanDatosServidorCorreo
        Exit Function
    End If
    
    If optAutenticacion(0).Value = True Then 'basica
        If txtSMTP(3).Text = "" Then
            basMensajes.FaltanDatosServidorCorreo
            Exit Function
        End If
    ElseIf optAutenticacion(1).Value = True Then 'Windows
        If txtSMTP(2).Text = "" Or txtSMTP(3).Text = "" Then
            basMensajes.FaltanDatosServidorCorreo
            Exit Function
        End If
    Else 'Anonima
        'Solo controlar Cuenta correo, ed, txtSMTP(4)
    End If

    If txtSMTP(4).Text = "" Then
        basMensajes.FaltanDatosServidorCorreo
        Exit Function
    Else
        If InStr(1, txtSMTP(4), "@") = 0 Then
            basMensajes.DireccionCorreoNoValida
            Exit Function
        End If
    End If
    
    If txtCC.Text <> "" Then
        If InStr(1, txtCC.Text, "@") = 0 Then
            basMensajes.DireccionCCNoValida
            Exit Function
        End If
    End If
    If txtCCO.Text <> "" Then
        If InStr(1, txtCCO.Text, "@") = 0 Then
            basMensajes.DireccionCCoNoValida
            Exit Function
        End If
    End If
    If txtRespuesta.Text <> "" Then
        If InStr(1, txtRespuesta.Text, "@") = 0 Then
            basMensajes.DireccionRespuestaNoValida
            Exit Function
        End If
    End If
    
    If txtRuta = "" Then
        basMensajes.RutaPlantillasNoValida
        Exit Function
    End If
    If txtRutaRPT = "" Then
        basMensajes.RutaDeRPTNoValida
        Exit Function
    End If
    VerificarParametros = True
    
End Function


Private Sub PrepararParametros(udtParamGenerales As ParametrosGenerales)
'***************************************************************
'*** Descripci�n: Asigna a una estructura de tipo            ***
'***              ParametrosGenerales los valores            ***
'***              correspondientes al estado de los          ***
'***              par�metros del formulario.                 ***
'***                                                         ***
'*** Par�metros:          -----------                        ***
'***                                                         ***
'*** Valor que devuelve:  Una estructura de tipo             ***
'***                      ParametrosGenerales                ***
'***************************************************************
    With udtParamGenerales
        .g_iNotifAut = chkOp1(0).Value
        .g_iConfirmAut = chkOp1(1).Value
        .g_iNotifDesaut = chkOp2(0).Value
        .g_iConfirmDesaut = chkOp2(1).Value
        .g_iNotifCambAct = chkOp3(0).Value
        .g_iConfirmCambAct = chkOp3(1).Value
        .g_iNotifRegCias = chkOp4(0).Value
        .g_iConfirmRegCias = chkOp4(1).Value
        .g_sMailPath = Me.txtRuta.Text
        .g_sRptPath = Me.txtRutaRPT.Text
        .g_sIdioma = Me.sdbcIdiCod.Columns("COD").Value
    End With
    
    If chkPremium.Value = vbChecked Then
        udtParamGenerales.g_bPremium = True
    Else
        udtParamGenerales.g_bPremium = False
    End If
    

    
    With udtParamGenerales
         
    .g_iNotifSolicReg = chkMail1.Value
    .g_iNotifSolicAct = chkMail2.Value
    .g_iNotifSolicComp = chkMail3.Value
    .g_iTipoEmail = chkMail4.Value
               
    .g_sNomPortal = txtNomPor.Text
    .g_sUrl = txtURL.Text
    .g_sAdm = txtAdm.Text
    .g_sEmail = txtEmail.Text
        
    .g_dblMaxAdjun = CDbl(txtTamanyoAdjun.Text) * 1024
    .g_dblMaxAdjunCias = CDbl(txtTamanyoAdjunCia.Text) * 1024
    .g_iMaxActividades = CDbl(Val(txtMaxActividades.Text))
    .g_bPremium = chkPremium.Value
    .g_bUnaCompradora = g_udtParametrosGenerales.g_bUnaCompradora
    End With
        
    m_iNivel = g_udtParametrosGenerales.g_iNivelMinAct
    udtParamGenerales.g_iNivelMinAct = Slider1.Value
    
    
    udtParamGenerales.giSMTPAutent = 0
    udtParamGenerales.gbSMTPBasicaGS = False
    udtParamGenerales.gsSMTPUser = ""
    udtParamGenerales.gsSMTPPwd = ""
    udtParamGenerales.gbSMTPUsarCuenta = False
    udtParamGenerales.gsSMTPCuentaMail = ""
    udtParamGenerales.gsSMTPDominio = ""
        
        
    udtParamGenerales.gsSMTPServer = Trim(txtSMTP(0).Text)
    udtParamGenerales.giSMTPPort = Trim(txtSMTP(1).Text)
    udtParamGenerales.gbSMTPSSL = chkCorreo(2).Value
    If optAutenticacion(2).Value = True Then 'anonima
        udtParamGenerales.giSMTPAutent = 0
    Else
        If optAutenticacion(0).Value = True Then 'basica
            udtParamGenerales.giSMTPAutent = 1
            udtParamGenerales.gbSMTPBasicaGS = False
            udtParamGenerales.gsSMTPPwd = Trim(txtSMTP(3).Text)
            udtParamGenerales.gsSMTPUser = Trim(txtSMTP(4).Text)
        Else
            udtParamGenerales.giSMTPAutent = 2
            udtParamGenerales.gsSMTPUser = Trim(txtSMTP(2).Text)
            udtParamGenerales.gsSMTPPwd = Trim(txtSMTP(3).Text)
            udtParamGenerales.gsSMTPDominio = Trim(txtSMTP(5).Text)
        End If
    End If
    udtParamGenerales.gbSMTPUsarCuenta = True
    udtParamGenerales.gsSMTPCuentaMail = Trim(txtSMTP(4).Text)

    udtParamGenerales.gsSMTPCC = Trim(txtCC.Text)
    udtParamGenerales.gsSMTPCCO = Trim(txtCCO.Text)
    udtParamGenerales.gsSMTPRespuestaMail = Trim(txtRespuesta.Text)
    
    If chkMostrarMail.Value = vbChecked Then
        udtParamGenerales.gbMostrarMail = True
    Else
        udtParamGenerales.gbMostrarMail = False
    End If
    If chkAcuseRecibo.Value = vbChecked Then
        udtParamGenerales.gbAcuseRecibo = True
    Else
        udtParamGenerales.gbAcuseRecibo = False
    End If
    
End Sub

Private Sub cmdCancelar_Click()
    MostrarParametros
    ModoConsulta
End Sub

Private Sub cmdDrive_Click()
Dim sBuffer As String
Dim sTit As String
Dim sInicio As String
 
On Error GoTo error
 
sTit = Me.fraRutaPlantillas.Caption & ":"
sInicio = txtRuta.Text ' Si le quieres pasar la ruta del textbox para que se coloque ah� sino se le pasa el directorio actual
If sInicio = "" Then sInicio = CurDir$
sBuffer = BrowseForFolder(Me.hWnd, sTit, sInicio)
 
If sBuffer <> "" Then
    bRespetarControl = True
    dirPlantillas.Path = sBuffer
    txtRuta.Text = sBuffer
    bChangeTxt = False
    bRespetarControl = False
End If
Exit Sub
 
error:
    TratarErrorBrowse Err.Number, Err.Description
    
    txtRuta.Text = ""
    bChangeTxt = False
    bRespetarControl = False
    

End Sub

Private Sub cmdEdicion_Click()
    ModoEdicion
End Sub

Private Sub cmdRutaRpt_Click()
Dim sBuffer As String
Dim sTit As String
Dim sInicio As String
 
On Error GoTo error
 
sTit = fraRutaRpt.Caption & ":"
sInicio = txtRutaRPT.Text ' Si le quieres pasar la ruta del textbox para que se coloque ah� sino se le pasa el directorio actual
If sInicio = "" Then sInicio = CurDir$
sBuffer = BrowseForFolder(Me.hWnd, sTit, sInicio)
 
If sBuffer <> "" Then
    bRespetarControl = True
    dirRutaRpt.Path = sBuffer
    txtRutaRPT.Text = sBuffer
    bChangeTxt = False
    bRespetarControl = False
End If
Exit Sub
 
error:
    TratarErrorBrowse Err.Number, Err.Description
    
    txtRutaRPT.Text = ""
    bChangeTxt = False
    bRespetarControl = False
    


End Sub

Private Sub dirRutaRpt_Change()
    If Not bRespetarControl Then
        bRespetarControl = True
        Me.txtRutaRPT.Text = dirRutaRpt.Path
        bRespetarControl = False
    End If

End Sub

Private Sub Form_Load()
Dim adores As Ador.Recordset
m_CargarComboDesde = False
    Me.Top = (frmMDI.Height - Me.Height) / 2
    Me.Left = (frmMDI.Width - Me.Width) / 2
    Me.Width = 9870
    sstabParam.Width = 9600
    Me.picNavigate.Width = sstabParam.Width
    Me.cmdEdicion.Left = Me.picNavigate.Width - Me.cmdEdicion.Width
    
    
    CargarRecursos
    Set adores = g_oRaiz.CodMonedaCentral
    m_sDenMonCentral = adores(1).Value
    adores.Close
    Set adores = Nothing
    sstabParam.Tab = 0
    MostrarParametros
    fraNotif.Enabled = False
    fraMail.Enabled = False
    fraGen1.Enabled = False
    
    ModoConsulta
    
    
    If basParametros.g_udtParametrosGenerales.g_bUnaCompradora Then
        chkMail3.Visible = False
        chkOp4(0).Visible = False
        chkOp4(1).Visible = False
    Else
        chkMail3.Visible = True
        chkOp4(0).Visible = True
        chkOp4(1).Visible = True
    End If
    
    g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Param_Acceso, g_sLitRegAccion_SPA(35), g_sLitRegAccion_ENG(35), g_sLitRegAccion_FRA(35), g_sLitRegAccion_GER(35)
End Sub


Private Sub ModoEdicion()
'***************************************************************
'*** Descripci�n: Desbloquea los controles para que puedan   ***
'***              ser modificados los valores de los         ***
'***              par�metros. Se pasa al modo edici�n.       ***
'***                                                         ***
'*** Par�metros:          -----------                        ***
'***                                                         ***
'*** Valor que devuelve:  -----------                        ***
'***************************************************************
    'picGenerales.Enabled = True
    picGestion.Enabled = True

    picEdit.Visible = True
    picNavigate.Visible = False
    
    fraNotif.Enabled = True
    
    fraMail.Enabled = True
    fraGen1.Enabled = True
    fraOpciones(2).Enabled = True
    picRutaRpt.Enabled = True
    
    
    
    
End Sub

Private Sub ModoConsulta()
'***************************************************************
'*** Descripci�n: Bloquea los controles para que no puedan   ***
'***              ser modificados los valores de los         ***
'***              par�metros. Se pasa al modo consulta.      ***
'***                                                         ***
'*** Par�metros:          -----------                        ***
'***                                                         ***
'*** Valor que devuelve:  -----------                        ***
'***************************************************************
    'picGenerales.Enabled = False
    picGestion.Enabled = False

    picEdit.Visible = False
    picNavigate.Visible = True
    
    fraNotif.Enabled = False
    fraMail.Enabled = False
    fraGen1.Enabled = False
    fraOpciones(2).Enabled = False
    picRutaRpt.Enabled = False
    
    
End Sub

Private Sub MostrarParametros()
'***************************************************************
'*** Descripci�n: Restaura en el formulario los valores      ***
'***              de los par�metros generales que est�n en   ***
'***              g_udtParametrosGenerales                   ***
'***                                                         ***
'*** Par�metros:          -----------                        ***
'***                                                         ***
'*** Valor que devuelve:  -----------                        ***
'***************************************************************
    On Error GoTo error
    
    'Gesti�n
    If g_udtParametrosGenerales.g_bPremium Then
        chkPremium.Value = vbChecked
    Else
        chkPremium.Value = vbUnchecked
    End If
    txtTamanyoAdjun.Text = Format(g_udtParametrosGenerales.g_dblMaxAdjun / 1024, "#,##0")
    Slider1.Value = g_udtParametrosGenerales.g_iNivelMinAct
    lblActiv = Slider1.Value
    
    txtTamanyoAdjunCia.Text = Format(g_udtParametrosGenerales.g_dblMaxAdjunCias / 1024, "#,##0")
    txtMaxActividades.Text = g_udtParametrosGenerales.g_iMaxActividades
    Dim Ador As Ador.Recordset
    Set Ador = g_oRaiz.DevolverIdiomasDelPortalDesde(basParametros.g_iCargaMaximaCombos, g_udtParametrosGenerales.g_sIdioma, , True)
    Me.sdbcIdiCod.AddItem Ador("ID").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("COD").Value
    Me.sdbcIdiCod.Value = Ador("DEN").Value
    Ador.Close
    Set Ador = Nothing
    
    
    'Emails administrativos del portal shell
    'vbUnchecked=0 ; vbChecked=1
    With g_udtParametrosGenerales
        chkOp1(0).Value = .g_iNotifAut
        chkOp1(1).Value = .g_iConfirmAut
        chkOp2(0).Value = .g_iNotifDesaut
        chkOp2(1).Value = .g_iConfirmDesaut
        chkOp3(0).Value = .g_iNotifCambAct
        chkOp3(1).Value = .g_iConfirmCambAct
        chkOp4(0).Value = .g_iNotifRegCias
        chkOp4(1).Value = .g_iConfirmRegCias
        Me.txtRuta = .g_sMailPath
        Me.txtRutaRPT.Text = .g_sRptPath
        Me.dirPlantillas.Path = .g_sMailPath
        Me.dirRutaRpt.Path = .g_sRptPath
        
    End With
    
        
    'Los valores del portal shell
    With g_udtParametrosGenerales
        
        chkMail1.Value = .g_iNotifSolicReg
        chkMail2.Value = .g_iNotifSolicAct
        chkMail3.Value = .g_iNotifSolicComp
        chkMail4.Value = .g_iTipoEmail
        
    'Los valores generales
        
        txtNomPor.Text = .g_sNomPortal '
        txtURL.Text = .g_sUrl
        Dim oMon As CMoneda
        
        Set oMon = g_oRaiz.Generar_CMoneda
        
        oMon.ID = .g_iMonedaCentral
        
        oMon.cargarMoneda
        
        txtMonCen.Text = oMon.Cod & " - " & oMon.Denominaciones.Item(g_udtParametrosGenerales.g_sIdioma).Den
        
        Set oMon = Nothing
        
        txtAdm.Text = .g_sAdm
        txtEmail.Text = .g_sEmail
                    
        
    'Los valores de gestion
        
        If .g_bPremium = True Then
            chkPremium.Value = 1
            Else
                chkPremium.Value = 0
        End If
        
    End With
        
        
    txtSMTP(0).Text = ""
    txtSMTP(1).Text = ""
    txtSMTP(2).Text = ""
    txtSMTP(3).Text = ""
    txtSMTP(4).Text = ""
    txtSMTP(5).Text = ""
    
    chkCorreo(2).Value = vbUnchecked
        
    txtSMTP(0).Text = g_udtParametrosGenerales.gsSMTPServer
    txtSMTP(1).Text = g_udtParametrosGenerales.giSMTPPort
    If g_udtParametrosGenerales.gbSMTPSSL Then
        chkCorreo(2).Value = vbChecked
    Else
        chkCorreo(2).Value = vbUnchecked
    End If
    Select Case g_udtParametrosGenerales.giSMTPAutent
    Case 0
        Me.optAutenticacion(2).Value = True
    Case 1
        Me.optAutenticacion(0).Value = True
        
        txtSMTP(3).Text = g_udtParametrosGenerales.gsSMTPPwd
    Case 2
        Me.optAutenticacion(1).Value = True
                
        txtSMTP(2).Text = g_udtParametrosGenerales.gsSMTPUser
        txtSMTP(3).Text = g_udtParametrosGenerales.gsSMTPPwd
        txtSMTP(5).Text = g_udtParametrosGenerales.gsSMTPDominio
    End Select
    
    txtSMTP(4).Text = g_udtParametrosGenerales.gsSMTPCuentaMail
    
    txtCC.Text = g_udtParametrosGenerales.gsSMTPCC
    txtCCO.Text = g_udtParametrosGenerales.gsSMTPCCO
    txtRespuesta.Text = g_udtParametrosGenerales.gsSMTPRespuestaMail
    
    If g_udtParametrosGenerales.gbMostrarMail = True Then
        chkMostrarMail.Value = vbChecked
    Else
        chkMostrarMail.Value = vbUnchecked
    End If
    If g_udtParametrosGenerales.gbAcuseRecibo = True Then
        chkAcuseRecibo.Value = vbChecked
    Else
        chkAcuseRecibo.Value = vbUnchecked
    End If
    
    If g_udtParametrosGenerales.gbSincronizacion = True Then
        Slider1.Max = 4
    Else
        Slider1.Max = 5
    End If
    
    Exit Sub
    
    
error:
    
    If Err.Number = 76 Or Err.Number = 68 Then
        Resume Next
    End If
    
End Sub






Private Sub Slider1_Change()
    lblActiv = Slider1.Value
End Sub

Private Sub chkOp1_Click(Index As Integer)
    Select Case Index
        Case 0:
            If (chkOp1(0).Value = vbUnchecked) Then
                chkOp1(1).Value = vbUnchecked
            End If
        Case 1:
            If (chkOp1(1).Value = vbChecked) Then
                chkOp1(0).Value = vbChecked
            End If
    End Select
End Sub

Private Sub chkOp2_Click(Index As Integer)
    Select Case Index
        Case 0:
            If (chkOp2(0).Value = vbUnchecked) Then
                chkOp2(1).Value = vbUnchecked
            End If
        Case 1:
            If (chkOp2(1).Value = vbChecked) Then
                chkOp2(0).Value = vbChecked
            End If
    End Select
End Sub

Private Sub chkOp3_Click(Index As Integer)
    Select Case Index
        Case 0:
            If (chkOp3(0).Value = vbUnchecked) Then
                chkOp3(1).Value = vbUnchecked
            End If
        Case 1:
            If (chkOp3(1).Value = vbChecked) Then
                chkOp3(0).Value = vbChecked
            End If
    End Select
End Sub

Private Sub chkOp4_Click(Index As Integer)
    Select Case Index
        Case 0:
            If (chkOp4(0).Value = vbUnchecked) Then
                chkOp4(1).Value = vbUnchecked
            End If
        Case 1:
            If (chkOp4(1).Value = vbChecked) Then
                chkOp4(0).Value = vbChecked
            End If
    End Select
End Sub

''' <summary>
''' carga los textos de pantalla
''' </summary>
''' <remarks>Llamada desde: Form_load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(frm_parametros, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.chkMail1.Caption = Ador(0).Value
        Ador.MoveNext
        Me.chkMail2.Caption = Ador(0).Value
        Ador.MoveNext
        Me.chkMail3.Caption = Ador(0).Value
        Ador.MoveNext
        Me.chkMail4.Caption = Ador(0).Value
        Ador.MoveNext
        Me.chkOp1(0).Caption = Ador(0).Value
        Me.chkOpcion1(0).Caption = Ador(0).Value
        Ador.MoveNext
        Me.chkOp1(1).Caption = Ador(0).Value
        Me.chkOp2(1).Caption = Ador(0).Value
        Me.chkOp3(1).Caption = Ador(0).Value
        Me.chkOp4(1).Caption = Ador(0).Value
        Me.chkOpcion1(1).Caption = Ador(0).Value
        Me.chkOpcion2(1).Caption = Ador(0).Value
        Me.chkOpcion3(1).Caption = Ador(0).Value
        Me.chkOpcion4(1).Caption = Ador(0).Value
        
        Ador.MoveNext
        Me.chkOp2(0).Caption = Ador(0).Value
        Me.chkOpcion2(0).Caption = Ador(0).Value
        Ador.MoveNext
        Me.chkOp3(0).Caption = Ador(0).Value
        Me.chkOpcion3(0).Caption = Ador(0).Value
        Ador.MoveNext
        Me.chkOp4(0).Caption = Ador(0).Value
        Me.chkOpcion4(0).Caption = Ador(0).Value
        Ador.MoveNext
        
        Me.chkPremium.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdEdicion.Caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Me.Label3.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label4.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label5.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label6.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label7.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label8.Caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        'lblBytes.Caption = Ador(0).Value
        'lblBytes2.Caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Me.lblemailsadm.Caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Me.lblNivel.Caption = Ador(0).Value
        Ador.MoveNext
        Me.lblPrimera.Caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Me.lblTamanyo.Caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Me.sstabParam.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        Me.sstabParam.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        Me.sstabParam.TabCaption(2) = Ador(0).Value
        Ador.MoveNext
        Me.sstabParam.TabCaption(3) = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        sIdiTamMax = Ador(0).Value
        Ador.MoveNext
        Me.lblIdioma.Caption = Ador(0).Value
        sIdiIdioma = Ador(0).Value
        Ador.MoveNext
        lblSMTP(0).Caption = Ador(0).Value
        Ador.MoveNext
        lblSMTP(1).Caption = Ador(0).Value
        Ador.MoveNext
        chkCorreo(2).Caption = Ador(0).Value
        Ador.MoveNext
        optAutenticacion(2).Caption = Ador(0).Value
        Ador.MoveNext
        optAutenticacion(0).Caption = Ador(0).Value
        Ador.MoveNext
        optAutenticacion(1).Caption = Ador(0).Value
        Ador.MoveNext
        lblSMTP(10).Caption = Ador(0).Value
        Ador.MoveNext
        lblSMTP(2).Caption = Ador(0).Value
        Ador.MoveNext
        lblSMTP(3).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sstabParam.TabCaption(4) = Ador(0).Value
        Ador.MoveNext
        Me.fraRutaPlantillas.Caption = Ador(0).Value
        Ador.MoveNext
        Me.sstabParam.TabCaption(5) = Ador(0).Value
        Ador.MoveNext
        Me.fraRutaRpt.Caption = Ador(0).Value
        Ador.MoveNext
        lblSMTP(4).Caption = Ador(0).Value
        Ador.MoveNext
        lblSMTP(5).Caption = Ador(0).Value
        Ador.MoveNext
        lblSMTP(6).Caption = Ador(0).Value
        Ador.MoveNext
        fraOpciones(5).Caption = Ador(0).Value
        Ador.MoveNext
        chkMostrarMail.Caption = Ador(0).Value
        Ador.MoveNext
        chkAcuseRecibo.Caption = Ador(0).Value
        Ador.MoveNext
        lblTamanyoCias.Caption = Ador(0).Value
        Ador.MoveNext
        lblSMTP(8).Caption = Ador(0).Value
        Ador.MoveNext
        lblSMTP(7).Caption = Ador(0).Value
        Ador.MoveNext
        lblSMTP(9).Caption = Ador(0).Value
        Ador.MoveNext
        lblMaxActividades(0).Caption = Ador(0).Value
        Ador.MoveNext
        lblMaxActividades(1).Caption = Ador(0).Value
        Ador.MoveNext
        lblMaxActividades(2).Caption = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub


Private Sub sdbcIdiCod_Change()
 
    If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
        m_CargarComboDesde = True
    End If
    
End Sub

Private Sub sdbcIdiCod_CloseUp()
    
    If sdbcIdiCod.Value = "..." Then
        sdbcIdiCod.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcIdiCod.Text = sdbcIdiCod.Columns(1).Text
    m_bRespetarCombo = False
    
    m_CargarComboDesde = False

End Sub

Private Sub sdbcIdiCod_DropDown()
Dim Ador As Ador.Recordset
    
   
    sdbcIdiCod.RemoveAll
    
    Set Ador = g_oRaiz.DevolverIdiomasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , , False)
    
    If Not Ador Is Nothing Then
        
        While Not Ador.EOF
        
            sdbcIdiCod.AddItem Ador("ID").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("COD").Value
            Ador.MoveNext
        
        Wend
        
        Ador.Close
        Set Ador = Nothing
        
    End If
    
        
End Sub

Private Sub sdbcIdiCod_InitColumnProps()
    
    sdbcIdiCod.DataFieldList = "Column 1"
    sdbcIdiCod.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcIdiCod_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcIdiCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcIdiCod.Rows - 1
            bm = sdbcIdiCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcIdiCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcIdiCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub


Private Sub sdbcIdiCod_Validate(Cancel As Boolean)
    If sdbcIdiCod.Text = "" Then
        Exit Sub
    Else
        Dim Ador As Ador.Recordset
        Set Ador = g_oRaiz.DevolverIdiomasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbcIdiCod.Text, True)
        If Ador.RecordCount = 0 Then
            NoValido (sIdiIdioma)
            sdbcIdiCod.Text = ""
            Cancel = True
        End If
        Ador.Close
        Set Ador = Nothing
    End If

End Sub

''' <summary>
''' Dependiendo de si se pulsa basica o windows o anonima, activa unos campos u otros
''' </summary>
''' <param name="Index">Cual de los radiobutton se ha pulsado</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub optAutenticacion_Click(Index As Integer)

    If Index = 0 Then
        If optAutenticacion(0).Value = True Then
            Me.txtSMTP(4).Visible = True
            Me.txtSMTP(3).Visible = True
            
            Me.lblSMTP(9).Visible = True
            Me.lblSMTP(3).Visible = True
            
            Me.txtSMTP(2).Visible = False
            Me.txtSMTP(5).Visible = False
            
            Me.lblSMTP(2).Visible = False
            Me.lblSMTP(7).Visible = False
                        
            Me.txtSMTP(3).Top = Me.txtSMTP(2).Top
            Me.lblSMTP(3).Top = Me.lblSMTP(2).Top
            
            fraOpciones(3).Height = 1065
            
        End If
    ElseIf Index = 1 Then
        If optAutenticacion(1).Value = True Then
            Me.txtSMTP(4).Visible = True
            Me.txtSMTP(2).Visible = True
            Me.txtSMTP(3).Visible = True
            Me.txtSMTP(5).Visible = True
            
            Me.lblSMTP(9).Visible = True
            Me.lblSMTP(2).Visible = True
            Me.lblSMTP(3).Visible = True
            Me.lblSMTP(7).Visible = True
            
            Me.txtSMTP(3).Top = 990
            Me.lblSMTP(3).Top = 1050
            
            fraOpciones(3).Height = 1800
        End If
    Else
        If optAutenticacion(2).Value = True Then
            Me.txtSMTP(4).Visible = True
            Me.lblSMTP(9).Visible = True
            
            Me.fraOpciones(3).Height = 735
            
            Me.txtSMTP(2).Visible = False
            Me.txtSMTP(3).Visible = False
            Me.txtSMTP(5).Visible = False
            
            Me.lblSMTP(2).Visible = False
            Me.lblSMTP(3).Visible = False
            Me.lblSMTP(7).Visible = False
        End If
    End If
    
    Me.fraOpciones(5).Top = Me.fraOpciones(3).Top + Me.fraOpciones(3).Height + 100
    
    Me.chkMostrarMail.Top = Me.fraOpciones(5).Top + Me.fraOpciones(5).Height + 100
    Me.chkAcuseRecibo.Top = Me.fraOpciones(5).Top + Me.fraOpciones(5).Height + 100
End Sub



 
Private Sub dirPlantillas_Change()
    If Not bRespetarControl Then
        bRespetarControl = True
        txtRuta.Text = dirPlantillas.Path
        bRespetarControl = False
    End If
    
End Sub
 
Private Sub txtRuta_Change()
    
    If Not bRespetarControl Then
        bChangeTxt = True
    End If
End Sub
 
Private Sub txtRuta_Validate(Cancel As Boolean)
 
On Error GoTo error
 
    If Replace(txtRuta.Text, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, "") <> dirPlantillas.Path Then
        bRespetarControl = True
        dirPlantillas.Path = Replace(txtRuta.Text, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, "")
        bChangeTxt = False
        bRespetarControl = False
    End If
    Exit Sub
 
error:
    TratarErrorBrowse Err.Number, Err.Description
    
    txtRuta.Text = ""
    bChangeTxt = False
    bRespetarControl = False
    
End Sub


 
Public Sub TratarErrorBrowse(ByVal Num As Long, ByVal Descr As String)

    Select Case Num
    Case 76
        'basMensajes.MensajeOKOnly 576 'Path not found
    Case 419
        'basMensajes.PermisoDenegadoRuta 'Permiso denegado
    Case 68
        'basMensajes.MensajeOKOnly 578 'Device unavailable
    Case Else
        'basMensajes.OtroError CStr(Num), Descr
    End Select
    
End Sub


