VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCoordinacionMonedasCentrales 
   Caption         =   "Coordinaci�n de monedas centrales"
   ClientHeight    =   2730
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5760
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCoordinacMonCentr.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   2730
   ScaleWidth      =   5760
   Begin VB.PictureBox picunacompradora 
      BackColor       =   &H00808000&
      Height          =   3855
      Left            =   0
      ScaleHeight     =   3795
      ScaleWidth      =   8955
      TabIndex        =   6
      Top             =   0
      Width           =   9015
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Default         =   -1  'True
         Height          =   375
         Left            =   2880
         TabIndex        =   12
         Top             =   2160
         Width           =   1335
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   375
         Left            =   1440
         TabIndex        =   11
         Top             =   2160
         Width           =   1215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMonCod 
         Height          =   285
         Left            =   2640
         TabIndex        =   10
         Top             =   1320
         Width           =   2895
         DataFieldList   =   "Column 0"
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3545
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "C�digo"
         Columns(2).Name =   "Cod"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   5106
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label Label2 
         BackColor       =   &H00808000&
         Caption         =   "Seleccione la moneda del portal a la que equivale"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   600
         TabIndex        =   9
         Top             =   960
         Width           =   3735
      End
      Begin VB.Label lblMonCenGS 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label2"
         Height          =   300
         Left            =   2640
         TabIndex        =   8
         Top             =   360
         Width           =   2415
      End
      Begin VB.Label Label1 
         BackColor       =   &H00808000&
         Caption         =   "Moneda central de GS"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   600
         TabIndex        =   7
         Top             =   360
         Width           =   2175
      End
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   5760
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   2175
      Width           =   5760
      Begin VB.CommandButton cmdListado 
         Caption         =   "Listado"
         Height          =   345
         Left            =   60
         TabIndex        =   5
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         Height          =   345
         Left            =   7950
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgMonedas 
      Height          =   3135
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   8895
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   7
      stylesets.count =   1
      stylesets(0).Name=   "Bloc"
      stylesets(0).BackColor=   16777151
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCoordinacMonCentr.frx":0CB2
      BeveColorScheme =   1
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   4392
      Columns(0).Caption=   "Compa��a"
      Columns(0).Name =   "CIA"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777136
      Columns(0).StyleSet=   "Bloc"
      Columns(1).Width=   1746
      Columns(1).Caption=   "C�d. FSGS"
      Columns(1).Name =   "MONFSGS"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).StyleSet=   "Bloc"
      Columns(2).Width=   3387
      Columns(2).Caption=   "Den. de FullStepGS"
      Columns(2).Name =   "DENFS"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).StyleSet=   "Bloc"
      Columns(3).Width=   1746
      Columns(3).Caption=   "C�d.Portal"
      Columns(3).Name =   "MONPORTAL"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3387
      Columns(4).Caption=   "Den. del portal"
      Columns(4).Name =   "DENPORTAL"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "ID"
      Columns(5).Name =   "ID"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "RELLENO"
      Columns(6).Name =   "RELLENO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   11
      Columns(6).FieldLen=   256
      _ExtentX        =   15690
      _ExtentY        =   5530
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddMonPortCod 
      Height          =   1545
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   4965
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2699
      Columns(0).Caption=   "C�digo en el portal"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5662
      Columns(1).Caption=   "Den.en el portal"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   8758
      _ExtentY        =   2725
      _StockProps     =   77
      ForeColor       =   0
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddMonPortDen 
      Height          =   1545
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   4965
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5662
      Columns(0).Caption=   "Den.en el portal"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2699
      Columns(1).Caption=   "C�digo en el portal"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   8758
      _ExtentY        =   2725
      _StockProps     =   77
      ForeColor       =   0
      DataFieldToDisplay=   "Column 0"
   End
End
Attribute VB_Name = "frmCoordinacionMonedasCentrales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

''' Control de errores
Private bHayErrores As Boolean
Private sCportal As String

Private sIdiEdicion As String
Private sIdiConsulta As String

Private Sub cmdAceptar_Click()
Dim oError As CTESError
Dim iEquiv As Integer

If basMensajes.PreguntaModEnlaceMonCentral = vbNo Then
    Exit Sub
End If

iEquiv = g_oRaiz.DevolverEquiv(sdbcMonCod.Columns("COD").Value)
If iEquiv = 0 Then
    basMensajes.MonSinEquivalencia
    Exit Sub
End If

Screen.MousePointer = vbHourglass

Set oError = g_oRaiz.EnlazarMonedaCentral(basParametros.g_udtParametrosGenerales.g_lIdCiaCompradora, sdbcMonCod.Columns("COD").Value, lblMonCenGS.Tag)

Screen.MousePointer = vbNormal

If oError.numerror <> 0 Then
    basErrores.TratarError oError
End If

Unload Me

    
  
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdListado_Click()
    frmLstMonCentr.WindowState = vbNormal
    frmLstMonCentr.SetFocus
End Sub

Private Sub cmdModoEdicion_Click()
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
 
    bHayErrores = False
    
    If sdbgMonedas.AllowUpdate Then
        'Pasar a modo consulta
        If sdbgMonedas.Row < sdbgMonedas.VisibleRows - 1 Then
            sdbgMonedas.Row = sdbgMonedas.Row + 1
            If sdbgMonedas.Row > 0 Then
                sdbgMonedas.Row = sdbgMonedas.Row - 1
            End If
        Else
            sdbgMonedas.Row = sdbgMonedas.Row - 1
        End If

        sdbgMonedas.Update
        
        DoEvents
        If Not bHayErrores Then
            sdbgMonedas.AllowUpdate = False
            cmdModoEdicion.Caption = sIdiEdicion
            cmdListado.Visible = True
        End If
        
    Else
        'Pasar a modo edici�n
            sdbgMonedas.AllowUpdate = True
            cmdModoEdicion.Caption = sIdiConsulta
            cmdListado.Visible = False
           
            ' sdbgMonedas.Row = sdbgMonedas.Row + 1
        
    End If
     
     bHayErrores = False
    
End Sub
Private Sub Form_Load()
    CargarRecursos
    'Dimensiona el formulario
    If basParametros.g_udtParametrosGenerales.g_bUnaCompradora Then
        Me.Height = 3120
        Me.Width = 6075
        Me.BorderStyle = 3
        picunacompradora.Visible = True
        CargarMonedaDeLaCiaCompradora
        
    Else
        Me.Height = 4290
        Me.Width = 9165
        
        Me.BorderStyle = 2
        
        picunacompradora.Visible = False
        
        sdbgMonedas.Columns("MONPORTAL").DropDownHwnd = sdbddMonPortCod.hWnd
        sdbddMonPortCod.AddItem ""
        sdbgMonedas.Columns("DENPORTAL").DropDownHwnd = sdbddMonPortDen.hWnd
        sdbddMonPortDen.AddItem ""
        sdbgMonedas.AllowUpdate = False
        cmdModoEdicion.Visible = True
        'Carga las compa�ias con sus respectivas monedas
        CargarMonedas
    End If

    
End Sub


Private Sub Form_Resize()
    Arrange
End Sub


Private Sub Arrange()
    ''' * Objetivo: Ajustar el tama�o y posicion de los controles
    ''' * Objetivo: al tama�o del formulario
    
    If Height >= 1155 Then sdbgMonedas.Height = Height - 1155
    If Width >= 270 Then sdbgMonedas.Width = Width - 270
    
    sdbgMonedas.Columns(0).Width = sdbgMonedas.Width * 28 / 100
    sdbgMonedas.Columns(1).Width = sdbgMonedas.Width * 11 / 100
    sdbgMonedas.Columns(2).Width = sdbgMonedas.Width * 22 / 100
    sdbgMonedas.Columns(3).Width = sdbgMonedas.Width * 11 / 100
    sdbgMonedas.Columns(4).Width = sdbgMonedas.Width * 22 / 100
    
    cmdModoEdicion.Left = sdbgMonedas.Left + sdbgMonedas.Width - cmdModoEdicion.Width
    
    picunacompradora.Width = Me.Width
    picunacompradora.Height = Me.Height
End Sub

Private Sub sdbcMonCod_DropDown()
Dim ador As ador.Recordset
    
   
    sdbcMonCod.RemoveAll
    Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , , False)
    
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbcMonCod.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value
            ador.MoveNext
        
        Wend
        
        ador.Close
        Set ador = Nothing
        
    End If
    

End Sub

Private Sub sdbcMonCod_InitColumnProps()
 sdbcMonCod.DataFieldList = "Column 1"
    sdbcMonCod.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcMonCod_PositionList(ByVal Text As String)
  Dim i As Long
   Dim bm As Variant

    On Error Resume Next
    
    sdbcMonCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcMonCod.Rows - 1
            bm = sdbcMonCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcMonCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcMonCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcMonCod_Validate(Cancel As Boolean)
  If sdbcMonCod.Text = "" Then
        Exit Sub
    Else
        Dim ador As ador.Recordset
        Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbcMonCod.Text, True)
        If ador.RecordCount = 0 Then
            NoValida ("Moneda")
            sdbcMonCod.Text = ""
            Cancel = True
        Else
            sdbcMonCod.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value
        End If
        ador.Close
        Set ador = Nothing
    End If
End Sub

Private Sub sdbddMonPortCod_CloseUp()
    If sdbgMonedas.Columns("MONPORTAL").Value <> "" Then
        sdbgMonedas.Columns("DENPORTAL").Value = sdbddMonPortCod.Columns("DEN").Value
    End If
    
End Sub

Private Sub sdbddMonPortCod_DropDown()
    Dim ador As ador.Recordset
    
    If sdbgMonedas.AllowUpdate = False Then
       sdbddMonPortCod.DroppedDown = False
    End If
    
    
    sdbddMonPortCod.RemoveAll
    
    Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, sdbgMonedas.ActiveCell.Value, , False, True)
    
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbddMonPortCod.AddItem ador("COD").Value & Chr(9) & ador("DEN").Value
            ador.MoveNext
        
        Wend
        
        ador.Close
        Set ador = Nothing
        
    End If
    
    'sCportal = sdbgMonedas.Columns("MONPORTAL").Value
    
End Sub

Private Sub sdbddMonPortCod_InitColumnProps()
    sdbddMonPortCod.DataFieldList = "Column 0"
    sdbddMonPortCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddMonPortCod_PositionList(ByVal Text As String)
Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddMonPortCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddMonPortCod.Rows - 1
            bm = sdbddMonPortCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddMonPortCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddMonPortCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddMonPortCod_ValidateList(Text As String, RtnPassed As Integer)
    Dim scod As String
    Dim ador As ador.Recordset
    
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True
        sdbgMonedas.Columns("DENPORTAL").Value = ""
    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddMonPortCod.Columns("COD").Value = Text Then
            RtnPassed = True
            sdbgMonedas.Columns("DENPORTAL").Value = sdbddMonPortCod.Columns("DEN").Value
            Exit Sub
        Else
            Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, sdbgMonedas.Columns("MONPORTAL").Value, , True)
        
            If ador Is Nothing Then
                RtnPassed = 0
            Else
                If ador.EOF Then
                    RtnPassed = 0
                Else
                    
                    sdbgMonedas.Columns("MONPORTAL").Value = ador("COD").Value
                    sdbgMonedas.Columns("DENPORTAL").Value = ador("DEN").Value
                        
                    RtnPassed = 1
                End If
            End If
            
            ador.Close
            Set ador = Nothing
            
        End If
        
    End If

End Sub

Private Sub sdbddMonPortDen_CloseUp()
    If sdbgMonedas.Columns("DENPORTAL").Value <> "" Then
        sdbgMonedas.Columns("MONPORTAL").Value = sdbddMonPortDen.Columns("COD").Value
    End If
End Sub

Private Sub sdbddMonPortDen_DropDown()
    Dim ador As ador.Recordset
    
    If sdbgMonedas.AllowUpdate = False Then
       sdbddMonPortDen.DroppedDown = False
    End If
    
    
    sdbddMonPortDen.RemoveAll
    
    Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbgMonedas.ActiveCell.Value, , False)
    
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbddMonPortDen.AddItem ador("DEN").Value & Chr(9) & ador("COD").Value
            ador.MoveNext
        
        Wend
        ador.Close
        Set ador = Nothing
        
    End If
    
End Sub

Private Sub sdbddMonPortDen_InitColumnProps()
    sdbddMonPortDen.DataFieldList = "Column 0"
    sdbddMonPortDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddMonPortDen_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddMonPortDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddMonPortDen.Rows - 1
            bm = sdbddMonPortCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddMonPortDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddMonPortDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddMonPortDen_ValidateList(Text As String, RtnPassed As Integer)
    Dim scod As String
    Dim ador As ador.Recordset
    
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True
        sdbgMonedas.Columns("MONPORTAL").Value = ""
    
    Else

        ''' Comprobar la existencia en la lista
        If sdbddMonPortDen.Columns("DEN").Value = Text Then
            RtnPassed = True
            sdbgMonedas.Columns("MONPORTAL").Value = sdbddMonPortDen.Columns("COD").Value
            Exit Sub
        Else
            Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbgMonedas.Columns("DENPORTAL").Value, True)
            If ador Is Nothing Then
                RtnPassed = 0
            Else
                If ador.EOF Then
                    RtnPassed = 0
                Else

                    sdbgMonedas.Columns("MONPORTAL").Value = ador("COD").Value
                    sdbgMonedas.Columns("DENPORTAL").Value = ador("DEN").Value

                    RtnPassed = 1
                End If
            End If

            ador.Close
            Set ador = Nothing

        End If

    End If

End Sub

'            If Ador Is Nothing Then
'                RtnPassed = True
'                sdbgMonedas.Columns("DENPORTAL").Value = ""
'                sdbgMonedas.Columns("MONPORTAL").Value = ""
'            Else
'                If Ador.EOF Then
'                    RtnPassed = True
'                    sdbgMonedas.Columns("DENPORTAL").Value = ""
'                    sdbgMonedas.Columns("MONPORTAL").Value = ""
'                Else
'
'                    sdbgMonedas.Columns("MONPORTAL").Value = Ador("COD").Value
'                    sdbgMonedas.Columns("DENPORTAL").Value = Ador("DEN").Value
'
'                    RtnPassed = 1
'                End If
'            End If
'
'            Ador.Close
'            Set Ador = Nothing
'
'        End If
'
'    End If
'
'End Sub
Private Sub sdbgMonedas_BeforeUpdate(Cancel As Integer)
    Dim oError As CTESError
    Dim iEquiv As Integer
    'evitar recalcular los precios con igual equiv
    
    bHayErrores = False
    
        If sCportal <> "" Then
            If sCportal = sdbgMonedas.Columns("MONPORTAL").Value Then Exit Sub
        End If
    
    'Codigo para hacer el update
    
    If sdbgMonedas.Columns("MONFSGS").Value = "" And sdbgMonedas.Columns("MONPORTAL").Value = "" Then
        bHayErrores = False
        Exit Sub
    End If
    
    If sdbgMonedas.Columns("MONPORTAL").Value <> "" And sdbgMonedas.Columns("DENPORTAL").Value <> "" Then
        
        'Si es una modificaci�n de la moneda central esto obligar� a recalcular
        'los vol�menes.Se presentar� un mensaje indic�ndolo
        If sdbgMonedas.Columns(6).Value = True Then
            If basMensajes.PreguntaModEnlaceMonCentral = vbNo Then
                bHayErrores = True
                Cancel = True
                sdbgMonedas.CancelUpdate
                sdbgMonedas.DataChanged = False
                'CargarMonedas
                Exit Sub
            End If
        End If
        iEquiv = g_oRaiz.DevolverEquiv(sdbgMonedas.Columns("MONPORTAL").Value)
        If iEquiv = 0 Then
            basMensajes.MonSinEquivalencia
            bHayErrores = True
            Cancel = True
            sdbgMonedas.CancelUpdate
            sdbgMonedas.DataChanged = False
            Exit Sub
        End If

        Screen.MousePointer = vbHourglass
        Set oError = g_oRaiz.EnlazarMonedaCentral(sdbgMonedas.Columns("ID").Value, sdbgMonedas.Columns("MONPORTAL").Value, sdbgMonedas.Columns("MONFSGS").Value)
        Screen.MousePointer = vbNormal
        If oError.numerror <> 0 Then
            basErrores.TratarError oError
            bHayErrores = True
            Cancel = True
            bHayErrores = False
            sdbgMonedas.CancelUpdate
            sdbgMonedas.DataChanged = False
        Else
            bHayErrores = False
        End If
        
    Else
    
        If sdbgMonedas.Columns("MONPORTAL").Value = "" Then
            basMensajes.EnlazarCentralVacia
            bHayErrores = True
            Cancel = True
            sdbgMonedas.CancelUpdate
            sdbgMonedas.DataChanged = False
            Exit Sub
        Else
            Cancel = True
            sdbgMonedas.CancelUpdate
            sdbgMonedas.DataChanged = False
            bHayErrores = True
            Exit Sub
        End If
    End If
    
  
   
End Sub


Private Sub sdbgMonedas_Click()
'    If sdbgMonedas.Columns("MONPORTAL").Value = "" Then Exit Sub
'
'    sCportal = sdbgMonedas.Columns("MONPORTAL").Value
End Sub

Private Sub sdbgMonedas_GotFocus()
    If sdbgMonedas.Columns("MONPORTAL").Value = "" Then Exit Sub

    sCportal = sdbgMonedas.Columns("MONPORTAL").Value

End Sub

Private Sub sdbgMonedas_HeadClick(ByVal ColIndex As Integer)
    'Ordena el grid seg�n la columna
    Dim ador As ador.Recordset
    Dim adorGS As ador.Recordset
    Dim oError As CTESError
    Dim bEnlazada As Boolean
    
    ''' Carga el grid con las monedas para cada compa��a
    Screen.MousePointer = vbHourglass
    
    'Limpia el grid
    sdbgMonedas.RemoveAll
    
    Set ador = g_oRaiz.DevolverMonedasCentralesDesde(, , , , ColIndex)
    
    If Not ador.EOF Then ador.MoveFirst
    
    While Not ador.EOF
    
        'Si no hay registros en CIAS_MON lee la moneda central de GS para la Cia
        If IsNull(ador("MON_GS")) Then
            Set adorGS = g_oRaiz.DevolverMonedaCentralGS(ador("ID"))
            If Not adorGS Is Nothing Then
                sdbgMonedas.AddItem ador("DEN").Value & Chr(9) & adorGS("MONCEN") & Chr(9) & adorGS("DEN") & Chr(9) & "" & Chr(9) & "" & Chr(9) & ador("ID") & Chr(9) & False
                
                adorGS.Close
                Set adorGS = Nothing
                
            Else  'Si no hay reg. lo rellena vac�o
                sdbgMonedas.AddItem ador("DEN").Value & Chr(9) & "" & Chr(9) & "" & Chr(9) & "" & Chr(9) & "" & Chr(9) & ador("ID") & Chr(9) & False
            End If
            
        Else  'Lee la moneda central de FSGS de CIAS_MON
            If IsNull(ador("MON_PORTAL")) Then
                bEnlazada = False
            Else
                bEnlazada = True
            End If
            sdbgMonedas.AddItem ador("DEN").Value & Chr(9) & ador("MON_GS") & Chr(9) & ador("DEN_GS") & Chr(9) & ador("MON_PORTAL").Value & Chr(9) & ador("DEN_PORTAL").Value & Chr(9) & ador("ID") & Chr(9) & bEnlazada
        End If
        ador.MoveNext
        
    Wend
    
    ador.Close
    Set ador = Nothing
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbgMonedas_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
        sdbgMonedas.CancelUpdate
        bHayErrores = False
    End If
    
End Sub


Private Sub CargarMonedas()
    Dim ador As ador.Recordset
    Dim adorGS As ador.Recordset
    'Dim oError As CTESError
    Dim bEnlazada As Boolean
    
    ''' Carga el grid con las monedas para cada compa��a
    Screen.MousePointer = vbHourglass
    
    sdbgMonedas.RemoveAll
    
    Set ador = g_oRaiz.DevolverMonedasCentralesDesde
    
    If Not ador.EOF Then ador.MoveFirst
    
    While Not ador.EOF
        'Si no hay registros en CIAS_MON lee la moneda central de GS para la Cia
        If IsNull(ador("MON_GS")) Then
            Set adorGS = g_oRaiz.DevolverMonedaCentralGS(ador("ID"))
            If Not adorGS Is Nothing Then
                sdbgMonedas.AddItem ador("DEN").Value & Chr(9) & adorGS("MONCEN") & Chr(9) & adorGS("DEN") & Chr(9) & "" & Chr(9) & "" & Chr(9) & ador("ID") & Chr(9) & False
                
                adorGS.Close
                Set adorGS = Nothing
                
            Else  'Si no hay reg. lo rellena vac�o
                sdbgMonedas.AddItem ador("DEN").Value & Chr(9) & "" & Chr(9) & "" & Chr(9) & "" & Chr(9) & "" & Chr(9) & ador("ID") & Chr(9) & False
            End If
            
            
        Else  'Lee la moneda central de FSGS de CIAS_MON
            If IsNull(ador("MON_PORTAL")) Then
                bEnlazada = False
            Else
                bEnlazada = True
            End If
            sdbgMonedas.AddItem ador("DEN").Value & Chr(9) & ador("MON_GS") & Chr(9) & ador("DEN_GS") & Chr(9) & ador("MON_PORTAL").Value & Chr(9) & ador("DEN_PORTAL").Value & Chr(9) & ador("ID") & Chr(9) & bEnlazada
        End If
        ador.MoveNext
        
    Wend
    
    ador.Close
    Set ador = Nothing
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub CargarMonedaDeLaCiaCompradora()
    Dim ador As ador.Recordset
    Dim adorGS As ador.Recordset
    'Dim oError As CTESError
    Dim bEnlazada As Boolean
    
    ''' Carga el grid con las monedas para cada compa��a
    Screen.MousePointer = vbHourglass
     
    
    Set ador = g_oRaiz.DevolverMonedasCentralesDesde(, , , , , basParametros.g_udtParametrosGenerales.g_lIdCiaCompradora)
        
    If ador Is Nothing Then
        lblMonCenGS.Caption = ""
        Exit Sub
    End If
    
    If ador.EOF Then
        lblMonCenGS.Caption = ""
        ador.Close
        Set ador = Nothing
        Exit Sub
    End If
    
    If IsNull(ador("MON_GS").Value) Or ador("MON_GS").Value = "" Then
            Set adorGS = g_oRaiz.DevolverMonedaCentralGS(basParametros.g_udtParametrosGenerales.g_lIdCiaCompradora)
            If Not adorGS Is Nothing Then
                lblMonCenGS.Caption = adorGS("MONCEN").Value & " - " & adorGS("DEN").Value
                lblMonCenGS.Tag = adorGS("MONCEN").Value
            Else  'Si no hay reg. lo rellena vac�o
                lblMonCenGS.Caption = ""
            End If
    Else  'Lee la moneda central de FSGS de CIAS_MON
        If IsNull(ador("MON_PORTAL")) Then
            bEnlazada = False
        Else
            bEnlazada = True
        End If
        
        lblMonCenGS.Caption = ador("MON_GS") & " - " & ador("DEN_GS")
        lblMonCenGS.Tag = ador("MON_GS").Value
        
        sdbcMonCod.Text = ador("DEN_PORTAL").Value
        sdbcMonCod_Validate False
    End If
    ador.Close
    Set ador = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgMonedas_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    
    'Habilita o no el grid para la modificaci�n dependiendo de si
    'se puede establecer o no la conexi�n con la compa��a
    If sdbgMonedas.Columns("MONFSGS").Value = "" Then
        sdbgMonedas.Columns("MONPORTAL").Locked = True
        sdbgMonedas.Columns("DENPORTAL").Locked = True
        sdbddMonPortCod.Enabled = False
        sdbddMonPortDen.Enabled = False
        sdbddMonPortCod.ListAutoPosition = False
        sdbddMonPortDen.ListAutoPosition = False
    Else
        sdbgMonedas.Columns("MONPORTAL").Locked = False
        sdbgMonedas.Columns("DENPORTAL").Locked = False
        sdbddMonPortCod.Enabled = True
        sdbddMonPortDen.Enabled = True
        sdbddMonPortCod.ListAutoPosition = True
        sdbddMonPortDen.ListAutoPosition = True
    End If
    
End Sub




Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_COORDINACMONCENTR, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdAceptar.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdCancelar.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdListado.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdModoEdicion.Caption = ador(0).Value
        sIdiEdicion = ador(0).Value
        
        ador.MoveNext
        Me.Label1.Caption = ador(0).Value
        ador.MoveNext
        Me.Label2.Caption = ador(0).Value
        ador.MoveNext
        Me.sdbcMonCod.Columns(1).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbcMonCod.Columns(2).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbddMonPortCod.Columns(0).Caption = ador(0).Value
        Me.sdbddMonPortDen.Columns(1).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbddMonPortCod.Columns(1).Caption = ador(0).Value
        Me.sdbddMonPortDen.Columns(0).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgMonedas.Columns(0).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgMonedas.Columns(1).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgMonedas.Columns(2).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgMonedas.Columns(3).Caption = ador(0).Value
        ador.MoveNext
        sIdiConsulta = ador(0).Value
        
        
        
        
        
        ador.Close
    End If
    
    Set ador = Nothing
           
End Sub


