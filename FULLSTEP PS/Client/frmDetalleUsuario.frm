VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmDetalleUsuario 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Datos del usuario"
   ClientHeight    =   7530
   ClientLeft      =   390
   ClientTop       =   1515
   ClientWidth     =   4665
   Icon            =   "frmDetalleUsuario.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7530
   ScaleMode       =   0  'User
   ScaleWidth      =   4655
   Begin VB.TextBox txtTfnoMovil 
      Height          =   285
      Left            =   1215
      MaxLength       =   20
      TabIndex        =   8
      Top             =   4135
      Width           =   1335
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   6825
      Left            =   0
      ScaleHeight     =   6825
      ScaleWidth      =   4755
      TabIndex        =   15
      Top             =   120
      Width           =   4750
      Begin VB.TextBox txtNif 
         Height          =   285
         Left            =   1215
         TabIndex        =   3
         Top             =   1700
         Width           =   2835
      End
      Begin VB.CheckBox chkBlockedUser 
         BackColor       =   &H00808000&
         Caption         =   "DUsuario bloqueado"
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   2640
         TabIndex        =   29
         Top             =   75
         Width           =   2000
      End
      Begin VB.TextBox txtCodigo 
         Height          =   285
         Left            =   1215
         TabIndex        =   0
         Top             =   120
         Width           =   1275
      End
      Begin VB.TextBox txtNombre 
         Height          =   285
         Left            =   1215
         TabIndex        =   1
         Top             =   800
         Width           =   2115
      End
      Begin VB.TextBox txtApellidos 
         Height          =   285
         Left            =   1215
         TabIndex        =   2
         Top             =   1250
         Width           =   2835
      End
      Begin VB.TextBox txtCargo 
         Height          =   285
         Left            =   1215
         TabIndex        =   4
         Top             =   2170
         Width           =   2835
      End
      Begin VB.TextBox txtDep 
         Height          =   285
         Left            =   1215
         TabIndex        =   5
         Top             =   2620
         Width           =   2835
      End
      Begin VB.TextBox txtTfno1 
         Height          =   285
         Left            =   1215
         MaxLength       =   20
         TabIndex        =   6
         Top             =   3085
         Width           =   1335
      End
      Begin VB.TextBox txtTfno2 
         Height          =   285
         Left            =   1215
         MaxLength       =   20
         TabIndex        =   7
         Top             =   3535
         Width           =   1335
      End
      Begin VB.TextBox txtFax 
         Height          =   285
         Left            =   1215
         MaxLength       =   20
         TabIndex        =   9
         Top             =   4480
         Width           =   1335
      End
      Begin VB.TextBox txtEmail 
         Height          =   285
         Left            =   1215
         TabIndex        =   10
         Top             =   4950
         Width           =   2835
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcIdiCod 
         Height          =   285
         Left            =   1215
         TabIndex        =   11
         Top             =   5410
         Width           =   915
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1111
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3545
         Columns(2).Caption=   "Denominaci�n"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   1614
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcIdiDen 
         Height          =   285
         Left            =   2130
         TabIndex        =   12
         Top             =   5410
         Width           =   1935
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3493
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1138
         Columns(2).Caption=   "C�digo"
         Columns(2).Name =   "COD"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   3413
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCatUsu 
         Height          =   285
         Left            =   1215
         TabIndex        =   28
         Top             =   6250
         Width           =   2835
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1138
         Columns(1).Caption=   "DC�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3493
         Columns(2).Caption=   "DDenominaci�n"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "COSTEHORA"
         Columns(3).Name =   "COSTEHORA"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         _ExtentX        =   5001
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label11 
         BackColor       =   &H00808000&
         Caption         =   "NIF:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   120
         TabIndex        =   30
         Top             =   1740
         Width           =   1005
      End
      Begin VB.Label lblCategoria 
         BackColor       =   &H00808000&
         Caption         =   "DCategoria laboral :"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   27
         Top             =   5990
         Width           =   1590
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00FFFFFF&
         X1              =   150
         X2              =   4065
         Y1              =   5870
         Y2              =   5870
      End
      Begin VB.Label Label13 
         BackColor       =   &H00808000&
         Caption         =   "Tfno. m�vil:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   26
         Top             =   4050
         Width           =   1005
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00FFFFFF&
         X1              =   120
         X2              =   4035
         Y1              =   590
         Y2              =   590
      End
      Begin VB.Label Label8 
         BackColor       =   &H00808000&
         Caption         =   "Tfno. 2:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   25
         Top             =   3570
         Width           =   900
      End
      Begin VB.Label Label7 
         BackColor       =   &H00808000&
         Caption         =   "C�digo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   24
         Top             =   135
         Width           =   615
      End
      Begin VB.Label Label5 
         BackColor       =   &H00808000&
         Caption         =   "E-mail:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   23
         Top             =   5025
         Width           =   900
      End
      Begin VB.Label Label4 
         BackColor       =   &H00808000&
         Caption         =   "Fax:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   22
         Top             =   4530
         Width           =   900
      End
      Begin VB.Label Label3 
         BackColor       =   &H00808000&
         Caption         =   "Tfno. 1:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   21
         Top             =   3120
         Width           =   900
      End
      Begin VB.Label Label2 
         BackColor       =   &H00808000&
         Caption         =   "Apellidos:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   20
         Top             =   1275
         Width           =   1005
      End
      Begin VB.Label Label1 
         BackColor       =   &H00808000&
         Caption         =   "Nombre:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   19
         Top             =   845
         Width           =   900
      End
      Begin VB.Label Label6 
         BackColor       =   &H00808000&
         Caption         =   "Cargo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   18
         Top             =   2225
         Width           =   900
      End
      Begin VB.Label Label9 
         BackColor       =   &H00808000&
         Caption         =   "Depart.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   17
         Top             =   2690
         Width           =   900
      End
      Begin VB.Label Label12 
         BackColor       =   &H00808000&
         Caption         =   "Idioma:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   150
         TabIndex        =   16
         Top             =   5495
         Width           =   990
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   345
      Left            =   2265
      TabIndex        =   14
      Top             =   7185
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   345
      Left            =   1140
      TabIndex        =   13
      Top             =   7185
      Width           =   1005
   End
End
Attribute VB_Name = "frmDetalleUsuario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public RespetarCombo As Boolean
Public CargarComboDesde As Boolean
Public Cia As Long
Public Usu As Integer
Public fecpwd As Date
Public oUsuario As CUsuario
Public bModificar As Boolean
Public sPais As String
Private sUsuCodAntiguo As String


Private sIdiContrasena As String
Private sIdiConfirm As String
Private sIdiCodigo As String
Private sIdiCodUsu As String
Private sIdiApe As String
Private sIdiNom As String
Private sIdiDep As String
Private sIdiCar As String
Private sIdiTfno1 As String
Private sIdiEmail As String
Private sIdiIdioma As String

Public RespetarComboCat As Boolean
Private m_oIdiomas As CIdiomas  'Aqu� mantenemos todos los idiomas de la aplicaci�n, teniendo cargado en primer lugar el idioma del usuario.

Public m_bBloquearComboCat As Boolean

Private m_sCodigo As String
Private m_bBlockedUser As Boolean
Private m_sNombre As String
Private m_sApellidos As String
Private m_sNif As String
Private m_sCargo As String
Private m_sDep As String
Private m_sTfno1 As String
Private m_sTfno2 As String
Private m_sTfnoMovil As String
Private m_sFax As String
Private m_sEmail As String
Private m_sIdiCod As String
Private m_sCatUsu As String


Private Function DatosGen_Cambiados(ByRef sLitRegAccion() As String) As String
Dim s As String
If m_sCodigo <> txtCodigo.Text Then
     s = s & " " & sLitRegAccion(53) & ";"
End If
If m_bBlockedUser <> chkBlockedUser.Value Then
    If chkBlockedUser.Value Then
        s = s & " " & sLitRegAccion(51) & ";"
    Else
        s = s & " " & sLitRegAccion(52) & ";"
    End If
End If
If m_sNombre <> txtNombre.Text Then s = s & " " & sLitRegAccion(27) & ";"
If m_sApellidos <> txtApellidos.Text Then s = s & " " & sLitRegAccion(1) & ";"
If m_sNif <> txtNif.Text Then s = s & " " & sLitRegAccion(25) & ";"
If m_sCargo <> txtCargo.Text Then s = s & " " & sLitRegAccion(6) & ";"
If m_sDep <> txtDep.Text Then s = s & " " & sLitRegAccion(15) & ";"
If m_sTfno1 <> txtTfno1.Text Then s = s & " " & sLitRegAccion(45) & " 1;"
If m_sTfno2 <> txtTfno2.Text Then s = s & " " & sLitRegAccion(45) & " 2;"
If m_sTfnoMovil <> txtTfnoMovil.Text Then s = s & " " & sLitRegAccion(46) & ";"
If m_sFax <> txtFax.Text Then s = s & " " & sLitRegAccion(20) & ";"
If m_sEmail <> txtEmail.Text Then s = s & " " & sLitRegAccion(19) & ";"
If m_sIdiCod <> sdbcIdiCod.Text Then s = s & " " & sLitRegAccion(22) & ";"
If m_sCatUsu <> sdbcCatUsu.Text Then s = s & " " & sLitRegAccion(7) & ";"

If s = "" Then
    s = sLitRegAccion(26)
End If
DatosGen_Cambiados = s

End Function

Private Sub DatosGen_Iniciales()
m_sCodigo = txtCodigo.Text
m_bBlockedUser = chkBlockedUser.Value
m_sNombre = txtNombre.Text
m_sApellidos = txtApellidos.Text
m_sNif = txtNif.Text
m_sCargo = txtCargo.Text
m_sDep = txtDep.Text
m_sTfno1 = txtTfno1.Text
m_sTfno2 = txtTfno2.Text
m_sTfnoMovil = txtTfnoMovil.Text
m_sFax = txtFax.Text
m_sEmail = txtEmail.Text
m_sIdiCod = sdbcIdiCod.Text
m_sCatUsu = sdbcCatUsu.Text
End Sub

Private Sub cmdAceptar_Click()
    Dim oError As CTESError
    Dim Fecha
    Dim desbloqueado As Boolean
    
    Fecha = Date
    
    If Validacion Then
        If oUsuario.ID = 0 Then
            Set oError = oUsuario.AnyadirUsuario(frmCompanias.g_oCiaSeleccionada.CodGS)
            If oError.NumError = TESnoerror Then
                    g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Usu_Anyadir, g_sLitRegAccion_SPA(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_SPA(48) & ": " & oUsuario.Cod, _
                                                        g_sLitRegAccion_ENG(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_SPA(48) & ": " & oUsuario.Cod, _
                                                        g_sLitRegAccion_FRA(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_FRA(48) & ": " & oUsuario.Cod, _
                                                        g_sLitRegAccion_GER(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_GER(48) & ": " & oUsuario.Cod, _
                                                        frmCompanias.g_oCiaSeleccionada.ID
            End If
        Else
            desbloqueado = oUsuario.UsuarioBloqueado And Not (oUsuario.UsuarioBloqueado = CBool(chkBlockedUser.Value))
            oUsuario.UsuarioBloqueado = chkBlockedUser.Value
            'Si est� activo el FSGA y la CIA est� enlazada se mostrar�n las columnas de categor�a laboral y Acceso FSGA.
            If frmCompanias.g_oCiaSeleccionada.CodGS <> "" And g_udtParametrosGenerales.g_bAccesoFSGA Then
                Set oError = oUsuario.ModificarDatosUsuario(sUsuCodAntiguo, g_oRaiz.SPTS, frmCompanias.g_oCiaSeleccionada.CodGS, desbloqueado)
            Else
                Set oError = oUsuario.ModificarDatosUsuario(sUsuCodAntiguo, g_oRaiz.SPTS, , desbloqueado)
            End If
            g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Usu_Modif_Datos, g_sLitRegAccion_SPA(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_SPA(48) & ": " & oUsuario.Cod & "; " & g_sLitRegAccion_SPA(13) & ":" & DatosGen_Cambiados(g_sLitRegAccion_SPA()), _
                                        g_sLitRegAccion_ENG(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_ENG(48) & ": " & oUsuario.Cod & "; " & g_sLitRegAccion_ENG(13) & ":" & DatosGen_Cambiados(g_sLitRegAccion_ENG()), _
                                        g_sLitRegAccion_FRA(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_FRA(48) & ": " & oUsuario.Cod & "; " & g_sLitRegAccion_FRA(13) & ":" & DatosGen_Cambiados(g_sLitRegAccion_FRA()), _
                                        g_sLitRegAccion_GER(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_GER(48) & ": " & oUsuario.Cod & "; " & g_sLitRegAccion_GER(13) & ":" & DatosGen_Cambiados(g_sLitRegAccion_GER()), _
                                        frmCompanias.g_oCiaSeleccionada.ID
        End If
        frmCompanias.CargarUsuariosCia
        If oError.NumError <> 0 Then
            basErrores.TratarError oError
            Exit Sub
        Else
            If desbloqueado Then EnviarMailDesbloqueoUsuario oUsuario
            Unload Me
        End If
    End If
End Sub


Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
    CargarRecursos
    Cia = oUsuario.Cia
    
    Set m_oIdiomas = g_oGestorParametros.DevolverIdiomasPrimeroIdiUsu
    
    'Si est� activo el FSGA y la CIA est� enlazada se mostrar�n la secci�n de categor�as laborales.
    If frmCompanias.g_oCiaSeleccionada.CodGS <> "" And g_udtParametrosGenerales.g_bAccesoFSGA Then
        frmDetalleUsuario.Height = 7800
        Picture1.Height = 6600
        cmdAceptar.Top = 6900
        cmdCancelar.Top = 6900
        Line2.Visible = True
        lblCategoria.Visible = True
        sdbcCatUsu.Visible = True
    Else
        frmDetalleUsuario.Height = 7005
        Picture1.Height = 5745
        cmdAceptar.Top = 6055
        cmdCancelar.Top = 6055
        Line2.Visible = False
        lblCategoria.Visible = False
        sdbcCatUsu.Visible = False
    End If
    
    
    If oUsuario.ID <> 0 Then
        Usu = oUsuario.ID
        txtApellidos = oUsuario.Apellidos
        txtNif = oUsuario.NIF
        txtCargo = oUsuario.Cargo
        txtCodigo = oUsuario.Cod
        txtDep = oUsuario.Departamento
        txtEmail = oUsuario.Email
        txtFax = oUsuario.Fax
        txtTfno1 = oUsuario.Tfno1
        txtTfno2 = oUsuario.Tfno2
        txtTfnoMovil = oUsuario.TfnoMovil
        txtNombre = oUsuario.Nombre
        txtTfnoMovil = oUsuario.TfnoMovil
        
        chkBlockedUser.Value = IIf(oUsuario.UsuarioBloqueado, Checked, Unchecked)
        chkBlockedUser.Visible = oUsuario.UsuarioBloqueado
        
        If Not IsNull(oUsuario.idioma) Then
            RespetarCombo = True
            sdbcIdiDen = oUsuario.Idiomaden
            sdbcIdiDen_Validate False
        End If
        'Si est� activo el FSGA y la CIA est� enlazada se mostrar� su categor�a laboral, si la tiene.
        If frmCompanias.g_oCiaSeleccionada.CodGS <> "" And g_udtParametrosGenerales.g_bAccesoFSGA Then
            If oUsuario.IDCategoria <> 0 Then
                RespetarComboCat = True
                sdbcCatUsu = frmCompanias.m_oCategoriasProve.Item(CStr(oUsuario.IDCategoria)).Denominaciones.Item(CStr(g_udtParametrosGenerales.g_iIdIdioma)).Den
                sdbcCatUsu.Columns("ID").Value = oUsuario.IDCategoria
                RespetarComboCat = False
            End If
 
        End If
        DatosGen_Iniciales
    Else
        chkBlockedUser.Value = 0
        chkBlockedUser.Visible = False
    End If

    
    If m_bBloquearComboCat Then
        sdbcCatUsu.Enabled = False
    Else
        sdbcCatUsu.Enabled = True
    End If
    
    If Not bModificar Then
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        cmdAceptar.Enabled = False
        cmdCancelar.Enabled = False
        Picture1.Enabled = False
    Else
        cmdAceptar.Visible = True
        cmdCancelar.Visible = True
        cmdAceptar.Enabled = True
        cmdCancelar.Enabled = True
        Picture1.Enabled = True
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oUsuario = Nothing
End Sub

Private Sub sdbcCatUsu_Change()
Dim sCat As String
Dim bm As Variant
    
    If RespetarComboCat Then Exit Sub
    
    'If Trim(sdbcCatUsu.Text = "") Then Exit Sub
    
    If Not sdbcCatUsu.DroppedDown Then SendKeys "{F4}"
    
    sCat = sdbcCatUsu.Text
    sCat = EliminarCaracteresEspeciales(sCat)
    bm = BuscarCatUsu(sCat)
    sdbcCatUsu.Bookmark = bm

End Sub

Private Sub sdbcCatUsu_CloseUp()

    sdbcCatUsu.Columns("ID").Value = sdbcCatUsu.Columns("ID").Value
    sdbcCatUsu.Columns("COD").Value = sdbcCatUsu.Columns("COD").Value
    sdbcCatUsu.Columns("DEN").Value = sdbcCatUsu.Columns("DEN").Value
    RespetarComboCat = True
    sdbcCatUsu.Text = sdbcCatUsu.Columns("DEN").Text
    RespetarComboCat = False
    sdbcCatUsu.Columns("COSTEHORA").Value = sdbcCatUsu.Columns("COSTEHORA").Value
  
End Sub

Private Sub sdbcCatUsu_DropDown()
Dim i As Integer
Dim sLinea As String
Dim oIdi As CIdioma

    With sdbcCatUsu
        .RemoveAll
        If frmCompanias.m_oCategoriasProve.Count > 0 Then
            For i = 1 To frmCompanias.m_oCategoriasProve.Count
                sLinea = frmCompanias.m_oCategoriasProve.Item(i).ID & Chr(9) & _
                    frmCompanias.m_oCategoriasProve.Item(i).Codigo
                    For Each oIdi In m_oIdiomas
                        If oIdi.Cod = g_udtParametrosGenerales.g_sIdioma Then
                            sLinea = sLinea & Chr(9) & frmCompanias.m_oCategoriasProve.Item(i).Denominaciones.Item(CStr(oIdi.ID)).Den
                        End If
                    Next
                    sLinea = sLinea & Chr(9) & frmCompanias.m_oCategoriasProve.Item(i).CosteH

                If CInt(frmCompanias.m_oCategoriasProve.Item(i).Dact) = 0 Then  'Solo se cargan en el combo los que no est�n dados de baja en el proveedor.
                    .AddItem sLinea
                End If
            Next
        End If
        
        If .Rows = 0 Then .AddItem ""
    End With
End Sub


Private Sub sdbcCatUsu_InitColumnProps()

    sdbcCatUsu.DataFieldList = "Column 2"
    sdbcCatUsu.DataFieldToDisplay = "Column 2"
    
End Sub

Private Sub sdbcCatUsu_PositionList(ByVal Text As String)
'''Dim i As Long
'''    Dim bm As Variant
'''
'''    On Error Resume Next
'''
'''    sdbcCatUsu.MoveFirst
'''
'''    If Text <> "" Then
'''        For i = 0 To sdbcCatUsu.Rows - 1
'''            bm = sdbcCatUsu.GetBookmark(i)
'''            If UCase(Text) = UCase(Mid(sdbcCatUsu.Columns(2).CellText(bm), 1, Len(Text))) Then
'''                sdbcCatUsu.Bookmark = bm
'''                Exit For
'''            End If
'''        Next i
'''    End If
End Sub

Private Sub sdbcIdiCod_Change()
 
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcIdiDen.Text = ""
    
        CargarComboDesde = True
        
        sdbcIdiCod = ""
       
        
        
    End If
    
End Sub

Private Sub sdbcIdiCod_CloseUp()
    
    If sdbcIdiCod.Value = "..." Then
        sdbcIdiCod.Text = ""
        Exit Sub
    End If
    
    RespetarCombo = True
    sdbcIdiDen.Text = sdbcIdiCod.Columns(2).Text
    sdbcIdiCod.Text = sdbcIdiCod.Columns(1).Text

    RespetarCombo = False
   
    
    CargarComboDesde = False
End Sub

Private Sub sdbcIdiCod_DropDown()
Dim Ador As Ador.Recordset
    
   
    sdbcIdiCod.RemoveAll
    
    If CargarComboDesde Then
        Set Ador = g_oRaiz.DevolverIdiomasDelPortalDesde(basParametros.g_iCargaMaximaCombos, sdbcIdiCod, , False)
    Else
        Set Ador = g_oRaiz.DevolverIdiomasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , , False)
    End If
    
    If Not Ador Is Nothing Then
        
        While Not Ador.EOF
        
            sdbcIdiCod.AddItem Ador("ID").Value & Chr(9) & Ador("COD").Value & Chr(9) & Ador("DEN").Value
            Ador.MoveNext
        
        Wend
        
        Ador.Close
        Set Ador = Nothing
        
    End If
    
        
End Sub

Private Sub sdbcIdiCod_InitColumnProps()
    
    sdbcIdiCod.DataFieldList = "Column 1"
    sdbcIdiCod.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcIdiCod_PositionList(ByVal Text As String)
Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcIdiCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcIdiCod.Rows - 1
            bm = sdbcIdiCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcIdiCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcIdiCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcIdiCod_Validate(Cancel As Boolean)
Dim scod As String
Dim Ador As Ador.Recordset
    
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
       
    If sdbcIdiCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    
     Set Ador = g_oRaiz.DevolverIdiomasDelPortalDesde(g_iCargaMaximaCombos, sdbcIdiCod.Text, , True, False)
    
    
    If Ador Is Nothing Then
        sdbcIdiCod.Text = ""
    Else
        If Ador.EOF Then
            sdbcIdiCod.Text = ""
        Else
        
            RespetarCombo = True
            sdbcIdiDen.Text = Ador("DEN").Value
            RespetarCombo = False
            CargarComboDesde = False
            
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    
End Sub

Private Sub sdbcIdiDen_Change()
    
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcIdiCod.Text = ""
    
        CargarComboDesde = True
        
        sdbcIdiDen = ""
        
    End If
End Sub

Private Sub sdbcIdiDen_CloseUp()
    
    If sdbcIdiDen.Value = "..." Then
        sdbcIdiDen.Text = ""
        Exit Sub
    End If
    
    RespetarCombo = True
    sdbcIdiDen.Text = sdbcIdiDen.Columns(1).Text
    sdbcIdiCod.Text = sdbcIdiDen.Columns(2).Text
    sdbcIdiCod.Columns("ID").Value = sdbcIdiDen.Columns("ID").Value
    RespetarCombo = False
    
    CargarComboDesde = False
End Sub

Private Sub sdbcIdiDen_DropDown()
Dim Ador As Ador.Recordset
    
    sdbcIdiDen.RemoveAll
    
    If CargarComboDesde Then
        Set Ador = g_oRaiz.DevolverIdiomasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbcIdiDen, , True)
    Else
        Set Ador = g_oRaiz.DevolverIdiomasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , , , True)
    End If
    
    If Not Ador Is Nothing Then
        
        While Not Ador.EOF
        
            sdbcIdiDen.AddItem Ador("ID").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("COD").Value
            Ador.MoveNext
        
        Wend
        Ador.Close
        Set Ador = Nothing
        
    End If
    
End Sub

Private Sub sdbcIdiDen_InitColumnProps()
    
    sdbcIdiDen.DataFieldList = "Column 0"
    sdbcIdiDen.DataFieldToDisplay = "Column 0"
    
End Sub

''' <summary>Realiza validaciones de datos</summary>
''' <returns>Booleano indicando si la validaci�n es correcta</returns>
''' <remarks>Llamada desde cmdAceptar_Click</remarks>
''' <revision>LTG 16/06/2011</revision>

Private Function Validacion() As Boolean
    Dim Ador As Ador.Recordset
    Dim oCia As CCia
    Dim oUsu As CUsuario
    Dim existe As Boolean
            
    If Trim(txtCodigo) = "" Then
        basMensajes.NoValido sIdiCodigo
        txtCodigo.SetFocus
        Validacion = False
        Exit Function
    Else
        If txtCodigo <> oUsuario.Cod Then
            Set oUsu = g_oRaiz.Generar_CUsuario
            existe = oUsu.ExisteCodigoUsuario(txtCodigo, Cia)
            If existe Then
                basMensajes.DatoDuplicado sIdiCodUsu
                Validacion = False
                Exit Function
            End If
            Set Ador = Nothing
        End If
    End If

    If Trim(txtApellidos) = "" Then
        basMensajes.NoValido sIdiApe
        txtApellidos.SetFocus
        Validacion = False
        Exit Function
    End If

    If Trim(txtNombre) = "" Then
        basMensajes.NoValido sIdiNom
        txtNombre.SetFocus
        Validacion = False
        Exit Function
    End If
    
    If Trim(txtDep) = "" Then
        basMensajes.NoValido sIdiDep
        txtDep.SetFocus
        Validacion = False
        Exit Function
    End If
    
    If Trim(txtCargo) = "" Then
        basMensajes.NoValido sIdiCar
        txtCargo.SetFocus
        Validacion = False
        Exit Function
    End If
    
    If Trim(txtTfno1) = "" Then
        basMensajes.NoValido sIdiTfno1
        txtTfno1.SetFocus
        Validacion = False
        Exit Function
    Else
        If g_udtParametrosGenerales.gbCompruebaTfno Then
            If Not ComprobarTelefono(txtTfno1.Text, sPais) Then
                basMensajes.TelefonoNoValido txtTfno1.Text
                txtTfno1.SetFocus
                Validacion = False
                Exit Function
            End If
        End If
    End If
    
    If txtTfno2.Text <> vbNullString Then
        If g_udtParametrosGenerales.gbCompruebaTfno Then
            If Not ComprobarTelefono(txtTfno2.Text, sPais) Then
                basMensajes.TelefonoNoValido txtTfno2.Text
                txtTfno2.SetFocus
                Validacion = False
                Exit Function
            End If
        End If
    End If
    
    If txtTfnoMovil.Text <> vbNullString Then
        If g_udtParametrosGenerales.gbCompruebaTfno Then
            If Not ComprobarTelefono(txtTfnoMovil.Text, sPais) Then
                basMensajes.TelefonoNoValido txtTfnoMovil.Text
                txtTfnoMovil.SetFocus
                Validacion = False
                Exit Function
            End If
        End If
    End If
    
    If txtFax.Text <> vbNullString Then
        If g_udtParametrosGenerales.gbCompruebaTfno Then
            If Not ComprobarTelefono(txtFax.Text, sPais) Then
                basMensajes.TelefonoNoValido txtFax.Text
                txtFax.SetFocus
                Validacion = False
                Exit Function
            End If
        End If
    End If
    
    If Trim(txtEmail) = "" Then
        basMensajes.NoValido sIdiEmail
        txtEmail.SetFocus
        Validacion = False
        Exit Function
    Else
        If Not ComprobarEmail(txtEmail.Text) Then
            basMensajes.EMailNoValido txtEmail.Text
            txtEmail.SetFocus
            Validacion = False
            Exit Function
        End If
    End If
    
    If sdbcIdiCod.Value = "" Then
        basMensajes.NoValido sIdiIdioma
        sdbcIdiCod.SetFocus
        Validacion = False
        Exit Function
    End If
    
    If Me.txtNif.Text <> "" And PaisEspanna(sPais) Then
        If Not basNIFCIF.VALIDAR_NIF(Me.txtNif.Text) Then
            basMensajes.NifIncorrecto
            Me.txtNif.SetFocus
            Validacion = False
            Exit Function
        End If
    End If
    
    sUsuCodAntiguo = oUsuario.Cod
    oUsuario.Cod = txtCodigo
    oUsuario.Apellidos = txtApellidos
    oUsuario.NIF = txtNif
    oUsuario.Nombre = txtNombre
    oUsuario.Departamento = txtDep
    oUsuario.Cargo = txtCargo
    oUsuario.Tfno1 = txtTfno1
    oUsuario.Tfno2 = txtTfno2
    oUsuario.TfnoMovil = txtTfnoMovil
    oUsuario.Fax = txtFax
    oUsuario.Email = txtEmail
    oUsuario.idioma = sdbcIdiCod.Columns("ID").Value
    If Trim(sdbcCatUsu.Columns("ID").Value) = "" Then
        oUsuario.IDCategoria = 0
    Else
        oUsuario.IDCategoria = sdbcCatUsu.Columns("ID").Value
    End If
    Validacion = True
End Function

Private Sub sdbcIdiDen_Validate(Cancel As Boolean)
Dim scod As String
Dim Ador As Ador.Recordset
    
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
       
    If sdbcIdiDen.Text = "" Then Exit Sub
    
    Set Ador = g_oRaiz.DevolverIdiomasDelPortalDesde(g_iCargaMaximaCombos, , sdbcIdiDen.Text, True, False)
    
    
    If Ador Is Nothing Then
        sdbcIdiDen.Text = ""
    Else
        If Ador.EOF Then
            sdbcIdiDen.Text = ""
        Else
        
            RespetarCombo = True
            sdbcIdiCod.AddItem Ador("ID").Value & Chr(9) & Ador("COD").Value & Chr(9) & Ador("DEN").Value
            sdbcIdiCod.Text = Ador("COD").Value
           'sdbcIdiCod.Columns("ID").Value = sdbcIdiDen.Columns("ID").Value
            RespetarCombo = False
            CargarComboDesde = False
            
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    

End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_DETALLEUSUARIO, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label1.Caption = Ador(0).Value & ":"
        sIdiNom = Ador(0).Value
        Ador.MoveNext
        sIdiContrasena = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Me.Label12.Caption = Ador(0).Value & ":"
        sIdiIdioma = Ador(0).Value
        Ador.MoveNext
        Me.Label13.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label2.Caption = Ador(0).Value & ":"
        sIdiApe = Ador(0).Value
        Ador.MoveNext
        Me.Label3.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label4.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label5.Caption = Ador(0).Value & ":"
        sIdiEmail = Ador(0).Value
        Ador.MoveNext
        Me.Label6.Caption = Ador(0).Value & ":"
        sIdiCar = Ador(0).Value
        Ador.MoveNext
        Me.Label7.Caption = Ador(0).Value & ":"
        sIdiCodigo = Ador(0).Value
        Me.sdbcIdiCod.Columns(1).Caption = Ador(0).Value
        Me.sdbcIdiDen.Columns(2).Caption = Ador(0).Value
        Me.sdbcCatUsu.Columns("COD").Caption = Ador(0).Value
        Me.sdbcCatUsu.Columns("COD").Caption = Ador(0).Value '14 C�digo
        Ador.MoveNext
        Me.Label8.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label9.Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcIdiCod.Columns(2).Caption = Ador(0).Value
        Me.sdbcIdiDen.Columns(1).Caption = Ador(0).Value
        Me.sdbcCatUsu.Columns("DEN").Caption = Ador(0).Value
        Ador.MoveNext
        sIdiConfirm = Ador(0).Value
        Ador.MoveNext
        sIdiCodUsu = Ador(0).Value
        Ador.MoveNext
        sIdiDep = Ador(0).Value
        Ador.MoveNext
        sIdiTfno1 = Ador(0).Value
        Ador.MoveNext
        lblCategoria.Caption = Ador(0).Value    '22 Categor�a laboral
        Ador.MoveNext
        Me.sdbcCatUsu.Columns("COSTEHORA").Caption = Ador(0).Value  '23 Coste / Hora
        Ador.MoveNext
        chkBlockedUser.Caption = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub



''' <summary>Busca una categoria en el combo de categorias</summary>
''' <param name="sCat">Cadena con la categor�a</param>
''' <returns>El bookmark del elemento encontrado en el combo de categorias</returns>
''' <remarks>Llamada desde sdbgUsu_KeyDown y sdbdCatUsu_PositionList</remarks>

Private Function BuscarCatUsu(ByVal sCat As String) As Variant
    Dim i As Long
    Dim bm As Variant
    Dim sColValor As String
    Dim sColName As String
    

    With sdbcCatUsu
        .MoveFirst
    
        If sCat <> "" Then
            For i = 0 To .Rows - 1
                bm = .GetBookmark(i)
                sColName = sdbcCatUsu.Text
                sColValor = EliminarCaracteresEspeciales(.Columns("DEN").CellText(bm))
                If InStr(1, UCase(sColValor), UCase(sCat)) > 0 Then
                    BuscarCatUsu = bm
                    Exit For
                End If
            Next i
        End If
    End With
    
End Function

Private Sub EnviarMailDesbloqueoUsuario(poUsuario As CUsuario)
    Dim sAsunto As String
    Dim sCuerpo As String
    Dim sTo As String
    Dim errormail As TipoErrorSummit
    
    Select Case poUsuario.idioma
        Case 0
            sAsunto = g_udtParametrosGenerales.g_sNotifDesbloqueoUsuarioENG
        Case 1
            sAsunto = g_udtParametrosGenerales.g_sNotifDesbloqueoUsuarioSPA
        Case 2
            sAsunto = g_udtParametrosGenerales.g_sNotifDesbloqueoUsuarioGER
    End Select
    
    If g_oMailSender Is Nothing Then
        DoEvents
        IniciarSesionMail
    End If
    
    sTo = poUsuario.Email
    sCuerpo = GenerarMensajeDesbloqueoUsuario(poUsuario)
    
    If sCuerpo <> "" Then
        errormail = ComponerMensaje(sTo, sAsunto, sCuerpo, , "frmCompanias", poUsuario.Cia, SQLBinaryToBoolean(poUsuario.TipoMail))
        If errormail.NumError <> TESnoerror Then
            If errormail.NumError <> TESnoerror Then
                basMensajes.ErrorOriginal errormail
            End If
        End If
    End If
            
    Clipboard.Clear
End Sub

Private Function GenerarMensajeDesbloqueoUsuario(ByVal poUsuario As CUsuario) As String
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim sFileName As String
    Dim sIdioma As String
    Dim sCarpeta_Plantilla As String
    Select Case poUsuario.idioma
        Case 0
            sFileName = "ENG_"
            sIdioma = "ENG"
        Case 1
            sFileName = "SPA_"
            sIdioma = "SPA"
        Case 2
            sFileName = "GER_"
            sIdioma = "GER"
    End Select
   
    Select Case poUsuario.TipoMail
        Case 1
            'HTML
            sFileName = sFileName & "FSPSDesbloqueoUsuario.htm"
        Case Else
            'TEXTO
            sFileName = sFileName & "FSPSDesbloqueoUsuario.txt"
    End Select

    If g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS <> "" Then
        frmCompanias.g_oCiaSeleccionada.SacarRemitente
        sCarpeta_Plantilla = frmCompanias.g_oCiaSeleccionada.CarpetaPlantilla
    End If
    Set oFos = New FileSystemObject
    
    If Not oFos.FileExists(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, sCarpeta_Plantilla) & "\" & sFileName) Then
        basMensajes.RutaPlantillasNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeDesbloqueoUsuario = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, sCarpeta_Plantilla) & "\" & sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll

    GenerarMensajeDesbloqueoUsuario = sCuerpoMensaje
End Function
