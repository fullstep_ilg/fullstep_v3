VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCOM 
   Caption         =   "Tipos de comunicaci�n"
   ClientHeight    =   4680
   ClientLeft      =   720
   ClientTop       =   1590
   ClientWidth     =   6585
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCOM.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   4680
   ScaleWidth      =   6585
   Begin SSDataWidgets_B.SSDBGrid sdbgTipoCom 
      Height          =   4185
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   6525
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCOM.frx":014A
      stylesets(0).AlignmentText=   0
      stylesets(0).AlignmentPicture=   1
      AllowUpdate     =   0   'False
      ActiveCellStyleSet=   "Normal"
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      BalloonHelp     =   0   'False
      MaxSelectedRows =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "Normal"
      Columns.Count   =   3
      Columns(0).Width=   2302
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).AllowSizing=   0   'False
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   50
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777152
      Columns(1).Width=   8176
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   200
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ID"
      Columns(2).Name =   "ID"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   11509
      _ExtentY        =   7382
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   6585
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   4125
      Width           =   6585
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         Height          =   345
         Left            =   5520
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         Height          =   345
         Left            =   2340
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Height          =   345
         Left            =   60
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         Height          =   345
         Left            =   60
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         Height          =   345
         Left            =   1200
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmCOM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private oTiposCom As CTiposComunicacion
Private oTipoComSelec As CTipoComunicacion
Private bValError As Boolean
Private bAnyaError As Boolean
Private bModError As Boolean
Private bModoEdicion As Boolean
Private bAnyadir As Boolean
Private bCriterioCoincidenciaTotal  As Boolean

Private sIdiTipoCom As String
Private sIdiConsulta As String
Private sIdiComCon As String
Private sIdiEdicion As String
Private sIdiPTipoCom As String
Private sIdiCodigo As String
Private sIdiDenominacion As String
Private sIdiOrdenando As String



Private Sub cmdA�adir_Click()

    ''' * Objetivo: Situarnos en la fila de adicion
    
    sdbgTipoCom.Scroll 0, sdbgTipoCom.Rows - sdbgTipoCom.Row
    
    If sdbgTipoCom.VisibleRows > 0 Then
        
        If sdbgTipoCom.VisibleRows >= sdbgTipoCom.Rows Then
            sdbgTipoCom.Row = sdbgTipoCom.Rows
        Else
            sdbgTipoCom.Row = sdbgTipoCom.Rows - (sdbgTipoCom.Rows - sdbgTipoCom.VisibleRows) - 1
        End If
        
    End If
    
    bAnyadir = True
    sdbgTipoCom.SetFocus
    
End Sub

Private Sub cmdDeshacer_Click()

    ''' * Objetivo: Deshacer la edicion en el pais actual
    
    sdbgTipoCom.CancelUpdate
    sdbgTipoCom.DataChanged = False
    bAnyadir = False
    If Not oTipoComSelec Is Nothing Then
        Set oTipoComSelec = Nothing
    End If
    
    If Not sdbgTipoCom.IsAddRow Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
    Else
        cmdDeshacer.Enabled = False
    End If
    
End Sub
Private Sub cmdEliminar_Click()

    ''' * Objetivo: Eliminar el Tipo actual
    
    If sdbgTipoCom.Rows = 0 Then Exit Sub
    
    sdbgTipoCom.SelBookmarks.Add sdbgTipoCom.Bookmark
    sdbgTipoCom.DeleteSelected
    sdbgTipoCom.SelBookmarks.RemoveAll
    
End Sub
Private Sub cmdModoEdicion_Click()

    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    
    If Not bModoEdicion Then
                
        'cmdRestaurar_Click
        
        sdbgTipoCom.AllowAddNew = True
        sdbgTipoCom.AllowUpdate = True
        sdbgTipoCom.AllowDelete = True
        
        frmCOM.Caption = sIdiTipoCom
'        frmCOM.Caption = sIdiTitulos(4)
        
        cmdModoEdicion.Caption = sIdiConsulta
'        cmdModoEdicion.Caption = sIdiConsulta
        
        cmdRestaurar.Visible = False
        cmdA�adir.Visible = True
        cmdEliminar.Visible = True
        cmdDeshacer.Visible = True
            
        bModoEdicion = True
        
    Else
                
        If sdbgTipoCom.DataChanged = True Then
        
            v = sdbgTipoCom.ActiveCell.Value
            sdbgTipoCom.SetFocus
            sdbgTipoCom.ActiveCell.Value = v
            
            bValError = False
            bAnyaError = False
            bModError = False
            
            sdbgTipoCom.Update
            
            If bValError Or bAnyaError Or bModError Then
                Exit Sub
            End If
            
        End If
        
        sdbgTipoCom.AllowAddNew = False
        sdbgTipoCom.AllowUpdate = False
        sdbgTipoCom.AllowDelete = False
                
        cmdA�adir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
        cmdRestaurar.Visible = True
        
        frmCOM.Caption = sIdiComCon
        
        cmdModoEdicion.Caption = sIdiEdicion
        
        bModoEdicion = False

        cmdRestaurar_Click
        
        
    End If
    
    sdbgTipoCom.SetFocus
    
 
End Sub

Private Sub Form_Load()
    
    ''' * Objetivo: Cargar los Tipos e iniciar
    ''' * Objetivo: el formulario
    
    CargarRecursos
    
    Me.Width = 6705
    Me.Height = 5085
    
    If Me.Top + Me.Height > frmMDI.Top + frmMDI.ScaleHeight Or Me.Left + Me.Width > frmMDI.Left + frmMDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    Show
    DoEvents
    
    
    bModoEdicion = False
    
    bCriterioCoincidenciaTotal = False
    
    cmdRestaurar_Click
    
End Sub
Private Sub Arrange()

    ''' * Objetivo: Ajustar el tamanyo y posicion de los controles
    ''' * Objetivo: al tamanyo del formulario
    
    If Height >= 900 Then sdbgTipoCom.Height = Height - 900
    If Width >= 250 Then sdbgTipoCom.Width = Width - 180
    
    'sdbgTipoCom.Columns(0).Width = sdbgTipoCom.Width * 15 / 100
    'sdbgTipoCom.Columns(1).Width = sdbgTipoCom.Width * 55 / 100
   'sdbgTipoCom.Columns(2).Width = sdbgTipoCom.Width * 30 / 100 - 550
        
    cmdModoEdicion.Left = sdbgTipoCom.Left + sdbgTipoCom.Width - cmdModoEdicion.Width
    
End Sub

Private Sub cmdRestaurar_Click()
    ''' * Objetivo: Restaurar el contenido de las grid
    ''' * Objetivo: desde la base de datos
    
    Me.Caption = sIdiComCon

    
    Set oTiposCom = Nothing
    Set oTiposCom = g_oRaiz.Generar_CTiposComunicacion
    oTiposCom.CargarTodosLosTiposDesde 32000
    
    CargarGridTipos
     
    sdbgTipoCom.SetFocus
    
    bCriterioCoincidenciaTotal = False
    
End Sub
Private Sub CargarGridTipos()
Dim oTipo As CTipoComunicacion

    sdbgTipoCom.RemoveAll
    If Not oTiposCom Is Nothing Then
    
       For Each oTipo In oTiposCom
            
            sdbgTipoCom.AddItem oTipo.Codigo & Chr(9) & oTipo.Denominacion & Chr(9) & oTipo.ID
    
        Next
    
    End If
End Sub

Private Sub Form_Resize()

    ''' * Objetivo: Adecuar los controles
    
    Arrange
    
End Sub


Private Sub Form_Unload(Cancel As Integer)

    ''' * Objetivo: Descargar el formulario si no
    ''' * Objetivo: hay cambios pendientes
    
    Dim v As Variant
    
    If sdbgTipoCom.DataChanged = True Then
    
        v = sdbgTipoCom.ActiveCell.Value
        sdbgTipoCom.SetFocus
        sdbgTipoCom.ActiveCell.Value = v
        
        bValError = False
        bAnyaError = False
        bModError = False
                
        sdbgTipoCom.Update
            
        If bValError Or bAnyaError Or bModError Then
            Cancel = True
        End If
        
    End If
    
    Set oTiposCom = Nothing
    Set oTipoComSelec = Nothing

End Sub
Private Sub sdbgTipoCom_AfterDelete(RtnDispErrMsg As Integer)
    
    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    
    sdbgTipoCom.SetFocus
    sdbgTipoCom.Bookmark = sdbgTipoCom.RowBookmark(sdbgTipoCom.Row)
    
End Sub
Private Sub sdbgTipoCom_AfterInsert(RtnDispErrMsg As Integer)
    
    If Not bModoEdicion Then Exit Sub
    
    
    If bAnyaError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
    End If

    bAnyadir = False

    Exit Sub
    
End Sub
Private Sub sdbgTipoCom_AfterUpdate(RtnDispErrMsg As Integer)

    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If Not bModoEdicion Then Exit Sub
        
   

    
    If bAnyaError = False And bModError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
    End If
    
End Sub
Private Sub sdbgTipoCom_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    Dim Ind As Integer
    Dim oError As CTESError

    ''' * Objetivo: Confirmacion antes de eliminar
    
    Dim iRespuesta As Integer
    
    DispPromptMsg = 0
    
    If sdbgTipoCom.IsAddRow Or bAnyadir Then
        Cancel = True
        Exit Sub
    End If
    
    iRespuesta = basMensajes.PreguntaEliminar(sIdiPTipoCom & sdbgTipoCom.Columns(0).Value & " (" & sdbgTipoCom.Columns(1).Value & ")")
    
    If iRespuesta = vbNo Then
        Cancel = True
        Exit Sub
    End If
    
    If sdbgTipoCom.Rows > 0 Then
        sdbgTipoCom.SelBookmarks.Add sdbgTipoCom.Bookmark
        '''Eliminar de Base de datos (columna 2=ID)
        Set oError = oTiposCom.Item(sdbgTipoCom.Columns(2).Value).EliminarRegistro
        If oError.NumError <> 0 Then
            basErrores.TratarError oError
            Cancel = True
            Exit Sub
        End If
        
        ''' Eliminar de la coleccion
        Ind = Val(sdbgTipoCom.Bookmark)
        oTiposCom.Remove (sdbgTipoCom.Columns(2).CellText(Ind))
        
    End If

    
End Sub

Private Sub sdbgTipoCom_BeforeInsert(Cancel As Integer)
Dim oError As CTESError
    
    If Not bModoEdicion Then Exit Sub
        
    bAnyadir = True
    bAnyaError = False
    

End Sub

Private Sub sdbgTipoCom_BeforeUpdate(Cancel As Integer)
Dim oError As CTESError
Dim oTipo As CTipoComunicacion
    ''' * Objetivo: Validar los datos
    

    
    bValError = False
    bModError = False
    Cancel = False
    
    If Trim(sdbgTipoCom.Columns(0).Value) = "" Then
        basMensajes.NoValido sIdiCodigo
        GoTo Salir
    End If
    
    If Trim(sdbgTipoCom.Columns(1).Value) = "" Then
        basMensajes.NoValida sIdiDenominacion
        GoTo Salir
    End If
    
    oTiposCom.CargarTodosLosTiposDesde 1, , sdbgTipoCom.Columns(0).Text, , True
    If oTiposCom.Count > 0 Then
            For Each oTipo In oTiposCom
                If oTipo.ID <> sdbgTipoCom.Columns(2).Value Or (bAnyadir And sdbgTipoCom.IsAddRow) Then
                    basMensajes.DatoDuplicado sIdiCodigo
                    GoTo Salir
                End If
            Next
            
    End If
    
    
    Set oTipoComSelec = Nothing
    Set oTipoComSelec = g_oRaiz.Generar_CTipoComunicacion
    
    oTipoComSelec.Codigo = sdbgTipoCom.Columns(0).Text
    oTipoComSelec.Denominacion = sdbgTipoCom.Columns(1).Text
    
    If bAnyadir And sdbgTipoCom.IsAddRow Then
        Set oError = oTipoComSelec.AnyadirRegistro
    
        If oError.NumError <> 0 Then
            basErrores.TratarError oError
            bAnyaError = True
            Cancel = True
            sdbgTipoCom.SetFocus
            Exit Sub
        Else
            sdbgTipoCom.Columns(2).Value = oTipoComSelec.ID
            bAnyadir = False
        End If
    
    Else

        oTipoComSelec.ID = sdbgTipoCom.Columns(2).Value
    
        Set oError = oTipoComSelec.ModificarRegistro
    
        If oError.NumError <> 0 Then
            basErrores.TratarError oError
            bModError = True
            Cancel = True
        End If
    End If
    
    oTiposCom.CargarTodosLosTiposDesde 32000

    Exit Sub
    
Salir:
    Cancel = True
    bValError = Cancel
    sdbgTipoCom.SetFocus
    'bAnyadir = False
End Sub
Private Sub sdbgTipoCom_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    Dim oError As CTESError
    
    If cmdDeshacer.Enabled = False Then
    
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
    
    End If
    
    If bModoEdicion And Not sdbgTipoCom.IsAddRow Then
    
        Set oTipoComSelec = Nothing
        Set oTipoComSelec = oTiposCom.Item(CStr(sdbgTipoCom.Bookmark))
    End If
    
End Sub
Private Sub sdbgTipoCom_HeadClick(ByVal ColIndex As Integer)

    ''' * Objetivo: Ordenar el grid segun la columna
    
    Dim sCodigo As String
    Dim sDenominacion As String
    Dim lCodigo As Integer
    Dim lDenominacion As Integer
    Dim sHeadCaption As String
    
    'If bModoEdicion Then Exit Sub
    
    sHeadCaption = sdbgTipoCom.Columns(ColIndex).Caption
    sdbgTipoCom.Columns(ColIndex).Caption = sIdiOrdenando
    
    
    Set oTiposCom = Nothing
    Set oTiposCom = g_oRaiz.Generar_CTiposComunicacion
        
        Select Case ColIndex
        Case 0
            oTiposCom.CargarTodosLosTiposDesde 32000, False
        Case 1
            oTiposCom.CargarTodosLosTiposDesde 32000, True
        End Select
    
    
    CargarGridTipos
    sdbgTipoCom.Columns(ColIndex).Caption = sHeadCaption

End Sub


Private Sub sdbgTipoCom_KeyPress(KeyAscii As Integer)

    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
    
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgTipoCom.DataChanged = False Then
            
            sdbgTipoCom.CancelUpdate
            sdbgTipoCom.DataChanged = False
            
            If Not oTipoComSelec Is Nothing Then
                'cancelar edicion
                Set oTipoComSelec = Nothing
            End If
            
            If Not sdbgTipoCom.IsAddRow Then
            
                cmdA�adir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
            
            Else
            
                cmdA�adir.Enabled = False
                cmdEliminar.Enabled = False
                cmdDeshacer.Enabled = True
            
            End If
                    
       End If
       
    End If
    
End Sub

Private Sub sdbgTipoCom_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    
    If Not sdbgTipoCom.IsAddRow Then
        If sdbgTipoCom.DataChanged = False Then
            cmdA�adir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
        End If
    Else
        If sdbgTipoCom.DataChanged = True Then cmdDeshacer.Enabled = True
        If Not IsNull(LastRow) Then
            If Val(LastRow) <> Val(sdbgTipoCom.Row) Then
                sdbgTipoCom.Col = 0
                cmdDeshacer.Enabled = False
            End If
        End If
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        
    End If
         
    
End Sub


Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_COM, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdA�adir.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdDeshacer.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdEliminar.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdModoEdicion.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdRestaurar.Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgTipoCom.Columns(0).Caption = ador(0).Value
        sIdiCodigo = ador(0).Value
        ador.MoveNext
        Me.sdbgTipoCom.Columns(1).Caption = ador(0).Value
        sIdiDenominacion = ador(0).Value
        ador.MoveNext
        sIdiTipoCom = ador(0).Value
        ador.MoveNext
        sIdiConsulta = ador(0).Value
        ador.MoveNext
        sIdiComCon = ador(0).Value
        ador.MoveNext
        sIdiEdicion = ador(0).Value
        ador.MoveNext
        sIdiPTipoCom = ador(0).Value
        ador.MoveNext
        sIdiOrdenando = ador(0).Value

        
        ador.Close
    End If
    
    Set ador = Nothing
           
End Sub

