VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmListados 
   Caption         =   "Listados"
   ClientHeight    =   4575
   ClientLeft      =   5220
   ClientTop       =   2130
   ClientWidth     =   5730
   Icon            =   "frmListados.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   4575
   ScaleWidth      =   5730
   Begin VB.CommandButton cmdSeleccionar 
      Caption         =   "Seleccionar"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4560
      TabIndex        =   1
      Top             =   4170
      Width           =   1155
   End
   Begin MSComctlLib.TreeView tvwListados 
      Height          =   4095
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5715
      _ExtentX        =   10081
      _ExtentY        =   7223
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList imglstListados 
      Left            =   210
      Top             =   4065
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   16777215
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      UseMaskColor    =   0   'False
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmListados.frx":0CB2
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmListados.frx":0DC4
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmListados.frx":0ED6
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmListados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public ofrmLstEnlaceProvePremium1 As frmLstEnlaceProvePremium
Public ofrmLstEnlaceProvePremium2 As frmLstEnlaceProvePremium

Private sIdiListados As String
Private sIdiMto As String
Private sIdiMon As String
Private sIdiAct As String
Private sIdiGest As String
Private sIdiComp As String
Private sIdiProcPub As String
Private sIdiEnProve As String
Private sIdiEnComp As String
Private sIdiEnPorProve As String
Private sIdiInf As String
Private sIdiVolAdjProve As String
Private sIdiVolAdjGen As String
Private sIdiOpc As String
Private sIdiCoorMon As String
Private sIdiCoorPai As String
Private sIdiCoorProvi As String
Private sIdiCoorMonCen As String
Private sIdiRegComunic As String
Private sIdiAutorizacionCIAS As String


Private Sub Form_Resize()
    If Me.Height > 885 And Me.Width > 135 Then
        tvwListados.Width = Me.Width - 135
        tvwListados.Height = Me.Height - 885
        cmdSeleccionar.Left = tvwListados.Width - cmdSeleccionar.Width
        cmdSeleccionar.Top = tvwListados.Height + 80
    End If
End Sub

Private Sub tvwListados_NodeClick(ByVal Node As MSComctlLib.Node)
    Dim nod As MSComctlLib.Node

    For Each nod In tvwListados.Nodes
        If nod.image = 3 Then
            nod.Bold = False
            cmdSeleccionar.Enabled = False
        End If
    Next
    If Node.image = 3 Then
        Node.Bold = True
        cmdSeleccionar.Enabled = True
    End If
    

End Sub

Private Sub cmdSeleccionar_Click()
Dim DenMonCentral As Variant
Dim ador As ador.Recordset
    Set ador = g_oRaiz.CodMonedaCentral
    DenMonCentral = ador(1)
    If IsNull(DenMonCentral) Then
        DenMonCentral = ""
    End If
    ador.Close
    Set ador = Nothing

    Select Case tvwListados.SelectedItem.Key
        Case "A1B1"
            frmLstMon.WindowState = vbNormal
            frmLstMon.SetFocus
            
        Case "A1B2"
            frmLstACT.g_sIdioma = g_udtParametrosGenerales.g_sIdioma
            frmLstACT.WindowState = vbNormal
            frmLstACT.SetFocus

        Case "A2B1"
            frmLstCIAS.g_sOrigen = "LstCIAS"
            frmLstCIAS.WindowState = vbNormal
            frmLstCIAS.SetFocus
            
        Case "A2B3"
            frmLstProcPub.WindowState = vbNormal
            frmLstProcPub.SetFocus
    
        Case "A2B2C1"
            Set ofrmLstEnlaceProvePremium1 = New frmLstEnlaceProvePremium
            ofrmLstEnlaceProvePremium1.sOrigen = "A2B2C1"
            ofrmLstEnlaceProvePremium1.WindowState = vbNormal
            ofrmLstEnlaceProvePremium1.SetFocus
        
        Case "A2B2C2"
            Set ofrmLstEnlaceProvePremium2 = New frmLstEnlaceProvePremium
            ofrmLstEnlaceProvePremium2.sOrigen = "A2B2C2"
            ofrmLstEnlaceProvePremium2.WindowState = vbNormal
            ofrmLstEnlaceProvePremium2.SetFocus
        
        Case "A2B4"
            frmLstRegComunic.WindowState = vbNormal
            frmLstRegComunic.SetFocus
        Case "A2B5"
            frmLstCIAS.g_sOrigen = "LstAutorizacionCIAS"
            frmLstCIAS.WindowState = vbNormal
            frmLstCIAS.SetFocus
        Case "A3B1"
            frmLstInfVolProv.WindowState = vbNormal
            frmLstInfVolProv.DenMonCentral = DenMonCentral
            frmLstInfVolProv.Show vbModal
        Case "A3B2"
            frmLstInfVol.WindowState = vbNormal
            frmLstInfVol.DenMonCentral = DenMonCentral
            frmLstInfVol.Show vbModal
        
        Case "A4B1"
            frmLstCoorMon.WindowState = vbNormal
            frmLstCoorMon.SetFocus
            
        Case "A4B2"
            frmLstCoorPai.WindowState = vbNormal
            frmLstCoorPai.SetFocus
            
        Case "A4B3"
            frmLstCoorProv.WindowState = vbNormal
            frmLstCoorProv.SetFocus
            
        Case "A4B4"
            frmLstMonCentr.WindowState = vbNormal
            frmLstMonCentr.SetFocus
    End Select
End Sub

Private Sub Form_Load()
    Me.Height = 4980
    Me.Width = 5850
    CargarRecursos
    
    ''' Generar treeview teniendo en cuenta la seguridad
    
    tvwListados.Indentation = 200
    tvwListados.ImageList = imglstListados
    tvwListados.Nodes.Add , tvwFirst, "A", sIdiListados
    tvwListados.Nodes("A").image = 0
    tvwListados.Nodes("A").ExpandedImage = 2
    tvwListados.Nodes("A").Expanded = True
    
    tvwListados.Nodes.Add "A", tvwChild, "A1", sIdiMto
    tvwListados.Nodes("A1").image = 1
    tvwListados.Nodes("A1").ExpandedImage = 2
    
    If g_udtParametrosGenerales.g_bUnaCompradora = False Then
        tvwListados.Nodes.Add "A1", tvwChild, "A1B1", sIdiMon
        tvwListados.Nodes("A1B1").image = 3
    End If
    
    tvwListados.Nodes.Add "A1", tvwChild, "A1B2", sIdiAct
    tvwListados.Nodes("A1B2").image = 3
    
    tvwListados.Nodes.Add "A", tvwChild, "A2", sIdiGest
    tvwListados.Nodes("A2").image = 1
    tvwListados.Nodes("A2").ExpandedImage = 2
    
    'Compañías
    tvwListados.Nodes.Add "A2", tvwChild, "A2B1", sIdiComp
    tvwListados.Nodes("A2B1").image = 3
    'Compañías
    tvwListados.Nodes.Add "A2", tvwChild, "A2B5", sIdiAutorizacionCIAS
    tvwListados.Nodes("A2B5").image = 3
    'Procesos publicados
    tvwListados.Nodes.Add "A2", tvwChild, "A2B3", sIdiProcPub
    tvwListados.Nodes("A2B3").image = 3
    
    'Premium
    'condicion para que aparezca
    If g_udtParametrosGenerales.g_bUnaCompradora = False And g_udtParametrosGenerales.g_bPremium Then
        tvwListados.Nodes.Add "A2", tvwChild, "A2B2", sIdiEnProve
        tvwListados.Nodes("A2B2").image = 1
        tvwListados.Nodes("A2B2").ExpandedImage = 2
        tvwListados.Nodes.Add "A2B2", tvwChild, "A2B2C1", sIdiEnComp
        tvwListados.Nodes("A2B2C1").image = 3
        tvwListados.Nodes.Add "A2B2", tvwChild, "A2B2C2", sIdiEnPorProve
        tvwListados.Nodes("A2B2C2").image = 3
    End If
    tvwListados.Nodes.Add "A2", tvwChild, "A2B4", sIdiRegComunic
    tvwListados.Nodes("A2B4").image = 3
    
    
    tvwListados.Nodes.Add "A", tvwChild, "A3", sIdiInf
    tvwListados.Nodes("A3").image = 1
    tvwListados.Nodes("A3").ExpandedImage = 2
    If g_udtParametrosGenerales.g_bUnaCompradora = False Then
        tvwListados.Nodes.Add "A3", tvwChild, "A3B1", sIdiVolAdjProve
        tvwListados.Nodes("A3B1").image = 3
    End If
    tvwListados.Nodes.Add "A3", tvwChild, "A3B2", sIdiVolAdjGen
    tvwListados.Nodes("A3B2").image = 3
    
    
    'Coordinación de tablas:
    If g_udtParametrosGenerales.g_bUnaCompradora = False Then
        tvwListados.Nodes.Add "A", tvwChild, "A4", sIdiOpc
        tvwListados.Nodes("A4").image = 1
        tvwListados.Nodes("A4").ExpandedImage = 2
    
        tvwListados.Nodes.Add "A4", tvwChild, "A4B1", sIdiCoorMon
        tvwListados.Nodes("A4B1").image = 3
    
        tvwListados.Nodes.Add "A4", tvwChild, "A4B2", sIdiCoorPai
        tvwListados.Nodes("A4B2").image = 3
        
        tvwListados.Nodes.Add "A4", tvwChild, "A4B3", sIdiCoorProvi
        tvwListados.Nodes("A4B3").image = 3
    
        tvwListados.Nodes.Add "A4", tvwChild, "A4B4", sIdiCoorMonCen
        tvwListados.Nodes("A4B4").image = 3
    End If
    
End Sub
Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_LISTADOS, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        sIdiListados = ador(0).Value
        ador.MoveNext
        Me.cmdSeleccionar.Caption = ador(0).Value
        ador.MoveNext
        sIdiMto = ador(0).Value
        ador.MoveNext
        sIdiMon = ador(0).Value
        ador.MoveNext
        sIdiAct = ador(0).Value
        ador.MoveNext
        sIdiGest = ador(0).Value
        ador.MoveNext
        sIdiComp = ador(0).Value
        ador.MoveNext
        sIdiProcPub = ador(0).Value
        ador.MoveNext
        sIdiEnProve = ador(0).Value
        ador.MoveNext
        sIdiEnComp = ador(0).Value
        ador.MoveNext
        sIdiEnPorProve = ador(0).Value
        ador.MoveNext
        sIdiInf = ador(0).Value
        ador.MoveNext
        sIdiVolAdjProve = ador(0).Value
        ador.MoveNext
        sIdiVolAdjGen = ador(0).Value
        ador.MoveNext
        sIdiOpc = ador(0).Value
        ador.MoveNext
        sIdiCoorMon = ador(0).Value
        ador.MoveNext
        sIdiCoorPai = ador(0).Value
        ador.MoveNext
        sIdiCoorProvi = ador(0).Value
        ador.MoveNext
        sIdiCoorMonCen = ador(0).Value
        ador.MoveNext
        sIdiRegComunic = ador(0).Value
        ador.MoveNext
        sIdiAutorizacionCIAS = ador(0).Value
        ador.Close
    End If
    
    Set ador = Nothing
           
End Sub

