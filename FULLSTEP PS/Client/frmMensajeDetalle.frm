VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmMensajeDetalle 
   Caption         =   "DMensaje enviado el xxx"
   ClientHeight    =   7800
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9600
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmMensajeDetalle.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7800
   ScaleWidth      =   9600
   StartUpPosition =   3  'Windows Default
   Begin SHDocVwCtl.WebBrowser wbMyBrowser 
      Height          =   4570
      Left            =   0
      TabIndex        =   17
      Top             =   3225
      Visible         =   0   'False
      Width           =   9555
      ExtentX         =   16854
      ExtentY         =   8061
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   "http:///"
   End
   Begin VB.TextBox txtMensaje 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4570
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   14
      Top             =   3225
      Width           =   9555
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3150
      Left            =   0
      TabIndex        =   0
      Top             =   20
      Width           =   9555
      Begin VB.CommandButton cmdAdjuntar 
         Caption         =   "Adjuntar"
         Height          =   705
         Left            =   8400
         Picture         =   "frmMensajeDetalle.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   1995
         UseMaskColor    =   -1  'True
         Width           =   945
      End
      Begin VB.CommandButton cmdEnviar 
         Appearance      =   0  'Flat
         Caption         =   "Enviar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   120
         Picture         =   "frmMensajeDetalle.frx":05FC
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   2350
         Width           =   915
      End
      Begin VB.TextBox txtAsunto 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2120
         TabIndex        =   6
         Top             =   1650
         Width           =   7245
      End
      Begin VB.TextBox txtDestino 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2120
         TabIndex        =   5
         Top             =   210
         Width           =   7245
      End
      Begin VB.TextBox txtCc 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2120
         TabIndex        =   4
         Top             =   570
         Width           =   7245
      End
      Begin VB.TextBox txtBcc 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2120
         TabIndex        =   3
         Top             =   930
         Width           =   7245
      End
      Begin VB.CheckBox chkAcuseRecibo 
         Caption         =   "DPedir acuse de recibo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2120
         TabIndex        =   2
         Top             =   2760
         Width           =   6255
      End
      Begin VB.TextBox txtRespuestaA 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2120
         TabIndex        =   1
         Top             =   1290
         Width           =   7245
      End
      Begin MSComctlLib.ListView lstvwAdjuntos 
         Height          =   675
         Left            =   2120
         TabIndex        =   7
         Top             =   2010
         Width           =   6245
         _ExtentX        =   11033
         _ExtentY        =   1191
         View            =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "FicheroAdjunto"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.Label lblAdjuntos 
         Caption         =   "Archivos adjuntos:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   120
         TabIndex        =   13
         Top             =   2070
         Width           =   1960
      End
      Begin VB.Label lblAsunto 
         Caption         =   "Asunto:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   1710
         Width           =   1700
      End
      Begin VB.Label lblDestino 
         Caption         =   "Para:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   11
         Top             =   270
         Width           =   1700
      End
      Begin VB.Label lblCc 
         Caption         =   "Cc:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   630
         Width           =   1700
      End
      Begin VB.Label lblBcc 
         Caption         =   "Cco:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   9
         Top             =   990
         Width           =   1700
      End
      Begin VB.Label lblRespuestaA 
         Caption         =   "Enviar la respuesta a:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   1350
         Width           =   1960
      End
   End
   Begin MSComDlg.CommonDialog cmmdAdjuntar 
      Left            =   7260
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7920
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMensajeDetalle.frx":0906
            Key             =   "ADJ"
            Object.Tag             =   "ADJ"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmMensajeDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oRegistro As CRegistroEmail
Public g_bLock As Boolean
Public g_sCodCia As String
Private sTemp As String
Private iNumElmtos As Integer
Private oFos As Scripting.FileSystemObject

'Variables para Multilenguaje
Private sIdiSelAdjuntos As String
Private sIdiTodos As String
Private sIdiDirNoValida As String
Private sCaption As String
Private sCuerpoName As String

Private bCargando As Boolean

Private Sub cmdAdjuntar_Click()
Dim sFileName As String
Dim sFileTitle As String
Dim oFile As File
On Error GoTo ErrAdjuntar
    
    cmmdAdjuntar.CancelError = True
    cmmdAdjuntar.DialogTitle = sIdiSelAdjuntos
    cmmdAdjuntar.Flags = cdlOFNHideReadOnly
    cmmdAdjuntar.Filter = sIdiTodos & "|*.*"
    cmmdAdjuntar.ShowOpen
    
    sFileName = cmmdAdjuntar.FileName
    sFileTitle = cmmdAdjuntar.FileTitle
    If sFileName = "" Then Exit Sub
    iNumElmtos = iNumElmtos + 1
    lstvwAdjuntos.ListItems.Add , "NEW" & CStr(iNumElmtos), sFileTitle, , "ADJ"
    
    Set oFile = oFos.GetFile(sFileName)
    oFile.Copy sTemp & sFileTitle, True
    
Exit Sub
    
ErrAdjuntar:
    
    Exit Sub
End Sub

''' <summary>
''' Envia un correo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdEnviar_Click()
Dim Item As MSComctlLib.ListItem
Dim errormail As TipoErrorSummit
Dim arAdjuntos() As String
Dim i As Integer

    If txtDestino = "" Then
        basMensajes.errormail sIdiDirNoValida
        Exit Sub
    End If
    cmdEnviar.Enabled = False
    Screen.MousePointer = vbHourglass
    
    ReDim arAdjuntos(0)
    i = 0
    For Each Item In lstvwAdjuntos.ListItems
        arAdjuntos(i) = Item.Text
        ReDim Preserve arAdjuntos(i + 1)
        i = i + 1
    Next
    
    If g_oMailSender Is Nothing Then
        DoEvents
        IniciarSesionMail
    End If
    
    
    If i > 0 Then
        errormail = EnviarMensaje(txtDestino.Text, txtAsunto.Text, IIf(g_oRegistro.Tipo = 1, g_oRegistro.Cuerpo, txtMensaje.Text), arAdjuntos, Trim(txtCC.Text), Trim(txtBcc.Text), SQLBinaryToBoolean(chkAcuseRecibo.Value), SQLBinaryToBoolean(g_oRegistro.Tipo), Trim(txtRespuestaA.Text), g_oRegistro.Cia)
    Else
        errormail = EnviarMensaje(txtDestino.Text, txtAsunto.Text, IIf(g_oRegistro.Tipo = 1, g_oRegistro.Cuerpo, txtMensaje.Text), , Trim(txtCC.Text), Trim(txtBcc.Text), SQLBinaryToBoolean(chkAcuseRecibo.Value), SQLBinaryToBoolean(g_oRegistro.Tipo), Trim(txtRespuestaA.Text), g_oRegistro.Cia)
    End If
    
    If errormail.NumError <> TESnoerror Then
        basMensajes.ErrorOriginal errormail
    Else
        g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.RegMail_Reenviar_Notif_Cia, g_sLitRegAccion_SPA(8) & ": " & g_sCodCia & "; " & g_sLitRegAccion_SPA(36) & " " & txtAsunto.Text, _
                                        g_sLitRegAccion_ENG(8) & ": " & g_sCodCia & "; " & g_sLitRegAccion_ENG(36) & " " & txtAsunto.Text, _
                                        g_sLitRegAccion_FRA(8) & ": " & g_sCodCia & "; " & g_sLitRegAccion_FRA(36) & " " & txtAsunto.Text, _
                                        g_sLitRegAccion_GER(8) & ": " & g_sCodCia & "; " & g_sLitRegAccion_GER(36) & " " & txtAsunto.Text, _
                                        g_oRegistro.Id
    End If
    
    g_oMailSender.FuerzaFinalizeSmtpClient
    Set g_oMailSender = Nothing

    
    cmdEnviar.Enabled = True
    Screen.MousePointer = vbNormal
    
    Unload Me
    
End Sub

Private Sub Form_Load()
    Screen.MousePointer = vbNormal
    CargarRecursos
    
    sTemp = basUtilidades.DevolverPathFichTemp
    Set oFos = New Scripting.FileSystemObject
    
    CargarDatosEmail
    If g_bLock Then
        cmdEnviar.Visible = False
        cmdAdjuntar.Visible = False
        Frame1.Enabled = False
        txtMensaje.Locked = True
    End If
    
End Sub

Private Sub Form_Resize()
    'Redimensiona el formulario:
    If Me.Width < 4000 Then Exit Sub
    If Me.Height < 4000 Then Exit Sub
    
    txtMensaje.Height = Me.Height - txtMensaje.Top - 515
    txtMensaje.Width = Me.Width - 165
    
    wbMyBrowser.Height = Me.Height - wbMyBrowser.Top - 530
    wbMyBrowser.Width = Me.Width - 150
    
    Frame1.Width = Me.Width - 165
    txtAsunto.Width = Me.Width - 2475
    txtBcc.Width = Me.Width - 2475
    txtCC.Width = Me.Width - 2475
    txtDestino.Width = Me.Width - 2475
    txtRespuestaA.Width = Me.Width - 2460
    lstvwAdjuntos.Width = Me.Width - 3475
    
    cmdAdjuntar.Left = lstvwAdjuntos.Left + lstvwAdjuntos.Width + 35
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim Item As MSComctlLib.ListItem
Dim bErroresEnElBorrado As Boolean

On Error GoTo error:

    Screen.MousePointer = vbHourglass
    
    For Each Item In lstvwAdjuntos.ListItems
        If oFos.FileExists(sTemp & Item.Text) Then
            oFos.DeleteFile sTemp & Item.Text, True
        End If
    Next
    
    If g_oRegistro.Tipo = 1 Then
        oFos.DeleteFile sTemp & sCuerpoName, True
    End If
    
    Set g_oRegistro = Nothing
    Set oFos = Nothing
    
    Screen.MousePointer = vbDefault
    Exit Sub

error:
    
    Select Case Err.Number
        
        Case 1
            
        Case 2
            bErroresEnElBorrado = True
    End Select
    
    Resume Next
    
End Sub


Private Sub lstvwAdjuntos_DblClick()
Dim Item As MSComctlLib.ListItem
Dim sFileName As String

    Set Item = lstvwAdjuntos.SelectedItem
    
    If Item Is Nothing Then
        basMensajes.SeleccioneFichero
    Else
        sFileName = sTemp & Item.Text
        'Lanzamos la aplicacion
        ShellExecute frmMDI.hWnd, "Open", sFileName, 0&, "C:\", 1
    End If
    
End Sub

Private Sub lstvwAdjuntos_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 Then
    ' suprimir
        If lstvwAdjuntos.SelectedItem Is Nothing Then
            basMensajes.SeleccioneFichero
        Else
            lstvwAdjuntos.ListItems.Remove (CStr(lstvwAdjuntos.SelectedItem.Key))
            lstvwAdjuntos.Refresh
        End If
    End If
End Sub

Private Sub CargarRecursos()

Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_MENSAJE, g_udtParametrosGenerales.g_sIdioma)
    
    If Not ador Is Nothing Then
        
        ador.MoveNext
        ador.MoveNext
        ador.MoveNext
        lblDestino.Caption = ador(0).Value
        ador.MoveNext
        lblAsunto.Caption = ador(0).Value
        ador.MoveNext
        lblAdjuntos.Caption = ador(0).Value
        ador.MoveNext
        cmdAdjuntar.Caption = ador(0).Value
        ador.MoveNext
        
        sIdiSelAdjuntos = ador(0).Value
        ador.MoveNext
        sIdiTodos = ador(0).Value
        ador.MoveNext
        sIdiDirNoValida = ador(0).Value
        ador.MoveNext
        lblBcc.Caption = ador(0).Value
        ador.MoveNext
        chkAcuseRecibo.Caption = ador(0).Value

        ador.MoveNext
        lblCc.Caption = ador(0).Value
        ador.MoveNext
        lblRespuestaA.Caption = ador(0).Value
        
        ador.MoveNext
        sCaption = ador(0).Value
        ador.MoveNext
        cmdEnviar.Caption = ador(0).Value
        
        ador.Close
    
    End If


   Set ador = Nothing



End Sub

Private Sub CargarDatosEmail()
    Dim oAdjun As CAdjuntoEmail
    Dim sFileName As String
    Dim oError As CTESError
    Dim iDataFile As Integer
    Dim lInit As Long
    Dim lSize As Long
    Dim bytChunk() As Byte
    
    On Error GoTo Cancelar
    
    'Carga los datos correspondientes:
    Me.Caption = sCaption & " " & Format(g_oRegistro.Fecha, "Short Date")
    
    txtDestino.Text = NullToStr(g_oRegistro.Para)
    txtCC.Text = NullToStr(g_oRegistro.CC)
    txtBcc.Text = NullToStr(g_oRegistro.CCO)
    txtAsunto.Text = NullToStr(g_oRegistro.Subject)
    
    g_oRegistro.CargarDetalleMensaje
    If g_oRegistro.Tipo = 1 Then
        'Es de tipo HTML
        bCargando = True
        wbMyBrowser.Visible = True
        txtMensaje.Visible = False
        
        sCuerpoName = "Cuerpo" & CStr(Year(Now())) & CStr(Month(Now())) & CStr(Day(Now())) & CStr(Hour(Now())) & CStr(Minute(Now())) & CStr(Second(Now())) & ".htm"
        iDataFile = 1
        Open sTemp & sCuerpoName For Binary Access Write As iDataFile
        Put iDataFile, , CStr(g_oRegistro.Cuerpo)
    
        wbMyBrowser.Navigate2 sTemp & sCuerpoName
        Close iDataFile
        iDataFile = 0
        bCargando = False
        
    Else
        'Es de tipo texto
        wbMyBrowser.Visible = False
        txtMensaje.Visible = True
        txtMensaje.Text = g_oRegistro.Cuerpo
    End If
    
    txtRespuestaA.Text = NullToStr(g_oRegistro.ResponderA)
    If g_oRegistro.AcuseRecibo = 1 Then
        chkAcuseRecibo.Value = vbChecked
    Else
        chkAcuseRecibo.Value = vbUnchecked
    End If
    
    'carga los adjuntos:
    If g_oRegistro.HayAdjuntos = True Then
        g_oRegistro.CargarTodosLosAdjuntos
        
        If Not g_oRegistro.Adjuntos Is Nothing Then
            iNumElmtos = 1
            'Los a�ade al listview
            For Each oAdjun In g_oRegistro.Adjuntos
                lstvwAdjuntos.ListItems.Add , "ADJ" & CStr(iNumElmtos), oAdjun.Nombre, , "ADJ"
            
                'Copia el archivo en temporal para despu�s enviarlo desde all�:
                sFileName = sTemp & oAdjun.Nombre
                Set oError = oAdjun.ComenzarLecturaData
                If oError.NumError <> 0 Then
                    basErrores.TratarError oError
                    Exit Sub
                End If
                
                iDataFile = 1
            
                'Abrimos el fichero para escritura binaria
                Open sFileName For Binary Access Write As iDataFile
                           
                lInit = 0
                lSize = giChunkSize
                
                While lInit < oAdjun.DataSize
                    bytChunk = oAdjun.ReadData(lInit, lSize)
                    Put iDataFile, , bytChunk
                    lInit = lInit + lSize + 1
                    DoEvents
                Wend
                
                'Verificar escritura
                If LOF(iDataFile) <> oAdjun.DataSize Then
                    basMensajes.ErrorDeEscrituraEnDisco
                End If
                Close iDataFile
                iDataFile = 0
                
                iNumElmtos = iNumElmtos + 1
            Next
        End If
    End If
    
Cancelar:

    If iDataFile <> 0 Then
        Close iDataFile
        iDataFile = 0
    End If
    
End Sub

Private Sub wbMyBrowser_BeforeNavigate2(ByVal pDisp As Object, URL As Variant, Flags As Variant, TargetFrameName As Variant, PostData As Variant, Headers As Variant, Cancel As Boolean)
    If bCargando = True Then Exit Sub
    If URL <> sTemp & "Cuerpo.htm" Then Cancel = True
End Sub
