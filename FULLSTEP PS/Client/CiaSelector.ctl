VERSION 5.00
Begin VB.UserControl CiaSelector 
   BackStyle       =   0  'Transparent
   ClientHeight    =   480
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   375
   ControlContainer=   -1  'True
   ScaleHeight     =   480
   ScaleWidth      =   375
   Begin VB.CheckBox Check1 
      Caption         =   "T"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   0
      Width           =   285
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "mnuMenu"
      Visible         =   0   'False
      Begin VB.Menu mnuPend 
         Caption         =   "Compa�ias con solicitudes pendientes"
      End
      Begin VB.Menu mnuTodas 
         Caption         =   "Todas las compa��as"
      End
   End
End
Attribute VB_Name = "CiaSelector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Enum PSSeleccion
    PSPendientes = 0
    PSTodos = 1
End Enum

Private bShow As Boolean
Private mvarAbrevPendientes As String
Private mvarAbrevTodos As String

Private bModoExtendido As Boolean
Private iSeleccion As PSSeleccion

Public Event Click(ByVal opcion As PSSeleccion)

Public Property Get Seleccion() As Integer
    Seleccion = iSeleccion
End Property
Public Property Let Seleccion(ByVal iSel As Integer)
    
    Select Case iSel
        Case 0
                Check1.Caption = mvarAbrevPendientes
                iSeleccion = 0
        Case 1
                Check1.Caption = mvarAbrevTodos
                iSeleccion = 1
    End Select
    
    Check1.Value = vbUnchecked
    Check1.Refresh
        
End Property

Private Sub Check1_Click()
    
    If bShow Then
        PopupMenu mnuMenu
    End If
    
End Sub

Private Sub mnuTodas_Click()
    bShow = True
    iSeleccion = PSTodos
    Check1.Value = vbUnchecked
    Check1.Caption = mvarAbrevTodos
    RaiseEvent Click(PSTodos)
    
End Sub

Private Sub mnuPend_Click()
    bShow = True
    iSeleccion = PSPendientes
    Check1.Value = vbUnchecked
    Check1.Caption = mvarAbrevPendientes
    RaiseEvent Click(PSPendientes)
End Sub

Private Sub UserControl_Initialize()
    bShow = True
    iSeleccion = 1
    CargarRecursos
End Sub

Private Sub UserControl_LostFocus()
    
    If Check1.Value = vbChecked Then
        bShow = False
        Check1.Value = vbUnchecked
    End If
    
End Sub

Public Property Let AbrevParaTodos(ByVal Den As String)
    mvarAbrevTodos = Den
End Property

Public Property Let AbrevParaPendientes(ByVal Den As String)
    mvarAbrevPendientes = Den
End Property

'Public Property Let DenParaTodos(ByVal Den As String)
    'mnuTodas.Caption = Den
'End Property

'Public Property Let DenParaPendientes(ByVal Den As String)
    'mnuPend.Caption = Den
'End Property

Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(CTL_CIASELECTOR, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        mnuPend.Caption = ador(0).Value
        ador.MoveNext
        mnuTodas.Caption = ador(0).Value
        ador.MoveNext
        Check1.Caption = ador(0).Value
        ador.Close
        
    
    End If
    
    Set ador = Nothing
           
End Sub

