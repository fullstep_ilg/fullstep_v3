Attribute VB_Name = "CRbasPublic"
Option Explicit

Public crs_crapp As CRAXDRT.Application
Public crs_Server As String
Public crs_Database As String
Public crs_Connected As Boolean
Public crs_User As String
Public crs_Password As String
Public Function crs_ParameterIndex(rpt As CRAXDRT.Report, parametername As String)

    Dim i As Integer
    
    crs_ParameterIndex = 0
    
    For i = 1 To rpt.ParameterFields.Count
        If rpt.ParameterFields(i).ParameterFieldName = parametername Then
            crs_ParameterIndex = i
            Exit For
        End If
    Next i
    
End Function
Public Function crs_FormulaIndex(rpt As CRAXDRT.Report, formulaname As String)

    Dim i As Integer
    
    crs_FormulaIndex = 0
    
    For i = 1 To rpt.FormulaFields.Count
        If rpt.FormulaFields(i).FormulaFieldName = formulaname Then
            crs_FormulaIndex = i
            Exit For
        End If
    Next i
    
End Function
Public Function crs_Tableindex(rpt As CRAXDRT.Report, tablename As String) As Integer

    Dim i As Integer
    
    crs_Tableindex = 0
    
    For i = 1 To rpt.Database.Tables.Count
        If rpt.Database.Tables(i).Name = tablename Then
            crs_Tableindex = i
            Exit For
        End If
    Next i
        
End Function
Public Function crs_FieldIndex(rpt As CRAXDRT.Report, tablename As String, fieldname As String)

    Dim i As Integer
    
    Dim Tableindex As Integer
    
    Tableindex = 0
    
    For i = 1 To rpt.Database.Tables.Count
        If rpt.Database.Tables(i).Name = tablename Then
            Tableindex = i
            Exit For
        End If
    Next i
    
    crs_FieldIndex = 0
    
    For i = 1 To rpt.Database.Tables(Tableindex).Fields.Count
        If rpt.Database.Tables(Tableindex).Fields(i).DatabaseFieldName = fieldname Then
            crs_FieldIndex = i
            Exit For
        End If
    Next i
    
End Function
Public Function crs_SortDirection(sortstr As String) As CRSortDirection

    Select Case Left(sortstr, 1)

    Case "+"
        crs_SortDirection = crAscendingOrder
    Case "-"
        crs_SortDirection = crDescendingOrder
    
    End Select
    
End Function
Public Function crs_SortTable(sortstr As String) As String

    Dim f As Integer
    
    f = InStr(3, sortstr, ".")
    
    crs_SortTable = Mid(sortstr, 3, f - 3)
    
End Function
Public Function crs_SortField(sortstr As String) As String
    
    Dim i As Integer, f As Integer
    
    i = InStr(3, sortstr, ".") + 1
    f = InStr(i, sortstr, "}")
    
    crs_SortField = Mid(sortstr, i, f - i)
    
End Function



