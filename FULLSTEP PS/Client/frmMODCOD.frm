VERSION 5.00
Begin VB.Form frmMODCOD 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   1650
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3405
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmMODCOD.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1650
   ScaleWidth      =   3405
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   1695
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   1260
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   600
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1260
      Width           =   1005
   End
   Begin VB.TextBox txtCodNue 
      Height          =   285
      Left            =   1680
      TabIndex        =   0
      Top             =   720
      Width           =   1590
   End
   Begin VB.TextBox txtCodAct 
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   225
      Width           =   1590
   End
   Begin VB.Label Label2 
      BackColor       =   &H00808000&
      Caption         =   "C�digo nuevo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   60
      TabIndex        =   5
      Top             =   780
      Width           =   1620
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "C�digo actual:"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   60
      TabIndex        =   4
      Top             =   270
      Width           =   1620
   End
End
Attribute VB_Name = "frmMODCOD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_bCodigo As Boolean
Public g_sfrmOrigen As String
Public g_oMonedaEd As CMoneda

Private sIdiCodigo As String
Private Sub cmdAceptar_Click()
    Dim teserror As CTESError
    Dim sCodNuevo As String
    
    If (LTrim(RTrim(txtCodNue.Text)) = "") Then
        basMensajes.NoValido sIdiCodigo
        txtCodNue.Text = ""
        Exit Sub
    End If
    
    sCodNuevo = Trim(txtCodNue.Text)
    
    Select Case g_sfrmOrigen
        Case "frmActiv"
               frmActiv.g_sCodigoNuevo = sCodNuevo
        Case "frmMON"
            Set teserror = New CTESError
            teserror.NumError = TESnoerror
            If (UCase(sCodNuevo) = UCase(g_oMonedaEd.Cod)) Then
                basMensajes.NoValido sIdiCodigo
                txtCodNue.Text = ""
                Exit Sub
            End If
            Set teserror = g_oMonedaEd.CambiarCodigo(sCodNuevo)
            If (teserror.NumError <> TESnoerror) Then
                basErrores.TratarError teserror
                txtCodNue.Text = ""
                Exit Sub
            End If
        Case "frmCompanias"
            frmCompanias.g_sCodigoNuevo = sCodNuevo
    End Select
    m_bCodigo = True
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
   frmActiv.g_bCodigoCancelar = True
   Unload Me
End Sub

Private Sub Form_Load()
    CargarRecursos
    Me.Top = frmMDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = frmMDI.ScaleWidth / 2 - Me.Width / 2

    m_bCodigo = False

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
        Select Case g_sfrmOrigen
            Case "frmActiv"
                If m_bCodigo = False Then
                    frmActiv.g_bCodigoCancelar = True
                End If
            Case "frmCompanias"
                If m_bCodigo = False Then
                    frmCompanias.g_sCodigoNuevo = frmCompanias.g_oCiaSeleccionada.Cod
                End If
        End Select
    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_MODCOD, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        
        Me.cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label1.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label2.Caption = Ador(0).Value
        Ador.MoveNext
        sIdiCodigo = Ador(0).Value
        
        
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub



