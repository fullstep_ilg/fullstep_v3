VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CREnlaceProvePremium"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False

Public Sub Listadopropre(oReport As CRAXDRT.Report, Optional ByVal OrdCod As Boolean, Optional ByVal OrdDen As Boolean, Optional ByVal IdCia As Integer, Optional ByVal Todos As Boolean, Optional ByVal Selec As Boolean)
Dim Table As CRAXDRT.DatabaseTable
    Dim RecordSortFields As String
    Dim sSQL As String
    'Dim sRecordSelectionFormula As String
    Dim GroupOrder1 As String
    
    If crs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
    Next
    
     sSQL = ObtenerSQLpropre(IdCia, OrdCod, OrdDen, Todos, Selec)
     
     oReport.SQLQueryString = sSQL
    
     'oReport.RecordSelectionFormula = sRecordSelectionFormula
     
    If OrdCod Then
        RecordSortFields = "+{CIAS.COD}"
        GroupOrder1 = "{CIAS.COD}"
    End If
    
    If OrdDen = True Then
        RecordSortFields = "+{CIAS.DEN}"
        GroupOrder1 = "{CIAS.DEN}"
    End If
    
    While oReport.RecordSortFields.Count > 0
        oReport.RecordSortFields.Delete 1
    Wend
    
    If OrdCod Or OrdDen Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "ORDEN")).Text = GroupOrder1
        oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex(oReport, crs_SortTable(RecordSortFields))).Fields(crs_FieldIndex(oReport, crs_SortTable(RecordSortFields), crs_SortField(RecordSortFields))), crs_SortDirection(RecordSortFields)
    End If
    
End Sub

''' <summary>
''' Crear la consulta para el listado proveedores premium
''' </summary>
''' <param name="IdCia">Compania</param>
''' <param name="OrdCod">Ordenar por codigo</param>
''' <param name="OrdDen">Ordenar por denominacion</param>
''' <param name="Todos">Todos o solo la compania dada</param>
''' <param name="Selec">Selec</param>
''' <returns>String con la consulta</returns>
''' <remarks>Llamada desde: Listadopropre ; Tiempo m�ximo: 0</remarks>
''' Revisado por: Jbg. Fecha: 03/07/2012
Private Function ObtenerSQLpropre(Optional ByVal IdCia As Integer, Optional ByVal OrdCod As Boolean, Optional ByVal OrdDen As Boolean, Optional ByVal Todos As Boolean, Optional ByVal Selec As Boolean)
Dim sConsulta As String

If Todos Then
    sConsulta = "SELECT REL_CIAS.CIA_PROVE,REL_CIAS.CIA_COMP,REL_CIAS.COD_PROVE_CIA,REL_CIAS.PREM_ACTIVO,CIAS.DEN,CIAS.COD,CIASC.DEN AS DENC,CIASC.COD AS CODC FROM REL_CIAS WITH(NOLOCK) INNER JOIN CIAS WITH(NOLOCK) ON REL_CIAS.CIA_PROVE = CIAS.ID INNER JOIN CIAS AS CIASC WITH(NOLOCK) ON REL_CIAS.CIA_COMP=CIASC.ID WHERE 1=1"
Else
    sConsulta = "SELECT REL_CIAS.CIA_PROVE,REL_CIAS.CIA_COMP,REL_CIAS.COD_PROVE_CIA,REL_CIAS.PREM_ACTIVO,CIAS.DEN,CIAS.COD,CIASC.DEN AS DENC,CIASC.COD AS CODC FROM REL_CIAS WITH(NOLOCK) INNER JOIN CIAS WITH(NOLOCK) ON REL_CIAS.CIA_PROVE = CIAS.ID INNER JOIN CIAS AS CIASC WITH(NOLOCK) ON REL_CIAS.CIA_COMP=CIASC.ID WHERE AND REL_CIAS.CIA_PROVE =" & IdCia
End If

If OrdCod Then
    sConsulta = sConsulta & " ORDER BY CIAS.COD"
End If
If OrdDen Then
    sConsulta = sConsulta & " ORDER BY CIAS.DEN"
End If

ObtenerSQLpropre = sConsulta

End Function

''' <summary>
''' Crear la consulta para el listado de Companias
''' </summary>
''' <param name="IdCia">Compania</param>
''' <param name="OrdCod">Ordenar por codigo</param>
''' <param name="OrdDen">Ordenar por denominacion</param>
''' <param name="Todos">Todos o solo la compania dada</param>
''' <param name="Selec">Selec</param>
''' <returns>String con la consulta</returns>
''' <remarks>Llamada desde: ListadoCompa�ias ; Tiempo m�ximo: 0</remarks>
''' Revisado por: Jbg. Fecha: 03/07/2012
Private Function ObtenerSQLcomp(Optional ByVal IdCia As Integer, Optional ByVal OrdCod As Boolean, Optional ByVal OrdDen As Boolean, Optional ByVal Todos As Boolean, Optional ByVal Selec As Boolean)
If Todos Then
    sConsulta = "SELECT REL_CIAS.CIA_PROVE,REL_CIAS.CIA_COMP,REL_CIAS.COD_PROVE_CIA,REL_CIAS.PREM_ACTIVO,CIAS.DEN,CIAS.COD,CIASC.DEN AS DENC,CIASC.COD AS CODC FROM REL_CIAS WITH(NOLOCK) INNER JOIN CIAS WITH(NOLOCK) ON REL_CIAS.CIA_PROVE = CIAS.ID INNER JOIN CIAS AS CIASC WITH(NOLOCK) ON REL_CIAS.CIA_COMP=CIASC.ID WHERE 1 = 1 "
Else
    sConsulta = "SELECT REL_CIAS.CIA_PROVE,REL_CIAS.CIA_COMP,REL_CIAS.COD_PROVE_CIA,REL_CIAS.PREM_ACTIVO,CIAS.DEN,CIAS.COD,CIASC.DEN AS DENC,CIASC.COD AS CODC FROM REL_CIAS WITH(NOLOCK) INNER JOIN CIAS WITH(NOLOCK) ON REL_CIAS.CIA_PROVE = CIAS.ID INNER JOIN CIAS AS CIASC WITH(NOLOCK) ON REL_CIAS.CIA_COMP=CIASC.ID WHERE REL_CIAS.CIA_COMP =" & IdCia
End If

If OrdCod Then
    sConsulta = sConsulta & " ORDER BY CIASC.COD,CIAS.COD"
End If
If OrdDen Then
    sConsulta = sConsulta & " ORDER BY CIASC.DEN,CIAS.DEN"
End If

ObtenerSQLcomp = sConsulta

End Function

Public Sub ListadoCompa�ias(oReport As CRAXDRT.Report, Optional ByVal OrdCod As Boolean, Optional ByVal OrdDen As Boolean, Optional ByVal IdCia As Integer, Optional ByVal Todos As Boolean, Optional ByVal Selec As Boolean)
Dim Table As CRAXDRT.DatabaseTable
    Dim RecordSortFields As String
    Dim sSQL As String
    'Dim sRecordSelectionFormula As String
    Dim GroupOrder1 As String
    
    If crs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
    Next
    
     sSQL = ObtenerSQLcomp(IdCia, OrdCod, OrdDen, Todos, Selec)
     
     oReport.SQLQueryString = sSQL
    
     'oReport.RecordSelectionFormula = sRecordSelectionFormula
     
    If OrdCod Then
    '    RecordSortFields = "+{CIAS.COD}"
        GroupOrder1 = "{CIASC.COD}"
    End If
    
    If OrdDen = True Then
    '    RecordSortFields = "+{CIAS.DEN}"
        GroupOrder1 = "{CIASC.DEN}"
    End If
    
    'While oReport.RecordSortFields.Count > 0
    '    oReport.RecordSortFields.Delete 1
    'Wend
    
    If OrdCod Or OrdDen Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "ORDEN")).Text = GroupOrder1
    '    oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex(oReport, crs_SortTable(RecordSortFields))).Fields(crs_FieldIndex(oReport, crs_SortTable(RecordSortFields), crs_SortField(RecordSortFields))), crs_SortDirection(RecordSortFields)
    End If

End Sub

