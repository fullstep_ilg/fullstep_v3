VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstMonCentr 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de coordinaci�n de monedas centrales"
   ClientHeight    =   2355
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4845
   Icon            =   "frmLstMonCentr.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2355
   ScaleWidth      =   4845
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   4845
      TabIndex        =   0
      Top             =   1980
      Width           =   4845
      Begin VB.CommandButton cmdObtener 
         Caption         =   "&Obtener"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3480
         TabIndex        =   1
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   1900
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   3360
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Orden"
      TabPicture(0)   =   "frmLstMonCentr.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      Begin VB.Frame Frame1 
         Height          =   1335
         Left            =   120
         TabIndex        =   3
         Top             =   420
         Width           =   4575
         Begin VB.OptionButton optOrdCia 
            Caption         =   "Compa��a"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   240
            TabIndex        =   6
            Top             =   240
            Value           =   -1  'True
            Width           =   1455
         End
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo FullStepGS"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   240
            TabIndex        =   5
            Top             =   600
            Width           =   3075
         End
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n FullStepGS"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   240
            TabIndex        =   4
            Top             =   960
            Width           =   2955
         End
      End
   End
End
Attribute VB_Name = "frmLstMonCentr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public g_adores_Ador As Ador.Recordset

Private sIdiTxtTitulo As String
Private sIditxtCompania As String
Private sIdiTxtCodGS As String
Private sIdiTxtDenGS As String
Private sIdiTxtCodP As String
Private sIdiTxtDenP As String
Private sIdiTxtPag As String
Private sIdiTxtDe As String

Private Sub ObtenerListado()
    Dim oReport As CRAXDRT.Report
    Dim pv As Preview
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim Table As CRAXDRT.DatabaseTable
    Dim intOrden As Integer
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    'Comprueba que el el path sea correcto y que exista el informe
    If basParametros.g_sRptPath = "" Then
        basMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    RepPath = basParametros.g_sRptPath & "\rptMonCentrales.rpt"

    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        basMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set oFos = Nothing
    
    
    'Obtiene las monedas centrales
    If opOrdCod.Value = True Then
        intOrden = 1
    ElseIf opOrdDen.Value = True Then
        intOrden = 2
    Else
        intOrden = 0
    End If
    
    Set g_adores_Ador = g_oRaiz.DevolverMonedasCentralesDesde(, , , True, intOrden)
    
    
    'Abre el report
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
    Next
    
    'Le pasa los datos al informe
    oReport.Database.SetDataSource g_adores_Ador
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "Titulo")).Text = "'" & sIdiTxtTitulo & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCompania")).Text = "'" & sIditxtCompania & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodGS")).Text = "'" & sIdiTxtCodGS & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDenGS")).Text = "'" & sIdiTxtDenGS & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodP")).Text = "'" & sIdiTxtCodP & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDenP")).Text = "'" & sIdiTxtDenP & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = "'" & sIdiTxtPag & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = "'" & sIdiTxtDe & "'"

    
    'Imprime el informe
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
   
    Set pv = New Preview
    
    pv.Hide
    pv.g_sOrigen = "frmLstMonCentr"
    pv.Caption = Me.Caption
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    pv.crViewer.ViewReport
    pv.Show
    
    Set oReport = Nothing
    
    Unload Me
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdObtener_Click()
    '''Obtiene el listado
    ObtenerListado
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTMONCENTR, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        sIdiTxtTitulo = Ador(0).Value
        Ador.MoveNext
        Me.cmdObtener.Caption = Ador(0).Value
        Ador.MoveNext
        Me.opOrdCod.Caption = Ador(0).Value
        Ador.MoveNext
        Me.opOrdDen.Caption = Ador(0).Value
        Ador.MoveNext
        Me.optOrdCia.Caption = Ador(0).Value
        sIditxtCompania = Ador(0).Value
        Ador.MoveNext
        Me.SSTab1.Caption = Ador(0).Value
        Ador.MoveNext
        sIdiTxtCodGS = Ador(0).Value
        Ador.MoveNext
        sIdiTxtDenGS = Ador(0).Value
        Ador.MoveNext
        sIdiTxtCodP = Ador(0).Value
        Ador.MoveNext
        sIdiTxtDenP = Ador(0).Value
        Ador.MoveNext
        sIdiTxtPag = Ador(0).Value
        Ador.MoveNext
        sIdiTxtDe = Ador(0).Value
        
        
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub







Private Sub Form_Load()
CargarRecursos

End Sub
