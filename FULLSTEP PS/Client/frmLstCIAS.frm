VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmLstCIAS 
   Caption         =   "Listado de compa��as"
   ClientHeight    =   5640
   ClientLeft      =   1425
   ClientTop       =   1545
   ClientWidth     =   10140
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstCIAS.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5640
   ScaleWidth      =   10140
   Begin VB.CommandButton cmdObtener 
      Caption         =   "&Obtener"
      Height          =   345
      Left            =   9075
      TabIndex        =   27
      Top             =   5280
      Width           =   1005
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5220
      Left            =   0
      TabIndex        =   28
      Top             =   0
      Width           =   10065
      _ExtentX        =   17754
      _ExtentY        =   9208
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstCIAS.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "picDatos"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Opciones"
      TabPicture(1)   =   "frmLstCIAS.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame5"
      Tab(1).Control(1)=   "Frame4"
      Tab(1).Control(2)=   "Frame3"
      Tab(1).ControlCount=   3
      Begin VB.PictureBox picDatos 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4875
         Left            =   75
         ScaleHeight     =   4875
         ScaleWidth      =   9885
         TabIndex        =   40
         Top             =   315
         Width           =   9885
         Begin VB.Frame Frame2 
            Caption         =   "Ordenado por"
            Height          =   870
            Left            =   30
            TabIndex        =   61
            Top             =   3900
            Width           =   9840
            Begin VB.OptionButton optOrdAutCIA 
               Caption         =   "Denominaci�n"
               Height          =   240
               Index           =   1
               Left            =   5205
               TabIndex        =   63
               Top             =   360
               Width           =   3375
            End
            Begin VB.OptionButton optOrdAutCIA 
               Caption         =   "C�digo"
               Height          =   255
               Index           =   0
               Left            =   810
               TabIndex        =   62
               Top             =   360
               Value           =   -1  'True
               Width           =   3210
            End
         End
         Begin VB.Frame Frame1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2355
            Left            =   15
            TabIndex        =   44
            Top             =   0
            Width           =   9870
            Begin VB.TextBox txtVolHasta 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   6195
               TabIndex        =   7
               Top             =   1470
               Width           =   1780
            End
            Begin VB.TextBox txtCod 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1710
               TabIndex        =   0
               Top             =   300
               Width           =   2895
            End
            Begin VB.TextBox txtDen 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   6195
               TabIndex        =   5
               Top             =   300
               Width           =   2895
            End
            Begin VB.TextBox txtVol 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1710
               TabIndex        =   3
               Top             =   1470
               Width           =   1845
            End
            Begin VB.TextBox txtCliRef1 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   6195
               TabIndex        =   8
               Top             =   1860
               Width           =   2895
            End
            Begin VB.TextBox txtHom1 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1710
               TabIndex        =   4
               Top             =   1860
               Width           =   2895
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcPaiCod 
               Height          =   285
               Left            =   1710
               TabIndex        =   1
               Top             =   690
               Width           =   2895
               DataFieldList   =   "Column 0"
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3228
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Caption=   "C�digo"
               Columns(2).Name =   "COD"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   5106
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcMonCod 
               Height          =   285
               Left            =   1710
               TabIndex        =   2
               Top             =   1080
               Width           =   2895
               DataFieldList   =   "Column 0"
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3545
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Caption=   "C�digo"
               Columns(2).Name =   "COD"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   5106
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProviCod 
               Height          =   285
               Left            =   6195
               TabIndex        =   6
               Top             =   690
               Width           =   2895
               DataFieldList   =   "Column 0"
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3545
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Caption=   "C�digo"
               Columns(2).Name =   "COD"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   5106
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label Label14 
               Caption         =   "(en miles de Euros)"
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   8010
               TabIndex        =   54
               Top             =   1485
               Width           =   1830
            End
            Begin VB.Label Label20 
               Caption         =   "hasta:"
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   4755
               TabIndex        =   53
               Top             =   1492
               Width           =   1455
            End
            Begin VB.Label Label8 
               Caption         =   "C�digo:"
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   180
               TabIndex        =   52
               Top             =   322
               Width           =   1575
            End
            Begin VB.Label Label3 
               Caption         =   "Pa�s:"
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   180
               TabIndex        =   51
               Top             =   712
               Width           =   1575
            End
            Begin VB.Label Label15 
               Caption         =   "Moneda:"
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   180
               TabIndex        =   50
               Top             =   1102
               Width           =   1575
            End
            Begin VB.Label Label16 
               Caption         =   "Provincia:"
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   4755
               TabIndex        =   49
               Top             =   712
               Width           =   1455
            End
            Begin VB.Label Label1 
               Caption         =   "Denominaci�n:"
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   4755
               TabIndex        =   48
               Top             =   322
               Width           =   1455
            End
            Begin VB.Label Label5 
               Caption         =   "Vol. fact. desde:"
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   180
               TabIndex        =   47
               Top             =   1492
               Width           =   1575
            End
            Begin VB.Label Label7 
               Caption         =   "Cliente de ref.:"
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   4755
               TabIndex        =   46
               Top             =   1882
               Width           =   1455
            End
            Begin VB.Label Label19 
               Caption         =   "Homologaci�n:"
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   180
               TabIndex        =   45
               Top             =   1882
               Width           =   1575
            End
         End
         Begin VB.Frame FraActividades 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   810
            Left            =   15
            TabIndex        =   42
            Top             =   2340
            Width           =   9870
            Begin VB.CommandButton cmdSELACT 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   9015
               Picture         =   "frmLstCIAS.frx":0CEA
               Style           =   1  'Graphical
               TabIndex        =   10
               Top             =   330
               Width           =   285
            End
            Begin VB.CommandButton cmdBorrar 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   9360
               Picture         =   "frmLstCIAS.frx":106C
               Style           =   1  'Graphical
               TabIndex        =   11
               Top             =   330
               Width           =   270
            End
            Begin VB.Label Label17 
               Caption         =   "Actividad:"
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   180
               TabIndex        =   43
               Top             =   360
               Width           =   1125
            End
            Begin VB.Label lblActividad 
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1320
               TabIndex        =   9
               Top             =   330
               Width           =   7425
            End
         End
         Begin VB.Frame fraAcceso 
            Caption         =   "DAcceso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1650
            Left            =   15
            TabIndex        =   41
            Top             =   3135
            Width           =   9870
            Begin VB.TextBox txtAutDesde 
               Enabled         =   0   'False
               Height          =   285
               Left            =   1140
               TabIndex        =   13
               Top             =   720
               Width           =   1110
            End
            Begin VB.TextBox txtAutHasta 
               Enabled         =   0   'False
               Height          =   285
               Left            =   1140
               TabIndex        =   15
               Top             =   1065
               Width           =   1110
            End
            Begin VB.CommandButton cmdCalAutDesde 
               Enabled         =   0   'False
               Height          =   285
               Left            =   2300
               Picture         =   "frmLstCIAS.frx":15AE
               Style           =   1  'Graphical
               TabIndex        =   14
               TabStop         =   0   'False
               ToolTipText     =   "Mantenimiento"
               Top             =   720
               Width           =   315
            End
            Begin VB.CommandButton cmdCalAutHasta 
               Enabled         =   0   'False
               Height          =   285
               Left            =   2300
               Picture         =   "frmLstCIAS.frx":1B38
               Style           =   1  'Graphical
               TabIndex        =   16
               TabStop         =   0   'False
               ToolTipText     =   "Mantenimiento"
               Top             =   1065
               Width           =   315
            End
            Begin VB.TextBox txtDesAutDesde 
               Enabled         =   0   'False
               Height          =   285
               Left            =   4405
               TabIndex        =   18
               Top             =   720
               Width           =   1110
            End
            Begin VB.TextBox txtDesAutHasta 
               Enabled         =   0   'False
               Height          =   285
               Left            =   4405
               TabIndex        =   20
               Top             =   1065
               Width           =   1110
            End
            Begin VB.CommandButton cmdCalDesAutDesde 
               Enabled         =   0   'False
               Height          =   285
               Left            =   5560
               Picture         =   "frmLstCIAS.frx":20C2
               Style           =   1  'Graphical
               TabIndex        =   19
               TabStop         =   0   'False
               ToolTipText     =   "Mantenimiento"
               Top             =   720
               Width           =   315
            End
            Begin VB.CommandButton cmdCalDesAutHasta 
               Enabled         =   0   'False
               Height          =   285
               Left            =   5560
               Picture         =   "frmLstCIAS.frx":264C
               Style           =   1  'Graphical
               TabIndex        =   21
               TabStop         =   0   'False
               ToolTipText     =   "Mantenimiento"
               Top             =   1065
               Width           =   315
            End
            Begin VB.TextBox txtSolDesde 
               Enabled         =   0   'False
               Height          =   285
               Left            =   7595
               TabIndex        =   23
               Top             =   720
               Width           =   1110
            End
            Begin VB.TextBox txtSolHasta 
               Enabled         =   0   'False
               Height          =   285
               Left            =   7595
               TabIndex        =   25
               Top             =   1065
               Width           =   1110
            End
            Begin VB.CommandButton cmdCalSolDesde 
               Enabled         =   0   'False
               Height          =   285
               Left            =   8750
               Picture         =   "frmLstCIAS.frx":2BD6
               Style           =   1  'Graphical
               TabIndex        =   24
               TabStop         =   0   'False
               ToolTipText     =   "Mantenimiento"
               Top             =   720
               Width           =   315
            End
            Begin VB.CommandButton cmdCalSolHasta 
               Enabled         =   0   'False
               Height          =   285
               Left            =   8750
               Picture         =   "frmLstCIAS.frx":3160
               Style           =   1  'Graphical
               TabIndex        =   26
               TabStop         =   0   'False
               ToolTipText     =   "Mantenimiento"
               Top             =   1065
               Width           =   315
            End
            Begin VB.CheckBox chkFunProDes 
               Caption         =   "Desautorizada"
               Height          =   225
               Left            =   3500
               TabIndex        =   17
               Top             =   350
               Width           =   2580
            End
            Begin VB.CheckBox chkFunProPen 
               Caption         =   "Pendiente de autorizar"
               Height          =   225
               Left            =   6700
               TabIndex        =   22
               Top             =   350
               Width           =   2580
            End
            Begin VB.CheckBox chkFunProAut 
               Caption         =   "Autorizada"
               Height          =   225
               Left            =   240
               TabIndex        =   12
               Top             =   350
               Width           =   2355
            End
            Begin VB.Label lblAutDesde 
               Caption         =   "DDesde:"
               Height          =   240
               Left            =   240
               TabIndex        =   60
               Top             =   765
               Width           =   810
            End
            Begin VB.Label lblAutHasta 
               Caption         =   "DHasta:"
               Height          =   240
               Left            =   240
               TabIndex        =   59
               Top             =   1125
               Width           =   810
            End
            Begin VB.Label lblDesAutDesde 
               Caption         =   "DDesde:"
               Height          =   240
               Left            =   3500
               TabIndex        =   58
               Top             =   765
               Width           =   810
            End
            Begin VB.Label lblDesAutHasta 
               Caption         =   "DHasta:"
               Height          =   240
               Left            =   3500
               TabIndex        =   57
               Top             =   1125
               Width           =   810
            End
            Begin VB.Label lblSolDesde 
               Caption         =   "DDesde:"
               Height          =   240
               Left            =   6700
               TabIndex        =   56
               Top             =   765
               Width           =   810
            End
            Begin VB.Label lblSolHasta 
               Caption         =   "DHasta:"
               Height          =   240
               Left            =   6700
               TabIndex        =   55
               Top             =   1125
               Width           =   810
            End
         End
      End
      Begin VB.Frame Frame3 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1065
         Left            =   -74805
         TabIndex        =   37
         Top             =   570
         Width           =   9015
         Begin VB.OptionButton opResum 
            Caption         =   "Listado resumido"
            Height          =   255
            Left            =   795
            TabIndex        =   39
            Top             =   465
            Value           =   -1  'True
            Width           =   3210
         End
         Begin VB.OptionButton opDetalle 
            Caption         =   "Listado en detalle"
            Height          =   240
            Left            =   5145
            TabIndex        =   38
            Top             =   465
            Width           =   3375
         End
      End
      Begin VB.Frame Frame4 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1485
         Left            =   -74820
         TabIndex        =   32
         Top             =   1755
         Width           =   9030
         Begin VB.CheckBox chkUsu 
            Caption         =   "Incluir usuarios"
            Enabled         =   0   'False
            Height          =   255
            Left            =   3240
            TabIndex        =   36
            Top             =   435
            Width           =   2460
         End
         Begin VB.CheckBox chkEsp 
            Caption         =   "Incluir archivos adjuntos"
            Enabled         =   0   'False
            Height          =   255
            Left            =   6195
            TabIndex        =   35
            Top             =   435
            Width           =   2715
         End
         Begin VB.CheckBox chkUsuDet 
            Caption         =   "Incluir detalle de usuarios"
            Enabled         =   0   'False
            Height          =   255
            Left            =   3615
            TabIndex        =   34
            Top             =   930
            Width           =   2850
         End
         Begin VB.CheckBox chkPremium 
            Caption         =   "S�lo proveedores Premium"
            Height          =   255
            Left            =   360
            TabIndex        =   33
            Top             =   435
            Width           =   2460
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Ordenado por"
         Height          =   1350
         Left            =   -74820
         TabIndex        =   29
         Top             =   3405
         Width           =   9030
         Begin VB.OptionButton optOrdCod 
            Caption         =   "C�digo"
            Height          =   255
            Left            =   810
            TabIndex        =   31
            Top             =   570
            Value           =   -1  'True
            Width           =   3210
         End
         Begin VB.OptionButton optOrdDen 
            Caption         =   "Denominaci�n"
            Height          =   240
            Left            =   5205
            TabIndex        =   30
            Top             =   570
            Width           =   3375
         End
      End
   End
   Begin MSComDlg.CommonDialog cmmExcel 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "frmLstCIAS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oCias As CCias
Public g_oCiaSeleccionada As CCia

Public g_bRespetarCombo As Boolean
Private m_CargarComboDesde As Boolean

Public g_sACN1Seleccionad As String
Public g_sACN2Seleccionad As String
Public g_sACN3Seleccionad As String
Public g_sACN4Seleccionad As String
Public g_sACN5Seleccionad As String
Public g_sOrigen As String

Private sIdiVolFact As String
Private sIdiProvincia As String
Private sIdiCodigo As String
Private sIdiDen As String
Private sIdiPai As String
Private sIdiMon As String
Private sIdiHom As String
Private sIdiCliRef As String
Private sIdiAct As String
Private sIdiCompradora As String
Private sIdiAutorizada As String
Private sIdiDesautorizada As String
Private sIdiPendAuto As String
Private sIdiProveedora As String
Private sIdiTodas As String

Private sIditxtCodigo As String
Private sIditxtNombre As String
Private sIditxtCP As String
Private sIditxtDireccion As String
Private sIditxtPoblacion As String
Private sIditxtTituloResumido As String
Private sIditxtCompania As String
Private sIditxtFunc As String
Private sIditxtNIF As String
Private sIditxtWWW As String
Private sIditxtPMVinculada As String
Private sIditxtVolFact As String
Private sIditxtUsus As String
Private sIditxtAdjuntos As String
Private sIditxtProveAut As String
Private sIditxtProveDesaut As String
Private sIditxtProveSolicit As String
Private sIditxtTipoUsu As String
Private sIditxtDepto As String
Private sIditxtTfno As String
Private sIditxtEmail As String
Private sIditxtCargo As String
Private sIditxtFax As String
Private sIditxtTfnoMovil As String
Private sIditxtTituloDetalle As String
Private sIdiTxtPag As String
Private sIdiTxtDe As String
Private sIdiTxtHomol As String
Private sIdiTxtComent As String
Private sIdiTxtCliRef As String
Private sIdiTxtCodGS As String
Private sIditxtHasta As String
Private sIditxtDesde As String
Private sIditxtAccesoAutorizado As String
Private sIditxtAccesoDesautorizado As String
Private sIditxtAccesoSolicitado As String
Private sIdiCol(0 To 7) As String

Private Function GenerarExcelAutorizacionCIAS(ByVal Docexcel As String) As Boolean
Dim sConnect As String
Dim oExcelAdoConn As adodb.Connection
Dim oExcelAdoRS As adodb.Recordset
Dim SQL As String
On Error GoTo ERROR_Frm
Set oExcelAdoRS = m_oCias.ListadoAutorizacionCias(optOrdAutCIA(1).Value, optOrdAutCIA(0).Value, txtCod, txtDen, Val(sdbcPaiCod.Columns(0).Text), Val(sdbcMonCod.Columns(0).Text), _
                                    Val(sdbcProviCod.Columns(0).Text), chkFunProAut, chkFunProDes, chkFunProPen, txtAutDesde.Text, txtAutHasta.Text, _
                                    txtDesAutDesde.Text, txtDesAutHasta.Text, txtSolDesde.Text, txtSolHasta.Text)
        
Set oExcelAdoConn = New adodb.Connection
sConnect = "Provider=MSDASQL.1;Extended Properties=""DBQ=" & Docexcel & ";Driver={Microsoft Excel Driver (*.xls)};" _
         & "FIL=excel 8.0;ReadOnly=0;UID=admin;"""
        
oExcelAdoConn.Open sConnect

'Columnas fijas!!
SQL = "CREATE TABLE [Autorizaci�n de compa��as$]  ([" & sIdiCol(0) & "] memo,[" & sIdiCol(1) & "] memo, [" & sIdiCol(2) & "] memo, [" & sIdiCol(3) & "] memo,[" & sIdiCol(4) & "] memo,[" & sIdiCol(5) & "] memo,[" & sIdiCol(6) & "] memo,[" & sIdiCol(7) & "] memo)"
oExcelAdoConn.Execute SQL

While Not oExcelAdoRS.EOF
    'dtFecha = CDate(Left(oExcelAdoRS("FECHA").Value, Len(oExcelAdoRS("FECHA").Value) - 4))

    SQL = "INSERT INTO [Autorizaci�n de compa��as$] values ('" & oExcelAdoRS("COD").Value & "','" & oExcelAdoRS("COD_PROVE_CIA").Value & "','" & oExcelAdoRS("NIF").Value & "'"
    SQL = SQL & ",'" & Replace(oExcelAdoRS("DEN").Value, "'", "''") & "','" & oExcelAdoRS("FECINS").Value & "','" & oExcelAdoRS("FECACTFP").Value & "','" & oExcelAdoRS("INSTANCIA_VINCULADA").Value & "'"
    SQL = SQL & ",'" & oExcelAdoRS("USUARIO").Value & "')"
   
    oExcelAdoConn.Execute SQL
    oExcelAdoRS.MoveNext
Wend
   
GenerarExcelAutorizacionCIAS = True

Exit Function
      
ERROR_Frm:
   Select Case Err.Number
       Case 9:
           Resume 0
       
       Case 1004:
           Resume Next
       
       Case Else:
           MsgBox Err.Description
           If Err.Number = 7 Then
               GenerarExcelAutorizacionCIAS = False
               Exit Function
           End If
   End Select
Exit Function
End Function


Private Sub ObtenerInformeAutorizacionCIAS()
Dim sTemp As String
Dim fso As Scripting.FileSystemObject
Dim RepPath As String
Dim oFos As FileSystemObject
Dim oFile As Scripting.File
    Dim sFileName As String
Dim sFileTitle As String

    On Error GoTo Cancelar:

'Comprueba que el el path sea correcto y que exista el informe

RepPath = Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, "") & "\infAutorizacionCIAS.xls"
sTemp = DevolverPathFichTemp & "infAutorizacionCIAS.xls"

Set fso = New Scripting.FileSystemObject
fso.CopyFile RepPath, sTemp
Set oFile = fso.GetFile(sTemp)
If oFile.Attributes And 1 Then
    oFile.Attributes = oFile.Attributes Xor 1
End If
Set oFile = Nothing

If GenerarExcelAutorizacionCIAS(sTemp) Then
    cmmExcel.CancelError = True
           
    cmmExcel.DialogTitle = Me.Caption
    cmmExcel.Filter = "Excel |*.xls"
    cmmExcel.FileName = "infAutorizacionCIAS.xls"
    
    cmmExcel.ShowSave
            
  
    sFileName = cmmExcel.FileName
    sFileTitle = cmmExcel.FileTitle
    If sFileTitle = "" Then
        'oMensajes.NoValido "File" 'm_sIdiArchivo
        Exit Sub
    End If
        
    fso.CopyFile sTemp, sFileName
    Set fso = Nothing
   
End If

Cancelar:

If Err.Number <> 32755 Then '32755 = han pulsado cancel

End If
End Sub


Private Sub chkFunProAut_Click()
    If chkFunProAut.Value = vbChecked Then
        txtAutDesde.Enabled = True
        txtAutHasta.Enabled = True
        cmdCalAutDesde.Enabled = True
        cmdCalAutHasta.Enabled = True
    Else
        txtAutDesde.Enabled = False
        txtAutHasta.Enabled = False
        txtAutDesde.Text = ""
        txtAutHasta.Text = ""
        cmdCalAutDesde.Enabled = False
        cmdCalAutHasta.Enabled = False
    End If
End Sub

Private Sub chkFunProDes_Click()
    If chkFunProDes.Value = vbChecked Then
        txtDesAutDesde.Enabled = True
        txtDesAutHasta.Enabled = True
        cmdCalDesAutDesde.Enabled = True
        cmdCalDesAutHasta.Enabled = True
    Else
        txtDesAutDesde.Enabled = False
        txtDesAutHasta.Enabled = False
        txtDesAutDesde.Text = ""
        txtDesAutHasta.Text = ""
        cmdCalDesAutDesde.Enabled = False
        cmdCalDesAutHasta.Enabled = False
    End If
End Sub

Private Sub chkFunProPen_Click()
    If chkFunProPen.Value = vbChecked Then
        txtSolDesde.Enabled = True
        txtSolHasta.Enabled = True
        cmdCalSolDesde.Enabled = True
        cmdCalSolHasta.Enabled = True
    Else
        txtSolDesde.Enabled = False
        txtSolHasta.Enabled = False
        txtSolDesde.Text = ""
        txtSolHasta.Text = ""
        cmdCalSolDesde.Enabled = False
        cmdCalSolHasta.Enabled = False
    End If
End Sub

Private Sub chkusu_Click()
    If chkUsu.Value = vbChecked Then
        chkUsuDet.Enabled = True
    Else
        chkUsuDet.Enabled = False
        chkUsuDet.Value = vbUnchecked
    End If
End Sub

Private Sub cmdBorrar_Click()
    lblActividad.Caption = ""
    g_sACN1Seleccionad = ""
    g_sACN2Seleccionad = ""
    g_sACN3Seleccionad = ""
    g_sACN4Seleccionad = ""
    g_sACN5Seleccionad = ""

End Sub

Private Sub cmdObtener_Click()
    If txtVol <> "" Then
        If Not IsNumeric(txtVol) Then
            'mensaje de error
            basMensajes.NoValido sIdiVolFact
            txtVol.SetFocus
            Exit Sub
        End If
    End If
    If txtVolHasta <> "" Then
        If Not IsNumeric(txtVolHasta) Then
            'mensaje de error
            basMensajes.NoValido sIdiVolFact
            txtVolHasta.SetFocus
            Exit Sub
        End If
    End If
    
    Select Case g_sOrigen
        Case "LstCIAS"
            ObtenerInformeCIAS
        Case "LstAutorizacionCIAS"
            ObtenerInformeAutorizacionCIAS
    End Select
    
    Unload Me
    
    g_sACN1Seleccionad = ""
    g_sACN2Seleccionad = ""
    g_sACN3Seleccionad = ""
    g_sACN4Seleccionad = ""
    g_sACN5Seleccionad = ""

End Sub

Private Sub ObtenerInformeCIAS()
    Dim oReport As CRAXDRT.Report
    Dim oCRCias As New CRCIas
    Dim pv As Preview
    Dim RepPath, sCriterio As String
    Dim sublistado As Object
    Dim Table As Object
    Dim X As String
    Dim oFos As Scripting.FileSystemObject
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    'Comprueba que la ruta sea la correcta y que exista el informe

    If opResum.Value Then
        RepPath = basParametros.g_sRptPath & "\rptCiasResumido.rpt"
    Else
        RepPath = basParametros.g_sRptPath & "\rptCiasDetalle.rpt"
    End If
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        basMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)

    If opDetalle.Value Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = "'" & sIditxtTituloDetalle & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCompania")).Text = "'" & sIditxtCompania & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodigo")).Text = "'" & sIdiCodigo & ":'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFuncion")).Text = "'" & sIditxtFunc & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNIF")).Text = "'" & sIditxtNIF & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtVolFact")).Text = "'" & sIditxtVolFact & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDireccion")).Text = "'" & sIditxtDireccion & ":'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProvincia")).Text = "'" & sIdiProvincia & ":'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPais")).Text = "'" & sIdiPai & ":'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtWWW")).Text = "'" & sIditxtWWW & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPMVinculada")).Text = "'" & sIditxtPMVinculada & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUsus")).Text = "'" & sIditxtUsus & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAdjuntos")).Text = "'" & sIditxtAdjuntos & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtHomol")).Text = "'" & sIdiTxtHomol & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtComent")).Text = "'" & sIdiTxtComent & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCliRef")).Text = "'" & sIdiTxtCliRef & "'"
        
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProveSolicit")).Text = "'" & sIditxtAccesoSolicitado & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProveAut")).Text = "'" & sIditxtAccesoAutorizado & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProveDesaut")).Text = "'" & sIditxtAccesoDesautorizado & "'"
        
        
        If basParametros.g_udtParametrosGenerales.g_bUnaCompradora Then
            oReport.FormulaFields(crs_FormulaIndex(oReport, "UNACOMP")).Text = "'S'"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodGS")).Text = "'" & sIdiTxtCodGS & "'"
        Else
            oReport.FormulaFields(crs_FormulaIndex(oReport, "UNACOMP")).Text = "'N'"
        End If


        If chkUsu.Value = vbChecked Then
            If chkUsuDet.Value = vbChecked Then
                oReport.FormulaFields(crs_FormulaIndex(oReport, "USUDET")).Text = "'S'"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "USU")).Text = "'N'"
                Set sublistado = oReport.OpenSubreport("rptUSUDet")
                For Each Table In sublistado.Database.Tables
                     Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
                Next
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtNombre")).Text = "'" & sIditxtNombre & ":'"
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtCodigo")).Text = "'" & sIdiCodigo & ":'"
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtTipoUsu")).Text = "'" & sIditxtTipoUsu & "'"
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtDepto")).Text = "'" & sIditxtDepto & "'"
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtCargo")).Text = "'" & sIditxtCargo & ":'"
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtTfno")).Text = "'" & sIditxtTfno & ":'"
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtFax")).Text = "'" & sIditxtFax & ":'"
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtEmail")).Text = "'" & sIditxtEmail & "'"
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtTfnoMovil")).Text = "'" & sIditxtTfnoMovil & "'"
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtProveSolicit")).Text = "'" & sIditxtProveSolicit & "'"
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtProveDesaut")).Text = "'" & sIditxtProveDesaut & "'"
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtProveAut")).Text = "'" & sIditxtProveAut & "'"
                    
            Else
                oReport.FormulaFields(crs_FormulaIndex(oReport, "USUDET")).Text = "'N'"
                oReport.FormulaFields(crs_FormulaIndex(oReport, "USU")).Text = "'S'"
                Set sublistado = oReport.OpenSubreport("rptUsu")
                For Each Table In sublistado.Database.Tables
                         Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
                Next
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtCodigo")).Text = "'" & sIdiCodigo & "'"
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtNombre")).Text = "'" & sIditxtNombre & "'"
                'sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtFComp")).Text = "'" & sIditxtFComp & "'"
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtFProve")).Text = "'" & Left(sIditxtTipoUsu, Len(sIditxtTipoUsu) - 1) & "'"
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtProveSolicit")).Text = "'" & sIditxtProveSolicit & "'"
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtProveDesaut")).Text = "'" & sIditxtProveDesaut & "'"
                sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtProveAut")).Text = "'" & sIditxtProveAut & "'"
                
            End If
        Else
            oReport.FormulaFields(crs_FormulaIndex(oReport, "USUDET")).Text = "'N'"
            oReport.FormulaFields(crs_FormulaIndex(oReport, "USU")).Text = "'N'"
        End If
        If chkEsp.Value = vbChecked Then
            oReport.FormulaFields(crs_FormulaIndex(oReport, "ESP")).Text = "'S'"
        Else
            oReport.FormulaFields(crs_FormulaIndex(oReport, "ESP")).Text = "'N'"
        End If
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = "'" & sIditxtTituloResumido & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNombre")).Text = "'" & sIditxtNombre & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodigo")).Text = "'" & sIdiCodigo & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDireccion")).Text = "'" & sIditxtDireccion & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCP")).Text = "'" & sIditxtCP & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPoblacion")).Text = "'" & sIditxtPoblacion & "'"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPMVinculada")).Text = "'" & sIditxtPMVinculada & "'"
    End If
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & sIdiTxtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & sIdiTxtDe & """"
    
    sCriterio = ObtenerCriterio

    oCRCias.ListadoCompa�ias oReport, chkPremium.Value, optOrdDen, optOrdCod, txtCod, txtDen, Val(txtVolHasta), Val(txtVol), txtHom1, txtCliRef1, opDetalle, opResum, _
                             Val(sdbcPaiCod.Columns(0).Text), Val(sdbcMonCod.Columns(0).Text), Val(sdbcProviCod.Columns(0).Text), _
                             chkFunProAut, chkFunProDes, chkFunProPen, g_sACN1Seleccionad, _
                             g_sACN2Seleccionad, g_sACN3Seleccionad, g_sACN4Seleccionad, g_sACN5Seleccionad, _
                             txtAutDesde.Text, txtAutHasta.Text, txtDesAutDesde.Text, txtDesAutHasta.Text, txtSolDesde.Text, txtSolHasta.Text
                             
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & sCriterio & """"
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set pv = New Preview
    Unload Me
    pv.Hide
    
    pv.Caption = Me.Caption
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport ' SQLQueryString
    
    pv.crViewer.ViewReport
    pv.Show
    
    Set oReport = Nothing
    
    Unload Me
    
    Screen.MousePointer = vbNormal
End Sub



Private Sub cmdSELACT_Click()
 frmSelActividad.sOrigen = "frmLstCIAS"
    frmSelActividad.Show 1
End Sub

Private Sub Form_Load()
    CargarRecursos
    Set m_oCias = g_oRaiz.Generar_CCias
    frmLstCIAS.Width = 10260
    frmLstCIAS.Height = 6045
    If Not g_udtParametrosGenerales.g_bPremium Then
       chkPremium.Visible = False
       chkPremium.Enabled = False
    End If
    Select Case g_sOrigen
        Case "LstCIAS"
            Frame2.Visible = False
        Case "LstAutorizacionCIAS"
            SSTab1.TabVisible(1) = False
            FraActividades.Visible = False
            Frame1.Height = Frame1.Height - 900
            fraAcceso.Top = Frame1.Top + Frame1.Height + 30
            Frame2.Top = fraAcceso.Top + fraAcceso.Height + 30
            picDatos.Height = Frame2.Top + Frame2.Height + 100
            SSTab1.Height = Frame2.Top + Frame2.Height + 500
            cmdObtener.Top = SSTab1.Top + SSTab1.Height + 30
            Me.Height = cmdObtener.Top + cmdObtener.Height + 500
    End Select
End Sub

Private Sub opDetalle_Click()
    If opDetalle Then
        chkUsu.Enabled = True
        chkEsp.Enabled = True
    End If
End Sub

Private Sub opResum_Click()
    If opResum Then
        chkUsu.Value = vbUnchecked
        chkUsu.Enabled = False
        chkUsuDet.Value = vbUnchecked
        chkUsuDet.Enabled = False
        chkEsp.Value = vbUnchecked
        chkEsp.Enabled = False
    End If
End Sub
Private Sub sdbcMonCod_Change()
    If Not g_bRespetarCombo Then
        g_bRespetarCombo = True
        m_CargarComboDesde = True
    End If
End Sub

Private Sub sdbcMonCod_CloseUp()
    
     If sdbcMonCod.Value = "..." Then
        sdbcMonCod.Text = ""
        Exit Sub
    End If
    
    g_bRespetarCombo = True
    sdbcMonCod.Text = sdbcMonCod.Columns(1).Text
    g_bRespetarCombo = False
   
    
    m_CargarComboDesde = False

End Sub

Private Sub sdbcMonCod_DropDown()
Dim ador As ador.Recordset
    
   
    sdbcMonCod.RemoveAll
    
    If m_CargarComboDesde Then
        Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, sdbcMonCod, , False)
    Else
        Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , , False)
    End If
    
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbcMonCod.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value
            ador.MoveNext
        
        Wend
        
        ador.Close
        Set ador = Nothing
        
    End If
    
        
End Sub

Private Sub sdbcMonCod_InitColumnProps()
    
    sdbcMonCod.DataFieldList = "Column 1"
    sdbcMonCod.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcMonCod_PositionList(ByVal Text As String)
    Dim i As Long
   Dim bm As Variant

    On Error Resume Next
    
    sdbcMonCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcMonCod.Rows - 1
            bm = sdbcMonCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcMonCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcMonCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcMonCod_Validate(Cancel As Boolean)
    If sdbcMonCod.Text = "" Then
        Exit Sub
    Else
        Dim ador As ador.Recordset
        Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbcMonCod.Text, True)
        If ador.RecordCount = 0 Then
            NoValida (sIdiMon)
            sdbcMonCod.Text = ""
            Cancel = True
        End If
        ador.Close
        Set ador = Nothing
    End If
End Sub
Private Sub sdbcPaiCod_Change()
 
    If Not g_bRespetarCombo Then
        sdbcProviCod.Text = ""
        g_bRespetarCombo = True
        m_CargarComboDesde = True
    End If
    
End Sub

Private Sub sdbcPaiCod_CloseUp()
    If sdbcPaiCod.Value = "..." Then
        sdbcPaiCod.Text = ""
        Exit Sub
    End If
    
    g_bRespetarCombo = True
    sdbcPaiCod.Text = sdbcPaiCod.Columns(1).Text
    sdbcProviCod.Text = ""
    g_bRespetarCombo = False
    
    m_CargarComboDesde = False
    
End Sub

Private Sub sdbcPaiCod_DropDown()
Dim ador As ador.Recordset
    
   
    sdbcPaiCod.RemoveAll
    
    If m_CargarComboDesde Then
        Set ador = g_oRaiz.DevolverPaisesDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbcPaiCod.Columns(1).Text, , False)
    Else
        Set ador = g_oRaiz.DevolverPaisesDelPortalDesde(basParametros.g_iCargaMaximaCombos, , , False)
    End If
    
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbcPaiCod.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value
            ador.MoveNext
        
        Wend
        
        ador.Close
        Set ador = Nothing
        
    End If
    
        
End Sub

Private Sub sdbcPaiCod_InitColumnProps()
    
    sdbcPaiCod.DataFieldList = "Column 1"
    sdbcPaiCod.DataFieldToDisplay = "Column 1"
    
End Sub




Private Sub sdbcPaiCod_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPaiCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPaiCod.Rows - 1
            bm = sdbcPaiCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPaiCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcPaiCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcPaiCod_Validate(Cancel As Boolean)
        If sdbcPaiCod.Text = "" Then
        Exit Sub
    Else
        Dim ador As ador.Recordset
        Set ador = g_oRaiz.DevolverPaisesDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbcPaiCod.Text, True)
        If ador.RecordCount = 0 Then
            NoValido ("Pais")
            sdbcPaiCod.Text = ""
            Cancel = True
        End If
        ador.Close
        Set ador = Nothing
    End If

End Sub
Private Sub sdbcProviCod_Change()

    If Not g_bRespetarCombo Then
    
        g_bRespetarCombo = True
        
        m_CargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcProviCod_CloseUp()
    
    If sdbcPaiCod.Text = "..." Then
        sdbcPaiCod.Text = ""
        Exit Sub
    End If
    
    g_bRespetarCombo = True
    sdbcProviCod.Text = sdbcProviCod.Columns(1).Text
    g_bRespetarCombo = False
    
    m_CargarComboDesde = False
End Sub

Private Sub sdbcProviCod_DropDown()
Dim ador As ador.Recordset
    
    sdbcProviCod.RemoveAll
    
    If sdbcPaiCod.Columns("ID").Value <> "" Then
    
        Set ador = g_oRaiz.DevolverTodasLasProvinciasDesde(sdbcPaiCod.Columns("ID").Value, , , False)
   
    End If
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbcProviCod.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value
            ador.MoveNext
        
        Wend
        
        ador.Close
        Set ador = Nothing
        
    End If

        
End Sub

Private Sub sdbcProviCod_InitColumnProps()
    
    sdbcProviCod.DataFieldList = "Column 1"
    sdbcProviCod.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcProviCod_Validate(Cancel As Boolean)
    Dim ador As ador.Recordset
    If sdbcProviCod.Text = "" Then
        Exit Sub
    Else
        If sdbcPaiCod.Text = "" Then
            NoValida (sIdiProvincia)
            sdbcProviCod.Text = ""
            Cancel = True
        Else
            Set ador = g_oRaiz.DevolverTodasLasProvinciasDesde(sdbcPaiCod.Columns(0).Value, , sdbcProviCod.Text, True)
            If ador.RecordCount = 0 Then
                NoValida (sIdiProvincia)
                sdbcProviCod.Text = ""
                Cancel = True
            End If
            ador.Close
            Set ador = Nothing
        End If
    End If

End Sub
Private Sub sdbcProviCod_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProviCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProviCod.Rows - 1
            bm = sdbcProviCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProviCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcProviCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Function ObtenerCriterio() As String
    Dim sCriterio As String
    
    sCriterio = ""
    If txtCod <> "" Then
        sCriterio = sCriterio & " " & sIdiCodigo & ":" & txtCod.Text
    End If
    If txtDen <> "" Then
        sCriterio = sCriterio & " " & sIdiDen & ":" & txtCod.Text
    End If
    If sdbcPaiCod.Text <> "" Then
        sCriterio = sCriterio & "  " & sIdiPai & ":" & sdbcPaiCod.Text
    End If
    If sdbcProviCod.Text <> "" Then
        sCriterio = sCriterio & "  " & sIdiProvincia & ":" & sdbcProviCod.Text
    End If
    If sdbcMonCod.Text <> "" Then
        sCriterio = sCriterio & "  " & sIdiMon & ":" & sdbcMonCod.Text
    End If
    If txtVol.Text <> "" Then
        sCriterio = sCriterio & "  " & sIdiVolFact & " > " & txtVol.Text
    End If
    If txtVolHasta.Text <> "" Then
        sCriterio = sCriterio & " " & sIdiVolFact & " < " & txtVolHasta.Text
    End If
    If txtHom1.Text <> "" Then
        sCriterio = sCriterio & "  " & sIdiHom & ":" & txtHom1.Text
    End If
    If txtCliRef1.Text <> "" Then
        sCriterio = sCriterio & " " & sIdiCliRef & ":" & txtCliRef1.Text
    End If
    If lblActividad.Caption <> "" Then
        sCriterio = sCriterio & "  " & sIdiAct & ": " & lblActividad.Caption
    End If
    
    If chkFunProAut.Value = 1 Or chkFunProDes.Value = 1 Or chkFunProPen.Value = 1 Then
        sCriterio = sCriterio & "  " & fraAcceso.Caption
        If chkFunProAut.Value = 1 Then
            sCriterio = sCriterio & " " & sIdiAutorizada
        End If
        If chkFunProDes.Value = 1 Then
            sCriterio = sCriterio & " " & sIdiDesautorizada
        End If
        If chkFunProPen.Value = 1 Then
            sCriterio = sCriterio & " " & sIdiPendAuto
        End If
    End If
    
    If sCriterio = "" Then
        ObtenerCriterio = sIdiTodas
    Else
        ObtenerCriterio = sCriterio
    End If
End Function


Private Sub CargarRecursos()
Dim ador As ador.Recordset

Dim sMoncentral As String
Dim sMon As String
    On Error Resume Next
        
    Dim oMon As CMoneda
    Set oMon = g_oRaiz.Generar_CMoneda
    oMon.Id = g_udtParametrosGenerales.g_iMonedaCentral
    oMon.cargarMoneda
    sMoncentral = oMon.Cod

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTCIAS, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        ador.MoveNext
        Me.chkEsp.Caption = ador(0).Value
        ador.MoveNext
        'Me.chkFunCom.Caption = Ador(0).Value
        ador.MoveNext
        
        chkFunProAut.Caption = ador(0).Value
        sIdiAutorizada = ador(0).Value
        ador.MoveNext
        
        chkFunProDes.Caption = ador(0).Value
        sIdiDesautorizada = ador(0).Value
        ador.MoveNext
        
        Me.chkFunProPen.Caption = ador(0).Value
        sIdiPendAuto = ador(0).Value
        ador.MoveNext
        
        ador.MoveNext
        
        Me.chkPremium.Caption = ador(0).Value
        ador.MoveNext
        Me.chkUsu.Caption = ador(0).Value
        ador.MoveNext
        Me.chkUsuDet.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdObtener.Caption = ador(0).Value
        ador.MoveNext
        sMon = ador(0).Value
        sMon = Replace(sMon, "XXX", sMoncentral)
        Me.Label14.Caption = sMon
        ador.MoveNext
        Me.Label15.Caption = ador(0).Value & ":"
        sIdiMon = ador(0).Value
        ador.MoveNext
        Me.Label16.Caption = ador(0).Value & ":"
        sIdiProvincia = ador(0).Value
        ador.MoveNext
        Me.Label17.Caption = ador(0).Value & ":"
        sIdiAct = ador(0).Value
        ador.MoveNext
        Me.Label19.Caption = ador(0).Value & ":"
        sIdiHom = ador(0).Value
        ador.MoveNext
        Me.Label20.Caption = ador(0).Value
        ador.MoveNext
        Me.Label3.Caption = ador(0).Value & ":"
        sIdiPai = ador(0).Value
        ador.MoveNext
        Me.Label5.Caption = ador(0).Value
        ador.MoveNext
        Me.Label7.Caption = ador(0).Value & ":"
        sIdiCliRef = ador(0).Value
        ador.MoveNext
        Me.Label8.Caption = ador(0).Value & ":"
        sIdiCodigo = ador(0).Value
        sIditxtCodigo = ador(0).Value
        Me.sdbcMonCod.Columns(2).Caption = ador(0).Value
        Me.sdbcPaiCod.Columns(2).Caption = ador(0).Value
        Me.sdbcProviCod.Columns(2).Caption = ador(0).Value
        Me.optOrdCod.Caption = ador(0).Value
        optOrdAutCIA(0).Caption = ador(0).Value
        ador.MoveNext
        Me.opDetalle.Caption = ador(0).Value
        ador.MoveNext
        Me.opResum.Caption = ador(0).Value
        ador.MoveNext
        Me.Label1.Caption = ador(0).Value & ":"
        sIdiCol(3) = ador(0).Value
        Me.sdbcMonCod.Columns(1).Caption = ador(0).Value
        Me.sdbcPaiCod.Columns(1).Caption = ador(0).Value
        Me.sdbcProviCod.Columns(1).Caption = ador(0).Value
        Me.optOrdDen.Caption = ador(0).Value
        optOrdAutCIA(1).Caption = ador(0).Value
        sIdiDen = ador(0).Value
        ador.MoveNext
        Me.SSTab1.TabCaption(0) = ador(0).Value
        ador.MoveNext
        Me.SSTab1.TabCaption(1) = ador(0).Value
        ador.MoveNext
        sIdiVolFact = ador(0).Value
        ador.MoveNext
'        sIdiSi = Ador(0).Value '= "S"
        ador.MoveNext
        sIditxtProveSolicit = ador(0).Value
'        sIdiNo = Ador(0).Value '= "N"
        ador.MoveNext
        sIdiCompradora = ador(0).Value '30
        ador.MoveNext
        sIdiProveedora = ador(0).Value
        ador.MoveNext
        sIdiTodas = ador(0).Value
        ador.MoveNext
        
        sIditxtNombre = ador(0).Value
        ador.MoveNext
        sIditxtCP = ador(0).Value
        ador.MoveNext
        sIditxtDireccion = ador(0).Value
        ador.MoveNext
        sIditxtPoblacion = ador(0).Value
        ador.MoveNext
        sIditxtTituloResumido = ador(0).Value
        ador.MoveNext
        sIditxtCompania = ador(0).Value
        ador.MoveNext
        sIditxtFunc = ador(0).Value
        ador.MoveNext
        sIditxtNIF = ador(0).Value
        ador.MoveNext
        sIditxtWWW = ador(0).Value
        ador.MoveNext
        sMon = ador(0).Value
        sMon = Replace(sMon, "XXX", sMoncentral)
        sIditxtVolFact = sMon
        ador.MoveNext
        sIditxtUsus = ador(0).Value
        ador.MoveNext
        sIditxtAdjuntos = ador(0).Value
        ador.MoveNext
        sIditxtProveAut = ador(0).Value
        ador.MoveNext
        sIditxtProveDesaut = ador(0).Value
        ador.MoveNext
        sIditxtTipoUsu = ador(0).Value
        ador.MoveNext
        sIditxtDepto = ador(0).Value
        ador.MoveNext
        sIditxtTfno = ador(0).Value
        ador.MoveNext
        sIditxtEmail = ador(0).Value
        ador.MoveNext
        sIditxtCargo = ador(0).Value
        ador.MoveNext
        sIditxtFax = ador(0).Value
        ador.MoveNext
        sIditxtTfnoMovil = ador(0).Value
        ador.MoveNext
        sIditxtTituloDetalle = ador(0).Value
        ador.MoveNext
        sIdiTxtPag = ador(0).Value
        ador.MoveNext
        sIdiTxtDe = ador(0).Value
        ador.MoveNext
        sIdiTxtHomol = ador(0).Value
        ador.MoveNext
        sIdiTxtComent = ador(0).Value
        ador.MoveNext
        sIdiTxtCliRef = ador(0).Value
        
        ador.MoveNext
        sIdiTxtCodGS = ador(0).Value
        ador.MoveNext
        Me.Frame5.Caption = ador(0).Value
        Me.Frame2.Caption = ador(0).Value
        ador.MoveNext
        fraAcceso.Caption = ador(0).Value
        
        ador.MoveNext
        lblAutDesde.Caption = ador(0).Value & ":"
        lblSolDesde.Caption = ador(0).Value & ":"
        lblDesAutDesde.Caption = ador(0).Value & ":"
        sIditxtDesde = ador(0).Value
        
        ador.MoveNext
        lblAutHasta.Caption = ador(0).Value & ":"
        lblSolHasta.Caption = ador(0).Value & ":"
        lblDesAutHasta.Caption = ador(0).Value & ":"
        sIditxtHasta = ador(0).Value
        
        ador.MoveNext
        sIditxtAccesoAutorizado = ador(0).Value
        ador.MoveNext
        sIditxtAccesoDesautorizado = ador(0).Value
        ador.MoveNext
        sIditxtAccesoSolicitado = ador(0).Value
        ador.MoveNext
        sIditxtPMVinculada = ador(0).Value
        ador.MoveNext
        sIdiCol(0) = ador(0).Value
        ador.MoveNext
        sIdiCol(1) = ador(0).Value
        ador.MoveNext
        sIdiCol(2) = ador(0).Value
        ador.MoveNext
        sIdiCol(4) = ador(0).Value
        ador.MoveNext
        sIdiCol(5) = ador(0).Value
        ador.MoveNext
        sIdiCol(6) = ador(0).Value
        ador.MoveNext
        sIdiCol(7) = ador(0).Value
        ador.Close
    End If
    
    Set ador = Nothing
           
End Sub

Private Sub txtAutDesde_Validate(Cancel As Boolean)
    If Not IsDate(txtAutDesde.Text) And Not txtAutDesde.Text = "" Then
        basMensajes.NoValida (fraAcceso.Caption & " " & sIdiAutorizada & " " & sIditxtDesde)
        txtAutDesde.Text = ""
        Cancel = True
    End If
End Sub


Private Sub txtAutHasta_Validate(Cancel As Boolean)
    If Not IsDate(txtAutHasta.Text) And Not txtAutHasta.Text = "" Then
        basMensajes.NoValida (fraAcceso.Caption & " " & sIdiAutorizada & " " & sIditxtHasta)
        txtAutHasta.Text = ""
        Cancel = True
    End If
End Sub


Private Sub txtDesAutDesde_Validate(Cancel As Boolean)
    If Not IsDate(txtDesAutDesde.Text) And Not txtDesAutDesde.Text = "" Then
        basMensajes.NoValida (fraAcceso.Caption & " " & sIdiDesautorizada & " " & sIditxtDesde)
        txtDesAutDesde.Text = ""
        Cancel = True
    End If
End Sub


Private Sub txtDesAutHasta_Validate(Cancel As Boolean)
    If Not IsDate(txtDesAutHasta.Text) And Not txtDesAutHasta.Text = "" Then
        basMensajes.NoValida (fraAcceso.Caption & " " & sIdiDesautorizada & " " & sIditxtHasta)
        txtDesAutHasta.Text = ""
        Cancel = True
    End If
End Sub


Private Sub txtSolDesde_Validate(Cancel As Boolean)
    If Not IsDate(txtSolDesde.Text) And Not txtSolDesde.Text = "" Then
        basMensajes.NoValida (fraAcceso.Caption & " " & sIdiPendAuto & " " & sIditxtDesde)
        txtSolDesde.Text = ""
        Cancel = True
    End If
End Sub


Private Sub txtSolHasta_Validate(Cancel As Boolean)
    If Not IsDate(txtSolHasta.Text) And Not txtSolHasta.Text = "" Then
        basMensajes.NoValida (fraAcceso.Caption & " " & sIdiPendAuto & " " & sIditxtHasta)
        txtSolHasta.Text = ""
        Cancel = True
    End If
End Sub

Private Sub cmdCalAutDesde_Click()
    'Muestra el formulario del calendario
    
    Set frmCalendar.frmDestination = frmLstCIAS
    Set frmCalendar.ctrDestination = txtAutDesde
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtAutDesde <> "" Then
        frmCalendar.Calendar.Value = txtAutDesde
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub

Private Sub cmdCalAutHasta_Click()
    Set frmCalendar.frmDestination = frmLstCIAS
    Set frmCalendar.ctrDestination = txtAutHasta
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtAutHasta <> "" Then
        frmCalendar.Calendar.Value = txtAutHasta
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub


Private Sub cmdCalDesAutDesde_Click()
    Set frmCalendar.frmDestination = frmLstCIAS
    Set frmCalendar.ctrDestination = txtDesAutDesde
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtDesAutDesde <> "" Then
        frmCalendar.Calendar.Value = txtDesAutDesde
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub


Private Sub cmdCalDesAutHasta_Click()
    Set frmCalendar.frmDestination = frmLstCIAS
    Set frmCalendar.ctrDestination = txtDesAutHasta
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtDesAutHasta <> "" Then
        frmCalendar.Calendar.Value = txtDesAutHasta
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub


Private Sub cmdCalSolDesde_Click()
    Set frmCalendar.frmDestination = frmLstCIAS
    Set frmCalendar.ctrDestination = txtSolDesde
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtSolDesde <> "" Then
        frmCalendar.Calendar.Value = txtSolDesde
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub


Private Sub cmdCalSolHasta_Click()
    Set frmCalendar.frmDestination = frmLstCIAS
    Set frmCalendar.ctrDestination = txtSolHasta
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtSolHasta <> "" Then
        frmCalendar.Calendar.Value = txtSolHasta
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub

