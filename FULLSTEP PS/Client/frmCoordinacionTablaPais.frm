VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCoordinacionTablaPais 
   Caption         =   "Coordinaci�n tabla de paises"
   ClientHeight    =   3555
   ClientLeft      =   690
   ClientTop       =   2295
   ClientWidth     =   9660
   Icon            =   "frmCoordinacionTablaPais.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   3555
   ScaleWidth      =   9660
   Begin VB.CommandButton cmdmodlistado 
      Caption         =   "&Listado"
      Height          =   345
      Left            =   0
      TabIndex        =   4
      Top             =   3060
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddPaiPortDen 
      Height          =   1545
      Left            =   4380
      TabIndex        =   1
      Top             =   1080
      Width           =   4965
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5662
      Columns(0).Caption=   "Den.en el portal"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2699
      Columns(1).Caption=   "C�digo en el portal"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   8758
      _ExtentY        =   2725
      _StockProps     =   77
      ForeColor       =   0
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.CommandButton cmdModoEdicion 
      Caption         =   "&Edici�n"
      Height          =   345
      Left            =   8610
      TabIndex        =   2
      Top             =   2985
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddPaiPortCod 
      Height          =   1545
      Left            =   4140
      TabIndex        =   0
      Top             =   1080
      Width           =   4965
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2699
      Columns(0).Caption=   "C�digo en el portal"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5662
      Columns(1).Caption=   "Den.en el portal"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   8758
      _ExtentY        =   2725
      _StockProps     =   77
      ForeColor       =   0
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTabla 
      Height          =   2865
      Left            =   60
      TabIndex        =   3
      Top             =   60
      Width           =   9555
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   4
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCoordinacionTablaPais.frx":0CB2
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   2990
      Columns(0).Caption=   "C�digo de FullStepGS"
      Columns(0).Name =   "COD_FSGS"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   3200
      Columns(1).Caption=   "Den. de FullStepGS"
      Columns(1).Name =   "DEN_FSGS"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   2725
      Columns(2).Caption=   "C�digo del portal"
      Columns(2).Name =   "COD_FSP"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "Den. del portal"
      Columns(3).Name =   "DEN_FSP"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   16854
      _ExtentY        =   5054
      _StockProps     =   79
      ForeColor       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmCoordinacionTablaPais"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public oCia As CCia
Private CodPaiSeleccionado As String
Private IdPaiPortalSeleccionado As Integer
Private CodPaiPortalSeleccionado As String
Private bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean
Private bHayErrores As Boolean
Private bRespetarDropdown As Boolean


Private sIdiEdicion As String
Private sIdiConsulta As String



Private Sub cmdmodlistado_Click()
'Load frmLstCoorPai
'frmLstCoorPai.Visible = True
 frmLstCoorPai.WindowState = vbNormal
    Set frmLstCoorPai.oCiaSeleccionada = oCia
    If Not oCia Is Nothing Then
       frmLstCoorPai.sdbcCiaCod.Text = oCia.Cod
       frmLstCoorPai.sdbcCiaCod_Validate False
       
    End If
    frmLstCoorPai.SetFocus
End Sub

Private Sub cmdModoEdicion_Click()
    
    If sdbgTabla.AllowUpdate Then
        'Pasar a modo consulta
        sdbgTabla.Update
        DoEvents
        If Not bHayErrores Then
            
            sdbgTabla.AllowUpdate = False
            cmdModoEdicion.Caption = sIdiEdicion
        End If
        
    Else
        'Pasar a modo edici�n
        sdbgTabla.AllowUpdate = True
        cmdModoEdicion.Caption = sIdiConsulta
    End If
    
End Sub

Private Sub Form_Load()
Dim i As Integer
Dim Ador As Ador.Recordset
    
    Width = 7335
    Height = 3930
    CargarRecursos
    
    sdbgTabla.Columns("COD_FSP").DropDownHwnd = sdbddPaiPortCod.HWND
    sdbddPaiPortCod.AddItem ""
    sdbgTabla.Columns("DEN_FSP").DropDownHwnd = sdbddPaiPortDen.HWND
    sdbddPaiPortDen.AddItem ""
    sdbgTabla.AllowUpdate = False
    cmdModoEdicion.Visible = True
    
    
End Sub


Private Sub Form_Resize()
    
    If Me.Height < 1000 Then Exit Sub
    If Me.Width < 1005 Then Exit Sub
    sdbgTabla.Width = Me.Width - 200
    sdbgTabla.Height = Me.Height - 1000
    
    sdbgTabla.Columns("COD_FSGS").Width = sdbgTabla.Width * 0.2
    sdbgTabla.Columns("DEN_FSGS").Width = sdbgTabla.Width * 0.3
    sdbgTabla.Columns("COD_FSP").Width = sdbgTabla.Width * 0.2
    sdbgTabla.Columns("DEN_FSP").Width = sdbgTabla.Width * 0.3 - 570
    cmdModoEdicion.Left = sdbgTabla.Width - 1005
    cmdModoEdicion.Top = Me.Height - 780
    cmdmodlistado.Left = sdbgTabla.Left
    cmdmodlistado.Top = Me.Height - 780
    
   
    
    
    
End Sub



'Private Sub CargarProvincias()
'Dim ador As ador.Recordset
    
'    sdbgTabla.RemoveAll
    
'    Set ador = oCia.DevolverProvinciasEnCia(IdPaiPortalSeleccionado, CodPaiSeleccionado)
'    If Not ador Is Nothing Then
            
'        bRespetarDropdown = True
    
'        While Not ador.EOF
        
'            sdbgTabla.AddItem ador("COD").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("FSP_COD").Value & Chr(9) & ador("FSP_DEN").Value
'            ador.MoveNext
            
'        Wend
        
'        bRespetarDropdown = False
    
'    End If
    
'    ador.Close
'    Set ador = Nothing
    

'End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set oCia = Nothing
End Sub

Private Sub sdbddPaiPortCod_CloseUp()
    
    sdbgTabla.Columns("DEN_FSP").Value = sdbddPaiPortCod.Columns("DEN").Value
    
End Sub

Private Sub sdbddPaiPortCod_DropDown()
Dim Ador As Ador.Recordset
    
    If sdbgTabla.AllowUpdate = False Then
       sdbddPaiPortCod.DroppedDown = False
    End If
    
    
    sdbddPaiPortCod.RemoveAll
    
    Set Ador = g_oRaiz.DevolverPaisesDelPortalDesde(basParametros.g_iCargaMaximaCombos, sdbgTabla.ActiveCell.Value, , False)
    
    If Not Ador Is Nothing Then
        
        While Not Ador.EOF
        
            sdbddPaiPortCod.AddItem Ador("COD").Value & Chr(9) & Ador("DEN").Value
            Ador.MoveNext
        
        Wend
        
        Ador.Close
        Set Ador = Nothing
        
    End If
    
        
        
    
End Sub

Private Sub sdbddPaiPortCod_InitColumnProps()
    
    sdbddPaiPortCod.DataFieldList = "Column 0"
    sdbddPaiPortCod.DataFieldToDisplay = "Column 0"
    
    'sdbddArticulos.DataFieldList = "Column 0"
    'sdbddArticulos.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddPaiPortCod_PositionList(ByVal Text As String)
      
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddPaiPortCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddPaiPortCod.Rows - 1
            bm = sdbddPaiPortCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddPaiPortCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddPaiPortCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddPaiPortCod_ValidateList(Text As String, RtnPassed As Integer)
Dim scod As String
Dim Ador As Ador.Recordset
    
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True
        
    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddPaiPortCod.Columns("COD").Value = Text Then
            RtnPassed = True
            sdbgTabla.Columns("DEN_FSP").Value = sdbddPaiPortCod.Columns("DEN").Value
            Exit Sub
        Else
            Set Ador = g_oRaiz.DevolverPaisesDelPortalDesde(basParametros.g_iCargaMaximaCombos, sdbgTabla.Columns("COD_FSP").Value, , True)
            
            If Ador Is Nothing Then
                RtnPassed = 0
            Else
                If Ador.EOF Then
                    RtnPassed = 0
                Else
                    
                    
                    sdbgTabla.Columns("COD_FSP").Value = Ador("COD").Value
                    sdbgTabla.Columns("DEN_FSP").Value = Ador("DEN").Value
                        
                    RtnPassed = 1
                End If
            End If
            
            
        End If
        
    End If
End Sub

Private Sub sdbddPaiPortDen_CloseUp()
    
    sdbgTabla.Columns("COD_FSP").Value = sdbddPaiPortDen.Columns("COD").Value
    
End Sub

Private Sub sdbddPaiPortDen_DropDown()
    Dim Ador As Ador.Recordset
    
    If sdbgTabla.AllowUpdate = False Then
       sdbddPaiPortDen.DroppedDown = False
    End If
    
    
    sdbddPaiPortDen.RemoveAll
    
    Set Ador = g_oRaiz.DevolverPaisesDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbgTabla.ActiveCell.Value, , True)
    
    If Not Ador Is Nothing Then
        
        While Not Ador.EOF
        
            sdbddPaiPortDen.AddItem Ador("DEN").Value & Chr(9) & Ador("COD").Value
            Ador.MoveNext
        
        Wend
        Ador.Close
        Set Ador = Nothing
        
    End If
    
        
End Sub

Private Sub sdbddPaiPortDen_InitColumnProps()

    sdbddPaiPortDen.DataFieldList = "Column 0"
    sdbddPaiPortDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbddPaiPortDen_PositionList(ByVal Text As String)
     Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddPaiPortDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddPaiPortDen.Rows - 1
            bm = sdbddPaiPortCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddPaiPortDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddPaiPortDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbgTabla_BeforeUpdate(Cancel As Integer)
Dim oError As CTESError

    'Codigo para hacer el update
    If sdbgTabla.Columns("COD_FSP").Value <> "" And sdbgTabla.Columns("DEN_FSP").Value <> "" Then
        
        Set oError = oCia.EnlazarPais(sdbgTabla.Columns("COD_FSGS").Value, sdbgTabla.Columns("COD_FSP").Value)
        If oError.NumError <> 0 Then
            basErrores.TratarError oError
            bHayErrores = True
            Cancel = True
        Else
            bHayErrores = False
        End If
    Else
        If sdbgTabla.Columns("COD_FSP").Value = "" Then
            Set oError = oCia.EnlazarPais(sdbgTabla.Columns("COD_FSGS").Value, "")
            If oError.NumError <> 0 Then
                    basErrores.TratarError (oError)
                    bHayErrores = True
                    Cancel = True
                Else
                    sdbgTabla.Columns("DEN_FSP").Value = ""
                    bHayErrores = False
                End If
            Else
                Cancel = True
                bHayErrores = True
            End If
    End If
    
End Sub

Private Sub sdbgTabla_Change()
    
    'If Not bRespetarDropdown Then
     '   sdbgTabla.Columns("COD_FSP").Value = ""
      '  sdbgTabla.Columns("DEN_FSP").Value = ""
    'End If
    
End Sub

Private Sub sdbgTabla_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyEscape Then
        sdbgTabla.CancelUpdate
        bHayErrores = False
    End If
     
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_COORDINACIONTABLAPAIS, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdmodlistado.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdModoEdicion.Caption = Ador(0).Value
        sIdiEdicion = Ador(0).Value
        Ador.MoveNext
        Me.sdbddPaiPortCod.Columns(0).Caption = Ador(0).Value
        Me.sdbddPaiPortDen.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbddPaiPortCod.Columns(1).Caption = Ador(0).Value
        Me.sdbddPaiPortDen.Columns(0).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgTabla.Columns(0).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgTabla.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgTabla.Columns(2).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgTabla.Columns(3).Caption = Ador(0).Value
        Ador.MoveNext
        sIdiConsulta = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub


