VERSION 5.00
Begin VB.MDIForm frmMDI 
   BackColor       =   &H8000000C&
   Caption         =   "FULLSTEP PS"
   ClientHeight    =   7230
   ClientLeft      =   2370
   ClientTop       =   2055
   ClientWidth     =   10380
   Icon            =   "frmMDI.frx":0000
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin VB.Menu mnuMantenimiento 
      Caption         =   "&Mantenimiento"
      Begin VB.Menu mnuMonedas 
         Caption         =   "Monedas"
      End
      Begin VB.Menu mnuTipoComunic 
         Caption         =   "Tipos de comunicaci�n"
      End
      Begin VB.Menu mnuActividades 
         Caption         =   "Actividades"
      End
   End
   Begin VB.Menu mnuGestion 
      Caption         =   "&Gesti�n"
      Begin VB.Menu mnuCompa�ias 
         Caption         =   "Compa��as"
      End
      Begin VB.Menu mnuAccesosPremium 
         Caption         =   "Enlace de proveedores"
         Begin VB.Menu mnuEnlazarProve 
            Caption         =   "Enlaces por compa��as proveedoras"
         End
         Begin VB.Menu mnuEnlazarComp 
            Caption         =   "Enlaces por compa��as compradoras"
         End
      End
      Begin VB.Menu mnuConfirmarAct 
         Caption         =   "Solicitudes de cambio de actividad"
      End
      Begin VB.Menu mnuConfirmarRegComp 
         Caption         =   "Solicitudes de cambio de registro en compradoras"
      End
      Begin VB.Menu mnuRegComunic 
         Caption         =   "Registro de comunicaciones"
      End
      Begin VB.Menu mnuAcciones 
         Caption         =   "Acciones"
      End
   End
   Begin VB.Menu mnuInformes 
      Caption         =   "&Informes"
      Begin VB.Menu mnuVolumenUnaCompradora 
         Caption         =   "Volumen adjudicado"
      End
      Begin VB.Menu mnuVolumenAdjudicado 
         Caption         =   "Volumen adjudicado"
         Begin VB.Menu mnuVolPorProve 
            Caption         =   "Por proveedor"
         End
         Begin VB.Menu mnuVolGeneral 
            Caption         =   "General"
         End
      End
      Begin VB.Menu mnuListados 
         Caption         =   "Listados"
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Coordinaci�n de datos"
      Begin VB.Menu mnuControlConectividad 
         Caption         =   "Control de conectividad"
      End
      Begin VB.Menu mnuCoordinacionTablas 
         Caption         =   "Coordinaci�n de tablas base"
      End
      Begin VB.Menu mnuCoordinacionMonedas 
         Caption         =   "Coordinaci�n de monedas centrales"
      End
      Begin VB.Menu mnusincronizarusuarios 
         Caption         =   "Sincronizar usuarios de compras conjuntas"
      End
   End
   Begin VB.Menu mnuParametros 
      Caption         =   "&Par�metros"
   End
   Begin VB.Menu mnuEstrActRaiz 
      Caption         =   "POPUPEstrActRaiz"
      Visible         =   0   'False
      Begin VB.Menu mnuAnyadirAct1 
         Caption         =   "A�adir actividad nivel 1"
      End
      Begin VB.Menu mnu1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuOrdCod 
         Caption         =   "Ordenado por c�digo"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuOrdDen 
         Caption         =   "Ordenado por denominaci�n"
      End
   End
   Begin VB.Menu mnuEstrAct 
      Caption         =   "POPUPEstrAct"
      Visible         =   0   'False
      Begin VB.Menu mnuAnyadir 
         Caption         =   "A�adir"
      End
      Begin VB.Menu mnu2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuModif 
         Caption         =   "Modificar"
      End
      Begin VB.Menu mnuEliminar 
         Caption         =   "Eliminar"
      End
      Begin VB.Menu mnuCamCod 
         Caption         =   "Cambiar c�digo"
      End
      Begin VB.Menu mnu3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDetalle 
         Caption         =   "Detalle"
      End
   End
   Begin VB.Menu mnuSeguridad 
      Caption         =   "&Seguridad"
      Begin VB.Menu mnuCambContr 
         Caption         =   "Cambiar contrase�a"
      End
   End
End
Attribute VB_Name = "frmMDI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private sIdiSync As String
 
Private Sub MDIForm_Load()
    CargarRecursos
    
    '****************************************************
    'OCULTAR FUNCIONALIDAD DE MANTENIMIENTO DE TIPOS DE COMUNICACIONES
    mnuTipoComunic.Visible = False
    '****************************************************
    
    If Not g_udtParametrosGenerales.g_bPremium Then
        mnuAccesosPremium.Enabled = False
        mnuAccesosPremium.Visible = False
    End If
    If Not g_udtParametrosGenerales.g_bCollab Then
        mnusincronizarusuarios.Enabled = False
        mnusincronizarusuarios.Visible = False
    End If
    If g_udtParametrosGenerales.g_bUnaCompradora Then
        mnuAccesosPremium.Visible = False
        mnuConfirmarRegComp.Visible = False
        mnuVolumenAdjudicado.Visible = False
        mnuVolumenUnaCompradora.Visible = True
        mnuOpciones.Visible = False
        mnuMonedas.Visible = False
    Else
        mnuVolumenUnaCompradora.Visible = False
    End If
    If g_udtParametrosGenerales.gbSincronizacion = True Then
        If mnuMonedas.Visible = False And mnuTipoComunic.Visible = False Then
            mnuMantenimiento.Visible = False
        Else
            mnuActividades.Visible = False
        End If
    End If
    
End Sub

''' <summary>
''' Descarga los objetos globales
''' </summary>
''' <param name="Cancel">Cancelar el cierre</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub MDIForm_Unload(Cancel As Integer)
    Set g_oGestorParametros = Nothing
    If Not (g_oMailSender Is Nothing) Then g_oMailSender.FuerzaFinalizeSmtpClient
    Set g_oMailSender = Nothing
    Set g_oGestorInformes = Nothing
    Set g_oRaiz = Nothing
    g_sInstancia = ""
    End
End Sub

Private Sub mnuAcciones_Click()
    frmRegAcciones.WindowState = vbNormal
    frmRegAcciones.SetFocus
End Sub

Private Sub mnuActividades_Click()
frmActiv.WindowState = vbNormal
frmActiv.SetFocus
End Sub

Private Sub mnuAnyadir_Click()
 frmActiv.cmdA�adir_Click
End Sub

Private Sub mnuAnyadirAct1_Click()
 frmActiv.cmdA�adir_Click
End Sub

Private Sub mnuCambContr_Click()
    Screen.MousePointer = vbHourglass
    frmCambContr.Show 1
End Sub

Private Sub mnuCamCod_Click()
    frmActiv.CambiarCodigo
End Sub

Private Sub mnuCompa�ias_Click()
    frmCompanias.WindowState = vbNormal
    frmCompanias.g_sOrigen = "frmCompanias"
    frmCompanias.SetFocus
End Sub

Private Sub mnuConfirmarAct_Click()
frmConfirmarActCia.WindowState = vbNormal
frmConfirmarActCia.SetFocus
End Sub

Private Sub mnuConfirmarRegComp_Click()
frmConfirmarRegCias.WindowState = vbNormal
frmConfirmarRegCias.SetFocus

End Sub

Private Sub mnuControlConectividad_Click()
    frmConectSel.WindowState = vbNormal
    frmConectSel.SetFocus
End Sub

Private Sub mnuCoordinacionMonedas_Click()
    frmCoordinacionMonedasCentrales.WindowState = vbNormal
    frmCoordinacionMonedasCentrales.SetFocus
End Sub

Private Sub mnuCoordinacionTablas_Click()
    frmCoordinacionTablasSel.WindowState = vbNormal
    frmCoordinacionTablasSel.SetFocus
End Sub

Private Sub mnuDetalle_Click()
    frmActiv.mnuPopUpDetalle
End Sub

Private Sub mnuEliminar_Click()
    frmActiv.cmdEli_Click
End Sub

Private Sub mnuEnlazarComp_Click()
    frmEnlazarComprador.WindowState = vbNormal
    frmEnlazarComprador.SetFocus
End Sub

Private Sub mnuEnlazarProve_Click()
    frmEnlazarProveedor.WindowState = vbNormal
    frmEnlazarProveedor.SetFocus
End Sub

Private Sub mnuListados_Click()
frmListados.WindowState = vbNormal
frmListados.SetFocus
End Sub

Private Sub mnuModif_Click()
 frmActiv.cmdModif_Click
End Sub

Private Sub mnuMonedas_Click()
    frmMON.WindowState = vbNormal
    frmMON.SetFocus
End Sub

Private Sub mnuOrdCod_Click()
    mnuOrdCod.Checked = Not mnuOrdCod.Checked
    mnuOrdDen.Checked = Not mnuOrdDen.Checked
    frmActiv.Ordenar Not mnuOrdCod.Checked
End Sub

Private Sub mnuOrdDen_Click()
    mnuOrdCod.Checked = Not mnuOrdCod.Checked
    mnuOrdDen.Checked = Not mnuOrdDen.Checked
    frmActiv.Ordenar Not mnuOrdCod.Checked
End Sub

Private Sub mnuParametros_Click()
    frmParametros.WindowState = vbNormal
    frmParametros.SetFocus
End Sub

Private Sub mnuRegComunic_Click()
    frmRegComunicaciones.WindowState = vbNormal
    frmRegComunicaciones.SetFocus
End Sub

Private Sub mnusincronizarusuarios_Click()
    Dim oError As CTESError
    frmEspera.Show
    DoEvents
    Set oError = g_oRaiz.SincronizarUsuarios
    If oError.NumError <> 0 Then
        MsgBox sIdiSync, vbExclamation, "FULLSTEP PS"
    End If
    Unload frmEspera
    
End Sub

Private Sub mnuTipoComunic_Click()
    frmCOM.WindowState = vbNormal
    frmCOM.SetFocus
End Sub

Private Sub mnuVolGeneral_Click()
    frmVolAdjudicado.WindowState = vbNormal
    frmVolAdjudicado.SetFocus
End Sub

Private Sub mnuVolPorProve_Click()
    frmVolAdjudicadoProv.WindowState = vbNormal
    frmVolAdjudicadoProv.SetFocus
End Sub



Private Sub mnuVolumenUnaCompradora_Click()
    frmVolAdjudicado.WindowState = vbNormal
    frmVolAdjudicado.SetFocus
End Sub


Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_MDI, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.mnuAccesosPremium.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuActividades.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuAnyadir.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuAnyadirAct1.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuCambContr.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuCamCod.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuCompa�ias.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuConfirmarAct.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuConfirmarRegComp.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuControlConectividad.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuCoordinacionMonedas.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuCoordinacionTablas.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuDetalle.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuEliminar.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuEnlazarComp.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuEnlazarProve.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuGestion.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuInformes.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuListados.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuMantenimiento.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuModif.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuMonedas.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuOpciones.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuOrdCod.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuOrdDen.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuParametros.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuRegComunic.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuSeguridad.Caption = ador(0).Value
        ador.MoveNext
        Me.mnusincronizarusuarios.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuTipoComunic.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuVolGeneral.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuVolPorProve.Caption = ador(0).Value
        ador.MoveNext
        Me.mnuVolumenAdjudicado.Caption = ador(0).Value
        Me.mnuVolumenUnaCompradora.Caption = ador(0).Value
        ador.MoveNext
        sIdiSync = ador(0).Value
        ador.MoveNext
        mnuAcciones.Caption = ador(0).Value
        
        ador.Close
    End If
    
    Set ador = Nothing
           
End Sub




