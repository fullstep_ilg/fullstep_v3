VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmEnlazarComprador 
   Caption         =   "Enlazar proveedores"
   ClientHeight    =   5295
   ClientLeft      =   1530
   ClientTop       =   2880
   ClientWidth     =   9255
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmEnlazarComprador.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5295
   ScaleWidth      =   9255
   Begin SSDataWidgets_B.SSDBGrid sdbgProve 
      Height          =   3825
      Left            =   30
      TabIndex        =   4
      Top             =   945
      Width           =   9150
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   6
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmEnlazarComprador.frx":0CB2
      UseGroups       =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Groups.Count    =   2
      Groups(0).Width =   9631
      Groups(0).Caption=   "Proveedor"
      Groups(0).Columns.Count=   3
      Groups(0).Columns(0).Width=   2672
      Groups(0).Columns(0).Caption=   "C�digo"
      Groups(0).Columns(0).Name=   "PREMCOD"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).Locked=   -1  'True
      Groups(0).Columns(0).HasBackColor=   -1  'True
      Groups(0).Columns(0).BackColor=   16777136
      Groups(0).Columns(1).Width=   6959
      Groups(0).Columns(1).Caption=   "Denominaci�n"
      Groups(0).Columns(1).Name=   "PREMDEN"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(1).Locked=   -1  'True
      Groups(0).Columns(1).HasBackColor=   -1  'True
      Groups(0).Columns(1).BackColor=   16777136
      Groups(0).Columns(2).Width=   3387
      Groups(0).Columns(2).Visible=   0   'False
      Groups(0).Columns(2).Caption=   "COMPID"
      Groups(0).Columns(2).Name=   "PREMID"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      Groups(1).Width =   5503
      Groups(1).Caption=   "Proveedor en FULLSTEP GS"
      Groups(1).Columns.Count=   3
      Groups(1).Columns(0).Width=   2646
      Groups(1).Columns(0).Caption=   "C�digo"
      Groups(1).Columns(0).Name=   "PROVECOD"
      Groups(1).Columns(0).DataField=   "Column 3"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(0).Locked=   -1  'True
      Groups(1).Columns(0).Style=   1
      Groups(1).Columns(1).Width=   1164
      Groups(1).Columns(1).Caption=   "Activo"
      Groups(1).Columns(1).Name=   "ACTIV"
      Groups(1).Columns(1).DataField=   "Column 4"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(1).Locked=   -1  'True
      Groups(1).Columns(1).Style=   2
      Groups(1).Columns(2).Width=   1693
      Groups(1).Columns(2).Caption=   "Material"
      Groups(1).Columns(2).Name=   "MAT"
      Groups(1).Columns(2).DataField=   "Column 5"
      Groups(1).Columns(2).DataType=   8
      Groups(1).Columns(2).FieldLen=   256
      Groups(1).Columns(2).Style=   4
      Groups(1).Columns(2).ButtonsAlways=   -1  'True
      _ExtentX        =   16140
      _ExtentY        =   6747
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   30
      ScaleHeight     =   525
      ScaleWidth      =   9180
      TabIndex        =   6
      Top             =   4755
      Width           =   9180
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         Height          =   345
         Left            =   15
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         Default         =   -1  'True
         Height          =   345
         Left            =   8160
         TabIndex        =   5
         Top             =   120
         Width           =   1005
      End
   End
   Begin VB.Frame fraCias 
      Caption         =   "Compa��a compradora"
      Height          =   825
      Left            =   45
      TabIndex        =   0
      Top             =   15
      Width           =   9090
      Begin VB.CommandButton cmdBuscar 
         Height          =   285
         Left            =   8490
         Picture         =   "frmEnlazarComprador.frx":0CCE
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   330
         Width           =   300
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaCod 
         Height          =   285
         Left            =   270
         TabIndex        =   1
         Top             =   330
         Width           =   1980
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   7064
         Columns(2).Caption=   "Denominaci�n"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FC"
         Columns(3).Name =   "FC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FP"
         Columns(4).Name =   "FP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   3492
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaDen 
         Height          =   285
         Left            =   2325
         TabIndex        =   2
         Top             =   330
         Width           =   6090
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5980
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "C�digo"
         Columns(2).Name =   "COD"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FC"
         Columns(3).Name =   "FC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FP"
         Columns(4).Name =   "FP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   10742
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
      End
   End
End
Attribute VB_Name = "frmEnlazarComprador"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Public sOrigen As String
Public oCiaSeleccionada As CCia
Private oCias As CCias
Private oEnlaces As CEnlaces
Private bModoEdicion As Boolean
Public bRespetarCombo As Boolean
Public bCargarComboDesde As Boolean
Private bLoad As Boolean
Public aWidth As Integer
Public aHeight As Integer
Private bNoIrABusqueda As Boolean

Private sIdiEdicion As String
Private sIdiConsulta As String
Private msIdiProve As String



Private Sub cmdBuscar_Click()
    frmCiaBuscar.g_sOrigen = "frmEnlazarComprador"
    frmCiaBuscar.WindowState = vbNormal
    frmCiaBuscar.Show 1

End Sub

Private Sub cmdListado_Click()
        Set frmListados.ofrmLstEnlaceProvePremium2 = New frmLstEnlaceProvePremium
        frmListados.ofrmLstEnlaceProvePremium2.sOrigen = "A2B2C2"
        frmListados.ofrmLstEnlaceProvePremium2.WindowState = vbNormal
        frmListados.ofrmLstEnlaceProvePremium2.SetFocus

End Sub


Private Sub sdbcCiaCod_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaDen.Text = ""
        sdbgProve.RemoveAll
        Set oCiaSeleccionada = Nothing
        bRespetarCombo = False
        
        bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCiaCod_Click()
    
    If Not sdbcCiaCod.DroppedDown Then
        sdbcCiaCod = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaCod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaCod.Rows - 1
            bm = sdbcCiaCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaCod_CloseUp()

    If sdbcCiaCod.Value = "-1" Then
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaCod.Text = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaCod.Columns(2).Text
    bRespetarCombo = True
    sdbcCiaCod.Text = sdbcCiaCod.Columns(1).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
    CiaSeleccionada
    
    
End Sub

Private Sub sdbcCiaCod_Validate(Cancel As Boolean)
Dim Ador As Ador.Recordset

    If Trim(sdbcCiaCod.Text) = "" Then Exit Sub
    
    Set Ador = oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , , , True)
            
    If Ador Is Nothing Then
        
        sdbcCiaCod = ""
        Exit Sub
        
    Else
        If Ador.EOF Then
            sdbcCiaCod.Value = ""
            Exit Sub
        Else
            If UCase(sdbcCiaCod.Text) <> UCase(Ador("COD").Value) Then
                sdbcCiaCod.Value = ""
                Exit Sub
            Else
                bRespetarCombo = True
                sdbcCiaDen.Text = Ador("DEN").Value
                sdbcCiaCod.Text = Ador("COD").Value
                sdbcCiaCod.Columns(0).Value = Ador("ID").Value
                CiaSeleccionada
                bRespetarCombo = False
                bCargarComboDesde = False
            End If
        End If
        
        Ador.Close
        Set Ador = Nothing
        
    End If
    
End Sub
Private Sub sdbcCiaCod_DropDown()
    Dim i As Long
    Dim Ador As Ador.Recordset
    
    sdbcCiaCod.RemoveAll
    sdbgProve.RemoveAll
    Set oCiaSeleccionada = Nothing
    
    'cmdRestaurar.Enabled = False


    If bCargarComboDesde Then
        Set Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False, True)
    Else
        Set Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, True)
    End If
    
    
    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaCod.AddItem Ador("ID").Value & Chr(9) & Ador("COD").Value & Chr(9) & Ador("DEN").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcCiaCod.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    sdbcCiaCod.SelStart = 0
    sdbcCiaCod.SelLength = Len(sdbcCiaCod.Text)
    sdbcCiaCod.Refresh
    
End Sub
Private Sub sdbcCiaCod_InitColumnProps()
    
    sdbcCiaCod.DataField = "Column 1"
    sdbcCiaCod.DataFieldToDisplay = "Column 1"
    
End Sub

Public Sub CiaSeleccionada()
Dim Ador As Ador.Recordset
Dim oEnlace As CEnlace

    sdbgProve.RemoveAll
    If sOrigen <> "frmCompanias" Then
        If sdbcCiaCod.Text = "" Then Exit Sub
        oCias.CargarTodasLasCiasDesde 1, sdbcCiaCod.Columns(0).Value, , , True
    
        Set oCiaSeleccionada = Nothing
        Set oCiaSeleccionada = oCias.Item(CStr(sdbcCiaCod.Columns(0).Value))
    End If
    
    Set oEnlaces = oCiaSeleccionada.DevolverTodosLosEnlaces(False, TipoOrdenacionEnlaces.OrdPorProvePCod, False)
    
    For Each oEnlace In oEnlaces
        sdbgProve.AddItem oEnlace.CodigoProveedorPortal & Chr(9) & oEnlace.DenProveedorPortal & Chr(9) & oEnlace.IDProveedorPortal & Chr(9) & oEnlace.CodigoProveedorGS & Chr(9) & oEnlace.PremiumActivo
    Next
    
    bRespetarCombo = False
End Sub


Private Sub cmdModoEdicion_Click()
    
    If bModoEdicion Then
        If sdbgProve.DataChanged Then
            'sdbgProve_BeforeColUpdate sdbgProve.Col, oEnlaces.Item(CStr(sdbgProve.Columns(2).Value)).CodigoProveedorGS, False
            sdbgProve.Update
        End If
        cmdModoEdicion.Caption = sIdiEdicion
        bModoEdicion = False
        sdbgProve.Columns("PROVECOD").Locked = True
        sdbgProve.Columns("ACTIV").Locked = True
        sdbgProve.Columns("MAT").Locked = True
        'cmdRestaurar.Visible = True
        cmdListado.Visible = True
        fraCias.Enabled = True
    Else
        sdbgProve.Columns("PROVECOD").Locked = False
        sdbgProve.Columns("ACTIV").Locked = False
        sdbgProve.Columns("MAT").Locked = False
        cmdModoEdicion.Caption = sIdiConsulta
        bModoEdicion = True
        'cmdRestaurar.Visible = False
        cmdListado.Visible = False
        fraCias.Enabled = False
    End If
    
End Sub

Private Sub Form_Load()
    
    bLoad = True
    aWidth = 0
    aHeight = 0
    CargarRecursos
    If sOrigen = "frmCompanias" Then
        Me.Height = 4920
        Me.Width = 9375
        fraCias.Visible = False
        sdbgProve.Top = 105
        picNavigate.Top = 3930
    Else
        Me.Height = 5700
        Me.Width = 9375
    End If
    bLoad = False
    Set oCias = g_oRaiz.Generar_CCias
        
End Sub

Private Sub Form_Resize()
If Me.WindowState = 2 Then
    If sOrigen = "frmCompanias" Then
        aHeight = 4920
        aWidth = 9375
    Else
        aHeight = 5700
        aWidth = 9375
    End If
    Arrange

End If

If Not bLoad Then Arrange

End Sub

Private Sub Form_Unload(Cancel As Integer)
 Set oCiaSeleccionada = Nothing
 Set oCias = Nothing
 Set oEnlaces = Nothing
 bModoEdicion = False
End Sub




Private Sub sdbgProve_AfterColUpdate(ByVal ColIndex As Integer)
sdbgProve.Update
End Sub

Private Sub sdbgProve_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Dim iRespuesta As Integer
    Dim teserror As CTESError
    Dim bYaEnlazado As Boolean
    bYaEnlazado = False


    'Para la columna 3,proveedor GS, PROVECOD
    If ColIndex <> 3 Then Exit Sub
    
    
    If sdbgProve.Columns("PROVECOD").Value = "" Then
    'Eliminar enlace
        Screen.MousePointer = vbHourglass
        iRespuesta = basMensajes.PreguntaEliminarEnlaceProve(sdbgProve.Columns("PREMCOD").Value, sdbgProve.Columns("PROVECOD").Value)
        
        If iRespuesta = vbYes Then
           frmEsperaPortal.Show
           DoEvents
           Set teserror = oEnlaces.Item(CStr(sdbgProve.Columns("PREMID").Value)).EliminarEnlace
           Unload frmEsperaPortal
           
           Screen.MousePointer = vbNormal
           
           If teserror.NumError <> TESnoerror Then
               basErrores.TratarError teserror
               sdbgProve.Columns("PROVECOD").Value = OldValue
               sdbgProve.Columns("PROVECOD").Text = OldValue
               Exit Sub
           Else
               
               sdbgProve.Columns(4).Value = 0
               Exit Sub
           End If
        Else
            
            sdbgProve.Columns("PROVECOD").Value = OldValue
            sdbgProve.Columns("PROVECOD").Text = OldValue
            Cancel = True
            sdbgProve.DataChanged = False
            bNoIrABusqueda = True
            Screen.MousePointer = vbNormal
        
            Exit Sub
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    
    oEnlaces.Item(CStr(sdbgProve.Columns("PREMID").Value)).CodigoProveedorGS = sdbgProve.Columns("PROVECOD").Text
    
    If oEnlaces.Item(CStr(sdbgProve.Columns("PREMID").Value)).ComprobarExistenciaProveedorGS Then
        
        iRespuesta = basMensajes.PreguntaModificarEnlaceProve(sdbgProve.Columns("PREMCOD").Value)
        
        If iRespuesta = vbNo Then
            sdbgProve.Columns("PROVECOD").Value = OldValue
            sdbgProve.Columns("PROVECOD").Text = OldValue
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    Else
        iRespuesta = basMensajes.PreguntaAnyadirEnlaceProve(sdbgProve.Columns("PREMCOD").Value)
        
        If iRespuesta = vbNo Then
            sdbgProve.Columns("PROVECOD").Value = OldValue
            sdbgProve.Columns("PROVECOD").Text = OldValue
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
    
    Dim oProvesVinculados As CProveedores
    Dim sProvesVinc As String
    Dim oProve As CProveedor
    Dim iRet As Integer
    
    If g_oRaiz.CodigosErpActivado(oEnlaces.Item(CStr(sdbgProve.Columns("PREMID").Value)).IDCiaCompradora) Then
        Set oProvesVinculados = g_oRaiz.ProveedoresVinculados(oEnlaces.Item(CStr(sdbgProve.Columns("PREMID").Value)).IDCiaCompradora, oEnlaces.Item(CStr(sdbgProve.Columns("PREMID").Value)).NIF, oEnlaces.Item(CStr(sdbgProve.Columns("PREMID").Value)).CodigoProveedorPortal)
        If oProvesVinculados.Count > 0 Then
            sProvesVinc = msIdiProve & ": " & vbCrLf
            For Each oProve In oProvesVinculados
                sProvesVinc = sProvesVinc & oProve.Cod & " - " & oProve.Den & vbCrLf
            Next
            iRet = basMensajes.ExistenProveedoresVinculados(sProvesVinc)
            If iRet = vbNo Then
                sdbgProve.Columns("PROVECOD").Value = OldValue
                sdbgProve.Columns("PROVECOD").Text = OldValue
                Screen.MousePointer = vbNormal
                Set oProvesVinculados = Nothing
                Exit Sub
            End If
        End If
    End If
    
    
    
    frmEsperaPortal.Show
    DoEvents
    
    Set teserror = oEnlaces.Item(CStr(sdbgProve.Columns("PREMID").Value)).AnyadirEnlace(OldValue)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Screen.MousePointer = vbNormal
        Unload frmEsperaPortal
        sdbgProve.Columns("PROVECOD").Value = OldValue
        Cancel = True
        sdbgProve.DataChanged = False
        Exit Sub
        
    End If
    bYaEnlazado = True
    'Despu�s de a�adir el enlace comprueba que tenga materiales asignados y en caso
    'contrario muestra la pantalla de materiales
    With oEnlaces.Item(CStr(sdbgProve.Columns("PREMID").Value))
    If IsNull(.ServidorGS) Or .ServidorGS = "" Or IsNull(.BaseDatosGS) Or .BaseDatosGS = "" Then
        Set teserror = New CTESError
        teserror.NumError = FSPSServer.TESImposibleConectarALaCia
        Unload frmEsperaPortal
    Else
        If .DevolverEstrMatDeProveedorEnCiaComp = False Then
            basMensajes.SeleccionarMaterial
            frmPROVEMatPorProve.bAsignar = True
            frmPROVEMatPorProve.bAnadirProveedorGS = Not bYaEnlazado
            frmPROVEMatPorProve.vOldValue = OldValue
            'frmPROVEMatPorProve.vOldValue = oEnlaces.Item(CStr(sdbgProve.Columns("PREMID").Value)).CodigoProveedorGS
            Set frmPROVEMatPorProve.oEnlaceSeleccionado = oEnlaces.Item(CStr(sdbgProve.Columns("COMPID").Value))
            frmPROVEMatPorProve.bModif = bModoEdicion
            frmPROVEMatPorProve.Hide
            frmPROVEMatPorProve.Show 1
            Set teserror = New CTESError
            teserror.NumError = 0
        End If
    End If
    
    End With
    
    Unload frmEsperaPortal
    
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        sdbgProve.Columns("PROVECOD").Value = OldValue
        sdbgProve.Columns("PROVECOD").Text = OldValue
        oEnlaces.Item(CStr(sdbgProve.Columns("PREMID").Value)).CodigoProveedorGS = OldValue
    Else
        
        sdbgProve.Columns(4).Value = 1

    End If
End Sub

Private Sub sdbgProve_BtnClick()
    'ZZ
    DoEvents
    
    If Not bModoEdicion Then Exit Sub
    
    If sdbgProve.Col <> 3 Then Exit Sub
    
    If sdbgProve.DataChanged = True Then
        sdbgProve.Update
        DoEvents
    End If
    
    If bNoIrABusqueda Then
        bNoIrABusqueda = False
        Exit Sub
    End If
    
    frmPROVEBuscar.sOrigen = "frmEnlazarComprador"
    frmPROVEBuscar.IdCia = oCiaSeleccionada.Id
    frmPROVEBuscar.Hide
    frmPROVEBuscar.Show 1
    
End Sub

Private Sub sdbgProve_Change()
Dim teserror As CTESError

    If sdbgProve.Col = 4 Then
    
        Screen.MousePointer = vbHourglass
    
        If sdbgProve.Columns("PROVECOD").Value = "" Then
            sdbgProve.CancelUpdate
            sdbgProve.DataChanged = False
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        If Abs(Val(sdbgProve.Columns(4).Value)) = 1 Then
            frmEsperaPortal.Show
            DoEvents
            Set teserror = oEnlaces.Item(CStr(sdbgProve.Columns("PREMID").Value)).ActivarProveedor
            Unload frmEsperaPortal
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                sdbgProve.DataChanged = False
                sdbgProve.CancelUpdate
            Else
                sdbgProve.Update
            End If
            
        Else
            frmEsperaPortal.Show
            DoEvents
            Set teserror = oEnlaces.Item(CStr(sdbgProve.Columns("PREMID").Value)).DesactivarProveedor
            Unload frmEsperaPortal
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                sdbgProve.DataChanged = False
                sdbgProve.CancelUpdate
            Else
                sdbgProve.Update
            End If
        End If
    
    
    
    End If
    
    Screen.MousePointer = vbNormal
End Sub



Private Sub sdbgProve_HeadClick(ByVal ColIndex As Integer)
Dim oEnlace As CEnlace

    If sdbcCiaCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If sdbgProve.Columns("ACTIV").Locked = True Then
        
       
        Select Case ColIndex
        
            Case 0
                    sdbgProve.RemoveAll
                    frmEsperaPortal.Show
                    DoEvents
                    Set oEnlaces = Nothing
                    Set oEnlaces = oCiaSeleccionada.DevolverTodosLosEnlaces(False, OrdPorProvePCod)
                
            Case 1
                    sdbgProve.RemoveAll
                    frmEsperaPortal.Show
                    DoEvents
                    Set oEnlaces = Nothing
                    Set oEnlaces = oCiaSeleccionada.DevolverTodosLosEnlaces(False, OrdPorProvePDen)
    
            Case 3
                    sdbgProve.RemoveAll
                    frmEsperaPortal.Show
                    DoEvents
                    Set oEnlaces = Nothing
                    Set oEnlaces = oCiaSeleccionada.DevolverTodosLosEnlaces(False, OrdPorProveGSCod)
    
            Case 4
                    sdbgProve.RemoveAll
                    frmEsperaPortal.Show
                    DoEvents
                    Set oEnlaces = Nothing
                    Set oEnlaces = oCiaSeleccionada.DevolverTodosLosEnlaces(False, OrdPorPremActivo)
                    
            Case Else
                    Screen.MousePointer = vbNormal
                    Exit Sub
        End Select
    End If
    
    Unload frmEsperaPortal
    
    sdbgProve.RemoveAll
    For Each oEnlace In oEnlaces
        With oEnlace
            
            sdbgProve.AddItem .CodigoProveedorPortal & Chr(9) & .DenProveedorPortal & Chr(9) & .IDProveedorPortal & Chr(9) & .CodigoProveedorGS & Chr(9) & .PremiumActivo
        
        End With
    Next
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgProve_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    
    If sdbgProve.DataChanged Then
        sdbgProve.Update
    End If
    
    If sdbgProve.Col = 5 Then
        If sdbgProve.Columns("PROVECOD").Value = "" Then Exit Sub
        Set frmPROVEMatPorProve.oEnlaceSeleccionado = oEnlaces.Item(CStr(sdbgProve.Columns("PREMID").Value))
        frmPROVEMatPorProve.bAnadirProveedorGS = False
        frmPROVEMatPorProve.bModif = bModoEdicion
        frmPROVEMatPorProve.Hide
        frmPROVEMatPorProve.Show 1

        sdbgProve.Col = 0
    End If
    
End Sub


Private Sub sdbcCiaDen_DropDown()
    Dim i As Long
    Dim Ador As Ador.Recordset
    
    sdbcCiaDen.RemoveAll
    sdbcCiaDen.ReBind
    
    
    If bCargarComboDesde Then
        Set Ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True, True)
    Else
        Set Ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , , , True, True)
    End If

    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaDen.AddItem Ador("ID").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("COD").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcCiaDen.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    sdbcCiaDen.SelStart = 0
    sdbcCiaDen.SelLength = Len(sdbcCiaDen.Text)
    sdbcCiaDen.Refresh
    
End Sub
Private Sub sdbcCiaDen_InitColumnProps()
    
    sdbcCiaDen.DataField = "Column 1"
    sdbcCiaDen.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcCiaDen_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaCod.Text = ""
        Set oCiaSeleccionada = Nothing
        sdbgProve.RemoveAll
        bRespetarCombo = False
        bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCiaDen_Click()
    
    If Not sdbcCiaDen.DroppedDown Then
        sdbcCiaDen = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaDen_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaDen.Rows - 1
            bm = sdbcCiaDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaDen.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaDen_CloseUp()

    If sdbcCiaDen.Columns(0).Value = "-1" Then
        sdbcCiaDen.Text = ""
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaDen.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaDen.Columns(1).Text
    sdbcCiaCod.Text = sdbcCiaDen.Columns(2).Text
    sdbcCiaCod.Columns("ID").Value = sdbcCiaDen.Columns("ID").Value
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
    CiaSeleccionada
    
End Sub
Private Sub Arrange()
Dim iH As Integer
Dim iV As Integer
On Error Resume Next
If aWidth = 0 Then
    aWidth = Me.Width
    aHeight = Me.Height
End If
If Me.Height < 1500 Or Me.Width < 200 Then
    Me.Height = aHeight
    Me.Width = aWidth
End If
iH = Me.Height - aHeight
iV = Me.Width - aWidth
sdbgProve.Height = sdbgProve.Height + iH
picNavigate.Top = picNavigate.Top + iH
sdbgProve.Width = sdbgProve.Width + iV
cmdModoEdicion.Left = cmdModoEdicion.Left + iV
sdbgProve.Groups(0).Width = 0.6 * sdbgProve.Width
sdbgProve.Groups(1).Width = 0.35 * sdbgProve.Width
picNavigate.Width = picNavigate.Width + iV

sdbgProve.Columns("PREMCOD").Width = 0.15 * sdbgProve.Width
sdbgProve.Columns("PREMDEN").Width = 0.45 * sdbgProve.Width
sdbgProve.Columns("PROVECOD").Width = 0.13 * sdbgProve.Width
sdbgProve.Columns("ACTIV").Width = 0.07 * sdbgProve.Width
sdbgProve.Columns("MAT").Width = 0.15 * sdbgProve.Width
aWidth = Me.Width
aHeight = Me.Height
End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_ENLAZARCOMPRADOR, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdListado.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdModoEdicion.Caption = Ador(0).Value
        sIdiEdicion = Ador(0).Value
        Ador.MoveNext
        Me.fraCias.Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcCiaCod.Columns(1).Caption = Ador(0).Value
        Me.sdbcCiaDen.Columns(2).Caption = Ador(0).Value
        Me.sdbgProve.Columns(0).Caption = Ador(0).Value
        Me.sdbgProve.Columns(3).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcCiaCod.Columns(2).Caption = Ador(0).Value
        Me.sdbcCiaDen.Columns(1).Caption = Ador(0).Value
        Me.sdbgProve.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgProve.Groups(0).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgProve.Groups(1).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgProve.Columns(4).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgProve.Columns(5).Caption = Ador(0).Value
        Ador.MoveNext
        sIdiConsulta = Ador(0).Value
        Ador.MoveNext
        msIdiProve = Ador(0).Value
        
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub


