VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmConectSel 
   Caption         =   "Control de conectividad"
   ClientHeight    =   1680
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5970
   Icon            =   "frmConectSel.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   1680
   ScaleWidth      =   5970
   Begin VB.PictureBox picUnaCompradora 
      BorderStyle     =   0  'None
      Height          =   1455
      Left            =   0
      ScaleHeight     =   1455
      ScaleWidth      =   5895
      TabIndex        =   6
      Top             =   120
      Width           =   5895
      Begin VB.CommandButton cmdEmpezar 
         Caption         =   "Iniciar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1800
         TabIndex        =   7
         Top             =   840
         Width           =   2175
      End
      Begin VB.Label Label1 
         Caption         =   "Pulse iniciar para realizar la comprobaci�n de conectividad"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   8
         Top             =   240
         Width           =   5415
      End
   End
   Begin VB.CommandButton cmdIniciar 
      Caption         =   "Iniciar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   4890
      TabIndex        =   3
      Top             =   1230
      Width           =   1005
   End
   Begin VB.Frame Frame1 
      Caption         =   "Comprobar conectividad"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1065
      Left            =   60
      TabIndex        =   0
      Top             =   90
      Width           =   5835
      Begin VB.OptionButton optCia 
         Caption         =   "Con esta compa��a:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   180
         TabIndex        =   2
         Top             =   630
         Width           =   1785
      End
      Begin VB.OptionButton optTod 
         Caption         =   "Con todas las compa��as con funci�n compradora activa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   180
         TabIndex        =   1
         Top             =   300
         Value           =   -1  'True
         Width           =   4965
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaCod 
         Height          =   285
         Left            =   1980
         TabIndex        =   4
         Top             =   630
         Width           =   885
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Denominaci�n"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FC"
         Columns(3).Name =   "FC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FP"
         Columns(4).Name =   "FP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   1561
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaDen 
         Height          =   285
         Left            =   2910
         TabIndex        =   5
         Top             =   630
         Width           =   2745
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "C�digo"
         Columns(2).Name =   "COD"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   4842
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmConectSel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private IdCiaSeleccionada As Long
Private bCargarComboDesde As Boolean
Private bRespetarCombo As Boolean
Private oCias As CCias


Private Sub cmdEmpezar_Click()
Dim Res() As TipoResultadoConexion
Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    oCias.CargarTodasLasCiasDesde 1, basParametros.g_udtParametrosGenerales.g_lIdCiaCompradora, , , True
    If Not oCias.Item(CStr(basParametros.g_udtParametrosGenerales.g_lIdCiaCompradora)) Is Nothing Then
        If oCias.Item(CStr(basParametros.g_udtParametrosGenerales.g_lIdCiaCompradora)).ComprobarConectividadConCia Then
            basMensajes.ConexionCorrecta
        Else
            basMensajes.ConexionIncorrecta
        End If
    Else
         basMensajes.CiaEliminada
    End If
            
    Screen.MousePointer = vbNormal
    
    
End Sub

Private Sub cmdIniciar_Click()
Dim Res() As TipoResultadoConexion
Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    If optTod Then
            
        Res = oCias.ComprobarConectividadConTodasLasCias
        
        If Not IsEmpty(Res) Then
            
            Unload frmConectRes
            i = 1
            
            
            While i <= UBound(Res)
            
                frmConectRes.sdbgCias.AddItem Res(i).Id & Chr(9) & Res(i).Cod & Chr(9) & Res(i).Den & Chr(9) & Res(i).Res & Chr(9) & Res(i).FSGS_SRV & Chr(9) & Res(i).FSGS_BD
                
                i = i + 1
            Wend
            
            
        End If
        
    Else
        
        If IdCiaSeleccionada <> -1 Then
            oCias.CargarTodasLasCiasDesde 1, IdCiaSeleccionada, , , True
            If Not oCias.Item(CStr(IdCiaSeleccionada)) Is Nothing Then
                If oCias.Item(CStr(IdCiaSeleccionada)).ComprobarConectividadConCia Then
                    basMensajes.ConexionCorrecta
                Else
                    basMensajes.ConexionIncorrecta
                End If
           
            Else
                basMensajes.CiaEliminada
            End If
            
        End If
        
    End If
    
    Screen.MousePointer = vbNormal
    
    
End Sub



Private Sub Form_Load()
        
    Width = 6090
    Height = 2085
    CargarRecursos
    If basParametros.g_udtParametrosGenerales.g_bUnaCompradora Then
        picUnaCompradora.Visible = True
    Else
        picUnaCompradora.Visible = False
    End If
    
    Set oCias = g_oRaiz.Generar_CCias
    
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set oCias = Nothing
End Sub

Private Sub optTod_Click()
    
    sdbcCiaCod.Value = ""
    
End Sub

Private Sub sdbcCiaCod_DropDown()

    Dim i As Integer
    Dim ador As ador.Recordset
    
    sdbcCiaCod.RemoveAll

    If bCargarComboDesde Then
        Set ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False, True)
    Else
        Set ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, True)
    End If
    
    If Not ador Is Nothing Then
        
        i = 1
        
        While Not ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaCod.AddItem ador("ID").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("FCEST").Value
            ador.MoveNext
        Wend
        
        If Not ador.EOF Then
            sdbcCiaCod.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        ador.Close
        Set ador = Nothing
    End If
    
    
    sdbcCiaCod.SelStart = 0
    sdbcCiaCod.SelLength = Len(sdbcCiaCod.Text)
    sdbcCiaCod.Refresh
    
End Sub
Private Sub sdbcCiaCod_InitColumnProps()
    
    sdbcCiaCod.DataField = "Column 1"
    sdbcCiaCod.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcCiaCod_Change()

    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaDen.Text = ""
        IdCiaSeleccionada = -1
        bRespetarCombo = False
        bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCiaCod_Click()
    
    If Not sdbcCiaCod.DroppedDown Then
        sdbcCiaCod = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaCod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaCod.Rows - 1
            bm = sdbcCiaCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaCod_CloseUp()

    If sdbcCiaCod.Value = "-1" Then
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaCod.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaCod.Columns(2).Text
    sdbcCiaCod.Text = sdbcCiaCod.Columns(1).Text
    
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
    IdCiaSeleccionada = sdbcCiaCod.Columns(0).Value
    
    
    
End Sub

Private Sub sdbcCiaCod_Validate(Cancel As Boolean)

    Dim ador As ador.Recordset

    If sdbcCiaCod.Text = "" Then
        Exit Sub
    End If
    
    Set ador = oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , True, , True)
    
    If ador.RecordCount = 0 Then
        
        sdbcCiaCod.Text = ""
        sdbcCiaDen.Text = ""
        Exit Sub
        
    Else
        bRespetarCombo = True
        sdbcCiaDen.Text = ador("DEN").Value
        bRespetarCombo = False
        bCargarComboDesde = False
        IdCiaSeleccionada = ador("ID").Value
    End If
    ador.Close
    Set ador = Nothing

        
End Sub

Private Sub sdbcCiaDen_DropDown()

    Dim i As Integer
    Dim ador As ador.Recordset
    
    sdbcCiaDen.RemoveAll

    If bCargarComboDesde Then
        Set ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True, True)
    Else
        Set ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , , False, True, True)
    End If
    
    If Not ador Is Nothing Then
        
        i = 1
        
        While Not ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaDen.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("FCEST").Value
            ador.MoveNext
        Wend
        
        If Not ador.EOF Then
            sdbcCiaDen.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        ador.Close
        Set ador = Nothing
    End If
    
    
    sdbcCiaDen.SelStart = 0
    sdbcCiaDen.SelLength = Len(sdbcCiaDen.Text)
    sdbcCiaDen.Refresh
    
End Sub
Private Sub sdbcCiaDen_InitColumnProps()
    
    sdbcCiaDen.DataField = "Column 1"
    sdbcCiaDen.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcCiaDen_Change()

    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaCod.Text = ""
        IdCiaSeleccionada = -1
        bRespetarCombo = False
        bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCiaDen_Click()
    
    If Not sdbcCiaDen.DroppedDown Then
        sdbcCiaDen = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaDen_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaDen.Rows - 1
            bm = sdbcCiaDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaDen.Columns(2).CellText(bm), 1, Len(Text))) Then
                sdbcCiaDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaDen_CloseUp()

    If sdbcCiaDen.Columns(0).Value = "-1" Then
        sdbcCiaDen.Text = ""
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaDen.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaDen.Columns(1).Text
    sdbcCiaCod.Text = sdbcCiaDen.Columns(2).Text
    bRespetarCombo = False
        
    bCargarComboDesde = False
    
    IdCiaSeleccionada = sdbcCiaDen.Columns(0).Value

    
End Sub




Private Sub sdbcCiaDen_Validate(Cancel As Boolean)
    
    Dim ador As ador.Recordset

    If sdbcCiaDen.Text = "" Then
        Exit Sub
    End If
    
    Set ador = oCias.DevolverCiasDesde(1, , sdbcCiaDen.Text, True, , True)
    
    If ador.RecordCount = 0 Then
        
        sdbcCiaCod.Text = ""
        sdbcCiaDen.Text = ""
        Exit Sub
        
    Else
        bRespetarCombo = True
        sdbcCiaCod.Text = ador("COD").Value
        bRespetarCombo = False
        bCargarComboDesde = False
        IdCiaSeleccionada = ador("ID").Value
    End If
    ador.Close
    Set ador = Nothing

End Sub

Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CONECTSEL, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdEmpezar.Caption = ador(0).Value
        Me.cmdIniciar.Caption = ador(0).Value
        ador.MoveNext
        Me.Frame1.Caption = ador(0).Value
        ador.MoveNext
        Me.Label1.Caption = ador(0).Value
        ador.MoveNext
        Me.optCia.Caption = ador(0).Value
        ador.MoveNext
        Me.optTod.Caption = ador(0).Value
        ador.MoveNext
        Me.sdbcCiaCod.Columns(1).Caption = ador(0).Value
        Me.sdbcCiaDen.Columns(2).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbcCiaCod.Columns(2).Caption = ador(0).Value
        Me.sdbcCiaDen.Columns(1).Caption = ador(0).Value
        ador.Close
    End If
    
    Set ador = Nothing
           
End Sub


