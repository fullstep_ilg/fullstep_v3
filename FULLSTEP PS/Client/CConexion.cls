VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CConexion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Private mvarAdoCon As ADODB.Connection
Private mvarAdoErs As ADODB.Errors


Friend Property Set AdoCon(ByVal vData As ADODB.Connection)
    Set mvarAdoCon = vData
End Property


Friend Property Get AdoCon() As ADODB.Connection
    Set AdoCon = mvarAdoCon
End Property

Public Property Set adoErs(ByVal vData As ADODB.Errors)
    Set mvarAdoErs = vData
End Property


Public Property Get adoErs() As ADODB.Errors
    Set adoErs = mvarAdoErs
End Property


Private Sub Class_Terminate()
On Error Resume Next

mvarAdoCon.Close
Set mvarAdoCon = Nothing


End Sub


