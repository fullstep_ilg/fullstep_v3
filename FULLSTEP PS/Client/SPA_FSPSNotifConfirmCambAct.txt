Le notificamos que ha sido confirmada la solicitud de cambio de actividades.


Datos de la cia:
  C�digo: @CODCIA
  Denominaci�n: @DENCIA
  NIF de la cia: @NIF
  C.P.: @CP
  Poblaci�n: @POB

  Provincia: 
    C�digo: @CODPROVI
    Denominaci�n: @DENPROVI

  Pa�s:
    C�digo: @CODPAI
    Denominaci�n: @DENPAI

  Direcci�n web de la cia: @URL

  Funci�n compradora autorizada: @FC
  Funci�n proveedora autorizada: @FP
  Funci�n Premium autorizada: @PREMIUM

  Actividades en las que estaba registrada la cia antes de la confirmaci�n:

@ACTIVIDADES_ANTES

  Actividades que han sido eliminadas tras la confirmaci�n de la solicitud:

@ACTIVIDADES_A_ELIMINAR

  Actividades que han sido suscritas tras la confirmaci�n de la solicitud:

@ACTIVIDADES_A_SUSCRIBIR

  Actividades en las que est� registrada la cia tras la confirmaci�n:

@ACTIVIDADES_ACTUALES

Datos del usuario que realiz� la solicitud de cambio de actividades:
  C�digo: @CODUSU
  Nombre: @NOMUSU
  Apellidos: @APEUSU
  Tel�fono 1: @TFNO1USU
  Tel�fono 2: @TFNO2USU
  Tel�fono m�vil: @MOVILUSU
  Fax: @FAXUSU
  E-mail: @EMAILUSU
  Cargo: @CARGOUSU
  Departamento: @DEPUSU


Un saludo


(Si desea recibir este email en HTML acceda al portal y modifique sus preferencias en el apartado de registro de usuario)
