VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRConector"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Public Function Conectar(ByVal sInstancia As String, Optional ByVal sServidor As String, Optional ByVal sBaseDatos As String) As Boolean
'************************************************************************
'*** Descripción : Abre una conexion al servidor de listados con el login y password de FSPSBD.LIC
'                  A diferencia del FSGS se encriptan y desencriptan los datos utilizando dos claves PAR e IMPAR
'*** Parámetros  : Instancia del registro que contiene el servidor y base de datos
'
'*** Valor que devuelve : True (OK) False (KO)
'************************************************************************
Dim fso As New FileSystemObject, fil As File, ts As TextStream
Dim sLicfec As String, sLicLoginEnc As String, sLicContraEnc As String

On Error GoTo error:

    Set crs_crapp = New CRAXDRT.Application
    
    If sServidor = "" Or sBaseDatos = "" Then
        crs_Server = GetStringSetting("FULLSTEP PS", sInstancia, "Conexion", "Servidor")
        crs_Database = GetStringSetting("FULLSTEP PS", sInstancia, "Conexion", "BaseDeDatos")
    Else
        crs_Server = sServidor
        crs_Database = sBaseDatos
    
    End If
    
    ''' Recogemos los valores de autorización del servidor de base de datos

    Set fil = fso.GetFile(App.Path & "\FSPSDB.LIC")
    Set ts = fil.OpenAsTextStream(ForReading)

    sLicfec = Format(ts.ReadLine, "DD\/MM\/YYYY HH\:NN\:SS")
    sLicLoginEnc = ts.ReadLine
    sLicContraEnc = ts.ReadLine
    ts.Close
    
    ''' Desencriptamos el usuario y la contraseña del servidor de objetos
    crs_User = basUtilidades.DesEncriptar(sLicLoginEnc, sLicfec)
    crs_Password = basUtilidades.DesEncriptar(sLicContraEnc, sLicfec)

    crs_crapp.LogOnServer "p2ssql.dll", crs_Server, crs_Database, crs_User, crs_Password
        
    Conectar = True
    crs_Connected = True
    Set fso = Nothing
    Set fil = Nothing

    
    Exit Function

error:

    Set fso = Nothing
    Set fil = Nothing
    Conectar = False
    
End Function
