VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstACT 
   Caption         =   "Listado de actividades"
   ClientHeight    =   2475
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7170
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstACT.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   2475
   ScaleWidth      =   7170
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   7170
      TabIndex        =   0
      Top             =   2100
      Width           =   7170
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         Height          =   375
         Left            =   5805
         TabIndex        =   10
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   2055
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   7155
      _ExtentX        =   12621
      _ExtentY        =   3625
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstACT.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Orden"
      TabPicture(1)   =   "frmLstACT.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraTipoPer"
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1185
         Left            =   120
         TabIndex        =   9
         Top             =   600
         Width           =   6930
         Begin VB.CommandButton cmdSelAct 
            Height          =   315
            Left            =   6400
            Picture         =   "frmLstACT.frx":0CEA
            Style           =   1  'Graphical
            TabIndex        =   4
            TabStop         =   0   'False
            Top             =   625
            Width           =   345
         End
         Begin VB.CommandButton cmdBorrar 
            Height          =   315
            Left            =   6050
            Picture         =   "frmLstACT.frx":106C
            Style           =   1  'Graphical
            TabIndex        =   3
            Top             =   625
            Width           =   315
         End
         Begin VB.OptionButton optRama 
            Caption         =   "Listar Rama:"
            Height          =   195
            Left            =   180
            TabIndex        =   2
            Top             =   690
            Width           =   1670
         End
         Begin VB.OptionButton optTodos 
            Caption         =   "Listado completo"
            Height          =   195
            Left            =   180
            TabIndex        =   1
            Top             =   300
            Value           =   -1  'True
            Width           =   4590
         End
         Begin VB.TextBox txtEstAct 
            BackColor       =   &H80000018&
            Height          =   285
            Left            =   1845
            Locked          =   -1  'True
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   645
            Width           =   4185
         End
      End
      Begin VB.Frame fraTipoPer 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1185
         Left            =   -74850
         TabIndex        =   8
         Top             =   540
         Width           =   6810
         Begin VB.OptionButton optOrdCod 
            Caption         =   "C�digo"
            Height          =   195
            Left            =   240
            TabIndex        =   5
            Top             =   360
            Value           =   -1  'True
            Width           =   4215
         End
         Begin VB.OptionButton optOrdDen 
            Caption         =   "Denominaci�n"
            Height          =   195
            Left            =   240
            TabIndex        =   6
            Top             =   735
            Width           =   4740
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   1000
         Left            =   4680
         Top             =   -180
      End
   End
End
Attribute VB_Name = "frmLstACT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_sACT1Cod As String
Private m_sACT2Cod As String
Private m_sACT3Cod As String
Private m_sACT4Cod As String
Private m_sACT5Cod As String
Private m_lACT1Id As Long
Private m_lACT2Id As Long
Private m_lACT3Id As Long
Private m_lACT4Id As Long
Private m_lACT5Id As Long
Public g_adores_Ador As Ador.Recordset
Public g_sOrigen As String
Public g_sIdioma As String
Public g_oACN1Seleccionado As CActividadNivel1
Public g_oACN2Seleccionado As CActividadNivel2
Public g_oACN3Seleccionado As CActividadNivel3
Public g_oACN4Seleccionado As CActividadNivel4
Public g_oACN5Seleccionado As CActividadNivel5


Private sIdiSeleccion As String
Private sIdiTodasAct As String
Private sIdiIdioma  As String
Private sIdiTitulo As String
Private sIdiTxtPag As String
Private sIdiTxtDe As String
Private sIdiTxtNivel1 As String
Private sIdiTxtNivel2 As String
Private sIdiTxtNivel3 As String
Private sIdiTxtNivel4 As String
Private sIdiTxtNivel5 As String



Public Sub PonerActSeleccionada()

    m_lACT1Id = 0
    m_lACT2Id = 0
    m_lACT3Id = 0
    m_lACT4Id = 0
    m_lACT5Id = 0

    If Not g_oACN5Seleccionado Is Nothing Then
        m_lACT1Id = g_oACN5Seleccionado.ACN1
        m_lACT2Id = g_oACN5Seleccionado.ACN2
        m_lACT3Id = g_oACN5Seleccionado.ACN3
        m_lACT4Id = g_oACN5Seleccionado.ACN4
        m_lACT5Id = g_oACN5Seleccionado.Id
        txtEstAct = g_oACN1Seleccionado.Cod & " - " & g_oACN2Seleccionado.Cod & " - " & g_oACN3Seleccionado.Cod & " - " & g_oACN4Seleccionado.Cod & " - " & g_oACN5Seleccionado.Cod & " - " & g_oACN5Seleccionado.Den
        Exit Sub
    End If

    If Not g_oACN4Seleccionado Is Nothing Then
        m_lACT1Id = g_oACN4Seleccionado.ACN1
        m_lACT2Id = g_oACN4Seleccionado.ACN2
        m_lACT3Id = g_oACN4Seleccionado.ACN3
        m_lACT4Id = g_oACN4Seleccionado.Id
        txtEstAct = g_oACN1Seleccionado.Cod & " - " & g_oACN2Seleccionado.Cod & " - " & g_oACN3Seleccionado.Cod & " - " & g_oACN4Seleccionado.Cod & " - " & g_oACN4Seleccionado.Den
        Exit Sub
    End If

    If Not g_oACN3Seleccionado Is Nothing Then
        m_lACT1Id = g_oACN3Seleccionado.ACN1
        m_lACT2Id = g_oACN3Seleccionado.ACN2
        m_lACT3Id = g_oACN3Seleccionado.Id
        txtEstAct = g_oACN1Seleccionado.Cod & " - " & g_oACN2Seleccionado.Cod & " - " & g_oACN3Seleccionado.Cod & " - " & g_oACN3Seleccionado.Den
        Exit Sub
    End If

    If Not g_oACN2Seleccionado Is Nothing Then
        m_lACT1Id = g_oACN2Seleccionado.ACN1
        m_lACT2Id = g_oACN2Seleccionado.Id
        txtEstAct = g_oACN1Seleccionado.Cod & " - " & g_oACN2Seleccionado.Cod & " - " & g_oACN2Seleccionado.Den
        Exit Sub
    End If

    If Not g_oACN1Seleccionado Is Nothing Then
        m_lACT1Id = g_oACN1Seleccionado.Id
        txtEstAct = g_oACN1Seleccionado.Cod & " - " & g_oACN1Seleccionado.Den
        Exit Sub
    End If


    
    
End Sub



Private Sub cmdBorrar_Click()
    txtEstAct.Text = ""
    m_lACT1Id = 0
    m_lACT2Id = 0
    m_lACT3Id = 0
    m_lACT4Id = 0
    m_lACT5Id = 0
End Sub

Private Sub cmdObtener_Click()

''' * Objetivo: Obtener un listado de Actividades en el idioma en el que actualmente se esta visualizando la estructura
ObtenerInforme
End Sub

Private Sub ObtenerInforme()
Dim oReport As CRAXDRT.Report
Dim pv As Preview
Dim oFos As FileSystemObject
Dim RepPath As String
Dim intOrden As Integer
Dim i As Integer

Dim oActividadesNivel1 As CActividadesNivel1
' ARRAY SelectionText, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula
Dim SelectionText(1 To 2, 1 To 2) As String
Dim GroupOrder(1 To 2, 1 To 5) As String
       
    If crs_Connected = False Then
        Exit Sub
    End If
    
    
    If basParametros.g_sRptPath = "" Then
        basMensajes.RutaDeRPTNoValida
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    RepPath = basParametros.g_sRptPath & "\rptACT.rpt"
        
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        basMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
   
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
        
        
    SelectionText(2, 1) = "SEL"
    If optTodos = True Then
        SelectionText(1, 1) = sIdiSeleccion & ": " & sIdiTodasAct
    Else
        SelectionText(1, 1) = sIdiSeleccion & ": " & txtEstAct
    End If
   
    Set g_adores_Ador = g_oRaiz.DevolverIdiomasDelPortalDesde(1, g_sIdioma, , True)
    
    
    SelectionText(2, 2) = "Idioma"
    If Not g_adores_Ador Is Nothing Then
        SelectionText(1, 2) = sIdiIdioma & ": " & g_adores_Ador("DEN").Value
        g_adores_Ador.Close
        Set g_adores_Ador = Nothing
    End If

    For i = 1 To 5
        GroupOrder(2, i) = "ORDEN_GRUPO" & i
        If optOrdDen Then
            GroupOrder(1, i) = "{ACT_TTX.den_act" & i & "}"
        Else
            GroupOrder(1, i) = "{ACT_TTX.cod_act" & i & "}"
        End If
    Next
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & sIdiTxtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & sIdiTxtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & sIdiTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "ACT1NIVEL")).Text = """" & sIdiTxtNivel1 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "ACT2NIVEL")).Text = """" & sIdiTxtNivel2 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "ACT3NIVEL")).Text = """" & sIdiTxtNivel3 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "ACT4NIVEL")).Text = """" & sIdiTxtNivel4 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "ACN5NIVEL")).Text = """" & sIdiTxtNivel5 & """"
    
    For i = 1 To UBound(SelectionText, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionText(2, i))).Text = """" & SelectionText(1, i) & """"
    Next i
    For i = 1 To UBound(GroupOrder, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, GroupOrder(2, i))).Text = GroupOrder(1, i)
    Next i
    Screen.MousePointer = vbHourglass
    
    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
    Set g_adores_Ador = Nothing
            
    Set g_adores_Ador = oActividadesNivel1.GenerarListadoEstructuraActividades(optOrdDen, g_sIdioma, m_lACT1Id, m_lACT2Id, m_lACT3Id, m_lACT4Id, m_lACT5Id)
    
    oReport.Database.SetDataSource g_adores_Ador

    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    

    Set pv = New Preview
    pv.g_sOrigen = "frmLstACT"
    pv.Hide
    pv.Caption = sIdiTitulo
    Set pv.g_oReport = oReport
    pv.CRViewer.ReportSource = oReport
    pv.CRViewer.ViewReport
    pv.Show
    Unload Me
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdSELACT_Click()
    optRama.Value = True
    frmSelActividad.sOrigen = "frmLstACT"
    frmSelActividad.Show 1
    PonerActSeleccionada
End Sub


Private Sub Form_Load()

    Me.Width = 7290
    Me.Height = 2880
    
    CargarRecursos
    
    m_lACT1Id = 0
    m_lACT2Id = 0
    m_lACT3Id = 0
    m_lACT4Id = 0
    m_lACT5Id = 0
    'If Not g_sOrigen = "frmActiv" Then
        Set g_oACN1Seleccionado = Nothing
        Set g_oACN1Seleccionado = g_oRaiz.Generar_CActividadNivel1
        Set g_oACN2Seleccionado = Nothing
        Set g_oACN2Seleccionado = g_oRaiz.Generar_CActividadNivel2
        Set g_oACN3Seleccionado = Nothing
        Set g_oACN3Seleccionado = g_oRaiz.Generar_CActividadNivel3
        Set g_oACN4Seleccionado = Nothing
        Set g_oACN4Seleccionado = g_oRaiz.Generar_CActividadNivel4
        Set g_oACN5Seleccionado = Nothing
        Set g_oACN5Seleccionado = g_oRaiz.Generar_CActividadNivel5
    'End If
    
End Sub

Private Sub optTodos_Click()
    txtEstAct = ""
    optRama = False
    m_lACT1Id = 0
    m_lACT2Id = 0
    m_lACT3Id = 0
    m_lACT4Id = 0
    m_lACT5Id = 0
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTACT, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        sIdiTitulo = Ador(0).Value
        Ador.MoveNext
        Me.cmdObtener.Caption = Ador(0).Value
        Ador.MoveNext
        Me.optOrdCod.Caption = Ador(0).Value
        Ador.MoveNext
        Me.optOrdDen.Caption = Ador(0).Value
        Ador.MoveNext
        Me.optRama.Caption = Ador(0).Value
        Ador.MoveNext
        Me.optTodos.Caption = Ador(0).Value
        Ador.MoveNext
        Me.SSTab1.TabCaption(0) = Ador(0).Value
        sIdiSeleccion = Ador(0).Value
        Ador.MoveNext
        Me.SSTab1.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        sIdiTodasAct = Ador(0).Value
        Ador.MoveNext
        sIdiIdioma = Ador(0).Value
        Ador.MoveNext
        sIdiTxtPag = Ador(0).Value
        Ador.MoveNext
        sIdiTxtDe = Ador(0).Value
        Ador.MoveNext
        sIdiTxtNivel1 = Ador(0).Value
        Ador.MoveNext
        sIdiTxtNivel2 = Ador(0).Value
        Ador.MoveNext
        sIdiTxtNivel3 = Ador(0).Value
        Ador.MoveNext
        sIdiTxtNivel4 = Ador(0).Value
        Ador.MoveNext
        sIdiTxtNivel5 = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub



