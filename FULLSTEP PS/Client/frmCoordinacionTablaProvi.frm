VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCoordinacionTablaProvi 
   Caption         =   "Coordinaci�n de tablas de provincias"
   ClientHeight    =   3885
   ClientLeft      =   345
   ClientTop       =   1680
   ClientWidth     =   9645
   Icon            =   "frmCoordinacionTablaProvi.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   3885
   ScaleWidth      =   9645
   Begin VB.CommandButton Cmdlistado 
      Caption         =   "&Listado"
      Height          =   345
      Left            =   0
      TabIndex        =   10
      Top             =   3540
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddProviPortDen 
      Height          =   1545
      Left            =   2280
      TabIndex        =   8
      Top             =   1800
      Width           =   4965
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5662
      Columns(0).Caption=   "Den.en el portal"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2699
      Columns(1).Caption=   "C�digo en el portal"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   8758
      _ExtentY        =   2725
      _StockProps     =   77
      ForeColor       =   0
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddProviPortCod 
      Height          =   1545
      Left            =   1080
      TabIndex        =   3
      Top             =   1380
      Width           =   4965
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2699
      Columns(0).Caption=   "C�digo en el portal"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5662
      Columns(1).Caption=   "Den.en el portal"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   8758
      _ExtentY        =   2725
      _StockProps     =   77
      ForeColor       =   0
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.CommandButton cmdModoEdicion 
      Caption         =   "&Edici�n"
      Height          =   345
      Left            =   8565
      TabIndex        =   7
      Top             =   3540
      Width           =   1005
   End
   Begin VB.Frame Frame1 
      Height          =   555
      Left            =   30
      TabIndex        =   0
      Top             =   -30
      Width           =   9615
      Begin SSDataWidgets_B.SSDBCombo sdbcPaiCod 
         Height          =   285
         Left            =   420
         TabIndex        =   4
         Top             =   180
         Width           =   885
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Denominaci�n"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FSP_COD"
         Columns(3).Name =   "FSP_COD"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FSP_DEN"
         Columns(4).Name =   "FSP_DEN"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   1561
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcPaiDen 
         Height          =   285
         Left            =   1350
         TabIndex        =   5
         Top             =   180
         Width           =   2745
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "C�digo"
         Columns(2).Name =   "COD"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FSP_DEN"
         Columns(3).Name =   "FSP_DEN"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FSP_COD"
         Columns(4).Name =   "FSP_COD"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   4842
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblPaiPort 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   6060
         TabIndex        =   9
         Top             =   180
         Width           =   3375
      End
      Begin VB.Label lblPortal 
         Caption         =   "Equivalente en el portal:"
         Height          =   195
         Left            =   4140
         TabIndex        =   6
         Top             =   240
         Width           =   1755
      End
      Begin VB.Label lblName 
         Caption         =   "Pa�s:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   1
         Top             =   210
         Width           =   375
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTabla 
      Height          =   2865
      Left            =   0
      TabIndex        =   2
      Top             =   600
      Width           =   9555
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   4
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCoordinacionTablaProvi.frx":0CB2
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   2990
      Columns(0).Caption=   "C�digo de FullStepGS"
      Columns(0).Name =   "COD_FSGS"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   3200
      Columns(1).Caption=   "Den. de FullStepGS"
      Columns(1).Name =   "DEN_FSGS"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   2725
      Columns(2).Caption=   "C�digo del portal"
      Columns(2).Name =   "COD_FSP"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "Den. del portal"
      Columns(3).Name =   "DEN_FSP"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   16854
      _ExtentY        =   5054
      _StockProps     =   79
      ForeColor       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmCoordinacionTablaProvi"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public oCia As CCia
Private CodPaiSeleccionado As String
Private IdPaiPortalSeleccionado As Integer
Private CodPaiPortalSeleccionado As String
Private bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean
Private bHayErrores As Boolean
Private bRespetarDropdown As Boolean
Private indiceCias As Integer
Public sOrdenListado As String
'Private oPaises As CPaises


Private sIdiEdicion As String
Private sIdiConsulta As String

Private Sub cmdListado_Click()
'Load frmLstCoorProv
'frmLstCoorProv.Visible = True
 ''' * Objetivo: Obtener un listado de provincias de un pa�s o paises
    
    frmLstCoorProv.WindowState = vbNormal
    Set frmLstCoorProv.oCia = oCia
    If Not oCia Is Nothing Then
       frmLstCoorProv.sdbcCiaCod.Text = oCia.Cod
       frmLstCoorProv.sdbcCiaCod_Validate False
       
    End If

 '   If sOrdenListado = "DEN" Then frmLstCoorProv.opOrdDenF = True
    If Not sdbcPaiCod.Value = "" Then
        frmLstCoorProv.opPais = True
        frmLstCoorProv.sdbcPaiCod.Text = sdbcPaiCod.Text
        frmLstCoorProv.sdbcPaiCod_Validate False
        
    Else
        frmLstCoorProv.opTodos = True
    End If
    frmLstCoorProv.SetFocus
End Sub
Private Sub sdbcPaiCod_Validate(Cancel As Boolean)
'Dim scod As String
'Dim Ador As Ador.Recordset
'
'    ''' * Objetivo: Validar la seleccion (nulo o existente)
'
'   ''' Si es nulo, correcto
'
'    If sdbcPaiCod.Text = "" Then
'        'RtnPassed = True
'        sdbcPaiCod_Validate False
'    Else
'
'        ''' Comprobar la existencia en la lista
'        If sdbcPaiCod.Columns("COD").Value = sdbcPaiCod.Text Then
'            sdbgTabla.Columns("DEN_FSP").Value = sdbcPaiCod.Columns("DEN").Value
'            'sdbcPaiCod_Validate False
'            Exit Sub
'        Else
'            Set Ador = g_oRaiz.DevolverPaisesDelPortalDesde(basParametros.g_iCargaMaximaCombos, sdbgTabla.Columns("COD_FSP").Value, , True)
'
'            If Ador Is Nothing Then
'                'RtnPassed = 0
'                sdbcPaiCod_Validate False
'            Else
'                If Ador.EOF Then
'                sdbcPaiCod_Validate False
'                '    RtnPassed = 0
'                Else
'
'
'                    sdbgTabla.Columns("COD_FSP").Value = Ador("COD").Value
'                    sdbgTabla.Columns("DEN_FSP").Value = Ador("DEN").Value
'                    sdbcPaiCod_Validate True
'                    'RtnPassed = 1
'                End If
''            End If
'
            
'        End If
        
'    End If
Dim ador As ador.Recordset

    If Trim(sdbcPaiCod.Text) = "" Then Exit Sub
    
    Set ador = oCia.DevolverPaisesDeLaCiaDesde(1, sdbcPaiCod.Text, , True)
    
    If ador Is Nothing Then
        
        sdbcPaiCod = ""
        Exit Sub
        
    Else
        If ador.EOF Then
            sdbcPaiCod.Value = ""
            Exit Sub
        Else
            If UCase(sdbcPaiCod.Text) <> UCase(ador("COD").Value) Then
                sdbcPaiCod.Value = ""
                Exit Sub
            Else
                bRespetarCombo = True
                sdbcPaiDen.Text = ador("DEN").Value
                sdbcPaiDen.Columns("FSP_COD").Value = ador("FSP_COD").Value
                sdbcPaiDen.Columns("FSP_DEN").Value = ador("FSP_DEN").Value
                
                If IsNull(ador("ID").Value) Then
                    IdPaiPortalSeleccionado = -1
                    lblPaiPort.Caption = ""
                    CodPaiPortalSeleccionado = ""
                Else
                    IdPaiPortalSeleccionado = ador("ID").Value
                    CodPaiSeleccionado = ador("COD").Value
                    lblPaiPort.Caption = sdbcPaiDen.Columns("FSP_COD").Value & " (" & sdbcPaiDen.Columns("FSP_DEN").Value & ")"
                    CodPaiPortalSeleccionado = sdbcPaiCod.Columns("FSP_COD").Value
                End If
                
                bRespetarCombo = False
                bCargarComboDesde = False
                
                CargarProvincias
            End If
        End If
        
        ador.Close
        Set ador = Nothing
        
    End If
        
End Sub
Private Sub cmdModoEdicion_Click()
    
    If sdbgTabla.AllowUpdate Then
        'Pasar a modo consulta
        sdbgTabla.Update
        DoEvents
        If Not bHayErrores Then
            
            sdbgTabla.AllowUpdate = False
            sdbcPaiCod.Enabled = True
            sdbcPaiDen.Enabled = True
            cmdModoEdicion.Caption = sIdiEdicion
        End If
        
    Else
        'Pasar a modo edici�n
        sdbgTabla.AllowUpdate = True
        sdbcPaiCod.Enabled = False
        sdbcPaiDen.Enabled = False
        cmdModoEdicion.Caption = sIdiConsulta
    End If
    
End Sub

Private Sub Form_Load()
    
    Width = 9765
    Height = 3930
    CargarRecursos
    
    sdbgTabla.Columns("COD_FSP").DropDownHwnd = sdbddProviPortCod.HWND
    sdbddProviPortCod.AddItem ""
    sdbgTabla.Columns("DEN_FSP").DropDownHwnd = sdbddProviPortDen.HWND
    sdbddProviPortDen.AddItem ""
    sdbgTabla.AllowUpdate = False
    cmdModoEdicion.Visible = True
    
    Cmdlistado.Visible = True
    
    
End Sub


Private Sub Form_Resize()
    
    If Me.Height < 1500 Then Exit Sub
    If Me.Width < 1205 Then Exit Sub
    sdbgTabla.Width = Me.Width - 200
    sdbgTabla.Height = Me.Height - 1500
    
    sdbgTabla.Columns("COD_FSGS").Width = sdbgTabla.Width * 0.2
    sdbgTabla.Columns("DEN_FSGS").Width = sdbgTabla.Width * 0.3
    sdbgTabla.Columns("COD_FSP").Width = sdbgTabla.Width * 0.2
    sdbgTabla.Columns("DEN_FSP").Width = sdbgTabla.Width * 0.3 - 570
    cmdModoEdicion.Left = sdbgTabla.Width - 1005
    cmdModoEdicion.Top = Me.Height - 850
    
    Cmdlistado.Left = sdbgTabla.Left
    Cmdlistado.Top = Me.Height - 850
    
   
    
    
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oCia = Nothing
End Sub

Private Sub sdbcPaiCod_DropDown()

    Dim i As Integer
    Dim ador As ador.Recordset
    
    sdbcPaiCod.RemoveAll
    
    If bCargarComboDesde Then
        Set ador = oCia.DevolverPaisesDeLaCiaDesde(g_iCargaMaximaCombos, Trim(sdbcPaiCod.Text), , False, False)
    Else
        Set ador = oCia.DevolverPaisesDeLaCiaDesde(g_iCargaMaximaCombos, , , False, False)
    End If
    
    If Not ador Is Nothing Then
        
        i = 1
        
        While Not ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcPaiCod.AddItem ador("ID").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("FSP_COD").Value & Chr(9) & ador("FSP_DEN").Value
            ador.MoveNext
        Wend
        
        If Not ador.EOF Then
            sdbcPaiCod.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        ador.Close
        Set ador = Nothing
    End If
    
    
    sdbcPaiCod.SelStart = 0
    sdbcPaiCod.SelLength = Len(sdbcPaiCod.Text)
    sdbcPaiCod.Refresh
    
End Sub

Private Sub sdbcPaiCod_InitColumnProps()
    
    sdbcPaiCod.DataField = "Column 1"
    sdbcPaiCod.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcPaiCod_Change()

    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcPaiDen.Text = ""
        CodPaiSeleccionado = -1
        IdPaiPortalSeleccionado = -1
        bRespetarCombo = False
        bCargarComboDesde = True
        sdbgTabla.RemoveAll
    End If
    
End Sub

Private Sub sdbcPaiCod_Click()
    
    If Not sdbcPaiCod.DroppedDown Then
        sdbcPaiCod = ""
        sdbcPaiDen = ""
    End If
End Sub

Private Sub sdbcPaiCod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPaiCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPaiCod.Rows - 1
            bm = sdbcPaiCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPaiCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcPaiCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcPaiCod_CloseUp()

    If sdbcPaiCod.Text = "-1" Then
        sdbcPaiCod.Text = ""
        Exit Sub
    End If
    
    If sdbcPaiCod.Text = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcPaiDen.Text = sdbcPaiCod.Columns(2).Text
    sdbcPaiCod.Text = sdbcPaiCod.Columns(1).Text
    
    bRespetarCombo = False
    
    bCargarComboDesde = False
    If sdbcPaiCod.Columns("ID").Value = "" Then
        IdPaiPortalSeleccionado = -1
        lblPaiPort.Caption = ""
    Else
        IdPaiPortalSeleccionado = sdbcPaiCod.Columns("ID").Value
         lblPaiPort.Caption = sdbcPaiCod.Columns("FSP_COD").Value & " (" & sdbcPaiCod.Columns("FSP_DEN").Value & ")"
         CodPaiPortalSeleccionado = sdbcPaiCod.Columns("FSP_COD").Value
    End If
    
    CodPaiSeleccionado = sdbcPaiCod.Columns("COD").Value
    bRespetarCombo = True
    
    bRespetarCombo = False
    
    
    CargarProvincias
    
    
End Sub

Private Sub CargarProvincias()
Dim ador As ador.Recordset
    
    sdbgTabla.RemoveAll
    
    Set ador = oCia.DevolverProvinciasEnCia(IdPaiPortalSeleccionado, CodPaiSeleccionado)
    If Not ador Is Nothing Then
            
        bRespetarDropdown = True
    
        While Not ador.EOF
        
            sdbgTabla.AddItem ador("COD").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("FSP_COD").Value & Chr(9) & ador("FSP_DEN").Value
            ador.MoveNext
            
        Wend
        
        bRespetarDropdown = False
    
    End If
    
    ador.Close
    Set ador = Nothing
    

End Sub


Private Sub sdbcPaiDen_DropDown()

    Dim i As Integer
    Dim ador As ador.Recordset
    
    sdbcPaiDen.RemoveAll

    If bCargarComboDesde Then
        Set ador = oCia.DevolverPaisesDeLaCiaDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcPaiDen.Text), , True)
    Else
        Set ador = oCia.DevolverPaisesDeLaCiaDesde(basParametros.g_iCargaMaximaCombos, , , False, True)
    End If
    
    If Not ador Is Nothing Then
        
        i = 1
        
        While Not ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcPaiDen.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("FSP_COD").Value
            ador.MoveNext
        Wend
        
        If Not ador.EOF Then
            sdbcPaiDen.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        ador.Close
        Set ador = Nothing
    End If
    
    
    sdbcPaiDen.SelStart = 0
    sdbcPaiDen.SelLength = Len(sdbcPaiDen.Text)
    sdbcPaiDen.Refresh
    
End Sub
Private Sub sdbcPaiDen_InitColumnProps()
    
    sdbcPaiDen.DataField = "Column 1"
    sdbcPaiDen.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcPaiDen_Change()

    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcPaiCod.Text = ""
        CodPaiSeleccionado = -1
        IdPaiPortalSeleccionado = -1
        bRespetarCombo = False
        bCargarComboDesde = True
        sdbgTabla.RemoveAll
    End If
    
End Sub

Private Sub sdbcPaiDen_Click()
    
    If Not sdbcPaiDen.DroppedDown Then
        sdbcPaiCod = ""
        sdbcPaiDen = ""
    End If
End Sub

Private Sub sdbcPaiDen_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPaiDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPaiDen.Rows - 1
            bm = sdbcPaiDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPaiDen.Columns("DEN").CellText(bm), 1, Len(Text))) Then
                sdbcPaiDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcPaiDen_CloseUp()

    If sdbcPaiDen.Text = "-1" Then
        sdbcPaiDen.Text = ""
        Exit Sub
    End If
    
    If sdbcPaiDen.Text = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcPaiDen.Text = sdbcPaiDen.Columns("DEN").Text
    sdbcPaiCod.Text = sdbcPaiDen.Columns("COD").Text
    
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
    If sdbcPaiDen.Columns("ID").Value = "" Then
        IdPaiPortalSeleccionado = -1
        lblPaiPort.Caption = ""
        CodPaiPortalSeleccionado = ""
    Else
        IdPaiPortalSeleccionado = sdbcPaiDen.Columns("ID").Value
        lblPaiPort.Caption = sdbcPaiDen.Columns("FSP_COD").Value & " (" & sdbcPaiDen.Columns("FSP_DEN").Value & ")"
        CodPaiPortalSeleccionado = sdbcPaiDen.Columns("FSP_COD").Value
    End If
    
    CodPaiSeleccionado = sdbcPaiDen.Columns("COD").Value
    bRespetarCombo = True
    
    
    bRespetarCombo = False
    
    
    CargarProvincias
    
    
    
End Sub

Private Sub sdbddProviPortCod_CloseUp()

    sdbgTabla.Columns("DEN_FSP").Value = sdbddProviPortCod.Columns("DEN").Value
    
End Sub

Private Sub sdbddProviPortCod_DropDown()
Dim ador As ador.Recordset
    
    If sdbgTabla.AllowUpdate = False Then
       sdbddProviPortCod.DroppedDown = False
    End If
    
    
    sdbddProviPortCod.RemoveAll
    
    Set ador = g_oRaiz.DevolverTodasLasProvinciasDesde(IdPaiPortalSeleccionado, sdbgTabla.Columns("COD_FSP").Value, , False)
    
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbddProviPortCod.AddItem ador("COD").Value & Chr(9) & ador("DEN").Value
            ador.MoveNext
        
        Wend
        
    End If
    
        
        
    
End Sub

Private Sub sdbddProviPortCod_InitColumnProps()
    
    sdbddProviPortCod.DataFieldList = "Column 0"
    sdbddProviPortCod.DataFieldToDisplay = "Column 0"
    
    'sdbddArticulos.DataFieldList = "Column 0"
    'sdbddArticulos.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddProviPortCod_PositionList(ByVal Text As String)
      
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddProviPortCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProviPortCod.Rows - 1
            bm = sdbddProviPortCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProviPortCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProviPortCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddProviPortCod_ValidateList(Text As String, RtnPassed As Integer)
Dim scod As String
Dim ador As ador.Recordset
    
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True
        
    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddProviPortCod.Columns("COD").Value = Text Then
            RtnPassed = True
            sdbgTabla.Columns("DEN_FSP").Value = sdbddProviPortCod.Columns("DEN").Value
            Exit Sub
        Else
            Set ador = g_oRaiz.DevolverTodasLasProvinciasDesde(IdPaiPortalSeleccionado, sdbgTabla.Columns("COD_FSP").Value, , True)
            
            If ador Is Nothing Then
                RtnPassed = 0
            Else
                If ador.EOF Then
                    RtnPassed = 0
                Else
                    
                    
                    sdbgTabla.Columns("COD_FSP").Value = ador("COD").Value
                    sdbgTabla.Columns("DEN_FSP").Value = ador("DEN").Value
                        
                    RtnPassed = 1
                End If
            End If
            
            
        End If
        
    End If
End Sub

Private Sub sdbddProviPortDen_CloseUp()
    
    sdbgTabla.Columns("COD_FSP").Value = sdbddProviPortDen.Columns("COD").Value
    
End Sub

Private Sub sdbddProviPortDen_DropDown()
    Dim ador As ador.Recordset
    
    If sdbgTabla.AllowUpdate = False Then
       sdbddProviPortDen.DroppedDown = False
    End If
    
    
    sdbddProviPortDen.RemoveAll
    
    Set ador = g_oRaiz.DevolverTodasLasProvinciasDesde(IdPaiPortalSeleccionado, , sdbgTabla.Columns("DEN_FSP").Value, , True)
    
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbddProviPortDen.AddItem ador("DEN").Value & Chr(9) & ador("COD").Value
            ador.MoveNext
        
        Wend
        
    End If
    
        
End Sub

Private Sub sdbddProviPortDen_InitColumnProps()

    sdbddProviPortDen.DataFieldList = "Column 0"
    sdbddProviPortDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbddProviPortDen_PositionList(ByVal Text As String)
     Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddProviPortDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProviPortDen.Rows - 1
            bm = sdbddProviPortCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProviPortDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProviPortDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbgTabla_BeforeUpdate(Cancel As Integer)
Dim oError As CTESError

    'Codigo para hacer el update
    If sdbgTabla.Columns("COD_FSP").Value <> "" And sdbgTabla.Columns("DEN_FSP").Value <> "" Then
        
        Set oError = oCia.EnlazarProvincia(CodPaiSeleccionado, sdbgTabla.Columns("COD_FSGS").Value, CodPaiPortalSeleccionado, sdbgTabla.Columns("COD_FSP").Value)
        If oError.numerror <> 0 Then
            basErrores.TratarError oError
            bHayErrores = True
            Cancel = True
        Else
            bHayErrores = False
        End If
    Else
        If sdbgTabla.Columns("COD_FSP").Value = "" Then
            Set oError = oCia.EnlazarProvincia(CodPaiSeleccionado, sdbgTabla.Columns("COD_FSGS").Value, "", "")
            If oError.numerror <> 0 Then
                    basErrores.TratarError (oError)
                    bHayErrores = True
                    Cancel = True
                Else
                    sdbgTabla.Columns("DEN_FSP").Value = ""
                    bHayErrores = False
                End If
            Else
                Cancel = True
                bHayErrores = True
            End If
    End If
    
End Sub

Private Sub sdbgTabla_Change()
    
    'If Not bRespetarDropdown Then
     '   sdbgTabla.Columns("COD_FSP").Value = ""
      '  sdbgTabla.Columns("DEN_FSP").Value = ""
    'End If
    
End Sub

Private Sub sdbgTabla_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyEscape Then
        sdbgTabla.CancelUpdate
        bHayErrores = False
    End If
     
End Sub

Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_COORDINACIONTABLAPROVI, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        ador.MoveNext
        Me.Cmdlistado.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdModoEdicion.Caption = ador(0).Value
        sIdiEdicion = ador(0).Value
        ador.MoveNext
        Me.lblName.Caption = ador(0).Value
        ador.MoveNext
        Me.lblPortal.Caption = ador(0).Value
        ador.MoveNext
        Me.sdbcPaiCod.Columns(1).Caption = ador(0).Value
        Me.sdbcPaiDen.Columns(2).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbcPaiCod.Columns(2).Caption = ador(0).Value
        Me.sdbcPaiDen.Columns(1).Caption = ador(0).Value
        ador.MoveNext
        
        Me.sdbddProviPortCod.Columns(0).Caption = ador(0).Value
        Me.sdbddProviPortDen.Columns(1).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbddProviPortCod.Columns(1).Caption = ador(0).Value
        Me.sdbddProviPortDen.Columns(0).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgTabla.Columns(0).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgTabla.Columns(1).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgTabla.Columns(2).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgTabla.Columns(3).Caption = ador(0).Value
        ador.MoveNext
        sIdiConsulta = ador(0).Value
        ador.Close
    End If
    
    Set ador = Nothing
           
End Sub



